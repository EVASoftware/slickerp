package com.slicktechnologies.client.views.device;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import java_cup.internal_error;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.thirdparty.javascript.rhino.head.ast.FunctionNode.Form;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.Slick_Erp;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.notification.NotificationService;
import com.slicktechnologies.client.notification.NotificationServiceAsync;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.services.CommunicationLogServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.CustomerProjectService;
import com.slicktechnologies.client.services.CustomerProjectServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.services.ServiceListService;
import com.slicktechnologies.client.services.ServiceListServiceAsync;
import com.slicktechnologies.client.services.SmsService;
import com.slicktechnologies.client.services.SmsServiceAsync;
import com.slicktechnologies.client.services.UpdateService;
import com.slicktechnologies.client.services.UpdateServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utility.UnitConversionUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.MultipleExpensemanagmentForm;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.MultipleExpensemanagmentPresenter;
import com.slicktechnologies.client.views.communicationlog.CommunicationLogPopUp;
import com.slicktechnologies.client.views.complain.ComplainForm;
import com.slicktechnologies.client.views.complain.ComplainPresenter;
import com.slicktechnologies.client.views.complain.ServiceDateBoxPopup;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.contract.StackDetailsPopUp;
import com.slicktechnologies.client.views.fumigationAustralia.FumigationAustraliaForm;
import com.slicktechnologies.client.views.fumigationAustralia.FumigationAustraliaPresenter;
import com.slicktechnologies.client.views.fumigationdetails.FumigationALPForm;
import com.slicktechnologies.client.views.fumigationdetails.FumigationALPPresenter;
import com.slicktechnologies.client.views.fumigationdetails.FumigationForm;
import com.slicktechnologies.client.views.fumigationdetails.FumigationPresenter;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.FromAndToDateBoxPopup;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListPresenter;
import com.slicktechnologies.client.views.popups.NewEmailPopUp;
import com.slicktechnologies.client.views.popups.SMSPopUp;
import com.slicktechnologies.client.views.popups.ServiceCancellationPopup;
import com.slicktechnologies.client.views.project.concproject.ProjectForm;
import com.slicktechnologies.client.views.project.concproject.ProjectPresenter;
import com.slicktechnologies.server.android.contractwiseservice.MarkCompletedMultipleServices;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.ModificationHistory;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.FeedbackUrlAsPerCustomerCategory;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.service.StackDetails;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.smshistory.SmsHistory;

public class DevicePresenter extends FormScreenPresenter<Service> implements ValueChangeHandler<String>, SelectionHandler<Suggestion> ,ClickHandler,ChangeHandler{

	//Token to set the concrete FormScreen class name
	public  DeviceForm form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	ServicReschdulePopUp reschedule=new ServicReschdulePopUp();
	/*** Date 20-03-2019 by Vijay commeted new popup added with named as CancellationPopup ****/
//	ServiceCancellation cancellation=new ServiceCancellation();
	SaveServiceAsync asyncService=GWT.create(SaveService.class);
	PopupPanel panel=new PopupPanel(true);
	GenricServiceAsync genasync=GWT.create(GenricService.class);
	static List<SalesLineItem> lineItemLis=null;
	ServiceMarkCompletePopUp markcompletepopup=new ServiceMarkCompletePopUp();
	PopupPanel markpanel=new PopupPanel(true);
	CustomerProjectServiceAsync projectAsync=GWT.create(CustomerProjectService.class);
	final SmsServiceAsync smsserviceAsync=GWT.create(SmsService.class);
	
	
	//Added by Rahul
		TrackServicePopUp trackServicePopUp=new TrackServicePopUp();
		List<TrackTableDetails> tracktablelist;
	
	 FromAndToDateBoxPopup frmAndToPop=new FromAndToDateBoxPopup();
	
	 ServiceDateBoxPopup datebox = new ServiceDateBoxPopup("Valid Until",null);
	 PopupPanel Panel = new PopupPanel(true);
	 
	 /** commented by komal **/
		//ConditionDialogBox conditionPopup=new ConditionDialogBox("Do you want create fumigation certificate for Australia !",AppConstants.YES,AppConstants.NO);
		 /** date 06.04.2018 added by komal **/
		ConditionDialogBox conditionPopup = new ConditionDialogBox("Select fumigation certificate : " , AppConstants.FUMIGATIONMB , AppConstants.FUMIGTAIONALP , AppConstants.FUMIGATIONAFAS);    
		PopupPanel panel123;
		
	
	
	//************changes changes here ****************
			ConditionDialogBox conditionPopupForPrint=new ConditionDialogBox("Do you want to print on preprinted Stationery",AppConstants.YES,AppConstants.NO);
			PopupPanel panelForSericeCertificate ;
		//****************ends here ***********	
	
	/****** For SMS *****/
	 private long cellNo;
	 private String companyName;
	 private String actualMsg;
	 boolean antipestFlag = false;
	 /**Date 17-1-2020 by Amol Added a flag For New Bydefault Service Voucher Pdf **/
	 boolean custServRecPdf=false;
	 
	 /** Date 26-04-2019 by vijay commented below code updated new code with new popup ****/
//	  date : 
//	StackDetailsPopUp stackDetailsPopUp = new StackDetailsPopUp();
//	PopupPanel stackDetailsPanel;

	/**
	 * 	Created By : nidhi
	 *  Date : 18-11-2017
	 *  Discription : add for download fumigation value and contract revanue details 
	 *  
	 *  
	 */
	UpdateServiceAsync updateSer = GWT.create(UpdateService.class);
	/**
	 * nidhi
	 *  ||*
	 *  11-10-2018
	 */
	 HashMap<Integer,String> billingList = new HashMap<Integer,String>();
		SaveServiceAsync mapService=GWT.create(SaveService.class);
		
		PopupPanel panelForWPM;
		ConditionDialogBox conditionPopupForWPMPrint=new ConditionDialogBox("Do you want to print on preprinted Stationery",AppConstants.YES,AppConstants.NO);
		
		
		/*
		 * @author Ashwini
		 * Date:24-01-2018
		 */
		PopupPanel panelForCommodityfm;
		ConditionDialogBox popupForCommodityFum = new ConditionDialogBox("Do you want to print on preprinted Stationery", AppConstants.YES, AppConstants.NO);
		
		/*
		 * end by Ashwini
		 */
		
		/**
		 * Date 23-03-2019 by Vijay for NBHC CCPM service cancellation with dropdown and normal textbox
		 */
		ServiceCancellationPopup  cancellation = new ServiceCancellationPopup();
		/*** Date 25-04-2019 by Vijay for NBHC CCPM Stack details old code updated with new popup **********/
		StackDetailsPopUp stackDetailsPopUp = new StackDetailsPopUp();
		
		/**
		 * @author Anil , Date : 19-09-2019
		 * 
		 */
		ConditionDialogBox complainBillPopup=new ConditionDialogBox("Do you want to create bill for this service.",AppConstants.YES,AppConstants.NO);
		
		final ServiceListServiceAsync servicelistserviceAsync = GWT.create(ServiceListService.class);

		ConditionDialogBox conditionPopupForpecoppPrint=new ConditionDialogBox("Do you want to print on preprinted Stationery",AppConstants.YES,AppConstants.NO);
		PopupPanel panelForpecoppSericeCertificate ;
		int count=0;
		int pecopp;
		
		
		ConditionDialogBox conditionPopupForSRFormatversion1Print=new ConditionDialogBox("Do you want to print on preprinted Stationery",AppConstants.YES,AppConstants.NO);
		PopupPanel panesrformatversion1 ;
	
		int srcopyversion1=0;
		
		NewEmailPopUp emailpopup = new NewEmailPopUp();
		SMSPopUp smspopup = new SMSPopUp();

		CommunicationLogPopUp CommunicationLogPopUp = new CommunicationLogPopUp();
		CommunicationLogServiceAsync communicationService = GWT.create(CommunicationLogService.class);
		ServiceDateBoxPopup otpPopup=new ServiceDateBoxPopup("OTP"); //Ashwini Patil Date:5-07-2023 to restrict download with otp for hi tech client
		PopupPanel otppopupPanel;
		int generatedOTP;
		SmsServiceAsync smsService = GWT.create(SmsService.class);
		
		ServiceCancellationPopup  suspendservicepopup = new ServiceCancellationPopup();
		GeneralViewDocumentPopup genralInvoicePopup; 
		public static PopupPanel generalPanel;

	public DevicePresenter  (FormScreen<Service> view, Service model) {
		super(view, model);
		form=(DeviceForm) view;
		form.setPresenter(this);
		form.tbContractId.addValueChangeHandler(this);
		reschedule.getBtnReschedule().addClickHandler(this);
		reschedule.getBtnCancel().addClickHandler(this);
		form.prodComposite.getProdID().addSelectionHandler(this);
		form.prodComposite.getProdCode().addSelectionHandler(this);
		form.prodComposite.getProdName().addSelectionHandler(this);
		markcompletepopup.getBtnOk().addClickHandler(this);
		
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
		/** date 06.04.2018 added by komal for  fumigation **/
		conditionPopup.getBtnThree().addClickHandler(this);
		
		
		frmAndToPop.btnOne.addClickHandler(this);
		frmAndToPop.btnTwo.addClickHandler(this);
		
		markcompletepopup.getBtnCancel().addClickHandler(this);
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.DEVICE,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
		datebox.getBtnOne().addClickHandler(this);
		datebox.getBtnTwo().addClickHandler(this);
		
		
		form.catchAddButton.addClickHandler(this);
		
		//Added by Rahul
		form.btstackDetails.addClickHandler(this);
//		stackDetailsPopUp.getBtnOk().addClickHandler(this);
//		stackDetailsPopUp.getBtnCancel().addClickHandler(this);
		
		/**
		 * Rahul Verma added this on 28 Sept 2018
		 * @author Anil,Date : 09-01-2019
		 * commented this ,it is checking some validation for android app for sending notification
		 */
//		form.tbServiceEngg.addChangeHandler(this);
		/**
		 * Ends for Rahul
		 */
		
		/**
		 * Date 27-09-2018 By Vijay
		 * for NBHC CCPM Service record print on preeprint stationary
		 */
		conditionPopupForPrint.getBtnOne().addClickHandler(this);
		conditionPopupForPrint.getBtnTwo().addClickHandler(this);

		
		/*
		 * Date:24-01-2018
		 * @author AshwiniprintCommodityFumigationReport
		 * Des:for nbhc wpm fumigation record print on preeprint stationary
		 */
		
		conditionPopupForWPMPrint.getBtnOne().addClickHandler(this);
		conditionPopupForWPMPrint.getBtnTwo().addClickHandler(this);
		
		popupForCommodityFum.getBtnOne().addClickHandler(this);
		popupForCommodityFum.getBtnTwo().addClickHandler(this);
		
      /*
       * end by Ashwini
       */
		
		/**
		 * Date 23-03-2019 by Vijay for service cancellation popup
		 */
		cancellation.getLblOk().addClickHandler(this);
		cancellation.getLblCancel().addClickHandler(this);
		/**
		 * ends here
		 */
		/*** Date 26-04-2019 by Vijay for NBHC CCPM Stack details form contract ****/
		stackDetailsPopUp.getLblOk().addClickHandler(this);
		
		complainBillPopup.getBtnOne().addClickHandler(this);
		complainBillPopup.getBtnTwo().addClickHandler(this);
		
		form.tbserviceBranch.addChangeHandler(this);
		
		conditionPopupForpecoppPrint.getBtnOne().addClickHandler(this);//conditionPopupForpecoppPrint
		conditionPopupForpecoppPrint.getBtnTwo().addClickHandler(this);
		
		
		CommunicationLogPopUp.getBtnOk().addClickHandler(this);
		CommunicationLogPopUp.getBtnCancel().addClickHandler(this);
		CommunicationLogPopUp.getBtnAdd().addClickHandler(this);
		
		conditionPopupForSRFormatversion1Print.getBtnOne().addClickHandler(this);
		conditionPopupForSRFormatversion1Print.getBtnTwo().addClickHandler(this);
		
		otpPopup.getBtnOne().addClickHandler(this);
		otpPopup.getBtnTwo().addClickHandler(this);

		suspendservicepopup.getLblOk().addClickHandler(this);
		suspendservicepopup.getLblCancel().addClickHandler(this);
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e)
	{
		//super.reactToProcessBarEvents(e);
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		if(text.equals("Manage Customer Project"))
		{
//			/**
//			 * Date 13 Feb 2017
//			 * by Vijay
//			 * Description :- if condition for nbhc service engineer is mondatory if we click manage customer project then validation will
//			 * display message 
//			 * else for all clients service engineer non mondatory
//			 */
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","OnlyForNBHC"))
//			{
////				if(form.tbServiceEngg.getSelectedIndex()==0){
////					form.showDialogMessage("Please add Service engineer first!");
////				}else{
//					/**
//					 * Date : 11-02-2017 By Anil
//					 * changing label from manage customer project to processing
//					 * so if user clicks that button twice then it will not process for second time
//					 */
//					label.setText("Processing.....");
//					/**
//					 * End
//					 */
//					createProjectCorrespondingToService("Button");
////				}
//				
//				
//			}else{
			
				/**
				 * Date 31-12-2018 By Vijay
				 * Des :- NBHC for BOM need Quantity and UOM is Mandatory 
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MakeQty&UomMandatory")){
					if(form.quantity.getValue()!=null && form.quantity.getValue()==0){
						form.showDialogMessage("Quantity is mandatory!");
						return;
					}
					if(form.olbUOM.getValue()==null){
						form.showDialogMessage("UOM is mandatory!");
						return;
					}
				}
				/**
				 * ends here
				 */
				
				 /***
				  * Date 31-12-2018
				  * Des :- for old services which have already created but dont have the bom so added
				  * validation
				  */
				 if(LoginPresenter.billofMaterialActive){
					 
					 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(model.getProduct());
					 if(billPrDt!=null){
						 UnitConversionUtility unitConver = new UnitConversionUtility();
						 if(!unitConver.varifyUnitConversion(billPrDt, form.olbUOM.getValue())){
							 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
							 return;
						 }
					 }
				 }
				
				if(form.isPopUpAppMenubar()){
							Console.log("Device POPUP : Manage Customer Project clicked! returning");
							return;				
				}
				 /**
				  * end
				  */
				/**
				 * Date : 11-02-2017 By Anil
				 * changing label from manage customer project to processing
				 * so if user clicks that button twice then it will not process for second time
				 */
				label.setText("Processing.....");
				/**
				 * End
				 */
				createProjectCorrespondingToService("Button");
//			}
			
			/**
			 * end here
			 */
		}
		
		if(text.equals("Reschedule"))
		{
			if(!model.getStatus().equalsIgnoreCase(Service.SERVICESTATUSTECHCOMPLETED)){
				
				if(model.getStatus().equals(Service.SERVICETCOMPLETED)){
					form.showDialogMessage("This service is marked "+Service.SERVICETCOMPLETED);
				}
				/**
				 * @author Vijay Chougule Date :- 19-06-2020
				 * Des :- Suspended Service can not be reschedule added else if condition for the validation
				 */
				else if(model.getStatus().equals(Service.SERVICESUSPENDED)){
					form.showDialogMessage("This service is marked "+Service.SERVICESUSPENDED);
				}
				else{
					reactOnReschedule();
				}
			}else{
				form.showDialogMessage("This service is already marked "+Service.SERVICESTATUSTECHCOMPLETED);
			}
			
		}
		
		if(text.equals("Periodic Servic record"))
		{
			printServiceRecord();
		}
		
		if(text.equals("WPM Fumigation"))
		{
			printFumigationCertificate();
		}
		if(text.equals("Commodity Fumigation"))
		{
			printCommodityFumigationReport();
		}
		
		if(text.equals("Mark Completed"))
		{
			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "ComplainServiceWithTurnAroundTime")) {
				reactonMarkComplete();
				return;
			}
			
			//for orion 30-10-2023
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio", "PC_DocumentUploadMandatory")){
				if(form.getUpTestReport().getValue()==null){
					form.showDialogMessage("Service Report upload is mandatory! Please upload Service Report in Service ");
					return;
				}						
			}
			
			
			/*
			 * Added by Sheetal:27-10-2021
			 * Des : Document upload in Customer Report section should be mandatory before closure of service
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "PC_DocumentUploadMandatory")){
				if(AppUtility.ValidateCustomerReport(form.getUpTestReport().getValue(),form.serviceSummaryReport2.getValue())){
					form.showDialogMessage("Please upload atleast one document in Customer Report tab on Customer Service page to complete service");
					return;
				}
				
			}
		
			/*
			 * Ashwini Patil 
			 * Date:29-08-2024
			 * Orion does not want to allow service completion of last month services after specific deadline
			 * deadline is defined in company - Customer service setup - Service completion deadline field
			 */
			boolean validDate=AppUtility.validateServiceCompletionDate(form.tbServiceDate.getValue());
			Console.log("validDate="+validDate);
			if(!validDate) {
				form.showDialogMessage("You cannot completed old service now!");
				return;
			}
			
			
			/** date 18.4.2018 added by komal for orion **/
			Console.log("report: "+ form.getUpTestReport());
			boolean summaryFlag = false;
			if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceSummaryMandatoryForServiceCompletion")) {
				if (form.getUpTestReport().getValue() == null) {
					Console.log("report: "+ form.getUpTestReport());
					summaryFlag = true;
				}
			}
			if (summaryFlag) {
                form.showDialogMessage("Service can not complete as service summary is not uploaded.");
			} else if (AppUtility.checkForProcessConfigurartionIsActiveOrNot(
					"Service", "CheckAreaInCustomerBranchAndService")) {
				checkCustomerBranchArea();
			} 
			/**
			 * @author Vijay Chougule Date 21-09-2019 
			 * Des :- NBHC CCPM complaint service assessment must be close
			 */
			else if(form.tbTicketNumber.getValue()!=null && form.tbTicketNumber.getValue().length()> 0 &&
					AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableValidateAssesmentReport")){
				reactonValidateAssessment();
			}
			else {			
                 reactOnMarkComplete();
			}
		}
		
		if(text.equals("Cancel"))
		{
			reactOnCancel();
		}
		
		if(text.equals("Close"))
		{
			reactOnClose();
		}
		
		if(text.equals("New"))
		{
			if(form.isPopUpAppMenubar()){
				Console.log("Device POPUP : New clicked!");
				return;
			}
			
			reactOnNew();
		}
		
		/**
		 * @author Vijay Date 26-11-2020
		 * Des :- as per nitin sir commented below code no use
		 */
//		if(text.equals("Create Fumigation")){
//			if(model.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCOMPLETED)){
//				System.out.println("in side react on prinnt");
//				panel123=new PopupPanel(true);
//				panel123.add(conditionPopup);
//				panel123.setGlassEnabled(true);
//				panel123.show();
//				panel123.center();
//			}else{
//				form.showDialogMessage("Service should be completed before creating fumigation.");
//			}
//			
//			
//		}
		
		if (text.equals("Reminder SMS")){
			sendSMSLogic();
		}
		
		if (text.equals("Update Entity")){
			panel = new PopupPanel(true);
			panel.add(frmAndToPop);
			panel.show();
			panel.center();
		}
		
		if(text.equals("Print Service Certificate")){
			reactOnServiceCertificate();
		}
		
		if(text.equals("Service Details")){
			reactOnServiceDetails();
		}
		
		if(text.equals("Reverse Service Completion"))
		{
			reactOnReverseServiceCompletion();
		}
		
		/**
		 * Date : 16-03-2017 By ANIL
		 */
		if(text.equals("MIS")){
			final String url = GWT.getModuleBaseURL()+"pdfprintservice"+"?Id="+model.getId()
					+"&"+"type="+"MIS";
			Window.open(url, "test", "enabled");
		}
		/**
		 * Date : 31-10-2017 BY ANIL
		 */
		if(text.equals(AppConstants.VIEWBILL)){
			if(form.isPopUpAppMenubar()){
				Console.log("Device POPUP : View Bill clicked!");
				return;
			}
			reactOnViewBill();
		}
		
		/**@Sheetal : 15-02-2022**/
		if(text.equals(AppConstants.VIEWORDER)){
			if(form.isPopUpAppMenubar()){
				Console.log("Device POPUP : View Order clicked!");
				return;
			}
			reactOnViewOrder();
		}
		/**
		 * end
		 */
		
		/**
		 * Date : 27/03/2018 By:Manisha
		 * 
		 */
		
		if(text.equals("printIntimationLetter"))
		{
			final String url = GWT.getModuleBaseURL() + "PcambIntimationLetterpdf"+"?Id="+model.getId();
			Window.open(url, "test", "enabled");
		}
		/**End
		
		/**
		 *  nidhi
		 *  10-11-2017
		 *  for Daily fumigation report 
		 *  
		 */
		if(text.equals(AppConstants.SERVICEFUMIGATIONREPORT)){
			updateSer.dailyFumigationReportDetails(model.getCompanyId(), null, new AsyncCallback<Void>() {
				
				@Override
				public void onSuccess(Void result) {

					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "CreateXLXSServlet"+"?type="+1;
					Window.open(url, "test", "enabled");
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		
		
		/**
		 *  nidhi
		 *  16-11-2017
		 *  for Daily fumigation value report 
		 *  
		 */
		if(text.equals(AppConstants.SERVICEFUMIGATIONVALUEREPORT)){
			updateSer.dailyFumigationValueReportDetails(model.getCompanyId(), null, new AsyncCallback<Void>() {
				
				@Override
				public void onSuccess(Void result) {

					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "CreateXLXSServlet"+"?type="+2;
					Window.open(url, "test", "enabled");
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		
		
		/**
		 * Date 10/3/2018 
		 * By Jayshree 
		 * Des.add The suspend button to hold the services
		 */
		
		
		
		if(text.equals(AppConstants.SUSPEND)){
			
			if(model.getStatus().equalsIgnoreCase(Service.SERVICESTATUSSCHEDULE)||model.getStatus().equalsIgnoreCase(Service.SERVICESTATUSRESCHEDULE))
			{
//				model.setStatus(Service.SERVICESUSPENDED);
//				form.showDialogMessage("Service Get Suspended ");
//				
//			final ModificationHistory history=new ModificationHistory();
//			history.oldServiceDate=model.getServiceDate();
//			history.resheduleDate=model.getServiceDate();
//			
//			history.resheduleTime=model.getServiceTime();
//			
//			history.resheduleBranch=model.getBranch();
//			history.reason="Srvice is Suspended ";
//			history.userName=LoginPresenter.loggedInUser;
//			history.systemDate=new Date();
//		
//			model.getListHistory().add(history);
//			
////			form.table.getDataprovider().getList().add(history);
//			
////			form.table.getTable().redraw();
//			
////			}
////			else if(model.getStatus().equalsIgnoreCase(Service.SERVICESUSPENDED)){
////				model.setStatus(Service.SERVICESTATUSRESCHEDULE);
////				form.showDialogMessage("Service Get Reschedule ");
////			}
//			
//			
//				genasync.save(model, new AsyncCallback<ReturnFromServer>() {
//
//				@Override
//				public void onFailure(Throwable caught) {
//					
//				}
//
//				@Override
//				public void onSuccess(ReturnFromServer result) {
//					System.out.println("model.getStatus()"+model.getStatus());
//					form.tbStatus.setValue(model.getStatus());
//					form.toggleProcessLevelMenu();
//					
////					form.table.setEnable(true);
//					form.table.getDataprovider().getList().add(history);
//				}
//			});
				
				
				suspendservicepopup.showPopUp();
		  }
		  else{
			  form.showDialogMessage("Service status should be Scheduled/Rescheduled!");
		  }
				
		}
		
		//End By jayshree
		
		if(text.equals(AppConstants.SERVICEVOUCHER)){
			final String url = GWT.getModuleBaseURL() + "ServiceVoucherPrint" + "?Id="
					+ model.getId();
			Window.open(url, "test", "enabled");
		}
		/** date 26.3.2019 added by komal by for sr copy of nbhc **/
		if(text.equalsIgnoreCase("Service Record")){
			printServiceRecord();
		}
		
		if(text.equalsIgnoreCase("View Complain")){
			viewComplain();
		}
		
		if(text.equals("Email")){
			reactOnEmail();
		}
		/**
		 * @author Anil
		 * @since 20-05-2020
		 * For premium tech raised by rahul tiwari
		 * need to register/view expense from from service
		 */
		if(text.equals("Register Expense")){
//			reactOnRegisterExpense();
			reactonRegisterExpenseToOpenPopup();//Ashwini Patil Date:8-07-2024 to open expenses in popup
		}
		
		if(text.equals("View Expense")){
//			reactOnViewExpense();
			reactonViewExpenseToOpenPopup();//Ashwini Patil Date:8-07-2024 to open expenses in popup
		
		}
	}

	

	private void reactOnViewOrder() {
		MyQuerry querry = new MyQuerry();
		Vector<Filter> filterVec = new Vector<Filter>();
		Filter filter;
		
		filter = new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(model.getContractCount());
		filterVec.add(filter);
		
		querry.setFilters(filterVec);
		querry.setQuerryObject(new Contract());
		form.showWaitSymbol();
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("Contract Document Not Found!");
				}else{
					final Contract contract = (Contract) result.get(0);
					final ContractForm form = ContractPresenter.initalize();
					Timer timer = new Timer() {
						
						@Override
						public void run() {
							form.updateView(contract);
							form.setToViewState();
						}
					};
					timer.schedule(1000); 
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}
		});
	
		
	}

	private void reactOnViewBill() {
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("rateContractServiceId");
		filter.setIntValue(model.getCount());
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new BillingDocument());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No billing document found.");
					return;
				}
				if(result.size()==1){
					final BillingDocument billDocument=(BillingDocument) result.get(0);
					final BillingDetailsForm form=BillingDetailsPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				}else{
					BillingListPresenter.initalize(querry);
				}
				
			}
		});
		
	}

	/**
	 * Date : 21-03-2017 By Anil
	 * this method checks service complete
	 */
	private boolean validateCompletion() {
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC")){
			System.out.println("Config true");
			if(!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
				System.out.println("Not an admin");
				DateTimeFormat format= DateTimeFormat.getFormat("dd/MM/yyyy");
				Date serviceDate=form.tbServiceDate.getValue();
				Date lastValidDate=CalendarUtil.copyDate(serviceDate);
				Console.log("Service Date : "+format.format(serviceDate));
				System.out.println("Service Date : "+format.format(serviceDate));
				if(serviceDate!=null){
					CalendarUtil.addMonthsToDate(lastValidDate, 1);
					lastValidDate.setDate(5);
					Console.log("Valid/Deadline date "+format.format(lastValidDate));
					System.out.println("Valid/Deadline date "+format.format(lastValidDate));
					Date todaysDate=new Date();
					lastValidDate=format.parse(format.format(lastValidDate));
					todaysDate=format.parse(format.format(todaysDate));
					Console.log("Todays Date : "+format.format(todaysDate));
					System.out.println("Todays Date : "+format.format(todaysDate));
					if(todaysDate.after(lastValidDate)){
						form.showDialogMessage("Service completion of previous month is not allowed..!");
						return false;
					}
				}else{
					form.showDialogMessage("Service date is null");
					return false;
				}
			}
			return true;
		}else{
			System.out.println("Config false");
			return true;
		}
	}
	
	private void reactOnReverseServiceCompletion() {
		
		/**Date 6-3-2020 by Amol added a Process Configuration for to reverse the service Status
		 * as "Open" raised by Rahul T. for Orkin
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceStatusAsPlanned")){
			model.setStatus(Service.SERVICESTATUSOPEN);
			model.setServiceCompletionDate(null);
		}else{
			model.setStatus(Service.SERVICESTATUSSCHEDULE);	
		}
		model.setServiceCompleteDuration(0);
		model.setServiceCompleteRemark("");
		model.setServiceCompletionDate(null);
		form.showWaitSymbol();
		Timer timer=new Timer() 
    	 {
				@Override
				public void run() {
					genasync.save(model,new AsyncCallback<ReturnFromServer>() {
						
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected Error occured !");
					}

					@Override
					public void onSuccess(ReturnFromServer result) {
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceStatusAsPlanned")){
							form.getTbStatus().setValue(Service.SERVICESTATUSOPEN);	
							form.getDbServiceCompletionDate().setValue(null);
						}else{
							form.getTbStatus().setValue(Service.SERVICESTATUSSCHEDULE);	
						}
						form.getDbServiceCompletionDate().setValue(null);
						form.getTbservicecompletionremark().setValue("");
						form.getIbServiceDuration().setValue(0);
						form.setToViewState();
						form.setAppHeaderBarAsPerStatus();
						form.showDialogMessage("Updated Successfully!");
					}
					});
						form.hideWaitSymbol();
					}
 			};
            timer.schedule(3000);
	}

	private void addDefaultValueInMarkCompletePopUp(ServiceMarkCompletePopUp markcompletepopup2) {
		
		Console.log("Inside addDefaultValue");
		Console.log("model.getServiceCompleteDuration() :::"+model.getServiceCompleteDuration());
		Console.log("model.getServiceCompletionDate():::"+model.getServiceCompletionDate());
		Console.log("model.getServiceCompleteRemark():::"+model.getServiceCompleteRemark());

		if (model.getServiceCompleteDuration() != 0)
			markcompletepopup2.getServiceTime().setValue(
					model.getServiceCompleteDuration());

		/*if (model.getServiceCompletionDate() != null) {
			DateTimeFormat sdf = DateTimeFormat.getFormat("dd/MM/yyyy");

			markcompletepopup2.getServiceCompletionDate().setValue(
					sdf.parse(sdf.format(model.getServiceCompletionDate())));

		}*/
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "SetServiceDateAsCompletionDate") && model.getServiceDate()!=null){
			DateTimeFormat sdf = DateTimeFormat.getFormat("dd/MM/yyyy");

			markcompletepopup2.getServiceCompletionDate().setValue(
					sdf.parse(sdf.format(model.getServiceDate())));
		}else if (model.getServiceCompletionDate() != null) {
			DateTimeFormat sdf = DateTimeFormat.getFormat("dd/MM/yyyy");

			markcompletepopup2.getServiceCompletionDate().setValue(
					sdf.parse(sdf.format(model.getServiceCompletionDate())));

		}

		if (model.getServiceCompleteRemark() != null
				&& !model.getServiceCompleteRemark().equalsIgnoreCase("")) {
			markcompletepopup2.getRemark().setValue(
					model.getServiceCompleteRemark().trim());
		}

		if (model.getCustomerFeedback() != null
				&& !model.getCustomerFeedback().equalsIgnoreCase("")) {
			int index = 0;
			for (int i = 0; i < markcompletepopup2.getLbRating().getItemCount(); i++) {
				if (model
						.getCustomerFeedback()
						.trim()
						.equalsIgnoreCase(
								markcompletepopup2.getLbRating().getValue(i))) {
					index = i;
				}
			}
			
			Console.log("index:::"+index);
			markcompletepopup2.getLbRating().setSelectedIndex(index);
		}
		
	}

	/*
	 * commented by Ashwini
	 */
	
//	private void printCommodityFumigationReport() {
//		final String url = GWT.getModuleBaseURL() + "commodityFumigationReport"+"?Id="+model.getId();
//		Window.open(url, "test", "enabled");		
//	}
	
	/*
	 * Date:24-01-2018
	 * @author Ashwini
	 */
	
	private void printCommodityFumigationReport(){
		panelForCommodityfm = new PopupPanel(true);
		panelForCommodityfm.add(popupForCommodityFum);
		panelForCommodityfm.setGlassEnabled(true);
		panelForCommodityfm.show();
		panelForCommodityfm.center();
		
	}
	
	/*
	 * end by Ashwini
	 */
	private void reactOnServiceDetails() {
		// TODO Auto-generated method stub
		System.out.println("Clicked on Track Service");
		Panel = new PopupPanel(true);
		Panel.add(trackServicePopUp);
		Panel.show();
		Panel.center();
		System.out.println("Track Table List:::::::::::"+tracktablelist);
		trackServicePopUp.trackServTable.getDataprovider().setList(model.getTrackServiceTabledetails());
	}
	
	/*
	 * commented by Ashwini
	 */

//	private void printFumigationCertificate() {
//		
//		final String url = GWT.getModuleBaseURL() + "fumigationCefrtificate"+"?Id="+model.getId();
//		Window.open(url, "test", "enabled");
//	}

	/*
	 * end by Ashwini
	 */
	
	private void printFumigationCertificate() {
		panelForWPM=new PopupPanel(true);
		panelForWPM.add(conditionPopupForWPMPrint);
		panelForWPM.setGlassEnabled(true);
		panelForWPM.show();
		panelForWPM.center();
		
	}
	
	private void printServiceRecord() {
		System.out.println("in side react on prinnt NBHC Quotation");
		
		/**
		 * Date 27-09-2018 By Vijay
		 * for NBHC CCPM Service record print on preeprint stationary
		 */
		panelForSericeCertificate=new PopupPanel(true);
		panelForSericeCertificate.add(conditionPopupForPrint);
		panelForSericeCertificate.setGlassEnabled(true);
		panelForSericeCertificate.show();
		panelForSericeCertificate.center();
		
//		final String url = GWT.getModuleBaseURL() + "serviceRecord"+"?Id="+model.getId();
//		Window.open(url, "test", "enabled");
	}

	private void reactOnServiceCertificate(){
			Panel = new PopupPanel(true);
			Panel.add(datebox);
			Panel.show();
			Panel.center();
	}
	
	

	private void reactOnFumigation() {
		if(model.getProduct().getProductName().equalsIgnoreCase("Methyle Bromide")){
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Fumigation",Screen.FUMIGATION);
		final FumigationForm form=FumigationPresenter.initalize();
		form.showWaitSymbol();
		Timer t = new Timer() {
		      @Override
		      public void run() {
		    	  form.setToNewState();
		    	  form.hideWaitSymbol();
		    	  form.getTbcount().setText("");
		    	  if(model.getContractCount()!=-1){
		    		  form.getTbcontractId().setValue(model.getContractCount()+"");
		    	  }
		    	  
		    	  form.getTbserviceId().setValue(model.getCount()+"");
		    	  form.getDbcontractstartdate().setValue(model.getContractStartDate());
		    	  form.getDbcontractenddate().setValue(model.getContractEndDate());
		    	  form.getDbservicedate().setValue(model.getServiceDate());
		    	  form.getLbnamefumigation().setSelectedIndex(1);
		    	  /**
				    * Updated By: Viraj
				    * Date: 17-04-2019
				    * Description: changed quantity from double to string in fumigation to handle this change below code is writen
				  */
		    	  String quantity="";
		    	  if(model.getQuantity() != 0) {
		    		  quantity = String.valueOf(model.getQuantity());
		    	  }
		    	  form.getIbQuantity().setValue(quantity);
		    	  /** Ends **/
		    	  
		    	  form.getPersonInfoComposite().setValue(model.getPersonInfo());
		    	  form.getPersonInfoComposite().setEnable(false);
		      }
		    };
		    t.schedule(5000);
		}else{
			form.showDialogMessage("Product in service is Aluminium Phosphide.");
		}
	
	}
   /** date 06.04.2018 added by komal for fumigation(ALP)**/
	private void reactOnFumigationALP() {
		if(model.getProduct().getProductName().equalsIgnoreCase("Aluminium Phosphide")){
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Fumigation(ALP)",Screen.FUMIGATIONALP);
		final FumigationALPForm form=FumigationALPPresenter.initialize();
		form.showWaitSymbol();
		Timer t = new Timer() {
		      @Override
		      public void run() {
		    	  form.setToNewState();
		    	  form.hideWaitSymbol();
		    	  form.getTbcount().setText("");
		    	  if(model.getContractCount()!=-1){
		    		  form.getTbcontractId().setValue(model.getContractCount()+"");
		    	  }
		    	  
		    	  form.getTbserviceId().setValue(model.getCount()+"");
		    	  form.getDbcontractstartdate().setValue(model.getContractStartDate());
		    	  form.getDbcontractenddate().setValue(model.getContractEndDate());
		    	  form.getDbservicedate().setValue(model.getServiceDate());
		    	  form.getLbnamefumigation().setSelectedIndex(1);
		    	  /**
				    * Updated By: Viraj
				    * Date: 17-04-2019
				    * Description: changed quantity from double to string in fumigation to handle this change below code is writen
				  */
		    	  String quantity="";
		    	  if(model.getQuantity() != 0) {
		    		  quantity = String.valueOf(model.getQuantity());
		    	  }
		    	  form.getIbQuantity().setValue(quantity);
		    	  /** Ends **/
		    	  form.getPersonInfoComposite().setValue(model.getPersonInfo());
		    	  form.getPersonInfoComposite().setEnable(false);
		      }
		    };
		    t.schedule(5000);
		}else{
			form.showDialogMessage("Product in service is Methyle Bromide");
		}
	}
	/**
	 * end komal
	 */
	private void reactOnFumigationForAus(){
		 /** date 06.04.2018 changed by komal for fumigation(ALP)**/
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Fumigation(AFAS)",Screen.FUMIGATIONAUSTRALIA);
		final FumigationAustraliaForm form=FumigationAustraliaPresenter.initalize();
		form.showWaitSymbol();
		Timer t = new Timer() {
		      @Override
		      public void run() {
		    	  form.setToNewState();
		    	  form.hideWaitSymbol();
		    	  form.getTbcount().setText("");
		    	  if(model.getContractCount()!=-1){
		    		  form.getTbcontractId().setValue(model.getContractCount()+"");
		    	  }
		    	  
		    	  form.getTbserviceId().setValue(model.getCount()+"");
		    	  form.getDbcontractstartdate().setValue(model.getContractStartDate());
		    	  form.getDbcontractenddate().setValue(model.getContractEndDate());
		    	  form.getDbservicedate().setValue(model.getServiceDate());
		    	  /**
				    * Updated By: Viraj
				    * Date: 17-04-2019
				    * Description: changed quantity from double to string in fumigation to handle this change below code is writen
				  */
		    	  String quantity="";
		    	  if(model.getQuantity() != 0) {
		    		  quantity = String.valueOf(model.getQuantity());
		    	  }
		    	  form.getIbQuantity().setValue(quantity);
		    	  /** Ends **/
		    	  
		    	  form.getCbcommodity().setValue(false);
		    	  form.getStackUnderSheet().setValue(false);
		    	  form.getCbpacking().setValue(false);
		    	  form.getCbbothComAndPacking().setValue(false);
		    	  form.getPermantChamber().setValue(false);
		    	  form.getPressureTestedContaner().setValue(false);
		    	  form.getContainerUnderSheet().setValue(false);
		      }
		    };
		    t.schedule(5000);
		
	}
	
	
	private void reactOnReschedule() {
		reschedule.makeCustBranchListBox(model.getPersonInfo().getCount(), model.getCompanyId(),model.getServiceBranch());
		reschedule.getTime().setValue(model.getServiceTime());
		
		System.out.println("Service Date ::::::::: "+model.getServiceDate());
		Date date = CalendarUtil.copyDate(model.getServiceDate());
		System.out.println("Service Date1 ::::::::: "+date);
		
		/** date 16/10/2017 added by komal to show new service date as old service date **/
//		if(!model.getServiceDay().equals("")&&!model.getServiceDay().equals("Not Select")){
//			CalendarUtil.addDaysToDate(date,7);
//		    System.out.println("Changed Date + 7 ::::::::: "+date);
//		}
		
		reschedule.getDateBox().setValue(date);
		   model.setStatus(Service.SERVICESTATUSRESCHEDULE);
		   panel=new PopupPanel(true);
		   panel.add(reschedule);
		   reschedule.clear();
		   panel.show();
		   panel.center();
	}
		
	private void reactOnClose() {
		
		model.setStatus(Service.SERVICESTATUSCLOSED);
		manageProcessLevelBar("Marked Closed !","Failed To Mark Close !",Service.SERVICESTATUSCLOSED, 
				null, null,null,null);
	}
	
	private void reactOnCancel() {
		/**
		 * @author Anil
		 * @since 20-05-2020
		 * adding cancellation restriction for in house fumigation services
		 */
		if(model.isWmsServiceFlag()==true){
			form.showDialogMessage("Can not cancel In-House services!");
			return;
		}
		
		/**
		 * Date 16-4-2018
		 * By Jayshree tovalidate service cancelation
		 */
		if(model.getStatus().equalsIgnoreCase(Service.SERVICESUSPENDED)){
			form.showDialogMessage("Service can not be cancel");
		}
		else{
//		model.setStatus(Service.SERVICESTATUSCANCELLED);
//		panel=new PopupPanel(true);
//		panel.add(cancellation);
//		cancellation.clear();
//		panel.center();
//		panel.show();
		/**** Date 23-03-2019 by Vijay for cancellation popup ****/
		cancellation.showPopUp();
		
		}
	}
	
	
	private void reactOnComplete(Date completionDt,String formTime,String toTime) {
			model.setStatus(Service.SERVICESTATUSCOMPLETED);
			System.out.println("in side if");
			model.setServiceCompletionDate(completionDt);
			
		if(markcompletepopup.getRemark().getValue()!=null){
			model.setServiceCompleteRemark(markcompletepopup.getRemark().getValue());
		}
		
		if(markcompletepopup.getServiceTime().getValue()!=null){
			model.setServiceCompleteDuration(markcompletepopup.getServiceTime().getValue());
		}
		
		if(!formTime.equals("")){
			model.setFromTime(formTime);
		}

		if(!toTime.equals("") ){
			model.setToTime(toTime);
		}
		
		if(markcompletepopup.lbRating.getSelectedIndex()!=0){
			model.setCustomerFeedback(markcompletepopup.lbRating.getValue(markcompletepopup.lbRating.getSelectedIndex()));
		}
		/**
		 * nidhi
		 *  15-10-2018
		 *   ||*
		 */
//		if(model.getBillingCount())
		model.setBillingCount(Integer.parseInt(form.getTbBillingCount().getValue()));
		manageProcessLevelBar("Marked Completed !","Failed To Mark Complete !",Service.SERVICESTATUSCOMPLETED, 
				null, null,null,null);
		/**
		 * nidhi
		 * this methos commented and called after customer project complete valitation 
		 * 
		 */
//		sendSMSLogic();
		/**
		 * end
		 */
		form.getIbServiceDuration().setValue(model.getServiceCompleteDuration());
		form.getTbservicecompletionremark().setValue(model.getServiceCompleteRemark());
		form.getDbServiceCompletionDate().setValue(model.getServiceCompletionDate());
		
		if(!model.getServiceCompletionDate().equals(model.getServiceDate())){
			form.getDbServiceCompletionDate().addStyleName("red");
		}
		
		form.tbCustomerFeedback.setValue(model.getCustomerFeedback());
	}
	
	
	private void reactOnNew() {
		initalize();
	}
	
	
	private void createProjectCorrespondingToService(final String typeOfTransaction)
	{
		Console.log("Inside createProjectCorrespondingToService");
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","PrintServiceJobCardPdf")){
			custServRecPdf=true;
		}
		
		/**
		 * @author Anil
		 * @since 16-06-2020
		 * Removeing project validation for printing standard pdf
		 * for UMAS
		 */
		boolean defaultFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","RemoveProjectValidationForPrint");
		
		/**
		 * @author Anil
		 * @since 14-05-2020
		 * service pdf for premium tech
		 */
		if((AppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain","ComplainServiceWithTurnAroundTime")||defaultFlag)&&typeOfTransaction.trim().equals("Print")){
			Console.log("Inside Customer Service Record Pdf");
			final String url= GWT.getModuleBaseURL()+"pdfCustserjob"+"?Id="+model.getId()+"&"+"companyId="+model.getCompanyId();
			Window.open(url, "test", "enabled");
			return;
		}
            form.showWaitSymbol();
    		Timer timer=new Timer() 
        	 {
    				@Override
    				public void run() {
		
						projectAsync.createCustomerProject(model.getCompanyId(), model.getContractCount(), model.getCount(),model.getStatus(), new AsyncCallback<Integer>(){
				
							@Override
							public void onFailure(Throwable caught) {
								form.hideWaitSymbol();
								form.showDialogMessage("An Unexpected Error occured !");
							}
				
							@Override
							public void onSuccess(Integer result) {
								if(result==1){
									if(typeOfTransaction.trim().equals("Button"))
									{
										reactOnAddProject();
									}
									if(typeOfTransaction.trim().equals("Print"))
									{
										if (antipestFlag == true) {
											final String url1 = GWT.getModuleBaseURL()+ "pdfjobcerti"+ "?Id="+ model.getId();
											Window.open(url1, "test", "enabled");
											
										}
										else if(custServRecPdf) {
											Console.log("Inside custServRecPdf "+custServRecPdf);
//										final String url = GWT.getModuleBaseURL() + "customerserpdf"+"?Id="+model.getId();
//										Window.open(url, "test", "enabled");
										
										final String url = GWT.getModuleBaseURL()+"pdfserjob"+ "?Id="+ model.getId()+"&"+"companyId="+model.getCompanyId();
										Window.open(url, "test", "enabled");
										
										}else{
											Console.log("Inside Customer Service Record Pdf");
											final String url= GWT.getModuleBaseURL()+"pdfCustserjob"+"?Id="+model.getId()+"&"+"companyId="+model.getCompanyId();
											Window.open(url, "test", "enabled");
										}
									}
								}
							//  vijay addded this code
								if(result==-1){
									if(typeOfTransaction.trim().equals("Button"))
									{
										form.showDialogMessage("Project deos not exist for this Service Service already Completed!");

									}else{
										form.showDialogMessage("Project deos not exist for this Service Can Not print Job Card!");
									}
								}
								
								if(result==0){
									form.showDialogMessage("An unexpected error occurred. Please try again!");
								}
							}
						});
						form.hideWaitSymbol();
    				}
  			};
             timer.schedule(3000);
 		
	}
	
	private void reactOnAddProject()
	{
		  MyQuerry querry=new MyQuerry();
		  Vector<Filter> filtervec=new Vector<Filter>();
	      Filter filter=null;
	      filter=new Filter();
	      filter.setQuerryString("serviceId");
	      filter.setIntValue(model.getCount());
	      filtervec.add(filter);
	      
	      filter=new Filter();
	      filter.setQuerryString("companyId");
	      filter.setLongValue(model.getCompanyId());
	      filtervec.add(filter);
	      
	      querry.setFilters(filtervec);
	      querry.setQuerryObject(new ServiceProject());
	        
    	  ProjectForm form=ProjectPresenter.initalize(querry);
    	  form.setToNewState();
		  AppMemory.getAppMemory().stickPnel(form);
          
//          ServiceProject project=new ServiceProject();
//	      project.setserviceId(model.getCount());
//	      project.setserviceEngineer(model.getEmployee());
//	      project.setServiceStatus(model.getStatus());
//	      project.setserviceDate(model.getServiceDate());
//	      project.setContractId(model.getContractCount());
//	      project.setContractStartDate(model.getContractStartDate());
//	      project.setContractEndDate(model.getContractEndDate());
//	      project.setPersonInfo(model.getPersonInfo());
//	      project.setContractId(model.getContractCount());
//	      project.setAddr(model.getAddress());
//	      project.setProjectStatus(ServiceProject.SCHEDULED);
//	      form.updateView(project);
//	      form.serviceId.setEnabled(false);
	        
          filter=new Filter();
	        filter.setQuerryString("personInfo.count");
	        filter.setIntValue(model.getPersonInfo().getCount());
	        querry=new MyQuerry();
	        querry.setQuerryObject(new ClientSideAsset());
	        querry.getFilters().add(filter);
	        form.olbClientSideAsset.MakeLive(querry);
            
            if(model.getStatus().equals(Service.SERVICESTATUSCOMPLETED)
            		||model.getStatus().equals(Service.SERVICESTATUSCLOSED)
            		||model.getStatus().equals(Service.SERVICESTATUSCANCELLED))
            {
            	form.setToNewState();
            }
	}

	@Override
	public void reactOnPrint() {
		
		Console.log("Inside reactOnPrint");
		// ********************************************
		MyQuerry querry = new MyQuerry();
		Company c = new Company();

		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("processName");
		filter.setStringValue("Service");
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("processList.status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);

		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProcessConfiguration());

		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				Console.log("result set size +++++++"+ result.size());

				List<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();

				int antipest = 0;
				if (result.size() == 0) {
					//Ashwini Patil Date:23-07-2022 If there is no process config against service then noting was happening on print button click. Now added code to print default pdf
					Console.log("Inside result size zero");
					createProjectCorrespondingToService("Print");	
				} else {
					Console.log("in else");
					for (SuperModel model : result) {
						ProcessConfiguration processConfig = (ProcessConfiguration) model;
						processList.addAll(processConfig.getProcessList());

					}

					for (int k = 0; k < processList.size(); k++) {
						if (processList.get(k).getProcessType().trim().equalsIgnoreCase("JobCompletionCertificate")	&& processList.get(k).isStatus() == true) {
							antipest = antipest + 1;
						}

						if (antipest > 0) {
							antipestFlag = true;
						}
						System.out.println("Flag value++++++++++++ "+ antipestFlag);
						
						if(processList.get(k).getProcessType().trim().equalsIgnoreCase("OnlyForPecopp")&&processList.get(k).isStatus()==true){
							pecopp=pecopp+1;
						}
						
						if(processList.get(k).getProcessType().trim().equalsIgnoreCase(AppConstants.PC_SRFORMATVERSION1)&&processList.get(k).isStatus()==true){
							srcopyversion1=srcopyversion1+1;
						}
						
						if(processList.get(k).getProcessType().trim().equalsIgnoreCase("CompanyLetterHead")&&processList.get(k).isStatus()==true){
							count=count+1;
						}
					}
					
					if(count>0){
		                System.out.println("Inside print");
		                panelForpecoppSericeCertificate=new PopupPanel(true);
		                panelForpecoppSericeCertificate.add(conditionPopupForpecoppPrint);//conditionPopupForpecoppPrint
		                panelForpecoppSericeCertificate.setGlassEnabled(true);
		                panelForpecoppSericeCertificate.show();
		                panelForpecoppSericeCertificate.center();
		                return;
		                
					}else if(pecopp>0){
						Console.log("Inside Customer Service Record Pdf");
						final String url= GWT.getModuleBaseURL()+"pdfCustserjob"+"?Id="+model.getId()+"&"+"companyId="+model.getCompanyId()+"&"+"preprint="+"plane";
						//final String url = GWT.getModuleBaseURL()+"pdfCustserjob"+"?Id="+model.getId()+"&"+"preprint="+"plane";
						Window.open(url, "test", "enabled");
						return;
					}
					else if(srcopyversion1>0){
			                panelForpecoppSericeCertificate=new PopupPanel(true);
			                panelForpecoppSericeCertificate.add(conditionPopupForSRFormatversion1Print);//conditionPopupForpecoppPrint
			                panelForpecoppSericeCertificate.setGlassEnabled(true);
			                panelForpecoppSericeCertificate.show();
			                panelForpecoppSericeCertificate.center();
			                return;
					}
					/**
					 * Date 13 Feb 2017
					 * by Vijay
					 * Description :- if condition for nbhc service engineer is mondatory if we click on print then validation will
					 * display message if service engineer is blank in service
					 * else for all clients service engineer non mondatory
					 * @author Anil @since 28-09-2021
					 * Code was on wrong place 
					 * this was causing an server issue while printin SR copy
					 */
					else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","OnlyForNBHC")){
						if (form.tbServiceEngg.getSelectedIndex() == 0) {
							form.showDialogMessage("Please add Service engineer first!");
						} else {
							createProjectCorrespondingToService("Print");
						}
					}else{
						Console.log("Inside elseee");
						createProjectCorrespondingToService("Print");	
					}
					
					/**
					 * end here
					 */
					
					
				}
			}
		});
		
//		/**
//		 * Date 13 Feb 2017
//		 * by Vijay
//		 * Description :- if condition for nbhc service engineer is mondatory if we click on print then validation will
//		 * display message if service engineer is blank in service
//		 * else for all clients service engineer non mondatory
//		 */
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","OnlyForNBHC")){
//			if (form.tbServiceEngg.getSelectedIndex() == 0) {
//				form.showDialogMessage("Please add Service engineer first!");
//			} else {
//				createProjectCorrespondingToService("Print");
//			}
//		}else{
//			Console.log("Inside elseee");
//			createProjectCorrespondingToService("Print");	
//		}
//		
//		/**
//		 * end here
//		 */
	}

	@Override
	public void reactOnEmail() {
		Console.log("Inside reactOnEmail");
//		GeneralServiceAsync async=GWT.create(GeneralService.class);
//		List<Service> serviceList = new ArrayList<Service>();
//		serviceList.add(model);
//		
//		if(serviceList.size()==0){
//			form.showDialogMessage("Please select record!");
//			return;
//		}
//		ArrayList<Integer> serviceIdList = new ArrayList<Integer>();
//		for(Service ser : serviceList){
//			serviceIdList.add(ser.getCount());
//		}
//		
//		async.sendSrMail(UserConfiguration.getCompanyId(), serviceIdList, new AsyncCallback<String>() {
//
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				form.showDialogMessage("Failed");
//			}
//
//			@Override
//			public void onSuccess(String result) {
//				// TODO Auto-generated method stub
//				form.showDialogMessage("Email sent successfully.");
//			}
//		});
		
		
		/**
		 * @author Vijay Date 29-10-2021
		 * Des :- above old code commented 
		 * Added New Email Popup Functionality
		 */
		emailpopup.showPopUp();
		setEmailPopUpData();
		
		/**
		 * ends here
		 */
		
	
	}

	/**
	 * Method token to make new model
	 */
	@Override
	protected void makeNewModel() {
		model=new Service();
	}

	
	public static DeviceForm  initalize()
	{
		AppMemory.getAppMemory().currentState=ScreeenState.NEW;
		DeviceForm form=new  DeviceForm();
		ServiceTable gentable=new ServiceTable();
		gentable.setView(form);
		gentable.applySelectionModle();
		DevicePresenterSearch.staticSuperTable=gentable;
		ServiceSearchPopUp searchpopup=new ServiceSearchPopUp();
		form.setSearchpopupscreen(searchpopup);
		/**
		 * Date : 08-11-2017 BY ANIL
		 * This line was added to update the name of screen loaded from any other screen or by clicking on billing list on account model
		 */
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Service",Screen.DEVICE);
		/**
		 * END
		 */
		DevicePresenter  presenter=new DevicePresenter(form,new Service());
		AppMemory.getAppMemory().stickPnel(form);
		
		return form;
	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.Service")
	public static  class DevicePresenterSearch extends SearchPopUpScreen<Service>{

		@Override
		public MyQuerry getQuerry() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}};

		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.Service")
		public static class DevicePresenterTable extends SuperTable<Service> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				return null;
			}

			@Override
			public void createTable() {
			}

			@Override
			protected void initializekeyprovider() {
			}

			@Override
			public void addFieldUpdater() {
			}

			@Override
			public void setEnable(boolean state) {

			}

			@Override
			public void applyStyle() {
			}} ;

			 
			public void setContractDetails()
			{
				Integer contractId = Integer.parseInt(form.tbContractId.getValue());
			    if(contractId != null)
			    {
			    	MyQuerry querry = new MyQuerry();
			    	Vector<Filter> filtervec=new Vector<Filter>();
			        Filter temp = new Filter();
			        temp.setIntValue(contractId);
			        temp.setQuerryString("count");
			        filtervec.add(temp);
			        
			        temp = new Filter();
			        temp.setStringValue(Contract.APPROVED);
			        temp.setQuerryString("status");
			        filtervec.add(temp);
			        
			        querry.setFilters(filtervec);
			        querry.setQuerryObject(new Contract());
			       form.showWaitSymbol();
			        service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					 
			        	
			            public void onSuccess(ArrayList<SuperModel> result)
			            {
			                if(result.size()==0)
			                {
			                	form.showDialogMessage("No Approved Contract exists Corresponding to this ID !");
			                	form.tbContractId.setText("");
			                	form.poc.clear();
			                	
			                	return;
			                }
			            	Contract cont = (Contract)result.get(0);
			                if(cont.getStartDate() == null)
			                {
			                    form.showDialogMessage("The Contract does not have any service !");
			                    form.tbContractId.setText("");
			                    form.poc.clear();
			                    
			                	return;
			                } 
			                else
			                {
			                	lineItemLis=new ArrayList<SalesLineItem>();
			                    form.getPoc().setValue(cont.getCinfo());
			                    form.startDate.setValue(cont.getStartDate());
			                    form.endDate.setValue(cont.getEndDate());
			                    form.poc.makeMandatoryMessageFalse();
			                    form.tbBranch.setValue(cont.getBranch());
			                    if(cont.getEmployee()!=null)
			                    form.tbServiceEngg.setValue(cont.getEmployee());
			                    lineItemLis=cont.getItems();
			                    System.out.println("1111111111");
			                    retrieveCustomerAddress(cont.getCinfo().getCount());
			                    /** Date 14-01-2018 By vijay for Adhoc services Setting number Range **/
			                    if(cont.getNumberRange()!=null)
				                 form.numberRange = cont.getNumberRange();
			                }
			                Console.log("Ends here");
			                form.hideWaitSymbol();
			            }
			
			            public void onFailure(Throwable throwable)
			            {
			            	form.hideWaitSymbol();
			            	form.showDialogMessage("Failed to retreive Contract details !");
			            	throwable.printStackTrace();
			            }
			        });
			    }
			}

			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				setContractDetails();
				form.prodComposite.setEnabled(true);
				DeviceForm.prodflag=true;
			}
			
	@Override
	public void onClick(ClickEvent event)
	{
		super.onClick(event);
		
		//Ashwini Patil Date:8-07-2024 added to handle expense popup
		if (genralInvoicePopup != null) {
			if (event.getSource() == genralInvoicePopup.btnClose) {
				Console.log("Exoense form btn close");
				AppMemory.getAppMemory().currentScreen = Screen.DEVICE;				
				form.setToViewState();
				genralInvoicePopup.viewDocumentPanel.hide();
				form.toggleAppHeaderBarMenu();
				form.addClickEventOnActionAndNavigationMenus();
			}
		}
		
		if(event.getSource()==frmAndToPop.btnOne){
			updateTaxes();
			panel.hide();
		}
		
		if(event.getSource()==frmAndToPop.btnTwo){
			panel.hide();
		}
		
		/** date 06.04.2018 changed by komal for fumigation(MB)**/
		if(event.getSource().equals(conditionPopup.getBtnOne()))
		{
			System.out.println("in side one yes");
			reactOnFumigation();
			panel123.hide();
		}
		/** date 06.04.2018 changed by komal for fumigation(ALP)**/
		if(event.getSource().equals(conditionPopup.getBtnTwo()))
		{
			System.out.println("inside two no");
			reactOnFumigationALP();
			panel123.hide();
		}
		/** date 06.04.2018 added by komal for fumigation(AFAS) **/
		if(event.getSource().equals(conditionPopup.getBtnThree()))
		{
			System.out.println("inside two no");
			reactOnFumigationForAus();
			panel123.hide();
		}
		
		
		if(event.getSource()==reschedule.getBtnReschedule())
		{
			boolean val=validateReschedule();
			if(val==true)
			{
				ModificationHistory history=new ModificationHistory();
				history.oldServiceDate=model.getServiceDate();
				history.resheduleDate=reschedule.getDateBox().getValue();
				
				String serviceHrs=reschedule.getP_servicehours().getItemText(reschedule.getP_servicehours().getSelectedIndex());
				String serviceMin=reschedule.getP_servicemin().getItemText(reschedule.getP_servicemin().getSelectedIndex());
				String serviceAmPm=reschedule.getP_ampm().getItemText(reschedule.getP_ampm().getSelectedIndex());
				String calculatedServiceTime=serviceHrs+":"+serviceMin+""+serviceAmPm;
				
				/**Date 18-4-2020 by Amol if service time is not selected then it will Show Flexible**/
				if(serviceHrs.equals("--") || serviceMin.equals("--")){
					Console.log("Inside Service schedule time blank1111");
					history.resheduleTime="Flexible";
				}else{
					Console.log("Inside Service schedule time not blank111");
					history.resheduleTime=calculatedServiceTime;	
				}
				
				Console.log("Service schedule time value in device presenter"+history.resheduleTime);
				
				
				
//				history.resheduleTime=calculatedServiceTime;
				history.resheduleBranch=reschedule.getP_seviceBranch().getItemText(reschedule.getP_seviceBranch().getSelectedIndex());
				
				System.out.println("service branch == "+reschedule.getP_seviceBranch().getItemText(reschedule.getP_seviceBranch().getSelectedIndex()));
				
				history.reason=reschedule.getTextArea().getText();
				history.userName=LoginPresenter.loggedInUser;
				history.systemDate=new Date();
				Date reschDate=reschedule.getDateBox().getValue();
				model.setServiceTime(history.resheduleTime);
				model.getListHistory().add(history);
				model.setServiceDate(reschDate);
				model.setServiceScheduled(false); //added on 26-07-2024 
				
				
				String serviceBranch=reschedule.getP_seviceBranch().getItemText(reschedule.getP_seviceBranch().getSelectedIndex());
				model.setServiceBranch(serviceBranch);
				
				System.out.println("servic   === "+serviceBranch);
				/** Date :- 15-01-2020 by vijay for while rescheduling updating service Day ***/ 
				model.setServiceDay(ContractForm.serviceDay(reschDate));
				
				manageProcessLevelBar("Service Reschedule !","Failed To Reschedule !",Service.SERVICESTATUSRESCHEDULE, history,reschDate,model.getServiceTime(),serviceBranch);
			}
		}
		if(event.getSource()==reschedule.getBtnCancel()){
			panel.hide();
		}
		
		
		if(event.getSource()==cancellation.getLblOk())
		{
		
			/*** Date 23-03-2019 by Vijay with process config service cancellation remark drop down added ***/ 
			String cancellationReason="";
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableCancellationRemark")){
				if(cancellation.getOlbCancellationRemark().getSelectedIndex()==0){
					form.showDialogMessage("Cancellation Reason is Mandatory !");
					return;
				}
				cancellationReason = cancellation.getOlbCancellationRemark().getValue();
//			}
//			else{
//				cancellationReason =cancellation.getTextArea().getText().trim();
//			}
//
//			String cancellationReason=cancellation.getTextArea().getText().trim();
//			if(cancellationReason.equals("")==true)
//			{
//				form.showDialogMessage("Cancellation Reason is Mandatory !");
//				return ;
//			}
			/**
			 *  Date 23-03-2019 by Vijay
			 *  Des :- cancellation remark must store in remark field only and historical data who when cancelled must store in reason for change 
			 */  
			model.setRemark(cancellationReason);
			model.setStatus(Service.SERVICESTATUSCANCELLED);
			model.setReasonForChange("Service ID ="+model.getCount()+" "+"Service status ="+model.getStatus().trim()+"\n"
					+"has been cancelled by "+LoginPresenter.loggedInUser+" with remark "+"\n"
					+"Remark ="+cancellationReason+" "+"Cancellation Date ="+AppUtility.parseDate(new Date()));
			/** Date 27-07-2019 by Vijay Cancellation Date set seperately *****/
			model.setCancellationDate(new Date());
			manageProcessLevelBar("Service Cancelled!","Failed To Cancell !",Service.SERVICESTATUSCANCELLED, null,null,null,null);
			form.remark.setValue(cancellationReason);
			cancellation.hidePopUp();
			form.taReasonForChange.setText(cancellationReason);
		}
		
		if(event.getSource()==cancellation.getLblCancel())
		{
			cancellation.hidePopUp();
		}
		
		//Ashwini Patil Date:16-07-2022
		if(event.getSource()==markcompletepopup.getBtnOk()){
			
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC")){
					
						if(markcompletepopup.getServiceTime().getValue()!=null){
							if(markcompletepopup.getServiceTime().getValue()!=0){
								
							
								String formTime = markcompletepopup.getP_servicehours().getItemText(markcompletepopup.getP_servicehours().getSelectedIndex())+":"+
										markcompletepopup.getP_servicemin().getItemText(markcompletepopup.getP_servicemin().getSelectedIndex())+":"+
										markcompletepopup.getP_ampm().getItemText(markcompletepopup.getP_ampm().getSelectedIndex());
								
								System.out.println("formTime form popup "+formTime);
								
								String toTime = markcompletepopup.getTo_servicehours().getItemText(markcompletepopup.getTo_servicehours().getSelectedIndex())+":"+
										markcompletepopup.getTo_servicemin().getItemText(markcompletepopup.getTo_servicemin().getSelectedIndex())+":"+
										markcompletepopup.getTo_ampm().getItemText(markcompletepopup.getTo_ampm().getSelectedIndex());
								System.out.println("toTime form popup "+toTime);
								
							if(markcompletepopup.getRemark().getValue()!=null){
								form.setF_markRemark(markcompletepopup.getRemark().getValue());
							}else{
								form.setF_markRemark(null);
							}
							
							if(markcompletepopup.getServiceTime().getValue()!=null){
								form.setF_markduration(markcompletepopup.getServiceTime().getValue());
							}else{
								form.setF_markduration(0);
							}
							
							if(markcompletepopup.getServiceCompletionDate().getValue()!=null){
								form.getDbServiceCompletionDate().setValue(markcompletepopup.getServiceCompletionDate().getValue());
							}
							
							if(markcompletepopup.lbRating.getSelectedIndex()!=0){
								form.tbCustomerFeedback.setValue(markcompletepopup.lbRating.getValue(markcompletepopup.lbRating.getSelectedIndex()));
							}else{
								form.tbCustomerFeedback.setValue(null);
							}
							
							/**
							 * Date 23-11-2019 by Vijay
							 *  Des :- This code not required as per new Fumigation requirement
							 */
//							/**
//							 * Added By Rahul Verma
//							 * This method is only for nbhc. and it will check whether there stock current status is>0 and they have service or not.
//							 * 
//							 */
//							if(model.getEmployee().trim().equalsIgnoreCase("Snehal Abhishek Palav") && model.getStackDetailsList().size()!=0){
//								createExtraServiceIfStackZero(model);
//							}
//							/**
//							 * ends here 
//							 */
							
							/**
							 * nidhi
							 *  15-10-2018
							 *   ||*
							 */
							boolean validateFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceBillingDetailMapingProcess");
							boolean validateBill = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceBillingDetailMapingMandetory");
							
							
							if(validateFlag  && !model.isServiceWiseBilling() && markcompletepopup.getBillingIdList().getSelectedIndex()!=0){
								form.getTbBillingCount().setValue(markcompletepopup.getBillingIdList().getValue(markcompletepopup.getBillingIdList().getSelectedIndex()));
							}else if(validateFlag && validateBill && !model.isServiceWiseBilling()){
								form.showDialogMessage("Please select billing id");
								return;
							}
							
							reactOnComplete(form.getDbServiceCompletionDate().getValue(),formTime,toTime);
							markpanel.hide();
							
							// ********vaishnavi****************
							createProjectonMarkComplete();
							
							//Ashwini Patil Date:22-07-2022
							CommonServiceAsync commonSer = GWT.create(CommonService.class);
							String branchEmail="";
							commonSer.callSRCopyEmailTaskQueue(model.getCompanyId(), model, branchEmail,new AsyncCallback<Void>() {

								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									Console.log("Error : "+ caught);
								}

								@Override
								public void onSuccess(Void result) {
									// TODO Auto-generated method stub
									Console.log("SR Email sending process started");
								}
							});
							
							
							
							
						}else{
							form.showDialogMessage("Please add Service Duration (In Minutes)");
						}
				}else{
					form.showDialogMessage("Service Duration (In Minutes) is mandetory..!");
				}
		}
		else
		{
			
			
			if(markcompletepopup.getP_ampm().getSelectedIndex()==0){
				form.showDialogMessage("Please select From AM/PM !");
				return;
			}
			
			if(markcompletepopup.getTo_ampm().getSelectedIndex()==0){
				form.showDialogMessage("Please select To AM/PM !");
				return;
			}
			
					String formTime = markcompletepopup.getP_servicehours().getItemText(markcompletepopup.getP_servicehours().getSelectedIndex())+":"+
							markcompletepopup.getP_servicemin().getItemText(markcompletepopup.getP_servicemin().getSelectedIndex())+":"+
							markcompletepopup.getP_ampm().getItemText(markcompletepopup.getP_ampm().getSelectedIndex());
					
					System.out.println("formTime form popup "+formTime);
					
					String toTime = markcompletepopup.getTo_servicehours().getItemText(markcompletepopup.getTo_servicehours().getSelectedIndex())+":"+
							markcompletepopup.getTo_servicemin().getItemText(markcompletepopup.getTo_servicemin().getSelectedIndex())+":"+
							markcompletepopup.getTo_ampm().getItemText(markcompletepopup.getTo_ampm().getSelectedIndex());
					System.out.println("toTime form popup "+toTime);
					
				if(markcompletepopup.getRemark().getValue()!=null){
					form.setF_markRemark(markcompletepopup.getRemark().getValue());
				}else{
					form.setF_markRemark(null);
				}
				
				if(markcompletepopup.getServiceTime().getValue()!=null){
					form.setF_markduration(markcompletepopup.getServiceTime().getValue());
				}else{
					form.setF_markduration(0);
				}
				
				if(markcompletepopup.getServiceCompletionDate().getValue()!=null){
					form.getDbServiceCompletionDate().setValue(markcompletepopup.getServiceCompletionDate().getValue());
				}
				
				if(markcompletepopup.lbRating.getSelectedIndex()!=0){
					form.tbCustomerFeedback.setValue(markcompletepopup.lbRating.getValue(markcompletepopup.lbRating.getSelectedIndex()));
				}else{
					form.tbCustomerFeedback.setValue(null);
				}
				
				/**
				 * Date 23-11-2019 by Vijay
				 *  Des :- This code not required as per new Fumigation requirement
				 */
//				/**
//				 * Added By Rahul Verma
//				 * This method is only for nbhc. and it will check whether there stock current status is>0 and they have service or not.
//				 * 
//				 */
//				if(model.getEmployee().trim().equalsIgnoreCase("Snehal Abhishek Palav") && model.getStackDetailsList().size()!=0){
//					createExtraServiceIfStackZero(model);
//				}
//				/**
//				 * ends here 
//				 */
				/**
				 * nidhi
				 *  15-10-2018
				 *   ||*
				 */
				boolean validateFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceBillingDetailMapingProcess");
				boolean validateBill = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceBillingDetailMapingMandetory");
				
				
				if(validateFlag  && !model.isServiceWiseBilling() && markcompletepopup.getBillingIdList().getSelectedIndex()!=0){
					form.getTbBillingCount().setValue(markcompletepopup.getBillingIdList().getValue(markcompletepopup.getBillingIdList().getSelectedIndex()));
				}else if(validateFlag && validateBill && !model.isServiceWiseBilling()){
					form.showDialogMessage("Please select billing id");
					return;
				}
				
				/**
				 * @author Anil , Date : 19-09-2019
				 * For Hvac complain service ,at the time of completion given user option to make that service billable
				 * raised by Rohan
				 */
				if(model.getTicketNumber()!=0&&model.getTicketNumber()!=-1){
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MakeComplainServiceBillable")){
						if(!formTime.equals("")){
							model.setFromTime(formTime);
						}
						if(!toTime.equals("") ){
							model.setToTime(toTime);
						}
						markpanel.hide();
						panel=new PopupPanel(true);
						panel.add(complainBillPopup);
						panel.show();
						panel.center();
						return;
					}
				}
				reactOnComplete(form.getDbServiceCompletionDate().getValue(),formTime,toTime);
				markpanel.hide();
				
				// ********vaishnavi****************
				createProjectonMarkComplete();
				
				//Ashwini Patil Date:22-07-2022
				CommonServiceAsync commonSer = GWT.create(CommonService.class);
				String branchEmail="";
				commonSer.callSRCopyEmailTaskQueue(model.getCompanyId(), model, branchEmail,new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						Console.log("Error : "+ caught);
					}

					@Override
					public void onSuccess(Void result) {
						// TODO Auto-generated method stub
						Console.log("SR Email sending process started");
					}
				});
		}
			
			
			
	}
		if(event.getSource()==markcompletepopup.getBtnCancel()){
			markpanel.hide();
		}
		
		if (event.getSource() == datebox.getBtnOne()) {
			if (datebox.getServiceDate().getValue() != null) {
				
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "CompanyAsLetterHead"))
			{
				panelForSericeCertificate = new PopupPanel(true);
				panelForSericeCertificate.add(conditionPopupForPrint);
				panelForSericeCertificate.show();
				panelForSericeCertificate.center();
				Panel.show();
			}	
			else
			{
				String serviceDate = datebox.getServiceDate().getValue()+"$";
				System.out.println("Date :::::::: " + serviceDate);
				
				final String url = GWT.getModuleBaseURL() +"Service"+"?Id="+model.getId()+"&"+"date="+serviceDate+"&"+"preprint="+"plane";
				Window.open(url, "test", "enabled");
			
				Panel.show();
			}
				
			} 
			else
			{
				form.showDialogMessage("Please select date!");
			}
		}
		
		if (event.getSource() == datebox.getBtnTwo()) {
			com.slicktechnologies.client.utils.Console.log("in side button two when cancel");
			Panel.hide();
		}
		
		
		/*
		 * @author Ashwini
		 * Date:24-01-2018
		 */
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC")){
			
			if(event.getSource().equals(conditionPopupForWPMPrint.getBtnOne())){
				final String url = GWT.getModuleBaseURL() + "fumigationCefrtificate"+"?Id="+model.getId()+"&preprint=yes";
				Window.open(url, "test", "enabled");
				panelForWPM.hide();
			}
			
			if(event.getSource().equals(conditionPopupForWPMPrint.getBtnTwo())){
				final String url = GWT.getModuleBaseURL() + "fumigationCefrtificate"+"?Id="+model.getId()+"&preprint=no";
				Window.open(url, "test", "enabled");
				panelForWPM.hide();
			}
			
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC")){
			if(event.getSource().equals(popupForCommodityFum.getBtnOne())){
				final String url = GWT.getModuleBaseURL() + "commodityFumigationReport"+"?Id="+model.getId()+"&preprint="+"yes";
				Window.open(url, "test", "enabled");
				panelForCommodityfm.hide();
			}
			
			if(event.getSource().equals(popupForCommodityFum.getBtnTwo())){
				
				final String url = GWT.getModuleBaseURL() + "commodityFumigationReport"+"?Id="+model.getId()+"&preprint="+"no";
				Window.open(url, "test", "enabled");
				panelForCommodityfm.hide();
			}
		}
		
		/*
		 * end by Ashwini
		 */
		
		//   rohan added this code for preprint 
		if (event.getSource() == conditionPopupForPrint.getBtnOne()) {
			
			String serviceDate = datebox.getServiceDate().getValue()+"$";
			System.out.println("Date :::::::: " + serviceDate);
			com.slicktechnologies.client.utils.Console.log("in side button one when cancel"+serviceDate);
			/**
			 * Date 15-09-2018 by vijay
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC")){
				final String url = GWT.getModuleBaseURL() + "serviceRecord"+"?Id="+model.getId()+"&"+"preprint="+"yes";
				Window.open(url, "test", "enabled");
				panelForSericeCertificate.hide();
			}else{
				final String url = GWT.getModuleBaseURL() +"Service"+"?Id="+model.getId()+"&"+"date="+serviceDate+"&"+"preprint="+"yes";
				Window.open(url, "test", "enabled");
				panelForSericeCertificate.hide();
			}
			
			
		}
		
		
		if (event.getSource() == conditionPopupForPrint.getBtnTwo()) {
			String serviceDate = datebox.getServiceDate().getValue()+"$";
			System.out.println("Date :::::::: " + serviceDate);
			com.slicktechnologies.client.utils.Console.log("in side button one when cancel"+serviceDate);
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC")){
				final String url = GWT.getModuleBaseURL() + "serviceRecord"+"?Id="+model.getId()+"&"+"preprint="+"no"+"&"+
			"SRNumber="+model.getSrCopyNumber()+"&"+"companyId="+UserConfiguration.getCompanyId();
				Window.open(url, "test", "enabled");
				panelForSericeCertificate.hide();
			}else{
				final String url = GWT.getModuleBaseURL() +"Service"+"?Id="+model.getId()+"&"+"date="+serviceDate+"&"+"preprint="+"no";
				Window.open(url, "test", "enabled");
				panelForSericeCertificate.hide();
			}
			
		}
		
		
		if (event.getSource() ==form.catchAddButton) {
			System.out.println("in side onclick ");
			boolean flag = false;
			if(model.getProduct() != null && model.getProduct().getProductCode()!= null){
				if(model.getFumigationProductCodes().contains(model.getProduct().getProductCode())){
					flag = true;
				 }
				}
			if(!flag && form.getPestName().getSelectedIndex()!=0 && form.getPestCount()!=null){
					addinformationTotable();
			}else if(flag && form.tbCommodity.getValue() != null){
				addinformationTotable();
			}
			else
			{
				form.showDialogMessage("Add Required information");
			}
			
		}
		
//		//Added by Rahul
//		if(event.getSource()==form.btstackDetails){
//			reactofstackDetailsPopUp();
//			stackDetailsPopUp.getStackDetailsTable().setEnable(false);
//			stackDetailsPanel = new PopupPanel(true);
//			stackDetailsPanel.add(stackDetailsPopUp);
//			stackDetailsPanel.show();
//			stackDetailsPanel.center();
//		}
//		
//		if(event.getSource()==stackDetailsPopUp.getBtnOk()){
//			stackDetailsPanel.hide();
//		}
//		
//		if(event.getSource()==stackDetailsPopUp.getBtnCancel()){
//			stackDetailsPanel.hide();
//		}
		
		/*** Date 26-04-2019 by Vijay for NBHC CCPM updated with new popup ****/
		if(event.getSource()==form.btstackDetails){
			reactOnStackDetails();
		}
//		if(event.getSource()==stackDetailsPopUp.getLblOk()){
//			List<StackDetails> stackdetailslist = stackDetailsPopUp.getStackDetailsTable().getDataprovider().getList();
//			ArrayList<StackDetails> selectedDtackdetailslist = getSelectedRecord(stackdetailslist);
//			if(selectedDtackdetailslist.size()!=0){
//				model.setStackDetailsList(selectedDtackdetailslist);
//				stackDetailsPopUp.hidePopUp();
//			}
//			else{
//				form.showDialogMessage("Please select stack for the service!");
//			}
//			
//		}
		
		if(event.getSource()==complainBillPopup.getBtnOne()){
			panel.hide();
			model.setServiceWiseBilling(true);
			reactOnComplete(form.getDbServiceCompletionDate().getValue(),"","");
//			markpanel.hide();
			createProjectonMarkComplete();
			
		}
		
		if(event.getSource()==complainBillPopup.getBtnTwo()){
			panel.hide();
			reactOnComplete(form.getDbServiceCompletionDate().getValue(),"","");
			createProjectonMarkComplete();
		}
		if(event.getSource()==complainBillPopup.getBtnThree()){
			panel.hide();
		}
		
		/**
		 * @author Anil @since 27-09-2021
		 * SR copy for pecopp was not printing on first click
		 * issue raised by Ashwini for pecopp
		 */
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForPecopp")){
			
			if(event.getSource().equals(conditionPopupForpecoppPrint.getBtnOne())){
				if(count>0&&AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForPecopp")){
					//final String url= GWT.getModuleBaseURL()+"pdfCustserjob"+"?Id="+model.getId()+"&"+"companyId="+model.getCompanyId()+"&"+"SRNumber="+model.getSrCopyNumber()+"&"+"preprint="+"yes";
					final String url = GWT.getModuleBaseURL()+"pdfCustserjob"+"?Id="+model.getId()+"&"+"companyId="+model.getCompanyId()+"&"+"preprint="+"yes";
					//final String url = GWT.getModuleBaseURL()+"pdfCustserjob"+"?Id="+model.getId()+"&"+"preprint="+"yes";
					Window.open(url, "test", "enabled");
					panelForpecoppSericeCertificate.hide();
				}
			}
			
			
			
			if(event.getSource().equals(conditionPopupForpecoppPrint.getBtnTwo())){
				if(count>0&&AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForPecopp")){
					//final String url= GWT.getModuleBaseURL()+"pdfCustserjob"+"?Id="+model.getId()+"&"+"companyId="+model.getCompanyId()+"&"+"SRNumber="+model.getSrCopyNumber()+"&"+"preprint="+"no";
					final String url = GWT.getModuleBaseURL()+"pdfCustserjob"+"?Id="+model.getId()+"&"+"companyId="+model.getCompanyId()+"&"+"preprint="+"no";
					//final String url = GWT.getModuleBaseURL()+"pdfCustserjob"+"?Id="+model.getId()+"&"+"preprint="+"no";
					Window.open(url, "test", "enabled");
					panelForpecoppSericeCertificate.hide();
				}
			}
			
//		}
		
			if(event.getSource().equals(conditionPopupForSRFormatversion1Print.getBtnOne())){
				if(srcopyversion1>0){
					final String url = GWT.getModuleBaseURL()+"pdfCustserjob"+"?Id="+model.getId()+"&"+"companyId="+model.getCompanyId()+"&"+"preprint="+"yes";
					Window.open(url, "test", "enabled");
					panelForpecoppSericeCertificate.hide();
				}
			}
			if(event.getSource().equals(conditionPopupForSRFormatversion1Print.getBtnTwo())){
				if(srcopyversion1>0){
					final String url = GWT.getModuleBaseURL()+"pdfCustserjob"+"?Id="+model.getId()+"&"+"companyId="+model.getCompanyId()+"&"+"preprint="+"no";
					Window.open(url, "test", "enabled");
					panelForpecoppSericeCertificate.hide();
				}
			}

			
			if(event.getSource() == CommunicationLogPopUp.getBtnOk()){
				form.showWaitSymbol();
			    List<InteractionType> list = CommunicationLogPopUp.getCommunicationLogTable().getDataprovider().getList();
			    ArrayList<InteractionType> interactionlist = new ArrayList<InteractionType>();
			    interactionlist.addAll(list);
			    boolean checkNewInteraction = AppUtility.checkNewInteractionAdded(interactionlist);
			    if(checkNewInteraction==false){
			    	form.showDialogMessage("Please add new interaction details");
			    	form.hideWaitSymbol();
			    }else{	
//			    	final Date followUpDate = interactionlist.get(interactionlist.size()-1).getInteractionDueDate();
			    	 communicationService.saveCommunicationLog(interactionlist, new AsyncCallback<Void>() {
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("Unexpected Error");
							form.hideWaitSymbol();
							LoginPresenter.communicationLogPanel.hide();
						}

						@Override
						public void onSuccess(Void result) {
							form.showDialogMessage("Data Save Successfully");
							form.hideWaitSymbol();
//							model.setFollowUpDate(followUpDate);
//							form.getDbleaddate().setValue(followUpDate);
							LoginPresenter.communicationLogPanel.hide();
						}
					});
			    }
			}
			if(event.getSource() == CommunicationLogPopUp.getBtnCancel()){
				LoginPresenter.communicationLogPanel.hide();
			}
			if(event.getSource() == CommunicationLogPopUp.getBtnAdd()){
					form.showWaitSymbol();
					String remark = CommunicationLogPopUp.getRemark().getValue();
					Date dueDate = CommunicationLogPopUp.getDueDate().getValue();
					String interactiongGroup =null;
					if(CommunicationLogPopUp.getOblinteractionGroup().getSelectedIndex()!=0)
						interactiongGroup=CommunicationLogPopUp.getOblinteractionGroup().getValue(CommunicationLogPopUp.getOblinteractionGroup().getSelectedIndex());
					boolean validationFlag = AppUtility.validateCommunicationlog(remark,dueDate);
					if(validationFlag){
						InteractionType communicationLog =  AppUtility.getCommunicationLog(AppConstants.SERVICEMODULE,AppConstants.CUSTOMERSERVICE,model.getCount(),model.getEmployee(),remark,dueDate,model.getPersonInfo(),null,interactiongGroup, model.getBranch(),model.getStatus());
						CommunicationLogPopUp.getCommunicationLogTable().getDataprovider().getList().add(communicationLog);
						CommunicationLogPopUp.getRemark().setValue("");
						CommunicationLogPopUp.getDueDate().setValue(null);
						CommunicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
					}
					form.hideWaitSymbol();
			}
			
			if(event.getSource() == otpPopup.getBtnOne()){
				Console.log("otp popup ok button clicked");
				Console.log("otp value="+otpPopup.getOtp().getValue());
				if(otpPopup.getOtp().getValue()!=null){
					if(generatedOTP==otpPopup.getOtp().getValue()){
						runRegularDownloadProgram();
						otppopupPanel.hide();
					}else {
						form.showDialogMessage("Invalid OTP");	
						smsService.checkConfigAndSendWrongOTPAttemptMessage(model.getCompanyId(), LoginPresenter.loggedInUser, generatedOTP, "Customer Service", new  AsyncCallback<String>() {

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								form.showDialogMessage("Failed");
							}

							@Override
							public void onSuccess(String result) {
								// TODO Auto-generated method stub
								Console.log("InvalidOTPDownloadNotification result="+result);
							}
						} );
					
					}
				}else
					form.showDialogMessage("Enter OTP");
			}
			if(event.getSource() == otpPopup.getBtnTwo()){
				otppopupPanel.hide();
			}
			
			
			
			if(event.getSource()==suspendservicepopup.getLblOk())
			{
			
				String cancellationReason="";
				if(suspendservicepopup.getOlbCancellationRemark().getSelectedIndex()==0){
					form.showDialogMessage("Cancellation Reason is Mandatory !");
					return;
				}
				cancellationReason = suspendservicepopup.getOlbCancellationRemark().getValue();
				model.setRemark(cancellationReason);
				form.remark.setValue(cancellationReason);
				model.setStatus(Service.SERVICESUSPENDED);
				manageProcessLevelBar("Service Suspened!","Failed To Suspend !",Service.SERVICESUSPENDED, null,null,null,null);
				suspendservicepopup.hidePopUp();
			}
			
			if(event.getSource()==suspendservicepopup.getLblCancel())
			{
				suspendservicepopup.hidePopUp();
			}
			
			
			
	}
	
	
	
	public ArrayList<StackDetails> getSelectedRecord(List<StackDetails> stackdetailslist) {
		ArrayList<StackDetails> arrstackDetails = new ArrayList<StackDetails>();
		for(StackDetails stackdetails : stackdetailslist){
//			if(stackdetails.isRecordSelect()){
//				arrstackDetails.add(stackdetails);
//			}
		}
		return arrstackDetails;
	}

	private void createProjectonMarkComplete() {
		Timer timer = new Timer() {
			@Override
			public void run() {

				projectAsync.createCustomerProject(model.getCompanyId(),
						model.getContractCount(), model.getCount(),model.getStatus(),
						new AsyncCallback<Integer>() {

							@Override
							public void onFailure(Throwable caught) {
								form.hideWaitSymbol();
								form.showDialogMessage("An Unexpected Error occured !");
							}

							@Override
							public void onSuccess(Integer result) {
								System.out.println("project created .......!!");
							}
						});
			}
		};
		timer.schedule(3000);

	}
	
	
	private void addinformationTotable() {
		
		if(validate()){
			boolean flag = false;
			if(model.getProduct() != null && model.getProduct().getProductCode()!= null){
				if(model.getFumigationProductCodes().contains(model.getProduct().getProductCode())){
					flag = true;
				 }
				}
		CatchTraps traps = new CatchTraps();
		if(flag){
			traps.setContainerNo(form.tbContainerNumber.getValue()+"");
		}else{
			traps.setCount(form.getPestCount().getValue());
		}
		if(flag){
			traps.setPestName(form.tbCommodity.getValue());
		}else{
			traps.setPestName(form.getPestName().getValue(form.getPestName().getSelectedIndex()).trim());
		}
		
		/** date 26.3.2019 added by komal to save location **/
		if(form.tbLocation.getValue() != null){
		if(flag){
			traps.setContainerSize(form.tbLocation.getValue());
		}else{
			traps.setLocation(form.tbLocation.getValue());
		}
		}
		form.catchTrap.getDataprovider().getList().add(traps);
		}
		else
		{
			form.showDialogMessage("Can't add duplicate name ...!!!");
		}
	}

	private boolean validate(){
		
		if(form.catchTrap.getDataprovider().getList().size()!=0){
			for(int i=0;i<form.catchTrap.getDataprovider().getList().size();i++){
				if(form.catchTrap.getDataprovider().getList().get(i).equals(form.getPestName().getValue())){
					return false;
				}
			}
		}
		
		return true;
	}

	@Override
	public void reactOnDownload() {
		if(view.getSearchpopupscreen().getSupertable().getListDataProvider().getList()==null||view.getSearchpopupscreen().getSupertable().getListDataProvider().getList().size()==0)
		{
			form.showDialogMessage("No records found for download!");
			return;
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "RestrictServiceDownloadWithOTP")){
			generatedOTP=(int) Math.floor(100000 + Math.random() * 900000);
			Console.log("generatedOTP="+generatedOTP);
			smsService.checkConfigAndSendServiceDownloadOTPMessage(model.getCompanyId(), LoginPresenter.loggedInUser, generatedOTP, new  AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.showDialogMessage("Failed");
				}

				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					Console.log("in success");
					if(result!=null&&result.equalsIgnoreCase("success")) {						
						otpPopup.clearOtp();
						otppopupPanel = new PopupPanel(true);
						otppopupPanel.add(otpPopup);
						otppopupPanel.show();
						otppopupPanel.center();
					}else {
						form.showDialogMessage(result);
					}
						
				}
			} );
		
		}else
			runRegularDownloadProgram();
		
		
	}
	
	public void manageProcessLevelBar(final String sucesssMessage,
			final String failureMessage,
			final String statusText,final ModificationHistory history,
			final Date ServiceDate,final String serviceTime,final String serviceBranch)
	{
		form.showWaitSymbol();
		asyncService.saveStatus(model,new AsyncCallback<Boolean>() {
        
			@Override
			public void onFailure(Throwable caught) {
				
				form.showDialogMessage(failureMessage);
				form.hideWaitSymbol();
				caught.printStackTrace();
				
			}

			@Override
			public void onSuccess(Boolean result) {
				if(result==false)
				{
					form.showDialogMessage("Service Cannot be marked Completed as there are "
							+ "Scheduled Projects");
					form.hideWaitSymbol();
					return;
				}
				form.showDialogMessage(sucesssMessage);
				/**
				 *  nidhi
				 *  this method set for send method after all validation check and service complition
				 */
				if(statusText.equals(Service.SERVICESTATUSCOMPLETED)){
				sendSMSLogic();
				}
				/**
				 * end
				 */
				form.tbStatus.setText(statusText);
				if(ServiceDate!=null)
				form.tbServiceDate.setValue(ServiceDate);
				if(serviceTime!=null)
					form.tbServicetime.setValue(serviceTime);
				
				if(serviceBranch!=null)
					form.tbserviceBranch.setValue(serviceBranch);
				
				if(history!=null)
				{
					System.out.println("Size of Data Profider is "+form.table.getDataprovider().getList().size());
					form.table.getDataprovider().getList().add(history);
				   System.out.println("Size of Data Profider is "+form.table.getDataprovider().getList().size());
			   
				}
			  /*** Date 15-01-2020 by vijay for rescheduling case updating service Day ***/	
			 if(statusText.equals(Service.SERVICESTATUSRESCHEDULE)){	
				 if(ServiceDate!=null){
				 form.tbServiceday.setValue(ContractForm.serviceDay(ServiceDate));
				 }
			  }
			 panel.hide();
//			 form.setMenuAsPerStatus();
			 form.setToViewState(); // Date 21-07-2023 added by vijay as status is not updated on customer service screen
			 form.hideWaitSymbol();
			}
		});
	}
	
	public boolean validateReschedule()
	{
		Date rescheduleDate = reschedule.getDateBox().getValue();
		Date contractEndDate = model.getContractEndDate();
		Date contractStartDate = model.getContractStartDate();
//		Date oldServiceDate=model.getServiceDate();
		String reasonForChange=reschedule.getTextArea().getText().trim();
		
		/**Date 24-10-2020 by Amol**/
		if(reschedule.getP_ampm().getSelectedIndex()==0){
			form.showDialogMessage("Please select AM/PM  !");
			return false;
		}
		
		if(rescheduleDate==null)
		{
			form.showDialogMessage("Reschedule Date Cannot be empty !");
			return false;
		}
		
		/**
		 * nidhi
		 * 21-06-2018
		 * for reschedule service for same month validate
		 * Date :- 23-07-2018 By Vijay as discussed with Vaishali mam this validation not for Admin level
		 */
			boolean reScheduleValide  = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceRescheduleWithInMonthOnly");
			
			if(reScheduleValide && !LoginPresenter.myUserEntity.getRole().getRoleName().equals("ADMIN")){
					Date serDate = reschedule.getDateBox().getValue();
					
					Date preFirstDate = CalendarUtil.copyDate(form.getTbServiceDate().getValue());
					Date preLastDate = CalendarUtil.copyDate(form.getTbServiceDate().getValue());
					
					CalendarUtil.addMonthsToDate(preLastDate, 1);
					
					preLastDate.setDate(1);
					CalendarUtil.addDaysToDate(preLastDate, -1);
					
					/**
					 * Date 10-Aug-2018 By Vijay
					 * Des :- Reschedule date only allow service date month only.
					 */
					Date firstDateServiceDate = CalendarUtil.copyDate(form.getTbServiceDate().getValue());
					CalendarUtil.setToFirstDayOfMonth(firstDateServiceDate);
					
					if(serDate.after(preLastDate) || serDate.before(firstDateServiceDate) ){
						form.showDialogMessage("Service date can be Reschedule With in Service date's month only.");
						return false;
					}
					
//					if(serDate.before(form.getTbServiceDate().getValue())){
//						form.showDialogMessage("Reschedule service date should be after current service date.");
//						return false;
//					}
					
					/**
					 * ends here and above old code commented because reschedule date allow for the same month date. 
					 */
					
			}
		
		/**
		 * rohan added this code to restrict service date in between contract period only contract end date
		 * Date : 18 /1/2017
		 */
		/**
		 *  nidhi
		 *  2-10-2017
		 *  commented condition for resuchedule date even after contract end date 
		 */
//		int flag =rescheduleDate.compareTo(contractEndDate);
//		if(flag > 0)
//		{
//			form.showDialogMessage("Reschedule date should not be greater than contract period..!");
//			return false;
//		}
		/**
		 * end
		 */
		
////		if(rescheduleDate.before(contractStartDate))
//		int cnt =rescheduleDate.compareTo(contractStartDate);
//		
//		/***
//		 * Date 24-4-2018
//		 * By Jayshree
//		 * des.to alloved the service reschedule at the contract start date
//		 */
//		if(cnt < 0)
//		{
////			form.showDialogMessage("Reschedule date should be after contract start date..!");
//			form.showDialogMessage("Reschedule date should not before the contract start date!");
//			return false;
//		}
//		
//		/**
//		 * ends here 
//		 */
		/**
		 * @author Vijay Date :- 26-11-2021
		 * Des :- Bug validation not allowing reschedule on contract start date. it should not before contract start date
		 */
		if(AppUtility.formatDate(AppUtility.parseDate(rescheduleDate)).before(AppUtility.formatDate(AppUtility.parseDate(contractStartDate)))){
			form.showDialogMessage("Reschedule date should be after contract start date..!");
			return false;
		}
		
		/**
		 * ends here 
		 */
		
		
/**   rohan coomented this code for NBHC on Date : 15/11/2016 
 * 
 * Reason : This is used to  reschedule date of service as per need
 */
		
//		String currDateString=AppUtility.parseDate(new Date());
//		DateTimeFormat format=DateTimeFormat.getFormat("dd/MM/yyyy");
//		Date currDate=format.parse(currDateString);
//		if(rescheduleDate.before(currDate))
//		{
//			form.showDialogMessage("Reschedule Date Should be greater or equal to current  "
//					+ "Date !");
//			return false;
//		}
		/*if(rescheduleDate.before(oldServiceDate)||rescheduleDate.equals(oldServiceDate))
		{
			form.showDialogMessage("Reschedule Date Should be greater then "
					+ "Service Date !");
			return false;
		}*/
		
		if(reasonForChange.equals(""))
		{
			form.showDialogMessage("Reason For Change Cannot be Empty !");
			return false;
		}
		
		/**
		 * @author Vijay Chougule
		 * Project :- Fumigation Tracker
		 * Des :- when CIO Reschedule the stack fumigation service then In EVA ERP System it can not allow to
		 * reschedule CIO Reschedule Date + 5 Days. 
		 */
		if(model.isWmsServiceFlag() && (model.getProduct().getProductCode().trim().equals("STK-01") || model.getProduct().getProductCode().trim().equals("PHM-01"))
				&& model.getCioRescheduleDate()!=null){
			if(!LoginPresenter.myUserEntity.getRole().getRoleName().equals("ADMIN")){
				Date newrescheduleDate = reschedule.getDateBox().getValue();
				Date cioRescheduleDate = model.getCioRescheduleDate();
				CalendarUtil.addDaysToDate(cioRescheduleDate, 5);
				Console.log("cioRescheduleDate"+cioRescheduleDate);
				if(newrescheduleDate.after(cioRescheduleDate)){
					form.showDialogMessage("Can not reschedule more than 5 days of Cio Reschedule Date"+AppUtility.parseDate(cioRescheduleDate));
					return false;
				}
			}
		}
		/**
		 * ends here
		 */
		
		return true;
	  
	
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		if(event.getSource().equals(form.prodComposite.getProdID())||event.getSource().equals(form.prodComposite.getProdName())||event.getSource().equals(form.prodComposite.getProdName()))
		{
			retrieveServiceNo();
		}
	}
	
	
	protected void retrieveServiceNo()
	{
		MyQuerry querry=new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("count");
		temp.setIntValue(Integer.parseInt(form.prodComposite.getProdID().getValue()));
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("productCode");
		temp.setStringValue(form.prodComposite.getProdCode().getValue().trim());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(model.getCompanyId());
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ServiceProduct());
		form.showWaitSymbol();
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			 
	            public void onSuccess(ArrayList<SuperModel> result)
	            {
	            	Console.log("product result"+result.size());
	            	if(result.size()>0)
	            	{
	            		for(SuperModel smodel:result)
		            	{
	            			ServiceProduct superProdEntity=(ServiceProduct)smodel;
	            			/*
	            			 * @author Ashwini Patil 
	            			 * Date:11-10-2022 
	            			 * validating product by name instead of id since Ankita pest is facingissue while creating service.
	            			 * When contract is created by upload program default product code and id gets set to the productline which is not matching with the one in database and gives error.
	            			 */
//            				boolean prodValidation = validateProduct(superProdEntity.getCount());
	            			boolean prodValidation = validateProduct(superProdEntity.getProductName(),superProdEntity.getCount());
	            			Console.log("prodValidation "+prodValidation);
            				if(prodValidation==true)
            				{
            					retrieveLatestServiceNo(superProdEntity);
            				}
            				else
            				{
            					form.hideWaitSymbol();
            					form.getProdComposite().getProdID().setValue("");
            					form.getProdComposite().getProdName().setValue("");
            					form.getProdComposite().getProdCode().setValue("");
            					form.showDialogMessage("The product selected does not exists in Contract!");
            				}
		            	}
	            	}
	            	else
	            	{
	            		form.hideWaitSymbol();
	            		form.showDialogMessage("No product found!");
	            	}
	            }

				@Override
				public void onFailure(Throwable caught) {
					form.showDialogMessage("An unexpected error occurred!");
					form.hideWaitSymbol();
				}
		});
		
	}
	
	protected void retrieveLatestServiceNo(final ServiceProduct prodDetails)
	{
		service.getSearchResult(getServiceNoQuerry(prodDetails.getCount()), new AsyncCallback<ArrayList<SuperModel>>() {
			 
            public void onSuccess(ArrayList<SuperModel> result)
            {
//            	if(result.size()>0)
//            	{
            		form.serviceProdEntity=prodDetails;
					form.getIbServiceSrNo().setValue(result.size()+1);
//            	}
				
					Console.log("For sr no querry");
				initializeServiceBranch();
            }

            

			public void onFailure(Throwable throwable)
            {
            	form.showDialogMessage("An unexpected error occurred!");
            	throwable.printStackTrace();
            	form.hideWaitSymbol();
            }

        });
	}
	
	
	protected MyQuerry getServiceNoQuerry(int prodCountId)
	{
		MyQuerry querry = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setLongValue(c.getCompanyId());
		temp.setQuerryString("companyId");
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setIntValue(Integer.parseInt(form.tbContractId.getValue()));
		temp.setQuerryString("contractCount");
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setIntValue(prodCountId);
		temp.setQuerryString("product.count");
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Service());
		
		return querry;
	}
	
	protected boolean validateProduct(String pname,int prodId)
	{
		int ctr=0;
		boolean flagValue=false;
		if(lineItemLis.size()!=0)
		{
			for(int i=0;i<lineItemLis.size();i++)
			{

				if(lineItemLis.get(i).getPrduct().getProductName()==pname)
				{
					ctr=ctr+1;
				}else if(lineItemLis.get(i).getPrduct().getCount()==prodId)
				{
					ctr=ctr+1;
				}
			}
			if(ctr>0)
			{
				flagValue = true;
			}
			else
			{
				flagValue = false;
			}
		}
		return flagValue;
	}
	
	protected void retrieveCustomerAddress(int customerId)
	{
		MyQuerry querry=new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		Company c=new Company();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("count");
		temp.setIntValue(customerId);
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(c.getCompanyId());
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Customer());
		
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			 
        	
            public void onSuccess(ArrayList<SuperModel> result)
            {
            	if(result.size()==0)
            	{
            		form.showDialogMessage("No Customer Found. Please reload!");
            	}
            	if(result.size()>0)
            	{
            		for(SuperModel model:result)
            		{
                		Customer custEntity=(Customer)model;
                		if(custEntity!=null){
                			//Ashwini Patil Date:23-02-2023 Pest-o-shield reported while creating service from customer service screen, billing address is getting mapped
                			form.addresInfo=custEntity.getSecondaryAdress();
                			form.addressComp.setValue(custEntity.getSecondaryAdress());
                		}
                		else{
                			form.showDialogMessage("Customer Address Not Set. Please reload");
                		}
            		}
            	}
            }

            public void onFailure(Throwable throwable)
            {
            	form.showDialogMessage("An unexpected error occurred!");
            	throwable.printStackTrace();
            }
        });
	}
	
	
	/********************************************Sms Method*****************************************/
	
	
	private void sendSMSLogic()
	{
		
		
		
		/**
		 * @author Anil @since 17-05-2021
		 * shifted SMS code to server side 
		 * for making tiny url
		 */
		servicelistserviceAsync.sendServiceComletionSMS(model, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				if(result!=null && !result.equals("")){
					form.showDialogMessage(result);
				}
			}
		});
//		Vector<Filter> filtrvec = new Vector<Filter>();
//		Filter filtr = null;
//		
//		filtr = new Filter();
//		filtr.setLongValue(model.getCompanyId());
//		filtr.setQuerryString("companyId");
//		filtrvec.add(filtr);
//		
//		filtr = new Filter();
//		filtr.setBooleanvalue(true);
//		filtr.setQuerryString("status");
//		filtrvec.add(filtr);
//		
//		MyQuerry querry = new MyQuerry();
//		querry.setFilters(filtrvec);
//		querry.setQuerryObject(new SmsConfiguration());
////		form.showWaitSymbol();
//		
//		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//			
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				if(result.size()!=0){
//					for(SuperModel model:result){
//						SmsConfiguration smsconfig = (SmsConfiguration) model;
//						boolean smsStatus = smsconfig.getStatus();
//						String accountsid = smsconfig.getAccountSID();
//						String authotoken = smsconfig.getAuthToken();
//						String fromnumber = smsconfig.getPassword();
//						
//						if(smsStatus==true){
//						 sendSMS(accountsid, authotoken, fromnumber);
//						}
//						else{
//							form.showDialogMessage("SMS Configuration is InActive");
//						}
//					}
//				}
//			}
//
//			@Override
//			public void onFailure(Throwable caught) {
//			}
//		});
		
	}
	

	private void sendSMS(final String accountsid, final String authotoken,final String fromnumber) {
		
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filtr = null;
		filtr = new Filter();
		filtr.setLongValue(model.getCompanyId());
		filtr.setQuerryString("companyId");
		filtervec.add(filtr);
		
		System.out.println("SERVICE STATUS :: "+model.getStatus());
		
		if (model.getStatus().equals(Service.SERVICESTATUSCOMPLETED)) {

			filtr = new Filter();
			filtr.setStringValue("Service Completion");
			filtr.setQuerryString("event");
			filtervec.add(filtr);
		} 
//		else if (model.getStatus().equals(Service.SERVICESTATUSSCHEDULE)) {
//
//			filtr = new Filter();
//			filtr.setStringValue("Service Schedule");
//			filtr.setQuerryString("event");
//			filtervec.add(filtr);
//		}else if (model.getStatus().equals(Service.SERVICESTATUSRESCHEDULE)) {
//
//			filtr = new Filter();
//			filtr.setStringValue("Service Reschedule");
//			filtr.setQuerryString("event");
//			filtervec.add(filtr);
//		}else{
//			filtr = new Filter();
//			filtr.setStringValue("");
//			filtr.setQuerryString("event");
//			filtervec.add(filtr);
//		}
//		
		filtr = new Filter();
		filtr.setBooleanvalue(true);
		filtr.setQuerryString("status");
		filtervec.add(filtr);
		
		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SmsTemplate());
//		form.showWaitSymbol();
		
		genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if (result.size() != 0) {
					for(SuperModel model1:result){
						SmsTemplate sms =(SmsTemplate) model1;
						final String templateMsgwithbraces = new String(sms.getMessage()); 
//						String custName = form.getPoc().getName().getValue();
						final String prodName = form.getProdComposite().getProdNameValue();
						final String serNo = form.getIbServiceSrNo().getValue()+"";
						final String conId=form.getTbContractId().getValue();
						System.out.println("Service No===="+serNo);
						
						cellNo = form.getPoc().getCellValue();
						final String serDate = AppUtility.parseDate(form.getTbServiceDate().getValue());
						
						
						/**
						 * Date - 29 jun 2017 added by vijay for Eco Friendly getting customer name or customer correspondence name
						 */
						
//						if(LoginPresenter.bhashSMSFlag){

							MyQuerry myquerry = AppUtility.getcustomerName(form.getPoc().getIdValue(), model.getCompanyId());
							genasync.getSearchResult(myquerry, new AsyncCallback<ArrayList<SuperModel>>() {
								
								@Override
								public void onSuccess(ArrayList<SuperModel> result) {
									// TODO Auto-generated method stub
									for(SuperModel model2 :result){
										Customer customer = (Customer) model2;
										 
										if(LoginPresenter.bhashSMSFlag){
											String custName;
											 Console.log("Currespondence=="+customer.getCustPrintableName());
											if(!customer.getCustPrintableName().equals("")){
												custName = customer.getCustPrintableName();
												Console.log("Customer correspondence Name =="+custName);
											}else{
												custName = customer.getFullname();
												Console.log("Customer  Name =="+custName);
											}
											String cutomerName = templateMsgwithbraces.replace("{CustomerName}", custName);
											String productName = cutomerName.replace("{ProductName}", prodName);
	//										String serviceNo = productName.replace("{ServiceNo}", serNo);
											String serviceDate =  productName.replace("{ServiceDate}", serDate);
											companyName = Slick_Erp.businessUnitName;
											actualMsg = serviceDate.replace("{companyName}", companyName);
											
											
											/**
											 * @author Anil @since 03-03-2021
											 * Adding feedback url in service completion sms
											 */
											Service service=(Service) model;
											getFeedbackFormUrl(service,customer);
											System.out.println("Actual MSG:"+actualMsg);
										
										}
										
										/**
										 * Date 9-11-2019 by Amol raised by Prasad
										 * For EVRIM add Contract Id in sms template
										 * in EVA
										 */
										else if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ChangeMessageTemplate")) {
											String custName = form.getPoc().getName().getValue();

											String cutomerName = templateMsgwithbraces.replace("{CustomerName}",custName);
											String productName = cutomerName.replace("{ProductName}",prodName);
											String serviceNo = productName.replace("{ServiceNo}", serNo);
											String contractId = serviceNo.replace("{Contract id}", conId);
											String serviceDate = contractId.replace("{ServiceDate}",serDate);
											companyName = Slick_Erp.businessUnitName;
											actualMsg = serviceDate.replace("{companyName}", companyName);

											/**
											 * @author Anil @since 03-03-2021 Adding
											 *         feedback url in service
											 *         completion sms
											 */
											Service service = (Service) model;
											getFeedbackFormUrl(service, customer);

										} else {
											String custName = form.getPoc().getName().getValue();

											String cutomerName = templateMsgwithbraces.replace("{CustomerName}",custName);
											String productName = cutomerName.replace("{ProductName}",prodName);
											String serviceNo = productName.replace("{ServiceNo}", serNo);
											String serviceDate = serviceNo.replace("{ServiceDate}", serDate);
											companyName = Slick_Erp.businessUnitName;
											actualMsg = serviceDate.replace("{companyName}", companyName);

											/**
											 * @author Anil @since 03-03-2021 Adding
											 *         feedback url in service
											 *         completion sms
											 */
											Service service = (Service) model;
											getFeedbackFormUrl(service, customer);

											System.out.println("Actual MSG:"+ actualMsg);
											System.out.println("Company Name========================:"+ companyName);
										}
									}
								}
								
								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									
								}
							});
							
//							} else {
//								/**
//								 * ends here
//								 */
//								/**
//								 * Date 9-11-2019 by Amol raised by Prasad
//								 * For EVRIM add Contract Id in sms template
//								 * in EVA
//								 */
//								if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ChangeMessageTemplate")) {
//									String custName = form.getPoc().getName().getValue();
//
//									String cutomerName = templateMsgwithbraces.replace("{CustomerName}",custName);
//									String productName = cutomerName.replace("{ProductName}",prodName);
//									String serviceNo = productName.replace("{ServiceNo}", serNo);
//									String contractId = serviceNo.replace("{Contract id}", conId);
//									String serviceDate = contractId.replace("{ServiceDate}",serDate);
//									companyName = Slick_Erp.businessUnitName;
//									actualMsg = serviceDate.replace("{companyName}", companyName);
//
//									/**
//									 * @author Anil @since 03-03-2021 Adding
//									 *         feedback url in service
//									 *         completion sms
//									 */
//									Service service = (Service) model;
//									getFeedbackFormUrl(service, customer);
//
//								} else {
//									String custName = form.getPoc().getName().getValue();
//
//									String cutomerName = templateMsgwithbraces.replace("{CustomerName}",custName);
//									String productName = cutomerName.replace("{ProductName}",prodName);
//									String serviceNo = productName.replace("{ServiceNo}", serNo);
//									String serviceDate = serviceNo.replace("{ServiceDate}", serDate);
//									companyName = Slick_Erp.businessUnitName;
//									actualMsg = serviceDate.replace("{companyName}", companyName);
//
//									/**
//									 * @author Anil @since 03-03-2021 Adding
//									 *         feedback url in service
//									 *         completion sms
//									 */
//									Service service = (Service) model;
//									getFeedbackFormUrl(service, customer);
//
//									System.out.println("Actual MSG:"+ actualMsg);
//									System.out.println("Company Name========================:"+ companyName);
//								}
//							}
						
//						smsserviceAsync.sendSmsToClient(actualMsg, cellNo, accountsid, authotoken,fromnumber,model.getCompanyId(), new AsyncCallback<Integer>(){
//
//							@Override
//							public void onFailure(Throwable caught) {
//								form.hideWaitSymbol();
//							}
//
//							@Override
//							public void onSuccess(Integer result) {
//								form.hideWaitSymbol();
//								if(result==1)
//								{
//									form.showDialogMessage("SMS Sent Successfully!");
//									saveSmsHistory();
//
//								}
//							}} );
							
							smsserviceAsync.sendMessage(AppConstants.SERVICEMODULE, AppConstants.SERVICE, sms.getEvent(), model1.getCompanyId(), actualMsg, cellNo,false, new AsyncCallback<String>() {
								
								@Override
								public void onSuccess(String result) {
									// TODO Auto-generated method stub
									form.hideWaitSymbol();
									form.showDialogMessage("Message Sent Successfully!");

								}
								
								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									form.hideWaitSymbol();
								}
							});
							
						form.hideWaitSymbol();
					}
				}
			
				
			}
			@Override
			public void onFailure(Throwable caught) {
			}
		});

				form.hideWaitSymbol();
	}	
	
	private void saveSmsHistory (){

		SmsHistory smshistory = new SmsHistory();
		smshistory.setDocumentId(model.getCount());
		smshistory.setDocumentType("Service");
		smshistory.setSmsText(actualMsg);
		smshistory.setName(form.getPoc().getName().getValue());
		smshistory.setCellNo(cellNo);
		smshistory.setUserId(LoginPresenter.loggedInUser);
		System.out.println("UserID"+LoginPresenter.loggedInUser);
		System.out.println("Doc Id"+model.getCount());
		System.out.println("Name"+form.getPoc().getName().getValue());
		genasync.save(smshistory, new AsyncCallback<ReturnFromServer>() {
			
			@Override
			public void onSuccess(ReturnFromServer result) {
				System.out.println("Data Saved SuccessFully");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				System.out.println("Data Saved UnsuccessFully");

			}
		});		
	}
	
	private void updateTaxes() {
		Vector<Filter> filtervec=new Vector<Filter>();
		
		MyQuerry querry = new MyQuerry();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("serviceDate >=");
		filter.setDateValue(frmAndToPop.fromDate.getValue());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("serviceDate <=");
		filter.setDateValue(frmAndToPop.toDate.getValue());
		filtervec.add(filter);
		
//		filter = new Filter();
//		filter.setQuerryString("status");
//		filter.setStringValue(BillingDocument.CREATED);
//		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Service());
		form.showWaitSymbol();
		genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if (result.size() != 0) {
					for(SuperModel model:result){
						Service con=(Service) model;
						if(con.getTeam()!=null){
							
						}else{
							con.setTeam("");
							
							genasync.save(con,new AsyncCallback<ReturnFromServer>() {
								@Override
								public void onFailure(Throwable caught) {
									form.hideWaitSymbol();
								}
								@Override
								public void onSuccess(ReturnFromServer result) {
									form.hideWaitSymbol();
									form.showDialogMessage("Entity updated successfully!");
								}
							});
						}
						
					}
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage("Unexpected Error!");
			}
		});
	}
	
		
	/**
	 * Date : 25/01/2017
	 * Created by: Rahul Verma
	 * Desp: This method set visibilty of each field false 
	 */
	
//	private void reactofstackDetailsPopUp() {
//		// TODO Auto-generated method stub
//		stackDetailsPopUp.wareHouse.setVisible(false);
//		stackDetailsPopUp.storageLocation.setVisible(false);
//		stackDetailsPopUp.storageBin.setVisible(false);
//		stackDetailsPopUp.quantity.setVisible(false);
//		stackDetailsPopUp.description.setVisible(false);
//		stackDetailsPopUp.tbWareHouse.setVisible(false);
//		stackDetailsPopUp.tbStorageLocation.setVisible(false);
//		stackDetailsPopUp.tbStorageBin.setVisible(false);
//		stackDetailsPopUp.tbQuantity.setVisible(false);
//		stackDetailsPopUp.tbDescription.setVisible(false);
//		stackDetailsPopUp.addStack.setVisible(false);
//		stackDetailsPopUp.stackDetailsTable.setValue(model.getStackDetailsList());
//	}
	
	/**
	 * ends here 
	 */
	
	/**
	 * Description: This is for NBHC.
	 * By Rahul Verma
	 * Date : 25/01/2017
	 * @param model 
	 */
	private void createExtraServiceIfStackZero(Service model) {
		// TODO Auto-generated method stub
		Console.log("Inside Create Extra Service");
		for (int i = 0; i < model.getStackDetailsList().size(); i++) {
			if(model.getStackDetailsList().get(i).getQauntity()!=0){
				Console.log("Quantity not eQAUL to zero");
				createAnExtraService(model);
				break;
			}else{
				System.out.println("Do not create service");
			}
		}
	}
	
	
	private void createAnExtraService(final Service model) {
		// TODO Auto-generated method stub
		Console.log("Calling RPC to complete services");
		final CreateSingleServiceServiceAsync createSingleServiceAsync = GWT
				.create(CreateSingleServiceService.class);
		Timer timer=new Timer() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				createSingleServiceAsync.createSingleService(model.getCount(),model.getServiceDate(),model.getServiceIndexNo(),model.getServiceSerialNo(),model.getContractCount(),model.getCompanyId(),new AsyncCallback<ArrayList<Integer>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						Console.log("Failed"+caught);
						form.showDialogMessage("An Unexpected Error");
					}

					@Override
					public void onSuccess(ArrayList<Integer> result) {
						// TODO Auto-generated method stub
						if(result.contains(-1)){
							form.showDialogMessage("Unexpected Error");
						}else if(result.contains(-2)){
							form.showDialogMessage("Parsing Previous Date Error");
						}else if(result.contains(-3)){
							form.showDialogMessage("Service Date Parsing Error");
						}else if(result.contains(1)){
							form.showDialogMessage("Successfull created Upcoming Services");
						}
					}
				});
			}
		};
		timer.schedule(3000);
		
	}
	
	/** DATE 18.4.2018 added by komal for orion  **/
	
	public void checkCustomerBranchArea() {
		MyQuerry querry=new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(c.getCompanyId());
		filtervec.add(temp);
		
		if(form.getPoc()!=null){
		temp=new Filter();
		temp.setQuerryString("cinfo.count");
		temp.setIntValue(form.getPoc().getIdValue());
		filtervec.add(temp);
		}
		temp=new Filter();
		temp.setQuerryString("status");
		temp.setBooleanvalue(true);
		filtervec.add(temp);
		
		if(form.getTbserviceBranch().getValue()!=null){
		temp=new Filter();
		temp.setQuerryString("buisnessUnitName");
		temp.setStringValue(form.getTbserviceBranch().getValue());
		filtervec.add(temp);
		}
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerBranchDetails());
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
				//showDialogMessage("An Unexpected error occurred!");
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if(result.size()>0){
					for(SuperModel s : result){
						CustomerBranchDetails customerBranchDetails = (CustomerBranchDetails) s;
						if(form.quantity.getValue()!=null){
							if(customerBranchDetails.getArea() != form.quantity.getValue()){
								form.showDialogMessage("Service can not be  completed as area in service and area in customer branch are different.");
							    break;
							}else{
								reactOnMarkComplete();
							}
						}else{
							reactOnMarkComplete();
						}
					}
				 }else{
                     reactOnMarkComplete();
				 }

				}
			 });
		}
	/** date 18.04.2018 added by komal for mark complete popup**/
	private void reactOnMarkComplete(){
	/** * Date : 21-03-2017 By Anil adding restriction to complete the  service for NBHC CCPM **/
	
		/**
		 * Date 17-4-2018
		 * By jayshree
		 */
		if(model.getStatus().equalsIgnoreCase(Service.SERVICESUSPENDED)){
			form.showDialogMessage("Service status is suspended we Can not be mark completed !");
		}
		else{
		
		if (validateCompletion()) {
		Date todayDate = new Date();
		/**
		 * @author Anil
		 * @since 09-11-2020
		 * As service date's time is default set to 13, cuasing an issue while completing service before 13:00 PM
		 */
		if(AppUtility.formatDate(AppUtility.parseDate(todayDate)).before(AppUtility.formatDate(AppUtility.parseDate(form.tbServiceDate.getValue())))){
//		if (todayDate.before(form.tbServiceDate.getValue())) {
			System.out.println("Service Date Exceeds");
			form.showDialogMessage("Service Date exceeds current date. Please reschedule the date");

		}
		/**
		 * Date :- 05-Oct-2018 By Vijay
		 * Des :- NBHC CCPM Validation for service can not complete directly without creating service project
		 */
		else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC")){
			if(!LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
				MyQuerry querry = validateProject(model.getContractCount(),model.getCount());
				form.showWaitSymbol();
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						if(result.size()==0){
							form.showDialogMessage("Please create project first for this service!");
						}else{
							

							/**
							 * nidhi
							 *  ||*
							 *  11-10-2018	
							 */
							boolean flagBillMap = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceBillingDetailMapingProcess");
							
							if(flagBillMap ){
								if( !model.isRenewContractFlag() && !model.isServiceWiseBilling()){
									MyQuerry querry = validateBillingIdProject(model.getContractCount(), model.getCount());
									genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
										
										@Override
										public void onSuccess(ArrayList<SuperModel> result) {
												
											if(result.size()>0){
												for (SuperModel sp : result) {
													BillingDocument billDoc = (BillingDocument) sp;
													if(result.size() == 1 ){
														billingList.put(billDoc.getCount(), billDoc.getCount() +"  -  "+AppUtility.parseDate(billDoc.getBillingDate()));
														markcompletepopup.getBillingIdList().addItem(billDoc.getCount() +"  -  "+AppUtility.parseDate(billDoc.getBillingDate()), billDoc.getCount()+"");
													
													}else if(billDoc.getStatus().equals(BillingDocument.CREATED)){
														billingList.put(billDoc.getCount(), billDoc.getCount() +"  -  "+AppUtility.parseDate(billDoc.getBillingDate()));
														markcompletepopup.getBillingIdList().addItem(billDoc.getCount() +"  -  "+AppUtility.parseDate(billDoc.getBillingDate()), billDoc.getCount()+"");
													
													}
												}
												
											}
											markpanel = new PopupPanel(true);
											markpanel.add(markcompletepopup);
											markpanel.show();
											markpanel.center();
											
										}
										@Override
										public void onFailure(Throwable caught) {
											form.hideWaitSymbol();
										}
									});
								}else if(model.isRenewContractFlag()){
									billingList.put(model.getBillingCount(),"");
									markcompletepopup.getBillingIdList().addItem(model.getBillingCount()+"",model.getBillingCount()+"");
									markpanel = new PopupPanel(true);
									markpanel.add(markcompletepopup);
									markpanel.show();
									markpanel.center();
								}
								/**
								 * Date 18-03-2019 by Vijay to show mark complete popup
								 */
								else{
									billingList.put(model.getBillingCount(),"");
									markcompletepopup.getBillingIdList().addItem(model.getBillingCount()+"",model.getBillingCount()+"");
									markpanel = new PopupPanel(true);
									markpanel.add(markcompletepopup);
									markpanel.show();
									markpanel.center();
								}
								
							}else{
								billingList.put(model.getBillingCount(),"");
								markcompletepopup.getBillingIdList().addItem(model.getBillingCount()+"",model.getBillingCount()+"");
								markcompletepopup.getBillingIdList().setVisible(false);
								markcompletepopup.billLbl.setVisible(false);
								markpanel = new PopupPanel(true);
								markpanel.add(markcompletepopup);
								markpanel.show();
								markpanel.center();
							}
							//validateBillingIdProject
							

							addDefaultValueInMarkCompletePopUp(markcompletepopup);
						
						/*	markpanel = new PopupPanel(true);
							markpanel.add(markcompletepopup);
							markpanel.show();
							markpanel.center();

							addDefaultValueInMarkCompletePopUp(markcompletepopup);*/
						}
						form.hideWaitSymbol();
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
					}
				});
			}else{
				

				/**
				 * nidhi
				 *  ||*
				 *  11-10-2018	
				 */
				boolean flagBillMap = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceBillingDetailMapingProcess");
				
				if(flagBillMap ){
					if( !model.isRenewContractFlag() && !model.isServiceWiseBilling()){
						MyQuerry querry = validateBillingIdProject(model.getContractCount(), model.getCount());
						genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
							
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
									
								if(result.size()>0){
									for (SuperModel sp : result) {
										BillingDocument billDoc = (BillingDocument) sp;
										
										if(result.size() == 1 ){
											billingList.put(billDoc.getCount(), billDoc.getCount() +"  -  "+AppUtility.parseDate(billDoc.getBillingDate()));
											markcompletepopup.getBillingIdList().addItem(billDoc.getCount() +"  -  "+AppUtility.parseDate(billDoc.getBillingDate()), billDoc.getCount()+"");
										
										}else if(billDoc.getStatus().equals(BillingDocument.CREATED)){
											billingList.put(billDoc.getCount(), billDoc.getCount() +"  -  "+AppUtility.parseDate(billDoc.getBillingDate()));
											markcompletepopup.getBillingIdList().addItem(billDoc.getCount() +"  -  "+AppUtility.parseDate(billDoc.getBillingDate()), billDoc.getCount()+"");
										
										}
									}
									
								}
								markpanel = new PopupPanel(true);
								markpanel.add(markcompletepopup);
								markpanel.show();
								markpanel.center();
								
							}
							@Override
							public void onFailure(Throwable caught) {
								form.hideWaitSymbol();
							}
						});
					}else if(model.isRenewContractFlag()){
						billingList.put(model.getBillingCount(),"");
						markcompletepopup.getBillingIdList().addItem(model.getBillingCount()+"",model.getBillingCount()+"");
						markpanel = new PopupPanel(true);
						markpanel.add(markcompletepopup);
						markpanel.show();
						markpanel.center();
					}
					/**
					 * Date 18-03-2019 by Vijay to show mark complete popup
					 */
					else{
						billingList.put(model.getBillingCount(),"");
						markcompletepopup.getBillingIdList().addItem(model.getBillingCount()+"",model.getBillingCount()+"");
						markpanel = new PopupPanel(true);
						markpanel.add(markcompletepopup);
						markpanel.show();
						markpanel.center();
					}
					
				}else{
					billingList.put(model.getBillingCount(),"");
					markcompletepopup.getBillingIdList().addItem(model.getBillingCount()+"",model.getBillingCount()+"");
					markcompletepopup.getBillingIdList().setVisible(false);
					markcompletepopup.billLbl.setVisible(false);
					markpanel = new PopupPanel(true);
					markpanel.add(markcompletepopup);
					markpanel.show();
					markpanel.center();
				}
				//validateBillingIdProject
				

				addDefaultValueInMarkCompletePopUp(markcompletepopup);
			
			/*	markpanel = new PopupPanel(true);
				markpanel.add(markcompletepopup);
				markpanel.show();
				markpanel.center();

				addDefaultValueInMarkCompletePopUp(markcompletepopup);*/
			}
			
		}
		else {
			
			
			/**
			 * nidhi
			 *  ||*
			 *  11-10-2018	
			 */
			boolean flagBillMap = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceBillingDetailMapingProcess");
			
			if(flagBillMap ){
				if( !model.isRenewContractFlag() && !model.isServiceWiseBilling()){
					MyQuerry querry = validateBillingIdProject(model.getContractCount(), model.getCount());
					genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
								
							if(result.size()>0){
								for (SuperModel sp : result) {
									BillingDocument billDoc = (BillingDocument) sp;
									if(result.size() == 1 ){
										billingList.put(billDoc.getCount(), billDoc.getCount() +"  -  "+AppUtility.parseDate(billDoc.getBillingDate()));
										markcompletepopup.getBillingIdList().addItem(billDoc.getCount() +"  -  "+AppUtility.parseDate(billDoc.getBillingDate()), billDoc.getCount()+"");
									
									}else if(billDoc.getStatus().equals(BillingDocument.CREATED)){
										billingList.put(billDoc.getCount(), billDoc.getCount() +"  -  "+AppUtility.parseDate(billDoc.getBillingDate()));
										markcompletepopup.getBillingIdList().addItem(billDoc.getCount() +"  -  "+AppUtility.parseDate(billDoc.getBillingDate()), billDoc.getCount()+"");
									
									}
									
								}
								markpanel = new PopupPanel(true);
								markpanel.add(markcompletepopup);
								markpanel.show();
								markpanel.center();
							}
							
						}
						@Override
						public void onFailure(Throwable caught) {
							form.hideWaitSymbol();
						}
					});
				}else{
					billingList.put(model.getBillingCount(),"");
					markcompletepopup.getBillingIdList().addItem(model.getBillingCount()+"",model.getBillingCount()+"");
					markpanel = new PopupPanel(true);
					markpanel.add(markcompletepopup);
					markpanel.show();
					markpanel.center();
				}
				
			}else{
				billingList.put(model.getBillingCount(),"");
				markcompletepopup.getBillingIdList().addItem(model.getBillingCount()+"",model.getBillingCount()+"");
				markcompletepopup.getBillingIdList().setVisible(false);
				markcompletepopup.billLbl.setVisible(false);
				markpanel = new PopupPanel(true);
				markpanel.add(markcompletepopup);
				markpanel.show();
				markpanel.center();
			}
			//validateBillingIdProject
			

			addDefaultValueInMarkCompletePopUp(markcompletepopup);
		
			
			/*System.out.println("Pop Up Showing");
			markpanel = new PopupPanel(true);
			markpanel.add(markcompletepopup);
			markpanel.show();
			markpanel.center();

			addDefaultValueInMarkCompletePopUp(markcompletepopup);*/
		}
	}
		}
  }

	
	/**
	 * Date :- 05-Oct-2018 By Vijay
	 * Des :- NBHC CCPM Validation for service can not complete directly without creating service project
	 */
	private MyQuerry validateProject(Integer contractCount, int serviceId) {
		MyQuerry querry = new MyQuerry();
		Vector<Filter> filterVec = new Vector<Filter>();
		Filter filter;
		
		filter = new Filter();
		filter.setQuerryString("contractId");
		filter.setIntValue((int)contractCount);
		filterVec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("serviceId");
		filter.setIntValue(serviceId);
		filterVec.add(filter);
		
		querry.setFilters(filterVec);
		querry.setQuerryObject(new ServiceProject());
		
		return querry;
	}
	/**
	 * ends here
	 */
	/**
	 * nidhi
	 * 11-10-2018
	 *  ||*
	 * @param contractCount
	 * @param serviceId
	 * @return
	 */
	private MyQuerry validateBillingIdProject(Integer contractCount, int serviceId) {
		MyQuerry querry = new MyQuerry();
		Vector<Filter> filterVec = new Vector<Filter>();
		Filter filter;
		
		filter = new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue((int)contractCount);
		filterVec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("personInfo.count");
		filter.setIntValue(model.getPersonInfo().getCount());
		filterVec.add(filter);
		
		/*filter = new Filter();
		filter.setQuerryString("status");
		filter.setStringValue(BillingDocument.CREATED);
		filterVec.add(filter);*/
		
		querry.setFilters(filterVec);
		querry.setQuerryObject(new BillingDocument());
		
		return querry;
	}
	
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		
		/**
		 * Date : 28-12-2018 BY ANIL
		 * Commenting this code,because of this we are not able to select the technicain as its checks the whether selected technician is registered for app
		 */
		
//		if(event.getSource()==form.tbServiceEngg){
//			 /**
//			  * Rahul Verma added below for notification on 28th Sept 2018
//			  * For Reschedule
//			  */
//			 NotificationServiceAsync asyncService=GWT.create(NotificationService.class);
//			 if(form.tbServiceEngg.getSelectedIndex()!=0){
//				 
//				 String message="Service Id: "+model.getCount()+" is assigned to you.";
//				 Console.log("message"+message);
//				 Console.log("form.tbServiceEngg.getValue(form.tbServiceEngg.getSelectedIndex())"+form.tbServiceEngg.getValue(form.tbServiceEngg.getSelectedIndex()));
//				 
//				 asyncService.sendNotification(model.getCompanyId(), form.tbServiceEngg.getValue(form.tbServiceEngg.getSelectedIndex()), AppConstants.IAndroid.EVA_PEDIO,message, new AsyncCallback<ArrayList<Integer>>() {
//
//					@Override
//					public void onFailure(Throwable caught) {
//						// TODO Auto-generated method stub
//						
//					}
//
//					@Override
//					public void onSuccess(ArrayList<Integer> result) {
//						// TODO Auto-generated method stub
//						int resultValue=result.get(0);
//						if(resultValue==1){
//							form.showDialogMessage("Successful to send notification to technician");
//						}else if(resultValue==2){
//							form.showDialogMessage("Device not register for "+form.tbServiceEngg.getValue(form.tbServiceEngg.getSelectedIndex()));
//						}else if(resultValue==3){
//							form.showDialogMessage("No Username defined for "+form.tbServiceEngg.getValue(form.tbServiceEngg.getSelectedIndex()));
//						}else if(resultValue==0){
//							form.showDialogMessage("Notification not sent to EVA Pedio");
//						}
//					}
//				});
//			 }
//			 /**
//			  * Ends for Rahul
//			  */
//		}
		
		if(event.getSource()==form.tbserviceBranch){
			CustomerBranchDetails custBranch=form.tbserviceBranch.getSelectedItem();
			if(custBranch!=null){
				if(custBranch.getAddress()!=null){
					form.addressComp.setValue(custBranch.getAddress());
				}
			}
		}
		
	}
	
	
	private void reactOnStackDetails() {
//		if(model.getStackDetailsList()!=null){
//			System.out.println("Hi in the service data");
//			stackDetailsPopUp.showPopUp();
//			if(AppMemory.getAppMemory().currentState.toString().equals("NEW") || AppMemory.getAppMemory().currentState.toString().equals("EDIT")){
//				stackDetailsPopUp.setEnable(true);
//				stackDetailsPopUp.getLblOk().setVisible(true);
//			}
//			else{
//				stackDetailsPopUp.setEnable(false);
//				stackDetailsPopUp.getLblOk().setVisible(false);
//			}
//			stackDetailsPopUp.getStackDetailsTable().getDataprovider().setList(model.getStackDetailsList());
//		}
//		else{
//			System.out.println("From the Contract data");
//			System.out.println("Contract ID"+model.getContractCount());
//			MyQuerry querry = new MyQuerry();
//			Vector<Filter> filetVec = new Vector<Filter>();
//			Filter filter = null;
//			
//			filter = new Filter();
//			filter.setQuerryString("count");
//			filter.setIntValue(model.getContractCount());
//			filetVec.add(filter);
//			
//			querry.setFilters(filetVec);
//			querry.setQuerryObject(new Contract());
//			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
//				
//				@Override
//				public void onSuccess(ArrayList<SuperModel> result) {
//					// TODO Auto-generated method stub
//					if(result.size()!=0){
//						Contract contract = new Contract();
//						for(SuperModel model : result){
//							contract = (Contract) model;
//							break;
//						}
//						stackDetailsPopUp.getStackDetailsTable().connectToLocal();
//						stackDetailsPopUp.showPopUp();
//						if(AppMemory.getAppMemory().currentState.toString().equals("NEW") || AppMemory.getAppMemory().currentState.toString().equals("EDIT")){
//							stackDetailsPopUp.setEnable(true);
//							stackDetailsPopUp.getLblOk().setVisible(true);
//						}
//						else{
//							stackDetailsPopUp.setEnable(false);
//							stackDetailsPopUp.getLblOk().setVisible(false);
//						}
//						if(contract.getStackDetailsList()!=null&&contract.getStackDetailsList().size()!=0){
//							stackDetailsPopUp.getStackDetailsTable().getDataprovider().setList(contract.getStackDetailsList());
//						}
//					}
//					else{
//						form.showDialogMessage("No Data found from Contract");
//					}
//				}
//				
//				@Override
//				public void onFailure(Throwable caught) {
//					// TODO Auto-generated method stub
//					
//				}
//			});
//		}
		
//		if(model.getStackDetailsList()!=null){
			System.out.println("Hi in the service data");
			ArrayList<StackDetails> stackdetailslist = new ArrayList<StackDetails>();
			StackDetails stackdetails = new StackDetails();
			stackdetails.setWareHouse(model.getWareHouse());
			stackdetails.setWarehouseCode(model.getRefNo2());
			stackdetails.setShade(model.getShade());
			stackdetails.setStorageLocation(model.getCompartment());
			stackdetails.setStackNo(model.getStackNo());
			stackdetails.setQauntity(model.getStackQty());
//			stackdetails.setDescription(model.getco);
			
			stackDetailsPopUp.showPopUp();
			stackdetailslist.add(stackdetails);
			stackDetailsPopUp.getStackDetailsTable().getDataprovider().setList(stackdetailslist);
			stackDetailsPopUp.getLblOk().setVisible(false);

//		}
		
	}

	/**
	 * @author Vijay Chougule Date 21-09-2019 
	 * Des :- NBHC CCPM complaint service assessment must be close
	 */
	private void reactonValidateAssessment() {
		AppUtility apputility = new AppUtility(); 
		 MyQuerry query =  apputility.complaintIdQueryy(model.getTicketNumber());
		 form.showWaitSymbol();
		genasync.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				if(result.size()==0){
					reactOnMarkComplete();
				}
				else{
					form.showDialogMessage("Please complete Assessment first for complaint service");
				}
				form.hideWaitSymbol();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
			}
		});
	}
	
	
	public void viewComplain(){
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(model.getTicketNumber());
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new Complain());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No complain document found.");
					return;
				}
				if(result.size()==1){
					AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Support",Screen.COMPLAIN);
					final Complain complaiin=(Complain) result.get(0);
					final ComplainForm form=ComplainPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(complaiin);
							form.setToViewState();
						}
					};
					timer.schedule(3000);
				}
				
			}
		});
		
	
	}
	
	
	private void reactOnRegisterExpense() {
		// TODO Auto-generated method stub
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Expense Management",Screen.MULTIPLEEXPENSEMANAGMENT);
		final MultipleExpenseMngt expense=new MultipleExpenseMngt();
		final MultipleExpensemanagmentForm form=MultipleExpensemanagmentPresenter.initalize();
		expense.setBranch(model.getBranch());
		expense.setServiceId(model.getCount());
		form.showWaitSymbol();
		Timer timer=new Timer() {
			@Override
			public void run() {
				form.hideWaitSymbol();
				form.setToNewState();
				form.getPersonInfo().setEnable(false);
				form.getTbServiceId().setEnabled(false);
				form.updateView(expense);
				form.getPersonInfo().setValue(form.getPersonInfo().getEmployeeInfoFromName(model.getEmployee()));
				form.getPersonInfo().getValue().setFullName(model.getEmployee());
				form.getPersonInfo().getName().setValue(model.getEmployee());
			}
		};
		timer.schedule(5000);
	}
	private void reactonRegisterExpenseToOpenPopup() {
		AppMemory.getAppMemory().currentScreen = Screen.MULTIPLEEXPENSEMANAGMENT;
		genralInvoicePopup = new GeneralViewDocumentPopup(true,false,true);
		final MultipleExpenseMngt expense=new MultipleExpenseMngt();
		expense.setBranch(model.getBranch());
		expense.setServiceId(model.getCount());
		genralInvoicePopup.setModel(AppConstants.MULTIPLEXPMANAGEMENT,expense);
		generalPanel = new PopupPanel();
		generalPanel.add(genralInvoicePopup);
		generalPanel.show();
		generalPanel.center();
		genralInvoicePopup.btnClose.addClickHandler(this);		
	}
	private void reactonViewExpenseToOpenPopup() {
		
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("serviceId");
		filter.setIntValue(model.getCount());
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new MultipleExpenseMngt());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No expense document found.");
					return;
				}
				if(result.size()==1){
					
					final MultipleExpenseMngt expense=(MultipleExpenseMngt) result.get(0);//					
					openViewExpensePopup(expense);
				}else {
					form.showDialogMessage("More than one expense found for this service. Please go to Accounts -> Expense Management -> Search with Service Id to check all expenses.");
				}
			}
		});
		
		
	}

	private void openViewExpensePopup(MultipleExpenseMngt expense) {
		AppMemory.getAppMemory().currentScreen = Screen.MULTIPLEEXPENSEMANAGMENT;
		genralInvoicePopup = new GeneralViewDocumentPopup(true,false,true);
		genralInvoicePopup.setModel("View Expense",expense);
		generalPanel = new PopupPanel();
		generalPanel.add(genralInvoicePopup);
		generalPanel.show();
		generalPanel.center();
		genralInvoicePopup.btnClose.addClickHandler(this);	
	}
	private void reactOnViewExpense() {
		// TODO Auto-generated method stub
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("serviceId");
		filter.setIntValue(model.getCount());
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new MultipleExpenseMngt());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No expense document found.");
					return;
				}
				if(result.size()==1){
					final MultipleExpenseMngt expense=(MultipleExpenseMngt) result.get(0);
					final MultipleExpensemanagmentForm form=MultipleExpensemanagmentPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(expense);
							form.setToViewState();
						}
					};
					timer.schedule(3000);
				}
			}
		});
	}
	
	private void initializeServiceBranch() {
		// TODO Auto-generated method stub
		HashSet<String>branchhs=new HashSet<String>();
		if(lineItemLis!=null&&form.prodComposite.getValue()!=null){
			Console.log("inside lineItemLis!=null initializeServiceBranch");
			for(SalesLineItem item:lineItemLis){
				if(item.getPrduct().getCount()==Integer.parseInt(form.prodComposite.getProdID().getValue().trim())){
					if(item.getCustomerBranchSchedulingInfo()!=null){
						for(BranchWiseScheduling branchObj:item.getCustomerBranchSchedulingInfo().get(item.getProductSrNo())){
							if(branchObj.isCheck()==true){
								branchhs.add(branchObj.getBranchName());
							}
						}
					}
					
				}
			}
			Console.log("branchhs size"+branchhs.size());

			if(branchhs.size()!=0){
				final List<String> branchList=new ArrayList<String>(branchhs);
				

				MyQuerry querry=new MyQuerry();
				Company c=new Company();
				Vector<Filter> filtervec=new Vector<Filter>();
				Filter temp=null;
				
				temp=new Filter();
				temp.setQuerryString("companyId");
				temp.setLongValue(c.getCompanyId());
				filtervec.add(temp);
				
				if(form.getPoc()!=null){
				temp=new Filter();
				temp.setQuerryString("cinfo.count");
				temp.setIntValue(form.getPoc().getIdValue());
				filtervec.add(temp);
				}
				temp=new Filter();
				temp.setQuerryString("status");
				temp.setBooleanvalue(true);
				filtervec.add(temp);
				
				temp=new Filter();
				temp.setQuerryString("buisnessUnitName IN");
				temp.setList(branchList);
				filtervec.add(temp);
				
				querry.setFilters(filtervec);
				querry.setQuerryObject(new CustomerBranchDetails());
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						Console.log("CustomerBranchDetails size"+result.size());
						
						form.hideWaitSymbol();
						if(result.size()>0){
							List<CustomerBranchDetails> custBranchLis=new ArrayList<CustomerBranchDetails>();
							for(SuperModel model:result){
								CustomerBranchDetails branch=(CustomerBranchDetails) model;
								custBranchLis.add(branch);
							}
							
							form.tbserviceBranch.setListItems(custBranchLis);
							if(branchList.contains("Service Address")){
								form.tbserviceBranch.addItem("Service Address");
							}
							form.tbserviceBranch.setEnabled(true);
						}
					}
				 });
				
			}else{
				
				form.tbserviceBranch.addItem("Service Address");
				form.tbserviceBranch.setEnabled(true);
				form.tbserviceBranch.setValue("Service Address");
				form.hideWaitSymbol();
			}
		}
		else{
			form.hideWaitSymbol();
		}
	}
	
	
	/**
	 * @author Vijay Date 23-11-2020
	 * Des :- if ComplainServiceWithTurnAroundTime process config is active
	 * then service will close directly withoused asking user invoice requirement from Rahul for PTSPL
	 */
	private void reactonMarkComplete() {
		Console.log("markCompleteServicesWithBulk");
		ArrayList<Service> selectedlist = new ArrayList<Service>();
		selectedlist.add(model);
		
		if(model.getServiceDate().after(new Date())){
			form.showDialogMessage("Service Date exceeds current date. Please reschedule the date");
			return;
		}
		final Date completionDate = new Date();
		
		
		form.showWaitSymbol();
		servicelistserviceAsync.markCompleteServicesWithBulk(selectedlist, "", completionDate, new AsyncCallback<Integer>() {
			@Override
			public void onSuccess(Integer result) {
				// TODO Auto-generated method stub
				if(result==1){
					model.setStatus(Service.SERVICESTATUSCOMPLETED);
					model.setServiceCompletionDate(completionDate);
					form.getDbServiceCompletionDate().setValue(completionDate);
					form.showDialogMessage("Marked Completed !");
					form.getTbStatus().setValue(Service.SERVICESTATUSCOMPLETED);
					form.setToViewState();
				}
				form.hideWaitSymbol();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();

			}
		});
		
	}
	/**
	 * ends here
	 */

	/**
	 * @author Anil @since 03-03-2021
	 * Adding feedback url in service completion sms
	 */
	public String getFeedbackFormUrl(Service service,Customer customer){
		/**
		 * @author Anil @since 03-03-2021
		 * Adding feedback form url on service completion sms
		 * Raised by Nitin Sir for Pecopp
		 */
		String feedbackFormUrl="";
		
		if(LoginPresenter.company!=null&&LoginPresenter.company.getFeedbackUrlList()!=null&&LoginPresenter.company.getFeedbackUrlList().size()!=0){
//			ProcessConfiguration processConfig=null;
//			if(LoginPresenter.globalProcessConfig!=null&&LoginPresenter.globalProcessConfig.size()!=0){
//				for(ProcessConfiguration obj:LoginPresenter.globalProcessConfig){
//					if(obj.getProcessName().equals("ZohoFeedbackForm")&&obj.isConfigStatus().equals(true)){
//						processConfig=obj;
//						break;
//					}
//				}
//			}
//			if(processConfig!=null){
//				for(ProcessTypeDetails obj:processConfig.getProcessList()){
//					if(obj.isStatus()==true){
//						feedbackFormUrl=obj.getProcessType();
//					}
//				}
//			}
			if(customer!=null){
				if(customer.getCategory()!=null&&!customer.getCategory().equals("")){
					for(FeedbackUrlAsPerCustomerCategory feedback:LoginPresenter.company.getFeedbackUrlList()){
						if(feedback.getCustomerCategory().equals(customer.getCategory())){
							feedbackFormUrl=feedback.getFeedbackUrl();
							break;
						}
					}
				}
			}
			if(feedbackFormUrl!=null&&!feedbackFormUrl.equals("")){
				Console.log("ZOHO FEEDBACK URL " + feedbackFormUrl);
				StringBuilder sbPostData= new StringBuilder(feedbackFormUrl);
		        sbPostData.append("?customerId="+service.getPersonInfo().getCount());
		        sbPostData.append("&customerName="+service.getPersonInfo().getFullName());
		        sbPostData.append("&serviceId="+service.getCount());
		        sbPostData.append("&serviceDate="+AppUtility.parseDate(service.getServiceDate()));
		        sbPostData.append("&serviceName="+service.getProductName());
		        sbPostData.append("&technicianName="+service.getEmployee());
		        sbPostData.append("&branch="+service.getBranch());
		        feedbackFormUrl = sbPostData.toString();
		        feedbackFormUrl=feedbackFormUrl.replaceAll("\\s", "%20");
		        Console.log("FINAL ZOHO FEEDBACK URL " + feedbackFormUrl);
				actualMsg=actualMsg.replace("{Link}", feedbackFormUrl);
			}
		}
		return feedbackFormUrl;
	}
	
	private void setEmailPopUpData() {
		
		if(!model.getServiceBranch().equalsIgnoreCase("Service Address")){
			getCustomerBranchDetails(model.getServiceBranch(),"Email");
		}
		else{
			setEmailAllData(null);
		}
		
		
		
	}

	private void getCustomerBranchDetails(String serviceBranch, final String popupType) {

		MyQuerry querry=new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(c.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("cinfo.count");
		temp.setIntValue(model.getPersonInfo().getCount());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("status");
		temp.setBooleanvalue(true);
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("buisnessUnitName");
		temp.setStringValue(serviceBranch);
		filtervec.add(temp);

		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerBranchDetails());
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<ContactPersonIdentification> contactpersonlist = new ArrayList<ContactPersonIdentification>();
				for(SuperModel model : result){
					CustomerBranchDetails customerBranch = (CustomerBranchDetails) model;
					if(popupType.trim().equals("Email")){
						if(customerBranch.getEmail()!=null && !customerBranch.getEmail().equals("")){
							ContactPersonIdentification contactperson = new ContactPersonIdentification();
							contactperson.setName(customerBranch.getBusinessUnitName());
							contactperson.setEmail(customerBranch.getEmail());
							contactpersonlist.add(contactperson);
							if(emailpopup!=null && customerBranch.getEmail()!=null)
							emailpopup.taToEmailId.setValue(customerBranch.getEmail());

						}
					}
					else{
						if(customerBranch.getCellNumber1()!=0){
							ContactPersonIdentification contactperson = new ContactPersonIdentification();
							contactperson.setName(customerBranch.getBusinessUnitName());
							contactperson.setCell(customerBranch.getCellNumber1());
							contactpersonlist.add(contactperson);
						}
					}
					
					
				}
			
				if(popupType.trim().equals("Email")){
					setEmailAllData(contactpersonlist);
				}
				else{
					loadSMSPopUpData(contactpersonlist);
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				
			}
		});
	}
	
	public void setEmailAllData(final ArrayList<ContactPersonIdentification> contactpersonlist){
		
		form.showWaitSymbol();
		String customerEmail = "";

		String branchName = form.tbBranch.getValue();
		String fromEmailId = AppUtility.getFromEmailAddress(branchName,model.getEmployee());
		customerEmail = AppUtility.getCustomerEmailid(model.getPersonInfo().getCount());
		
		String customerName = model.getPersonInfo().getFullName();
		if(!customerName.equals("") && !customerEmail.equals("")){
			label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getPersonInfo().getCount(),customerName,customerEmail,contactpersonlist);
			emailpopup.taToEmailId.setValue(customerEmail);
		}
		else{
	
			MyQuerry querry = AppUtility.getcustomerQuerry(model.getPersonInfo().getCount());
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					for(SuperModel smodel : result){
						Customer custEntity  = (Customer) smodel;
						System.out.println("customer Name = =="+custEntity.getFullname());
						System.out.println("Customer Email = == "+custEntity.getEmail());
						
						if(custEntity.getEmail()!=null){
							label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getPersonInfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),contactpersonlist);
							emailpopup.taToEmailId.setValue(custEntity.getEmail());
						}
						else{
							label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getPersonInfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),contactpersonlist);
						}
						break;
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
				}
			});
		}
		
		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		Console.log("screenName "+screenName);
		AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,AppConstants.SERVICE,screenName);
		emailpopup.taFromEmailId.setValue(fromEmailId);
		emailpopup.smodel = model;
		form.hideWaitSymbol();
		
	}
	
	@Override
	public void reactOnSMS() {
		
		smspopup.showPopUp();
		
		if(!model.getServiceBranch().equalsIgnoreCase("Service Address")){
			getCustomerBranchDetails(model.getServiceBranch(),"SMS");
		}
		else{
			loadSMSPopUpData(null);
		}
		
	}

	private void loadSMSPopUpData(ArrayList<ContactPersonIdentification> contactpersonlist) {
			form.showWaitSymbol();
			String customerCellNo = model.getPersonInfo().getCellNumber()+"";

			String customerName = model.getPersonInfo().getFullName();
			if(!customerName.equals("") && !customerCellNo.equals("")){
				AppUtility.loadContactPersonlist(smspopup.olbContactlistEmailId,"Customer",model.getPersonInfo().getCount(),customerName,customerCellNo,contactpersonlist,true);
			}
			Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
			Console.log("screenName "+screenName);
			AppUtility.makeLiveSmsTemplateConfig(smspopup.oblsmsTemplate,AppConstants.SERVICE,screenName,true,AppConstants.SMS);
			smspopup.getRbSMS().setValue(true);
			smspopup.smodel = model;
			form.hideWaitSymbol();
	
	}
	


	
	@Override
	public void reactOnCommunicationLog() {
		
		form.showWaitSymbol();
		MyQuerry querry = AppUtility.communicationLogQuerry(model.getCount(), model.getCompanyId(),AppConstants.SERVICEMODULE,AppConstants.CUSTOMERSERVICE);

		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				ArrayList<InteractionType> list = new ArrayList<InteractionType>();
				
				for(SuperModel model : result){
					
					InteractionType interactionType = (InteractionType) model;
					list.add(interactionType);
				}
				form.hideWaitSymbol();
				Comparator<InteractionType> comp=new Comparator<InteractionType>() {
					@Override
					public int compare(InteractionType e1, InteractionType e2) {
						Integer ee1=e1.getCount();
						Integer ee2=e2.getCount();
						return ee2.compareTo(ee1);
						
					}
				};
				Collections.sort(list,comp);
				/**
				 * End
				 */
				CommunicationLogPopUp.getRemark().setValue("");
				CommunicationLogPopUp.getDueDate().setValue(null);
				CommunicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
				CommunicationLogPopUp.getCommunicationLogTable().getDataprovider().setList(list);
				
				LoginPresenter.communicationLogPanel = new PopupPanel(true);
				LoginPresenter.communicationLogPanel.add(CommunicationLogPopUp);
				LoginPresenter.communicationLogPanel.show();
				LoginPresenter.communicationLogPanel.center();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}
		});
		
	}
	
	
	private void runRegularDownloadProgram() {
		// TODO Auto-generated method stub
		final CsvServiceAsync async=GWT.create(CsvService.class);
		ArrayList<Service> custarray=new ArrayList<Service>();
		final List<Service> list=form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		
		custarray.addAll(list); 
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceBillingDetailMapingProcess")){
			mapService.mapBillingDetails(custarray, new AsyncCallback<ArrayList<Service>>() {
				
				@Override
				public void onSuccess(ArrayList<Service> result) {
					// TODO Auto-generated method stub
					
					async.setservice(result, new AsyncCallback<Void>() {

						@Override
						public void onFailure(Throwable caught) {
							System.out.println("RPC call Failed"+caught);
							
						}

						@Override
						public void onSuccess(Void result) {
							
							/**
							 * nidhi
							 * 16-4-2018
							 * for service list custom report download using process configration
							 * 
							 */
							
							if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceReport")){
								String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
								final String url = gwt + "csvservlet" + "?type=" + 137;
								Window.open(url, "test", "enabled");
							}else{
								String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
								final String url=gwt + "csvservlet"+"?type="+5;
								Window.open(url, "test", "enabled");
							}
						}
					});
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}else{
//			async.setservice(custarray, new AsyncCallback<Void>() {
//
//				@Override
//				public void onFailure(Throwable caught) {
//					System.out.println("RPC call Failed"+caught);
//					
//				}
//
//				@Override
//				public void onSuccess(Void result) {
//					
//					/**
//					 * nidhi
//					 * 16-4-2018
//					 * for service list custom report download using process configration
//					 * 
//					 */
//					
//					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceReport")){
//						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
//						final String url = gwt + "csvservlet" + "?type=" + 137;
//						Window.open(url, "test", "enabled");
//					}else{
//						String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
////						final String url=gwt + "csvservlet"+"?type="+5;
////						Window.open(url, "test", "enabled");
//						
////						final String url=gwt + "CreateXLXSServlet"+"?type="+13;
////						Window.open(url, "test", "enabled");
//						
//						
//						int columnCount = 43;
//						/**
//						 * nidhi
//						 */
//						Long companyId = (long) 0 ;
//						
//						if(list.size()>0)
//							companyId = list.get(0).getCompanyId();
//						
//						boolean complainSeviceTatFlag = false;
//						boolean serviceDurationFlag = false;
//						boolean showHideSerialNo = false;
//						
//						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ComplainServiceWithTurnAroundTime")){
//							complainSeviceTatFlag=true;
//							columnCount +=7;
//						}
//						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICEMODULE, AppConstants.PC_ADDSERVICEDURATIONCOLUMN)){
//							serviceDurationFlag=true;
//							columnCount++;
//						}
//
//						 /**
//						 * nidhi
//						 * 9-08-2018
//						 * for map serial no
//						 */
//						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("HVASC", "HideOrShowModelNoAndSerialNoDetails")){
//							showHideSerialNo = true;
//							columnCount +=2; 
//						}
//						
//						ArrayList<String> servicelist = new ArrayList<String>();
//						servicelist.add("SERVICE ID");
//						servicelist.add("SERVICE NUMBER");
//						servicelist.add("CONTRACT ID");
//						servicelist.add("REF NO.1");
//						servicelist.add("REF NO.2");
//						servicelist.add("CONTRACT START DATE");
//						servicelist.add("CONTRACT END DATE");
//						servicelist.add("CUSTOMER ID");
//						servicelist.add("CUSTOMER NAME");
//						servicelist.add("CUSTOMER CELL NO.1");
//
////						/*
////						 * Date:22/08/2018
////						 * Developer:Ashwini
////						 * Des:To add contact Details in customer Service list
////						 */
////						servicelist.add("CUSTOMER CELL NO.2");
////						servicelist.add("LANDLINE NO");
////						servicelist.add("Email Id");
//						
//						/*
//						 * End by Ashwini
//					    */
//						servicelist.add("ADDRESS LINE 1");
//						servicelist.add("ADDRESS LINE 2");
//						servicelist.add("LANDMARK");
//						servicelist.add("COUNTRY");
//						servicelist.add("STATE");
//						servicelist.add("CITY");
//						servicelist.add("LOCALITY");
//						servicelist.add("PIN");
//						servicelist.add("PRODUCT ID");
//						servicelist.add("PRODUCT CODE");
//						servicelist.add("PRODUCT NAME");
//						servicelist.add("SERVICE DATE");
//						servicelist.add("SERVICE TIME");
//
//						
//						if(serviceDurationFlag){
//							servicelist.add("SERVICE DURATION");
//						}
//						servicelist.add("SERVICE DAY");
//						servicelist.add("SERVICE COMPLETION DATE");
//
//						/*
//						 * Date:14/07/2018
//						 * Developer:Ashwini
//						 */
//						servicelist.add("SERVICE COMPLETION REMARK");
//						/*
//					    * End by Ashwini
//					    */
//						servicelist.add("SERVICE BRANCH");
//						/**
//						 *  nidhi
//						 *  4-12-2017
//						 */
//						servicelist.add("SERVICE TYPE");
//						servicelist.add("SERVICE ENGINEER");
//						servicelist.add("TEAM");
//						servicelist.add("REASON FOR CANCELLATION");
//						servicelist.add("QUANTITY");
//						servicelist.add("PREMISES");
//						servicelist.add("UOM");
//						servicelist.add("PER UNIT PRICE");
//
//						/**
//						 * Date : 30-11-2017 BY ANIL
//						 * add service value column
//						 * 
//						 */
//						servicelist.add("SERVICE VALUE");
//						/**
//						 * End
//						 */
//						servicelist.add("BRANCH");
//						servicelist.add("STATUS");
//						servicelist.add("TECHNICIANS");
//						servicelist.add("COST CENTER");
//
//				        /**
//				         * Date 30-4-2018
//				         * By jayshree
//				         * Des.Add date And Time And Status column
//				         */
//						servicelist.add("STARTED");
//						servicelist.add("REPORTED");
//						servicelist.add("COMPLETED");//COMPLETED BY TECHNICIAN
//				        //End
//				       
//						
//						if(showHideSerialNo){
//							servicelist.add("Product Model No.");
//							servicelist.add("Product Serial No.");
//						}
//						/**
//						 * end
//						 */
//						
//						/**
//						 * @author Anil
//						 * @since 06-06-2020
//						 */
//						if(complainSeviceTatFlag){
//							servicelist.add("TIER NAME");
//							servicelist.add("ASSET ID");
//							servicelist.add("COMPONENT NAME");
//							servicelist.add("MFG. NO.");
//							servicelist.add("MFG. DATE");
//							servicelist.add("REPLACEMENT DATE");
//							servicelist.add("UNIT");
//						}
//						
//						/**
//						 * end
//						 */
//						
//						for(Service s:list)
//						{
//							servicelist.add(s.getCount()+"");
//							servicelist.add(s.getServiceSerialNo()+"");
//							servicelist.add(s.getContractCount()+"");
//							/**
//							 * Date:24-01-2017 By ANil
//							 */
//							if(s.getRefNo()!=null){
//								servicelist.add(s.getRefNo()+"");
//							}else{
//								servicelist.add("");
//							}
//							
//							if(s.getRefNo2()!=null){
//								servicelist.add(s.getRefNo2()+"");
//							}else{
//								servicelist.add("");
//							}
//							/**
//							 * End
//							 */
//							
//							if(s.getContractStartDate()!=null){
//								servicelist.add(AppUtility.parseDate(s.getContractStartDate(),"dd-MMM-yyyy"));
//
//							}else{
//								servicelist.add("");
//							}
//							
//							if(s.getContractEndDate()!=null){
//								servicelist.add(AppUtility.parseDate(s.getContractEndDate(),"dd-MMM-yyyy"));
//
//							}else{
//								servicelist.add("");
//							}
//							servicelist.add(s.getCustomerId()+"");
//							servicelist.add(s.getCustomerName());
//
//							if(s.getCellNumber()!=null && s.getCellNumber()!=0){
//								servicelist.add(s.getCellNumber()+"");
//							}
//							else{
//								servicelist.add("");
//							}
//							
//							if(s.getAddrLine1()!=null){
//								servicelist.add(s.getAddrLine1());
//							}
//							else{
//								servicelist.add("");
//							}
//							
//							if(s.getAddrLine2()!=null){
//								servicelist.add(s.getAddrLine2());
//							}
//							else{
//								servicelist.add("");
//							}
//							
//							if(s.getLandmark()!=null){
//								servicelist.add(s.getLandmark());
//							}
//							else{
//								servicelist.add("");
//							}
//							if(s.getCountry()!=null){
//								servicelist.add(s.getCountry());
//							}else{
//								servicelist.add("");
//							}
//							
//							if(s.getState()!=null){
//								servicelist.add(s.getState());
//							}else{
//								servicelist.add("");
//							}
//						
//							if(s.getCity()!=null){
//								servicelist.add(s.getCity());
//							}else{
//								servicelist.add("");
//							}
//							if(s.getLocality()!=null){
//								servicelist.add(s.getLocality());
//							}
//							else{
//								servicelist.add("");
//							}
//							servicelist.add(s.getPin()+"");
//
//							if(s.getProduct()!=null){
//								servicelist.add(s.getProduct().getCount()+"");
//							}else{
//								servicelist.add("");
//							}
//							
//							if(s.getProduct()!=null&&s.getProduct().getProductCode()!=null){
//								servicelist.add(s.getProduct().getProductCode()+"");
//							}else{
//								servicelist.add("");
//							}
//							
//							if(s.getProduct()!=null&&s.getProduct().getProductName()!=null){
//								servicelist.add(s.getProduct().getProductName()+"");
//							}else{
//								servicelist.add("");
//							}
//							if(s.getServiceDate()!=null){
//								servicelist.add(AppUtility.parseDate(s.getServiceDate(),"dd-MMM-yyyy"));
//							}else{
//								servicelist.add("");
//							}
//							if(s.getServiceTime()!=null){
//								servicelist.add(s.getServiceTime());
//							}else{
//								servicelist.add("");
//							}
//							
//							if(serviceDurationFlag){
//								if(s.getServiceDuration()!=0){
//									servicelist.add(s.getServiceDuration()+"");
//								}else{
//									servicelist.add("");
//								}
//							}
//							
//							if(s.getServiceDateDay()!=null){
//								servicelist.add(s.getServiceDateDay());
//							}else{
//								servicelist.add("");
//							}
//							
//							if(s.getServiceCompletionDate()!=null)
//							{
//								servicelist.add(AppUtility.parseDate(s.getServiceCompletionDate(),"dd-MMM-yyyy"));
//							}
//							else
//							{
//								servicelist.add("NA");
//							}
//							
//							/*
//							 * Date:14/07/2018
//							 * Developer:Ashwini
//							 */
//							if(s.getServiceCompleteRemark()!=null)
//							{
//								servicelist.add(s.getServiceCompleteRemark());
//							}
//							else  {
//								servicelist.add("");
//							}
//							//end by Ashwini
//							
//							if(s.getServiceBranch()!=null){
//								servicelist.add(s.getServiceBranch());
//							}
//							else{
//								servicelist.add("");
//							}
//							/**
//							 *  nidhi
//							 *  4-12-2017
//							 *  for display service type
//							 */
//							if(s.getServiceType()!=null){
//								servicelist.add(s.getServiceType());
//							}
//							else{
//								servicelist.add("");
//							}
//							/**
//							 * end
//							 */
//							if(s.getEmployee()!=null){
//								servicelist.add(s.getEmployee());
//							}else{	
//								servicelist.add("N.A");
//							}
//							if(s.getTeam()!=null && !s.getTeam().equals("")) 
//								servicelist.add(s.getTeam());
//							else	
//								servicelist.add("N.A");
//							
//							if(s.getReasonForChange()!=null&&!s.getReasonForChange().equals("")){
//								servicelist.add(s.getReasonForChange());
//							}else{	
//								servicelist.add("");
//							}
//							
//							if(s.getQuantity()!=0){
//								servicelist.add(s.getQuantity()+"");
//							}else{
//								servicelist.add("");
//							}
//							//Ashwini Patil Date:22-07-2022 Adding Premises column as per Nitin sir's instruction
//							if(s.getPremises()!=null&&!s.getPremises().equals("")){
//								servicelist.add(s.getPremises());
//							}else{
//								servicelist.add("");
//							}
//							
//							if(s.getUom()!=null){
//								servicelist.add(s.getUom());
//							}else{
//								servicelist.add("");
//							}
//							/** date 16.02.2018 added by komal for area wise billing**/
//							if(s.getPerUnitPrice()!=0){
//								servicelist.add(s.getPerUnitPrice()+"");
//							}else{
//								servicelist.add("");
//							}
//							/**
//							 * Date : 30-11-2017 BY ANIL
//							 */
//							if(s.getServiceValue()!=0){
//								servicelist.add(s.getServiceValue()+"");
//							}else{
//								servicelist.add("");
//							}
//							/**
//							 * End
//							 */
//							if(s.getBranch()!=null){
//								servicelist.add(s.getBranch());
//							}else{
//								servicelist.add("");
//							}
//							
//							if(s.getStatus()!=null){
//								servicelist.add(s.getStatus());
//							}else{
//								servicelist.add("");
//							}
//							/**
//							 * Date : 06-11-2017 BY ANIL
//							 * Added technician info in service
//							 * for NBHC
//							 */
//							String technicians="";
//							if(s.getTechnicians()!=null){
//								for(EmployeeInfo object:s.getTechnicians()){
//									technicians=technicians+object.getFullName()+"/";
//								}
//								try{
//									if(!technicians.equals("")){
//										technicians=technicians.substring(0,technicians.length()-1);
//									}
//								}catch(Exception e){
//									
//								}
//							}
//							servicelist.add(technicians);
//							/**
//							 * End
//							 */
//							/** date 17.02.2018 added by komal for area wise billing**/
//					        if(s.getCostCenter()!=null){
//								servicelist.add(s.getCostCenter());
//					        }else{
//								servicelist.add("");
//					        }
//					        /**
//					         * Date 30-4-2018
//					         * By Jayshree
//					         * Des.add the stated time,reported time ,completion by technician 
//					         */
//					        
//					        String starttime=null;
//					        String reportedtime=null;
//					        String completedbytch=null;
//					        if(s.getTrackServiceTabledetails().size()!=0){
//					        	
//					        	for (int i = 0; i <s.getTrackServiceTabledetails().size(); i++) {
//									if(s.getTrackServiceTabledetails().get(i).getStatus().equalsIgnoreCase("Started")){
//										starttime=s.getTrackServiceTabledetails().get(i).getDate_time();
//									}
//									if(s.getTrackServiceTabledetails().get(i).getStatus().equalsIgnoreCase("Reported")){
//										reportedtime=s.getTrackServiceTabledetails().get(i).getDate_time();
//									}
//									
//									if(s.getTrackServiceTabledetails().get(i).getStatus().equalsIgnoreCase("Completed")){//Completed By Technician
//										completedbytch=s.getTrackServiceTabledetails().get(i).getDate_time();
//									}
//								}
//					        	}
//					        
//					        if(starttime!=null){
//								servicelist.add(starttime);
//					        }
//					        else{
//								servicelist.add("");
//					        }
//					        if(reportedtime!=null){
//								servicelist.add(reportedtime);
//					        }
//					        else{
//								servicelist.add("");
//					        }
//					        if(completedbytch!=null){
//								servicelist.add(completedbytch);
//					        }
//					        else{
//								servicelist.add("");
//					        }
//					        //End By jayshree
//					        /**
//							 * nidhi
//							 * 9-08-2018
//							 * for map serial no
//							 */
//							if(showHideSerialNo){
//								servicelist.add(s.getProModelNo());
//								servicelist.add(s.getProSerialNo());
//							}
//							/**
//							 * end
//							 */
//							
//							/**
//							 * @author Anil
//							 * @since 06-06-2020
//							 */
//							if(complainSeviceTatFlag){
//								servicelist.add(s.getProSerialNo());
//								servicelist.add(s.getTierName());
//								servicelist.add(s.getAssetId()+"");
//								servicelist.add(s.getComponentName());
//								servicelist.add(s.getMfgNo());
//								if(s.getMfgDate()!=null){
//									servicelist.add(AppUtility.parseDate(s.getMfgDate(),"dd-MMM-yyyy"));
//								}
//								else{
//									servicelist.add("");
//								}
//								if(s.getReplacementDate()!=null){
//									servicelist.add(AppUtility.parseDate(s.getReplacementDate(),"dd-MMM-yyyy"));
//								}
//								else{
//									servicelist.add("");
//								}
//								if(s.getAssetUnit()!=null&&!s.getAssetUnit().equals("")) {
//									servicelist.add(s.getAssetUnit());
//								}
//								else{
//									servicelist.add("");
//								}
//							}
//						}
//						
//						async.setExcelData(servicelist, columnCount, AppConstants.SERVICE, new AsyncCallback<String>() {
//							
//							@Override
//							public void onSuccess(String result) {
//								
//								String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//								final String url=gwt + "CreateXLXSServlet"+"?type="+13;
//								Window.open(url, "test", "enabled");
//							}
//							
//							@Override
//							public void onFailure(Throwable caught) {
//								// TODO Auto-generated method stub
//								
//							}
//						});
//						
//					}
//				}
//			});
//		}
		async.setservice(custarray, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				
				/**
				 * nidhi
				 * 16-4-2018
				 * for service list custom report download using process configration
				 * 
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceReport")){
					String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url = gwt + "csvservlet" + "?type=" + 137;
					Window.open(url, "test", "enabled");
				}else{
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+5;
					Window.open(url, "test", "enabled");
				}
				
			
			}
		});
		}
	}

	
	

}
