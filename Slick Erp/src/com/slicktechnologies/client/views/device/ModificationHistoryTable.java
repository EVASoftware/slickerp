package com.slicktechnologies.client.views.device;

import java.util.Date;

import com.google.gwt.cell.client.DateCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.ModificationHistory;

public class ModificationHistoryTable extends SuperTable<ModificationHistory> {

	TextColumn<ModificationHistory> username;
	TextColumn<ModificationHistory> oldServiceDate;
	TextColumn<ModificationHistory> resheduleDate;
	TextColumn<ModificationHistory> systemDate;
	TextColumn<ModificationHistory> reason;
	
	TextColumn<ModificationHistory> resheduleTime;
	TextColumn<ModificationHistory> resheduleBranch;
	
	
	@Override
	public void createTable() {
	addColumnUsername();
	addColumnmodificationHistory();
	addColumnreshduleDate();
//	addColumnsystemDate();
	addColumnresheduleTime();
	addColumnresheduleBranch();
	addColumnreason();
	/**Date 18-4-2020 by Amol shift the "system date" column to last raised by Nitin Sir**/
	addColumnsystemDate();
	}

	public void addColumnUsername()
	{
		username=new TextColumn<ModificationHistory>() {
			
			@Override
			public String getValue(ModificationHistory object) {
				return object.getUserName();
			}
		};table.addColumn(username,"Username");
	}
	
	public void addColumnmodificationHistory()
	{
		
		oldServiceDate=new TextColumn<ModificationHistory>() {

			@Override
			public String getValue(ModificationHistory object) {
				if(object==null)
					return "";
				return AppUtility.parseDate(object.oldServiceDate);
			}
		};
		table.addColumn(oldServiceDate,"Old Service Date");
	}
	
	public void addColumnsystemDate()
	{
		
		systemDate=new TextColumn<ModificationHistory>() {
			String pattern="yyyy.MM.dd HH:mm:ss";
			DateTimeFormat format=DateTimeFormat.getFormat(pattern);

			@Override
			public String getValue(ModificationHistory object) {
				if(object==null)
					return "";
				if(object.systemDate!=null)
				{
					return format.format(object.systemDate);
					
				}
				else
					return "N.A";
			}
		};
		table.addColumn(systemDate,"System Date");
	}
	
	private void addColumnresheduleBranch() {
		

		resheduleBranch=new TextColumn<ModificationHistory>() {
			@Override
			public String getValue(ModificationHistory object) {
				return object.getResheduleBranch();
			}
		};
		table.addColumn(resheduleBranch,"Service Branch");
	
		
		
	}

	private void addColumnresheduleTime() {
		resheduleTime=new TextColumn<ModificationHistory>() {
			@Override
			public String getValue(ModificationHistory object) {
				return object.getResheduleTime();
			}
		};
		table.addColumn(resheduleTime,"Service Time");
	}
	
	public void addColumnreshduleDate()
	{
		
		resheduleDate=new TextColumn<ModificationHistory>() {

			@Override
			public String getValue(ModificationHistory object) {
				if(object==null)
					return "";
				// TODO Auto-generated method stub
				return AppUtility.parseDate(object.resheduleDate);
			}
		};
         table.addColumn(resheduleDate,"New Service Date");
	}
	
	public void addColumnreason()
	{
		reason=new TextColumn<ModificationHistory>() {
			

			@Override
			public String getValue(ModificationHistory object) {
				// TODO Auto-generated method stub
				if(object==null)
					return "";
				return object.reason;
			}
		};
		
		table.addColumn(reason,"Reason");
	}
	
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
