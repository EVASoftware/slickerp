package com.slicktechnologies.client.views.device;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.slicktechnologies.shared.Device;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.paymentlayer.Payment;

public class DeviceTable extends SuperTable<Device>
{
	Column<Device,String> deviceName;
	Column<Device,Number> deviceId;



	public DeviceTable()
	{
		super();
	}
	public DeviceTable(UiScreen<Device> view)
	{
		super(view);	
	}

	public void createTable()
	{
		createColumndeviceNameColumn();
		createColumndeviceIdColumn();
		


		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);		 
	}

	protected void createColumndeviceNameColumn()
	{
		EditTextCell editCell=new EditTextCell();
		deviceName=new Column<Device,String>(editCell)
				{
			@Override
			public String getValue(Device object)
			{
				return object.getDeviceName()+"";
			}
				};
				table.addColumn(deviceName,"Device Name");
	}
	protected void createColumndeviceIdColumn()
	{
		NumberCell editCell=new NumberCell();
		deviceId=new Column<Device,Number>(editCell)
				{
			@Override
			public Number getValue(Device object)
			{
				if(object.getDeviceId()==0){
					return 0;}
				else{
					return object.getDeviceId();}
			}
				};
				table.addColumn(deviceId,"Device Id");
	}
	

	@Override public void addFieldUpdater() 
	{


	}



	public void addColumnSorting(){

		createSortingdeviceIdColumn();
		createSortingdeviceNameColumn();

	}

	protected void createSortingdeviceNameColumn(){
		List<Device> list=getDataprovider().getList();
		columnSort=new ListHandler<Device>(list);
		columnSort.setComparator(deviceName, new Comparator<Device>()
				{
			@Override
			public int compare(Device e1,Device e2)
			{
				if(e1!=null && e2!=null)
				{if( e1.getDeviceName()!=null && e2.getDeviceName()!=null){
					return e1.getDeviceName().compareTo(e2.getDeviceName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void createSortingdeviceIdColumn(){
		List<Device> list=getDataprovider().getList();
		columnSort=new ListHandler<Device>(list);
		columnSort.setComparator(deviceId, new Comparator<Device>()
				{
			@Override
			public int compare(Device e1,Device e2)
			{
				if(e1!=null && e2!=null)
				{if(e1.getDeviceId()== e2.getDeviceId()){return 0;}if(e1.getDeviceId()> e2.getDeviceId()){return 1;}else{return -1;}}else{return 0;}}
				});
		table.addColumnSortHandler(columnSort);
	}
	

	@Override
	public void setEnable(boolean state)
	{

	}

	@Override
	public void applyStyle()
	{

	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<Device>()
				{
			@Override
			public Object getKey(Device item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
}
