package com.slicktechnologies.client.views.device;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;


@Embed
public class CatchTraps extends SuperModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4898293488229086530L;
	int count;
	String pestName; // store commodity name in case of fumigation
	/** date 26.3.2019 added by komal to add location (station no.) **/
	String location;
	String containerSize;
	String containerNo;
	String catchLevel;
	//    getters and setters 
	
	
	public int getCount() {
		return count;
	}
public String getCatchLevel() {
		return catchLevel;
	}
	public void setCatchLevel(String catchLevel) {
		this.catchLevel = catchLevel;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getPestName() {
		return pestName;
	}
	public void setPestName(String pestName) {
		this.pestName = pestName;
	}
	
	Boolean status;
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public CatchTraps(){
		super();
		pestName = "";
		location = "";
		containerSize = "";
		containerNo = "";
		status = false;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}

	public String getContainerSize() {
		return containerSize;
	}
	public void setContainerSize(String containerSize) {
		this.containerSize = containerSize;
	}
	public String getContainerNo() {
		return containerNo;
	}
	public void setContainerNo(String containerNo) {
		this.containerNo = containerNo;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	
}
