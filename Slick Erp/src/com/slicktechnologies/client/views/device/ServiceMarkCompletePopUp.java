package com.slicktechnologies.client.views.device;


import java.util.Date;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.datepicker.client.DateBox;
import com.slicktechnologies.client.utility.AppUtility;

public class ServiceMarkCompletePopUp extends AbsolutePanel {
	

	IntegerBox serviceTime;
	TextArea remark;
	Button btnCancel, btnOk;
	ListBox lbRating;
	
	
	private ListBox p_servicehours;
	private ListBox p_servicemin;
	private ListBox p_ampm;
	
	private ListBox to_servicehours;
	private ListBox to_servicemin;
	private ListBox to_ampm;
	
	DateBox serviceCompletionDate;
	/** date 26.02.2018 added by komal for return qunatity popup in schedule popup **/
	Button btnOne,btnTwo;
	DoubleBox returnQuantity;

	/**
	 *  nidhi
	 *   ||*
	 *   11-10-2018
	 */
	private ListBox billingIdList;
	InlineLabel billLbl= new InlineLabel("Billing Id");
	/**
	 * end
	 */

	public ServiceMarkCompletePopUp(){

		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy"); 
		btnCancel = new Button("Cancel");
		btnOk = new Button("Ok");
		
		
		InlineLabel remark1 = new InlineLabel("Service Mark Complete");
		add(remark1, 60, 10);
		remark1.getElement().getStyle().setFontSize(20, Unit.PX);

		InlineLabel sertime =null;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "OnlyForNBHC")){
			sertime= new InlineLabel("* Service Duration (In Minutes)");
		}
		else{
			sertime = new InlineLabel("Service Duration (In Minutes)");
		}
		add(sertime, 10, 50);
		
		serviceTime = new IntegerBox();
		add(serviceTime , 240, 50);
		serviceTime.setMaxLength(4);
		
		InlineLabel hhtime=new InlineLabel("From Time HH");
		add(hhtime,00,80);
		InlineLabel coluntime=new InlineLabel(":");
		add(coluntime,90,80);
		InlineLabel mmtime=new InlineLabel("MM");
		add(mmtime,100,80);
		
		p_servicehours=new ListBox();
		add(p_servicehours,10,100);
		p_servicehours.setSize("45px", "24px");
		p_servicehours.insertItem("--", 0);
		/**
		 * Updated By: Viraj
		 * Date: 13-12-2018
		 * Description: Needed 1-12 in timings instead of 0-11
		 */
		p_servicehours.insertItem("01", 1);
		p_servicehours.insertItem("02", 2);
		p_servicehours.insertItem("03", 3);
		p_servicehours.insertItem("04", 4);
		p_servicehours.insertItem("05", 5);
		p_servicehours.insertItem("06", 6);
		p_servicehours.insertItem("07", 7);
		p_servicehours.insertItem("08", 8);
		p_servicehours.insertItem("09", 9);
		p_servicehours.insertItem("10", 10);
		p_servicehours.insertItem("11", 11);
		p_servicehours.insertItem("12", 12);
		/** Ends **/
		InlineLabel colon=new InlineLabel(":");
		add(colon,65,100);
		
		p_servicemin=new ListBox();
		add(p_servicemin,70,100);
		p_servicemin.setSize("45px", "24px");
		p_servicemin.insertItem("--", 0);
		p_servicemin.insertItem("00", 1);
		p_servicemin.insertItem("15", 2);
		p_servicemin.insertItem("30", 3);
		p_servicemin.insertItem("45", 4);
		
		
		
		
		p_ampm = new ListBox();
		add(p_ampm,120,100);
		p_ampm.setSize("55px", "24px");
		p_ampm.insertItem("--", 0);
		p_ampm.insertItem("AM", 1);
		p_ampm.insertItem("PM", 2);
		
		
		

		InlineLabel tohhtime=new InlineLabel("To Time HH");
		add(tohhtime,190,80);
		InlineLabel tocoluntime=new InlineLabel(":");
		add(tocoluntime,260,80);
		InlineLabel tommtime=new InlineLabel("MM");
		add(tommtime,265,80);
		
		to_servicehours=new ListBox();
		add(to_servicehours,190,100);
		to_servicehours.setSize("45px", "24px");
		to_servicehours.insertItem("--", 0);
		/**
		 * Updated By: Viraj
		 * Date: 13-12-2018
		 * Description: Needed 1-12 in timings instead of 0-11
		 */
		to_servicehours.insertItem("01", 1);
		to_servicehours.insertItem("02", 2);
		to_servicehours.insertItem("03", 3);
		to_servicehours.insertItem("04", 4);
		to_servicehours.insertItem("05", 5);
		to_servicehours.insertItem("06", 6);
		to_servicehours.insertItem("07", 7);
		to_servicehours.insertItem("08", 8);
		to_servicehours.insertItem("09", 9);
		to_servicehours.insertItem("10", 10);
		to_servicehours.insertItem("11", 11);
		to_servicehours.insertItem("12", 12);
		/** Ends **/
		InlineLabel tocolon=new InlineLabel(":");
		add(tocolon,260,100);
		
		to_servicemin=new ListBox();
		add(to_servicemin,270,100);
		to_servicemin.setSize("45px", "24px");
		to_servicemin.insertItem("--", 0);
		to_servicemin.insertItem("00", 1);
		to_servicemin.insertItem("15", 2);
		to_servicemin.insertItem("30", 3);
		to_servicemin.insertItem("45", 4);
		
		
		
		
		to_ampm = new ListBox();
		add(to_ampm,320,100);
		to_ampm.setSize("55px", "24px");
		to_ampm.insertItem("--", 0);
		to_ampm.insertItem("AM", 1);
		to_ampm.insertItem("PM", 2);
		//       
		
		
		
		InlineLabel rating = new InlineLabel("Customer Feedback");
		add(rating, 10, 140);
		lbRating = new ListBox();
		lbRating.addItem("--SELECT--");
		lbRating.addItem("Excellent");
		lbRating.addItem("Very Good");
		lbRating.addItem("Good");
		//Ashwini Patil Date:20-1-2022 changing value to average as in Customer portal and in zoho API Satisfactory rating is not mapped
		lbRating.addItem("Average");//old value Satisfactory 
		lbRating.addItem("Poor");

		add(lbRating, 10, 160);
		
		
//	    rohan added service completion date for back dated records
		
			InlineLabel serCompDt = new InlineLabel("Service Completion Date");
			add(serCompDt, 150, 140);
			
			serviceCompletionDate =new DateBox();
			serviceCompletionDate.setValue(new Date());

			add(serviceCompletionDate, 160, 160);
			serviceCompletionDate.setFormat(new DateBox.DefaultFormat(dateFormat));

			/**
			 *  nidhi
			 *  11-10-2018
			 *   ||*
			 */

		
			
			add(billLbl,10,190);
			billingIdList= new ListBox();
			billingIdList.addItem("--SELECT--");
			add(billingIdList,10,210);
			
			/**
			 * end
			 */
		InlineLabel Remark = new InlineLabel("Remark");
		add(Remark, 10, 240);
		remark = new TextArea();
		add(remark, 10, 260);

		remark.setSize("250px", "30px");

		add(btnOk, 80, 330);
		add(btnCancel, 180, 330);
		setSize("420px", "420px");
		this.getElement().setId("form");
	}
	/** date 26.02.2018 added by komal for return qunatity popup in schedule popup **/
	public  ServiceMarkCompletePopUp(boolean flag){

		btnOne=new Button("Ok");
		btnTwo=new Button("Cancel");
		
		InlineLabel displayMessage ;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MINBasedOnConsumedQuantity"))//Ashwini Patil Date:27-06-2023
			displayMessage = new InlineLabel("Consumed quantity");
		else
			displayMessage = new InlineLabel("Return quantity");
					 
			add(displayMessage,10,10);
			
			returnQuantity = new DoubleBox();
			add(returnQuantity,10,30);
			
			
			btnOne.getElement().getStyle().setWidth(50, Unit.PX);
			btnTwo.getElement().getStyle().setWidth(60, Unit.PX);
			
			add(btnOne,30,65);
			add(btnTwo,120,65); 
			setSize("220px", "110px");
			this.getElement().setId("form");
	
	}

	/** date 13.3.2019  added by komal for qunatity popup in schedule popup **/
	public  ServiceMarkCompletePopUp(boolean flag , String value){

		btnOne=new Button("Ok");
		btnTwo=new Button("Cancel");
		
	
			InlineLabel displayMessage = new InlineLabel(value);
			add(displayMessage,10,10);
			
			returnQuantity = new DoubleBox();
			add(returnQuantity,10,30);
			
			
			btnOne.getElement().getStyle().setWidth(50, Unit.PX);
			btnTwo.getElement().getStyle().setWidth(60, Unit.PX);
			
			add(btnOne,30,65);
			add(btnTwo,120,65); 
			setSize("220px", "110px");
			this.getElement().setId("form");
	
	}

	public void clear(){
		serviceTime.setValue(null);
		remark.setValue(null);
		lbRating.setSelectedIndex(0);
	}
		

//		****************getter and setters*****************
		
		public IntegerBox getServiceTime() {
			return serviceTime;
		}


		public void setServiceTime(IntegerBox serviceTime) {
			this.serviceTime = serviceTime;
		}


		public TextArea getRemark() {
			return remark;
		}


		public void setRemark(TextArea remark) {
			this.remark = remark;
		}


		public Button getBtnCancel() {
			return btnCancel;
		}


		public void setBtnCancel(Button btnCancel) {
			this.btnCancel = btnCancel;
		}


		public Button getBtnOk() {
			return btnOk;
		}


		public void setBtnOk(Button btnOk) {
			this.btnOk = btnOk;
		}

		public ListBox getLbRating() {
			return lbRating;
		}

		public void setLbRating(ListBox lbRating) {
			this.lbRating = lbRating;
		}

		public DateBox getServiceCompletionDate() {
			return serviceCompletionDate;
		}

		public void setServiceCompletionDate(DateBox serviceCompletionDate) {
			this.serviceCompletionDate = serviceCompletionDate;
		}

		public ListBox getP_servicehours() {
			return p_servicehours;
		}

		public void setP_servicehours(ListBox p_servicehours) {
			this.p_servicehours = p_servicehours;
		}

		public ListBox getP_servicemin() {
			return p_servicemin;
		}

		public void setP_servicemin(ListBox p_servicemin) {
			this.p_servicemin = p_servicemin;
		}

		public ListBox getP_ampm() {
			return p_ampm;
		}

		public void setP_ampm(ListBox p_ampm) {
			this.p_ampm = p_ampm;
		}

		public ListBox getTo_servicehours() {
			return to_servicehours;
		}

		public void setTo_servicehours(ListBox to_servicehours) {
			this.to_servicehours = to_servicehours;
		}

		public ListBox getTo_servicemin() {
			return to_servicemin;
		}

		public void setTo_servicemin(ListBox to_servicemin) {
			this.to_servicemin = to_servicemin;
		}

		public ListBox getTo_ampm() {
			return to_ampm;
		}

		public void setTo_ampm(ListBox to_ampm) {
			this.to_ampm = to_ampm;
		}
		
		public Button getBtnOne() {
			return btnOne;
		}
		public void setBtnOne(Button btnOne) {
			this.btnOne = btnOne;
		}
		public Button getBtnTwo() {
			return btnTwo;
		}
		public void setBtnTwo(Button btnTwo) {
			this.btnTwo = btnTwo;
		}
		public DoubleBox getReturnQuantity() {
			return returnQuantity;
		}
		public void setReturnQuantity(DoubleBox returnQuantity) {
			this.returnQuantity = returnQuantity;
		}
		public ListBox getBillingIdList() {
			return billingIdList;
		}
		public void setBillingIdList(ListBox billingIdList) {
			this.billingIdList = billingIdList;
		}

}
