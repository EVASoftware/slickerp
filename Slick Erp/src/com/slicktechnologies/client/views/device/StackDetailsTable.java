package com.slicktechnologies.client.views.device;
import java.util.List;
import java.util.logging.Logger;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.service.StackDetails;

/*
 * This table is created on 08-Sep-2016, because of NBHC requirements in SERVICEFORM
 */
public class StackDetailsTable extends SuperTable<StackDetails>{

	Logger logger = Logger.getLogger("StackDetailsTable.class");
	/*
	 * Field to be added in Columns.
	 */
	TextColumn<StackDetails> wareHouseColumn;
	TextColumn<StackDetails> storageLocationColumn;
	TextColumn<StackDetails> stackNoColumn;
	TextColumn<StackDetails> quantityColumn;
	TextColumn<StackDetails> decriptionColumn;

	Column<StackDetails, String> delete;
	
	/**** Date 26-04-2019 by Vijay NBHC CCPM for Stack selection ***/
	Column<StackDetails, Boolean> checkColumn;
	Header<Boolean> selectAllHeader;
	
	TextColumn<StackDetails> warehouseCode;
	TextColumn<StackDetails> warehouseShade;
	
	public StackDetailsTable() {
		super();
	}

	@Override
	public void createTable() {
		// TODO Auto-generated method stub
//		addCheckBoxCell();
		addwareHosueColumn();
		addwarehouseCodeColumn();
		addwarehouseShade();
		addstorageLocationColumn();
		addstackNoColumn();
		addquantity();
		addDescription();
//		addColumnDelete();
//		addFieldUpdater();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}

	

	private void addwarehouseShade() {
		warehouseShade = new TextColumn<StackDetails>() {
			
			@Override
			public String getValue(StackDetails object) {
				// TODO Auto-generated method stub
				return object.getShade();
			}
		};
		table.addColumn(warehouseShade, "Shade");
		table.setColumnWidth(warehouseShade, 100,Unit.PX);

	}

	private void addwarehouseCodeColumn() {
		warehouseCode = new TextColumn<StackDetails>() {
			
			@Override
			public String getValue(StackDetails object) {
				// TODO Auto-generated method stub
				return object.getWarehouseCode();
			}
		};
		table.addColumn(warehouseCode, "Warehouse Code");
		table.setColumnWidth(warehouseCode, 100,Unit.PX);

	}

	private void addquantity() {
		// TODO Auto-generated method stub
		Console.log("iNSIDE addquantity");
		quantityColumn = new TextColumn<StackDetails>() {

			@Override
			public String getValue(StackDetails object) {
				// TODO Auto-generated method stub
				return object.getQauntity() + "";
			}
		};
		table.addColumn(quantityColumn, "Quantity");
		table.setColumnWidth(quantityColumn, 90,Unit.PX);
		quantityColumn.setSortable(true);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
//		setFieldUpdaterOnDelete();
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		 int tablecolcount = this.table.getColumnCount();
		 for (int i = tablecolcount - 1; i > -1; i--)
		 table.removeColumn(i);
		 
		 if (state){
			 addeditColumn();
		 }else{
			 addViewColumn();
		 }
		 
		 
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub

	}

	private void addwareHosueColumn() {
		wareHouseColumn = new TextColumn<StackDetails>() {

			@Override
			public String getValue(StackDetails object) {
				return object.getWareHouse();
			}
		};
		table.addColumn(wareHouseColumn, "Warehouse");
		table.setColumnWidth(wareHouseColumn, 100,Unit.PX);

	}

	/*
	 * This method will add storageLocation in StackDetailsTable.
	 */
	private void addstorageLocationColumn() {
		// TODO Auto-generated method stub
		Console.log("Inside addstorageLocationColumn");
		storageLocationColumn = new TextColumn<StackDetails>() {

			@Override
			public String getValue(StackDetails object) {
				// TODO Auto-generated method stub
				return object.getStorageLocation();
			}
		};
		table.addColumn(storageLocationColumn, "Compartment");
		table.setColumnWidth(storageLocationColumn, 100,Unit.PX);
		storageLocationColumn.setSortable(true);
	}

	/*
	 * This method will add stackNo in StackDetailsTable
	 */
	private void addstackNoColumn() {
		// TODO Auto-generated method stub
		Console.log("Inside addstackNoColumn");
		stackNoColumn = new TextColumn<StackDetails>() {

			@Override
			public String getValue(StackDetails object) {
				// TODO Auto-generated method stub
				return object.getStackNo();
			}
		};
		table.addColumn(stackNoColumn, "Stack No");
		table.setColumnWidth(stackNoColumn, 80,Unit.PX);
		stackNoColumn.setSortable(true);

	}

	/*
	 * Added to provide description in SERVICE for individual stack in
	 * storageLocation.
	 */
	private void addDescription() {
		// TODO Auto-generated method stub
		Console.log("Inside addDescription");
		decriptionColumn = new TextColumn<StackDetails>() {

			@Override
			public String getValue(StackDetails object) {
				// TODO Auto-generated method stub
				return object.getDescription();
			}
		};
		table.addColumn(decriptionColumn, "Commodity Name");
		table.setColumnWidth(decriptionColumn, 130,Unit.PX);
		decriptionColumn.setSortable(true);

	}

	/*
	 * Added because if these details is filled manually and they added wrong
	 * details, then they can edit it.
	 */
//	private void addColumnDelete() {
//		// TODO Auto-generated method stub
//		Console.log("Inside addColumnDelete");
//		ButtonCell btnCell = new ButtonCell();
//		delete = new Column<StackDetails, String>(btnCell) {
//			@Override
//			public String getValue(StackDetails object) {
//
//				return "Delete";
//			}
//		};
//		table.addColumn(delete, "Delete");
////		table.setColumnWidth(delete, 150,Unit.PX);
//	}

	/*
	 * This method updates the table when delete button is clicked
	 */
//	private void setFieldUpdaterOnDelete() {
//		// TODO Auto-generated method stub
//		Console.log("Inside setFieldUpdaterOnDelete");
//		delete.setFieldUpdater(new FieldUpdater<StackDetails, String>() {
//
//			@Override
//			public void update(int index, StackDetails object, String value)
//			{
//				getDataprovider().getList().remove(index);
//				table.redrawRow(index);
//			}
//		});
//	}

	/*
	 * This is added to perform action on Form, when form is in EDIT state
	 */
	private void addeditColumn() {
		Console.log("Inside Edit Colummns");
//		addCheckBoxCell();
		addwareHosueColumn();
		addstorageLocationColumn();
		addstackNoColumn();
		addquantity();
		addDescription();
//		try{
//		addColumnDelete();
//		}catch(Exception e){
//			e.printStackTrace();
//			Console.log(""+e);
//		}
		addFieldUpdater();
	}

	/*
	 * This is added to perform action on Form, when form is in VIEW state
	 */
	private void addViewColumn() {
		Console.log("Inside View Colummns");
		addwareHosueColumn();
		addstorageLocationColumn();
		addstackNoColumn();
		addquantity();
		addDescription();
	}
	
	/**** Date 26-04-2019 by Vijay NBHC CCPM for Stack selection ***/

//private void addCheckBoxCell() {
//
//		
//		checkColumn=new Column<StackDetails, Boolean>(new CheckboxCell()) {
//			@Override
//			public Boolean getValue(StackDetails object) {
//				return object.isRecordSelect();
//			}
//		};
//		
//		checkColumn.setFieldUpdater(new FieldUpdater<StackDetails, Boolean>() {
//			@Override
//			public void update(int index, StackDetails object, Boolean value) {
//				System.out.println("Check Column ....");
//				object.setRecordSelect(value);
//				selectAllHeader.getValue();
//				table.redrawHeaders();
//				
//				
//			
//			}
//		});
//		
//		
//		selectAllHeader =new Header<Boolean>(new CheckboxCell()) {
//			@Override
//			public Boolean getValue() {
//				if(getDataprovider().getList().size()!=0){
//					
//				}
//				return false;
//			}
//		};
//		
//		
//		selectAllHeader.setUpdater(new ValueUpdater<Boolean>() {
//			@Override
//			public void update(Boolean value) {
//				List<StackDetails> list=getDataprovider().getList();
//				for(StackDetails object:list){
//					object.setRecordSelect(value);
//				}
//				getDataprovider().setList(list);
//				table.redraw();
//				addColumnSorting();
//			}
//		});
//		table.addColumn(checkColumn,selectAllHeader);
//		table.setColumnWidth(checkColumn,40,Unit.PX);
//
//			
//	}
}
