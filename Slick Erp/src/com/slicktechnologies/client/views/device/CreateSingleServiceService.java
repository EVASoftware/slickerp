package com.slicktechnologies.client.views.device;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;

@RemoteServiceRelativePath("createSingleService")
public interface CreateSingleServiceService extends RemoteService{

	ArrayList<Integer> createSingleService(int serviceCount,Date previousDate,int serviceNo,int serviceSerialNo,int contractId,long companyId);
	ArrayList<Customer>createSinglecustomer(int serviceCount,ArrayList<Integer> custId,long companyId);//Added by Ashwini
}
