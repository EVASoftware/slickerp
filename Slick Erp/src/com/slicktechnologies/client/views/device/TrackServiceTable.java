package com.slicktechnologies.client.views.device;

import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;

public class TrackServiceTable extends SuperTable<TrackTableDetails>{


	public TextColumn<TrackTableDetails> srNo;
	public TextColumn<TrackTableDetails> serviceId;
	public TextColumn<TrackTableDetails> date_time;
	public TextColumn<TrackTableDetails> status;
	/**
	 * Updated By: Viraj
	 * Date: 01-03-2019
	 * Description: To show lat and long
	 */
	public TextColumn<TrackTableDetails> lat;
	public TextColumn<TrackTableDetails> lon;
	/** Ends **/
	/**
	 * Updated By: komal
	 * Date: 01-04-2019
	 * Description: To show location
	 */
	public TextColumn<TrackTableDetails> location;
	
	public TextColumn<TrackTableDetails> km;
	public TrackServiceTable() {
		// TODO Auto-generated constructor stub
		setHeight("350px");
	}

	// creating all table
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		addColumngetsrNO();
		addColumngetdate_time();
		addColumngetStatus();
		/**
		 * Updated By: Viraj
		 * Date: 01-03-2019
		 * Description: To show lat and long
		 */
		addColumngetLat();
		addColumngetLon();
		/**
		 * Updated By: komal
		 * Date: 01-04-2019
		 * Description: To show location
		 */
		addColumngetLocation();
		addColumngetkm();
	}

	/**
	 * Updated By: Viraj
	 * Date: 01-03-2019
	 * Description: To show lat and long
	 */
	private void addColumngetLon() {
		lon=new TextColumn<TrackTableDetails>() {

			@Override
			public String getValue(TrackTableDetails object) {
				// TODO Auto-generated method stub
				return object.getLongitude();
			}
		};
		table.addColumn(lon, "Longitude");
		table.setColumnWidth(lon, 40, Unit.PX);
		
	}

	private void addColumngetLat() {
		lat=new TextColumn<TrackTableDetails>() {

			@Override
			public String getValue(TrackTableDetails object) {
				// TODO Auto-generated method stub
				return object.getLatitude();
			}
		};
		table.addColumn(lat, "Latitude");
		table.setColumnWidth(lat, 40, Unit.PX);
		
	}
	/** Ends **/
	private void addColumngetStatus() {
		// TODO Auto-generated method stub
		status=new TextColumn<TrackTableDetails>() {

			@Override
			public String getValue(TrackTableDetails object) {
				// TODO Auto-generated method stub
				return object.getStatus();
			}
		};
		table.addColumn(status, "Status");
		table.setColumnWidth(status, 30, Unit.PX);
		
	}

	private void addColumngetdate_time() {
		// TODO Auto-generated method stub
		date_time=new TextColumn<TrackTableDetails>() {

			@Override
			public String getValue(TrackTableDetails object) {
				// TODO Auto-generated method stub
				return object.getDate_time();
			}
		};
		table.addColumn(date_time, "Date and Time");
		table.setColumnWidth(date_time, 30, Unit.PX);
		
	}

	private void addColumngetsrNO() {
		// TODO Auto-generated method stub
		srNo = new TextColumn<TrackTableDetails>() {

			@Override
			public String getValue(TrackTableDetails object) {
				// TODO Auto-generated method stub
				return object.getSrNo()+ "";
			}
		};
		table.addColumn(srNo, "SrNo");
		table.setColumnWidth(srNo, 30, Unit.PX);
	}
	
	/**
	 * Updated By: komal
	 * Date: 01-04-2019
	 * Description: To show location
	 */
	private void addColumngetLocation(){
		
			location=new TextColumn<TrackTableDetails>() {

				@Override
				public String getValue(TrackTableDetails object) {
					// TODO Auto-generated method stub
					if(object.getLocationName() != null){
						return object.getLocationName();
					}
					return "";
				}
			};
			table.addColumn(location, "Location");
			table.setColumnWidth(location, 40, Unit.PX);
			
	}

	private void addColumngetkm(){
		
		km=new TextColumn<TrackTableDetails>() {

			@Override
			public String getValue(TrackTableDetails object) {
				// TODO Auto-generated method stub
					return object.getKiloMeters()+"";
			
			}
		};
		table.addColumn(km, "km");
		table.setColumnWidth(km, 40, Unit.PX);
		
}
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub

	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub

	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub

	}
}
