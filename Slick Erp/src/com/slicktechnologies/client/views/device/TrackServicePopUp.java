package com.slicktechnologies.client.views.device;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;

public class TrackServicePopUp extends AbsolutePanel {


	TextBox serviceId,date_Time,status;
	
	TrackServiceTable trackServTable;
	
	public TrackServicePopUp() {
		// TODO Auto-generated constructor stub
		
		trackServTable =new TrackServiceTable();
		trackServTable.connectToLocal();
		
		//Heading of Pop Up
		InlineLabel remark1 = new InlineLabel("Service Details");
		add(remark1,160,10);
		remark1.getElement().getStyle().setFontSize(20, Unit.PX);

		
		add(trackServTable.getTable(),10,40);
		setSize("650px", "400px");			//Updated by viraj Date: 02-03-2019
		this.getElement().setId("form");
	}
	
	

	public TextBox getServiceId() {
		return serviceId;
	}

	public void setServiceId(TextBox serviceId) {
		this.serviceId = serviceId;
	}

	public TextBox getDate_Time() {
		return date_Time;
	}

	public void setDate_Time(TextBox date_Time) {
		this.date_Time = date_Time;
	}

	public TextBox getStatus() {
		return status;
	}

	public void setStatus(TextBox status) {
		this.status = status;
	}
}
