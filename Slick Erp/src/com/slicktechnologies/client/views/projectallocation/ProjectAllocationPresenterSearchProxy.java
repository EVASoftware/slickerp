package com.slicktechnologies.client.views.projectallocation;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.views.projectallocation.ProjectAllocationPresenter.ProjectAllocationPresenterSearch;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;

public class ProjectAllocationPresenterSearchProxy extends ProjectAllocationPresenterSearch{
	
	public PersonInfoComposite personInfo;
	public IntegerBox tbContractId;
	public IntegerBox tbProjectId;
	ObjectListBox<EmployeeInfo> olbSuperVisorInfo;
	ObjectListBox<EmployeeInfo> olbManagerInfo;
	 /**23-11-2018  added by amol projectobjectlistbox**/
	
	ObjectListBox<ProjectAllocation> project;
	
	
	/**
	 * @author Abhinav 
	 * @since 10/10/2019
	 * sasha
	 */
	ObjectListBox<Branch> branch;
	
	public ProjectAllocationPresenterSearchProxy() {
		super();
		// TODO Auto-generated constructor stub
		createGui();
	}
	
	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
//		super.createScreen();
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		builder = new FormFieldBuilder("Contract Id",tbContractId);
		FormField ftbContractId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Project Id",tbProjectId);
		FormField ftbProjectId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Supervisor",olbSuperVisorInfo);
		FormField folbSuperVisorInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Manager",olbManagerInfo);
		FormField folbManagerInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder=new FormFieldBuilder("Project Name",project);
		FormField fproject=builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * @author Abhinav
		 * @since 10/10/2019
		 * sasha
		 */
		builder=new FormFieldBuilder("Branch",branch);
		FormField fbranch=builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder();
		FormField fgroupingCustomerDetails=builder.setlabel("Customer Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		builder = new FormFieldBuilder();
		FormField fgroupingProjectDetails=builder.setlabel("Project Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		this.fields=new FormField[][]{
				{fgroupingCustomerDetails},
				{fpersonInfo},
				{fgroupingProjectDetails},
				{ftbContractId,ftbProjectId,folbSuperVisorInfo,folbManagerInfo},
				{fproject,fbranch}
		};
		
	}
	
	@Override
	public MyQuerry getQuerry() {
		// TODO Auto-generated method stub
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		if(olbSuperVisorInfo.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbSuperVisorInfo.getValue().trim());
			temp.setQuerryString("supervisorName");
			filtervec.add(temp);
		}
		if(olbManagerInfo.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbManagerInfo.getValue().trim());
			temp.setQuerryString("managerName");
			filtervec.add(temp);
		}
		if(personInfo.getIdValue()!=-1){
		  temp=new Filter();
		  temp.setIntValue(personInfo.getIdValue());
		  temp.setQuerryString("personInfo.count");
		  filtervec.add(temp);
		}
		if(!(personInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(personInfo.getFullNameValue());
		  temp.setQuerryString("personInfo.fullName");
		  filtervec.add(temp);
		  }
		  if(personInfo.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(personInfo.getCellValue());
		  temp.setQuerryString("personInfo.cellNumber");
		  filtervec.add(temp);
		  }
		  
		  if(tbContractId.getValue()!=null){
				temp=new Filter();
				temp.setIntValue(tbContractId.getValue());
				temp.setQuerryString("contractId");
				filtervec.add(temp);
		 }
		  if(tbProjectId.getValue()!=null){
				temp=new Filter();
				temp.setIntValue(tbProjectId.getValue());
				temp.setQuerryString("count");
				filtervec.add(temp);
		 }
		  if(project.getValue()!=null){
				temp=new Filter();
				temp.setStringValue(project.getValue());
				temp.setQuerryString("projectName");
				filtervec.add(temp);
		 }
		  /**
		   * @author Abhinav
		   * @since 10/101/2019
		   * sasha
		   */
		  if(branch.getValue()!=null){
				temp=new Filter();
				temp.setStringValue(branch.getValue());
				temp.setQuerryString("branch");
				filtervec.add(temp);
		 }
		  
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProjectAllocation());
		return querry;
	}

	private void initWidget() {
		// TODO Auto-generated method stub
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		tbContractId=new IntegerBox();
		tbProjectId=new IntegerBox();
		olbSuperVisorInfo=new ObjectListBox<EmployeeInfo>();
		EmployeeInfo.makeObjectListBoxLive(olbSuperVisorInfo);
		olbSuperVisorInfo=new ObjectListBox<EmployeeInfo>();
		EmployeeInfo.makeObjectListBoxLive(olbSuperVisorInfo);
		olbManagerInfo=new ObjectListBox<EmployeeInfo>();
		EmployeeInfo.makeObjectListBoxLive(olbManagerInfo);

		project = new ObjectListBox<ProjectAllocation>();
		MyQuerry q1=new MyQuerry();
		q1.setQuerryObject(new ProjectAllocation());
		project.MakeLive(q1);
		
		branch= new ObjectListBox<Branch>();
		MyQuerry q2=new MyQuerry();
		q2.setQuerryObject(new Branch());
		branch.MakeLive(q2);
	}
	
	
}
