package com.slicktechnologies.client.views.projectallocation;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.projectallocation.ProjectAllocationPresenter.ProjectAllocationPresenterTable;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.cnc.CNC;

public class ProjectAllocationPresenterTableProxy extends
		ProjectAllocationPresenterTable {
	TextColumn<ProjectAllocation> customerCellNumberColumn;
	TextColumn<ProjectAllocation> customerFullNameColumn;
	TextColumn<ProjectAllocation> customerIdColumn;
	TextColumn<ProjectAllocation> contractCountColumn;
	TextColumn<ProjectAllocation> projectCountColumn;
	TextColumn<ProjectAllocation> projectStartDateColumn;
	TextColumn<ProjectAllocation> projectEndDateColumn;
	TextColumn<ProjectAllocation> projectSupervisorColumn;
	TextColumn<ProjectAllocation> projectManagerColumn;
	TextColumn<ProjectAllocation> projectNameColumn;
	/**
	 * @author Abhinav
	 * @since 10/10/2019
	 * sasha
	 */
	TextColumn<ProjectAllocation> branchColumn;

	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		addprojectCountColumn();
		addprojectNameColumn();
		addbranchColumn();
		addprojectStartDateColumn();
		addprojectEndDateColumn();
		addcontractCountColumn();
		addcustomerIdColumn();
		addcustomerFullNameColumn();
		addcustomerCellNumberColumn();
		addprojectSupervisorColumn();
		addprojectManagerColumn();
	}
	
	@Override
	public void addColumnSorting() {
		// TODO Auto-generated method stub
		addSortingprojectCountColumn();
		addSortingprojectNameColumn();
		addSortingbranchColumn();
		addSortingprojectStartDateColumn();
		addSortingprojectEndDateColumn();
		addSortingcontractCountColumn();
		addSortingcustomerIdColumn();
		addSortingcustomerFullNameColumn();
		addSortingcustomerCellNumberColumn();
		addSortingprojectSupervisorColumn();
		addSortingprojectManagerColumn();
		
	}

	/**
	 * @author Abhinav
	 * @since 10/10/2019
	 * sasha
	 */
	private void addbranchColumn() {
		// TODO Auto-generated method stub
		branchColumn = new TextColumn<ProjectAllocation>(){

			@Override
			public String getValue(ProjectAllocation object) {
				// TODO Auto-generated method stub
				return object.getBranch();
			}
		};
		table.addColumn(branchColumn,"Branch");
		table.setColumnWidth(branchColumn,130, Unit.PX);
		branchColumn.setSortable(true);
		
	}

	private void addprojectNameColumn() {
		// TODO Auto-generated method stub
		projectNameColumn = new TextColumn<ProjectAllocation>() {

			@Override
			public String getValue(ProjectAllocation object) {
				// TODO Auto-generated method stub
				return object.getProjectName();
			}

		};

		table.addColumn(projectNameColumn, "Project Name");
		table.setColumnWidth(projectNameColumn, 130, Unit.PX);
		projectNameColumn.setSortable(true);
	}

	private void addprojectManagerColumn() {
		// TODO Auto-generated method stub
		projectManagerColumn = new TextColumn<ProjectAllocation>() {

			@Override
			public String getValue(ProjectAllocation object) {
				// TODO Auto-generated method stub
				if(object.getManagerName()!=null){
					return object.getManagerName();
				}else {
					return "";
				}
				
			}

		};

		table.addColumn(projectManagerColumn, "Manager Name");
		table.setColumnWidth(projectManagerColumn, 130, Unit.PX);
		projectManagerColumn.setSortable(true);
	}

	private void addprojectSupervisorColumn() {
		// TODO Auto-generated method stub
		projectSupervisorColumn = new TextColumn<ProjectAllocation>() {

			@Override
			public String getValue(ProjectAllocation object) {
				// TODO Auto-generated method stub
				if(object.getSupervisorName()!=null){
					return object.getSupervisorName();
				}else{
					return "";
					}
			}

		};

		table.addColumn(projectSupervisorColumn, "Supervisor Name");
		table.setColumnWidth(projectSupervisorColumn, 130, Unit.PX);
		projectSupervisorColumn.setSortable(true);
	}

	private void addprojectEndDateColumn() {
		// TODO Auto-generated method stub
		projectEndDateColumn = new TextColumn<ProjectAllocation>() {

			@Override
			public String getValue(ProjectAllocation object) {
				// TODO Auto-generated method stub
				if(object.getEndDate()!=null){
					return AppUtility.parseDate(object.getEndDate());
				}else{
					return "";
				}
				
			}

		};

		table.addColumn(projectEndDateColumn, "End Date");
		table.setColumnWidth(projectEndDateColumn, 130, Unit.PX);
		projectEndDateColumn.setSortable(true);
	}

	private void addprojectStartDateColumn() {
		// TODO Auto-generated method stub
		projectStartDateColumn = new TextColumn<ProjectAllocation>() {

			@Override
			public String getValue(ProjectAllocation object) {
				// TODO Auto-generated method stub
				if(object.getStartDate()!=null){
					return AppUtility.parseDate(object.getStartDate());
				}
				else{
					return "";
				}
			}

		};

		table.addColumn(projectStartDateColumn, "Start Date");
		table.setColumnWidth(projectStartDateColumn, 130, Unit.PX);
		projectStartDateColumn.setSortable(true);
	}

	private void addprojectCountColumn() {
		// TODO Auto-generated method stub
		projectCountColumn = new TextColumn<ProjectAllocation>() {

			@Override
			public String getValue(ProjectAllocation object) {
				// TODO Auto-generated method stub
				return object.getCount()+"";
			}

		};

		table.addColumn(projectCountColumn, "Project Id");
		table.setColumnWidth(projectCountColumn, 130, Unit.PX);
		projectCountColumn.setSortable(true);
	}

	private void addcontractCountColumn() {
		// TODO Auto-generated method stub
		contractCountColumn = new TextColumn<ProjectAllocation>() {

			@Override
			public String getValue(ProjectAllocation object) {
				// TODO Auto-generated method stub
				return object.getContractId()+"";
			}

		};

		table.addColumn(contractCountColumn, "Contract Id");
		table.setColumnWidth(contractCountColumn, 130, Unit.PX);
		contractCountColumn.setSortable(true);
	}

	private void addcustomerIdColumn() {
		// TODO Auto-generated method stub
		customerIdColumn = new TextColumn<ProjectAllocation>() {

			@Override
			public String getValue(ProjectAllocation object) {
				// TODO Auto-generated method stub
				return object.getPersonInfo().getCount()+"";
			}

		};

		table.addColumn(customerIdColumn, "Customer Id");
		table.setColumnWidth(customerIdColumn, 130, Unit.PX);
		customerIdColumn.setSortable(true);
	}

	private void addcustomerFullNameColumn() {
		// TODO Auto-generated method stub
		customerFullNameColumn = new TextColumn<ProjectAllocation>() {

			@Override
			public String getValue(ProjectAllocation object) {
				// TODO Auto-generated method stub
				return object.getPersonInfo().getFullName();
			}

		};

		table.addColumn(customerFullNameColumn, "Customer Name");
		table.setColumnWidth(customerFullNameColumn, 130, Unit.PX);
		customerFullNameColumn.setSortable(true);
	}

	private void addcustomerCellNumberColumn() {
		// TODO Auto-generated method stub
		customerCellNumberColumn = new TextColumn<ProjectAllocation>() {

			@Override
			public String getValue(ProjectAllocation object) {
				// TODO Auto-generated method stub
				return object.getPersonInfo().getCellNumber()+"";
			}

		};

		table.addColumn(customerCellNumberColumn, "Customer Cell");
		table.setColumnWidth(customerCellNumberColumn, 130, Unit.PX);
		customerCellNumberColumn.setSortable(true);
	}

	
	
	public void addSortingprojectCountColumn() {
		List<ProjectAllocation> list=getDataprovider().getList();
		  columnSort=new ListHandler<ProjectAllocation>(list);
		  columnSort.setComparator(projectCountColumn, new Comparator<ProjectAllocation>()
		  {
		  @Override
		  public int compare(ProjectAllocation e1,ProjectAllocation e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if(e1.getCount()== e2.getCount()){
		  return 0;}
		  if(e1.getCount()> e2.getCount()){
		  return 1;}
		  else{
		  return -1;}
		  }
		  else{
		  return 0;}
		  }
		  });
		  table.addColumnSortHandler(columnSort);
		
	}
	
	public void addSortingprojectNameColumn() {
		List<ProjectAllocation> list=getDataprovider().getList();
		  columnSort=new ListHandler<ProjectAllocation>(list);
		  columnSort.setComparator(projectNameColumn, new Comparator<ProjectAllocation>()
		  {
		  @Override
		  public int compare(ProjectAllocation e1,ProjectAllocation e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getProjectName()!=null && e2.getProjectName()!=null){
		  return e1.getProjectName().compareTo(e2.getProjectName());}
		  }
		  else{
		  return 0;}
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);
	  
		
	}
    public void addSortingprojectStartDateColumn() {
    	List<ProjectAllocation> list=getDataprovider().getList();
		  columnSort=new ListHandler<ProjectAllocation>(list);
		  columnSort.setComparator(projectStartDateColumn, new Comparator<ProjectAllocation>()
		  {
		  @Override
		  public int compare(ProjectAllocation e1,ProjectAllocation e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getStartDate()!=null && e2.getStartDate()!=null){
		  return e1.getStartDate().compareTo(e2.getStartDate());}
		  }
		  else{
		  return 0;}
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);
	  
    }
	
	public void addSortingprojectEndDateColumn() {
		List<ProjectAllocation> list=getDataprovider().getList();
		  columnSort=new ListHandler<ProjectAllocation>(list);
		  columnSort.setComparator(projectEndDateColumn, new Comparator<ProjectAllocation>()
		  {
		  @Override
		  public int compare(ProjectAllocation e1,ProjectAllocation e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getEndDate()!=null && e2.getEndDate()!=null){
		  return e1.getEndDate().compareTo(e2.getEndDate());}
		  }
		  else{
		  return 0;}
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);
		
	}
	public void addSortingcontractCountColumn() {
		List<ProjectAllocation> list=getDataprovider().getList();
		  columnSort=new ListHandler<ProjectAllocation>(list);
		  columnSort.setComparator(contractCountColumn, new Comparator<ProjectAllocation>()
		  {
		  @Override
		  public int compare(ProjectAllocation e1,ProjectAllocation e2)
		  {
		if(e1!=null && e2!=null)
		  {
		  if(e1.getCount()== e2.getCount()){
		  return 0;}
		  if(e1.getCount()> e2.getCount()){
		  return 1;}
		  else{
		  return -1;}
		  }
		  else{
		  return 0;}
		  }
		  });
		  table.addColumnSortHandler(columnSort);
		
	}
	public void addSortingcustomerIdColumn() {
		List<ProjectAllocation> list=getDataprovider().getList();
		  columnSort=new ListHandler<ProjectAllocation>(list);
		  columnSort.setComparator(contractCountColumn, new Comparator<ProjectAllocation>()
		  {
		  @Override
		  public int compare(ProjectAllocation e1,ProjectAllocation e2)
		  {
		
		 if(e1!=null && e2!=null)
		  {
		  if(e1.getPersonInfo().getCount()== e2.getPersonInfo().getCount()){
		  return 0;}
		  if(e1.getPersonInfo().getCount()> e2.getPersonInfo().getCount()){
		  return 1;}
		  else{
		  return -1;}
		  }
		  else{
		  return 0;}
		  }
		  });
		  table.addColumnSortHandler(columnSort);
		  
		
	}
	    public void addSortingcustomerFullNameColumn() {
		List<ProjectAllocation> list=getDataprovider().getList();
		columnSort=new ListHandler<ProjectAllocation>(list);
		columnSort.setComparator(customerFullNameColumn, new Comparator<ProjectAllocation>()
		  {
		  @Override
		  public int compare(ProjectAllocation e1,ProjectAllocation e2)
		  {
		 
			  if(e1!=null && e2!=null)
			   {
			   if( e1.getPersonInfo().getFullName()!=null && e2.getPersonInfo().getFullName()!=null){
			   return e1.getPersonInfo().getFullName().compareTo(e2.getPersonInfo().getFullName());}
			   }
			   else{
			   return 0;}
			   return 0;
			   }
			   });
			   table.addColumnSortHandler(columnSort);
		
	}
	public void addSortingcustomerCellNumberColumn() {
		List<ProjectAllocation> list = getDataprovider().getList();
		columnSort = new ListHandler<ProjectAllocation>(list);
		columnSort.setComparator(customerCellNumberColumn,new Comparator<ProjectAllocation>() {
				@Override
				public int compare(ProjectAllocation e1,ProjectAllocation e2) {
					if (e1 != null && e2 != null) {
						if (e1.getPersonInfo().getCellNumber() != null&& e2.getPersonInfo().getCellNumber() != null) {
							return e1.getPersonInfo().getCellNumber().compareTo(e2.getPersonInfo().getCellNumber());
						}
					} else {
						return 0;
					}
					return 0;
				}
			});
		table.addColumnSortHandler(columnSort);
	}
	public void addSortingprojectSupervisorColumn() {
		List<ProjectAllocation> list=getDataprovider().getList();
		  columnSort=new ListHandler<ProjectAllocation>(list);
		  columnSort.setComparator(projectSupervisorColumn, new Comparator<ProjectAllocation>()
		  {
		  @Override
		  public int compare(ProjectAllocation e1,ProjectAllocation e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getSupervisorName()!=null && e2.getSupervisorName()!=null){
		  return e1.getSupervisorName().compareTo(e2.getSupervisorName());}
		  }
		  else{
		  return 0;}
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);
	}
	public void addSortingprojectManagerColumn() {
		List<ProjectAllocation> list=getDataprovider().getList();
		  columnSort=new ListHandler<ProjectAllocation>(list);
		  columnSort.setComparator(projectManagerColumn, new Comparator<ProjectAllocation>()
		  {
		  @Override
		  public int compare(ProjectAllocation e1,ProjectAllocation e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getManagerName()!=null && e2.getManagerName()!=null){
		  return e1.getManagerName().compareTo(e2.getManagerName());}
		  }
		  else{
		  return 0;}
		  return 0;
		  }
		  });
		  table.addColumnSortHandler(columnSort);
	}
	
	/**
	 * @author Abhinav
	 * @since 10/10/2019
	 * sasha
	 */
	public void addSortingbranchColumn() {
		List<ProjectAllocation> list=getDataprovider().getList();
		columnSort=new ListHandler<ProjectAllocation>(list);
		columnSort.setComparator(branchColumn, new Comparator<ProjectAllocation>()
		{

			@Override
			public int compare(ProjectAllocation arg0, ProjectAllocation arg1) {
				// TODO Auto-generated method stub

				  if(arg0!=null && arg1!=null)
				  {
				  if( arg0.getManagerName()!=null && arg1.getManagerName()!=null){
				  return arg0.getManagerName().compareTo(arg1.getManagerName());}
				  }
				  else{
				  return 0;}
				  return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
			
		}
		
	}