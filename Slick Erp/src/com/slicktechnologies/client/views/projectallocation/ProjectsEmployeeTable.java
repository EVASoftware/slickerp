package com.slicktechnologies.client.views.projectallocation;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;
import java.util.Comparator; 

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.core.java.util.Collections;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.popups.ProjectAllocationEmployeeUpdatePopUp;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.Consumables;
import com.slicktechnologies.shared.common.cnc.EquipmentRental;
import com.slicktechnologies.shared.common.cnc.StaffingDetails;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.EducationalInfo;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;

public class ProjectsEmployeeTable  extends SuperTable<EmployeeProjectAllocation> implements ClickHandler{

	TextColumn<EmployeeProjectAllocation> roleName;
	
	TextColumn<EmployeeProjectAllocation> employeeId;
	TextColumn<EmployeeProjectAllocation> employeeName;
	TextColumn<EmployeeProjectAllocation> shift,viewDelete;
	Column<EmployeeProjectAllocation,String> deleteButton;
	Column<EmployeeProjectAllocation,String> editEmployeeName,editShift ,designation,editEmployeeId;
	ArrayList<String> employeeList;
	ArrayList<String>employeeIdList;
	HashMap<String, EmployeeInfo> employeeInfoListMap;
	ArrayList<String> shiftList;
	HashMap<String, Shift> shiftListMap;
	/** date 15.8.2018 added by komal**/
	TextColumn<EmployeeProjectAllocation> startDate , endDate;
	/**
	 * @author Anil , Date : 22-03-2019
	 * commentong ot columns
	 */
//	TextColumn<EmployeeProjectAllocation> startDate , endDate , overtime;
//	Column<EmployeeProjectAllocation , String> editOvertime;
	Column<EmployeeProjectAllocation,Date> editStartDate , editEndDate;

	ArrayList<String> designationNameList;
	ArrayList<String> overtimeList;
	/** date 7.9.2018 added by komal**/
	Column<EmployeeProjectAllocation, Boolean> checkColumn;
	TextColumn<EmployeeProjectAllocation> extraEmployee;
	ProjectAllocationForm projectallocation;
	String branchName;
	
	/**
	 * @author Anil
	 * @since 20-08-2020
	 */
	TextColumn<EmployeeProjectAllocation> nonEditableSiteLocation;
	Column<EmployeeProjectAllocation,String> editableSiteLocation;
	int custId;
	String state;
	ArrayList<String> locationList;
	boolean siteLocationFlag=false;
	/**
	 * @author Vijay Date :- 28-01-2021
	 * Des :- added updatebutton to update employee role and site location
	 * updated this code loading issue if data having more than 300 with this change it will work smooth
	 */
	Column<EmployeeProjectAllocation,String> updateButton;
	int rowIndex=0;
	ProjectAllocationEmployeeUpdatePopUp updatepoup = new ProjectAllocationEmployeeUpdatePopUp();
	
	
	public ProjectsEmployeeTable() {
		// TODO Auto-generated constructor stub
		createGui();
		table.setWidth("100%");
		table.setHeight("350px");
		employeeList=new ArrayList<String>();
		employeeIdList=new ArrayList<String>();
		employeeInfoListMap=new HashMap<String, EmployeeInfo>();
		shiftList=new ArrayList<String>();
		shiftListMap=new HashMap<String, Shift>();
		designationNameList = new ArrayList<String>();
		overtimeList = new ArrayList<String>();
		
		locationList=new ArrayList<String>();
		siteLocationFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("ProjectAllocation", "EnableSiteLocation");
		updatepoup.getLblOk().addClickHandler(this);
		updatepoup.getLblCancel().addClickHandler(this);
	}

	public ProjectsEmployeeTable(UiScreen<EmployeeProjectAllocation> mv) {
		super(mv);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		siteLocationFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("ProjectAllocation", "EnableSiteLocation");
//		if(siteLocationFlag){
//			addEditableSiteLocationCol();
//		}else{
//			makeList();
//			addDesignation();
//			//addColumnRoleName();
//			addEditColumnEmployeeName();
//		}
		
		/**
		 * @author Vijay Date :- 28-01-2021
		 * Des :- added updatebutton to update employee role and site location on popup new design old design commented
		 * updated this code loading issue if data having more than 300 with this change it will work smooth
		 */
		
		addcolumnUpdateButton();
		if(siteLocationFlag){
			addNonEditableSiteLocation();
		}
		addColumnRoleName();
		addColumnEmployeeID();
		addColumnEmployeeName();
		addColumnShift();
		createColumnStartDateColumn();
		createColumnEndDateColumn();
		addCheckBoxCell();
		addDeleteButton();
		setFieldUpdaterOnDelete();
		
	}

//	private void addEditableSiteLocationCol() {
//		final GenricServiceAsync service = GWT.create(GenricService.class);
//		MyQuerry query = new MyQuerry();
//		Company c=new Company();
//		
//		Vector<Filter> filtervec=new Vector<Filter>();
//		Filter filter = null;
//		
//		filter = new Filter();
//		filter.setQuerryString("companyId");
//		filter.setLongValue(c.getCompanyId());
//		filtervec.add(filter);
//		
//		filter = new Filter();
//		filter.setQuerryString("cinfo.count");
//		filter.setIntValue(custId);
//		filtervec.add(filter);
//		
//		if(state!=null&&!state.equals("")){
//			filter = new Filter();
////			filter.setQuerryString("billingAddress.state");
//			filter.setQuerryString("contact.address.state");
//			filter.setStringValue(state);
//			filtervec.add(filter);
//		}
//		
//		filter = new Filter();
//		filter.setQuerryString("status");
//		filter.setBooleanvalue(true);
//		filtervec.add(filter);
//		query.setFilters(filtervec);
//		
//		query.setQuerryObject(new CustomerBranchDetails());
//		service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
//			
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				locationList.clear();
//				locationList.add("--SELECT--");
//				for (SuperModel model : result) {
//					CustomerBranchDetails location=(CustomerBranchDetails) model;
//					locationList.add(location.getBusinessUnitName());
////					shiftListMap.put(location.getShiftName(), location);
//				}
//				System.out.println("locationList:::"+locationList.size());
//				SelectionCell locLis= new SelectionCell(locationList);
//				editableSiteLocation = new Column<EmployeeProjectAllocation, String>(locLis) {
//					@Override
//					public String getValue(EmployeeProjectAllocation object) {
//						if (object.getSiteLocation() != null)
//							return object.getSiteLocation();
//						else
//							return "";
//						
//					}
//				};
//				table.addColumn(editableSiteLocation, "#Site Location");
//				table.setColumnWidth(editableSiteLocation, 120, Unit.PX);
//				
//				editableSiteLocation.setFieldUpdater(new FieldUpdater<EmployeeProjectAllocation, String>() {
//					@Override
//					public void update(int index,EmployeeProjectAllocation object, String value) {
//						if(value!=null){
//							object.setSiteLocation(value);
////							Shift shiftDetails=shiftListMap.get(value.trim());
//						}
//						table.redrawRow(index);
//					}
//				});
//				
//				
//				makeList();
//				addDesignation();
//				//addColumnRoleName();
//				addEditColumnEmployeeName();
//			}
//			
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				
//			}
//		});
//	}
	
	private void addNonEditableSiteLocation() {
		// TODO Auto-generated method stub
		nonEditableSiteLocation = new TextColumn<EmployeeProjectAllocation>() {
			
			@Override
			public String getValue(EmployeeProjectAllocation object) {
				if(object.getSiteLocation()!=null){
					return object.getSiteLocation();
				}else{
					return "";
				}
			}
		};
		table.addColumn(nonEditableSiteLocation,"Site Location");
		table.setColumnWidth(nonEditableSiteLocation, 120,Unit.PX);
	}

//	private void addEditColumnShift() {
//		// TODO Auto-generated method stub
//		final GenricServiceAsync service = GWT.create(GenricService.class);
//		MyQuerry query = new MyQuerry();
//		Company c=new Company();
//		System.out.println("Company Id :: "+c.getCompanyId());
//		
//		Vector<Filter> filtervec=new Vector<Filter>();
//		Filter filter = null;
//		
//		filter = new Filter();
//		filter.setQuerryString("companyId");
//		filter.setLongValue(c.getCompanyId());
//		filtervec.add(filter);
//		/** date 27.8.2018 added by komal to load status shiftwise**/
//		filter = new Filter();
//		filter.setQuerryString("status");
//		filter.setBooleanvalue(true);
//		filtervec.add(filter);
//		query.setFilters(filtervec);
//		
//		query.setQuerryObject(new Shift());
//		service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
//			
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				// TODO Auto-generated method stub
//				shiftList.clear();
//				shiftList.add("--SELECT--");
//				for (SuperModel model : result) {
//					Shift shift=(Shift) model;
//					shiftList.add(shift.getShiftName());
//					shiftListMap.put(shift.getShiftName(), shift);
//				}
//				System.out.println("shiftList:::"+shiftList.size());
//				SelectionCell employeeListSel= new SelectionCell(shiftList);
//				editShift = new Column<EmployeeProjectAllocation, String>(employeeListSel) {
//					@Override
//					public String getValue(EmployeeProjectAllocation object) {
//						// TODO Auto-generated method stub
//						if (object.getShift() != null)
//							return object.getShift();
//						else
//							return "";
//						
//					}
//				};
//				table.addColumn(editShift, "#Shift");
//				table.setColumnWidth(editShift, 100, Unit.PX);
//				
//				editShift.setFieldUpdater(new FieldUpdater<EmployeeProjectAllocation, String>() {
//
//					@Override
//					public void update(int index,
//							EmployeeProjectAllocation object, String value) {
//						// TODO Auto-generated method stub
//						if(value!=null){
//							object.setShift(value);
////							Shift shiftDetails=shiftListMap.get(value.trim());
//						}
//						table.redrawRow(index);
//					}
//				});
//				createColumnStartDateColumn();
//				createColumnEndDateColumn();
////				addEditOvertime();
//				addCheckBoxCell();
//				addDeleteButton();
//				setFieldUpdaterOnDelete();
//			}
//			
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				
//			}
//		});
//		
//	
//	}

//	private void addEditColumnEmployeeName() {
//		// TODO Auto-generated method stub
//		final GenricServiceAsync service = GWT.create(GenricService.class);
//		MyQuerry query = new MyQuerry();
//		Company c=new Company();
//		System.out.println("Company Id :: "+c.getCompanyId());
//		
//		Vector<Filter> filtervec=new Vector<Filter>();
//		Filter filter = null;
//		
//		filter = new Filter();
//		filter.setQuerryString("companyId");
//		filter.setLongValue(c.getCompanyId());
//		filtervec.add(filter);
//		/**
//		 * Date 17-08-2020 by Vijay
//		 * Des :- when we edit Project if Employee status is InActive then its not showing in Edit Mode
//		 * but its showing in view mode so commented below filter it should display in edit and View mode
//		 * Requirement Raised by Rahul
//		 */
////		filter = new Filter();
////		filter.setQuerryString("status");
////		filter.setBooleanvalue(true);
////		filtervec.add(filter);
//		
//		/**date 18-12-2019 by AMol**/
//		if(this.getBranchName()!=null){
//		Console.log("Employee BranchName");	
//		filter=new Filter();
//		filter.setQuerryString("branch");
//		filter.setStringValue(this.getBranchName());
//		filtervec.add(filter);
//		}
//		query.setFilters(filtervec);
//		
//		query.setQuerryObject(new EmployeeInfo());
//		service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
//			
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				// TODO Auto-generated method stub
//				employeeInfoListMap.clear();
//				employeeList.clear();
//				employeeList.add("--SELECT--");
//				
//				for (SuperModel model : result) {
//					EmployeeInfo employee=(EmployeeInfo) model;
//					/** date 3.9.2018 added by komal**/
//					/**Date 21-8-2019 by Amol do not add employee in dropdown who's Date of joining is greater than current date**/
//					Date date=new Date();
//					if(employee.isLeaveAllocated()&&employee.getDateOfJoining().before(date)){
////						employeeList.add(employee.getFullName());
////						employeeInfoListMap.put(employee.getFullName(), employee);
//						
//						/**
//						 * @author Anil
//						 * @since 17-12-2020
//						 * added employee id along with employee name
//						 * if same name appears more than once
//						 */
//						if(employeeList.contains(employee.getFullName())){
//							employeeList.add(employee.getFullName()+" / "+employee.getEmpCount());
//						}else{
//							employeeList.add(employee.getFullName());
//						}
//						
//						if(employeeInfoListMap.containsKey(employee.getFullName())){
//							employeeInfoListMap.put(employee.getFullName()+" / "+employee.getEmpCount(), employee);
//						}else{
//							employeeInfoListMap.put(employee.getFullName(), employee);
//						}
//						
//					}
//					
//				}
//				
//				/***Date 23-2-2019 added by amol for sorting ***/
//				java.util.Collections.sort(employeeList);
//				
//				System.out.println("Employee List::"+employeeList.size());
//				System.out.println("Employee List::"+employeeInfoListMap.size());
//				
//				
//				
//		         /**Date 1-8-2019 by Amol Added a Employee Id column**/
////						EditTextCell editCell = new EditTextCell();
//						employeeId = new TextColumn<EmployeeProjectAllocation>() {
//							@Override
//							public String getValue(EmployeeProjectAllocation object) {
//								if (object.getEmployeeId()!= null){
//									return object.getEmployeeId();
//								}else{
//									return "";
//								}
//							}
//
//						};
//						employeeId.setFieldUpdater(new FieldUpdater<EmployeeProjectAllocation, String>() {
//							@Override
//							public void update(int index, EmployeeProjectAllocation object,String value) {
//								if (value != null) {
//									object.setEmployeeId(value);
//									
//								}
//								table.redrawRow(index);
//							}
//						});
//						table.addColumn(employeeId,"#Employee ID");
//						table.setColumnWidth(employeeId, 110,Unit.PX);
//				
//				
//				SelectionCell employeeListSel= new SelectionCell(employeeList);
//				editEmployeeName = new Column<EmployeeProjectAllocation, String>(employeeListSel) {
//					@Override
//					public String getValue(EmployeeProjectAllocation object) {
//						// TODO Auto-generated method stub
//						if (object.getEmployeeName() != null){
//							if(employeeInfoListMap.size()!=0){
//								/**
//								 * @author Anil
//								 * @since 17-12-2020
//								 * loaded employee id from ref number first
//								 */
//								if(object.getRefName()!=null&&!object.getRefName().equals("")&&employeeInfoListMap.containsKey(object.getRefName())){
//									object.setEmployeeId(employeeInfoListMap.get(object.getRefName()).getEmpCount()+"");
//								}else if(employeeInfoListMap.containsKey(object.getEmployeeName())){
//									object.setEmployeeId(employeeInfoListMap.get(object.getEmployeeName()).getEmpCount()+"");
//								}else{
//									object.setEmployeeId("");
//								}
//							}
//							
//							if(object.getRefName()!=null&&!object.getRefName().equals("")){
//								return object.getRefName();
//							}
//							return object.getEmployeeName();
//						}
//						else
//							return "";
//						
//					}
//				};
//				table.addColumn(editEmployeeName, "#Employee");
//				table.setColumnWidth(editEmployeeName, 100, Unit.PX);
//				
//				editEmployeeName.setFieldUpdater(new FieldUpdater<EmployeeProjectAllocation, String>() {
//
//					@Override
//					public void update(int index,EmployeeProjectAllocation object, String value) {
//						// TODO Auto-generated method stub
//						if(value!=null){
//							if (value.equalsIgnoreCase("--SELECT--")) {
//								object.setEmployeeName("");
//								object.setEmployeeId("");
//								
//								object.setRefName(null);
//							} else {
////								boolean validEmp=true;
//								for(EmployeeProjectAllocation emp:getDataprovider().getList()){
//									if(emp.getEmployeeName().equals(value)){
//										EmployeeInfo employeeDetails=employeeInfoListMap.get(value.trim());
//										if(emp.getEmployeeId().equals(employeeDetails.getEmpCount()+"")){
//											GWTCAlert alert=new GWTCAlert();
//											alert.alert(value+ " already selected");
//										}
////										validEmp=false;
//										
////										GWTCAlert alert=new GWTCAlert();
////										alert.alert(value+ " already selected");
//										
////										object.setEmployeeName("");
////										object.setEmployeeId("");
//									}
//								}
////								if(validEmp){
//								
//								Console.log("SELECTED EMP "+value);
//								/***
//								 * @author Anil
//								 * @since 17-12-2020
//								 */
//								if(value.contains(" / ")){
//									String str=value;
//									String[] arrOfStr = str.split(" / ");
//									String updatedName=arrOfStr[0];
//									System.out.println(""+updatedName);
//									Console.log("updatedName "+updatedName);
//									object.setRefName(value);
//									object.setEmployeeName(updatedName);
//									EmployeeInfo employeeDetails=employeeInfoListMap.get(value.trim());
//									object.setEmployeeId(employeeDetails.getCount()+"");
//									
//								}else{
//									object.setEmployeeName(value);
//									Console.log("ELSE "+object.getEmployeeName());
//									EmployeeInfo employeeDetails=employeeInfoListMap.get(value.trim());
//									object.setEmployeeId(employeeDetails.getCount()+"");
//									object.setRefName(null);
//								}
//								
//								
//								
//								
//							}
//							
//						}
//						table.redrawRow(index);
//						table.redraw();
//					}
//				});
//              
//				addEditColumnShift();
//				
//			}
//			
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				
//			}
//		});
//		
//	}

	private void setFieldUpdaterOnDelete() {
		// TODO Auto-generated method stub
		deleteButton.setFieldUpdater(new FieldUpdater<EmployeeProjectAllocation, String>() {
			
			@Override
			public void update(int index, EmployeeProjectAllocation object, String value) {
				
				ProjectAllocationForm.deleteempprojectallocationlist.add(object);
				
				getDataprovider().getList().remove(index);
				table.redrawRow(index);
			}
		});
	}

	private void addDeleteButton() {
		// TODO Auto-generated method stub
		ButtonCell btnCell= new ButtonCell();
		deleteButton = new Column<EmployeeProjectAllocation, String>(btnCell) {

			@Override
			public String getValue(EmployeeProjectAllocation object) {
				
				return " - ";
			}
		};
		table.addColumn(deleteButton,"Delete");
		table.setColumnWidth(deleteButton, 50,Unit.PX);
	}

	private void addColumnShift() {
		// TODO Auto-generated method stub
		shift = new TextColumn<EmployeeProjectAllocation>() {
			
			@Override
			public String getValue(EmployeeProjectAllocation object) {
				return object.getShift();
			}
		};
		table.addColumn(shift,"Shift");
		table.setColumnWidth(shift, 110,Unit.PX);
	}

	private void addColumnEmployeeName() {
		// TODO Auto-generated method stub
		employeeName = new TextColumn<EmployeeProjectAllocation>() {
			
			@Override
			public String getValue(EmployeeProjectAllocation object) {
				return object.getEmployeeName();
			}
		};
		table.addColumn(employeeName,"Employee Name");
		table.setColumnWidth(employeeName, 110,Unit.PX);
	}

	private void addColumnRoleName() {
		// TODO Auto-generated method stub
		roleName = new TextColumn<EmployeeProjectAllocation>() {
			
			@Override
			public String getValue(EmployeeProjectAllocation object) {
				return object.getEmployeeRole();
			}
		};
		table.addColumn(roleName,"Role Name");
		table.setColumnWidth(roleName, 110,Unit.PX);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true){
			addcolumnUpdateButton();
			if(siteLocationFlag){
				addNonEditableSiteLocation();
//				loadSiteLocation();

			}
			addColumnRoleName();
			addColumnEmployeeID();
			addColumnEmployeeName();
			addColumnShift();
			createColumnStartDateColumn();
			createColumnEndDateColumn();
			addCheckBoxCell();
			addDeleteButton();
			setFieldUpdaterOnDelete();
			
		}
//			addeditColumn();
		if(state==false)
			addViewColumn();
	}

	private void addViewColumn() {
		// TODO Auto-generated method stub
		if(siteLocationFlag){
			addNonEditableSiteLocation();
		}
		addColumnRoleName();
		addColumnEmployeeID();
		addColumnEmployeeName();
		addColumnShift();
		createViewStartdateColumn();
		createViewEnddateColumn();
//		addViewOvertime();
		addExtraEmployeeColumn();
		addViewDeleteButton();
	}

	private void addColumnEmployeeID() {
		// TODO Auto-generated method stub
		employeeId = new TextColumn<EmployeeProjectAllocation>() {
			
			@Override
			public String getValue(EmployeeProjectAllocation object) {
				return object.getEmployeeId();
			}
		};
		table.addColumn(employeeId,"Employee ID");
		table.setColumnWidth(employeeId, 110,Unit.PX);
	}

	private void addViewDeleteButton() {
		// TODO Auto-generated method stub
		
		viewDelete= new TextColumn<EmployeeProjectAllocation>() {
			
		@Override
		public String getValue(EmployeeProjectAllocation object) {
				return "";
			}
		};
		table.addColumn(viewDelete,"Delete");
		table.setColumnWidth(viewDelete, 110,Unit.PX);
	}

//	private void addeditColumn() {
//		// TODO Auto-generated method stub
//		if(siteLocationFlag){
//			addEditableSiteLocationCol();
//		}else{
//			makeList();
//			addDesignation();
//			//addColumnRoleName();
//			addEditColumnEmployeeName();
//		}
//	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

	protected void createColumnStartDateColumn() {
		DateTimeFormat fmt = DateTimeFormat.getFormat("dd-MM-yyyy");
		DatePickerCell date = new DatePickerCell(fmt);
		editStartDate = new Column<EmployeeProjectAllocation, Date>(date) {
			@Override
			public Date getValue(EmployeeProjectAllocation object) {
				if (object.getStartDate() == null)
					return new Date();
				return object.getStartDate();
			}

		};
		editStartDate.setFieldUpdater(new FieldUpdater<EmployeeProjectAllocation, Date>() {
			@Override
			public void update(int index, EmployeeProjectAllocation object,
					Date value) {
				if (value != null) {
					object.setStartDate(value);
					System.out.println("start date"
							+ object.getStartDate());
				}
				table.redrawRow(index);
			}
		});
		table.addColumn(editStartDate,"#Start Date");
		table.setColumnWidth(editStartDate, 110,Unit.PX);

	}
	protected void createColumnEndDateColumn() {
		DateTimeFormat fmt = DateTimeFormat.getFormat("dd-MM-yyyy");
		DatePickerCell date = new DatePickerCell(fmt);
		editEndDate = new Column<EmployeeProjectAllocation, Date>(date) {
			@Override
			public Date getValue(EmployeeProjectAllocation object) {
				if (object.getEndDate() == null)
					return new Date();
				return object.getEndDate();
			}

		};
		editEndDate.setFieldUpdater(new FieldUpdater<EmployeeProjectAllocation, Date>() {
			@Override
			public void update(int index, EmployeeProjectAllocation object,
					Date value) {
				if (value != null) {
					object.setEndDate(value);
					System.out.println("end date"
							+ object.getEndDate());
				}
				table.redrawRow(index);
			}
		});
		table.addColumn(editEndDate,"#End Date");
		table.setColumnWidth(editEndDate, 110,Unit.PX);
	}
	protected void createViewStartdateColumn() {
		startDate = new TextColumn<EmployeeProjectAllocation>() {
			@Override
			public String getValue(EmployeeProjectAllocation object) {
				System.out.println("Start date"+ object.getStartDate());
					DateTimeFormat format = DateTimeFormat.getFormat("dd-MM-yyyy");
					if(object.getStartDate()!=null){
						String date = format.format(object.getStartDate());
						return date;
					}else{
						return "N.A";
					}
			}
		};
		table.addColumn(startDate,"Start Date");
		table.setColumnWidth(startDate, 110,Unit.PX);

	}
	
	protected void createViewEnddateColumn() {
		endDate = new TextColumn<EmployeeProjectAllocation>() {
			@Override
			public String getValue(EmployeeProjectAllocation object) {
				System.out.println("End Date"+ object.getEndDate());
					DateTimeFormat format = DateTimeFormat.getFormat("dd-MM-yyyy");
					if(object.getEndDate()!=null){
						String date = format.format(object.getEndDate());
						return date;
					}else{
						return "N.A";
					}
					
			}
		};
		table.addColumn(endDate,"End Date");
		table.setColumnWidth(endDate, 110,Unit.PX);

	}
	private void addDesignation() {
		// TODO Auto-generated method stub
		SelectionCell designationSelection = new SelectionCell(
				designationNameList);
		designation = new Column<EmployeeProjectAllocation, String>(designationSelection) {
			@Override
			public String getValue(EmployeeProjectAllocation object) {
				// TODO Auto-generated method stub
				if (object.getEmployeeRole() != null)
					return object.getEmployeeRole();
				else
					return "";

			}
		};
		table.addColumn(designation, "#Role Name");
		table.setColumnWidth(designation, 100, Unit.PX);

		designation.setFieldUpdater(new FieldUpdater<EmployeeProjectAllocation, String>() {
					@Override
					public void update(int index, EmployeeProjectAllocation object,
							String value) {

						if (value.equalsIgnoreCase("--SELECT--")) {
							object.setEmployeeRole("");
						} else {
							object.setEmployeeRole(value);
						}
						// getDataprovider().getList().remove(index);

						table.redrawRow(index);
					}
				});
	}

	private void makeList() {

		ArrayList<Config> designationList = new ArrayList<Config>();
		designationList = LoginPresenter.globalConfig;
		designationNameList = new ArrayList<String>();
		designationNameList.add("--SELECT--");

		if (designationList.size() != 0) {
			for (int i = 0; i < designationList.size(); i++) {
				if (designationList.get(i).getType() == 13
						&& designationList.get(i).isStatus() == true) {
					if (designationList.get(i).getName() != null) {
						try {
							designationNameList.add(designationList.get(i).getName());
						} catch (Exception e) {
							System.out.println(e);
						}
					}
				}
			}
		}
		ArrayList<Overtime> otList = new ArrayList<Overtime>();
		otList = LoginPresenter.globalOvertimeList;
		overtimeList = new ArrayList<String>();
		overtimeList.add("--SELECT--");

		if (otList.size() != 0) {
			for (int i = 0; i < otList.size(); i++) {
				if (otList.get(i).isStatus() == true) {
					if (otList.get(i).getName() != null) {
						try {
							overtimeList.add(otList.get(i)
									.getName());
						} catch (Exception e) {
							System.out.println(e);
						}
					}
				}
			}
		}

	}
//	private void addEditOvertime() {
//		// TODO Auto-generated method stub
//		SelectionCell overtimeSelection = new SelectionCell(
//				overtimeList);
//		editOvertime = new Column<EmployeeProjectAllocation, String>(overtimeSelection) {
//			@Override
//			public String getValue(EmployeeProjectAllocation object) {
//				// TODO Auto-generated method stub
//				if (object.getOvertime() != null)
//					return object.getOvertime();
//				else
//					return "";
//
//			}
//		};
//		table.addColumn(editOvertime, "#Overtime");
//		table.setColumnWidth(editOvertime, 100, Unit.PX);
//
//		editOvertime
//				.setFieldUpdater(new FieldUpdater<EmployeeProjectAllocation, String>() {
//
//					@Override
//					public void update(int index, EmployeeProjectAllocation object,
//							String value) {
//
//						if (value.equalsIgnoreCase("--SELECT--")) {
//							object.setOvertime("");
//						} else {
//							object.setOvertime(value);
//							for(Overtime ot : LoginPresenter.globalOvertimeList){
//								if(ot.getName().equals(value)){
//									object.setOtCount(ot.getCount());
//								}
//							}
//						}
//						// getDataprovider().getList().remove(index);
//
//						table.redrawRow(index);
//					}
//				});
//	}
//	private void addViewOvertime() {
//		// TODO Auto-generated method stub
//		overtime = new TextColumn<EmployeeProjectAllocation>() {
//			
//			@Override
//			public String getValue(EmployeeProjectAllocation object) {
//				if(object.getOvertime()!=null)
//					return object.getOvertime();
//				
//				return "";
//			}
//		};
//		table.addColumn(overtime,"Overtime");
//		table.setColumnWidth(overtime, 110,Unit.PX);
//	}
	private void addCheckBoxCell() {
		
		checkColumn=new Column<EmployeeProjectAllocation, Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue(EmployeeProjectAllocation object) {
				return object.isExtra();
			}
		};
		
		checkColumn.setFieldUpdater(new FieldUpdater<EmployeeProjectAllocation, Boolean>() {
			@Override
			public void update(int index, EmployeeProjectAllocation object, Boolean value) {
				System.out.println("Check Column ....");
				if(value != null){
					object.setExtra(value);
				}else{
					object.setExtra(false);
				}			
				table.redrawRow(index);			
			}
		});
		table.addColumn(checkColumn, "#Extra Employee");
		table.setColumnWidth(checkColumn, 100, Unit.PX);
	}
	
	private void addExtraEmployeeColumn() {
		
		extraEmployee=new TextColumn<EmployeeProjectAllocation>() {
			@Override
			public String getValue(EmployeeProjectAllocation object) {
				// TODO Auto-generated method stub
				if(object.isExtra()){
					return "Extra";
				}
				return "";
			}			
		};
		table.addColumn(extraEmployee, "Extra Employee");
		table.setColumnWidth(extraEmployee, 100, Unit.PX);
	}
	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	private void loadSiteLocation() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(custId);
		filtervec.add(filter);
		
		if(state!=null&&!state.equals("")){
			filter = new Filter();
//			filter.setQuerryString("billingAddress.state");
			filter.setQuerryString("contact.address.state");
			filter.setStringValue(state);
			filtervec.add(filter);
		}
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		query.setFilters(filtervec);
		
		query.setQuerryObject(new CustomerBranchDetails());
		service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				locationList.clear();
				locationList.add("--SELECT--");
				for (SuperModel model : result) {
					CustomerBranchDetails location=(CustomerBranchDetails) model;
					locationList.add(location.getBusinessUnitName());
				}
				Console.log("Site location size"+locationList.size());
			}
			
			@Override
			public void onFailure(Throwable arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	private void addcolumnUpdateButton() {
		// TODO Auto-generated method stub
		ButtonCell btnupdate = new ButtonCell();
		updateButton = new Column<EmployeeProjectAllocation, String>(btnupdate) {
			
			@Override
			public String getValue(EmployeeProjectAllocation object) {
				// TODO Auto-generated method stub
				return "Update";
			}
		};
		
		table.addColumn(updateButton, "Update");
		table.setColumnWidth(updateButton, 100, Unit.PX);
		
		updateButton.setFieldUpdater(new FieldUpdater<EmployeeProjectAllocation, String>() {
			
			@Override
			public void update(int index, EmployeeProjectAllocation object, String value) {
				// TODO Auto-generated method stub
				
				ArrayList<EmployeeInfo> empInfolist = new ArrayList<EmployeeInfo>();
				Console.log("LoginPresenter.globalEmployeeInfo "+LoginPresenter.globalEmployeeInfo.size());
				try {
					for (EmployeeInfo employee : LoginPresenter.globalEmployeeInfo) {
						if(branchName!=null && !branchName.equals("")) {
							if(branchName.equals(employee.getBranch())) {
								Date date=new Date();
								if(employee.isLeaveAllocated() && employee.getDateOfJoining()!=null && employee.getDateOfJoining().before(date)){
									empInfolist.add(employee);
								}

							}
						}
						else {
							Date date=new Date();
							if(employee.isLeaveAllocated() && employee.getDateOfJoining()!=null && employee.getDateOfJoining().before(date)){
								empInfolist.add(employee);
							}
						}
						
					}
				} catch (Exception e) {
					e.printStackTrace();
					Console.log("Exception"+e.getCause());
				}
				
				Console.log("employeeList $ =="+empInfolist.size());
				/***Date 23-2-2019 added by amol for sorting ***/
				java.util.Collections.sort(empInfolist);
				
				updatepoup.showPopUp();
				rowIndex = index;
				updatepoup.loadSiteLocation(custId, state);
				updatepoup.loadEmployeeListData(empInfolist);
			}
		});
	}
	
	@Override
	public void onClick(ClickEvent event) {

		if(event.getSource().equals(updatepoup.getLblOk())){
			if(updatepoup.getOlbEmployee().getSelectedIndex()!=0){
				Console.log("size"+ updatepoup.getOlbEmployee().getItems().size());
				String empName = updatepoup.getOlbEmployee().getValue(updatepoup.getOlbEmployee().getSelectedIndex());
				boolean updateEmpFlag = true;
				
				Label : for(EmployeeProjectAllocation emp:getDataprovider().getList()){
					if(emp.getEmployeeName().equals(empName)){
						EmployeeInfo employeeDetails=updatepoup.getOlbEmployee().getSelectedItem();
						if(emp.getEmployeeId().equals(employeeDetails.getEmpCount()+"")){
							GWTCAlert alert=new GWTCAlert();
							alert.alert(empName+ " already selected");
							updateEmpFlag = false;
							break;
						}
					}
				}
				if(updateEmpFlag){
					if(empName.contains(" / ")){
						String str=empName;
						String[] arrOfStr = str.split(" / ");
						String updatedName=arrOfStr[0];
						System.out.println(""+updatedName);
						Console.log("updatedName "+updatedName);
						getDataprovider().getList().get(rowIndex).setRefName(empName);
						getDataprovider().getList().get(rowIndex).setEmployeeName(updatedName);
						EmployeeInfo employeeDetails=updatepoup.getOlbEmployee().getSelectedItem();
						getDataprovider().getList().get(rowIndex).setEmployeeId(employeeDetails.getCount()+"");
						
					}else{
						getDataprovider().getList().get(rowIndex).setEmployeeName(empName);
						Console.log("ELSE "+getDataprovider().getList().get(rowIndex).getEmployeeName());
						EmployeeInfo employeeDetails=updatepoup.getOlbEmployee().getSelectedItem();
						getDataprovider().getList().get(rowIndex).setEmployeeId(employeeDetails.getCount()+"");
						getDataprovider().getList().get(rowIndex).setRefName(null);
					}
				}

			}
			if(updatepoup.getOlbEmplRole().getSelectedIndex()!=0){
				getDataprovider().getList().get(rowIndex).setEmployeeRole(updatepoup.getOlbEmplRole().getValue(updatepoup.getOlbEmplRole().getSelectedIndex()));
			}
			if(updatepoup.getOlbSiteLocation().getSelectedIndex()!=0){
				getDataprovider().getList().get(rowIndex).setSiteLocation(updatepoup.getOlbSiteLocation().getValue(updatepoup.getOlbSiteLocation().getSelectedIndex()));
			}
			if(updatepoup.getOlbshift().getSelectedIndex()!=0){
				getDataprovider().getList().get(rowIndex).setShift(updatepoup.getOlbshift().getValue(updatepoup.getOlbshift().getSelectedIndex()));
			}
			table.redrawRow(rowIndex);
			
			updatepoup.hidePopUp();
			
		}
		if(event.getSource().equals(updatepoup.getLblCancel())){
			updatepoup.hidePopUp();
		}
	}
	
}
