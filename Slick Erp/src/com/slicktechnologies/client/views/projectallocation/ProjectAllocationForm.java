package com.slicktechnologies.client.views.projectallocation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.cnc.ConsumablesTable;
import com.slicktechnologies.client.views.cnc.EquipmentRentalTable;
import com.slicktechnologies.client.views.cnc.OtherServicesTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.Consumables;
import com.slicktechnologies.shared.common.cnc.EquipmentRental;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.smartgwt.client.widgets.events.ValueChangedEvent;
import com.smartgwt.client.widgets.events.ValueChangedHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
/**
 * Rahul Verm a created on 10 May 2018
 * 
 * @author Admin
 *
 */
public class ProjectAllocationForm extends FormScreen<ProjectAllocation>
		implements ClickHandler, ValueChangedHandler ,ChangeHandler{
//	ProjectAllocation projectAllocationObj;
	HashMap<String, ProjectAllocation> projectAllocationMap;
	int recordSelected;
	TextBox tbProjectId;
	TextBox tbContract_Id, tbStatus,tb_projectName,tbManagementFees,tbOverHeadCostPerEmployee,tbNoOFMonthsForRental,tbInterestOfMachinary,tbMaintaniancePercentage;
	DateBox dbProjectStartDate;
	DateBox dbProjectEndDate;
	TextBox tbRemark;
	ObjectListBox<Branch> olbbBranch;
	
	ObjectListBox<EmployeeInfo> olbSuperVisorInfo;
	ObjectListBox<EmployeeInfo> olbManagerInfo;
	TabPanel tabPanel;
	HorizontalPanel tableheader;
	String[] tableheaderbarnames;
	VerticalPanel employeeVerticalPanel;
	Button bt_addNewEmployee;//, bt_Go;
	PersonInfoComposite personInfoComposite;
	ListGrid employeeGrid;
	ProjectAllocation projectAllocationObject;
	PopupPanel panel = new PopupPanel(true);
	PopupPanel rowUpdatepanel = new PopupPanel(true);
	/**
	 * Tables to be shown in tab
	 */
	EquipmentRentalTable equipmentRentalTable;
	ConsumablesTable consumableTable;
	OtherServicesTable otherServiceTable;
	ProjectsEmployeeTable employeeTable;
	/** date 14.8.2018 added by komal**/
	ObjectListBox<EmployeeInfo> olbBranchManagerInfo;
	Button bt_addEquipmentDetails,
	bt_consumablesDetails;
	/**
	 * date 10.1.2019 added by komal
	 * description : to set sync , submit frequency ,lat - long of project , allowed radius within
	 *               which one is allowed to mark attendance
	 **/
	DoubleBox syncFrequency;
	DoubleBox submitFrequency;
	DoubleBox allowedRadius;
	DoubleBox locationLatitude;
	DoubleBox locationLongitude;
	/** date 25.01.2018 added by komal to shaw project status**/
	CheckBox cbStatus;
	
	
	/**
	 * @author Anil,Date : 22-02-2019
	 * Paid leave and bonus as additional allowance
	 */
	CheckBox cbPaidLeaveAndBonusAsAdditionalAllowance;
	/** date 16.08.2019 added by komal to store minimum ot hours**/
	DoubleBox dbMiOtMinutes;
	ObjectListBox<CustomerBranchDetails> oblCustomerBranch;
	List<CustomerBranchDetails> customerBranchList=new ArrayList<CustomerBranchDetails>();
	final GenricServiceAsync async =GWT.create(GenricService.class);
	
	/**
	 * @author Anil
	 * @since 24-07-2020
	 * Adding field payroll start day which will define payroll cycle
	 */
	IntegerBox ibPayrollStartDay;
	boolean payrollCycleFlag;
	
	/**
	 * @author Anil
	 * @since 20-08-2020
	 * Sunrise facility : state and site location changes
	 * Raised by Rahul Tiwari
	 */
	ObjectListBox<State> oblState;
	boolean addStateFlag=false;
	
	Button btnClear;
	
	public static ArrayList<EmployeeProjectAllocation> deleteempprojectallocationlist = new ArrayList<EmployeeProjectAllocation>();
	
	
	public ProjectAllocationForm() {
		super(FormStyle.DEFAULT);
		createGui();
		employeeTable.connectToLocal();
		equipmentRentalTable.connectToLocal();
		consumableTable.connectToLocal();
		otherServiceTable.connectToLocal();
//		projectAllocationObj=new ProjectAllocation();

		bt_addNewEmployee.addClickHandler(this);
		tbInterestOfMachinary.addChangeHandler(this);
		tbMaintaniancePercentage.addChangeHandler(this);
		tb_projectName.setEnabled(false);
		cbPaidLeaveAndBonusAsAdditionalAllowance.setValue(false);
		
		payrollCycleFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("ProjectAllocation", "PayrollDayForMultiplePayrollCycle");
		ibPayrollStartDay.setEnabled(false);
		
		addStateFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "AddStatefield");
		
	}

	private VerticalPanel getVerticalPanel() {

		VerticalPanel verticalPnl = new VerticalPanel();
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		horizontalPanel.getElement().getStyle().setWidth(100, Unit.PCT);
		horizontalPanel.add(getFieldTB("Project Name", tb_projectName));
		horizontalPanel.add(getFieldOLSupervisor("Supervisor",olbSuperVisorInfo));
		horizontalPanel.add(getFieldOLManager("Manager", olbManagerInfo));
		/** date 14.8.2018 added by komal**/
		horizontalPanel.add(getFieldOLManager("Branch Manager", olbBranchManagerInfo));
		
		if(addStateFlag){
			horizontalPanel.add(getFieldObl("State", oblState));
		}
		
		verticalPnl.add(horizontalPanel);
		horizontalPanel = new HorizontalPanel();
		horizontalPanel.getElement().getStyle().setWidth(100, Unit.PCT);
		horizontalPanel.add(getFieldTB("Project Id", tbProjectId));
		horizontalPanel.add(getFieldDB("Start Date", dbProjectStartDate));
		horizontalPanel.add(getFieldDB("End Date", dbProjectEndDate));
		horizontalPanel.add(getFieldTB("Contract Id", tbContract_Id));
		horizontalPanel.add(getFieldOL("Branch", olbbBranch));
		horizontalPanel.add(getFieldOLB("Customer Branch", oblCustomerBranch));
		
		ibPayrollStartDay=new IntegerBox();
		ibPayrollStartDay.setEnabled(false);
		if(payrollCycleFlag){
			horizontalPanel.add(getField("Payroll (Start Day)", ibPayrollStartDay));
		}
		
		verticalPnl.add(horizontalPanel);

		return verticalPnl;
	}

	private VerticalPanel getFieldOLManager(String labelName,ObjectListBox<EmployeeInfo> olbManagerInfo2) {
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.getElement().getStyle().setMargin(2.0, Unit.PCT);
		verticalPanel.getElement().getStyle().setWidth(30, Unit.PCT);
		Label label = new Label(labelName);
		verticalPanel.add(label);
		olbManagerInfo2.getElement().getStyle().setWidth(200, Unit.PX);
		olbManagerInfo2.getElement().getStyle().setHeight(130, Unit.PCT);
		verticalPanel.add(olbManagerInfo2);
		return verticalPanel;
	}

	private VerticalPanel getFieldOLSupervisor(String labelName,
			ObjectListBox<EmployeeInfo> olbSuperVisorInfo2) {
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.getElement().getStyle().setMargin(2.0, Unit.PCT);
		verticalPanel.getElement().getStyle().setWidth(30, Unit.PCT);
		Label label = new Label(labelName);
		verticalPanel.add(label);
		olbSuperVisorInfo2.getElement().getStyle().setWidth(200, Unit.PX);
		olbSuperVisorInfo2.getElement().getStyle().setHeight(130, Unit.PCT);
		verticalPanel.add(olbSuperVisorInfo2);
		return verticalPanel;
	}
	
	private VerticalPanel getFieldObl(String labelName,ObjectListBox olbSuperVisorInfo2) {
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.getElement().getStyle().setMargin(2.0, Unit.PCT);
		verticalPanel.getElement().getStyle().setWidth(30, Unit.PCT);
		Label label = new Label(labelName);
		verticalPanel.add(label);
		olbSuperVisorInfo2.getElement().getStyle().setWidth(200, Unit.PX);
		olbSuperVisorInfo2.getElement().getStyle().setHeight(130, Unit.PCT);
		verticalPanel.add(olbSuperVisorInfo2);
		return verticalPanel;
	}

	private VerticalPanel getFieldLB(String labelName, ListBox lb) {
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.getElement().getStyle().setMargin(2.0, Unit.PCT);
		verticalPanel.getElement().getStyle().setWidth(30, Unit.PCT);
		Label label = new Label(labelName);
		verticalPanel.add(label);
		lb.getElement().getStyle().setWidth(200, Unit.PX);
		verticalPanel.add(lb);
		return verticalPanel;
	}

	private VerticalPanel getFieldOL(String labelName,
			ObjectListBox<Branch> olbb) {
		// TODO Auto-generated method stub
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.getElement().getStyle().setMargin(2.0, Unit.PCT);
		verticalPanel.getElement().getStyle().setWidth(20, Unit.PCT);
		Label label = new Label(labelName);
		verticalPanel.add(label);
		olbb.getElement().getStyle().setWidth(150, Unit.PX);
		olbb.getElement().getStyle().setHeight(27, Unit.PX);
		verticalPanel.add(olbb);
		return verticalPanel;
	}

	private VerticalPanel getFieldOLB(String labelName,
			ObjectListBox<CustomerBranchDetails> olbb) {
		// TODO Auto-generated method stub
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.getElement().getStyle().setMargin(2.0, Unit.PCT);
		verticalPanel.getElement().getStyle().setWidth(20, Unit.PCT);
		Label label = new Label(labelName);
		label.getElement().getStyle().setMarginLeft(8.0, Unit.PX);
		verticalPanel.add(label);
		olbb.getElement().getStyle().setWidth(150, Unit.PX);
		olbb.getElement().getStyle().setMarginLeft(8.0, Unit.PX);
		olbb.getElement().getStyle().setHeight(27, Unit.PX);
		verticalPanel.add(olbb);
		return verticalPanel;
	}
	
	
	private VerticalPanel getField(String labelName,Widget widget) {
		// TODO Auto-generated method stub
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.getElement().getStyle().setMargin(2.0, Unit.PCT);
		verticalPanel.getElement().getStyle().setWidth(20, Unit.PCT);
		Label label = new Label(labelName);
		label.getElement().getStyle().setMarginLeft(8.0, Unit.PX);
		verticalPanel.add(label);
		
		if(widget instanceof IntegerBox){
			widget.getElement().getStyle().setWidth(150, Unit.PX);
			widget.getElement().getStyle().setMarginLeft(8.0, Unit.PX);
			widget.getElement().getStyle().setHeight(25, Unit.PX);
		}
		
		verticalPanel.add(widget);
		return verticalPanel;
	}
	
	
	
	
	private VerticalPanel getFieldDB(String labelName, DateBox dateBox) {
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.getElement().getStyle().setMargin(2.0, Unit.PCT);
		Label label = new Label(labelName);
		verticalPanel.add(label);

		dateBox.getElement().getStyle().setMarginRight(5.0, Unit.PX);
		dateBox.getElement().getStyle().setWidth(150, Unit.PX);
		verticalPanel.add(dateBox);

		return verticalPanel;
	}

	private VerticalPanel getFieldTB(String labelName, TextBox textbox) {
		// TODO Auto-generated method stub
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.getElement().getStyle().setMargin(2.0, Unit.PCT);
		Label label = new Label(labelName);
		// label.getElement().getStyle().setMarginBottom(5.0,Unit.PX);
		// label.getElement().getStyle().setMarginRight(5.0,Unit.PX);

		verticalPanel.add(label);
		textbox.getElement().getStyle().setMarginRight(5.0, Unit.PX);
		textbox.getElement().getStyle().setWidth(150, Unit.PX);
		verticalPanel.add(textbox);

		return verticalPanel;
	}

	@SuppressWarnings("unused")
	private void initalizeWidget() {
		
		tbRemark=new TextBox();
		
		tabPanel = new TabPanel();
		tbNoOFMonthsForRental=new TextBox();
		//tbNoOFMonthsForRental.setEnabled(false);
		
		tbInterestOfMachinary=new TextBox();
	//	tbInterestOfMachinary.setEnabled(false);
		
		tbMaintaniancePercentage=new TextBox();
	//	tbMaintaniancePercentage.setEnabled(false);
		
		tb_projectName=new TextBox();
		tb_projectName.setEnabled(false);
		
		tbProjectId = new TextBox();
		tbProjectId.setEnabled(false);
		
		tbStatus = new TextBox();
		tbStatus.setValue(Contract.CREATED);
		tbStatus.setEnabled(false);
		
//		dbProjectStartDate=new DateBox();
		dbProjectStartDate = new DateBoxWithYearSelector();
		dbProjectStartDate.setEnabled(false);

		
//		dbProjectEndDate=new DateBox();
		dbProjectEndDate = new DateBoxWithYearSelector();
		dbProjectEndDate.setEnabled(false);
		
		tbContract_Id=new TextBox();
		tbContract_Id.setEnabled(false);
		

		tbManagementFees=new TextBox();
	//	tbManagementFees.setEnabled(false);
		
		tbOverHeadCostPerEmployee=new TextBox();
	//	tbOverHeadCostPerEmployee.setEnabled(false);
		
		
		olbManagerInfo = new ObjectListBox<EmployeeInfo>();
		Filter filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
	//	EmployeeInfo.makeObjectListBoxLive(olbManagerInfo);
		/** date 14.8.2018 added by komal **/
		olbManagerInfo.makeEmployeeLive(AppConstants.HRMODULE, "Project Allocation", "Manager");
	//	olbManagerInfo.setEnabled(false);
		
		olbSuperVisorInfo = new ObjectListBox<EmployeeInfo>();
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		//EmployeeInfo.makeObjectListBoxLive(olbSuperVisorInfo);//Supervisor
		olbSuperVisorInfo.makeEmployeeLive(AppConstants.HRMODULE, "Project Allocation", "Supervisor");
		//olbSuperVisorInfo.setEnabled(false);

		/** date 14.8.2018 added by komal**/
		olbBranchManagerInfo = new ObjectListBox<EmployeeInfo>();
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		//EmployeeInfo.makeObjectListBoxLive(olbSuperVisorInfo);//Supervisor
		olbBranchManagerInfo.makeEmployeeLive(AppConstants.HRMODULE, "Project Allocation", "Branch Manager");
	//	olbBranchManagerInfo.setEnabled(false);
		/**
		 * end komal
		 */
		personInfoComposite = AppUtility.customerInfoComposite(new Customer());
		personInfoComposite.getCustomerId().getHeaderLabel()
				.setText("* Customer ID");
		personInfoComposite.getCustomerName().getHeaderLabel()
				.setText("* Customer Name");
		personInfoComposite.getCustomerCell().getHeaderLabel()
				.setText("* Customer Cell");
		//personInfoComposite.setEnable(false);

		olbbBranch = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbBranch);
		
		oblCustomerBranch=new ObjectListBox<CustomerBranchDetails>();
		oblCustomerBranch.addItem("--SELECT--");
	//	olbbBranch.setEnabled(false);
		
		/** date 25.01.2018 added by komal to shaw project status**/
		cbStatus = new CheckBox();
		cbStatus.setValue(false);
		employeeTable = new ProjectsEmployeeTable();
		equipmentRentalTable=new EquipmentRentalTable();
		consumableTable=new ConsumablesTable();
		otherServiceTable=new OtherServicesTable();
		
		oblState=new ObjectListBox<State>();
		State.makeOjbectListBoxLive(oblState);
		oblState.addChangeHandler(this);
		oblState.setEnabled(false);
		
		FlowPanel flowpanelProject = new FlowPanel();
		FlowPanel flowpanelEmployee = new FlowPanel();
		FlowPanel flowpanelEquipment = new FlowPanel();
		FlowPanel flowpanelMaterial = new FlowPanel();
		FlowPanel flowpanelOtherService = new FlowPanel();
		

		String tabTitleProject = "Project";
		String tabTitleEmployee = "Employee";
		String tabTitleMaterial = "Consumables";
		String tabTitleEquipment = "Equipment";
		String tabOtherServices = "Other Services";
		
		bt_addNewEmployee = new Button("Add Employee");
		bt_addNewEmployee.getElement().addClassName("buttonMarginBottom");
		
		bt_addEquipmentDetails = new Button("Add Housekeeping Equipment");
		bt_addEquipmentDetails.getElement().addClassName("buttonMarginBottom");
		bt_addEquipmentDetails.addClickHandler(this);

		bt_consumablesDetails = new Button("Add Housekeeping Consumables");
		bt_consumablesDetails.getElement().addClassName("buttonMarginBottom");
		bt_consumablesDetails.addClickHandler(this);

		
//		employeeVerticalPanel=new VerticalPanel();
//		employeeVerticalPanel.setWidth("100%");
//		employeeVerticalPanel.setHeight("600px");
//		HorizontalPanel h1 = new HorizontalPanel();
//		h1.add(bt_addNewEmployee);
//		HorizontalPanel h2 = new HorizontalPanel();
//		h2.add(employeeTable.getTable());
		
		FlowPanel addemployeeandClearbtn = new FlowPanel();
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		btnClear = new Button("Clear");
		btnClear.addClickHandler(this);
		
		bt_addNewEmployee.getElement().getStyle().setMarginRight(5, Unit.PX);

		horizontalPanel.add(bt_addNewEmployee);
		horizontalPanel.add(btnClear);
		
		horizontalPanel.getElement().getStyle().setMarginTop(5, Unit.PX);
		flowpanelEmployee.add(horizontalPanel);

		flowpanelEmployee.add(employeeTable.getTable());
//		employeeVerticalPanel.add(h1);
//		employeeVerticalPanel.add(h2);
		
		flowpanelProject.add(getVerticalPanel());
		//flowpanelEmployee.add(employeeVerticalPanel);
	//	VerticalPanel equipmentVerPanel = new VerticalPanel();
	//	equipmentVerPanel.setWidth("100%");
	//	equipmentVerPanel.setHeight("600px");
		flowpanelEquipment.add(getFormPanelForEquiment());
		flowpanelEquipment.add(equipmentRentalTable.getTable());
	//	flowpanelEquipment.add(equipmentVerPanel);
		flowpanelMaterial.add(bt_consumablesDetails);
		flowpanelMaterial.add(consumableTable.getTable());
		flowpanelOtherService.add(otherServiceTable.getTable());
		
		tabPanel.add(flowpanelProject, tabTitleProject);
		tabPanel.add(flowpanelEmployee, tabTitleEmployee);
		/**
		 * @author Amol, Date:25-04-2019
		 * as per the disscussion with Nitin sir and Rahul Tiwari ,need to hide equipment ,material and other service tab
		 */
//		tabPanel.add(flowpanelEquipment, tabTitleEquipment);
//		tabPanel.add(flowpanelMaterial, tabTitleMaterial);
//		tabPanel.add(flowpanelOtherService, tabOtherServices);
		
		tabPanel.selectTab(0);
		tabPanel.setSize("100%", "700px");
		tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {

			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				int tabId = event.getSelectedItem();
				System.out.println("TAB ID " + tabId);
				glassPanel = new GWTCGlassPanel();

				if (tabId == 0) {

				} else if (tabId == 1) {
					employeeTable.getTable().redraw();
				} else if (tabId == 2) {
					equipmentRentalTable.getTable().redraw();
				} else if (tabId == 3) {
					consumableTable.getTable().redraw();
				} else if (tabId == 4) {
					otherServiceTable.getTable().redraw();
				}
		}});
		
		
//		Timer timer=new Timer() {
//			@Override
//			public void run() {
//				setDefaultDataOfLoggedInUser();
//			}
//		};
//		timer.schedule(4000);
		
		/**
		 * date 10.1.2019 added by komal
		 * description : to set sync , submit frequency ,lat - long of project , allowed radius within
		 *               which one is allowed to mark attendance
		 **/
		syncFrequency = new DoubleBox();
		submitFrequency = new DoubleBox();
		allowedRadius = new DoubleBox();
		locationLatitude = new DoubleBox();
		locationLongitude = new DoubleBox();
		
		cbPaidLeaveAndBonusAsAdditionalAllowance=new CheckBox();
		cbPaidLeaveAndBonusAsAdditionalAllowance.setValue(false);
		
		/** date 16.08.2019 added by komal to store minimum ot hours**/
		dbMiOtMinutes = new DoubleBox();
		
		
	}

	public void loadCustomerBranch(int custId,final String custBranch) {
		customerBranchList=new ArrayList<CustomerBranchDetails>();
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(custId);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerBranchDetails());

		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				oblCustomerBranch.clear();
				oblCustomerBranch.addItem("--SELECT--");
				showDialogMessage("failed." + caught);
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<CustomerBranchDetails> custBranchList=new ArrayList<CustomerBranchDetails>();
				if(result.size()!=0){
					for(SuperModel model:result){
						CustomerBranchDetails obj=(CustomerBranchDetails) model;
						custBranchList.add(obj);
						customerBranchList.add(obj);
					}
					Console.log("custBranchList Size11 "+custBranchList.size());
					
					Console.log("customerBranchList Size222 "+customerBranchList.size());
				}
				
				if(custBranchList.size()!=0){
					oblCustomerBranch.setListItems(custBranchList);
					if(custBranch!=null){
						oblCustomerBranch.setValue(custBranch);
					}else{
						oblCustomerBranch.setEnabled(true);
					}
				}else{
					oblCustomerBranch.addItem("--SELECT--");
				}
			}
		});
	}
	private Widget getFormPanelForEquiment() {
		// TODO Auto-generated method stub
		VerticalPanel verticalPnl = new VerticalPanel();
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		horizontalPanel.getElement().getStyle().setWidth(100, Unit.PCT);
		horizontalPanel.add(bt_addEquipmentDetails);
		horizontalPanel.getElement().getStyle().setWidth(100, Unit.PCT);
		//horizontalPanel.add(getFieldTB("No of months for Rental", tbNoOFMonthsForRental));
		horizontalPanel.add(getFieldTB("Interest On Machinary(%)", tbInterestOfMachinary));
		horizontalPanel.add(getFieldTB("Maintainance %", tbMaintaniancePercentage));
		verticalPnl.add(horizontalPanel);
		
		return verticalPnl;
	}

	@Override
	public void createScreen() {
		payrollCycleFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("ProjectAllocation", "PayrollDayForMultiplePayrollCycle");
		addStateFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "AddStatefield");
//		siteLocationFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("ProjectAllocation", "EnableSiteLocation");
		
		// TODO Auto-generated method stub
		initalizeWidget();
		this.processlevelBarNames = new String[] {AppConstants.NEW,AppConstants.VIEWCNC,"Allocate Overtime"};//,AppConstants.CANCEL,"Amend Project" //,"Raise Bill",AppConstants.VIEWBILL
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();

		FormField fgroupingClientInformation = fbuilder
				.setlabel("Client Information").widgetType(FieldType.Grouping)
				.setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("", tabPanel);
		FormField fpanel = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();

		fbuilder = new FormFieldBuilder("", personInfoComposite);
		FormField fpersonInfoComposite = fbuilder.setMandatory(false)
				.setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Status", cbStatus);
		FormField ftbStatus = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		/**
		 * date 10.1.2019 added by komal
		 * description : to set sync , submit frequency ,lat - long of project , allowed radius within
		 *               which one is allowed to mark attendance
		 **/
		fbuilder = new FormFieldBuilder("Sync Frequency",syncFrequency);
		FormField fsyncFrequency= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("Submit Frequency",submitFrequency);
		FormField fsubmitFrequency= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Allowed Radius",allowedRadius);
		FormField fallowedRadius= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Project Latitude",locationLatitude);
		FormField flocationLatitude= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Project Longitude",locationLongitude);
		FormField flocationLongitude= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fgroupingAppInfo = fbuilder.setlabel("Additional Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Paid leave and bonus as additional allowance", cbPaidLeaveAndBonusAsAdditionalAllowance);
		FormField fcbPaidLeaveAndBonusAsAdditionalAllowance = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		
		/** date 16.08.2019 added by komal to store minimum ot hours**/
		fbuilder = new FormFieldBuilder("Min OT Minutes", dbMiOtMinutes);
		FormField fdbMiOtMinutes = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Payroll (Start Day)",ibPayrollStartDay);
		FormField fibPayrollStartDay = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).setVisible(payrollCycleFlag).build();

		
		FormField[][] formfield = { { fgroupingClientInformation },
				{ fpersonInfoComposite},
				{ ftbStatus },
				{fgroupingAppInfo},
				{fallowedRadius , flocationLatitude , flocationLongitude }, /** date 12.1.2019 added by komal to add details related to attendance app(Nitin sir's requirement)**/
				{fsyncFrequency , fsubmitFrequency,fcbPaidLeaveAndBonusAsAdditionalAllowance,fdbMiOtMinutes},
				{ fpanel } };
		this.fields = formfield;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateModel(ProjectAllocation model) {
		System.out.println("Hello this is update Model");
		if (personInfoComposite.getValue() != null)
			model.setPersonInfo(personInfoComposite.getValue());

		if(tb_projectName.getValue()!=null){
			model.setProjectName(tb_projectName.getValue().trim());
		}
		
		if (tbContract_Id.getValue() != null) {
			model.setContractId(Integer.parseInt(tbContract_Id.getValue()
					.trim()));
		}
		if (tbStatus.getValue() != null) {
			model.setStatus(tbStatus.getValue());
		}
		if (dbProjectStartDate.getValue() != null) {
			model.setStartDate(dbProjectStartDate.getValue());
		}
		if (dbProjectEndDate.getValue() != null) {
			model.setEndDate(dbProjectEndDate.getValue());
		}

		if (tbRemark.getValue() != null) {
			model.setRemark(tbRemark.getValue());
		}
		if (olbSuperVisorInfo.getValue() != null) {
			model.setSupervisorName(olbSuperVisorInfo
					.getValue(olbSuperVisorInfo.getSelectedIndex()));
		}
		if (olbManagerInfo.getValue() != null) {
			model.setManagerName(olbManagerInfo.getValue(olbManagerInfo
					.getSelectedIndex()));
		}
		if (olbbBranch.getValue() != null) {
			model.setBranch(olbbBranch.getValue(olbbBranch.getSelectedIndex()));
		}
		if(oblCustomerBranch.getValue()!=null){
			model.setCustomerBranch(oblCustomerBranch.getValue());
		}else{
			model.setCustomerBranch("");
		}
		if(employeeTable.getValue().size()!=0){
//			model.setEmployeeProjectAllocationList(employeeTable.getValue());
			model.setEmployeeProjectAllocationList(getUpdatedList(employeeTable.getValue()));
			if(model.getEmployeeProjectAllocationList().size()>AppConstants.employeeProjectAllocationListNumber){
				model.setSeperateEntityFlag(true);
			}
		}
		if(equipmentRentalTable.getValue().size()!=0){
			model.setEquipmentRentalList(equipmentRentalTable.getValue());
		}
		if(consumableTable.getValue().size()!=0){
			model.setConsumablesList(consumableTable.getValue());
		}
		if(otherServiceTable.getValue().size()!=0){
			model.setOtherServicesList(otherServiceTable.getValue());
		}
		//Here we will update the list instead of adding
//		try{
//		if(model.getVersionMap()==null){
//			HashMap<Integer, ProjectAllocation> versionMap=new HashMap<Integer, ProjectAllocation>();
//			versionMap.put(1, model);
//			model.setVersionMap(versionMap);
//		}
//		}catch(Exception e){
//			e.printStackTrace();
//			Console.log("Error"+e);
//		}
		
		if(tbInterestOfMachinary.getValue()!=null){
			try{
				model.setInterestOnMachinary(Double.parseDouble(tbInterestOfMachinary.getValue()));
			}catch(Exception e){
				e.printStackTrace();
				model.setInterestOnMachinary(0);
			}
		}
		
		if(tbNoOFMonthsForRental.getValue()!=null){
			try{
				model.setNoOfMonthsForRental(Double.parseDouble(tbNoOFMonthsForRental.getValue()));
			}catch(Exception e){
				e.printStackTrace();
				model.setNoOfMonthsForRental(0);
			}
		}
		
		if(tbMaintaniancePercentage.getValue()!=null){
			try{
				model.setMaintainanceCharges(Double.parseDouble(tbMaintaniancePercentage.getValue()));
			}catch(Exception e){
				e.printStackTrace();
				model.setMaintainanceCharges(0);
			}
		}
		/** date 14.8.2018 added by komal **/
		if(olbBranchManagerInfo.getSelectedIndex()!=0){
			model.setBranchMangerName(olbBranchManagerInfo.getValue(olbBranchManagerInfo.getSelectedIndex()));
		}
		
		/**
		 * date 10.1.2019 added by komal
		 * description : to set sync , submit frequency ,lat - long of project , allowed radius within
		 *               which one is allowed to mark attendance
		 **/
		if(submitFrequency.getValue() != null){
			model.setSubmitFrequency(submitFrequency.getValue());
		}
		if(syncFrequency.getValue() != null){
			model.setSyncFrequency(syncFrequency.getValue());
		}
		if(allowedRadius.getValue() != null){
			model.setAllowedRadius(allowedRadius.getValue());
		}
		if(locationLatitude.getValue() != null){
			model.setLocationLatitude(locationLatitude.getValue());
		}
		if(locationLongitude.getValue() != null){
			model.setLocationLongitude(locationLongitude.getValue());
		}
		/** date 25.01.2018 added by komal to shaw project status**/
		if(cbStatus.getValue() !=null){
			model.setProjectStatus(cbStatus.getValue());
		}
		
		if(cbPaidLeaveAndBonusAsAdditionalAllowance.getValue()!=null){
			model.setPaidLeaveAndBonusAsAdditionalAllowance(cbPaidLeaveAndBonusAsAdditionalAllowance.getValue());
		}
		
		if(dbMiOtMinutes.getValue() != null){
			model.setMiOtMinutes(dbMiOtMinutes.getValue());
		}
		
		if(ibPayrollStartDay.getValue()!=null){
			model.setPayrollStartDay(ibPayrollStartDay.getValue());
		}else{
			model.setPayrollStartDay(null);
		}
		
		if(model.isSeperateEntityFlag() && deleteempprojectallocationlist!=null && deleteempprojectallocationlist.size()>0){
			model.setDeletedEmpoyeeProjectAllocationlist(deleteempprojectallocationlist);
		}
		
		model.setState(oblState.getValue());
		
		projectAllocationObject = model;

		presenter.setModel(model);
	}

	private List<EmployeeProjectAllocation> getUpdatedList(List<EmployeeProjectAllocation> list) {
		ArrayList<EmployeeProjectAllocation> updatedlist = new ArrayList<EmployeeProjectAllocation>();
		for(EmployeeProjectAllocation empproject : list){
			if(empproject.getEmployeeId()!=null && !empproject.getEmployeeId().equals("")){
				updatedlist.add(empproject);
			}
		}
		Console.log("updatedlist size "+updatedlist.size());
		return updatedlist;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateView(final ProjectAllocation view) {
		projectAllocationObject = view;

		if (view.getPersonInfo() != null)
			personInfoComposite.setValue(view.getPersonInfo());
		if (view.getCount() != -1 || view.getCount() != 0) {
			tbProjectId.setValue(view.getCount() + "");
		}
		if (view.getContractId() != 0) {
			tbContract_Id.setValue(view.getContractId() + "");
		}
		if (view.getStartDate() != null) {
			dbProjectStartDate.setValue(view.getStartDate());
		}
		if (view.getEndDate() != null) {
			dbProjectEndDate.setValue(view.getEndDate());
		}
		if (view.getRemark() != null) {
			tbRemark.setValue(view.getRemark());
		}
		/** date 14.8.2018 added by komal **/
//		Timer timer = new Timer(){
//
//			@Override
//			public void run() {
				// TODO Auto-generated method stub
				if (view.getSupervisorName() != null) {
					olbSuperVisorInfo.setValue(view.getSupervisorName());
				}

				if (view.getManagerName() != null) {
					olbManagerInfo.setValue(view.getManagerName());
				}
				if(view.getBranchMangerName()!=null){
					olbBranchManagerInfo.setValue(view.getBranchMangerName());
				}
//			}
//			
//		};
//		 timer.schedule(2000); 
		 /**
		  * end komal
		  */
			
		if (view.getBranch() != null) {
			olbbBranch.setValue(view.getBranch());
		}
		if(view.getCustomerBranch()!=null&&!view.getCustomerBranch().equals("")&&view.getPersonInfo()!=null&&view.getPersonInfo().getCount()!=0){
			loadCustomerBranch(view.getPersonInfo().getCount(), view.getCustomerBranch());
			oblCustomerBranch.setValue(view.getCustomerBranch());
		}
		
		
		if (view.getProjectName() != null) {
			tb_projectName.setValue(view.getProjectName());
		}

		if (view.getStatus() != null) {
			tbStatus.setValue(view.getStatus());
		}
		
		
		employeeTable.setBranchName(view.getBranch());
		
		if(view.getEmployeeProjectAllocationList().size()!=0){
			employeeTable.setValue(view.getEmployeeProjectAllocationList());
		}
		else{
			if(view.isSeperateEntityFlag()){
				
				final GWTCGlassPanel gpanel=new GWTCGlassPanel();
				gpanel.show();
				
				MyQuerry querry=new MyQuerry();
				
				Filter filter =null;
				Vector<Filter> vecList=new Vector<Filter>();
				
				filter=new Filter();
				filter.setQuerryString("projectAllocationId");
				filter.setIntValue(view.getCount());
				vecList.add(filter);
				
				querry.setFilters(vecList);
				querry.setQuerryObject(new EmployeeProjectAllocation());
				
				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						gpanel.hide();
						Console.log("list size "+result.size());

						ArrayList<EmployeeProjectAllocation> list = new ArrayList<EmployeeProjectAllocation>();
						for(SuperModel model : result){
							EmployeeProjectAllocation empProject = (EmployeeProjectAllocation) model;
							list.add(empProject);
						}
						if(list.size()>0){
							employeeTable.setValue(list);
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						gpanel.hide();
					}
				});
				
			}
		}
		
		if(view.getConsumablesList().size()!=0){
			consumableTable.setValue(view.getConsumablesList());
		}

		if(view.getEquipmentRentalList().size()!=0){
			equipmentRentalTable.setValue(view.getEquipmentRentalList());
		}
		
		if(view.getOtherServicesList().size()!=0){
			otherServiceTable.setValue(view.getOtherServicesList());
		}
		
		if(view.getNoOfMonthsForRental()!=0){
			tbNoOFMonthsForRental.setValue(view.getNoOfMonthsForRental()+"");
		}
		
		if(view.getInterestOnMachinary()!=0){
			tbInterestOfMachinary.setValue(view.getInterestOnMachinary()+"");
		}

		if(view.getMaintainanceCharges()!=0){
			tbMaintaniancePercentage.setValue(view.getMaintainanceCharges()+"");
		}
		/**
		 * date 10.1.2019 added by komal
		 * description : to set sync , submit frequency ,lat - long of project , allowed radius within
		 *               which one is allowed to mark attendance
		 **/
		syncFrequency.setValue(view.getSyncFrequency());
		submitFrequency.setValue(view.getSubmitFrequency());
		allowedRadius.setValue(view.getAllowedRadius());
		locationLatitude.setValue(view.getLocationLatitude());
		locationLongitude.setValue(view.getLocationLongitude());
		/** date 25.01.2018 added by komal to shaw project status**/
		cbStatus.setValue(view.isProjectStatus());
		
		cbPaidLeaveAndBonusAsAdditionalAllowance.setValue(view.isPaidLeaveAndBonusAsAdditionalAllowance());
		dbMiOtMinutes.setValue(view.getMiOtMinutes());
		
		if(view.getPayrollStartDay()!=null){
			ibPayrollStartDay.setValue(view.getPayrollStartDay());
		}
		
		if(view.getState()!=null){
			oblState.setValue(view.getState());
		}
		
		if (presenter != null) {
			presenter.setModel(view);
		}
	}

	public HorizontalPanel getTableheader() {
		return tableheader;
	}

	public void setTableheader(HorizontalPanel tableheader) {
		this.tableheader = tableheader;
	}

	public String[] getTableheaderbarnames() {
		return tableheaderbarnames;
	}

	public void setTableheaderbarnames(String[] tableheaderbarnames) {
		this.tableheaderbarnames = tableheaderbarnames;
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if (event.getSource().equals(bt_addNewEmployee)) {
			reactOnNewEmployeeAdd();
		}
		
		if(event.getSource().equals(equipmentRentalTable.getValue())){
			reactOnValidate();
		}

		if(event.getSource()==bt_addEquipmentDetails){
			double machineryInterestValue=0,maintanancePercentage=0;
			try{
				machineryInterestValue=Double.parseDouble(tbInterestOfMachinary.getValue().trim());
			}catch(Exception e){
				
			}
			try{
				maintanancePercentage=Double.parseDouble(tbMaintaniancePercentage.getValue().trim());
			}catch(Exception e){
				
			}
			EquipmentRental equipmentRental=new EquipmentRental();
			equipmentRental.setMachineryInterest(machineryInterestValue);
			equipmentRental.setMaintanancePercentage(maintanancePercentage);
			equipmentRentalTable.getDataprovider().getList().add(equipmentRental);
		}
		
		if(event.getSource()==bt_consumablesDetails){
			Consumables consumables=new Consumables();
			consumableTable.getDataprovider().getList().add(consumables);
		}
//		if (event.getSource().equals(employeePopup.getBtnOk())) {
//			System.out.println("Clicked on ok");
//			String branchStr, roleStr, employeeStr, shiftStr;
//			branchStr = employeePopup.getOlbbBranch().getItemText(
//					employeePopup.getOlbbBranch().getSelectedIndex());
//			System.out.println("branchStr" + branchStr);
//			roleStr = employeePopup.getOlbRole().getItemText(
//					employeePopup.getOlbRole().getSelectedIndex());
//			System.out.println("roleStr" + roleStr);
//			employeeStr = employeePopup.getOlbEmployee().getItemText(
//					employeePopup.getOlbEmployee().getSelectedIndex());
//			System.out.println("employeeStr" + employeeStr);
//			shiftStr = employeePopup.getOlbShift().getItemText(
//					employeePopup.getOlbShift().getSelectedIndex());
//			System.out.println("shiftStr" + shiftStr);
//			employeeGrid.getRecordList().add(
//					new EmployeeRecord(roleStr, employeeStr, shiftStr));
//			String employeeId="";
//			String employeeProfileUrl="";
//			for (Employee employee : LoginPresenter.globalEmployee) {
//				if(employee.getFullname().trim().equalsIgnoreCase(employeeStr.trim())){
//					employeeId=employee.getCount()+"";
//					employeeProfileUrl=employee.getPhoto().getUrl();
//				}
//			}			
//			projectAllocationObject.getEmployeeProjectAllocationList().add(new EmployeeProjectAllocation(roleStr, employeeStr, shiftStr,employeeId,employeeProfileUrl));
//		}

//		if (event.getSource().equals(updateEmployeePopUp.getBtnOk())) {
//			String employeeName = updateEmployeePopUp.getOlbEmployee()
//					.getValue();
//			String employeeRole = updateEmployeePopUp.getOlbRole().getValue();
//			String employeeShift = updateEmployeePopUp.getOlbShift().getValue();
//			System.out.println("recordSelected" + recordSelected
//					+ "employeeName" + employeeName + "employeeRole"
//					+ employeeRole + "employeeShift" + employeeShift);
//			employeeGrid.getRecord(recordSelected).setAttribute("employeeName",
//					employeeName);
//			employeeGrid.getRecord(recordSelected).setAttribute("employeeRole",
//					employeeRole);
//			employeeGrid.getRecord(recordSelected).setAttribute(
//					"employeeShift", employeeShift);
//			employeeGrid.refreshRow(recordSelected);
//			String employeeId="";
//			String employeeProfileUrl="";
//			for (Employee employee : LoginPresenter.globalEmployee) {
//				if(employee.getFullname().trim().equalsIgnoreCase(employeeName.trim())){
//					employeeId=employee.getCount()+"";
//					employeeProfileUrl=employee.getPhoto().getUrl();
//				}
//			}
//			projectAllocationObject.getEmployeeProjectAllocationList().add(new EmployeeProjectAllocation(employeeRole, employeeName, employeeShift,employeeId,employeeProfileUrl));
//			
//			rowUpdatepanel.hide();
//			// employeeGrid.notify();
//		}

//		if (event.getSource().equals(employeePopup.getBtnCancel())) {
//			panel.hide();
//		}
//		if (event.getSource().equals(updateEmployeePopUp.getBtnCancel())) {
//			rowUpdatepanel.hide();
//		}

//		if (event.getSource().equals(bt_Go)) {
//			if (personInfoComposite.getId().getValue() != null
//					&& !personInfoComposite.getId().getValue().equals("")) {
//				showWaitSymbol();
//				ArrayList<Filter> filtervec = new ArrayList<Filter>();
//				Filter temp = null;
//
//				temp = new Filter();
//				temp.setQuerryString("companyId");
//				if (UserConfiguration.getCompanyId() != null)
//					temp.setLongValue(UserConfiguration.getCompanyId());
//				filtervec.add(temp);
//
//				if (!personInfoComposite.getId().getValue().equals("")) {
//					temp = new Filter();
//					temp.setIntValue(Integer.parseInt(personInfoComposite
//							.getId().getValue()));
//					temp.setQuerryString("personInfo.count");
//					filtervec.add(temp);
//				}
//
//				if (!(personInfoComposite.getName().getValue().equals(""))) {
//					temp = new Filter();
//					temp.setStringValue(personInfoComposite.getName()
//							.getValue());
//					temp.setQuerryString("personInfo.fullName");
//					filtervec.add(temp);
//				}
//				showWaitSymbol();
//				ProjectAllocationServiceAsync projectAsync = GWT
//						.create(ProjectAllocationService.class);
//				projectAsync.getProjectionDataofCustomer(filtervec,
//						new AsyncCallback<ArrayList<ProjectAllocation>>() {
//
//							@Override
//							public void onSuccess(
//									ArrayList<ProjectAllocation> result) {
//								// TODO Auto-generated method stub
//								if (result.size() != 0) {
//									for (ProjectAllocation project : result) {
//										lbProject_Name.addItem(project
//												.getProjectName().trim());
//										projectAllocationMap = new HashMap<String, ProjectAllocation>();
//										projectAllocationMap.put(project
//												.getProjectName().trim(),
//												project);
//									}
//									tbProjectId.setEnabled(false);
//									tbContract_Id.setEnabled(false);
//									dbProjectStartDate.setEnabled(false);
//									dbProjectEndDate.setEnabled(false);
//									olbbBranch.setEnabled(false);
//
//								} else {
//									showDialogMessage("No Project Found!!");
//								}
//								hideWaitSymbol();
//							}
//
//							@Override
//							public void onFailure(Throwable caught) {
//								// TODO Auto-generated method stub
//								hideWaitSymbol();
//								showDialogMessage("Error : "
//										+ caught.toString());
//							}
//						});
//
//			} else {
//				showDialogMessage("Please select customer and try again!!");
//			}
//		}
		
		if (event.getSource()==btnClear) {
			System.out.println("on clear");
			reactonClearBtn();
		}
	}


	private void reactonClearBtn() {
		showWaitSymbol();
		List<EmployeeProjectAllocation> employeelist = employeeTable.getDataprovider().getList();
		
		List<EmployeeProjectAllocation> updatedemplist = new ArrayList<EmployeeProjectAllocation>();
		
		for(EmployeeProjectAllocation empprojectallocation : employeelist){
			if(empprojectallocation.getEmployeeId()!=null)
			System.out.println(" id "+empprojectallocation.getEmployeeId());
			if(empprojectallocation.getEmployeeId()!=null && !empprojectallocation.getEmployeeId().equals("")){
				updatedemplist.add(empprojectallocation);
			}
		}
		if(updatedemplist.size()!=0){
			System.out.println("updatedemplist size"+updatedemplist.size());
			employeeTable.clear();
			employeeTable.getDataprovider().setList(updatedemplist);
		}
		hideWaitSymbol();
	}

	//@Override
	//public boolean validate() {
		// TODO Auto-generated method stub
	//	boolean superValidate = super.validate();
	//	if (!reactOnValidate()) {
	//		return false;
	//	}
	//	if (superValidate) {
	//		return true;
	//	} else {
	//		return false;
	//	}

	//}
	public boolean reactOnValidate() {
		int counter = 0;
		// TODO Auto-generated method stub
		System.out.println("inside react on validate");
		List<EmployeeProjectAllocation>emptableList=employeeTable.getValue();
		for(EmployeeProjectAllocation el:emptableList){
			 counter=0;
			for(EmployeeProjectAllocation el1:emptableList){
				/** Date 17-08-2020 by Vijay added if condition with !"". this validation should not check when employee Name is blank ***/
				if(el!=null && el.getEmployeeName()!=null && !el.getEmployeeName().equals("")&& !el.getEmployeeName().equalsIgnoreCase("--SELECT--") && el.getEmployeeName().equals(el1.getEmployeeName())&&el.getEmployeeId()==el1.getEmployeeId()){
					counter++;
				}
			}
			if(counter>1){
				showDialogMessage("Employee Already Added");
				return false;
			}
			
		}
	
		return true;
	}

	private void reactOnNewEmployeeAdd() {
		// TODO Auto-generated method stub
		System.out.println("INSIDE POPUP CUSTOMER!!!");
//		panel = new PopupPanel(true);
//		panel.add(employeePopup);
//		panel.center();
//		// employeePopup.clear();
//		panel.show();
//		EmployeeProjectAllocation empProjectAllocation=new EmployeeProjectAllocation();
//		employeeTable.getDataprovider().getList().add(empProjectAllocation);
	}

	/*******************
	 * Getters and Setters
	 * 
	 * @param projectAllocationObject
	 ***********************/

//	public Canvas getViewPanel(final ProjectAllocation projectAllocationObject) {
//
//		employeeGrid = new ListGrid();
//		employeeGrid.setWidth("900px");
//		employeeGrid.setHeight("300px");
//		employeeGrid.setAlternateRecordStyles(true);
//		employeeGrid.setShowAllRecords(true);
//		employeeGrid.getElement().getStyle().setMarginTop(5, Unit.PCT);
//		employeeGrid.getElement().getStyle().setZIndex(0);
//		employeeGrid.getElement().addClassName("marginTopandZIndex");
//		ListGridField employeeRoleField = new ListGridField("employeeRole",
//				"Role");
//		ListGridField employeeNameField = new ListGridField("employeeName",
//				"Name");
//		ListGridField shiftField = new ListGridField("employeeShift", "Shift");
//		employeeGrid.setFields(new ListGridField[] { employeeRoleField,
//				employeeNameField, shiftField });
//		employeeGrid.setCanResizeFields(true);
//		if (projectAllocationObject != null) {
//			EmployeeData employeeData = new EmployeeData();
//			employeeGrid.setData(employeeData
//					.getRecords(projectAllocationObject
//							.getEmployeeProjectAllocationList()));
//		}
//		employeeGrid.addCellClickHandler(new CellClickHandler() {
//
//			@Override
//			public void onCellClick(CellClickEvent event) {
//				int colNum = event.getColNum();
//				int rowNum = event.getRowNum();
//
//				System.out.println("colNum" + colNum + "rowNum" + rowNum);
//				ListGridField field = employeeGrid.getField(colNum);
//				String fieldName = employeeGrid.getFieldName(colNum);
//				String fieldTitle = field.getTitle();
//				System.out.println("fieldName" + fieldName + "fieldTitle"
//						+ fieldTitle);
//				// String role=field.getAttribute("employeeRole");
//				// String employeeName=field.getAttribute("employeeName");
//				// String shift=field.getAttribute("employeeShift");
//				// System.out.println("role"+role+"employeeName"+employeeName+"shift"+shift);
//				recordSelected = rowNum;
//				String role = event.getRecord().getAttribute("employeeRole");
//				String employeeName = event.getRecord().getAttribute(
//						"employeeName");
//				String shift = event.getRecord().getAttribute("employeeShift");
//
//				System.out.println("role" + role + "employeeName"
//						+ employeeName + "shift" + shift);
//				updateEmployeePopUp.getOlbbBranch().setValue(
//						projectAllocationObject.getBranch());
//				updateEmployeePopUp.getOlbbBranch().setEnabled(false);
//				updateEmployeePopUp.getOlbRole().setValue(role);
//				updateEmployeePopUp.getOlbRole().setEnabled(false);
//				if (employeeName != null && !employeeName.equals("")) {
//					updateEmployeePopUp.getOlbEmployee().setValue(employeeName);
//				}
//				if (shift != null && !shift.equals("")) {
//					updateEmployeePopUp.getOlbShift().setValue(shift);
//				}
//				rowUpdatepanel = new PopupPanel(true);
//				rowUpdatepanel.add(updateEmployeePopUp);
//				rowUpdatepanel.center();
//				rowUpdatepanel.show();
//
//			}
//		});
//
//		return employeeGrid;
//	}

	class EmployeeData {
		private EmployeeRecord[] records;
		ArrayList<EmployeeProjectAllocation> listOfEmployeeForProject = new ArrayList<EmployeeProjectAllocation>();

		public EmployeeRecord[] getRecords(
				ArrayList<EmployeeProjectAllocation> listOfEmployeeForProject) {
			this.listOfEmployeeForProject.addAll(listOfEmployeeForProject);
			if (records == null) {
				records = getNewRecords(listOfEmployeeForProject);

			}
			return records;
		}

		private EmployeeRecord[] getNewRecords(
				ArrayList<EmployeeProjectAllocation> listOfEmployeeForProject) {
			// TODO Auto-generated method stub
			EmployeeRecord[] employeeArrayRecord = new EmployeeRecord[listOfEmployeeForProject
					.size()];
			int count = 0;
			for (EmployeeProjectAllocation employeeAllocation : listOfEmployeeForProject) {
				employeeArrayRecord[count] = new EmployeeRecord(
						employeeAllocation.getEmployeeRole(),
						employeeAllocation.getEmployeeName(),
						employeeAllocation.getShift());
				count = count + 1;
			}
			return employeeArrayRecord;
		}
	}

	class EmployeeRecord extends ListGridRecord {
		public EmployeeRecord(String role, String employeeName, String shift) {
			setRole(role);
			setEmployeeName(employeeName);
			setShiftName(shift);
		}

		private void setShiftName(String shift) {
			// TODO Auto-generated method stub
			setAttribute("employeeShift", shift);
		}

		private void setEmployeeName(String employeeName) {
			// TODO Auto-generated method stub
			setAttribute("employeeName", employeeName);
		}

		private void setRole(String role) {
			// TODO Auto-generated method stub
			setAttribute("employeeRole", role);
		}

	}

	public void setToNewState() {
		clear();
		AppMemory.getAppMemory().currentState = ScreeenState.NEW;
		toggleAppHeaderBarMenu();
		if (processLevelBar != null)
			processLevelBar.setVisibleFalse(false);
		setEnable(true);
		cbPaidLeaveAndBonusAsAdditionalAllowance.setValue(false);
	}

	@Override
	public void setToViewState() {
		// TODO Auto-generated method stub
		super.setToViewState();
		setMenuAsPerStatus();
		
//		SuperModel model=new ProjectAllocation();
//		model=projectAllocationObj;
		
//		employeeTable.setEnable(false);
//		equipmentRentalTable.setEnable(false);
//		consumableTable.setEnable(false);
//		otherServiceTable.setEnable(false);
	}

	private void setMenuAsPerStatus() {
		// TODO Auto-generated method stub
		this.toggleAppHeaderBarMenu();
		this.toggleProcessLevelMenu();
	}

	private void toggleProcessLevelMenu() {
		// TODO Auto-generated method stub
		ProjectAllocation entity=(ProjectAllocation) presenter.getModel();
		String status=entity.getStatus();
		for(int i=0;i<getProcesslevelBarNames().length;i++)
		{
			
		}
	}

	

	@Override
	public void onValueChanged(ValueChangedEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		super.setEnable(state);
		/**
		 * @author Anil
		 * @since 21-08-2020
		 */
		if(projectAllocationObject!=null){
			System.out.println("projectAllocationObj 1");
			if(projectAllocationObject.getPersonInfo()!=null){
				employeeTable.setCustId(projectAllocationObject.getPersonInfo().getCount());
				System.out.println("projectAllocationObj 2 "+employeeTable.getCustId());
			}
			if(projectAllocationObject.getState()!=null){
				employeeTable.setState(projectAllocationObject.getState());
				System.out.println("projectAllocationObj 3 "+employeeTable.getState());
			}
			
		}
		personInfoComposite.setEnable(state);
		tbStatus.setEnabled(false);
//		bt_Go.setEnabled(false);
//		lbProject_Name.setEnabled(false);
		olbSuperVisorInfo.setEnabled(state);
		olbManagerInfo.setEnabled(state);
	//	tbProjectId.setEnabled(state);
	//	dbProjectStartDate.setEnabled(state);
	//	dbProjectEndDate.setEnabled(state);
	//	tbContract_Id.setEnabled(false);
		olbbBranch.setEnabled(state);
		oblCustomerBranch.setEnabled(false);
		bt_addNewEmployee.setEnabled(state);
		employeeTable.setEnable(state);
		equipmentRentalTable.setEnable(state);
		consumableTable.setEnable(state);
		otherServiceTable.setEnable(state);
		tb_projectName.setEnabled(state);
		olbBranchManagerInfo.setEnabled(state);
		bt_addEquipmentDetails.setEnabled(state);
		bt_consumablesDetails.setEnabled(state);
		
		tb_projectName.setEnabled(false);
		
		/**
		 * @author Abhinav Bihade
		 * @since 03/01/2020
		 * As per Rahul Tiwari's Requirement for Sachin: Branch option in Project Allocation,
		 * page should be non-editable-setEnabled(false);
		 */
		olbbBranch.setEnabled(false);
		
		ibPayrollStartDay.setEnabled(false);
		oblState.setEnabled(false);
		btnClear.setEnabled(state);
		
	}

	@Override
	public void setToEditState() {
		System.out.println("Edit State");
		super.setToEditState();
		processLevelBar.setVisibleFalse(false);
//		bt_addNewEmployee.setEnabled(true);
//		olbSuperVisorInfo.setEnabled(true);
//		olbManagerInfo.setEnabled(true);
//		employeeTable.setEnable(true);
//		equipmentRentalTable.setEnable(true);
//		consumableTable.setEnable(true);
//		otherServiceTable.setEnable(true);
	}
	
	

	@Override
	public void toggleAppHeaderBarMenu() {
		System.out.println("toggleAppHeaderBarMenu called");
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 
				else
					menus[k].setVisible(false);  
				
				if(text.contains("Allocate Overtime")){
					menus[k].setVisible(false); 
				}
				
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Edit")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION)/*||text.contains("Print")|| text.contains(AppConstants.COMMUNICATIONLOG)*/)
						menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.CONTRACT,LoginPresenter.currentModule.trim());
	}

//	public ListBox getLbProject_Name() {
//		return lbProject_Name;
//	}
//
//	public void setLbProject_Name(ListBox lbProject_Name) {
//		this.lbProject_Name = lbProject_Name;
//	}

	public DateBox getDbProjectStartDate() {
		return dbProjectStartDate;
	}

	public void setDbProjectStartDate(DateBox dbProjectStartDate) {
		this.dbProjectStartDate = dbProjectStartDate;
	}

	public DateBox getDbProjectEndDate() {
		return dbProjectEndDate;
	}

	public void setDbProjectEndDate(DateBox dbProjectEndDate) {
		this.dbProjectEndDate = dbProjectEndDate;
	}

	public ObjectListBox<Branch> getOlbbBranch() {
		return olbbBranch;
	}

	public void setOlbbBranch(ObjectListBox<Branch> olbbBranch) {
		this.olbbBranch = olbbBranch;
	}

	
	
	
	
	
	public ObjectListBox<CustomerBranchDetails> getOblCustomerBranch() {
		return oblCustomerBranch;
	}

	public void setOblCustomerBranch(
			ObjectListBox<CustomerBranchDetails> oblCustomerBranch) {
		this.oblCustomerBranch = oblCustomerBranch;
	}

	public ObjectListBox<EmployeeInfo> getOlbSuperVisorInfo() {
		return olbSuperVisorInfo;
	}

	public void setOlbSuperVisorInfo(ObjectListBox<EmployeeInfo> olbSuperVisorInfo) {
		this.olbSuperVisorInfo = olbSuperVisorInfo;
	}

	public ObjectListBox<EmployeeInfo> getOlbManagerInfo() {
		return olbManagerInfo;
	}

	public void setOlbManagerInfo(ObjectListBox<EmployeeInfo> olbManagerInfo) {
		this.olbManagerInfo = olbManagerInfo;
	}
	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		if (event.getSource().equals(tbInterestOfMachinary)) {
			if (equipmentRentalTable != null) {
				for (int i = 0; i < this.equipmentRentalTable.getDataprovider()
						.getList().size(); i++) {
					EquipmentRental equipmentRental = this.equipmentRentalTable
							.getDataprovider().getList().get(i);
					try {
						equipmentRental.setMachineryInterest(Double
								.parseDouble(tbInterestOfMachinary.getValue()));
					} catch (Exception e) {
						e.printStackTrace();
						equipmentRental.setMachineryInterest(0d);
					}
					this.equipmentRentalTable.updateRow(equipmentRental, i);
				}
			}
		}

		if (event.getSource().equals(tbMaintaniancePercentage)) {
			if (equipmentRentalTable != null) {
				for (int i = 0; i < this.equipmentRentalTable.getDataprovider()
						.getList().size(); i++) {
					EquipmentRental equipmentRental = this.equipmentRentalTable
							.getDataprovider().getList().get(i);
					try {
						equipmentRental.setMaintanancePercentage(Double
								.parseDouble(tbInterestOfMachinary.getValue()));
					} catch (Exception e) {
						e.printStackTrace();
						equipmentRental.setMaintanancePercentage(0d);
					}
					this.equipmentRentalTable.updateRow(equipmentRental, i);
				}
			}
		}
		
		if (event.getSource().equals(oblState)) {
			if(oblState.getSelectedIndex()!=0){
				employeeTable.setState(oblState.getValue());
			}else{
				employeeTable.setState(null);
			}
			for(EmployeeProjectAllocation empProj:employeeTable.getValue()){
				empProj.setSiteLocation(null);
			}
			employeeTable.setEnable(true);
		}

	}
//	private void setDefaultDataOfLoggedInUser(){
//		olbSuperVisorInfo.setValue(LoginPresenter.loggedInUser);
//		olbManagerInfo.setValue(LoginPresenter.loggedInUser);
//		olbBranchManagerInfo.setValue(LoginPresenter.loggedInUser);
//	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		 boolean superValidate = super.validate();
	        
	        if(superValidate==false)
	        	return false;

			if (!reactOnValidate()) {
				return false;
			}
		 /** date 9.5.2019 added by komal for shift mandatory validation **/
        if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ProjectAllocation", "MakeShiftMandatory")){
        	for(EmployeeProjectAllocation empAllocation :employeeTable.getDataprovider().getList()){
        		if(empAllocation.getShift() == null || empAllocation.getShift().equals("") || empAllocation.getShift().equalsIgnoreCase("--SELECT--")){
        			 showDialogMessage("Please assign shift to all employeees.");
        			return false;
        		}
        	}
        }
		return true;
	}

	public PersonInfoComposite getPersonInfoComposite() {
		return personInfoComposite;
	}

	public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
		this.personInfoComposite = personInfoComposite;
	}

//	
	
	
}
