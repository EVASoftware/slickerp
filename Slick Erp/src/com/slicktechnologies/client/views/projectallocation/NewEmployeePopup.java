package com.slicktechnologies.client.views.projectallocation;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;

public class NewEmployeePopup extends AbsolutePanel{
	Button btnCancel,btnOk;
	ObjectListBox<Branch> olbbBranch;
	ObjectListBox<EmployeeInfo> olbEmployee;
	ObjectListBox<Shift> olbShift;
	ObjectListBox<Config> olbRole;
	InlineLabel label;
//	lbRole=new ObjectListBox<Config>();
//	AppUtility.MakeLiveConfig(lbRole,Screen.EMPLOYEEROLE);
	
	public NewEmployeePopup() {
		// TODO Auto-generated constructor stub
		label=new InlineLabel("Employee Details");
		add(label,150,10);
		
		InlineLabel lable1=new InlineLabel("Branch");
		add(lable1,10,30);
		olbbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbBranch);
		add(olbbBranch,10,50);
		
		InlineLabel lableRole=new InlineLabel("Role");
		add(lableRole,300,30);
		
		olbRole=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbRole,Screen.EMPLOYEEROLE);
		add(olbRole,300,50);
		
		InlineLabel lableEmployee=new InlineLabel("Employee");
		add(lableEmployee,10,80);
		
		olbEmployee=new ObjectListBox<EmployeeInfo>();
		EmployeeInfo.makeObjectListBoxLive(olbEmployee);
		add(olbEmployee,10,100);
		
		InlineLabel lableShift=new InlineLabel("Shift");
		add(lableShift,300,80);
		
		olbShift=new ObjectListBox<Shift>();
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Shift());
		olbShift.MakeLive(querry);
		
		add(olbShift,300,100);
		
		btnOk=new Button("  Ok  ");
		add(btnOk, 150, 150);
		btnCancel = new Button("Cancel");
		add(btnCancel,250,150);
		
		setSize("500px","300px");
		this.getElement().setId("form");
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public ObjectListBox<Branch> getOlbbBranch() {
		return olbbBranch;
	}

	public void setOlbbBranch(ObjectListBox<Branch> olbbBranch) {
		this.olbbBranch = olbbBranch;
	}

	public ObjectListBox<EmployeeInfo> getOlbEmployee() {
		return olbEmployee;
	}

	public void setOlbEmployee(ObjectListBox<EmployeeInfo> olbEmployee) {
		this.olbEmployee = olbEmployee;
	}

	public ObjectListBox<Shift> getOlbShift() {
		return olbShift;
	}

	public void setOlbShift(ObjectListBox<Shift> olbShift) {
		this.olbShift = olbShift;
	}

	public ObjectListBox<Config> getOlbRole() {
		return olbRole;
	}

	public void setOlbRole(ObjectListBox<Config> olbRole) {
		this.olbRole = olbRole;
	}

	public InlineLabel getLabel() {
		return label;
	}

	public void setLabel(InlineLabel label) {
		this.label = label;
	}
	
	
	
	public void clear(){
		olbbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbBranch);
		
		olbRole=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbRole,Screen.EMPLOYEEROLE);
		
		olbEmployee=new ObjectListBox<EmployeeInfo>();
		EmployeeInfo.makeObjectListBoxLive(olbEmployee);
		
		olbShift=new ObjectListBox<Shift>();
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Shift());
		olbShift.MakeLive(querry);
	}
	
	
}
