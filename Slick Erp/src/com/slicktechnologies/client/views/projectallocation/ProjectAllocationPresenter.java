package com.slicktechnologies.client.views.projectallocation;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.cnc.CNCForm;
import com.slicktechnologies.client.views.cnc.CNCPresenter;
import com.slicktechnologies.client.views.cnc.CncBillService;
import com.slicktechnologies.client.views.cnc.CncBillServiceAsync;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.humanresource.project.HrProjectForm;
import com.slicktechnologies.client.views.humanresource.project.HrProjectPresenter;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.FromAndToDateBoxPopup;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListPresenter;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderForm;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderPresenter;
import com.slicktechnologies.client.views.salesorder.SalesOrderForm;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;

public class ProjectAllocationPresenter extends
		FormScreenPresenter<ProjectAllocation> implements ClickHandler,SelectionHandler<Suggestion>{

	public static ProjectAllocationForm form;
	NewEmployeePopup employeePopUp;
	/** date 5.9.2018 added by komal**/
	CncBillServiceAsync cncBillService = GWT.create(CncBillService.class);
	FromAndToDateBoxPopup fromToDatePopup=new FromAndToDateBoxPopup();
	PopupPanel panel=new PopupPanel(true);
	CsvServiceAsync csvservice = GWT.create(CsvService.class);


	public ProjectAllocationPresenter(FormScreen<ProjectAllocation> view,ProjectAllocation model) {
		super(view, model);
		form = (ProjectAllocationForm) view;
		form.setPresenter(this);
		employeePopUp=new NewEmployeePopup();
		form.bt_addNewEmployee.addClickHandler(this);
		fromToDatePopup.getBtnOne().addClickHandler(this);
		fromToDatePopup.getBtnTwo().addClickHandler(this);
		
		form.personInfoComposite.getId().addSelectionHandler(this);
		form.personInfoComposite.getName().addSelectionHandler(this);
		form.personInfoComposite.getPhone().addSelectionHandler(this);
	}

	public static ProjectAllocationForm initalize() {
		ProjectAllocationForm form = new ProjectAllocationForm();
		ProjectAllocationPresenterTable gentable = new ProjectAllocationPresenterTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		ProjectAllocationPresenterSearch.staticSuperTable = gentable;
		ProjectAllocationPresenterSearch searchpopup = new ProjectAllocationPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);
		new ProjectAllocationPresenter(form, new ProjectAllocation());
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Human Resource/Project Allocation",Screen.PROJECTALLOCATION);
		AppMemory.getAppMemory().stickPnel(form);
		
		if(LoginPresenter.globalEmployeeInfo.size()==0) {
			Company c = new Company();
			String empInfoType=AppConstants.GLOBALRETRIEVALEMPLOYEEINFO+"-"+c.getCompanyId();
			LoginPresenter.globalMakeLiveEmployeeInfo(empInfoType);
		}
		if(LoginPresenter.shiftList.size()==0) {
			LoginPresenter.GlobalLoadShift();
		}
		
		return form;
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub

	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub

	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.ProjectAllocation")
	public static class ProjectAllocationPresenterSearch extends
			SearchPopUpScreen<ProjectAllocation> {

		@Override
		public MyQuerry getQuerry() {
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}
	};

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.ProjectAllocation")
	public static class ProjectAllocationPresenterTable extends
			SuperTable<ProjectAllocation> implements GeneratedVariableRefrence {

		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void createTable() {
			// TODO Auto-generated method stub
			
		}

		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub
			
		}
		
	};
	
	@Override
	public void reactOnSearch() {
		// TODO Auto-generated method stub
		super.reactOnSearch();
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
		if(text.equalsIgnoreCase(AppConstants.NEW)){
			reactToNew();
		}
		/** date 5.9.2018 added by komal**/
		if(text.equals(AppConstants.VIEWBILL)){

			final MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("contractCount");
			filter.setIntValue(model.getCount());
			temp.add(filter);
			
			querry.setFilters(temp);
			querry.setQuerryObject(new BillingDocument());
			form.showWaitSymbol();
			service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					if(result.size()==0){
						form.showDialogMessage("No billing document found.");
						return;
					}
					if(result.size()==1){
						final BillingDocument billDocument=(BillingDocument) result.get(0);
						final BillingDetailsForm form=BillingDetailsPresenter.initalize();
						Timer timer=new Timer() {
							@Override
							public void run() {
								form.updateView(billDocument);
								form.setToViewState();
							}
						};
						timer.schedule(1000);
					}else{
						BillingListPresenter.initalize(querry);
					}
					
				}
			});
		}
		/** date 5.9.2018 added by komal**/
		if(text.equals("Raise Bill")){
			panel=new PopupPanel(true);
			panel.add(fromToDatePopup);
			panel.center();
			panel.show();
		}
		
		if(text.equals("Allocate Overtime")){
			reactOnAllocateOvertime();
		}
		if (text.equalsIgnoreCase(AppConstants.VIEWCNC)) {

			final MyQuerry querry = new MyQuerry();
			Vector<Filter> temp = new Vector<Filter>();
			Filter filter = null;

			filter = new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(model.getContractId());
			temp.add(filter);

			querry.setFilters(temp);
			querry.setQuerryObject(new CNC());
			form.showWaitSymbol();
			service.getSearchResult(querry,
					new AsyncCallback<ArrayList<SuperModel>>() {
						@Override
						public void onFailure(Throwable caught) {
							form.hideWaitSymbol();
						}

						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							form.hideWaitSymbol();
							if (result.size() == 0) {
								form.showDialogMessage("No order document found.");
								return;
							} else {
								Console.log("CNC LOG 1");
								final CNC billDocument = (CNC) result.get(0);
								final CNCForm cncform = CNCPresenter.initalize();
								Console.log("CNC LOG 2");
//								cncform.loadCustomerBranch(form.getPersonInfoComposite().getIdValue(), null);
//								cncform.loadCustomerBranch(billDocument.getPersonInfo().getCount(), form.getOblCustomerBranch().getValue());
								Console.log("CNC LOG 3");
								Timer timer = new Timer() {
									@Override
									public void run() {
//										if(form.getOblCustomerBranch().getSelectedIndex()!=0){
//											cncform.getOblCustomerBranch().setValue(form.getOblCustomerBranch().getValue());
//										}
										Console.log("CNC LOG 4");
										cncform.updateView(billDocument);
										Console.log("CNC LOG 5");
										cncform.setToViewState();
										Console.log("CNC LOG 6");
									}
								};
								timer.schedule(4000);
							}

						}
					});

		}
	}

	private void reactOnAllocateOvertime() {
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("projectName");
		filter.setStringValue(model.getProjectName());
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new HrProject());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No Project found.");
					return;
				}
				if(result.size()!=0){
					final HrProject hrproject=(HrProject) result.get(0);
					final HrProjectForm hrform=HrProjectPresenter.initalize();
					hrform.loadCustomerBranch(form.getPersonInfoComposite().getIdValue(), null);
					Timer timer=new Timer() {
						@Override
						public void run() {
							if(form.getOblCustomerBranch().getSelectedIndex()!=0) {
								hrform.getOblCustomerBranch().setValue(form.getOblCustomerBranch().getValue());
							}
							hrform.updateView(hrproject);
							hrform.setToViewState();
						}
					};
					timer.schedule(4000);
				}
			}
		});
	
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		super.onClick(event);
		if(event.getSource()==form.bt_addNewEmployee){
			reactToAddEmployee();
		}
		if(event.getSource()==fromToDatePopup.getBtnOne()){
//		form.showWaitSymbol();
//		cncBillService.generateBill(fromToDatePopup.getFromDate().getValue(), fromToDatePopup.getToDate().getValue(), model, new AsyncCallback<String>() {
//
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				form.hideWaitSymbol();
//			}
//
//			@Override
//			public void onSuccess(String result) {
//				// TODO Auto-generated method stub
//				form.hideWaitSymbol();
//
//				form.showDialogMessage(result);
//			}
//		});
	}
	
	if(event.getSource()==fromToDatePopup.getBtnTwo()){
		panel.hide();
	}
	}

	private void reactToAddEmployee() {
		// TODO Auto-generated method stub
		EmployeeProjectAllocation employee=new EmployeeProjectAllocation();
		
		form.employeeTable.getDataprovider().getList().add(employee);
	}
	
	private void reactToNew() {
		form.setToNewState();
		initalize();
	}
	
	public void reactOnDownload() {
		ArrayList<ProjectAllocation> projectallocationArray = new ArrayList<ProjectAllocation>();
		List<ProjectAllocation> list = (List<ProjectAllocation>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
		projectallocationArray.addAll(list);
		csvservice.setProjectallocationlist(projectallocationArray, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed" + caught);
			}

			@Override
			public void onSuccess(Void result) {
				String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url = gwt + "csvservlet" + "?type=" + 156;
				Window.open(url, "test", "enabled");
			}
		});
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		if (event.getSource().equals(form.getPersonInfoComposite().getId())
				|| event.getSource().equals(
						form.getPersonInfoComposite().getName())
				|| event.getSource().equals(
						form.getPersonInfoComposite().getPhone())) {
			form.oblCustomerBranch.clear();
			form.loadCustomerBranch(form.getPersonInfoComposite().getIdValue(), null);
			
			form.employeeTable.setCustId(form.getPersonInfoComposite().getIdValue());
			form.employeeTable.setState(form.oblState.getValue());
			for(EmployeeProjectAllocation empProj:form.employeeTable.getValue()){
				empProj.setSiteLocation(null);
			}
			form.employeeTable.setEnable(true);
			
		}		
	}
	
	
	
	
	
}
