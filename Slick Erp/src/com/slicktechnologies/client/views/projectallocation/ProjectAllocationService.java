package com.slicktechnologies.client.views.projectallocation;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.slicktechnologies.shared.common.ProjectAllocation;

@RemoteServiceRelativePath("projectAllocationImpl")
public interface ProjectAllocationService extends RemoteService{
	ArrayList<ProjectAllocation> getProjectionDataofCustomer(
			ArrayList<Filter> filter);
	
}
