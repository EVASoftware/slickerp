package com.slicktechnologies.client.views.projectallocation;

import java.util.ArrayList;





import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.TextArea;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.views.contract.CancelContractTable;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;

public class DepartmentPopup extends PopupScreen implements ClickHandler{

	ObjectListBox<Department> olbDepartment;
	Button btAdd;
	DepartmentTable departmentTable;
	
	public DepartmentPopup(){
		super();
		departmentTable.connectToLocal();
		getPopup().setWidth("800px");
		departmentTable.getTable().setHeight("400px");
		departmentTable.getTable().setWidth("800px");
		btAdd.addClickHandler(this);
		createGui();
	}
	
	private void initializeWidget() {

		departmentTable = new DepartmentTable();
		olbDepartment = new ObjectListBox<Department>();
		Department.makeDepartMentListBoxLive(olbDepartment);
		btAdd = new Button("ADD");
	}

	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(btAdd)){
			if(olbDepartment.getSelectedIndex()!=0){
			Department dept = olbDepartment.getSelectedItem();
			if(dept.getHodName() == null || dept.getHodName().equals("") ){
				showDialogMessage("Please add HOD name in department.");
				return;
			}else if(dept.getEmail() == null || dept.getEmail().equals("")){
				showDialogMessage("Please add email of HOD in department.");
				return;
			}else{
				departmentTable.getDataprovider().getList().add(dept);
			}
			}else{
				showDialogMessage("Please select department.");
			}
		}
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingEmailInformation=fbuilder.setlabel("Email Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(2).build();
		fbuilder = new FormFieldBuilder("Department", olbDepartment);
		FormField folbDepartment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("", btAdd);
		FormField fbtAdd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("", departmentTable.getTable());
		FormField fdepartmentTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		FormField[][] formfield = {  
				{fgroupingEmailInformation},
				{folbDepartment , fbtAdd},
				{fdepartmentTable}
			
		};
		this.fields=formfield;

		
	}

	public ObjectListBox<Department> getOlbDepartment() {
		return olbDepartment;
	}

	public void setOlbDepartment(ObjectListBox<Department> olbDepartment) {
		this.olbDepartment = olbDepartment;
	}

	public Button getBtAdd() {
		return btAdd;
	}

	public void setBtAdd(Button btAdd) {
		this.btAdd = btAdd;
	}

	public DepartmentTable getDepartmentTable() {
		return departmentTable;
	}

	public void setDepartmentTable(DepartmentTable departmentTable) {
		this.departmentTable = departmentTable;
	}
	

}
