package com.slicktechnologies.client.views.projectallocation;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;

public class DepartmentTable extends SuperTable<Department> {
	
	TextColumn<Department> getDeptNameColumn;
	TextColumn<Department> getHodNameColumn;
	TextColumn<Department> getEmailColumn;
	public Object getVarRef(String varName)
	{
		if(varName.equals("getDeptNameColumn"))
			return this.getDeptNameColumn;
		if(varName.equals("getHodNameColumn"))
			return this.getHodNameColumn;
		if(varName.equals("getEmailColumn"))
			return this.getEmailColumn;
		
		return null ;
	}
	public DepartmentTable()
	{
		super();
	}
	@Override public void createTable() {
		
		addColumngetDeptName();
		addColumngetHodName();
		addColumngetEmail();
	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<Department>()
				{
			@Override
			public Object getKey(Department item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		addSortinggetDeptName();
		addSortinggetHodName();
		addSortinggetEmail();
	}
	@Override public void addFieldUpdater() {
	}
		protected void addSortinggetDeptName()
	{
		List<Department> list=getDataprovider().getList();
		columnSort=new ListHandler<Department>(list);
		columnSort.setComparator(getDeptNameColumn, new Comparator<Department>()
				{
			@Override
			public int compare(Department e1,Department e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getDeptName()!=null && e2.getDeptName()!=null){
						return e1.getDeptName().compareTo(e2.getDeptName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetDeptName()
	{
		getDeptNameColumn=new TextColumn<Department>()
				{
			@Override
			public String getValue(Department object)
			{
				return object.getDeptName()+"";
			}
				};
				table.addColumn(getDeptNameColumn,"Department");
				getDeptNameColumn.setSortable(true);
				table.setColumnWidth(getDeptNameColumn,"110px");
	}
	protected void addSortinggetEmail()
	{
		List<Department> list=getDataprovider().getList();
		columnSort=new ListHandler<Department>(list);
		columnSort.setComparator(getEmailColumn, new Comparator<Department>()
				{
			@Override
			public int compare(Department e1,Department e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmail()!=null && e2.getEmail()!=null){
						return e1.getEmail().compareTo(e2.getEmail());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetEmail()
	{
		getEmailColumn=new TextColumn<Department>()
				{
			@Override
			public String getValue(Department object)
			{
				return object.getEmail()+"";
			}
				};
				table.addColumn(getEmailColumn,"Email");
				getEmailColumn.setSortable(true);
				table.setColumnWidth(getEmailColumn,"110px");
	}
	protected void addSortinggetHodName()
	{
		List<Department> list=getDataprovider().getList();
		columnSort=new ListHandler<Department>(list);
		columnSort.setComparator(getHodNameColumn, new Comparator<Department>()
				{
			@Override
			public int compare(Department e1,Department e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getHodName()!=null && e2.getHodName()!=null){
						return e1.getHodName().compareTo(e2.getHodName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetHodName()
	{
		getHodNameColumn=new TextColumn<Department>()
				{
			@Override
			public String getValue(Department object)
			{
				return object.getHodName()+"";
			}
				};
				table.addColumn(getHodNameColumn,"H.O.D");
				getHodNameColumn.setSortable(true);
				table.setColumnWidth(getHodNameColumn,"110px");
	}
}
