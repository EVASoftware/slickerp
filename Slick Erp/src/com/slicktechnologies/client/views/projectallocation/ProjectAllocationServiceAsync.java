package com.slicktechnologies.client.views.projectallocation;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.slicktechnologies.shared.common.ProjectAllocation;

public interface ProjectAllocationServiceAsync {

	void getProjectionDataofCustomer(ArrayList<Filter> filter,
			AsyncCallback<ArrayList<ProjectAllocation>> callback);

}
