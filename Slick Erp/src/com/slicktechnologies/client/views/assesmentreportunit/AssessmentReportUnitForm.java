package com.slicktechnologies.client.views.assesmentreportunit;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.AssessmentReportUnit;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class AssessmentReportUnitForm extends FormTableScreen<AssessmentReportUnit> {

	ObjectListBox<Type> olbDeficiency;
	TextBox tbUnitName;
	CheckBox chkStatus;
	public AssessmentReportUnitForm(SuperTable<AssessmentReportUnit> table, int mode,boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
	}
	
	private void initalizeWidget() {
		
		olbDeficiency=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbDeficiency,Screen.ASSESSMENTREPORTDEFICIENCYTYPE);
		
		tbUnitName = new TextBox();
		chkStatus=new CheckBox();
		chkStatus.setValue(true); 
	}

	@Override
	public void createScreen() {
		initalizeWidget();
		this.processlevelBarNames=new String[]{"New"};
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCompanyInformation=fbuilder.setlabel("Deficiency Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("* Deficiency",olbDeficiency);
		FormField folbDeficiency= fbuilder.setMandatory(true).setMandatoryMsg("Deficiency is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Unit",tbUnitName);
		FormField ftbUnitName= fbuilder.setMandatory(true).setMandatoryMsg("Deficiency is mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",chkStatus);
		FormField fchkStatus= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		FormField[][] formfield = {   {fgroupingCompanyInformation},
				  {folbDeficiency,ftbUnitName,fchkStatus}
				};

		this.fields=formfield;
	}

	

	@Override
	public void updateModel(AssessmentReportUnit model) {
		
		if(olbDeficiency.getSelectedIndex()!=0){
			model.setDeficiency(olbDeficiency.getValue());
		}
		if(tbUnitName.getValue()!=null){
			model.setUnit(tbUnitName.getValue());
		}
		model.setStatus(chkStatus.getValue());
	}

	@Override
	public void updateView(AssessmentReportUnit view) {

		if(view.getDeficiency()!=null){
			olbDeficiency.setValue(view.getDeficiency());
		}
		if(view.getUnit()!=null){
			tbUnitName.setValue(view.getUnit());
		}
		if(view.getStatus()!=null)
		chkStatus.setValue(view.getStatus());
	}
	

	@Override
	public void toggleAppHeaderBarMenu() {

		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Edit")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.ASSESMENTREPORT,LoginPresenter.currentModule.trim());
			
	}

}
