package com.slicktechnologies.client.views.assesmentreportunit;

import java.util.Vector;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.AssessmentReportUnit;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class AssessmentReportUnitPresenter extends FormTableScreenPresenter<AssessmentReportUnit>{

	AssessmentReportUnitForm form;
	
	public AssessmentReportUnitPresenter(FormTableScreen<AssessmentReportUnit> view, AssessmentReportUnit model) {
		super(view, model);
		form=(AssessmentReportUnitForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getQuery());
		form.setPresenter(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.ASSESSMENTREPORTUNIT,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	
	private MyQuerry getQuery() {

		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new AssessmentReportUnit());
		return quer;
	}


	public static AssessmentReportUnitForm initalize()
	{
		AssessmentReportUnitTableProxy gentableScreen = new AssessmentReportUnitTableProxy();
		AssessmentReportUnitForm form = new AssessmentReportUnitForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		AssessmentReportUnitPresenter  presenter=new  AssessmentReportUnitPresenter  (form,new AssessmentReportUnit());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		if(text.equals("New"))
		{
			reactOnNew();
		}	
	}

	 private void reactOnNew()
	  {
		  form.setToNewState();
		  this.initalize();
	  }
	 
	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		model = new AssessmentReportUnit();
	}

}
