package com.slicktechnologies.client.views.assesmentreportunit;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.AssessmentReportUnit;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;

public class AssessmentReportUnitTableProxy extends SuperTable<AssessmentReportUnit>{

	TextColumn<AssessmentReportUnit> getcolumnId;
	TextColumn<AssessmentReportUnit> getcolumnDeficiency;
	TextColumn<AssessmentReportUnit> getcolumnUnit;
	TextColumn<AssessmentReportUnit> getcolumnStatus;
	
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		addColumnId();
		addColumnDeficiency();
		addColumnUnit();
		addColumnStatus();
		
	}

	private void addColumnStatus() {
		getcolumnStatus = new TextColumn<AssessmentReportUnit>() {
			
			@Override
			public String getValue(AssessmentReportUnit object) {
				if(object.getStatus()!=null){
					return "N.A";
				}
				if(object.getStatus()){
					return "Active";
				}else{
					return "InActive";
				}
			}
		};
		table.addColumn(getcolumnStatus,"Status");
		table.setColumnWidth(getcolumnStatus, 100,Unit.PX);
		getcolumnStatus.setSortable(true);
	}
	
	private void addColumnUnit() {
		getcolumnUnit = new TextColumn<AssessmentReportUnit>() {
			
			@Override
			public String getValue(AssessmentReportUnit object) {
				if(object.getUnit()!=null){
					return object.getUnit();
				}else{
					return "N.A";
				}
			}
		};
		table.addColumn(getcolumnUnit,"Unit");
		table.setColumnWidth(getcolumnUnit, 100,Unit.PX);
		getcolumnUnit.setSortable(true);
	}

	private void addColumnDeficiency() {
		getcolumnDeficiency = new TextColumn<AssessmentReportUnit>() {
			
			@Override
			public String getValue(AssessmentReportUnit object) {
				if(object.getDeficiency()!=null){
					return object.getDeficiency();
				}else{
					return "N.A";
				}
			}
		};
		table.addColumn(getcolumnDeficiency,"Deficiency");
		table.setColumnWidth(getcolumnDeficiency, 100,Unit.PX);
		getcolumnDeficiency.setSortable(true);

	}

	private void addColumnId() {
		getcolumnId = new TextColumn<AssessmentReportUnit>() {
			
			@Override
			public String getValue(AssessmentReportUnit object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getcolumnId,"Id");
		table.setColumnWidth(getcolumnId, 100,Unit.PX);
		getcolumnId.setSortable(true);

	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void addColumnSorting() {
		super.addColumnSorting();
		
		addSortingOnIdCol();
		addSortingOnDeficiencyNameCol();
		addSortingOnUnit();
		addSortingOnStatusCol();
	}


	private void addSortingOnStatusCol() {
		List<AssessmentReportUnit> list=getDataprovider().getList();
		columnSort=new ListHandler<AssessmentReportUnit>(list);
		columnSort.setComparator(getcolumnStatus, new Comparator<AssessmentReportUnit>()
		{
			@Override
			public int compare(AssessmentReportUnit e1,AssessmentReportUnit e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);		
	}

	private void addSortingOnUnit() {

		List<AssessmentReportUnit> list=getDataprovider().getList();
		columnSort=new ListHandler<AssessmentReportUnit>(list);
		columnSort.setComparator(getcolumnUnit, new Comparator<AssessmentReportUnit>()
		{
			@Override
			public int compare(AssessmentReportUnit e1,AssessmentReportUnit e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getUnit()!=null && e2.getUnit()!=null){
						return e1.getUnit().compareTo(e2.getUnit());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addSortingOnDeficiencyNameCol() {

		List<AssessmentReportUnit> list=getDataprovider().getList();
		columnSort=new ListHandler<AssessmentReportUnit>(list);
		columnSort.setComparator(getcolumnDeficiency, new Comparator<AssessmentReportUnit>()
		{
			@Override
			public int compare(AssessmentReportUnit e1,AssessmentReportUnit e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getDeficiency()!=null && e2.getDeficiency()!=null){
						return e1.getDeficiency().compareTo(e2.getDeficiency());}
				}
				else{
					return 0;}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
		
	
	}

	private void addSortingOnIdCol() {

		List<AssessmentReportUnit> list=getDataprovider().getList();
		columnSort=new ListHandler<AssessmentReportUnit>(list);
		columnSort.setComparator(getcolumnId, new Comparator<AssessmentReportUnit>()
		{
			@Override
			public int compare(AssessmentReportUnit e1,AssessmentReportUnit e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;
					}
				}
				else{
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
		
	
	}


}
