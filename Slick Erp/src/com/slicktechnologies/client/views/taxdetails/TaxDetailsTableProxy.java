package com.slicktechnologies.client.views.taxdetails;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.othercharges.OtherChargesPresenter.OtherChargesPresenterTable;
import com.slicktechnologies.client.views.taxdetails.TaxDetailsPresenter.TaxDetailsPresenterTable;
import com.slicktechnologies.shared.OtherTaxCharges;
import com.slicktechnologies.shared.TaxDetails;

public class TaxDetailsTableProxy extends TaxDetailsPresenterTable {
TextColumn<TaxDetails> getCountColumn;
TextColumn<TaxDetails> getTaxDetailsNameColumn;
TextColumn<TaxDetails> getTaxDetailsPercentColumn;
TextColumn<TaxDetails> getGLAccountName;
TextColumn<TaxDetails> getTaxDetailsStatusColummn;
TextColumn<TaxDetails> getTaxDetailsCentralTaxColummn;
TextColumn<TaxDetails> getTaxDetailsServiceTaxColummn;
TextColumn<TaxDetails> getTaxDetailsVatTaxColummn;



public TaxDetailsTableProxy() {
	super();
}
@Override
	public void createTable() {
		// TODO Auto-generated method stub
	addSortinggetCount();
	addColumngetCount();
	addColumngetTaxChargeName();
	addColumngetTaxDetailsPercent();
	addColumngetGLAccountName();
	addColumngetTaxDetailsCentralTax();
	addColumngetTaxDetailsServiceTax();
	addColumngetTaxDetailsVatTax();
	addColumngetTaxDetailsStatus();
	
	

	}

public void addColumnSorting(){
	addSortinggetCount();
	addSortinggetTaxDetailsName();
	addSortinggetTaxDetailsPercent();
	addSortinggetGLAccount();
	addSortinggetTaxDetailsStatus();
	addSortinggetTaxDetailsCentralTax();
	addSortinggetTaxDetailsServiceTax();
	addSortinggetTaxDetailsVatTax();
}

protected void addSortinggetCount()
{
	List<TaxDetails> list=getDataprovider().getList();
	columnSort=new ListHandler<TaxDetails>(list);
	columnSort.setComparator(getCountColumn, new Comparator<TaxDetails>()
			{
		@Override
		public int compare(TaxDetails e1,TaxDetails e2)
		{
			if(e1!=null && e2!=null)
			{
				if(e1.getCount()== e2.getCount()){
					return 0;}
				if(e1.getCount()> e2.getCount()){
					return 1;}
				else{
					return -1;}
			}
			else{
				return 0;}
		}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetCount()
{
	getCountColumn=new TextColumn<TaxDetails>() {

		@Override
		public String getValue(TaxDetails object) {
			// TODO Auto-generated method stub
			if(object.getCount()==-1)
				return "N.A";
			else return object.getCount()+"";
		
		}
	};
	table.addColumn(getCountColumn, "ID");
	getCountColumn.setSortable(true);
}


protected void addSortinggetTaxDetailsName()
{
	List<TaxDetails> list=getDataprovider().getList();
	columnSort=new ListHandler<TaxDetails>(list);
	columnSort.setComparator(getTaxDetailsNameColumn, new Comparator<TaxDetails>()
			{
		@Override
		public int compare(TaxDetails e1,TaxDetails e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getTaxChargeName()!=null && e2.getTaxChargeName()!=null){
					return e1.getTaxChargeName().compareTo(e2.getTaxChargeName());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}


protected void addColumngetTaxChargeName()
{
	getTaxDetailsNameColumn=new TextColumn<TaxDetails>()
			{
		@Override
		public String getValue(TaxDetails object)
		{
			return object.getTaxChargeName().toUpperCase().trim();
		}
			};
			table.addColumn(getTaxDetailsNameColumn,"Tax Name");
			getTaxDetailsNameColumn.setSortable(true);
}
protected void addSortinggetTaxDetailsPercent()
{
	List<TaxDetails> list=getDataprovider().getList();
	columnSort=new ListHandler<TaxDetails>(list);
	columnSort.setComparator(getTaxDetailsPercentColumn, new Comparator<TaxDetails>()
			{
		@Override
		public int compare(TaxDetails e1,TaxDetails e2)
		{
			if(e1!=null && e2!=null)
			{
				if(e1.getTaxChargePercent()!=null && e2.getTaxChargePercent()!=null){
					return e1.getTaxChargePercent().compareTo(e2.getTaxChargePercent());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetTaxDetailsPercent()
{
	getTaxDetailsPercentColumn=new TextColumn<TaxDetails>()
			{
		@Override
		public String getValue(TaxDetails object)
		{
			if(object.getTaxChargePercent()==null)
			{
				return "";
			}
			else
			{
			return object.getTaxChargePercent()+"";
			}
		}
			};
			table.addColumn(getTaxDetailsPercentColumn,"Percent");
			getTaxDetailsPercentColumn.setSortable(true);
}

protected void addSortinggetGLAccount()
{
	List<TaxDetails> list=getDataprovider().getList();
	columnSort=new ListHandler<TaxDetails>(list);
	columnSort.setComparator(getGLAccountName, new Comparator<TaxDetails>()
			{
		@Override
		public int compare(TaxDetails e1,TaxDetails e2)
		{
			if(e1!=null && e2!=null)
			{
				if(e1.getGlAccountName()!=null && e2.getGlAccountName()!=null){
					return e1.getGlAccountName().compareTo(e2.getGlAccountName());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetGLAccountName()
{
	getGLAccountName=new TextColumn<TaxDetails>()
			{
		@Override
		public String getValue(TaxDetails object)
		{
			return object.getGlAccountName().trim();
		}
			};
			table.addColumn(getGLAccountName,"GL Account");
			getGLAccountName.setSortable(true);
}


protected void addSortinggetTaxDetailsCentralTax()
{
	List<TaxDetails> list=getDataprovider().getList();
	columnSort=new ListHandler<TaxDetails>(list);
	columnSort.setComparator(getTaxDetailsCentralTaxColummn, new Comparator<TaxDetails>()
			{
		@Override
		public int compare(TaxDetails e1,TaxDetails e2)
		{
			if(e1!=null && e2!=null)
			{
				if(e1.getCentralTax()!=null && e2.getTaxChargeStatus()!=null){
					return e1.getCentralTax().compareTo(e2.getCentralTax());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}


protected void addColumngetTaxDetailsCentralTax()
{
	getTaxDetailsCentralTaxColummn=new TextColumn<TaxDetails>()
	{
		@Override
		public String getValue(TaxDetails object)
		{
			if(object.getCentralTax()==true){
              	return AppConstants.YES;
			}
             else{
              	return AppConstants.NO;
             }
		}
	};
	table.addColumn(getTaxDetailsCentralTaxColummn,"Central Tax");
	getTaxDetailsCentralTaxColummn.setSortable(true);
}


protected void addSortinggetTaxDetailsServiceTax()
{
	List<TaxDetails> list=getDataprovider().getList();
	columnSort=new ListHandler<TaxDetails>(list);
	columnSort.setComparator(getTaxDetailsServiceTaxColummn, new Comparator<TaxDetails>()
			{
		@Override
		public int compare(TaxDetails e1,TaxDetails e2)
		{
			if(e1!=null && e2!=null)
			{
				if(e1.getServiceTax()!=null && e2.getServiceTax()!=null){
					return e1.getServiceTax().compareTo(e2.getServiceTax());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}


protected void addColumngetTaxDetailsServiceTax()
{
	getTaxDetailsServiceTaxColummn=new TextColumn<TaxDetails>()
	{
		@Override
		public String getValue(TaxDetails object)
		{
			if(object.getServiceTax()==true){
              	return AppConstants.YES;
			}
             else{
              	return AppConstants.NO;
             }
		}
	};
	table.addColumn(getTaxDetailsServiceTaxColummn,"Service Tax");
	getTaxDetailsServiceTaxColummn.setSortable(true);
}

protected void addSortinggetTaxDetailsVatTax()
{
	List<TaxDetails> list=getDataprovider().getList();
	columnSort=new ListHandler<TaxDetails>(list);
	columnSort.setComparator(getTaxDetailsVatTaxColummn, new Comparator<TaxDetails>()
			{
		@Override
		public int compare(TaxDetails e1,TaxDetails e2)
		{
			if(e1!=null && e2!=null)
			{
				if(e1.getVatTax()!=null && e2.getVatTax()!=null){
					return e1.getVatTax().compareTo(e2.getVatTax());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}


protected void addColumngetTaxDetailsVatTax()
{
	getTaxDetailsVatTaxColummn=new TextColumn<TaxDetails>()
	{
		@Override
		public String getValue(TaxDetails object)
		{
			if(object.getVatTax()==true){
              	return AppConstants.YES;
			}
             else{
              	return AppConstants.NO;
             }
		}
	};
	table.addColumn(getTaxDetailsVatTaxColummn,"VAT");
	getTaxDetailsVatTaxColummn.setSortable(true);
}



protected void addSortinggetTaxDetailsStatus()
{
	List<TaxDetails> list=getDataprovider().getList();
	columnSort=new ListHandler<TaxDetails>(list);
	columnSort.setComparator(getTaxDetailsStatusColummn, new Comparator<TaxDetails>()
			{
		@Override
		public int compare(TaxDetails e1,TaxDetails e2)
		{
			if(e1!=null && e2!=null)
			{
				if(e1.getTaxChargeStatus()!=null && e2.getTaxChargeStatus()!=null){
					return e1.getTaxChargeStatus().compareTo(e2.getTaxChargeStatus());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}


protected void addColumngetTaxDetailsStatus()
{
	getTaxDetailsStatusColummn=new TextColumn<TaxDetails>()
	{
		@Override
		public String getValue(TaxDetails object)
		{
			if(object.getTaxChargeStatus()==true){
              	return "Active";
			}
             else{
              	return "In Active";
             }
		}
	};
	table.addColumn(getTaxDetailsStatusColummn,"Status");
	getTaxDetailsStatusColummn.setSortable(true);
}



}
