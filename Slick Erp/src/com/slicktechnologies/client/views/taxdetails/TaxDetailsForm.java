package com.slicktechnologies.client.views.taxdetails;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.GLAccount;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.simplesoftwares.client.library.FormField;

public class TaxDetailsForm  extends FormTableScreen<TaxDetails>{
	TextBox tbtaxdetailschargename;
	TextBox tbtaxId;
	DoubleBox dotdpercent;
	TextArea tadescription;
	CheckBox cbtdstatus;
	ObjectListBox<GLAccount> olbglaccount;
	RadioButton rbServiceTax,rbVatTax,rbCentralTax;
	TextBox taxPrintableName;
	
	public TaxDetailsForm(SuperTable<TaxDetails> table ,int mode,boolean captionmode)
	{
		super(table, mode, captionmode);
		createGui();
	}
	
	
	private void initalizeWidget()
	{
		tbtaxId=new TextBox();
		tbtaxId.setEnabled(false);
		tbtaxdetailschargename=new TextBox();
		dotdpercent =new DoubleBox();
		tadescription=new TextArea();
		cbtdstatus=new CheckBox();
		cbtdstatus.setValue(true);
		olbglaccount=new ObjectListBox<GLAccount>();
		GLAccount.initializeGLAccount(olbglaccount);
		rbServiceTax=new RadioButton("grp1","");
		rbVatTax=new RadioButton("grp1","");
		rbCentralTax=new RadioButton("grp1","");
		
		/**
		 *    rohan added this code for GST implementation on Date: 22-06-2017
		 */
		taxPrintableName = new TextBox();
		/****************************************************************************/
		
	}
	
	@Override
	public void createScreen() {
		initalizeWidget();
		
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","HideOrShowCreateServicesButton"))
//		{
//			this.processlevelBarNames=new String[]{"Create GST Tax"};
//		} 
		
	FormFieldBuilder fBuilder=new FormFieldBuilder();
	 
	FormField fgroupingTaxInformation=fBuilder.setlabel("Tax Charges Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build(); 
	fBuilder=new FormFieldBuilder("Tax ID",tbtaxId);
	FormField ftbtaxid=fBuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	fBuilder=new FormFieldBuilder("* Tax Name",tbtaxdetailschargename);
	FormField ftbtaxchargename=fBuilder.setMandatory(true).setMandatoryMsg("Tax Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
	fBuilder=new FormFieldBuilder("* Percent",dotdpercent);
	FormField fdtdpercent=fBuilder.setMandatory(true).setMandatoryMsg("Percent is Mandatory!").setRowSpan(0).setColSpan(0).build();
	fBuilder=new FormFieldBuilder("* GL Account Name",olbglaccount);
	FormField folbglaccount=fBuilder.setMandatory(true).setMandatoryMsg("GL Account Name is Mandatory! ").setRowSpan(0).setColSpan(0).build();
	fBuilder=new FormFieldBuilder("Status",cbtdstatus);
	FormField fcbtdstatus=fBuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	fBuilder=new FormFieldBuilder("Central Tax",rbCentralTax);
	FormField frbCentralTax=fBuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	fBuilder=new FormFieldBuilder("Tax 1",rbServiceTax);
	FormField frbServiceTax=fBuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	fBuilder=new FormFieldBuilder("Tax 2",rbVatTax);
	FormField frbVatTax=fBuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	fBuilder=new FormFieldBuilder("Description",tadescription);
	FormField ftadescription=fBuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	fBuilder=new FormFieldBuilder("* Tax Print Name",taxPrintableName);
	FormField ftaxPrintableName=fBuilder.setMandatory(true).setMandatoryMsg("Tax Print Name Is Mandetory..!").setRowSpan(0).setColSpan(1).build();
	
	FormField[][] formFields={{fgroupingTaxInformation},
			{ftbtaxid,ftbtaxchargename,fdtdpercent,folbglaccount},
			{frbCentralTax,frbServiceTax,frbVatTax,fcbtdstatus},
			{ftaxPrintableName},
			{ftadescription},
			};
	this.fields=formFields;
	}
	
	@Override
	public void updateModel(TaxDetails model)
	{
		if(tbtaxdetailschargename.getValue()!=null)
			model.setTaxChargeName(tbtaxdetailschargename.getValue().toUpperCase());
		if(dotdpercent.getValue()!=null)
			model.setTaxChargePercent(dotdpercent.getValue());
		if(olbglaccount.getValue()!=null)
			model.setGlAccountName(olbglaccount.getValue());
		if(cbtdstatus.getValue()!=null)
			model.setTaxChargeStatus(cbtdstatus.getValue());
		if(rbCentralTax.getValue()!=null)
			model.setCentralTax(rbCentralTax.getValue());
		if(rbServiceTax.getValue()!=null)
			model.setServiceTax(rbServiceTax.getValue());
		if(rbVatTax.getValue()!=null)
			model.setVatTax(rbVatTax.getValue());
		if(tadescription.getValue()!=null)
			model.setTaxDescription(tadescription.getValue());
		
		if(taxPrintableName.getValue()!=null ){
			model.setTaxPrintName(taxPrintableName.getValue());
		}

		presenter.setModel(model);
	}
	@Override
	public void updateView(TaxDetails view) {
		tbtaxId.setValue(view.getCount()+"");
		if(view.getTaxChargeName()!=null)
			tbtaxdetailschargename.setValue(view.getTaxChargeName());
		if(view.getTaxChargePercent()!=null)
			dotdpercent.setValue(view.getTaxChargePercent());
		if(view.getGlAccountName()!=null)
			olbglaccount.setValue(view.getGlAccountName());
		if(view.getTaxChargeStatus()!=null)
			cbtdstatus.setValue(view.getTaxChargeStatus());
		if(view.getCentralTax()!=null)
			rbCentralTax.setValue(view.getCentralTax());
		if(view.getServiceTax()!=null)
			rbServiceTax.setValue(view.getServiceTax());
		if(view.getVatTax()!=null)
			rbVatTax.setValue(view.getVatTax());
		if(view.getTaxDescription()!=null)
			tadescription.setValue(view.getTaxDescription());
		
		if(view.getTaxPrintName()!=null && !view.getTaxPrintName().equals(""))
			taxPrintableName.setValue(view.getTaxPrintName());
		
		
		presenter.setModel(view);
	}
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		tbtaxId.setEnabled(false);
		cbtdstatus.setValue(true);
	}
	
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

//		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
//		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
//			for(int k=0;k<menus.length;k++)
//			{
//				String text=menus[k].getText();
//				if(text.equals("Save")||text.equals("Discard"))
//					menus[k].setVisible(true); 
//
//				else
//					menus[k].setVisible(false);  		   
//			}
//		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
//				if(text.equals("Discard")||text.equals("Edit"))
				if(text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.TAXDETAILS,LoginPresenter.currentModule.trim());
	}


	@Override
	public boolean validate() {
		boolean superres=super.validate();
		boolean percentval=false;
		
		NumberFormat nf=NumberFormat.getFormat("0.00");
		if(Double.parseDouble(nf.format(this.dotdpercent.getValue()))==this.dotdpercent.getValue()){
			percentval=true;
		}
		else{
			percentval=false;
			showDialogMessage("Please enter percent in proper format");
		}
		
		return superres&&percentval;
	}

	

	}
