package com.slicktechnologies.client.views.taxdetails;

import java.util.Date;
import java.util.Vector;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.othercharges.OtherChargesForm;
import com.slicktechnologies.client.views.othercharges.OtherChargesPresenter;
import com.slicktechnologies.client.views.othercharges.OtherChargesTableProxy;
import com.slicktechnologies.client.views.othercharges.OtherChargesPresenter.OtherChargesPresenterTable;
import com.slicktechnologies.shared.OtherTaxCharges;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class TaxDetailsPresenter extends FormTableScreenPresenter<TaxDetails> {
	
	TaxDetailsForm form;
	public TaxDetailsPresenter(FormTableScreen<TaxDetails> view,TaxDetails model) {
		
		super(view,model);
		form=(TaxDetailsForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getTaxConfigQuery());
		form.setPresenter(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.TAXDETAILS,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	public static void initalize()
	{
		TaxDetailsPresenterTable gentableScreen=new TaxDetailsTableProxy();
		TaxDetailsForm form=new TaxDetailsForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		TaxDetailsPresenter presenter=new TaxDetailsPresenter(form,new TaxDetails());
		AppMemory.getAppMemory().stickPnel(form);
	}
	public MyQuerry getTaxConfigQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new TaxDetails());
		return quer;
	}

	@Override
	protected void makeNewModel() {
		model=new TaxDetails();	
	}
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();

		if(text.equals("Create GST Tax")) {
			createGSTTaxDetails();
		}

	}

	private void createGSTTaxDetails() {
		
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		
		
	}
	
	 @EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.TaxDetails")
	 public static class TaxDetailsPresenterTable extends SuperTable<TaxDetails> implements GeneratedVariableRefrence{

		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void createTable() {
			// TODO Auto-generated method stub
			
		}

		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub
			
		}

		
		}

	




}
