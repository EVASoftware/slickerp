package com.slicktechnologies.client.views.techniciandashboard;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.composites.EmployeeInfoComposite;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class TechnicianDashboardForm extends FormScreen<DummyEntityOnlyForScreen> {

	
	public EmployeeInfoComposite employeeInfo;
	ObjectListBox<Branch> olbbranch;
	public TechnicianDashboardTableProxy tecnicianTable;
	Label lbfrmdate,lbtodate,lblbranch,lblAssigned,lblCompleted,lblRework;
	TextBox tbtotal,tbAssigned,tbCompleted,tbRework,tbblank;
	DateBox dbFromDate;
	DateBox dbToDate;
	Button btnGo;
	
	TextBox tbCancelled;
	
	public TechnicianDashboardForm() {
		super();
		createGui();
		tecnicianTable.connectToLocal();
	}	
	
	
	private void initializeWidget(){
		
		employeeInfo=AppUtility.employeeInfoComposite(new EmployeeInfo(),false);
		
		tecnicianTable = new TechnicianDashboardTableProxy();

		lbfrmdate = new Label("From Date");
		lbtodate = new Label("To Date");
		
		dbFromDate = new DateBoxWithYearSelector();
		dbToDate = new DateBoxWithYearSelector();
		
		lblbranch = new Label("Branch");
		
		olbbranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbranch);
		
		btnGo = new Button("Go");
		
		tbblank =new TextBox();
		
		tbAssigned = new TextBox();
		tbAssigned.setEnabled(false);
		tbCompleted = new TextBox();
		tbCompleted.setEnabled(false);
		tbRework = new TextBox();
		tbRework.setEnabled(false);
		tbtotal = new TextBox();
		tbtotal.setEnabled(false);

		tbCancelled = new TextBox();
		tbCancelled.setEnabled(false);
		
	}

	@Override
	public void createScreen() {

		// TODO Auto-generated method stub
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		FormField fgroupingEmployeeInformation=fbuilder.setlabel("Employee Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		
		fbuilder = new FormFieldBuilder("",employeeInfo);
		FormField fperson= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",lblbranch);
		FormField flblbranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",olbbranch);
		FormField folbbranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",lbfrmdate);
		FormField flbfrmdate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",lbtodate);
		FormField flbtodate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",dbFromDate);
		FormField fdbFromDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",dbToDate);
		FormField fdbToDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnGo);
		FormField fbtnGo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fservicecountgrouping=fbuilder.setlabel("Service Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(8).build();
		
		fbuilder = new FormFieldBuilder("Scheduled/Rescheduled",tbAssigned);
		FormField ftbAssigned= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Completed",tbCompleted);
		FormField ftbCompleted= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Rework",tbRework);
		FormField ftbRework= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total",tbtotal);
		FormField ftbtotal= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fblank=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		FormField fservicegrouping=fbuilder.setlabel("Service Deatils").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(8).build();
		
		fbuilder = new FormFieldBuilder("",tecnicianTable.getTable());
		FormField ftechnicintable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(8).build();
		
		fbuilder = new FormFieldBuilder("Cancelled",tbCancelled);
		FormField ftbCancelled= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		FormField[][] formFields = {   
				{fgroupingEmployeeInformation},
				{fperson},
				{flblbranch,flbfrmdate,flbtodate,fblank},
				{folbbranch,fdbFromDate,fdbToDate,fbtnGo},
				{fblank},
				{fservicecountgrouping},
				{ftbtotal,ftbAssigned,ftbCompleted,ftbRework,ftbCancelled},
				{fblank},
				{fservicegrouping},
				{ftechnicintable}
				
		};
		this.fields=formFields;
	
	}


	@Override
	public void toggleAppHeaderBarMenu() {

		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Download"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals("Download"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Download"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.TECHNICIANDASHBOARD,LoginPresenter.currentModule.trim());

	}

	@Override
	public void updateModel(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void updateView(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		
	}

	public ObjectListBox<Branch> getOlbbranch() {
		return olbbranch;
	}

	public void setOlbbranch(ObjectListBox<Branch> olbbranch) {
		this.olbbranch = olbbranch;
	}

	public DateBox getDbFromDate() {
		return dbFromDate;
	}

	public void setDbFromDate(DateBox dbFromDate) {
		this.dbFromDate = dbFromDate;
	}

	public DateBox getDbToDate() {
		return dbToDate;
	}

	public void setDbToDate(DateBox dbToDate) {
		this.dbToDate = dbToDate;
	}


	public TextBox getTbAssigned() {
		return tbAssigned;
	}


	public void setTbAssigned(TextBox tbAssigned) {
		this.tbAssigned = tbAssigned;
	}


	public TextBox getTbCompleted() {
		return tbCompleted;
	}


	public void setTbCompleted(TextBox tbCompleted) {
		this.tbCompleted = tbCompleted;
	}


	public TextBox getTbRework() {
		return tbRework;
	}


	public void setTbRework(TextBox tbRework) {
		this.tbRework = tbRework;
	}


	public TextBox getTbtotal() {
		return tbtotal;
	}


	public void setTbtotal(TextBox tbtotal) {
		this.tbtotal = tbtotal;
	}


	public TechnicianDashboardTableProxy getTecnicianTable() {
		return tecnicianTable;
	}


	public void setTecnicianTable(TechnicianDashboardTableProxy tecnicianTable) {
		this.tecnicianTable = tecnicianTable;
	}


	public TextBox getTbCancelled() {
		return tbCancelled;
	}


	public void setTbCancelled(TextBox tbCancelled) {
		this.tbCancelled = tbCancelled;
	}

	/**
	 *  29-12-2020 Added by Priyanka issue raised by Vaishali Mam.
	 */
	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		tecnicianTable.getTable().redraw();
	}
	
	
	
}
