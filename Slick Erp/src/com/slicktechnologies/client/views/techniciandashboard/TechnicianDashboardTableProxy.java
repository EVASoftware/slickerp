package com.slicktechnologies.client.views.techniciandashboard;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.salesperson.TargetInformation;

public class TechnicianDashboardTableProxy extends SuperTable<Service>{

	TextColumn<Service> getcolumnContractId;
	TextColumn<Service> getcolumnCustomerName;
	TextColumn<Service> getcolumnProductName;
	TextColumn<Service> getcolumnServiceDate;
	TextColumn<Service> getcolumnBranch;
	TextColumn<Service> getcolumnServiceEngineer;
	TextColumn<Service> getcolumnStatus;
	TextColumn<Service> getcolumnServiceId;

	TechnicianDashboardTableProxy(){
		super();
		setHeight("250px");
	}
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		addColumnBranch();
		addColumnServiceEngineer();
		addColumnServiceDate();
		addColumnProductName();
		addColumnStatus();
		addColumnContractId();
		addColumnServiceId();
		addColumnCustomerName();
	}

	private void addColumnServiceId() {
		getcolumnServiceId = new TextColumn<Service>() {
			@Override
			public String getValue(Service object) {
				// TODO Auto-generated method stub
				return object.getCount()+"";
			}
		};
		table.addColumn(getcolumnServiceId,"Service Id");
		table.setColumnWidth(getcolumnServiceId, 80,Unit.PX);
	}

	private void addColumnContractId() {
		
		getcolumnContractId = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				return object.getContractCount()+"";
			}
		};
		table.addColumn(getcolumnContractId,"Contract Id");
		table.setColumnWidth(getcolumnContractId, 80,Unit.PX);
	}

	private void addColumnCustomerName() {

		getcolumnCustomerName = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				return object.getCustomerName();
			}
		};
		table.addColumn(getcolumnCustomerName,"Customer Name");
		table.setColumnWidth(getcolumnCustomerName, 80,Unit.PX);
	}

	private void addColumnProductName() {
		
		getcolumnProductName = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				return object.getProductName();
			}
		};
		table.addColumn(getcolumnProductName,"Product Name");
		table.setColumnWidth(getcolumnProductName, 80,Unit.PX);
	}

	private void addColumnServiceDate() {
		getcolumnServiceDate = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				return AppUtility.parseDate(object.getServiceDate());
			}
		};
		table.addColumn(getcolumnServiceDate,"Service Date");
		table.setColumnWidth(getcolumnServiceDate, 80,Unit.PX);
	}

	private void addColumnBranch() {
		getcolumnBranch = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				return object.getBranch();
			}
		};
		table.addColumn(getcolumnBranch,"Branch");
		table.setColumnWidth(getcolumnBranch, 80,Unit.PX);
		
	}

	private void addColumnServiceEngineer() {

		getcolumnServiceEngineer = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				return object.getEmployee()+"";
			}
		};
		table.addColumn(getcolumnServiceEngineer,"Service Engineer");
		table.setColumnWidth(getcolumnServiceEngineer, 80,Unit.PX);
	}

	private void addColumnStatus() {

		getcolumnStatus = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				return object.getStatus();
			}
		};
		table.addColumn(getcolumnStatus,"Status");
		table.setColumnWidth(getcolumnStatus, 80,Unit.PX);
	}



	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
