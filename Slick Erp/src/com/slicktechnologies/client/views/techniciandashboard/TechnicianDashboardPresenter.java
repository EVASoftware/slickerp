package com.slicktechnologies.client.views.techniciandashboard;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.TechnicianDashboardService;
import com.slicktechnologies.client.services.TechnicianDashboardServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class TechnicianDashboardPresenter extends FormScreenPresenter<DummyEntityOnlyForScreen> implements ClickHandler{

	/* Technician Dashboard developed by Vijay
	 *  Date - 26 sep 2016
	 *  Release Date - 30 sep 2016
	 *  
	 */
	
	TechnicianDashboardForm form;
	final GenricServiceAsync async =GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	NumberFormat numformat = NumberFormat.getFormat("0.00");
	
	TechnicianDashboardServiceAsync techsereviceAsync = GWT.create(TechnicianDashboardService.class);
	
	public TechnicianDashboardPresenter(UiScreen<DummyEntityOnlyForScreen> view,DummyEntityOnlyForScreen model) {
		super(view, model);
		form = (TechnicianDashboardForm) view;
		form.btnGo.addClickHandler(this);
		
		/* 
		 *  below code for To load FromDate last month 1st date and ToDate last month end date
		 */
		Date date= new Date();
		int month = date.getMonth();
		form.dbToDate.setValue(calulateEndDateOfMonth(month));
		form.dbFromDate.setValue(calulateStartDateOfMonth(month));
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.TECHNICIANDASHBOARD,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	/*
	 * Below method accept current date Month and calculate last month 1st Date
	 */
	
	private Date calulateStartDateOfMonth(int selectedMonth) {

		DateTimeFormat fmt = DateTimeFormat.getFormat("yyyy-MM-dd");
		System.out.println("Inside Start Month ");
		System.out.println("Selected Month "+selectedMonth);
		String startDate;
		
		Date date = new Date();
		date.setDate(1);
		date.setMonth(selectedMonth-1);
		
		startDate=fmt.format(date);
		
		System.out.println("Start Date Of Month "+startDate);
		Console.log("START DATE OF MONTH "+startDate);
		Date lastmonthstartDate = fmt.parse(startDate);
		System.out.println(" Date =="+lastmonthstartDate);
		return lastmonthstartDate;
	
	}

	/*
	 * Below method accept current date Month and calculate last month End Date
	 */
	private Date calulateEndDateOfMonth(int selectedMonth) {
		DateTimeFormat fmt = DateTimeFormat.getFormat("yyyy-MM-dd");
		System.out.println("Inside End Month ");
		System.out.println("Selected Month "+selectedMonth);
		String endDate;
		
		Date date = new Date();
		
		date.setMonth(selectedMonth);
		CalendarUtil.setToFirstDayOfMonth(date);
		CalendarUtil.addDaysToDate(date, -1);
		endDate=fmt.format(date);
		Console.log("END DATE OF MONTH "+endDate);
		System.out.println(" end date =="+endDate);
		
		 Date lastmonthendDate = fmt.parse(endDate);
		 System.out.println(" End date =="+lastmonthendDate);
		return lastmonthendDate;
	
	}


	public static void initalize()
	{
		
		TechnicianDashboardForm homeform = new TechnicianDashboardForm();
		AppMemory.getAppMemory().currentScreen = Screen.TECHNICIANDASHBOARD;
		TechnicianDashboardPresenter presentor = new TechnicianDashboardPresenter(homeform, new DummyEntityOnlyForScreen());
		AppMemory.getAppMemory().stickPnel(homeform);
		
		
	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		if(event.getSource()== form.btnGo){
			System.out.println(" inside go btn");
			
			form.getTbAssigned().setValue("");
			form.getTbCompleted().setValue("");
			form.getTbRework().setValue("");
			form.getTbtotal().setValue("");
			form.getTecnicianTable().clear();
			
			
			if(form.dbFromDate.getValue()==null && form.dbToDate.getValue()==null || form.dbFromDate.getValue()==null || form.dbToDate.getValue()==null){
				form.showDialogMessage("Please Select From and To Date");
			}
			else if(form.employeeInfo.getEmployeeName().equals("") && form.getOlbbranch().getSelectedIndex()==0){
				form.showDialogMessage("please select Employee or Branch");
			}
			else if(!form.employeeInfo.getEmployeeName().equals("") && form.getOlbbranch().getSelectedIndex()!=0){
				form.showDialogMessage("please select either Employee or Branch");
				form.employeeInfo.clear();
				form.olbbranch.setSelectedIndex(0);
			}
			else{
				getAllDetails(form.dbFromDate.getValue(),form.dbToDate.getValue());
			}
		}
	}

	private void getAllDetails(Date fromdate, Date todate) {

		if(fromdate!=null && todate!=null && !form.employeeInfo.getEmployeeName().equals("")){
			
			getemployeeDetails(fromdate,todate,form.employeeInfo.getEmployeeName());
				
		}
		
		if(fromdate!=null && todate!=null && form.getOlbbranch().getSelectedIndex()!=0){
			getbranchWiseDeatils(fromdate,todate,form.getOlbbranch().getValue());
		}
			
	}

	private void getbranchWiseDeatils(Date fromdate, Date todate, String branch) {
		// TODO Auto-generated method stub
		
		MyQuerry querry = new MyQuerry();
		
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter=null;

		filter = new Filter();
		filter.setQuerryString("branch");
		filter.setStringValue(branch);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("serviceDate >=");
		filter.setDateValue(fromdate);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("serviceDate <=");
		filter.setDateValue(todate);
		filtervec.add(filter);
		
//		/**
//		 * 
//		 * Rohan Added this status filter for removing cancelled services
//		 */
//		List<String> statusList;
//		statusList=new ArrayList<String>();
//		statusList.add(Service.SERVICESTATUSCOMPLETED);
//		statusList.add(Service.SERVICESTATUSRESCHEDULE);
//		statusList.add(Service.SERVICESTATUSSCHEDULE);
//		statusList.add(Service.SERVICESTATUSCLOSED);
//		statusList.add(Service.SERVICESTATUSPENDING);
//		
//		filter = new Filter();
//		filter.setQuerryString("status IN");
//		filter.setList(statusList);
//		filtervec.add(filter);
//		
//		/**
//		 * ends here 
//		 */
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Service());
		
		form.showWaitSymbol();
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				System.out.println("hi branch wise result value === "+result.size());
				form.hideWaitSymbol();
				form.getTbtotal().setValue(result.size()+"");
				
				ArrayList<Service> servicelist = new ArrayList<Service>();
				for(SuperModel model:result){
					Service service = (Service) model;
					servicelist.add(service);
				}
				getcompletedandScheduleserviceDetails(servicelist);
			 
				form.getTecnicianTable().getDataprovider().setList(servicelist);
			}

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	/* 
	 * Description :- below method accept total servicelist and calculate which are completed and which are scheduled or rescheduled 
	 * 
	 */
	
	private void getcompletedandScheduleserviceDetails(ArrayList<Service> servicelist) {
		
		ArrayList<Service> completedserviceslist = new ArrayList<Service>();
		ArrayList<Service> scheduleserviceslist = new ArrayList<Service>();
		
		/**
		 * rohan added this for calculating cancelled services against Technician
		 */
		ArrayList<Service> cancelledserviceslist = new ArrayList<Service>();
		
		for(int i=0;i<servicelist.size();i++){
			if(servicelist.get(i).getStatus().equals("Completed") || servicelist.get(i).getStatus().equals(Service.SERVICETCOMPLETED)){
				completedserviceslist.add(servicelist.get(i));
			}
			if(servicelist.get(i).getStatus().equals("Scheduled") || servicelist.get(i).getStatus().equals("Rescheduled") ||
					servicelist.get(i).getStatus().equals(Service.SERVICESTATUSSTARTED) || servicelist.get(i).getStatus().equals(Service.SERVICESTATUSREPORTED)){
				scheduleserviceslist.add(servicelist.get(i));
			}
			if(servicelist.get(i).getStatus().equals("Cancelled") || servicelist.get(i).getStatus().equals(Service.SERVICESUSPENDED)){
				cancelledserviceslist.add(servicelist.get(i));
			}
			
		}
		form.getTbCompleted().setValue(completedserviceslist.size()+"");
		form.getTbAssigned().setValue(scheduleserviceslist.size()+"");
		form.getTbCancelled().setValue(cancelledserviceslist.size()+"");
		getReWorkServiceDetails(completedserviceslist);
	}

	/*
	 * Description :- below method accept total completedservicelist and checking which are complaint service raised against specific
	 * service
	 */
	
	private void getReWorkServiceDetails(ArrayList<Service> completedserviceslist) {

		techsereviceAsync.getReWorkdetails(model.getCompanyId(), completedserviceslist, new AsyncCallback<Integer>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.showDialogMessage("Unexpected Error");
				System.out.println(caught);
			}

			@Override
			public void onSuccess(Integer result) {
				// TODO Auto-generated method stub
				form.getTbRework().setValue(result+"");
				
			}
		});
		
	}

	private void getemployeeDetails(Date fromdate, Date todate,	String employeeName) {

		MyQuerry querry = new MyQuerry();
		
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter=null;

		filter = new Filter();
		filter.setQuerryString("employee");
		filter.setStringValue(employeeName);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("serviceDate >=");
		filter.setDateValue(fromdate);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("serviceDate <=");
		filter.setDateValue(todate);
		filtervec.add(filter);
		
		
//		/**
//		 * 
//		 * Rohan Added this status filter for removing cancelled services
//		 * Date : 11/1/2016
//		 * Reason : This required by NBHC for technician dashoard 
//		 */
//		List<String> statusList;
//		statusList=new ArrayList<String>();
//		statusList.add(Service.SERVICESTATUSCOMPLETED);
//		statusList.add(Service.SERVICESTATUSRESCHEDULE);
//		statusList.add(Service.SERVICESTATUSSCHEDULE);
//		statusList.add(Service.SERVICESTATUSCLOSED);
//		statusList.add(Service.SERVICESTATUSPENDING);
//		
//		filter = new Filter();
//		filter.setQuerryString("status IN");
//		filter.setList(statusList);
//		filtervec.add(filter);
//		
//		/**
//		 * ends here 
//		 */
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Service());
		
		form.showWaitSymbol();
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				System.out.println("hi vijay result value === "+result.size());
				
				form.hideWaitSymbol();
				
				form.getTbtotal().setValue(result.size()+"");
				
				ArrayList<Service> servicelist = new ArrayList<Service>();
				for(SuperModel model:result){
					Service service = (Service) model;
					servicelist.add(service);
				}
				getcompletedandScheduleserviceDetails(servicelist);
				form.getTecnicianTable().getDataprovider().setList(servicelist);

			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}




	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnDownload() {
		
		System.out.println(" inside download");
		ArrayList<Service> serviceArray = new ArrayList<Service>();
		List<Service> servicelist =form.tecnicianTable.getDataprovider().getList();
		
		for(int i=0;i<servicelist.size();i++){
			Service service = new Service();
			service.setBranch(servicelist.get(i).getBranch());
			service.setEmployee(servicelist.get(i).getEmployee());
			service.setServiceDate(servicelist.get(i).getServiceDate());
			service.setStatus(servicelist.get(i).getStatus());
			// for download here i am seeting product name into premises setter
			service.setPremises(servicelist.get(i).getProductName());
			service.setContractCount(servicelist.get(i).getContractCount());
			service.setCount(servicelist.get(i).getCount());
			// for download here i am seeting Customer name into setComment setter
			service.setComment(servicelist.get(i).getCustomerName());
			serviceArray.add(service);
		}
		
		ArrayList<String> summarylist = new ArrayList<String>();
		summarylist.add(form.getTbtotal().getValue());
		summarylist.add(form.getTbAssigned().getValue());
		summarylist.add(form.getTbCompleted().getValue());
		summarylist.add(form.getTbRework().getValue());
		
		ArrayList<String> personinfowithDate = new ArrayList<String>();
		personinfowithDate.add(form.employeeInfo.getEmployeeId()+"");
		personinfowithDate.add(form.employeeInfo.getEmployeeName());
		personinfowithDate.add(form.employeeInfo.getCellNumber()+"");
		personinfowithDate.add(form.getOlbbranch().getValue());
		personinfowithDate.add(AppUtility.parseDate(form.dbFromDate.getValue()));
		personinfowithDate.add(AppUtility.parseDate(form.dbToDate.getValue()));
		
		System.out.println("info size ==="+personinfowithDate.size());
		
		csvservice.setTechnicianInfoReport(personinfowithDate,serviceArray, summarylist, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(Void result) {
				// TODO Auto-generated method stub
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+117;
				Window.open(url, "test", "enabled");
			}
		});
		
		
	
	}

	
}
