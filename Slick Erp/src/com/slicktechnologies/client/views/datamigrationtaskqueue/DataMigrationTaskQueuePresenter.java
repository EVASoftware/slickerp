package com.slicktechnologies.client.views.datamigrationtaskqueue;

import java.util.ArrayList;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.slicktechnologies.client.services.DataMigrationTaskQueueService;
import com.slicktechnologies.client.services.DataMigrationTaskQueueServiceAsync;
import com.slicktechnologies.client.services.DocumentUploadService;
import com.slicktechnologies.client.services.DocumentUploadServiceAsync;
import com.slicktechnologies.client.views.datamigration.DataMigrationForm;
import com.slicktechnologies.client.views.datamigration.DataMigrationPresentor;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.PhysicalInventoryMaintaince;

public class DataMigrationTaskQueuePresenter extends FormScreenPresenter<DummyEntityOnlyForScreen>{

	final DataMigrationTaskQueueServiceAsync datamigtaskqueueAsync = GWT.create(DataMigrationTaskQueueService.class);
	
	DocumentUploadServiceAsync datauploadService = GWT.create(DocumentUploadService.class);

	DataMigrationTaskQueueForm form;
	
	public DataMigrationTaskQueuePresenter(FormScreen<DummyEntityOnlyForScreen> view,DummyEntityOnlyForScreen model) {
		super(view, model);
		
		form=(DataMigrationTaskQueueForm) view;

		form.setPresenter(this);
		form.btnMasterGo.addClickHandler(this);
		form.btnTransactionGo.addClickHandler(this);
	}
	
	public static void initialize(){
		AppMemory.getAppMemory().skeleton.isMenuVisble=false;
		AppMemory.getAppMemory().currentState=ScreeenState.NEW;

		DataMigrationTaskQueueForm form = new DataMigrationTaskQueueForm();

		
		DataMigrationTaskQueuePresenter presenter=new DataMigrationTaskQueuePresenter(form, new DummyEntityOnlyForScreen());		
		AppMemory.getAppMemory().stickPnel(form);
		
	}
	
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		super.onClick(event);

		if(event.getSource()==form.btnMasterGo){
			System.out.println("Hi Gm");
			
			Company c = new Company();
			
			String entityName = form.lstboxMasters.getValue(form.lstboxMasters.getSelectedIndex());
			
			form.txtfilecount.setValue(null);
			form.txtsavedrecords.setValue(null);
			
//			datamigtaskqueueAsync.saveRecords(c.getCompanyId(), name, new AsyncCallback<Integer>() {
//
//				@Override
//				public void onFailure(Throwable caught) {
//					// TODO Auto-generated method stub
//					form.showDialogMessage("Failed");
//
//				}
//
//				@Override
//				public void onSuccess(Integer result) {
//					// TODO Auto-generated method stub
//					form.showDialogMessage("Record saving successfully");
//					
//				}
//			});
			
			/**
			 * Date 20-02-2018 By vijay for Uploading data task queue 
			 * above old code rpc commented
			 */
			
			datamigtaskqueueAsync.saveRecords(c.getCompanyId(), entityName, new AsyncCallback<ArrayList<Integer>>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.showDialogMessage("Failed");
				}

				@Override
				public void onSuccess(ArrayList<Integer> result) {
					// TODO Auto-generated method stub
					
					if(result.contains(-1)){
						form.showDialogMessage("Invalid Excel Sheet");
					}else if(result.contains(-2)){
						form.showDialogMessage("Error: Locality Name Already Exist!!");
					}else if(result.contains(-3)){
						form.showDialogMessage("Error: Customer Name Already Exist!!");
					}else if(result.contains(-4)){/*** date 26.04.2019 added by komal to store employee , user , technician warehouse details and product inventory view**/
						form.showDialogMessage("Error: Branch Name Does Not Exist!!");
					}else if(result.contains(-5)){
						form.showDialogMessage("Error: Warehouse Name Does Not Exist!!");
					}else if(result.contains(-6)){
						form.showDialogMessage("Error: Storage Location Name Does Not Exist!!");
					}else if(result.contains(-7)){
						form.showDialogMessage("Error: Storage Bin Name Does Not Exist!!");
					}
					else if(result.contains(-8)){
						form.showDialogMessage("Error: Storage Bin Name Already Exist!!");
					}
					else{
						form.showDialogMessage("Record saving Process started successfully");
					}
				}
			});

			
		}
		
		/**
		 * Date 21-11-2019 By Vijay
		 * Des :- NBHC Lock Seal with GRN and MMN with process config 
		 * Note :- No
		 * 
		 */
		if(event.getSource()==form.btnTransactionGo){
			
			final	Company company=new Company();
			
			String entityNameStockUpdate = form.lstboxTransaction.getValue(form.lstboxTransaction.getSelectedIndex());
			String bloburl="";
			
			if(form.lstboxTransaction.getSelectedIndex()==0){
				form.showDialogMessage("Please select transaction Name!");
				return;
			}
			if(form.lstboxTransaction.getValue(form.lstboxTransaction.getSelectedIndex()).equals("GRN")){
				reactonUploadGRNTransaction();
			}
			else if(form.lstboxTransaction.getValue(form.lstboxTransaction.getSelectedIndex()).equals("MMN")){
				reactonUploadMMN();
			}
			else if(form.lstboxTransaction.getValue(form.lstboxTransaction.getSelectedIndex()).equals("lock Seal Stock Update")){
				reactonlockSealStockUpdate();
			}
			
			else if(entityNameStockUpdate.equalsIgnoreCase("Stock Update")){
				form.showWaitSymbol();
			bloburl=form.transactionUploadComposite.getValue().getUrl();
			System.out.println(" Stock upload blob key URL :- "+bloburl);
			datamigtaskqueueAsync.readStockUpdateExcel(entityNameStockUpdate,company.getCompanyId(),bloburl, new AsyncCallback<ArrayList<String>>() { // Done
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					System.out.println("First Method Execution Fail ");
					form.showDialogMessage("Please Check Excel Format / Field");	
				}
				@Override
				public void onSuccess(ArrayList<String> stockupdateExcel) {
					// TODO Auto-generated method stub
					if (stockupdateExcel.size() == 1) {
						form.showDialogMessage(stockupdateExcel.get(0));
						form.hideWaitSymbol();
					}else{
//						form.showWaitSymbol();
						System.out.println("First Method Execute Sucessful ");	
					datamigtaskqueueAsync.reactOnStockUpdateValidate(stockupdateExcel,company.getCompanyId(),new AsyncCallback<ArrayList<String>>() {
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.showDialogMessage("Please Check Enter Values");	
							form.hideWaitSymbol();
						}
						@Override
						public void onSuccess(ArrayList<String> stockExcel) {
							// TODO Auto-generated method stub
					   if (stockExcel.size() == 1) {
						     System.out.println(" Second Method Excel Value "+stockExcel);
								form.showDialogMessage(stockExcel.get(0));
								form.hideWaitSymbol();
							}else{
								System.out.println("Third Method Execute Sucessful ");
//								form.showWaitSymbol();
								System.out.println(" stockExcel.get(1) value :- "+stockExcel.get(1));
						      datamigtaskqueueAsync.stockUpdateExcelUpload(stockExcel.get(1),company.getCompanyId(),new AsyncCallback<String>() {
							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub                       
								System.out.println("Please Check Enter Values ");
								form.hideWaitSymbol();
							
							}

							@Override
							public void onSuccess(String result) {
								// TODO Auto-generated method stub
								System.out.println(" Execute Sucessful ");
								System.out.println(" Result Value "+result);
								form.showDialogMessage(result);
								form.hideWaitSymbol();
							}
						});
							}
							
						}
					});
					}
				}
			});
		  } //Stock Update End 
			
			/**
			 * @author Vijay Chougule
			 * Des :- NBHC CCPM Fumigation Service update when services completed from MIN
			 * and when fumigation all related data not updated in Fumigation Report Entity
			 */
			else if(form.lstboxTransaction.getValue(form.lstboxTransaction.getSelectedIndex()).equals("Update Fumigation Services")){
				reactonFumigationServiceUpdation();
			}
			
		}
		/**
		 * ends here
		 */
		
	}


	

	
	

	

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();
		
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		
	}

	private void reactonUploadGRNTransaction() {
		form.showWaitSymbol();
		
		datauploadService.readexcelFile(new AsyncCallback<ArrayList<String>>() {
			
			@Override
			public void onSuccess(ArrayList<String> result) {
				// TODO Auto-generated method stub
				if(result.size()!=0){
					if(result.size()==1){
						form.showDialogMessage(result.get(0));
					}
					else{
						Company company = new Company();
						datauploadService.uploadGRN(company.getCompanyId(), result, new AsyncCallback<ArrayList<String>>() {

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								form.hideWaitSymbol();
								form.showDialogMessage("Failed please try again or contact to EVA Support");
							}

							@Override
							public void onSuccess(ArrayList<String> result) {
								// TODO Auto-generated method stub
								form.showDialogMessage(result.get(0));
								form.hideWaitSymbol();
							}
						});
					}
				}
				else{
					form.hideWaitSymbol();
				}
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
				form.showDialogMessage("Unable to read excel file please try again");

			}
		});
	}
	
	
	private void reactonUploadMMN() {
		form.showWaitSymbol();
		datauploadService.readexcelFile(new AsyncCallback<ArrayList<String>>() {
			
			@Override
			public void onSuccess(ArrayList<String> result) {
				if(result.size()!=0){
					if(result.size()==1){
						form.showDialogMessage(result.get(0));
						form.hideWaitSymbol();
					}
					else{
						Company company = new Company();
						datauploadService.uploadMMN(company.getCompanyId(), result, new AsyncCallback<String>() {

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								form.hideWaitSymbol();
								form.showDialogMessage("Failed please try again or contact to EVA Support");
							}

							@Override
							public void onSuccess(String result) {
								// TODO Auto-generated method stub
								form.hideWaitSymbol();
								form.showDialogMessage(result);
								
							}
						});
					}
				}	
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
				form.showDialogMessage("Unable to read excel file please try again");
			}
		});
	}
	
	private void reactonlockSealStockUpdate() {
		Company company = new Company();
		form.showWaitSymbol();
		datauploadService.uploadLockSealStockUpdate(company.getCompanyId(), new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				form.showDialogMessage(result);
				form.hideWaitSymbol();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
			}
		});
	}

	
	private void reactonFumigationServiceUpdation() {

		Company company = new Company();
		form.showWaitSymbol();
		datauploadService.updateInhouseFumigationServices(company.getCompanyId(), new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				form.showDialogMessage(result);
				form.hideWaitSymbol();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
			}
		});
	}
	
}
