package com.slicktechnologies.client.views.datamigrationtaskqueue;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class DataMigrationTaskQueueForm extends FormScreen<DummyEntityOnlyForScreen> implements ClickHandler {

	
	ListBox lstboxMasters,lstboxConfigurations, lstboxTransaction;
	Label lblMasters, lblConfiguration, lblTransaction, lblfilecount, lblsavedrecords;
	UploadComposite mastersUploadComposite,configurationsUploadComposite,transactionUploadComposite;
	Button btnMasterGo, btnConfigurationGo, btnTransactionGo;
	TextBox txtfilecount, txtsavedrecords;
	
	
	public DataMigrationTaskQueueForm() {
		super();
       createGui();
//       form.setHeight("300px");

	}

	public DataMigrationTaskQueueForm(String[] processlevel, FormField[][] fields,
			FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
	}
	
	
private void initalizeWidget(){
		
		lstboxMasters = new ListBox();
		lstboxMasters.addItem("--SELECT--");
		lstboxMasters.addItem("Customer");
		lstboxMasters.addItem("Locality");
		lstboxMasters.addItem("TechnicianWarehouseDetails");


		lstboxConfigurations = new ListBox();
		lstboxConfigurations.addItem("--SELECT--");
		lstboxTransaction = new ListBox();
		lstboxTransaction.addItem("--SELECT--");
		
		/**
		 * Date 21-11-2019 By Vijay
		 * Des :- NBHC Lock Seal with GRN and MMN with process config
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("DataMigration", "EnableUploadGRNMMNForLockSeal")){
			lstboxTransaction.addItem("GRN");
			lstboxTransaction.addItem("MMN");
			lstboxTransaction.addItem("lock Seal Stock Update");
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("DataMigration", "EnableFumigationServiceUpdation")){
			lstboxTransaction.addItem("Update Fumigation Services");
		}
		
		lblMasters = new Label();
		lblMasters.setText("MASTERS");
		lblConfiguration = new Label();
		lblConfiguration.setText("CONFIGURATIONS");
		lblTransaction = new Label();
		lblTransaction.setText("TRANSACTIONS");
		
		
		
		
		mastersUploadComposite = new UploadComposite();
		configurationsUploadComposite = new UploadComposite();
		transactionUploadComposite = new UploadComposite();
		
		btnMasterGo = new Button("Go");
		btnConfigurationGo = new Button("Go");
		btnTransactionGo = new Button("Go");
		
		txtfilecount = new TextBox();
		txtfilecount.setEnabled(false);
		txtsavedrecords = new TextBox();
		txtsavedrecords.setEnabled(false);
		
		/******* Deepak Salve - add Stock update Upload task 13-11-2019 *********/
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryView", "UpdateStockbaseondemandquantity")){
			lstboxTransaction.addItem("Stock Update");	
		}
	}

	@Override
	public void createScreen() {

		initalizeWidget();
		
		
		this.processlevelBarNames=new String[]{"New"};
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDataMigration=fbuilder.setlabel("Data Migration").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",lblMasters);
		FormField flblmasters = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",lstboxMasters);
		FormField flstboxMasters= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",mastersUploadComposite);
		FormField fmastersUploadComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",btnMasterGo);
		FormField fbtnMasterGo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",lblConfiguration);
		FormField flblConfiguration = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",lstboxConfigurations);
		FormField flstboxConfigurations= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",configurationsUploadComposite);
		FormField fconfigurationsUploadComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",btnConfigurationGo);
		FormField fbtnConfigurationGo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",lblTransaction);
		FormField flblTransaction = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",lstboxTransaction);
		FormField flstboxTransaction= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",transactionUploadComposite);
		FormField ftransactionUploadComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",btnTransactionGo);
		FormField fbtnTransactionGo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		
		FormField[][] formfield = { {fgroupingDataMigration,},
				  {flblmasters,flstboxMasters,fmastersUploadComposite,fbtnMasterGo},
				  {flblConfiguration,flstboxConfigurations,fconfigurationsUploadComposite,fbtnConfigurationGo},
				  {flblTransaction,flstboxTransaction,ftransactionUploadComposite,fbtnTransactionGo},
				  
				  };

				this.fields=formfield;		
		
	}

	

	@Override
	public void toggleAppHeaderBarMenu() {
		// TODO Auto-generated method stub

		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		
		AuthorizationHelper.setAsPerAuthorization(Screen.DATAMIGRATIONTASKQUEUE,LoginPresenter.currentModule.trim());
	
		
	
	}

		

	@Override
	public void updateModel(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateView(DummyEntityOnlyForScreen model) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

}
