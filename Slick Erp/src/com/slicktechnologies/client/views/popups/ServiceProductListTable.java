package com.slicktechnologies.client.views.popups;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;

public class ServiceProductListTable extends SuperTable<ProductGroupList>{

	
	TextColumn<ProductGroupList> viewProdIdColumn;
	TextColumn<ProductGroupList> viewProdCodeColumn;
	TextColumn<ProductGroupList> viewProdNameColumn;
	TextColumn<ProductGroupList> viewProdQtyColumn;
	TextColumn<ProductGroupList> viewProdUnitColumn;
	TextColumn<ProductGroupList> viewProdPriceColumn;
	
	public ServiceProductListTable()
	{
		super();
	}
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		addViewProductIdColumn();
		addViewProductCodeColumn();
		addViewProductNameColumn();
		addViewProductQtyColumn();
		addViewProductUnitColumn();
		addViewProductPriceColumn();
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		addViewProductIdColumn();
		addViewProductCodeColumn();
		addViewProductNameColumn();
		addViewProductQtyColumn();
		addViewProductUnitColumn();
		addViewProductPriceColumn();
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

	void addViewProductIdColumn(){

		viewProdIdColumn=new TextColumn<ProductGroupList>() {
			@Override
			public String getValue(ProductGroupList object) {
				return object.getProduct_id()+"";
			}
		};
		table.addColumn(viewProdIdColumn,"Product Id");
		table.setColumnWidth(viewProdIdColumn, 10, Unit.PCT);
	}

	
	void addViewProductCodeColumn(){

		viewProdCodeColumn=new TextColumn<ProductGroupList>() {
			@Override
			public String getValue(ProductGroupList object) {
				return object.getCode()+"";
			}
		};
		table.addColumn(viewProdCodeColumn,"Product Code");
		table.setColumnWidth(viewProdCodeColumn, 10, Unit.PCT);
	}
	
	void addViewProductNameColumn(){

		viewProdNameColumn=new TextColumn<ProductGroupList>() {
			@Override
			public String getValue(ProductGroupList object) {
				return object.getName()+"";
			}
		};
		table.addColumn(viewProdNameColumn,"Product Name");
		table.setColumnWidth(viewProdNameColumn, 10, Unit.PCT);
	}

	void addViewProductQtyColumn(){

		viewProdQtyColumn=new TextColumn<ProductGroupList>() {
			@Override
			public String getValue(ProductGroupList object) {
				return object.getQuantity()+"";
			}
		};
		table.addColumn(viewProdQtyColumn,"Quantity");
		table.setColumnWidth(viewProdQtyColumn, 10, Unit.PCT);
	}
	

	
	void addViewProductUnitColumn(){

		viewProdUnitColumn=new TextColumn<ProductGroupList>() {
			@Override
			public String getValue(ProductGroupList object) {
				return object.getUnit()+"";
			}
		};
		table.addColumn(viewProdUnitColumn,"Unit Of Measurement");
		table.setColumnWidth(viewProdUnitColumn, 10, Unit.PCT);
	}
	
	void addViewProductPriceColumn(){

		viewProdPriceColumn=new TextColumn<ProductGroupList>() {
			@Override
			public String getValue(ProductGroupList object) {
				return object.getPrice()+"";
			}
		};
		table.addColumn(viewProdPriceColumn,"Price");
		table.setColumnWidth(viewProdPriceColumn, 10, Unit.PCT);
	}
	
}
