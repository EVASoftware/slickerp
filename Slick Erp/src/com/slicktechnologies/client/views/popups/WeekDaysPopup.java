package com.slicktechnologies.client.views.popups;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.FontStyle;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.DataMigrationService;
import com.slicktechnologies.client.services.DataMigrationServiceAsync;
import com.slicktechnologies.client.services.DataMigrationTaskQueueService;
import com.slicktechnologies.client.services.DataMigrationTaskQueueServiceAsync;
import com.slicktechnologies.client.services.DocumentUploadService;
import com.slicktechnologies.client.services.DocumentUploadServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.datamigration.DataMigrationForm;
import com.slicktechnologies.client.views.datamigration.uploadOptionPopUp;
import com.slicktechnologies.shared.ContractUploadDetails;
import com.slicktechnologies.shared.EmployeeAsset;
import com.slicktechnologies.shared.MINUploadExcelDetails;
import com.slicktechnologies.shared.common.attendance.EmployeeLeaveBalance;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorPriceListDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.servicerelated.AssetMovementInfo;

public class WeekDaysPopup extends AbsolutePanel implements ClickHandler {

	Button btnOk,btnCancel;
	
	InlineLabel headingLabel;
	
//	DocumentUploadServiceAsync documentuploadAsync = GWT.create(DocumentUploadService.class);
//	ConditionDialogBox conditionPopup = new ConditionDialogBox(
//			"Do you want to cancel inactive Customer Branch's open services? It will not cancel completed services. ", AppConstants.YES,
//			AppConstants.NO);
//	PopupPanel panel;
	TextArea info;
	public CheckBox cbSun,cbMon,cbTues,cbWed,cbThurs,cbFri,cbSat;
	
	
	private void initializeWidget(){
		
		btnOk=new Button("Apply");
		btnOk.getElement().getStyle().setWidth(80, Unit.PX);
		btnCancel=new Button("Cancel");
		btnCancel.getElement().getStyle().setWidth(80, Unit.PX);
		info=new TextArea();
		info.setHeight("300px");
		info.setWidth("550px");
		
		cbSun = new CheckBox("Sunday");
		cbSun.setValue(false);
		cbSun.setText("Sunday");
		
		cbMon = new CheckBox();
		cbMon.setValue(false);
		cbMon.setText("Monday");
		
		cbTues = new CheckBox();
		cbTues.setValue(false);
		cbTues.setText("Tuesday");
		
		cbWed = new CheckBox();
		cbWed.setValue(false);
		cbWed.setText("Wedbesday");
		
		cbThurs = new CheckBox();
		cbThurs.setValue(false);
		cbThurs.setText("Thursday");
		
		cbFri = new CheckBox();
		cbFri.setValue(false);
		cbFri.setText("Friday");
		
		cbSat = new CheckBox();
		cbSat.setValue(false);
		cbSat.setText("Saturday");
	}
	
	public WeekDaysPopup(String title) {
		initializeWidget();
		
		headingLabel=new InlineLabel();
		headingLabel.getElement().getStyle().setFontSize(15, Unit.PX);
		headingLabel.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		headingLabel.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		if(title==null)
			title="";
		InlineLabel lbtitle=new InlineLabel(title);
		lbtitle.getElement().getStyle().setFontSize(16, Unit.PX);
		lbtitle.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		lbtitle.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		add(headingLabel,175,20);
		
		add(lbtitle,175,30);
		
		add(cbSun,10,85);
		add(cbMon,110,85);
		add(cbTues,210,85);
		add(cbWed,310,85);
		add(cbThurs,410,85);
		add(cbFri,510,85);
		add(cbSat,610,85);
				
		add(btnOk,200,120);
		add(btnCancel,320,120);
		
		
		setSize("700px","200px");
		this.getElement().setId("login-form");
	}
	
	
	public void clear(){
		headingLabel.setText("");
		cbSun.setValue(false);
		cbMon.setValue(false);
		cbTues.setValue(false);
		cbWed.setValue(false);
		cbThurs.setValue(false);
		cbFri.setValue(false);
		cbSat.setValue(false);
	}
	
	public boolean validate(){
		GWTCAlert alert=new GWTCAlert();
		return true;
	}
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		Console.log("on click");
		
	}
	
	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}



	public InlineLabel getHeadingLabel() {
		return headingLabel;
	}



	public void setHeadingLabel(InlineLabel headingLabel) {
		this.headingLabel = headingLabel;
	}


	public void showDialogMessage(String msg){
		GWTCAlert alert=new GWTCAlert();
		alert.alert(msg);
	}
	protected GWTCGlassPanel glassPanel;
	public void showWaitSymbol() {
		glassPanel = new GWTCGlassPanel();
		glassPanel.show();
	}
	public void hideWaitSymbol() {
	   glassPanel.hide();
		
	}
	
	public void setInfo(TextArea info) {
		this.info = info;
	}

	public CheckBox getCbSun() {
		return cbSun;
	}

	public void setCbSun(CheckBox cbSun) {
		this.cbSun = cbSun;
	}

	public CheckBox getCbMon() {
		return cbMon;
	}

	public void setCbMon(CheckBox cbMon) {
		this.cbMon = cbMon;
	}

	public CheckBox getCbTues() {
		return cbTues;
	}

	public void setCbTues(CheckBox cbTues) {
		this.cbTues = cbTues;
	}

	public CheckBox getCbWed() {
		return cbWed;
	}

	public void setCbWed(CheckBox cbWed) {
		this.cbWed = cbWed;
	}

	public CheckBox getCbThurs() {
		return cbThurs;
	}

	public void setCbThurs(CheckBox cbThurs) {
		this.cbThurs = cbThurs;
	}

	public CheckBox getCbFri() {
		return cbFri;
	}

	public void setCbFri(CheckBox cbFri) {
		this.cbFri = cbFri;
	}

	public CheckBox getCbSat() {
		return cbSat;
	}

	public void setCbSat(CheckBox cbSat) {
		this.cbSat = cbSat;
	}

	
}

