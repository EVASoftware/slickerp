package com.slicktechnologies.client.views.popups;

import com.google.gwt.user.client.ui.Button;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;

public class DownloadPopPup extends PopupScreen{

	
	Button btnDownloadFormat1, btnDownloadFormat2,btnDownloadFormat3;
	
	
	public DownloadPopPup(){
		super();
		createGui();
	}
	
	@Override
	public void createScreen() {
		
		btnDownloadFormat1 = new Button("Download Format1");
		btnDownloadFormat2 = new Button("Download Format2");
		btnDownloadFormat3 = new Button("Download Format3");
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField downalodFormatInfo = fbuilder.setlabel("Download Formats").setColSpan(4).widgetType(FieldType.Grouping).build();
		
		
		fbuilder = new FormFieldBuilder(" ", btnDownloadFormat1);
		FormField fbtnDownloadFormat1 = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ", btnDownloadFormat2);
		FormField fbtnDownloadFormat2 = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ", btnDownloadFormat3);
		FormField fbtnDownloadFormat3 = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		FormField[][] formfield = {
				{ downalodFormatInfo   },
				{fbtnDownloadFormat1 ,fbtnDownloadFormat2,fbtnDownloadFormat3},
				};
		this.fields = formfield;
		
	}

	public Button getBtnDownloadFormat1() {
		return btnDownloadFormat1;
	}

	public void setBtnDownloadFormat1(Button btnDownloadFormat1) {
		this.btnDownloadFormat1 = btnDownloadFormat1;
	}

	public Button getBtnDownloadFormat2() {
		return btnDownloadFormat2;
	}

	public void setBtnDownloadFormat2(Button btnDownloadFormat2) {
		this.btnDownloadFormat2 = btnDownloadFormat2;
	}

	public Button getBtnDownloadFormat3() {
		return btnDownloadFormat3;
	}

	public void setBtnDownloadFormat3(Button btnDownloadFormat3) {
		this.btnDownloadFormat3 = btnDownloadFormat3;
	}

	
	
}
