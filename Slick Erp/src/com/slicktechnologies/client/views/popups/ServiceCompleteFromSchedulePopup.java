package com.slicktechnologies.client.views.popups;

import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;

public class ServiceCompleteFromSchedulePopup extends PopupScreen {
	
	CheckBox cbServiceDateAsServiceCompletionDate;
	DateBox dbServiceCompletionDate;
	TextArea taCustomerFeedback;
	TextArea taTechRemark;
	ListBox lbRating;
	
	public ServiceCompleteFromSchedulePopup(){
		super();
		createGui();
	}

	private void initializeWidgets() {
		cbServiceDateAsServiceCompletionDate = new CheckBox();
		cbServiceDateAsServiceCompletionDate.setValue(false);
		dbServiceCompletionDate = new DateBoxWithYearSelector();
		taCustomerFeedback = new TextArea();
		taTechRemark = new TextArea();
		lbRating = new ListBox();
		lbRating.addItem("--SELECT--");
		lbRating.addItem("Excellent");
		lbRating.addItem("Very Good");
		lbRating.addItem("Good");
		lbRating.addItem("Satisfactory");
		lbRating.addItem("Poor");		
	}
	
	@Override
	public void createScreen() {
		
		initializeWidgets();
		

		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField faddressInfo = fbuilder.setlabel("Service Mark Complete").widgetType(FieldType.Grouping).setColSpan(2).build();
		
		
		fbuilder = new FormFieldBuilder("Service Date As Service Completion Date",  cbServiceDateAsServiceCompletionDate);
		FormField fcbServiceDateAsServiceCompletionDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Service Completion Date",  dbServiceCompletionDate);
		FormField fdbServiceCompletionDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Customer Feedback",  taCustomerFeedback);
		FormField ftaCustomerFeedback = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Customer Rating",  lbRating);
		FormField flbRating = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Technician Remark",  taTechRemark);
		FormField ftaTechRemark = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
	
		FormField [][]formfield = {
				{faddressInfo},
				{fcbServiceDateAsServiceCompletionDate},
				{fdbServiceCompletionDate},
				{ftaCustomerFeedback},
				{flbRating},
				{ftaTechRemark},
		};
		this.fields=formfield;
	}

	

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	public CheckBox getCbServiceDateAsServiceCompletionDate() {
		return cbServiceDateAsServiceCompletionDate;
	}

	public void setCbServiceDateAsServiceCompletionDate(
			CheckBox cbServiceDateAsServiceCompletionDate) {
		this.cbServiceDateAsServiceCompletionDate = cbServiceDateAsServiceCompletionDate;
	}

	public DateBox getDbServiceCompletionDate() {
		return dbServiceCompletionDate;
	}

	public void setDbServiceCompletionDate(DateBox dbServiceCompletionDate) {
		this.dbServiceCompletionDate = dbServiceCompletionDate;
	}

	public TextArea getTaCustomerFeedback() {
		return taCustomerFeedback;
	}

	public void setTaCustomerFeedback(TextArea taCustomerFeedback) {
		this.taCustomerFeedback = taCustomerFeedback;
	}

	public TextArea getTaTechRemark() {
		return taTechRemark;
	}

	public void setTaTechRemark(TextArea taTechRemark) {
		this.taTechRemark = taTechRemark;
	}

	public ListBox getLbRating() {
		return lbRating;
	}

	public void setLbRating(ListBox lbRating) {
		this.lbRating = lbRating;
	}

}

