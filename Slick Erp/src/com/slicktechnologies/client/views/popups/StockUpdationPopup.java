package com.slicktechnologies.client.views.popups;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DoubleBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.shared.Quotation;

public class StockUpdationPopup extends PopupScreen{


	DoubleBox dbAvailableStockQty;
	GenricServiceAsync genAsync = GWT.create(GenricService.class);
	

	public StockUpdationPopup(){
		super();
		createGui();
	}

	@Override
	public void createScreen() {
		dbAvailableStockQty = new DoubleBox();
		
		FormFieldBuilder fbuilder;

		fbuilder = new FormFieldBuilder();
		FormField fsearchFilters = fbuilder.setlabel("Stock Information").widgetType(FieldType.Grouping).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Available Qty",dbAvailableStockQty);
		FormField olbQuotationId = fbuilder.setMandatory(true).setMandatoryMsg("Available Qty is mandatory!").setColSpan(0)
									.setRowSpan(0).build();
		FormField [][]formfield = {
				{fsearchFilters},
				{olbQuotationId},
		};
		this.fields=formfield;
	}
	

	@Override
	public void onClick(ClickEvent event) {

		if(event.getSource()==this.getLblCancel()){
			hidePopUp();
		}
	}

	public DoubleBox getDbAvailableStockQty() {
		return dbAvailableStockQty;
	}

	public void setDbAvailableStockQty(DoubleBox dbAvailableStockQty) {
		this.dbAvailableStockQty = dbAvailableStockQty;
	}

	
	


}
