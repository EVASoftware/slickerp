package com.slicktechnologies.client.views.popups;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.Label;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;

public class ProductSerailNumberPopup extends PopupScreen{

	ProductSerialNumberMappingTable proSerNoTable;
	boolean editable = false,selectable = false;
	public ProductSerailNumberPopup() {
		super();
		createGui();
		// TODO Auto-generated constructor stub
	}
	
	public ProductSerailNumberPopup(boolean editable1,boolean selectable1) {
		super();
		getPopup().getElement().addClassName("popupProSerialNumber");
		this.editable = editable1;
		this.selectable = selectable1;
		proSerNoTable.setEnable(editable);
		createGui();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	private void initializeWidgets() {
		proSerNoTable = new ProductSerialNumberMappingTable(editable,selectable);
		proSerNoTable.setEnable(editable);
	}
	@Override
	public void createScreen() {
		initializeWidgets();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fserialInfo = fbuilder.setlabel("Product Serial Number Details").widgetType(FieldType.Grouping).build();
		
		fbuilder = new FormFieldBuilder("",  proSerNoTable.getTable());
		FormField fproSerNoTable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField [][]formfield = {
				{fserialInfo},
				{fproSerNoTable},
		};
		this.fields=formfield;
	}

	public ProductSerialNumberMappingTable getProSerNoTable() {
		return proSerNoTable;
	}

	public void setProSerNoTable(ProductSerialNumberMappingTable proSerNoTable) {
		this.proSerNoTable = proSerNoTable;
	}
	
	public void setProSerialenble(boolean editable1,boolean selectable1){
		this.editable = editable1;
		this.selectable = selectable1;
		proSerNoTable.serNoCnt=0;
		proSerNoTable.selectable = selectable1;
		proSerNoTable.setEnable(editable);
	}
}
