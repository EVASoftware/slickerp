package com.slicktechnologies.client.views.popups;

import java.util.ArrayList;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class InvoiceDatePopup extends PopupScreen{

	DateComparator dateComparator;
	CommonServiceAsync commonService = GWT.create(CommonService.class);
	boolean paymentflag= false;
	public InvoiceDatePopup(){
		super();
		createGui();
	}
	
	public InvoiceDatePopup(boolean flag){
		super();
		createGui();
		paymentflag = flag;
	}
	
	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		dateComparator = new DateComparator("invoiceDate",new Invoice());

		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder("From Date",dateComparator.getFromDate());
		FormField fdbFromDate = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("To Date",dateComparator.getToDate());
		FormField fdbToDate = fbuilder.setRowSpan(0).setColSpan(0).build();
		
		FormField[][] formfield = {
				{fdbFromDate,fdbToDate},
		};
		
		this.fields=formfield;
	}

	@Override
	public void onClick(ClickEvent event) {
		
		Company comp = new Company();
		
		if(event.getSource().equals(this.getLblOk())){
			showWaitSymbol();
			commonService.deleteAndCreateAccountingInterfaceData(dateComparator.getfromDateValue(), dateComparator.gettoDateValue(), comp.getCompanyId(),paymentflag, new AsyncCallback<String>() {
				
				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					hideWaitSymbol();
					showDialogMessage(result);
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					hideWaitSymbol();
				}
			});
		}
		
		if(event.getSource().equals(this.getLblCancel())){
			hidePopUp();
		}
	}

	
	
}
