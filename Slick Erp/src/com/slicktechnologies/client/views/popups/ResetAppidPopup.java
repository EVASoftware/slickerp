package com.slicktechnologies.client.views.popups;


import java.awt.TextField;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.FontStyle;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.DataMigrationService;
import com.slicktechnologies.client.services.DataMigrationServiceAsync;
import com.slicktechnologies.client.services.DataMigrationTaskQueueService;
import com.slicktechnologies.client.services.DataMigrationTaskQueueServiceAsync;
import com.slicktechnologies.client.services.DocumentUploadService;
import com.slicktechnologies.client.services.DocumentUploadServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.datamigration.DataMigrationForm;
import com.slicktechnologies.client.views.datamigration.uploadOptionPopUp;
import com.slicktechnologies.shared.ContractUploadDetails;
import com.slicktechnologies.shared.EmployeeAsset;
import com.slicktechnologies.shared.MINUploadExcelDetails;
import com.slicktechnologies.shared.common.attendance.EmployeeLeaveBalance;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorPriceListDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.servicerelated.AssetMovementInfo;

public class ResetAppidPopup extends AbsolutePanel implements ClickHandler {

	Button btnOk,btnCancel;
	
	InlineLabel headingLabel;
	TextBox txtAppId;	
	TextBox txtCompanyId;
	TextBox txtCompanyName;
	
	String uploadDoc;
	String url;
	
	DataMigrationServiceAsync datamigAsync = GWT.create(DataMigrationService.class);
	DataMigrationTaskQueueServiceAsync dataTaskAsync = GWT.create(DataMigrationTaskQueueService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	DocumentUploadServiceAsync documentuploadAsync = GWT.create(DocumentUploadService.class);
	ConditionDialogBox conditionPopup = new ConditionDialogBox(
			"Do you want to cancel inactive Customer Branch's open services? It will not cancel completed services. ", AppConstants.YES,
			AppConstants.NO);
	PopupPanel panel;
	TextArea info;
	
	private void initializeWidget(){
		
		btnOk=new Button("Ok");
		btnOk.getElement().getStyle().setWidth(80, Unit.PX);
		btnCancel=new Button("Cancel");
		btnCancel.getElement().getStyle().setWidth(80, Unit.PX);
		info=new TextArea();
		info.setHeight("90px");
		info.setWidth("400px");
		
		txtAppId=new TextBox();
		txtCompanyId=new TextBox();
		txtCompanyName=new TextBox();
		txtAppId.getElement().getStyle().setWidth(120, Unit.PX);
		txtCompanyId.getElement().getStyle().setWidth(120, Unit.PX);
		txtCompanyName.getElement().getStyle().setWidth(120, Unit.PX);
	
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
	}
	
	public ResetAppidPopup() {
		initializeWidget();
		headingLabel=new InlineLabel("Reset Appid");
		headingLabel.getElement().getStyle().setFontSize(15, Unit.PX);
		headingLabel.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		headingLabel.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		InlineLabel step1=new InlineLabel("Step 1 : Enter AppId");
		step1.getElement().getStyle().setFontSize(12, Unit.PX);
		step1.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		step1.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		InlineLabel step2=new InlineLabel("Step 2 : Enter Company ID");
		step2.getElement().getStyle().setFontSize(12, Unit.PX);
		step2.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		step2.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		InlineLabel step3=new InlineLabel("Step 3 : Enter Company Name");
		step3.getElement().getStyle().setFontSize(12, Unit.PX);
		step3.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		step3.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		add(headingLabel,10,10);
		
		add(info,10,30);
		
		add(step1,10,140);
		add(txtAppId,10,160);
		
		add(step2,10,200);
		add(txtCompanyId,10,220);
		
		add(step3,10,260);
		add(txtCompanyName,10,280);
		
		add(btnOk,80,310);
		add(btnCancel,200,310);
		
		
		setSize("450px","400px");
		this.getElement().setId("login-form");
	}
	
	public ResetAppidPopup(String processName) {
		if(processName.equals("UpdateCustomerIdInAllDocs")) {
		initializeWidget();
		headingLabel=new InlineLabel("Update New Customer ID in all documents");
		headingLabel.getElement().getStyle().setFontSize(15, Unit.PX);
		headingLabel.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		headingLabel.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		InlineLabel step1=new InlineLabel("Step 1 : Enter Customer Id which you want to update in all documents of current customer.");
		step1.getElement().getStyle().setFontSize(12, Unit.PX);
		step1.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		step1.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		add(headingLabel,10,10);
		
//		add(info,10,30);
		
		add(step1,10,60);
		add(txtAppId,10,130);
		
		
		add(btnOk,80,220);
		add(btnCancel,200,220);
		
		
		setSize("450px","300px");
		this.getElement().setId("login-form");
		}
	}
	
	public void clear(){
		headingLabel.setText("");
		url="";
		uploadDoc="";
		
	}
	
	public boolean validate(){
		GWTCAlert alert=new GWTCAlert();
		
		return true;
	}
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
		
		
		if(event.getSource()==conditionPopup.getBtnOne()){
			Console.log("in conditionPopup.getBtnOne() click");
			final DocumentUploadServiceAsync datauploadService = GWT.create(DocumentUploadService.class);

			final Company company = new Company();

			showWaitSymbol();
			datauploadService.validateCustomerBranchStatusUpdation(company.getCompanyId(),true, new AsyncCallback<Integer>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					hideWaitSymbol();
				}

				@Override
				public void onSuccess(Integer result) {
					Console.log("in validateCustomerBranchStatusUpdation success");
					if(result==null||result==-1){
						hideWaitSymbol();
						showDialogMessage("Invalid file.");
						return;
					}
					if(result==-2){
						hideWaitSymbol();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 218;
						Window.open(url, "test", "enabled");
						showDialogMessage("Please find error file.");
						return;
					}
					datauploadService.updateCustomerBranchStatus(company.getCompanyId(),true,new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							hideWaitSymbol();
						}

						@Override
						public void onSuccess(String result) {
							Console.log("in updateCustomerBranchStatus success");
							// TODO Auto-generated method stub
							hideWaitSymbol();
							showDialogMessage(result);
						}
					});
					
					
				}
			});
		
			
		
			
			
			
			
			panel.hide();
		}
		if(event.getSource()==conditionPopup.getBtnTwo()){

			Console.log("in conditionPopup.getBtnTwo() click");
			final DocumentUploadServiceAsync datauploadService = GWT.create(DocumentUploadService.class);

			final Company company = new Company();

			showWaitSymbol();
			datauploadService.validateCustomerBranchStatusUpdation(company.getCompanyId(),false, new AsyncCallback<Integer>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					hideWaitSymbol();
				}

				@Override
				public void onSuccess(Integer result) {
					Console.log("in validateCustomerBranchStatusUpdation success");
					if(result==null||result==-1){
						hideWaitSymbol();
						showDialogMessage("Invalid file.");
						return;
					}
					
					datauploadService.updateCustomerBranchStatus(company.getCompanyId(),false,new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							hideWaitSymbol();
						}

						@Override
						public void onSuccess(String result) {
							Console.log("in updateCustomerBranchStatus success");
							// TODO Auto-generated method stub
							hideWaitSymbol();
							showDialogMessage(result);
						}
					});
					
					
				}
			});
		
		
			panel.hide();
		}
	}
	


	



	
	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}



	public InlineLabel getHeadingLabel() {
		return headingLabel;
	}



	public void setHeadingLabel(InlineLabel headingLabel) {
		this.headingLabel = headingLabel;
	}



	public String getUploadDoc() {
		return uploadDoc;
	}



	public void setUploadDoc(String uploadDoc) {
		this.uploadDoc = uploadDoc;
	}



	public String getUrl() {
		return url;
	}



	public void setUrl(String url) {
		this.url = url;
	}


	public void showDialogMessage(String msg){
		GWTCAlert alert=new GWTCAlert();
		alert.alert(msg);
	}
	protected GWTCGlassPanel glassPanel;
	public void showWaitSymbol() {
		glassPanel = new GWTCGlassPanel();
		glassPanel.show();
		
		
	}
	public void hideWaitSymbol() {
	   glassPanel.hide();
		
	}
	
	public void uploadTransactionDocuments(final String name, final PopupPanel popPanel){
		Company c=new Company();

		if (name.equals("Employee Asset Allocation")) {
			showWaitSymbol();
			datamigAsync = GWT.create(DataMigrationService.class);
			datamigAsync.saveEmployeeAsset(UserConfiguration.getCompanyId(),new AsyncCallback<ArrayList<EmployeeAsset>>() {
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					hideWaitSymbol();
				}
	
				@Override
				public void onSuccess(ArrayList<EmployeeAsset> result) {
					// TODO Auto-generated method stub
					if (result == null) {
						hideWaitSymbol();
						showDialogMessage("Invalid file.");
						return;
					}
	
					datamigAsync.validateEmployeeAsset(result,new AsyncCallback<ArrayList<EmployeeAsset>>() {
	
					@Override
					public void onFailure(Throwable caught) {
						hideWaitSymbol();
					}
	
					@Override
					public void onSuccess(ArrayList<EmployeeAsset> result) {
	
						for (EmployeeAsset obj : result) {
							if (obj.getCount() == -1) {
								hideWaitSymbol();
								String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
								final String url = gwt+ "csvservlet"+ "?type="+ 173;
								Window.open(url,"test","enabled");
								showDialogMessage("Please find error file.");
								return;
							}
						}
						
						datamigAsync.uploadEmployeeAsset(result,new AsyncCallback<ArrayList<EmployeeAsset>>() {
	
							@Override
							public void onFailure(Throwable caught) {
								hideWaitSymbol();
							}
	
							@Override
							public void onSuccess(ArrayList<EmployeeAsset> result) {
								hideWaitSymbol();
								showDialogMessage("Employee asset upload process started.");
								String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
								final String url = gwt+ "csvservlet"+ "?type="+ 173;
								Window.open(url,"test","enabled");
								popPanel.hide();
							}
						});
					}
				});
				}
			});

		}
		 else {
			datamigAsync.savedTransactionDetails(c.getCompanyId(),name, new AsyncCallback<ArrayList<Integer>>() {
				@Override
				public void onFailure(Throwable caught) {
					showDialogMessage("Failed");
				}

				@Override
				public void onSuccess(ArrayList<Integer> result) {
					if (result.contains(-1)) {
						showDialogMessage("Invalid Excel File");
					}else if (result.contains(-2)) {
						showDialogMessage("Customer does not exist Please create first");
					} else if (result.contains(-3)) {
						showDialogMessage("Product does not exist Please create first");
					}else {
						showDialogMessage("Successfull");
					}
				}
			});
		}
	
	}
	
	public void uploadConfigurationDocuments(final String name, final PopupPanel popPanel){
		Company c=new Company();
		datamigAsync.savedConfigDetails(c.getCompanyId(), name,new AsyncCallback<ArrayList<Integer>>() {
			@Override
			public void onFailure(Throwable caught) {
				showDialogMessage("Failed");
			}

			@Override
			public void onSuccess(ArrayList<Integer> result) {
				if(name.equalsIgnoreCase("Category")){
					if(result.contains(-1)){
						Console.log("inside Category on success error occurs");
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 188;
						Window.open(url, "test", "enabled");
						return;
					}
					else{
						Console.log("Category upload successfully");
						showDialogMessage("Category upload successfully.");
						popPanel.hide();
					}
				}
				
				if(name.equalsIgnoreCase("Type")){
					if(result.contains(-1)){
						Console.log("inside Category on success error occurs");
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 189;
						Window.open(url, "test", "enabled");
						return;
					}
					else{
						Console.log("Type upload successfully");
						showDialogMessage("Type upload successfully.");
						popPanel.hide();
					}
				}
				
				
				
				if (result.contains(-1)) {
					showDialogMessage("Invalid Excel File");
				} else {
					System.out.println("Inside Else of onSuccess");
					if(name.equalsIgnoreCase("Category")||name.equalsIgnoreCase("Type")){
					}else{
						showDialogMessage("Successfull");
						popPanel.hide();
					}
					Company c=new Company();
					String categoryType=AppConstants.GLOBALRETRIEVALCATEGORY+"-"+c.getCompanyId();
					LoginPresenter.globalMakeLiveCategory(categoryType);
					String configType=AppConstants.GLOBALRETRIEVALCONFIG+"-"+c.getCompanyId();
					LoginPresenter.globalMakeLiveConfig(configType);
					String typeType=AppConstants.GLOBALRETRIEVALTYPE+"-"+c.getCompanyId();
					LoginPresenter.globalMakeLiveType(typeType);
				}
			}
		});
	}
	
	public void uploadMasterDocuments(final String name, final PopupPanel popPanel){
		
		if(name.equalsIgnoreCase("Update Leave Balance")){
			updateLeaveBalance(popPanel);
			return;
		}else if(name.equalsIgnoreCase("Customer Branch")){
			saveCustomerBranch(popPanel);
			return;
		}else if(name.equalsIgnoreCase("Vendor Product Price")){
			reactonVendorProductPrice(popPanel);
			return;
		}else if(name.equalsIgnoreCase("CTC Template")){
			reactonCTCTemplate(popPanel);
			return;
		}
		
		Company c=new Company();
		datamigAsync.savedRecordsDeatils(c.getCompanyId(), name, true,LoginPresenter.loggedInUser,new AsyncCallback<ArrayList<Integer>>() {
			@Override
			public void onFailure(Throwable caught) {
				showDialogMessage("Failed");

			}

			@Override
			public void onSuccess(ArrayList<Integer> result) {

				if (name.equalsIgnoreCase("Update Employee")) {
					if (result.contains(-1)) {
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet"+ "?type=" + 186;
						Window.open(url, "test", "enabled");
						return;
					} else {
						Console.log("Employee update successfully");
						showDialogMessage("Employee  update successfully.");
						popPanel.hide();
					}
				}
				if (name.equalsIgnoreCase("City")) {
					if (result.contains(-1)) {
						Console.log("inside update employee on success error occurs");
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet"+ "?type=" + 191;
						Window.open(url, "test", "enabled");
						return;
					} else {
						Console.log("City update successfully");
						showDialogMessage("City update successfully");
						popPanel.hide();
					}
				}
				if (name.equalsIgnoreCase("Locality")) {
					Console.log("in if (name.equalsIgnoreCase(Locality))" );
					if (result.contains(-1)) {
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet"+ "?type=" + 192;
						Window.open(url, "test", "enabled");
						return;
					} else {
						Console.log("Locality update successfully");
						showDialogMessage("Locality update successfully");
						popPanel.hide();
					}
				}

				if (result.contains(-1)) {
					showDialogMessage("Invalid Excel File");
				} else if (result.contains(-2)) {
					showDialogMessage("Error: Branch Name Already Exist!!");
				} else if (result.contains(-3)) {
					showDialogMessage("Error: City Name Already Exist!!");
				} else if (result.contains(-4)) {
					showDialogMessage("Error: Country Name Already Exist!!");
				} else if (result.contains(-5)) {
					showDialogMessage("Error: Customer Already Exist!!");
				} else if (result.contains(-6)) {
					showDialogMessage("Error: Department And Parent Department Already Exist!!");
				} else if (result.contains(-7)) {
					showDialogMessage("Error: Employee Name Already Exist!!");
				} else if (result.contains(-8)) {
					showDialogMessage("Error: Product Code Already Exist!!");
				} else if (result.contains(-9)) {
					showDialogMessage("Error: Locality Name Already Exist!!");
				} else if (result.contains(-10)) {
					showDialogMessage("Error: Procode Code Already Exist!!");
				} else if (result.contains(-11)) {
					showDialogMessage("Error: State Name Already Exist!!");
				} else if (result.contains(-12)) {
					showDialogMessage("Error: Storage Bin Already Exist!!");
				} else if (result.contains(-13)) {
					showDialogMessage("Error: Storage Location Already Exist!!");
				} else if (result.contains(-14)) {
					showDialogMessage("Error: Vendor Name Already Exist!!");
				} else if (result.contains(-15)) {
					showDialogMessage("Error: WareHouse Already Exist!!");
				} else if (result.contains(-16)) {
					showDialogMessage("Error: Asset Name Already Exists!!");
				} else if (result.contains(-17)) {
					showDialogMessage("Error: Please set date format to date column!!");
				} else if (result.contains(-18)) {
					showDialogMessage("Error: Please upload .xls format file!!");
				} else if (result.contains(-19)) {
					showDialogMessage("Error: Stock is already exist!!");
				} else if (result.contains(-20)) {
					showDialogMessage("Error: Contract/customer id does not exist!!");
				} else if (result.contains(-21)) {
					showDialogMessage("Error: No contract found for updation!!");
				} else if (result.contains(-22)) {
					showDialogMessage("Error: Customer branch is already exist!!");
				} else if (result.contains(-23)) {
					showDialogMessage("Error: Customer does not exist!!");
				} else if (result.contains(-24)) {
					showDialogMessage("Error: Please enter values in all Mandatory Fields !!");
				} else if (result.contains(-25)) {
					showDialogMessage("Error: Servicing Branch does not exist or does not match Please check !!");
				} else if (result.contains(-26)) {
					showDialogMessage("Error: City does not exist or does not match Please check !!");
				} else if (result.contains(-27)) {
					showDialogMessage("Error: Duplicate employee name found in the excel please check !!");
				} else {
					System.out.println("Successfully");
					if (name.equalsIgnoreCase("Update Employee")
							|| name.equalsIgnoreCase("Locality")
							|| name.equalsIgnoreCase("City")) {
					} else {
						showDialogMessage("Sucessfully");
						popPanel.hide();
					}
				}
			}
		});
	}
	
	private void updateLeaveBalance(final PopupPanel popPanel) {
		showWaitSymbol();
		dataTaskAsync.getEmployeeLeaveDetails(UserConfiguration.getCompanyId(),new AsyncCallback<ArrayList<EmployeeLeaveBalance>>() {
			@Override
			public void onFailure(Throwable caught) {
				hideWaitSymbol();
				System.out.println("First FAIL RPC...");
				popPanel.hide();
			}
			@Override
			public void onSuccess(ArrayList<EmployeeLeaveBalance> result) {
				System.out.println("FIRST RPC...");
				if (result != null && result.size() != 0) {
					if (result.get(0).getEmpId() == -1) {
						hideWaitSymbol();
						showDialogMessage("Invalid excel format.");
					} else {
						dataTaskAsync.validateEmployeeLeaveDetails(result,new AsyncCallback<ArrayList<EmployeeLeaveBalance>>() {

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated
								// method stub
								hideWaitSymbol();
								System.out.println("SECOND FAIL RPC...");
								popPanel.hide();
							}

							@Override
							public void onSuccess(ArrayList<EmployeeLeaveBalance> result) {
								System.out.println("SECOND RPC...");
								if (result.get(0).getEmpId() == -1) {
									hideWaitSymbol();
									showDialogMessage("Please find excel.");

									String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
									final String url = gwt+ "csvservlet"+ "?type="+ 164;
									Window.open(url,"test","enabled");
								} else {
									dataTaskAsync.updateLeaveBalance(result,new AsyncCallback<String>() {
										@Override
										public void onFailure(Throwable caught) {
											hideWaitSymbol();
											popPanel.hide();
										}

										@Override
										public void onSuccess(String result) {
											hideWaitSymbol();
											showDialogMessage("Leave balance update successful.");
											popPanel.hide();
										}
									});
								}
							}
						});
					}
				}
			}
		});
	}
	
	private void saveCustomerBranch(PopupPanel popPanel){
		Company c = new Company();
		showWaitSymbol();
		datamigAsync =  GWT.create(DataMigrationService.class);
		datamigAsync.uploadCustomerBranch(c.getCompanyId(), new AsyncCallback<ArrayList<CustomerBranchDetails>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<CustomerBranchDetails> result) {
				// TODO Auto-generated method stub
				if(result==null){
					hideWaitSymbol();
					showDialogMessage("Invalid Excel File Format.");
					return;
				}
				if(result.size() == 1 && result.get(0).getCount() == -2){
					hideWaitSymbol();
					showDialogMessage("You can only upload 150 branches.");
					return;
				}
				for(CustomerBranchDetails obj:result){
					if(obj.getCount()==-1){
						hideWaitSymbol();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 178;
						Window.open(url, "test", "enabled");
						showDialogMessage("Please find error file.");
						return;
					}
				}
				
				datamigAsync.saveCustomerBranch(result, new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						hideWaitSymbol();
					}

					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						hideWaitSymbol();
						showDialogMessage("Customer branch upload process started.");
					}
				});
				
			}
		});
	}
	
	private void reactonVendorProductPrice(PopupPanel popPanel) {
		final Company c = new Company();
		showWaitSymbol();
		datamigAsync =  GWT.create(DataMigrationService.class);
		datamigAsync.uploadVendorProductPrice(c.getCompanyId(), new AsyncCallback<ArrayList<VendorPriceListDetails>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();

			}

			@Override
			public void onSuccess(ArrayList<VendorPriceListDetails> result) {

				if(result==null){
					hideWaitSymbol();
					showDialogMessage("Invalid file.");
					return;
				}
				for(VendorPriceListDetails obj:result){
					if(obj.getCount()==-1){
						hideWaitSymbol();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 184;
						Window.open(url, "test", "enabled");
						showDialogMessage("Please find error file.");
						return;
					}
				}
				datamigAsync.vednorProductPrice(c.getCompanyId(), "Vendor Product Price", new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						hideWaitSymbol();
					}

					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						showDialogMessage(result);
						hideWaitSymbol();
					}
				} );
				
			}
		});
	}
	
	
	
	private void reactonCTCTemplate(PopupPanel popPanel) {

		final Company c = new Company();
		showWaitSymbol();
		datamigAsync =  GWT.create(DataMigrationService.class);
		
		datamigAsync.validateCTCTemplate(c.getCompanyId(), new AsyncCallback<ArrayList<CTCTemplate>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<CTCTemplate> result) {
				// TODO Auto-generated method stub


				if(result==null){
					hideWaitSymbol();
					showDialogMessage("Invalid file.");
					return;
				}
				for(CTCTemplate obj:result){
					if(obj.getCount()==-1){
						hideWaitSymbol();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 185;
						Window.open(url, "test", "enabled");
						showDialogMessage("Please find error file.");
						return;
					}
				}
			
				datamigAsync.uploadCTCTemplate(c.getCompanyId(), "CTC Template", new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						hideWaitSymbol();
					}

					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						showDialogMessage(result);
						hideWaitSymbol();
					}
				});
				
			}
		});
	}
	
	private void reactonUpdateAssetDetails() {

		final Company c = new Company();
		showWaitSymbol();
		
		datamigAsync.validateUpdateAssetDetails(c.getCompanyId(), new AsyncCallback<ArrayList<AssetMovementInfo>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<AssetMovementInfo> result) {
				// TODO Auto-generated method stub
				if(result==null){
					hideWaitSymbol();
					showDialogMessage("Invalid file.");
					return;
				}
				for(AssetMovementInfo obj:result){
					if(obj.getCount()==-1){
						hideWaitSymbol();
						String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 187;
						Window.open(url, "test", "enabled");
						showDialogMessage("Please find error file.");
						return;
					}
				}
				
				datamigAsync.uploadAssetDetails(c.getCompanyId(), AppConstants.UPDATEASSETDETAILS, new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						hideWaitSymbol();	
					}

					@Override
					public void onSuccess(String result) {
						showDialogMessage(result);
						hideWaitSymbol();						
					}
				});
			}
		});
	}
	
	public void multiSiteContractUpload(final PopupPanel popPanel){

		String entityName = "Contract Upload";
		final Company comp = new Company();
		showWaitSymbol();
		documentuploadAsync.readContractUploadExcelFile(entityName, comp.getCompanyId(), new AsyncCallback<ArrayList<String>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<String> contractExcellist) {
				// TODO Auto-generated method stub
				if(contractExcellist.size()==1){
					showDialogMessage(contractExcellist.get(0));
					hideWaitSymbol();
				}
				else{
					documentuploadAsync.reactonValidateContractUpload(contractExcellist, comp.getCompanyId(), new AsyncCallback<ArrayList<String>>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							hideWaitSymbol();
						}

						@Override
						public void onSuccess(ArrayList<String> contractexcel) {
							// TODO Auto-generated method stub
							if(contractexcel.size()==1){
								showDialogMessage(contractexcel.get(0));
								hideWaitSymbol();
							}
							else{
								documentuploadAsync.reactonContractUpload(contractexcel.get(1), comp.getCompanyId(), new AsyncCallback<String>() {

									@Override
									public void onFailure(Throwable caught) {
										// TODO Auto-generated method stub
										hideWaitSymbol();
									}

									@Override
									public void onSuccess(String result) {
										// TODO Auto-generated method stub
										showDialogMessage(result);
										hideWaitSymbol();
										popPanel.hide();
									}
								});
							}
						}
					});
				 }
			}
		});
	}
	
	
	
	//Ashwini Patil Date:20-10-2023 As per Nitin sir upload programs from data migration and screen should call the same common method so copying code from datamigrationpresenter 
	private void reactOnProductUpload(final String name){
		final Company c = new Company();
		datamigAsync.savedRecordsDeatils(c.getCompanyId(), name, true,LoginPresenter.loggedInUser,
				new AsyncCallback<ArrayList<Integer>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onSuccess(ArrayList<Integer> result) {
						// TODO Auto-generated method stub
						if (name.equalsIgnoreCase("Service Product")) {
							if (result.contains(-1)) {
								String gwt = com.google.gwt.core.client.GWT
										.getModuleBaseURL();
								final String url = gwt + "csvservlet"
										+ "?type=" + 196;
								Window.open(url, "test", "enabled");
								showDialogMessage("Please find error file!");
							} else if(result.contains(-2)) {
								showDialogMessage("Excel sheet header not matched Please get latest Excel sheet to upload! ");
							}
							else if(result.contains(0)){
								Console.log("Service Product Upload Process Started");
								showDialogMessage("Service Product Upload Process Started");
							}
							else{
								Console.log("Failed"+result);
							}
								
							return;
						}
						if (name.equalsIgnoreCase("Item Product")) {
							if (result.contains(-1)) {
								String gwt = com.google.gwt.core.client.GWT
										.getModuleBaseURL();
								final String url = gwt + "csvservlet"
										+ "?type=" + 197;
								Window.open(url, "test", "enabled");
								showDialogMessage("Please find error file!");
							} else if(result.contains(-2)) {
								showDialogMessage("Excel sheet header not matched Please get latest Excel sheet to upload! ");
							}
							else if(result.contains(0)){
								Console.log("Item Product Upload Process Started");
								showDialogMessage("Item Product Upload Process Started");
							}
							else{
								Console.log("Failed"+result);
							}
								
							return;
						}
										
							
					}
			
		});
	}
	public TextArea getInfo() {
		return info;
	}

	public void setInfo(TextArea info) {
		this.info = info;
	}
	
	public TextBox getTxtAppId() {
		return txtAppId;
	}

	public void setTxtAppId(TextBox txtAppId) {
		this.txtAppId = txtAppId;
	}

	public TextBox getTxtCompanyId() {
		return txtCompanyId;
	}

	public void setTxtCompanyId(TextBox txtCompanyId) {
		this.txtCompanyId = txtCompanyId;
	}

	public TextBox getTxtCompanyName() {
		return txtCompanyName;
	}

	public void setTxtCompanyName(TextBox txtCompanyName) {
		this.txtCompanyName = txtCompanyName;
	}

}
