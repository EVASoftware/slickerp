package com.slicktechnologies.client.views.popups;

import com.google.gwt.event.dom.client.ClickEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.client.views.inventory.recievingnote.ProductTableGRN;

public class PartialGRNCancellationPopup extends PopupScreen{

	public ProductTableGRN table;
	
	public PartialGRNCancellationPopup(){
		super();
		getPopup().getElement().addClassName("setWidth");
		createGui();
	}
	
	private void initializeWidget() {
		table = new ProductTableGRN();
		table.setEnable(false);
		
	}
	
	@Override
	public void createScreen() {
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fMaterialInformation = fbuilder.setlabel("Material Information").widgetType(FieldType.Grouping).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",table.getTable());
		FormField fmatrialTable = fbuilder.setMandatory(false).setColSpan(0).setRowSpan(0).build();
		
		FormField [][] formfield = {
									{fMaterialInformation},
									{fmatrialTable}
							       };
		 this.fields = formfield;
								   
		
	}

	@Override
	public void onClick(ClickEvent event) {

	}

	public ProductTableGRN getTable() {
		return table;
	}

	public void setTable(ProductTableGRN table) {
		this.table = table;
	}
	
	
}
