package com.slicktechnologies.client.views.popups;

import java.util.HashMap;

import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Locality;

/**
 * 16-04-2018 for locality wise search through composite 
 * and also city composite
 * @author Vijay Chougule
 * for :- Orion pest
 */
public class DocumentsComposite extends Composite implements SelectionHandler<Suggestion>{

	 public SuggestBox localityName,cityName;
	 
	 /** The name to person info Map */
	public   HashMap<String,Locality>localityNameInfo = new HashMap<String,Locality>();
	 /** The panel. */
    private FlowPanel panel;
    
    private FormField flocalityName,fcityName;
    private FormField[][]formfield;
    
    private Locality localityEntity;

	// city composite
	public   HashMap<String,City>cityNameInfo = new HashMap<String,City>();
	private Locality cityEntity;
	
	boolean flag;
	
	public DocumentsComposite(){
		super();
		flag = true;
		createForm();
		initWidget(panel);
		initializeLocality();
		
	}
	
	// for city search composite
	public DocumentsComposite(boolean cityFlag){
		super();
		this.flag = cityFlag;
		createForm();
		initWidget(panel);
		initializeLocality();
	}


	public void initializeLocality() {
		// TODO Auto-generated method stub
		for(Locality locality: LoginPresenter.globalLocality){
			String localityName = locality.getLocality();
			localityNameInfo.put(localityName, locality);
		}
		
		for(City city: LoginPresenter.globalCity){
			String cityName = city.getCityName();
			cityNameInfo.put(cityName, city);
		}
		if(flag){
			initalizeLocalityNameOracle();
		}else{
			initalizeCityNameOracle();
		}
		
			
	}

	
	/**
 	 * Initalize City name oracle.
 	 */
	 private void initalizeCityNameOracle() {
		 MultiWordSuggestOracle orcl = (MultiWordSuggestOracle) this.cityName.getSuggestOracle();
		 for(String name:cityNameInfo.keySet())
		   orcl.add(name);	
	}

	/**
 	 * Initalize name oracle.
 	 */
 	private void initalizeLocalityNameOracle()
 	{
 		Console.log("In Name Oracle Entered");
		 MultiWordSuggestOracle orcl = (MultiWordSuggestOracle) this.localityName.getSuggestOracle();
		 for(String name:localityNameInfo.keySet())
		   orcl.add(name);		 
		 
	 }
	 
 	

	private void createForm() {

		FormFieldBuilder builder;
		
		localityName=new SuggestBox();
		builder=new FormFieldBuilder("Locality Name",localityName);
		flocalityName=builder.setMandatory(false).build();
		
		cityName = new SuggestBox();
		builder=new FormFieldBuilder("City Name",cityName);
		fcityName=builder.setMandatory(false).build();
		
		if(flag){
			formfield=new FormField[][]
					{
						{flocalityName }
				    };
		}else{
			
			formfield=new FormField[][]
					{
						{fcityName }
				    };
		}
		
				
		FlexForm form = new FlexForm(formfield,FormStyle.ROWFORM);
		panel=new FlowPanel();
		form.setWidth("98%");
		panel.add(form);
		
	}


	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		// TODO Auto-generated method stub
		
		SuggestBox sb=(SuggestBox) event.getSource();
		String selectedItem=sb.getText().trim();
		
		
		if(sb==this.localityName)
		{
//			String[]xyz=selectedItem.split("-");
//			if(xyz.length==0)
//			{
//				sb.setText("");
//				return;	
//			}
//			else
//			{
				localityEntity=localityNameInfo.get(selectedItem);
				
				if(localityEntity==null)
					sb.setText("");
				
				else
				{
					this.localityName.setText(localityEntity.getLocality());
				}
//			}
		}
		else if(sb==this.cityName){
			
//			String[]xyz=selectedItem.split("-");
//			if(xyz.length==0)
//			{
//				sb.setText("");
//				return;	
//			}
//			else
//			{
				cityEntity=localityNameInfo.get(selectedItem);
				
				if(cityEntity==null)
					sb.setText("");
				
				else
				{
					this.cityName.setText(localityEntity.getLocality());
				}
//			}
		}
	}


	public SuggestBox getLocalityName() {
		return localityName;
	}


	public void setLocalityName(SuggestBox localityName) {
		this.localityName = localityName;
	}

	public SuggestBox getCityName() {
		return cityName;
	}

	public void setCityName(SuggestBox cityName) {
		this.cityName = cityName;
	}
	
	
	
		
}
