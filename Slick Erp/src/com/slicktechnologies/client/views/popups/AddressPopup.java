package com.slicktechnologies.client.views.popups;

import java.util.HashMap;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;


public class AddressPopup extends PopupScreen {
	Label lblAddress;
	
	/**
	 * @author Ashwini Patil
	 * @since 24-05-2022
	 * added branch, area and location label as per requirement
	 */
	Label lblBranch; 
	Label lblArea;
	Label lblLocation;
	

	public AddressPopup(){
		super();
		createGui();
		
	}
	
	private void initializeWidgets() {
		lblAddress = new Label("");
		lblBranch=new Label("");
		lblLocation=new Label("");
		lblArea=new Label("");
	}

	
	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initializeWidgets();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField faddressInfo = fbuilder.setlabel("Service Branch Address").widgetType(FieldType.Grouping).build();
		
		fbuilder = new FormFieldBuilder("",  lblAddress);
		FormField flbAddress = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",  lblBranch);
		FormField flblBranch = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",  lblLocation);
		FormField flblLocation = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",  lblArea);
		FormField flblArea = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
	
		FormField [][]formfield = {
				{faddressInfo},
				{flblBranch},
				{flbAddress},
				{flblLocation},
				{flblArea},
		};
		this.fields=formfield;
				
	}
	
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	public Label getLblAddress() {
		return lblAddress;
	}

	public void setLblAddress(Label lblAddress) {
		this.lblAddress = lblAddress;
	}	
	public Label getLblBranch() {
		return lblBranch;
	}

	public void setLblBranch(Label lblBranch) {
		this.lblBranch = lblBranch;
	}


	public Label getLblLocation() {
		return lblLocation;
	}

	public void setLblLocation(Label lblLocation) {
		this.lblLocation = lblLocation;
	}


	
	public Label getLblArea() {
		return lblArea;
	}

	public void setLblArea(Label lblArea) {
		this.lblArea = lblArea;
	}
}

