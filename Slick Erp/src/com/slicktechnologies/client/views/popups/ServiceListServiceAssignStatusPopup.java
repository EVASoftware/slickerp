package com.slicktechnologies.client.views.popups;
/**
 * Updated By:Viraj
 * Date: 29-06-2019
 * Description: To load Popup with Status dropdown
 */
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Service;

public class ServiceListServiceAssignStatusPopup extends PopupScreen{
	public ListBox status;
	
	public ServiceListServiceAssignStatusPopup(){
		super();
		createGui();
	}
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	public void initializeWidgets(){
		
		status=new ListBox();
		AppUtility.setStatusListBox(status,Service.getStatusList());
		
	}
	
	
	@Override
	public void createScreen() {

		initializeWidgets();
		

		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder("Status", status);
		FormField folbBranch = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
	
		FormField [][]formfield = {
				{folbBranch},
		};
		this.fields=formfield;
	
	}
	
	public ListBox getStatus() {
		return status;
	}

	public void setStatus(ListBox status) {
		this.status = status;
	}
}
