package com.slicktechnologies.client.views.popups;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranchTable;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.scheduling.TeamManagement;

public class SRSchedulePrintPopup extends PopupScreen {

//	ObjectListBox<TeamManagement> olbTeam;
	ObjectListBox<Employee> olbEmployee;
	DateBox dbFromDate;
	DateBox dbTodate;
	
	EmailTextBox tbEmailBox ;
	Button btAdd,btnprint;
	EmployeeBranchTable emailTable;
	
	InlineLabel lbltext;
	
	public SRSchedulePrintPopup(){
		super();
		getPopup().setWidth("500px");
		createGui();
	}
	
	private void initializeWidget() {
//		olbTeam = new ObjectListBox<TeamManagement>();
		olbEmployee = new ObjectListBox<Employee>();
		olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERSERVICELIST, "Service Engineer");
		
		
		dbFromDate = new DateBox();
		dbTodate = new DateBox();
		
		tbEmailBox = new EmailTextBox();
		btAdd = new Button("Add");
		btAdd.addClickHandler(this);
		emailTable = new EmployeeBranchTable();
		emailTable.connectToLocal();
		
		btnprint = new Button("Print Schedule");
		btnprint.addClickHandler(this);
		
		lbltext = new InlineLabel("If services are grater than 60 then print schedule will not support. if services are gretaer than 60 then use email option to get print schedule");
	}

	@Override
	public void onClick(ClickEvent event) {

	
		if(event.getSource().equals(btAdd)){
			if(tbEmailBox.getValue() != null && !tbEmailBox.getValue().equalsIgnoreCase("")){
				EmployeeBranch branch = new EmployeeBranch();
				branch.setBranchName(tbEmailBox.getValue());				
				emailTable.getDataprovider().getList().add(branch);
				
			}else{
				showDialogMessage("Please add email");
			}
		}
		

	}

	

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initializeWidget();
//		initalizeTeamListBox();
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSchedulePrint=fbuilder.setlabel("Schedule Print").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
//		fbuilder = new FormFieldBuilder("Team", olbTeam);
//		FormField folbTeam= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Technician", olbEmployee);
		FormField folbEmployee= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("From Date", dbFromDate);
		FormField fdbFromDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		
		fbuilder = new FormFieldBuilder("To Date", dbTodate);
		FormField fdbTodate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Email",tbEmailBox);
		FormField ftbEmailBox= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",btAdd);
		FormField fbtAdd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", emailTable.getTable());
		FormField femailTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();//original 4

		
		fbuilder = new FormFieldBuilder("",btnprint);
		FormField fbtnprint= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",lbltext);
		FormField flbltext= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		FormField fGroupingEmailInformation=fbuilder.setlabel("Email Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();//original 4

	
		FormField[][]  formfield = {  
				{fgroupingSchedulePrint},
				{fdbFromDate , fdbTodate,folbEmployee,fbtnprint},
				{flbltext},
				{fGroupingEmailInformation},
				{ftbEmailBox , fbtAdd},
				{femailTable}

			
		};
		this.fields=formfield;
		
		

	}
//	private void initalizeTeamListBox() {
//		MyQuerry querry = new MyQuerry();
//		querry.setQuerryObject(new TeamManagement());
//		getOlbTeam().MakeLive(querry);
//	}

//	public ObjectListBox<TeamManagement> getOlbTeam() {
//		return olbTeam;
//	}
//
//	public void setOlbTeam(ObjectListBox<TeamManagement> olbTeam) {
//		this.olbTeam = olbTeam;
//	}

	public ObjectListBox<Employee> getOlbEmployee() {
		return olbEmployee;
	}

	public void setOlbEmployee(ObjectListBox<Employee> olbEmployee) {
		this.olbEmployee = olbEmployee;
	}

	public DateBox getDbFromDate() {
		return dbFromDate;
	}

	public void setDbFromDate(DateBox dbFromDate) {
		this.dbFromDate = dbFromDate;
	}

	public DateBox getDbTodate() {
		return dbTodate;
	}

	public void setDbTodate(DateBox dbTodate) {
		this.dbTodate = dbTodate;
	}

	public EmployeeBranchTable getEmailTable() {
		return emailTable;
	}

	public void setEmailTable(EmployeeBranchTable emailTable) {
		this.emailTable = emailTable;
	}
	

	public Button getBtnprint() {
		return btnprint;
	}

	public void setBtnprint(Button btnprint) {
		this.btnprint = btnprint;
	}

	
}
