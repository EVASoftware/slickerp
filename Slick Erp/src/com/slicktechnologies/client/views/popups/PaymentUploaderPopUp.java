package com.slicktechnologies.client.views.popups;

import java.util.ArrayList;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.slicktechnologies.client.services.DocumentUploadService;
import com.slicktechnologies.client.services.DocumentUploadServiceAsync;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;

public class PaymentUploaderPopUp extends PopupScreen{

	
	UploadComposite uploadComposite;
	
	DocumentUploadServiceAsync datauploadService = GWT.create(DocumentUploadService.class);

	public PaymentUploaderPopUp(){
		super();
		createGui();
		
	}
	
	private void initializeWidgets() {
		uploadComposite = new UploadComposite();
	}
	
	
	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initializeWidgets();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fpaymentUpload = fbuilder.setlabel("Payment Uploader").widgetType(FieldType.Grouping).build();
		
		fbuilder = new FormFieldBuilder("",  uploadComposite);
		FormField fuploadComposite = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
				
	
		FormField [][]formfield = {
				{fpaymentUpload},
				{fuploadComposite},
		};
		this.fields=formfield;}
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		final Company company = new Company();
		
		if(event.getSource()== this.getLblOk()){
			showWaitSymbol();
			datauploadService.validatePaymentUploader(company.getCompanyId(), new AsyncCallback<ArrayList<CustomerPayment>>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<CustomerPayment> result) {
					if(result==null){
						hideWaitSymbol();
						showDialogMessage("Invalid file.");
						hidePopUp();
						return;
					}
					for(CustomerPayment obj:result){
						if(obj.getCount()==-1){
							hideWaitSymbol();
							hidePopUp();
							String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
							final String url = gwt + "csvservlet" + "?type=" + 190;
							Window.open(url, "test", "enabled");
							showDialogMessage("Please find error file.");
							return;
						}
					}
					
					datauploadService.uploadPaymentDetails(company.getCompanyId(), "Payment Updation", new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							hideWaitSymbol();
						}

						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							hideWaitSymbol();
							showDialogMessage(result);
							hidePopUp();
						}
					});
					
					
				}
			});
		}
		
		if(event.getSource() == this.getLblCancel()){
			hidePopUp();
		}
	}


}
