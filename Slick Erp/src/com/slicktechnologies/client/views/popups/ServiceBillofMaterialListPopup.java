package com.slicktechnologies.client.views.popups;

import com.google.gwt.event.dom.client.ClickEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;

public class ServiceBillofMaterialListPopup  extends PopupScreen{

	ServiceProductListTable serviceProListTable;
	
	public ServiceBillofMaterialListPopup(){
		super();
		createGui();
	}
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	public void initializeWidgets(){
		 serviceProListTable = new ServiceProductListTable();
	}
	@Override
	public void createScreen() {
		initializeWidgets();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fserialInfo = fbuilder.setlabel("Service Product List Details").widgetType(FieldType.Grouping).build();
		
		fbuilder = new FormFieldBuilder("",  serviceProListTable.getTable());
		FormField fproSerNoTable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField [][]formfield = {
				{fserialInfo},
				{fproSerNoTable},
		};
		this.fields=formfield;
		// TODO Auto-generated method stub
		
	}

	public ServiceProductListTable getServiceProListTable() {
		return serviceProListTable;
	}

	public void setServiceProListTable(ServiceProductListTable serviceProListTable) {
		this.serviceProListTable = serviceProListTable;
	}

}
