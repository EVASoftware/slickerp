package com.slicktechnologies.client.views.popups;

import com.google.gwt.event.dom.client.ClickEvent;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
/**
 * @author Viraj
 * Date: 15-06-2019
 * Description: To load Popup with branch dropdown
 */
public class ServiceListServiceAssignBranchPopup extends PopupScreen{
	
	  ObjectListBox<Branch> olbBranch;
	
	public ServiceListServiceAssignBranchPopup(){
		super();
		createGui();
	}
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	public void initializeWidgets(){
		
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
	}
	
	
	@Override
	public void createScreen() {

		initializeWidgets();
		

		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder("Branch",  olbBranch);
		FormField folbBranch = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
	
		FormField [][]formfield = {
				{folbBranch},
		};
		this.fields=formfield;
	
	}

	public ObjectListBox<Branch> getOlbBranch() {
		return olbBranch;
	}

	public void setOlbBranch(ObjectListBox<Branch> olbBranch) {
		this.olbBranch = olbBranch;
	}

	

}
