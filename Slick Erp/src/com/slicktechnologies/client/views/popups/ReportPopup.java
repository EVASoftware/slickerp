package com.slicktechnologies.client.views.popups;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;

public class ReportPopup extends PopupScreen{

	DateBox dbfromDate,dbToDate;
	
	CsvServiceAsync csvService = GWT.create(CsvService.class);
	
	public ReportPopup() {
		super();
		createGui();
	}

	@Override
	public void createScreen() {
		initializeWidgets();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fsearchFilters = fbuilder.setlabel("Search filters").widgetType(FieldType.Grouping).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("From Date",  dbfromDate);
		FormField fdbfromDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("To Date",  dbToDate);
		FormField fdbToDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
				
	
		FormField [][]formfield = {
				{fsearchFilters},
				{fdbfromDate,fdbToDate},
		};
		this.fields=formfield;
		
	}

	private void initializeWidgets() {
		dbfromDate = new DateBoxWithYearSelector();
		dbToDate = new DateBoxWithYearSelector();
		
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		System.out.println("On Click");
		if(event.getSource()==this.getLblOk()){
			csvService.setComplainReport(dbfromDate.getValue(), dbToDate.getValue(), new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onSuccess(Void result) {
					// TODO Auto-generated method stub
					String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url = gwt + "csvservlet" + "?type=" + 153;
					Window.open(url, "test", "enabled");
					hidePopUp();
				}
			});
		}
		if(event.getSource()==this.getLblCancel()){
			hidePopUp();
		}
	}
	
}
