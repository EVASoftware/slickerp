package com.slicktechnologies.client.views.popups;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.client.utils.Console;

public class ContractResetMessagePopup extends PopupScreen{

	RadioButton rbcancel,rbedelete;
	CheckBox cbcancel;
	boolean serviceMessage = false;
	boolean invocieMessage = false;
	
	public Label lblservicemsg,lblInvoicemsg;
	
	public ContractResetMessagePopup(){
		super();
		createGui();

	}
	
	private void initializeWidget() {
		rbcancel = new RadioButton("Cancel");
		rbcancel.addClickHandler(this);
		rbcancel.setValue(true);

		rbedelete = new RadioButton("Delete");
		rbedelete.addClickHandler(this);
		rbedelete.setValue(false);
		
		cbcancel = new CheckBox();
		cbcancel.setValue(false);
		
		lblservicemsg = new Label();
		lblservicemsg.setText("There may be completed service! please select option to cancel or delete services");
		lblInvoicemsg = new Label();
		lblInvoicemsg.setText("There may be some invoice document created! please select checkbox to cancel or remain same");
	}

	@Override
	public void createScreen() {
		
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
//		fbuilder = new FormFieldBuilder();
//		FormField fgroupingContractCancel=fbuilder.setlabel("Contract Reset Popup").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", lblservicemsg);
		FormField flblservicemsg= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("", lblInvoicemsg);
		FormField flblInvoicemsg= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Do you want to cancel", rbcancel);
		FormField frbcancel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Do you want to delete", rbedelete);
		FormField frbedelete= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Do you want to cancel", cbcancel);
		FormField fcbcancel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupinginvoices=fbuilder.setlabel("invoices").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingServices=fbuilder.setlabel("Services").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		
		if(serviceMessage && !invocieMessage){
			FormField[][] formfield = {  
					{fgroupingServices},
					{flblservicemsg},
					{frbcancel,frbedelete}
				
			};
			this.fields=formfield;
		}
		else if(invocieMessage && !serviceMessage){
			FormField[][] formfield = {  
					{fgroupinginvoices},
					{flblInvoicemsg},
					{fcbcancel}
				
			};
			this.fields=formfield;
		}
		else{
			Console.log("showing popup");
			FormField[][] formfield = {  
					{fgroupingServices},
					{flblservicemsg},
					{frbcancel,frbedelete},
					{fgroupinginvoices},
					{flblInvoicemsg},
					{fcbcancel}
				
			};
			this.fields=formfield;
		}
		
		
		
	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		
		if(event.getSource().equals(rbcancel)){
			rbedelete.setValue(false);
		}
		if(event.getSource().equals(rbedelete)){
			rbcancel.setValue(false);
		}
	}

	public boolean isServiceMessage() {
		return serviceMessage;
	}

	public void setServiceMessage(boolean serviceMessage) {
		this.serviceMessage = serviceMessage;
	}

	public boolean isInvocieMessage() {
		return invocieMessage;
	}

	public void setInvocieMessage(boolean invocieMessage) {
		this.invocieMessage = invocieMessage;
	}

	public RadioButton getRbcancel() {
		return rbcancel;
	}

	public void setRbcancel(RadioButton rbcancel) {
		this.rbcancel = rbcancel;
	}

	public RadioButton getRbedelete() {
		return rbedelete;
	}

	public void setRbedelete(RadioButton rbedelete) {
		this.rbedelete = rbedelete;
	}

	public CheckBox getCbcancel() {
		return cbcancel;
	}

	public void setCbcancel(CheckBox cbcancel) {
		this.cbcancel = cbcancel;
	}

}
