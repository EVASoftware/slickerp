package com.slicktechnologies.client.views.popups;

import java.util.ArrayList;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.slicktechnologies.client.services.DocumentUploadService;
import com.slicktechnologies.client.services.DocumentUploadServiceAsync;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;

public class TallyFilePaymentUploaderPopUp extends PopupScreen{

	UploadComposite uploadComposite;
	
	DocumentUploadServiceAsync datauploadService = GWT.create(DocumentUploadService.class);

	public TallyFilePaymentUploaderPopUp(){
		super();
		createGui();
		
	}
	
	private void initializeWidgets() {
		uploadComposite = new UploadComposite();
	}
	
	
	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initializeWidgets();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fpaymentUpload = fbuilder.setlabel("Tally Payment Uploader").widgetType(FieldType.Grouping).build();
		
		fbuilder = new FormFieldBuilder("",  uploadComposite);
		FormField fuploadComposite = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
				
	
		FormField [][]formfield = {
				{fpaymentUpload},
				{fuploadComposite},
		};
		this.fields=formfield;}
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		final Company company = new Company();
		
		if(event.getSource()== this.getLblOk()){
			showWaitSymbol();
			datauploadService.validateTallyPaymentUploader(company.getCompanyId(), new AsyncCallback<ArrayList<CustomerPayment>>() {

				@Override
				public void onFailure(Throwable arg0) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onSuccess(ArrayList<CustomerPayment> result) {
					// TODO Auto-generated method stub
					if(result==null){
						hideWaitSymbol();
						showDialogMessage("Excel File Header Not Matched");
						hidePopUp();
						return;
					}
					for(CustomerPayment obj:result){
						if(obj.getCount()==-1){
							hideWaitSymbol();
							hidePopUp();
							String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
							final String url = gwt + "csvservlet" + "?type=" + 193;
							Window.open(url, "test", "enabled");
							showDialogMessage("Please find error file.");
							return;
						}
					}
					
					datauploadService.uploadTallyPaymentDetails(company.getCompanyId(), "Payment Updation", new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable arg0) {
							// TODO Auto-generated method stub
							hideWaitSymbol();
						}

						@Override
						public void onSuccess(String result1) {
							// TODO Auto-generated method stub
							hideWaitSymbol();
							showDialogMessage(result1);
							hidePopUp();
						}
					});
				}
			});
		}
		
		if(event.getSource() == this.getLblCancel()){
			hidePopUp();
		}
	}



}
