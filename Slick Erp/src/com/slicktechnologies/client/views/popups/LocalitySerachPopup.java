package com.slicktechnologies.client.views.popups;

import com.google.gwt.event.dom.client.ClickEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;

public class LocalitySerachPopup extends PopupScreen{

	public DocumentsComposite locality;  
	
	public LocalitySerachPopup(){
		super();
		createGui();
	}

	

	@Override
	public void createScreen() {
		
		initializeLocality();
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder();
		FormField flocalityInfo = fbuilder.setlabel("Locality Information").widgetType(FieldType.Grouping).build();
		
		fbuilder = new FormFieldBuilder(" ",locality);
		FormField ftblocality = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
	
		FormField [][]formfield = {
				{flocalityInfo},
				{ftblocality},
		};
		this.fields=formfield;
	}
	
	
	public void initializeLocality() {
		locality = new DocumentsComposite();
		
	}


	
	

	public DocumentsComposite getLocality() {
		return locality;
	}



	public void setLocality(DocumentsComposite locality) {
		this.locality = locality;
	}



	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}
	

}
