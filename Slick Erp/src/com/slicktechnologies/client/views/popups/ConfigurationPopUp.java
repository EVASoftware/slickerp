package com.slicktechnologies.client.views.popups;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.client.services.DemoConfigService;
import com.slicktechnologies.client.services.DemoConfigServiceAsync;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.ScreenName;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class ConfigurationPopUp extends PopupScreen{

	Button btnDefualtConfig;
	Button btnUserRole;
	
	ScreenNameTable screenNameTable;

	DemoConfigServiceAsync democonfigAsync = GWT.create(DemoConfigService.class);
	
	/**
	 * @author Anil @since 25-11-2021
	 */
	public TextBox tbRefernceLinkUrl; //Ashwini Patil added public keyword
	
	public ConfigurationPopUp(){
		super();
		createGui();
		screenNameTable.getTable().setWidth("800px");
		screenNameTable.getTable().setHeight("300px");

	}
	
	
	private void initializeWidgets() {
		btnDefualtConfig = new Button("Default Configuration");
		btnDefualtConfig.addClickHandler(this);
		
		btnUserRole = new Button("Create User Role");
		btnUserRole.addClickHandler(this);
		
		tbRefernceLinkUrl=new TextBox();
		
		screenNameTable = new ScreenNameTable();
		
	}
	
	@Override
	public void createScreen() {
		initializeWidgets();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fconfiggrouping = fbuilder.setlabel("Configuration").widgetType(FieldType.Grouping).build();
		
		fbuilder = new FormFieldBuilder("",  btnDefualtConfig);
		FormField fbtnDefualtConfig = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",  btnUserRole);
		FormField fbtnUserRole = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",  screenNameTable.getTable());
		FormField fscreenNameTable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Reference Link URL"+'\n'+"(Do not include https://, http://, / , and -dot- in URL)",  tbRefernceLinkUrl);
		FormField ftbRefernceLinkUrl = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
				
	
		FormField [][]formfield = {
				{fconfiggrouping},
//				{fbtnDefualtConfig,fbtnUserRole},
				{ftbRefernceLinkUrl},
				{fscreenNameTable}
		};
		this.fields=formfield;
				
	
	}
	
	
	

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource()==btnDefualtConfig){
			reactOnCreateDefaultConfiguration();
		}
		
		if(event.getSource()==btnUserRole){
			reactOncreateUserRoleData();
		}
		
		if(event.getSource().equals(this.lblOk)){
//			reactOnCreateSelectedConfiguration();
			if(validateUrl()){
				reactOnCopyConfiguration();
			}
		}
		if(event.getSource().equals(this.lblCancel)){
			hidePopUp();
		}
	}

	/**
	 *  Date : 10/02/2021 By Priyanka.
	 *  Des : Default Configuration Requirement.
	 */

	private void reactOncreateUserRoleData() {
		Company comp = new Company();
		showWaitSymbol();
		democonfigAsync.createUserRoleData(comp.getCompanyId(), new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				showDialogMessage("Failed Please try again!");
				hideWaitSymbol();
				hidePopUp();

			}

			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
				showDialogMessage(result);
				hidePopUp();
			}
		});
	}

	private void reactOnCreateSelectedConfiguration() {
		// TODO Auto-generated method stub
		List<ScreenName> list = screenNameTable.getDataprovider().getList();
		ArrayList<String> selectedlist = getSelectedlist(list);
		if(selectedlist.size()==0){
			showDialogMessage("Please select records");
		}
		else{
			Company comp = new Company();
			showWaitSymbol();
			democonfigAsync.createselectedConfiguration(comp.getCompanyId(), selectedlist, new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					showDialogMessage("Failed Please try again!");
					hideWaitSymbol();
					hidePopUp();
				}

				@Override
				public void onSuccess(String result) {
					hideWaitSymbol();
					showDialogMessage(result);
					hidePopUp();

				}
			});
		}
		
		
	}

	private ArrayList<String> getSelectedlist(List<ScreenName> list) {
		ArrayList<String> selectedlist = new ArrayList<String>();
		for(ScreenName screenName : list){
			if(screenName.isCheck()){
				selectedlist.add(screenName.getScreenName());
			}
		}
		return selectedlist;
	}

	private void reactOnCreateDefaultConfiguration() {
		Company comp = new Company();
		showWaitSymbol();
		democonfigAsync.createDefaultConfiguration(comp.getCompanyId(), new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				showDialogMessage("Failed Please try again!");
				hideWaitSymbol();
				hidePopUp();

			}

			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
				showDialogMessage(result);
				hidePopUp();
			}
		});
	}


	public Button getBtnDefualtConfig() {
		return btnDefualtConfig;
	}


	public void setBtnDefualtConfig(Button btnDefualtConfig) {
		this.btnDefualtConfig = btnDefualtConfig;
	}

	public Button getBtnUserRole() {
		return btnUserRole;
	}


	public void setBtnUserRole(Button btnUserRole) {
		this.btnUserRole = btnUserRole;
	}


	public ScreenNameTable getScreenNameTable() {
		return screenNameTable;
	}


	public void setScreenNameTable(ScreenNameTable screenNameTable) {
		this.screenNameTable = screenNameTable;
	}
	
	
	private ArrayList<ScreenName> getSelectedScreenList(List<ScreenName> list) {
		ArrayList<ScreenName> selectedlist = new ArrayList<ScreenName>();
		for(ScreenName screenName : list){
			if(screenName.isCheck()){
				selectedlist.add(screenName);
			}
		}
		return selectedlist;
	}
	
	public String getUpdatedUrl(String urlCalled){
		try{
			if(urlCalled.contains("-dot-")){
				urlCalled=urlCalled.replace("-dot-",".");
			}
			if(urlCalled.contains("https://")){
				urlCalled=urlCalled.replace("https://","");
			}
			if(urlCalled.contains("http://")){
				urlCalled=urlCalled.replace("http://","");
			}
			if(urlCalled.contains("/")){
				urlCalled=urlCalled.replace("/", "").trim();
			}
		}catch(Exception e){
			
		}
		return urlCalled;
	}
	
	private void reactOnCopyConfiguration() {
		List<ScreenName> list = screenNameTable.getDataprovider().getList();
		ArrayList<ScreenName> selectedlist = getSelectedScreenList(list);
		
		String url=Window.Location.getHref();
		Console.log("OUR URL : "+url);
		
		url=getUpdatedUrl(url);
		Console.log("OUR UPDATED URL : "+url);
		
		Company comp = new Company();
		showWaitSymbol();
		democonfigAsync.copyConfiguration(comp.getCompanyId(),url,tbRefernceLinkUrl.getValue(),selectedlist, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				showDialogMessage("Failed Please try again!");
				hideWaitSymbol();
				hidePopUp();
			}

			@Override
			public void onSuccess(String result) {
				hideWaitSymbol();
				showDialogMessage(result);
				hidePopUp();
			}
		});
	}


	private boolean validateUrl() {
		String url=Window.Location.getHref();
		Console.log("OUR URL : "+url);
		
		if(tbRefernceLinkUrl.getValue()==null||tbRefernceLinkUrl.getValue().equals("")){
			showDialogMessage("Please insert reference link url");
			return false;
		}
		
		if(!tbRefernceLinkUrl.getValue().contains(".appspot.com")){
			showDialogMessage("Please insert valid reference link url for copying configuration");
			return false;
		}
		
		if(tbRefernceLinkUrl.getValue().contains("-dot-")||tbRefernceLinkUrl.getValue().contains("https://")||tbRefernceLinkUrl.getValue().contains("http://")||tbRefernceLinkUrl.getValue().contains("/")){
			showDialogMessage("Reference link should not contain https://, http://, /, and -dot-.");
			return false;
		}
		
		url=getUpdatedUrl(url);
		Console.log("OUR UPDATED URL : "+url);
		
		if(url.equals(tbRefernceLinkUrl.getValue())){
			showDialogMessage("Can not insert the same link url as reference link url");
			return false;
		}
		
		boolean flag=false;
		for(ScreenName obj:screenNameTable.getValue()){
			if(obj.isCheck()){
				flag=true;
				break;
			}
		}
		
		if(!flag){
			showDialogMessage("Please select atleast one record for copying configuration");
			return false;
		}
		
		
		return true;
	}
	

}
