package com.slicktechnologies.client.views.popups;

import com.google.gwt.user.client.ui.TextArea;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;

public class CompanyIntroductionPopUp extends PopupScreen{

	
	TextArea taIntroduction;
	
	
	public CompanyIntroductionPopUp(){
		super();
		createGui();
		
	}
	
	@Override
	public void createScreen() {

		taIntroduction = new TextArea();
		taIntroduction.setHeight("400px");
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField downalodFormatInfo = fbuilder.setlabel("Introduction").setColSpan(4).widgetType(FieldType.Grouping).build();
		
		
		fbuilder = new FormFieldBuilder("Applicable To -  Service Quotation, Sales Quotation.    For Bold - <b></b> i.e. <b>This is bold test</b>)      For Page Break - <pagebreak>", taIntroduction);
		FormField ftaIntroduction = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(0).build();
		
		
		FormField[][] formfield = {
				{ downalodFormatInfo },
				{ftaIntroduction },
				};
		this.fields = formfield;
	}

	public TextArea getTaIntroduction() {
		return taIntroduction;
	}

	public void setTaIntroduction(TextArea taIntroduction) {
		this.taIntroduction = taIntroduction;
	}

	
	
}
