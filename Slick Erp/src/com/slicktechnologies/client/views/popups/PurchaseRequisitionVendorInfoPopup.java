package com.slicktechnologies.client.views.popups;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.views.lead.LeadPresenterSearchProxy;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;

/**
 * Date 12-01-2019 
 * @author Vijay Chougule
 * Des :- for NBHC Inventory management When Approving PR 
 * need information for Purchasing material from local vendor or from head office  
 */
public class PurchaseRequisitionVendorInfoPopup extends PopupScreen{

	CheckBox chkLocalVendor;
	CheckBox chkHOVendor;
	Button btnNewVendor;
	PersonInfoComposite vendorComp; 
	
	NewVendorInformationPopUp newVendorInfo = new NewVendorInformationPopUp();
	GenricServiceAsync genasync = GWT.create(GenricService.class);

	public double vendorprice = 0;
	
	public PurchaseRequisitionVendorInfoPopup(){
		super();
		createGui();
		
		newVendorInfo.getLblOk().addClickHandler(this);
		newVendorInfo.getLblCancel().addClickHandler(this);
		
	}
	
	private void initializeWidget() {

		chkLocalVendor = new CheckBox();
		chkLocalVendor.setValue(false);
		chkLocalVendor.addClickHandler(this);
		chkHOVendor = new CheckBox();
		chkHOVendor.setValue(false);
		chkHOVendor.addClickHandler(this);
		
		btnNewVendor = new Button("New Vendor");
		btnNewVendor.addClickHandler(this);
		
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new Vendor());
		Filter filter = new Filter();
		filter.setQuerryString("vendorStatus");
		filter.setBooleanvalue(true);
		querry.getFilters().add(filter);
		vendorComp = new PersonInfoComposite(querry, false);

		
	}
	
	@Override
	public void createScreen() {
	
		initializeWidget();
			
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder();
		FormField fdbVendorInfo=fbuilder.setlabel("Vendor Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Local Vendor",  chkLocalVendor);
		FormField fchkpurchaseLocally = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("HO Vendor",  chkHOVendor);
		FormField fchkHOVendor = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnNewVendor);
		FormField fbtnNewVendor = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", vendorComp);
		FormField fvendorComp = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();
		
	
	
		FormField [][]formfield = {
				{fdbVendorInfo},
				{fchkpurchaseLocally,fchkHOVendor,fbtnNewVendor},
				{fvendorComp}
				
		};
		this.fields=formfield;	
	}

	

	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource()==newVendorInfo.getLblOk()){
			if(newVendorInfo.validate()){
				reactonOk();
			}
		}
		if(event.getSource()==newVendorInfo.getLblCancel()){
			newVendorInfo.hidePopUp();
		}
		if(event.getSource()==btnNewVendor){
			newVendorInfo.showPopUp();
		}
		if(event.getSource()==chkHOVendor){
			if(chkHOVendor.getValue()==true){
				this.chkLocalVendor.setEnabled(false);
			}
			else{
				this.chkLocalVendor.setEnabled(true);
			}
			
		}
		if(event.getSource()==chkLocalVendor){
			if(chkLocalVendor.getValue()==true){
				this.chkHOVendor.setEnabled(false);
			}
			else{
				this.chkHOVendor.setEnabled(true);
			}
		}
	}

	public CheckBox getChkLocalVendor() {
		return chkLocalVendor;
	}

	public void setChkLocalVendor(CheckBox chkLocalVendor) {
		this.chkLocalVendor = chkLocalVendor;
	}

	public CheckBox getChkHOVendor() {
		return chkHOVendor;
	}

	public void setChkHOVendor(CheckBox chkHOVendor) {
		this.chkHOVendor = chkHOVendor;
	}

	public PersonInfoComposite getVendorComp() {
		return vendorComp;
	}

	public void setVendorComp(PersonInfoComposite vendorComp) {
		this.vendorComp = vendorComp;
	}
	
	
	
	private void reactonOk() {
		
		final Vendor vendorInfo = new Vendor();
		vendorInfo.setfullName(newVendorInfo.getTbfullName().getValue().trim());
		vendorInfo.setVendorName(newVendorInfo.getTbVendorName().getValue().trim());
		vendorInfo.setEmail(newVendorInfo.getEtbEmail().getValue());
		vendorInfo.setCellNumber1(newVendorInfo.getPnbCellNo1().getValue());
		vendorInfo.setPrimaryAddress(newVendorInfo.getPrimaryAddressComposite().getValue());
		vendorInfo.setSecondaryAddress(newVendorInfo.getPrimaryAddressComposite().getValue());
		
		if(newVendorInfo.getGstNumber().getValue()!=null&&!newVendorInfo.getGstNumber().getValue().equals("")){
			ArticleType articalType=new ArticleType();
			articalType.setDocumentName("Invoice");
			articalType.setArticleTypeName("GSTIN");
			articalType.setArticleTypeValue(newVendorInfo.getGstNumber().getValue());
			articalType.setArticlePrint("Yes");
			vendorInfo.getArticleTypeDetails().add(articalType);
		}
		
		vendorprice = newVendorInfo.getDbvendorPrice().getValue();
		
		showWaitSymbol();
		genasync.save(vendorInfo, new AsyncCallback<ReturnFromServer>() {
			
			@Override
			public void onSuccess(ReturnFromServer result) {
				hideWaitSymbol();
				newVendorInfo.hidePopUp();
				setVendorInfoTOComposite(result.count,vendorInfo);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				hideWaitSymbol();
				newVendorInfo.hidePopUp();
			}
		});
		
	}

	protected void setVendorInfoTOComposite(int count, Vendor vendorInfo) {
		// TODO Auto-generated method stub
		
		PersonInfo vendorinfo = new PersonInfo();
		vendorinfo.setCount(count);
		vendorinfo.setFullName(vendorInfo.getVendorName());
		vendorinfo.setCellNumber(vendorInfo.getCellNumber1());
		vendorinfo.setPocName(vendorInfo.getfullName());
		
		this.vendorComp.setPersonInfoHashMap(vendorinfo);
		this.vendorComp.setValue(vendorinfo);
		PersonInfoComposite.globalVendorArray.add(vendorinfo);
		
//	    LeadPresenterSearchProxy search = (LeadPresenterSearchProxy) form.getSearchpopupscreen();
//	    search.personInfo.setPersonInfoHashMap(info);
		
	}
	
}
