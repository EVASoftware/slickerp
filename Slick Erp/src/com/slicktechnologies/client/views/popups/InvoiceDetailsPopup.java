package com.slicktechnologies.client.views.popups;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;

/**
 * Date :- 28-03-2019 
 * @author Vijay Chougule
 * Des :- for NBHC CCPM when user click on billing document submit button (Self approval) then invoice details need 
 * to capture to create invoice and sent for request for approval.
 */
public class InvoiceDetailsPopup extends PopupScreen implements ChangeHandler{

	ObjectListBox<Employee> olbApproverName;
	ObjectListBox<Config> olbinvoicegroup;
	ObjectListBox<ConfigCategory> olbInvoiceCategory;
	ObjectListBox<Type> olbInvoiceConfigType;
	
	public InvoiceDetailsPopup(){
		super();
		createGui();
	}

	
	private void initializewidgets() {
		
		olbApproverName=new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(olbApproverName,"Invoice Details");
		olbInvoiceCategory=new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbInvoiceCategory, Screen.INVOICECATEGORY);
		olbInvoiceConfigType=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbInvoiceConfigType, Screen.INVOICECONFIGTYPE);
		olbInvoiceCategory.addChangeHandler(this);
		olbinvoicegroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbinvoicegroup, Screen.INVOICEGROUP);
		
	}
	
	@Override
	public void createScreen() {

		initializewidgets();
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingInvoiceInformation=fbuilder.setlabel("Invoice Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
	
		fbuilder = new FormFieldBuilder("* Approver Name",olbApproverName);
		FormField folbApproverName= fbuilder.setMandatory(true).setMandatoryMsg("Approver Name is Mandatory!").setRowSpan(0).setColSpan(0).build();

		FormField folbinvoicegroup;
		FormField folbInvoiceCategory;
		FormField folbInvoiceConfigType;
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","MakeInvoiceGroupMandatory"))
		{
			fbuilder = new FormFieldBuilder("* Invoice Group",olbinvoicegroup);
			folbinvoicegroup=fbuilder.setMandatory(true).setMandatoryMsg("Invoice Group is Mandatory !").setRowSpan(0).setColSpan(0).build();

		
		}
		else
		{
			fbuilder = new FormFieldBuilder("Invoice Group",olbinvoicegroup);
			folbinvoicegroup =fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","MakeInvoiceCategoryMandatory"))
		{
			fbuilder = new FormFieldBuilder("* Invoice Category",olbInvoiceCategory);
			folbInvoiceCategory=fbuilder.setMandatory(true).setMandatoryMsg("Invoice Category is Mandatory !").setRowSpan(0).setColSpan(0).build();
		}
		else
		{
			fbuilder = new FormFieldBuilder("Invoice Category",olbInvoiceCategory);
			folbInvoiceCategory =fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","MakeInvoiceTypeMandatory"))
		{
			fbuilder = new FormFieldBuilder("* Invoice Type",olbInvoiceConfigType);
			folbInvoiceConfigType=fbuilder.setMandatory(true).setMandatoryMsg("Invoice Type is Mandatory !").setRowSpan(0).setColSpan(0).build();
			
		}
		else
		{
			fbuilder = new FormFieldBuilder("Invoice Type",olbInvoiceConfigType);
			folbInvoiceConfigType =fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		

		FormField[][] formfield = {   
				{fgroupingInvoiceInformation},
				{folbApproverName,folbinvoicegroup,folbInvoiceCategory,folbInvoiceConfigType},
		};

		this.fields=formfield;

	}
	

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		
	}


	public ObjectListBox<Employee> getOlbApproverName() {
		return olbApproverName;
	}


	public ObjectListBox<Config> getOlbinvoicegroup() {
		return olbinvoicegroup;
	}


	public ObjectListBox<ConfigCategory> getOlbInvoiceCategory() {
		return olbInvoiceCategory;
	}


	public ObjectListBox<Type> getOlbInvoiceConfigType() {
		return olbInvoiceConfigType;
	}


	public void setOlbApproverName(ObjectListBox<Employee> olbApproverName) {
		this.olbApproverName = olbApproverName;
	}


	public void setOlbinvoicegroup(ObjectListBox<Config> olbinvoicegroup) {
		this.olbinvoicegroup = olbinvoicegroup;
	}


	public void setOlbInvoiceCategory(ObjectListBox<ConfigCategory> olbInvoiceCategory) {
		this.olbInvoiceCategory = olbInvoiceCategory;
	}


	public void setOlbInvoiceConfigType(ObjectListBox<Type> olbInvoiceConfigType) {
		this.olbInvoiceConfigType = olbInvoiceConfigType;
	}
	
	

}
