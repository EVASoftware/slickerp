package com.slicktechnologies.client.views.popups;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.Button;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranchTable;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class EmailPopup extends PopupScreen{
	
	public EmailPopup(){
		super();
		createGui();
		getPopup().getElement().addClassName("setWidth");
		emailTable.connectToLocal();
		this.getLblCancel().addClickHandler(this);
		btAdd.addClickHandler(this);
		
	}
	EmailTextBox tbEmailBox ;
	Button btAdd;
	EmployeeBranchTable emailTable;
	
	

	

	@Override
	public void createScreen() {
		tbEmailBox = new EmailTextBox();
		
		
		
		
		btAdd = new Button("Add");
		emailTable = new EmployeeBranchTable();
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder("Email",tbEmailBox);
		FormField ftbEmailBox= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",btAdd);
		FormField fbtAdd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", emailTable.getTable());
		FormField femailTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		FormField [][]formfield = {
				{ftbEmailBox,fbtAdd},
				{femailTable},
		};
		this.fields=formfield;
		
		
	}
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
		if(event.getSource().equals(this.getLblCancel())){
			this.hidePopUp();
		}
		
		
		
		if(event.getSource().equals(this.getBtAdd())){
			if(tbEmailBox.getValue() != null&&!tbEmailBox.getValue().equals("")){
				EmployeeBranch branch = new EmployeeBranch();
				branch.setBranchName(tbEmailBox.getValue());				
				this.emailTable.getDataprovider().getList().add(branch);
			}else{
				showDialogMessage("Please add emailId.");
			}
		}
		
		
	}
	public EmailTextBox getTbEmailBox() {
		return tbEmailBox;
	}

	public void setTbEmailBox(EmailTextBox tbEmailBox) {
		this.tbEmailBox = tbEmailBox;
	}

	public Button getBtAdd() {
		return btAdd;
	}

	public void setBtAdd(Button btAdd) {
		this.btAdd = btAdd;
	}

	public EmployeeBranchTable getEmailTable() {
		return emailTable;
	}

	public void setEmailTable(EmployeeBranchTable emailTable) {
		this.emailTable = emailTable;
	}

}
