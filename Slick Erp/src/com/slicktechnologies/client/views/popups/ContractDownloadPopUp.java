package com.slicktechnologies.client.views.popups;

import java.util.Date;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.Contract;

public class ContractDownloadPopUp extends PopupScreen implements ValueChangeHandler<Date>{

	public DateComparator endDateComparator;

	CheckBox chkActiveContracts;
	EmailTable emailTable;
	Button btnAdd;
	EmailTextBox tbEmailBox ;

	public ContractDownloadPopUp(){
		super();
		createGui();
	}
	
	@Override
	public void createScreen() {
		
	    endDateComparator= new DateComparator("endDate",new Contract());
	    
	    endDateComparator.getFromDate().addValueChangeHandler(this);
	    endDateComparator.getToDate().addValueChangeHandler(this);

	    chkActiveContracts = new CheckBox();
	    chkActiveContracts.addClickHandler(this);
	    emailTable = new EmailTable();
	    btnAdd = new Button("Add");
	    btnAdd.addClickHandler(this);
	    tbEmailBox = new EmailTextBox();
	    
		FormFieldBuilder builder;

		builder = new FormFieldBuilder();
		FormField fgroupingDateFilterInformation=builder.setlabel("Filter Combination").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

	    builder = new FormFieldBuilder("From Date (End Date)",endDateComparator.getFromDate());
		FormField fendDateComparatorfrom= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date (End Date)",endDateComparator.getToDate());
		FormField fendDateComparatorto= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Active Contracts",chkActiveContracts);
		FormField fchkActiveContracts= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		

		builder = new FormFieldBuilder("Email Id",tbEmailBox);
		FormField ftbEmailBox= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("",btnAdd);
		FormField fbtnAdd= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("",emailTable.getTable());
		FormField femailTable= builder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		
		this.fields=new FormField[][]{
				{fgroupingDateFilterInformation},
				{fendDateComparatorfrom,fendDateComparatorto, fchkActiveContracts,},
				{ftbEmailBox,fbtnAdd},
				{femailTable}
		};
	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		
		if(event.getSource().equals(btnAdd)){
			
			FlexForm flexform = new FlexForm();
			
			
			if(!tbEmailBox.getValue().equals("")){
				
				if(flexform.validateEmail(tbEmailBox.getValue())){
					this.emailTable.getDataprovider().getList().add(tbEmailBox.getValue());
					tbEmailBox.setValue("");
				}
				else{
					showDialogMessage("Invalid email id");
				}
				
			}
			else{
				showDialogMessage("Please add email id");
				tbEmailBox.setValue("");
			}
		}
		if(event.getSource().equals(chkActiveContracts)){
			if(chkActiveContracts.getValue()){
				this.endDateComparator.getFromDate().setValue(null);
				this.endDateComparator.getToDate().setValue(null);
			}
			
		}
	}

	public DateComparator getEndDateComparator() {
		return endDateComparator;
	}

	public void setEndDateComparator(DateComparator endDateComparator) {
		this.endDateComparator = endDateComparator;
	}

	public CheckBox getChkActiveContracts() {
		return chkActiveContracts;
	}

	public void setChkActiveContracts(CheckBox chkActiveContracts) {
		this.chkActiveContracts = chkActiveContracts;
	}

	public EmailTable getEmailTable() {
		return emailTable;
	}

	public void setEmailTable(EmailTable emailTable) {
		this.emailTable = emailTable;
	}

	public EmailTextBox getTbEmailBox() {
		return tbEmailBox;
	}

	public void setTbEmailBox(EmailTextBox tbEmailBox) {
		this.tbEmailBox = tbEmailBox;
	}

	@Override
	public void onValueChange(ValueChangeEvent<Date> event) {

		if(event.getSource().equals(endDateComparator.getFromDate())){
//			validateDatebox(endDateComparator.getFromDate());
			if(chkActiveContracts.getValue()){
				endDateComparator.getFromDate().setValue(null);
				showDialogMessage("Please add either from date to date or only active contracts filter");
			}
		}
		if(event.getSource().equals(endDateComparator.getToDate())){
			if(endDateComparator.getToDate().getValue()!=null){
//				validateDatebox(endDateComparator.getToDate());
				if(chkActiveContracts.getValue()){
					endDateComparator.getToDate().setValue(null);
					showDialogMessage("Please add either from date to date or only active contracts filter");
				}
			}
			
		}
	}

	
	

}
