package com.slicktechnologies.client.views.popups;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class CustomerInfoPopup extends PopupScreen {

	public PersonInfoComposite personInfo;
	 final CommonServiceAsync commonserviceasync=GWT.create(CommonService.class);

	public CustomerInfoPopup(){
		super();
		createGui();
	}
	
	@Override
	public void createScreen() {

		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		
		FormFieldBuilder builder;

		builder = new FormFieldBuilder("",personInfo);
		FormField fPersonInfo = builder.setMandatory(false).setRowSpan(0).setColSpan(3).build();

		builder = new FormFieldBuilder();
		FormField fgroupingcustomerbranch=builder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		FormField [][]formfield = {
				{fgroupingcustomerbranch},
				{fPersonInfo},
		};
		this.fields=formfield;
	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		
		if(event.getSource().equals(this.getLblOk())){
			if(personInfo.getValue()==null || personInfo.getValue().equals("")){
				showDialogMessage("Please add customer information");
			}
			else{
				Company comp = new Company();
				showWaitSymbol();
				commonserviceasync.updateCustomerBranchServiceAddressInServices(personInfo.getValue().getCount(), comp.getCompanyId(), new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						hideWaitSymbol();

					}

					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						hideWaitSymbol();
						showDialogMessage(result);
					}
				});
			}
		}
		if(event.getSource().equals(this.getLblCancel())){
			hidePopUp();
		}
	}
	
}
