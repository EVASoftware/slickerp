package com.slicktechnologies.client.views.popups;

import com.google.gwt.event.dom.client.ClickEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.client.views.inventory.materialreuestnote.SubProductTableMrn;

/**
 * Date 06-02-2019
 * @author Vijay Chougule
 * Des :- To display MMN and MRN Product details in MRN and MMN Dashboard
 * Requirements :- NBHC Inventory Management
 *
 */
public class MaterialInfoDeatilsPopup extends PopupScreen{

	SubProductTableMrn mrnProdTable;
	
	public MaterialInfoDeatilsPopup(){
		super();
		createGui();
		getPopup().getElement().addClassName("setWidth");
	}
	
	private void initializeWidget() {
		// TODO Auto-generated method stub
		mrnProdTable = new SubProductTableMrn();
	}
	
	@Override
	public void createScreen() {

		initializeWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder();
		FormField fdbMaterialDetail=fbuilder.setlabel("Material Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",mrnProdTable.getTable());
		FormField fdbprodTable= fbuilder.setRowSpan(0).setColSpan(4).build();
		
		FormField[][] formfield ={
				{fdbMaterialDetail},
				{fdbprodTable},

		};
		
		this.fields=formfield;
	}

	

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource()==this.getLblOk()){
			this.hidePopUp();
		}
		if(event.getSource()==this.getLblCancel()){
			this.hidePopUp();
		}
	}

	public SubProductTableMrn getMrnProdTable() {
		return mrnProdTable;
	}

	public void setMrnProdTable(SubProductTableMrn mrnProdTable) {
		this.mrnProdTable = mrnProdTable;
	}
	
	
}
