package com.slicktechnologies.client.views.popups;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.email.EmailDetails;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;

/**
 * @author Vijay Date :- 19-03-2021
 * Des :- Email Popup to Send Email
 */

public class NewEmailPopUp extends PopupScreen implements ChangeHandler, KeyDownHandler{

	
	public TextArea taFromEmailId,taToEmailId,taCCEmailId,taBccEmailId, taEmailSubject, taEmailBody, tahtmlformatEmailBody;
	public ObjectListBox<EmailTemplate> oblTemplateName;
	UploadComposite ucAttachment;

	public ObjectListBox<ContactPersonIdentification> olbContactlistEmailId;

	/** The default form style. */
	protected static FormStyle DEFAULT_FORM_STYLE=FormStyle.ROWFORM;
	
	Label lblbalnk1,lblbalk2,lblbalnk3,lblbalnk4;
	
	CheckBox checkboxPdfAttachment;
	
	EmailServiceAsync emailAsync = GWT.create(EmailService.class);
	public SuperModel smodel;
	
	ObjectListBox<String> oblPlaceholders;
	public String buttonLabel="";
//	Label lblblank,lblblank2,lblblank3;
	
	protected GWTCGlassPanel glassPanel;//Ashwini Patil Date:21-12-2022
	
	public NewEmailPopUp(){
		super(DEFAULT_FORM_STYLE,"SEND", "CANCEL");
		createGui();
		getPopup().getElement().addClassName("setWidth");
	}
	
	private void initializeWidgets() {
		glassPanel = new GWTCGlassPanel();
		taFromEmailId = new  TextArea();	
		taFromEmailId.setEnabled(false);
		taFromEmailId.setStyleName("textAreaWidth");
		taToEmailId = new  TextArea();	
		taToEmailId.setStyleName("textAreaWidth");
		taToEmailId.setName("InActive");
		taToEmailId.addClickHandler(this);

		taCCEmailId = new  TextArea();	
		taCCEmailId.setStyleName("textAreaWidth");
		taCCEmailId.setName("InActive");
		taCCEmailId.addClickHandler(this);

		taBccEmailId = new  TextArea();		
		taBccEmailId.setStyleName("textAreaWidth2");
		taBccEmailId.getElement().getStyle().setMarginRight(5, Unit.PX);
		taBccEmailId.setName("InActive");
		taBccEmailId.addClickHandler(this);

		taEmailSubject = new TextArea();
		taEmailSubject.getElement().getStyle().setMarginRight(10, Unit.PX);
		taEmailSubject.addClickHandler(this);
		taEmailSubject.setName("InActive");
		
		taEmailBody = new TextArea();
		taEmailBody.addClickHandler(this);
		taEmailBody.setName("InActive");
		
		oblTemplateName = new ObjectListBox<EmailTemplate>();
		AppUtility.MakeliveEmailTemplate(oblTemplateName);
		oblTemplateName.addChangeHandler(this);
		ucAttachment=new UploadComposite();
		
		lblbalnk1 = new Label();
		lblbalk2 = new Label();
		lblbalnk3 = new Label();
		lblbalnk4 = new Label();
		
		olbContactlistEmailId = new ObjectListBox<ContactPersonIdentification>();
		olbContactlistEmailId.addItem("--SELECT--");
		olbContactlistEmailId.addChangeHandler(this);

		checkboxPdfAttachment = new CheckBox();
		checkboxPdfAttachment.setValue(false);

		tahtmlformatEmailBody = new TextArea();
		
		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;

		oblPlaceholders = new ObjectListBox<String>();
		oblPlaceholders.addItem("--SELECT--");
		if(screenName.equals(Screen.CONTRACTRENEWAL)){
			oblPlaceholders.addItem("Previous Contract ID");
		}
		else{
			oblPlaceholders.addItem("ID");
		}
		oblPlaceholders.addItem("Document Date");
		if(screenName.equals(Screen.LEAD) || screenName.equals(Screen.QUOTATION) || screenName.equals(Screen.CONTRACT)
				|| screenName.equals(Screen.SALESQUOTATION) || screenName.equals(Screen.SALESORDER) || screenName.equals(Screen.CONTRACTRENEWAL)){
			oblPlaceholders.addItem("Customer Name");
		}
		else if(screenName.equals(Screen.BILLINGDETAILS)  || screenName.equals(Screen.INVOICEDETAILS) || screenName.equals(Screen.PAYMENTDETAILS) ){
			oblPlaceholders.addItem("BP Name");
		}
		oblPlaceholders.addItem("Reference Number");
		oblPlaceholders.addItem("Reference Date");
		oblPlaceholders.addItem("Digital Payment Link");
		if(screenName.equals(Screen.SERVICE) || screenName.equals(Screen.DEVICE)){
			oblPlaceholders.addItem("{Feedback_Link}");
		}

		oblPlaceholders.getElement().getStyle().setWidth(200, Unit.PX);
		
		oblPlaceholders.addChangeHandler(this);
		
	}
	
	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initializeWidgets();
		
		FormFieldBuilder fbuilder;
//		fbuilder = new FormFieldBuilder();
//		FormField femailInfo=fbuilder.setlabel("Email").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			
		fbuilder = new FormFieldBuilder("* From",  taFromEmailId);
		FormField ftaFromEmailId = fbuilder.setMandatory(true).setMandatoryMsg("From Email is mandatory").setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder("* To",  taToEmailId);
		FormField ftaToEmailId = fbuilder.setMandatory(true).setMandatoryMsg("To Email is mandatory").setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("CC",  taCCEmailId);
		FormField ftaCCEmailId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("BCC",  taBccEmailId);
		FormField ftaBccEmailId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Select Template",  oblTemplateName);
		FormField foblTemplateName = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("* Subject",  taEmailSubject);
		FormField ftaEmailSubject = fbuilder.setMandatory(true).setMandatoryMsg("Subject is mandatory").setRowSpan(0).setColSpan(2).build();
				
		fbuilder = new FormFieldBuilder("",  taEmailBody);
		FormField ftaEmailBody = fbuilder.setMandatory(true).setMandatoryMsg("Body is mandatory").setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Document Attachment",  ucAttachment);
		FormField fucAttachment = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Contact List",olbContactlistEmailId);
		FormField folbToEmailId=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("PDF Attachment",checkboxPdfAttachment);
		FormField fcheckboxPdfAttachment=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Insert Placeholders",oblPlaceholders);
		FormField foblPlaceholders=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fblankgroup3 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",lblbalnk1);
		FormField flblbalnk1 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",lblbalk2);
		FormField flblbalk2 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField [][]formfield = {
				{ftaFromEmailId,foblTemplateName},
				{ftaToEmailId,folbToEmailId},
				{ftaCCEmailId,ftaBccEmailId},
//				{flblbalnk1,flblbalnk1,flblbalk2,},
				{ftaEmailSubject,flblbalnk1,foblPlaceholders},
				{ftaEmailBody},
				{fucAttachment,fcheckboxPdfAttachment},
		};
		this.fields=formfield;
				
	}
	
	
	


	
	@Override
	public void applyStyle() {
		super.applyStyle();
		
		taFromEmailId.getElement().getStyle().setHeight(21, Unit.PX);
		taCCEmailId.getElement().getStyle().setHeight(21, Unit.PX);
		taBccEmailId.getElement().getStyle().setHeight(21, Unit.PX);

		taEmailSubject.getElement().getStyle().setHeight(21, Unit.PX);
		taEmailBody.getElement().getStyle().setHeight(200, Unit.PX);
		
	}

	@Override
	public void onChange(ChangeEvent event) {

		if(event.getSource().equals(oblTemplateName)){
			if(oblTemplateName.getSelectedIndex()!=0){
				reactonMapSelectedTemplate();
			}
			else{
				taEmailSubject.setValue("");
				taEmailBody.setValue("");
			}
		}
		
		if(event.getSource().equals(olbContactlistEmailId)){
			if(olbContactlistEmailId.getSelectedIndex()!=0){
				reactOnContactlistDropDown();
			}
		}
		
		if(event.getSource().equals(oblPlaceholders)){
			if(oblPlaceholders.getSelectedIndex()!=0){
				reactonInsertPlaceholders();
			}
		}
	}

	

	private void reactOnContactlistDropDown() {
		ContactPersonIdentification contactPerson = olbContactlistEmailId.getSelectedItem();		
		if(contactPerson!=null){
			if(taToEmailId.getName().equals("Active")){
				
				if(taToEmailId.getValue()!=null && !taToEmailId.getValue().equals("") && taToEmailId.getValue().length()>0){
					System.out.println("contactPerson.getEmail() "+contactPerson.getEmail());
					if(contactPerson.getEmail()!=null && !contactPerson.getEmail().equals("")){
						if(taToEmailId.getValue().contains(contactPerson.getEmail())){
							showDialogMessage("Can't add duplicate email id ");
						}
						else{
							String toemailId = taToEmailId.getValue() +","+ contactPerson.getEmail();
							System.out.println("toemailId "+toemailId);
							taToEmailId.setValue(toemailId);
						}
						
					}
					else{
						showDialogMessage("Email id does not exist");
					}
					
				}
				else{
					System.out.println("else block ");
					if(contactPerson.getEmail()!=null && !contactPerson.getEmail().equals("")){
						taToEmailId.setValue(contactPerson.getEmail());
					}
					else{
						showDialogMessage("Email id does not exist");
					}
				}
				
			}
			else if(taBccEmailId.getName().equals("Active")){

				
				if(taBccEmailId.getValue()!=null && !taBccEmailId.getValue().equals("") && taBccEmailId.getValue().length()>0){
					System.out.println("contactPerson.getEmail() "+contactPerson.getEmail());
					if(contactPerson.getEmail()!=null && !contactPerson.getEmail().equals("")){
						if(taBccEmailId.getValue().contains(contactPerson.getEmail())){
							showDialogMessage("Can't add duplicate email id");
						}
						else{
							String toemailId = taBccEmailId.getValue() +","+ contactPerson.getEmail();
							System.out.println("toemailId "+toemailId);
							taBccEmailId.setValue(toemailId);
						}
						
					}
					else{
						showDialogMessage("Email id does not exist");
					}
					
				}
				else{
					System.out.println("else block ");
					if(contactPerson.getEmail()!=null && !contactPerson.getEmail().equals("")){
						taBccEmailId.setValue(contactPerson.getEmail());
					}
					else{
						showDialogMessage("Email id does not exist");
					}
				}
				
			
			}
			else if(taCCEmailId.getName().equals("Active")){

				
				if(taCCEmailId.getValue()!=null && !taCCEmailId.getValue().equals("") && taCCEmailId.getValue().length()>0){
					System.out.println("contactPerson.getEmail() "+contactPerson.getEmail());
					if(contactPerson.getEmail()!=null && !contactPerson.getEmail().equals("")){
						if(taCCEmailId.getValue().contains(contactPerson.getEmail())){
							showDialogMessage("Can't add duplicate email id");
						}
						else{
							String toemailId = taCCEmailId.getValue() +","+ contactPerson.getEmail();
							System.out.println("toemailId "+toemailId);
							taCCEmailId.setValue(toemailId);
						}
						
					}
					else{
						showDialogMessage("Email id does not exist");
					}
					
				}
				else{
					System.out.println("else block ");
					if(contactPerson.getEmail()!=null && !contactPerson.getEmail().equals("")){
						taCCEmailId.setValue(contactPerson.getEmail());
					}
					else{
						showDialogMessage("Email id does not exist");
					}
				}
				
			
			}
			
			
		}
	}

	private void reactonMapSelectedTemplate() {
		
		EmailTemplate emailTemplateEntity = oblTemplateName.getSelectedItem();
		System.out.println("Email Body "+emailTemplateEntity.getEmailBody());
		System.out.println("Email Subject "+emailTemplateEntity.getSubject());

		if(emailTemplateEntity!=null){
			if(emailTemplateEntity.getSubject()!=null){
				if(emailTemplateEntity.getSubject().contains("{ID}") || emailTemplateEntity.getSubject().contains("{Document Date}")
						|| emailTemplateEntity.getSubject().contains("{Reference Number}") || emailTemplateEntity.getSubject().contains("{Reference Date}")
						|| emailTemplateEntity.getSubject().contains("{Digital Payment Link}") ){
					System.out.println("if block emailTemplateEntity.getSubject() ");

					String updatedEmailSub = getUpdatedValue(emailTemplateEntity.getSubject());
					taEmailSubject.setValue(updatedEmailSub);

				}
				else{
					System.out.println("else block emailTemplateEntity.getSubject() ");
					taEmailSubject.setValue(emailTemplateEntity.getSubject());
				}
			}
			
			if(emailTemplateEntity.getEmailBody()!=null){
//				taEmailBody.setValue(emailTemplateEntity.getEmailBody());
				

				if(emailTemplateEntity.getEmailBody().contains("{ID}") || emailTemplateEntity.getEmailBody().contains("{Document Date}")
						|| emailTemplateEntity.getEmailBody().contains("{Reference Number}") || emailTemplateEntity.getEmailBody().contains("{Reference Date}")
						|| emailTemplateEntity.getEmailBody().contains("{Digital Payment Link}") ){
					System.out.println("if block emailTemplateEntity.getEmailBody() ");

					String updatedEmailSub = getUpdatedValue(emailTemplateEntity.getEmailBody());
					taEmailBody.setValue(updatedEmailSub);

				}
				else{
					System.out.println("else block emailTemplateEntity.getEmailBody() ");
					taEmailBody.setValue(emailTemplateEntity.getEmailBody());
				}
			
				
			}
			checkboxPdfAttachment.setValue(emailTemplateEntity.isPdfattachment());
		}
	}
	
	
	


	@Override
	public void onClick(ClickEvent event) {
		
		if(event.getSource().equals(this.lblOk)){
			System.out.println("Send btn");
			if(validate()){
				glassPanel.show();
				Company comp = new Company();
				
				System.out.println("found ok");
				EmailDetails email = new EmailDetails();
				
				email.setFromEmailid(taFromEmailId.getValue().trim());
				ArrayList<String> toemailId = new ArrayList<String>();
				toemailId = getEmailIdlist(taToEmailId);
				email.setToEmailId(toemailId);
				
				ArrayList<String> ccemailId = getEmailIdlist(taCCEmailId);
				ArrayList<String> bccEmailId = getEmailIdlist(taBccEmailId);
				if(ccemailId!=null && ccemailId.size()>0){
					System.out.println("inside cc >0"+ccemailId);
					email.setCcEmailId(ccemailId);
				}
				if(bccEmailId!=null && bccEmailId.size()>0){
					email.setBccEmailId(bccEmailId);
				}
				
				if(taEmailSubject.getValue()!=null && !taEmailSubject.getValue().equals("")){
					email.setSubject(taEmailSubject.getValue());
				}
				
				boolean digitalPaymentRequestLink = false;
				if(taEmailBody.getValue()!=null && !taEmailBody.getValue().equals("")){
					email.setEmailBody(taEmailBody.getValue());
					String emailbody = taEmailBody.getValue();
					String landingPageText = "";
					if(emailbody.contains("<Start_LandingPage_Text>") && emailbody.contains("</End_LandingPage_Text>")){
						try {
							String[] startLnadingpageText = emailbody.split("<Start_LandingPage_Text>");
							String[] endlandingPageText = startLnadingpageText[1].split("</End_LandingPage_Text>");
							landingPageText = endlandingPageText[0];
							emailbody = emailbody.replace("<Start_LandingPage_Text>", "");
							emailbody = emailbody.replace("</End_LandingPage_Text>", "");
							emailbody = emailbody.replace(landingPageText, "");
							
							email.setLandingPageText(landingPageText);
						} catch (Exception e) {
							// TODO: handle exception
						}

					}
					
					String resultString = emailbody.replaceAll("[\n]", "<br>");
					String resultString1 = resultString.replaceAll("[\\s]", "&nbsp;");
					System.out.println("resultString1 "+resultString1);
					email.setEmailBody(resultString1);
					if(resultString1.contains(AppConstants.EMAILPAYMENTGATEWAYREQUEST)||resultString1.contains(AppConstants.EMAILPAYMENTGATEWAYREQUESTLINK2)){
						Console.log("Email body contains digitalpaymentlink");
						digitalPaymentRequestLink = true;
						
						String Url = com.google.gwt.core.client.GWT.getModuleBaseURL(); 
						String Appid = "";
						if(Url.contains("-dot-")){
							String [] urlarray = Url.split("-dot-");
							 Appid = urlarray[1];
						}
						else{
							String [] urlarray = Url.split("\\.");
							 Appid = urlarray[1];
						}
						email.setAppId(Appid);
						if(!Url.contains("-dot-") && Url.contains(".")) {
							StringBuilder str = new StringBuilder();
							String [] strurl = Url.split("\\.");
							str.append(strurl[0]);
							str.append("-dot-");
							str.append(strurl[1]);
							str.append(".");
							str.append(strurl[2]);
							str.append(".");
							str.append(strurl[3]);
							Console.log("str == "+str);

							Console.log("strurl[1] "+strurl[1]);
							Console.log("strurl[2] "+strurl[2]);
							Console.log("strurl[3] "+strurl[3]);
							Console.log("strurl[3] "+strurl[4]);
							Console.log("strurl[3] "+strurl[5]);

							Url = str.toString();
							
							Url = Url.replace("http", "https");
							
							Console.log("final Url "+Url);

						}
						Console.log("Url =="+Url);
						email.setAppURL(Url);
					}
				}
				
				Console.log("URL -=="+email.getAppURL());
//				if(tahtmlformatEmailBody.getValue()!=null && !tahtmlformatEmailBody.getValue().equals("")){
//					email.setEmailBody(tahtmlformatEmailBody.getValue());
//				}

//				System.out.println("Email body part "+ taEmailBody.getText());
//				System.out.println("tahtmlformatEmailBody ="+tahtmlformatEmailBody);
				
				email.setPdfAttachment(checkboxPdfAttachment.getValue());
				
				if(ucAttachment.getValue()!=null){
					email.setUploadedDocument(ucAttachment.getValue());
				}
				Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
				String EntityName =AppUtility.getEntityName(screenName);
				email.setEntityName(EntityName);
				Console.log("screenName"+screenName);
				Console.log("EntityName"+EntityName);
				
				
				if(smodel!=null){
					email.setModel(smodel);
				}
				if(EntityName.trim().equals("Service")){
					int documentid = smodel.getCount();
					email.setModel(null);
					email.setDocumentId(documentid);
				}
				if(EntityName.trim().equals("Contract")&&!buttonLabel.equals("") && buttonLabel.equals("Email Reminder")){
					email.setRenewalEmailReminder(true); // this is to manage renewal reminder email from contract screen
				}
				
				if(digitalPaymentRequestLink){
					final String pdfModuleBaseURL = com.google.gwt.core.client.GWT.getModuleBaseURL();
					email.setPdfURL(pdfModuleBaseURL);
				}
				hidePopUp();
//				showWaitSymbol();
				emailAsync.sendEmail(email, comp.getCompanyId(), new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
//						hideWaitSymbol();
						glassPanel.hide();
					}

					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
//						hideWaitSymbol();
						glassPanel.hide();
						showDialogMessage(result);						
						
					}
				});
			}
			

		}
		if(event.getSource().equals(this.lblCancel)){
			System.out.println("cancel btn");
			hidePopUp();
		}

		if(event.getSource().equals(this.taEmailBody)){
			taEmailBody.setName("Active");
		}
		else{
			if(taEmailBody!=null){
				taEmailBody.setName("InActive");
			}
		}
		
		if(event.getSource().equals(this.taEmailSubject)){
			taEmailSubject.setName("Active");
		}
		else{
			if(taEmailSubject!=null){
				taEmailSubject.setName("InActive");
			}
		}
		
		if(event.getSource().equals(this.taToEmailId)){
			taToEmailId.setName("Active");
		}
		else{
			if(taToEmailId!=null){
				taToEmailId.setName("InActive");
			}
		}
		
		
		if(event.getSource().equals(this.taCCEmailId)){
			taCCEmailId.setName("Active");
		}
		else{
			if(taCCEmailId!=null){
				taCCEmailId.setName("InActive");
			}
		}
		
		if(event.getSource().equals(this.taBccEmailId)){
			taBccEmailId.setName("Active");
		}
		else{
			if(taBccEmailId!=null){
				taBccEmailId.setName("InActive");
			}
		}
		
	}

	private ArrayList<String> getEmailIdlist(TextArea taEmailId) {
		ArrayList<String> emailIdlist = new ArrayList<String>();
		if(taEmailId.getValue()!=null && !taEmailId.getValue().trim().equals("") && taEmailId.getValue().trim().contains(",")){
			String emailidsplit[] = taEmailId.getValue().split("\\,");
			for(int i=0;i<emailidsplit.length;i++){
				if(!emailidsplit[i].equals("")){
					emailIdlist.add(emailidsplit[i].trim());
				}
			}
		}
		else{
			if(taEmailId.getValue()!=null && !taEmailId.getValue().equals("")){
				emailIdlist.add(taEmailId.getValue().trim());
			}
		}
		return emailIdlist;
	}

	@Override
	public void onKeyDown(KeyDownEvent event) {

		if(event.getSource().equals(taEmailBody)){
			
			String stremailbody = ""; 
			if(tahtmlformatEmailBody.getValue()!=null && !tahtmlformatEmailBody.getValue().equals("")){
				stremailbody = tahtmlformatEmailBody.getValue();
			}
			else{
				stremailbody = taEmailBody.getValue();
			}
			if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER){
				stremailbody += "\n";
			}
			if(event.getNativeKeyCode()==KeyCodes.KEY_TAB){
				stremailbody += "\t";

			}
			if(event.getNativeKeyCode()==KeyCodes.KEY_SPACE){
				stremailbody += "\\s";

			}
			tahtmlformatEmailBody.setValue(stremailbody);
			
			System.out.println("stremailbody "+stremailbody);
		}
		
	}

	
	private void reactonInsertPlaceholders() {
		
		DateTimeFormat dateformat = DateTimeFormat.getFormat("dd-MM-yyyy");

		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		String EntityName =AppUtility.getEntityName(screenName);
		Console.log("Entity Name =="+EntityName);


			if(taEmailBody.getName().equals("Active")){
				System.out.println("Inside Email body textarea");

				StringBuffer strvalue = new StringBuffer(taEmailBody.getValue());
				String documentId = smodel.getCount()+"";
				String documentDate ="";
				String customerName ="";
				String referenceNumber ="";
				String referenceDate ="";
				String vendorName ="";
				String previousOrderId = "";
				
				
				
				if(!EntityName.trim().equals(AppConstants.CONTRACT_RENEWAL) && oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex()).trim().equals("ID")){
					strvalue.insert(taEmailBody.getCursorPos(), documentId);
				}
				
				if(EntityName.trim().equals(AppConstants.LEAD)){
					Lead leadEntity = (Lead) smodel;
					documentDate = dateformat.format(leadEntity.getCreationDate());
					customerName = leadEntity.getPersonInfo().getFullName();
				}
				else if(EntityName.trim().equals(AppConstants.QUOTATION)){
					Quotation quotationEntity = (Quotation) smodel;
					documentDate = dateformat.format(quotationEntity.getQuotationDate());
					customerName = quotationEntity.getCinfo().getFullName();
					if(quotationEntity.getReferenceNumber()!=null)
						referenceNumber = quotationEntity.getReferenceNumber();
					if(quotationEntity.getReferenceDate()!=null){
						referenceDate = dateformat.format(quotationEntity.getReferenceDate());
					}
				}
				else if(EntityName.trim().equals(AppConstants.CONTRACT)){
					Contract contractEntity = (Contract) smodel;
					documentDate = dateformat.format(contractEntity.getContractDate());
					customerName = contractEntity.getCinfo().getFullName();
					if(contractEntity.getReferenceNumber()!=null)
						referenceNumber = contractEntity.getReferenceNumber();
					if(contractEntity.getReferenceDate()!=null){
						referenceDate = dateformat.format(contractEntity.getReferenceDate());
					}
				}
				else if(EntityName.trim().equals(AppConstants.BILLINGDOCUMENT)){
					BillingDocument billingEntity = (BillingDocument) smodel;
					documentDate = dateformat.format(billingEntity.getBillingDate());
					customerName = billingEntity.getPersonInfo().getFullName();
					if(billingEntity.getReferenceNumer()!=null)
						referenceNumber = billingEntity.getReferenceNumer();
				}
				else if(EntityName.trim().equals("Invoice")){
					Invoice invoiceEntity = (Invoice) smodel;
					documentDate = dateformat.format(invoiceEntity.getInvoiceDate());
					customerName = invoiceEntity.getPersonInfo().getFullName();
					if(invoiceEntity.getRefNumber()!=null)
						referenceNumber = invoiceEntity.getRefNumber();
					if(invoiceEntity.getReferenceDate()!=null){
						referenceDate = dateformat.format(invoiceEntity.getReferenceDate());
					}
				}
				else if(EntityName.trim().equals("CustomerPayment")){
					CustomerPayment customerPayment = (CustomerPayment) smodel;
					documentDate = dateformat.format(customerPayment.getPaymentDate());
					customerName = customerPayment.getPersonInfo().getFullName();
					if(customerPayment.getRefNumber()!=null)
						referenceNumber = customerPayment.getRefNumber();
				}
				else if(EntityName.trim().equals("ContractRenewal")){
					ContractRenewal contractRenewal = (ContractRenewal) smodel;
					documentDate = dateformat.format(contractRenewal.getContractDate());
					customerName = contractRenewal.getCustomerName();
					previousOrderId = contractRenewal.getContractId()+"";
				}
				else if(EntityName.trim().equals("SalesQuotation")){
					SalesQuotation salesQuotation = (SalesQuotation) smodel;
					documentDate = dateformat.format(salesQuotation.getQuotationDate());
					customerName = salesQuotation.getCinfo().getFullName();
				}
				else if(EntityName.trim().equals("SalesOrder")){
					SalesOrder salesOrderEntity = (SalesOrder) smodel;
					documentDate = dateformat.format(salesOrderEntity.getSalesOrderDate());
					customerName = salesOrderEntity.getCinfo().getFullName();
					if(salesOrderEntity.getReferenceNo()!=null)
						referenceNumber = salesOrderEntity.getReferenceNo();
					if(salesOrderEntity.getReferenceDate()!=null){
						referenceDate = dateformat.format(salesOrderEntity.getReferenceDate());
					}
				}
				else if(EntityName.trim().equals("PurchaseRequisition")){
					PurchaseRequisition purchaseRequisition = (PurchaseRequisition) smodel;
					documentDate = dateformat.format(purchaseRequisition.getCreationDate());
					if(purchaseRequisition.getRefOrderNO()!=null){
						referenceNumber = purchaseRequisition.getRefOrderNO();
					}
					if(purchaseRequisition.getRefOrderDate()!=null){
						referenceDate = dateformat.format(purchaseRequisition.getRefOrderDate());
					}
				}
				else if(EntityName.trim().equals("RequsestForQuotation")){
					RequsestForQuotation rfqEntity = (RequsestForQuotation) smodel;
					documentDate = dateformat.format(rfqEntity.getCreationDate());
					if(rfqEntity.getVendorinfo()!=null && rfqEntity.getVendorinfo().size()>0){
						vendorName = rfqEntity.getVendorinfo().get(0).getVendorName();
					}
					if(rfqEntity.getRefOrderNO()!=null){
						referenceNumber = rfqEntity.getRefOrderNO();
					}
				}
				else if(EntityName.trim().equals("LetterOfIntent")){
					LetterOfIntent letterOfIntentEntity = (LetterOfIntent) smodel;
					documentDate = dateformat.format(letterOfIntentEntity.getCreationDate());
					if(letterOfIntentEntity.getVendorinfo()!=null && letterOfIntentEntity.getVendorinfo().size()>0){
						vendorName = letterOfIntentEntity.getVendorinfo().get(0).getVendorName();
					}
					if(letterOfIntentEntity.getRefOrderNO()!=null){
						referenceNumber = letterOfIntentEntity.getRefOrderNO();
					}
				}
				else if(EntityName.trim().equals("PurchaseOrder")){
					PurchaseOrder poEntity = (PurchaseOrder) smodel;
					documentDate = dateformat.format(poEntity.getPODate());
					if(poEntity.getVendorDetails()!=null && poEntity.getVendorDetails().size()>0){
						for(VendorDetails vendorDetails : poEntity.getVendorDetails()){
							if(vendorDetails.getStatus()){
								vendorName = vendorDetails.getVendorName();
							}
						}
					}
					if(poEntity.getRefOrderNO()!=null){
						referenceNumber = poEntity.getRefOrderNO();
					}
				}
				else if(EntityName.trim().equals("VendorInvoice")){
					VendorInvoice vendorInvoiceEntity = (VendorInvoice) smodel;
					documentDate = dateformat.format(vendorInvoiceEntity.getInvoiceDate());
					vendorName =vendorInvoiceEntity.getPersonInfo().getFullName();
					if(vendorInvoiceEntity.getRefNumber()!=null){
						referenceNumber = vendorInvoiceEntity.getRefNumber();
					}
					if(vendorInvoiceEntity.getReferenceDate()!=null){
						referenceDate = dateformat.format(vendorInvoiceEntity.getReferenceDate());
					}
				}
				
				if(oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex()).trim().equals("Document Date")){
					strvalue.insert(taEmailBody.getCursorPos(), documentDate);
				}
				else if(oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex()).trim().equals("Customer Name")){
					strvalue.insert(taEmailBody.getCursorPos(), customerName);
				}
				else if(oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex()).trim().equals("BP Name")){
					strvalue.insert(taEmailBody.getCursorPos(), customerName);
				}
				else if(oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex()).trim().equals("Reference Number")){
					if(!referenceNumber.equals(""))
						strvalue.insert(taEmailBody.getCursorPos(), referenceNumber);
				}
				else if(oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex()).trim().equals("Reference Date")){
					if(!referenceDate.equals("")){
						strvalue.insert(taEmailBody.getCursorPos(), referenceDate);
					}
				}
				else if(oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex()).trim().equals("Previous Contract ID")){
					if(!previousOrderId.equals("")){
						strvalue.insert(taEmailBody.getCursorPos(), previousOrderId);
					}
				}
				else if(oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex()).trim().equals("Digital Payment Link")){
//					String digitalPaymentlink =  "\""+oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex())+"\"";
					String bodyIntro = "<Start_LandingPage_Text> </End_LandingPage_Text>";
					String digitalPaymentlink = bodyIntro+"\n"+ "{"+oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex())+"}";
					strvalue.insert(taEmailBody.getCursorPos(), digitalPaymentlink);
				}
				else if(oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex()).trim().equals("{Feedback_Link}")){
					strvalue.insert(taEmailBody.getCursorPos(), oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex()));
				}

//				strvalue.insert(taEmailBody.getCursorPos(), oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex()));
//				System.out.println("place holders =="+  oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex()));
				
				taEmailBody.setValue(strvalue.toString());
				
			}
			else if(taEmailSubject.getName().equals("Active")){
				System.out.println("Inside Email body textarea");

				StringBuffer strvalue = new StringBuffer(taEmailSubject.getValue());
				String documentId = smodel.getCount()+"";
				String documentDate ="";
				String customerName ="";
				String referenceNumber ="";
				String referenceDate ="";
				String vendorName ="";
				String previousOrderId = "";

				if(!EntityName.trim().equals(AppConstants.CONTRACT_RENEWAL) && oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex()).trim().equals("ID")){
					strvalue.insert(taEmailSubject.getCursorPos(), documentId);
				}
				
				if(EntityName.trim().equals(AppConstants.LEAD)){
					Lead leadEntity = (Lead) smodel;
					documentDate = dateformat.format(leadEntity.getCreationDate());
					customerName = leadEntity.getPersonInfo().getFullName();
					
				}
				else if(EntityName.trim().equals(AppConstants.QUOTATION)){

					Quotation quotationEntity = (Quotation) smodel;
					documentDate = dateformat.format(quotationEntity.getQuotationDate());
					customerName = quotationEntity.getCinfo().getFullName();
					if(quotationEntity.getReferenceNumber()!=null)
						referenceNumber = quotationEntity.getReferenceNumber();
					if(quotationEntity.getReferenceDate()!=null){
						referenceDate = dateformat.format(quotationEntity.getReferenceDate());
					}
				}
				else if(EntityName.trim().equals(AppConstants.CONTRACT)){

					Contract contractEntity = (Contract) smodel;
					documentDate = dateformat.format(contractEntity.getContractDate());
					customerName = contractEntity.getCinfo().getFullName();
					if(contractEntity.getReferenceNumber()!=null)
						referenceNumber = contractEntity.getReferenceNumber();
					if(contractEntity.getReferenceDate()!=null){
						referenceDate = dateformat.format(contractEntity.getReferenceDate());
					}
				}
				else if(EntityName.trim().equals(AppConstants.CONTRACT_RENEWAL)){

					ContractRenewal contractRenewalEntity = (ContractRenewal) smodel;
					documentDate = dateformat.format(contractRenewalEntity.getContractDate());
					customerName = contractRenewalEntity.getCustomerName();
					previousOrderId = contractRenewalEntity.getContractId()+"";

				}
				else if(EntityName.trim().equals(AppConstants.BILLINGDOCUMENT)){
					BillingDocument billingEntity = (BillingDocument) smodel;
					documentDate = dateformat.format(billingEntity.getBillingDate());
					customerName = billingEntity.getPersonInfo().getFullName();
					if(billingEntity.getReferenceNumer()!=null)
						referenceNumber = billingEntity.getReferenceNumer();
				}
				else if(EntityName.trim().equals("Invoice")){
					Invoice invoiceEntity = (Invoice) smodel;
					documentDate = dateformat.format(invoiceEntity.getInvoiceDate());
					customerName = invoiceEntity.getPersonInfo().getFullName();
					if(invoiceEntity.getRefNumber()!=null)
						referenceNumber = invoiceEntity.getRefNumber();
					if(invoiceEntity.getReferenceDate()!=null){
						referenceDate = dateformat.format(invoiceEntity.getReferenceDate());
					}
				}
				else if(EntityName.trim().equals("CustomerPayment")){
					CustomerPayment customerPayment = (CustomerPayment) smodel;
					documentDate = dateformat.format(customerPayment.getPaymentDate());
					customerName = customerPayment.getPersonInfo().getFullName();
					if(customerPayment.getRefNumber()!=null)
						referenceNumber = customerPayment.getRefNumber();
				}
				else if(EntityName.trim().equals("SalesQuotation")){
					SalesQuotation salesQuotation = (SalesQuotation) smodel;
					documentDate = dateformat.format(salesQuotation.getQuotationDate());
					customerName = salesQuotation.getCinfo().getFullName();
				}
				else if(EntityName.trim().equals("SalesOrder")){
					SalesOrder salesOrderEntity = (SalesOrder) smodel;
					documentDate = dateformat.format(salesOrderEntity.getSalesOrderDate());
					customerName = salesOrderEntity.getCinfo().getFullName();
					if(salesOrderEntity.getReferenceNo()!=null)
						referenceNumber = salesOrderEntity.getReferenceNo();
					if(salesOrderEntity.getReferenceDate()!=null){
						referenceDate = dateformat.format(salesOrderEntity.getReferenceDate());
					}
				}
				else if(EntityName.trim().equals("PurchaseRequisition")){
					PurchaseRequisition purchaseRequisition = (PurchaseRequisition) smodel;
					documentDate = dateformat.format(purchaseRequisition.getCreationDate());
					if(purchaseRequisition.getRefOrderNO()!=null){
						referenceNumber = purchaseRequisition.getRefOrderNO();
					}
					if(purchaseRequisition.getRefOrderDate()!=null){
						referenceDate = dateformat.format(purchaseRequisition.getRefOrderDate());
					}
				}
				else if(EntityName.trim().equals("RequsestForQuotation")){
					RequsestForQuotation rfqEntity = (RequsestForQuotation) smodel;
					documentDate = dateformat.format(rfqEntity.getCreationDate());
					if(rfqEntity.getVendorinfo()!=null && rfqEntity.getVendorinfo().size()>0){
						vendorName = rfqEntity.getVendorinfo().get(0).getVendorName();
					}
					if(rfqEntity.getRefOrderNO()!=null){
						referenceNumber = rfqEntity.getRefOrderNO();
					}
				}
				else if(EntityName.trim().equals("LetterOfIntent")){
					LetterOfIntent letterOfIntentEntity = (LetterOfIntent) smodel;
					documentDate = dateformat.format(letterOfIntentEntity.getCreationDate());
					if(letterOfIntentEntity.getVendorinfo()!=null && letterOfIntentEntity.getVendorinfo().size()>0){
						vendorName = letterOfIntentEntity.getVendorinfo().get(0).getVendorName();
					}
					if(letterOfIntentEntity.getRefOrderNO()!=null){
						referenceNumber = letterOfIntentEntity.getRefOrderNO();
					}
				}
				else if(EntityName.trim().equals("PurchaseOrder")){
					PurchaseOrder poEntity = (PurchaseOrder) smodel;
					documentDate = dateformat.format(poEntity.getPODate());
					if(poEntity.getVendorDetails()!=null && poEntity.getVendorDetails().size()>0){
						for(VendorDetails vendorDetails : poEntity.getVendorDetails()){
							if(vendorDetails.getStatus()){
								vendorName = vendorDetails.getVendorName();
							}
						}
					}
					if(poEntity.getRefOrderNO()!=null){
						referenceNumber = poEntity.getRefOrderNO();
					}
				}
				else if(EntityName.trim().equals("VendorInvoice")){
					VendorInvoice vendorInvoiceEntity = (VendorInvoice) smodel;
					documentDate = dateformat.format(vendorInvoiceEntity.getInvoiceDate());
					vendorName =vendorInvoiceEntity.getPersonInfo().getFullName();
					if(vendorInvoiceEntity.getRefNumber()!=null){
						referenceNumber = vendorInvoiceEntity.getRefNumber();
					}
					if(vendorInvoiceEntity.getReferenceDate()!=null){
						referenceDate = dateformat.format(vendorInvoiceEntity.getReferenceDate());
					}
				}
				
				if(oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex()).trim().equals("Document Date")){
					strvalue.insert(taEmailSubject.getCursorPos(), documentDate);
				}
				else if(oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex()).trim().equals("Customer Name")){
					strvalue.insert(taEmailSubject.getCursorPos(), customerName);
				}
				else if(oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex()).trim().equals("Reference Number")){
					if(!referenceNumber.equals(""))
						strvalue.insert(taEmailSubject.getCursorPos(), referenceNumber);
				}
				else if(oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex()).trim().equals("Reference Date")){
					if(!referenceDate.equals(""))
						strvalue.insert(taEmailSubject.getCursorPos(), referenceDate);
				}
				else if(oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex()).trim().equals("Previous Contract ID")){
					if(!previousOrderId.equals("")){
						strvalue.insert(taEmailSubject.getCursorPos(), previousOrderId);
					}
				}
				else if(oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex()).trim().equals("Digital Payment Link")){
					String digitalPaymentlink =  "{"+oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex())+"}";
					strvalue.insert(taEmailBody.getCursorPos(), digitalPaymentlink);
				}
				else if(oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex()).trim().equals("{Feedback_Link}")){
					strvalue.insert(taEmailBody.getCursorPos(), oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex()));
				}
				taEmailSubject.setValue(strvalue.toString());
				
			}
			
	}
	
	private String getUpdatedValue(String message) {
		
				DateTimeFormat dateformat = DateTimeFormat.getFormat("dd-MM-yyyy");
		
				
				Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
				String EntityName =AppUtility.getEntityName(screenName);
				Console.log("Entity Name =="+EntityName);


				System.out.println("Inside Email body textarea");

				String strvalue = new String(message);
				String documentId = smodel.getCount()+"";
				String documentDate ="";
				String customerName ="";
				String referenceNumber ="";
				String referenceDate ="";
				String vendorName ="";

				
				
				if(EntityName.trim().equals(AppConstants.LEAD)){
					Lead leadEntity = (Lead) smodel;
					documentDate = dateformat.format(leadEntity.getCreationDate());
					customerName = leadEntity.getPersonInfo().getFullName();
				}
				else if(EntityName.trim().equals(AppConstants.QUOTATION)){
					Quotation quotationEntity = (Quotation) smodel;
					documentDate = dateformat.format(quotationEntity.getQuotationDate());
					customerName = quotationEntity.getCinfo().getFullName();
					if(quotationEntity.getReferenceNumber()!=null)
						referenceNumber = quotationEntity.getReferenceNumber();
					if(quotationEntity.getReferenceDate()!=null){
						referenceDate = dateformat.format(quotationEntity.getReferenceDate());
					}
				}
				else if(EntityName.trim().equals(AppConstants.CONTRACT)){
					Contract contractEntity = (Contract) smodel;
					documentDate = dateformat.format(contractEntity.getContractDate());
					customerName = contractEntity.getCinfo().getFullName();
					if(contractEntity.getReferenceNumber()!=null)
						referenceNumber = contractEntity.getReferenceNumber();
					if(contractEntity.getReferenceDate()!=null){
						referenceDate = dateformat.format(contractEntity.getReferenceDate());
					}
				}
				else if(EntityName.trim().equals(AppConstants.BILLINGDOCUMENT)){
					BillingDocument billingEntity = (BillingDocument) smodel;
					documentDate = dateformat.format(billingEntity.getBillingDate());
					customerName = billingEntity.getPersonInfo().getFullName();
					if(billingEntity.getReferenceNumer()!=null)
						referenceNumber = billingEntity.getReferenceNumer();
				}
				else if(EntityName.trim().equals("Invoice")){
					Invoice invoiceEntity = (Invoice) smodel;
					documentDate = dateformat.format(invoiceEntity.getInvoiceDate());
					customerName = invoiceEntity.getPersonInfo().getFullName();
					if(invoiceEntity.getRefNumber()!=null)
						referenceNumber = invoiceEntity.getRefNumber();
					if(invoiceEntity.getReferenceDate()!=null){
						referenceDate = dateformat.format(invoiceEntity.getReferenceDate());
					}
				}
				else if(EntityName.trim().equals("CustomerPayment")){
					CustomerPayment customerPayment = (CustomerPayment) smodel;
					documentDate = dateformat.format(customerPayment.getPaymentDate());
					customerName = customerPayment.getPersonInfo().getFullName();
					if(customerPayment.getRefNumber()!=null)
						referenceNumber = customerPayment.getRefNumber();
				}
				else if(EntityName.trim().equals("ContractRenewal")){
					ContractRenewal contractRenewal = (ContractRenewal) smodel;
					documentDate = dateformat.format(contractRenewal.getContractDate());
					customerName = contractRenewal.getCustomerName();
				}
				else if(EntityName.trim().equals("SalesQuotation")){
					SalesQuotation salesQuotation = (SalesQuotation) smodel;
					documentDate = dateformat.format(salesQuotation.getQuotationDate());
					customerName = salesQuotation.getCinfo().getFullName();
				}
				else if(EntityName.trim().equals("SalesOrder")){
					SalesOrder salesOrderEntity = (SalesOrder) smodel;
					documentDate = dateformat.format(salesOrderEntity.getSalesOrderDate());
					customerName = salesOrderEntity.getCinfo().getFullName();
					if(salesOrderEntity.getReferenceNo()!=null)
						referenceNumber = salesOrderEntity.getReferenceNo();
					if(salesOrderEntity.getReferenceDate()!=null){
						referenceDate = dateformat.format(salesOrderEntity.getReferenceDate());
					}
				}
				else if(EntityName.trim().equals("PurchaseRequisition")){
					PurchaseRequisition purchaseRequisition = (PurchaseRequisition) smodel;
					documentDate = dateformat.format(purchaseRequisition.getCreationDate());
					if(purchaseRequisition.getRefOrderNO()!=null){
						referenceNumber = purchaseRequisition.getRefOrderNO();
					}
					if(purchaseRequisition.getRefOrderDate()!=null){
						referenceDate = dateformat.format(purchaseRequisition.getRefOrderDate());
					}
				}
				else if(EntityName.trim().equals("RequsestForQuotation")){
					RequsestForQuotation rfqEntity = (RequsestForQuotation) smodel;
					documentDate = dateformat.format(rfqEntity.getCreationDate());
					if(rfqEntity.getVendorinfo()!=null && rfqEntity.getVendorinfo().size()>0){
						vendorName = rfqEntity.getVendorinfo().get(0).getVendorName();
					}
					if(rfqEntity.getRefOrderNO()!=null){
						referenceNumber = rfqEntity.getRefOrderNO();
					}
				}
				else if(EntityName.trim().equals("LetterOfIntent")){
					LetterOfIntent letterOfIntentEntity = (LetterOfIntent) smodel;
					documentDate = dateformat.format(letterOfIntentEntity.getCreationDate());
					if(letterOfIntentEntity.getVendorinfo()!=null && letterOfIntentEntity.getVendorinfo().size()>0){
						vendorName = letterOfIntentEntity.getVendorinfo().get(0).getVendorName();
					}
					if(letterOfIntentEntity.getRefOrderNO()!=null){
						referenceNumber = letterOfIntentEntity.getRefOrderNO();
					}
				}
				else if(EntityName.trim().equals("PurchaseOrder")){
					PurchaseOrder poEntity = (PurchaseOrder) smodel;
					documentDate = dateformat.format(poEntity.getPODate());
					if(poEntity.getVendorDetails()!=null && poEntity.getVendorDetails().size()>0){
						for(VendorDetails vendorDetails : poEntity.getVendorDetails()){
							if(vendorDetails.getStatus()){
								vendorName = vendorDetails.getVendorName();
							}
						}
					}
					if(poEntity.getRefOrderNO()!=null){
						referenceNumber = poEntity.getRefOrderNO();
					}
				}
				else if(EntityName.trim().equals("VendorInvoice")){
					VendorInvoice vendorInvoiceEntity = (VendorInvoice) smodel;
					documentDate = dateformat.format(vendorInvoiceEntity.getInvoiceDate());
					vendorName =vendorInvoiceEntity.getPersonInfo().getFullName();
					if(vendorInvoiceEntity.getRefNumber()!=null){
						referenceNumber = vendorInvoiceEntity.getRefNumber();
					}
					if(vendorInvoiceEntity.getReferenceDate()!=null){
						referenceDate = dateformat.format(vendorInvoiceEntity.getReferenceDate());
					}
				}
				
				System.out.println("before replacing string "+ strvalue);
				
				if(strvalue.contains("{ID}")){
					strvalue = strvalue.replace("{ID}", documentId);
				}
				
				if(strvalue.contains("{Document Date}")){
					strvalue = strvalue.replace("{Document Date}", documentDate);
				}
				if(strvalue.contains("{Customer Name}")){
					strvalue = strvalue.replace("{Customer Name}", customerName);

				}
				if(strvalue.contains("{Reference Number}")){
					if(!referenceNumber.equals(""))
					strvalue = strvalue.replace("{Reference Number}", referenceNumber);

				}
				if(strvalue.contains("{Reference Date}")){
					if(!referenceDate.equals("")){
						strvalue = strvalue.replace("{Reference Date}", referenceDate);
					}
				}
				System.out.println("after replacing data "+strvalue);
				
		return strvalue;
	}
}
