package com.slicktechnologies.client.views.popups;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.SmsService;
import com.slicktechnologies.client.services.SmsServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.sms.SMSDetails;

public class SMSPopUp extends PopupScreen implements ChangeHandler {

	TextArea taToNumber,tasmsTemplate;
	
	public ObjectListBox<ContactPersonIdentification> olbContactlistEmailId;
	public ObjectListBox<SmsTemplate> oblsmsTemplate;
	
	SmsServiceAsync smsService = GWT.create(SmsService.class);

	public SuperModel smodel;

	public RadioButton rbSMS,rbWhatsapp;
	
//	public ObjectListBox<String> oblInsertPlaceholders;
	
	TextArea taLandingPagetext;
	
	GenricServiceAsync genasync = GWT.create(GenricService.class);

	public SMSPopUp(){
		super(DEFAULT_FORM_STYLE,"SEND", "CANCEL");
		createGui();
//		getPopup().getElement().addClassName("setWidth");
		getPopup().getElement().addClassName("setWidthsmspopup");
		smodel = null;
	}
	
	private void initilizeWidget() {
		taToNumber = new TextArea();
		taToNumber.getElement().getStyle().setHeight(45, Unit.PX);
		taToNumber.setStyleName("textAreaWidth3");

		oblsmsTemplate = new ObjectListBox<SmsTemplate>();
		AppUtility.makeLiveSMSTemplate(oblsmsTemplate);
		oblsmsTemplate.addChangeHandler(this);
		
		tasmsTemplate = new TextArea();
		tasmsTemplate.setEnabled(false);
		tasmsTemplate.getElement().getStyle().setHeight(65, Unit.PX);
		tasmsTemplate.setStyleName("textAreaWidth3");
		
		olbContactlistEmailId = new ObjectListBox<ContactPersonIdentification>();
		olbContactlistEmailId.addChangeHandler(this);
		
//		oblInsertPlaceholders = new ObjectListBox<String>();
//		oblInsertPlaceholders.addItem("--SELECT--");
//		oblInsertPlaceholders.addItem("PDF Link");
//		oblInsertPlaceholders.addItem("Customer Name");

		taLandingPagetext = new TextArea();
		
		rbSMS = new RadioButton("SMS");
		rbSMS.addClickHandler(this);
		rbSMS.setValue(true);

		
		rbWhatsapp = new RadioButton("WhatsApp");
		rbWhatsapp.addClickHandler(this);
		rbWhatsapp.setValue(false);
		
	}

	@Override
	public void createScreen() {
		
		initilizeWidget();
		
		FormFieldBuilder fbuilder;
			
		fbuilder = new FormFieldBuilder();
		FormField femailInfo=fbuilder.setlabel("Send Message").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("* To Number",  taToNumber);
		FormField ftaToNumber = fbuilder.setMandatory(true).setMandatoryMsg("To number is mandatory").setRowSpan(0).setColSpan(2).build();
	
		fbuilder = new FormFieldBuilder("* Contact List",  olbContactlistEmailId);
		FormField fcontractNumberList = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("Template",  oblsmsTemplate);
		FormField foblsmsTemplate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("* Message",  tasmsTemplate);
		FormField ftasmsTemplate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
	
		fbuilder = new FormFieldBuilder();
		FormField fblankgroup = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Landing Page Text",taLandingPagetext);
		FormField ftaLandingPagetext = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
//		fbuilder = new FormFieldBuilder("Add Placeholders",  oblInsertPlaceholders);
//		FormField foblInsertPlaceholders = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("SMS",  rbSMS);
		FormField fcbSMS = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("WhatsApp",  rbWhatsapp);
		FormField fcbwhatsapp = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		FormField [][]formfield = {
				{femailInfo},
				{fcbSMS,fcbwhatsapp},
				{ftaToNumber,fblankgroup,fcontractNumberList},
				{ftasmsTemplate,fblankgroup,foblsmsTemplate},
				{ftaLandingPagetext}
		};
		this.fields=formfield;
		
		
	}

	@Override
	public void onChange(ChangeEvent event) {

		
		if(event.getSource().equals(olbContactlistEmailId)){
			if(olbContactlistEmailId.getSelectedIndex()!=0){
				reactOnContactlistDropDown();
			}
		}
		if(event.getSource().equals(oblsmsTemplate)){
			if(oblsmsTemplate.getSelectedIndex()==0){
				tasmsTemplate.setValue("");
			}
			else{
				reactonSMSTemplate();
			}
		}
		
	}

	private void reactonSMSTemplate() {

		SmsTemplate smsTemplateEntity = oblsmsTemplate.getSelectedItem();
		
		if(smsTemplateEntity!=null){
			tasmsTemplate.setValue(smsTemplateEntity.getMessage());
			if(smsTemplateEntity.getLandingPageText()!=null){
				taLandingPagetext.setValue(smsTemplateEntity.getLandingPageText());
			}
		}
		
	}

	private void reactOnContactlistDropDown() {
		
		ContactPersonIdentification contactPerson = olbContactlistEmailId.getSelectedItem();		
		if(contactPerson!=null){
			
			if(taToNumber.getValue()!=null && !taToNumber.getValue().equals("") && taToNumber.getValue().length()>0){
				System.out.println("contactPerson.getEmail() "+contactPerson.getCell());
				if(contactPerson.getCell()!=null && !contactPerson.getCell().equals("") && !contactPerson.getCell().equals("0")){
					if(taToNumber.getValue().contains(contactPerson.getCell()+"")){
						showDialogMessage("Can't add duplicate cell number");
					}
					else{
						String toCellNumber = taToNumber.getValue() +","+ contactPerson.getCell();
						System.out.println("toemailId "+toCellNumber);
						taToNumber.setValue(toCellNumber);
					}
					
				}
				else{
					showDialogMessage("Cell number does not exist");
				}
				
			}
			else{

				if(contactPerson.getCell()!=null && !contactPerson.getCell().equals("")){
					taToNumber.setValue(contactPerson.getCell()+"");
				}
				else{
					showDialogMessage("Cell number does not exist");
				}
			}
		}
		
	}

	
	@Override
	public void onClick(ClickEvent event) {
		
		if(event.getSource().equals(this.getLblOk())){
			
				if((rbSMS.getValue() ) || (rbWhatsapp.getValue())){
					validateAndSendSMS(oblsmsTemplate.getSelectedItem());
					
					if(!rbWhatsapp.getValue())
					tasmsTemplate.setEnabled(false);
				}
				else{
					tasmsTemplate.setEnabled(true);
//					tasmsTemplate.setValue("");
				}
		}
		
		if(event.getSource().equals(this.getLblCancel())){
			hidePopUp();
		}
		
		if(event.getSource().equals(rbSMS)){
			Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
			Console.log("screenName - "+screenName);
			AppUtility.makeLiveSmsTemplateConfig(this.oblsmsTemplate,LoginPresenter.currentModule.trim(),screenName,true,AppConstants.SMS);
			tasmsTemplate.setEnabled(false);
			rbWhatsapp.setValue(false);
		}
		if(event.getSource().equals(rbWhatsapp)){
//			Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
//			Console.log("screenName - "+screenName);
//			AppUtility.makeLiveSmsTemplateConfig(this.oblsmsTemplate,LoginPresenter.currentModule.trim(),screenName,true,AppConstants.WHATSAPP);
//			tasmsTemplate.setEnabled(true);
			rbSMS.setValue(false);
			
			validateWhatsAppConfiguration();
		}
	}

	

	private void validateAndSendSMS(SmsTemplate smsTemplate) {
		
		if(smodel==null){
			showDialogMessage("Please refresh the screen and try again!");
			return;
		}
		if(taToNumber.getValue()==null || taToNumber.getValue().equals("")){
			showDialogMessage("Please enter mobile number!");
			return;
		}
		if(tasmsTemplate.getValue()==null || tasmsTemplate.getValue().equals("")) {
			showDialogMessage("Message can not be blank!");
			return;
		}
		
		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		String entityName =AppUtility.getEntityName(screenName);
		
		String mobileNo = taToNumber.getValue();
		if(mobileNo.contains(",")){
			String[] strmobileno = mobileNo.split("\\,");
			StringBuilder sbmobileNo = new StringBuilder();
			
			Console.log("strmobileno length "+strmobileno.length);
			
			for(int i=0;i<strmobileno.length;i++){
//				sbmobileNo.append("91");
				sbmobileNo.append(strmobileno[i]);
				sbmobileNo.append(",");
			}
			mobileNo = sbmobileNo.toString();
		}
		else{
//			mobileNo = "91"+mobileNo;
			mobileNo = mobileNo;

		}
		Console.log("mobileNo =="+mobileNo);
		
		SMSDetails smsDetails = new SMSDetails();
		
		String Url = com.google.gwt.core.client.GWT.getModuleBaseURL(); 
		String Appid = "";
		if(Url.contains("-dot-")){
			String [] urlarray = Url.split("-dot-");
			 Appid = urlarray[1];
		}
		else{
			String [] urlarray = Url.split("\\.");
			 Appid = urlarray[1];
		}
		

		if(!Url.contains("-dot-") && Url.contains(".")) {
			StringBuilder str = new StringBuilder();
			String [] strurl = Url.split("\\.");
			str.append(strurl[0]);
			str.append("-dot-");
			str.append(strurl[1]);
			str.append(".");
			str.append(strurl[2]);
			str.append(".");
			str.append(strurl[3]);
			Console.log("str == "+str);

			Console.log("strurl[1] "+strurl[1]);
			Console.log("strurl[2] "+strurl[2]);
			Console.log("strurl[3] "+strurl[3]);
			Console.log("strurl[3] "+strurl[4]);
			Console.log("strurl[3] "+strurl[5]);

			Url = str.toString();
			
			Url = Url.replace("http", "https");
			
			Console.log("final Url "+Url);

		}
		Console.log("Url =="+Url);
		
		smsDetails.setAppId(Appid);
		smsDetails.setAppURL(Url);
		smsDetails.setSmsTemplate(smsTemplate);
		smsDetails.setModel(smodel);
		smsDetails.setEntityName(entityName);
		smsDetails.setMobileNumber(mobileNo);
		smsDetails.setPdfURL(Url);
		if(taLandingPagetext.getValue()!=null)
		smsDetails.setLandingPageText(taLandingPagetext.getValue());
		
		if(rbSMS.getValue()){
			smsDetails.setCommunicationChannel(AppConstants.SMS);
		}
		if(rbWhatsapp.getValue()){
			smsDetails.setCommunicationChannel(AppConstants.WHATSAPP);
		}
		smsDetails.setMessage(tasmsTemplate.getValue());
		
		showWaitSymbol();
		
		smsService.validateAndSendSMS(smsDetails, new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
				if(result.equalsIgnoreCase("True")){
					showDialogMessage("SMS sent successfully");
				}
				else{
					showDialogMessage(result);
				}
				hidePopUp();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
				hidePopUp();
			}
		});
		
	}

	public RadioButton getRbSMS() {
		return rbSMS;
	}

	public void setRbSMS(RadioButton rbSMS) {
		this.rbSMS = rbSMS;
	}

	public RadioButton getRbWhatsapp() {
		return rbWhatsapp;
	}

	public void setRbWhatsapp(RadioButton rbWhatsapp) {
		this.rbWhatsapp = rbWhatsapp;
	}

	
	private void validateWhatsAppConfiguration() {
		
		showWaitSymbol();
		Vector<Filter> filtervec=new Vector<Filter>();
		
		Company comp = new Company();
		
		final MyQuerry querry=new MyQuerry();
		Filter filter = null;
		
		filter=new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(comp.getCompanyId());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Company());
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
				
				for(SuperModel model : result) {
					Company company = (Company) model;
					if(company.getWhatsAppApiUrl()==null || company.getWhatsAppApiUrl().equals("")) {
						showDialogMessage("WhatsApp not enabled. Configure whatsapp at Accounts --> Accounts Configuration --> Company - WhatsApp Integration OR else contact your system administrator/EVA Support support@evasoftwaresolutions.com to subscribe it");
					}
					else if(company.isWhatsAppApiStatus()==false) {
						showDialogMessage("WhatsApp Api status is inactive");
					}
					else {
						Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
						Console.log("screenName - "+screenName);
						AppUtility.makeLiveSmsTemplateConfig(oblsmsTemplate,LoginPresenter.currentModule.trim(),screenName,true,AppConstants.WHATSAPP);
						tasmsTemplate.setEnabled(true);
						rbSMS.setValue(false);
					}
					break;
				}
			}
			
			@Override
			public void onFailure(Throwable arg0) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
			}
		});
		
	}
	
}
