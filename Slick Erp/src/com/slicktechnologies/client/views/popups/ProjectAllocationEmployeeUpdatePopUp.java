package com.slicktechnologies.client.views.popups;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;

public class ProjectAllocationEmployeeUpdatePopUp extends PopupScreen{

	ObjectListBox<EmployeeInfo> olbEmployee;
	ObjectListBox<CustomerBranchDetails> olbSiteLocation;
	ObjectListBox<Config> olbEmplRole;
	ObjectListBox<Shift> olbshift;

	boolean siteLocationFlag = false;
	
	public ProjectAllocationEmployeeUpdatePopUp(){
		super();
		createGui();
	}
	
	private void initializeWidget() {
		
		olbEmployee = new ObjectListBox<EmployeeInfo>();
		olbSiteLocation = new ObjectListBox<CustomerBranchDetails>();
		olbEmplRole = new ObjectListBox<Config>();
		makeliveEmpRole();
		olbshift = new ObjectListBox<Shift>();
		makeliveShift();
		siteLocationFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("ProjectAllocation", "EnableSiteLocation");

	}
	
	
	

	@Override
	public void createScreen() {
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder();
		FormField fUpdation=fbuilder.setlabel("Updation").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Site Location",olbSiteLocation);
		FormField folbSiteLocation= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Employee",olbEmployee);
		FormField folbEmployee= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Role Name",olbEmplRole);
		FormField folbEmplRole= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Shift",olbshift);
		FormField folbshift= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		if(siteLocationFlag){
			FormField[][] formfield = {  
					{fUpdation},
					{folbSiteLocation,folbEmplRole,folbEmployee,folbshift},
			};
			this.fields=formfield;
		}
		else{
			FormField[][] formfield = {  
					{fUpdation},
					{folbEmplRole,folbEmployee,folbshift},
			};
			this.fields=formfield;
		}
		
	}
	
	

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}
	
	private void makeliveShift() {
		olbshift.setListItems(LoginPresenter.globalshiftList);
	}

	private void makeliveEmpRole() {

		ArrayList<Config> designationList = new ArrayList<Config>();
		designationList = LoginPresenter.globalConfig;

		ArrayList<Config> emprolelist = new ArrayList<Config>();
		
		if (designationList.size() != 0) {
			for (int i = 0; i < designationList.size(); i++) {
				if (designationList.get(i).getType() == 13
						&& designationList.get(i).isStatus() == true) {
					if (designationList.get(i).getName() != null) {
						try {
							emprolelist.add(designationList.get(i));
						} catch (Exception e) {
							System.out.println(e);
						}
					}
				}
			}
		}
		olbEmplRole.setListItems(emprolelist);
	}
	
//	public void initializeEmployeeDropDown(ObjectListBox<EmployeeInfo> olbEmployee,
//			ArrayList<EmployeeInfo> globalEmployeeInfo, String branchName) {
//		
//		olbEmployee = new ObjectListBox<EmployeeInfo>();
//		ArrayList<EmployeeInfo> empInfolist = new ArrayList<EmployeeInfo>();
//		
//		for (EmployeeInfo employee : LoginPresenter.globalEmployeeInfo) {
//			if(branchName!=null && !branchName.equals("")) {
//				if(branchName.equals(employee.getBranch())) {
//					Date date=new Date();
//					if(employee.isLeaveAllocated()&&employee.getDateOfJoining().before(date)){
//						empInfolist.add(employee);
//					}
//
//				}
//			}
//			else {
//				Date date=new Date();
//				if(employee.isLeaveAllocated()&&employee.getDateOfJoining().before(date)){
//					empInfolist.add(employee);
//				}
//			}
//			
//		}
//		Console.log("employeeList =="+empInfolist.size());
//		/***Date 23-2-2019 added by amol for sorting ***/
//		java.util.Collections.sort(empInfolist);
//		
//		loadEmployeeListData(empInfolist, olbEmployee);
//	}

	public void loadEmployeeListData(ArrayList<EmployeeInfo> empInfolist) {

//		olbEmployee = new ObjectListBox<EmployeeInfo>();
		Console.log("Employee list size"+empInfolist.size());
		olbEmployee.clear();
		olbEmployee.setListItems(empInfolist);

		ArrayList<String> empNameList=new ArrayList<String>();
		for(int i=0;i<olbEmployee.getItemCount();i++){
			if(empNameList.contains(olbEmployee.getItemText(i))){
				Console.log("EMPLOYEE NAME REPEATED "+olbEmployee.getItemText(i));
				EmployeeInfo info=olbEmployee.getItems().get(i-1);
				olbEmployee.setItemText(i, info.getFullName()+" / "+info.getEmpCount());
				Console.log("EMPLOYEE NAME REPEATED "+olbEmployee.getItemText(i));
			}
			empNameList.add(olbEmployee.getItemText(i));
		}
		Console.log("Employee list size = "+olbEmployee.getItems().size());

	}

	
	public void loadSiteLocation(int customerId, String state) {
		final GenricServiceAsync service = GWT.create(GenricService.class); 
		
		
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(customerId);
		filtervec.add(filter);
		
		if(state!=null&&!state.equals("")){
			filter = new Filter();
//			filter.setQuerryString("billingAddress.state");
			filter.setQuerryString("contact.address.state");
			filter.setStringValue(state);
			filtervec.add(filter);
		}
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		query.setFilters(filtervec);
		showWaitSymbol();
		query.setQuerryObject(new CustomerBranchDetails());
		service.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				olbSiteLocation.clear();
				ArrayList<CustomerBranchDetails> customerbranchlist = new ArrayList<CustomerBranchDetails>();
				for (SuperModel model : result) {
					CustomerBranchDetails location=(CustomerBranchDetails) model;
					customerbranchlist.add(location);
				}
				Console.log("Site location size"+customerbranchlist.size());
				olbSiteLocation.setListItems(customerbranchlist);
				hideWaitSymbol();
			}
			
			@Override
			public void onFailure(Throwable arg0) {
				// TODO Auto-generated method stub
				hideWaitSymbol();

			}
		});
	}
	

	public ObjectListBox<EmployeeInfo> getOlbEmployee() {
		return olbEmployee;
	}

	public void setOlbEmployee(ObjectListBox<EmployeeInfo> olbEmployee) {
		this.olbEmployee = olbEmployee;
	}

	public ObjectListBox<CustomerBranchDetails> getOlbSiteLocation() {
		return olbSiteLocation;
	}

	public void setOlbSiteLocation(
			ObjectListBox<CustomerBranchDetails> olbSiteLocation) {
		this.olbSiteLocation = olbSiteLocation;
	}

	public ObjectListBox<Config> getOlbEmplRole() {
		return olbEmplRole;
	}

	public void setOlbEmplRole(ObjectListBox<Config> olbEmplRole) {
		this.olbEmplRole = olbEmplRole;
	}

	public ObjectListBox<Shift> getOlbshift() {
		return olbshift;
	}

	public void setOlbshift(ObjectListBox<Shift> olbshift) {
		this.olbshift = olbshift;
	}

	public boolean isSiteLocationFlag() {
		return siteLocationFlag;
	}

	public void setSiteLocationFlag(boolean siteLocationFlag) {
		this.siteLocationFlag = siteLocationFlag;
	}

	
	

}
