package com.slicktechnologies.client.views.popups;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.client.views.licenseinfo.LicenseDetailsTable;

public class LicenseInfoPopup extends PopupScreen{

	
	LicenseDetailsTable activelicenseinfo;
	LicenseDetailsTable expiredlicenseinfo;

	public TabPanel tabPanel;
	
	public LicenseInfoPopup(){
		super();
		createGui();
		activelicenseinfo.getTable().setWidth("750px");
		expiredlicenseinfo.getTable().setWidth("750px");
		activelicenseinfo.getTable().setHeight("400px");
		expiredlicenseinfo.getTable().setHeight("400px");

		getPopup().getElement().addClassName("setWidthlicenseinfo");
		this.getLblOk().setVisible(false);
	}
	
	private void initalizeWidget() {

		activelicenseinfo = new LicenseDetailsTable();
		expiredlicenseinfo = new LicenseDetailsTable();

		FlowPanel activelicenseinfopanel= new FlowPanel();
		FlowPanel exipredlicenseinfopanel = new FlowPanel();
		
		activelicenseinfopanel.add(activelicenseinfo.getTable());
		exipredlicenseinfopanel.add(expiredlicenseinfo.getTable());
		
		tabPanel = new TabPanel();
		tabPanel.add(activelicenseinfopanel, "Active License Details");
		tabPanel.add(exipredlicenseinfopanel, "Expired License Details");
		
		tabPanel.setSize("900", "450px");
		
		tabPanel.selectTab(0);
	}
	
	@Override
	public void createScreen() {

		initalizeWidget();
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField licenseinfo = fbuilder.setlabel("License Information").setColSpan(4).widgetType(FieldType.Grouping).build();
		
		
		fbuilder = new FormFieldBuilder(" ", tabPanel);
		FormField ftabPanel = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();
		

		FormField[][] formfield = {
				{ licenseinfo   },
				{ftabPanel } };
		this.fields = formfield;
		
	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		
		if(event.getSource().equals(this.getLblCancel())){
			hidePopUp();
		}
	}

	public LicenseDetailsTable getActivelicenseinfo() {
		return activelicenseinfo;
	}

	public void setActivelicenseinfo(LicenseDetailsTable activelicenseinfo) {
		this.activelicenseinfo = activelicenseinfo;
	}

	public LicenseDetailsTable getExpiredlicenseinfo() {
		return expiredlicenseinfo;
	}

	public void setExpiredlicenseinfo(LicenseDetailsTable expiredlicenseinfo) {
		this.expiredlicenseinfo = expiredlicenseinfo;
	}


	

}
