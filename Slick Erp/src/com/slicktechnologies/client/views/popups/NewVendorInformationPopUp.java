package com.slicktechnologies.client.views.popups;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.PhoneNumberBox;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;

/**
 * Date 12-01-2019
 * @author Vijay Chougule
 * Des :- New Vendor Information with vendor price information popup for NBHC Inventory management 
 */
public class NewVendorInformationPopUp extends PopupScreen implements ValueChangeHandler<Double> {

	TextBox tbVendorName,tbfullName,gstNumber;
	CheckBox checkBoxStatus;
	EmailTextBox etbEmail;
	PhoneNumberBox pnbCellNo1;
	AddressComposite PrimaryAddressComposite;
	DoubleBox dbvendorPrice;
	
	public NewVendorInformationPopUp(){
		super();
		createGui();
	}

	private void initializeWidget() {
		tbVendorName=new TextBox();
		tbfullName = new TextBox();
		etbEmail=new EmailTextBox();
		pnbCellNo1=new PhoneNumberBox();
		PrimaryAddressComposite=new AddressComposite();
		dbvendorPrice = new DoubleBox();
		dbvendorPrice.addValueChangeHandler(this);
		gstNumber = new TextBox();
	}
	
	@Override
	public void createScreen() {

		initializeWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder();
		FormField fdbVendorInfo=fbuilder.setlabel("Vendor Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("* Vendor Name",tbVendorName);
		FormField ftbVendorName= fbuilder.setMandatory(true).setMandatoryMsg("Vendor Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("* POC Full Name",tbfullName);
		FormField ftbfullName= fbuilder.setMandatory(true).setMandatoryMsg("Full Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Email",etbEmail);
		FormField fetbEmail= fbuilder.setMandatory(true).setMandatoryMsg("Email is Mandatory!").setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("* Cell No. 1",pnbCellNo1);
		FormField fpnbCellNo1= fbuilder.setMandatory(true).setMandatoryMsg("Cell No 1 is Mandatory!").setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPrimaryAddress=fbuilder.setlabel("Primary Address").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",PrimaryAddressComposite);
		FormField fPrimaryAddressComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("* Vendor Price",dbvendorPrice);
		FormField fdbvendorPrice= fbuilder.setMandatory(true).setMandatoryMsg("Vendor Price is Mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("GST Number",gstNumber);
		FormField fgstNumber = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField [][]formfield = {
				{fdbVendorInfo},
				{ftbVendorName,ftbfullName,fetbEmail,fpnbCellNo1},
				{fdbvendorPrice,fgstNumber},
				{fgroupingPrimaryAddress},
				{fPrimaryAddressComposite}
				
				
		};
		this.fields=formfield;	
	}

	

	@Override
	public void onClick(ClickEvent event) {
		
//		if(event.getSource()==this.getLblOk()){
//			if(this.form.validate()){
//				System.out.println("Validated");
//				reactonOk();
//			}
//		}
		if(event.getSource()==this.getLblCancel()){
			this.hidePopUp();
		}
	}

	public TextBox getTbVendorName() {
		return tbVendorName;
	}

	public void setTbVendorName(TextBox tbVendorName) {
		this.tbVendorName = tbVendorName;
	}

	public TextBox getTbfullName() {
		return tbfullName;
	}

	public void setTbfullName(TextBox tbfullName) {
		this.tbfullName = tbfullName;
	}

	public CheckBox getCheckBoxStatus() {
		return checkBoxStatus;
	}

	public void setCheckBoxStatus(CheckBox checkBoxStatus) {
		this.checkBoxStatus = checkBoxStatus;
	}

	public EmailTextBox getEtbEmail() {
		return etbEmail;
	}

	public void setEtbEmail(EmailTextBox etbEmail) {
		this.etbEmail = etbEmail;
	}

	public PhoneNumberBox getPnbCellNo1() {
		return pnbCellNo1;
	}

	public void setPnbCellNo1(PhoneNumberBox pnbCellNo1) {
		this.pnbCellNo1 = pnbCellNo1;
	}

	public AddressComposite getPrimaryAddressComposite() {
		return PrimaryAddressComposite;
	}

	public void setPrimaryAddressComposite(AddressComposite primaryAddressComposite) {
		PrimaryAddressComposite = primaryAddressComposite;
	}

	public DoubleBox getDbvendorPrice() {
		return dbvendorPrice;
	}

	public void setDbvendorPrice(DoubleBox dbvendorPrice) {
		this.dbvendorPrice = dbvendorPrice;
	}

	public TextBox getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(TextBox gstNumber) {
		this.gstNumber = gstNumber;
	}
	
 /**
  * Date 12-07-2019 by Vijay for NBHC IM New Vendor price should not be 0 & Negative
  */
	@Override
	public void onValueChange(ValueChangeEvent<Double> event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(dbvendorPrice)){
			if(dbvendorPrice.getValue()==0){
				showDialogMessage("Vendor Price should not be 0!");
				dbvendorPrice.setValue(null);
			}
			if(dbvendorPrice.getValue()<0){
				showDialogMessage("Vendor Price should not be Negavtive value!");
				dbvendorPrice.setValue(null);
			}
		}
	}

	
	

}
