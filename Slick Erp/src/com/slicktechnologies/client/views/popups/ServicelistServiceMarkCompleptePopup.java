package com.slicktechnologies.client.views.popups;

import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;

public class ServicelistServiceMarkCompleptePopup extends PopupScreen {
	
	CheckBox cbServiceDateAsServiceCompletionDate;
	DateBox dbServiceCompletionDate;
	TextArea taRemark;
	
	public ServicelistServiceMarkCompleptePopup(){
		super();
		createGui();
	}

	private void initializeWidgets() {
		cbServiceDateAsServiceCompletionDate = new CheckBox();
		cbServiceDateAsServiceCompletionDate.setValue(false);
		dbServiceCompletionDate = new DateBoxWithYearSelector();
		taRemark = new TextArea();
		
	}
	
	@Override
	public void createScreen() {
		
		initializeWidgets();
		

		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField faddressInfo = fbuilder.setlabel("Service Mark Complete").widgetType(FieldType.Grouping).build();
		
		
		fbuilder = new FormFieldBuilder("Service Date As Service Completion Date",  cbServiceDateAsServiceCompletionDate);
		FormField fcbServiceDateAsServiceCompletionDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Service Completion Date",  dbServiceCompletionDate);
		FormField fdbServiceCompletionDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Remark",  taRemark);
		FormField ftaRemark = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
				
	
		FormField [][]formfield = {
				{faddressInfo},
				{fcbServiceDateAsServiceCompletionDate},
				{fdbServiceCompletionDate},
				{ftaRemark},
		};
		this.fields=formfield;
	}

	

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	public CheckBox getCbServiceDateAsServiceCompletionDate() {
		return cbServiceDateAsServiceCompletionDate;
	}

	public void setCbServiceDateAsServiceCompletionDate(
			CheckBox cbServiceDateAsServiceCompletionDate) {
		this.cbServiceDateAsServiceCompletionDate = cbServiceDateAsServiceCompletionDate;
	}

	public DateBox getDbServiceCompletionDate() {
		return dbServiceCompletionDate;
	}

	public void setDbServiceCompletionDate(DateBox dbServiceCompletionDate) {
		this.dbServiceCompletionDate = dbServiceCompletionDate;
	}

	public TextArea getTaRemark() {
		return taRemark;
	}

	public void setTaRemark(TextArea taRemark) {
		this.taRemark = taRemark;
	}

}
