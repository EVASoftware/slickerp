package com.slicktechnologies.client.views.popups;

import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Image;

/**
 * 
 * @author Vijay Chougule
 * Des :- This popup is used for showing loading image for load customer composite
 *
 */
public class LoadingPopup extends AbsolutePanel{

	/**
	 * Date :- 27-11-2018
	 * Des :- Updated image properly
	 */
	public LoadingPopup(){
		
		Image loadingImage = new Image("/images/loadimg.gif");
		add(loadingImage,0,0);
		setSize("64px", "64px");
	}
	
}
