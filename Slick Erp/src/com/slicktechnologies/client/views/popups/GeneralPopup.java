package com.slicktechnologies.client.views.popups;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.DomEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.complain.TimeFormatBox;
import com.slicktechnologies.client.views.customerbranchdetails.servicelocation.Area;
import com.slicktechnologies.client.views.customerbranchdetails.servicelocation.AreaTable;
import com.slicktechnologies.shared.common.RoleDefinition;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchServiceLocation;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.personlayer.Person;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.client.library.mywidgets.PhoneNumberBox;

public class GeneralPopup extends PopupScreen {

	String screenName;
	int internalType;
	String categoryName;
	
	String moduleName;
	String documentName;
	String employeeRole;
	
	// For Config
	public TextBox name;
	public CheckBox status;
	public TextBox tbUnitPrintable;
	public TimeFormatBox tmbTat;
	public CheckBox gstApplicable;
	
	// For ConfigCategory
	CheckBox cbStatus;
	TextBox tbCategoryName;
	TextBox tbCatcode;
	TextArea taDescription;
	IntegerBox catId;
	
	// For Type
	TextBox tbtypeName;
	TextBox tbtypecode;
	ObjectListBox<ConfigCategory> olbCategoryName;
	TextBox tbcatcode;
	
	//For City
	TextBox tbCity;
	ObjectListBox<State>olbState;
	ObjectListBox<Config> oblClassificationOfCity;
	
	//For Locality
	TextBox tbLocality;
	ObjectListBox<City>olbCity;
	LongBox tbLocalityPin;
	
	//For Branch
	TextBox tbBranchName;
	EmailTextBox etbEmail;
	PhoneNumberBox pnbCellPhoneNo1;
	
	//For Employee
	TextBox tbfullName;
	ObjectListBox<Branch> lbBranch;
	ObjectListBox<Config> lbRole;
	
	// For customer Branch
	public PersonInfo pinfo=null;
	TextBox tbCustomerBranchName,tbPocName;
	ObjectListBox<Branch> olbbServicingBranch;

	// for Department
	TextBox tbDepartmentName;
	PhoneNumberBox pnbDepartmentCellNo;

	
	GenricServiceAsync async = GWT.create(GenricService.class);
	SuperModel model=null;
	
	/**
	 * @author Anil @since 01-10-2021
	 * Adding invoice title for non gst invoices and print bank details flag
	 * Raised by Rahul Tiwari and Nitin Sir
	 */
	TextBox tbInvoiceTitle;
	CheckBox cbPrintBankDetails;
	  
	/**
	 * @author Anil @since 06-01-2022
	 * Adding fields for Service Location
	 */
	public PersonInfo cinfo=null;
	public String customerBranchName="";
	public int customerBranchId=0;
	TextBox tbServiceLocation,tbFrequency;
	TextBox tbArea;
	Button btnAdd;
	AreaTable areaTbl;
	DoubleBox dbnoOfServices;
	
	private void initializeWidgets(String screenName, int internalType,String categoryName) {
		status = new CheckBox();
		status.setValue(true);
		
		cbStatus=new CheckBox();
		cbStatus.setValue(true);
		taDescription=new TextArea();
		
		if(AppConstants.CONFIG.equals(screenName)){
			// For Config
			name = new TextBox();
			tbUnitPrintable = new TextBox();
			tmbTat=new TimeFormatBox();
			gstApplicable = new CheckBox();
			gstApplicable.setValue(false);
			
			tbInvoiceTitle=new TextBox();
			cbPrintBankDetails=new CheckBox();
			cbPrintBankDetails.setValue(false);
		
		}
		
		if(AppConstants.CONFIGCATEGORY.equals(screenName)){
			// For ConfigCategory
			tbCategoryName=new TextBox();
			tbCatcode=new TextBox();
			tbCatcode.setEnabled(false);
			taDescription=new TextArea();
			catId=new IntegerBox();
		}
		
		
		if(AppConstants.TYPE.equals(screenName)){
			// For Type
			tbtypeName=new TextBox();
			
			tbtypecode=new TextBox();
			tbtypecode.setEnabled(false);
			
			tbcatcode=new TextBox();
			olbCategoryName=new ObjectListBox<ConfigCategory>();
			olbCategoryName.addChangeHandler(new ChangeHandler() {
				@Override
				public void onChange(ChangeEvent event) {
					if(olbCategoryName.getSelectedIndex()!=0){
						ConfigCategory catEntity=olbCategoryName.getSelectedItem();
						tbcatcode.setValue(catEntity.getCategoryCode().trim());
					}
					if(olbCategoryName.getSelectedIndex()==0){
						tbcatcode.setValue(null);
					}
				}
			});
	
		    olbCategoryName.setDropDown(AppConstants.CONFIGCATEGORY, internalType, "Remove_Add_New"+"$"+categoryName);
		    if(categoryName!=null){
		    	olbCategoryName.setValue(categoryName);
		    }
	    
		}
	    
		if(AppConstants.CITY.equals(screenName)){
		    //For City
			tmbTat=new TimeFormatBox();
		    tbCity=new TextBox();
			olbState=new ObjectListBox<State>();
			status=new CheckBox();
			State.makeOjbectListBoxLive(olbState);
			
			oblClassificationOfCity=new ObjectListBox<Config>();
		    oblClassificationOfCity.setDropDown(AppConstants.CONFIG, 117,"Remove_Add_New");
			oblClassificationOfCity.addChangeHandler(new ChangeHandler() {
				
				@Override
				public void onChange(ChangeEvent event) {
					if(oblClassificationOfCity.getSelectedIndex()!=0){
						Config tier=oblClassificationOfCity.getSelectedItem();
						if(tier.getDescription()!=null){
							tmbTat.setValue(tier.getDescription());
						}
					}else{
						tmbTat.setValue(null);
					}
				}
			});
		}
		
		if(AppConstants.LOCALITY.equals(screenName)){
			//For Locality
			tbLocality=new TextBox();
			olbCity=new ObjectListBox<City>();
			status=new CheckBox();
			olbCity.setDropDown(AppConstants.CITY, internalType, "Remove_Add_New");
			tbLocalityPin=new LongBox();
		}
		
		if(AppConstants.EMPLOYEE.equals(screenName)||AppConstants.BRANCH.equals(screenName)){
			//For Branch
			tbBranchName=new TextBox();
			pnbCellPhoneNo1=new PhoneNumberBox();
			etbEmail=new EmailTextBox();
		}
		
		if(AppConstants.EMPLOYEE.equals(screenName)){
			//For Employee
			lbBranch=new ObjectListBox<Branch>();
			lbRole=new ObjectListBox<Config>();
			lbBranch.setDropDown(AppConstants.BRANCH, internalType, "Remove_Add_New");
			lbRole.setDropDown(AppConstants.CONFIG, 12, "Remove_Add_New");
			tbfullName=new TextBox();
		}
		
		if("CustomerBranchServiceLocation".equals(screenName)){
			tbServiceLocation=new TextBox();
			tbFrequency = new TextBox();
			dbnoOfServices = new DoubleBox();
			tbArea=new TextBox();
			areaTbl=new AreaTable();
			btnAdd=new Button("Add");
			btnAdd.addClickHandler(this);
		}
		
		if("CustomerBranchDetails".equals(screenName)){
//			pinfo = new PersonInfo();
			tbCustomerBranchName = new TextBox();
			olbbServicingBranch = new ObjectListBox<Branch>();
			AppUtility.makeBranchListBoxLive(olbbServicingBranch);
			tbPocName = new TextBox();

		}
		
		if("Department".equals(screenName)){
			tbDepartmentName = new TextBox();
			pnbDepartmentCellNo = new PhoneNumberBox();
		}
	}
	
	public GeneralPopup(String screenName,int internalType) {
		super(screenName,internalType);
		this.screenName=screenName;
		this.internalType=internalType;
		Console.log(screenName+" "+internalType);
		createGui();
		
		if(areaTbl!=null){
			areaTbl.connectToLocal();
		}
	}
	
	public GeneralPopup(String screenName,int internalType,String categoryName) {
		super(screenName,internalType,categoryName);
		this.screenName=screenName;
		this.internalType=internalType;
		this.categoryName=categoryName;
		Console.log(screenName+" "+internalType+" "+categoryName);
		createGui();
		
		if(areaTbl!=null){
			areaTbl.connectToLocal();
		}
	}
	
	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource().equals(getLblCancel())){
			hidePopUp();
		}
		
		if(event.getSource().equals(btnAdd)){
			if(tbArea.getValue().equals("")){
				showDialogMessage("Please enter area to add!");
				return;
			}
			if(areaTbl.getValue()!=null){
				for(Area obj:areaTbl.getValue()){
					if(obj.getArea().equals(tbArea.getValue())){
						showDialogMessage(obj.getArea() +" is already added.");
						return;
					}
				}
			}
			
			Area obj=new Area();
			obj.setArea(tbArea.getValue());
			
			if(dbnoOfServices.getValue()!=null)
				obj.setPlannedservices(dbnoOfServices.getValue());
			if(tbFrequency.getValue()!=null)
				obj.setFrequency(tbFrequency.getValue());
			
			areaTbl.getDataprovider().getList().add(obj);
			areaTbl.getTable().redraw();
			tbArea.setValue("");
		}
	}

	@Override
	public void createScreen() {
		
	}
	@Override
	public void createScreen(String screenName,int internalType ) {
		Console.log("createScreen");
		
		initializeWidgets(screenName,internalType,null);
		
		if(screenName.equals(AppConstants.CONFIG)){
			
			
			
			FormFieldBuilder fbuilder;
			fbuilder = new FormFieldBuilder();
			
			fbuilder = new FormFieldBuilder("* Name", name);
			FormField fieldname = fbuilder.setMandatory(true).setMandatoryMsg("Name is mandatory!").build();

			fbuilder = new FormFieldBuilder("Status", status);
			FormField fieldstatus = fbuilder.build();

			
			fbuilder = new FormFieldBuilder("Printable Name", tbUnitPrintable);
			FormField fieldDesciption = fbuilder.setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Invoice Title", tbInvoiceTitle);
			FormField ftbInvoiceTitle = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Print bank details on invoice", cbPrintBankDetails);
			FormField fcbPrintBankDetails = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

			if(internalType == 100 || internalType == 47){
				this.fields = new FormField[][] { { fieldname },{fieldDesciption} };
			}else if(internalType == 117){
				fbuilder = new FormFieldBuilder("TAT", tmbTat.getTimePanel());
				fieldDesciption = fbuilder.setColSpan(4).build();
				this.fields = new FormField[][] { { fieldname },{fieldDesciption} };
			}
			else if(internalType == 91){
				fbuilder = new FormFieldBuilder("GST Applicable",gstApplicable);
				FormField fieldGSTApplicable = fbuilder.setColSpan(0).build();
				this.fields = new FormField[][] { { fieldname,  fieldGSTApplicable,ftbInvoiceTitle,fcbPrintBankDetails } };
			}
			else{
				this.fields = new FormField[][] { { fieldname }};
			}

			status.setValue(true);
			name.getElement().setId("configname");
			
		}else if(screenName.equals(AppConstants.CONFIGCATEGORY)){
			FormFieldBuilder fbuilder;
			fbuilder = new FormFieldBuilder("* Category Name",tbCategoryName);

			FormField ftbCategoryName= fbuilder.setMandatory(true).setMandatoryMsg("Category Name is mandatory!").setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("* Category Code",tbCatcode);
			FormField ftbCategoryCode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Status",cbStatus);
			FormField fcbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Description",taDescription);
			FormField ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			if(internalType == 25 ){
				FormField[][] formfield = {{ftbCategoryName},
											{ftaDescription},};
				this.fields=formfield;	
			}else{
				FormField[][] formfield = {{ftbCategoryName},
				};
				this.fields=formfield;	
			}
			
			
		}else if(screenName.equals(AppConstants.TYPE)){
			FormFieldBuilder fbuilder;
			fbuilder = new FormFieldBuilder("* Type Name",tbtypeName);
			FormField ftbCategoryName= fbuilder.setMandatory(true).setMandatoryMsg("Type Name is mandatory!").setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("* Type Code",tbtypecode);
			FormField ftbCategoryCode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Status",cbStatus);
			FormField fcbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Description",taDescription);
			FormField ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Category Code",tbcatcode);
			FormField ftbcatcode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("* Category",olbCategoryName);
			FormField folbcategory= fbuilder.setMandatory(true).setMandatoryMsg("Category Name is mandatory!").setRowSpan(0).setColSpan(0).build();

			FormField[][] formfield = {{folbcategory,ftbCategoryName},
			};

			this.fields=formfield;		
		}else if(screenName.equals(AppConstants.CITY)){
			FormFieldBuilder fbuilder;
			fbuilder = new FormFieldBuilder("* City Name",tbCity);

			FormField ftbCategoryName= fbuilder.setMandatory(true).setMandatoryMsg("City  is mandatory!").setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("* State",olbState);
			FormField ftbCategoryCode= fbuilder.setMandatory(true).setMandatoryMsg("State is mandatory!").setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Status",status);
			FormField fcbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Classification Of City",oblClassificationOfCity);
			FormField foblClassificationOfCity= fbuilder.setMandatory(false).setMandatoryMsg("Currency is Mandatory!").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("TAT",tmbTat.getTimePanel());
			FormField ftmbTat= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

			FormField[][] formfield = {{ftbCategoryCode,ftbCategoryName,foblClassificationOfCity,ftmbTat},};
			this.fields=formfield;	
		}else if(screenName.equals(AppConstants.LOCALITY)){
			FormFieldBuilder fbuilder;
			fbuilder = new FormFieldBuilder("* Locality Name",tbLocality);
			FormField ftbCategoryName= fbuilder.setMandatory(true).setMandatoryMsg("Locality  is mandatory!").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("* City",olbCity);
			FormField ftbCategoryCode= fbuilder.setMandatory(true).setMandatoryMsg("City is mandatory!").setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Status",status);
			FormField fcbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Pincode",tbLocalityPin);
			FormField ftbLocalityPin= fbuilder.setMandatory(false).setMandatoryMsg("Pincode is mandatory!").setRowSpan(0).setColSpan(0).build();

			FormField[][] formfield = {{ftbCategoryCode,ftbCategoryName,ftbLocalityPin},};
			this.fields=formfield;	
		}else if(screenName.equals(AppConstants.BRANCH)){
			FormFieldBuilder fbuilder;
			fbuilder = new FormFieldBuilder("* Branch Name",tbBranchName);
			FormField ftbBranchName= fbuilder.setMandatory(true).setMandatoryMsg("Branch Name is mandatory!").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("* Cell No.",pnbCellPhoneNo1);
			FormField fpnbCellPhoneNo1= fbuilder.setMandatory(true).setMandatoryMsg("Cell No. is mandatory!").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("* Email",etbEmail);
			FormField fetbEmail= fbuilder.setMandatory(true).setMandatoryMsg("Email is mandatory!").setRowSpan(0).setColSpan(0).build();
			
			FormField[][] formfield = {{ftbBranchName,fpnbCellPhoneNo1,fetbEmail},};
			this.fields=formfield;
		}else if(screenName.equals(AppConstants.EMPLOYEE)){
			FormFieldBuilder fbuilder;
			fbuilder = new FormFieldBuilder("* Employee Name",tbfullName);
			FormField ftbBranchName= fbuilder.setMandatory(true).setMandatoryMsg("Employee Name is mandatory!").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("* Branch",lbBranch);
			FormField flbBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is mandatory!").setRowSpan(0).setColSpan(0).build();
		
			fbuilder = new FormFieldBuilder("* Role",lbRole);
			FormField flbRole= fbuilder.setMandatory(true).setMandatoryMsg("Role is mandatory!").setRowSpan(0).setColSpan(0).build();
		
			fbuilder = new FormFieldBuilder("* Cell No.",pnbCellPhoneNo1);
			FormField fpnbCellPhoneNo1= fbuilder.setMandatory(true).setMandatoryMsg("Cell No. is mandatory!").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Email",etbEmail);
			FormField fetbEmail= fbuilder.setMandatory(false).setMandatoryMsg("Email is mandatory!").setRowSpan(0).setColSpan(0).build();
			
			FormField[][] formfield = {{ftbBranchName,flbBranch,flbRole},{fpnbCellPhoneNo1,fetbEmail},};
			this.fields=formfield;
		}else if("CustomerBranchServiceLocation".equals(screenName)){
			
			FormFieldBuilder fbuilder;
			fbuilder = new FormFieldBuilder("* Service Location",tbServiceLocation);
			FormField ftbServiceLocation= fbuilder.setMandatory(true).setMandatoryMsg("Service Location is mandatory!").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Area",tbArea);
			FormField ftbArea= fbuilder.setMandatory(false).setMandatoryMsg("Cell No. is mandatory!").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("",btnAdd);
			FormField fbtnAdd= fbuilder.setMandatory(false).setMandatoryMsg("Email is mandatory!").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("",areaTbl.getTable());
			FormField fareaTbl= fbuilder.setMandatory(false).setMandatoryMsg("Email is mandatory!").setRowSpan(0).setColSpan(3).build();
			
			fbuilder = new FormFieldBuilder("Planned Service",dbnoOfServices);
			FormField fdbnoOfServices= fbuilder.setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Frequency",tbFrequency);
			FormField ftbFrequency= fbuilder.setRowSpan(0).setColSpan(0).build();
		
			
			FormField[][] formfield = {{ftbServiceLocation,ftbArea,fdbnoOfServices,ftbFrequency,fbtnAdd},{fareaTbl}};
			this.fields=formfield;
			
		}
		
		else if("CustomerBranchDetails".equals(screenName)){
			
			FormFieldBuilder fbuilder;
			fbuilder = new FormFieldBuilder("* Branch Name",tbCustomerBranchName);
			FormField ftbCustomerBrnachName= fbuilder.setMandatory(true).setMandatoryMsg("Branch Name is mandatory!").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Servicing Branch",olbbServicingBranch);
			FormField folbbServicingBranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Point Of Contact Name",tbPocName);
			FormField ftbPocName= fbuilder.setMandatory(false).setMandatoryMsg("Email is mandatory!").setRowSpan(0).setColSpan(0).build();
			
			
			FormField[][] formfield = {{ftbCustomerBrnachName,folbbServicingBranch,ftbPocName}};
			this.fields=formfield;
			
		}
		
		else if("Department".equals(screenName)){
			
			FormFieldBuilder fbuilder;
			fbuilder = new FormFieldBuilder("* Department Name",tbDepartmentName);
			FormField ftbDepartmentName= fbuilder.setMandatory(true).setMandatoryMsg("Department Name is mandatory!").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Cell Phone No 1",pnbDepartmentCellNo);
			FormField fpnbCellPhoneNo1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			
			FormField[][] formfield = {{ftbDepartmentName,fpnbCellPhoneNo1}};
			this.fields=formfield;
			
		}
		
	}
	
	@Override
	public void createScreen(String screenName,int internalType,String categoryName ) {
		Console.log("createScreen");
		
		initializeWidgets(screenName,internalType,categoryName);
		
		if(screenName.equals(AppConstants.TYPE)){
			FormFieldBuilder fbuilder;
			fbuilder = new FormFieldBuilder("* Type Name",tbtypeName);
			FormField ftbCategoryName= fbuilder.setMandatory(true).setMandatoryMsg("Type Name is mandatory!").setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("* Type Code",tbtypecode);
			FormField ftbCategoryCode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Status",cbStatus);
			FormField fcbStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("Description",taDescription);
			FormField ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Category Code",tbcatcode);
			FormField ftbcatcode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("* Category",olbCategoryName);
			FormField folbcategory= fbuilder.setMandatory(true).setMandatoryMsg("Category Name is mandatory!").setRowSpan(0).setColSpan(0).build();

			if(categoryName!=null){
				olbCategoryName.setValue(categoryName);
				olbCategoryName.setEnabled(false);
			}
			FormField[][] formfield = {{folbcategory,ftbCategoryName},
			};

			this.fields=formfield;		
		}
	}
	
	public SuperModel getModel(){
		Console.log("getModel "+screenName);
		SuperModel model=null;
		
		if(screenName.equals(AppConstants.CONFIG)){
			Config config=new Config();
			config.setName(this.name.getText());
			config.setStatus(this.status.getValue());
			config.setDescription(this.tbUnitPrintable.getValue());
			if(internalType!=0){
				if(internalType == 117 ){
					config.setDescription(this.tmbTat.getValue());
				}
				if(internalType == 91){
					config.setGstApplicable(this.gstApplicable.getValue());
					config.setInvoiceTitle(tbInvoiceTitle.getValue());
					config.setPrintBankDetails(cbPrintBankDetails.getValue());
				}
				config.setType(internalType);
			}
			model=config;
		}else if(screenName.equals(AppConstants.CONFIGCATEGORY)){
			ConfigCategory configCat=new ConfigCategory();
			if(tbCategoryName.getValue()!=null)
				configCat.setCategoryName(tbCategoryName.getValue());
			configCat.setStatus(cbStatus.getValue());
			if(tbCatcode.getValue()!=null)
				configCat.setCategoryCode(tbCatcode.getValue());
			if(taDescription.getValue()!=null)
				configCat.setDescription(taDescription.getValue());
			configCat.setInternalType(internalType);
			
			model=configCat;
			
		}else if(screenName.equals(AppConstants.TYPE)){
			Type type=new Type();
			if(olbCategoryName.getValue(olbCategoryName.getSelectedIndex())!=null){
				type.setCategoryName(olbCategoryName.getValue(olbCategoryName.getSelectedIndex()));
			}
			if(cbStatus.getValue()!=null)
				type.setStatus(cbStatus.getValue());
			if(tbtypecode.getValue()!=null)
				type.setTypeCode(this.tbtypecode.getValue());
			if(taDescription.getValue()!=null)
				type.setDescription(taDescription.getValue());
			if(tbtypeName.getValue()!=null)
				type.setTypeName(tbtypeName.getValue());
			if(tbcatcode.getValue()!=null){
				type.setCatCode(tbcatcode.getValue());
			}
			type.setInternalType(internalType);
			
			model=type;
		}else if(screenName.equals(AppConstants.CITY)){
			City city=new City();
			if(tbCity.getValue()!=null)
				city.setCityName(tbCity.getValue());
			
				city.setStatus(status.getValue());
			if(olbState.getValue()!=null)
				city.setState(olbState.getValue());
			if(oblClassificationOfCity.getValue(oblClassificationOfCity.getSelectedIndex())!=null){
				city.setClassName(oblClassificationOfCity.getValue(oblClassificationOfCity.getSelectedIndex()));
			}else{
				city.setClassName("");
			}
			
			if(tmbTat.getValue()!=null){
				city.setTat(tmbTat.getValue());
			}else{
				city.setTat(null);
			}
			
			model=city;
		}else if(screenName.equals(AppConstants.LOCALITY)){
			Locality locality=new Locality();
			if(tbLocality.getValue()!=null)
				locality.setLocality(tbLocality.getValue());
			
			locality.setStatus(status.getValue());
			if(olbCity.getValue(olbCity.getSelectedIndex())!=null)
				locality.setCityName(olbCity.getValue(olbCity.getSelectedIndex()));
			
			if(tbLocalityPin.getValue()!=null)
				locality.setPinCode(tbLocalityPin.getValue());
			model=locality;
		}else if(screenName.equals(AppConstants.BRANCH)){
			Branch branch=new Branch();
			if(tbBranchName.getValue()!=null)
				branch.setBusinessUnitName(tbBranchName.getValue().trim());
			if(etbEmail.getValue()!=null)
				branch.setEmail(etbEmail.getValue());
			branch.setstatus(true);
			if(pnbCellPhoneNo1.getValue()!=null)
				branch.setCellNumber1(pnbCellPhoneNo1.getValue());
			model=branch;
		}else if(screenName.equals(AppConstants.EMPLOYEE)){
			Employee employee=new Employee();
			if(tbfullName.getValue()!=null)
				employee.setFullname(tbfullName.getValue().trim());
			if(etbEmail.getValue()!=null)
				employee.setEmail(etbEmail.getValue());
			employee.setStatus(true);
			if(pnbCellPhoneNo1.getValue()!=null)
				employee.setCellNumber1(pnbCellPhoneNo1.getValue());
			if(lbBranch.getValue(lbBranch.getSelectedIndex())!=null){
				employee.setBranchName(lbBranch.getValue(lbBranch.getSelectedIndex()));
			}
			if(lbRole.getValue(lbRole.getSelectedIndex())!=null){
				employee.setRoleName(lbRole.getValue(lbRole.getSelectedIndex()));
			}
			model=employee;
		}else if("CustomerBranchServiceLocation".equals(screenName)){
			Console.log("CustomerBranchServiceLocation");
			CustomerBranchServiceLocation serviceLocation=new CustomerBranchServiceLocation();
			if(tbServiceLocation.getValue()!=null){
				serviceLocation.setServiceLocation(tbServiceLocation.getValue());
			}
			if(cinfo!=null){
				serviceLocation.setCinfo(cinfo);
			}
			serviceLocation.setCustBranchId(customerBranchId);
			serviceLocation.setCustomerBranchName(customerBranchName);
			if(areaTbl.getValue()!=null){
				serviceLocation.setAreaList(areaTbl.getValueAsString());
			}
			serviceLocation.setStatus(true);
			model=serviceLocation;
		}
		
		else if("CustomerBranchDetails".equals(screenName)){
			Console.log("CustomerBranchDetails");
			CustomerBranchDetails customerBranchDetails = new CustomerBranchDetails();
			if(pinfo!=null){
				customerBranchDetails.setCinfo(pinfo);
			}
			customerBranchDetails.setBusinessUnitName(tbCustomerBranchName.getValue());
			if(olbbServicingBranch.getSelectedIndex()!=0)
			customerBranchDetails.setBranch(olbbServicingBranch.getValue());
			
			if(tbPocName.getValue()!=null){
				Person person = new Person();
				person.setFullname(tbPocName.getValue());
				customerBranchDetails.setPointOfContact(person);
			}
			customerBranchDetails.setStatus(true);
			
			model=customerBranchDetails;
		}
		
		else if("Department".equals(screenName)){
			Console.log("Department");
			Department department = new Department();
			department.setDeptName(tbDepartmentName.getValue());
			if(pnbDepartmentCellNo.getValue()!=null && pnbDepartmentCellNo.getValue()!=0)
			department.setCellNo1(pnbDepartmentCellNo.getValue());
			department.setStatus(true);
			model=department;
		}
		
		
		
		return model;
	}
	
	String alertName="";
	public <T> void validateData(final ObjectListBox<T> objectListBox){
		Console.log("validateData "+screenName);
		MyQuerry query=new MyQuerry();
		Filter temp=null;
		Vector<Filter> vecList=new Vector<Filter>();
				
		
		if(screenName.equals(AppConstants.CONFIG)){
			alertName=name.getValue();
			temp=new Filter();
			temp.setQuerryString("name");
			temp.setStringValue(name.getValue());
			vecList.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("type");
			temp.setIntValue(internalType);
			vecList.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("status");
			temp.setBooleanvalue(true);
			vecList.add(temp);
			
			query.setQuerryObject(new Config());
			query.setFilters(vecList);
		}else if(screenName.equals(AppConstants.CONFIGCATEGORY)){
			alertName=tbCategoryName.getValue();
			temp=new Filter();
			temp.setQuerryString("categoryName");
			temp.setStringValue(tbCategoryName.getValue());
			vecList.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("internalType");
			temp.setIntValue(internalType);
			vecList.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("status");
			temp.setBooleanvalue(true);
			vecList.add(temp);
			
			query.setQuerryObject(new ConfigCategory());
			query.setFilters(vecList);
			
		}else if(screenName.equals(AppConstants.TYPE)){
			alertName=tbtypeName.getValue();
			temp=new Filter();
			temp.setQuerryString("categoryName");
			temp.setStringValue(olbCategoryName.getValue());
			vecList.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("typeName");
			temp.setStringValue(tbtypeName.getValue());
			vecList.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("internalType");
			temp.setIntValue(internalType);
			vecList.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("status");
			temp.setBooleanvalue(true);
			vecList.add(temp);
			
			query.setQuerryObject(new Type());
			query.setFilters(vecList);
		}else if(screenName.equals(AppConstants.CITY)){
			alertName=tbCity.getValue();
			temp=new Filter();
			temp.setQuerryString("stateName");
			temp.setStringValue(olbState.getValue());
			vecList.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("cityName");
			temp.setStringValue(tbCity.getValue());
			vecList.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("status");
			temp.setBooleanvalue(true);
			vecList.add(temp);
			
			query.setQuerryObject(new City());
			query.setFilters(vecList);
		}else if(screenName.equals(AppConstants.LOCALITY)){
			alertName=tbLocality.getValue();
			temp=new Filter();
			temp.setQuerryString("cityName");
			temp.setStringValue(olbCity.getValue());
			vecList.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("locality");
			temp.setStringValue(tbLocality.getValue());
			vecList.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("status");
			temp.setBooleanvalue(true);
			vecList.add(temp);
			
			query.setQuerryObject(new Locality());
			query.setFilters(vecList);
		}else if(screenName.equals(AppConstants.BRANCH)){
			alertName=tbBranchName.getValue().trim();
			temp=new Filter();
			temp.setQuerryString("buisnessUnitName");
			temp.setStringValue(tbBranchName.getValue().trim());
			vecList.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("status");
			temp.setBooleanvalue(true);
			vecList.add(temp);
			
			query.setQuerryObject(new Branch());
			query.setFilters(vecList);
		}else if(screenName.equals(AppConstants.EMPLOYEE)){
			alertName=tbfullName.getValue();
			temp=new Filter();
			temp.setQuerryString("fullname");
			temp.setStringValue(tbfullName.getValue());
			vecList.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("status");
			temp.setBooleanvalue(true);
			vecList.add(temp);
			
			query.setQuerryObject(new Employee());
			query.setFilters(vecList);
		}else if("CustomerBranchServiceLocation".equals(screenName)){
			Console.log("CustomerBranchServiceLocation");
			alertName=tbServiceLocation.getValue();
			temp=new Filter();
			temp.setQuerryString("cinfo.count");
			temp.setIntValue(cinfo.getCount());
			vecList.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("custBranchId");
			temp.setIntValue(customerBranchId);
			vecList.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("serviceLocation");
			temp.setStringValue(tbServiceLocation.getValue());
			vecList.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("status");
			temp.setBooleanvalue(true);
			vecList.add(temp);
			
			query.setQuerryObject(new CustomerBranchServiceLocation());
			query.setFilters(vecList);
		}
		
		else if("CustomerBranchDetails".equals(screenName)){
			Console.log("customerbranchdetails");
			temp=new Filter();
			temp.setQuerryString("cinfo.count");
			temp.setIntValue(pinfo.getCount());
			vecList.add(temp);
			
			
			temp=new Filter();
			temp.setQuerryString("buisnessUnitName");
			temp.setStringValue(tbCustomerBranchName.getValue());
			vecList.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("status");
			temp.setBooleanvalue(true);
			vecList.add(temp);
			
			query.setQuerryObject(new CustomerBranchDetails());
			query.setFilters(vecList);
		}
		
		else if("Department".equals(screenName)){
			Console.log("Department");
			
			temp=new Filter();
			temp.setQuerryString("deptName");
			temp.setStringValue(tbDepartmentName.getValue());
			vecList.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("status");
			temp.setBooleanvalue(true);
			vecList.add(temp);
			
			query.setQuerryObject(new Department());
			query.setFilters(vecList);
		}
		
		
		async.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				Console.log("onSuccess");
				if(result!=null&&result.size()!=0){
					showDialogMessage(alertName+" already exist!");
				}else{
					saveData(objectListBox);
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				
			}
		});
	}
	
	public <T> void saveData(final ObjectListBox<T> objectListBox){
		Console.log("saveData");
		model=getModel();
		
		if(model!=null){
			async.save(model, new AsyncCallback<ReturnFromServer>() {

				@Override
				public void onFailure(Throwable caught) {
					Console.log("onFailure");
				}

				@Override
				public void onSuccess(ReturnFromServer result) {
					Console.log("onSuccess");
					model.setCount(result.count);
					model.setId(result.id);
					
					if(model instanceof Config){
						Config config=(Config) model;
						LoginPresenter.globalConfig.add(config);
						objectListBox.setDropDown(screenName, internalType, config.getName());
					}else if(model instanceof ConfigCategory){
						ConfigCategory config=(ConfigCategory) model;
						LoginPresenter.globalCategory.add(config);
						objectListBox.setDropDown(screenName, internalType, config.getCategoryName());
					}else if(model instanceof Type){
						Type config=(Type) model;
						LoginPresenter.globalType.add(config);
						objectListBox.setDropDown(screenName, internalType, config.getTypeName());
					}else if(model instanceof City){
						City config=(City) model;
						LoginPresenter.globalCity.add(config);
						objectListBox.setDropDown(screenName, internalType, config.getCityName());
					}else if(model instanceof Locality){
						Locality config=(Locality) model;
						LoginPresenter.globalLocality.add(config);
						objectListBox.setDropDown(screenName, internalType, config.getLocality());
					}else if(model instanceof Branch){
						Branch config=(Branch) model;
						LoginPresenter.globalBranch.add(config);
						
						try{
							LoginPresenter.customerScreenBranchList.add(config);
						}catch(Exception e){
							
						}
						
						if(documentName!=null&&documentName.equals("Customer")){
							objectListBox.setDropDownForCustomerScreen(screenName,internalType, config.getBusinessUnitName()); 
						}else{
							objectListBox.setDropDown(screenName, internalType, config.getBusinessUnitName());
						}
						
						
					}else if(model instanceof Employee){
						Employee config=(Employee) model;
						LoginPresenter.globalEmployee.add(config);
						
						
						if(employeeRole!=null){
							boolean loadAllSalesPersonFlag=false;
							if(employeeRole!=null&&employeeRole.equals("Sales Person")){ 
								loadAllSalesPersonFlag = true;
							}else if(employeeRole!=null&&employeeRole.equals("Requested By")){ 
								loadAllSalesPersonFlag = true;
							}
							ArrayList<String> roleList=new ArrayList<String>();
							for(RoleDefinition role:LoginPresenter.globalRoleDefinition){
								roleList.addAll(role.getRoleList());
							}
							objectListBox.setEmployeeToDropDown(roleList,loadAllSalesPersonFlag,config.getFullname());
						}else{
							objectListBox.setDropDown(screenName, internalType, config.getFullname());	
						}
						
						
					}else if(model instanceof CustomerBranchServiceLocation){
						CustomerBranchServiceLocation config=(CustomerBranchServiceLocation) model;
						List<CustomerBranchServiceLocation> list=(List<CustomerBranchServiceLocation>) objectListBox.getItems();
						LoginPresenter.globalServiceLocationList.clear();
						LoginPresenter.globalServiceLocationList.addAll(list);
						LoginPresenter.globalServiceLocationList.add(config);
						
						objectListBox.setDropDown(screenName, internalType, config.getServiceLocation());
						
//						objectListBox.addItem(config.getServiceLocation());
//						objectListBox.getItems().add((T) model);
//						objectListBox.setValue(config.getServiceLocation());
//						objectListBox.fireChangeEvevnt();
					}
//					objectListBox.setDropDown(screenName, internalType, null);
					
					else if(model instanceof CustomerBranchDetails){
						CustomerBranchDetails customerbranch=(CustomerBranchDetails) model;
						List<CustomerBranchDetails> list=(List<CustomerBranchDetails>) objectListBox.getItems();
						LoginPresenter.globalCustomerBranchList.addAll(list);
						LoginPresenter.globalCustomerBranchList.add(customerbranch);
						
						objectListBox.setDropDown(screenName, internalType, customerbranch.getBusinessUnitName());
						
					}
					
					else if(model instanceof Department){
						Department deaprtment=(Department) model;
						List<Department> list=(List<Department>) objectListBox.getItems();
						LoginPresenter.globalDepartmentList.addAll(list);
						LoginPresenter.globalDepartmentList.add(deaprtment);
						
						objectListBox.setDropDown(screenName, internalType, deaprtment.getDeptName());
						
					}
					
				}
			});
		}
	}

	@Override
	public void applyStyle() {
		content.getElement().getStyle().setWidth(100, Unit.PCT);
		content.getElement().getStyle().setBorderWidth(1, Unit.PX);
		content.getElement().getStyle().setBorderStyle(BorderStyle.SOLID);
		form.getElement().setId("form");
		horizontal.getElement().addClassName("popupcentering");
		lblOk.getElement().setId("addbutton");
		lblCancel.getElement().setId("addbutton");
		popup.setWidth("100%");
		popup.center();
//		z-index: 9999999;
		popup.getElement().getStyle().setZIndex(999999);
		taDescription.getElement().getStyle().setWidth(98, Unit.PCT);
	}

	@Override
	public void showPopUp() {
		super.showPopUp();
		
		this.status.setValue(true);
		
		cbStatus.setValue(true);
		if(screenName.equals(AppConstants.TYPE)){
			olbCategoryName.setEnabled(false);
			olbCategoryName.setValue(categoryName);
		}
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getEmployeeRole() {
		return employeeRole;
	}

	public void setEmployeeRole(String employeeRole) {
		this.employeeRole = employeeRole;
	}
	
	
	
}
