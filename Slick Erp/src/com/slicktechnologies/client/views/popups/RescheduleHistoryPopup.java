package com.slicktechnologies.client.views.popups;

import com.google.gwt.event.dom.client.ClickEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.client.views.device.ModificationHistoryTable;
import com.slicktechnologies.client.views.multipleserviceschedule.MultipleServicesScheduleTable;

public class RescheduleHistoryPopup extends PopupScreen {

	ModificationHistoryTable modificationHistoryTable;
	public RescheduleHistoryPopup(){
		super();
		modificationHistoryTable.connectToLocal();

		getPopup().setHeight("560px");
		modificationHistoryTable.getTable().setHeight("400px");
		modificationHistoryTable.getTable().setWidth("800px");
		getLblOk().setVisible(false);
		getLblCancel().addClickHandler(this);
		createGui();
	}
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource() == getLblCancel()){
			this.hidePopUp();
		}
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		initalizeWidget();
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingServicesInfo = fbuilder
				.setlabel("Communication Log")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder(" ", modificationHistoryTable.getTable());
		FormField ftabPanel = fbuilder.setMandatory(false).setRowSpan(0)
				.setColSpan(4).build();
		

		FormField[][] formfield = {
				{ fgroupingServicesInfo   },
				{ftabPanel } };
		this.fields = formfield;
		
		
	
	}
	@SuppressWarnings("unused")
	private void initalizeWidget() {
		modificationHistoryTable = new ModificationHistoryTable();
	}
	public ModificationHistoryTable getModificationHistoryTable() {
		return modificationHistoryTable;
	}
	public void setModificationHistoryTable(
			ModificationHistoryTable modificationHistoryTable) {
		this.modificationHistoryTable = modificationHistoryTable;
	}
	
}
