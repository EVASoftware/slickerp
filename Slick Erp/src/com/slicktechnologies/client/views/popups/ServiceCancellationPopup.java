package com.slicktechnologies.client.views.popups;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.TextArea;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.Config;

public class ServiceCancellationPopup extends PopupScreen {

	ObjectListBox<Config> olbCancellationRemark;
//	TextArea textArea; 
	public ServiceCancellationPopup(){
		super();
		createGui();
	}
	
	@Override
	public void createScreen() {

		olbCancellationRemark = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbCancellationRemark, Screen.CANCELLATIONREMARK);
		
//		textArea = new TextArea();
		
		FormFieldBuilder fbuilder;

		fbuilder = new FormFieldBuilder();
		FormField fCancellationInfo = fbuilder.setlabel("Cancellation/Suspend Information").widgetType(FieldType.Grouping).setColSpan(0).build();
		FormField olbCancellationRemark;
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableCancellationRemark")){
			fbuilder = new FormFieldBuilder("Reason For Cancellation/Suspend", this.olbCancellationRemark);
			olbCancellationRemark = fbuilder.setMandatory(false).setColSpan(0).setRowSpan(0).build();
//		}
//		else{
//			fbuilder = new FormFieldBuilder("Reason For Cancellation", textArea);
//			olbCancellationRemark = fbuilder.setMandatory(false).setColSpan(0).setRowSpan(0).build();
//		}
		
		
		FormField [][]formfield = {
				{fCancellationInfo},
				{olbCancellationRemark}
		};
		this.fields=formfield;
	}

	@Override
	public void onClick(ClickEvent event) {
		
	}

	public ObjectListBox<Config> getOlbCancellationRemark() {
		return olbCancellationRemark;
	}

//	public TextArea getTextArea() {
//		return textArea;
//	}

	public void setOlbCancellationRemark(ObjectListBox<Config> olbCancellationRemark) {
		this.olbCancellationRemark = olbCancellationRemark;
	}

//	public void setTextArea(TextArea textArea) {
//		this.textArea = textArea;
//	}
	
	
}
