package com.slicktechnologies.client.views.popups;

import com.google.gwt.event.dom.client.ClickEvent;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;

public class EmailIdPopup extends PopupScreen{

	EmailTextBox tbEmailBox ;
	public EmailIdPopup(){
		super();
		createGui();
		
	}
	
	
	
	
	
	
	
	
	@Override
	public void onClick(ClickEvent event) {

		if(event.getSource().equals(this.getLblCancel())){
			this.hidePopUp();
		}
				
	}

	@Override
	public void createScreen() {
		tbEmailBox = new EmailTextBox();
		FormFieldBuilder fbuilder;
		
		
		
		fbuilder = new FormFieldBuilder("Customer Email",tbEmailBox);
		FormField ftbEmailBox= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		FormField [][]formfield = {
				{ftbEmailBox},
		};
		this.fields=formfield;
		
		
	}
	public EmailTextBox getTbEmailBox() {
		return tbEmailBox;
	}

	public void setTbEmailBox(EmailTextBox tbEmailBox) {
		this.tbEmailBox = tbEmailBox;
	}
}
