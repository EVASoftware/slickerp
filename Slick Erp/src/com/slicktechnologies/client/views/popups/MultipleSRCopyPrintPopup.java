package com.slicktechnologies.client.views.popups;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.device.ModificationHistoryTable;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranchTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.MaterialConsumptionReport;
import com.smartgwt.client.widgets.form.fields.RadioGroupItem;

public class MultipleSRCopyPrintPopup extends PopupScreen {

	DateBox dbfromDate;
	DateBox dbToDate;
	IntegerBox ibServiceId;
	ObjectListBox<Branch> olbBranch;
	EmailTextBox tbEmailBox ;
	Button btAdd;
	EmployeeBranchTable emailTable;
	Contract contract;
	public RadioButton rbPdf,rbExcel;//By Ashwini Patil
	public CheckBox cbCompleted,cbCancelled,cbOther;//By Ashwini Patil
	
	public MultipleSRCopyPrintPopup(Contract con){
		super();
		getPopup().setWidth("600px");
	//		getPopup().setWidth("1000px");//changed from 600 to 800 Ashwini Patil		
	
		createGui();
		getPopup().getElement().addClassName("multipleSRCopyPopupWidth");
		
//		this.getLblOk().addClickHandler(this);
//		this.getLblCancel().addClickHandler(this);
		rbPdf.setValue(true);
		cbCompleted.setValue(true);
		this.btAdd.addClickHandler(this);
		contract=con;
		Console.log("Contract set in constructor");
		Console.log("Customer Id="+getContract().getCustomerId());
		AppUtility.makeCustomerBranchLive(olbBranch, contract.getCustomerId());
	}
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource() == this.getLblCancel()){
//			this.hidePopUp();
			
			showDialogMessage("Service status filter and excel format is not applicable for the checklist report. All services will be fetched.");			
			reactOnSummary("checklist");
		}
		if(event.getSource() == this.getLblOk()){
			Console.log("inside ok : ");
			reactOnPrintServiceRecord();
		}
		
		if(event.getSource().equals(btAdd)){
			if(tbEmailBox.getValue() != null && !tbEmailBox.getValue().equalsIgnoreCase("")){
				EmployeeBranch branch = new EmployeeBranch();
				branch.setBranchName(tbEmailBox.getValue());				
				emailTable.getDataprovider().getList().add(branch);
				
			}else{
				showDialogMessage("Please add emailId.");
			}
		}
		
		if(event.getSource().equals(rbPdf)){
			if(rbPdf.getValue()==true){
				rbExcel.setValue(false);
			}
			if(cbCancelled.getValue()==false&&cbOther.getValue()==false&&rbExcel.getValue()==false){
				getLblOk().setVisible(true);
				getLblCancel().setVisible(true);
			}
			
		}
		
		if(event.getSource().equals(cbCancelled) || event.getSource().equals(cbOther)||event.getSource().equals(rbExcel)) {
			if(cbCancelled.getValue()==true||cbOther.getValue()==true||rbExcel.getValue()==true){
				getLblOk().setVisible(false);
			}
			if(cbCancelled.getValue()==false&&cbOther.getValue()==false&&rbExcel.getValue()==false){
				getLblOk().setVisible(true);
				getLblCancel().setVisible(true);
			}
			if(rbExcel.getValue()==true) {
					rbPdf.setValue(false);
					getLblCancel().setVisible(false);
			}
			
		}
		
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		initalizeWidget();
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingServicesInfo = fbuilder
				.setlabel("Multiple SR Copy Print")
				.widgetType(FieldType.Grouping).setMandatory(false)
				.setColSpan(6).build(); 
		
		fbuilder = new FormFieldBuilder("From Date", dbfromDate);
		FormField fdbfromDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("To Date", dbToDate);
		FormField fdbToDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Customer Branch", olbBranch);
		FormField folbBranch = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Service Id", ibServiceId);
		FormField fibServiceId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Email",tbEmailBox);
		FormField ftbEmailBox= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Pdf",rbPdf);
		FormField frbpdf= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Excel",rbExcel);
		FormField frbExcel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();		
		
		fbuilder = new FormFieldBuilder("Completed",cbCompleted);
		FormField fcbCompleted= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();		
		
		fbuilder = new FormFieldBuilder("In Progress",cbOther);
		FormField fcbOther= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();		
		
		fbuilder = new FormFieldBuilder("Cancelled or Closed",cbCancelled);
		FormField fcbCancelled= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();		
		
		fbuilder = new FormFieldBuilder("",btAdd);
		FormField fbtAdd= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", emailTable.getTable());
		FormField femailTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();//original 4

		FormField fGroupingEmailInformation=fbuilder.setlabel("Email Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();//original 4

		FormField fGroupingServiceStatus=fbuilder.setlabel("Select Service Status").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(5).build();//original 4

		FormField[][] formfield = {
				{ fgroupingServicesInfo   },
				{fdbfromDate , fdbToDate , folbBranch , fibServiceId} ,
				{fGroupingServiceStatus},
				{fcbCompleted , fcbCancelled , fcbOther},
				{fGroupingEmailInformation},
				{ftbEmailBox , fbtAdd , frbpdf , frbExcel },
				{femailTable}
		};
		this.fields = formfield;
		
		
	
	}
	@SuppressWarnings("unused")
	private void initalizeWidget() {
		Console.log("multipleSRCopyPrintPopup initalizeWidget called");
//		dbfromDate = new DateBox();
		dbfromDate = new DateBoxWithYearSelector();

//		dbToDate = new DateBox();
		dbToDate = new DateBoxWithYearSelector();

		ibServiceId = new IntegerBox();
		olbBranch = new ObjectListBox<Branch>();
//		AppUtility.makeBranchListBoxLive(olbBranch);
//		Console.log("Customer Id="+getContract().getCustomerId());
//		AppUtility.makeCustomerBranchLive(olbBranch, contract.getCustomerId());
		tbEmailBox = new EmailTextBox();
		btAdd = new Button("Add");
		
		
		rbPdf=new RadioButton("Pdf");
		rbPdf.setValue(true);
		rbPdf.addClickHandler(this);
		
		rbExcel=new RadioButton("Excel");
		rbExcel.setValue(false);
		rbExcel.addClickHandler(this);
		
		cbCompleted=new CheckBox();
		cbCompleted.setValue(true);
		cbCompleted.addClickHandler(this);
		cbCancelled=new CheckBox();
		cbCancelled.addClickHandler(this);
		cbOther=new CheckBox();
		cbOther.addClickHandler(this);
						
		emailTable = new EmployeeBranchTable();
	}
	
	public DateBox getDbfromDate() {
		return dbfromDate;
	}
	public void setDbfromDate(DateBox dbfromDate) {
		this.dbfromDate = dbfromDate;
	}
	public DateBox getDbToDate() {
		return dbToDate;
	}
	public void setDbToDate(DateBox dbToDate) {
		this.dbToDate = dbToDate;
	}
	public IntegerBox getIbServiceId() {
		return ibServiceId;
	}
	public void setIbServiceId(IntegerBox ibServiceId) {
		this.ibServiceId = ibServiceId;
	}
	public ObjectListBox<Branch> getOlbBranch() {
		return olbBranch;
	}
	public void setOlbBranch(ObjectListBox<Branch> olbBranch) {
		this.olbBranch = olbBranch;
	}
	private void reactOnPrintServiceRecord(){
		ArrayList<String> emailIdList = new ArrayList<String>();
		if(emailTable.getDataprovider().getList().size() > 0){	
			for(EmployeeBranch branch : emailTable.getDataprovider().getList()){
				emailIdList.add(branch.getBranchName());	
			}
		}else{
			showDialogMessage("Please add emailId in email information.");
			return;
		}
		CommonServiceAsync commonSer = GWT.create(CommonService.class);
		Date fromDate = null , toDate = null;
		String branch = "";
		int serviceId = 0;
		if(this.getDbfromDate().getValue() != null){
			fromDate = this.getDbfromDate().getValue();
		}
		Console.log("1: ");
		if(this.getDbToDate().getValue() != null){
			toDate = this.getDbToDate().getValue();
		}
		Console.log("2 : ");
		if(this.getOlbBranch().getSelectedIndex() != 0){
			branch = this.getOlbBranch().getValue();
		}
		Console.log("3 : ");
		if(this.getIbServiceId().getValue() != null){
			serviceId = this.getIbServiceId().getValue();
		}
		
		
		Console.log("5 : " + fromDate +" "+toDate +" "+ branch +" "+serviceId+" "+ this.getContract());
			commonSer.printSRCopy(UserConfiguration.getCompanyId(), fromDate,
				toDate, branch, serviceId ,emailIdList,this.getContract()
				, new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						Console.log("Error : "+ caught);
					}

					@Override
					public void onSuccess(Void result) {
						// TODO Auto-generated method stub
						showDialogMessage("Email sending process started. Please check your inbox after few minutes.");
						hidePopUp();
					}
				});
	}
	
	
	/**
	 * @author Ashwini Patil 
	 * @since 12-02-2022
	 * When summary button will be clicked, multiple sr copy pdf or excel will be mailed as per reportformat and service status selection
	 * This development is common for all the clients
	 **/
	public void reactOnSummary(String reportName){
		ArrayList<String> emailIdList = new ArrayList<String>();
		if(emailTable.getDataprovider().getList().size() > 0){	
			for(EmployeeBranch branch : emailTable.getDataprovider().getList()){
				emailIdList.add(branch.getBranchName());	
			}
		}else{
			showDialogMessage("Please add emailId in email information.");
			return;
		}
		CommonServiceAsync commonSer = GWT.create(CommonService.class);
		Date fromDate = null , toDate = null;
		String branch = "";
		int serviceId = 0;
		if(this.getDbfromDate().getValue() != null){
			fromDate = this.getDbfromDate().getValue();
		}
		Console.log("1: ");
		if(this.getDbToDate().getValue() != null){
			toDate = this.getDbToDate().getValue();
		}
		Console.log("2 : ");
		if(this.getOlbBranch().getSelectedIndex() != 0){
			branch = this.getOlbBranch().getValue();
		}
		Console.log("3 : ");
		if(this.getIbServiceId().getValue() != null){
			serviceId = this.getIbServiceId().getValue();
		}
		
		String reportFormat="";
		if(rbPdf.getValue()==true)
			reportFormat="pdf";
		if(rbExcel.getValue()==true)
			reportFormat="excel";
		Console.log("reportFormat="+reportFormat);
		if(rbPdf.getValue()==false&&rbExcel.getValue()==false) {
			showDialogMessage("Please select Pdf or Excel format to get summary report");
			return;
		}
			
		
		Boolean completedFlag=false, CancelledFlag=false, otherFlag=false;
		if(cbCompleted.getValue()==true)
			completedFlag=true;
		if(cbCancelled.getValue()==true)
			CancelledFlag=true;
		if(cbOther.getValue()==true)
			otherFlag=true;
		if(cbCompleted.getValue()==false&&cbCancelled.getValue()==false&&cbOther.getValue()==false) {
			showDialogMessage("Please select atleast one service status");
			return;
		}
		
		Console.log("flags are"+completedFlag+CancelledFlag+otherFlag);
			
					
		
		Console.log("flags are"+completedFlag+CancelledFlag+otherFlag);
		Console.log("5 : " + fromDate +" "+toDate +" "+ branch +" "+serviceId+" "+ this.getContract());
			commonSer.printSummarySRCopy(UserConfiguration.getCompanyId(), fromDate,
				toDate, branch, serviceId ,emailIdList,this.getContract(),reportName,reportFormat,completedFlag,CancelledFlag,otherFlag
				, new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						Console.log("Error : "+ caught);
					}

					@Override
					public void onSuccess(Void result) {
						// TODO Auto-generated method stub
						showDialogMessage("Email sending process started. Please check your inbox after few minutes.");
						hidePopUp();
					}
				});
	}
	
	public Contract getContract() {
		return contract;
	}
	public void setContract(Contract contract) {
		this.contract = contract;
	}
	
}
