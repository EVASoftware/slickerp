package com.slicktechnologies.client.views.popups;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.FontStyle;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.DataMigrationService;
import com.slicktechnologies.client.services.DataMigrationServiceAsync;
import com.slicktechnologies.client.services.DataMigrationTaskQueueService;
import com.slicktechnologies.client.services.DataMigrationTaskQueueServiceAsync;
import com.slicktechnologies.client.services.DocumentUploadService;
import com.slicktechnologies.client.services.DocumentUploadServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.datamigration.DataMigrationForm;
import com.slicktechnologies.client.views.datamigration.uploadOptionPopUp;
import com.slicktechnologies.shared.ContractUploadDetails;
import com.slicktechnologies.shared.EmployeeAsset;
import com.slicktechnologies.shared.MINUploadExcelDetails;
import com.slicktechnologies.shared.common.attendance.EmployeeLeaveBalance;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorPriceListDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.servicerelated.AssetMovementInfo;

public class UploadFilePopup extends AbsolutePanel implements ClickHandler {

	UploadComposite upload;
	Button btnOk,btnCancel;
	
	InlineLabel headingLabel;
	
	String uploadDoc;
	String url;
	
	DocumentUploadServiceAsync documentuploadAsync = GWT.create(DocumentUploadService.class);
	ConditionDialogBox conditionPopup = new ConditionDialogBox(
			"Do you want to cancel inactive Customer Branch's open services? It will not cancel completed services. ", AppConstants.YES,
			AppConstants.NO);
	PopupPanel panel;
	TextArea info;
	
	private void initializeWidget(){
		
		upload=new UploadComposite();
		upload.getElement().getStyle().setHeight(25, Unit.PX);
		btnOk=new Button("Save");
		btnOk.getElement().getStyle().setWidth(80, Unit.PX);
		btnCancel=new Button("Cancel");
		btnCancel.getElement().getStyle().setWidth(80, Unit.PX);
		info=new TextArea();
		info.setHeight("300px");
		info.setWidth("550px");

	}
	
	
	


	public UploadFilePopup() {
		initializeWidget();
		
		headingLabel=new InlineLabel();
		headingLabel.getElement().getStyle().setFontSize(15, Unit.PX);
		headingLabel.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		headingLabel.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		
		InlineLabel step3=new InlineLabel("Choose file to upload.");
		step3.getElement().getStyle().setFontSize(12, Unit.PX);
		step3.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		step3.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		add(headingLabel,175,20);
		
		add(step3,10,60);
		add(upload,10,85);
				
		add(btnOk,100,120);
		add(btnCancel,220,120);
		
		
		setSize("450px","200px");
		this.getElement().setId("login-form");
	}
	
	
	public void clear(){
		upload.clear();
		headingLabel.setText("");
		url="";
		uploadDoc="";
		
	}
	
	public boolean validate(){
		GWTCAlert alert=new GWTCAlert();
		if(upload.getValue()==null||upload.getValue().getName()==null||upload.getValue().getName().equals("")){
			alert.alert("Please upload excel!");
			return false;
		}
			
		return true;
	}
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		Console.log("on click");
		
	}
	
	
	

	


	public UploadComposite getUpload() {
		return upload;
	}

	public void setUpload(UploadComposite upload) {
		this.upload = upload;
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}



	public InlineLabel getHeadingLabel() {
		return headingLabel;
	}



	public void setHeadingLabel(InlineLabel headingLabel) {
		this.headingLabel = headingLabel;
	}



	public String getUploadDoc() {
		return uploadDoc;
	}



	public void setUploadDoc(String uploadDoc) {
		this.uploadDoc = uploadDoc;
	}



	public String getUrl() {
		return url;
	}



	public void setUrl(String url) {
		this.url = url;
	}

	
	public void showDialogMessage(String msg){
		GWTCAlert alert=new GWTCAlert();
		alert.alert(msg);
	}
	protected GWTCGlassPanel glassPanel;
	public void showWaitSymbol() {
		glassPanel = new GWTCGlassPanel();
		glassPanel.show();
	}
	public void hideWaitSymbol() {
	   glassPanel.hide();
		
	}
	
	public void setInfo(TextArea info) {
		this.info = info;
	}

}

