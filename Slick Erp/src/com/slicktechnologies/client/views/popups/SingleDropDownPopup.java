package com.slicktechnologies.client.views.popups;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.shared.Quotation;

public class SingleDropDownPopup extends PopupScreen{

	ObjectListBox<Quotation> olbQuotationId;
	GenricServiceAsync genAsync = GWT.create(GenricService.class);
	

	public SingleDropDownPopup(){
		super(FormStyle.DEFAULT);
		createGui();
	}

	@Override
	public void createScreen() {
		olbQuotationId = new ObjectListBox<Quotation>();
		
		FormFieldBuilder fbuilder;

		fbuilder = new FormFieldBuilder();
		FormField fsearchFilters = fbuilder.setlabel("Quotation Information").widgetType(FieldType.Grouping).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Quotation Id",olbQuotationId);
		FormField olbQuotationId = fbuilder.setMandatory(true).setMandatoryMsg("Quotation id is mandatory!").setColSpan(0)
									.setRowSpan(0).build();
		FormField [][]formfield = {
				{fsearchFilters},
				{olbQuotationId},
		};
		this.fields=formfield;
	}
	
	public void makequotaionlive(int customerId) {

		MyQuerry querry = new MyQuerry();
		Vector<Filter> filterVec = new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(customerId);
		filterVec.add(filter);
		
		/**
		 * @author Anil @since 07-04-2021
		 * Added search field for loading quotation
		 */
		
//		filter = new Filter();
//		filter.setQuerryString("");
//		filter.setStringValue(Quotation.APPROVED);
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setStringValue(Quotation.APPROVED);
		
		querry.setFilters(filterVec);
		querry.setQuerryObject(new Quotation());
		
		this.showWaitSymbol();
		genAsync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				olbQuotationId.clear();
				olbQuotationId.addItem("--SELECT--");
				if(result!=null&&result.size()!=0){
					List<Quotation> quotationList = new ArrayList<Quotation>();
					for(SuperModel model : result){
						Quotation quotation = (Quotation) model;
						quotationList.add(quotation);
					}
					Collections.sort(quotationList, new Comparator<Quotation>() {
	
						@Override
						public int compare(Quotation q1, Quotation q2) {
							return q2.compareTo(q1);
						}
					});
					olbQuotationId.setListItems(quotationList);
				}
				hideWaitSymbol();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				hideWaitSymbol();
			}
		});
		
	}

	@Override
	public void onClick(ClickEvent event) {

		
	}

	public ObjectListBox<Quotation> getOlbQuotationId() {
		return olbQuotationId;
	}

	public void setOlbQuotationId(ObjectListBox<Quotation> olbQuotationId) {
		this.olbQuotationId = olbQuotationId;
	}

	
	

}


