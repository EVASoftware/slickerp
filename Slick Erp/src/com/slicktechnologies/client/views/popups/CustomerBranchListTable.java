package com.slicktechnologies.client.views.popups;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;

public class CustomerBranchListTable extends SuperTable<CustomerBranchDetails>{

	
	TextColumn<CustomerBranchDetails> txtcolumnBranchName;
	Column<CustomerBranchDetails,String> deleteColumn;

	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		createColumnBranchName();
		createColumndeleteColumn();
	}

	private void createColumnBranchName() {
		txtcolumnBranchName = new TextColumn<CustomerBranchDetails>() {
			
			@Override
			public String getValue(CustomerBranchDetails object) {
				if(object.getBusinessUnitName()!=null){
					return object.getBusinessUnitName();
				}
				return "";
			}
		};
		table.addColumn(txtcolumnBranchName,"Branch Name");
		table.setColumnWidth(txtcolumnBranchName, 100,Unit.PX);
	}

	
	protected void createColumndeleteColumn()
	{
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<CustomerBranchDetails,String>(btnCell)
				{
			@Override
			public String getValue(CustomerBranchDetails object)
			{
				return  "Delete" ;
			}
				};
				table.addColumn(deleteColumn,"Delete");
				table.setColumnWidth(deleteColumn, 100,Unit.PX);
				
				
				deleteColumn.setFieldUpdater(new FieldUpdater<CustomerBranchDetails,String>()
						{
					@Override
					public void update(int index,CustomerBranchDetails object,String value)
					{
						getDataprovider().getList().remove(object);
						table.redrawRow(index);
					}
				});
	}
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
