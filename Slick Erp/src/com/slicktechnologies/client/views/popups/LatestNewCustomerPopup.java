package com.slicktechnologies.client.views.popups;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.client.library.mywidgets.PhoneNumberBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;

public  class LatestNewCustomerPopup extends PopupScreen implements ValueChangeHandler<String>{

	TextBox tbCompanyName;
	TextBox tbFullName;
	
	EmailTextBox etbEmail;
	PhoneNumberBox pnbLandlineNo;
	
	Button btnserviceaddr,btnbillingaddr;
	
	final static GenricServiceAsync async=GWT.create(GenricService.class);
	
	ObjectListBox<Branch> olbbBranch;
	
	
	
	
	AddressComposite addressComp;
	
	AddressComposite addressCompService;
	TextBox tbGstNumber;
	boolean nonManadatory;
	public FlowPanel content;
	public FlexForm form;
	
	/*
	 * date:13-12-2018
	 * @author Ashwini
	 * Des:To add sales person dropdown 
	 */
	
	ObjectListBox<ConfigCategory>olbcustcategory; 
	
	ObjectListBox<Employee>olbsalesPerson;
	
	/**Date 31-8-2019 by Amol added customer group and customer type**/
	ObjectListBox<Config>olbCustomerGroup; 
	ObjectListBox<Type> olbCustomerType;
	
	boolean salesPersonRestrictionFlag= false;
	
	ObjectListBox<String> olbSalutation;
	
	public LatestNewCustomerPopup(){
		super();
		getPopup().getElement().addClassName("popupNewCustomer");
		
		createGui();
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","SalesPersonRestriction")
				&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Sales"))
		{
			
			salesPersonRestrictionFlag=true;
			olbsalesPerson.setValue(LoginPresenter.loggedInUser);
			olbsalesPerson.setEnabled(false);
		}
		
	}
	 
  public void initializeWidget(){
	 
	  tbCompanyName = new TextBox();
	  
	
	  
	  tbFullName = new TextBox();
	  tbCompanyName.addValueChangeHandler(this);
	  tbFullName.addValueChangeHandler(this);
//	 
	 
	  olbbBranch = new ObjectListBox<Branch>();
	  AppUtility.makeCustomerBranchListBoxLive(olbbBranch);
	 
	  
	  etbEmail = new EmailTextBox();
	 
	  
	  pnbLandlineNo = new PhoneNumberBox();
	  
	 
	  tbGstNumber = new TextBox();
	  
	  
	  btnserviceaddr = new Button("Copy Service Address");
	  btnbillingaddr = new Button("Copy Billing Address");
	  addressComp = new AddressComposite();
	  addressCompService = new AddressComposite();
	  
	  olbcustcategory = new ObjectListBox<ConfigCategory>();
	  AppUtility.MakeLiveCategoryConfig(olbcustcategory, Screen.CUSTOMERCATEGORY);
	  olbcustcategory.addChangeHandler(new ChangeHandler() {
		
		@Override
		public void onChange(ChangeEvent event) {
			if (olbcustcategory.getSelectedIndex() != 0) {
				ConfigCategory cat = olbcustcategory.getSelectedItem();
				if (cat != null) {
					AppUtility.makeLiveTypeDropDown(olbCustomerType,
							cat.getCategoryName(), cat.getCategoryCode(),
							cat.getInternalType());
				}
			}
		}
	});
	/*
	 *  @author Ashwini
	 *  Date :10-01-2018
	 */
	 
	  
	  if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","SalesPersonRestriction")
			  &&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Sales")){
		  salesPersonRestrictionFlag = true;
		  olbsalesPerson = new ObjectListBox<Employee>();
		  olbsalesPerson.makeEmployeeLive(LoginPresenter.currentModule, AppConstants.LEAD, "Sales Person");
		
	    Timer t = new Timer(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				makeSalesPersonEnable();
			}
			  
		  };t.schedule(3000);
		  
	  }else{
		  
		  olbsalesPerson = new ObjectListBox<Employee>();
		  olbsalesPerson.makeEmployeeLive(LoginPresenter.currentModule, AppConstants.LEAD, "Sales Person");
	  }
 
	  /*
	   * end by Ashwini
	   */
	    olbCustomerType=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbCustomerType, Screen.CUSTOMERTYPE);
		
		olbCustomerGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbCustomerGroup, Screen.CUSTOMERGROUP);
		
		olbSalutation = new ObjectListBox<String>();
		AppUtility.MakeLiveConfig(olbSalutation, Screen.SALUTATION);
		
  }
	protected void validateClientName() {
//		if(form.isCompany.getValue()==true&&form.getTbClientName().getValue().trim()!=null){
			final MyQuerry querry=new MyQuerry();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
			Company c =new Company();
			temp=new Filter();
			temp.setQuerryString("isCompany");
			temp.setBooleanvalue(true);
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("companyName");
			temp.setStringValue(tbCompanyName.getValue().toUpperCase().trim());
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("companyId");
			temp.setLongValue(c.getCompanyId());
			filtervec.add(temp);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Customer());
			async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						showDialogMessage("An Unexpected Error occured !");
						
					}
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						System.out.println("Success"+result.size());
						if(result.size()!=0){
						showDialogMessage("Client Name Already Exists!");
						tbCompanyName.setValue("");
						}
						if(result.size()==0){
						}
					}
					
				});

	}

	private void validateCust() {
//		if(!form.getTbFirstName().getValue().equals("")&&!form.getTbLastName().getValue().equals("")){
			if(!tbFullName.getValue().equals("")){
			String custnameval=tbFullName.getValue();
			
			custnameval=custnameval.toUpperCase().trim();
			if(custnameval!=null){

					final MyQuerry querry=new MyQuerry();
					Vector<Filter> filtervec=new Vector<Filter>();
					Filter temp=null;
					Company c =new Company();
//					temp=new Filter();
//					temp.setQuerryString("isCompany");
//					if(form.isCompany.getValue()==false){
//						temp.setBooleanvalue(false);
//					}
//					if(form.isCompany.getValue()==true){
//						temp.setBooleanvalue(true);
//					}
//					filtervec.add(temp);
					
					temp=new Filter();
					temp.setQuerryString("companyId");
					temp.setLongValue(c.getCompanyId());
					filtervec.add(temp);
					
					temp=new Filter();
					temp.setQuerryString("fullname");
					temp.setStringValue(custnameval);
					filtervec.add(temp);
					
					querry.setFilters(filtervec);
					querry.setQuerryObject(new Customer());
					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								showDialogMessage("An Unexpected Error occured !");
								
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								System.out.println("Success"+result.size());
								if(result.size()!=0){
									showDialogMessage("Customer Name Already Exists");
									tbFullName.setValue("");
									
								}
								if(result.size()==0){
								}
							}
						});
			}
		}
	}

	@Override
	public void onClick(ClickEvent event) {}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		
        initializeWidget();
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder();
		FormField fdbCustomerDetail=fbuilder.setlabel("Customer Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		/**Date 21-9-2019 by Amol change the lable using process configuration raised by Rahul For Orkin **/
		FormField fdbcompanyName;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "ChangeCompanyNameAndFullNameLable")){
			fbuilder = new FormFieldBuilder("* Account Name",tbCompanyName);
			fdbcompanyName= fbuilder.setMandatory(true).setMandatoryMsg("Account Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		}else{
		fbuilder = new FormFieldBuilder("Company Name",tbCompanyName);
		fdbcompanyName= fbuilder.setRowSpan(0).setColSpan(0).build();
		}
		FormField fdbFullName;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "ChangeCompanyNameAndFullNameLable")){
			fbuilder = new FormFieldBuilder("Point Of Contact",tbFullName);
			fdbFullName= fbuilder.setMandatory(false).setMandatoryMsg("Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		}else{
		fbuilder = new FormFieldBuilder("* Full Name",tbFullName);
		fdbFullName= fbuilder.setRowSpan(0).setColSpan(0).build();
		}
		/**
		 *  Added By Priyanka  - 18/08/2021
		 *  Des : As per Nitin Sir Instruction Branch is Mandatory
		 */
		fbuilder = new FormFieldBuilder("*Branch",olbbBranch);
		FormField fdbBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" Email",etbEmail);
		FormField fdbemail= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Cell No.",pnbLandlineNo);
		//FormField fdbCellNo= fbuilder.setRowSpan(0).setColSpan(0).build();
		FormField fdbCellNo= fbuilder.setMandatory(true).setMandatoryMsg("Check Mobile Number Entered!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" GST Number",tbGstNumber);
		FormField fdbGstnNo= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "CustomerTypeForNbhc")){
			
			fbuilder = new FormFieldBuilder("*Segment",olbcustcategory);
		}else{
		fbuilder = new FormFieldBuilder(" Customer Category",olbcustcategory);
		}
		
		
		
		FormField fdbcustcategoary= fbuilder.setMandatory(true).setMandatoryMsg(" Segment is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Sales Person",olbsalesPerson);
		FormField fdbsalesPerson = fbuilder.setMandatory(false).setColSpan(0).setRowSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingBillingAddress=fbuilder.setlabel("Billing Address").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder=new FormFieldBuilder("",btnserviceaddr);
		FormField fserviceadrsbtn=fbuilder.build();
				
		fbuilder = new FormFieldBuilder("",addressComp);
		FormField fbillingAddressComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingServiceAddress=fbuilder.setlabel("Service Address").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder=new FormFieldBuilder("",btnbillingaddr);
		FormField fbillingadsbtn=fbuilder.build();
				
		fbuilder = new FormFieldBuilder("",addressCompService);
		FormField fserviceaddresscomposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Customer Group",olbCustomerGroup);
		FormField  folbCustomerGroup= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).setMandatoryMsg("Group is Mandatory!").build();
		
		fbuilder = new FormFieldBuilder("Customer Type",olbCustomerType);
		FormField folbcCustomerType= fbuilder.setMandatory(false).setMandatoryMsg("Customer Type is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Salutation",olbSalutation);
		FormField folbSalutation= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField[][] formfield ={
				{fdbCustomerDetail},
				{folbSalutation,fdbcompanyName,fdbFullName},
				{fdbemail,fdbCellNo,fdbBranch},
				{folbCustomerGroup,fdbcustcategoary,folbcCustomerType},
				{fdbsalesPerson,fdbGstnNo},
				{fgroupingBillingAddress},
				{fserviceadrsbtn},
				{fbillingAddressComposite},
				{fgroupingServiceAddress},
				{fbillingadsbtn},
				{fserviceaddresscomposite},
				
		};
		
		this.fields=formfield;
       
		
		
	}
	
	
	
	private void makeSalesPersonEnable() {
		
		olbsalesPerson.setValue(LoginPresenter.loggedInUser);
		
		olbsalesPerson.setEnabled(false);
	}
	
	
	
	
	
	
	/*
	 * Date::13-12-2018
	 * @author Ashwini
	 */
	public ObjectListBox<ConfigCategory> getolbcustcategory(){
		
		return olbcustcategory ;
	}
	
	public void setolbcustcategory(ObjectListBox<ConfigCategory> olbcustcategory){
		this.olbcustcategory = olbcustcategory;
	}
	
	/*
	 * end by Ashwini
	 */
    
	public TextBox getTbCompanyName() {
		return tbCompanyName;
	}

	public void setTbCompanyName(TextBox tbCompanyName) {
		this.tbCompanyName = tbCompanyName;
	}

	
	public ObjectListBox<Branch> getOlbbBranch() {
		return olbbBranch;
	}

	public void setOlbbBranch(ObjectListBox<Branch> olbbBranch) {
		this.olbbBranch = olbbBranch;
	}
	
	public EmailTextBox getEtbEmail() {
		return etbEmail;
	}

	public void setEtbEmail(EmailTextBox etbEmail) {
		this.etbEmail = etbEmail;
	}

	public PhoneNumberBox getPnbLandlineNo() {
		return pnbLandlineNo;
	}

	public void setPnbLandlineNo(PhoneNumberBox pnbLandlineNo) {
		this.pnbLandlineNo = pnbLandlineNo;
	}
	
	public TextBox getTbFullName() {
		return tbFullName;
	}

	public void setTbFullName(TextBox tbFullName) {
		this.tbFullName = tbFullName;
	}

	public AddressComposite getAddressComp() {
		return addressComp;
	}

	public void setAddressComp(AddressComposite addressComp) {
		this.addressComp = addressComp;
	}

	/**
	 * Date 26-4-2018
	 * By jayshree
	 */
	
	public AddressComposite getAddressCompservice() {
		return addressCompService;
	}

	public void setAddressCompservice(AddressComposite addressCompservice) {
		this.addressCompService = addressCompservice;
	}
	
	public Button getBtnserviceaddr() {
		return btnserviceaddr;
	}
	
	public void setBtnserviceaddr(Button btnserviceaddr) {
		this.btnserviceaddr = btnserviceaddr;
	}
	
	public Button getBtnbillingaddr() {
		return btnbillingaddr;
	}
	
	public void setBtnbillingaddr(Button btnbillingaddr) {
		this.btnbillingaddr = btnbillingaddr;
	}
	
	//End By jayshree
	
	public TextBox getTbGstNumber() {
		return tbGstNumber;
	}

	public void setTbGstNumber(TextBox tbGstNumber) {
		this.tbGstNumber = tbGstNumber;
	}
   
	/*
	 * date:10-01-2018
	 * @author Ashwini
	
	 */
	public ObjectListBox<Employee> getOlbsalesPerson() {
		return olbsalesPerson;
	}

	public void setOlbsalesPerson(ObjectListBox<Employee> olbsalesPerson) {
		this.olbsalesPerson = olbsalesPerson;
	}

	public ObjectListBox<Config> getOlbCustomerGroup() {
		return olbCustomerGroup;
	}

	public void setOlbCustomerGroup(ObjectListBox<Config> olbCustomerGroup) {
		this.olbCustomerGroup = olbCustomerGroup;
	}

	public ObjectListBox<Type> getOlbCustomerType() {
		return olbCustomerType;
	}

	public void setOlbCustomerType(ObjectListBox<Type> olbCustomerType) {
		this.olbCustomerType = olbCustomerType;
	}

	
	
	
	
	public ObjectListBox<String> getOlbSalutation() {
		return olbSalutation;
	}

	public void setOlbSalutation(ObjectListBox<String> olbSalutation) {
		this.olbSalutation = olbSalutation;
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(tbFullName)){
			validateCust();
	}
		if(event.getSource().equals(tbCompanyName)){
			validateClientName();
	}
	}

	
	



	
	
	
}
