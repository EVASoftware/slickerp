package com.slicktechnologies.client.views.popups;

import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.services.CustomerNameChangeService;
import com.slicktechnologies.client.services.CustomerNameChangeServiceAsync;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class ContractIdPopUp extends PopupScreen{

	ObjectListBox<Contract> obcontractidlist;
	int customerId =0;
	
	public ContractIdPopUp() {
		super();
		createGui();
	}


	@Override
	public void createScreen() {
		obcontractidlist = new ObjectListBox<Contract>();
		obcontractidlist.MakeLive(getQuerry(customerId));
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fsearchFilters = fbuilder.setlabel("Contract Information").widgetType(FieldType.Grouping).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Contract Id",  obcontractidlist);
		FormField folbBranch = fbuilder.setMandatory(true).setMandatoryMsg("Branch is mandatory!").setRowSpan(0).setColSpan(0).build();
		
	
		FormField [][]formfield = {
				{fsearchFilters},
				{folbBranch},
		};
		this.fields=formfield;
	}
	
	public MyQuerry getQuerry(int customerId) {
		Console.log("customerId =="+customerId);
		MyQuerry myqueery = new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("cinfo.count");
		temp.setIntValue(customerId);
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("status");
		temp.setStringValue("Approved");
		filtervec.add(temp);
		
		myqueery.setFilters(filtervec);
		myqueery.setQuerryObject(new Contract());
		return myqueery;
	}


	@Override
	public void onClick(ClickEvent event) {
		
		
		if(event.getSource() == this.getLblCancel()) {
			hidePopUp();
		}
		
	}
	
	


	public ObjectListBox<Contract> getObcontractidlist() {
		return obcontractidlist;
	}


	public void setObcontractidlist(ObjectListBox<Contract> obcontractidlist) {
		this.obcontractidlist = obcontractidlist;
	}


	public int getCustomerId() {
		return customerId;
	}


	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	
	
	
	
}
