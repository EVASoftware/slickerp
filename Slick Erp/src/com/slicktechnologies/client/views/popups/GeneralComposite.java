package com.slicktechnologies.client.views.popups;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Locality;

public class GeneralComposite extends Composite implements SelectionHandler<Suggestion> {
	
	 public SuggestBox name;
	 /** The name to person info Map */
	public   HashMap<String,SuperModel> info = new HashMap<String,SuperModel>();
	 /** The panel. */
   private FlowPanel panel;
   
   private FormField fName;
   private FormField[][]formfield;
   
   private SuperModel model;
   private String header;
   MyQuerry querry ;
   
   public GeneralComposite(){
   super();

	createForm();
	initWidget(panel);
	initializeName();
   }
   
   public GeneralComposite(String headerName , MyQuerry qu){
	   super();
	   header = headerName;
	   querry = qu;
		createForm();
		initWidget(panel);
		initializeName();
	   }
	
	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		SuggestBox sb = (SuggestBox) event.getSource();
		String selectedItem = sb.getText().trim();
		if (sb == this.name) {
			System.out.println("GEN COMP SELECTED");
			model = info.get(selectedItem);
			System.out.println("MODEL "+model);
			if (model == null){
				sb.setText("");
			}else {
				System.out.println("MODEL1  "+model.toString());
				this.name.setText(model.toString());
			}
		}

	}
	private void initializeNameOracle(){
	 MultiWordSuggestOracle orcl = (MultiWordSuggestOracle) this.name.getSuggestOracle();
	 for(String name:info.keySet())
	   orcl.add(name);	
	}
	private void createForm() {

		FormFieldBuilder builder;
		
		name=new SuggestBox();
		name.addSelectionHandler(this);
		builder=new FormFieldBuilder(header,name);
		fName=builder.setMandatory(false).build();
		
	
			formfield=new FormField[][]
					{
						{fName }
				    };
		
		
				
		FlexForm form = new FlexForm(formfield,FormStyle.ROWFORM);
		panel=new FlowPanel();
		form.setWidth("98%");
		form.setHeight("15px");
		panel.add(form);
		
	}
	
	public void initializeName() {
		// TODO Auto-generated method stub
		GenricServiceAsync genricservice = GWT.create(GenricService.class);
		genricservice.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>()
				{
						@Override
						public void onFailure(Throwable caught) {
							caught.printStackTrace();
						}

						@SuppressWarnings("unchecked")
						@Override
						public void onSuccess(ArrayList<SuperModel> result)
						{
							for(SuperModel m : result){
								info.put(m.toString(), m);
							}
							initializeNameOracle();
						}
			});
	
		
	}
	/**** Date 6-2-2019 added  by AMOL***/
  public SuperModel getModel() {
		return model;
	}
   public void setModel(SuperModel model) {
		this.model = model;
	}
}
