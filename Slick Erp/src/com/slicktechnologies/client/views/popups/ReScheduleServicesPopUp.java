package com.slicktechnologies.client.views.popups;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;

public class ReScheduleServicesPopUp extends PopupScreen implements ChangeHandler, ValueChangeHandler<Date>{
	

	IntegerBox p_interval;
//	static ListBox specificDay;
	ListBox week;
//	public  ListBox excludeDay;
	
	ServiceRePlanScheduleTable servicetable;
	DateBox dbCurrentDate,dbContractEndDate;

	Button btnSpecificDay,btnExcludeDay;
	public ArrayList<Integer> specificDaysList=new ArrayList<Integer>();
	
	
	public ReScheduleServicesPopUp(){
		super();
		servicetable.connectToLocal();
		getPopup().getElement().addClassName("replanpopupWidth");
		servicetable.getTable().setHeight("400px");
		servicetable.getTable().setWidth("1400px");
		createGui();

	}

	private void initalizeWidget() {

		p_interval = new IntegerBox();

//		specificDay = new ListBox();
//		
//		specificDay.insertItem("--Select--", 0);
//		specificDay.insertItem("Sunday", 1);
//		specificDay.insertItem("Monday", 2);
//		specificDay.insertItem("Tuesday", 3);
//		specificDay.insertItem("Wednesday", 4);
//		specificDay.insertItem("Thursday",5);
//		specificDay.insertItem("Friday", 6);
//		specificDay.insertItem("Saturday", 7);
		
		
		week = new ListBox();
		week.insertItem("--", 0);
		week.insertItem("01", 1);
		week.insertItem("02", 2);
		week.insertItem("03", 3);
		week.insertItem("04", 4);
		
		
//		excludeDay = new ListBox();
//		
//		excludeDay.insertItem("--Select--", 0);
//		excludeDay.insertItem("Sunday", 1);
//		excludeDay.insertItem("Monday", 2);
//		excludeDay.insertItem("Tuesday", 3);
//		excludeDay.insertItem("Wednesday", 4);
//		excludeDay.insertItem("Thursday",5);
//		excludeDay.insertItem("Friday", 6);
//		excludeDay.insertItem("Saturday", 7);
		
		servicetable = new ServiceRePlanScheduleTable();
		
//		p_interval.addValueChangeHandler(this);
		p_interval.addChangeHandler(this);
		
//		specificDay.addChangeHandler(this);
		week.addChangeHandler(this);
//		excludeDay.addChangeHandler(this);
		
		
		dbCurrentDate = new DateBox();
		dbCurrentDate.setValue(new Date());
		dbCurrentDate.addValueChangeHandler(this);
		
		dbContractEndDate = new DateBox();
		dbContractEndDate.setEnabled(false);
		
		btnSpecificDay=new Button("Select Specific Days");
		btnSpecificDay.addClickHandler(this);
		
		btnExcludeDay=new Button("Select Exclude Days");
		btnExcludeDay.addClickHandler(this);
		
		
	}
	
	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initalizeWidget();
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingServicesInfo = fbuilder.setlabel("Schedule Filters").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build(); 
		
		fbuilder = new FormFieldBuilder("Interval", p_interval);
		FormField fp_interval = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("Specific Day", specificDay);
//		FormField fspecificDay = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Week", week);
		FormField fweek = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("Exclude Day", excludeDay);
//		FormField fexcludeDay = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Current Date", dbCurrentDate);
		FormField fdbCurrentDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Contract End Date", dbContractEndDate);
		FormField fdbContractEndDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ", servicetable.getTable());
		FormField fservicetable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", btnSpecificDay);
		FormField fbtnSpecificDay = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("", btnExcludeDay);
		FormField fbtnExcludeDay = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField[][] formfield = {
				{ fgroupingServicesInfo   },
//				{fp_interval , fspecificDay , fweek , fexcludeDay },
				{fp_interval , fbtnSpecificDay , fweek , fbtnExcludeDay },
				{fdbCurrentDate,fdbContractEndDate},
				{fservicetable}
		};
		this.fields = formfield;
		
	
	}

	public IntegerBox getP_interval() {
		return p_interval;
	}

	public void setP_interval(IntegerBox p_interval) {
		this.p_interval = p_interval;
	}

	public ListBox getWeek() {
		return week;
	}

	public void setWeek(ListBox week) {
		this.week = week;
	}

//	public ListBox getExcludeDay() {
//		return excludeDay;
//	}
//
//	public void setExcludeDay(ListBox excludeDay) {
//		this.excludeDay = excludeDay;
//	}

	public ServiceRePlanScheduleTable getServicetable() {
		return servicetable;
	}

	public void setServicetable(ServiceRePlanScheduleTable servicetable) {
		this.servicetable = servicetable;
	}

//	@Override
//	public void onValueChange(ValueChangeEvent<Integer> event) {
//		
//		
////		if (event.getSource() == this.getP_interval()) {
////		
////			List<Service> servicelist = this.getServicetable().getDataprovider().getList();
////		
////			this.getExcludeDay().setSelectedIndex(0);
////			this.getWeek().setSelectedIndex(0);
////			this.getSpecificDay().setSelectedIndex(0);
////			
////			int interval = this.getP_interval().getValue();
////			
////			List<Service> updatedServicelist = getServiceSchedulelist(AppConstants.INTERNAL, servicelist, 0, 0, 0, interval);
////			this.getServicetable().getDataprovider().setList(updatedServicelist);
////		}
//		
//		
//
//	}


	@Override
	public void onChange(ChangeEvent event) {
		
//		if(event.getSource() == this.getSpecificDay()){
//			List<Service> servicelist = this.getServicetable().getDataprovider().getList();
//			this.getExcludeDay().setSelectedIndex(0);
//			this.getP_interval().setValue(null);
//			
//			int specificDay = this.getSpecificDay().getSelectedIndex();
//			int selectedWeek = this.getWeek().getSelectedIndex();
//			
//			if(specificDay==0){
//				this.getServicetable().getDataprovider().setList(servicelist);
//			}
//			else{
//				List<Service> updatedServicelist = getServiceSchedulelist(AppConstants.DAY, servicelist, 0, specificDay, selectedWeek, -1);
//				this.getServicetable().getDataprovider().setList(updatedServicelist);
//
//			}
//			
//
//		}
		
		if(event.getSource() == this.getWeek()){
			List<Service> servicelist = this.getServicetable().getDataprovider().getList();
//			this.getExcludeDay().setSelectedIndex(0);
			this.getP_interval().setValue(null);
			
//			int specificDay = this.getSpecificDay().getSelectedIndex();
			int selectedWeek = this.getWeek().getSelectedIndex();
			
			if(selectedWeek==0){
				this.getServicetable().getDataprovider().setList(servicelist);
			}
			else{
//				List<Service> updatedServicelist = getServiceSchedulelist(AppConstants.WEEK, servicelist, 0, specificDay, selectedWeek, -1);
				List<Service> updatedServicelist = getServiceSchedulelistNew(AppConstants.WEEK, servicelist,null, specificDaysList, selectedWeek, -1);
				
				this.getServicetable().getDataprovider().setList(updatedServicelist);

			}			

		}
		
//		if(event.getSource() == this.getExcludeDay()){
//			List<Service> servicelist = this.getServicetable().getDataprovider().getList();
//			this.getP_interval().setValue(null);
//			this.getWeek().setSelectedIndex(0);
////			this.getSpecificDay().setSelectedIndex(0);
//			this.specificDaysList.clear();
//			
//			
//			int excludeDay = this.getExcludeDay().getSelectedIndex();
//			
//			if(excludeDay==0){
//				this.getServicetable().getDataprovider().setList(servicelist);
//			}
//			else{
//				List<Service> updatedServicelist = getServiceSchedulelist(AppConstants.EXCLUDEDAY, servicelist, excludeDay, 0, 0, -1);
//				
//				this.getServicetable().getDataprovider().setList(updatedServicelist);
//
//			}
//			
//
//		}
		
		if (event.getSource() == this.getP_interval()) {
			
			List<Service> servicelist = this.getServicetable().getDataprovider().getList();
		
//			this.getExcludeDay().setSelectedIndex(0);
			this.getWeek().setSelectedIndex(0);
//			this.getSpecificDay().setSelectedIndex(0);
			if(this.specificDaysList!=null&&this.specificDaysList.size()>0)				
			this.specificDaysList.clear();
			
			int interval = this.getP_interval().getValue();
			
//			List<Service> updatedServicelist = getServiceSchedulelist(AppConstants.INTERNAL, servicelist, 0, 0, 0, interval);
			List<Service> updatedServicelist = getServiceSchedulelistNew(AppConstants.INTERNAL, servicelist, null, null, 0, interval);
			
			this.getServicetable().getDataprovider().setList(updatedServicelist);
		}
	}

	public List<Service> getServiceSchedulelist(String scheduleOperation, List<Service> servicelist, 
			 int selectedExcludDay,	int selectedDayIndex, int selectedServiceWeekIndex, int internalValue){
				
		DateTimeFormat format = DateTimeFormat.getFormat("c"); 
		List<Service> updatedServicelist = new ArrayList<Service>();

		HashSet<String> hsproductCodelist = new HashSet<String>();
		for(Service serviceEntity : servicelist){
			hsproductCodelist.add(serviceEntity.getProduct().getProductCode());
		}
		
		List<Service> productwiseServicelist = new ArrayList<Service>();
		
		for(String productCode : hsproductCodelist){
			for(Service serviceEntity : servicelist){
				if(productCode.trim().equals(serviceEntity.getProduct().getProductCode().trim())){
					productwiseServicelist.add(serviceEntity);
				}
			}
		}
		
		for(String strproductCode : hsproductCodelist){
			ArrayList<Service> productwiselist = new ArrayList<Service>();
			for(Service serviceEntity : productwiseServicelist){
				if(strproductCode.trim().equals(serviceEntity.getProduct().getProductCode().trim())){
					productwiselist.add(serviceEntity);
				}
			}
			
			
			ServiceProduct serviceproduct = (ServiceProduct) productwiselist.get(0).getProduct();
			int interval = 0;
			if(serviceproduct!=null){
				Console.log("serviceproduct.getDuration() "+serviceproduct.getDuration());
				Console.log("serviceproduct.getNumberOfServices() "+serviceproduct.getNumberOfServices());

				interval = serviceproduct.getDuration()/serviceproduct.getNumberOfServices();
			}
			Console.log("interval "+interval);
			if(scheduleOperation.trim().equals(AppConstants.INTERNAL) && internalValue!=-1){
				interval = internalValue;
			}	
			
			boolean unitflag = false;
			boolean greterservicesthanweeks = false;
			boolean flag =false;
			boolean sameMonthserviceflag = false;
			
			Date d = new Date(productwiselist.get(0).getServiceDate().getTime());
			if(dbCurrentDate.getValue()!=null){
				 d = new Date(dbCurrentDate.getValue().getTime());
			}
			
//			Date d = new Date(servicelist.get(0).getServiceDate().getTime());
			Date servicedate = new Date(d.getTime());
			Date productEndDate = new Date(productwiselist.get(0).getContractEndDate().getTime());
			Date tempdate=new Date();

			if(scheduleOperation.trim().equals(AppConstants.EXCLUDEDAY)){
				int startday = servicedate.getDay();
				
				if(selectedExcludDay !=0  && startday == selectedExcludDay-1){
					CalendarUtil.addDaysToDate(servicedate, 1);
				}
				if(selectedExcludDay !=0 ){
					CalendarUtil.addDaysToDate(productEndDate, -1);
				}
				d = new Date(servicedate.getTime());
				int lastday = productEndDate.getDay();
				
				if(selectedExcludDay !=0  && lastday == selectedExcludDay-1){
					CalendarUtil.addDaysToDate(productEndDate, -1);
				}
				/**
				 * end
				 */
			}
//			if(scheduleOperation.trim().equals(AppConstants.WEEK)){
//				int months = noOfdays/30;
//				if(noServices>months){
//					greterservicesthanweeks = true;
//					showDialogMessage("Services greater than availabe weeks can not schedule services with weeks");
//					break;
//				}
//			}
			
			
			
			for(int i=0;i<productwiselist.size();i++){
				
				
				if(i>0)
				{
					System.out.println("D == Date"+d);
					if(interval>0)
					CalendarUtil.addDaysToDate(d, interval);
				}
			
				if(scheduleOperation.trim().equals(AppConstants.EXCLUDEDAY)){
					int serDay = d.getDay();
					if(selectedExcludDay !=0  && serDay == selectedExcludDay-1){
						CalendarUtil.addDaysToDate(d, 1);
					}
					
				}
				tempdate = new Date(d.getTime());
				Console.log("tempdate == " +tempdate);
			
				if(i==0){
					productwiselist.get(i).setServiceDate(servicedate);
				}
				if(i!=0){
					if(productEndDate.before(d) == false){
						productwiselist.get(i).setServiceDate(tempdate);
					}else{
						productwiselist.get(i).setServiceDate(productEndDate);
					}
				}
				
				/***  here are for Schedule popup operation ***/ 
				if(scheduleOperation.trim().equals(AppConstants.DAY) || scheduleOperation.trim().equals(AppConstants.WEEK)){
					
					Date day = null;
					int adday1 = selectedDayIndex - 1;
					
					if(scheduleOperation.trim().equals(AppConstants.DAY)){
						String dayOfWeek1 = format.format(d);
						System.out.println("dayOfWeek1 "+dayOfWeek1);
						int adate = Integer.parseInt(dayOfWeek1);
						adday1 = selectedDayIndex - 1;
						System.out.println("adate "+adate);
						System.out.println("adday1 "+adday1);

						int adday = 0;
						if (adate <= adday1) {
							adday = (adday1 - adate);
						} else {
							adday = (7 - adate + adday1);
						}
						System.out.println("adday1 "+adday1);
						System.out.println("d "+d);
						System.out.println("adday "+adday);
						Date newDate = new Date(d.getTime());
						System.out.println("newDate "+newDate);
						CalendarUtil.addDaysToDate(newDate, adday);

//						Date day = new Date(newDate.getTime());
						day = new Date(newDate.getTime());
						System.out.println("day == "+day);
					}
					else{
						day = new Date(d.getTime());
//						adday1 = selectedDayIndex;
					}
					

				
					/**
					 * Date 8-03-2017
					 * added by vijay for scheduling service with week day and week
					 */
					
					if(selectedServiceWeekIndex!=0){
						
						CalendarUtil.setToFirstDayOfMonth(day);

						if(selectedServiceWeekIndex==1){
						 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
						if(day.before(productwiselist.get(i).getContractStartDate())){
							if(i==0){
								day =productwiselist.get(i).getContractStartDate();
							}
							else{
								flag = true;
							}
						}
						if(flag || i>0){
							CalendarUtil.setToFirstDayOfMonth(day);
							 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
						}
					 }
					else if(selectedServiceWeekIndex==2){
							CalendarUtil.addDaysToDate(day, 7);
							 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
							if(day.before(productwiselist.get(i).getContractStartDate())){
								if(i==0){
									day =productwiselist.get(i).getContractStartDate();
								}
								else{
									flag = true;
								}
							}
							if(flag || i>0){
								CalendarUtil.setToFirstDayOfMonth(day);
								CalendarUtil.addDaysToDate(day, 7);
								 day = AppUtility.getCalculatedDatewithWeek(day, adday1);;
							}
					}else if(selectedServiceWeekIndex==3){
							CalendarUtil.addDaysToDate(day, 14);
							 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
							if(day.before(productwiselist.get(i).getContractStartDate())){
								if(i==0){
									day =productwiselist.get(i).getContractStartDate();
								}
								else{
									flag = true;
								}
							}
							if(flag || i>0){
								CalendarUtil.setToFirstDayOfMonth(day);
								CalendarUtil.addDaysToDate(day, 14);
								 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
							}
					}
					else if(selectedServiceWeekIndex==4){
							CalendarUtil.addDaysToDate(day, 21);

							 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
							if(day.before(productwiselist.get(i).getContractStartDate())){
								if(i==0){
									day =productwiselist.get(i).getContractStartDate();
								}
								else{
									flag = true;
								}
							}
							if(flag || i>0){
								CalendarUtil.setToFirstDayOfMonth(day);
								CalendarUtil.addDaysToDate(day, 21);
								 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
							}
					 }
//						 temp = day;
						 
					/**
					 * ends here
					 */
					
					
				}
					
					System.out.println("Day =="+day);	
					if (productEndDate.before(day) == false) {
						productwiselist.get(i).setServiceDate(day);
					} else {
						productwiselist.get(i).setServiceDate(productEndDate);
					}

			  }
				productwiselist.get(i).setServiceDay(ContractForm.serviceDay(productwiselist.get(i).getServiceDate()));

			}
			updatedServicelist.addAll(productwiselist);
			
		}
		

		return updatedServicelist;
		
	}
	
	
	public int getDifferenceDays(Date d1, Date d2) {
	    int daysdiff = 0;
	    long diff = d2.getTime() - d1.getTime();
	    long diffDays = diff / (24 * 60 * 60 * 1000) + 1;
	    daysdiff = (int) diffDays;
	    return daysdiff;
	}

//	public static ListBox getSpecificDay() {
//		return specificDay;
//	}
//
//	public static void setSpecificDay(ListBox specificDay) {
//		ReScheduleServicesPopUp.specificDay = specificDay;
//	}

	public DateBox getDbCurrentDate() {
		return dbCurrentDate;
	}

	public void setDbCurrentDate(DateBox dbCurrentDate) {
		this.dbCurrentDate = dbCurrentDate;
	}

	public DateBox getDbContractEndDate() {
		return dbContractEndDate;
	}

	public void setDbContractEndDate(DateBox dbContractEndDate) {
		this.dbContractEndDate = dbContractEndDate;
	}

	@Override
	public void onValueChange(ValueChangeEvent<Date> event) {
		
		if (event.getSource()==this.dbCurrentDate) {
			Console.log("on value change cureent date");
//			this.getExcludeDay().setSelectedIndex(0);
			this.getWeek().setSelectedIndex(0);
//			this.getSpecificDay().setSelectedIndex(0);
			if(this.specificDaysList!=null&&this.specificDaysList.size()>0)				
			this.specificDaysList.clear();
			
			List<Service> servicelist = this.getServicetable().getDataprovider().getList();
			int noofservices = servicelist.size();
			
			int duration = AppUtility.getDifferenceDays(this.dbCurrentDate.getValue(), servicelist.get(0).getContractEndDate());
			int interval = duration/noofservices;
			
//			List<Service> updatedServicelist = getServiceSchedulelist(AppConstants.INTERNAL, servicelist, 0, 0, 0, interval);
			List<Service> updatedServicelist = getServiceSchedulelistNew(AppConstants.INTERNAL, servicelist, null, null, 0, interval);
			
			this.getServicetable().getDataprovider().setList(updatedServicelist);

		}
		
	}

	public Button getBtnSpecificDay() {
		return btnSpecificDay;
	}

	public void setBtnSpecificDay(Button btnSpecificDay) {
		this.btnSpecificDay = btnSpecificDay;
	}

	public Button getBtnExcludeDay() {
		return btnExcludeDay;
	}

	public void setBtnExcludeDay(Button btnExcludeDay) {
		this.btnExcludeDay = btnExcludeDay;
	}

	public List<Service> getServiceSchedulelistNew(String scheduleOperation, List<Service> servicelist, 
			 ArrayList<Integer> selectedExcludDay,	ArrayList<Integer> selectedSpecificDay, int selectedServiceWeekIndex, int internalValue){
		Console.log("in getServiceSchedulelistNew");
		DateTimeFormat format = DateTimeFormat.getFormat("c"); 
		List<Service> updatedServicelist = new ArrayList<Service>();

		HashSet<String> hsproductCodelist = new HashSet<String>();
		for(Service serviceEntity : servicelist){
			hsproductCodelist.add(serviceEntity.getProduct().getProductCode());
		}
		
		List<Service> productwiseServicelist = new ArrayList<Service>();
		
		for(String productCode : hsproductCodelist){
			for(Service serviceEntity : servicelist){
				if(productCode.trim().equals(serviceEntity.getProduct().getProductCode().trim())){
					productwiseServicelist.add(serviceEntity);
				}
			}
		}
		
		for(String strproductCode : hsproductCodelist){
			ArrayList<Service> productwiselist = new ArrayList<Service>();
			for(Service serviceEntity : productwiseServicelist){
				if(strproductCode.trim().equals(serviceEntity.getProduct().getProductCode().trim())){
					productwiselist.add(serviceEntity);
				}
			}
			
			
			ServiceProduct serviceproduct = (ServiceProduct) productwiselist.get(0).getProduct();
			int interval = 0;
			if(serviceproduct!=null){
				Console.log("serviceproduct.getDuration() "+serviceproduct.getDuration()+" productwiselist size="+productwiselist.size());
				Console.log("serviceproduct.getNumberOfServices() "+serviceproduct.getNumberOfServices());

				interval = serviceproduct.getDuration()/serviceproduct.getNumberOfServices();
			}
			Console.log("interval "+interval);
			if(scheduleOperation.trim().equals(AppConstants.INTERNAL) && internalValue!=-1){
				interval = internalValue;
			}	
			
			boolean unitflag = false;
			boolean greterservicesthanweeks = false;
			boolean flag =false;
			boolean sameMonthserviceflag = false;
			
			Date d = new Date(productwiselist.get(0).getServiceDate().getTime());
			if(dbCurrentDate.getValue()!=null){
				 d = new Date(dbCurrentDate.getValue().getTime());
			}
			
//			Date d = new Date(servicelist.get(0).getServiceDate().getTime());
			Date servicedate = new Date(d.getTime());
			Date productEndDate = new Date(productwiselist.get(0).getContractEndDate().getTime());
			Date tempdate=new Date();

			if(scheduleOperation.trim().equals(AppConstants.EXCLUDEDAY)){
				int startday = servicedate.getDay();
				if(selectedExcludDay!=null&&selectedExcludDay.size()==1) {
					Console.log("scheduleOperation exclude single day startday="+startday+" serviceid="+productwiselist.get(0).getCount()+" exclude days="+selectedExcludDay.toString()+" servicedate="+servicedate);
					
					int excludedDay=selectedExcludDay.get(0);
					if(startday == excludedDay){
						Console.log("in  step1");
						CalendarUtil.addDaysToDate(servicedate, 1);
						Console.log("in  step2 servicedate="+servicedate);
					}
//					if(excludedDay !=0 ){
						Console.log("in  step3");
						CalendarUtil.addDaysToDate(productEndDate, -1);
						Console.log("in  step4 productEndDate="+productEndDate);
//				}
					d = new Date(servicedate.getTime());
					Console.log("in  step5");
					int lastday = productEndDate.getDay();
					Console.log("in  step6");
					if(lastday == excludedDay){
						Console.log("in  step7");
						CalendarUtil.addDaysToDate(productEndDate, -1);
						Console.log("in  step8 productEndDate="+productEndDate);
					}
				}else if(selectedExcludDay!=null&&selectedExcludDay.size()>1) {
					Console.log("scheduleOperation exclude multiple days startday="+startday+" serviceid="+productwiselist.get(0).getCount()+" exclude days="+selectedExcludDay.toString()+" servicedate ="+servicedate);
					while(selectedExcludDay.contains(startday)){
							Console.log("in while");
							CalendarUtil.addDaysToDate(servicedate, 1);
							Console.log("in while step1 servicedate="+servicedate);
							startday = servicedate.getDay();
							Console.log("in while startday="+startday);							
					}
					Console.log("after 1st while startday="+startday);	
//					CalendarUtil.addDaysToDate(productEndDate, -1);
						
					d = new Date(servicedate.getTime());
					int lastday = productEndDate.getDay();
					Console.log("lastday="+lastday +" productEndDate="+productEndDate);		
					while(selectedExcludDay.contains(lastday)){
							CalendarUtil.addDaysToDate(productEndDate, -1);
							lastday = productEndDate.getDay();
							Console.log("in while lastday="+lastday);
					}
					Console.log("after 2nd while lastday="+lastday+" productEndDate="+productEndDate);
				}else {
					Console.log("selectedExcludDay list null");
				}
				
				
				
			}
//			if(scheduleOperation.trim().equals(AppConstants.WEEK)){
//				int months = noOfdays/30;
//				if(noServices>months){
//					greterservicesthanweeks = true;
//					showDialogMessage("Services greater than availabe weeks can not schedule services with weeks");
//					break;
//				}
//			}
			
			
			
			for(int i=0;i<productwiselist.size();i++){
				
				
				if(i>0)
				{
					System.out.println("D == Date"+d);
					Console.log("d before interval="+d);
					if(interval>0)
					CalendarUtil.addDaysToDate(d, interval);
					Console.log("d after interval="+d);
				}
			
				if(scheduleOperation.trim().equals(AppConstants.EXCLUDEDAY)){
					int serDay = productwiselist.get(i).getServiceDate().getDay();
					d=productwiselist.get(i).getServiceDate();
//					if(selectedExcludDay !=0  && serDay == selectedExcludDay-1){
//						CalendarUtil.addDaysToDate(d, 1);
//					}
					if(selectedExcludDay!=null) {
						Console.log("before tempdate while serDay="+serDay+" serviceid="+productwiselist.get(i).getCount());
						while(selectedExcludDay.contains(serDay)) {
							CalendarUtil.addDaysToDate(d, 1);
							serDay=d.getDay();
						}
						Console.log("after tempdate while d="+d+" serviceid="+productwiselist.get(i).getCount());
						
					}
					
				}
				tempdate = new Date(d.getTime());
				Console.log("tempdate == " +tempdate+" serviceId="+productwiselist.get(i).getCount());
			
				if(i==0){
					productwiselist.get(i).setServiceDate(servicedate);
				}
				if(i!=0){
					Console.log("step 2.1 d="+d);
					if(productEndDate.before(d) == false){
						Console.log("step 2.2");
						productwiselist.get(i).setServiceDate(tempdate);
						Console.log("step 2.3");
					}else{
						Console.log("step 2.4");
						productwiselist.get(i).setServiceDate(productEndDate);
						Console.log("step 2.5");
					}
				}
				
				/***  here are for Schedule popup operation ***/ 
				if(scheduleOperation.trim().equals(AppConstants.DAY) || scheduleOperation.trim().equals(AppConstants.WEEK)){
					
					Date day = null;
//					int adday1 = selectedDayIndex - 1;
					
					if(scheduleOperation.trim().equals(AppConstants.DAY)){
						d=productwiselist.get(i).getServiceDate();
						String dayOfWeek1 = format.format(d);
						Console.log("dayOfWeek1="+dayOfWeek1);
						System.out.println("dayOfWeek1 "+dayOfWeek1);
						int adate = Integer.parseInt(dayOfWeek1);
//						adday1 = selectedDayIndex - 1;
//						System.out.println("adate "+adate);
//						System.out.println("adday1 "+adday1);

						int adday = 0;
//						if(!selectedSpecificDay.contains(adate)) {
//							for(int sd=0;sd<selectedSpecificDay.size();sd++) {
//							if (adate <= selectedSpecificDay.get(sd)) {
//								adday = (selectedSpecificDay.get(sd) - adate);
//							} else {
//								adday = (7 - adate + selectedSpecificDay.get(0));
//							}			
//							}
//						}
						Console.log("Date "+d);
						if(selectedSpecificDay!=null&&selectedSpecificDay.size()>0) {
						while(!selectedSpecificDay.contains(adate)) {
							CalendarUtil.addDaysToDate(d, 1);
							adate=d.getDay();
						}
						}
						
//						System.out.println("adday1 "+adday1);
						System.out.println("d "+d);
						System.out.println("adday "+adday);
						Date newDate = new Date(d.getTime());
						Console.log("newDate "+newDate);
//						CalendarUtil.addDaysToDate(newDate, adday);
//						Console.log("newDate after"+newDate);
						
//						Date day = new Date(newDate.getTime());
						day = new Date(newDate.getTime());
						System.out.println("day == "+day);					
					}
					else{
						day = new Date(d.getTime());
//						adday1 = selectedDayIndex;
					}
					
					/**
					 * Date 8-03-2017
					 * added by vijay for scheduling service with week day and week
					 */
					
					if(selectedServiceWeekIndex!=0){
						
						CalendarUtil.setToFirstDayOfMonth(day);

						if(selectedServiceWeekIndex==1){
							day =  AppUtility.getCalculatedDatewithWeekNew(day, selectedSpecificDay);
//						 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
						if(day.before(productwiselist.get(i).getContractStartDate())){
							if(i==0){
								day =productwiselist.get(i).getContractStartDate();
							}
							else{
								flag = true;
							}
						}
						if(flag || i>0){
							CalendarUtil.setToFirstDayOfMonth(day);
//							 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
							day =  AppUtility.getCalculatedDatewithWeekNew(day, selectedSpecificDay);
						}
					 }
					else if(selectedServiceWeekIndex==2){
							CalendarUtil.addDaysToDate(day, 7);
//							 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
							day =  AppUtility.getCalculatedDatewithWeekNew(day, selectedSpecificDay);
							if(day.before(productwiselist.get(i).getContractStartDate())){
								if(i==0){
									day =productwiselist.get(i).getContractStartDate();
								}
								else{
									flag = true;
								}
							}
							if(flag || i>0){
								CalendarUtil.setToFirstDayOfMonth(day);
								CalendarUtil.addDaysToDate(day, 7);
//								 day = AppUtility.getCalculatedDatewithWeek(day, adday1);;
								day =  AppUtility.getCalculatedDatewithWeekNew(day, selectedSpecificDay);
							}
					}else if(selectedServiceWeekIndex==3){
							CalendarUtil.addDaysToDate(day, 14);
//							 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
							day =  AppUtility.getCalculatedDatewithWeekNew(day, selectedSpecificDay);
							if(day.before(productwiselist.get(i).getContractStartDate())){
								if(i==0){
									day =productwiselist.get(i).getContractStartDate();
								}
								else{
									flag = true;
								}
							}
							if(flag || i>0){
								CalendarUtil.setToFirstDayOfMonth(day);
								CalendarUtil.addDaysToDate(day, 14);
//								 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
								day =  AppUtility.getCalculatedDatewithWeekNew(day, selectedSpecificDay);
							}
					}
					else if(selectedServiceWeekIndex==4){
							CalendarUtil.addDaysToDate(day, 21);

//							 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
							day =  AppUtility.getCalculatedDatewithWeekNew(day, selectedSpecificDay);
							if(day.before(productwiselist.get(i).getContractStartDate())){
								if(i==0){
									day =productwiselist.get(i).getContractStartDate();
								}
								else{
									flag = true;
								}
							}
							if(flag || i>0){
								CalendarUtil.setToFirstDayOfMonth(day);
								CalendarUtil.addDaysToDate(day, 21);
//								 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
								day =  AppUtility.getCalculatedDatewithWeekNew(day, selectedSpecificDay);
							}
					 }
//						 temp = day;
						 
					/**
					 * ends here
					 */
					
					
				}
					
					System.out.println("Day =="+day);	
					if (productEndDate.before(day) == false) {
						productwiselist.get(i).setServiceDate(day);
					} else {
						productwiselist.get(i).setServiceDate(productEndDate);
					}

			  }
				Console.log("step 2.6");
				productwiselist.get(i).setServiceDay(ContractForm.serviceDay(productwiselist.get(i).getServiceDate()));
				Console.log("date set to service id "+productwiselist.get(i).getCount());
			}
			updatedServicelist.addAll(productwiselist);
			
		}
		

		return updatedServicelist;
		
	}
	
}
