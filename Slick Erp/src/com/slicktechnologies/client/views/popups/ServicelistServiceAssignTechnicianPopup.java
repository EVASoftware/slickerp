package com.slicktechnologies.client.views.popups;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.Button;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.project.concproject.EmpolyeeTable;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class ServicelistServiceAssignTechnicianPopup extends PopupScreen{

	/**
	 *  nidhi
	 * 23-10-2017
	 *  for set ser Engineer in service 
	 */
	 public ObjectListBox<Employee> olbEmployee;
	/**
	 *  end
	 */
	  
	/**
	 * @author Anil @Since 27-01-2021
	 */
	public FormField folbEmployee;
	
	public ServicelistServiceAssignTechnicianPopup(){
		super();
		createGui();
	}
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	public void initializeWidgets(){
		
		olbEmployee = new ObjectListBox<Employee>();
		/**
		 *  nidhi
		 *  12-10-2017
		 *  
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "LoadOperatorDropDown")){
			olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERPROJECT, "Operator");
		}else{
			olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CUSTOMERPROJECT, "Technician");
		}
		/**
		 *  end
		 */
	}
	
	
	@Override
	public void createScreen() {

		initializeWidgets();
		/**
		 * @author Anil
		 * @since 02-07-2020
		 */
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "LoadAccountManager")){
//			olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CONTRACT, "Account Manager");
//		}

		FormFieldBuilder fbuilder;
		
//		fbuilder = new FormFieldBuilder("Service Engineer",olbEmployee);
		/**@Sheetal:17-03-2022 
		 *  Des : Renaming Service Engineer to Technician, requirement by nitin sir**/
		fbuilder = new FormFieldBuilder("Technician",olbEmployee);
		folbEmployee = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
	
		FormField [][]formfield = {
				{folbEmployee},
		};
		this.fields=formfield;
	
	}

	public ObjectListBox<Employee> getOlbEmployee() {
		return olbEmployee;
	}

	public void setOlbEmployee(ObjectListBox<Employee> olbEmployee) {
		this.olbEmployee = olbEmployee;
	}

}
