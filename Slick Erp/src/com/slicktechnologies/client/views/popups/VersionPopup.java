package com.slicktechnologies.client.views.popups;


import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.Button;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.project.concproject.EmpolyeeTable;
import com.slicktechnologies.shared.common.cnc.CNCVersion;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class VersionPopup extends PopupScreen{
	 
	  ObjectListBox<CNCVersion> olbVersion;
	 
	public VersionPopup(){
		super();
		createGui();
	}
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	public void initializeWidgets(){
		
		 olbVersion = new ObjectListBox<CNCVersion>();
	}
	
	
	@Override
	public void createScreen() {

		initializeWidgets();
		

		FormFieldBuilder fbuilder;
	
			fbuilder = new FormFieldBuilder("Select version",  olbVersion);
			FormField folbVersion = fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
			
		
			FormField [][]formfield = {
					{folbVersion},
			};
			this.fields=formfield;
	}
	public ObjectListBox<CNCVersion> getOlbVersion() {
		return olbVersion;
	}
	public void setOlbVersion(ObjectListBox<CNCVersion> olbVersion) {
		this.olbVersion = olbVersion;
	}
	
}
