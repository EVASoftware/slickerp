package com.slicktechnologies.client.views.popups;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Service;

public class ServiceRePlanScheduleTable extends SuperTable<Service>{

	
	TextColumn<Service> serviceId;
	TextColumn<Service> customerName;
	TextColumn<Service> serviceBranch;
	TextColumn<Service> serviceDate;
	TextColumn<Service> status;
	TextColumn<Service> serviceEngineer;
	TextColumn<Service> serviceTime;
	TextColumn<Service> productName;
	TextColumn<Service> serviceDay;
	TextColumn<Service> branch;
	TextColumn<Service> contractId;

	@Override
	public void createTable() {

		createColumnServiceId();
		createColumnCutomerName();
		createColumnServiceBranch();
		createColumnServiceDate();
		createColumnStatus();
		createColumnTechnician();
		createColumnServiceTime();
		createColumnProductName();
		createColumnServiceDay();
		createColumnBranch();
		createColumnContractId();
	}

	private void createColumnContractId() {
		contractId = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				return object.getContractCount()+"";
			}
		};
		table.addColumn(contractId, "Contract ID");
		table.setColumnWidth(contractId, 100, Unit.PX);
	}

	private void createColumnBranch() {
		branch = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				return object.getBranch();
			}
		};
		table.addColumn(branch, "Branch");
		table.setColumnWidth(branch, 90, Unit.PX);
	}

	private void createColumnServiceDay() {

		serviceDay = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				if(object.getServiceDay()!=null)
					return object.getServiceDay();
				else
					return "";
			}
		};
		table.addColumn(serviceDay, "Service Day");
		table.setColumnWidth(serviceDay, 120, Unit.PX);
	}

	private void createColumnProductName() {
		productName = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				return object.getProductName();
			}
		};
		table.addColumn(productName, "Product");
		table.setColumnWidth(productName, 120, Unit.PX);
	}

	private void createColumnServiceTime() {
		serviceTime = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				if(object.getServiceTime()!=null)
					return object.getServiceTime();
				else
					return "";
			}
		};
		table.addColumn(serviceTime, "Service Time");
		table.setColumnWidth(serviceTime, 120, Unit.PX);
	}

	private void createColumnTechnician() {

		serviceEngineer = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				if(object.getEmployee()!=null)
					return object.getEmployee();
				else
					return "";
			}
		};
		table.addColumn(serviceEngineer, "Technician");
		table.setColumnWidth(serviceEngineer, 120, Unit.PX);
	}

	private void createColumnStatus() {
		status = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				return object.getStatus();
			}
		};
		table.addColumn(status, "Status");
		table.setColumnWidth(status, 100, Unit.PX);
	}

	private void createColumnServiceDate() {
		serviceDate = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				if (object.getServiceDate() != null) {
					String serDate = AppUtility.parseDate(object
							.getServiceDate());
					return serDate;
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(serviceDate, "Service Date");
		table.setColumnWidth(serviceDate, 120, Unit.PX);
	}

	private void createColumnServiceBranch() {
		serviceBranch = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				return object.getServiceBranch();
			}
		};
		table.addColumn(serviceBranch, "Service Branch");
		table.setColumnWidth(serviceBranch, 150, Unit.PX);
	}

	private void createColumnCutomerName() {
		customerName = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				if(object.getPersonInfo()!=null)
					return object.getPersonInfo().getFullName();
				return "";
			}
		};
		table.addColumn(customerName, "Customer Name");
		table.setColumnWidth(customerName, 150, Unit.PX);
	}

	private void createColumnServiceId() {

		serviceId = new TextColumn<Service>() {
			
			@Override
			public String getValue(Service object) {
				return object.getCount()+"";
			}
		};
		
		table.addColumn(serviceId, "Service ID");
		table.setColumnWidth(serviceId, "100px");
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		
	}

}
