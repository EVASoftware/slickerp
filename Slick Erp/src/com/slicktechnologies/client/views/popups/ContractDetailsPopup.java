package com.slicktechnologies.client.views.popups;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;

public class ContractDetailsPopup extends PopupScreen {
	
	 TextBox cellno1;
	  TextBox cellno2;
	  TextBox Landlineno;
	  TextBox EmailId;
	  
	 
	 public ContractDetailsPopup(){
			super();
			createGui();
		}
	 
	 private void initilizewidget() {
		 cellno1 = new TextBox();
		 cellno2 =  new TextBox();
		 Landlineno =new TextBox();	
		 EmailId = new TextBox();
	 }
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		
initilizewidget();
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder("Cell no.1",cellno1);
		FormField fdbcellno1= fbuilder.setRowSpan(0).setColSpan(3).build();
	
		fbuilder = new FormFieldBuilder("Cell no.2",cellno2);
		FormField fdbcellno2= fbuilder.setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("Landline number",Landlineno);
		FormField ftalandlineno= fbuilder.setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("Email Id",EmailId);
		FormField ftaemailid= fbuilder.setRowSpan(0).setColSpan(3).build();
		
		
		FormField[][] formfield = {
				{fdbcellno1},
				{fdbcellno2},
				{ftalandlineno},
				{ftaemailid},
				};


		this.fields=formfield;		
	}
	
	
	
	public TextBox getCellNumber1(){
		 return cellno1;
	}
	
 public void setCellNumber1(TextBox cellno1){
		
		this.cellno1=cellno1;
		
	}
	
	public TextBox getCellNumber2(){
	    return cellno2;
		
		}
	public void setCellNumber2(TextBox cellno2){
		
		this.cellno2=cellno2;
		
	}
	
	public TextBox getLandLine(){
	    return Landlineno;
		
		}
	public void setLandLine(TextBox Landlineno){
		
		this.Landlineno=Landlineno;
		
	}
	
	public TextBox getEmail(){
	    return EmailId;
		
		}
	public void setEmail(TextBox EmailId){
		
		this.EmailId=EmailId;
		
	}
	

	public void showcontactpopup() {
		// TODO Auto-generated method stub
		
	}

}
