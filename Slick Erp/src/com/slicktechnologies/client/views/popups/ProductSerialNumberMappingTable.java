package com.slicktechnologies.client.views.popups;

import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.inventory.GRNDetails;

public class ProductSerialNumberMappingTable extends SuperTable<ProductSerialNoMapping> {

	TextColumn<ProductSerialNoMapping> viewProdSerNoColumn;
	
	Column<ProductSerialNoMapping, String> editProdSerNoColumn;
	TextColumn<ProductSerialNoMapping> serNoCount ;
	public int serNoCnt = 0;
	boolean editable = false,selectable = false;
	Column<ProductSerialNoMapping, Boolean> checkColumn;
	Header<Boolean> selectAllHeader;
	public ProductSerialNumberMappingTable(){
		super();
	}
	
	public ProductSerialNumberMappingTable(boolean editable,boolean selectable){
		super();
		this.editable = editable;
		this.selectable = selectable;
		
	}
	
	@Override
	public void createTable() {
		if(selectable){
			addCheckBoxCellColumn();
		}
		addProdSerialNoCountColumn();
		editProdSerialNoColumn();
		addFieldUpdater();
		table.setWidth("auto");
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		createFieldUpdaterprodQtyColumn();
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		if(state){
			if(selectable){
				addCheckBoxCellColumn();
			}
			addProdSerialNoCountColumn();
			editProdSerialNoColumn();
			addFieldUpdater();
		}else{
			addProdSerialNoCountColumn();
			addProdSerialNoColumn();
		}
		table.setWidth("auto");
	}

	
	 
	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

	void addProdSerialNoCountColumn(){

		serNoCount=new TextColumn<ProductSerialNoMapping>() {
			@Override
			public String getValue(ProductSerialNoMapping object) {
				return ++serNoCnt+"";
			}
		};
		table.addColumn(serNoCount,"Sr No.");
		table.setColumnWidth(viewProdSerNoColumn, 10, Unit.PCT);
	}
	
	void  addProdSerialNoColumn(){

		viewProdSerNoColumn=new TextColumn<ProductSerialNoMapping>() {
			
			/*public String getCellStyleNames(Context context, ProductSerialNoMapping object) {
				if (!object.isAvailableStatus()){
					return "red";
				} else {
					return "black";
				}
			}*/
			@Override
			public String getValue(ProductSerialNoMapping object) {
				if(object.getProSerialNo() !=null){
					return object.getProSerialNo();
				}else{
					return "";
				}
				
			}
		};
		table.addColumn(viewProdSerNoColumn,"#Product Serail No");
		table.setColumnWidth(viewProdSerNoColumn, 90, Unit.PCT);
	
	}
	
	public void editProdSerialNoColumn() {
		EditTextCell editCell = new EditTextCell();
		editProdSerNoColumn = new Column<ProductSerialNoMapping, String>(editCell) {
			@Override
			public String getValue(ProductSerialNoMapping object) {
				
				if(object.getProSerialNo()!=null){
					
					return object.getProSerialNo();
				}else{
					return "";
				}
				
			}
		};
		table.addColumn(editProdSerNoColumn, "Product Serail No");
		table.setColumnWidth(editProdSerNoColumn,90,Unit.PCT);
	}
	
	protected void createFieldUpdaterprodQtyColumn() {
		editProdSerNoColumn.setFieldUpdater(new FieldUpdater<ProductSerialNoMapping, String>() {
					@Override
					public void update(int index, ProductSerialNoMapping object,String value) {
						try {
							
//							if (!object.isAvailableStatus())
								table.getRowElement(index).getStyle().setBackgroundColor("Red");
							
							System.out.println("get value 00 "+ value);
								object.setProSerialNo(value);
//								object.setStatus(true);
								serNoCnt = index;
						} catch (NumberFormatException e) {
						}
						table.redrawRow(index);
					}
				});
	}
	
	private void addCheckBoxCellColumn() {
		checkColumn=new Column<ProductSerialNoMapping, Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue(ProductSerialNoMapping object) {
				return object.isStatus();
			}
		};
		
		checkColumn.setFieldUpdater(new FieldUpdater<ProductSerialNoMapping, Boolean>() {
			@Override
			public void update(int index, ProductSerialNoMapping object, Boolean value) {
				object.setStatus(value);
			
				table.redrawRow(index);
				selectAllHeader.getValue();
				table.redrawHeaders();
			}
		});
		
		
		selectAllHeader =new Header<Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue() {
				if(getDataprovider().getList().size()!=0){
					
				}
				return false;
			}
		};
		
		
		selectAllHeader.setUpdater(new ValueUpdater<Boolean>() {
			@Override
			public void update(Boolean value) {
				List<ProductSerialNoMapping> list=getDataprovider().getList();
				for(ProductSerialNoMapping object:list){
					
					if(object.getProSerialNo().length()>0){
						object.setStatus(value);
					}
				}
				getDataprovider().setList(list);
				table.redraw();
				addColumnSorting();
			}
		});
		table.addColumn(checkColumn,selectAllHeader);
		table.setColumnWidth(checkColumn, 60, Unit.PX);
	}
}
