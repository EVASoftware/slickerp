package com.slicktechnologies.client.views.popups;

import java.util.HashMap;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
/**
 * Date 16-04-2018
 * @author Vijay Chougule
 * for loading locality on basis of city so created city popup 
 */
public class FetchLocalityPopUp extends PopupScreen {

	
	public DocumentsComposite city;  
	
	public FetchLocalityPopUp(){
		super();
		createGui();
		
	}

	private void initializeWidgets() {
		city = new DocumentsComposite(false);
	}

	
	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initializeWidgets();
		
		FormFieldBuilder fbuilder;
		
			
		fbuilder = new FormFieldBuilder();
		FormField fCityInfo = fbuilder.setlabel("City Information").widgetType(FieldType.Grouping).build();
		
		fbuilder = new FormFieldBuilder(" ",city);
		FormField ftbCity = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
	
		FormField [][]formfield = {
				{fCityInfo},
				{ftbCity},
		};
		this.fields=formfield;
		
	}
	
	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	public DocumentsComposite getCity() {
		return city;
	}

	public void setCity(DocumentsComposite city) {
		this.city = city;
	}

	
	
	
}
