package com.slicktechnologies.client.views.popups;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;

/**@Sheetal : 06-04-2022
 *   Des : Added new popup for resetting password **/

public class NewPasswordResetPopUp extends PopupScreen implements ValueChangeHandler<String> {

//	TextBox tbNewPassword;
//	TextBox tbConfirmPassword;
	PasswordTextBox tbNewPassword,tbConfirmPassword;

	Label lblLength,lblUpperCase,lblLowerCase, lblOneNumber, lblSymbol;
	public NewPasswordResetPopUp(){
		super(FormStyle.DEFAULT,false,false);
		createGui();
		getPopup().getElement().addClassName("setWidthpopup");

	}
	
	 public void initializeWidget(){
		 
		 tbNewPassword=new PasswordTextBox();
		 tbConfirmPassword=new PasswordTextBox();
		 
		 tbNewPassword.addValueChangeHandler(this);
		 tbConfirmPassword.addValueChangeHandler(this);
		 
		 lblLength = new Label();
		 lblLength.setText("* Password must be at least eight characters in length");
		
		 lblUpperCase = new Label();
		 lblUpperCase.setText("* Password must have atleast one uppercase character");
		 
		 lblLowerCase = new Label();
		 lblLowerCase.setText("* Password must have atleast one lowercase character");
		 
		 lblOneNumber = new Label();
		 lblOneNumber.setText("* Password must have atleast one number");

		 lblSymbol = new Label();
		 lblSymbol.setText("$ symbol not allowed in the password");

		
	 }
	@Override
	public void createScreen() {
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder();
		FormField fheading = fbuilder.setlabel("Reset Password").widgetType(FieldType.Grouping).setColSpan(6).build();
				
		fbuilder = new FormFieldBuilder("* New Password",  tbNewPassword);
		FormField ftbNewPassword = fbuilder.setMandatory(true).setRowSpan(0).setMandatoryMsg("Please enter New password").setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("* Confirm Password",  tbConfirmPassword);
		FormField ftbConfirmPassword = fbuilder.setMandatory(true).setMandatoryMsg("Please enter confirm password").setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",  lblLength);
		FormField flblLength = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",  lblUpperCase);
		FormField flblUpperCase = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",  lblLowerCase);
		FormField fflblLowerCase = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",  lblOneNumber);
		FormField flblOneNumber = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	
		fbuilder = new FormFieldBuilder("",  lblSymbol);
		FormField flblSymbol = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	
		
		FormField [][]formfield = {
				{fheading},
				{ftbNewPassword},
				{ftbConfirmPassword},
				{flblLength},
				{flblUpperCase},
				{fflblLowerCase},
				{flblOneNumber},
				{flblSymbol},
		};
		this.fields=formfield;
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {

		String password = getTbNewPassword().getValue();
			
			if(password.length() < 8){
			    showDialogMessage("Password must be at least eight characters in length");
				return;
				
			}
			
			String upperCaseChars = "(.*[A-Z].*)";
			if (!password.matches(upperCaseChars)){
				showDialogMessage("Password must have atleast one uppercase character");
				return;
				
			}
			 String lowerCaseChars = "(.*[a-z].*)";
			  if (!password.matches(lowerCaseChars)){
				 showDialogMessage("Password must have atleast one lowercase character"); 
				  return;
				  
			  }
			  String numbers = "(.*[0-9].*)";
			  if (!password.matches(numbers)){
				  showDialogMessage("Password must have atleast one number"); 
				return;
				 
			  }
			  
			  if (password.contains("$")){
				  showDialogMessage("$ Symbol can not allowed in the password"); 
				return;
				 
			  }

	}

	public PasswordTextBox getTbNewPassword() {
		return tbNewPassword;
	}

	public void setTbNewPassword(PasswordTextBox tbNewPassword) {
		this.tbNewPassword = tbNewPassword;
	}

	public PasswordTextBox getTbConfirmPassword() {
		return tbConfirmPassword;
	}

	public void setTbConfirmPassword(PasswordTextBox tbConfirmPassword) {
		this.tbConfirmPassword = tbConfirmPassword;
	}

	

	

}
