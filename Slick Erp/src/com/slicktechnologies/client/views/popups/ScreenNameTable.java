package com.slicktechnologies.client.views.popups;

import java.util.List;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.ScreenName;

public class ScreenNameTable extends SuperTable<ScreenName>{

	
	TextColumn<ScreenName> screenNameColumn;
	Column<ScreenName, Boolean> checkColumn;
	Header<Boolean> selectAllHeader;
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		addColumnCheckBox();
		addColumneScrenName();

	}

	private void addColumneScrenName() {
		// TODO Auto-generated method stub
		screenNameColumn = new TextColumn<ScreenName>() {
			
			@Override
			public String getValue(ScreenName object) {
				return object.getScreenName();
			}
		};
		
		table.addColumn(screenNameColumn,"Screen Name");
		table.setColumnWidth(screenNameColumn, 60, Unit.PX);
	
	}

	private void addColumnCheckBox() {
		// TODO Auto-generated method stub

		checkColumn=new Column<ScreenName, Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue(ScreenName object) {
				return object.isCheck();
			}
		};
		
		checkColumn.setFieldUpdater(new FieldUpdater<ScreenName, Boolean>() {
			@Override
			public void update(int index, ScreenName object, Boolean value) {
				object.setCheck(value);
				table.redrawRow(index);
				selectAllHeader.getValue();
				table.redrawHeaders();
			}
		});
		
		
		selectAllHeader =new Header<Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue() {
				if(getDataprovider().getList().size()!=0){
					
				}
				return false;
			}
		};
		
		
		selectAllHeader.setUpdater(new ValueUpdater<Boolean>() {
			@Override
			public void update(Boolean value) {
				List<ScreenName> list=getDataprovider().getList();
				for(ScreenName object:list){
					object.setCheck(value);
				}
				getDataprovider().setList(list);
				table.redraw();
				addColumnSorting();
			}
		});
		table.addColumn(checkColumn,selectAllHeader);
		table.setColumnWidth(checkColumn, 60, Unit.PX);
	
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
