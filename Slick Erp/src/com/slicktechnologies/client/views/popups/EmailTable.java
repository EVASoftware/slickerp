package com.slicktechnologies.client.views.popups;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;

public class EmailTable extends SuperTable<String>{

	TextColumn<String> emailidColumn;
	Column<String,String> deleteColumn;

	@Override
	public void createTable() {

		CreateColumnEmaildColumn();
		createColumndeleteColumn();

	}

	private void CreateColumnEmaildColumn() {
		
		emailidColumn=new TextColumn<String>()
		{
			@Override
			public String getValue(String emailId)
			{
				
				return emailId;
				
			}
		};
		table.addColumn(emailidColumn,"Email Id");
		table.setColumnWidth(emailidColumn, 100,Unit.PX);
	}

	
	protected void createColumndeleteColumn()
	{
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<String,String>(btnCell)
				{
				@Override
				public String getValue(String object)
				{
					return  "Delete" ;
				}
			};
		table.addColumn(deleteColumn,"Delete");
		table.setColumnWidth(deleteColumn, 100,Unit.PX);
		
		deleteColumn.setFieldUpdater(new FieldUpdater<String,String>()
				{
			@Override
			public void update(int index,String object,String value)
			{
				getDataprovider().getList().remove(object);
			}
		});
	}
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
