package com.slicktechnologies.client.views.popups;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.client.services.DocumentUploadService;
import com.slicktechnologies.client.services.DocumentUploadServiceAsync;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class SerailNumberRangePopup extends PopupScreen {

	
	TextBox tbserialnumberRange;
	DocumentUploadServiceAsync datauploadService = GWT.create(DocumentUploadService.class);

	public SerailNumberRangePopup(){
		super();
		createGui();
	}

	
	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		tbserialnumberRange = new TextBox();
		
		FormFieldBuilder fbuilder;

		fbuilder = new FormFieldBuilder();
		FormField fsearchFilters = fbuilder.setlabel("Serial Number Range Information").widgetType(FieldType.Grouping).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Serial Number Range",tbserialnumberRange);
		FormField olbQuotationId = fbuilder.setMandatory(true).setMandatoryMsg("Serial Number Range is mandatory!").setColSpan(0)
									.setRowSpan(0).build();
		FormField [][]formfield = {
				{fsearchFilters},
				{olbQuotationId},
		};
		this.fields=formfield;
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource()==this.getLblOk()){
			Company comp = new Company();
			if(validate()){
				showWaitSymbol();
				datauploadService.updateLockSealSerialNumberMaster(comp.getCompanyId(), tbserialnumberRange.getValue(), new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						hideWaitSymbol();
						hidePopUp();
					}

					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						showDialogMessage(result);
						hideWaitSymbol();
						hidePopUp();
					}
				});
			}
		}
		if(event.getSource()==this.getLblCancel()){
			hidePopUp();
		}
	}
}
