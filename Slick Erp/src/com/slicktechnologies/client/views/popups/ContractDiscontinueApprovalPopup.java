package com.slicktechnologies.client.views.popups;

import com.google.gwt.event.dom.client.ClickEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.Config;

public class ContractDiscontinueApprovalPopup extends PopupScreen{

	/*** Date 26-03-2019 by Vijay for NBHC CCPM contract discontinue Approval dropdown popup ***/
	ObjectListBox<Config> olbApprovarName;
	ObjectListBox<Config> olbCancellationRemark;
	
	public ContractDiscontinueApprovalPopup(){
		super();
		createGui();
	}
	
	@Override
	public void createScreen() {

			olbApprovarName = new ObjectListBox<Config>();
			olbApprovarName.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CONTRACTDISCOUNTINUED, "Zonal Head");
			
			olbCancellationRemark = new ObjectListBox<Config>();
			AppUtility.MakeLiveConfig(olbCancellationRemark, Screen.CANCELLATIONREMARK);

			FormFieldBuilder fbuilder;

			fbuilder = new FormFieldBuilder();
			FormField fsearchFilters = fbuilder.setlabel("Approver Information").widgetType(FieldType.Grouping).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("*Remark",olbCancellationRemark);
			FormField folbCancellationRemark = fbuilder.setMandatory(true).setMandatoryMsg("Remark Name is mandatory!").setColSpan(0)
										.setRowSpan(0).build();
			
			fbuilder = new FormFieldBuilder("* Approver Name",olbApprovarName);
			FormField ffolbApprovarName = fbuilder.setMandatory(true).setMandatoryMsg("Approver Name is mandatory!").setColSpan(0)
										.setRowSpan(0).build();
			
			FormField [][]formfield = {
					{fsearchFilters},
					{folbCancellationRemark},
					{ffolbApprovarName}
			};
			this.fields=formfield;
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}
	
	public ObjectListBox<Config> getOlbApprovarName() {
		return olbApprovarName;
	}


	public void setOlbApprovarName(ObjectListBox<Config> olbApprovarName) {
		this.olbApprovarName = olbApprovarName;
	}

	public ObjectListBox<Config> getOlbCancellationRemark() {
		return olbCancellationRemark;
	}

	public void setOlbCancellationRemark(ObjectListBox<Config> olbCancellationRemark) {
		this.olbCancellationRemark = olbCancellationRemark;
	}
	
	
	
}
