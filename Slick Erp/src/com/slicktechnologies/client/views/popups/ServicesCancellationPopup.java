package com.slicktechnologies.client.views.popups;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Text;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;

public class ServicesCancellationPopup extends PopupScreen implements SelectionHandler<Suggestion>{

	public PersonInfoComposite personInfo;
	ObjectListBox<CustomerBranchDetails> olbCustomerBranch;
	TextBox tbcustomerbranchname;
	
	 final static GenricServiceAsync async=GWT.create(GenricService.class);
	 List<CustomerBranchDetails> customerBranchList=new ArrayList<CustomerBranchDetails>();
	 
	 public CustomerBranchListTable customerbranchtable;
	 Button btnAdd;
	 
	 ObjectListBox<Config> olbCancellationRemark;

	 final CommonServiceAsync commonserviceasync=GWT.create(CommonService.class);

	public ServicesCancellationPopup(){
		super();
		createGui();
	}
	
	public void initWidget(){
		
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);

		olbCustomerBranch=new ObjectListBox<CustomerBranchDetails>();
		olbCustomerBranch.addItem("--SELECT--");
		
		tbcustomerbranchname = new TextBox();
		
		personInfo.getId().addSelectionHandler(this);
		personInfo.getName().addSelectionHandler(this);
		personInfo.getPhone().addSelectionHandler(this);
		
		customerbranchtable = new CustomerBranchListTable();
		customerbranchtable.connectToLocal();
		
		btnAdd = new Button("Add");
		btnAdd.addClickHandler(this);
		
		olbCancellationRemark = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbCancellationRemark, Screen.CANCELLATIONREMARK);
		
	}

	@Override
	public void createScreen() {
		initWidget();
		
		FormFieldBuilder builder;

		builder = new FormFieldBuilder("",personInfo);
		FormField fPersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
	
		builder = new FormFieldBuilder("Customer Branch",olbCustomerBranch);
		FormField folbCustomerBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		

		builder = new FormFieldBuilder("Customer Branch",tbcustomerbranchname);
		FormField ftbcustomerbranchname= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		builder = new FormFieldBuilder();
		FormField fgroupingcustomerbranch=builder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		builder = new FormFieldBuilder("",customerbranchtable.getTable());
		FormField fcustomerbranchtable= builder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
	
		builder = new FormFieldBuilder("",btnAdd);
		FormField fbtnAdd= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Reason For Cancellation", this.olbCancellationRemark);
		FormField folbCancellationRemark = builder.setMandatory(false).setColSpan(0).setRowSpan(0).build();
	
		FormField [][]formfield = {
				{fgroupingcustomerbranch},
				{fPersonInfo},
				{folbCustomerBranch,ftbcustomerbranchname,fbtnAdd},
				{fcustomerbranchtable},
				{folbCancellationRemark}
		};
		this.fields=formfield;
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {

		if(event.getSource().equals(personInfo.getId())||event.getSource().equals(personInfo.getName())||event.getSource().equals(personInfo.getPhone())){
			olbCustomerBranch.clear();
			loadCustomerBranch(Integer.parseInt(personInfo.getId().getValue().trim()), null);
			
		}
	}
	
	public void loadCustomerBranch(int custId,final String custBranch){
		customerBranchList=new ArrayList<CustomerBranchDetails>();
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(custId);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerBranchDetails());

		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				olbCustomerBranch.clear();
				olbCustomerBranch.addItem("--SELECT--");
				showDialogMessage("failed." + caught);
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<CustomerBranchDetails> custBranchList=new ArrayList<CustomerBranchDetails>();
				if(result.size()!=0){
					for(SuperModel model:result){
						CustomerBranchDetails obj=(CustomerBranchDetails) model;
						custBranchList.add(obj);
						customerBranchList.add(obj);
					}
				}
				
				if(custBranchList.size()!=0){
					olbCustomerBranch.setListItems(custBranchList);
					if(custBranch!=null){
						olbCustomerBranch.setValue(custBranch);
						if(olbCustomerBranch.getValue()!=null){
						
						}
					}else{
						olbCustomerBranch.setEnabled(true);
					}
				}else{
					olbCustomerBranch.addItem("--SELECT--");
				}
				
				
			}
		});
	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		
		if(event.getSource().equals(btnAdd)){
			if(personInfo.getValue()==null || personInfo.getValue().equals("")){
				showDialogMessage("Please add customer information first");
				return;
			}
			if(olbCustomerBranch.getSelectedIndex()!=0 && !tbcustomerbranchname.getValue().equals("")){
				showDialogMessage("Please add customer branch either from dropdown or from textbox");
				return;
			}
			if(olbCustomerBranch.getSelectedIndex()!=0){
				CustomerBranchDetails customerbranch = olbCustomerBranch.getSelectedItem();
				customerbranchtable.getDataprovider().getList().add(customerbranch);
				olbCustomerBranch.setSelectedIndex(0);
			}
			else if(!tbcustomerbranchname.getValue().equals("")){
				if(tbcustomerbranchname.getValue().contains(",")){
					String[] branches = tbcustomerbranchname.getValue().trim().split("\\,");
					for(int i=0;i<branches.length;i++){
						CustomerBranchDetails customerbranch = new CustomerBranchDetails();
						customerbranch.setBusinessUnitName(branches[i].trim());
						customerbranchtable.getDataprovider().getList().add(customerbranch);
					}
				}
				else{
					CustomerBranchDetails customerbranch = new CustomerBranchDetails();
					customerbranch.setBusinessUnitName(tbcustomerbranchname.getValue());
					customerbranchtable.getDataprovider().getList().add(customerbranch);
				}
				tbcustomerbranchname.setValue("");

			}
		}
		
		if(event.getSource().equals(this.getLblOk())){
			if(customerbranchtable.getDataprovider().getList().size()==0){
				showDialogMessage("Please add customer branch!");
			}
			else if(olbCancellationRemark.getSelectedIndex()==0){
				showDialogMessage("Please select cancelation remark!");
			}
			else if(personInfo.getValue()==null || personInfo.getValue().equals("")){
				showDialogMessage("Please add customer information");
			}
			else{
				ArrayList<CustomerBranchDetails> customerbranchlist = new ArrayList<CustomerBranchDetails>();
				List<CustomerBranchDetails> list = customerbranchtable.getDataprovider().getList();
				customerbranchlist.addAll(list);
				Company comp = new Company();
				
				showWaitSymbol();
				commonserviceasync.cancelCustomerBranchServices(customerbranchlist, comp.getCompanyId(), olbCancellationRemark.getValue(olbCancellationRemark.getSelectedIndex()), 
						LoginPresenter.loggedInUser,personInfo.getValue().getCount(), new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						hideWaitSymbol();
					}

					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						hideWaitSymbol();
						if(!result.equals(""))
						showDialogMessage(result);
						hidePopUp();
					}
				});
			}
		}
		if(event.getSource().equals(this.getLblCancel())){
			hidePopUp();
		}

	}

}
