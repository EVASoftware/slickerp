package com.slicktechnologies.client.views.popups;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;

public class DatePopPup extends PopupScreen{


	DateBox dbfromDate,dbToDate;
	ObjectListBox<Branch> olbBranch;
	public DatePopPup() {
		super();
		createGui();
	}

	@Override
	public void createScreen() {
		initializeWidgets();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fsearchFilters = fbuilder.setlabel("Date filters").widgetType(FieldType.Grouping).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("* From Date",  dbfromDate);
		FormField fdbfromDate = fbuilder.setMandatory(true).setMandatoryMsg("From Date is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* To Date",  dbToDate);
		FormField fdbToDate = fbuilder.setMandatory(true).setMandatoryMsg("To Date is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Branch",  olbBranch);
		FormField folbBranch = fbuilder.setMandatory(true).setMandatoryMsg("Branch is mandatory!").setRowSpan(0).setColSpan(0).build();
		
	
		FormField [][]formfield = {
				{fsearchFilters},
				{fdbfromDate,fdbToDate,folbBranch},
		};
		this.fields=formfield;
		
	}

	private void initializeWidgets() {
		dbfromDate = new DateBoxWithYearSelector();
		dbToDate = new DateBoxWithYearSelector();
		olbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
	}

	public DateBox getDbfromDate() {
		return dbfromDate;
	}

	public void setDbfromDate(DateBox dbfromDate) {
		this.dbfromDate = dbfromDate;
	}

	public DateBox getDbToDate() {
		return dbToDate;
	}

	public void setDbToDate(DateBox dbToDate) {
		this.dbToDate = dbToDate;
	}

	public ObjectListBox<Branch> getOlbBranch() {
		return olbBranch;
	}

	public void setOlbBranch(ObjectListBox<Branch> olbBranch) {
		this.olbBranch = olbBranch;
	}

	@Override
	public boolean validate() {
		boolean validateflag = super.validate();
		if(validateflag==false){
			return false;
		}
		return true;
	}
	
	


}
