package com.slicktechnologies.client.views.emailtemplate;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class EmailTemplateForm extends FormTableScreen<EmailTemplate> implements ChangeHandler, ClickHandler{

	TextBox tbtemplateId, tbtemplateName;
	TextArea taemailSub, taEmailBody;
	CheckBox cbStatus,cbpdfattachment;
	ObjectListBox<String> oblPlaceholders;

	EmailTemplate emailTemplateobj;
	
	public EmailTemplateForm  (SuperTable<EmailTemplate> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();

	}
	
	private void initalizeWidget() {
		tbtemplateId = new TextBox();
		tbtemplateId.setEnabled(false);
		
		tbtemplateName = new TextBox();
		taemailSub = new TextArea();
		taemailSub.setName("Active");
		taemailSub.addClickHandler(this);
		
		taEmailBody = new TextArea();
		taEmailBody.setName("Active");
		taEmailBody.addClickHandler(this);
		

		cbpdfattachment = new CheckBox();
		cbStatus = new CheckBox();
		cbStatus.setValue(true);//Ashwini Patil Date:15-02-2023
		
		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;

		oblPlaceholders = new ObjectListBox<String>();
		oblPlaceholders.addItem("--SELECT--");
		oblPlaceholders.addItem("ID");
		oblPlaceholders.addItem("Document Date");
		if(screenName.equals(Screen.LEAD) || screenName.equals(Screen.QUOTATION) || screenName.equals(Screen.CONTRACT)
				|| screenName.equals(Screen.SALESQUOTATION) || screenName.equals(Screen.SALESORDER) || screenName.equals(Screen.CONTRACTRENEWAL)){
			oblPlaceholders.addItem("Customer Name");
		}
		else if(screenName.equals(Screen.BILLINGDETAILS)  || screenName.equals(Screen.INVOICEDETAILS) || screenName.equals(Screen.PAYMENTDETAILS) ){
			oblPlaceholders.addItem("BP Name");
		}
		oblPlaceholders.addItem("Reference Number");
		oblPlaceholders.addItem("Reference Date");
		oblPlaceholders.addItem("Digital Payment Link");
		oblPlaceholders.addItem("Feedback_Link");
		oblPlaceholders.addItem("Contract_Renewal_Days_Left");
		
		oblPlaceholders.getElement().getStyle().setWidth(200, Unit.PX);
		
		oblPlaceholders.addChangeHandler(this);
	}

	@Override
	public void createScreen() {
		
		initalizeWidget();
		
		
		this.processlevelBarNames=new String[]{"New"};
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingEmailInformation=fbuilder.setlabel("Email Template Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Template ID",tbtemplateId);
		FormField tbtemplateId = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Template Name",tbtemplateName);
		FormField tbtemplateName= fbuilder.setMandatory(true).setMandatoryMsg("Template Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",cbStatus);
		FormField fchkbxStatus = fbuilder.setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Email Subject",taemailSub);
		FormField taemailSub = fbuilder.setMandatory(true).setMandatoryMsg("Email Subject is Mandatory!").setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Email Body",taEmailBody);
		FormField taEmailBody = fbuilder.setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Pdf Attachment",cbpdfattachment);
		FormField pdfattachment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder();
		FormField fblankgroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Insert Placeholders",oblPlaceholders);
		FormField foblPlaceholders=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField[][] formfield = { 
				   {fgroupingEmailInformation},
				  {tbtemplateId,tbtemplateName,pdfattachment,foblPlaceholders,fchkbxStatus},
				  {taemailSub},
				  {taEmailBody},
				  };

				this.fields=formfield;		
		
	}


	@Override
	public void updateModel(EmailTemplate model) 
	{
		if(tbtemplateName.getValue()!=null)
			model.setTemplateName(tbtemplateName.getValue());
		if(taemailSub.getValue()!=null)
			model.setSubject(taemailSub.getValue());
		if(taEmailBody.getValue()!=null)
			model.setEmailBody(taEmailBody.getValue());
		if(cbpdfattachment.getValue()!=null)
			model.setPdfattachment(cbpdfattachment.getValue());
		
		model.setTemplateStatus(cbStatus.getValue());
		
		manageGlobalList(model);
		
		emailTemplateobj=model;
		presenter.setModel(model);
	}
	
	
	@Override
	public void updateView(EmailTemplate view) 
	{
		emailTemplateobj=view;
		
		if(view.getTemplateName()!=null)
			tbtemplateName.setValue(view.getTemplateName());
		if(view.getSubject()!=null)
			taemailSub.setValue(view.getSubject());
		if(view.getEmailBody()!=null)
			taEmailBody.setValue(view.getEmailBody());
		
		tbtemplateId.setValue(view.getCount()+"");
		cbStatus.setValue(view.isTemplateStatus());
		cbpdfattachment.setValue(view.isPdfattachment());
		
		presenter.setModel(view);

	}	
	
	
	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.EMAILTEMPLATE,LoginPresenter.currentModule.trim());
	}

	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		tbtemplateId.setValue(count+"");	
	}

	
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		tbtemplateId.setEnabled(false);
	}

	@Override
	public boolean validate() {
		boolean superRes=super.validate();
	    return superRes;
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		

		SuperModel model=new SmsTemplate();
		model=emailTemplateobj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.EMAILTEMPLATE, emailTemplateobj.getCount(), null,null,null, false, model, null);
	
	}

	
	@Override
	public void setToNewState() {
		super.setToNewState();
		
		System.out.println("Inside NEW method");
		if(emailTemplateobj!=null){
			SuperModel model=new SmsTemplate();
			model=emailTemplateobj;
			AppUtility.addDocumentToHistoryTable(AppConstants.SETTINGSMODULE,AppConstants.EMAILTEMPLATE, emailTemplateobj.getCount(), null,null,null, false, model, null);
		
		}
	}

	@Override
	public void applyStyle() {
		super.applyStyle();
		taEmailBody.getElement().getStyle().setHeight(350, Unit.PX);
		taemailSub.getElement().getStyle().setHeight(21, Unit.PX);

	}
	

	public void manageGlobalList(EmailTemplate emailTemplateModel)
	{
		ScreeenState scrState=AppMemory.getAppMemory().currentState;
		
		if(scrState.equals(ScreeenState.NEW))
		{
			LoginPresenter.globalEmailTemplatelist.add(emailTemplateModel);
		}
		if(scrState.equals(ScreeenState.EDIT)){
			for(int i=0;i<LoginPresenter.globalEmailTemplatelist.size();i++)
			{
				if(LoginPresenter.globalEmailTemplatelist.get(i).getId().equals(emailTemplateModel.getId()))
				{
					LoginPresenter.globalEmailTemplatelist.add(emailTemplateModel);
					LoginPresenter.globalEmailTemplatelist.remove(i);
				}
			}
		}
		
	}

	@Override
	public void onChange(ChangeEvent event) {

		if(event.getSource().equals(oblPlaceholders)){
			if(oblPlaceholders.getSelectedIndex()!=0){
				reactonInsertPlaceholders();
			}
		}
	}

	private void reactonInsertPlaceholders() {

		if(taEmailBody.getName().equals("Active")){
			
			StringBuffer strvalue = new StringBuffer(taEmailBody.getValue());
			
			if(oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex()).equals("Digital Payment Link")){
				
				String bodyIntro = "<Start_LandingPage_Text> </End_LandingPage_Text>";
				String digitalPaymentlink = bodyIntro+"\n"+ "{"+oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex())+"}";
				strvalue.insert(taEmailBody.getCursorPos(), digitalPaymentlink);
			}
			else{
				strvalue.insert(taEmailBody.getCursorPos(), "{"+oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex())+"}");
			}
			taEmailBody.setValue(strvalue.toString());
			
		}
		else if(taemailSub.getName().equals("Active")){
			StringBuffer strvalue = new StringBuffer(taemailSub.getValue());
			strvalue.insert(taemailSub.getCursorPos(), "{"+oblPlaceholders.getValue(oblPlaceholders.getSelectedIndex())+"}");
			taemailSub.setValue(strvalue.toString());
		}


	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
		if(event.getSource().equals(this.taEmailBody)){
			taEmailBody.setName("Active");
		}
		else{
			if(taEmailBody!=null){
				taEmailBody.setName("InActive");
			}
		}
		
		if(event.getSource().equals(this.taemailSub)){
			taemailSub.setName("Active");
		}
		else{
			if(taemailSub!=null){
				taemailSub.setName("InActive");
			}
		}
	}
	
}
