package com.slicktechnologies.client.views.emailtemplate;

import java.util.Vector;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.smstemplate.SmsTemplateForm;
import com.slicktechnologies.client.views.smstemplate.SmsTemplatePresenter;
import com.slicktechnologies.client.views.smstemplate.SmsTemplateTable;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class EmailTemplatePresenter extends FormTableScreenPresenter<EmailTemplate>{

	EmailTemplateForm form;
	
	public EmailTemplatePresenter(FormTableScreen<EmailTemplate> view,
			EmailTemplate model) {
		super(view, model);
		form=(EmailTemplateForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getEmailQuery());
		form.setPresenter(this);
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.EMAILTEMPLATE,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	


	public static EmailTemplateForm initalize() {
		
		EmailTemplateTable gentableScreen=new EmailTemplateTable();
		
		EmailTemplateForm  form=new  EmailTemplateForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();

		EmailTemplatePresenter  presenter=new  EmailTemplatePresenter(form,new EmailTemplate());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
		
	}


	/**
	 * Method template to set the processBar events
	 */

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {

		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals("New"))
		{
			form.setToNewState();
			initalize();
		}	
	}


	@Override
	protected void makeNewModel() {
		model=new EmailTemplate();

	}
	
	public void setModel(EmailTemplate entity)
	{
		model=entity;
	}


	private MyQuerry getEmailQuery() {
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new EmailTemplate());
		return quer;
	}
	
	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

}
