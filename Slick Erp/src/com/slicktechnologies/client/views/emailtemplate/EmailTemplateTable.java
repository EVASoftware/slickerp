package com.slicktechnologies.client.views.emailtemplate;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.email.EmailTemplate;

public class EmailTemplateTable extends SuperTable<EmailTemplate>{

	
	TextColumn<EmailTemplate> getcolumnTemplateId;
	TextColumn<EmailTemplate> getcolumnTemplateName;
	TextColumn<EmailTemplate> getcolumnSubject;
	TextColumn<EmailTemplate> getcolumnBody;
	TextColumn<EmailTemplate> getcolumnTemplateStatus;
	TextColumn<EmailTemplate> getcolumnAttachment;

	public EmailTemplateTable(){
		super();
	}
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		createColumnTemplateId();
		createColumnTemplateName();
		createColumnSubject();
		createColumnBody();
		createColumnTemplateStatus();
		createColumnAttachment();
	}

	private void createColumnAttachment() {
		getcolumnAttachment = new TextColumn<EmailTemplate>() {
			
			@Override
			public String getValue(EmailTemplate object) {
				if(object.isTemplateStatus()){
					return "Active";
				}
				else{
					return "InActive";
				}
			}
		};
		table.addColumn(getcolumnAttachment, "Attachment");
		getcolumnAttachment.setSortable(true);
		table.setColumnWidth(getcolumnAttachment, 100, Unit.PX);	
	}

	private void createColumnTemplateStatus() {
		getcolumnTemplateStatus = new TextColumn<EmailTemplate>() {
			
			@Override
			public String getValue(EmailTemplate object) {
				if(object.isTemplateStatus()){
					return "Active";
				}
				else{
					return "InActive";
				}
			}
		};
		table.addColumn(getcolumnTemplateStatus, "Status");
		getcolumnTemplateStatus.setSortable(true);
		table.setColumnWidth(getcolumnTemplateStatus, 100, Unit.PX);	
	}

	private void createColumnBody() {
		getcolumnBody = new TextColumn<EmailTemplate>() {
			
			@Override
			public String getValue(EmailTemplate object) {
				if(object.getEmailBody()!=null){
					return object.getEmailBody();
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(getcolumnBody, "Email Body");
		getcolumnBody.setSortable(true);
		table.setColumnWidth(getcolumnBody, 100, Unit.PX);	
	}

	private void createColumnSubject() {

		getcolumnSubject = new TextColumn<EmailTemplate>() {
			
			@Override
			public String getValue(EmailTemplate object) {
				if(object.getSubject()!=null){
					return object.getSubject();
				}
				else{
					return "";
				}
			}
		};
		
		table.addColumn(getcolumnSubject, "Subject");
		getcolumnSubject.setSortable(true);
		table.setColumnWidth(getcolumnSubject, 100, Unit.PX);	
	
	}

	private void createColumnTemplateId() {
		getcolumnTemplateId = new TextColumn<EmailTemplate>() {
			
			@Override
			public String getValue(EmailTemplate object) {
				// TODO Auto-generated method stub
				return object.getCount()+"";
			}
		};
		
		table.addColumn(getcolumnTemplateId, "ID");
		getcolumnTemplateId.setSortable(true);
		table.setColumnWidth(getcolumnTemplateId, 100, Unit.PX);		
	}

	private void createColumnTemplateName() {
		getcolumnTemplateName = new TextColumn<EmailTemplate>() {
			
			@Override
			public String getValue(EmailTemplate object) {
				if(object.getTemplateName()!=null){
					return object.getTemplateName();
				}
				else{
					return "";
				}
			}
		};
		
		table.addColumn(getcolumnTemplateName, "Template Name");
		getcolumnTemplateName.setSortable(true);
		table.setColumnWidth(getcolumnTemplateName, 100, Unit.PX);	
	}

	public void addColumnSorting(){
		addSortingCount();
		addSortingTemplateName();
		addSortingSubject();
		addSortingTemplateBody();
		addSortingTemplateStatus();
		addSortinggeAttachmentStatus();	
	}
	
	private void addSortinggeAttachmentStatus() {
		List<EmailTemplate> list = getDataprovider().getList();
		columnSort = new ListHandler<EmailTemplate>(list);
		columnSort.setComparator(getcolumnAttachment, new Comparator<EmailTemplate>() {
			
			@Override
			public int compare(EmailTemplate e1, EmailTemplate e2) {

				if(e1!=null && e2!=null)
				{
					if(e1.isPdfattachment()==e2.isPdfattachment()){
						return 0;
					}
					else{
						return -1;
					}
				}
				else{
					return 0;
				}
				
			}
		});
		
		table.addColumnSortHandler(columnSort);
	
	
	}

	private void addSortingTemplateStatus() {

		List<EmailTemplate> list = getDataprovider().getList();
		columnSort = new ListHandler<EmailTemplate>(list);
		columnSort.setComparator(getcolumnTemplateStatus, new Comparator<EmailTemplate>() {
			
			@Override
			public int compare(EmailTemplate e1, EmailTemplate e2) {

				if(e1!=null && e2!=null)
				{
					if(e1.isTemplateStatus()==e2.isTemplateStatus()){
						return 0;
					}
					else{
						return -1;
					}
				}
				else{
					return 0;
				}
				
			}
		});
		
		table.addColumnSortHandler(columnSort);
	
	}

	private void addSortingTemplateBody() {

		List<EmailTemplate> list = getDataprovider().getList();
		columnSort = new ListHandler<EmailTemplate>(list);
		columnSort.setComparator(getcolumnBody, new Comparator<EmailTemplate>() {

			@Override
			public int compare(EmailTemplate e1, EmailTemplate e2) {
				
				if(e1!=null && e2!=null)
					{
						if( e1.getEmailBody()!=null && e2.getEmailBody()!=null){
							return e1.getEmailBody().compareTo(e2.getEmailBody());}
					}
					else{
						return 0;}
					return 0;
			}
		});
		
		table.addColumnSortHandler(columnSort); 
	
	
	}

	private void addSortingSubject() {

		List<EmailTemplate> list = getDataprovider().getList();
		columnSort = new ListHandler<EmailTemplate>(list);
		columnSort.setComparator(getcolumnSubject, new Comparator<EmailTemplate>() {

			@Override
			public int compare(EmailTemplate e1, EmailTemplate e2) {
				
				if(e1!=null && e2!=null)
					{
						if( e1.getSubject()!=null && e2.getSubject()!=null){
							return e1.getSubject().compareTo(e2.getSubject());}
					}
					else{
						return 0;}
					return 0;
			}
		});
		
		table.addColumnSortHandler(columnSort); 
	
	}

	private void addSortingTemplateName() {
		List<EmailTemplate> list = getDataprovider().getList();
		columnSort = new ListHandler<EmailTemplate>(list);
		columnSort.setComparator(getcolumnTemplateName, new Comparator<EmailTemplate>() {

			@Override
			public int compare(EmailTemplate e1, EmailTemplate e2) {
				
				if(e1!=null && e2!=null)
					{
						if( e1.getTemplateName()!=null && e2.getTemplateName()!=null){
							return e1.getTemplateName().compareTo(e2.getTemplateName());}
					}
					else{
						return 0;}
					return 0;
			}
		});
		
		table.addColumnSortHandler(columnSort); 
	}

	private void addSortingCount() {


		List<EmailTemplate> list=getDataprovider().getList();
		columnSort=new ListHandler<EmailTemplate>(list);
		columnSort.setComparator(getcolumnTemplateId, new Comparator<EmailTemplate>()
				{
			@Override
			public int compare(EmailTemplate e1,EmailTemplate e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
			
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
