package com.slicktechnologies.client.views.financialcalendar;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.FinancialCalender;

public class FinancialCalendarTable extends SuperTable<FinancialCalender>{

	TextColumn<FinancialCalender> getcolumnCalendarName;
	TextColumn<FinancialCalender> getcolumnStartDate;
	TextColumn<FinancialCalender> getcolumnEndDate;
	TextColumn<FinancialCalender> getcolumnStatus;
	
	@Override
	public void createTable() {

		addcolumnCalendarName();
		addcolumnStartDate();
		addcolumnEndDate();
		addcolumnStatus();
		
	}
	
	
	

	private void addcolumnCalendarName() {

		getcolumnCalendarName = new TextColumn<FinancialCalender>() {
			
			@Override
			public String getValue(FinancialCalender object) {
				return object.getCalendarName();
			}
		};
		table.addColumn(getcolumnCalendarName,"Calendar Name");
		table.setColumnWidth(getcolumnCalendarName, 90,Unit.PX);
	}




	private void addcolumnStartDate() {
		getcolumnStartDate = new TextColumn<FinancialCalender>() {
			
			@Override
			public String getValue(FinancialCalender object) {
				return AppUtility.parseDate(object.getStartDate());
			}
		};
		table.addColumn(getcolumnStartDate,"Start Date");
		table.setColumnWidth(getcolumnStartDate, 90,Unit.PX);
	}




	private void addcolumnEndDate() {
		getcolumnEndDate = new TextColumn<FinancialCalender>() {
			
			@Override
			public String getValue(FinancialCalender object) {
				return  AppUtility.parseDate(object.getEndDate());
			}
		};
		table.addColumn(getcolumnEndDate,"End Date");
		table.setColumnWidth(getcolumnEndDate, 90,Unit.PX);
	}

	private void addcolumnStatus() {
		getcolumnStatus = new TextColumn<FinancialCalender>() {
			
			@Override
			public String getValue(FinancialCalender object) {
				if(object.isStatus()==true)
					return "Active";
					else return "InActive";
			}
		};
		table.addColumn(getcolumnStatus,"Status");
		table.setColumnWidth(getcolumnStatus, 90,Unit.PX);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
