package com.slicktechnologies.client.views.financialcalendar;

import java.util.Date;
import java.util.List;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.FinancialCalender;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class FinancialCalendarForm extends FormTableScreen<FinancialCalender> implements ValueChangeHandler<Date>{

	DateBox dbstartdate,dbenddate;
	TextBox tbcalendarname,tbblank;
	TextBox tbcalendarid;
	FinancialCalender financialCal;
	CheckBox chkStatus;
	 
	public FinancialCalendarForm(SuperTable<FinancialCalender> table, int mode, boolean captionmode){
		super(table, mode, captionmode);
		createGui();
	}
	
	 private void initalizeWidget() {

			dbstartdate = new DateBoxWithYearSelector();
			dbstartdate.addValueChangeHandler(this);
			dbenddate = new DateBoxWithYearSelector();
			dbenddate.addValueChangeHandler(this);
			tbcalendarname = new TextBox();
			tbcalendarid =new TextBox();
			tbcalendarid.setEnabled(false);
			
			chkStatus = new CheckBox();
			chkStatus.setValue(true);
			
			
		}


	

	@Override
	public void createScreen() {

		initalizeWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingCalendarInformation=fbuilder.setlabel("Physical Year Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Calendar ID",tbcalendarid);
		FormField fibcalendarid= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Calendar Name",tbcalendarname);
		FormField ftbcalendarname= fbuilder.setMandatory(true).setMandatoryMsg("Calendar Name is Manatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Satrt Date",dbstartdate);
		FormField fdbstartdate= fbuilder.setMandatory(true).setMandatoryMsg("Start Date is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* End Date",dbenddate);
		FormField dbenddate= fbuilder.setMandatory(true).setMandatoryMsg("End Date is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("",tbblank);
		FormField ftbblank= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status",chkStatus);
		FormField fchkStatus = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		FormField[][] formfield = {{fgroupingCalendarInformation},
				  {fibcalendarid,ftbcalendarname,fchkStatus,ftbblank},
				  {fdbstartdate,dbenddate,ftbblank,ftbblank},
				  };

				  this.fields=formfield;

	}

	
	public void updateModel(FinancialCalender model) {
		// TODO Auto-generated method stub

		if(tbcalendarname.getValue()!=null)
			model.setCalendarName(tbcalendarname.getValue());
		if(dbstartdate.getValue()!=null)
			model.setStartDate(dbstartdate.getValue());
		if(dbenddate.getValue()!=null)
			model.setEndDate(dbenddate.getValue());
		if(chkStatus.getValue()!=null)
			model.setStatus(chkStatus.getValue());
			
		financialCal=model;
			presenter.setModel(model);
	}

	@Override
	public void updateView(FinancialCalender view) {
		// TODO Auto-generated method stub
		
		financialCal=view;
		if(view.getCalendarName()!=null)
			tbcalendarname.setValue(view.getCalendarName());
		if(view.getStartDate()!=null)
			dbstartdate.setValue(view.getStartDate());
		if(view.getEndDate()!=null)
			dbenddate.setValue(view.getEndDate());
		
		chkStatus.setValue(view.isStatus());
	
		tbcalendarid.setValue(view.getCount()+"");
		
		presenter.setModel(view);
		
	}
	
	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.FINANCIALCALENDAR,LoginPresenter.currentModule.trim());
	}

	
	
	
	@Override
	public void setToNewState() {
		// TODO Auto-generated method stub
		super.setToNewState();
		
		System.out.println("Inside NEW method");
		if(financialCal!=null){
			SuperModel model=new SmsConfiguration();
			model=financialCal;
			AppUtility.addDocumentToHistoryTable(AppConstants.ACCOUNTMODULE,AppConstants.FINANCIALCALENDAR, financialCal.getCount(), null,null,null, false, model, null);
		
		}
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		
		SuperModel model=new FinancialCalender();
		model=financialCal;
		AppUtility.addDocumentToHistoryTable(AppConstants.ACCOUNTMODULE,AppConstants.FINANCIALCALENDAR, financialCal.getCount(), null,null,null, false, model, null);
	
	}
	
	
	

	@Override
	public boolean validate() {
		List<FinancialCalender> list = getSupertable().getDataprovider().getList();
		boolean flag = false;
		for(int i=0;i<list.size();i++)
		{
//			System.out.println(" iddddd "+this.tbcalendarid.getValue());
//			if(this.tbcalendarid.getValue()==null || this.tbcalendarid.equals("")){
//			
//			if(list.get(i).isStatus()==true)
//			{
//				System.out.println("1111111");
//				int calId = Integer.parseInt(this.tbcalendarid.getValue());
//				if(calId != list.get(i).getCount()){
//					showDialogMessage("Only one Calendar can Active");
//					return false;
//				}
//					
//			}
//			
//			}else{
//				
//				if(list.get(i).isStatus()==true)
//				{
//					System.out.println("1111111");
//					
//					if(this.chkStatus.getValue()==true){
//					showDialogMessage("Only one Calendar can Active");
//					return false;
//					}
//				}
//			}
//			if(list.get(i).getCount()!=count)
			
			int count;
			if(this.tbcalendarid.getValue().equals("")||this.tbcalendarid.getValue()==null){
				count = 0;
			}else{
				count = Integer.parseInt(this.tbcalendarid.getValue());
			}
				System.out.println(" hi vijay ===="+this.tbcalendarid.getValue());
			if(count==0){
				System.out.println("for new mode");
				if(list.get(i).isStatus()==true)
					{
						System.out.println("1111111");
						
						if(this.chkStatus.getValue()==true){
						showDialogMessage("Only one Calendar can Active");
						return false;
						}
					}
			}else{
				System.out.println(" count ==="+count);
				System.out.println("form db ==="+list.get(i).getCount());
				if(list.get(i).getCount()!=count){
					System.out.println("for edit mode");
					if(list.get(i).isStatus()==true){
						flag=true;
						break;
					}
				}
				
			}
			
		}
		
		if(flag ==true && this.chkStatus.getValue()==true){
			showDialogMessage("Only one Calendar can Active");
			return false;
		}
		
		int days = CalendarUtil.getDaysBetween(this.dbstartdate.getValue(), this.dbenddate.getValue());
		int months = days/30;
		System.out.println("Months === "+months);
		if(months!=12){
			showDialogMessage("Calendar Must have 12 months");
			return false;
		}
		
		return super.validate();
	
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		this.tbcalendarid.setEnabled(false);
	}
	
	@Override
	public void onValueChange(ValueChangeEvent<Date> event) {
		// TODO Auto-generated method stub
		
	}

}
