package com.slicktechnologies.client.views.financialcalendar;

import java.util.Vector;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.FinancialCalender;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class FinancialCalendarPresenter extends FormTableScreenPresenter<FinancialCalender>{

	FinancialCalendarForm form;
	
	public FinancialCalendarPresenter(FormTableScreen<FinancialCalender> view,
			FinancialCalender model) {
		super(view, model);
		// TODO Auto-generated constructor stub
		
		
		form = (FinancialCalendarForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getFinancialYearConfigQuery());
		form.setPresenter(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.FINANCIALCALENDAR,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	public MyQuerry getFinancialYearConfigQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new FinancialCalender());
		return quer;
	}


	public static FinancialCalendarForm initalize() {
		
		FinancialCalendarTable gentableScreen=new FinancialCalendarTable();
		
		FinancialCalendarForm form = new FinancialCalendarForm(gentableScreen, FormTableScreen.UPPER_MODE, true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		FinancialCalendarPresenter  presenter= new  FinancialCalendarPresenter(form, new FinancialCalender());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
		
	}



	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void reactToProcessBarEvents(ClickEvent e) {

		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
//		if(text.equals("New"))
//			{
//				form.setToNewState();
//				initalize();
//			}	
	}



	@Override
	protected void makeNewModel() {

		model = new FinancialCalender();
	}
	

	public void setModel(FinancialCalender entity)
	{
		model=entity;
	}
	
	

}
