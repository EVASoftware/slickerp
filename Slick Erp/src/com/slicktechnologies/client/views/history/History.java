package com.slicktechnologies.client.views.history;

import java.util.ArrayList;

import com.simplesoftwares.client.library.appstructure.SuperModel;

public class History {
	
	String moduleName;
	String documentType;
	Integer documentId;
	String screenState;
	boolean listForm;
	SuperModel model;
	ArrayList<SuperModel> listModel;
	
	Integer personId;
	String personName;
	Long personCell;
	
	
	
	public History() {
		
	}


	public String getDocumentType() {
		return documentType;
	}


	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}


	public Integer getDocumentId() {
		return documentId;
	}


	public void setDocumentId(Integer documentId) {
		if(documentId!=null)
		this.documentId = documentId;
	}


	public String getScreenState() {
		return screenState;
	}


	public void setScreenState(String screenState) {
		this.screenState = screenState;
	}


	public boolean isListForm() {
		return listForm;
	}


	public void setListForm(boolean listForm) {
		this.listForm = listForm;
	}


	public SuperModel getModel() {
		return model;
	}


	public void setModel(SuperModel model) {
		this.model = model;
	}


	public ArrayList<SuperModel> getListModel() {
		return listModel;
	}


	public void setListModel(ArrayList<SuperModel> listModel) {
		this.listModel = listModel;
	}


	public String getModuleName() {
		return moduleName;
	}


	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}


	public Integer getPersonId() {
		return personId;
	}


	public void setPersonId(Integer personId) {
		this.personId = personId;
	}


	public String getPersonName() {
		return personName;
	}


	public void setPersonName(String personName) {
		this.personName = personName;
	}


	public Long getPersonCell() {
		return personCell;
	}


	public void setPersonCell(Long personCell) {
		this.personCell = personCell;
	}
	
	
	
	

}
