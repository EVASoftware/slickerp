package com.slicktechnologies.client.views.history;

import org.eclipse.jetty.security.UserAuthentication;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.numbergenerator.NumberGeneratorForm;
import com.slicktechnologies.client.numbergenerator.NumberGeneratorPresenter;
import com.slicktechnologies.client.userauthorization.UserAuthorizationForm;
import com.slicktechnologies.client.userauthorization.UserAuthorizationPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.account.expensemanagment.ExpensemanagmentForm;
import com.slicktechnologies.client.views.account.expensemanagment.ExpensemanagmentPresenter;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.MultipleExpensemanagmentForm;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.MultipleExpensemanagmentPresenter;
import com.slicktechnologies.client.views.account.pettycash.PettyCashForm;
import com.slicktechnologies.client.views.account.pettycash.PettyCashPresenter;
import com.slicktechnologies.client.views.account.pettycashtransactions.PettyCashTransactionForm;
import com.slicktechnologies.client.views.account.pettycashtransactions.PettyCashTransactionPresenter;
import com.slicktechnologies.client.views.assetmanagementdashboard.ClientAssetTableProxy;
import com.slicktechnologies.client.views.billofproductmaterial.BillOfProductMaterialForm;
import com.slicktechnologies.client.views.billofproductmaterial.BillOfProductMaterialPresenter;
import com.slicktechnologies.client.views.complain.ComplainForm;
import com.slicktechnologies.client.views.complain.ComplainPresenter;
import com.slicktechnologies.client.views.contactpersonidentification.contactpersondetails.ContactPersonForm;
import com.slicktechnologies.client.views.contactpersonidentification.contactpersondetails.ContactPersonPresenter;
import com.slicktechnologies.client.views.contactpersonidentification.contactpersonlist.ContactListForm;
import com.slicktechnologies.client.views.contactpersonidentification.contactpersonlist.ContactListPresenter;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.customer.CustomerForm;
import com.slicktechnologies.client.views.customer.CustomerPresenter;
import com.slicktechnologies.client.views.customerbranchdetails.CustomerBranchForm;
import com.slicktechnologies.client.views.customerbranchdetails.CustomerBranchPresenter;
import com.slicktechnologies.client.views.deliverynote.DeliveryNoteForm;
import com.slicktechnologies.client.views.deliverynote.DeliveryNotePresenter;
import com.slicktechnologies.client.views.device.DeviceForm;
import com.slicktechnologies.client.views.device.DevicePresenter;
import com.slicktechnologies.client.views.fumigationAustralia.FumigationAustraliaForm;
import com.slicktechnologies.client.views.fumigationAustralia.FumigationAustraliaPresenter;
import com.slicktechnologies.client.views.fumigationdetails.FumigationForm;
import com.slicktechnologies.client.views.fumigationdetails.FumigationPresenter;
import com.slicktechnologies.client.views.humanresource.advance.AdvanceForm;
import com.slicktechnologies.client.views.humanresource.advance.AdvancePresenter;
import com.slicktechnologies.client.views.humanresource.ctc.CTCForm;
import com.slicktechnologies.client.views.humanresource.ctc.CTCpresenter;
import com.slicktechnologies.client.views.humanresource.ctctemplate.CTCTemplateForm;
import com.slicktechnologies.client.views.humanresource.ctctemplate.CTCTemplatePresenter;
import com.slicktechnologies.client.views.humanresource.employeeadditionaldetails.EmployeeAdditionalDetailsForm;
import com.slicktechnologies.client.views.humanresource.employeeadditionaldetails.EmployeeAdditionalDetailsPresenter;
import com.slicktechnologies.client.views.humanresource.employeeleave.EmployeeLeaveForm;
import com.slicktechnologies.client.views.humanresource.employeeleave.EmployeeLeavePresenter;
import com.slicktechnologies.client.views.humanresource.leaveapplication.LeaveApplicationForm;
import com.slicktechnologies.client.views.humanresource.leaveapplication.LeaveApplicationPresenter;
import com.slicktechnologies.client.views.humanresource.salarySlip.SalarySlipForm;
import com.slicktechnologies.client.views.humanresource.salarySlip.SalaryslipPresenter;
import com.slicktechnologies.client.views.humanresource.timereport.TimeReportForm;
import com.slicktechnologies.client.views.humanresource.timereport.TimeReportPresenter;
import com.slicktechnologies.client.views.interaction.interactiondetails.InteractionForm;
import com.slicktechnologies.client.views.interaction.interactiondetails.InteractionPresenter;
import com.slicktechnologies.client.views.interaction.interactionlist.InteractionListForm;
import com.slicktechnologies.client.views.interaction.interactionlist.InteractionListPresenter;
import com.slicktechnologies.client.views.inventory.billofmaterial.BillOfMaterialForm;
import com.slicktechnologies.client.views.inventory.billofmaterial.BillOfMaterialPresenter;
import com.slicktechnologies.client.views.inventory.grnlist.GRNListForm;
import com.slicktechnologies.client.views.inventory.grnlist.GRNListPresenter;
import com.slicktechnologies.client.views.inventory.inspection.InspectionForm;
import com.slicktechnologies.client.views.inventory.inspection.InspectionPresenter;
import com.slicktechnologies.client.views.inventory.inspectionlist.InspectionListForm;
import com.slicktechnologies.client.views.inventory.inspectionlist.InspectionListPresenter;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNoteForm;
import com.slicktechnologies.client.views.inventory.materialissuenote.MaterialIssueNotePresenter;
import com.slicktechnologies.client.views.inventory.materialmovementnote.MaterialMovementNotePresenter;
import com.slicktechnologies.client.views.inventory.materialmovementnote.MaterialMovementNoteForm;
import com.slicktechnologies.client.views.inventory.materialreuestnote.MaterialRequestNoteForm;
import com.slicktechnologies.client.views.inventory.materialreuestnote.MaterialRequestNotePresenter;
import com.slicktechnologies.client.views.inventory.productinventorytransaction.InventoryProductTransactionListForm;
import com.slicktechnologies.client.views.inventory.productinventorytransaction.InventoryProductTransactionPresenter;
import com.slicktechnologies.client.views.inventory.productinventoryview.ProductInventoryViewForm;
import com.slicktechnologies.client.views.inventory.productinventoryview.ProductInventoryViewPresenter;
import com.slicktechnologies.client.views.inventory.productinventoryviewlist.ProductInventoryViewListForm;
import com.slicktechnologies.client.views.inventory.productinventoryviewlist.ProductInventoryViewListPresenter;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNForm;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNPresenter;
import com.slicktechnologies.client.views.inventory.stockalert.StockAlertForm;
import com.slicktechnologies.client.views.inventory.stockalert.StockAlertPresenter;
import com.slicktechnologies.client.views.inventory.warehousedetails.WareHouseDetailsForm;
import com.slicktechnologies.client.views.inventory.warehousedetails.WareHouseDetailsPresenter;
import com.slicktechnologies.client.views.ipaddressauthorization.IPAddressAuthorizationForm;
import com.slicktechnologies.client.views.ipaddressauthorization.IPAddressAuthorizationPresenter;
import com.slicktechnologies.client.views.lead.LeadForm;
import com.slicktechnologies.client.views.lead.LeadPresenter;
import com.slicktechnologies.client.views.multilevelapprovaldetails.MultilevelApprovalForm;
import com.slicktechnologies.client.views.multilevelapprovaldetails.MultilevelApprovalPresenter;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsPresenter;
import com.slicktechnologies.client.views.productgroupidentification.productgroupdetails.ProductGroupDetailsForm;
import com.slicktechnologies.client.views.productgroupidentification.productgroupdetails.ProductGroupDetailsPresenter;
import com.slicktechnologies.client.views.products.itemproduct.ItemproductForm;
import com.slicktechnologies.client.views.products.itemproduct.ItemproductPresenter;
import com.slicktechnologies.client.views.products.serviceproduct.ServiceproductForm;
import com.slicktechnologies.client.views.products.serviceproduct.ServiceproductPresenter;
import com.slicktechnologies.client.views.project.clientsideasset.ClientSideAssetForm;
import com.slicktechnologies.client.views.project.clientsideasset.ClientSideAssetPresenter;
import com.slicktechnologies.client.views.project.concproject.ProjectForm;
import com.slicktechnologies.client.views.project.concproject.ProjectPresenter;
import com.slicktechnologies.client.views.project.tool.ToolForm;
import com.slicktechnologies.client.views.project.tool.ToolPresenter;
import com.slicktechnologies.client.views.project.toolGroup.ToolGroupForm;
import com.slicktechnologies.client.views.project.toolGroup.ToolGroupPresenter;
import com.slicktechnologies.client.views.purchase.letterofintent.LetterOfIntentForm;
import com.slicktechnologies.client.views.purchase.letterofintent.LetterOfIntentPresenter;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderForm;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderPresenter;
import com.slicktechnologies.client.views.purchase.purchaserequisition.PurchaseRequisitionForm;
import com.slicktechnologies.client.views.purchase.purchaserequisition.PurchaseRequitionPresenter;
import com.slicktechnologies.client.views.purchase.quickpurchase.QuickPurchaseOrderForm;
import com.slicktechnologies.client.views.purchase.quickpurchase.QuickPurchaseOrderPreseneter;
import com.slicktechnologies.client.views.purchase.requestquotation.RequestForQuotationForm;
import com.slicktechnologies.client.views.purchase.requestquotation.RequestForQuotationPresenter;
import com.slicktechnologies.client.views.purchase.servicepo.ServicePoForm;
import com.slicktechnologies.client.views.purchase.servicepo.ServicePoPresenter;
import com.slicktechnologies.client.views.purchase.vendor.VendorForm;
import com.slicktechnologies.client.views.purchase.vendor.VendorPresenter;
import com.slicktechnologies.client.views.purchase.vendorPriceList.VendorPriceListForm;
import com.slicktechnologies.client.views.purchase.vendorPriceList.VendorPriceListPresenter;
import com.slicktechnologies.client.views.purchase.vendorproductlist.VendorProductListForm;
import com.slicktechnologies.client.views.purchase.vendorproductlist.VendorProductListPresenter;
import com.slicktechnologies.client.views.quickcontract.QuickContractForm;
import com.slicktechnologies.client.views.quickcontract.QuickContractPresentor;
import com.slicktechnologies.client.views.quicksalesorder.QuickSalesOrderForm;
import com.slicktechnologies.client.views.quicksalesorder.QuickSalesOrderPresenter;
import com.slicktechnologies.client.views.salesorder.SalesOrderForm;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter;
import com.slicktechnologies.client.views.salespersontarget.SalesPersonTargetForm;
import com.slicktechnologies.client.views.salespersontarget.SalesPersonTargetPresentor;
import com.slicktechnologies.client.views.salesquotation.SalesQuotationForm;
import com.slicktechnologies.client.views.salesquotation.SalesQuotationPresenter;
import com.slicktechnologies.client.views.securitydeposit.salessecuritydepositlist.SalesSecurityDepositListForm;
import com.slicktechnologies.client.views.securitydeposit.salessecuritydepositlist.SalesSecurityDepositListPresenter;
import com.slicktechnologies.client.views.securitydeposit.servicesecuritydepositlist.ServiceSecurityDepositListForm;
import com.slicktechnologies.client.views.securitydeposit.servicesecuritydepositlist.ServiceSecurityDepositListPresenter;
import com.slicktechnologies.client.views.service.ServiceForm;
import com.slicktechnologies.client.views.service.ServicePresenter;
import com.slicktechnologies.client.views.serviceliaison.liaison.LiaisonForm;
import com.slicktechnologies.client.views.serviceliaison.liaison.LiaisonPresenter;
import com.slicktechnologies.client.views.serviceliaison.liaisongroup.LiaisonGroupForm;
import com.slicktechnologies.client.views.serviceliaison.liaisongroup.LiaisonGroupPresenter;
import com.slicktechnologies.client.views.serviceliaison.liaisonsteps.LiaisonStepsForm;
import com.slicktechnologies.client.views.serviceliaison.liaisonsteps.LiaisonStepsPresenter;
import com.slicktechnologies.client.views.settings.branch.BranchForm;
import com.slicktechnologies.client.views.settings.branch.BranchPresenter;
import com.slicktechnologies.client.views.settings.customerslogin.CustomerUserForm;
import com.slicktechnologies.client.views.settings.customerslogin.CustomerUserPresenter;
import com.slicktechnologies.client.views.settings.employee.EmployeeForm;
import com.slicktechnologies.client.views.settings.employee.EmployeePresenter;
import com.slicktechnologies.client.views.settings.roledefinition.RoleDefinitionForm;
import com.slicktechnologies.client.views.settings.roledefinition.RoleDefinitionPresenter;
import com.slicktechnologies.client.views.settings.user.UserForm;
import com.slicktechnologies.client.views.settings.user.UserPresenter;
import com.slicktechnologies.client.views.smsconfiguration.SmsConfigurationForm;
import com.slicktechnologies.client.views.smsconfiguration.SmsConfigurationPresenter;
import com.slicktechnologies.client.views.smstemplate.SmsTemplateForm;
import com.slicktechnologies.client.views.smstemplate.SmsTemplatePresenter;
import com.slicktechnologies.client.views.teammanagement.TeamManagementForm;
import com.slicktechnologies.client.views.teammanagement.TeamManagementPresenter;
import com.slicktechnologies.client.views.quotation.QuotationForm;
import com.slicktechnologies.client.views.quotation.QuotationPresenter;
import com.slicktechnologies.client.views.workorder.WorkOrderForm;
import com.slicktechnologies.client.views.workorder.WorkOrderPresenter;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.RoleDefinition;
import com.slicktechnologies.shared.common.billofproductmaterial.BillOfProductMaterial;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorPriceListDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.Contact;
import com.slicktechnologies.shared.common.helperlayer.CustomerUser;
import com.slicktechnologies.shared.common.helperlayer.IPAddressAuthorization;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Loan;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveApplication;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReport;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.inventory.PhysicalInventoryMaintaince;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApproval;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.role.UserRole;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesperson.SalesPersonTargets;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.scheduling.TeamManagement;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.Liaison;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonGroup;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonStep;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.servicerelated.ToolGroup;
import com.slicktechnologies.shared.common.workorder.WorkOrder;

public class HistoryPopup extends AbsolutePanel {
	HistoryTable table;
	String docType="";
	String moduleName="";
	
	public static History historyObj;
	
	protected GWTCGlassPanel glassPanel;
	
	public HistoryPopup() {
		
		table=new HistoryTable();
		setSelectionOnHistoryTable();
		add(table.getTable(),10,10);
		
//		setSize("700px","200px");
//		setSize("700px","400px");
		setSize("1200px","600px");
		table.getTable().setWidth("1190px");
		table.getTable().setHeight("590px");

		this.getElement().setId("form");
	}
	
	
	private void setSelectionOnHistoryTable() {
		
		final NoSelectionModel<History> selectionModelMyObj = new NoSelectionModel<History>();
	    SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
	    {
	        @Override
	        public void onSelectionChange(SelectionChangeEvent event) 
	        {
	        	final History entity=selectionModelMyObj.getLastSelectedObject();
	        	historyObj=entity;
	       	 	System.out.println("INSIDE ON ROW SELECTION");
	       	 	LoginPresenter.panel.hide();
	       	 	initializeDocument(entity);
	        }
	    };
	    selectionModelMyObj.addSelectionChangeHandler( tableHandler );
	    table.getTable().setSelectionModel(selectionModelMyObj);
	}
	
	private void initializeDocument(History object){
		
		docType = object.getDocumentType().trim();
  	 	moduleName=object.getModuleName().trim();
  	 	
  	 	/******************************** SERVICE MODULE HISTORY *******************************************/
  	 	
  	 	// done
  	 	if(docType.equals(AppConstants.QUOTATION)){
  	 		
  	 		if(moduleName.equals(AppConstants.SERVICEMODULE)){
  	 			
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Quotation",Screen.QUOTATION);
  			final QuotationForm form = QuotationPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final Quotation con=(Quotation)object.getModel();
  			
  			
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 		}
  	 		
  	 		if(moduleName.equals(AppConstants.SALESMODULE)){
  	 			
  	  	 		glassPanel=new GWTCGlassPanel();
  				glassPanel.show();
  				
  	  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  	  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Quotation",Screen.SALESQUOTATION);
  	  			final SalesQuotationForm form = SalesQuotationPresenter.initalize();
  	  			AppMemory.getAppMemory().stickPnel(form);
  	  			final SalesQuotation con=(SalesQuotation)object.getModel();
	  	  		Timer t = new Timer() {
					@Override
					public void run() {
						form.updateView(con);
						form.setToViewState();
					}
				};
				t.schedule(4000);
  			    
  			    glassPanel.hide();
  			    
  	  	 	}
  	 	}
  	 	
  	 	
  	 	// done
  	 	if(docType.equals(AppConstants.CONTRACT)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contract",Screen.CONTRACT);
  			final ContractForm form = ContractPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final Contract con=(Contract)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
 	 	// done
  	 	if(docType.equals(AppConstants.QUICKCONTRACTSCREEN)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/QuickContract",Screen.QUICKCONTRACT);
  			final QuickContractForm form = QuickContractPresentor.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final Contract con=(Contract)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	
  	 	//  done
  	 	if(docType.equals(AppConstants.LEAD)){
  	 		
  	 		if(moduleName.equals(AppConstants.SERVICEMODULE)){
  	 			glassPanel=new GWTCGlassPanel();
  				glassPanel.show();
  				
  	  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  	  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Lead",Screen.LEAD);
  	  			final LeadForm form = LeadPresenter.initalize();
  	  			AppMemory.getAppMemory().stickPnel(form);
  	  			final Lead con=(Lead)object.getModel();
	  	  		Timer t = new Timer() {
					@Override
					public void run() {
						form.updateView(con);
						form.setToViewState();
					}
				};
				t.schedule(4000);
  			    
  			    glassPanel.hide();
  	 		}
  	 		
  	 		if(moduleName.equals(AppConstants.SALESMODULE)){
  	 			glassPanel=new GWTCGlassPanel();
  				glassPanel.show();
  				
  	  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  	  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Lead",Screen.SALESLEAD);
  	  			final LeadForm form = LeadPresenter.initalize();
	  			AppMemory.getAppMemory().stickPnel(form);
	  			final Lead con=(Lead)object.getModel();
	  			Timer t = new Timer() {
					@Override
					public void run() {
						form.updateView(con);
						form.setToViewState();
					}
				};
				t.schedule(4000);
  			    
  			    glassPanel.hide();
  	 		}
  	 	}
  	 	
	 	//  done
  	 	if(docType.equals(AppConstants.CUSTOMER)){
  	 		
  	 		if(moduleName.equals(AppConstants.SERVICEMODULE)){
  	 			glassPanel=new GWTCGlassPanel();
  				glassPanel.show();
  				
  	  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  	  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer",Screen.CUSTOMER);
  	  			final CustomerForm form = CustomerPresenter.initalize();
  	  			AppMemory.getAppMemory().stickPnel(form);
  	  			final Customer con=(Customer)object.getModel();
	  	  		Timer t = new Timer() {
					@Override
					public void run() {
						form.updateView(con);
						form.setToViewState();
					}
				};
				t.schedule(4000);
  			    
  			    glassPanel.hide();
  	 		}
  	 		
  	 		if(moduleName.equals(AppConstants.SALESMODULE)){
  	 			glassPanel=new GWTCGlassPanel();
  				glassPanel.show();
  				
  	  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  	  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Customer",Screen.SALESCUSTOMER);
  	  			final CustomerForm form = CustomerPresenter.initalize();
  	  			AppMemory.getAppMemory().stickPnel(form);
  	  			final Customer con=(Customer)object.getModel();
	  	  		Timer t = new Timer() {
					@Override
					public void run() {
						form.updateView(con);
						form.setToViewState();
					}
				};
				t.schedule(4000);
  			    
  			    glassPanel.hide();
  	 		}
  	 	}
  	 	
  	 	//  done
  	 	if(docType.equals(AppConstants.CUSTOMERSERVICE)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Service",Screen.DEVICE);
  			final DeviceForm form = DevicePresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final Service con=(Service)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	//   rohan added this for project navigation 
 	 	//  done
  	 	if(docType.equals(AppConstants.CUSTOMERPROJECT)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Project",Screen.PROJECT);
  			final ProjectForm form = ProjectPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final ServiceProject con=(ServiceProject)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	
  	 	
  	 	
  	 	
  	 	 //  done 	 	
  	 	if(docType.equals(AppConstants.TEAMMANAGEMENT)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Team Management",Screen.TEAMMANAGEMENT);
  			final TeamManagementForm form = TeamManagementPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final TeamManagement con=(TeamManagement)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	//  done
  	 	if(docType.equals(AppConstants.CUSTOMERBRANCH)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Branch",Screen.CUSTOMERBRANCH);
  			final CustomerBranchForm form = CustomerBranchPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final CustomerBranchDetails con=(CustomerBranchDetails)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	
  	 	//  done
  	 	if(docType.equals(AppConstants.FUMIGATION)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Fumigation",Screen.FUMIGATION);
  			final FumigationForm form = FumigationPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final Fumigation con=(Fumigation)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	// done
  	 	if(docType.equals(AppConstants.FUMIGATIONAUS)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Fumigation(Aus)",Screen.FUMIGATIONAUSTRALIA);
  			final FumigationAustraliaForm form = FumigationAustraliaPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final Fumigation con=(Fumigation)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	//   done
  	 	if(docType.equals(AppConstants.CUSTOMERSUPPORT)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Fumigation(Aus)",Screen.FUMIGATIONAUSTRALIA);
  			final ComplainForm form = ComplainPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final Complain con=(Complain)object.getModel();
  			form.updateView(con);
		    form.setToViewState();Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	
  	 	// done
  	 	if(docType.equals(AppConstants.LIASIONSTEPS)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Liasion Steps",Screen.LIAISONSTEPS);
  			final LiaisonStepsForm form = LiaisonStepsPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final LiaisonStep con=(LiaisonStep)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	
  	 	// done
  	 	if(docType.equals(AppConstants.LIASIONGROUP)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Liasion Group",Screen.LIAISONGROUP);
  			final LiaisonGroupForm form = LiaisonGroupPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final LiaisonGroup con=(LiaisonGroup)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	//  done
  	 	if(docType.equals(AppConstants.LIASION)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Liasion",Screen.LIASION);
  			final LiaisonForm form = LiaisonPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final Liaison con=(Liaison)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	
  	 	
  	 	
  	 	
  	 	
  	 	
//  	 	if(docType.equals(AppConstants.INTERACTIONTODOLIST)){
//  	 		
//  	 		glassPanel=new GWTCGlassPanel();
//			glassPanel.show();
//			
//  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
//  	 		
//  	 		if(moduleName.equals(AppConstants.SERVICEMODULE)){
//  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Interaction To Do List",Screen.INTERACTIONLIST);
//  	 		}
//  	 		if(moduleName.equals(AppConstants.SALESMODULE)){
//  	  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Interaction To Do List",Screen.INTERACTIONLIST);
//  	  	 	}
//  	 		if(moduleName.equals(AppConstants.PURCHASEMODULE)){
//  	  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Interaction To Do List",Screen.INTERACTIONLIST);
//  	  	 	}
//  	 		if(moduleName.equals(AppConstants.INVENTORYMODULE)){
//  	  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/Interaction To Do List",Screen.INTERACTIONLIST);
//  	  	 	}
//  	 		
//  	 		
//  			LoginPresenter.historyFlag=true;
//  			final InteractionListForm form = InteractionListPresenter.initalize();
//  			AppMemory.getAppMemory().stickPnel(form);
//  			
//			form.updateList(object.getListModel());
//		    form.setToViewState();
//		    
//		    glassPanel.hide();
//		    
//  	 	}
  	 	
  	 	//  done
  	 	
  	 	if(docType.equals(AppConstants.INTERACTIONTODO)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  	 		
 	 		
  	 		if(moduleName.equals(AppConstants.SERVICEMODULE)){
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Interaction To Do",Screen.INSPECTION);
  	 		}
  	 		if(moduleName.equals(AppConstants.SALESMODULE)){
  	  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Interaction To Do",Screen.INSPECTION);
  	  	 	}
  	 		if(moduleName.equals(AppConstants.PURCHASEMODULE)){
  	  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Interaction To Do",Screen.INSPECTION);
  	  	 	}
  	 		if(moduleName.equals(AppConstants.INVENTORYMODULE)){
  	  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/Interaction To Do",Screen.INSPECTION);
  	  	 	}
  	 		
  			final InteractionForm form = InteractionPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final InteractionType con=(InteractionType)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	
//  	 	if(docType.equals(AppConstants.CONTACTPERSONLIST)){
//  	 		
//  	 		glassPanel=new GWTCGlassPanel();
//			glassPanel.show();
//			
//  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
//  	 		
//  	 		
// 	 		
//  	 		if(moduleName.equals(AppConstants.SERVICEMODULE)){
//  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contact Person List",Screen.CONTACTPERSONLIST);
//  	 		}
//  	 		if(moduleName.equals(AppConstants.SALESMODULE)){
//  	  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Contact Person List",Screen.CONTACTPERSONLIST);
//  	  	 	}
//  	 		if(moduleName.equals(AppConstants.PURCHASEMODULE)){
//  	  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Contact Person List",Screen.CONTACTPERSONLIST);
//  	  	 	}
//  	 		
//  	 		
//  			LoginPresenter.historyFlag=true;
//  			final ContactListForm form = ContactListPresenter.initialize();;
//  			AppMemory.getAppMemory().stickPnel(form);
//  			
//			form.updateList(object.getListModel());
//		    form.setToViewState();
//		    
//		    glassPanel.hide();
//		    
//  	 	}
  	 	
  	 	//  done
  	 	if(docType.equals(AppConstants.CONTACTPERSONDETAILS)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  	 		
	 		
  	 		if(moduleName.equals(AppConstants.SERVICEMODULE)){
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contact Person Details",Screen.CONTACTPERSONDETAILS);
  	 		}
  	 		if(moduleName.equals(AppConstants.SALESMODULE)){
  	  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Contact Person Details",Screen.CONTACTPERSONDETAILS);
  	  	 	}
  	 		if(moduleName.equals(AppConstants.PURCHASEMODULE)){
  	  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Contact Person Details",Screen.CONTACTPERSONDETAILS);
  	  	 	}
  	 		
  			final ContactPersonForm form = ContactPersonPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final ContactPersonIdentification con=(ContactPersonIdentification)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
 	//vijay sales person
  	 	
  	 	if(docType.equals(AppConstants.SALESPERSONTARGET)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Sales Person Target",Screen.SALESPERSONTARGET);
  			final SalesPersonTargetForm form = SalesPersonTargetPresentor.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final SalesPersonTargets con=(SalesPersonTargets)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
//  	 	if(docType.equals(AppConstants.SECURITYDEPOSITELIST)){
//  	 		
//  	 		if(moduleName.equals(AppConstants.SERVICEMODULE)){
//  	 		
//	  	 		glassPanel=new GWTCGlassPanel();
//				glassPanel.show();
//				
//	  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
//	  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Security Deposite List",Screen.SERVICESECURITYDEPOSITLIST);
//	  			final ServiceSecurityDepositListForm form = ServiceSecurityDepositListPresenter.initialize();;
//	  			AppMemory.getAppMemory().stickPnel(form);
//	  			final Quotation con=(Quotation)object.getModel();
//	  			Timer t = new Timer() {
//					@Override
//					public void run() {
//						form.updateView(con);
//						form.setToViewState();
//					}
//				};
//				t.schedule(4000);
//			    
//			    glassPanel.hide();
//  	 		}
//  	 		
//  	 		
//  	 		if(moduleName.equals(AppConstants.SALESMODULE)){
//  	  	 		
//  	  	 		glassPanel=new GWTCGlassPanel();
//  				glassPanel.show();
//  				
//  	  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
//  	  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Security Deposite List",Screen.SALESSECURITYDEPOSITLIST);
//  	  			final SalesSecurityDepositListForm form = SalesSecurityDepositListPresenter.initialize();;
//  	  			AppMemory.getAppMemory().stickPnel(form);
//  	  			final SalesQuotation con=(SalesQuotation)object.getModel();
//  	  		Timer t = new Timer() {
//				@Override
//				public void run() {
//					form.updateView(con);
//					form.setToViewState();
//				}
//			};
//			t.schedule(4000);
//  			    
//  			    glassPanel.hide();
//  	  	 		}
//		    
//  	 	}
  	 	
  	 	
  	 	
  		/******************************** SALES MODULE HISTORY *******************************************/	
  	 	
  	 	
  	 	
  	 	//   done
  	 	
  	 	if(docType.equals(AppConstants.SALESORDER)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Sales Order",Screen.SALESORDER);
  			final SalesOrderForm form = SalesOrderPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final SalesOrder con=(SalesOrder)object.getModel();
		    Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	
  	 	// added by vijay for quick sales
  	 	if(docType.equals(AppConstants.QUICKSALESORDER)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Quick Sales Order",Screen.QUICKSALESORDER);
  			final QuickSalesOrderForm form = QuickSalesOrderPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final SalesOrder salesorder=(SalesOrder)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(salesorder);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	//  done
  	 	if(docType.equals(AppConstants.WORK_ORDER)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Work Order",Screen.WORKORDER);
  			final WorkOrderForm form = WorkOrderPresenter.initialize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final WorkOrder con=(WorkOrder)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	
  	 	//  done
  	 	if(docType.equals(AppConstants.DELIVERYNOTE)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Delivery Note",Screen.DELIVERYNOTE);
  			final DeliveryNoteForm form = DeliveryNotePresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final DeliveryNote con=(DeliveryNote)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	
  		/******************************** PURCHASE MODULE HISTORY *******************************************/	
  	 	//   done
  	 	if(docType.equals(AppConstants.VENDOR)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Vendor",Screen.VENDOR);
  			final VendorForm form = VendorPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final Vendor con=(Vendor)object.getModel();
  			form.updateView(con);
		    form.setToViewState();Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	//  done
  	 	if(docType.equals(AppConstants.PURCHASEREQUISION)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Purchase Requision",Screen.PURCHASEREQUISITE);
  			final PurchaseRequisitionForm form = PurchaseRequitionPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final PurchaseRequisition con=(PurchaseRequisition)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	
  	 	//   done
  	 	if(docType.equals(AppConstants.RFQ)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/RFQ",Screen.REQUESTFORQUOTATION);
  			final RequestForQuotationForm form = RequestForQuotationPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final RequsestForQuotation con=(RequsestForQuotation)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	//  done
  	 	if(docType.equals(AppConstants.LETTEROFINTENT)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Letter Of Intent(LOI)",Screen.LETTEROFINTENT);
  			final LetterOfIntentForm form = LetterOfIntentPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final LetterOfIntent con=(LetterOfIntent)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	
  	 	//  done
  	 	if(docType.equals(AppConstants.PURCHASEORDER)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Purchase Order",Screen.PURCHASEORDER);
  			final PurchaseOrderForm form = PurchaseOrderPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final PurchaseOrder con=(PurchaseOrder)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	//   done 
  	 	if(docType.equals(AppConstants.VENDORPRODUCTPRICE)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Vendor Product Price",Screen.VENDORPRICELIST);
  			final VendorPriceListForm form = VendorPriceListPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final PriceList con=(PriceList)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
//  	 	if(docType.equals(AppConstants.VENDORPRODUCTPRICELIST)){
//  	 		
//  	 		glassPanel=new GWTCGlassPanel();
//			glassPanel.show();
//			
//  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
//  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Vendor Product Pricelist",Screen.VENDORPRODUCTLIST);
//  			final VendorProductListForm form = VendorProductListPresenter.initalize();
//  			AppMemory.getAppMemory().stickPnel(form);
//  			
//  			LoginPresenter.historyFlag=true;
//  			form.updateList(object.getListModel());
//		    form.setToViewState();
//		    
//		    glassPanel.hide();
//		    
//  	 	}
  	 	
  	 	
  	 	
/******************************** INVENTORY MODULE HISTORY *******************************************/	
  	 	
  	 	
  	 	
  	 	
//  	 	if(docType.equals(AppConstants.INVENTORYTRANSACTIONLIST)){
//  	 		
//  	 		glassPanel=new GWTCGlassPanel();
//			glassPanel.show();
//			
//  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
//  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/Inventory Transaction List",Screen.PRODUCTINVENTORYTRANSACTION);
//  			final InventoryProductTransactionListForm form = InventoryProductTransactionPresenter.initalize();
//  			AppMemory.getAppMemory().stickPnel(form);
//  			
//  			LoginPresenter.historyFlag=true;
//  			form.updateList(object.getListModel());
//		    form.setToViewState();
//		    
//		    glassPanel.hide();
//		    
//  	 	}
  	 	
  	 	//   done
  	 	if(docType.equals(AppConstants.PRODUCTINVENTORYVIEW)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/Product Inventory View",Screen.PRODUCTINVENTORYVIEW);
  			final ProductInventoryViewForm form = ProductInventoryViewPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final ProductInventoryView con=(ProductInventoryView)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	

//  	 	if(docType.equals(AppConstants.PRODUCTINVENTORYLIST)){
//  	 		
//  	 		glassPanel=new GWTCGlassPanel();
//			glassPanel.show();
//			
//  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
//  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/Product Inventory List",Screen.PRODUCTINVENTORYVIEWLIST);
//  			final ProductInventoryViewListForm form = ProductInventoryViewListPresenter.initalize();
//  			AppMemory.getAppMemory().stickPnel(form);
//  			
//  			LoginPresenter.historyFlag=true;
//  			form.updateList(object.getListModel());
//		    form.setToViewState();
//		    
//		    glassPanel.hide();
//		    
//  	 	}
  	 	
//  	 	if(docType.equals(AppConstants.GRNLIST)){
//  	 		
//  	 		glassPanel=new GWTCGlassPanel();
//			glassPanel.show();
//			
//  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
//  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/GRN List",Screen.GRNLIST);
//  			final GRNListForm form = GRNListPresenter.initalize();
//  			AppMemory.getAppMemory().stickPnel(form);
//  			
//  			LoginPresenter.historyFlag=true;
//  			form.updateList(object.getListModel());
//		    form.setToViewState();
//		    
//		    glassPanel.hide();
//		    
//  	 	}
  	 	
  	 	//   done
  	 	if(docType.equals(AppConstants.GRN)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/GRN",Screen.GRN);
  			final GRNForm form = GRNPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final GRN con=(GRN)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	

//  	 	if(docType.equals(AppConstants.INSPECTIONLIST)){
//  	 		
//  	 		glassPanel=new GWTCGlassPanel();
//			glassPanel.show();
//			
//  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
//  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/Inspection List",Screen.INSPECTIONLIST);
//  			final InspectionListForm form = InspectionListPresenter.initalize();
//  			AppMemory.getAppMemory().stickPnel(form);
//  			
//  			LoginPresenter.historyFlag=true;
//  			form.updateList(object.getListModel());
//		    form.setToViewState();
//		    
//		    glassPanel.hide();
//		    
//  	 	}
  	 	
  	 	//   done
  	 	if(docType.equals(AppConstants.INSPECTION)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/Inspection",Screen.INSPECTION);
  			final InspectionForm form = InspectionPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final Inspection con=(Inspection)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	//    done
  	 	if(docType.equals(AppConstants.MATERIALREQUESTNOTE)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/Material Request Note",Screen.MATERIALREQUESTNOTE);
  			final MaterialRequestNoteForm form = MaterialRequestNotePresenter.initialize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final MaterialRequestNote con=(MaterialRequestNote)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
 	 	
  	 	//   done 
  	 	if(docType.equals(AppConstants.MATERIALISSUENOTE)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/Material Issue Note",Screen.MATERIALISSUENOTE);
  			final MaterialIssueNoteForm form = MaterialIssueNotePresenter.initialize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final MaterialIssueNote con=(MaterialIssueNote)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
 	 	
  	 	//     done
  	 	if(docType.equals(AppConstants.MATERIALMOVEMENTNOTE)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/Materail Movement Note",Screen.MATERIALMOVEMENTNOTE);
  			final MaterialMovementNoteForm form = MaterialMovementNotePresenter.initialize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final MaterialMovementNote con=(MaterialMovementNote)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
 	 	
  	 	//   done 
  	 	if(docType.equals(AppConstants.PHYSICALINVENTORY)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/Physical Inventory",Screen.WAREHOUSEDETAILS);
  			final WareHouseDetailsForm form = WareHouseDetailsPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final PhysicalInventoryMaintaince con=(PhysicalInventoryMaintaince)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	
  	 	/******************************** HR ADMIN MODULE HISTORY *******************************************/	
  	 	
  	 	//    done
  	 	if(docType.equals(AppConstants.EMPLOYEE)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Human Resource/Employee",Screen.EMPLOYEE);
  			final EmployeeForm form = EmployeePresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final Employee con=(Employee)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}

  	 	//   done
  	 	if(docType.equals(AppConstants.EMPLOYEEADDITIONALDETAILS)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Human Resource/Employee Additional Details",Screen.EMPLOYEEADDITIONALDETAILS);
  			final EmployeeAdditionalDetailsForm form = EmployeeAdditionalDetailsPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final EmployeeAdditionalDetails con=(EmployeeAdditionalDetails)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	//   done
  	 	if(docType.equals(AppConstants.LEAVEAPPLICATION)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Human Resource/Leave Application",Screen.LEAVEAPPLICATION);
  			LoginPresenter.historyFlag=true;
  			final LeaveApplicationForm form = LeaveApplicationPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final LeaveApplication con=(LeaveApplication)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		   
		    
		    glassPanel.hide();
		    
  	 	}

  	 	//   done
  	 	if(docType.equals(AppConstants.ADVANCEREQUEST)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Human Resource/Advance Request",Screen.ADVANCE);
  			final AdvanceForm form = AdvancePresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final Loan con=(Loan)object.getModel();
  			
  			
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}

  	 	//    done
  	 	if(docType.equals(AppConstants.TIMEREPORT)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Human Resource/Advance Request",Screen.ADVANCE);
  			final TimeReportForm form = TimeReportPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final TimeReport con=(TimeReport)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	
  	 	//   done
  	 	if(docType.equals(AppConstants.CTC)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Human Resource/CTC",Screen.CTC);
  			final CTCForm form = CTCpresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final CTC con=(CTC)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  		//   done
  	 	if(docType.equals(AppConstants.LEAVEBALANCE)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Human Resource/Leave Balance",Screen.LEAVEBALANCE);
  			final EmployeeLeaveForm form = EmployeeLeavePresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final LeaveBalance con=(LeaveBalance)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  		//    done
  	 	if(docType.equals(AppConstants.SALARYSLIP)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Human Resource/Salary Slip",Screen.SALARYSLIP);
  			final SalarySlipForm form = SalaryslipPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final PaySlip con=(PaySlip)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	
  	 	
  		
  	 	/******************************** PRODUCT MODULE HISTORY *******************************************/	
  	 	
  	 	//     done
  	 	if(docType.equals(AppConstants.SERVICEPRODUCT)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Product/Service Product",Screen.SERVICEPRODUCT);
  			final ServiceproductForm form = ServiceproductPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final ServiceProduct con=(ServiceProduct)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  		//   done
  	 	if(docType.equals(AppConstants.ITEMPRODUCT)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Product/Item Product",Screen.ITEMPRODUCT);
  			final ItemproductForm form = ItemproductPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final ItemProduct con=(ItemProduct)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	//   done
  	 	if(docType.equals(AppConstants.PRODUCTGROUPDETAILS)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Product/Product Group Details",Screen.PRODUCTGROUPDETAILS);
  			final ProductGroupDetailsForm form = ProductGroupDetailsPresenter.initialize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final ProductGroupDetails con=(ProductGroupDetails)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  		
  	 	//   done
  	 	if(docType.equals(AppConstants.BILLOFMATERIAL)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Product/Bill Of Material",Screen.BILLOFMATERIAL);
  			final BillOfProductMaterialForm form = BillOfProductMaterialPresenter.initialize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final BillOfProductMaterial con=(BillOfProductMaterial)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	
/******************************** ASSET MANAGEMENT MODULE HISTORY *******************************************/	
  	 	
  	 	//    done
  	 	if(docType.equals(AppConstants.COMPANYASSET)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Asset Management/Company Asset",Screen.COMPANYASSET);
  			final ToolForm  form = ToolPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final CompanyAsset con=(CompanyAsset)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  		
  	 	
  	 	//  done
  	 	if(docType.equals(AppConstants.CLIENTASSET)){
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Asset Management/Client Asset",Screen.CLIENTSIDEASSET);
  			final ClientSideAssetForm  form = ClientSideAssetPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final ClientSideAsset con=(ClientSideAsset)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
  	 	}
  	 	
  	 	//    done
  	 	if(docType.equals(AppConstants.TOOLSET)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Asset Management/Tool Set",Screen.TOOLGROUP);
  			final ToolGroupForm  form = ToolGroupPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final ToolGroup con=(ToolGroup)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  	 	
  	 	
/******************************** ACCOUNT MODULE HISTORY *******************************************/	
  	 	
  	 	//    done
  	 	if(docType.equals(AppConstants.EXPENSEMANAGEMET)){
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Expense Management",Screen.EXPENSEMANAGMENT);
  			final ExpensemanagmentForm  form = ExpensemanagmentPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final Expense con=(Expense)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    glassPanel.hide();
  	 	}

  	 	
  	 	//    done
  	 	if(docType.equals(AppConstants.PETTYCASHTRANSACTION)){
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Petty Cash Transaction",Screen.PETTYCASHTRANSACTION);
  			final PettyCashTransactionForm form = PettyCashTransactionPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final PettyCash con=(PettyCash)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    glassPanel.hide();
  	 	}
  	 	
  	 	
  	 	
  	 	//    done
  	 	if(docType.equals(AppConstants.BILLINGDETAILS)){
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Billing Details",Screen.BILLINGDETAILS);
  			final BillingDetailsForm form = BillingDetailsPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final BillingDocument con=(BillingDocument)object.getModel();
  			
  			Timer t = new Timer() {
				@Override
				public void run() {
					
					 BillingDetailsForm.flag=false;
 					 BillingDetailsForm.flag1=true;
					
					form.updateView(con);
					 form.setToViewState();
				}
			};
			t.schedule(4000);
  			
//  			form.updateView(con);
//		    form.setToViewState();
		    
		    
		    
		    glassPanel.hide();
  	 	}
  		
  	 	
  	 	//    done
  	 	if(docType.equals(AppConstants.INVOICEDETAILS)){
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Invoice Details",Screen.INVOICEDETAILS);
  			final InvoiceDetailsForm form = InvoiceDetailsPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final Invoice con=(Invoice)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    glassPanel.hide();
  	 	}
  	 	
  	 	//    done
  	 	if(docType.equals(AppConstants.PAYMENTDETAILS)){
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Payment Details",Screen.PAYMENTDETAILS);
  			final PaymentDetailsForm form = PaymentDetailsPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final CustomerPayment con=(CustomerPayment)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    glassPanel.hide();
  	 	}
  	 	
  	 	
/******************************** SETTING MODULE HISTORY *******************************************/	
  	 	
  	 	//   done
  	 	if(docType.equals(AppConstants.BRANCH)){
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Setting/Branch",Screen.BRANCH);
  			final BranchForm  form = BranchPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final Branch con=(Branch)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    glassPanel.hide();
  	 	}
  	 	
  		//    done
  	 	if(docType.equals(AppConstants.USER)){
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Setting/User",Screen.BRANCH);
  			final UserForm  form = UserPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final User con=(User)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    glassPanel.hide();
  	 	}
  	 	
  	 	//    done
  	 	if(docType.equals(AppConstants.CUSTOMERUSER)){
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Setting/Customer User",Screen.BRANCH);
  			final CustomerUserForm  form = CustomerUserPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final CustomerUser con=(CustomerUser)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    glassPanel.hide();
  	 	}
  	 	
  	 	//    done
  	 	if(docType.equals(AppConstants.NUMBERGENERATION)){
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Setting/Number Generation",Screen.NUMBERGENERATOR);
  			final NumberGeneratorForm  form = NumberGeneratorPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final NumberGeneration con=(NumberGeneration)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    glassPanel.hide();
  	 	}
  	 	
  	 	
  	 	//    done
  	 	if(docType.equals(AppConstants.SMSTEMPLATE)){
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Setting/SMS Template",Screen.SMSTEMPLATE);
  			final SmsTemplateForm  form = SmsTemplatePresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final SmsTemplate con=(SmsTemplate)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    glassPanel.hide();
  	 	}
  	 	
  	 	//    done   
  	 	if(docType.equals(AppConstants.SMSCONFIGURATION)){
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Setting/SMS Configuration",Screen.SMSCONFIG);
  			final SmsConfigurationForm  form = SmsConfigurationPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final SmsConfiguration con=(SmsConfiguration)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    glassPanel.hide();
  	 	}
  	 	
  	 	
  	 	//    done
  	 	if(docType.equals(AppConstants.MULTILEVELAPPROVAL)){
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Setting/Multilevel Approval",Screen.MULTILEVELAPPROVAL);
  			final MultilevelApprovalForm  form = MultilevelApprovalPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final MultilevelApproval con=(MultilevelApproval)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    glassPanel.hide();
  	 	}

  	 	//   done
  	 	if(docType.equals(AppConstants.IPADDRESSAUTHPRIZATION)){
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Setting/IP Address Authorization",Screen.IPADDRESSAUTHORIZATION);
  			final IPAddressAuthorizationForm  form = IPAddressAuthorizationPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final IPAddressAuthorization con=(IPAddressAuthorization)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    glassPanel.hide();
  	 	}

  	 	//   done 
  	 	if(docType.equals(AppConstants.USERAUTHORIZATION)){
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Setting/User Authorization",Screen.USERAUTHORIZATION);
  			final UserAuthorizationForm  form = UserAuthorizationPresenter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final UserRole con=(UserRole)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    glassPanel.hide();
  	 	}
  	 	
  	 	
  	 	
  	 	if(docType.equals(AppConstants.SERVICEPO)){
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Service PO",Screen.SERVICEPO);
  			final ServicePoForm  form = ServicePoPresenter.initialize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final ServicePo con=(ServicePo)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    glassPanel.hide();
  	 	}
  	 	
  		if(docType.equals(AppConstants.ROLEDEFINITION)){
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Settings/Role Definition",Screen.ROLEDEFINITION);
  			final RoleDefinitionForm  form = RoleDefinitionPresenter.initialize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final RoleDefinition con=(RoleDefinition)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    glassPanel.hide();
  	 	}
  	 	
  		if(docType.equals(AppConstants.QUICKPURCHASEORDER)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Purchase/Quick Purchase Order",Screen.QUICKPURCHASEORDER);
  			final QuickPurchaseOrderForm form = QuickPurchaseOrderPreseneter.initalize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final PurchaseOrder purchaseOrder=(PurchaseOrder)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(purchaseOrder);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  		
       if(docType.equals(AppConstants.CTCTEMPLATE)){
  	 		
  	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
			
  	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
  			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("HR Configuration/"+AppConstants.CTCTEMPLATE,Screen.CTCTEMPLATE);
  			final CTCTemplateForm form = CTCTemplatePresenter.initialize();
  			AppMemory.getAppMemory().stickPnel(form);
  			final CTCTemplate ctctemplate=(CTCTemplate)object.getModel();
  			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(ctctemplate);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    
		    glassPanel.hide();
		    
  	 	}
  		
       /**
		 * @author Anil
		 * @since 05-01-2021
		 * Navigation for expense management was not working
		 * raised by Vaishnavi
		 */
       if(docType.equals(AppConstants.MULTIPLEEXPENSEMANAGEMET)){
 	 		glassPanel=new GWTCGlassPanel();
			glassPanel.show();
 	 		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
 			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Expense Management",Screen.MULTIPLEEXPENSEMANAGMENT);
 			final MultipleExpensemanagmentForm  form = MultipleExpensemanagmentPresenter.initalize();
 			AppMemory.getAppMemory().stickPnel(form);
 			final MultipleExpenseMngt con=(MultipleExpenseMngt)object.getModel();
 			Timer t = new Timer() {
				@Override
				public void run() {
					form.updateView(con);
					form.setToViewState();
				}
			};
			t.schedule(4000);
		    glassPanel.hide();
 	 	}
  		
  		
  	 	
	}


	public HistoryTable getTable() {
		return table;
	}


	public void setTable(HistoryTable table) {
		this.table = table;
	}


	public String getDocType() {
		return docType;
	}


	public void setDocType(String docType) {
		this.docType = docType;
	}


	public String getModuleName() {
		return moduleName;
	}


	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	
	
	
	
	
}
