package com.slicktechnologies.client.views.history;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;

public class HistoryTable extends SuperTable<History>{

	TextColumn<History> getModuleNameColoumn;
	TextColumn<History> getDocumentTypeColoumn;
	TextColumn<History> getDocumentIdColoumn;
	
	TextColumn<History> getPersonIdColoumn;
	TextColumn<History> getPersonNameColoumn;
	TextColumn<History> getPersonCellColoumn;
	
	
	@Override
	public void createTable() {
		getModuleNameColoumn();		
		getDocumentTypeColoumn();
		getDocumentIdColoumn();
		
		getPersonIdColoumn();
		getPersonNameColoumn();
		getPersonCellColoumn();
		
		
	}

	private void getPersonIdColoumn() {
		getPersonIdColoumn=new TextColumn<History>() {
			
			@Override
			public String getValue(History object) {
				if(object.getPersonId()!=null){
				return object.getPersonId()+"";
				}
				return "";
			}
		};
		table.addColumn(getPersonIdColoumn, "Id");
		table.setColumnWidth(getPersonIdColoumn,90,Unit.PX);
	}

	private void getPersonNameColoumn() {
		getPersonNameColoumn=new TextColumn<History>() {
			
			@Override
			public String getValue(History object) {
				if(object.getPersonName()!=null){
				return object.getPersonName();
				}
				return "";
			}
		};
		table.addColumn(getPersonNameColoumn, "Name");
		table.setColumnWidth(getPersonNameColoumn,90,Unit.PX);
	}

	private void getPersonCellColoumn() {
		getPersonCellColoumn=new TextColumn<History>() {
			
			@Override
			public String getValue(History object) {
				if(object.getPersonCell()!=null){
				return object.getPersonCell()+"";
				}
				return "";
			}
		};
		table.addColumn(getPersonCellColoumn, "Cell");
		table.setColumnWidth(getPersonCellColoumn,100,Unit.PX);
	}

	private void getModuleNameColoumn() {
		getModuleNameColoumn=new TextColumn<History>() {
			
			@Override
			public String getValue(History object) {
				if(object.getModuleName()!=null){
				return object.getModuleName();
				}
				return "";
			}
		};
		table.addColumn(getModuleNameColoumn, "Module Name");
		table.setColumnWidth(getModuleNameColoumn,100,Unit.PX);
		table.setStyleName("blue");
	}

	private void getDocumentTypeColoumn() {
		getDocumentTypeColoumn=new TextColumn<History>() {
			
			@Override
			public String getValue(History object) {
				if(object.getDocumentType()!=null){
				return object.getDocumentType();
				}
				return "";
			}
		};
		table.addColumn(getDocumentTypeColoumn, "Document Name");
		table.setColumnWidth(getDocumentTypeColoumn,140,Unit.PX);
	}

	private void getDocumentIdColoumn() {
		getDocumentIdColoumn=new TextColumn<History>() {
			
			@Override
			public String getValue(History object) {
				if(object.getDocumentId()!=null){
				return object.getDocumentId()+"";
				}
				return "";
			}
		};
		table.addColumn(getDocumentIdColoumn, "Document Id");
		table.setColumnWidth(getDocumentIdColoumn,110,Unit.PX);
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

}
