package com.slicktechnologies.client.views.contract;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.slicktechnologies.shared.common.paymentlayer.Payment;


public class PaymentTable extends SuperTable<Payment>  
{
	Column<Payment,Date> dateColumn;
  Column<Payment,Number> amountColumn,cashAmountColumn,cardAmtColumn,chqAmtColumn;
  
	
	
	public PaymentTable()
	{
		super();
	}
	public PaymentTable(UiScreen<Payment> view)
	{
		super(view);	
	}

	public void createTable()
	{
		 createColumndateColumn();
createColumnamountColumn();
createColumncashAmountColumn();
createColumncardAmtColumn();
createColumnchqAmtColumn();

		
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);		 
	}
	
	protected void createColumndateColumn()
{
DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
DatePickerCell date= new DatePickerCell(fmt);
dateColumn=new Column<Payment,Date>(date)
{
@Override
public Date getValue(Payment object)
{
if(object.getDate()!=null){
return object.getDate();}
else{object.setDate(new Date());
return new Date();
}
}
};
table.addColumn(dateColumn,"Date");
}
protected void createColumnamountColumn()
{
NumberCell editCell=new NumberCell();
amountColumn=new Column<Payment,Number>(editCell)
{
@Override
public Number getValue(Payment object)
{
if(object.getTotalAmount()==0){
return 0;}
else{
return object.getTotalAmount();}
}
};
table.addColumn(amountColumn,"Amount");
}
protected void createColumncashAmountColumn()
{
NumberCell editCell=new NumberCell();
cashAmountColumn=new Column<Payment,Number>(editCell)
{
@Override
public Number getValue(Payment object)
{
if(object.getCashAmount()==0){
return 0;}
else{
return object.getCashAmount();}
}
};
table.addColumn(cashAmountColumn,"Cash Amount");
}
protected void createColumncardAmtColumn()
{
NumberCell editCell=new NumberCell();
cardAmtColumn=new Column<Payment,Number>(editCell)
{
@Override
public Number getValue(Payment object)
{
if(object.getCardAmount()==0){
return 0;}
else{
return object.getCardAmount();}
}
};
table.addColumn(cardAmtColumn,"Card Amount");
}
protected void createColumnchqAmtColumn()
{
NumberCell editCell=new NumberCell();
chqAmtColumn=new Column<Payment,Number>(editCell)
{
@Override
public Number getValue(Payment object)
{
if(object.getChqAmount()==0){
return 0;}
else{
return object.getChqAmount();}
}
};
table.addColumn(chqAmtColumn,"Cheque Amount");
}


      
	@Override public void addFieldUpdater() 
	{
		

	}
	
	

	public void addColumnSorting(){
	
		createSortingdateColumn();
createSortingamountColumn();
createSortingcashAmountColumn();
createSortingcardAmtColumn();
createSortingchqAmtColumn();

	}
	
	protected void createSortingdateColumn(){
List<Payment> list=getDataprovider().getList();
columnSort=new ListHandler<Payment>(list);
columnSort.setComparator(dateColumn, new Comparator<Payment>()
{
@Override
public int compare(Payment e1,Payment e2)
{
if(e1!=null && e2!=null)
{if( e1.getDate()!=null && e2.getDate()!=null){
return e1.getDate().compareTo(e2.getDate());}
}
else{
return 0;}
return 0;
}
});
table.addColumnSortHandler(columnSort);
}
protected void createSortingamountColumn(){
List<Payment> list=getDataprovider().getList();
columnSort=new ListHandler<Payment>(list);
columnSort.setComparator(amountColumn, new Comparator<Payment>()
{
@Override
public int compare(Payment e1,Payment e2)
{
if(e1!=null && e2!=null)
{if(e1.getTotalAmount()== e2.getTotalAmount()){return 0;}if(e1.getTotalAmount()> e2.getTotalAmount()){return 1;}else{return -1;}}else{return 0;}}
});
table.addColumnSortHandler(columnSort);
}
protected void createSortingcashAmountColumn(){
List<Payment> list=getDataprovider().getList();
columnSort=new ListHandler<Payment>(list);
columnSort.setComparator(cashAmountColumn, new Comparator<Payment>()
{
@Override
public int compare(Payment e1,Payment e2)
{
if(e1!=null && e2!=null)
{if(e1.getCashAmount()== e2.getCashAmount()){return 0;}if(e1.getCashAmount()> e2.getCashAmount()){return 1;}else{return -1;}}else{return 0;}}
});
table.addColumnSortHandler(columnSort);
}
protected void createSortingcardAmtColumn(){
List<Payment> list=getDataprovider().getList();
columnSort=new ListHandler<Payment>(list);
columnSort.setComparator(cardAmtColumn, new Comparator<Payment>()
{
@Override
public int compare(Payment e1,Payment e2)
{
if(e1!=null && e2!=null)
{if(e1.getCardAmount()== e2.getCardAmount()){return 0;}if(e1.getCardAmount()> e2.getCardAmount()){return 1;}else{return -1;}}else{return 0;}}
});
table.addColumnSortHandler(columnSort);
}
protected void createSortingchqAmtColumn(){
List<Payment> list=getDataprovider().getList();
columnSort=new ListHandler<Payment>(list);
columnSort.setComparator(chqAmtColumn, new Comparator<Payment>()
{
@Override
public int compare(Payment e1,Payment e2)
{
if(e1!=null && e2!=null)
{if(e1.getChqAmount()== e2.getChqAmount()){return 0;}if(e1.getChqAmount()> e2.getChqAmount()){return 1;}else{return -1;}}else{return 0;}}
});
table.addColumnSortHandler(columnSort);
}

	
	@Override
	public void setEnable(boolean state)
	{
	
	}
	
	@Override
	public void applyStyle()
	{
	
	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<Payment>()
		{
			@Override
			public Object getKey(Payment item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
		};
	}
	
	public double calculatePaidAmt()
	{
		List<Payment>list=getDataprovider().getList();

		double paidAmt=0;
		for(int i=0;i<list.size();i++)
		{
			Payment entity=list.get(i);
			paidAmt=paidAmt+entity.getCardAmount()+entity.getCashAmount()+entity.getChqAmount();
			System.out.println("******************"+paidAmt);
		}
		
		return paidAmt;
	}
}
