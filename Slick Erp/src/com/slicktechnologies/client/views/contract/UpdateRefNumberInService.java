package com.slicktechnologies.client.views.contract;

import java.util.Date;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
@RemoteServiceRelativePath("updaterefNumberinservice")
public interface UpdateRefNumberInService extends RemoteService{

	public void updateService(Date fromDate, Date toDate);

}
