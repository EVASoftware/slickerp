package com.slicktechnologies.client.views.contract;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;

public class ClientAssetPopup  extends AbsolutePanel{

	Button btnCancel, btnOk, btnReset;
	ClientAssetTable assetTable;
	ClientSideAsset assetEntity;

	public ClientAssetPopup() {

		assetEntity = new ClientSideAsset();

		InlineLabel remark1 = new InlineLabel("Contract Client Assets");
		add(remark1, 370, 10);
		remark1.getElement().getStyle().setFontSize(20, Unit.PX);

		// btnReset = new Button("Reset");
		// add(btnReset, 810, 117);

		assetTable = new ClientAssetTable();
		assetTable.connectToLocal();
		add(assetTable.getTable(), 7, 80);

		btnOk = new Button("Ok");
		btnCancel = new Button("Cancel");

		add(btnOk, 400, 300);
		add(btnCancel, 500, 300);

//		setSize("1010px", "560px");
//		this.getElement().setId("form");
		
		setSize("1000px", "400px");
		this.getElement().setId("form");
		
	}

	//   getters and setters 
	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public Button getBtnReset() {
		return btnReset;
	}

	public void setBtnReset(Button btnReset) {
		this.btnReset = btnReset;
	}

	public ClientAssetTable getAssetTable() {
		return assetTable;
	}

	public void setAssetTable(ClientAssetTable assetTable) {
		this.assetTable = assetTable;
	}

	public ClientSideAsset getAssetEntity() {
		return assetEntity;
	}

	public void setAssetEntity(ClientSideAsset assetEntity) {
		this.assetEntity = assetEntity;
	}
	
	
}
