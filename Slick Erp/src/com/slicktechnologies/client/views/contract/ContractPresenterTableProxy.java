package com.slicktechnologies.client.views.contract;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;
import java.util.Date;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.dom.client.Style.Unit;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.client.views.contract.ContractPresenter.ContractPresenterTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;

public class ContractPresenterTableProxy extends ContractPresenterTable {
	TextColumn<Contract> getCustomerCellNumberColumn;
	TextColumn<Contract> getCustomerFullNameColumn;
	TextColumn<Contract> getCustomerIdColumn;
	TextColumn<Contract> getContractCountColumn;
	TextColumn<Contract> getContractStartDateColumn;
	TextColumn<Contract> getContractEndDateColumn;
	TextColumn<Contract> getNetPayableColumn;
	TextColumn<Contract> getQuotationCountColumn;
	TextColumn<Contract> getLeadCountColumn;
	TextColumn<Contract> getBranchColumn;
	TextColumn<Contract> getEmployeeColumn;
	TextColumn<Contract> getStatusColumn;
	TextColumn<Contract> getCreationDateColumn;
	TextColumn<Contract> getGroupColumn;
	TextColumn<Contract> getCategoryColumn;
	TextColumn<Contract> getTypeColumn;
	TextColumn<Contract> getCountColumn;
	TextColumn<Contract>getColumnTicketID;
	
	/*** rohan added this for ankita pest control request *******/
	TextColumn<Contract> getRefNo;
	
	/**
	 * rohan added this code for NBHC for 
	 */

	TextColumn<Contract> getCreatedByColumn;
	
	/**
	 * Date : 24-01-2017 By Anil
	 * Added ref no. 2 for PCAAMB and Pest Cure Incorporation
	 */
	TextColumn<Contract> getRefNo2;
	/**
	 * End
	 */
	
	/*
	 *  nidhi
	 * 30-06-2017
	 */
	TextColumn<Contract> getSegmentColumn;
	
	//****************Date 04/09/2017 by jayshree add this product composite column****************
		TextColumn<Contract>getProductIdColumn;
		TextColumn<Contract>getProductCodeColumn;
		TextColumn<Contract>getProductNameColumn;
		TextColumn<Contract>getProductQtyColumn;
	
		/**03-10-2017 sagar sore [adding number range column in result of contract search pop up]**/
		TextColumn<Contract>getNumberRangeColumn;
		
		/**
		 * @author Anil
		 * @since 11-06-2020
		 * This flag is used to minimize the no. of contract fields for user
		 */
		boolean pedioContractFlag=false;
	
		public Object getVarRef(String varName)
	{
		if(varName.equals("getCustomerCellNumberColumn"))
			return this.getCustomerCellNumberColumn;
		if(varName.equals("getCustomerFullNameColumn"))
			return this.getCustomerFullNameColumn;
		if(varName.equals("getCustomerIdColumn"))
			return this.getCustomerIdColumn;
		if(varName.equals("getContractCountColumn"))
			return this.getContractCountColumn;
		if(varName.equals("getContractStartDateColumn"))
			return this.getContractStartDateColumn;
		if(varName.equals("getContractEndDateColumn"))
			return this.getContractEndDateColumn;
		if(varName.equals("getNetPayableColumn"))
			return this.getNetPayableColumn;
		if(varName.equals("getQuotationCountColumn"))
			return this.getQuotationCountColumn;
		if(varName.equals("getLeadCountColumn"))
			return this.getLeadCountColumn;
		if(varName.equals("getBranchColumn"))
			return this.getBranchColumn;
		if(varName.equals("getEmployeeColumn"))
			return this.getEmployeeColumn;
		if(varName.equals("getStatusColumn"))
			return this.getStatusColumn;
		if(varName.equals("getCreationDateColumn"))
			return this.getCreationDateColumn;
		if(varName.equals("getGroupColumn"))
			return this.getGroupColumn;
		if(varName.equals("getCategoryColumn"))
			return this.getCategoryColumn;
		if(varName.equals("getTypeColumn"))
			return this.getTypeColumn;
		if(varName.equals("getCountColumn"))
			return this.getCountColumn;
		if(varName.equals("getSegmentColumn"))
			return this.getSegmentColumn;
		//***Date 05/09/2017 by jayshree*****************
		if(varName.equals("getProductIdColumn"))
			return this.getProductIdColumn;
		if (varName.equals("getProductCodeColumn"))
			return this.getProductCodeColumn;
		if (varName.equals("getProductNameColumn"))
			return this.getProductNameColumn;
		return null ;
	}
	public ContractPresenterTableProxy()
	{
		super();
	}
	public ContractPresenterTableProxy(boolean pedioContractFlag) {
		// TODO Auto-generated constructor stub
		super(pedioContractFlag);
		this.pedioContractFlag=pedioContractFlag;
		createTable1();
	}
	@Override public void createTable() {
		/**03-10-2017 sagar sore [ Number range column added and order of column changed ]**/
		/**old order of columns**/
//		addColumngetCount();
//		addColumngetLeadCount();
//		addColumngetQuotationCount();
//		createColumnTicketID();
//		addColumnRefNumbar();
//		addColumngetRefNo2();
//		addColumngetCreatedBy();
//		addColumngetCustomerId();
//		addColumngetCustomerFullName();
//		addColumngetCustomerCellNumber();
//		addColumngetContractStartDate();
//		addColumngetContractEndDate();
//		addColumngetEmployee();
//		addColumngetBranch();
//		addColumngetCreationDate();
//		addColumngetGroup();
//		addColumngetCategory();
//		addColumngetType();
//		addColumngetStatus();
//		addColumngetSegment();
//		//************Date 05/09/2017 by jayshree************
//		addProductIdColumn();
//		addProductCodeColumn();
//		addProductNameColumn();
		
		/**updated order of column**/
		addColumngetCustomerFullName();
		addColumngetCustomerCellNumber();
		addColumngetCount();
		addProductNameColumn();
		addQuantityColumn();
	    addColumngetStatus();
		addColumngetContractStartDate();
		addColumngetContractEndDate();
		addColumnNetPayableColumn();
		addColumngetEmployee();
		addColumngetBranch();
		addColumngetNumberRange();
		addColumngetCategory();
		addColumngetType();
		addColumngetGroup();
		addColumngetSegment();
		addColumngetLeadCount();
		addColumngetQuotationCount();
		createColumnTicketID();
		addColumnRefNumbar();
		addColumngetRefNo2();
		addColumngetCustomerId();
		addColumngetCreationDate();
		addColumngetCreatedBy();
		
		//************Date 05/09/2017 by jayshree************
		addProductIdColumn();
		addProductCodeColumn();
		
	}
	
	//************Date 05/09/2017 by jayshree
	
		private void addProductIdColumn() {
			getProductIdColumn=new TextColumn<Contract>(){

				@Override
				public String getValue(Contract object) {
					// TODO Auto-generated method stub
					return getProductInfoList(object,"productid");
				}
			};
			table.addColumn(getProductIdColumn,"Product id");
			getProductIdColumn.setSortable(true);
			table.setColumnWidth(getProductIdColumn, 120, Unit.PX);
		}
		
		

		private void addProductCodeColumn() {
			getProductCodeColumn=new TextColumn<Contract>(){

				@Override
				public String getValue(Contract object) {
					
					return getProductInfoList(object,"Product Code");	
				}
				
			};
			table.addColumn(getProductCodeColumn,"Product Code");
			getProductCodeColumn.setSortable(true);
			table.setColumnWidth(getProductCodeColumn, 150, Unit.PX);
		}
		
		
		private void addProductNameColumn() {
			getProductNameColumn=new TextColumn<Contract>(){

				@Override
				public String getValue(Contract object) {
					// TODO Auto-generated method stub
					return getProductInfoList(object,"Product Name");
				}

				
			};
				table.addColumn(getProductNameColumn,"Product Name");
				getProductNameColumn.setSortable(true);
				table.setColumnWidth(getProductNameColumn, 180, Unit.PX);
		}
	
	protected void addColumngetSegment() {
		getSegmentColumn = new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				return object.getSegment() + "";
			}
		};
		table.addColumn(getSegmentColumn, "Segment");
		getSegmentColumn.setSortable(true);
		table.setColumnWidth(getSegmentColumn, 100, Unit.PX);

	}
	
	private void addColumngetRefNo2() {
		
		getRefNo2=new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				if(object.getRefNo2()!=null){
					return object.getRefNo2()+"";
				}else
					return "N.A.";
			}
		};
		table.addColumn(getRefNo2, "Ref No.2");
		table.setColumnWidth(getRefNo2,120,Unit.PX);
		getRefNo2.setSortable(true);
	}
	
	
	private void addSortinggetrefNumber2() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getRefNo2, new Comparator<Contract>() {
			@Override
			public int compare(Contract e1, Contract e2) {
				if (e1 != null && e2 != null) {
					if (e1.getRefNo2() != null && e2.getRefNo2() != null) {
						return e1.getRefNo2().compareTo(e2.getRefNo2());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	
	private void addSortingOnSegment() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getSegmentColumn, new Comparator<Contract>() {
			@Override
			public int compare(Contract e1, Contract e2) {
				if (e1 != null && e2 != null) {
					if (e1.getSegment() != null && e2.getSegment() != null) {
						return e1.getSegment().compareTo(e2.getSegment());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addColumngetCreatedBy() {
		
		getCreatedByColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				return object.getCreatedBy();
			}
				};
				table.addColumn(getCreatedByColumn,"Created By");
				table.setColumnWidth(getCreatedByColumn, 110, Unit.PX);
				getCreatedByColumn.setSortable(true);
	}
	
	
	protected void addSortinggetCreatedBy()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getCreatedByColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCreatedBy()!=null && e2.getCreatedBy()!=null){
						return e1.getCreatedBy().compareTo(e2.getCreatedBy());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	private void addColumnRefNumbar() {
		getRefNo=new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				if(object.getRefNo()!=null){
				return object.getRefNo()+"";
				}else
					return "N.A.";
			}
		};
		table.addColumn(getRefNo, "Ref No");
		table.setColumnWidth(getRefNo,120,Unit.PX);
		getRefNo.setSortable(true);
	}
	
	private void addSortinggetrefNumber() {
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getRefNo, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getRefNo()!=null && e2.getRefNo()!=null){
						return e1.getRefNo().compareTo(e2.getRefNo());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	private void createColumnTicketID() {
		getColumnTicketID=new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				if(object.getTicketNumber()!=-1){
				return object.getTicketNumber()+"";
				}else
					return "N.A.";
			}
		};
		table.addColumn(getColumnTicketID, "Ticket Id");
		table.setColumnWidth(getColumnTicketID,120,Unit.PX);
		getColumnTicketID.setSortable(true);
		
	}
	
	private void addColumnTicketNoSorting() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		Comparator<Contract> quote = new Comparator<Contract>() {
			@Override
			public int compare(Contract e1, Contract e2) {
				if (e1 != null && e2 != null) {
					if(e1.getTicketNumber()== e2.getTicketNumber()){
					  return 0;}
				  if(e1.getTicketNumber()> e2.getTicketNumber()){
					  return 1;}
				  else{
					  return -1;}} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnTicketID, quote);
		table.addColumnSortHandler(columnSort);
	}
	
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<Contract>()
				{
			@Override
			public Object getKey(Contract item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
				}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		if(pedioContractFlag){
			addSortingForPedioContract();
			return;
		}
		addSortinggetCount();
		addSortinggetBranch();
		addColumnTicketNoSorting();
		addSortinggetEmployee();
		addSortinggetStatus();
		addSortinggetLeadCount();
		addSortinggetContractStartDate();
		addSortinggetContractEndDate();
		addSortinggetGroup();
		addSortinggetQuotationCount();
		addSortinggetCategory();
		addSortinggetCustomerId();
		addSortinggetType();
		addSortinggetCustomerFullName();
		addSortinggetCustomerCellNumber();
		addSortinggetContractCount();
		addSortinggetCreationDate();
		addSortinggetrefNumber();
		addSortinggetCreatedBy();
		addSortinggetrefNumber2();
		addSortingOnSegment();
		/**03-10-2017 sagar sore [ adding sorting for number range]**/
        addSortinggetNumberRange();
	}
	
	@Override public void addFieldUpdater() {
	}
	protected void addSortinggetCount()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getCountColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"ID");

				table.setColumnWidth(getCountColumn,100,Unit.PX);
				getCountColumn.setSortable(true);
	}
	protected void addColumngetCreationDate()
	{
		getCreationDateColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				if(object.getCreationDate()!=null)
					return AppUtility.parseDate(object.getCreationDate());
				else
				return "N.A";
			
			}
				};
				table.addColumn(getCreationDateColumn,"Creation Date");
				table.setColumnWidth(getCreationDateColumn,100,Unit.PX);
				getCreationDateColumn.setSortable(true);
	}
	
	protected void addSortinggetCreationDate()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getCreationDateColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCreationDate()!=null && e2.getCreationDate()!=null){
						return e1.getCreationDate().compareTo(e2.getCreationDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetContractStartDate()
	{
		getContractStartDateColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				if(object.getCreationDate()!=null)
					return AppUtility.parseDate(object.getStartDate());
				else{
					return "N.A";
				}
			
			}
				};
				table.addColumn(getContractStartDateColumn,"Start Date");
				table.setColumnWidth(getContractStartDateColumn,100,Unit.PX);
				getContractStartDateColumn.setSortable(true);
	}
	
	protected void addSortinggetContractStartDate()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getContractStartDateColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStartDate()!=null && e2.getStartDate()!=null){
						return e1.getStartDate().compareTo(e2.getStartDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetContractEndDate()
	{
		getContractEndDateColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				if(object.getCreationDate()!=null)
					return AppUtility.parseDate(object.getEndDate());
				else{
					return "N.A";
				}
			
			}
				};
				table.addColumn(getContractEndDateColumn,"End Date");
				table.setColumnWidth(getContractEndDateColumn,100,Unit.PX);
				getContractEndDateColumn.setSortable(true);
	}
	
	protected void addSortinggetContractEndDate()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getContractEndDateColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEndDate()!=null && e2.getEndDate()!=null){
						return e1.getEndDate().compareTo(e2.getEndDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetBranch()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getBranchColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetBranch()
	{
		getBranchColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				return object.getBranch()+"";
			}
				};
				table.addColumn(getBranchColumn,"Branch");
				table.setColumnWidth(getBranchColumn, 100, Unit.PX);
				getBranchColumn.setSortable(true);
	}
	protected void addSortinggetEmployee()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getEmployeeColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployee()!=null && e2.getEmployee()!=null){
						return e1.getEmployee().compareTo(e2.getEmployee());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetEmployee()
	{
		getEmployeeColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				return object.getEmployee()+"";
			}
				};
				table.addColumn(getEmployeeColumn,"Sales Person");
				getEmployeeColumn.setSortable(true);
				table.setColumnWidth(getEmployeeColumn,110,Unit.PX);

	}
	protected void addSortinggetStatus()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				return object.getStatus()+"";
			}
				};
				table.addColumn(getStatusColumn,"Status");
				getStatusColumn.setSortable(true);
				table.setColumnWidth(getStatusColumn,100,Unit.PX);

	}
	protected void addSortinggetLeadCount()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getLeadCountColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getLeadCount()== e2.getLeadCount()){
						return 0;}
					if(e1.getLeadCount()> e2.getLeadCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetLeadCount()
	{
		getLeadCountColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				if( object.getLeadCount()==-1)
					return "N.A";
				else return object.getLeadCount()+"";
			}
				};
				table.addColumn(getLeadCountColumn,"Lead ID");
				table.setColumnWidth(getLeadCountColumn,100,Unit.PX);

				getLeadCountColumn.setSortable(true);
	}
	protected void addSortinggetGroup()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getGroupColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getGroup()!=null && e2.getGroup()!=null){
						return e1.getGroup().compareTo(e2.getGroup());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetGroup()
	{
		getGroupColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				return object.getGroup()+"";
			}
				};
				table.addColumn(getGroupColumn,"Group");
				table.setColumnWidth(getGroupColumn,120,Unit.PX);
				getGroupColumn.setSortable(true);
	}
	protected void addSortinggetQuotationCount()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getQuotationCountColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getQuotationCount()== e2.getQuotationCount()){
						return 0;}
					if(e1.getQuotationCount()> e2.getQuotationCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);


	}
	protected void addColumngetQuotationCount()
	{
		getQuotationCountColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				if( object.getQuotationCount()==-1)
					return "N.A";
				else return object.getQuotationCount()+"";
			}
				};
				table.addColumn(getQuotationCountColumn,"Quotation ID");
				table.setColumnWidth(getQuotationCountColumn,100,Unit.PX);
				getQuotationCountColumn.setSortable(true);
	}
	protected void addSortinggetCategory()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getCategoryColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCategory()!=null && e2.getCategory()!=null){
						return e1.getCategory().compareTo(e2.getCategory());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCategory()
	{
		getCategoryColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				return object.getCategory()+"";
			}
				};
				table.addColumn(getCategoryColumn,"Category");
				table.setColumnWidth(getCategoryColumn,120,Unit.PX);
				getCategoryColumn.setSortable(true);
	}
	protected void addSortinggetCustomerId()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getCustomerIdColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCustomerId()== e2.getCustomerId()){
						return 0;}
					if(e1.getCustomerId()> e2.getCustomerId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetCustomerId()
	{
		getCustomerIdColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				if( object.getCustomerId()==-1)
					return "N.A";
				else return object.getCustomerId()+"";
			}
				};
				table.addColumn(getCustomerIdColumn,"Customer ID");
				table.setColumnWidth(getCustomerIdColumn,110,Unit.PX);
				getCustomerIdColumn.setSortable(true);
	}
	protected void addSortinggetType()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getTypeColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getType()!=null && e2.getType()!=null){
						return e1.getType().compareTo(e2.getType());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetType()
	{
		getTypeColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				return object.getType()+"";
			}
				};
				table.addColumn(getTypeColumn,"Type");
				table.setColumnWidth(getTypeColumn,100,Unit.PX);
				getTypeColumn.setSortable(true);
	}
	protected void addSortinggetCustomerFullName()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getCustomerFullNameColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCustomerFullName()!=null && e2.getCustomerFullName()!=null){
						return e1.getCustomerFullName().compareTo(e2.getCustomerFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);

	}
	protected void addColumngetCustomerFullName()
	{
		getCustomerFullNameColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				return object.getCustomerFullName()+"";
			}
				};
				table.addColumn(getCustomerFullNameColumn,"Customer Name");
				table.setColumnWidth(getCustomerFullNameColumn,130,Unit.PX);
				getCustomerFullNameColumn.setSortable(true);
	}
	protected void addSortinggetCustomerCellNumber()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getCustomerCellNumberColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCustomerCellNumber()== e2.getCustomerCellNumber()){
						return 0;}
					if(e1.getCustomerCellNumber()> e2.getCustomerCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCustomerCellNumber()
	{
		getCustomerCellNumberColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				return object.getCustomerCellNumber()+"";
			}
				};
				table.addColumn(getCustomerCellNumberColumn,"Customer Cell");
				table.setColumnWidth(getCustomerCellNumberColumn,120,Unit.PX);
				getCustomerCellNumberColumn.setSortable(true);
	}
	protected void addSortinggetContractCount()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getContractCountColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getContractCount()== e2.getContractCount()){
						return 0;}
					if(e1.getContractCount()> e2.getContractCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetContractCount()
	{
		getContractCountColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				if( object.getContractCount()==-1)
					return "N.A";
				else return object.getContractCount()+"";
			}
				};
				table.addColumn(getContractCountColumn,"Contract Id");
				table.setColumnWidth(getContractCountColumn,150,Unit.PX);

				getContractCountColumn.setSortable(true);
	}
	
	/**03-10-2017 sagar sore [ Column for Number range]**/
	protected void addColumngetNumberRange() {
		getNumberRangeColumn = new TextColumn<Contract>() {
			@Override
			public String getValue(Contract object) {
				if (object.getNumberRange() != null)
					return object.getNumberRange() + "";
				else
					return "";
			}
		};
		table.addColumn(getNumberRangeColumn, "Number Range");
		table.setColumnWidth(getNumberRangeColumn, 150, Unit.PX);
		getNumberRangeColumn.setSortable(true);
	}

	protected void addSortinggetNumberRange() {
		List<Contract> list = getDataprovider().getList();
		columnSort = new ListHandler<Contract>(list);
		columnSort.setComparator(getNumberRangeColumn,new Comparator<Contract>() {
					@Override
					public int compare(Contract e1, Contract e2) {
						if (e1 != null && e2 != null) {
							
							if (e1.getNumberRange() == null && e2.getNumberRange() == null){
								return 0;
							}
							if(e1.getNumberRange() == null) return 1;							
							if(e2.getNumberRange() == null) return -1;							
							return e1.getNumberRange().compareTo(e2.getNumberRange());

						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}
	/**end**/
	
	private String getProductInfoList(Contract object, String producttype) {
		// TODO Auto-generated method stub
		String name="";
		
		switch(producttype){
		
		
		case "productid":
			
		for(SalesLineItem product:object.getItems()){
			name = name+product.getPrduct().getCount()+" ,";
		}
		break;
		case "Product Code":
			System.out.println("projectcode");
		for(SalesLineItem product:object.getItems()){
			name = name+product.getPrduct().getProductCode()+" ,";
		}
		break;
		case "Product Name":
		for(SalesLineItem product:object.getItems()){
			name = name+product.getProductName()+" ,";
		}
		break;
		case "Quantity":			
			for(SalesLineItem product:object.getItems()){
				if(product.getArea()!=null)
					name = name+product.getArea()+" ,";
				else
					name = name+"NA ,";
			}
			break;
		default:
			break;
			}
		try{
			if(!name.equals(""))
				name=name.substring(0,name.length()-1);
		}catch(Exception e){
			
		}
		
		return name;
	}
	
	public void createTable1() {
		
		addColumngetCustomerFullName();
		addColumngetCustomerCellNumber();
		addColumngetCount();
	    addColumngetStatus();
		addColumngetContractStartDate();
		addColumngetContractEndDate();
		addColumnNetPayableColumn();
		addColumngetBranch();
		addColumngetCustomerId();
		addColumngetCreationDate();
		addColumngetCreatedBy();
		addProductIdColumn();
		addProductCodeColumn();
		addProductNameColumn();
		addQuantityColumn();
	}
	
	public void addSortingForPedioContract(){

		addSortinggetCount();
		addSortinggetBranch();
		addSortinggetStatus();
		addSortinggetContractStartDate();
		addSortinggetContractEndDate();
		addSortinggetCustomerId();
		addSortinggetCustomerFullName();
		addSortinggetCustomerCellNumber();
		addSortinggetContractCount();
		addSortinggetCreationDate();
		addSortinggetCreatedBy();
	
	}
	
	
	protected void addColumnNetPayableColumn()
	{
		getNetPayableColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				return object.getNetpayable()+"";
			
			}
				};
				table.addColumn(getNetPayableColumn,"Net Payable");
				table.setColumnWidth(getNetPayableColumn,100,Unit.PX);
				getNetPayableColumn.setSortable(true);
	}
	private void addQuantityColumn() {
		getProductQtyColumn=new TextColumn<Contract>(){

			@Override
			public String getValue(Contract object) {
				// TODO Auto-generated method stub
				return getProductInfoList(object,"Quantity");
			}

			
		};
			table.addColumn(getProductQtyColumn,"Qty");
			getProductQtyColumn.setSortable(true);
			table.setColumnWidth(getProductQtyColumn, 100, Unit.PX);
	}
}
