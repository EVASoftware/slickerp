package com.slicktechnologies.client.views.contract;

import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Timer;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListPresenter;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;

public class CancelContractTable extends SuperTable<CancelContract> {

	TextColumn<CancelContract> getColumnDocumentId;
	TextColumn<CancelContract> getColumnDocumentName;
	TextColumn<CancelContract> getColumnBPName;
	TextColumn<CancelContract> getColumnBranch;
	TextColumn<CancelContract> getColumnStatus;
	
	Column<CancelContract, Boolean> checkColumn;
	Header<Boolean> selectAllHeader;
	protected GWTCGlassPanel glassPanel;
	
	
	
	@Override
	public void createTable() {
		addCheckBoxCell();
		createColumnDocumentId();
		createColumnDocumentName();
		createColumnBPName();
		createColumnBranch();
		createColumnStatus();
		
		
	}

	public void createColumnDocumentId(){
		
		getColumnDocumentId=new TextColumn<CancelContract>() {

				@Override
				public String getValue(CancelContract object) {
					if(object.getDocumnetId()!= 0)
					{
						return object.getDocumnetId()+"";
					}
					return "";
				}
			};
			
			table.addColumn(getColumnDocumentId,"Document Id");
			
			
		}	
	
public void createColumnDocumentName(){
		
		getColumnDocumentName=new TextColumn<CancelContract>() {

				@Override
				public String getValue(CancelContract object) {
					if(object.getDocumnetName()!= null)
					{
						return object.getDocumnetName();
					}
					return "";
				}
			};
			
			table.addColumn(getColumnDocumentName,"Document Name");
			
			
		}	

public void createColumnBPName(){
	
	getColumnBPName=new TextColumn<CancelContract>() {

			@Override
			public String getValue(CancelContract object) {
				if(object.getBpName()!= null)
				{
					return object.getBpName();
				}
				return "";
			}
		};
		
		table.addColumn(getColumnBPName,"BP Name");
		
		
	}

public void createColumnBranch(){
	
	getColumnBranch=new TextColumn<CancelContract>() {

			@Override
			public String getValue(CancelContract object) {
				if(object.getBranch()!= null)
				{
					return object.getBranch();
				}
				return "";
			}
		};
		
		table.addColumn(getColumnBranch,"Branch");
		
		
	}

   public void createColumnStatus(){
	
	getColumnStatus=new TextColumn<CancelContract>() {

			@Override
			public String getValue(CancelContract object) {
				if(object.getDocumentStatus()!= null)
				{
					return object.getDocumentStatus();
				}
				return "";
			}
		};
		
		table.addColumn(getColumnStatus,"Status");
		
    }
		private void addCheckBoxCell() {
			
			checkColumn=new Column<CancelContract, Boolean>(new CheckboxCell()) {
				@Override
				public Boolean getValue(CancelContract object) {
					return object.isRecordSelect();
				}
			};
			
			checkColumn.setFieldUpdater(new FieldUpdater<CancelContract, Boolean>() {
				@Override
				public void update(int index, CancelContract object, Boolean value) {
					System.out.println("Check Column ....");
					object.setRecordSelect(value);
					table.redrawRow(index);
					selectAllHeader.getValue();
					table.redrawHeaders();
				
				}
			});
			
			
			selectAllHeader =new Header<Boolean>(new CheckboxCell()) {
				@Override
				public Boolean getValue() {
					if(getDataprovider().getList().size()!=0){
						
					}
					return false;
				}
			};
			
			
			selectAllHeader.setUpdater(new ValueUpdater<Boolean>() {
				@Override
				public void update(Boolean value) {
					List<CancelContract> list=getDataprovider().getList();
					for(CancelContract object:list){
						object.setRecordSelect(value);
					}
					getDataprovider().setList(list);
					table.redraw();
					addColumnSorting();
				}
			});
			table.addColumn(checkColumn,selectAllHeader);

		
	}

	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

	
}

