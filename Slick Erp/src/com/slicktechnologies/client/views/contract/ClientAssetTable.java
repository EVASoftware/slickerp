package com.slicktechnologies.client.views.contract;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.views.contractrenewal.RenewalResult;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;

public class ClientAssetTable extends SuperTable<ClientSideAsset>{


	TextColumn<ClientSideAsset> nameColumn;
	TextColumn<ClientSideAsset> statusColumn;
	TextColumn<ClientSideAsset> idColumn;
	TextColumn<ClientSideAsset> brandColumn;
	TextColumn<ClientSideAsset> modelNoColumn;
	TextColumn<ClientSideAsset> srNoColumn;
	TextColumn<ClientSideAsset> lattitudeColumn;
	TextColumn<ClientSideAsset> longitudeColumn;
	Column<ClientSideAsset, Date> manufactureDateColumn;
	Column<ClientSideAsset, Date> installationDateColumn;
	Column<ClientSideAsset, String> deleteColumn;

	// *********vaishnavi***************
	Column<ClientSideAsset, Boolean> checkRecord;

	ListHandler<ClientSideAsset> columnSort;
	GenricServiceAsync service = GWT.create(GenricService.class);

	public ClientAssetTable() {
		super();
		// addNameSorting();

	}

	public ClientAssetTable(UiScreen<ClientSideAsset> view) {

		super(view);

	}

	private void createColumnName() {
		nameColumn = new TextColumn<ClientSideAsset>() {
			@Override
			public String getValue(ClientSideAsset object) {
				return object.getName();
			}
		};
		table.addColumn(nameColumn, "Name");
		table.setColumnWidth(nameColumn, "60px");
		nameColumn.setSortable(true);
	}

	private void createColumnBrand() {
		brandColumn = new TextColumn<ClientSideAsset>() {
			@Override
			public String getValue(ClientSideAsset object) {
				return object.getBrand();
			}
		};
		table.addColumn(brandColumn, "Brand");
		table.setColumnWidth(brandColumn, "60px");
		brandColumn.setSortable(true);
	}

	private void createColumnModelNo() {
		modelNoColumn = new TextColumn<ClientSideAsset>() {
			@Override
			public String getValue(ClientSideAsset object) {
				return object.getModelNo();
			}
		};
		table.addColumn(modelNoColumn, "Model Number");
		table.setColumnWidth(modelNoColumn, "90px");
		modelNoColumn.setSortable(true);
	}

	private void createColumnSrNo() {
		srNoColumn = new TextColumn<ClientSideAsset>() {
			@Override
			public String getValue(ClientSideAsset object) {
				return object.getSrNo();
			}
		};
		table.addColumn(srNoColumn, "Sr Number");
		table.setColumnWidth(srNoColumn, "90px");
		srNoColumn.setSortable(true);
	}

	private void createColumnLattitude() {
		lattitudeColumn = new TextColumn<ClientSideAsset>() {
			@Override
			public String getValue(ClientSideAsset object) {
				return object.getLattitude();
			}
		};
		table.addColumn(lattitudeColumn, "Lattitude");
		table.setColumnWidth(lattitudeColumn, "80px");
		lattitudeColumn.setSortable(true);
	}

	private void createColumnLongitude() {
		longitudeColumn = new TextColumn<ClientSideAsset>() {
			@Override
			public String getValue(ClientSideAsset object) {
				return object.getLongitude();
			}
		};
		table.addColumn(longitudeColumn, "Longitude");
		table.setColumnWidth(longitudeColumn, "80px");
		longitudeColumn.setSortable(true);
	}

	private void createColumnManufactureDate() {
		DateTimeFormat fmt = DateTimeFormat
				.getFormat(PredefinedFormat.DATE_SHORT);
		DatePickerCell date = new DatePickerCell(fmt);
		manufactureDateColumn = new Column<ClientSideAsset, Date>(date) {
			@Override
			public Date getValue(ClientSideAsset object) {
				return object.getDateOfManufacture();
			}
		};
		table.addColumn(manufactureDateColumn, "Manufacture Date");
		table.setColumnWidth(manufactureDateColumn, "110px");
		manufactureDateColumn.setSortable(true);
	}

	private void createColumnInstallationDate() {
		DateTimeFormat fmt = DateTimeFormat
				.getFormat(PredefinedFormat.DATE_SHORT);
		DatePickerCell date = new DatePickerCell(fmt);
		installationDateColumn = new Column<ClientSideAsset, Date>(date) {
			@Override
			public Date getValue(ClientSideAsset object) {
				return object.getDateOfInstallation();
			}
		};
		table.addColumn(installationDateColumn, "Installation Date");
		table.setColumnWidth(installationDateColumn, "110px");
		installationDateColumn.setSortable(true);
	}

	protected void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<ClientSideAsset, String>(btnCell) {
			@Override
			public String getValue(ClientSideAsset object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "Delete");
		table.setColumnWidth(deleteColumn, "60px");
		
	}

	public void createTable() {
		addColumnCheckBox();
		createColumnName();
		// createColumnId();
		createColumnBrand();
		createColumnModelNo();
		createColumnSrNo();
		createColumnLattitude();
		createColumnLongitude();
		createColumnManufactureDate();
		createColumnInstallationDate();
		createColumndeleteColumn();

		table.setWidth("100%");
		setFieldupdaterName();
		setFieldupdaterBrand();
		setFieldupdaterModelNo();
		setFieldupdaterSrNo();
		setFieldupdaterManufactureDate();
		setFieldupdaterInstallationDate();
		createFieldUpdaterdeleteColumn();

		// **********vaishnavi************
		setFieldUpdaterOnCheckBox();

	}

	private void addColumnCheckBox() {

		CheckboxCell checkCell = new CheckboxCell();
		checkRecord = new Column<ClientSideAsset, Boolean>(checkCell) {

			@Override
			public Boolean getValue(ClientSideAsset object) {

				return object.isRecordSelect();
			}
		};
		table.addColumn(checkRecord, "Select");
		table.setColumnWidth(checkRecord, 60, Unit.PX);

	}

	private void setFieldUpdaterOnCheckBox() {

		System.out.println("inside check box updator..");
		checkRecord
				.setFieldUpdater(new FieldUpdater<ClientSideAsset, Boolean>() {

					@Override
					public void update(int index, ClientSideAsset object,
							Boolean value) {
						try {
							Boolean val1 = (value);
							object.setRecordSelect(val1);
						} catch (Exception e) {
						}
						table.redrawRow(index);

					}
				});
	}

	protected void createFieldUpdaterdeleteColumn() {
		deleteColumn
				.setFieldUpdater(new FieldUpdater<ClientSideAsset, String>() {
					@Override
					public void update(int index, ClientSideAsset object,
							String value) {
						getDataprovider().getList().remove(object);

						table.redrawRow(index);
					}
				});
	}

	private void setFieldupdaterName() {
		nameColumn.setFieldUpdater(new FieldUpdater<ClientSideAsset, String>() {

			@Override
			public void update(int index, ClientSideAsset object, String value) {
				object.setName(value);
				table.redrawRow(index);

			}
		});

	}

	private void setFieldupdaterBrand() {
		brandColumn
				.setFieldUpdater(new FieldUpdater<ClientSideAsset, String>() {

					@Override
					public void update(int index, ClientSideAsset object,
							String value) {
						object.setBrand(value);
						table.redrawRow(index);

					}
				});

	}

	private void setFieldupdaterModelNo() {
		modelNoColumn
				.setFieldUpdater(new FieldUpdater<ClientSideAsset, String>() {

					@Override
					public void update(int index, ClientSideAsset object,
							String value) {
						object.setModelNo(value);
						table.redrawRow(index);

					}
				});

	}

	private void setFieldupdaterSrNo() {
		srNoColumn.setFieldUpdater(new FieldUpdater<ClientSideAsset, String>() {

			@Override
			public void update(int index, ClientSideAsset object, String value) {
				object.setSrNo(value);
				table.redrawRow(index);

			}
		});

	}

	private void setFieldupdaterManufactureDate() {
		manufactureDateColumn
				.setFieldUpdater(new FieldUpdater<ClientSideAsset, Date>() {

					@Override
					public void update(int index, ClientSideAsset object,
							Date value) {
						object.setDateOfManufacture(value);
						table.redrawRow(index);

					}

				});

	}

	private void setFieldupdaterInstallationDate() {
		installationDateColumn
				.setFieldUpdater(new FieldUpdater<ClientSideAsset, Date>() {

					@Override
					public void update(int index, ClientSideAsset object,
							Date value) {
						object.setDateOfInstallation(value);
						table.redrawRow(index);

					}

				});

	}

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<ClientSideAsset>() {
			@Override
			public Object getKey(ClientSideAsset item) {
				if (item == null)
					return null;
				else
					return item.getId();
			}
		};

	}

	// Since config table live status depends upon the type hence querry
	// Structure will be differnt

	public void addColumnSorting() {

		addNameSorting();

	}

	public void addNameSorting() {
		List<ClientSideAsset> list = getDataprovider().getList();
		columnSort = new ListHandler<ClientSideAsset>(list);
		columnSort.setComparator(nameColumn, new Comparator<ClientSideAsset>() {

			@Override
			public int compare(ClientSideAsset e1, ClientSideAsset e2) {
				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.");
				if (e1 != null && e2 != null) {
					if (e1.getName() != null && e2.getName() != null)
						return e1.getName().compareTo(e2.getName());
					return 0;
				} else
					return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	@Override
	public void setEnable(boolean state) {

	}

	public void applyStyle() {
	}

	@Override
	public void addFieldUpdater() {

	}

}
