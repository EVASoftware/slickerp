package com.slicktechnologies.client.views.contract;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.slicktechnologies.client.login.LoginPresenter;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.quickcontract.QuickContractPresentor;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.ComponentDetails;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;


public class SalesLineItemTable extends SuperTable<SalesLineItem>  implements ClickHandler
{
	TextColumn<SalesLineItem> prodCategoryColumn,prodCodeColumn;
	Column<SalesLineItem,String> prodQtyColumn,prodNameColumn;
	
	
	TextColumn<SalesLineItem> viewprodQtyColumn;
	Column<SalesLineItem,Date> dateColumn;
	TextColumn<SalesLineItem> viewdColumn;
	Column<SalesLineItem,String> durationColumn,noServicesColumn,servicingTime,discountAmt;
	TextColumn<SalesLineItem> viewdurationColumn,viewnoServicesColumn,viewServicingTime,viewdiscountAmt;
	Column<SalesLineItem, String> priceColumn,premisesDetails;
	TextColumn<SalesLineItem> viewPriceColumn,viewpremisesDetails;
	
	/************ vijay for area wise **************/
	Column<SalesLineItem, String> prodAreaColumn;
	TextColumn<SalesLineItem> viewProdAreaColumn;
	
	//*********rohan remove this 
	TextColumn<SalesLineItem> createColumn;
	TextColumn<SalesLineItem> productSrNo;
	
	Column<SalesLineItem,String> vatColumn,serviceColumn;
	
	TextColumn<SalesLineItem> viewServiceTaxColumn;
	TextColumn<SalesLineItem> viewVatTaxColumn;
	
	public  ArrayList<TaxDetails> vattaxlist;
	public  ArrayList<TaxDetails> servicetaxlist;
	public  ArrayList<String> list;
	public  ArrayList<String> vatlist;
	//*************changes ends here **************
	
	Column<SalesLineItem, String>prodPercentDiscColumn;
	TextColumn<SalesLineItem> viewprodPercDiscColumn;
	Column<SalesLineItem,String> deleteColumn;
	Column<SalesLineItem,String> totalColumn;
	
	NumberFormat nf=NumberFormat.getFormat("0.00");
	int productIdCount=0;
	int productSrNoCount=0;
	
	public static boolean newModeFlag=false;
	/**
	 * Date : 04-07-2017 BY NIDHI
	 */
	public static boolean newBranchFlag=true;
	/**
	 * End
	 */
	
	// date 7 feb 2017 added by vijay
	TextColumn<SalesLineItem> columnviewRemark;
	Column<SalesLineItem, String> columnRemark;

	
	/**
	 * Date : 24-03-2017 By ANIL
	 * Adding button branches,which will be used to map branches in services shown on the schedule popup
	 * NBHC CCPM,PCAAMB
	 */
	
	Column<SalesLineItem, String> getBranchButtonColumn;
	AdvanceSchedulingPopup advSchPopup=new AdvanceSchedulingPopup();
	AdvanceSchedulingViewPopup advSchViewPopup=new AdvanceSchedulingViewPopup(); //Ashwini Patil Date:25-09-2022
	PopupPanel panel=new PopupPanel();
	int rowIndex=0;
	public String documentType="";
	/**
	 * End
	 */
	
	/**
	 * rohan added this code for adding HSN code in prouct
	 */
	
	TextColumn<SalesLineItem> viewHsnCodeColumn;
	Column<SalesLineItem,String> hsnCodeColumn;
	/**
	 * ends here  
	 */
	
	
	/**
	 * Date :08-08-2017 By ANIL
	 * adding service end date
	 */
	Column<SalesLineItem,Date> serviceEndDateCol;
	TextColumn<SalesLineItem> viewServiceEndDateCol;
	/**
	 * End
	 */
	
	/**Date :17/02/2018 BY: Manisha 
	 * Add frequency & TypeOfTreatment column  at product level as per sonu's requirement.!! 
	 */
	public ArrayList<String> frequencylist;
	public ArrayList<String> typeoftreatmentlist;
	
	Column<SalesLineItem,String> prodfrequencycolumn;
	TextColumn<SalesLineItem> viewprodfrequencycolumn;
	
	Column<SalesLineItem,String> typeoftreatmentcolumn;
	TextColumn<SalesLineItem> viewtypeoftreatmentcolumn;
	
	/**END**/
	
	/**Added by Sheetal : 21-03-2022
	 * Des : Adding Unit of Measurement column at product level,requirement by Nitin sir**/
	ArrayList<String> UOM = new ArrayList<String>();
	Column<SalesLineItem,String> UOMcolumn;
	TextColumn<SalesLineItem> viewUOMcolumn;
	/**end**/
	
	/**
	 * Date 06-06-2018 By vijay
	 * Des :- warranty of the product
	 * Requirement :- Neatedge Services
	 */
	Column<SalesLineItem, String> getcolWarrantyPeriod;
	TextColumn<SalesLineItem> viewcolWarrantyPeriod;
	/**
	 * ends here
	 */
	
	/**
	 * Date 06-jun-2018
	 * Developer :- Vijay
	 * Des :- Inclusive tax calculation
	 * Requirements :- Neatedge services 
	 */
	
	Column<SalesLineItem,Boolean> getColIsTaxInclusive;
	TextColumn<SalesLineItem> viewColIsTaxInclusive;
	
	GWTCAlert alert = new GWTCAlert();

	/**
	 * nidhi
	 * 8-08-2018
	 * for map pro model , serialno
	 */
	Column<SalesLineItem, String> getProModelNo;
	Column<SalesLineItem, String> getProSerialNo;
	TextColumn<SalesLineItem> getViewProModelNo, getViewProSerialNo;
	/**
	 * ends here
	 */
	
	//NOTE//
	
	//   19December
	
	// In this table the total amount column is changed.
	// The value of total amount is calc by price*qty. And taxes which are inclusive are subtracted from price*qty
	/**
	 * nidhi
	 * 11-09-2018 for billofmaterial
	 */
	ArrayList<String> areaUnit = new ArrayList<String>();
	Column<SalesLineItem,String> unitColumn;
	
	TextColumn<SalesLineItem> viewUnitColumn;
	public ContractForm form;
	/**
	 * end
	 */
	/** date 11.12.2018 added by komal for nbhc service branches button and popup **/
	Column<SalesLineItem, String> getServiceBranchesButton;
	ServiceBranchesPopup serviceBranchesPopup = new ServiceBranchesPopup();
	/**
	 * end komal
	 * 
	 * 
	 */
	/**Date 14-9-2019 by Amol ***/
	public static int quotationCount=0;
	
	/**
	 * @author Anil ,Date : 16-11-2019
	 * Make price column non editable and make editable only price is zero
	 * raised by rahul tiwari for orkins
	 */
	boolean disablePriceCol=false;
	
	/**
	 * @author Anil
	 * @since 11-06-2020
	 * This flag is used to minimize the no. of contract fields for user
	 */
	boolean pedioContractFlag=false;
	
	boolean complainTatFlag=false;
	
	public SalesLineItemTable()
	{
		super();
		advSchPopup.btnOk.addClickHandler(this);
		advSchPopup.btnCancel.addClickHandler(this);
		advSchViewPopup.btnCancel.addClickHandler(this);
		/** date 11.12.2018 added by komal for nbhc service branches button and popup **/
		serviceBranchesPopup.getLblOk().addClickHandler(this);
		serviceBranchesPopup.getLblCancel().addClickHandler(this);
		table.setHeight("300px");
		
		complainTatFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ComplainServiceWithTurnAroundTime");
	}
	public SalesLineItemTable(UiScreen<SalesLineItem> view)
	{
		super(view);	
		advSchPopup.btnOk.addClickHandler(this);
		advSchPopup.btnCancel.addClickHandler(this);
		advSchViewPopup.btnCancel.addClickHandler(this);
		/** date 11.12.2018 added by komal for nbhc service branches button and popup **/
		serviceBranchesPopup.getLblOk().addClickHandler(this);
		serviceBranchesPopup.getLblCancel().addClickHandler(this);
		table.setHeight("300px");
		
		complainTatFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ComplainServiceWithTurnAroundTime");
	}

	public void createTable()
	{
		complainTatFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ComplainServiceWithTurnAroundTime");
		
		if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation", "MakeColumnDisable")) {
			disablePriceCol=true;
			Console.log("dis flag value : "+disablePriceCol);
		}
		if(newModeFlag==false){
//		createColumnprodCategoryColumn();
//			createColumnprodSrNoColumn();
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","MAKEPRODUCTNAMENONEDITABLE")){
        	createViewColumnprodNameColumn();
        }else{
            createColumnprodNameColumn();
        }
		createColumndateColumn();
		/**
		 * Date : 08-08-2017 By ANIL
		 */
		createColumnServiceEndDateColumn();
		/**
		 * End
		 */
		
		
		/**Date :17/02/2018 BY: Manisha 
		 * Add frequency & TypeOfTreatment column  at product level as per sonu's requirement.!! 
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","FrequencyAtProductLevel")){
		createColumnprodFrequencyColumn();
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","TypeOfTreatmentAtProductLevel")){
		createColumnTypeOfTreatmentColumn();	
		}
		
		/**End**/
		
		createColumndurationColumn();
		createColumnnoServicesColumn();
		createColumnUOMColumn();
		
		
//		createColumnprodQtyColumn();
		
		createColumnprodAreaColumn();
		/**
		 *  *:*:*
		 * nidhi
		 * 11-09-2018
		 */
		if(LoginPresenter.billofMaterialActive){
			if(areaUnit != null && areaUnit.size()>0)
				areaUnit.clear();
			else{
				areaUnit = new ArrayList<String>();
			}
			
			for(Config cfg : LoginPresenter.unitForService){
				areaUnit.add(cfg.getName());
			}
			addEditColumnAreaUnit();
		}
		
		/**
		 * nidhi
		 * 8-08-2018
		 * for map model no and serialno
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			createColumnProdModelNo();
			createColumnProdSerialNo();
		}
		
//		if (AppUtility.checkForProcessConfigurartionIsActiveOrNot(
//				"Quotation", "MakeColumnDisable")) {
//			createViewColumnpriceColumn();
//		} else {
			createColumnpriceColumn();
//		}
		createColumnPercDiscount();
		
		//*************rohan added this for discount amt *************8
		createColumnDiscountAmt();
		createColumntotalColumn();
		
//		createColumnvatColumn();
		
//		createColumnserviceColumn();
		retriveServiceTax();
		
//		createColumnPercDiscount();
//		createColumntotalColumn();
//		createColumndeleteColumn();
//		addFieldUpdater();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);	
		}
		}
	
	
	
	
	private void createColumnUOMColumn() {
		if(UOM != null && UOM.size()>0)
			UOM.clear();
		else{
			UOM = new ArrayList<String>();
		}
		    UOM.add("--SELECT--");
		for(Config cfg : LoginPresenter.unitForProduct){
			UOM.add(cfg.getName());
		}
		SelectionCell UOMselection = new SelectionCell(UOM);
		UOMcolumn = new Column<SalesLineItem, String>(UOMselection){

			@Override
			public String getValue(SalesLineItem object) {
				if(object.getUnitOfMeasurement()!=null&&!object.getUnitOfMeasurement().equals("")){
					return object.getUnitOfMeasurement();
				}else{
					return "NA";
				}
			}
		};
		table.addColumn(UOMcolumn, "#UOM");
		table.setColumnWidth(UOMcolumn, 100, Unit.PX);
	}
	/**Date :17/02/2018 BY: Manisha 
	 * Add frequency & TypeOfTreatment column  at product level as per sonu's requirement.!! 
	 */
	 private void createColumnprodFrequencyColumn() {
			
		   makefrequencydropdown();
		   System.out.println("GGGGGGGGGGGGG"+frequencylist.size());
			SelectionCell freSelectionCell= new SelectionCell(frequencylist);
			
			prodfrequencycolumn = new Column<SalesLineItem, String>(freSelectionCell) {
				@Override
				public String getValue(SalesLineItem object) {
					// TODO Auto-generated method stub
					if(object.getProfrequency()!=null)
					return object.getProfrequency();
					else
					return"";
					
				}
			};
			table.addColumn(prodfrequencycolumn, "#Frequency");
			table.setColumnWidth(prodfrequencycolumn, 100, Unit.PX);
			
			
			prodfrequencycolumn.setFieldUpdater(new FieldUpdater<SalesLineItem,String>(){
				
				@Override
				public void update(int index, SalesLineItem object, String value) {
					/**
					 * Date : 18-12-2017 BY ANIL
					 **/
					if(value.equalsIgnoreCase("--SELECT--")){
						object.setProfrequency("");
					}else{
						object.setProfrequency(value);
					}
					/**
					 * End
					 */
				
				    RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
						
					table.redrawRow(index);
				}
			});
		}
	 
	 
	 private void createColumnTypeOfTreatmentColumn() {
		   maketypeoftreatmentdropdown();
		   SelectionCell typeoftreatSelectionCell= new SelectionCell(typeoftreatmentlist);
		   typeoftreatmentcolumn = new Column<SalesLineItem, String>(typeoftreatSelectionCell) {
				@Override
				public String getValue(SalesLineItem object) {
					
					if(object.getTypeoftreatment()!=null)
					return object.getTypeoftreatment();
					else
					return"";
					
				}
			};
			table.addColumn(typeoftreatmentcolumn, "#Type Of Treatment");
			table.setColumnWidth(typeoftreatmentcolumn, 100, Unit.PX);
			
			
			typeoftreatmentcolumn.setFieldUpdater(new FieldUpdater<SalesLineItem,String>(){
				@Override
				public void update(int index, SalesLineItem object, String value) {
					if(value.equalsIgnoreCase("--SELECT--")){
						object.setTypeoftreatment("");
					}else{
						object.setTypeoftreatment(value);
					}
				
				    RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
						
					table.redrawRow(index);
			}
		});
	}
	
	 /**End**/
	/**
	 * Date : 08-08-2017 BY ANIL
	 */
	
	private void createColumnServiceEndDateColumn() {
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date = new DatePickerCell(fmt);
		serviceEndDateCol = new Column<SalesLineItem, Date>(date) {
			@Override
			public Date getValue(SalesLineItem object) {
				if (object.getEndDate() != null) {
					return fmt.parse(fmt.format(object.getEndDate()));
				} else {
					Date date=CalendarUtil.copyDate(object.getStartDate());
					CalendarUtil.addDaysToDate(date,object.getDuration()-1);
					object.setEndDate(date);
					return fmt.parse(fmt.format(date));
				}
			}
		};
		table.addColumn(serviceEndDateCol, "#End Date");
		table.setColumnWidth(serviceEndDateCol, 100, Unit.PX);
		
	}
	
	
	protected void addFieldUpdaterOnServiceEndDateColumn()
	{
		serviceEndDateCol.setFieldUpdater(new FieldUpdater<SalesLineItem, Date>() {
			@Override
			public void update(int index, SalesLineItem object, Date value) {
				object.setEndDate(value);
				int duration=CalendarUtil.getDaysBetween(object.getStartDate(), object.getEndDate());
				duration++;
				object.setDuration(duration);
				System.out.println("Updated End Date : "+value+" ST DATE : "+object.getStartDate()+" EN DATE : "+object.getEndDate());
				System.out.println("Contract Duration : "+duration+"  Dur "+object.getDuration());
				productIdCount = object.getPrduct().getCount();
				productSrNoCount = object.getProductSrNo();
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
		});
	}
	
	protected void createViewColumnServiceEndDateColumn() {
		viewServiceEndDateCol = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getEndDate() != null) {
					return AppUtility.parseDate(object.getEndDate());
				} else {
					return "N.A.";
				}
			}
		};
		table.addColumn(viewServiceEndDateCol, "#End Date");
		table.setColumnWidth(viewServiceEndDateCol, 100, Unit.PX);
	}
	
	/**
	 * End
	 */
	
	
	private void createColumnprodHSNCodeColumn() {
		EditTextCell editCell=new EditTextCell();
		hsnCodeColumn=new Column<SalesLineItem,String>(editCell)
		{
			@Override
			public String getValue(SalesLineItem object)
			{
				
				if(object.getPrduct().getHsnNumber()!=null && !object.getPrduct().getHsnNumber().equals("")){
					return object.getPrduct().getHsnNumber();
				}
				else{
					return "";
				}
			}
		};
		table.addColumn(hsnCodeColumn,"#HSN/SAC Code");
		table.setColumnWidth(hsnCodeColumn, 100,Unit.PX);
	}
	
	private void createColumnViewHSNCodeColumn() {
		
		viewHsnCodeColumn=new TextColumn<SalesLineItem>()
			{
				@Override
				public String getValue(SalesLineItem object)
				{
					return object.getPrduct().getHsnNumber();
				}
			};
			table.addColumn(viewHsnCodeColumn,"#SAC Code");  // Ajinkya HSN code to SAC code On Date : 08-08-2017
			table.setColumnWidth(viewHsnCodeColumn, 100,Unit.PX);
	}
	
	
	private void createColumnprodSrNoColumn() {
		
			productSrNo=new TextColumn<SalesLineItem>()
				{
					@Override
					public String getValue(SalesLineItem object)
					{
							return object.getProductSrNo()+"";}
				};
				table.addColumn(productSrNo,"Prod SrNo");
				table.setColumnWidth(productSrNo, 100,Unit.PX);
	}
	private void createColumnServicingTimeColumn() {
		EditTextCell editCell=new EditTextCell();
		servicingTime=new Column<SalesLineItem,String>(editCell)
		{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getServicingTIme()==0){
					return 0+"";
				}
				else{
					return object.getServicingTIme()+"";
				}
			}
		};
		table.addColumn(servicingTime,"#Ser.Time(Hrs)");
		table.setColumnWidth(servicingTime, 100,Unit.PX);
		
	}
	
	private void createViewColumnServicingTimeColumn() {
		viewServicingTime=new TextColumn<SalesLineItem>()
		{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getServicingTIme()==0){
					return "N.A"+"";}
				else{
					return object.getServicingTIme()+"";}
			}
		};
		table.addColumn(viewServicingTime,"#Ser.Time(Hrs)");
		table.setColumnWidth(viewServicingTime, 100,Unit.PX);
	}
	
	//**************rohan added this mthod for editing tax details at contract level ******** 


private void createColumnPremisesDetails() {
	EditTextCell editCell=new EditTextCell();
	premisesDetails=new Column<SalesLineItem,String>(editCell)
			{
		@Override
		public String getValue(SalesLineItem object)
		{
			if(object.getPremisesDetails()!=null){
				return object.getPremisesDetails()+"";}
			else{
				return "";
				}
		}
		};
		/**Added by Sheetal : 16-03-2022
		 * Des : Replacing Premises heading as Actual Qty with below process configuration,requirement by WHC**/
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","PC_CHANGE_TITLE_PREMISE_TO_QUANTITY")){
			table.addColumn(premisesDetails,"#Actual Qty");	
		}else{
			table.addColumn(premisesDetails,"#Premises");	
		}		
		
		table.setColumnWidth(premisesDetails, 100,Unit.PX);
		
		/**
		 * @author Vijay Date :- 09-02-2021
		 * Des :- commented code from addfieldupdate becuase in edit mode its now executed so added code here
		 */
		premisesDetails.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{
				System.out.println("inside update");
				
				try{
					System.out.println("value primices"+value);
					object.setPremisesDetails(value);
					/** Date 18-11-2020 by Vijay added validation comma not allowed requirement from Pest Terminators **/
					if(value.contains(",")) {
						object.setPremisesDetails("");
						setEnable(true);
						alert.alert("Comma Not Allowed!");
					}
				}
				catch (Exception e)
				{

				}
				
				table.redrawRow(index);
			}
				});
}
	
	
	protected void createViewServiceTaxColumn(){
		
		viewServiceTaxColumn=new TextColumn<SalesLineItem>() {
			public String getValue(SalesLineItem object)
			{
				return object.getServiceTaxEdit();
			}
		};
		table.addColumn(viewServiceTaxColumn,"# Tax 2" );
		table.setColumnWidth(viewServiceTaxColumn, 100,Unit.PX);
	}
	
	
	protected void createViewVatTaxColumn(){
		
		viewVatTaxColumn=new TextColumn<SalesLineItem>() {
			public String getValue(SalesLineItem object)
			{
				return object.getVatTaxEdit();
			}
		};
		table.addColumn(viewVatTaxColumn,"# Tax 1" );
		table.setColumnWidth(viewVatTaxColumn, 100,Unit.PX);
	}
	
	private void retriveServiceTax() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		System.out.println("Company Id :: "+c.getCompanyId());
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new TaxDetails());
		
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

		
			public void onSuccess(ArrayList<SuperModel> result){
				
				System.out.println("result size of tax "+result);
				list=new ArrayList<String>();
				servicetaxlist=new ArrayList<TaxDetails>();
				vattaxlist = new ArrayList<TaxDetails>();
				vatlist=new ArrayList<String>();
				List<TaxDetails> backlist=new ArrayList<TaxDetails>();
				
				for (SuperModel model : result) {
					TaxDetails entity = (TaxDetails) model;
					
					backlist.add(entity);
					servicetaxlist.add(entity);
					vattaxlist.add(entity);
				}
				
				for(int i=0;i<backlist.size();i++){
					//  rohan commented this code for loading all the taxes in drop down
//					if(backlist.get(i).getServiceTax().equals(true)){
						list.add(backlist.get(i).getTaxChargeName());
						System.out.println("list size"+list.size());
//					}
//					else if(backlist.get(i).getVatTax().equals(true)){
						vatlist.add(backlist.get(i).getTaxChargeName());
						System.out.println("valist size"+vatlist.size());
//					}
				}
				
				
				/**
				 * Date 06-jun-2018
				 * Developer :- Vijay
				 * Des :- Inclusive tax calculation
				 */
				getColIsTaxInclusive();
				/**
				 * END
				 */
				addEditColumnVatTax();
				addEditColumnServiceTax();
				
				
				
				/** Date 09/06/2018 By vijay ***/
				 createColumnPremisesDetails();

				createColumnServicingTimeColumn();
				

				
				/**
				 * Date : 24-03-2017 By ANIL
				 */
//				getBranchButtonColumn();
				/**
				 * Date : 24-06-2017 By Nidhi
				 */
				if(newBranchFlag){
					getBranchButtonColumn();
				}
				/** date 11.12.2018 added by komal for nbhc service branches button and popup **/
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableServiceBranch")){
					getServiceBranchesButton();
				}
				
				
				/**
		  		 * Date : 29-03-2017 By ANIL
		  		 * Earlier quantity was editable made it read only if needed to change ,change through branch button
		  		 */
				createViewQtyColumn(); 
				createColumnRemark();
				
				/**
				 * Date 06-06-2018 By vijay
				 */
				getColWarrantyPeriod();
				/**
				 * ends here
				 */
				createColumnprodHSNCodeColumn();
				createColumnprodCodeColumn();
				createColumndeleteColumn();
				addFieldUpdater();
			}


			
		});
	}
	
	
	/**
	 * Date 06-jun-2018
	 * Developer :- Vijay
	 * Des :- Inclusive tax calculation
	 * Requirements :- Neatedge services 
	 */
	
	private void getColIsTaxInclusive() {
		
		
		CheckboxCell cell=new CheckboxCell();
		getColIsTaxInclusive=new Column<SalesLineItem, Boolean>(cell) {
			@Override
			public Boolean getValue(SalesLineItem object) {
				
				if(object.getVatTax()!=null&&object.getServiceTax()!=null){
					if(object.getVatTax().isInclusive()==true&&object.getServiceTax().isInclusive()==true){
						object.setInclusive(true);
					}else if(object.getVatTax().isInclusive()==false&&object.getServiceTax().isInclusive()==false){
						object.setInclusive(false);
					}else{
						object.setInclusive(false);
					}
				}
				
				return object.isInclusive();
			}
		};
		table.addColumn(getColIsTaxInclusive, "Is Inclusive");
		table.setColumnWidth(getColIsTaxInclusive,80,Unit.PX);
		
		getColIsTaxInclusive.setFieldUpdater(new FieldUpdater<SalesLineItem, Boolean>() {
			@Override
			public void update(int index, SalesLineItem object, Boolean value) {
				object.setInclusive(value);
				object.getServiceTax().setInclusive(value);
				object.getVatTax().setInclusive(value);
				
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redraw();
				
				setEnable(true);
			}
		});
	}
	
	private void viewColIsTaxInclusive() {
		viewColIsTaxInclusive=new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				// TODO Auto-generated method stub
				
				if(object.isInclusive()==true){
					return "Yes";
				}else{
					return "No";
				}
			}
		};
		table.addColumn(viewColIsTaxInclusive, "Is Inclusive");
		table.setColumnWidth(viewColIsTaxInclusive,80,Unit.PX);
	}
	/**
	 * ends here
	 */
	
	
	/**
	 * Date 06-06-2018 By vijay
	 * Des :- Warranty period of the product
	 */
	private void getColWarrantyPeriod() {
		EditTextCell editCell = new EditTextCell();
		getcolWarrantyPeriod = new Column<SalesLineItem, String>(editCell) {
			
			@Override
			public String getValue(SalesLineItem object) {
				if(object.getWarrantyPeriod()!=0){
					return object.getWarrantyPeriod()+"";
				}
				return 0+"";
			}
		};
		table.addColumn(getcolWarrantyPeriod,"#Warranty Period");
		table.setColumnWidth(getcolWarrantyPeriod, 100,Unit.PX);
		
	}
	
	
	private void getColViewWarrantyPeriod(){
		viewcolWarrantyPeriod = new TextColumn<SalesLineItem>() {
			
			@Override
			public String getValue(SalesLineItem object) {
				return object.getWarrantyPeriod()+"";
			}
		};
		table.addColumn(viewcolWarrantyPeriod,"#Warranty Period");
		table.setColumnWidth(viewcolWarrantyPeriod, 100,Unit.PX);
	}
	
	/**
	 * ends here
	 */
	
	protected void createColumnRemark() {

		EditTextCell editcell = new EditTextCell();
		columnRemark = new Column<SalesLineItem, String>(editcell) {
			
			@Override
			public String getValue(SalesLineItem object) {
				if(object.getRemark()!=null)
				return object.getRemark();
				else return "";
			}
		};
		table.addColumn(columnRemark,"#Remark");
		table.setColumnWidth(columnRemark, 180,Unit.PX);
		columnRemark.setCellStyleNames("wordWrap");
	}
	
	//***************discount Amt column *************** 
	private void createColumnDiscountAmt() {
		EditTextCell editCell=new EditTextCell();
		discountAmt=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getDiscountAmt()==0){
					return 0+"";}
				else{
					return object.getDiscountAmt()+"";}
			}
				};
				table.addColumn(discountAmt,"#Disc Amt");
				table.setColumnWidth(discountAmt, 100,Unit.PX);
	}
	
	
	
	protected void createViewDiscountAmt()
	{
		viewdiscountAmt=new TextColumn<SalesLineItem>()
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getDiscountAmt()==0){
					return 0+"";}
				else{
					return object.getDiscountAmt()+"";}
			}
				};
				table.addColumn(viewdiscountAmt,"#Disc Amt");
				table.setColumnWidth(viewdiscountAmt, 100,Unit.PX);
	}
	
	
//	vat tax Selection List Column
	public void addEditColumnVatTax(){
		SelectionCell employeeselection= new SelectionCell(vatlist);
		vatColumn = new Column<SalesLineItem, String>(employeeselection) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getVatTax().getTaxConfigName()!= null
						&&!object.getVatTax().getTaxConfigName().equals("")) {
					
					object.setVatTaxEdit(object.getVatTax().getTaxConfigName());

					return object.getVatTax().getTaxConfigName();
				} else
					return "NA";
				
				// rohan added this code for Gst implementation on date : 1-7-2017
//				
//				if(object.getVatTax().getTaxPrintName()!=null && !object.getVatTax().getTaxPrintName().equals("")){
//					
//					return object.getVatTax().getTaxPrintName();
//				}
//				else
//				{
//					if (object.getVatTax().getTaxConfigName()!= null
//							&&!object.getVatTax().getTaxConfigName().equals("")) {
//						return object.getVatTax().getTaxConfigName();
//					} else
//						return "NA";
//				}
				
			}
		};
		table.addColumn(vatColumn, "#Tax 1");
		table.setColumnWidth(vatColumn,130,Unit.PX);
		table.setColumnWidth(vatColumn, 100,Unit.PX);
		
	}	
	
	public void updateVatTaxColumn()
	{

		vatColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			
			@Override
			public void update(int index, SalesLineItem object, String value) {
				try {
					String val1 = (value.trim());
					object.setVatTaxEdit(val1);
					Tax tax = object.getVatTax();
					for(int i=0;i<vattaxlist.size();i++)
					{
						if(val1.trim().equals(vattaxlist.get(i).getTaxChargeName())){
							tax.setTaxName(val1);
							tax.setPercentage(vattaxlist.get(i).getTaxChargePercent());
							/**
							 * rohan added this for GST implementation Date :23-06-2017
							 */
							tax.setTaxPrintName(vattaxlist.get(i).getTaxPrintName());
							/**
							 * Date : 03-08-2017 By ANIL
							 */
							tax.setTaxConfigName(val1);
							/**
							 * End
							 */
							/**
							 * nidhi
							 * 8-06-2018
							 * for tax selection option
							 */
//							checkTaxValue(tax,object);
							Tax taxDt = AppUtility.checkTaxValue(tax, object.getServiceTax(), vattaxlist);
							if(taxDt != null){
								object.setServiceTax(taxDt);
							}
							/**
							 * end
							 */
							
							/**
							 * Date 28-07-2018 By Vijay
							 * Des :- If we select IGST then second tax box will be disable.if we select other than IGST then second tax box will be visible
							 */
							if(vattaxlist.get(i).getTaxPrintName().equals("IGST") || vattaxlist.get(i).getTaxPrintName().equals("IGST@18") || vattaxlist.get(i).getTaxPrintName().equals("SEZ")){
								serviceColumn.setCellStyleNames("hideVisibility");
							}else{
								serviceColumn.setCellStyleNames("showVisibility");
							}
							/**
							 * ends here
							 */
							
							break;
						}
					}
					object.setVatTax(tax);
					
					productIdCount=object.getPrduct().getCount();
					productSrNoCount = object.getProductSrNo();
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				} catch (Exception e) {
				}
				table.redrawRow(index);
			}
		});
	}
	
	
	
//	service tax Selection List Column
	public void addEditColumnServiceTax() {
		SelectionCell employeeselection= new SelectionCell(list);
		serviceColumn = new Column<SalesLineItem, String>(employeeselection) {
			@Override
			public String getValue(SalesLineItem object) {
				if (object.getServiceTax().getTaxConfigName()!= null
						&&!object.getServiceTax().getTaxConfigName().equals("")) {
					
					object.setServiceTaxEdit(object.getServiceTax().getTaxConfigName());

					return object.getServiceTax().getTaxConfigName();
				} else
					return "NA";
				
	// rohan added this code for Gst implementation on date : 1-7-2017
//				
//				if(object.getServiceTax().getTaxPrintName()!=null && !object.getServiceTax().getTaxPrintName().equals("")){
//					
//					return object.getServiceTax().getTaxPrintName();
//				}
//				else
//				{
//					if (object.getServiceTax().getTaxConfigName()!= null
//							&&!object.getServiceTax().getTaxConfigName().equals("")) {
//						return object.getServiceTax().getTaxConfigName();
//					} else
//						return "NA";
//				}
			}

			
		};
		table.addColumn(serviceColumn, "#Tax 2");
		table.setColumnWidth(serviceColumn,140,Unit.PX);
		table.setColumnWidth(serviceColumn, 100,Unit.PX);
	}
	
	
	public void updateServiceTaxColumn()
	{

		serviceColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			
			@Override
			public void update(int index, SalesLineItem object, String value) {
				try {
					String val1 = (value.trim());
					object.setServiceTaxEdit(val1);
					Tax tax = object.getServiceTax();
					for(int i=0;i<servicetaxlist.size();i++)
					{
						if(val1.trim().equals(servicetaxlist.get(i).getTaxChargeName())){
							tax.setTaxName(val1);
							tax.setPercentage(servicetaxlist.get(i).getTaxChargePercent());
							/**
							 * rohan added this for GST implementation Date :23-06-2017
							 */
							tax.setTaxPrintName(servicetaxlist.get(i).getTaxPrintName());
							/**
							 * Date : 03-08-2017 By ANIL
							 */
							tax.setTaxConfigName(val1);
							/**
							 * End
							 */
							/**
							 * nidhi
							 * 8-06-2018
							 * 
							 */
							//checkServiceTaxValue(tax, object);
//							Tax vatTax =AppUtility.checkse
							Tax vatTax = AppUtility.checkTaxValue(tax, object.getVatTax(), vattaxlist);
							if(vatTax!=null){
								object.setVatTax(vatTax);
							}
							/**
							 * end
							 */
							
							/**
							 * Date 28-07-2018 By Vijay
							 * Des :- If we select IGST then second tax box will be disable.if we select other than IGST then second tax box will be visible
							 */
							
							if(servicetaxlist.get(i).getTaxPrintName().equals("IGST") || servicetaxlist.get(i).getTaxPrintName().equals("IGST@18") || servicetaxlist.get(i).getTaxPrintName().equals("SEZ")){
								vatColumn.setCellStyleNames("hideVisibility");
							}else{
								vatColumn.setCellStyleNames("showVisibility");
							}
							/**
							 * ends here
							 */
							
							break;
						}
					}
					object.setServiceTax(tax);
					System.out.println("TZZZXXX ="+tax.getTaxPrintName());
					
					productIdCount=object.getPrduct().getCount();
					productSrNoCount = object.getProductSrNo();
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				} catch (Exception e) {
				}
				table.redrawRow(index);
			}
		});
	}
	
	
//	public void updateVatTaxColumn()
//	{
//
//		vatColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
//			
//			@Override
//			public void update(int index, SalesLineItem object, String value) {
//				try {
//					String val1 = (value.trim());
//					object.setVatTaxEdit(val1);
//					Tax tax = object.getServiceTax();
//					for(int i=0;i<taxlist.size();i++)
//					{
//						if(val1.trim().equals(taxlist.get(i).getTaxChargeName())){
//							tax.setTaxName(val1);
//							tax.setPercentage(taxlist.get(i).getTaxChargePercent());
//							break;
//						}
//					}
//					object.setServiceTax(tax);
//					
//					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
//					
//				} catch (NumberFormatException e) {
//				}
//				table.redrawRow(index);
//			}
//		});
//	}
	
	
	//******************************changes ends here ***************************************
	protected void createColumnprodCategoryColumn()
	{
		prodCategoryColumn=new TextColumn<SalesLineItem>()
				{
			@Override public String getValue(SalesLineItem object){
				return object.getProductCategory();
			}
				};
				table.addColumn(prodCategoryColumn,"Category");
				table.setColumnWidth(prodCategoryColumn, 100,Unit.PX);

	}
	protected void createColumnprodCodeColumn()
	{
		prodCodeColumn=new TextColumn<SalesLineItem>()
				{
			@Override public String getValue(SalesLineItem object){
				return object.getProductCode();
			}
				};
				table.addColumn(prodCodeColumn,"PCode");
				table.setColumnWidth(prodCodeColumn, 100,Unit.PX);

	}
	protected void createColumnprodNameColumn()
	{
		
		EditTextCell editCell=new EditTextCell();
		prodNameColumn=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				return object.getProductName();
			}
				};
				table.addColumn(prodNameColumn,"#Name");
				table.setColumnWidth(prodNameColumn, 100,Unit.PX);
	}
	protected void createColumnprodQtyColumn()
	{
		EditTextCell editCell=new EditTextCell();
		prodQtyColumn=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getQty()==0){
					return 0+"";}
				else{
					return object.getQty()+"";}
			}
				};
				table.addColumn(prodQtyColumn,"No of Branches");
				table.setColumnWidth(prodQtyColumn, 120,Unit.PX);
	}
	
	
	protected void createViewQtyColumn()
	{
		
		viewprodQtyColumn=new TextColumn<SalesLineItem>()
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getQty()==0){
					return 0+"";}
				else{
					return object.getQty()+"";}
			}
				};
				table.addColumn(viewprodQtyColumn,"No of Branches");
				table.setColumnWidth(viewprodQtyColumn, 100,Unit.PX);
	}

	protected void createColumnPercDiscount()
	{
		EditTextCell editCell=new EditTextCell();
		prodPercentDiscColumn=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getPercentageDiscount()==0||object.getPercentageDiscount()==null){
					return 0+"";}
				else{
					return object.getPercentageDiscount()+"";}
			}
				};
				table.addColumn(prodPercentDiscColumn,"#% Disc");
				table.setColumnWidth(prodPercentDiscColumn, 100,Unit.PX);
               
	}
	
	protected void createViewPercDiscount()
	{
		viewprodPercDiscColumn=new TextColumn<SalesLineItem>()
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getPercentageDiscount()==0){
					return 0+"";}
				else{
					return object.getPercentageDiscount()+"";}
			}
				};
				table.addColumn(viewprodPercDiscColumn,"#% Disc");
				table.setColumnWidth(viewprodPercDiscColumn, 100,Unit.PX);
	}
	

	
	protected void createColumndateColumn()
	{
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date= new DatePickerCell(fmt);
		dateColumn=new Column<SalesLineItem,Date>(date)
				{
			@Override
			public Date getValue(SalesLineItem object)
			{
				if(object.getStartDate()!=null){
						return fmt.parse(fmt.format(object.getStartDate()));
					}
				else{
				object.setStartDate(new Date());
					return fmt.parse(fmt.format(new Date()));
				}
			}
			
			@Override
					public void onBrowserEvent(Context context, Element elem,
							SalesLineItem object, NativeEvent event) {
						// TODO Auto-generated method stub
					if(form!=null && form.paymentDateFlag){
						
					}else{
						super.onBrowserEvent(context, elem, object, event);
					}
						
					}
				};
				table.setColumnWidth(dateColumn, 90, Unit.PX);
				table.addColumn(dateColumn,"#Start Date");
				table.setColumnWidth(dateColumn, 100,Unit.PX);
	}
	
	protected void createViewColumndateColumn()
	{
		
		
		viewdColumn=new TextColumn<SalesLineItem>()
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getStartDate()!=null)
				{
					
					return AppUtility.parseDate(object.getStartDate());}
				else{
				return "N.A.";
				}
			}
				};
				table.addColumn(viewdColumn,"#Start Date");
				table.setColumnWidth(viewdColumn, 100,Unit.PX);
	}
	
	protected void createColumndurationColumn()
	{
		EditTextCell editCell=new EditTextCell();
		durationColumn=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getDuration()==0){
					return 0+"";}
				else{
					return object.getDuration()+"";
				}
			}
				};
				table.addColumn(durationColumn,"#Duration");
				table.setColumnWidth(durationColumn, 100,Unit.PX);
	}
	
	protected void createColumnViewDuration()
	{
		
		viewdurationColumn=new TextColumn<SalesLineItem>()
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getDuration()==0)
					return "N.A";
				else{
					return object.getDuration()+"";}
			}
				};
				table.addColumn(viewdurationColumn,"#Duration");
				table.setColumnWidth(viewdurationColumn, 100,Unit.PX);
	}
	
	protected void createColumnnoServicesColumn()
	{
		EditTextCell editCell=new EditTextCell();
		noServicesColumn=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getNumberOfServices()==0){
					return 0+"";
				}
				else{
					return object.getNumberOfServices()+"";
				}
			}
				};
				table.addColumn(noServicesColumn,"#Services");
				table.setColumnWidth(noServicesColumn, 100,Unit.PX);
	}
	
	
	protected void createViewColumnnoServicesColumn()
	{
		
		viewnoServicesColumn=new TextColumn<SalesLineItem>()
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getNumberOfServices()==0){
					return "N.A"+"";}
				else{
					return object.getNumberOfServices()+"";}
			}
				};
				table.addColumn(viewnoServicesColumn,"#Services");
				table.setColumnWidth(viewnoServicesColumn, 100,Unit.PX);
	}
	
	
	
	protected void createColumnpriceColumn() {
		EditTextCell editCell = new EditTextCell();
		priceColumn = new Column<SalesLineItem, String>(editCell) {
			@Override
			public String getValue(SalesLineItem object) {
				SuperProduct product = object.getPrduct();
				double tax = removeAllTaxes(product);

				System.out.println("roohan price" + object.getPrice());
				double origPrice = object.getPrice() - tax;

				return nf.format(origPrice);
			}
			
			@Override
			public void onBrowserEvent(Context context, Element elem,SalesLineItem object, NativeEvent event) {
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "MakePriceColumnDisable")){
					disablePriceCol=true;
				}
				Console.log("dis flag : "+disablePriceCol+" price : "+object.getPrice());
				if(disablePriceCol==false||(disablePriceCol==true&&object.getPrice()==0)){
					super.onBrowserEvent(context, elem, object, event);
				}
			}
		};
		table.addColumn(priceColumn, "#Price");
		table.setColumnWidth(priceColumn, 100, Unit.PX);
	}
	
	protected void createViewColumnpriceColumn()
	{
		
		viewPriceColumn=new TextColumn<SalesLineItem>()
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				System.out.println("rohan price "+object.getPrice());
				if(object.getPrice()==0){
					return 0+"";}
				else{
					SuperProduct product=object.getPrduct();
					double tax=removeAllTaxes(product);
					double origPrice=object.getPrice()-tax;
					
					return nf.format(origPrice);
					}
			}
				};
				table.addColumn(viewPriceColumn,"#Price");
				table.setColumnWidth(viewPriceColumn, 100,Unit.PX);
	}
	
	
//	protected void createColumnvatColumn()
//	{
//		TextCell editCell=new TextCell();
//		vatColumn=new Column<SalesLineItem,String>(editCell)
//				{
//			@Override
//			public String getValue(SalesLineItem object)
//			{
//				if(object.getVatTax()!=null){
//					return object.getVatTax().getPercentage()+"";
//				}
//				else{ 
//					return "N.A.";
//				}
//			}
//				};
//				table.addColumn(vatColumn,"VAT");
//	}
//	protected void createColumnserviceColumn()
//	{
//		TextCell editCell=new TextCell();
//		serviceColumn=new Column<SalesLineItem,String>(editCell)
//		{
//			@Override
//			public String getValue(SalesLineItem object)
//			{
//				//SuperProduct prod=object.getPrduct();
//				if(object.getServiceTax()!=null){
//					return object.getServiceTax().getPercentage()+"";
//				}
//				else{ 
//					return "N.A.";
//				}
//			}
//				};
//				table.addColumn(serviceColumn,"Ser. Tax");
//	}
	protected void createColumntotalColumn()
	{
		EditTextCell editCell=new EditTextCell();
		totalColumn=new Column<SalesLineItem,String>(editCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				double total=0;
				SuperProduct product=object.getPrduct();
				double tax=removeAllTaxes(product);
				double origPrice=object.getPrice()-tax;
		
				//****************old code 
				
//				if(object.getPercentageDiscount()==null){
//					total=origPrice*object.getQty();
//				}
//				if(object.getPercentageDiscount()!=null){
//					total=origPrice-(origPrice*object.getPercentageDiscount()/100);
//					total=total*object.getQty();
//				}
				//****************new code for discount by rohan ******************
				// **********  this code is commented by vijay for area + disc on total *************
//				if((object.getPercentageDiscount()==null && object.getPercentageDiscount()==0) && (object.getDiscountAmt()==0) ){
//					
//					System.out.println("inside both 0 condition");
//					total=origPrice*object.getQty();
//				}
//				
//				else if((object.getPercentageDiscount()!=null)&& (object.getDiscountAmt()!=0)){
//					
//					System.out.println("inside both not null condition");
//					
//					total=origPrice-(origPrice*object.getPercentageDiscount()/100);
//					total=total-object.getDiscountAmt();
//					total=total*object.getQty();
//				}
//				else 
//				{
//					System.out.println("inside oneof the null condition");
//					
//						if(object.getPercentageDiscount()!=null){
//							System.out.println("inside getPercentageDiscount oneof the null condition");
//							total=origPrice-(origPrice*object.getPercentageDiscount()/100);
//						}
//						else 
//						{
//							System.out.println("inside getDiscountAmt oneof the null condition");
//							total=origPrice-object.getDiscountAmt();
//						}
//						
//						total=total*object.getQty();
//						
//				}
//				
//				
//				return nf.format(total);
//			}
//				};
//				table.addColumn(totalColumn,"Total");
				
				
				/**
				 * Date 07-06-2018 By vijay
				 * Des :- we need to calculate total every time for reverse calculation 
				 * so i have removed below code and we use total getter and setter for other calculation
				 */
//				if(object.getTotalAmount()!=0){
//					return nf.format(object.getTotalAmount());
//				}else{
					return nf.format(calculateTotalAmount(object));
//				}
				/**
				 * ends here
				 */
				
				/** Date 02 oct 2017 below code commented by Sagar Sore new code added above **************/
				
//				// new code by vijay add code for area calculation
//				
//				System.out.println("Get value from area =="+ object.getArea());
//				if(object.getArea().equals("") || object.getArea().equals("0")){
//					System.out.println(" vijay mmmmmmm ");
//					object.setArea("NA");
//				}
//				
//				System.out.println("area == "+object.getArea());
//				
//				if((object.getPercentageDiscount()==null && object.getPercentageDiscount()==0) && (object.getDiscountAmt()==0) ){
//					
//					System.out.println("inside both 0 condition");
//					
//					/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
//					System.out.println("Get value from area =="+ object.getArea());
//					System.out.println("total amount before area calculation =="+total);
//					if(!object.getArea().equalsIgnoreCase("NA") ){
//					double area = Double.parseDouble(object.getArea());
//					 total = origPrice*area;
//					System.out.println(" Final TOTAL if no discount per & no discount Amt ==="+total);
//					}else{
//						total = origPrice;
////						total=total;
//					}
//					
//				}
//				/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
//				else if((object.getPercentageDiscount()!=null)&& (object.getDiscountAmt()!=0)){
//					
//					System.out.println("inside both not null condition");
//					System.out.println("total amount before area calculation =="+total);
//					if(!object.getArea().equalsIgnoreCase("NA")){
//						double area = Double.parseDouble(object.getArea());
//						total=origPrice*area;
//						System.out.println("total before discount per ===="+total);
//						total = total - (total*object.getPercentageDiscount()/100);
//						System.out.println("after discount per total === "+total);
//						total = total - object.getDiscountAmt();
//						System.out.println("after discount AMT total === "+total);
//
//					System.out.println(" Final TOTAL   discount per &  discount Amt ==="+total);
//					}else{
//						System.out.println(" normal === total "+total);
//						total=origPrice;
//						total=total-(total*object.getPercentageDiscount()/100);
//						total=total-object.getDiscountAmt();
//					}
//				}
//				else 
//				{
//					System.out.println("inside oneof the null condition");
//						if(object.getPercentageDiscount()!=null || object.getPercentageDiscount()!=0){
//							System.out.println("inside getPercentageDiscount oneof the null condition");
//							/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
//							System.out.println("tatal $$$$$$$$$$ ===="+total);
//							if(!object.getArea().equalsIgnoreCase("NA")){
//								double area = Double.parseDouble(object.getArea());
//								total=origPrice*area;
//								System.out.println("total before discount per ===="+total);
//								total = total - (total*object.getPercentageDiscount()/100);
//								System.out.println("after discount per total === "+total);
//								
//							}else{
//								System.out.println("old code");
//								total=origPrice;
//								total=total-(total*object.getPercentageDiscount()/100);
//							}
//						}
//						else 
//						{
//							System.out.println("inside getDiscountAmt oneof the null condition");
//							/******** vijay added code for square area wise calculation new code added in if condition and old code added in else***************/
//							System.out.println("tatal $$$$$$$$$$ ===="+total);
//							if(!object.getArea().equalsIgnoreCase("NA")){
//								double area = Double.parseDouble(object.getArea());
//								total=origPrice*area;
//								System.out.println("total before discount amt ===="+total);
//								total = total - object.getDiscountAmt();
//								System.out.println("after discount amt total === "+total);
//								
//							}else{
//								total=total;
//								total=total-object.getDiscountAmt();
//							}
//
//						}
//						System.out.println("6666666666666");
//				}
//				
//				
//				return nf.format(total);
			}
				};
				table.addColumn(totalColumn,"Total");
				table.setColumnWidth(totalColumn, 100,Unit.PX);
	}
	protected void createColumndeleteColumn()
	{
		ButtonCell btnCell= new ButtonCell();
		deleteColumn=new Column<SalesLineItem,String>(btnCell)
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				return  "Delete" ;
			}
				};
				table.addColumn(deleteColumn,"Delete");
				table.setColumnWidth(deleteColumn, 100,Unit.PX);
	}

	
	/**
	 * DAte : 24-03-2017 BY Anil
	 */

	protected void getBranchButtonColumn() {
		ButtonCell btnCell = new ButtonCell();
		getBranchButtonColumn = new Column<SalesLineItem, String>(btnCell) {
			@Override
			public String getValue(SalesLineItem object) {
				return "Branches";
			}
		};
		table.addColumn(getBranchButtonColumn, "Branches");
		table.setColumnWidth(getBranchButtonColumn, 100, Unit.PX);
	}


	@Override 
	public void addFieldUpdater() 
	{
		if(pedioContractFlag){
			Console.log("PEDIO UPDATOR");
			pedioContractFieldUpdater();
			return;
		}
		System.out.println("&&&&&&&&&&&&&&&&&&&&&&&");
		if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","MAKEPRODUCTNAMENONEDITABLE"))
	        createFieldUpdaterproductNameColumn();
	
//		createFieldUpdaterprodQtyColumn();
		createFieldUpdaterdateColumn();
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","MakeColumnDisable")){
//  			createFieldUpdaterprice();
//  		}else{
		createFieldUpdaterpriceColumn();
//		}
//		createFieldUpdaterPremisesColumn();
		
		/**
		 * nidhi
		 * 9-08-2018
		 * for map model and serial no
		 */
		if(LoginPresenter.mapModelSerialNoFlag){
			createFieldUpdaterProModelNoColumn();
			createFieldUpdateProSerailNo();
		}
		/**
		 * end
		 */
		createFieldUpdaterprodDurationColumn();
		createFieldUpdaterprodServices();
		createFieldUpdaterprodServicingTime();
		createFieldUpdaterPercDiscColumn();
		createFieldUpdaterDiscAmtColumn();
		createFieldUpdaterdeleteColumn();
		createFieldUpdaterProdAreaColumn();
		updateServiceTaxColumn();
		updateVatTaxColumn();
		updateRemarkColumn();
		/*
		 * nidhi 24-06-2017
		 */
		if(newBranchFlag){
		createFieldUpdaterOnBranchesColumn(false);
		}
		/*
		 * end
		 */
		addFieldUpdaterOnServiceEndDateColumn();
		
		//Date 17-08-2017 added by vijay for hsn code
		updateHSNCode();
		
		addFieldUpdaterWarrantyPeriod();
		if(LoginPresenter.billofMaterialActive){
			updateAreaUOMColumn();
		}
		addFieldUpdaterTotal();
		
		/** date 11.12.2018 added by komal for nbhc service branches button and popup **/
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableServiceBranch")){
			addFieldUpdaterOnServiceBranchesButton();
		}
		updateUOMColumn();
	}
	
	
	private void updateUOMColumn() {
		UOMcolumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{

					@Override
					public void update(int index, SalesLineItem object, String value) {
						try{
							String val1 =  (value.trim());
							if(value.equalsIgnoreCase("--SELECT--")){
								object.setUnitOfMeasurement("");
							}else {
							object.setUnitOfMeasurement(val1);
							}
						}catch(Exception e){
							
						}
						table.redrawRow(index);
					}
				});
	}
	private void createFieldUpdaterprice() {
		viewPriceColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{
				try
				{
					Double val1=Double.parseDouble(value.trim());
			
				object.setPrice(val1);
				productIdCount=object.getPrduct().getCount();
				productSrNoCount = object.getProductSrNo();
				
				/**28-09-2017 sagar sore to store total amount**/
				calculateTotalAmount(object);
				
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				
				}
				catch(Exception e)
				{
				}
				table.redrawRow(index);
				/** Date 06-06-2018 By vijay for reveser calculation with total and area **/
				setEnable(true);
			}
				});
	}
	/**
	 * Date 06-jun-2018
	 * Developer :- Vijay
	 * Des :- Reverse calculation with area(Unit)
	 * Requirements :- Neatedge services 
	 */
	
	private void addFieldUpdaterTotal() {

		totalColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			
			@Override
			public void update(int index, SalesLineItem object, String value) {
				double oldtotalAmt = object.getTotalAmount();
				double totalAmt = 0;
				try {
					totalAmt = Double.parseDouble(value);
					object.setTotalAmount(totalAmt);
				} catch (NumberFormatException e) {
					alert.alert("Only Numric values are allowed !");
					object.setTotalAmount(oldtotalAmt);
				}
				
				try {
					double area = Double.parseDouble(object.getArea());
					double price = totalAmt/area;
					object.setPrice(price);
						
				} catch (Exception e) {
					alert.alert("Please Define Area first for reverse calculation with Area!");
					object.setTotalAmount(object.getTotalAmount());
				}
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
				setEnable(true);
			}
		});
	}
	
	/**
	 * ends here
	 */
	
	/**
	 * Date 06-06-2018 by Vijay
	 * Des :- for Service product warranty period 
	 * Requirement :- Neatedge Services
	 */
	
	private void addFieldUpdaterWarrantyPeriod() {
		getcolWarrantyPeriod.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			
			@Override
			public void update(int index, SalesLineItem object, String value) {
				// TODO Auto-generated method stub
				try {
					int warrantyPeriod =Integer.parseInt(value);
					object.setWarrantyPeriod(warrantyPeriod);
					if(complainTatFlag){
						if(getDataprovider().getList().get(index).getCustomerBranchSchedulingInfo()!=null){
							if(getDataprovider().getList().get(index).getCustomerBranchSchedulingInfo().containsKey(getDataprovider().getList().get(index).getProductSrNo())){
								Integer productSrNo = getDataprovider().getList().get(index).getProductSrNo();
								for(BranchWiseScheduling obj:getDataprovider().getList().get(index).getCustomerBranchSchedulingInfo().get(productSrNo)){
									obj.setDurationForReplacement(warrantyPeriod);
									for(ComponentDetails comp:obj.getComponentList()){
										comp.setDurationForReplacement(warrantyPeriod);
									}
								}
							}
						}
					}
					
				} catch (NumberFormatException ex) {
					GWTCAlert alert = new GWTCAlert();
					alert.alert("Please enter numberic value.");
				}
			}
		});
	}
	/**
	 * ends here
	 */
	
	//Date 17-08-2017 added by vijay for hsn code
		private void updateHSNCode() {
			hsnCodeColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {

				@Override
				public void update(int index, SalesLineItem object, String value) {
					object.getPrduct().setHsnNumber(value);
					table.redrawRow(index);
				}
			});
		}
		/**
		 * Date : 24-03-2017 By Anil
		 */
		
		protected void createFieldUpdaterOnBranchesColumn(final Boolean hideCheckBoxColumn) {
			
			getBranchButtonColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
				@Override
				public void update(int index, SalesLineItem object,String value) {
					
					/**
					 * Date : 03-04-2017 By ANIL
					 */
//					ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(object.getBranchSchedulingInfo());
					
					/**
					 * Date : 14-06-2017 By ANIL
					 */
					String[] branchArray=new String[10];
					if(object.getBranchSchedulingInfo()!=null
							&&!object.getBranchSchedulingInfo().equals("")){
						branchArray[0]=object.getBranchSchedulingInfo();
					}
					if(object.getBranchSchedulingInfo1()!=null
							&&!object.getBranchSchedulingInfo1().equals("")){
						branchArray[1]=object.getBranchSchedulingInfo1();
					}
					if(object.getBranchSchedulingInfo2()!=null
							&&!object.getBranchSchedulingInfo2().equals("")){
						branchArray[2]=object.getBranchSchedulingInfo2();
					}
					if(object.getBranchSchedulingInfo3()!=null
							&&!object.getBranchSchedulingInfo3().equals("")){
						branchArray[3]=object.getBranchSchedulingInfo3();
					}
					if(object.getBranchSchedulingInfo4()!=null
							&&!object.getBranchSchedulingInfo4().equals("")){
						branchArray[4]=object.getBranchSchedulingInfo4();
					}
					
					if(object.getBranchSchedulingInfo5()!=null
							&&!object.getBranchSchedulingInfo5().equals("")){
						branchArray[5]=object.getBranchSchedulingInfo5();
					}
					if(object.getBranchSchedulingInfo6()!=null
							&&!object.getBranchSchedulingInfo6().equals("")){
						branchArray[6]=object.getBranchSchedulingInfo6();
					}
					if(object.getBranchSchedulingInfo7()!=null
							&&!object.getBranchSchedulingInfo7().equals("")){
						branchArray[7]=object.getBranchSchedulingInfo7();
					}
					if(object.getBranchSchedulingInfo8()!=null
							&&!object.getBranchSchedulingInfo8().equals("")){
						branchArray[8]=object.getBranchSchedulingInfo8();
					}
					if(object.getBranchSchedulingInfo9()!=null
							&&!object.getBranchSchedulingInfo9().equals("")){
						branchArray[9]=object.getBranchSchedulingInfo9();
					}
					
					/**
					 * End
					 */
//					ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(branchArray);
					
					/**
					 * Date 02-05-2018 
					 * Developer : Vijay
					 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
					 * so i have updated below code using hashmap. So old contract i will get the branches from string and new contract from hashmpa if branches exist 
					 */
					
					ArrayList<BranchWiseScheduling> branchSchedulingList=null;
					
					/**
					 * Date 04-05-2018 By vijay
					 * if condition for old contract renewal and else block for new updated new contract renewal
					 */
					if(object.getBranchSchedulingInfo()!=null
							&&!object.getBranchSchedulingInfo().equals("")){
						branchSchedulingList = AppUtility.getBranchSchedulingList(branchArray);
						Console.log("prepared branchSchedulingList in if "+branchSchedulingList.size());
					}else{
						Console.log("Updated branch scheduling popup... object.getCustomerBranchSchedulingInfo() size="+object.getCustomerBranchSchedulingInfo().size());
						branchSchedulingList = object.getCustomerBranchSchedulingInfo().get(object.getProductSrNo());						
						Console.log("prepared branchSchedulingList in else "+branchSchedulingList.size());
						
					}
					
					ArrayList<BranchWiseScheduling> branchlist = AppUtility.getCustomerBranchSchedulinglistInfo(form.customerbranchlist,form.olbbBranch.getValue(),"",0);
					if(branchlist!=null) {
						if(branchSchedulingList!=null&&branchSchedulingList.size()>0) {
							for(BranchWiseScheduling binfo:branchlist) {
								boolean found=false;
								for(BranchWiseScheduling prodBInfo:branchSchedulingList) {									
									if(prodBInfo.getBranchName().equals(binfo.getBranchName())) {
										found=true;
										break;
									}
								}
								if(!found) {
									branchSchedulingList.add(binfo);
								}
							}
						}else {
							for(BranchWiseScheduling binfo:branchlist) {
								branchSchedulingList.add(binfo); 
							}
						}
						
					}
					
					
//					Console.log("Ashwini prepared branchSchedulingList");
					/**
					 * ends here
					 */
				
//					if(branchSchedulingList!=null&&branchSchedulingList.size()!=0){
//						for(BranchWiseScheduling bs:branchSchedulingList) {
//							if(bs.getBranchName().equals("Service Address")) {
//								branchSchedulingList.remove(bs);
//								Console.log("Ashwini removed Service Address from list");
//							}
//						}
//					}
					
					if(branchSchedulingList!=null&&branchSchedulingList.size()!=0){
						Console.log("branchSchedulingList not null");			
						if(hideCheckBoxColumn){
							panel=new PopupPanel(true);
							advSchViewPopup.clear();
//							advSchPopup.getBranchTable().getDataprovider().getList().addAll(branchSchedulingList); // old code commented by vijay
							advSchViewPopup.getBranchTable().getDataprovider().setList(branchSchedulingList);						
							advSchViewPopup.getBtnOk().setVisible(false);
							panel.add(advSchViewPopup);
							panel.center();
							panel.show();								
						}
						else{
							panel=new PopupPanel(true);
							advSchPopup.clear();
//							advSchPopup.getBranchTable().getDataprovider().getList().addAll(branchSchedulingList); // old code commented by vijay
							advSchPopup.getBranchTable().getDataprovider().setList(branchSchedulingList);						
							panel.add(advSchPopup);
							panel.center();
							panel.show();						
						}
						rowIndex=index;
					}else{
						GWTCAlert alert=new GWTCAlert();
						alert.alert("Customer branch is not defined.");
					}
				}
			});
		}
		
		
	private void updateRemarkColumn() {

		columnRemark.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
		
			@Override
			public void update(int index, SalesLineItem object, String value) {

				object.setRemark(value);
				productIdCount=object.getPrduct().getCount();
				productSrNoCount = object.getProductSrNo();
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				
				table.redrawRow(index);
			}
		});
	}

	private void createFieldUpdaterProdAreaColumn() {
		prodAreaColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
			
			@Override
			public void update(int index, SalesLineItem object, String value) {
				
				/**10-09-2017 manisha to solve machine hang problem
				 * when user enter string value in area column
				 * **/
				
				try{
					if(value.equals("NA") ){
						object.setArea(value);
					}else{
						Double val1=Double.parseDouble(value.trim());
						
						object.setArea(value);
					}
					productIdCount=object.getPrduct().getCount();
					productSrNoCount = object.getProductSrNo();
					
					/**28-09-2017 sagar sore to store total amount**/
					calculateTotalAmount(object);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
					
				}
				catch (NumberFormatException e){
					e.printStackTrace();
					GWTCAlert alert=new GWTCAlert();
					object.setArea("NA");
					alert.alert("Only numeric values are allowed!");
				}
				catch (Exception e){
					
				}
				table.redrawRow(index);
				/** Date 06-06-2018 By vijay for reveser calculation with total and area **/
				setEnable(true);
			}
		});
		
	}

//private void createFieldUpdaterPremisesColumn() {
//	System.out.println("inside primices method");
//	premisesDetails.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
//			{
//		@Override
//
//		public void update(int index,SalesLineItem object,String value)
//		{
//			System.out.println("inside update");
//			
//			try{
//				System.out.println("value primices"+value);
//				object.setPremisesDetails(value);
//				/** Date 18-11-2020 by Vijay added validation comma not allowed requirement from Pest Terminators **/
//				if(value.contains(",")) {
//					object.setPremisesDetails("");
//					setEnable(true);
//					alert.alert("Comma Not Allowed!");
//				}
//			}
//			catch (Exception e)
//			{
//
//			}
//			
//			table.redrawRow(index);
//		}
//			});
//}
	
	protected void createFieldUpdaterprodServicingTime()
	{
		servicingTime.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
		{
			@Override
			public void update(int index,SalesLineItem object,String value)
			{
				try{
					Double val1=Double.parseDouble(value.trim());
					object.setServicingTime(val1);
					
					productIdCount=object.getPrduct().getCount();
					productSrNoCount = object.getProductSrNo();
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}
				catch (Exception e)
				{
				}
				table.redrawRow(index);
			}
		});
	}
	
	
	protected void createFieldUpdaterprodQtyColumn()
	{
		prodQtyColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{

				try{
					Double val1=Double.parseDouble(value.trim());
					object.setQuantity(val1);
					productIdCount=object.getPrduct().getCount();
					productSrNoCount = object.getProductSrNo();
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				}
				catch (NumberFormatException e)
				{

				}


				table.redrawRow(index);
			}
				});
	}
	
	
	protected void createFieldUpdaterDiscAmtColumn()
	{
		discountAmt.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{

				try{
					Double val1=Double.parseDouble(value.trim());
					object.setDiscountAmt(val1);
					
					productIdCount=object.getPrduct().getCount();
					productSrNoCount = object.getProductSrNo();
					
					/**28-09-2017 sagar sore to store total amount**/
					calculateTotalAmount(object);
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}
				catch (Exception e)
				{

				}
				table.redrawRow(index);
			}
				});
	}
	
	
	protected void createFieldUpdaterPercDiscColumn()
	{
		prodPercentDiscColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{

				try{
					Double val1=Double.parseDouble(value.trim());
					object.setPercentageDiscount(val1);
					
					productIdCount=object.getPrduct().getCount();
					productSrNoCount = object.getProductSrNo();
					
					/**28-09-2017 sagar sore to store total amount**/
					calculateTotalAmount(object);
					
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}
				catch (Exception e)
				{

				}
				table.redrawRow(index);
			}
				});
	}

	
	
	protected void createFieldUpdaterdateColumn()
	{
		dateColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, Date>()
				{
			@Override
			public void update(int index,SalesLineItem object,Date value)
			{
				object.setStartDate(value);
				/**
				 * Date : 08-08-2017 By ANIL
				 */
				Date date=CalendarUtil.copyDate(object.getStartDate());
				CalendarUtil.addDaysToDate(date, object.getDuration()-1);	
				object.setEndDate(date);
				
				System.out.println("Updated Start Date : "+value+" ST DATE : "+object.getStartDate()+" EN DATE : "+object.getEndDate());
				/**
				 * End
				 */
				productIdCount=object.getPrduct().getCount();
				productSrNoCount = object.getProductSrNo();
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				table.redrawRow(index);
			}
				});
	}

	protected void createFieldUpdaterprodDurationColumn()
	{
		durationColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{

				try{
					Integer val1=Integer.parseInt(value.trim());
					object.setDuration(val1);
					/**
					 * Date : 09-08-2017 BY ANIL
					 */
					Date date=CalendarUtil.copyDate(object.getStartDate());
					CalendarUtil.addDaysToDate(date, object.getDuration()-1);
					object.setEndDate(date);
					/**
					 * End
					 */
					productIdCount=object.getPrduct().getCount();
					productSrNoCount = object.getProductSrNo();
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}
				catch (Exception e)
				{
				}
				table.redrawRow(index);
			}
		});
	}


	protected void createFieldUpdaterprodServices()
	{
		noServicesColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{

				try{
					Integer val1=Integer.parseInt(value.trim());
					object.setNumberOfService(val1);
					productIdCount=object.getPrduct().getCount();
					productSrNoCount = object.getProductSrNo();
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}
				catch (Exception e)
				{
				}
				table.redrawRow(index);
			}
		});
	}
	
	
	protected void createFieldUpdaterproductNameColumn()
	{
		prodNameColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{

				try{
					String val1=value.trim();
					object.setProductName(val1);
					productIdCount=object.getPrduct().getCount();
					productSrNoCount = object.getProductSrNo();
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}
				catch (Exception e)
				{

				}
				table.redrawRow(index);
			}
				});
	}

	
	
	protected void createFieldUpdaterpriceColumn()
	{
		priceColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
				{
			@Override

			public void update(int index,SalesLineItem object,String value)
			{
				try
				{
					Double val1=Double.parseDouble(value.trim());
			
				object.setPrice(val1);
				productIdCount=object.getPrduct().getCount();
				productSrNoCount = object.getProductSrNo();
				
				/**28-09-2017 sagar sore to store total amount**/
				calculateTotalAmount(object);
				
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				
				}
				catch(Exception e)
				{
				}
				table.redrawRow(index);
				/** Date 06-06-2018 By vijay for reveser calculation with total and area **/
				setEnable(true);
			}
		});
	}
	protected void createFieldUpdaterdeleteColumn()
	{
		deleteColumn.setFieldUpdater(new FieldUpdater<SalesLineItem,String>()
				{
			@Override
			public void update(int index,SalesLineItem object,String value)
			{
				getDataprovider().getList().remove(object);
				productIdCount=0;
				/**
				 * Date : 26-08-2017 BY ANIL
				 */
				productSrNoCount=0;
				/**
				 * End
				 */
				table.redrawRow(index);
			}
		});
	}

	
	private void createColumnprodAreaColumn() {
		EditTextCell editCell=new EditTextCell();
		
		prodAreaColumn = new Column<SalesLineItem, String>(editCell) {
			
			@Override
			public String getValue(SalesLineItem object) {
				if(object.getArea()!=null){
					return object.getArea();
				}else{
					return "";
				}
				
			}
		};
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","OnlyForNBHC"))
		{
			table.addColumn(prodAreaColumn,"#Unit");
		}else{
//			table.addColumn(prodAreaColumn,"#Area");//commented by Ashwini
			
			table.addColumn(prodAreaColumn,"#Qty");//Added by Ashwini
		}
		table.setColumnWidth(prodAreaColumn, 100,Unit.PX);

		
	}
	

	public void addColumnSorting(){

		//createSortinglbtColumn();

	}

/*	protected void createSortinglbtColumn(){
		List<SalesLineItem> list=getDataprovider().getList();
		columnSort=new ListHandler<SalesLineItem>(list);
		columnSort.setComparator(lbtColumn, new Comparator<SalesLineItem>()
				{
			@Override
			public int compare(SalesLineItem e1,SalesLineItem e2)
			{
				if(e1!=null && e2!=null)
				{if( e1.getLbtTax()!=null && e2.getLbtTax()!=null){
					return e1.getLbtTax().compareTo(e2.getLbtTax());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}*/


	@Override
	public void setEnable(boolean state)
	{ Console.log("1. state="+state);
		
		if(pedioContractFlag){
			setEnableForPedioContract(state);
			return;
		}
		if (AppUtility.checkForProcessConfigurartionIsActiveOrNot(
				"Quotation", "MakeColumnDisable")) {
			disablePriceCol=true;
			Console.log("dis flag value : "+disablePriceCol);
		}   
		
        for(int i=table.getColumnCount()-1;i>-1;i--)
    	  table.removeColumn(i); 
        	  
      
          if(state==true)
          {
        	  
//        	createColumnprodCategoryColumn();
//        	  createColumnprodSrNoColumn();
//      		createColumnprodCodeColumn();
        	if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","MAKEPRODUCTNAMENONEDITABLE")){
        		createViewColumnprodNameColumn();
            }else{
            	createColumnprodNameColumn();
            }
      		
      		/**
    		 * Date 06-06-2018 By vijay
    		 */
//    		getColWarrantyPeriod();
    		/**
    		 * ends here
    		 */
    		

      		/**Date :17/02/2018 BY: Manisha 
      		 * Add frequency & TypeOfTreatment column  at product level as per sonu's requirement.!! 
      		 */
      		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","FrequencyAtProductLevel")){
      		createColumnprodFrequencyColumn();
      		}
      		
      		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","TypeOfTreatmentAtProductLevel")){
      		createColumnTypeOfTreatmentColumn();
      		}
      		/**End**/
      		
      		
    		createColumndateColumn();
      		/**
      		 * Date : 09-08-2017 BY ANIL
      		 */
      		createColumnServiceEndDateColumn();
      		/**
      		 * End
      		 */
      		
      		createColumndurationColumn();
      		createColumnnoServicesColumn();
      		/**
      		 * Date : 29-03-2017 By ANIL
      		 * Earlier quantity was editable made it read only if needed to change ,change through branch button
      		 */
//      		createViewQtyColumn();
//      		createColumnprodQtyColumn();
      		createColumnUOMColumn();
    		createColumnprodAreaColumn();
    		/**
    		 * nidhi
    		 * 11-09-2018
    		 */
    		if(LoginPresenter.billofMaterialActive){
    			if(areaUnit != null && areaUnit.size()>0)
    				areaUnit.clear();
    			else{
    				areaUnit = new ArrayList<String>();
    			}
    			
    			for(Config cfg : LoginPresenter.unitForService){
    				areaUnit.add(cfg.getName());
    			}
    			addEditColumnAreaUnit();
    		}
//      		createColumnPremisesDetails();

      		/**
      		 * nidhi
      		 * 8-08-2018 for map serial no
      		 */
      		if(LoginPresenter.mapModelSerialNoFlag){
      			createColumnProdModelNo();
      			createColumnProdSerialNo();
      		}
      		
//      		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","MakeColumnDisable")){
//      			createViewColumnpriceColumn();
//      		}else{
      			createColumnpriceColumn();
//      		}
      		
      		createColumnPercDiscount();
    		
    		//*************rohan added this for discount amt *************8
    		createColumnDiscountAmt();
    		createColumntotalColumn();
    		
      		
      		
//      		createColumnServicingTimeColumn();
      		
      		
//      		createColumnvatColumn();
//      		createColumnserviceColumn();
//      		createColumnPercDiscount();
//      		createColumntotalColumn();
//      		createColumndeleteColumn();
//      		addFieldUpdater();
      		
  
      		retriveServiceTax();
      	  }
 
          else
          {
//        	createColumnprodCategoryColumn();
//        	  createColumnprodSrNoColumn();
        	
      		createColumnprodCodeColumn();
      		createViewColumnprodNameColumn();
      		
      		

      		
      		/**Date :17/02/2018 BY: Manisha 
      		 * Add frequency & TypeOfTreatment column  at product level as per sonu's requirement.!! 
      		 */
      		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","FrequencyAtProductLevel")){
      		createViewfrequencyColumn();
      		}
      		
      		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","TypeOfTreatmentAtProductLevel")){
      		createViewTypeOfTreatmentColumn();
      		}
      		/**End**/
      		
      		createViewColumndateColumn();
      		/**
      		 * Date :09-08-2017 By ANIL
      		 */
      		createViewColumnServiceEndDateColumn();
      		/**
      		 * ENd
      		 */
      		
      		
      		createColumnViewDuration();
      		createViewColumnnoServicesColumn();
      

			getBranchButtonColumn(); //Ashwini Patil Date:26-09-2022
//			CreateFieldUpdaterOnBranchesViewColumn();
			createFieldUpdaterOnBranchesColumn(true);//29-09-2022
			Console.log("Branch button added");

      		createViewQtyColumn();
      		createViewUOMColumn();
      		createViewColumnProdAreaView();
      		/**
      		 *  *:*:*
    		 * nidhi
    		 * 11-09-2018
    		 */
    		if(LoginPresenter.billofMaterialActive){
    			createViewAreaUOMColumn();
    		}
      		createViewColumnPremisesDetailsColumn();
      		
      		/**
      		 * nidhi
      		 * 8-08-2018
      		 * for map serial no
      		 */
      		if(LoginPresenter.mapModelSerialNoFlag){
      			createColumnViewProdModelNo();
      			createColumnViewProdSerialNo();
      		}
      		createViewColumnpriceColumn();
      		
      		createViewPercDiscount();
      		createViewDiscountAmt();
      		createColumntotalColumn();
      		
//      		createColumnvatColumn();
//      		createColumnserviceColumn();
			createViewVatTaxColumn();
      		createViewServiceTaxColumn();
      		/**
      		 * Date 06/06/2018 By Vijay
      		 */
      		viewColIsTaxInclusive();
      		/**
      		 * ends here
      		 */
      		createViewColumnServicingTimeColumn();
      		getColViewWarrantyPeriod();
      		createColumnViewHSNCodeColumn();
      		createViewRemark();
      		
          }
	}
	
	private void createViewUOMColumn() {
	
		viewUOMcolumn=new  TextColumn<SalesLineItem>(){

			@Override
			public String getValue(SalesLineItem object) 
			    {
				return object.getUnitOfMeasurement();
				}
			};
			table.addColumn(viewUOMcolumn,"#UOM");
			table.setColumnWidth(viewUOMcolumn, 100,Unit.PX);
}
	private void createViewColumnprodNameColumn() {
		
		createColumn = new TextColumn<SalesLineItem>() {
			
			@Override
			public String getValue(SalesLineItem object) {
				if(object.getProductName()!=null)
				return object.getProductName();
				else return "";
			}
		};
		table.addColumn(createColumn,"#Name");
		table.setColumnWidth(createColumn, 100,Unit.PX);
	}
	private void createViewRemark() {

		columnviewRemark = new TextColumn<SalesLineItem>() {
			
			@Override
			public String getValue(SalesLineItem object) {
				if(object.getRemark()!=null)
				return object.getRemark();
				else return "";
			}
		};
		table.addColumn(columnviewRemark,"#Remark");
		table.setColumnWidth(columnviewRemark, 100,Unit.PX);
	}

	
private void createViewColumnProdAreaView() {
		
		viewProdAreaColumn = new TextColumn<SalesLineItem>() {
			@Override
			public String getValue(SalesLineItem object) {
				return object.getArea();
			}
		};
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","OnlyForNBHC"))
		{
			table.addColumn(viewProdAreaColumn,"#Unit");
		}else{
//			table.addColumn(viewProdAreaColumn,"#Area"); 
			table.addColumn(viewProdAreaColumn,"#Qty"); //Added by sheetal:18-11-2021 for UDS ,replacing Area Heading as Qty//
		}
		table.setColumnWidth(viewProdAreaColumn, 100,Unit.PX);
	}
	
	private void createViewColumnPremisesDetailsColumn() {
		viewpremisesDetails=new TextColumn<SalesLineItem>()
				{
			@Override
			public String getValue(SalesLineItem object)
			{
				if(object.getPremisesDetails()!=null){
					return object.getPremisesDetails();
					}
				else{
									
					return "";
					}
			}
				};
				/**Added by Sheetal : 16-03-2022
				 * Des : Replacing Premises heading as Actual Qty with below process configuration,requirement by WHC**/
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","PC_CHANGE_TITLE_PREMISE_TO_QUANTITY")){
					table.addColumn(viewpremisesDetails,"#Actual Qty");
				}else{
					table.addColumn(viewpremisesDetails,"#Premises");
				}			
				table.setColumnWidth(viewpremisesDetails, 100,Unit.PX);
				
				
		
		}
	
	/**Date :17/02/2018 BY: Manisha 
	 * Add frequency & TypeOfTreatment column  at product level as per sonu's requirement.!! 
	 */
	   private void createViewTypeOfTreatmentColumn() {
	       viewtypeoftreatmentcolumn = new TextColumn<SalesLineItem>() {
				
				@Override
				public String getValue(SalesLineItem object) {
					if(object.getTypeoftreatment()!=null)
					return object.getTypeoftreatment();
					else return "";
				}
			};
			table.addColumn(viewtypeoftreatmentcolumn,"#Type Of Treatment");
			table.setColumnWidth(viewtypeoftreatmentcolumn, 100,Unit.PX);
		}
		
		private void createViewfrequencyColumn() {
			viewprodfrequencycolumn = new TextColumn<SalesLineItem>() {
				
				@Override
				public String getValue(SalesLineItem object) {
					if(object.getProfrequency()!=null)
					return object.getProfrequency();
					else return "";
				}
			};
			table.addColumn(viewprodfrequencycolumn,"#Frequency");
			table.setColumnWidth(viewprodfrequencycolumn, 100,Unit.PX);
			}
	
		/**End**/
	@Override
	public void applyStyle()
	{

	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<SalesLineItem>()
				{
			@Override
			public Object getKey(SalesLineItem item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}

	public double calculateTotalExcludingTax()
	{
		List<SalesLineItem>list=getDataprovider().getList();
		double sum=0,priceVal=0;
		for(int i=0;i<list.size();i++)
		{
			SalesLineItem entity=list.get(i);
			SuperProduct prod=entity.getPrduct();
			double taxAmt=removeAllTaxes(prod);
//			if(entity.getPercentageDiscount()==null){
//				priceVal=entity.getPrice()-taxAmt;
//				priceVal=priceVal*entity.getQty();
//				sum=sum+priceVal;
//			}
//			if(entity.getPercentageDiscount()!=null){
//				priceVal=entity.getPrice()-taxAmt;
//				priceVal=priceVal-(priceVal*entity.getPercentageDiscount()/100);
//				priceVal=priceVal*entity.getQty();
//				
//				sum=sum+priceVal;
//			}
			
			
			
			//*****************new code for discount by rohan ***************** 
//*******************this old code is commented by  vijay for sq area + disc on total *************** 
			
//			if((entity.getPercentageDiscount()==null && entity.getPercentageDiscount()==0) && (entity.getDiscountAmt()==0) ){
//				
//				System.out.println("inside both 0 condition");
//				
//				priceVal=entity.getPrice()-taxAmt;
//				priceVal=priceVal*entity.getQty();
//				sum=sum+priceVal;
//				System.out.println("RRRRRRRRRRRRRR sum"+sum);
//			}
//			
//			else if((entity.getPercentageDiscount()!=null)&& (entity.getDiscountAmt()!=0)){
//				
//				System.out.println("inside both not null condition");
//				
//				priceVal=entity.getPrice()-taxAmt;
//				priceVal=priceVal-(priceVal*entity.getPercentageDiscount()/100);
//				priceVal=priceVal-entity.getDiscountAmt();
//				priceVal=priceVal*entity.getQty();
//				
//				sum=sum+priceVal;
//				System.out.println("RRRRRRRRRRRRRR sum"+sum);
//			}
//			else 
//			{
//				System.out.println("inside oneof the null condition");
//				
//					priceVal=entity.getPrice()-taxAmt;
//					if(entity.getPercentageDiscount()!=null){
//						System.out.println("inside getPercentageDiscount oneof the null condition");
//					priceVal=priceVal-(priceVal*entity.getPercentageDiscount()/100);
//					}
//					else 
//					{
//						System.out.println("inside getDiscountAmt oneof the null condition");
//					priceVal=priceVal-entity.getDiscountAmt();
//					}
//					
//					priceVal=priceVal*entity.getQty();
//					
//					sum=sum+priceVal;
//					System.out.println("RRRRRRRRRRRRRR sum"+sum);
//			}
//			
//	
//		}
//		return sum;
			
			
		// *******************   new code by   vijay (rohan) for sq area + disc on total ********************************	
			
			/**
			 * rohan removed quantity form price calculation Required for NBHC 
			 * Date : 14- 10 - 2016
			 * i have removed field (entity.getQty()) from every calculation
			 */
			
			System.out.println("Square Area === "+entity.getArea());
			
			if((entity.getPercentageDiscount()==null && entity.getPercentageDiscount()==0) && (entity.getDiscountAmt()==0) ){
				
				System.out.println("inside both 0 condition");
				/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
				/**
				 * nidhi
				 * 21-09-2018
				 */
				if(!entity.getArea().equalsIgnoreCase("NA") && !LoginPresenter.areaWiseCalRestricFlg){
				double squareArea = Double.parseDouble(entity.getArea());
				priceVal=entity.getPrice()-taxAmt;
				priceVal=priceVal*squareArea;
				sum=sum+priceVal;
				System.out.println("RRRRRRRRRRRRRR sum"+sum);
				}
				else{
					System.out.println("Old code");
					priceVal=entity.getPrice()-taxAmt;
//					priceVal=priceVal*entity.getQty();
					sum=sum+priceVal;
					System.out.println("RRRRRRRRRRRRRR sum"+sum);
				}
				
				/******************** vijay ***************************/

			}
			
			else if((entity.getPercentageDiscount()!=null)&& (entity.getDiscountAmt()!=0)){
				
				System.out.println("inside both not null condition");
				
				/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
				/**
				 * nidhi
				 * 21-09-20
				 */
				if(!entity.getArea().equalsIgnoreCase("NA") && !LoginPresenter.areaWiseCalRestricFlg){
					priceVal=entity.getPrice()-taxAmt;
					double squareArea = Double.parseDouble(entity.getArea());
					priceVal=priceVal*squareArea;
					priceVal=priceVal-(priceVal*entity.getPercentageDiscount()/100);
					priceVal=priceVal-entity.getDiscountAmt();
					sum=sum+priceVal;
					System.out.println("RRRRRRRRRRRRRR sum"+sum);
					
				}else{
					System.out.println("Old code ===");
					priceVal=entity.getPrice()-taxAmt;
//					priceVal=priceVal*entity.getQty();
					priceVal=priceVal-(priceVal*entity.getPercentageDiscount()/100);
					priceVal=priceVal-entity.getDiscountAmt();
					sum=sum+priceVal;
					System.out.println("RRRRRRRRRRRRRR sum"+sum);
				}
				
				
				
				/********************************************************/
				
				
			}
			else 
			{
				System.out.println("inside oneof the null condition");
				
					priceVal=entity.getPrice()-taxAmt;
					if(entity.getPercentageDiscount()!=null){
						System.out.println("inside getPercentageDiscount oneof the null condition");
						/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
						/**
						 * nidhi
						 * 21-09-2018
						 */
						if(!entity.getArea().equalsIgnoreCase("NA") && !LoginPresenter.areaWiseCalRestricFlg){
							double squareArea = Double.parseDouble(entity.getArea());
							priceVal=priceVal*squareArea;
							priceVal=priceVal-(priceVal*entity.getPercentageDiscount()/100);
							sum=sum+priceVal;
							System.out.println("RRRRRRRRRRRRRR sum"+sum);
						}else{
							System.out.println("old code");
//							priceVal=priceVal*entity.getQty();
							sum=sum+priceVal-(priceVal*entity.getPercentageDiscount()/100);
							
						}
						/****************************************************************************************/
					}
					else 
					{
						System.out.println("inside getDiscountAmt oneof the null condition");
						/******************** Square Area Calculation code added in if condition and without square area code calculation in else block ***************************/
						/**
						 * nidhi
						 * 21-09-2018
						 */
						if(!entity.getArea().equalsIgnoreCase("NA") && !LoginPresenter.areaWiseCalRestricFlg){
							double squareArea = Double.parseDouble(entity.getArea());
							priceVal=priceVal*squareArea;
							priceVal=priceVal-entity.getDiscountAmt();
							sum=sum+priceVal;
							System.out.println("RRRRRRRRRRRRRR sum"+sum);
						}else{
							System.out.println("old code");
//							priceVal=priceVal*entity.getQty();
							sum=sum+priceVal-entity.getDiscountAmt();
							
						}
						/********************new code end here************************************************/
					}
					
					System.out.println("RRRRRRRRRRRRRR sum"+sum);
			}
			
	
		}
		return sum;			
	}
	


	public double removeAllTaxes(SuperProduct entity)
	{
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;
		if(entity instanceof ServiceProduct)
		{
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
		}

		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
			retrVat=entity.getPrice()/(1+(vat/100));
			retrVat=entity.getPrice()-retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=entity.getPrice()/(1+service/100);
			retrServ=entity.getPrice()-retrServ;
		}
		if(service!=0&&vat!=0){
//			// Here if both are inclusive then first remove service tax and then on that amount
//			// calculate vat.
//			double removeServiceTax=(entity.getPrice()/(1+service/100));
//						
//			//double taxPerc=service+vat;
//			//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
//			retrServ=(removeServiceTax/(1+vat/100));
//			retrServ=entity.getPrice()-retrServ;
			
			
			if(entity instanceof ItemProduct)
			{
				ItemProduct prod=(ItemProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{

					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
//					retrVat=0;

				}
				else{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
			
			if(entity instanceof ServiceProduct)
			{
				ServiceProduct prod=(ServiceProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{
					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
				}
				else
				{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
		}
		tax=retrVat+retrServ;
		return tax;
	}
	
	
		public boolean removeChargesOnDelete(){
		
		int tableSize=getDataprovider().getList().size();
		
		if(tableSize<1){
			return false;
		}
		return true;
	}
		
		public int identifyProductId()
		{
			return productIdCount;
		}
		public int identifySrNo() {
			return productSrNoCount;
		}
		
		
		@Override
		public void onClick(ClickEvent event) {
			if(event.getSource().equals(advSchPopup.btnOk)){
				getDataprovider().getList().get(rowIndex).setQuantity(getNoOfSelectedBranch(advSchPopup.getBranchTable().getValue()));
////				branchSchedulingInfo=AppUtility.getCustomersUpdatedBranchSchedulingList(advSchPopup.getBranchTable().getValue());
//				
//				String branchSchedulingInfo="";
//				String branchSchedulingInfo1="";
//				String branchSchedulingInfo2="";
//				String branchSchedulingInfo3="";
//				String branchSchedulingInfo4="";
//				
//				/**
//				 * Date : 14-06-2017 BY ANIL
//				 */
//				String branchSchedulingInfo5="";
//				String branchSchedulingInfo6="";
//				String branchSchedulingInfo7="";
//				String branchSchedulingInfo8="";
//				String branchSchedulingInfo9="";
//				/**
//				 * End
//				 */
//				
//				String branchArray[]=AppUtility.getCustomersUpdatedBranchSchedulingList(advSchPopup.getBranchTable().getValue());
//				System.out.println("BRANCH ARRAY OK "+branchArray.length);
//				
//					if(branchArray[0]!=null){
//						branchSchedulingInfo=branchArray[0];
//					}
//					if(branchArray[1]!=null){
//						branchSchedulingInfo1=branchArray[1];
//					}
//					if(branchArray[2]!=null){
//						branchSchedulingInfo2=branchArray[2];
//					}
//					if(branchArray[3]!=null){
//						branchSchedulingInfo3=branchArray[3];
//					}
//					if(branchArray[4]!=null){
//						branchSchedulingInfo4=branchArray[4];
//					}
//					
//					/**
//					 * Date : 14-06-2017 BY ANIL
//					 */
//					if(branchArray[5]!=null){
//						branchSchedulingInfo5=branchArray[5];
//					}
//					if(branchArray[6]!=null){
//						branchSchedulingInfo6=branchArray[6];
//					}
//					if(branchArray[7]!=null){
//						branchSchedulingInfo7=branchArray[7];
//					}
//					if(branchArray[8]!=null){
//						branchSchedulingInfo8=branchArray[8];
//					}
//					if(branchArray[9]!=null){
//						branchSchedulingInfo9=branchArray[9];
//					}
//					
//					/**
//					 * End
//					 */
//				
//				getDataprovider().getList().get(rowIndex).setBranchSchedulingInfo(branchSchedulingInfo);
//				getDataprovider().getList().get(rowIndex).setBranchSchedulingInfo1(branchSchedulingInfo1);
//				getDataprovider().getList().get(rowIndex).setBranchSchedulingInfo2(branchSchedulingInfo2);
//				getDataprovider().getList().get(rowIndex).setBranchSchedulingInfo3(branchSchedulingInfo3);
//				getDataprovider().getList().get(rowIndex).setBranchSchedulingInfo4(branchSchedulingInfo4);
//				
//				/**
//				 * Date : 14-06-2017 By ANIL
//				 */
//				
//				getDataprovider().getList().get(rowIndex).setBranchSchedulingInfo5(branchSchedulingInfo5);
//				getDataprovider().getList().get(rowIndex).setBranchSchedulingInfo6(branchSchedulingInfo6);
//				getDataprovider().getList().get(rowIndex).setBranchSchedulingInfo7(branchSchedulingInfo7);
//				getDataprovider().getList().get(rowIndex).setBranchSchedulingInfo8(branchSchedulingInfo8);
//				getDataprovider().getList().get(rowIndex).setBranchSchedulingInfo9(branchSchedulingInfo9);
//				
//				/**
//				 * End
//				 */
				
				/**
				 * Date 30-04-2018 By vijay updated code using hashmap for customer branches scheduling services
				 * above old code commented and old contract renewal for else block
				 */
				List<BranchWiseScheduling> brancheslist = advSchPopup.getBranchTable().getDataprovider().getList();
				ArrayList<BranchWiseScheduling> customerBrancheslist = new ArrayList<BranchWiseScheduling>();
				customerBrancheslist.addAll(brancheslist);
				
				boolean serviceAmountFlag = false;
				for(BranchWiseScheduling obj:customerBrancheslist){
					if(obj.isCheck() && obj.getAmount()>0){
						serviceAmountFlag = true;
					}
				}
				if(serviceAmountFlag){
					double totalAmount = 0;
						for(BranchWiseScheduling branchWiseScheduling : customerBrancheslist){
							if(branchWiseScheduling.isCheck()){
								totalAmount += branchWiseScheduling.getAmount();
							}
						}
						if(totalAmount !=  getDataprovider().getList().get(rowIndex).getTotalAmount()){
							GWTCAlert alert = new GWTCAlert();
							alert.alert("Customer branches service amount should be equal to product total amount.");
						}

				}
				/**
				 * @author Anil
				 * @since 07-06-2020
				 * if component is not mentioned then also asset id need to be generated
				 */
				if(ContractForm.complainTatFlag){
					for(BranchWiseScheduling obj:customerBrancheslist){
						List<ComponentDetails> list=new ArrayList<ComponentDetails>();
						/**
						 * @author Anil
						 * @since 07-01-2021
						 * here we were restting every data,it should be not like that
						 * if user has not updated component details only selected the branch then
						 * only map default data to component list
						 * Raised by RAHUL Tiwari
						 */
						if(obj.isCheck()
								&&(obj.getComponentList()==null||obj.getComponentList().size()==0)){
//						if(obj.isCheck()){
							Console.log("Component list null");
							if(obj.getAssetQty()>0){
								for(int i=0;i<obj.getAssetQty();i++){
									ComponentDetails comp=new ComponentDetails();
//									comp.setMfgNum(i+"");
									comp.setComponentName(obj.getComponentName());
									comp.setSrNo(i);
									/**
									 * @author Anil
									 * @since 15-10-2020
									 * setting duration
									 */
									comp.setDurationForReplacement(obj.getDurationForReplacement());
									
									list.add(comp);
								}
							}
							if(list.size()!=0){
								obj.setComponentList(list);
							}
						}
					}
				}
				
				if(LoginPresenter.billofMaterialActive && customerBrancheslist!=null && customerBrancheslist.size()>0){
					 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(getDataprovider().getList().get(rowIndex).getPrduct());
					 if(billPrDt!=null){
//							customerBrancheslist.addAll(getDataprovider().getList().get(rowIndex).getCustomerBranchSchedulingInfo().get(getDataprovider().getList().get(rowIndex).getProductSrNo()));
							
							if(LoginPresenter.billofMaterialActive && customerBrancheslist!=null && customerBrancheslist.size()>0){
								for(BranchWiseScheduling branch : customerBrancheslist){
									if(branch.isCheck() )
										{
										if(branch.getUnitOfMeasurement()==null){
											form.showDialogMessage("Unit of mesuarement or area is not define for selected branch ."
													+ "\n Please define area and unit of masuarement.");
											break;
										}
										if( branch.getArea()==0 ||  branch.getUnitOfMeasurement().trim().length() ==0){
											form.showDialogMessage("Unit of mesuarement or area is not define for selected branch ."
													+ "\n Please define area and unit of masuarement.");
												break;
										}
									}
								}
							}
					 }
					
				}
				if(getDataprovider().getList().get(rowIndex).getCustomerBranchSchedulingInfo()!=null){
					if(getDataprovider().getList().get(rowIndex).getCustomerBranchSchedulingInfo().containsKey(getDataprovider().getList().get(rowIndex).getProductSrNo())){
						Integer productSrNo = getDataprovider().getList().get(rowIndex).getProductSrNo();
						getDataprovider().getList().get(rowIndex).getCustomerBranchSchedulingInfo().put(productSrNo, customerBrancheslist);
					}
					Console.log("Updating branch scheduling list on ok click..");
				}
				else{
					String branchArray[]=AppUtility.getCustomersUpdatedBranchSchedulingList(advSchPopup.getBranchTable().getValue());
					Console.log("branchArray.length =="+branchArray.length);
					if(branchArray.length>0){
						ArrayList<BranchWiseScheduling> branchlist = AppUtility.getBranchSchedulingList(branchArray);
						Integer prodSrNo = getDataprovider().getList().get(rowIndex).getProductSrNo();
						HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
						customerBranchlist.put(prodSrNo, branchlist);
						getDataprovider().getList().get(rowIndex).setCustomerBranchSchedulingInfo(customerBranchlist);
				
					}
				}
				/**
				 * ends here
				 */
				
				table.redraw();
				System.out.println("DONE");
				if(!LoginPresenter.serviceScheduleNewLogicFlag){
					if(documentType.equals("Contract")){
						ContractPresenter.setScheduleServiceOnSchedulingTable(AppUtility.getScheduleList(getDataprovider().getList()));
					}else if(documentType.equals("QuickContract")){
						QuickContractPresentor.setScheduleServiceOnSchedulingTable(AppUtility.getScheduleList(getDataprovider().getList()));
					}
				}
				else{
					/**
					 * @author Vijay Date :- 13-11-2021
					 * Des :- added code for innovative service scheduling new logic
					 */
					if(documentType.equals("Contract")){
//						ContractPresenter.setScheduleServiceOnSchedulingTable(AppUtility.getScheduleList(getDataprovider().getList()));
						ContractForm form = new ContractForm();
						ArrayList<ServiceSchedule> serviceschedulelist = new ArrayList<ServiceSchedule> ();
						serviceschedulelist.addAll(form.getServiceSchedulelist("default",getDataprovider().getList(),null,null,null,0,0,0,0,null));
						ContractPresenter.setScheduleServiceOnSchedulingTable(serviceschedulelist);

					}else if(documentType.equals("QuickContract")){
						ArrayList<ServiceSchedule> serviceschedulelist = new ArrayList<ServiceSchedule> ();
						serviceschedulelist.addAll(form.getServiceSchedulelist("default",getDataprovider().getList(),null,null,null,0,0,0,0,null));
						QuickContractPresentor.setScheduleServiceOnSchedulingTable(serviceschedulelist);
					}
				}
				
				panel.hide();
				if(form!=null)
				form.addClickEventOnActionAndNavigationMenus(); //Ashwini Patil Date:27-03-2024
			}
			if(event.getSource().equals(advSchPopup.btnCancel)){
				panel.hide();
				if(form!=null)
				form.addClickEventOnActionAndNavigationMenus(); //Ashwini Patil Date:27-03-2024
			}
			
			if(event.getSource().equals(advSchViewPopup.btnCancel)){
				panel.hide();
				if(form!=null)
				form.addClickEventOnActionAndNavigationMenus(); //Ashwini Patil Date:27-03-2024
			}
			if(event.getSource().equals(serviceBranchesPopup.getLblOk())){
				List<BranchWiseScheduling> brancheslist = serviceBranchesPopup.getServiceBranchesTable().getDataprovider().getList();
				ArrayList<BranchWiseScheduling> customerBrancheslist = new ArrayList<BranchWiseScheduling>();
				customerBrancheslist.addAll(brancheslist);
				double totalAmount = 0;
				boolean flag = true;
				if(customerBrancheslist!=null && customerBrancheslist.size()>0){
					for(BranchWiseScheduling branchWiseScheduling : customerBrancheslist){
						if(branchWiseScheduling.isCheck()){
							totalAmount += branchWiseScheduling.getArea();
						}
					}
					if(totalAmount !=  getDataprovider().getList().get(rowIndex).getTotalAmount()){
						GWTCAlert alert = new GWTCAlert();
						alert.alert("Service branches total amount should be equal to product total amount.");
						flag = false;
					}
				}
				if(flag){
				  if(getDataprovider().getList().get(rowIndex).getServiceBranchesInfo()!=null){
					if(getDataprovider().getList().get(rowIndex).getServiceBranchesInfo().containsKey(getDataprovider().getList().get(rowIndex).getProductSrNo())){
						Integer productSrNo = getDataprovider().getList().get(rowIndex).getProductSrNo();
						getDataprovider().getList().get(rowIndex).getServiceBranchesInfo().put(productSrNo, customerBrancheslist);
					}
				  }
				}else{
					ArrayList<BranchWiseScheduling> customerBrancheslist1 = new ArrayList<BranchWiseScheduling>();
					for(BranchWiseScheduling branch : customerBrancheslist){
						branch.setArea(0);
						branch.setCheck(false);
						customerBrancheslist1.add(branch);
					}
					if(getDataprovider().getList().get(rowIndex).getServiceBranchesInfo()!=null){
						if(getDataprovider().getList().get(rowIndex).getServiceBranchesInfo().containsKey(getDataprovider().getList().get(rowIndex).getProductSrNo())){
							Integer productSrNo = getDataprovider().getList().get(rowIndex).getProductSrNo();
							getDataprovider().getList().get(rowIndex).getServiceBranchesInfo().put(productSrNo, customerBrancheslist1);
						}
					  }
				}
				
				/**
				 * ends here
				 */
				
				table.redraw();
		
				serviceBranchesPopup.hidePopUp();
				if(form!=null)
				form.addClickEventOnActionAndNavigationMenus(); //Ashwini Patil Date:27-03-2024
			}
			if(event.getSource().equals(serviceBranchesPopup.getLblCancel())){
				serviceBranchesPopup.hidePopUp();
				if(form!=null)
				form.addClickEventOnActionAndNavigationMenus(); //Ashwini Patil Date:27-03-2024
			}
			
		}
		
		public double getNoOfSelectedBranch(List<BranchWiseScheduling> list){
			int noOfBranches=0;
			for(BranchWiseScheduling branch:list){
				if(branch.isCheck()==true){
					noOfBranches++;
				}
			}
			return noOfBranches;
		}
		
		
		/** 28-09-2017 by sagar sore [to calculate total amount for product]**/
		public double calculateTotalAmount(SalesLineItem object){
		
				double total=0;
				SuperProduct product=object.getPrduct();
				double tax=removeAllTaxes(product);
				double origPrice=object.getPrice()-tax;
		
				// new code by vijay add code for area calculation
		
				if(object.getArea().equals("") || object.getArea().equals("0")){
					object.setArea("NA");
				}
				
				System.out.println("area == "+object.getArea());
				
				if((object.getPercentageDiscount()==null && object.getPercentageDiscount()==0) && (object.getDiscountAmt()==0) )
				{
					
					System.out.println("inside both 0 condition");
					
					/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
					if(!object.getArea().equalsIgnoreCase("NA") && !LoginPresenter.areaWiseCalRestricFlg){
						double area = Double.parseDouble(object.getArea());
						 total = origPrice*area;
						}else{
							total = origPrice;
//							total=total;
						}
					
				}
				/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
				else if((object.getPercentageDiscount()!=null)&& (object.getDiscountAmt()!=0)){
					
					System.out.println("inside both not null condition");
					System.out.println("total amount before area calculation =="+total);
					/**
					 * nidhi
					 * 21-09-2018
					 */
					if(!object.getArea().equalsIgnoreCase("NA")  && !LoginPresenter.areaWiseCalRestricFlg){
						double area = Double.parseDouble(object.getArea());
						total=origPrice*area;
						total = total - (total*object.getPercentageDiscount()/100);
						total = total - object.getDiscountAmt();
					}else{
						total=origPrice;
						total=total-(total*object.getPercentageDiscount()/100);
						total=total-object.getDiscountAmt();
					}
				}
				else 
				{
					System.out.println("inside oneof the null condition");
						if(object.getPercentageDiscount()!=null || object.getPercentageDiscount()!=0){
							/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
							/**
							 * nidhi
							 * 21-09-2018
							 */
							if(!object.getArea().equalsIgnoreCase("NA")  && !LoginPresenter.areaWiseCalRestricFlg){
								double area = Double.parseDouble(object.getArea());
								total=origPrice*area;
								total = total - (total*object.getPercentageDiscount()/100);
								
							}else{
								System.out.println("old code");
								total=origPrice;
								total=total-(total*object.getPercentageDiscount()/100);
							}
						}
						else 
						{
							System.out.println("inside getDiscountAmt oneof the null condition");
							/******** vijay added code for square area wise calculation new code added in if condition and old code added in else***************/
							System.out.println("tatal $$$$$$$$$$ ===="+total);
							/**
							 * nidhi
							 * 21-09-2018
							 */
							if(!object.getArea().equalsIgnoreCase("NA")  && !LoginPresenter.areaWiseCalRestricFlg){
								double area = Double.parseDouble(object.getArea());
								total=origPrice*area;
								total = total - object.getDiscountAmt();
								
							}else{
								total=total;
								total=total-object.getDiscountAmt();
							}

						}			
				}
				object.setTotalAmount(total);
				return total;
				}
	
		
		
		public void makefrequencydropdown(){
			  ArrayList<Config> freqencyarray=new ArrayList<Config>();
			    freqencyarray=LoginPresenter.globalConfig;
			    System.out.println("ssssssssssssssss"+freqencyarray.size());
			   
			    frequencylist=new ArrayList<String>();
			    frequencylist.add("--SELECT--");
			   
			     if(freqencyarray.size()!=0){
			    	 for(int a=0;a<freqencyarray.size();a++){
			    		 if(freqencyarray.get(a).getType()==102 && freqencyarray.get(a).isStatus()==true){
			    			 if(freqencyarray.get(a).getName()!=null){
			    				 try{
			    			      frequencylist.add(freqencyarray.get(a).getName());
			    				 }
			    				 catch(Exception e){
			    					 System.out.println(e);
			    				 }
			    			 }
			    		 }
				    }
			    }
		
			   }
		
		public void maketypeoftreatmentdropdown(){
			 ArrayList<Config> typeoftreatmentarray=new ArrayList<Config>();
			 typeoftreatmentarray=LoginPresenter.globalConfig;
			    typeoftreatmentlist=new ArrayList<String>();
			    typeoftreatmentlist.add("--SELECT--");
			    
			     if(typeoftreatmentarray.size()!=0){
			    	 for(int i=0;i<typeoftreatmentarray.size();i++){
			    		 if(typeoftreatmentarray.get(i).getType()==103 && typeoftreatmentarray.get(i).isStatus()==true){
			    			 if(typeoftreatmentarray.get(i).getName()!=null){
			    				 try{
			    					 typeoftreatmentlist.add(typeoftreatmentarray.get(i).getName());
			    				  }
			    				 catch(Exception e){
			    					 System.out.println(e);
			    				 }
			    			 }
			    		 }
				    }
			    }
			
		}
		/**End**/


		
		private void createColumnProdModelNo() {
			EditTextCell editCell=new EditTextCell();
			getProModelNo=new Column<SalesLineItem,String>(editCell)
					{
				@Override
				public String getValue(SalesLineItem object)
				{
					if(object.getProModelNo()!=null){
						return object.getProModelNo()+"";}
					else{
						return "";
						}
				}
				};
				table.addColumn(getProModelNo,"#Model No");	
				table.setColumnWidth(getProModelNo, 100,Unit.PX);
		}
		
		private void createColumnProdSerialNo() {
			EditTextCell editCell=new EditTextCell();
			getProSerialNo=new Column<SalesLineItem,String>(editCell)
					{
				@Override
				public String getValue(SalesLineItem object)
				{
					if(object.getProSerialNo()!=null){
						return object.getProSerialNo()+"";}
					else{
						return "";
						}
				}
				};
				table.addColumn(getProSerialNo,"#Serial No");	
				table.setColumnWidth(getProSerialNo, 100,Unit.PX);
		}
		
		private void createColumnViewProdModelNo() {
			getViewProModelNo=new TextColumn<SalesLineItem>()
					{
				@Override
				public String getValue(SalesLineItem object)
				{
					if(object.getProModelNo()!=null){
						return object.getProModelNo();
						}
					else{
										
						return "";
						}
				}
					};
					table.addColumn(getProModelNo,"#Model No");	
					table.setColumnWidth(getProModelNo, 100,Unit.PX);
			
			}
		
		private void createColumnViewProdSerialNo() {
			getViewProSerialNo=new TextColumn<SalesLineItem>()
					{
				@Override
				public String getValue(SalesLineItem object)
				{
					if(object.getProSerialNo()!=null){
						return object.getProSerialNo();
						}
					else{
										
						return "";
						}
				}
					};
					table.addColumn(getProSerialNo,"#Serial No");	
					table.setColumnWidth(getProSerialNo, 100,Unit.PX);
			
			}


		private void createFieldUpdaterProModelNoColumn() {
			getProModelNo.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
					{
				@Override

				public void update(int index,SalesLineItem object,String value)
				{

					try{
						System.out.println("value"+value);
						object.setProModelNo(value);
					}
					catch (Exception e)
					{

					}
					table.redrawRow(index);
				}
					});
		}
		
		
		private void createFieldUpdateProSerailNo() {
			getProSerialNo.setFieldUpdater(new FieldUpdater<SalesLineItem, String>()
					{
				@Override

				public void update(int index,SalesLineItem object,String value)
				{

					try{
						System.out.println("value"+value);
						object.setProSerialNo(value);
					}
					catch (Exception e)
					{

					}
					table.redrawRow(index);
				}
					});
		}
		public void addEditColumnAreaUnit(){
			SelectionCell employeeselection= new SelectionCell(areaUnit);
			unitColumn = new Column<SalesLineItem, String>(employeeselection) {
				@Override
				public String getValue(SalesLineItem object) {
					if (object.getAreaUnit()!= null
							&&!object.getAreaUnit().equals("")) {
						
						object.setAreaUnitEditValue(object.getAreaUnit());

						return object.getAreaUnit();
					} else
						return "NA";
					
				}
			};
			table.addColumn(unitColumn, "#UOM");
			table.setColumnWidth(unitColumn,130,Unit.PX);
			table.setColumnWidth(unitColumn, 100,Unit.PX);
			
		}
		
		public void updateAreaUOMColumn()
		{

			unitColumn.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
				
				@Override
				public void update(int index, SalesLineItem object, String value) {
					try {
						String val1 = (value.trim());
						object.setAreaUnitEditValue(val1);
//						object.setUnitOfMeasurement(val1);
						object.setAreaUnit(val1);
						productIdCount=object.getPrduct().getCount();
						productSrNoCount = object.getProductSrNo();
						
						RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
						
					} catch (Exception e) {
					}
					table.redrawRow(index);
				}
			});
		}
		
		protected void createViewAreaUOMColumn(){
			
			viewUnitColumn=new TextColumn<SalesLineItem>() {
				public String getValue(SalesLineItem object)
				{
					return object.getAreaUnit();
				}
			};
			table.addColumn(viewUnitColumn,"# UOM" );
			table.setColumnWidth(viewUnitColumn, 100,Unit.PX);
		}
		
		/** date 11.12.2018 added by komal for nbhc service branches button and popup **/
		protected void getServiceBranchesButton() {
			ButtonCell btnCell = new ButtonCell();
			getServiceBranchesButton = new Column<SalesLineItem, String>(btnCell) {
				@Override
				public String getValue(SalesLineItem object) {
					return "Service Branches";
				}
			};
			table.addColumn(getServiceBranchesButton, "Service Branches");
			table.setColumnWidth(getServiceBranchesButton, 100, Unit.PX);
		}
		
		protected void addFieldUpdaterOnServiceBranchesButton() {
			getServiceBranchesButton.setFieldUpdater(new FieldUpdater<SalesLineItem, String>() {
				@Override
				public void update(int index, SalesLineItem object,String value) {
					List<BranchWiseScheduling> branchList = new ArrayList<BranchWiseScheduling>();
					if(object.getServiceBranchesInfo() != null && object.getServiceBranchesInfo().size() >0){
						branchList = object.getServiceBranchesInfo().get(object.getProductSrNo());
					}
					serviceBranchesPopup.showPopUp();
					if(branchList != null){
						serviceBranchesPopup.getServiceBranchesTable().getDataprovider().setList(branchList);
					}
					rowIndex=index;
					
				}
			});
		}
		
	/**
	 * @author Anil
	 * @since 11-06-2020
	 * line items for pedio contract
	 */
	
	public SalesLineItemTable(boolean pedioContractFlag){
		super(pedioContractFlag);
		this.pedioContractFlag=pedioContractFlag;
		createTable1();
		advSchPopup.btnOk.addClickHandler(this);
		advSchPopup.btnCancel.addClickHandler(this);
		table.setHeight("250px");
	}
	
	public void createTable1() {
		Console.log("in saleslinetable createTable1 newModeFlag="+newModeFlag);
		if (newModeFlag == false) {
			createColumnprodCodeColumn();
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","MAKEPRODUCTNAMENONEDITABLE")){
        		createViewColumnprodNameColumn();
            }else{
            	createColumnprodNameColumn();
            }
			createColumndateColumn();
			createColumnServiceEndDateColumn();
			createColumndurationColumn();
			createColumnnoServicesColumn();
			createColumnprodAreaColumn();
			createColumnPremisesDetails();
			if(newBranchFlag||pedioContractFlag){
				getBranchButtonColumn();
			}
			createColumndeleteColumn();
			addFieldUpdater();
			table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		}
	}
	
	public void pedioContractFieldUpdater(){
		if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","MAKEPRODUCTNAMENONEDITABLE"))
        	createFieldUpdaterproductNameColumn();
		createFieldUpdaterdateColumn();
//		createFieldUpdaterPremisesColumn();
		createFieldUpdaterprodDurationColumn();
		createFieldUpdaterprodServices();
		
		createFieldUpdaterdeleteColumn();
		createFieldUpdaterProdAreaColumn();
		
		if(newBranchFlag||pedioContractFlag){
			Console.log("PEDIO UPDATOR 1");
			createFieldUpdaterOnBranchesColumn(false);
		}
		addFieldUpdaterOnServiceEndDateColumn();
	}
	
	public void setEnableForPedioContract(boolean state) {
		for (int i = table.getColumnCount() - 1; i > -1; i--)
			table.removeColumn(i);
		
		if (state == true) {
			createColumnprodCodeColumn();
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","MAKEPRODUCTNAMENONEDITABLE")){
        		createViewColumnprodNameColumn();
            }else{
            	createColumnprodNameColumn();
            }
			createColumndateColumn();
			createColumnServiceEndDateColumn();
			createColumndurationColumn();
			createColumnnoServicesColumn();
			createColumnprodAreaColumn();
			createColumnPremisesDetails();
			if(newBranchFlag||pedioContractFlag){
				getBranchButtonColumn();
			}
			createColumndeleteColumn();
			addFieldUpdater();
		}else {
			createColumnprodCodeColumn();
			createViewColumnprodNameColumn();
			createViewColumndateColumn();
			createViewColumnServiceEndDateColumn();
			createColumnViewDuration();
			createViewColumnnoServicesColumn();
			createViewColumnProdAreaView();
			createViewColumnPremisesDetailsColumn();
		}
	}
		

}
