package com.slicktechnologies.client.views.contract;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.dom.client.Style.FontStyle;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.BranchWiseScheduling;

public class AdvanceSchedulingPopup extends AbsolutePanel implements ChangeHandler{


	BranchWiseSchedulingTable branchTable;
	Button btnOk,btnCancel;
	ListBox lbDay;
	
	boolean complainTatFlag=false;
	
	private void initializeWidget(){
		branchTable=new BranchWiseSchedulingTable();
		branchTable.getTable().setHeight("200px");
		btnOk=new Button("Ok");
		btnOk.getElement().getStyle().setWidth(60, Unit.PX);
		btnCancel=new Button("Cancel");
		lbDay=new ListBox();
		lbDay.insertItem("--SELECT--",0);
		lbDay.insertItem("Sunday", 1);
		lbDay.insertItem("Monday", 2);
		lbDay.insertItem("Tuesday", 3);
		lbDay.insertItem("Wednesday", 4);
		lbDay.insertItem("Thursday",5);
		lbDay.insertItem("Friday", 6);
		lbDay.insertItem("Saturday", 7);
		
		lbDay.addChangeHandler(this);
		
		complainTatFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ComplainServiceWithTurnAroundTime");
	}
	
	public AdvanceSchedulingPopup() {
		initializeWidget();
		
		InlineLabel label=new InlineLabel("Day ");
//		label.getElement().getStyle().setFontSize(20, Unit.PX);
//		label.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		label.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		add(label,35,15);
		add(lbDay,100,15);
		FlowPanel flowPanel=new FlowPanel();
		flowPanel.add(branchTable.getTable());
		
		/**
		 * @author Anil
		 * @since 08-06-2020
		 */
		if(complainTatFlag){
			branchTable.getTable().getElement().getStyle().setHeight(560, Unit.PX);
			flowPanel.setWidth("785px");
			add(btnOk,300,610);
			add(btnCancel,400,610);
		}else{
			flowPanel.setWidth("550px");
			add(btnOk,125,250);
			add(btnCancel,250,250);
		}
		flowPanel.setHeight("180px");
//		flowPanel.getElement().getStyle().setBorderWidth(1, Unit.PX);
		
		add(flowPanel,10,40);
		
//		add(btnOk,125,250);
//		add(btnCancel,250,250);
		
		/**
		 * @author Anil
		 * @since 08-06-2020
		 */
		if(complainTatFlag){
			setSize("800px","650px");
		}else{
			setSize("600px","290px");
		}
		this.getElement().setId("login-form");
	}
	
	public void clear(){
		branchTable.connectToLocal();
		lbDay.setSelectedIndex(0);
	}

	
	public BranchWiseSchedulingTable getBranchTable() {
		return branchTable;
	}

	public void setBranchTable(BranchWiseSchedulingTable branchTable) {
		this.branchTable = branchTable;
	}

	@Override
	public void onChange(ChangeEvent event) {
		System.out.println("ON CHANGE ADV SCH POPUP");
		
		if(event.getSource().equals(lbDay)){
			if(branchTable.getDataprovider().getList().size()!=0){
				boolean branchFlag=isBranchSelected();
				if(!branchFlag){
					GWTCAlert alert=new GWTCAlert();
					alert.alert("Please select branch first!");
					lbDay.setSelectedIndex(0);
					return;
				}
				if(lbDay.getSelectedIndex()==0){
					for(BranchWiseScheduling branch:branchTable.getDataprovider().getList()){
						branch.setDay("");
					}
				}else{
					for(BranchWiseScheduling branch:branchTable.getDataprovider().getList()){
						if(branch.isCheck()==true){
							branch.setDay(lbDay.getValue(lbDay.getSelectedIndex()));
						}
					}
//					lbDay.setSelectedIndex(0);
				}
				branchTable.getTable().redraw();
			}
		}
		
	}

	private boolean isBranchSelected() {
		for(BranchWiseScheduling branch:branchTable.getDataprovider().getList()){
			if(branch.isCheck()==true){
				return true;
			}
		}
		return false;
	}
	
	/*
	 * Nidhi 20-06-2017
	 */
	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}
	
	
//	public double getNoOfSelectedBranch(){
//		int noOfBranches=0;
//		for(BranchWiseScheduling branch:branchTable.getDataprovider().getList()){
//			if(branch.isCheck()==true){
//				noOfBranches++;
//			}
//		}
//		return noOfBranches;
//	}
}
