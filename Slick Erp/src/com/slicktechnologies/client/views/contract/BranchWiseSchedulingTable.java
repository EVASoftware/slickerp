package com.slicktechnologies.client.views.contract;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.quotation.QuotationForm;
import com.slicktechnologies.client.views.quotation.QuotationPresenter;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.ComponentDetails;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.helperlayer.Config;

public class BranchWiseSchedulingTable extends SuperTable<BranchWiseScheduling> implements ClickHandler{

	TextColumn<BranchWiseScheduling> branchNameColumn;
	TextColumn<BranchWiseScheduling> dayColumn;
	Column<BranchWiseScheduling, Boolean> checkColumn;
	Header<Boolean> selectAllHeader;
	/** date 14.04.2018 added by komal for orion(area column)**/
	TextColumn<BranchWiseScheduling> areaColumn;
	/**
	 * nidhi
	 * 20-09-2018
	 *  *:*:*
	 */
//	TextColumn<BranchWiseScheduling> unitColumn;
	Column<BranchWiseScheduling, String> prodAreaColumn;
	/**
	 * end
	 */
	/**
	 * nidhi
	 * 11-09-2018 for billofmaterial
	 */
	ArrayList<String> areaUnit = new ArrayList<String>();
	Column<BranchWiseScheduling,String> unitColumn;
	
	TextColumn<BranchWiseScheduling> viewUnitColumn;
	public ContractForm form;
	/**
	 * end
	 */
	
	/**
	 * @author Anil
	 * @since 04-06-2020
	 * Adding component and asset details for ups maintaince
	 * for premium tech raised by Rahul Tiwari
	 */
	boolean tatFlag=false;
	Column<BranchWiseScheduling,String> tierNameCol;
	Column<BranchWiseScheduling,String> tatCol;
	Column<BranchWiseScheduling,String> assetQtyCol;
	Column<BranchWiseScheduling,String> componentDetailCol;
	ArrayList<Config> tierNameList=new ArrayList<Config>();
	ArrayList<String> tierList=new ArrayList<String>();
	ComponentDetailPopup componentPopup=null;
	PopupPanel panel=new PopupPanel();
	int rowIndex=0;
	
	/**
	 * @author Vijay Chougule Date 09-09-2020
	 * Des :- Adding Service duration with process config for LifeLine
	 */
	Column<BranchWiseScheduling, String> serviceDurationCol;
	
	Column<BranchWiseScheduling, String> serviceAmount;


	public BranchWiseSchedulingTable() {
		// TODO Auto-generated constructor stub
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ComplainServiceWithTurnAroundTime")){
			tierNameList=new ArrayList<Config>();
			for(Config obj:LoginPresenter.globalConfig){
				if(obj.getType()==117&&obj.isStatus()==true){
					tierNameList.add(obj);
				}
			}
			componentPopup=new ComponentDetailPopup();
			componentPopup.getBtnOk().addClickHandler(this);
			componentPopup.getBtnCancel().addClickHandler(this);
		}
	}
	
	@Override
	public void createTable() {
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ComplainServiceWithTurnAroundTime")){
			tatFlag=true;
			tierList=new ArrayList<String>();
			tierList.add("NA");
			for(Config obj:LoginPresenter.globalConfig){
				if(obj.getType()==117&&obj.isStatus()==true){
					tierList.add(obj.getName());
				}
			}
		}
		addCheckBoxCellColumn();
		addBranchColumn();
		addAmountColumn();
		addDayColumn();
		/**
		 * @author Vijay Chougule Date 09-09-2020
		 * Des :- Adding Service duration with process config for LifeLine
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.CONTRACT, AppConstants.PC_ADDSERVICEDURATIONCOLUMN)){
			addServiceDurationColumn();
		}
		
		if(!tatFlag){
			/** date 14.04.2018 added by komal for orion(area column)**/
			addAreaColumn();
			/**
			 * nidhi
			 *  20-09-2018
			 *   *:*:*
			 */
			if(areaUnit != null && areaUnit.size()>0)
				areaUnit.clear();
			else{
				areaUnit = new ArrayList<String>();
			}
			
			for(Config cfg : LoginPresenter.unitForService){
				areaUnit.add(cfg.getName());
			}
			addEditColumnAreaUnit();
			updateAreaUOMColumn();
			/**
			 * end
			 */
		}

		if(tatFlag){
			addTierNameCol();
			addTatCol();
			addAssetQtyCol();
			addComponentDetailCol();
		}

		
		addColumnSorting();

	}
	
	private void addAmountColumn() {
		EditTextCell editcell = new EditTextCell();
		serviceAmount = new Column<BranchWiseScheduling, String>(editcell) {
			
			@Override
			public String getValue(BranchWiseScheduling object) {
				return object.getAmount()+"";
			}
		};
		
		table.addColumn(serviceAmount,"#Service Amount");
		table.setColumnWidth(serviceAmount, 90, Unit.PX);
		
		serviceAmount.setFieldUpdater(new FieldUpdater<BranchWiseScheduling, String>() {
			
			@Override
			public void update(int index, BranchWiseScheduling object, String value) {

				try {
					if(value!=null && value.trim().matches("[0-9.]+"))
						object.setAmount(Double.parseDouble(value));

					else
						object.setAmount(0);
					
				} catch (Exception e) {
					GWTCAlert alert  = new GWTCAlert();
					alert.alert("Numeric value allowed only!");
				}
				
				table.redrawRow(index);
			}
		});
	}

	private void addServiceDurationColumn() {
		EditTextCell editcell = new EditTextCell();
		serviceDurationCol = new Column<BranchWiseScheduling, String>(editcell) {
			
			@Override
			public String getValue(BranchWiseScheduling object) {
					if(object.getServiceDuration()!=0){
						return object.getServiceDuration()+"";
					}
					else{
						return "";
					}
			}
		};
		
		table.addColumn(serviceDurationCol,"#Service Duration");
		table.setColumnWidth(serviceDurationCol, 90, Unit.PX);
		
		serviceDurationCol.setFieldUpdater(new FieldUpdater<BranchWiseScheduling, String>() {
			
			@Override
			public void update(int index, BranchWiseScheduling object, String value) {

				try {
					if(value!=null && value.trim().matches("[0-9.]+"))
						object.setServiceDuration(Double.parseDouble(value));

					else{
						object.setServiceDuration(0);
					}
					
				} catch (Exception e) {
				}
				
				table.redrawRow(index);
			}
		});
	}

	private void addComponentDetailCol() {
		// TODO Auto-generated method stub
		ButtonCell editCell=new ButtonCell();
		componentDetailCol = new Column<BranchWiseScheduling, String>(editCell) {
			@Override
			public String getValue(BranchWiseScheduling object) {
				return "Component Details";
			}
			@Override
			public void render(Context context,BranchWiseScheduling object,SafeHtmlBuilder sb) {
				if(object.isCheck()&&object.getAssetQty()>0){
					super.render(context, object, sb);
				}
			}
			
		};
		table.addColumn(componentDetailCol,"");
		table.setColumnWidth(componentDetailCol, 120, Unit.PX);
		
		
		
		componentDetailCol.setFieldUpdater(new FieldUpdater<BranchWiseScheduling, String>(){
			@Override
			public void update(int index, BranchWiseScheduling object,String value) {
				Console.log("Comp Updator called .. ");
				ArrayList<ComponentDetails> compList=null;
				if(object.getComponentList()!=null&&object.getComponentList().size()!=0){
					if(isAnyModificationInCompoenetList(object)){
						compList=getcomponentDetailsList(object);
					}else{
						compList=object.getComponentList();
					}
				}else{
					compList=getcomponentDetailsList(object);
				}
				panel=new PopupPanel(true);
				componentPopup.clear();
				componentPopup.getComponentTable().getDataprovider().setList(compList);
				panel.add(componentPopup);
				panel.center();
				panel.show();
				rowIndex=index;
				table.redrawRow(index);
			}
		});
	}

	protected boolean isAnyModificationInCompoenetList(BranchWiseScheduling object) {
		// TODO Auto-generated method stub
		if(object.getAssetQty()!=object.getComponentList().size()){
			return true;
		}
		return false;
	}

	protected ArrayList<ComponentDetails> getcomponentDetailsList(BranchWiseScheduling object) {
		// TODO Auto-generated method stub
		ArrayList<ComponentDetails> list=new ArrayList<ComponentDetails>();
		if(object!=null){
			for(int i=0;i<object.getAssetQty();i++){
				ComponentDetails obj=new ComponentDetails();
				obj.setComponentName(object.getComponentName());
				obj.setDurationForReplacement(object.getDurationForReplacement());
				obj.setSrNo(i);
				list.add(obj);
			}
		}
		return list;
	}

	public String getTatDetails(String tierName){
		Console.log("tierName : "+tierName +" "+tierNameList.size());
		if(tierNameList!=null&&tierNameList.size()!=0){
			for(Config obj:tierNameList){
				if(obj.getName().equals(tierName)){
					if(obj.getDescription()!=null&&!obj.getDescription().equals("")){
						Console.log("TAT : "+obj.getDescription());
						return obj.getDescription();
					}
				}
			}
		}
		return null;
	}
	

	private void addAssetQtyCol() {
		// TODO Auto-generated method stub
		EditTextCell editCell=new EditTextCell();
		assetQtyCol = new Column<BranchWiseScheduling, String>(editCell) {
			
			@Override
			public String getValue(BranchWiseScheduling object) {
//				setCellStyleNames("disableTextBox");
				return object.getAssetQty()+"";
			}
//			@Override
//			public void onBrowserEvent(Context context, Element elem,BranchWiseScheduling object, NativeEvent event) {
//				super.onBrowserEvent(context, elem, object, event);
//			}
			
			@Override
			public void render(Context context,BranchWiseScheduling object,SafeHtmlBuilder sb) {
				if(object.isCheck()){
					super.render(context, object, sb);
				}
			}
			
		};
		table.addColumn(assetQtyCol," Qty.");
		table.setColumnWidth(assetQtyCol, 90, Unit.PX);
		
		assetQtyCol.setFieldUpdater(new FieldUpdater<BranchWiseScheduling, String>(){
			@Override
			public void update(int index, BranchWiseScheduling object,String value) {
				try{
					System.out.println("value"+value);
					if(value!=null && value.trim().matches("[0-9.]+"))
						object.setAssetQty(Integer.parseInt(value.trim()));
					else{
						object.setAssetQty(0);
					}
					if(object.getAssetQty()==0){
						if(object.getComponentList()!=null&&object.getComponentList().size()!=0){
							object.getComponentList().clear();
						}
					}
				}
				catch (Exception e)
				{

				}
				table.redrawRow(index);
			}
		});
	}


	private void addTatCol() {
		// TODO Auto-generated method stub
		EditTextCell editCell=new EditTextCell();
		tatCol= new Column<BranchWiseScheduling, String>(editCell) {
			@Override
			public String getValue(BranchWiseScheduling object) {
				if(object.getTat()!=null&&!object.getTat().equals("")){
					return object.getTat();
				}
				return "";
			}
			@Override
			public void render(Context context,BranchWiseScheduling object,SafeHtmlBuilder sb) {
				if(object.isCheck()){
					super.render(context, object, sb);
				}
			}
//			@Override
//			public void onBrowserEvent(Context context, Element elem,BranchWiseScheduling object, NativeEvent event) {
//				super.onBrowserEvent(context, elem, object, event);
//			}
		};
		table.addColumn(tatCol,"#TAT");
		table.setColumnWidth(tatCol, 90, Unit.PX);
		
		tatCol.setFieldUpdater(new FieldUpdater<BranchWiseScheduling, String>(){
			@Override
			public void update(int index, BranchWiseScheduling object,String value) {
				DateTimeFormat fmt= DateTimeFormat.getFormat("HH:mm");
				GWTCAlert alert=new GWTCAlert();
				try{
					System.out.println("value"+value);
					if(value!=null&&!value.equals("")){
						fmt.parse(value);
					}else{
						object.setTat(null);
					}
				}catch (Exception e){
					alert.alert("Tat should be in HH:mm format only.");
				}
				table.redrawRow(index);
			}
		});
	}


	private void addTierNameCol() {
		// TODO Auto-generated method stub
		SelectionCell employeeselection= new SelectionCell(tierList);
		tierNameCol = new Column<BranchWiseScheduling, String>(employeeselection) {
			@Override
			public String getValue(BranchWiseScheduling object) {
				if (object.getTierName()!= null&&!object.getTierName().equals("")) {
					return object.getTierName();
				} else{
					return "NA";
				}
			}
			@Override
			public void render(Context context,BranchWiseScheduling object,SafeHtmlBuilder sb) {
				if(object.isCheck()){
					super.render(context, object, sb);
				}
			}
//			@Override
//			public void onBrowserEvent(Context context, Element elem,BranchWiseScheduling object, NativeEvent event) {
//				super.onBrowserEvent(context, elem, object, event);
//			}
		};
		table.addColumn(tierNameCol, "#Tier Name");
		table.setColumnWidth(tierNameCol, 100,Unit.PX);
		
		tierNameCol.setFieldUpdater(new FieldUpdater<BranchWiseScheduling, String>() {
			@Override
			public void update(int index, BranchWiseScheduling object, String value) {
				try {
					Console.log("Tier Updator called .. "+value);
					String val1 = (value.trim());
					object.setTierName(val1);
					object.setTat(getTatDetails(object.getTierName()));
					
				} catch (Exception e) {
				}
				table.redrawRow(index);
			}
		});
	}


	private void addCheckBoxCellColumn() {
		checkColumn=new Column<BranchWiseScheduling, Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue(BranchWiseScheduling object) {
				return object.isCheck();
			}
		};
		
		checkColumn.setFieldUpdater(new FieldUpdater<BranchWiseScheduling, Boolean>() {
			@Override
			public void update(int index, BranchWiseScheduling object, Boolean value) {
				object.setCheck(value);
				if(value==false){
					if(!object.getDay().equals("")){
						object.setDay("");
					}
					/**
					 * @author Anil
					 * @since 06-06-2020
					 */
					object.setAssetQty(0);
					if(object.getComponentList()!=null&&object.getComponentList().size()!=0){
						object.getComponentList().clear();
					}
					if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT && !QuotationForm.addnewproductFlag){
						QuotationPresenter.updateAddressFlag = true;
					}


				}else{
					if(ContractForm.complainTatFlag){
						object.setAssetQty(1);
					}
					if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT && !QuotationForm.addnewproductFlag){
						QuotationPresenter.updateAddressFlag = true;
					}
				}
				table.redrawRow(index);
				selectAllHeader.getValue();
				table.redrawHeaders();
			}
		});
		
		
		selectAllHeader =new Header<Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue() {
				if(getDataprovider().getList().size()!=0){
					
				}
				return false;
			}
		};
		
		
		selectAllHeader.setUpdater(new ValueUpdater<Boolean>() {
			@Override
			public void update(Boolean value) {
				List<BranchWiseScheduling> list=getDataprovider().getList();
				for(BranchWiseScheduling object:list){
					object.setCheck(value);
					if(value==false){
						if(!object.getDay().equals("")){
							object.setDay("");
						}
						/**
						 * @author Anil
						 * @since 06-06-2020
						 */
						object.setAssetQty(0);
						if(object.getComponentList()!=null&&object.getComponentList().size()!=0){
							object.getComponentList().clear();
						}
					}else{
						if(ContractForm.complainTatFlag){
							object.setAssetQty(1);
						}
					}
				}
				getDataprovider().setList(list);
				table.redraw();
				addColumnSorting();
			}
		});
		table.addColumn(checkColumn,selectAllHeader);
		table.setColumnWidth(checkColumn, 60, Unit.PX);
	}
	
	private void addDayColumn() {
		dayColumn=new TextColumn<BranchWiseScheduling>() {
			@Override
			public String getValue(BranchWiseScheduling object) {
				if(object.getDay()!=null){
					return object.getDay();
				}
				return "";
			}
		};
		table.addColumn(dayColumn,"Day");
		table.setColumnWidth(dayColumn, 90, Unit.PX);
	}
	
	private void addBranchColumn() {
		branchNameColumn=new TextColumn<BranchWiseScheduling>() {
			@Override
			public String getValue(BranchWiseScheduling object) {
				if(object.getBranchName()!=null){
					return object.getBranchName();
				}
				return "";
			}
		};
		table.addColumn(branchNameColumn,"Branch");
		table.setColumnWidth(branchNameColumn,250, Unit.PX);
	}
	
	/** date 14.04.2018 added by komal for orion(area column)**/
	private void addAreaColumn(){
		EditTextCell editCell=new EditTextCell();
		
		prodAreaColumn = new Column<BranchWiseScheduling, String>(editCell) {
			
			@Override
			public String getValue(BranchWiseScheduling object) {
				
				setCellStyleNames("disableTextBox");
					return object.getArea()+"";
				
			}
			@Override
			public void onBrowserEvent(Context context, Element elem,
					BranchWiseScheduling object, NativeEvent event) {
				// TODO Auto-generated method stub
//				if(object.getBranchName().equals("Service Address")){
					super.onBrowserEvent(context, elem, object, event);
//				}
				
			}
			
		};
		table.addColumn(prodAreaColumn,"Area");
		table.setColumnWidth(prodAreaColumn, 90, Unit.PX);
		prodAreaColumn.setSortable(true);
		
		prodAreaColumn.setFieldUpdater(new FieldUpdater<BranchWiseScheduling, String>(){

			@Override
			public void update(int index, BranchWiseScheduling object,
					String value) {
				// TODO Auto-generated method stub

				try{
					System.out.println("value"+value);
					if(value!=null && value.trim().matches("[0-9.]+"))
						object.setArea(Double.parseDouble(value.trim()));
					else{
						object.setArea(0.0);
					}
				}
				catch (Exception e)
				{

				}
				table.redrawRow(index);
			
			}
			
		});
		
		/*
		areaColumn=new TextColumn<BranchWiseScheduling>() {
			@Override
			public String getValue(BranchWiseScheduling object) {
				if(object.getArea()!=0){
					return object.getArea()+"";
				}
				return "";
			}
		};
		table.addColumn(areaColumn,"Area");
		table.setColumnWidth(areaColumn, 90, Unit.PX);
		areaColumn.setSortable(true);*/
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void addColumnSorting() {
		// TODO Auto-generated method stub
		addAreaSorting();
	}

	public void addAreaSorting() {
		List<BranchWiseScheduling> list = getDataprovider().getList();
		columnSort = new ListHandler<BranchWiseScheduling>(list);
		columnSort.setComparator(areaColumn, new Comparator<BranchWiseScheduling>() {
			@Override
			public int compare(BranchWiseScheduling e1, BranchWiseScheduling e2) {
				if (e1 != null && e2 != null) {
					if (e1.getArea() == e2.getArea()) {
						return 0;
					}
					if (e1.getArea() > e2.getArea()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}


	public void addEditColumnAreaUnit(){
		SelectionCell employeeselection= new SelectionCell(areaUnit);
		unitColumn = new Column<BranchWiseScheduling, String>(employeeselection) {
			@Override
			public String getValue(BranchWiseScheduling object) {
				if (object.getUnitOfMeasurement()!= null
						&&!object.getUnitOfMeasurement().equals("")) {
					
					object.setSetUOMEditValue(object.getUnitOfMeasurement());

					return object.getUnitOfMeasurement();
				} else
					return "NA";
				
				
			}
		};
		table.addColumn(unitColumn, "# UOM");
		table.setColumnWidth(unitColumn,130,Unit.PX);
		table.setColumnWidth(unitColumn, 100,Unit.PX);
		
	}
	
	public void updateAreaUOMColumn()
	{

		unitColumn.setFieldUpdater(new FieldUpdater<BranchWiseScheduling, String>() {
			
			@Override
			public void update(int index, BranchWiseScheduling object, String value) {
				try {
					String val1 = (value.trim());
					object.setSetUOMEditValue(val1);
//					object.setUnitOfMeasurement(val1);
					object.setUnitOfMeasurement(val1);
					
//					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				} catch (Exception e) {
				}
				table.redrawRow(index);
			}
		});
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
		if(event.getSource().equals(componentPopup.getBtnOk())){
			
			if(componentPopup.getComponentTable().getValue()!=null){
				getDataprovider().getList().get(rowIndex).setComponentList(componentPopup.getComponentTable().getValue());
			}
			
			panel.hide();
		}
		if(event.getSource().equals(componentPopup.getBtnCancel())){
			panel.hide();
		}
		
	}

}
