package com.slicktechnologies.client.views.contract;

import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.ContractPresenter.ContractPresenterSearch;
import com.slicktechnologies.client.views.project.concproject.ProjectPresenter;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class ContractPresenterSearchProxy extends ContractPresenterSearch {
	public PersonInfoComposite personInfo;
	public IntegerBox tbContractId;
	public IntegerBox tbQuotationId;
	public IntegerBox tbLeadId;
	public ObjectListBox<Branch> olbBranch;
	public static ObjectListBox<Employee> olbEmployee;
	public DateComparator dateComparator;

	public ObjectListBox<Config> olbContractGroup;
	public ObjectListBox<ConfigCategory> olbContractCategory;
	public ObjectListBox<Type> olbContractType;
	public ListBox olbContractStatus;
	public ObjectListBox<Config> olbContractPriority;
	
	public TextBox tbRefNumber;
	public IntegerBox tbTicketNumber;
	
	/**
	 * rohan added created by 
	 */
	
	public ObjectListBox<Employee> olbCreatedBy;
	
	/**
	 * 
	 */
	
	/**
	 * Date :24-01-2017 by anil
	 * 
	 */
	public TextBox tbRefNum2;
	/**
	 * End 
	 */	
	
	/**
	 * Added by Rahul Verma for NBHC Clients
	 */
	public TextBox tbWarehouse,tbStorageLocation,tbStackNo;
	
	
	/*
	 * nidhi 30-06-2017 customer category search
	 * These fields are used to search contract by segment
	 * For NBHC we will show customer category as segment because they copy customer category as segment in contract
	 */
	public ObjectListBox<ConfigCategory> olbCustomerCategory;
	public TextBox tbSegment;

	/*
	 * end
	 */
	
	//****************Date 04/09/2017 by jayshree add the production composite in search popup****************
	public ProductInfoComposite prodInfoComposite;
	
		/**02-10-2017 sagar sore [To search by number range(billing and non billing) in contract search popup]**/
	public ObjectListBox<Config> olbcNumberRange;
	
	/**
	 * Date : 24-10-2017 BY ANIL
	 * Adding search on contract start date and contract end date
	 * 
	 */
		
	public DateComparator startDateComparator;
	public DateComparator endDateComparator;
	/**
	 *End 
	 * 
	 */
	
	/**
	 * Date 23-01-2018 By vijay
	 * for contract period for pcamb
	 */
	public ObjectListBox<Config> olbcontractPeriod;
	
	/**
	 * Date 19-03-2019 by Vijay
	 * Des :- Do not renewal search fields
	 */
	 public ObjectListBox<String> olbNonRenewal;

	 /***
	  * Date 03-04-2019 by Vijay
	  * Des :- Cancellation contract to search
	  */
	 public DateComparator cancellationDateComparator;	
	 
	 /**
	 * @author Anil
	 * @since 11-06-2020
	 * This flag is used to minimize the no. of contract fields for user
	 */
	public boolean pedioContractFlag=false;
	
	/**
	 * @author Anil
	 * @since 18-02-2022
	 * adding contract date fiter
	 */
	
	public DateComparator contractDateComparator;
		
	//  rohan added this field as per ankita pest control request
	
	public Object getVarRef(String varName)
	{
		if(varName.equals("personInfo"))
			return this.personInfo;
		if(varName.equals("tbContractId"))
			return this.tbContractId;
		if(varName.equals("tbQuotationId"))
			return this.tbQuotationId;
		if(varName.equals("tbLeadId"))
			return this.tbLeadId;
		if(varName.equals("olbBranch"))
			return this.olbBranch;
		if(varName.equals("olbEmployee"))
			return this.olbEmployee;
		if(varName.equals("dateComparator"))
			return this.dateComparator;
		if(varName.equals("olbQuotationGroup"))
			return this.olbContractGroup;
		if(varName.equals("olbQuotationCategory"))
			return this.olbContractCategory;
		if(varName.equals("olbQuotationType"))
			return this.olbContractType;
		if(varName.equals("olbQuotationStatus"))
			return this.olbContractStatus;
		if(varName.equals("olbQuotationPriority"))
			return this.olbContractPriority;
		
		/**
		 * nidhi 30-06-2017 tbSegment
		 */
		if (varName.equals("olbCustomerCategory"))
			return this.olbCustomerCategory;
		if (varName.equals("tbSegment"))
			return this.tbSegment; 
		/**
		 * End
		 */
		return null ;
	}
	
	public ContractPresenterSearchProxy()
	{
		super(FormStyle.ROWFORM);
		createGui();
	}
	public ContractPresenterSearchProxy(boolean pedioContractFlag) {
		super(FormStyle.DEFAULT);
		this.pedioContractFlag=pedioContractFlag;
		createScreen();
		createGui();
	}

	public void initWidget()
	{
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		tbContractId= new IntegerBox();
		tbQuotationId= new IntegerBox();
		tbLeadId= new IntegerBox();
		tbTicketNumber=new IntegerBox();
		olbBranch= new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
	
//		if(ProjectPresenter.checkForProcessConfigurartionIsActiveOrNot("Contract","SalesPersonRestriction"))
//		{
//			
//			olbEmployee=new ObjectListBox<Employee>();
//			AppUtility.makeSalesPersonListBoxLive(olbEmployee);
//		
//			Timer t = new Timer() {
//			
//			@Override
//			public void run() {
//
//				makeSalesPersonEnable();
//			}
//
//			
//		};t.schedule(3000);
//	}
//	else
//	{
		olbEmployee= new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbEmployee);
		
		/**
		 * Date 19-01-2019 By Vijay
		 * Des :- For Zonal Head Login in search pop who employee are reporting to him they all are show in Sales person
		 * Drop Down including all branches
		 * Requirement :- NBHC CCPM
		 */
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "EnableSalesPersonAsPerReportingTo")
				&& LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Zonal Head")){
				olbEmployee.makeEmployeeLiveAsperReportingTo(LoginPresenter.loggedInUser);
		}else{
			olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CONTRACT, "Sales Person");
		}
		
//	}
		
		
		
		dateComparator= new DateComparator("creationDate",new Contract());
		
		olbContractGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbContractGroup, Screen.CONTRACTGROUP);
		
		olbContractCategory=new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbContractCategory, Screen.CONTRACTCATEGORY);
		
		olbContractType=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbContractType, Screen.CONTRACTTYPE);
		
		olbContractStatus=new ListBox();
		AppUtility.setStatusListBox(olbContractStatus, Contract.getStatusList());
		
		//olbContractPriority=new ObjectListBox<Config>();
		//AppUtility.MakeLiveConfig(olbContractPriority, Screen.CONTRACTPRIORITY);
//		CategoryTypeFactory.initiateOneManyFunctionality(olbContractCategory,olbContractType);
		
		personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		
		tbRefNumber =new TextBox();
		
		olbCreatedBy= new ObjectListBox<Employee>();
		AppUtility.makeSalesPersonListBoxLive(olbCreatedBy);
		
		tbRefNum2=new TextBox();
		
		
		/**
		 * Added for NBHC Field in search popup so that they can find this data.
		 */
		tbWarehouse=new TextBox();
		tbStorageLocation=new TextBox();
		tbStackNo=new TextBox();
		
		/*
		 * 	nidhi
		 *  30-06-2017
		 *  for search
		 */
		
		
		olbCustomerCategory= new ObjectListBox<ConfigCategory>();
//		AppUtility.makeTypeListBoxLive(olbCustomerCategory, Screen.CONTRACTTYPE);
		AppUtility.MakeLiveCategoryConfig(olbCustomerCategory, Screen.CUSTOMERCATEGORY);
				
		tbSegment = new TextBox();
		/* 
		 * 
		 *   end
		 */
		
		//****************Date 04/09/2017 by jayshree add the production composite in search popup**********************
		/**
		 * @author Anil
		 * @since 09-02-2022		
		 */
//		prodInfoComposite=AppUtility.initiateServiceProductComposite(new SuperProduct());
		prodInfoComposite=AppUtility.initiateServiceProductComposite(new SuperProduct(),true);
		
		/**02-10-2017 sagar sore To display values added from process configuration for Number range **/
		olbcNumberRange= new ObjectListBox<Config>();
	    AppUtility.MakeLiveConfig(olbcNumberRange,Screen.NUMBERRANGE);		
	    
	    /**
	     * Date : 24-10-2017 BY ANIL
	     */
	    startDateComparator= new DateComparator("startDate",new Contract());
	    endDateComparator= new DateComparator("endDate",new Contract());
	    /**
	     * End
	     */
	    
	    /**
		 * Date 23-01-2018 By vijay
		 * for contract period for pcamb
		 */
		
		olbcontractPeriod = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcontractPeriod, Screen.CONTRACTPERIOD);
		
		/**
		 * ends here
		 */
		
		/**
		 * Date 19-03-2019 by Vijay For NBHC CCPM Non Renewal contracts search filter
		 */
		olbNonRenewal = new ObjectListBox<String>();
		olbNonRenewal.addItem("--SELECT--");
		olbNonRenewal.addItem("Yes");
		olbNonRenewal.addItem("No");
		
		/*** Date 03-04-2019 by Vijay for cancelled contract search filter ****/
		cancellationDateComparator = new DateComparator("cancellationDate", new Contract());
		
		contractDateComparator= new DateComparator("contractDate",new Contract());

	}
	
	/**
	 * rohan added this method for setting value to sales person and and make it non editable
	 */
	public static void makeSalesPersonEnable() {
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","SalesPersonRestriction")
				&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Sales"))
		{
		olbEmployee.setValue(LoginPresenter.loggedInUser);
		olbEmployee.setEnabled(false);
		}
	}
	
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		builder = new FormFieldBuilder("Contract Id",tbContractId);
		FormField ftbContractId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Quotation Id",tbQuotationId);
		FormField ftbQuotationId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Ticket Id",tbTicketNumber);
		FormField ftbTicketNumber= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Lead Id",tbLeadId);
		FormField ftbLeadId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Sales Person",olbEmployee);
		FormField folbEmployee= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("From Date (Creation Date)",dateComparator.getFromDate());
		FormField fdateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date (Creation Date)",dateComparator.getToDate());
		FormField fdateComparator1= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		builder = new FormFieldBuilder("Contract Group",olbContractGroup);
		FormField folbQuotationGroup= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Contract Category",olbContractCategory);
		FormField folbQuotationCategory= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Contract Type",olbContractType);
		FormField folbQuotationType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Contract Status",olbContractStatus);
		FormField folbQuotationStatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Ref Number 1",tbRefNumber);
		FormField frefNumber= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Created By",olbCreatedBy);
		FormField folbCreatedBy= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Ref Number 2",tbRefNum2);
		FormField ftbRefNum2= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Warehouse",tbWarehouse);
		FormField fWarehouse= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Storage Location",tbStorageLocation);
		FormField fStorageLoc= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Stack No",tbStackNo);
		FormField fStackNo= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("* Note : If no filter is selected, by default today's data will be displayed on click of Go Button.",null);
		FormField fnote= builder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		/*
		 * nidhi
		 * 30-06-2017
		 * search for segment
		 * and  this field is added in 4 row , LAst col
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC")){
			builder = new FormFieldBuilder("Segment Type",olbCustomerCategory);
		}
		else{
			builder = new FormFieldBuilder("Segment Type",tbSegment);
		}
		FormField folbCustCategory= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/* 
		 * end
		 */
		
		//****************Date 04/09/2017 by jayshree add the production composite in search popup****************
				builder = new FormFieldBuilder("",prodInfoComposite);
				FormField fprodInfoComposite= builder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
				
				/**02-10-2017 sagar sore[ adding Number Range field in search pop up]**/
				FormField folbcNumberRange;	
				builder = new FormFieldBuilder("Number Range",olbcNumberRange);
				folbcNumberRange= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
				
		builder = new FormFieldBuilder("From Date (Start Date)",startDateComparator.getFromDate());
		FormField fstartDateComparatorfrom= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date (Start Date)",startDateComparator.getToDate());
		FormField fstartDateComparatorto= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("From Date (End Date)",endDateComparator.getFromDate());
		FormField fendDateComparatorfrom= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date (End Date)",endDateComparator.getToDate());
		FormField fendDateComparatorto= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date 12-01-2018 By Vijay 
		 * differentiated date combination search and other filter search
		 */
		builder = new FormFieldBuilder();
		FormField fgroupingDateFilterInformation=builder.setlabel("Date Filter Combination").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		builder = new FormFieldBuilder();
		FormField fgroupingOtherFilterInformation=builder.setlabel("Other Filter Combination").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		/**
		 * ends here
		 */
		
		/**
		 * Date : 17/2/2018 BY MANISHA
		 * Change contract period name as salesource as per sonu's requirement..!!
		 */
		FormField folbcontractPeriod;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForOrionPestSolutions")){
			builder = new FormFieldBuilder("Sale Source",olbcontractPeriod);
		     folbcontractPeriod = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}else{
			builder = new FormFieldBuilder("Contract Period",olbcontractPeriod);
		    folbcontractPeriod = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		/**
		 * End
		 **/
		
		
//		builder = new FormFieldBuilder("Contract Period",olbcontractPeriod);
//		FormField folbcontractPeriod = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date 19-03-2019 By Vijay
		 * Des :- NBHC CCPM Non Renewal contract search filter
		 */
		builder = new FormFieldBuilder("Non Renewal",olbNonRenewal);
		FormField foblNonRenewal = builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		/**
		 * Date 03-04-2019 by Vijay for Cancelled records search
		 */
		builder = new FormFieldBuilder("From Date (Cancelled Date)",cancellationDateComparator.getFromDate());
		FormField fcancellationFrmDateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date (Cancelled Date)",cancellationDateComparator.getToDate());
		FormField fcancellationToDateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		/**
		 * @author Anil
		 * @since 18-02-2022
		 * Adding four grouping to crate four filter tab
		 */
		builder = new FormFieldBuilder();
		FormField fgroupingFilter1=builder.setlabel("Filter 1").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		builder = new FormFieldBuilder();
		FormField fgroupingFilter2=builder.setlabel("Filter 2").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		builder = new FormFieldBuilder();
		FormField fgroupingFilter3=builder.setlabel("Filter 3").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		builder = new FormFieldBuilder();
		FormField fgroupingFilter4=builder.setlabel("Filter 4").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();

		builder = new FormFieldBuilder("From Date (Contract Date)",contractDateComparator.getFromDate());
		FormField fContractDateComp1= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("To Date (Contract Date)",contractDateComparator.getToDate());
		FormField fContractDateComp2= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		if(pedioContractFlag){
			this.fields=new FormField[][]{
					{fgroupingDateFilterInformation},
					{fdateComparator,fdateComparator1, fstartDateComparatorfrom,fstartDateComparatorto},
					{fendDateComparatorfrom,fendDateComparatorto, folbBranch, folbQuotationStatus},
					{folbcontractPeriod},
					{fpersonInfo},
					{fnote}
			};
			return;
		}
		
		
		/**
		 * Date 12-07-2018 By Vijay
		 * Des :- for NBHC CCPM need only limited search fields and else for all standard
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC")){
			this.fields=new FormField[][]{
					{fgroupingDateFilterInformation},
					{fdateComparator,fdateComparator1, fstartDateComparatorfrom,fstartDateComparatorto},
					{fendDateComparatorfrom,fendDateComparatorto, folbBranch, folbQuotationStatus},
					{fcancellationFrmDateComparator,fcancellationToDateComparator,folbQuotationGroup,foblNonRenewal},
					{fgroupingOtherFilterInformation},
					{folbQuotationCategory ,folbQuotationType,ftbTicketNumber,folbcontractPeriod},
					{ftbQuotationId,ftbLeadId,ftbContractId,folbEmployee},
					{fWarehouse,fStorageLoc,fStackNo,ftbRefNum2},
					{fpersonInfo},
					{fnote}
			};
		}
			else{
			this.fields=new FormField[][]{
					/**02-10-2017 sagar sore [folbcNumberRange added and order of fields changed]**/
					/**old code**/
	//				{fdateComparator,fdateComparator1,folbQuotationGroup,folbQuotationCategory},
	//				{folbQuotationType,folbQuotationStatus,folbEmployee,folbBranch},
	//				{ftbQuotationId,ftbLeadId,ftbContractId,ftbTicketNumber},
	//				{frefNumber,ftbRefNum2,folbCreatedBy,folbCustCategory},
	//				{fWarehouse,fStorageLoc,fStackNo},
	//				{fpersonInfo},
	//				{fprodInfoComposite},
	//				{fnote}
					/*** updated order **/
	//				{fdateComparator,fdateComparator1,folbQuotationGroup,folbQuotationCategory},
	//				{folbQuotationType,folbQuotationStatus,folbcNumberRange,folbBranch},
	//				{ftbQuotationId,ftbLeadId,ftbContractId,folbEmployee},
	//				{frefNumber,ftbRefNum2,folbCreatedBy,folbCustCategory},				
	//				{fWarehouse,fStorageLoc,fStackNo,ftbTicketNumber},
	//				{fpersonInfo},
	//				{fprodInfoComposite},
	//				{fstartDateComparatorfrom,fstartDateComparatorto,fendDateComparatorfrom,fendDateComparatorto},
	//				{fnote}
					
					/**  date 12-01-2018 by vijay above old code commented and below groping added  **/
					
//					{fgroupingDateFilterInformation},
//					{fdateComparator,fdateComparator1, fstartDateComparatorfrom,fstartDateComparatorto},
//					{fendDateComparatorfrom,fendDateComparatorto, folbBranch, folbQuotationStatus},
//					{fcancellationFrmDateComparator,fcancellationToDateComparator,folbQuotationGroup,foblNonRenewal},
//					{fgroupingOtherFilterInformation},
//					{folbQuotationCategory ,folbQuotationType,folbcNumberRange,ftbTicketNumber}, 
//					{ftbQuotationId,ftbLeadId,ftbContractId,folbEmployee},
//					{frefNumber,ftbRefNum2,folbCreatedBy,folbCustCategory},				
//					{fWarehouse,fStorageLoc,fStackNo,folbcontractPeriod},
//					{fpersonInfo},
//					{fprodInfoComposite},
//					{fnote},
					
					
					
					
					
					
					
					{fgroupingFilter1},
					{fpersonInfo},
					{folbBranch, folbQuotationStatus,fContractDateComp1,fContractDateComp2},
					{fnote},
					
					{fgroupingFilter2},
					{fdateComparator,fdateComparator1, fstartDateComparatorfrom,fstartDateComparatorto},
					{fendDateComparatorfrom,fendDateComparatorto,fcancellationFrmDateComparator,fcancellationToDateComparator },
					{folbQuotationGroup,foblNonRenewal},
					
					{fgroupingFilter3},
					{ftbContractId,ftbQuotationId,ftbLeadId,ftbTicketNumber},
					{fprodInfoComposite},
					{frefNumber,ftbRefNum2,},
					
					{fgroupingFilter4},
					{folbQuotationCategory ,folbQuotationType,folbEmployee,folbcNumberRange}, 
					{folbCustCategory,folbcontractPeriod,folbCreatedBy},				
					{fWarehouse,fStorageLoc,fStackNo,},
					
					
			};
			}
	}
	
	
	public MyQuerry getQuerry()
	{
		Console.log("in getquery of contract");
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		/**
		 * @author Ashwini Patil
		 * @since 21-04-2022
		 * if any of below filter is present then any other filter is not required
		 * Contract Id, Quotation Id, Lead Id, Ticket Id, Ref Number 1, Ref Number 2
		 */
		  if(tbQuotationId.getValue()!=null){
				temp=new Filter();
				temp.setIntValue(tbQuotationId.getValue());
				temp.setQuerryString("quotationCount");
				filtervec.add(temp);
				
				if(dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null||startDateComparator.getFromDate().getValue()!=null || startDateComparator.getToDate().getValue()!=null||endDateComparator.getFromDate().getValue()!=null || endDateComparator.getToDate().getValue()!=null||cancellationDateComparator.getFromDate().getValue()!=null || cancellationDateComparator.getToDate().getValue()!=null)
					showDialogMessage("Date filters have been ignored");	
				
				if(LoginPresenter.branchRestrictionFlag==true) //19-10-2023  as record was getting searched by id
				{
					temp=new Filter();
					temp.setList(AppUtility.getbranchlist());
					temp.setQuerryString("branch IN");
					filtervec.add(temp);
				}
				
				MyQuerry querry= new MyQuerry();
				querry.setFilters(filtervec);
				querry.setQuerryObject(new Contract());
				return querry;
			}
			if(tbLeadId.getValue()!=null){
				temp=new Filter();
				temp.setIntValue(tbLeadId.getValue());
				temp.setQuerryString("leadCount");
				filtervec.add(temp);
				
				
				if(dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null||startDateComparator.getFromDate().getValue()!=null || startDateComparator.getToDate().getValue()!=null||endDateComparator.getFromDate().getValue()!=null || endDateComparator.getToDate().getValue()!=null||cancellationDateComparator.getFromDate().getValue()!=null || cancellationDateComparator.getToDate().getValue()!=null)
					showDialogMessage("Date filters have been ignored");
				
				if(LoginPresenter.branchRestrictionFlag==true)//19-10-2023  as record was getting searched by id
				{
					temp=new Filter();
					temp.setList(AppUtility.getbranchlist());
					temp.setQuerryString("branch IN");
					filtervec.add(temp);
				}
				MyQuerry querry= new MyQuerry();
				querry.setFilters(filtervec);
				querry.setQuerryObject(new Contract());

				return querry;
			}
			if(tbContractId.getValue()!=null){
				temp=new Filter();
				temp.setIntValue(tbContractId.getValue());
				temp.setQuerryString("count");
				filtervec.add(temp);
				
				
				if(dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null||startDateComparator.getFromDate().getValue()!=null || startDateComparator.getToDate().getValue()!=null||endDateComparator.getFromDate().getValue()!=null || endDateComparator.getToDate().getValue()!=null||cancellationDateComparator.getFromDate().getValue()!=null || cancellationDateComparator.getToDate().getValue()!=null)
					showDialogMessage("Date filters have been ignored");
				
				Console.log("LoginPresenter.branchRestrictionFlag="+LoginPresenter.branchRestrictionFlag);
				if(LoginPresenter.branchRestrictionFlag==true)//19-10-2023  as record was getting searched by id
				{
					temp=new Filter();
					temp.setList(AppUtility.getbranchlist());
					temp.setQuerryString("branch IN");
					filtervec.add(temp);
				}
				MyQuerry querry= new MyQuerry();
				querry.setFilters(filtervec);
				querry.setQuerryObject(new Contract());
				
				return querry;
			}
			if(tbTicketNumber.getValue()!=null){
				temp=new Filter();
				temp.setIntValue(tbTicketNumber.getValue());
				temp.setQuerryString("ticketNumber");
				filtervec.add(temp);
				
				
				if(dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null||startDateComparator.getFromDate().getValue()!=null || startDateComparator.getToDate().getValue()!=null||endDateComparator.getFromDate().getValue()!=null || endDateComparator.getToDate().getValue()!=null||cancellationDateComparator.getFromDate().getValue()!=null || cancellationDateComparator.getToDate().getValue()!=null)
					showDialogMessage("Date filters have been ignored");
				
				if(LoginPresenter.branchRestrictionFlag==true)//19-10-2023  as record was getting searched by id
				{
					temp=new Filter();
					temp.setList(AppUtility.getbranchlist());
					temp.setQuerryString("branch IN");
					filtervec.add(temp);
				}
				MyQuerry querry= new MyQuerry();
				querry.setFilters(filtervec);
				querry.setQuerryObject(new Contract());

				return querry;
			}
			
			if(!tbRefNumber.getValue().equals("")){
				temp=new Filter();
				temp.setStringValue(tbRefNumber.getValue());
				temp.setQuerryString("refNo");
				filtervec.add(temp);
				
				
				if(dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null||startDateComparator.getFromDate().getValue()!=null || startDateComparator.getToDate().getValue()!=null||endDateComparator.getFromDate().getValue()!=null || endDateComparator.getToDate().getValue()!=null||cancellationDateComparator.getFromDate().getValue()!=null || cancellationDateComparator.getToDate().getValue()!=null)
					showDialogMessage("Date filters have been ignored");
				
				if(LoginPresenter.branchRestrictionFlag==true)//19-10-2023  as record was getting searched by id
				{
					temp=new Filter();
					temp.setList(AppUtility.getbranchlist());
					temp.setQuerryString("branch IN");
					filtervec.add(temp);
				}
				MyQuerry querry= new MyQuerry();
				querry.setFilters(filtervec);
				querry.setQuerryObject(new Contract());
				return querry;
			}
			
			if(!tbRefNum2.getValue().equals("")){
				temp=new Filter();
				temp.setStringValue(tbRefNum2.getValue());
				temp.setQuerryString("refNo2");
				filtervec.add(temp);

				
				if(dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null||startDateComparator.getFromDate().getValue()!=null || startDateComparator.getToDate().getValue()!=null||endDateComparator.getFromDate().getValue()!=null || endDateComparator.getToDate().getValue()!=null||cancellationDateComparator.getFromDate().getValue()!=null || cancellationDateComparator.getToDate().getValue()!=null)
					showDialogMessage("Date filters have been ignored");
				
				if(LoginPresenter.branchRestrictionFlag==true)//19-10-2023  as record was getting searched by id
				{
					temp=new Filter();
					temp.setList(AppUtility.getbranchlist());
					temp.setQuerryString("branch IN");
					filtervec.add(temp);
				}
				MyQuerry querry= new MyQuerry();
				querry.setFilters(filtervec);
				querry.setQuerryObject(new Contract());
				return querry;
			}
				
		
		/**
		 * @author Anil
		 * @since 18-02-2022
		 */
		if(isCustomerAndDateSelected()&&(!LoginPresenter.branchRestrictionFlag||UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin"))){
			Console.log("Contract Search Filter 1 both customer and contract date selected !!");
			if(contractDateComparator.getValue()!=null){
				filtervec.addAll(contractDateComparator.getValue());
			}
			
			if (personInfo.getIdValue() != -1) {
				temp = new Filter();
				temp.setIntValue(personInfo.getIdValue());
				temp.setQuerryString("cinfo.count");
				filtervec.add(temp);
			}
			
			MyQuerry querry= new MyQuerry();
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Contract());

			return querry;
		}
		
		if(contractDateComparator.getValue()!=null)
		{
			filtervec.addAll(contractDateComparator.getValue());
			Console.log("Status In filter added as contract date filer values are "+contractDateComparator.getfromDateValue()+" - "+contractDateComparator.gettoDateValue());
			//Index is available in combination of contract date and status. If only contract date filter is applied then below code is adding all status filter 
			if(olbContractStatus.getSelectedIndex()==0&&contractDateComparator.getfromDateValue()!=null&&contractDateComparator.gettoDateValue()!=null){
				temp=new Filter();
				temp.setList(Contract.getStatusList());
				temp.setQuerryString("status IN");
				filtervec.add(temp);
			}
			
		}
		
		
		if(dateComparator.getValue()!=null)
		{
			filtervec.addAll(dateComparator.getValue());
		}
		if(olbEmployee.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbEmployee.getValue().trim());
			temp.setQuerryString("employee");
			filtervec.add(temp);
		}
		
		if(LoginPresenter.branchRestrictionFlag==true && olbBranch.getSelectedIndex()==0)
		{
			temp=new Filter();
			temp.setList(AppUtility.getbranchlist());
			temp.setQuerryString("branch IN");
			filtervec.add(temp);
		}
		else{
			if(olbBranch.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(olbBranch.getValue().trim());
				temp.setQuerryString("branch");
				filtervec.add(temp);
			}
		}
		  
		//Ashwini Patil Date:26-08-2024 If there is change in customer name or cell number then records are not getting searched so keeping search only on contract id
	 	
		if(personInfo.getIdValue()!=-1&&personInfo.getFullNameValue()!=null&&!(personInfo.getFullNameValue().equals(""))&&personInfo.getCellValue()!=-1l)
		{
		  temp=new Filter();
		  temp.setIntValue(personInfo.getIdValue());
		  temp.setQuerryString("cinfo.count");
		  filtervec.add(temp);
		 }else {
		
			 if(personInfo.getIdValue()!=-1) {
					temp=new Filter();
					temp.setIntValue(personInfo.getIdValue());
					temp.setQuerryString("cinfo.count");
					filtervec.add(temp);
			 }
			 if(!(personInfo.getFullNameValue().equals("")))
			  {
			  temp=new Filter();
			  temp.setStringValue(personInfo.getFullNameValue());
			  temp.setQuerryString("cinfo.fullName");
			  filtervec.add(temp);
			  }
			 
			  if(personInfo.getCellValue()!=-1l)
			  {
			  temp=new Filter();
			  temp.setLongValue(personInfo.getCellValue());
			  temp.setQuerryString("cinfo.cellNumber");
			  filtervec.add(temp);
			  }
		 }
			 
		 
	
			
			if(!tbWarehouse.getValue().equals("")){
				temp=new Filter();
//				temp.setStringValue(tbWarehouse.getValue().trim().toUpperCase());
//				temp.setQuerryString("stackDetailsList.wareHouse");
				
				temp.setStringValue(tbWarehouse.getValue());
				temp.setQuerryString("warehouseName");
				
				
				filtervec.add(temp);
			}
			
			if(!tbStorageLocation.getValue().equals("")){
				temp=new Filter();
				temp.setStringValue(tbStorageLocation.getValue().trim().toUpperCase());
				temp.setQuerryString("stackDetailsList.storageLocation");
				filtervec.add(temp);
			}
			
			if(!tbStackNo.getValue().equals("")){
				temp=new Filter();
				temp.setStringValue(tbStackNo.getValue().trim().toUpperCase());
				temp.setQuerryString("stackDetailsList.stackNo");
				filtervec.add(temp);
			}
			
			
			if(olbCreatedBy.getSelectedIndex()!=0){
				temp=new Filter();temp.setStringValue(olbCreatedBy.getValue());
				temp.setQuerryString("createdBy");
				filtervec.add(temp);
			}
			
		
		if(olbContractGroup.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbContractGroup.getValue().trim());
			temp.setQuerryString("group");
			filtervec.add(temp);
		}
		
		if(olbContractCategory.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbContractCategory.getValue().trim());
			temp.setQuerryString("category");
			filtervec.add(temp);
		}
		
		if(olbContractType.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbContractType.getValue().trim());
			temp.setQuerryString("type");
			filtervec.add(temp);
		}
		
		if(olbContractStatus.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbContractStatus.getValue(olbContractStatus.getSelectedIndex()).trim());
			temp.setQuerryString("status");
			filtervec.add(temp);
		}
		
		
		/*
		 * nidhi
		 * 30-06-017
		 */
		if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","OnlyForNBHC")) {
			if (olbCustomerCategory.getSelectedIndex() != 0) {
				temp = new Filter();
				temp.setStringValue(olbCustomerCategory.getValue().trim());
				temp.setQuerryString("segment");
				filtervec.add(temp);
			}
		} else {
			if (!tbSegment.getValue().equals("")) {
				temp = new Filter();
				temp.setStringValue(tbSegment.getValue().trim());
				temp.setQuerryString("segment");
				filtervec.add(temp);
			}
		}
		/*
		 * end
		 * 
		 */
		
		 if(prodInfoComposite.getValue()!=null){
			temp=new Filter();
			temp.setIntValue(prodInfoComposite.getIdValue());
			temp.setQuerryString("items.serviceProduct.count");
			filtervec.add(temp);
		 }
		 
		 /**02-10-2017 sagar sore[ filter for search by number range i.e. Billing and Non-billing]**/
		if(olbcNumberRange.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbcNumberRange.getValue().trim());
			temp.setQuerryString("numberRange");
			filtervec.add(temp);
		}
		
		/**
		 * Date : 24-10-2017 BY ANIL
		 */
		if(startDateComparator.getValue()!=null){
			filtervec.addAll(startDateComparator.getValue());
		}
		
		if(endDateComparator.getValue()!=null){
			filtervec.addAll(endDateComparator.getValue());
		}
		/**
		 * end
		 */
		
		
		/**
		 * Date 23-01-2018 By vijay
		 * for contract period for pcamb
		 */
		if(olbcontractPeriod.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbcontractPeriod.getValue().trim());
			temp.setQuerryString("contractPeriod");
			filtervec.add(temp);
		}
		/**
		 * ends here
		 */
		
		
		
		/**
		 * Date 19-03-2019 By Vijay
		 * Des :- NBHC CCPM Non Renewal contract search filter
		 */
		if(olbNonRenewal.getSelectedIndex()!=0){
			boolean nonRenwalValue = false;
			if(olbNonRenewal.getValue(olbNonRenewal.getSelectedIndex()).trim().equalsIgnoreCase("Yes")){
				nonRenwalValue = true;
			}
			temp = new Filter();
			temp.setQuerryString("customerInterestFlag");
			temp.setBooleanvalue(nonRenwalValue);
			filtervec.add(temp);
		}

		/**
		 * Date : 03-04-2019 by Vijay for cancelled Date search filter
		 */
		if(cancellationDateComparator.getValue()!=null){
			filtervec.addAll(cancellationDateComparator.getValue());
		}
		
		
		/**
		 * Added by Rahul on 16 June 2017
		 * When we press GO directly then only today's data will get download.
		 */
		if(filtervec.size()==0){
			temp=new Filter();
			temp.setQuerryString("creationDate"+" >=");
			temp.setDateValue(new Date());
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("creationDate"+" <=");
			temp.setDateValue(new Date());
			filtervec.add(temp);
		}
		/**
		 * End
		 */

		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Contract());

		return querry;
	}

	@Override
	public boolean validate() {
		Console.log("in validate of contractpresentersearchproxy");
//		if(LoginPresenter.branchRestrictionFlag)
//		{
//			/**
//			 * Date 19-01-2019 By Vijay
//			 * Des :- NBHC CCPM when zonal head login then if he selected sales person name in the search filter
//			 * then branch restriction not required 
//			 */
//			boolean flag = true;
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "EnableSalesPersonAsPerReportingTo")
//					&& LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Zonal Head")){
//				if(olbEmployee.getSelectedIndex()!=0){
//					flag = false;
//				}
//			}
//			/**
//			 * ends here
//			 */
//			
//			if(flag && olbBranch.getSelectedIndex()==0)
//			{
//				showDialogMessage("Select Branch");
//				return false;
//			}
//		}
		
		/**
		 * Date 12-01-2018 By Vijay 
		 * for Date combination validation 
		 */
		String msg = "Wrong filter selection with From Date and To Date ! Please select proper filters with Date";
		
		
		//Commented by Ashwini Patil as new code has been written to make these filters work together
//		if(dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null){
//			if(personInfo.getIdValue()!=-1	||
//				    !(personInfo.getFullNameValue().equals("")) || personInfo.getCellValue()!=-1l ||
//				    tbQuotationId.getValue()!=null || tbLeadId.getValue()!=null || tbContractId.getValue()!=null ||olbCreatedBy.getSelectedIndex()!=0 ||
//				    tbTicketNumber.getValue()!=null ||!tbRefNumber.getValue().equals("") || !tbRefNum2.getValue().equals("") ||
//				    !tbWarehouse.getValue().equals("") || !tbStorageLocation.getValue().equals("") || 
//				    !tbStackNo.getValue().equals("") || olbCreatedBy.getSelectedIndex()!=0 ||
//				    olbContractCategory.getSelectedIndex()!=0 || olbContractType.getSelectedIndex()!=0 
//				    || olbCustomerCategory.getSelectedIndex() != 0 || !tbSegment.getValue().equals("") ){
//					showDialogMessage(msg);
//					return false;
//			}
//		}
//		if(startDateComparator.getFromDate().getValue()!=null || startDateComparator.getToDate().getValue()!=null){
//			
//			if(olbEmployee.getSelectedIndex()!=0 || personInfo.getIdValue()!=-1	||
//				    !(personInfo.getFullNameValue().equals("")) || personInfo.getCellValue()!=-1l ||
//				    tbQuotationId.getValue()!=null || tbLeadId.getValue()!=null || tbContractId.getValue()!=null ||olbCreatedBy.getSelectedIndex()!=0 ||
//				    tbTicketNumber.getValue()!=null ||!tbRefNumber.getValue().equals("") || !tbRefNum2.getValue().equals("") ||
//				    !tbWarehouse.getValue().equals("") || !tbStorageLocation.getValue().equals("") || 
//				    !tbStackNo.getValue().equals("") || olbCreatedBy.getSelectedIndex()!=0 ||
//				    olbContractCategory.getSelectedIndex()!=0 || olbContractType.getSelectedIndex()!=0 
//				    || olbCustomerCategory.getSelectedIndex() != 0 || !tbSegment.getValue().equals("") ){
//					showDialogMessage(msg);
//					return false;
//			}
//		}
//		if(endDateComparator.getFromDate().getValue()!=null || endDateComparator.getToDate().getValue()!=null){
//			if(olbEmployee.getSelectedIndex()!=0 || personInfo.getIdValue()!=-1	||
//				    !(personInfo.getFullNameValue().equals("")) || personInfo.getCellValue()!=-1l ||
//				    tbQuotationId.getValue()!=null || tbLeadId.getValue()!=null || tbContractId.getValue()!=null ||olbCreatedBy.getSelectedIndex()!=0 ||
//				    tbTicketNumber.getValue()!=null ||!tbRefNumber.getValue().equals("") || !tbRefNum2.getValue().equals("") ||
//				    !tbWarehouse.getValue().equals("") || !tbStorageLocation.getValue().equals("") || 
//				    !tbStackNo.getValue().equals("") || olbCreatedBy.getSelectedIndex()!=0 ||
//				    olbContractCategory.getSelectedIndex()!=0 || olbContractType.getSelectedIndex()!=0 
//				    || olbCustomerCategory.getSelectedIndex() != 0 || !tbSegment.getValue().equals("") ){
//					showDialogMessage(msg);
//					return false;
//			}
//		}
//		/**
//		 * ends here
//		 */
//		/*** Date 08-04-2019 by Vijay for cancellation Date filter validation msg ****/
//		if(cancellationDateComparator.getFromDate().getValue()!=null || cancellationDateComparator.getToDate().getValue()!=null){
//			if(olbEmployee.getSelectedIndex()!=0 || personInfo.getIdValue()!=-1	||
//				    !(personInfo.getFullNameValue().equals("")) || personInfo.getCellValue()!=-1l ||
//				    tbQuotationId.getValue()!=null || tbLeadId.getValue()!=null || tbContractId.getValue()!=null ||olbCreatedBy.getSelectedIndex()!=0 ||
//				    tbTicketNumber.getValue()!=null ||!tbRefNumber.getValue().equals("") || !tbRefNum2.getValue().equals("") ||
//				    !tbWarehouse.getValue().equals("") || !tbStorageLocation.getValue().equals("") || 
//				    !tbStackNo.getValue().equals("") || olbCreatedBy.getSelectedIndex()!=0 ||
//				    olbContractCategory.getSelectedIndex()!=0 || olbContractType.getSelectedIndex()!=0 
//				    || olbCustomerCategory.getSelectedIndex() != 0 || !tbSegment.getValue().equals("") ){
//					showDialogMessage(msg);
//					return false;
//			}
//		}	
		//Ashwini Patil Date:27-09-2023 
		
		if((contractDateComparator.getFromDate().getValue()!=null && contractDateComparator.getToDate().getValue()!=null)){
			int diffdays = AppUtility.getDifferenceDays(contractDateComparator.getFromDate().getValue(), contractDateComparator.getToDate().getValue());
			Console.log("diffdays "+diffdays);
			if(diffdays>31){
				showDialogMessage("Please select from date and to date range within 30 days");
				return false;
			}
		}
		if((dateComparator.getFromDate().getValue()!=null && dateComparator.getToDate().getValue()!=null)){
			int diffdays = AppUtility.getDifferenceDays(dateComparator.getFromDate().getValue(), dateComparator.getToDate().getValue());
			Console.log("diffdays "+diffdays);
			if(diffdays>31){
				showDialogMessage("Please select from date and to date range within 30 days");
				return false;
			}
		}
		if((startDateComparator.getFromDate().getValue()!=null && startDateComparator.getToDate().getValue()!=null)){
			int diffdays = AppUtility.getDifferenceDays(startDateComparator.getFromDate().getValue(), startDateComparator.getToDate().getValue());
			Console.log("diffdays "+diffdays);
			if(diffdays>31){
				showDialogMessage("Please select from date and to date range within 30 days");
				return false;
			}
		}
		if((endDateComparator.getFromDate().getValue()!=null && endDateComparator.getToDate().getValue()!=null)){
			int diffdays = AppUtility.getDifferenceDays(endDateComparator.getFromDate().getValue(), endDateComparator.getToDate().getValue());
			Console.log("diffdays "+diffdays);
			if(diffdays>31){
				showDialogMessage("Please select from date and to date range within 30 days");
				return false;
			}
		}
		return super.validate();
	}

	/**
	 * @author Anil
	 * @since 18-02-2022
	 * This method is used to clear form fields on switch of tab
	 */
	@Override
	public void refreshTableData() {
		//Ashwini Patil Date:25-03-2022 Description: Commented this code as In contract, Search filters 1, 2, 3, and 4 were not working together.
//		Console.log("Refresh Method from contract search table called!!");
//		if(isDataPresent()){
//			showDialogMessage("Previouse tab search filters will get cleared!");
//		}
//		clear();
	}
	
	public void getFilteredData(ArrayList<Contract> result) {
		Console.log("in getFilteredData result size="+result.size());
		for(Contract obj:result){
			if(olbBranch.getValue()!=null){
				if(!obj.getBranch().equals(olbBranch.getValue())){
					continue;
				}
			}
			if(olbContractStatus.getSelectedIndex()!=0){
				if(!obj.getStatus().equals(olbContractStatus.getItemText(olbContractStatus.getSelectedIndex()))){
					continue;
				}
			}
			getSupertable().getListDataProvider().getList().add(obj);
		}
		getLblCount().setText("Rows: "+getSupertable().getListDataProvider().getList().size());//Ashwini Patil Date:4-04-2023
	}
	
	public boolean isCustomerAndDateSelected(){
		if (personInfo.getIdValue() != -1 && (contractDateComparator.getFromDate().getValue()!=null||contractDateComparator.getToDate().getValue()!=null)) {
			return true;
		}
		return false;
	}
	
	
}
