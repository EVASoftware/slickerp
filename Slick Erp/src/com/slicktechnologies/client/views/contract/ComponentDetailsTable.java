package com.slicktechnologies.client.views.contract;

import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.ComponentDetails;

public class ComponentDetailsTable extends SuperTable<ComponentDetails> {

	TextColumn<ComponentDetails> componentNameCol;
	Column<ComponentDetails,String> mfgNumCol;
	Column<ComponentDetails,Date> mfgDateCol;
	TextColumn<ComponentDetails> replacementDateCol;
	Column<ComponentDetails,String> unitCol;
	
	TextColumn<ComponentDetails> assetIdCol;
	
	Column<ComponentDetails, Boolean> checkColumn;
	Header<Boolean> selectAllHeader;

	public ComponentDetailsTable() {
		super();
	}
	public ComponentDetailsTable(boolean flag) {
		super(flag);
		createTable1();
	}
	
	@Override
	public void createTable() {
		// TODO Auto-generated method stub
		addComponentNameCol();
		addMfgNumCol();
		addMfgDateCol();
		addReplacementDateCol();
		addUnitCol();
		addCheckBoxCellColumn();
	}
	
	public void createTable1() {
		// TODO Auto-generated method stub
		addAssetIdCol();
		addComponentNameCol();
		addMfgNumCol();
		addMfgDateCol();
		addReplacementDateCol();
		addUnitCol();
		addCheckBoxCellColumn();
	}

	private void addUnitCol() {
		// TODO Auto-generated method stub
		EditTextCell editCell=new EditTextCell();
		unitCol=new Column<ComponentDetails,String>(editCell) {
			@Override
			public String getValue(ComponentDetails object) {
				if(object.getAssetUnit()!=null){
					return object.getAssetUnit();
				}
				return "";
			}
		};
		table.addColumn(unitCol,"#Unit");
		table.setColumnWidth(unitCol,110, Unit.PX);
		
		unitCol.setFieldUpdater(new FieldUpdater<ComponentDetails, String>(){
			@Override
			public void update(int index, ComponentDetails object,String value) {
				try{
					System.out.println("value"+value);
					if(value!=null)
						object.setAssetUnit(value);
					else{
						object.setAssetUnit(null);
					}
				}catch (Exception e){

				}
				table.redrawRow(index);
			}
		});
	
	}

	private void addReplacementDateCol() {
		// TODO Auto-generated method stub
		replacementDateCol=new TextColumn<ComponentDetails>() {
			@Override
			public String getValue(ComponentDetails object) {
				if(object.getReplacementDate()!=null){
					return AppUtility.parseDate(object.getReplacementDate());
				}
				return "";
			}
		};
		table.addColumn(replacementDateCol,"Replacement Date");
		table.setColumnWidth(replacementDateCol,110, Unit.PX);
	}

	private void addMfgDateCol() {
		// TODO Auto-generated method stub
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date= new DatePickerCell(fmt);
		mfgDateCol=new Column<ComponentDetails,Date>(date) {
			@Override
			public Date getValue(ComponentDetails object) {
				if(object.getMfgDate()!=null){
					return object.getMfgDate();
				}else{
					if(object.isCheck()){
						object.setMfgDate(new Date());
						Date date = CalendarUtil.copyDate(object.getMfgDate());
						CalendarUtil.addDaysToDate(date, object.getDurationForReplacement());
						object.setReplacementDate(date);
						return object.getMfgDate();
					}
					else{
						return null;
					}
					
				}
			}
		};
		table.addColumn(mfgDateCol,"#Mfg. Date");
		table.setColumnWidth(mfgDateCol,110, Unit.PX);
		

		mfgDateCol.setFieldUpdater(new FieldUpdater<ComponentDetails, Date>() {
			@Override
			public void update(int index, ComponentDetails object, Date value) {
				
				if(value!=null){
					object.setMfgDate(value);
					Date date = CalendarUtil.copyDate(object.getMfgDate());
					CalendarUtil.addDaysToDate(date, object.getDurationForReplacement());
					object.setReplacementDate(date);
					table.redrawRow(index);
				}
				
			}
		});
	
	}

	private void addMfgNumCol() {
		// TODO Auto-generated method stub
		EditTextCell editCell=new EditTextCell();
		mfgNumCol=new Column<ComponentDetails,String>(editCell) {
			@Override
			public String getValue(ComponentDetails object) {
				if(object.getMfgNum()!=null){
					return object.getMfgNum();
				}
				return "";
			}
		};
		table.addColumn(mfgNumCol,"#Mfg. No.");
		table.setColumnWidth(mfgNumCol,110, Unit.PX);
		
		mfgNumCol.setFieldUpdater(new FieldUpdater<ComponentDetails, String>(){
			@Override
			public void update(int index, ComponentDetails object,String value) {
				try{
					System.out.println("value"+value);
					if(value!=null)
						object.setMfgNum(value);
					else{
						object.setMfgNum(null);
					}
				}
				catch (Exception e)
				{

				}
				table.redrawRow(index);
			}
		});
	}

	private void addComponentNameCol() {
		// TODO Auto-generated method stub
		componentNameCol=new TextColumn<ComponentDetails>() {
			@Override
			public String getValue(ComponentDetails object) {
				if(object.getComponentName()!=null){
					return object.getComponentName();
				}
				return "";
			}
		};
		table.addColumn(componentNameCol,"Component Name");
		table.setColumnWidth(componentNameCol,110, Unit.PX);
	}

	
	private void addAssetIdCol() {
		// TODO Auto-generated method stub
		assetIdCol=new TextColumn<ComponentDetails>() {
			@Override
			public String getValue(ComponentDetails object) {
				if(object.getAssetId()!=0){
					return object.getAssetId()+"";
				}
				return "";
			}
		};
		table.addColumn(assetIdCol,"Asset Id");
		table.setColumnWidth(assetIdCol,90, Unit.PX);
	}
	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	private void addCheckBoxCellColumn() {
		checkColumn = new Column<ComponentDetails, Boolean>(new CheckboxCell()) {
			
			@Override
			public Boolean getValue(ComponentDetails object) {
				// TODO Auto-generated method stub
				return object.isCheck();
				}
		};
		
		checkColumn.setFieldUpdater(new FieldUpdater<ComponentDetails, Boolean>() {
			@Override
			public void update(int index, ComponentDetails object, Boolean value) {
				object.setCheck(value);
				if(value==false){
					object.setReplacementDate(null);
					object.setMfgDate(null);
				}else{
						
				}
				table.redrawRow(index);
				table.redrawHeaders();
			}
		});
		
		selectAllHeader =new Header<Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue() {
				if(getDataprovider().getList().size()!=0){
					
				}
				return false;
			}
		};
		
		
		selectAllHeader.setUpdater(new ValueUpdater<Boolean>() {
			@Override
			public void update(Boolean value) {
				List<ComponentDetails> list=getDataprovider().getList();
				for(ComponentDetails object:list){
					object.setCheck(value);
					if(value==false){
						object.setReplacementDate(null);
						object.setMfgDate(null);
					}
				}
				getDataprovider().setList(list);
				table.redraw();
				addColumnSorting();
			}
		});
		
		table.addColumn(checkColumn,selectAllHeader);
		table.setColumnWidth(checkColumn, 60, Unit.PX);
	}

}
