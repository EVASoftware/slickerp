package com.slicktechnologies.client.views.contract;

import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;

public class ComponentDetailPopup extends AbsolutePanel  {



	ComponentDetailsTable componentTable;
	Button btnOk,btnCancel;
	
	private void initializeWidget(){
		componentTable=new ComponentDetailsTable();
		componentTable.getTable().setHeight("200px");
		btnOk=new Button("Ok");
		btnOk.getElement().getStyle().setWidth(60, Unit.PX);
		btnCancel=new Button("Cancel");
	}
	
	public ComponentDetailPopup() {
		initializeWidget();
		
		InlineLabel label=new InlineLabel("Component Details");
		label.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		label.getElement().getStyle().setFontSize(15, Unit.PX);
		
		add(label,190,15);
//		add(lbDay,100,15);
		
		FlowPanel flowPanel=new FlowPanel();
		flowPanel.add(componentTable.getTable());
		flowPanel.setWidth("600px");
		flowPanel.setHeight("160px");
//		flowPanel.setWidth("550px");
//		flowPanel.setHeight("180px");
//		flowPanel.getElement().getStyle().setBorderWidth(1, Unit.PX);
		
		add(flowPanel,10,40);
		add(btnOk,495,243);
		add(btnCancel,570,243);
//		add(btnOk,125,250);
//		add(btnCancel,250,250);
		setSize("650px","270px");
//		setSize("600px","290px");
		this.getElement().setId("login-form");
	}
	
	public void clear(){
//		componentTable.connectToLocal();
//		lbDay.setSelectedIndex(0);
	}
	
	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}

	public ComponentDetailsTable getComponentTable() {
		return componentTable;
	}

	public void setComponentTable(ComponentDetailsTable componentTable) {
		this.componentTable = componentTable;
	}
	
	
	
}
