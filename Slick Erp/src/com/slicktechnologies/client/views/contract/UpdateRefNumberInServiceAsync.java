package com.slicktechnologies.client.views.contract;

import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface UpdateRefNumberInServiceAsync {

	public void updateService(Date fromDate, Date toDate,AsyncCallback<Void>callBack);
}
