package com.slicktechnologies.client.views.contract;

import java.util.ArrayList;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.Config;

public class CancellationPopUp extends PopupScreen {
	
	CancelContractTable allContractDocumentTable;
	ArrayList<CancelContractTable> allCancelDocumentList;
    Button btnCancel,btnOk;
	public TextArea remark;								//updated by viraj for using in PO on date: 23-01-2019
	
	/*** Date 23-03-2019 by Vijay for NBHC CCPM with process config remark dropdown with configuration ****/
	ObjectListBox<Config> olbCancellationRemark;
	
	public CancellationPopUp(){
		super();
		allContractDocumentTable.connectToLocal();
		getPopup().setWidth("800px");
		//"getPopup().setHeight("500px");
		getAllContractDocumentTable().getTable().setHeight("400px");
		/** Date 03-07-2018 by vijay for popup proper width ***/
		getAllContractDocumentTable().getTable().setWidth("800px");

		createGui();
	}
	
	private void initializeWidget() {

		allCancelDocumentList = new ArrayList<CancelContractTable>();
		allContractDocumentTable = new CancelContractTable();
		remark = new TextArea();
		
		olbCancellationRemark = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbCancellationRemark, Screen.CANCELLATIONREMARK);
		olbCancellationRemark.getElement().getStyle().setWidth(200, Unit.PX);
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		
		initializeWidget();
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingContractCancel=fbuilder.setlabel("Cancellation Popup").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("", allContractDocumentTable.getTable());
		FormField fallContractTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		/*** Date 23-03-2019 by Vijay for NBHC CCPM with process config remark dropdown with configurations values ****/
		FormField olbCancellationRemark;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableCancellationRemark")){
			fbuilder = new FormFieldBuilder("Remark", this.olbCancellationRemark);
			olbCancellationRemark= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		}
		else{
			fbuilder = new FormFieldBuilder("Remark", remark);
			olbCancellationRemark= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		}
		
		FormField[][] formfield = {  
				{fgroupingContractCancel},
				{fallContractTable},
				{olbCancellationRemark}
			
		};
		this.fields=formfield;
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	public CancelContractTable getAllContractDocumentTable() {
		return allContractDocumentTable;
	}

	public void setAllContractDocumentTable(
	    CancelContractTable allContractDocumentTable) {
		this.allContractDocumentTable = allContractDocumentTable;
	}

	public TextArea getRemark() {
		return remark;
	}

	public void setRemark(TextArea remark) {
		this.remark = remark;
	}

	public ObjectListBox<Config> getOlbCancellationRemark() {
		return olbCancellationRemark;
	}

	public void setOlbCancellationRemark(ObjectListBox<Config> olbCancellationRemark) {
		this.olbCancellationRemark = olbCancellationRemark;
	}
	
	
	
}
