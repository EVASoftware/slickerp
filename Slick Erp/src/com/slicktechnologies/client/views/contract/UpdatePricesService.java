package com.slicktechnologies.client.views.contract;

import java.util.ArrayList;
import java.util.List;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.SalesLineItem;

@RemoteServiceRelativePath("updatePrices")
public interface UpdatePricesService extends RemoteService{
	ArrayList<Integer> saveAfterPriceUpdateForContract_Billing(long id,int contractId,List<SalesLineItem> listOfItems);

}
