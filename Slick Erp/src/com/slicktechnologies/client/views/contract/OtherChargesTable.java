package com.slicktechnologies.client.views.contract;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.shared.OtherTaxCharges;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;

public class OtherChargesTable extends SuperTable<OtherCharges>{

	TextColumn<OtherCharges> getOtherChargeNameCol;
	TextColumn<OtherCharges> getOtherChargeAmountCol;
	TextColumn<OtherCharges> getTax1Col;
	TextColumn<OtherCharges> getTax2Col;
	TextColumn<OtherCharges> getHsnsacCode;
	
	Column<OtherCharges,String> viewOtherChargeNameCol;
	Column<OtherCharges,String> viewOtherChargeAmountCol;
	Column<OtherCharges,String> viewTax1Col;
	Column<OtherCharges,String> viewTax2Col;
	Column<OtherCharges,String> viewHsnSacCode;
	
	Column<OtherCharges, String> delete;
	Column<OtherCharges, String> addNew;
	
	public  ArrayList<TaxDetails> taxList1;
	public  ArrayList<TaxDetails> taxList2;
	public  ArrayList<String> list1;
	public  ArrayList<String> list2;
	
	public ArrayList<String> chargesList;
	public ArrayList<OtherTaxCharges> otcLis;
	NumberFormat nf=NumberFormat.getFormat("0.00");
	
	public OtherChargesTable() {
		super();
	}
	
	@Override
	public void createTable() {
		addColumnRemoveCharge();
		retriveChargesNames();
	}

	private void retrieveTax() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		query.setFilters(filtervec);
		query.setQuerryObject(new TaxDetails());
		
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
			}
			public void onSuccess(ArrayList<SuperModel> result){
				
				list1=new ArrayList<String>();
				list2=new ArrayList<String>();
				taxList1 = new ArrayList<TaxDetails>();
				taxList2 = new ArrayList<TaxDetails>();
				List<TaxDetails> backlist=new ArrayList<TaxDetails>();
				for (SuperModel model : result) {
					TaxDetails entity = (TaxDetails) model;
					backlist.add(entity);
					taxList1.add(entity);
					taxList2.add(entity);
				}
				
				if (list1.size() == 0) {
					list1.add("SELECT");
				}
				if (list2.size() == 0) {
					list2.add("SELECT");
				}
				
				
				for(int i=0;i<backlist.size();i++){
					list1.add(backlist.get(i).getTaxChargeName());
					list2.add(backlist.get(i).getTaxChargeName());
				}
				
				viewOtherChargeNameCol();
				viewOtherChargeAmountCol();
				viewTax1Col();
				viewTax2Col();
				viewHsnSacCode();
				addColumnNewCharge();
				addFieldUpdater();
				table.setWidth("auto");
			}
		});
				
	}
	
	protected void viewHsnSacCode() {
		EditTextCell editCell = new EditTextCell();
		viewHsnSacCode = new Column<OtherCharges, String>(editCell) {
			@Override
			public String getValue(OtherCharges object) {
				if(object.getHsnSacCode()!=null){
				return object.getHsnSacCode()+"";
				}else{
					return "";
				}
			}
		};
		table.addColumn(viewHsnSacCode, "#HSN/SAC Code");
		table.setColumnWidth(viewHsnSacCode, 100,Unit.PX);
		
	}

	public void retriveChargesNames(){
		
		final GenricServiceAsync service = GWT.create(GenricService.class);
		Vector<Filter> filtervalue = new Vector<Filter>();
		MyQuerry query = new MyQuerry();
		Filter filter = null;
		Company c=new Company();
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervalue.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("otherChargeStatus");
		filter.setBooleanvalue(true);
		filtervalue.add(filter);
		
		query.setFilters(filtervalue);
		query.setQuerryObject(new OtherTaxCharges());
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {

			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				otcLis=new ArrayList<OtherTaxCharges>();
				chargesList=new ArrayList<String>();
				chargesList.clear();
				if (chargesList.size() == 0) {
					chargesList.add("SELECT");
				}
				for (SuperModel model : result) {
					OtherTaxCharges entity = (OtherTaxCharges) model;
					if (chargesList.size() >= 1) {
						chargesList.add(entity.getOtherChargeName());
					}
					otcLis.add(entity);
				}
				
				retrieveTax();
			}
		});
	}
	
	public OtherTaxCharges getOtherChargesDetails(String otherChargeName){
		if(otcLis!=null&&otcLis.size()!=0){
			for(OtherTaxCharges obj:otcLis){
				if(obj.getOtherChargeName().equals(otherChargeName)){
					return obj;
				}
			}
		}
		return null;
	}

	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		setFieldUpdaterOnRemoveCharge();
		setFieldUpdaterOnChargeName();
		setUpdaterOnTax1();
		setFieldUpdaterOnTax2();
		setFieldUpdaterOnNew();
		setFieldUpdaterOnAmountCol();
	}

	@Override
	public void setEnable(boolean state) {
		for(int i=table.getColumnCount()-1;i>-1;i--)
	    	  table.removeColumn(i); 
	      if(state==true)
	      {
	    	addColumnRemoveCharge();
	  		retriveChargesNames(); 
	      }else{
	    	 getOtherChargeNameCol();
	    	 getOtherChargeAmountCol();
	    	 getTax1Col();
	    	 getTax2Col();
	    	 getHsnSacCode();
	      }
	      table.setWidth("auto");
	}

	   private void getHsnSacCode() {
		getHsnsacCode = new TextColumn<OtherCharges>() {
			@Override
			public String getValue(OtherCharges object) {
				if(object.getHsnSacCode()!=null){
					return object.getHsnSacCode().trim();
				}
				return "";
			}
		};
		table.addColumn(getHsnsacCode, "#HSN/SAC Code");
		table.setColumnWidth(getHsnsacCode,130,Unit.PX);
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	private void addColumnRemoveCharge() {
		ButtonCell btnCell = new ButtonCell();
		delete = new Column<OtherCharges, String>(btnCell) {
			@Override
			public String getValue(OtherCharges object) {
				return "-";
			}
		};
		table.addColumn(delete, "");
		table.setColumnWidth(delete,60,Unit.PX);
	}
	private void setFieldUpdaterOnRemoveCharge() {
		delete.setFieldUpdater(new FieldUpdater<OtherCharges, String>() {
			@Override
			public void update(int index, OtherCharges object,String value) {
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
			}
		});
	}
	
	public void viewOtherChargeNameCol() {
		SelectionCell chargeselection = new SelectionCell(chargesList);
		viewOtherChargeNameCol = new Column<OtherCharges, String>(chargeselection) {

			@Override
			public String getValue(OtherCharges object) {
				if (object.getOtherChargeName() != null) {
					return object.getOtherChargeName();
				} else {
					return "";
				}
			}
		};
		table.addColumn(viewOtherChargeNameCol, "Charge");
		table.setColumnWidth(viewOtherChargeNameCol,130,Unit.PX);
	}

	public void getOtherChargeNameCol() {
		getOtherChargeNameCol = new TextColumn<OtherCharges>() {
			@Override
			public String getValue(OtherCharges object) {
				return object.getOtherChargeName().trim();
			}
		};
		table.addColumn(getOtherChargeNameCol, "Charge");
		table.setColumnWidth(getOtherChargeNameCol,130,Unit.PX);
	}

	protected void setFieldUpdaterOnChargeName() {
		viewOtherChargeNameCol.setFieldUpdater(new FieldUpdater<OtherCharges, String>() {
			@Override
			public void update(int index, OtherCharges object,String value) {
				try {
					String val1 = (value.trim());
					object.setOtherChargeName(val1);
					
					OtherTaxCharges obj=getOtherChargesDetails(object.getOtherChargeName());
					if(obj!=null){
						object.setHsnSacCode(obj.getSacCode());
					}
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
					
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}
	
	public void viewTax1Col(){
		SelectionCell employeeselection= new SelectionCell(list1);
		viewTax1Col = new Column<OtherCharges, String>(employeeselection) {
			@Override
			public String getValue(OtherCharges object) {
				if (object.getTax1().getTaxConfigName()!= null&&!object.getTax1().getTaxConfigName().equals("")) {
					return object.getTax1().getTaxConfigName();
				} else
					return "NA";
			}
		};
		table.addColumn(viewTax1Col, "# Tax 1");
		table.setColumnWidth(viewTax1Col,130,Unit.PX);
		
	}	
	
	public void setUpdaterOnTax1() {
		viewTax1Col.setFieldUpdater(new FieldUpdater<OtherCharges, String>() {
			@Override
			public void update(int index, OtherCharges object, String value) {
				try {
					String val1 = (value.trim());
					Tax tax = object.getTax1();
					boolean selectFlag=true;
					for (int i = 0; i < taxList1.size(); i++) {
						if (val1.trim().equals(taxList1.get(i).getTaxChargeName())) {
							tax.setTaxName(val1);
							tax.setPercentage(taxList1.get(i).getTaxChargePercent());
							tax.setTaxPrintName(taxList1.get(i).getTaxPrintName());
							tax.setTaxConfigName(val1);
							selectFlag=false;
							break;
						}
					}
					if(selectFlag){
						tax.setTaxPrintName(val1);	
					}
					object.setTax1(tax);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}

	public void viewTax2Col(){
		SelectionCell employeeselection= new SelectionCell(list2);
		viewTax2Col = new Column<OtherCharges, String>(employeeselection) {
			@Override
			public String getValue(OtherCharges object) {
				if (object.getTax2().getTaxConfigName()!= null&&!object.getTax2().getTaxConfigName().equals("")) {
					return object.getTax2().getTaxConfigName();
				} else
					return "NA";
			}
		};
		table.addColumn(viewTax2Col, "# Tax 2");
		table.setColumnWidth(viewTax2Col,130,Unit.PX);
		
	}	
	
	public void setFieldUpdaterOnTax2() {
		viewTax2Col.setFieldUpdater(new FieldUpdater<OtherCharges, String>() {
			@Override
			public void update(int index, OtherCharges object, String value) {
				try {
					String val1 = (value.trim());
					Tax tax = object.getTax2();
					boolean selectFlag=true;
					for (int i = 0; i < taxList2.size(); i++) {
						if (val1.trim().equals(taxList2.get(i).getTaxChargeName())) {
							tax.setTaxName(val1);
							tax.setPercentage(taxList2.get(i).getTaxChargePercent());
							tax.setTaxPrintName(taxList2.get(i).getTaxPrintName());
							tax.setTaxConfigName(val1);
							selectFlag=false;
							break;
						}
					}
					if(selectFlag){
						tax.setTaxPrintName(val1);	
					}
					object.setTax2(tax);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}

	protected void getTax1Col(){
		
		getTax1Col=new TextColumn<OtherCharges>() {
			public String getValue(OtherCharges object)
			{
				if(object.getTax1()!=null){
					return object.getTax1().getTaxConfigName();
				}else{
					return "NA";
				}
			}
		};
		table.addColumn(getTax1Col,"# Tax 1" );
		table.setColumnWidth(getTax1Col, 130,Unit.PX);
	}
	
	
	protected void getTax2Col(){
		
		getTax2Col=new TextColumn<OtherCharges>() {
			public String getValue(OtherCharges object)
			{
				if(object.getTax2()!=null){
					return object.getTax2().getTaxConfigName();
				}else{
					return "NA";
				}
			}
		};
		table.addColumn(getTax2Col,"# Tax 2" );
		table.setColumnWidth(getTax2Col, 130,Unit.PX);
	}
	
	
	
	private void addColumnNewCharge() {
		ButtonCell btnCell = new ButtonCell();
		addNew = new Column<OtherCharges, String>(btnCell) {
			@Override
			public String getValue(OtherCharges object) {
				return "+";
			}
		};
		table.addColumn(addNew, "");
		table.setColumnWidth(addNew, 60,Unit.PX);
	}

	private void setFieldUpdaterOnNew() {
		addNew.setFieldUpdater(new FieldUpdater<OtherCharges, String>() {
			@Override
			public void update(int index, OtherCharges object,String value) {
				System.out.println("Add updater called..!!");
				Tax tax1=new Tax();
				Tax tax2=new Tax();
				OtherCharges poc = new OtherCharges();
				System.out.println("Step 1");
				poc.setOtherChargeName("");
				poc.setAmount(0);
				poc.setTax1(tax1);
				poc.setTax2(tax2);
				System.out.println("Step 2");
				getDataprovider().getList().add(poc);
				System.out.println("Step 3");
				table.redrawRow(index);
				System.out.println("Step 4");
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
			}
		});
	}
	
	
	protected void viewOtherChargeAmountCol() {
		EditTextCell editCell = new EditTextCell();
		viewOtherChargeAmountCol = new Column<OtherCharges, String>(editCell) {
			@Override
			public String getValue(OtherCharges object) {
				return object.getAmount()+"";
			}
		};
		table.addColumn(viewOtherChargeAmountCol, "#Amount");
		table.setColumnWidth(viewOtherChargeAmountCol, 100,Unit.PX);
		
	}

	protected void getOtherChargeAmountCol() {
		getOtherChargeAmountCol = new TextColumn<OtherCharges>() {
			@Override
			public String getValue(OtherCharges object) {
				return object.getAmount() + "";
			}
		};
		table.addColumn(getOtherChargeAmountCol, "#Amount");
		table.setColumnWidth(getOtherChargeAmountCol, 100,Unit.PX);
	}
	
	protected void setFieldUpdaterOnAmountCol() {
		viewOtherChargeAmountCol.setFieldUpdater(new FieldUpdater<OtherCharges, String>() {
			@Override
			public void update(int index, OtherCharges object,String value) {
				try {
					Double val1 = Double.parseDouble(value.trim());
					object.setAmount(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				} catch (NumberFormatException e) {
				}
				table.redrawRow(index);
			}
		});
	}
	
	
	public double calculateOtherChargesSum(){
		double sum = 0;
		for(OtherCharges object:getDataprovider().getList()){
			sum=sum+object.getAmount();
		}
		return sum;
	}

}
