package com.slicktechnologies.client.views.contract;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.slicktechnologies.client.views.contractrenewal.RenewalResult;
import com.slicktechnologies.shared.AmcAssuredRevenueReport;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;

public interface ContractServiceAsync 
{
	public void changeStatus(Contract contract,AsyncCallback<Void>callBack);
	
	public void createServicesFromContrat(Contract contract,AsyncCallback<Boolean>callBack);
	/**
	 * rohan added this method to create services as per EVA requirement (Only For EVA)
	 * Date: 12-06-2017
	 */
	public void createServicesAsPerEVA(Contract contract,AsyncCallback<Boolean>callBack);
	
	/*
	 *  nidhi
	 *  10-07-2017
	 *  method add for service and second method for technician revenue report
	 *  
	 */
	void serviceRevanueReport(Long companyId, Date startDate, Date endDate,String branchName,AsyncCallback<String> callBack);
		
	void technicianRevanueReport(Long companyId, Date startDate, Date endDate,String branchName,AsyncCallback<String> callBack);
	/*
	 *  end
	 */
	
	/**
	 *  Date : 13-09-2017 By ANIL
	 * Following methods are added for saving contract services ,if it has more than 1000 services in it.
	 * 
	 * @param companyId
	 * @param documnetType
	 * @param documentId
	 * @param callBack
	 */
	
	void getScheduleServiceSearchResult(Long companyId,String documnetType,int documentId,AsyncCallback<ArrayList<ServiceSchedule>> callBack);
	void updateScheduleService(String operation,ArrayList<ServiceSchedule>serSchList,AsyncCallback<Void> callBack);
	void deleteSchServices(Long companyId,AsyncCallback<Void> callBack);
	/**
	 * End
	 */
	void getAmcAssuredRevenueReport(Long companyId, Date date, String branch , AsyncCallback<ArrayList<AmcAssuredRevenueReport>> callBack) ;
	
	/** Date 30-05-2018 By vijay for updating services ***/
	public void updateServices(Contract contract, AsyncCallback<Boolean>callback );
	
	public void contractRenewalList(ArrayList<Filter>filter,ArrayList<Filter>filter1,AsyncCallback<ArrayList<RenewalResult>> callback);
	public void contractRenewalReeport(ArrayList<RenewalResult> contList,AsyncCallback<ArrayList<RenewalResult>> callback );
}
