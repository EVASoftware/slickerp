package com.slicktechnologies.client.views.contract;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.BranchWiseScheduling;

public class ServiceBranchesTable extends SuperTable<BranchWiseScheduling> {

	TextColumn<BranchWiseScheduling> branchNameColumn;
	Column<BranchWiseScheduling, Boolean> checkColumn;
	Header<Boolean> selectAllHeader;
	TextColumn<BranchWiseScheduling> areaColumn;
	Column<BranchWiseScheduling, String> prodAreaColumn;
	
	@Override
	public void createTable() {
		addCheckBoxCellColumn();
		addBranchColumn();
		addAreaColumn();
		addColumnSorting();

	}
	

	private void addCheckBoxCellColumn() {
		checkColumn=new Column<BranchWiseScheduling, Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue(BranchWiseScheduling object) {
				return object.isCheck();
			}
		};
		
		checkColumn.setFieldUpdater(new FieldUpdater<BranchWiseScheduling, Boolean>() {
			@Override
			public void update(int index, BranchWiseScheduling object, Boolean value) {
				object.setCheck(value);
				if(value==false){
					if(!object.getDay().equals("")){
						object.setDay("");
					}
				}
				table.redrawRow(index);
				selectAllHeader.getValue();
				table.redrawHeaders();
			}
		});
		
		
		selectAllHeader =new Header<Boolean>(new CheckboxCell()) {
			@Override
			public Boolean getValue() {
				if(getDataprovider().getList().size()!=0){
					
				}
				return false;
			}
		};
		
		
		selectAllHeader.setUpdater(new ValueUpdater<Boolean>() {
			@Override
			public void update(Boolean value) {
				List<BranchWiseScheduling> list=getDataprovider().getList();
				for(BranchWiseScheduling object:list){
					object.setCheck(value);
					if(value==false){
						if(!object.getDay().equals("")){
							object.setDay("");
						}
					}
				}
				getDataprovider().setList(list);
				table.redraw();
				addColumnSorting();
			}
		});
		table.addColumn(checkColumn,selectAllHeader);
		table.setColumnWidth(checkColumn, 60, Unit.PX);
	}
	
	
	private void addBranchColumn() {
		branchNameColumn=new TextColumn<BranchWiseScheduling>() {
			@Override
			public String getValue(BranchWiseScheduling object) {
				if(object.getBranchName()!=null){
					return object.getBranchName();
				}
				return "";
			}
		};
		table.addColumn(branchNameColumn,"Branch");
		table.setColumnWidth(branchNameColumn,250, Unit.PX);
	}
	
	private void addAreaColumn(){
		EditTextCell editCell=new EditTextCell();
		
		prodAreaColumn = new Column<BranchWiseScheduling, String>(editCell) {
			
			@Override
			public String getValue(BranchWiseScheduling object) {
				
				//setCellStyleNames("disableTextBox");
					return object.getArea()+"";
				
			}
//			@Override
//			public void onBrowserEvent(Context context, Element elem,
//					BranchWiseScheduling object, NativeEvent event) {
//				// TODO Auto-generated method stub
//				super.onBrowserEvent(context, elem, object, event);				
//			}
			
		};
		table.addColumn(prodAreaColumn,"Amount");
		table.setColumnWidth(prodAreaColumn, 90, Unit.PX);
		prodAreaColumn.setSortable(true);
		
		prodAreaColumn.setFieldUpdater(new FieldUpdater<BranchWiseScheduling, String>(){

			@Override
			public void update(int index, BranchWiseScheduling object,
					String value) {
				// TODO Auto-generated method stub

				try{
					System.out.println("value"+value);
					if(value!=null && value.trim().matches("[0-9.]+"))
						object.setArea(Double.parseDouble(value.trim()));
					else{
						object.setArea(0.0);
					}
				}
				catch (Exception e)
				{

				}
				table.redrawRow(index);
			
			}
			
		});
		
	}

	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void addColumnSorting() {
		// TODO Auto-generated method stub
		addBranchNameSorting();
		addAreaSorting();
	}

	public void addAreaSorting() {
		List<BranchWiseScheduling> list = getDataprovider().getList();
		columnSort = new ListHandler<BranchWiseScheduling>(list);
		columnSort.setComparator(areaColumn, new Comparator<BranchWiseScheduling>() {
			@Override
			public int compare(BranchWiseScheduling e1, BranchWiseScheduling e2) {
				if (e1 != null && e2 != null) {
					if (e1.getArea() == e2.getArea()) {
						return 0;
					}
					if (e1.getArea() > e2.getArea()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	public void addBranchNameSorting() {
	List<BranchWiseScheduling> list = getDataprovider().getList();
	columnSort = new ListHandler<BranchWiseScheduling>(list);
	columnSort.setComparator(branchNameColumn, new Comparator<BranchWiseScheduling>() {

		@Override
		public int compare(BranchWiseScheduling e1, BranchWiseScheduling e2) {
		
			if (e1 != null && e2 != null) {
				if (e1.getBranchName() != null && e2.getBranchName() != null)
					return e1.getBranchName().compareTo(e2.getBranchName());
				return 0;
			} else
				return 0;
		}
	});
	table.addColumnSortHandler(columnSort);
}

}
