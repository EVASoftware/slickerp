package com.slicktechnologies.client.views.contract;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.client.views.device.StackDetailsTable;
import com.slicktechnologies.shared.common.service.StackDetails;

public class StackDetailsPopUp extends PopupScreen {

	public TextBox tbWareHouse;
	public TextBox tbStorageLocation;
	public TextBox tbStorageBin;
	public DoubleBox dbQuantity;
	public TextBox tbDescription;
	public Button addStack; 
	public StackDetailsTable stackDetailsTable; 
	
	public StackDetailsPopUp(){
		super();
//		getPopup().getElement().addClassName("setWidth");
		createGui();
	}

	private void initializewidgets() {
		tbWareHouse = new TextBox();
		tbStorageLocation = new TextBox();
		tbStorageBin = new TextBox();
		dbQuantity = new DoubleBox();
		tbDescription = new TextBox();
		addStack = new Button("Add");
		addStack.addClickHandler(this);
		stackDetailsTable = new StackDetailsTable();
	}

	
	@Override
	public void createScreen() {

		initializewidgets();
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingStackInformation=fbuilder.setlabel("Stack Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
	
		fbuilder = new FormFieldBuilder("* Warehouse Name",tbWareHouse);
		FormField ftbWareHouse= fbuilder.setMandatory(true).setMandatoryMsg("Warehouse Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Storage Location",tbStorageLocation);
		FormField ftbStorageLocation= fbuilder.setMandatory(true).setMandatoryMsg("Storage Location is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Stack No",tbStorageBin);
		FormField ftbStorageBin= fbuilder.setMandatory(true).setMandatoryMsg("Stack No is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Quantity",dbQuantity);
		FormField ftbQuantity= fbuilder.setMandatory(true).setMandatoryMsg("Quantity is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Description",tbDescription);
		FormField ftbDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",addStack);
		FormField faddStack= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",stackDetailsTable.getTable());
		FormField fstackDetailsTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();

		FormField formfields [][]= {
										{fgroupingStackInformation},
										{ftbWareHouse,ftbStorageLocation,ftbStorageBin,ftbQuantity},
										{ftbDescription,faddStack},
										{fstackDetailsTable},
				
									};
		this.fields=formfields;
	}

	
	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource()==addStack){
			if(validate()){
				reactonAddStacks();
			}
		}
		if(event.getSource().equals(this.getLblCancel())){
			this.hidePopUp();
		}
	}

	private void reactonAddStacks() {
		StackDetails stack = new StackDetails();
		stack.setWareHouse(tbWareHouse.getValue());
		stack.setStorageLocation(tbStorageLocation.getValue());
		stack.setStackNo(tbStorageBin.getValue());
		stack.setQauntity(dbQuantity.getValue());
		stack.setDescription(tbDescription.getValue());
		stackDetailsTable.getDataprovider().getList().add(stack);
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		stackDetailsTable.setEnable(state);
		tbWareHouse.setEnabled(state);
		tbStorageLocation.setEnabled(state);
		tbStorageBin.setEnabled(state);
		tbDescription.setEnabled(state);
		addStack.setEnabled(state);
	}

	public StackDetailsTable getStackDetailsTable() {
		return stackDetailsTable;
	}

	public void setStackDetailsTable(StackDetailsTable stackDetailsTable) {
		this.stackDetailsTable = stackDetailsTable;
	}
	
	
	
	
	
}
