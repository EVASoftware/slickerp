package com.slicktechnologies.client.views.contract;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.dom.client.Style.FontStyle;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.BranchWiseScheduling;

public class AdvanceSchedulingViewPopup extends AbsolutePanel implements ChangeHandler{


	BranchWiseSchedulingViewTable branchTable;
	Button btnOk,btnCancel;
	
	
	boolean complainTatFlag=false;
	
	private void initializeWidget(){

		branchTable=new BranchWiseSchedulingViewTable();
		branchTable.getTable().setHeight("200px");
		btnOk=new Button("Ok");
		btnOk.getElement().getStyle().setWidth(60, Unit.PX);
		btnCancel=new Button("Cancel");
		
		complainTatFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ComplainServiceWithTurnAroundTime");
	}
	
	public AdvanceSchedulingViewPopup() {
		initializeWidget();
		
		FlowPanel flowPanel=new FlowPanel();
		flowPanel.add(branchTable.getTable());
		
		/**
		 * @author Anil
		 * @since 08-06-2020
		 */
		if(complainTatFlag){
			branchTable.getTable().getElement().getStyle().setHeight(560, Unit.PX);
			flowPanel.setWidth("785px");
			add(btnOk,300,610);
			add(btnCancel,400,610);
		}else{
			flowPanel.setWidth("550px");
			add(btnOk,125,250);
			add(btnCancel,250,250);
		}
		flowPanel.setHeight("180px");
//		flowPanel.getElement().getStyle().setBorderWidth(1, Unit.PX);
		
		add(flowPanel,10,40);
		
//		add(btnOk,125,250);
//		add(btnCancel,250,250);
		
		/**
		 * @author Anil
		 * @since 08-06-2020
		 */
		if(complainTatFlag){
			setSize("800px","650px");
		}else{
			setSize("600px","290px");
		}
		this.getElement().setId("login-form");
	}
		
	public void clear(){
		branchTable.connectToLocal();
	}

	
	public BranchWiseSchedulingViewTable getBranchTable() {
		return branchTable;
	}

	public void setBranchTable(BranchWiseSchedulingViewTable branchTable) {
		this.branchTable = branchTable;
	}

	@Override
	public void onChange(ChangeEvent event) {
		System.out.println("ON CHANGE ADV SCH POPUP");
		
	}

	private boolean isBranchSelected() {
		for(BranchWiseScheduling branch:branchTable.getDataprovider().getList()){
			if(branch.isCheck()==true){
				return true;
			}
		}
		return false;
	}
	
	/*
	 * Nidhi 20-06-2017
	 */
	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}
	
	
//	public double getNoOfSelectedBranch(){
//		int noOfBranches=0;
//		for(BranchWiseScheduling branch:branchTable.getDataprovider().getList()){
//			if(branch.isCheck()==true){
//				noOfBranches++;
//			}
//		}
//		return noOfBranches;
//	}
}
