package com.slicktechnologies.client.views.contract;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ProcessLevelBar;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.Slick_Erp;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreenPresenter;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.AutoInvoiceService;
import com.slicktechnologies.client.services.AutoInvoiceServiceAsync;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.services.CommunicationLogServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.services.MaterialConsumptionService;
import com.slicktechnologies.client.services.MaterialConsumptionServiceAsync;
import com.slicktechnologies.client.services.RateContractServicesBillingService;
import com.slicktechnologies.client.services.RateContractServicesBillingServiceAsync;
import com.slicktechnologies.client.services.ServiceListService;
import com.slicktechnologies.client.services.ServiceListServiceAsync;
import com.slicktechnologies.client.services.SmsService;
import com.slicktechnologies.client.services.SmsServiceAsync;
import com.slicktechnologies.client.services.UpdateService;
import com.slicktechnologies.client.services.UpdateServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.ProcessLevelMenu;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utility.UnitConversionUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.accountinginterface.IntegrateSyncService;
import com.slicktechnologies.client.views.accountinginterface.IntegrateSyncServiceAsync;
import com.slicktechnologies.client.views.communicationlog.CommunicationLogPopUp;
import com.slicktechnologies.client.views.complain.ComplainForm;
import com.slicktechnologies.client.views.complain.ComplainPresenter;
import com.slicktechnologies.client.views.complain.ServiceDateBoxPopup;
import com.slicktechnologies.client.views.contractrenewal.ContractNonRenewalPopup;
import com.slicktechnologies.client.views.contractrenewal.ContractRenewalProduct;
import com.slicktechnologies.client.views.contractrenewal.ContractRenewalProductTablePouup;
import com.slicktechnologies.client.views.contractrenewal.ContractRenewalService;
import com.slicktechnologies.client.views.contractrenewal.ContractRenewalServiceAsync;
import com.slicktechnologies.client.views.contractrenewal.RenewalResult;
import com.slicktechnologies.client.views.documentcancellation.CancellationSummary;
import com.slicktechnologies.client.views.documentcancellation.CancellationSummaryPopUp;
import com.slicktechnologies.client.views.documentcancellation.CancellationSummaryPopUpTable;
import com.slicktechnologies.client.views.documentcancellation.DocumentCancellationPopUp;
import com.slicktechnologies.client.views.humanresource.allocation.payslipallocation.InformationPopup;
import com.slicktechnologies.client.views.lead.LeadForm;
import com.slicktechnologies.client.views.lead.LeadPresenter;
import com.slicktechnologies.client.views.materialconsumptionreport.MaterialConsumptionReportForm;
import com.slicktechnologies.client.views.materialconsumptionreport.MaterialConsumptionReportPresenter;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.FromAndToDateBoxPopup;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListPresenter;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.DescriptionPopup;
import com.slicktechnologies.client.views.popups.ContractDiscontinueApprovalPopup;
import com.slicktechnologies.client.views.popups.ContractResetMessagePopup;
import com.slicktechnologies.client.views.popups.MultipleSRCopyPrintPopup;
import com.slicktechnologies.client.views.popups.NewEmailPopUp;
import com.slicktechnologies.client.views.popups.SMSPopUp;
import com.slicktechnologies.client.views.popups.ServiceCancellationPopup;
import com.slicktechnologies.client.views.popups.ServicelistServiceAssignTechnicianPopup;
import com.slicktechnologies.client.views.popups.SingleDropDownPopup;
import com.slicktechnologies.client.views.project.concproject.ProjectForm;
import com.slicktechnologies.client.views.project.concproject.ProjectPresenter;
import com.slicktechnologies.client.views.project.tool.UploadAssetPopup;
import com.slicktechnologies.client.views.quickcontract.ViewInvoicePaymentServicePopUp;
import com.slicktechnologies.client.views.quotation.QuotationForm;
import com.slicktechnologies.client.views.quotation.QuotationPresenter;
import com.slicktechnologies.client.views.ratecontractproductpopup.ProductInfo;
import com.slicktechnologies.client.views.ratecontractproductpopup.ProductPopup;
import com.slicktechnologies.client.views.ratecontractproductpopup.ProductPopupForMultipleBranches;
import com.slicktechnologies.client.views.salesorder.SalesOrderForm;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter;
import com.slicktechnologies.client.views.scheduleservice.ServiceSchedulePopUp;
import com.slicktechnologies.client.views.service.ServiceForm;
import com.slicktechnologies.client.views.service.ServicePresenter;
import com.slicktechnologies.client.views.workorder.WorkOrderForm;
import com.slicktechnologies.client.views.workorder.WorkOrderPresenter;
import com.slicktechnologies.server.addhocdownload.XlsxWriter;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cancelsummary.CancelSummary;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Holiday;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.inventory.MaterialConsumptionReport;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.role.ScreenAuthorization;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.service.StackDetails;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
import com.slicktechnologies.shared.common.workorder.WorkOrder;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
/**
 * FormScreen presenter template.
 */

public class ContractPresenter extends ApprovableFormScreenPresenter<Contract>
		implements RowCountChangeEvent.Handler, SelectionHandler<Suggestion>,
		ChangeHandler, ValueChangeHandler<Integer> {

	// Token to set the concrete FormScreen class name
	public static ContractForm form;
	CsvServiceAsync csvservice = GWT.create(CsvService.class);
	EmailServiceAsync emailService = GWT.create(EmailService.class);
	final ContractServiceAsync async = GWT.create(ContractService.class);
	static List<CancellationSummary> cancellis;
	CancellationSummaryPopUp summaryPopup1 = new CancellationSummaryPopUp();
	PopupPanel summaryPanel;
	DocumentCancellationPopUp popup = new DocumentCancellationPopUp();
	PopupPanel cancelpanel;
	final GenricServiceAsync genasync = GWT.create(GenricService.class);
	SmsServiceAsync smsService = GWT.create(SmsService.class);
	final MaterialConsumptionServiceAsync consumptionAsync = GWT
			.create(MaterialConsumptionService.class);
	ArrayList<MaterialConsumptionReport> consumptionList = new ArrayList<MaterialConsumptionReport>();

	// rohan added this for ref update in service from contract
	UpdateRefNumberInServiceAsync myasync = GWT.create(UpdateRefNumberInService.class);
	
	// rohan added this code for Cancel Contract on Date : 28/2/2017  
	UpdateServiceAsync update = GWT.create(UpdateService.class);
	
	// ************changes changes here ****************
	ConditionDialogBox conditionPopup = new ConditionDialogBox(
			"Do you want to print on preprinted Stationery", AppConstants.YES,
			AppConstants.NO);
	PopupPanel panel;
	// ****************ends here ***********

	/**
	 * Date : 23-11-2017 BY ANIl
	 * initializing schedule popup with new constructor which has option to filter the list
	 */
//	ServiceSchedulePopUp serviceSchedulePopUp = new ServiceSchedulePopUp("Contract Id", "Contract Date");
	ServiceSchedulePopUp serviceSchedulePopUp = new ServiceSchedulePopUp("Contract Id", "Contract Date",true);
	/**
	 * End
	 */
	
	PopupPanel schedulePanel;
	DateTimeFormat format = DateTimeFormat.getFormat("c");
	public static int custcount = 0;
	public static int specificday = 0;

	
	/**
	 * Date 14 may 2017 addded by Rohan for communication log
	 */
	CommunicationLogPopUp CommunicationLogPopUp = new CommunicationLogPopUp();
	CommunicationLogServiceAsync communicationService = GWT.create(CommunicationLogService.class);
	/**
	 * ends here
	 */
	
	/************************* updation Code *********************/

	FromAndToDateBoxPopup frmAndToPop = new FromAndToDateBoxPopup();

	FromAndToDateBoxPopup frmAndToPopforServiceUpdate = new FromAndToDateBoxPopup();
	ArrayList<TaxDetails> vattaxlist;
	ArrayList<TaxDetails> servicetaxlist;

	// rohan added this for monthly report pdf

	ShowMonthlyPeriodPopup frmAndToPopForMonthlyReport = new ShowMonthlyPeriodPopup();
	PopupPanel serviceReportPanel;
	/*** Date 25-04-2019 by Vijay below code commented updated with new code ***/
//	/**
//	 * Created by Rahul on 10-10-2016 For NBHC Reusing
//	 * ContractRenewalProductTablePouup used in Contract Renewal PopUp
//	 */
//	StackDetailsPopUp stackDetailsPopUp = new StackDetailsPopUp();
//	ArrayList<StackDetails> stackList;
//	PopupPanel stackDetailsPanel;
	ContractRenewalProductTablePouup updatePricesPopUp = new ContractRenewalProductTablePouup();
	PopupPanel updatePricePanel;

	/*
	 * Colledction of Pop Up Products
	 */
	List<ContractRenewalProduct> listOfProductinUpdatePrices;

	/**
	 * ends here
	 */
	ViewInvoicePaymentServicePopUp invoicePaymentPopup;
	PopupPanel invoicePaymentPanel;
	
	

	// *************vaishnavi*****************
	ClientAssetPopup clientassetPopUp = new ClientAssetPopup();
	PopupPanel assetPanel;

	// vijay
	ProductPopup productpopup = new ProductPopup();
	
	/**
	 * @author Ashwini Patil
	 * @Date 5-01-2022
	 * If multiple branches are selected on branch popup then we need to do not want to allow user to edit quantity in popup.
	 * Managing this in same popup was not possible so creating another popup with non editable field
	 */
	ProductPopupForMultipleBranches productPopupForMultipleBranches=new ProductPopupForMultipleBranches();
	PopupPanel productPanel;
	final RateContractServicesBillingServiceAsync contractRateasyn = GWT
			.create(RateContractServicesBillingService.class);

	int cnt = 0;
	int pestoIndia = 0;
	int vcare = 0;
	int hygeia = 0;
	int genesis = 0;
	int universal = 0;
	int pecopp = 0;
	int ompest=0;
	
	/** Date 13-09-2017 2017 added by vijay for customer Address ******/
	CustomerAddressPopup custAddresspopup = new CustomerAddressPopup();
	InlineLabel lblUpdate = new InlineLabel("Update");
	InlineLabel lblSummary = new InlineLabel("Summary"); //Date: 17-02-2022 Added by Ashwini Patil for MultipleSRCopyPrintPopup
	InlineLabel lblChecklistReport = new InlineLabel("Cancel");////Date: 8-08-2022 Added by Ashwini Patil for MultipleSRCopyPrintPopup
	
	/**
	 * Date : 06-11-2017 By ANIL
	 */
	UploadAssetPopup uploadPopup=new UploadAssetPopup("Upload T&C");
	/**
	 * End
	 */
	
	/** date 01/11/2017 added by komal for new cancellation popup **/
	CancellationPopUp cancellationPopup = new CancellationPopUp();
	
	GeneralServiceAsync generalAsync = GWT.create(GeneralService.class);

	/**
	 * nidhi
	 *18-05-2018
	 *for contract discontinue 
	 */
	boolean contractDisContinue = false;
	/**
	 * nidhi
	 * 9-06-2018
	 * ContractNonRenewalPopup nonRenewalMsgPopup =null;
	 * @param view
	 * @param model
	 */
	ContractNonRenewalPopup nonRenewalMsgPopup = new ContractNonRenewalPopup();
	
	/**
	 * nidhi
	 *  *:*:*
	 */
	public static ArrayList<BillOfMaterial> globalProdBillOfMaterial;
	
	/**
	 * Date 22-03-2019 by Vijay for NBHC CCPM for renew contract quotation id is mandatory
	 */
	SingleDropDownPopup singledropdownpopup = new SingleDropDownPopup();
	/** Date 26-03-2019 by Vijay For NBHC CCPM contract discountinue with approval process ***/
	ContractDiscontinueApprovalPopup contractDiscountinue = null;
	
	/*** Date 25-04-2019 by Vijay for NBHC CCPM Stack details old code updated with new popup **********/
	StackDetailsPopUp stackDetailsPopUp = new StackDetailsPopUp();

	MultipleSRCopyPrintPopup multipleSRCopyPrintPopup;
	ServiceCancellationPopup  cancellation = new ServiceCancellationPopup();
	
	/**
	 * @author Anil , Date : 04-12-2019
	 */
	FromAndToDateBoxPopup datePopUp=new FromAndToDateBoxPopup(true,"Product Id");
	PopupPanel datePanel=new PopupPanel();
	
	/**
	 * @author Anil
	 * @since 29-06-2020
	 * For updating account manager after approval
	 */
	ServicelistServiceAssignTechnicianPopup accMgrPopup = new ServicelistServiceAssignTechnicianPopup();
	
	
	ContractRenewalServiceAsync conrenewalAsync = GWT.create(ContractRenewalService.class);
	
	
	ConditionDialogBox conRenewalPopup = new ConditionDialogBox(
			"Do you want to print on preprinted Stationery", AppConstants.YES,
			AppConstants.NO);
	
	NewEmailPopUp emailpopup = new NewEmailPopUp();

	CommonServiceAsync commonservice = GWT.create(CommonService.class);

	SMSPopUp smspopup = new SMSPopUp();
	boolean flag = false;
	ServicelistServiceAssignTechnicianPopup technicianPopup = new ServicelistServiceAssignTechnicianPopup();
	
	ContractResetMessagePopup contractResetPopup = new ContractResetMessagePopup();
	ConditionDialogBox contractResetconditionPopup = new ConditionDialogBox("Do you want to reset contract", AppConstants.YES,
			AppConstants.NO);
			
	List<Contract> searchedContractList;
	boolean multipleBranchesSelectedFlag=false;//Ashwini Patil Date:4-1-2023
	boolean accessPayments=false; //Ashwini Patil Date:8-06-2023 to restrict access to payment as per user authorization
	ServiceDateBoxPopup otpPopup=new ServiceDateBoxPopup("OTP"); //Ashwini Patil Date:5-07-2023 to restrict download with otp for hi tech client
	PopupPanel otppopupPanel;
	int generatedOTP;
	InformationPopup infoPopup=new InformationPopup("Unable to create service and bill",true);//Ashwini Patil Date:18-03-2024
	PopupPanel infoPanel=new PopupPanel();
	ServiceListServiceAsync serviceAsync = GWT.create(ServiceListService.class);
	PopupPanel custBranchPanel=new PopupPanel();
	ConditionDialogBox addExtraServicesconditionPopup = new ConditionDialogBox(
			"Do you want to create replica of existing services for product level customer branches?", AppConstants.YES,
			AppConstants.NO);
	DescriptionPopup descriptionPopup=new DescriptionPopup();
	
	public ContractPresenter(FormScreen<Contract> view, Contract model) {
		super(view, model);
		form = (ContractForm) view;
		form.setPresenter(this);
		form.getSaleslineitemtable().getTable().addRowCountChangeHandler(this);
		form.getChargesTable().getTable().addRowCountChangeHandler(this);
		form.getPersonInfoComposite().getId().addSelectionHandler(this);
		form.getPersonInfoComposite().getName().addSelectionHandler(this);
		form.getPersonInfoComposite().getPhone().addSelectionHandler(this);
		popup.getBtnOk().addClickHandler(this);
		popup.getBtnCancel().addClickHandler(this);
		summaryPopup1.getBtnOk().addClickHandler(this);
		summaryPopup1.getBtnDownload().addClickHandler(this);

		// **********rohan ************************
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);

		/**
		 * Added by Rahul
		 */
		Console.log("Ashwini in contract presenter constructor");	
		List<ScreenAuthorization> moduleValidation = null;
		if(UserConfiguration.getRole()!=null){
			moduleValidation=UserConfiguration.getRole().getAuthorization();
			Console.log("Ashwini user role="+UserConfiguration.getRole());	
		}
		for(int f=0;f<moduleValidation.size();f++)
		{
//			Console.log("Ashwini module screen name="+moduleValidation.get(f).getScreens().trim());	
			if(moduleValidation.get(f).getScreens().trim().equals("Payment Details")||moduleValidation.get(f).getScreens().trim().equals("Payment List")||moduleValidation.get(f).getScreens().trim().equals("PAYMENTDETAILS")||moduleValidation.get(f).getScreens().trim().equals("PAYMENTLIST")){
				accessPayments=true;
				break;
			}
		}
		invoicePaymentPopup = new ViewInvoicePaymentServicePopUp(accessPayments);
		invoicePaymentPopup.btnClose.addClickHandler(this);
		form.btstackDetails.addClickHandler(this);
		form.btstackDetails.setEnabled(true);
		/*** Date 25-04-2019 by Vijay updated code with so below old code commented **/
//		stackDetailsPopUp.getBtnCancel().addClickHandler(this);
//		stackDetailsPopUp.getBtnOk().addClickHandler(this);
//		stackDetailsPopUp.addStack.addClickHandler(this);
		updatePricesPopUp.getBtnCancel().addClickHandler(this);
		updatePricesPopUp.getBtnOk().addClickHandler(this);
		/**
		 * Ends
		 */

		/**
		 * rohan added this code 
		 */
		
		CommunicationLogPopUp.getBtnOk().addClickHandler(this);
		CommunicationLogPopUp.getBtnCancel().addClickHandler(this);
		CommunicationLogPopUp.getBtnAdd().addClickHandler(this);
		
		/**
		 * ends here 
		 */
		
		/******************************* Service Schedule Logic ********************************/
		form.f_btnservice.addClickHandler(this);
		form.f_btnservice.setEnabled(true);
		serviceSchedulePopUp.getBtnOk().addClickHandler(this);
		serviceSchedulePopUp.getBtnCancel().addClickHandler(this);
		serviceSchedulePopUp.getBtnReset().addClickHandler(this);

		serviceSchedulePopUp.getP_default().addClickHandler(this);
		serviceSchedulePopUp.getP_customise().addClickHandler(this);

		serviceSchedulePopUp.getP_mondaytofriday().addChangeHandler(this);
		serviceSchedulePopUp.getP_interval().addValueChangeHandler(this);

		serviceSchedulePopUp.getP_servicehours().addChangeHandler(this);
		serviceSchedulePopUp.getP_servicemin().addChangeHandler(this);
		serviceSchedulePopUp.getP_ampm().addChangeHandler(this);

		clientassetPopUp.getBtnOk().addClickHandler(this);
		clientassetPopUp.getBtnCancel().addClickHandler(this);
		// form.btnclientAsset.addClickHandler(this);
		// form.btnclientAsset.setEnabled(true);
		/*************************************************************************************/

		frmAndToPop.btnOne.addClickHandler(this);
		frmAndToPop.btnTwo.addClickHandler(this);

		frmAndToPopforServiceUpdate.btnOne.addClickHandler(this);
		frmAndToPopforServiceUpdate.btnTwo.addClickHandler(this);

		frmAndToPopForMonthlyReport.btnOne.addClickHandler(this);
		frmAndToPopForMonthlyReport.btnTwo.addClickHandler(this);

		productpopup.getBtnOk().addClickHandler(this);
		productpopup.getBtnCancel().addClickHandler(this);
		
		productPopupForMultipleBranches.getBtnOk().addClickHandler(this);
		productPopupForMultipleBranches.getBtnCancel().addClickHandler(this);
		

		form.chkservicewithBilling.addClickHandler(this);
		
		//Vijay on 3 march 2017
		serviceSchedulePopUp.getP_serviceWeek().addChangeHandler(this);


		boolean isDownload = AuthorizationHelper.getDownloadAuthorization(
				Screen.CONTRACT, LoginPresenter.currentModule.trim());
		if (isDownload == false) {
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
		
		/** date 4.1.2018 commented by komal as click event was firing twice
		CommunicationLogPopUp.getBtnOk().addClickHandler(this);
		CommunicationLogPopUp.getBtnCancel().addClickHandler(this);
		CommunicationLogPopUp.getBtnAdd().addClickHandler(this);
		**/
		
		/** Date 13-09-2017 2017 added by vijay for customer Address ******/
		custAddresspopup.getLblOk().addClickHandler(this);
		custAddresspopup.getLblCancel().addClickHandler(this);
		custAddresspopup.getBtnbillingaddr().addClickHandler(this);
		lblUpdate.addClickHandler(this);
		
		lblSummary.addClickHandler(this);
		lblChecklistReport.addClickHandler(this);
		
		
		
		uploadPopup.getBtnOk().addClickHandler(this);
		uploadPopup.getBtnCancel().addClickHandler(this);
		
		/** date 01/11/2017 added by komal for new cancellation popup add anf cancel button**/
		cancellationPopup.getLblOk().addClickHandler(this);
		cancellationPopup.getLblCancel().addClickHandler(this);
		
		/**
		 * Date : 23-11-2017 BY ANIL
		 * adding event handler on advance filter component of schedule service popup
		 */
		serviceSchedulePopUp.getLbServices().addChangeHandler(this);
		serviceSchedulePopUp.getOlbCustomerBranch().addChangeHandler(this);
		serviceSchedulePopUp.getOlbServicingBranch().addChangeHandler(this);
		/**
		 * End
		 */
		/**
		 * nidhi
		 * ((..
		 * for exclude day
		 */
		serviceSchedulePopUp.getE_mondaytofriday().addChangeHandler(this);
		
		/** Date 19-03-2019 by Vijay for Do not renwe popup ****/
		nonRenewalMsgPopup.getLblOk().addClickHandler(this);
		nonRenewalMsgPopup.getLblCancel().addClickHandler(this);
		
		/**
		 * Date 22-03-2019 by Vijay for NBHC CCPM Quotation is mandatory to renew contract.
		 */
		singledropdownpopup.getLblOk().addClickHandler(this);
		singledropdownpopup.getLblCancel().addClickHandler(this);
		
		/**
		 * Date 26-03-2019 by Vijay For NBHC CCPM contract discountinue with approval process  
		 */
//		contractDiscountinue.getLblOk().addClickHandler(this);
//		contractDiscountinue.getLblCancel().addClickHandler(this);
		
		/*** Date 25-04-2019 by Vijay for Stack details NBHC CCPM **/
		stackDetailsPopUp.getLblOk().addClickHandler(this);
		/** date 06-11-2019 added by komal to cancel open status documents directly **/
		cancellation.getLblOk().addClickHandler(this);
		cancellation.getLblCancel().addClickHandler(this);
		form.getObjPaymentMode().addChangeHandler(this);
		
		datePopUp.getBtnOne().addClickHandler(this);
		datePopUp.getBtnTwo().addClickHandler(this);
		
		accMgrPopup.getLblOk().addClickHandler(this);
		accMgrPopup.getLblCancel().addClickHandler(this);
		
		accMgrPopup.folbEmployee.getHeaderLabel().setText("Account Manager");
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "LoadAccountManager")){
			accMgrPopup.olbEmployee.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CONTRACT, "Account Manager");
		}
		
		conRenewalPopup.getBtnOne().addClickHandler(this);
		conRenewalPopup.getBtnTwo().addClickHandler(this);
		technicianPopup.getLblOk().addClickHandler(this);
		technicianPopup.getLblCancel().addClickHandler(this);
		
		form.chkStationedService.addClickHandler(this);
		
//		form.objContactList.addChangeHandler(this);
		
		form.objContactList.addFocusHandler(new FocusHandler() {
			
			@Override
			public void onFocus(FocusEvent event) {
				// TODO Auto-generated method stub
				try{
					Console.log("Focus EVENT Fired!!!");
					if(form.personInfoComposite.getValue()!=null){
						Console.log("Focus EVENT Fired-1 !!!");
						if(form.personInfoComposite.contactObj!=null){
							Console.log("Focus EVENT Fired-2 !!!");
							boolean flag=false;
							for(int i=1;i<form.objContactList.getItemCount();i++){
								if(form.objContactList.getItemText(i).equals(form.personInfoComposite.contactObj.getName())){
									flag=true;
									break;
								}
							}
							
							if(!flag){
								Console.log("Focus EVENT Fired-3 !!!");
								form.objContactList.addItem(form.personInfoComposite.contactObj.getName());
//								for(int i=1;i<form.objContactList.getItemCount();i++){
//									if(form.objContactList.getItemText(i).equals(form.personInfoComposite.contactObj.getName())){
//										form.objContactList.setSelectedIndex(i);
//										break;
//									}
//								}
							}
						}
					}
				}catch(Exception e){
					Console.log("Exception-Focus EVENT Fired!!!");
				}
			}
		});
		
		
		contractResetPopup.getLblCancel().addClickHandler(this);
		contractResetPopup.getLblOk().addClickHandler(this);
		
		contractResetconditionPopup.getBtnOne().addClickHandler(this);
		contractResetconditionPopup.getBtnTwo().addClickHandler(this);
		
		otpPopup.getBtnOne().addClickHandler(this);
		otpPopup.getBtnTwo().addClickHandler(this);
		
		infoPopup.getBtnOne().addClickHandler(this);
		infoPopup.getBtnTwo().addClickHandler(this);
		infoPopup.getInfo().setEnabled(false);
		
		addExtraServicesconditionPopup.getBtnOne().addClickHandler(this);
		addExtraServicesconditionPopup.getBtnTwo().addClickHandler(this);
		
		descriptionPopup.getLblOk().addClickHandler(this);
		descriptionPopup.getLblCancel().addClickHandler(this);
	}

	@Override
	public void reactOnSearch() {
		super.reactOnSearch();

		ContractPresenterSearchProxy.makeSalesPersonEnable();
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
//		 super.reactToProcessBarEvents(e);
		InlineLabel label = (InlineLabel) e.getSource();
		String text = label.getText().trim();

		
		if(text.equals(AppConstants.VIEWINVOICEPAYMENT)){
			String status=model.getStatus();
			searchedContractList=new ArrayList<Contract>();
			searchedContractList=view.getSearchpopupscreen().getSupertable().getListDataProvider().getList();
			Console.log("searchedContractList size="+searchedContractList.size());
			if(status.equals(Contract.CREATED)||status.equals(Contract.REQUESTED)||status.equals(Contract.REJECTED)) {
				form.showDialogMessage("Invoice & payment are generated after contract is approved");
				return;
			}
			//Ashwini Patil Date:2-05-2024 added process config as evrim is not ready to accept one month document restriction
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "RemoveOneMonthFilterOnInvoiceAndPayment")) {
				reactOnViewInvoicePaymentAndService(true);
			}else {
			serviceAsync.getContractBillingCount(model, new AsyncCallback<Integer>() {
				
				@Override
				public void onSuccess(Integer result) {
					// TODO Auto-generated method stub
					if(result==null||result==0) {
						form.showDialogMessage("No Billing document found");
						return;
					}else if(result>50) {
						boolean conf = Window.confirm("There are total "+result+" bills exist for this contract. Since bills are more in number, only open billing documents will be shown. If you want to see list then go to Accounts - Billing Details/Billing List - search. Do you want to continue?");
						if (conf) {						
							reactOnViewInvoicePaymentAndService(false);
						}
							
					}else
						reactOnViewInvoicePaymentAndService(true);
						
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
			}
			
		}
		
		
		if (text.equals("Print Renewal Reminder")){
				reactOnPrintreminder();
		}
		if (text.equals("Email Renewal Reminder")){
			reactOnEmailRenewalLetter();
	    }
		
		if (text.equals(AppConstants.CANCLECONTRACT)) {
			/**
			 * Old Cancellation popup/logic
			 */
//			popup.getPaymentDate().setValue(new Date() + "");
//			popup.getPaymentID().setValue(
//					form.getTbContractId().getValue().trim());
//			popup.getPaymentStatus().setValue(
//					form.getstatustextbox().getValue().trim());
//			cancelpanel = new PopupPanel(true);
//			cancelpanel.add(popup);
//			cancelpanel.show();
//			cancelpanel.center();
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "CancelOpenDocumentsDirectly")){
				cancellation.showPopUp();
			}else{
			/**
			 * Date : 14-11-2017 bY Komal
			 */
			contractDisContinue = false;
			form.showWaitSymbol();
			boolean conf = Window.confirm("Do you really want to cancel this contract?");
			if(conf==true){
			getAllDocumentDetails();
			}
			/**
			 * End
			 */
			}
		}

		if (text.equals(AppConstants.MANAGEPROJECT))
			reactOnManageProject();

		if (text.equals("View Services")){
			if(form.isPopUpAppMenubar()){
				Console.log("SERVICE CONTRACT POPUP : View Services clicked!!");
				return;
			}

			//Ashwini Patil Date:2-05-2024 added process config as evrim is not ready to accept one month document restriction
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "RemoveOneMonthFilterOnViewServices")) {
				reactToViewServices();
			}else {
				serviceAsync.getContractServiceCount(model, new AsyncCallback<Integer>() {
					
					@Override
					public void onSuccess(Integer result) {
						// TODO Auto-generated method stub
						if(result==null||result==0) {
							form.showDialogMessage("No services found");
							return;
						}else if(result>100) {
//							form.showDialogMessage("There are total "+result+" services exist for this contract. This Program can show only upto 100 services. So you can view only current month services now. If you want to view all services then, copy contract id -> click on service menu -> customer service list -> click on search and enter contract id -> click on go");
							boolean conf = Window.confirm("There are total "+result+" services exist for this contract. This Program can show only upto 100 services. So you can view only current month services now. If you want to view all services then, copy contract id -> click on service menu -> customer service list -> click on search and enter contract id -> click on go. Do you want to continue?");
							if (conf)
								reactOnViewCurrentDayServices();
						}else
							reactToViewServices();
							
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				});
			}
			
		}
			
		if (text.equals("JobCard"))
			reactToJobCard();
		if (text.equals("New"))
		{
			if(form.isPopUpAppMenubar()){
				Console.log("SERVICE CONTRACT POPUP : New clicked!!");
				return;
			}
			reactToNew();
		}
		if (text.equals("Raise Invoice"))
			reactToInvoice();

		if (text.equals("Monthly Service Record")) {
			serviceReportPanel = new PopupPanel(true);
			serviceReportPanel.add(frmAndToPopForMonthlyReport);
			serviceReportPanel.show();
			serviceReportPanel.center();
		}

		if (text.equals(AppConstants.PRINTJOBCARD))
			reactOnJobCard();
		if (text.equals("Email"))
			reactOnEmail();
		if (text.equals(AppConstants.CONTRACTRENEWAL)){
			/**
			 * Date 22-03-2019 By Vijay 
			 * Des :- For NBHC CCPM for renew contract first Quotation must create then allow
			 * to renew contract with process config.
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "EnableQuotationMandatoryForContractRenwal")){
				singledropdownpopup.showPopUp();
				singledropdownpopup.makequotaionlive(model.getCinfo().getCount());
			}
			else{
				if(form.isPopUpAppMenubar()){
					Console.log("SERVICE CONTRACT POPUP : CONTRACT RENEWAL clicked!!");
					return;
				}
				if(model.isRenewFlag()){
				boolean conf = Window.confirm("This contract is already renewed,Do you really want to renew it again?");
				if(conf==true){
				reactOnContractRenewal();
				}
				}else{
					reactOnContractRenewal();	
				}
			}
		}
		if (text.equals(AppConstants.CANCELLATIONSUMMARY)) {
			getSummaryFromContractID();
		}
		if (text.equals(AppConstants.WORKORDER)) {
			if(form.isPopUpAppMenubar()){
				Console.log("SERVICE CONTRACT POPUP : CREATE WORK ORDER clicked!!");
				return;
			}
			reactOnWorkOrder();
		}
		if (text.equals(AppConstants.CUSTOMERINTEREST)) {
			reactOncustomerInterest();
		}
		if (text.equals(AppConstants.MATERIALREQUIREDREPORT)) {
			getMINFromContractID();
		}

		if (text.equals("Update Services")) {
			System.out.println("in side update service");
			updateServicesWithFerNumber();
		}

		if (text.equals(ManageApprovals.APPROVALREQUEST)) {
			/*** Date 13-10-2018 By Vijay For dont allow user to click twice ***/
			form.showWaitSymbol();
			
			/**
			 * rohan added code for NBHC 
			 * Date : 11-05-2017
			 * I have added this code for NBHC for validating Ref Number 1 is mandetory 
			 */
			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC"))
			{
				MyQuerry querry = AppUtility.checkRefNUmberInCustomerByCustomerId(model.getCustomerId(),model.getCompanyId());

				service.getSearchResult(querry,	new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						String refNumber="";
						if (result.size() == 0) {

						}
						else {

							for (SuperModel model : result) {
								Customer customer = (Customer) model;
								 refNumber=customer.getRefrNumber1();
							}
						}
						
						System.out.println("inside customer ref number ="+refNumber);
						if(refNumber.equals("")){
							form.showDialogMessage("Please fill Customer reference number 1 in customer screen..!");
							form.hideWaitSymbol();//Vijay
						}else{
							
							System.out.println("INSIDE REQUEST APPROVAL!!!!!!!");
							if (!form.tbTicketNumber.getValue().equals("")) {
								System.out.println("INSIDE TICKET NUMBER NOT NULL !!!!!!");
								checkingServiceStatusAgainstTicket(
								Integer.parseInt(form.tbTicketNumber.getValue()),
								Integer.parseInt(form.tbContractId.getValue()));
								
							} else {
								System.out.println("INSIDE TICKET NUMBER NULL !!!!!!");
								
								/**
								 *  nidhi 
								 *  7-10-2017
								 *  for set customer name and id for approval info
								 */
								form.getManageapproval().setBpId(model.getCustomerId()+"");
								form.getManageapproval().setBpName(model.getCustomerFullName());
								form.hideWaitSymbol();//Vijay
								form.getManageapproval().reactToRequestForApproval();
								
								/**
								 * end
								 */
							}
						}			
						
						
					}
				});
			}
			else{
				System.out.println("INSIDE REQUEST APPROVAL!!!!!!!");
				if (!form.tbTicketNumber.getValue().equals("")) {
					System.out.println("INSIDE TICKET NUMBER NOT NULL !!!!!!");
					checkingServiceStatusAgainstTicket(
					Integer.parseInt(form.tbTicketNumber.getValue()),
					Integer.parseInt(form.tbContractId.getValue()));
					
				} else {
					System.out.println("INSIDE TICKET NUMBER NULL !!!!!!");
					/**
					 *  nidhi 
					 *  7-10-2017
					 *  for set customer name and id for approval info
					 */
					form.getManageapproval().setBpId(model.getCustomerId()+"");
					form.getManageapproval().setBpName(model.getCustomerFullName());
					form.hideWaitSymbol();//Vijay
					form.getManageapproval().reactToRequestForApproval();
					/**
					 *  end
					 */
					
				}
			}
		}
		if (text.equals(ManageApprovals.CANCELAPPROVALREQUEST)) {
			System.out.println("INSIDE APPROVAL REQUEST CANCEL!!!!!!!");
			form.getManageapproval().reactToApprovalRequestCancel();
		}

		if (text.equals("Update Taxes")) {
			panel = new PopupPanel(true);
			panel.add(frmAndToPop);
			panel.show();
			panel.center();

		}

		if (text.equals("CreateServices")) {
			System.out.println("in side create services");
			createServicesByContract();
		}

		// vijay contract rate services n billing
		if (text.equals(AppConstants.CREATESERVICE)) {
			/**
			 * Date 28-09-2018 by Vijay
			 * Des :- if rate contract expired then it should not allow to create service and billing documents validation
			 */
			Date todayDate = new Date();
			if(model.isContractRate()){
				/**
				 * Date :- 24-11-2018 By Vijay
				 * Des :- Rate contract expired validation with process config
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "DoNotAllowCreateServiceBillingForExipredRateContract") ){
				final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
				if(model.getEndDate()!=null && fmt.format(model.getEndDate()).equals(fmt.format(todayDate)) || todayDate.before(model.getEndDate())){
					reactOnCreateServices();
				}
				else{
					form.showDialogMessage("Contract Expired Can Not Create Service And Billing!");
				}
				}
				else{
					reactOnCreateServices();
				}
			}
			else{
				form.showDialogMessage("This is only applicable to Rate Contract!");
			}
		}

		// Rahul Added this for NBHC on 15-10-2016
		if (text.equals(AppConstants.UPDATEPRICES)) {
			reactOnUpdatePrices();
		}

		// ajinkya added this for PCAMB
		if (text.equals("rodentCountMonthlyReport")) {
			createRodentCountReportMonthly();

		}
		// ajinkya added this for PCAMB
		if (text.equals("GetServiceCard")) {
			creategetServiceCard();

		}
		// ajinkya added this for PCAMB
		if(text.equals("GetServiceVoucher")){
			creategetServiceVoucher();
		}
		
		// ajinkya added this for PCAMB
		 if(text.equals("printIntimationLetter")){
				createprintIntimationLetter();
				
			}
		
		if (text.equals(AppConstants.CANCELSERVICES)) {
			
			if(model.getStatus().equals(Contract.CANCELLED)){
				ContractServiceAsync conAsync = GWT.create(ContractService.class);
				form.showWaitSymbol();
				conAsync.changeStatus(model, new AsyncCallback<Void>() {
					@Override
					public void onSuccess(Void result) {
						form.hideWaitSymbol();
						form.showDialogMessage("Services Cancelled Successfully.");
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
						
					}
				});
			}else{
				form.showDialogMessage("This option is valid only if contract is in cancelled state.");
			}
		}
		
		
//		if(text.equals("Send SMS")){
//			reactOnSendSMS();
//		}

		
		/**
		 * Date : 14-03-2017 By ANIL
		 */
		if(text.equals("FAR")){
			if(validateSelectedRecord()){
				final String url = GWT.getModuleBaseURL()+"pdfprintservice"+"?Id="+model.getId()
						+"&"+"type="+"FAR";
				Window.open(url, "test", "enabled");
			}
		}
		if(text.equals("FMR")){
			if(validateSelectedRecord()){
				final String url = GWT.getModuleBaseURL()+"pdfprintservice"+"?Id="+model.getId()
						+"&"+"type="+"FMR";
				Window.open(url, "test", "enabled");
			}
		}
		if(text.equals("CHR")){
			if(validateSelectedRecord()){
				final String url = GWT.getModuleBaseURL()+"pdfprintservice"+"?Id="+model.getId()
						+"&"+"type="+"CHR";
				Window.open(url, "test", "enabled");
			}
		}
		
		if (text.equals("Create EVA Services")) {
			System.out.println("in side update service");
			updateServicesAsPerEVARequirement();
		}
		
		/**
		 * Date : 01-08-2017 By ANIL
		 */
		if(text.equals(ManageApprovals.SUBMIT)){
			
			//Ashwini Patil Date:14-02-2024
			if(!LoginPresenter.loggedInUser.equals(model.getApproverName()))
			{
				form.showDialogMessage("Approver name ("+model.getApproverName()+") and Logged in username ("+LoginPresenter.loggedInUser+") are different. To submit contract, keep the approver name same as username.");					
				return;
			}
			if(form.getManageapproval().isSelfApproval()){
			
			
			/**
			 * @author Anil , Date : 28-05-2019
			 */
			/**Added by Sheetal:28-03-2022 
			 * Des :Adding condition that Quick contracts can not be submitted from contract screen,only contracts 
			 *     created from contract will be submitted from here**/
			        
			
			if(!model.isQuickContract()){
				form.getManageapproval().submitLbl=label;
				System.out.println("SUBMIT BF:: "+form.getManageapproval().submitLbl.getText());
				form.getManageapproval().submitLbl.setText("Processing...");
				System.out.println("SUBMIT AF:: "+form.getManageapproval().submitLbl.getText());
				form.getManageapproval().reactToSubmit();
				Console.log("Contract");
				 } 
			else{
				Console.log("QuickContract value = "+model.isQuickContract());
				 form.showDialogMessage("Quick Contract can not be submitted from here !!");
			 }
			}else {
				
				 form.showDialogMessage("Self approval is not authorized by admin. Kindly activate self approval. Click on Implementation icon → Setup tab → Service tab → tick the checkbox \"Contract Self Approval\"");					
				 return;
			}
			/**end**/
		}
		/**
		 * End
		 */
		
		/**
		 * Date : 29-07-2017 By ANIL
		 * 
		 */
		if (text.equals(AppConstants.VIEWBILL)) {
		
			if(form.isPopUpAppMenubar()){
				Console.log("SERVICE CONTRACT POPUP : VIEW BILL clicked!!");
				return;
			}
			/**
			 * @author Vijay Date - 09-02-2022
			 * Des :- when we open invoicePayment tab then we click on view bill screen getting freeze so added timer to work smoothly
			 */
			if(flag){
				Timer timer = new Timer() {
					
					@Override
					public void run() {
						viewBillingDocuments();
					}
				};
				timer.schedule(2000);
			}
			else{
				viewBillingDocuments();
			}
		}
		/**@Sheetal : 16-02-2022
		 *  Adding view lead , view Quotation button***/
		if (text.equals(AppConstants.VIEWLEAD)) {	
			if(form.isPopUpAppMenubar()){
				Console.log("SERVICE CONTRACT POPUP : View Lead clicked!!");
				return;
			}
			viewLeadDocument();
			Console.log("view lead");
		}
        if (text.equals(AppConstants.VIEWQUOTATION)) {
        	if(form.isPopUpAppMenubar()){
				Console.log("SERVICE CONTRACT POPUP : View Quotation clicked!!");
				return;
			}
			viewQuotationDocument();
		     Console.log("view Quotation");
		}
		/** Date 12-09-2017 added by vijay for creating new contract from cancelled contracts Ultra**/
		if(text.equals(AppConstants.COPYCONTRACT)){
			reactOnCopyContract();
		}
		
		/** Date 13-09-2017 added by vijay for Customer address  **/
		if(text.equals(AppConstants.CUSTOMERADDRESS)){
			reactOnCustomerAddres();
		}
		
		/**
		 * Date 14-10-2017
		 * By Jayshree 
		 * Des. Changes are made to print the service Card for frends Pest
		 */
		if(text.equals(AppConstants.SERVICECARD)){
			System.out.println("contract presentor check"+model.getId());
			System.out.println("URL"+GWT.getModuleBaseURL()
					+ "serviceCardServlet?Id=" + model.getId());
			final String url = GWT.getModuleBaseURL()
					+ "serviceCardServlet?Id=" + model.getId();
			Window.open(url, "test", "enabled");
//			final String url = GWT.getModuleBaseURL()
//					+ "pdfservice" + "?Id=" + model.getId()
//					+ "&" + "type=" + "c" + "&"
//					+ "preprint=" + "plane";
			Window.open(url, "test", "enabled");
		}
		
		/** Date 06-11-2017 added by ANIL for Upload T&C  **/
		if(text.equals(AppConstants.UPLOADTERMSANDCONDITION)){
			uploadPopup.clear();
			panel=new PopupPanel(true);
			panel.add(uploadPopup);
			panel.show();
			panel.center();
		}
		
		/** Date 27-02-017 added by VIJAY for Approval Status for multilevel aprroval  **/
		if(text.equals(ManageApprovals.APPROVALSTATUS)){
			form.getManageapproval().getApprovalStatus();
		}
		
		/** Date 09-04-2017 added by Anil for updating clients license **/
		if(text.equals(AppConstants.LICENSETYPELIST.UPDATE_CLIENTS_LICENSE)){
			
			updateClientsLicense();
		}
		
		/** Date 19-04-2017 added by Anil  **/
		if(text.equals(AppConstants.VIEWSALESORDER)){
			viewSaleOrder();
		}
		
		/** Date 30-05-2018 By vijay for updating services ***/
		if(text.equals(AppConstants.UPDATESERVICES)){
			reactonUpdateService();
		}
		/**
		 *  nidhi
		 *  5-06-2018
		 */
		if(text.equals(AppConstants.CONTRACTDISCOUNTINUED)){
//			contractDisContinue = true;
//			getAllDocumentDetails();
			
			
			contractDiscountinue = new ContractDiscontinueApprovalPopup(); 
			contractDiscountinue.getLblOk().addClickHandler(this);
			contractDiscountinue.getLblCancel().addClickHandler(this);
			contractDiscountinue.showPopUp();
		}
		/**
		 * @author Anil , Date : 29-05-2019
		 * Deleting extra generated services for NBHC
		 */
		if(text.equals("Delete duplicate services")){
			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "DeleteExtraServiceProductWise")){
//				deleteExtraGeneratedService();
				datePanel=new PopupPanel(true);
				datePanel.add(datePopUp);
				datePanel.center();
				datePanel.show();
			}else{
				deleteExtraGeneratedService(0);
			}
			
			
		}
		
		/**
		 * @author Vijay Chougule
		 * Des - NBHC CCPM Upload contract Service Brnaches Updation in
		 * Services
		 */
		if(text.equals("UpdateServiceBranches")){
			updateServiceBranchesinServices();
		}
		
		if (text.equalsIgnoreCase(AppConstants.SERVICERECORD)) {
			multipleSRCopyPrintPopup = new MultipleSRCopyPrintPopup(model);
			
			// multipleSRCopyPrintPopup.getPopup().clear();
			
			/**
			 * @author Ashwini Patil
			 * @since 17-02-2022 
			 * As per Nitin Sir's Instructions One Summary button is added and ok button text changed to "SR Copy"
			 * By default pdf format will be selected and service status completed will be checked
			 */
			
			
			lblSummary.getElement().setId("addbutton");	
			multipleSRCopyPrintPopup.getHorizontal().insert(lblSummary, 1);
			
			lblChecklistReport.getElement().setId("addbutton");	
			multipleSRCopyPrintPopup.getHorizontal().insert(lblChecklistReport, 3);
			
			
			multipleSRCopyPrintPopup.getLblOk().setText("SR Copy");	
			multipleSRCopyPrintPopup.getLblCancel().setText("Checklist Report");	
			
			multipleSRCopyPrintPopup.setContract(model);
						

			multipleSRCopyPrintPopup.showPopUp();
			Console.log("multipleSRCopyPrintPopup.showPopUp() called");
			multipleSRCopyPrintPopup.rbPdf.setValue(true);
			multipleSRCopyPrintPopup.cbCompleted.setValue(true);			
			
		}
		
		if (text.equals("Update Account Manager")) {
			accMgrPopup.showPopUp();
		}
		
		if(text.equals(AppConstants.MAKERENEWABLE)){
			reactonRenewable();
		}
		
		if(text.equals(AppConstants.UPDATESERVICEVALUE)){
			reactOnUpdateServiceValue();
		}
		
		if(text.equals(AppConstants.UPDATETECHNICIANNAME)){
			reactOnChangeTechnicianName();
		}
		
		if(text.equals(AppConstants.CONTRACTRESET)){
			reactOnContractReset();
		}
		if(text.equals("Copy")){
			if(form.isPopUpAppMenubar()){
				Console.log("SERVICE CONTRACT POPUP : Copy clicked!!");
				return;
			}
			reactOnCopy();
		}
		
		if(text.equals(AppConstants.DELETECLIENTLICENSE)){
			deleteClientsLicense();
		}
		
		if(text.equals(AppConstants.VIEWCOMPLAIN)){
			reactonViewComplaint();
		}
		if(text.equals(AppConstants.UpdateSalesPerson)){
			Console.log("UpdateSalesPerson clicked");
			if(form.olbeSalesPerson.getValue()!=null) {
				update.updateSalesPerson(model.getCompanyId(), model.getCount(), model.getEmployee(), new AsyncCallback<String>() {
					
					@Override
					public void onSuccess(String result) {
						if(result!=null)
							form.showDialogMessage(result);
						else
							form.showDialogMessage("Failed to update sales person");
				
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("Failed to update sales person");						
					}
				});			
			}else
				form.showDialogMessage("Select sales person fisrt");
		}
		
		if(text.equals(AppConstants.AddCustomerBranches)) {
			custBranchPanel = new PopupPanel(true);
			custBranchPanel.add(addExtraServicesconditionPopup);
			custBranchPanel.setGlassEnabled(true);
			custBranchPanel.show();
			custBranchPanel.center();
	
		}
		
		if (text.equals(AppConstants.markcontractasrenewed)){
			Console.log("markcontractasrenewed clicked");

				if(model.isRenewFlag()) {
					form.showDialogMessage("This contract is already renewed!");
					return;
				}else
					descriptionPopup.showPopUp();
		}
		
		
	}
	
	
	
	/**@Sheetal : 16-02-2022
	 *  Adding view lead , view Quotation button***/

	private void viewQuotationDocument() {


		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(model.getQuotationCount());
		Console.log("quotation id : "+model.getQuotationCount());
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new Quotation());
		Console.log("Query Executed");
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				Console.log("fail");
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No Quotation document found.");
					Console.log("quotation size: "+result.size());
					return;
				}
				else{
					final Quotation QuotationDocument=(Quotation) result.get(0);
					final QuotationForm form=QuotationPresenter.initalize();
					Console.log("quotation size>0: "+result.size());
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(QuotationDocument);
							form.setToViewState();
							Console.log("view state quotation");
							
						}
					};
					timer.schedule(1000);
				}
			}
		});
		
	}

	private void viewLeadDocument() {

		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(model.getLeadCount());
		temp.add(filter);
		
		Console.log("lead id"+model.getLeadCount());
		querry.setFilters(temp);
		querry.setQuerryObject(new Lead());
		Console.log("Query executed");
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				Console.log("fail");
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				
				if(result.size()==0){
					form.showDialogMessage("No Lead document found.");
					Console.log("lead size: " +result.size());
					return;
				}
				else{
					final Lead leadDocument=(Lead) result.get(0);
					final LeadForm form=LeadPresenter.initalize();
					Console.log("lead size>0: "+result.size());
					Console.log("initialize");
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(leadDocument);
							form.setToViewState();
							Console.log("lead view");
						}
					};
					timer.schedule(1000);
				}
			}
		});
		
	
		
	}

	private void reactOnViewInvoicePaymentAndService(final boolean showAllFlag) {
		Console.log("in reactOnViewInvoicePaymentAndService showAllFlag="+showAllFlag);
		MyQuerry query=null;
		if(showAllFlag) {
			query = getQuery(model);
			query.setQuerryObject(new BillingDocument());
		}else {
			List<String> lstatus=new ArrayList<String>();
			lstatus.add("Created");
			lstatus.add("Approved");
			
			query = new MyQuerry();
			
			Vector<Filter> filtervec = new Vector<Filter>();
			
			Filter filter = new Filter();
			filter.setIntValue(model.getCount());
			filter.setQuerryString("contractCount");
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setLongValue(model.getCompanyId());
			filter.setQuerryString("companyId");
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setStringValue("Service Order");
			filter.setQuerryString("typeOfOrder");
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setList(lstatus);
			filter.setQuerryString("status IN");
			filtervec.add(filter);
			
			query.setQuerryObject(new BillingDocument());
			
			query.setFilters(filtervec);
		}

		genasync.getSearchResult(query, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
			
				Console.log("billing Result size=="+result.size());
				ArrayList<BillingDocument> billinglist = new ArrayList<BillingDocument>();
				for(SuperModel model : result){
					BillingDocument billingEntity = (BillingDocument) model;
					
					billinglist.add(billingEntity);
				}

				if (result.size() != 0) {
					if (billinglist.size() != 0) {
						Comparator<BillingDocument> countSort = new Comparator<BillingDocument>() {

							@Override
							public int compare(BillingDocument r1, BillingDocument r2) {
								int count1 = r1.getCount();
								int count2 = r2.getCount();
								if (count1 == count2) {
									return 0;
								}
								if (count1 > count2) {
									return 1;
								} else {
									return -1;
								}
							}
						};
						Collections.sort(billinglist, countSort);
					}
				}
				
				invoicePaymentPopup.companyId=model.getCompanyId();
				invoicePaymentPopup.OrderId=model.getCount();
				invoicePaymentPopup.showAllFlag=showAllFlag;
				/**
				 * @author Anil @since 13-10-2021
				 */
				invoicePaymentPopup.tabPanel.selectTab(0);
				invoicePaymentPopup.getTabBillingTable().getDataprovider().setList(billinglist);
				invoicePaymentPopup.getTabBillingTable().getTable().redraw();
				invoicePaymentPanel = new PopupPanel(false,true);//Ashwini Patil Date:24-03-2022 Description: If User clicks outside the popup system will not respond to that event. Popup needs to be closed first
		
				
				invoicePaymentPanel.add(invoicePaymentPopup);
				invoicePaymentPanel.center();
				invoicePaymentPanel.show();
				Console.log("after invoicePaymentPanel.show()");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.showDialogMessage("Unexpected Error Occurred");

			}
		});
		
	
			
	}

	private MyQuerry getQuery(Contract model) {

		MyQuerry querry = new MyQuerry();
		
		Vector<Filter> filtervec = new Vector<Filter>();
		
		Filter filter = new Filter();
		filter.setIntValue(model.getCount());
		filter.setQuerryString("contractCount");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setLongValue(model.getCompanyId());
		filter.setQuerryString("companyId");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setStringValue("Service Order");
		filter.setQuerryString("typeOfOrder");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		return querry;
	}

	private void deleteExtraGeneratedService(int productId) {
		// TODO Auto-generated method stub
		if(model!=null){
			form.showWaitSymbol();
			generalAsync.deleteExtraServicesAgainstContract(model,productId, new AsyncCallback<String>() {
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
				}
				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
					form.showDialogMessage(result);
				}
			});
		}
	}
	private void reactOnEmailRenewalLetter() {
		Console.log("inside reactOnEmailRenewalLetter");
//		boolean conf = Window.confirm("Do you really want to send email?");
//		if (conf == true) {
//			emailService.initiateContractRenewalNewEmail( model,new AsyncCallback<Void>() {
//				@Override
//				public void onFailure(Throwable caught) {
//					Window.alert("Resource Quota Ended ");
//					caught.printStackTrace();
//				}
//
//				@Override
//				public void onSuccess(Void result) {
//					Window.alert("Email Sent Sucessfully !");
//				}
//			});
			
			/**
			 * @author Vijay Date 09-11-2021
			 * Des :- above old code commented 
			 * Added New Email Popup Functionality
			 */
			emailpopup.showPopUp();
			setEmailPopUpData(Screen.CONTRACTRENEWAL);
			
			/**
			 * ends here
			 */
			
//		}
	}

	private void reactOnPrintreminder() {

		if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "CompanyAsLetterHead")) {
			panel = new PopupPanel(true);
			panel.add(conRenewalPopup);
			panel.setGlassEnabled(true);
			panel.show();
			panel.center();
			Console.log("inside print reminder");
		} else {
			final String url = GWT.getModuleBaseURL() + "NewContractRenewalPdf"
					+ "?ContractId=" + model.getCount() + "&CompanyId="
					+ model.getCompanyId() + "&" + "preprint=" + "plane&flag="
					+ false+"&renewalFmt=New"+"&callFrom=Contract";//Ashwini Patil Date:27-12-2023 added callFrom to understand renewal print is getting called from contract screen
			Window.open(url, "test", "enabled");

		}
	}


	/** Date 30-05-2018 By vijay for updating services ***/
	private void reactonUpdateService() {
		
		form.showWaitSymbol();
		async.updateServices(model, new AsyncCallback<Boolean>() {
			
			@Override
			public void onSuccess(Boolean result) {
				if(result){
					form.showDialogMessage("Services Updated Successfully");
				}
				form.hideWaitSymbol();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();

			}
		});
	}

	private void viewSaleOrder() {
		int salesCount;
		try{
			salesCount=Integer.parseInt(model.getRefNo().trim());
		}catch(NumberFormatException e){
			form.showDialogMessage("No sales order found.");
			return;
		}
		
		
		
		MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(salesCount);
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new SalesOrder());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No sales order found.");
					return;
				}
				if(result.size()>0){
					final SalesOrder contract=(SalesOrder) result.get(0);
					final SalesOrderForm form=SalesOrderPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(contract);
							form.setToViewState();
						}
					};
					timer.schedule(1000);
				}
			}
		});
	
	}
	
	
	private void updateClientsLicense() {
		String url=Window.Location.getHref();
		Console.log("URL-------- "+url);
		String[] spliturl=url.split("\\.");
		String companyname=spliturl[0];
		Console.log("COMPANY :: "+companyname);
		companyname=companyname.replace("http://","");
		Console.log("COMPANY1 :: "+companyname);
		IntegrateSyncServiceAsync intAsync=GWT.create(IntegrateSyncService.class);
		String actionTask="";
		if(model.getStatus().equals(Contract.APPROVED)){
			actionTask=AppConstants.LICENSETYPELIST.ACTION_TASK_CREATE;
		}else if(model.getStatus().equals(Contract.CANCELLED)){
			actionTask=AppConstants.LICENSETYPELIST.ACTION_TASK_CANCEL;
		}else{
			form.showDialogMessage("Not a valid call.");
			return;
		}
		
		form.showWaitSymbol();
		intAsync.callLicenseUpdateInteface(model, actionTask, new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(String result) {
				form.hideWaitSymbol();
				form.showDialogMessage(result);
			}
		});
	}
	
	
	/** Date 13-09-2017 2017 added by vijay for customer Address ******/
	private void reactOnCustomerAddres() {

		System.out.println("Customer address"+model.getNewcustomerAddress());
		System.out.println(model.getCustomerServiceAddress());
		if(model.getNewcustomerAddress()!=null && model.getNewcustomerAddress().getAddrLine1()!=null && !model.getNewcustomerAddress().getAddrLine1().equals("")&& model.getCustomerServiceAddress()!=null &&model.getCustomerServiceAddress().getAddrLine1()!=null && !model.getCustomerServiceAddress().getAddrLine1().equals("")){
			System.out.println("Contract ===");
			lblUpdate.getElement().setId("addbutton");
			custAddresspopup.getHorizontal().add(lblUpdate);
//			custAddresspopup.getLblOk().setText("Save");
			
			custAddresspopup.showPopUp();
			
			/**
			 * Date 20-04-2018 By vijay  if condition for orion pest locality refreshing
			 * and else block for old code
			 */
			boolean flag = false;
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Locality", "DontLoadAllLocalityAndCity")){
				setAddressDropDown();
				flag = true;
			}else{
				
				custAddresspopup.getBillingAddressComposite().setValue(model.getNewcustomerAddress());
				custAddresspopup.getServiceAddressComposite().setValue(model.getCustomerServiceAddress());
			}
			/**
			 * ends here
			 */
			
			custAddresspopup.getBillingAddressComposite().setEnable(false);
			custAddresspopup.getServiceAddressComposite().setEnable(false);
			
			if(flag){

				if(LoginPresenter.globalLocality.size()>custAddresspopup.getBillingAddressComposite().locality.getItems().size()
						|| LoginPresenter.globalLocality.size()>custAddresspopup.getServiceAddressComposite().locality.getItems().size()){
					custAddresspopup.getServiceAddressComposite().locality.setListItems(LoginPresenter.globalLocality);
					custAddresspopup.getBillingAddressComposite().locality.setListItems(LoginPresenter.globalLocality);
					
					custAddresspopup.getBillingAddressComposite().setValue(model.getNewcustomerAddress());
					custAddresspopup.getServiceAddressComposite().setValue(model.getCustomerServiceAddress());
					
					}
			}
			
		}else{
			getCustomerAddress();
			
		}
	}
	
	
	/**
	 * Date 20-04-2018 By vijay
	 * here i am refreshing address composite data if global list size greater than drop down list size
	 * because we are adding locality and city from an entity in view state to global list if doest not exist in global list 
	 */
	
	private void setAddressDropDown() {

		if(LoginPresenter.globalCity.size()==0){
			City city = new City();
			city.setCityName(model.getNewcustomerAddress().getCity());
			city.setState(model.getNewcustomerAddress().getState());
			city.setStatus(true);
			LoginPresenter.globalCity.add(city);
			
			Locality locality = new Locality();
			locality.setLocality(model.getNewcustomerAddress().getLocality());
			locality.setCityName(model.getNewcustomerAddress().getCity());
			locality.setStatus(true);
			LoginPresenter.globalLocality.add(locality);
		}
		boolean flag = true;
		for(int i=0;i<LoginPresenter.globalCity.size();i++){
			if(model.getNewcustomerAddress().getCity()!=null && !model.getNewcustomerAddress().getCity().equals("") ){
				if(LoginPresenter.globalCity.get(i).getCityName().equals(model.getNewcustomerAddress().getCity())){
					flag = false;
					break;
				}
			}
		}
		if(flag){
			City city = new City();
			city.setCityName(model.getNewcustomerAddress().getCity());
			city.setState(model.getNewcustomerAddress().getState());
			city.setStatus(true);
			LoginPresenter.globalCity.add(city);
		}
		else{
			flag = true;
		}
		
		for(int i=0;i<LoginPresenter.globalCity.size();i++){
			if(model.getCustomerServiceAddress().getCity()!=null && !model.getCustomerServiceAddress().getCity().equals("") ){
				if(!model.getCustomerServiceAddress().getCity().equals(model.getNewcustomerAddress().getCity())){
					if(LoginPresenter.globalCity.get(i).getCityName().equals(model.getCustomerServiceAddress().getCity())){
						flag = false;
						break;
					}
				}else{
					flag = false;
					break;
				}
			}
		}
		
		if(flag){
			City city = new City();
			city.setCityName(model.getCustomerServiceAddress().getCity());
			city.setState(model.getCustomerServiceAddress().getState());
			city.setStatus(true);
			LoginPresenter.globalCity.add(city);
			System.out.println("222222222");

		}
		else{
			flag = true;
		}
		
		
		for(int j=0;j<LoginPresenter.globalLocality.size();j++){
			if(model.getNewcustomerAddress().getLocality()!=null && !model.getNewcustomerAddress().getLocality().equals("") ){
				if(LoginPresenter.globalLocality.get(j).getLocality().equals(model.getNewcustomerAddress().getLocality())){
					flag = false;
					break;
				}
			}
			
		}	
		
		if(flag){
			Locality locality = new Locality();
			locality.setLocality(model.getNewcustomerAddress().getLocality());
			locality.setCityName(model.getNewcustomerAddress().getCity());
			locality.setStatus(true);
			LoginPresenter.globalLocality.add(locality);

		}else{
			flag = true;
		}
		
		for(int j=0;j<LoginPresenter.globalLocality.size();j++){
			
			if(model.getCustomerServiceAddress().getLocality()!=null && !model.getCustomerServiceAddress().getLocality().equals("") ){
				if(!model.getCustomerServiceAddress().getLocality().equals(model.getNewcustomerAddress().getLocality())){
					if(LoginPresenter.globalLocality.get(j).getLocality().equals(model.getCustomerServiceAddress().getLocality())){
						flag = false;
						break;
					}
				}else{
					flag = false;
					break;	
				}
				
			}
		}

		if(flag){
			Locality locality = new Locality();
			locality.setLocality(model.getCustomerServiceAddress().getLocality());
			locality.setCityName(model.getCustomerServiceAddress().getCity());
			locality.setStatus(true);
			LoginPresenter.globalLocality.add(locality);

		}
		
	}
	

	private void getCustomerAddress() {
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(model.getCinfo().getCount());
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new Customer());
		form.showWaitSymbol();
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();

				for(SuperModel model:result){
					
					Customer customer = (Customer)model;
					System.out.println("hi vijay"+customer.getAdress().getCompleteAddress());
					lblUpdate.getElement().setId("addbutton");
					custAddresspopup.getHorizontal().add(lblUpdate);
//					custAddresspopup.getLblOk().setText("Save");
					custAddresspopup.showPopUp();
					
					custAddresspopup.getBillingAddressComposite().setValue(customer.getAdress());
					custAddresspopup.getServiceAddressComposite().setValue(customer.getSecondaryAdress());

					custAddresspopup.getBillingAddressComposite().setEnable(false);
					custAddresspopup.getServiceAddressComposite().setEnable(false);
					
				}
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.showDialogMessage("Unexpected Error Ouccured");
				form.hideWaitSymbol();

			}
		});
	}

	private void reactOnCopyContract() {
//		form.setToNewState();
		
//		form.showWaitSymbol();
		
		SalesLineItemTable.newBranchFlag = true;
//		final ContractForm form = new ContractForm();
//		ContractPresenterTable gentable = new ContractPresenterTableProxy();
//		gentable.setView(form);
//		gentable.applySelectionModle();
//		ContractPresenterSearch.staticSuperTable = gentable;
//		ContractPresenterSearch searchpopup = new ContractPresenterSearchProxy();
//		form.setSearchpopupscreen(searchpopup);
//		ContractPresenter presenter = new ContractPresenter(form,new Contract());
//		AppMemory.getAppMemory().stickPnel(form);
//		form.showWaitSymbol();
		
		final ContractForm form=ContractPresenter.initalize();
//		form.checkCustomerBranch(model.getCinfo().getCount());
//		form.checkCustomerStatus(model.getCinfo().getCount(),true,true);
		
		final Contract contract = new Contract();
		contract.setCount(0);
		contract.setDescription("");
		contract.setCreatedBy("");
		contract.setStartDate(null);
		contract.setEndDate(null);
		contract.setStatus("Created");
		
		contract.setPremisesDesc(model.getPremisesDesc());
		contract.setCinfo(model.getCinfo());
		contract.setRefNo(model.getRefNo());
		contract.setRefDate(model.getRefDate());
		contract.setReferedBy(model.getReferedBy());
		contract.setDocument(model.getDocument());
		
		contract.setContractDate(model.getContractDate());
		
		contract.setTotalAmount(model.getTotalAmount());
		contract.setNetpayable(model.getNetpayable());
		contract.setLeadCount(model.getLeadCount());
		contract.setContractCount(model.getContractCount());
		contract.setQuotationCount(model.getQuotationCount());
		contract.setCreditPeriod(model.getCreditPeriod());
		contract.setRefContractCount(model.getContractCount());
		//contract.setItems(model.getItems());
		contract.setPaymentTermsList(model.getPaymentTermsList());
		contract.setProductTaxes(model.getProductTaxes());
		contract.setProductCharges(model.getProductCharges());
		contract.setServiceScheduleList(model.getServiceScheduleList());
		contract.setScheduleServiceDay(model.getScheduleServiceDay());
		contract.setContractRate(model.isContractRate());
		contract.setBranchWiseBilling(model.isBranchWiseBilling());
		
		contract.setPayTerms(model.getPayTerms());
		contract.setCustomersaveflag(false);
		contract.setRefNo2(model.getRefNo2());
		contract.setTechnicianName(model.getTechnicianName());
		contract.setSegment(model.getSegment());
		contract.setDiscountAmt(model.getDiscountAmt());
		contract.setFinalTotalAmt(model.getFinalTotalAmt());
		contract.setRoundOffAmt(model.getRoundOffAmt());
		contract.setGrandTotalAmount(model.getGrandTotalAmount());
		contract.setInclutaxtotalAmount(model.getInclutaxtotalAmount());
		

		contract.setBranch(model.getBranch());
		contract.setEmployee(model.getEmployee());
		if(model.getPaymentMethod()!=null)
		contract.setPaymentMethod(model.getPaymentMethod());
		if(model.getNumberRange()!=null)
		contract.setNumberRange(model.getNumberRange());
		if(model.getCategory()!=null)
		contract.setCategory(model.getCategory());
		if(model.getType()!=null)
		contract.setType(model.getType());
		if(model.getGroup()!=null)
		contract.setGroup(model.getGroup());
		contract.setApproverName(model.getApproverName());
		/** date 11.12.2018 added by komal for nbhc service branches button and popup **/
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableServiceBranch")){
			List<SalesLineItem> itemList = new ArrayList<SalesLineItem>();
			for(SalesLineItem item : model.getItems()){
			if(item.getServiceBranchesInfo() != null && item.getServiceBranchesInfo().size() > 0){
				item.setServiceBranchesInfo(item.getServiceBranchesInfo());
				itemList.add(item);
			}else{
				ArrayList<BranchWiseScheduling> branchArrayList = new ArrayList<BranchWiseScheduling>();
				for(Branch branch : LoginPresenter.customerScreenBranchList){
					BranchWiseScheduling branchWiseScheduling = new BranchWiseScheduling();
					branchWiseScheduling.setBranchName(branch.getBusinessUnitName());
					branchWiseScheduling.setArea(0);
					branchWiseScheduling.setCheck(false);
					branchArrayList.add(branchWiseScheduling);
				}
				HashMap<Integer, ArrayList<BranchWiseScheduling>> serviceBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
				serviceBranchlist.put(item.getProductSrNo(), branchArrayList);
				item.setServiceBranchesInfo(serviceBranchlist);
				itemList.add(item);
			}
		  }
			contract.setItems(itemList);
		}else{
			contract.setItems(model.getItems());
		}
		
		
		form.showWaitSymbol();
		AppMemory.getAppMemory().currentState=ScreeenState.NEW;
		form.toggleAppHeaderBarMenu();
		Timer timer = new Timer() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
				form.updateView(contract);
				/**
				 * @author Anil @since 04-08-2021
				 * Requirement raised by Ashwini if we are copying contract then number range should be editable
				 */
				form.olbcNumberRange.setEnabled(true);
			}
		};
		timer.schedule(6000);
				
	}

	
	private void viewBillingDocuments() {
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(model.getCount());
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new BillingDocument());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No billing document found.");
					return;
				}
				if(result.size()==1){
					final BillingDocument billDocument=(BillingDocument) result.get(0);
					final BillingDetailsForm form=BillingDetailsPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(billDocument);
							form.setToViewState();
							
						}
					};
					timer.schedule(1000);
				}else{
					BillingListPresenter.initalize(querry);
				}
			}
		});
		
	}

	
	/**
	 * Date : 14-03-2017 By ANIL
	 * this method validate the selected record for printing FAR,FMR or CHR PDF
	 */
	
	public boolean validateSelectedRecord(){
		String validationMsg="";
		if(!model.getStatus().equals(Contract.APPROVED)){
			validationMsg=validationMsg+"Contract should be in approved state.\n";
		}
		
		boolean prodValidFlag=false;
		for(SalesLineItem item:model.getItems()){
			if(item.getProductName().equalsIgnoreCase("Stack Fumigation")){
				prodValidFlag=true;
				break;
			}
		}
		if(!prodValidFlag){
			validationMsg=validationMsg+"This is applicable for Stack Fumigation service.";
		}
		
		if(!validationMsg.equals("")){
			form.showDialogMessage(validationMsg);
			return false;
		}
		return true;
	}
	
	
	/**
	 * Date 16-03-2017
	 * added by vijay
	 * for Sending SMS for Contract Cancellation 
	 * requirement from Petra pest
	 */

	private void reactOnSendSMS() {

		try {
			
			String CompanyName = Slick_Erp.businessUnitName;
			form.showWaitSymbol();
			smsService.checkSMSConfigAndSendContractCancellationSMS(model.getCompanyId(), model.getCinfo().getFullName(), model.getCinfo().getCellNumber(), model.getCount(), CompanyName,LoginPresenter.bhashSMSFlag,model.getCinfo().getCount(), new AsyncCallback<ArrayList<Integer>>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<Integer> result) {
					
					
//					if(result.contains(-1)){
//						form.showDialogMessage("SMS Service is not enable in order to enable SMS Service please contact at support@evasoftwaresolutions.com ");
//					}
//					else if(result.contains(-2)){
//						form.showDialogMessage("SMS Configuration is InActive");
//					}
//					else if(result.contains(-3)){
//						form.showDialogMessage("Please define SMS Template");
//					}
//					else if(result.contains(-4)){
//						form.showDialogMessage("SMS Template is InActive");
//					}
//					else if(result.contains(-5)){
//						form.showDialogMessage("Can not send SMS customer DND status is active");
//					}
//					else if(result.contains(1)){
//						form.showDialogMessage("SMS Sent Sucessfully");
//					}
					
					form.hideWaitSymbol();
				}
			});
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	/**
	 * ends here
	 */
	
		private void createprintIntimationLetter() {
			final String url = GWT.getModuleBaseURL() + "PcambIntimationLetterpdf"+"?Id="+model.getId();
			Window.open(url, "test", "enabled");
		}
	
	private void createRodentCountReportMonthly() {
		final String url = GWT.getModuleBaseURL()+ "PcambrodentCountMonthlyReportpdf" + "?Id=" + model.getId();
		Window.open(url, "test", "enabled");
	}

	private void creategetServiceCard() {
		final String url = GWT.getModuleBaseURL() + "PcambServiceCardPdf"+ "?Id=" + model.getId();
		Window.open(url, "test", "enabled");
	}
	
	 private void creategetServiceVoucher() {
		final String url = GWT.getModuleBaseURL() + "PcambServiceVoucherPdf"+"?Id="+model.getId();
		Window.open(url, "test", "enabled");
	}

	/**
	 * Description: This method will update prices of Approved contract. This
	 * method is only for NBHC Created By: Rahul Verma Created On: 15-10-2016
	 * Created for: NBHC (This is for NBHC for Webservice created contract so
	 * that they can change their prices of Approved Contract)
	 * 
	 */
	private void reactOnUpdatePrices() {
		// TODO Auto-generated method stub
		Console.log("Inside React on Update Prices and Product table size"
				+ model.getItems().size());
		listOfProductinUpdatePrices = new ArrayList<ContractRenewalProduct>();

		for (SalesLineItem salesLineItem : model.getItems()) {
			ContractRenewalProduct contractRenewalProduct = new ContractRenewalProduct();
			contractRenewalProduct.setPrduct(salesLineItem.getPrduct());
			contractRenewalProduct.setSrNo(salesLineItem.getProductSrNo());
			contractRenewalProduct.setProductCode(salesLineItem
					.getProductCode());
			contractRenewalProduct.setProductName(salesLineItem
					.getProductName());
			contractRenewalProduct.setDuaration(salesLineItem.getDuration());
			contractRenewalProduct.setProductPrice(Math.round(salesLineItem
					.getPrice()));
			contractRenewalProduct.setProductNewPrice(Math.round(salesLineItem
					.getPrice()));
			contractRenewalProduct.setVatTax(salesLineItem.getVatTax());
			contractRenewalProduct.setServiceTax(salesLineItem.getServiceTax());
			contractRenewalProduct.setVatTaxEdit(salesLineItem.getVatTaxEdit());
			contractRenewalProduct.setServiceTaxEdit(salesLineItem
					.getServiceTaxEdit());
			// updatePricesPopUp.productList.getDataprovider().getList().add(contractRenewalProduct);
			contractRenewalProduct.setRemark("");
			if (salesLineItem.getPremisesDetails() != null) {
				contractRenewalProduct.setPremises(salesLineItem
						.getPremisesDetails());
			} else {
				contractRenewalProduct.setPremises("");
			}

			listOfProductinUpdatePrices.add(contractRenewalProduct);
		}
		updatePricesPopUp.productList.getDataprovider().setList(
				listOfProductinUpdatePrices);
		updatePricePanel = new PopupPanel(true);
		updatePricePanel.add(updatePricesPopUp);
		updatePricePanel.show();
		updatePricePanel.center();
	}

	/**
	 * ENDS
	 */

	private void reactOnCreateServices() {
		System.out.println(" hi vijay inside ===");
		productPanel = new PopupPanel(true);

		ArrayList<ProductInfo> productlist = new ArrayList<ProductInfo>();

		List<SalesLineItem> list = form.getSaleslineitemtable().getDataprovider().getList();
		System.out.println(" product list size ====" + list.size());

		for (int i = 0; i < list.size(); i++) {

			ProductInfo prodInfo = new ProductInfo();
			prodInfo.setSelectedProduct(true);
			prodInfo.setProductCode(list.get(i).getProductCode());
			prodInfo.setProductName(list.get(i).getProductName());
			prodInfo.setServiceDate(new Date());
			prodInfo.setPrice(list.get(i).getPrice());
			prodInfo.setTotalAmount(list.get(i).getPrice());
			prodInfo.setServiceTax(list.get(i).getServiceTax());
			prodInfo.setServiceTaxEdit(list.get(i).getServiceTaxEdit());
			/**
			 * Date : 30-10-2017 BY ANIL
			 */
			prodInfo.setVatTax(list.get(i).getVatTax());
			prodInfo.setVatTaxEdit(list.get(i).getVatTaxEdit());
			prodInfo.setSrNo(list.get(i).getProductSrNo());
			/**
			 * End
			 */
			prodInfo.setAreaprSqFeet(list.get(i).getArea());
			prodInfo.setProductQty(list.get(i).getQty());

			productlist.add(prodInfo);
			
			if(productlist.get(i).getProductQty()>1){
				multipleBranchesSelectedFlag=true;	
				}

		}

//		System.out.println("for popup list size ===" + productlist.size());
//		for (int i = 0; i < productlist.size(); i++) {
//			System.out.println(" price value vijay ===="+ productlist.get(i).getPrice());
//		}

		if(multipleBranchesSelectedFlag){
			productPopupForMultipleBranches.getProductTable().getDataprovider().setList(productlist);
			productPanel.add(productPopupForMultipleBranches);
			productPanel.show();
			productPanel.center();
		}else{
			productpopup.getProductTable().getDataprovider().setList(productlist);
			productPanel.add(productpopup);
			productPanel.show();
			productPanel.center();		
		}

	}

	private void updateServicesWithFerNumber() {

		System.out.println("in side popup call");
		panel = new PopupPanel(true);
		panel.add(frmAndToPopforServiceUpdate);
		panel.show();
		panel.center();

	}

	private void mymethodforprintingPdf(Date fromdate, Date todate) {
		// SimpleDateFormat fmt12345 = new SimpleDateFormat("dd-MM-yyyy");
		DateTimeFormat format = DateTimeFormat.getFormat("dd-MM-yyyy");
		String fromdtAndTodt = format.format(fromdate) + "$"+format.format(todate);

		final String url = GWT.getModuleBaseURL() + "mypdf" + "?Id="
				+ model.getId() + "&" + "date=" + fromdtAndTodt;
		Window.open(url, "test", "enabled");
	}

	@Override
	public void reactOnPrint() {

		// *************rohan changes ********************

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("processName");
		filter.setStringValue("Contract");
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("processList.status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);

		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProcessConfiguration());

		service.getSearchResult(querry,	new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						System.out.println(" result set size +++++++"
								+ result.size());

						List<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();

						if (result.size() == 0) {

							final String url = GWT.getModuleBaseURL()
									+ "pdfservice" + "?Id=" + model.getId()
									+ "&" + "type=" + "c" + "&" + "preprint="
									+ "plane";
							Window.open(url, "test", "enabled");
						} else {

							for (SuperModel model : result) {
								ProcessConfiguration processConfig = (ProcessConfiguration) model;
								processList.addAll(processConfig
										.getProcessList());

							}

							for (int k = 0; k < processList.size(); k++) {
								if (processList
										.get(k)
										.getProcessType()
										.trim()
										.equalsIgnoreCase("CompanyAsLetterHead")
										&& processList.get(k).isStatus() == true) {

									cnt = cnt + 1;

								}

								if (processList
										.get(k)
										.getProcessType()
										.trim()
										.equalsIgnoreCase(
												"PestoIndiaQuotations")
										&& processList.get(k).isStatus() == true) {

									pestoIndia = pestoIndia + 1;

								}

								if (processList.get(k).getProcessType().trim()
										.equalsIgnoreCase("VCareCustomization")
										&& processList.get(k).isStatus() == true) {

									vcare = vcare + 1;

								}

								if (processList
										.get(k)
										.getProcessType()
										.trim()
										.equalsIgnoreCase(
												"HygeiaPestManagement")
										&& processList.get(k).isStatus() == true) {

									hygeia = hygeia + 1;

								}

								if (processList.get(k).getProcessType().trim()
										.equalsIgnoreCase("AMCForGenesis")
										&& processList.get(k).isStatus() == true) {

									genesis = genesis + 1;

								}
								if (processList
										.get(k)
										.getProcessType()
										.trim()
										.equalsIgnoreCase(
												"UniversalPestCustomization")
										&& processList.get(k).isStatus() == true) {
									universal = universal + 1;
								}
								
								if (processList
										.get(k)
										.getProcessType()
										.trim()
										.equalsIgnoreCase(
												"OnlyForPecopp")
										&& processList.get(k).isStatus() == true) {
									pecopp = pecopp + 1;
								}

								if (processList.get(k).getProcessType().trim()
										.equalsIgnoreCase("PCAMBCustomization")
										&& processList.get(k).isStatus() == true) {
									final String url = GWT.getModuleBaseURL()
											+ "PcambContractpdf" + "?Id="
											+ model.getId();
									Window.open(url, "test", "enabled");
									return;
								}

							}

							if (cnt > 0) {
								System.out.println("in side react on prinnt");
								panel = new PopupPanel(true);
								panel.add(conditionPopup);
								panel.setGlassEnabled(true);
								panel.show();
								panel.center();
							} else if (pestoIndia > 0) {
								System.out.println("in side react on prinnt");
								panel = new PopupPanel(true);
								panel.add(conditionPopup);
								panel.setGlassEnabled(true);
								panel.show();
								panel.center();

							} else if (universal > 0) {
								System.out.println("in side react on prinnt Universal pest Quotation");
								panel = new PopupPanel(true);
								panel.add(conditionPopup);
								panel.setGlassEnabled(true);
								panel.show();
								panel.center();
							}

							else if (vcare > 0) {
								System.out.println("in side react on prinnt");
								panel = new PopupPanel(true);
								panel.add(conditionPopup);
								panel.setGlassEnabled(true);
								panel.show();
								panel.center();

							} else if (hygeia > 0) {

								final String url = GWT.getModuleBaseURL()
										+ "pdfcon" + "?Id=" + model.getId();
								Window.open(url, "test", "enabled");

							} else if (genesis > 0) {

								final String url = GWT.getModuleBaseURL()
										+ "pdfamc" + "?Id=" + model.getId();
								Window.open(url, "test", "enabled");

							}else if(pecopp > 0){                       
								final String url = GWT.getModuleBaseURL()+"contractserviceone"+"?Id="+model.getId()+"&"+"preprint=plane"+"&"+"type="+"Contract";
								Window.open(url, "test", "enabled");//"pecopppdfservlet"+"?Id="+model.getId()+"&"+"type="+"
							}else {
								final String url = GWT.getModuleBaseURL()
										+ "pdfservice" + "?Id=" + model.getId()
										+ "&" + "type=" + "c" + "&"
										+ "preprint=" + "plane";
								Window.open(url, "test", "enabled");

							}
						}
					}
				});
		// **************************changes ends here
		// ********************************

	}

	public void reactOnJobCard() {
		final String url = GWT.getModuleBaseURL() + "jobcardpdf" + "?Id="
				+ model.getId();
		Window.open(url, "test", "enabled");
	}

	@Override
	public void reactOnDownload() {
		if(view.getSearchpopupscreen().getSupertable().getListDataProvider().getList()==null||view.getSearchpopupscreen().getSupertable().getListDataProvider().getList().size()==0)
		{
			form.showDialogMessage("No records found for download!");
			return;
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "RestrictContractsDownloadWithOTP")){
			generatedOTP=(int) Math.floor(100000 + Math.random() * 900000);
			Console.log("generatedOTP="+generatedOTP);
			smsService.checkConfigAndSendContractDownloadOTPMessage(model.getCompanyId(), LoginPresenter.loggedInUser, generatedOTP, new  AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.showDialogMessage("Failed");
				}

				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					Console.log("in success");
					if(result!=null&&result.equalsIgnoreCase("success")) {						
						otpPopup.clearOtp();
						otppopupPanel = new PopupPanel(true);
						otppopupPanel.add(otpPopup);
						otppopupPanel.show();
						otppopupPanel.center();
					}else {
						form.showDialogMessage(result);
					}
						
				}
			} );
		
		}else
			runRegularDownloadProgram();
	}

	public void reactOnEmail() {
//		boolean conf = Window.confirm("Do you really want to send email?");
//		if (conf == true) {
//			emailService.initiateServiceContractEmail((Sales) model,
//					new AsyncCallback<Void>() {
//
//						@Override
//						public void onFailure(Throwable caught) {
//							Window.alert("Resource Quota Ended ");
//							caught.printStackTrace();
//
//						}
//
//						@Override
//						public void onSuccess(Void result) {
//							Window.alert("Email Sent Sucessfully !");
//
//						}
//					});
//		}
		
		/**
		 * @author Vijay Date 19-03-2021
		 * Des :- above old code commented 
		 * Added New Email Popup Functionality
		 */
		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		emailpopup.showPopUp();
		setEmailPopUpData(screenName);
		
		/**
		 * ends here
		 */
	}

	/**
	 * Method token to make new model
	 */
	@Override
	protected void makeNewModel() {
		model = new Contract();
	}

	private void reactOncustomerInterest() {/*

		model.setCustomerInterestFlag(true);
		form.showWaitSymbol();
		Timer timer = new Timer() {
			@Override
			public void run() {
				genasync.save(model, new AsyncCallback<ReturnFromServer>() {

					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An Unexpected Error occured !");
					}

					@Override
					public void onSuccess(ReturnFromServer result) {
						form.setAppHeaderBarAsPerStatus();
						form.setToViewState();
						System.out.println("Success");
						form.showDialogMessage("Updated Successfully!");
						form.getTbdonotrenew().setValue(
								AppConstants.CONTRACTDONOTRENEW);
					}
				});
				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);

	*/

		nonRenewalMsgPopup.showPopUp();
	}
	
	public void reactOnRenewalOk(){
		
		if(nonRenewalMsgPopup.validate()){
			model.setCustomerInterestFlag(true);
			model.setNonRenewalRemark(nonRenewalMsgPopup.getOlbNonRenewalRemark().getValue(nonRenewalMsgPopup.getOlbNonRenewalRemark().getSelectedIndex()));
			form.showWaitSymbol();
			Timer timer = new Timer() {
				@Override
				public void run() {
					genasync.save(model, new AsyncCallback<ReturnFromServer>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
							nonRenewalMsgPopup.hidePopUp();
							form.hideWaitSymbol();

						}

						@Override
						public void onSuccess(ReturnFromServer result) {
							form.setAppHeaderBarAsPerStatus();
							form.setToViewState();
							System.out.println("Success");
							form.showDialogMessage("Updated Successfully!");
							form.getTbdonotrenew().setValue(AppConstants.CONTRACTDONOTRENEW);
							form.getTbDonotRenewalMsg().setValue(nonRenewalMsgPopup.getOlbNonRenewalRemark().getValue());
							nonRenewalMsgPopup.hidePopUp();
							form.hideWaitSymbol();
							
							createDoNotRenewaEntryInContractRenewal();

						}

						
					});
				}
			};
			timer.schedule(3000);}
	}

	public static ContractForm initalize() {
		/**
		 * Date : 04-07-2017 BY NIDHI
		 */
		SalesLineItemTable.newBranchFlag = true;
		/**
		 * End
		 */
		ContractForm form = new ContractForm();
		ContractPresenterTable gentable = new ContractPresenterTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		ContractPresenterSearch.staticSuperTable = gentable;
		ContractPresenterSearch searchpopup = new ContractPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);
		form.addClickEventOnActionAndNavigationMenus(); //this line was missing due to which 3 line menu was not coming
		ContractPresenter presenter = new ContractPresenter(form,new Contract());
//		form.setToNewState();
		/**
		 * Date : 11-10-2017 BY ANIL
		 * This line was added to update the name of screen loaded from any other screen or by clicking on billing details on account model
		 */
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contract",Screen.CONTRACT);
		/**
		 * END
		 */
		AppMemory.getAppMemory().stickPnel(form);
		form.paymentDateFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ContractPaymentNonEditable");
		
		return form;
	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.Contract")
	public static class ContractPresenterSearch extends
			SearchPopUpScreen<Contract> {

		public ContractPresenterSearch(FormStyle rowform) {
			super(rowform);
		}

		@Override
		public MyQuerry getQuerry() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}
	};

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.Contract")
	public static class ContractPresenterTable extends SuperTable<Contract>
			implements GeneratedVariableRefrence {
		
		public ContractPresenterTable(boolean flag) {
			// TODO Auto-generated constructor stub
			super(flag);
		}
		
		public ContractPresenterTable() {
			// TODO Auto-generated constructor stub
			super();
		}

		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void createTable() {
			// TODO Auto-generated method stub

		}

		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub

		}

		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub

		}

		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub

		}
	};

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		flag = false;
	if(event.getSource() == invoicePaymentPopup.btnClose){
//			this.initalize(); //Commented by Ashwini Patil on 7-09-2022 and written following logic instead
		


		/**
		 * @author Ashwini Patil
		 * Date: 7-09-2022
		 * Problem was Search screen getting refreshed when we click on Invoice and Payment in Contract
		 */
		SalesLineItemTable.newBranchFlag = true;
		
		final ContractForm form = new ContractForm();
		ContractPresenterTable gentable = new ContractPresenterTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		ContractPresenterSearch.staticSuperTable = gentable;
		ContractPresenterSearch searchpopup = new ContractPresenterSearchProxy();
		searchpopup.getSupertable().getListDataProvider().setList(searchedContractList);
		Console.log("list set to searchpopup");
		form.setSearchpopupscreen(searchpopup);
		form.addClickEventOnActionAndNavigationMenus(); 
		ContractPresenter presenter = new ContractPresenter(form,new Contract());
		form.setToNewState();
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contract",Screen.CONTRACT);
		AppMemory.getAppMemory().stickPnel(form);
		form.paymentDateFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ContractPaymentNonEditable");
		
//		------------------------
	
			
			Timer time = new Timer() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					form.updateView(model);
		        	form.setToViewState();
					invoicePaymentPanel.hide();
				}
			};
			time.schedule(2000);
		
		flag = true;
//		/** Date 09-03-2021 by Vijay updated code for smooth execution **/
//		AppMemory.getAppMemory().currentScreen = Screen.CONTRACT;
//		
//
//		if(form.getProcesslevelBarNames()!=null)
//		form.setProcesslevelBarNames(form.getProcesslevelBarNames());
//		if(form.getProcessLevelBar()!=null)
//		form.setProcessLevelBar(form.getProcessLevelBar());
//		if(form.getProcesslevel()!=null)
//		form.setProcesslevel(form.getProcesslevel());
//
////		if(!AppMemory.getAppMemory().enableMenuBar||FormStyle.DEFAULT==form.formstyle){
////			form.content.add(form.processLevelBar.content);	
////		}else{
////			form.addClickEventOnActionAndNavigationMenus();
////		}
//		
//		
//		form.setToViewState();
//		invoicePaymentPanel.hide();
//		setEventHandeling();
		invoicePaymentPopup.showAllFlag=true;
		}








		if(event.getSource()==datePopUp.getBtnOne()){
			if(datePopUp.getIbServiceId().getValue()==null){
				form.showDialogMessage("Please enter product id.");
				return;
			}
			deleteExtraGeneratedService(datePopUp.getIbServiceId().getValue());
			datePanel.hide();
		}
		if(event.getSource()==datePopUp.getBtnTwo()){
			datePanel.hide();
		}
		if (event.getSource() == frmAndToPop.btnOne) {
			getTaxesDetails();
			panel.hide();
		}

		if (event.getSource() == frmAndToPop.btnTwo) {
			panel.hide();
		}

		if (event.getSource() == frmAndToPopForMonthlyReport.btnOne) {
			System.out.println("in side onclick event");
				Date fromdate = frmAndToPopForMonthlyReport.getStartDate().getValue();
				Date endDate = frmAndToPopForMonthlyReport.getEndDate().getValue();
			
					mymethodforprintingPdf(fromdate,endDate);
					serviceReportPanel.hide();
		}

		if (event.getSource() == frmAndToPopForMonthlyReport.btnTwo) {
			serviceReportPanel.hide();
		}

		if (event.getSource() == frmAndToPopforServiceUpdate.btnOne) {
			System.out.println("in side onclick of popup "
					+ frmAndToPopforServiceUpdate.getFromDate().getValue()
					+ "-" + frmAndToPopforServiceUpdate.getToDate().getValue());
			upddateRefNoInServices(frmAndToPopforServiceUpdate.getFromDate()
					.getValue(), frmAndToPopforServiceUpdate.getToDate()
					.getValue());
			panel.hide();
		}

		if (event.getSource() == frmAndToPopforServiceUpdate.btnTwo) {
			getTaxesDetails();
			panel.hide();
		}

		if (event.getSource().equals(conditionPopup.getBtnOne())) {

			if (cnt > 0) {
				/**
				 *   Date : 02/04/2021 Added By Priyanka
				 *   Des : CompanyAsletterHead Process conf add for Pecopp 
				 */
				if(pecopp>0){
					final String url = GWT.getModuleBaseURL() + "contractserviceone"+"?Id="+model.getId()+"&"+"preprint=yes"+"&"+"type="+"Contract";
					Window.open(url, "test", "enabled");
					panel.hide();
					}else{
					System.out.println("in side one yes");
					final String url = GWT.getModuleBaseURL() + "pdfservice"
							+ "?Id=" + model.getId() + "&" + "type=" + "c" + "&"
							+ "preprint=" + "yes";
					Window.open(url, "test", "enabled");
					panel.hide();
				}
			}

			else if (pestoIndia > 0) {
				System.out.println("Pesto india quotation");
				final String url = GWT.getModuleBaseURL()
						+ "pdfpestoIndiaservice" + "?Id=" + model.getId() + "&"
						+ "type=" + "c" + "&" + "preprint=" + "yes";
				Window.open(url, "test", "enabled");
				panel.hide();
			} else if (vcare > 0) {
				System.out.println("vcare contract");
				final String url = GWT.getModuleBaseURL() + "Contract" + "?Id="
						+ model.getId() + "&" + "preprint=" + "yes";
				Window.open(url, "test", "enabled");
				panel.hide();
			} else if (universal > 0) {
				System.out.println("universal pest quotation");
				final String url = GWT.getModuleBaseURL()
						+ "universalServiceQuotation" + "?Id=" + model.getId()
						+ "&" + "preprint=" + "yes" + "&" + "type=" + "c";
				Window.open(url, "test", "enabled");
				panel.hide();
			}else if(ompest>0){
				final String url = GWT.getModuleBaseURL() + "NewContractRenewalPdf" + "?ContractId="+ model.getCount()+ "&CompanyId="+ model.getCompanyId()+"&"+"preprint="+"yes&flag="+false;
				Window.open(url, "test", "enabled");
				panel.hide();
			}

		}

		if (event.getSource().equals(conditionPopup.getBtnTwo())) {
			if (cnt > 0) {
				System.out.println("inside two no");
				/**
				 *   Date : 02/04/2021 Added By Priyanka
				 *   Des : CompanyAsletterHead Process conf add for Pecopp 
				 */
				if(pecopp>0){
					final String url = GWT.getModuleBaseURL() + "contractserviceone"+"?Id="+model.getId()+"&"+"preprint=no"+"&"+"type="+"Contract";
					Window.open(url, "test", "enabled");
					panel.hide();
					}else{
					System.out.println("in side one yes");
					final String url = GWT.getModuleBaseURL() + "pdfservice"
							+ "?Id=" + model.getId() + "&" + "type=" + "c" + "&"
							+ "preprint=" + "no";
					Window.open(url, "test", "enabled");
					panel.hide();
				}

			}

			else if (pestoIndia > 0) {
				System.out.println("Pesto india quotation");
				final String url = GWT.getModuleBaseURL()
						+ "pdfpestoIndiaservice" + "?Id=" + model.getId() + "&"
						+ "type=" + "c" + "&" + "preprint=" + "no"; // +"&"+"group="+model.getGroup();//1st
																	// PDF
				Window.open(url, "test", "enabled");

				panel.hide();

			} else if (vcare > 0) {
				System.out.println("vcare contract");
				final String url = GWT.getModuleBaseURL() + "Contract" + "?Id="
						+ model.getId() + "&" + "preprint=" + "no";
				Window.open(url, "test", "enabled");
				panel.hide();
			}

			else if (universal > 0) {
				System.out.println("universal pest quotation");
				final String url = GWT.getModuleBaseURL()
						+ "universalServiceQuotation" + "?Id=" + model.getId()
						+ "&" + "preprint=" + "no" + "&" + "type=" + "c";
				Window.open(url, "test", "enabled");
				panel.hide();
			}else if(ompest>0){
				final String url = GWT.getModuleBaseURL() + "NewContractRenewalPdf" + "?ContractId="+ model.getCount()+ "&CompanyId="+ model.getCompanyId()+"&"+"preprint="+"no&flag="+false;
				Window.open(url, "test", "enabled");
				panel.hide();
			}
		}
		
		if (event.getSource().equals(conRenewalPopup.getBtnOne())) {
			final String url = GWT.getModuleBaseURL() + "NewContractRenewalPdf" + "?ContractId="+ model.getCount()+ "&CompanyId="+ model.getCompanyId()+"&"+"preprint="+"yes&flag="+false+"&renewalFmt=New"+"&callFrom=Contract";//Ashwini Patil Date:27-12-2023 added callFrom to understand renewal print is getting called from contract screen
			Window.open(url, "test", "enabled");
			panel.hide();
		}

		if (event.getSource().equals(conRenewalPopup.getBtnTwo())) {
			final String url = GWT.getModuleBaseURL() + "NewContractRenewalPdf" + "?ContractId="+ model.getCount()+ "&CompanyId="+ model.getCompanyId()+"&"+"preprint="+"no&flag="+false+"&renewalFmt=New";
			Window.open(url, "test", "enabled");
			panel.hide();
		}

		if (event.getSource() == popup.getBtnOk()) {
			cancelpanel.hide();

			if (form.getstatustextbox().getValue().equals(Contract.APPROVED)) {
				
				if(validateRemark()){
				//  react on contract cancel
				reactOnCancelContract(popup.getRemark().getValue().trim());
//				checkForBillingDetailsStatus();
				}
				else{
					form.showDialogMessage("Please enter valid remark to cancel contract..!");
				}
			}

		} else if (event.getSource() == popup.getBtnCancel()) {
			cancelpanel.hide();
			popup.remark.setValue("");
		}

		if (event.getSource() == summaryPopup1.getBtnOk()) {
			summaryPanel.hide();

		} else if (event.getSource() == summaryPopup1.getBtnDownload()) {
			summaryPanel.hide();
			reactOnCancelSummaryDownload();
		}

		/****************************** Service Schedule Logic ********************************/

		if (event.getSource().equals(form.f_btnservice)) {
			if (form.getSaleslineitemtable().getDataprovider().getList().size() == 0) {
				form.showDialogMessage("Add Atleast One Product");
			} else {
				/**
				 * Date: 23-11-2017 BY ANIl
				 * initializing customer branches and service list box used for advance filter
				 */
				serviceSchedulePopUp.initializeCustomersBranch(form.customerbranchlist);
				serviceSchedulePopUp.initializeServiceListBox(form.getSaleslineitemtable().getDataprovider().getList());
				/**
				 * End
				 */
				reactToService();
				reactforTable();
				serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
			}
		}

		if (event.getSource().equals(serviceSchedulePopUp.getBtnOk())) {

			int cnt = 0;
			int pic = 0;
			List<SalesLineItem> salesitemlis = form.saleslineitemtable
					.getDataprovider().getList();
			List<ServiceSchedule> prodservicelist = this.serviceSchedulePopUp
					.getPopScheduleTable().getDataprovider().getList();

			for (int k = 0; k < salesitemlis.size(); k++) {
				for (int i = 0; i < prodservicelist.size(); i++) {
					if (calculateEndDate(salesitemlis.get(k).getStartDate(),
							salesitemlis.get(k).getDuration()).equals(
							prodservicelist.get(i).getScheduleServiceDate())) {
						cnt = cnt + 1;
					}
				}
			}

			if (cnt > 1) {
				form.showDialogMessage("Please Schedule services properly");
			} else {
				reactOnOk();
			}
		}

		if (event.getSource() == serviceSchedulePopUp.getBtnCancel()) {
			schedulePanel.hide();
		}

		if (event.getSource() == serviceSchedulePopUp.getP_default()) {/**
			 * Date : 24-11-2017 BY ANIL
			 * For default selection with advance filter
			 */
			if(isAdvanceFilterSelected()){
				List<ServiceSchedule> serSchList=getUpdatedScheduledServices(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList(),"Default",
						serviceSchedulePopUp.getP_mondaytofriday().getSelectedIndex(),
						serviceSchedulePopUp.getP_serviceWeek().getSelectedIndex(),
						serviceSchedulePopUp.getE_mondaytofriday().getSelectedIndex());
				updateServiceScheduleList(serSchList,form.globalScheduleServiceList);
				serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serSchList);
				serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
				
			}else{
				reactfordefaultTable();
				serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
			}
//			reactfordefaultTable();
//			serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
			/**
			 * End
			 */
			}

		if (event.getSource() == serviceSchedulePopUp.getP_customise()) {
			List<ServiceSchedule> prodservicelist = this.serviceSchedulePopUp
					.getPopScheduleTable().getDataprovider().getList();
			if (prodservicelist.size() != 0) {
				serviceSchedulePopUp.getPopScheduleTable().setEnable(true);
				form.checkCustomerBranch(Integer.parseInt(form
						.getPersonInfoComposite().getId().getValue()));
				reactforcustumize();
			} else {
				form.showDialogMessage("Please Select Either Equal or Interval");
				serviceSchedulePopUp.getP_customise().setValue(false);
			}

		}
		if (event.getSource() == serviceSchedulePopUp.getBtnReset()) {
			reactforreset();
		}

		// if (event.getSource().equals(form.btnclientAsset)) {
		// getclientsideasset();
		// }

		// if (event.getSource().equals(clientassetPopUp.getBtnOk())) {
		// System.out.println("Ok button click...");
		//
		// reactOnOk1();
		// assetPanel.hide();
		//
		// }

		if (event.getSource() == clientassetPopUp.getBtnCancel()) {
			assetPanel.hide();
		}

		// vijay
		if (event.getSource() == productpopup.getBtnOk()) {
			System.out.println(" hi vijay inside ok bttn");
			boolean checkContractRate = Validate();
			if (checkContractRate) {
				CreateServices(model,true);
				productPanel.hide();
			}
			if (checkContractRate == false) {
				form.showDialogMessage("Cant Create Services For this Type of Contract");
			}
		}

		if (event.getSource() == productpopup.getBtnCancel()) {
			System.out.println(" hi vijay inside cancell bttn");
			productPanel.hide();
		}
		
		
		if (event.getSource() == productPopupForMultipleBranches.getBtnOk()) {
			boolean checkContractRate = Validate();
			if (checkContractRate) {
				CreateServices(model,true);
				productPanel.hide();
			}
			if (checkContractRate == false) {
				form.showDialogMessage("Cant Create Services For this Type of Contract");
			}
		}

		if (event.getSource() == productPopupForMultipleBranches.getBtnCancel()) {
			System.out.println(" hi vijay inside cancell bttn");
			productPanel.hide();
		}

		
		if (event.getSource() == form.chkservicewithBilling) {
			if (form.chkservicewithBilling.getValue()) {
				System.out.println(" hi rohan inside onclick event for check box");
				ArrayList<PaymentTerms> ptermsList = new ArrayList<PaymentTerms>();

				form.paymentTermsTable.connectToLocal();
				PaymentTerms pterm = new PaymentTerms();
				pterm.setPayTermDays(0);
				pterm.setPayTermPercent(100.00);
				pterm.setPayTermComment("Advance");

				ptermsList.add(pterm);

				form.paymentTermsTable.getDataprovider().setList(ptermsList);
				form.addPaymentTerms.setEnabled(false);
				form.addPayTerms.setEnabled(false);

			} else {
				form.paymentTermsTable.connectToLocal();
				form.addPaymentTerms.setEnabled(true);
				form.addPayTerms.setEnabled(true);
			}
		}
		/*** Date 25-04-2019 by Vijay updated code with so below old code commented **/
		// Added by Rahul
//		if (event.getSource().equals(form.btstackDetails)) {
//			stackDetailsPanel = new PopupPanel(true);
//			reacttoStackDetails();
//			reactforStackDetailsTable();
//			// stackDetailsPopUp.getStackDetailsTable().setEnable(false);
//			stackDetailsPanel.add(stackDetailsPopUp);
//			stackDetailsPanel.show();
//			stackDetailsPanel.center();
//		}
//		if (event.getSource().equals(stackDetailsPopUp.addStack)) {
//			Console.log("Clicked on Add Stack");
//			reactOnAddStack();
//		}
//
//		if (event.getSource().equals(stackDetailsPopUp.getBtnOk())) {
//			reactOnStackDetailsOKBtn();
//		}
//
//		if (event.getSource().equals(stackDetailsPopUp.getBtnCancel())) {
//			reactOnStackDetailsCanBtn();
//		}

		if (event.getSource().equals(updatePricesPopUp.getBtnOk())) {
			reactOnUpdatePricesOKBtn();
		}

		if (event.getSource().equals(updatePricesPopUp.getBtnCancel())) {
			updatePricePanel.hide();
		}
		// Addition bY Rahul Ends Here

		

		/** added by rohan for communication log
		 */
		if(event.getSource() == CommunicationLogPopUp.getBtnOk()){
			form.showWaitSymbol();
		    List<InteractionType> list = CommunicationLogPopUp.getCommunicationLogTable().getDataprovider().getList();
		   
		    ArrayList<InteractionType> interactionlist = new ArrayList<InteractionType>();
		    interactionlist.addAll(list);
		    
		    boolean checkNewInteraction = AppUtility.checkNewInteractionAdded(interactionlist);
		    
		    if(checkNewInteraction==false){
		    	form.showDialogMessage("Please add new interaction details");
		    	form.hideWaitSymbol();
		    }	
		    else{	
		    	/**
		    	 *   Date : 06/05/2021 Added By Priyanka.
		    	 *   Des : need interaction due date copy into followup date on contract screen.
		    	 */
		       	final Date followUpDate = interactionlist.get(interactionlist.size()-1).getInteractionDueDate();
		    	 communicationService.saveCommunicationLog(interactionlist, new AsyncCallback<Void>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("Unexpected Error");
							form.hideWaitSymbol();
							LoginPresenter.communicationLogPanel.hide();
						}

						@Override
						public void onSuccess(Void result) {
							form.showDialogMessage("Data Save Successfully");
							form.hideWaitSymbol();
							model.setFollowUpDate(followUpDate);
							form.getDbconfollowupdate().setValue(followUpDate);
//							communicationPanel.hide();
							LoginPresenter.communicationLogPanel.hide();
						}
					});
		    }
		   
		}
		if(event.getSource() == CommunicationLogPopUp.getBtnCancel()){
//			communicationPanel.hide();
			LoginPresenter.communicationLogPanel.hide();
		}
		if(event.getSource() == CommunicationLogPopUp.getBtnAdd()){
				form.showWaitSymbol();
				String remark = CommunicationLogPopUp.getRemark().getValue();
				Date dueDate = CommunicationLogPopUp.getDueDate().getValue();
				String interactiongGroup =null;
				if(CommunicationLogPopUp.getOblinteractionGroup().getSelectedIndex()!=0)
					interactiongGroup=CommunicationLogPopUp.getOblinteractionGroup().getValue(CommunicationLogPopUp.getOblinteractionGroup().getSelectedIndex());
				boolean validationFlag = AppUtility.validateCommunicationlog(remark,dueDate);
				if(validationFlag){
					InteractionType communicationLog =  AppUtility.getCommunicationLog(AppConstants.SERVICEMODULE,AppConstants.CONTRACT,model.getCount(),model.getEmployee(),remark,dueDate,model.getCinfo(),null,interactiongGroup, model.getBranch(),"");
					CommunicationLogPopUp.getCommunicationLogTable().getDataprovider().getList().add(communicationLog);
					CommunicationLogPopUp.getRemark().setValue("");
					CommunicationLogPopUp.getDueDate().setValue(null);
					CommunicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
				}
				form.hideWaitSymbol();
		}
		/**ends here
		 */

		
		/** Date 13-09-2017 2017 added by vijay for customer Address ******/
		if(event.getSource()==custAddresspopup.getLblOk()){
			saveCustomerAddress();
		}
		if(event.getSource()==custAddresspopup.getLblCancel()){
			custAddresspopup.hidePopUp();
		}
		if(event.getSource()==custAddresspopup.getBtnbillingaddr()){
			custAddresspopup.getServiceAddressComposite().setValue(custAddresspopup.getBillingAddressComposite().getValue());
		}
//		if(event.getSource() instanceof InlineLabel){
//			InlineLabel lbl = (InlineLabel) event.getSource();
//			if(lbl.getText().equals("Edit")){
//				customerAddressEdiatable();
//			}
//		}
		
		if(event.getSource()==lblUpdate){
			System.out.println("EDITttttttttttttttttttttt");
			customerAddressEdiatable();
		}
		
		
		if(event.getSource()==lblSummary){
			multipleSRCopyPrintPopup.reactOnSummary("summary");
		}
		
		if(event.getSource()==lblChecklistReport){
//			Console.log("lblChecklistReport clicked");
//			form.showDialogMessage("Service status filter and excel format is not applicable for the checklist report. All services will be fetched.");			
//			multipleSRCopyPrintPopup.reactOnSummary("checklist");
			multipleSRCopyPrintPopup.hidePopUp();
		}
		
		/**
		 * Date : 06-11-2017 BY ANIL
		 */
		if(event.getSource().equals(uploadPopup.getBtnOk())){
			form.showWaitSymbol();
			model.setDocument(uploadPopup.getUpload().getValue());
			service.save(model, new AsyncCallback<ReturnFromServer>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
					panel.hide();
				}

				@Override
				public void onSuccess(ReturnFromServer result) {
					form.hideWaitSymbol();
					form.getUcUploadTAndCs().setValue(uploadPopup.getUpload().getValue());
					form.getUcUploadTAndCs().setEnable(false);
					form.showDialogMessage("Document updated sucessfully!");
					panel.hide();
					
				}
			});
			panel.hide();
		}
		
		if(event.getSource().equals(uploadPopup.getBtnCancel())){
			panel.hide();
		}
		/**
		 * End
		 */
		
		/** Date 02/11/2017 added by komal for new cancellation popup ok button and cancel button **/
		if(event.getSource() == cancellationPopup.getLblOk()){
			
			/*** Date 23-03-2019 by Vijay for NBHC CCPM with cancellation remark with configuration values and old code in else block****/
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableCancellationRemark")){
				if(cancellationPopup.getOlbCancellationRemark().getSelectedIndex()==0){
					form.showDialogMessage("Please enter valid remark to cancel contract..!");
				}
				else{
					reactOnCancelContractButton(cancellationPopup.getOlbCancellationRemark().getValue());
					cancellationPopup.hidePopUp();
				}
			} /*** ends here ****/
			else{
				if(cancellationPopup.getRemark().getValue().trim()!= null && !cancellationPopup.getRemark().getValue().trim().equals("")){
					//  react on contract cancel
					reactOnCancelContractButton(cancellationPopup.getRemark().getValue().trim());
					cancellationPopup.hidePopUp();
				}
				else{
					form.showDialogMessage("Please enter valid remark to cancel contract..!");
				}
			}
			
		}
        if(event.getSource() == cancellationPopup.getLblCancel()){
        	cancellationPopup.hidePopUp();
			cancellationPopup.remark.setValue("");
		}
        
        /**
         * nidhi
         * 9-06-2018
         */
    	if(nonRenewalMsgPopup != null && event.getSource().equals(nonRenewalMsgPopup.getLblOk())){
			reactOnRenewalOk();
		}
		if(nonRenewalMsgPopup != null && event.getSource().equals(nonRenewalMsgPopup.getLblCancel())){
			nonRenewalMsgPopup.hidePopUp();
		}
		
		/**
		 * Date 22-03-2019 by Vijay
		 * Des :- NBHC CCPM contract renewal quotation is mandatory
		 */
		if(event.getSource().equals(singledropdownpopup.getLblOk())){
			if(singledropdownpopup.getOlbQuotationId().getSelectedIndex()==0){
				form.showDialogMessage("Create valid quotation before you renew the contract!");
			}else{
				singledropdownpopup.hidePopUp();
				reactOnContractRenewal();
			}
		}
		if(event.getSource().equals(singledropdownpopup.getLblCancel())){
			singledropdownpopup.hidePopUp();
		}
		
		/**
		 * Date 26-03-2019 by Vijay
		 * Des :- NBHC CCPM Contract discontinue with Approval process
		 */
		if(contractDiscountinue !=null && event.getSource().equals(contractDiscountinue.getLblOk())){
			if(contractDiscountinue.getOlbApprovarName().getSelectedIndex()==0){
				form.showDialogMessage("Please select approver name!");
			}
			else if(contractDiscountinue.getOlbCancellationRemark().getSelectedIndex()==0){
				form.showDialogMessage("Please select remark!");
			}
			else{
				reactonContractDiscontinue();
				contractDiscountinue.hidePopUp();
			}
		}
		if(contractDiscountinue !=null && event.getSource().equals(contractDiscountinue.getLblCancel())){
			contractDiscountinue.hidePopUp();
		}
		/**
		 * ends here
		 */
		
		/**
		 * Date 25-04-2019 by Vijay Updated code for stack details for NBHC CCPM
		 */
		if(event.getSource().equals(form.btstackDetails)){
			stackDetailsPopUp.showPopUp();
			if(model.getStackDetailsList()!=null && model.getStackDetailsList().size()!=0)
			stackDetailsPopUp.getStackDetailsTable().getDataprovider().setList(model.getStackDetailsList());
			if(AppMemory.getAppMemory().currentState.toString().equals("NEW") ||
					AppMemory.getAppMemory().currentState.toString().equals("EDIT")){
				stackDetailsPopUp.setEnable(true);
			}else{
				stackDetailsPopUp.setEnable(false);
				stackDetailsPopUp.getLblOk().setVisible(false);
			}
		}
		if(event.getSource()==stackDetailsPopUp.getLblOk()){
			List<StackDetails> stackdetailslist = stackDetailsPopUp.getStackDetailsTable().getDataprovider().getList();
			if(stackdetailslist.size()!=0){
			ArrayList<StackDetails>arrayStackDetails = new ArrayList<StackDetails>();
			arrayStackDetails.addAll(stackdetailslist);
			model.setStackDetailsList(arrayStackDetails);
			}
			stackDetailsPopUp.hidePopUp();
		}
		if(event.getSource() == cancellation.getLblOk()){

			/**
			 * @author Anil @since 02-12-2021
			 * after enabling the remark drop down system was validating it with text field
			 * Raised by Ashwini for Pecop
			 */
//			if(cancellation.getOlbCancellationRemark().getSelectedIndex()==0){
//				form.showDialogMessage("Please enter valid remark to cancel contract..!");
//				return;
//			}
			
			//31-10-2023
			if(cancellation.getOlbCancellationRemark().getSelectedIndex()==0){
				form.showDialogMessage("Please enter valid remark to cancel contract..!");
				return;
			}
			
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableCancellationRemark")){
//				if(cancellation.getOlbCancellationRemark().getSelectedIndex()==0){
//					form.showDialogMessage("Please enter valid remark to cancel contract..!");
//				}
//			}else{
//				if(cancellationPopup.getRemark().getValue().trim()== null||cancellationPopup.getRemark().getValue().trim().equals("")){
//					form.showDialogMessage("Please enter valid remark to cancel contract..!");
//				}
//			}
			final String remark = cancellation.getOlbCancellationRemark().getValue(cancellation.getOlbCancellationRemark().getSelectedIndex());
			update.reactOnCancelContract(model, remark, LoginPresenter.loggedInUser, new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					cancellation.hidePopUp();
				}

				@Override
				public void onSuccess(Void result) {
					// TODO Auto-generated method stub
					if(!(model.getStatus().equals(Contract.CANCELLED))  && !contractDisContinue){
						model.setStatus(Contract.CANCELLED);
						form.getTbQuotationStatus().setValue(Contract.CANCELLED);
						form.getTaDescription().setValue(model.getDescription() + "\n" + "Contract Id ="
						+ model.getCount() + " "
						+ "Contract Status = "+model.getStatus()
						+ "\n"
						+ "has been cancelled by " + LoginPresenter.loggedInUser
						+ " with remark" + "\n" + "Remark ="
						+ remark);
						form.showDialogMessage("Contract Cancellation process started. It may take few minutes.");
						form.setToViewState();
						model.setRemark(remark);
						model.setCancellationDate(new Date());
						cancellation.hidePopUp();
						if(model.getStatus().equals(Contract.CANCELLED)) {
							Console.log("Cancellation Contract SMS");
							reactOnSendSMS();
						}
				}
				}
			});
		}
		if(event.getSource() == cancellation.getLblCancel()){
			cancellation.hidePopUp();
		}
		
		if(event.getSource()==accMgrPopup.getLblOk()){
			if(accMgrPopup.getOlbEmployee().getSelectedIndex()!=0){
				model.setAccountManager(accMgrPopup.getOlbEmployee().getValue());
				form.olbAccountManager.setValue(model.getAccountManager());
				accMgrPopup.hidePopUp();
				reactOnSave();
			}else{
				form.showDialogMessage("Please Select Account Manager.");
			}
		}
		
		if(event.getSource()==accMgrPopup.getLblCancel()){
			accMgrPopup.hidePopUp();
		}
		
		if(event.getSource() == form.chkStationedService){
			reactonStationTechnician();
		}
		
		if(event.getSource() == technicianPopup.getLblOk()){
			updatTechnicianName();
		}
		if(event.getSource() == technicianPopup.getLblCancel()){
			technicianPopup.hidePopUp();
		}
		if(event.getSource() == contractResetconditionPopup.getBtnOne()){
			updateContractReset();
		}
		if(event.getSource() == contractResetconditionPopup.getBtnTwo()){
			panel.hide();
		}
		
		if(event.getSource() == contractResetPopup.getLblOk()){
			reactonContractResetPopup();
		}
		if(event.getSource() == contractResetPopup.getLblCancel()){
			contractResetPopup.hidePopUp();
		}		

		if(event.getSource() == otpPopup.getBtnOne()){
			Console.log("otp popup ok button clicked");
			Console.log("otp value="+otpPopup.getOtp().getValue());
			if(otpPopup.getOtp().getValue()!=null){
				if(generatedOTP==otpPopup.getOtp().getValue()){
					runRegularDownloadProgram();
					otppopupPanel.hide();
				}else {
					form.showDialogMessage("Invalid OTP");	
					smsService.checkConfigAndSendWrongOTPAttemptMessage(model.getCompanyId(), LoginPresenter.loggedInUser, generatedOTP, "Contract", new  AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.showDialogMessage("Failed");
						}

						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							Console.log("InvalidOTPDownloadNotification result="+result);
						}
					} );
				
				}
			}else
				form.showDialogMessage("Enter OTP");
		}
		if(event.getSource() == otpPopup.getBtnTwo()){
			otppopupPanel.hide();
		}
		
		if(event.getSource().equals(infoPopup.getBtnOne())){
			CreateServices(model,false);
			infoPanel.hide();
		}
		if(event.getSource().equals(infoPopup.getBtnTwo())){
			infoPanel.hide();
		}
		if(event.getSource().equals(addExtraServicesconditionPopup.getBtnOne())){
			

			form.showWaitSymbol();
			update.createExtraServicesForCustomerBranches(null, model, LoginPresenter.loggedInUser, new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					form.showDialogMessage("Failed");
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(String result) {
					if(result!=null&&!result.equals("")) 
						form.showDialogMessage(result);
					else
						form.showDialogMessage("Failed");
					form.hideWaitSymbol();
					custBranchPanel.hide();
				}
			});
			
		}
		if(event.getSource().equals(addExtraServicesconditionPopup.getBtnTwo())){
			custBranchPanel.hide();
		}
		
		if(event.getSource().equals(descriptionPopup.getLblOk())) {


			Console.log("ok clicked on description popup");
			String log="";
			if(descriptionPopup.gettaDescription().getText()!=null) {
				log=LoginPresenter.loggedInUser+ " marked this contract as renewed on "+new Date()+" using 'mark contract as renewed' option with description: "+ descriptionPopup.gettaDescription().getText();
			}else
				log=LoginPresenter.loggedInUser+ " marked this contract as renewed on "+new Date()+" using 'mark contract as renewed' option";
			
			final String desc=log;
			
			conrenewalAsync.contractRenewalEntry(model, AppConstants.RENEWED, descriptionPopup.gettaDescription().getText(),new AsyncCallback<String>() {
					
					@Override
					public void onSuccess(String arg0) {
						Console.log("Contract renewal entity created");
						model.setRenewFlag(true);
						model.setUpdationLog(desc);
						model.setRenewProcessed("YES");
						genasync.save(model, new AsyncCallback<ReturnFromServer>() {
							
							@Override
							public void onSuccess(ReturnFromServer result) {
								Console.log("Contract entity updated");
								form.showDialogMessage("Contract successfully marked as renewed");	
								
								form.tbdonotrenew.setValue("Renewed");
							}
							
							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("Failed to update contract");	
							}
						});
						
					}
					
					@Override
					public void onFailure(Throwable arg0) {
						form.showDialogMessage("Failed to update");
					}
			});
			descriptionPopup.hidePopUp();
		}
		if(event.getSource().equals(descriptionPopup.getLblCancel())) {
			descriptionPopup.hidePopUp();
		}
	}

	

	

	
	

	private void updateContractReset() {

		form.showWaitSymbol();
		commonservice.updateContractDocuments(model.getCount(), model.getCompanyId(), false,
				true, false, true, new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}

					@Override
					public void onSuccess(String result) {
						form.hideWaitSymbol();
						if(result!=null && result.equalsIgnoreCase("success")){
							reactonResetContract();
						}
					}
				});
	}

	private void customerAddressEdiatable() {
		// TODO Auto-generated method stub
		custAddresspopup.getBillingAddressComposite().setEnable(true);
		custAddresspopup.getServiceAddressComposite().setEnable(true);
		
	}

	private void saveCustomerAddress() {
		model.setNewcustomerAddress(custAddresspopup.getBillingAddressComposite().getValue());
		model.setCustomerServiceAddress(custAddresspopup.getServiceAddressComposite().getValue());
//		genasync.save(model, new AsyncCallback<ReturnFromServer>() {
//			
//			@Override
//			public void onSuccess(ReturnFromServer result) {
//				// TODO Auto-generated method stub
//				form.showDialogMessage("Customer Address Updated Succesfully");
//				custAddresspopup.hidePopUp();
//				form.setToViewState();
//			}
//			
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				form.showDialogMessage("Unexpected Error");
//				custAddresspopup.hidePopUp();
//			}
//		});
		
		
		/**
		 * Date 01-06-2018
		 * Developer :- Vijay
		 * Des :- if services having multi-location then service address can not be updated from contract 
		 */
		boolean flag = false;
		
		if(model.getServiceScheduleList()!=null&&model.getServiceScheduleList().size()!=0){ // 11-12-2019 Deepak Salve Added this line
		for (int i = 0; i < model.getServiceScheduleList().size(); i++) {
			if(model.getServiceScheduleList().get(i).getScheduleProBranch()!=null&&model.getServiceScheduleList().size()!=0){
			if (!model.getServiceScheduleList().get(i).getScheduleProBranch().equalsIgnoreCase("Service Address")) {
				flag = true;
				break;
			}
		  }
		}
	}
		final boolean customerbranchservices = flag;
		
		/**
		 * end shere
		 */
		
		generalAsync.updatedServiceAddress(model,customerbranchservices, new AsyncCallback<Boolean>() {
			
			@Override
			public void onSuccess(Boolean result) {
				// TODO Auto-generated method stub
				// Vijay for multilocation services message flag for services address will not update
				if(customerbranchservices){
					form.showDialogMessage("Multi-Locations services Service Address can not be update in services");
				}else{
					form.showDialogMessage("Customer Address Updated Succesfully");
				}
				custAddresspopup.hidePopUp();
				form.setToViewState();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				custAddresspopup.hidePopUp();
			}
		});
		
	}

	private boolean validateRemark(){
		
		if(popup.getRemark().getValue().trim()!= null && !popup.getRemark().getValue().trim().equals("")){
			
			return true;
		}
		return false;
	}

	/**
	 * Description: This method will react when user clicks on OK Button of
	 * Update Price PopUp. Created By:Rahul Verma Created On: 15-10-2016 Created
	 * for : NBHC
	 * 
	 */
	private void reactOnUpdatePricesOKBtn() {
		// TODO Auto-generated method stub
		
		
		if(model.getEmployee().equalsIgnoreCase("Snehal Abhishek Palav")){
			
			Console.log("Clicked on Update Prices OK Button");
			List<SalesLineItem> salesLineItemList = new ArrayList<SalesLineItem>();

		for (int i = 0; i < updatePricesPopUp.productList.getDataprovider()
				.getList().size(); i++) {
			SalesLineItem item = new SalesLineItem();
			item.setPrduct(updatePricesPopUp.productList.getDataprovider()
					.getList().get(i).getPrduct());
			item.setProductSrNo(updatePricesPopUp.productList.getDataprovider()
					.getList().get(i).getSrNo());
			item.setProductCode(updatePricesPopUp.productList.getDataprovider()
					.getList().get(i).getProductCode());
			item.setProductName(updatePricesPopUp.productList.getDataprovider()
					.getList().get(i).getProductName());
			item.setDuration(updatePricesPopUp.productList.getDataprovider()
					.getList().get(i).getDuaration());
			item.setPrice(Math.round(updatePricesPopUp.productList
					.getDataprovider().getList().get(i).getProductNewPrice()));
			item.setVatTax(updatePricesPopUp.productList.getDataprovider()
					.getList().get(i).getVatTax());
			item.setServiceTax(updatePricesPopUp.productList.getDataprovider()
					.getList().get(i).getServiceTax());
			item.setVatTaxEdit(updatePricesPopUp.productList.getDataprovider()
					.getList().get(i).getVatTaxEdit());
			item.setServiceTaxEdit(updatePricesPopUp.productList
					.getDataprovider().getList().get(i).getServiceTaxEdit());
			if (updatePricesPopUp.productList.getDataprovider().getList()
					.get(i).getPremises() != null) {
				item.setPremisesDetails(updatePricesPopUp.productList
						.getDataprovider().getList().get(i).getPremises());
			} else {
				item.setPremisesDetails("");
			}
			/*
			 * The fields which i am not getting from Pop, so setting them the
			 * previous values only
			 */
			if (model.getItems().get(i).getPrduct().getCount() != 0) {
				item.getPrduct().setCount(
						model.getItems().get(i).getPrduct().getCount());
			}
			item.setStartDate(model.getItems().get(i).getStartDate());
			item.setNumberOfService(model.getItems().get(i)
					.getNumberOfServices());
			item.setQuantity(model.getItems().get(i).getQty());

			salesLineItemList.add(item);

		}

		final UpdatePricesServiceAsync updatePricesAsync = GWT
				.create(UpdatePricesService.class);

		updatePricesAsync.saveAfterPriceUpdateForContract_Billing(
				model.getCompanyId(), model.getCount(), salesLineItemList,
				new AsyncCallback<ArrayList<Integer>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onSuccess(ArrayList<Integer> result) {
						// TODO Auto-generated method stub
						if (result.contains(0)) {
							form.showDialogMessage("Failed to Update");
						} else if (result.contains(1)) {
							form.showDialogMessage("Updated Succesfully");
							/*
							 * This will update values in screen.
							 */
							Timer timer = new Timer() {

								@Override
								public void run() {
									// TODO Auto-generated method stub
									form.updateView(model);
								}
							};
							timer.schedule(3000);

						} else if (result.contains(-1)) {
							form.showDialogMessage("Unexpected Error Caused");
						}
					}

				});

		updatePricePanel.hide();
	}
		else{
			form.showDialogMessage("This contract is not created by an inward servce..");
		}
	}

	/**
	 * Description: This method handles view/edit state of form. Created by:
	 * Rahul Verma Created on: 15-10-2016 Created for: NBHC
	 * 
	 * @param null
	 * @return null
	 */
//	private void reacttoStackDetails() {
//		// TODO Auto-generated method stub
//
//		ScreeenState transactionStatus = AppMemory.getAppMemory().currentState;
//		if (transactionStatus.toString().equals("VIEW")) {
//			stackDetailsPopUp.getTbWareHouse().setEnabled(false);
//			stackDetailsPopUp.getTbStorageLocation().setEnabled(false);
//			stackDetailsPopUp.getTbStorageBin().setEnabled(false);
//			stackDetailsPopUp.getTbQuantity().setEnabled(false);
//			stackDetailsPopUp.getTbDescription().setEnabled(false);
//			stackDetailsPopUp.getAddStack().setEnabled(false);
//			stackDetailsPopUp.getStackDetailsTable().setEnable(true);
//		}
//		if (transactionStatus.toString().equals("EDIT")) {
//			stackDetailsPopUp.getTbWareHouse().setEnabled(true);
//			stackDetailsPopUp.getTbStorageLocation().setEnabled(true);
//			stackDetailsPopUp.getTbStorageBin().setEnabled(true);
//			stackDetailsPopUp.getTbQuantity().setEnabled(true);
//			stackDetailsPopUp.getTbDescription().setEnabled(true);
//			stackDetailsPopUp.getAddStack().setEnabled(true);
//			stackDetailsPopUp.getStackDetailsTable().setEnable(false);
//		}
//
//	}
//
//	/**
//	 * Ends
//	 */
//	private void reactOnStackDetailsCanBtn() {
//		// TODO Auto-generated method stub
//		stackDetailsPopUp.tbWareHouse.setValue("");
//		stackDetailsPopUp.tbStorageLocation.setValue("");
//		stackDetailsPopUp.tbStorageBin.setValue("");
//		stackDetailsPopUp.tbQuantity.setValue("");
//		stackDetailsPopUp.tbDescription.setValue("");
//		stackDetailsPopUp.stackDetailsTable.connectToLocal();
//		stackDetailsPanel.hide();
//	}
//
//	private void reactOnStackDetailsOKBtn() {
//		// TODO Auto-generated method stub
//		stackList = new ArrayList<StackDetails>();
//		for (int i = 0; i < stackDetailsPopUp.stackDetailsTable
//				.getDataprovider().getList().size(); i++) {
//			stackList.add(stackDetailsPopUp.stackDetailsTable.getDataprovider()
//					.getList().get(i));
//		}
//		model.setStackDetailsList(stackList);
////		stackDetailsPanel.hide();
//	}
//
//	/**
//	 * Description : This is for NBHC Only. Whenever they will click on
//	 * StackDetails they should get this value. Created on : 07/10/2016 Created
//	 * by: Rahul Verma
//	 */
//	private void reactforStackDetailsTable() {
//		// TODO Auto-generated method stub
//		// stackDetailsPopUp.tbWareHouse.setVisible(false);
//		// stackDetailsPopUp.tbStorageLocation.setVisible(false);
//		// stackDetailsPopUp.tbStorageBin.setVisible(false);
//		// stackDetailsPopUp.tbDescription.setVisible(false);
//		// stackDetailsPopUp.addStack.setVisible(false);
//		stackDetailsPopUp.stackDetailsTable.setValue(model
//				.getStackDetailsList());
//	}
//
//	/**
//	 * End
//	 */
//	/**
//	 * Description : This method will perform function when user clicks on ADD
//	 * button, this will data data in StackDetailsTable. Date : 09-09-2019
//	 * Project: Required For : This is used for NBHC WEbservice Input: Output:
//	 */
//	private void reactOnAddStack() {
//		// TODO Auto-generated method stub
//		String wareHouseStr = "";
//		String storageLocation = "";
//		String quantity = "";
//		String stackNum = "";
//		String description = "";
//
//		if (stackDetailsPopUp.tbWareHouse.getValue() != null
//				&& !(stackDetailsPopUp.tbWareHouse.getValue().equals(""))) {
//			wareHouseStr = stackDetailsPopUp.tbWareHouse.getValue().trim()
//					.toUpperCase();
//		}
//
//		if (stackDetailsPopUp.tbStorageLocation.getValue() != null
//				&& !(stackDetailsPopUp.tbStorageLocation.getValue().equals(""))) {
//			storageLocation = stackDetailsPopUp.tbStorageLocation.getValue()
//					.trim().toUpperCase();
//		}
//
//		if (stackDetailsPopUp.tbStorageBin.getValue() != null
//				&& !(stackDetailsPopUp.tbStorageBin.getValue().equals(""))) {
//			stackNum = stackDetailsPopUp.tbStorageBin.getValue().trim()
//					.toUpperCase();
//		}
//
//		if (stackDetailsPopUp.tbQuantity.getValue() != null
//				&& !(stackDetailsPopUp.tbQuantity.getValue().equals(""))) {
//			quantity = stackDetailsPopUp.tbQuantity.getValue().trim();
//		}
//
//		if (stackDetailsPopUp.tbDescription.getValue() != null
//				&& !(stackDetailsPopUp.tbDescription.getValue().equals(""))) {
//			description = stackDetailsPopUp.tbDescription.getValue().trim()
//					.toUpperCase();
//		}
//
//		/*
//		 * Adding details in StackDetails Table
//		 */
//		Console.log("After Adding Data");
//		Console.log("wareHouseStr" + wareHouseStr);
//		Console.log("storageLocation" + wareHouseStr);
//		Console.log("stackNum" + stackNum);
//		Console.log("quantity" + quantity);
//		Console.log("description" + description);
//
//		if (!wareHouseStr.equals("") && !storageLocation.equals("")
//				&& !stackNum.equals("")) {
//			Console.log("Inside Adding stack Details");
//			StackDetails stackDetails = new StackDetails();
//			stackDetails.setWareHouse(wareHouseStr);
//			stackDetails.setStorageLocation(storageLocation);
//			stackDetails.setStackNo(stackNum);
//			stackDetails.setQauntity(Double.parseDouble(quantity));
//			stackDetails.setDescription(description);
//			stackDetailsPopUp.stackDetailsTable.getDataprovider().getList()
//					.add(stackDetails);
//			clearStackDetailsFields();
//		}
//	}
//
//	/**
//	 * Description : This method will clear Fields in StackDetails. Date :
//	 * 09-09-2019 Project: Required For : This is used for NBHC Input: Output:
//	 */
//	private void clearStackDetailsFields() {
//		// TODO Auto-generated method stub
//		stackDetailsPopUp.tbWareHouse.setValue("");
//		stackDetailsPopUp.tbStorageLocation.setValue("");
//		stackDetailsPopUp.tbStorageBin.setValue("");
//		stackDetailsPopUp.tbQuantity.setValue("");
//		stackDetailsPopUp.tbDescription.setValue("");
//	}
//
//	/**
//	 * End
//	 */

	private boolean Validate() {
		boolean value = false;

		if (form.chkservicewithBilling.getValue() == true) {
			value = true;
		}

		return value;
	}

	private void CreateServices(Contract con,boolean validate) {
		form.showWaitSymbol();
		
		List<ProductInfo> popupproductList = productpopup.getProductTable().getDataprovider().getList();
		
		if(multipleBranchesSelectedFlag) {
			 popupproductList = productPopupForMultipleBranches.getProductTable().getDataprovider().getList();
			
		}		
		ArrayList<ProductInfo> productarraylist = new ArrayList<ProductInfo>();
		for (int i = 0; i < popupproductList.size(); i++) {
			
			/**
			 * Date : 30-10-2017 BY ANIL
			 * Only selected product should be addded to list
			 */
			if(popupproductList.get(i).isSelectedProduct()){
				ProductInfo prodInfo = new ProductInfo();
				prodInfo.setSelectedProduct(popupproductList.get(i).isSelectedProduct());
				prodInfo.setProductName(popupproductList.get(i).getProductName());
				prodInfo.setProductCode(popupproductList.get(i).getProductCode());
				prodInfo.setServiceDate(popupproductList.get(i).getServiceDate());
				prodInfo.setPrice(popupproductList.get(i).getPrice());
				prodInfo.setServiceTax(popupproductList.get(i).getServiceTax());
				prodInfo.setTotalAmount(popupproductList.get(i).getTotalAmount());
				prodInfo.setAreaprSqFeet(popupproductList.get(i).getAreaprSqFeet());
				prodInfo.setProductQty(popupproductList.get(i).getProductQty());
				prodInfo.setVatTax(popupproductList.get(i).getVatTax());
				prodInfo.setVatTaxEdit(popupproductList.get(i).getVatTaxEdit());
				prodInfo.setServiceTaxEdit(popupproductList.get(i).getServiceTaxEdit());
				/**
				 * Date : 30-10-2017 BY ANIL
				 */
				prodInfo.setSrNo(popupproductList.get(i).getSrNo());
				/**
				 * End
				 */
				
				/**
				 * Date 06/06/2018 By vijay
				 * for product warranty need to show in invoice
				 */
				if(popupproductList.get(i).getWarrantyPeriod()!=0)
					prodInfo.setWarrantyPeriod(popupproductList.get(i).getWarrantyPeriod());
				
				/**
				 * ends here
				 */
				productarraylist.add(prodInfo);
//				if(itemObj.getCustomerBranchSchedulingInfo()!=null){
//					ArrayList<BranchWiseScheduling> branchSchedulingList = itemObj.getCustomerBranchSchedulingInfo().get(itemObj.getProductSrNo());
				if(popupproductList.get(i).getProductQty()>1){
				multipleBranchesSelectedFlag=true;	
				}
			}
		}
		if(multipleBranchesSelectedFlag){
			Console.log("multipleBranchesSelectedFlag productarraylist size="+productarraylist.size());

			contractRateasyn.CreateServiceAndBillingForBranches(con, productarraylist,validate,new AsyncCallback<ArrayList<String>>() {
				@Override
				public void onSuccess(ArrayList<String> result) {
					Console.log("inside successs result size="+result.size());
					form.hideWaitSymbol();
					if (result.contains("1") && result.contains("2")) {
						form.showDialogMessage("Service And Billing Created Successfully");
					} 
//					else if (result.contains("0")) {
//						form.showDialogMessage("Service Created Successfully");
//					} else if (result.contains("1")) {
//						form.showDialogMessage("Billing Created Successfully");
//					}
					else if (result.contains("3")&& result.contains("4")) {
						form.showDialogMessage("Service and bill creation failed , Please contact support!");
					}else if (result.contains("3")) {
						form.showDialogMessage("Service creation failed , Please contact support!");
					}else if (result.contains("4")) {
						form.showDialogMessage("Bill creation failed , Please contact support!");
					}else {
						Console.log("in else");
						for(String s:result) {
							Console.log("in for s="+s);
							if(s.contains("AreaExceedMsg")) {
								s=s.replace("AreaExceedMsg", "");
								infoPopup.getInfo().setText(s);	
								infoPopup.getBtnOne().setText("Still Create Service & Bill");
								infoPopup.getInfo().setHeight("300px");
								infoPopup.getInfo().setWidth("450px");
								infoPopup.setSize("500px", "500px");
								infoPanel=new PopupPanel(true);
								infoPanel.add(infoPopup);
								infoPanel.center();
								infoPanel.show();
							}
						}
					}
						
				}
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}
			});
			
		}else{
		contractRateasyn.CreateServiceAndBilling(con, productarraylist,validate,new AsyncCallback<ArrayList<String>>() {
			@Override
			public void onSuccess(ArrayList<String> result) {
				Console.log("inside successs result size="+result.size());
				
				form.hideWaitSymbol();
				if (result.contains("1") && result.contains("2")) {
					form.showDialogMessage("Service And Billing Created Successfully");
				} 
//				else if (result.contains("0")) {
//					form.showDialogMessage("Service Created Successfully");
//				} else if (result.contains("1")) {
//					form.showDialogMessage("Billing Created Successfully");
//				}
				else if (result.contains("3")&& result.contains("4")) {
					form.showDialogMessage("Service and bill creation failed , Please contact support!");
				}else if (result.contains("3")) {
					form.showDialogMessage("Service creation failed , Please contact support!");
				}else if (result.contains("4")) {
					form.showDialogMessage("Bill creation failed , Please contact support!");
				}else {
					Console.log("in else");
					for(String s:result) {
						Console.log("in for s="+s);
						if(s.contains("AreaExceedMsg")){
							s=s.replace("AreaExceedMsg", "");
							infoPopup.getInfo().setText(s);	
							infoPopup.getBtnOne().setText("Still Create Service & Bill");
							infoPopup.getInfo().setHeight("300px");
							infoPopup.getInfo().setWidth("450px");
							infoPopup.setSize("500px", "500px");
							infoPanel=new PopupPanel(true);
							infoPanel.add(infoPopup);
							infoPanel.center();
							infoPanel.show();
						}
					}
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}
		});
		}

	}

	private ArrayList<ClientSideAsset> getAllSelectedRecords() {
		List<ClientSideAsset> listassettable = this.clientassetPopUp.assetTable
				.getDataprovider().getList();
		ArrayList<ClientSideAsset> selectedlist = new ArrayList<ClientSideAsset>();
		for (ClientSideAsset ass : listassettable) {
			if (ass.isRecordSelect() == true)
				selectedlist.add(ass);
		}
		return selectedlist;
	}

	// private void reactOnOk1() {
	// System.out.println("Inside of react on ok.....");
	//
	// genasync.save(model, new AsyncCallback<ReturnFromServer>() {
	//
	// @Override
	// public void onFailure(Throwable caught) {
	// form.hideWaitSymbol();
	// form.showDialogMessage("Unexpected Error Occured");
	// }
	//
	// @Override
	// public void onSuccess(ReturnFromServer result) {
	//
	// List<ClientSideAsset> assList = new ArrayList<ClientSideAsset>();
	// form.selectedlist = getAllSelectedRecords();
	//
	// if (form.selectedlist.size() != 0) {
	// System.out.println("SIZE OF SELECTED LIST:::::::::::::"
	// + form.selectedlist.size());
	// }
	// }
	//
	// });
	// }

	private void getclientsideasset() {
		System.out.println("INSIDE CLIENT ASSET POPUP!!!");
		assetPanel = new PopupPanel(true);
		assetPanel.add(clientassetPopUp);
		assetPanel.center();
		// clientassetPopUp.clear();
		assetPanel.show();

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);

		System.out.println("customer id"
				+ form.getPersonInfoComposite().getIdValue());
		filter = new Filter();
		filter.setQuerryString("personInfo.count");
		filter.setIntValue(form.getPersonInfoComposite().getIdValue());
		filtervec.add(filter);

		querry.setFilters(filtervec);
		querry.setQuerryObject(new ClientSideAsset());

		genasync.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
						form.showDialogMessage("Unexpected Error Occured!");
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						System.out.println("Size of List::::::::::"
								+ result.size());

						ArrayList<ClientSideAsset> clientList = new ArrayList<ClientSideAsset>();

						for (SuperModel model : result) {
							ClientSideAsset client = (ClientSideAsset) model;
							clientList.add(client);

							System.out
									.println("lis size before setting to table "
											+ clientList.size());

							for (int i = 0; i < clientList.size(); i++) {

								System.out.println("asset name "
										+ clientList.get(i).getName());
							}
						}

						clientassetPopUp.getAssetTable().getDataprovider()
								.setList(clientList);
					}
				});
	}

	private void reactToCancel() {
		model.setStatus(Contract.CONTRACTCANCEL);
		form.getTbQuotationStatus().setText(Contract.CONTRACTCANCEL);
		changeStatus("Failed To Cancel Contract !", "Contract Cancelled !");
		form.setAppHeaderBarAsPerStatus();
		form.setMenuAsPerStatus();
	}

	private void reactToViewServices() {
//		Filter filter = new Filter();
//		filter.setIntValue(model.getCount());
//		filter.setQuerryString("contractCount");
//		Vector<Filter> vecfilter = new Vector<Filter>();
//		vecfilter.add(filter);
//		MyQuerry query = new MyQuerry(vecfilter, new Service());
//		ServicePresenter.initalize(query);
		
		

		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(model.getCount());
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new Service());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No service document found.");
					return;
				}
				else{
					Console.log("in view services result size="+result.size());
					ServiceForm.rowcount=result.size(); //Ashwini Patil Date:7-09-2023 as count was showing zero when clicked on view services
					ServicePresenter.initalize(querry);
				}
			}
		});
		
	
		
		
	}

	private void reactToInvoice() {

	}

	private void reactToJobCard() {
		final String url = GWT.getModuleBaseURL() + "jobcardpdf" + "?Id="
				+ model.getId();
		Window.open(url, "test", "enabled");
	}

	private void reactToNew() {
		form.setToNewState();
		initalize();
	}

	private void upddateRefNoInServices(Date fromDate, Date toDate) {

		myasync.updateService(fromDate, toDate, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {

				form.showDialogMessage("Fail");
			}

			@Override
			public void onSuccess(Void result) {

				form.showDialogMessage("Entity Successfully updated");
			}

		});

	}

	private void changeStatus(final String failureMessage,
			final String successMessage) {
		async.changeStatus(model, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage(failureMessage);
			}

			@Override
			public void onSuccess(Void result) {
				form.showDialogMessage(successMessage);
			}
		});
	}

	@Override
	public void onRowCountChange(RowCountChangeEvent event) {
		NumberFormat nf = NumberFormat.getFormat("0.00");
		if (event.getSource().equals(form.getSaleslineitemtable().getTable())) {
			this.reactOnProductsChange();
			if (form.getSaleslineitemtable().getDataprovider().getList().size() == 0){
				form.getServiceScheduleTable().connectToLocal();
				serviceSchedulePopUp.getPopScheduleTable().connectToLocal();
			}

			// rohan added this method for product serial number
			// setProductSrNoMethod();

//			if (form.servicesFlag != true) {
				reactFordeleteproduct(form.getSaleslineitemtable()
						.identifyProductId(), form.getSaleslineitemtable()
						.identifySrNo());
//			}
//			form.servicesFlag = false;
		}

		if (event.getSource().equals(form.getChargesTable().getTable())) {
			if (form.getDoamtincltax().getValue() != null) {
				double newnetpay = form.getDoamtincltax().getValue()
						+ form.getChargesTable().calculateNetPayable();
				
				/**
				 * @author Vijay Date :- 18-01-2021 
				 * Des :- if below flag is active then round off not required for Innovative client done through process config
				 * below code execute for all but not applicable for innovative
				 */
				if(!LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
					newnetpay = Math.round(newnetpay);
					int netPayAmt = (int) newnetpay;
					newnetpay = netPayAmt;
				}
				
//				form.getDonetpayamt().setValue(
//				Double.parseDouble(nf.format(newnetpay)));
				
				/** Date 02-09-2017 added by vijay for discount amount net payable calculation with other charges ***/
				if(form.getDonetpayamt().getValue()==null || form.getDonetpayamt().getValue()==0 ){
					form.getDonetpayamt().setValue(newnetpay);
				}else{
					String roundOff = form.getTbroundOffAmt().getValue();
					double roundoffAmt = 0;
					if(!roundOff.equals(""))
					 roundoffAmt =  AppUtility.calculateRoundOff(roundOff, newnetpay);
					if(roundoffAmt!=0){
						form.getDonetpayamt().setValue(roundoffAmt);

					}else{
						form.getDonetpayamt().setValue(newnetpay);
						form.getTbroundOffAmt().setValue("");
					}
				}
				form.getDbGrandTotalAmt().setValue(newnetpay);
				
				
			}
		}

	}

	// private void setProductSrNoMethod() {
	//
	// if(form.getSaleslineitemtable().getDataprovider().getList().size()!=0){
	// List<SalesLineItem> tableList =
	// form.getSaleslineitemtable().getDataprovider().getList();
	// for (int i=0;i<tableList.size();i++)
	// {
	// tableList.get(i).setProductSrNo(i+1);
	// }
	//
	// form.getSaleslineitemtable().getDataprovider().setList(tableList);
	// }
	// }

	private void reactOnManageProject() {
		MyQuerry querry = new MyQuerry();
		Filter filter = new Filter();
		filter.setQuerryString("contractId");
		filter.setIntValue(model.getCount());
		querry.setQuerryObject(new ServiceProject());
		querry.getFilters().add(filter);
		ProjectForm form = ProjectPresenter.initalize(querry);
		ServiceProject project = new ServiceProject();
		project.setserviceId(model.getCount());
		project.setserviceEngineer(model.getEmployee());
		project.setServiceStatus(model.getStatus());

		project.setContractId(model.getContractCount());
		project.setContractStartDate(model.getStartDate());
		project.setContractEndDate(model.getEndDate());
		project.setPersonInfo(model.getCinfo());
		project.setContractId(model.getCount());
		form.updateView(project);
		form.serviceId.setEnabled(true);

		filter = new Filter();
		filter.setQuerryString("contractId");
		filter.setIntValue(model.getContractCount());
		querry = new MyQuerry();
		querry.setQuerryObject(new ClientSideAsset());
		querry.getFilters().add(filter);
		form.olbClientSideAsset.MakeLive(querry);
	}

	private double fillNetPayable(double amtincltax) {
		double amtval = 0;
		if (form.getChargesTable().getDataprovider().getList().size() == 0) {
			amtval = amtincltax;
		}
		if (form.getChargesTable().getDataprovider().getList().size() != 0) {
			amtval = amtincltax + form.getChargesTable().calculateNetPayable();
		}
		/**
		 * @author Vijay Date :- 18-01-2021 
		 * Des :- if below flag is active then round off not required for Innovative client done through process config
		 * below code execute for all but not applicable for innovative
		 */
		if(LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
			return amtval;
		}
		/**
		 * ends here
		 */
		amtval = Math.round(amtval);
		int retAmtVal = (int) amtval;
		amtval = retAmtVal;
		return amtval;
	}

	private void reactOnProductsChange() {
		form.showWaitSymbol();
		Timer timer = new Timer() {
			@Override
			public void run() {
				NumberFormat nf = NumberFormat.getFormat("0.00");
				double totalExcludingTax = form.getSaleslineitemtable().calculateTotalExcludingTax();
				totalExcludingTax = Double.parseDouble(nf.format(totalExcludingTax));
				form.getDototalamt().setValue(totalExcludingTax);

				boolean chkSize = form.getSaleslineitemtable().removeChargesOnDelete();
				if (chkSize == false) {
					form.getChargesTable().connectToLocal();
				}
				
				 /**
			     * Date 01-09-2017 added by vijay for final total amount after discount
			     */
			    if(form.getDbDiscountAmt().getValue()!=null && form.getDbDiscountAmt().getValue()!=0){
		    		if(form.getDbDiscountAmt().getValue()>totalExcludingTax){
			    		form.getDbDiscountAmt().setValue(0d);
		    		}
		    		else if(form.getDbDiscountAmt().getValue()<totalExcludingTax){
				    	form.getDbFinalTotalAmt().setValue(totalExcludingTax - form.getDbDiscountAmt().getValue());
		    		}
		    		else{
			    		form.getDbFinalTotalAmt().setValue(totalExcludingTax);
		    		}
		    	}
			    else{
			    	form.getDbFinalTotalAmt().setValue(totalExcludingTax);
		    	}
			    
			    /**
			     * ends here
			     */

				try {
					form.prodTaxTable.connectToLocal();
					form.addProdTaxes();
//					double totalIncludingTax = form.getDototalamt().getValue()+ form.getProdTaxTable().calculateTotalTaxes();

					/** Date 01-09-2017 above old code commented and new code below line added by vijay for final total amount after discount **/
					double totalIncludingTax=form.getDbFinalTotalAmt().getValue()+form.getProdTaxTable().calculateTotalTaxes();
					
					/***
					 * @author Vijay Date : 19-01-2021
					 * Des :- Using number format if number id 10.09 then its showing 10.10 which is wrong
					 * so updated below manual logic to show accurate Amt for innovative 
					 */
					if(LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
						String includingTaxAmt = totalIncludingTax+"";
						System.out.println("includingTaxAmt "+includingTaxAmt);
						if(includingTaxAmt.contains(".")){
							String [] includingTaxAmtString = includingTaxAmt.split("\\.");
							String firstDecimal = includingTaxAmtString[0];
							String secondDecimal = includingTaxAmtString[1];
							System.out.println("firstDecimal = "+firstDecimal);
							System.out.println("secondDecimal = "+secondDecimal);

							String twoDecimal = null;
							for(int i=0;i<secondDecimal.length();i++){
								if(i==0){
									twoDecimal = secondDecimal.charAt(i)+"";
								}
								else{
									twoDecimal += secondDecimal.charAt(i)+"";
								}
								if(i==1){
									break;
								}
							}
							System.out.println("twoDecimal "+twoDecimal);
							if(twoDecimal!=null){
								firstDecimal = includingTaxAmtString[0] +"."+twoDecimal;
								System.out.println("firstDecimal "+firstDecimal);
								form.getDoamtincltax().setValue(Double.parseDouble(firstDecimal));

							}
							else{
								form.getDoamtincltax().setValue(Double.parseDouble(nf.format(totalIncludingTax)));
							}
						}
						else{
							form.getDoamtincltax().setValue(Double.parseDouble(nf.format(totalIncludingTax)));
						}
					}
						
					else{
						form.getDoamtincltax().setValue(Double.parseDouble(nf.format(totalIncludingTax)));

					}
					
					
//					form.getDoamtincltax().setValue(Double.parseDouble(nf.format(totalIncludingTax)));
					form.updateChargesTable();
				} catch (Exception e) {
					e.printStackTrace();
				}
				double netPay = fillNetPayable(form.getDoamtincltax().getValue());
				/**
				 * @author Vijay Date :- 18-01-2021 
				 * Des :- if below flag is active then round off not required for Innovative client done through process config
				 * below code execute for all but not applicable for innovative
				 */
				if(!LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
					netPay = Math.round(netPay);
					int netPayable = (int) netPay;
					netPay = netPayable;
				}
				
//				netPay = Math.round(netPay);
//				int netPayable = (int) netPay;
//				netPay = netPayable;
////				form.getDonetpayamt().setValue(Double.parseDouble(nf.format(netPay)));

				/** Date 01-09-2017 added by vijay for final netpayable amount after discount **/
				form.getDbGrandTotalAmt().setValue(netPay);
				if(!form.getTbroundOffAmt().getValue().equals("") && form.getTbroundOffAmt().getValue()!=null){
					String roundOff = form.getTbroundOffAmt().getValue();
					double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
					if(roundoffAmt!=0){
						form.getDonetpayamt().setValue(roundoffAmt);
					}else{
						form.getDonetpayamt().setValue(roundoffAmt);
						form.getTbroundOffAmt().setValue("");
					}
				}
				else{
					form.getDonetpayamt().setValue(netPay);
				}
				if(form.getSaleslineitemtable().getDataprovider().getList().size()==0){
					form.getDbDiscountAmt().setValue(0d);
					form.getTbroundOffAmt().setValue(0d+"");
				}
				/** ends here *******/
				
				String approverName = "";
				if (form.olbApproverName.getValue() != null) {
					approverName = form.olbApproverName.getValue();
				}
				AppUtility.getApproverListAsPerTotalAmount(form.olbApproverName, "Contract", approverName, form
								.getDonetpayamt().getValue());
				form.hideWaitSymbol();
			}
		};
		timer.schedule(3000);
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		if (event.getSource().equals(form.getPersonInfoComposite().getId())
				|| event.getSource().equals(
						form.getPersonInfoComposite().getName())
				|| event.getSource().equals(
						form.getPersonInfoComposite().getPhone())) {
			form.checkCustomerStatus(form.getPersonInfoComposite().getIdValue(),true,true);
			form.checkCustomerBranch(form.personInfoComposite.getIdValue());
			custcount = form.getPersonInfoComposite().getIdValue();
			
			/**
			 *  nidhi
			 *  29-01-2018
			 *  for multiple contact list in drop down
			 */
			int custcountno = form.getPersonInfoComposite().getIdValue();
			form.getCustomerContactList(custcountno,null);
			/**
			 *  end
			 */
			/**
			 * Date 31-01-2019 by Vijay for NBHC CCPM with process config
			 * if lead and quotation id is not in the contract then  should not allow to create Contract
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "MakeLeadAndQuotationIdMandatory")
				&&  !LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Admin")	){
				if(form.olbContractGroup.getSelectedIndex()!=0 && !form.olbContractGroup.getValue(form.olbContractGroup.getSelectedIndex()).equals("In-House") &&
						!form.olbContractGroup.getValue(form.olbContractGroup.getSelectedIndex()).equals("In-House (On Site)")){
				if(form.tbLeadId.getValue().equals("") || form.tbQuotatinId.getValue().equals("")){
					form.showDialogMessage("Lead and Quotation id is mandatory! Please create Lead and Quotation first!");
					form.getPersonInfoComposite().clear();
					return;
				}
//				if(form.tbQuotatinId.getValue().equals("")){
//					form.showDialogMessage("Quotation id is mandatory! Please create Lead first!");
//					form.getPersonInfoComposite().clear();
//				}
				
				}
			}
			/**
			 * ends here
			 */
		}
	}

	private void getMINFromContractID() {
		final Company c = new Company();
		consumptionList.clear();
		consumptionAsync.retrieveMinList(c.getCompanyId(),
				Integer.parseInt(form.getTbContractId().getValue()),
				new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An unexpected error occurred. Please try again!");
					}

					@Override
					public void onSuccess(
							ArrayList<MaterialConsumptionReport> result) {
						if (result.size() == 0) {
							form.showDialogMessage("Material is not issued yet!");
						} else {
							consumptionList.addAll(result);

							consumptionAsync.retrieveMmnList(
									c.getCompanyId(),
									Integer.parseInt(form.getTbContractId()
											.getValue()),
									new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

										@Override
										public void onFailure(Throwable caught) {
											form.showDialogMessage("An unexpected error occurred. Please try again!");
										}

										@Override
										public void onSuccess(
												ArrayList<MaterialConsumptionReport> result) {
											if (result.size() != 0) {
												consumptionList.addAll(result);

											}
											if (consumptionList.size() != 0) {
												AppMemory
														.getAppMemory()
														.setCurrentScreenandChangeProcessName(
																"Inventory/Material Consumption Report",
																Screen.MATERIALCONSUMPTIONREPORT);
												final MaterialConsumptionReportForm form = MaterialConsumptionReportPresenter
														.initialize();

												form.getContractCount()
														.addItem(
																model.getCount()
																		+ "");
												form.getContractStatus()
														.setValue(
																model.getStatus());
												form.getStartDate()
														.setValue(
																AppUtility
																		.parseDate(model
																				.getStartDate()));
												form.getEndDate()
														.setValue(
																AppUtility
																		.parseDate(model
																				.getEndDate()));
												form.getPersonInfoComposite()
														.setValue(
																model.getCinfo());
												form.getTable()
														.getDataprovider()
														.setList(
																consumptionList);
												double sum = 0;
												double totVal = 0;

												for (int f = 0; f < consumptionList
														.size(); f++) {
													if (consumptionList.get(f)
															.getDocType()
															.trim()
															.equals("MMN")) {
														totVal = -(consumptionList
																.get(f)
																.getPrice() * consumptionList
																.get(f)
																.getQuantity());
													} else {
														totVal = consumptionList
																.get(f)
																.getPrice()
																* consumptionList
																		.get(f)
																		.getQuantity();
													}
													sum = sum + totVal;
												}
												form.getNetAmt().setValue(sum);
											}
										}
									});
						}
					}
				});

	}

	private void reactOnContractRenewal() {
		
		/**
		 * @author Anil , Date : 17-04-2020
		 * in case of large service contract we are storing scheduling list on different entity
		 * in that case we will get zero scheduling list size and system will not allow to renew the contract
		 */
		 List<SalesLineItem>itemList=null;
		if(model.getServiceScheduleList().size()==0){
			Console.log("4b- Old Contract 1- "+form.getServiceScheduleTable().getValue().size());
			model.setServiceScheduleList(form.getServiceScheduleTable().getValue());
			Console.log("4b- Old Contract 2- "+model.getServiceScheduleList().size());
		}
		
		Console.log("1-Inside Contract Renewal");
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contract", Screen.CONTRACT);
		final ContractForm form1 = ContractPresenter.initalize();
		
		/**
		 * Date : 12-05-2017 By ANIl
		 */
		form1.checkCustomerBranch(model.getCinfo().getCount());
		/**
		 * End
		 */
		
		/**
		 * Date : 19-07-2017 By ANIl
		 */
		form1.checkCustomerStatus(model.getCinfo().getCount(),true,true);
		/**
		 * End
		 */
		
		form1.showWaitSymbol();
		Timer t = new Timer() {
			@Override
			public void run() {
				String response=AppUtility.validateProductCode(model.getItems());
				
				
				form1.setToNewState();
				form1.hideWaitSymbol();
				form1.getTbContractId().setText("");
				form1.getOlbContractCategory().setValue(model.getCategory());
				/**
		    	   * nidhi
		    	   * 16-01-2018
		    	   * unchecked all check box by default 
		    	   */
		    	  form1.getCbRenewFlag().setValue(false);
		    	  form1.getCbServiceWiseBilling().setValue(false);
		    	  form1.getChkbillingatBranch().setValue(false);
		    	  form1.getChkservicewithBilling().setValue(false);
		    	  form1.getCbStartOfPeriod().setValue(false);
		    	  form1.getCbConsolidatePrice().setValue(false);
		    	  /***
		    	   *  end
		    	   */
				/**
				 * nidhi
				 * 16-01-2018
				 * for map segment ref number and contract type 
				 */
				form1.getOlbContractCategory().setValue(model.getCategory());
				if (model.getRefNo() != null) {
					form1.getTbReferenceNumber().setValue(model.getRefNo());
				} else {
					form1.getTbReferenceNumber().setText("");
				}
				form1.getTbCustSegment().setValue(model.getSegment());
				if((form1.getTbReferenceNumber().getValue()== null || form1.getTbReferenceNumber().getValue().trim()=="") ||
		    			  (form1.getTbCustSegment().getValue()== null || form1.getTbCustSegment().getValue().trim()=="") ||
		    			  (form1.getOlbContractType().getValue()== null || form1.getOlbContractType().getValue().trim()=="")){
		    		  /**
			    		  * Date:10-07-2017 BY NIDHI
			    		  */
			    		 if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC")){
			    			 form1.checkCustomerStatus(model.getCustomerId(), true, false);
			    		 }
			    		 
			    		
			    		 /**
			    		  * End
			    		  */
		    	  }
				 if(form1.getOlbContractCategory().getSelectedIndex()!=0){
		 				ConfigCategory cat=form1.getOlbContractCategory().getSelectedItem();
						if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableRenewalContractTypeDefault")){
//							form.showDialogMessage("category -- " +cat.getCategoryName() );
			 				if(cat!=null){
			 					AppUtility.makeLiveTypeDropDown(form1.getOlbContractType(), cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
			 					 form1.getOlbContractType().setValue(model.getType());
			 				}
						}

		 			}
				/**
				 * 
				 *  end
				 */
				 form1.getCustomerContactList(model.getCinfo().getCount(),model.getPocName());
				 form1.getObjContactList().setValue(model.getPocName());
				 
				if (model.getLeadCount() == -1) {
					form1.getTbLeadId().setText("");
				}
				form1.getPersonInfoComposite().setEnabled(false);
				if (model.getRefNo() != null) {
					form1.getTbReferenceNumber().setValue(model.getRefNo());
				} else {
					form1.getTbReferenceNumber().setText("");
				}

				if (model.getReferedBy() != "") {
					form1.getTbRefeRRedBy().setValue(model.getReferedBy());
				} else {
					form1.getTbRefeRRedBy().setText("");
				}

				if (model.getRefDate() != null) {
					form1.getDbrefernceDate().setValue(model.getRefDate());
				} else {
					form1.getDbrefernceDate().setValue(null);
				}

				form1.getPersonInfoComposite().setValue(model.getCinfo());

				// rohan added this code for setting customer branches in
				// schedule popup
				ContractPresenter.custcount = model.getCinfo().getCount();
				// ends here

				form1.getOlbbBranch().setValue(model.getBranch());
				form1.getOlbApproverName().setValue(model.getApproverName());
			  /**
	    	   * @author Vijay Date :- 27-10-2020
	    	   * Des  Bug :- when contract renew it should not map lead id in new renewed contract
	    	   */
//				if (model.getLeadCount() != -1) {
//					form1.getTbLeadId().setValue(model.getLeadCount() + "");
//				}
				
				/**
				 * Commented old code
				 * By Anil Date : 13-05-2017
				 */
//				if (model.getContractCount() != -1) {
//					form.getTbContractId().setValue(model.getContractCount() + "");
//				}
				form1.getIbOldContract().setValue(model.getCount() + "");
				if (model.getQuotationCount() != -1) {
					form1.getTbQuotatinId().setValue(model.getQuotationCount() + "");
				}
				form1.getOlbcPaymentMethods().setValue(model.getPaymentMethod());
				form1.getOlbeSalesPerson().setValue(model.getEmployee());
				form1.getOlbContractGroup().setValue(model.getGroup());
				form1.getOlbContractCategory().setValue(model.getCategory());
				form1.getOlbContractType().setValue(model.getType());

				if(response.equals("")) {
				form1.getProdTaxTable().setValue(model.getProductTaxes());
				form1.getChargesTable().setValue(model.getProductCharges());
				}
				/**
				 * Updated By: Viraj
				 * Date: 18-01-2019
				 * Description: to not map payment terms
				 */
//				form.getPaymentTermsTable().setValue(model.getPaymentTermsList());

				// rohan added this for setting flag value
				form1.chkbillingatBranch.setValue(false);
				form1.chkservicewithBilling.setValue(false);

				
				
				if(response.equals("")) {
				ArrayList<SalesLineItem> arraylis = new ArrayList<SalesLineItem>();
				List<SalesLineItem> listProduct = new ArrayList<SalesLineItem>();
				listProduct.addAll(model.getItems());
				System.out.println("listproduct size===" + listProduct.size());

				for (int i = 0; i < listProduct.size(); i++) {
					
						
						SalesLineItem sales = new SalesLineItem();
						sales.setPrduct(listProduct.get(i).getPrduct());
						CalendarUtil.addDaysToDate(listProduct.get(i).getStartDate(),listProduct.get(i).getDuration() + 1);
						
						/**
						 * @author Anil
						 * @since 10-06-2020
						 * contract data get corrupt at the time of renwal becuase of product master document upload
						 */
						sales.setTermsAndConditions(new DocumentUpload());
						sales.setProductImage(new DocumentUpload());
						sales.getPrduct().setProductImage1(new DocumentUpload());
						sales.getPrduct().setProductImage2(new DocumentUpload());
						sales.getPrduct().setProductImage3(new DocumentUpload());
						sales.getPrduct().setComment("");
						sales.getPrduct().setCommentdesc("");
						sales.getPrduct().setCommentdesc1("");
						sales.getPrduct().setCommentdesc2("");
						
						
						/**
						 * Date : 09-06-2017 By ANIL
						 * PCAMB RENEWAL ERROR FOR NO. OF BRANCH ERROR
						 */
						//  rohan added this code for sessting service sr no while renew contract
						if(listProduct.get(i).getProductSrNo()==0){
							sales.setProductSrNo(i+1);
						}else{
							sales.setProductSrNo(listProduct.get(i).getProductSrNo());
						}
						
						/**
						 * End
						 */
						sales.setStartDate(listProduct.get(i).getStartDate());
						sales.setQuantity(listProduct.get(i).getQty());
						sales.setDuration(listProduct.get(i).getDuration());
						sales.setNumberOfService(listProduct.get(i).getNumberOfServices());
						sales.setPrice(listProduct.get(i).getPrice());
						sales.setVatTax(listProduct.get(i).getVatTax());
						sales.setServiceTax(listProduct.get(i).getServiceTax());
						sales.setPercentageDiscount(listProduct.get(i).getPercentageDiscount());
						sales.setServicingTime(listProduct.get(i).getServicingTIme());
						
						/**
						 * Date:16-06-2017 BY ANIL
						 */
						sales.setRemark(listProduct.get(i).getRemark());
						/**
						 * End
						 */
						
						/**
						 * Date:23/06/2018 BY Vijay
						 */
						if(listProduct.get(i).getArea()!=null && !listProduct.get(i).getArea().equals(""))
							sales.setArea(listProduct.get(i).getArea());
						/**
						 * ends here
						 */
						
						/**
						 * Date : 13-05-2017 By ANIL
						 */
						sales.setBranchSchedulingInfo(listProduct.get(i).getBranchSchedulingInfo());
						sales.setBranchSchedulingInfo1(listProduct.get(i).getBranchSchedulingInfo1());
						sales.setBranchSchedulingInfo2(listProduct.get(i).getBranchSchedulingInfo2());
						sales.setBranchSchedulingInfo3(listProduct.get(i).getBranchSchedulingInfo3());
						sales.setBranchSchedulingInfo4(listProduct.get(i).getBranchSchedulingInfo4());
						/**
						 * End
						 */

						
						/**
						 * Date : 12-06-2017 By ANIL
						 */
						sales.setBranchSchedulingInfo5(listProduct.get(i).getBranchSchedulingInfo5());
						sales.setBranchSchedulingInfo6(listProduct.get(i).getBranchSchedulingInfo6());
						sales.setBranchSchedulingInfo7(listProduct.get(i).getBranchSchedulingInfo7());
						sales.setBranchSchedulingInfo8(listProduct.get(i).getBranchSchedulingInfo8());
						sales.setBranchSchedulingInfo9(listProduct.get(i).getBranchSchedulingInfo9());
						/**
						 * End
						 */
						
						/**
						 * Date 02-05-2018 
						 * Developer : Vijay
						 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
						 * so i have updated below code using hashmap. So old contract i will get the branches from string and new contract from hashmpa if branches exist 
						 */
						if(listProduct.get(i).getCustomerBranchSchedulingInfo()!=null){
							sales.setCustomerBranchSchedulingInfo(listProduct.get(i).getCustomerBranchSchedulingInfo());
						}else{
							
							/**
							 * Date 21-07-2018 By Vijay
							 * Des :- Old contract updated through customerBranch hashmap new
							 */
							
							String[] branchArray=new String[10];
							if(listProduct.get(i).getBranchSchedulingInfo()!=null
									&&!listProduct.get(i).getBranchSchedulingInfo().equals("")){
								branchArray[0]=listProduct.get(i).getBranchSchedulingInfo();
							}
							if(listProduct.get(i).getBranchSchedulingInfo1()!=null
									&&!listProduct.get(i).getBranchSchedulingInfo1().equals("")){
								branchArray[1]=listProduct.get(i).getBranchSchedulingInfo1();
							}
							if(listProduct.get(i).getBranchSchedulingInfo2()!=null
									&&!listProduct.get(i).getBranchSchedulingInfo2().equals("")){
								branchArray[2]=listProduct.get(i).getBranchSchedulingInfo2();
							}
							if(listProduct.get(i).getBranchSchedulingInfo3()!=null
									&&!listProduct.get(i).getBranchSchedulingInfo3().equals("")){
								branchArray[3]=listProduct.get(i).getBranchSchedulingInfo3();
							}
							if(listProduct.get(i).getBranchSchedulingInfo4()!=null
									&&!listProduct.get(i).getBranchSchedulingInfo4().equals("")){
								branchArray[4]=listProduct.get(i).getBranchSchedulingInfo4();
							}
							
							if(listProduct.get(i).getBranchSchedulingInfo5()!=null
									&&!listProduct.get(i).getBranchSchedulingInfo5().equals("")){
								branchArray[5]=listProduct.get(i).getBranchSchedulingInfo5();
							}
							if(listProduct.get(i).getBranchSchedulingInfo6()!=null
									&&!listProduct.get(i).getBranchSchedulingInfo6().equals("")){
								branchArray[6]=listProduct.get(i).getBranchSchedulingInfo6();
							}
							if(listProduct.get(i).getBranchSchedulingInfo7()!=null
									&&!listProduct.get(i).getBranchSchedulingInfo7().equals("")){
								branchArray[7]=listProduct.get(i).getBranchSchedulingInfo7();
							}
							if(listProduct.get(i).getBranchSchedulingInfo8()!=null
									&&!listProduct.get(i).getBranchSchedulingInfo8().equals("")){
								branchArray[8]=listProduct.get(i).getBranchSchedulingInfo8();
							}
							if(listProduct.get(i).getBranchSchedulingInfo9()!=null
									&&!listProduct.get(i).getBranchSchedulingInfo9().equals("")){
								branchArray[9]=listProduct.get(i).getBranchSchedulingInfo9();
							}
							
							ArrayList<BranchWiseScheduling> branchSchedulingList=null;
							if(listProduct.get(i).getBranchSchedulingInfo()!=null
									&&!listProduct.get(i).getBranchSchedulingInfo().equals("")){
								branchSchedulingList = AppUtility.getBranchSchedulingList(branchArray);
							}else{
								if(listProduct.get(i).getCustomerBranchSchedulingInfo()!=null){
								branchSchedulingList = listProduct.get(i).getCustomerBranchSchedulingInfo().get(listProduct.get(i).getProductSrNo());
								}

							}
							if(branchSchedulingList!=null&&branchSchedulingList.size()!=0){
								String branchAray[]=AppUtility.getCustomersUpdatedBranchSchedulingList(branchSchedulingList);
								if(branchAray.length>0){
									ArrayList<BranchWiseScheduling> branchlist = AppUtility.getBranchSchedulingList(branchAray);
									Integer prodSrNo = listProduct.get(i).getProductSrNo();
									HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
									customerBranchlist.put(prodSrNo, branchlist);
									sales.setCustomerBranchSchedulingInfo(customerBranchlist);
							
								}
							}
						
						}
						/**
						 * ends here
						 */
						/** date 11.12.2018 added by komal for nbhc service branches button and popup **/
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableServiceBranch")){
							if(listProduct.get(i).getServiceBranchesInfo() != null && listProduct.get(i).getServiceBranchesInfo().size() > 0){
								sales.setServiceBranchesInfo(listProduct.get(i).getServiceBranchesInfo());
							}else{
								ArrayList<BranchWiseScheduling> branchArrayList = new ArrayList<BranchWiseScheduling>();
								for(Branch branch : LoginPresenter.customerScreenBranchList){
									BranchWiseScheduling branchWiseScheduling = new BranchWiseScheduling();
									branchWiseScheduling.setBranchName(branch.getBusinessUnitName());
									branchWiseScheduling.setArea(0);
									branchWiseScheduling.setCheck(false);
									branchArrayList.add(branchWiseScheduling);
								}
								HashMap<Integer, ArrayList<BranchWiseScheduling>> serviceBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
								serviceBranchlist.put(sales.getProductSrNo(), branchArrayList);
								sales.setServiceBranchesInfo(serviceBranchlist);
							}
						}
						
						/**
						 * end komal
						 */
						arraylis.add(sales);
					
					
				}
				
				
				
				Console.log("2-Line item size:  "+arraylis.size());
				 /**
		    	   * Date : 12-05-2017 By Anil
		    	   * This method update itemlist by adding branch info at product level.
		    	   */
				if(model.getItems().get(0).getBranchSchedulingInfo()!=null&&!model.getItems().get(0).getBranchSchedulingInfo().equals("")){
					Console.log("3- New and Updated Contract ");
					form1.getSaleslineitemtable().setValue(arraylis);
					List<ServiceSchedule> popuplist = form1.reactfordefaultTableOnRenewal(arraylis);
					form1.getServiceScheduleTable().getDataprovider().setList(popuplist);
				}else{
					Console.log("4- Old Contract ");
		    	  List<SalesLineItem>itemList=AppUtility.getItemList(arraylis, model.getServiceScheduleList(),form.customerbranchlist,model.getBranch());
		    	  if(itemList!=null){
		    		  form1.getSaleslineitemtable().setValue(itemList);
		    		  ArrayList<SalesLineItem> items = new ArrayList<SalesLineItem>(itemList);
		    		  List<ServiceSchedule> popuplist = form1.reactfordefaultTableOnRenewal(items);
					  form1.getServiceScheduleTable().getDataprovider().setList(popuplist);
		    	  }else{
		    		  form1.showDialogMessage("No. of branches at product level and no. of selected branches at service scheduling popup are different.");
		    	  }
				}
				/**
	    	    * End
	    	    */
				form1.getDototalamt().setValue(model.getTotalAmount());
				form1.getDonetpayamt().setValue(model.getNetpayable());
				}
				else {
					form.showDialogMessage(response);
				}
				/**
				 * Date : 14-12-2017 BY ANIL
				 */
				form1.getCbServiceWiseBilling().setValue(false);
				form1.cbStartOfPeriod.setValue(false);
				/**
				 * nidhi
				 * 27-04-2018
				 * for renewal contract from old contract
				 */
				form1.contractRenewFlag =true;
				/**
				 * End
				 */
				form1.findMaxServiceSrNumber();
				/**
				 * Date 16-06-2018 by vijay 
				 * Des :- for contract renewal schedule services branch name. for old contract hashmap not exist so
				 */
				AppUtility.branchName=model.getBranch();
				
				/**
		  		 * Date 28-09-2018 By Vijay 
		  		 * Des:- NBHC CCPM for every contract this must be true by default it does not allow to change for user
		  		 */
		  		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC")){
		  			form1.cbStartOfPeriod.setValue(true);
		  			form1.cbStartOfPeriod.setEnabled(false);
		  		}
		  		/**
		  		 * ends here
		  		 */
		  		/**
		  		 * Date 20-03-2019 by vijay for NBHC CCPM for renewal contracts must have the quotation id
		  		 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "EnableQuotationMandatoryForContractRenwal")){
			  		if(singledropdownpopup!=null && singledropdownpopup.getOlbQuotationId().getSelectedIndex()!=0){
				  		String quotationId[] = singledropdownpopup.getOlbQuotationId().getValue(singledropdownpopup.getOlbQuotationId().getSelectedIndex()).split("/");
				  		System.out.println("quotationId[] =="+quotationId[0]);
				  		form1.getTbQuotatinId().setValue(quotationId[0]);
			  		}
				}
				
				/**
				 * Date 26-07-2019 by Vijay 
				 * Des :- NBHC CCPM for renewal contract set default to AMC and Rate.
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableRenewalContractTypeDefault")){
					if(model.isContractRate()){
						form1.getOlbContractType().setValue("Rate Contract(Renewal)");
						form1.getOlbContractType().setEnabled(false);
					}
					else{
						System.out.println("Contract Type =="+form1.getOlbContractType().getValue());
						form1.getOlbContractType().setValue("AMC(Renewal)");
						form1.getOlbContractType().setEnabled(false);
						System.out.println("Contract Type "+form1.getOlbContractType().getValue());

					}
				}
				/**
				 * ends here
				 */
				
				/**
				 * @author Vijay Date :- 18-11-2021
				 * Des :- As per Nitin sir payment terms should be copied to contract renewal time
				 */
				if(model.getPayTerms()!=null && !model.getPayTerms().equals(""))
					form1.olbPaymentTerms.setValue(model.getPayTerms());
				
				if(model.getPaymentTermsList()!=null){
					/**
					 * @author Vijay Date :- 10-05-2023
					 * Des :- when we renew contract payment date also copying and as per old contract billing document created
					 * so set payment date blank
					 */
					List<PaymentTerms> paymenttermslist = new ArrayList<PaymentTerms>();
					paymenttermslist.addAll(model.getPaymentTermsList());
					for(PaymentTerms paymentterms : paymenttermslist){
						paymentterms.setPaymentDate(null);
					}
//					form1.paymentTermsTable.getDataprovider().setList(model.getPaymentTermsList());
					form1.paymentTermsTable.getDataprovider().setList(paymenttermslist);

					/**
					 * ends here
					 */
				}
				if(model.getNumberRange()!=null && !model.getNumberRange().equals(""))
					form1.olbcNumberRange.setValue(model.getNumberRange());
				
				
				form.getOlbbBranch().setValue(model.getBranch());
		    	  Branch branchEntity =form.getOlbbBranch().getSelectedItem();
		    	  if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
						form.olbcNumberRange.setValue(branchEntity.getNumberRange());
						Console.log("Number range set to drop down by default");
		    	  }else {
		    		  	String range=LoginPresenter.branchWiseNumberRangeMap.get(model.getBranch());
				  		Console.log("LoginPresenter.branchWiseNumberRangeMap size="+LoginPresenter.branchWiseNumberRangeMap.size());	
						if(range!=null&&!range.equals("")) {
							form.olbcNumberRange.setValue(range);
							Console.log("in else Number range set to drop down by default");							
						}				
		    	  }
		  		/**
		  		 * ends here
		  		 */
			}
		};
		/**
		 * @author Anil
		 * @since 18-06-2020
		 * Hodling screen for 10 sec for loading configuration details
		 * NBHC faces issues while renewing large contract of multiple location
		 * location details are not getting mapped on line item
		 */
//		t.schedule(3000);
		t.schedule(10000);
	}

	private void checkForBillingDetailsStatus() {
		cancellis = new ArrayList<CancellationSummary>();
		final int conId = Integer.parseInt(form.getTbContractId().getValue()
				.trim());
		final String conStatus = form.getstatustextbox().getValue().trim();

		System.out.println("inside Cheque For Billing Details Status");
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(conId);
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("typeOfOrder");
		filter.setStringValue(AppConstants.ORDERTYPEFORSERVICE);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new BillingDocument());

		genasync.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {

					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {

						if (result.size() == 0) {
							System.out
									.println("This part is for rate contract without billing ");
							checkForServices();
							createNewContract();
						} else {
							int req = 0;
							ArrayList<BillingDocument> slist = new ArrayList<BillingDocument>();

							for (SuperModel model : result) {
								BillingDocument billingEntity = (BillingDocument) model;
								billingEntity.setStatus(billingEntity.getStatus());
								slist.add(billingEntity);

								if (BillingDocument.BILLINGINVOICED.equals(billingEntity.getStatus().trim())) 
								{
									CancellationSummary table1 = new CancellationSummary();
									table1.setDocID(billingEntity.getCount());
									table1.setDocType(AppConstants.APPROVALBILLINGDETAILS);
									table1.setRemark(popup.getRemark()
											.getValue().trim());
									table1.setLoggedInUser(LoginPresenter.loggedInUser);
									table1.setOldStatus(billingEntity
											.getStatus());
									table1.setNewStatus(BillingDocument.BILLINGINVOICED);
									table1.setSrNo(1);

									cancellis.add(table1);
								} else {
									CancellationSummary table1 = new CancellationSummary();
									table1.setDocID(billingEntity.getCount());
									table1.setDocType(AppConstants.APPROVALBILLINGDETAILS);
									table1.setRemark(popup.getRemark()
											.getValue().trim());
									table1.setLoggedInUser(LoginPresenter.loggedInUser);
									table1.setOldStatus(billingEntity
											.getStatus());
									table1.setNewStatus(BillingDocument.CANCELLED);
									table1.setSrNo(1);

									cancellis.add(table1);
								}
							}

							System.out.println("Billing done cancellis size ::"
									+ cancellis.size());

							int cnt = 0;
							int flag = 0;
							for (int i = 0; i < slist.size(); i++) {

								if (slist.get(i).getStatus()
										.equals(BillingDocument.APPROVED)) {
									flag = flag + 1;
								} else if (slist
										.get(i)
										.getStatus()
										.equals(BillingDocument.BILLINGINVOICED)) {
									cnt = cnt + 1;

								} else if (slist.get(i).getStatus()
										.equals(BillingDocument.CREATED)) {
									flag = flag + 1;
								} else if (slist.get(i).getStatus()
										.equals(BillingDocument.REJECTED)) {
									flag = flag + 1;
								} else if (slist.get(i).getStatus()
										.equals(BillingDocument.REQUESTED)) {
									req = req + 1;
								}

							}
							if (cnt > 0) {

								System.out
										.println("in side cnt > 0 condition ");
								// **************************************************************
								MyQuerry querry = new MyQuerry();
								Company c = new Company();
								Vector<Filter> filtervec = new Vector<Filter>();
								Filter filter = null;
								filter = new Filter();
								filter.setQuerryString("companyId");
								filter.setLongValue(c.getCompanyId());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("contractCount");
								filter.setIntValue(conId);
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("typeOfOrder");
								filter.setStringValue(AppConstants.ORDERTYPESERVICE);
								filtervec.add(filter);
								querry.setFilters(filtervec);
								querry.setQuerryObject(new Invoice());
								genasync.getSearchResult(
										querry,
										new AsyncCallback<ArrayList<SuperModel>>() {

											@Override
											public void onFailure(
													Throwable caught) {
											}

											@Override
											public void onSuccess(
													ArrayList<SuperModel> result) {												int cnt = 0;
												int requested = 0;
												ArrayList<Invoice> slist = new ArrayList<Invoice>();

												for (SuperModel model : result) {
													Invoice invoiceEntity = (Invoice) model;

													invoiceEntity.setStatus(invoiceEntity.getStatus());
													invoiceEntity.setContractCount(invoiceEntity.getContractCount());
													invoiceEntity.setInvoiceCount(invoiceEntity.getCount());
													invoiceEntity.setStatus(invoiceEntity.getStatus());
													slist.add(invoiceEntity);

													if (Invoice.APPROVED.equals(invoiceEntity.getStatus().trim())
															|| (BillingDocument.BILLINGINVOICED.equals(invoiceEntity.getStatus().trim()))) {
														CancellationSummary table2 = new CancellationSummary();

														table2.setDocID(invoiceEntity
																.getCount());
														table2.setDocType(AppConstants.APPROVALINVOICEDETAILS);
														table2.setRemark(popup.getRemark().getValue().trim());
														table2.setLoggedInUser(LoginPresenter.loggedInUser);

														if (Invoice.APPROVED.equals(invoiceEntity.getStatus().trim())) {

															table2.setOldStatus(invoiceEntity
																	.getStatus());
															table2.setNewStatus(Invoice.APPROVED);
														} else {
															table2.setOldStatus(invoiceEntity
																	.getStatus());
															table2.setNewStatus(BillingDocument.BILLINGINVOICED);
														}

														table2.setSrNo(2);

														cancellis.add(table2);
													} else {
														CancellationSummary table2 = new CancellationSummary();

														table2.setDocID(invoiceEntity
																.getCount());
														table2.setDocType(AppConstants.APPROVALINVOICEDETAILS);
														table2.setRemark(popup
																.getRemark()
																.getValue()
																.trim());
														table2.setLoggedInUser(LoginPresenter.loggedInUser);
														table2.setOldStatus(invoiceEntity
																.getStatus());
														table2.setNewStatus(Invoice.CANCELLED);
														table2.setSrNo(2);

														cancellis.add(table2);
													}
												}

												System.out
														.println("in side invoice cancel cancelist size"
																+ cancellis
																		.size());

												for (int i = 0; i < slist
														.size(); i++) {

													if (form.getTbContractId()
															.getValue()
															.trim()
															.equals(slist
																	.get(i)
																	.getContractCount()))
														;
													{
														if (slist
																.get(i)
																.getStatus()
																.equals(Invoice.CREATED)) {
														}

														else if (slist
																.get(i)
																.getStatus()
																.equals(Invoice.APPROVED)) {
															cnt = cnt + 1;
														} else if (slist
																.get(i)
																.getStatus()
																.equals(Invoice.REJECTED)) {
														} else if (slist
																.get(i)
																.getStatus()
																.equals(Invoice.REQUESTED)) {
															requested = requested + 1;
														}

													}

												}
												if (cnt > 0) {

													MyQuerry querry = new MyQuerry();
													Company c = new Company();
													Vector<Filter> filtervec = new Vector<Filter>();
													Filter filter = null;
													filter = new Filter();
													filter.setQuerryString("companyId");
													filter.setLongValue(c
															.getCompanyId());
													filtervec.add(filter);
													filter = new Filter();
													filter.setQuerryString("contractCount");
													filter.setIntValue(conId);
													filtervec.add(filter);
													filter = new Filter();
													filter.setQuerryString("typeOfOrder");
													filter.setStringValue(AppConstants.ORDERTYPESERVICE);
													filtervec.add(filter);
													querry.setFilters(filtervec);
													querry.setQuerryObject(new CustomerPayment());
													genasync.getSearchResult(
															querry,
															new AsyncCallback<ArrayList<SuperModel>>() {

																@Override
																public void onFailure(
																		Throwable caught) {
																}

																@Override
																public void onSuccess(
																		ArrayList<SuperModel> result) {

																	int cnt = 0;

																	ArrayList<CustomerPayment> slist = new ArrayList<CustomerPayment>();

																	for (SuperModel model : result) {
																		CustomerPayment custPayEntity = (CustomerPayment) model;

																		custPayEntity
																				.setStatus(custPayEntity
																						.getStatus());
																		custPayEntity
																				.setInvoiceCount(custPayEntity
																						.getInvoiceCount());
																		custPayEntity
																				.setContractCount(custPayEntity
																						.getContractCount());
																		slist.add(custPayEntity);

																		if (custPayEntity
																				.getStatus()
																				.equals(CustomerPayment.PAYMENTCLOSED)) {
																			CancellationSummary table2 = new CancellationSummary();

																			table2.setDocID(custPayEntity
																					.getCount());
																			table2.setDocType("Payment Details");
																			table2.setRemark(popup
																					.getRemark()
																					.getValue()
																					.trim());
																			table2.setLoggedInUser(LoginPresenter.loggedInUser);
																			table2.setOldStatus(custPayEntity
																					.getStatus());
																			table2.setNewStatus(CustomerPayment.PAYMENTCLOSED);
																			table2.setSrNo(2);

																			cancellis
																					.add(table2);
																		} else {
																			CancellationSummary table2 = new CancellationSummary();

																			table2.setDocID(custPayEntity
																					.getCount());
																			table2.setDocType("Payment Details");
																			table2.setRemark(popup
																					.getRemark()
																					.getValue()
																					.trim());
																			table2.setLoggedInUser(LoginPresenter.loggedInUser);
																			table2.setOldStatus(custPayEntity
																					.getStatus());
																			table2.setNewStatus(Invoice.CANCELLED);
																			table2.setSrNo(2);

																			cancellis
																					.add(table2);
																		}
																	}

																	for (int i = 0; i < slist.size(); i++) {

																		if (form.getTbContractId().getValue().trim().equals(slist.get(i).getContractCount()));
																		{
																			if (slist.get(i).getStatus().equals("Created")) {
																				cnt = cnt + 1;
																			}

																			else if (slist
																					.get(i)
																					.getStatus()
																					.equals("Closed")) {

																			}
																		}
																	}

																	if (cnt > 0) {
																		// ***********for
																		// cancle
																		// billing************
																		ChequeForMIN();
																		ChequeForMMN();
																		checkForServices();
																		ChequeForServiceProject();
																		changeStatus();
																		cancelInvoiceDetails();
																		cancelPaymentDetails();

																		createNewContract();
																	} else {

																		// ***********for
																		// cancle
																		// billing************
																		ChequeForMIN();
																		ChequeForMMN();
																		checkForServices();
																		ChequeForServiceProject();
																		changeStatus();
																		cancelInvoiceDetails();
																		cancelPaymentDetails();

																		createNewContract();
																	}
																}
															});
												} else {
													if (requested > 0) {
														for (int i = 0; i < requested; i++) {

															MyQuerry querry = new MyQuerry();
															Company c = new Company();
															Vector<Filter> filtervec = new Vector<Filter>();
															Filter filter = null;
															filter = new Filter();
															filter.setQuerryString("companyId");
															filter.setLongValue(c
																	.getCompanyId());
															filtervec
																	.add(filter);
															filter = new Filter();
															filter.setQuerryString("businessprocessId");
															filter.setIntValue(slist
																	.get(i)
																	.getCount());
															filtervec
																	.add(filter);
															filter = new Filter();
															filter.setQuerryString("businessprocesstype");
															filter.setStringValue(ApproverFactory.CONTRACT);
															filtervec
																	.add(filter);
															querry.setFilters(filtervec);
															querry.setQuerryObject(new Approvals());
															System.out
																	.println("querry completed......");
															genasync.getSearchResult(
																	querry,
																	new AsyncCallback<ArrayList<SuperModel>>() {

																		@Override
																		public void onFailure(
																				Throwable caught) {
																		}

																		@Override
																		public void onSuccess(
																				ArrayList<SuperModel> result) {

																			for (SuperModel smodel : result) {
																				Approvals approval = (Approvals) smodel;

																				approval.setStatus(Approvals.CANCELLED);
																				approval.setRemark("COntract Id ="
																						+ conId
																						+ " "
																						+ "Contract status ="
																						+ conStatus
																						+ "\n"
																						+ "has been cancelled by "
																						+ LoginPresenter.loggedInUser
																						+ " with remark"
																						+ "\n"
																						+ "Remark ="
																						+ popup.getRemark()
																								.getValue()
																								.trim());

																				genasync.save(
																						approval,
																						new AsyncCallback<ReturnFromServer>() {

																							@Override
																							public void onFailure(
																									Throwable caught) {
																								form.showDialogMessage("An Unexpected Error occured !");
																							}

																							@Override
																							public void onSuccess(
																									ReturnFromServer result) {

																							}
																						});

																			}
																		}
																	});
														}

														// ***********for cancle
														// billing************
														ChequeForServiceProject();
														checkForServices();
														ChequeForMIN();
														ChequeForMMN();
														changeStatus();
														cancelInvoiceDetails();
														createNewContract();
													}

													// ***********for cancle
													// billing************
													ChequeForServiceProject();
													checkForServices();
													ChequeForMIN();
													ChequeForMMN();
													changeStatus();
													cancelInvoiceDetails();
													createNewContract();
												}

											}
										});
								// **************************************************************
							} else {

								if (flag > 0) {

									checkForServices();
									ChequeForServiceProject();
									ChequeForMIN();
									ChequeForMMN();
									changeStatus();
									createNewContract();

								}
								if (req > 0) {

									for (int i = 0; i < req; i++) {

										MyQuerry querry = new MyQuerry();
										Company c = new Company();
										Vector<Filter> filtervec = new Vector<Filter>();
										Filter filter = null;
										filter = new Filter();
										filter.setQuerryString("companyId");
										filter.setLongValue(c.getCompanyId());
										filtervec.add(filter);
										filter = new Filter();
										filter.setQuerryString("businessprocessId");
										filter.setIntValue(slist.get(i)
												.getCount());
										filtervec.add(filter);
										querry.setFilters(filtervec);
										querry.setQuerryObject(new Approvals());
										System.out
												.println("querry completed......");
										genasync.getSearchResult(
												querry,
												new AsyncCallback<ArrayList<SuperModel>>() {

													@Override
													public void onFailure(
															Throwable caught) {
													}

													@Override
													public void onSuccess(
															ArrayList<SuperModel> result) {

														for (SuperModel smodel : result) {
															Approvals approval = (Approvals) smodel;

															approval.setStatus(Approvals.CANCELLED);
															approval.setRemark("Contract Id ="
																	+ conId
																	+ " "
																	+ "Contract status ="
																	+ form.getstatustextbox()
																			.getValue()
																	+ "\n"
																	+ "has been cancelled by "
																	+ LoginPresenter.loggedInUser
																	+ " with remark"
																	+ "\n"
																	+ "Remark ="
																	+ popup.getRemark()
																			.getValue()
																			.trim());

															genasync.save(
																	approval,
																	new AsyncCallback<ReturnFromServer>() {

																		@Override
																		public void onFailure(
																				Throwable caught) {
																			form.showDialogMessage("An Unexpected Error occured !");
																		}

																		@Override
																		public void onSuccess(
																				ReturnFromServer result) {

																		}
																	});

														}
													}
												});
									}

								}
							}
						}
					}
				});

	}

	private void ChequeForMIN() {
		final int conId = Integer.parseInt(form.getTbContractId().getValue()
				.trim());
		final String conStatus = form.getstatustextbox().getValue().trim();
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("minSoId");
		filter.setIntValue(conId);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new MaterialIssueNote());

		genasync.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An unexpected error occured!");
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {

						for (SuperModel model : result) {
							MaterialIssueNote minEntity = (MaterialIssueNote) model;

							CancellationSummary table5 = new CancellationSummary();

							table5.setDocID(minEntity.getCount());
							table5.setDocType("MIN");
							table5.setRemark(popup.getRemark().getValue()
									.trim());
							table5.setLoggedInUser(LoginPresenter.loggedInUser);
							table5.setOldStatus(minEntity.getStatus());
							table5.setNewStatus(Service.CANCELLED);
							table5.setSrNo(6);

							cancellis.add(table5);

							if (minEntity.getStatus().equals(
									MaterialIssueNote.CREATED)) {
								minEntity.setStatus(Service.CANCELLED);
								minEntity.setMinDescription("Contract Id ="
										+ conId + " " + "Contract Status ="
										+ conStatus + "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark." + "\n" + "Remark ="
										+ popup.getRemark().getValue().trim());

								genasync.save(minEntity,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
												System.out
														.println("Succes For Contract MIN===Succes SAve");
											}
										});

							}
							if (minEntity.getStatus().equals(
									MaterialIssueNote.REJECTED)) {
								System.out
										.println("Succes For Contract MIN===rejected");
								minEntity.setStatus(Service.CANCELLED);
								minEntity.setMinDescription("Contract Id ="
										+ conId + " " + "Contract Status ="
										+ conStatus + "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark." + "\n" + "Remark ="
										+ popup.getRemark().getValue().trim());

								genasync.save(minEntity,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});

							}
							if (minEntity.getStatus().equals(
									MaterialIssueNote.REQUESTED)) {
								minEntity.setStatus(Service.CANCELLED);
								minEntity.setMinDescription("Contract Id ="
										+ conId + " " + "Contract Status ="
										+ conStatus + "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark." + "\n" + "Remark ="
										+ popup.getRemark().getValue().trim());

								genasync.save(minEntity,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});

								MyQuerry querry = new MyQuerry();
								Company c = new Company();
								Vector<Filter> filtervec = new Vector<Filter>();
								Filter filter = null;
								filter = new Filter();
								filter.setQuerryString("companyId");
								filter.setLongValue(c.getCompanyId());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("businessprocessId");
								filter.setIntValue(minEntity.getCount());
								filtervec.add(filter);
								filtervec.add(filter);
								querry.setFilters(filtervec);
								querry.setQuerryObject(new Approvals());
								System.out.println("querry completed......");

								genasync.getSearchResult(
										querry,
										new AsyncCallback<ArrayList<SuperModel>>() {

											@Override
											public void onFailure(
													Throwable caught) {
											}

											@Override
											public void onSuccess(
													ArrayList<SuperModel> result) {

												for (SuperModel model : result) {
													Approvals appEntity = (Approvals) model;

													appEntity
															.setStatus(Approvals.CANCELLED);
													appEntity
															.setRemark("Contarct Id ="
																	+ conId
																	+ " "
																	+ "Contract Status ="
																	+ conStatus
																	+ "\n"
																	+ "has been cancelled by "
																	+ LoginPresenter.loggedInUser
																	+ " with remark"
																	+ "\n"
																	+ "Remark ="
																	+ popup.getRemark()
																			.getValue()
																			.trim());

													genasync.save(
															appEntity,
															new AsyncCallback<ReturnFromServer>() {
																@Override
																public void onFailure(
																		Throwable caught) {
																	form.showDialogMessage("An Unexpected Error occured !");
																}

																@Override
																public void onSuccess(
																		ReturnFromServer result) {
																}
															});
												}

											}
										});

							}

							if (minEntity.getStatus().equals(
									MaterialIssueNote.APPROVED)) {
								minEntity.setStatus(Service.CANCELLED);
								minEntity.setMinDescription("Contract Id ="
										+ conId + " " + "Contract Status ="
										+ conStatus + "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark." + "\n" + "Remark ="
										+ popup.getRemark().getValue().trim());

								genasync.save(minEntity,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});

								// ****************

								MaterialMovementNote mmn = new MaterialMovementNote();

								mmn.setMmnTitle("MMN Return for Project");

								mmn.setMmnDate(new Date());

								mmn.setMmnTransactionType("RETURN");

								mmn.setTransDirection("IN");

								if (minEntity.getCategory() != null) {
									mmn.setMmnCategory(minEntity.getCategory());
								} else {
									mmn.setMmnCategory("");
								}

								if (minEntity.getCategory() != null) {
									mmn.setMmnType(minEntity.getType());
								} else {
									mmn.setMmnType("");
								}
								if (minEntity.getBranch() != null) {
									mmn.setBranch(minEntity.getBranch());
								}
								if (minEntity.getEmployee() != null) {
									mmn.setMmnPersonResposible(minEntity
											.getEmployee());
								}

								if (minEntity.getApproverName() != null) {
									mmn.setApproverName(minEntity
											.getApproverName());
								}
								if (minEntity.getEmployee() != null) {
									mmn.setEmployee(minEntity.getEmployee());
								}

								mmn.setStatus(MaterialMovementNote.CREATED);

								if (minEntity.getMinDescription() != null) {
									mmn.setMmnDescription("Ref MIN -: "
											+ minEntity.getCount() + "\n"
											+ minEntity.getMinDescription());
								}

								mmn.setSubProductTableMmn(minEntity
										.getSubProductTablemin());

								mmn.setCompanyId(minEntity.getCompanyId());

								genasync.save(mmn,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");

											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {

											}
										});

								// **************************
							}
						}
					}
				});
	}

	private void ChequeForMMN() {
		final int conId = Integer.parseInt(form.getTbContractId().getValue()
				.trim());
		final String conStatus = form.getstatustextbox().getValue().trim();
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("orderID");
		filter.setIntValue(conId);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new MaterialMovementNote());

		genasync.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("An unexpected error occured!");
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {

						for (SuperModel model : result) {
							MaterialMovementNote mmnEntity = (MaterialMovementNote) model;

							CancellationSummary table5 = new CancellationSummary();

							table5.setDocID(mmnEntity.getCount());
							table5.setDocType("MMN");
							table5.setRemark(popup.getRemark().getValue()
									.trim());
							table5.setLoggedInUser(LoginPresenter.loggedInUser);
							table5.setOldStatus(mmnEntity.getStatus());
							table5.setNewStatus(MaterialMovementNote.CANCELLED);
							table5.setSrNo(6);

							cancellis.add(table5);

							System.out.println("Succes For Contract MIN===1");

							if (mmnEntity.getStatus().equals(
									MaterialMovementNote.CREATED)) {
								mmnEntity.setStatus(Service.CANCELLED);
								mmnEntity.setMmnDescription("Contract Id ="
										+ conId + " " + "Contract Status ="
										+ conStatus + "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark " + "\n" + "Remark ="
										+ popup.getRemark().getValue().trim());

								genasync.save(mmnEntity,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
												System.out
														.println("Succes For Contract MIN===Succes SAve");
											}
										});

							}
							if (mmnEntity.getStatus().equals(
									MaterialMovementNote.REJECTED)) {
								mmnEntity
										.setStatus(MaterialMovementNote.CANCELLED);
								mmnEntity.setMmnDescription("Contract Id ="
										+ conId + " " + "Contract Status ="
										+ conStatus + "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark " + "\n" + "Remark ="
										+ popup.getRemark().getValue().trim());

								genasync.save(mmnEntity,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});

							}
							if (mmnEntity.getStatus().equals(
									MaterialMovementNote.REQUESTED)) {
								mmnEntity.setStatus(Service.CANCELLED);
								mmnEntity.setMmnDescription("Contract Id ="
										+ conId + " " + "Contract Status ="
										+ conStatus + "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark " + "\n" + "Remark ="
										+ popup.getRemark().getValue().trim());

								genasync.save(mmnEntity,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});

								MyQuerry querry = new MyQuerry();
								Company c = new Company();
								Vector<Filter> filtervec = new Vector<Filter>();
								Filter filter = null;
								filter = new Filter();
								filter.setQuerryString("companyId");
								filter.setLongValue(c.getCompanyId());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("businessprocessId");
								filter.setIntValue(mmnEntity.getCount());
								filtervec.add(filter);
								filtervec.add(filter);
								querry.setFilters(filtervec);
								querry.setQuerryObject(new Approvals());

								genasync.getSearchResult(
										querry,
										new AsyncCallback<ArrayList<SuperModel>>() {

											@Override
											public void onFailure(
													Throwable caught) {
											}

											@Override
											public void onSuccess(
													ArrayList<SuperModel> result) {

												for (SuperModel model : result) {
													Approvals appEntity = (Approvals) model;

													appEntity
															.setStatus(Approvals.CANCELLED);
													appEntity
															.setRemark("Contarct Id ="
																	+ conId
																	+ " "
																	+ "Contract Status ="
																	+ conStatus
																	+ "\n"
																	+ "has been cancelled by "
																	+ LoginPresenter.loggedInUser
																	+ " with remark "
																	+ "\n"
																	+ "Remark ="
																	+ popup.getRemark()
																			.getValue()
																			.trim());

													genasync.save(
															appEntity,
															new AsyncCallback<ReturnFromServer>() {
																@Override
																public void onFailure(
																		Throwable caught) {
																	form.showDialogMessage("An Unexpected Error occured !");
																}

																@Override
																public void onSuccess(
																		ReturnFromServer result) {
																}
															});
												}

											}
										});

							}

							if (mmnEntity.getStatus().equals(
									MaterialMovementNote.APPROVED)) {
								mmnEntity.setStatus(Service.CANCELLED);
								mmnEntity.setMmnDescription("Contract Id ="
										+ conId + " " + "Contract Status ="
										+ conStatus + "\n"
										+ "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark " + "\n" + "Remark ="
										+ popup.getRemark().getValue().trim());

								genasync.save(mmnEntity,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});

								// ****************

								// MaterialMovementNote mmn=new
								// MaterialMovementNote();
								//
								// mmn.setMmnTitle("MMN Return for Project");
								//
								// mmn.setMmnDate(new Date());
								//
								// mmn.setMmnTransactionType("RETURN");
								//
								// mmn.setTransDirection("IN");
								//
								// if(minEntity.getCategory()!=null){
								// mmn.setMmnCategory(minEntity.getCategory());
								// }else
								// {
								// mmn.setMmnCategory("");
								// }
								//
								// if(minEntity.getCategory()!=null){
								// mmn.setMmnType(minEntity.getType());
								// }
								// else
								// {
								// mmn.setMmnType("");
								// }
								// if(minEntity.getBranch()!=null){
								// mmn.setBranch(minEntity.getBranch());
								// }
								// System.out.println("rrrrrrrrrrrrrooooooooooohhhhhhhhhhaaaaaaannnnnnnn");
								// if(minEntity.getEmployee()!=null){
								// mmn.setMmnPersonResposible(minEntity.getEmployee());
								// }
								//
								// if(minEntity.getApproverName()!=null){
								// mmn.setApproverName(minEntity.getApproverName());
								// }
								// System.out.println("7777777777777777777777777777777777777777777777");
								// if(minEntity.getEmployee()!=null){
								// mmn.setEmployee(minEntity.getEmployee());
								// }
								//
								// mmn.setStatus(MaterialMovementNote.CREATED);
								//
								//
								// if(minEntity.getMinDescription()!=null){
								// mmn.setMmnDescription("Ref MIN -: "+minEntity.getCount()+"\n"+minEntity.getMinDescription());
								// }
								//
								// mmn.setSubProductTableMmn(minEntity.getSubProductTablemin());
								//
								//
								// System.out.println("8888888888888888888888888888888888888888888");
								//
								// mmn.setCompanyId(minEntity.getCompanyId());
								//
								// genasync.save(mmn,new
								// AsyncCallback<ReturnFromServer>() {
								//
								// @Override
								// public void onFailure(Throwable caught) {
								// form.showDialogMessage("An Unexpected Error occured !");
								//
								// }
								//
								// @Override
								// public void onSuccess(ReturnFromServer
								// result) {
								//
								// }
								// });

								// **************************
							}
						}
					}
				});
	}

	private void checkForServices() {

		final int conId = Integer.parseInt(form.getTbContractId().getValue()
				.trim());
		final String conStatus = form.getstatustextbox().getValue().trim();

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(conId);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Service());
		System.out.println("querry completed......");

		genasync.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {

						if (result.size() == 0) {
							createNewContract();
						} else {
							for (SuperModel model : result) {
								Service ser = (Service) model;
								if (ser.getStatus().trim()
										.equals(Service.SERVICESTATUSCOMPLETED)) {
									CancellationSummary table5 = new CancellationSummary();

									table5.setDocID(ser.getCount());
									table5.setDocType("Service Details");
									table5.setRemark(popup.getRemark()
											.getValue().trim());
									table5.setLoggedInUser(LoginPresenter.loggedInUser);
									table5.setOldStatus(ser.getStatus());
									table5.setNewStatus(Service.SERVICESTATUSCOMPLETED);
									table5.setSrNo(5);

									cancellis.add(table5);
								} else {
									CancellationSummary table5 = new CancellationSummary();

									table5.setDocID(ser.getCount());
									table5.setDocType("Service Details");
									table5.setRemark(popup.getRemark()
											.getValue().trim());
									table5.setLoggedInUser(LoginPresenter.loggedInUser);
									table5.setOldStatus(ser.getStatus());
									table5.setNewStatus(Service.CANCELLED);
									table5.setSrNo(5);

									cancellis.add(table5);
								}

								if (ser.getStatus().equals(
										Service.SERVICESTATUSSCHEDULE)) {

									ser.setStatus(Service.CANCELLED);
									ser.setReasonForChange("Contract Id ="
											+ conId
											+ " "
											+ "Contract Status ="
											+ conStatus
											+ "\n"
											+ "has been cancelled by "
											+ LoginPresenter.loggedInUser
											+ " with remark."
											+ "\n"
											+ "Remark ="
											+ popup.getRemark().getValue()
													.trim());

									genasync.save(
											ser,
											new AsyncCallback<ReturnFromServer>() {
												@Override
												public void onFailure(
														Throwable caught) {
													form.showDialogMessage("An Unexpected Error occured !");
												}

												@Override
												public void onSuccess(
														ReturnFromServer result) {
												}
											});
								}

								if (ser.getStatus().equals(
										Service.SERVICESTATUSCOMPLETED)) {

								}

								if (ser.getStatus().equals(
										Service.SERVICESTATUSRESCHEDULE)) {

									ser.setStatus(Service.CANCELLED);
									ser.setReasonForChange("Contract Id ="
											+ conId
											+ " "
											+ "Contract Status ="
											+ conStatus
											+ "\n"
											+ "has been cancelled by "
											+ LoginPresenter.loggedInUser
											+ " with remark."
											+ "\n"
											+ "Remark ="
											+ popup.getRemark().getValue()
													.trim());

									genasync.save(
											ser,
											new AsyncCallback<ReturnFromServer>() {
												@Override
												public void onFailure(
														Throwable caught) {
													form.showDialogMessage("An Unexpected Error occured !");
												}

												@Override
												public void onSuccess(
														ReturnFromServer result) {
												}
											});
								}

								if (ser.getStatus().equals(
										Service.SERVICESTATUSPENDING)) {

									ser.setStatus(Service.CANCELLED);
									ser.setReasonForChange("Contract Id ="
											+ conId
											+ " "
											+ "Contract Status ="
											+ conStatus
											+ "\n"
											+ "has been cancelled by "
											+ LoginPresenter.loggedInUser
											+ " with remark."
											+ "\n"
											+ "Remark ="
											+ popup.getRemark().getValue()
													.trim());

									genasync.save(
											ser,
											new AsyncCallback<ReturnFromServer>() {
												@Override
												public void onFailure(
														Throwable caught) {
													form.showDialogMessage("An Unexpected Error occured !");
												}

												@Override
												public void onSuccess(
														ReturnFromServer result) {
												}
											});
								}
							}
						}
					}
				});

	}

	private void ChequeForServiceProject() {
		final int conId = Integer.parseInt(form.getTbContractId().getValue()
				.trim());
		final String conStatus = form.getstatustextbox().getValue().trim();

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("contractId");
		filter.setIntValue(conId);
		filtervec.add(filter);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ServiceProject());

		genasync.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {

						for (SuperModel model : result) {
							ServiceProject ser = (ServiceProject) model;

							if (ser.getProjectStatus().trim()
									.equals(ServiceProject.COMPLETED)) {

								CancellationSummary table6 = new CancellationSummary();

								table6.setDocID(ser.getCount());
								table6.setDocType("ServiceProject");
								table6.setRemark(popup.getRemark().getValue()
										.trim());
								table6.setLoggedInUser(LoginPresenter.loggedInUser);
								table6.setOldStatus(ser.getProjectStatus());
								table6.setNewStatus(ServiceProject.COMPLETED);
								table6.setSrNo(6);

								cancellis.add(table6);

							} else {
								CancellationSummary table6 = new CancellationSummary();

								table6.setDocID(ser.getCount());
								table6.setDocType("ServiceProject");
								table6.setRemark(popup.getRemark().getValue()
										.trim());
								table6.setLoggedInUser(LoginPresenter.loggedInUser);
								table6.setOldStatus(ser.getProjectStatus());
								table6.setNewStatus(Service.CANCELLED);
								table6.setSrNo(6);

								cancellis.add(table6);
							}

							if (ser.getProjectStatus().equals(
									ServiceProject.CREATED)) {

								ser.setProjectStatus(ServiceProject.CANCELLED);
								ser.setComment("Contract Id =" + conId + " "
										+ "Contract Status =" + conStatus
										+ "\n" + "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark." + "\n" + "Remark ="
										+ popup.getRemark().getValue().trim());

								genasync.save(ser,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (ser.getProjectStatus().equals(
									ServiceProject.COMPLETED)) {
							}

							if (ser.getProjectStatus().equals(
									ServiceProject.SCHEDULED)) {

								ser.setProjectStatus(Service.CANCELLED);
								ser.setComment("Contract Id =" + conId + " "
										+ "Contract Status =" + conStatus
										+ "\n" + "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark." + "\n" + "Remark ="
										+ popup.getRemark().getValue().trim());

								genasync.save(ser,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

						}
					}
				});
	}

	private void createNewContract() {

		model.setStatus(Contract.CANCELLED);
		model.setDescription(model.getDescription() + "\n" + "Contract Id ="
				+ form.getTbContractId().getValue().trim() + " "
				+ "Contract Status ="
				+ form.getstatustextbox().getValue().trim() + "\n"
				+ "has been cancelled by " + LoginPresenter.loggedInUser
				+ " with remark" + "\n" + "Remark ="
				+ popup.getRemark().getValue());

		genasync.save(model, new AsyncCallback<ReturnFromServer>() {

			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected Error occured !");
			}

			@Override
			public void onSuccess(ReturnFromServer result) {

				CancellationSummary table5 = new CancellationSummary();

				table5.setDocID(model.getCount());
				table5.setDocType(AppConstants.APPROVALCONTRACT);
				table5.setRemark(popup.getRemark().getValue().trim());
				table5.setLoggedInUser(LoginPresenter.loggedInUser);
				table5.setOldStatus(Contract.APPROVED);
				table5.setNewStatus(model.getStatus());
				table5.setSrNo(4);

				cancellis.add(table5);
				CancellationSummaryPopUpTable popupTable = new CancellationSummaryPopUpTable();
				popupTable.setValue(cancellis);

				List<CancellationSummary> cancellistsetting1 = new ArrayList<CancellationSummary>();
				for (int i = 0; i < cancellis.size(); i++) {
					CancellationSummary sum = new CancellationSummary();
					sum.setSrNo(i + 1);
					sum.setDocID(cancellis.get(i).getDocID());
					sum.setDocType(cancellis.get(i).getDocType());
					sum.setLoggedInUser(cancellis.get(i).getLoggedInUser());
					sum.setOldStatus(cancellis.get(i).getOldStatus());
					sum.setNewStatus(cancellis.get(i).getNewStatus());
					sum.setRemark(cancellis.get(i).getRemark());

					cancellistsetting1.add(sum);
				}

				summaryPanel = new PopupPanel(true);
				summaryPanel.add(summaryPopup1);
				summaryPanel.show();
				summaryPanel.center();
				summaryPopup1.setSummaryTable(cancellistsetting1);

				CancelSummary cancelSummary = new CancelSummary();
				cancelSummary.setCancelLis(cancellistsetting1);
				cancelSummary.setDocType(AppConstants.APPROVALCONTRACT);
				cancelSummary.setDocID(model.getCount());
				try {
					genasync.save(cancelSummary,
							new AsyncCallback<ReturnFromServer>() {

								@Override
								public void onFailure(Throwable caught) {
									form.showDialogMessage("An Unexpected Error occured !");
								}

								@Override
								public void onSuccess(ReturnFromServer result) {

								}
							});
				} catch (Exception e) {
					e.printStackTrace();
				}

				form.getTbQuotationStatus().setValue(model.getStatus());
				form.getTaDescription().setValue(model.getDescription());
				form.toggleProcessLevelMenu();
			}
		});

	}

	private void changeStatus() {
		final int conId = Integer.parseInt(form.getTbContractId().getValue()
				.trim());
		final String conStatus = form.getstatustextbox().getValue().trim();

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(conId);
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("typeOfOrder");
		filter.setStringValue(AppConstants.ORDERTYPEFORSERVICE);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new BillingDocument());
		System.out.println("querry completed......");
		genasync.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {

						for (SuperModel smodel : result) {
							BillingDocument biil = (BillingDocument) smodel;

							if (biil.getStatus()
									.equals(BillingDocument.CREATED)) {

								biil.setStatus(BillingDocument.CANCELLED);
								biil.setComment("Contract Id =" + conId + " "
										+ "Contract status =" + conStatus
										+ "\n" + "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark " + "\n" + "Remark ="
										+ popup.getRemark().getValue().trim());
								genasync.save(biil,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
												//
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (biil.getStatus().equals(
									BillingDocument.APPROVED)) {

								biil.setStatus(BillingDocument.CANCELLED);
								biil.setComment("Contract Id =" + conId + " "
										+ "Contract status =" + conStatus
										+ "\n" + "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark " + "\n" + "Remark ="
										+ popup.getRemark().getValue().trim());
								genasync.save(biil,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
												//
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}
							if (biil.getStatus().equals(
									BillingDocument.BILLINGINVOICED)) {
								biil.setRenewFlag(true);
								biil.setStatus(BillingDocument.BILLINGINVOICED);
								biil.setComment("Contract Id =" + conId + " "
										+ "Contract status =" + conStatus
										+ "\n" + "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark " + "\n" + "Remark ="
										+ popup.getRemark().getValue().trim());
								genasync.save(biil,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
												//
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (biil.getStatus().equals(
									BillingDocument.REJECTED)) {

								biil.setStatus(BillingDocument.CANCELLED);
								biil.setComment("Contract Id =" + conId + " "
										+ "Contract status =" + conStatus
										+ "\n" + "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark " + "\n" + "Remaek ="
										+ popup.getRemark().getValue().trim());
								genasync.save(biil,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
												//
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (biil.getStatus().equals(
									BillingDocument.REQUESTED)) {

								biil.setStatus(BillingDocument.CANCELLED);
								biil.setComment("Contract Id =" + conId + " "
										+ "Contract status =" + conStatus
										+ "\n" + "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark " + "\n" + "Remark ="
										+ popup.getRemark().getValue().trim());
								genasync.save(biil,
										new AsyncCallback<ReturnFromServer>() {

											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
												//
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
								// }
								//
								// if(biil.getStatus().equals(BillingDocument.REQUESTED)){
								//

								MyQuerry querry = new MyQuerry();
								Company c = new Company();
								Vector<Filter> filtervec = new Vector<Filter>();
								Filter filter = null;
								filter = new Filter();
								filter.setQuerryString("companyId");
								filter.setLongValue(c.getCompanyId());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("businessprocessId");
								filter.setIntValue(biil.getCount());
								filtervec.add(filter);
								querry.setFilters(filtervec);
								querry.setQuerryObject(new Approvals());
								genasync.getSearchResult(
										querry,
										new AsyncCallback<ArrayList<SuperModel>>() {

											@Override
											public void onFailure(
													Throwable caught) {
											}

											@Override
											public void onSuccess(
													ArrayList<SuperModel> result) {

												for (SuperModel smodel : result) {
													Approvals approval = (Approvals) smodel;

													approval.setStatus(Approvals.CANCELLED);
													approval.setRemark("Contract Id ="
															+ conId
															+ " "
															+ "Contract status ="
															+ conStatus
															+ "\n"
															+ "has been cancelled by "
															+ LoginPresenter.loggedInUser
															+ " with remark "
															+ "\n"
															+ "Remark ="
															+ popup.getRemark()
																	.getValue()
																	.trim());

													genasync.save(
															approval,
															new AsyncCallback<ReturnFromServer>() {

																@Override
																public void onFailure(
																		Throwable caught) {
																	form.showDialogMessage("An Unexpected Error occured !");
																}

																@Override
																public void onSuccess(
																		ReturnFromServer result) {
																}
															});
												}
											}
										});
							}
						}
					}
				});
	}

	private void cancelPaymentDetails() {

		final int conId = Integer.parseInt(form.getTbContractId().getValue()
				.trim());
		final String conStatus = form.getstatustextbox().getValue().trim();

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(conId);
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("typeOfOrder");
		filter.setStringValue(AppConstants.ORDERTYPEFORSERVICE);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerPayment());
		System.out.println("querry completed......");
		genasync.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {

						for (SuperModel smodel : result) {

							CustomerPayment custpay = (CustomerPayment) smodel;
							if (custpay.getStatus().equals(
									CustomerPayment.CREATED)) {
								custpay.setStatus(CustomerPayment.CANCELLED);
								custpay.setComment("Contract Id =" + conId
										+ " " + "Contract Status =" + conStatus
										+ "\n" + "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark " + "\n" + "Remark ="
										+ popup.getRemark().getValue());
								genasync.save(custpay,
										new AsyncCallback<ReturnFromServer>() {
											//
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
												//
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (custpay.getStatus().equals(
									CustomerPayment.PAYMENTCLOSED)) {
								custpay.setRenewFlag(true);
								custpay.setStatus(CustomerPayment.PAYMENTCLOSED);
								custpay.setComment("Contract Id =" + conId
										+ " " + "Contract Status =" + conStatus
										+ "\n" + "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark " + "\n" + "Remark ="
										+ popup.getRemark().getValue());
								genasync.save(custpay,
										new AsyncCallback<ReturnFromServer>() {
											//
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
												//
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

						}

					}

				});
	}

	private void cancelInvoiceDetails() {

		final int conId = Integer.parseInt(form.getTbContractId().getValue()
				.trim());
		final String conStatus = form.getstatustextbox().getValue().trim();

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(conId);
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("typeOfOrder");
		filter.setStringValue(AppConstants.ORDERTYPEFORSERVICE);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Invoice());
		System.out.println("querry completed......");
		genasync.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {

						for (SuperModel smodel : result) {
							// **************inset login user name in comment
							// *******************

							Invoice invoice = (Invoice) smodel;
							if (invoice.getStatus().equals(Invoice.CREATED)) {
								invoice.setStatus(Invoice.CANCELLED);
								invoice.setComment("Contract Id =" + conId
										+ " " + "Contract Status =" + conStatus
										+ "\n" + "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark " + "\n" + "Remark ="
										+ popup.getRemark().getValue());
								genasync.save(invoice,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (invoice.getStatus().equals(Invoice.APPROVED)) {
								invoice.setRenewFlag(true);
								invoice.setStatus(Invoice.APPROVED);
								invoice.setComment("Contract Id =" + conId
										+ " " + "Contract Status =" + conStatus
										+ "\n" + "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark " + "\n" + "Remark ="
										+ popup.getRemark().getValue());
								genasync.save(invoice,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (invoice.getStatus().equals(Invoice.REJECTED)) {
								invoice.setStatus(Invoice.CANCELLED);
								invoice.setComment("Contract Id =" + conId
										+ " " + "Contract Status =" + conStatus
										+ "\n" + "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark " + "\n" + "Remark ="
										+ popup.getRemark().getValue());
								genasync.save(invoice,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (invoice.getStatus().equals(
									BillingDocument.PROFORMAINVOICE)) {
								invoice.setStatus(Invoice.CANCELLED);
								invoice.setComment("Contract Id =" + conId
										+ " " + "Contract Status =" + conStatus
										+ "\n" + "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark " + "\n" + "Remark ="
										+ popup.getRemark().getValue());
								genasync.save(invoice,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});
							}

							if (invoice.getStatus().equals(Invoice.REQUESTED)) {
								invoice.setStatus(Invoice.CANCELLED);
								invoice.setComment("Contract Id =" + conId
										+ " " + "Contract Status =" + conStatus
										+ "\n" + "has been cancelled by "
										+ LoginPresenter.loggedInUser
										+ " with remark " + "\n" + "Remark ="
										+ popup.getRemark().getValue());
								genasync.save(invoice,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.showDialogMessage("An Unexpected Error occured !");
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
											}
										});

								MyQuerry querry = new MyQuerry();
								Company c = new Company();
								Vector<Filter> filtervec = new Vector<Filter>();
								Filter filter = null;
								filter = new Filter();
								filter.setQuerryString("companyId");
								filter.setLongValue(c.getCompanyId());
								filtervec.add(filter);
								filter = new Filter();
								filter.setQuerryString("businessprocessId");
								filter.setIntValue(invoice.getCount());
								filtervec.add(filter);
								querry.setFilters(filtervec);
								querry.setQuerryObject(new Approvals());
								System.out.println("querry completed......");
								genasync.getSearchResult(
										querry,
										new AsyncCallback<ArrayList<SuperModel>>() {

											@Override
											public void onFailure(
													Throwable caught) {
											}

											@Override
											public void onSuccess(
													ArrayList<SuperModel> result) {

												for (SuperModel smodel : result) {
													Approvals approval = (Approvals) smodel;

													approval.setStatus(Approvals.CANCELLED);
													approval.setRemark("Contract Id ="
															+ conId
															+ " "
															+ "Contract status ="
															+ conStatus
															+ "\n"
															+ "has been cancelled by "
															+ LoginPresenter.loggedInUser
															+ " with remark "
															+ "\n"
															+ "Remark ="
															+ popup.getRemark()
																	.getValue()
																	.trim());

													genasync.save(
															approval,
															new AsyncCallback<ReturnFromServer>() {

																@Override
																public void onFailure(
																		Throwable caught) {
																	form.showDialogMessage("An Unexpected Error occured !");
																}

																@Override
																public void onSuccess(
																		ReturnFromServer result) {

																}
															});
												}
											}
										});
							}
						}
					}
				});
	}

	private void getSummaryFromContractID() {

		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("docID");
		filter.setIntValue(Integer.parseInt(form.getTbContractId().getValue()
				.trim()));
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("docType");
		filter.setStringValue(AppConstants.APPROVALCONTRACT);
		filtervec.add(filter);

		querry.setFilters(filtervec);
		querry.setQuerryObject(new CancelSummary());
		System.out.println("querry completed......");
		genasync.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						for (SuperModel smodel : result) {
							CancelSummary cancelEntity = (CancelSummary) smodel;

							summaryPanel = new PopupPanel(true);
							summaryPanel.add(summaryPopup1);
							summaryPanel.show();
							summaryPanel.center();
							summaryPopup1.setSummaryTable(cancelEntity
									.getCancelLis());
						}
					}
				});
	}

	private void reactOnCancelSummaryDownload() {
		ArrayList<CancelSummary> cancelsummarylist = new ArrayList<CancelSummary>();
		List<CancellationSummary> cancel = summaryPopup1.getSummaryTable()
				.getDataprovider().getList();
		CancelSummary cancelEntity = new CancelSummary();
		cancelEntity.setCancelLis(cancel);

		cancelsummarylist.add(cancelEntity);
		csvservice.setCancelSummaryDetails(cancelsummarylist,
				new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						System.out.println("RPC call Failed" + caught);
					}

					@Override
					public void onSuccess(Void result) {
						String gwt = com.google.gwt.core.client.GWT
								.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 82;
						Window.open(url, "test", "enabled");
					}
				});

	}

	/********************************* Service Schedule Logic **********************************/

	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(form.objContactList)){
			try{
				Console.log("Change EVENT Fired!!!");
				if(form.personInfoComposite.getValue()!=null){
					Console.log("Change EVENT Fired-1 !!!");
					if(form.personInfoComposite.contactObj!=null){
						Console.log("Change EVENT Fired-2 !!!");
						boolean flag=false;
						for(int i=1;i<form.objContactList.getItemCount();i++){
							if(form.objContactList.getItemText(i).equals(form.personInfoComposite.contactObj.getName())){
								flag=true;
								break;
							}
						}
						
						if(!flag){
							Console.log("Change EVENT Fired-3 !!!");
							form.objContactList.addItem(form.personInfoComposite.contactObj.getName());
//							for(int i=1;i<form.objContactList.getItemCount();i++){
//								if(form.objContactList.getItemText(i).equals(form.personInfoComposite.contactObj.getName())){
//									form.objContactList.setSelectedIndex(i);
//									break;
//								}
//							}
						}
					}
				}
			}catch(Exception e){
				Console.log("Exception-Change EVENT Fired!!!");
			}
		}
		if(event.getSource().equals(form.getObjPaymentMode())){
			form.setBankSelectedDetails();
		}
		
		
		
		if (event.getSource().equals(serviceSchedulePopUp.getP_mondaytofriday())) {

			serviceSchedulePopUp.getP_default().setValue(false);
			serviceSchedulePopUp.getP_customise().setValue(false);
			serviceSchedulePopUp.getP_interval().setValue(null);
			serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
			serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
			serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);

			serviceSchedulePopUp.getP_servicehours().setEnabled(true);
			serviceSchedulePopUp.getP_servicemin().setEnabled(true);
			serviceSchedulePopUp.getP_ampm().setEnabled(true);
			serviceSchedulePopUp.getP_customise().setEnabled(true);
			/**
			 * nidhi
			 *  ))..
			 *  for exclude day.
			 */
			serviceSchedulePopUp.getE_mondaytofriday().setSelectedIndex(0);
			/**
			 * end
			 */
			/**
			 * Date : 24-11-2017 BY ANIL
			 * For day selection with advance filter
			 */
			if(isAdvanceFilterSelected()){
				List<ServiceSchedule> serSchList=getUpdatedScheduledServices(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList(),"Day",
						serviceSchedulePopUp.getP_mondaytofriday().getSelectedIndex(),
						serviceSchedulePopUp.getP_serviceWeek().getSelectedIndex(),
						serviceSchedulePopUp.getE_mondaytofriday().getSelectedIndex());
				updateServiceScheduleList(serSchList,form.globalScheduleServiceList);
				serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serSchList);
				serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
				
			}else{
				reactOnMondaytoFridayList(serviceSchedulePopUp.getP_mondaytofriday().getSelectedIndex(),serviceSchedulePopUp.getP_serviceWeek().getSelectedIndex());
				serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
			}

//			reactOnMondaytoFridayList(serviceSchedulePopUp.getP_mondaytofriday().getSelectedIndex(), serviceSchedulePopUp.getP_serviceWeek().getSelectedIndex());
//			serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
			
			/**
			 * End
			 */
		}

		if (event.getSource().equals(serviceSchedulePopUp.getP_servicehours())) {
			
			/**
			 * Date : 25-11-2017 BY ANIL
			 */
			serviceSchedulePopUp.getP_default().setValue(false);
			serviceSchedulePopUp.getP_customise().setValue(false);
			serviceSchedulePopUp.getP_interval().setValue(null);
			/** Date 23-06-2018 By vijay below code commented for time combition with day and week***/
//			serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
//			serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
			/**
			 * ends here
			 */
//			serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
			serviceSchedulePopUp.getP_servicehours().setEnabled(true);
			serviceSchedulePopUp.getP_servicemin().setEnabled(true);
			serviceSchedulePopUp.getP_ampm().setEnabled(true);
			serviceSchedulePopUp.getP_customise().setEnabled(true);

			if (serviceSchedulePopUp.getP_servicehours().getSelectedIndex() != 0
					&& serviceSchedulePopUp.getP_servicemin().getSelectedIndex() != 0
					&& serviceSchedulePopUp.getP_ampm().getSelectedIndex() != 0) {
				List<ServiceSchedule> prodservicelist = this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
				if (prodservicelist.size() != 0) {
					
					/**
					 * Date : 24-11-2017 BY ANIL
					 * For time selection with advance filter
					 */
					if(isAdvanceFilterSelected()){
						List<ServiceSchedule> serSchList=getUpdatedScheduledServices(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList(),"Time",0,0,0);
						updateServiceScheduleList(serSchList,form.globalScheduleServiceList);
						serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serSchList);
						serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
						
					}else{
						reactOnHoursList();
					}
//					reactOnHoursList();
					/**
					 * End
					 */
				} else {
					form.showDialogMessage("Please Select Either Equal or Interval");
					serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
					serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
					serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
				}

			}

			// serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
		}

		if (event.getSource().equals(serviceSchedulePopUp.getP_servicemin())) {
			serviceSchedulePopUp.getP_default().setValue(false);
			serviceSchedulePopUp.getP_customise().setValue(false);
			serviceSchedulePopUp.getP_interval().setValue(null);
			/** Date 23-06-2018 By vijay below code commented for time combition with day and week***/
//			serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
//			serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
			/*** Ends here ****/
//			serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
			serviceSchedulePopUp.getP_servicehours().setEnabled(true);
			serviceSchedulePopUp.getP_servicemin().setEnabled(true);
			serviceSchedulePopUp.getP_ampm().setEnabled(true);
			serviceSchedulePopUp.getP_customise().setEnabled(true);

			if (serviceSchedulePopUp.getP_servicehours().getSelectedIndex() != 0
					&& serviceSchedulePopUp.getP_servicemin().getSelectedIndex() != 0
					&& serviceSchedulePopUp.getP_ampm().getSelectedIndex() != 0) {
				List<ServiceSchedule> prodservicelist = this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
				if (prodservicelist.size() != 0) {
					/**
					 * Date : 24-11-2017 BY ANIL
					 * For time selection with advance filter
					 */
					if(isAdvanceFilterSelected()){
						List<ServiceSchedule> serSchList=getUpdatedScheduledServices(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList(),"Time",0,0,0);
						updateServiceScheduleList(serSchList,form.globalScheduleServiceList);
						serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serSchList);
						serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
						
					}else{
						reactOnHoursList();
					}
//					reactOnHoursList();
					/**
					 * End
					 */
				} else {
					form.showDialogMessage("Please Select Either Equal or Interval");
					serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
					serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
					serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
				}
			}
		}

		if (event.getSource().equals(serviceSchedulePopUp.getP_ampm())) {
			serviceSchedulePopUp.getP_default().setValue(false);
			serviceSchedulePopUp.getP_customise().setValue(false);
			serviceSchedulePopUp.getP_interval().setValue(null);
			/** Date 23-06-2018 By vijay below code commented for time combition with day and week***/
//			serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
//			serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
			/*** Ends here ***/
//			serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
			serviceSchedulePopUp.getP_servicehours().setEnabled(true);
			serviceSchedulePopUp.getP_servicemin().setEnabled(true);
			serviceSchedulePopUp.getP_ampm().setEnabled(true);
			serviceSchedulePopUp.getP_customise().setEnabled(true);

			if (serviceSchedulePopUp.getP_servicehours().getSelectedIndex() != 0
					&& serviceSchedulePopUp.getP_servicemin().getSelectedIndex() != 0
					&& serviceSchedulePopUp.getP_ampm().getSelectedIndex() != 0) {
				List<ServiceSchedule> prodservicelist = this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
				if (prodservicelist.size() != 0) {
					/**
					 * Date : 24-11-2017 BY ANIL
					 * For time selection with advance filter
					 */
					if(isAdvanceFilterSelected()){
						List<ServiceSchedule> serSchList=getUpdatedScheduledServices(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList(),"Time",0,0,0);
						updateServiceScheduleList(serSchList,form.globalScheduleServiceList);
						serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serSchList);
						serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
						
					}else{
						reactOnHoursList();
					}
//					reactOnHoursList();
					/**
					 * End
					 */
				}else {
					form.showDialogMessage("Please Select Either Equal or Interval");
					serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
					serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
					serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
				}
			}
		}
		
		if(event.getSource().equals(serviceSchedulePopUp.getP_serviceWeek())){
			
			serviceSchedulePopUp.getP_default().setValue(false);
			serviceSchedulePopUp.getP_customise().setValue(false);
			serviceSchedulePopUp.getP_interval().setValue(null);
			serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
			serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
			serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);

			serviceSchedulePopUp.getP_servicehours().setEnabled(true);
			serviceSchedulePopUp.getP_servicemin().setEnabled(true);
			serviceSchedulePopUp.getP_ampm().setEnabled(true);
			serviceSchedulePopUp.getP_customise().setEnabled(true);
			
			/**
			 * Date : 23-11-2017 BY ANIL
			 * For week selection with advance filter
			 */
			if(isAdvanceFilterSelected()){
				if(serviceSchedulePopUp.getP_serviceWeek().getSelectedIndex()!=0){
				List<ServiceSchedule> serSchList=getUpdatedScheduledServices(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList(),"Week",
						serviceSchedulePopUp.getP_mondaytofriday().getSelectedIndex(),
						serviceSchedulePopUp.getP_serviceWeek().getSelectedIndex(),
						serviceSchedulePopUp.getE_mondaytofriday().getSelectedIndex());
				updateServiceScheduleList(serSchList,form.globalScheduleServiceList);
				serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serSchList);
				serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
				}
				
			}else{
				reactOnWeekList(serviceSchedulePopUp.getP_mondaytofriday().getSelectedIndex()
						,serviceSchedulePopUp.getP_serviceWeek().getSelectedIndex()
						,serviceSchedulePopUp.getE_mondaytofriday().getSelectedIndex());
				serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
			}
		}
		
		/**
		 * Date : 23-11-2017 BY ANIL
		 */
		if(event.getSource().equals(serviceSchedulePopUp.getLbServices())){
			serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
			serviceSchedulePopUp.getP_default().setValue(false);
			serviceSchedulePopUp.getP_customise().setValue(false);
			serviceSchedulePopUp.getP_interval().setValue(null);
			serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
			serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
			serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
			serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
			/**
			 * nidhi
			 * 22-05-2018
			 * ((..
			 * for exclude day
			 */
			serviceSchedulePopUp.getE_mondaytofriday().setSelectedIndex(0);
			/**
			 * end
			 */
			if(serviceSchedulePopUp.getLbServices().getSelectedIndex()!=0){
				/** Date 14-04-2018 By vijay below old code commented for new combination updation code**/
//				serviceSchedulePopUp.getOlbCustomerBranch().setSelectedIndex(0);
//				serviceSchedulePopUp.getOlbServicingBranch().setSelectedIndex(0);
//				ArrayList<ServiceSchedule> filteredList=new ArrayList<ServiceSchedule>();
//				for(ServiceSchedule obj:form.globalScheduleServiceList){
//        			if(obj.getScheduleProdName().equals(serviceSchedulePopUp.getLbServices().getValue(serviceSchedulePopUp.getLbServices().getSelectedIndex()))){
//        				filteredList.add(obj);
//        			}
//        		}
				
				ArrayList<ServiceSchedule> filteredList=new ArrayList<ServiceSchedule>();
				for(ServiceSchedule obj:form.globalScheduleServiceList){
					/** Date 14-04-2018 By vijay below code updated with adavance filter with combination
					 * and old code added in else block
					 **/
					if(serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()!=0 && serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()!=0){
						if(obj.getScheduleProdName().equals(serviceSchedulePopUp.getLbServices().getValue(serviceSchedulePopUp.getLbServices().getSelectedIndex())) && obj.getScheduleProBranch().equals(serviceSchedulePopUp.getOlbCustomerBranch().getValue(serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()))
								&& obj.getServicingBranch().equals(serviceSchedulePopUp.getOlbServicingBranch().getValue(serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()))){
							filteredList.add(obj);
						}
					}
				    else if(serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()!=0){
						if(obj.getScheduleProBranch().equals(serviceSchedulePopUp.getOlbCustomerBranch().getValue(serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex())) && obj.getScheduleProdName().equals(serviceSchedulePopUp.getLbServices().getValue(serviceSchedulePopUp.getLbServices().getSelectedIndex()))){
							filteredList.add(obj);
						}
					}
				    else if(serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()!=0){
				    	if(obj.getServicingBranch().equals(serviceSchedulePopUp.getOlbServicingBranch().getValue(serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex())) && obj.getScheduleProdName().equals(serviceSchedulePopUp.getLbServices().getValue(serviceSchedulePopUp.getLbServices().getSelectedIndex()))){
				    		filteredList.add(obj);
				    	}
				    }
					else{
						/** Date 14-04-2018 By vijay old code added here **/
						if(obj.getScheduleProdName().equals(serviceSchedulePopUp.getLbServices().getValue(serviceSchedulePopUp.getLbServices().getSelectedIndex()))){
	        				filteredList.add(obj);
	        			}
					}
        		}
        		serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(filteredList);
			}else{
				/** Date 14-04-2018 By vijay below code updated with adavance filter with combination
				 * and old code added in else block
				 **/
				if(serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()!=0 && serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()!=0){
					ArrayList<ServiceSchedule> filteredList=new ArrayList<ServiceSchedule>();
					for(ServiceSchedule obj:form.globalScheduleServiceList){
						if(obj.getScheduleProBranch().equals(serviceSchedulePopUp.getOlbCustomerBranch().getValue(serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()))
							&& obj.getServicingBranch().equals(serviceSchedulePopUp.getOlbServicingBranch().getValue(serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()))	){
	        				filteredList.add(obj);
	        			}
					}
	        		serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(filteredList);
	        		
				}
				else if(serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()!=0 &&serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()==0){
					ArrayList<ServiceSchedule> filteredList=new ArrayList<ServiceSchedule>();
					for(ServiceSchedule obj:form.globalScheduleServiceList){
						if(obj.getScheduleProBranch().equals(serviceSchedulePopUp.getOlbCustomerBranch().getValue(serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()))){
	        				filteredList.add(obj);
	        			}
					}
	        		serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(filteredList);
				}
				else if(serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()!=0 && serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()==0){
					ArrayList<ServiceSchedule> filteredList=new ArrayList<ServiceSchedule>();
					for(ServiceSchedule obj:form.globalScheduleServiceList){
						if(obj.getServicingBranch().equals(serviceSchedulePopUp.getOlbServicingBranch().getValue(serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()))){
	        				filteredList.add(obj);
	        			}
					}
	        		serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(filteredList);
				}
				else {
//				if(serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()==0
//						&&serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()==0){
					sortSechduleServicesByProdSerNoAndSerSrNo(form.globalScheduleServiceList);
					serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(form.globalScheduleServiceList);
				}
			}
		}
		if(event.getSource().equals(serviceSchedulePopUp.getOlbCustomerBranch())){
			
			serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
			serviceSchedulePopUp.getP_default().setValue(false);
			serviceSchedulePopUp.getP_customise().setValue(false);
			serviceSchedulePopUp.getP_interval().setValue(null);
			serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
			serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
			serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
			serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
			/**
			 * nidhi
			 * 22-05-2018
			 * ((..
			 * for exclude day
			 */
			serviceSchedulePopUp.getE_mondaytofriday().setSelectedIndex(0);
			/**
			 * end
			 */
			if(serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()!=0){
				/** Date 14-04-2018 By vijay below old code commented for new combination updation code**/
//				serviceSchedulePopUp.getLbServices().setSelectedIndex(0);
//				serviceSchedulePopUp.getOlbServicingBranch().setSelectedIndex(0);
//				ArrayList<ServiceSchedule> filteredList=new ArrayList<ServiceSchedule>();
//				for(ServiceSchedule obj:form.globalScheduleServiceList){
//        			if(obj.getScheduleProBranch().equals(serviceSchedulePopUp.getOlbCustomerBranch().getValue(serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()))){
//        				filteredList.add(obj);
//        			}
//        		}
				
				ArrayList<ServiceSchedule> filteredList=new ArrayList<ServiceSchedule>();
				/** Date 14-04-2018 By vijay below code updated with adavance filter with combination
				 *  and old code added in else block
				 **/
				for(ServiceSchedule obj:form.globalScheduleServiceList){
					
					if(serviceSchedulePopUp.getLbServices().getSelectedIndex()!=0 && serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()!=0){
						if(obj.getScheduleProdName().equals(serviceSchedulePopUp.getLbServices().getValue(serviceSchedulePopUp.getLbServices().getSelectedIndex())) && obj.getServicingBranch().equals(serviceSchedulePopUp.getOlbServicingBranch().getValue(serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()))
								&& obj.getScheduleProBranch().equals(serviceSchedulePopUp.getOlbCustomerBranch().getValue(serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()))){
	        				filteredList.add(obj);
	        			}
					}
					else if(serviceSchedulePopUp.getLbServices().getSelectedIndex()!=0){
						if(obj.getScheduleProdName().equals(serviceSchedulePopUp.getLbServices().getValue(serviceSchedulePopUp.getLbServices().getSelectedIndex())) && obj.getScheduleProBranch().equals(serviceSchedulePopUp.getOlbCustomerBranch().getValue(serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()))){
	        				filteredList.add(obj);
	        			}
					}
					else if(serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()!=0){
						if(obj.getServicingBranch().equals(serviceSchedulePopUp.getOlbServicingBranch().getValue(serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex())) && obj.getScheduleProBranch().equals(serviceSchedulePopUp.getOlbCustomerBranch().getValue(serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()))){
	        				filteredList.add(obj);
	        			}
					}
					else{
						/** Date 14-04-2018 By vijay old code added here **/
						if(obj.getScheduleProBranch().equals(serviceSchedulePopUp.getOlbCustomerBranch().getValue(serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()))){
	        				filteredList.add(obj);
	        			}
					}
        		}
				
        		serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(filteredList);
			}else{
				/** Date 14-04-2018 By vijay below code updated with adavance filter with combination
				 * and old code added in else block
				 **/
				if(serviceSchedulePopUp.getLbServices().getSelectedIndex()!=0 && serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()!=0 ){
					ArrayList<ServiceSchedule> filteredList=new ArrayList<ServiceSchedule>();
					for(ServiceSchedule obj:form.globalScheduleServiceList){
						if(obj.getScheduleProdName().equals(serviceSchedulePopUp.getLbServices().getValue(serviceSchedulePopUp.getLbServices().getSelectedIndex()))
						   && obj.getServicingBranch().equals(serviceSchedulePopUp.getOlbServicingBranch().getValue(serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()))){
	        				filteredList.add(obj);
	        			}
	        		}
	        		serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(filteredList);
				}
				else if(serviceSchedulePopUp.getLbServices().getSelectedIndex()!=0 && serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()==0 ){
					ArrayList<ServiceSchedule> filteredList=new ArrayList<ServiceSchedule>();
					for(ServiceSchedule obj:form.globalScheduleServiceList){
						if(obj.getScheduleProdName().equals(serviceSchedulePopUp.getLbServices().getValue(serviceSchedulePopUp.getLbServices().getSelectedIndex()))){
	        				filteredList.add(obj);
	        			}
	        		}
	        		serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(filteredList);

				}
				else if(serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()!=0 && serviceSchedulePopUp.getLbServices().getSelectedIndex()==0 ){
					ArrayList<ServiceSchedule> filteredList=new ArrayList<ServiceSchedule>();
					for(ServiceSchedule obj:form.globalScheduleServiceList){
						if(obj.getServicingBranch().equals(serviceSchedulePopUp.getOlbServicingBranch().getValue(serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()))){
	        				filteredList.add(obj);
	        			}
	        		}
	        		serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(filteredList);

				}
				// ends here
				else {
//				if(serviceSchedulePopUp.getLbServices().getSelectedIndex()==0
//						&&serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()==0){
					sortSechduleServicesByProdSerNoAndSerSrNo(form.globalScheduleServiceList);
					serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(form.globalScheduleServiceList);
				}
			}	
		}
		if(event.getSource().equals(serviceSchedulePopUp.getOlbServicingBranch())){
			
			serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
			serviceSchedulePopUp.getP_default().setValue(false);
			serviceSchedulePopUp.getP_customise().setValue(false);
			serviceSchedulePopUp.getP_interval().setValue(null);
			serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
			serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
			serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
			serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
			/**
			 * nidhi
			 * 22-05-2018
			 * ((..
			 * for exclude day
			 */
			serviceSchedulePopUp.getE_mondaytofriday().setSelectedIndex(0);
			/**
			 * end
			 */
			if(serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()!=0){
				/** Date 14-04-2018 By vijay below old code commented for new combination updation code**/
//				serviceSchedulePopUp.getLbServices().setSelectedIndex(0);
//				serviceSchedulePopUp.getOlbCustomerBranch().setSelectedIndex(0);
//				ArrayList<ServiceSchedule> filteredList=new ArrayList<ServiceSchedule>();
//				for(ServiceSchedule obj:form.globalScheduleServiceList){
//        			if(obj.getServicingBranch().equals(serviceSchedulePopUp.getOlbServicingBranch().getValue(serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()))){
//        				filteredList.add(obj);
//        			}
//        		}
				
				/** Date 14-04-2018 By vijay below code updated with adavance filter with combination
				 *  and old code added in else block
				 **/
				ArrayList<ServiceSchedule> filteredList=new ArrayList<ServiceSchedule>();
				for(ServiceSchedule obj:form.globalScheduleServiceList){
					if(serviceSchedulePopUp.getLbServices().getSelectedIndex()!=0 && serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()!=0){
						if(obj.getScheduleProdName().equals(serviceSchedulePopUp.getLbServices().getValue(serviceSchedulePopUp.getLbServices().getSelectedIndex())) &&
							obj.getScheduleProBranch().equals(serviceSchedulePopUp.getOlbCustomerBranch().getValue(serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex())) &&
							obj.getServicingBranch().equals(serviceSchedulePopUp.getOlbServicingBranch().getValue(serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()))){
							filteredList.add(obj);
						}
					}else if(serviceSchedulePopUp.getLbServices().getSelectedIndex()!=0 ){
						if(obj.getScheduleProdName().equals(serviceSchedulePopUp.getLbServices().getValue(serviceSchedulePopUp.getLbServices().getSelectedIndex())) &&
								obj.getServicingBranch().equals(serviceSchedulePopUp.getOlbServicingBranch().getValue(serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()))){
							filteredList.add(obj);
						}
					}
					else if(serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()!=0){
						if(obj.getScheduleProBranch().equals(serviceSchedulePopUp.getOlbServicingBranch().getValue(serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()))
								&& obj.getServicingBranch().equals(serviceSchedulePopUp.getOlbServicingBranch().getValue(serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()))){
							filteredList.add(obj);
						}
					}
					else{
						/** Date 14-04-2018 By vijay old code added here **/
						if(obj.getServicingBranch().equals(serviceSchedulePopUp.getOlbServicingBranch().getValue(serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()))){
	        				filteredList.add(obj);
	        			}
					}
			  }
				
        		serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(filteredList);
			}else{
				/** Date 14-04-2018 By vijay below code updated with adavance filter with combination
				 * and old code added in else block
				 **/
				if(serviceSchedulePopUp.getLbServices().getSelectedIndex()!=0 && serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()!=0){
					ArrayList<ServiceSchedule> filteredList=new ArrayList<ServiceSchedule>();
					for(ServiceSchedule obj:form.globalScheduleServiceList){
						if(obj.getScheduleProdName().equals(serviceSchedulePopUp.getLbServices().getValue(serviceSchedulePopUp.getLbServices().getSelectedIndex()))
							&& obj.getScheduleProBranch().equals(serviceSchedulePopUp.getOlbCustomerBranch().getValue(serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()))	){
	        				filteredList.add(obj);
	        			}
	        		}
	        		serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(filteredList);
				}
				else if(serviceSchedulePopUp.getLbServices().getSelectedIndex()!=0 && serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()==0 ){
					ArrayList<ServiceSchedule> filteredList=new ArrayList<ServiceSchedule>();
					for(ServiceSchedule obj:form.globalScheduleServiceList){
						if(obj.getScheduleProdName().equals(serviceSchedulePopUp.getLbServices().getValue(serviceSchedulePopUp.getLbServices().getSelectedIndex()))){
	        				filteredList.add(obj);
	        			}
	        		}
	        		serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(filteredList);
				}
				else if(serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()!=0  && serviceSchedulePopUp.getLbServices().getSelectedIndex()==0 ){
					ArrayList<ServiceSchedule> filteredList=new ArrayList<ServiceSchedule>();
					for(ServiceSchedule obj:form.globalScheduleServiceList){
						if(obj.getScheduleProdName().equals(serviceSchedulePopUp.getLbServices().getValue(serviceSchedulePopUp.getLbServices().getSelectedIndex()))){
	        				filteredList.add(obj);
	        			}
	        		}
	        		serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(filteredList);

				}else{
//				if(serviceSchedulePopUp.getLbServices().getSelectedIndex()==0
//						&&serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()==0){
					sortSechduleServicesByProdSerNoAndSerSrNo(form.globalScheduleServiceList);
					serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(form.globalScheduleServiceList);
				}
			}
		}
		/**
		 * End
		 *//**
		 * nidhi
		 * 22-05-2018
		 * ((..
		 * for exclude daye
		 */
		else if(event.getSource().equals(serviceSchedulePopUp.getE_mondaytofriday())){
			System.out.println("on chane exclude sunday");
			
			if(serviceSchedulePopUp.getE_mondaytofriday().getSelectedIndex()!=0){
				if(serviceSchedulePopUp.getE_mondaytofriday().getValue(serviceSchedulePopUp.getE_mondaytofriday().getSelectedIndex()).equals(AppConstants.HOLIDAYS)){
					if(serviceSchedulePopUp.getOlbcalendar().getSelectedIndex()==0){
						form.showDialogMessage("please select calendar");
						serviceSchedulePopUp.getE_mondaytofriday().setSelectedIndex(0);
						return;
					}
				}
			}
			
			serviceSchedulePopUp.getP_default().setValue(false);
			serviceSchedulePopUp.getP_customise().setValue(false);
//			serviceSchedulePopUp.getP_interval().setValue(null);
			serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
			serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
			serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
			serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
			serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);

			serviceSchedulePopUp.getP_servicehours().setEnabled(true);
			serviceSchedulePopUp.getP_servicemin().setEnabled(true);
			serviceSchedulePopUp.getP_ampm().setEnabled(true);
			serviceSchedulePopUp.getP_customise().setEnabled(true);
			
			/**
			 * Date : 23-11-2017 BY ANIL
			 * For week selection with advance filter
			 */
			if(isAdvanceFilterSelected()){
				System.out.println("111111111");
				if(serviceSchedulePopUp.getE_mondaytofriday().getSelectedIndex()!=0){
				List<ServiceSchedule> serSchList=getUpdatedScheduledServices(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList(),"ExcludeDay",
						serviceSchedulePopUp.getP_mondaytofriday().getSelectedIndex(),
						serviceSchedulePopUp.getP_serviceWeek().getSelectedIndex(),serviceSchedulePopUp.getE_mondaytofriday().getSelectedIndex());
				updateServiceScheduleList(serSchList,form.globalScheduleServiceList);
				serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serSchList);
				serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
				}
				
			}else{
				System.out.println("222222222");
				reactOnExcludeDay();
				serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
			}
		}
		
	}

	/**
	 * Date : 08-03-2017
	 * added by vijay
	 * for scheduling week wise services 
	 * by default Monday is service day of that specific week
	 * requirement from PCAAMB
	 */
	
	/**
	 * Here i am scheduling service days by week of month and day of week
	 * if we not selected any day then service day is scheduled by monday of week
	 * if we select both day and week and service date scheduled by as per selected day and week
	 */
	
	private void reactOnWeekList(int selectedIndex, int selectedWeekindex,int selectedExcludDay) {
		
		
		/**
		 * @author Vijay Date :-  07-09-2021
		 *  Des :- Service Scheduling with new single common method with new round up logic for innovative
		 */
		if(LoginPresenter.serviceScheduleNewLogicFlag){
			if(selectedWeekindex==0){
				reactforTable();
				serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
				selectedIndex = 0;
				selectedExcludDay = 0;
			}else{ 
				
				 List<SalesLineItem> salesitem = new ArrayList<SalesLineItem>();
				 salesitem = form.getSaleslineitemtable().getDataprovider().getList();
				 List<ServiceSchedule> serschelist=form.getServiceSchedulelist(AppConstants.WEEK,salesitem,form.dbContractDate.getValue(),form.customerbranchlist,form.olbbBranch.getValue(),selectedExcludDay,selectedIndex,selectedWeekindex,0,serviceSchedulePopUp);
			
				 serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
				 form.globalScheduleServiceList=new ArrayList<ServiceSchedule>();
				 form.globalScheduleServiceList=serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
				 
			}
			 return;
		}	
		
		if(selectedIndex==0 && selectedWeekindex==0 && selectedExcludDay == 0){
			reactforTable();
			serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
		}else{ 	
			boolean unitflag = false;
			List<SalesLineItem> salesItemList = form.saleslineitemtable.getDataprovider().getList();
			ArrayList<ServiceSchedule> updatedSchSerList = new ArrayList<ServiceSchedule>();
			updatedSchSerList.clear();
			List<ServiceSchedule> schServList = serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
			
			boolean flag =false;
			boolean greterservicesthanweeks = false;
			boolean sameMonthserviceflag = false;
			Date temp =null;
			
			for (int i = 0; i < salesItemList.size(); i++) {
				int qty = 0;
				qty = (int) salesItemList.get(i).getQty();
				for (int k = 0; k < qty; k++) {

					long noServices = (long) (salesItemList.get(i).getNumberOfServices());
					int noOfdays = salesItemList.get(i).getDuration();
					int interval = (int) (noOfdays / salesItemList.get(i).getNumberOfServices());

					Date servicedate = salesItemList.get(i).getStartDate();
					Date d = new Date(servicedate.getTime());
					
					int months = noOfdays/30;
					if(noServices>months){
						greterservicesthanweeks = true;
						form.showDialogMessage("Services greater than availabe weeks can not schedule services with weeks");
						serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
						break;
					}

					Date productEndDate = new Date(servicedate.getTime());
					CalendarUtil.addDaysToDate(productEndDate, noOfdays);
					Date prodenddate = new Date(productEndDate.getTime());
					productEndDate = calculateEndDate(servicedate,salesItemList.get(i).getDuration()-1 );
					
					/**
					 * nidhi
					 *  ))..
					 *  for exclude day.
					 */
					int startday = servicedate.getDay();
					
					if(selectedExcludDay !=0  && startday == selectedExcludDay-1){
						CalendarUtil.addDaysToDate(servicedate, 1);
					}
//					Date d = new Date(servicedate.getTime());
					
					if(selectedExcludDay !=0 ){
						CalendarUtil.addDaysToDate(productEndDate, -1);
					}
					
					int lastday = productEndDate.getDay();
					
					if(selectedExcludDay !=0  && lastday == selectedExcludDay-1){
						CalendarUtil.addDaysToDate(productEndDate, -1);
					}
					/**
					 * end
					 */
					
					
					for (int j = 0; j < noServices; j++) {

						ServiceSchedule scheduleEntity = new ServiceSchedule();

						Date Stardaate = null;
						Stardaate = salesItemList.get(i).getStartDate();
						scheduleEntity.setSerSrNo(salesItemList.get(i).getProductSrNo());
						scheduleEntity.setScheduleStartDate(Stardaate);
						scheduleEntity.setScheduleProdId(salesItemList.get(i).getPrduct().getCount());
						scheduleEntity.setScheduleProdName(salesItemList.get(i).getProductName());
						scheduleEntity.setScheduleDuration(salesItemList.get(i).getDuration());
						scheduleEntity.setScheduleNoOfServices(salesItemList.get(i).getNumberOfServices());
						scheduleEntity.setScheduleServiceNo(j + 1);
						scheduleEntity.setScheduleProdStartDate(Stardaate);
						scheduleEntity.setScheduleProdEndDate(productEndDate);
						scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
						scheduleEntity.setTotalServicingTime(salesItemList.get(i).getServicingTIme());

						String calculatedServiceTime = "";
						if (schServList.size() != 0) {
							calculatedServiceTime = schServList.get(j).getScheduleServiceTime();
						} else {
							calculatedServiceTime = "Flexible";
						}
						scheduleEntity.setScheduleServiceTime(calculatedServiceTime);

						if (form.customerbranchlist.size() == qty) {
							scheduleEntity.setScheduleProBranch(form.customerbranchlist.get(k).getBusinessUnitName());
							if (form.customerbranchlist.get(k).getBranch() != null) {
								scheduleEntity.setServicingBranch(form.customerbranchlist.get(k).getBranch());
							} else {
								scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
							}
						} else if (form.customerbranchlist.size() == 0) {
							scheduleEntity.setScheduleProBranch("Service Address");
							scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
						} else {
							scheduleEntity.setScheduleProBranch(null);
						}
						Date day = new Date(d.getTime());
						
						/**
						 * Date 1 jun 2017 added by vijay for service getting same month 
						 */
						if(temp!=null){
							String currentServiceMonth =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( day ).split("-")[1];
							String lastserviceDateMonth = DateTimeFormat.getFormat( "d-M-yyyy" ).format( temp ).split("-")[1];
							if(currentServiceMonth.equals(lastserviceDateMonth)){
								sameMonthserviceflag =true;
							}
						}
						if(sameMonthserviceflag){
							CalendarUtil.addDaysToDate(day, 30);
						}
						/**
						 * ends here
						 */
						
						CalendarUtil.setToFirstDayOfMonth(day);
						
						if (selectedWeekindex == 1) {
							day = AppUtility.getCalculatedDate(day,selectedIndex);
							if (day.before(Stardaate)) {
								if (j == 0) {
									day = Stardaate;
								} else {
									flag = true;
								}
							}
							if (flag || j > 0) {
								day = AppUtility.getCalculatedDate(day,selectedIndex);
							}
						}else if (selectedWeekindex == 2) {
							CalendarUtil.addDaysToDate(day, 7);
							day = AppUtility.getCalculatedDate(day,selectedIndex);
							if (day.before(Stardaate)) {
								if (j == 0) {
									day = Stardaate;
								} else {
									flag = true;
								}
							}
							if (flag || j > 0) {
								CalendarUtil.setToFirstDayOfMonth(day);
								CalendarUtil.addDaysToDate(day, 7);
								day = AppUtility.getCalculatedDate(day,selectedIndex);
							}
						}else if (selectedWeekindex == 3) {
							CalendarUtil.addDaysToDate(day, 14);
							day = AppUtility.getCalculatedDate(day,
									selectedIndex);
							if (day.before(Stardaate)) {
								if (j == 0) {
									day = Stardaate;
								} else {
									flag = true;
								}
							}
							if (flag || j > 0) {
								CalendarUtil.setToFirstDayOfMonth(day);
								CalendarUtil.addDaysToDate(day, 14);
								day = AppUtility.getCalculatedDate(day,selectedIndex);
							}
						}else if (selectedWeekindex == 4) {
							CalendarUtil.addDaysToDate(day, 21);
							day = AppUtility.getCalculatedDate(day,selectedIndex);
							if (day.before(Stardaate)) {
								if (j == 0) {
									day = Stardaate;
								} else {
									flag = true;
								}
							}
							if (flag || j > 0) {
								CalendarUtil.setToFirstDayOfMonth(day);
								CalendarUtil.addDaysToDate(day, 21);
								day = AppUtility.getCalculatedDate(day,selectedIndex);
							}
						}
						temp = day;
						 
						/**
						 * Date 26 jun 2017 added by vijay for issues problems in 3 and 4 service gap 
						 */
						/**
						 * nidhi
						 *  ))..
						 *  for exclude day.
						 */
						int serDay = day.getDay();
						if(selectedExcludDay !=0  && serDay == selectedExcludDay-1){
							CalendarUtil.addDaysToDate(day, 1);
//							d = day;
						}
						/**
						 * end
						 */
						
						System.out.println(productEndDate.before(day) == false);
						if (productEndDate.before(day) == false) {
							scheduleEntity.setScheduleServiceDate(day);
						} else {
							scheduleEntity.setScheduleServiceDate(productEndDate);
							
						}
						CalendarUtil.addDaysToDate(d, interval);
						int days =0;
						String serviceStartDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( Stardaate ).split("-")[0];
						if(serviceStartDate.equals("1")){
							String currentServiceDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( d ).split("-")[0];
							days = AppUtility.checkDateMonthOFEndAndAddDays(currentServiceDate,d);
							CalendarUtil.addDaysToDate(d, days);
						}
						Date tempdate = new Date(d.getTime());
						d = tempdate;
						/**
						 * ends here
						 */
						
						scheduleEntity.setScheduleServiceDay(ContractForm.serviceDay(scheduleEntity.getScheduleServiceDate()));
						scheduleEntity.setServiceRemark(salesItemList.get(i).getRemark());
						/**
						 * Date 03-04-2017 added by vijay for week number
						 */
						int weeknumber = AppUtility.getweeknumber(scheduleEntity.getScheduleServiceDate());
						scheduleEntity.setWeekNo(weeknumber);
						/**
						 * ends here
						 */
						
						 /***
						  * nidhi *:*:*
						  * 18-09-2018
						  * for map bill product
						  * if any update please update in else conditions also
						  */
						 if(LoginPresenter.billofMaterialActive){
							 
							 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(salesItemList.get(i).getPrduct());
							 if(billPrDt!=null){
								 
								 
								 UnitConversionUtility unitConver = new UnitConversionUtility();

								 
								 if(form.customerbranchlist.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, form.customerbranchlist.get(k).getUnitOfMeasurement())){
									 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
									 seList.add(scheduleEntity);
									 seList = unitConver.getServiceitemProList(seList,salesItemList.get(i).getPrduct(),salesItemList.get(i),billPrDt);
									 if(seList!=null && seList.size()>0)
										 scheduleEntity = seList.get(0);
									 
								 }else{
									 unitflag = true;
//									 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
								 }
								 
								 
								 
								 
							 }
						 }
						 /**
						  * end
						  */
						 if(schServList.get(j).getServiceDuration()!=0){
							 scheduleEntity.setServiceDuration(schServList.get(j).getServiceDuration());
						 }
						 
						updatedSchSerList.add(scheduleEntity);
					}
				}

				if(greterservicesthanweeks){
					break;
				}
				serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(updatedSchSerList);
				/**
				 * Date : 24-11-2017 BY ANIL
				 */
				form.globalScheduleServiceList=serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
				/**
				 * End
				 */
			}
			if(unitflag){
				form.showDialogMessage("Unit of service are and product group detail area not mapped at branch level. Or conversion not available");
			}
		}
	}
	
	/**
	 * ends here
	 */
	
	@Override
	public void onValueChange(ValueChangeEvent<Integer> event) {
		if (event.getSource() == serviceSchedulePopUp.getP_interval()) {
			/**
			 * Date : 24-11-2017 BY ANIL
			 * For day selection with advance filter
			 */
			if(isAdvanceFilterSelected()){
				List<ServiceSchedule> serSchList=getUpdatedScheduledServices(serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList(),"Interval",0,0,0);
				updateServiceScheduleList(serSchList,form.globalScheduleServiceList);
				serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serSchList);
				serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
				
			}else{
				valueChangeMethodLogicForScheduling();
			}
//			valueChangeMethodLogicForScheduling();
			/**
			 * End
			 */
		}
		serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
	}

	private void reactOnWorkOrder() {

		final WorkOrderForm form = WorkOrderPresenter.initialize();
		final WorkOrder wo = new WorkOrder();

		wo.setOrderId(model.getCount());
		wo.setOrderDate(model.getContractDate());
		if (model.getRefNo() != null)
			wo.setRefNum(model.getRefNo());
		if (model.getRefDate() != null)
			wo.setRefDate(model.getRefDate());
		wo.setBranch(model.getBranch());
		wo.setSalesPerson(model.getEmployee());
		wo.setApproverName(model.getApproverName());

		List<SalesLineItem> productList = model.getItems();
		wo.setBomTable(productList);

		form.showWaitSymbol();

		Timer t = new Timer() {
			@Override
			public void run() {
				form.setToNewState();
				form.updateView(wo);
				form.getIbOrderId().setEnabled(false);
				form.getDbOrderDate().setEnabled(false);
				form.getOblSalesPerson().setEnabled(false);
				form.getOblWoBranch().setEnabled(false);
				form.getOblSalesPerson().setEnabled(false);
				form.getUpload().getCancel().setVisible(false);
				form.getOblWoApproverName().setEnabled(false);
				form.getProdInfoComp().setEnabled(false);
				if (model.getRefNo() != null)
					form.getTbRefNumId().setEnabled(false);
				if (model.getRefDate() != null)
					form.getDbRefDate().setEnabled(false);
				form.getLbComProdId().clear();
				form.getLbComProdId().addItem("--SELECT--");
				for (int i = 0; i < model.getItems().size(); i++) {
					form.flage = false;
					form.getBillOfProductMaterial(model.getItems().get(i)
							.getPrduct().getCount(), model.getItems().get(i)
							.getProductName(),
							model.getItems().get(i).getQty(), false);
					form.getLbComProdId()
							.addItem(
									model.getItems().get(i).getPrduct()
											.getCount()
											+ "");
				}
				form.hideWaitSymbol();
			}
		};
		t.schedule(5000);
	}

	/** NOTE : For Service Schedule Logic . Any change add here */
	/************************************** Service Scheduling Logic ************************************/
	/************************ Service Schedule Logic Starts Here **************************/

	private void reactforreset() {

		serviceSchedulePopUp.getP_default().setValue(false);
		serviceSchedulePopUp.getP_interval().setValue(null);
		serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
		serviceSchedulePopUp.getP_customise().setValue(false);

		serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
		serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
		serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);

		serviceSchedulePopUp.getP_customise().setEnabled(false);
		serviceSchedulePopUp.getP_servicehours().setEnabled(false);
		serviceSchedulePopUp.getP_servicemin().setEnabled(false);
		serviceSchedulePopUp.getP_ampm().setEnabled(false);

		serviceSchedulePopUp.getPopScheduleTable().connectToLocal();
	}

	private void reactforcustumize() {

		// if(specificday==1){
		// form.showDialogMessage("You Selected Specific Day is Remove");
		// specificday=0;
		// }

		serviceSchedulePopUp.getP_default().setValue(false);
		serviceSchedulePopUp.getP_interval().setValue(null);
		// serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);

		serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
		serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
		serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);

	}

	// private boolean validateProductList(){
	// List<ServiceSchedule> updatedSerList=new ArrayList<ServiceSchedule>();
	// List<SalesLineItem>
	// servlist=form.getSaleslineitemtable().getDataprovider().getList();
	//
	// List<ServiceSchedule>
	// serviceitemlist=form.f_sstable.getDataprovider().getList();
	// System.out.println("size of salesline is from the form111 ==== "+servlist.size()
	// );
	// System.out.println("size of service is from the form ==== "+serviceitemlist.size()
	// );
	//
	// int size=serviceitemlist.size();
	//
	// for(int i=0;i<servlist.size();i++){
	// for(int j=0;j<serviceitemlist.size();j++){
	// System.out.println(" Sales product id  == "+i+" "+servlist.get(i).getPrduct().getCount());
	// System.out.println(" Service product id  == "+j+" "+serviceitemlist.get(j).getProd_id());
	//
	// if(servlist.get(i).getPrduct().getCount()!=serviceitemlist.get(j).getProd_id()){
	// System.out.println("Service List Size ==  "+serviceitemlist.size());
	// // serviceitemlist.remove(j);
	// //
	// System.out.println("Service List Size After ==  "+serviceitemlist.size());
	// // size=size-1;
	// // System.out.println("Inside cond print i ==  "+i);
	// // System.out.println("Inside cond print j ==  "+j);
	// updatedSerList.add(serviceitemlist.get(j));
	// }
	// }
	// }
	//
	// return true;
	//
	// }

	private void reactforTable() {
		List<ServiceSchedule> serviceitemlist = form.serviceScheduleTable.getDataprovider().getList();
		ArrayList<ServiceSchedule> serschelist = new ArrayList<ServiceSchedule>();
		for (int i = 0; i < serviceitemlist.size(); i++) {
			ServiceSchedule scheduleEntity = new ServiceSchedule();
			if (serviceitemlist.get(i).getScheduleStartDate() != null) {
				scheduleEntity.setScheduleStartDate(serviceitemlist.get(i).getScheduleStartDate());
			} else {
				scheduleEntity.setScheduleStartDate(null);
			}
			// rohan added this for pesto india changes
			if (serviceitemlist.get(i).getSerSrNo() != 0) {
				scheduleEntity.setSerSrNo(serviceitemlist.get(i).getSerSrNo());
			} else {
				scheduleEntity.setSerSrNo(0);
			}
			if (serviceitemlist.get(i).getScheduleProdId() != 0) {
				scheduleEntity.setScheduleProdId(serviceitemlist.get(i).getScheduleProdId());
			} else {
				scheduleEntity.setScheduleProdId(0);
			}
			if (!serviceitemlist.get(i).getScheduleProdName().equals("")) {
				scheduleEntity.setScheduleProdName(serviceitemlist.get(i).getScheduleProdName());
			} else {
				scheduleEntity.setScheduleProdName(null);
			}
			if (serviceitemlist.get(i).getScheduleDuration() != 0) {
				scheduleEntity.setScheduleDuration(serviceitemlist.get(i).getScheduleDuration());
			} else {
				scheduleEntity.setScheduleDuration(0);
			}
			if (serviceitemlist.get(i).getScheduleNoOfServices() != 0) {
				scheduleEntity.setScheduleNoOfServices(serviceitemlist.get(i).getScheduleNoOfServices());
			} else {
				scheduleEntity.setScheduleNoOfServices(0);
			}
			if (serviceitemlist.get(i).getScheduleServiceNo() != 0) {
				scheduleEntity.setScheduleServiceNo(serviceitemlist.get(i).getScheduleServiceNo());
			} else {
				scheduleEntity.setScheduleServiceNo(0);
			}
			if (serviceitemlist.get(i).getScheduleServiceDate() != null) {
				scheduleEntity.setScheduleServiceDate(serviceitemlist.get(i).getScheduleServiceDate());
			} else {
				scheduleEntity.setScheduleServiceDate(null);
			}
			if (serviceitemlist.get(i).getScheduleProdStartDate() != null) {
				scheduleEntity.setScheduleProdStartDate(serviceitemlist.get(i).getScheduleProdStartDate());
			} else {
				scheduleEntity.setScheduleProdStartDate(null);
			}
			if (serviceitemlist.get(i).getScheduleProdEndDate() != null) {
				scheduleEntity.setScheduleProdEndDate(serviceitemlist.get(i).getScheduleProdEndDate());
			} else {
				scheduleEntity.setScheduleProdEndDate(null);
			}
			if (serviceitemlist.get(i).getScheduleServiceTime() != null) {
				scheduleEntity.setScheduleServiceTime(serviceitemlist.get(i).getScheduleServiceTime());
			} else {
				scheduleEntity.setScheduleServiceTime("Flexible");
			}
			if (serviceitemlist.get(i).getPremisesDetails() != null) {
				scheduleEntity.setPremisesDetails(serviceitemlist.get(i).getPremisesDetails());
			} else {
				scheduleEntity.setPremisesDetails(null);
			}
			if (serviceitemlist.get(i).getScheduleProBranch() != null) {
				scheduleEntity.setScheduleProBranch(serviceitemlist.get(i).getScheduleProBranch());
			} else {
				scheduleEntity.setScheduleProBranch(null);
			}
			if (serviceitemlist.get(i).getScheduleServiceDay() != null) {
				scheduleEntity.setScheduleServiceDay(serviceitemlist.get(i).getScheduleServiceDay());
			} else {
				scheduleEntity.setScheduleServiceDay(null);
			}
			if (serviceitemlist.get(i).getServicingBranch() != null) {
				scheduleEntity.setServicingBranch(serviceitemlist.get(i).getServicingBranch());
			} else {
				scheduleEntity.setServicingBranch(null);
			}
			if (serviceitemlist.get(i).getTotalServicingTime() != null) {
				scheduleEntity.setTotalServicingTime(serviceitemlist.get(i).getTotalServicingTime());
			} else {
				scheduleEntity.setServicingBranch(null);
			}
			// vijay
			if(serviceitemlist.get(i).getServiceRemark()!=null){
				scheduleEntity.setServiceRemark(serviceitemlist.get(i).getServiceRemark());
			}else{
				scheduleEntity.setServiceRemark("");
			}
			/**
			 * Date 01-04-2017 added by vijay for week number
			 */
			scheduleEntity.setWeekNo(serviceitemlist.get(i).getWeekNo());
			/**
			 * ends here
			 */
			 /***
			  * nidhi *:*:*
			  * 18-09-2018
			  * for map bill product
			  * if any update please update in else conditions also
			  */
			 if(LoginPresenter.billofMaterialActive && serviceitemlist.get(i).getServiceProductList()!=null){
				 scheduleEntity.setServiceProductList(serviceitemlist.get(i).getServiceProductList());
			 }
			 /**
			  * end
			  */
			 
			 /**
			  * @author Anil
			  * @since 06-06-2020
			  * copying tat details
			  */
			 if(form.complainTatFlag){
				form.copyComponentAndTatDetails(scheduleEntity,serviceitemlist.get(i)); 
			 }
			 if(serviceitemlist.get(i).getServiceDuration()!=0){
				scheduleEntity.setServiceDuration(serviceitemlist.get(i).getServiceDuration());
			 }
 
			System.out.println("Service Duration"+serviceitemlist.get(i).getServiceDuration());	 
			serschelist.add(scheduleEntity);
		}
		
		/** 
		 * Date 1 nov 2017 added by vijay for sorting with product Id and Sr.No
		 */
		Collections.sort(serschelist, new Comparator<ServiceSchedule>() {
			@Override
			public int compare(ServiceSchedule s1, ServiceSchedule s2) {
				Integer productId = s1.getScheduleProdId();
				Integer productId2 = s2.getScheduleProdId();
				return productId.compareTo(productId2);
			}
		});
		Collections.sort(serschelist, new Comparator<ServiceSchedule>() {
			@Override
			public int compare(ServiceSchedule s1, ServiceSchedule s2) {
				Integer srNo1 = s1.getSerSrNo();
				Integer srNo2 = s2.getSerSrNo();
				return srNo1.compareTo(srNo2);
			}
		});
		/**
		 * ends here
		 */
		serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
		/**
		 * Date : 23-11-2017 BY ANIL
		 */
		form.globalScheduleServiceList=new ArrayList<ServiceSchedule>();
		form.globalScheduleServiceList=serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
		/**
		 * End
		 */
	}

	private void reactfordefaultTable() {
		serviceSchedulePopUp.getP_customise().setValue(false);
		serviceSchedulePopUp.getP_interval().setValue(null);
		serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);

		serviceSchedulePopUp.getP_customise().setEnabled(true);
		serviceSchedulePopUp.getP_servicehours().setEnabled(true);
		serviceSchedulePopUp.getP_servicemin().setEnabled(true);
		serviceSchedulePopUp.getP_ampm().setEnabled(true);

		serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
		serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
		serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
		//date 04-04-2017 added by vijay
		serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);

		
		/**
		 * @author Vijay Date :-  07-09-2021
		 *  Des :- Service Scheduling with new single common method with new round up logic for innovative
		 */
		if(LoginPresenter.serviceScheduleNewLogicFlag){
				
			 List<SalesLineItem> salesitem = new ArrayList<SalesLineItem>();
			 salesitem = form.getSaleslineitemtable().getDataprovider().getList();
			 List<ServiceSchedule> serschelist=form.getServiceSchedulelist("Default",salesitem,form.dbContractDate.getValue(),form.customerbranchlist,form.olbbBranch.getValue(),0,0,0,0,serviceSchedulePopUp);
		
			 serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
			 form.globalScheduleServiceList=new ArrayList<ServiceSchedule>();
			 form.globalScheduleServiceList=serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
			return;
		}
		
		List<SalesLineItem> salesitemlis = form.saleslineitemtable.getDataprovider().getList();
		ArrayList<ServiceSchedule> serschelist = new ArrayList<ServiceSchedule>();
		serschelist.clear();

		List<ServiceSchedule> popuptablelist = serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
		boolean unitFlag = false;
		for (int i = 0; i < salesitemlis.size(); i++) {
			/**
			 * Date : 29-03-2017 By ANil
			 */
			boolean branchSchFlag=false;
			int qty=0;
			/**
			 * Date : 03-04-2017 By ANIL
			 */
//			ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(salesitemlis.get(i).getBranchSchedulingInfo());
			
			/**
			 * Date 02-05-2018 
			 * Developer : Vijay
			 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
			 * so i have updated below code using hashmap.
			 */
			
//			/**
//			 * Date : 12-06-2017 BY ANIL
//			 */
//			String[] branchArray=new String[10];
//			
//			if(salesitemlis.get(i).getBranchSchedulingInfo()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo().equals("")){
//				branchArray[0]=salesitemlis.get(i).getBranchSchedulingInfo();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo1()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo1().equals("")){
//				branchArray[1]=salesitemlis.get(i).getBranchSchedulingInfo1();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo2()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo2().equals("")){
//				branchArray[2]=salesitemlis.get(i).getBranchSchedulingInfo2();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo3()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo3().equals("")){
//				branchArray[3]=salesitemlis.get(i).getBranchSchedulingInfo3();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo4()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo4().equals("")){
//				branchArray[4]=salesitemlis.get(i).getBranchSchedulingInfo4();
//			}
//			
//			if(salesitemlis.get(i).getBranchSchedulingInfo5()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo5().equals("")){
//				branchArray[5]=salesitemlis.get(i).getBranchSchedulingInfo5();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo6()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo6().equals("")){
//				branchArray[6]=salesitemlis.get(i).getBranchSchedulingInfo6();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo7()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo7().equals("")){
//				branchArray[7]=salesitemlis.get(i).getBranchSchedulingInfo7();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo8()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo8().equals("")){
//				branchArray[8]=salesitemlis.get(i).getBranchSchedulingInfo8();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo9()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo9().equals("")){
//				branchArray[9]=salesitemlis.get(i).getBranchSchedulingInfo9();
//			}
//			
//			/**
//			 * End
//			 */
//			ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(branchArray);
			/**
			 * ends here
			 */
			
			/** Date 02-05-2018 By vijay for updated customer branch scheduling using hashmap **/
			ArrayList<BranchWiseScheduling> branchSchedulingList = salesitemlis.get(i).getCustomerBranchSchedulingInfo().get(salesitemlis.get(i).getProductSrNo());
			
			
			if(branchSchedulingList!=null
					&&branchSchedulingList.size()!=0){
				branchSchFlag=true;
				qty=branchSchedulingList.size();
			}else{
				qty=(int) salesitemlis.get(i).getQty();
			}
			
			/**
			 * End
			 */
			if(branchSchFlag){
				
				for (int k = 0; k < qty; k++) {
					if(branchSchedulingList.get(k).isCheck()==true){
						
					long noServices = (long) (salesitemlis.get(i).getNumberOfServices());
					int noOfdays = salesitemlis.get(i).getDuration();
					/********* below one line commented by vijay this interval calculation missing precision so services not getting created still end date ***********/
//					int interval = (int) (noOfdays / salesitemlis.get(i).getNumberOfServices());
					
					/*** Date 28 sep 2017 below code added by vijay interval calculation with precision and using roundoff ******************/
					double days =  noOfdays;
					double noofservices= salesitemlis.get(i).getNumberOfServices();
					double interval =  days / noofservices;
					double temp = interval;
					double tempvalue=interval;
					/******************** ends here ********************/
					
					Date servicedate = salesitemlis.get(i).getStartDate();
					Date d = new Date(servicedate.getTime());
					Date productEndDate = new Date(servicedate.getTime());
					CalendarUtil.addDaysToDate(productEndDate, noOfdays);
					Date prodenddate = new Date(productEndDate.getTime());
					productEndDate = prodenddate;
					Date tempdate = new Date();
					for (int j = 0; j < noServices; j++) {
							
						ServiceSchedule ssch = new ServiceSchedule();
						Date Stardaate = salesitemlis.get(i).getStartDate();

						ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
						ssch.setScheduleStartDate(Stardaate);
						ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
						ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
						ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
						ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
						ssch.setScheduleServiceNo(j + 1);
						ssch.setScheduleProdStartDate(Stardaate);
						ssch.setScheduleProdEndDate(productEndDate);
						ssch.setScheduleServiceTime("Flexible");

						ssch.setServicingBranch(form.olbbBranch.getValue());
						ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());

						
					
						ssch.setScheduleProBranch(branchSchedulingList.get(k).getBranchName());
						if (branchSchedulingList.get(k).getServicingBranch()!=null) {
							ssch.setServicingBranch(branchSchedulingList.get(k).getServicingBranch());
						} 
						
						int dayIndex=AppUtility.getDayIndex(branchSchedulingList.get(k).getDay());
						
						if(dayIndex!=0){
							String dayOfWeek1 = format.format(d);
							int adate = Integer.parseInt(dayOfWeek1);
							int adday1 = dayIndex - 1;
							int adday = 0;
							if (adate <= adday1) {
								adday = (adday1 - adate);
							} else {
								adday = (7 - adate + adday1);
							}
							
							Date newDate = new Date(d.getTime());
							CalendarUtil.addDaysToDate(newDate, adday);
							Date day = new Date(newDate.getTime());

							if (productEndDate.before(day) == false) {
								ssch.setScheduleServiceDate(day);
							} else {
								ssch.setScheduleServiceDate(productEndDate);
							}

							/*** added by vijay for interval calculation *************/
							int interval2 =(int) Math.round(interval);
							
							
							CalendarUtil.addDaysToDate(d, interval2);
							Date tempdate1 = new Date(d.getTime());
							d = tempdate1;
							ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
							
							/************* added by vijay for scheduling interval calculation with including precision using roundoff*********************/
							interval =(interval+temp)-tempvalue;
						    tempvalue = Math.round(interval);
						    /**************** ends here **********************************/
						    
						}else{
							
							/*** added by vijay for interval calculation *************/
							int interval2 =(int) Math.round(interval);
							
							if (j > 0) {
								CalendarUtil.addDaysToDate(d, interval2);
								tempdate = new Date(d.getTime());
								
								/************* added by vijay for scheduling interval including calculation using roundoff *********************/
								interval =(interval+temp)-tempvalue;
							    tempvalue = Math.round(interval);
							    /**************** ends here **********************************/
							}
							
							if (j == 0) {
								ssch.setScheduleServiceDate(servicedate);
							}
							if (j != 0) {
								if (productEndDate.before(d) == false) {
									ssch.setScheduleServiceDate(tempdate);
								} else {
									ssch.setScheduleServiceDate(productEndDate);
								}
							}
						}
						

//						ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
						ssch.setServiceRemark(salesitemlis.get(i).getRemark());
						
						/**
						 * Date 01-04-2017 added by vijay for week number
						 */
						int weaknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
						ssch.setWeekNo(weaknumber);
						
						/**
						 * ends here
						 */
						/***
						  * nidhi *:*:*
						  * 18-09-2018
						  * for map bill product
						  * if any update please update in else conditions also
						  */
						 if(LoginPresenter.billofMaterialActive){
							 
							 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(salesitemlis.get(i).getPrduct());
							 if(billPrDt!=null){
								 
								 
								 UnitConversionUtility unitConver = new UnitConversionUtility();

								 
								 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
									 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
									 seList.add(ssch);
									 seList = unitConver.getServiceitemProList(seList,salesitemlis.get(i).getPrduct(),salesitemlis.get(i),billPrDt);
									 if(seList!=null && seList.size()>0)
										 ssch = seList.get(0);
									 
								 }else{
									 unitFlag = true;
//									 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
								 }
								 
								 
								 
								 
							 }
						 }
						 /**
						  * end
						  */
						 
						 /**
						  * @author Vijay Chougule Date :- 09-09-2020
						  * Des :- added Service duration 
						  */
						 if(branchSchedulingList.get(k).getServiceDuration()!=0){
							 ssch.setServiceDuration(branchSchedulingList.get(k).getServiceDuration());
						 }
						 /**
						  * ends here
						  */
						 
						serschelist.add(ssch);
					}
				}
				}
			
			}else{
			
//			int qty=0;
//			qty=(int) salesitemlis.get(i).getQty();
			for (int k = 0; k < qty; k++) {
				long noServices = (long) (salesitemlis.get(i).getNumberOfServices());
				int noOfdays = salesitemlis.get(i).getDuration();
				/********* below one line commented by vijay this interval calculation missing precision so services not getting created still end date ***********/
//				int interval = (int) (noOfdays / salesitemlis.get(i).getNumberOfServices());
				
				/*** Date 28 sep 2017 below code added by vijay interval calculation with precision and using roundoff ******************/
				double days =  noOfdays;
				double noofservices= salesitemlis.get(i).getNumberOfServices();
				double interval =  days / noofservices;
				double temp = interval;
				double tempvalue=interval;
				/******************** ends here ********************/
				
				calculateEndDate(salesitemlis.get(i).getStartDate(),salesitemlis.get(i).getDuration());
				Date servicedate = salesitemlis.get(i).getStartDate();
				
				Date d = new Date(servicedate.getTime());
				Date tempdate = new Date();
				for (int j = 0; j < noServices; j++) {

					/*** added by vijay for interval calculation *************/
					int interval2 =(int) Math.round(interval);
					
					if (j > 0) {
						CalendarUtil.addDaysToDate(d, interval2);
						tempdate = new Date(d.getTime());
						
						/************* added by vijay *********************/
						interval =(interval+temp)-tempvalue;
					    tempvalue = Math.round(interval);
					    /************** ends here ************************/
					
					}

					ServiceSchedule ssch = new ServiceSchedule();

					// rohan added this 1 field
					ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());

					Date Stardaate = null;
					Stardaate = salesitemlis.get(i).getStartDate();
					ssch.setScheduleStartDate(Stardaate);
					ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
					ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
					ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
					ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
					ssch.setScheduleServiceNo(j + 1);
					ssch.setScheduleProdStartDate(Stardaate);

					ssch.setServicingBranch(form.olbbBranch.getValue());
					ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());

					String calculatedServiceTime = "";
					if (popuptablelist.size() != 0) {
						calculatedServiceTime = popuptablelist.get(j).getScheduleServiceTime();
					} else {
						calculatedServiceTime = "Flexible";
					}

					ssch.setScheduleServiceTime(calculatedServiceTime);

					if (form.customerbranchlist.size() == qty) {
						ssch.setScheduleProBranch(form.customerbranchlist.get(k).getBusinessUnitName());

						/**
						 * rohan added this code for automatic servicing branch
						 * setting for NBHC
						 */
						if (form.customerbranchlist.get(k).getBranch() != null) {
							ssch.setServicingBranch(form.customerbranchlist.get(k).getBranch());
						} else {
							ssch.setServicingBranch(form.olbbBranch.getValue());
						}
						/**
						 * ends here
						 */

					} else if (form.customerbranchlist.size() == 0) {
						ssch.setScheduleProBranch("Service Address");

						/**
						 * rohan added this code for automatic servicing branch
						 * setting for NBHC
						 */
						ssch.setServicingBranch(form.olbbBranch.getValue());
						/**
						 * ends here
						 */

					} else {
						ssch.setScheduleProBranch("");
					}

					ssch.setScheduleProdEndDate(calculateEndDate(salesitemlis.get(i).getStartDate(), salesitemlis.get(i).getDuration()));
					if (j == 0) {
						ssch.setScheduleServiceDate(servicedate);
					}
					if (j != 0) {
						if (calculateEndDate(salesitemlis.get(i).getStartDate(),salesitemlis.get(i).getDuration()).before(d) == false) {
							ssch.setScheduleServiceDate(tempdate);
						} else {
							ssch.setScheduleServiceDate(calculateEndDate(salesitemlis.get(i).getStartDate(),salesitemlis.get(i).getDuration()));
						}
					}
					ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
					ssch.setServiceRemark(salesitemlis.get(i).getRemark());
					
					/**
					 * Date 01-04-2017 added by vijay for week number
					 */
					int weaknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
					ssch.setWeekNo(weaknumber);
					
					/**
					 * ends here
					 */
					 /***
					  * nidhi *:*:*
					  * 18-09-2018
					  * for map bill product
					  * if any update please update in else conditions also
					  */
					 if(LoginPresenter.billofMaterialActive){
						 
						 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(salesitemlis.get(i).getPrduct());
						 if(billPrDt!=null){
							 
							 
							 UnitConversionUtility unitConver = new UnitConversionUtility();

							 
							 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
								 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
								 seList.add(ssch);
								 seList = unitConver.getServiceitemProList(seList,salesitemlis.get(i).getPrduct(),salesitemlis.get(i),billPrDt);
								 if(seList!=null && seList.size()>0)
									 ssch = seList.get(0);
								 
							 }else{
								unitFlag = true; 
//								 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
							 }
							 
							 
							 
							 
						 }
					 }
					 /**
					  * end
					  */
					serschelist.add(ssch);
				}
			}
		}

			/**
			 * nidhi |*|
			 * 7-01-2018
			 */
			if(unitFlag){
				 form.showDialogMessage("Unit of service are and product group detail area not mapped at branch level. Or conversion not available");
			}
			serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
			
			/**
			 * Date : 25-11-2017 BY ANIL
			 */
			form.globalScheduleServiceList=new ArrayList<ServiceSchedule>();
			form.globalScheduleServiceList=serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
			/**
			 * End
			 */
		}
	}

	private Date calculateEndDate(Date servicedt, int noOfdays) {
		Date servicedate = servicedt;
		Date d = new Date(servicedate.getTime());
		Date productEndDate = new Date(servicedate.getTime());
		CalendarUtil.addDaysToDate(productEndDate, noOfdays);
		Date prodenddate = new Date(productEndDate.getTime());
		productEndDate = prodenddate;

		return productEndDate;

	}

	// **********************changes ends here *************************

	private void reactOnOk() {
		/**
		 * Date : 24-11-2017 BY ANIL
		 * setting the data from global list to service schedule table if any of filter is selected 
		 */
//		List<ServiceSchedule> popupSchSerList = this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
		List<ServiceSchedule> popupSchSerList=null;
		if(isAdvanceFilterSelected()&&form.globalScheduleServiceList!=null&&form.globalScheduleServiceList.size()!=0){
			popupSchSerList = new ArrayList<ServiceSchedule>(form.globalScheduleServiceList);
		}else{
			popupSchSerList = this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
		}
		/**
		 * End
		 */
		
		ArrayList<ServiceSchedule> updatedSchSerList = new ArrayList<ServiceSchedule>();
//		List<SalesLineItem> salesItemList = form.saleslineitemtable.getDataprovider().getList();
		Date bigdate = new Date();
		Date smalldate = new Date();

		if (popupSchSerList.size() != 0) {
			if (serviceSchedulePopUp.getP_mondaytofriday().getSelectedIndex() != 0) {
				form.f_serviceDay = serviceSchedulePopUp.getP_mondaytofriday().getItemText(serviceSchedulePopUp.getP_mondaytofriday().getSelectedIndex());
			} else {
				form.f_serviceDay = "Not Select";
			}

			if ((serviceSchedulePopUp.getP_servicehours().getSelectedIndex() != 0
					&& serviceSchedulePopUp.getP_servicemin().getSelectedIndex() != 0 
					&& serviceSchedulePopUp.getP_ampm().getSelectedIndex() != 0) || 
					(serviceSchedulePopUp.getP_servicehours().getSelectedIndex() == 0
						&& serviceSchedulePopUp.getP_servicemin().getSelectedIndex() == 0 
						&& serviceSchedulePopUp.getP_ampm().getSelectedIndex() == 0)) {

				int branchflag = 0;

				for (int i = 0; i < popupSchSerList.size(); i++) {
					if (form.customerbranchlist.size() != 0) {
						if (popupSchSerList.get(i).getScheduleProBranch() == null|| popupSchSerList.get(i).getScheduleProBranch().equalsIgnoreCase("SELECT")) {
							branchflag = 1;
						}
					}

					ServiceSchedule sss = new ServiceSchedule();
					// rohan added this 1 field
					sss.setSerSrNo(popupSchSerList.get(i).getSerSrNo());
					sss.setScheduleStartDate(popupSchSerList.get(i).getScheduleStartDate());
					sss.setScheduleProdId(popupSchSerList.get(i).getScheduleProdId());
					sss.setScheduleProdName(popupSchSerList.get(i).getScheduleProdName());
					sss.setScheduleDuration(popupSchSerList.get(i).getScheduleDuration());
					sss.setScheduleNoOfServices(popupSchSerList.get(i).getScheduleNoOfServices());
					sss.setScheduleServiceNo(popupSchSerList.get(i).getScheduleServiceNo());
					if (bigdate.before(popupSchSerList.get(i).getScheduleServiceDate())) {
						bigdate = popupSchSerList.get(i).getScheduleServiceDate();
					}
					if (smalldate.after(popupSchSerList.get(i).getScheduleServiceDate())) {
						smalldate = popupSchSerList.get(i).getScheduleServiceDate();
					}
					sss.setScheduleServiceDate(popupSchSerList.get(i).getScheduleServiceDate());
					sss.setScheduleProdStartDate(popupSchSerList.get(i).getScheduleProdStartDate());
					sss.setScheduleProdEndDate(popupSchSerList.get(i).getScheduleProdEndDate());
					sss.setScheduleServiceTime(popupSchSerList.get(i).getScheduleServiceTime());
					sss.setPremisesDetails(popupSchSerList.get(i).getPremisesDetails());
					sss.setServicingBranch(popupSchSerList.get(i).getServicingBranch());
					sss.setTotalServicingTime(popupSchSerList.get(i).getTotalServicingTime());
					if (popupSchSerList.get(i).getScheduleProBranch() != null) {
						sss.setScheduleProBranch(popupSchSerList.get(i).getScheduleProBranch());
					} else {
						sss.setScheduleProBranch("");
					}
					sss.setScheduleServiceDay(popupSchSerList.get(i).getScheduleServiceDay());
					//vijay
					sss.setServiceRemark(popupSchSerList.get(i).getServiceRemark());
					/**
					 * Date 03-04-2017 added by vijay for week number
					 */
					sss.setWeekNo(popupSchSerList.get(i).getWeekNo());
					/**
					 * ends here
					 */
//					sss.setServiceProductList(serviceSchedulePopUp.getPopScheduleTable()
//							.getDataprovider().getList().get(i).getServiceProductList());
					
					 /***
					  * nidhi *:*:*
					  * 18-09-2018
					  * for map bill product
					  * if any update please update in else conditions also
					  */
					 if(LoginPresenter.billofMaterialActive){
						 sss.setServiceProductList(popupSchSerList.get(i).getServiceProductList());
					 }
					 /**
					  * end
					  */
					 /**
					  * @author Anil
					  * @since 06-06-2020
					  * copying tat details
					  */
					 if(form.complainTatFlag){
						form.copyComponentAndTatDetails(sss,popupSchSerList.get(i)); 
					 }
					 if(popupSchSerList.get(i).getServiceDuration()!=0){
						 sss.setServiceDuration(popupSchSerList.get(i).getServiceDuration());
					 }
					 
					updatedSchSerList.add(sss);
				}
				Date d = new Date(bigdate.getTime());
				Date d1 = new Date(smalldate.getTime());
				CalendarUtil.addDaysToDate(d1, 1);
				if (branchflag == 0) {
					form.getServiceScheduleTable().getDataprovider().setList(updatedSchSerList);
					schedulePanel.hide();
				} else {
					form.showDialogMessage("Please Set Branch By Clicking On Customize");
				}
			} else {
				form.showDialogMessage("Please Schedule Service Time Properly");
			}
		} else {
			form.showDialogMessage("Please Schedule Service Date");
		}

	}

	private void reactToService() {
		if (form.tbContractId.getValue() == null) {
			serviceSchedulePopUp.getP_docid().setValue("");
		} else {
			serviceSchedulePopUp.getP_docid().setValue(form.tbContractId.getValue() + "");
		}
		if (form.dbContractDate.getValue() == null) {
			serviceSchedulePopUp.getP_docdate().setValue(null);
		} else {
			serviceSchedulePopUp.getP_docdate().setValue(form.dbContractDate.getValue());
		}

		serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
		serviceSchedulePopUp.getP_default().setValue(false);
		serviceSchedulePopUp.getP_customise().setValue(false);
		serviceSchedulePopUp.getP_interval().setValue(null);
		serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
		serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
		serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
		// below setter added by vijay on 8-03-2017
		serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
		/**
		 * Date : 23-11-2017 BY ANIL
		 */
		serviceSchedulePopUp.getLbServices().setSelectedIndex(0);
		serviceSchedulePopUp.getOlbCustomerBranch().setSelectedIndex(0);
		serviceSchedulePopUp.getOlbServicingBranch().setSelectedIndex(0);
		/**
		 * End
		 */
		
		serviceSchedulePopUp.getOlbcalendar().setSelectedIndex(0);
		serviceSchedulePopUp.getE_mondaytofriday().setSelectedIndex(0);

		schedulePanel = new PopupPanel(true);

		ScreeenState transactionStatus = AppMemory.getAppMemory().currentState;
		if (transactionStatus.toString().equals("VIEW")) {
			serviceSchedulePopUp.getP_default().setEnabled(false);
			serviceSchedulePopUp.getP_mondaytofriday().setEnabled(false);
			serviceSchedulePopUp.getP_interval().setEnabled(false);
			serviceSchedulePopUp.getP_customise().setEnabled(false);
			serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
			serviceSchedulePopUp.getBtnReset().setEnabled(false);
			serviceSchedulePopUp.getP_servicehours().setEnabled(false);
			serviceSchedulePopUp.getP_servicemin().setEnabled(false);
			serviceSchedulePopUp.getP_ampm().setEnabled(false);
			// below setter added by vijay on 16-03-2017
			serviceSchedulePopUp.getP_serviceWeek().setEnabled(false);
			
			/**
			 * Date : 23-11-2017 BY ANIL
			 */
			serviceSchedulePopUp.getLbServices().setEnabled(false);
			serviceSchedulePopUp.getOlbCustomerBranch().setEnabled(false);
			serviceSchedulePopUp.getOlbServicingBranch().setEnabled(false);
			/**
			 * End
			 */
		}
		if (transactionStatus.toString().equals("EDIT")) {
			serviceSchedulePopUp.getP_default().setEnabled(true);
			serviceSchedulePopUp.getP_mondaytofriday().setEnabled(true);
			serviceSchedulePopUp.getP_interval().setEnabled(true);
			serviceSchedulePopUp.getP_customise().setEnabled(true);
			serviceSchedulePopUp.getPopScheduleTable().setEnable(false);
			serviceSchedulePopUp.getBtnReset().setEnabled(true);
			serviceSchedulePopUp.getP_servicehours().setEnabled(true);
			serviceSchedulePopUp.getP_servicemin().setEnabled(true);
			serviceSchedulePopUp.getP_ampm().setEnabled(true);
			// below setter added by vijay on 16-03-2017
			serviceSchedulePopUp.getP_serviceWeek().setEnabled(true);
			
			/**
			 * Date : 23-11-2017 BY ANIL
			 */
			serviceSchedulePopUp.getLbServices().setEnabled(true);
			serviceSchedulePopUp.getOlbCustomerBranch().setEnabled(true);
			serviceSchedulePopUp.getOlbServicingBranch().setEnabled(true);
			/**
			 * End
			 */
		}

		schedulePanel.add(serviceSchedulePopUp);
		/*
		 * nidhi
		 * 05-08-2017
		 *  for make schedule popup responsive
		 *  style css class declared in SimpleERP.css
		 */
		schedulePanel.getElement().addClassName("schedulepopup");
		/*
		 * end
		 */
		schedulePanel.show();
		schedulePanel.center();
	}

	private void reactOnMondaytoFridayList(int selectedIndex, int selectedServiceWeekIndex) {
		
			/**
			 * @author Vijay Date :-  07-09-2021
			 *  Des :- Service Scheduling with new single common method with new round up logic for innovative
			 */
			if(LoginPresenter.serviceScheduleNewLogicFlag){
				if (selectedIndex == 0) {
					reactforTable();
					serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
				} else {
					
					 List<SalesLineItem> salesitem = new ArrayList<SalesLineItem>();
					 salesitem = form.getSaleslineitemtable().getDataprovider().getList();
					 List<ServiceSchedule> serschelist=form.getServiceSchedulelist(AppConstants.DAY,salesitem,form.dbContractDate.getValue(),form.customerbranchlist,form.olbbBranch.getValue(),0,selectedIndex,selectedServiceWeekIndex,0,serviceSchedulePopUp);
				
					 serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
					 form.globalScheduleServiceList=new ArrayList<ServiceSchedule>();
					 form.globalScheduleServiceList=serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
				}
				return;
			}
			
			if (selectedIndex == 0) {
				reactforTable();
				serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
			} else {
				boolean unitFlag= false;
				specificday = 1;
				List<SalesLineItem> salesitemlis = form.saleslineitemtable.getDataprovider().getList();
				ArrayList<ServiceSchedule> serschelist = new ArrayList<ServiceSchedule>();
				serschelist.clear();
				List<ServiceSchedule> popuptablelist = serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();

				boolean flag =false;
				boolean sameMonthserviceflag = false;
				Date temp =null;
				
				for (int i = 0; i < salesitemlis.size(); i++) {
//					int qty = 0;
//					qty = (int) salesitemlis.get(i).getQty();
					
					/**
					 * Date : 29-03-2017 By ANil
					 */
					boolean branchSchFlag=false;
					int qty=0;
					
					
					/**
					 * Date 02-05-2018 
					 * Developer : Vijay
					 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
					 * so i have updated below code using hashmap.
					 */
					
//					/**
//					 * Date : 03-04-2017 By ANIL
//					 */
////					ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(salesitemlis.get(i).getBranchSchedulingInfo());
//					
//					/**
//					 * Date : 12-06-2017 BY ANIl
//					 */
//					String[] branchArray=new String[10];
//					
//					if(salesitemlis.get(i).getBranchSchedulingInfo()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo().equals("")){
//						branchArray[0]=salesitemlis.get(i).getBranchSchedulingInfo();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo1()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo1().equals("")){
//						branchArray[1]=salesitemlis.get(i).getBranchSchedulingInfo1();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo2()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo2().equals("")){
//						branchArray[2]=salesitemlis.get(i).getBranchSchedulingInfo2();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo3()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo3().equals("")){
//						branchArray[3]=salesitemlis.get(i).getBranchSchedulingInfo3();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo4()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo4().equals("")){
//						branchArray[4]=salesitemlis.get(i).getBranchSchedulingInfo4();
//					}
//					
//					if(salesitemlis.get(i).getBranchSchedulingInfo5()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo5().equals("")){
//						branchArray[5]=salesitemlis.get(i).getBranchSchedulingInfo5();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo6()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo6().equals("")){
//						branchArray[6]=salesitemlis.get(i).getBranchSchedulingInfo6();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo7()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo7().equals("")){
//						branchArray[7]=salesitemlis.get(i).getBranchSchedulingInfo7();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo8()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo8().equals("")){
//						branchArray[8]=salesitemlis.get(i).getBranchSchedulingInfo8();
//					}
//					if(salesitemlis.get(i).getBranchSchedulingInfo9()!=null
//							&&!salesitemlis.get(i).getBranchSchedulingInfo9().equals("")){
//						branchArray[9]=salesitemlis.get(i).getBranchSchedulingInfo9();
//					}
//					/**
//					 * End
//					 */
//					ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(branchArray);
					
					/** Date 02-05-2018 By vijay for updated branch scheduling using hashmap **/
					ArrayList<BranchWiseScheduling> branchSchedulingList = salesitemlis.get(i).getCustomerBranchSchedulingInfo().get(salesitemlis.get(i).getProductSrNo());
					
					/**
					 * ends here
					 */
					
					if(branchSchedulingList!=null
							&&branchSchedulingList.size()!=0){
						branchSchFlag=true;
						qty=branchSchedulingList.size();
					}else{
						qty=(int) salesitemlis.get(i).getQty();
					}
					
					/**
					 * End
					 */
					if(branchSchFlag){
						for (int k = 0; k < qty; k++) {
							if(branchSchedulingList.get(k).isCheck()==true){
							long noServices = (long) (salesitemlis.get(i).getNumberOfServices());
							int noOfdays = salesitemlis.get(i).getDuration();
//							int interval = (int) (noOfdays / salesitemlis.get(i).getNumberOfServices());
							
							/*** Date 28 sep 2017 added by vijay  for scheduling proper calculation with precision number
							 *  for interval  
							 */
							double nodays =  noOfdays;
							double noofservices= salesitemlis.get(i).getNumberOfServices();
							double interval =  nodays / noofservices;
							double tempinterval = interval;
							double tempvalue=interval;
							/******************** ends here ********************/
							
							Date servicedate = salesitemlis.get(i).getStartDate();
							Date d = new Date(servicedate.getTime());
		
							Date productEndDate = new Date(servicedate.getTime());
							CalendarUtil.addDaysToDate(productEndDate, noOfdays);
							Date prodenddate = new Date(productEndDate.getTime());
							productEndDate = prodenddate;
		
							for (int j = 0; j < noServices; j++) {
		
								ServiceSchedule scheduleEntity = new ServiceSchedule();
		
								Date Stardaate = null;
								Stardaate = salesitemlis.get(i).getStartDate();
		
								scheduleEntity.setSerSrNo(salesitemlis.get(i).getProductSrNo());
								scheduleEntity.setScheduleStartDate(Stardaate);
								scheduleEntity.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
								scheduleEntity.setScheduleProdName(salesitemlis.get(i).getProductName());
								scheduleEntity.setScheduleDuration(salesitemlis.get(i).getDuration());
								scheduleEntity.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
								scheduleEntity.setScheduleServiceNo(j + 1);
		
								scheduleEntity.setScheduleProdStartDate(Stardaate);
								scheduleEntity.setScheduleProdEndDate(productEndDate);
		
								scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
								scheduleEntity.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
		
								String calculatedServiceTime = "";
								if (popuptablelist.size() != 0) {
									calculatedServiceTime = popuptablelist.get(j).getScheduleServiceTime();
								} else {
									calculatedServiceTime = "Flexible";
								}
		
								scheduleEntity.setScheduleServiceTime(calculatedServiceTime);
		
//								if (form.customerbranchlist.size() == qty) {
//									scheduleEntity.setScheduleProBranch(form.customerbranchlist.get(k).getBusinessUnitName());
//									if (form.customerbranchlist.get(k).getBranch() != null) {
//										scheduleEntity.setServicingBranch(form.customerbranchlist.get(k).getBranch());
//									} else {
//										scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
//									}
//								} else if (form.customerbranchlist.size() == 0) {
//									scheduleEntity.setScheduleProBranch("Service Address");
//									scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
//								} else {
//									scheduleEntity.setScheduleProBranch(null);
//								}
								
								scheduleEntity.setScheduleProBranch(branchSchedulingList.get(k).getBranchName());
								if (branchSchedulingList.get(k).getServicingBranch()!=null) {
									scheduleEntity.setServicingBranch(branchSchedulingList.get(k).getServicingBranch());
								} 
		
								String dayOfWeek1 = format.format(d);
								int adate = Integer.parseInt(dayOfWeek1);
								int adday1 = selectedIndex - 1;
								
								int adday = 0;
								if (adate <= adday1) {
									adday = (adday1 - adate);
								} else {
									adday = (7 - adate + adday1);
								}
								Date newDate = new Date(d.getTime());
								CalendarUtil.addDaysToDate(newDate, adday);
		
								Date day = new Date(newDate.getTime());
								
								/**
								 * Date 8-03-2017
								 * added by vijay for scheduling service with week day and week
								 */
								
								if(selectedServiceWeekIndex!=0){
									
									/**
									 * Date 1 jun 2017 added by vijay for service getting same month 
									 */
									if(temp!=null){
										String currentServiceMonth =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( day ).split("-")[1];
										String lastserviceDateMonth = DateTimeFormat.getFormat( "d-M-yyyy" ).format( temp ).split("-")[1];
										if(currentServiceMonth.equals(lastserviceDateMonth)){
											sameMonthserviceflag =true;
										}else{
											sameMonthserviceflag= false;
										}
									}
									
									if(sameMonthserviceflag){
										CalendarUtil.addDaysToDate(day, 10);
									}
									/**
									 * ends here
									 */
									
									CalendarUtil.setToFirstDayOfMonth(day);

									if(selectedServiceWeekIndex==1){
									 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										CalendarUtil.setToFirstDayOfMonth(day);
										 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
									}
								 }
								else if(selectedServiceWeekIndex==2){
										CalendarUtil.addDaysToDate(day, 7);
										 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
										if(day.before(Stardaate)){
											if(j==0){
												day =Stardaate;
											}
											else{
												flag = true;
											}
										}
										if(flag || j>0){
											CalendarUtil.setToFirstDayOfMonth(day);
											CalendarUtil.addDaysToDate(day, 7);
											 day = AppUtility.getCalculatedDatewithWeek(day, adday1);;
										}
								}else if(selectedServiceWeekIndex==3){
										CalendarUtil.addDaysToDate(day, 14);
										 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
										if(day.before(Stardaate)){
											if(j==0){
												day =Stardaate;
											}
											else{
												flag = true;
											}
										}
										if(flag || j>0){
											CalendarUtil.setToFirstDayOfMonth(day);
											CalendarUtil.addDaysToDate(day, 14);
											 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
										}
								}
								else if(selectedServiceWeekIndex==4){
										CalendarUtil.addDaysToDate(day, 21);

										 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
										if(day.before(Stardaate)){
											if(j==0){
												day =Stardaate;
											}
											else{
												flag = true;
											}
										}
										if(flag || j>0){
											CalendarUtil.setToFirstDayOfMonth(day);
											CalendarUtil.addDaysToDate(day, 21);
											 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
										}
								 }
									 temp = day;
									 
									 /**
									  * Date 26 jun 2017 added by vijay 
									  * for getting issues in 4 services and 6 services gap  with day selection
									  */
										
										CalendarUtil.addDaysToDate(d, (int)interval);
										int days =0;
										String serviceStartDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( Stardaate ).split("-")[0];
										if(serviceStartDate.equals("1")){
											String currentServiceDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( d ).split("-")[0];
											 days = AppUtility.checkDateMonthOFEndAndAddDays(currentServiceDate,d);
											CalendarUtil.addDaysToDate(d, days);

										}
										/**
										 * ends here
										 */
								}
								/**
								 * ends here
								 */
									
								if (productEndDate.before(day) == false) {
									scheduleEntity.setScheduleServiceDate(day);
								} else {
									scheduleEntity.setScheduleServiceDate(productEndDate);
								}
								
								/*** Date 28 sep 2017 added by vijay for interval calculation *************/
								int interval2 =(int) Math.round(interval);


								//Date 26 jun 2017 aaded by vijay this only when week is not selected
								if(selectedServiceWeekIndex==0){
									CalendarUtil.addDaysToDate(d, interval2);
								}  // ends here
								
								Date tempdate = new Date(d.getTime());
								d = tempdate;
								
								
								/************* Date 28 sep 2017 added by vijay for scheduling interval calculation with including precision using roundoff*********************/
								interval =(interval+tempinterval)-tempvalue;
							    tempvalue = Math.round(interval);
							    /**************** ends here **********************************/;
							    
							    
								scheduleEntity.setScheduleServiceDay(ContractForm.serviceDay(scheduleEntity.getScheduleServiceDate()));
								//  Vijay addd this code 
								scheduleEntity.setServiceRemark(salesitemlis.get(i).getRemark());
								/**
								 * Date 03-04-2017 added by vijay for week number
								 */
								int weeknumber = AppUtility.getweeknumber(scheduleEntity.getScheduleServiceDate());
								scheduleEntity.setWeekNo(weeknumber);
								
								/**
								 * ends here
								 */
								 /***
								  * nidhi *:*:*
								  * 18-09-2018
								  * for map bill product
								  * if any update please update in else conditions also
								  */
								 if(LoginPresenter.billofMaterialActive){
									 
									 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(salesitemlis.get(i).getPrduct());
									 if(billPrDt!=null){
										 
										 
										 UnitConversionUtility unitConver = new UnitConversionUtility();

										 
										 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
											 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
											 seList.add(scheduleEntity);
											 seList = unitConver.getServiceitemProList(seList,salesitemlis.get(i).getPrduct(),salesitemlis.get(i),billPrDt);
											 if(seList!=null && seList.size()>0)
												 scheduleEntity = seList.get(0);
											 
										 }else{
											 unitFlag = true;
//											 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
										 }
										 
										 
										 
										 
									 }
								 }
								 /**
								  * end
								  */
								 
								 /**
								  * @author Vijay Chougule Date :- 09-09-2020
								  * Des :- added Service duration 
								  */
								 if(branchSchedulingList.get(k).getServiceDuration()!=0){
									 scheduleEntity.setServiceDuration(branchSchedulingList.get(k).getServiceDuration());
								 }
								 /**
								  * ends here
								  */
								 
								serschelist.add(scheduleEntity);
							}
						}
						}
					}else{
						for (int k = 0; k < qty; k++) {
							long noServices = (long) (salesitemlis.get(i).getNumberOfServices());
							int noOfdays = salesitemlis.get(i).getDuration();
//							int interval = (int) (noOfdays / salesitemlis.get(i).getNumberOfServices());
							
							/*** Date 28 sep 2017 added by vijay  for scheduling proper calculation with precision number
							 *  for interval  
							 */
							double nodays =  noOfdays;
							double noofservices= salesitemlis.get(i).getNumberOfServices();
							double interval =  nodays / noofservices;
							double tempinterval = interval;
							double tempvalue=interval;
							/******************** ends here ********************/
							
							Date servicedate = salesitemlis.get(i).getStartDate();
							Date d = new Date(servicedate.getTime());
		
							Date productEndDate = new Date(servicedate.getTime());
							CalendarUtil.addDaysToDate(productEndDate, noOfdays);
							Date prodenddate = new Date(productEndDate.getTime());
							productEndDate = prodenddate;
		
							for (int j = 0; j < noServices; j++) {
		
								ServiceSchedule scheduleEntity = new ServiceSchedule();
		
								Date Stardaate = null;
								Stardaate = salesitemlis.get(i).getStartDate();
		
								scheduleEntity.setSerSrNo(salesitemlis.get(i).getProductSrNo());
		
								scheduleEntity.setScheduleStartDate(Stardaate);
								scheduleEntity.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
								scheduleEntity.setScheduleProdName(salesitemlis.get(i).getProductName());
								scheduleEntity.setScheduleDuration(salesitemlis.get(i).getDuration());
								scheduleEntity.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
								scheduleEntity.setScheduleServiceNo(j + 1);
		
								scheduleEntity.setScheduleProdStartDate(Stardaate);
								scheduleEntity.setScheduleProdEndDate(productEndDate);
		
								scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
								scheduleEntity.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
		
								String calculatedServiceTime = "";
								if (popuptablelist.size() != 0) {
									calculatedServiceTime = popuptablelist.get(j).getScheduleServiceTime();
								} else {
									calculatedServiceTime = "Flexible";
								}
		
								scheduleEntity.setScheduleServiceTime(calculatedServiceTime);
		
								if (form.customerbranchlist.size() == qty) {
									scheduleEntity.setScheduleProBranch(form.customerbranchlist.get(k).getBusinessUnitName());
									/**
									 * rohan added this code for automatic servicing
									 * branch setting for NBHC
									 */
									if (form.customerbranchlist.get(k).getBranch() != null) {
										scheduleEntity.setServicingBranch(form.customerbranchlist.get(k).getBranch());
									} else {
										scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
									}
									/**
									 * ends here
									 */
								} else if (form.customerbranchlist.size() == 0) {
									scheduleEntity.setScheduleProBranch("Service Address");
									/**
									 * rohan added this code for automatic servicing
									 * branch setting for NBHC
									 */
									scheduleEntity.setServicingBranch(form.olbbBranch.getValue());
									/**
									 * ends here
									 */
								} else {
									scheduleEntity.setScheduleProBranch(null);
								}
		
								String dayOfWeek1 = format.format(d);
								int adate = Integer.parseInt(dayOfWeek1);
								int adday1 = selectedIndex - 1;
								
								int adday = 0;
								if (adate <= adday1) {
									adday = (adday1 - adate);
								} else {
									adday = (7 - adate + adday1);
								}
								Date newDate = new Date(d.getTime());
								CalendarUtil.addDaysToDate(newDate, adday);
		
								Date day = new Date(newDate.getTime());
								
								/**
								 * Date 8-03-2017
								 * added by vijay for scheduling service with week day and week
								 */
								if(selectedServiceWeekIndex!=0){
									
									/**
									 * Date 1 jun 2017 added by vijay for service getting same month 
									 */
									if(temp!=null){
										String currentServiceMonth =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( day ).split("-")[1];
										String lastserviceDateMonth = DateTimeFormat.getFormat( "d-M-yyyy" ).format( temp ).split("-")[1];
										if(currentServiceMonth.equals(lastserviceDateMonth)){
											sameMonthserviceflag =true;
										}else{
											sameMonthserviceflag= false;
										}
									}
									
									if(sameMonthserviceflag){
										CalendarUtil.addDaysToDate(day, 10);
									}
									/**
									 * ends here
									 */
									
									CalendarUtil.setToFirstDayOfMonth(day);
									if(selectedServiceWeekIndex==1){
									 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										CalendarUtil.setToFirstDayOfMonth(day);
										 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
									}
								 }
								else if(selectedServiceWeekIndex==2){
										
										CalendarUtil.addDaysToDate(day, 7);

										 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
										if(day.before(Stardaate)){
											if(j==0){
												day =Stardaate;
											}
											else{
												flag = true;
											}
										}
										if(flag || j>0){
											CalendarUtil.setToFirstDayOfMonth(day);
											CalendarUtil.addDaysToDate(day, 7);
											 day = AppUtility.getCalculatedDatewithWeek(day, adday1);;
										}
								}else if(selectedServiceWeekIndex==3){
										CalendarUtil.addDaysToDate(day, 14);
										 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
										if(day.before(Stardaate)){
											if(j==0){
												day =Stardaate;
											}
											else{
												flag = true;
											}
										}
										if(flag || j>0){
											CalendarUtil.setToFirstDayOfMonth(day);
											CalendarUtil.addDaysToDate(day, 14);
											 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
										}
								}
								else if(selectedServiceWeekIndex==4){
										CalendarUtil.addDaysToDate(day, 21);

										 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
										if(day.before(Stardaate)){
											if(j==0){
												day =Stardaate;
											}
											else{
												flag = true;
											}
										}
										if(flag || j>0){
											CalendarUtil.setToFirstDayOfMonth(day);
											CalendarUtil.addDaysToDate(day, 21);
											 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
										}
								     }
									
									 temp = day;
									 
									 /**
									  * Date 26 jun 2017 added by vijay 
									  * for getting issues in 4 services and 6 services gap 
									  */
										
										CalendarUtil.addDaysToDate(d, (int)interval);
										int days =0;
										String serviceStartDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( Stardaate ).split("-")[0];
										if(serviceStartDate.equals("1")){
											String currentServiceDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( d ).split("-")[0];
											 days = AppUtility.checkDateMonthOFEndAndAddDays(currentServiceDate,d);
											CalendarUtil.addDaysToDate(d, days);

										}
										
										/**
										 * ends here
										 */
								}
								/**
								 * ends here
								 */
								
								if (productEndDate.before(day) == false) {
									scheduleEntity.setScheduleServiceDate(day);
								} else {
									scheduleEntity.setScheduleServiceDate(productEndDate);
								}
								
								/*** Date 28 sep 2017 added by vijay for interval calculation *************/
								int interval2 =(int) Math.round(interval);
								
								//Date 26 jun if condtion added by vijay for applicable only weeak is not selected
								if(selectedServiceWeekIndex==0){
									CalendarUtil.addDaysToDate(d, interval2);
								}
								//ends here
								
								Date tempdate = new Date(d.getTime());
								d = tempdate;
								scheduleEntity.setScheduleServiceDay(ContractForm.serviceDay(scheduleEntity.getScheduleServiceDate()));
								//  Vijay addd this code 
								scheduleEntity.setServiceRemark(salesitemlis.get(i).getRemark());
								
								
								/************* Date 28 sep 2017 added by vijay for scheduling interval calculation with including precision using roundoff*********************/
								interval =(interval+tempinterval)-tempvalue;
							    tempvalue = Math.round(interval);
							    /**************** ends here **********************************/;
							    
							    
								
								/**
								 * Date 03-04-2017 added by vijay for week number
								 */
								int weeknumber = AppUtility.getweeknumber(scheduleEntity.getScheduleServiceDate());
								scheduleEntity.setWeekNo(weeknumber);
								
								/**
								 * ends here
								 */
								 /***
								  * nidhi *:*:*
								  * 18-09-2018
								  * for map bill product
								  * if any update please update in else conditions also
								  */
								 if(LoginPresenter.billofMaterialActive){
									 
									 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(salesitemlis.get(i).getPrduct());
									 if(billPrDt!=null){
										 
										 
										 UnitConversionUtility unitConver = new UnitConversionUtility();

										 
										 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
											 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
											 seList.add(scheduleEntity);
											 seList = unitConver.getServiceitemProList(seList,salesitemlis.get(i).getPrduct(),salesitemlis.get(i),billPrDt);
											 if(seList!=null && seList.size()>0)
												 scheduleEntity = seList.get(0);
											 
										 }else{
											 unitFlag =true;
//											 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
										 }
										 
										 
										 
										 
									 }
								 }
								 /**
								  * end
								  */
								serschelist.add(scheduleEntity);
							}
						}
					}

					serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
					/**
					 * Date : 25-11-2017 BY ANIL
					 */
					form.globalScheduleServiceList=new ArrayList<ServiceSchedule>();
					form.globalScheduleServiceList=serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
					/**
					 * End
					 */
					if(unitFlag){
						 form.showDialogMessage("Unit of service are and product group detail area not mapped at branch level. Or conversion not available");
					}
				}
			}
			

		
	}

	private void reactOnHoursList() {

		List<ServiceSchedule> scheduleLis = this.serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
		ArrayList<ServiceSchedule> arrOfServices = new ArrayList<ServiceSchedule>();
		for (int i = 0; i < scheduleLis.size(); i++) {
			ServiceSchedule serviceScheduleEntity = new ServiceSchedule();

			String serviceHrs = serviceSchedulePopUp.getP_servicehours().getItemText(serviceSchedulePopUp.getP_servicehours().getSelectedIndex());
			String serviceMin = serviceSchedulePopUp.getP_servicemin().getItemText(serviceSchedulePopUp.getP_servicemin().getSelectedIndex());
			String serviceAmPm = serviceSchedulePopUp.getP_ampm().getItemText(serviceSchedulePopUp.getP_ampm().getSelectedIndex());
			String calculatedServiceTime = serviceHrs + ":" + serviceMin + ""+ serviceAmPm;
			serviceScheduleEntity.setScheduleServiceTime(calculatedServiceTime);
			
			scheduleLis.get(i).setScheduleServiceTime(calculatedServiceTime);
			/***
			 * @author Vijay Date :- 20-09-2021 
			 * Des :- here just we need to update time below code not required its already exist in the list.
			 */
//			// rohan added this for
//			serviceScheduleEntity.setSerSrNo(serviceSchedulePopUp
//					.getPopScheduleTable().getDataprovider().getList().get(i).getSerSrNo());
//
//			serviceScheduleEntity.setScheduleProdStartDate(serviceSchedulePopUp
//					.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleProdStartDate());
//			serviceScheduleEntity.setScheduleProdId(serviceSchedulePopUp
//					.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleProdId());
//			serviceScheduleEntity.setScheduleProdName(serviceSchedulePopUp
//					.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleProdName());
//			serviceScheduleEntity.setScheduleDuration(serviceSchedulePopUp
//					.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleDuration());
//			serviceScheduleEntity.setScheduleNoOfServices(serviceSchedulePopUp
//					.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleNoOfServices());
//			serviceScheduleEntity.setScheduleServiceNo(serviceSchedulePopUp
//					.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleServiceNo());
//			serviceScheduleEntity.setScheduleServiceDate(serviceSchedulePopUp
//					.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleServiceDate());
//			serviceScheduleEntity.setScheduleProdEndDate(serviceSchedulePopUp
//					.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleProdEndDate());
//			serviceScheduleEntity.setScheduleStartDate(serviceSchedulePopUp
//					.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleStartDate());
//			serviceScheduleEntity.setScheduleProBranch(serviceSchedulePopUp
//					.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleProBranch());
//			serviceScheduleEntity.setScheduleServiceDay(serviceSchedulePopUp
//					.getPopScheduleTable().getDataprovider().getList().get(i).getScheduleServiceDay());
//			serviceScheduleEntity.setServicingBranch(form.olbbBranch.getValue());
//			serviceScheduleEntity.setTotalServicingTime(serviceSchedulePopUp
//					.getPopScheduleTable().getDataprovider().getList().get(i).getTotalServicingTime());
//			//  vijay aaded this 
//			serviceScheduleEntity.setServiceRemark(serviceSchedulePopUp.getPopScheduleTable().
//					getDataprovider().getList().get(i).getServiceRemark());
//			/**
//			 * Date 03-04-2017 added by vijay for week number
//			 */
//			serviceScheduleEntity.setWeekNo(serviceSchedulePopUp.getPopScheduleTable().
//					getDataprovider().getList().get(i).getWeekNo());
//			/**
//			 * ends here
//			 */
//			/**
//			 * nidhi 
//			 *  10-10-2018
//			 *  
//			 */
////			serviceScheduleEntity.setServiceProductList(serviceSchedulePopUp.getPopScheduleTable()
////					.getDataprovider().getList().get(i).getServiceProductList());
//			 /***
//			  * nidhi |*|
//			  * 18-09-2018
//			  * for map bill product
//			  * if any update please update in else conditions also
//			  */
//			 if(LoginPresenter.billofMaterialActive && serviceSchedulePopUp.getPopScheduleTable().
//						getDataprovider().getList().get(i).getServiceProductList()!=null){
//				 serviceScheduleEntity.setServiceProductList(serviceSchedulePopUp.getPopScheduleTable().
//							getDataprovider().getList().get(i).getServiceProductList());
//			 }
//			 /**
//			  * end
//			  */
//			 if(scheduleLis.get(i).getServiceDuration()!=0){
//				 serviceScheduleEntity.setServiceDuration(scheduleLis.get(i).getServiceDuration());
//			 }
//			 
//			arrOfServices.add(serviceScheduleEntity);
		}
		serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(scheduleLis);
	}

	protected void valueChangeMethodLogicForScheduling() {
		
		
		serviceSchedulePopUp.getP_customise().setValue(false);
		serviceSchedulePopUp.getP_default().setValue(false);
		serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);

		serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
		serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
		serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);

		serviceSchedulePopUp.getP_servicehours().setEnabled(true);
		serviceSchedulePopUp.getP_servicemin().setEnabled(true);
		serviceSchedulePopUp.getP_ampm().setEnabled(true);
		serviceSchedulePopUp.getP_customise().setEnabled(true);

		//04-04-2017 added by vijay 
		serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
				
		int selectedExcludDay = serviceSchedulePopUp.getE_mondaytofriday().getSelectedIndex();
		
		
		/**
		 * @author Vijay Date :-  07-09-2021
		 *  Des :- Service Scheduling with new single common method with new round up logic for innovative
		 */
		if(LoginPresenter.serviceScheduleNewLogicFlag){
			int inreval = 0;
			if(serviceSchedulePopUp.getP_interval().getValue() != null && serviceSchedulePopUp.getP_interval().getValue() != 0){
				inreval =  serviceSchedulePopUp.getP_interval().getValue();
			}	
			if(serviceSchedulePopUp.getP_interval().getValue()==null){
				inreval = -1;
			}
			 List<SalesLineItem> salesitem = new ArrayList<SalesLineItem>();
			 salesitem = form.getSaleslineitemtable().getDataprovider().getList();
			 List<ServiceSchedule> serschelist=form.getServiceSchedulelist(AppConstants.INTERNAL,salesitem,form.dbContractDate.getValue(),form.customerbranchlist,form.olbbBranch.getValue(),selectedExcludDay,0,0,inreval,serviceSchedulePopUp);
		
			 serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
			 form.globalScheduleServiceList=new ArrayList<ServiceSchedule>();
			 form.globalScheduleServiceList=serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
			return;
		}
		/**
		 * ends here
		 */
		 
		List<SalesLineItem> salesitemlis = form.saleslineitemtable.getDataprovider().getList();
		ArrayList<ServiceSchedule> serschelist = new ArrayList<ServiceSchedule>();
		serschelist.clear();

		List<ServiceSchedule> popuptablelist = serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();

		for (int i = 0; i < salesitemlis.size(); i++) {


//			int qty = 0;
//			qty = (int) salesitemlis.get(i).getQty();
			
			/**
			 * Date : 29-03-2017 By ANil
			 */
			boolean branchSchFlag=false;
			int qty=0;
			
			/**
			 * Date 02-05-2018 
			 * Developer : Vijay
			 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
			 * so i have updated below code using hashmap. 
			 */
			
			
			/** Date 02-05-2018 By vijay for updated branch scheduling using hashmap **/
			ArrayList<BranchWiseScheduling> branchSchedulingList = salesitemlis.get(i).getCustomerBranchSchedulingInfo().get(salesitemlis.get(i).getProductSrNo());
			
		/**
		 * ends here	
		 */
			if(branchSchedulingList!=null
					&&branchSchedulingList.size()!=0){
				branchSchFlag=true;
				qty=branchSchedulingList.size();
			}else{
				qty=(int) salesitemlis.get(i).getQty();
			}
			
			/**
			 * End
			 */
			if(branchSchFlag){
				for (int k = 0; k < qty; k++) {
					
					if(branchSchedulingList.get(k).isCheck()==true){
						
				/*	long noServices = (long) (item.getNumberOfServices());
					int noOfdays = item.getDuration();
					double nodays =  noOfdays;
					double noofservices= item.getNumberOfServices();
					int inreval = nodays / noofservices;
					if(serviceSchedulePopUp.getP_interval().getValue() != null || serviceSchedulePopUp.getP_interval().getValue() != 0){
						inreval =  serviceSchedulePopUp.getP_interval().getValue().doubleValue();
					}
					int noOfdays = salesitemlis.get(i).getDuration();*/

						
					long noServices = (long) (salesitemlis.get(i).getNumberOfServices());
					int noOfdays = salesitemlis.get(i).getDuration();
					double nodays =  noOfdays;
					double noofservices= salesitemlis.get(i).getNumberOfServices();
//					int inreval = serviceSchedulePopUp.getP_interval().getValue();
					int inreval =  (int) (nodays / noofservices);
					if(serviceSchedulePopUp.getP_interval().getValue() != null && serviceSchedulePopUp.getP_interval().getValue() != 0){
						inreval =  serviceSchedulePopUp.getP_interval().getValue();
					}	
					Date servicedate = new Date(salesitemlis.get(i).getStartDate().getTime());
	
					Date productEndDate = new Date(servicedate.getTime());
					CalendarUtil.addDaysToDate(productEndDate, noOfdays);
					Date prodenddate = new Date(productEndDate.getTime());
					productEndDate = prodenddate;
					
					

					/**
					 * nidhi
					 *  ))..
					 *  for exclude day.
					 */
				/*	int startday = servicedate.getDay();
					
					if(selectedExcludDay !=0  && startday == selectedExcludDay-1){
						CalendarUtil.addDaysToDate(servicedate, 1);
					}
					Date d = new Date(servicedate.getTime());
					int lastday = productEndDate.getDay();
					
					if(selectedExcludDay !=0  && lastday == selectedExcludDay-1){
						CalendarUtil.addDaysToDate(productEndDate, -1);
					}
					*/
					/**
					 * end
					 */
					
					/**
					 * nidhi
					 *  ))..
					 *  for exclude day.
					 */
					productEndDate = calculateEndDate(servicedate, noOfdays-1);
					int startday = servicedate.getDay();
					
					if(selectedExcludDay !=0  && startday == selectedExcludDay-1){
						CalendarUtil.addDaysToDate(servicedate, 1);
					}
//					Date d = new Date(servicedate.getTime());
					
					if(selectedExcludDay !=0 ){
						CalendarUtil.addDaysToDate(productEndDate, -1);
					}
					
					int lastday = productEndDate.getDay();
					
					if(selectedExcludDay !=0  && lastday == selectedExcludDay-1){
						CalendarUtil.addDaysToDate(productEndDate, -1);
					}
					Date d = new Date(servicedate.getTime());
					/**
					 * end
					 */
					Date tempdate = new Date();
					tempdate = servicedate;
					for (int j = 0; j < noServices; j++) {
						/*if (j > 0) {
							CalendarUtil.addDaysToDate(d, inreval);
							tempdate = new Date(d.getTime());
						}*/
						

						if (j > 0) {
							CalendarUtil.addDaysToDate(d, inreval);
						}
	
						/**
						 * nidhi
						 *  ))..
						 *  for exclude day.
						 */
						int serDay = d.getDay();
//						if(selectedExcludDay !=0  && serDay == selectedExcludDay-1){
//							CalendarUtil.addDaysToDate(d, 1);
//						}
						
						/**
						 * @author Vijay Date :- 09-02-2023
						 * Des :- added for exclude holidays and weekly off days from calnedar
						 */
						if(serviceSchedulePopUp.getE_mondaytofriday().getValue(serviceSchedulePopUp.getE_mondaytofriday().getSelectedIndex())
								.equals(AppConstants.HOLIDAYS)){
							Console.log("Inside exclude holiday");
							Calendar calendar = serviceSchedulePopUp.getOlbcalendar().getSelectedItem();
							form.excludeDateFromInHolidayDateFromCalendar(d,calendar);
						}
						else{
							if(selectedExcludDay !=0  && serDay == selectedExcludDay-1){
								CalendarUtil.addDaysToDate(d, 1);
							}
						}
						
						
						tempdate = new Date(d.getTime());
						
						ServiceSchedule ssch = new ServiceSchedule();
						Date Stardaate = null;
						
						ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
						Stardaate = salesitemlis.get(i).getStartDate();
						ssch.setScheduleStartDate(Stardaate);
						ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
						ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
						ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
						ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
						ssch.setScheduleServiceNo(j + 1);
						ssch.setScheduleProdStartDate(Stardaate);
						ssch.setScheduleProdEndDate(productEndDate);
						ssch.setServicingBranch(form.olbbBranch.getValue());
						ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
						String calculatedServiceTime = "";
						if (popuptablelist.size() != 0) {
							calculatedServiceTime = popuptablelist.get(j).getScheduleServiceTime();
						} else {
							calculatedServiceTime = "Flexible";
						}
	
						//vijay
						ssch.setServiceRemark(salesitemlis.get(i).getRemark());
						ssch.setScheduleServiceTime(calculatedServiceTime);
	
						
						ssch.setScheduleProBranch(branchSchedulingList.get(k).getBranchName());
						if (branchSchedulingList.get(k).getServicingBranch()!=null) {
							ssch.setServicingBranch(branchSchedulingList.get(k).getServicingBranch());
						} 
						
						 /**
						  * @author Vijay Chougule Date :- 09-09-2020
						  * Des :- added Service duration 
						  */
						 if(branchSchedulingList.get(k).getServiceDuration()!=0){
							 ssch.setServiceDuration(branchSchedulingList.get(k).getServiceDuration());
						 }
						 /**
						  * ends here
						  */
						
	
						if (j == 0) {
							ssch.setScheduleServiceDate(servicedate);
							ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
							serschelist.add(ssch);
						}
						if (j != 0) {
	
							if (productEndDate.after(d)) {
								ssch.setScheduleServiceDate(tempdate);
								ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
								serschelist.add(ssch);
							} else {
								ssch.setScheduleServiceDate(productEndDate);
								ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
								serschelist.add(ssch);
							}
						}
						/**
						 * Date 03-04-2017 added by vijay for week number
						 */
						int weeknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
						ssch.setWeekNo(weeknumber);
						 /***
						  * nidhi *:*:*
						  * 18-09-2018
						  * for map bill product
						  * if any update please update in else conditions also
						  */
						 if(LoginPresenter.billofMaterialActive && ssch.getServiceProductList()!=null){
							 ssch.setServiceProductList(ssch.getServiceProductList());
							 
						 }
						 /**
						  * end
						  */
						/**
						 * ends here
						 */
					}
				}
				}
				
			}else{
				for (int k = 0; k < qty; k++) {
					long noServices = (long) (salesitemlis.get(i).getNumberOfServices());
					int noOfdays = salesitemlis.get(i).getDuration();
					double nodays =  noOfdays;
					double noofservices= salesitemlis.get(i).getNumberOfServices();
					int inreval =  (int) (nodays / noofservices);
					if(serviceSchedulePopUp.getP_interval().getValue() != null && serviceSchedulePopUp.getP_interval().getValue() != 0){
						inreval =  serviceSchedulePopUp.getP_interval().getValue();
					}	
					Date servicedate = new Date(salesitemlis.get(i).getStartDate().getTime());
					Date d = new Date(servicedate.getTime());
	
					Date productEndDate = new Date(servicedate.getTime());
					CalendarUtil.addDaysToDate(productEndDate, noOfdays);
					Date prodenddate = new Date(productEndDate.getTime());
					productEndDate = prodenddate;
					
					/**
					 * nidhi
					 *  ))..
					 *  for exclude day.
					 */
					int startday = servicedate.getDay();
					
					if(selectedExcludDay !=0  && startday == selectedExcludDay-1){
						CalendarUtil.addDaysToDate(servicedate, 1);
					}
					int lastday = productEndDate.getDay();
					
					if(selectedExcludDay !=0  && lastday == selectedExcludDay-1){
						CalendarUtil.addDaysToDate(productEndDate, -1);
					}
					/**
					 * end
					 */
					Date tempdate = new Date();
					tempdate = servicedate;
					for (int j = 0; j < noServices; j++) {
	
						
						
						if (j > 0) {
							CalendarUtil.addDaysToDate(d, inreval);
						}
	
						/**
						 * nidhi
						 *  ))..
						 *  for exclude day.
						 */
						int serDay = d.getDay();
						if(selectedExcludDay !=0  && serDay == selectedExcludDay-1){
							CalendarUtil.addDaysToDate(d, 1);
						
						}
						
						tempdate = new Date(d.getTime());
						/**
						 * end
						 */
						
						ServiceSchedule ssch = new ServiceSchedule();
						Date Stardaate = null;
						
						ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
						Stardaate = salesitemlis.get(i).getStartDate();
						ssch.setScheduleStartDate(Stardaate);
						ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
						ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
						ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
						ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
						ssch.setScheduleServiceNo(j + 1);
						ssch.setScheduleProdStartDate(Stardaate);
						ssch.setScheduleProdEndDate(prodenddate);
						ssch.setServicingBranch(form.olbbBranch.getValue());
						ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
						String calculatedServiceTime = "";
						if (popuptablelist.size() != 0) {
							calculatedServiceTime = popuptablelist.get(j).getScheduleServiceTime();
						} else {
							calculatedServiceTime = "Flexible";
						}
	
						//vijay
						ssch.setServiceRemark(salesitemlis.get(i).getRemark());
						
						ssch.setScheduleServiceTime(calculatedServiceTime);
	
						if (form.customerbranchlist.size() == qty) {
							ssch.setScheduleProBranch(form.customerbranchlist.get(k).getBusinessUnitName());
							/**
							 * rohan added this code for automatic servicing branch
							 * setting for NBHC
							 */
							if (form.customerbranchlist.get(k).getBranch() != null) {
								ssch.setServicingBranch(form.customerbranchlist.get(k).getBranch());
							} else {
								ssch.setServicingBranch(form.olbbBranch.getValue());
							}
							/**
							 * ends here
							 */
	
						} else if (form.customerbranchlist.size() == 0) {
							ssch.setScheduleProBranch("Service Address");
	
							/**
							 * rohan added this code for automatic servicing branch
							 * setting for NBHC
							 */
							ssch.setServicingBranch(form.olbbBranch.getValue());
							/**
							 * ends here
							 */
	
						} else {
							ssch.setScheduleProBranch(null);
						}
					
						
	
						if (j == 0) {
							ssch.setScheduleServiceDate(servicedate);
							ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
							serschelist.add(ssch);
						}
						if (j != 0) {
	
							if (productEndDate.after(d)) {
								ssch.setScheduleServiceDate(tempdate);
								ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
								serschelist.add(ssch);
							} else {
								ssch.setScheduleServiceDate(productEndDate);
								ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
								serschelist.add(ssch);
							}
						}
						
						/**
						 * Date 03-04-2017 added by vijay for week number
						 */
						int weeknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
						ssch.setWeekNo(weeknumber);
						 /***
						  * nidhi *:*:*
						  * 18-09-2018
						  * for map bill product
						  * if any update please update in else conditions also
						  */
						 if(LoginPresenter.billofMaterialActive && ssch.getServiceProductList()!=null){
							 ssch.setServiceProductList(ssch.getServiceProductList());
						 }
						 /**
						  * end
						  */
								/**
						 * ends here
						 */
					}
				}
		}

			serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
			/**
			 * Date : 25-11-2017 BY ANIL
			 */
			form.globalScheduleServiceList=new ArrayList<ServiceSchedule>();
			form.globalScheduleServiceList=serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
			/**
			 * End
			 */
		}
	
	
	
}

	private void reactFordeleteproduct(int proid, int srNo) {

		int prodid1 = proid;
		if (form.getSaleslineitemtable().getDataprovider().getList().size() != 0) {
			System.out.println("Before Update Services Method");
			updateServices(prodid1, srNo);
		}
	}

	void updateServices(int changeproductid, int srNo) {
		

		
		System.out.println("PRODUCT ID "+changeproductid+" SR NO. "+srNo);

		List<ServiceSchedule> origserviceitemlist = form.serviceScheduleTable.getDataprovider().getList();
		List<Boolean> serLis = new ArrayList<Boolean>();
		List<ServiceSchedule> newlist = new ArrayList<ServiceSchedule>();
		newlist.clear();

		for (int j = 0; j < origserviceitemlist.size(); j++) {
			boolean prodIdCheck = findProductId(origserviceitemlist.get(j)
					.getScheduleProdId(), origserviceitemlist.get(j).getSerSrNo());
			if (prodIdCheck == false) {
				serLis.add(false);
			} else {
				serLis.add(true);
			}
		}

		form.serviceScheduleTable.connectToLocal();
		boolean unitFlag = false;
		for (int k = 0; k < serLis.size(); k++) {

			System.out.println("boolean loop " + serLis.get(k));
			if (serLis.get(k)) {
//				System.out.println(" conditin for dele product");
//				System.out.println(" chnge product" + changeproductid);
//				System.out.println(" form pooup table product"
//						+ origserviceitemlist.get(k).getScheduleProdId());
				//
				// old code
				// if(changeproductid!=origserviceitemlist.get(k).getScheduleProdId()
				// && srNo != origserviceitemlist.get(k).getSerSrNo()){

				// new code by for rohan for same product add in contract
				if (srNo != origserviceitemlist.get(k).getSerSrNo()) {

					System.out.println(" conditin for dele product 11");

					ServiceSchedule serviceScheduleObj = new ServiceSchedule();

					serviceScheduleObj.setSerSrNo(origserviceitemlist.get(k)
							.getSerSrNo());

					serviceScheduleObj.setScheduleStartDate(origserviceitemlist
							.get(k).getScheduleStartDate());
					serviceScheduleObj.setScheduleProdId(origserviceitemlist
							.get(k).getScheduleProdId());
					serviceScheduleObj.setScheduleProdName(origserviceitemlist
							.get(k).getScheduleProdName());
					serviceScheduleObj.setScheduleDuration(origserviceitemlist
							.get(k).getScheduleDuration());
					serviceScheduleObj
							.setScheduleNoOfServices(origserviceitemlist.get(k)
									.getScheduleNoOfServices());
					serviceScheduleObj.setScheduleServiceNo(origserviceitemlist
							.get(k).getScheduleServiceNo());
					serviceScheduleObj
							.setScheduleServiceDate(origserviceitemlist.get(k)
									.getScheduleServiceDate());

					if (origserviceitemlist.get(k).getScheduleProdEndDate() != null) {
						serviceScheduleObj
								.setScheduleProdEndDate(origserviceitemlist
										.get(k).getScheduleProdEndDate());
					} else {
						serviceScheduleObj.setScheduleProdEndDate(new Date());
					}

					if (origserviceitemlist.get(k).getScheduleProdStartDate() != null) {
						serviceScheduleObj
								.setScheduleProdStartDate(origserviceitemlist
										.get(k).getScheduleProdStartDate());
					} else {
						serviceScheduleObj.setScheduleProdStartDate(new Date());
					}
					serviceScheduleObj
							.setScheduleServiceTime(origserviceitemlist.get(k)
									.getScheduleServiceTime());
					serviceScheduleObj.setPremisesDetails(origserviceitemlist
							.get(k).getPremisesDetails());
					serviceScheduleObj.setScheduleProBranch(origserviceitemlist
							.get(k).getScheduleProBranch());
					serviceScheduleObj
							.setScheduleServiceDay(origserviceitemlist.get(k)
									.getScheduleServiceDay());
					serviceScheduleObj.setServicingBranch(origserviceitemlist
							.get(k).getServicingBranch());
					serviceScheduleObj
							.setTotalServicingTime(origserviceitemlist.get(k)
									.getTotalServicingTime());
					// vijay
					serviceScheduleObj.setServiceRemark(origserviceitemlist
							.get(k).getServiceRemark());

					/**
					 * Date 03-04-2017 added by vijay for week number
					 */
					int weeknumber = AppUtility.getweeknumber(serviceScheduleObj.getScheduleServiceDate());
					serviceScheduleObj.setWeekNo(weeknumber);
					
					/**
					 * ends here
					 */
					 /***
					  * nidhi *:*:*
					  * 18-09-2018
					  * for map bill product
					  * if any update please update in else conditions also
					  */
					 if(LoginPresenter.billofMaterialActive){
						 serviceScheduleObj.setServiceProductList(serviceScheduleObj.getServiceProductList());
					 }
					 /**
					  * end
					  */
					 /**
					  * @author Anil
					  * @since 06-06-2020
					  * copying tat details
					  */
					 if(form.complainTatFlag){
						form.copyComponentAndTatDetails(serviceScheduleObj,origserviceitemlist.get(k)); 
					 }
					 
					 /**
					  * @author Vijay Chougule Date :- 09-09-2020
					  * Des :- added Service duration 
					  */
					 if(origserviceitemlist.get(k).getServiceDuration()!=0){
						 serviceScheduleObj.setServiceDuration(origserviceitemlist.get(k).getServiceDuration());
					 }
					 /**
					  * ends here
					  */
					newlist.add(serviceScheduleObj);
				}
			}
		}

		List<SalesLineItem> formLis = form.getSaleslineitemtable().getDataprovider().getList();

		for (int i = 0; i < formLis.size(); i++) {

			if (changeproductid == formLis.get(i).getPrduct().getCount()
					&& srNo == formLis.get(i).getProductSrNo()) {
				System.out.println("CHANGED PRODUCT "+changeproductid+" SR "+srNo);

				/**
				 * @author Vijay Date :- 16-09-2021
				 * Des :- Updated service scheduling with new logic and single common method with process config
				 */
				if(LoginPresenter.serviceScheduleNewLogicFlag){
					 List<SalesLineItem> salesitem = new ArrayList<SalesLineItem>();
					 salesitem.add(formLis.get(i));
					 List<ServiceSchedule> popuplist=form.getServiceSchedulelist("default",salesitem,form.dbContractDate.getValue(),form.customerbranchlist,form.olbbBranch.getValue(),0,
							 0,0,0,serviceSchedulePopUp);
					 System.out.println("popuplist size "+popuplist.size());
					 newlist.addAll(popuplist); 
					 form.serviceScheduleTable.getDataprovider().setList(newlist);
					 return;
				}
				
				/**
				 * Date : 29-03-2017 By ANil
				 */
				boolean branchSchFlag=false;
				
				int qty = 0;
//				qty = (int) formLis.get(i).getQty();
				
				/**
				 * Date 02-05-2018 
				 * Developer : Vijay
				 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
				 * so i have updated below code using hashmap. 
				 */
				
//				/**
//				 * Date : 03-04-2017 By ANIL
//				 */
////				ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(formLis.get(i).getBranchSchedulingInfo());
//				
//				/**
//				 * Date : 12-06-2017 BY ANIL
//				 */
//				String[] branchArray=new String[10];
//				if(formLis.get(i).getBranchSchedulingInfo()!=null
//						&&!formLis.get(i).getBranchSchedulingInfo().equals("")){
//					branchArray[0]=formLis.get(i).getBranchSchedulingInfo();
//				}
//				if(formLis.get(i).getBranchSchedulingInfo1()!=null
//						&&!formLis.get(i).getBranchSchedulingInfo1().equals("")){
//					branchArray[1]=formLis.get(i).getBranchSchedulingInfo1();
//				}
//				if(formLis.get(i).getBranchSchedulingInfo2()!=null
//						&&!formLis.get(i).getBranchSchedulingInfo2().equals("")){
//					branchArray[2]=formLis.get(i).getBranchSchedulingInfo2();
//				}
//				if(formLis.get(i).getBranchSchedulingInfo3()!=null
//						&&!formLis.get(i).getBranchSchedulingInfo3().equals("")){
//					branchArray[3]=formLis.get(i).getBranchSchedulingInfo3();
//				}
//				if(formLis.get(i).getBranchSchedulingInfo4()!=null
//						&&!formLis.get(i).getBranchSchedulingInfo4().equals("")){
//					branchArray[4]=formLis.get(i).getBranchSchedulingInfo4();
//				}
//				
//				if(formLis.get(i).getBranchSchedulingInfo5()!=null
//						&&!formLis.get(i).getBranchSchedulingInfo5().equals("")){
//					branchArray[5]=formLis.get(i).getBranchSchedulingInfo5();
//				}
//				if(formLis.get(i).getBranchSchedulingInfo6()!=null
//						&&!formLis.get(i).getBranchSchedulingInfo6().equals("")){
//					branchArray[6]=formLis.get(i).getBranchSchedulingInfo6();
//				}
//				if(formLis.get(i).getBranchSchedulingInfo7()!=null
//						&&!formLis.get(i).getBranchSchedulingInfo7().equals("")){
//					branchArray[7]=formLis.get(i).getBranchSchedulingInfo7();
//				}
//				if(formLis.get(i).getBranchSchedulingInfo8()!=null
//						&&!formLis.get(i).getBranchSchedulingInfo8().equals("")){
//					branchArray[8]=formLis.get(i).getBranchSchedulingInfo8();
//				}
//				if(formLis.get(i).getBranchSchedulingInfo9()!=null
//						&&!formLis.get(i).getBranchSchedulingInfo9().equals("")){
//					branchArray[9]=formLis.get(i).getBranchSchedulingInfo9();
//				}
//				
//				/**
//				 * End
//				 */
//				ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(branchArray);
				
				/** Date 02-05-2018 By vijay for updated branch scheduling using hashmap **/
				ArrayList<BranchWiseScheduling> branchSchedulingList = null;
				if(formLis.get(i).getCustomerBranchSchedulingInfo()!=null)
				 branchSchedulingList = formLis.get(i).getCustomerBranchSchedulingInfo().get(formLis.get(i).getProductSrNo());
			
				/**
				 * ends here	
				 */
				
				if(branchSchedulingList!=null&&branchSchedulingList.size()!=0){
					branchSchFlag=true;
					qty=branchSchedulingList.size();
				}else{
					qty=(int) formLis.get(i).getQty();
				}
				
				/**
				 * End
				 */
				if(branchSchFlag){
				for (int k = 0; k < qty; k++) {
					if(branchSchedulingList.get(k).isCheck()==true){

					System.out.println("condition == ");

					long noServices = (long) (formLis.get(i).getNumberOfServices());
					int noOfdays = formLis.get(i).getDuration();
					
					/********* below one line commented by vijay this interval calculation missing precision so services not getting created still end date ***********/
//					int interval = (int) (noOfdays / formLis.get(i).getNumberOfServices());
					
					/*** Date 28 sep 2017 added by vijay  for scheduling proper calculation with precision number
					 *  for interval  
					 */
					double days =  noOfdays;
					double noofservices= formLis.get(i).getNumberOfServices();
					double interval =  days / noofservices;
					System.out.println(Math.round(days/noofservices));
					System.out.println("interval "+interval);
					double temp = interval;
					double tempvalue=interval;
					/******************** ends here ********************/

					
					Date servicedate = formLis.get(i).getStartDate();
					Date d = new Date(servicedate.getTime());
					Date productEndDate = new Date(servicedate.getTime());
					CalendarUtil.addDaysToDate(productEndDate, noOfdays);
					Date prodenddate = new Date(productEndDate.getTime());
					productEndDate = prodenddate;
					Date tempdate = new Date();
					for (int j = 0; j < noServices; j++) {
						/**
						 * Date :09/06/2017   by Ajinkya 
						 * Nbhc Scheduling Issue 
						 */
//						if (j > 0) {
//							CalendarUtil.addDaysToDate(d, interval);
//							tempdate = new Date(d.getTime());
//						}
						
						/**
						 *  End Here
						 */

						ServiceSchedule ssch = new ServiceSchedule();
						Date Stardaate = formLis.get(i).getStartDate();

						ssch.setSerSrNo(formLis.get(i).getProductSrNo());

						ssch.setScheduleStartDate(Stardaate);
						ssch.setScheduleProdId(formLis.get(i).getPrduct()
								.getCount());
						ssch.setScheduleProdName(formLis.get(i)
								.getProductName());
						ssch.setScheduleDuration(formLis.get(i).getDuration());
						ssch.setScheduleNoOfServices(formLis.get(i)
								.getNumberOfServices());
						ssch.setScheduleServiceNo(j + 1);
						ssch.setScheduleProdStartDate(Stardaate);
						ssch.setScheduleProdEndDate(productEndDate);
						ssch.setScheduleServiceTime("Flexible");

						ssch.setServicingBranch(form.olbbBranch.getValue());
						ssch.setTotalServicingTime(formLis.get(i).getServicingTIme());

						ssch.setScheduleProBranch(branchSchedulingList.get(k).getBranchName());
						if (branchSchedulingList.get(k).getServicingBranch()!=null) {
							ssch.setServicingBranch(branchSchedulingList.get(k).getServicingBranch());
						} 
						
						int dayIndex=AppUtility.getDayIndex(branchSchedulingList.get(k).getDay());
						
						if(dayIndex!=0){
							String dayOfWeek1 = format.format(d);
							int adate = Integer.parseInt(dayOfWeek1);
							int adday1 = dayIndex - 1;
							int adday = 0;
							if (adate <= adday1) {
								adday = (adday1 - adate);
							} else {
								adday = (7 - adate + adday1);
							}
							
							Date newDate = new Date(d.getTime());
							CalendarUtil.addDaysToDate(newDate, adday);
							Date day = new Date(newDate.getTime());

							if (productEndDate.before(day) == false) {
								ssch.setScheduleServiceDate(day);
							} else {
								ssch.setScheduleServiceDate(productEndDate);
							}
							
							/*** added by vijay for interval calculation *************/
							int interval2 =(int) Math.round(interval);


							CalendarUtil.addDaysToDate(d, interval2);
							Date tempdate1 = new Date(d.getTime());
							d = tempdate1;
							ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
							
							/************* added by vijay for scheduling interval calculation with including precision using roundoff*********************/
							interval =(interval+temp)-tempvalue;
						    tempvalue = Math.round(interval);
						    /**************** ends here **********************************/
	
						    
						}else{
							
							/*** added by vijay for interval calculation *************/
							int interval2 =(int) Math.round(interval);
							
							if (j > 0) {
								CalendarUtil.addDaysToDate(d, interval2);
								tempdate = new Date(d.getTime());
								
								/************* added by vijay for scheduling interval including calculation using roundoff *********************/
								interval =(interval+temp)-tempvalue;
							    tempvalue = Math.round(interval);
							    /**************** ends here **********************************/

							}
							
							if (j == 0) {
								ssch.setScheduleServiceDate(servicedate);
							}
							if (j != 0) {
								if (productEndDate.before(d) == false) {
									ssch.setScheduleServiceDate(tempdate);
								} else {
									ssch.setScheduleServiceDate(productEndDate);
								}
							}
						}

						

						ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
						ssch.setServiceRemark(formLis.get(i).getRemark());
						/**
						 * Date 01-04-2017 added by vijay for week number
						 */
						int weeknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
						ssch.setWeekNo(weeknumber);
						
						/**
						 * ends here
						 */
						 /***
						  * nidhi *:*:*
						  * 18-09-2018
						  * for map bill product
						  * if any update please update in else conditions also
						  */
						 if(LoginPresenter.billofMaterialActive){
							 
							 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(formLis.get(i).getPrduct());
							 if(billPrDt!=null){
								 
								 
								 UnitConversionUtility unitConver = new UnitConversionUtility();

								 
								 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
									 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
									 seList.add(ssch);
									 seList = unitConver.getServiceitemProList(seList,formLis.get(i).getPrduct(),formLis.get(i),billPrDt);
									 if(seList!=null && seList.size()>0)
										 ssch = seList.get(0);
									 
								 }else{
									 unitFlag = true;
//									 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
								 }
								 
								 
								 
								 
							 }
						 }
						 /**
						  * end
						  */
						 
						 /**
						  * @author Vijay Chougule Date :- 09-09-2020
						  * Des :- added Service duration 
						  */
						 if(branchSchedulingList.get(k).getServiceDuration()!=0){
							 ssch.setServiceDuration(branchSchedulingList.get(k).getServiceDuration());
						 }
						 /**
						  * ends here
						  */
						 
						newlist.add(ssch);
						
						/**
						 * @author Anil
						 * @since 06-06-2020
						 * Updating tat and component details on scheduling object
						 */
						if(form.complainTatFlag){
							List<ServiceSchedule>list=form.updateSchedulingListComponentWise(ssch, branchSchedulingList.get(k));
							if(list!=null&&list.size()!=0){
								newlist.addAll(list);
							}
						}
					}
				}

				}
				}
				else{
					
					for (int k = 0; k < qty; k++) {
						
						long noServices = (long) (formLis.get(i).getNumberOfServices());
						int noOfdays = formLis.get(i).getDuration();
						/********* below one line commented by vijay this interval calculation missing precision so services not getting created still end date ***********/
//						int interval = (int) (noOfdays / formLis.get(i).getNumberOfServices());
						
						/*** Date 28 sep 2017 below code added by vijay interval calculation with precision and using roundoff ******************/
						double days =  noOfdays;
						double noofservices= formLis.get(i).getNumberOfServices();
						double interval =  days / noofservices;
						System.out.println(Math.round(days/noofservices));
						System.out.println("interval "+interval);
						double temp = interval;
						double tempvalue=interval;
						/******************** ends here ********************/
						
						Date servicedate = formLis.get(i).getStartDate();
						Date d = new Date(servicedate.getTime());
						Date productEndDate = new Date(servicedate.getTime());
						CalendarUtil.addDaysToDate(productEndDate, noOfdays);
						Date prodenddate = new Date(productEndDate.getTime());
						productEndDate = prodenddate;
						Date tempdate = new Date();
						
						for (int j = 0; j < noServices; j++) {
							
							/*** added by vijay for interval calculation *************/
							int interval2 =(int) Math.round(interval);
							
							if (j > 0) {
								CalendarUtil.addDaysToDate(d, interval2);
								tempdate = new Date(d.getTime());
								
								/************* added by vijay *********************/
								interval =(interval+temp)-tempvalue;
							    tempvalue = Math.round(interval);
							    /************** ends here ************************/
							}
	
							ServiceSchedule ssch = new ServiceSchedule();
							Date Stardaate = formLis.get(i).getStartDate();
	
							ssch.setSerSrNo(formLis.get(i).getProductSrNo());
							ssch.setScheduleStartDate(Stardaate);
							ssch.setScheduleProdId(formLis.get(i).getPrduct().getCount());
							ssch.setScheduleProdName(formLis.get(i).getProductName());
							ssch.setScheduleDuration(formLis.get(i).getDuration());
							ssch.setScheduleNoOfServices(formLis.get(i).getNumberOfServices());
							ssch.setScheduleServiceNo(j + 1);
							ssch.setScheduleProdStartDate(Stardaate);
							ssch.setScheduleProdEndDate(productEndDate);
							ssch.setScheduleServiceTime("Flexible");
	
							ssch.setServicingBranch(form.olbbBranch.getValue());
							ssch.setTotalServicingTime(formLis.get(i).getServicingTIme());
	
							if (j == 0) {
								ssch.setScheduleServiceDate(servicedate);
							}
							if (j != 0) {
								if (productEndDate.before(d) == false) {
									ssch.setScheduleServiceDate(tempdate);
								} else {
									ssch.setScheduleServiceDate(productEndDate);
								}
							}
							
							if (form.customerbranchlist.size() == qty) {
								String branchName = form.customerbranchlist.get(k).getBusinessUnitName();
								ssch.setScheduleProBranch(branchName);
	
								/**
								 * rohan added this code for automatic servicing
								 * branch setting for NBHC
								 */
	
								if (form.customerbranchlist.get(k).getBranch() != null) {
									ssch.setServicingBranch(form.customerbranchlist.get(k).getBranch());
								} else {
									ssch.setServicingBranch(form.olbbBranch.getValue());
								}
								/**
								 * ends here
								 */
	
							} else if (form.customerbranchlist.size() == 0) {
								ssch.setScheduleProBranch("Service Address");
	
								/**
								 * rohan added this code for automatic servicing
								 * branch setting for NBHC
								 */
								ssch.setServicingBranch(form.olbbBranch.getValue());
								/**
								 * ends here
								 */
	
							} else {
								ssch.setScheduleProBranch(null);
							}
	
							ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
							ssch.setServiceRemark(formLis.get(i).getRemark());
							/**
							 * Date 01-04-2017 added by vijay for week number
							 */
							int weeknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
							ssch.setWeekNo(weeknumber);
							
							/**
							 * ends here
							 */
							 /***
							  * nidhi *:*:*
							  * 18-09-2018
							  * for map bill product
							  * if any update please update in else conditions also
							  */
							 if(LoginPresenter.billofMaterialActive){
								 
								 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(formLis.get(i).getPrduct());
								 if(billPrDt!=null){
									 
									 
									 UnitConversionUtility unitConver = new UnitConversionUtility();

									 
									 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
										 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
										 seList.add(ssch);
										 seList = unitConver.getServiceitemProList(seList,formLis.get(i).getPrduct(),formLis.get(i),billPrDt);
										 if(seList!=null && seList.size()>0)
											 ssch = seList.get(0);
										 
									 }else{
										 unitFlag = true;
//										 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
									 }
									 
									 
									 
									 
								 }
							 }
							 /**
							  * end
							  */
							newlist.add(ssch);
						}
					}
				}
			}
			if(unitFlag){
				form.showDialogMessage("Unit of service are and product group detail area not mapped at branch level. Or conversion not available");
			}
		}

		form.serviceScheduleTable.getDataprovider().setList(newlist);
	}

	protected boolean findProductId(int prodId, int srNo) {
		List<SalesLineItem> formLis = form.getSaleslineitemtable()
				.getDataprovider().getList();
		boolean flagVal = false;
		for (int i = 0; i < formLis.size(); i++) {
			if (formLis.get(i).getPrduct().getCount() == prodId
					&& formLis.get(i).getProductSrNo() == srNo) {
				flagVal = true;
			}
		}
		return flagVal;
	}
	
	
	/**
	 * Date : 28-03-2017 By ANIL
	 * setting data on schedule popup 
	 */
	public static void setScheduleServiceOnSchedulingTable(ArrayList<ServiceSchedule> list){
		System.out.println("SCHEDULE LIST : "+list.size());
//		serviceSchedulePopUp.getPopScheduleTable().connectToLocal();
//		serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(list);
		
		for(ServiceSchedule service : list){
			System.out.println("product name =="+service.getScheduleProdName());
			System.out.println("Service Duration ="+service.getServiceDuration());
			
		}
		form.getServiceScheduleTable().connectToLocal();
		form.getServiceScheduleTable().getDataprovider().setList(list);
//		System.out.println("SCHEDULE TBL : "+serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList().size());
	}

	/********************************** anil code *********************************************************/

	private void checkingServiceStatusAgainstTicket(int ticketNumber,
			int contractId) {

		System.out.println("TiCKET NUMBER :: " + ticketNumber);
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;

		temp = new Filter();
		temp.setLongValue(c.getCompanyId());
		temp.setQuerryString("companyId");
		filtervec.add(temp);

		temp = new Filter();
		temp.setIntValue(ticketNumber);
		temp.setQuerryString("ticketNumber");
		filtervec.add(temp);

		temp = new Filter();
		temp.setIntValue(contractId);
		temp.setQuerryString("contractCount");
		filtervec.add(temp);

		querry.setFilters(filtervec);
		querry.setQuerryObject(new Service());

		service.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					public void onSuccess(ArrayList<SuperModel> result) {
						System.out.println("Ticket number result size :: "
								+ result.size());
						if (result.size() > 0) {
							for (SuperModel model : result) {
								Service entity = (Service) model;
								if (entity.getStatus().equals(
										Service.SERVICESTATUSCOMPLETED)) {
									System.out
											.println("SERVICE COMPLETED!!!!!!");
									/**
									 *  nidhi 
									 *  7-10-2017
									 *  for set cusstomer name and id for approval info
									 */
									form.getManageapproval().setBpId(entity.getCustomerId()+"");
									form.getManageapproval().setBpName(entity.getPersonInfo().getFullName());
									form.hideWaitSymbol();//Vijay
									form.getManageapproval().reactToRequestForApproval();
									
									/**
									 *  end
									 */
								} else {
									System.out
											.println("SERVICE NOT COMPLETED!!!!!!");
									form.showDialogMessage("Please complete service first!");
									form.hideWaitSymbol();//Vijay
								}
							}
						} else {
							form.showDialogMessage("Please complete service first!");
							form.hideWaitSymbol();//Vijay
						}
					}

					public void onFailure(Throwable throwable) {
						form.showDialogMessage("An unexpected error occurred!");
						throwable.printStackTrace();
						form.hideWaitSymbol();//Vijay
					}

				});
	}

	/*****************************************************************************************/

	private void getTaxesDetails() {
		final GenricServiceAsync service = GWT.create(GenricService.class);
		MyQuerry query = new MyQuerry();
		Company c = new Company();
		System.out.println("Company Id :: " + c.getCompanyId());

		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);

		query.setFilters(filtervec);
		query.setQuerryObject(new TaxDetails());
		form.showWaitSymbol();
		service.getSearchResult(query,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
					}

					public void onSuccess(ArrayList<SuperModel> result) {

						System.out.println("result size of tax " + result);
						servicetaxlist = new ArrayList<TaxDetails>();
						vattaxlist = new ArrayList<TaxDetails>();
						List<TaxDetails> backlist = new ArrayList<TaxDetails>();

						for (SuperModel model : result) {
							TaxDetails entity = (TaxDetails) model;
							backlist.add(entity);
							if (entity.getServiceTax().equals(true)) {
								servicetaxlist.add(entity);
							}
							if (entity.getVatTax().equals(true)) {
								vattaxlist.add(entity);
							}
						}
						System.out.println();
						System.out.println();
						for (int i = 0; i < servicetaxlist.size(); i++) {
							System.out.println(servicetaxlist.get(i)
									.getTaxChargeName()
									+ " "
									+ servicetaxlist.get(i)
											.getTaxChargePercent());
						}
						System.out.println();
						System.out.println();
						for (int i = 0; i < vattaxlist.size(); i++) {
							System.out.println(vattaxlist.get(i)
									.getTaxChargeName()
									+ " "
									+ vattaxlist.get(i).getTaxChargePercent());
						}
						System.out.println();
						System.out.println();

						updateTaxes();

					}
				});
	}

	private void updateTaxes() {
		Vector<Filter> filtervec = new Vector<Filter>();

		MyQuerry querry = new MyQuerry();
		Filter filter = null;

		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("startDate >=");
		filter.setDateValue(frmAndToPop.fromDate.getValue());
		filtervec.add(filter);

		filter = new Filter();
		filter.setQuerryString("startDate <=");
		filter.setDateValue(frmAndToPop.toDate.getValue());
		filtervec.add(filter);

		// filter = new Filter();
		// filter.setQuerryString("status");
		// filter.setStringValue(BillingDocument.CREATED);
		// filtervec.add(filter);

		querry.setFilters(filtervec);
		querry.setQuerryObject(new Contract());
		// form.showWaitSymbol();
		genasync.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						if (result.size() != 0) {
							for (SuperModel model : result) {
								Contract con = (Contract) model;

								for (int i = 0; i < con.getItems().size(); i++) {

									if (con.getItems().get(i).getServiceTax() != null) {
										for (int j = 0; j < servicetaxlist
												.size(); j++) {
											if (con.getItems().get(i)
													.getServiceTax()
													.getPercentage() == servicetaxlist
													.get(j)
													.getTaxChargePercent()) {
												con.getItems()
														.get(i)
														.setServiceTaxEdit(
																servicetaxlist
																		.get(j)
																		.getTaxChargeName());
											}
										}
									}

									if (con.getItems().get(i).getVatTax() != null) {
										for (int j = 0; j < vattaxlist.size(); j++) {
											if (con.getItems().get(i)
													.getVatTax()
													.getPercentage() == vattaxlist
													.get(j)
													.getTaxChargePercent()) {
												con.getItems()
														.get(i)
														.setVatTaxEdit(
																vattaxlist
																		.get(j)
																		.getTaxChargeName());
											}
										}
									}
								}

								genasync.save(con,
										new AsyncCallback<ReturnFromServer>() {
											@Override
											public void onFailure(
													Throwable caught) {
												form.hideWaitSymbol();
											}

											@Override
											public void onSuccess(
													ReturnFromServer result) {
												form.hideWaitSymbol();
												form.showDialogMessage("Entity updated successfully!");
											}
										});
							}
						}
					}

					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
						form.showDialogMessage("Unexpected Error!");
					}
				});
	}

	// rohan added this method to create services
	private void createServicesByContract() {
		form.showWaitSymbol();
		async.createServicesFromContrat(model, new AsyncCallback<Boolean>() {

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
				form.showDialogMessage(caught.getMessage());
			}

			@Override
			public void onSuccess(Boolean result) {
				if (result) {
					form.hideWaitSymbol();
					form.showDialogMessage("service created successful.....!!!");
				} else {
					form.hideWaitSymbol();
					form.showDialogMessage("services already exist.....!!!");
				}
			}
		});
	}

	
	
	/**
	 * rohan added this code for cancelling contract
	 * Cotract Cancel Logic
	 * Date :28/2/2017  
	 */
	
		private void reactOnCancelContract(final String remark) {
			
			final String loginUser = LoginPresenter.loggedInUser.trim();
			form.showWaitSymbol();
			update.reactOnCancelContract(model,remark,loginUser, new AsyncCallback<Void>() {
				
				@Override
				public void onSuccess(Void result) {
					model.setStatus(Contract.CANCELLED);
					form.getTbQuotationStatus().setValue(Contract.CANCELLED);
					form.getTaDescription().setValue(model.getDescription() + "\n" + "Contract Id ="
					+ model.getCount() + " "
					+ "Contract Status = "+model.getStatus()
					+ "\n"
					+ "has been cancelled by " + loginUser
					+ " with remark" + "\n" + "Remark ="
					+ remark);
					form.hideWaitSymbol();
					form.showDialogMessage("Contract Cancellation Successful..!");
					form.setToViewState();
				}
				
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
					form.showDialogMessage("Fail Try Again..!");
				}
			});
		}
	
	/**
	 * ends here 
	 */
		
		
		//  rohan added this code for communication logs in quotation
		@Override
		public void reactOnCommunicationLog() {
			
			form.showWaitSymbol();
			MyQuerry querry = AppUtility.communicationLogQuerry(model.getCount(), model.getCompanyId(),AppConstants.SERVICEMODULE,AppConstants.CONTRACT);
			
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					ArrayList<InteractionType> list = new ArrayList<InteractionType>();
					
					for(SuperModel model : result){
						
						InteractionType interactionType = (InteractionType) model;
						list.add(interactionType);
						
					}
					/** date 4.1.2019 added by komal to sort communication log list **/
					Comparator<InteractionType> comp=new Comparator<InteractionType>() {
						@Override
						public int compare(InteractionType e1, InteractionType e2) {
							if (e1.getCount() == e2.getCount()) {
								return 0;
							}
							if (e1.getCount() > e2.getCount()) {
								return -1;
							} else {
								return 1;
							}
						}
					};
					Collections.sort(list,comp);
					form.hideWaitSymbol();
					CommunicationLogPopUp.getRemark().setValue("");
					CommunicationLogPopUp.getDueDate().setValue(null);
					CommunicationLogPopUp.getOblinteractionGroup().setSelectedIndex(0);
					CommunicationLogPopUp.getCommunicationLogTable().getDataprovider().setList(list);
//					communicationPanel = new PopupPanel(true);
//					communicationPanel.add(CommunicationLogPopUp);
//					communicationPanel.show();
//					communicationPanel.center();
					
					LoginPresenter.communicationLogPanel = new PopupPanel(true);
					LoginPresenter.communicationLogPanel.add(CommunicationLogPopUp);
					LoginPresenter.communicationLogPanel.show();
					LoginPresenter.communicationLogPanel.center();
				}
				
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}
			});
			
		}
		
		@Override
		protected void reactOnGo() {
//			super.reactOnGo();
			if (view.getSearchpopupscreen().validate()) {
				view.showWaitSymbol();
				MyQuerry querry = view.getSearchpopupscreen().getQuerry();
				view.getSearchpopupscreen().getSupertable().connectToLocal();
				service.getContractSearchResult(querry,new AsyncCallback<ArrayList<Contract>>() {
					@Override
					public void onSuccess(ArrayList<Contract> result) {
						if (result.size() == 0){
							view.showDialogMessage("No Result To Display !");
							}

						/**
						 * Date 15/3/2018
						 * By jayshree
						 * To sort the result by default in assending order
						 */
						if(result.size()>0){
						Comparator<Contract> comp=new Comparator<Contract>() {
							@Override
							public int compare(Contract e1, Contract e2) {
//								Integer coutn1=o1.getCount();
//								Integer count2=o2.getCount();
//								return ;
								if (e1.getCount() == e2.getCount()) {
									return 0;
								}
								if (e1.getCount() > e2.getCount()) {
									return -1;
								} else {
									return 1;
								}
							}
						};
						Collections.sort(result,comp);
						}
						//End By jayshree
						/**
						 * @author Anil
						 * @since 18-02-2022
						 */
						try{
							if(result!=null&&result.size()!=0){
								Console.log("Contract Result Size : "+result.size());
							}
							Console.log("Contract Search tab number : "+view.getSearchpopupscreen().tabPanel.getDeckPanel().getVisibleWidget());
							if(view.getSearchpopupscreen().tabPanel.getDeckPanel().getVisibleWidget()==0){
								Console.log("First Filter Selected!!");
								ContractPresenterSearchProxy search=(ContractPresenterSearchProxy) view.getSearchpopupscreen();
								search.getFilteredData(result);
								Console.log("Filtered Contract Result Size : "+view.getSearchpopupscreen().getSupertable().getListDataProvider().getList().size());
								view.hideWaitSymbol();
								return;
							}
						}catch(Exception e){
							
						}
						
						for (Contract model : result) {
							 view.getSearchpopupscreen().getSupertable().getListDataProvider().getList().add(model);
						}
						view.getSearchpopupscreen().getSupertable().getTable().setRowCount(result.size());
						view.hideWaitSymbol();
					}

					@Override
					public void onFailure(Throwable caught) {
						caught.printStackTrace();
						view.showDialogMessage("Failed To Retrive Search Result!");
						view.hideWaitSymbol();
					}
				});
			}
		
		}
		
		/**
		 * This mthod is used for creating EVA Services On date : 19-06-2017
		 */
		private void updateServicesAsPerEVARequirement() 
		{
			async.createServicesAsPerEVA(model, new AsyncCallback<Boolean>() {


				@Override
				public void onSuccess(Boolean result) {
					
					if(result){
						form.showDialogMessage("Service created successfully..!");
					}else{
						form.showDialogMessage("Services already created...!");
					}
				}

				@Override
				public void onFailure(Throwable caught) {
					
				}
			});
		}
		
		/** date 1/11/2017 added by komal for new contract cancel **/
		private void getAllDocumentDetails(){/*
			update.getRecordsForContractCancel(model.getCompanyId(), model.getCount(), new AsyncCallback<ArrayList<CancelContract>>(){

				@Override
				public void onSuccess(ArrayList<CancelContract> result) {
					// TODO Auto-generated method stub
					cancellationPopup.getAllContractDocumentTable().getDataprovider().getList().clear();
                    System.out.println("result size cancel : "+result.size());
                    if(!(model.getStatus().equalsIgnoreCase(Contract.CANCELLED))){
	                    CancelContract canContract = new CancelContract();
	            		canContract.setDocumnetId(model.getCount());
	            		canContract.setBranch(model.getBranch());
	            		canContract.setBpName(model.getCinfo().getFullName());
	            		canContract.setDocumentStatus(model.getStatus());
	            		canContract.setDocumnetName(AppConstants.CONTRACT);
	            		canContract.setRecordSelect(true);
	            		cancellationPopup.getAllContractDocumentTable().getDataprovider().getList().add(canContract);
                    }
                    cancellationPopup.getAllContractDocumentTable().getDataprovider().getList().addAll(result);
					cancellationPopup.getAllContractDocumentTable().getTable().redraw();
					if(cancellationPopup.getAllContractDocumentTable().getDataprovider().getList().size() > 0){
						 cancellationPopup.showPopUp();
						}else{
							form.showDialogMessage("No documents are found for cancellation");
						}	
				} 
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					System.out.println("can not load data");
				}

				
			});
		*/
			

			update.getRecordsForContractCancel(model.getCompanyId(), model.getCount(),contractDisContinue,true,"ContractPresenter", new AsyncCallback<ArrayList<CancelContract>>(){

				@Override
				public void onSuccess(ArrayList<CancelContract> result) {
					// TODO Auto-generated method stub
					cancellationPopup.getAllContractDocumentTable().getDataprovider().getList().clear();
                    System.out.println("result size cancel : "+result.size());
                    if(!(model.getStatus().equalsIgnoreCase(Contract.CANCELLED)) && !contractDisContinue){
	                    CancelContract canContract = new CancelContract();
	            		canContract.setDocumnetId(model.getCount());
	            		canContract.setBranch(model.getBranch());
	            		canContract.setBpName(model.getCinfo().getFullName());
	            		canContract.setDocumentStatus(model.getStatus());
	            		canContract.setDocumnetName(AppConstants.CONTRACT);
	            		canContract.setRecordSelect(true);
	            		cancellationPopup.getAllContractDocumentTable().getDataprovider().getList().add(canContract);
                    }
                    cancellationPopup.getAllContractDocumentTable().getDataprovider().getList().addAll(result);
					cancellationPopup.getAllContractDocumentTable().getTable().redraw();
					
					if(cancellationPopup.getAllContractDocumentTable().getDataprovider().getList().size() > 0){
						cancellationPopup.showPopUp();
						Timer timer=new Timer() {
							@Override
							public void run() {
								form.hideWaitSymbol();
							}
						};
						timer.schedule(5000); 
					
						}else{
							/** nidhi 18-05-2018 */
							if(!contractDisContinue){
								form.showDialogMessage("No documents are found for cancellation");
							}else{
								
							}
						}	
				} 
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					System.out.println("can not load data");
				}
				
			});
		
		
		}
		
		private void reactOnCancelContractButton(final String remark){
			final String loginUser = LoginPresenter.loggedInUser.trim();
			List<CancelContract> cancelContractList = cancellationPopup.getAllContractDocumentTable().getDataprovider().getList();
			ArrayList<CancelContract> selectedCancelContractList = new ArrayList<CancelContract>();
			for(CancelContract canContract : cancelContractList){
				if(canContract.isRecordSelect()){
					selectedCancelContractList.add(canContract);
				}
			}
			System.out.println("Selected list size :" + selectedCancelContractList.size());
			update.saveContractCancelData(model, remark, loginUser, selectedCancelContractList, new AsyncCallback<Void>(){

				@Override
				public void onSuccess(Void result) {
					// TODO Auto-generated method stub
					
					if(!(model.getStatus().equals(Contract.CANCELLED))  && !contractDisContinue){
							model.setStatus(Contract.CANCELLED);
							form.getTbQuotationStatus().setValue(Contract.CANCELLED);
							form.getTaDescription().setValue(model.getDescription() + "\n" + "Contract Id ="
							+ model.getCount() + " "
							+ "Contract Status = "+model.getStatus()
							+ "\n"
							+ "has been cancelled by " + loginUser
							+ " with remark" + "\n" + "Remark ="
							+ remark);
							
							form.hideWaitSymbol();
							form.showDialogMessage("Contract Cancellation process started. It may take few minutes.");
							form.setToViewState();
							// Date 01-04-2019 by Vijay for NBHC CCPM to download cancelled remark only ***/
							model.setRemark(remark);
							model.setCancellationDate(new Date());
							if(model.getStatus().equals(Contract.CANCELLED)) {
								Console.log("Contract for cancellation for SMS");
								reactOnSendSMS();
							}
					}
					
				
					/**
					 * nihdi
					 * 19-05-2018
					 * for discontinue contract 
					 */
					if(contractDisContinue){
						model.setStatus(Contract.CONTRACTDISCOUNTINED);
						model.setDescription(model.getDescription() + "\n" + "Contract Id ="
						+ model.getCount() + " "
						+ "Contract Status = "+model.getStatus()
						+ "\n"
						+ "has been cancelled by " + loginUser
						+ " with remark" + "\n" + "Remark ="
						+ remark);
						form.getTbQuotationStatus().setValue(Contract.CONTRACTDISCOUNTINED);
						form.getTaDescription().setValue(model.getDescription() + "\n" + "Contract Id ="
						+ model.getCount() + " "
						+ "Contract Status = "+model.getStatus()
						+ "\n"
						+ "has been cancelled by " + loginUser
						+ " with remark" + "\n" + "Remark ="
						+ remark);
						contractDisContinue = false;
						// Date 01-04-2019 by Vijay for NBHC CCPM to download cancelled remark only ***/
						model.setRemark(remark);
						genasync.save(model, new AsyncCallback<ReturnFromServer>() {
							
							@Override
							public void onSuccess(ReturnFromServer result) {
								// TODO Auto-generated method stub
								form.hideWaitSymbol();
								form.showDialogMessage("Contract Discontinued Successfully..!");
								form.setToViewState();
							}
							
							
							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								
							}
						});
						
					}
				
				}
				
				
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
					form.showDialogMessage("Please Try again...!");
					
				}

				
				
			});
			
		}
		
		public List<ServiceSchedule> getUpdatedScheduledServices(List<ServiceSchedule> list,String subFilterType,int dayIndex,int weekIndex, int excludeDay){
			
			sortSechduleServicesByProdSerNoAndSerSrNo(list);
			Date temp =null;
			boolean flag =false;
			boolean sameMonthserviceflag = false;
			boolean serviceGreaterFlag = false;
			
//			HashSet<UniqueProduct> productsHs =new HashSet<ContractPresenter.UniqueProduct>();
			HashSet<Integer> productsHs =new HashSet<Integer>();
			
			HashSet<Integer> productsSrNoHs =new HashSet<Integer>();
//			HashMap<Integer,Integer> productsMap=new HashMap<Integer,Integer>();
			for(ServiceSchedule serSch:list){
//				UniqueProduct obj=new UniqueProduct();
//				obj.setProductId(serSch.getScheduleProdId());
//				obj.setSrNo(serSch.getSerSrNo());
//				productsHs.add(obj);
				productsHs.add(serSch.getScheduleProdId());
				productsSrNoHs.add(serSch.getSerSrNo());
				
				
			}
			System.out.println("HASH SET SIZE : "+productsHs.size());
//			ArrayList<UniqueProduct> productList =new ArrayList<ContractPresenter.UniqueProduct>(productsHs);
			ArrayList<Integer> productList =new ArrayList<Integer>(productsHs);
			System.out.println("UNIQUE PROD LIST  SIZE : "+productList.size());
			
			ArrayList<SalesLineItem> itemList=new ArrayList<SalesLineItem>();
			for(Integer obj:productList){
				for(Integer serSrNo:productsSrNoHs){
					for(SalesLineItem item:form.getSaleslineitemtable().getDataprovider().getList()){
						if(obj==item.getPrduct().getCount()&&serSrNo==item.getProductSrNo()){
							itemList.add(item);
						}
					}
				}
			}
			System.out.println("ITEM LIST SIZE : "+itemList.size());
			
			for(SalesLineItem item:itemList){
				/**
				 * Filtering data through week 
				 */
				if(subFilterType.equals("Week")){
					
					Date Stardaate = null;
					Stardaate = item.getStartDate();
					long noServices = (long) (item.getNumberOfServices());
					int noOfdays = item.getDuration();
					int interval = (int) (noOfdays / item.getNumberOfServices());
					/** @author Vijay Date :- 25-09-2021
					 * Des :- if service scheduling process config is active then new min and max round up logic will apply
					 * for innovative
					 */
					if(LoginPresenter.serviceScheduleNewLogicFlag){
						double days = noOfdays;
						double noofservices = noServices;
						double dbinterval = days/noofservices;
						interval = form.getValueWithRoundUp(dbinterval);
					}
					Date servicedate = item.getStartDate();
//					Date d = new Date(servicedate.getTime());
					int months = noOfdays/30;
					
					if(noServices>months){
						serviceGreaterFlag = true;
						form.showDialogMessage("Services greater than availabe weeks can not schedule services with weeks");
						serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
						return null;
					}
					
					Date productEndDate = new Date(servicedate.getTime());
					CalendarUtil.addDaysToDate(productEndDate, noOfdays);
					Date prodenddate = new Date(productEndDate.getTime());
//					productEndDate = prodenddate;
					/**
					 * nidhi
					 *  ))..
					 *  for exclude day.
					 */
					productEndDate = calculateEndDate(item.getStartDate(),item.getDuration()-1);
					int startday = servicedate.getDay();
					
					if(excludeDay !=0  && startday == excludeDay-1){
						CalendarUtil.addDaysToDate(servicedate, 1);
					}
					Date d = new Date(servicedate.getTime());
					
					
					if(excludeDay !=0 ){
						CalendarUtil.addDaysToDate(productEndDate, -1);
					}
					int lastday = productEndDate.getDay();
					if(lastday == excludeDay -1){
						CalendarUtil.addDaysToDate(productEndDate, -1);
					}
					/**
					 * end
					 */
					
				
					for(ServiceSchedule schSer:list){
						
						if(item.getPrduct().getCount()==schSer.getScheduleProdId()
								&&item.getProductSrNo()==schSer.getSerSrNo()){
							
						Date day = new Date(d.getTime());
						if(temp!=null){
							String currentServiceMonth =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( day ).split("-")[1];
							String lastserviceDateMonth = DateTimeFormat.getFormat( "d-M-yyyy" ).format( temp ).split("-")[1];
							if(currentServiceMonth.equals(lastserviceDateMonth)){
								sameMonthserviceflag =true;
							}
						}
						if(sameMonthserviceflag){
							CalendarUtil.addDaysToDate(day, 30);
						}
						
						CalendarUtil.setToFirstDayOfMonth(day);
						if (weekIndex == 1) {
							day = AppUtility.getCalculatedDate(day,dayIndex);
							if (day.before(Stardaate)) {
								if (schSer.getScheduleServiceNo() == 1) {
									day = Stardaate;
								} else {
									flag = true;
								}
							}
							if (flag || schSer.getScheduleServiceNo() > 1) {
								day = AppUtility.getCalculatedDate(day,dayIndex);
							}
						}else if (weekIndex == 2) {
							CalendarUtil.addDaysToDate(day, 7);
							day = AppUtility.getCalculatedDate(day,dayIndex);
							if (day.before(Stardaate)) {
								if (schSer.getScheduleServiceNo() == 1) {
									day = Stardaate;
								} else {
									flag = true;
								}
							}
							if (flag || schSer.getScheduleServiceNo() > 1) {
								CalendarUtil.setToFirstDayOfMonth(day);
								CalendarUtil.addDaysToDate(day, 7);
								day = AppUtility.getCalculatedDate(day,dayIndex);
							}
						}else if (weekIndex == 3) {
							CalendarUtil.addDaysToDate(day, 14);
							day = AppUtility.getCalculatedDate(day,dayIndex);
							if (day.before(Stardaate)) {
								if (schSer.getScheduleServiceNo() == 1) {
									day = Stardaate;
								} else {
									flag = true;
								}
							}
							if (flag || schSer.getScheduleServiceNo() > 1) {
								CalendarUtil.setToFirstDayOfMonth(day);
								CalendarUtil.addDaysToDate(day, 14);
								day = AppUtility.getCalculatedDate(day,dayIndex);
							}
						}else if (weekIndex == 4) {
							CalendarUtil.addDaysToDate(day, 21);
							day = AppUtility.getCalculatedDate(day,dayIndex);
							if (day.before(Stardaate)) {
								if (schSer.getScheduleServiceNo() == 1) {
									day = Stardaate;
								} else {
									flag = true;
								}
							}
							if (flag || schSer.getScheduleServiceNo() > 1) {
								CalendarUtil.setToFirstDayOfMonth(day);
								CalendarUtil.addDaysToDate(day, 21);
								day = AppUtility.getCalculatedDate(day,dayIndex);
							}
						}
						
						
						/**
						 * nidhi
						 *  ))..
						 *  for exclude day.
						 */
						int serDay = day.getDay();
						if(excludeDay !=0  && serDay == excludeDay-1){
							CalendarUtil.addDaysToDate(day, 1);
						
						}
						/**
						 * end
						 */
						
						temp = day;
						
						
						if (productEndDate.before(day) == false) {
							schSer.setScheduleServiceDate(day);
						} else {
							schSer.setScheduleServiceDate(productEndDate);
							
						}
						CalendarUtil.addDaysToDate(d, interval);
						int days =0;
						String serviceStartDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( Stardaate ).split("-")[0];
						if(serviceStartDate.equals("1")){
							String currentServiceDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( d ).split("-")[0];
							days = AppUtility.checkDateMonthOFEndAndAddDays(currentServiceDate,d);
							CalendarUtil.addDaysToDate(d, days);
						}
						Date tempdate = new Date(d.getTime());
						d = tempdate;
						
						schSer.setScheduleServiceDay(ContractForm.serviceDay(schSer.getScheduleServiceDate()));
						int weeknumber = AppUtility.getweeknumber(schSer.getScheduleServiceDate());
						schSer.setWeekNo(weeknumber);
//						schSer.setServiceProductList(serviceSchedulePopUp.getPopScheduleTable()
//								.getDataprovider().getList().get(i).getServiceProductList())
								 /***
								  * nidhi *:*:*
								  * 18-09-2018
								  * for map bill product
								  * if any update please update in else conditions also
								  */
								 if(LoginPresenter.billofMaterialActive){
									 schSer.setServiceProductList(schSer.getServiceProductList());
								 }
								 /**
								  * end
								  */
					}
						
				}
			}
			/**
			 * Filtering through Day
			 */
			else if(subFilterType.equals("Day")){
				long noServices = (long) (item.getNumberOfServices());
				int noOfdays = item.getDuration();
				double nodays =  noOfdays;
				double noofservices= item.getNumberOfServices();
				double interval =  nodays / noofservices;
				double tempinterval = interval;
				double tempvalue=interval;
				
				/** @author Vijay Date :- 25-09-2021
				 * Des :- if service scheduling process config is active then new min and max round up logic will apply
				 * for innovative
				 */
				if(LoginPresenter.serviceScheduleNewLogicFlag){
					double days = noOfdays;
					double noofservice = noServices;
					double dbinterval = days/noofservice;
					interval = form.getValueWithRoundUp(dbinterval);
				}
				/**
				 * ends here
				 */
				Date servicedate = item.getStartDate();
//				Date d = new Date(servicedate.getTime());
				Date productEndDate = new Date(servicedate.getTime());
				CalendarUtil.addDaysToDate(productEndDate, noOfdays);
				Date prodenddate = new Date(productEndDate.getTime());
				
//				productEndDate = prodenddate;
				/**
				 * nidhi
				 *  ))..
				 *  for exclude day.
				 */
				productEndDate = calculateEndDate(item.getStartDate(),item.getDuration()-1);
				int startday = servicedate.getDay();
				
				if(excludeDay !=0  && startday == excludeDay-1){
					CalendarUtil.addDaysToDate(servicedate, 1);
				}
				Date d = new Date(servicedate.getTime());
				
				
				if(excludeDay !=0 ){
					CalendarUtil.addDaysToDate(productEndDate, -1);
				}
				int lastday = productEndDate.getDay();
				if(lastday == excludeDay -1){
					CalendarUtil.addDaysToDate(productEndDate, -1);
				}
				/**
				 * end
				 */
				
				
				/**
				 * Date 22-10-2019
				 * Des :- Day wise scheduling with branch wise services and with product wise scheduling
				 */
				HashSet<String> customerBranch = new HashSet<String>();
				
				for(ServiceSchedule schSer:list){
					customerBranch.add(schSer.getScheduleProBranch());
				}
				ArrayList<ServiceSchedule> serviceschedulelist = new ArrayList<ServiceSchedule>();
				for(String strcustomerBranch :customerBranch ){
					ArrayList<ServiceSchedule> branchwiselist = new ArrayList<ServiceSchedule>();
					for(ServiceSchedule schSer:list){
						if(strcustomerBranch.trim().equals(schSer.getScheduleProBranch())){
							branchwiselist.add(schSer);
						}
					}
					Date tempdate = new Date(d.getTime());
					serviceschedulelist.addAll(scheduleDayWiseData(branchwiselist,item,dayIndex,tempdate,weekIndex,temp,sameMonthserviceflag,flag,
											interval,tempvalue,tempinterval,productEndDate));
				}
				return serviceschedulelist;
				/**
				 * Ends here
				 */
				
				
//				for(ServiceSchedule schSer:list){
//					
//					if(item.getPrduct().getCount()==schSer.getScheduleProdId()
//							&&item.getProductSrNo()==schSer.getSerSrNo()){
//					
//					Date Stardaate = null;
//					Stardaate = item.getStartDate();
//					
//					String dayOfWeek1 = format.format(d);
//					int adate = Integer.parseInt(dayOfWeek1);
//					int adday1 = dayIndex - 1;
//					
//					int adday = 0;
//					if (adate <= adday1) {
//						adday = (adday1 - adate);
//					} else {
//						adday = (7 - adate + adday1);
//					}
//					Date newDate = new Date(d.getTime());
//					CalendarUtil.addDaysToDate(newDate, adday);
//
//					Date day = new Date(newDate.getTime());
//					
//					if(weekIndex!=0){
//						if(temp!=null){
//							String currentServiceMonth =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( day ).split("-")[1];
//							String lastserviceDateMonth = DateTimeFormat.getFormat( "d-M-yyyy" ).format( temp ).split("-")[1];
//							if(currentServiceMonth.equals(lastserviceDateMonth)){
//								sameMonthserviceflag =true;
//							}else{
//								sameMonthserviceflag= false;
//							}
//						}
//						if(sameMonthserviceflag){
//							CalendarUtil.addDaysToDate(day, 10);
//						}
//												
//						CalendarUtil.setToFirstDayOfMonth(day);
//						if(weekIndex==1){
//							day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
//							if(day.before(Stardaate)){
//								if(schSer.getScheduleServiceNo() == 1){
//									day =Stardaate;
//								}else{
//									flag = true;
//								}
//							}
//							if(flag || schSer.getScheduleServiceNo() >1){
//								CalendarUtil.setToFirstDayOfMonth(day);
//								day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
//							}
//						}else if(weekIndex==2){
//							
//							CalendarUtil.addDaysToDate(day, 7);
//
//							 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
//							if(day.before(Stardaate)){
//								if(schSer.getScheduleServiceNo()==1){
//									day =Stardaate;
//								}
//								else{
//									flag = true;
//								}
//							}
//							if(flag || schSer.getScheduleServiceNo()>1){
//								CalendarUtil.setToFirstDayOfMonth(day);
//								CalendarUtil.addDaysToDate(day, 7);
//								 day = AppUtility.getCalculatedDatewithWeek(day, adday1);;
//							}
//						}else if(weekIndex==3){
//							CalendarUtil.addDaysToDate(day, 14);
//							 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
//							if(day.before(Stardaate)){
//								if(schSer.getScheduleServiceNo()==1){
//									day =Stardaate;
//								}
//								else{
//									flag = true;
//								}
//							}
//							if(flag || schSer.getScheduleServiceNo()>1){
//								CalendarUtil.setToFirstDayOfMonth(day);
//								CalendarUtil.addDaysToDate(day, 14);
//								 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
//							}
//						}else if(weekIndex==4){
//							CalendarUtil.addDaysToDate(day, 21);
//
//							 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
//							if(day.before(Stardaate)){
//								if(schSer.getScheduleServiceNo()==1){
//									day =Stardaate;
//								}
//								else{
//									flag = true;
//								}
//							}
//							if(flag || schSer.getScheduleServiceNo()>1){
//								CalendarUtil.setToFirstDayOfMonth(day);
//								CalendarUtil.addDaysToDate(day, 21);
//								 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
//							}
//					     }
//						
//						 temp = day;
//						
//						CalendarUtil.addDaysToDate(d, (int)interval);
//						int days =0;
//						String serviceStartDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( Stardaate ).split("-")[0];
//						if(serviceStartDate.equals("1")){
//							String currentServiceDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( d ).split("-")[0];
//							 days = AppUtility.checkDateMonthOFEndAndAddDays(currentServiceDate,d);
//							CalendarUtil.addDaysToDate(d, days);
//
//						}
//					}
//					if (productEndDate.before(day) == false) {
//						schSer.setScheduleServiceDate(day);
//					} else {
//						schSer.setScheduleServiceDate(productEndDate);
//					}
//					int interval2 =(int) Math.round(interval);
//					if(weekIndex==0){
//						CalendarUtil.addDaysToDate(d, interval2);
//					}
//					Date tempdate = new Date(d.getTime());
//					d = tempdate;
//					schSer.setScheduleServiceDay(ContractForm.serviceDay(schSer.getScheduleServiceDate()));
//					
//					interval =(interval+tempinterval)-tempvalue;
//				    tempvalue = Math.round(interval);
//				   
//					int weeknumber = AppUtility.getweeknumber(schSer.getScheduleServiceDate());
//					schSer.setWeekNo(weeknumber);
////					schSer.setServiceProductList(serviceSchedulePopUp.getPopScheduleTable()
////							.getDataprovider().getList().get(i).getServiceProductList())
//					 /***
//					  * nidhi *:*:*
//					  * 18-09-2018
//					  * for map bill product
//					  * if any update please update in else conditions also
//					  */
//					 if(LoginPresenter.billofMaterialActive){
//						 schSer.setServiceProductList(schSer.getServiceProductList());
//					 }
//					 /**
//					  * end
//					  */
//				}
//				}
			}
			/**
			 * Filtering through Time
			 */
			else if(subFilterType.equals("Time")){
				for(ServiceSchedule schSer:list){
					String serviceHrs = serviceSchedulePopUp.getP_servicehours().getItemText(serviceSchedulePopUp.getP_servicehours().getSelectedIndex());
					String serviceMin = serviceSchedulePopUp.getP_servicemin().getItemText(serviceSchedulePopUp.getP_servicemin().getSelectedIndex());
					String serviceAmPm = serviceSchedulePopUp.getP_ampm().getItemText(serviceSchedulePopUp.getP_ampm().getSelectedIndex());
					String calculatedServiceTime = serviceHrs + ":" + serviceMin + ""+ serviceAmPm;
					schSer.setScheduleServiceTime(calculatedServiceTime);
				}
			}
			/**
			 * Filtering through Interval
			 */
			else if(subFilterType.equals("Interval")){
				serviceSchedulePopUp.getP_customise().setValue(false);
				serviceSchedulePopUp.getP_default().setValue(false);
				serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
				serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
				serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
				serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
				serviceSchedulePopUp.getP_servicehours().setEnabled(true);
				serviceSchedulePopUp.getP_servicemin().setEnabled(true);
				serviceSchedulePopUp.getP_ampm().setEnabled(true);
				serviceSchedulePopUp.getP_customise().setEnabled(true);
				serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
				
				long noServices = (long) (item.getNumberOfServices());
				int inreval = serviceSchedulePopUp.getP_interval().getValue();
				int noOfdays = item.getDuration();
				Date servicedate = new Date(item.getStartDate().getTime());
//				Date d = new Date(servicedate.getTime());

				Date productEndDate = new Date(servicedate.getTime());
				CalendarUtil.addDaysToDate(productEndDate, noOfdays);
				Date prodenddate = new Date(productEndDate.getTime());
				productEndDate = prodenddate;
				Date tempdate = new Date();
				
				/**
				 * nidhi
				 *  ))..
				 *  for exclude day.
				 */
				productEndDate = calculateEndDate(item.getStartDate(),item.getDuration()-1);
				int startday = servicedate.getDay();
				
				if(excludeDay !=0  && startday == excludeDay-1){
					CalendarUtil.addDaysToDate(servicedate, 1);
				}
				Date d = new Date(servicedate.getTime());
				
				
				if(excludeDay !=0 ){
					CalendarUtil.addDaysToDate(productEndDate, -1);
				}
				int lastday = productEndDate.getDay();
				if(lastday == excludeDay -1){
					CalendarUtil.addDaysToDate(productEndDate, -1);
				}
				/**
				 * end
				 */
				
				
				for(ServiceSchedule schSer:list){
					
					if(item.getPrduct().getCount()==schSer.getScheduleProdId()
							&&item.getProductSrNo()==schSer.getSerSrNo()){
					if (schSer.getScheduleServiceNo() > 1) {
						CalendarUtil.addDaysToDate(d, inreval);
						
						/**
						 * nidhi
						 *  ))..
						 *  for exclude day.
						 */
						int serDay = d.getDay();
						if(excludeDay !=0  && serDay == excludeDay-1){
							CalendarUtil.addDaysToDate(d, 1);
						
						}
						/**
						 * end
						 */
						
						tempdate = new Date(d.getTime());
					}

					Date Stardaate = null;
					if (schSer.getScheduleServiceNo() == 1) {
						schSer.setScheduleServiceDate(servicedate);
						schSer.setScheduleServiceDay(ContractForm.serviceDay(schSer.getScheduleServiceDate()));
					}
					if (schSer.getScheduleServiceNo() != 1) {
						if (productEndDate.after(d)) {
							schSer.setScheduleServiceDate(tempdate);
							schSer.setScheduleServiceDay(ContractForm.serviceDay(schSer.getScheduleServiceDate()));
						} else {
							schSer.setScheduleServiceDate(productEndDate);
							schSer.setScheduleServiceDay(ContractForm.serviceDay(schSer.getScheduleServiceDate()));
						}
					}
					int weeknumber = AppUtility.getweeknumber(schSer.getScheduleServiceDate());
					schSer.setWeekNo(weeknumber);
//					schSer.setServiceProductList(serviceSchedulePopUp.getPopScheduleTable()
//							.getDataprovider().getList().get(i).getServiceProductList())
					 /***
					  * nidhi *:*:*
					  * 18-09-2018
					  * for map bill product
					  * if any update please update in else conditions also
					  */
					 if(LoginPresenter.billofMaterialActive){
						 schSer.setServiceProductList(schSer.getServiceProductList());
					 }
					 /**
					  * end
					  */
				}
				}
			}
			/**
			 * Filtering through Default
			 */
			else if(subFilterType.equals("Default")){
				serviceSchedulePopUp.getP_customise().setValue(false);
				serviceSchedulePopUp.getP_interval().setValue(null);
				serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);
				serviceSchedulePopUp.getP_customise().setEnabled(true);
				serviceSchedulePopUp.getP_servicehours().setEnabled(true);
				serviceSchedulePopUp.getP_servicemin().setEnabled(true);
				serviceSchedulePopUp.getP_ampm().setEnabled(true);
				serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
				serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
				serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);
				serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
				
				long noServices = (long) (item.getNumberOfServices());
				int noOfdays = item.getDuration();
				double days =  noOfdays;
				double noofservices= item.getNumberOfServices();
				double interval =  days / noofservices;
				
				/** @author Vijay Date :- 25-09-2021
				 * Des :- if service scheduling process config is active then new min and max round up logic will apply
				 * for innovative
				 */
				if(LoginPresenter.serviceScheduleNewLogicFlag){
					interval = form.getValueWithRoundUp(interval);
				}
				/**
				 * ends here
				 */
				
				double temp1 = interval;
				double tempvalue=interval;
				
				calculateEndDate(item.getStartDate(),item.getDuration());
				Date servicedate = item.getStartDate();
				
				Date d = new Date(servicedate.getTime());
				Date tempdate = new Date();
				
				for(ServiceSchedule schSer:list){
					if(item.getPrduct().getCount()==schSer.getScheduleProdId()
							&&item.getProductSrNo()==schSer.getSerSrNo()){
					int interval2 =(int) Math.round(interval);
					
					if (schSer.getScheduleServiceNo() > 1) {
						CalendarUtil.addDaysToDate(d, interval2);
						tempdate = new Date(d.getTime());
						interval =(interval+temp1)-tempvalue;
					    tempvalue = Math.round(interval);
					}
					
					schSer.setScheduleProdEndDate(calculateEndDate(item.getStartDate(), item.getDuration()));
					if (schSer.getScheduleServiceNo() == 1) {
						schSer.setScheduleServiceDate(servicedate);
					}
					if (schSer.getScheduleServiceNo() != 1) {
						if (calculateEndDate(item.getStartDate(),item.getDuration()).before(d) == false) {
							schSer.setScheduleServiceDate(tempdate);
						} else {
							schSer.setScheduleServiceDate(calculateEndDate(item.getStartDate(),item.getDuration()));
						}
					}
					schSer.setScheduleServiceDay(ContractForm.serviceDay(schSer.getScheduleServiceDate()));
					int weaknumber = AppUtility.getweeknumber(schSer.getScheduleServiceDate());
					schSer.setWeekNo(weaknumber);
//					schSer.setServiceProductList(serviceSchedulePopUp.getPopScheduleTable()
//							.getDataprovider().getList().get(i).getServiceProductList())
					 /***
					  * nidhi *:*:*
					  * 18-09-2018
					  * for map bill product
					  * if any update please update in else conditions also
					  */
					 if(LoginPresenter.billofMaterialActive){
						 schSer.setServiceProductList(schSer.getServiceProductList());
					 }
					 /**
					  * end
					  */
				}
				}
			}else if(subFilterType.equals("ExcludeDay")){
				
				long noServices = (long) (item.getNumberOfServices());
				int noOfdays = item.getDuration();
				double nodays =  noOfdays;
				double noofservices= item.getNumberOfServices();
				double interval2 =  nodays / noofservices;
				if(serviceSchedulePopUp.getP_interval().getValue() != null && serviceSchedulePopUp.getP_interval().getValue() != 0){
					interval2 =  serviceSchedulePopUp.getP_interval().getValue().doubleValue();
				}
//				double tempinterval = interval;
//				double tempvalue=interval;
				int interval = (int) Math.round(interval2);
				
				/** @author Vijay Date :- 25-09-2021
				 * Des :- if service scheduling process config is active then new min and max round up logic will apply
				 * for innovative
				 */
				if(LoginPresenter.serviceScheduleNewLogicFlag){
					interval = form.getValueWithRoundUp(interval);
				}
				/**
				 * ends here
				 */
				
				Date servicedate = item.getStartDate();
//				Date d = new Date(servicedate.getTime());
				Date productEndDate = new Date(servicedate.getTime());
				CalendarUtil.addDaysToDate(productEndDate, noOfdays);
				Date prodenddate = new Date(productEndDate.getTime());
				productEndDate = prodenddate;
				Date tempdate = new Date();
				/**
				 * nidhi
				 *  ))..
				 *  for exclude day.
				 */
				productEndDate = calculateEndDate(item.getStartDate(),item.getDuration()-1);
				int startday = servicedate.getDay();
				
				if(excludeDay !=0  && startday == excludeDay-1){
					CalendarUtil.addDaysToDate(servicedate, 1);
				}
				Date d = new Date(servicedate.getTime());
				
				
				if(excludeDay !=0 ){
					CalendarUtil.addDaysToDate(productEndDate, -1);
				}
				int lastday = productEndDate.getDay();
				if(lastday == excludeDay -1){
					CalendarUtil.addDaysToDate(productEndDate, -1);
				}
				/**
				 * end
				 */
				for(ServiceSchedule schSer:list){
					
					if(item.getPrduct().getCount()==schSer.getScheduleProdId()
							&&item.getProductSrNo()==schSer.getSerSrNo()){
					if (schSer.getScheduleServiceNo() > 1) {
						CalendarUtil.addDaysToDate(d,interval );
						
					}
					
					/**
					 * @author Vijay Date :- 09-02-2023
					 * Des :- added for exclude holidays and weekly off days from calnedar
					 */
					if(serviceSchedulePopUp.getE_mondaytofriday().getValue(serviceSchedulePopUp.getE_mondaytofriday().getSelectedIndex())
							.equals(AppConstants.HOLIDAYS)){
						Console.log("Inside exclude holiday");
						Calendar calendar = serviceSchedulePopUp.getOlbcalendar().getSelectedItem();
						form.excludeDateFromInHolidayDateFromCalendar(d,calendar);
					}
					else{
						
						/**
						 * nidhi
						 *  ))..
						 *  for exclude day.
						 */
						int serDay = d.getDay();
						if(excludeDay !=0  && serDay == excludeDay-1){
							CalendarUtil.addDaysToDate(d, 1);
						
						}
						/**
						 * end
						 */
						
					}
					tempdate = new Date(d.getTime());

					Date Stardaate = null;
					if (schSer.getScheduleServiceNo() == 1) {
						if(serviceSchedulePopUp.getE_mondaytofriday().getValue(serviceSchedulePopUp.getE_mondaytofriday().getSelectedIndex())
								.equals(AppConstants.HOLIDAYS)){
							schSer.setScheduleServiceDate(tempdate);
							schSer.setScheduleServiceDay(ContractForm.serviceDay(schSer.getScheduleServiceDate()));
						}
						else{
							schSer.setScheduleServiceDate(servicedate);
							schSer.setScheduleServiceDay(ContractForm.serviceDay(schSer.getScheduleServiceDate()));
						}
					
					}
					if (schSer.getScheduleServiceNo() != 1) {
						if (productEndDate.after(d)) {
							schSer.setScheduleServiceDate(tempdate);
							schSer.setScheduleServiceDay(ContractForm.serviceDay(schSer.getScheduleServiceDate()));
						} else {
							schSer.setScheduleServiceDate(productEndDate);
							schSer.setScheduleServiceDay(ContractForm.serviceDay(schSer.getScheduleServiceDate()));
						}
					}
					int weeknumber = AppUtility.getweeknumber(schSer.getScheduleServiceDate());
					schSer.setWeekNo(weeknumber);
//					schSer.setServiceProductList(serviceSchedulePopUp.getPopScheduleTable()
//							.getDataprovider().getList().get(i).getServiceProductList())
					 /***
					  * nidhi *:*:*
					  * 18-09-2018
					  * for map bill product
					  * if any update please update in else conditions also
					  */
					 if(LoginPresenter.billofMaterialActive){
						 schSer.setServiceProductList(schSer.getServiceProductList());
					 }
					 /**
					  * end
					  */
				}
				}
			}
		}
			return list;
		}
		
		public SalesLineItem getItemDetails(List<SalesLineItem> list,int productId,int productSrNo){
			for(SalesLineItem obj:list){
				if(obj.getPrduct().getCount()==productId&&obj.getProductSrNo()==productSrNo){
					return obj;
				}
			}
			return null;
		}
		
		public void sortSechduleServicesByProdSerNoAndSerSrNo(List<ServiceSchedule> list){
			Comparator<ServiceSchedule> prodSerNoComp = new Comparator<ServiceSchedule>() {  
				@Override  
				public int compare(ServiceSchedule o1, ServiceSchedule o2) {  
					if (o1.getSerSrNo() == o2.getSerSrNo()) {
						return 0;
					}
					if (o1.getSerSrNo() > o2.getSerSrNo()) {
						return 1;
					} else {
						return -1;
					}
				}  
			};
			Comparator<ServiceSchedule> serSerNoComp = new Comparator<ServiceSchedule>() {  
				@Override  
				public int compare(ServiceSchedule o1, ServiceSchedule o2) {  
					if (o1.getScheduleServiceNo() == o2.getScheduleServiceNo()) {
						return 0;
					}
					if (o1.getScheduleServiceNo() > o2.getScheduleServiceNo()) {
						return 1;
					} else {
						return -1;
					}
				}  
			};
			Collections.sort(list,serSerNoComp);
			Collections.sort(list,prodSerNoComp);
			
//			for(ServiceSchedule ss:list){
//				System.out.println("Product Sr No. - "+ss.getSerSrNo()+" Service Sr No. - "+ss.getScheduleServiceNo());
//			}
		}
		
		public boolean isAdvanceFilterSelected(){
			if(serviceSchedulePopUp.getLbServices().getSelectedIndex()!=0
					||serviceSchedulePopUp.getOlbCustomerBranch().getSelectedIndex()!=0
					||serviceSchedulePopUp.getOlbServicingBranch().getSelectedIndex()!=0){
				return true;
			}
			return false;
		}
		
		/**
		 * Date 04-08-2018 By Vijay Updated customer branch condition added in if condition
		 * Des :- Issue Reported IMP and Fumigation:- when we applyed adavanced filter and updated time then same customer branches applied to
		 * all services for product 
		 */
		
		private void updateServiceScheduleList(List<ServiceSchedule> serSchList,List<ServiceSchedule> globalScheduleServiceList) {
			
			for(int i=0;i<globalScheduleServiceList.size();i++){
				for(int j=0;j<serSchList.size();j++){
					if(globalScheduleServiceList.get(i).getScheduleProdId()==serSchList.get(j).getScheduleProdId()
							&&globalScheduleServiceList.get(i).getSerSrNo()==serSchList.get(j).getSerSrNo()
							&&globalScheduleServiceList.get(i).getScheduleServiceNo()==serSchList.get(j).getScheduleServiceNo()
							&& globalScheduleServiceList.get(i).getScheduleProBranch().equals(serSchList.get(j).getScheduleProBranch())){
						globalScheduleServiceList.set(i, serSchList.get(j));
					}
				}
			}
		}
		
	public void reactOnExcludeDay(){
			serviceSchedulePopUp.getP_customise().setValue(false);
			serviceSchedulePopUp.getP_default().setValue(false);
			serviceSchedulePopUp.getP_mondaytofriday().setSelectedIndex(0);

			serviceSchedulePopUp.getP_servicehours().setSelectedIndex(0);
			serviceSchedulePopUp.getP_servicemin().setSelectedIndex(0);
			serviceSchedulePopUp.getP_ampm().setSelectedIndex(0);

			serviceSchedulePopUp.getP_servicehours().setEnabled(true);
			serviceSchedulePopUp.getP_servicemin().setEnabled(true);
			serviceSchedulePopUp.getP_ampm().setEnabled(true);
			serviceSchedulePopUp.getP_customise().setEnabled(true);

			//04-04-2017 added by vijay 
			serviceSchedulePopUp.getP_serviceWeek().setSelectedIndex(0);
					
			int selectedExcludDay = serviceSchedulePopUp.getE_mondaytofriday().getSelectedIndex();
			
			/**
			 * @author Vijay Service Scheduling with new single common method with new round up logic for innovative
			 */
			if(LoginPresenter.serviceScheduleNewLogicFlag){
				 List<SalesLineItem> salesitem = new ArrayList<SalesLineItem>();
				 salesitem = form.getSaleslineitemtable().getDataprovider().getList();
				 List<ServiceSchedule> serschelist=form.getServiceSchedulelist(AppConstants.EXCLUDEDAY,salesitem,form.dbContractDate.getValue(),form.customerbranchlist,form.olbbBranch.getValue(),selectedExcludDay,0,0,0,serviceSchedulePopUp);
			
				 serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
				 form.globalScheduleServiceList=new ArrayList<ServiceSchedule>();
				 form.globalScheduleServiceList=serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
				 
				 return;
			}	
				
			List<SalesLineItem> salesitemlis = form.saleslineitemtable.getDataprovider().getList();
			ArrayList<ServiceSchedule> serschelist = new ArrayList<ServiceSchedule>();
			serschelist.clear();

			List<ServiceSchedule> popuptablelist = serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
			boolean unitFlag = false;
			for (int i = 0; i < salesitemlis.size(); i++) {


//				int qty = 0;
//				qty = (int) salesitemlis.get(i).getQty();
				
				/**
				 * Date : 29-03-2017 By ANil
				 */
				boolean branchSchFlag=false;
				int qty=0;
				
				/**
				 * Date 02-05-2018 
				 * Developer : Vijay
				 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
				 * so i have updated below code using hashmap. 
				 */
				
				
				/** Date 02-05-2018 By vijay for updated branch scheduling using hashmap **/
				ArrayList<BranchWiseScheduling> branchSchedulingList = salesitemlis.get(i).getCustomerBranchSchedulingInfo().get(salesitemlis.get(i).getProductSrNo());
				
			/**
			 * ends here	
			 */
				if(branchSchedulingList!=null
						&&branchSchedulingList.size()!=0){
					branchSchFlag=true;
					qty=branchSchedulingList.size();
				}else{
					qty=(int) salesitemlis.get(i).getQty();
				}
				
				/**
				 * End
				 */
				if(branchSchFlag){
					for (int k = 0; k < qty; k++) {
						
						if(branchSchedulingList.get(k).isCheck()==true){
					/*	long noServices = (long) (item.getNumberOfServices());
						int noOfdays = item.getDuration();
						double nodays =  noOfdays;
						double noofservices= item.getNumberOfServices();
						int inreval = nodays / noofservices;
						if(serviceSchedulePopUp.getP_interval().getValue() != null || serviceSchedulePopUp.getP_interval().getValue() != 0){
							inreval =  serviceSchedulePopUp.getP_interval().getValue().doubleValue();
						}
						int noOfdays = salesitemlis.get(i).getDuration();*/

							
//						long noServices = (long) (salesitemlis.get(i).getNumberOfServices());
//						int noOfdays = salesitemlis.get(i).getDuration();
//						double nodays =  noOfdays;
//						double noofservices= salesitemlis.get(i).getNumberOfServices();
//						int inreval = (int) (nodays / noofservices);
//						if(serviceSchedulePopUp.getP_interval().getValue() != null && serviceSchedulePopUp.getP_interval().getValue() != 0){
//							inreval =  serviceSchedulePopUp.getP_interval().getValue();
//						}
						
						/*** Date 28 sep 2017 added by vijay  for scheduling proper calculation with precision number
						 *  for interval  
						 */
						double days =  salesitemlis.get(i).getDuration();
						double noofservices= salesitemlis.get(i).getNumberOfServices();
						double interval =  days / noofservices;
						double temp = interval;
						double tempvalue=interval;
						/******************** ends here ********************/
						int noOfdays = 	(int) days;
						Date servicedate = new Date(salesitemlis.get(i).getStartDate().getTime());
						int noServices = (int) noofservices;
						Date productEndDate = new Date(servicedate.getTime());
						CalendarUtil.addDaysToDate(productEndDate, noOfdays);
						Date prodenddate = new Date(productEndDate.getTime());
						productEndDate = prodenddate;
						
						

						/**
						 * nidhi
						 *  ))..
						 *  for exclude day.
						 */
						int startday = servicedate.getDay();
						
						if(selectedExcludDay !=0  && startday == selectedExcludDay-1){
							CalendarUtil.addDaysToDate(servicedate, 1);
						}
						Date d = new Date(servicedate.getTime());
						int lastday = productEndDate.getDay();
						
						if(selectedExcludDay !=0  && lastday == selectedExcludDay-1){
							CalendarUtil.addDaysToDate(productEndDate, -1);
						}
						/**
						 * end
						 */
						Date tempdate = new Date();
						tempdate = servicedate;
						
						double originalInterval =  days / noofservices;


						for (int j = 0; j < noServices; j++) {
							/*if (j > 0) {
								CalendarUtil.addDaysToDate(d, inreval);
								tempdate = new Date(d.getTime());
							}*/
							
							
//							/*** added by vijay for interval calculation *************/
							int interval2 = (int) Math.round(interval);
							
							if (j >0) {
								CalendarUtil.addDaysToDate(d, interval2);
							}
							else{
								interval =(interval+temp)-tempvalue;
							    tempvalue = Math.round(interval);
								
							}
		
							/**
							 * nidhi
							 *  ))..
							 *  for exclude day.
							 */
							int serDay = d.getDay();
							
							/**
							 * @author Vijay Date :- 09-02-2023
							 * Des :- added for exclude holidays and weekly off days from calnedar
							 */
							if(serviceSchedulePopUp.getE_mondaytofriday().getValue(serviceSchedulePopUp.getE_mondaytofriday().getSelectedIndex())
									.equals(AppConstants.HOLIDAYS)){
								Console.log("Inside exclude holiday");
								Calendar calendar = serviceSchedulePopUp.getOlbcalendar().getSelectedItem();
								form.excludeDateFromInHolidayDateFromCalendar(d,calendar);
							}
							else{
								if(selectedExcludDay !=0  && serDay == selectedExcludDay-1){
								    
									CalendarUtil.addDaysToDate(d, 1);
									
								}
							}
							tempdate = new Date(d.getTime());
							ServiceSchedule ssch = new ServiceSchedule();
							Date Stardaate = null;
							
							ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
							Stardaate = salesitemlis.get(i).getStartDate();
							ssch.setScheduleStartDate(Stardaate);
							ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
							ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
							ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
							ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
							ssch.setScheduleServiceNo(j + 1);
							ssch.setScheduleProdStartDate(Stardaate);
							ssch.setScheduleProdEndDate(productEndDate);
							ssch.setServicingBranch(form.olbbBranch.getValue());
							ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
							String calculatedServiceTime = "";
							if (popuptablelist.size() != 0) {
								calculatedServiceTime = popuptablelist.get(j).getScheduleServiceTime();
							} else {
								calculatedServiceTime = "Flexible";
							}
		
							//vijay
							ssch.setServiceRemark(salesitemlis.get(i).getRemark());
							ssch.setScheduleServiceTime(calculatedServiceTime);
		
							
							ssch.setScheduleProBranch(branchSchedulingList.get(k).getBranchName());
							if (branchSchedulingList.get(k).getServicingBranch()!=null) {
								ssch.setServicingBranch(branchSchedulingList.get(k).getServicingBranch());
							} 
							
							
		
							
							/***
							  * nidhi *:*:*
							  * 18-09-2018
							  * for map bill product
							  * if any update please update in else conditions also
							  */
							 if(LoginPresenter.billofMaterialActive){
								 
								 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(salesitemlis.get(i).getPrduct());
								 if(billPrDt!=null){
									 
									 
									 UnitConversionUtility unitConver = new UnitConversionUtility();

									 
									 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
										 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
										 seList.add(ssch);
										 seList = unitConver.getServiceitemProList(seList,salesitemlis.get(i).getPrduct(),salesitemlis.get(i),billPrDt);
										 if(seList!=null && seList.size()>0)
											 ssch = seList.get(0);
										 
									 }else{
										 unitFlag = true;
//										 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
									 }
									 
									 
									 
									 
								 }
							 }
							 /**
							  * end
							  */
							 
							 /**
							  * @author Vijay Chougule Date :- 09-09-2020
							  * Des :- added Service duration 
							  */
							 if(branchSchedulingList.get(k).getServiceDuration()!=0){
								 ssch.setServiceDuration(branchSchedulingList.get(k).getServiceDuration());
							 }
							 /**
							  * ends here
							  */
							 
							 if (j == 0) {
									 if(serviceSchedulePopUp.getE_mondaytofriday().getValue(serviceSchedulePopUp.getE_mondaytofriday().getSelectedIndex())
												.equals(AppConstants.HOLIDAYS)){
											ssch.setScheduleServiceDate(tempdate);
											Console.log("tempdate===="+tempdate);
									 }
									 else{
											ssch.setScheduleServiceDate(servicedate);
											Console.log("servicedate"+servicedate);
									 }
									 Console.log("ssch.getScheduleServiceDate() "+ssch.getScheduleServiceDate());
//									ssch.setScheduleServiceDate(servicedate);
									ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
									int weeknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
									ssch.setWeekNo(weeknumber);
									
									serschelist.add(ssch);
								}
								if (j != 0) {
									if (productEndDate.after(d)) {
										ssch.setScheduleServiceDate(tempdate);
										ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
										int weeknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
										ssch.setWeekNo(weeknumber);
										serschelist.add(ssch);
									} else {
										ssch.setScheduleServiceDate(productEndDate);
										ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
										int weeknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
										ssch.setWeekNo(weeknumber);
										serschelist.add(ssch);
									}
								}
							/**
							 * ends here
							 */
								
								/************* added by vijay for scheduling interval including calculation using roundoff *********************/
								String strinterwal = interval+"";
								if(strinterwal.contains("-")){
									interval = Math.abs(interval);
								}
								
								if(originalInterval<1){
									interval =(((int)(interval))+temp)- tempvalue;
								}
								else{
									interval =(interval+temp)- tempvalue;
								}
							    tempvalue = Math.round(interval);
							    /**************** ends here **********************************/
	
						}
					}
					}
					
				}else{
					for (int k = 0; k < qty; k++) {
						long noServices = (long) (salesitemlis.get(i).getNumberOfServices());
						int noOfdays = salesitemlis.get(i).getDuration();
						double nodays =  noOfdays;
						double noofservices= salesitemlis.get(i).getNumberOfServices();
						int inreval = (int) (nodays / noofservices);
						if(serviceSchedulePopUp.getP_interval().getValue() != null || serviceSchedulePopUp.getP_interval().getValue() != 0){
							inreval =  serviceSchedulePopUp.getP_interval().getValue();
						}
//						int noOfdays = salesitemlis.get(i).getDuration();
						Date servicedate = new Date(salesitemlis.get(i).getStartDate().getTime());
						Date d = new Date(servicedate.getTime());
		
						Date productEndDate = new Date(servicedate.getTime());
						CalendarUtil.addDaysToDate(productEndDate, noOfdays);
						Date prodenddate = new Date(productEndDate.getTime());
						productEndDate = prodenddate;
						
						/**
						 * nidhi
						 *  ))..
						 *  for exclude day.
						 */
						int startday = servicedate.getDay();
						
						if(selectedExcludDay !=0  && startday == selectedExcludDay-1){
							CalendarUtil.addDaysToDate(servicedate, 1);
						}
						int lastday = productEndDate.getDay();
						
						if(selectedExcludDay !=0  && lastday == selectedExcludDay-1){
							CalendarUtil.addDaysToDate(productEndDate, -1);
						}
						/**
						 * end
						 */
						Date tempdate = new Date();
						tempdate = servicedate;
						for (int j = 0; j < noServices; j++) {
		
							
							
							if (j > 0) {
								CalendarUtil.addDaysToDate(d, inreval);
							}
		
							/**
							 * nidhi
							 *  ))..
							 *  for exclude day.
							 */
							int serDay = d.getDay();
							if(selectedExcludDay !=0  && serDay == selectedExcludDay-1){
								CalendarUtil.addDaysToDate(d, 1);
							
							}
							tempdate = new Date(d.getTime());
							/**
							 * end
							 */
							
							ServiceSchedule ssch = new ServiceSchedule();
							Date Stardaate = null;
							
							ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
							Stardaate = salesitemlis.get(i).getStartDate();
							ssch.setScheduleStartDate(Stardaate);
							ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
							ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
							ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
							ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
							ssch.setScheduleServiceNo(j + 1);
							ssch.setScheduleProdStartDate(Stardaate);
							ssch.setScheduleProdEndDate(prodenddate);
							ssch.setServicingBranch(form.olbbBranch.getValue());
							ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
							String calculatedServiceTime = "";
							if (popuptablelist.size() != 0) {
								calculatedServiceTime = popuptablelist.get(j).getScheduleServiceTime();
							} else {
								calculatedServiceTime = "Flexible";
							}
		
							//vijay
							ssch.setServiceRemark(salesitemlis.get(i).getRemark());
							
							ssch.setScheduleServiceTime(calculatedServiceTime);
		
							if (form.customerbranchlist.size() == qty) {
								ssch.setScheduleProBranch(form.customerbranchlist.get(k).getBusinessUnitName());
								/**
								 * rohan added this code for automatic servicing branch
								 * setting for NBHC
								 */
								if (form.customerbranchlist.get(k).getBranch() != null) {
									ssch.setServicingBranch(form.customerbranchlist.get(k).getBranch());
								} else {
									ssch.setServicingBranch(form.olbbBranch.getValue());
								}
								/**
								 * ends here
								 */
		
							} else if (form.customerbranchlist.size() == 0) {
								ssch.setScheduleProBranch("Service Address");
		
								/**
								 * rohan added this code for automatic servicing branch
								 * setting for NBHC
								 */
								ssch.setServicingBranch(form.olbbBranch.getValue());
								/**
								 * ends here
								 */
		
							} else {
								ssch.setScheduleProBranch(null);
							}
						
							
		
							if (j == 0) {
								ssch.setScheduleServiceDate(servicedate);
								ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
								serschelist.add(ssch);
							}
							if (j != 0) {
								if (productEndDate.after(d)) {
									ssch.setScheduleServiceDate(tempdate);
									ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
									serschelist.add(ssch);
								} else {
									ssch.setScheduleServiceDate(productEndDate);
									ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
									serschelist.add(ssch);
								}
							}
							
							/**
							 * Date 03-04-2017 added by vijay for week number
							 */
							int weeknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
							ssch.setWeekNo(weeknumber);
							/***
							  * nidhi *:*:*
							  * 18-09-2018
							  * for map bill product
							  * if any update please update in else conditions also
							  */
							 if(LoginPresenter.billofMaterialActive){
								 
								 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(salesitemlis.get(i).getPrduct());
								 if(billPrDt!=null){
									 
									 
									 UnitConversionUtility unitConver = new UnitConversionUtility();

									 
									 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
										 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
										 seList.add(ssch);
										 seList = unitConver.getServiceitemProList(seList,salesitemlis.get(i).getPrduct(),salesitemlis.get(i),billPrDt);
										 if(seList!=null && seList.size()>0)
											 ssch = seList.get(0);
										 
									 }else{
										 unitFlag = true;
//										 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
									 }
									 
									 
									 
									 
								 }
							 }
							 /**
							  * end
							  */
							/**
							 * ends here
							 */
						}
					}
			}

				serviceSchedulePopUp.getPopScheduleTable().getDataprovider().setList(serschelist);
				/**
				 * Date : 25-11-2017 BY ANIL
				 */
				form.globalScheduleServiceList=new ArrayList<ServiceSchedule>();
				form.globalScheduleServiceList=serviceSchedulePopUp.getPopScheduleTable().getDataprovider().getList();
				/**
				 * End
				 */
			}
		}
	
	
	
	

	/**
	 * Date 26-03-2019 by Vijay
	 * Des :- NBHC CCPM Contract discontinue with Approval process
	 */
	private void reactonContractDiscontinue() {

		form.showWaitSymbol();
		final Approvals approval=new Approvals();
		approval.setApproverName(contractDiscountinue.getOlbApprovarName().getValue());
		approval.setBranchname(model.getBranch());
		approval.setBusinessprocessId(model.getCount());
		approval.setRequestedBy(LoginPresenter.loggedInUser);
		approval.setPersonResponsible(model.getEmployee());
		
		approval.setApprovalLevel(1);
		approval.setStatus(Approvals.PENDING);
		approval.setBusinessprocesstype(AppConstants.CONTRACTDISCOUNTINUED);
		approval.setBpId(model.getCinfo().getCount()+"");
		approval.setBpName(model.getCinfo().getFullName());
		approval.setDocumentValidation(false);
		approval.setRemark(contractDiscountinue.getOlbCancellationRemark().getValue());
		
		genasync.save(approval, new AsyncCallback<ReturnFromServer>() {
			
			@Override
			public void onSuccess(ReturnFromServer result) {
				// TODO Auto-generated method stub
				form.showDialogMessage("Approval Request Sent!");
				form.hideWaitSymbol();
				
				form.getManageapproval().reactOnApprovalEmail(approval);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();

			}
		});
		
	}
	
	/**
	 * @author Vijay Chougule
	 * Des - NBHC CCPM Upload contract Service Brnaches Updation in
	 * Services
	 */
	private void updateServiceBranchesinServices() {
		
		AutoInvoiceServiceAsync autoserviceasync = GWT.create(AutoInvoiceService.class);
		form.showWaitSymbol();
		autoserviceasync.updateServiceBranchinServies(model.getCount(), model.getCompanyId(), new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				form.showDialogMessage(result);
				form.hideWaitSymbol();
			}
		});
	}
	
	

	/**
	 * @author Vijay Chougule.
	 * Date 22-10-2019
	 * Des :- Day wise scheduling with branch wise services and with product wise scheduling
	 */
	
	private ArrayList<ServiceSchedule> scheduleDayWiseData(ArrayList<ServiceSchedule> branchwiselist, 
			SalesLineItem item, int dayIndex, Date d, int weekIndex, Date temp, boolean sameMonthserviceflag, 
			boolean flag, double interval, double tempvalue, double tempinterval, Date productEndDate) {

		ArrayList<ServiceSchedule> updatedschedulelist = new ArrayList<ServiceSchedule>();
		for(ServiceSchedule schSer: branchwiselist){
			
			if(item.getPrduct().getCount()==schSer.getScheduleProdId()
					&&item.getProductSrNo()==schSer.getSerSrNo()){
			
			Date Stardaate = null;
			Stardaate = item.getStartDate();
			String dayOfWeek1 = format.format(d);
			int adate = Integer.parseInt(dayOfWeek1);
			int adday1 = dayIndex - 1;
			int adday = 0;
			if (adate <= adday1) {
				adday = (adday1 - adate);
			} else {
				adday = (7 - adate + adday1);
			}
			Date newDate = new Date(d.getTime());
			CalendarUtil.addDaysToDate(newDate, adday);

			Date day = new Date(newDate.getTime());
			
			if(weekIndex!=0){
				if(temp!=null){
					String currentServiceMonth =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( day ).split("-")[1];
					String lastserviceDateMonth = DateTimeFormat.getFormat( "d-M-yyyy" ).format( temp ).split("-")[1];
					if(currentServiceMonth.equals(lastserviceDateMonth)){
						sameMonthserviceflag =true;
					}else{
						sameMonthserviceflag= false;
					}
				}
				if(sameMonthserviceflag){
					CalendarUtil.addDaysToDate(day, 10);
				}
										
				CalendarUtil.setToFirstDayOfMonth(day);
				if(weekIndex==1){
					day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
					if(day.before(Stardaate)){
						if(schSer.getScheduleServiceNo() == 1){
							day =Stardaate;
						}else{
							flag = true;
						}
					}
					if(flag || schSer.getScheduleServiceNo() >1){
						CalendarUtil.setToFirstDayOfMonth(day);
						day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
					}
				}else if(weekIndex==2){
					
					CalendarUtil.addDaysToDate(day, 7);

					 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
					if(day.before(Stardaate)){
						if(schSer.getScheduleServiceNo()==1){
							day =Stardaate;
						}
						else{
							flag = true;
						}
					}
					if(flag || schSer.getScheduleServiceNo()>1){
						CalendarUtil.setToFirstDayOfMonth(day);
						CalendarUtil.addDaysToDate(day, 7);
						 day = AppUtility.getCalculatedDatewithWeek(day, adday1);;
					}
				}else if(weekIndex==3){
					CalendarUtil.addDaysToDate(day, 14);
					 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
					if(day.before(Stardaate)){
						if(schSer.getScheduleServiceNo()==1){
							day =Stardaate;
						}
						else{
							flag = true;
						}
					}
					if(flag || schSer.getScheduleServiceNo()>1){
						CalendarUtil.setToFirstDayOfMonth(day);
						CalendarUtil.addDaysToDate(day, 14);
						 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
					}
				}else if(weekIndex==4){
					CalendarUtil.addDaysToDate(day, 21);

					 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
					if(day.before(Stardaate)){
						if(schSer.getScheduleServiceNo()==1){
							day =Stardaate;
						}
						else{
							flag = true;
						}
					}
					if(flag || schSer.getScheduleServiceNo()>1){
						CalendarUtil.setToFirstDayOfMonth(day);
						CalendarUtil.addDaysToDate(day, 21);
						 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
					}
			     }
				
				 temp = day;
				
				CalendarUtil.addDaysToDate(d, (int)interval);
				int days =0;
				String serviceStartDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( Stardaate ).split("-")[0];
				if(serviceStartDate.equals("1")){
					String currentServiceDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( d ).split("-")[0];
					 days = AppUtility.checkDateMonthOFEndAndAddDays(currentServiceDate,d);
					CalendarUtil.addDaysToDate(d, days);

				}
			}
			if (productEndDate.before(day) == false) {
				schSer.setScheduleServiceDate(day);
			} else {
				schSer.setScheduleServiceDate(productEndDate);
			}
			int interval2 =(int) Math.round(interval);
			if(weekIndex==0){
				CalendarUtil.addDaysToDate(d, interval2);
			}
			Date tempdate = new Date(d.getTime());
			d = tempdate;
			schSer.setScheduleServiceDay(ContractForm.serviceDay(schSer.getScheduleServiceDate()));
			
			interval =(interval+tempinterval)-tempvalue;
		    tempvalue = Math.round(interval);
		   
			int weeknumber = AppUtility.getweeknumber(schSer.getScheduleServiceDate());
			schSer.setWeekNo(weeknumber);
			 /***
			  * nidhi *:*:*
			  * 18-09-2018
			  * for map bill product
			  * if any update please update in else conditions also
			  */
			 if(LoginPresenter.billofMaterialActive){
				 schSer.setServiceProductList(schSer.getServiceProductList());
			 }
			 /**
			  * end
			  */
			 
			 updatedschedulelist.add(schSer);
		}
		
	}
	
		return updatedschedulelist;
	}
	
	/**
	 * @author Vijay Date :- 12-11-2020
	 * Des :- As per Nitin Sir this logic to update old contract making available for Renewal
	 * this button only visible when process config is active
	 */
	private void reactonRenewable() {

		if(model.getStatus().equals(Contract.APPROVED)){
			Console.log("model.getgroup"+model.getGroup());
			if(!model.getGroup().trim().equalsIgnoreCase("One Time")){
				model.setCustomerInterestFlag(false);
				model.setNonRenewalRemark("");
				form.showWaitSymbol();
				genasync.save(model, new AsyncCallback<ReturnFromServer>() {
					
					@Override
					public void onSuccess(ReturnFromServer result) {
						// TODO Auto-generated method stub
						Console.log("On Success");
						form.setToViewState();
						form.hideWaitSymbol();
						form.showDialogMessage("Update Successfully");
						form.getTbdonotrenew().setValue("");
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						Console.log("On Failure");
						form.hideWaitSymbol();
					}
				});
			}
			else{
				form.showDialogMessage("Contract Group One Time Not Applicable for Renewal");
			}
		}
	}

	
	private void createDoNotRenewaEntryInContractRenewal() {
		conrenewalAsync.contractRenewalEntry(model, AppConstants.DONOTRENEW,"", new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(String arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onFailure(Throwable arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	
	private void setEmailPopUpData(Screen screenName) {
		form.showWaitSymbol();
		String customerEmail = "";

		String branchName = form.olbbBranch.getValue();
		String fromEmailId = AppUtility.getFromEmailAddress(branchName,model.getEmployee());
		customerEmail = AppUtility.getCustomerEmailid(model.getCinfo().getCount());
		
		String customerName = model.getCinfo().getFullName();
		if(!customerName.equals("") && !customerEmail.equals("")){
			label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getCinfo().getCount(),customerName,customerEmail,null);
			emailpopup.taToEmailId.setValue(customerEmail);
		}
		else{
	
			MyQuerry querry = AppUtility.getcustomerQuerry(model.getCinfo().getCount());
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					for(SuperModel smodel : result){
						Customer custEntity  = (Customer) smodel;
						System.out.println("customer Name = =="+custEntity.getFullname());
						System.out.println("Customer Email = == "+custEntity.getEmail());
						
						if(custEntity.getEmail()!=null){
							label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getCinfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),null);
							emailpopup.taToEmailId.setValue(custEntity.getEmail());
						}
						else{
							label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getCinfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),null);
						}
						break;
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
				}
			});
		}
		
//		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		Console.log("screenName "+screenName);
		AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,AppConstants.SERVICE,screenName);//LoginPresenter.currentModule.trim()
		emailpopup.taFromEmailId.setValue(fromEmailId);
		emailpopup.smodel = model;
		if(screenName==Screen.CONTRACTRENEWAL){
			emailpopup.buttonLabel="Email Reminder";
		}
		form.hideWaitSymbol();
		
	}
	private void reactonStationTechnician() {
		if(model.getStatus().equals(Contract.APPROVED)){
			System.out.println("value =="+form.chkStationedService.getValue());
			if(form.chkStationedService.getValue()==false){
				boolean conf = Window.confirm("Would you like to convert this into NON stationed technician site contract?");
				if (conf) {
					commonservice.updateStationedContractIntoNormalContract(model.getCount(), model.getCompanyId(), new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							form.showDialogMessage(result);
						}
					});
				
				}
				else{
					form.chkStationedService.setValue(true);
				}
			}
			else{
				System.out.println("else block");
				boolean conf = Window.confirm("Would you like to convert this into stationed technician site contract?");
				if (conf) {
					commonservice.updateNormalContractIntoTechnicianContract(model.getCount(), model.getCompanyId(), new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							form.showDialogMessage(result);

						}
					});
				}
				else{
					form.chkStationedService.setValue(false);
				}
			}
		}
	
	}
	
	@Override
	public void reactOnSMS() {
		smspopup.showPopUp();
		loadSMSPopUpData();
	}

	private void loadSMSPopUpData() {
		form.showWaitSymbol();
		String customerCellNo = model.getCinfo().getCellNumber()+"";

		String customerName = model.getCinfo().getFullName();
		if(!customerName.equals("") && !customerCellNo.equals("")){
			AppUtility.loadContactPersonlist(smspopup.olbContactlistEmailId,"Customer",model.getCinfo().getCount(),customerName,customerCellNo,null,true);
		}
		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		Console.log("screenName "+screenName);
		AppUtility.makeLiveSmsTemplateConfig(smspopup.oblsmsTemplate,AppConstants.SERVICE,screenName,true,AppConstants.SMS);
		smspopup.getRbSMS().setValue(true);
		smspopup.smodel = model;
		form.hideWaitSymbol();
	}
	
	private void reactOnUpdateServiceValue() {
		
		if(model.isContractRate()){
			form.showDialogMessage("Can not update service value for rate contract!");
		}
		else{
			form.showWaitSymbol();
			commonservice.updateServiceValue(model.getCount(), model.getCompanyId(), new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(String result) {
					form.hideWaitSymbol();
					form.showDialogMessage(result);
				}
			});
		}
	}
	
	private void reactOnChangeTechnicianName() {
		technicianPopup.showPopUp();
	}
	
	private void updatTechnicianName() {
		if(technicianPopup.getOlbEmployee().getSelectedIndex()!=0){
			final String newTechnicianName = technicianPopup.getOlbEmployee().getValue(technicianPopup.getOlbEmployee().getSelectedIndex());
			model.setTechnicianName(newTechnicianName);
			form.showWaitSymbol();
			genasync.save(model, new AsyncCallback<ReturnFromServer>() {
				
				@Override
				public void onSuccess(ReturnFromServer result) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
					form.showDialogMessage("Technician name updated successfully");
					form.olbTechnicainName.setValue(newTechnicianName);
					technicianPopup.hidePopUp();
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
				}
			});
		}
		else{
			form.showDialogMessage("Please select technician name");
		}
		
	}
	
	
	private void reactOnContractReset() {

		form.showWaitSymbol();
		commonservice.getServiceInvoiceDocument(model.getCount(), model.getCompanyId(), new AsyncCallback<ArrayList<CancelContract>>() {

			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<CancelContract> result) {
				
				Console.log("result size"+result.size());

				if(result.size()>0){
					contractResetPopup.showPopUp();

					if(result.contains(AppConstants.SERVICE) && result.contains(AppConstants.Invoice)){
						contractResetPopup.setServiceMessage(true);
						contractResetPopup.setInvocieMessage(true);
//						contractResetPopup.lblInvoicemsg.setText("There are some approved invoices found! please select checkbox to cancel or remain same");
//						contractResetPopup.lblservicemsg.setText("There are some completed service found! please select option to cancel or delete services");
					}
					else if(result.contains(AppConstants.SERVICE)){
						contractResetPopup.setServiceMessage(true);
						contractResetPopup.setInvocieMessage(false);
//						contractResetPopup.lblservicemsg.setText("There are some completed service found! please select option to cancel or delete services");

					}
					else if(result.contains(AppConstants.Invoice)){
						contractResetPopup.setInvocieMessage(true);
						contractResetPopup.setServiceMessage(false);
//						contractResetPopup.lblInvoicemsg.setText("There are some approved invoices found! please select checkbox to cancel or remain same");
					}
					else{
						contractResetPopup.setInvocieMessage(false);
						contractResetPopup.setServiceMessage(false);
					}
					contractResetPopup.getCbcancel().setValue(true);
				}
				else{
					panel = new PopupPanel(true);
					panel.add(contractResetconditionPopup);
					panel.setGlassEnabled(true);
					panel.show();
					panel.center();
				}
				form.hideWaitSymbol();

			}
		});
	}
	
	private void reactonResetContract() {
		
		form.showWaitSymbol();
		model.setStatus(Contract.CREATED);
		genasync.save(model, new AsyncCallback<ReturnFromServer>() {
			
			@Override
			public void onSuccess(ReturnFromServer result) {
				// TODO Auto-generated method stub
				form.showDialogMessage("Contract reset successfully");
				form.hideWaitSymbol();
				form.tbQuotationStatus.setValue(Contract.CREATED);
				form.setToViewState();
				if(panel!=null)
				panel.hide();
				
				try {
					contractResetPopup.hidePopUp();
				} catch (Exception e) {
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
				if(panel!=null)
				panel.hide();
				
				try {
					contractResetPopup.hidePopUp();
				} catch (Exception e) {
				}
			}
		});
	}

	private void reactonContractResetPopup() {

		if(contractResetPopup.getRbcancel().getValue()==false && contractResetPopup.getRbedelete().getValue() == false){
			form.showDialogMessage("please select either cancel or delete service");
		}
		else{
			form.showWaitSymbol();
			commonservice.updateContractDocuments(model.getCount(), model.getCompanyId(), 
					contractResetPopup.getRbcancel().getValue(), contractResetPopup.getRbedelete().getValue(),
					contractResetPopup.getCbcancel().getValue(), false, new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							form.hideWaitSymbol();
						}

						@Override
						public void onSuccess(String result) {
							form.hideWaitSymbol();
							if(result!=null && result.equalsIgnoreCase("success")){
								reactonResetContract();
							}
						}
					});
		}
	}
	
	private void reactOnCopy(){
		try {
			Console.log("get id : " +model.getCount());
			final Contract object=new Contract();
			object.setCount(0);
			object.setContractCount(0);
			object.setQuotationCount(0);
			object.setQuotationDate(null);
			object.setApprovalDate(null);
			object.setCreationDate(null);
			object.setValidUntill(null);
			object.setStatus(Quotation.CREATED);
			object.setId(null);
			
			//Header
			object.setCinfo(model.getCinfo());
			object.setBranch(model.getBranch());
			object.setContractDate(new Date());//29-09-2022
			if(model.getEmployee()!=null)
				object.setEmployee(model.getEmployee());
			if(model.getNumberRange()!=null)
				object.setNumberRange(model.getNumberRange());
			object.setApproverName(model.getApproverName());
			if(model.getFollowUpDate()!=null)
				object.setFollowUpDate(model.getFollowUpDate());
			object.setStationedTechnician(model.isStationedTechnician());
			object.setContractRate(model.isContractRate());
			object.setServiceWiseBilling(model.isServiceWiseBilling());
			object.setBranchWiseBilling(model.isBranchWiseBilling());
			object.setConsolidatePrice(model.isConsolidatePrice());
			
			//classification
			if(model.getCategory()!=null)
				object.setCategory(model.getCategory());
			if(model.getGroup()!=null)
				object.setGroup(model.getGroup());
			if(model.getType()!=null)
				object.setType(model.getType());
			if(model.getSegment()!=null)
				object.setSegment(model.getSegment());
			
			//reference
			object.setStartDate(model.getStartDate());
			object.setEndDate(model.getEndDate());
			
			//payment terms
			object.setPaymentTermsList(model.getPaymentTermsList());
			if(model.getPaymentMethod()!=null)
				object.setPaymentMethod(model.getPaymentMethod());
			if(model.getPaymentMode()!=null)
				object.setPaymentMode(model.getPaymentMode());
			object.setCreditPeriod(model.getCreditPeriod());
			
			//Product table
			if(model.getPremisesDesc()!=null)
				object.setPremisesDesc(model.getPremisesDesc());
			if(model.getTechnicianName()!=null)
				object.setTechnicianName(model.getTechnicianName());
			object.setServiceScheduleList(model.getServiceScheduleList());
			List<SalesLineItem> items=model.getItems(); //29-09-2022
			for(SalesLineItem item:items) {
				item.setStartDate(new Date());
				int duration=item.getDuration()-1;
				Date enddate=new Date();
				CalendarUtil.addDaysToDate(enddate, duration);
				item.setEndDate(enddate);
			}
			object.setItems(items);
			
			
			
			object.setDiscountAmt(model.getDiscountAmt());
			object.setTotalAmount(model.getTotalAmount());
			object.setNetpayable(model.getNetpayable());
			object.setFinalTotalAmt(model.getFinalTotalAmt());
			object.setGrandTotalAmount(model.getGrandTotalAmount());
			object.setGrandTotal(model.getGrandTotal());
			object.setProductTaxes(model.getProductTaxes());	
			object.setProductCharges(model.getProductCharges());
			
			System.out.println("get id after : " +model.getCount()+ " ID "+model.getId() + " get value model  : "+ model.getCount() +" model Id ;;;; -- "+model.getId());

			AppMemory.getAppMemory().currentState=ScreeenState.VIEW;

//			form.setToViewState(); 
//			Console.log("After form.setToNewState");
			Timer timer = new Timer() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					
					
					form.updateView(object);
					Console.log("After form.updateView");
					Timer timer = new Timer() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							form.setToEditState();
						}
						
					};
					timer.schedule(1000);
					
					
								
					form.getTbQuotatinId().setValue("");
					form.getTbContractId().setValue("");
					form.getTbQuotatinId().setValue("");
				}
			};
			timer.schedule(2000);
			
//			form.tbQuotationStatus.setValue(Quotation.CREATED);
//			
		} 
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	private void deleteClientsLicense() {


		String url=Window.Location.getHref();
		Console.log("URL-------- "+url);
		String[] spliturl=url.split("\\.");
		String companyname=spliturl[0];
		Console.log("COMPANY :: "+companyname);
		companyname=companyname.replace("http://","");
		Console.log("COMPANY1 :: "+companyname);
		IntegrateSyncServiceAsync intAsync=GWT.create(IntegrateSyncService.class);
		String actionTask="";
		if(model.getStatus().equals(Contract.APPROVED)){
			actionTask=AppConstants.DELETECLIENTLICENSE;
		}else{
			form.showDialogMessage("Not a valid call.");
			return;
		}
		
		form.showWaitSymbol();
		intAsync.callLicenseUpdateInteface(model, actionTask, new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(String result) {
				form.hideWaitSymbol();
				form.showDialogMessage(result);
			}
		});
	
	}
private void reactonViewComplaint() {
		



		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("contrtactId");
		filter.setIntValue(model.getCount());
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new Complain());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No Complaint document found.");
					return;
				}
					final Complain complain=(Complain) result.get(0);
					final ComplainForm form=ComplainPresenter.initalize();
					Timer timer=new Timer() {
						@Override
						public void run() {
							form.updateView(complain);
							form.setToViewState();
							
						}
					};
					timer.schedule(1000);
				
			}
		});
		

		
	}
	
	public void runRegularDownloadProgram(){

		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ContractReport") || AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ContractDownloadNew")){
			
			/**
			 * Date 04-01-2017 By Vijay
			 * here i am adding all serachpopup filter into stringbuilder with querryString and value to CSVServlet
			 * for load filter specific data and download it. Using String seperator " ~ "
			 * old code added in else block
			 */
			
			StringBuilder filterValue = new StringBuilder();
	 		StringBuilder filterQuerry = new StringBuilder();
	 		
	 		ContractPresenterSearchProxy searchpopup =  (ContractPresenterSearchProxy) form.getSearchpopupscreen();
	 		
	 		
	 		if(searchpopup.dateComparator.getfromDateValue()!=null){
	 			filterQuerry.append("creationDate >="+"~");
	 			filterValue.append(AppUtility.parseDate(searchpopup.dateComparator.getFromDate().getValue())+"~");
	 		}
	 		if(searchpopup.dateComparator.gettoDateValue()!=null){
	 			filterQuerry.append("creationDate <="+"~");
	 			filterValue.append(AppUtility.parseDate(searchpopup.dateComparator.getToDate().getValue())+"~");
	 		}
	 		if(searchpopup.olbContractGroup.getSelectedIndex()!=0){
	 			filterQuerry.append("group"+"~");
	 			filterValue.append(searchpopup.olbContractGroup.getValue().trim()+"~");
	 		}
	 		if(searchpopup.olbContractCategory.getSelectedIndex()!=0){
	 			filterQuerry.append("category"+"~");
	 			filterValue.append(searchpopup.olbContractCategory.getValue().trim()+"~");
	 		}
	 		if(searchpopup.olbContractType.getSelectedIndex()!=0){
	 			filterQuerry.append("type"+"~");
	 			filterValue.append(searchpopup.olbContractType.getValue()+"~");
	 		}
	 		if(searchpopup.olbContractStatus.getSelectedIndex()!=0){
	 			filterQuerry.append("status"+"~");
	 			filterValue.append(searchpopup.olbContractStatus.getValue(searchpopup.olbContractStatus.getSelectedIndex()).trim()+"~");
	 		}
	 		if(searchpopup.olbcNumberRange.getSelectedIndex()!=0){
	 			filterQuerry.append("numberRange"+"~");
	 			filterValue.append(searchpopup.olbcNumberRange.getValue().trim()+"~");
	 		}
	 		if(searchpopup.olbBranch.getSelectedIndex()!=0){
	 			filterQuerry.append("branch"+"~");
	 			filterValue.append(searchpopup.olbBranch.getValue().trim()+"~");
	 		}
	 		if(searchpopup.tbQuotationId.getValue()!=null){
	 			filterQuerry.append("quotationCount"+"~");
	 			filterValue.append(searchpopup.tbQuotationId.getValue()+"~");
	 		}
	 		if(searchpopup.tbLeadId.getValue()!=null){
	 			filterQuerry.append("leadCount"+"~");
	 			filterValue.append(searchpopup.tbLeadId.getValue()+"~");
	 		}
	 		if(searchpopup.tbContractId.getValue()!=null){
	 			filterQuerry.append("count"+"~");
	 			filterValue.append(searchpopup.tbContractId.getValue()+"~");
	 		}
	 		if(searchpopup.olbEmployee.getSelectedIndex()!=0){
	 			filterQuerry.append("employee"+"~");
	 			filterValue.append(searchpopup.olbEmployee.getValue().trim()+"~");
	 		}
	 		if(!searchpopup.tbRefNumber.getValue().equals("")){
	 			filterQuerry.append("refNo"+"~");
	 			filterValue.append(searchpopup.tbRefNumber.getValue()+"~");
	 		}
	 		if(!searchpopup.tbRefNum2.getValue().equals("")){
	 			filterQuerry.append("refNo2"+"~");
	 			filterValue.append(searchpopup.tbRefNum2.getValue()+"~");
	 		}
	 		if(searchpopup.olbCreatedBy.getSelectedIndex()!=0){
	 			filterQuerry.append("createdBy"+"~");
	 			filterValue.append(searchpopup.olbCreatedBy.getValue()+"~");
	 		}
	 		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","OnlyForNBHC")){
	 			if(searchpopup.olbCustomerCategory.getSelectedIndex() != 0){
	 				filterQuerry.append("segment"+"~");
	 				filterValue.append(searchpopup.olbContractCategory.getValue()+"~");
	 			}
	 		}else{
	 			if(!searchpopup.tbSegment.getValue().equals("")){
	 				filterQuerry.append("segment"+"~");
	 				filterValue.append(searchpopup.tbSegment.getValue().trim()+"~");
	 			}
	 		}
	 		if(!searchpopup.tbWarehouse.getValue().equals("")){
	 			filterQuerry.append("stackDetailsList.wareHouse"+"~");
	 			filterValue.append(searchpopup.tbWarehouse.getValue().trim().toUpperCase()+"~");
	 		}
	 		if(!searchpopup.tbStorageLocation.getValue().equals("")){
	 			filterQuerry.append("stackDetailsList.storageLocation"+"~");
	 			filterValue.append(searchpopup.tbStorageLocation.getValue().trim().toUpperCase()+"~");
	 		}
	 		if(!searchpopup.tbStackNo.getValue().equals("")){
	 			filterQuerry.append("stackDetailsList.stackNo"+"~");
	 			filterValue.append(searchpopup.tbStackNo.getValue().trim().toUpperCase()+"~");
	 		}
	 		if(searchpopup.tbTicketNumber.getValue()!=null){
	 			filterQuerry.append("ticketNumber"+"~");
	 			filterValue.append(searchpopup.tbTicketNumber.getValue()+"~");
	 		}
	 		if(searchpopup.personInfo.getIdValue()!=-1){
	 			filterQuerry.append("cinfo.count"+"~");
	 			filterValue.append(searchpopup.personInfo.getIdValue()+"~");
	 		}
	 		if(!searchpopup.personInfo.getFullNameValue().equals("")){
	 			filterQuerry.append("cinfo.fullName"+"~");
	 			filterValue.append(searchpopup.personInfo.getFullNameValue()+"~");
	 		}
	 		if(searchpopup.personInfo.getCellValue()!=-1l){
	 			filterQuerry.append("cinfo.cellNumber"+"~");
	 			filterValue.append(searchpopup.personInfo.getCellValue()+"~");
	 		}
	 		if(searchpopup.prodInfoComposite.getValue()!=null){
	 			filterQuerry.append("items.serviceProduct.count"+"~");
	 			filterValue.append(searchpopup.prodInfoComposite.getIdValue()+"~");
	 		}
	 		if(searchpopup.startDateComparator.getfromDateValue()!=null){
	 			filterQuerry.append("startDate >="+"~");
	 			filterValue.append(AppUtility.parseDate(searchpopup.startDateComparator.getfromDateValue())+"~");
	 		}
	 		if(searchpopup.startDateComparator.gettoDateValue()!=null){
	 			filterQuerry.append("startDate <="+"~");
	 			filterValue.append(AppUtility.parseDate(searchpopup.startDateComparator.gettoDateValue())+"~");
	 		}
	 		
	 		if(searchpopup.endDateComparator.getfromDateValue()!=null){
	 			filterQuerry.append("endDate >="+"~");
	 			filterValue.append(AppUtility.parseDate(searchpopup.endDateComparator.getfromDateValue())+"~");
	 		}
	 		if(searchpopup.endDateComparator.gettoDateValue()!=null){
	 			filterQuerry.append("endDate <="+"~");
	 			filterValue.append(AppUtility.parseDate(searchpopup.endDateComparator.gettoDateValue())+"~");
	 		}
	 		/**** Date 27-03-2019 by Vijay for NBHC CCPM non renewal data download ****/
	 		if(searchpopup.olbNonRenewal.getSelectedIndex()!=0){
	 			filterQuerry.append("customerInterestFlag"+"~");
	 			if(searchpopup.olbNonRenewal.getValue(searchpopup.olbNonRenewal.getSelectedIndex()).trim().equalsIgnoreCase("Yes")){
		 			filterValue.append("True"+"~");
	 			}
	 			else{
	 				filterValue.append("False"+"~");
	 			}
	 		}
	 		/*** Date 09-04-2019 by Vijay for NBHC CCPM Cancelled Date filter****/
	 		if(searchpopup.cancellationDateComparator.getfromDateValue()!=null){
	 			filterQuerry.append("cancellationDate >="+"~");
	 			filterValue.append(AppUtility.parseDate(searchpopup.cancellationDateComparator.getfromDateValue())+"~");
	 		}
	 		if(searchpopup.cancellationDateComparator.gettoDateValue()!=null){
	 			filterQuerry.append("cancellationDate <="+"~");
	 			filterValue.append(AppUtility.parseDate(searchpopup.cancellationDateComparator.gettoDateValue())+"~");
	 		}
	 	
	 		//Ashwini Patil Date:25-07-2024 rex reported that contractDate filter was not working
	 		if(searchpopup.contractDateComparator.getfromDateValue()!=null){
	 			filterQuerry.append("contractDate >="+"~");
	 			filterValue.append(AppUtility.parseDate(searchpopup.contractDateComparator.getfromDateValue())+"~");
	 		}
	 		if(searchpopup.contractDateComparator.gettoDateValue()!=null){
	 			filterQuerry.append("contractDate <="+"~");
	 			filterValue.append(AppUtility.parseDate(searchpopup.contractDateComparator.gettoDateValue())+"~");
	 		}
	 		
	 		filterQuerry.append("companyId");
	 		filterValue.append(model.getCompanyId());
	 		
	 		String querry = filterQuerry.toString();
	 		String value = filterValue.toString();
	 		
	 		
	 		String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
	 		
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ContractReport")){
				
				/**
				 * Date : 24-10-2017 BY ANIL
				 * New Contract Report format for NBHC
				 * Process Type-ContractReport
				 */
					final String url = gwt + "csvservlet" + "?type=" + 127+ "&" +"querry="+querry+ "&" +"value="+value;
					Window.open(url, "test", "enabled");
			}
			
			/**
			 * Date 11-01-2018 added by vijay for download Common contract data with new logic
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ContractDownloadNew")){
			
				final String url = gwt + "csvservlet" + "?type=" + 134+ "&" +"querry="+querry+ "&" +"value="+value;
				Window.open(url, "test", "enabled");
			}
			
			
		}else{
			
			
			final ArrayList<Contract> contractArray = new ArrayList<Contract>();
			List<Contract> list = (List<Contract>) form.getSearchpopupscreen()
					.getSupertable().getDataprovider().getList();

			contractArray.addAll(list);

			csvservice.setcontractlist(contractArray, new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed" + caught);

				}

				@Override
				public void onSuccess(Void result) {
					String gwt = com.google.gwt.core.client.GWT.getModuleBaseURL();
					/**
					 * Date : 24-10-2017 BY ANIL
					 * New Contract Report format for NBHC
					 * Process Type-ContractReport
					 */
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ContractReport")){
						final String url = gwt + "csvservlet" + "?type=" + 127;
						Window.open(url, "test", "enabled");
					}else{
						final String url = gwt + "csvservlet" + "?type=" + 6;
						Window.open(url, "test", "enabled");
						
////						final String url=gwt + "CreateXLXSServlet"+"?type="+12;
////						Window.open(url, "test", "enabled");
//						
//						
//						 int columnCount = 55; //Ashwini Patil changed 54 to 55
//						 boolean enableCustomerBranchFlag = false;
//						 if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableCustomerBranchDownload")){
//							 enableCustomerBranchFlag = true;
//							 columnCount++;
//						 }
//
//						/**
//						 * nidhi
//						 * 9-08-2018
//						 * for map serial no
//						 */
//						 boolean showHideSerialNo = false;
//						 if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("HVASC", "HideOrShowModelNoAndSerialNoDetails")){
//							 columnCount+=2;
//							 showHideSerialNo = true;
//						 }
//
//							
//					     ArrayList<String> contractlist  = new ArrayList<String>();
//					     contractlist.add("ID");
//					     contractlist.add("PREVIOUS ORDER ID");
//					     contractlist.add("QUOTATION ID");
//					     contractlist.add("LEAD ID");
//					     contractlist.add("REF NUMBER1");
//					     contractlist.add("REF DATE");
//					     contractlist.add("REF NUMBER2");
//					     contractlist.add("CREATED BY");
//					     contractlist.add("CUSTOMER ID");
//					     contractlist.add("CUSTOMER NAME");
//							
//						 if(enableCustomerBranchFlag){
//						     contractlist.add("CUSTOMER BRANCH");
//						 }
//					     contractlist.add("CUSTOMER E-MAIL ID");
//					     contractlist.add("CUSTOMER CELL");
//					     contractlist.add("CUSTOMER LOCALITY");
//					     contractlist.add("POC NAME");
//					     contractlist.add("CONTRACT START DATE");
//					     contractlist.add("CONTRACT END DATE");
//					     contractlist.add("CONTRACT DATE");
//					     contractlist.add("CREATION DATE");
//					     contractlist.add("CREDIT DAYS");
//					     contractlist.add("CONTRACT GROUP");
//					     contractlist.add("CONTRACT CATEGORY");
//					     contractlist.add("CONTRACT TYPE");
//					     contractlist.add("BRANCH");
//					     contractlist.add("SALES PERSON");
//					     contractlist.add("APPROVER NAME");
//						/**
//						 * @author Anil
//						 * @since 02-04-2020
//						 * Turn around time for premimum tech
//						 */
//					     contractlist.add("TAT/SLA(In hours)");
//					     contractlist.add("PRODUCT ID");
//					     contractlist.add("PRODUCT CODE");
//					     contractlist.add("PRODUCT NAME");
//					     contractlist.add("PRODUCT QUANTITY");//Ashwini Patil Date:8-03-2023
//					     contractlist.add("SERVICE FREQUENCY");
//					     //rohan added this 4fields for NBHC on 2/1/2017
//					     contractlist.add("PRODUCT PRICE");
//					     contractlist.add("TOTAL GROSS VALUE");
//					     contractlist.add("TAX 1");
//					     contractlist.add("TAX %");
//					     contractlist.add("TAX AMOUNT");
//					     contractlist.add("TAX 2");
//					     contractlist.add("TAX %");
//					     contractlist.add("TAX AMOUNT");
//					     contractlist.add("TOTAL AMOUNT");
//					     contractlist.add("DISCOUNT AMOUNT");
//					     contractlist.add("AFTER DISCOUNT TOTAL AMOUNT");
//					     contractlist.add("AFTER TAXES TOTAL AMOUNT");
//					     contractlist.add("AFTER OTHER CHARGES TOTAL AMOUNT");
//					     contractlist.add("ROUND OFF");
//					     contractlist.add("NET PAYABLE");
//					     contractlist.add("PAYMENT METHOD");
//					     contractlist.add("STATUS");
//					     contractlist.add("STACK DETAILS");
//					     contractlist.add("SEGMENT");
//					     contractlist.add("CONTRACT PERIOD");
//					     contractlist.add("DESCRIPTION");
//
//						if(showHideSerialNo){
//						     contractlist.add("PRODUCT MODEL NO.");
//						     contractlist.add("PRODUCT SERIAL NO.");
//						}
//						/** date 04-06-2019 added by komal to show renewed flag **/
//				     	contractlist.add("RENEW STATUS");
//
//						/**
//						 * Date 19-03-2019 By Vijay
//						 * Des :- For NBHC CCPM Non Renewal Remark
//						 */
//					     contractlist.add("NON RENEWAL REMARK");
//
//						/**
//						 * @author Anil
//						 * @since 29-06-2020
//						 */
//					     contractlist.add("ACCOUNT MANAGER");
//
//						for(Contract q:contractArray)
//						{
//							for(int g=0;g<q.getItems().size();g++)
//							{
//							     contractlist.add(q.getCount()+"");
//
//							     if(q.getRefContractCount()==-1)
//								     contractlist.add("N.A");
//								else
//								     contractlist.add(q.getRefContractCount()+"");
//								
//								if(q.getQuotationCount()==-1)
//									contractlist.add("N.A");
//								else
//								     contractlist.add(q.getQuotationCount()+"");
//
//								if(q.getLeadCount()==-1)
//									contractlist.add("N.A");
//								else
//									contractlist.add(q.getLeadCount()+"");
//								
//								
//								if(q.getRefNo()!=null&&!q.getRefNo().equals("")){
//									contractlist.add(q.getRefNo());
//								}else{
//									contractlist.add("N.A");
//								}
//								
//								if(q.getRefDate()!=null&&!q.getRefDate().equals("")){
//									contractlist.add(AppUtility.parseDate(q.getRefDate(),"dd-MMM-yyyy"));
//								}else{
//									contractlist.add("N.A");
//								}
//								
//								if(q.getRefNo2()!=null&&!q.getRefNo2().equals("")){
//									contractlist.add(q.getRefNo2());
//								}else{
//									contractlist.add("N.A");
//								}
//								
//								if(q.getCreatedBy()!=null&&!q.getCreatedBy().equals("")){
//									contractlist.add(q.getCreatedBy());
//								}else{
//									contractlist.add("N.A");
//								}
//								contractlist.add(q.getCustomerId()+"");
//
//								if(q.getCustomerFullName()!=null&&!q.getCustomerFullName().equals("")){
//									contractlist.add(q.getCustomerFullName());
//								}else{
//									contractlist.add("N.A");
//								}
//
//								/**@author Abhinav bihade
//								 * @since 22/01/2020
//								 * As per Vaishnavi Pawar's Reqirement for ISPC - Got a call from Roshan stating that,
//								 * he needs customer branch column in quotation and contract download
//								 */
//								
//								if(enableCustomerBranchFlag){
//									HashSet<String> branchHs=new HashSet<String>();
//										if(q.getItems().get(g).getCustomerBranchSchedulingInfo()!=null){
//											ArrayList<BranchWiseScheduling> branchSchedulingList = q.getItems().get(g).getCustomerBranchSchedulingInfo().get(q.getItems().get(g).getProductSrNo());
//											for(BranchWiseScheduling obj:branchSchedulingList){
//												if(obj.isCheck()==true){
//													branchHs.add(obj.getBranchName());
//												}
//											}
//					
//										}
//									String branchNames = "";
//									for(String branch:branchHs){
//										if(!branchNames.equals("")) {
//											branchNames = branchNames + "/"+branch;
//										}
//										else{
//											branchNames = branchNames + branch;
//										}
//									}
//								
//								contractlist.add(branchNames);
//
//								}				
//								
//								
//								if(q.getCinfo().getEmail()!=null&&!q.getCinfo().getEmail().equals("")){
//									contractlist.add(q.getCinfo().getEmail());
//								}else{
//									contractlist.add("");
//								}
//								
//								if(q.getCustomerCellNumber()!=null){
//									contractlist.add(q.getCustomerCellNumber()+"");
//								}else{
//									contractlist.add("");
//								}
//								
//								/**Date 10-4-2019 by Amol added Customer Locality column (as we are store the customer billing address in Contract entity hence we have
//								 *  taken the locality from Contract entity)**/
//								if(q.getNewcustomerAddress()!=null&&q.getNewcustomerAddress().getLocality()!=null){
//									contractlist.add(q.getNewcustomerAddress().getLocality());
//
//								}else{
//									contractlist.add("");
//								}
//								
//								if(q.getCinfo().getPocName()!=null&&!q.getCinfo().getPocName().equals("")){
//									contractlist.add(q.getCinfo().getPocName());
//								}else{
//									contractlist.add("");
//								}
//								
//								if(q.getStartDate()!=null){
//									contractlist.add(AppUtility.parseDate(q.getStartDate(),"dd-MMM-yyyy"));
//								}else{
//									contractlist.add("");
//								}
//								
//								if(q.getEndDate()!=null){
//									contractlist.add(AppUtility.parseDate(q.getEndDate(),"dd-MMM-yyyy"));
//								}else{
//									contractlist.add("");
//								}
//								
//								if(q.getContractDate()!=null){
//									contractlist.add(AppUtility.parseDate(q.getContractDate(),"dd-MMM-yyyy"));
//								}
//								else{
//									contractlist.add("N.A");
//								}
//
//								if(q.getCreationDate()!=null){
//									contractlist.add(AppUtility.parseDate(q.getCreationDate(),"dd-MMM-yyyy"));
//								}else{
//									contractlist.add("");
//								}
//								
//								if(q.getCreditPeriod()!=0){
//									contractlist.add(q.getCreditPeriod()+"");
//								}
//								else{
//									contractlist.add("");
//								}
//								
//								if(q.getGroup()!=null&&!q.getGroup().equals("")){
//									contractlist.add(q.getGroup());
//								}else{
//									contractlist.add("");
//								}
//								
//								if(q.getCategory()!=null&&!q.getCategory().equals("")){
//									contractlist.add(q.getCategory());
//								}else{
//									contractlist.add("");
//								}
//								
//								if(q.getType()!=null&&!q.getType().equals("")){
//									contractlist.add(q.getType());
//								}else{
//									contractlist.add("");
//								}
//								
//								if(q.getBranch()!=null&&!q.getBranch().equals("")){
//									contractlist.add(q.getBranch());
//								}else{
//									contractlist.add("");
//								}
//								
//								if(q.getEmployee()!=null&&!q.getEmployee().equals("")){
//									contractlist.add(q.getEmployee());
//								}else{
//									contractlist.add("");
//								}
//								
//								if(q.getApproverName()!=null&&!q.getApproverName().equals("")){
//									contractlist.add(q.getApproverName());
//								}else{
//									contractlist.add("");
//								}
//								
//								/**
//								 * @author Anil
//								 * @since 02-04-2020
//								 * Turn around time for premimum tech
//								 */
//								if(q.getTat()!=null){
//									contractlist.add(q.getTat()+"");
//								}else{
//									contractlist.add("");
//								}
//								contractlist.add(q.getItems().get(g).getPrduct().getCount()+"");
//								contractlist.add(q.getItems().get(g).getProductCode());
//								contractlist.add(q.getItems().get(g).getProductName());
//
//								//Ashwini Patil Date:8-03-2023
//								if(q.getItems().get(g).getArea()!=null)
//									contractlist.add(q.getItems().get(g).getArea());
//								else
//									contractlist.add("");
//								
//								if(q.getItems().get(g).getNumberOfServices()!=0){
//									contractlist.add(q.getItems().get(g).getNumberOfServices()+"");
//								}else{
//									contractlist.add("");
//								}
//								
//								contractlist.add(q.getItems().get(g).getPrice()+"");
//								contractlist.add(q.getTotalAmount()+"");
//								/**
//								 *  Date : 17-03-2021 Added By Priyanka.
//								 *  Des : Pecop-Unable to Download Contract Data Technical Bug By Ashwini.
//								 */
//								
//								if(q.getItems().get(g).getServiceTax()!=null && 
//										!q.getItems().get(g).getServiceTax().equals("")
//										&&q.getItems().get(g).getServiceTax().getPercentage()!=0)
//								//if(q.getItems().get(g).getServiceTax().getPercentage()!=0)
//								{
//									double taxValue = q.getItems().get(g).getServiceTax().getPercentage();
//									double taxAmt  = q.getTotalAmount()*taxValue /100;
//									
//									if(q.getItems().get(g).getServiceTax().getTaxPrintName()!=null && !q.getItems().get(g).getServiceTax().getTaxPrintName().equals("")){
//										contractlist.add(q.getItems().get(g).getServiceTax().getTaxPrintName());
//									}
//									else{
//										contractlist.add(q.getItems().get(g).getServiceTax().getTaxConfigName());
//									}
//									contractlist.add(taxValue+"");
//									contractlist.add(taxAmt+"");
//									
//								}
//								else
//								{
//									contractlist.add("NA");
//									contractlist.add("NA");
//									contractlist.add("NA");
//								}
//								if(q.getItems().get(g).getVatTax()!=null && 
//										!q.getItems().get(g).getVatTax().equals("")
//										&&q.getItems().get(g).getVatTax().getPercentage()!=0)
//								//if(q.getItems().get(g).getVatTax().getPercentage()!=0)
//								{
//									double taxValue = q.getItems().get(g).getVatTax().getPercentage();
//									double taxAmt  = q.getTotalAmount()*taxValue /100;
//									
//									//  new codeby rohan for gst 
//									if(q.getItems().get(g).getVatTax().getTaxPrintName()!=null && !q.getItems().get(g).getVatTax().getTaxPrintName().equals("")){
//										contractlist.add(q.getItems().get(g).getVatTax().getTaxPrintName());
//									}
//									else{
//										contractlist.add(q.getItems().get(g).getVatTax().getTaxConfigName());
//									}
//									
//									contractlist.add(taxValue+"");
//									contractlist.add(taxAmt+"");
//									
//								}
//								else
//								{
//									contractlist.add("NA");
//									contractlist.add("NA");
//									contractlist.add("NA");
//								}
////								ends here 
//								
//								/*** Date 11-09-2017 added by vijay for discount amt round off amt ***/
//								contractlist.add(q.getTotalAmount()+"");
//								contractlist.add(q.getDiscountAmt()+"");
//								contractlist.add(q.getFinalTotalAmt()+"");
//								contractlist.add(q.getInclutaxtotalAmount()+"");
//								contractlist.add(q.getGrandTotalAmount()+"");
//								contractlist.add(q.getRoundOffAmt()+"");
//								contractlist.add(q.getNetpayable()+"");
//
//								if(q.getPaymentMethod()!=null){
//									contractlist.add(q.getPaymentMethod());
//								}else{
//									contractlist.add("");
//								}
//								contractlist.add(q.getStatus());
//
//								String stackDetaisl="";
//								if(q.getStackDetailsList()!=null){
//									for (int i = 0; i < q.getStackDetailsList().size(); i++) {
//										if (i == 0) {
//											stackDetaisl = q.getStackDetailsList().get(i)
//													.getWareHouse()
//													+ "$"
//													+ q.getStackDetailsList().get(i)
//															.getStorageLocation()
//													+ "$"
//													+ q.getStackDetailsList().get(i)
//															.getStackNo()
//													+ "$"
//													+ q.getStackDetailsList().get(i)
//															.getQauntity()
//													+ "$"
//													+ q.getStackDetailsList().get(i)
//															.getDescription();
//
//										} else {
//											stackDetaisl = stackDetaisl
//													+ "$$$"
//													+ q.getStackDetailsList().get(i)
//															.getWareHouse()
//													+ "$"
//													+ q.getStackDetailsList().get(i)
//															.getStorageLocation()
//													+ "$"
//													+ q.getStackDetailsList().get(i)
//															.getStackNo()
//													+ "$"
//													+ q.getStackDetailsList().get(i)
//															.getQauntity()
//													+ "$"
//													+ q.getStackDetailsList().get(i)
//															.getDescription();
//
//										}
//									}
//								}
//								if(stackDetaisl!=null&&!stackDetaisl.equals("")){
//									contractlist.add(stackDetaisl.trim());
//								}else{
//									contractlist.add("");
//								}
//								/*
//								 *  nidhi
//								 *   30-06-2017  segment field
//								 */
//									if(q.getSegment()!=null&&!q.getSegment().equals("")){		
//										contractlist.add(q.getSegment());
//									}else{
//										contractlist.add("");
//									}
//									if(q.getContractPeriod()!=null&&!q.getContractPeriod().equals("")){		
//										contractlist.add(q.getContractPeriod());
//									}else{
//										contractlist.add("");
//									}
//									/** date 19.03.2018 added by komal for description **/
//									if(q.getDescription()!=null){
//										contractlist.add(q.getDescription());
//									}else{
//										contractlist.add("");
//									}
//									/**
//									 * nidhi
//									 * 9-08-2018
//									 * for map serial no
//									 */
//									if(showHideSerialNo){
//										contractlist.add(q.getItems().get(g).getProModelNo());
//										contractlist.add(q.getItems().get(g).getProSerialNo());
//									}
//									/**
//									 * end
//									 */
//									
//									/*** Date :- 19-09-2019 by Vijay Des:- For NBHC CCPM DO not renewal Remark ***/
//									
//									/** date 04-06-2019 added by komal to show renewed flag **/
//									if(q.isRenewFlag()){
//										contractlist.add("YES");
//									}else{
//										contractlist.add("NO");
//									}
//									contractlist.add(q.getNonRenewalRemark());
//
//									if(q.getAccountManager()!=null){
//										contractlist.add(q.getAccountManager());
//									}else{
//										contractlist.add("");
//									}
//							
//							}
//									
//						}
//						
//						
//						csvservice.setExcelData(contractlist, columnCount, AppConstants.CONTRACT, new AsyncCallback<String>() {
//							
//							@Override
//							public void onSuccess(String result) {
//								// TODO Auto-generated method stub
//								
//								String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//								final String url=gwt + "CreateXLXSServlet"+"?type="+12;
//								Window.open(url, "test", "enabled");
//							}
//							
//							@Override
//							public void onFailure(Throwable caught) {
//								// TODO Auto-generated method stub
//								
//							}
//						});
						
					}
				}
			});
		}
		
	}
	
	private void reactOnViewCurrentDayServices() {
		
		Date today=new Date();
		Date startdate=new Date(today.getYear(), today.getMonth(), 1); // new Date(today.getYear(), today.getMonth(), 1);
		Console.log("StartDate="+startdate);
		Date enddate=new Date(today.getYear(), today.getMonth(), 1);
		//CalendarUtil.addDaysToDate(enddate, 30);
		CalendarUtil.addMonthsToDate(enddate, 1);
		Console.log("endDate="+enddate);
		
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("contractCount");
		filter.setIntValue(model.getCount());
		temp.add(filter);
		
		
		filter=new Filter();
		filter.setQuerryString("serviceDate >=");
		filter.setDateValue(startdate);
		temp.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("serviceDate <=");
		filter.setDateValue(enddate);
		temp.add(filter);
		
		
		querry.setFilters(temp);
		querry.setQuerryObject(new Service());
		form.showWaitSymbol();
		service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				form.hideWaitSymbol();
				if(result.size()==0){
					form.showDialogMessage("No service found for current month.");
					return;
				}
				else{
					Console.log("in view services result size="+result.size());
					ServiceForm.rowcount=result.size(); //Ashwini Patil Date:7-09-2023 as count was showing zero when clicked on view services
					ServicePresenter.initalize(querry);
				}
			}
		});
		
	
		
		
	
	}
}
