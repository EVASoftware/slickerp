package com.slicktechnologies.client.views.contract;
import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.SalesLineItem;
public interface UpdatePricesServiceAsync {

	void saveAfterPriceUpdateForContract_Billing(long id,int contractId,
			List<SalesLineItem> listOfItems,
			AsyncCallback<ArrayList<Integer>> callback);
}
