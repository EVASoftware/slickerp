package com.slicktechnologies.client.views.contract;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.slicktechnologies.client.views.contractrenewal.RenewalResult;
import com.slicktechnologies.shared.AmcAssuredRevenueReport;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
@RemoteServiceRelativePath("contractservice")
public interface ContractService extends RemoteService 
{
	public void changeStatus(Contract contract);
	
	public boolean createServicesFromContrat(Contract contract);

	/**
	 * rohan added this method to create services as per EVA requirement (Only For EVA)
	 * Date: 12-06-2017
	 */
	public boolean createServicesAsPerEVA(Contract contract);
	
	/*
	 *  nidhi 
	 *  1-07-2017
	 * 	method for service revanue report genrator report 	
	 * last update 13-04-2018
	 */
	public String serviceRevanueReport(Long companyId,Date startDate, Date endDate,String branchName);
	
	public String technicianRevanueReport(Long companyId,Date startDate, Date endDate,String branchName);
	/*
	 * end
	 */
	
	/**
	 * Date : 13-09-2017 By ANIL
	 * Following methods are added for saving contract services ,if it has more than 1000 services in it.
	 * 
	 * @param companyId
	 * @param documnetType
	 * @param documentId
	 * @param callBack
	 */
	
	public ArrayList<ServiceSchedule> getScheduleServiceSearchResult(Long companyId,String documnetType,int documentId);
	
	public void updateScheduleService(String operation,ArrayList<ServiceSchedule>serSchList);
	
	public void deleteSchServices(Long companyId);
	
	public ArrayList<AmcAssuredRevenueReport> getAmcAssuredRevenueReport(Long companyId, Date date, String branch);
	/**
	 * End
	 */
	
	/** Date 30-05-2018 By vijay for updating services ***/
	public boolean updateServices(Contract contract);

	public ArrayList<RenewalResult> contractRenewalList(ArrayList<Filter>filter,ArrayList<Filter>filter1);
	public ArrayList<RenewalResult> contractRenewalReeport(ArrayList<RenewalResult> contList);
//	public boolean renewContractReport()
}
