package com.slicktechnologies.client.views.contract;

import java.util.Date;

import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;

public class PaymentTermsTable extends SuperTable<PaymentTerms> {
	TextColumn<PaymentTerms> daysColumn;
	TextColumn<PaymentTerms> percentColumn;
	TextColumn<PaymentTerms> viewCommentColumn;
	Column<PaymentTerms,String>commentColumn;
	TextColumn<PaymentTerms> branchColumn;
	private Column<PaymentTerms, String> delete;

	NumberFormat nf=NumberFormat.getFormat("0.00");
	
	/**
	 * Date :08-08-2017 By ANIL
	 */
	TextColumn<PaymentTerms> paymentDateColumn;
	
	/**
	 * End
	 */
	
	public PaymentTermsTable() {
		super();
	}

	@Override
	public void createTable() {
		addColumnDays();
		addColumnPercent();
		addColumnComment();
		addColumnBranch();
		addColumnPaymentDate();
		addColumnDelete();
		setFieldUpdaterOnDelete();
		/**
		 * Date : 12-10-2017 BY ANIL
		 * Setting Updater on payment terms comment
		 */
		setFieldUpdaterOnComment();
		
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}
	
	private void setFieldUpdaterOnComment() {
		commentColumn.setFieldUpdater(new FieldUpdater<PaymentTerms, String>() {
			@Override
			public void update(int index, PaymentTerms object, String value) {
				object.setPayTermComment(value);
				table.redraw();
			}
		});
	}

	private void addColumnPaymentDate() {
		paymentDateColumn = new TextColumn<PaymentTerms>() {
			@Override
			public String getValue(PaymentTerms object) {
				if (object.getPaymentDate() != null) {
					return AppUtility.parseDate(object.getPaymentDate());
				} else {
					return " ";
				}
			}
		};
		table.addColumn(paymentDateColumn, "Payment Date / Billing Date"); //Ashwini Patil Date:8-07-2024 added billing date label on request of ultima pest
		paymentDateColumn.setSortable(true);
		
	}

	private void addColumnBranch() {

		branchColumn=new TextColumn<PaymentTerms>() {

			@Override
			public String getValue(PaymentTerms object) {
				if(object.getBranch()!=null)
				{
					return object.getBranch();
				}
				else{
				return " ";
				}
			}
		};
		
		table.addColumn(branchColumn,"Branch");
		//table.setColumnWidth(commentColumn, 150, Unit.PX);
		branchColumn.setSortable(true);
	}
	
	public void addColumnDays()
	{
		daysColumn=new TextColumn<PaymentTerms>() {

			@Override
			public String getValue(PaymentTerms object) {
				return object.getPayTermDays()+"";
			}
		};
		
		table.addColumn(daysColumn,"Days");
		//table.setColumnWidth(daysColumn, 150, Unit.PX);
		daysColumn.setSortable(true);
	}
	
	public void addColumnPercent()
	{
		percentColumn=new TextColumn<PaymentTerms>() {

			@Override
			public String getValue(PaymentTerms object) {
//				return object.getPayTermPercent()+"";
				return nf.format(object.getPayTermPercent());
			}
		};
		
		table.addColumn(percentColumn,"Percent");
		//table.setColumnWidth(percentColumn, 150, Unit.PX);
		percentColumn.setSortable(true);
	}
	
	/**
	 * Date : 12-10-2107 BY ANIL
	 **/
	public void viewColumnComment()
	{
		viewCommentColumn=new TextColumn<PaymentTerms>() {

			@Override
			public String getValue(PaymentTerms object) {
				return object.getPayTermComment().trim();
			}
		};
		
		table.addColumn(viewCommentColumn,"#Comment");
		//table.setColumnWidth(commentColumn, 150, Unit.PX);
		viewCommentColumn.setSortable(true);
	}
	
	public void addColumnComment()
	{
//		commentColumn=new TextColumn<PaymentTerms>() {
//
//			@Override
//			public String getValue(PaymentTerms object) {
//				return object.getPayTermComment().trim();
//			}
//		};
//		
//		table.addColumn(commentColumn,"Comment");
//		//table.setColumnWidth(commentColumn, 150, Unit.PX);
//		commentColumn.setSortable(true);
		
		//  rohan made this field editable  
		EditTextCell editCell=new EditTextCell();
		commentColumn=new Column<PaymentTerms,String>(editCell)
				{
			@Override
			public String getValue(PaymentTerms object)
			{
				return object.getPayTermComment();
			}
				};
				table.addColumn(commentColumn,"#Comment");
				table.setColumnWidth(commentColumn, 100,Unit.PX);
	}
	
	private void addColumnDelete()
	{
		ButtonCell btnCell= new ButtonCell();
		delete = new Column<PaymentTerms, String>(btnCell) {

			@Override
			public String getValue(PaymentTerms object) {
				
				return "Delete";
			}
		};
		table.addColumn(delete,"Delete");
	}

	private void setFieldUpdaterOnDelete()
	{
		delete.setFieldUpdater(new FieldUpdater<PaymentTerms, String>() {
			
			@Override
			public void update(int index, PaymentTerms object, String value) {
				getDataprovider().getList().remove(index);
				table.redrawRow(index);
			
			}
		});
	}
	
	public  void setEnabled(boolean state)
	{
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();
	}
	
	private void addeditColumn()
	{
		addColumnDays();
		addColumnPercent();
		addColumnComment();
		addColumnBranch();
		addColumnPaymentDate();
		addColumnDelete();
		setFieldUpdaterOnDelete();
		/**
		 * Date : 12-10-2017 BY ANIL
		 * Setting Updater on payment terms comment
		 */
		setFieldUpdaterOnComment();
	}
	
	private void addViewColumn()
	{
		addColumnDays();
		addColumnPercent();
		viewColumnComment();
		addColumnBranch();
		addColumnPaymentDate();
	}
	
	

	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<PaymentTerms>() {
			@Override
			public Object getKey(PaymentTerms item) {
				if(item==null)
					return null;
				else
					return new Date().getTime();
			}
		};
	
		
	}

	@Override
	public void addFieldUpdater() {
		
		createFieldUpdaterOnCommentFieldColumn();
	}
	
	
	
	protected void createFieldUpdaterOnCommentFieldColumn()
	{
		commentColumn.setFieldUpdater(new FieldUpdater<PaymentTerms, String>()
				{
			@Override

			public void update(int index,PaymentTerms object,String value)
			{

				try{
					String val1=value.trim();
					object.setPayTermComment(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}
				catch (NumberFormatException e)
				{

				}
				table.redrawRow(index);
			}
				});
	}
	
	

	@Override
	public void setEnable(boolean state) {
		int tablecolcount=this.table.getColumnCount();
		for(int i=tablecolcount-1;i>-1;i--)
		  table.removeColumn(i);
		if(state ==true)
			addeditColumn();
		if(state==false)
			addViewColumn();
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}	

}
