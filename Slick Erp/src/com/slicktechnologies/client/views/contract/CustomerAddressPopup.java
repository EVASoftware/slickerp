package com.slicktechnologies.client.views.contract;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.Button;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.simplesoftwares.client.library.composite.AddressComposite;

public class CustomerAddressPopup extends PopupScreen {

	
	AddressComposite billingAddressComposite,serviceAddressComposite;
	Button btnbillingaddr;
	
	public CustomerAddressPopup(){
		super();
		createGui();
	}
	
	private void initializeWidget() {

		billingAddressComposite=new AddressComposite();
		serviceAddressComposite=new AddressComposite();
		btnbillingaddr=new Button("Copy Billing Address");
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		
		initializeWidget();
		
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingBillingAddress=fbuilder.setlabel("Billing Address").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",billingAddressComposite);
		FormField fbillingAddressComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder=new FormFieldBuilder("",btnbillingaddr);
		FormField fserviceadrsbtn=fbuilder.build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingServiceAddress=fbuilder.setlabel("Service Address").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",serviceAddressComposite);
		FormField fserviceAddressComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		FormField[][] formfield = {  
				{fgroupingBillingAddress},
				{fbillingAddressComposite},
				{fgroupingServiceAddress},
				{fserviceadrsbtn},
				{fserviceAddressComposite},
		};
		this.fields=formfield;
	}
	

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	public AddressComposite getBillingAddressComposite() {
		return billingAddressComposite;
	}

	public void setBillingAddressComposite(AddressComposite billingAddressComposite) {
		this.billingAddressComposite = billingAddressComposite;
	}

	public AddressComposite getServiceAddressComposite() {
		return serviceAddressComposite;
	}

	public void setServiceAddressComposite(AddressComposite serviceAddressComposite) {
		this.serviceAddressComposite = serviceAddressComposite;
	}

	public Button getBtnbillingaddr() {
		return btnbillingaddr;
	}

	public void setBtnbillingaddr(Button btnbillingaddr) {
		this.btnbillingaddr = btnbillingaddr;
	}
	
	
	

}
