package com.slicktechnologies.client.views.contract;

import java.util.List;

import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.ProductOtherCharges;

public class ProductTaxesTable extends SuperTable<ProductOtherCharges> {
	
	/******************************** Column declaration **************************/
	
	TextColumn<ProductOtherCharges> prodTaxNameColumn;
	TextColumn<ProductOtherCharges> prodTaxPercentColumn;
	TextColumn<ProductOtherCharges> assessableAmountColumn;
	TextColumn<ProductOtherCharges> calculatedTaxAmount;
	NumberFormat nf = NumberFormat.getFormat("0.00");
	
	AppUtility appUtility = new AppUtility();
	/****************************************************************************/
	
	/*********************************Constructor********************************/
	
	public ProductTaxesTable() {
		super();
	}
	
	/****************************************************************************/
	
	/********** Create Table Method ***********/
	@Override
	public void createTable() {
		createColumnTaxName();
		createColumnTaxPercent();
		createColumnAssessableAmount();
		createColumnCalcTaxAmount();
	    table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}
	
	
	/*********************************** Tax Name Column *****************************/
	public void createColumnTaxName()
	{
		prodTaxNameColumn=new TextColumn<ProductOtherCharges>() {

			@Override
			public String getValue(ProductOtherCharges object) {	
				return object.getChargeName().trim();
			}
		};
		table.addColumn(prodTaxNameColumn,"Tax");
	//	table.setColumnWidth(prodTaxNameColumn, 4, Unit.PX);
	}
	
	/*********************************** Tax Percent Column *****************************/
	public void createColumnTaxPercent()
	{
		prodTaxPercentColumn=new TextColumn<ProductOtherCharges>() {
			@Override
			public String getValue(ProductOtherCharges object) {
				return object.getChargePercent()+"";
			}
		};
		table.addColumn(prodTaxPercentColumn,"%");
	//	table.setColumnWidth(prodTaxPercentColumn, 4, Unit.PX);
	}
	
	/*********************************** Assessable Amount Column *****************************/
	/**
	 * This column represents the amount on which tax will be calculated. 
	 */
	
	public void createColumnAssessableAmount()
	{
		assessableAmountColumn=new TextColumn<ProductOtherCharges>() {
			@Override
			public String getValue(ProductOtherCharges object) {
				
				
				object.getAssessableAmount();
			
				return nf.format(object.getAssessableAmount());
				}
		};
		table.addColumn(assessableAmountColumn,"Ass. Val.");
	//	table.setColumnWidth(assessableAmountColumn, 5, Unit.PX);
		
	}
	
	/************************************Calculated Tax Amount***********************************/
	public void createColumnCalcTaxAmount()
	{
		calculatedTaxAmount=new TextColumn<ProductOtherCharges>() {
			@Override
			public String getValue(ProductOtherCharges object) {
			//	DecimalFormat df = new DecimalFormat("#.00");
				double calcAmt=object.getAssessableAmount()*object.getChargePercent()/100;
				//return Math.round(calcAmt)+"";
				nf.format(calcAmt);
				object.setChargePayable(calcAmt);
				if(LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
					double amt = appUtility.getLastTwoDecimalOnly(calcAmt);
					Console.log("tax Amt "+amt);
					return amt+"";
				}
				else{
					return nf.format(object.getChargePayable());
				}
			}
		};
		table.addColumn(calculatedTaxAmount,"Amt");
	//	table.setColumnWidth(calculatedTaxAmount, 4, Unit.PX);
		
	}

	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		
	}

	@Override
	public void setEnable(boolean state) {
		
	}

	@Override
	public void applyStyle() {
		
	}
	
	
	public double calculateTotalTaxes()
	{
		List<ProductOtherCharges>list=getDataprovider().getList();

		double sum=0;
		for(int i=0;i<list.size();i++)
		{
			ProductOtherCharges entity=list.get(i);
			//double totalTaxAmt=Math.round(entity.getAssessableAmount()*entity.getChargePercent()/100);
			double totalTaxAmt=(entity.getAssessableAmount()*entity.getChargePercent()/100);
			
			//Ashwini Patil Date:25-04-2022
			if(LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
				double amt = appUtility.getLastTwoDecimalOnly(totalTaxAmt);
				sum=sum+amt;
			}
			else{
				sum=sum+totalTaxAmt;
			}
			
		}
		
		return sum;

	}


	
}
