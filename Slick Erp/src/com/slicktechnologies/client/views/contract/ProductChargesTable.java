package com.slicktechnologies.client.views.contract;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.slicktechnologies.shared.OtherTaxCharges;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

public class ProductChargesTable extends SuperTable<ProductOtherCharges> {
	
	/******************************** Column declaration **************************/
	
	Column<ProductOtherCharges,String> prodChargeNameColumn;
	Column<ProductOtherCharges,String> prodChargePercentColumn;
	Column<ProductOtherCharges,String> prodChargeAbsValColumn;
	TextColumn<ProductOtherCharges> assessmentAmtColumn;
	TextColumn<ProductOtherCharges> calculateChargeAmtColumn;
	private Column<ProductOtherCharges, String> delete;
	private Column<ProductOtherCharges, String> addNew;
	public ArrayList<String> chargesList;
	ArrayList<OtherTaxCharges> otcLis;
	NumberFormat nf=NumberFormat.getFormat("0.00");
	/****************************************************************************/
	
	/*********************************Constructor********************************/

	
	public ProductChargesTable() {
		super();
		chargesList=new ArrayList<String>();
		otcLis=new ArrayList<OtherTaxCharges>();
	}
	/****************************************************************************/
	
	/********** Create Table Method ***********/
	
	@Override
	public void createTable() {
		addColumnRemoveCharge();
		retriveChargesNames();
	    table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}

	/********************************Retrieve Charge Names Method***************************************/
	/**
	 * This method is used to retrieving charge names in drop down list in column.
	 * This is add surcharges (if any) on product.
	 */
	
	public void retriveChargesNames()
	{
		final GenricServiceAsync service=GWT.create(GenricService.class);
		Vector<Filter>filtervalue=new Vector<Filter>(); 
		MyQuerry query=new MyQuerry();
		query.setFilters(filtervalue);
		query.setQuerryObject(new OtherTaxCharges());
		service.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
			
		@Override
		public void onFailure(Throwable caught) {
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				chargesList.clear();
				if(chargesList.size()==0){
					chargesList.add("SELECT");
				}
				for(SuperModel model:result)
				{
				 OtherTaxCharges entity=(OtherTaxCharges) model;
				 if(chargesList.size()>=1){
					 chargesList.add(entity.getOtherChargeName());
				 }
				 otcLis.add(entity);
				}
				createColumnChargeName();
				createColumnPercent();
				createColumnAbsoluteValue();
				createColumnAssessmentAmt();
				createColumnChargeAmt();
				addColumnNewCharge();
				addFieldUpdater();
			}
		});
	}
	
	/********************************Charge Name Column*******************************************/
	
	public void createColumnChargeName()
	{
		SelectionCell chargeselection=new SelectionCell(chargesList);
		prodChargeNameColumn=new Column<ProductOtherCharges, String>(chargeselection) {

			@Override
			public String getValue(ProductOtherCharges object) {
				if(object.getChargeName()!=null){
					return object.getChargeName();
				}
				else{
					return "";
				}
				
			}
		};
		table.addColumn(prodChargeNameColumn,"Charge");
	}
	
	/********************************Charge Name Column (In View) *************************************/
	
	public void createColumnViewChargeName()
	{
		prodChargeNameColumn=new TextColumn<ProductOtherCharges>() {
			
			@Override
			public String getValue(ProductOtherCharges object) {
				if(object.getChargeName()!=null){
					return object.getChargeName().trim();
				}
				return "";
			}
		};
		table.addColumn(prodChargeNameColumn,"Charge");
	}
	
	/**********************************Charge Name Column (For Updater)*******************************/
	
	protected void createFieldUpdaterChargeName()
	{
		prodChargeNameColumn.setFieldUpdater(new FieldUpdater<ProductOtherCharges, String>()
		{
			@Override
			public void update(int index,ProductOtherCharges object,String value)
			{
				try{
					String val1=(value.trim());
					double perVal=0,absVal=0;
					
					for(int i=0;i<otcLis.size();i++)
					{
						if(val1.equals(otcLis.get(i).getOtherChargeName())&&otcLis.get(i).getOtherChargePercent()!=null){
							perVal=otcLis.get(i).getOtherChargePercent();
						}
						if(val1.equals(otcLis.get(i).getOtherChargeName())&&otcLis.get(i).getOtherChargeAbsValue()!=null){
							absVal=otcLis.get(i).getOtherChargeAbsValue();
						}
					}
					object.setChargePercent(perVal);
					object.setChargeAbsValue(absVal);
					object.setChargeName(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				}
				catch (NumberFormatException e){
					e.printStackTrace();
				}table.redrawRow(index);
				
			}
		});
	}
	
	/************************************Percent Column********************************************/
	
	protected void createColumnPercent()
	{
	EditTextCell editCell=new EditTextCell();
	prodChargePercentColumn=new Column<ProductOtherCharges,String>(editCell)
	{
	@Override
	public String getValue(ProductOtherCharges object)
	{
		if(object.getChargePercent()==0||object.getChargePercent()>100)
		{
			return "";
		}
		else
		{
			return object.getChargePercent()+"";
		}
	}
	};
	table.addColumn(prodChargePercentColumn,"%");
	}
	
	/**********************************Percent Column (In View)*******************************************/
	
	protected void createColumnViewPercent()
	{
		prodChargePercentColumn=new TextColumn<ProductOtherCharges>()
				{
			@Override
			public String getValue(ProductOtherCharges object)
			{
				if(object.getChargePercent()==0||object.getChargePercent()>100){
					return "";}
				else{
					return object.getChargePercent()+"";}
			}
				};
				table.addColumn(prodChargePercentColumn,"%");
	}
	
	/*************************************Percent Column (For Updater)***********************************/
	
	protected void createFieldUpdaterPercent()
	{
		prodChargePercentColumn.setFieldUpdater(new FieldUpdater<ProductOtherCharges, String>()
		{
		@Override
		public void update(int index,ProductOtherCharges object,String value)
		{
			try{
				Double val1=Double.parseDouble(value.trim());
				double valueforperc=val1;
				if(valueforperc<=100){
					object.setChargeAbsValue(0);
					object.setChargePercent(valueforperc);
				}
				if(valueforperc>100){
					valueforperc=0;
				}
				object.setChargePercent(valueforperc);
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
			}
			catch (NumberFormatException e)
			{
			}
			table.redrawRow(index);
		}
		});
	}
	
	/**********************************Absolute Value Column*******************************************/
	
	protected void createColumnAbsoluteValue()
	{
	EditTextCell editCell=new EditTextCell();
	prodChargeAbsValColumn=new Column<ProductOtherCharges,String>(editCell)
	{
	@Override
	public String getValue(ProductOtherCharges object)
	{
		if(object.getChargeAbsValue()==0){
			return "";
		}
		else{
			object.setAssessableAmount(0);
			return object.getChargeAbsValue()+"";
		}
	}
	};
	table.addColumn(prodChargeAbsValColumn,"Abs Val");
	}
	
	/**********************************Absolute Value Column (In View)***************************************/
	
	protected void createColumnViewAbsoluteValue()
	{
		prodChargeAbsValColumn=new TextColumn<ProductOtherCharges>()
				{
			@Override
			public String getValue(ProductOtherCharges object)
			{
				if(object.getChargeAbsValue()==0){
					return "";}
				else{
					object.setAssessableAmount(0);
					return object.getChargeAbsValue()+"";}
			}
				};
				table.addColumn(prodChargeAbsValColumn,"Abs Val");
	}
	
	/**************************Absolute Value Column (For Updater)**************************************/
	
	protected void createFieldUpdaterAbsoluteValue()
	{
		prodChargeAbsValColumn.setFieldUpdater(new FieldUpdater<ProductOtherCharges, String>()
		{
		@Override
		public void update(int index,ProductOtherCharges object,String value)
		{
			try{
				Integer val1=Integer.parseInt(value.trim());
				object.setChargePercent(0);
				object.setChargeAbsValue(val1);
				
				RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
			}
			catch (NumberFormatException e)
			{
			}
			table.redrawRow(index);
		}
		});
	}
	
	/************************************Assessment Amount Column**************************************/
	
	protected void createColumnAssessmentAmt()
	{
	assessmentAmtColumn=new TextColumn<ProductOtherCharges>()
	{
	@Override
	public String getValue(ProductOtherCharges object)
	{
		return object.getAssessableAmount()+"";
	}
	};
	table.addColumn(assessmentAmtColumn,"Ass. Val.");
	}
	
	/********************************Calculated Charge Amount Column****************************************/
	
	protected void createColumnChargeAmt()
	{
		calculateChargeAmtColumn=new TextColumn<ProductOtherCharges>()
		{
		@Override
		public String getValue(ProductOtherCharges object)
		{
			double chargePercAmt=0,totalCalcAmt=0;
			if(object.getChargePercent()!=0){
				chargePercAmt=object.getAssessableAmount()*object.getChargePercent()/100 ;
				totalCalcAmt=chargePercAmt;
			}
			if(object.getChargeAbsValue()!=0){
				totalCalcAmt=object.getChargeAbsValue();
			}
			totalCalcAmt=Double.parseDouble(nf.format(totalCalcAmt));
			
			object.setChargePayable(totalCalcAmt);
			
			return nf.format(object.getChargePayable());
		}
		};
		table.addColumn(calculateChargeAmtColumn,"Amt");
	}

	/***********************************Remove Charge Button************************************/
	/**
	 * This button is used for deleting any charge added
	 */
	
	 private void addColumnRemoveCharge()
	 {
			ButtonCell btnCell= new ButtonCell();
			delete = new Column<ProductOtherCharges, String>(btnCell) {

				@Override
				public String getValue(ProductOtherCharges object) {
					
					return "-";
				}
			};
			table.addColumn(delete,"");
			
	}
	 
	 /*********************************Remove Charge Button(For Updater)**********************************/

	private void setFieldUpdaterOnRemoveCharge()
	{
		delete.setFieldUpdater(new FieldUpdater<ProductOtherCharges, String>() {
			
			@Override
			public void update(int index, ProductOtherCharges object, String value) {
				if(object.getFlagVal()==true){
					getDataprovider().getList().remove(object);
				}
				else{
					removeSurcharges(object.getIndexCheck(),object);
				}
				
				table.redrawRow(index);
			}
		});
	}
	
	/*********************************Add Charge Button*************************************/
	/**
	 * This button is used for adding new charge on product.
	 */
	
	 private void addColumnNewCharge()
		{
			ButtonCell btnCell= new ButtonCell();
			addNew = new Column<ProductOtherCharges, String>(btnCell) {

				@Override
				public String getValue(ProductOtherCharges object) {
					
					return "+";
				}
			};
			table.addColumn(addNew,"");
		}

	 /*********************************Add Charge Button(For Updater)**********************************/
	 /**
	  * On click of this button, new record will be added of type ProductOtherCharges and
	  * the assessable amount will be retrieved from the previous record.
	  * It basically represents surcharge tax.
	  */
	 
		private void setFieldUpdaterOnNew()
		{
			addNew.setFieldUpdater(new FieldUpdater<ProductOtherCharges, String>() {
				
				@Override
				public void update(int index, ProductOtherCharges object, String value) {
					double calcVal=0;
					if(object.getChargePercent()!=0){
						calcVal=object.getChargePercent()*object.getAssessableAmount()/100;
						calcVal=Double.parseDouble(nf.format(calcVal));
					}
					if(object.getChargeAbsValue()!=0){
						calcVal=object.getChargeAbsValue();
					}
					ProductOtherCharges poc=new ProductOtherCharges();
					poc.setChargeName("");
					poc.setChargeAbsValue(0);
					poc.setChargePercent(0);
					poc.setAssessableAmount(calcVal);
					poc.setFlagVal(true);
					poc.setIndexCheck(object.getIndexCheck());
					getDataprovider().getList().add(poc);
					table.redrawRow(index);
				}
			});
		}
			
			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}
			
	/*************************Field Updater Method (Overridden)*************************************/
			/**
			 * This method represents the columns which can be updated.
			 */

			@Override
			public void addFieldUpdater() {
				createFieldUpdaterChargeName();
				createFieldUpdaterPercent();
				createFieldUpdaterAbsoluteValue();
				setFieldUpdaterOnRemoveCharge();
				setFieldUpdaterOnNew();
			}

	/********************************Added It Method**************************************/
			/**
			 * This method represents the columns which are visible in Edit State
			 */
			
			private void addeditColumn()
			{
				addColumnRemoveCharge();
				retriveChargesNames();
//				addFieldUpdater();
			}
			
	/*************************************Add View Column***************************************/
			/**
			 * This method represents the column which is to be displayed in View State.
			 */
			
			/*
			 *  nidhi
			 *   18-07-2017
			 *   addColumnRemoveCharge , addColumnRemoveCharge method commented for make table read only
			 */
			
			private void addViewColumn()
			{
//				addColumnRemoveCharge();
				createColumnViewChargeName();
				createColumnViewPercent();
				createColumnViewAbsoluteValue();
				createColumnAssessmentAmt();
				createColumnChargeAmt();
//				addColumnRemoveCharge();
			}
			
	/*******************************Set Enable Method (Overridden)********************************************/
			/**
			 * This method represents the state of the screen through which the column will be
			 * displayed in table
			 */
			
			@Override
			public void setEnable(boolean state) {
				int tablecolcount=this.table.getColumnCount();
				for(int i=tablecolcount-1;i>-1;i--)
				  table.removeColumn(i);
				if(state ==true){
					addeditColumn();
				}
				if(state==false){
					addViewColumn();
				}
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}
	
	/*************************Calculate Net Payable Method******************************************/
			/**
			 * This method calculates the total charges amount i.e surcharge amount.
			 * The amount is calculated and fetched in Presenter and the value is set to
			 * Net Payable field in Form.
			 * @return
			 */
			
			public double calculateNetPayable()
			{
				List<ProductOtherCharges>list=getDataprovider().getList();

				double sum=0,calcTotal=0;
				for(int i=0;i<list.size();i++)
				{
					ProductOtherCharges entity=list.get(i);
					if(entity.getChargePercent()!=0){
						calcTotal=entity.getChargePercent()*entity.getAssessableAmount()/100;
					}
					if(entity.getChargeAbsValue()!=0){
						calcTotal=entity.getChargeAbsValue();
					}
					sum=sum+calcTotal;
				}
				
				return sum;
			}
			
	/************************************Retrieve Charge Values*****************************************/
			/**
			 * This method returns the index of the selected charge. This index is then matched will the
			 * preloaded list, named otcLis through which the corresponding charge % or abs val is 
			 * retrieved.
			 * @param prodChrg
			 * @return
			 */
	
			public int retrieveChargeValues(String prodChrg)
			{
				for(int i=0;i<chargesList.size();i++)
				{
					if(prodChrg.equals(chargesList.get(i)))
					{
						return i;
					}
				}
				return -1;
			}
			
			private void removeSurcharges(int indexCheckVal,ProductOtherCharges parentIndex)
			{
				List<ProductOtherCharges>list=getDataprovider().getList();
				for(int i=0;i<list.size();i++)
				{
					if(list.get(i).getFlagVal()==true&&list.get(i).getIndexCheck()==indexCheckVal){
						getDataprovider().getList().remove(i);
					}
				}
				getDataprovider().getList().remove(parentIndex);
			}
			


}
