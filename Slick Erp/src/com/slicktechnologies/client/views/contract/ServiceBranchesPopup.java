package com.slicktechnologies.client.views.contract;

import com.google.gwt.event.dom.client.ClickEvent;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;

public class ServiceBranchesPopup extends PopupScreen{

	ServiceBranchesTable serviceBranchesTable;
	
	public ServiceBranchesPopup(){
		super();
		serviceBranchesTable.getTable().setWidth("500px");
		serviceBranchesTable.getTable().setHeight("300px");
		//getPopup().setWidth("600px");
		getPopup().setHeight("450px");
		createGui();
	}
	
	private void initializeWidget(){
		serviceBranchesTable = new ServiceBranchesTable();
	}
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingServiceBranches=fbuilder.setlabel("Service Branches").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("", serviceBranchesTable.getTable());
		FormField fServiceBranchesTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		FormField[][] formfield = {  
				{fgroupingServiceBranches},
				{fServiceBranchesTable}
		};
		this.fields=formfield;
	}

	public ServiceBranchesTable getServiceBranchesTable() {
		return serviceBranchesTable;
	}

	public void setServiceBranchesTable(ServiceBranchesTable serviceBranchesTable) {
		this.serviceBranchesTable = serviceBranchesTable;
	}

}
