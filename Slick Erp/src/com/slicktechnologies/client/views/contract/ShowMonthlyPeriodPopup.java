package com.slicktechnologies.client.views.contract;

import java.util.Date;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;

public class ShowMonthlyPeriodPopup extends AbsolutePanel implements ValueChangeHandler{

	public DateBox fromDate;
	public Button btnOne;
	public Button btnTwo;
	
	public DateBox startDate;
	public DateBox endDate;
	
	public ShowMonthlyPeriodPopup() {
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy"); 
		
		btnOne=new Button("Ok");
		btnTwo=new Button("Cancel");
		
		InlineLabel displayMessage = new InlineLabel("From Date");
		add(displayMessage,10,10);
		
		fromDate=new DateBoxWithYearSelector();
		fromDate.addValueChangeHandler(this);
		add(fromDate,10,30);
		fromDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		InlineLabel stDtMsg = new InlineLabel("Start Date");
		add(stDtMsg,10,70);
		
		startDate=new DateBoxWithYearSelector();
		startDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		startDate.setEnabled(false);
		add(startDate,10,100);
		
		InlineLabel endDtMsg = new InlineLabel("End Date");
		add(endDtMsg,200,70);
		
		endDate=new DateBoxWithYearSelector();
		endDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		endDate.setEnabled(false);
		add(endDate,200,100);
		
		
		add(btnOne,30,150);
		add(btnTwo,120,150); 
		setSize("400px", "200px");
		this.getElement().setId("form");
		
	}

	public DateBox getFromDate() {
		return fromDate;
	}

	public void setFromDate(DateBox fromDate) {
		this.fromDate = fromDate;
	}

	public Button getBtnOne() {
		return btnOne;
	}

	public void setBtnOne(Button btnOne) {
		this.btnOne = btnOne;
	}

	public Button getBtnTwo() {
		return btnTwo;
	}

	public void setBtnTwo(Button btnTwo) {
		this.btnTwo = btnTwo;
	}

	public DateBox getStartDate() {
		return startDate;
	}

	public void setStartDate(DateBox startDate) {
		this.startDate = startDate;
	}

	public DateBox getEndDate() {
		return endDate;
	}

	public void setEndDate(DateBox endDate) {
		this.endDate = endDate;
	}

	@Override
	public void onValueChange(ValueChangeEvent event){
		
		System.out.println("In side value change handler");
		
		if(fromDate.getValue()!= null)
		{
//			Calendar calendar = Calendar.getInstance(); 
//			calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
//			Date nextMonthFirstDay = calendar.getTime();
//			calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
//			Date nextMonthLastDay = calendar.getTime();
//			
//			System.out.println("MonthFirstDay"+nextMonthFirstDay);
//			System.out.println("MonthLastDay"+nextMonthLastDay);
			
			DateTimeFormat fmt= DateTimeFormat.getFormat("DD-MM-YYYY");
			Date date =fromDate.getValue();
			
			date.setDate(1);
			date.setMonth(date.getMonth());
			Date startDate = date;
			
			this.startDate.setValue(date);
			
			System.out.println("Start Date of month "+startDate);
			
			date.setMonth(date.getMonth()+1);
			CalendarUtil.setToFirstDayOfMonth(date);
			CalendarUtil.addDaysToDate(date, -1);
			Date endDate = date;
			
			this.endDate.setValue(date);
			System.out.println("Start Date of month "+endDate);
			
		}
	}
	
	
	
}

