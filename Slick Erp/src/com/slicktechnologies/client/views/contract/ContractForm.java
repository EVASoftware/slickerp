package com.slicktechnologies.client.views.contract;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.itextpdf.text.log.SysoCounter;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ApprovableFormScreen;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utility.UnitConversionUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.complain.TimeFormatBox;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.project.concproject.ProjectPresenter;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.scheduleservice.ServiceSchedulePopUp;
import com.slicktechnologies.client.views.scheduleservice.ServiceScheduleTable;
import com.slicktechnologies.client.views.quotation.QuotationForm;
import com.slicktechnologies.client.views.quotation.QuotationPresenter;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.ComponentDetails;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Holiday;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
import com.slicktechnologies.client.views.popups.EmailIdPopup;

public class ContractForm extends ApprovableFormScreen<Contract> implements ClickHandler, ChangeHandler, ValueChangeHandler<Date> {

	//***********************************Variable Declaration Starts********************************************//
	//Token to add the varialble declarations
	//DateBox validuntil;
	ObjectListBox<Branch> olbbBranch;
	TextBox tbReferenceNumber;
	DateBox dbrefernceDate;
	
	/**
	 * Date:05/06/2021 By Priyanka Bhagwat.
	 * Adding Followup date for kernal pest.
	 */
	
	DateBox dbconfollowupdate;
	
	/**
	 * End
	 */	
	ObjectListBox<Config> olbcPriority,olbcPaymentMethods;
	PaymentTable paymenttable;
	TextBox tbContractId;
	TextBox tbRefeRRedBy;
	TextBox tbQuotationStatus;
	TextArea taDescription;
	UploadComposite ucUploadTAndCs;
	PersonInfoComposite personInfoComposite;
	ObjectListBox<Employee> olbeSalesPerson,olbApproverName;
	DateBox dbContractStartDate,dbContractEndDate;
	ObjectListBox<Config> olbContractGroup;
	ObjectListBox<Type> olbContractType;
	ObjectListBox<ConfigCategory> olbContractCategory;
	IntegerBox ibCreditPeriod;
	DoubleBox dototalamt,doamtincltax,donetpayamt;
	TextBox tbremark;
	TextBox tbLeadId,tbQuotatinId;
	IntegerBox ibdays;
	DoubleBox dopercent,doAmount;//Ashwini Patil Date:22-06-2022 Pepcopp requested this textbox where they will type amount and payment percent will be calculated automatically and vice versa 
	

	TextBox tbcomment,tbdonotrenew;
	Button addPaymentTerms;
	public PaymentTermsTable paymentTermsTable;
	FormField fgroupingCustomerInformation;
	 public SalesLineItemTable saleslineitemtable;
	
	ProductChargesTable chargesTable;
	Button addOtherCharges;
	ProductTaxesTable prodTaxTable;
	ProductInfoComposite prodInfoComposite;
	Button baddproducts;
	DateBox dbContractDate;
	CheckBox cbRenewFlag;
	TextBox ibOldContract;
	String f_serviceDay,f_serviceTime;
	Button f_btnservice;
	Date f_conStartDate;
	Date f_conEndDate;
	int f_duration;
	ServiceScheduleTable serviceScheduleTable;
	ServiceSchedule scheduleEntity;
	ServiceSchedulePopUp servicepop=new ServiceSchedulePopUp("Contract Id", "Contract Date");
	public ArrayList<CustomerBranchDetails> customerbranchlist=new ArrayList<CustomerBranchDetails>();
//	public boolean servicesFlag=false;
	static DateTimeFormat format = DateTimeFormat.getFormat("c"); 
	final GenricServiceAsync genasync=GWT.create(GenricService.class);
	
	TextBox tbTicketNumber;
	
	TextBox tbPremisesUnderContract;
	
	Contract contractObject;
	
	
	public CheckBox chkbillingatBranch;
	
	/** added by vijay for contrat is Contract Rate so if this checkbox is true then this contract service and billing will create
	 through manually using process level bar button i.e. Create Services
	 */
	public CheckBox chkservicewithBilling;
	
	/********************* changes for cash and chechque ********************/
	public ObjectListBox<Config> olbcNumberRange;
	
	/**
	 * Rohan added created By field for NBHC to know who is going to create/ generate interaction
	 * date : 03/11/2016
	 */
		TextBox tbCreatedBy;
	
	/**
	 * ends here 
	 */
		
	/**
	 * rohan added this flag for checking process configuration
	 */
	
		boolean salesPersonRestrictionFlag= false;
	
	/**
	 * ends here 
	 */
	
	// **********vaishnavi************
		// Amc Contract pdf Genesis
//	Button btnclientAsset;
//	ClientAssetTable clientassetTable;
//	ClientSideAsset clientAssetEntity;
//
//	ArrayList<ClientSideAsset> selectedlist;
	
	
	/**
	 * Older version developed by vaishnavi    
	 * I have made this method global so that it can be reusable   
	 * This variable are used for calculating payment terms Automatically
	 * I/p : 1.MaxDuration (Calculate max duration in all the product present in the table) 
	 * Developed by : Rohan Bhagde.
	 */
		public ObjectListBox<ConfigCategory> olbPaymentTerms;
		Button addPayTerms;
		public CheckBox cbStartOfPeriod;
		NumberFormat nf = NumberFormat.getFormat("0.00");
		
//		QuotationForm quotForm;
	/**
	 * Ends here 
	 */
		
		
	/**
	 * Date:24-01-2017 By Anil
	 * Adding reference no.2 for PCAMB and Pest Cure Incorporation
	 */
	
	TextBox tbRefNo2;
	
	/**
	 * End
	 */		
	
	
	
	
	
	
	/**
	 * Date : 25/01/2017
	 * Added by Rahul Verma from NBHC
	 * 
	 */
	
	/**
	 * rohan added this code for integrated pest control 
	 * Date:12-05-2017
	 * this is used is used to add technician in contract form 	
	 */
	
	ObjectListBox<Employee> olbTechnicainName;
	
	/**
	 * ends here 	
	 */
	
	Button btstackDetails;
		
		
		//  rohan added this 
		
	public static int productSrNo=0;
	
	
	/*
	 * nidhi
	 * 29-06-2017
	 * This field Stores the segment
	 */
	
	TextBox tbCustSegment;
	/*
	 *  end
	 */
	
	/**
	 * Date 1-07-2017 added by vijay for Tax validation
	 */
	Date date = DateTimeFormat.getFormat("yyyy-MM-dd HH:mm:ss").parse("2017-07-01 00:00:00");
	final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");

	/**
	 * ends here
	 */
	
	/***
	 * Date : 08-08-2017 By ANIL
	 * adding payment date 
	 */
	
	DateBox dbPaymentDate;
	/**
	 * End
	 */
	
	/** Date 01-09-2017 added by vijay for discount on total and round off **/
	DoubleBox dbDiscountAmt;
	DoubleBox dbFinalTotalAmt;
	TextBox tbroundOffAmt;
	DoubleBox dbGrandTotalAmt;
	
	/**
	 * Date : 06-10-2017 BY ANIL
	 * Following checkbox is used to generate bill service wise
	 */
	CheckBox cbServiceWiseBilling;
	/**
	 * End
	 */
	
	/**
	 * Date : 23-11-2017 BY ANIL
	 * this list stores the services of scheduled popup while doing advance filtering option
	 */
	
	public List<ServiceSchedule> globalScheduleServiceList=new ArrayList<ServiceSchedule>();
	
	 /** Date 06/2/2018 added by komal for consolidate price checkbox **/
	public CheckBox cbConsolidatePrice;
	 
 	/**
	 * Date 23-01-2018 By vijay
	 * for contract period for pcamb
	 */
	ObjectListBox<Config> olbcontractPeriod;
	
	//***********************************Variable Declaration Ends********************************************//

/****************************************Constructor*************************************************/
	/**
	 *  nidhi
	 *  29-01-2018
	 *  add for multiple contact list on customer
	 */
	ObjectListBox<String> objContactList;
	HorizontalPanel  hrflowPanel;
	/**
	 * nidhi
	 */
	public boolean contractRenewFlag =false;
	/**
	 * nidhi
	 * 9-06-2018
	 */
	public TextBox tbDonotRenewalMsg ;
	/** nidhi  *:*:* */
	Customer cust;
	/**
	 * Author  : nidhi 
	 * Date 26-10-2018
	 */
	public Date firstServiceDate = new Date();
	public boolean paymentDateFlag = false;
	
	/**Date 29-8-2019 by Amol Added 2 fields po number and po Date**/
	TextBox poNumber;
	DateBox poDate;
	boolean addPoDetails=false;
	boolean custCatandType=false;
    boolean	contractDateDisable=false;
    
    /**Date 10-9-2019 by Amol **/
    boolean makePoDetailsMandatory=false;
    boolean hideTechnicianAndContractPeriod=false;
    /**Date 23-10-2019 by Amol**/
	ObjectListBox<Config> olbProductGroup;
	ObjectListBox<ServiceProduct>olbProductName;
	boolean changeProductComposites=false;
	final GenricServiceAsync async = GWT.create(GenricService.class);
	/**Date 19-3-2020 by Amol*/
	ObjectListBox<CompanyPayment> objPaymentMode;
	TextBox tbPaymentName,tbPaymentBankAC,tbPaymentPayableAt ,tbPaymentAccountName;
	boolean paymentTermForOmPest=false;
	 ArrayList<CompanyPayment> paymentModeList = new ArrayList<CompanyPayment>();
	 
	 /**
	  * @author Anil
	  * @since 02-05-2020
	  * Added turn around time for completing service(TAT)/service level agreement (SLA) for premium
	  */
	 
	TimeFormatBox dbTat;
	public static boolean complainTatFlag=false;
	
	/**
	 * @author Anil
	 * @since 29-06-2020
	 * Adding account manager drop down list
	 * Raised by Nitin Sir for pecopp
	 */
	ObjectListBox<Employee> olbAccountManager;
	 EmailIdPopup mailpopup=new EmailIdPopup();
	 String customerEmail="";
	 
	public Address customerServiceAddress;
	public Address customerBillingAddress;
	
   /**
	 * @author Vijay Date :- 28-01-2021
	 * Des :- if Free Material process config is active then stationed Service flag will true 
	 * or else it will false. if this flag is true then service will create with Automatic Scheduling 
	 */
	CheckBox chkStationedService;
	
	AppUtility appUtility = new AppUtility();
	
	/**
	 * @author Vijay  Date :- 28-03-2022 
	 * Des :- When this checkbox is true then service address will not print in pdf
	 */
	CheckBox cbdonotprintserviceAddress;
	
	ObjectListBox<String> invoicepdfNametoprint;
	
	ObjectListBox<Config> olbservicetype;//Ashwini Patil Date:29-02-2024 For orion

	TextBox tbZohoQuotID;
	public  ContractForm() {
		super();
		createGui();
		this.tbContractId.setEnabled(false);
		this.tbZohoQuotID.setEnabled(false);
		this.tbQuotationStatus.setEnabled(false);
		this.tbQuotationStatus.setText(Contract.CREATED);
		saleslineitemtable.connectToLocal();
		paymentTermsTable.connectToLocal();
		chargesTable.connectToLocal();
		prodTaxTable.connectToLocal();
//		addPayTerms.addClickHandler(this);
		//************************************************************************************
//		processLevelBar.btnLabels[10].setVisible(true);
		
		/**
		 * Date : 11-05-2017 By ANIL
		 */
		QuotationPresenter.custcount=0;
		/**
		 * End
		 */
		
		/** Date 01-09-2017 added by vijay for discount amount change handler ***/
		dbDiscountAmt.addChangeHandler(this);
		tbroundOffAmt.addChangeHandler(this);
		doAmount.addChangeHandler(this);
		dopercent.addChangeHandler(this);
		
		mailpopup.getLblCancel().addClickHandler(this);
		mailpopup.getLblOk().addClickHandler(this);
		productSrNo=0;
		
		/**
		 * Date : 06-10-2017 BY ANIL
		 */
		cbServiceWiseBilling.setValue(false);
		cbServiceWiseBilling.setTitle("Select this in case contract has fixed duration & fixed service schedule i.e. 1 year contract & 12 services, & to be billed to client only after service completion");
		/**
		 * End
		 */
		contractRenewFlag = false;
		/**
		 * nidhi
		 * 5-11-2018
		 */
		dbContractDate.addValueChangeHandler(this);
		Date date=new Date();
		dbContractDate.setValue(date);
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "MakeContractDateDisable")
				&&!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
					contractDateDisable=true;
					this.dbContractDate.setEnabled(false);
					}
		
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","SalesPersonRestriction")
				&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Sales"))
		{
			salesPersonRestrictionFlag=true;
			olbeSalesPerson.setValue(LoginPresenter.loggedInUser);
			olbeSalesPerson.setEnabled(false);
		
		}
		 if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "MakeCategoryAndTypeDisable")
				 &&!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
	   		 this.olbContractCategory.setEnabled(false);
	   		 this.olbContractType.setEnabled(false);
	   	  }
		 
		complainTatFlag=AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ComplainServiceWithTurnAroundTime");
		
//		
	}

	public ContractForm  (String[] processlevel, FormField[][] fields,
			FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
		
//		addPayTerms.addClickHandler(this);
	}

	//***********************************Variable Initialization********************************************//

	/**
	 * Method template to initialize the declared variables.
	 */
	@SuppressWarnings("unused")
	private void initalizeWidget()
	{
		
		objPaymentMode = new ObjectListBox<CompanyPayment>();
		loadPaymentMode();
		
		tbPaymentName = new TextBox();
		tbPaymentAccountName = new TextBox();
		tbPaymentBankAC = new TextBox();
		tbPaymentPayableAt = new TextBox();
		
		
		
		
		
		
		tbReferenceNumber=new TextBox();
		tbReferenceNumber.setTitle("Client PO  Number or Work Order Number");
		
		dbrefernceDate=new DateBoxWithYearSelector();
		dbrefernceDate.setTitle("Client PO  Date or Work Order Date");
		dbconfollowupdate=new DateBoxWithYearSelector();
		
		olbbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbBranch);
		olbbBranch.addChangeHandler(this);

		
		olbcPriority=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcPriority,Screen.CONTRACTPRIORITY);

		olbcPaymentMethods=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcPaymentMethods,Screen.PAYMENTMETHODS);

		paymenttable=new PaymentTable();
		paymenttable.connectToLocal();
		dbContractDate=new DateBoxWithYearSelector();
		/**Date 7-9-2019 by Amol added a Contract date as a Current date**/
		Date date=new Date();
		dbContractDate.setValue(date);
		
		
		
		

		
		
		
		
		tbLeadId=new TextBox();
		tbLeadId.setTitle("Lead from which order is created");
		tbQuotatinId=new TextBox();
		tbQuotatinId.setTitle("Quotation from which order is created");
		tbContractId=new TextBox();
		tbRefeRRedBy=new TextBox();
		tbRefeRRedBy.setTitle("Contract referral person");
		tbQuotationStatus=new TextBox();
		taDescription=new TextArea();
		taDescription.setTitle("Description or Remarks to be printed on contract");
		ucUploadTAndCs=new UploadComposite();
		ucUploadTAndCs.setTitle("Contract terms & conditions attachment");
		
		saleslineitemtable=new SalesLineItemTable();
		/**
		 * Date:29-03-2017 By ANIL
		 */
		saleslineitemtable.documentType="Contract";
		/**
		 * nidhi
		 * 19-10-2018
		 */
		saleslineitemtable.form = this;
		saleslineitemtable.quotationCount=0;
		personInfoComposite=AppUtility.customerInfoComposite(new Customer());
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","SalesPersonRestriction")
				&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Sales"))
		{
			
			salesPersonRestrictionFlag = true;
			olbeSalesPerson=new ObjectListBox<Employee>();
//			AppUtility.makeSalesPersonListBoxLive(olbeSalesPerson);
			olbeSalesPerson.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CONTRACT, "Sales Person");
			Timer t = new Timer() {
				
				@Override
				public void run() {

					makeSalesPersonEnable();
				}

				
			};t.schedule(3000);
		}
		else
		{
		olbeSalesPerson=new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbeSalesPerson);
		olbeSalesPerson.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CONTRACT, "Sales Person");
		}
	
		olbContractGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbContractGroup, Screen.CONTRACTGROUP);
		olbContractCategory=new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbContractCategory, Screen.CONTRACTCATEGORY);
		olbContractCategory.addChangeHandler(this);
		olbContractType=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbContractType,Screen.CONTRACTTYPE);
		
		olbApproverName=new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(olbApproverName,"Contract");
		
		this.tbContractId.setEnabled(false);
		this.tbLeadId.setEnabled(false);
		this.tbQuotatinId.setEnabled(false);
		tbdonotrenew=new TextBox();
		tbdonotrenew.setTitle("Renewal status i.e. renewed, not renewed etc");
		tbdonotrenew.setEnabled(false);
		
		dbContractStartDate=new DateBoxWithYearSelector();
		dbContractEndDate=new DateBoxWithYearSelector();
		dbContractStartDate.setEnabled(false);
		dbContractEndDate.setEnabled(false);
		
		personInfoComposite.getCustomerId().getHeaderLabel().setText("* Customer ID");
		personInfoComposite.getCustomerName().getHeaderLabel().setText("* Customer Name");
		personInfoComposite.getCustomerCell().getHeaderLabel().setText("* Customer Cell");
		
		tbremark=new TextBox();
		tbremark.setEnabled(false);
		
		prodInfoComposite=AppUtility.initiateServiceProductComposite(new SuperProduct());
		ibCreditPeriod=new IntegerBox();
		ibCreditPeriod.setTitle("Credit period to be given to customer for payment");
		ibdays=new IntegerBox();
		ibdays.addChangeHandler(this);
		ibdays.setTitle("Days in which you are expecting payment");
		dopercent=new DoubleBox();
		dopercent.setTitle("% of payment expecpted ");
		doAmount=new DoubleBox();		
		tbcomment=new TextBox();
		tbcomment.setTitle("Comments for the payment to be received i.e. Advance, First installemnet, Balance etc.");
		addPaymentTerms=new Button("ADD");
		addPaymentTerms.addClickHandler(this);
		paymentTermsTable=new PaymentTermsTable();
		chargesTable=new ProductChargesTable();
		addOtherCharges=new Button("+");
		addOtherCharges.addClickHandler(this);
		prodTaxTable=new ProductTaxesTable();
		dototalamt=new DoubleBox();
		dototalamt.setEnabled(false);
		doamtincltax=new DoubleBox();
		doamtincltax.setEnabled(false);
		donetpayamt=new DoubleBox();
		donetpayamt.setEnabled(false);
		baddproducts=new Button("ADD");
		baddproducts.addClickHandler(this);
		ibOldContract=new TextBox();
		ibOldContract.setTitle("Contract ID from which contract was renewed");		
		ibOldContract.setEnabled(false);
		cbRenewFlag=new CheckBox();
		cbRenewFlag.setValue(false);
		
		f_btnservice = new Button("Schedule");
		this.f_btnservice.setEnabled(true);
		serviceScheduleTable = new ServiceScheduleTable();
		scheduleEntity= new ServiceSchedule();
		
		
		tbTicketNumber=new TextBox();
		tbTicketNumber.setEnabled(false);
		
		tbPremisesUnderContract=new TextBox();
		tbPremisesUnderContract.setTitle("Scope of contract i.e. Garden, bedroom or particular AC ");
		
		
		chkbillingatBranch = new CheckBox();		
		chkbillingatBranch.addClickHandler(this);
		chkbillingatBranch.setValue(false);
		
		olbcNumberRange = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcNumberRange, Screen.NUMBERRANGE);
		olbcNumberRange.addChangeHandler(this);
		olbcNumberRange.setTitle("Define multiple number range based on number of companies i.e. GST / NON GST etc.");
		
		// *********vaishnavi**************
		olbPaymentTerms = new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbPaymentTerms,Screen.ADDPAYMENTTERMS);
		/**
		 * @author Vijay Chougule on 17-12-2019
		 */
		olbPaymentTerms.addChangeHandler(this);
		olbPaymentTerms.setTitle("Monthly (Partial First Month) - If billing start date is 25th then first bill will be from 25th to last day of month & remaining months will be calendar month i.e. 1st day of month to last day of month");
		
		addPayTerms = new Button("ADD");
		addPayTerms.addClickHandler(this);

		cbStartOfPeriod = new CheckBox();
		cbStartOfPeriod.addClickHandler(this);
		/**
		 * Date 26-12-2020 By Priyanka 
		 * Des - "Start Of Period" checkbox should be seleceted by default raised by Vaishnavi mam
		 */
		cbStartOfPeriod.setValue(true);
		cbStartOfPeriod.setTitle("Date from which payment data shall start");
		/**
		 * Date 28-09-2018 By Vijay 
		 * Des:- NBHC CCPM for every contract this must be true by default it does not allow to change for user
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC")){
			cbStartOfPeriod.setValue(true);
			cbStartOfPeriod.setEnabled(true);
		}
		/**
		 * ends here
		 */
		
//		btnclientAsset = new Button("Assets To Be Maintained");
//		btnclientAsset.addClickHandler(this);
//		this.btnclientAsset.setEnabled(true);
//
//		clientassetTable = new ClientAssetTable();
//		 clientassetTable.connectToLocal();
//		clientAssetEntity = new ClientSideAsset();
		
		// vijay
				chkservicewithBilling = new CheckBox();
				chkservicewithBilling.addClickHandler(this);
				chkservicewithBilling.setValue(false);
				chkservicewithBilling.setTitle("Select this in case contract has fixed duration but service schedule is not defined. Typically client request service on demand any time");
		tbCreatedBy = new TextBox();
		tbCreatedBy.setEnabled(false);
		tbCreatedBy.setValue(UserConfiguration.getUserconfig().getUser().getEmployeeName().trim());	
		
		tbRefNo2=new TextBox();
		tbRefNo2.setTitle("Any other reference information you want to maintain");
		
		btstackDetails=new Button("Stack Details");
		this.btstackDetails.setEnabled(true);
		
		olbTechnicainName = new ObjectListBox<Employee>();
//		olbTechnicainName.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CONTRACT, "Sales Person");
		olbTechnicainName.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CONTRACT, "Technician");
		
		olbTechnicainName.setTitle("Assign technician to all the services of contract");
		/*
		 *  nidhi
		 *   30-06-2017
		 */
		tbCustSegment = new TextBox();
		tbCustSegment.setTitle("Industry Segment of the client");
		/*
		 * end
		 */
		
		/*
		 * nidhi 
		 * 30-06-2017
		 * for segment edition status maintain
		 */
		boolean flag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC");
		System.out.println("Check status for nbhc -  "+flag);
		if(flag){
			this.tbCustSegment.setEnabled(false);
		}else{
			this.tbCustSegment.setEnabled(true);
		}
		/*
		 * nidhi
		 * 30-06-2017
		 */
		
		/**
		 * Date : 08-08-2017 BY ANIL
		 */
		dbPaymentDate=new DateBoxWithYearSelector();
		dbPaymentDate.setTitle("Date on which Payment is due");
		/**
		 * End
		 */
		
		/** Date 01-09-2017 added by vijay for discount amt round off total amt***/
		dbDiscountAmt = new DoubleBox();
		dbFinalTotalAmt = new DoubleBox();
		dbFinalTotalAmt.setEnabled(false);
		tbroundOffAmt = new TextBox();
		dbGrandTotalAmt = new DoubleBox();
		dbGrandTotalAmt.setEnabled(false);
		this.changeWidgets();
		
		/**
		 * Date : 06-10-2017 BY ANIL
		 */
		cbServiceWiseBilling=new CheckBox();
		cbServiceWiseBilling.setValue(false);
		cbServiceWiseBilling.addClickHandler(this);
		/**
		 * End
		 */
		 /** Date 06/2/2018 added by komal for consolidate price checkbox **/
		 cbConsolidatePrice = new CheckBox();
		 cbConsolidatePrice.setTitle("Select this in case you want to total price & not individual product prices. This is used for bundle pricing");
		 if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","ConsolidatePrice")){
			 cbConsolidatePrice.setValue(true);
		 }else{
			 cbConsolidatePrice.setValue(false);
		 }
		 /**
		  * end komal
		  */
		 
	 	/**
		 * Date 23-01-2018 By vijay
		 * for contract period for pcamb
		 */
		
		olbcontractPeriod = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcontractPeriod, Screen.CONTRACTPERIOD);
		
		/**
		 * ends here
		 */
		
		/**
		 *  nidhi
		 *  29-01-2018
		 *  for multiple contact list
		 */
		objContactList = new ObjectListBox<String>();
		objContactList.removeAllItems();
		objContactList.addItem("--Select--");
		objContactList.getElement().addClassName("txtalign");
		VerticalPanel hrPanel1=new VerticalPanel();
		/** Added by Priyanka
		 *  Des : change label to POC name to Contacts
		 */
		//InlineLabel label=new InlineLabel("POC Name");				//Updated By: Viraj Date:06-02-2019
		InlineLabel label=new InlineLabel("Contacts");
		/**end**/
		hrPanel1.add(label);
		hrPanel1.add(objContactList);
//		objContactList.setSize("145px", "12px");
		
		hrflowPanel=new HorizontalPanel();
		hrflowPanel.add(hrPanel1);
		/**
		 * end
		 */
		/**
		 * nidhi
		 * 6-09-2018
		 * 
		 */
		tbDonotRenewalMsg = new TextBox();
		tbDonotRenewalMsg.setTitle("Why contract was not renewed");
		if(paymentDateFlag){
			dbPaymentDate.setEnabled(false);
			ibdays.setEnabled(false);
			dopercent.setEnabled(false);
			doAmount.setEnabled(false);
			tbcomment.setEnabled(false);
		}
		
		
		poNumber=new TextBox();
		poDate=new DateBox();
		
		olbProductGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbProductGroup, Screen.PRODUCTGROUP);
		olbProductGroup.addChangeHandler(this);
		
		olbProductName=new ObjectListBox<ServiceProduct>();
		AppUtility.initializeServiceProduct(olbProductName);
		
		 if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","ChangeProductComposite")){
			 changeProductComposites=true;
			}
		dbTat=new TimeFormatBox();
		 
		olbAccountManager=new ObjectListBox<Employee>();
		olbAccountManager.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.CONTRACT, "Account Manager");
		
		chkStationedService = new CheckBox();
		chkStationedService.setValue(false);
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICEMODULE, AppConstants.PC_FREEMATERIAL)){
//			chkStationedService.setValue(true);
//		}

		cbdonotprintserviceAddress = new CheckBox();
		cbdonotprintserviceAddress.setValue(false);
		
		invoicepdfNametoprint = new ObjectListBox<String>();
		invoicepdfNametoprint.addItem("--SELECT--");
		invoicepdfNametoprint.addItem(AppConstants.STANDARDINVOICEFORMAT);
		invoicepdfNametoprint.addItem(AppConstants.NEWSTANDARDINVOICEFORMAT);
		invoicepdfNametoprint.addItem(AppConstants.CUSTOMIZEDINVOICEFORMAT);

		this.changeWidth(invoicepdfNametoprint, 200, Unit.PX);
		olbservicetype=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbservicetype, Screen.SERVICETYPE);
		tbZohoQuotID = new TextBox();

	}

	
	private void loadPaymentMode() {
		try {
			 GenricServiceAsync async =  GWT.create(GenricService.class);

			MyQuerry querry = new MyQuerry();
			
			
			Company c = new Company();
			Vector<Filter> filtervec = new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			
			
			querry.setQuerryObject(new CompanyPayment());

			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub

					System.out.println(" in side date load size"+result.size());
					Console.log("get result size of payment mode --" + result.size());
					objPaymentMode.clear();
					objPaymentMode.addItem("--SELECT--");
//					paymentModeList = new ArrayList<CompanyPayment>();
					if (result.size() == 0) {

					} else {
						for (SuperModel model : result) {
							CompanyPayment payList = (CompanyPayment) model;
							paymentModeList.add(payList);
						}
						
						if (paymentModeList.size() != 0) {
							objPaymentMode.setListItems(paymentModeList);
						}
						
						for(int i=1;i<objPaymentMode.getItemCount();i++){
							String paymentDet = objPaymentMode.getItemText(i);
							Console.log("SERVICE DETAILS : "+paymentDet);
						}	
					}
				
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
			

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * rohan added this method for setting value to sales person and and make it non editable
	 */
	private void makeSalesPersonEnable() {
	
		olbeSalesPerson.setValue(LoginPresenter.loggedInUser);
		olbeSalesPerson.setEnabled(false);
	}
	
	
	
	/**
	 * method template to create screen formfields
	 */
	@Override
	public void createScreen() {
		boolean hideCategory=true;
		boolean hideType=true;
		boolean hideTechnician=true;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "HideCategory")){
			hideCategory=false;
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "HideType")){
			hideType=false;
		}
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "HideTechnician")){
			hideTechnician=false;
		}
		
		/**
		 * @author Anil
		 * @since 29-06-2020
		 * Account manager mandatory flag
		 */
		boolean accMgrMandatory=false;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "AccountManagerMandatory")){
			accMgrMandatory=true;
		}
		
				

		initalizeWidget();
		
		boolean consolidateFlag = true;
		boolean serviceWiseFlag = true;
		//Ashwini Patil Date:29-12-2022 Requirement is to hide BranchWiseBillingOption 
		//now process config will have no effect as flag set to false by default
		boolean branchWiseFlag = false;//true
		int fieldCount =1;
		
		//Ashwini Patil Date:29-12-2022 commented below code
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "HideBranchWiseBillingOption") && 
//				!LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") &&
//				LoginPresenter.branchRestrictionFlag){
//			branchWiseFlag = false;
//		}else{
//			fieldCount++;
//		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "HideServiceWiseBillingOption") && 
				!LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") &&
				LoginPresenter.branchRestrictionFlag){
			serviceWiseFlag = false;
		}else{
			fieldCount++;
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "HideConsolidatedPrice") && 
				!LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") &&
				LoginPresenter.branchRestrictionFlag){
			consolidateFlag = false;
		}else{
			fieldCount++;
		}
		cbServiceWiseBilling.setVisible(serviceWiseFlag);
		cbConsolidatePrice.setVisible(consolidateFlag);
		chkbillingatBranch.setVisible(branchWiseFlag);
	
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","AddPoDetails")){
			addPoDetails=true;
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","MakePODetailsMandatory")){
			makePoDetailsMandatory=true;
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","HideTechnicianNameAndContractPeriod")){
			hideTechnicianAndContractPeriod=true;
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "ContractRenewalNewFormat")){
			paymentTermForOmPest=true;
		}
		
	//	setDisabled(paymentComposite);


		//Token to initialize the processlevel menus.
		
		//************rohan remove AppConstants.PRINTJOBCARD this button on 21/8/2015 *****************
		

//		/**
//		 * Date : 29-07-2017 BY ANIL
//		 * added view bill button on process level bar
//		 * Date :19-04-2018 BY ANIL
//		 * added view sales order button for HVAC/Rohan/Nitin Sir
//		 */
//		/**
//		 * Rohan added this code for NBHC for  removing mark complete button hide and only show to ADMIN role
//		 * Date :17/01/2017
//		 */
//		
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","OnlyForNBHC"))
//		{
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","HideOrShowCreateServicesButton"))
//			{
//				
//				/**
//				 * Date : 14-03-2017 By Anil
//				 * Added 3 buttons for printing FAR,FMR and CHR PDF
//				 * NBHC CCPM
//				 * Date : 28-05-2019 By ANIL
//				 * Added Button Delete duplicate services
//				 */
//				
//				this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLECONTRACT,
//						AppConstants.VIEWSERVICES,AppConstants.CANCELLATIONSUMMARY, AppConstants.NEW,AppConstants.CONTRACTRENEWAL,AppConstants.WORKORDER,AppConstants.CUSTOMERINTEREST,AppConstants.MATERIALREQUIREDREPORT,"Monthly Service Record",AppConstants.CREATESERVICE,
//						AppConstants.UPDATEPRICES,AppConstants.CANCELSERVICES,"CreateServices","FAR","FMR","CHR",ManageApprovals.SUBMIT
//						,AppConstants.VIEWBILL,AppConstants.COPYCONTRACT,AppConstants.CUSTOMERADDRESS,AppConstants.UPLOADTERMSANDCONDITION,ManageApprovals.APPROVALSTATUS
//						,AppConstants.VIEWSALESORDER,AppConstants.UPDATESERVICES,/** nidhi 18-05-2018 */ AppConstants.CONTRACTDISCOUNTINUED,"Delete duplicate services","UpdateServiceBranches",AppConstants.SERVICERECORD,"Update Account Manager"};
//			}
//			else
//			{
//				
//				/**
//				 * Date : 14-03-2017 By Anil
//				 * Added 3 buttons for printing FAR,FMR and CHR PDF
//				 * NBHC CCPM
//				 */
//				
//				this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLECONTRACT,
//						AppConstants.VIEWSERVICES,AppConstants.CANCELLATIONSUMMARY, AppConstants.NEW,AppConstants.CONTRACTRENEWAL
//						,AppConstants.WORKORDER,AppConstants.CUSTOMERINTEREST,AppConstants.MATERIALREQUIREDREPORT
//						,"Monthly Service Record",AppConstants.CREATESERVICE,AppConstants.UPDATEPRICES,AppConstants.CANCELSERVICES
//						,"FAR","FMR","CHR",ManageApprovals.SUBMIT,AppConstants.VIEWBILL,AppConstants.COPYCONTRACT
//						,AppConstants.CUSTOMERADDRESS,AppConstants.UPLOADTERMSANDCONDITION,ManageApprovals.APPROVALSTATUS
//						,AppConstants.VIEWSALESORDER,/** nidhi 18-05-2018 */ AppConstants.CONTRACTDISCOUNTINUED,"Delete duplicate services","UpdateServiceBranches",AppConstants.SERVICERECORD,"Update Account Manager",AppConstants.VIEWINVOICEPAYMENT};
//			}
//				
//		}
//		else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","PCAMBCustomization"))
//		{
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","HideOrShowCreateServicesButton"))
//			{
//				this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLECONTRACT,
//						AppConstants.VIEWSERVICES,AppConstants.CANCELLATIONSUMMARY, AppConstants.NEW
//						,AppConstants.CONTRACTRENEWAL,AppConstants.WORKORDER,AppConstants.CUSTOMERINTEREST
//						,AppConstants.MATERIALREQUIREDREPORT,AppConstants.CREATESERVICE,"rodentCountMonthlyReport"
//						,"GetServiceCard","GetServiceVoucher","CreateServices",ManageApprovals.SUBMIT
//						,AppConstants.VIEWBILL,AppConstants.COPYCONTRACT,AppConstants.CUSTOMERADDRESS
//						,AppConstants.UPLOADTERMSANDCONDITION,ManageApprovals.APPROVALSTATUS,AppConstants.VIEWSALESORDER
//						,AppConstants.UPDATESERVICES,"Delete duplicate services","Update Account Manager",AppConstants.VIEWINVOICEPAYMENT};
//			}
//			else
//			{
//				this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLECONTRACT,
//						AppConstants.VIEWSERVICES,AppConstants.CANCELLATIONSUMMARY, AppConstants.NEW,AppConstants.CONTRACTRENEWAL,AppConstants.WORKORDER,AppConstants.CUSTOMERINTEREST,AppConstants.MATERIALREQUIREDREPORT,AppConstants.CREATESERVICE,"rodentCountMonthlyReport","GetServiceCard","GetServiceVoucher",ManageApprovals.SUBMIT,AppConstants.VIEWBILL,AppConstants.COPYCONTRACT,AppConstants.CUSTOMERADDRESS,AppConstants.UPLOADTERMSANDCONDITION,ManageApprovals.APPROVALSTATUS,AppConstants.VIEWSALESORDER,"Delete duplicate services","Update Account Manager",AppConstants.VIEWINVOICEPAYMENT};
//			}
//				
//		}/**
//		 * Date 14-10-2017 
//		 * By Jayshree 
//		 * Changes are made for add the service card button
//		 * only for frends pest control
//		 */
//		
//		else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","OnlyForFriendsPestControl"))
//		{
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","HideOrShowCreateServicesButton"))
//			{
//				this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLECONTRACT,
//						
//						AppConstants.VIEWSERVICES,AppConstants.CANCELLATIONSUMMARY, AppConstants.NEW,AppConstants.CONTRACTRENEWAL,AppConstants.WORKORDER,AppConstants.CUSTOMERINTEREST,AppConstants.MATERIALREQUIREDREPORT,AppConstants.CREATESERVICE,"GetServiceCard","CreateServices",ManageApprovals.SUBMIT,AppConstants.VIEWBILL,AppConstants.COPYCONTRACT,AppConstants.CUSTOMERADDRESS,AppConstants.SERVICECARD,AppConstants.UPLOADTERMSANDCONDITION,ManageApprovals.APPROVALSTATUS,AppConstants.VIEWSALESORDER,AppConstants.UPDATESERVICES,"Delete duplicate services","Update Account Manager",AppConstants.VIEWINVOICEPAYMENT};
//			}
//			else
//			{
//				this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLECONTRACT,
//						AppConstants.VIEWSERVICES,AppConstants.CANCELLATIONSUMMARY, AppConstants.NEW,AppConstants.CONTRACTRENEWAL,AppConstants.WORKORDER,AppConstants.CUSTOMERINTEREST,AppConstants.MATERIALREQUIREDREPORT,AppConstants.CREATESERVICE,"GetServiceCard",ManageApprovals.SUBMIT,AppConstants.VIEWBILL,AppConstants.COPYCONTRACT,AppConstants.CUSTOMERADDRESS,AppConstants.SERVICECARD,AppConstants.UPLOADTERMSANDCONDITION,ManageApprovals.APPROVALSTATUS,AppConstants.VIEWSALESORDER,"Delete duplicate services","Update Account Manager",AppConstants.VIEWINVOICEPAYMENT};
//			}
//				
//		}
//		else
//		{
//			//"Update Services" use for bpt,"Monthly Service Record"
//			/**
//			 * Date : 09-04-2018 By  ANIL
//			 * Added update clients button which is visible to eva user  only
//			 * 
//			 */
//			
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","HideOrShowCreateServicesButton"))
//			{
//				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","OnlyForEVA")){
//					this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLECONTRACT,
//					AppConstants.VIEWSERVICES,AppConstants.CANCELLATIONSUMMARY, AppConstants.NEW,AppConstants.CONTRACTRENEWAL,AppConstants.WORKORDER,AppConstants.CUSTOMERINTEREST,AppConstants.MATERIALREQUIREDREPORT,AppConstants.CREATESERVICE,"CreateServices","Send SMS","Create EVA Services",ManageApprovals.SUBMIT,AppConstants.VIEWBILL,AppConstants.COPYCONTRACT,AppConstants.CUSTOMERADDRESS,AppConstants.UPLOADTERMSANDCONDITION,ManageApprovals.APPROVALSTATUS,AppConstants.LICENSETYPELIST.UPDATE_CLIENTS_LICENSE,AppConstants.VIEWSALESORDER,"Delete duplicate services","Update Account Manager",AppConstants.VIEWINVOICEPAYMENT};
//				}
//				else{
//				this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLECONTRACT,
//						AppConstants.VIEWSERVICES,AppConstants.CANCELLATIONSUMMARY, AppConstants.NEW,AppConstants.CONTRACTRENEWAL,AppConstants.WORKORDER,AppConstants.CUSTOMERINTEREST,AppConstants.MATERIALREQUIREDREPORT,AppConstants.CREATESERVICE,"CreateServices","Send SMS",ManageApprovals.SUBMIT,AppConstants.VIEWBILL,AppConstants.COPYCONTRACT,AppConstants.CUSTOMERADDRESS,AppConstants.UPLOADTERMSANDCONDITION,ManageApprovals.APPROVALSTATUS,AppConstants.VIEWSALESORDER,AppConstants.UPDATESERVICES,"Delete duplicate services","Update Account Manager",AppConstants.VIEWINVOICEPAYMENT};
//				}
//			}
//			else
//			{
//				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","OnlyForEVA")){
//					this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLECONTRACT,
//					AppConstants.VIEWSERVICES,AppConstants.CANCELLATIONSUMMARY, AppConstants.NEW,AppConstants.CONTRACTRENEWAL,AppConstants.WORKORDER,AppConstants.CUSTOMERINTEREST,AppConstants.MATERIALREQUIREDREPORT,AppConstants.CREATESERVICE,"CreateServices","Send SMS","Create EVA Services",ManageApprovals.SUBMIT,AppConstants.VIEWBILL,AppConstants.COPYCONTRACT,AppConstants.CUSTOMERADDRESS,AppConstants.UPLOADTERMSANDCONDITION,ManageApprovals.APPROVALSTATUS,AppConstants.LICENSETYPELIST.UPDATE_CLIENTS_LICENSE,AppConstants.VIEWSALESORDER,"Delete duplicate services","Update Account Manager",AppConstants.VIEWINVOICEPAYMENT,AppConstants.MAKERENEWABLE};
//				}
//				else{
//				this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLECONTRACT,
//						AppConstants.VIEWSERVICES,AppConstants.CANCELLATIONSUMMARY, AppConstants.NEW,AppConstants.CONTRACTRENEWAL,AppConstants.WORKORDER,AppConstants.CUSTOMERINTEREST,AppConstants.MATERIALREQUIREDREPORT,AppConstants.CREATESERVICE,"Send SMS",ManageApprovals.SUBMIT,AppConstants.VIEWBILL,AppConstants.COPYCONTRACT,AppConstants.CUSTOMERADDRESS,AppConstants.UPLOADTERMSANDCONDITION,ManageApprovals.APPROVALSTATUS,AppConstants.VIEWSALESORDER,"Delete duplicate services","Print Renewal Reminder","Email Renewal Reminder","Update Account Manager",AppConstants.VIEWINVOICEPAYMENT,AppConstants.MAKERENEWABLE};
//				}
//			}
		
		/**
		 * Date : 28-12-2020 By Priyanka
		 * DES-Change Navigation flow as a part of Simplification_Project raised by Nitin Sir.
		 * 
		 **/
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","OnlyForNBHC"))
		{
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","HideOrShowCreateServicesButton"))
			{
				
				/**
				 * Date : 14-03-2017 By Anil
				 * Added 3 buttons for printing FAR,FMR and CHR PDF
				 * NBHC CCPM
				 * Date : 28-05-2019 By ANIL
				 * Added Button Delete duplicate services
				 */	
				this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLECONTRACT,AppConstants.NEW
						                              ,AppConstants.VIEWSERVICES,AppConstants.VIEWBILL,AppConstants.VIEWLEAD,AppConstants.VIEWQUOTATION,AppConstants.CREATESERVICE,AppConstants.WORKORDER,AppConstants.VIEWSALESORDER
						                              ,AppConstants.CANCELLATIONSUMMARY, AppConstants.CONTRACTRENEWAL,AppConstants.CUSTOMERINTEREST,AppConstants.MATERIALREQUIREDREPORT,"Monthly Service Record",AppConstants.UPDATEPRICES,AppConstants.CANCELSERVICES,"CreateServices","FAR","FMR","CHR"
						                              ,ManageApprovals.SUBMIT,AppConstants.COPYCONTRACT,AppConstants.CUSTOMERADDRESS,AppConstants.UPLOADTERMSANDCONDITION
						                              ,ManageApprovals.APPROVALSTATUS,AppConstants.UPDATESERVICES, AppConstants.CONTRACTDISCOUNTINUED,"Delete duplicate services","UpdateServiceBranches",AppConstants.SERVICERECORD,"Update Account Manager",AppConstants.UPDATETECHNICIANNAME,AppConstants.CONTRACTRESET,"Copy",AppConstants.DELETECLIENTLICENSE,AppConstants.VIEWCOMPLAIN, AppConstants.UpdateSalesPerson,AppConstants.AddCustomerBranches,AppConstants.markcontractasrenewed};//Ashwini Patil Date:9-09-2022 added "copy"
			} 
			else
			{
				
				/**
				 * Date : 14-03-2017 By Anil
				 * Added 3 buttons for printing FAR,FMR and CHR PDF
				 * NBHC CCPM
				 */
				
				this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLECONTRACT,AppConstants.NEW,AppConstants.VIEWSERVICES,
						                              AppConstants.VIEWBILL,AppConstants.VIEWLEAD,AppConstants.VIEWQUOTATION,AppConstants.VIEWINVOICEPAYMENT,AppConstants.SERVICERECORD,AppConstants.WORKORDER,
						                              AppConstants.VIEWSALESORDER,AppConstants.CREATESERVICE,AppConstants.UPDATEPRICES,AppConstants.CANCELSERVICES,AppConstants.CANCELLATIONSUMMARY,
						                              AppConstants.CONTRACTRENEWAL,AppConstants.CUSTOMERINTEREST,AppConstants.MATERIALREQUIREDREPORT,"Monthly Service Record","FAR","FMR","CHR",ManageApprovals.SUBMIT,AppConstants.COPYCONTRACT,AppConstants.CUSTOMERADDRESS,AppConstants.UPLOADTERMSANDCONDITION,
						                              ManageApprovals.APPROVALSTATUS,AppConstants.CONTRACTDISCOUNTINUED,"Delete duplicate services","UpdateServiceBranches",AppConstants.SERVICERECORD,"Update Account Manager",AppConstants.UPDATETECHNICIANNAME,AppConstants.CONTRACTRESET,"Copy",AppConstants.DELETECLIENTLICENSE,AppConstants.VIEWCOMPLAIN, AppConstants.UpdateSalesPerson,AppConstants.markcontractasrenewed};//Ashwini Patil Date:9-09-2022 added "copy"
				
			}
		}
		/**
		 *   Date : 22-02-2021  Added By : Priyanka
		 *   Des : Add "CONTRACTDISCOUNTINUED " Button to all client as per Requiremnet raised by Vaishnavi Mam.
		 */
		else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","PCAMBCustomization"))
		{
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","HideOrShowCreateServicesButton"))
			{
				this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLECONTRACT, AppConstants.NEW,AppConstants.VIEWSERVICES,
						                              AppConstants.VIEWBILL,AppConstants.VIEWLEAD,AppConstants.VIEWQUOTATION,AppConstants.VIEWINVOICEPAYMENT,AppConstants.CREATESERVICE,AppConstants.WORKORDER,AppConstants.VIEWSALESORDER,
						                              AppConstants.CANCELLATIONSUMMARY,AppConstants.CONTRACTRENEWAL,AppConstants.CUSTOMERINTEREST,AppConstants.CONTRACTDISCOUNTINUED,"rodentCountMonthlyReport","GetServiceCard","GetServiceVoucher",
						                              "CreateServices",ManageApprovals.SUBMIT,AppConstants.COPYCONTRACT,AppConstants.CUSTOMERADDRESS,AppConstants.UPLOADTERMSANDCONDITION,AppConstants.UPDATESERVICES,
						                              "Delete duplicate services","Print Renewal Reminder","Email Renewal Reminder","Update Account Manager",AppConstants.MATERIALREQUIREDREPORT,ManageApprovals.APPROVALSTATUS,AppConstants.SERVICERECORD,AppConstants.UPDATETECHNICIANNAME,AppConstants.CONTRACTRESET,"Copy",AppConstants.DELETECLIENTLICENSE,AppConstants.VIEWCOMPLAIN, AppConstants.UpdateSalesPerson,AppConstants.AddCustomerBranches,AppConstants.markcontractasrenewed};
			}
			else
			{
				this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLECONTRACT,AppConstants.NEW,AppConstants.VIEWSERVICES,
						                               AppConstants.VIEWBILL,AppConstants.VIEWLEAD,AppConstants.VIEWQUOTATION,AppConstants.VIEWINVOICEPAYMENT,AppConstants.CANCELLATIONSUMMARY,AppConstants.CREATESERVICE,AppConstants.WORKORDER,
						                               AppConstants.VIEWSALESORDER,AppConstants.CONTRACTRENEWAL,AppConstants.CUSTOMERINTEREST,AppConstants.CONTRACTDISCOUNTINUED,"rodentCountMonthlyReport","GetServiceCard","GetServiceVoucher",
						                               ManageApprovals.SUBMIT,AppConstants.COPYCONTRACT,AppConstants.CUSTOMERADDRESS,AppConstants.UPLOADTERMSANDCONDITION,"Delete duplicate services","Print Renewal Reminder","Email Renewal Reminder",
						                               "Update Account Manager",AppConstants.MATERIALREQUIREDREPORT,ManageApprovals.APPROVALSTATUS,AppConstants.SERVICERECORD,AppConstants.UPDATETECHNICIANNAME,AppConstants.CONTRACTRESET,"Copy",AppConstants.DELETECLIENTLICENSE,AppConstants.VIEWCOMPLAIN, AppConstants.UpdateSalesPerson,AppConstants.AddCustomerBranches,AppConstants.markcontractasrenewed};
			}
				
		}/**
		 * Date 14-10-2017 
		 * By Jayshree 
		 * Changes are made for add the service card button
		 * only for frends pest control
		 */
		
		else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","OnlyForFriendsPestControl"))
		{
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","HideOrShowCreateServicesButton"))
			{
				this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLECONTRACT,AppConstants.NEW,AppConstants.VIEWSERVICES,
													  AppConstants.VIEWBILL,AppConstants.VIEWLEAD,AppConstants.VIEWQUOTATION,AppConstants.VIEWINVOICEPAYMENT,AppConstants.CREATESERVICE,AppConstants.WORKORDER,AppConstants.VIEWSALESORDER,
						                              AppConstants.CANCELLATIONSUMMARY,AppConstants.CONTRACTRENEWAL,AppConstants.CUSTOMERINTEREST,AppConstants.CONTRACTDISCOUNTINUED,"GetServiceCard","CreateServices",
						                              ManageApprovals.SUBMIT,AppConstants.COPYCONTRACT,AppConstants.CUSTOMERADDRESS,AppConstants.SERVICECARD,AppConstants.UPLOADTERMSANDCONDITION,
						                              AppConstants.UPDATESERVICES,"Delete duplicate services","Print Renewal Reminder","Email Renewal Reminder","Update Account Manager",AppConstants.MATERIALREQUIREDREPORT,ManageApprovals.APPROVALSTATUS,AppConstants.SERVICERECORD,AppConstants.UPDATETECHNICIANNAME,AppConstants.CONTRACTRESET,"Copy",AppConstants.DELETECLIENTLICENSE,AppConstants.VIEWCOMPLAIN, AppConstants.UpdateSalesPerson,AppConstants.AddCustomerBranches,AppConstants.markcontractasrenewed};
			}
			else
			{
				this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLECONTRACT,AppConstants.NEW,AppConstants.VIEWSERVICES,
													  AppConstants.VIEWBILL,AppConstants.VIEWLEAD,AppConstants.VIEWQUOTATION,AppConstants.VIEWINVOICEPAYMENT,AppConstants.CREATESERVICE,AppConstants.WORKORDER,AppConstants.VIEWSALESORDER,					
						                              AppConstants.CANCELLATIONSUMMARY,AppConstants.CONTRACTRENEWAL,AppConstants.CUSTOMERINTEREST,AppConstants.CONTRACTDISCOUNTINUED,"GetServiceCard",ManageApprovals.SUBMIT,
						                              AppConstants.COPYCONTRACT,AppConstants.CUSTOMERADDRESS,AppConstants.SERVICECARD,AppConstants.UPLOADTERMSANDCONDITION,"Delete duplicate services",
						                              "Update Account Manager","Print Renewal Reminder","Email Renewal Reminder",AppConstants.MATERIALREQUIREDREPORT,ManageApprovals.APPROVALSTATUS,AppConstants.SERVICERECORD,AppConstants.UPDATETECHNICIANNAME,AppConstants.CONTRACTRESET,"Copy",AppConstants.DELETECLIENTLICENSE,AppConstants.VIEWCOMPLAIN, AppConstants.UpdateSalesPerson,AppConstants.AddCustomerBranches,AppConstants.markcontractasrenewed};
			}
				
		}
		else
		{
			//"Update Services" use for bpt,"Monthly Service Record"
			/**
			 * Date : 09-04-2018 By  ANIL
			 * Added update clients button which is visible to eva user  only
			 * 
			 */
			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","HideOrShowCreateServicesButton"))
			{
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","OnlyForEVA")){
					this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLECONTRACT,AppConstants.NEW,AppConstants.VIEWSERVICES,
														   AppConstants.VIEWBILL,AppConstants.VIEWLEAD,AppConstants.VIEWQUOTATION,AppConstants.VIEWINVOICEPAYMENT,AppConstants.CREATESERVICE,AppConstants.WORKORDER,AppConstants.VIEWSALESORDER,
														   AppConstants.UPLOADTERMSANDCONDITION,AppConstants.CUSTOMERADDRESS,AppConstants.LICENSETYPELIST.UPDATE_CLIENTS_LICENSE,AppConstants.CANCELLATIONSUMMARY, 
														   AppConstants.CONTRACTRENEWAL,AppConstants.CUSTOMERINTEREST,AppConstants.CONTRACTDISCOUNTINUED,"CreateServices",ManageApprovals.SUBMIT,AppConstants.COPYCONTRACT,
														   "Delete duplicate services","Update Account Manager","Print Renewal Reminder","Email Renewal Reminder",AppConstants.MATERIALREQUIREDREPORT,ManageApprovals.APPROVALSTATUS,AppConstants.SERVICERECORD,AppConstants.UPDATESERVICEVALUE,AppConstants.UPDATETECHNICIANNAME,AppConstants.CONTRACTRESET,"Copy",AppConstants.DELETECLIENTLICENSE,AppConstants.VIEWCOMPLAIN, AppConstants.UpdateSalesPerson,AppConstants.AddCustomerBranches,AppConstants.markcontractasrenewed};				}
				//Ashwini Patil Date:10-03-2023 removed option as per Nitin sir's instruction "Create EVA Services"
				else{
				this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLECONTRACT,AppConstants.NEW,AppConstants.VIEWSERVICES,
													  AppConstants.VIEWBILL,AppConstants.VIEWLEAD,AppConstants.VIEWQUOTATION,AppConstants.VIEWINVOICEPAYMENT,AppConstants.CREATESERVICE,AppConstants.WORKORDER,AppConstants.VIEWSALESORDER,
													  AppConstants.CANCELLATIONSUMMARY, AppConstants.CONTRACTRENEWAL,AppConstants.CUSTOMERINTEREST,AppConstants.CONTRACTDISCOUNTINUED,"CreateServices",
													  ManageApprovals.SUBMIT,AppConstants.COPYCONTRACT,AppConstants.CUSTOMERADDRESS,AppConstants.UPLOADTERMSANDCONDITION,
													  AppConstants.UPDATESERVICES,"Delete duplicate services","Update Account Manager","Print Renewal Reminder","Email Renewal Reminder",AppConstants.MATERIALREQUIREDREPORT,ManageApprovals.APPROVALSTATUS,AppConstants.SERVICERECORD,AppConstants.UPDATESERVICEVALUE,AppConstants.UPDATETECHNICIANNAME,AppConstants.CONTRACTRESET,"Copy",AppConstants.DELETECLIENTLICENSE,AppConstants.VIEWCOMPLAIN, AppConstants.UpdateSalesPerson,AppConstants.AddCustomerBranches,AppConstants.markcontractasrenewed};				}
			}
			else
			{
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","OnlyForEVA")){
					this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLECONTRACT,AppConstants.NEW,AppConstants.VIEWSERVICES,AppConstants.VIEWBILL,
							                              AppConstants.VIEWLEAD,AppConstants.VIEWQUOTATION,AppConstants.VIEWINVOICEPAYMENT,AppConstants.CREATESERVICE,AppConstants.WORKORDER,AppConstants.VIEWSALESORDER,
							                              AppConstants.UPLOADTERMSANDCONDITION,AppConstants.CUSTOMERADDRESS,"Update Account Manager",AppConstants.LICENSETYPELIST.UPDATE_CLIENTS_LICENSE,
							                              AppConstants.MATERIALREQUIREDREPORT,ManageApprovals.APPROVALSTATUS,AppConstants.CANCELLATIONSUMMARY,AppConstants.CONTRACTRENEWAL,ManageApprovals.SUBMIT,		
							                              AppConstants.CUSTOMERINTEREST,AppConstants.CONTRACTDISCOUNTINUED,"CreateServices",AppConstants.COPYCONTRACT,"Delete duplicate services","Print Renewal Reminder","Email Renewal Reminder",AppConstants.MAKERENEWABLE,AppConstants.SERVICERECORD,AppConstants.UPDATESERVICEVALUE,AppConstants.UPDATETECHNICIANNAME,AppConstants.CONTRACTRESET,"Copy",AppConstants.DELETECLIENTLICENSE,AppConstants.VIEWCOMPLAIN, AppConstants.UpdateSalesPerson,AppConstants.AddCustomerBranches,AppConstants.markcontractasrenewed};			
					//Ashwini Patil Date:10-03-2023 removed option as per Nitin sir's instruction "Create EVA Services"
					
				}
				else{
				this.processlevelBarNames=new String[]{ManageApprovals.APPROVALREQUEST,ManageApprovals.CANCELAPPROVALREQUEST,AppConstants.CANCLECONTRACT,AppConstants.NEW,AppConstants.VIEWSERVICES,AppConstants.VIEWBILL,AppConstants.VIEWLEAD,AppConstants.VIEWQUOTATION,
													  AppConstants.VIEWINVOICEPAYMENT,AppConstants.CREATESERVICE,AppConstants.WORKORDER,AppConstants.VIEWSALESORDER,AppConstants.UPLOADTERMSANDCONDITION,AppConstants.CUSTOMERADDRESS,
													  "Update Account Manager",AppConstants.MATERIALREQUIREDREPORT,ManageApprovals.APPROVALSTATUS,AppConstants.CANCELLATIONSUMMARY,AppConstants.CONTRACTRENEWAL, AppConstants.CUSTOMERINTEREST,
													  AppConstants.CONTRACTDISCOUNTINUED,ManageApprovals.SUBMIT,AppConstants.COPYCONTRACT,"Delete duplicate services","Print Renewal Reminder","Email Renewal Reminder",AppConstants.MAKERENEWABLE,AppConstants.SERVICERECORD,AppConstants.UPDATESERVICEVALUE,AppConstants.UPDATETECHNICIANNAME,AppConstants.CONTRACTRESET,"Copy",AppConstants.DELETECLIENTLICENSE,AppConstants.VIEWCOMPLAIN, AppConstants.UpdateSalesPerson,AppConstants.AddCustomerBranches,AppConstants.markcontractasrenewed};}
			}

			
			

		}				
		/**
		 * 
		 * END 
		 **/
		//==========,"Update Taxes"
		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		
		String mainScreenLabel="Contract Details";
		if(contractObject!=null&&contractObject.getCount()!=0){
			mainScreenLabel=contractObject.getCount()+" "+"/"+" "+contractObject.getStatus()+" "+"/"+" "+AppUtility.parseDate(contractObject.getCreationDate());
		}
		
		//fgroupingCustomerInformation.setLabel(mainScreenLabel);
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fgroupingCustomerInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
		fbuilder = new FormFieldBuilder("",personInfoComposite);
		FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		//Ashwini Patil Date:28-02-2023
		FormField ftbLeadId;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","makeLeadIdMandatory")
				&& !LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Admin")){
			fbuilder = new FormFieldBuilder("* Lead ID",tbLeadId);
			ftbLeadId= fbuilder.setMandatory(true).setMandatoryMsg("Lead Id is Mandatory").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Lead ID",tbLeadId);
			ftbLeadId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		//Ashwini Patil Date:28-02-2023
		FormField ftbQuotatinId;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","makeQuotationIdMandatory")
				&& !LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Admin")){
			fbuilder = new FormFieldBuilder("* Quotation ID",tbQuotatinId);
			ftbQuotatinId= fbuilder.setMandatory(true).setMandatoryMsg("Quotation Id is Mandatory").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Quotation ID",tbQuotatinId);
			ftbQuotatinId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		fbuilder = new FormFieldBuilder("Order ID",tbContractId);
		FormField ftbContractId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 * Date : 18-11-2017 BY ANIL
		 * making reference number 1 mandatory through process configuration
		 */
		FormField ftbReferenceNumber;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "MakeReferenceNumberMandatory")){
			fbuilder = new FormFieldBuilder("* Reference Number 1",tbReferenceNumber);
			ftbReferenceNumber= fbuilder.setMandatory(true).setMandatoryMsg("Reference Number is mandatory.").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Reference Number 1",tbReferenceNumber);
			ftbReferenceNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		/**
		 * End
		 */
		paymentDateFlag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ContractPaymentNonEditable");
		if(paymentDateFlag){
			dbPaymentDate.setEnabled(false);
			ibdays.setEnabled(false);
			dopercent.setEnabled(false);
			doAmount.setEnabled(false);
			tbcomment.setEnabled(false);
		}
		fbuilder = new FormFieldBuilder("Referred By",tbRefeRRedBy);
		FormField ftbRefeRRedBy= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Reference Date",dbrefernceDate);
		FormField fdbrefernceDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Contract Start Date",dbContractStartDate);
		FormField fdbContractStartDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Contract End Date",dbContractEndDate);
		FormField fdbContractEndDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Previous Order ID",ibOldContract);
		FormField fibOldContract= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Renewal Status",tbdonotrenew);
		FormField ftbdonotrenew= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		//fbuilder = new FormFieldBuilder("Valid Untill",validuntil);
		//FormField fvaliduntil= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDocuments=fbuilder.setlabel("Attachment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		FormField fucUploadTAndCs=null;
		if(addPoDetails){
			fbuilder = new FormFieldBuilder("Upload PO Document",ucUploadTAndCs);
			fucUploadTAndCs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		}else {		
			fbuilder = new FormFieldBuilder("Upload T&Cs",ucUploadTAndCs);
			fucUploadTAndCs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		}
		
		if(makePoDetailsMandatory){
			fbuilder = new FormFieldBuilder("* Upload PO Document",ucUploadTAndCs);
			fucUploadTAndCs= fbuilder.setMandatory(true).setMandatoryMsg("Upload PO Document is Mandatory").setRowSpan(0).setColSpan(4).build();
		}else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","MakeTermsUploadMandatory")){
			fbuilder = new FormFieldBuilder("* Upload T&Cs",ucUploadTAndCs);
			fucUploadTAndCs= fbuilder.setMandatory(true).setMandatoryMsg("Upload Terms and conditions document!").setRowSpan(0).setColSpan(4).build();
		}else {
			fbuilder = new FormFieldBuilder("Upload T&Cs",ucUploadTAndCs);
			fucUploadTAndCs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		}
		
		
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingGeneralInformation=fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		FormField fgroupingSalesInformation=fbuilder.setlabel("Classification").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Status",tbQuotationStatus);
		FormField ftbQuotationStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Priority",olbcPriority);
		FormField folbcPriority= fbuilder.setMandatory(false).setMandatoryMsg("Priority is Mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Branch",olbbBranch);
		FormField folbbBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory").setRowSpan(0).setColSpan(0).build();
		/**@Sheetal:01-02-2022
        Des : Making Sales person non mandatory,requirement by UDS Water**/
		fbuilder = new FormFieldBuilder("Sales Person",olbeSalesPerson);
		FormField folbeSalesPerson= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Payment Method",olbcPaymentMethods);
		FormField folbcPaymentMethods= fbuilder.setMandatory(false).setMandatoryMsg("Payment Methods is Mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingDescription=fbuilder.setlabel("Description").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Description (Max 500 characters)",taDescription);
		FormField ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingProducts=fbuilder.setlabel("Product").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",paymenttable.getTable());
		FormField fpaymenttable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		/** date 06/03/2018 added by komal for nbhc **/
		FormField folbContractGroup;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "makeGroupMandatory")){
			fbuilder = new FormFieldBuilder("* Contract Group",olbContractGroup);
			folbContractGroup= fbuilder.setMandatory(true).setMandatoryMsg("Contract Group is Mandatory").setRowSpan(0).setColSpan(0).build();
			
		}else{
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "RenameGroupToType")){
				fbuilder = new FormFieldBuilder("Contract Type",olbContractGroup);
				folbContractGroup= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			}else{
				fbuilder = new FormFieldBuilder("Contract Group",olbContractGroup);
				folbContractGroup= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			}
		}
		FormField folbContractType;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "makeTypeMandatory")){
			fbuilder = new FormFieldBuilder("* Contract Type",olbContractType);
			folbContractType= fbuilder.setMandatory(true).setMandatoryMsg("Contract Type is Mandatory").setRowSpan(0).setColSpan(0).build();
			
		}else{
			fbuilder = new FormFieldBuilder("Contract Type",olbContractType);
			folbContractType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).setVisible(hideType).build();			
		}
		/** 
		 * end komal
		 */
		/** new makecategorymandatory process type added by komal on 17.4.2018 for orion **/
		FormField folbContractCategory ;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC") || AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","makeCategoryMandatory")){
		fbuilder = new FormFieldBuilder("* Contract Category",olbContractCategory);
		 folbContractCategory= fbuilder.setMandatory(true).setMandatoryMsg("Contract Category is Mandatory").setRowSpan(0).setColSpan(0).build();
		}else{
		fbuilder = new FormFieldBuilder("Contract Category",olbContractCategory);
		 folbContractCategory= fbuilder.setMandatory(false).setMandatoryMsg("Contract Category is Mandatory").setVisible(hideCategory).setRowSpan(0).setColSpan(0).build();
		}
		fbuilder = new FormFieldBuilder("* Approver Name",olbApproverName);
		FormField folbApproverName= fbuilder.setMandatory(true).setMandatoryMsg("Approver Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPaymentTermsDetails=fbuilder.setlabel("Payment Terms").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Days",ibdays);
		FormField fibdays= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Percent",dopercent);		
		FormField fdopercent= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Amount",doAmount);		
		FormField fdoAmount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Comment",tbcomment);
		
		FormField ftbcomment= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Approve/Rejection Remark",tbremark);
		FormField ftbremark= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",addPaymentTerms);
		FormField faddPaymentTerms= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",paymentTermsTable.getTable());
		FormField fpaymentTermsTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fblankgroup=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder();
		FormField fblankgroupthree=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		fbuilder = new FormFieldBuilder();
		FormField fblankgroupone=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",prodInfoComposite);
		FormField fprodInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		fbuilder = new FormFieldBuilder("",baddproducts);
		FormField fbaddproducts= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",saleslineitemtable.getTable());
		FormField fsaleslineitemtable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",prodTaxTable.getTable());
		FormField fprodTaxTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		//fbuilder = new FormFieldBuilder("Total Amount",doAmtExcludingTax);
		//FormField fdoAmtExcludingTax= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Total Amount",doamtincltax);
		FormField fdoAmtIncludingTax= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("* Contract Date",dbContractDate);
		FormField fdbContractDate= fbuilder.setMandatory(true).setMandatoryMsg("Contract Date is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Credit Period  (Days)",ibCreditPeriod);
		FormField fibCreditPeriod= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total Amount",dototalamt);
		FormField fpayCompTotalAmt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",addOtherCharges);
		FormField faddOtherCharges= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("",chargesTable.getTable());
		FormField fchargesTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		fbuilder = new FormFieldBuilder("Net Payable",donetpayamt);
		FormField fpayCompNetPay= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("",f_btnservice);
		FormField fbtnservice= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Ticket Number",tbTicketNumber);
		FormField ftbTicketNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Premises Under Contract",tbPremisesUnderContract);
		FormField ftbPremisesUnderContract= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Generate branchwise billing",chkbillingatBranch);
		FormField fchkbillingatBranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		/** Date 02 Oct 2017  Number range mandatory with configuration added by vijay *********/
		/**
		 * Date : 30-10-2017 BY ANIL
		 * New Process Type:MakeNumberRangeMandatory
		 */
	
		FormField folbcNumberRange;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange", "MakeNumberRangeMandatory")){
			fbuilder = new FormFieldBuilder("* Number Range",olbcNumberRange);									//Updated By:Viraj Date: 13-02-2019 Description: Added space between mandatory(*) and Number Range 	
			folbcNumberRange= fbuilder.setMandatory(true).setMandatoryMsg("Number Range is Mandatory!").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Number Range",olbcNumberRange);
			folbcNumberRange= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		/*
		 * Ashwini Patil
		 * Date:29-05-2024
		 * Ultima search want to map number range directly from branch so they want this field read only
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange", "MakeNumberRangeReadOnly"))
			olbcNumberRange.setEnabled(false);
		
		
		// **********vaishnavi***********
		fbuilder = new FormFieldBuilder("Payment Terms", olbPaymentTerms);
		FormField folbPaymentTerms = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("", addPayTerms);
		FormField faddPayTerms = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Start Of Period", cbStartOfPeriod);
		FormField fcbStartOfPeriod = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		// ***********vaishnavi*****************
//		fbuilder = new FormFieldBuilder("", btnclientAsset);
//		FormField fbtnclientAsset = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
////				
////				
		fbuilder = new FormFieldBuilder("Rate Contract",chkservicewithBilling);
		FormField fchkservicewithBilling= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();		
	
		
		
		fbuilder = new FormFieldBuilder("Created By", tbCreatedBy);
		FormField ftbCreatedBy = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Reference Number 2",tbRefNo2);
		FormField ftbRefNo2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		
		/**
		 * Rohan added this code for NBHC for  removing mark complete button hide and only show to ADMIN role
		 * Date :17/01/2017
		 */
		
		FormField fbtstackDetails = null;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","OnlyForNBHC") )
		{
			fbuilder = new FormFieldBuilder("",btstackDetails);
			fbtstackDetails= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		
		fbuilder = new FormFieldBuilder("Technician Name",olbTechnicainName);
		FormField folbTechnicainName= fbuilder.setMandatory(false).setRowSpan(0).setVisible(hideTechnician).setColSpan(0).build();
		
		/*
		 *  nidhi
		 *  30-06-2017
		 */
		
		fbuilder =  new FormFieldBuilder("Segment",tbCustSegment);
		FormField fotbsegment = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/*
		 *  end
		 */
		
		/**
		 *Date : 08-08-2017 BY ANIL
		 */
		
		fbuilder =  new FormFieldBuilder("Payment Date",dbPaymentDate);
		FormField fdbPaymentDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**
		 *  end
		 */
		
		/**
		 * Date:05/06/2021 By Priyanka Bhagwat.
		 * Adding Followup date for kernal pest.
		 */
		
		fbuilder = new FormFieldBuilder("Follow-up Date", dbconfollowupdate);
		FormField fdbleaddate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/** Date 01-09-2017 added by vijay for discount amount final total amount and round off ***/
		
		fbuilder = new FormFieldBuilder("Discount Amount", dbDiscountAmt);
		FormField fdbDiscountAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total Amount", dbFinalTotalAmt);
		FormField fdbFinalTotalAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Round Off", tbroundOffAmt);
		FormField fdbroundOffAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Total Amount", dbGrandTotalAmt);
		FormField fdbGrandTotalAmt = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Generate service wise billing",cbServiceWiseBilling);
		FormField fcbServiceWiseBilling= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Consolidate Price",cbConsolidatePrice);
		FormField fcbConsolidatePrice= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		
	
		/**
		 * Date : 17/2/2018 BY MANISHA
		 * Change contract period name as salesource as per sonu's requirement..!!
		 */
		FormField folbcontractPeriod;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForOrionPestSolutions")){
			fbuilder = new FormFieldBuilder("* Sale Source",olbcontractPeriod);
		     folbcontractPeriod = fbuilder.setMandatory(true).setMandatoryMsg("Sales Source is Mandatory").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Contract Period",olbcontractPeriod);
		    folbcontractPeriod = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		/**
		 * End
		 **/
		/**
		 * nidhi
		 * 29-01-2018
		 * get multiple list of contact details of customer
		 */
		fbuilder = new FormFieldBuilder(" ",hrflowPanel);
		FormField fobjContactList= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/***Date 29-8-2019 by Amol added Po date Po Number Raised by Rahul for Orkin*****/
		
		
		FormField fpoNumber=null;
		FormField fpoDate=null;
		if(makePoDetailsMandatory){
			fbuilder = new FormFieldBuilder("* PO Number",poNumber);
			fpoNumber= fbuilder.setMandatory(true).setMandatoryMsg("PO Number is Mandatory").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("* PO Date",poDate);
			fpoDate= fbuilder.setMandatory(true).setMandatoryMsg("PO Date is Mandatory").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("PO Number",poNumber);
			fpoNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("PO Date",poDate);
			fpoDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		
		
		
		
		
		
		
		
//		fbuilder = new FormFieldBuilder("Contract Period",olbcontractPeriod);
//		FormField folbcontractPeriod = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		
		
		
		FormField[] formCh = new FormField[fieldCount];
		int flagcount =0;
		formCh[flagcount] = (fchkservicewithBilling);
		if(serviceWiseFlag){
			flagcount++;
			formCh[flagcount] = fcbServiceWiseBilling;
		}
		
		if(branchWiseFlag){
			flagcount++;
			formCh[flagcount] = fchkbillingatBranch;
		}
		
		if(consolidateFlag){
			flagcount++;
			formCh[flagcount] = fcbConsolidatePrice;
		}
		fbuilder = new FormFieldBuilder("Non Renewal Remark",tbDonotRenewalMsg);
		FormField ftbDonotRenewalMsg= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Product Group",olbProductGroup);
		FormField folbProductGroup= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Service Product",olbProductName);
		FormField folbProductName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Bank Name",tbPaymentName);
		FormField ftbPaymentName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Account Number",tbPaymentBankAC);
		FormField ftbPaymentBankAC= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Payable At",tbPaymentPayableAt);
		FormField ftbPaymentPayableAt= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Bank Branch",tbPaymentAccountName);
		FormField ftbPaymentAccountName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Payment Mode",objPaymentMode);
		FormField fobjPaymentMode= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("TAT/SLA(In hours)",dbTat.getTimePanel());
		FormField fdbTat= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		String accMgrLabel="Account Manager";
		if(accMgrMandatory){
			accMgrLabel="* Account Manager";
		}
		fbuilder = new FormFieldBuilder(accMgrLabel,olbAccountManager);
		FormField folbAccountManager= fbuilder.setMandatory(accMgrMandatory).setMandatoryMsg("Account Manager is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Stationed Technician",chkStationedService);
		FormField fchkStationedService= fbuilder.setMandatory(accMgrMandatory).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Do not print service address" , cbdonotprintserviceAddress);
		FormField fcbdonotprintserviceAddress = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingPrintOptions=fbuilder.setlabel("Print Options").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
	
		fbuilder = new FormFieldBuilder("Invoice Format" , invoicepdfNametoprint);
		FormField finvoicepdfNametoprint = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField folbservicetype=null;
	     if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "MakeServiceTypeMandatory")){
	    	 fbuilder = new FormFieldBuilder("* Service Type",olbservicetype);
		     folbservicetype= fbuilder.setMandatory(true).setMandatoryMsg("Please select service type!").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Service Type",olbservicetype);
		     folbservicetype= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
	     
	     
	     
	     fbuilder = new FormFieldBuilder("Zoho Quotation ID",tbZohoQuotID);
	     FormField ftbZohoQuotID= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////
		
		
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","OnlyForNBHC")){
			 Console.log("Inside IF onlynbhc  ");
//			FormField[][] formfield = { 
//					
//					{fgroupingCustomerInformation},
//					{fpersonInfoComposite,fobjContactList},
//					{ftbContractId,ftbLeadId,ftbQuotatinId,ftbReferenceNumber},
//					{fdbrefernceDate,ftbRefNo2,ftbRefeRRedBy,ftbTicketNumber},
//					{fibOldContract,ftbdonotrenew,fdbContractStartDate,fdbContractEndDate},
//					{ftbPremisesUnderContract,fotbsegment},
//					{fgroupingDocuments},
//					{fucUploadTAndCs},
//					{fgroupingSalesInformation},
//					{ftbQuotationStatus,folbbBranch,folbeSalesPerson,folbcPaymentMethods},
//					{folbApproverName,folbContractGroup,folbContractCategory,folbContractType},
//					{fdbContractDate,fibCreditPeriod,ftbremark,folbcNumberRange},
//					/** date 06/03/2018 commented by komal to remove technician name **/
//					//{folbTechnicainName,ftbCreatedBy,fbtstackDetails},
//					/** date 06/03/2018 added by komal to remove technician name **/
//					{ftbCreatedBy,fbtstackDetails,ftbDonotRenewalMsg,folbAccountManager},
//					{fgroupingDescription},
//					{ftaDescription},
//					/**
//					 * Manisha Moved the position of payterm table at bottom from this position
//					 */
//	
//					{fgroupingProducts},
//					{fprodInfoComposite,fbaddproducts},
//					{fbtnservice},
//					{fsaleslineitemtable},
//					{fblankgroupthree,fpayCompTotalAmt},
////					{fblankgroupthree},
//					{fblankgroupthree,fdbDiscountAmt},
//					{fblankgroupthree,fdbFinalTotalAmt},
//					{fblankgroup,fprodTaxTable},
//					{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
//					{fblankgroup,fchargesTable},
//					{fblankgroupthree,fdbGrandTotalAmt},
//					{fblankgroupthree,fdbroundOffAmt},
//					{fblankgroup,fblankgroupone,fpayCompNetPay},
//					/**Date: 16-11-2017 By: Manisha
//					 * To take a payment terms table below the product table ..!!! 
//					 */
//					{fgroupingPaymentTermsDetails},
//					{fibdays,fdopercent,ftbcomment,faddPaymentTerms},
//					{folbPaymentTerms, fcbStartOfPeriod,fdbPaymentDate,faddPayTerms},
//					//{fchkbillingatBranch,fchkservicewithBilling,fcbServiceWiseBilling,fcbConsolidatePrice},/** Date : 06-10-2017 By ANIL**/ /** Date : 06-02-2018 BY komal - fcbConsolidatePrice**/
//					formCh,/** nidhi for hide check box as process configration */
//					{fpaymentTermsTable},
//	
//			};
//			this.fields=formfield;
		
			 FormField[][] formfield = { 
					 /**CONTRACT DETAILS**/
					{fgroupingCustomerInformation},
					{fpersonInfoComposite,fobjContactList},
					{fdbContractDate,folbbBranch,folbeSalesPerson,folbcNumberRange},
					{folbApproverName,ftbremark,fdbleaddate,fchkStationedService},// Added by Priyanka	
					formCh,
					/**PRODUCT DETAILS**/
					{fgroupingProducts},
					{ftbPremisesUnderContract,folbTechnicainName,folbservicetype},
					{fprodInfoComposite,fbaddproducts},
					{fbtnservice},
					{fsaleslineitemtable},
					{fblankgroupthree,fpayCompTotalAmt},
					{fblankgroupthree,fdbDiscountAmt},
					{fblankgroupthree,fdbFinalTotalAmt},
					{fblankgroup,fprodTaxTable},
					{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
					{fblankgroup,fchargesTable},
					{fblankgroupthree,fdbGrandTotalAmt},
					{fblankgroupthree,fdbroundOffAmt},
					{fblankgroup,fblankgroupone,fpayCompNetPay},
					/**PAYMENT DETAILS**/
					{fgroupingPaymentTermsDetails},
					{folbPaymentTerms,folbcPaymentMethods,fobjPaymentMode,fibCreditPeriod},
					{fcbStartOfPeriod,fdbPaymentDate,faddPayTerms},
					{fibdays,fdopercent,fdoAmount,ftbcomment,faddPaymentTerms},
					//{fchkbillingatBranch,fchkservicewithBilling,fcbServiceWiseBilling,fcbConsolidatePrice},/** Date : 06-10-2017 By ANIL**/ /** Date : 06-02-2018 BY komal - fcbConsolidatePrice**/
					
					//{fchkStationedService},
					{fpaymentTermsTable},
					/**CLASSIFICATION DETAILS**/
					{fgroupingSalesInformation},
					{folbContractGroup,folbContractCategory,folbContractType,fotbsegment},	
					/**GENERAL INFO DETAILS**/
					{fgroupingGeneralInformation},
					{fdbContractStartDate,fdbContractEndDate,folbAccountManager,folbcontractPeriod},
					{ftbLeadId,ftbQuotatinId,ftbReferenceNumber,ftbRefNo2},
				    {fdbrefernceDate,ftbRefeRRedBy,fibOldContract,ftbdonotrenew},
				    {ftbDonotRenewalMsg,ftbTicketNumber,ftbCreatedBy,fpoNumber},
				    {fpoDate,ftbZohoQuotID,fcbdonotprintserviceAddress},
					{ftaDescription},
					{fgroupingDocuments},
     				{fucUploadTAndCs},
     				{fgroupingPrintOptions},
					{finvoicepdfNametoprint}
						
											
		
				};
				this.fields=formfield;
		
		
		}else if(changeProductComposites&&addPoDetails&&hideTechnicianAndContractPeriod){
			 Console.log("Inside IF changeProductComposites addPoDetails hideTechnicianAndContractPeriod  ");
//			FormField[][] formfield = { 
//					{fgroupingCustomerInformation},
//					{fpersonInfoComposite,fobjContactList},
//					{ftbContractId,ftbLeadId,ftbQuotatinId,ftbReferenceNumber},
//					{fdbrefernceDate,ftbRefNo2,ftbRefeRRedBy,ftbTicketNumber},
//					{fibOldContract,ftbdonotrenew,fdbContractStartDate,fdbContractEndDate},
//					{ftbPremisesUnderContract,fotbsegment,fpoNumber,fpoDate},
//					{fgroupingDocuments},
//					{fucUploadTAndCs},
//					{fgroupingSalesInformation},
//					{ftbQuotationStatus,folbbBranch,folbeSalesPerson,folbcPaymentMethods},
//					{folbApproverName,folbContractGroup,folbContractCategory,folbContractType},
//					{fdbContractDate,fibCreditPeriod,ftbremark,folbcNumberRange},
//					{ftbCreatedBy,folbcontractPeriod,ftbDonotRenewalMsg,folbAccountManager},//28-12-2019 Deepak Salve added "addPoDetails" as per Rahul sir requirement
//					{fgroupingDescription},
//					{ftaDescription},
//					/**
//					 * Manisha Moved the position of payterm table at bottom from this position
//					 */
//					{fgroupingProducts},
////					{fprodInfoComposite,fbaddproducts},
//					{folbProductGroup,folbProductName,fbaddproducts},
//					{fbtnservice},
//					{fsaleslineitemtable},
//					{fblankgroupthree,fpayCompTotalAmt},
////					{fblankgroupthree},
//					
//					{fblankgroupthree,fdbDiscountAmt},
//					{fblankgroupthree,fdbFinalTotalAmt},
//					{fblankgroup,fprodTaxTable},
//					{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
//					{fblankgroup,fchargesTable},
//					{fblankgroupthree,fdbGrandTotalAmt},
//					{fblankgroupthree,fdbroundOffAmt},
//					{fblankgroup,fblankgroupone,fpayCompNetPay},
//
//					/**Date: 16-11-2017 By: Manisha
//					 * To take a payment terms table below the product table ..!!! 
//					 */
//					
//					{fgroupingPaymentTermsDetails},
//					{fibdays,fdopercent,ftbcomment,faddPaymentTerms},
//					{folbPaymentTerms, fcbStartOfPeriod,fdbPaymentDate,faddPayTerms},
//					//{fchkbillingatBranch,fchkservicewithBilling,fcbServiceWiseBilling,fcbConsolidatePrice},/** Date : 06-10-2017 By ANIL**/ /** Date : 06-02-2018 BY komal - fcbConsolidatePrice**/
//					formCh,/** nidhi for hide check box as process configration */
//					{fpaymentTermsTable},
//					/**
//					 * Ends
//					 */
			 
			 FormField[][] formfield = { 
					 /**CONTRACT DETAILS**/
					{fgroupingCustomerInformation},
					{fpersonInfoComposite,fobjContactList},
					{fdbContractDate,folbbBranch,folbeSalesPerson,folbcNumberRange},
					{folbApproverName,ftbremark,fdbleaddate,fchkStationedService},	
					formCh,
					/**PRODUCT DETAILS**/
					{fgroupingProducts},
					{ftbPremisesUnderContract,folbTechnicainName,folbservicetype},
					//{fprodInfoComposite,fbaddproducts},
					{fbtnservice},
					{fsaleslineitemtable},
					{fblankgroupthree,fpayCompTotalAmt},
					{fblankgroupthree,fdbDiscountAmt},
					{fblankgroupthree,fdbFinalTotalAmt},
					{fblankgroup,fprodTaxTable},
					{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
					{fblankgroup,fchargesTable},
					{fblankgroupthree,fdbGrandTotalAmt},
					{fblankgroupthree,fdbroundOffAmt},
					{fblankgroup,fblankgroupone,fpayCompNetPay},
					/**PAYMENT DETAILS**/
					{fgroupingPaymentTermsDetails},
					{folbcPaymentMethods,fobjPaymentMode,fibCreditPeriod,folbPaymentTerms},
					{fcbStartOfPeriod,fdbPaymentDate,faddPayTerms},
					{fibdays,fdopercent,fdoAmount,ftbcomment,faddPaymentTerms},
					//{fchkbillingatBranch,fchkservicewithBilling,fcbServiceWiseBilling,fcbConsolidatePrice},/** Date : 06-10-2017 By ANIL**/ /** Date : 06-02-2018 BY komal - fcbConsolidatePrice**/
				
					//{fchkStationedService},
					{fpaymentTermsTable},
					/**CLASSIFICATION DETAILS**/
					{fgroupingSalesInformation},
					{folbContractGroup,folbContractCategory,folbContractType,fotbsegment},				
					/**GENERAL INFO DETAILS**/
					{fgroupingGeneralInformation},
					{fdbContractStartDate,fdbContractEndDate,folbAccountManager,folbcontractPeriod},
					{ftbLeadId,ftbQuotatinId,ftbReferenceNumber,ftbRefNo2},
				    {fdbrefernceDate,ftbRefeRRedBy,fibOldContract,ftbdonotrenew},
				    {ftbDonotRenewalMsg,ftbTicketNumber,ftbCreatedBy,fpoNumber},
				    {fpoDate,ftbZohoQuotID,fcbdonotprintserviceAddress},
					{ftaDescription},
					{fgroupingDocuments},
     				{fucUploadTAndCs},
     				{fgroupingPrintOptions},
					{finvoicepdfNametoprint}
					
						
						
			};
			this.fields=formfield;
		       }else if(changeProductComposites){

		    	   Console.log("Inside IF changeProductComposites ");
//					FormField[][] formfield = {   {fgroupingCustomerInformation},
//							{fpersonInfoComposite,fobjContactList},
//							{ftbContractId,ftbLeadId,ftbQuotatinId,ftbReferenceNumber},
//							{fdbrefernceDate,ftbRefNo2,ftbRefeRRedBy,ftbTicketNumber},
//							{fibOldContract,ftbdonotrenew,fdbContractStartDate,fdbContractEndDate},
//							{ftbPremisesUnderContract,fotbsegment,fpoNumber,fpoDate},
//							{fgroupingDocuments},
//							{fucUploadTAndCs},
//							{fgroupingSalesInformation},
//							{ftbQuotationStatus,folbbBranch,folbeSalesPerson,folbcPaymentMethods},
//							{folbApproverName,folbContractGroup,folbContractCategory,folbContractType},
//							{fdbContractDate,fibCreditPeriod,ftbremark,folbcNumberRange},
//							{ftbCreatedBy,ftbDonotRenewalMsg,folbAccountManager},
//							{fgroupingDescription},
//							{ftaDescription},
//							/**
//							 * Manisha Moved the position of payterm table at bottom from this position
//							 */
//							{fgroupingProducts},
////							{fprodInfoComposite,fbaddproducts},
//							{folbProductGroup,folbProductName,fbaddproducts},
//							{fbtnservice},
//							{fsaleslineitemtable},
//							{fblankgroupthree,fpayCompTotalAmt},
////							{fblankgroupthree},
//							
//							{fblankgroupthree,fdbDiscountAmt},
//							{fblankgroupthree,fdbFinalTotalAmt},
//							{fblankgroup,fprodTaxTable},
//							{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
//							{fblankgroup,fchargesTable},
//							{fblankgroupthree,fdbGrandTotalAmt},
//							{fblankgroupthree,fdbroundOffAmt},
//							{fblankgroup,fblankgroupone,fpayCompNetPay},
//
//							/**Date: 16-11-2017 By: Manisha
//							 * To take a payment terms table below the product table ..!!! 
//							 */
//							
//							{fgroupingPaymentTermsDetails},
//							{fibdays,fdopercent,ftbcomment,faddPaymentTerms},
//							{folbPaymentTerms, fcbStartOfPeriod,fdbPaymentDate,faddPayTerms},
//							//{fchkbillingatBranch,fchkservicewithBilling,fcbServiceWiseBilling,fcbConsolidatePrice},/** Date : 06-10-2017 By ANIL**/ /** Date : 06-02-2018 BY komal - fcbConsolidatePrice**/
//							formCh,/** nidhi for hide check box as process configration */
//							{fpaymentTermsTable},
//							/**
//							 * Ends
//							 */
		    	   
		    	   FormField[][] formfield = { 
							 /**CONTRACT DETAILS**/
							{fgroupingCustomerInformation},
							{fpersonInfoComposite,fobjContactList},
							{fdbContractDate,folbbBranch,folbeSalesPerson,folbcNumberRange},
							{folbApproverName,ftbremark,fdbleaddate,fchkStationedService},	
							formCh,
							/**PRODUCT DETAILS**/
							{fgroupingProducts},
							{ftbPremisesUnderContract,folbTechnicainName,folbservicetype},
							//{fprodInfoComposite,fbaddproducts},
							{fbtnservice},
							{fsaleslineitemtable},
							{fblankgroupthree,fpayCompTotalAmt},
							{fblankgroupthree,fdbDiscountAmt},
							{fblankgroupthree,fdbFinalTotalAmt},
							{fblankgroup,fprodTaxTable},
							{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
							{fblankgroup,fchargesTable},
							{fblankgroupthree,fdbGrandTotalAmt},
							{fblankgroupthree,fdbroundOffAmt},
							{fblankgroup,fblankgroupone,fpayCompNetPay},
							/**PAYMENT DETAILS**/
							{fgroupingPaymentTermsDetails},
							{folbcPaymentMethods,fobjPaymentMode,fibCreditPeriod,folbPaymentTerms},
							{fcbStartOfPeriod,fdbPaymentDate,faddPayTerms},
							{fibdays,fdopercent,fdoAmount,ftbcomment,faddPaymentTerms},
							//{fchkbillingatBranch,fchkservicewithBilling,fcbServiceWiseBilling,fcbConsolidatePrice},/** Date : 06-10-2017 By ANIL**/ /** Date : 06-02-2018 BY komal - fcbConsolidatePrice**/
						
							//{fchkStationedService},
							{fpaymentTermsTable},
							/**CLASSIFICATION DETAILS**/
							{fgroupingSalesInformation},
							{folbContractGroup,folbContractCategory,folbContractType,fotbsegment},
							/**GENERAL INFO DETAILS**/
							{fgroupingGeneralInformation},
							{fdbContractStartDate,fdbContractEndDate,folbAccountManager,folbcontractPeriod},
							{ftbLeadId,ftbQuotatinId,ftbReferenceNumber,ftbRefNo2},
						    {fdbrefernceDate,ftbRefeRRedBy,fibOldContract,ftbdonotrenew},
						    {ftbDonotRenewalMsg,ftbTicketNumber,ftbCreatedBy,fpoNumber},
						    {fpoDate,ftbZohoQuotID,fcbdonotprintserviceAddress},
							{ftaDescription},
							{fgroupingDocuments},
		     				{fucUploadTAndCs},
		     				{fgroupingPrintOptions},
							{finvoicepdfNametoprint}
							
							
							
					};
		    	   	this.fields=formfield;
				  }
			else if(addPoDetails){
				Console.log("Inside addPoDetails");
//			FormField[][] formfield = {   {fgroupingCustomerInformation},
//					{fpersonInfoComposite,fobjContactList},
//					{ftbContractId,ftbLeadId,ftbQuotatinId,ftbReferenceNumber},
//					{fdbrefernceDate,ftbRefNo2,ftbRefeRRedBy,ftbTicketNumber},
//					{fibOldContract,ftbdonotrenew,fdbContractStartDate,fdbContractEndDate},
//					{ftbPremisesUnderContract,fotbsegment,fpoNumber,fpoDate},
//					{fgroupingDocuments},
//					{fucUploadTAndCs},
//					{fgroupingSalesInformation},
//					{ftbQuotationStatus,folbbBranch,folbeSalesPerson,folbcPaymentMethods},
//					{folbApproverName,folbContractGroup,folbContractCategory,folbContractType},
//					{fdbContractDate,fibCreditPeriod,ftbremark,folbcNumberRange},
//					{folbTechnicainName,ftbCreatedBy,folbcontractPeriod,ftbDonotRenewalMsg},
//					{folbAccountManager},
//					{fgroupingDescription},
//					{ftaDescription},
//					/**
//					 * Manisha Moved the position of payterm table at bottom from this position
//					 */
//					{fgroupingProducts},
//					{fprodInfoComposite,fbaddproducts},
//					{fbtnservice},
//					{fsaleslineitemtable},
//					{fblankgroupthree,fpayCompTotalAmt},
////					{fblankgroupthree},
//					
//					{fblankgroupthree,fdbDiscountAmt},
//					{fblankgroupthree,fdbFinalTotalAmt},
//					{fblankgroup,fprodTaxTable},
//					{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
//					{fblankgroup,fchargesTable},
//					{fblankgroupthree,fdbGrandTotalAmt},
//					{fblankgroupthree,fdbroundOffAmt},
//					{fblankgroup,fblankgroupone,fpayCompNetPay},
//
//					/**Date: 16-11-2017 By: Manisha
//					 * To take a payment terms table below the product table ..!!! 
//					 */
//					
//					{fgroupingPaymentTermsDetails},
//					{fibdays,fdopercent,ftbcomment,faddPaymentTerms},
//					{folbPaymentTerms, fcbStartOfPeriod,fdbPaymentDate,faddPayTerms},
//					//{fchkbillingatBranch,fchkservicewithBilling,fcbServiceWiseBilling,fcbConsolidatePrice},/** Date : 06-10-2017 By ANIL**/ /** Date : 06-02-2018 BY komal - fcbConsolidatePrice**/
//					formCh,/** nidhi for hide check box as process configration */
//					{fpaymentTermsTable},
//					/**
//					 * Ends
//					 */
				FormField[][] formfield = { 
						 /**CONTRACT DETAILS**/
						{fgroupingCustomerInformation},
						{fpersonInfoComposite,fobjContactList},
						{fdbContractDate,folbbBranch,folbeSalesPerson,folbcNumberRange},
						{folbApproverName,ftbremark,fdbleaddate,fchkStationedService},	
						formCh,
						/**PRODUCT DETAILS**/
						{fgroupingProducts},
						{ftbPremisesUnderContract,folbTechnicainName,folbservicetype},
						{fprodInfoComposite,fbaddproducts},
						{fbtnservice},
						{fsaleslineitemtable},
						{fblankgroupthree,fpayCompTotalAmt},
						{fblankgroupthree,fdbDiscountAmt},
						{fblankgroupthree,fdbFinalTotalAmt},
						{fblankgroup,fprodTaxTable},
						{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
						{fblankgroup,fchargesTable},
						{fblankgroupthree,fdbGrandTotalAmt},
						{fblankgroupthree,fdbroundOffAmt},
						{fblankgroup,fblankgroupone,fpayCompNetPay},
						/**PAYMENT DETAILS**/
						{fgroupingPaymentTermsDetails},
						{folbcPaymentMethods,fobjPaymentMode,fibCreditPeriod,folbPaymentTerms},
						{fcbStartOfPeriod,fdbPaymentDate,faddPayTerms},
						{fibdays,fdopercent,fdoAmount,ftbcomment,faddPaymentTerms},
						//{fchkbillingatBranch,fchkservicewithBilling,fcbServiceWiseBilling,fcbConsolidatePrice},/** Date : 06-10-2017 By ANIL**/ /** Date : 06-02-2018 BY komal - fcbConsolidatePrice**/
						/** nidhi for hide check box as process configration */
						{fpaymentTermsTable},
						/**CLASSIFICATION DETAILS**/
						{fgroupingSalesInformation},
						{folbContractGroup,folbContractCategory,folbContractType,fotbsegment},
						/**GENERAL INFO DETAILS**/
						{fgroupingGeneralInformation},
						{fdbContractStartDate,fdbContractEndDate,folbAccountManager,folbcontractPeriod},
						{ftbLeadId,ftbQuotatinId,ftbReferenceNumber,ftbRefNo2},
					    {fdbrefernceDate,ftbRefeRRedBy,fibOldContract,ftbdonotrenew},
					    {ftbDonotRenewalMsg,ftbTicketNumber,ftbCreatedBy,fpoNumber},
					    {fpoDate,ftbZohoQuotID,fcbdonotprintserviceAddress},
						{ftaDescription},
						{fgroupingDocuments},
	     				{fucUploadTAndCs},
	     				{fgroupingPrintOptions},
						{finvoicepdfNametoprint}
						
									
			};
			this.fields=formfield;
		}else if(hideTechnicianAndContractPeriod){
			Console.log("Inside hideTechnicianAndContractPeriod");
//			FormField[][] formfield = {   {fgroupingCustomerInformation},
//					{fpersonInfoComposite,fobjContactList},
//					{ftbContractId,ftbLeadId,ftbQuotatinId,ftbReferenceNumber},
//					{fdbrefernceDate,ftbRefNo2,ftbRefeRRedBy,ftbTicketNumber},
//					{fibOldContract,ftbdonotrenew,fdbContractStartDate,fdbContractEndDate},
//					{ftbPremisesUnderContract,fotbsegment},
//					{fgroupingDocuments},
//					{fucUploadTAndCs},
//					{fgroupingSalesInformation},
//					{ftbQuotationStatus,folbbBranch,folbeSalesPerson,folbcPaymentMethods},
//					{folbApproverName,folbContractGroup,folbContractCategory,folbContractType},
//					{fdbContractDate,fibCreditPeriod,ftbremark,folbcNumberRange},
//					{ftbCreatedBy,ftbDonotRenewalMsg,folbAccountManager},
//					{fgroupingDescription},
//					{ftaDescription},
//					/**
//					 * Manisha Moved the position of payterm table at bottom from this position
//					 */
//					{fgroupingProducts},
//					{fprodInfoComposite,fbaddproducts},
//					{fbtnservice},
//					{fsaleslineitemtable},
//					{fblankgroupthree,fpayCompTotalAmt},
////					{fblankgroupthree},
//					
//					{fblankgroupthree,fdbDiscountAmt},
//					{fblankgroupthree,fdbFinalTotalAmt},
//					{fblankgroup,fprodTaxTable},
//					{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
//					{fblankgroup,fchargesTable},
//					{fblankgroupthree,fdbGrandTotalAmt},
//					{fblankgroupthree,fdbroundOffAmt},
//					{fblankgroup,fblankgroupone,fpayCompNetPay},
//
//					/**Date: 16-11-2017 By: Manisha
//					 * To take a payment terms table below the product table ..!!! 
//					 */
//					
//					{fgroupingPaymentTermsDetails},
//					{fibdays,fdopercent,ftbcomment,faddPaymentTerms},
//					{folbPaymentTerms, fcbStartOfPeriod,fdbPaymentDate,faddPayTerms},
//					//{fchkbillingatBranch,fchkservicewithBilling,fcbServiceWiseBilling,fcbConsolidatePrice},/** Date : 06-10-2017 By ANIL**/ /** Date : 06-02-2018 BY komal - fcbConsolidatePrice**/
//					formCh,/** nidhi for hide check box as process configration */
//					{fpaymentTermsTable},
//					/**
//					 * Ends
//					 */
			FormField[][] formfield = { 
					 /**CONTRACT DETAILS**/
					{fgroupingCustomerInformation},
					{fpersonInfoComposite,fobjContactList},
					{fdbContractDate,folbbBranch,folbeSalesPerson,folbcNumberRange},
					{folbApproverName,ftbremark,fdbleaddate,fchkStationedService},	
					formCh,
					/**PRODUCT DETAILS**/
					{fgroupingProducts},
					{ftbPremisesUnderContract,folbTechnicainName,folbservicetype},
					{fprodInfoComposite,fbaddproducts},
					{fbtnservice},
					{fsaleslineitemtable},
					{fblankgroupthree,fpayCompTotalAmt},
					{fblankgroupthree,fdbDiscountAmt},
					{fblankgroupthree,fdbFinalTotalAmt},
					{fblankgroup,fprodTaxTable},
					{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
					{fblankgroup,fchargesTable},
					{fblankgroupthree,fdbGrandTotalAmt},
					{fblankgroupthree,fdbroundOffAmt},
					{fblankgroup,fblankgroupone,fpayCompNetPay},
					/**PAYMENT DETAILS**/
					{fgroupingPaymentTermsDetails},
					{folbcPaymentMethods,fobjPaymentMode,fibCreditPeriod,folbPaymentTerms},
					{fcbStartOfPeriod,fdbPaymentDate,faddPayTerms},
					{fibdays,fdopercent,fdoAmount,ftbcomment,faddPaymentTerms},
					//{fchkbillingatBranch,fchkservicewithBilling,fcbServiceWiseBilling,fcbConsolidatePrice},/** Date : 06-10-2017 By ANIL**/ /** Date : 06-02-2018 BY komal - fcbConsolidatePrice**/

				
					//{fchkStationedService},
					{fpaymentTermsTable},
					/**CLASSIFICATION DETAILS**/
					{fgroupingSalesInformation},
					{folbContractGroup,folbContractCategory,folbContractType,fotbsegment},
					/**GENERAL INFO DETAILS**/
					{fgroupingGeneralInformation},
					{fdbContractStartDate,fdbContractEndDate,folbAccountManager,folbcontractPeriod},
					{ftbLeadId,ftbQuotatinId,ftbReferenceNumber,ftbRefNo2},
				    {fdbrefernceDate,ftbRefeRRedBy,fibOldContract,ftbdonotrenew},
				    {ftbDonotRenewalMsg,ftbTicketNumber,ftbCreatedBy,fpoNumber},
				    {fpoDate,ftbZohoQuotID,fcbdonotprintserviceAddress},
					{ftaDescription},
					{fgroupingDocuments},
    				{fucUploadTAndCs},
    				{fgroupingPrintOptions},
					{finvoicepdfNametoprint}
					
					
			};
			this.fields=formfield;
		}else if(paymentTermForOmPest){
			Console.log("Inside else");
//			FormField[][] formfield = {   {fgroupingCustomerInformation},
//					{fpersonInfoComposite,fobjContactList},
//					{ftbContractId,ftbLeadId,ftbQuotatinId,ftbReferenceNumber},
//					{fdbrefernceDate,ftbRefNo2,ftbRefeRRedBy,ftbTicketNumber},
//					{fibOldContract,ftbdonotrenew,fdbContractStartDate,fdbContractEndDate},
//					{ftbPremisesUnderContract,fotbsegment},
//					{fgroupingDocuments},
//					{fucUploadTAndCs},
//					{fgroupingSalesInformation},
//					{ftbQuotationStatus,folbbBranch,folbeSalesPerson,folbcPaymentMethods},
//					{folbApproverName,folbContractGroup,folbContractCategory,folbContractType},
//					{fdbContractDate,fibCreditPeriod,ftbremark,folbcNumberRange},
//					{folbTechnicainName,ftbCreatedBy,folbcontractPeriod,ftbDonotRenewalMsg},
//					{folbAccountManager},
//					{fgroupingDescription},
//					{ftaDescription},
//					/**
//					 * Manisha Moved the position of payterm table at bottom from this position
//					 */
//					{fgroupingProducts},
//					{fprodInfoComposite,fbaddproducts},
//					{fbtnservice},
//					{fsaleslineitemtable},
//					{fblankgroupthree,fpayCompTotalAmt},
////					{fblankgroupthree},
//					
//					{fblankgroupthree,fdbDiscountAmt},
//					{fblankgroupthree,fdbFinalTotalAmt},
//					{fblankgroup,fprodTaxTable},
//					{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
//					{fblankgroup,fchargesTable},
//					{fblankgroupthree,fdbGrandTotalAmt},
//					{fblankgroupthree,fdbroundOffAmt},
//					{fblankgroup,fblankgroupone,fpayCompNetPay},
//
//					/**Date: 16-11-2017 By: Manisha
//					 * To take a payment terms table below the product table ..!!! 
//					 */
//					
//					{fgroupingPaymentTermsDetails},
//					{fibdays,fdopercent,ftbcomment,faddPaymentTerms},
//					{folbPaymentTerms, fcbStartOfPeriod,fdbPaymentDate,faddPayTerms},
//					{fobjPaymentMode},
//					{ftbPaymentName,ftbPaymentBankAC,ftbPaymentAccountName,ftbPaymentPayableAt},
//					//{fchkbillingatBranch,fchkservicewithBilling,fcbServiceWiseBilling,fcbConsolidatePrice},/** Date : 06-10-2017 By ANIL**/ /** Date : 06-02-2018 BY komal - fcbConsolidatePrice**/
//					formCh,/** nidhi for hide check box as process configration */
//					{fpaymentTermsTable},
//					/**
//					 * Ends
//					 */
			FormField[][] formfield = { 
					 /**CONTRACT DETAILS**/
					{fgroupingCustomerInformation},
					{fpersonInfoComposite,fobjContactList},
					{fdbContractDate,folbbBranch,folbeSalesPerson,folbcNumberRange},
					{folbApproverName,ftbremark,fdbleaddate,fchkStationedService},	
					formCh,
					/**PRODUCT DETAILS**/
					{fgroupingProducts},
					{ftbPremisesUnderContract,folbTechnicainName,folbservicetype},
					{fprodInfoComposite,fbaddproducts},
					{fbtnservice},
					{fsaleslineitemtable},
					{fblankgroupthree,fpayCompTotalAmt},
					{fblankgroupthree,fdbDiscountAmt},
					{fblankgroupthree,fdbFinalTotalAmt},
					{fblankgroup,fprodTaxTable},
					{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
					{fblankgroup,fchargesTable},
					{fblankgroupthree,fdbGrandTotalAmt},
					{fblankgroupthree,fdbroundOffAmt},
					{fblankgroup,fblankgroupone,fpayCompNetPay},
					/**PAYMENT DETAILS**/
					{fgroupingPaymentTermsDetails},
					{folbcPaymentMethods,fibCreditPeriod,folbPaymentTerms},
					{fcbStartOfPeriod,fdbPaymentDate,faddPayTerms},
					{fibdays,fdopercent,fdoAmount,ftbcomment,faddPaymentTerms},
					{fobjPaymentMode},
					{ftbPaymentName,ftbPaymentBankAC,ftbPaymentAccountName,ftbPaymentPayableAt},
					//{fchkbillingatBranch,fchkservicewithBilling,fcbServiceWiseBilling,fcbConsolidatePrice},/** Date : 06-10-2017 By ANIL**/ /** Date : 06-02-2018 BY komal - fcbConsolidatePrice**/
				
					//{fchkStationedService},
					{fpaymentTermsTable},
					/**CLASSIFICATION DETAILS**/
					{fgroupingSalesInformation},
					{folbContractGroup,folbContractCategory,folbContractType,fotbsegment},	
					/**GENERAL INFO DETAILS**/
					{fgroupingGeneralInformation},
					{fdbContractStartDate,fdbContractEndDate,folbAccountManager,folbcontractPeriod},
					{ftbLeadId,ftbQuotatinId,ftbReferenceNumber,ftbRefNo2},
				    {fdbrefernceDate,ftbRefeRRedBy,fibOldContract,ftbdonotrenew},
				    {ftbDonotRenewalMsg,ftbTicketNumber,ftbCreatedBy,fpoNumber},
				    {fpoDate,ftbZohoQuotID,fcbdonotprintserviceAddress},
					{ftaDescription},
					{fgroupingDocuments},
    				{fucUploadTAndCs},
    				{fgroupingPrintOptions},
					{finvoicepdfNametoprint}
					
					
			};
			this.fields=formfield;
		}
		else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ComplainServiceWithTurnAroundTime")){
			Console.log("Inside else");
//			FormField[][] formfield = {   
//					{fgroupingCustomerInformation},
//					{fpersonInfoComposite,fobjContactList},
//					{ftbContractId,ftbLeadId,ftbQuotatinId,ftbReferenceNumber},
//					{fdbrefernceDate,ftbRefNo2,ftbRefeRRedBy,ftbTicketNumber},
//					{fibOldContract,ftbdonotrenew,fdbContractStartDate,fdbContractEndDate},
//					{ftbPremisesUnderContract,fotbsegment},
//					{fgroupingDocuments},
//					{fucUploadTAndCs},
//					{fgroupingSalesInformation},
//					{ftbQuotationStatus,folbbBranch,folbeSalesPerson,folbcPaymentMethods},
//					{folbApproverName,folbContractGroup,fdbContractDate,folbcNumberRange},
//					{fibCreditPeriod,folbcontractPeriod,ftbremark,ftbCreatedBy},
//					{ftbDonotRenewalMsg,fdbTat,folbContractCategory,folbContractType},
//					{folbTechnicainName,folbAccountManager},
//					{fgroupingDescription},
//					{ftaDescription},
//					
//					{fgroupingProducts},
//					{fprodInfoComposite,fbaddproducts},
//					{fbtnservice},
//					{fsaleslineitemtable},
//					{fblankgroupthree,fpayCompTotalAmt},
////					{fblankgroupthree},
//					
//					{fblankgroupthree,fdbDiscountAmt},
//					{fblankgroupthree,fdbFinalTotalAmt},
//					{fblankgroup,fprodTaxTable},
//					{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
//					{fblankgroup,fchargesTable},
//					{fblankgroupthree,fdbGrandTotalAmt},
//					{fblankgroupthree,fdbroundOffAmt},
//					{fblankgroup,fblankgroupone,fpayCompNetPay},
//
//					{fgroupingPaymentTermsDetails},
//					{fibdays,fdopercent,ftbcomment,faddPaymentTerms},
//					{folbPaymentTerms, fcbStartOfPeriod,fdbPaymentDate,faddPayTerms},
//					formCh,
//					{fpaymentTermsTable},
//					
			
			FormField[][] formfield = { 
					 /**CONTRACT DETAILS**/
					{fgroupingCustomerInformation},
					{fpersonInfoComposite,fobjContactList},
					{fdbContractDate,folbbBranch,folbeSalesPerson,folbcNumberRange},
					{folbApproverName,ftbremark,fdbleaddate,fchkStationedService},	
					formCh,
					/**PRODUCT DETAILS**/
					{fgroupingProducts},
					{ftbPremisesUnderContract,folbTechnicainName,folbservicetype},
					{fprodInfoComposite,fbaddproducts},
					{fbtnservice},
					{fsaleslineitemtable},
					{fblankgroupthree,fpayCompTotalAmt},
					{fblankgroupthree,fdbDiscountAmt},
					{fblankgroupthree,fdbFinalTotalAmt},
					{fblankgroup,fprodTaxTable},
					{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
					{fblankgroup,fchargesTable},
					{fblankgroupthree,fdbGrandTotalAmt},
					{fblankgroupthree,fdbroundOffAmt},
					{fblankgroup,fblankgroupone,fpayCompNetPay},
					/**PAYMENT DETAILS**/
					{fgroupingPaymentTermsDetails},
					{folbcPaymentMethods,fobjPaymentMode,fibCreditPeriod,folbPaymentTerms},
					{fcbStartOfPeriod,fdbPaymentDate,faddPayTerms},
					{fibdays,fdopercent,fdoAmount,ftbcomment,faddPaymentTerms},

					
					//{fchkStationedService},
					{fpaymentTermsTable},
					/**CLASSIFICATION DETAILS**/
					{fgroupingSalesInformation},
					{folbContractGroup,folbContractCategory,folbContractType,fotbsegment},	
					/**GENERAL INFO DETAILS**/
					{fgroupingGeneralInformation},
					{fdbContractStartDate,fdbContractEndDate,folbAccountManager,folbcontractPeriod},
					{ftbLeadId,ftbQuotatinId,ftbReferenceNumber,ftbRefNo2},
				    {fdbrefernceDate,ftbRefeRRedBy,fibOldContract,ftbdonotrenew},
				    {ftbDonotRenewalMsg,fdbTat,ftbTicketNumber,ftbCreatedBy},
				    {fpoNumber,fpoDate,ftbZohoQuotID,fcbdonotprintserviceAddress},
					{ftaDescription},
					{fgroupingDocuments},
    				{fucUploadTAndCs},
    				{fgroupingPrintOptions},
					{finvoicepdfNametoprint}
								
			};
			this.fields=formfield;
		}
		else{
			Console.log("Inside else");
//			FormField[][] formfield = {   {fgroupingCustomerInformation},
//					{fpersonInfoComposite,fobjContactList},
//					{ftbContractId,ftbLeadId,ftbQuotatinId,ftbReferenceNumber},
//					{fdbrefernceDate,ftbRefNo2,ftbRefeRRedBy,ftbTicketNumber},
//					{fibOldContract,ftbdonotrenew,fdbContractStartDate,fdbContractEndDate},
//					{ftbPremisesUnderContract,fotbsegment},
//					{fgroupingDocuments},
//					{fucUploadTAndCs},
//					{fgroupingSalesInformation},
//					{ftbQuotationStatus,folbbBranch,folbeSalesPerson,folbcPaymentMethods},
//					{folbApproverName,folbContractGroup,folbContractCategory,folbContractType},
//					{fdbContractDate,fibCreditPeriod,ftbremark,folbcNumberRange},
//					{folbTechnicainName,ftbCreatedBy,folbcontractPeriod,ftbDonotRenewalMsg},
//					{folbAccountManager},
//					{fgroupingDescription},
//					{ftaDescription},
//					/**
//					 * Manisha Moved the position of payterm table at bottom from this position
//					 */
//					{fgroupingProducts},
//					{fprodInfoComposite,fbaddproducts},
//					{fbtnservice},
//					{fsaleslineitemtable},
//					{fblankgroupthree,fpayCompTotalAmt},
////					{fblankgroupthree},
//					
//					{fblankgroupthree,fdbDiscountAmt},
//					{fblankgroupthree,fdbFinalTotalAmt},
//					{fblankgroup,fprodTaxTable},
//					{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
//					{fblankgroup,fchargesTable},
//					{fblankgroupthree,fdbGrandTotalAmt},
//					{fblankgroupthree,fdbroundOffAmt},
//					{fblankgroup,fblankgroupone,fpayCompNetPay},
//
//					/**Date: 16-11-2017 By: Manisha
//					 * To take a payment terms table below the product table ..!!! 
//					 */
//					
//					{fgroupingPaymentTermsDetails},
//					{fibdays,fdopercent,ftbcomment,faddPaymentTerms},
//					{folbPaymentTerms, fcbStartOfPeriod,fdbPaymentDate,faddPayTerms},
//					//{fchkbillingatBranch,fchkservicewithBilling,fcbServiceWiseBilling,fcbConsolidatePrice},/** Date : 06-10-2017 By ANIL**/ /** Date : 06-02-2018 BY komal - fcbConsolidatePrice**/
//					formCh,/** nidhi for hide check box as process configration */
//					{fpaymentTermsTable},
//					/**
//					 * Ends
//					 */
			
						
				
			
			FormField[][] formfield = { 
					 /**CONTRACT DETAILS**/
					{fgroupingCustomerInformation},
					{fpersonInfoComposite,fobjContactList},
					{fdbContractDate,folbbBranch,folbeSalesPerson,folbcNumberRange},
					{folbApproverName,ftbremark,fdbleaddate,fchkStationedService},	
					formCh,
					/**PRODUCT DETAILS**/
					{fgroupingProducts},
					{ftbPremisesUnderContract,folbTechnicainName,folbservicetype},
					{fprodInfoComposite,fbaddproducts},
					{fbtnservice},
					{fsaleslineitemtable},
					{fblankgroupthree,fpayCompTotalAmt},
					{fblankgroupthree,fdbDiscountAmt},
					{fblankgroupthree,fdbFinalTotalAmt},
					{fblankgroup,fprodTaxTable},
					{fblankgroup,faddOtherCharges,fdoAmtIncludingTax},
					{fblankgroup,fchargesTable},
					{fblankgroupthree,fdbGrandTotalAmt},
					{fblankgroupthree,fdbroundOffAmt},
					{fblankgroup,fblankgroupone,fpayCompNetPay},
					/**PAYMENT DETAILS**/
					{fgroupingPaymentTermsDetails},
					{folbcPaymentMethods,fobjPaymentMode,fibCreditPeriod,folbPaymentTerms},
					{fcbStartOfPeriod,fdbPaymentDate,faddPayTerms},
					{fibdays,fdopercent,fdoAmount,ftbcomment,faddPaymentTerms},
					//{fchkbillingatBranch,fchkservicewithBilling,fcbServiceWiseBilling,fcbConsolidatePrice},/** Date : 06-10-2017 By ANIL**/ /** Date : 06-02-2018 BY komal - fcbConsolidatePrice**/

				
					//{fchkStationedService},
					{fpaymentTermsTable},
					/**CLASSIFICATION DETAILS**/
					{fgroupingSalesInformation},
					{folbContractGroup,folbContractCategory,folbContractType,fotbsegment},
					/**GENERAL INFO DETAILS**/
					{fgroupingGeneralInformation},
					{fdbContractStartDate,fdbContractEndDate,folbAccountManager,folbcontractPeriod},
					{ftbLeadId,ftbQuotatinId,ftbReferenceNumber,ftbRefNo2},
				    {fdbrefernceDate,ftbRefeRRedBy,fibOldContract,ftbdonotrenew},
				    {ftbDonotRenewalMsg,ftbTicketNumber,ftbCreatedBy,fpoNumber},
				    {fpoDate,ftbZohoQuotID,fcbdonotprintserviceAddress},
					{ftaDescription},
					{fgroupingDocuments},
     				{fucUploadTAndCs},
     				{fgroupingPrintOptions},
					{finvoicepdfNametoprint}
					
			};
			this.fields=formfield;
		}

		

	}

	/**
	 * method template to update the model with token entity name
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void updateModel(Contract model) 
	{
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","MakeColumnDisable")&&
				!tbQuotatinId.getValue().equals("")){
			this.getSaleslineitemtable().setEnable(false);
		}
		
		
		
		if(!tbTicketNumber.getValue().equals("")){
			model.setTicketNumber(Integer.parseInt(tbTicketNumber.getValue()));
			model.setComplaintFlag(true);
			model.setIsQuotation(false);
		}
		
		
		if(!tbPremisesUnderContract.getValue().equals("")){
			model.setPremisesDesc(tbPremisesUnderContract.getValue());
			}
		
		if(personInfoComposite.getValue()!=null)
			model.setCinfo(personInfoComposite.getValue());
		if(tbReferenceNumber.getValue()!=null)
		{
			model.setRefNo(tbReferenceNumber.getValue());
			
		}
		else
		{
			model.setRefNo(0+"");
		}
		/**
		 * Date : 16-09-2017 BY JAYSHREE
		 * Description : To check null condition if date is blank
		 */
		if(dbrefernceDate.getValue()!=null){
			model.setRefDate(dbrefernceDate.getValue());
		}else{
			model.setRefDate(null);
		}
		/**
		 * End
		 */
		if(tbRefeRRedBy.getValue()!=null)
			model.setReferedBy(tbRefeRRedBy.getValue());
		//if(validuntil.getValue()!=null)
			//model.setValidUntill(validuntil.getValue());
		   
		if(ucUploadTAndCs.getValue()!=null)
			model.setDocument(ucUploadTAndCs.getValue());
		else
			model.setDocument(null);
		if(tbQuotationStatus.getValue()!=null)
			model.setStatus(tbQuotationStatus.getValue());
		if(olbbBranch.getValue(olbbBranch.getSelectedIndex())!=null)
			model.setBranch(olbbBranch.getValue(olbbBranch.getSelectedIndex()));
		if(olbeSalesPerson.getValue()!=null)
			model.setEmployee(olbeSalesPerson.getValue(olbeSalesPerson.getSelectedIndex()));
		if(olbcPaymentMethods.getValue()!=null)
			model.setPaymentMethod(olbcPaymentMethods.getValue());
		if(taDescription.getValue()!=null)
			model.setDescription(taDescription.getValue());
		
		if(dbContractDate.getValue()!=null)
			model.setContractDate(dbContractDate.getValue());
		/*if(paymenttable.getValue()!=null)
			model.setPayments(paymenttable.getValue());*/
		
		/**
		 * Date  10-4-2018
		 * By Jayshree
		 */
		if(olbContractType.getSelectedIndex()!=0){
			Console.log("Contract type "+olbContractType.getValue(olbContractType.getSelectedIndex()));
			model.setType(olbContractType.getValue(olbContractType.getSelectedIndex()));
		}
		else{
			model.setType("");
		}
		
		if(olbContractGroup.getSelectedIndex()!=0)
			model.setGroup(olbContractGroup.getValue(olbContractGroup.getSelectedIndex()));
		if(olbContractCategory.getSelectedIndex()!=0)
			model.setCategory(olbContractCategory.getValue(olbContractCategory.getSelectedIndex()));
		if(olbApproverName.getSelectedIndex()!=0)
			model.setApproverName(olbApproverName.getValue(olbApproverName.getSelectedIndex()));
		if(dototalamt.getValue()!=null)
			model.setTotalAmount(dototalamt.getValue());
		if(donetpayamt.getValue()!=null)
			model.setNetpayable(donetpayamt.getValue());
		if(!tbLeadId.getValue().equals(""))
			model.setLeadCount(Integer.parseInt(tbLeadId.getValue()));
		if(!tbContractId.getValue().equals(""))
			model.setContractCount(Integer.parseInt(tbContractId.getValue()));
		if(!tbQuotatinId.getValue().equals("")){
			model.setQuotationCount(Integer.parseInt(tbQuotatinId.getValue()));
		}
		if(ibCreditPeriod.getValue()!=null){
			model.setCreditPeriod(ibCreditPeriod.getValue());
		}
		if(!ibOldContract.getValue().equals("")){
			model.setRefContractCount(Integer.parseInt(ibOldContract.getValue()));
		}
		cbRenewFlag.setValue(false);
		
		List<SalesLineItem> saleslis=this.getSaleslineitemtable().getDataprovider().getList();
		if(saleslis.size()!=0){
			model.setStartDate(AppUtility.calculateQuotationStartDate(saleslis));
			dbContractStartDate.setValue(model.getStartDate());
		}
		
		if(saleslis.size()!=0)
		{
			model.setEndDate(AppUtility.calculateQuotationEndDate(saleslis));
			dbContractEndDate.setValue(model.getEndDate());
		}
		
		List<SalesLineItem> saleslist=this.getSaleslineitemtable().getDataprovider().getList();
		ArrayList<SalesLineItem> arrItems=new ArrayList<SalesLineItem>();
		arrItems.addAll(saleslist);
		model.setItems(arrItems);
		
		List<PaymentTerms> payTermsLis=this.paymentTermsTable.getDataprovider().getList();
		
		for (int i = 0; i < payTermsLis.size(); i++) {
			System.out.println("Payment %"+payTermsLis.get(i).getPayTermPercent());
		}
		
		ArrayList<PaymentTerms> payTermsArr=new ArrayList<PaymentTerms>();
		payTermsArr.addAll(payTermsLis);
		model.setPaymentTermsList(payTermsArr);
		
		List<ProductOtherCharges> productTaxLis=this.prodTaxTable.getDataprovider().getList();
		ArrayList<ProductOtherCharges> productTaxArr=new ArrayList<ProductOtherCharges>();
		productTaxArr.addAll(productTaxLis);
		model.setProductTaxes(productTaxArr);
		
		List<ProductOtherCharges> productChargeLis=this.chargesTable.getDataprovider().getList();
		ArrayList<ProductOtherCharges> productChargeArr=new ArrayList<ProductOtherCharges>();
		productChargeArr.addAll(productChargeLis);
		model.setProductCharges(productChargeArr);
		
		List<ServiceSchedule> prodservicelist=this.serviceScheduleTable.getDataprovider().getList();
		ArrayList<ServiceSchedule> prodserviceArr=new ArrayList<ServiceSchedule>();
		prodserviceArr.addAll(prodservicelist);
		model.setServiceScheduleList(prodserviceArr);
		
		if(f_serviceDay!=null){
			model.setScheduleServiceDay(f_serviceDay);
		}
		
		model.setContractRate(chkservicewithBilling.getValue());
		model.setBranchWiseBilling(chkbillingatBranch.getValue());
		
		
		if(olbcNumberRange.getSelectedIndex()!=0){
			model.setNumberRange(olbcNumberRange.getValue(olbcNumberRange.getSelectedIndex()));
		}
		else{
			model.setNumberRange("");
		}
		

		// ********vaishnavi************
		
//		model.setClientSideAsset(selectedlist);
		
		if (olbPaymentTerms.getSelectedIndex() != 0)
			model.setPayTerms(olbPaymentTerms.getValue(olbPaymentTerms.getSelectedIndex()));
		
		model.setCustomersaveflag(false);
		
		
		if(tbRefNo2.getValue()!=null){
			model.setRefNo2(tbRefNo2.getValue());
		}
		
		//By Priyanka.
		if(dbconfollowupdate.getValue()!=null){
			model.setFollowUpDate(dbconfollowupdate.getValue());
		}
		
		if(olbTechnicainName.getSelectedIndex()!=0){
			model.setTechnicianName(olbTechnicainName.getValue(olbTechnicainName.getSelectedIndex()));
		}
		
		/*
		 * nidhi
		 * 29-06-2017
		 * 
		 */
		if(!tbCustSegment.getValue().equals("")){
			model.setSegment(tbCustSegment.getValue());
		}
		/*
		 * end
		 */
		
		/** Date 1-09-2017 added by vijay for discount amount final total amount and round off ***/
		if(dbDiscountAmt.getValue()!=null)
			model.setDiscountAmt(dbDiscountAmt.getValue());
		if(dbFinalTotalAmt.getValue() !=null)
			model.setFinalTotalAmt(dbFinalTotalAmt.getValue());
		if(tbroundOffAmt.getValue()!=null && !tbroundOffAmt.getValue().equals(""))
			model.setRoundOffAmt(Double.parseDouble(tbroundOffAmt.getValue()));
		if(dbGrandTotalAmt.getValue()!=null)
			model.setGrandTotalAmount(dbGrandTotalAmt.getValue());
		if(doamtincltax.getValue()!=null){
			model.setInclutaxtotalAmount(doamtincltax.getValue());
		}
		if(cbServiceWiseBilling.getValue()!=null){
			model.setServiceWiseBilling(cbServiceWiseBilling.getValue());
		}
		
		if(olbPaymentTerms.getSelectedIndex()==0){
			model.setPayTerms("");
		}
		/** Date 06/2/2018 added by komal for consolidate price checkbox **/
		if(cbConsolidatePrice.getValue() !=null){
			model.setConsolidatePrice(cbConsolidatePrice.getValue());
		}
		
		if(olbcontractPeriod.getSelectedIndex()!=0){
			model.setContractPeriod(olbcontractPeriod.getValue().trim());
		}
		
		/**
		 *  nidhi
		 *  29-01-2018
		 *  contact person
		 */
//		objContactList.setValue(view.getPocName());
//		showDialogMessage("Poc name -- " + objContactList.getValue(objContactList.getSelectedIndex()));
		if(objContactList.getSelectedIndex()!=0){
			model.setPocName(objContactList.getValue(objContactList.getSelectedIndex()));
		}
		/**
		 * end
		 */
		model.setRenewContractFlag(this.contractRenewFlag);
		/**
		 * nidhi
		 * 9-06-2018
		 * for do not renew remark
		 */
		model.setNonRenewalRemark(this.tbDonotRenewalMsg.getValue());
		
		if(poNumber.getValue()!=null){
			model.setPoNumber(poNumber.getValue());
		}
		
		if(poDate.getValue()!=null){
			model.setPoDate(poDate.getValue());
		}
		if(olbProductGroup.getValue()!=null)
			  model.setProductGroup(olbProductGroup.getValue());
		
		
		if(objPaymentMode.getSelectedIndex()!=0){
			model.setPaymentMode(objPaymentMode.getValue(objPaymentMode.getSelectedIndex()));
		}else{
			model.setPaymentMode("");
			
		}
		
		
		if(dbTat.getValue()!=null) {
			model.setTat(dbTat.getValue());
		}else {
			model.setTat(null);
		}
		
		if(olbAccountManager.getValue()!=null){
			model.setAccountManager(olbAccountManager.getValue());
		}else{
			model.setAccountManager("");
		}
		
		if(customerBillingAddress!=null){
			Console.log("biling address update model"+customerBillingAddress.getCompleteAddress());
			model.setNewcustomerAddress(customerBillingAddress);
		}
		if(customerServiceAddress!=null){
			model.setCustomerServiceAddress(customerServiceAddress);
			Console.log("Service address update model"+customerServiceAddress.getCompleteAddress());

		}
		
		if(invoicepdfNametoprint.getSelectedIndex()!=0){
			model.setInvoicePdfNameToPrint(invoicepdfNametoprint.getValue(invoicepdfNametoprint.getSelectedIndex()));
		}
		else{
			model.setInvoicePdfNameToPrint("");
		}
		
		model.setStationedTechnician(chkStationedService.getValue());
		
		model.setDonotprintServiceAddress(cbdonotprintserviceAddress.getValue());
		
		if(cbStartOfPeriod.getValue()!=null)
			model.setStartOfPeriod(cbStartOfPeriod.getValue());
		
		if(olbservicetype.getValue()!=null){
			model.setServicetype(olbservicetype.getValue());
		}else{
			model.setServicetype("");
		}
		model.setUpdatedBy(LoginPresenter.loggedInUser);
		model.setLastUpdatedDate(new Date());
		
		contractObject=model;
		
		presenter.setModel(model);

	}

	/**
	 * method template to update the view with token entity name
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void updateView(Contract view) 
	{
		contractObject=view;
		
        	tbContractId.setValue(view.getCount()+"");
        	
        	/**
        	 * Date : 12-05-2017 By ANIL
        	 */
        	ContractPresenter.custcount=view.getCinfo().getCount();
        	/**
        	 * End
        	 */
        	
        	/**
        	 * Date : 15-07-2017 By ANIL
        	 * Added segment flag as method parameter to restrict from mapping customer category at view stage.
        	 */
        	this.checkCustomerStatus(view.getCinfo().getCount(),false,false);
        	
        	/**
        	 * Date : 04-04-2017 by ANIL
        	 * loading customer branches at the time of viewing document
        	 */
        	this.checkCustomerBranch(view.getCinfo().getCount());
        	
        	if(view.getTicketNumber()!=-1){
        		tbTicketNumber.setValue(view.getTicketNumber()+"");
        		System.out.println("TICKET NUMBER :: "+view.getTicketNumber()+" && COMPLAINT FLAG :: "+view.getComplaintFlag() );
        		}
        	
        if(view.getPremisesDesc()!=null){
    		tbPremisesUnderContract.setValue(view.getPremisesDesc()+"");
   		}	
        	
		if(view.getCinfo()!=null)
			personInfoComposite.setValue(view.getCinfo());
		
		//Date : 2/2/2017   rohan added this code for loading customer branch
		ContractPresenter.custcount=view.getCinfo().getCount();
		//  ends here 
		if(view.getRefNo()!=null)
			tbReferenceNumber.setValue(view.getRefNo());
		if(view.getReferedBy()!=null)
			tbRefeRRedBy.setValue(view.getReferedBy());
	
		if(view.getRefDate()!=null){
			dbrefernceDate.setValue(view.getRefDate());
		}
		if(view.getStartDate()!=null){
			dbContractStartDate.setValue(view.getStartDate());
		}
		if(view.getEndDate()!=null){
			dbContractEndDate.setValue(view.getEndDate());
		}
		if(view.getContractDate()!=null)
			dbContractDate.setValue(view.getContractDate());
		if(view.getDocument()!=null)
			ucUploadTAndCs.setValue(view.getDocument());
		if(view.getStatus()!=null)
			tbQuotationStatus.setValue(view.getStatus());
		if(view.getBranch()!=null)
			olbbBranch.setValue(view.getBranch());
		if(view.getEmployee()!=null)
			olbeSalesPerson.setValue(view.getEmployee());
		if(view.getApproverName()!=null)
			olbApproverName.setValue(view.getApproverName());
		if(view.getPaymentMethod()!=null)
			olbcPaymentMethods.setValue(view.getPaymentMethod());
		if(view.getDescription()!=null)
			taDescription.setValue(view.getDescription());
		/** date 11.12.2018 added by komal for nbhc service branches button and popup **/
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableServiceBranch")){
			List<SalesLineItem> list = new ArrayList<SalesLineItem>();
			ArrayList<BranchWiseScheduling> branchArrayList = new ArrayList<BranchWiseScheduling>();
			for(Branch branch : LoginPresenter.customerScreenBranchList){
				BranchWiseScheduling branchWiseScheduling = new BranchWiseScheduling();
				branchWiseScheduling.setBranchName(branch.getBusinessUnitName());
				branchWiseScheduling.setArea(0);
				branchWiseScheduling.setCheck(false);
				branchArrayList.add(branchWiseScheduling);
			}
			for(SalesLineItem item : view.getItems()){
				if(item.getServiceBranchesInfo() != null && item.getServiceBranchesInfo().size() > 0){
					list.add(item);
				}else{
					HashMap<Integer, ArrayList<BranchWiseScheduling>> serviceBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
					serviceBranchlist.put(item.getProductSrNo(), branchArrayList);
					item.setServiceBranchesInfo(serviceBranchlist);
					list.add(item);
				}			
			}
			
			try{
				/**
				 * @author Anil @since 06-04-2021
				 * sequece of items entered is getting mismatched
				 * Raised by Rahul Tiwari for Life Line services
				 */
				Comparator<SalesLineItem> comp =new Comparator<SalesLineItem>() {
					@Override
					public int compare(SalesLineItem item1,SalesLineItem item2) {
						if (item1.getProductSrNo()== item2.getProductSrNo()) {
							return 0;
						}else if (item1.getProductSrNo()> item2.getProductSrNo()) {
							return 1;
						} else {
							return -1;
						}
					}
				};
				Collections.sort(list,comp);
			}catch(Exception e){
				
			}
			
			saleslineitemtable.setValue(list);
		}else{
			Console.log("INSIDE ELSE SETTING PRODUCT ");
			try{
				/**
				 * @author Anil @since 06-04-2021
				 * sequece of items entered is getting mismatched
				 * Raised by Rahul Tiwari for Life Line services
				 */
				
				Comparator<SalesLineItem> comp =new Comparator<SalesLineItem>() {
					@Override
					public int compare(SalesLineItem item1,SalesLineItem item2) {
						
						if (item1.getProductSrNo()== item2.getProductSrNo()) {
							return 0;
						}else if (item1.getProductSrNo()> item2.getProductSrNo()) {
							return 1;
						} else {
							return -1;
						}
					}
				};
				Collections.sort(view.getItems(),comp);
			}catch(Exception e){
				
			}
			
			for(SalesLineItem item:view.getItems()){
				Console.log(item.getProductSrNo()+" "+item.getProductName()+" "+item.getPrice()+" "+item.getProSerialNo()+" "+item.getProModelNo());
			}
			saleslineitemtable.setValue(view.getItems());
		}
		
		/**
		 * end komal
		 */
		/*if(view.getPayments()!=null)
			paymenttable.setValue(view.getPayments());*/
		if(view.getCategory()!=null)
			olbContractCategory.setValue(view.getCategory());

		if(view.getType()!=null){
			olbContractType.setValue(view.getType());
		}
		
		if(view.getGroup()!=null)
			olbContractGroup.setValue(view.getGroup());
		
		
		if(view.getCreditPeriod()!=null){
			ibCreditPeriod.setValue(view.getCreditPeriod());
		}
		if(view.getRefContractCount()!=-1){
			ibOldContract.setValue(view.getRefContractCount()+"");
		}
		if(view.getRemark()!=null){
			tbremark.setValue(view.getRemark());
		}
		if(view.isCustomerInterestFlag()==false){
			//Ashwini Patil Date:15-05-2024 Ultima search reported that renewal status is not showing renewed
			if(view.isRenewFlag())
				tbdonotrenew.setValue("Renewed");
			else
				tbdonotrenew.setValue("");
		}
		else{
			if(view.isRenewFlag())
				tbdonotrenew.setValue("Renewed");
			else {
				tbdonotrenew.setValue(AppConstants.CONTRACTDONOTRENEW);
				tbdonotrenew.getElement().addClassName("redtext");
			}
		}

		if(view.getLeadCount()!=-1)
			tbLeadId.setValue(view.getLeadCount()+"");
		if(view.getQuotationCount()!=-1)
			tbQuotatinId.setValue(view.getQuotationCount()+"");

			paymentTermsTable.setValue(view.getPaymentTermsList());
			prodTaxTable.setValue(view.getProductTaxes());
			chargesTable.setValue(view.getProductCharges());
			dototalamt.setValue(view.getTotalAmount());
			donetpayamt.setValue(view.getNetpayable());
			
			
			
			serviceScheduleTable.setValue(view.getServiceScheduleList());
			if(view.getScheduleServiceDay()!=null){
				f_serviceDay=view.getScheduleServiceDay();
			}
			
			chkbillingatBranch.setValue(view.isBranchWiseBilling());
			
			if(view.getNumberRange()!=null)
				olbcNumberRange.setValue(view.getNumberRange());
			
			/**
			 * Rahul COmmented this on 23 Nov 2017
			 */
			if (view.getPayTerms() != null)
				olbPaymentTerms.setValue(view.getPayTerms());
			/**
			 * Ends for Rahul
			 */
			//vijay
			chkservicewithBilling.setValue(view.isContractRate());
			
			if(view.getRefNo2()!=null){
				tbRefNo2.setValue(view.getRefNo2());
			}
			
			
			/** by Priyanka **/
			
			if(view.getFollowUpDate()!=null){
				dbconfollowupdate.setValue(view.getFollowUpDate());
			}
			
			if (view.getCreatedBy() != null){
				tbCreatedBy.setValue(view.getCreatedBy());	
			}
			
			if (view.getTechnicianName() != null){
				olbTechnicainName.setValue(view.getTechnicianName());	
			}
			if (view.getSegment() != null){
				tbCustSegment.setValue(view.getSegment());	
			}
			
			/** Date 1-09-2017 added by vijay for discount amount final total amount and round off ***/
			dbDiscountAmt.setValue(view.getDiscountAmt());
			dbFinalTotalAmt.setValue(view.getFinalTotalAmt());
			tbroundOffAmt.setValue(view.getRoundOffAmt()+"");
			dbGrandTotalAmt.setValue(view.getGrandTotalAmount());
			doamtincltax.setValue(view.getInclutaxtotalAmount());
			
			/**
			 * Date : 06-10-2017 BY ANIL
			 */
			cbServiceWiseBilling.setValue(view.isServiceWiseBilling());
			if(view.isServiceWiseBilling()==true){
				addPaymentTerms.setEnabled(false);
				addPayTerms.setEnabled(false);
			}
			/**
			 * End
			 */
			
			/**
			 * Date : 31-08-2017 BY ANIL
			 * If service schedule list is greater than 1000 then we have to retrieve data from separate entity as 
			 * we are storing it in separate entity.
			 */
			if(view.getServiceScheduleList().size()==0){
				Console.log("SERVICE SCHEDULE LIST ");
				Console.log("COM_ID "+view.getCompanyId()+" DOC_TYPE "+"Contract"+" DOC_ID "+view.getCount());
				final ContractServiceAsync conSer=GWT.create(ContractService.class);
				final GWTCGlassPanel gpanel=new GWTCGlassPanel();
				gpanel.show();
				conSer.getScheduleServiceSearchResult(view.getCompanyId(), "Contract", view.getCount(), new AsyncCallback<ArrayList<ServiceSchedule>>() {
					@Override
					public void onFailure(Throwable caught) {
						Console.log("SERVICE SCHEDULE FAILURE ");
						gpanel.hide();
					}
					@Override
					public void onSuccess(ArrayList<ServiceSchedule> result) {
//						view.setServiceScheduleList(result);
						Console.log("SERVICE SCHEDULE SUCCESS "+result.size());
						if(result.size()!=0){
						serviceScheduleTable.setValue(result);
						}
						gpanel.hide();
					}
				});
				      
			}
			/**
			 * End
			 */
			/**
			 * Rahul Verma Added this at 23 Nov 2017
			 */
//			olbPaymentTerms.setValue(null);
			/**
			 * Ends for Rahul
			 */
			/** Date 06/2/2018 added by komal for consolidate price checkbox **/
			
			cbConsolidatePrice.setValue(view.isConsolidatePrice());

			/**
			 * end komal
			 */
		
			if(view.getContractPeriod()!=null)
				olbcontractPeriod.setValue(view.getContractPeriod());
			
			
			/**
			 *  nidhi
			 *  29-01-2018
			 *  contact person
			 */
			objContactList.setValue(view.getPocName());
			/**
			 * end
			 */
			/**
        	 *  nidhi
        	 *  29-01-2018
        	 *  get customer contact list
        	 */
        
        	
        	/** end */
			
			if(view.getCinfo().getCount()!=0){
        		this.getCustomerContactList(view.getCinfo().getCount(),view.getPocName());
        	}
        	Console.log("after contact list count --" +view.getCinfo().getCount());
        	
			/*
			 *  end
			 */
        	/**
        	 * nidhi
        	 * 
        	 */
        	contractRenewFlag = view.isRenewContractFlag();
        	/**
        	 * end
        	 */
        	
    		/**
        	 * nidhi
        	 * 9-06-2018
        	 * 
        	 * 
        	 */
        	tbDonotRenewalMsg.setValue(view.getNonRenewalRemark());
        	
        	if(view.getPoNumber()!=null){
        		poNumber.setValue(view.getPoNumber());
        	}
        	if(view.getPoDate()!=null){
        		poDate.setValue(view.getPoDate());
        	}
        	
        	if(view.getProductGroup()!=null)
    			olbProductGroup.setValue(view.getProductGroup());
        	
        	
        	if(view.getPaymentMode()!=null){
            	objPaymentMode.setValue(view.getPaymentMode());
            	}
        	if(view.getTat()!=null){
        		dbTat.setValue(view.getTat());
        	}
            	setBankSelectedDetails();
            	
        	
            if(view.getAccountManager()!=null){
            	olbAccountManager.setValue(view.getAccountManager());
            }
            
            chkStationedService.setValue(view.isStationedTechnician());
			
            /* 
			 * for approval process
			 *  nidhi
			 *  5-07-2017
			 */
        	findMaxServiceSrNumber();
			if(presenter != null){
				presenter.setModel(view);
			}
			/*
			 *  end
			 */
			if(glassPanel!=null){
				hideWaitSymbol();
			}
			
			if(view.getInvoicePdfNameToPrint()!=null && !view.getInvoicePdfNameToPrint().equals("")){
				invoicepdfNametoprint.setValue(view.getInvoicePdfNameToPrint());
			}
			cbdonotprintserviceAddress.setValue(view.isDonotprintServiceAddress());

			cbStartOfPeriod.setValue(view.isStartOfPeriod());

			if(view.getServicetype()!=null&&!view.getServicetype().equals("")){
				olbservicetype.setValue(view.getServicetype());
			}
			if(view.getZohoQuotID()!=null&&!view.getZohoQuotID().equals("")){
				tbZohoQuotID.setValue(view.getZohoQuotID());
			}
	}

	public DateBox getDbconfollowupdate() {
		return dbconfollowupdate;
	}

	public void setDbconfollowupdate(DateBox dbconfollowupdate) {
		this.dbconfollowupdate = dbconfollowupdate;
	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Anil @since 15-07-2021
			 * for summary screen
			 */
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Anil @since 15-07-2021
			 * for summary screen
			 */
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION) || text.contains(AppConstants.COMMUNICATIONLOG))/** date 4.1.2019 added by komal to show communication log button in edit state**/
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * @author Anil @since 15-07-2021
			 * for summary screen
			 */
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Edit")||text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.NAVIGATION)|| text.contains(AppConstants.COMMUNICATIONLOG))
						menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.CONTRACT,LoginPresenter.currentModule.trim());
	}

/**
 * Toogels the Application Process Level Menu as Per Application State
 */
	public void toggleProcessLevelMenu()
	{
		Console.log("in toggleProcessLevelMenu UserConfiguration.getRole().getRoleName()="+UserConfiguration.getRole().getRoleName());
		/**
		 * @author Anil , Date : 28-05-2019
		 */
		boolean deleteServFlag=false;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ShowDeleteDuplicateServiceButton")){
			deleteServFlag=true;
		}
		/**
		 * @author Vijay Chougule
		 * Des :- NBHC CCPM Uploaded contracts Service Branches Updation in services
		 */
		boolean updateServiceBranchFlag = false;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableUpdateServicesServiceBranch")){
			updateServiceBranchFlag = true;
		}
		System.out.println("INSIDE TOGGLE PROCESS LEVEL MENU..."+deleteServFlag);
		Contract entity=(Contract) presenter.getModel();
		
		if(entity.getGroup()!=null && !entity.getGroup().equals("") && entity.getStatus().equals(Contract.APPROVED)
				 && (entity.getGroup().equalsIgnoreCase("One Time") || entity.getGroup().equalsIgnoreCase("Single") )  ) {
			entity.setCustomerInterestFlag(true);
			tbdonotrenew.setValue(AppConstants.CONTRACTDONOTRENEW);
			tbdonotrenew.getElement().addClassName("redtext");
		}
		
		boolean makeRenewalFlag = false;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableShowMakeRenewableButton")){
			makeRenewalFlag = true;
		}
		
		String status=entity.getStatus();
		boolean renewStatus=entity.isRenewFlag();
		/**amol**/
		boolean renewalFlag=entity.isRenewContractFlag();
		boolean donotrenewStatus=entity.isCustomerInterestFlag();
		
		
		/**
		 * @author Vijay Date - 07-03-2022
		 * Des :- Update Client license update only visible to essevaerp link
		 */
		String Url = GWT.getModuleBaseURL();
		String Appid = "";
		if(Url.contains("-dot-")){
			String [] urlarray = Url.split("-dot-");
			 String url1 = urlarray[1];
			 
			 String [] urlarray1 = url1.split("\\.");
			 Appid = urlarray1[0];
		}
		else{
			String [] urlarray = Url.split("\\.");
			 Appid = urlarray[1];
		}
		Console.log("Appid "+Appid);
		boolean clientLicencesUpdateFlag = false;
		if(Appid.trim().equals("essevaerp")) {
			clientLicencesUpdateFlag = true;
		}
		
		Console.log("contract status "+entity.getStatus());
		
		for(int i=0;i<getProcesslevelBarNames().length;i++)
		{
			InlineLabel label=getProcessLevelBar().btnLabels[i];
			String text=label.getText().trim();
//			Console.log("old renew flag "+renewStatus);
//			Console.log("renewalFlag status "+renewalFlag);
			Console.log("text="+text);
			
			
			
			if(status.equals(Contract.CREATED))
			{   
				
				
				if(text.equals(AppConstants.VIEWINVOICEPAYMENT))
					label.setVisible(true);
				
				
				
				
				
				if(text.equals("Print Renewal Reminder")){
					label.setVisible(false);
				}
				if(text.equals("Email Renewal Reminder")){
					label.setVisible(false);
				}
				
				if(getManageapproval().isSelfApproval()){
					/**
					 * Ajinkya added this code 
					 * Date:1/08/2017
					 */
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(true);
					if(text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(false);
					/**
					 * End here
					 */
				}else{
					/**
					 * Ajinkya added this code 
					 * Date:1/08/2017
					 */
					if(text.equals(ManageApprovals.SUBMIT))
						label.setVisible(true);
					if(text.equals(ManageApprovals.APPROVALREQUEST))
						label.setVisible(true);
					/**
					 * End here
					 */
				}
				if(text.equals(AppConstants.PAYMENT))
					label.setVisible(false);
				else if(text.equals(AppConstants.VIEWSERVICES))
					label.setVisible(false);
				else if(text.equals(AppConstants.PRINTJOBCARD))
					label.setVisible(false);
//				else if(text.equals(AppConstants.MANAGEPROJECT))
//					label.setVisible(false);
				else if(text.equals(AppConstants.CONTRACTRENEWAL))
					label.setVisible(false);
				else if(text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(false);
				else if(text.equals(AppConstants.WORKORDER))
					label.setVisible(false);
				else if(text.equals(AppConstants.CUSTOMERINTEREST))
					label.setVisible(false);
				if(text.equals(AppConstants.MATERIALREQUIREDREPORT))
					label.setVisible(false);
				if(text.equals("Monthly Service Record"))
					label.setVisible(false);
				
				if(text.equals(AppConstants.CREATESERVICE))   //vijay
					label.setVisible(false);
				
				if(text.equals("CreateServices"))   //rohan
					label.setVisible(false);
				
				if(text.equals(AppConstants.CANCLECONTRACT))
					label.setVisible(false);
				
				/*********Done by Rahul on 15-10-2016 only for NBHC******/
				if(text.equals(AppConstants.UPDATEPRICES))
					label.setVisible(false);
				
				if(text.equals(AppConstants.CANCELSERVICES))
					label.setVisible(false);
				
				
				/**
				 * Date : 14-03-2017 By Anil
				 * FAR,FMR and CHR PDF
				 * NBHC CCPM
				 */
				if(text.equals("FAR")){
					label.setVisible(false);
				}
				if(text.equals("FMR")){
					label.setVisible(false);
				}
				if(text.equals("CHR")){
					label.setVisible(false);
				}
				if(text.equals("Create EVA Services")){
					label.setVisible(false);
				}
				
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWBILL)){
					label.setVisible(false);
				}
				/**
				 * End
				 */
				
				/** date 12-09-2017 added by vijay for copy Contract ****/
				if(text.equals(AppConstants.COPYCONTRACT)){
					label.setVisible(false);
				}
				/** date 13-09-2017 added by vijay for customer address ****/
				if(text.equals(AppConstants.CUSTOMERADDRESS)){
					label.setVisible(true);
				}
				
				/** date 07-11-2017 added by ANIL for Upload T&C ****/
				if(text.equals(AppConstants.UPLOADTERMSANDCONDITION)){
					label.setVisible(false);
				}
				/** date 09-04-2018 added by ANIL  ****/
				if(text.equals(AppConstants.LICENSETYPELIST.UPDATE_CLIENTS_LICENSE)){
					label.setVisible(false);
				}
				
				/** date 19-04-2018 added by ANIL ****/
				if(text.equals(AppConstants.VIEWSALESORDER)){
					label.setVisible(false);
				}
				
				if(text.equals(AppConstants.MAKERENEWABLE)){
					label.setVisible(false);
				}
				
				if(text.equals(AppConstants.SERVICERECORD)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.UPDATESERVICEVALUE)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.UPDATETECHNICIANNAME)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.CONTRACTRESET)){
					label.setVisible(false);
				}
				
				if(text.equals(AppConstants.DELETECLIENTLICENSE)){
						label.setVisible(false);
				}
				if(text.equals(AppConstants.VIEWCOMPLAIN)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.UpdateSalesPerson)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.AddCustomerBranches)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.markcontractasrenewed)){
					label.setVisible(false);
				}
			}
			else if(status.equals(Contract.APPROVED)&&renewStatus==false&&donotrenewStatus==false)
			{
				if(text.equals("Print Renewal Reminder")){
					label.setVisible(false);
				}
				if(text.equals("Email Renewal Reminder")){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.Approve))
					label.setVisible(false);	
				if(text.equals(AppConstants.VIEWSERVICES))
					label.setVisible(true);	
				if(entity.getbalance()>0)
				{
					if(text.equals(AppConstants.PAYMENT))
						label.setVisible(true);
				}
//				else if(text.equals(AppConstants.CANCLECONTRACT))
//					label.setVisible(true);
				if(text.equals(AppConstants.PRINTJOBCARD))
					label.setVisible(true);
//				if(text.equals(AppConstants.MANAGEPROJECT))
//					label.setVisible(true);
				if(text.equals(AppConstants.CONTRACTRENEWAL))
					label.setVisible(true);
				if(text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(false);
				if(text.equals(AppConstants.WORKORDER))
					label.setVisible(true);
				if(text.equals(AppConstants.CUSTOMERINTEREST))
					label.setVisible(true);
				if(text.equals(AppConstants.MATERIALREQUIREDREPORT))
					label.setVisible(true);
				
				if(text.equals("Monthly Service Record"))
					label.setVisible(true);
				
				if(text.equals(AppConstants.CREATESERVICE)) //vijay
					label.setVisible(true);
				
				if(text.equals("CreateServices"))   //rohan
					label.setVisible(true);
				
				/**
				 * rohan added this code for NBHC Changes ADMIN role should  
				 */
				if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") 
						|| LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("Zonal Coordinator")){
					
					if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
						if(text.equals(AppConstants.CANCLECONTRACT)){
							label.setVisible(true);
						}
						if(text.equals(AppConstants.CANCELSERVICES))
							label.setVisible(true);
					}
					
					/**
					 * nidhi
					 * 18-05-2018
					 * 
					 */
					/** Date 02-10-2019 by Vijay for NBHC CCPM ContractDiscotinue to zonal coordinator **/
					if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") ||
							LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("Zonal Coordinator") ){
						if(text.equals(AppConstants.CONTRACTDISCOUNTINUED)  ){
							label.setVisible(true);
						}
						
						if(text.equals(AppConstants.CONTRACTRESET)){
							label.setVisible(true);
						}
					}
				}
				else
				{
					if(text.equals(AppConstants.CANCLECONTRACT))
						label.setVisible(false);
					
					if(text.equals(AppConstants.CANCELSERVICES))
						label.setVisible(false);
					
					/**
					 * nidhi
					 * 18-05-2018
					 * 
					 */
					if(text.equals(AppConstants.CONTRACTDISCOUNTINUED)  ){
						label.setVisible(false);
					}
					
					if(text.equals(AppConstants.CONTRACTRESET)){
						label.setVisible(false);
					}
				}
				
				/*********Done by Rahul on 15-10-2016 only for NBHC******/
				if(text.equals(AppConstants.UPDATEPRICES))
					label.setVisible(true);
				
				if(text.equals(AppConstants.CANCELSERVICES))
					label.setVisible(false);
				
				
				/**
				 * Date : 14-03-2017 By Anil
				 * FAR,FMR and CHR PDF
				 * NBHC CCPM
				 */
				if(text.equals("FAR")){
					label.setVisible(true);
				}
				if(text.equals("FMR")){
					label.setVisible(true);
				}
				if(text.equals("CHR")){
					label.setVisible(true);
				}
				
				if(text.equals("Create EVA Services")){
					label.setVisible(true);
				}
				
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWBILL)){
					label.setVisible(true);
				}
				/**@Sheetal : 16-02-2022
				 *    Des : Adding view lead,view quotation button**/
				if(text.equals(AppConstants.VIEWLEAD)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.VIEWQUOTATION)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				
				/**
				 * Ajinkya added this code 
				 * Date:1/08/2017
				 */
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				/**
				 * End here
				 */
				
				/** date 12-09-2017 added by vijay for copy Contract ****/
				if(text.equals(AppConstants.COPYCONTRACT)){
					label.setVisible(false);
				}
				/** date 13-09-2017 added by vijay for customer address ****/
				if(text.equals(AppConstants.CUSTOMERADDRESS)){
					label.setVisible(true);
				}
				/** date 06-11-2017 added by ANIL for Upload T&C ****/
				if(text.equals(AppConstants.UPLOADTERMSANDCONDITION)){
					label.setVisible(true);
				}
				/** date 09-04-2018 added by ANIL  ****/
				if(text.equals(AppConstants.LICENSETYPELIST.UPDATE_CLIENTS_LICENSE)){
					if(clientLicencesUpdateFlag) 
						label.setVisible(true);
					else
						label.setVisible(false);
				}
				
				/** date 19-04-2018 added by ANIL ****/
				if(text.equals(AppConstants.VIEWSALESORDER)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.VIEWINVOICEPAYMENT))
					label.setVisible(true);
				
				
				if(makeRenewalFlag){
					if(text.equals(AppConstants.MAKERENEWABLE)){
						label.setVisible(true);
					}
				}
				else{
					if(text.equals(AppConstants.MAKERENEWABLE)){
						label.setVisible(false);
					}
				}
				
				if(text.equals(AppConstants.SERVICERECORD)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.UPDATESERVICEVALUE)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.UPDATETECHNICIANNAME)){
					label.setVisible(true);
				}
				
				if(text.equals(AppConstants.DELETECLIENTLICENSE)){
					if(clientLicencesUpdateFlag) 
						label.setVisible(true);
					else
						label.setVisible(false);
				}
				
				if(text.equals(AppConstants.VIEWCOMPLAIN)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.UpdateSalesPerson)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.AddCustomerBranches)&&AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableAddCustomerBranchesOption")&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")) {
						label.setVisible(true);				
				}else if(text.equals(AppConstants.AddCustomerBranches)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.markcontractasrenewed)&&AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "ShowMarkRenewedButton")) {
					label.setVisible(true);				
				}else if(text.equals(AppConstants.markcontractasrenewed)){
					label.setVisible(false);
				}
			}
			else if(status.equals(Contract.APPROVED)&&renewStatus==true)
			{
				if(text.equals("Print Renewal Reminder")){
					label.setVisible(false);
				}
				if(text.equals("Email Renewal Reminder")){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.Approve))
					label.setVisible(false);	
				if(text.equals(AppConstants.VIEWSERVICES))
					label.setVisible(true);	
				if(entity.getbalance()>0)
				{
					if(text.equals(AppConstants.PAYMENT))
						label.setVisible(true);
				}
//				else if(text.equals(AppConstants.CANCLECONTRACT))
//					label.setVisible(false);
				if(text.equals(AppConstants.PRINTJOBCARD))
					label.setVisible(true);
//				if(text.equals(AppConstants.MANAGEPROJECT))
//					label.setVisible(true);
				/**
				 * @author Abhinav
				 * @since 11/10/2019
				 * 
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","EnableShowContractRenewForRenewedContracts")
						&& LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")
						||AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "ContractRenewalNewFormat")){
					System.out.println("Hi Abhiav inside");
					if(text.equals(AppConstants.CONTRACTRENEWAL))
						label.setVisible(true);
					if(text.equals(AppConstants.CONTRACTRESET)){
						label.setVisible(true);
					}
				}
				else{
					if(text.equals(AppConstants.CONTRACTRENEWAL))
						label.setVisible(true);//Ashwini Patil Date:18-04-2023
				}
				
				if(text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(false);
				if(text.equals(AppConstants.WORKORDER))
					label.setVisible(true);
				if(text.equals(AppConstants.CUSTOMERINTEREST))
					label.setVisible(false);
				if(text.equals(AppConstants.MATERIALREQUIREDREPORT))
					label.setVisible(true);
				
				if(text.equals("Monthly Service Record"))
					label.setVisible(true);
				
				if(text.equals(AppConstants.CREATESERVICE)) //vijay
					label.setVisible(true);
				
				if(text.equals("CreateServices"))   //rohan
					label.setVisible(true);
				
				
				if(text.equals(AppConstants.CANCLECONTRACT)){ // Deepak Salve
					if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")
					&&AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "showCancelButtonForRenew")){// Date 04-12-2019 Deepak Salve Add this configration
					      label.setVisible(true);
					}else {
						label.setVisible(false);
					}
//					if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")
//					&&AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "showCancelButtonForRenew")){// Date 04-12-2019 Deepak Salve Add this configration
//					      label.setVisible(true);
//					}else {
//						label.setVisible(false);
//					}
				}
				
				/*********Done by Rahul on 15-10-2016 only for NBHC******/
				if(text.equals(AppConstants.UPDATEPRICES))
					label.setVisible(false);
				
				if(text.equals(AppConstants.CANCELSERVICES))
					label.setVisible(false);
				
				/**
				 * Date : 14-03-2017 By Anil
				 * FAR,FMR and CHR PDF
				 * NBHC CCPM
				 */
				if(text.equals("FAR")){
					label.setVisible(true);
				}
				if(text.equals("FMR")){
					label.setVisible(true);
				}
				if(text.equals("CHR")){
					label.setVisible(true);
				}
				
				if(text.equals("Create EVA Services")){
					label.setVisible(true);
				}
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWBILL)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**@Sheetal : 16-02-2022
				 *    Des : Adding view lead,view quotation button**/
				if(text.equals(AppConstants.VIEWLEAD)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.VIEWQUOTATION)){
					label.setVisible(true);
				}
				/**
				 * Ajinkya added this code 
				 * Date:1/08/2017
				 */
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				/**
				 * End here
				 */
				
				/** date 12-09-2017 added by vijay for copy Contract ****/
				if(text.equals(AppConstants.COPYCONTRACT)){
					label.setVisible(false);
				}
				
				/** date 13-09-2017 added by vijay for customer address ****/
				if(text.equals(AppConstants.CUSTOMERADDRESS)){
					label.setVisible(true);
				}
				
				/** date 06-11-2017 added by ANIL for Upload T&C ****/
				if(text.equals(AppConstants.UPLOADTERMSANDCONDITION)){
					label.setVisible(true);
				}
				
				/** date 09-04-2018 added by ANIL  ****/
				if(text.equals(AppConstants.LICENSETYPELIST.UPDATE_CLIENTS_LICENSE) ){
					if(clientLicencesUpdateFlag) 
						label.setVisible(true);
					else
						label.setVisible(false);
				}
				
				/** date 19-04-2018 added by ANIL ****/
				if(text.equals(AppConstants.VIEWSALESORDER)){
					label.setVisible(true);
				}
//				/**
//				 * nidhi
//				 * 18-05-2018
//				 * 
//				 */
//				if(text.equals(AppConstants.CONTRACTDISCOUNTINUED)){
//					label.setVisible(false);
//				}
				/** Date 02-10-2019 by Vijay for NBHC CCPM ContractDiscotinue to zonal coordinator **/
				if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") ||
						LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("Zonal Coordinator") ){
					if(text.equals(AppConstants.CONTRACTDISCOUNTINUED)){
						label.setVisible(true);
					}
					
					if(text.equals(AppConstants.CONTRACTRESET)){
						label.setVisible(true);
					}
				}
				else{
					if(text.equals(AppConstants.CONTRACTDISCOUNTINUED)){
						label.setVisible(false);
					}
					if(text.equals(AppConstants.CONTRACTRESET)){
						label.setVisible(false);
					}
				}
				if(text.equals(AppConstants.VIEWINVOICEPAYMENT))
					label.setVisible(true);
				
				
				if(makeRenewalFlag){
					if(text.equals(AppConstants.MAKERENEWABLE)){
						label.setVisible(true);
					}
				}
				else{
					if(text.equals(AppConstants.MAKERENEWABLE)){
						label.setVisible(false);
					}
				}
				
				if(text.equals(AppConstants.SERVICERECORD)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.UPDATESERVICEVALUE)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.UPDATETECHNICIANNAME)){
					label.setVisible(true);
				}
				
				if(text.equals(AppConstants.DELETECLIENTLICENSE)){
					if(clientLicencesUpdateFlag) 
						label.setVisible(true);
					else
						label.setVisible(false);
				}
				
				if(text.equals(AppConstants.VIEWCOMPLAIN)){
					label.setVisible(true);
				}	
				if(text.equals(AppConstants.UpdateSalesPerson)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.AddCustomerBranches)&&AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableAddCustomerBranchesOption")&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")) {
					label.setVisible(true);				
				}else if(text.equals(AppConstants.AddCustomerBranches)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.markcontractasrenewed)&&AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "ShowMarkRenewedButton")) {
					label.setVisible(true);				
				}else if(text.equals(AppConstants.markcontractasrenewed)){
					label.setVisible(false);
				}
			}
			else if(status.equals(Contract.CONTRACTEXPIRED))
			{
				if(text.equals(AppConstants.VIEWINVOICEPAYMENT))
					label.setVisible(true);
				if(text.equals("Print Renewal Reminder")){
					label.setVisible(false);
				}
				if(text.equals("Email Renewal Reminder")){
					label.setVisible(false);
				}
				if(text.contains(AppConstants.RENEW))
					label.setVisible(true);
				else
					label.setVisible(false);
				
				if(text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(false);
				if(text.equals(AppConstants.CUSTOMERINTEREST))
					label.setVisible(false);
				if(text.equals("CreateServices"))   //rohan
					label.setVisible(false);
				
				/*********Done by Rahul on 15-10-2016 only for NBHC******/
				if(text.equals(AppConstants.UPDATEPRICES))
					label.setVisible(false);
				
				if(text.equals(AppConstants.CANCELSERVICES))
					label.setVisible(false);
				
				
				/**
				 * Date : 14-03-2017 By Anil
				 * FAR,FMR and CHR PDF
				 * NBHC CCPM
				 */
				if(text.equals("FAR")){
					label.setVisible(false);
				}
				if(text.equals("FMR")){
					label.setVisible(false);
				}
				if(text.equals("CHR")){
					label.setVisible(false);
				}
				
				if(text.equals("Create EVA Services")){
					label.setVisible(false);
				}
				
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWBILL)){
					label.setVisible(false);
				}
				/**
				 * End
				 */
				/**@Sheetal : 16-02-2022
				 *    Des : Adding view lead,view quotation button**/
				if(text.equals(AppConstants.VIEWLEAD)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.VIEWQUOTATION)){
					label.setVisible(false);
				}
				/**
				 * Ajinkya added this code 
				 * Date:1/08/2017
				 */
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				/**
				 * End here
				 */
				
				/** date 12-09-2017 added by vijay for copy Contract ****/
				if(text.equals(AppConstants.COPYCONTRACT)){
					label.setVisible(false);
				}
				
				/** date 13-09-2017 added by vijay for customer address ****/
				if(text.equals(AppConstants.CUSTOMERADDRESS)){
					label.setVisible(false);
				}
				/** date 07-11-2017 added by ANIL for Upload T&C ****/
				if(text.equals(AppConstants.UPLOADTERMSANDCONDITION)){
					label.setVisible(false);
				}
				
				/** date 09-04-2018 added by ANIL  ****/
				if(text.equals(AppConstants.LICENSETYPELIST.UPDATE_CLIENTS_LICENSE)){
					label.setVisible(false);
				}
				
				/** date 19-04-2018 added by ANIL ****/
				if(text.equals(AppConstants.VIEWSALESORDER)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.CONTRACTDISCOUNTINUED)){
					label.setVisible(true);
				}

				
				if(text.equals(AppConstants.MAKERENEWABLE)){
					label.setVisible(false);
				}
				
				if(text.equals(AppConstants.SERVICERECORD)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.UPDATESERVICEVALUE)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.UPDATETECHNICIANNAME)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.CONTRACTRESET)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.CONTRACTRESET)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.DELETECLIENTLICENSE)){
						label.setVisible(false);
				}
				
				if(text.equals(AppConstants.VIEWCOMPLAIN)){
					label.setVisible(false);
				}
			}

			else if(status.equals(Contract.CONTRACTCANCEL))
			{
				if(text.equals("Print Renewal Reminder")){
					label.setVisible(false);
				}
				if(text.equals("Email Renewal Reminder")){
					label.setVisible(false);
				}
				if(text.equals(ManageApprovals.APPROVALREQUEST))
					label.setVisible(false);
				if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					label.setVisible(false);
				
				if((text.contains(AppConstants.NEW)))
					label.setVisible(true);
				else
					label.setVisible(false);
				
//				if(text.equals(AppConstants.CANCLECONTRACT))
//					label.setVisible(false);
				if(text.equals(AppConstants.CONTRACTRENEWAL))
					label.setVisible(false);
				if(text.equals(AppConstants.PRINTJOBCARD))
					label.setVisible(false);
//				if(text.equals(AppConstants.MANAGEPROJECT))
//					label.setVisible(false);
				
				if(text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(true);
				else if(text.equals(AppConstants.WORKORDER))
					label.setVisible(false);
				if(text.equals(AppConstants.CUSTOMERINTEREST))
					label.setVisible(false);
				if(text.equals(AppConstants.MATERIALREQUIREDREPORT))
					label.setVisible(false);
				
				if(text.equals(AppConstants.CREATESERVICE)) //vijay
					label.setVisible(false);
				if(text.equals("CreateServices"))   //rohan
					label.setVisible(false);
				if(text.equals(AppConstants.CANCLECONTRACT))
					label.setVisible(false);
				if(UserConfiguration.getRole().getRoleName().equals("Admin")){
				if(text.equals(AppConstants.CANCELSERVICES))
					label.setVisible(true);
				}
				/*********Done by Rahul on 15-10-2016 only for NBHC******/
				if(text.equals(AppConstants.UPDATEPRICES))
					label.setVisible(false);
				
				
				/**
				 * Date : 14-03-2017 By Anil
				 * FAR,FMR and CHR PDF
				 * NBHC CCPM
				 */
				if(text.equals("FAR")){
					label.setVisible(false);
				}
				if(text.equals("FMR")){
					label.setVisible(false);
				}
				if(text.equals("CHR")){
					label.setVisible(false);
				}
				if(text.equals("Create EVA Services")){
					label.setVisible(false);
				}
				
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWBILL)){
					label.setVisible(false);
				}
				/**
				 * End
				 */
				/**@Sheetal : 16-02-2022
				 *    Des : Adding view lead,view quotation button**/
				if(text.equals(AppConstants.VIEWLEAD)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.VIEWQUOTATION)){
					label.setVisible(false);
				}
				/**
				 * Ajinkya added this code 
				 * Date:1/08/2017
				 */
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				/**
				 * End here
				 */
				
				/** date 12-09-2017 added by vijay for copy Contract ****/
				if(text.equals(AppConstants.COPYCONTRACT))
					label.setVisible(true);
				/** date 13-09-2017 added by vijay for customer address ****/
				if(text.equals(AppConstants.CUSTOMERADDRESS)){
					label.setVisible(false);
				}
				/** date 07-11-2017 added by ANIL for Upload T&C ****/
				if(text.equals(AppConstants.UPLOADTERMSANDCONDITION)){
					label.setVisible(false);
				}
				
				/** date 09-04-2018 added by ANIL  ****/
				if(text.equals(AppConstants.LICENSETYPELIST.UPDATE_CLIENTS_LICENSE)){
					label.setVisible(false);
				}
				
				/** date 19-04-2018 added by ANIL ****/
				if(text.equals(AppConstants.VIEWSALESORDER)){
					label.setVisible(false);
				}
				
				/**
				 * nidhi
				 * 18-05-2018
				 * 
				 */
				if(text.equals(AppConstants.CONTRACTDISCOUNTINUED)  ){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.VIEWINVOICEPAYMENT))
					label.setVisible(true);
				
				
				
				if(text.equals(AppConstants.MAKERENEWABLE)){
					label.setVisible(false);
				}
				
				if(text.equals(AppConstants.SERVICERECORD)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.UPDATESERVICEVALUE)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.UPDATETECHNICIANNAME)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.CONTRACTRESET)){
					label.setVisible(false);
				}
				
				if(text.equals(AppConstants.DELETECLIENTLICENSE)){
						label.setVisible(false);
				}
				
				if(text.equals(AppConstants.VIEWCOMPLAIN)){
					label.setVisible(false);
				}
			}
			
			else if(status.equals(Contract.REQUESTED))
			{
				
				if(text.equals(AppConstants.VIEWINVOICEPAYMENT))
					label.setVisible(true);
				if((text.contains(AppConstants.PRINTJOBCARD)||text.contains(AppConstants.PAYMENT)||
						text.contains(AppConstants.VIEWSERVICES)||text.contains(AppConstants.VIEWSERVICES)||
						text.contains(ManageApprovals.APPROVALREQUEST))||text.contains(AppConstants.CREATECONTRACT)||text.contains(AppConstants.CONTRACTRENEWAL))
					label.setVisible(false);
				else
					label.setVisible(true);
				
				if(text.equals(AppConstants.MATERIALREQUIREDREPORT))
					label.setVisible(false);
//				if(text.equals(AppConstants.CANCLECONTRACT))
//					label.setVisible(false);
				if(text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(false);
				if(text.equals(AppConstants.WORKORDER))
					label.setVisible(false);
				if(text.equals(AppConstants.CUSTOMERINTEREST))
					label.setVisible(false);
				
				if(text.equals("Monthly Service Record"))
					label.setVisible(false);
				
				if(text.equals(AppConstants.CREATESERVICE)) //vijay
					label.setVisible(false);
				
				if(text.equals("CreateServices"))   //rohan
					label.setVisible(false);
				
				if(text.equals(AppConstants.CANCLECONTRACT))
					label.setVisible(false);
				
				/*********Done by Rahul on 15-10-2016 only for NBHC******/
				if(text.equals(AppConstants.UPDATEPRICES))
					label.setVisible(false);
				
				if(text.equals(AppConstants.CANCELSERVICES))
					label.setVisible(false);
				
				
				/**
				 * Date : 14-03-2017 By Anil
				 * FAR,FMR and CHR PDF
				 * NBHC CCPM
				 */
				if(text.equals("FAR")){
					label.setVisible(false);
				}
				if(text.equals("FMR")){
					label.setVisible(false);
				}
				if(text.equals("CHR")){
					label.setVisible(false);
				}
				if(text.equals("Create EVA Services")){
					label.setVisible(false);
				}
				
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWBILL)){
					label.setVisible(false);
				}
				/**
				 * End
				 */
				/**@Sheetal : 16-02-2022
				 *    Des : Adding view lead,view quotation button**/
				if(text.equals(AppConstants.VIEWLEAD)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.VIEWQUOTATION)){
					label.setVisible(false);
				}
				/**
				 * Ajinkya added this code 
				 * Date:1/08/2017
				 */
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				/**
				 * End here
				 */
				
				/** date 12-09-2017 added by vijay for copy Contract ****/
				if(text.equals(AppConstants.COPYCONTRACT)){
					label.setVisible(false);
				}
				/** date 13-09-2017 added by vijay for customer address ****/
				if(text.equals(AppConstants.CUSTOMERADDRESS)){
					label.setVisible(false);
				}
				/** date 07-11-2017 added by ANIL for Upload T&C ****/
				if(text.equals(AppConstants.UPLOADTERMSANDCONDITION)){
					label.setVisible(false);
				}
				
				/** date 09-04-2018 added by ANIL  ****/
				if(text.equals(AppConstants.LICENSETYPELIST.UPDATE_CLIENTS_LICENSE)){
					label.setVisible(false);
				}
				
				/** date 19-04-2018 added by ANIL ****/
				if(text.equals(AppConstants.VIEWSALESORDER)){
					label.setVisible(false);
				}
				/**
				 * nidhi
				 * 18-05-2018
				 * 
				 */
				if(text.equals(AppConstants.CONTRACTDISCOUNTINUED)  ){
					label.setVisible(false);
				}
				if(text.equals("Print Renewal Reminder")){
					label.setVisible(false);
				}
				if(text.equals("Email Renewal Reminder")){
					label.setVisible(false);
				}
				
				
				
				if(text.equals(AppConstants.MAKERENEWABLE)){
					label.setVisible(false);
				}
				
				if(text.equals(AppConstants.SERVICERECORD)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.UPDATESERVICEVALUE)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.UPDATETECHNICIANNAME)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.CONTRACTRESET)){
					label.setVisible(false);
				}
				
				if(text.equals(AppConstants.DELETECLIENTLICENSE)){
						label.setVisible(false);
				}
				if(text.equals(AppConstants.VIEWCOMPLAIN)){
					label.setVisible(false);
				}
			}
			
			else if(status.equals(Contract.REJECTED))
			{
				if(text.equals(AppConstants.VIEWINVOICEPAYMENT))
					label.setVisible(true);
				
				if(text.equals("Print Renewal Reminder")){
					label.setVisible(false);
				}
				if(text.equals("Email Renewal Reminder")){
					label.setVisible(false);
				}
				if(text.contains(AppConstants.NEW))
					label.setVisible(true);
				else
					label.setVisible(false);
				
				if(text.equals(AppConstants.CANCELLATIONSUMMARY))
					label.setVisible(false);
				else if(text.equals(AppConstants.WORKORDER))
					label.setVisible(false);
				if(text.equals(AppConstants.CUSTOMERINTEREST))
					label.setVisible(false);
				if(text.equals(AppConstants.MATERIALREQUIREDREPORT))
					label.setVisible(false);
				
				if(text.equals("Monthly Service Record"))
					label.setVisible(false);
				
				if(text.equals(AppConstants.CREATESERVICE))
					label.setVisible(false); // vijay
				
				if(text.equals("CreateServices"))   //rohan
					label.setVisible(false);
				
				if(text.equals(AppConstants.CANCLECONTRACT))
					label.setVisible(false);
				
				/**
				 * nidhi
				 * 18-05-2018
				 * 
				 */
				if(text.equals(AppConstants.CONTRACTDISCOUNTINUED)  ){
					label.setVisible(false);
				}
				
				/*********Done by Rahul on 15-10-2016 only for NBHC******/
				if(text.equals(AppConstants.UPDATEPRICES))
					label.setVisible(false);
				
				if(text.equals(AppConstants.CANCELSERVICES))
					label.setVisible(false);
				
				/**
				 * Date : 14-03-2017 By Anil
				 * FAR,FMR and CHR PDF
				 * NBHC CCPM
				 */
				if(text.equals("FAR")){
					label.setVisible(false);
				}
				if(text.equals("FMR")){
					label.setVisible(false);
				}
				if(text.equals("CHR")){
					label.setVisible(false);
				}
				if(text.equals("Create EVA Services")){
					label.setVisible(false);
				}
				
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWBILL)){
					label.setVisible(false);
				}
				/**
				 * End
				 */
				/**@Sheetal : 16-02-2022
				 *    Des : Adding view lead,view quotation button**/
				if(text.equals(AppConstants.VIEWLEAD)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.VIEWQUOTATION)){
					label.setVisible(false);
				}
				/**
				 * Ajinkya added this code 
				 * Date:1/08/2017
				 */
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				/**
				 * End here
				 */
				
				/** date 12-09-2017 added by vijay for copy Contract ****/
				if(text.equals(AppConstants.COPYCONTRACT)){
					label.setVisible(false);
				}
				/** date 13-09-2017 added by vijay for customer address ****/
				if(text.equals(AppConstants.CUSTOMERADDRESS)){
					label.setVisible(false);
				}
				/** date 07-11-2017 added by ANIL for Upload T&C ****/
				if(text.equals(AppConstants.UPLOADTERMSANDCONDITION)){
					label.setVisible(false);
				}
				
				/** date 09-04-2018 added by ANIL  ****/
				if(text.equals(AppConstants.LICENSETYPELIST.UPDATE_CLIENTS_LICENSE)){
					label.setVisible(false);
				}
				
				/** date 19-04-2018 added by ANIL ****/
				if(text.equals(AppConstants.VIEWSALESORDER)){
					label.setVisible(false);
				}
				
				if(text.equals(AppConstants.MAKERENEWABLE)){
					label.setVisible(false);
				}
				
				if(text.equals(AppConstants.SERVICERECORD)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.UPDATESERVICEVALUE)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.UPDATETECHNICIANNAME)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.CONTRACTRESET)){
					label.setVisible(false);
				}
				
				if(text.equals(AppConstants.DELETECLIENTLICENSE)){
						label.setVisible(false);
				}
				if(text.equals(AppConstants.VIEWCOMPLAIN)){
					label.setVisible(true);
				}
			}
			else if(status.equals(Contract.APPROVED)&&donotrenewStatus==true)
			{
				if(text.equals(AppConstants.CUSTOMERINTEREST)){
					label.setVisible(false);
				}
				else if(text.equals(AppConstants.CONTRACTRENEWAL)){
					label.setVisible(true);//Ashwini Patil Date:18-04-2023
				}
				else if(text.equals(ManageApprovals.APPROVALREQUEST)){
					label.setVisible(false);
				}
				else if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST)){
					label.setVisible(false);
				}
				else if(text.equals(AppConstants.CANCELLATIONSUMMARY)){
					label.setVisible(false);
				}
				else if(text.equals(AppConstants.MATERIALREQUIREDREPORT))
					label.setVisible(true);
				else{
					label.setVisible(true);
				}
				if(text.equals("Monthly Service Record"))
					label.setVisible(true);
				
				if(text.equals(AppConstants.CREATESERVICE))  //vijay
					label.setVisible(true);
				
				if(text.equals("CreateServices"))   //rohan
					label.setVisible(false);
				/**
				 * Updated By: Viraj
				 * Date: 25-02-2019
				 * Description: To show cancel contract button
				 */
				if(text.equals(AppConstants.CANCLECONTRACT))
					label.setVisible(true);
				/** Ends **/
				
				/*********Done by Rahul on 15-10-2016 only for NBHC******/
				if(text.equals(AppConstants.UPDATEPRICES))
					label.setVisible(false);
				
				if(text.equals(AppConstants.CANCELSERVICES))
					label.setVisible(false);
				
				/**
				 * Date : 14-03-2017 By Anil
				 * FAR,FMR and CHR PDF
				 * NBHC CCPM
				 */
				if(text.equals("FAR")){
					label.setVisible(true);
				}
				if(text.equals("FMR")){
					label.setVisible(true);
				}
				if(text.equals("CHR")){
					label.setVisible(true);
				}
				if(text.equals("Create EVA Services")){
					label.setVisible(true);
				}
				
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWBILL)){
					label.setVisible(true);
				}
				/**
				 * End
				 */
				/**@Sheetal : 16-02-2022
				 *    Des : Adding view lead,view quotation button**/
				if(text.equals(AppConstants.VIEWLEAD)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.VIEWQUOTATION)){
					label.setVisible(true);
				}
				/**
				 * Ajinkya added this code 
				 * Date:1/08/2017
				 */
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				/**
				 * End here
				 */
				
				/** date 12-09-2017 added by vijay for copy Contract ****/
				if(text.equals(AppConstants.COPYCONTRACT)){
					label.setVisible(false);
				}
				
				/** date 13-09-2017 added by vijay for customer address ****/
				if(text.equals(AppConstants.CUSTOMERADDRESS)){
					label.setVisible(true);
				}
				/** date 07-11-2017 added by ANIL for Upload T&C ****/
				if(text.equals(AppConstants.UPLOADTERMSANDCONDITION)){
					label.setVisible(true);
				}
				
				/** date 09-04-2018 added by ANIL  ****/
				if(text.equals(AppConstants.LICENSETYPELIST.UPDATE_CLIENTS_LICENSE)){
					if(clientLicencesUpdateFlag) 
						label.setVisible(true);
					else
						label.setVisible(false);
				}
				/** date 19-04-2018 added by ANIL ****/
				if(text.equals(AppConstants.VIEWSALESORDER)){
					label.setVisible(true);
				}
				
				/** Date 02-10-2019 by Vijay for NBHC CCPM ContractDiscotinue to zonal coordinator **/
				if(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") ||
						LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("Zonal Coordinator") ){
					if(text.equals(AppConstants.CONTRACTDISCOUNTINUED)){
						label.setVisible(true);
					}
					if(text.equals(AppConstants.CONTRACTRESET)){
						label.setVisible(true);
					}
				}
				else{
					if(text.equals(AppConstants.CONTRACTDISCOUNTINUED)){
						label.setVisible(false);
					}
					if(text.equals(AppConstants.CONTRACTRESET)){
						label.setVisible(false);
					}
				}
				if(text.equals("Print Renewal Reminder")){
					label.setVisible(false);
				}
				if(text.equals("Email Renewal Reminder")){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.VIEWINVOICEPAYMENT))
					label.setVisible(true);
				
				if(makeRenewalFlag){
					if(text.equals(AppConstants.MAKERENEWABLE)){
						label.setVisible(true);
					}
				}
				else{
					if(text.equals(AppConstants.MAKERENEWABLE)){
						label.setVisible(false);
					}
				}
				
				if(text.equals(AppConstants.SERVICERECORD)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.UPDATESERVICEVALUE)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.UPDATETECHNICIANNAME)){
					label.setVisible(true);
				}				
				if(text.equals(AppConstants.DELETECLIENTLICENSE)){
					if(clientLicencesUpdateFlag) 
						label.setVisible(true);
					else
						label.setVisible(false);
				}
				
				if(text.equals(AppConstants.VIEWCOMPLAIN)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.UpdateSalesPerson)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.AddCustomerBranches)&&AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableAddCustomerBranchesOption")&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")) {
					label.setVisible(true);				
				}else if(text.equals(AppConstants.AddCustomerBranches)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.markcontractasrenewed)&&AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "ShowMarkRenewedButton")) {
					label.setVisible(true);				
				}else if(text.equals(AppConstants.markcontractasrenewed)){
					label.setVisible(false);
				}
			}
			
			/**
			 * Date 06-07-2018 By Vijay
			 * Des :- for discountinue contract 
			 */

			else if(status.equals("Discontinued")){

				if(text.equals(AppConstants.CUSTOMERINTEREST)){
					label.setVisible(false);
				}
				else if(text.equals(AppConstants.CONTRACTRENEWAL)){
					label.setVisible(false);
				}
				else if(text.equals(ManageApprovals.APPROVALREQUEST)){
					label.setVisible(false);
				}
				else if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST)){
					label.setVisible(false);
				}
				else if(text.equals(AppConstants.CANCELLATIONSUMMARY)){
					label.setVisible(false);
				}
				else if(text.equals(AppConstants.MATERIALREQUIREDREPORT))
					label.setVisible(false);
				
				if(text.equals("Monthly Service Record"))
					label.setVisible(false);
				
				if(text.equals(AppConstants.CREATESERVICE))  //vijay
					label.setVisible(false);
				
				if(text.equals("CreateServices"))   //rohan
					label.setVisible(false);
				
				if(text.equals(AppConstants.CANCLECONTRACT))
					label.setVisible(false);
				
				/*********Done by Rahul on 15-10-2016 only for NBHC******/
				if(text.equals(AppConstants.UPDATEPRICES))
					label.setVisible(false);
				
				if(text.equals(AppConstants.CANCELSERVICES))
					label.setVisible(false);
				
				/**
				 * Date : 14-03-2017 By Anil
				 * FAR,FMR and CHR PDF
				 * NBHC CCPM
				 */
				if(text.equals("FAR")){
					label.setVisible(false);
				}
				if(text.equals("FMR")){
					label.setVisible(false);
				}
				if(text.equals("CHR")){
					label.setVisible(false);
				}
				if(text.equals("Create EVA Services")){
					label.setVisible(false);
				}
				
				/**
				 * Date : 29-07-2017 By ANIL
				 */
				if(text.equals(AppConstants.VIEWBILL)){
					label.setVisible(false);
				}
				/**
				 * End
				 */
				/**@Sheetal : 16-02-2022
				 *    Des : Adding view lead,view quotation button**/
				if(text.equals(AppConstants.VIEWLEAD)){
					label.setVisible(true);
				}
				if(text.equals(AppConstants.VIEWQUOTATION)){
					label.setVisible(true);
				}
				/**
				 * Ajinkya added this code 
				 * Date:1/08/2017
				 */
				if(text.equals(ManageApprovals.SUBMIT))
					label.setVisible(false);
				/**
				 * End here
				 */
				
				/** date 12-09-2017 added by vijay for copy Contract ****/
				if(text.equals(AppConstants.COPYCONTRACT)){
					label.setVisible(false);
				}
				
				/** date 13-09-2017 added by vijay for customer address ****/
				if(text.equals(AppConstants.CUSTOMERADDRESS)){
					label.setVisible(false);
				}
				/** date 07-11-2017 added by ANIL for Upload T&C ****/
				if(text.equals(AppConstants.UPLOADTERMSANDCONDITION)){
					label.setVisible(false);
				}
				
				/** date 09-04-2018 added by ANIL  ****/
				if(text.equals(AppConstants.LICENSETYPELIST.UPDATE_CLIENTS_LICENSE)){
					label.setVisible(false);
				}
				
				/** date 19-04-2018 added by ANIL ****/
				if(text.equals(AppConstants.VIEWSALESORDER)){
					label.setVisible(false);
				}
			
				if(text.equals(AppConstants.VIEWSERVICES)){
					label.setVisible(false);
				}
				if(text.equals(manageapproval.APPROVALSTATUS)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.CONTRACTDISCOUNTINUED)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.WORKORDER)){
					label.setVisible(false);
				}
				if(text.equals("Print Renewal Reminder")){
					label.setVisible(false);
				}
				if(text.equals("Email Renewal Reminder")){
					label.setVisible(false);
				}
				
				if(text.equals(AppConstants.MAKERENEWABLE)){
					label.setVisible(false);
				}
				
				if(text.equals(AppConstants.SERVICERECORD)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.UPDATESERVICEVALUE)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.UPDATETECHNICIANNAME)){
					label.setVisible(false);
				}
				if(text.equals(AppConstants.CONTRACTRESET)){
					label.setVisible(false);
				}
				
				if(text.equals(AppConstants.DELETECLIENTLICENSE)){
						label.setVisible(false);
				}
				if(text.equals(AppConstants.VIEWCOMPLAIN)){
					label.setVisible(true);
				}
			}
			
			/**
			 * ends here
			 */
			
			/**
			 * @author Anil , Date : 28-05-2019
			 */
			if(deleteServFlag&&status.equals("Approved")&&UserConfiguration.getRole()!=null&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")&&text.equals("Delete duplicate services")){
				label.setVisible(true);
			}else if(text.equals("Delete duplicate services")){
				label.setVisible(false);
			}
			/**
			 * @author Vijay Chougule 
			 * Des :- NBHC CCPM Updating services service branch
			 */
			if(updateServiceBranchFlag && status.equals("Approved")&&LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
				if(text.equals("UpdateServiceBranches"))
				label.setVisible(true);
				
				if(text.equals("Print Renewal Reminder")){
					label.setVisible(false);
				}	
			}
			else if(text.equals("UpdateServiceBranches")){
				label.setVisible(false);
			}
			/**
			 * @author Anil @since 03-02-2021
			 * As per Nitin sir instruction removing process configuration
			 */
//			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "ContractRenewalNewFormat")){
			if(status.equals(Contract.CREATED)&&renewalFlag==true){	
				Console.log("Inside my Condition");
				
				if(getManageapproval().isSelfApproval()){
					if(text.equals("Print Renewal Reminder")){
						label.setVisible(true);
					}	
				}else{
					if(text.equals("Print Renewal Reminder")){
						label.setVisible(true);
					}
				}
				if(text.equals(AppConstants.CANCLECONTRACT)){
					label.setVisible(true);
				}
				
				if(text.equals("Email Renewal Reminder")){
					label.setVisible(true);
				}
			}
//			}
			
			/**
			 * @author Anil
			 * @since 29-06-2020
			 */
			if(status.equals(Contract.APPROVED)){
				if(text.equals("Update Account Manager")){
					label.setVisible(true);
				}
			}else{
				if(text.equals("Update Account Manager")){
					label.setVisible(false);
				}
			}
			//Ashwini Patil Date:20-09-2022
			if(isPopUpAppMenubar()) {
				
				if(text.equalsIgnoreCase(AppConstants.VIEWINVOICEPAYMENT)||text.equalsIgnoreCase(AppConstants.COPYCONTRACT)){
					label.setVisible(false);	
				}
			}
		    //Ashwini Patil Date:20-09-2022
			if(isHideNavigationButtons()) {
				Console.log("in changeProcessLevel isHideNavigationButtons");
				if(text.equalsIgnoreCase(AppConstants.NEW)
						||text.equalsIgnoreCase(AppConstants.VIEWSERVICES)
						||text.equalsIgnoreCase(AppConstants.VIEWBILL)
						||text.equalsIgnoreCase(AppConstants.VIEWLEAD)
						||text.equalsIgnoreCase(AppConstants.WORKORDER)
						||text.equalsIgnoreCase(AppConstants.VIEWQUOTATION)
						||text.equalsIgnoreCase(AppConstants.VIEWINVOICEPAYMENT)
						||text.equalsIgnoreCase(AppConstants.VIEWSALESORDER)
						||text.equalsIgnoreCase(AppConstants.CONTRACTRENEWAL)
						||text.equalsIgnoreCase(AppConstants.COPYCONTRACT)
						||text.equalsIgnoreCase(AppConstants.CONTRACTRESET)
						||text.equalsIgnoreCase("Copy")
						||text.equalsIgnoreCase(AppConstants.VIEWINVOICEPAYMENT)){
					label.setVisible(false);	
				}
			}
		}
		Console.log("INSIDE TOGGLE PROCESS LEVEL MENU END...");
		
	}
	/**
	 * Sets the Application Header Bar as Per Status
	 */

	public void setAppHeaderBarAsPerStatus()
	{
		System.out.println("INSIDE APP HEADER");
		Contract entity=(Contract) presenter.getModel();
		String status=entity.getStatus();
		
		if(status.equals(Contract.CONTRACTEXPIRED)||status.equals(Contract.CONTRACTCANCEL)||status.equals(Contract.REJECTED)||status.equals(Contract.APPROVED)||status.equals(Contract.CONTRACTDISCOUNTINED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.equals(AppConstants.NAVIGATION) || text.contains(AppConstants.COMMUNICATIONLOG) || text.contains(AppConstants.MESSAGE))/** date 4.1.19 added by komal**/
				{
					menus[k].setVisible(true); 
					System.out.println("Value of text is "+menus[k].getText());
				}
				else
				{
					menus[k].setVisible(false);  
				}
			}
		}

		if(status.equals(Contract.APPROVED))
		{
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				/**
				 * Date : 10-04-2018 By ANIL
				 * Only for EVA ,we are allowing to edit contract after approved state for 
				 * updating contract start and end date and no of user( unit) 
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForEVA")
						&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
					if (text.contains("Discard") || text.contains("Search")
							|| text.contains("Email") || text.contains("Print")
							|| text.equals(AppConstants.NAVIGATION)|| text.equals("Edit") || text.contains(AppConstants.COMMUNICATIONLOG) || text.contains(AppConstants.MESSAGE))/** date 4.1.19 added by komal**/ 
					{
						menus[k].setVisible(true);
					} else {
						menus[k].setVisible(false);
					}
				}else{
					if (text.contains("Discard") || text.contains("Search")
							|| text.contains("Email") || text.contains("Print")
							|| text.equals(AppConstants.NAVIGATION) || text.contains(AppConstants.COMMUNICATIONLOG) || text.contains(AppConstants.MESSAGE))/** date 4.1.19 added by komal**/ 
					{
						menus[k].setVisible(true);
					} else {
						menus[k].setVisible(false);
					}
				}
			}
		}
		
		if(status.equals(Contract.REQUESTED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.equals(AppConstants.NAVIGATION) || text.contains(AppConstants.COMMUNICATIONLOG))/** date 4.1.19 added by komal**/
				{
					menus[k].setVisible(true); 

				}
				else
				{
					menus[k].setVisible(false);  
				}
			}
		}
		
		if(status.equals(Contract.REJECTED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains("Edit")||text.equals(AppConstants.NAVIGATION)|| text.contains(AppConstants.COMMUNICATIONLOG))/** date 4.1.19 added by komal**/
				{
					menus[k].setVisible(true); 

				}
				else
				{
					menus[k].setVisible(false);  
				}
			}
		}
		
	/*	if(status.equals(Contract.CREATED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Save")
						||text.contains("Edit"))
				{
					menus[k].setVisible(true); 

				}
				else
				{
					menus[k].setVisible(false);  
				}
			}
		}
*/


		/*else if(status.equals(Contract.CREATED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")
						||text.contains("Print")||text.contains("Save")||text.contains("Edit"))
				{
					menus[k].setVisible(true); 

				}
				else
				{
					menus[k].setVisible(false);  
				}
			}
		}*/
	}
	/**
	 * The method is responsible for changing Application state as per 
	 * status
	 */
	public void setMenuAsPerStatus()
	{    
		this.setAppHeaderBarAsPerStatus();
		this.toggleProcessLevelMenu();
		
	}
	
	
   

	
	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		tbContractId.setValue(count+"");
	}

/****************************************On Click Mehtod*******************************************/
	/**
	 * This method is called on click of any widget.
	 * 
	 * 	Involves Payment Terms Button  (ADD)
	 *  Involves Add Charges Button (Plus symbol)
	 * 	Involves OtmComposite i.e Products which are added
	 */
	
	
	
	@Override
	public void onClick(ClickEvent event) 
	{
		if(event.getSource().equals(mailpopup.getLblOk())){
			Console.log("inside mailpopup ok button");
			validateCustomerEmailId(ContractPresenter.custcount);
		}
		
		
		if(event.getSource().equals(chkbillingatBranch)){
			System.out.println("Here clicked");
			Console.log("clicked on checkbox ");
			if(chkbillingatBranch.getValue()==true){
				/**
				 * Date : 30-10-2017 BY ANIL
				 */
				chkservicewithBilling.setValue(false);
				cbServiceWiseBilling.setValue(false);
				/**
				 * End
				 */
				System.out.println("If true===");
				Console.log("checkbox true");
				List<PaymentTerms> paymentTermsTablelist = paymentTermsTable.getDataprovider().getList();
				if(paymentTermsTablelist.size()==0){
					showDialogMessage("Please add Payment Terms");
				}else{
					checkcustomerbranch();
				}
				
			}
			if(chkbillingatBranch.getValue()==false){
				System.out.println("checkbox false");
				paymentTermsTable.getDataprovider().getList().clear();
			}
		}
		
		
		if(event.getSource().equals(addPaymentTerms)){
			reactOnAddPaymentTerms();
		}
		if(event.getSource().equals(addOtherCharges)){
			int size=chargesTable.getDataprovider().getList().size();
			if(this.doamtincltax.getValue()!=null){
				ProductOtherCharges prodCharges=new ProductOtherCharges();
				prodCharges.setAssessableAmount(this.getDoamtincltax().getValue());
				prodCharges.setFlagVal(false);
				prodCharges.setIndexCheck(size+1);
				this.chargesTable.getDataprovider().getList().add(prodCharges);
			}
		}
		if(event.getSource().equals(baddproducts)){
//			changeProductComposites=true;
			/**
			 * @author Vijay Date 08-02-2021
			 * Des when new customer created from customer then customer not loaded so updated code to load customer Master
			 * @author Anil
			 * @since 14-02-2022
			 * Becuase of this method user is not able add more than one product in contract, screen got freez
			 * Raised by Nithila for Innovative
			 */
			if(cust==null) //By: Ashwini Patil as we were unable to add products
			checkCustomerStatus(getPersonInfoComposite().getIdValue(),true,true);

			if (AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ChangeProductComposite")) {
				changeProductComposites = true;
			}else{
				changeProductComposites = false;
			}
			Console.log("Inside ADD"+changeProductComposites);
			
			if(changeProductComposites){
				Console.log("Inside Chnage product composite process config");
			reactOnAddServiceProduct();
			}else{
			if(personInfoComposite.getId().getValue().equals("")){
				showDialogMessage("Please add customer information!");
			}
			
			/**
			 * Date 24/2/2018
			 * By Jayshree
			 * Add the new dialog message
			 */
			else if(dbContractDate.getValue()==null){
				System.out.println("Inside contract start date");
				showDialogMessage("Please add Contract Date!");
			}
			//End By jayshree
			else{
				if(olbbBranch.getSelectedIndex()!=0){
					if(!prodInfoComposite.getProdCode().getValue().equals("")){
						System.out.println("PrintPCD"+prodInfoComposite.getProdCode().getValue());
//						this.checkCustomerBranch(personInfoComposite.getIdValue());
						
						//  this code is commented by rohan for pesto india changes 
						
//						boolean productValidation=validateProductDuplicate(prodInfoComposite.getProdCode().getValue().trim());
//						
//						if(productValidation==true){
//							showDialogMessage("Product already exists!");
//						}
//						else{
	
						/**
						 * Date 15-10-2018 By Vijay
						 * Des :- If Payment Terms already added then i am clearing Payment Terms becuase
						 * based on the Product Duration Payment Terms will Create So i have updated the code
						 */
						if(this.olbPaymentTerms.getSelectedIndex()!=0 && this.paymentTermsTable.getDataprovider().getList().size()!=0){
							this.paymentTermsTable.clear();
						}
						/**
						 * ends here
						 */
						
						try {
							DateTimeFormat dateformat = DateTimeFormat.getFormat("dd-MM-yyyy");
							String strDate = dateformat.format(dbContractDate.getValue());
							if(strDate.length()>10 || strDate.length()<10){
								GWTCAlert alert = new GWTCAlert();
								alert.alert("Invalid Contract Date Format! ");
								return;
							}
						} catch (Exception e) {
							// TODO: handle exception
						}
						
						/**
						 * @author Vijay Date 08-02-2021
						 * Des when new customer created from customer then customer not loaded so updated code to load customer Master
						 */
						if(cust==null){
							Console.log("in producttype customer null");
							 Timer timer = new Timer() {
									
									@Override
									public void run() {
										// TODO Auto-generated method stub
										ProdctType(prodInfoComposite.getProdCode().getValue().trim(),prodInfoComposite.getProdID().getValue().trim());
										prodInfoComposite.clear();
									}
									
								};
								timer.schedule(3000);
						}
						else{
							this.ProdctType(this.prodInfoComposite.getProdCode().getValue().trim(),this.prodInfoComposite.getProdID().getValue().trim());
							/**
							 * Date : 12-10-2017 BY ANIL
							 * After adding product ,product composite should be clear
							 */
							prodInfoComposite.clear();
						}
						
							
//						}
					}
				}else{
					showDialogMessage("Please select branch !");
				}
			}
			}
		}
		
		// *********vaishnavi************
		if (event.getSource().equals(addPayTerms)) {
			
			reactOnAddPayTerms();
		}
		
		/**
		 * Date : 06-10-2017 BY ANIL
		 */
		if(event.getSource().equals(cbServiceWiseBilling)){
			if(cbServiceWiseBilling.getValue().equals(true)){
				paymentTermsTable.connectToLocal();
				addPaymentTerms.setEnabled(false);
				addPayTerms.setEnabled(false);
				/**
				 * Date : 30-10-2017  BY ANIL
				 */
				chkservicewithBilling.setValue(false);
				chkbillingatBranch.setValue(false);
				/**
				 * End
				 */
				
			}else{
				addPaymentTerms.setEnabled(true);
				addPayTerms.setEnabled(true);
			}
		}
			
			
	}

	
	private void reactOnAddServiceProduct() {
		Console.log("Inside reactOnService product");
		if (olbProductName.getSelectedIndex() == 0
				&&olbProductGroup.getSelectedIndex() == 0) {
			Console.log("Inside please select Product");
			showDialogMessage("Please select Product Group And Product Name!");
		}
		
        if(olbProductGroup.getSelectedIndex()!=0&&olbProductName.getSelectedIndex() == 0){
        	showDialogMessage("Please Select Product Name");
        }
        if(olbProductGroup.getSelectedIndex()==0&&olbProductName.getSelectedIndex()!=0){
        	showDialogMessage("Please Select Product Group");
        }
		ServiceProduct serProduct = olbProductName.getSelectedItem();

		Console.log("serice product code" + serProduct);

		if (olbProductName.getSelectedIndex() != 0&&olbProductGroup.getSelectedIndex() != 0) {
			Console.log("Inside the add method");
//			boolean productValidation = validateProductDuplicate(
//					serProduct.getCount(), serProduct.getProductCode());
//			if (productValidation == true) {
//				showDialogMessage("Product already exists!");
//			} else {
				Console.log("Inside Else");
				ProdctType(serProduct.getProductCode(),String.valueOf(serProduct.getCount()));
				
//			}
		}
	}

	private void reactOnAddPayTerms() {
		if (olbPaymentTerms.getSelectedIndex() == 0) {
			showDialogMessage("Please select payment terms");
			return;
		}

		if (olbPaymentTerms.getSelectedIndex() != 0) {

			/**
			 *  nidhi
			 *  2-04-2018
			 *  for payment days validation as contract duration 
			 */
			int conDuration = getDurationFromProductTable();
			/**
			 *  end
			 */
			
			ConfigCategory conCategory = olbPaymentTerms.getSelectedItem();
			if (conCategory != null) {

				boolean flag = true;
				if(olbPaymentTerms.getValue(olbPaymentTerms.getSelectedIndex()).equalsIgnoreCase(AppConstants.MONTHLYPARTIALFIRSTMONTH) ||
						olbPaymentTerms.getValue(olbPaymentTerms.getSelectedIndex()).equalsIgnoreCase(AppConstants.MONTHLYPREVIOUSMONTHBILLINGPERIOD)){
					flag = false;
				}
				
				if(olbPaymentTerms.getValue(olbPaymentTerms.getSelectedIndex()).equalsIgnoreCase(AppConstants.MONTHLYPREVIOUSMONTHBILLINGPERIOD)){
					if(dbPaymentDate.getValue()==null){
						showDialogMessage("Please add Payment Date!");
						return;
					}
					
				}
				
				String str = conCategory.getDescription();
				if(flag){
					if (str.equals("")) {
						showDialogMessage("Please add Payment Days.");
						return;
					}
				}

				int days = Integer.parseInt(str);
				System.out.println("Payment terms Days::::::::::::::" + days);
				if(flag==false){
					days = 30;
				}
				
				if (saleslineitemtable.getDataprovider().getList().size() == 0) {
					showDialogMessage("Please add Product");
					return;
				}

				int duration = getDurationFromProductTable();
				System.out.println("Duration::::::::: " + duration);

				if (duration == 0) {
					showDialogMessage("Please add duration!");
					return;
				}

				int noOfPayTerms = duration / days;
				System.out.println("No. of payment terms::::::::: "+ noOfPayTerms);

				if (noOfPayTerms == 0) {
					noOfPayTerms = 1;
				}

				Float no = (float) noOfPayTerms;
				
				System.out.println("No. Value::::::::: "+ no);
				
				float percent1 = 100 / no;
				System.out.println("Percent1::::::::: " + percent1);

				Double percent = (double) percent1;
				//rohan commentd this line for Date : 04/10/2017 
//				percent = Double.parseDouble(nf.format(percent));
				System.out.println("Percent2:::::::::  " + percent);

				int day = 0;
				double totalPer = 0;

				if (cbStartOfPeriod.getValue() == true) {
					day = 0;
				}

				if (cbStartOfPeriod.getValue() == false) {
					day = days;
				}

				/**
				 * Date : 08-08-2017 BY ANIL
				 * 
				 */
				if(dbPaymentDate.getValue()!=null){
					if(days<30&&noOfPayTerms>1){
						showDialogMessage("Payment term duration should be greater than 30 days for using payment date option.");
						return;
					}
				}
				int monthCounter=0;
				
				if(days==30){
					monthCounter=1;
				}else if(days==60){
					monthCounter=2;
				}else if(days==90){
					monthCounter=3;
				}else if(days==120){
					monthCounter=4;
				}else if(days==150){
					monthCounter=5;
				}else if(days==180){
					monthCounter=6;
				}else if(days==210){
					monthCounter=7;
				}else if(days==240){
					monthCounter=8;
				}else if(days==270){
					monthCounter=9;
				}else if(days==300){
					monthCounter=10;
				}else if(days==330){
					monthCounter=11;
				}else if(days>360&&days<=366){
					monthCounter=12;
				}
				Date paymentDate=null;
				if(dbPaymentDate.getValue()!=null){
					paymentDate=CalendarUtil.copyDate(dbPaymentDate.getValue());
				}
				/**
				 * End
				 */
				ArrayList<PaymentTerms> ptermsList = new ArrayList<PaymentTerms>();
				
				Date invoiceDate = this.dbContractDate.getValue();

				System.out.println("month counter "+monthCounter);
				for (int i = 1; i <= noOfPayTerms; i++) {
					Date paymentDt=CalendarUtil.copyDate(paymentDate);
					
					
					totalPer = totalPer + percent;
					System.out.println("Rohan ABC== "+percent);
					System.out.println("Rohan xyz =="+totalPer);
					
					/**
					 * @author Vijay Date :- 13-01-2022
					 * Des :- updated code calculating days as per calendar Date 
					 */
					if(olbPaymentTerms.getSelectedIndex()!=0 ){
						if(this.dbContractDate.getValue()==null){
							showDialogMessage("Please add contract date first!");
							return;
						}
						System.out.println("i"+i);
						System.out.println(invoiceDate);
						
						int k=0;
						
						if(olbPaymentTerms.getValue(olbPaymentTerms.getSelectedIndex()).equalsIgnoreCase("Half Yearly")){
							k = 6;
						}
						else if(olbPaymentTerms.getValue(olbPaymentTerms.getSelectedIndex()).equalsIgnoreCase("Quartelry")){
							k = 3;
						}
						else if(olbPaymentTerms.getValue(olbPaymentTerms.getSelectedIndex()).equalsIgnoreCase("Bi-Monthly")){
							k = 2;
						}
						else if(olbPaymentTerms.getValue(olbPaymentTerms.getSelectedIndex()).equalsIgnoreCase("Monthly") ||
								olbPaymentTerms.getValue(olbPaymentTerms.getSelectedIndex()).equalsIgnoreCase(AppConstants.MONTHLYPREVIOUSMONTHBILLINGPERIOD)){
							k = 1;
						}
						else if(olbPaymentTerms.getValue(olbPaymentTerms.getSelectedIndex()).equalsIgnoreCase("Monthly (Partial First Month)")){
							k = 1;
						}
						
						if (cbStartOfPeriod.getValue() == true ) {
							
							if(olbPaymentTerms.getValue(olbPaymentTerms.getSelectedIndex()).equalsIgnoreCase("Advance") ||
									olbPaymentTerms.getValue(olbPaymentTerms.getSelectedIndex()).equalsIgnoreCase("Yearly")){
								k = 0;
							}
							
							if(i==1){
								k=0;
							}
							CalendarUtil.addMonthsToDate(invoiceDate, k);

							if(olbPaymentTerms.getValue(olbPaymentTerms.getSelectedIndex()).equalsIgnoreCase("Monthly (Partial First Month)")){
								if(i==2){
									System.out.println("invoiceDate == "+invoiceDate);
									CalendarUtil.setToFirstDayOfMonth(invoiceDate);
									System.out.println("firt month of day"+invoiceDate);
								}
							}
							
						}
						else{
							
							if(olbPaymentTerms.getValue(olbPaymentTerms.getSelectedIndex()).equalsIgnoreCase("Advance")){
								k = 0;
							}
							else if(olbPaymentTerms.getValue(olbPaymentTerms.getSelectedIndex()).equalsIgnoreCase("Yearly")){
								k = 12;
							}
							
							CalendarUtil.addMonthsToDate(invoiceDate, k);

							if(olbPaymentTerms.getValue(olbPaymentTerms.getSelectedIndex()).equalsIgnoreCase("Monthly (Partial First Month)")){
								if(i==1){
									System.out.println("invoiceDate == "+invoiceDate);
									CalendarUtil.setToFirstDayOfMonth(invoiceDate);
									System.out.println("firt month of day"+invoiceDate);
								}
							}
						}
						System.out.println("invoiceDate"+invoiceDate);
						Console.log("invoiceDate"+invoiceDate);
						int difference = getDifferencebetweenDate(this.dbContractDate.getValue(),invoiceDate);
						Console.log("difference"+difference);
						System.out.println("difference"+difference);
						day = difference;
					}
					/**
					 * @author Vijay Date-21-08-2023
					 * Des :- if we added duration more than 365 then next payment term showing only 365 so commented the same
					 */
//					if(day>365){
//						day = 365;
//					}
					/**
					 * ends here
					 */
					if (i == noOfPayTerms) {
						if (totalPer == 100) {
							
							/**
							 *  nidhi
							 *  2-04-2018
							 *  for payment days validation as contract duration 
							 */
							if(conDuration<day && i == 1){
								day = conDuration;
							}
							/**
							 *  end
							 */
							
							
							PaymentTerms pterm = new PaymentTerms();
							pterm.setPayTermDays(day);
							pterm.setPayTermPercent(percent);
							pterm.setPayTermComment(i + " Payment");
							/**
							 * Date : 08-08-2017 BY ANIL
							 */
							if(dbPaymentDate.getValue()!=null){
								pterm.setPaymentDate(paymentDt);
							}
							/**
							 * End
							 */
							ptermsList.add(pterm);

						} else {
							double diff = 0;
							if (totalPer < 100) {
								diff = 100 - totalPer;
								percent = percent + diff;
								//rohan commented this code on date date 04/10/2017
//								percent = Double.parseDouble(nf.format(percent));

								PaymentTerms pterm = new PaymentTerms();
								pterm.setPayTermDays(day);
								pterm.setPayTermPercent(percent);
								pterm.setPayTermComment(i + " Payment");
								/**
								 * Date : 08-08-2017 BY ANIL
								 */
								if(dbPaymentDate.getValue()!=null){
									pterm.setPaymentDate(paymentDt);
								}
								/**
								 * End
								 */
								ptermsList.add(pterm);
							} else {
								diff = totalPer - 100;
								percent = percent - diff;
								//rohan commented this code on date date 04/10/2017
//								percent = Double.parseDouble(nf.format(percent));

								PaymentTerms pterm = new PaymentTerms();
								pterm.setPayTermDays(day);
								pterm.setPayTermPercent(percent);
								pterm.setPayTermComment(i + " Payment");
								/**
								 * Date : 08-08-2017 BY ANIL
								 */
								if(dbPaymentDate.getValue()!=null){
									pterm.setPaymentDate(paymentDt);
								}
								/**
								 * End
								 */
								ptermsList.add(pterm);
							}
						}

					} else {
						PaymentTerms pterm = new PaymentTerms();
						pterm.setPayTermDays(day);
						pterm.setPayTermPercent(percent);
						pterm.setPayTermComment(i + " Payment");
						/**
						 * Date : 08-08-2017 BY ANIL
						 */
						if(dbPaymentDate.getValue()!=null){
							pterm.setPaymentDate(paymentDt);
						}
						/**
						 * End
						 */
						ptermsList.add(pterm);
					}
					day = day + days;
					
					if(dbPaymentDate.getValue()!=null){
						System.out.println(" BF DATE :: "+paymentDate);
						int month=paymentDate.getMonth();
						month=month+monthCounter;
						paymentDate.setMonth(month);
						System.out.println(" AF DATE :: "+paymentDate);
					}
				}
				
				//  rohan added 
				paymentTermsTable.getDataprovider().setList(ptermsList);
			}
		}}
	
	
	private int getDifferencebetweenDate(Date contractDate, Date invoiceDate) {
		int value = CalendarUtil.getDaysBetween(contractDate, invoiceDate);
		Console.log("value"+value);
		return value;
	}

	private int getDurationFromProductTable() {
		int maxDur = 0;
		int currDur = 0;
		for (int i = 0; i < this.saleslineitemtable.getDataprovider().getList().size(); i++) {
			currDur = this.saleslineitemtable.getDataprovider().getList().get(i).getDuration();
			System.out.println(" curruent dur" + currDur);
			if (maxDur <= currDur) {
				maxDur = currDur;
			}
		}
	
		return maxDur;
	}
	
	@Override
	public TextBox getstatustextbox() {
		
		return this.tbQuotationStatus;
	}

	
	/******************************React On Add Payment Terms Method*******************************/
	/**
	 * This method is called when add button of payment terms is clicked.
	 * This method involves the inclusion of payment terms in the corresponding table
	 */
	
	public void reactOnAddPaymentTerms()
	{
		System.out.println("In method of payemtn terms");	
		int payTrmDays=0;
		double payTrmPercent=0;
		String payTrmComment="";
		boolean flag=true;
		
		if(ibdays.getValue()==null||ibdays.getValue().equals(""))
		{
			this.showDialogMessage("Days cannot be empty!");
			return;
		}
				
		if(dopercent.getValue()==null||dopercent.getValue().equals(""))
		{
			this.showDialogMessage("Percent cannot be empty!");
			return;
		}
		
		if(tbcomment.getValue()==null||tbcomment.getValue().equals(""))
		{
			this.showDialogMessage("Comment cannot be empty!");
			return;
		}
		
		boolean checkTermDays=this.validatePaymentTrmDays(ibdays.getValue());
		if(checkTermDays==false)
		{
			if(chk==0){
				showDialogMessage("Days already defined.Please enter unique days!");
			}
//			clearPaymentTermsFields();
			flag=false;
		}
		
		
		if(ibdays.getValue()!=null&&ibdays.getValue()>=0){
			payTrmDays=ibdays.getValue();
		}
		else{
			showDialogMessage("Days cannot be empty!");
			flag=false;
		}
		
		
		if(dopercent.getValue()!=null&&dopercent.getValue()>=0)
		{
			if(dopercent.getValue()>100){
				showDialogMessage("Percent cannot be greater than 100");
				dopercent.setValue(null);
				flag=false;
			}
			else if(dopercent.getValue()==0){
				showDialogMessage("Percent should be greater than 0!");
				dopercent.setValue(null);
			}
			else{
			payTrmPercent=dopercent.getValue();
			}
		}
		else{
			showDialogMessage("Percent cannot be empty!");
		}
		
		if(tbcomment.getValue()!=null){
			payTrmComment=tbcomment.getValue();
		}
		
		if(!payTrmComment.equals("")&&payTrmPercent!=0&&flag==true)
		{
			PaymentTerms payTerms=new PaymentTerms();
			payTerms.setPayTermDays(payTrmDays);
			payTerms.setPayTermPercent(payTrmPercent);
			payTerms.setPayTermComment(payTrmComment);
			payTerms.setBranch("");
			paymentTermsTable.getDataprovider().getList().add(payTerms);
			clearPaymentTermsFields();
		}
	}
	
	public boolean checkProducts(String pcode)
	{
		List<SalesLineItem> salesitemlis=saleslineitemtable.getDataprovider().getList();
		for(int i=0;i<salesitemlis.size();i++)
		{
			if(salesitemlis.get(i).getProductCode().equals(pcode)||salesitemlis.get(i).getProductCode()==pcode){
				return true;
			}
		}
		return false;
	}
	
	
	
	public void ProdctType(String pcode,String productId)
	{
		Console.log("Inside prodcttype method productSrNo="+productSrNo);
		final GenricServiceAsync genasync=GWT.create(GenricService.class);
		
		Vector<Filter> filtervec=new Vector<Filter>();
		int prodId=Integer.parseInt(productId);
		
		final MyQuerry querry=new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("productCode");
		filter.setStringValue(pcode.trim());
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(prodId);
		filtervec.add(filter);
		
		filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new SuperProduct());
		showWaitSymbol();
		 Timer timer=new Timer() 
    	 {
				@Override
				public void run() {
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
					showDialogMessage("An Unexpected error occurred!");
				}
	
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					
					if(result.size()==0)
					{
						showDialogMessage("Please check whether product status is active or not!");
					}
					SalesLineItem lis=null;
					
					for(SuperModel model:result)
					{
//						SuperProduct superProdEntity = (SuperProduct)model;
						/** Date 04-11-2019 by Vijay ****/
						SuperProduct superProdEntity = new SuperProduct();
						superProdEntity = (SuperProduct)model;
						
						Branch branchEntity = olbbBranch.getSelectedItem();
						boolean gstApplicable = false;
						if(olbcNumberRange.getSelectedIndex()!=0){
							Config configEntity = olbcNumberRange.getSelectedItem();
							if(configEntity!=null)
							gstApplicable = configEntity.isGstApplicable();
						}
						else{
							gstApplicable = true;
						}
//						lis=AppUtility.ReactOnAddProductComposite(superProdEntity);
						lis=AppUtility.ReactOnAddProductComposite(superProdEntity,cust,branchEntity,gstApplicable);

						//Ashwini Patil Date:16-04-2024 for orion
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "RemoveServiceAddressFromBranchPopup")){
							lis.setQuantity(0.0);
						}else
							lis.setQuantity(1.0);
						
						lis.setProductSrNo(productSrNo+1);
						/**
						 * Date 26/2/2018
						 * By jayshree
						 * Des.to change the service date to contract
						 */
						lis.setStartDate(dbContractDate.getValue());
						//End By jayshree
						
						/**
						 * Date 02-05-2018 
						 * Developer : Vijay
						 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
						 * so i have updated below code using hashmap 
						 */
//						/**
//						 * Date : 03-04-2017 By ANIL
//						 */
////						lis.setBranchSchedulingInfo(AppUtility.getCustomersBranchsForSchedulingInfo(customerbranchlist,olbbBranch.getValue()));
//						String branchArray[]=AppUtility.getCustomersBranchsForSchedulingInfo(customerbranchlist,olbbBranch.getValue());
//							if(branchArray[0]!=null){
//								lis.setBranchSchedulingInfo(branchArray[0]);
//							}
//							if(branchArray[1]!=null){
//								lis.setBranchSchedulingInfo1(branchArray[1]);
//							}
//							if(branchArray[2]!=null){
//								lis.setBranchSchedulingInfo2(branchArray[2]);
//							}
//							if(branchArray[3]!=null){
//								lis.setBranchSchedulingInfo3(branchArray[3]);
//							}
//							if(branchArray[4]!=null){
//								lis.setBranchSchedulingInfo4(branchArray[4]);
//							}
//							
//							/**
//							 * Date : 12-06-2017 BY ANIL
//							 */
//							if(branchArray[5]!=null){
//								lis.setBranchSchedulingInfo5(branchArray[5]);
//							}
//							if(branchArray[6]!=null){
//								lis.setBranchSchedulingInfo6(branchArray[6]);
//							}
//							if(branchArray[7]!=null){
//								lis.setBranchSchedulingInfo7(branchArray[7]);
//							}
//							if(branchArray[8]!=null){
//								lis.setBranchSchedulingInfo8(branchArray[8]);
//							}
//							if(branchArray[9]!=null){
//								lis.setBranchSchedulingInfo9(branchArray[9]);
//							}
//							
//							/**
//							 * End
//							 */
						
						/**
						 * Date 30-04-2018 
						 * Developer : Vijay
						 * Des : Customer braches adding it into hashmap so we can load more customer bracnhes large data
						 */
						String custUOM = "";
						if(cust.getUnitOfMeasurement()!=null && cust.getUnitOfMeasurement().trim().length()>0){
							custUOM = cust.getUnitOfMeasurement();
						}else{
							custUOM = superProdEntity.getUnitOfMeasurement();
						}
						ArrayList<BranchWiseScheduling> branchlist = AppUtility.getCustomerBranchSchedulinglistInfo(customerbranchlist,olbbBranch.getValue(),custUOM,cust.getArea());
						Integer prodSrNo = lis.getProductSrNo();
						/**
						 * @author Anil
						 */
						//Ashwini Patil Date:16-04-2024
						Console.log("checking customer branches");
						if(branchlist==null||branchlist.size()==0) {
							showDialogMessage("No Customer Branches found for this customer. Please define customer branch.");
							return;
						}else {
							Console.log("Ashwini branchlist size in producttype="+branchlist.size());
						}
						
						if(complainTatFlag){
							for(BranchWiseScheduling obj:branchlist){
								obj.setComponentName(lis.getPrduct().getProductGroup());
								ServiceProduct serviceProduct=(ServiceProduct) lis.getPrduct();
								obj.setDurationForReplacement(serviceProduct.getWarrantyPeriod());
							}
						}
						/**
						 * @author Vijay Date :- 05-11-2020
						 * Des :- Asset id not generated when only branch as Service Address 
						 * so updated code
						 */
						if(complainTatFlag){
							for(BranchWiseScheduling obj:branchlist){
								List<ComponentDetails> list=new ArrayList<ComponentDetails>();
								if(obj.isCheck()
										&&(obj.getComponentList()==null||obj.getComponentList().size()==0)){
									if(obj.getAssetQty()>0){
										for(int i=0;i<obj.getAssetQty();i++){
											ComponentDetails comp=new ComponentDetails();
											comp.setComponentName(obj.getComponentName());
											Console.log("obj.getComponentName() "+obj.getComponentName());
											comp.setSrNo(i);
											list.add(comp);
										}
									}
									if(list.size()!=0){
										obj.setComponentList(list);
									}
								}
							}
						}
						/**
						 * ends here
						 */
						
						HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
						customerBranchlist.put(prodSrNo, branchlist);
						lis.setCustomerBranchSchedulingInfo(customerBranchlist);
						Console.log("Ashwini customerBranchlist size "+customerBranchlist.size() +" set to item"+lis.getProductName());
						
						/**
						 * ends here	
						 */
						/** date 11.12.2018 added by komal for nbhc service branches button and popup **/
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableServiceBranch")){
							ArrayList<BranchWiseScheduling> branchArrayList = new ArrayList<BranchWiseScheduling>();
							for(Branch branch : LoginPresenter.customerScreenBranchList){
								BranchWiseScheduling branchWiseScheduling = new BranchWiseScheduling();
								branchWiseScheduling.setBranchName(branch.getBusinessUnitName());
								branchWiseScheduling.setArea(0);
								branchWiseScheduling.setCheck(false);
								branchArrayList.add(branchWiseScheduling);
							}
							HashMap<Integer, ArrayList<BranchWiseScheduling>> serviceBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
							serviceBranchlist.put(prodSrNo, branchArrayList);
							lis.setServiceBranchesInfo(serviceBranchlist);
						}
						
						/**
						 * end komal
						 */
						
					    getSaleslineitemtable().getDataprovider().getList().add(lis);
					    productSrNo++;
					    System.out.println("Rohan I value "+productSrNo);
					    Console.log("productSrNo updated value="+productSrNo);
					}
					
//					setProductSrNoMethod();
					
					if(serviceScheduleTable.getDataprovider().getList().size() == 0){
						
						
						if(!LoginPresenter.serviceScheduleNewLogicFlag){
							 List<ServiceSchedule> popuplist=reactfordefaultTable(lis);
							 /***
							  *  *:*:*
							  * nidhi
							  * 18-09-2018
							  * for map bill product
							  */
							 if(LoginPresenter.billofMaterialActive){
								 
								 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(lis.getPrduct());
								 if(billPrDt!=null){
									 
									 
									 UnitConversionUtility unitConver = new UnitConversionUtility();

									 
									 if(unitConver.varifyUnitConversion(billPrDt, lis.getAreaUnit())){
										 
										 popuplist = unitConver.getServiceitemProList(popuplist,lis.getPrduct(),lis,billPrDt);
										 
										 
									 }else{
										 showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
									 }
									 
									 
									 
									 
								 }
							 }
							 /**
							  * end
							  */
							 
							 getServiceScheduleTable().getDataprovider().setList(popuplist);
							 getServiceScheduleTable().getTable().setVisibleRange(0, popuplist.size());
							 getServiceScheduleTable().getDataprovider().setList(popuplist);
						}
						/**
						 * @author Vijay Date :- 16-09-2021
						 * Des :- Updated service scheduling with new logic and single common method with process config
						 */
						else{
							 Console.log("new service scheduling logic schedule size ==0 ");
							 List<SalesLineItem> salesitem = new ArrayList<SalesLineItem>();
							 salesitem.add(lis);
							 List<ServiceSchedule> popuplist=getServiceSchedulelist("default",salesitem,dbContractDate.getValue(),customerbranchlist,olbbBranch.getValue(),0,0,0,0,null);

							
							 System.out.println("table list isze before "+popuplist.size());
							 Console.log("new service scheduling logic popuplist "+popuplist.size());

							 getServiceScheduleTable().getDataprovider().setList(popuplist);
							 getServiceScheduleTable().getTable().setVisibleRange(0, popuplist.size());
							 getServiceScheduleTable().getDataprovider().setList(popuplist);
						}
						 
						 
						}else{
							 Console.log("new service scheduling logic schedule size !=0 ");

							 List<ServiceSchedule> privlist=serviceScheduleTable.getDataprovider().getList();
							 List<ServiceSchedule> popuplist1=new ArrayList<ServiceSchedule>();
							 List<ServiceSchedule> popuplist = new ArrayList<ServiceSchedule>();
							 
							 if(!LoginPresenter.serviceScheduleNewLogicFlag){
//								 List<ServiceSchedule> popuplist=reactfordefaultTable(lis);
								 popuplist=reactfordefaultTable(lis);

								 /***
								  *  *:*:*
								  * nidhi
								  * 18-09-2018
								  * for map bill product
								  */
								 if(LoginPresenter.billofMaterialActive){
									 
									 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(lis.getPrduct());
									 if(billPrDt!=null){
										 
										 
										 UnitConversionUtility unitConver = new UnitConversionUtility();
	
										 
										 if(unitConver.varifyUnitConversion(billPrDt, lis.getAreaUnit())){
											 
											 popuplist = unitConver.getServiceitemProList(popuplist,lis.getPrduct(),lis,billPrDt);
											 
											 
										 }else{
											 showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
										 }
										 
										 
									 }
								 }
								 /**
								  * end
								  */
								 
							 }
							 else{
									
								 List<SalesLineItem> salesitem = new ArrayList<SalesLineItem>();
								 salesitem.add(lis);
								 popuplist=getServiceSchedulelist("default",salesitem,dbContractDate.getValue(),customerbranchlist,olbbBranch.getValue(),0,0,0,0,null);
								 Console.log("new service scheduling logic schedule size !=0 popuplist size"+popuplist.size());

							 }
							 
//							
							 popuplist1.addAll(privlist);
							 popuplist1.addAll(popuplist);
							 
							 System.out.println("table list isze before "+popuplist1.size());
							 
							 getServiceScheduleTable().getDataprovider().setList(popuplist1);
							 getServiceScheduleTable().getTable().setVisibleRange(0, popuplist1.size());
					
							 System.out.println("table list isze after "+getServiceScheduleTable().getDataprovider().getList().size());
//							 for (int i = 0; i < getServiceScheduleTable().getDataprovider().getList().size(); i++) {
//									
//									System.out.println("my warm up rohan12345"+getServiceScheduleTable().getDataprovider().getList().get(i).getSerSrNo());
//								}
							}
					}
				 });
				hideWaitSymbol();
 				}
	 	  };
          timer.schedule(3000); 
	}
	
//	private void setProductSrNoMethod() {
//		
//		if(getSaleslineitemtable().getDataprovider().getList().size()!=0){
//			List<SalesLineItem> tableList = getSaleslineitemtable().getDataprovider().getList();
//			for (int i=0;i<tableList.size();i++)
//			{
//				tableList.get(i).setProductSrNo(i+1);
//			}
//			
//			getSaleslineitemtable().getDataprovider().setList(tableList);
//		}
//	}
	
	/*****************************************Add Product Taxes Method***********************************/
	
	/**
	 * This method is called when a product is added to SalesLineItem table.
	 * Taxes related to corresponding products will be added in ProductTaxesTable.
	 * @throws Exception
	 */
	public void addProdTaxes() throws Exception
	{
		List<SalesLineItem> salesLineItemLis=this.saleslineitemtable.getDataprovider().getList();
		List<ProductOtherCharges> taxList=this.prodTaxTable.getDataprovider().getList();
		NumberFormat nf=NumberFormat.getFormat("#.00");
		boolean areaWiseflg  = LoginPresenter.areaWiseCalRestricFlg;
		for(int i=0;i<salesLineItemLis.size();i++)
		{
			double priceqty=0,taxPrice=0;
			
			//*******************old code for discount **************
			
//			if(salesLineItemLis.get(i).getPercentageDiscount()==null){
//				taxPrice=this.getSaleslineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
//				priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice)*salesLineItemLis.get(i).getQty();
//				priceqty=Double.parseDouble(nf.format(priceqty));
//			}
//			if(salesLineItemLis.get(i).getPercentageDiscount()!=null){
//				taxPrice=this.getSaleslineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
//				priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
//				priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
//				priceqty=priceqty*salesLineItemLis.get(i).getQty();
//				priceqty=Double.parseDouble(nf.format(priceqty));
//			}
			
			
			//****************new code for discount by rohan ***************
			
//	if((salesLineItemLis.get(i).getPercentageDiscount()==null && salesLineItemLis.get(i).getPercentageDiscount()==0) && (salesLineItemLis.get(i).getDiscountAmt()==0) ){
//				
//				System.out.println("inside both 0 condition");
//				
//				taxPrice=this.getSaleslineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
//				priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice)*salesLineItemLis.get(i).getQty();
//				priceqty=Double.parseDouble(nf.format(priceqty));
//			}
//			
//			else if(((salesLineItemLis.get(i).getPercentageDiscount()!=null)&& (salesLineItemLis.get(i).getDiscountAmt()!=0))){
//				
//				System.out.println("inside both not null condition");
//				
//				taxPrice=this.getSaleslineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
//				priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
//				priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
//				
//				priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
//				
//				priceqty=priceqty*salesLineItemLis.get(i).getQty();
//				priceqty=Double.parseDouble(nf.format(priceqty));
//			}
//			else 
//			{
//				System.out.println("inside oneof the null condition");
//				
//				
//				taxPrice=this.getSaleslineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
//				priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
//				
//				if(salesLineItemLis.get(i).getPercentageDiscount()!=null){
//				priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
//				}
//				else 
//				{
//				priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
//				}
//				priceqty=priceqty*salesLineItemLis.get(i).getQty();
//				priceqty=Double.parseDouble(nf.format(priceqty));
//			}
//			
//			//****************changes ends here *********************************
//			
//			if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
//				ProductOtherCharges pocentity=new ProductOtherCharges();
//				pocentity.setChargeName("VAT");
//				pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
//				pocentity.setIndexCheck(i+1);
//				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
//				if(indexValue!=-1){
//					pocentity.setAssessableAmount(0);
//					this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
//				}
//				
//				if(indexValue==-1){
//					pocentity.setAssessableAmount(0);
//				}
//				this.prodTaxTable.getDataprovider().getList().add(pocentity);
//			}
//			
//			if(salesLineItemLis.get(i).getServiceTax().getPercentage()==0){
//				ProductOtherCharges pocentity=new ProductOtherCharges();
//				pocentity.setChargeName("Service Tax");
//				pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
//				pocentity.setIndexCheck(i+1);
//				int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");
//				if(indexValue!=-1){
//					pocentity.setAssessableAmount(0);
//					this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
//				}
//				
//				if(indexValue==-1){
//					pocentity.setAssessableAmount(0);
//				}
//				this.prodTaxTable.getDataprovider().getList().add(pocentity);
//			}

			//   new code by vijay for sq area + disc on total 
			System.out.println(" AREA Value ===== "+salesLineItemLis.get(i).getArea());
			
			/**
			 * rohan removed quantity form price calculation Required for NBHC 
			 * Date : 14- 10 - 2016
			 * i have removed field (salesLineItemLis.get(i).getQty()) from every calculation
			 */
			
			if((salesLineItemLis.get(i).getPercentageDiscount()==null && salesLineItemLis.get(i).getPercentageDiscount()==0) && (salesLineItemLis.get(i).getDiscountAmt()==0) ){
						
						System.out.println("inside both 0 condition");
						
						taxPrice=this.getSaleslineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
						/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
						/**
						 * nidhi
						 * 21-09-2018
						 * for stop area wise priccing
						 */
						if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA") && !areaWiseflg){
							double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
							priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice)*squareArea;
							priceqty=Double.parseDouble(nf.format(priceqty));
							System.out.println("RRRRRRRRRRRRRR price == "+priceqty);
							}else{
								System.out.println("old code");
								priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
								priceqty=Double.parseDouble(nf.format(priceqty));
							}
						/**********************************************/
					}
					
					else if(((salesLineItemLis.get(i).getPercentageDiscount()!=null)&& (salesLineItemLis.get(i).getDiscountAmt()!=0))){
						
						System.out.println("inside both not null condition");
						
						taxPrice=this.getSaleslineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
						priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
						/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
						/**
						 * nidhi
						 * 21-09-2018
						 */
						if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")  && !areaWiseflg){
							double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
							priceqty=priceqty*squareArea;
							priceqty= priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							priceqty=Double.parseDouble(nf.format(priceqty));
						}else{
//							priceqty=priceqty*salesLineItemLis.get(i).getQty();
							priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							priceqty=Double.parseDouble(nf.format(priceqty));
							
						}
						
						
						/***************************************************************/

					}
					else 
					{
						System.out.println("inside oneof the null condition");
						
						taxPrice=this.getSaleslineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
						priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
						
//						if(salesLineItemLis.get(i).getPercentageDiscount()!=null){
//							/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
//							if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
//								Double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
//								priceqty = priceqty*squareArea;
//								priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
//							}else{
////								priceqty=priceqty*salesLineItemLis.get(i).getQty();
//								priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
//							}
//							
//						}
//						else 
//						{
//							/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
//							if(!salesLineItemLis.get(i).getArea().equals("NA")){
//								Double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
//								priceqty = priceqty*squareArea;
//								priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
//							}else{
////								priceqty=priceqty*salesLineItemLis.get(i).getQty();
//								priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
//							}
//						}
						
						/**
						 * nidhi
						 * 21-09-2018
						 */
						if(salesLineItemLis.get(i).getPercentageDiscount()!=null   && !areaWiseflg){
							/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
							/**
							 * nidhi
							 * 21-09-2018
							 */
							if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")   && !areaWiseflg){
								Double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
								priceqty = priceqty*squareArea;
								priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							}else{
//								priceqty=priceqty*salesLineItemLis.get(i).getQty();
								priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							}
							
						}
						else 
						{
							/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
							/**\
							 * nidhi
							 * 21-09-2018
							 */
							if(!salesLineItemLis.get(i).getArea().equals("NA")   && !areaWiseflg){
								Double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
								priceqty = priceqty*squareArea;
								priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							}else{
//								priceqty=priceqty*salesLineItemLis.get(i).getQty();
								priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							}
						}
						
						priceqty=Double.parseDouble(nf.format(priceqty));
						System.out.println("Price ====== "+priceqty);
					}
			//****************changes ends here *********************************
		
			
			/**
		     * Date 1-09-2017 added by vijay for if discount amount is there then taxes calculation based on after
		     * discount Total amount
		     */
			if(dbDiscountAmt.getValue()!=null && dbDiscountAmt.getValue()!=0){
//				System.out.println("hi vijay inside discount amount");
//				priceqty = dbFinalTotalAmt.getValue();
				
				/**
				 * Date : 23-09-2017 BY ANIL
				 * 
				 */
				if(isValidForDiscount()){
					priceqty = dbFinalTotalAmt.getValue()/saleslineitemtable.getDataprovider().getList().size();
					System.out.println("hi vijay inside discount amount "+priceqty);
				}else{
					dbDiscountAmt.setValue(0.0);
					dbFinalTotalAmt.setValue(dototalamt.getValue());
				}
			}
			
			/**
			 * ends here
			 */
			
			if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
				//  rohan added this conditions
				
				System.out.println("hi vijay vat=="+salesLineItemLis.get(i).getVatTax().getTaxPrintName());
				if(salesLineItemLis.get(i).getVatTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getVatTax().getTaxPrintName().equals(""))
				{
					System.out.println("Inside vat GST");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					//   old code commented by rohan on date : 22-6-2017
	//				pocentity.setChargeName("VAT");
					//   new code by rohan Date 22-6-2017 
					System.out.println("salesLineItemLis.get(i).getVatTax().getTaxName()"+salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					pocentity.setChargeName(salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//						pocentity.setAssessableAmount(priceqty);
						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
				}
				else{
					System.out.println("Vattx");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					//   old code commented by rohan on date : 22-6-2017
	//				pocentity.setChargeName("VAT");
					//   new code by rohan Date 22-6-2017 
					System.out.println("salesLineItemLis.get(i).getVatTax().getTaxName()"+salesLineItemLis.get(i).getVatTax().getTaxName());
					pocentity.setChargeName("VAT");
					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT");
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
				}
			}
			
			if(salesLineItemLis.get(i).getServiceTax().getPercentage()!=0){
				
				System.out.println(" HI Vijay Service =="+salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
				if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals(""))
				{
					System.out.println("Inside service GST");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					//   old code commented by rohan on date : 22-6-2017
	//				pocentity.setChargeName("VAT");
					//   new code by rohan Date 22-6-2017 
					System.out.println("salesLineItemLis.get(i).getServiceTax().getTaxName()"+salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					pocentity.setChargeName(salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//						pocentity.setAssessableAmount(priceqty);
						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
				}
				else{
				
					System.out.println("ServiceTx");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					//   old code commented by rohan on date : 22-6-2017
	//				pocentity.setChargeName("Service Tax");
					//   new code by rohan Date 22-6-2017 
					System.out.println("salesLineItemLis.get(i).getServiceTax().getTaxName()"+salesLineItemLis.get(i).getServiceTax().getTaxName());
					pocentity.setChargeName("Service");
					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service");
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						double assessValue=0;
						if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
							assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
							pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
						if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						}
					}
					if(indexValue==-1){
						System.out.println("Index As -1");
						double assessVal=0;
						if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
							assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
							pocentity.setAssessableAmount(assessVal);
						}
						if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
							pocentity.setAssessableAmount(priceqty);
						}
						
					}
					this.prodTaxTable.getDataprovider().getList().add(pocentity);
					
					System.out.println("DONE ....");
				}
			}
		}
	}

	/*********************************Check Tax Percent*****************************************/
	/**
	 * This method returns 
	 * @param taxValue
	 * @param taxName
	 * @return
	 */
	
	public int checkTaxPercent(double taxValue,String taxName)
	{
		List<ProductOtherCharges> taxesList=this.prodTaxTable.getDataprovider().getList();
		System.out.println("rohan taxesList size"+taxesList.size());
		for(int i=0;i<taxesList.size();i++)
		{
			double listval=taxesList.get(i).getChargePercent();
			int taxval=(int)listval;
			
			if(taxName.equals("Service")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
					return i;
				}
			}
			if(taxName.equals("VAT")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					return i;
				}
			}
			
			//  rohan added this for GST
			if(taxName.equals("CGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
					return i;
				}
			}
			
		//  rohan added this for GST
			if(taxName.equals("SGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
					return i;
				}
			}
			
		//  rohan added this for GST
			if(taxName.equals("IGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
					return i;
				}
			}
			
		}
		return -1;
	}
	
	
	
	/*************************Update Charges Table****************************************/
	/**
	 * This method updates the charges table.
	 * The table is updated when any row value is changed through field updater in table i.e
	 * row change event is fired
	 */
	
	public void updateChargesTable()
	{
		List<ProductOtherCharges> prodOtrLis=this.chargesTable.getDataprovider().getList();
		ArrayList<ProductOtherCharges> prodOtrArr=new ArrayList<ProductOtherCharges>();
		prodOtrArr.addAll(prodOtrLis);
		
		this.chargesTable.connectToLocal();
		System.out.println("prodo"+prodOtrArr.size());
		for(int i=0;i<prodOtrArr.size();i++)
		{
			ProductOtherCharges prodChargesEnt=new ProductOtherCharges();
			prodChargesEnt.setChargeName(prodOtrArr.get(i).getChargeName());
			prodChargesEnt.setChargePercent(prodOtrArr.get(i).getChargePercent());
			prodChargesEnt.setChargeAbsValue(prodOtrArr.get(i).getChargeAbsValue());
			//prodChargesEnt.setAssessableAmount(prodOtrArr.get(i).getAssessableAmount());
			prodChargesEnt.setFlagVal(prodOtrArr.get(i).getFlagVal());
			prodChargesEnt.setIndexCheck(prodOtrArr.get(i).getIndexCheck());
			if(prodOtrArr.get(i).getFlagVal()==false){
				prodChargesEnt.setAssessableAmount(this.getDoamtincltax().getValue());
			}
			if(prodOtrArr.get(i).getFlagVal()==true){
				double assesableVal=this.retrieveSurchargeAssessable(prodOtrArr.get(i).getIndexCheck());
				prodChargesEnt.setAssessableAmount(assesableVal);
			}
			
			this.chargesTable.getDataprovider().getList().add(prodChargesEnt);
		}
	}
	
	public double retrieveSurchargeAssessable(int indexValue)
	{
		List<ProductOtherCharges> otrChrgLis=this.chargesTable.getDataprovider().getList();
		ArrayList<ProductOtherCharges> otrChrgArr=new ArrayList<ProductOtherCharges>();
		otrChrgArr.addAll(otrChrgLis);
	
		double getAssessVal=0;
		for(int i=0;i<otrChrgArr.size();i++)
		{
			if(otrChrgArr.get(i).getFlagVal()==false&&otrChrgArr.get(i).getIndexCheck()==indexValue)
			{
				if(otrChrgArr.get(i).getChargePercent()!=0){
					getAssessVal=otrChrgArr.get(i).getAssessableAmount()*otrChrgArr.get(i).getChargePercent()/100;
				}
				if(otrChrgArr.get(i).getChargeAbsValue()!=0){
					getAssessVal=otrChrgArr.get(i).getChargeAbsValue();
				}
			}
		}
		return getAssessVal;
	}
	
	
	public boolean validateContractProdQty()
	{
		List<SalesLineItem> contractprodlis=saleslineitemtable.getDataprovider().getList();
		int ctr=0;
		for(int i=0;i<contractprodlis.size();i++)
		{
			if(contractprodlis.get(i).getQty()==0)
			{
				ctr=ctr+1;
			}
		}
		
		if(ctr==0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public boolean validateServicePopupBranch(){
		
		if(customerbranchlist.size()!=0){
			List<ServiceSchedule> prodservicelist=this.serviceScheduleTable.getDataprovider().getList();
			for(int i=0;i<prodservicelist.size();i++){
				//  rohan added this code for validating while saving data   
				if(prodservicelist.get(i).getScheduleProBranch()==null || prodservicelist.get(i).getScheduleProBranch().equalsIgnoreCase("SELECT") ){
					return false;
				}
		
			}
			return true;
		}else{
			return true;
		}
	}

	/************************************Overridden Methods*******************************************/


	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		getstatustextbox().setEnabled(false);
		this.tbContractId.setEnabled(false);
		this.tbLeadId.setEnabled(false);
		this.tbQuotatinId.setEnabled(false);
		this.saleslineitemtable.setEnable(state);
		this.paymentTermsTable.setEnable(state);
		this.chargesTable.setEnable(state);
		this.prodTaxTable.setEnable(state);
		this.doamtincltax.setEnabled(false);
		this.donetpayamt.setEnabled(false);
		this.dototalamt.setEnabled(false);
		this.ibOldContract.setEnabled(false);
		this.tbremark.setEnabled(false);
		this.dbContractStartDate.setEnabled(false);
		this.dbContractEndDate.setEnabled(false);
		this.f_btnservice.setEnabled(true);
		this.tbdonotrenew.setEnabled(false);
		this.tbTicketNumber.setEnabled(false);
		
		this.btstackDetails.setEnabled(true);
		
		/*
		 * nidhi
		 * 29-06-2017
		 * 
		 */
		
		boolean flag = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC");
		System.out.println("Check status for nbhc -  "+flag);
		if(flag){
			this.tbCustSegment.setEnabled(false);
		}else{
			this.tbCustSegment.setEnabled(state);
		}
		
		//Date 01-09-2017 added by vijay for total amount discount and roundoff
		this.dbDiscountAmt.setEnabled(state);
		this.dbFinalTotalAmt.setEnabled(false);
		this.tbroundOffAmt.setEnabled(state);
		this.dbGrandTotalAmt.setEnabled(false);
		
		/**
		 * Date : 12-10-2017 BY ANIL
		 * if number range is selected and contract id got generated then number range should not be editable
		 */
//		if(tbContractId.getValue()!=null&&!tbContractId.getValue().equals("")){
//			this.olbcNumberRange.setEnabled(false);
//		}
		if(contractObject!=null&&contractObject.getCount()!=0){
			this.olbcNumberRange.setEnabled(false);
		}
		
		paymentTermsTable.setEnable(state);
		/**
		 * End
		 */
		this.cbServiceWiseBilling.setEnabled(state);
		/**
		 *  nidhi
		 *  7-03-2018
		 *  		 *  
		 */
		/**
		 *  nidhi
		 *13-03-2018  
		 */
		this.objContactList.setEnabled(state);
		
		/**
		 * nidhi
		 * 31-07-2018
		 * for set  quotaion created employee 
		 */
		this.tbCreatedBy.setEnabled(false);
		
		/**
		 * Date 28-09-2018 By Vijay 
		 * Des:- NBHC CCPM for every contract this must be true by default it does not allow to change for user
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC")){
			/**
			 * Date 26-12-2020 By Priyanka 
			 * Des - "Start Of Period" checkbox should be seleceted by default raised by Vaishnavi mam
			 */
			cbStartOfPeriod.setValue(true);
		}
		/**
		 * ends here
		 */
		if(paymentDateFlag){
			dbPaymentDate.setEnabled(false);
			ibdays.setEnabled(false);
			dopercent.setEnabled(false);
			doAmount.setEnabled(false);
			tbcomment.setEnabled(false);
		}
		
		
		if(contractDateDisable){
			this.dbContractDate.setEnabled(false);
		}
		if(salesPersonRestrictionFlag){
			this.olbeSalesPerson.setEnabled(false);
		}
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","MakeColumnDisable")
				&&saleslineitemtable.quotationCount>0){
			this.saleslineitemtable.setEnable(false);
		}
		
		 if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "MakeCategoryAndTypeDisable")){
	   		 this.olbContractCategory.setEnabled(false);
	   		 this.olbContractType.setEnabled(false);
	   	  }
		    objPaymentMode.setEnabled(state);
			tbPaymentName.setEnabled(false);
			tbPaymentBankAC.setEnabled(false);
			tbPaymentPayableAt.setEnabled(false);
			tbPaymentAccountName.setEnabled(false);
			
			/**
			 * @author Anil
			 * @since 03-06-2020
			 */
			dbTat.setEnabled(state);
			
			chkStationedService.setEnabled(state);
			
		invoicepdfNametoprint.setEnabled(state);
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange", "MakeNumberRangeReadOnly"))
  		{	olbcNumberRange.setEnabled(false);
  		}
  		
	}

	@Override
	public void setToViewState() {
	
		super.setToViewState();
		setMenuAsPerStatus();
		
		SuperModel model=new Contract();
		model=contractObject;
		AppUtility.addDocumentToHistoryTable(AppConstants.SERVICEMODULE,AppConstants.CONTRACT, contractObject.getCount(), Integer.parseInt(personInfoComposite.getId().getValue()),personInfoComposite.getName().getValue(),Long.parseLong(personInfoComposite.getPhone().getValue()), false, model, null);
		if(paymentDateFlag){
			dbPaymentDate.setEnabled(false);
			ibdays.setEnabled(false);
			dopercent.setEnabled(false);
			doAmount.setEnabled(false);
			tbcomment.setEnabled(false);
		}
		
		//   rohan commented this code used for cat config load 
//		Timer t =new Timer() {
//			
//			@Override
//			public void run() {
//				AppUtility.getConfigDatainViewAndEditState(olbContractCategory,"ConfigCategory",Screen.CONTRACTCATEGORY, AppMemory.getAppMemory().currentState);				
//			}
//		};
//		t.schedule(3000);
		
		/**
		 * Added by Priyanka - 27-11-2020
		 * 
		 */
		String mainScreenLabel="Contract Details";
		if(contractObject!=null&&contractObject.getCount()!=0){
			mainScreenLabel=contractObject.getCount()+" "+"/"+" "+contractObject.getStatus()+" "+ "/"+" "+AppUtility.parseDate(contractObject.getCreationDate());
		}
		

		fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
		
		/**
		 *  end
		 * 
		 */
		
		
		
	}
	
	@Override
	public void setToNewState(){
		super.setToNewState();
		this.contractRenewFlag = false;
		if(paymentDateFlag){
			dbPaymentDate.setEnabled(false);
			ibdays.setEnabled(false);
			dopercent.setEnabled(false);
			doAmount.setEnabled(false);
			tbcomment.setEnabled(false);
			
		}
		Date date=new Date();
		dbContractDate.setValue(date);	
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "MakeContractDateDisable")
				&&!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
//					contractDateDisable=true;
					this.dbContractDate.setEnabled(false);
					}
		
		
		
		
		if(salesPersonRestrictionFlag&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Sales"))
		{
			Console.log("Inside Sales");
			olbeSalesPerson.setEnabled(false);
		}
		findMaxServiceSrNumber();
		  /*
  		 * Ashwini Patil
  		 * Date:14-06-2024
  		 * Ultima search want to map number range directly from branch so they want this field read only
  		 */
		Console.log("about to Make number range non editable from newState");
  		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange", "MakeNumberRangeReadOnly"))
  		{	olbcNumberRange.setEnabled(false);
  		Console.log("Making number range non editable from newState");
  		}
  		
  		
	}
	
	@Override
	public void setToEditState() {
			
		super.setToEditState();
		
		//Ashwini Patil Date:27-08-2022 
		if(contractObject!=null) {
		if(contractObject.getStatus().equals(AppConstants.APPROVED)){
			showDialogMessage("You are editing approved contract. Changes done related to number of services or billing amount or payment will not reflect in services or billing document.\r\n"
					+ "\r\n"
					+ "If you would like to get services or amount reflection either cancel the contract & create one OR use Contract Reset button in three line menu.");
		
			if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EditApprovedContract")){
				
				baddproducts.setEnabled(false);
				saleslineitemtable.setEnable(false);
				prodInfoComposite.setEnabled(false);
				addPayTerms.setEnabled(false);
				addPaymentTerms.setEnabled(false);
				paymentTermsTable.setEnable(false);
			}
			
			
			personInfoComposite.setEnable(false);
			chkservicewithBilling.setEnabled(false);
			cbServiceWiseBilling.setEnabled(false);
			
		
		}
		}
		
		
		Console.log("In side edit state ");
		
		this.processLevelBar.setVisibleFalse(false);
		
		if(!tbQuotatinId.getValue().equals(""))
		{
//			baddproducts.setEnabled(false);
			addOtherCharges.setEnabled(false);
		}
		
		if(salesPersonRestrictionFlag)
		{
			olbeSalesPerson.setEnabled(false);
		}
		
		
		changeStatusToCreated();
		
		
		//   rohan added method to find max product sr number
		
		findMaxServiceSrNumber();
		
		
		//  rohan added this code for contract making contract status rejected to Created  
		
		
		//Date 2 7 2017 added by vijay
		this.olbcNumberRange.setEnabled(false);
		
		
		/**
		 * Date : 12-04-2018 By ANIL
		 * Getting app header level button issue ,after editing contract in approved state
		 */
		toggleAppHeaderBarMenu();
		
		if(paymentDateFlag){
			dbPaymentDate.setEnabled(false);
			ibdays.setEnabled(false);
			dopercent.setEnabled(false);
			doAmount.setEnabled(false);
			tbcomment.setEnabled(false);
		}
		
		
		String mainScreenLabel="Contract Details";
		if(contractObject!=null&&contractObject.getCount()!=0){
			mainScreenLabel=contractObject.getCount()+" "+"/"+" "+contractObject.getStatus()+" "+ "/"+" "+AppUtility.parseDate(contractObject.getCreationDate());
		}
		
		
		fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
		
		/**
		 *  Date : 08/07/2021  By Priyanka
		 *  Des : When we create rate contract then payment term will add automatically and add button lock but after editing 
		 *        the same rate contract add button will unlock -- Issue Raised by Rahul Tiwari. 
		 */
		
		
			if(chkservicewithBilling.getValue().equals(true)){
	
				addPaymentTerms.setEnabled(false);
				addPayTerms.setEnabled(false);
				
				
		}
			  /*
	  		 * Ashwini Patil
	  		 * Date:14-06-2024
	  		 * Ultima search want to map number range directly from branch so they want this field read only
	  		 */
	  		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange", "MakeNumberRangeReadOnly"))
	  			olbcNumberRange.setEnabled(false);
	  		
			
	}
	
	
	
	public void findMaxServiceSrNumber() {
		int maxSrNumber = 0;
		System.out.println("rohan in side max service number");
		List<SalesLineItem> list = this.saleslineitemtable.getDataprovider().getList();
		if (list != null && list.size() > 0) {
//			System.out.println("mazza list size " + list.size());
//			Comparator<SalesLineItem> compare = new Comparator<SalesLineItem>() {
//				@Override
//				public int compare(SalesLineItem o1, SalesLineItem o2) {
//					return Integer.compare(o2.getProductSrNo(),o1.getProductSrNo());
//				}
//			};
//			Collections.sort(list, compare);
//
//			System.out.println("product sr number "+ list.get(0).getProductSrNo());
//			maxSrNumber = list.get(0).getProductSrNo();
//			productSrNo = maxSrNumber;
			
			/**
			 * @author Anil @since 28-05-2021
			 * Earlier removed the sorting becuase list was already sorted from updateView method
			 * IPM facess extra services genration issue for renewal contracts if another product added Raised by Vaishnavi
			 */
			Comparator<SalesLineItem> comp =new Comparator<SalesLineItem>() {
				@Override
				public int compare(SalesLineItem item1,SalesLineItem item2) {
					
					if (item1.getProductSrNo()== item2.getProductSrNo()) {
						return 0;
					}else if (item1.getProductSrNo()> item2.getProductSrNo()) {
						return 1;
					} else {
						return -1;
					}
				}
			};
			Collections.sort(list,comp);
			
			maxSrNumber = list.get(list.size()-1).getProductSrNo();
			productSrNo = maxSrNumber;
			Console.log("In findmaxservicesrnumber productSrNo "+ productSrNo);
		}
	}

	public void changeStatusToCreated()
	{
		Contract entity=(Contract) presenter.getModel();
		String status=entity.getStatus();
		
		if(SalesOrder.REJECTED.equals(status.trim()))
		{
			this.tbQuotationStatus.setValue(SalesOrder.CREATED);
		}
	}
	
	@Override
	public void clear() {
		super.clear();
		getstatustextbox().setText(Contract.CREATED);
		getPaymenttable().clear();
		
		/**
		 * @author Anil
		 * @since 03-06-2020
		 */
		dbTat.clear();
	}
	
	
	/**
	 * Validate For Duplicate Products
	 */
	
	public boolean validateProductDuplicate(String pcode)
	{
		List<SalesLineItem> salesItemLis=this.getSaleslineitemtable().getDataprovider().getList();
		for(int i=0;i<salesItemLis.size();i++)
		{
			System.out.println("salesl"+salesItemLis.get(i).getProductCode());
			if(salesItemLis.get(i).getProductCode().trim().equals(pcode)){
				return true;
			}
		}
		System.out.println("Out");
		return false;
	}
	 
	public void validateCustomerEmailId(int count){
		 Console.log("inside validateCustomerEmailId "+count);
		
		
		final MyQuerry querry=new MyQuerry();
		Vector<Filter> temp=new Vector<Filter>();
		Filter filter=null;
		Company c = new Company();
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		temp.add(filter);
		
		
		
		filter=new Filter();
		filter.setQuerryString("count");
		filter.setIntValue(count);
		temp.add(filter);
		
		querry.setFilters(temp);
		querry.setQuerryObject(new Customer());
//		showWaitSymbol();
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
            Console.log("result size "+result.size());
				for(SuperModel model:result){
					
					Customer customer = (Customer)model;
					
						customer.setEmail(mailpopup.getTbEmailBox().getValue().trim());
						Console.log("cust email "+customer.getEmail());
						customerEmail=customer.getEmail();
						genasync.save(customer, new AsyncCallback<ReturnFromServer>() {
							
							@Override
							public void onSuccess(ReturnFromServer result) {
								Console.log("Customer email updated successFully "+customerEmail);
								showDialogMessage("Customer email updated successFully");
							}
							
							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								showDialogMessage("Data Saved UnsuccessFully");

							}
						});	
						mailpopup.hidePopUp();
					
					
				}
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				showDialogMessage("Unexpected Error Ouccured");
				hideWaitSymbol();

			}
		});
		
		
		
	
		
	}
	

	/****************************************Validation Method(Overridden)**********************************/
	/**
	 * Validates form fields
	 */
	
	@Override
	 public boolean validate()
	 {
		  boolean superValidate = super.validate();
		  
		  if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EmailMandatoryWhileCreatingContract")){
		  Console.log("customerEmail 111--"+customerEmail);
			if(customerEmail.equals("")){
				Console.log("customerEmail inside if "+customerEmail);
				mailpopup.showPopUp();
				return false;
			}
		  }
		
		
		  
		  
		  
		if(saleslineitemtable.getDataprovider().getList().size() == 0)
        {
            showDialogMessage("Please Select at least one Product!");
            return false;
        }else{
        	//Ashwini Patil Date:7-08-2023 Orion want this frequency column mandatory
        	if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","MakeFrequencyMandatoryAtProductLevel")){
        		List<SalesLineItem> prodList= saleslineitemtable.getDataprovider().getList();
        		for(SalesLineItem item:prodList){
        			if(item.getProfrequency()==null||item.getProfrequency().equals("")){
        				showDialogMessage("Please add frequency in product!");
        	            return false;
        			}
        			     				
        		}
          	}
        }
        
		//Ashwini Patil Date:11-04-2024
		List<SalesLineItem> prodList= saleslineitemtable.getDataprovider().getList();
		ArrayList<Integer> productSrNoList=new ArrayList<Integer>();
		for(SalesLineItem item:prodList){
			if(item.getCustomerBranchSchedulingInfo()==null) {
				showDialogMessage("Please select atleast one branch for product "+item.getProductName());
				return false;
			}   
			if(!productSrNoList.contains(item.getProductSrNo()))
				productSrNoList.add(item.getProductSrNo());
			else {
				showDialogMessage("Found duplicate product serial numbers. Please remove all products and add them again. Make sure that you add all products first and then you make changes in its price, branches etc.");
				return false;
			}
		}
        boolean valPayPercent=this.validatePaymentTermPercent();
        if(valPayPercent==false)
        {
        	showDialogMessage("Payment Term Not 100%, Please Correct!");
        	return false;
        }
        
  //***********rohan remove this as per ultra pest control requirement *************       
        
//        if(validateContractPrice()>0){
//        	showDialogMessage("Please enter product price greater than 0!");
//        	return false;
//        }
        
        if(!validateContractProdQty()){
			showDialogMessage("Please enter appropriate quantity required!");
			return false;
		}
		
        if(validateServicePopupBranch()==false){
        	this.showDialogMessage("Please Select Branch in Schedule Popup");
        	return false;
        }
        
      
        if(superValidate==false){
        	 return false;
        }
        
        //***************Date 29/08/2017 by jayshree******************
        
        if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "PremiseDetailMandatory")
        		&&validatePremises()==false){
        	this.showDialogMessage("Please enter premise details at product level.");
        	return false;
        }
      //***********End****************
        
        /**
		 * Date 03-07-2017 added by vijay tax validation
		 */
		List<ProductOtherCharges> prodtaxlist = this.prodTaxTable.getDataprovider().getList();
		for(int i=0;i<prodtaxlist.size();i++){
			
			if(dbContractDate.getValue()!=null && dbContractDate.getValue().before(date)){
				if(prodtaxlist.get(i).getChargeName().equalsIgnoreCase("CGST") || prodtaxlist.get(i).getChargeName().equalsIgnoreCase("SGST")
						|| prodtaxlist.get(i).getChargeName().equalsIgnoreCase("IGST")){
					showDialogMessage("As Contract date "+fmt.format(dbContractDate.getValue())+" GST Tax is not applicable");
					return false;
				}
			}
			/**
			 * @author Anil @since 21-07-2021
			 * Issue raised by Rahul for Innovative Pest(Thailand) 
			 * Vat is applicable there so system should not through validation for VAT
			 */
//			else if(dbContractDate.getValue()!=null && dbContractDate.getValue().equals(date) || dbContractDate.getValue().after(date)){
//				if(prodtaxlist.get(i).getChargeName().equalsIgnoreCase("CST") || prodtaxlist.get(i).getChargeName().equalsIgnoreCase("Service Tax")
//						|| prodtaxlist.get(i).getChargeName().equalsIgnoreCase("VAT")){
//					showDialogMessage("As Contract date "+fmt.format(dbContractDate.getValue())+" VAT/CST/Service Tax is not applicable");
//					return false;
//				}
//			}
		}
		/**
		 * ends here
		 */
       
		
		/**
		 * Date : 06-09-2017 By ANIL
		 */
		Console.log("No of services at schedule popuop "+serviceScheduleTable.getDataprovider().getList().size());
		if(serviceScheduleTable.getDataprovider().getList().size()==0){
			showDialogMessage("There are no services at schedule popup.");
			return false;
		}
		int noOfServices=0;
		for(SalesLineItem item:saleslineitemtable.getDataprovider().getList()){
			noOfServices=(int) (noOfServices+(item.getQty()*item.getNumberOfServices()));
		}
		Console.log("No of services at product level "+saleslineitemtable.getDataprovider().getList().size());
		
		/**
		 * @author Anil
		 * @since 06-06-2020
		 */
		if(serviceScheduleTable.getDataprovider().getList().size()!=noOfServices&&complainTatFlag==false){
			showDialogMessage("No. of services at schedule popup does not match with the no. of services at product level.");
			return false;
		}
		/**
		 * End
		 */
		
		
		/**
		 * Date : 30-10-2017 BY ANIL
		 */
		int checkboxValCtr=0;
		if(chkbillingatBranch.getValue()==true){
			checkboxValCtr++;
		}
		if(chkservicewithBilling.getValue()==true){
			checkboxValCtr++;
		}
		if(cbServiceWiseBilling.getValue()==true){
			checkboxValCtr++;
		}
		if(checkboxValCtr>1){
			showDialogMessage("Only one should be tick from ");
			return false;
		}
		
		/**
		 * Date : 02-11-2017 BY ANIL
		 * adding validation for product level area column
		 */
		for(SalesLineItem item:saleslineitemtable.getDataprovider().getList()){
			if(item.getArea()!=null){
				if(item.getArea().equals("")){
					showDialogMessage("Please put NA in area column at product table.");
					return false;
				}
				if(!item.getArea().equalsIgnoreCase("NA")){
					try {
						Double area = Double.parseDouble(item.getArea().trim());
					} catch (NumberFormatException e) {
						showDialogMessage("Please put only number in area column at product table.");
						return false;
					}
				}
				//Ashwini Patil Date:11-04-2024
				if(item.getCustomerBranchSchedulingInfo()==null) {
					showDialogMessage("Please select atleast one branch for product "+item.getProductName());
					return false;
				}
			}
		}
		/**
		 *  nidhi
		 *  7-03-2018
		 *  for validate contract date as per nbhc
		 */
	Date lastDate = new Date();
		
		CalendarUtil.addDaysToDate(lastDate, -45);
		Console.log(lastDate+"");
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ContractStartDateValidation")){
			
//			Date lastDate = new Date();
//			
//			CalendarUtil.addDaysToDate(lastDate, -30);
//			if(lastDate.after(dbContractStartDate.getValue())){
//				showDialogMessage("Contract start date Should not be less than a month.");
//				return false;
//			}
			Console.log("get entered in condition--");
//			showDialogMessage(lastDate+"");
			if(!LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN")){
				
				for(SalesLineItem item:saleslineitemtable.getDataprovider().getList()){
					
					if(lastDate.after(item.getStartDate())){
							showDialogMessage("Serivce Product start date Should not be less than a month.");
							return false;
					}
				}
			}
			
			
		}
		
		/**
		 * Date 07-02-2019 by Vijay 
		 * Des :- As per vaishali madam they dont want this validation
		 * because they will not maintain BOM for all services so for not able to create contracts 
		 */
//		/**
//		 * nidhi
//		 * 19-10-2018
//		 * for validate bom area and unit
//		 */
//		if(LoginPresenter.billofMaterialActive){
//			boolean flag = validateAreaAndUnit();
//			if(!flag){
//				showDialogMessage("Unit of mesuarement or area is not define for selected branch ."
//						+ "\n Please define area and unit of masuarement.");
//				return flag;
//			}
//		}
		/**
		 * ends here
		 */
		
		/** date 11.12.2018 added by komal for nbhc service branches button and popup **/
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableServiceBranch")){
		for(SalesLineItem item:saleslineitemtable.getDataprovider().getList()){
			double totalAmount = 0;
			if(item.getServiceBranchesInfo()!=null && item.getServiceBranchesInfo().size()>0){
			ArrayList<BranchWiseScheduling> list = item.getServiceBranchesInfo().get(item.getProductSrNo());
			if(list != null && list.size() > 0){
			 for(BranchWiseScheduling branchWiseScheduling : list){
				if(branchWiseScheduling.isCheck()){
					totalAmount += branchWiseScheduling.getArea();
				}
			 }
			}
			if(totalAmount !=  item.getTotalAmount()){
				showDialogMessage("Service branches total amount should be equal to product total amount.");
				return false;
			}
		 }
		}
	  }
		/**
		 * end komal
		 */
		/**
		 * Date 05-01-2019 by Vijay
		 * Des :- for NBHC CCPM Lead id and quotation mandatory with process config 
		 * after specific date restriction
		 */
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "MakeLeadAndQuotationIdMandatory")
				&&  !LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Admin")){
			if(!olbContractGroup.getValue(olbContractGroup.getSelectedIndex()).equals("In-House") &&
					!olbContractGroup.getValue(olbContractGroup.getSelectedIndex()).equals("In-House (On Site)")){
			String dateser =  AppUtility.getForProcessConfigurartionIsActiveOrNot("ContractLeadIDQuotationIDRestriction");
			if(dateser!=null){
			DateTimeFormat smpDate = DateTimeFormat.getFormat("dd/MM/yyyy");
			final Date serDate = (dateser != null) ? smpDate.parse(dateser) : null;
//				if( dbContractDate.getValue().equals(serDate) || dbContractDate.getValue().after(serDate)){
					if( new Date().equals(serDate) || new Date().after(serDate)){
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "EnableQuotationMandatoryForContractRenwal") && !ibOldContract.getValue().equals("") && tbQuotatinId.getValue().equals("")){
						showDialogMessage("Quotation id is mandatory! Please create quotation first then renew the contract!");
						return false;
					}
					else if(ibOldContract.getValue().equals("")){
						Console.log("ibOldContract if condition"+(ibOldContract.getValue().equals("")));
						if(tbLeadId.getValue().equals("") || tbQuotatinId.getValue().equals("")){
						showDialogMessage("Lead id and Quotation id is mandatory! Please create Lead and Quotation first!");
						return false;
						}
					}
				}
			}
			}
		}
		
	
		
		boolean serviceAmountFlag = false;
		for(SalesLineItem item:saleslineitemtable.getDataprovider().getList()){
			if(item.getCustomerBranchSchedulingInfo()!=null && item.getCustomerBranchSchedulingInfo().size()>0){
			ArrayList<BranchWiseScheduling> list = item.getCustomerBranchSchedulingInfo().get(item.getProductSrNo());
			System.out.println("list "+list.size());
			if(list != null && list.size() > 0){
				 for(BranchWiseScheduling obj : list){
					 if(obj.isCheck() && obj.getAmount()>0){
							serviceAmountFlag = true;
							System.out.println("serviceAmountFlag "+serviceAmountFlag);
					}
				 }

			}
			}
		}
		System.out.println("serviceAmountFlag =="+serviceAmountFlag);
		if(serviceAmountFlag){
			for(SalesLineItem item:saleslineitemtable.getDataprovider().getList()){
				double totalAmount = 0;
				if(item.getCustomerBranchSchedulingInfo()!=null && item.getCustomerBranchSchedulingInfo().size()>0){
				ArrayList<BranchWiseScheduling> list = item.getCustomerBranchSchedulingInfo().get(item.getProductSrNo());
				if(list != null && list.size() > 0){
					 for(BranchWiseScheduling obj : list){
						 if(obj.isCheck()){
							 totalAmount += obj.getAmount();
						}
					 }

				}
				System.out.println("totalAmount "+totalAmount);
				System.out.println("item.getTotalAmount() "+item.getTotalAmount());
				if(totalAmount !=  item.getTotalAmount()){
					showDialogMessage("Customer branches service amount should be equal to product total amount. Customer branch total amount="+totalAmount+" Product total amount="+item.getTotalAmount());
					return false;
				}
				
				}
			}
		}
		
		System.out.println("tbTicketNumber.getValue()"+tbTicketNumber.getValue());
		if(tbTicketNumber.getValue()!=null && !tbTicketNumber.getValue().equals("") && !tbTicketNumber.getValue().equals("0")){
			for(SalesLineItem item:saleslineitemtable.getDataprovider().getList()){
				if(item.getNumberOfServices()>1){
					showDialogMessage("For complaint contract no of services must be 1");
					return false;
				}
			}

		}
		
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","MakeTermsUploadMandatory")){
			 
			if(getUcUploadTAndCs().getValue()==null){
				showDialogMessage("Upload terms and conditions! It is mandatory.");
				return false;
			}
		}
		addClickEventOnActionAndNavigationMenus(); //Ashwini Patil Date:02-04-2024 this line was missing due to which 3 line menu was not coming
		
		/**
		 * @author Anil @since 28-05-2021
		 * Add validation for Product serial number
		 * extra or less services are getting generated due to duplicate product serial number
		 * Raised by Ashwini for Pecopp and Vaishnavi for IPM
		 */
		if(validateProductSerialNumber()){
			return true;
		}
	
        return true;
	 }
	
	private boolean validateProductSerialNumber() {
		
		HashSet<Integer> productSrNoHs=new HashSet<Integer>();
		List<SalesLineItem> itemList=saleslineitemtable.getDataprovider().getList();
		
		if(itemList!=null&&itemList.size()!=0){
			for(SalesLineItem item:itemList){
				productSrNoHs.add(item.getProductSrNo());
			}
			
			if(itemList.size()!=productSrNoHs.size()){
				String alert="";
				for(Integer prodSrNo:productSrNoHs){
					int srNoCtr=0;
					for(SalesLineItem item:itemList){
						if(prodSrNo==item.getProductSrNo()){
							srNoCtr++;
							if(srNoCtr>1){
								alert=alert+"Product serial number for "+item.getProductName()+" is duplicated. Kindly remove this product and add it again.\n";
							}
						}
					}
				}
				
				if(!alert.equals("")){
					showDialogMessage(alert);
					return false;
				}
			}
		}
		return true;
	}

	public boolean validateAreaAndUnit(){
		boolean flag = true,bomCheck = true;
		
		
		for(SalesLineItem item:saleslineitemtable.getDataprovider().getList()){
			
			 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(item.getPrduct());
			 if(billPrDt!=null){
					ArrayList<BranchWiseScheduling> customerBrancheslist = new ArrayList<BranchWiseScheduling>();
					customerBrancheslist.addAll(item.getCustomerBranchSchedulingInfo().get(item.getProductSrNo()));
					
					if(LoginPresenter.billofMaterialActive && customerBrancheslist!=null && customerBrancheslist.size()>0){
						for(BranchWiseScheduling branch : customerBrancheslist){
							if(branch.isCheck() )
								{
								if(branch.getUnitOfMeasurement()==null){
									flag = false;
									break;
								}
								if( branch.getArea()==0 ||  branch.getUnitOfMeasurement().trim().length() ==0){
										flag = false;
										break;
								}
								
								UnitConversionUtility unitConver = new UnitConversionUtility();

								 
								 if(!unitConver.varifyUnitConversion(billPrDt,branch.getUnitOfMeasurement())){
									 showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
									 flag = false;
										break;
								 }
							}
						}
					}
			 }else{
				 bomCheck = false;
				 flag = false;
				 showDialogMessage("Bill of materila is not created for " + item.getProductName() + " service. Please create Bom first. \n "
				 		+ " You are not allow to save Contract with this service.");
				 break;
			 }
		}
		return flag;
	}
	
	/*****************Date 29/08/2017 By Jayshree changes are made to disply the popup message premise  details are 
	 * madatory
	 */
	private boolean validatePremises() {
		System.out.println("Inside Premeise detail validation method");
		List<SalesLineItem> premise = saleslineitemtable.getDataprovider().getList();
		for (int i = 0; i < premise.size(); i++) {
			if (premise.get(i).getPremisesDetails()==null||premise.get(i).getPremisesDetails().equals("")) {
				return false;
			}
		}
		return true;
	}
	
	
	

	/********************************Clear Payment Terms Fields*******************************************/
	
	public void clearPaymentTermsFields()
	{
		ibdays.setValue(null);
		dopercent.setValue(null);
		doAmount.setValue(null);
		tbcomment.setValue("");
	}
	
	/*******************************Validate Payment Term Percent*****************************************/
	/**
	 * This method validates payment term percent. i.e the total of percent should be equal to 100
	 * @return
	 */
	
	public boolean validatePaymentTermPercent()
	{
		/**
		 * Date : 06-10-2017 BY ANIL
		 */
		if(cbServiceWiseBilling.getValue()!=null&&cbServiceWiseBilling.getValue().equals(true)){
			return true;
		}
		/**
		 * End
		 */
		
		List<PaymentTerms> payTermsLis=this.paymentTermsTable.getDataprovider().getList();
		double totalPayPercent=0;
		for(int i=0;i<payTermsLis.size();i++)
		{
			totalPayPercent=totalPayPercent+payTermsLis.get(i).getPayTermPercent();
		}
		System.out.println("Validation of Payment Percent as 100"+totalPayPercent);
		
		if(Math.round(totalPayPercent)!=100.0)
		{
			return false;
		}
		
		return true;
	}
	
	/********************************Validate Payment Terms Days******************************************/
	/**
	 * This method represents the validation of payment terms days
	 * @param retrDays
	 * @return
	 */
	
	int chk=0;
	public boolean validatePaymentTrmDays(int retrDays)
	{
		List<PaymentTerms> payTermsLis=this.paymentTermsTable.getDataprovider().getList();
		for(int i=0;i<payTermsLis.size();i++)
		{
			if(retrDays==payTermsLis.get(i).getPayTermDays())
			{
				return false;
			}
			if(retrDays<payTermsLis.get(i).getPayTermDays())
			{
				showDialogMessage("Days should be defined in increasing order!");
				chk=1;
				return false;
			}
			else{
				chk=0;
			}
			if(retrDays>10000){
				showDialogMessage("Days cannot be greater than 4 digits!");
				return false;
			}
		}
		return true;
	}
	
	
//*************rohan remove thiscode for ultra pest control changes  ******************	
	
//	public int validateContractPrice()
//	{
//		List<SalesLineItem> salesItemLis=this.getSaleslineitemtable().getDataprovider().getList();
//		int ctr=0;
//		for(int i=0;i<salesItemLis.size();i++)
//		{
//			if(salesItemLis.get(i).getPrice()==0){
//				ctr=ctr+1;
//			}
//		}
//		
//		return ctr;
//	}
	
	
	
	
	/**********************************Change Widgets Method******************************************/
	/**
	 * This method represents changing the widgets style. Used for modifying payment composite
	 * widget fields
	 */
	
	private void changeWidgets()
	{
		//  For changing the text alignments.
		this.changeTextAlignment(dototalamt);
		this.changeTextAlignment(donetpayamt);
		this.changeTextAlignment(doamtincltax);
		this.changeTextAlignment(dbDiscountAmt);
		this.changeTextAlignment(dbFinalTotalAmt);
		this.changeTextAlignment(tbroundOffAmt);
		this.changeTextAlignment(dbGrandTotalAmt);

		
		
		
		//  For changing widget widths.
		this.changeWidth(dototalamt, 200, Unit.PX);
		this.changeWidth(donetpayamt, 200, Unit.PX);
		this.changeWidth(doamtincltax, 200, Unit.PX);
		this.changeWidth(dbDiscountAmt, 200, Unit.PX);
		this.changeWidth(dbFinalTotalAmt, 200, Unit.PX);
		this.changeWidth(tbroundOffAmt, 200, Unit.PX);
		this.changeWidth(dbGrandTotalAmt, 200, Unit.PX);

		
	}
	
	/*******************************Text Alignment Method******************************************/
	/**
	 * Method used for align of payment composite fields
	 * @param widg
	 */
	
	private void changeTextAlignment(Widget widg)
	{
		widg.getElement().getStyle().setTextAlign(TextAlign.CENTER);
	}
	
	/*****************************Change Width Method**************************************/
	/**
	 * Method used for changing width of fields
	 * @param widg
	 * @param size
	 * @param unit
	 */
	
	private void changeWidth(Widget widg,double size,Unit unit)
	{
		widg.getElement().getStyle().setWidth(size, unit);
	}
	
	/**
	 * Date : 12-10-2017 BY ANIL
	 * Added parameter chkValFlag which check the validation of new customer 
	 * as when we create Quick Contract and viewing those contract from contract screen customer information got disaapeared
	 * same thing happened when we came from dashboard 
	 * 
	 */
	
	public void checkCustomerStatus(int cCount,final boolean segmentFlag,final boolean chkValFlag)
	{
		MyQuerry querry=new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(c.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("count");
		temp.setIntValue(cCount);
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Customer());
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
				showDialogMessage("An Unexpected error occurred!");
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
					for(SuperModel model:result)
					{
						Customer custEntity = (Customer)model;
						cust= custEntity;
						if(chkValFlag&&custEntity.getNewCustomerFlag()==true){
							showDialogMessage("Please fill customer information by editing customer");
							personInfoComposite.clear();
						}
						else if(AppConstants.ACTIVE.equals(custEntity.getStatus().trim())&&(custEntity.getNewCustomerFlag()==false)){
							 getPersonInfoComposite().getId().getElement().addClassName("personactive");
				    		 getPersonInfoComposite().getName().getElement().addClassName("personactive");
				    		 getPersonInfoComposite().getPhone().getElement().addClassName("personactive");
				    		 getPersonInfoComposite().getTbpocName().getElement().addClassName("personactive");
				    		 
				    		 /**
				    		  * Date:10-07-2017 BY NIDHI
				    		  */
				    		 if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC")&&segmentFlag){
				    			 tbCustSegment.setValue(custEntity.getCategory());
				    		 }
				    		 /**
				    		  * End
				    		  */
				    		 
				    		 /**
				    		  * Date : 24-10-2017 BY ANIL
				    		  * Copying the customer's reference no 1 to contract's refence no 1
				    		  * this is only for NBHC
				    		  */
				    		 if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC")&&segmentFlag){
				    			 tbReferenceNumber.setValue(custEntity.getRefrNumber1());
				    		 }
				    		 /**
				    		  * End
				    		  */
						}
						
						/**Date 9-6-2019 by Amol map a customer category and customer type For ORKIN using Process Confi ****/
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "MapCustomerCategoryAndTypeaAsaDocumentCategoryAndType")){
							custCatandType=true;
						}
						if(custCatandType){
							olbContractCategory.setValue(custEntity.getCategory());
						    olbContractType.setValue(custEntity.getType());
						}
							customerEmail=custEntity.getEmail();
						/**
						 * @author Abhinav Bihade
						 * @since 03/01/2019
						 * As per Rahul Tiwari's Reqiurement for Orkin:When we create a contract for a customer, 
						 * then Customer Group should map in Sale Source field in contract page   
						 */
						olbcontractPeriod.setValue(custEntity.getGroup());
						
						
						
						
						
						/**
						 * Date : 04-11-2017 BY ANIL
						 * if customer approval process is true then we can not make Quotation of customer whose status is created. 
						 */
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "CustomerApprovalProcess")){
							if(!custEntity.getStatus().equals(Customer.CUSTOMERACTIVE)){
								showDialogMessage("Please approve the customer!");
								personInfoComposite.clear();
							}
						}
						/**
						 * End
						 */
						
					}
					
					if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT || AppMemory.getAppMemory().currentState==ScreeenState.NEW){
						updateTaxesDetails(cust);
					}
				}
			 });
	}
	
	/*************************************Service Scheduling************************************/

	public List<ServiceSchedule> reactfordefaultTable(SalesLineItem lis) {
		
		
		List<SalesLineItem> salesitemlis=new ArrayList<SalesLineItem>();
		salesitemlis.add(lis);
//		System.out.println("@@@@@@@@ PRODUCT LIST SIZE ::: "+salesitemlis.size());
		ArrayList<ServiceSchedule> serschelist=new ArrayList<ServiceSchedule>();
		serschelist.clear();
		boolean unitflag = false;
		for(int i=0;i<salesitemlis.size();i++){
			
			System.out.println("lis size"+salesitemlis.get(i).getBranchSchedulingInfo()+"-"+salesitemlis.get(i).getBranchSchedulingInfo1()
					+"-"+salesitemlis.get(i).getBranchSchedulingInfo2()+"-"+salesitemlis.get(i).getBranchSchedulingInfo3());
			/**
			 * 
			 * Date : 29-03-2017 By ANil
			 */
			boolean branchSchFlag=false;
			int qty=0;
			
//			if(salesitemlis.get(i).getBranchSchedulingList()!=null
//					&&salesitemlis.get(i).getBranchSchedulingList().size()!=0){
//				branchSchFlag=true;
//				for(BranchWiseScheduling branch:salesitemlis.get(i).getBranchSchedulingList()){
//					if(branch.isCheck()==true){
//						qty++;
//					}
//				}
//			}else{
//				qty=(int) salesitemlis.get(i).getQty();
//			}
			
			
			/**
			 * Date 02-05-2018 
			 * Developer : Vijay
			 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
			 * so i have updated below code using hashmap. below old code commneted and updated code below
			 */
			
//			/**
//			 * Date : 03-04-2017 By ANIL
//			 */
//			
////			ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(salesitemlis.get(i).getBranchSchedulingInfo());
//			/**
//			 * Date : 12-06-2017 BY ANIL
//			 */
//			String[] branchArray=new String[10];
//			if(salesitemlis.get(i).getBranchSchedulingInfo()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo().equals("")){
//				branchArray[0]=salesitemlis.get(i).getBranchSchedulingInfo();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo1()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo1().equals("")){
//				branchArray[1]=salesitemlis.get(i).getBranchSchedulingInfo1();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo2()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo2().equals("")){
//				branchArray[2]=salesitemlis.get(i).getBranchSchedulingInfo2();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo3()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo3().equals("")){
//				branchArray[3]=salesitemlis.get(i).getBranchSchedulingInfo3();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo4()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo4().equals("")){
//				branchArray[4]=salesitemlis.get(i).getBranchSchedulingInfo4();
//			}
//			
//			if(salesitemlis.get(i).getBranchSchedulingInfo5()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo5().equals("")){
//				branchArray[5]=salesitemlis.get(i).getBranchSchedulingInfo5();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo6()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo6().equals("")){
//				branchArray[6]=salesitemlis.get(i).getBranchSchedulingInfo6();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo7()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo7().equals("")){
//				branchArray[7]=salesitemlis.get(i).getBranchSchedulingInfo7();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo8()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo8().equals("")){
//				branchArray[8]=salesitemlis.get(i).getBranchSchedulingInfo8();
//			}
//			if(salesitemlis.get(i).getBranchSchedulingInfo9()!=null
//					&&!salesitemlis.get(i).getBranchSchedulingInfo9().equals("")){
//				branchArray[9]=salesitemlis.get(i).getBranchSchedulingInfo9();
//			}
//			
//			/**
//			 * End
//			 */
//			
//			ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(branchArray);
			
			/** Date 02-05-2018 By vijay for updated branch scheduling using hashmap **/
			ArrayList<BranchWiseScheduling> branchSchedulingList = salesitemlis.get(i).getCustomerBranchSchedulingInfo().get(salesitemlis.get(i).getProductSrNo());
			
			if(branchSchedulingList!=null&&branchSchedulingList.size()!=0){
				branchSchFlag=true;
				qty=branchSchedulingList.size();
//				for(BranchWiseScheduling branch:branchSchedulingList){
//					if(branch.isCheck()==true){
//						qty++;
//					}
//				}
			}else{
				qty=(int) salesitemlis.get(i).getQty();
			}
			
			/**
			 * End
			 */
			
			if(branchSchFlag==true){
				for(int k=0;k < qty;k++){
					
				 	if(branchSchedulingList.get(k).isCheck()==true){
					long noServices=(long) (salesitemlis.get(i).getNumberOfServices());
					int noOfdays=salesitemlis.get(i).getDuration();
					int interval= (int) (noOfdays/salesitemlis.get(i).getNumberOfServices());
//					Date servicedate=new Date();
					
					/**
					 * Date 16-06-2018 By vijay
					 * Des :- backdated contract, service schedule Date service date issue becuase start date set as contract date
					 */
					Date servicedate=null;

					if(dbContractDate.getValue()!=null){
						servicedate = dbContractDate.getValue();
					}else{
						servicedate = new Date();
					}
					/**
					 * ends here
					 */
					
					Date d=new Date(servicedate.getTime());
					Date productEndDate= new Date(servicedate.getTime());
					CalendarUtil.addDaysToDate(productEndDate, noOfdays);
					Date prodenddate=new Date(productEndDate.getTime());
					productEndDate=prodenddate;
					Date tempdate=new Date();
					
					for(int j=0;j<noServices;j++){
						if(j>0)
						{
							CalendarUtil.addDaysToDate(d, interval);
							tempdate=new Date(d.getTime());
						}
						
						ServiceSchedule ssch=new ServiceSchedule();
//						Date Stardaate=new Date();
						 /** Date 16-06-2018 By vijay above old line commented **/
						Date Stardaate=servicedate;

						
						//  rohan added this 1 field 
//						System.out.println("In side method RRRRRRRRRRRRR"+salesitemlis.get(i).getProductSrNo());
						ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
						
						ssch.setScheduleStartDate(Stardaate);
						ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
						ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
						ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
						ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
						ssch.setScheduleServiceNo(j+1);
						ssch.setScheduleProdStartDate(Stardaate);
						ssch.setScheduleProdEndDate(productEndDate);
						ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
						/**
						 *   rohan commented this code for NBHC 
						 *   Purpose : wanetd to set servicing branch as customer branch is selected  
						 */
//						ssch.setServicingBranch(olbbBranch.getValue());
						/**
						 * ends here 
						 */
						
//						System.out.println("SERVICING TIME ::: "+salesitemlis.get(i).getServicingTIme());
//						System.out.println("BRANCH ::: "+olbbBranch.getValue());
						ssch.setScheduleServiceTime("Flexible");
						
						if(j==0){
							ssch.setScheduleServiceDate(servicedate);
						}
						if(j!=0){
							if(productEndDate.before(d)==false){
								ssch.setScheduleServiceDate(tempdate);
							}else{
								ssch.setScheduleServiceDate(productEndDate);
							}
						}
					
						ssch.setScheduleProBranch(branchSchedulingList.get(k).getBranchName());
						if (branchSchedulingList.get(k).getServicingBranch()!=null) {
							ssch.setServicingBranch(branchSchedulingList.get(k).getServicingBranch());
						} 
						
						int dayIndex=AppUtility.getDayIndex(branchSchedulingList.get(k).getDay());
						
						if(dayIndex!=0){
							String dayOfWeek1 = format.format(d);
							int adate = Integer.parseInt(dayOfWeek1);
							int adday1 = dayIndex - 1;
							int adday = 0;
							if (adate <= adday1) {
								adday = (adday1 - adate);
							} else {
								adday = (7 - adate + adday1);
							}
							
							Date newDate = new Date(d.getTime());
							CalendarUtil.addDaysToDate(newDate, adday);
							Date day = new Date(newDate.getTime());

							if (productEndDate.before(day) == false) {
								ssch.setScheduleServiceDate(day);
							} else {
								ssch.setScheduleServiceDate(productEndDate);
							}

							CalendarUtil.addDaysToDate(d, interval);
							Date tempdate1 = new Date(d.getTime());
							d = tempdate1;
							ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
						}
						
						/**
						 * Date 01-04-2017 added by vijay for week number
						 */
						int weaknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
						ssch.setWeekNo(weaknumber);
						
						/**
						 * ends here
						 */
						
					ssch.setScheduleServiceDay(serviceDay(ssch.getScheduleServiceDate()));
					
					 /***
					  *  *:*:*
					  * nidhi
					  * 18-09-2018
					  * for map bill product
					  * if any update please update in else conditions also
					  */
					 if(LoginPresenter.billofMaterialActive){
						 
						 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(lis.getPrduct());
						 if(billPrDt!=null){
							 
							 
							 UnitConversionUtility unitConver = new UnitConversionUtility();

							 
							 if(unitConver.varifyUnitConversion(billPrDt, lis.getAreaUnit())){
								 
								 List<ServiceSchedule> sev = new ArrayList<ServiceSchedule>();
								 sev.add(ssch);
								 sev = unitConver.getServiceitemProList(sev,lis.getPrduct(),lis,billPrDt);
								 
								 if(sev!=null && sev.size()>0){
									 ssch = sev.get(0).copyOfObject();
								 }
								 
								 
							 }else{
								 unitflag = true;
//								 showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
							 }
							 
							 
							 
							 
						 }
					 }
					 /**
					  * end
					  */
					 
					 /**
					  * @author Vijay Chougule Date :- 09-09-2020
					  * Des :- added Service duration 
					  */
						 if(branchSchedulingList.get(k).getServiceDuration()!=0){
							 ssch.setServiceDuration(branchSchedulingList.get(k).getServiceDuration());
						 }
					 /**
					  * ends here
					  */
						 
					serschelist.add(ssch);
					 /**
					  * @author Vijay Chougule Date :- 06-11-2020
					  * Des :- Bug - if branch as service address then its not scheduling as per compaintTat requirement
					  * so added code
					  */
					 if(ContractForm.complainTatFlag){
							Console.log(".. Complain flag..");
							List<ServiceSchedule>list=ContractForm.updateSchedulingListComponentWise(ssch, branchSchedulingList.get(k));
							if(list!=null&&list.size()!=0){
								serschelist.addAll(list);

							}
						}
					}
					/**
					 * ends here
					 */
					
				}
				}
			}else{
			
			for(int k=0;k < qty;k++){
			
			 	
			long noServices=(long) (salesitemlis.get(i).getNumberOfServices());
			int noOfdays=salesitemlis.get(i).getDuration();
			int interval= (int) (noOfdays/salesitemlis.get(i).getNumberOfServices());
			Date servicedate=new Date();
			
			/**
			 * Date 16-06-2018 By vijay
			 * Des :- backdated contract, service schedule Date service date issue becuase start date set as contract date
			 */
//			Date servicedate=null;

			if(dbContractDate.getValue()!=null){
				servicedate = dbContractDate.getValue();
			}else{
				servicedate = new Date();
			}
			/**
			 * ends here
			 */
			
			Date d=new Date(servicedate.getTime());
			Date productEndDate= new Date(servicedate.getTime());
			CalendarUtil.addDaysToDate(productEndDate, noOfdays);
			Date prodenddate=new Date(productEndDate.getTime());
			productEndDate=prodenddate;
			Date tempdate=new Date();
			
			for(int j=0;j<noServices;j++){
				if(j>0)
				{
					CalendarUtil.addDaysToDate(d, interval);
					tempdate=new Date(d.getTime());
				}
				
				ServiceSchedule ssch=new ServiceSchedule();
				/**
				 * Date 16-06-2018 By vijay
				 */
//				Date Stardaate=new Date();
				Date Stardaate=servicedate;
				/**
				 * ends here
				 */
				
				//  rohan added this 1 field 
//				System.out.println("In side method RRRRRRRRRRRRR"+salesitemlis.get(i).getProductSrNo());
				ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
				
				ssch.setScheduleStartDate(Stardaate);
				ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
				ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
				ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
				ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
				ssch.setScheduleServiceNo(j+1);
				ssch.setScheduleProdStartDate(Stardaate);
				ssch.setScheduleProdEndDate(productEndDate);
				ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
				/**
				 *   rohan commented this code for NBHC 
				 *   Purpose : wanetd to set servicing branch as customer branch is selected  
				 */
//				ssch.setServicingBranch(olbbBranch.getValue());
				/**
				 * ends here 
				 */
				
//				System.out.println("SERVICING TIME ::: "+salesitemlis.get(i).getServicingTIme());
//				System.out.println("BRANCH ::: "+olbbBranch.getValue());
				ssch.setScheduleServiceTime("Flexible");
				
				if(j==0){
					ssch.setScheduleServiceDate(servicedate);
				}
				if(j!=0){
					if(productEndDate.before(d)==false){
						ssch.setScheduleServiceDate(tempdate);
					}else{
						ssch.setScheduleServiceDate(productEndDate);
					}
				}
				
				if(customerbranchlist.size()==qty){
					ssch.setScheduleProBranch(customerbranchlist.get(k).getBusinessUnitName());
					/**
					 * rohan added this code for automatic servicing branch setting for NBHC 
					 */
					
					if(customerbranchlist.get(k).getBranch()!=null)
					{
						ssch.setServicingBranch(customerbranchlist.get(k).getBranch());
					}
					else
					{
						ssch.setServicingBranch(olbbBranch.getValue());
					}
					/**
					 * ends here 
					 */
				}else if(customerbranchlist.size()==0){
					ssch.setScheduleProBranch("Service Address");
					/**
					 * rohan added this code for automatic servicing branch setting for NBHC 
					 */
						ssch.setServicingBranch(olbbBranch.getValue());
					/**
					 * ends here 
					 */	
						
				}else{
					ssch.setScheduleProBranch(null);
				}
				
				ssch.setScheduleServiceDay(serviceDay(ssch.getScheduleServiceDate()));
				
				/**
				 * Date 01-04-2017 added by vijay for week number
				 */
				int weaknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
				ssch.setWeekNo(weaknumber);
				
				/**
				 * ends here
				 */
				 /***
				  *  *:*:*
				  * nidhi
				  * 18-09-2018
				  * for map bill product
				  * if any update please update in else conditions also
				  */
				 if(LoginPresenter.billofMaterialActive){
					 
					 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(lis.getPrduct());
					 if(billPrDt!=null){
						 
						 
						 UnitConversionUtility unitConver = new UnitConversionUtility();

						 
						 if(unitConver.varifyUnitConversion(billPrDt, lis.getAreaUnit())){
							 
							 List<ServiceSchedule> sev = new ArrayList<ServiceSchedule>();
							 sev.add(ssch);
							 sev = unitConver.getServiceitemProList(sev,lis.getPrduct(),lis,billPrDt);
							 
							 if(sev!=null && sev.size()>0){
								 ssch = sev.get(0).copyOfObject();
							 }
							 
							 
						 }else{
							 /**
							  * nidhi 7-1-2018 |*|
							  */
							 unitflag = true;
							 /**
							  * end
							  */
//							 showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
						 }
						 
						 
						 
						 
					 }
				 }
				 /**
				  * end
				  */
				serschelist.add(ssch);
			}
		}
			
		}
			
			
		}
		
		
		//  rohan added this for testing 
		
//		for (int l = 0; l < serschelist.size(); l++)
//		{
//			System.out.println("My Rohan in service schedule table "+serschelist.get(l).getSerSrNo());
//		}
		/**
		 * nidhi |*|
		 */
		if(unitflag){
			showDialogMessage("Unit of service are and product group detail area not mapped at branch level. Or conversion not available");
		}
		
		return serschelist;
	}

	public void checkCustomerBranch(int customerId) {
		Console.log("Ashwini checkCustomerBranch called from contractform");
		int custid=customerId;
		MyQuerry querry=new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(c.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("cinfo.count");
		temp.setIntValue(custid);
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("status");
		temp.setBooleanvalue(true);
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerBranchDetails());
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
				showDialogMessage("An Unexpected error occurred!");
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				customerbranchlist.clear();
				
				if(result.size()>0)
				{
					for(SuperModel model:result)
					{
						CustomerBranchDetails custbranchEntity = (CustomerBranchDetails)model;
						CustomerBranchDetails bbb=new CustomerBranchDetails(); 
				    	bbb.setBusinessUnitName(custbranchEntity.getBusinessUnitName());
						bbb.setCinfo(custbranchEntity.getCinfo());
						bbb.setBranch(custbranchEntity.getBranch());
						/** date 17/4/2018 added by komal to set area from customer branch**/
						bbb.setArea(custbranchEntity.getArea());
						/**
						 * nidhi
						 * 20-09-2018
						 *  *:*:*
						 */
						bbb.setUnitOfMeasurement(custbranchEntity.getUnitOfMeasurement());
						/**
						 * end
						 */
						
						/**
						 * @author Anil
						 * @since 06-06-2020
						 */
						bbb.setTierName(custbranchEntity.getTierName());
						bbb.setTat(custbranchEntity.getTat());
						customerbranchlist.add(bbb);
					}
					Console.log("Ashwini customerbranchlist size="+customerbranchlist.size()+"after checkCustomerBranch call from contractform");
				}
				}
			 });
		}
	

	public static String serviceDay(Date d){
		String day=null;
		
		String dayOfWeek = format.format(d);
		if(dayOfWeek.equals("0"))
		   return day="Sunday";
		if(dayOfWeek.equals("1"))
			return day="Monday";
		if(dayOfWeek.equals("2"))
			return day="Tuesday";
		if(dayOfWeek.equals("3"))
			return day="Wednesday";
		if(dayOfWeek.equals("4"))
			return day="Thursday";
		if(dayOfWeek.equals("5"))
			return day="Friday";
		if(dayOfWeek.equals("6"))
			return day="Saturday";
		
		return day;
	}
	
	
	/**
	 *  Service Scheduling Method for Contract Renewal
	 *  When contract renewal is clicked all service scheduling as per date is updated
	 */
	
	public List<ServiceSchedule> reactfordefaultTableOnRenewalForSettingBranches(ArrayList<SalesLineItem> lis) {
		
		
		ArrayList<SalesLineItem> salesitemlis=new ArrayList<SalesLineItem>();
		salesitemlis=lis;
		ArrayList<ServiceSchedule> serschelist=new ArrayList<ServiceSchedule>();
		serschelist.clear();
		
		for(int i=0;i<salesitemlis.size();i++){
		
			int qty=0;
			qty=(int) salesitemlis.get(i).getQty();
			for(int k=0;k < qty;k++){
		
			long noServices=(long) (salesitemlis.get(i).getNumberOfServices());
			int noOfdays=salesitemlis.get(i).getDuration();
			int interval= (int) (noOfdays/salesitemlis.get(i).getNumberOfServices());
			//  old code 
//			Date servicedate= new Date();
			//  new code by rohan on Date : 14/3/2017
			Date servicedate= salesitemlis.get(i).getStartDate();
			
			Date d=new Date(servicedate.getTime());
			Date productEndDate= new Date(servicedate.getTime());
			CalendarUtil.addDaysToDate(productEndDate, noOfdays);
			Date prodenddate=new Date(productEndDate.getTime());
			productEndDate=prodenddate;
			Date tempdate=new Date();
			for(int j=0;j<noServices;j++){
	
				
				if(j>0)
				{
					CalendarUtil.addDaysToDate(d, interval);
					tempdate=new Date(d.getTime());
				}
				
				ServiceSchedule ssch=new ServiceSchedule();
				Date Stardaate=new Date();
				
				ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
				ssch.setScheduleStartDate(Stardaate);
				ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
				ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
				ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
				ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
				ssch.setScheduleServiceNo(j+1);
				ssch.setScheduleProdStartDate(Stardaate);
				ssch.setScheduleProdEndDate(productEndDate);
//				String str="Flexible";
				ssch.setScheduleServiceTime("Flexible");
				if(j==0){
					ssch.setScheduleServiceDate(servicedate);
					}
				if(j!=0){
					if(productEndDate.before(d)==false){
						ssch.setScheduleServiceDate(tempdate);
					}else{
						ssch.setScheduleServiceDate(productEndDate);
					}
				}
				ssch.setScheduleProBranch(null);
				ssch.setScheduleServiceDay(serviceDay(ssch.getScheduleServiceDate()));
				serschelist.add(ssch);
			}
		}
		}
		return serschelist;
	}
	
	
	
	
	/**
	 *  Service Scheduling Method for Contract Renewal
	 *  When contract renewal is clicked all service scheduling as per date is updated
	 */
	
	public List<ServiceSchedule> reactfordefaultTableOnRenewal(ArrayList<SalesLineItem> lis) {
		
		
		ArrayList<SalesLineItem> salesitemlis=new ArrayList<SalesLineItem>();
		salesitemlis=lis;
		ArrayList<ServiceSchedule> serschelist=new ArrayList<ServiceSchedule>();
		serschelist.clear();
		/**
		 * nidhi |*|
		 * 7-1-2019
		 */
		boolean unitFlag = false;
		for(int i=0;i<salesitemlis.size();i++){
			
			/**
			 * 
			 * Date : 13-05-2017 By ANil
			 */
			boolean branchSchFlag=false;
			int qty=0;
//			qty=(int) salesitemlis.get(i).getQty();
			
			/**
			 * Date : 12-06-2017 By ANIL
			 */
			String[] branchArray=new String[10];
			if(salesitemlis.get(i).getBranchSchedulingInfo()!=null
					&&!salesitemlis.get(i).getBranchSchedulingInfo().equals("")){
				branchArray[0]=salesitemlis.get(i).getBranchSchedulingInfo();
			}
			if(salesitemlis.get(i).getBranchSchedulingInfo1()!=null
					&&!salesitemlis.get(i).getBranchSchedulingInfo1().equals("")){
				branchArray[1]=salesitemlis.get(i).getBranchSchedulingInfo1();
			}
			if(salesitemlis.get(i).getBranchSchedulingInfo2()!=null
					&&!salesitemlis.get(i).getBranchSchedulingInfo2().equals("")){
				branchArray[2]=salesitemlis.get(i).getBranchSchedulingInfo2();
			}
			if(salesitemlis.get(i).getBranchSchedulingInfo3()!=null
					&&!salesitemlis.get(i).getBranchSchedulingInfo3().equals("")){
				branchArray[3]=salesitemlis.get(i).getBranchSchedulingInfo3();
			}
			if(salesitemlis.get(i).getBranchSchedulingInfo4()!=null
					&&!salesitemlis.get(i).getBranchSchedulingInfo4().equals("")){
				branchArray[4]=salesitemlis.get(i).getBranchSchedulingInfo4();
			}
			
			
			if(salesitemlis.get(i).getBranchSchedulingInfo5()!=null
					&&!salesitemlis.get(i).getBranchSchedulingInfo5().equals("")){
				branchArray[5]=salesitemlis.get(i).getBranchSchedulingInfo5();
			}
			if(salesitemlis.get(i).getBranchSchedulingInfo6()!=null
					&&!salesitemlis.get(i).getBranchSchedulingInfo6().equals("")){
				branchArray[6]=salesitemlis.get(i).getBranchSchedulingInfo6();
			}
			if(salesitemlis.get(i).getBranchSchedulingInfo7()!=null
					&&!salesitemlis.get(i).getBranchSchedulingInfo7().equals("")){
				branchArray[7]=salesitemlis.get(i).getBranchSchedulingInfo7();
			}
			if(salesitemlis.get(i).getBranchSchedulingInfo8()!=null
					&&!salesitemlis.get(i).getBranchSchedulingInfo8().equals("")){
				branchArray[8]=salesitemlis.get(i).getBranchSchedulingInfo8();
			}
			if(salesitemlis.get(i).getBranchSchedulingInfo9()!=null
					&&!salesitemlis.get(i).getBranchSchedulingInfo9().equals("")){
				branchArray[9]=salesitemlis.get(i).getBranchSchedulingInfo9();
			}
			
			/**
			 * ENd
			 */
			
//			ArrayList<BranchWiseScheduling> branchSchedulingList=AppUtility.getBranchSchedulingList(branchArray);
			
			/**
			 * Date 02-05-2018 
			 * Developer : Vijay
			 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
			 * so i have updated below code using hashmap. So old contract i will get the branches from string and new contract from hashmpa if branches exist 
			 */
			
			ArrayList<BranchWiseScheduling> branchSchedulingList=null;
			if(salesitemlis.get(i).getBranchSchedulingInfo()!=null
					&&!salesitemlis.get(i).getBranchSchedulingInfo().equals("")){
				branchSchedulingList=AppUtility.getBranchSchedulingList(branchArray);
			}else{
				if(salesitemlis.get(i).getCustomerBranchSchedulingInfo()!=null)
				branchSchedulingList = salesitemlis.get(i).getCustomerBranchSchedulingInfo().get(salesitemlis.get(i).getProductSrNo());
			}
			
			/**
			 * ends here
			 */
			
			if(branchSchedulingList!=null&&branchSchedulingList.size()!=0){
				branchSchFlag=true;
				qty=branchSchedulingList.size();
			}else{
				qty=(int) salesitemlis.get(i).getQty();
			}
			
			if(branchSchFlag){

				for(int k=0;k < qty;k++){
					
				 	if(branchSchedulingList.get(k).isCheck()==true){
					long noServices=(long) (salesitemlis.get(i).getNumberOfServices());
					int noOfdays=salesitemlis.get(i).getDuration();
					int interval= (int) (noOfdays/salesitemlis.get(i).getNumberOfServices());
					
//					Date servicedate=new Date();
					/**
					 * Date : 13-05-2017 By Anil
					 */
					Date servicedate=salesitemlis.get(i).getStartDate();
					/**
					 * End
					 */
					Date d=new Date(servicedate.getTime());
					Date productEndDate= new Date(servicedate.getTime());
					CalendarUtil.addDaysToDate(productEndDate, noOfdays);
					Date prodenddate=new Date(productEndDate.getTime());
					productEndDate=prodenddate;
					Date tempdate=new Date();
					
					for(int j=0;j<noServices;j++){
						if(j>0)
						{
							CalendarUtil.addDaysToDate(d, interval);
							tempdate=new Date(d.getTime());
						}
						
						ServiceSchedule ssch=new ServiceSchedule();
						Date Stardaate=new Date();
						
						//  rohan added this 1 field 
//						System.out.println("In side method RRRRRRRRRRRRR"+salesitemlis.get(i).getProductSrNo());
						ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
						
						ssch.setScheduleStartDate(Stardaate);
						ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
						ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
						ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
						ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
						ssch.setScheduleServiceNo(j+1);
						ssch.setScheduleProdStartDate(Stardaate);
						ssch.setScheduleProdEndDate(productEndDate);
						ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
						/**
						 *   rohan commented this code for NBHC 
						 *   Purpose : wanetd to set servicing branch as customer branch is selected  
						 */
//						ssch.setServicingBranch(olbbBranch.getValue());
						/**
						 * ends here 
						 */
						
//						System.out.println("SERVICING TIME ::: "+salesitemlis.get(i).getServicingTIme());
//						System.out.println("BRANCH ::: "+olbbBranch.getValue());
						ssch.setScheduleServiceTime("Flexible");
						
						if(j==0){
							ssch.setScheduleServiceDate(servicedate);
						}
						if(j!=0){
							if(productEndDate.before(d)==false){
								ssch.setScheduleServiceDate(tempdate);
							}else{
								ssch.setScheduleServiceDate(productEndDate);
							}
						}
					
						ssch.setScheduleProBranch(branchSchedulingList.get(k).getBranchName());
						if (branchSchedulingList.get(k).getServicingBranch()!=null) {
							ssch.setServicingBranch(branchSchedulingList.get(k).getServicingBranch());
						} 
						
						int dayIndex=AppUtility.getDayIndex(branchSchedulingList.get(k).getDay());
						
						if(dayIndex!=0){
							String dayOfWeek1 = format.format(d);
							int adate = Integer.parseInt(dayOfWeek1);
							int adday1 = dayIndex - 1;
							int adday = 0;
							if (adate <= adday1) {
								adday = (adday1 - adate);
							} else {
								adday = (7 - adate + adday1);
							}
							
							Date newDate = new Date(d.getTime());
							CalendarUtil.addDaysToDate(newDate, adday);
							Date day = new Date(newDate.getTime());

							if (productEndDate.before(day) == false) {
								ssch.setScheduleServiceDate(day);
							} else {
								ssch.setScheduleServiceDate(productEndDate);
							}

							CalendarUtil.addDaysToDate(d, interval);
							Date tempdate1 = new Date(d.getTime());
							d = tempdate1;
							ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
						}
						
						/**
						 * Date 01-04-2017 added by vijay for week number
						 */
						int weaknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
						ssch.setWeekNo(weaknumber);
						
						/**
						 * ends here
						 */
						
					ssch.setScheduleServiceDay(serviceDay(ssch.getScheduleServiceDate()));
					
					/**
					 * Date : 16-06-2017 By ANIL
					 */
					ssch.setServiceRemark(salesitemlis.get(i).getRemark());
					/**
					 * End
					 */
					/***
					  * nidhi |*|
					  * 7-01-2019
					  * for map bill product
					  * if any update please update in else conditions also
					  */
					 if(LoginPresenter.billofMaterialActive){
						 
						 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(salesitemlis.get(i).getPrduct());
						 if(billPrDt!=null){
							 
							 
							 UnitConversionUtility unitConver = new UnitConversionUtility();

							 
							 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
								 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
								 seList.add(ssch);
								 seList = unitConver.getServiceitemProList(seList,salesitemlis.get(i).getPrduct(),salesitemlis.get(i),billPrDt);
								 if(seList!=null && seList.size()>0)
									 ssch = seList.get(0);
								 
							 }else{
								 unitFlag = true;
//								 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
							 }
							 
							 
							 
							 
						 }
					 }
					 /**
					  * end
					  */
					 
					 /**
					  * @author Vijay Chougule Date :- 09-09-2020
					  * Des :- added Service duration 
					  */

					 if(branchSchedulingList.get(k).getServiceDuration()!=0){
						 ssch.setServiceDuration(branchSchedulingList.get(k).getServiceDuration());
					 }
					 /**
					  * ends here
					  */
					 
					serschelist.add(ssch);
					}
					
				}
				}
			
			}else{
				
			for(int k=0;k < qty;k++){
		
			long noServices=(long) (salesitemlis.get(i).getNumberOfServices());
			int noOfdays=salesitemlis.get(i).getDuration();
			int interval= (int) (noOfdays/salesitemlis.get(i).getNumberOfServices());
			
//			Date servicedate=new Date();
			/**
			 * Date : 13-05-2017 By Anil
			 */
			Date servicedate=salesitemlis.get(i).getStartDate();
			/**
			 * End
			 */
			
			Date d=new Date(servicedate.getTime());
			Date productEndDate= new Date(servicedate.getTime());
			CalendarUtil.addDaysToDate(productEndDate, noOfdays);
			Date prodenddate=new Date(productEndDate.getTime());
			productEndDate=prodenddate;
			Date tempdate=new Date();
			for(int j=0;j<noServices;j++){
	
				
				if(j>0)
				{
					CalendarUtil.addDaysToDate(d, interval);
					tempdate=new Date(d.getTime());
				}
				
				ServiceSchedule ssch=new ServiceSchedule();
				Date Stardaate=new Date();
				
				ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
				ssch.setScheduleStartDate(Stardaate);
				ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
				ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
				ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
				ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
				ssch.setScheduleServiceNo(j+1);
				ssch.setScheduleProdStartDate(Stardaate);
				ssch.setScheduleProdEndDate(productEndDate);
//				String str="Flexible";
				ssch.setScheduleServiceTime("Flexible");
				if(j==0){
					ssch.setScheduleServiceDate(servicedate);
					}
				if(j!=0){
					if(productEndDate.before(d)==false){
						ssch.setScheduleServiceDate(tempdate);
					}else{
						ssch.setScheduleServiceDate(productEndDate);
					}
				}
				if(customerbranchlist.size()==qty){
					ssch.setScheduleProBranch(customerbranchlist.get(k).getBusinessUnitName());
					/**
					 * rohan added this code for automatic servicing branch setting for NBHC 
					 */
					if(customerbranchlist.get(k).getBranch()!=null)
					{
						ssch.setServicingBranch(customerbranchlist.get(k).getBranch());
					}
					else
					{
						ssch.setServicingBranch(olbbBranch.getValue());
					}
					/**
					 * ends here 
					 */
					
					
				}else if(customerbranchlist.size()==0){
					ssch.setScheduleProBranch("Service Address");
					
					/**
					 * rohan added this code for automatic servicing branch setting for NBHC 
					 */
						ssch.setServicingBranch(olbbBranch.getValue());
					/**
					 * ends here 
					 */	
				}else{
					ssch.setScheduleProBranch(null);
				}
				ssch.setScheduleServiceDay(serviceDay(ssch.getScheduleServiceDate()));
				/***
				  * nidhi |*|
				  * 7-01-2019
				  * for map bill product
				  * if any update please update in else conditions also
				  */
				 if(LoginPresenter.billofMaterialActive){
					 
					 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(salesitemlis.get(i).getPrduct());
					 if(billPrDt!=null){
						 
						 
						 UnitConversionUtility unitConver = new UnitConversionUtility();

						 
						 if(branchSchedulingList.get(k).getUnitOfMeasurement() != null && unitConver.varifyUnitConversion(billPrDt, branchSchedulingList.get(k).getUnitOfMeasurement())){
							 List<ServiceSchedule> seList = new ArrayList<ServiceSchedule>();
							 seList.add(ssch);
							 seList = unitConver.getServiceitemProList(seList,salesitemlis.get(i).getPrduct(),salesitemlis.get(i),billPrDt);
							 if(seList!=null && seList.size()>0)
								 ssch = seList.get(0);
							 
						 }else{
							 unitFlag = true;
//							 form.showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
						 }
						 
						 
						 
						 
					 }
				 }
				 /**
				  * end
				  */
				serschelist.add(ssch);
			}
		}
			}
		}
		/**
		 * nidhi |*|
		 * 7-01-2018
		 */
		if(unitFlag){
			 showDialogMessage("Unit of service are and product group detail area not mapped at branch level. Or conversion not available");
		}
		return serschelist;
	}

	
/*****************************************Type Drop Down Logic****************************************/
	
	@Override
	public void onChange(ChangeEvent event) {
		if(event.getSource().equals(doAmount))
		{
			if(doAmount.getValue()!=null){
				Console.log("In amount not null");
//				DecimalFormat df = new DecimalFormat("0.00");

				if(donetpayamt.getValue()!=null&&donetpayamt.getValue()>0){
					double netpay=donetpayamt.getValue();
					Console.log("amount change called.amount="+doAmount.getValue()+" net pay="+netpay);
					double percent=doAmount.getValue()/netpay*100;
					Console.log("percent="+percent);
					dopercent.setValue(percent);
					Console.log("percent set");
				}
				else{
					showDialogMessage("Add products first");
				}
			}
			
		}
		if(event.getSource().equals(dopercent))
		{
			if(dopercent.getValue()!=null){
				Console.log("In dopercent not null");
//				DecimalFormat df = new DecimalFormat("0.00");

				if(donetpayamt.getValue()!=null&&donetpayamt.getValue()>0){
					double netpay=donetpayamt.getValue();
					Console.log("amount change called.amount="+doAmount.getValue()+" net pay="+netpay);
					double amount=dopercent.getValue()/100*netpay;
					doAmount.setValue(amount);
				}
				else{
					showDialogMessage("Add products first");
				}
			}
			
		}
		
		if(event.getSource().equals(olbProductGroup)){
			retrieveServiceProduct();
		}
		
		
		if(event.getSource().equals(olbContractCategory))
		{
			if(olbContractCategory.getSelectedIndex()!=0){
				ConfigCategory cat=olbContractCategory.getSelectedItem();
				if(cat!=null){
					/*** Date 27-07-2019 by Vijay for NBHC CCPM Renewal Contract Type Set by Default***/
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableRenewalContractTypeDefault")){
						System.out.println("COntract ==========");
						if(contractRenewFlag==false){
							System.out.println("COntract ========== 1111111111");
							AppUtility.makeLiveTypeDropDown(olbContractType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
						}
					}
					else{
						AppUtility.makeLiveTypeDropDown(olbContractType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
					}
				}
			}
		}
		
		/**
	     * Date 01-09-2017 added by vijay for final total amount and net payable after discount
	     */
		
		if(event.getSource().equals(dbDiscountAmt)){
			System.out.println("on change amt ==");

			if(this.getDbDiscountAmt().getValue()==null){
				this.getDbFinalTotalAmt().setValue(this.getDototalamt().getValue());
				
				this.prodTaxTable.connectToLocal();
				try {
					this.addProdTaxes();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				double totalIncludingTax=this.getDbFinalTotalAmt().getValue()+this.getProdTaxTable().calculateTotalTaxes();

				double netPay = Double.parseDouble(nf.format(totalIncludingTax));
				netPay = Math.round(netPay);
				this.getDoamtincltax().setValue(netPay);

				netPay = Math.round(netPay + this.getChargesTable().calculateNetPayable());
				this.getDbGrandTotalAmt().setValue(netPay);

				if(!tbroundOffAmt.getValue().equals("") && tbroundOffAmt.getValue()!=null){
					String roundOff = tbroundOffAmt.getValue();
					double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
					if(roundoffAmt!=0){
						this.getDonetpayamt().setValue(roundoffAmt);
//						this.getDoamtincltax().setValue(roundoffAmt);
					}else{
						this.getDonetpayamt().setValue(netPay);
//						this.getDoamtincltax().setValue(netPay);
					}
				}else{
					this.getDonetpayamt().setValue(netPay);
//					this.getDoamtincltax().setValue(netPay);

				}
			}
			else if(this.getDbDiscountAmt().getValue() > this.getDototalamt().getValue()){
				showDialogMessage("Discount Amount can not be greater than total amount");
				this.getDbDiscountAmt().setValue(0d);
			}else{
				reactOnDiscountAmtChange();
			}
			
		}
		/**
		 * ends here
		 */
		
		/**
		 * Date 01-09-2017 added by vijay for roundoff and managing net payable
		 */
		if(event.getSource().equals(tbroundOffAmt)){

			double totalIncludingTax=this.getDbFinalTotalAmt().getValue()+this.getProdTaxTable().calculateTotalTaxes();
			double netPay = Double.parseDouble(nf.format(totalIncludingTax));
			/**
			 * @author Vijay Date :- 18-01-2021 
			 * Des :- if below flag is active then round off not required for Innovative client done through process config
			 */
			if(!LoginPresenter.PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
				//this is for if there in any other charges
				netPay = Math.round(netPay + this.getChargesTable().calculateNetPayable());
			}
			
			this.getDbGrandTotalAmt().setValue(netPay);

			if(tbroundOffAmt.getValue().equals("") || tbroundOffAmt.getValue()==null){
				this.getDonetpayamt().setValue(netPay);
			}else{
				if(!tbroundOffAmt.getValue().equals("")&& tbroundOffAmt.getValue()!=null){
					String roundOff = tbroundOffAmt.getValue();
					double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
					if(roundoffAmt!=0){
						this.getDonetpayamt().setValue(roundoffAmt);
					}else{
						this.getDonetpayamt().setValue(netPay);
						this.getTbroundOffAmt().setValue("");
					}
				}
			}
		}
		/**
		 * ends here
		 */
		/**
		 * @author Vijay Chougule
		 * Des :- NBHC CCPM user adding payment terms and changing dropdown value as select so 
		 * In there reports showing payment terms as blank. System does not allow to do this to user so updated the code 
		 */
		if(event.getSource().equals(olbPaymentTerms)){
			if(paymentTermsTable.getDataprovider().getList().size()>0){
				paymentTermsTable.getDataprovider().getList().clear();
			}
			if(olbPaymentTerms.getSelectedIndex()!=0 && olbPaymentTerms.getValue(olbPaymentTerms.getSelectedIndex()).equals(AppConstants.MONTHLYPREVIOUSMONTHBILLINGPERIOD)){
				cbStartOfPeriod.setEnabled(false);
			}
			if(olbPaymentTerms.getSelectedIndex()!=0 && olbPaymentTerms.getValue(olbPaymentTerms.getSelectedIndex()).equals(AppConstants.MONTHLYPARTIALFIRSTMONTH)){
				dbPaymentDate.setEnabled(false);
				cbStartOfPeriod.setEnabled(true);
			}
			else{
				cbStartOfPeriod.setEnabled(true);
				dbPaymentDate.setEnabled(true);
			}
		}
		if(event.getSource().equals(ibdays)){
			if(olbPaymentTerms.getSelectedIndex()!=0){
				olbPaymentTerms.setSelectedIndex(0);
				if(paymentTermsTable.getDataprovider().getList().size()>0){
					paymentTermsTable.getDataprovider().getList().clear();
				}
			}
		}
		/**
		 * ends here
		 */
		


		if(event.getSource().equals(olbcNumberRange)){
			updateTaxesDetails(cust);
		}
		
		if(event.getSource().equals(olbbBranch)){
			if(olbbBranch.getSelectedIndex()!=0) {
				
				/*
				 * Ashwini Patil 
				 * Date:13-05-2024 
				 * Added for ultima search. 
				 * If number range is selected on branch then it will automatically get selected on contract screen.
				 */

				Branch branchEntity =olbbBranch.getSelectedItem();
				if(branchEntity!=null) {
					Console.log("branch entity not null");
					Console.log("branch "+branchEntity.getBusinessUnitName()+"number range="+branchEntity.getNumberRange());
					if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
						olbcNumberRange.setValue(branchEntity.getNumberRange());
						Console.log("Number range set to drop down by default");
					}else {
						Console.log("in else Number range");
						String range=LoginPresenter.branchWiseNumberRangeMap.get(branchEntity.getBusinessUnitName());
						if(range!=null&&!range.equals("")) {
							olbcNumberRange.setValue(range);
							Console.log("in else Number range set to drop down by default");							
						}else if(contractObject!=null&&contractObject.getNumberRange()!=null&&!contractObject.getNumberRange().equals("")) {
							olbcNumberRange.setValue(contractObject.getNumberRange());							
						}else {
							olbcNumberRange.setSelectedIndex(0);
						}
					}
				}else
					Console.log("branch entity null");
				updateTaxesDetails(cust);
				
			}
		}
	}
	
	
	private void retrieveServiceProduct() {
	       
		
			MyQuerry query = new MyQuerry();
			Company c=new Company();
			System.out.println("Company Id :: "+c.getCompanyId());
			
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			
			if(olbProductGroup.getSelectedIndex()!=0){
			Console.log("Inside product group query");
			filter = new Filter();
			filter.setQuerryString("productGroup");
			filter.setStringValue(olbProductGroup.getValue(olbProductGroup.getSelectedIndex()));
			filtervec.add(filter);
			}
			
			filter=new Filter();
			filter.setQuerryString("status");
			filter.setBooleanvalue(true);
			filtervec.add(filter);
			
			query.setFilters(filtervec);
			query.setQuerryObject(new ServiceProduct());
			
			async.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					olbProductName.clear();
					olbProductName.removeAllItems();
					
					olbProductName.addItem("--SELECT--");
					List<ServiceProduct> list = new ArrayList<ServiceProduct>();
					for (SuperModel model : result) { 
						ServiceProduct entity = (ServiceProduct) model;
						if(olbProductGroup.getValue().equals(entity.getProductGroup())){
						  list.add(entity);
						//	olbProductName.addItem(entity.toString());
						//	setListItems();
						//Console.log("Inside set product name");
						  
						}
					}
					olbProductName.setListItems(list);
				}
			});
		
		}

	/**
     * Date 01-09-2017 added by vijay for final total amount and net payable after discount
     */
	
	private void reactOnDiscountAmtChange() {
				showWaitSymbol();
				NumberFormat nf=NumberFormat.getFormat("0.00");
				double discountAmt = this.getDbDiscountAmt().getValue();
				double finalAmt = this.getDototalamt().getValue()-discountAmt;
			    this.getDbFinalTotalAmt().setValue(finalAmt);
			    
				try {
					this.prodTaxTable.connectToLocal();
					this.addProdTaxes();
					double totalIncludingTax=this.getDbFinalTotalAmt().getValue()+this.getProdTaxTable().calculateTotalTaxes();
					double netPay = Double.parseDouble(nf.format(totalIncludingTax));
					netPay = Math.round(netPay);
					this.getDoamtincltax().setValue(netPay);
					
					netPay = Math.round(netPay + this.getChargesTable().calculateNetPayable());
					this.getDbGrandTotalAmt().setValue(netPay);

					if(!tbroundOffAmt.getValue().equals("") && tbroundOffAmt.getValue()!=null){
						String roundOff = tbroundOffAmt.getValue();
						double roundoffAmt =  AppUtility.calculateRoundOff(roundOff, netPay);
						if(roundoffAmt!=0){
							this.getDonetpayamt().setValue(roundoffAmt);
//							this.getDoamtincltax().setValue(roundoffAmt);
						}else{
							this.getDonetpayamt().setValue(netPay);
							this.getTbroundOffAmt().setValue("");
						}
					}
					else{
						this.getDonetpayamt().setValue(netPay);
//						this.getDoamtincltax().setValue(netPay);

					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				this.hideWaitSymbol();
	}

	/**
	 * ends here
	 */
	
	/************************************Getters And Setters*******************************************/
	
	public ObjectListBox<Employee> getOlbApproverName() {
		return olbApproverName;
	}

	public void setOlbApproverName(ObjectListBox<Employee> olbApproverName) {
		this.olbApproverName = olbApproverName;
	}

	public ObjectListBox<Config> getOlbContractGroup() {
		return olbContractGroup;
	}

	public void setOlbContractGroup(ObjectListBox<Config> olbContractGroup) {
		this.olbContractGroup = olbContractGroup;
	}

	public ObjectListBox<Type> getOlbContractType() {
		return olbContractType;
	}

	public void setOlbContractType(ObjectListBox<Type> olbContractType) {
		this.olbContractType = olbContractType;
	}

	public ObjectListBox<ConfigCategory> getOlbContractCategory() {
		return olbContractCategory;
	}

	public void setOlbContractCategory(
			ObjectListBox<ConfigCategory> olbContractCategory) {
		this.olbContractCategory = olbContractCategory;
	}
	
	public ProductChargesTable getChargesTable() {
		return chargesTable;
	}

	public void setChargesTable(ProductChargesTable chargesTable) {
		this.chargesTable = chargesTable;
	}

	public ProductTaxesTable getProdTaxTable() {
		return prodTaxTable;
	}

	public void setProdTaxTable(ProductTaxesTable prodTaxTable) {
		this.prodTaxTable = prodTaxTable;
	}

	public SalesLineItemTable getSaleslineitemtable() {
		return saleslineitemtable;
	}
	public void setSaleslineitemtable(SalesLineItemTable saleslineitemtable) {
		this.saleslineitemtable = saleslineitemtable;
	}
	public ObjectListBox<Branch> getOlbbBranch() {
		return olbbBranch;
	}

	public void setOlbbBranch(ObjectListBox<Branch> olbbBranch) {
		this.olbbBranch = olbbBranch;
	}

	public TextBox getTbReferenceNumber() {
		return tbReferenceNumber;
	}

	public void setTbReferenceNumber(TextBox tbReferenceNumber) {
		this.tbReferenceNumber = tbReferenceNumber;
	}

	public ObjectListBox<Config> getOlbcPriority() {
		return olbcPriority;
	}

	public void setOlbcPriority(ObjectListBox<Config> olbcPriority) {
		this.olbcPriority = olbcPriority;
	}

	public ObjectListBox<Config> getOlbcPaymentMethods() {
		return olbcPaymentMethods;
	}

	public void setOlbcPaymentMethods(ObjectListBox<Config> olbcPaymentMethods) {
		this.olbcPaymentMethods = olbcPaymentMethods;
	}

	public PaymentTable getPaymenttable() {
		return paymenttable;
	}

	public void setPaymenttable(PaymentTable paymenttable) {
		this.paymenttable = paymenttable;
	}

	public TextBox getTbContractId() {
		return tbContractId;
	}

	public void setTbContractId(TextBox tbContractId) {
		this.tbContractId = tbContractId;
	}

	public TextBox getTbLeadId() {
		return tbLeadId;
	}

	public void setTbLeadId(TextBox tbLeadId) {
		this.tbLeadId = tbLeadId;
	}

	public TextBox getTbQuotatinId() {
		return tbQuotatinId;
	}

	public void setTbQuotatinId(TextBox tbQuotatinId) {
		this.tbQuotatinId = tbQuotatinId;
	}

	public TextBox getTbRefeRRedBy() {
		return tbRefeRRedBy;
	}

	public void setTbRefeRRedBy(TextBox tbRefeRRedBy) {
		this.tbRefeRRedBy = tbRefeRRedBy;
	}

	public TextBox getTbQuotationStatus() {
		return tbQuotationStatus;
	}

	public void setTbQuotationStatus(TextBox tbQuotationStatus) {
		this.tbQuotationStatus = tbQuotationStatus;
	}

	public TextArea getTaDescription() {
		return taDescription;
	}

	public void setTaDescription(TextArea taDescription) {
		this.taDescription = taDescription;
	}

	public UploadComposite getUcUploadTAndCs() {
		return ucUploadTAndCs;
	}

	public void setUcUploadTAndCs(UploadComposite ucUploadTAndCs) {
		this.ucUploadTAndCs = ucUploadTAndCs;
	}

	public PersonInfoComposite getPersonInfoComposite() {
		return personInfoComposite;
	}

	public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
		this.personInfoComposite = personInfoComposite;
	}

	public ObjectListBox<Employee> getOlbeSalesPerson() {
		return olbeSalesPerson;
	}

	public void setOlbeSalesPerson(ObjectListBox<Employee> olbeSalesPerson) {
		this.olbeSalesPerson = olbeSalesPerson;
	}

	public IntegerBox getIbdays() {
		return ibdays;
	}

	public void setIbdays(IntegerBox ibdays) {
		this.ibdays = ibdays;
	}

	public DoubleBox getDopercent() {
		return dopercent;
	}

	public void setDopercent(DoubleBox dopercent) {
		this.dopercent = dopercent;
	}

	public DoubleBox getDoAmount() {
		return doAmount;
	}

	public void setDoAmount(DoubleBox doAmount) {
		this.doAmount = doAmount;
	}
	public TextBox getTbcomment() {
		return tbcomment;
	}

	public void setTbcomment(TextBox tbcomment) {
		this.tbcomment = tbcomment;
	}

	public PaymentTermsTable getPaymentTermsTable() {
		return paymentTermsTable;
	}

	public void setPaymentTermsTable(PaymentTermsTable paymentTermsTable) {
		this.paymentTermsTable = paymentTermsTable;
	}

	public DoubleBox getDototalamt() {
		return dototalamt;
	}

	public void setDototalamt(DoubleBox dototalamt) {
		this.dototalamt = dototalamt;
	}

	public DoubleBox getDoamtincltax() {
		return doamtincltax;
	}

	public void setDoamtincltax(DoubleBox doamtincltax) {
		this.doamtincltax = doamtincltax;
	}

	public DoubleBox getDonetpayamt() {
		return donetpayamt;
	}

	public void setDonetpayamt(DoubleBox donetpayamt) {
		this.donetpayamt = donetpayamt;
	}

	public IntegerBox getIbCreditPeriod() {
		return ibCreditPeriod;
	}

	public void setIbCreditPeriod(IntegerBox ibCreditPeriod) {
		this.ibCreditPeriod = ibCreditPeriod;
	}

	public DateBox getDbrefernceDate() {
		return dbrefernceDate;
	}

	public void setDbrefernceDate(DateBox dbrefernceDate) {
		this.dbrefernceDate = dbrefernceDate;
	}

	public CheckBox getCbRenewFlag() {
		return cbRenewFlag;
	}

	public void setCbRenewFlag(CheckBox cbRenewFlag) {
		this.cbRenewFlag = cbRenewFlag;
	}

	public TextBox getIbOldContract() {
		return ibOldContract;
	}

	public void setIbOldContract(TextBox ibOldContract) {
		this.ibOldContract = ibOldContract;
	}

	public Button getBaddproducts() {
		return baddproducts;
	}

	public void setBaddproducts(Button baddproducts) {
		this.baddproducts = baddproducts;
	}

	public Button getAddOtherCharges() {
		return addOtherCharges;
	}

	public void setAddOtherCharges(Button addOtherCharges) {
		this.addOtherCharges = addOtherCharges;
	}

	public TextBox getTbremark() {
		return tbremark;
	}

	public void setTbremark(TextBox tbremark) {
		this.tbremark = tbremark;
	}

	public Button getF_btnservice() {
		return f_btnservice;
	}

	public void setF_btnservice(Button f_btnservice) {
		this.f_btnservice = f_btnservice;
	}

	public Date getF_conStartDate() {
		return f_conStartDate;
	}

	public void setF_conStartDate(Date f_conStartDate) {
		this.f_conStartDate = f_conStartDate;
	}

	public Date getF_conEndDate() {
		return f_conEndDate;
	}

	public void setF_conEndDate(Date f_conEndDate) {
		this.f_conEndDate = f_conEndDate;
	}

	public int getF_duration() {
		return f_duration;
	}

	public void setF_duration(int f_duration) {
		this.f_duration = f_duration;
	}

	public ServiceScheduleTable getServiceScheduleTable() {
		return serviceScheduleTable;
	}

	public void setServiceScheduleTable(ServiceScheduleTable serviceScheduleTable) {
		this.serviceScheduleTable = serviceScheduleTable;
	}

	public ServiceSchedule getScheduleEntity() {
		return scheduleEntity;
	}

	public void setScheduleEntity(ServiceSchedule scheduleEntity) {
		this.scheduleEntity = scheduleEntity;
	}

	public ServiceSchedulePopUp getServicepop() {
		return servicepop;
	}

	public void setServicepop(ServiceSchedulePopUp servicepop) {
		this.servicepop = servicepop;
	}

	public String getF_serviceDay() {
		return f_serviceDay;
	}

	public void setF_serviceDay(String f_serviceDay) {
		this.f_serviceDay = f_serviceDay;
	}

	public String getF_serviceTime() {
		return f_serviceTime;
	}

	public void setF_serviceTime(String f_serviceTime) {
		this.f_serviceTime = f_serviceTime;
	}


	public DateBox getDbContractStartDate() {
		return dbContractStartDate;
	}

	public void setDbContractStartDate(DateBox dbContractStartDate) {
		this.dbContractStartDate = dbContractStartDate;
	}

	public DateBox getDbContractEndDate() {
		return dbContractEndDate;
	}

	public void setDbContractEndDate(DateBox dbContractEndDate) {
		this.dbContractEndDate = dbContractEndDate;
	}

	public TextBox getTbdonotrenew() {
		return tbdonotrenew;
	}

	public void setTbdonotrenew(TextBox tbdonotrenew) {
		this.tbdonotrenew = tbdonotrenew;
	}

	public TextBox getTbTicketNumber() {
		return tbTicketNumber;
	}

	public void setTbTicketNumber(TextBox tbTicketNumber) {
		this.tbTicketNumber = tbTicketNumber;
	}
	
	
	public CheckBox getChkbillingatBranch() {
		return chkbillingatBranch;
	}

	public void setChkbillingatBranch(CheckBox chkbillingatBranch) {
		this.chkbillingatBranch = chkbillingatBranch;
	}

	public TextBox getTbPremisesUnderContract() {
		return tbPremisesUnderContract;
	}

	public void setTbPremisesUnderContract(TextBox tbPremisesUnderContract) {
		this.tbPremisesUnderContract = tbPremisesUnderContract;
	}
	public ObjectListBox<CompanyPayment> getObjPaymentMode() {
		return objPaymentMode;
	}

	public void setObjPaymentMode(ObjectListBox<CompanyPayment> objPaymentMode) {
		this.objPaymentMode = objPaymentMode;
	}

	public TextBox getTbPaymentName() {
		return tbPaymentName;
	}

	public void setTbPaymentName(TextBox tbPaymentName) {
		this.tbPaymentName = tbPaymentName;
	}

	public TextBox getTbPaymentBankAC() {
		return tbPaymentBankAC;
	}

	public void setTbPaymentBankAC(TextBox tbPaymentBankAC) {
		this.tbPaymentBankAC = tbPaymentBankAC;
	}

	public TextBox getTbPaymentPayableAt() {
		return tbPaymentPayableAt;
	}

	public void setTbPaymentPayableAt(TextBox tbPaymentPayableAt) {
		this.tbPaymentPayableAt = tbPaymentPayableAt;
	}

	public TextBox getTbPaymentAccountName() {
		return tbPaymentAccountName;
	}

	public void setTbPaymentAccountName(TextBox tbPaymentAccountName) {
		this.tbPaymentAccountName = tbPaymentAccountName;
	}

	//************code for billing at branch level ********************88
	private void checkcustomerbranch() {
		
		System.out.println("Inside method == ");
		System.out.println("person==="		+this.personInfoComposite.getFullNameValue());
		System.out.println("IDD=="+this.personInfoComposite.getIdValue());
		
		Company comp = new Company();
		System.out.println("CompanyId"+comp.getCompanyId());

		MyQuerry querry = new MyQuerry();
		Vector<Filter> filtervec = new Vector<Filter>();
		
		Filter filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(this.personInfoComposite.getIdValue());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(comp.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerBranchDetails());
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				System.out.println("Result==="+result.size());
				Console.log("Ashwini in checkcustomerbranch(no param) Customer Branch Result====="+result.size());
				
				List<CustomerBranchDetails> customerbranchlist = new ArrayList<CustomerBranchDetails>();
				
				if(result.size()!=0){
					for(SuperModel model:result){
					
					CustomerBranchDetails branchdetails = (CustomerBranchDetails) model;
					customerbranchlist.add(branchdetails);
					
					}
				
					List<PaymentTerms> paymentTermsTablelist = paymentTermsTable.getDataprovider().getList();

					List<PaymentTerms> paymenttermlist = new ArrayList<PaymentTerms>();
					
						for(int i=0;i<paymentTermsTablelist.size();i++){
							
							for(int j=0;j<customerbranchlist.size();j++){
							
							PaymentTerms terms = new PaymentTerms();
							terms.setPayTermPercent(paymentTermsTablelist.get(i).getPayTermPercent()/customerbranchlist.size());
							terms.setPayTermDays(paymentTermsTablelist.get(i).getPayTermDays());
							terms.setPayTermComment(paymentTermsTablelist.get(i).getPayTermComment());
							terms.setBranch(customerbranchlist.get(j).getBusinessUnitName());
							
							paymenttermlist.add(terms);
							

							}
							
						}
						
						System.out.println("===="+paymenttermlist.size());
						paymentTermsTable.getDataprovider().setList(paymenttermlist);

				}else{
					showDialogMessage("No branch found for this customer");
					chkbillingatBranch.setValue(false);
				}
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				
				Console.log("Customer Branch Result Failed ==");

			}
		});
		
	}

	public ObjectListBox<String> getObjContactList() {
		return objContactList;
	}

	public void setObjContactList(ObjectListBox<String> objContactList) {
		this.objContactList = objContactList;
	}
	public CheckBox getChkservicewithBilling() {
		return chkservicewithBilling;
	}

	public void setChkservicewithBilling(CheckBox chkservicewithBilling) {
		this.chkservicewithBilling = chkservicewithBilling;
	}
	
	public Button getbtstackDetails() {
		return btstackDetails;
	}

	public void setbtstackDetails(Button btstackDetails) {
		this.btstackDetails = btstackDetails;
	}
	

	public DoubleBox getDbDiscountAmt() {
		return dbDiscountAmt;
	}
	
	public void setDbDiscountAmt(DoubleBox dbDiscountAmt) {
		this.dbDiscountAmt = dbDiscountAmt;
	}


	public DoubleBox getDbFinalTotalAmt() {
		return dbFinalTotalAmt;
	}


	public void setDbFinalTotalAmt(DoubleBox dbFinalTotalAmt) {
		this.dbFinalTotalAmt = dbFinalTotalAmt;
	}
	public DoubleBox getDbGrandTotalAmt() {
		return dbGrandTotalAmt;
	}

	public void setDbGrandTotalAmt(DoubleBox dbGrandTotalAmt) {
		this.dbGrandTotalAmt = dbGrandTotalAmt;
	}
	public TextBox getTbroundOffAmt() {
		return tbroundOffAmt;
	}
	public void setTbroundOffAmt(TextBox tbroundOffAmt) {
		this.tbroundOffAmt = tbroundOffAmt;
	}
	
	
	
	
	public CheckBox getCbStartOfPeriod() {
		return cbStartOfPeriod;
	}

	public void setCbStartOfPeriod(CheckBox cbStartOfPeriod) {
		this.cbStartOfPeriod = cbStartOfPeriod;
	}

	public TextBox getTbCustSegment() {
		return tbCustSegment;
	}

	public void setTbCustSegment(TextBox tbCustSegment) {
		this.tbCustSegment = tbCustSegment;
	}

	public CheckBox getCbServiceWiseBilling() {
		return cbServiceWiseBilling;
	}

	public void setCbServiceWiseBilling(CheckBox cbServiceWiseBilling) {
		this.cbServiceWiseBilling = cbServiceWiseBilling;
	}
	
	
	
	public CheckBox getCbConsolidatePrice() {
		return cbConsolidatePrice;
	}

	public void setCbConsolidatePrice(CheckBox cbConsolidatePrice) {
		this.cbConsolidatePrice = cbConsolidatePrice;
	}

	/**
	 * Date :23-09-2017 BY ANIL
	 * This method checks whether we should apply discount or not
	 * if taxes applied on all products then we can give discount
	 */
	public boolean isValidForDiscount(){
		String tax1="",tax2="";
		if(saleslineitemtable.getDataprovider().getList().size()==0){
			return false;
		}
		tax1=saleslineitemtable.getDataprovider().getList().get(0).getVatTax().getTaxConfigName().trim();
		tax2=saleslineitemtable.getDataprovider().getList().get(0).getServiceTax().getTaxConfigName().trim();
		for(SalesLineItem obj:saleslineitemtable.getDataprovider().getList()){
			if(!obj.getVatTax().getTaxConfigName().trim().equals(tax1)
					||!obj.getServiceTax().getTaxConfigName().trim().equals(tax2)){
				showDialogMessage("Discount is applicable only if same taxes are applied to all products.");
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * End
	 */
	
	/**
	 *  nidhi
	 *  29-01-2018
	 *  for multiple contact list details
	 */
	
	public void getCustomerContactList(int customerCount,final String pocName){
		try {
			Console.log("Customer contact list --" +customerCount);
			MyQuerry querry=new MyQuerry();
			Company c=new Company();
			Vector<Filter> filtervec=new Vector<Filter>();
			Filter temp=null;
			
			temp=new Filter();
			temp.setQuerryString("companyId");
			temp.setLongValue(c.getCompanyId());
			filtervec.add(temp);
			
			temp=new Filter();
			temp.setQuerryString("personInfo.count");
			temp.setIntValue(customerCount);
			filtervec.add(temp);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new ContactPersonIdentification());
			
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onFailure(Throwable caught) {
					showDialogMessage("An Unexpected error occurred!");
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
//					showDialogMessage("Check customer details --" + result.s);
					
					if(glassPanel!=null){
						hideWaitSymbol();
					}
					Console.log("Customer contact list --" +result.size());
					objContactList.removeAllItems();
					objContactList.addItem("--Select--");
						for(SuperModel model:result)
						{
							ContactPersonIdentification custEntity = (ContactPersonIdentification)model;

							objContactList.addItem(custEntity.getName());
						}
						if(pocName!=null){
							objContactList.setValue(pocName);
						}
						
					}
				 });
		
			
		} catch (Exception e) {
			e.printStackTrace();
			Console.log("Customer getMessage -" +e.getMessage());
			System.out.println(e.getMessage());
		}
	}

	public TextBox getTbDonotRenewalMsg() {
		return tbDonotRenewalMsg;
	}

	public void setTbDonotRenewalMsg(TextBox tbDonotRenewalMsg) {
		this.tbDonotRenewalMsg = tbDonotRenewalMsg;
	}
	
	
	/**
	 * nidhi
	 * 31-07-2018
	 * for 
	 * @return
	 */
	public TextBox getTbCreatedBy() {
		return tbCreatedBy;
	}

	public void setTbCreatedBy(TextBox tbCreatedBy) {
		this.tbCreatedBy = tbCreatedBy;
	}

	@Override
	public void onValueChange(ValueChangeEvent<Date> event) {
		// TODO Auto-generated method stub


		if(paymentDateFlag && event.getSource().equals(dbContractDate)){
			Date newDate  = dbContractDate.getValue();
			
			CalendarUtil.addDaysToDate(newDate, 2);
			
			Date lastDate = dbContractDate.getValue();
			
			CalendarUtil.addMonthsToDate(lastDate, 1);
			CalendarUtil.setToFirstDayOfMonth(lastDate);
			CalendarUtil.addDaysToDate(lastDate, -1);
			
			if(newDate.after(lastDate)){
				newDate = lastDate;
			}
			
			dbPaymentDate.setValue(newDate);
		}
	
	}

	public void setBankSelectedDetails() {
		if(getObjPaymentMode().getSelectedIndex()!=0){
			CompanyPayment pay = (CompanyPayment) getObjPaymentMode().getSelectedItem();
			getTbPaymentName().setValue(pay.getPaymentBankName());
			getTbPaymentBankAC().setValue(pay.getPaymentAccountNo());
			getTbPaymentPayableAt().setValue(pay.getPaymentPayableAt());
			getTbPaymentAccountName().setValue(pay.getPaymentBranch());
		}else{
			getTbPaymentName().setValue("");
			getTbPaymentBankAC().setValue("");
			getTbPaymentPayableAt().setValue("");
			getTbPaymentAccountName().setValue("");
		}
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	
	
	/*@Override
	public void onValueChange(ValueChangeEvent<Date> event) {

		if(paymentDateFlag && event.getSource().equals(dbContractDate)){
			Date newDate  = dbContractDate.getValue();
			
			CalendarUtil.addDaysToDate(newDate, 2);
			
			Date lastDate = dbContractDate.getValue();
			
			CalendarUtil.addMonthsToDate(lastDate, 1);
			CalendarUtil.setToFirstDayOfMonth(lastDate);
			CalendarUtil.addDaysToDate(lastDate, -1);
			
			if(newDate.after(lastDate)){
				newDate = lastDate;
			}
			
			dbPaymentDate.setValue(newDate);
		}
	}*/
	
	
	/**
	 * 
	 * @param serSch
	 * @param branchSch
	 * @param compObj
	 * @author Anil
	 * @since 06-06-2020
	 * This methods copies asset and component details on sechduling object
	 * @return
	 */
	public static ServiceSchedule setAssetAndComponentDetails(ServiceSchedule serSch,BranchWiseScheduling branchSch,ComponentDetails compObj){
		Console.log("Inside set asset and component details..");
		serSch.setTierName(branchSch.getTierName());
		serSch.setTat(branchSch.getTat());
		serSch.setComponentName(compObj.getComponentName());
		serSch.setMfgNum(compObj.getMfgNum());
		serSch.setMfgDate(compObj.getMfgDate());
		serSch.setReplacementDate(compObj.getReplacementDate());
		serSch.setSrNo(compObj.getSrNo());
		serSch.setAssetUnit(compObj.getAssetUnit());
		return serSch;
	}
	
	public static List<ServiceSchedule> updateSchedulingListComponentWise(ServiceSchedule serSch,BranchWiseScheduling branchSch){
		Console.log("Inside component details update method...");
		List<ServiceSchedule> list=new ArrayList<ServiceSchedule>();
		Console.log("Asset qty "+branchSch.getAssetQty());
		if(branchSch.getAssetQty()>0){
			Console.log("branchSch.getComponentList()"+branchSch.getComponentList().size());

			if(branchSch.getComponentList()!=null&&branchSch.getComponentList().size()!=0){
				Console.log("Component List "+branchSch.getComponentList().size());
				int counter=0;
				for(ComponentDetails component:branchSch.getComponentList()){
					if(counter==0){
						Console.log("11");
						setAssetAndComponentDetails(serSch, branchSch, component);
						
//						ServiceSchedule obj=new ServiceSchedule();
//						obj=serSch.copyOfObject();
//						setAssetAndComponentDetails(obj, branchSch, component);
//						list.add(obj);
						
					}else{
						Console.log("22");
						ServiceSchedule obj=new ServiceSchedule();
						obj=serSch.copyOfObject();
						setAssetAndComponentDetails(obj, branchSch, component);
						list.add(obj);
					}
					counter++;
				}
			}else{
				Console.log("branchSch.getAssetQty() == "+branchSch.getAssetQty());
				for(int i=0;i<branchSch.getAssetQty()-1;i++){
					ServiceSchedule obj=new ServiceSchedule();
					obj=serSch.copyOfObject();
					list.add(obj);
				}
			}
		}else{
			return null;
		}
		Console.log("List Size : "+list.size());
		
		return list;
	}

	public void copyComponentAndTatDetails(ServiceSchedule object1,ServiceSchedule object2) {
		// TODO Auto-generated method stub
		object1.setTierName(object2.getTierName());
		object1.setTat(object2.getTat());
		object1.setComponentName(object2.getComponentName());
		object1.setMfgNum(object2.getMfgNum());
		object1.setMfgDate(object2.getMfgDate());
		object1.setReplacementDate(object2.getReplacementDate());
		
		object1.setSrNo(object2.getSrNo());
		object1.setAssetUnit(object2.getAssetUnit());
	}

	/**
	 *  25-12-2020 Added by Priyanka issue raised by Nitin Sir
	 */
	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		saleslineitemtable.getTable().redraw();
		paymenttable.getTable().redraw();
		paymentTermsTable.getTable().redraw();
		chargesTable.getTable().redraw();
		prodTaxTable.getTable().redraw();
	}
	
	/**
	 *  End
	 */
	public CheckBox getChkStationedService() {
		return chkStationedService;
	}

	public void setChkStationedService(CheckBox chkStationedService) {
		this.chkStationedService = chkStationedService;
	}
	
	protected void updateTaxesDetails(Customer cust) {
		Console.log("inside update tax method");
		
		if(glassPanel!=null){
			hideWaitSymbol();
		}
		
		System.out.println("inside update tax method"+cust);
		List<SalesLineItem> prodlist = saleslineitemtable.getDataprovider().getList();
		if(prodlist.size()>0){
			Branch branchEntity =olbbBranch.getSelectedItem();
			
			boolean gstApplicable = false;
			if(olbcNumberRange.getSelectedIndex()!=0){
				System.out.println("111");
				Config configEntity = olbcNumberRange.getSelectedItem();
				if(configEntity!=null){
					System.out.println("configEntity.isGstApplicable() "+configEntity.isGstApplicable());
					gstApplicable = configEntity.isGstApplicable();
					System.out.println("NAME =="+configEntity.getName());
					System.out.println("Type "+configEntity.getType());
				}
			}
			else{
				System.out.println("no selection number range");
				gstApplicable = true;
			}
			System.out.println("gstApplicable"+gstApplicable);
			if(cust!=null ){
				System.out.println("customer onselection");
				Console.log("update taxes method");
				 String customerBillingaddressState = cust.getAdress().getState();
				 List<SalesLineItem> productlist = AppUtility.updateTaxesDetails(prodlist, branchEntity, customerBillingaddressState,gstApplicable);
				 saleslineitemtable.getDataprovider().setList(productlist);
				 saleslineitemtable.getTable().redraw();
				RowCountChangeEvent.fire(saleslineitemtable.getTable(), saleslineitemtable.getDataprovider().getList().size(), true);
				setEnable(true);
				/**
				 * @a
				 */
				if(glassPanel!=null){
					hideWaitSymbol();
				}
			}
			
		}
	}

	public ObjectListBox<Config> getOlbcNumberRange() {
		return olbcNumberRange;
	}

	public void setOlbcNumberRange(ObjectListBox<Config> olbcNumberRange) {
		this.olbcNumberRange = olbcNumberRange;
	}
	
	
	public List<ServiceSchedule> getServiceSchedulelist(String scheduleOperation, List<SalesLineItem> salesitemlist, 
			Date contractDate, ArrayList<CustomerBranchDetails> customerbranchlist, String branch, int selectedExcludDay,
			int selectedDayIndex, int selectedServiceWeekIndex, int internalValue, ServiceSchedulePopUp serviceSchedulePopUp){
		
		Console.log("Inside Common Method new service schedule logic");
		List<SalesLineItem> salesitemlis=new ArrayList<SalesLineItem>();
		salesitemlis.addAll(salesitemlist);
		
		ArrayList<ServiceSchedule> serschelist=new ArrayList<ServiceSchedule>();
		
		List<ServiceSchedule> popuptablelist = null;
		
		boolean unitflag = false;
		boolean greterservicesthanweeks = false;
		boolean flag =false;
		boolean sameMonthserviceflag = false;
		Date temp =null;

		for(int i=0;i<salesitemlis.size();i++){

			boolean branchSchFlag=false;
			int qty=0;
			
			ArrayList<BranchWiseScheduling> branchSchedulingList = salesitemlis.get(i).getCustomerBranchSchedulingInfo().get(salesitemlis.get(i).getProductSrNo());
			
			if(branchSchedulingList!=null&&branchSchedulingList.size()!=0){
				branchSchFlag=true;
				qty=branchSchedulingList.size();
			}else{
				qty=(int) salesitemlis.get(i).getQty();
			}
			
			if(branchSchFlag==true){
				System.out.println("Branch flag true");
				Console.log("Branch flag true");
				for(int k=0;k < qty;k++){
					try {
						Console.log("1");
				 	if(branchSchedulingList.get(k).isCheck()==true){
					long noServices=(long) (salesitemlis.get(i).getNumberOfServices());
					int noOfdays=salesitemlis.get(i).getDuration();
//					int interval= (int) (noOfdays/salesitemlis.get(i).getNumberOfServices());
					double days = noOfdays;
					double noofservices = salesitemlis.get(i).getNumberOfServices();
					double dbinterval = days/noofservices;
					System.out.println("dbinterval "+dbinterval);
					int interval = getValueWithRoundUp(dbinterval);
					Console.log("interval "+interval);
					if(scheduleOperation.trim().equals(AppConstants.INTERNAL) && internalValue!=-1){
						interval = internalValue;
					}	
					
//					Date servicedate=salesitemlis.get(i).getStartDate();
					Date servicedate = new Date(salesitemlis.get(i).getStartDate().getTime());

//					if(contractDate!=null){
//						servicedate = contractDate;
//					}else{
//						servicedate = new Date();
//					}
					/**
					 * ends here
					 */
					
					Date d=new Date(servicedate.getTime());
					Date productEndDate= new Date(servicedate.getTime());
					CalendarUtil.addDaysToDate(productEndDate, noOfdays);
					Date prodenddate=new Date(productEndDate.getTime());
					productEndDate=prodenddate;
					Date tempdate=new Date();
					
					Console.log("5");
					productEndDate = calculateEndDate(servicedate, noOfdays-1);

					if(scheduleOperation.trim().equals(AppConstants.EXCLUDEDAY)){
						/**
						 * nidhi for exclude day.
						 */
						int startday = servicedate.getDay();
						
						if(selectedExcludDay !=0  && startday == selectedExcludDay-1){
							CalendarUtil.addDaysToDate(servicedate, 1);
						}
						if(selectedExcludDay !=0 ){
							CalendarUtil.addDaysToDate(productEndDate, -1);
						}
						d = new Date(servicedate.getTime());
						int lastday = productEndDate.getDay();
						
						if(selectedExcludDay !=0  && lastday == selectedExcludDay-1){
							CalendarUtil.addDaysToDate(productEndDate, -1);
						}
						/**
						 * end
						 */
					}
					if(scheduleOperation.trim().equals(AppConstants.WEEK)){
						int months = noOfdays/30;
						if(noServices>months){
							greterservicesthanweeks = true;
							showDialogMessage("Services greater than availabe weeks can not schedule services with weeks");
							break;
						}
					}
					Console.log("6");
					for(int j=0;j<noServices;j++){

						System.out.println("interval "+interval);
							if(j>0)
							{
								System.out.println("D == Date"+d);
								CalendarUtil.addDaysToDate(d, interval);
//								tempdate=new Date(d.getTime());
//								System.out.println("tempdate "+tempdate);
							}
						
						if(scheduleOperation.trim().equals(AppConstants.EXCLUDEDAY)){
							
							
							/**
							 * @author Vijay Date :- 09-02-2023
							 * Des :- added for exclude holidays and weekly off days from calnedar
							 */
							if(serviceSchedulePopUp!=null && serviceSchedulePopUp.getE_mondaytofriday().getValue(serviceSchedulePopUp.getE_mondaytofriday().getSelectedIndex())
									.equals(AppConstants.HOLIDAYS)){
								Console.log("Inside exclude holiday");
								Calendar calendar = serviceSchedulePopUp.getOlbcalendar().getSelectedItem();
								excludeDateFromInHolidayDateFromCalendar(d,calendar);
							}
							else{
								/**
								 * nidhi
								 *  ))..
								 *  for exclude day.
								 */
								int serDay = d.getDay();
								if(selectedExcludDay !=0  && serDay == selectedExcludDay-1){
									CalendarUtil.addDaysToDate(d, 1);
								}
							}
							
							
						}
						tempdate = new Date(d.getTime());
						Console.log("tempdate == " +tempdate);

						ServiceSchedule ssch=new ServiceSchedule();
						Date Stardaate=servicedate;
						Console.log("7");

						ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
						
						ssch.setScheduleStartDate(Stardaate);
						ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
						ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
						ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
						ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
						ssch.setScheduleServiceNo(j+1);
						ssch.setScheduleProdStartDate(Stardaate);
						ssch.setScheduleProdEndDate(productEndDate);
						ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
//						ssch.setScheduleServiceTime("Flexible");
						String calculatedServiceTime = "";
						if (popuptablelist!=null && popuptablelist.size() != 0) {
							calculatedServiceTime = popuptablelist.get(j).getScheduleServiceTime();
						} else {
							calculatedServiceTime = "Flexible";
						}
						ssch.setScheduleServiceTime(calculatedServiceTime);

						if(j==0){
							ssch.setScheduleServiceDate(tempdate);
						}
						if(j!=0){
							if(productEndDate.before(d) == false){
								ssch.setScheduleServiceDate(tempdate);
							}else{
								ssch.setScheduleServiceDate(productEndDate);
							}
						}
						
						/**
						 * ends here
						 */
							
						System.out.println("setScheduleServiceDate "+ssch.getScheduleServiceDate());
						ssch.setScheduleProBranch(branchSchedulingList.get(k).getBranchName());
						if (branchSchedulingList.get(k).getServicingBranch()!=null) {
							ssch.setServicingBranch(branchSchedulingList.get(k).getServicingBranch());
						} 

						int dayIndex=AppUtility.getDayIndex(branchSchedulingList.get(k).getDay());
						Console.log(" dayIndex = "+dayIndex);
						if(dayIndex!=0){
							String dayOfWeek1 = format.format(d);
							int adate = Integer.parseInt(dayOfWeek1);
							int adday1 = dayIndex - 1;
							int adday = 0;
							if (adate <= adday1) {
								adday = (adday1 - adate);
							} else {
								adday = (7 - adate + adday1);
							}
							
							Date newDate = new Date(d.getTime());
							CalendarUtil.addDaysToDate(newDate, adday);
							Date day = new Date(newDate.getTime());
							Console.log("inside dayIndex!=0 day "+day);

							if (productEndDate.before(day) == false) {
								ssch.setScheduleServiceDate(day);
							} else {
								ssch.setScheduleServiceDate(productEndDate);
							}

							CalendarUtil.addDaysToDate(d, interval);
							Date tempdate1 = new Date(d.getTime());
							d = tempdate1;
							ssch.setScheduleServiceDay(ContractForm.serviceDay(ssch.getScheduleServiceDate()));
						}
						
						Console.log("10");
						
						/**
						 * ends here
						 */
						
					
						/***  here are for Schedule popup operation ***/ 
						if(scheduleOperation.trim().equals(AppConstants.DAY) || scheduleOperation.trim().equals(AppConstants.WEEK)){
							
							Date day = null;
							int adday1 = selectedDayIndex - 1;
							
							if(scheduleOperation.trim().equals(AppConstants.DAY)){
								String dayOfWeek1 = format.format(d);
								int adate = Integer.parseInt(dayOfWeek1);
//								int adday1 = selectedDayIndex - 1;
								adday1 = selectedDayIndex - 1;
								
								int adday = 0;
								if (adate <= adday1) {
									adday = (adday1 - adate);
								} else {
									adday = (7 - adate + adday1);
								}
								Date newDate = new Date(d.getTime());
								CalendarUtil.addDaysToDate(newDate, adday);
		
//								Date day = new Date(newDate.getTime());
								day = new Date(newDate.getTime());

							}
							else{
								day = new Date(d.getTime());
//								adday1 = selectedDayIndex;
							}
							

						
							/**
							 * Date 8-03-2017
							 * added by vijay for scheduling service with week day and week
							 */
							
							if(selectedServiceWeekIndex!=0){
								
								/**
								 * Date 1 jun 2017 added by vijay for service getting same month 
								 */
								if(temp!=null){
									String currentServiceMonth =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( day ).split("-")[1];
									String lastserviceDateMonth = DateTimeFormat.getFormat( "d-M-yyyy" ).format( temp ).split("-")[1];
									if(currentServiceMonth.equals(lastserviceDateMonth)){
										sameMonthserviceflag =true;
									}else{
										sameMonthserviceflag= false;
									}
								}
								
								if(sameMonthserviceflag){
									CalendarUtil.addDaysToDate(day, 10);
								}
								/**
								 * ends here
								 */
								
								CalendarUtil.setToFirstDayOfMonth(day);

								if(selectedServiceWeekIndex==1){
								 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
								if(day.before(Stardaate)){
									if(j==0){
										day =Stardaate;
									}
									else{
										flag = true;
									}
								}
								if(flag || j>0){
									CalendarUtil.setToFirstDayOfMonth(day);
									 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
								}
							 }
							else if(selectedServiceWeekIndex==2){
									CalendarUtil.addDaysToDate(day, 7);
									 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										CalendarUtil.setToFirstDayOfMonth(day);
										CalendarUtil.addDaysToDate(day, 7);
										 day = AppUtility.getCalculatedDatewithWeek(day, adday1);;
									}
							}else if(selectedServiceWeekIndex==3){
									CalendarUtil.addDaysToDate(day, 14);
									 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										CalendarUtil.setToFirstDayOfMonth(day);
										CalendarUtil.addDaysToDate(day, 14);
										 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
									}
							}
							else if(selectedServiceWeekIndex==4){
									CalendarUtil.addDaysToDate(day, 21);

									 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										CalendarUtil.setToFirstDayOfMonth(day);
										CalendarUtil.addDaysToDate(day, 21);
										 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
									}
							 }
								 temp = day;
								 
//								 /**
//								  * Date 26 jun 2017 added by vijay 
//								  * for getting issues in 4 services and 6 services gap  with day selection
//								  */
//									
//									CalendarUtil.addDaysToDate(d, (int)interval);
//									int days2 =0;
//									String serviceStartDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( Stardaate ).split("-")[0];
//									if(serviceStartDate.equals("1")){
//										String currentServiceDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( d ).split("-")[0];
//										days2 = AppUtility.checkDateMonthOFEndAndAddDays(currentServiceDate,d);
//										CalendarUtil.addDaysToDate(d, days2);
//
//									}
//									/**
//									 * ends here
//									 */
							}
							/**
							 * ends here
							 */
							System.out.println("Day =="+day);	
							if (productEndDate.before(day) == false) {
								ssch.setScheduleServiceDate(day);
							} else {
								ssch.setScheduleServiceDate(productEndDate);
							}
							
						}
						Console.log("11");

						 /***
						  *  *:*:*
						  * nidhi
						  * 18-09-2018
						  * for map bill product
						  * if any update please update in else conditions also
						  */
						 if(LoginPresenter.billofMaterialActive){
							 
							 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(salesitemlis.get(i).getPrduct());
							 if(billPrDt!=null){
								 
								 
								 UnitConversionUtility unitConver = new UnitConversionUtility();

								 
								 if(unitConver.varifyUnitConversion(billPrDt, salesitemlis.get(i).getAreaUnit())){
									 
									 List<ServiceSchedule> sev = new ArrayList<ServiceSchedule>();
									 sev.add(ssch);
									 sev = unitConver.getServiceitemProList(sev,salesitemlis.get(i).getPrduct(),salesitemlis.get(i),billPrDt);
									 
									 if(sev!=null && sev.size()>0){
										 ssch = sev.get(0).copyOfObject();
									 }
									 
									 
								 }else{
									 unitflag = true;
								 }
								 
							 }
						 }
						 /**
						  * end
						  */
					System.out.println("last setScheduleServiceDate "+ssch.getScheduleServiceDate());

					int weaknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
					ssch.setWeekNo(weaknumber);
					 
					ssch.setScheduleServiceDay(serviceDay(ssch.getScheduleServiceDate()));

					 /**
					  * @author Vijay Chougule Date :- 09-09-2020
					  * Des :- added Service duration 
					  */
					 if(branchSchedulingList.get(k).getServiceDuration()!=0){
						 ssch.setServiceDuration(branchSchedulingList.get(k).getServiceDuration());
					 }
					 /**
					  * ends here
					  */
						Console.log("13");

						System.out.println("serschelist sizeee "+serschelist.size()); 
					serschelist.add(ssch);
					 /**
					  * @author Vijay Chougule Date :- 06-11-2020
					  * Des :- Bug - if branch as service address then its not scheduling as per compaintTat requirement
					  * so added code
					  */
					 if(ContractForm.complainTatFlag){
							Console.log(".. Complain flag..");
							List<ServiceSchedule>list=ContractForm.updateSchedulingListComponentWise(ssch, branchSchedulingList.get(k));
							if(list!=null&&list.size()!=0){
								serschelist.addAll(list);

							}
						}
					}

					/**
					 * ends here
					 */
					
				}
				 	
				 	
				} catch (Exception e) {
					Console.log("Exception "+e.getLocalizedMessage());
					Console.log("getCause "+e.getCause());
					e.printStackTrace();
				}
				
				}
				
				if(greterservicesthanweeks){
					break;
				}
				
			}else{
				Console.log("Branch flag false");

					for(int k=0;k < qty;k++){
					
					 	System.out.println("branch flag false");
					long noServices=(long) (salesitemlis.get(i).getNumberOfServices());
					int noOfdays=salesitemlis.get(i).getDuration();
//					int interval= (int) (noOfdays/salesitemlis.get(i).getNumberOfServices());
					double dbinterval= noOfdays/salesitemlis.get(i).getNumberOfServices();
					int interval = getValueWithRoundUp(dbinterval);
					
					if(scheduleOperation.trim().equals(AppConstants.INTERNAL)){
						interval = internalValue;
					}	
					Date servicedate=salesitemlis.get(i).getStartDate();
					
//					if(contractDate!=null){
//						servicedate = contractDate;
//					}else{
//						servicedate = new Date();
//					}
					/**
					 * ends here
					 */
					
					Date d=new Date(servicedate.getTime());
					Date productEndDate= new Date(servicedate.getTime());
					CalendarUtil.addDaysToDate(productEndDate, noOfdays);
					Date prodenddate=new Date(productEndDate.getTime());
					productEndDate=prodenddate;
					Date tempdate=new Date();
					
					if(scheduleOperation.trim().equals(AppConstants.EXCLUDEDAY)){
						/**
						 * nidhi for exclude day.
						 */
						productEndDate = calculateEndDate(servicedate, noOfdays-1);
						int startday = servicedate.getDay();
						
						if(selectedExcludDay !=0  && startday == selectedExcludDay-1){
							CalendarUtil.addDaysToDate(servicedate, 1);
						}
						if(selectedExcludDay !=0 ){
							CalendarUtil.addDaysToDate(productEndDate, -1);
						}
						
						int lastday = productEndDate.getDay();
						
						if(selectedExcludDay !=0  && lastday == selectedExcludDay-1){
							CalendarUtil.addDaysToDate(productEndDate, -1);
						}
						/**
						 * end
						 */
					}
					
					if(scheduleOperation.trim().equals(AppConstants.WEEK)){
						int months = noOfdays/30;
						if(noServices>months){
							greterservicesthanweeks = true;
							showDialogMessage("Services greater than availabe weeks can not schedule services with weeks");
							break;
						}
					}
					

					for(int j=0;j<noServices;j++){
						if(j>0)
						{
							CalendarUtil.addDaysToDate(d, interval);
							tempdate=new Date(d.getTime());
						}
						
						if(scheduleOperation.trim().equals(AppConstants.EXCLUDEDAY)){
							/**
							 * nidhi
							 *  ))..
							 *  for exclude day.
							 */
							int serDay = d.getDay();
							if(selectedExcludDay !=0  && serDay == selectedExcludDay-1){
								CalendarUtil.addDaysToDate(d, 1);
							}
						}
						
						ServiceSchedule ssch=new ServiceSchedule();
						Date Stardaate=servicedate;
						ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
						ssch.setScheduleStartDate(Stardaate);
						ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
						ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
						ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
						ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
						ssch.setScheduleServiceNo(j+1);
						ssch.setScheduleProdStartDate(Stardaate);
						ssch.setScheduleProdEndDate(productEndDate);
						ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
						ssch.setScheduleServiceTime("Flexible");
						
						if(j==0){
							ssch.setScheduleServiceDate(servicedate);
						}
						if(j!=0){
							if(productEndDate.before(d)==false){
								ssch.setScheduleServiceDate(tempdate);
							}else{
								ssch.setScheduleServiceDate(productEndDate);
							}
						}
						
						if(customerbranchlist!=null && customerbranchlist.size()==qty){
							ssch.setScheduleProBranch(customerbranchlist.get(k).getBusinessUnitName());
							/**
							 * rohan added this code for automatic servicing branch setting for NBHC 
							 */
							
							if(customerbranchlist!=null && customerbranchlist.get(k).getBranch()!=null)
							{
								ssch.setServicingBranch(customerbranchlist.get(k).getBranch());
							}
							else
							{
								if(branch!=null)
									ssch.setServicingBranch(branch);
								else
									ssch.setServicingBranch("Service Address");
							}
							/**
							 * ends here 
							 */
						}else if(customerbranchlist!=null && customerbranchlist.size()==0){
							ssch.setScheduleProBranch("Service Address");
							/**
							 * rohan added this code for automatic servicing branch setting for NBHC 
							 */
							if(branch!=null)
								ssch.setServicingBranch(branch);
							else
								ssch.setServicingBranch("Service Address");
							/**
							 * ends here 
							 */	
								
						}else{
							ssch.setScheduleProBranch(null);
						}
						
						ssch.setScheduleServiceDay(serviceDay(ssch.getScheduleServiceDate()));
						
						/**
						 * Date 01-04-2017 added by vijay for week number
						 */
						int weaknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
						ssch.setWeekNo(weaknumber);
						
						/**
						 * ends here
						 */
						
						/***  here are for Schedule popup operation ***/ 
						
						if(scheduleOperation.trim().equals(AppConstants.DAY) || scheduleOperation.trim().equals(AppConstants.WEEK)){
							
							Date day = null;
							int adday1 = selectedDayIndex - 1;
							
							if(scheduleOperation.trim().equals(AppConstants.DAY)){
								String dayOfWeek1 = format.format(d);
								int adate = Integer.parseInt(dayOfWeek1);
//								int adday1 = selectedDayIndex - 1;
								adday1 = selectedDayIndex - 1;
								
								int adday = 0;
								if (adate <= adday1) {
									adday = (adday1 - adate);
								} else {
									adday = (7 - adate + adday1);
								}
								Date newDate = new Date(d.getTime());
								CalendarUtil.addDaysToDate(newDate, adday);
		
//								Date day = new Date(newDate.getTime());
								day = new Date(newDate.getTime());

							}
							else{
								day = new Date(d.getTime());
//								adday1 = selectedDayIndex;
							}
							
							/**
							 * Date 8-03-2017
							 * added by vijay for scheduling service with week day and week
							 */
							
							if(selectedServiceWeekIndex!=0){
								
								/**
								 * Date 1 jun 2017 added by vijay for service getting same month 
								 */
								if(temp!=null){
									String currentServiceMonth =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( day ).split("-")[1];
									String lastserviceDateMonth = DateTimeFormat.getFormat( "d-M-yyyy" ).format( temp ).split("-")[1];
									if(currentServiceMonth.equals(lastserviceDateMonth)){
										sameMonthserviceflag =true;
									}else{
										sameMonthserviceflag= false;
									}
								}
								
								if(sameMonthserviceflag){
									CalendarUtil.addDaysToDate(day, 10);
								}
								/**
								 * ends here
								 */
								
								CalendarUtil.setToFirstDayOfMonth(day);

								if(selectedServiceWeekIndex==1){
								 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
								if(day.before(Stardaate)){
									if(j==0){
										day =Stardaate;
									}
									else{
										flag = true;
									}
								}
								if(flag || j>0){
									CalendarUtil.setToFirstDayOfMonth(day);
									 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
								}
							 }
							else if(selectedServiceWeekIndex==2){
									CalendarUtil.addDaysToDate(day, 7);
									 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										CalendarUtil.setToFirstDayOfMonth(day);
										CalendarUtil.addDaysToDate(day, 7);
										 day = AppUtility.getCalculatedDatewithWeek(day, adday1);;
									}
							}else if(selectedServiceWeekIndex==3){
									CalendarUtil.addDaysToDate(day, 14);
									 day = AppUtility.getCalculatedDatewithWeek(day, adday1);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										CalendarUtil.setToFirstDayOfMonth(day);
										CalendarUtil.addDaysToDate(day, 14);
										 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
									}
							}
							else if(selectedServiceWeekIndex==4){
									CalendarUtil.addDaysToDate(day, 21);

									 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);
									if(day.before(Stardaate)){
										if(j==0){
											day =Stardaate;
										}
										else{
											flag = true;
										}
									}
									if(flag || j>0){
										CalendarUtil.setToFirstDayOfMonth(day);
										CalendarUtil.addDaysToDate(day, 21);
										 day =  AppUtility.getCalculatedDatewithWeek(day, adday1);;
									}
							 }
								 temp = day;
								 
//								 /**
//								  * Date 26 jun 2017 added by vijay 
//								  * for getting issues in 4 services and 6 services gap  with day selection
//								  */
//									
//									CalendarUtil.addDaysToDate(d, (int)interval);
//									int days2 =0;
//									String serviceStartDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( Stardaate ).split("-")[0];
//									if(serviceStartDate.equals("1")){
//										String currentServiceDate =  DateTimeFormat.getFormat( "d-M-yyyy" ).format( d ).split("-")[0];
//										days2 = AppUtility.checkDateMonthOFEndAndAddDays(currentServiceDate,d);
//										CalendarUtil.addDaysToDate(d, days2);
//
//									}
//									/**
//									 * ends here
//									 */
							}
							/**
							 * ends here
							 */
								
							if (productEndDate.before(day) == false) {
								ssch.setScheduleServiceDate(day);
							} else {
								ssch.setScheduleServiceDate(productEndDate);
							}
							
						}
					
					
						 /***
						  *  *:*:*
						  * nidhi
						  * 18-09-2018
						  * for map bill product
						  * if any update please update in else conditions also
						  */
						 if(LoginPresenter.billofMaterialActive){
							 
							 BillOfMaterial billPrDt = AppUtility.verifyBillofMaterilProd(salesitemlis.get(i).getPrduct());
							 if(billPrDt!=null){
								 
								 
								 UnitConversionUtility unitConver = new UnitConversionUtility();
	
								 
								 if(unitConver.varifyUnitConversion(billPrDt, salesitemlis.get(i).getAreaUnit())){
									 
									 List<ServiceSchedule> sev = new ArrayList<ServiceSchedule>();
									 sev.add(ssch);
									 sev = unitConver.getServiceitemProList(sev,salesitemlis.get(i).getPrduct(),salesitemlis.get(i),billPrDt);
									 
									 if(sev!=null && sev.size()>0){
										 ssch = sev.get(0).copyOfObject();
									 }
									 
									 
								 }else{
									 /**
									  * nidhi 7-1-2018 |*|
									  */
									 unitflag = true;
									 /**
									  * end
									  */
								 }
							 }
						 }
						 /**
						  * end
						  */
						serschelist.add(ssch);
					}
					
					if(greterservicesthanweeks){
						break;
					}
					
				}
			
		}
		
		}
		Console.log("unitflag "+unitflag);
		if(unitflag){
			showDialogMessage("Unit of service area and product group area is differeant and conversion of unit is not avaliable.");
		}
		
		System.out.println("serschelist == "+serschelist.size());
		Console.log("final returning serschelist "+serschelist.size());
		return serschelist;
		
	}
	
	public int getValueWithRoundUp(double value){
		System.out.println("inside round up method value "+value);
		Console.log("inside round up method value "+value);
		String strvalue = value+"";
		try {
			
		String[] stringvalue = strvalue.split("\\.");
//		double number = Double.parseDouble(stringvalue[1]);
//		System.out.println("number inside round up method"+number);
//		Console.log("number "+number);
		if(stringvalue.length>0) {
			if(!stringvalue[1].startsWith("0")){
				int intvalue = (int) value;
				intvalue++;
				Console.log("final rounded value "+intvalue);
				System.out.println("final rounded value "+intvalue);
				return intvalue;
			}
			else{
				int intvalue = (int) value;
				Console.log("else block final rounded value "+intvalue);
				System.out.println("else block final rounded value "+intvalue);
				return intvalue;
			}
		}
		else {
			int intvalue = (int) value;
			Console.log("else block final rounded value length !>0"+intvalue);
			System.out.println("else block final rounded value "+intvalue);
			return intvalue;
		}
		
		} catch (Exception e) {
			
		}
		
		int intvalue = (int) value;
		Console.log("final rounded value length !>0"+intvalue);
		return intvalue;
		
	}
	
	private Date calculateEndDate(Date servicedt, int noOfdays) {
		Date servicedate = servicedt;
		Date d = new Date(servicedate.getTime());
		Date productEndDate = new Date(servicedate.getTime());
		CalendarUtil.addDaysToDate(productEndDate, noOfdays);
		Date prodenddate = new Date(productEndDate.getTime());
		productEndDate = prodenddate;
	
		return productEndDate;

	}
	
	
 public void excludeDateFromInHolidayDateFromCalendar(Date date, Calendar calendar) {
		
		for(Holiday holiday : calendar.getHoliday()){
			if(AppUtility.parseDate(holiday.getHolidaydate()).equals(AppUtility.parseDate(date))){
				CalendarUtil.addDaysToDate(date, 1);
			}
		}
//		if(calendar.checkWeekleyOff(date)){
//			CalendarUtil.addDaysToDate(date, 1);
//			if(calendar.checkWeekleyOff(date)){
//				CalendarUtil.addDaysToDate(date, 1);
//			}
//		}
		Console.log("date =="+date);
		for(int i=0;i<7;i++){
			if(calendar.checkWeekleyOff(date)){
				Console.log(" weekly off true");
				CalendarUtil.addDaysToDate(date, 1);
				Console.log("after weekly off date"+date);
			}
		}
	}
	
 	
}
