package com.slicktechnologies.client.views.customer;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;

import java.util.Comparator;

import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.google.gwt.cell.client.FieldUpdater;

import java.util.List;
import java.util.Date;

import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.cell.client.EditTextCell;
import  com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import  com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.dom.client.Style.Unit;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.client.views.customer.CustomerPresenter.CustomerPresenterTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;

public class CustomerPresenterTableProxy extends CustomerPresenterTable {
  TextColumn<Customer> getBranchColumn;
  TextColumn<Customer> getEmployeeColumn;
  TextColumn<Customer> getStatusColumn;
  TextColumn<Customer> getCreationDateColumn;
  TextColumn<Customer> getGroupColumn;
  TextColumn<Customer> getCategoryColumn;
  TextColumn<Customer> getCustomerLevelColumn;
  TextColumn<Customer> getCustomerPriorityColumn;
  TextColumn<Customer> getCellNumber1Column;
  TextColumn<Customer> getCompanyNameColumn;
  TextColumn<Customer> getTypeColumn;
  TextColumn<Customer> getCountColumn;
  
  /**
   * rohan added this field for NBHC for searching customer details 
   */
 
  TextColumn<Customer> getRefNumber1;
  /**
   * ends here 
   */
  
  TextColumn<Customer> getAccountManagerColumn;
  
  public Object getVarRef(String varName)
  {
  if(varName.equals("getBranchColumn"))
  return this.getBranchColumn;
  if(varName.equals("getEmployeeColumn"))
  return this.getEmployeeColumn;
  if(varName.equals("getStatusColumn"))
  return this.getStatusColumn;
  if(varName.equals("getCreationDateColumn"))
  return this.getCreationDateColumn;
  if(varName.equals("getGroupColumn"))
  return this.getGroupColumn;
  if(varName.equals("getCategoryColumn"))
  return this.getCategoryColumn;
  if(varName.equals("getCustomerLevelColumn"))
  return this.getCustomerLevelColumn;
  if(varName.equals("getCustomerPriorityColumn"))
  return this.getCustomerPriorityColumn;
  if(varName.equals("getCellNumber1Column"))
  return this.getCellNumber1Column;
  if(varName.equals("getCompanyNameColumn"))
  return this.getCompanyNameColumn;
  if(varName.equals("getTypeColumn"))
  return this.getTypeColumn;
  if(varName.equals("getCountColumn"))
  return this.getCountColumn;
   return null ;
  }
  public CustomerPresenterTableProxy()
  {
  super();
  }
  @Override public void createTable() {
  addColumngetCount();
  addColumngetCompanyName();
  addColumngetCellNumber1();
  addColumngetEmployee();
  addColumngetCreationDate();
  addColumngetGroup();
  addColumngetCategory();
  addColumngetType();
  addColumngetCustomerLevel();
  addColumngetBranch();
  addColumngetCustomerPriority();
  addColumngetCustomerRefNumber();
  addColumngetStatus();
  
  addColumngetAccountManager();
  
  }
  
  

@Override
  protected void initializekeyprovider() {
  keyProvider= new ProvidesKey<Customer>()
  {
  @Override
  public Object getKey(Customer item)
  {
  if(item==null)
  {
  return null;
  }
  else
  return item.getId();
  }
  };
  }
  @Override
  public void setEnable(boolean state)
  {
  }
  @Override
  public void applyStyle()
  {
  }
  
  
   public void addColumnSorting(){
  addSortinggetCount();
  addSortinggetCellNumber1();
  addSortinggetCompanyName();
  addSortinggetEmployee();
  addSortinggetCreationDate();
  addSortinggetGroup();
  addSortinggetCategory();
  addSortinggetType();
  addSortinggetCustomerLevel();
  addSortinggetStatus();
  addSortinggetCustomerPriority();
  addSortinggetCustomerRefNo1();
  addSortinggetBranch();
  addSortingAccountManager();
  }
  
@Override public void addFieldUpdater() {
  }
  protected void addSortinggetCount()
  {
  List<Customer> list=getDataprovider().getList();
  columnSort=new ListHandler<Customer>(list);
  columnSort.setComparator(getCountColumn, new Comparator<Customer>()
  {
  @Override
  public int compare(Customer e1,Customer e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getCount()== e2.getCount()){
  return 0;}
  if(e1.getCount()> e2.getCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  
  
  protected void addColumngetCount()
  {
  getCountColumn=new TextColumn<Customer>()
  {
  @Override
  public String getValue(Customer object)
  {
  if( object.getCount()==-1)
  return "N.A";
  else return object.getCount()+"";
  }
  };
  table.addColumn(getCountColumn,"ID");
  table.setColumnWidth(getCountColumn, 100, Unit.PX);
  getCountColumn.setSortable(true);
  }
  
  /**
   * rohan added this code sorting
   */
  
  
  protected void addSortinggetCustomerRefNo1()
  {
  List<Customer> list=getDataprovider().getList();
  columnSort=new ListHandler<Customer>(list);
  columnSort.setComparator(getCellNumber1Column, new Comparator<Customer>()
  {
  @Override
  public int compare(Customer e1,Customer e2)
  {
  if(e1!=null && e2!=null)
  {
  if(Integer.parseInt(e1.getRefrNumber1())== Integer.parseInt(e2.getRefrNumber1())){
  return 0;}
  if(Integer.parseInt(e1.getRefrNumber1())> Integer.parseInt(e2.getRefrNumber1())){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  
  /**
   * end here 
   */
  
  
  protected void addSortinggetCellNumber1()
  {
  List<Customer> list=getDataprovider().getList();
  columnSort=new ListHandler<Customer>(list);
  columnSort.setComparator(getCellNumber1Column, new Comparator<Customer>()
  {
  @Override
  public int compare(Customer e1,Customer e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getCellNumber1()== e2.getCellNumber1()){
  return 0;}
  if(e1.getCellNumber1()> e2.getCellNumber1()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  
  protected void addColumngetCellNumber1()
  {
  getCellNumber1Column=new TextColumn<Customer>()
  {
  @Override
  public String getValue(Customer object)
  {
  return object.getCellNumber1()+"";
  }
  };
  table.addColumn(getCellNumber1Column,"Cell");
  table.setColumnWidth(getCellNumber1Column, 90, Unit.PX);
  getCellNumber1Column.setSortable(true);
  }
  
  protected void addColumngetCustomerRefNumber()
  {
	  getRefNumber1=new TextColumn<Customer>()
  {
  @Override
  public String getValue(Customer object)
  {
	  if(object.getRefrNumber1()!=null ){
		  return object.getRefrNumber1();
	  }
	  else
	  {
		  return "NA";
	  }
  }
  };
  table.addColumn(getRefNumber1,"Ref NO.1");
  table.setColumnWidth(getRefNumber1, 90, Unit.PX);
  getRefNumber1.setSortable(true);
  }
  
  protected void addSortinggetCompanyName()
  {
  List<Customer> list=getDataprovider().getList();
  columnSort=new ListHandler<Customer>(list);
  columnSort.setComparator(getCompanyNameColumn, new Comparator<Customer>()
  {
  @Override
  public int compare(Customer e1,Customer e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getCompanyName()!=null && e2.getCompanyName()!=null){
  return e1.getCompanyName().compareTo(e2.getCompanyName());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  
  
  
  protected void addColumngetCompanyName()
  {
  getCompanyNameColumn=new TextColumn<Customer>()
  {
  @Override
  public String getValue(Customer object)
  {
  return object.getCompanyName()+"";
  }
  };
  table.addColumn(getCompanyNameColumn,"Name");
  table.setColumnWidth(getCompanyNameColumn, 100, Unit.PX);
  getCompanyNameColumn.setSortable(true);
  }
  
  
  
  protected void addSortinggetEmployee()
  {
  List<Customer> list=getDataprovider().getList();
  columnSort=new ListHandler<Customer>(list);
  columnSort.setComparator(getEmployeeColumn, new Comparator<Customer>()
  {
  @Override
  public int compare(Customer e1,Customer e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getEmployee()!=null && e2.getEmployee()!=null){
  return e1.getEmployee().compareTo(e2.getEmployee());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  
  
  protected void addColumngetEmployee()
  {
  getEmployeeColumn=new TextColumn<Customer>()
  {
  @Override
  public String getValue(Customer object)
  {
  return object.getEmployee()+"";
  }
  };
  table.addColumn(getEmployeeColumn,"Sales Person");
  table.setColumnWidth(getEmployeeColumn, 100, Unit.PX);
  getEmployeeColumn.setSortable(true);
  }
  
  
  
  protected void addSortinggetCreationDate()
  {
  List<Customer> list=getDataprovider().getList();
  columnSort=new ListHandler<Customer>(list);
  columnSort.setComparator(getCreationDateColumn, new Comparator<Customer>()
  {
  @Override
  public int compare(Customer e1,Customer e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getCreationDate()!=null && e2.getCreationDate()!=null){
  return e1.getCreationDate().compareTo(e2.getCreationDate());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetCreationDate()
  {
  DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
  DatePickerCell date= new DatePickerCell(fmt);
  getCreationDateColumn=new TextColumn<Customer>()
  {
  @Override
  public String getValue(Customer object)
  {
  if(object.getCreationDate()!=null){
  return AppUtility.parseDate(object.getCreationDate());}
  else{
  object.setCreationDate(new Date());
  return "N.A";
  }
  }
  };
  table.addColumn(getCreationDateColumn,"Reg. Date");
  table.setColumnWidth(getCreationDateColumn, 100, Unit.PX);
  getCreationDateColumn.setSortable(true);
  }
  
  protected void addSortinggetGroup()
  {
  List<Customer> list=getDataprovider().getList();
  columnSort=new ListHandler<Customer>(list);
  columnSort.setComparator(getGroupColumn, new Comparator<Customer>()
  {
  @Override
  public int compare(Customer e1,Customer e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getGroup()!=null && e2.getGroup()!=null){
  return e1.getGroup().compareTo(e2.getGroup());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  
  
  protected void addColumngetGroup()
  {
  getGroupColumn=new TextColumn<Customer>()
  {
  @Override
  public String getValue(Customer object)
  {
  return object.getGroup()+"";
  }
  };
  table.addColumn(getGroupColumn,"Group");
  table.setColumnWidth(getGroupColumn, 100, Unit.PX);
  getGroupColumn.setSortable(true);
  }
  
  
  protected void addSortinggetCategory()
  {
  List<Customer> list=getDataprovider().getList();
  columnSort=new ListHandler<Customer>(list);
  columnSort.setComparator(getCategoryColumn, new Comparator<Customer>()
  {
  @Override
  public int compare(Customer e1,Customer e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getCategory()!=null && e2.getCategory()!=null){
  return e1.getCategory().compareTo(e2.getCategory());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  protected void addColumngetCategory()
  {
  getCategoryColumn=new TextColumn<Customer>()
  {
  @Override
  public String getValue(Customer object)
  {
  return object.getCategory()+"";
  }
  };
  table.addColumn(getCategoryColumn,"Category");
  table.setColumnWidth(getCategoryColumn, 130, Unit.PX);
  getCategoryColumn.setSortable(true);
  }
  protected void addSortinggetType()
  {
  List<Customer> list=getDataprovider().getList();
  columnSort=new ListHandler<Customer>(list);
  columnSort.setComparator(getTypeColumn, new Comparator<Customer>()
  {
  @Override
  public int compare(Customer e1,Customer e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getType()!=null && e2.getType()!=null){
  return e1.getType().compareTo(e2.getType());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  
  
  protected void addColumngetType()
  {
  getTypeColumn=new TextColumn<Customer>()
  {
  @Override
  public String getValue(Customer object)
  {
  return object.getType()+"";
  }
  };
  table.addColumn(getTypeColumn,"Type");
  table.setColumnWidth(getTypeColumn, 110, Unit.PX);
  getTypeColumn.setSortable(true);
  }
  
  
  protected void addSortinggetCustomerLevel()
  {
  List<Customer> list=getDataprovider().getList();
  columnSort=new ListHandler<Customer>(list);
  columnSort.setComparator(getCustomerLevelColumn, new Comparator<Customer>()
  {
  @Override
  public int compare(Customer e1,Customer e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getCustomerLevel()!=null && e2.getCustomerLevel()!=null){
  return e1.getCustomerLevel().compareTo(e2.getCustomerLevel());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  
  
  protected void addColumngetCustomerLevel()
  {
  getCustomerLevelColumn=new TextColumn<Customer>()
  {
  @Override
  public String getValue(Customer object)
  {
  return object.getCustomerLevel()+"";
  }
  };
  table.addColumn(getCustomerLevelColumn,"Level");
  table.setColumnWidth(getCustomerLevelColumn, 110, Unit.PX);
  getCustomerLevelColumn.setSortable(true);
  }
  
  
  protected void addSortinggetStatus()
  {
  List<Customer> list=getDataprovider().getList();
  columnSort=new ListHandler<Customer>(list);
  columnSort.setComparator(getStatusColumn, new Comparator<Customer>()
  {
  @Override
  public int compare(Customer e1,Customer e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getStatus()!=null && e2.getStatus()!=null){
  return e1.getStatus().compareTo(e2.getStatus());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  
  
  protected void addColumngetStatus()
  {
  getStatusColumn=new TextColumn<Customer>()
  {
  @Override
  public String getValue(Customer object)
  {
  return object.getStatus()+"";
  }
  };
  table.addColumn(getStatusColumn,"Status");
  table.setColumnWidth(getStatusColumn, 100, Unit.PX);
  getStatusColumn.setSortable(true);
  }
  
  
  protected void addSortinggetCustomerPriority()
  {
  List<Customer> list=getDataprovider().getList();
  columnSort=new ListHandler<Customer>(list);
  columnSort.setComparator(getCustomerPriorityColumn, new Comparator<Customer>()
  {
  @Override
  public int compare(Customer e1,Customer e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getCustomerPriority()!=null && e2.getCustomerPriority()!=null){
  return e1.getCustomerPriority().compareTo(e2.getCustomerPriority());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  
  
  protected void addColumngetCustomerPriority()
  {
  getCustomerPriorityColumn=new TextColumn<Customer>()
  {
  @Override
  public String getValue(Customer object)
  {
  return object.getCustomerPriority()+"";
  }
  };
  table.addColumn(getCustomerPriorityColumn,"Priority");
  table.setColumnWidth(getCustomerPriorityColumn, 120, Unit.PX);
  getCustomerPriorityColumn.setSortable(true);
  }
  
  
  protected void addSortinggetBranch()
  {
  List<Customer> list=getDataprovider().getList();
  columnSort=new ListHandler<Customer>(list);
  columnSort.setComparator(getBranchColumn, new Comparator<Customer>()
  {
  @Override
  public int compare(Customer e1,Customer e2)
  {
  if(e1!=null && e2!=null)
  {
  if( e1.getBranch()!=null && e2.getBranch()!=null){
  return e1.getBranch().compareTo(e2.getBranch());}
  }
  else{
  return 0;}
  return 0;
  }
  });
  table.addColumnSortHandler(columnSort);
  }
  
  
  protected void addColumngetBranch()
  {
  getBranchColumn=new TextColumn<Customer>()
  {
  @Override
  public String getValue(Customer object)
  {
  return object.getBranch()+"";
  }
  };
  table.addColumn(getBranchColumn,"Branch");
  table.setColumnWidth(getBranchColumn, 100, Unit.PX);
  getBranchColumn.setSortable(true);
  }
  
  
  private void addColumngetAccountManager() {

	  getAccountManagerColumn = new TextColumn<Customer>() {
		
		@Override
		public String getValue(Customer object) {
			if(object.getAccountManager()!=null){
				return object.getAccountManager();
			}
			return "";
		}
	};
	  table.addColumn(getAccountManagerColumn,"Account Manager");
	  table.setColumnWidth(getAccountManagerColumn, 100, Unit.PX);
	  getAccountManagerColumn.setSortable(true);
  }
  
  
  private void addSortingAccountManager() {


	  List<Customer> list=getDataprovider().getList();
	  columnSort=new ListHandler<Customer>(list);
	  columnSort.setComparator(getAccountManagerColumn, new Comparator<Customer>()
	  {
		  @Override
		  public int compare(Customer e1,Customer e2)
		  {
		  if(e1!=null && e2!=null)
		  {
		  if( e1.getAccountManager()!=null && e2.getAccountManager()!=null){
		  return e1.getAccountManager().compareTo(e2.getAccountManager());}
		  }
		  else{
		  return 0;}
		  return 0;
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  
}
}
