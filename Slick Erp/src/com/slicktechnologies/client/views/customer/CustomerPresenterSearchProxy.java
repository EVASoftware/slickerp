package com.slicktechnologies.client.views.customer;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.customer.CustomerPresenter.CustomerPresenterSearch;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.relationalLayer.CustomerRelation;

public class CustomerPresenterSearchProxy extends CustomerPresenterSearch {
	public ObjectListBox<Branch> olbBranch;
	public static ObjectListBox<Employee> olbSalesPerson;
	public ListBox lbStatus;
	public DateComparator dateComparator;
	public ObjectListBox<ConfigCategory> olbConfigCustomerCategory;
	public ObjectListBox<Config>olbCustomerGroup;
	public ObjectListBox<Type> olbConfigCustomerType;

	public ObjectListBox<Config> olbConfigCustomerLevel;
	public ObjectListBox<Config> olbConfigCustomerPriority;

	
	/**
	 * ROhan added this for NBHC requiredment 
	 */
		TextBox tbrefnumber1;
	/**
	 * ends here 
	 */
		
	/**
	 * Date 3 march 2017 added by vijay for PCAMB requirement	
	 */
	public ObjectListBox<Config> olbCustomerReligion;

	/**
	 * edns here
	 */
	
	public PersonInfoComposite personInfo;
	private SuggestBox sbclientname;
	
	ObjectListBox<Employee> olbAccountManager;

	  
	public Object getVarRef(String varName)
	{
		if(varName.equals("olbBranch"))
			return this.olbBranch;
		if(varName.equals("olbSalesPerson"))
			return this.olbSalesPerson;
		if(varName.equals("lbStatus"))
			return this.lbStatus;
		if(varName.equals("dateComparator"))
			return this.dateComparator;
		if(varName.equals("olbConfigCustomerCategory"))
			return this.olbConfigCustomerCategory;
		if(varName.equals("olbConfigCustomerLevel"))
			return this.olbConfigCustomerLevel;
		if(varName.equals("olbConfigCustomerPriority"))
			return this.olbConfigCustomerPriority;
		if(varName.equals("olbConfigCustomerType"))
			return this.olbConfigCustomerType;
		/*if(varName.equals("personInfo"))
			return this.personInfo;*/
		
		return null ;
	}
	public CustomerPresenterSearchProxy()
	{
		super();
		createGui();
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","SalesPersonRestriction")
				&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Sales"))
		{
//			salesPersonRestrictionFlag=true;
			olbSalesPerson.setValue(LoginPresenter.loggedInUser);
			olbSalesPerson.setEnabled(false);
		
		}
		
	}
	public void initWidget()
	{
		olbBranch= new ObjectListBox<Branch>();

		/**
		 * @author Vijay Chougule Date 14-09-2020
		 * Des :- added branch restriction with process config requirement raised by Rahul Tiwari
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.CUSTOMER, AppConstants.PC_BranchLevelRestrictionForCustomer)
				&& !LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Admin") && 
				!LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator")) {
			AppUtility.makeBranchListBoxLive(olbBranch);
		}
		else {
			AppUtility.makeCustomerBranchListBoxLive(olbBranch);
		}
		

		/**Date 9-9-2019 by Amol added a process configuration "Salespersonrestriction" 
		 * raised by Rahul Tiwari for Orkin**
		 * 
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","SalesPersonRestriction")
				&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Sales"))
		{
//			salesPersonRestrictionFlag = true;
			olbSalesPerson=new ObjectListBox<Employee>();
			olbSalesPerson.makeEmployeeLive(LoginPresenter.currentModule, AppConstants.CUSTOMER, "Sales Person");
			Timer t = new Timer() {
				@Override
				public void run() {
					makeSalesPersonEnable();
				}
			};t.schedule(3000);
		}
		else{
			olbSalesPerson=new ObjectListBox<Employee>();
			olbSalesPerson.makeEmployeeLive(LoginPresenter.currentModule, AppConstants.CUSTOMER, "Sales Person");
		}
		
		
		
//		olbSalesPerson= new ObjectListBox<Employee>();
//		AppUtility.makeSalesPersonListBoxLive(olbSalesPerson);
//		olbSalesPerson.makeEmployeeLive(LoginPresenter.currentModule, AppConstants.CUSTOMER, "Sales Person");
		lbStatus= new ListBox();
		AppUtility.setStatusListBox(lbStatus,Customer.getStatusList());
		dateComparator= new DateComparator("businessProcess.creationDate",new Customer());
		
//		AppUtility.MakeLiveCategoryConfig(olbConfigCustomerCategory,Screen.CUSTOMERCATEGORY);
		olbConfigCustomerLevel= new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbConfigCustomerLevel,Screen.CUSTOMERLEVEL);
		olbConfigCustomerPriority= new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbConfigCustomerPriority,Screen.CUSTOMERPRIORITY);
		
		olbConfigCustomerCategory= new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbConfigCustomerCategory, Screen.CUSTOMERCATEGORY);
		
		olbConfigCustomerType= new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbConfigCustomerType, Screen.CUSTOMERTYPE);
//		CategoryTypeFactory.initiateOneManyFunctionality(this.olbConfigCustomerCategory,this.olbConfigCustomerType); 
//		AppUtility.makeTypeListBoxLive(olbConfigCustomerType,Screen.CUSTOMERTYPE);
		
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false,true,false);
		
		sbclientname=new SuggestBox();
		initializeClientNameOracle();
		olbCustomerGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbCustomerGroup,Screen.CUSTOMERGROUP);

		//For giving type category functionality
		
        
		personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		AppUtility.makeTypeListBoxLive(olbConfigCustomerType,Screen.CUSTOMERTYPE);
		
		tbrefnumber1 =new TextBox();
		
		olbCustomerReligion = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbCustomerReligion, Screen.RELIGION);
		
		
		olbAccountManager=new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(olbAccountManager,"Customer");
	}
	
	private void initializeClientNameOracle()
	{

		 final MultiWordSuggestOracle orcl = (MultiWordSuggestOracle) this.sbclientname.getSuggestOracle();
		 GenricServiceAsync service=GWT.create(GenricService.class);
		 service.getSearchResult(new MyQuerry(new Vector<Filter>(), new CustomerRelation()), new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				for(SuperModel supermodel:result)
				{
					CustomerRelation cust=(CustomerRelation) supermodel;
					orcl.add(cust.getFullName());
				}
					 
			}
			
			@Override
			public void onFailure(Throwable caught) {
				
			}
		});
		 
		 
	}
	public static void makeSalesPersonEnable() {
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","SalesPersonRestriction")
				&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Sales"))
		{
			olbSalesPerson.setValue(LoginPresenter.loggedInUser);
			olbSalesPerson.setEnabled(false);
		}
	}
	
	
	
	public void createScreen()
	{
		initWidget();
		FormFieldBuilder builder;
		builder = new FormFieldBuilder("Branch",olbBranch);
		FormField folbBranch= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Sales Person",olbSalesPerson);
		FormField folbSalesPerson= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Status",lbStatus);
		FormField flbStatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("From Date",dateComparator.getFromDate());
		FormField fdateComparator= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("To Date",dateComparator.getToDate());
		FormField fdateComparator1= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Group",olbCustomerGroup);
		FormField fgroup= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		builder = new FormFieldBuilder("Customer Category",olbConfigCustomerCategory);
		FormField folbConfigCustomerCategory= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Customer Level",olbConfigCustomerLevel);
		FormField folbConfigCustomerLevel= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Customer Priority",olbConfigCustomerPriority);
		FormField folbConfigCustomerPriority= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("Customer Type",olbConfigCustomerType);
		FormField folbConfigCustomerType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Client name",sbclientname);
		FormField fsbclientname= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Reference No. 1",tbrefnumber1);
		FormField ftbrefnumber1= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= builder.setMandatory(false).setRowSpan(1).setColSpan(5).build();
		
		builder = new FormFieldBuilder("Customer Religion",olbCustomerReligion);
	    FormField folbCustomerReligion= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
	    builder = new FormFieldBuilder("Account Manager",olbAccountManager);
	    FormField folbAccountManager= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	    
	      
		this.fields=new FormField[][]{
				{fpersonInfo},/**Added by Priyanka : As per Nitin Sirs Instruction**/
				{fdateComparator,fdateComparator1,folbBranch,flbStatus,folbConfigCustomerCategory,},
				{folbConfigCustomerType,folbConfigCustomerPriority,folbConfigCustomerLevel,folbSalesPerson,fgroup},
				{ftbrefnumber1,folbCustomerReligion,folbAccountManager},
				{fpersonInfo},
				//{fsbclientname}
		};
	}
	public MyQuerry getQuerry()
	{
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		if(dateComparator.getValue()!=null)
		{
			filtervec.addAll(dateComparator.getValue());
		}
		if(lbStatus.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(lbStatus.getItemText(lbStatus.getSelectedIndex()).trim());
			temp.setQuerryString("businessProcess.status");
			filtervec.add(temp);
		}
		if(olbConfigCustomerType.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbConfigCustomerType.getValue().trim());
			temp.setQuerryString("businessProcess.type");
			filtervec.add(temp);
		}

		if(olbCustomerGroup.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbCustomerGroup.getValue().trim());
			temp.setQuerryString("businessProcess.group");
			filtervec.add(temp);
		}
		
		

		if(olbConfigCustomerCategory.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbConfigCustomerCategory.getValue().trim());
			temp.setQuerryString("businessProcess.category");
			filtervec.add(temp);
		}
		if(olbSalesPerson.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbSalesPerson.getValue().trim());
			temp.setQuerryString("businessProcess.employee");
			filtervec.add(temp);
		}
		
		if(tbrefnumber1.getValue().trim()!=null && !tbrefnumber1.getValue().trim().equals("")){
			temp=new Filter();
			temp.setStringValue(tbrefnumber1.getValue().trim());
			temp.setQuerryString("refrNumber1");
			filtervec.add(temp);
		}
		
			
		if(LoginPresenter.branchRestrictionFlag==true && olbBranch.getSelectedIndex()==0)
		{
			temp=new Filter();
			temp.setList(AppUtility.getbranchlist());
			temp.setQuerryString("businessProcess.branch IN");
			filtervec.add(temp);
		}
		else{
			if(olbBranch.getSelectedIndex()!=0){
				temp=new Filter();temp.setStringValue(olbBranch.getValue().trim());
				temp.setQuerryString("businessProcess.branch");
				filtervec.add(temp);
			}
		}
		
		if(olbConfigCustomerLevel.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbConfigCustomerLevel.getValue().trim());
			temp.setQuerryString("customerLevel");
			filtervec.add(temp);
		}
		if(olbConfigCustomerPriority.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbConfigCustomerPriority.getValue().trim());
			temp.setQuerryString("customerPriority");
			filtervec.add(temp);
		}

		{


			if(personInfo.getIdValue()!=-1)
			{  
				temp=new Filter();
				temp.setIntValue(personInfo.getIdValue());
				temp.setQuerryString("count");
				filtervec.add(temp);
			}

			if(!(personInfo.getFullNameValue().equals("")))
			{
				temp=new Filter();
				temp.setStringValue(personInfo.getFullNameValue());
				temp.setQuerryString("companyName");
				filtervec.add(temp);
			}
			
			
			 if(personInfo.getCellValue()!=-1l)
			  {
			  temp=new Filter();
			  temp.setLongValue(personInfo.getCellValue());
			  temp.setQuerryString("contacts.cellNo1");
			  filtervec.add(temp);
			  }
			/*if(sbclientname.getValueBox().getText().trim()!=null)
			{
				temp=new Filter();
				temp.setStringValue(sbclientname.getValueBox().getText().trim());
				temp.setQuerryString("companyName");
				filtervec.add(temp);
			}*/
			 
			 
			 if(olbCustomerReligion.getSelectedIndex()!=0){
				temp=new Filter();
				temp.setStringValue(olbCustomerReligion.getValue().trim());
				temp.setQuerryString("religion");
				filtervec.add(temp);
			 }
		}
		
		if(olbAccountManager.getSelectedIndex()!=0){
			temp=new Filter();temp.setStringValue(olbAccountManager.getValue());
			temp.setQuerryString("accountManager");
			filtervec.add(temp);
		}
		
		MyQuerry querry= new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Customer());
		return querry;
	}
	@Override
	public boolean validate() {
		
//		/**
//		 * @author Vijay Chougule Date 14-09-2020
//		 * Des :- added branch restriction with process config requirement raised by Rahul Tiwari
//		 */
//		if(!LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Admin") && 
//				!LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator") &&
//				AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.CUSTOMER, AppConstants.PC_BranchLevelRestrictionForCustomer)){
//			if(olbBranch.getSelectedIndex()==0)
//			{
//				showDialogMessage("Select Branch");
//				return false;
//			}
//		}
//		/**
//		 * ends here
//		 */
		
	
		//Ashwini Patil Date:25-01-2024 	
		boolean filterAppliedFlag=false;
		Console.log("filterAppliedFlag before="+filterAppliedFlag);				
		
		if(dateComparator.getFromDate().getValue()!=null || dateComparator.getToDate().getValue()!=null||
				olbBranch.getSelectedIndex()!=0 ||lbStatus.getSelectedIndex()!=0 ||olbConfigCustomerLevel.getSelectedIndex()!=0 ||olbAccountManager.getSelectedIndex()!=0 ||
				olbSalesPerson.getSelectedIndex()!=0 || personInfo.getIdValue()!=-1	|| !(personInfo.getFullNameValue().equals("")) || personInfo.getCellValue()!=-1l ||
			    olbConfigCustomerPriority.getSelectedIndex()!=0 ||
			    olbConfigCustomerCategory.getSelectedIndex()!=0 ||olbCustomerGroup.getSelectedIndex()!=0 || olbCustomerReligion.getSelectedIndex()!=0 ||
			    olbConfigCustomerType.getSelectedIndex()!=0 || (tbrefnumber1.getValue()!=null && !tbrefnumber1.getValue().equals(""))||
			    (sbclientname.getValue()!=null && !sbclientname.getValue().equals("")) ){
			
							filterAppliedFlag=true;
		}
		Console.log("filterAppliedFlag after="+filterAppliedFlag);				
					
		if(!filterAppliedFlag){
				showDialogMessage("Please apply at least one filter!");
				return false;
		}
		
		if((dateComparator.getFromDate().getValue()!=null && dateComparator.getToDate().getValue()!=null)){
			int diffdays = AppUtility.getDifferenceDays(dateComparator.getFromDate().getValue(), dateComparator.getToDate().getValue());
			Console.log("diffdays "+diffdays);
			if(diffdays>31){
					showDialogMessage("Please select from date and to date range within 30 days");
					return false;
			}
	}
		
		return super.validate();
	}
	
	
}
