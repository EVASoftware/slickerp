package com.slicktechnologies.client.views.customer;


import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.*;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.*;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.*;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.*;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.*;
import com.slicktechnologies.client.config.CategoryTypeFactory;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.*;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.composites.articletypecomposite.ArticleTypeComposite;
import com.slicktechnologies.client.views.customer.CustomerPresenter.CustomerPresenterSearch;
import com.slicktechnologies.client.views.customerlist.CustomerListGridPresenter;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.*;
import com.slicktechnologies.shared.common.businessunitlayer.*;
import com.slicktechnologies.shared.common.helperlayer.*;
import com.slicktechnologies.shared.common.personlayer.*;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;



public class CustomerForm extends FormScreen<Customer> implements ClickHandler, ChangeHandler, KeyUpHandler {

	//Token to add the varialble declarations
	DateBox dbDateOfBirth;
	ObjectListBox<Branch> olbbBranch;
	EmailTextBox etbEmail;

	ObjectListBox<Config> olbcCustomerLevel,olbCustomerGroup,olbCustomerPriBox;
	ObjectListBox<Type> olbCustomerType;
	ObjectListBox<ConfigCategory> olbCustomerCategory;
	TextArea taDescription;
	AddressComposite billingAddressComposite,serviceAddressComposite;
	SocialInfoComposites socialInfoComposite;
	TaxInformationComposite taxInfoComposite;
	public TextBox tbClientName,tbCustomerStatus;
	
//	TextBox tbFirstName,tbMiddleName,tbLastName;
	TextBox tbFullName;

	TextBox tbCustomerId;
	PhoneNumberBox pnbLandlineNo,pnbCellNo1,pnbCellNo2,pnbFaxNo;
	ObjectListBox<Employee> olbeSalesPerson;
	Button btnserviceaddr,btnbillingaddr;
	public CheckBox isCompany;
	TextBox tbrefnumber1,tbrefnumber2,tbrefnumber3,tbrefnumber4;
	TextBox tbcustwebsite;
	ArticleTypeComposite tbArticleTypeInfoComposite;
	
	
	TextBox customerCorrespondence,custPrintableName;
	
	FormField fgroupingCustomerInformation;
	
	Customer customerObject;
	boolean customerEditFlag=false;
	
	/**
	 *  Rohan added this salutation code for NBHC on 7/2/2017
	 */

	ObjectListBox<String> olbSalutation;
	/**
	 * ends here  
	 */
	
	/**
	 * Date 3 march 2017 added by vijay for PCAMB requirement customer religion	
	 */
		ObjectListBox<Config> olbCustomerReligion;
	/**
	 * ends here
	 */
		
	/**
	 * Date : 15-05-2017 By ANIL
	 * Adding approver name list box for approval process required for NBHC CCPM	
	 */
	
	ObjectListBox<Employee> olbApproverName;
	TextBox tbRemark;
		
	/**
	 * End
	 */	
	final static GenricServiceAsync async=GWT.create(GenricService.class);
	

	/**
	 * nidhi
	 * 25-09-2018
	 */
	DoubleBox quantity;
	 ObjectListBox<Config> olbUOM;
	 /**
	  * end
	  */
	/** date 6.10.2018 added by komal to upload LOI**/
	UploadComposite uploadLOI;
	
	boolean disablecellNovalidation=false;

	 /**
	   * Updated By: Viraj
	   * Date: 23-03-2019
	   * Description: To add franchise Information
	   */
	  TextBox franchiseUrl,masterFranchiseUrl;
	  ListBox lbFranchiseType;
	  Company comp;
	  Long compId;
	  /**Date 7-9-2019 added by Amol for hide customer group field**/
	  boolean hideCustomerGroup=false;
	  /** Ends **/
	  boolean salesPersonRestrictionFlag= false;
	  
	  CheckBox cbDND,cbDisableCustPortal;
	  
	  boolean cellNoNonMandatory=false; //Ashwini Patil Date: 12-05-2022
	  TextBox tbServiceAddressName;//Ashwini Patil Date: 1-07-2022 Every client had different logic for service address name so made this Textbox where they can put exact name which they want to print on contract and invoice print.
	  
	  
	  static boolean customerFlag = false;
	  
	  /**
	   * @author Vijay Date - 20-06-2023
	   * Des :- added for orion
	   */
	  ObjectListBox<Employee> olbAccountManager;
	  
	  ObjectListBox<String> olbeInvoiceSupplyType;//Ashwini Patil Date:09-08-2023
	  
	  TextBox tbUpdatedBy, tbUpdatedOn, tbCreatedBy, tbCreatedOn;
	  
	  TextBox tbZohoCustomerId;
	  
	  TextBox tbDocumentLink;
	  TextBox tbReportLink,tbReport2Link;

	public  CustomerForm() {
		super();
		createGui();
		this.tbCustomerId.setEnabled(false);
		this.tbCustomerStatus.setEnabled(false);
		this.tbCustomerStatus.setText(Customer.CUSTOMERCREATED);
//		/**
//		 * Date : 04-08-2017 By ANIL
//		 */
//		isCompany.setValue(false);
//		tbClientName.setEnabled(false);
//		/**
//		 * End
//		 */
		
		/**
		 * Date : 25-12-2020 By Priyanka issue raised by Rahul
		 */
		isCompany.setValue(true);
		isCompany.setTitle("Select if commercial customer");
		
		tbClientName.setEnabled(true);
		tbClientName.setTitle("Name of the company if commercial or searchable name ");
		/**
		 * End
		 */
		/** Date 20-03-2018 By Vijay below value change handler commented and on click handler added for iscompany in presenter **/
//		isCompany.addValueChangeHandler(this);
		/** Date 20-03-2018 By vijay for cell number duplication warning Information **/
		pnbCellNo1.addChangeHandler(this);
		
		tbArticleTypeInfoComposite.setForm(this);
		
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","SalesPersonRestriction")
				&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Sales"))
		{
			salesPersonRestrictionFlag=true;
			olbeSalesPerson.setValue(LoginPresenter.loggedInUser);
			olbeSalesPerson.setEnabled(false);
		
		}
		
		customerFlag = false;
		

	}
	
	/**
	 * @author Anil @since 18-08-2021
	 * constructor for popup customer
	 */
	public  CustomerForm(boolean isPopup) {
		super(isPopup);
		createGui();
		this.tbCustomerId.setEnabled(false);
		this.tbCustomerStatus.setEnabled(false);
		this.tbCustomerStatus.setText(Customer.CUSTOMERCREATED);
//		/**
//		 * Date : 04-08-2017 By ANIL
//		 */
//		isCompany.setValue(false);
//		tbClientName.setEnabled(false);
//		/**
//		 * End
//		 */
		
		/**
		 * Date : 25-12-2020 By Priyanka issue raised by Rahul
		 */
		isCompany.setValue(true);
		tbClientName.setEnabled(true);
		/**
		 * End
		 */
		/** Date 20-03-2018 By Vijay below value change handler commented and on click handler added for iscompany in presenter **/
//		isCompany.addValueChangeHandler(this);
		/** Date 20-03-2018 By vijay for cell number duplication warning Information **/
		pnbCellNo1.addChangeHandler(this);
		tbArticleTypeInfoComposite.setForm(this);
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","SalesPersonRestriction")
				&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Sales")){
			salesPersonRestrictionFlag=true;
			olbeSalesPerson.setValue(LoginPresenter.loggedInUser);
			olbeSalesPerson.setEnabled(false);
		}
	}

	public CustomerForm  (String[] processlevel, FormField[][] fields,FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
	}


	/**
	 * Method template to initialize the declared variables.
	 */
	private void initalizeWidget()
	{
		dbDateOfBirth=new DateBoxWithYearSelector();
//		dbDateOfBirth.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd/MM/YYYY")));
//		AppUtility.returnFormattedDate(dbDateOfBirth);
		olbbBranch=new ObjectListBox<Branch>();  
		olbbBranch.setTitle("Company branch");
		AppUtility.makeCustomerBranchListBoxLive(olbbBranch);
		/**
		 * End here..
		 */
		etbEmail=new EmailTextBox();
		etbEmail.addChangeHandler(this);
		olbcCustomerLevel=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbcCustomerLevel,Screen.CUSTOMERLEVEL);
		olbCustomerCategory= new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbCustomerCategory, Screen.CUSTOMERCATEGORY);
		/*******************************************Cateogry*******************************/
		
		olbCustomerCategory.addChangeHandler(this);
		
		/********************************************************************************/
		
		olbCustomerType=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbCustomerType, Screen.CUSTOMERTYPE);
//		setOneToManyCombo();
		olbCustomerGroup=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbCustomerGroup, Screen.CUSTOMERGROUP);
		olbCustomerPriBox=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbCustomerPriBox, Screen.CUSTOMERPRIORITY);
		billingAddressComposite=new AddressComposite();
		serviceAddressComposite=new AddressComposite();
		socialInfoComposite=new SocialInfoComposites();
		taxInfoComposite=new TaxInformationComposite();
		tbCustomerId=new TextBox();
		tbClientName=new TextBox();
		
		
//		tbFirstName=new TextBox();
//		tbMiddleName=new TextBox();
//		tbLastName=new TextBox();
		tbFullName = new TextBox();
		tbServiceAddressName=new TextBox();
		
		tbCustomerStatus=new TextBox();
		pnbLandlineNo=new PhoneNumberBox();
		pnbCellNo1=new PhoneNumberBox();
		pnbCellNo2=new PhoneNumberBox();
		pnbFaxNo=new PhoneNumberBox();
		/**Date 9-9-2019 by Amol added a process configuration "Salespersonrestriction" 
		 * raised by Rahul Tiwari for Orkin**
		 * 
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","SalesPersonRestriction")
				&&UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Sales"))
		{
			salesPersonRestrictionFlag = true;
			olbeSalesPerson=new ObjectListBox<Employee>();
			olbeSalesPerson.makeEmployeeLive(LoginPresenter.currentModule, AppConstants.CUSTOMER, "Sales Person");
			Timer t = new Timer() {
				@Override
				public void run() {
					makeSalesPersonEnable();
				}
			};t.schedule(3000);
		}
		else{
			olbeSalesPerson=new ObjectListBox<Employee>();
			olbeSalesPerson.makeEmployeeLive(LoginPresenter.currentModule, AppConstants.CUSTOMER, "Sales Person");
		}
		olbeSalesPerson.setTitle("Salesperson who will manage this customer");
//		AppUtility.makeSalesPersonListBoxLive(olbeSalesPerson);
		btnserviceaddr=new Button("Copy Service Address");
		btnbillingaddr=new Button("Copy Billing Address");
		btnserviceaddr.addClickHandler(this);
		btnbillingaddr.addClickHandler(this);
		taDescription=new TextArea();
		tbArticleTypeInfoComposite = new ArticleTypeComposite();
		
		isCompany=new CheckBox();
//		/**
//		 * Date : 04-08-2017 By ANIL
//		 */
//		isCompany.setValue(false);
//		tbClientName.setEnabled(false);
//		/**
//		 * End
//		 */
		
		/**
		 * Date : 25-12-2020 By Priyanka issue raised by Rahul
		 */
		isCompany.setValue(true);
		tbClientName.setEnabled(true);
		/**
		 * End
		 */
		
		
		tbrefnumber1=new TextBox();
		tbrefnumber1.setTitle("old system customer name or id or any specific information of customer ");
		
		/**
		 * Date : 24-10-2017 BY ANIL
		 * adding the key press event on text box ref no.1
		 * this is ONLY for NBHC
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "OnlyForNBHC")){
			tbrefnumber1.addKeyUpHandler(this);
		 }
		
		/**
		 * end
		 */
		tbrefnumber2=new TextBox();
		tbrefnumber2.setTitle("old system customer name or id or any specific information of customer ");
		tbcustwebsite=new TextBox();
		
		//************rohan added this field
		customerCorrespondence = new TextBox();
		
		custPrintableName = new TextBox();
		custPrintableName.setTitle("Billing name i.e. Biller name on the invoice . In some cases your billing name is different than searchable name i.e. service HDFC Mumbai, HDFC Pune but invoice should be on HDFC Bank. In this case HDFC Mumbai goes in company name & HDFC Bank goes in correspondence name");
		
		olbSalutation = new ObjectListBox<String>();
		AppUtility.MakeLiveConfig(olbSalutation, Screen.SALUTATION);
		
		olbCustomerReligion = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbCustomerReligion, Screen.RELIGION);

		/**
		 * Date : 15-05-2017 By ANil
		 */
		olbApproverName=new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(olbApproverName,"Customer");
		
		tbRemark=new TextBox();
		tbRemark.setEnabled(false);
		/**
		 * ENd
		 */
		/**
		 * nidhi
		 * 25-09-2018
		 */
		quantity=new DoubleBox();
		quantity.setEnabled(true);
		
		olbUOM=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbUOM, Screen.SERVICEUOM);
		/**
		 * end
		 */
		/** date 6.10.2018 added by komal to upload LOI**/
		uploadLOI = new UploadComposite();
		
		/**
		 * Updated By: Viraj 
		 * Date: 24-03-2019
		 * Description: To add franchise Information   
		 */
		compId = UserConfiguration.getCompanyId();
		lbFranchiseType = new ListBox();
		lbFranchiseType.addChangeHandler(this);
		franchiseUrl = new TextBox();
	    masterFranchiseUrl = new TextBox();
		   
		MyQuerry querry = new MyQuerry();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Company());
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>(){

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				for(SuperModel model:result) {
					comp = (Company) model;
					loadFranchiseDropdown();
				}
				
			}
		});
	    franchiseUrl.setEnabled(false);
	    masterFranchiseUrl.setEnabled(false);
		 /** Ends **/

	    cbDND = new CheckBox();
	    cbDisableCustPortal=new CheckBox();
	    tbrefnumber3=new TextBox();
	    tbrefnumber4=new TextBox();
	    
	    
	    olbAccountManager=new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(olbAccountManager,"Customer");
		
		
		olbeInvoiceSupplyType = new ObjectListBox<String>();
		olbeInvoiceSupplyType.insertItem("B2B", 0);
		olbeInvoiceSupplyType.insertItem("SEZWP", 1);
		olbeInvoiceSupplyType.insertItem("SEZWOP", 2);
		olbeInvoiceSupplyType.insertItem("EXPWP", 3);
		olbeInvoiceSupplyType.insertItem("EXPWOP", 4);
		olbeInvoiceSupplyType.insertItem("DEXP", 5);
		
		olbeInvoiceSupplyType.setTitle("SEZWP for SEZ supplies with payment of tax.\r\nSEZWOP for SEZ supplies without payment of tax.\r\nEXPWP for export supplies with payment of tax.\r\nEXPWOP for export supplies without payment of tax.");
	
		tbUpdatedBy= new TextBox();
		tbUpdatedBy.setEnabled(false);
		tbUpdatedOn= new TextBox();
		tbUpdatedOn.setEnabled(false);
		tbCreatedBy = new TextBox();
		tbCreatedBy.setEnabled(false);
		tbCreatedOn= new TextBox();
		tbCreatedOn.setEnabled(false);
		
		tbZohoCustomerId=new TextBox();
		
		tbDocumentLink=new TextBox();
		tbReportLink=new TextBox();
		tbReport2Link=new TextBox();
		
	}
	
	
	
	private void makeSalesPersonEnable() {
		
		olbeSalesPerson.setValue(LoginPresenter.loggedInUser);
		olbeSalesPerson.setEnabled(false);
	}
	
	
	private void loadFranchiseDropdown() {
		   Console.log("company :" +comp.getFranchiseType());
		   /*** Date 11-06-2019 by Vijay for getting issue unable to Save customer from NBHC ***/
		   lbFranchiseType.insertItem("--Select--", 0);

		   if(comp.getFranchiseType().equalsIgnoreCase("BRAND")){
//			   lbFranchiseType = new ListBox();
			   lbFranchiseType.insertItem("--Select--", 0);
			   lbFranchiseType.insertItem("MASTER FRANCHISE", 1);
			   lbFranchiseType.insertItem("FRANCHISE", 2);
		   }
		   
		   if(comp.getFranchiseType().equalsIgnoreCase("MASTER FRANCHISE")) {
//			   lbFranchiseType = new ListBox();
			   lbFranchiseType.insertItem("--Select--", 0);
			   lbFranchiseType.insertItem("FRANCHISE", 1);
		   }
		   
		   if(comp.getFranchiseType().equalsIgnoreCase("FRANCHISE")) {
//			   lbFranchiseType = new ListBox();
			   lbFranchiseType.insertItem("--Select--", 0);
		   }
		
	}
	/**
	 * method template to create screen formfields
	 */
	@Override
	public void createScreen() {


		initalizeWidget();
		//Token to initialize the processlevel menus.
		
		/**
		 * Date : 15-05-2017 By ANIl
		 */
		/*
		 *  07-07-2017
		 *   BY	nidhi
		 *   Quick Contract LABEL ADDED FOR QUICK CONTRACT PROCESS
		 */
		/*
		 * 20-07-2017
		 * by sagar sore
		 * Quick contract label renamed to Quick order
		 * */
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","CustomerApprovalProcess")){
//			if(CustomerPresenter.isButtonStatus()==true)
//			{
//				this.processlevelBarNames=new String[]{"Create Lead","Create Quotation","Create Order","View Services","New","View Contact List",AppConstants.CUSTOMERVIEWBRANCHES,AppConstants.MATERIALREQUIREDREPORT,"Reference Letter","Change Customer Name","Request For Approval","Cancel Approval Request","Quick Order"};
//			}
//			/**
//			 * Date 12-09-2018 By Vijay
//			 * Des :- NBHC CCPM  Quick Order Button should be disable
//			 */
//			else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","OnlyForNBHC")){
//				this.processlevelBarNames=new String[]{"Create Lead","Create Quotation","Create Order","View Services","New","View Contact List",AppConstants.CUSTOMERVIEWBRANCHES,AppConstants.MATERIALREQUIREDREPORT,"Change Customer Name","Request For Approval","Cancel Approval Request"};
//			}
//			else
//			{
//				this.processlevelBarNames=new String[]{"Create Lead","Create Quotation","Create Order","View Services","New","View Contact List",AppConstants.CUSTOMERVIEWBRANCHES,AppConstants.MATERIALREQUIREDREPORT,"Change Customer Name","Request For Approval","Cancel Approval Request","Quick Order"};
//			}
//		}else{
//			/**
//			 * Updated By: Viraj
//			 * Date: 03-06-2019
//			 * Description: added create cnc button 
//			 */
//			if(CustomerPresenter.isButtonStatus()==true)
//			{
//				this.processlevelBarNames=new String[]{"Create Lead","Create Quotation","Create Order","View Services","New","View Contact List",AppConstants.CUSTOMERVIEWBRANCHES,AppConstants.MATERIALREQUIREDREPORT,"Reference Letter","Change Customer Name","Quick Order","Create CNC"};
//			}
//			else
//			{
//				this.processlevelBarNames=new String[]{"Create Lead","Create Quotation","Create Order","View Services","New","View Contact List",AppConstants.CUSTOMERVIEWBRANCHES,AppConstants.MATERIALREQUIREDREPORT,"Change Customer Name","Quick Order","Create CNC"};
//			}
//		}
//		/**
//		 * End
//		 */
		/**
		 * Updated By: Priyanka
		 * Date: 26-12-2020
		 * Description: Change Navigation flow as per suggestion of Nitin Sir. 
		 */
		//Ashwini Patil Date:3-1-2024 adding Reset appid option only for essevaerp using companyid 
		if(UserConfiguration.getCompanyId().toString().equals("5722646637445120")){
					if(CustomerPresenter.isButtonStatus()==true)
					{
						this.processlevelBarNames=new String[]{"New",AppConstants.CUSTOMERVIEWBRANCHES,"Contact List","Create Lead","Create Quotation","Create Order","Quick Order","View Services","Create CNC",AppConstants.MATERIALREQUIREDREPORT,"Reference Letter","Update Customer Address",AppConstants.LISENCEINFORMATION,AppConstants.RESETAPPID};
					}
					else
					{
						this.processlevelBarNames=new String[]{"New",AppConstants.CUSTOMERVIEWBRANCHES,"Contact List","Create Lead","Create Quotation","Create Order","Quick Order","View Services","Create CNC",AppConstants.MATERIALREQUIREDREPORT,"Update Customer Address",AppConstants.LISENCEINFORMATION,AppConstants.RESETAPPID};
					}
		}
		else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","CustomerApprovalProcess")){
			if(CustomerPresenter.isButtonStatus()==true)
			{
				this.processlevelBarNames=new String[]{"New",AppConstants.CUSTOMERVIEWBRANCHES,"Contact List","Create Lead","Create Quotation","Create Order","Create Quick Order","View Services",AppConstants.MATERIALREQUIREDREPORT,"Reference Letter","Request For Approval","Cancel Approval Request","Update Customer Address",AppConstants.LISENCEINFORMATION};
			}
			/**
			 * Date 12-09-2018 By Vijay
			 * Des :- NBHC CCPM  Quick Order Button should be disable
			 */
			else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","OnlyForNBHC")){
				this.processlevelBarNames=new String[]{"New",AppConstants.CUSTOMERVIEWBRANCHES,"Contact List","Create Lead","Create Quotation","Create Order","View Services",AppConstants.MATERIALREQUIREDREPORT,"Request For Approval","Cancel Approval Request","Update Customer Address",AppConstants.LISENCEINFORMATION};
			}
			else
			{
				this.processlevelBarNames=new String[]{"New",AppConstants.CUSTOMERVIEWBRANCHES,"Contact List","Create Lead","Create Quotation","Create Order","Quick Order",AppConstants.MATERIALREQUIREDREPORT,"Request For Approval","Cancel Approval Request","Update Customer Address",AppConstants.LISENCEINFORMATION};
			}
		}else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","ShowUpdateCustomerIdOption")){
			/**
			 * Updated By: Viraj
			 * Date: 03-06-2019
			 * Description: added create cnc button 
			 */
			if(CustomerPresenter.isButtonStatus()==true)
			{
				this.processlevelBarNames=new String[]{"New",AppConstants.CUSTOMERVIEWBRANCHES,"Contact List","Create Lead","Create Quotation","Create Order","Quick Order","View Services","Create CNC",AppConstants.MATERIALREQUIREDREPORT,"Reference Letter","Update Customer Address",AppConstants.LISENCEINFORMATION,AppConstants.UpdateCustomerIdInAllDocs};
			}
			
			
			else
			{
				this.processlevelBarNames=new String[]{"New",AppConstants.CUSTOMERVIEWBRANCHES,"Contact List","Create Lead","Create Quotation","Create Order","Quick Order","View Services","Create CNC",AppConstants.MATERIALREQUIREDREPORT,"Update Customer Address",AppConstants.LISENCEINFORMATION,AppConstants.UpdateCustomerIdInAllDocs};
			}
		}
		else {
			/**
			 * Updated By: Viraj
			 * Date: 03-06-2019
			 * Description: added create cnc button 
			 */
			if(CustomerPresenter.isButtonStatus()==true)
			{
				this.processlevelBarNames=new String[]{"New",AppConstants.CUSTOMERVIEWBRANCHES,"Contact List","Create Lead","Create Quotation","Create Order","Quick Order","View Services","Create CNC",AppConstants.MATERIALREQUIREDREPORT,"Reference Letter","Update Customer Address",AppConstants.LISENCEINFORMATION};
			}
			
			
			else
			{
				this.processlevelBarNames=new String[]{"New",AppConstants.CUSTOMERVIEWBRANCHES,"Contact List","Create Lead","Create Quotation","Create Order","Quick Order","View Services","Create CNC",AppConstants.MATERIALREQUIREDREPORT,"Update Customer Address",AppConstants.LISENCEINFORMATION};
			}
		}
		/**
		 * End
		 */
		
		if(isPopUpAppMenubar()){
//			this.processlevelBarNames=new String[]{};
			this.processlevelBarNames=null;
			Console.log("POP up is true and process level bar is null");
		}


		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		//Token to initialize formfield
		
		String mainScreenLabel="Customer Details";
		if(customerObject!=null&&customerObject.getCount()!=0){
			mainScreenLabel=customerObject.getCount()+" "+"/"+" "+customerObject.getStatus()+" "+"/"+" "+AppUtility.parseDate(customerObject.getCreationDate());
		}
		
//		fgroupingCustomerInformation.setLabel(mainScreenLabel);
		
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		//FormField fgroupingCustomerInformation=fbuilder.setlabel("Customer Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build();
		fgroupingCustomerInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).setKeyField(true).build(); // added by priyanka.
		//fbuilder = new FormFieldBuilder("Customer ID",tbCustomerId);
		//FormField ftbCustomerId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		
		
		/**Date 21-9-2019 by Amol for orkin raised by Rahul T.**/
		FormField ftbClientName;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "ChangeCompanyNameAndFullNameLable")){
			fbuilder = new FormFieldBuilder("* Account Name",tbClientName);
			ftbClientName= fbuilder.setMandatory(true).setMandatoryMsg("Account Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Company Name",tbClientName);
			ftbClientName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		
		fbuilder = new FormFieldBuilder("D.O.B/Establishment Date",dbDateOfBirth);
		FormField fdbDateOfBirth= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("* First Name",tbFirstName);
//		FormField ftbFirstName= fbuilder.setMandatory(true).setMandatoryMsg("First Name is mandatory!").setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Middle Name",tbMiddleName);
//		FormField ftbMiddleName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("* Last Name",tbLastName);
		
		FormField ftbFullName;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","ChangeCompanyNameAndFullNameLable")){
			fbuilder = new FormFieldBuilder("* Point Of Contact",tbFullName);
			ftbFullName= fbuilder.setMandatory(true).setMandatoryMsg("Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		}else{
		fbuilder = new FormFieldBuilder("* Name",tbFullName);
		ftbFullName= fbuilder.setMandatory(true).setMandatoryMsg("Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		}
		fbuilder = new FormFieldBuilder("Service Address Name",tbServiceAddressName);
		FormField ftbServiceAddressName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		 
		
		
		FormField ftbLastName= fbuilder.setMandatory(true).setMandatoryMsg("Last Name is mandatory!").setRowSpan(0).setColSpan(0).build();
		/** date 06/03/2018 added by komal for nbhc **/
		FormField fetbEmail;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "makeEmailMandatory")){
			fbuilder = new FormFieldBuilder("* Email",etbEmail);
			fetbEmail= fbuilder.setMandatory(true).setMandatoryMsg("Email is Mandatory").setRowSpan(0).setColSpan(0).build();
			
		}else{
			fbuilder = new FormFieldBuilder("Email",etbEmail);
			fetbEmail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		/**
		 * end komal
		 */
		fbuilder = new FormFieldBuilder("Landline No",pnbLandlineNo);
		FormField fpnbLandlineNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		/**
		 * Date 13-6-2018 by jayshree 
		 * Des.As per mascom requrment some fiels are make non mandatory
		 */
		FormField fpnbCellNo1;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "OnlyForMASCOM")
				|| AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "MakeCellNumberNonMandatory")){

			fbuilder = new FormFieldBuilder("Cell No 1",pnbCellNo1);
			 fpnbCellNo1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();}
		else{
			fbuilder = new FormFieldBuilder("* Cell No 1",pnbCellNo1);
			 fpnbCellNo1= fbuilder.setMandatory(true).setMandatoryMsg("Cell No. 1 is mandatory!").setRowSpan(0).setColSpan(0).build();
		}
		fbuilder = new FormFieldBuilder("Cell No 2",pnbCellNo2);
		FormField fpnbCellNo2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Fax No",pnbFaxNo);
		FormField fpnbFaxNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder();
		//FormField fgroupingSalesInformation=fbuilder.setlabel("Sales Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		FormField fgroupingSalesInformation=fbuilder.setlabel("Classification").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).build();
		fbuilder = new FormFieldBuilder("Customer Status",tbCustomerStatus);
		FormField ftbCustomerStatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* Branch",olbbBranch);
		FormField folbbBranch= fbuilder.setMandatory(true).setMandatoryMsg("Branch is mandatory!").setRowSpan(0).setColSpan(0).build();
		/**
		 * Date 13-6-2018 by jayshree 
		 * Des.As per mascom requrment some fiels are make non mandatory
		 */
		FormField folbeSalesPerson;
		//Ashwini Patil Date:7-06-2023 for orion
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "MakeSalesPersonMandatory")) {
			fbuilder = new FormFieldBuilder("* Sales Person",olbeSalesPerson);
			folbeSalesPerson= fbuilder.setMandatory(true).setMandatoryMsg("Sales Person is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		}
		else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "OnlyForMASCOM"))
		{
			fbuilder = new FormFieldBuilder("Sales Person",olbeSalesPerson);
			 folbeSalesPerson= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		else
		{   /**@Sheetal:01-02-2022
		        Des : Making Sales person non mandatory,requirement by UDS Water**/
			fbuilder = new FormFieldBuilder("Sales Person",olbeSalesPerson);
			folbeSalesPerson= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		/**
		 * Date 13-6-2018 by jayshree 
		 * Des.As per mascom requrment some fiels are make non mandatory
		 */
		FormField folbcCustomerType;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "OnlyForMASCOM"))
		{
			fbuilder = new FormFieldBuilder(" Customer Type",olbCustomerType);
			 folbcCustomerType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		else
		{
			fbuilder = new FormFieldBuilder("* Customer Type",olbCustomerType);
			 folbcCustomerType= fbuilder.setMandatory(true).setMandatoryMsg("Customer Type is mandatory!").setRowSpan(0).setColSpan(0).build();
		}
		
		/**Date 13-8-2019 by Amol
		 * as per Ankitapesterp requirement Customer Level Mandatory
		 * 
		 */
		FormField folbcCustomerLevel;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "MakeCustomerLevelMandatory")){
		fbuilder = new FormFieldBuilder("* Customer Level",olbcCustomerLevel);
		folbcCustomerLevel= fbuilder.setMandatory(true).setMandatoryMsg("Customer Level is mandatory!").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Customer Level",olbcCustomerLevel);
			folbcCustomerLevel= fbuilder.setMandatory(false).setMandatoryMsg("Customer Level is mandatory!").setRowSpan(0).setColSpan(0).build();	
		}
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingGeneralInformation=fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingSocialInformation=fbuilder.setlabel("Social Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",socialInfoComposite);
		FormField fsocialInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder();
		FormField fgroupingBillingAddress=fbuilder.setlabel("Billing Address").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder=new FormFieldBuilder("",btnserviceaddr);
		FormField fbillingadrsbtn=fbuilder.build();
		/**
		 * Date 13-6-2018 by jayshree 
		 * Des.As per mascom requrment some fiels are make non mandatory
		 */
		FormField fbillingAddressComposite;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "OnlyForMASCOM"))
		{
			fbuilder = new FormFieldBuilder("",billingAddressComposite);
			 fbillingAddressComposite= fbuilder.setMandatory(true).setMandatoryMsg("Billing Address is Mandatory").setRowSpan(0).setColSpan(4).build();
		}
		else
		{	
			fbuilder = new FormFieldBuilder("",billingAddressComposite);
			 fbillingAddressComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			
		}
		fbuilder = new FormFieldBuilder();
		FormField fgroupingServiceAddress=fbuilder.setlabel("Service/Shipping Address").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder=new FormFieldBuilder("",btnbillingaddr);
		FormField fserviceadrsbtn=fbuilder.build();
		/**
		 * Date 13-6-2018 by jayshree 
		 * Des.As per mascom requrment some fiels are make non mandatory
		 */
		FormField fserviceAddressComposite;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "OnlyForMASCOM"))
		{
			fbuilder = new FormFieldBuilder("",serviceAddressComposite);
			 fserviceAddressComposite= fbuilder.setMandatory(true).setMandatoryMsg("Service/Shipping Address is Mandatory").setRowSpan(0).setColSpan(4).build();
		}
		else
		{
			fbuilder = new FormFieldBuilder("",serviceAddressComposite);
			 fserviceAddressComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		}
		fbuilder = new FormFieldBuilder();
		FormField fgroupingTaxInformation=fbuilder.setlabel("Article Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("",taxInfoComposite);
		FormField ftaxInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Company",isCompany);
		FormField cbIsCompany= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		/**Date 13-8-2019
		 * BY Amol
		 * As per Ankitapesterp Requirement by Rahul T.
		 * Make reference Number Mandatory
		 */
		
		FormField ftbrefnumber1 ;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "OnlyForNBHC")||AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "MakeRefNumber1Mandatory")){
			
			fbuilder = new FormFieldBuilder("* Reference No. 1",tbrefnumber1);
			 ftbrefnumber1= fbuilder.setMandatory(true).setMandatoryMsg("Reference No. 1 Mandetory").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Reference No. 1",tbrefnumber1);
			 ftbrefnumber1= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		fbuilder = new FormFieldBuilder("Reference No. 2",tbrefnumber2);
		FormField ftbrefnumber2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/**@author Sheetal : 28-05-2022, Changing label of reference no 1 and 2 also adding 2 new fields,requirement by envocare**/
		  FormField ftbBillToCustAccNo,ftbBillToCustSiteNo,ftbShipToCustAccNo,ftbShipToCustSiteNo;

			 fbuilder = new FormFieldBuilder("* Bill-to Customer Account Number",tbrefnumber1);
			 ftbBillToCustAccNo= fbuilder.setMandatory(true).setMandatoryMsg("Bill-to Customer Account Number Mandetory").setRowSpan(0).setColSpan(0).build();
		
		     fbuilder = new FormFieldBuilder("* Bill-to Customer Site Number",tbrefnumber2);
		     ftbBillToCustSiteNo= fbuilder.setMandatory(true).setMandatoryMsg("Bill-to Customer Site Number Mandetory").setRowSpan(0).setColSpan(0).build();
		
			 fbuilder = new FormFieldBuilder("* Ship-to Customer Account Number",tbrefnumber3);
			 ftbShipToCustAccNo= fbuilder.setMandatory(true).setMandatoryMsg("Ship-to Customer Account Number Mandetory").setRowSpan(0).setColSpan(0).build();
			 
			 fbuilder = new FormFieldBuilder("* Ship-to Customer Site Number",tbrefnumber4);
			 ftbShipToCustSiteNo= fbuilder.setMandatory(true).setMandatoryMsg("Ship-to Customer Site Number Mandetory").setRowSpan(0).setColSpan(0).build();
		/**end**/
       
		/** date 06/03/2018 added by komal for nbhc **/
		FormField folbgroup;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "makeGroupMandatory")){
			fbuilder = new FormFieldBuilder("* Customer Group",olbCustomerGroup);
	        folbgroup= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).setMandatoryMsg("Group is Mandatory!").build();
	        
		}else{
			fbuilder = new FormFieldBuilder("Customer Group",olbCustomerGroup);
	        folbgroup= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).setMandatoryMsg("Group is Mandatory!").build();
	        
		}
		/**
		 * end komal
		 */
     
		/**
		 * Date 13-6-2018 by jayshree 
		 * Des.As per mascom requrment some fiels are make non mandatory
		 */
		FormField folbcategory;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "OnlyForMASCOM"))
		{
			fbuilder = new FormFieldBuilder(" Customer Category",olbCustomerCategory);
			 folbcategory= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        }
		else
		{
			fbuilder = new FormFieldBuilder("* Customer Category",olbCustomerCategory);
	        folbcategory= fbuilder.setMandatory(true).setMandatoryMsg("Customer Category is Mandatory!").setRowSpan(0).setColSpan(0).build();
	       
		}
        fbuilder = new FormFieldBuilder("Customer Priority",olbCustomerPriBox);
        FormField folbpriority= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).setMandatoryMsg("Priority is mandatory!").build();

        fbuilder = new FormFieldBuilder("Website",tbcustwebsite);
        FormField ftbcustwebsite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        
        fbuilder = new FormFieldBuilder("",tbArticleTypeInfoComposite);
        FormField ftbTaxTypeInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
        fbuilder = new FormFieldBuilder();
        FormField fgroupingAttach=fbuilder.setlabel("Attachment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		FormField fgroupingDescription=fbuilder.setlabel("Description").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Description (Max 500 characters)",taDescription);
	    FormField ftaDescription= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();        
	    
	    
	    fbuilder = new FormFieldBuilder("Point Of Contact",customerCorrespondence);
	    FormField fcustomerCorrespondence= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();        
		
	    FormField fcustPrintableName;
	    //Ashwini Patil Date:04-05-2022 
	    if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "PC_CorrespondenceNameMandatory")){
	    	Console.log("in Company Correspondence Name Mandatory");
	    	fbuilder = new FormFieldBuilder("* Company Correspondence Name",custPrintableName);
	 	    fcustPrintableName= fbuilder.setMandatory(true).setMandatoryMsg("Company Correspondence Name is mandatory!").setRowSpan(0).setColSpan(1).build();      
	     }
	    else
	    {
	    	fbuilder = new FormFieldBuilder("Company Correspondence Name",custPrintableName);
	 	    fcustPrintableName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();       	 		
	    }
	    
	    FormField folbSalutation;
	    if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "OnlyForNBHC")){
		    fbuilder = new FormFieldBuilder("* Salutation",olbSalutation);
		    folbSalutation= fbuilder.setMandatory(true).setMandatoryMsg("Salutation is mandatory!").setRowSpan(0).setColSpan(0).build();
	    }
	    else
	    {
	    	fbuilder = new FormFieldBuilder("Salutation",olbSalutation);
	    	folbSalutation= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	    }
	    
	    fbuilder = new FormFieldBuilder("Customer Religion",olbCustomerReligion);
        FormField folbCustomerReligion= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	   
        /**
		 * Date 13-6-2018 by jayshree 
		 * Des.As per mascom requrment some fiels are make non mandatory
		 */
        FormField folbApproverName;
        if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "OnlyForMASCOM"))
        {
        		fbuilder = new FormFieldBuilder(" Approver Name",olbApproverName);
        		folbApproverName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();}
        else
        {
        		fbuilder = new FormFieldBuilder("* Approver Name",olbApproverName);
        		folbApproverName= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();}
        
        
        fbuilder = new FormFieldBuilder("Remark",tbRemark);
        FormField ftbRemark= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
        
		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////

        FormField fquantity;
		FormField fUOM;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "MakeQty&UomMandatory")){
			fbuilder = new FormFieldBuilder("* Quantity",quantity);
			fquantity= fbuilder.setMandatory(true).setMandatoryMsg("Quantity is mandatory!").setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("* UOM",olbUOM);
			fUOM= fbuilder.setMandatory(true).setMandatoryMsg("UOM is mandatory!").setRowSpan(0).setColSpan(0).build();
		}else{
			fbuilder = new FormFieldBuilder("Quantity",quantity);
			fquantity= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("UOM",olbUOM);
			fUOM= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
        /**
         * Date : 15-05-2017 By ANIL
         */
		 /** date 06.10.2018 added by komal **/
        fbuilder = new FormFieldBuilder("Attach",uploadLOI);
        FormField fuploadLOI= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
        
        /**
		 * Updated By: Viraj 
		 * Date: 24-03-2019
		 * Description: To add franchise Information   
		 */
		FormField flbFranchiseType = null;
		FormField fFranchiseBrandUrl = null;
		FormField fmasterUrl = null;
		FormField fgroupingFranchiseInfo = null;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","FranchiseInformation")) {
			fgroupingFranchiseInfo = fbuilder.setlabel("Franchise Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			
			fbuilder = new FormFieldBuilder("Franchise Type",lbFranchiseType);
			flbFranchiseType = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Franchise URL",franchiseUrl);
			fFranchiseBrandUrl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			
			fbuilder = new FormFieldBuilder("Master Franchise URL",masterFranchiseUrl);
			fmasterUrl= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
        /** Ends **/

		  fbuilder = new FormFieldBuilder("DND",cbDND);
	      FormField fisDND= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	       
	      fbuilder = new FormFieldBuilder("Disable Customer Portal",cbDisableCustPortal);
	      FormField fcbDisableCustPortal= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	       
	    //Ashwini Patil Date:7-06-2023 for orion
	      FormField folbAccountManager;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "MakeAccountManagerMandatory")) {
			 fbuilder = new FormFieldBuilder("* Account Manager",olbAccountManager);
		     folbAccountManager= fbuilder.setMandatory(true).setMandatoryMsg("Account Manager is mandatory!").setRowSpan(0).setColSpan(0).build();	
		}else {
			fbuilder = new FormFieldBuilder("Account Manager",olbAccountManager);
			folbAccountManager= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		}
		
		
	     fbuilder = new FormFieldBuilder("Supply Type (This is required for e-invoicing)",olbeInvoiceSupplyType);
	     FormField  folbeInvoiceSupplyType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
	    
	   //Ashwini Patil Date:14-09-2022
		FormField fgroupingHistory=fbuilder.setlabel("Document History").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			
		fbuilder = new FormFieldBuilder("Created By",tbCreatedBy);
		FormField ftbCreatedBy=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Created On",tbCreatedOn);
		FormField ftbCreatedOn=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

	    fbuilder = new FormFieldBuilder("Updated By",tbUpdatedBy);
		FormField ftbUpdatedBy=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("Updated On",tbUpdatedOn);
		FormField ftbUpdatedOn=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Zoho Customer ID",tbZohoCustomerId);
		FormField ftbZohoCustomerId=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("Document Link",tbDocumentLink);
		FormField ftbDocumentLink=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Pest Trend Report Link",tbReportLink);
		FormField ftbReportLink=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Service Dashboard Link",tbReport2Link);
		FormField ftbReport2Link=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
        if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","CustomerApprovalProcess")){
//        	FormField[][] formfield = {  
//    				{fgroupingCustomerInformation},
//    				{ftbCustomerId,cbIsCompany,ftbClientName,fdbDateOfBirth},
//    				{folbSalutation,ftbFullName,fetbEmail,fcustPrintableName},
//    				{fpnbLandlineNo,fpnbCellNo1,fpnbCellNo2,fpnbFaxNo},
//    				{fgroupingSalesInformation},
//    				{ftbCustomerStatus,folbbBranch,folbeSalesPerson,folbgroup},
//    				{folbcategory,folbcCustomerType,folbpriority,folbcCustomerLevel},
//    				{ftbrefnumber1,ftbrefnumber2,ftbcustwebsite,fcustomerCorrespondence},
//    				{folbCustomerReligion,folbApproverName,ftbRemark,fuploadLOI},/** date 06.10.2018 added by komal **/
//    				{fquantity,fUOM},
//    				{fgroupingDescription},
//    				{ftaDescription},
//    				{fgroupingSocialInformation},
//    				{fbillingadrsbtn},
//    				{fbillingAddressComposite},
//    				{fgroupingServiceAddress},
//    				{fserviceadrsbtn},
//    				{fserviceAddressComposite},
//    				{fgroupingTaxInformation},
//    				{ftbTaxTypeInfoComposite}
//    		};
//    		this.fields=formfield;
    		
    		
    		FormField[][] formfield = { 
    				// MAIN SCREEN
    				{fgroupingCustomerInformation},
    				{cbIsCompany,ftbClientName,fcustPrintableName,ftbServiceAddressName},
    				{fcustomerCorrespondence,folbSalutation,ftbFullName,fpnbCellNo1},
    				{fetbEmail,folbbBranch,folbeSalesPerson,folbApproverName},
    				{folbcategory,folbcCustomerType,folbgroup,folbAccountManager},
    				//MAIN SCREEN END
    				//Address start
    				{fgroupingBillingAddress},
    				{fbillingadrsbtn},
    				{fbillingAddressComposite},
    				{fgroupingServiceAddress},
    				{fserviceadrsbtn},
    				{fserviceAddressComposite},
    				//Address end
    				//classification start
    				{fgroupingSalesInformation},
    				/**
    				 * Date : 14-07-2021
    				 * Des :Remove compny type , group and category from classification tab abd add into main tab
    				 * Raised by Nitin Sir.
    				 */
    				{folbpriority,folbcCustomerLevel,folbCustomerReligion,fisDND,fcbDisableCustPortal},
    				//classification end
    				//Article Start
    				{fgroupingTaxInformation},
    				{folbeInvoiceSupplyType},
    				{ftbTaxTypeInfoComposite},
    				//Article end
    				//GeneralInformation start
    				{fgroupingGeneralInformation},
    				{fdbDateOfBirth,fpnbLandlineNo,fpnbCellNo2,fpnbFaxNo,},
    				{ftbrefnumber1,ftbrefnumber2,fquantity,fUOM}, 
    				{ftbZohoCustomerId},
    				//GeneralInformation start
    				{fgroupingSocialInformation},
    				{fserviceadrsbtn},
    				//GeneralInformation end
    				//Description start
    				{fgroupingDescription},
    				{ftaDescription},
    				{fgroupingAttach},    				
    				{fuploadLOI},
    				{ftbDocumentLink},
    				{ftbReportLink},
    				{ftbReport2Link},
    				
    				{fgroupingHistory},
    				{ftbCreatedBy,ftbCreatedOn},
    				{ftbUpdatedBy,ftbUpdatedOn}
    				
    				
    		};
    		
    		
    		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "HideCustomerReligion")) {
    			folbCustomerReligion.setVisible(false);
    		}
    		this.fields=formfield;

    		
    		
        }
        /**
		 * Updated By: Viraj 
		 * Date: 24-03-2019
		 * Description: To add franchise Information   
		 */
		else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","FranchiseInformation")) {
//			FormField[][] formfield = {  
//    				{fgroupingCustomerInformation},
//    				{ftbCustomerId,cbIsCompany,ftbClientName,fdbDateOfBirth},
//    				
////    				{ftbFirstName,ftbMiddleName,ftbLastName,fetbEmail},
//    				
//    				{folbSalutation,ftbFullName,fetbEmail,fcustPrintableName},
////    				{ftbFirstName,ftbMiddleName,ftbLastName,fetbEmail},
//    				
//    				{fpnbLandlineNo,fpnbCellNo1,fpnbCellNo2,fpnbFaxNo},
//    				{fgroupingFranchiseInfo},
//					{flbFranchiseType,fFranchiseBrandUrl,fmasterUrl},
//    				{fgroupingSalesInformation},
//    				{ftbCustomerStatus,folbbBranch,folbeSalesPerson,folbgroup},
//    				{folbcategory,folbcCustomerType,folbpriority,folbcCustomerLevel},
//    				{ftbrefnumber1,ftbrefnumber2,ftbcustwebsite,fcustomerCorrespondence},
//    				{folbCustomerReligion,fquantity,fUOM,fuploadLOI},/** date 06.10.2018 added by komal **/
//    				{fgroupingBillingAddress},
//    				{fbillingadrsbtn},
//    				{fbillingAddressComposite},
//    				{fgroupingServiceAddress},
//    				{fserviceadrsbtn},
//    				{fserviceAddressComposite},
//    				{fgroupingTaxInformation},
//    				{ftbTaxTypeInfoComposite},
//    				/**
//    				 * Updated By:Viraj 
//    				 * Date: 06-02-2019
//    				 * Description: To show description and social info in bottom
//    				 */
//    				{fgroupingDescription},
//    				{ftaDescription},
//    				{fgroupingSocialInformation},
//    				{fsocialInfoComposite}
			FormField[][] formfield = { 
					// MAIN SCREEN
					{fgroupingCustomerInformation},
					
					{cbIsCompany,ftbClientName,fcustPrintableName,ftbServiceAddressName},
    				{fcustomerCorrespondence,folbSalutation,ftbFullName,fpnbCellNo1},
    				/**
    				 * Commneted by Priyanka 
    				 * Des : Approver name should not be reflected on cust screen issue raised by Rahul
    				 ***/
    				
    				{fetbEmail,folbbBranch,folbeSalesPerson,/**folbApproverName,**/folbcategory},
    				{folbcCustomerType,folbgroup,folbAccountManager},
    				//MAIN SCREEN END
    				{fgroupingFranchiseInfo},
					{flbFranchiseType,fFranchiseBrandUrl,fmasterUrl},
    				//Address start
    				{fgroupingBillingAddress},
    				{fbillingadrsbtn},
    				{fbillingAddressComposite},
    				{fgroupingServiceAddress},
    				{fserviceadrsbtn},
    				{fserviceAddressComposite},
    				//Address end
    				//classification start
    				{fgroupingSalesInformation},
//    				{folbgroup,folbcategory,folbcCustomerType,folbpriority},
//    				{folbcCustomerLevel,folbCustomerReligion},
    				{folbpriority,folbcCustomerLevel,folbCustomerReligion,fisDND,fcbDisableCustPortal},
    				//classification end
    				//Article Start
    				{fgroupingTaxInformation},
    				{folbeInvoiceSupplyType},
    				{ftbTaxTypeInfoComposite},
    				//Article end
    				//GeneralInformation start
    				{fgroupingGeneralInformation},
    				{fdbDateOfBirth,fpnbLandlineNo,fpnbCellNo2,fpnbFaxNo,},
    				{ftbrefnumber1,ftbrefnumber2,fcustomerCorrespondence,fquantity},
    				{fUOM,ftbZohoCustomerId},
    				//GeneralInformation start
    				{fgroupingSocialInformation},
    				{fsocialInfoComposite},
    				//GeneralInformation end
    				//Description start
    				{fgroupingDescription},
    				{ftaDescription},
    				//Description end
    				{fgroupingAttach},
    				{fuploadLOI},
    				{ftbDocumentLink},
    				{ftbReportLink},
    				{ftbReport2Link},
    				
    				{fgroupingHistory},
    				{ftbCreatedBy,ftbCreatedOn},
    				{ftbUpdatedBy,ftbUpdatedOn}
					
    		};
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "HideCustomerReligion")) {
    			folbCustomerReligion.setVisible(false);
    		}
    		this.fields=formfield;
        }else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","HideCustomerGroup")){
//        	FormField[][] formfield = {  
//    				{fgroupingCustomerInformation},
//    				{ftbCustomerId,cbIsCompany,ftbClientName,fdbDateOfBirth},
//    				
////    				{ftbFirstName,ftbMiddleName,ftbLastName,fetbEmail},
//    				
//    				{folbSalutation,ftbFullName,fetbEmail,fcustPrintableName},
////    				{ftbFirstName,ftbMiddleName,ftbLastName,fetbEmail},
//    				
//    				{fpnbLandlineNo,fpnbCellNo1,fpnbCellNo2,fpnbFaxNo},
//    				{fgroupingSalesInformation},
//    				{ftbCustomerStatus,folbbBranch,folbeSalesPerson,folbcategory},
//    				{folbcCustomerType,folbpriority,folbcCustomerLevel,ftbrefnumber1},
//    				{ftbrefnumber2,ftbcustwebsite,fcustomerCorrespondence,folbCustomerReligion},
//    				{fquantity,fUOM,fuploadLOI},/** date 06.10.2018 added by komal **/
//    				{fgroupingBillingAddress},
//    				{fbillingadrsbtn},
//    				{fbillingAddressComposite},
//    				{fgroupingServiceAddress},
//    				{fserviceadrsbtn},
//    				{fserviceAddressComposite},
//    				{fgroupingTaxInformation},
//    				{ftbTaxTypeInfoComposite},
//    				/**
//    				 * Updated By:Viraj 
//    				 * Date: 06-02-2019
//    				 * Description: To show description and social info in bottom
//    				 */
//    				{fgroupingDescription},
//    				{ftaDescription},
//    				{fgroupingSocialInformation},
//    				{fsocialInfoComposite}
//    		};
//    		this.fields=formfield;
        	
        	FormField[][] formfield = {  
        					// MAIN SCREEN
        			{fgroupingCustomerInformation},
        			{cbIsCompany,ftbClientName,fcustPrintableName,ftbServiceAddressName},
    				{fcustomerCorrespondence,folbSalutation,ftbFullName,fpnbCellNo1},
    				/**
    				 * Commneted by Priyanka 
    				 * Des :Approver name should not be reflected on cust screen issue raised by Rahul
    				 * 
    				 ***/
    				{fetbEmail,folbbBranch,folbeSalesPerson,/**folbApproverName,**/folbcategory},
    				{folbcCustomerType,folbAccountManager},
    				//MAIN SCREEN END
    				//Address start
    				{fgroupingBillingAddress},
    				{fbillingadrsbtn},
    				{fbillingAddressComposite},
    				{fgroupingServiceAddress},
    				{fserviceadrsbtn},
    				{fserviceAddressComposite},
    				//Address end
    				//classification start
    				{fgroupingSalesInformation},
//    				{/**folbgroup,(hide cust group)**/folbcategory,folbcCustomerType,folbpriority},
//    				{folbcCustomerLevel,folbCustomerReligion},
    				{folbpriority,folbcCustomerLevel,folbCustomerReligion,fisDND,fcbDisableCustPortal},
    				//classification end
    				//Article Start
    				{fgroupingTaxInformation},
    				{folbeInvoiceSupplyType},
    				{ftbTaxTypeInfoComposite},
    				//Article end
    				//GeneralInformation start
    				{fgroupingGeneralInformation},
    				{fdbDateOfBirth,fpnbLandlineNo,fpnbCellNo2,fpnbFaxNo,},
    				{ftbrefnumber1,ftbrefnumber2,fquantity,fUOM},  
    				{ftbZohoCustomerId},
    				//GeneralInformation start
    				{fgroupingSocialInformation},
    				{fsocialInfoComposite},
    				//GeneralInformation end
    				//Description star
    				{fgroupingDescription},
    				{ftaDescription},
    				//Description end
    				{fgroupingAttach},
    				{fuploadLOI},
    				{ftbDocumentLink},
    				{ftbReportLink},
    				{ftbReport2Link},
    				
    				{fgroupingHistory},
    				{ftbCreatedBy,ftbCreatedOn},
    				{ftbUpdatedBy,ftbUpdatedOn}
    		};
        	
        	if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "HideCustomerReligion")) {
    			folbCustomerReligion.setVisible(false);
    		}
    		this.fields=formfield;
        	
        	
        }else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice",AppConstants.PC_EnableOracletab)){
        	
        	FormField[][] formfield = {  
        					// MAIN SCREEN
        			{fgroupingCustomerInformation},
        			{cbIsCompany,ftbClientName,fcustPrintableName,ftbServiceAddressName},
    				{fcustomerCorrespondence,folbSalutation,ftbFullName,fpnbCellNo1},
    				{fetbEmail,folbbBranch,folbeSalesPerson,/**folbApproverName,**/folbcategory},
    				{folbcCustomerType,folbAccountManager},
    				//MAIN SCREEN END
    				//Address start
    				{fgroupingBillingAddress},
    				{fbillingadrsbtn},
    				{fbillingAddressComposite},
    				{fgroupingServiceAddress},
    				{fserviceadrsbtn},
    				{fserviceAddressComposite},
    				//Address end
    				//classification start
    				{fgroupingSalesInformation},
//    				{/**folbgroup,(hide cust group)**/folbcategory,folbcCustomerType,folbpriority},
//    				{folbcCustomerLevel,folbCustomerReligion},
    				{folbpriority,folbcCustomerLevel,folbCustomerReligion,fisDND,fcbDisableCustPortal},
    				//classification end
    				//Article Start
    				{fgroupingTaxInformation},
    				{folbeInvoiceSupplyType},
    				{ftbTaxTypeInfoComposite},
    				//Article end
    				//GeneralInformation start
    				{fgroupingGeneralInformation},
    				{fdbDateOfBirth,fpnbLandlineNo,fpnbCellNo2,fpnbFaxNo,},
    				{ftbBillToCustAccNo,ftbBillToCustSiteNo,ftbShipToCustAccNo,ftbShipToCustSiteNo},  
    				{ftbZohoCustomerId},
    				//GeneralInformation start
    				{fgroupingSocialInformation},
    				{fsocialInfoComposite},
    				//GeneralInformation end
    				//Description star
    				{fgroupingDescription},
    				{ftaDescription},
    				//Description end
    				{fgroupingAttach},
    				{fuploadLOI},
    				{ftbDocumentLink},
    				{ftbReportLink},
    				{ftbReport2Link},
    				
    				{fgroupingHistory},
    				{ftbCreatedBy,ftbCreatedOn},
    				{ftbUpdatedBy,ftbUpdatedOn}
    		};
        	
        	if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "HideCustomerReligion")) {
    			folbCustomerReligion.setVisible(false);
    		}
    		this.fields=formfield;
        	
        	
        
        	
        }
        /** Ends **/
        	else{
//        	FormField[][] formfield = {  
//    				{fgroupingCustomerInformation},
//    				{ftbCustomerId,cbIsCompany,ftbClientName,fdbDateOfBirth},
//    				
////    				{ftbFirstName,ftbMiddleName,ftbLastName,fetbEmail},
//    				
//    				{folbSalutation,ftbFullName,fetbEmail,fcustPrintableName},
////    				{ftbFirstName,ftbMiddleName,ftbLastName,fetbEmail},
//    				
//    				{fpnbLandlineNo,fpnbCellNo1,fpnbCellNo2,fpnbFaxNo},
//    				{fgroupingSalesInformation},
//    				{ftbCustomerStatus,folbbBranch,folbeSalesPerson,folbgroup},
//    				{folbcategory,folbcCustomerType,folbpriority,folbcCustomerLevel},
//    				{ftbrefnumber1,ftbrefnumber2,ftbcustwebsite,fcustomerCorrespondence},
//    				{folbCustomerReligion,fquantity,fUOM,fuploadLOI},/** date 06.10.2018 added by komal **/
//    				{fgroupingBillingAddress},
//    				{fbillingadrsbtn},
//    				{fbillingAddressComposite},
//    				{fgroupingServiceAddress},
//    				{fserviceadrsbtn},
//    				{fserviceAddressComposite},
//    				{fgroupingTaxInformation},
//    				{ftbTaxTypeInfoComposite},
//    				/**
//    				 * Updated By:Viraj 
//    				 * Date: 06-02-2019
//    				 * Description: To show description and social info in bottom
//    				 */
//    				{fgroupingDescription},
//    				{ftaDescription},
//    				{fgroupingSocialInformation},
//    				{fsocialInfoComposite}
//    		};
//    		this.fields=formfield;
        		
        		FormField[][] formfield = {  
        				{fgroupingCustomerInformation},
        				{cbIsCompany,ftbClientName,fcustPrintableName,ftbServiceAddressName},
        				{fcustomerCorrespondence,folbSalutation,ftbFullName,fpnbCellNo1},
        				/**
        				 * Commneted by Priyanka 
        				 * Des : Approver name should not be reflected on cust screen issue raised by Rahul
        				 * 
        				 ***/
        				{fetbEmail,folbbBranch,folbeSalesPerson,/**folbApproverName,**/folbcategory},
        				{folbcCustomerType,folbgroup,folbAccountManager},
        				//MAIN SCREEN END
        				//Address start
        				{fgroupingBillingAddress},
        				{fbillingadrsbtn},
        				{fbillingAddressComposite},
        				{fgroupingServiceAddress},
        				{fserviceadrsbtn},
        				{fserviceAddressComposite},
        				//Address end
        				//classification start
        				{fgroupingSalesInformation},
//        				{folbgroup,folbcategory,folbcCustomerType,folbpriority},
//        				{folbcCustomerLevel,folbCustomerReligion},
        				{folbpriority,folbcCustomerLevel,folbCustomerReligion,fisDND,fcbDisableCustPortal},
        				//classification end
        				//Article Start
        				{fgroupingTaxInformation},
        				{folbeInvoiceSupplyType},
        				{ftbTaxTypeInfoComposite},
        				//Article end
        				//GeneralInformation start
        				{fgroupingGeneralInformation},
        				{fdbDateOfBirth,fpnbLandlineNo,fpnbCellNo2,fpnbFaxNo,},
        				{ftbrefnumber1,ftbrefnumber2,fquantity,fUOM},  
        				{ftbZohoCustomerId},
        				//GeneralInformation start
        				{fgroupingSocialInformation},
        				{fsocialInfoComposite},
        				//GeneralInformation end
        				//Description start
        				{fgroupingDescription},
        				{ftaDescription},
        				//Description end
        				{fgroupingAttach},
        				{fuploadLOI},
        				{ftbDocumentLink},
        				{ftbReportLink},
        				{ftbReport2Link},
        				
        				{fgroupingHistory},
        				{ftbCreatedBy,ftbCreatedOn},
        				{ftbUpdatedBy,ftbUpdatedOn}
        		};
        		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "HideCustomerReligion")) {
        			folbCustomerReligion.setVisible(false);
        		}
        		this.fields=formfield;
        }
        /**
         * End
         */
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(Customer model) 
	{
		model.setNewCustomerFlag(false);
		if(dbDateOfBirth.getValue()!=null)
			model.setDob(dbDateOfBirth.getValue());
//		if(tbFirstName.getValue()!=null)
//			model.setFirstName(tbFirstName.getValue().trim().toUpperCase());
//		if(tbMiddleName.getValue()!=null)
//			model.setMiddleName(tbMiddleName.getValue().trim().toUpperCase());
//		if(tbLastName.getValue()!=null)
//			model.setLastName(tbLastName.getValue().trim().toUpperCase());
		
		if(tbFullName.getValue()!=null)
			model.setFullname(tbFullName.getValue().trim().toUpperCase());
		
//		model.setFullname();
		if(etbEmail.getValue()!=null)
			model.setEmail(etbEmail.getValue().trim());
		/**
		 * Date 27/12/2017 By Jayshree To Check The null condition
		 */
		if(pnbLandlineNo.getValue()!=null){
			model.setLanline(pnbLandlineNo.getValue());
		}
		else{
			model.setLanline(0l);
		}
		if(pnbCellNo1.getValue()!=null){
			
			
			model.setCellNumber1(pnbCellNo1.getValue());
		}
		else{
			model.setCellNumber1(0l);
		}
		/**
		 * Date 27/12/2017 By Jayshree To Check The null condition
		 */
		if(pnbCellNo2.getValue()!=null){
			model.setCellNumber2(pnbCellNo2.getValue());
		}
		else{
			model.setCellNumber2(0l);
		}
		if(pnbFaxNo.getValue()!=null)
			model.setFaxNumber(pnbFaxNo.getValue());
		if(olbbBranch.getValue(olbbBranch.getSelectedIndex())!=null)
			model.setBranch(olbbBranch.getValue(olbbBranch.getSelectedIndex()));
		if(olbeSalesPerson.getValue()!=null)
			model.setEmployee(olbeSalesPerson.getValue());
		if(olbCustomerType.getValue()!=null)
			model.setType(olbCustomerType.getValue(olbCustomerType.getSelectedIndex()));
		if(olbCustomerGroup.getValue()!=null)
			model.setGroup(olbCustomerGroup.getValue());
		if(olbCustomerPriBox.getValue()!=null)
			model.setCustomerPriority(olbCustomerPriBox.getValue());
		if(olbCustomerCategory.getValue()!=null)
			model.setCategory(olbCustomerCategory.getValue());
		if(olbcCustomerLevel.getValue()!=null)
			model.setCustomerLevel(olbcCustomerLevel.getValue());
		if(socialInfoComposite.getValue()!=null)
			model.setSocialInfo(socialInfoComposite.getValue());
		if(billingAddressComposite.getValue()!=null)
			model.setAdress(billingAddressComposite.getValue());
		if(serviceAddressComposite.getValue()!=null)
			model.setSecondaryAdress(serviceAddressComposite.getValue());
		if(tbArticleTypeInfoComposite.getValue()!=null)
			model.setArticleTypeDetails(tbArticleTypeInfoComposite.getValue());
		
		System.out.println("In side update model is comp value "+isCompany.getValue());
		if(isCompany.getValue()==true)
		{
			model.setCompany(isCompany.getValue());

			if(tbClientName.getValue()!=null)
				model.setCompanyName(tbClientName.getValue().trim().toUpperCase());
		}
		else
		{
			model.setCompany(isCompany.getValue());

			if(tbClientName.getValue()!=null)
				model.setCompanyName("");
		}
		
		if(taDescription.getValue()!=null){
			model.setDescription(taDescription.getValue());
		}
		
		if(tbrefnumber1.getValue()!=null)
			model.setRefrNumber1(tbrefnumber1.getValue());
		if(tbrefnumber2.getValue()!=null)
			model.setRefrNumber2(tbrefnumber2.getValue());
		
		if(this.tbCustomerStatus.getValue()!=null)
			model.setStatus(tbCustomerStatus.getValue());
		
		Screen s = (Screen) AppMemory.getAppMemory().currentScreen;
		String currentScrType=s.toString();
		
		if(currentScrType.equals(AppConstants.SCREENTYPESALESCUST)){
			model.setCustScreenType(AppConstants.SALESTYPECUST);
		}
		
		if(currentScrType.equals(AppConstants.SCREENTYPESERVICECUST)){
			model.setCustScreenType(AppConstants.SERVICETYPECUST);
		}
		if(tbcustwebsite.getValue()!=null)
			model.setCustWebsite(tbcustwebsite.getValue());
		
		model.setCustCorresponence(customerCorrespondence.getValue());
		
		model.setNewCustomerFlag(false);

		
		if(custPrintableName.getValue().trim()!=null)
		{
			model.setCustPrintableName(custPrintableName.getValue().trim());
		}
		customerObject=model;
		customerEditFlag=true;
		
		if(olbSalutation.getSelectedIndex()!=0)
		{
			model.setSalutation(olbSalutation.getValue(olbSalutation.getSelectedIndex()));
		}else{
			model.setSalutation("");//Ashwini Patil Date:20-06-2023
		}
		if(tbServiceAddressName.getValue().trim()!=null)
		{
			model.setServiceAddressName(tbServiceAddressName.getValue().trim());
		}
			
		if(olbCustomerReligion.getSelectedIndex()!=0){
			model.setReligion(olbCustomerReligion.getValue(olbCustomerReligion.getSelectedIndex()));
		}
		
		/**
		 * Date : 15-05-2017 By ANIL
		 */
		if(olbApproverName.getValue()!=null){
			model.setApproverName(olbApproverName.getValue());
		}
		
		/**
		 * End
		 */
		if(quantity.getValue()!=null){
			model.setArea(quantity.getValue());
		}
		if(olbUOM.getValue()!=null){
			model.setUnitOfMeasurement(olbUOM.getValue().trim());
		}
		
		/**
		 * Date 23 Feb 2017 added by vijay for customer global list for customer list
		 */
			manageCustomerGloballist(model);
		
		/**
		 * end here
		 */
			/** date 6.10.2018 added by komal**/
			if(uploadLOI.getValue()!=null){
				model.setLoi(uploadLOI.getValue());
			}
			
			/**
			 * Updated By: Viraj 
			 * Date: 24-03-2019
			 * Description: To add franchise Information   
			 */
			if(lbFranchiseType.getSelectedIndex() != 0) {
				model.setFranchiseType(lbFranchiseType.getValue(lbFranchiseType.getSelectedIndex()));
			}
			if(franchiseUrl.getValue() != null) {
				model.setFranchiseUrl(franchiseUrl.getValue());
			}
			if(masterFranchiseUrl.getValue() != null) {
				model.setMasterFranchiseUrl(masterFranchiseUrl.getValue());
			}
			/** Ends **/
			
			if(cbDND.getValue()!=null){
				model.setSmsDNDStatus(cbDND.getValue());
			}
			
			if(cbDisableCustPortal.getValue()!=null) {
				model.setDisableCustomerPortal(cbDisableCustPortal.getValue());
			}
			
			if(tbrefnumber3.getValue()!=null){
				model.setShipToCustAccNo(tbrefnumber3.getValue());
			}
			
			if(tbrefnumber4.getValue()!=null){
				model.setShipToCustSiteNo(tbrefnumber4.getValue());
			}
			
			if(olbAccountManager.getSelectedIndex()!=0){
				model.setAccountManager(olbAccountManager.getValue());
			}
			model.seteInvoiceSupplyType(olbeInvoiceSupplyType.getSelectedItemText());
			
			model.setUpdatedBy(LoginPresenter.loggedInUser);
			
			if(tbZohoCustomerId.getValue()!=null&&!tbZohoCustomerId.getValue().equals(""))
				model.setZohoCustomerId(Long.parseLong(tbZohoCustomerId.getValue()));
			else
				model.setZohoCustomerId(0);
			
			if(tbDocumentLink.getValue()!=null&&!tbDocumentLink.getValue().equals(""))
				model.setDocumentLink(tbDocumentLink.getValue());
			else
				model.setDocumentLink("");
			
			if(tbReportLink.getValue()!=null&&!tbReportLink.getValue().equals(""))
				model.setReportLink(tbReportLink.getValue());
			else
				model.setReportLink("");
			
			if(tbReport2Link.getValue()!=null&&!tbReport2Link.getValue().equals(""))
				model.setReport2Link(tbReport2Link.getValue());
			else
				model.setReport2Link("");
			
			
		presenter.setModel(model);
	}

	/**
	 * 
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(Customer view) 
	{
		customerObject=view;
		
		/**
		 * Date 20-04-2018 By vijay for orion pest locality load  
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Locality", "DontLoadAllLocalityAndCity")){
			setAddressDropDown();
		}
		/**
		 * ends here
		 */
		
	   tbCustomerId.setValue(view.getCount()+"");
		
	   if(!view.getCompanyName().equals("")&&view.isCompany()==true)
			tbClientName.setValue(view.getCompanyName());
		if(view.getDob()!=null)
			dbDateOfBirth.setValue(view.getDob());
//		if(view.getFirstName()!=null)
//			tbFirstName.setValue(view.getFirstName());
//		if(view.getMiddleName()!=null)
//			tbMiddleName.setValue(view.getMiddleName());
//		if(view.getLastName()!=null)
//			tbLastName.setValue(view.getLastName());
		
		
		if(view.getFullname()!=null)
			tbFullName.setValue(view.getFullname());
		
		if(view.getEmail()!=null)
			etbEmail.setValue(view.getEmail());
		if(view.getLandline()!=0)
			pnbLandlineNo.setValue(view.getLandline());
		if(view.getCellNumber1()!=null)
			pnbCellNo1.setValue(view.getCellNumber1());
	//	if(view.getCellNumber2()!=null)
		if(view.getCellNumber2()!=null &&  view.getCellNumber2() != 0 && view.getCellNumber2() != 1)
			pnbCellNo2.setValue(view.getCellNumber2());
		if(view.getFaxNumber()!=null)
			pnbFaxNo.setValue(view.getFaxNumber());
		if(view.getStatus()!=null)
			tbCustomerStatus.setValue(view.getStatus());
		if(view.getBranch()!=null)
			olbbBranch.setValue(view.getBranch());
		if(view.getEmployee()!=null)
			olbeSalesPerson.setValue(view.getEmployee());
		if(view.getType()!=null)
			olbCustomerType.setValue(view.getType());
		if(view.getCategory()!=null)
		{
			AppMemory.setList(view.getCategory(), olbCustomerCategory);
			olbCustomerCategory.setValue(view.getCategory());
		}
		if(view.getGroup()!=null)
			olbCustomerGroup.setValue(view.getGroup());
		if(view.getCustomerPriority()!=null)
			olbCustomerPriBox.setValue(view.getCustomerPriority());
		if(view.getCustomerLevel()!=null)
			olbcCustomerLevel.setValue(view.getCustomerLevel());
		if(view.getSocialInfo()!=null)
			socialInfoComposite.setValue(view.getSocialInfo());
		if(view.getAdress()!=null)
			billingAddressComposite.setValue(view.getAdress());
		if(view.getSecondaryAdress()!=null)
			serviceAddressComposite.setValue(view.getSecondaryAdress());
		if(view.getArticleTypeDetails()!=null)
			tbArticleTypeInfoComposite.setValue(view.getArticleTypeDetails());
		if(view.isCompany()!=null)
			isCompany.setValue(view.isCompany());
		if(view.getRefrNumber1()!=null)
			tbrefnumber1.setValue(view.getRefrNumber1());
		if(view.getRefrNumber2()!=null)
			tbrefnumber2.setValue(view.getRefrNumber2());
		if(view.getCustWebsite()!=null)
			tbcustwebsite.setValue(view.getCustWebsite());
		if(view.getDescription()!=null){
			taDescription.setValue(view.getDescription());
		}
		
		if(view.getCustCorresponence()!=null){
			customerCorrespondence.setValue(view.getCustCorresponence());
		}
		
		if(view.getCustPrintableName()!= null)
		{
			custPrintableName.setValue(view.getCustPrintableName());
		}
		
		checkForExistingCustomer(view.getStatus());
		
		if(view.getSalutation()!=null)
			olbSalutation.setValue(view.getSalutation());
		
		if(view.getServiceAddressName()!= null)
		{
			tbServiceAddressName.setValue(view.getServiceAddressName());
		}
		
		if(view.getReligion()!=null)
			olbCustomerReligion.setValue(view.getReligion());
		
		/**
		 * Date : 15-05-2017 By ANIL
		 */
		if(view.getApproverName()!=null){
			olbApproverName.setValue(view.getApproverName());
		}
		
		if(view.getRemark()!=null){
			tbRemark.setValue(view.getRemark());
		}
		
		/**
		 * End
		 */
		if(view.getUnitOfMeasurement()!=null&&!view.getUnitOfMeasurement().trim().equals("")){
			olbUOM.setValue(view.getUnitOfMeasurement().trim());
		}
		if(view.getArea()!=0){
			quantity.setValue(view.getArea());
		}
		/** date 6.10.2018 added by komal**/
		if(view.getLoi()!=null){
			uploadLOI.setValue(view.getLoi());
		}
		/**
		 * Updated By: Viraj 
		 * Date: 24-03-2019
		 * Description: To add franchise Information   
		 */
		if(view.getFranchiseType() != null) {
			int count = lbFranchiseType.getItemCount();
			String item=view.getFranchiseType().trim();
			for (int i = 0; i < count; i++) {
				if(item.trim().equals(lbFranchiseType.getItemText(i).trim())){
					lbFranchiseType.setSelectedIndex(i);
					break;
				}
			}
		}
		
		if(view.getFranchiseUrl() != null) {
			franchiseUrl.setValue(view.getFranchiseUrl());
		}
		
		if(view.getMasterFranchiseUrl() != null) {
			masterFranchiseUrl.setValue(view.getMasterFranchiseUrl());
		}
		/** Ends **/
		
		cbDND.setValue(view.isSmsDNDStatus());
		
		cbDisableCustPortal.setValue(view.isDisableCustomerPortal());
		
		if(view.getShipToCustAccNo()!=null){
			tbrefnumber3.setValue(view.getShipToCustAccNo());
		}
		
		if(view.getShipToCustSiteNo()!=null){
			tbrefnumber4.setValue(view.getShipToCustSiteNo());
		}
		
		if(view.getAccountManager()!=null && !view.getAccountManager().equals("")){
			olbAccountManager.setValue(view.getAccountManager());
		}
		
		if(view.geteInvoiceSupplyType()!=null) {
			olbeInvoiceSupplyType.setValue(view.geteInvoiceSupplyType());
		}
		
		/* 
		 * for approval process
		 *  nidhi
		 *  5-07-2017
		 */
		if(presenter != null){
			presenter.setModel(view);
		}
		/*
		 *  end
		 */
		
		if(view.getUpdatedBy()!=null){
			tbUpdatedBy.setValue(view.getUpdatedBy());
		}
		if(view.getLastUpdatedDate()!=null){
			tbUpdatedOn.setValue(view.getLastUpdatedDate().toString());
		}
		if(view.getCreatedBy()!=null){
			tbCreatedBy.setValue(view.getCreatedBy());
		}
		if(view.getCreationDate()!=null){
			tbCreatedOn.setValue(view.getCreationDate().toString());
		}
		
		if(view.getZohoCustomerId()!=0){
			tbZohoCustomerId.setValue(view.getZohoCustomerId()+"");
		}
		
		if(view.getDocumentLink()!=null){
			tbDocumentLink.setValue(view.getDocumentLink());
		}
		
		if(view.getReportLink()!=null){
			tbReportLink.setValue(view.getReportLink());
		}
		if(view.getReport2Link()!=null){
			tbReport2Link.setValue(view.getReport2Link());
		}
	}

	// Hand written code shift in presenter

	/**
	 * Date 20-04-2018 By vijay
	 * here i am refreshing address composite data if global list size greater than drop down list size
	 * because we are adding locality and city from an entity in view state to global list if doest not exist in global list 
	 */
	private void setAddressDropDown() {
		if(LoginPresenter.globalLocality.size()>billingAddressComposite.locality.getItems().size()
			|| LoginPresenter.globalLocality.size()>serviceAddressComposite.locality.getItems().size() ){
			billingAddressComposite.locality.setListItems(LoginPresenter.globalLocality);
			serviceAddressComposite.locality.setListItems(LoginPresenter.globalLocality);
		}
	}
	/**
	 * ends here 
	 */
	

	/**
	 * Toggles the app header bar menus as per screen state
	 */
	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		if(isPopUpAppMenubar()){
			return;
		}
		Screen scr=(Screen) AppMemory.getAppMemory().currentScreen;
		String typeofModule=LoginPresenter.currentModule;
		if(AppConstants.SCREENTYPESALESCUST.equals(scr.toString().trim()))
		{
			AuthorizationHelper.setAsPerAuthorization(Screen.SALESCUSTOMER,typeofModule);
		}
		
		if(AppConstants.SCREENTYPESERVICECUST.equals(scr.toString().trim()))
		{
			AuthorizationHelper.setAsPerAuthorization(Screen.CUSTOMER,typeofModule);
			
		}
	}
	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
		tbCustomerId.setValue(count+"");
	}


	@Override
	public void onClick(ClickEvent event) {
		Button butn=(Button) event.getSource();

		if(butn == btnserviceaddr)
		{
			billingAddressComposite.clear();
//			billingAddressComposite.setValue(serviceAddressComposite.getValue());
			/***
			 * @author Vijay
			 * Des :- updated code when we create new city its not loading in billing address tab 
			 */
			if(serviceAddressComposite.locality.getItemCount()!=billingAddressComposite.locality.getItemCount()){
				billingAddressComposite.reinitializeAdressDropDown("Locality");
				billingAddressComposite.setValue(serviceAddressComposite.getValue());
			}
			else if(billingAddressComposite.city.getItemCount()!=serviceAddressComposite.city.getItemCount()){
				billingAddressComposite.reinitializeAdressDropDown("City");
				billingAddressComposite.setValue(serviceAddressComposite.getValue());
			}
			else if(billingAddressComposite.state.getItemCount()!=serviceAddressComposite.state.getItemCount()){
				billingAddressComposite.reinitializeAdressDropDown("State");
				billingAddressComposite.setValue(serviceAddressComposite.getValue());
			}
			else if(billingAddressComposite.country.getItemCount()!=serviceAddressComposite.country.getItemCount()){
				billingAddressComposite.reinitializeAdressDropDown("Country");
				billingAddressComposite.setValue(serviceAddressComposite.getValue());
			}
			else{
				billingAddressComposite.setValue(serviceAddressComposite.getValue());
			}
			
		}
		if(butn == btnbillingaddr)
		{
			serviceAddressComposite.clear(); 
//			serviceAddressComposite.setValue(billingAddressComposite.getValue());
			/***
			 * @author Vijay
			 * Des :- updated code when we create new city its not loading in service address tab 
			 */
			if(billingAddressComposite.locality.getItemCount()!=serviceAddressComposite.locality.getItemCount()){
				serviceAddressComposite.reinitializeAdressDropDown("Locality");
				serviceAddressComposite.setValue(billingAddressComposite.getValue());
			}
			else if(billingAddressComposite.city.getItemCount()!=serviceAddressComposite.city.getItemCount()){
				serviceAddressComposite.reinitializeAdressDropDown("City");
				serviceAddressComposite.setValue(billingAddressComposite.getValue());
			}
			else if(billingAddressComposite.state.getItemCount()!=serviceAddressComposite.state.getItemCount()){
				serviceAddressComposite.reinitializeAdressDropDown("State");
				serviceAddressComposite.setValue(billingAddressComposite.getValue());
			}
			else if(billingAddressComposite.country.getItemCount()!=serviceAddressComposite.country.getItemCount()){
				serviceAddressComposite.reinitializeAdressDropDown("Country");
				serviceAddressComposite.setValue(billingAddressComposite.getValue());
			}
			else{
				serviceAddressComposite.setValue(billingAddressComposite.getValue());
			}
		}
	}


	
	
	//***********************************Buisness Logic Part****************************************************//
	
	@Override
	public void setEnable(boolean status)
	{
		super.setEnable(status);
		System.out.println("in side set enable ");
		this.tbCustomerId.setEnabled(false);
		this.tbCustomerStatus.setEnabled(false);
		this.tbRemark.setEnabled(false);
		/**
		 * Updated By: Viraj
		 * Date: 24-03-2019
		 * Description: To add Franchise Information
		 */
		this.franchiseUrl.setEnabled(false);
		this.masterFranchiseUrl.setEnabled(false);
		/** Ends **/
		/**
		 * Date : 04-08-2017 By ANIL  
		 */
		
//		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW ||AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
//		{
//			System.out.println("in side else condition");
//			isCompany.setEnabled(true);
//			CustomerPresenter custPresenter = (CustomerPresenter) this.presenter;
//			Customer cust = (Customer) custPresenter.getModel();
//			this.tbClientName.setEnabled(true);
//		}
//		else
//		{
//			System.out.println("inside condition");
//			isCompany.setEnabled(false);
//			CustomerPresenter custPresenter = (CustomerPresenter) this.presenter;
//			Customer cust = (Customer) custPresenter.getModel();
//			if (cust.isCompany() == false) {
//				this.tbClientName.setEnabled(false);
//			}
//		}
		
		/**
		 * End
		 */
	}

   @Override
   public void clear()
   {
	   super.clear();
	   this.tbCustomerStatus.setText(Customer.CUSTOMERCREATED);
   }
   
   public void setOneToManyCombo()
   {
	  CategoryTypeFactory.initiateOneManyFunctionality(this.olbCustomerCategory,this.olbCustomerType);
	  AppUtility.makeTypeListBoxLive(this.olbCustomerType,Screen.CUSTOMERCATEGORY);
	
   } 
   
   
   public void toggleProcessLevelMenu()
	{

		Customer entity=(Customer) presenter.getModel();
		String status=entity.getStatus();
		Screen s = (Screen) AppMemory.getAppMemory().currentScreen;
		String scrName=s.toString().trim();
		System.out.println("Current Screen Ecp"+s.toString());
		
		/**
		 * @author Vijay Date - 07-03-2022
		 * Des :- Update Client license update only visible to essevaerp link
		 */
		String Url = com.google.gwt.core.client.GWT.getModuleBaseURL();


		String Appid = "";
		if(Url.contains("-dot-")){
			String [] urlarray = Url.split("-dot-");
			 String url1 = urlarray[1];
			 
			 String [] urlarray1 = url1.split("\\.");
			 Appid = urlarray1[0];
		}
		else{
			String [] urlarray = Url.split("\\.");
			 Appid = urlarray[1];
		}
		Console.log("Appid "+Appid);
		boolean clientLicesnseInfo = true; // false
		if(Appid.trim().equals("essevaerp")) {
			clientLicesnseInfo = true;
		}
		
		/**
		 * Date : 15 -05-2017 By ANIL
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "CustomerApprovalProcess")){
			for(int i=0;i<getProcesslevelBarNames().length;i++)
			{
				InlineLabel label=getProcessLevelBar().btnLabels[i];
				String text=label.getText().trim();
								
				if(status.equals(Customer.CUSTOMERCREATED)||status.equals(Customer.CUSTOMERPROSPECT))
				{
					if(text.equals(AppConstants.VIEWSERVICES))
						label.setVisible(false);
					if(text.equals("Create Lead"))
						label.setVisible(false);
					if(text.equals("Create Quotation"))
						label.setVisible(false);
					if(text.equals("Create Order"))
						label.setVisible(false);
					if(text.equals("Contact List"))//View Contact List
						label.setVisible(false);
					if(text.equals(AppConstants.CUSTOMERVIEWBRANCHES))
						label.setVisible(false);
					if(text.equals(AppConstants.MATERIALREQUIREDREPORT))
						label.setVisible(false);
					if(text.equals("Cancel Approval Request"))
						label.setVisible(false);
				}
				if(status.equals("Requested"))
				{
					if(text.equals(AppConstants.VIEWSERVICES))
						label.setVisible(false);
					if(text.equals("Create Lead"))
						label.setVisible(false);
					if(text.equals("Create Quotation"))
						label.setVisible(false);
					if(text.equals("Create Order"))
						label.setVisible(false);
					if(text.equals("Contact List"))
						label.setVisible(false);
					if(text.equals(AppConstants.CUSTOMERVIEWBRANCHES))
						label.setVisible(false);
					if(text.equals(AppConstants.MATERIALREQUIREDREPORT))
						label.setVisible(false);
					if(text.equals("Request For Approval"))
						label.setVisible(false);
				}
				if(status.equals("Rejected"))
				{
					if(text.equals(AppConstants.VIEWSERVICES))
						label.setVisible(false);
					if(text.equals("Create Lead"))
						label.setVisible(false);
					if(text.equals("Create Quotation"))
						label.setVisible(false);
					if(text.equals("Create Order"))
						label.setVisible(false);
					if(text.equals("Contact List"))
						label.setVisible(false);
					if(text.equals(AppConstants.CUSTOMERVIEWBRANCHES))
						label.setVisible(false);
					if(text.equals(AppConstants.MATERIALREQUIREDREPORT))
						label.setVisible(false);
					if(text.equals("Request For Approval"))
						label.setVisible(false);
					if(text.equals("Cancel Approval Request"))
						label.setVisible(false);
					if(text.equals("Reference Letter"))
						label.setVisible(false);
					if(text.equals("Update Customer Data"))
						label.setVisible(false);
				
					
				}
				
				
				if(status.equals(Customer.CUSTOMERACTIVE)&&scrName.equals(AppConstants.SCREENTYPESALESCUST)){
					if(text.equals(AppConstants.VIEWSERVICES))
						label.setVisible(false);
				}
				
				if(status.equals(Customer.CUSTOMERACTIVE)&&scrName.equals(AppConstants.SCREENTYPESERVICECUST)){
					if(CustomerPresenter.isButtonStatus()==false){
						System.out.println("Process button Status ::"+CustomerPresenter.isButtonStatus());
						if(text.equals("Reference Letter")){
							label.setVisible(false);
						}else{
							label.setVisible(true);
						}
					}else{
						System.out.println("Process button Status ::"+CustomerPresenter.isButtonStatus());
						label.setVisible(true);
					}
				}
				
				/**
				 * @author Vijay Date :- 17-01-2023
				 * Des :- added to show licnese info to essevaerp
				 */
				if(status.equals(Customer.CUSTOMERACTIVE) && scrName.equals(AppConstants.SCREENTYPESERVICECUST)){
					if(text.equals(AppConstants.LICENSEINFO)){
						if(clientLicesnseInfo){
							label.setVisible(true);
						}
						else{
							label.setVisible(false);
						}
					}
				}
				/**
				 * ends here
				 */
			}
		}else{
			for(int i=0;i<getProcesslevelBarNames().length;i++)
			{
				InlineLabel label=getProcessLevelBar().btnLabels[i];
				String text=label.getText().trim();
								
				if(status.equals(Customer.CUSTOMERCREATED)||status.equals(Customer.CUSTOMERPROSPECT))
				{
					if(text.equals(AppConstants.VIEWSERVICES))
						label.setVisible(false);
				}
				
				if(status.equals(Customer.CUSTOMERACTIVE)&&scrName.equals(AppConstants.SCREENTYPESALESCUST)){
					if(text.equals(AppConstants.VIEWSERVICES))
						label.setVisible(false);
				}
				
				if(status.equals(Customer.CUSTOMERACTIVE)&&scrName.equals(AppConstants.SCREENTYPESERVICECUST)){
					if(CustomerPresenter.isButtonStatus()==false){
						System.out.println("Process button Status ::"+CustomerPresenter.isButtonStatus());
						if(text.equals("Reference Letter")){
							label.setVisible(false);
						}else{
							label.setVisible(true);
						}
					}else{
						System.out.println("Process button Status ::"+CustomerPresenter.isButtonStatus());
						label.setVisible(true);
					}
				}
				/**
				 * @author Vijay Date :- 17-01-2023
				 * Des :- added to show licnese info to essevaerp
				 */
				if(status.equals(Customer.CUSTOMERACTIVE) && scrName.equals(AppConstants.SCREENTYPESERVICECUST)){
					if(text.equals(AppConstants.LICENSEINFO)){
						if(clientLicesnseInfo){
							label.setVisible(true);
						}
						else{
							label.setVisible(false);
						}
					}
				}
				/**
				 * ends here
				 */
			}
		}
	}

	@Override
	public void setToViewState() {
		
		super.setToViewState();
		toggleProcessLevelMenu();	
		setAppHeaderBarAsPerStatus();
		
		
		System.out.println("Inside SET TO VIEW METHOD.........");
		Screen scr=(Screen) AppMemory.getAppMemory().currentScreen;
		
		String typeofModule=LoginPresenter.currentModule;
		if(AppConstants.SCREENTYPESALESCUST.equals(scr.toString().trim()))
		{
			SuperModel model=new Customer();
			model=customerObject;
			AppUtility.addDocumentToHistoryTable(AppConstants.SALESMODULE,AppConstants.CUSTOMER, customerObject.getCount(), customerObject.getCount(),customerObject.getFullname(),pnbCellNo1.getValue(), false, model, null);
		}
		
		if(AppConstants.SCREENTYPESERVICECUST.equals(scr.toString().trim()))
		{
			SuperModel model=new Customer();
			model=customerObject;
			AppUtility.addDocumentToHistoryTable(AppConstants.SERVICEMODULE,AppConstants.CUSTOMER,  customerObject.getCount(), customerObject.getCount(),customerObject.getFullname(),pnbCellNo1.getValue(), false, model, null);
			
		}
		
		
		
		if(customerEditFlag==true){
			System.out.println("CUSTOMER EDIT FLAG TRUE >>>>>>");
			
			
			/**
			 * Date 08-08-2018 By Vijay
			 * Des :- new customer updating in global list composite
			 */
			
			try {
				PersonInfo info=new PersonInfo();
				if(isCompany.getValue())
				{
					info.setCount(this.getPresenter().getModel().getCount());
					info.setFullName(tbClientName.getValue().trim().toUpperCase());
					if(pnbCellNo1.getValue()!=null)
						info.setCellNumber(pnbCellNo1.getValue());
					else
						info.setCellNumber(0l);
	
					info.setPocName(tbFullName.getValue());
				}
				else
				{
					info.setCount(this.getPresenter().getModel().getCount());
					info.setFullName(tbFullName.getValue().trim().toUpperCase());
					if(pnbCellNo1.getValue()!=null)
						info.setCellNumber(pnbCellNo1.getValue());
					else
						info.setCellNumber(0l);

					info.setPocName(tbFullName.getValue());
				}
				
			    CustomerPresenterSearchProxy search = (CustomerPresenterSearchProxy) this.getSearchpopupscreen();
			    search.personInfo.setPersonInfoHashMap(info);
			    
				
				
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e.getMessage());
			}
			
			/**
			 * ends here
			 */
			
			
			
			
			Console.log("customerObject Name"+customerObject.getFullname());
			PersonInfoComposite perComp=new PersonInfoComposite();
			perComp.updatingEditedCustomer(customerObject);
			
		}
		
		/**
		 * Added by Priyanka - 27-11-2020
		 * 
		 */

		String mainScreenLabel="Customer Details";
		if(customerObject!=null&&customerObject.getCount()!=0){
			mainScreenLabel=customerObject.getCount()+" "+"/"+customerObject.getStatus()+" "+ "/"+AppUtility.parseDate(customerObject.getCreationDate());
		}
		
//		fgroupingCustomerInformation.setLabel(mainScreenLabel);
		fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
		
		/**
		 *  end
		 * 
		 */
		
		/**
		 * @author Anil @since 30-08-2021
		 */
		if (isPopUpAppMenubar()){
			processLevelBar.setVisibleFalse(false);
		}
	}

	@Override
	public void setToEditState() {
		/**
		 * Date : 17-06-2017 BY ANIL
		 */
		if(tbCustomerStatus.getValue().equals("Rejected")){
			Customer customer=(Customer) presenter.getModel();
			customer.setStatus("Created");
			
			tbCustomerStatus.setValue("Created");
		}
		/**
		 * End
		 */
		System.out.println("in side edit set ");
		super.setToEditState();
		setAppHeaderBarAsPerStatus();
		toggleProcessLevelMenu();
		this.processLevelBar.setVisibleFalse(false);
		
		customerFlag = false;
		
		/**
		 * Updated By: Viraj 
		 * Date: 24-03-2019
		 * Description: To add franchise Information   
		 */
		franchiseUrl.setEnabled(false);
		masterFranchiseUrl.setEnabled(false);
		if(lbFranchiseType.getValue(lbFranchiseType.getSelectedIndex()).equalsIgnoreCase("FRANCHISE")) {
			franchiseUrl.setEnabled(true);
			masterFranchiseUrl.setEnabled(false);
		}
		if(lbFranchiseType.getValue(lbFranchiseType.getSelectedIndex()).equalsIgnoreCase("BRAND")) {
			franchiseUrl.setEnabled(false);
			masterFranchiseUrl.setEnabled(false);
		}
		if(lbFranchiseType.getValue(lbFranchiseType.getSelectedIndex()).equalsIgnoreCase("MASTER FRANCHISE")) {
			masterFranchiseUrl.setEnabled(true);
			franchiseUrl.setEnabled(false);
		}
		/**
		 * Added by Priyanka - 27-11-2020
		 * 
		 */

		String mainScreenLabel="Customer Details";
		if(customerObject!=null&&customerObject.getCount()!=0){
			mainScreenLabel=customerObject.getCount()+" "+"/"+customerObject.getStatus()+" "+ "/"+AppUtility.parseDate(customerObject.getCreationDate());
		}
		
//		fgroupingCustomerInformation.setLabel(mainScreenLabel);
		fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
		
		/**
		 *  end
		 * 
		 */
		tbCreatedBy.setEnabled(false);		
		tbCreatedOn.setEnabled(false);		
		tbUpdatedBy.setEnabled(false);		
		tbUpdatedOn.setEnabled(false);	
		
	
		
	}
	
	
	public void setAppHeaderBarAsPerStatus() {
		Customer entity = (Customer) presenter.getModel();
		String status = entity.getStatus();
		InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
		if(isPopUpAppMenubar()){
			 menus=GeneralViewDocumentPopup.applevelMenu;
		}

		if (status.equals(Quotation.REQUESTED)) {
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
		/**
		 * Date :17-06-2017 BY ANIL
		 */
		if (status.equals(Quotation.REJECTED)) {
			for (int k = 0; k < menus.length; k++) {
				String text = menus[k].getText();
				if (text.contains("Discard") || text.contains("Search")|| text.contains("Edit")|| text.contains(AppConstants.NAVIGATION)) {
					menus[k].setVisible(true);
				} else {
					menus[k].setVisible(false);
				}
			}
		}
		/**
		 * End
		 */
	}
	
	/**
	 * Date 20-03-2018 By vijay below old code moved into presenter for adding customer duplication check
	 */
//	@Override
//	public void onValueChange(ValueChangeEvent<Boolean> event) {
//		
//		if(isCompany.getValue()==true){
//			System.out.println("in side value change true "+isCompany.getValue());
//			tbClientName.setEnabled(true);
//		}
//		if(isCompany.getValue()==false){
//			System.out.println("in side value change false "+isCompany.getValue());
//			tbClientName.setValue("");
//			tbClientName.setEnabled(false);
//			
//			System.out.println("in side false comp value ="+tbClientName.getValue());
//		}
//	}
	/** 
	 * ends here
	 */

	@Override
	public boolean validate() {
		Console.log("in customer validate()");
		if(customerFlag){
			Console.log("customerFlag "+customerFlag);
			Timer timer = new Timer() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					if(glassPanel!=null){
						hideWaitSymbol();
					}
					Console.log("hide wait symbol "+customerFlag);
					customerFlag = false;
				}
			};
			timer.schedule(2000);
		}
		
		boolean superValidate= super.validate();
		if(!validateLongboxValue(pnbCellNo1)){
			showDialogMessage("Format of Cell Number is not proper");
			return false;
		}
		
		if(superValidate==false)
			return false;
		if(this.isCompany.getValue()==true && tbClientName.getText().trim().equals(""))
		{
			showDialogMessage("Company Name is Mandatory !");
			return false;
		}
		/**Date 27-3-2019
		 * Added by Amol for Orion to Disable the Cell Number Validation 
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "DisableCellNumberValidation")){
			disablecellNovalidation=true;
		}
		//Ashwini Patil Date:12-05-2022 If cell nnumber is mandatory then only it should check for validation
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "OnlyForMASCOM")
				|| AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "MakeCellNumberNonMandatory")) {
			cellNoNonMandatory=true;
		}
		if(!disablecellNovalidation&&!cellNoNonMandatory){
			/**
			 * Updated By: Viraj
			 * Date: 06-02-2019
			 * Description: To add cell No validation on value change
			 */
				int currNumLenght = 0;
				if(this.getPnbCellNo1() != null && !this.getPnbCellNo1().equals("")) {
					currNumLenght = this.getPnbCellNo1().getValue().toString().length();
					boolean phoneStatus = AppUtility.validateFieldRange(currNumLenght, 10, 10, "Cell No. 1 can not be less than 10 digit !", "Cell No. 1 can not be more than 10 digit !");
					
					if(!phoneStatus)
					return phoneStatus;
				}
			/** Ends **/
				}
		
		
		
	
		
//		if(CustomerPresenter.custflag==false){
//			showDialogMessage("Customer Name already exists!");
//			return false;
//		}
//		
//		if(CustomerPresenter.clientflag==false){
//			showDialogMessage("Client Name already exists!");
//			return false;
//		}
		
		/*
		 * nidhi
		 * 27-06-2017
		 * For put validation on article type field
		 */
		boolean customerValid = AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","MakeArticleTypeMandatory");
		if(customerValid){
			if(tbArticleTypeInfoComposite.getArticletypetable().getDataprovider().getList().size() == 0){
				showDialogMessage("Article Information is Mandatory !");
				return false;
			}
		}
		/*
		 * end
		 */
		/** date 06/03/2018 added by komal for nbhc**/
	    if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","makePinCodeMandatory")){
	    	if(billingAddressComposite.getPin().getValue() == null || billingAddressComposite.getPin().getValue()==0 ){
	    		showDialogMessage("Pin Code in billing address is Mandatory.");
	    		return false;
	    	}
	    	if(serviceAddressComposite.getPin().getValue() == null || serviceAddressComposite.getPin().getValue()==0){
	    		showDialogMessage("Pin code in service address is Mandatory.");
	    		return false;
	    	}
	    }
	    /** 
	     * end komal
	     */
	    
	    /**
	     * @author Anil
	     * @since 31-12-2020
	     * DOB/Date of establishment can not be greater than current date
	     */
	    DateTimeFormat format= DateTimeFormat.getFormat("dd-MM-yyyy");
	    if(dbDateOfBirth.getValue()!=null&&format.parse(format.format(dbDateOfBirth.getValue())).after(format.parse(format.format(new Date())))){
			showDialogMessage("D.O.B/Establishment Date should not be greater than current date!");
			return false;
		}
	    
		return true;
	}
	
//	public String fillFullName()
//	{
//		String fullname="";
//		if(this.getTbMiddleName().getValue().trim().equals("")){
//			fullname=this.getTbFirstName().getValue().trim()+" "+this.getTbLastName().getValue().trim();
//		}
//		else{
//		   fullname=this.getTbFirstName().getValue().trim()+" "+this.getTbMiddleName().getValue().trim()+" "+this.getTbLastName().getValue().trim();
//		}
//		System.out.println("Full Name Of Cust"+fullname);
//		return fullname;
//	}
	
	public TextBox getTbFullName() {
		return tbFullName;
	}

	public void setTbFullName(TextBox tbFullName) {
		this.tbFullName = tbFullName;
	}

	public void checkForExistingCustomer(String statusValue)
	{
		if(statusValue.trim().equals(AppConstants.ACTIVE)){
//			tbFirstName.getElement().addClassName("personactive");
//			tbMiddleName.getElement().addClassName("personactive");
//			tbLastName.getElement().addClassName("personactive");
			
			tbFullName.getElement().addClassName("personactive");
			
			tbClientName.getElement().addClassName("personactive");
		}
	}
	
	
	/**************************************Type Drop Down Logic****************************************/

	@Override
	public void onChange(ChangeEvent event) {
		
		/**
		 * @author Abhinav
		 * @since 03/12/2019
		 * Ankita pest control Requirement by Rahul Tiwari done with Anil Pal's Guidance "white spaces remove in email box"
		 */
		if(event.getSource().equals(etbEmail)){
			etbEmail.setValue(etbEmail.getValue().trim());
		}
	
		if(event.getSource().equals(olbCustomerCategory)){
			
			if(olbCustomerCategory.getSelectedIndex()!=0){
				
				ConfigCategory cat=olbCustomerCategory.getSelectedItem();
				if(cat!=null){
					AppUtility.makeLiveTypeDropDown(olbCustomerType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
				}
			}
		}
		
		/**
		 * @author Ashwini
		 * @since 13-1-2022
		 * Commenting below code to removing duplicate cell number validation
		 */
//		if(event.getSource() == pnbCellNo1){
//			if(pnbCellNo1.getValue()!=null && pnbCellNo1.getValue()!=0)
//			checkCustomerCellNo(pnbCellNo1.getValue());
//		}
		/**
		 * Updated By: Viraj 
		 * Date: 24-03-2019
		 * Description: To add franchise Information   
		 */
		if(event.getSource().equals(lbFranchiseType)) {
			if(lbFranchiseType.getValue(lbFranchiseType.getSelectedIndex()).equalsIgnoreCase("FRANCHISE")) {
				franchiseUrl.setEnabled(true);
				masterFranchiseUrl.setEnabled(false);
			}
			if(lbFranchiseType.getValue(lbFranchiseType.getSelectedIndex()).equalsIgnoreCase("BRAND")) {
				franchiseUrl.setEnabled(false);
				masterFranchiseUrl.setEnabled(false);
			}
			if(lbFranchiseType.getValue(lbFranchiseType.getSelectedIndex()).equalsIgnoreCase("MASTER FRANCHISE")) {
				masterFranchiseUrl.setEnabled(true);
				franchiseUrl.setEnabled(false);
			}
		}
		/** Ends **/
	}

	
	/**
	 * Date 20-03-2018 By vijay for checking customer cell no exist or not (duplicate cell no)
	 * @param long1 
	 */

	private void checkCustomerCellNo(Long customerCellNo) {

		final MyQuerry querry=new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;

		temp=new Filter();
		temp.setQuerryString("contacts.cellNo1");
		temp.setLongValue(customerCellNo);
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Customer());
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				if(result.size()!=0)
					showDialogMessage("Customer Cell Number Already Exists");
			}
		});
	}
	/**
	 * ends here
	 */

/****************************************Getters And Setters*******************************************/
		
		public ObjectListBox<Config> getOlbCustomerGroup() {
			return olbCustomerGroup;
		}
		
		public void setOlbCustomerGroup(ObjectListBox<Config> olbCustomerGroup) {
			this.olbCustomerGroup = olbCustomerGroup;
		}
		
		public ObjectListBox<Config> getOlbCustomerPriBox() {
			return olbCustomerPriBox;
		}
		
		public void setOlbCustomerPriBox(ObjectListBox<Config> olbCustomerPriBox) {
			this.olbCustomerPriBox = olbCustomerPriBox;
		}
		
		public ObjectListBox<Type> getOlbCustomerType() {
			return olbCustomerType;
		}
		
		public void setOlbCustomerType(ObjectListBox<Type> olbCustomerType) {
			this.olbCustomerType = olbCustomerType;
		}
		
		public ObjectListBox<ConfigCategory> getOlbCustomerCategory()
		{
			return olbCustomerCategory;
		}
		
		public void setOlbCustomerCategory(
				ObjectListBox<ConfigCategory> olbCustomerCategory) {
			this.olbCustomerCategory = olbCustomerCategory;
		}
		
		public TextBox getTbCustomerId() {
			return tbCustomerId;
		}

		public void setTbCustomerId(TextBox tbCustomerId) {
			this.tbCustomerId = tbCustomerId;
		}

		public Button getBtnserviceaddr() {
			return btnserviceaddr;
		}
		
		public void setBtnserviceaddr(Button btnserviceaddr) {
			this.btnserviceaddr = btnserviceaddr;
		}
		
		public Button getBtnbillingaddr() {
			return btnbillingaddr;
		}
		
		public void setBtnbillingaddr(Button btnbillingaddr) {
			this.btnbillingaddr = btnbillingaddr;
		}
		
		public CheckBox getIsCompany() {
			return isCompany;
		}
		
		public void setIsCompany(CheckBox isCompany) {
			this.isCompany = isCompany;
		}
		
		public DateBox getDbDateOfBirth() {
			return dbDateOfBirth;
		}
		
		public void setDbDateOfBirth(DateBox dbDateOfBirth) {
			this.dbDateOfBirth = dbDateOfBirth;
		}
		
		public ObjectListBox<Branch> getOlbbBranch() {
			return olbbBranch;
		}
		
		public void setOlbbBranch(ObjectListBox<Branch> olbbBranch) {
			this.olbbBranch = olbbBranch;
		}
		
		public EmailTextBox getEtbEmail() {
			return etbEmail;
		}
		
		public void setEtbEmail(EmailTextBox etbEmail) {
			this.etbEmail = etbEmail;
		}
		
		public ObjectListBox<Type> getOlbcCustomerType() {
			return olbCustomerType;
		}
		
		public void setOlbcCustomerType(ObjectListBox<Type> olbCustomerType) {
			this.olbCustomerType = olbCustomerType;
		}
		
		public ObjectListBox<Config> getOlbcCustomerLevel() {
			return olbcCustomerLevel;
		}
		
		public void setOlbcCustomerLevel(ObjectListBox<Config> olbcCustomerLevel) {
			this.olbcCustomerLevel = olbcCustomerLevel;
		}
		
		public AddressComposite getBillingAddressComposite() {
			return billingAddressComposite;
		}
		
		public void setBillingAddressComposite(AddressComposite billingAddressComposite) {
			this.billingAddressComposite = billingAddressComposite;
		}
		
		public AddressComposite getServiceAddressComposite() {
			return serviceAddressComposite;
		}
		
		public void setServiceAddressComposite(AddressComposite serviceAddressComposite) {
			this.serviceAddressComposite = serviceAddressComposite;
		}
		
		public SocialInfoComposites getSocialInfoComposite() {
			return socialInfoComposite;
		}
		
		public void setSocialInfoComposite(SocialInfoComposites socialInfoComposite) {
			this.socialInfoComposite = socialInfoComposite;
		}
		
		public TaxInformationComposite getTaxInfoComposite() {
			return taxInfoComposite;
		}
		
		public void setTaxInfoComposite(TaxInformationComposite taxInfoComposite) {
			this.taxInfoComposite = taxInfoComposite;
		}
		
		public TextBox getTbClientName() {
			return tbClientName;
		}
		
		public void setTbClientName(TextBox tbClientName) {
			this.tbClientName = tbClientName;
		}
		
//		public TextBox getTbFirstName() {
//			return tbFirstName;
//		}
//		
//		public void setTbFirstName(TextBox tbFirstName) {
//			this.tbFirstName = tbFirstName;
//		}
//		
//		public TextBox getTbMiddleName() {
//			return tbMiddleName;
//		}
//		
//		public void setTbMiddleName(TextBox tbMiddleName) {
//			this.tbMiddleName = tbMiddleName;
//		}
//		
//		public TextBox getTbLastName() {
//			return tbLastName;
//		}
//		
//		public void setTbLastName(TextBox tbLastName) {
//			this.tbLastName = tbLastName;
//		}
		
		public TextBox getTbCustomerStatus() {
			return tbCustomerStatus;
		}
		
		public void setTbCustomerStatus(TextBox tbCustomerStatus) {
			this.tbCustomerStatus = tbCustomerStatus;
		}
		
		public PhoneNumberBox getPnbLandlineNo() {
			return pnbLandlineNo;
		}
		
		public void setPnbLandlineNo(PhoneNumberBox pnbLandlineNo) {
			this.pnbLandlineNo = pnbLandlineNo;
		}
		
		public PhoneNumberBox getPnbCellNo1() {
			return pnbCellNo1;
		}
		
		public void setPnbCellNo1(PhoneNumberBox pnbCellNo1) {
			this.pnbCellNo1 = pnbCellNo1;
		}
		
		public PhoneNumberBox getPnbCellNo2() {
			return pnbCellNo2;
		}
		
		public void setPnbCellNo2(PhoneNumberBox pnbCellNo2) {
			this.pnbCellNo2 = pnbCellNo2;
		}
		
		public PhoneNumberBox getPnbFaxNo() {
			return pnbFaxNo;
		}
		
		public void setPnbFaxNo(PhoneNumberBox pnbFaxNo) {
			this.pnbFaxNo = pnbFaxNo;
		}
		
		public ObjectListBox<Employee> getOlbeSalesPerson() {
			return olbeSalesPerson;
		}
		
		public void setOlbeSalesPerson(ObjectListBox<Employee> olbeSalesPerson) {
			this.olbeSalesPerson = olbeSalesPerson;
		}

		public ArticleTypeComposite getTbArticleTypeInfoComposite() {
			return tbArticleTypeInfoComposite;
		}

		public void setTbArticleTypeInfoComposite(
				ArticleTypeComposite tbArticleTypeInfoComposite) {
			this.tbArticleTypeInfoComposite = tbArticleTypeInfoComposite;
		}

		
		
		
		public void manageCustomerGloballist(Customer customerModel)
		{
			ScreeenState scrState=AppMemory.getAppMemory().currentState;
			if(CustomerListGridPresenter.globalCustomerlist.size()!=0){
				
			
				if(scrState.equals(ScreeenState.NEW))
				{
					CustomerListGridPresenter.globalCustomerlist.add(customerModel);
				}
				if(scrState.equals(ScreeenState.EDIT)){
					for(int i=0;i<CustomerListGridPresenter.globalCustomerlist.size();i++)
					{
						if(CustomerListGridPresenter.globalCustomerlist.get(i).getId().equals(customerModel.getId()))
						{
							CustomerListGridPresenter.globalCustomerlist.add(customerModel);
							CustomerListGridPresenter.globalCustomerlist.remove(i);
						}
					}
				}
			
			}
			System.out.println("global customer list Size ====="+CustomerListGridPresenter.globalCustomerlist.size());
		}

		@Override
		public void onKeyUp(KeyUpEvent event) {
			String input = tbrefnumber1.getText();
			if (!input.matches("[0-9]*")) {
				String ip=input.substring(0, input.length() - 1);
				showDialogMessage("Please add numbers only!");
				tbrefnumber1.setValue(ip);
				return;
			}else{
				if(input.length()>7){
					showDialogMessage("Only 7 digit number is allowed!");
					String ip=input.substring(0, input.length() - 1);
					tbrefnumber1.setValue(ip);
					return;	
				}
			}
		}
		
		/**Date 8-4-2020
		 *@author Amol 
		 * @param longbox
		 * @return
		 */
		
		public boolean validateLongboxValue(MyLongBox longbox){
			
			if(longbox.getText()!=null&&!longbox.getText().equals("")){
				try {
					long longValue = Long.parseLong(longbox.getText());
					return true;
				} catch (Exception e) {
					return false;
				}
			}
			
			return true;
		}

		@Override
		public void refreshTableData() {
			// TODO Auto-generated method stub
			super.refreshTableData();
			tbArticleTypeInfoComposite.getArticletypetable().getTable().redraw();
		}
		
		
}
