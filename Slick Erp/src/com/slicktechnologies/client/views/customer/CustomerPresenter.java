package com.slicktechnologies.client.views.customer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gson.Gson;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.reusabledata.CopyDataConfig;
import com.slicktechnologies.client.reusabledata.RefrenceLetterPopup;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.services.CompanyRegistrationService;
import com.slicktechnologies.client.services.CompanyRegistrationServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.CustomerNameChangeService;
import com.slicktechnologies.client.services.CustomerNameChangeServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.services.SmsService;
import com.slicktechnologies.client.services.SmsServiceAsync;
import com.slicktechnologies.client.services.TaxInvoiceService;
import com.slicktechnologies.client.services.TaxInvoiceServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.accountinginterface.IntegrateSyncService;
import com.slicktechnologies.client.views.accountinginterface.IntegrateSyncServiceAsync;
import com.slicktechnologies.client.views.cnc.CNCForm;
import com.slicktechnologies.client.views.cnc.CNCPresenter;
import com.slicktechnologies.client.views.complain.ServiceDateBoxPopup;
import com.slicktechnologies.client.views.contactpersonidentification.contactpersonlist.ContactListPresenter;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.contract.ContractPresenterSearchProxy;
import com.slicktechnologies.client.views.customerbranchdetails.CustomerBranchPresenter;
import com.slicktechnologies.client.views.fumigationAustralia.FumigationAustraliaForm;
import com.slicktechnologies.client.views.fumigationdetails.FumigationALPForm;
import com.slicktechnologies.client.views.fumigationdetails.FumigationForm;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.lead.LeadForm;
import com.slicktechnologies.client.views.lead.LeadPresenter;
import com.slicktechnologies.client.views.materialconsumptionreport.MaterialConsumptionReportForm;
import com.slicktechnologies.client.views.materialconsumptionreport.MaterialConsumptionReportPresenter;
import com.slicktechnologies.client.views.popups.LicenseInfoPopup;
import com.slicktechnologies.client.views.popups.ResetAppidPopup;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderForm;
import com.slicktechnologies.client.views.quickcontract.QuickContractForm;
import com.slicktechnologies.client.views.quickcontract.QuickContractPresentor;
import com.slicktechnologies.client.views.quicksalesorder.QuickSalesOrderForm;
import com.slicktechnologies.client.views.quicksalesorder.QuickSalesOrderPresenter;
import com.slicktechnologies.client.views.salesorder.SalesOrderForm;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter;
import com.slicktechnologies.client.views.salesquotation.SalesQuotationForm;
import com.slicktechnologies.client.views.salesquotation.SalesQuotationPresenter;
import com.slicktechnologies.client.views.screenmenuconfiguration.UploadPopup;
import com.slicktechnologies.client.views.service.ServicePresenter;
import com.slicktechnologies.client.views.quotation.QuotationForm;
import com.slicktechnologies.client.views.quotation.QuotationPresenter;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.LicenseDetails;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.cnc.CNCVersion;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
/**
 * FormScreen presenter template.
 */
public class CustomerPresenter extends FormScreenPresenter<Customer> implements ClickHandler,ValueChangeHandler<String> {

	//Token to set the concrete FormScreen class name
	static CustomerForm form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	final static GenricServiceAsync async=GWT.create(GenricService.class);
	public static boolean custflag=true;
	public static boolean clientflag=true;
	String retrCompnyState=retrieveCompanyState();
	RefrenceLetterPopup letterpopup = new RefrenceLetterPopup();
	PopupPanel letterpanel;
	Company comp;

	public static boolean buttonStatus=false;
	/**
	 * Date : 15-05-2017 By ANIL
	 */
	EmailServiceAsync emailService=GWT.create(EmailService.class);
	/**
	 * End
	 */
	boolean custCatandType=false;
	
	CustomerNameChangeServiceAsync customernamechangeasync = GWT.create(CustomerNameChangeService.class);
	
	CommonServiceAsync commonserviceasync = GWT.create(CommonService.class);
	
	ConditionDialogBox conditionPopup = new ConditionDialogBox(
			"Do you update service address in services", AppConstants.YES,
			AppConstants.NO);
	PopupPanel panel;
	
	ResetAppidPopup resetPopup=new ResetAppidPopup();
	PopupPanel resetPopupPanel=new PopupPanel();
	ServiceDateBoxPopup otpPopup=new ServiceDateBoxPopup("OTP"); //Ashwini Patil Date:5-07-2023 to restrict download with otp for hi tech client
	PopupPanel otppopupPanel;
	int generatedOTP;
	SmsServiceAsync smsService = GWT.create(SmsService.class);
	Company companyEntity = null;
	ResetAppidPopup changeCustomerIdPopup=new ResetAppidPopup("UpdateCustomerIdInAllDocs");
	PopupPanel changeCustomerIdPopupPanel=new PopupPanel();
	TaxInvoiceServiceAsync taxAsync = GWT.create(TaxInvoiceService.class);	
    
	public CustomerPresenter  (FormScreen<Customer> view, Customer model) {
		super(view, model);
		form=(CustomerForm) view;
		form.setPresenter(this);
//		form.getTbFirstName().addValueChangeHandler(this);
//		form.getTbMiddleName().addValueChangeHandler(this);
//		form.getTbLastName().addValueChangeHandler(this);
		
		form.getTbFullName().addValueChangeHandler(this);
		
		form.getTbClientName().addValueChangeHandler(this);
		
		letterpopup.getBtnOk().addClickHandler(this);
		letterpopup.getBtnCancel().addClickHandler(this);
		
		letterpopup.getForAll().addClickHandler(this);
		letterpopup.getForBank().addClickHandler(this);
		
		/** Date 20-03-2018 By Vijay is company added click handler**/
		form.isCompany.addClickHandler(this);
		
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
		
		String typeofModule=LoginPresenter.currentModule;
		Screen scr=(Screen) AppMemory.getAppMemory().currentScreen;
		if(AppConstants.SCREENTYPESALESCUST.equals(scr.toString().trim()))
		{
			boolean isDownload1=AuthorizationHelper.getDownloadAuthorization(Screen.SALESCUSTOMER,typeofModule);
			if(isDownload1==false){
				form.getSearchpopupscreen().getDwnload().setVisible(false);
			}
		}
		
		if(AppConstants.SCREENTYPESERVICECUST.equals(scr.toString().trim()))
		{
			boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.CUSTOMER,typeofModule);
			if(isDownload==false){
				form.getSearchpopupscreen().getDwnload().setVisible(false);
			}
		}
		
		resetPopup.getBtnOk().addClickHandler(this);
		resetPopup.getBtnCancel().addClickHandler(this);
		otpPopup.getBtnOne().addClickHandler(this);
		otpPopup.getBtnTwo().addClickHandler(this);
		
		changeCustomerIdPopup.getBtnOk().addClickHandler(this);
		changeCustomerIdPopup.getBtnCancel().addClickHandler(this);
	}

	private static void retriveButtonStatus(){
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("processName");
		filter.setStringValue("Customer");
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("processList.status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new ProcessConfiguration());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}			

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println(" result set size +++++++"+result.size());
				
				List<ProcessTypeDetails> processList =new ArrayList<ProcessTypeDetails>();
					for(SuperModel model:result)
					{
						ProcessConfiguration processConfig=(ProcessConfiguration)model;
						processList.addAll(processConfig.getProcessList());
						
					}
				
				for(int k=0;k<processList.size();k++){	
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("InvoiceAndRefDocCustmization")&&processList.get(k).isStatus()==true){
					
					buttonStatus=true;
				}
			}
			}
		});
		//**************************changes ends here ********************************
		
	}

	
	@Override
	public void reactOnSearch() {
		super.reactOnSearch();

		CustomerPresenterSearchProxy.makeSalesPersonEnable();
	}
	
	
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals("Create Lead")){
			reactToCreateLead();
		}
		if(text.equals("Create Quotation")){
			if(form.validate()==true){
			reactToCreateQuotation();
			}else{
				form.showDialogMessage("Please fill mandatory fields.");
			}
		}
		if(text.equals("Create Order")){
			/**
			 * Date 31-01-2019 by Vijay For NBHC CCPM Lead and quotation mandatory so contract creation not allowed 
			 * from here if process config is active
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "MakeLeadAndQuotationIdMandatory")
					&&  !LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Admin")){
				form.showDialogMessage("Please create lead and quotation first! contract creation not allow from here!");
			}else{
				if(form.validate()==true){
					reactToCreateContract();
					}else{
						form.showDialogMessage("Please fill mandatory fields.");
					}	
			}

			
		}
		if(text.equals("View Services")){
			reactToViewServices();
		}
		if(text.equals("New")){
			reactToNew();
		}
		if(text.equals("Contact List")){
			reactOnContactList();
		}
		if(text.equals("View Branches")){
			reactOnViewBranches();
		}
		if(text.equals(AppConstants.MATERIALREQUIREDREPORT)){
			getcontractIdfomCustomerID();
		}
		
		if(text.equals("Reference Letter")){
			reactOnRefLteers();
		}
		
		/************ vijay for customer name chnage reflect in all doc *****************/
		if(text.equals("Update Customer Address")){
			System.out.println("i am in chnage customer name change");
			reactOnChnageCustomerName(true);
		}
		
		/**
		 * Date : 15-05-2017 By ANIL
		 */
		if(text.equals("Request For Approval"))
		{
			if(form.olbApproverName.getValue()==null){
				form.showDialogMessage("Please select approver name.");
				return;
			}
			form.showWaitSymbol();
			String documentCreatedBy="";
			if(model.getCreatedBy()!=null){
				documentCreatedBy=model.getCreatedBy();
			}
			
			ApproverFactory appFactory=new ApproverFactory();
//			Approvals approval=appFactory.getApprovals(LoginPresenter.myUserEntity.getEmployeeName(),model.getApproverName()
//					, model.getBranch(), model.getCount(),documentCreatedBy);
			/**
			 *  nidhi
			 *  7-10-2017
			 *   for customer approval name display
			 */
			Approvals approval=appFactory.getApprovals(LoginPresenter.myUserEntity.getEmployeeName(),model.getApproverName()
					, model.getBranch(), model.getCount(),documentCreatedBy,model.getCount()+"",model.getCustomerName());
			/**
			 * end
			 */
			
			saveApproval(approval,ConcreteBusinessProcess.REQUESTED,"Request Sent!");
			reactOnApprovalEmail(approval);
			
			
		}
		
		if(text.equals("Cancel Approval Request"))
		{
			form.showWaitSymbol(); 
	  		ApproverFactory approverfactory=new ApproverFactory();
			  
			  Filter filter=new Filter();
			  filter.setStringValue(approverfactory.getBuisnessProcessType());
			  filter.setQuerryString("businessprocesstype");
			  
			  Filter filterOnid=new Filter();
			  filterOnid.setIntValue(model.getCount());
			  filterOnid.setQuerryString("businessprocessId");
			  
			  Filter filterOnStatus=new Filter();
			  filterOnStatus.setStringValue(Approvals.PENDING);
			  filterOnStatus.setQuerryString("status");
			  
			  Vector<Filter> vecfilter=new Vector<Filter>();
			  vecfilter.add(filter);
			  vecfilter.add(filterOnid);
			  vecfilter.add(filterOnStatus);
			  MyQuerry querry=new MyQuerry(vecfilter, new Approvals());
			  final ArrayList<SuperModel> array = new ArrayList<SuperModel>();
			  async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

				@Override
				public void onFailure(Throwable caught) {
					caught.printStackTrace();
					
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					array.addAll(result);
					Approvals approval=(Approvals) array.get(0);
					approval.setStatus(Approvals.CANCELLED);
					 
					 saveApproval(approval, ConcreteBusinessProcess.CREATED, "Approval Request Cancelled!");
				}
			});
		}
		
		/**
		 * End
		 */
		
		/**
		 * Old Code
		 */
		
//		/*
//		 *  nidhi
//		 *   07-07-2017
//		 *   call new quick contract  
//		 */
//		if(text.equals("Quick Contract")){
//			if(form.validate()==true){
//				reactToQuickContract();
//			}else{
//				form.showDialogMessage("Please fill mandatory fields.");
//			}
//		}
		
		/*
		 *  sagar
		 *   20-09-2017
		 *   call new quick order  
		 */
		if (text.equals("Quick Order")) {
			
			Screen s = (Screen) AppMemory.getAppMemory().currentScreen;
			String currentScrType = s.toString();
			if (AppConstants.SCREENTYPESERVICECUST.equals(currentScrType)) {
				if (form.validate() == true) {
					reactToQuickContract();
				} else {
					form.showDialogMessage("Please fill mandatory fields.");
				}

			}
			if (AppConstants.SCREENTYPESALESCUST.equals(currentScrType)) {
				
				System.out.println("checking screen");
				if (form.validate() == true) {
					reactToQuickOrder();
				} else {
					form.showDialogMessage("Please fill mandatory fields.");
				}
			}
		}
		
		/**
		 * Updated By: Viraj
		 * Date: 03-06-2019
		 * Description: To switch to CNC screen on Click of create CNC
		 */
		if(text.equals("Create CNC")) {
			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/CNC",Screen.CNC);
			final CNCForm cncForm = CNCPresenter.initalize();
			
			cncForm.loadCustomerBranch(Integer.parseInt(form.tbCustomerId.getValue()), null,false);
			cncForm.showWaitSymbol();
			Timer t = new Timer() {

				@Override
				public void run() {
					System.out.println("Inside run");
					cncForm.setToNewState();
					cncForm.hideWaitSymbol();
					PersonInfo personInfo = new PersonInfo();
					personInfo.setCellNumber(model.getCellNumber1());
					personInfo.setCount(model.getCount());
					/**Date 30-7-2019 by Amol change the customer name to Company Name**/
					personInfo.setFullName(model.getCompanyName());
					
					cncForm.getPersonInfoComposite().setValue(personInfo);
					CNCPresenter cncPresen=(CNCPresenter) cncForm.getPresenter();
					cncPresen.getCustomerAddress();
					cncForm.getOlbbBranch().setValue(model.getBranch());
					cncForm.getOlbeSalesPerson().setValue(model.getEmployee());
				}
				
			};
			t.schedule(3000);
		}
		/** Ends **/
		
		if(text.equals(AppConstants.LISENCEINFORMATION)){
			reactonLisenceInfo();
		}
		if(text.equals(AppConstants.RESETAPPID)){
			reactonResetAppid();
		}
		if(text.equals(AppConstants.UpdateCustomerIdInAllDocs)) {
			changeCustomerIdPopup.getBtnOk().setText("Update");
			changeCustomerIdPopupPanel=new PopupPanel(true);
			changeCustomerIdPopupPanel.add(changeCustomerIdPopup);
			changeCustomerIdPopupPanel.center();
			changeCustomerIdPopupPanel.show();
		}
	}
	
	
	/*----------------------------------------------------------
	 * 20-09-2017
	 * by sagar
	 * reactToQuickOrder function added
	 * */

	private void reactToQuickOrder() {
		try {
			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Quick Order", Screen.QUICKSALESORDER);
			final QuickSalesOrderForm form = QuickSalesOrderPresenter.initalize();
			Console.log("Initalize the screen");
			form.showWaitSymbol();
			System.out.println("Starting timer");
			Timer t = new Timer() {
				@Override
				public void run() {
					Console.log("inside Run method");
					System.out.println("Inside run");
					form.setToNewState();
					form.hideWaitSymbol();
					PersonInfo personInfo = new PersonInfo();
					personInfo.setCellNumber(model.getCellNumber1());
					personInfo.setCount(model.getCount());
					if (model.isCompany() == true) {
						personInfo.setFullName(model.getCompanyName());
					} else {
						personInfo.setFullName(model.getFullname());
					}
					/*
					 * 21-09-2017 by sagar sore
					 */

					List<ArticleType> articleList;
					if (!(articleList = model.getArticleTypeDetails()).isEmpty()) {
						form.getTbGSTNumber().setValue(articleList.get(0).getArticleTypeValue());
					}
					/*
					 * End
					 */

					personInfo.setPocName(model.getFullname());
					System.out.println("PersonInfo Name "
							+ personInfo.getFullName());
					if (model.getStatus().equals(AppConstants.ACTIVE)) {
						form.getPersonInfoComposite().getId().getElement()
								.addClassName("personactive");
						form.getPersonInfoComposite().getName().getElement()
								.addClassName("personactive");
						form.getPersonInfoComposite().getPhone().getElement()
								.addClassName("personactive");
						form.getPersonInfoComposite().getTbpocName()
								.getElement().addClassName("personactive");
					}

					// form.checkCustomerBranch(model.getCount());

					form.getPersonInfoComposite().setValue(personInfo);

					form.getOlbbBranch().setValue(model.getBranch());
					form.getOlbeSalesPerson().setValue(model.getEmployee());
					// form.getTbStatus().setValue(Contract.CREATED);
					form.getTbQuotatinId().setText("");
					form.getTbContractId().setText("");
					form.getPersonInfoComposite().setEnabled(false);
					
					form.getAcshippingcomposite().setValue(model.getSecondaryAdress());
					form.getCustomerAddressComposite().setValue(model.getAdress());

					form.getTbCompanyName().setValue("");
					form.getTbCompanyName().setEnabled(false);

					form.getTbFullName().setEnabled(false);
					Console.log("start code for existing issue");
					/**
					 * @author Priyanka
					 * @since 31-12-2020
					 * While when created directly from Quick Sales Order then this field are non-editable.
					 * raised by Rahul Tiwari
					 */
					
					form.getPnbCellNo().setEnabled(false);
					form.getEtbEmail().setEnabled(false);
					form.getTbGSTNumber().setEnabled(false);
					form.getOlbbBranch().setEnabled(false);
					form.getCustomerAddressComposite().setEnable(false);
					/**
					 * End
					 */
					form.getOlbbBranch().setValue(model.getBranch());
			    	  Branch branchEntity =form.getOlbbBranch().getSelectedItem();
			    	  if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
							form.olbcNumberRange.setValue(branchEntity.getNumberRange());
							Console.log("Number range set to drop down by default");
			    	  }else {
			    		  	String range=LoginPresenter.branchWiseNumberRangeMap.get(model.getBranch());
					  		Console.log("LoginPresenter.branchWiseNumberRangeMap size="+LoginPresenter.branchWiseNumberRangeMap.size());	
							if(range!=null&&!range.equals("")) {
								form.olbcNumberRange.setValue(range);
								Console.log("in else Number range set to drop down by default");							
							}else {
								form.olbcNumberRange.setSelectedIndex(0);
								Console.log("could not set number range");
							}					
			    	  }

				}
			};
			t.schedule(3000);

		} catch (Exception e) {
			Console.log("" + e);
		}

	}

	/*
	 * change end here
	 */
	
	/*
	 *  nidhi
	 *   07-07-2017
	 *   move to quick contract screen
	 *   
	 */
	
	private void reactToQuickContract(){
		try {
			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Quick Contract",Screen.QUICKCONTRACT);
			final QuickContractForm form = QuickContractPresentor.initalize();
			form.showWaitSymbol();
			Timer t = new Timer() {
			      @Override
			      public void run() {

			    	form.setToNewState();
					form.hideWaitSymbol();
					PersonInfo personInfo = new PersonInfo();
					personInfo.setCellNumber(model.getCellNumber1());
					personInfo.setCount(model.getCount());
					if (model.isCompany() == true)
						personInfo.setFullName(model.getCompanyName());
					else {
//						model.fillAllNames();
						personInfo.setFullName(model.getFullname());

					}
					personInfo.setPocName(model.getFullname());

					if (model.getStatus().equals(AppConstants.ACTIVE)) {
						form.getPersonInfoComposite().getId().getElement()
								.addClassName("personactive");
						form.getPersonInfoComposite().getName().getElement()
								.addClassName("personactive");
						form.getPersonInfoComposite().getPhone().getElement()
								.addClassName("personactive");
						form.getPersonInfoComposite().getTbpocName().getElement()
								.addClassName("personactive");
					}
						
					  form.checkCustomerBranch(model.getCount());
					  form.getPersonInfoComposite().setValue(personInfo);
					  
					  form.getOlbbBranch().setValue(model.getBranch());
					  form.getOlbeSalesPerson().setValue(model.getEmployee());
					  form.getTbStatus().setValue(Contract.CREATED);
			    	  form.getTbQuotatinId().setText("");
			    	  form.getTbContractId().setText("");
			    	  form.getPersonInfoComposite().setEnabled(false);
			    	  form.getCustomerAddress(model.getCount());
			    	  form.getCustomerAddressComposite().setValue(model.getSecondaryAdress());
			    	  form.getCustomerAddressComposite().setEnable(false);
			    	  
			    	  form.getTbCompanyName().setValue("");
			    	  form.getTbCompanyName().setEnabled(false);
			    	 
			    	  form.getTbFullName().setValue("");
			    	  form.getTbFullName().setEnabled(false);
			    	  
			    	  form.getPnbCellNo().setValue(0l);
			    	  form.getPnbCellNo().setEnabled(false);
			    	  
			    	  form.getEtbEmail().setValue("");
			    	  form.getEtbEmail().setEnabled(false);
			    	  
			    	  
			    	 Branch branchEntity =form.getOlbbBranch().getSelectedItem();
			    	  if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
							form.olbcNumberRange.setValue(branchEntity.getNumberRange());
							Console.log("Number range set to drop down by default");
			    	  }else {
			    		  	String range=LoginPresenter.branchWiseNumberRangeMap.get(model.getBranch());
					  		Console.log("LoginPresenter.branchWiseNumberRangeMap size="+LoginPresenter.branchWiseNumberRangeMap.size());	
							if(range!=null&&!range.equals("")) {
								form.olbcNumberRange.setValue(range);
								Console.log("in else Number range set to drop down by default");							
							}else {
								form.olbcNumberRange.setSelectedIndex(0);
								Console.log("could not set number range");
							}					
			    	  }
			    	  
			      }
			    };
			    t.schedule(3000);
		
		
		} catch (Exception e) {
			Console.log(""+e);
		}
		
	}
	
	/*
	 *  end
	 */
	
	
	private void reactOnChnageCustomerName(boolean updateaddressFlag) {
		System.out.println("in the method");
		System.out.println("name --- "+model.getFullname());
		
		form.showWaitSymbol();
		customernamechangeasync.SaveCustomerChangeInAllDocs(model,updateaddressFlag, new AsyncCallback<ArrayList<Integer>>() {
			
			@Override
			public void onSuccess(ArrayList<Integer> result) {
				System.out.println("in on success ");
					form.showDialogMessage("Customer Address Updation Process has started. Please check after 30 minutes");
				form.hideWaitSymbol();
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("falied");
				form.showDialogMessage("Unexpected Error");
				form.hideWaitSymbol();
			}
		});
		
	}

	private void reactOnRefLteers() {
		
		System.out.println(" in side ref letter");
		
		
		    letterpanel=new PopupPanel(true);
		    letterpanel.add(letterpopup);
		    letterpanel.show();
		    letterpanel.center();
		
	}

	@Override
	public void reactOnPrint() {

	}

	@Override
	public void reactOnDownload() {
		
			ArrayList<Customer> custarray=new ArrayList<Customer>();
			List<Customer> list=(List<Customer>) form.getSearchpopupscreen().getSupertable().getDataprovider().getList();
			
			custarray.addAll(list);
			
			csvservice.setcustomerlist(custarray, new AsyncCallback<Void>(){

				@Override
				public void onFailure(Throwable caught) {
					System.out.println("RPC call Failed"+caught);
				}
				
				@Override
				public void onSuccess(Void result) {
		
					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
					final String url=gwt + "csvservlet"+"?type="+1;
					Window.open(url, "test", "enabled");
					
//					String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//					final String url=gwt + "CreateXLXSServlet"+"?type="+17;
//					Window.open(url, "test", "enabled");
					
				}
			});
			
//			downloadData(custarray);
	}
	
	
	private void reactOnViewBranches() {

		final MyQuerry querry=new MyQuerry();
		Vector<Filter>filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(model.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("cinfo.count");
		temp.setIntValue(model.getCount());
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerBranchDetails());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if(result==null||result.size()<1) //Ashwini Patil Date:28-12-2023 validation added
				{
					form.showDialogMessage("No Customer branches found!");
					return;
				}else{
					AppMemory.getAppMemory().currentState=ScreeenState.NEW;
			    	  
			    	AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Branch",Screen.CUSTOMERBRANCH);
			    	 
					CustomerBranchPresenter.initialize(querry);
				}
			}
		});
		

		
    	 
    	 
		
	}

	

	private void getcontractIdfomCustomerID() {
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(Integer.parseInt(form.getTbCustomerId().getValue().trim()));
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Contract());
	
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("result size ==="+ result.size());
				final List<String> contableList=  new ArrayList<String>();
				for(SuperModel model:result)
				{
					
					Contract conEntity = (Contract)model;
					contableList.add(conEntity.getCount()+"");
					
				}
				
				
				AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Inventory/Material Consumption Report",Screen.MATERIALCONSUMPTIONREPORT);
				final MaterialConsumptionReportForm form=MaterialConsumptionReportPresenter.initialize();
				form.getContractCount().addItem("Select");
				for(int i=0;i<contableList.size();i++){
				      form.getContractCount().addItem(contableList.get(i));
					 }
				form.getPersonInfoComposite().getId().setValue(model.getCount()+"");
				if(model.getCompanyName()!=null){
					form.getPersonInfoComposite().getName().setValue(model.getCompanyName());
				}
				else{
					form.getPersonInfoComposite().getName().setValue(model.getName());
				}
				form.getPersonInfoComposite().getPhone().setValue(model.getCellNumber1()+"");
				form.getPersonInfoComposite().getTbpocName().setValue(model.getName());
				
			}
			});
		
	}
	
	
	
	
	protected void reactOnContactList()
	{
		Console.log("contact button clicked ...");
		
		PersonInfo pinfo=new PersonInfo();
		pinfo.setCount(model.getCount());
		if(model.getCompanyName()!=null&&!model.getCompanyName().equals(""))
			pinfo.setFullName(model.getCompanyName());
		else
			pinfo.setFullName(model.getFullname());
		pinfo.setCellNumber(model.getCellNumber1());
		pinfo.setPocName(model.getFullname());
		GeneralViewDocumentPopup genPopup=new GeneralViewDocumentPopup();
		genPopup.getBtnClose().addClickHandler(this);
		genPopup.setCompositeScreen("Contact Person Details", null,pinfo,false);
		
		
		//old code
//		Screen s = (Screen) AppMemory.getAppMemory().currentScreen;
//		String currentScrType=s.toString();
//		System.out.println("Screen Type"+currentScrType);
//		
//		MyQuerry querry=new MyQuerry();
//		Vector<Filter>filtervec=new Vector<Filter>();
//		Filter temp=null;
//		
//		temp=new Filter();
//		temp.setQuerryString("companyId");
//		temp.setLongValue(model.getCompanyId());
//		filtervec.add(temp);
//		
//		temp=new Filter();
//		temp.setIntValue(model.getCount());
//		temp.setQuerryString("personInfo.count");
//		filtervec.add(temp);
		
//		querry.setFilters(filtervec);
//		querry.setQuerryObject(new ContactPersonIdentification());
//		
//    	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
//    	 
//    	 if(AppConstants.SCREENTYPESALESCUST.equals(currentScrType)){
//    		 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Contact Person List",Screen.CONTACTPERSONLIST);
//    	 }
//    	 else{
//    		 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contact Person List",Screen.CONTACTPERSONLIST);
//    	 }
//    	 
//    	 ContactListPresenter.initialize(querry);
	}
	


	

	@Override
	public void reactOnEmail() {


	}

	/**
	 * Method token to make new model
	 */
	@Override
	protected void makeNewModel() {
		model=new Customer();
	}




 public static CustomerForm initalize()
 {
	 /**
	  * Date : 17-06-2017 By ANIL
	  */
//		retriveButtonStatus();
//		Timer t = new Timer() {
//			@Override
//			public void run() {
	 	if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","InvoiceAndRefDocCustmization")){
	 		buttonStatus=true;
	 	}
		CustomerForm form = new CustomerForm();
		CustomerPresenterTable gentable = new CustomerPresenterTableProxy();
		gentable.setView(form);
		gentable.applySelectionModle();
		CustomerPresenterSearch.staticSuperTable = gentable;
		CustomerPresenterSearch searchpopup = new CustomerPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);
		CustomerPresenter presenter = new CustomerPresenter(form,new Customer());
//		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
//			}
//		};
//		t.schedule(4000);

		return form;
		/**
		 * ENd
		 */
	}

	@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessprocesslayer.Customer")
	public static  class CustomerPresenterSearch extends SearchPopUpScreen<Customer>{

		@Override
		public MyQuerry getQuerry() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean validate() {
			// TODO Auto-generated method stub
			return true;
		}};

		@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.common.businessprocesslayer.Customer")
		public static class CustomerPresenterTable extends SuperTable<Customer> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				return null;
			}

			@Override
			public void createTable() {

			}

			@Override
			protected void initializekeyprovider() {

			}

			@Override
			public void addFieldUpdater() {

			}

			@Override
			public void setEnable(boolean state) {

			}

			@Override
			public void applyStyle() {

			}} ;
			
		
//			private void validateCopyingDataForLead()
//			{
//				boolean validatingProcessName=CopyDataConfig.validateCopyData();
//				boolean validatingProcessConfig=false;
//				System.out.println("ValidateCopyData"+validatingProcessName);
//				
//				if(validatingProcessName==true)
//				{
//					validatingProcessConfig=CopyDataConfig.validateProcessConfig(AppConstants.LEADCOPYDATA);
//					System.out.println("Inside PC Copy"+validatingProcessConfig);
//				}
//				
//				System.out.println("PCONFIG"+validatingProcessConfig);
//				
//				if(validatingProcessName==true){
//					
//					System.out.println("Entered0");
//					if(validatingProcessConfig==true){
//						reactToCreateLead();
//					}
//					else{
//						switchToLeadScreen();
//					}
//				}
//				else{
//					switchToLeadScreen();
//				}
//			}
			
			
			private void reactToCreateLead()
			{
				System.out.println("Lead REact");
				boolean validateCopyingForData=CopyDataConfig.validateDataCopying(AppConstants.LEADCOPYDATA);
				boolean categoryDataVal=false;
				boolean typeDataVal=false;
				boolean groupDataVal=false;
				boolean priorityDataVal=false;
				
				if(validateCopyingForData==true)
				{
					categoryDataVal=CopyDataConfig.validateData(AppConstants.COPYCATEGORYDATA, model.getCategory(), Screen.LEADCATEGORY);
					typeDataVal=CopyDataConfig.validateData(AppConstants.COPYTYPEDATA, model.getType(), Screen.LEADTYPE);
					groupDataVal=CopyDataConfig.validateData(AppConstants.COPYGROUPDATA, model.getGroup(), Screen.LEADGROUP);
					priorityDataVal=CopyDataConfig.validateData(AppConstants.COPYPRIORITYDATA, model.getCustomerPriority(), Screen.LEADPRIORITY);
					
					if(categoryDataVal==false){
						form.showDialogMessage("Lead Category Value does not exists in Customer Category!");
					}
					
					if(categoryDataVal==true&&typeDataVal==false){
						form.showDialogMessage("Lead Type value does not exists in Customer Type!");
					}
					
					if(groupDataVal==false&&typeDataVal==true&&categoryDataVal==true){
						form.showDialogMessage("Lead Group value does not exists in Customer Group!");
					}
					
					if(priorityDataVal==false&&groupDataVal==true&&typeDataVal==true&&categoryDataVal==true){
						form.showDialogMessage("Lead Priority value does not exists in Customer Priority!");
					}
					
					if(priorityDataVal==true&&groupDataVal==true&&typeDataVal==true&&categoryDataVal==true){
						switchToLeadScreen(validateCopyingForData);
					}
					
				}
				else{
					switchToLeadScreen(validateCopyingForData);
				}
			}
			
			private void switchToLeadScreen(final boolean statusValue)
			{
				Screen s = (Screen) AppMemory.getAppMemory().currentScreen;
				String currentScrType=s.toString();
				System.out.println("Screen Type"+currentScrType);

				if(AppConstants.SCREENTYPESALESCUST.equals(currentScrType)){
					AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Lead",Screen.SALESLEAD);
					final Lead lead=new Lead();
					lead.setBranch(model.getBranch());
					lead.setEmployee(model.getEmployee());
					
					PersonInfo personInfo=new PersonInfo();
					personInfo.setCellNumber(model.getCellNumber1());
					personInfo.setCount(model.getCount());
					if(model.isCompany()==true){
					   personInfo.setFullName(model.getCompanyName());
					}
					else{
						model.fillAllNames();
						personInfo.setFullName(model.getFullname());
					}
					personInfo.setPocName(model.getFullname());
					lead.setPersonInfo(personInfo);
				    final LeadForm form=LeadPresenter.initalize();
					form.showWaitSymbol();
					Timer t = new Timer() {
					      @Override
					      public void run() {
					    	  form.setToNewState();
					    	  form.hideWaitSymbol();
					    	  form.updateView(lead);
					    	  
					    	  if(model.getStatus().trim().equals(AppConstants.ACTIVE)){
					    		  form.getPic().getId().getElement().addClassName("personactive");
					    		  form.getPic().getName().getElement().addClassName("personactive");
					    		  form.getPic().getPhone().getElement().addClassName("personactive");
					    		  form.getPic().getTbpocName().getElement().addClassName("personactive");
					    	  }
					    	  if(statusValue==true)
					    	  {
					    		  form.getOlbLeadCategory().setValue(model.getCategory());
					    		  form.getOlbLeadGroup().setValue(model.getGroup());
					    		  form.getOlbcLeadType().setValue(model.getType());
					    		  form.getOlbcLeadPriority().setValue(model.getCustomerPriority());
					    	  }
					    	  
					    	  /**
								 * date 26/10/2017 added by komal for lead Project coordinator
								 * dropdowns
								 **/
					    	  AppUtility.makeContactPersonListBoxLive(form.getOlbArchitect(), "Architect", form.getPic().getIdValue());
					    	  AppUtility.makeContactPersonListBoxLive(form.getOlbPMC(),"PMC", form.getPic().getIdValue());
							  AppUtility.makeContactPersonListBoxLive(form.getOlbContractor(), "Contractor", form.getPic().getIdValue());

					    	  form.getPic().setEnabled(false);
					    	  form.getTbLeadId().setText("");
					    	  
					    	  /*
					    	   * nidhi
					    	   * 31-07-2018
					    	   * for employee set
					    	   * 
					    	   */
					    	  form.getTbCreatedBy().setValue(LoginPresenter.loggedInUser);
					    	  form.getOlbbBranch().setValue(model.getBranch());
					    	  Branch branchEntity =form.getOlbbBranch().getSelectedItem();
					    	  if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
									form.olbcNumberRange.setValue(branchEntity.getNumberRange());
									Console.log("Number range set to drop down by default");
					    	  }else {
					    		  	String range=LoginPresenter.branchWiseNumberRangeMap.get(model.getBranch());
							  		Console.log("LoginPresenter.branchWiseNumberRangeMap size="+LoginPresenter.branchWiseNumberRangeMap.size());	
									if(range!=null&&!range.equals("")) {
										form.olbcNumberRange.setValue(range);
										Console.log("in else Number range set to drop down by default");							
									}else {
										form.olbcNumberRange.setSelectedIndex(0);
										Console.log("could not set number range");
									}					
					    	  }
					      }
					    };
					    t.schedule(5000);
					
				
				}
				
				if(AppConstants.SCREENTYPESERVICECUST.equals(currentScrType)){
					AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Lead",Screen.LEAD);
					final Lead lead=new Lead();
					lead.setBranch(model.getBranch());
					lead.setEmployee(model.getEmployee());
					PersonInfo personInfo=new PersonInfo();
					personInfo.setCellNumber(model.getCellNumber1());
					personInfo.setCount(model.getCount());
					if(model.isCompany()==true){
					   personInfo.setFullName(model.getCompanyName());
					}
					else{
						model.fillAllNames();
						personInfo.setFullName(model.getFullname());
					}
					personInfo.setPocName(model.getFullname());
					lead.setPersonInfo(personInfo);
				    final LeadForm form=LeadPresenter.initalize();
					form.showWaitSymbol();
					Timer t = new Timer() {
					      @Override
					      public void run() {
					    	  form.setToNewState();
					    	  form.hideWaitSymbol();
					    	  form.updateView(lead);
					    	  
					    	  form.getPic().setEnabled(false);
					    	  
					    	  if(model.getStatus().trim().equals(AppConstants.ACTIVE)){
					    		  form.getPic().getId().getElement().addClassName("personactive");
					    		  form.getPic().getName().getElement().addClassName("personactive");
					    		  form.getPic().getPhone().getElement().addClassName("personactive");
					    		  form.getPic().getTbpocName().getElement().addClassName("personactive");
					    	  }
					    	  
					    	  if(statusValue==true)
					    	  {
					    		  form.getOlbLeadCategory().setValue(model.getCategory());
					    		  form.getOlbLeadGroup().setValue(model.getGroup());
					    		  form.getOlbcLeadType().setValue(model.getType());
					    		  form.getOlbcLeadPriority().setValue(model.getCustomerPriority());
					    	  }
					    	  
					    	  /**Date 11-9-2019 by Amol mapping the fields from customer  Raised by Rohan Sir**/
					    	  form.getOlbbBranch().setValue(model.getBranch());
					    	  form.getOlbLeadCategory().setValue(model.getCategory());
					    	  form.getOlbcLeadType().setValue(model.getType());
					    	  form.getOlbLeadGroup().setValue(model.getGroup());
					    	  form.getOlbeSalesPerson().setValue(model.getEmployee());
					    	  
					    	  form.getTbLeadId().setText("");
					    	  
					    	  /*
					    	   * nidhi
					    	   * 31-07-2018
					    	   * for employee set
					    	   * 
					    	   */
					    	  form.getTbCreatedBy().setValue(LoginPresenter.loggedInUser);
					    	  form.getOlbbBranch().setValue(model.getBranch());
					    	  Branch branchEntity =form.getOlbbBranch().getSelectedItem();
					    	  if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
									form.olbcNumberRange.setValue(branchEntity.getNumberRange());
									Console.log("Number range set to drop down by default");
					    	  }else {
					    		  	String range=LoginPresenter.branchWiseNumberRangeMap.get(model.getBranch());
							  		Console.log("LoginPresenter.branchWiseNumberRangeMap size="+LoginPresenter.branchWiseNumberRangeMap.size());	
									if(range!=null&&!range.equals("")) {
										form.olbcNumberRange.setValue(range);
										Console.log("in else Number range set to drop down by default");							
									}else {
										form.olbcNumberRange.setSelectedIndex(0);
										Console.log("could not set number range");
									}					
					    	  }
					      }
					    };
					    t.schedule(5000);
					
				
				}
			}
			
			
			private void reactToCreateQuotation()
			{
				boolean validateCopyingForData=false;
				Screen s = (Screen) AppMemory.getAppMemory().currentScreen;
				String currentScrType=s.toString();
				
				if(AppConstants.SCREENTYPESALESCUST.equals(currentScrType)){
					validateCopyingForData=CopyDataConfig.validateDataCopying(AppConstants.SALESQUOTATIONCOPYDATA);
				}

				if(AppConstants.SCREENTYPESERVICECUST.equals(currentScrType)){
					validateCopyingForData=CopyDataConfig.validateDataCopying(AppConstants.QUOTATIONCOPYDATA);
				}
				
				
				boolean categoryDataVal=false;
				boolean typeDataVal=false;
				boolean groupDataVal=false;
				boolean priorityDataVal=false;
				
				
				
				if(validateCopyingForData==true)
				{
					if(AppConstants.SCREENTYPESALESCUST.equals(currentScrType)){
						categoryDataVal=CopyDataConfig.validateData(AppConstants.COPYCATEGORYDATA, model.getCategory(), Screen.SALESQUOTATIONCATEGORY);
						typeDataVal=CopyDataConfig.validateData(AppConstants.COPYTYPEDATA, model.getType(), Screen.SALESQUOTATIONTYPE);
						groupDataVal=CopyDataConfig.validateData(AppConstants.COPYGROUPDATA, model.getGroup(), Screen.SALESQUOTATIONGROUP);
						priorityDataVal=CopyDataConfig.validateData(AppConstants.COPYPRIORITYDATA, model.getCustomerPriority(), Screen.SALESQUOTATIONPRIORITY);
					}
					
					if(AppConstants.SCREENTYPESERVICECUST.equals(currentScrType)){
						categoryDataVal=CopyDataConfig.validateData(AppConstants.COPYCATEGORYDATA, model.getCategory(), Screen.QUOTATIONCATEGORY);
						typeDataVal=CopyDataConfig.validateData(AppConstants.COPYTYPEDATA, model.getType(), Screen.QUOTATIONTYPE);
						groupDataVal=CopyDataConfig.validateData(AppConstants.COPYGROUPDATA, model.getGroup(), Screen.QUOTATIONGROUP);
						priorityDataVal=CopyDataConfig.validateData(AppConstants.COPYPRIORITYDATA, model.getCustomerPriority(), Screen.QUOTATIONPRIORITY);
					}
					
					
					
					if(categoryDataVal==false){
						form.showDialogMessage("Quotation Category Value does not exists in Customer Category!");
					}
					
					if(categoryDataVal==true&&typeDataVal==false){
						form.showDialogMessage("Quotation Type value does not exists in Customer Type!");
					}
					
					if(groupDataVal==false&&typeDataVal==true&&categoryDataVal==true){
						form.showDialogMessage("Quotation Group value does not exists in Customer Group!");
					}
					
					if(priorityDataVal==false&&groupDataVal==true&&typeDataVal==true&&categoryDataVal==true){
						form.showDialogMessage("Quotation Priority value does not exists in Customer Priority!");
					}
					
					if(priorityDataVal==true&&groupDataVal==true&&typeDataVal==true&&categoryDataVal==true){
						switchToQuotationScreen(validateCopyingForData);
					}
					
				}
				else{
					switchToQuotationScreen(validateCopyingForData);
				}
			}
			
			
			
			private void switchToQuotationScreen(final boolean statusValue)
			{
				Screen s = (Screen) AppMemory.getAppMemory().currentScreen;
				String currentScrType=s.toString();
				System.out.println("Screen Type"+currentScrType);
				
				if(AppConstants.SCREENTYPESALESCUST.equals(currentScrType)){
					AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Quotation",Screen.SALESQUOTATION);
					final SalesQuotationForm form=SalesQuotationPresenter.initalize();
					form.showWaitSymbol();
					Timer t = new Timer() {
					      @Override
					      public void run() {
					    	  form.setToNewState();
					    	  form.hideWaitSymbol();
					    	  PersonInfo personInfo=new PersonInfo();
								personInfo.setCellNumber(model.getCellNumber1());
								personInfo.setCount(model.getCount());
								
								
								// rohan added this code for setting customer branches in schedule popup
									QuotationPresenter.custcount=model.getCount();
								// ends here 
								
								if(model.isCompany()==true)
									   personInfo.setFullName(model.getCompanyName());
									else
									{
										model.fillAllNames();
										personInfo.setFullName(model.getFullname());
										 
									}
								personInfo.setPocName(model.getFullname());
								
								  if(model.getStatus().trim().equals(AppConstants.ACTIVE)){
									  form.getPersonInfoComposite().getId().getElement().addClassName("personactive");
						    		  form.getPersonInfoComposite().getName().getElement().addClassName("personactive");
						    		  form.getPersonInfoComposite().getPhone().getElement().addClassName("personactive");
						    		  form.getPersonInfoComposite().getTbpocName().getElement().addClassName("personactive");
						    	  }
								
							  if(!retrCompnyState.trim().equals(model.getSecondaryAdress().getState().trim())&&!model.getSecondaryAdress().getState().trim().equals("")){
							    	 form.getCbcformlis().setEnabled(true);
							      }
							  
							  
							  if(statusValue==true)
					    	  {
					    		  form.getOlbQuotationCategory().setValue(model.getCategory());
					    		  form.getOlbQuotationGroup().setValue(model.getGroup());
					    		  form.getOlbQuotationType().setValue(model.getType());
					    		  form.getOlbcPriority().setValue(model.getCustomerPriority());
					    	  }
					    	  
							  
							  form.getPersonInfoComposite().setValue(personInfo);
							  form.getOlbbBranch().setValue(model.getBranch());
							  form.getOlbeSalesPerson().setValue(model.getEmployee());
							  form.getTbQuotationStatus().setValue(SalesQuotation.CREATED);
							  form.getAcshippingcomposite().setValue(model.getSecondaryAdress());
					    	  form.getTbLeadId().setText("");
					    	  form.getTbQuotatinId().setText("");
					    	  form.getTbContractId().setText("");
					    	  form.getPersonInfoComposite().setEnabled(false);
					    	  /**Date 6-9-2019 by Amol map the category and type of customer **/
					    	  if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "MapCustomerCategoryAndTypeaAsaDocumentCategoryAndType")){
									custCatandType=true;
								}
					    	  if(custCatandType){
					    	  form.getOlbQuotationCategory().setValue(model.getCategory());
					    	  form.getOlbQuotationType().setValue(model.getType());
					    	  }
					    	  
								AppUtility.makeCustomerBranchLive(form.olbcustBranch,form.getPersonInfoComposite().getIdValue());

					        
					      }
					    };
					    t.schedule(5000);
				}
				
				if(AppConstants.SCREENTYPESERVICECUST.equals(currentScrType)){
					AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Quotation",Screen.QUOTATION);
					final QuotationForm form=QuotationPresenter.initalize();
					//form.setToViewState();
					form.showWaitSymbol();
					
					Timer t = new Timer() {
					      @Override
					      public void run() {
					    	  form.setToNewState();
					    	  form.hideWaitSymbol();
					    	  PersonInfo personInfo=new PersonInfo();
								personInfo.setCellNumber(model.getCellNumber1());
								personInfo.setCount(model.getCount());
								if(model.isCompany()==true)
									   personInfo.setFullName(model.getCompanyName());
									else
									{
										model.fillAllNames();
										personInfo.setFullName(model.getFullname());
										 
									}
								personInfo.setPocName(model.getFullname());
								
								if(model.getStatus().equals(AppConstants.ACTIVE)){
								form.getPersonInfoComposite().getId().getElement().addClassName("personactive");
					    		  form.getPersonInfoComposite().getName().getElement().addClassName("personactive");
					    		  form.getPersonInfoComposite().getPhone().getElement().addClassName("personactive");
					    		  form.getPersonInfoComposite().getTbpocName().getElement().addClassName("personactive");
								}
								
								 if(statusValue==true)
						    	  {
						    		  form.getOlbQuotationCategory().setValue(model.getCategory());
						    		  form.getOlbQuotationGroup().setValue(model.getGroup());
						    		  form.getOlbQuotationType().setValue(model.getType());
						    		  form.getOlbcPriority().setValue(model.getCustomerPriority());
						    	  }
								
								 form.checkCustomerBranch(model.getCount());
							  form.getPersonInfoComposite().setValue(personInfo);
							  form.getOlbbBranch().setValue(model.getBranch());
							  form.getOlbeSalesPerson().setValue(model.getEmployee());
							  form.getTbQuotationStatus().setValue(Quotation.CREATED);
					    	  form.getTbLeadId().setText("");
					    	  form.getTbQuotatinId().setText("");
					    	  form.getTbContractId().setText("");
					    	  form.getPersonInfoComposite().setEnabled(false);
					    	  
					    	  
					    	  /**Date 11-9-2019 by Amol mapping the fields from customer  Raised by Rohan Sir**/
					    	  form.getOlbbBranch().setValue(model.getBranch());
					    	  form.getOlbQuotationCategory().setValue(model.getCategory());
					    	  form.getOlbQuotationType().setValue(model.getType());
					    	  form.getOlbQuotationGroup().setValue(model.getGroup());
					    	  form.getOlbeSalesPerson().setValue(model.getEmployee());
					    	  
					    	  
					    	  
					    	  
					    	  
					    	  
					    	  
					    	  
					    	  
					    	  /*
					    	   * nidhi
					    	   * 31-07-2018
					    	   * for employee set
					    	   * 
					    	   */
					    	  form.getTbCreatedBy().setValue(LoginPresenter.loggedInUser);
					    	  
					    	  /*** Date 17-11-2018 By Vijay for bug unable add product in quotation **/
					    	  form.checkCustomerStatus(form.getPersonInfoComposite().getIdValue());
					    	 
					    	  form.getOlbbBranch().setValue(model.getBranch());
					    	  Branch branchEntity =form.getOlbbBranch().getSelectedItem();
					    	  if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
									form.olbcNumberRange.setValue(branchEntity.getNumberRange());
									Console.log("Number range set to drop down by default");
					    	  }else {
					    		  	String range=LoginPresenter.branchWiseNumberRangeMap.get(model.getBranch());
							  		Console.log("LoginPresenter.branchWiseNumberRangeMap size="+LoginPresenter.branchWiseNumberRangeMap.size());	
									if(range!=null&&!range.equals("")) {
										form.olbcNumberRange.setValue(range);
										Console.log("in else Number range set to drop down by default");							
									}else {
										form.olbcNumberRange.setSelectedIndex(0);
										Console.log("could not set number range");
									}					
					    	  }
					    	  
					      }
					    };
					    t.schedule(5000);
				
				}
				
			}
			
			
			private void reactToCreateContract()
			{
				
				boolean validateCopyingForData=false;
				Screen s = (Screen) AppMemory.getAppMemory().currentScreen;
				String currentScrType=s.toString();
				
				if(AppConstants.SCREENTYPESALESCUST.equals(currentScrType)){
					validateCopyingForData=CopyDataConfig.validateDataCopying(AppConstants.SALESORDERCOPYDATA);
				}

				if(AppConstants.SCREENTYPESERVICECUST.equals(currentScrType)){
					validateCopyingForData=CopyDataConfig.validateDataCopying(AppConstants.CONTRACTCOPYDATA);
				}
				
				boolean categoryDataVal=false;
				boolean typeDataVal=false;
				boolean groupDataVal=false;
				
				if(validateCopyingForData==true)
				{
					if(AppConstants.SCREENTYPESALESCUST.equals(currentScrType)){
						categoryDataVal=CopyDataConfig.validateData(AppConstants.COPYCATEGORYDATA, model.getCategory(), Screen.SALESORDERCATEGORY);
						typeDataVal=CopyDataConfig.validateData(AppConstants.COPYTYPEDATA, model.getType(), Screen.SALESORDERTYPE);
						groupDataVal=CopyDataConfig.validateData(AppConstants.COPYGROUPDATA, model.getGroup(), Screen.SALESORDERGROUP);
					}
					
					if(AppConstants.SCREENTYPESERVICECUST.equals(currentScrType)){
						categoryDataVal=CopyDataConfig.validateData(AppConstants.COPYCATEGORYDATA, model.getCategory(), Screen.CONTRACTCATEGORY);
						typeDataVal=CopyDataConfig.validateData(AppConstants.COPYTYPEDATA, model.getType(), Screen.CONTRACTTYPE);
						groupDataVal=CopyDataConfig.validateData(AppConstants.COPYGROUPDATA, model.getGroup(), Screen.CONTRACTGROUP);
					}
					
					
					if(categoryDataVal==false&&currentScrType.trim().equals(AppConstants.SCREENTYPESALESCUST)){
						form.showDialogMessage("Sales Order Category Value does not exists in Customer Category!");
					}
					if(categoryDataVal==false&&currentScrType.trim().equals(AppConstants.SCREENTYPESERVICECUST)){
						form.showDialogMessage("Contract Category Value does not exists in Customer Category!");
					}
					
					if(categoryDataVal==true&&typeDataVal==false&&currentScrType.trim().equals(AppConstants.SCREENTYPESALESCUST)){
						form.showDialogMessage("Sales Order Type value does not exists in Customer Type!");
					}
					
					if(categoryDataVal==true&&typeDataVal==false&&currentScrType.trim().equals(AppConstants.SCREENTYPESERVICECUST)){
						form.showDialogMessage("Contract Type value does not exists in Customer Type!");
					}
					
					
					if(groupDataVal==false&&typeDataVal==true&&categoryDataVal==true&&currentScrType.trim().equals(AppConstants.SCREENTYPESALESCUST)){
						form.showDialogMessage("Sales Order Group value does not exists in Customer Group!");
					}
					
					if(groupDataVal==false&&typeDataVal==true&&categoryDataVal==true&&currentScrType.trim().equals(AppConstants.SCREENTYPESERVICECUST)){
						form.showDialogMessage("Contract Group value does not exists in Customer Group!");
					}
					
					
					if(groupDataVal==true&&typeDataVal==true&&categoryDataVal==true){
						switchToContractScreen(validateCopyingForData);
					}
					
				}
				else{
					switchToContractScreen(validateCopyingForData);
				}
			}
			
			private void switchToContractScreen(final boolean statusValue)
			{
				Screen s = (Screen) AppMemory.getAppMemory().currentScreen;
				String currentScrType=s.toString();
				System.out.println("Screen Type"+currentScrType);
				
				if(AppConstants.SCREENTYPESERVICECUST.equals(currentScrType)){
					AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contract",Screen.CONTRACT);
					final ContractForm form=ContractPresenter.initalize();
					final Contract quot=new Contract();
					form.showWaitSymbol();
					
					Timer t = new Timer() {
					      @Override
					      public void run() {
					    	  form.setToNewState();
					    	  form.hideWaitSymbol();
					    	  form.getTbQuotationStatus().setValue(Contract.CREATED);
					    	  /**
					    	   * Date:19-07-2017 By ANIL
					    	   */
					    	  form.checkCustomerStatus(model.getCount(), true,true);
					    	  /**
					    	   * End
					    	   */
					    	  PersonInfo personInfo=new PersonInfo();
								personInfo.setCellNumber(model.getCellNumber1());
								personInfo.setCount(model.getCount());
								
								// rohan added this code for setting customer branches in schedule popup
									ContractPresenter.custcount=model.getCount();
								// ends here 
								
								if(model.isCompany()==true){
									personInfo.setFullName(model.getCompanyName());
								}
								else{
									model.fillAllNames();
									personInfo.setFullName(model.getFullname());
								}
								personInfo.setPocName(model.getFullname());
								
								if(model.getStatus().equals(AppConstants.ACTIVE)){
									form.getPersonInfoComposite().getId().getElement().addClassName("personactive");
						    		form.getPersonInfoComposite().getName().getElement().addClassName("personactive");
						    		form.getPersonInfoComposite().getPhone().getElement().addClassName("personactive");
						    		form.getPersonInfoComposite().getTbpocName().getElement().addClassName("personactive");
								}
								
								 if(statusValue==true)
						    	  {
						    		  form.getOlbContractCategory().setValue(model.getCategory());
						    		  form.getOlbContractGroup().setValue(model.getGroup());
						    		  form.getOlbContractType().setValue(model.getType());
						    	  }
								 form.checkCustomerBranch(model.getCount());
								
							  form.getPersonInfoComposite().setValue(personInfo);
						      form.getOlbbBranch().setValue(model.getBranch());
						      form.getOlbeSalesPerson().setValue(model.getEmployee());
					    	  form.getTbLeadId().setText("");
					    	  form.getTbQuotatinId().setText("");
					    	  form.getTbContractId().setText("");
					    	  form.getTbReferenceNumber().setText("");
					    	  form.getChkbillingatBranch().setValue(false);
					    	  form.getPersonInfoComposite().setEnabled(false);
					    	  form.getChkservicewithBilling().setValue(false);
					    	  /**
					    	   * Date : 30-10-2017 BY ANIL
					    	   */
					    	  form.getCbServiceWiseBilling().setValue(false);
					    	  /**
					    	   * End
					    	   */
					    	  /**
					    	   * Date 6-9-2019 by Amol
					    	   */
					    	  if(custCatandType){
					    	  form.getOlbContractCategory().setValue(model.getCategory());
					    	  form.getOlbContractType().setValue(model.getType());
					    	  }
					    	  /**
					    	   * End
					    	   */
					    	  /**Date 11-9-2019 by Amol mapping the fields from customer  Raised by Rohan Sir**/
					    	  form.getOlbbBranch().setValue(model.getBranch());
					    	  form.getOlbContractCategory().setValue(model.getCategory());
					    	  form.getOlbContractType().setValue(model.getType());
					    	  form.getOlbContractGroup().setValue(model.getGroup());
					    	  form.getOlbeSalesPerson().setValue(model.getEmployee());
					    	  
					    	  
					    	  
					    	  
					    	  
					    	  /**
					    	   * Date : 24-02-2018
					    	   * map segment and ref number for nbhc and contact list
					    	   * nidhi
					    	   */
					    	  form.getCustomerContactList(model.getCount(),null);
					    	
					    	  /**
					    	   * end
					    	   */
					    	  form.cbConsolidatePrice.setValue(false);
					    	  
					    	  /*
					    	   * nidhi
					    	   * 31-07-2018
					    	   * for employee set
					    	   * 
					    	   */
					    	  form.getTbCreatedBy().setValue(LoginPresenter.loggedInUser);
					    	  
					    	  /**
						  		 * Date 28-09-2018 By Vijay 
						  		 * Des:- NBHC CCPM for every contract this must be true by default it does not allow to change for user
						  		 */
						  		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC")){
						  			form.cbStartOfPeriod.setValue(true);
						  			form.cbStartOfPeriod.setEnabled(false);
						  		}
						  		/**
						  		 * ends here
						  		 */
						  		
						  		/**
						  		 * @author Vijay Date :- 28-01-2021
						  		 * Des :- if Free Material process config is active then stationed Service flag will true 
						  		 * or else it will false. if this flag is true then service will create with Automatic Scheduling 
						  		 */
						  		form.getChkStationedService().setValue(false);
//						  		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICEMODULE, AppConstants.PC_FREEMATERIAL)){
//							  		form.getChkStationedService().setValue(true);
//								}
//						  		else{
//							  		form.getChkStationedService().setValue(false);
//						  		}
						  		
						  		 form.getOlbbBranch().setValue(model.getBranch());
						    	  Branch branchEntity =form.getOlbbBranch().getSelectedItem();
						    	  if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
										form.olbcNumberRange.setValue(branchEntity.getNumberRange());
										Console.log("Number range set to drop down by default");
						    	  }else {
						    		  	String range=LoginPresenter.branchWiseNumberRangeMap.get(model.getBranch());
								  		Console.log("LoginPresenter.branchWiseNumberRangeMap size="+LoginPresenter.branchWiseNumberRangeMap.size());	
										if(range!=null&&!range.equals("")) {
											form.olbcNumberRange.setValue(range);
											Console.log("in else Number range set to drop down by default");							
										}else {
											form.olbcNumberRange.setSelectedIndex(0);
											Console.log("could not set number range");
										}					
						    	  }
					    	 
					      }
					    };
					    t.schedule(5000);
				}
				
				if(AppConstants.SCREENTYPESALESCUST.equals(currentScrType)){
					
					AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Sales Order",Screen.SALESORDER);
					final SalesOrderForm form=SalesOrderPresenter.initalize();
					final SalesOrder soEntity=new SalesOrder();
					form.showWaitSymbol();
					soEntity.setStatus(SalesOrder.CREATED);
					Timer t = new Timer() {
					      @Override
					      public void run() {
					    	  form.setToNewState();
					    	  form.hideWaitSymbol();
					    	  form.getTbQuotationStatus().setValue(SalesOrder.CREATED);
					    	  PersonInfo personInfo=new PersonInfo();
							  personInfo.setCellNumber(model.getCellNumber1());
							  personInfo.setCount(model.getCount());
							  if(model.isCompany()==true){
								   personInfo.setFullName(model.getCompanyName());
							  }
							  else{
								model.fillAllNames();
								personInfo.setFullName(model.getFullname());
							  }
							  
							  personInfo.setPocName(model.getFullname());
							  if(!retrCompnyState.trim().equals(model.getSecondaryAdress().getState().trim())&&!model.getSecondaryAdress().getState().trim().equals("")){
						    	 form.getCbcformlis().setEnabled(true);
						      }
							  
							  if(model.getStatus().equals(AppConstants.ACTIVE)){
							  
								  form.getPersonInfoComposite().getId().getElement().addClassName("personactive");
					    		  form.getPersonInfoComposite().getName().getElement().addClassName("personactive");
					    		  form.getPersonInfoComposite().getPhone().getElement().addClassName("personactive");
					    		  form.getPersonInfoComposite().getTbpocName().getElement().addClassName("personactive");
							  }
							  
							  if(statusValue==true)
					    	  {
					    		  form.getOlbSalesOrderCategory().setValue(model.getCategory());
					    		  form.getOlbSalesOrderGroup().setValue(model.getGroup());
					    		  form.getOlbSalesOrderType().setValue(model.getType());
					    	  }
							  
							  
					    	  form.getPersonInfoComposite().setValue(personInfo);
					    	  form.getOlbbBranch().setValue(model.getBranch());
					    	  form.getOlbeSalesPerson().setValue(model.getEmployee());
					    	  form.getAcshippingcomposite().setValue(model.getSecondaryAdress());
					    	  form.getTbLeadId().setText("");
					    	  form.getTbQuotatinId().setText("");
					    	  form.getTbContractId().setText("");
					    	  form.getTbReferenceNumber().setText("");
					    	  form.getPersonInfoComposite().setEnabled(false);
					    	  
					    	  Branch branchEntity =form.getOlbbBranch().getSelectedItem();
					    	  if(branchEntity.getNumberRange()!=null&&!branchEntity.getNumberRange().equals("")) {
									form.olbcNumberRange.setValue(branchEntity.getNumberRange());
									Console.log("Number range set to drop down by default");
					    	  }else {
					    		  	String range=LoginPresenter.branchWiseNumberRangeMap.get(model.getBranch());
							  		Console.log("LoginPresenter.branchWiseNumberRangeMap size="+LoginPresenter.branchWiseNumberRangeMap.size());	
									if(range!=null&&!range.equals("")) {
										form.olbcNumberRange.setValue(range);
										Console.log("in else Number range set to drop down by default");							
									}else {
										form.olbcNumberRange.setSelectedIndex(0);
										Console.log("could not set number range");
									}					
					    	  }
					      }
					    };
					    t.schedule(5000);
				}
			}
			
			
			private void reactToViewServices()
			{
				Filter filter=new Filter();
				filter.setIntValue(model.getCount());
				filter.setQuerryString("personInfo.count");
				Vector<Filter> vecfilter=new Vector<Filter>();
				vecfilter.add(filter);
				MyQuerry query=new MyQuerry(vecfilter, new Service());
				ServicePresenter.initalize(query);
			}
			
			private void reactToNew()
			{
				form.setToNewState();
				this.initalize();
				form.toggleAppHeaderBarMenu();
			}

			private void validateCust(){
//				if(!form.getTbFirstName().getValue().equals("")&&!form.getTbLastName().getValue().equals("")){
					if(!form.getTbFullName().getValue().equals("")){
					String custnameval=form.getTbFullName().getValue();
					
					custnameval=custnameval.toUpperCase().trim();
					if(custnameval!=null){

							final MyQuerry querry=new MyQuerry();
							Vector<Filter> filtervec=new Vector<Filter>();
							Filter temp=null;
							
							temp=new Filter();
							temp.setQuerryString("isCompany");
							if(form.isCompany.getValue()==false){
								temp.setBooleanvalue(false);
							}
							if(form.isCompany.getValue()==true){
								temp.setBooleanvalue(true);
							}
							filtervec.add(temp);
							
							temp=new Filter();
							temp.setQuerryString("companyId");
							temp.setLongValue(model.getCompanyId());
							filtervec.add(temp);
							
							temp=new Filter();
							temp.setQuerryString("fullname");
							temp.setStringValue(custnameval);
							filtervec.add(temp);
							
							querry.setFilters(filtervec);
							querry.setQuerryObject(new Customer());
							async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("An Unexpected Error occured !");
										
									}
									@Override
									public void onSuccess(ArrayList<SuperModel> result) {
										System.out.println("Success"+result.size());
										if(result.size()!=0){
											form.showDialogMessage("Customer Name Already Exists");
											form.getTbFullName().setValue("");
											
//											form.getTbFirstName().setValue("");
//											form.getTbMiddleName().setValue("");
//											form.getTbLastName().setValue("");
										}
										if(result.size()==0){
//											custflag=true;
										}
									}
								});
					}
				}
			}
			
			private void validateClientName()
			{
				if(form.isCompany.getValue()==true&&form.getTbClientName().getValue().trim()!=null){
					form.showWaitSymbol();
					form.customerFlag = true;
					final MyQuerry querry=new MyQuerry();
					Vector<Filter> filtervec=new Vector<Filter>();
					Filter temp=null;
					
					temp=new Filter();
					temp.setQuerryString("isCompany");
					temp.setBooleanvalue(true);
					filtervec.add(temp);
					
					temp=new Filter();
					temp.setQuerryString("companyName");
					temp.setStringValue(form.getTbClientName().getValue().toUpperCase().trim());
					filtervec.add(temp);
					
					temp=new Filter();
					temp.setQuerryString("companyId");
					temp.setLongValue(model.getCompanyId());
					filtervec.add(temp);
					
					querry.setFilters(filtervec);
					querry.setQuerryObject(new Customer());
					async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.hideWaitSymbol();
								form.showDialogMessage("An Unexpected Error occured !");
								
							}
							@Override
							public void onSuccess(ArrayList<SuperModel> result) {
								System.out.println("Success"+result.size());
								form.hideWaitSymbol();

								if(result.size()!=0){
//									clientflag=false;
									form.showDialogMessage("Client Name Already Exists!");
									form.getTbClientName().setValue("");
								}
								if(result.size()==0){
//									clientflag=true;
								}

							}
							
						});
				}

			}
			
			
			/**
			 *   This method returns the name of state in which the company is located.
			 *   It is retrieved form Company address in Company Entity
			 * @return  It returns company state
			 */
			
			public String retrieveCompanyState()
			{
				final MyQuerry querry=new MyQuerry();
				Filter tempfilter=new Filter();
				tempfilter.setQuerryString("companyId");
				tempfilter.setLongValue(model.getCompanyId());
				querry.getFilters().add(tempfilter);
				querry.setQuerryObject(new Company());
				Timer timer=new Timer() 
			    {
					@Override
					public void run() 
					{
					async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected error occurred!");
						}
			
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							for(SuperModel model:result)
							{
								Company compentity = (Company)model;
								retrCompnyState=compentity.getAddress().getState();
								System.out.println("Retrieved "+retrCompnyState);
							}
							}
						 });
					}
		    	 };
		    	 timer.schedule(3000);
		    	 
		    	return retrCompnyState;
			}
			

			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				
				ScreeenState scr=AppMemory.getAppMemory().currentState;
				
				if(scr.equals(ScreeenState.NEW))
				{
//					if(event.getSource().equals(form.getTbFirstName())){
//						validateCust();
//					}
//					if(event.getSource().equals(form.getTbMiddleName())){
//						validateCust();
//					}
//					if(event.getSource().equals(form.getTbLastName())){
//						validateCust();
//					}
					

					if(event.getSource().equals(form.getTbFullName())){
						if(!form.isCompany.getValue()){
							validateCust();
						}
					}
					
					if(event.getSource().equals(form.getTbClientName())){
						form.customerFlag = false;
						if(form.isCompany.getValue()){
							validateClientName();
						}	
					}
				}
				
			}

			
//			protected boolean validateDataCopying(String typeOfScreenModule)
//			{
//				boolean copyingFlag=false;
//				boolean validatingProcessName=CopyDataConfig.validateCopyData();
//				System.out.println("Process name validation"+validatingProcessName);
//				boolean validatingProcessConfig=false;
//				
//				if(validatingProcessName==true)
//				{
//					validatingProcessConfig=CopyDataConfig.validateProcessConfig(typeOfScreenModule.trim());
//					System.out.println("Inside PC Copy"+validatingProcessConfig);
//				}
//				
//				System.out.println("PCONFIG"+validatingProcessConfig);
//				
//				if(validatingProcessName==true){
//					
//					if(validatingProcessConfig==true){
//						copyingFlag=true;
//					}
//				}
//				
//				return copyingFlag;
//			}
			
			
			@Override
			public void onClick(ClickEvent event) {
				super.onClick(event);
				
				
				if(event.getSource().equals(letterpopup.getForBank()))
				{
					
					letterpopup.getForAll().setValue(false);
					letterpopup.getForBank().setValue(true);
				}
				
				if(event.getSource().equals(letterpopup.getForAll()))
				{
					
					letterpopup.getForAll().setValue(true);
					letterpopup.getForBank().setValue(false);
				}
				
				if(event.getSource().equals(letterpopup.getBtnOk()))
				{
					
					if(letterpopup.getForBank().getValue()==true||letterpopup.getForAll().getValue()==true)
					{
					System.out.println("in side ok");
					if(letterpopup.getForBank().getValue()==true)
					{
						
						String bank = letterpopup.getRefNo().getValue()+"$"+letterpopup.getRefDate().getValue()+"$"+letterpopup.getDesignation().getValue()+"$"+"bank";
						final String url =com.google.gwt.core.client.GWT.getModuleBaseURL() + "pdfreference"+"?Id="+model.getId()+"&"+"type="+bank;
						Window.open(url, "test", "enabled");
					}
					else
					{
						String all = letterpopup.getRefNo().getValue()+"$"+letterpopup.getRefDate().getValue()+"$"+letterpopup.getDesignation().getValue()+"$"+"all";
						final String url =com.google.gwt.core.client.GWT.getModuleBaseURL() + "pdfreference"+"?Id="+model.getId()+"&"+"type="+all;
						Window.open(url, "test", "enabled");
					}
					
					letterpanel.hide();
					}
					
				else
					{
						form.showDialogMessage("Please Select bank or office");
					}
				}
				
				if(event.getSource().equals(letterpopup.getBtnCancel()))
				{
					System.out.println("inside cancel");
					
					    letterpanel.hide();
				}
				
				/**
				 * Date 20-03-2018 By vijay
				 * this code was in form and moved here and added also added customer duplication checking
				 */
				
				if(event.getSource() == form.isCompany){
					
					ScreeenState scr=AppMemory.getAppMemory().currentState;
						
						if(form.isCompany.getValue()==true){
							form.tbClientName.setEnabled(true);
							
						}
						if(form.isCompany.getValue()==false){
							form.tbClientName.setValue("");
							form.tbClientName.setEnabled(false);
							
							/**
							 * Date 20-03-2018 By vijay for checking customer exist or not on change of iscompany checkbox
							 */
							if(scr.equals(ScreeenState.NEW) || scr.equals(ScreeenState.EDIT))
							{
								if(form.tbFullName.getValue()!=null && !form.tbFullName.getValue().equals(""))
									validateCust();
							}
							/**
							 * ends here
							 */
						}
				}
				/**
				 * ends here
				 */
				
				
				if(event.getSource().equals(conditionPopup.getBtnOne())){
					Console.log("Inside yes condition to update services");
					panel.hide();
					reactonSave(true);
				}
				
				if(event.getSource().equals(conditionPopup.getBtnTwo())){
					Console.log("inside no condition");
					panel.hide();
					reactonSave(false);
				}
				
				if(event.getSource().equals(resetPopup.getBtnOk())){
					if(resetPopup.getTxtAppId().getValue()==null||resetPopup.getTxtAppId().getValue().equals("")) {
						form.showDialogMessage("Enter Appid");
						return;
					}
					if(resetPopup.getTxtCompanyId().getValue()==null||resetPopup.getTxtCompanyId().getValue().equals("")) {
						form.showDialogMessage("Enter Company ID");
						return;
					}
					if(resetPopup.getTxtCompanyName().getValue()==null||resetPopup.getTxtCompanyName().getValue().equals("")) {
						form.showDialogMessage("Enter Company Name");
						return;
					}
					
					generatedOTP=(int) Math.floor(100000 + Math.random() * 900000);
					Console.log("generatedOTP="+generatedOTP);
					
					MyQuerry querry = new MyQuerry();
				  	Vector<Filter> filtervec=new Vector<Filter>();
				  	Filter filter = null;
				  	filter = new Filter();
				  	filter.setQuerryString("companyId");
					filter.setLongValue(model.getCompanyId());
					filtervec.add(filter);
					
					querry.setFilters(filtervec);
					querry.setQuerryObject(new Company());
					
					async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
						
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							// TODO Auto-generated method stub
							if(result!=null) {
								companyEntity=(Company)result.get(0);
								Console.log("Company loaded");
								if(companyEntity!=null&&(companyEntity.getCellNumber2()==null||companyEntity.getCellNumber2()==0))
							  	{
							  		form.showDialogMessage("Add Mobile 2 in company settings.");
									return;
							  	}
							  	if(companyEntity!=null &&companyEntity.getWhatsAppApiUrl()!=null && !companyEntity.getWhatsAppApiUrl().equals("") &&
										companyEntity.getWhatsAppInstaceId()!=null && !companyEntity.getWhatsAppInstaceId().equals("")){
									if(companyEntity.isWhatsAppApiStatus()){
										String msg="User "+LoginPresenter.loggedInUser+" wants to reset appId : "+resetPopup.getTxtAppId().getValue()+" company ID : "+resetPopup.getTxtCompanyId().getValue()+" Company Name : "+ resetPopup.getTxtCompanyName().getValue()+". This process will delete entire database. OTP to complete this process is "+generatedOTP+". Provide OTP only if you want to proceed the deletion.";
										smsService.sendMessageOnWhatsApp(model.getCompanyId(), companyEntity.getCellNumber2()+"", msg, new  AsyncCallback<String>() {

											@Override
											public void onFailure(Throwable caught) {
												// TODO Auto-generated method stub
												form.showDialogMessage("Failed");
											}

											@Override
											public void onSuccess(String result) {
												// TODO Auto-generated method stub
												Console.log("in success");
												if(result!=null)
													Console.log("result="+result);
												if(result!=null&&result.contains("success")) {						
													otpPopup.clearOtp();
													otppopupPanel = new PopupPanel(true);
													otppopupPanel.add(otpPopup);
													otppopupPanel.show();
													otppopupPanel.center();
												}else {
													form.showDialogMessage(result);
//													resetPopupPanel.hide();
													return;
												}
													
											}
										} );
									}
									else{
										form.showDialogMessage("WhatsApp integration status is inactive");
										return;
									}
								}
								else{
									form.showDialogMessage("WhatsApp integration is not defined");
									return;
								}
							}
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}
					});
				  	
				  	Console.log("company Id="+companyEntity.getCompanyId());
				  	
				  	
				  	
				  	
					
				}
				
				if(event.getSource().equals(resetPopup.getBtnCancel())){
					resetPopupPanel.hide();
				}
				if(event.getSource().equals(otpPopup.getBtnOne())){

					Console.log("otp popup ok button clicked");
					Console.log("otp value="+otpPopup.getOtp().getValue());
					if(otpPopup.getOtp().getValue()!=null){
						if(generatedOTP==otpPopup.getOtp().getValue()){
							resetAppidFinalCall(Long.parseLong(resetPopup.getTxtCompanyId().getValue()), resetPopup.getTxtAppId().getValue(), resetPopup.getTxtCompanyName().getValue(), LoginPresenter.loggedInUser, generatedOTP);
							otppopupPanel.hide();
							resetPopupPanel.hide();
						}else {
							form.showDialogMessage("Invalid OTP");	
							smsService.checkConfigAndSendWrongOTPAttemptMessage(model.getCompanyId(), LoginPresenter.loggedInUser, generatedOTP, "Reset Appid", new  AsyncCallback<String>() {

								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									form.showDialogMessage("Failed");
								}

								@Override
								public void onSuccess(String result) {
									// TODO Auto-generated method stub
									Console.log("InvalidOTPDownloadNotification result="+result);
								}
							} );
						
						}
						
					}else
						form.showDialogMessage("Enter OTP");
				
					
				}
				if(event.getSource() == otpPopup.getBtnTwo()){
					otppopupPanel.hide();
				}
				if(event.getSource().equals(changeCustomerIdPopup.getBtnOk())){
					if(changeCustomerIdPopup.getTxtAppId().getValue()==null||changeCustomerIdPopup.getTxtAppId().getValue().equals("")) {
						form.showDialogMessage("Enter Customer Id");
						return;
					}
					taxAsync.UpdateCustomerIdInAllDocs(model, changeCustomerIdPopup.getTxtAppId().getValue(), LoginPresenter.loggedInUser, new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.showDialogMessage("Failed to update customer id");
						}

						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							if(result!=null)
								form.showDialogMessage(result);
							else
								form.showDialogMessage("Failed to update customer id in all docs");
						}
					});
				}
				if(event.getSource().equals(changeCustomerIdPopup.getBtnCancel())){
					changeCustomerIdPopupPanel.hide();
				}
			}

			public static boolean isButtonStatus() {
				return buttonStatus;
			}

			public static void setButtonStatus(boolean buttonStatus) {
				CustomerPresenter.buttonStatus = buttonStatus;
			}


			/**
			 * Anil added this code for customer approval process on Date : 15-05-2017
			 * @param approval
			 * @param requested
			 * @param string
			 */
			
			private void saveApproval(Approvals approval, final String requested,	final String string) {
				
				async.save(approval, new AsyncCallback<ReturnFromServer>() {
					@Override
					public void onSuccess(ReturnFromServer result) 
					{
						model.setStatus(requested);
						async.save(model, new AsyncCallback<ReturnFromServer>() {
							@Override
							public void onFailure(Throwable caught) {
								form.hideWaitSymbol();
							}
							@Override
							public void onSuccess(ReturnFromServer result) {
								form.setToViewState();
								form.tbCustomerStatus.setValue(model.getStatus());
								form.showDialogMessage(string);
//								form.setMenuAsPerStatus();
								form.toggleProcessLevelMenu();
								form.setAppHeaderBarAsPerStatus();
								form.hideWaitSymbol();
							}
						});
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.showDialogMessage("Request Failed! Try Again!");
						form.hideWaitSymbol();
					}
				});
			}
			
			public void reactOnApprovalEmail(Approvals approvalEntity)
			{
				emailService.initiateApprovalEmail(approvalEntity,new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Unable To Send Email");
						caught.printStackTrace();
					}

					@Override
					public void onSuccess(Void result) {
//						Window.alert("Email Sent Sucessfully !");
						
					}
				});
			}

			@Override
			public void reactOnSave() {
//				super.reactOnSave();
				final String serviceAddress = model.getSecondaryAdress().getCompleteAddress();
				Console.log("serviceAddress"+serviceAddress);

				if(validateAndUpdateModel())
				{
					//Ashwini Patil added two more conditions to avoid arrayindexOutOfBound exception
					if(form.uploadLOI.getValue()!=null&& form.uploadLOI.getValue().getUrl()!=null && !form.uploadLOI.getValue().getUrl().equals("")){
						System.out.println("URL "+form.uploadLOI.getValue().getUrl());
						Console.log("form.uploadLOI.getValue() == "+form.uploadLOI.getValue());
						Console.log("URL == "+form.uploadLOI.getValue().getUrl());
						commonserviceasync.validateUploadedFileSize(form.uploadLOI.getValue(), model.getCompanyId(), new AsyncCallback<String>() {

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								
							}

							@Override
							public void onSuccess(String result) {
								// TODO Auto-generated method stub
								if(result!=null && !result.equals("")){
									form.showDialogMessage(result);
								}
								else{
//									reactonSave();
									
									Address serviceaddress = form.getServiceAddressComposite().getValue();
									
									if(model.getId()!=null) {
										if(!serviceAddress.equals(serviceaddress.getCompleteAddress())){
											panel = new PopupPanel(true);
											panel.add(conditionPopup);
											panel.setGlassEnabled(true);
											panel.show();
											panel.center();
										}else
											reactonSave(false);
									
									}
									else{
										reactonSave(false);
									}
									
								}
							}
						});
					}
					else{
						
						Address serviceaddress = form.getServiceAddressComposite().getValue();
						Console.log("serviceaddress.getCompleteAddress()"+serviceaddress.getCompleteAddress());
						if(model.getId()!=null) {
							if(!serviceAddress.equals(serviceaddress.getCompleteAddress())){
								panel = new PopupPanel(true);
								panel.add(conditionPopup);
								panel.setGlassEnabled(true);
								panel.show();
								panel.center();
							}
							else{
								reactonSave(false);
							}
						}else
							reactonSave(false);
						
						
						
					}
					
					
					
				}
				
				
			
			}

			private void reactonSave(final boolean serviceAddressUpdationFlag) {
				
				Console.log("In reactonsave() of customer");
				// Async querry is going to start. We do not want user to do any thing untill it completes.
				view.showWaitSymbol();
				service.save(model,new AsyncCallback<ReturnFromServer>() {
					
					@Override
					public void onSuccess(ReturnFromServer result) {
						Console.log("in save on success result.error="+result.error);
						

							//Async run sucessfully , hide the wait symbol
							view.hideWaitSymbol();
							// set the count returned from server to this model
							model.setCount(result.count);
						   //set the id from server to this model
							model.setId(result.id);
							view.setToViewState();
							//Set count on view
							view.setCount(result.count);
							
							/**
							 * @author Anil , Date : 14-08-2019
							 * customer loading issue if large customer data process configuration is active
							 * For ankita raised by rahul tiwari
							 */
							 if(model instanceof Customer){
								 LoginPresenter.customerCount=model.getCount();
							 }
							 if(result.error==null||result.error.equals(""))
								 view.showDialogMessage("Data Successfully Saved !",GWTCAlert.OPTION_ROUNDED_BLUE ,GWTCAlert.OPTION_ANIMATION);
							 else
								 view.showDialogMessage(result.error,GWTCAlert.OPTION_ROUNDED_BLUE ,GWTCAlert.OPTION_ANIMATION);
							  
							if(serviceAddressUpdationFlag){
								customernamechangeasync.updateCustomerServiceAddress(model.getCompanyId(), model.getCount(), new AsyncCallback<String>() {

									@Override
									public void onFailure(Throwable caught) {
										// TODO Auto-generated method stub
										
									}

									@Override
									public void onSuccess(String result) {
										// TODO Auto-generated method stub
										Console.log("address updation log "+result);
										form.showDialogMessage("Service Addess updation Process Started Successfully.It will take some time to update.!");
									}
								});
							}
						

						
					}
					
					@Override
					public void onFailure(Throwable caught) {
						view.hideWaitSymbol();
						view.showDialogMessage("Data Save Unsuccessful! Try again.");
						caught.printStackTrace();
						
					}
				});
			}
			
			/**
			 * ends here
			 */
			
			
			private void reactonLisenceInfo() {

				final LicenseInfoPopup licensepopup = new LicenseInfoPopup();

				String url=Window.Location.getHref();
				Console.log("URL-------- "+url);
				String[] spliturl=url.split("\\.");
				String companyname=spliturl[0];
				Console.log("COMPANY :: "+companyname);
				companyname=companyname.replace("http://","");
				Console.log("COMPANY1 :: "+companyname);
				IntegrateSyncServiceAsync intAsync=GWT.create(IntegrateSyncService.class);
				String actionTask="";
//				if(model.getStatus().equals(Customer.CUSTOMERACTIVE)){
//					actionTask=AppConstants.GETLICENSEINFO;
//				}
//				else{
//					form.showDialogMessage("No contract found for this customer");
//					return;
//				}
				//Ashwini Patil Date:23-07-2024 commented above code as we have stopped creating contract in eva. We are creating salesorder in zoho and call license update api from there only so customer status in eva does not change to active. It remains created.
				actionTask=AppConstants.GETLICENSEINFO;
				form.showWaitSymbol();
				Console.log("calling getLicenseInfo");
				intAsync.getLicenseInfo(model, actionTask, new AsyncCallback<ArrayList<LicenseDetails>>() {
					
					@Override
					public void onSuccess(ArrayList<LicenseDetails> list) {
						// TODO Auto-generated method stub
						
						if(list.get(0).getContractCount()==-1){
							form.showDialogMessage(list.get(0).getLicenseType()); // here used as message purpose
						}
						else{
							ArrayList<LicenseDetails> activelicenselist = new ArrayList<LicenseDetails>();
							ArrayList<LicenseDetails> expiredlienselist = new ArrayList<LicenseDetails>();
							Date todaysdate = new Date();
							for(LicenseDetails licensedata : list){
								if(licensedata.getEndDate().equals(todaysdate)||licensedata.getEndDate().after(todaysdate)){
									activelicenselist.add(licensedata);
			                    }
								else{
									expiredlienselist.add(licensedata);
								}
							}
							licensepopup.showPopUp();
							licensepopup.getActivelicenseinfo().getDataprovider().setList(activelicenselist);
							licensepopup.getExpiredlicenseinfo().getDataprovider().setList(expiredlienselist);
						}
						form.hideWaitSymbol();
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
					}
				});
				
				
			}
			
			
			
			private void downloadData(ArrayList<Customer> custarray) {

				
				ArrayList<String> customerData = new ArrayList<String>();
				customerData.add("ID");
				customerData.add("CLIENT NAME");
				customerData.add("POC NAME");
				customerData.add("DATE OF ESTABLISHMENT");
				customerData.add("CELL NUMBER");
				customerData.add("EMAIL");
				customerData.add("CUSTOMER GROUP");
				customerData.add("CUSTOMER CATEGORY");
				customerData.add("CUSTOMER TYPE");
				customerData.add("CUSTOMER PRIORITY");
				customerData.add("CUSTOMER LEVEL");
				customerData.add("CREATION DATE");
				customerData.add("SALES PERSON");
				customerData.add("BRANCH");
				customerData.add("CREATED BY");
				customerData.add("BILLING ADDRESS LINE 1");
				customerData.add("BILLING ADDRESS LINE 2");
				customerData.add("BILLING ADDRESS LANDMARK");
				customerData.add("BILLING ADDRESS COUNTRY");
				customerData.add("BILLING ADDRESS STATE");
				customerData.add("BILLING ADDRESS CITY");
				customerData.add("BILLING ADDRESS LOCALITY");
				customerData.add("BILLING ADDRESS PINCODE");
				customerData.add("SHIPPING/SERVICE ADDRESS LINE 1");
				customerData.add("SHIPPING/SERVICE ADDRESS LINE 2");
				customerData.add("SHIPPING/SERVICE ADDRESS LANDMARK");
				customerData.add("SHIPPING/SERVICE ADDRESS COUNTRY");
				customerData.add("SHIPPING/SERVICE ADDRESS STATE");
				customerData.add("SHIPPING/SERVICE ADDRESS CITY");
				customerData.add("SHIPPING/SERVICE ADDRESS LOCALITY");
				customerData.add("SHIPPING/SERVICE ADDRESS PINCODE");
				customerData.add("GSTIN NUMBER");
				customerData.add("GOOGLE PLUS ID");
				customerData.add("FACEBOOK ID");
				customerData.add("TWITTER ID");
				customerData.add("REFERENCE NUMBER 1");
				customerData.add("REFERENCE NUMBER 2");
				customerData.add("WEBSITE");
				customerData.add("STATUS");
//				customerData.add("SEGMENT");

		        int columnCount = customerData.size();

				
				for(Customer cust:custarray)
				{
					customerData.add(cust.getCount()+"");
					customerData.add(cust.getCompanyName());
					customerData.add(cust.getFullname());

					
					if(cust.getDob()!=null){
						customerData.add(AppUtility.parseDate(cust.getDob(),"dd-MMM-yyyy"));
					}
					else{
						customerData.add("");
					}
					customerData.add(cust.getCellNumber1()+"");

					
					if(cust.getEmail()!=null){
						customerData.add(cust.getEmail());
					}
					else{
						customerData.add("");
					}
					if(cust.getGroup()!=null&&!cust.getGroup().equals("")){
						customerData.add(cust.getGroup());
					}else{
						customerData.add("");
					}
					if(cust.getCategory()!=null&&!cust.getCategory().equals("")){
						customerData.add(cust.getCategory());
					}else{
						customerData.add("");
					}
					
					if(cust.getType()!=null&&!cust.getType().equals("")){
						customerData.add(cust.getType());
					}else{
						customerData.add("");
					}
					
					if(cust.getCustomerPriority()!=null&&!cust.getCustomerPriority().equals("")){
						customerData.add(cust.getCustomerPriority());
					}else{
						customerData.add("");
					}
					if(cust.getCustomerLevel()!=null&&!cust.getCustomerLevel().equals("")){
						customerData.add(cust.getCustomerLevel());
					}else{
						customerData.add("");
					}
					if(cust.getCreationDate()!=null) {
						customerData.add(AppUtility.parseDate(cust.getCreationDate(),"dd-MMM-yyyy"));
					}
					else {
						customerData.add("");
					}

					if(cust.getEmployee()!=null&&!cust.getEmployee().equals("")){
						customerData.add(cust.getEmployee());
					}else{
						customerData.add("");
					}
					
					if(cust.getBranch()!=null&&!cust.getBranch().equals("")){
						customerData.add(cust.getBranch());
					}else{
						customerData.add("");
					}
					
					if(cust.getCreatedBy()!=null&&!cust.getCreatedBy().equals("")){
						customerData.add(cust.getCreatedBy());
					}else{
						customerData.add("");
					}
					customerData.add(cust.getAdress().getAddrLine1());

					if(cust.getAdress().getAddrLine2()!=null&&!cust.getAdress().getAddrLine2().equals("")){
						customerData.add(cust.getAdress().getAddrLine2());
					}
					else{
						customerData.add("");
					}
					if(cust.getAdress().getLandmark()!=null&&!cust.getAdress().getLandmark().equals("")){
						customerData.add(cust.getAdress().getLandmark());
					}
					else{
						customerData.add("");
					}
					if(cust.getAdress().getCountry()!=null&&!cust.getAdress().getCountry().equals("")){
						customerData.add(cust.getAdress().getCountry());
					}
					else{
						customerData.add("");
					}
					if(cust.getAdress().getState()!=null&&!cust.getAdress().getState().equals("")){
						customerData.add(cust.getAdress().getState());
					}
					else{
						customerData.add("");
					}
					if(cust.getAdress().getCity()!=null&&!cust.getAdress().getCity().equals("")){
						customerData.add(cust.getAdress().getCity());
					}
					else{
						customerData.add("");
					}
					if(cust.getAdress().getLocality()!=null&&!cust.getAdress().getLocality().equals("")){
						customerData.add(cust.getAdress().getLocality());
					}
					else{
						customerData.add("");
					}
					customerData.add(cust.getAdress().getPin()+"");
					customerData.add(cust.getSecondaryAdress().getAddrLine1());
					if(cust.getSecondaryAdress().getAddrLine2()!=null&&!cust.getSecondaryAdress().getAddrLine2().equals("")){
						customerData.add(cust.getSecondaryAdress().getAddrLine2());
					}
					else{
						customerData.add("");
					}
					if(cust.getSecondaryAdress().getLandmark()!=null&&!cust.getSecondaryAdress().getLandmark().equals("")){
						customerData.add(cust.getSecondaryAdress().getLandmark());
					}
					else{
						customerData.add("");
					}
					if(cust.getSecondaryAdress().getCountry()!=null&&!cust.getSecondaryAdress().getCountry().equals("")){
						customerData.add(cust.getSecondaryAdress().getCountry());
					}
					else{
						customerData.add("");
					}
					if(cust.getSecondaryAdress().getState()!=null&&!cust.getSecondaryAdress().getState().equals("")){
						customerData.add(cust.getSecondaryAdress().getState());
					}
					else{
						customerData.add("");
					}
					if(cust.getSecondaryAdress().getCity()!=null&&!cust.getSecondaryAdress().getCity().equals("")){
						customerData.add(cust.getSecondaryAdress().getCity());
					}
					else{
						customerData.add("");
					}
					if(cust.getSecondaryAdress().getLocality()!=null&&!cust.getSecondaryAdress().getLocality().equals("")){
						customerData.add(cust.getSecondaryAdress().getLocality());
					}
					else{
						customerData.add("");
					}
					customerData.add(cust.getSecondaryAdress().getPin()+"");
					/**@Sheetal : 02-03-2022
					 *  Des : Adding GSTIN Number column**/
					String artValue  = " ";
					if(cust.getArticleTypeDetails()!=null&&cust.getArticleTypeDetails().size()!=0){
					ArrayList<ArticleType> articleTypeDetails= cust.getArticleTypeDetails();
					for(ArticleType art : articleTypeDetails){
						if(art.getArticleTypeName().equalsIgnoreCase("gstIn")){
							artValue = art.getArticleTypeValue();
						}
					}
					customerData.add(artValue);

					}else{
						customerData.add("");
					}
					/**end**/

					if(cust.getSocialInfo().getGooglePlusId()!=null&&!cust.getSocialInfo().getGooglePlusId().equals("")){
						customerData.add(cust.getSocialInfo().getGooglePlusId());
					}
					else{
						customerData.add("");
					}
					if(cust.getSocialInfo().getFaceBookId()!=null&&!cust.getSocialInfo().getFaceBookId().equals("")){
						customerData.add(cust.getSocialInfo().getFaceBookId());
					}
					else{
						customerData.add("");
					}
					if(cust.getSocialInfo().getTwitterId()!=null&&!cust.getSocialInfo().getTwitterId().equals("")){
						customerData.add(cust.getSocialInfo().getTwitterId());
					}
					else{
						customerData.add("");
					}
					if(cust.getRefrNumber1()!=null&&!cust.getRefrNumber1().equals("")){
						customerData.add(cust.getRefrNumber1());
					}
					else{
						customerData.add("");
					}
					if(cust.getRefrNumber2()!=null&&!cust.getRefrNumber2().equals("")){
						customerData.add(cust.getRefrNumber2());
					}
					else{
						customerData.add("");

					}
					if(cust.getWebsite()!=null&&!cust.getWebsite().equals("")){
						customerData.add(cust.getCustWebsite());
					}
					else{
						customerData.add("");
					}
					customerData.add(cust.getStatus());
					
				}

				
				csvservice.setExcelData(customerData, columnCount, AppConstants.CUSTOMER, new AsyncCallback<String>() {
					
					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						
						String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
						final String url=gwt + "CreateXLXSServlet"+"?type="+17;
						Window.open(url, "test", "enabled");
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				});
				
			}

			private void reactonResetAppid() {
				String msg="This is very critical operation. When we get a new client and if we do not have any new link, we take an old link and delete all its data from the backend. All data from link will be deleted so be very carefull while doing this.";
				resetPopup.getInfo().setText(msg);	
				resetPopup.getInfo().setEnabled(false);
				resetPopup.getBtnOk().setText("Get OTP");
				resetPopupPanel=new PopupPanel(true);
				resetPopupPanel.add(resetPopup);
				resetPopupPanel.center();
				resetPopupPanel.show();
			}
			private void resetAppidFinalCall(long compnayId, String appId, String companyName, String loggedinuser, int otp) {
				Console.log("in resetAppidFinalCall");	
				CompanyRegistrationServiceAsync companyService=GWT.create(CompanyRegistrationService.class);
				companyService.resetAppid(compnayId, appId, companyName, loggedinuser, otp,companyEntity.getCellNumber2()+"", new AsyncCallback<String>() {
					
					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						if(result!=null)
							form.showDialogMessage(result);
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.showDialogMessage("Process Failed. Try again or check logs");
					}
				});
			}
}
