package com.slicktechnologies.client.views.implementation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.NavigationBarItem;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.config.configurations.ConfigForm;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConnector;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.customer.CustomerPresenter;
import com.slicktechnologies.client.views.customerDetails.CustomerDetailsPresenter;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.lead.LeadPresenter;
import com.slicktechnologies.server.api.GetUserRegistrationOtp;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigration;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.UserRole;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class ImplementationPresenter extends
		FormScreenPresenter<DummyEntityOnlyForScreen> {

	ImplementationForm form;
	
	GeneralViewDocumentPopup genralInvoicePopup; 
	public static PopupPanel generalPanel;

	GenricServiceAsync async = GWT.create(GenricService.class);

	CommonServiceAsync commonAsync = GWT.create(CommonService.class);
	
	String viewedScreenName="";//13-10-2022

//	 AccountsModuleProcessConfiguration accountsProcessConfiPopup = new AccountsModuleProcessConfiguration();
	 
//	 ServiceSchedulingProcessConfgsPopup serviceScheduleProcessConfigPopup = new ServiceSchedulingProcessConfgsPopup();
	 
//	 ServiceProcessConfigPopup serviceProcessConfigPopUp = new ServiceProcessConfigPopup();
	 
	CustomerPortalLinkSharePopup custportalPopup=new CustomerPortalLinkSharePopup();
	
	
	public ImplementationPresenter(UiScreen<DummyEntityOnlyForScreen> view,
			DummyEntityOnlyForScreen model) {
		super(view, model);
		
		form=(ImplementationForm) view;
		form.setPresenter(this);
		
		form.company.addClickHandler(this);
		form.Branch.addClickHandler(this);
		form.defineBranchAsCompany.addClickHandler(this);
		form.billing.addClickHandler(this);
		form.nonTaxNumberRange.addClickHandler(this);
		form.user.addClickHandler(this);
		form.serviceProduct.addClickHandler(this);
		form.itemProduct.addClickHandler(this);
		form.enableMakerChecker.addClickHandler(this);
		form.authorization.addClickHandler(this);
		form.restrictUserLocationAccess.addClickHandler(this);
		form.Locality.addClickHandler(this);
		form.city.addClickHandler(this);
		
		form.bank.addClickHandler(this);
		form.branchWiseInvoiceNumberRange.addClickHandler(this);
		form.invoicePrifix.addClickHandler(this);
		form.enableAccountingInterface.addClickHandler(this);
		
		form.enableMakerChecker.addClickHandler(this);
		
		form.cbBranchAsCompany.addClickHandler(this);
		
		form.cbCityNotMandatory.addClickHandler(this); //Date: 22-01-2022 Added By: Ashwini Description: to manage PC_CITY_NOT_MANDATORY process configuration
		
		form.cbNumberRangeMandatory.addClickHandler(this); //@Sheetal:23-02-2022,Des:Adding MakeNumberRangeMandatory process configuration
		
		form.cbBranchWiseOrCompanyWiseNumberRange.addClickHandler(this); //@Sheetal:23-02-2022, Des:Adding NumberRangeProcess process configuration

		form.cbMakeNumberRangeReadOnly.addClickHandler(this);
		
		form.enableAccountingInterface.addClickHandler(this);
		
		form.enableAccountsProcessConfigs.addClickHandler(this);
		
		form.cbOneTimeContract.addClickHandler(this);
		
		form.contractlossRenewalReasons.addClickHandler(this);
		
		form.cbHideBankDetailsonPDF.addClickHandler(this);
		form.quotationMandatory.addClickHandler(this);
		form.cbRenewalReminderToClient.addClickHandler(this);
		form.cbRenewalReminderSMS.addClickHandler(this);		
		form.cbprintServiceAddressOnRenewal.addClickHandler(this);
		form.cbRenewalReminderlistEmail.addClickHandler(this);
		form.cbDoNotPrintDiscountColumnOnRenewalPrint.addClickHandler(this);
		
		form.serviceRescheduleReasons.addClickHandler(this);
		form.serviceRescheduleReasons.addClickHandler(this);
		form.activateSchedulingProcessConfig.addClickHandler(this);
		
		form.activateDeactivateServiceProcessConfigs.addClickHandler(this);
		
		form.outstandingDueList.addClickHandler(this);
		form.cbQuotationSelfApproval.addClickHandler(this);
		
		form.cbNumberRangeMandatoryOnLead.addClickHandler(this);
		form.cbNumberRangeMandatoryOnQuotation.addClickHandler(this);
		
		form.cbContractSelfApproval.addClickHandler(this);
		form.cbSalesQuotationSelfApproval.addClickHandler(this);
		form.cbSalesOrderSelfApproval.addClickHandler(this);
		form.cbSalesOrderMakeReferenceNumberMandatory.addClickHandler(this);
		form.cbPOSelfApproval.addClickHandler(this);
		form.cbServicePOSelfApproval.addClickHandler(this);
		form.cbGRNSelfApproval.addClickHandler(this);
		form.cbMRNSelfApproval.addClickHandler(this);
		form.cbMINSelfApproval.addClickHandler(this);
		form.cbMMNSelfApproval.addClickHandler(this);
		form.cbBillingSelfApproval.addClickHandler(this);
		form.cbVendorInvoiceSelfApproval.addClickHandler(this);
		form.cbPaymentDateMandatory.addClickHandler(this);
		form.cbPaymentReceivedDateMandatory.addClickHandler(this);
		form.cbOnlyForEva.addClickHandler(this);
		form.cbEditApprovedContract.addClickHandler(this);
		form.cbRestrictContractsDownloadWithOTP.addClickHandler(this);
		form.cbFrequencyAtProductLevel.addClickHandler(this);
		form.cbMakeFrequencyMandatoryAtProductLevel.addClickHandler(this);
		form.cbShowDeleteDuplicateServiceButton.addClickHandler(this);
		form.cbPC_DoNotPrintQuantity.addClickHandler(this);
		form.cbPedioContract.addClickHandler(this);
		form.cbMAKEPRODUCTNAMENONEDITABLE.addClickHandler(this);
		form.cbMakeServiceTypeMandatory.addClickHandler(this);
		form.cbRemoveServiceAddressFromBranchPopup.addClickHandler(this);
		form.cbMakeTermsUploadMandatory.addClickHandler(this);
		form.cbPC_DoNotPrintProductWarrantyOnContract.addClickHandler(this);
						
		form.cbSalesPriceMandatory.addClickHandler(this);
		form.cbPurchasePriceMandatory.addClickHandler(this);
		
		form.cbBillSelfApproval.addClickHandler(this);
		form.cbMakeServiceCompletionMandatoryForAMCContract.addClickHandler(this);
		form.cbEnableAutoInvoice.addClickHandler(this);
		form.cbEnableServiceBranch.addClickHandler(this);
		
		form.cbinvoiceSelfApproval.addClickHandler(this);
		form.cbinvoiceAccountingInterface.addClickHandler(this);
		form.cbCompanyAsLetterHead.addClickHandler(this);
		form.cbPrintProductPrimicesinPDF.addClickHandler(this);
		form.cbInvoiceEditAllRoll.addClickHandler(this);
		form.cbmakeAddressToUppercase.addClickHandler(this);
		form.cbPrintPoDetailsOnInvoice.addClickHandler(this);
		form.cbHideRateandDiscountColumn.addClickHandler(this);
		form.cbPrintProductDiscriptionOnPdfInInvoice.addClickHandler(this);
		form.cbPC_DoNot_Print_Services.addClickHandler(this);
		form.cbMakeInvoiceGroupMandatory.addClickHandler(this);
		form.cbPrintDescriptionOnInvoicefromContract.addClickHandler(this);
		form.cbPrintDescriptionOnInvoicefromSalesOrder.addClickHandler(this);
		form.cbPC_NewInvoiceFormat.addClickHandler(this);	
		form.cbGenerateEinvoiceFromReferenceNumber.addClickHandler(this);
		form.cbEnableInvoiceIntegrationWithZohoBooks.addClickHandler(this);

		form.cbpaymentAccoutingInterface.addClickHandler(this);
		form.cbPaymentAddIntoPettyCash.addClickHandler(this);
		form.cbPaymentCompanyAsLetterHead.addClickHandler(this);
		form.cbPrintOrderNoAndGrnNo.addClickHandler(this);
		
		form.cbExpenseAccoutingInterface.addClickHandler(this);
		form.invoicePrifix.addClickHandler(this);
		form.contractRenewallistReminderEmail.addClickHandler(this);
		form.cbquotationMandatory.addClickHandler(this);
		
		form.cbTransferMaterialWhileScheduling.addClickHandler(this);
//		form.cbautoEmailJobCardOnCompletion.addClickHandler(this);
		form.cbComplainServiceWithTurnAroundTime.addClickHandler(this);
		form.cbCloseComplaintWhenServiceClose.addClickHandler(this);
		form.cbSendSrCopyOnServiceCompletion.addClickHandler(this);
		form.cbCustomizedSrCopy.addClickHandler(this);
		form.cbMAKEMATERIALMANDATORY.addClickHandler(this);
		form.cbMakeServiceEditableAfterCompletion.addClickHandler(this);
		form.cbMultipleExpenseMngt.addClickHandler(this);
		form.cbDATETECHNICIANWISESERVICEPOPUP.addClickHandler(this);
		
		form.cbHideCompleteRescheduleFumigationColoumn.addClickHandler(this);
		form.cbHideFumigationButton.addClickHandler(this);
		form.cbCOMPLAINSERVICEWITHTURNAROUNDTIME.addClickHandler(this);
		form.cbEmailCustomizedAndGenericSRCopy.addClickHandler(this);
		form.cbPC_SERVICESCHEDULEMINMAXROUNDUP.addClickHandler(this);
		form.cbPC_HIDE_CS_LIST_PLAN_BUTTON.addClickHandler(this);
		form.cbPC_PedioFindingAsServiceQuantityInBilling.addClickHandler(this);
		form.cbENABLECOMPLETEBUTTONONSCHEDULEPOPUP.addClickHandler(this);
		form.cbHideServiceValueExceptAdmin.addClickHandler(this);
		form.cbRestrictServiceDownloadWithOTP.addClickHandler(this);
		form.cbStopServiceRescheduleEmail.addClickHandler(this);
		form.cbUpdateFindingEntity.addClickHandler(this);
		

		form.cbEnablePedio.addClickHandler(this);
		form.cbReturnQtyUpdate.addClickHandler(this);
		form.cbServiceScheduleMaterailMandatory.addClickHandler(this);
		form.cbCopyMaterialToRemainingServices.addClickHandler(this);
		form.cbEnableFcmNotification.addClickHandler(this);
		form.cbEnableServiceScheduleNotification.addClickHandler(this);

		// 12-09-2022
		form.cbMAKETECHNICIANREMARKMANDITORY.addClickHandler(this);
		form.cbMAKECUSTOMERNAMEMANDITORY.addClickHandler(this);
		form.cbMAKECUSTOMERREMARKMANDITORY.addClickHandler(this);
		form.cbMAKETYPEOFTREATMENTMANDATORY.addClickHandler(this);
		form.cbMAKEFINDINGSMANDATORY.addClickHandler(this);
		form.cbHIDESERVICEFINDING.addClickHandler(this);
		form.cbHIDEMATERIALS.addClickHandler(this);
		form.cbSHOWEXPENSE.addClickHandler(this);
		form.cbDIRECTCOMPLETE.addClickHandler(this);
		form.cbTECHNICIANATTENDANCE.addClickHandler(this);
		form.cbSITEFINDING.addClickHandler(this);
		form.cbREJECTSERVICE.addClickHandler(this);
		form.cbHIDESRS.addClickHandler(this);
		form.cbLIVETRACKING.addClickHandler(this);
		form.cbSENDOTPFORSERVICECOMPLETION.addClickHandler(this);
		form.cbPC_PEDIO_HIDE_TREATMENTS.addClickHandler(this);
		form.cbPC_ADDMIXINGRATIOANDLOTNO.addClickHandler(this);
		form.cbPC_DocumentUploadMandatory.addClickHandler(this);
		form.cbPC_Pedio_Hide_Reschedule.addClickHandler(this);
		form.cbPC_Hide_Customer_Mobile.addClickHandler(this);
		form.cbLoadLoggedInUserWiseCustomer.addClickHandler(this);
		form.cbPREMISESFLAG.addClickHandler(this);
		form.cbMARKSERVICECOMPLETEDFROMAPP.addClickHandler(this);
		form.cbSHOWEMAIL.addClickHandler(this);
		form.cbPEDIO_HIDEPHOTOS.addClickHandler(this);
		form.cbShowProductPremises.addClickHandler(this);
		form.cbPC_DoNotShowRatings.addClickHandler(this);
		form.cbMAKESTARRATINGMANDITORY.addClickHandler(this);
		form.cbPC_HideCustomerRemark.addClickHandler(this);
		
		
		form.cbprintCustomerRemark.addClickHandler(this);
		form.cbPrintTreatmentOnSRCopy.addClickHandler(this);
		form.cbPrintPhotoOnSRCopy.addClickHandler(this);
		form.cbTechnicianRemark.addClickHandler(this);
		form.cbPrintFindingOnSRCopy.addClickHandler(this);
		form.cbPedioNotificationRemark.addClickHandler(this);
		form.cbhideMobileNo.addClickHandler(this);
		form.cbhideCustomerFeedback.addClickHandler(this);
		form.cbUploadSRCopy.addClickHandler(this);
		
		form.cbDecreaseTableRow.addClickHandler(this);
		form.cbHIDEMATERIALANDUOM.addClickHandler(this);
		form.cbPC_HideCellNumber_SRCopy.addClickHandler(this);
		form.cbPC_DoNotPrintMaterialOnSummarySR.addClickHandler(this);
		form.cbPRINTMATERIALONSRCOPY.addClickHandler(this);
		form.cbPRINTPHOTOONSRCOPY.addClickHandler(this);
		
		
		form.cbMakeCustomerEmailMandatory.addClickHandler(this);
		form.cbMakeCustGroupMandatory.addClickHandler(this);
		form.cbMakeCustPinCodeMandatory.addClickHandler(this);
		form.cbMakeCustRefMandatory.addClickHandler(this);
		form.cbMakeCustLevelMandatory.addClickHandler(this);
		form.cbHideCustomerGroup.addClickHandler(this);
		form.cbMakecustCategoryAndTypeDisable.addClickHandler(this);
		form.cbMakeCellNoNonMandatory.addClickHandler(this);
		form.cbMakeAccountManagerMandatory.addClickHandler(this);
		form.cbMakeSalesPersonMandatory.addClickHandler(this);
		form.cbEnableloadLargeCustomerData.addClickHandler(this);
		form.cbHideCustomerReligion.addClickHandler(this);		

		form.cbMakeLeadGroupMandatory.addClickHandler(this);
		form.cbLeadCustPopupEmailMandatory.addClickHandler(this);
		form.cbMakeCustomerPopUpCompanyNameMandatory.addClickHandler(this);
		form.cbMakeCustomerPopUpPhoneNumberMandatory.addClickHandler(this);
		form.cbEnableFreezeLead.addClickHandler(this);
		form.cbCommunicationLogColumn.addClickHandler(this);
		form.cbMapCustomerTypeToLeadCategory.addClickHandler(this);
		form.cbMakePriceColumnDisable.addClickHandler(this);
		form.cbChangeProductComposite.addClickHandler(this);
		form.cbDisableEmailIdValidation.addClickHandler(this);
		form.cbHideNumberRange.addClickHandler(this);//Added by sheetal:15-02-2022,Des:Hiding number range from lead for pecop

		
		form.cbMakeQuotationGroupMandatory.addClickHandler(this);
		form.cbQuotationTypeMandatory.addClickHandler(this);
		form.cbQuotationCategoryMandatory.addClickHandler(this);
		form.cbDonotProductDescription.addClickHandler(this);
		form.cbPrintProductDescriptionOnPDF.addClickHandler(this);
		form.cbMakeQuotationDateDisable.addClickHandler(this);
		form.cbMakeLeadIdMandatory.addClickHandler(this);
		form.cbprintProductRemarkWithPremise.addClickHandler(this);
		form.cbPrintProductPrimiseInQuotationPdf.addClickHandler(this);
		form.cbPC_DoNotPrintProductWarrantyOnQuotation.addClickHandler(this);
		
		
		form.cbMakeContractGroupMandatory.addClickHandler(this);
		form.cbMakeContractTypeMandatory.addClickHandler(this);
		form.cbPrintProductPrimiseInPdf.addClickHandler(this);
		form.cbMakeNumberRangeMandatory.addClickHandler(this);
		form.cbMakeContractDateDisable.addClickHandler(this);
		
		form.serviceCancellationReasons.addClickHandler(this);
		form.cbOutstandingtoClientEmail.addClickHandler(this);
		
		
		form.cbInvoiceSelfApproval.addClickHandler(this);
		form.cbPaymentAccountningInterface.addClickHandler(this);
		
		form.cbInvoiceAccoutingInterface.addClickHandler(this);
		
		form.cbBranchLevelRestriction.addClickHandler(this);
		form.cbCustomerBranchRestriction.addClickHandler(this);
		
		form.typeOfTreatment.addClickHandler(this);
		form.frequency.addClickHandler(this);
		form.findingType.addClickHandler(this);
		form.serviceChecklist.addClickHandler(this);
		
		form.btnSetupPedio.addClickHandler(this);
		form.btnSharePortalLink.addClickHandler(this);
		
		custportalPopup.getLblOk().addClickHandler(this);
		custportalPopup.getLblCancel().addClickHandler(this);
		
		form.cbDisableIPAddressEmail.addClickHandler(this);

	}

		public static ImplementationForm initalize(){
			if(UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")) { //Ashwini Patil Date:25-09-2022
				AppMemory.getAppMemory().setCurrentScreenandChangeProcessName(
				"Settings/Implementation", Screen.IMPLEMENTATIONSCREEN);
				ImplementationForm form = new ImplementationForm();

				ImplementationPresenter presenter = new ImplementationPresenter(form,
				new DummyEntityOnlyForScreen());
				Screen screenName = (Screen) AppMemory.getAppMemory().currentScreen;
				Console.log("screenName ==" + screenName);

				AppMemory.getAppMemory().stickPnel(form);
				Window.alert("All the settings in this implementation screen are applicable to the standard PDFs. It will not be applicable for client specific customized PDFs.");
				return form;
				}else {			
		  			  Window.alert("Login as administartor");
				}
				return null;
		}
	
	

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		Console.log("implementation form click event");
		
		try{
			reactOnPopupNavigation(event);
		}catch(Exception e){
			Console.log("SImplementation on click error"+e.toString());
		}
		
		if (genralInvoicePopup != null) {
			System.out.println("implementation form");
			if (event.getSource() == genralInvoicePopup.btnClose) {
				Console.log("implementation form btn close");

				form.setToNewState();
				genralInvoicePopup.viewDocumentPanel.hide();
				form.toggleAppHeaderBarMenu();
			}
		}

		if (event.getSource() == form.company) {
			reactOnCompany();
		}
		if (event.getSource() == form.Branch) {
			reactonBranch();
		}
		if (event.getSource() == form.user) {
			reactOnUser();
		}
		if (event.getSource() == form.serviceProduct) {
			reactOnServiceProduct();
		}
		if (event.getSource() == form.itemProduct) {
			reactOnItemProduct();
		}
		if (event.getSource() == form.authorization) {
			reactOnUserAutorization();
		}
		if (event.getSource() == form.Locality) {
			reactOnLocality();
		}
		if (event.getSource() == form.city) {
			reactOnCity();
		}
		if (event.getSource() == form.bank) {
			reactOnCompanyBank();
		}

		if (event.getSource() == form.cbBranchAsCompany) {
			reactOnBranchAsCompany();
		}

		if (event.getSource() == form.cbCityNotMandatory) {
			reactOnCityNotMandatory();
		}

		if (event.getSource() == form.cbNumberRangeMandatory) {
			reactOnUpdateProcessConfig("NumberRange",
					"MAKENUMBERRANGEMANDATORY", model.getCompanyId(),
					form.cbNumberRangeMandatory.getValue(),
					form.cbNumberRangeMandatory.getName());
		}

		if (event.getSource() == form.cbBranchWiseOrCompanyWiseNumberRange) {
			reactOnUpdateProcessConfig("NumberRange", "NUMBERRANGEPROCESS",
					model.getCompanyId(),
					form.cbBranchWiseOrCompanyWiseNumberRange.getValue(),
					form.cbBranchWiseOrCompanyWiseNumberRange.getName());
		}

		if (event.getSource() == form.cbMakeNumberRangeReadOnly) {
			reactOnUpdateProcessConfig("NumberRange",
					"MakeNumberRangeReadOnly", model.getCompanyId(),
					form.cbMakeNumberRangeReadOnly.getValue(),
					form.cbMakeNumberRangeReadOnly.getName());
		}

		if (event.getSource() == form.billing) {
			reactOnTaxNumberRange();
		}
		if (event.getSource() == form.nonTaxNumberRange) {
			reactonNonTaxNumberRange();
		}

		if (event.getSource() == form.cbOneTimeContract) {
			reactonSingleOrOneTimeContract();
		}

		if (event.getSource() == form.contractlossRenewalReasons) {
			reactonContractLossRenewalsReasons();
		}

		if (event.getSource() == form.cbHideBankDetailsonPDF) {
			reactOnUpdateProcessConfig("ContractRenewal", "HIDEBANKDETAILS",
					model.getCompanyId(),
					form.cbHideBankDetailsonPDF.getValue(),
					form.cbHideBankDetailsonPDF.getName());

		}
		if (event.getSource() == form.cbquotationMandatory) {
			reactOnUpdateProcessConfig("ContractRenewal",
					"EnableQuotationMandatoryForContractRenwal",
					model.getCompanyId(), form.cbquotationMandatory.getValue(),
					form.cbquotationMandatory.getName());
		}

		if (event.getSource() == form.serviceRescheduleReasons) {
			reactonServiceRescheduleReasons();
		}
		if (event.getSource() == form.serviceCancellationReasons) {
			reactonServiceCancellationReasons();
		}

		if (event.getSource() == form.cbQuotationSelfApproval) {
			reactOnUpdateProcessConfig("Quotation", AppConstants.SELFAPPROVAL,
					model.getCompanyId(),
					form.cbQuotationSelfApproval.getValue(),
					form.cbQuotationSelfApproval.getName());
		}

		if (event.getSource() == form.cbNumberRangeMandatoryOnLead) {
			reactOnUpdateProcessConfig("Lead", "PC_MakeNumberRangeMandatory",
					model.getCompanyId(),
					form.cbNumberRangeMandatoryOnLead.getValue(),
					form.cbNumberRangeMandatoryOnLead.getName());
		}
		if (event.getSource() == form.cbNumberRangeMandatoryOnQuotation) {
			reactOnUpdateProcessConfig("Quotation",
					"PC_MakeNumberRangeMandatory", model.getCompanyId(),
					form.cbNumberRangeMandatoryOnQuotation.getValue(),
					form.cbNumberRangeMandatoryOnQuotation.getName());
		}

		if (event.getSource() == form.cbContractSelfApproval) {
			reactOnUpdateProcessConfig("Contract", AppConstants.SELFAPPROVAL,
					model.getCompanyId(),
					form.cbContractSelfApproval.getValue(),
					form.cbContractSelfApproval.getName());
		}
		if (event.getSource() == form.cbSalesQuotationSelfApproval) {
			reactOnUpdateProcessConfig("SalesQuotation",
					AppConstants.SELFAPPROVAL, model.getCompanyId(),
					form.cbSalesQuotationSelfApproval.getValue(),
					form.cbSalesQuotationSelfApproval.getName());
		}
		if (event.getSource() == form.cbSalesOrderSelfApproval) {
			reactOnUpdateProcessConfig("SalesOrder", AppConstants.SELFAPPROVAL,
					model.getCompanyId(),
					form.cbSalesOrderSelfApproval.getValue(),
					form.cbSalesOrderSelfApproval.getName());
		}
		if (event.getSource() == form.cbSalesOrderMakeReferenceNumberMandatory) {
			reactOnUpdateProcessConfig("SalesOrder", "MakeReferenceNumberMandatory",
					model.getCompanyId(),
					form.cbSalesOrderMakeReferenceNumberMandatory.getValue(),
					form.cbSalesOrderMakeReferenceNumberMandatory.getName());
		}
		if (event.getSource() == form.cbPOSelfApproval) {
			reactOnUpdateProcessConfig("PurchaseOrder",
					AppConstants.SELFAPPROVAL, model.getCompanyId(),
					form.cbPOSelfApproval.getValue(),
					form.cbPOSelfApproval.getName());
		}
		if (event.getSource() == form.cbServicePOSelfApproval) {
			reactOnUpdateProcessConfig("ServicePo", AppConstants.SELFAPPROVAL,
					model.getCompanyId(),
					form.cbServicePOSelfApproval.getValue(),
					form.cbServicePOSelfApproval.getName());
		}
		if (event.getSource() == form.cbGRNSelfApproval) {
			reactOnUpdateProcessConfig("GRN", AppConstants.SELFAPPROVAL,
					model.getCompanyId(), form.cbGRNSelfApproval.getValue(),
					form.cbGRNSelfApproval.getName());
		}
		if (event.getSource() == form.cbMRNSelfApproval) {
			reactOnUpdateProcessConfig("MaterialRequestNote",
					AppConstants.SELFAPPROVAL, model.getCompanyId(),
					form.cbMRNSelfApproval.getValue(),
					form.cbMRNSelfApproval.getName());
		}
		if (event.getSource() == form.cbMINSelfApproval) {
			reactOnUpdateProcessConfig("MaterialIssueNote",
					AppConstants.SELFAPPROVAL, model.getCompanyId(),
					form.cbMINSelfApproval.getValue(),
					form.cbMINSelfApproval.getName());
		}
		if (event.getSource() == form.cbMMNSelfApproval) {
			reactOnUpdateProcessConfig("MaterialMovementNote",
					AppConstants.SELFAPPROVAL, model.getCompanyId(),
					form.cbMMNSelfApproval.getValue(),
					form.cbMMNSelfApproval.getName());
		}
		if (event.getSource() == form.cbBillingSelfApproval) {
			reactOnUpdateProcessConfig("BillingDocument",
					AppConstants.SELFAPPROVAL, model.getCompanyId(),
					form.cbBillingSelfApproval.getValue(),
					form.cbBillingSelfApproval.getName());
		}
		if (event.getSource() == form.cbInvoiceSelfApproval) {
			reactOnUpdateProcessConfig("Invoice", AppConstants.SELFAPPROVAL,
					model.getCompanyId(),
					form.cbInvoiceSelfApproval.getValue(),
					form.cbInvoiceSelfApproval.getName());
		}
		if (event.getSource() == form.cbVendorInvoiceSelfApproval) {
			reactOnUpdateProcessConfig("VendorInvoice",
					AppConstants.SELFAPPROVAL, model.getCompanyId(),
					form.cbVendorInvoiceSelfApproval.getValue(),
					form.cbVendorInvoiceSelfApproval.getName());
		}
		if (event.getSource() == form.cbPaymentDateMandatory) {
			reactOnUpdateProcessConfig("CustomerPayment",
					"PaymentDateMandatory", model.getCompanyId(),
					form.cbPaymentDateMandatory.getValue(),
					form.cbPaymentDateMandatory.getName());
		}
		if (event.getSource() == form.cbPaymentReceivedDateMandatory) {
			reactOnUpdateProcessConfig("CustomerPayment",
					"Paymentreceiveddatemandatory", model.getCompanyId(),
					form.cbPaymentReceivedDateMandatory.getValue(),
					form.cbPaymentReceivedDateMandatory.getName());
		}
		if (event.getSource() == form.cbOnlyForEva) {
			reactOnUpdateProcessConfig("Contract", "ONLYFOREVA",
					model.getCompanyId(), form.cbOnlyForEva.getValue(),
					form.cbOnlyForEva.getName());
		}
		if (event.getSource() == form.cbEditApprovedContract) {
			reactOnUpdateProcessConfig("Contract", "EditApprovedContract",
					model.getCompanyId(),
					form.cbEditApprovedContract.getValue(),
					form.cbEditApprovedContract.getName());
		}
		
		
		if (event.getSource() == form.cbRestrictContractsDownloadWithOTP) {
			reactOnUpdateProcessConfig("Contract", "RestrictContractsDownloadWithOTP",
					model.getCompanyId(),
					form.cbRestrictContractsDownloadWithOTP.getValue(),
					form.cbRestrictContractsDownloadWithOTP.getName());
		}
		
		if (event.getSource() == form.cbFrequencyAtProductLevel) {
			reactOnUpdateProcessConfig("Contract", "FrequencyAtProductLevel",
					model.getCompanyId(),
					form.cbFrequencyAtProductLevel.getValue(),
					form.cbFrequencyAtProductLevel.getName());
		}
		
		if (event.getSource() == form.cbMakeFrequencyMandatoryAtProductLevel) {
			reactOnUpdateProcessConfig("Contract", "MakeFrequencyMandatoryAtProductLevel",
					model.getCompanyId(),
					form.cbMakeFrequencyMandatoryAtProductLevel.getValue(),
					form.cbMakeFrequencyMandatoryAtProductLevel.getName());
			
		}
		
		if (event.getSource() == form.cbShowDeleteDuplicateServiceButton) {
			reactOnUpdateProcessConfig("Contract", "ShowDeleteDuplicateServiceButton",
					model.getCompanyId(),
					form.cbShowDeleteDuplicateServiceButton.getValue(),
					form.cbShowDeleteDuplicateServiceButton.getName());
			
		}
		if (event.getSource() == form.cbPC_DoNotPrintQuantity) {
			reactOnUpdateProcessConfig("Contract", "PC_DoNotPrintQuantity",
					model.getCompanyId(),
					form.cbPC_DoNotPrintQuantity.getValue(),
					form.cbPC_DoNotPrintQuantity.getName());
			
		}
		if (event.getSource() == form.cbPedioContract) {
			reactOnUpdateProcessConfig("Contract", "PedioContract",
					model.getCompanyId(),
					form.cbPedioContract.getValue(),
					form.cbPedioContract.getName());
			
		}
		
		if (event.getSource() == form.cbMAKEPRODUCTNAMENONEDITABLE) {
			reactOnUpdateProcessConfig("Contract", "MAKEPRODUCTNAMENONEDITABLE",
					model.getCompanyId(),
					form.cbMAKEPRODUCTNAMENONEDITABLE.getValue(),
					form.cbMAKEPRODUCTNAMENONEDITABLE.getName());
			
		}
		
		if (event.getSource() == form.cbMakeServiceTypeMandatory) {
			reactOnUpdateProcessConfig("Contract", "MakeServiceTypeMandatory",
					model.getCompanyId(),
					form.cbMakeServiceTypeMandatory.getValue(),
					form.cbMakeServiceTypeMandatory.getName());
			
		}
		if (event.getSource() == form.cbRemoveServiceAddressFromBranchPopup) {
			reactOnUpdateProcessConfig("Contract", "RemoveServiceAddressFromBranchPopup",
					model.getCompanyId(),
					form.cbRemoveServiceAddressFromBranchPopup.getValue(),
					form.cbRemoveServiceAddressFromBranchPopup.getName());			
		}
		
		if (event.getSource() == form.cbMakeTermsUploadMandatory) {
			reactOnUpdateProcessConfig("Contract", "MakeTermsUploadMandatory",
					model.getCompanyId(),
					form.cbMakeTermsUploadMandatory.getValue(),
					form.cbMakeTermsUploadMandatory.getName());			
		}
		
		if (event.getSource() == form.cbPC_DoNotPrintProductWarrantyOnContract) {
			reactOnUpdateProcessConfig("Contract", "PC_DoNotPrintProductWarranty",
					model.getCompanyId(),
					form.cbPC_DoNotPrintProductWarrantyOnContract.getValue(),
					form.cbPC_DoNotPrintProductWarrantyOnContract.getName());			
		}
		/**
		 * @Sheetal:19-03-2022 Des : Added Product tab,requirement by nitin sir
		 **/
		// Product tab
		if (event.getSource() == form.cbSalesPriceMandatory) {
			reactOnUpdateProcessConfig("ItemProduct",
					"PC_ItemProduct_SalesPriceMandatory", model.getCompanyId(),
					form.cbSalesPriceMandatory.getValue(),
					form.cbSalesPriceMandatory.getName());
		}
		if (event.getSource() == form.cbPurchasePriceMandatory) {
			reactOnUpdateProcessConfig("ItemProduct",
					"PC_ItemProduct_PurchasePriceMandatory",
					model.getCompanyId(),
					form.cbPurchasePriceMandatory.getValue(),
					form.cbPurchasePriceMandatory.getName());
		}

		// acounts tab
		if (event.getSource() == form.cbBillSelfApproval) {
			reactOnUpdateProcessConfig("BillingDocument",
					AppConstants.SELFAPPROVAL, model.getCompanyId(),
					form.cbBillSelfApproval.getValue(),
					form.cbBillingSelfApproval.getName());
		}
		if (event.getSource() == form.cbMakeServiceCompletionMandatoryForAMCContract) {
			reactOnUpdateProcessConfig("BillingDocument",
					"MakeServiceCompletionMandatoryForAMCContract",
					model.getCompanyId(),
					form.cbMakeServiceCompletionMandatoryForAMCContract
							.getValue(),
					form.cbMakeServiceCompletionMandatoryForAMCContract
							.getName());
		}
		if (event.getSource() == form.cbEnableAutoInvoice) {
			reactOnUpdateProcessConfig("BillingDocument", "EnableAutoInvoice",
					model.getCompanyId(), form.cbEnableAutoInvoice.getValue(),
					form.cbEnableAutoInvoice.getName());
		}
		if (event.getSource() == form.cbEnableServiceBranch) {
			reactOnUpdateProcessConfig("BillingDocument",
					"EnableServiceBranch", model.getCompanyId(),
					form.cbEnableServiceBranch.getValue(),
					form.cbEnableServiceBranch.getName());
		}
		if (event.getSource() == form.cbExpenseAccoutingInterface) {
			reactOnUpdateProcessConfig("MultipleExpenseMngt",
					AppConstants.PROCESSCONFIGINV, model.getCompanyId(),
					form.cbExpenseAccoutingInterface.getValue(),
					form.cbExpenseAccoutingInterface.getName());
		}

		if (event.getSource() == form.outstandingDueList) {
			cronJobSconfigurationScreen();
		}

		if (event.getSource() == form.invoicePrifix) {
			String url = "https://evasoftwaresolutions.freshdesk.com/support/solutions/articles/47001181936-how-to-set-prefix-in-invoice-number-in-eva-erp";
			Window.open(url, "Invoice Prefix", "enabled");
		}

		if (event.getSource() == form.cbinvoiceSelfApproval) {
			reactOnUpdateProcessConfig("Invoice", AppConstants.SELFAPPROVAL,
					model.getCompanyId(),
					form.cbinvoiceSelfApproval.getValue(),
					form.cbinvoiceSelfApproval.getName());
		}
		if (event.getSource() == form.cbRenewalReminderToClient) {
			reactOnUpdateProcessConfig("CronJob",
					"PC_ContractRenewalReminderToClient", model.getCompanyId(),
					form.cbRenewalReminderToClient.getValue(),
					form.cbRenewalReminderToClient.getName());
		}

		if (event.getSource() == form.contractRenewallistReminderEmail) {
			cronJobSconfigurationScreen();
		}

		if (event.getSource() == form.cbRenewalReminderSMS) {
			reactonRenewalReminderSMS();
		}
		if (event.getSource() == form.cbprintServiceAddressOnRenewal) {
			reactOnUpdateProcessConfig("ContractRenewal", "printServiceAddressOnRenewal",
					model.getCompanyId(),
					form.cbprintServiceAddressOnRenewal.getValue(),
					form.cbprintServiceAddressOnRenewal.getName());
		}
		if (event.getSource() == form.cbDoNotPrintDiscountColumnOnRenewalPrint) {
			reactOnUpdateProcessConfig("ContractRenewal", "DoNotPrintDiscountColumnOnRenewalPrint",
					model.getCompanyId(),
					form.cbDoNotPrintDiscountColumnOnRenewalPrint.getValue(),
					form.cbDoNotPrintDiscountColumnOnRenewalPrint.getName());
		}
		if (event.getSource() == form.cbTransferMaterialWhileScheduling) {
			// reactOnUpdateProcessConfig("CronJob",
			// "ContractRenewalWithoutProcessedCustomerEmail",
			// model.getCompanyId(), form.cbRenewalReminderToClient.getValue());
		}
		if (event.getSource() == form.cbComplainServiceWithTurnAroundTime) {
			reactOnUpdateProcessConfig("Service",
					"COMPLAINSERVICEWITHTURNAROUNDTIME", model.getCompanyId(),
					form.cbComplainServiceWithTurnAroundTime.getValue(),
					form.cbComplainServiceWithTurnAroundTime.getName());
		}
		if (event.getSource() == form.cbCloseComplaintWhenServiceClose) {
			reactOnUpdateProcessConfig("Service",
					"ENABLECOMPLETECOMPLAINTSERVICE", model.getCompanyId(),
					form.cbCloseComplaintWhenServiceClose.getValue(),
					form.cbCloseComplaintWhenServiceClose.getName());
		}
		if (event.getSource() == form.cbSendSrCopyOnServiceCompletion) {
			reactOnUpdateProcessConfig("Service",
					"SendSRCopyOnServiceCompletion", model.getCompanyId(),
					form.cbSendSrCopyOnServiceCompletion.getValue(),
					form.cbSendSrCopyOnServiceCompletion.getName());
		}
		if (event.getSource() == form.cbCustomizedSrCopy) {
			reactOnUpdateProcessConfig("EVA Pedio", "CustomizedSrCopy",
					model.getCompanyId(), form.cbCustomizedSrCopy.getValue(),
					form.cbCustomizedSrCopy.getName());
		}
		if (event.getSource() == form.cbMAKEMATERIALMANDATORY) {
			reactOnUpdateProcessConfig("Service", "MAKEMATERIALMANDATORY",
					model.getCompanyId(),
					form.cbMAKEMATERIALMANDATORY.getValue(),
					form.cbMAKEMATERIALMANDATORY.getName());
		}
		if (event.getSource() == form.cbMakeServiceEditableAfterCompletion) {
			reactOnUpdateProcessConfig("Service",
					"MakeServiceEditableAfterCompletion", model.getCompanyId(),
					form.cbMakeServiceEditableAfterCompletion.getValue(),
					form.cbMakeServiceEditableAfterCompletion.getName());
		}
		if (event.getSource() == form.cbMultipleExpenseMngt) {
			reactOnUpdateProcessConfig("Service", "MultipleExpenseMngt",
					model.getCompanyId(),
					form.cbMultipleExpenseMngt.getValue(),
					form.cbMultipleExpenseMngt.getName());
		}
		if (event.getSource() == form.cbDATETECHNICIANWISESERVICEPOPUP) {
			reactOnUpdateProcessConfig("Service", "DATETECHNICIANWISESERVICEPOPUP",
					model.getCompanyId(),
					form.cbDATETECHNICIANWISESERVICEPOPUP.getValue(),
					form.cbDATETECHNICIANWISESERVICEPOPUP.getName());
		}

		if (event.getSource() == form.cbHideCompleteRescheduleFumigationColoumn) {
			reactOnUpdateProcessConfig("Service", "HideCompleteRescheduleFumigationColoumn",
					model.getCompanyId(),
					form.cbHideCompleteRescheduleFumigationColoumn.getValue(),
					form.cbHideCompleteRescheduleFumigationColoumn.getName());
		}
		
		if (event.getSource() == form.cbHideFumigationButton) {
			reactOnUpdateProcessConfig("Service", "HideFumigationButton",
					model.getCompanyId(),
					form.cbHideFumigationButton.getValue(),
					form.cbHideFumigationButton.getName());
		}
		
		if (event.getSource() == form.cbCOMPLAINSERVICEWITHTURNAROUNDTIME) {
			reactOnUpdateProcessConfig("Service", "COMPLAINSERVICEWITHTURNAROUNDTIME",
					model.getCompanyId(),
					form.cbCOMPLAINSERVICEWITHTURNAROUNDTIME.getValue(),
					form.cbCOMPLAINSERVICEWITHTURNAROUNDTIME.getName());
		}
		
		if (event.getSource() == form.cbEmailCustomizedAndGenericSRCopy) {
			reactOnUpdateProcessConfig("Service", "EmailCustomizedAndGenericSRCopy",
					model.getCompanyId(),
					form.cbEmailCustomizedAndGenericSRCopy.getValue(),
					form.cbEmailCustomizedAndGenericSRCopy.getName());
		}
		
		if (event.getSource() == form.cbPC_SERVICESCHEDULEMINMAXROUNDUP) {
			reactOnUpdateProcessConfig("Service", "PC_SERVICESCHEDULEMINMAXROUNDUP",
					model.getCompanyId(),
					form.cbPC_SERVICESCHEDULEMINMAXROUNDUP.getValue(),
					form.cbPC_SERVICESCHEDULEMINMAXROUNDUP.getName());
		}
		
		if (event.getSource() == form.cbPC_HIDE_CS_LIST_PLAN_BUTTON) {
			reactOnUpdateProcessConfig("Service", "PC_HIDE_CS_LIST_PLAN_BUTTON",
					model.getCompanyId(),
					form.cbPC_HIDE_CS_LIST_PLAN_BUTTON.getValue(),
					form.cbPC_HIDE_CS_LIST_PLAN_BUTTON.getName());
		}
		
		if (event.getSource() == form.cbPC_PedioFindingAsServiceQuantityInBilling) {
			reactOnUpdateProcessConfig("Service", "PC_PedioFindingAsServiceQuantityInBilling",
					model.getCompanyId(),
					form.cbPC_PedioFindingAsServiceQuantityInBilling.getValue(),
					form.cbPC_PedioFindingAsServiceQuantityInBilling.getName());
		}
		
		if (event.getSource() == form.cbENABLECOMPLETEBUTTONONSCHEDULEPOPUP) {
			reactOnUpdateProcessConfig("Service", "ENABLECOMPLETEBUTTONONSCHEDULEPOPUP",
					model.getCompanyId(),
					form.cbENABLECOMPLETEBUTTONONSCHEDULEPOPUP.getValue(),
					form.cbENABLECOMPLETEBUTTONONSCHEDULEPOPUP.getName());
		}
		
		if (event.getSource() == form.cbHideServiceValueExceptAdmin) {
			reactOnUpdateProcessConfig("Service", "HideServiceValueExceptAdmin",
					model.getCompanyId(),
					form.cbHideServiceValueExceptAdmin.getValue(),
					form.cbHideServiceValueExceptAdmin.getName());
		}
		
		if (event.getSource() == form.cbRestrictServiceDownloadWithOTP) {
			reactOnUpdateProcessConfig("Service", "RestrictServiceDownloadWithOTP",
					model.getCompanyId(),
					form.cbRestrictServiceDownloadWithOTP.getValue(),
					form.cbRestrictServiceDownloadWithOTP.getName());
		}
		
		if (event.getSource() == form.cbStopServiceRescheduleEmail) {
			reactOnUpdateProcessConfig("Service", "StopServiceRescheduleEmail(Pedio,CustomerPortal)",
					model.getCompanyId(),
					form.cbStopServiceRescheduleEmail.getValue(),
					form.cbStopServiceRescheduleEmail.getName());
		}
		
		if (event.getSource() == form.cbUpdateFindingEntity) {
			reactOnUpdateProcessConfig("Service", "UpdateFindingEntity",
					model.getCompanyId(),
					form.cbUpdateFindingEntity.getValue(),
					form.cbUpdateFindingEntity.getName());
		}
		
		if (event.getSource() == form.cbEnablePedio) {
			reactonEnablePedio();
		}
		if (event.getSource() == form.cbReturnQtyUpdate) {
			reactOnUpdateProcessConfig("Service", "MINBASEDONCONSUMEDQUANTITY",
					model.getCompanyId(), form.cbReturnQtyUpdate.getValue(),
					form.cbReturnQtyUpdate.getName());
		}
		if (event.getSource() == form.cbServiceScheduleMaterailMandatory) {
			reactOnUpdateProcessConfig("Service", "WarehouseDetailsMandatory",
					model.getCompanyId(),
					form.cbServiceScheduleMaterailMandatory.getValue(),
					form.cbServiceScheduleMaterailMandatory.getName());
		}
		if (event.getSource() == form.cbCopyMaterialToRemainingServices) {
			reactOnUpdateProcessConfig("Service", "UpdateProjectMaterial",
					model.getCompanyId(),
					form.cbCopyMaterialToRemainingServices.getValue(),
					form.cbCopyMaterialToRemainingServices.getName());
		}
		if (event.getSource() == form.cbEnableFcmNotification) {
			reactOnUpdateProcessConfig("Service", "EnableFcmNotification",
					model.getCompanyId(),
					form.cbEnableFcmNotification.getValue(),
					form.cbEnableFcmNotification.getName());
			reactOnUpdateProcessConfig("EVA Pedio", "FCMNOTIFICATION",
					model.getCompanyId(),
					form.cbEnableFcmNotification.getValue(),
					form.cbEnableFcmNotification.getName());
		}
		if (event.getSource() == form.cbEnableServiceScheduleNotification) {
			reactOnUpdateProcessConfig("Service",
					"ENABLESERVICESCHEDULENOTIFICATION", model.getCompanyId(),
					form.cbEnableServiceScheduleNotification.getValue(),
					form.cbEnableServiceScheduleNotification.getName());
		}

		if (event.getSource() == form.cbMAKETECHNICIANREMARKMANDITORY) {
			reactOnUpdateProcessConfig("EVA Pedio",
					"MAKETECHNICIANREMARKMANDITORY", model.getCompanyId(),
					form.cbMAKETECHNICIANREMARKMANDITORY.getValue(),
					form.cbMAKETECHNICIANREMARKMANDITORY.getName());
		}
		if (event.getSource() == form.cbMAKECUSTOMERNAMEMANDITORY) {
			reactOnUpdateProcessConfig("EVA Pedio",
					"MAKECUSTOMERNAMEMANDITORY", model.getCompanyId(),
					form.cbMAKECUSTOMERNAMEMANDITORY.getValue(),
					form.cbMAKECUSTOMERNAMEMANDITORY.getName());
		}
		if (event.getSource() == form.cbMAKECUSTOMERREMARKMANDITORY) {
			reactOnUpdateProcessConfig("EVA Pedio",
					"MAKECUSTOMERREMARKMANDITORY", model.getCompanyId(),
					form.cbMAKECUSTOMERREMARKMANDITORY.getValue(),
					form.cbMAKECUSTOMERREMARKMANDITORY.getName());
		}
		if (event.getSource() == form.cbMAKEFINDINGSMANDATORY) {
			reactOnUpdateProcessConfig("EVA Pedio",
					"MAKEFINDINGSMANDATORY", model.getCompanyId(),
					form.cbMAKEFINDINGSMANDATORY.getValue(),
					form.cbMAKEFINDINGSMANDATORY.getName());
		}
		if (event.getSource() == form.cbMAKETYPEOFTREATMENTMANDATORY) {
			reactOnUpdateProcessConfig("EVA Pedio",
					"MAKETYPEOFTREATMENTMANDATORY", model.getCompanyId(),
					form.cbMAKETYPEOFTREATMENTMANDATORY.getValue(),
					form.cbMAKETYPEOFTREATMENTMANDATORY.getName());
		}
		
		
		if (event.getSource() == form.cbHIDESERVICEFINDING) {
			reactOnUpdateProcessConfig("EVA Pedio", "HIDESERVICEFINDING",
					model.getCompanyId(), form.cbHIDESERVICEFINDING.getValue(),
					form.cbHIDESERVICEFINDING.getName());
		}
		if (event.getSource() == form.cbHIDEMATERIALS) {
			reactOnUpdateProcessConfig("EVA Pedio", "HIDEMATERIALS",
					model.getCompanyId(), form.cbHIDEMATERIALS.getValue(),
					form.cbHIDEMATERIALS.getName());
		}
		if (event.getSource() == form.cbSHOWEXPENSE) {
			reactOnUpdateProcessConfig("EVA Pedio", "SHOWEXPENSE",
					model.getCompanyId(), form.cbSHOWEXPENSE.getValue(),
					form.cbSHOWEXPENSE.getName());
		}
		if (event.getSource() == form.cbDIRECTCOMPLETE) {
			reactOnUpdateProcessConfig("EVA Pedio", "DIRECTCOMPLETE",
					model.getCompanyId(), form.cbDIRECTCOMPLETE.getValue(),
					form.cbDIRECTCOMPLETE.getName());
		}
		if (event.getSource() == form.cbTECHNICIANATTENDANCE) {
			reactOnUpdateProcessConfig("EVA Pedio", "TECHNICIANATTENDANCE",
					model.getCompanyId(),
					form.cbTECHNICIANATTENDANCE.getValue(),
					form.cbTECHNICIANATTENDANCE.getName());
		}
		if (event.getSource() == form.cbSITEFINDING) {
			reactOnUpdateProcessConfig("EVA Pedio", "SITEFINDING",
					model.getCompanyId(), form.cbSITEFINDING.getValue(),
					form.cbSITEFINDING.getName());
		}
		if (event.getSource() == form.cbREJECTSERVICE) {
			reactOnUpdateProcessConfig("EVA Pedio", "REJECTSERVICE",
					model.getCompanyId(), form.cbREJECTSERVICE.getValue(),
					form.cbREJECTSERVICE.getName());
		}
		if (event.getSource() == form.cbHIDESRS) {
			reactOnUpdateProcessConfig("EVA Pedio", "HIDESRS",
					model.getCompanyId(), form.cbHIDESRS.getValue(),
					form.cbHIDESRS.getName());
		}
		if (event.getSource() == form.cbLIVETRACKING) {
			reactOnUpdateProcessConfig("EVA Pedio", "LIVETRACKING",
					model.getCompanyId(), form.cbLIVETRACKING.getValue(),
					form.cbLIVETRACKING.getName());
		}
		if (event.getSource() == form.cbSENDOTPFORSERVICECOMPLETION) {
			reactOnUpdateProcessConfig("Service",
					"SENDOTPFORSERVICECOMPLETION", model.getCompanyId(),
					form.cbSENDOTPFORSERVICECOMPLETION.getValue(),
					form.cbSENDOTPFORSERVICECOMPLETION.getName());
		}
		if (event.getSource() == form.cbPC_PEDIO_HIDE_TREATMENTS) {
			reactOnUpdateProcessConfig("EVA Pedio", "PC_PEDIO_HIDE_TREATMENTS",
					model.getCompanyId(),
					form.cbPC_PEDIO_HIDE_TREATMENTS.getValue(),
					form.cbPC_PEDIO_HIDE_TREATMENTS.getName());
		}
		if (event.getSource() == form.cbPC_ADDMIXINGRATIOANDLOTNO) {
			reactOnUpdateProcessConfig("EVA Pedio",
					"PC_ADDMIXINGRATIOANDLOTNO", model.getCompanyId(),
					form.cbPC_ADDMIXINGRATIOANDLOTNO.getValue(),
					form.cbPC_ADDMIXINGRATIOANDLOTNO.getName());
		}
		if (event.getSource() == form.cbPC_DocumentUploadMandatory) {
			reactOnUpdateProcessConfig("EVA Pedio",
					"PC_DocumentUploadMandatory", model.getCompanyId(),
					form.cbPC_DocumentUploadMandatory.getValue(),
					form.cbPC_DocumentUploadMandatory.getName());
		}
		if (event.getSource() == form.cbPC_Pedio_Hide_Reschedule) {
			reactOnUpdateProcessConfig("EVA Pedio", "PC_Pedio_Hide_Reschedule",
					model.getCompanyId(),
					form.cbPC_Pedio_Hide_Reschedule.getValue(),
					form.cbPC_Pedio_Hide_Reschedule.getName());
		}
		if (event.getSource() == form.cbPC_Hide_Customer_Mobile) {
			reactOnUpdateProcessConfig("EVA Pedio", "PC_Hide_Customer_Mobile",
					model.getCompanyId(),
					form.cbPC_Hide_Customer_Mobile.getValue(),
					form.cbPC_Hide_Customer_Mobile.getName());
		}
		if (event.getSource() == form.cbLoadLoggedInUserWiseCustomer) {
			reactOnUpdateProcessConfig("EVA Priora",
					"LoadLoggedInUserWiseCustomer", model.getCompanyId(),
					form.cbLoadLoggedInUserWiseCustomer.getValue(),
					form.cbLoadLoggedInUserWiseCustomer.getName());
		}
		if (event.getSource() == form.cbPREMISESFLAG) {
			reactOnUpdateProcessConfig("EVA Pedio", "PREMISESFLAG",
					model.getCompanyId(), form.cbPREMISESFLAG.getValue(),
					form.cbPREMISESFLAG.getName());
		}
		if (event.getSource() == form.cbMARKSERVICECOMPLETEDFROMAPP) {
			reactOnUpdateProcessConfig("Service", "MARKSERVICECOMPLETEDFROMAPP",
					model.getCompanyId(), form.cbMARKSERVICECOMPLETEDFROMAPP.getValue(),
					form.cbMARKSERVICECOMPLETEDFROMAPP.getName());
		}
		if (event.getSource() == form.cbSHOWEMAIL) {
			reactOnUpdateProcessConfig("EVA Pedio", "SHOWEMAIL",
					model.getCompanyId(), form.cbSHOWEMAIL.getValue(),
					form.cbSHOWEMAIL.getName());
		}
		if (event.getSource() == form.cbPEDIO_HIDEPHOTOS) {
			reactOnUpdateProcessConfig("EVA Pedio", "PEDIO_HIDEPHOTOS",
					model.getCompanyId(), form.cbPEDIO_HIDEPHOTOS.getValue(),
					form.cbPEDIO_HIDEPHOTOS.getName());
		}
		if (event.getSource() == form.cbShowProductPremises) {
			reactOnUpdateProcessConfig("Service", "ShowProductPremises",
					model.getCompanyId(), form.cbShowProductPremises.getValue(),
					form.cbShowProductPremises.getName());
		}
		if (event.getSource() == form.cbPC_DoNotShowRatings) {
			reactOnUpdateProcessConfig("EVA Pedio", "PC_DoNotShowRatings",
					model.getCompanyId(), form.cbPC_DoNotShowRatings.getValue(),
					form.cbPC_DoNotShowRatings.getName());
		}
		if (event.getSource() == form.cbMAKESTARRATINGMANDITORY) {
			reactOnUpdateProcessConfig("EVA Pedio", "MAKESTARRATINGMANDITORY",
					model.getCompanyId(), form.cbMAKESTARRATINGMANDITORY.getValue(),
					form.cbMAKESTARRATINGMANDITORY.getName());
		}
		if (event.getSource() == form.cbPC_HideCustomerRemark) {
			reactOnUpdateProcessConfig("EVA Pedio", "PC_HideCustomerRemark",
					model.getCompanyId(), form.cbPC_HideCustomerRemark.getValue(),
					form.cbPC_HideCustomerRemark.getName());
		}		
		
		
		
		if (event.getSource() == form.cbprintCustomerRemark) {
			reactOnUpdateProcessConfig("Service", "PRINTCUSTOMERREMARK",
					model.getCompanyId(),
					form.cbprintCustomerRemark.getValue(),
					form.cbprintCustomerRemark.getName());
		}
		if (event.getSource() == form.cbPrintTreatmentOnSRCopy) {
			reactOnUpdateProcessConfig("Service", "PRINTPESTTREATMENTONSRCOPY",
					model.getCompanyId(),
					form.cbPrintTreatmentOnSRCopy.getValue(),
					form.cbPrintTreatmentOnSRCopy.getName());
		}
		if (event.getSource() == form.cbPrintPhotoOnSRCopy) {
			reactOnUpdateProcessConfig("Service", "PRINTPHOTOONSRCOPY",
					model.getCompanyId(), form.cbPrintPhotoOnSRCopy.getValue(),
					form.cbPrintPhotoOnSRCopy.getName());
		}
		if (event.getSource() == form.cbTechnicianRemark) {
			reactOnUpdateProcessConfig("Service", "PRINTTECHNICIANREMARK",
					model.getCompanyId(), form.cbTechnicianRemark.getValue(),
					form.cbTechnicianRemark.getName());
		}
		if (event.getSource() == form.cbPrintFindingOnSRCopy) {
			reactOnUpdateProcessConfig("Service", "PRINTFINDINGSONSRCOPY",
					model.getCompanyId(),
					form.cbPrintFindingOnSRCopy.getValue(),
					form.cbPrintFindingOnSRCopy.getName());
		}
		if (event.getSource() == form.cbPedioNotificationRemark) {
			reactOnUpdateProcessConfig("EVA Pedio",
					"PC_PEDIO_NOTECHNICIAN_REMARK", model.getCompanyId(),
					form.cbPedioNotificationRemark.getValue(),
					form.cbPedioNotificationRemark.getName());
		}
		if (event.getSource() == form.cbhideMobileNo) {
			reactOnUpdateProcessConfig("EVA Pedio", "PC_PEDIO_NOMOBILENUMBER",
					model.getCompanyId(), form.cbhideMobileNo.getValue(),
					form.cbhideMobileNo.getName());
		}
		if (event.getSource() == form.cbhideCustomerFeedback) {
			reactOnUpdateProcessConfig("EVA Pedio",
					"PC_PEDIO_NOCUSTOMER_FEEDBACK", model.getCompanyId(),
					form.cbhideCustomerFeedback.getValue(),
					form.cbhideCustomerFeedback.getName());
		}
		if (event.getSource() == form.cbUploadSRCopy) {
			reactOnUpdateProcessConfig("EVA Pedio", "UPLOADSRCOPY",
					model.getCompanyId(), form.cbUploadSRCopy.getValue(),
					form.cbUploadSRCopy.getName());
		}
		
		if (event.getSource() == form.cbHIDEMATERIALANDUOM) {
			reactOnUpdateProcessConfig("Service", "HIDEMATERIALANDUOM",
					model.getCompanyId(), form.cbHIDEMATERIALANDUOM.getValue(),
					form.cbHIDEMATERIALANDUOM.getName());
		}
		if (event.getSource() == form.cbDecreaseTableRow) {
			reactOnUpdateProcessConfig("Service", "DecreaseTableRow",
					model.getCompanyId(), form.cbDecreaseTableRow.getValue(),
					form.cbDecreaseTableRow.getName());
		}
		if (event.getSource() == form.cbPC_HideCellNumber_SRCopy) {
			reactOnUpdateProcessConfig("Service", "PC_HideCellNumber_SRCopy",
					model.getCompanyId(), form.cbPC_HideCellNumber_SRCopy.getValue(),
					form.cbPC_HideCellNumber_SRCopy.getName());
		}
		if (event.getSource() == form.cbPC_DoNotPrintMaterialOnSummarySR) {
			reactOnUpdateProcessConfig("Service", "PC_DoNotPrintMaterialOnSummarySR",
					model.getCompanyId(), form.cbPC_DoNotPrintMaterialOnSummarySR.getValue(),
					form.cbPC_DoNotPrintMaterialOnSummarySR.getName());
		}
		if (event.getSource() == form.cbPRINTMATERIALONSRCOPY) {
			reactOnUpdateProcessConfig("Service", "PRINTMATERIALONSRCOPY",
					model.getCompanyId(), form.cbPRINTMATERIALONSRCOPY.getValue(),
					form.cbPRINTMATERIALONSRCOPY.getName());
		}
		if (event.getSource() == form.cbPRINTPHOTOONSRCOPY) {
			reactOnUpdateProcessConfig("Service", "PRINTPHOTOONSRCOPY",
					model.getCompanyId(), form.cbPRINTPHOTOONSRCOPY.getValue(),
					form.cbPRINTPHOTOONSRCOPY.getName());
		}	

		if (event.getSource() == form.cbMakeCustomerEmailMandatory) {
			reactOnUpdateProcessConfig("Customer", "MakeEmailMandatory",
					model.getCompanyId(),
					form.cbMakeCustomerEmailMandatory.getValue(),
					form.cbMakeCustomerEmailMandatory.getName());
		}
		if (event.getSource() == form.cbMakeCustGroupMandatory) {
			reactOnUpdateProcessConfig("Customer", "MakeGroupMandatory",
					model.getCompanyId(),
					form.cbMakeCustGroupMandatory.getValue(),
					form.cbMakeCustGroupMandatory.getName());
		}
		if (event.getSource() == form.cbMakeCustPinCodeMandatory) {
			reactOnUpdateProcessConfig("Customer", "MakePincodeMandatory",
					model.getCompanyId(),
					form.cbMakeCustPinCodeMandatory.getValue(),
					form.cbMakeCustPinCodeMandatory.getName());
		}
		if (event.getSource() == form.cbMakeCustRefMandatory) {
			reactOnUpdateProcessConfig("Customer", "MakeRefNumber1Mandatory",
					model.getCompanyId(),
					form.cbMakeCustRefMandatory.getValue(),
					form.cbMakeCustRefMandatory.getName());
		}
		if (event.getSource() == form.cbMakeCustLevelMandatory) {
			reactOnUpdateProcessConfig("Customer",
					"MakeCustomerLevelMandatory", model.getCompanyId(),
					form.cbMakeCustLevelMandatory.getValue(),
					form.cbMakeCustLevelMandatory.getName());
		}
		if (event.getSource() == form.cbHideCustomerGroup) {
			reactOnUpdateProcessConfig("Customer", "HideCustomerGroup",
					model.getCompanyId(), form.cbHideCustomerGroup.getValue(),
					form.cbHideCustomerGroup.getName());
		}
		if (event.getSource() == form.cbMakecustCategoryAndTypeDisable) {
			reactOnUpdateProcessConfig("Customer",
					"MakeCategoryAndTypeDisable", model.getCompanyId(),
					form.cbMakecustCategoryAndTypeDisable.getValue(),
					form.cbMakecustCategoryAndTypeDisable.getName());
		}
		if (event.getSource() == form.cbMakeCellNoNonMandatory) {
			reactOnUpdateProcessConfig("Customer",
					"MakeCellNumberNonMandatory", model.getCompanyId(),
					form.cbMakeCellNoNonMandatory.getValue(),
					form.cbMakeCellNoNonMandatory.getName());
		}
		
		if (event.getSource() == form.cbMakeAccountManagerMandatory) {
			reactOnUpdateProcessConfig("Customer",
					"MakeAccountManagerMandatory", model.getCompanyId(),
					form.cbMakeAccountManagerMandatory.getValue(),
					form.cbMakeAccountManagerMandatory.getName());
		}
		if (event.getSource() == form.cbMakeSalesPersonMandatory) {
			reactOnUpdateProcessConfig("Customer",
					"MakeSalesPersonMandatory", model.getCompanyId(),
					form.cbMakeSalesPersonMandatory.getValue(),
					form.cbMakeSalesPersonMandatory.getName());
		}
		if (event.getSource() == form.cbEnableloadLargeCustomerData) {
			reactOnUpdateProcessConfig("Customer",
					"EnableloadLargeCustomerData", model.getCompanyId(),
					form.cbEnableloadLargeCustomerData.getValue(),
					form.cbEnableloadLargeCustomerData.getName());
		}
		
		if (event.getSource() == form.cbHideCustomerReligion) {
			reactOnUpdateProcessConfig("Customer",
					"HideCustomerReligion", model.getCompanyId(),
					form.cbHideCustomerReligion.getValue(),
					form.cbHideCustomerReligion.getName());
		}
		
		if (event.getSource() == form.cbMakeLeadGroupMandatory) {
			reactOnUpdateProcessConfig("Lead", "makeGroupMandatory",
					model.getCompanyId(),
					form.cbMakeLeadGroupMandatory.getValue(),
					form.cbMakeLeadGroupMandatory.getName());
		}

		if (event.getSource() == form.cbLeadCustPopupEmailMandatory) {
			reactOnUpdateProcessConfig("Lead",
					"MakeCustomerPopUpEmailMandatory", model.getCompanyId(),
					form.cbLeadCustPopupEmailMandatory.getValue(),
					form.cbLeadCustPopupEmailMandatory.getName());
		}
		if (event.getSource() == form.cbMakeCustomerPopUpCompanyNameMandatory) {
			reactOnUpdateProcessConfig("Lead",
					"MakeCustomerPopUpCompanyNameMandatory",
					model.getCompanyId(),
					form.cbMakeCustomerPopUpCompanyNameMandatory.getValue(),
					form.cbMakeCustomerPopUpCompanyNameMandatory.getName());
		}

		if (event.getSource() == form.cbMakeCustomerPopUpPhoneNumberMandatory) {
			reactOnUpdateProcessConfig("Lead",
					"MakeCustomerPopUpPhoneNumberMandatory",
					model.getCompanyId(),
					form.cbMakeCustomerPopUpPhoneNumberMandatory.getValue(),
					form.cbMakeCustomerPopUpPhoneNumberMandatory.getName());
		}

		if (event.getSource() == form.cbEnableFreezeLead) {
			reactOnUpdateProcessConfig("Lead", "EnableFreezeLead",
					model.getCompanyId(), form.cbEnableFreezeLead.getValue(),
					form.cbEnableFreezeLead.getName());
		}

		if (event.getSource() == form.cbCommunicationLogColumn) {
			reactOnUpdateProcessConfig("Lead", "CommunicationLogColumn",
					model.getCompanyId(),
					form.cbCommunicationLogColumn.getValue(),
					form.cbCommunicationLogColumn.getName());
		}

		if (event.getSource() == form.cbMapCustomerTypeToLeadCategory) {
			reactOnUpdateProcessConfig("Lead", "MapCustomerTypeToLeadCategory",
					model.getCompanyId(),
					form.cbMapCustomerTypeToLeadCategory.getValue(),
					form.cbMapCustomerTypeToLeadCategory.getName());
		}

		if (event.getSource() == form.cbMakePriceColumnDisable) {
			reactOnUpdateProcessConfig("Lead", "MakePriceColumnDisable",
					model.getCompanyId(),
					form.cbMakePriceColumnDisable.getValue(),
					form.cbMakePriceColumnDisable.getName());
		}
		if (event.getSource() == form.cbChangeProductComposite) {
			reactOnUpdateProcessConfig("Lead", "ChangeProductComposite",
					model.getCompanyId(),
					form.cbChangeProductComposite.getValue(),
					form.cbChangeProductComposite.getName());
		}

		if (event.getSource() == form.cbDisableEmailIdValidation) {
			reactOnUpdateProcessConfig("Lead", "DISABLEEMAILIDVALIDATION",
					model.getCompanyId(),
					form.cbDisableEmailIdValidation.getValue(),
					form.cbDisableEmailIdValidation.getName());
		}
		/**
		 * Added by sheetal:15-02-2022 Des: Adding Hide Number Range checkbox
		 * under lead tab for pecop
		 ***/

		if (event.getSource() == form.cbHideNumberRange) {
			reactOnUpdateProcessConfig("Lead", "PC_LEAD_HIDE_NUMBER_RANGE",
					model.getCompanyId(), form.cbHideNumberRange.getValue(),
					form.cbHideNumberRange.getName());
		}
		if (event.getSource() == form.cbMakeQuotationGroupMandatory) {
			reactOnUpdateProcessConfig("Quotation", "makeGroupMandatory",
					model.getCompanyId(),
					form.cbMakeQuotationGroupMandatory.getValue(),
					form.cbMakeQuotationGroupMandatory.getName());
		}

		if (event.getSource() == form.cbQuotationTypeMandatory) {
			reactOnUpdateProcessConfig("Quotation", "MakeTypeMandatory",
					model.getCompanyId(),
					form.cbQuotationTypeMandatory.getValue(),
					form.cbQuotationTypeMandatory.getName());
		}

		if (event.getSource() == form.cbQuotationCategoryMandatory) {
			reactOnUpdateProcessConfig("Quotation", "MakeCatagoryMandatory",
					model.getCompanyId(),
					form.cbQuotationCategoryMandatory.getValue(),
					form.cbQuotationCategoryMandatory.getName());
		}

		if (event.getSource() == form.cbDonotProductDescription) {
			reactOnUpdateProcessConfig("Quotation", "DoNotProductDescription",
					model.getCompanyId(),
					form.cbDonotProductDescription.getValue(),
					form.cbDonotProductDescription.getName());
		}
		if (event.getSource() == form.cbPrintProductDescriptionOnPDF) {
			reactOnUpdateProcessConfig("Quotation",
					"PrintProductDiscriptionOnPdf", model.getCompanyId(),
					form.cbPrintProductDescriptionOnPDF.getValue(),
					form.cbPrintProductDescriptionOnPDF.getName());
		}

		if (event.getSource() == form.cbMakeQuotationDateDisable) {
			reactOnUpdateProcessConfig("Quotation", "MakeQuotationDateDisable",
					model.getCompanyId(),
					form.cbMakeQuotationDateDisable.getValue(),
					form.cbMakeQuotationDateDisable.getName());
		}

		if (event.getSource() == form.cbMakeLeadIdMandatory) {
			reactOnUpdateProcessConfig("Quotation", "makeLeadIdMandatory",
					model.getCompanyId(),
					form.cbMakeLeadIdMandatory.getValue(),
					form.cbMakeLeadIdMandatory.getName());
		}

		if (event.getSource() == form.cbprintProductRemarkWithPremise) {
			reactOnUpdateProcessConfig("Quotation", "printProductRemarkWithPremise",
					model.getCompanyId(),
					form.cbprintProductRemarkWithPremise.getValue(),
					form.cbprintProductRemarkWithPremise.getName());
		}
		
		if (event.getSource() == form.cbPrintProductPrimiseInQuotationPdf) {
			reactOnUpdateProcessConfig("Quotation", "printProductPremisesInPdf",
					model.getCompanyId(),
					form.cbPrintProductPrimiseInQuotationPdf.getValue(),
					form.cbPrintProductPrimiseInQuotationPdf.getName());
		}
		
		if (event.getSource() == form.cbPC_DoNotPrintProductWarrantyOnQuotation) {
			reactOnUpdateProcessConfig("Quotation", "PC_DoNotPrintProductWarranty",
					model.getCompanyId(),
					form.cbPC_DoNotPrintProductWarrantyOnQuotation.getValue(),
					form.cbPC_DoNotPrintProductWarrantyOnQuotation.getName());
		}
		
		if (event.getSource() == form.cbMakeContractGroupMandatory) {
			reactOnUpdateProcessConfig("Contract", "MakeGroupMandatory",
					model.getCompanyId(),
					form.cbMakeContractGroupMandatory.getValue(),
					form.cbMakeContractGroupMandatory.getName());
		}

		if (event.getSource() == form.cbMakeContractTypeMandatory) {
			reactOnUpdateProcessConfig("Contract", "MakeTypeMandatory",
					model.getCompanyId(),
					form.cbMakeContractTypeMandatory.getValue(),
					form.cbMakeContractTypeMandatory.getName());
		}

		if (event.getSource() == form.cbPrintProductPrimiseInPdf) {
			reactOnUpdateProcessConfig("Contract", "printProductPremisesInPdf",
					model.getCompanyId(),
					form.cbPrintProductPrimiseInPdf.getValue(),
					form.cbPrintProductPrimiseInPdf.getName());
		}

		if (event.getSource() == form.cbMakeNumberRangeMandatory) {
			reactOnUpdateProcessConfig("Contract", "MakeNumberRangeMandatory",
					model.getCompanyId(),
					form.cbMakeNumberRangeMandatory.getValue(),
					form.cbMakeNumberRangeMandatory.getName());
		}

		if (event.getSource() == form.cbMakeContractDateDisable) {
			reactOnUpdateProcessConfig("Contract", "MakeContractDateDisable",
					model.getCompanyId(),
					form.cbMakeContractDateDisable.getValue(),
					form.cbMakeContractDateDisable.getName());
		}

		if (event.getSource() == form.restrictUserLocationAccess) {
			reactonUserLocation();
		}

		if (event.getSource() == form.branchWiseInvoiceNumberRange) {
			reactonBranchWiseInvoiceNumberRange();
		}

		if (event.getSource() == form.cbinvoiceAccountingInterface) {
			reactOnUpdateProcessConfig("Invoice",
					AppConstants.PROCESSTYPEACCINTERFACE, model.getCompanyId(),
					form.cbinvoiceAccountingInterface.getValue(),
					form.cbinvoiceAccountingInterface.getName());
		}
		if (event.getSource() == form.cbCompanyAsLetterHead) {
			reactOnUpdateProcessConfig("Invoice", "CompanyAsLetterHead",
					model.getCompanyId(),
					form.cbCompanyAsLetterHead.getValue(),
					form.cbCompanyAsLetterHead.getName());
		}
		if (event.getSource() == form.cbPrintProductPrimicesinPDF) {
			reactOnUpdateProcessConfig("Invoice", "printProductPremisesInPdf",
					model.getCompanyId(),
					form.cbPrintProductPrimicesinPDF.getValue(),
					form.cbPrintProductPrimicesinPDF.getName());
		}
		if (event.getSource() == form.cbInvoiceEditAllRoll) {
			reactOnUpdateProcessConfig("Invoice",
					"EnableInvoiceEditableForAllRoles", model.getCompanyId(),
					form.cbInvoiceEditAllRoll.getValue(),
					form.cbInvoiceEditAllRoll.getName());
		}
		if (event.getSource() == form.cbmakeAddressToUppercase) {
			reactOnUpdateProcessConfig("Invoice", "MakeAdressToUppercase",
					model.getCompanyId(),
					form.cbmakeAddressToUppercase.getValue(),
					form.cbmakeAddressToUppercase.getName());
		}
		if (event.getSource() == form.cbPrintPoDetailsOnInvoice) {
			reactOnUpdateProcessConfig("Invoice", "PrintPoDetailsOnInvoice",
					model.getCompanyId(),
					form.cbPrintPoDetailsOnInvoice.getValue(),
					form.cbPrintPoDetailsOnInvoice.getName());
		}
		if (event.getSource() == form.cbHideRateandDiscountColumn) {
			reactOnUpdateProcessConfig("Invoice", "HideRateAndDiscountColumn",
					model.getCompanyId(),
					form.cbHideRateandDiscountColumn.getValue(),
					form.cbHideRateandDiscountColumn.getName());
		}
		if (event.getSource() == form.cbPrintProductDiscriptionOnPdfInInvoice) {
			reactOnUpdateProcessConfig("Invoice",
					"PrintProductDescriptionOnPdf", model.getCompanyId(),
					form.cbPrintProductDiscriptionOnPdfInInvoice.getValue(),
					form.cbPrintProductDiscriptionOnPdfInInvoice.getName());
		}
		if (event.getSource() == form.cbPaymentAccountningInterface) {
			reactOnUpdateProcessConfig("CustomerPayment",
					AppConstants.PROCESSTYPEACCINTERFACE, model.getCompanyId(),
					form.cbPaymentAccountningInterface.getValue(),
					form.cbPaymentAccountningInterface.getName());
		}
		if (event.getSource() == form.cbPaymentAddIntoPettyCash) {
			reactOnUpdateProcessConfig("CustomerPayment",
					"PaymentAddIntoPettyCash", model.getCompanyId(),
					form.cbPaymentAddIntoPettyCash.getValue(),
					form.cbPaymentAddIntoPettyCash.getName());
		}
		if (event.getSource() == form.cbPaymentCompanyAsLetterHead) {
			reactOnUpdateProcessConfig("CustomerPayment",
					"CompanyAsLetterHead", model.getCompanyId(),
					form.cbPaymentCompanyAsLetterHead.getValue(),
					form.cbPaymentCompanyAsLetterHead.getName());
		}
		if (event.getSource() == form.cbPrintOrderNoAndGrnNo) {
			reactOnUpdateProcessConfig("VendorInvoice", "PrintOrderNoAndGrnNo",
					model.getCompanyId(),
					form.cbPrintOrderNoAndGrnNo.getValue(),
					form.cbPrintOrderNoAndGrnNo.getName());
		}

		if (event.getSource() == form.cbRenewalReminderlistEmail) {
			reactOnUpdateProcessConfig("CronJob",
					"ContractRenewalWithoutProcessedCustomerEmail",
					model.getCompanyId(),
					form.cbRenewalReminderlistEmail.getValue(),
					form.cbRenewalReminderlistEmail.getName());
		}

		if (event.getSource() == form.cbOutstandingtoClientEmail) {
			reactOnUpdateProcessConfig("CronJob",
					"PaymentARDueDailyEmailToClient", model.getCompanyId(),
					form.cbOutstandingtoClientEmail.getValue(),
					form.cbOutstandingtoClientEmail.getName());
		}

		if (event.getSource() == form.cbInvoiceAccoutingInterface) {
			reactOnUpdateProcessConfig("Invoice",
					AppConstants.PROCESSTYPEACCINTERFACE, model.getCompanyId(),
					form.cbInvoiceAccoutingInterface.getValue(),
					form.cbInvoiceAccoutingInterface.getName());
		}

		if (event.getSource() == form.cbPC_DoNot_Print_Services) {
			reactOnUpdateProcessConfig("Invoice", "PC_DoNot_Print_Services",
					model.getCompanyId(),
					form.cbPC_DoNot_Print_Services.getValue(),
					form.cbPC_DoNot_Print_Services.getName());
		}
		
		if (event.getSource() == form.cbMakeInvoiceGroupMandatory) {
			reactOnUpdateProcessConfig("Invoice", "MakeInvoiceGroupMandatory",
					model.getCompanyId(),
					form.cbMakeInvoiceGroupMandatory.getValue(),
					form.cbMakeInvoiceGroupMandatory.getName());
		}
		
		if (event.getSource() == form.cbPrintDescriptionOnInvoicefromContract) {
			reactOnUpdateProcessConfig("Invoice", "PrintDescriptionOnInvoiceFromContract",
					model.getCompanyId(),
					form.cbPrintDescriptionOnInvoicefromContract.getValue(),
					form.cbPrintDescriptionOnInvoicefromContract.getName());
		}

		if (event.getSource() == form.cbBranchLevelRestriction) {
			reactOnUpdateProcessConfig("Branch", "BRANCHLEVELRESTRICTION",
					model.getCompanyId(),
					form.cbBranchLevelRestriction.getValue(),
					form.cbBranchLevelRestriction.getName());
		}
		if (event.getSource() == form.cbCustomerBranchRestriction) {
			reactOnUpdateProcessConfig("Customer",
					AppConstants.PC_BranchLevelRestrictionForCustomer,
					model.getCompanyId(),
					form.cbCustomerBranchRestriction.getValue(),
					form.cbCustomerBranchRestriction.getName());
		}
		
		if (event.getSource() == form.cbDisableIPAddressEmail) {
			reactOnUpdateProcessConfig("Company",AppConstants.PC_DISABLEIPADDRESSEMAIL,model.getCompanyId(),
					form.cbDisableIPAddressEmail.getValue(),
					form.cbDisableIPAddressEmail.getName());
		}

		if (event.getSource() == form.typeOfTreatment) {
			reactonTypeOfTreatment();
		}
		if (event.getSource() == form.frequency) {
			reactonFrequency();
		}
		if (event.getSource() == form.findingType) {
			reactonFindingType();
		}
		if (event.getSource() == form.serviceChecklist) {
			reactonServiceChecklist();
		}
		
		if(event.getSource() == form.btnSetupPedio){
			reactOnCopyPedioConfiguration();
		}
		if(event.getSource() == form.btnSharePortalLink){
			custportalPopup.getLblOk().setText("Yes");
			custportalPopup.getLblCancel().setText("No");
			custportalPopup.showPopUp();
		}
		if(event.getSource() == custportalPopup.getLblOk()){
			form.showWaitSymbol();
			Console.log("sms"+custportalPopup.sms.getValue());
			Console.log("whatsapp"+custportalPopup.whatsapp.getValue());
			Console.log("email"+custportalPopup.email.getValue());
			final boolean sms=custportalPopup.sms.getValue();
			final boolean whatsapp=custportalPopup.whatsapp.getValue();
			final boolean email=custportalPopup.email.getValue();
			if(!sms&&!whatsapp&&!email) {
				form.showDialogMessage("Select atleast one communication channel!");
				form.hideWaitSymbol();
			}else {				
				CommonServiceAsync commonSer = GWT.create(CommonService.class);
				commonSer.checkCustomerPortalLicense(model.getCompanyId(),sms,whatsapp,email, new AsyncCallback<String>() {
					
					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						if(result!=null&&!result.equals("")) {
							Console.log("checkCustomerPortalLicense result="+result);
							if(result.equalsIgnoreCase("success")) {
								callShareCustomerPortalLink(sms,whatsapp,email);
								form.hideWaitSymbol();
							}else {
								form.showDialogMessage(result);	
								custportalPopup.hidePopUp();
								form.hideWaitSymbol();
							}
						
						}else {
							form.showDialogMessage("No response from server");	
							custportalPopup.hidePopUp();
							form.hideWaitSymbol();
						}
						
					}
					
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.showDialogMessage("Process Failed.");	
						custportalPopup.hidePopUp();
						form.hideWaitSymbol();
					}
				});
				
				
			}
			
		}
		if(event.getSource() == custportalPopup.getLblCancel()){
			custportalPopup.hidePopUp();
		}
		
		if (event.getSource() == form.cbPrintDescriptionOnInvoicefromSalesOrder) {
			reactOnUpdateProcessConfig("Invoice", "PrintDescriptionOnInvoiceFromSalesOrder",
					model.getCompanyId(),
					form.cbPrintDescriptionOnInvoicefromSalesOrder.getValue(),
					form.cbPrintDescriptionOnInvoicefromSalesOrder.getName());
		}
		if (event.getSource() == form.cbPC_NewInvoiceFormat) {
			reactOnUpdateProcessConfig("Invoice", "PC_NewInvoiceFormat",
					model.getCompanyId(),
					form.cbPC_NewInvoiceFormat.getValue(),
					form.cbPC_NewInvoiceFormat.getName());
		}
		
		if (event.getSource() == form.cbGenerateEinvoiceFromReferenceNumber) {
			reactOnUpdateProcessConfig("Invoice", "GenerateEinvoiceFromReferenceNumber",
					model.getCompanyId(),
					form.cbGenerateEinvoiceFromReferenceNumber.getValue(),
					form.cbGenerateEinvoiceFromReferenceNumber.getName());
		}
		
		if (event.getSource() == form.cbEnableInvoiceIntegrationWithZohoBooks) {
			reactOnUpdateProcessConfig("Invoice", "EnableInvoiceIntegrationWithZohoBooks",
					model.getCompanyId(),
					form.cbEnableInvoiceIntegrationWithZohoBooks.getValue(),
					form.cbEnableInvoiceIntegrationWithZohoBooks.getName());
		}
	}

		private void reactonBranchWiseInvoiceNumberRange() {
			String url = "https://evasoftwaresolutions.freshdesk.com/a/solutions/articles/47001181935";
			Window.open(url, "Branch Wise Invoice Number Range", "enabled");

		}

		private void reactonUserLocation() {
			AppMemory.getAppMemory().currentScreen = Screen.USER;
			genralInvoicePopup = new GeneralViewDocumentPopup(true);
			genralInvoicePopup.setModel(AppConstants.USER,new User());
			generalPanel = new PopupPanel();
			generalPanel.add(genralInvoicePopup);
			generalPanel.show();
			generalPanel.center();
			genralInvoicePopup.btnClose.addClickHandler(this);
		}

		private void reactonEnablePedio() {
			form.showWaitSymbol();
			commonAsync.implementationScreenOperation("Enable Pedio", model.getCompanyId(), form.cbEnablePedio.getValue(), new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol();
						}

						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							form.showDialogMessage(result);
							form.hideWaitSymbol();
						}
			});
		}

		private void reactonRenewalReminderSMS() {

			form.showWaitSymbol();
			commonAsync.implementationScreenOperation("Renewal Reminder To Client(SMS)", model.getCompanyId(), form.cbRenewalReminderSMS.getValue(), new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					form.showDialogMessage(result);
					form.hideWaitSymbol();
				}
			});
		
			
		}

		private void cronJobSconfigurationScreen() {
			AppMemory.getAppMemory().currentScreen = Screen.CRONJOBCONFIGRATION;
			genralInvoicePopup = new GeneralViewDocumentPopup(true);
			genralInvoicePopup.setModel(AppConstants.CRONJOBCONFIGRATION,new CronJobConfigration());
			generalPanel = new PopupPanel();
			generalPanel.add(genralInvoicePopup);
			generalPanel.show();
			generalPanel.center();
			genralInvoicePopup.btnClose.addClickHandler(this);
			
		}

		private void reactOnUpdateProcessConfig(final String processName, final String processType, Long companyId, final Boolean status, String name) {

			form.showWaitSymbol();
			
			commonAsync.activateDeactivateProcessConfigs(processName, processType, status, companyId, name, new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(String result) {
					form.showDialogMessage(result);
					form.hideWaitSymbol();
					updateinGlobalProcessConfig(processName,processType,status);
				}

				
			});
		}

		private void updateinGlobalProcessConfig(String processName,String processType, Boolean status) {

			for(ProcessConfiguration processConfigs : LoginPresenter.globalProcessConfig){
				if(processConfigs.getProcessName().trim().equals(processName.trim())){
					/**
					 * @author Anil @since 05-10-2021
					 * If process type is not already present against process name then add it
					 */
					boolean updateFlag=false;
					for(ProcessTypeDetails processtypedetails : processConfigs.getProcessList()){
						if(processtypedetails.getProcessName().trim().equalsIgnoreCase(processName.trim()) && 
								processtypedetails.getProcessType().trim().equalsIgnoreCase(processType.trim())){
							processtypedetails.setStatus(status);
							updateFlag=true;
						}
					}
					if(!updateFlag){
						ProcessTypeDetails processtypedetails =new ProcessTypeDetails();
						processtypedetails.setProcessName(processName);
						processtypedetails.setProcessType(processType);
						processtypedetails.setStatus(status);
						processConfigs.getProcessList().add(processtypedetails);
					}
				}
			}
		}
		




		private void reactonServiceCancellationReasons() {
			AppMemory.getAppMemory().currentScreen = Screen.CANCELLATIONREMARK;
			genralInvoicePopup = new GeneralViewDocumentPopup(true);
			genralInvoicePopup.setModel(AppConstants.SERVICECANCELLATIONREMARK,new Config());
			generalPanel = new PopupPanel();
			generalPanel.add(genralInvoicePopup);
			generalPanel.show();
			generalPanel.center();
			genralInvoicePopup.btnClose.addClickHandler(this);
		
		
		}

		private void reactonServiceRescheduleReasons() {
			AppMemory.getAppMemory().currentScreen = Screen.SPECIFICREASONFORRESCHEDULING;
			genralInvoicePopup = new GeneralViewDocumentPopup(true);
			genralInvoicePopup.setModel(AppConstants.SPECIFICREASONFORRESCHEDULING,new Config());
			generalPanel = new PopupPanel();
			generalPanel.add(genralInvoicePopup);
			generalPanel.show();
			generalPanel.center();
			genralInvoicePopup.btnClose.addClickHandler(this);
//			Timer timer = new Timer() {
//				
//				@Override
//				public void run() {
//					AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Settings/Reschedule Reasons",Screen.SPECIFICREASONFORRESCHEDULING);
//
//				}
//			};
//			timer.schedule(2000);

		}

		private void reactonQuotationMandatory() {

			form.showWaitSymbol();
			commonAsync.implementationScreenOperation("Quotation Mandatory", model.getCompanyId(), form.cbquotationMandatory.getValue(), new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol();
						}

						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							form.showDialogMessage(result);
							form.hideWaitSymbol();
						}
			});
		
		
		}

		private void reactonHideBankDeatilsOnPDF() {

			form.showWaitSymbol();
			commonAsync.implementationScreenOperation("Hide Bank Details On PDF", model.getCompanyId(), form.cbHideBankDetailsonPDF.getValue(), new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol();
						}

						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							form.showDialogMessage(result);
							form.hideWaitSymbol();
						}
			});
		
		}

		private void reactonContractLossRenewalsReasons() {
			AppMemory.getAppMemory().currentScreen = Screen.DONOTRENEW;
			genralInvoicePopup = new GeneralViewDocumentPopup(true);
			genralInvoicePopup.setModel(AppConstants.DONOTRENEW,new Config());
			generalPanel = new PopupPanel();
			generalPanel.add(genralInvoicePopup);
			generalPanel.show();
			generalPanel.center();
			genralInvoicePopup.btnClose.addClickHandler(this);
		}

		private void reactonSingleOrOneTimeContract() {
			form.showWaitSymbol();
			commonAsync.implementationScreenOperation("One Time Service Contract", model.getCompanyId(), form.cbOneTimeContract.getValue(), new AsyncCallback<String>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							form.hideWaitSymbol();
						}

						@Override
						public void onSuccess(String result) {
							// TODO Auto-generated method stub
							form.showDialogMessage(result);
							form.hideWaitSymbol();
						}
			});
		}


		private void reactonNonTaxNumberRange() {
			form.showWaitSymbol();
			commonAsync.implementationScreenOperation("Non Tax Number Range", model.getCompanyId(), true, new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
					form.showDialogMessage(result);

				}
			});
		}

		private void reactOnTaxNumberRange() {
			form.showWaitSymbol();
			commonAsync.implementationScreenOperation("Tax Number Range", model.getCompanyId(), true, new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();

				}

				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
					form.showDialogMessage(result);

				}
			});
		}

		private void reactOnBranchAsCompany() {
			form.showWaitSymbol();
			Console.log("model.getCompanyId()="+model.getCompanyId()+" form.cbCityNotMandatory.getValue()="+form.cbCityNotMandatory.getValue());			
			commonAsync.implementationScreenOperation("Define Branch As Company", model.getCompanyId(),form.cbBranchAsCompany.getValue(), new AsyncCallback<String>() {
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(String result) {
					// TODO Auto-generated method stub
					form.showDialogMessage(result);
					form.hideWaitSymbol();
				}
			});
		}
		
		private void reactOnCityNotMandatory() {
			try{
				Console.log("in reactOnCityNotMandatory()");
				form.showWaitSymbol();
				Console.log("model.getCompanyId()="+model.getCompanyId()+" form.cbCityNotMandatory.getValue()="+form.cbCityNotMandatory.getValue());
				commonAsync.implementationScreenOperation("City Not Mandatory", model.getCompanyId(),form.cbCityNotMandatory.getValue(), new AsyncCallback<String>() {
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
					}

					@Override
					public void onSuccess(String result) {
						// it gives null pointer exception to result. result is null. 
						Console.log("result="+result);
						form.showDialogMessage(result);
						form.hideWaitSymbol();
					}
				});
				
			}
			catch(Exception e){
				e.printStackTrace();
				Console.log(e.getMessage());
			}
			
		}


		private void reactOnCompanyBank() {
			AppMemory.getAppMemory().currentScreen = Screen.COMPANYPAYMENT;
			genralInvoicePopup = new GeneralViewDocumentPopup(true);
			genralInvoicePopup.setModel(AppConstants.COMPANYPAYMENT,new CompanyPayment());
			generalPanel = new PopupPanel();
			generalPanel.add(genralInvoicePopup);
			generalPanel.show();
			generalPanel.center();
			genralInvoicePopup.btnClose.addClickHandler(this);
		
		}

		private void reactOnUser() {
//			AppMemory.getAppMemory().currentScreen = Screen.USER;
//			genralInvoicePopup = new GeneralViewDocumentPopup(true);
//			genralInvoicePopup.setModel(AppConstants.USER,new User());
//			generalPanel = new PopupPanel();
//			generalPanel.add(genralInvoicePopup);
//			generalPanel.show();
//			generalPanel.center();
//			genralInvoicePopup.btnClose.addClickHandler(this);
			
			AppMemory.getAppMemory().currentScreen = Screen.QUICKEMPLOYEESCREEN;
			genralInvoicePopup = new GeneralViewDocumentPopup(true);
			genralInvoicePopup.setModel(AppConstants.QUICKEMPLOYEE,new Employee());
			generalPanel = new PopupPanel();
			generalPanel.add(genralInvoicePopup);
			generalPanel.show();
			generalPanel.center();
			genralInvoicePopup.btnClose.addClickHandler(this);
					
		}

		private void reactOnCity() {
			AppMemory.getAppMemory().currentScreen = Screen.CITY;
			genralInvoicePopup = new GeneralViewDocumentPopup(true);
			genralInvoicePopup.setModel(AppConstants.CITY,new City());
			generalPanel = new PopupPanel();
			generalPanel.add(genralInvoicePopup);
			generalPanel.show();
			generalPanel.center();
			genralInvoicePopup.btnClose.addClickHandler(this);
		}

		private void reactOnLocality() {
			AppMemory.getAppMemory().currentScreen = Screen.LOCALITY;
			genralInvoicePopup = new GeneralViewDocumentPopup(true);
			genralInvoicePopup.setModel(AppConstants.LOCALITY,new Locality());
			generalPanel = new PopupPanel();
			generalPanel.add(genralInvoicePopup);
			generalPanel.show();
			generalPanel.center();
			genralInvoicePopup.btnClose.addClickHandler(this);
			
		
		}

		private void reactOnUserAutorization() {
			AppMemory.getAppMemory().currentScreen = Screen.USERAUTHORIZATION;
			genralInvoicePopup = new GeneralViewDocumentPopup(true);
			genralInvoicePopup.setModel(AppConstants.USERAUTHORIZATION,new UserRole());
			generalPanel = new PopupPanel();
			generalPanel.add(genralInvoicePopup);
			generalPanel.show();
			generalPanel.center();
			genralInvoicePopup.btnClose.addClickHandler(this);
			
		}

		private void reactOnItemProduct() {
			AppMemory.getAppMemory().currentScreen = Screen.ITEMPRODUCT;
			genralInvoicePopup = new GeneralViewDocumentPopup(true);
			genralInvoicePopup.setModel(AppConstants.ITEMPRODUCT,new ItemProduct());
			generalPanel = new PopupPanel();
			generalPanel.add(genralInvoicePopup);
			generalPanel.show();
			generalPanel.center();
			genralInvoicePopup.btnClose.addClickHandler(this);
					
		
		}

		private void reactOnServiceProduct() {
			AppMemory.getAppMemory().currentScreen = Screen.SERVICEPRODUCT;
			genralInvoicePopup = new GeneralViewDocumentPopup(true);
			genralInvoicePopup.setModel(AppConstants.SERVICEPRODUCT,new ServiceProduct());
			generalPanel = new PopupPanel();
			generalPanel.add(genralInvoicePopup);
			generalPanel.show();
			generalPanel.center();
			genralInvoicePopup.btnClose.addClickHandler(this);
		}

		private void reactonBranch() {
			AppMemory.getAppMemory().currentScreen = Screen.BRANCH;
			genralInvoicePopup = new GeneralViewDocumentPopup(true);
			genralInvoicePopup.setModel(AppConstants.BRANCH,new Branch());
			generalPanel = new PopupPanel();
			generalPanel.add(genralInvoicePopup);
			generalPanel.show();
			generalPanel.center();
			genralInvoicePopup.btnClose.addClickHandler(this);
					
		}

		private void reactOnCompany() {
			AppMemory.getAppMemory().currentScreen = Screen.COMPANY;
			genralInvoicePopup = new GeneralViewDocumentPopup();
			genralInvoicePopup.btnClose.addClickHandler(this);

			MyQuerry querry = new MyQuerry();

			Company c = new Company();
			Vector<Filter> filtervec = new Vector<Filter>();
			Filter filter = null;
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(c.getCompanyId());
			filtervec.add(filter);
			querry.setFilters(filtervec);
			
			
			querry.setQuerryObject(new Company());
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					for(SuperModel model : result){
						Company company = (Company) model;
						
						AppMemory.getAppMemory().currentScreen = Screen.COMPANY;
						genralInvoicePopup = new GeneralViewDocumentPopup();

						genralInvoicePopup.setModel(AppConstants.COMPANY,company);
						generalPanel = new PopupPanel();
						generalPanel.add(genralInvoicePopup);
						generalPanel.show();
						generalPanel.center();
						break;
					}
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		
		@Override
		public void reactToProcessBarEvents(ClickEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void reactOnPrint() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void reactOnEmail() {
			// TODO Auto-generated method stub
			
		}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub

	}

	private void reactonTypeOfTreatment(){
		AppMemory.getAppMemory().currentScreen = Screen.TYPEOFTREATMENT;
		if (genralInvoicePopup != null) {
			genralInvoicePopup.viewDocumentPanel.hide();
		}
		genralInvoicePopup = new GeneralViewDocumentPopup(true);
		genralInvoicePopup.setModel("TYPEOFTREATMENT",
				new Config());
		generalPanel = new PopupPanel();
		generalPanel.add(genralInvoicePopup);
		generalPanel.show();
		generalPanel.center();
		genralInvoicePopup.btnClose.addClickHandler(this);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("TYPEOFTREATMENT");
			}
		};
		timer.schedule(2000);
			
	}
	
	private void reactonFrequency() {
		AppMemory.getAppMemory().currentScreen = Screen.FREQUENCY1;
		if (genralInvoicePopup != null) {
			genralInvoicePopup.viewDocumentPanel.hide();
		}
		genralInvoicePopup = new GeneralViewDocumentPopup(true);
		genralInvoicePopup.setModel(AppConstants.FREQUENCY,
				new Config());
		generalPanel = new PopupPanel();
		generalPanel.add(genralInvoicePopup);
		generalPanel.show();
		generalPanel.center();
		genralInvoicePopup.btnClose.addClickHandler(this);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("FREQUENCY");
			}
		};
		timer.schedule(2000);
		
	}
	private void reactonFindingType() {
		AppMemory.getAppMemory().currentScreen = Screen.CATCHTRAPPESTNAME;
		if (genralInvoicePopup != null) {
			genralInvoicePopup.viewDocumentPanel.hide();
		}
		genralInvoicePopup = new GeneralViewDocumentPopup(true);
		genralInvoicePopup.setModel("FindingType",
				new Config());
		generalPanel = new PopupPanel();
		generalPanel.add(genralInvoicePopup);
		generalPanel.show();
		generalPanel.center();
		genralInvoicePopup.btnClose.addClickHandler(this);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("FindingType");
			}
		};
		timer.schedule(2000);
		
	}
	private void reactonServiceChecklist() {
		AppMemory.getAppMemory().currentScreen = Screen.SERVICECHECKLIST;
		if (genralInvoicePopup != null) {
			genralInvoicePopup.viewDocumentPanel.hide();
		}
		genralInvoicePopup = new GeneralViewDocumentPopup(true);
		genralInvoicePopup.setModel("SERVICECHECKLIST",
				new Config());
		generalPanel = new PopupPanel();
		generalPanel.add(genralInvoicePopup);
		generalPanel.show();
		generalPanel.center();
		genralInvoicePopup.btnClose.addClickHandler(this);
		Timer timer = new Timer() {
			@Override
			public void run() {
				applyHandlerOnProcessBarMenu("SERVICECHECKLIST");
			}
		};
		timer.schedule(2000);
		
	}
	
	public void applyHandlerOnProcessBarMenu(String tabName){
		Console.log("applyHandlerOnProcessBarMenu "+tabName);
		viewedScreenName=tabName;
		if(tabName.equals("TYPEOFTREATMENT")||tabName.equals("SERVICECHECKLIST")||tabName.equals("FindingType")||tabName.equals("FREQUENCY")){
			ConfigForm view=(ConfigForm) genralInvoicePopup.getPresenter().getView();

			if (view.getProcessLevelBar() != null) {
				InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
				for (int k = 0; k < lbl.length; k++) {
					lbl[k].addClickHandler(this);
				}
			}
		}
	}
	
		
	private void reactOnPopupNavigation(ClickEvent event) {
		Console.log("ImplementationPresenter-reactOnPopupNavigation");
		String text="";
		
		try{
			InlineLabel label=(InlineLabel) event.getSource();
			text=label.getText().trim();
			Console.log("Implementation "+text);
			if(text.equals("")){
				return;
			}
		
			Console.log("text="+text);
			Console.log("viewedScreenName="+viewedScreenName);

			if(viewedScreenName.equals("TYPEOFTREATMENT")){
				if(text.equals("New")) {
					reactonTypeOfTreatment();
				}
			}
				
			if(viewedScreenName.equals("SERVICECHECKLIST")){
				if(text.equals("New"))
					reactonServiceChecklist();
			}
			if(viewedScreenName.equals("FindingType")){
				if(text.equals("New"))
					reactonFindingType();
			}
			if(viewedScreenName.equals("FREQUENCY")){
				if(text.equals("New"))
					reactonFrequency();
			}
		
		}catch(Exception e){
			e.printStackTrace();
			Console.log(e.getMessage());
		}
	}
	
	private void reactOnCopyPedioConfiguration() {
		
		String url=Window.Location.getHref();
		Console.log("OUR URL : "+url);
		
		url=getUpdatedUrl(url);
		Console.log("OUR UPDATED URL : "+url);
		
		Company comp = new Company();
		String referenceUrlLink="my.evaerp505515.appspot.com";
		
		form.showWaitSymbol();
		commonAsync.setupPedio(comp.getCompanyId(), url,referenceUrlLink,new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("Failed Please try again!");
				form.hideWaitSymbol();
			}

			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
				form.showDialogMessage(result);
			}
		});
	}
	
	public String getUpdatedUrl(String urlCalled){
		try{
			if(urlCalled.contains("-dot-")){
				urlCalled=urlCalled.replace("-dot-",".");
			}
			if(urlCalled.contains("https://")){
				urlCalled=urlCalled.replace("https://","");
			}
			if(urlCalled.contains("http://")){
				urlCalled=urlCalled.replace("http://","");
			}
			if(urlCalled.contains("/")){
				urlCalled=urlCalled.replace("/", "").trim();
			}
		}catch(Exception e){
			
		}
		return urlCalled;
	}
	private void callShareCustomerPortalLink(boolean sms,boolean whatsapp,boolean email) {
		// TODO Auto-generated method stub
		CommonServiceAsync commonSer = GWT.create(CommonService.class);
		commonSer.shareCustomerPortalLink(model.getCompanyId(),sms,whatsapp,email, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				// TODO Auto-generated method stub
				form.showDialogMessage("Process has started. You will receive an report of this process on company email id");	
				custportalPopup.hidePopUp();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.showDialogMessage("Process failed");	
				custportalPopup.hidePopUp();
			}
		});
	}

}
