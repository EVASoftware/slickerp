package com.slicktechnologies.client.views.implementation;

import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class ImplementationForm extends FormScreen<DummyEntityOnlyForScreen>  {

	/** The tb lead title. */
	TextBox tbLeadTitle;
	
	FormField fgroupingLeadInformation;

//	InlineLabel[]menuLabels;
	Label[]menuLabels;

	
	Label lblSetup;

	Hyperlink company,Branch,defineBranchAsCompany,billing,nonTaxNumberRange,user,serviceProduct,itemProduct,enableMakerChecker,
	   authorization,restrictUserLocationAccess,Locality,city;

	Hyperlink bank,branchWiseInvoiceNumberRange,invoicePrifix,enableAccountingInterface,outstandingDueList,outstandingDuetoClientEmail;
	Hyperlink singleOrOnetimeCOntract,contractlossRenewalReasons,renewalReminderToclient, contractRenewallistReminderEmail,enableAccountsProcessConfigs,
	  hideBankDetailsonPDF, quotationMandatory,renewalRemindersms,printServiceAddressOnRenewal,doNotPrintDiscountColumnOnRenewalPrint;
	
	CheckBox cbBranchAsCompany,cbEnableSelfApproval,cbOutstandingDuelist,cbOutstandingtoClientEmail,cbBranchLevelRestriction,cbCustomerBranchRestriction,cbDisableIPAddressEmail;
	CheckBox cbOneTimeContract,cbHideBankDetailsonPDF,cbquotationMandatory,cbRenewalReminderlistEmail,cbRenewalReminderToClient,cbRenewalReminderSMS,cbprintServiceAddressOnRenewal,cbDoNotPrintDiscountColumnOnRenewalPrint;
	CheckBox cbCityNotMandatory; //By Ashwini
	/**@Sheetal : 23-02-2022
	 *  Des : Adding checkboxes for 2 process configurations,requirement by nitin sir**/
	CheckBox cbNumberRangeMandatory, cbBranchWiseOrCompanyWiseNumberRange,cbMakeNumberRangeReadOnly;
	Hyperlink serviceRescheduleReasons, serviceCancellationReasons, activateSchedulingProcessConfig;
	Hyperlink typeOfTreatment, frequency, findingType, serviceChecklist;//Ashwini Patil Date:22-09-2022
	Hyperlink activateDeactivateServiceProcessConfigs;
	Hyperlink activateDeactivateInventoryProcessConfigs;

	CheckBox cbQuotationSelfApproval, cbContractSelfApproval,cbSalesQuotationSelfApproval,cbSalesOrderSelfApproval,cbPOSelfApproval,cbGRNSelfApproval,cbServicePOSelfApproval,
			cbMRNSelfApproval,cbMMNSelfApproval,cbMINSelfApproval,cbBillingSelfApproval,cbInvoiceSelfApproval,cbVendorInvoiceSelfApproval,
	        cbSalesPriceMandatory,cbPurchasePriceMandatory,cbSalesOrderMakeReferenceNumberMandatory;//@Sheetal:19-03-2022,for making sales or purchase price mandatory
	
	CheckBox cbPaymentDateMandatory,cbPaymentReceivedDateMandatory; //Ashwini Patil Date:4-08-2022
	CheckBox cbOnlyForEva,cbEditApprovedContract,cbRestrictContractsDownloadWithOTP,cbFrequencyAtProductLevel,cbMakeFrequencyMandatoryAtProductLevel,cbShowDeleteDuplicateServiceButton,cbPC_DoNotPrintQuantity,cbPedioContract,cbMAKEPRODUCTNAMENONEDITABLE,cbMakeServiceTypeMandatory,cbRemoveServiceAddressFromBranchPopup,cbMakeTermsUploadMandatory,cbPC_DoNotPrintProductWarrantyOnContract; //Ashwini Patil Date:3-09-2022
	CheckBox cbMAKETECHNICIANREMARKMANDITORY,cbMAKECUSTOMERNAMEMANDITORY,cbMAKECUSTOMERREMARKMANDITORY,cbHIDESERVICEFINDING,cbHIDEMATERIALS,cbSHOWEXPENSE,cbDIRECTCOMPLETE,cbTECHNICIANATTENDANCE,cbSITEFINDING;
	CheckBox cbMAKEFINDINGSMANDATORY,cbMAKETYPEOFTREATMENTMANDATORY;
	//Ashwini Patil Date:13-09-2022
	CheckBox cbREJECTSERVICE,cbHIDESRS,cbLIVETRACKING,cbSENDOTPFORSERVICECOMPLETION,cbPC_PEDIO_HIDE_TREATMENTS,cbPC_ADDMIXINGRATIOANDLOTNO,cbPC_DocumentUploadMandatory,cbPC_Pedio_Hide_Reschedule,cbPC_Hide_Customer_Mobile,cbLoadLoggedInUserWiseCustomer,cbPREMISESFLAG;
	CheckBox cbMARKSERVICECOMPLETEDFROMAPP, cbSHOWEMAIL,cbPEDIO_HIDEPHOTOS,cbShowProductPremises,cbPC_DoNotShowRatings,cbMAKESTARRATINGMANDITORY,cbPC_HideCustomerRemark;
	/**
	 * @author Anil @since 05-10-2021
	 */
	CheckBox cbNumberRangeMandatoryOnLead,cbNumberRangeMandatoryOnQuotation;
	
	
	TabPanel setuptabCheckBoxPanel,accountsTabCheckboxpanel,serviceSchedulingTabPanel,serviceTabPanel;
	
	CheckBox cbInvoiceAccoutingInterface, cbpaymentAccoutingInterface, cbvendorInvoiceAccoutingInterface, cbvendorPaymentAccoutingInterface,
				cbExpenseAccoutingInterface, cbPayrollAccountingInterface;
	
	// Accounts Module process Configs
	CheckBox cbBillSelfApproval, cbMakeServiceCompletionMandatoryForAMCContract, cbPrintProductDiscriptionOnPdfInInvoice, cbEnableAutoInvoice, 
		cbEnableServiceBranch,cbPC_DoNot_Print_Services,cbMakeInvoiceGroupMandatory,cbPrintDescriptionOnInvoicefromContract,cbPrintDescriptionOnInvoicefromSalesOrder,cbPC_NewInvoiceFormat,cbGenerateEinvoiceFromReferenceNumber,cbEnableInvoiceIntegrationWithZohoBooks;

	CheckBox cbCompanyAsLetterHead,cbinvoiceAccountingInterface,cbinvoiceSelfApproval,cbPrintProductPrimicesinPDF,cbDeliveryNoteDailyEmail,
		cbInvoiceEditAllRoll,cbmakeAddressToUppercase,cbPrintPoDetailsOnInvoice,cbHideRateandDiscountColumn;

	CheckBox cbPaymentAccountningInterface,cbPaymentAddIntoPettyCash,cbPaymentCompanyAsLetterHead;

	CheckBox cbPrintOrderNoAndGrnNo;
	
	Label lblblank;
	
	// Service Scheduling
//	cbautoEmailJobCardOnCompletion
	CheckBox cbTransferMaterialWhileScheduling,cbComplainServiceWithTurnAroundTime,
		cbCloseComplaintWhenServiceClose,cbSendSrCopyOnServiceCompletion;
	
	//Ashwini Patil Date:14-09-2022
	CheckBox cbCustomizedSrCopy, cbMAKEMATERIALMANDATORY, cbMakeServiceEditableAfterCompletion, cbMultipleExpenseMngt,cbDATETECHNICIANWISESERVICEPOPUP;
	CheckBox cbHideCompleteRescheduleFumigationColoumn,cbHideFumigationButton,cbCOMPLAINSERVICEWITHTURNAROUNDTIME,cbEmailCustomizedAndGenericSRCopy,cbPC_SERVICESCHEDULEMINMAXROUNDUP,cbPC_HIDE_CS_LIST_PLAN_BUTTON,cbPC_PedioFindingAsServiceQuantityInBilling,cbENABLECOMPLETEBUTTONONSCHEDULEPOPUP;//Ashwini Patil Date:15-03-2023
	CheckBox cbHideServiceValueExceptAdmin,cbRestrictServiceDownloadWithOTP,cbStopServiceRescheduleEmail,cbUpdateFindingEntity;
	
	CheckBox cbEnablePedio, cbCloseServiceWithOTP, cbReturnQtyUpdate, cbServiceScheduleMaterailMandatory,
		cbCopyMaterialToRemainingServices, cbEnableFcmNotification, cbEnableServiceScheduleNotification;
	CheckBox cbprintCustomerRemark,cbPrintTreatmentOnSRCopy,cbTechnicianRemark,cbPrintPhotoOnSRCopy,cbPrintFindingOnSRCopy,cbPedioNotificationRemark,
		cbhideMobileNo,cbhideCustomerFeedback,cbSendOTPForServiceCompletion,cbUploadSRCopy;
	CheckBox cbHIDEMATERIALANDUOM,cbDecreaseTableRow,cbPC_HideCellNumber_SRCopy,cbPC_DoNotPrintMaterialOnSummarySR,cbPRINTMATERIALONSRCOPY,cbPRINTPHOTOONSRCOPY;//Ashwini Patil Date:15-03-2023
	

	// Service tab
	CheckBox cbMakeCustomerEmailMandatory, cbMakeCustGroupMandatory, cbMakeCustPinCodeMandatory, cbMakeCustRefMandatory,
		cbMakeCustLevelMandatory,cbHideCustomerGroup,cbMakecustCategoryAndTypeDisable,cbMakeCellNoNonMandatory,cbMakeAccountManagerMandatory,cbMakeSalesPersonMandatory,cbEnableloadLargeCustomerData,cbHideCustomerReligion;
	CheckBox cbMakeLeadGroupMandatory,cbLeadCustPopupEmailMandatory,cbMakeCustomerPopUpCompanyNameMandatory,cbMakeCustomerPopUpPhoneNumberMandatory,
		cbEnableFreezeLead, cbCommunicationLogColumn, cbMapCustomerTypeToLeadCategory,cbMakePriceColumnDisable,
		cbChangeProductComposite, cbDisableEmailIdValidation, cbHideNumberRange;//Adding hide number range by sheetal:16-02-2022
	CheckBox cbMakeQuotationGroupMandatory, cbQuotationTypeMandatory, cbQuotationCategoryMandatory, cbDonotProductDescription,
		cbPrintProductDescriptionOnPDF, cbMakeQuotationDateDisable, cbMakeLeadIdMandatory,cbprintProductRemarkWithPremise,cbPrintProductPrimiseInQuotationPdf,cbPC_DoNotPrintProductWarrantyOnQuotation;
	CheckBox cbMakeContractGroupMandatory,cbMakeContractTypeMandatory,cbPrintProductPrimiseInPdf, cbMakeNumberRangeMandatory,
		cbMakeContractDateDisable;
	
	Button btnSetupPedio;
	Button btnSharePortalLink;//Ashwini Patil Date:24-1-2023


	protected static FormStyle DEFAULT_FORM_STYLE=FormStyle.ROWFORM;

	public  ImplementationForm() {
		super(DEFAULT_FORM_STYLE);
		createGui();
	}
	
	
	private void initializeWidget() {
		tbLeadTitle = new TextBox();
		
		company = new Hyperlink("Company", "Company");
		company.setTitle("Define Tax,  Header, Footer, Logo, Digital Signature");
		
		Branch = new Hyperlink("Branch", "Branch");
		defineBranchAsCompany = new Hyperlink("Define Branch As Company","Define Branch As Company");
		billing = new Hyperlink("Tax Number Range","Tax Number Range");
		billing.setTitle("Billing Number Range - 1YYYYY0000");
		nonTaxNumberRange = new Hyperlink("Non Tax Number Range","Non Tax Number Range");
		nonTaxNumberRange.setTitle("Activate process configuration, Number Range Mandatory, Non Billing - 8YYYYY0000");
		user = new Hyperlink("Quick Employee","Quick Employee");
		serviceProduct = new Hyperlink("Service Product","Service Product");
		itemProduct = new Hyperlink("Item Product","Item Product");
		enableMakerChecker = new Hyperlink("Enable/Disable Self Approval","Enable/Disable Self Approval");
//		enableMakerChecker.setTitle("Enable Self Approval");
//		disableMakerChecker = new Hyperlink("Disable Maker/Cheker","Disable Maker/Cheker");
//		disableMakerChecker.setTitle("Disable Self Approval");

		authorization = new Hyperlink("Authorization","Authorization");
		restrictUserLocationAccess = new Hyperlink("Restrict User Location Access","Restrict User Location Access");
		restrictUserLocationAccess.setTitle("Restrict user accessing ERP from home or any particular location");
		
		Locality = new Hyperlink("Locality","Locality");
		city = new Hyperlink("City","City");
		
		bank = new Hyperlink("Bank","Bank");
		branchWiseInvoiceNumberRange = new Hyperlink("Branch Wise Invoice Number Range","Branch Wise Invoice Number Range");
		invoicePrifix = new Hyperlink("Invoice Prefix","Invoice Prefix");
		enableAccountingInterface = new Hyperlink("Enable Accounting Interface","Enable Accounting Interface");
		outstandingDueList = new Hyperlink("Outstanding Reminder Due List","Outstanding Reminder Due List");
		outstandingDuetoClientEmail = new Hyperlink("Outstanding Due Email To Client","Outstanding Due Email To Client");
		
		singleOrOnetimeCOntract = new Hyperlink("Single Or One-Time Service Contract","Single Or One-Time Service Contract");
		contractlossRenewalReasons = new Hyperlink("Contract Loss Renewal Reasons","Contract Loss Renewal Reasons");
//		renewalReminderToclient = new Hyperlink("Renewal Reminder To Client (Email)","Renewal Reminder To Client (Email)");
		renewalReminderToclient = new Hyperlink("Auto Renewal Reminder To Client (Email)","Auto Renewal Reminder To Client (Email)");
		contractRenewallistReminderEmail = new Hyperlink("Contract Renewal List Reminder Daily","Contract Renewal List Reminder Daily");
		
		
		cbBranchAsCompany = new CheckBox();
		cbBranchAsCompany.setName("Branch As Company");
		cbBranchAsCompany.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany"));
		cbBranchAsCompany.setTitle("Activate process configuration, Need to update company details on branch");
		
		Console.log("before initializing cbCityNotMandatory");
		cbCityNotMandatory = new CheckBox();
		cbCityNotMandatory.setName("City Not Mandatory");
		cbCityNotMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","PC_CITY_NOT_MANDATORY"));
		cbCityNotMandatory.setTitle("Activate process configuration, Need to update Customer details on city");
		Console.log("after initializing cbCityNotMandatory="+cbCityNotMandatory.getValue());
		
		cbNumberRangeMandatory = new CheckBox();
		cbNumberRangeMandatory.setName("Number Range Mandatory");
		cbNumberRangeMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange","MAKENUMBERRANGEMANDATORY"));
		
		cbMakeNumberRangeReadOnly = new CheckBox();
		cbMakeNumberRangeReadOnly.setName("Make Number Range Read Only");
		cbMakeNumberRangeReadOnly.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange","MakeNumberRangeReadOnly"));
				
		cbBranchWiseOrCompanyWiseNumberRange = new CheckBox();
		cbBranchWiseOrCompanyWiseNumberRange.setName("Branch wise or company wise number range (customized)");
		cbBranchWiseOrCompanyWiseNumberRange.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("NumberRange","NUMBERRANGEPROCESS"));
		
		cbEnableSelfApproval = new CheckBox();
		cbEnableSelfApproval.setValue(false);
		
		cbOutstandingDuelist = new CheckBox();
		cbOutstandingDuelist.setValue(false);
		
		cbOutstandingtoClientEmail = new CheckBox();
		cbOutstandingtoClientEmail.setValue(false);
		cbOutstandingtoClientEmail.setName("Outstanding Due Email To Client");
		cbOutstandingtoClientEmail.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CronJob", "PaymentARDueDailyEmailToClient"));

		
		enableAccountsProcessConfigs = new Hyperlink("Enable Process Configuration","Enable Process Configuration");
		cbOneTimeContract = new CheckBox();
		cbOneTimeContract.setName("Single Or One Time Contract");
		cbOneTimeContract.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation", AppConstants.SELFAPPROVAL));

		hideBankDetailsonPDF = new Hyperlink("Hide Bank Details On PDF","Hide Bank Details On PDF");
		cbHideBankDetailsonPDF = new CheckBox();
		cbHideBankDetailsonPDF.setName("Hide Bank Details On PDF");
		cbHideBankDetailsonPDF.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "HIDEBANKDETAILS"));

		
		quotationMandatory = new Hyperlink("Quotation Mandatory","Quotation Mandatory");
		renewalRemindersms = new Hyperlink("Renewal Reminder To Client (SMS)", "Renewal Reminder To Client (SMS)");
		cbRenewalReminderSMS = new CheckBox();
		cbRenewalReminderSMS.setName("Renewal Reminder To Client (SMS)");
		cbRenewalReminderSMS.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CronJob", "ContractRenewalSMS"));
		
		printServiceAddressOnRenewal = new Hyperlink("Print Service Address On Renewal", "Print Service Address On Renewal");
		cbprintServiceAddressOnRenewal = new CheckBox();
		cbprintServiceAddressOnRenewal.setName("Print Service Address On Renewal");
		cbprintServiceAddressOnRenewal.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "printServiceAddressOnRenewal"));
		
		doNotPrintDiscountColumnOnRenewalPrint=new Hyperlink("Do Not Print Discount Column On Renewal Print","Do Not Print Discount Column On Renewal Print");
		cbDoNotPrintDiscountColumnOnRenewalPrint = new CheckBox();
		cbDoNotPrintDiscountColumnOnRenewalPrint.setName("Do Not Print Discount Column On Renewal Print");
		cbDoNotPrintDiscountColumnOnRenewalPrint.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "DoNotPrintDiscountColumnOnRenewalPrint"));
		
		
		cbRenewalReminderToClient = new CheckBox();
		cbRenewalReminderToClient.setName("Auto Renewal Reminder To Client (Email)");
		cbRenewalReminderToClient.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CronJob", "PC_ContractRenewalReminderToClient"));
		Console.log("cbRenewalReminderToClient"+(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CronJob", "PC_ContractRenewalReminderToClient")));

		cbRenewalReminderlistEmail = new CheckBox();
		cbRenewalReminderlistEmail.setName("Auto Renewal Reminder To Client (Email)");
		
		cbquotationMandatory = new CheckBox();
		cbquotationMandatory.setName("Quotation Mandatory");
		cbquotationMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "EnableQuotationMandatoryForContractRenwal"));

		
		serviceRescheduleReasons = new Hyperlink("Service Reschedule Reasons","Service Reschedule Reasons");
		serviceCancellationReasons = new Hyperlink("Service Cancellation Reasons","Service Cancellation Reasons");
		activateSchedulingProcessConfig = new Hyperlink("Activate/Deactivate Process Configuration","Activate/Deactivate Process Configuration");
		typeOfTreatment = new Hyperlink("Type Of Treatment ","Type Of Treatment ");
		frequency = new Hyperlink("Frequency","Frequency");
		findingType = new Hyperlink("Finding Type","Finding Type");
		serviceChecklist = new Hyperlink("Service Checklist","Service Checklist");
		
		
		
		
		activateDeactivateServiceProcessConfigs = new Hyperlink("Activate/Deactivate Process Configuration","Activate/Deactivate Process Configuration");
	
		activateDeactivateInventoryProcessConfigs = new Hyperlink("Activate/Deactivate Process Configuration","Activate/Deactivate Process Configuration");
	
		setuptabCheckBoxPanel = new TabPanel();
		setuptabCheckBoxPanel.setWidth("100%");
		setuptabCheckBoxPanel.getElement().getStyle().setMarginTop(-3, Unit.PX);
		
		cbQuotationSelfApproval = new CheckBox();
		cbQuotationSelfApproval.setName("Quotation Self Approval");
		cbQuotationSelfApproval.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation", AppConstants.SELFAPPROVAL));
		
		cbNumberRangeMandatoryOnLead = new CheckBox();
		cbNumberRangeMandatoryOnLead.setName("Number range mandatory on lead");
		cbNumberRangeMandatoryOnLead.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead", "PC_MakeNumberRangeMandatory"));
		
		cbNumberRangeMandatoryOnQuotation = new CheckBox();
		cbNumberRangeMandatoryOnQuotation.setName("Number range mandatory on quotation");
		cbNumberRangeMandatoryOnQuotation.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","PC_MakeNumberRangeMandatory"));

		cbContractSelfApproval = new CheckBox();
		cbContractSelfApproval.setName("Contract Self Approval");
		cbContractSelfApproval.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", AppConstants.SELFAPPROVAL));
		cbSalesQuotationSelfApproval = new CheckBox();
		cbSalesQuotationSelfApproval.setName("Sales Quotation Self Approval");
		cbSalesQuotationSelfApproval.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesQuotation", AppConstants.SELFAPPROVAL));

		cbSalesOrderSelfApproval = new CheckBox();
		cbSalesOrderSelfApproval.setName("Sales Order Self Approval");
		cbSalesOrderSelfApproval.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesOrder", AppConstants.SELFAPPROVAL));
		
		cbSalesOrderMakeReferenceNumberMandatory = new CheckBox();
		cbSalesOrderMakeReferenceNumberMandatory.setName("Make Reference Number Mandatory");
		cbSalesOrderMakeReferenceNumberMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesOrder", "MakeReferenceNumberMandatory"));

		cbPOSelfApproval = new CheckBox();
		cbPOSelfApproval.setName("Purchase Order Self Approval");
		cbPOSelfApproval.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", AppConstants.SELFAPPROVAL));

		cbGRNSelfApproval = new CheckBox();
		cbGRNSelfApproval.setName("GRN Self Approval");
		cbGRNSelfApproval.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("GRN", AppConstants.SELFAPPROVAL));

		cbServicePOSelfApproval = new CheckBox();
		cbServicePOSelfApproval.setName("Service PO Self Approval");
		cbServicePOSelfApproval.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ServicePo", AppConstants.SELFAPPROVAL));

		cbMRNSelfApproval = new CheckBox();
		cbMRNSelfApproval.setName("MRN Self Approval");
		cbMRNSelfApproval.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialRequestNote", AppConstants.SELFAPPROVAL));

		cbMINSelfApproval = new CheckBox();
		cbMINSelfApproval.setName("MIN Self Approval");
		cbMINSelfApproval.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialIssueNote", AppConstants.SELFAPPROVAL));

		cbMMNSelfApproval = new CheckBox();
		cbMMNSelfApproval.setName("MMN Self Approval");
		cbMMNSelfApproval.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialMovementNote", AppConstants.SELFAPPROVAL));

		cbBillingSelfApproval = new  CheckBox();
		cbBillingSelfApproval.setName("Billing Self Approval");
		cbBillingSelfApproval.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", AppConstants.SELFAPPROVAL));

		cbInvoiceSelfApproval = new CheckBox();
		cbInvoiceSelfApproval.setName("Invoice Self Approval");
		cbInvoiceSelfApproval.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", AppConstants.SELFAPPROVAL));

		cbVendorInvoiceSelfApproval = new CheckBox();
		cbVendorInvoiceSelfApproval.setName("Vendor Invoice Self Approval");
		cbVendorInvoiceSelfApproval.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("VendorInvoice", AppConstants.SELFAPPROVAL));
		
		cbPaymentDateMandatory = new CheckBox();
		cbPaymentDateMandatory.setName("Payment Date Mandatory");
		cbPaymentDateMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment","PaymentDateMandatory"));
		
		
		cbPaymentReceivedDateMandatory = new CheckBox();
		cbPaymentReceivedDateMandatory.setName("Payment Received Date Mandatory");
		cbPaymentReceivedDateMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment", "Paymentreceiveddatemandatory"));
		
		cbOnlyForEva = new CheckBox();
		cbOnlyForEva.setName("Display Edit button on Approved contract");
		cbOnlyForEva.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ONLYFOREVA"));
		
		cbEditApprovedContract = new CheckBox();
		cbEditApprovedContract.setName("Allow editing product and payment terms after approval");
		cbEditApprovedContract.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EditApprovedContract"));
		
		
		cbRestrictContractsDownloadWithOTP = new CheckBox();
		cbRestrictContractsDownloadWithOTP.setTitle("RestrictContractsDownloadWithOTP");
		cbRestrictContractsDownloadWithOTP.setName("Restrict Contracts Download With OTP");
		cbRestrictContractsDownloadWithOTP.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "RestrictContractsDownloadWithOTP"));
		
		
		cbFrequencyAtProductLevel = new CheckBox();
		cbFrequencyAtProductLevel.setTitle("Show Frequency column At Product Level");
		cbFrequencyAtProductLevel.setName("Show Frequency column At Product Level");
		cbFrequencyAtProductLevel.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "FrequencyAtProductLevel"));
		
		cbMakeFrequencyMandatoryAtProductLevel = new CheckBox();
		cbMakeFrequencyMandatoryAtProductLevel.setTitle("Make Frequency Mandatory At Product Level");
		cbMakeFrequencyMandatoryAtProductLevel.setName("Make Frequency Mandatory At Product Level");
		cbMakeFrequencyMandatoryAtProductLevel.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "MakeFrequencyMandatoryAtProductLevel"));
		
		
		cbShowDeleteDuplicateServiceButton = new CheckBox();
		cbShowDeleteDuplicateServiceButton.setTitle("ShowDeleteDuplicateServiceButton");
		cbShowDeleteDuplicateServiceButton.setName("Show Delete Duplicate Service Button");
		cbShowDeleteDuplicateServiceButton.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ShowDeleteDuplicateServiceButton"));
		
		cbPC_DoNotPrintQuantity = new CheckBox();
		cbPC_DoNotPrintQuantity.setTitle("cbPC_DoNotPrintQuantity");
		cbPC_DoNotPrintQuantity.setName("Do not print Quatity column");
		cbPC_DoNotPrintQuantity.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "PC_DoNotPrintQuantity"));
		
		
		cbPedioContract = new CheckBox();
		cbPedioContract.setTitle("PedioContract");
		cbPedioContract.setName("Create only service contract (No billing)");
		cbPedioContract.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "PedioContract"));
	
		cbMAKEPRODUCTNAMENONEDITABLE = new CheckBox();
		cbMAKEPRODUCTNAMENONEDITABLE.setTitle("MAKEPRODUCTNAMENONEDITABLE");
		cbMAKEPRODUCTNAMENONEDITABLE.setName("Make Product Name Non Editable");
		cbMAKEPRODUCTNAMENONEDITABLE.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "MAKEPRODUCTNAMENONEDITABLE"));
	
		
		cbMakeServiceTypeMandatory = new CheckBox();
		cbMakeServiceTypeMandatory.setTitle("MakeServiceTypeMandatory");
		cbMakeServiceTypeMandatory.setName("Make Service Type Mandatory");
		cbMakeServiceTypeMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "MakeServiceTypeMandatory"));
	
		
		cbRemoveServiceAddressFromBranchPopup = new CheckBox();
		cbRemoveServiceAddressFromBranchPopup.setTitle("RemoveServiceAddressFromBranchPopup");
		cbRemoveServiceAddressFromBranchPopup.setName("Remove 'Service Address' From BranchPopup");
		cbRemoveServiceAddressFromBranchPopup.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "RemoveServiceAddressFromBranchPopup"));
	
		cbMakeTermsUploadMandatory = new CheckBox();
		cbMakeTermsUploadMandatory.setTitle("MakeTermsUploadMandatory");
		cbMakeTermsUploadMandatory.setName("Make Terms and Conditions Upload Mandatory");
		cbMakeTermsUploadMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "MakeTermsUploadMandatory"));
					
		cbPC_DoNotPrintProductWarrantyOnContract = new CheckBox();
		cbPC_DoNotPrintProductWarrantyOnContract.setTitle("PC_DoNotPrintProductWarranty");
		cbPC_DoNotPrintProductWarrantyOnContract.setName("Do Not Print Product Warranty On Contract Print");
		cbPC_DoNotPrintProductWarrantyOnContract.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "PC_DoNotPrintProductWarranty"));
	
		cbSalesPriceMandatory = new CheckBox();
		cbSalesPriceMandatory.setName("Sales Price Mandatory");
		cbSalesPriceMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ItemProduct", "PC_ItemProduct_SalesPriceMandatory"));
		
		cbPurchasePriceMandatory = new CheckBox();
		cbPurchasePriceMandatory.setName("Purchase Price Mandatory");
		cbPurchasePriceMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ItemProduct", "PC_ItemProduct_PurchasePriceMandatory"));

		accountsTabCheckboxpanel = new TabPanel();
		accountsTabCheckboxpanel.setWidth("100%");
		
		cbInvoiceAccoutingInterface = new CheckBox();
		cbInvoiceAccoutingInterface.setName("Accounting Interface");
		cbInvoiceAccoutingInterface.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", AppConstants.PROCESSTYPEACCINTERFACE));

		cbpaymentAccoutingInterface = new CheckBox();
		cbpaymentAccoutingInterface.setName("Accounting Interface");
		cbpaymentAccoutingInterface.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment", AppConstants.PROCESSTYPEACCINTERFACE));

		cbvendorInvoiceAccoutingInterface = new CheckBox();
		cbvendorInvoiceAccoutingInterface.setName("Accounting Interface");
		cbvendorInvoiceAccoutingInterface.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("VendorInvoice", AppConstants.PROCESSTYPEACCINTERFACE));

		
		cbvendorPaymentAccoutingInterface = new CheckBox();
		cbvendorPaymentAccoutingInterface.setName("Accounting Interface");
		cbvendorPaymentAccoutingInterface.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("VendorPayment", AppConstants.PROCESSTYPEACCINTERFACE));

		
		cbExpenseAccoutingInterface = new CheckBox();
		cbExpenseAccoutingInterface.setName("Accounting Interface");
		cbExpenseAccoutingInterface.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("MultipleExpenseMngt", AppConstants.PROCESSTYPEACCINTERFACE));

		cbPayrollAccountingInterface = new CheckBox();
		cbPayrollAccountingInterface.setName("Accounting Interface");
//		cbPayrollAccountingInterface.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("MultipleExpenseMngt", AppConstants.PROCESSTYPEACCINTERFACE));

		cbBillSelfApproval = new CheckBox();
		cbBillSelfApproval.setName("Self Approval");
		cbBillSelfApproval.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", AppConstants.SELFAPPROVAL));

		cbMakeServiceCompletionMandatoryForAMCContract = new CheckBox();
		cbMakeServiceCompletionMandatoryForAMCContract.setName("Make Service Completion Mandatory For AMC Contract");
		cbMakeServiceCompletionMandatoryForAMCContract.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "MakeServiceCompletionMandatoryForAMCContract"));

		cbPrintProductDiscriptionOnPdfInInvoice = new CheckBox();
		cbPrintProductDiscriptionOnPdfInInvoice.setName("Print Product Description on PDF");
		cbPrintProductDiscriptionOnPdfInInvoice.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "PrintProductDescriptionOnPdf"));

		cbEnableAutoInvoice = new CheckBox();
		cbEnableAutoInvoice.setName("Enable Auto Invoice");
		cbEnableAutoInvoice.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "EnableAutoInvoice"));

		cbEnableServiceBranch = new CheckBox();
		cbEnableServiceBranch.setName("Enable Service Branch");
		cbEnableServiceBranch.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "EnableServiceBranch"));

		cbinvoiceSelfApproval = new CheckBox();
		cbinvoiceSelfApproval.setName("Invoice Self Approval");
		cbinvoiceSelfApproval.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", AppConstants.SELFAPPROVAL));

		cbinvoiceAccountingInterface = new CheckBox();
		cbinvoiceAccountingInterface.setName("Accounting Interface");
		cbinvoiceAccountingInterface.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", AppConstants.PROCESSTYPEACCINTERFACE));

		cbCompanyAsLetterHead = new CheckBox();
		cbCompanyAsLetterHead.setName("Company As Letter Head");
		cbCompanyAsLetterHead.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","CompanyAsLetterHead"));

		cbPrintProductPrimicesinPDF = new CheckBox();
		cbPrintProductPrimicesinPDF.setName("Print Product Primices in PDF");
		cbPrintProductPrimicesinPDF.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","printProductPremisesInPdf"));

		cbInvoiceEditAllRoll = new CheckBox();
		cbInvoiceEditAllRoll.setName("Enable Invoice Editable For All Roles");
		cbInvoiceEditAllRoll.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","EnableInvoiceEditableForAllRoles"));

		cbmakeAddressToUppercase = new CheckBox();
		cbmakeAddressToUppercase.setName("Make Address To Upper Case");
		cbmakeAddressToUppercase.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","MakeAdressToUppercase"));

		cbPrintPoDetailsOnInvoice = new CheckBox();
		cbPrintPoDetailsOnInvoice.setName("Print PO Details on Invoice");
		cbPrintPoDetailsOnInvoice.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","PrintPoDetailsOnInvoice"));

		cbHideRateandDiscountColumn = new CheckBox();
		cbHideRateandDiscountColumn.setName("Hide Rate And Discount Column");
		cbHideRateandDiscountColumn.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","HideRateAndDiscountColumn"));

		cbPaymentAccountningInterface = new CheckBox();
		cbPaymentAccountningInterface.setName("Accounting Interface");
		cbPaymentAccountningInterface.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment",AppConstants.PROCESSTYPEACCINTERFACE));

		cbPaymentAddIntoPettyCash = new CheckBox();
		cbPaymentAddIntoPettyCash.setName("Payment Add Into Petty Cash");
		cbPaymentAddIntoPettyCash.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment","PaymentAddIntoPettyCash"));

		cbPaymentCompanyAsLetterHead = new CheckBox();
		cbPaymentCompanyAsLetterHead.setName("Company As Letter Head");
		cbPaymentCompanyAsLetterHead.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment","CompanyAsLetterHead"));

		cbPrintOrderNoAndGrnNo = new CheckBox();
		cbPrintOrderNoAndGrnNo.setName("Print OrderNo And GRN No");
		cbPrintOrderNoAndGrnNo.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("VendorInvoice","PrintOrderNoAndGrnNo"));

		lblblank = new Label("");
		
		serviceSchedulingTabPanel = new TabPanel();
		serviceSchedulingTabPanel.setWidth("100%");
		
		cbTransferMaterialWhileScheduling = new CheckBox();
		cbTransferMaterialWhileScheduling.setName("Transfer Material While Scheduling");
//		cbTransferMaterialWhileScheduling.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("VendorInvoice","PrintOrderNoAndGrnNo"));

//		cbautoEmailJobCardOnCompletion = new CheckBox();
//		cbautoEmailJobCardOnCompletion.setName("Enable Auto Job Card On Service Completion");
		
		cbComplainServiceWithTurnAroundTime = new CheckBox();
		cbComplainServiceWithTurnAroundTime.setName("Complain Service With Turn Around Time");
		cbComplainServiceWithTurnAroundTime.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","COMPLAINSERVICEWITHTURNAROUNDTIME"));

		cbCloseComplaintWhenServiceClose = new CheckBox();
		cbCloseComplaintWhenServiceClose.setName("Close Complain When Service Close");
		cbCloseComplaintWhenServiceClose.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ENABLECOMPLETECOMPLAINTSERVICE"));

		cbSendSrCopyOnServiceCompletion = new CheckBox();
		cbSendSrCopyOnServiceCompletion.setName("Send SR Copy On Service Completion");
		cbSendSrCopyOnServiceCompletion.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","SendSRCopyOnServiceCompletion"));

		
		cbCustomizedSrCopy = new CheckBox();
		cbCustomizedSrCopy.setName("Custom SR Copy");
		cbCustomizedSrCopy.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","CustomizedSrCopy"));

		cbMAKEMATERIALMANDATORY = new CheckBox();
		cbMAKEMATERIALMANDATORY.setName("Consmption Material Mandatory");
		cbMAKEMATERIALMANDATORY.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","MAKEMATERIALMANDATORY"));

		cbMakeServiceEditableAfterCompletion = new CheckBox();
		cbMakeServiceEditableAfterCompletion.setName("Enable Service Editing After Completion");
		cbMakeServiceEditableAfterCompletion.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","MakeServiceEditableAfterCompletion"));

		cbMultipleExpenseMngt = new CheckBox();
		cbMultipleExpenseMngt.setName("Allow Multiple Expenses");
		cbMultipleExpenseMngt.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","MultipleExpenseMngt"));

		cbDATETECHNICIANWISESERVICEPOPUP = new CheckBox();
		cbDATETECHNICIANWISESERVICEPOPUP.setName("Enable Schedule Print Without Team");
		cbDATETECHNICIANWISESERVICEPOPUP.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","DATETECHNICIANWISESERVICEPOPUP"));

		cbHideCompleteRescheduleFumigationColoumn = new CheckBox();
		cbHideCompleteRescheduleFumigationColoumn.setName("Disable Complete and Reschedule from customer service list. Disable Fumigation.");
		cbHideCompleteRescheduleFumigationColoumn.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","HideCompleteRescheduleFumigationColoumn"));

		cbHideFumigationButton = new CheckBox();
		cbHideFumigationButton.setName("Disable Fumigation from Customer Service List");
		cbHideFumigationButton.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","HideFumigationButton"));

		cbCOMPLAINSERVICEWITHTURNAROUNDTIME = new CheckBox();
		cbCOMPLAINSERVICEWITHTURNAROUNDTIME.setName("Enable Complaint Service Turn Around Time");
		cbCOMPLAINSERVICEWITHTURNAROUNDTIME.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","COMPLAINSERVICEWITHTURNAROUNDTIME"));

		cbEmailCustomizedAndGenericSRCopy = new CheckBox();
		cbEmailCustomizedAndGenericSRCopy.setName("Auto email service report on completion");
		cbEmailCustomizedAndGenericSRCopy.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","EmailCustomizedAndGenericSRCopy"));

		cbPC_SERVICESCHEDULEMINMAXROUNDUP = new CheckBox();
		cbPC_SERVICESCHEDULEMINMAXROUNDUP.setName("Enable Monthly Scheduling ");
		cbPC_SERVICESCHEDULEMINMAXROUNDUP.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","PC_SERVICESCHEDULEMINMAXROUNDUP"));

		cbPC_HIDE_CS_LIST_PLAN_BUTTON = new CheckBox();
		cbPC_HIDE_CS_LIST_PLAN_BUTTON.setName("Disable Plan Button On Customer Service List ");
		cbPC_HIDE_CS_LIST_PLAN_BUTTON.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","PC_HIDE_CS_LIST_PLAN_BUTTON"));
		
		cbPC_PedioFindingAsServiceQuantityInBilling = new CheckBox();
		cbPC_PedioFindingAsServiceQuantityInBilling.setName("Enable Findings as Quantity For Service Wise Billing");
		cbPC_PedioFindingAsServiceQuantityInBilling.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","PC_PedioFindingAsServiceQuantityInBilling"));

		cbENABLECOMPLETEBUTTONONSCHEDULEPOPUP = new CheckBox();
		cbENABLECOMPLETEBUTTONONSCHEDULEPOPUP.setName("Allow service completion from schedule popup");
		cbENABLECOMPLETEBUTTONONSCHEDULEPOPUP.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ENABLECOMPLETEBUTTONONSCHEDULEPOPUP"));

		cbHideServiceValueExceptAdmin = new CheckBox();
		cbHideServiceValueExceptAdmin.setTitle("HideServiceValueExceptAdmin");
		cbHideServiceValueExceptAdmin.setName("Hide Service Value Except Admin");
		cbHideServiceValueExceptAdmin.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","HideServiceValueExceptAdmin"));

		
		cbRestrictServiceDownloadWithOTP = new CheckBox();
		cbRestrictServiceDownloadWithOTP.setTitle("RestrictServiceDownloadWithOTP");
		cbRestrictServiceDownloadWithOTP.setName("Restrict Service Download With OTP");
		cbRestrictServiceDownloadWithOTP.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","RestrictServiceDownloadWithOTP"));

		
		cbStopServiceRescheduleEmail = new CheckBox();
		cbStopServiceRescheduleEmail.setTitle("StopServiceRescheduleEmail(Pedio,CustomerPortal). When service gets rescheduled from pedio or customer portal, reschedule email gets send to customer. To stop that email sending you can activate this process configuration.");
		cbStopServiceRescheduleEmail.setName("Stop sending Service Reschedule Email to customer when Rescheduled from Pedio or CustomerPortal");
		cbStopServiceRescheduleEmail.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","StopServiceRescheduleEmail(Pedio,CustomerPortal)"));

		
		cbUpdateFindingEntity = new CheckBox();
		cbUpdateFindingEntity.setTitle("Update Catch details in FindingEntity");
		cbUpdateFindingEntity.setName("Update Finding Entity");
		cbUpdateFindingEntity.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","UpdateFindingEntity"));

		
		cbEnablePedio = new CheckBox();
		cbEnablePedio.setName("Enable Pedio");
		cbReturnQtyUpdate = new CheckBox();
		cbReturnQtyUpdate.setName("Return Quantity Update");
		cbReturnQtyUpdate.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","MINBASEDONCONSUMEDQUANTITY"));

		
		cbServiceScheduleMaterailMandatory = new CheckBox();
		cbServiceScheduleMaterailMandatory.setName("Service Schedule Material Mandatory");
		cbServiceScheduleMaterailMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","WarehouseDetailsMandatory"));

		cbCopyMaterialToRemainingServices = new CheckBox();
		cbCopyMaterialToRemainingServices.setName("Copy Material To Remaining Services");
		cbCopyMaterialToRemainingServices.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","UpdateProjectMaterial"));

		cbEnableFcmNotification = new CheckBox();
		cbEnableFcmNotification.setName("Enable FCM Notification");
		boolean fcmFlag=false;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","EnableFcmNotification"))
			fcmFlag=true;
		else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","FCMNOTIFICATION"))
			fcmFlag=true;
		cbEnableFcmNotification.setValue(fcmFlag);//one checkbox for two configs

		cbEnableServiceScheduleNotification = new CheckBox();
		cbEnableServiceScheduleNotification.setName("Enable Service Schedule");
		cbEnableServiceScheduleNotification.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ENABLESERVICESCHEDULENOTIFICATION"));

		//Ashwini Patil Date:12-09-2022 
		
		cbMAKETECHNICIANREMARKMANDITORY = new CheckBox();
		cbMAKETECHNICIANREMARKMANDITORY.setName("Technician Remark Mandatory");
		cbMAKETECHNICIANREMARKMANDITORY.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","MAKETECHNICIANREMARKMANDITORY"));
		
		cbMAKECUSTOMERNAMEMANDITORY = new CheckBox();
		cbMAKECUSTOMERNAMEMANDITORY.setName("Signing Person Mandatory");
		cbMAKECUSTOMERNAMEMANDITORY.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","MAKECUSTOMERNAMEMANDITORY"));
		
		cbMAKECUSTOMERREMARKMANDITORY = new CheckBox();
		cbMAKECUSTOMERREMARKMANDITORY.setTitle("MAKECUSTOMERREMARKMANDITORY");
		cbMAKECUSTOMERREMARKMANDITORY.setName("Customer Remark Mandatory");
		cbMAKECUSTOMERREMARKMANDITORY.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","MAKECUSTOMERREMARKMANDITORY"));
				
		
		cbMAKEFINDINGSMANDATORY = new CheckBox();
		cbMAKEFINDINGSMANDATORY.setTitle("MAKEFINDINGSMANDATORY");
		cbMAKEFINDINGSMANDATORY.setName("Make findings Mandatory");
		cbMAKEFINDINGSMANDATORY.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","MAKEFINDINGSMANDATORY"));
		
		cbMAKETYPEOFTREATMENTMANDATORY = new CheckBox();
		cbMAKETYPEOFTREATMENTMANDATORY.setTitle("cbMAKETYPEOFTREATMENTMANDATORY");
		cbMAKETYPEOFTREATMENTMANDATORY.setName("Make Type Of Treatment Mandatory");
		cbMAKETYPEOFTREATMENTMANDATORY.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","MAKETYPEOFTREATMENTMANDATORY"));
		
		
		cbHIDESERVICEFINDING = new CheckBox();
		cbHIDESERVICEFINDING.setName("Disable Service Finding ");
		cbHIDESERVICEFINDING.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","HIDESERVICEFINDING"));
		
		cbHIDEMATERIALS = new CheckBox();
		cbHIDEMATERIALS.setName("Disable  Material Consumption");
		cbHIDEMATERIALS.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","HIDEMATERIALS"));
		
		cbSHOWEXPENSE = new CheckBox();
		cbSHOWEXPENSE.setName("Enable Expense");
		cbSHOWEXPENSE.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","SHOWEXPENSE"));
		
		cbDIRECTCOMPLETE = new CheckBox();
		cbDIRECTCOMPLETE.setName("Complete Service w/o Feedback");
		cbDIRECTCOMPLETE.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","DIRECTCOMPLETE"));
		
		cbTECHNICIANATTENDANCE = new CheckBox();
		cbTECHNICIANATTENDANCE.setName("Enable Technician Attendance");
		cbTECHNICIANATTENDANCE.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","TECHNICIANATTENDANCE"));
		
		cbSITEFINDING = new CheckBox();
		cbSITEFINDING.setName("Site Finding ");
		cbSITEFINDING.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","SITEFINDING"));
		
		cbREJECTSERVICE = new CheckBox();
		cbREJECTSERVICE.setName("Technician Can Reject Service");
		cbREJECTSERVICE.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","REJECTSERVICE"));
		
		cbHIDESRS = new CheckBox();
		cbHIDESRS.setName("Disable SR Print");
		cbHIDESRS.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","HIDESRS"));
		
		cbLIVETRACKING = new CheckBox();
		cbLIVETRACKING.setName("Enable Live Tracking");
		cbLIVETRACKING.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","LIVETRACKING"));
		
		cbSENDOTPFORSERVICECOMPLETION = new CheckBox();
		cbSENDOTPFORSERVICECOMPLETION.setName("Enable OTP for Service Completion");
		cbSENDOTPFORSERVICECOMPLETION.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","SENDOTPFORSERVICECOMPLETION"));
		
		cbPC_PEDIO_HIDE_TREATMENTS = new CheckBox();
		cbPC_PEDIO_HIDE_TREATMENTS.setName("Disable Treatments");
		cbPC_PEDIO_HIDE_TREATMENTS.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","PC_PEDIO_HIDE_TREATMENTS"));
	
		cbPC_ADDMIXINGRATIOANDLOTNO = new CheckBox();
		cbPC_ADDMIXINGRATIOANDLOTNO.setName("Enable Mixing Ration");
		cbPC_ADDMIXINGRATIOANDLOTNO.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","PC_ADDMIXINGRATIOANDLOTNO"));

		cbPC_DocumentUploadMandatory = new CheckBox();
		cbPC_DocumentUploadMandatory.setName("Upload Document Mandatory");
		cbPC_DocumentUploadMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","PC_DocumentUploadMandatory"));
		
		cbPC_Pedio_Hide_Reschedule = new CheckBox();
		cbPC_Pedio_Hide_Reschedule.setName("Disable Rescheduling");
		cbPC_Pedio_Hide_Reschedule.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","PC_Pedio_Hide_Reschedule"));
		
		cbPC_Hide_Customer_Mobile = new CheckBox();
		cbPC_Hide_Customer_Mobile.setName("Hide Customer Mobile Number ");
		cbPC_Hide_Customer_Mobile.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","PC_Hide_Customer_Mobile"));
		
		cbLoadLoggedInUserWiseCustomer = new CheckBox();
		cbLoadLoggedInUserWiseCustomer.setName("Restrict Customers by Logged in User");
		cbLoadLoggedInUserWiseCustomer.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Priora","LoadLoggedInUserWiseCustomer"));
		
		cbPREMISESFLAG = new CheckBox();
		cbPREMISESFLAG.setName("Enable Premise");
		cbPREMISESFLAG.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","PREMISESFLAG"));
		
		cbMARKSERVICECOMPLETEDFROMAPP = new CheckBox();
		cbMARKSERVICECOMPLETEDFROMAPP.setName("Mark Service Completed Automatically after T-Completed");
		cbMARKSERVICECOMPLETEDFROMAPP.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","MARKSERVICECOMPLETEDFROMAPP"));
		
		cbSHOWEMAIL = new CheckBox();
		cbSHOWEMAIL.setName("Edit EmailId to which SR copy to be sent after Service Completion form Pedio");
		cbSHOWEMAIL.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","SHOWEMAIL"));
		
		cbPEDIO_HIDEPHOTOS = new CheckBox();
		cbPEDIO_HIDEPHOTOS.setName("Disable Photos");
		cbPEDIO_HIDEPHOTOS.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","PEDIO_HIDEPHOTOS"));
		
		cbShowProductPremises = new CheckBox();
		cbShowProductPremises.setName("Display product premises in Service details");
		cbShowProductPremises.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ShowProductPremises"));
		
		cbPC_DoNotShowRatings = new CheckBox();
		cbPC_DoNotShowRatings.setName("Disable Customer Rating (Feedback)");
		cbPC_DoNotShowRatings.setTitle("PC_DoNotShowRatings");
		cbPC_DoNotShowRatings.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","PC_DoNotShowRatings"));
		
		cbMAKESTARRATINGMANDITORY = new CheckBox();
		cbMAKESTARRATINGMANDITORY.setName("Customer Rating (Feedback) Mandatory");
		cbMAKESTARRATINGMANDITORY.setTitle("MAKESTARRATINGMANDITORY");
		cbMAKESTARRATINGMANDITORY.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","MAKESTARRATINGMANDITORY"));
		
		cbPC_HideCustomerRemark = new CheckBox();
		cbPC_HideCustomerRemark.setName("Disable Customer Remark");
		cbPC_HideCustomerRemark.setTitle("PC_HideCustomerRemark");
		cbPC_HideCustomerRemark.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","PC_HideCustomerRemark"));
		
		
		//Ashwini Patil Date:12-09-2022 end of comment
		
		cbprintCustomerRemark = new CheckBox();
		cbprintCustomerRemark.setName("Print Customer Remark");
		cbprintCustomerRemark.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","PRINTCUSTOMERREMARK"));

		cbPrintTreatmentOnSRCopy = new CheckBox();
		cbPrintTreatmentOnSRCopy.setName("Print Treatments");
		cbPrintTreatmentOnSRCopy.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","PRINTPESTTREATMENTONSRCOPY"));

		cbPrintPhotoOnSRCopy = new CheckBox();
		cbPrintPhotoOnSRCopy.setName("Print Photo");
		cbPrintPhotoOnSRCopy.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","PRINTPHOTOONSRCOPY"));

		cbTechnicianRemark = new CheckBox();
		cbTechnicianRemark.setName("Print Technicial Remark");
		cbTechnicianRemark.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","PRINTTECHNICIANREMARK"));

		cbPrintFindingOnSRCopy = new CheckBox();
		cbPrintFindingOnSRCopy.setName("Print Findings on SR Copy");
		cbPrintFindingOnSRCopy.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","PRINTFINDINGSONSRCOPY"));

		cbPedioNotificationRemark = new CheckBox();
		cbPedioNotificationRemark.setName("Print Notification Remark");
		cbPedioNotificationRemark.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","PC_PEDIO_NOTECHNICIAN_REMARK"));

		cbhideMobileNo = new CheckBox();
		cbhideMobileNo.setName("Hide Mobile Number");
		cbhideMobileNo.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","PC_PEDIO_NOMOBILENUMBER"));

		cbhideCustomerFeedback = new CheckBox();
		cbhideCustomerFeedback.setName("Hide Customer Feedback");
		cbhideCustomerFeedback.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","PC_PEDIO_NOCUSTOMER_FEEDBACK"));

		cbUploadSRCopy = new CheckBox();
		cbUploadSRCopy.setName("Upload SR Copy");
		cbUploadSRCopy.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio","UPLOADSRCOPY"));
			
		cbHIDEMATERIALANDUOM = new CheckBox();
		cbHIDEMATERIALANDUOM.setName("Hide UoM on SR Copy");
		cbHIDEMATERIALANDUOM.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","HIDEMATERIALANDUOM"));

		cbDecreaseTableRow = new CheckBox();
		cbDecreaseTableRow.setName("Reduce blank rows on SR Copy");
		cbDecreaseTableRow.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","DecreaseTableRow"));

		cbPC_HideCellNumber_SRCopy = new CheckBox();
		cbPC_HideCellNumber_SRCopy.setName("Remove Technican Mobile from SR Copy");
		cbPC_HideCellNumber_SRCopy.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","PC_HideCellNumber_SRCopy"));

		cbPC_DoNotPrintMaterialOnSummarySR = new CheckBox();
		cbPC_DoNotPrintMaterialOnSummarySR.setName("Do not print Material on Summary SR Copy");
		cbPC_DoNotPrintMaterialOnSummarySR.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","PC_DoNotPrintMaterialOnSummarySR"));

		cbPRINTMATERIALONSRCOPY = new CheckBox();
		cbPRINTMATERIALONSRCOPY.setName("Print Material On SR Copy");
		cbPRINTMATERIALONSRCOPY.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","PRINTMATERIALONSRCOPY"));

		cbPRINTPHOTOONSRCOPY = new CheckBox();
		cbPRINTPHOTOONSRCOPY.setName("Print Photo On SR Copy");
		cbPRINTPHOTOONSRCOPY.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","PRINTPHOTOONSRCOPY"));

		
		serviceTabPanel = new TabPanel();
		serviceTabPanel.setWidth("100%");
		
		cbMakeCustomerEmailMandatory = new CheckBox();
		cbMakeCustomerEmailMandatory.setName("Make Customer Email Mandatory");
		cbMakeCustomerEmailMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","MakeEmailMandatory"));

		cbMakeCustGroupMandatory = new CheckBox();
		cbMakeCustGroupMandatory.setName("Make Customer Group Mandatoy");
		cbMakeCustGroupMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","MakeGroupMandatory"));

		cbMakeCustPinCodeMandatory = new CheckBox();
		cbMakeCustPinCodeMandatory.setName("Make Customer Pin Code Mandatory");
		cbMakeCustPinCodeMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","MakePincodeMandatory"));

		cbMakeCustRefMandatory = new CheckBox();
		cbMakeCustRefMandatory.setName("Make Customer Reference No 1 Mandatory");
		cbMakeCustRefMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","MakeRefNumber1Mandatory"));

		cbMakeCustLevelMandatory = new CheckBox();
		cbMakeCustLevelMandatory.setName("Make Customer Level Mandatory");
		cbMakeCustLevelMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","MakeCustomerLevelMandatory"));

		cbHideCustomerGroup = new CheckBox();
		cbHideCustomerGroup.setName("Hide Customer Group");
		cbHideCustomerGroup.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","HideCustomerGroup"));

		cbMakecustCategoryAndTypeDisable = new CheckBox();
		cbMakecustCategoryAndTypeDisable.setName("Make Customer Category Type Disable");
		cbMakecustCategoryAndTypeDisable.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","MakeCategoryAndTypeDisable"));

		cbMakeCellNoNonMandatory = new CheckBox();
		cbMakeCellNoNonMandatory.setName("Make Cell No Non Mandatory");
		cbMakeCellNoNonMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","MakeCellNumberNonMandatory"));
		
		cbMakeAccountManagerMandatory = new CheckBox();
		cbMakeAccountManagerMandatory.setName("Make Account Manager Mandatory");
		cbMakeAccountManagerMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","MakeAccountManagerMandatory"));
		
		cbMakeSalesPersonMandatory = new CheckBox();
		cbMakeSalesPersonMandatory.setName("Make Sales Person Mandatory");
		cbMakeSalesPersonMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","MakeSalesPersonMandatory"));
		
		cbEnableloadLargeCustomerData = new CheckBox();
		cbEnableloadLargeCustomerData.setName("Enable load Large Customer Data");
		cbEnableloadLargeCustomerData.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","EnableloadLargeCustomerData"));
		
		cbHideCustomerReligion = new CheckBox();
		cbHideCustomerReligion.setName("Hide Customer Religion");
		cbHideCustomerReligion.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer","HideCustomerReligion"));
		
		cbMakeLeadGroupMandatory = new CheckBox();
		cbMakeLeadGroupMandatory.setName("Make Group Mandatory");
		cbMakeLeadGroupMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","makeGroupMandatory"));

		cbLeadCustPopupEmailMandatory = new CheckBox();
		cbLeadCustPopupEmailMandatory.setName("Make Customer Popup Email Mandatory");
		cbLeadCustPopupEmailMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","MakeCustomerPopUpEmailMandatory"));

		cbMakeCustomerPopUpCompanyNameMandatory = new CheckBox();
		cbMakeCustomerPopUpCompanyNameMandatory.setName("Make Customer Popup Company Name Mandatory");
		cbMakeCustomerPopUpCompanyNameMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","MakeCustomerPopUpCompanyNameMandatory"));

		cbMakeCustomerPopUpPhoneNumberMandatory = new CheckBox();
		cbMakeCustomerPopUpPhoneNumberMandatory.setName("Make Customer Popup Phone No Mandatory");
		cbMakeCustomerPopUpPhoneNumberMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","MakeCustomerPopUpPhoneNumberMandatory"));

		cbEnableFreezeLead = new CheckBox();
		cbEnableFreezeLead.setName("Enable Freeze Lead");
		cbEnableFreezeLead.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","EnableFreezeLead"));

		cbCommunicationLogColumn = new CheckBox();
		cbCommunicationLogColumn.setName("Enable Communication Log In Download");
		cbCommunicationLogColumn.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","CommunicationLogColumn"));

		cbMapCustomerTypeToLeadCategory = new CheckBox();
		cbMapCustomerTypeToLeadCategory.setName("Map Customer Type To Lead Category");
		cbMapCustomerTypeToLeadCategory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","MapCustomerTypeToLeadCategory"));

		cbMakePriceColumnDisable = new CheckBox();
		cbMakePriceColumnDisable.setName("Make Price Column Disable");
		cbMakePriceColumnDisable.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","MakePriceColumnDisable"));

		cbChangeProductComposite = new CheckBox();
		cbChangeProductComposite.setName("Change Product Composite");
		cbChangeProductComposite.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","ChangeProductComposite"));

		cbDisableEmailIdValidation = new CheckBox();
		cbDisableEmailIdValidation.setName("Disable Email Id Validation");
		cbDisableEmailIdValidation.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","ChangeProductComposite"));
		
		cbHideNumberRange = new CheckBox();
		cbHideNumberRange.setName("Hide Number Range");
		cbHideNumberRange.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Lead","PC_LEAD_HIDE_NUMBER_RANGE"));

		cbMakeQuotationGroupMandatory = new CheckBox();
		cbMakeQuotationGroupMandatory.setName("Make Group Mandatory");
		cbMakeQuotationGroupMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","makeGroupMandatory"));

		cbQuotationTypeMandatory = new CheckBox();
		cbQuotationTypeMandatory.setName("Make Type Mandatory");
		cbQuotationTypeMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","MakeTypeMandatory"));

		cbQuotationCategoryMandatory = new CheckBox();
		cbQuotationCategoryMandatory.setName("Make Category Mandatory");
		cbQuotationCategoryMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","MakeCatagoryMandatory"));

		cbDonotProductDescription = new CheckBox();
		cbDonotProductDescription.setName("Enable Do Not Print Product Description");
		cbDonotProductDescription.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","DoNotProductDescription"));

		cbPrintProductDescriptionOnPDF = new CheckBox();
		cbPrintProductDescriptionOnPDF.setName("Enable Print Product Description");
		cbPrintProductDescriptionOnPDF.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","PrintProductDiscriptionOnPdf"));

		cbMakeQuotationDateDisable = new CheckBox();
		cbMakeQuotationDateDisable.setName("Make Quotation Date Disable");
		cbMakeQuotationDateDisable.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","MakeQuotationDateDisable"));

		cbMakeLeadIdMandatory = new CheckBox();
		cbMakeLeadIdMandatory.setName("Make Lead Id Mandatory");
		cbMakeLeadIdMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","MakeQuotationDateDisable"));

		cbprintProductRemarkWithPremise = new CheckBox();
		cbprintProductRemarkWithPremise.setName("Print Product Remark With Premise");
		cbprintProductRemarkWithPremise.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","printProductRemarkWithPremise"));

		cbPrintProductPrimiseInQuotationPdf = new CheckBox();
		cbPrintProductPrimiseInQuotationPdf.setName("Print Product Primices in PDF");
		cbPrintProductPrimiseInQuotationPdf.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","printProductPremisesInPdf"));
		
		
		cbPC_DoNotPrintProductWarrantyOnQuotation = new CheckBox();
		cbPC_DoNotPrintProductWarrantyOnQuotation.setName("Do Not Print Product Warranty On Quotation Print");
		cbPC_DoNotPrintProductWarrantyOnQuotation.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation","PC_DoNotPrintProductWarranty"));
		
		cbMakeContractGroupMandatory = new CheckBox();
		cbMakeContractGroupMandatory.setName("Make Group Mandaory");
		cbMakeContractGroupMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","MakeGroupMandatory"));

		cbMakeContractTypeMandatory = new CheckBox();
		cbMakeContractTypeMandatory.setName("Make Type Mandatory");
		cbMakeContractTypeMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","MakeTypeMandatory"));

		cbPrintProductPrimiseInPdf = new CheckBox();
		cbPrintProductPrimiseInPdf.setName("Print Product Primices in PDF");
		cbPrintProductPrimiseInPdf.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","printProductPremisesInPdf"));

		cbMakeNumberRangeMandatory = new CheckBox();
		cbMakeNumberRangeMandatory.setName("Make Number Range Mandatory");
		cbMakeNumberRangeMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","MakeNumberRangeMandatory"));

		cbMakeContractDateDisable = new CheckBox();
		cbMakeContractDateDisable.setName("Make Contract Date Disable");
		cbMakeContractDateDisable.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","MakeContractDateDisable"));

		cbPC_DoNot_Print_Services = new CheckBox();
		cbEnableServiceBranch.setName("PC_DoNot_Print_Services");
		cbEnableServiceBranch.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "PC_DoNot_Print_Services"));

		cbMakeInvoiceGroupMandatory = new CheckBox();
		cbMakeInvoiceGroupMandatory.setName("Invoice Group Mandatory");
		cbMakeInvoiceGroupMandatory.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "MakeInvoiceGroupMandatory"));

		
		cbPrintDescriptionOnInvoicefromContract = new CheckBox();
		cbPrintDescriptionOnInvoicefromContract.setName("Print Description On Invoice from Contract");
		cbPrintDescriptionOnInvoicefromContract.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "PrintDescriptionOnInvoiceFromContract"));

		cbPrintDescriptionOnInvoicefromSalesOrder = new CheckBox();
		cbPrintDescriptionOnInvoicefromSalesOrder.setName("Print Description On Invoice from Sales Order");
		cbPrintDescriptionOnInvoicefromSalesOrder.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "PrintDescriptionOnInvoiceFromSalesOrder"));
		
		cbPC_NewInvoiceFormat = new CheckBox();
		cbPC_NewInvoiceFormat.setName("PC_NewInvoiceFormat");
		cbPC_NewInvoiceFormat.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "PC_NewInvoiceFormat"));
		
		cbGenerateEinvoiceFromReferenceNumber = new CheckBox();
		cbGenerateEinvoiceFromReferenceNumber.setName("Generate Einvoice From Reference Number");
		cbGenerateEinvoiceFromReferenceNumber.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "GenerateEinvoiceFromReferenceNumber"));

		cbEnableInvoiceIntegrationWithZohoBooks = new CheckBox();
		cbEnableInvoiceIntegrationWithZohoBooks.setName("Enable Invoice Integration With Zoho Books");
		cbEnableInvoiceIntegrationWithZohoBooks.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableInvoiceIntegrationWithZohoBooks"));

		cbBranchLevelRestriction = new CheckBox();
		cbBranchLevelRestriction.setName("Branch Level Restriction");
		cbBranchLevelRestriction.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BRANCHLEVELRESTRICTION"));
//		cbBranchLevelRestriction.setTitle("Activate process configuration, Need to update company details on branch");
		
		cbCustomerBranchRestriction = new CheckBox();
		cbCustomerBranchRestriction.setName("Customer Branch Level Restriction");
		cbCustomerBranchRestriction.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", AppConstants.PC_BranchLevelRestrictionForCustomer));
		
		
		btnSetupPedio = new Button("Setup Pedio");
		btnSharePortalLink=new Button("Share Customer Portal Link with all customers");
		
		
		cbDisableIPAddressEmail = new CheckBox();
		cbDisableIPAddressEmail.setName("Disable IP Address Email");
		cbDisableIPAddressEmail.setValue(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Company", AppConstants.PC_DISABLEIPADDRESSEMAIL));
		
		
	}
	
	/**
	 * method template to create screen formfields.
	 */
	@Override
	public void createScreen() {

		initializeWidget();

		this.processlevelBarNames=new String[]{"New"};

		
		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////
		
		String mainScreenLabel="Lead Details";

		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fgroupingLeadInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(2).setKeyField(true).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingGenralInformation=fbuilder.setlabel("Setup").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("",company);
		FormField fcompany= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("",Branch);
		FormField fBranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("",billing);
		FormField fbilling= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",nonTaxNumberRange);
		FormField fnonTaxNumberRange= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",user);
		FormField fuser= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",serviceProduct);
		FormField fserviceProduct= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",itemProduct);
		FormField fitemProduct= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		

		fbuilder = new FormFieldBuilder("",authorization);
		FormField fauthorization= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("",restrictUserLocationAccess);
		FormField frestrictUserLocationAccess= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("",Locality);
		FormField fLocality= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("",city);
		FormField fcity= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder();
		FormField fgroupingRenewals=fbuilder.setlabel("Renewals").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(0).build();

		fbuilder = new FormFieldBuilder();
		FormField fgroupingServiceScheduling=fbuilder.setlabel("Customer Service").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(0).build();

		fbuilder = new FormFieldBuilder();
		FormField fgroupingAccounts=fbuilder.setlabel("Accounts").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",bank);
		FormField fbank= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("",branchWiseInvoiceNumberRange);
		FormField fbranchWiseInvoiceNumberRange= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("",invoicePrifix);
		FormField finvoicePrifix= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("",contractlossRenewalReasons);
		FormField fcontractlossRenewalReasons= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		HorizontalPanel branchAsCompany = new HorizontalPanel();
		branchAsCompany.add(defineBranchAsCompany);
		branchAsCompany.add(cbBranchAsCompany);
		
		Console.log("in createscreen setting horizontal panel for CityNotMandatory");
		HorizontalPanel CityNotMandatory = new HorizontalPanel();
		//branchAsCompany.add(defineBranchAsCompany);
		CityNotMandatory.add(cbCityNotMandatory);
		
		HorizontalPanel NumberRangeMandatory = new HorizontalPanel();
		NumberRangeMandatory.add(cbNumberRangeMandatory);
		
		HorizontalPanel BranchWiseOrCompanyWiseNumberRange = new HorizontalPanel();
		BranchWiseOrCompanyWiseNumberRange.add(cbBranchWiseOrCompanyWiseNumberRange);
		
		HorizontalPanel enableSelfApproval = new HorizontalPanel();
		enableSelfApproval.add(enableMakerChecker);
		enableSelfApproval.add(cbEnableSelfApproval);
		
		HorizontalPanel enableSelfApproval1 = new HorizontalPanel();
		enableSelfApproval1.add(enableSelfApproval);
		
		HorizontalPanel outstandingDuelist = new HorizontalPanel();
		outstandingDuelist.add(cbOutstandingDuelist);
		outstandingDuelist.add(outstandingDueList);
		
		fbuilder = new FormFieldBuilder("",outstandingDueList);
		FormField foutstandingDueList= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		HorizontalPanel outstandingDueClientEmail = new HorizontalPanel();
		outstandingDueClientEmail.add(cbOutstandingtoClientEmail);
		outstandingDueClientEmail.add(outstandingDuetoClientEmail);
		
		fbuilder = new FormFieldBuilder("",outstandingDueClientEmail);
		FormField foutstandingDueClientEmail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		HorizontalPanel hsOneTimeContract = new HorizontalPanel();
		hsOneTimeContract.add(cbOneTimeContract);
		hsOneTimeContract.add(singleOrOnetimeCOntract);
		
		fbuilder = new FormFieldBuilder("",hsOneTimeContract);
		FormField fhsOneTimeContract= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		HorizontalPanel hsContractRenwallistemail = new HorizontalPanel();
		hsContractRenwallistemail.add(contractRenewallistReminderEmail);
		hsContractRenwallistemail.add(cbRenewalReminderlistEmail);
		
		fbuilder = new FormFieldBuilder("",contractRenewallistReminderEmail);
		FormField fhsContractRenwallistemail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		
		HorizontalPanel hsContractRenwalToClientSMS = new HorizontalPanel();
		hsContractRenwalToClientSMS.add(cbRenewalReminderSMS);
		hsContractRenwalToClientSMS.add(renewalRemindersms);
		HorizontalPanel hsprintServiceAddressOnRenewal = new HorizontalPanel();
		hsprintServiceAddressOnRenewal.add(cbprintServiceAddressOnRenewal);
		hsprintServiceAddressOnRenewal.add(printServiceAddressOnRenewal);
	
		
		HorizontalPanel hscbDoNotPrintDiscountColumnOnRenewalPrint = new HorizontalPanel();
		hscbDoNotPrintDiscountColumnOnRenewalPrint.add(cbDoNotPrintDiscountColumnOnRenewalPrint);
		hscbDoNotPrintDiscountColumnOnRenewalPrint.add(doNotPrintDiscountColumnOnRenewalPrint);
		
		fbuilder = new FormFieldBuilder("",hsContractRenwalToClientSMS);
		FormField fhsContractRenwalToClientSMS= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("",hsprintServiceAddressOnRenewal);
		FormField fhsprintServiceAddressOnRenewal= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		fbuilder = new FormFieldBuilder("",hscbDoNotPrintDiscountColumnOnRenewalPrint);
		FormField fhscbDoNotPrintDiscountColumnOnRenewalPrint= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		HorizontalPanel hsrenewalRemindertoClientEmail = new HorizontalPanel();
		hsrenewalRemindertoClientEmail.add(cbRenewalReminderToClient);
		hsrenewalRemindertoClientEmail.add(renewalReminderToclient);
		
		fbuilder = new FormFieldBuilder("",hsrenewalRemindertoClientEmail);
		FormField fhsrenewalRemindertoClientEmail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		HorizontalPanel hsHideBankdetailsonpdf = new HorizontalPanel();
		hsHideBankdetailsonpdf.add(cbHideBankDetailsonPDF);
		hsHideBankdetailsonpdf.add(hideBankDetailsonPDF);
		
		fbuilder = new FormFieldBuilder("",hsHideBankdetailsonpdf);
		FormField fhsHideBankdetailsonpdf= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		HorizontalPanel hsQuotationMandatory = new HorizontalPanel();
		hsQuotationMandatory.add(cbquotationMandatory);
		hsQuotationMandatory.add(quotationMandatory);
		
		fbuilder = new FormFieldBuilder("",hsQuotationMandatory);
		FormField fhsQuotationMandatory= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		HorizontalPanel hpScheduling = new HorizontalPanel();
		hpScheduling.getElement().getStyle().setWidth(70, Unit.PCT);
		hpScheduling.add(serviceRescheduleReasons);
		hpScheduling.add(serviceCancellationReasons);
		hpScheduling.add(typeOfTreatment);
		hpScheduling.add(frequency);
		hpScheduling.add(findingType);
		hpScheduling.add(serviceChecklist);
		
		fbuilder = new FormFieldBuilder("",hpScheduling);
		FormField fhpScheduling= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingService=fbuilder.setlabel("Service").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(0).build();

		HorizontalPanel hpBranchAsCompany = new HorizontalPanel();
		hpBranchAsCompany.add(getFieldCB("Define Branch As Company", cbBranchAsCompany));
			
		fbuilder = new FormFieldBuilder("",hpBranchAsCompany);
		FormField fhpBranchAsCompany= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		Console.log("setting up FormField in create screen");
		HorizontalPanel hpCityNotMandatory = new HorizontalPanel();
		hpCityNotMandatory.add(getFieldCB("City Not Mandatory", cbCityNotMandatory));
		
		fbuilder = new FormFieldBuilder("",hpCityNotMandatory);
		FormField fhpCityNotMandatory= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		HorizontalPanel hpNumberRangeMandatory = new HorizontalPanel();
		hpNumberRangeMandatory.add(getFieldCB("Number Range Mandatory", cbNumberRangeMandatory ));
		
		fbuilder = new FormFieldBuilder("",hpNumberRangeMandatory);
		FormField fhpNumberRangeMandatory= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		HorizontalPanel hpBranchWiseOrCompanyWiseNumberRange = new HorizontalPanel();
		hpBranchWiseOrCompanyWiseNumberRange.add(getFieldCB("Branch wise or company wise number range (customized)", cbBranchWiseOrCompanyWiseNumberRange));
		
		fbuilder = new FormFieldBuilder("",hpBranchWiseOrCompanyWiseNumberRange);
		FormField fhpBranchWiseOrCompanyWiseNumberRange= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		HorizontalPanel hpMakeNumberRangeReadOnly= new HorizontalPanel();
		hpMakeNumberRangeReadOnly.add(getFieldCB("Make Number Range Read Only", cbMakeNumberRangeReadOnly));
				
		fbuilder = new FormFieldBuilder("",hpMakeNumberRangeReadOnly);
		FormField fhpMakeNumberRangeReadOnly= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		VerticalPanel vpService = new VerticalPanel();
		vpService.add(getFieldCB("Quotation Self Approval", cbQuotationSelfApproval));
		vpService.add(getFieldCB("Contract Self Approval", cbContractSelfApproval));
		
		vpService.add(getFieldCB("Number range mandatory on lead", cbNumberRangeMandatoryOnLead));
		vpService.add(getFieldCB("Number range mandatory on quotation", cbNumberRangeMandatoryOnQuotation));

		
		VerticalPanel vpSales = new VerticalPanel();
		vpSales.add(getFieldCB("Sales Quotation Self Approval", cbSalesQuotationSelfApproval));
		vpSales.add(getFieldCB("Sales Order Self Approval", cbSalesOrderSelfApproval));
		vpSales.add(getFieldCB("Make Reference Number Mandatory", cbSalesOrderMakeReferenceNumberMandatory));
		
		VerticalPanel vpPurchase = new VerticalPanel();
		vpPurchase.add(getFieldCB("Purchase Order Self Approval", cbPOSelfApproval));
		vpPurchase.add(getFieldCB("Service PO Self Approval", cbServicePOSelfApproval));
		vpPurchase.add(getFieldCB("GRN Self Approval", cbGRNSelfApproval));

		VerticalPanel vpInventory = new VerticalPanel();
		vpInventory.add(getFieldCB("MRN Self Approval", cbMRNSelfApproval));
		vpInventory.add(getFieldCB("MIN Self Approval", cbMINSelfApproval));
		vpInventory.add(getFieldCB("MMN Self Approval", cbMMNSelfApproval));

		VerticalPanel vpAccounts = new VerticalPanel();
		vpAccounts.add(getFieldCB("Billing Self Approval", cbBillingSelfApproval));
		vpAccounts.add(getFieldCB("Invoice Self Approval", cbInvoiceSelfApproval));
		vpAccounts.add(getFieldCB("Vendor Invoice Self Approval", cbVendorInvoiceSelfApproval));
		vpAccounts.add(getFieldCB("Payment Date Mandatory", cbPaymentDateMandatory));
		vpAccounts.add(getFieldCB("Payment Received Date Mandatory", cbPaymentReceivedDateMandatory));
		
		VerticalPanel vpProduct = new VerticalPanel();
		vpProduct.add(getFieldCB("Sales Price Mandatory", cbSalesPriceMandatory));
		vpProduct.add(getFieldCB("Purchase Price Mandatory", cbPurchasePriceMandatory));

		VerticalPanel vpCustPortal = new VerticalPanel();
		vpCustPortal.add(btnSharePortalLink);
		
		
		setuptabCheckBoxPanel.add(vpService, "Service");
		setuptabCheckBoxPanel.add(vpSales, "Sales");
		setuptabCheckBoxPanel.add(vpPurchase, "Purchase");
		setuptabCheckBoxPanel.add(vpInventory, "Inventory");
		setuptabCheckBoxPanel.add(vpAccounts, "Accounts");
		setuptabCheckBoxPanel.add(vpProduct, "Product");//@Sheetal:19-03-2022,Adding new tab for Product
		setuptabCheckBoxPanel.add(vpCustPortal, "Customer Portal");//Ashwini Patil Date:24-1-2023
		
		setuptabCheckBoxPanel.selectTab(0);
		
		fbuilder = new FormFieldBuilder("",setuptabCheckBoxPanel);
		FormField fsetuptabPanel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		HorizontalPanel hpGroupingPanel2 = new HorizontalPanel();
		hpGroupingPanel2.setWidth("100%");
		hpGroupingPanel2.getElement().getStyle().setHeight(24, Unit.PX);
		InlineLabel lbl2=new InlineLabel();
		lbl2.setText("Preferences");
		hpGroupingPanel2.add(lbl2);
		hpGroupingPanel2.getElement().getStyle().setBackgroundColor("#D0D0D0");
		fbuilder = new FormFieldBuilder("",hpGroupingPanel2);
		FormField fPreferences= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
	
		fbuilder = new FormFieldBuilder("", getFieldCB("Expense Accouting Interface", cbExpenseAccoutingInterface));
		FormField fcbExpenseAccoutingInterface= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("", getFieldCB("Payroll Accouting Interface", cbPayrollAccountingInterface));
		FormField fcbPayrollAccountingInterface= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		
		VerticalPanel vpBilling = new VerticalPanel();
		vpBilling.add(getFieldCB("Self Approval", cbBillSelfApproval));
		vpBilling.add(getFieldCB("Make Service Completion Mandatory For AMC Contract", cbMakeServiceCompletionMandatoryForAMCContract));
		vpBilling.add(getFieldCB("Enable Auto Invoice", cbEnableAutoInvoice));
		vpBilling.add(getFieldCB("Enable Service Branch", cbEnableServiceBranch));

		
		VerticalPanel vpInvoice = new VerticalPanel();
		vpInvoice.add(getFieldCB("Self Approval", cbinvoiceSelfApproval));
		vpInvoice.add(getFieldCB("Accounting Interface", cbinvoiceAccountingInterface));
		vpInvoice.add(getFieldCB("Company As Letter Head", cbCompanyAsLetterHead));
		vpInvoice.add(getFieldCB("Print Product Premices in Pdf", cbPrintProductPrimicesinPDF));
		vpInvoice.add(getFieldCB("EnableInvoiceEditableForAllRoles", cbInvoiceEditAllRoll));
		vpInvoice.add(getFieldCB("Make Address To Uppercase", cbmakeAddressToUppercase));
		vpInvoice.add(getFieldCB("Print Po Details On Invoice", cbPrintPoDetailsOnInvoice));
		vpInvoice.add(getFieldCB("Hide Rate And Discount Column", cbHideRateandDiscountColumn));
		vpInvoice.add(getFieldCB("Print Product Description On Pdf", cbPrintProductDiscriptionOnPdfInInvoice));
		vpInvoice.add(getFieldCB("PC_DoNot_Print_#Services", cbPC_DoNot_Print_Services));
		vpInvoice.add(getFieldCB("Invoice Group Mandatory", cbMakeInvoiceGroupMandatory));
		vpInvoice.add(getFieldCB("Print Description On Invoice from Contract", cbPrintDescriptionOnInvoicefromContract));
		vpInvoice.add(getFieldCB("Print Description On Invoice from Sales Order", cbPrintDescriptionOnInvoicefromSalesOrder));
		vpInvoice.add(getFieldCB("PC_NewInvoiceFormat", cbPC_NewInvoiceFormat));
		vpInvoice.add(getFieldCB("Generate Einvoice From Reference Number", cbGenerateEinvoiceFromReferenceNumber));
		vpInvoice.add(getFieldCB("Enable Invoice Integration With Zoho Books", cbEnableInvoiceIntegrationWithZohoBooks));
				
		VerticalPanel vpPayment = new VerticalPanel();
		vpPayment.add(getFieldCB("Accounting Interface", cbPaymentAccountningInterface));
		vpPayment.add(getFieldCB("Payment Add Into PettyCash", cbPaymentAddIntoPettyCash));
		vpPayment.add(getFieldCB("Company As Letter Head", cbPaymentCompanyAsLetterHead));

		
		VerticalPanel vpVendorInvoice = new VerticalPanel();
		vpVendorInvoice.add(getFieldCB("Print Order No And GrnNo", cbPrintOrderNoAndGrnNo));
		
		accountsTabCheckboxpanel.add(vpBilling, "Billing");
		accountsTabCheckboxpanel.add(vpInvoice, "Invoice");
		accountsTabCheckboxpanel.add(vpPayment, "Customer Payment");
		accountsTabCheckboxpanel.add(vpVendorInvoice, "Vendor Invoice");

		accountsTabCheckboxpanel.selectTab(0);
		
		
		fbuilder = new FormFieldBuilder("",accountsTabCheckboxpanel);
		FormField faccountsTabCheckboxpanel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		
		VerticalPanel vpServiceScheduling = new VerticalPanel();
//		vpServiceScheduling.add(getFieldCB("Transfer Material While Scheduling", cbTransferMaterialWhileScheduling));
//		vpServiceScheduling.add(getFieldCB("Auto Email Job Card On Service Completion", cbautoEmailJobCardOnCompletion));
		vpServiceScheduling.add(getFieldCB("Complain Service With Turn Around Time", cbComplainServiceWithTurnAroundTime));
		vpServiceScheduling.add(getFieldCB("Close Complain When Service Close", cbCloseComplaintWhenServiceClose));
		vpServiceScheduling.add(getFieldCB("Send SR Copy On Service Completion", cbSendSrCopyOnServiceCompletion));
		vpServiceScheduling.add(getFieldCB("Custom SR Copy", cbCustomizedSrCopy));
		vpServiceScheduling.add(getFieldCB("Consmption Material Mandatory", cbMAKEMATERIALMANDATORY));
		vpServiceScheduling.add(getFieldCB("Enable Service Editing After Completion", cbMakeServiceEditableAfterCompletion));
		vpServiceScheduling.add(getFieldCB("Allow Multiple Expenses", cbMultipleExpenseMngt));
		vpServiceScheduling.add(getFieldCB("Enable Schedule Print Without Team", cbDATETECHNICIANWISESERVICEPOPUP));
		
		vpServiceScheduling.add(getFieldCB("Disable Complete and Reschedule from customer service list. Disable Fumigation.", cbHideCompleteRescheduleFumigationColoumn));
		vpServiceScheduling.add(getFieldCB("Disable Fumigation from Customer Service List", cbHideFumigationButton));
		vpServiceScheduling.add(getFieldCB("Enable Complaint Service Turn Around Time", cbCOMPLAINSERVICEWITHTURNAROUNDTIME));
		vpServiceScheduling.add(getFieldCB("Auto email service report on completion", cbEmailCustomizedAndGenericSRCopy));
		vpServiceScheduling.add(getFieldCB("Enable Monthly Scheduling ", cbPC_SERVICESCHEDULEMINMAXROUNDUP));
		vpServiceScheduling.add(getFieldCB("Disable Plan Button On Customer Service List ", cbPC_HIDE_CS_LIST_PLAN_BUTTON));
		vpServiceScheduling.add(getFieldCB("Enable Findings as Quantity For Service Wise Billing", cbPC_PedioFindingAsServiceQuantityInBilling));
		vpServiceScheduling.add(getFieldCB("Allow service completion from schedule popup", cbENABLECOMPLETEBUTTONONSCHEDULEPOPUP));
		vpServiceScheduling.add(getFieldCB("Hide Service Value Except Admin", cbHideServiceValueExceptAdmin));		
		vpServiceScheduling.add(getFieldCB("Restrict Service Download With OTP", cbRestrictServiceDownloadWithOTP));		
		vpServiceScheduling.add(getFieldCB("Stop sending Service Reschedule Email to customer when Rescheduled from Pedio or CustomerPortal", cbStopServiceRescheduleEmail));		
		vpServiceScheduling.add(getFieldCB("Update Finding Entity", cbUpdateFindingEntity));
		
		
		VerticalPanel vpPedio = new VerticalPanel();
//		vpPedio.getElement().setClassName("vertical_scroll");//13-09-2022
		
		vpPedio.add(getFieldCB("Enable Pedio", cbEnablePedio));
		vpPedio.add(getFieldCB("Return Quantity Update", cbReturnQtyUpdate));
		vpPedio.add(getFieldCB("Service Schedule Material Mandatory", cbServiceScheduleMaterailMandatory));
		vpPedio.add(getFieldCB("Copy Material To Remaining Services", cbCopyMaterialToRemainingServices));
		vpPedio.add(getFieldCB("Enable FCM Notification", cbEnableFcmNotification));
		vpPedio.add(getFieldCB("Enable Service Schedule Notification", cbEnableServiceScheduleNotification));

		//12-09-2022
		vpPedio.add(getFieldCB("Technician Remark Mandatory", cbMAKETECHNICIANREMARKMANDITORY));
		vpPedio.add(getFieldCB("Signing Person Mandatory", cbMAKECUSTOMERNAMEMANDITORY));		
		vpPedio.add(getFieldCB("Disable Service Finding", cbHIDESERVICEFINDING));
		vpPedio.add(getFieldCB("Disable  Material Consumption", cbHIDEMATERIALS));		
		vpPedio.add(getFieldCB("Enable Expense", cbSHOWEXPENSE));
		vpPedio.add(getFieldCB("Complete Service w/o Feedback", cbDIRECTCOMPLETE));
		vpPedio.add(getFieldCB("Enable Technician Attendance", cbTECHNICIANATTENDANCE));
		vpPedio.add(getFieldCB("Site Finding", cbSITEFINDING));
		vpPedio.add(getFieldCB("Technician Can Reject Service", cbREJECTSERVICE));		
		vpPedio.add(getFieldCB("Disable SR Print", cbHIDESRS));
		vpPedio.add(getFieldCB("Enable Live Tracking", cbLIVETRACKING));
		vpPedio.add(getFieldCB("Enable OTP for Service Completion", cbSENDOTPFORSERVICECOMPLETION));
		vpPedio.add(getFieldCB("Disable Treatments", cbPC_PEDIO_HIDE_TREATMENTS));
		vpPedio.add(getFieldCB("Enable Mixing Ration", cbPC_ADDMIXINGRATIOANDLOTNO));		
		vpPedio.add(getFieldCB("Upload Document Mandatory", cbPC_DocumentUploadMandatory));
		vpPedio.add(getFieldCB("Disable Rescheduling", cbPC_Pedio_Hide_Reschedule));
		vpPedio.add(getFieldCB("Hide Customer Mobile Number", cbPC_Hide_Customer_Mobile));
		vpPedio.add(getFieldCB("Restrict Customers by Logged in User", cbLoadLoggedInUserWiseCustomer));
		vpPedio.add(getFieldCB("Enable Premise", cbPREMISESFLAG));
		vpPedio.add(getFieldCB("Mark Service Completed Automatically after T-Completed", cbMARKSERVICECOMPLETEDFROMAPP));
		vpPedio.add(getFieldCB("Edit EmailId to which SR copy to be sent after Service Completion form Pedio", cbSHOWEMAIL));		
		vpPedio.add(getFieldCB("Disable Photos", cbPEDIO_HIDEPHOTOS));
		vpPedio.add(getFieldCB("Display product premises in Service details", cbShowProductPremises));
		vpPedio.add(getFieldCB("Disable Customer Rating (Feedback)", cbPC_DoNotShowRatings));
		vpPedio.add(getFieldCB("Customer Rating (Feedback) Mandatory", cbMAKESTARRATINGMANDITORY));
		vpPedio.add(getFieldCB("Disable Customer Remark", cbPC_HideCustomerRemark));
		vpPedio.add(getFieldCB("Customer Remark Mandatory", cbMAKECUSTOMERREMARKMANDITORY));
		vpPedio.add(getFieldCB("Make Findings Mandatory", cbMAKEFINDINGSMANDATORY));
		vpPedio.add(getFieldCB("Make Type Of Treatment Mandatory", cbMAKETYPEOFTREATMENTMANDATORY));
		
			
		VerticalPanel vpJobCard = new VerticalPanel();
		vpJobCard.add(getFieldCB("Print Customer Remark", cbprintCustomerRemark));
		vpJobCard.add(getFieldCB("Print Treatments", cbPrintTreatmentOnSRCopy));
		vpJobCard.add(getFieldCB("Print Photo", cbPrintPhotoOnSRCopy));
		vpJobCard.add(getFieldCB("Print Technician Remark", cbTechnicianRemark));
		vpJobCard.add(getFieldCB("Print Notification Remark", cbPedioNotificationRemark));
		vpJobCard.add(getFieldCB("Hide mobile Number", cbhideMobileNo));
		vpJobCard.add(getFieldCB("Hide Customer Feedback", cbhideCustomerFeedback));
		vpJobCard.add(getFieldCB("Upload SR Copy", cbUploadSRCopy));

		vpJobCard.add(getFieldCB("Hide UoM on SR Copy", cbHIDEMATERIALANDUOM));
		vpJobCard.add(getFieldCB("Reduce blank rows on SR Copy", cbDecreaseTableRow));
		vpJobCard.add(getFieldCB("Remove Technican Mobile from SR Copy", cbPC_HideCellNumber_SRCopy));
		vpJobCard.add(getFieldCB("Do not print Material on Summary SR Copy", cbPC_DoNotPrintMaterialOnSummarySR));
		vpJobCard.add(getFieldCB("Print Material On SR Copy", cbPRINTMATERIALONSRCOPY));
		vpJobCard.add(getFieldCB("Print Photo On SR Copy", cbPRINTPHOTOONSRCOPY));


		serviceSchedulingTabPanel.add(vpServiceScheduling, "Customer Service");//"Service Scheduling" changed to "Customer Service"
		serviceSchedulingTabPanel.add(vpPedio, "Pedio");
		serviceSchedulingTabPanel.add(vpJobCard, "SR Copy");//job card changed to SR Copy

		serviceSchedulingTabPanel.selectTab(0);
		
		fbuilder = new FormFieldBuilder("",serviceSchedulingTabPanel);
		FormField fserviceSchedulingTabPanel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
		
		VerticalPanel vpCustomer = new VerticalPanel();
		vpCustomer.add(getFieldCB("Make Customer Email Mandatory", cbMakeCustomerEmailMandatory));
		vpCustomer.add(getFieldCB("Make Customer Group Mandatory", cbMakeCustGroupMandatory));
		vpCustomer.add(getFieldCB("Make Customer Pin Code Mandatory", cbMakeCustPinCodeMandatory));
		vpCustomer.add(getFieldCB("Make Customer Reference No 1", cbMakeCustRefMandatory));
		vpCustomer.add(getFieldCB("Make Customer Level Mandatory", cbMakeCustLevelMandatory));
		vpCustomer.add(getFieldCB("Hide Customer Group", cbHideCustomerGroup));
		vpCustomer.add(getFieldCB("Make Customer Category Type Disable", cbMakecustCategoryAndTypeDisable));
		vpCustomer.add(getFieldCB("Make Cell No Non Mandatory", cbMakeCellNoNonMandatory));
		vpCustomer.add(getFieldCB("Make Account Manager Mandatory", cbMakeAccountManagerMandatory));
		vpCustomer.add(getFieldCB("Make Sales Person Mandatory", cbMakeSalesPersonMandatory));
		vpCustomer.add(getFieldCB("Enable load Large Customer Data", cbEnableloadLargeCustomerData));
		vpCustomer.add(getFieldCB("Hide Customer Religion", cbHideCustomerReligion));
		
		
//		cbMakeLeadGroupMandatory = new CheckBox();
//		cbLeadCustPopupEmailMandatory = new CheckBox();
//		cbMakeCustomerPopUpCompanyNameMandatory = new CheckBox();
//		cbMakeCustomerPopUpPhoneNumberMandatory = new CheckBox();
//		cbEnableFreezeLead = new CheckBox();
//		cbCommunicationLogColumn = new CheckBox();
//		cbMapCustomerTypeToLeadCategory = new CheckBox();
//		cbMakePriceColumnDisable = new CheckBox();
//		cbChangeProductComposite = new CheckBox();
//		cbDisableEmailIdValidation = new CheckBox();
//		
//		cbMakeQuotationGroupMandatory = new CheckBox();
//		cbQuotationTypeMandatory = new CheckBox();
//		cbQuotationCategoryMandatory = new CheckBox();
//		cbDonotProductDescription = new CheckBox();
//		cbPrintProductDescriptionOnPDF = new CheckBox();
//		cbMakeQuotationDateDisable = new CheckBox();
//		cbMakeLeadIdMandatory = new CheckBox();
//		
//		cbMakeContractGroupMandatory = new CheckBox();
//		cbMakeContractTypeMandatory = new CheckBox();
//		cbPrintProductPrimiseInPdf = new CheckBox();
//		cbMakeNumberRangeMandatory = new CheckBox();
//		cbMakeContractDateDisable = new CheckBox();
		
		VerticalPanel vpLead = new VerticalPanel();
		vpLead.add(getFieldCB("Make Group Mandatory", cbMakeLeadGroupMandatory));
		vpLead.add(getFieldCB("Make Customer Popup Email Mandatory", cbLeadCustPopupEmailMandatory));
		vpLead.add(getFieldCB("Make Customer Popup Company Name Mandatory", cbMakeCustomerPopUpCompanyNameMandatory));
		vpLead.add(getFieldCB("Make Customer Popup Phone No Mandatory", cbMakeCustomerPopUpPhoneNumberMandatory));
		vpLead.add(getFieldCB("Enable Freeze Lead", cbEnableFreezeLead));
		vpLead.add(getFieldCB("Enable Communication Log Column In Download", cbCommunicationLogColumn));
		vpLead.add(getFieldCB("Map Customer Type To Lead Category", cbMapCustomerTypeToLeadCategory));
		vpLead.add(getFieldCB("Make Price Column Disable", cbMakePriceColumnDisable));
		vpLead.add(getFieldCB("Change Product Composite", cbChangeProductComposite));
		vpLead.add(getFieldCB("Disable Email Id Validation", cbDisableEmailIdValidation));
		vpLead.add(getFieldCB("Hide Number Range", cbHideNumberRange));//Added by sheetal:16-02-2022

		VerticalPanel vpQuotation = new VerticalPanel();
		vpQuotation.add(getFieldCB("Make Group Mandatory", cbMakeQuotationGroupMandatory));
		vpQuotation.add(getFieldCB("Make Type Mandatory", cbQuotationTypeMandatory));
		vpQuotation.add(getFieldCB("Make Category Mandatory", cbQuotationCategoryMandatory));
		vpQuotation.add(getFieldCB("Enable Do Not Print Product Description", cbDonotProductDescription));
		vpQuotation.add(getFieldCB("Enable Print Product Description", cbPrintProductDescriptionOnPDF));
		vpQuotation.add(getFieldCB("Make Quotation Date Disable", cbMakeQuotationDateDisable));
		vpQuotation.add(getFieldCB("Make Lead Id Mandatory", cbMakeLeadIdMandatory));
		vpQuotation.add(getFieldCB("Print Product Primices in PDF", cbPrintProductPrimiseInQuotationPdf));
		vpQuotation.add(getFieldCB("Print Product Remark With Premise", cbprintProductRemarkWithPremise));
		vpQuotation.add(getFieldCB("Do Not Print Product Warranty On Quotation Print", cbPC_DoNotPrintProductWarrantyOnQuotation));
						
		VerticalPanel vpContract = new VerticalPanel();
		vpContract.add(getFieldCB("Make Group Mandatory", cbMakeContractGroupMandatory));
		vpContract.add(getFieldCB("Make Type Mandatory", cbMakeContractTypeMandatory));
		vpContract.add(getFieldCB("Print Product Primices in PDF", cbPrintProductPrimiseInPdf));
		vpContract.add(getFieldCB("Make Number Range Mandatory", cbMakeNumberRangeMandatory));
		vpContract.add(getFieldCB("Make Contract Date Disable", cbMakeContractDateDisable));
		vpContract.add(getFieldCB("Display Edit button on Approved contract", cbOnlyForEva));
		vpContract.add(getFieldCB("Allow editing product and payment terms after approval", cbEditApprovedContract));
		vpContract.add(getFieldCB("Restrict Contracts Download With OTP", cbRestrictContractsDownloadWithOTP));
		vpContract.add(getFieldCB("Show Frequency column At Product Level", cbFrequencyAtProductLevel));
		vpContract.add(getFieldCB("Make Frequency Mandatory At Product Level", cbMakeFrequencyMandatoryAtProductLevel));
		vpContract.add(getFieldCB("Show Delete Duplicate Service Button", cbShowDeleteDuplicateServiceButton));
		vpContract.add(getFieldCB("Do Not Print Quantity column", cbPC_DoNotPrintQuantity));
		vpContract.add(getFieldCB("Create only service contract (No billing)", cbPedioContract));
		vpContract.add(getFieldCB("Make Product Name Non Editable", cbMAKEPRODUCTNAMENONEDITABLE));
		vpContract.add(getFieldCB("Make Service Type Mandatory", cbMakeServiceTypeMandatory));
		vpContract.add(getFieldCB("Remove Service Address From Branch Popup", cbRemoveServiceAddressFromBranchPopup));
		vpContract.add(getFieldCB("Make Terms and Conditions Upload Mandatory", cbMakeTermsUploadMandatory));
		vpContract.add(getFieldCB("Do Not Print Product Warranty On Contract Print", cbPC_DoNotPrintProductWarrantyOnContract));
		serviceTabPanel.add(vpCustomer, "Customer");
		serviceTabPanel.add(vpLead, "Lead");
		serviceTabPanel.add(vpQuotation, "Quotation");
		serviceTabPanel.add(vpContract, "Contract");

		serviceTabPanel.selectTab(0);
		
		fbuilder = new FormFieldBuilder("",serviceTabPanel);
		FormField fserviceTabPanel= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
	
		
		HorizontalPanel hpBranchLevelRestriction = new HorizontalPanel();
		hpBranchLevelRestriction.add(getFieldCB("Branch Level Restriction", cbBranchLevelRestriction));
			
		fbuilder = new FormFieldBuilder("",hpBranchLevelRestriction);
		FormField fhpBranchLevelRestriction= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		HorizontalPanel hpCustomerBranchLevelRestriction = new HorizontalPanel();
		hpCustomerBranchLevelRestriction.add(getFieldCB("Customer Branch Level Restriction", cbCustomerBranchRestriction));
			
		fbuilder = new FormFieldBuilder("",hpCustomerBranchLevelRestriction);
		FormField fhpCustomerBranchLevelRestriction= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		fbuilder = new FormFieldBuilder("",btnSetupPedio);
		FormField fbtnCopyPedioConfiguration= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		cbDisableIPAddressEmail
		
		HorizontalPanel hpDisableIPAddressEmail = new HorizontalPanel();
		hpDisableIPAddressEmail.add(getFieldCB("Disable IP Address Email", cbDisableIPAddressEmail));
		fbuilder = new FormFieldBuilder("",hpDisableIPAddressEmail);
		FormField fhpDisableIPAddressEmail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		
		Console.log("before setting FormField[][]");
			FormField[][] formfield = { 
					      {fgroupingGenralInformation},
					      {fcompany,fBranch,fbilling,fnonTaxNumberRange,fuser},
					      {fserviceProduct,fitemProduct,fauthorization,frestrictUserLocationAccess,fLocality},
					      {fcity,fhpBranchAsCompany,fhpCityNotMandatory,fhpBranchWiseOrCompanyWiseNumberRange},
					      {fhpNumberRangeMandatory,fhpBranchLevelRestriction,fhpCustomerBranchLevelRestriction,fhpDisableIPAddressEmail},
					      {fhpMakeNumberRangeReadOnly},
					      {fPreferences},
					      {fsetuptabPanel},
					      {fgroupingAccounts},
					      {fbank,finvoicePrifix,fbranchWiseInvoiceNumberRange,foutstandingDueList,foutstandingDueClientEmail},
					      {fcbExpenseAccoutingInterface,fcbPayrollAccountingInterface},
					      {faccountsTabCheckboxpanel},   
					      {fgroupingRenewals},
					      {fcontractlossRenewalReasons,fhsContractRenwallistemail,fhsOneTimeContract,fhsHideBankdetailsonpdf},
					      {fhsQuotationMandatory,fhsrenewalRemindertoClientEmail,fhsContractRenwalToClientSMS,fhsprintServiceAddressOnRenewal},
					      {fhscbDoNotPrintDiscountColumnOnRenewalPrint},
					      {fgroupingServiceScheduling},
					      {fhpScheduling},
					      {fbtnCopyPedioConfiguration},
					      {fserviceSchedulingTabPanel},
					      {fgroupingService},
					      {fserviceTabPanel},
//					      {fgroupingInventory},
//					      {factivateDeactivateInventoryProcessConfigs},
//					      {fgroupingSales},
//					      {fgroupingPurchase},
//					      {fgroupingHRPayroll}
					      
			};

			this.fields=formfield;
			Console.log("after setting up FormField[][]");

		
		

	}




	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW || AppMemory.getAppMemory().currentState==ScreeenState.EDIT || AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
					menus[k].setVisible(false);  		   

			}
		}

		AuthorizationHelper.setAsPerAuthorization(Screen.IMPLEMENTATIONSCREEN,LoginPresenter.currentModule.trim());

	}


	@Override
	public void applyStyle() {
		content.getElement().setId("formcontent");//Scrollbar was not coming so added this line

		
		company.getElement().setId("hyperlinkfont1");
		Branch.getElement().setId("hyperlinkfont1");
		defineBranchAsCompany.getElement().setId("hyperlinkfont1");
		billing.getElement().setId("hyperlinkfont1");
		nonTaxNumberRange.getElement().setId("hyperlinkfont1");
		user.getElement().setId("hyperlinkfont1");
		serviceProduct.getElement().setId("hyperlinkfont1");
		itemProduct.getElement().setId("hyperlinkfont1");
		enableMakerChecker.getElement().setId("hyperlinkfont1");
		authorization.getElement().setId("hyperlinkfont1");
		restrictUserLocationAccess.getElement().setId("hyperlinkfont1");
		Locality.getElement().setId("hyperlinkfont1");
		city.getElement().setId("hyperlinkfont1");
		
		bank.getElement().setId("hyperlinkfont1");
		branchWiseInvoiceNumberRange.getElement().setId("hyperlinkfont1");
		invoicePrifix.getElement().setId("hyperlinkfont1");
		enableAccountingInterface.getElement().setId("hyperlinkfont1");
		outstandingDueList.getElement().setId("hyperlinkfont1");
		outstandingDuetoClientEmail.getElement().setId("hyperlinkfont1");

		singleOrOnetimeCOntract.getElement().setId("hyperlinkfont1");
		contractlossRenewalReasons.getElement().setId("hyperlinkfont1");
		renewalReminderToclient.getElement().setId("hyperlinkfont1");
		renewalRemindersms.getElement().setId("hyperlinkfont1");
		printServiceAddressOnRenewal.getElement().setId("hyperlinkfont1");
		doNotPrintDiscountColumnOnRenewalPrint.getElement().setId("hyperlinkfont1");
		contractRenewallistReminderEmail.getElement().setId("hyperlinkfont1");
		quotationMandatory.getElement().setId("hyperlinkfont1");
		hideBankDetailsonPDF.getElement().setId("hyperlinkfont1");
		

		cbBranchAsCompany.setStyleName("cbboxWidth");
		cbCityNotMandatory.setStyleName("cbboxWidth");
		cbNumberRangeMandatory.setStyleName("cbboxWidth");
		cbBranchWiseOrCompanyWiseNumberRange.setStyleName("cbboxWidth");
		cbEnableSelfApproval.setStyleName("cbboxWidth");
		cbOutstandingDuelist.setStyleName("cbboxWidth");
		cbOutstandingtoClientEmail.setStyleName("cbboxWidth");
		cbquotationMandatory.setStyleName("cbboxWidth");
		cbHideBankDetailsonPDF.setStyleName("cbboxWidth");
		cbRenewalReminderSMS.setStyleName("cbboxWidth");
		printServiceAddressOnRenewal.setStyleName("cbboxWidth");
		doNotPrintDiscountColumnOnRenewalPrint.setStyleName("cbboxWidth");
		cbRenewalReminderlistEmail.setStyleName("cbboxWidth");
		cbOneTimeContract.setStyleName("cbboxWidth");
		cbRenewalReminderToClient.setStyleName("cbboxWidth");

		
		enableAccountsProcessConfigs.getElement().setId("hyperlinkfont1");
		
		serviceRescheduleReasons.getElement().setId("hyperlinkfont1");
		serviceCancellationReasons.getElement().setId("hyperlinkfont1");
		typeOfTreatment.getElement().setId("hyperlinkfont1");
		frequency.getElement().setId("hyperlinkfont1");
		findingType.getElement().setId("hyperlinkfont1");
		serviceChecklist.getElement().setId("hyperlinkfont1");
		activateSchedulingProcessConfig.getElement().setId("hyperlinkfont1");
		activateDeactivateServiceProcessConfigs.getElement().setId("hyperlinkfont1");
		activateDeactivateInventoryProcessConfigs.getElement().setId("hyperlinkfont1");

		cbQuotationSelfApproval.setStyleName("cbboxWidth");
		
		cbNumberRangeMandatoryOnLead.setStyleName("cbboxWidth");
		cbNumberRangeMandatoryOnQuotation.setStyleName("cbboxWidth");
		
		cbContractSelfApproval.setStyleName("cbboxWidth");

		cbSalesQuotationSelfApproval.setStyleName("cbboxWidth");
		cbSalesOrderSelfApproval.setStyleName("cbboxWidth");
		cbSalesOrderMakeReferenceNumberMandatory.setStyleName("cbboxWidth");
		cbPOSelfApproval.setStyleName("cbboxWidth");
		cbServicePOSelfApproval.setStyleName("cbboxWidth");
		cbGRNSelfApproval.setStyleName("cbboxWidth");
		cbMRNSelfApproval.setStyleName("cbboxWidth");
		cbMINSelfApproval.setStyleName("cbboxWidth");
		cbMMNSelfApproval.setStyleName("cbboxWidth");
		cbBillingSelfApproval.setStyleName("cbboxWidth");
		cbBillSelfApproval.setStyleName("cbboxWidth");
		cbInvoiceSelfApproval.setStyleName("cbboxWidth");
		cbinvoiceSelfApproval.setStyleName("cbboxWidth");
		cbVendorInvoiceSelfApproval.setStyleName("cbboxWidth");
		cbPaymentDateMandatory.setStyleName("cbboxWidth");
		cbPaymentReceivedDateMandatory.setStyleName("cbboxWidth");
		cbOnlyForEva.setStyleName("cbboxWidth");
		cbEditApprovedContract.setStyleName("cbboxWidth");
		cbRestrictContractsDownloadWithOTP.setStyleName("cbboxWidth");
		cbFrequencyAtProductLevel.setStyleName("cbboxWidth");
		cbMakeFrequencyMandatoryAtProductLevel.setStyleName("cbboxWidth");
		cbShowDeleteDuplicateServiceButton.setStyleName("cbboxWidth");
		cbPC_DoNotPrintQuantity.setStyleName("cbboxWidth");	
		cbPedioContract.setStyleName("cbboxWidth");
		cbMAKEPRODUCTNAMENONEDITABLE.setStyleName("cbboxWidth");
		cbMakeServiceTypeMandatory.setStyleName("cbboxWidth");
		cbRemoveServiceAddressFromBranchPopup.setStyleName("cbboxWidth");
		cbMakeTermsUploadMandatory.setStyleName("cbboxWidth");
		cbPC_DoNotPrintProductWarrantyOnContract.setStyleName("cbboxWidth");
				
		cbSalesPriceMandatory.setStyleName("cbboxWidth");
		cbPurchasePriceMandatory.setStyleName("cbboxWidth");
		cbMakeServiceCompletionMandatoryForAMCContract.setStyleName("cbboxWidth");
		cbEnableAutoInvoice.setStyleName("cbboxWidth");
		cbEnableServiceBranch.setStyleName("cbboxWidth");

		cbinvoiceAccountingInterface.setStyleName("cbboxWidth");
		cbInvoiceAccoutingInterface.setStyleName("cbboxWidth");
		cbCompanyAsLetterHead.setStyleName("cbboxWidth");
		cbPrintProductPrimicesinPDF.setStyleName("cbboxWidth");
		cbInvoiceEditAllRoll.setStyleName("cbboxWidth");
		cbmakeAddressToUppercase.setStyleName("cbboxWidth");
		cbPrintPoDetailsOnInvoice.setStyleName("cbboxWidth");
		cbHideRateandDiscountColumn.setStyleName("cbboxWidth");
		cbPrintProductDiscriptionOnPdfInInvoice.setStyleName("cbboxWidth");
		
		cbTransferMaterialWhileScheduling.setStyleName("cbboxWidth");
//		cbautoEmailJobCardOnCompletion.setStyleName("cbboxWidth");
		cbComplainServiceWithTurnAroundTime.setStyleName("cbboxWidth");
		cbCloseComplaintWhenServiceClose.setStyleName("cbboxWidth");
		cbSendSrCopyOnServiceCompletion.setStyleName("cbboxWidth");
		cbCustomizedSrCopy.setStyleName("cbboxWidth");
		cbMAKEMATERIALMANDATORY.setStyleName("cbboxWidth");
		cbMakeServiceEditableAfterCompletion.setStyleName("cbboxWidth");
		cbMultipleExpenseMngt.setStyleName("cbboxWidth");
		cbDATETECHNICIANWISESERVICEPOPUP.setStyleName("cbboxWidth");
		
		cbHideCompleteRescheduleFumigationColoumn.setStyleName("cbboxWidth");
		cbHideFumigationButton.setStyleName("cbboxWidth");
		cbCOMPLAINSERVICEWITHTURNAROUNDTIME.setStyleName("cbboxWidth");
		cbEmailCustomizedAndGenericSRCopy.setStyleName("cbboxWidth");
		cbPC_SERVICESCHEDULEMINMAXROUNDUP.setStyleName("cbboxWidth");
		cbPC_HIDE_CS_LIST_PLAN_BUTTON.setStyleName("cbboxWidth");
		cbPC_PedioFindingAsServiceQuantityInBilling.setStyleName("cbboxWidth");
		cbENABLECOMPLETEBUTTONONSCHEDULEPOPUP.setStyleName("cbboxWidth");
		cbHideServiceValueExceptAdmin.setStyleName("cbboxWidth");
		cbRestrictServiceDownloadWithOTP.setStyleName("cbboxWidth");
		cbStopServiceRescheduleEmail.setStyleName("cbboxWidth");
		cbUpdateFindingEntity.setStyleName("cbboxWidth");
		
		cbEnablePedio.setStyleName("cbboxWidth");
		cbReturnQtyUpdate.setStyleName("cbboxWidth");
		cbServiceScheduleMaterailMandatory.setStyleName("cbboxWidth");
		cbCopyMaterialToRemainingServices.setStyleName("cbboxWidth");
		cbEnableFcmNotification.setStyleName("cbboxWidth");
		cbEnableServiceScheduleNotification.setStyleName("cbboxWidth");

		cbMAKETECHNICIANREMARKMANDITORY.setStyleName("cbboxWidth");
		cbMAKECUSTOMERNAMEMANDITORY.setStyleName("cbboxWidth");
		cbMAKECUSTOMERREMARKMANDITORY.setStyleName("cbboxWidth");
		cbMAKETYPEOFTREATMENTMANDATORY.setStyleName("cbboxWidth");
		cbMAKEFINDINGSMANDATORY.setStyleName("cbboxWidth");
		
		cbHIDESERVICEFINDING.setStyleName("cbboxWidth");
		cbHIDEMATERIALS.setStyleName("cbboxWidth");
		cbDATETECHNICIANWISESERVICEPOPUP.setStyleName("cbboxWidth");
		cbDIRECTCOMPLETE.setStyleName("cbboxWidth");
		cbTECHNICIANATTENDANCE.setStyleName("cbboxWidth");
		cbSITEFINDING.setStyleName("cbboxWidth");
		cbREJECTSERVICE.setStyleName("cbboxWidth");
		cbHIDESRS.setStyleName("cbboxWidth");
		cbLIVETRACKING.setStyleName("cbboxWidth");
		cbSENDOTPFORSERVICECOMPLETION.setStyleName("cbboxWidth");
		cbPC_PEDIO_HIDE_TREATMENTS.setStyleName("cbboxWidth");
		cbPC_ADDMIXINGRATIOANDLOTNO.setStyleName("cbboxWidth");
		cbPC_DocumentUploadMandatory.setStyleName("cbboxWidth");
		cbPC_Pedio_Hide_Reschedule.setStyleName("cbboxWidth");
		cbPC_Hide_Customer_Mobile.setStyleName("cbboxWidth");
		cbLoadLoggedInUserWiseCustomer.setStyleName("cbboxWidth");
		cbPREMISESFLAG.setStyleName("cbboxWidth");
		cbMARKSERVICECOMPLETEDFROMAPP.setStyleName("cbboxWidth");
		cbSHOWEMAIL.setStyleName("cbboxWidth");
		cbPEDIO_HIDEPHOTOS.setStyleName("cbboxWidth");
		cbShowProductPremises.setStyleName("cbboxWidth");
		cbPC_DoNotShowRatings.setStyleName("cbboxWidth");
		cbMAKESTARRATINGMANDITORY.setStyleName("cbboxWidth");
		cbPC_HideCustomerRemark.setStyleName("cbboxWidth");
				
		cbprintCustomerRemark.setStyleName("cbboxWidth");
		cbPrintTreatmentOnSRCopy.setStyleName("cbboxWidth");
		cbPrintPhotoOnSRCopy.setStyleName("cbboxWidth");
		cbTechnicianRemark.setStyleName("cbboxWidth");
		cbPedioNotificationRemark.setStyleName("cbboxWidth");
		cbhideMobileNo.setStyleName("cbboxWidth");
		cbhideCustomerFeedback.setStyleName("cbboxWidth");
		cbUploadSRCopy.setStyleName("cbboxWidth");
		
		cbHIDEMATERIALANDUOM.setStyleName("cbboxWidth");
		cbDecreaseTableRow.setStyleName("cbboxWidth");
		cbPC_HideCellNumber_SRCopy.setStyleName("cbboxWidth");
		cbPC_DoNotPrintMaterialOnSummarySR.setStyleName("cbboxWidth");
		cbPRINTMATERIALONSRCOPY.setStyleName("cbboxWidth");
		cbPRINTPHOTOONSRCOPY.setStyleName("cbboxWidth");


		cbMakeCustomerEmailMandatory.setStyleName("cbboxWidth");
		cbMakeCustGroupMandatory.setStyleName("cbboxWidth");
		cbMakeCustPinCodeMandatory.setStyleName("cbboxWidth");
		cbMakeCustRefMandatory.setStyleName("cbboxWidth");
		cbMakeCustLevelMandatory.setStyleName("cbboxWidth");
		cbHideCustomerGroup.setStyleName("cbboxWidth");
		cbMakecustCategoryAndTypeDisable.setStyleName("cbboxWidth");
		cbMakeCellNoNonMandatory.setStyleName("cbboxWidth");
		cbMakeAccountManagerMandatory.setStyleName("cbboxWidth");
		cbMakeSalesPersonMandatory.setStyleName("cbboxWidth");
		cbEnableloadLargeCustomerData.setStyleName("cbboxWidth");
		cbHideCustomerReligion.setStyleName("cbboxWidth");
		

		cbMakeLeadGroupMandatory.setStyleName("cbboxWidth");
		cbLeadCustPopupEmailMandatory.setStyleName("cbboxWidth");
		cbMakeCustomerPopUpCompanyNameMandatory.setStyleName("cbboxWidth");
		cbMakeCustomerPopUpPhoneNumberMandatory.setStyleName("cbboxWidth");
		cbEnableFreezeLead.setStyleName("cbboxWidth");
		cbCommunicationLogColumn.setStyleName("cbboxWidth");
		cbMapCustomerTypeToLeadCategory.setStyleName("cbboxWidth");
		cbMakePriceColumnDisable.setStyleName("cbboxWidth");
		cbChangeProductComposite.setStyleName("cbboxWidth");
		cbDisableEmailIdValidation.setStyleName("cbboxWidth");
		cbHideNumberRange.setStyleName("cbboxWidth");


		cbMakeQuotationGroupMandatory.setStyleName("cbboxWidth");
		cbQuotationTypeMandatory.setStyleName("cbboxWidth");
		cbQuotationCategoryMandatory.setStyleName("cbboxWidth");
		cbDonotProductDescription.setStyleName("cbboxWidth");
		cbPrintProductDescriptionOnPDF.setStyleName("cbboxWidth");
		cbMakeQuotationDateDisable.setStyleName("cbboxWidth");
		cbMakeLeadIdMandatory.setStyleName("cbboxWidth");
		cbprintProductRemarkWithPremise.setStyleName("cbboxWidth");
		cbPrintProductPrimiseInQuotationPdf.setStyleName("cbboxWidth");
		cbPC_DoNotPrintProductWarrantyOnQuotation.setStyleName("cbboxWidth");
		
		cbMakeContractGroupMandatory.setStyleName("cbboxWidth");
		cbMakeContractTypeMandatory.setStyleName("cbboxWidth");
		cbPrintProductPrimiseInPdf.setStyleName("cbboxWidth");
		cbMakeNumberRangeMandatory.setStyleName("cbboxWidth");
		cbMakeContractDateDisable.setStyleName("cbboxWidth");

		cbpaymentAccoutingInterface.setStyleName("cbboxWidth");
		cbPaymentAddIntoPettyCash.setStyleName("cbboxWidth");
		cbPaymentCompanyAsLetterHead.setStyleName("cbboxWidth");
		
		cbPrintOrderNoAndGrnNo.setStyleName("cbboxWidth");
		cbPaymentAccountningInterface.setStyleName("cbboxWidth");
	}
	
	private Widget getFieldCB(String labelName, CheckBox checkBox) {
		
		HorizontalPanel horizontelpanel = new HorizontalPanel();
//		horizontelpanel.setSpacing(10);
//
//		checkBox.getElement().getStyle().setMarginRight(2.0, Unit.PX);
//		checkBox.getElement().getStyle().setMarginTop(2.0, Unit.PX);
//		checkBox.getElement().getStyle().setWidth(50, Unit.PX);
//		checkBox.getElement().getStyle().setMarginLeft(2, Unit.PX);

		horizontelpanel.add(checkBox);

		Label label = new Label(labelName);
		label.getElement().getStyle().setMarginTop(3, Unit.PX);
//		label.setWidth("150px");
		horizontelpanel.add(label);

		return horizontelpanel;
	}
	
	
	
	private Widget getFieldBlankLabel(String labelName, Label label2) {
		
			HorizontalPanel horizontelpanel = new HorizontalPanel();
	
			horizontelpanel.add(label2);
	
			Label label = new Label(labelName);
			label.setWidth("150px");
			horizontelpanel.add(label);
	
			return horizontelpanel;
		}
	
	}
