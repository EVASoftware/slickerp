package com.slicktechnologies.client.views.implementation;


import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;

public class CustomerPortalLinkSharePopup extends PopupScreen{
	
	
	public CheckBox sms;
	//Button custUploadOkBtn,custUploadCancleBtn;
	public CheckBox whatsapp;
	public CheckBox email;
	public Label msg;
	
	public CustomerPortalLinkSharePopup(){
		super();
		createGui();
		
	}
	
	
	
	private void initilizewidget() {
		
		sms = new CheckBox("SMS");
		whatsapp = new CheckBox("WhatsApp");
		email = new CheckBox("Email");
		msg=new Label("select communication channel");
		
	}
	
	@Override
	public void createScreen() {
		initilizewidget();
		
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder("",msg);
		FormField fmsg= fbuilder.setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder();		
		FormField fgroupingExpenseDetails=fbuilder.setlabel("Do you want to send customer portal link to all your customers?").widgetType(FieldType.GREYGROUPING).setMandatory(false).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",sms);
		FormField fsms= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",whatsapp);
		FormField fwhatsapp= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",email);
		FormField femail= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		
		FormField[][] formfield = {
				{fgroupingExpenseDetails},
				{fmsg},
				{fsms,fwhatsapp,femail}
		};


		this.fields=formfield;		

	}

	
	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}


}

