package com.slicktechnologies.client.views.fumigationdetails;

import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.PhoneNumberBox;

public class FumigationNewCustomerPopup extends AbsolutePanel {
	Button btnCancel,btnOk;
	TextBox tbcompanyName;
	TextBox tbPocName;
	EmailTextBox etbEmail;
	TextBox pnbLandlineNo;
	InlineLabel label;
	
	public FumigationNewCustomerPopup() {
//		label=new InlineLabel(type);
//		add(label,10,10);
		
//		InlineLabel lable1=new InlineLabel("Company Name");
//		add(lable1,10,30);
//		tbcompanyName=new TextBox();
//		add(tbcompanyName,10,50);
//		
//		InlineLabel lable2=new InlineLabel("POC Name");
//		add(lable2,100,70);
//		tbPocName=new TextBox();
//		add(tbPocName,100,70);
//
//		InlineLabel lable3=new InlineLabel("Email");
//		add(lable3,10,90);
//		etbEmail=new EmailTextBox();
//		add(etbEmail,10,110);
//		
//		InlineLabel lable4=new InlineLabel("Cell No.");
//		add(lable4,10,130);
//		pnbLandlineNo=new PhoneNumberBox();
//		add(pnbLandlineNo,100,130);
//		
//		
//		btnOk=new Button("  Ok  ");
//		add(btnOk, 275, 300);
//		btnCancel = new Button("Cancel");
//		add(btnCancel,375,300);
//		
//		setSize("700px","400px");
//		this.getElement().setId("form");
	}

	public FumigationNewCustomerPopup(String type) {
		label=new InlineLabel(type);
		add(label,10,10);
		
		InlineLabel lable1=new InlineLabel("Company Name");
		add(lable1,10,30);
		tbcompanyName=new TextBox();
		add(tbcompanyName,10,50);
		
		InlineLabel lable2=new InlineLabel("POC Name");
		add(lable2,200,30);
		tbPocName=new TextBox();
		add(tbPocName,200,50);

		InlineLabel lable3=new InlineLabel("Email");
		add(lable3,10,90);
		etbEmail=new EmailTextBox();
		add(etbEmail,10,110);
		
		InlineLabel lable4=new InlineLabel("Cell No.");
		add(lable4,200,90);
		pnbLandlineNo=new TextBox();
		add(pnbLandlineNo,200,110);
		
		
		btnOk=new Button("  Ok  ");
		add(btnOk, 150, 180);
		btnCancel = new Button("Cancel");
		add(btnCancel,250,180);
		
		setSize("400px","230px");
		this.getElement().setId("form");
		
	}

	public Button getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}

	public Button getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}

	public TextBox getTbcompanyName() {
		return tbcompanyName;
	}

	public void setTbcompanyName(TextBox tbcompanyName) {
		this.tbcompanyName = tbcompanyName;
	}

	public TextBox getTbPocName() {
		return tbPocName;
	}

	public void setTbPocName(TextBox tbPocName) {
		this.tbPocName = tbPocName;
	}

	public EmailTextBox getEtbEmail() {
		return etbEmail;
	}

	public void setEtbEmail(EmailTextBox etbEmail) {
		this.etbEmail = etbEmail;
	}


	public InlineLabel getLabel() {
		return label;
	}

	public void setLabel(InlineLabel label) {
		this.label = label;
	}

	public TextBox getPnbLandlineNo() {
		return pnbLandlineNo;
	}

	public void setPnbLandlineNo(TextBox pnbLandlineNo) {
		this.pnbLandlineNo = pnbLandlineNo;
	}
	
	
	
	

}
