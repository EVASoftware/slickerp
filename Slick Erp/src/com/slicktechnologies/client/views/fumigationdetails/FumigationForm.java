package com.slicktechnologies.client.views.fumigationdetails;

import java.util.Vector;

import com.gargoylesoftware.htmlunit.WebConsole.Logger;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ProcessLevelBar;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Declaration;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class FumigationForm extends FormScreen<Fumigation> implements ChangeHandler{

	TextBox tbtreatmentcardno;
	DateBox dbdateofissue;
	ObjectListBox<Country> olbimportcountry;
	
	TextBox tbdescriptionofgoods;
	TextBox tbibQuantitydeclared;
	TextBox tbdistinguishingmarks;
	TextBox tbconsignmentlink;
	
	
	//   rohan added this 3 fields for siddhivinay pest control 
	
	TextBox packing;
	TextBox volumeOfContainer;
	TextBox totalNetWeight;
	TextBox grossWeight;
	
	//****************rohan chnages here ap  per suma pest cotrol changes ************
	
	/** The person info composite. */
	PersonInfoComposite personInfoComposite;
	
	Button addbyCONSIGNERComposite;
	PersonInfoComposite personInfoCONSIGNERComposite;
	Button addbyCONSIGNEEComposite;
	PersonInfoComposite personInfoCONSIGNEEComposite;
	TextBox tbShippingBillNo;
	TextBox tbInvoiceNo;
	TextBox tbCertificateNo;
	ObjectListBox<Declaration> declarationMessageList;
	Button addDeclarationButton;
	
	
	//***************button for editing customer ************* 
	Button editconsignee,editconsigner;
 	
	ObjectListBox<Config> olbdoquantitydeclareUnit;
	//************************changes ends here *************************************
	
	TextBox tbnameofvessel;
	ObjectListBox<Country> olbcountrydestinatoin;
	
//	ObjectListBox<Config> olbpointofentry;
	TextBox tbconsignername;
	/**
	 * Date 26-7-2018 byjayshree
	 */
	
	Label address,address1;
	TextArea fromacshipping;
	TextArea toacshipping;
	


	//	AddressComposite fromacshippingcomposite;
	TextBox tbconsignee;
//	AddressComposite toacshippingcomposite;
	
	ListBox lbnamefumigation;
	DateBox dbdatefumigation; 
//	TextBox tbplaceoffumigation;
//	TextBox tbportcountyloading;
	
//	ObjectListBox<Config> olbdosagerateoffumigation;
//	ObjectListBox<Config> olbdurationoffumigation;//comment by jayshree
	TextBox tbbdurationoffumigation;
	
	
	

	

//	ObjectListBox<Config> olbportcountyloading;
	
	
//	ObjectListBox<Config> olplaceoffumigation;
	ObjectListBox<Config> fumigationPerformed;//Add By Jayshree
	ObjectListBox<Config> ventilated;//Add By jayshree
	TextBox olbminairtemp;				//Updated By:Viraj Date:12-01-2019 Description: Changed to textBox
	
	TextArea tacomment,moreComment;
	TextBox tbplace;
	DateBox dbdateadd;
	
	TextBox tbcontractId;
	TextBox tbserviceId;
	DateBox dbcontractstartdate;
	DateBox dbcontractenddate;
	DateBox dbservicedate;
	
	TextBox tbcount;
	
	Label note1,note2,note3,note4,note5;
	Label note11,note12,note13,note14,note15;
	ListBox lbnote1,lbnote2,lbnote3,lbnote4;
	ListBox lbnote11,lbnote12,lbnote13,lbnote14,lbnote15;
	ListBox lbnote123;
	
	Button newFromCustomer;
	Button newToCustomer;
	
	String consignerName;
	String consignerPocName;
	String consignerEmail;
	Long consignerCellNo;
	
	String consigneeName;
	String consigneePocName;
	String consigneeEmail;
	Long consigneeCellNo;
	
	public boolean consignereditFlag=false;
	public boolean consigneeEditFlag=false;
	
	DateBox invoiceDate;
	DateBox shippingBillDate;
	Fumigation fumigationObj;
	//komal
	IntegerBox tbSerialNumber;
	ObjectListBox<String> olbImportOrExport;
	TextBox tbDtePPQSRegistrationNumber;
	AddressComposite bcBranchAddress;
	TextBox tbNameOfCommodity;
	/** Updated  By: Viraj Date: 17-04-2019 Description:Changed according to pest mortom requirement **/
	TextBox ibQuantity;
	/** Ends **/
	ObjectListBox<Branch> olbBranch;
	TextArea taDescrioptionOfGoods;
	Label branch , certificateNumber , pqrsRegdNumber  , fumigationId , serialNumber , dateOfIssue , ImportExport;
	Label descriptionOfGoods , commodity , qty;
	/** date 24.5.2018 added by komal for number range **/
	ObjectListBox<Config> olbNumberRange;
	//komal
	Button selectTemplateButton;
	
	/** date 12.1.2018 added by komal to make dropdowns to textbox **/
	TextBox tbPlaceOfFumigation;
	TextBox tbDosageOfFumigation;
	TextBox tbPortOfLoading;
	TextBox tbPortOfEntry;
	
	public FumigationForm() {
		super();
		createGui();
	}
	
	public FumigationForm (String[] processlevel, FormField[][] fields,
			FormStyle formstyle) 
	{
		super(processlevel, fields, formstyle);
		createGui();
		
		
	}
	
	
	private void initalizeWidget(){
		
		
		declarationMessageList = new ObjectListBox<Declaration>();
		MyQuerry querry123 = new MyQuerry(new Vector<Filter>(),new Declaration());
		declarationMessageList.MakeLive(querry123);
		
		addDeclarationButton= new Button("Add Declaration");
		
		tbcount=new TextBox();
		tbcount.setEnabled(false);
		
		tbtreatmentcardno=new TextBox();
		dbdateofissue=new  DateBoxWithYearSelector();
		
		
		tbdescriptionofgoods=new TextBox();
		tbibQuantitydeclared=new TextBox();
		tbdistinguishingmarks=new TextBox();
		tbconsignmentlink=new TextBox();
		
		
		 packing = new TextBox();
		 volumeOfContainer =  new TextBox();
		 totalNetWeight= new TextBox();
		 grossWeight = new TextBox();
		
		//***************rohan changes 
		
		 addbyCONSIGNERComposite = new Button("Add");
		 	MyQuerry custquerry1 = new MyQuerry();
			Filter custfilteer1 = new Filter();
			custfilteer1.setQuerryString("status");
			custfilteer1.setBooleanvalue(true);
			custquerry1.setQuerryObject(new Customer());
			personInfoCONSIGNERComposite = new PersonInfoComposite(custquerry1, false);
		 
		
		
		addbyCONSIGNEEComposite= new Button("Add");
		
		MyQuerry custquerry = new MyQuerry();
		Filter custfilteer = new Filter();
		custfilteer.setQuerryString("status");
		custfilteer.setBooleanvalue(true);
		custquerry.setQuerryObject(new Customer());
		personInfoCONSIGNEEComposite = new PersonInfoComposite(custquerry, false);
		
		
		
		
		tbCertificateNo=new TextBox();
		/** date 24.5.2018 added by komal**/
		tbCertificateNo.setEnabled(false);
		tbInvoiceNo = new TextBox();
		tbShippingBillNo= new TextBox();
		
//		tbportcountyloading=new TextBox();
		tbnameofvessel=new TextBox();
		
		olbimportcountry=new ObjectListBox<Country>();
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Country());
		olbimportcountry.MakeLive(querry);
		
		olbcountrydestinatoin=new ObjectListBox<Country>();
		olbcountrydestinatoin.MakeLive(querry);
			
//		olbpointofentry=new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(olbpointofentry, Screen.FUMIGATIONDECLAREDPOE);
		
		tbconsignername=new TextBox();
		tbconsignername.setEnabled(false);
		
		fromacshipping=new TextArea();
		toacshipping=new TextArea();
//		fromacshippingcomposite=new AddressComposite(true);
		
		tbconsignee=new TextBox();
		tbconsignee.setEnabled(false);
		
//		toacshippingcomposite=new AddressComposite(true);
		
		lbnamefumigation=new ListBox();
		lbnamefumigation.addItem("--SELECT--");
		lbnamefumigation.addItem("Methyle Bromide");
	//	lbnamefumigation.addItem("Aluminium Phosphide");
		lbnamefumigation.setSelectedIndex(1);
		dbdatefumigation=new  DateBoxWithYearSelector();
//		tbplaceoffumigation=new TextBox();
		
		
		personInfoComposite=AppUtility.customerInfoComposite(new Customer());
		personInfoComposite.getCustomerId().getHeaderLabel().setText("* Customer ID");
		personInfoComposite.getCustomerName().getHeaderLabel().setText("* Customer Name");
		personInfoComposite.getCustomerCell().getHeaderLabel().setText("* Customer Cell");
		
	//	olbdosagerateoffumigation=new ObjectListBox<Config>();
	//	AppUtility.MakeLiveConfig(/, Screen.DOSAGERATEOFFUMIGANT);
		
		
//		olbdurationoffumigation=new ObjectListBox<Config>();//comment by jayshree
//		AppUtility.MakeLiveConfig(olbdurationoffumigation, Screen.DURATIONOFFUMIGATION);
		
		tbbdurationoffumigation=new TextBox();
		/**
		 * Updated By:Viraj 
		 * Date:12-01-2019 
		 * Description: Changed to textBox
		 */		
		olbminairtemp = new TextBox();
//		olbminairtemp=new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(olbminairtemp, Screen.MINIMUMAIRTEMP);
		/** Ends **/
		
//		olbportcountyloading= new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(olbportcountyloading, Screen.COUNTRYOFLOADING);
//
//		olplaceoffumigation= new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(olplaceoffumigation, Screen.PLACEOFFUMIGATION);
		
		
		fumigationPerformed= new ObjectListBox<Config>();//add by jayshree
		AppUtility.MakeLiveConfig(fumigationPerformed, Screen.FUMIGATIONPERFORMED);
		
		ventilated= new ObjectListBox<Config>();//add by jayshree
		AppUtility.MakeLiveConfig(ventilated, Screen.VENTILATED);
		
		//****************rohan *******************
		olbdoquantitydeclareUnit =new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbdoquantitydeclareUnit, Screen.QUANTITYDECLAREUNIT);
		
		tacomment=new TextArea();
		moreComment= new TextArea();
		tbplace=new TextBox();
		dbdateadd=new  DateBoxWithYearSelector();
		dbservicedate=new DateBoxWithYearSelector();
		dbservicedate.setEnabled(false);
		tbcontractId=new TextBox();
		tbcontractId.setEnabled(false);
		tbserviceId=new TextBox();
		tbserviceId.setEnabled(false);
		dbcontractstartdate=new DateBoxWithYearSelector();
		dbcontractstartdate.setEnabled(false);
		dbcontractenddate=new DateBoxWithYearSelector();
		dbcontractenddate.setEnabled(false);
//		note1=new Label("Fumigation has been performed in a container / under Gas tight enclosure / sheet ");
		note1=new Label("Fumigation has been performed in ");
//		note1.getElement().getStyle().setFontSize(15, Unit.PX);
		note2=new Label("Container pressure test conducted");
		note3=new Label("Container has 200 mm free air space at top of container ");
		note4=new Label("In transit fumigation needs ventillation at port of discharge ");
//		note5=new Label("Container/Enclosure has been ventillated to below 5ppm v/v Methyl Bromide ");
		note5=new Label(" has been ventillated to below 5ppm v/v Methyl Bromide ");
		lbnote123= new ListBox();
		lbnote123.addItem("--SELECT--");
		lbnote123.addItem("YES");
		lbnote123.addItem("NO");
		lbnote123.addItem("N.A.");
		lbnote123.setSelectedIndex(1);
		
		
		lbnote1=new ListBox();
		lbnote1.addItem("--SELECT--");
		lbnote1.addItem("YES");
		lbnote1.addItem("NO");
		lbnote1.addItem("N.A.");
		lbnote1.setSelectedIndex(3);
		
		lbnote2=new ListBox();
		lbnote2.addItem("--SELECT--");
		lbnote2.addItem("YES");
		lbnote2.addItem("NO");
		lbnote2.addItem("N.A.");
		lbnote2.setSelectedIndex(1);
		
		lbnote3=new ListBox();
		lbnote3.addItem("--SELECT--");
		lbnote3.addItem("YES");
		lbnote3.addItem("NO");
		lbnote3.addItem("N.A.");
		lbnote3.setSelectedIndex(3);
		
		lbnote4=new ListBox();
		lbnote4.addItem("--SELECT--");
		lbnote4.addItem("YES");
		lbnote4.addItem("NO");
		lbnote4.addItem("N.A.");
		lbnote4.setSelectedIndex(1);
		
		
		note11=new Label("Has the commodity been fumigated prior to lacquering,varnishing,painting or wrapping ?");
		note12=new Label("Has plastic Wrapping been use in consignment ?");
		note13=new Label("*If yes, has the consignment been fumigated prior wrapping ?");
		note14=new Label("*Or has the plastic wrapping been slashed,opened or perforated in accordance with the wrapping and perforation standards ?");
		note15=new Label("Is the timber in this consignment less than 200 mm thick in dimention and correctly spaced every 200 mm in height ?");
		
		lbnote11=new ListBox();
		lbnote11.addItem("--SELECT--");
		lbnote11.addItem("YES");
		lbnote11.addItem("NO");
		lbnote11.addItem("N.A.");
		lbnote11.setSelectedIndex(2);
		
		lbnote12=new ListBox();
		lbnote12.addItem("--SELECT--");
		lbnote12.addItem("YES");
		lbnote12.addItem("NO");
		lbnote12.addItem("N.A.");
		lbnote12.setSelectedIndex(3);
		
		lbnote13=new ListBox();
		lbnote13.addItem("--SELECT--");
		lbnote13.addItem("YES");
		lbnote13.addItem("NO");
		lbnote13.addItem("N.A.");
		lbnote13.setSelectedIndex(3);
		
		lbnote14=new ListBox();
		lbnote14.addItem("--SELECT--");
		lbnote14.addItem("YES");
		lbnote14.addItem("NO");
		lbnote14.addItem("N.A.");
		lbnote14.setSelectedIndex(3);
		
		lbnote15=new ListBox();
		lbnote15.addItem("--SELECT--");
		lbnote15.addItem("YES");
		lbnote15.addItem("NO");
		lbnote15.addItem("N.A.");
		lbnote15.setSelectedIndex(1);
		
		newFromCustomer=new Button("New Consigner");
		newToCustomer=new Button("New Consignee");
		
		invoiceDate =new DateBox();
		shippingBillDate = new DateBox();
		
		editconsignee = new Button("Edit Consignee");
		editconsigner = new Button("Edit Consigner");
		
		//komal
		tbSerialNumber = new IntegerBox();
		olbImportOrExport = new ObjectListBox<String>();		
		olbImportOrExport.addItem("--SELECT--");
		olbImportOrExport.addItem("Import");
		olbImportOrExport.addItem("Export");
		tbDtePPQSRegistrationNumber = new TextBox();
		bcBranchAddress = new AddressComposite();
	    tbNameOfCommodity = new TextBox();
	    ibQuantity = new TextBox();
	    olbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
		taDescrioptionOfGoods = new TextArea();
		taDescrioptionOfGoods.getElement().addClassName("textArea");
		branch = new Label("* Branch");
		certificateNumber =new Label("* Certificate No.");
		pqrsRegdNumber = new Label("Dte. PQRS Regd. No.");
		fumigationId = new Label("Fumigation ID");
		serialNumber = new Label("Serial No.");
		dateOfIssue = new Label("* Date of Issue");
		ImportExport = new Label("Import/Export");
		commodity = new Label("Name Of Commodity");
		qty = new Label("QTY");
		descriptionOfGoods = new Label("* Description of Goods");
		olbBranch.addChangeHandler(this);
		//komal
		selectTemplateButton = new Button("Select Template");
		/** date 24.5.2018 added by komal **/
		olbNumberRange = new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbNumberRange, Screen.NUMBERRANGE);
		
		/** date 12.1.2018 added by komal to make dropdowns to textbox **/
		tbPlaceOfFumigation = new TextBox();
		tbDosageOfFumigation = new TextBox();
		tbPortOfLoading = new TextBox();
		tbPortOfEntry = new TextBox();
	}


	@Override
	public void setCount(int count) {

		tbcount.setValue(count+"");
	
	}

	@Override
	public void createScreen() {


		initalizeWidget();
	
		this.processlevelBarNames = new String[] { "New" ,"Copy Certificate" , "Invoice" };/** date 25.4.2018 added by komla to create invoice from fi=umigation**/
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		
		FormField fgroupingCustomerInformation=fbuilder.setlabel("CUSTOMER INFORMATION").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(8).build();
		fbuilder = new FormFieldBuilder("",personInfoComposite);
		FormField fpersonInfoCompositeCustomer= fbuilder.setRowSpan(0).setColSpan(8).build();	
		
		fbuilder = new FormFieldBuilder();
		FormField information=fbuilder.setlabel("REFERENCE INFORMATION").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(8).build();
		
		fbuilder = new FormFieldBuilder("Contract ID",tbcontractId);
		FormField contrctid= fbuilder.setColSpan(2).build();
		fbuilder = new FormFieldBuilder("Service ID",tbserviceId);
		FormField serviceid= fbuilder.setColSpan(2).build();
		fbuilder = new FormFieldBuilder("Contract Start Date",dbcontractstartdate);
		FormField contractstart= fbuilder.setColSpan(2).build();
		fbuilder = new FormFieldBuilder("Contract End Date",dbcontractenddate);
		FormField contractend= fbuilder.setColSpan(2).build();
		fbuilder = new FormFieldBuilder("Service Date",dbservicedate);
		FormField servicedate= fbuilder.setColSpan(2).build();

		
		FormField fgroupingfumigationInformation=fbuilder.setlabel("* FUMIGATION DETAILS").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(8).build();
		fbuilder = new FormFieldBuilder("",fumigationId);
		FormField ffumigationId= fbuilder.setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",tbcount);
		FormField ftbfumigationcount= fbuilder.setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", serialNumber);
		FormField fserialNumber= fbuilder.setRowSpan(0).setColSpan(1).build();

		fbuilder = new FormFieldBuilder("", tbSerialNumber);
		FormField ftbSerialNumber= fbuilder.setMandatory(false).setMandatoryMsg("Serial No. is Mandatory").setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", dateOfIssue);
		FormField fdateOfIssue= fbuilder.setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",dbdateofissue);
		FormField fdbdateissue= fbuilder.setMandatory(true).setMandatoryMsg("Date of Issue is Mandatory").setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", ImportExport);
		FormField fImportExport= fbuilder.setRowSpan(0).setColSpan(1).build();

		fbuilder = new FormFieldBuilder("", olbImportOrExport);
		FormField folbImportOrExport= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", pqrsRegdNumber);
		FormField fpqrsRegdNumber = fbuilder.setColSpan(1).build();
	
		fbuilder = new FormFieldBuilder("", tbDtePPQSRegistrationNumber);
		FormField ftbDtePPQSRegistrationNumber= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",branch);
		FormField fbranch = fbuilder.setColSpan(1).build();
	
		fbuilder = new FormFieldBuilder("",olbBranch);
		FormField folbBranch = fbuilder.setMandatory(true).setMandatoryMsg("Branch is Mandatory").setRowSpan(0).setColSpan(1).build();
			
		fbuilder = new FormFieldBuilder("",certificateNumber);
		FormField fcertificateNumber = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
	
		fbuilder = new FormFieldBuilder("",tbCertificateNo);
		FormField ftbCertificateNo= fbuilder.setMandatory(false).setMandatoryMsg("Certificate No. is Mandatory").setRowSpan(0).setColSpan(1).build();
	
		fbuilder = new FormFieldBuilder();
		FormField fempty= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fempty4= fbuilder.setRowSpan(1).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder();		
		FormField fgroupingGOODSformation=fbuilder.setlabel("DETAILS OF GOOD").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(8).build();
		fbuilder = new FormFieldBuilder("", descriptionOfGoods);
		FormField fdescofgoods= fbuilder.setRowSpan(1).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",taDescrioptionOfGoods);
		FormField ftaDescrioptionOfGoods= fbuilder.setMandatory(true).setMandatoryMsg("Description is Mandatory").setRowSpan(1).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("", commodity);
		FormField fcommodity= fbuilder.setRowSpan(1).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",tbNameOfCommodity);
		FormField ftbNameOfCommodity= fbuilder.setRowSpan(1).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", qty);
		FormField fqty= fbuilder.setRowSpan(1).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("", ibQuantity);
		FormField fibQuantity= fbuilder.setRowSpan(1).setColSpan(1).build();
		
		FormField fgroupingdetailsOfTreatment=fbuilder.setlabel("DETAILS OF TREATMENT").widgetType(FieldType.HALFGROUPING).setRowSpan(1).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",new Label("* Quantity Declared"));
		FormField fqtydeclare= fbuilder.setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",tbibQuantitydeclared);
		FormField ftbqtydeclare= fbuilder.setMandatory(true).setMandatoryMsg("Quantity Declared is Mandatory").setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",new Label("* Distinguishing Marks"));
		FormField fdistmark= fbuilder.setRowSpan(0).setColSpan(1).build();
	
		fbuilder = new FormFieldBuilder("",tbdistinguishingmarks);
		FormField ftbdistmark= fbuilder.setMandatory(true).setMandatoryMsg("Distinguishing Marks is Mandatory").setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",new Label("* Consignment Link/Container No"));
		FormField fconsinglink= fbuilder.setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",tbconsignmentlink);
		FormField ftbconsinglink= fbuilder.setMandatory(true).setMandatoryMsg("Consignment Link is Mandatory").setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("",new Label("* Port & Country of Loading"));
		FormField fcountryloading= fbuilder.setRowSpan(0).setColSpan(1).build();

		fbuilder = new FormFieldBuilder("",tbPortOfLoading);
		FormField folbcountryloading= fbuilder.setMandatory(true).setMandatoryMsg("Port & Country of Loading is Mandatory").setRowSpan(0).setColSpan(1).build();

		fbuilder = new FormFieldBuilder("",new Label("Country of Exp."));
		FormField fimportcountry= fbuilder.setRowSpan(0).setColSpan(1).build();

		fbuilder = new FormFieldBuilder("",olbimportcountry);
		FormField folbimportcountry= fbuilder.setRowSpan(0).setColSpan(1).build();

		fbuilder = new FormFieldBuilder("", new Label("* Name of Vessel/Ship"));
		FormField fnameofvessel= fbuilder.setRowSpan(0).setColSpan(1).build();

		fbuilder = new FormFieldBuilder("",tbnameofvessel);
		FormField ftbnameofvessel= fbuilder.setMandatory(true).setMandatoryMsg("Name of Vessel/Ship is Mandatory").setRowSpan(0).setColSpan(3).build();

		fbuilder = new FormFieldBuilder("",new Label("* Country of Destination"));
		FormField fcountrydesct= fbuilder.setRowSpan(0).setColSpan(1).build();

		fbuilder = new FormFieldBuilder("",olbcountrydestinatoin);
		FormField folbcountrydesct= fbuilder.setMandatory(true).setMandatoryMsg("Country of Destination is Mandatory").setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("",new Label("* Declare Point of Entry"));
		FormField fdeclareentry= fbuilder.setRowSpan(0).setColSpan(1).build();

		fbuilder = new FormFieldBuilder("",tbPortOfEntry);
		FormField folbdeclareentry= fbuilder.setMandatory(true).setMandatoryMsg("Declare Point of Entry is Mandatory").setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingfromformation=fbuilder.setlabel("NAME AND CONSIGNER ADDRESS").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(8).build();
		
		fbuilder = new FormFieldBuilder("* Company Name",tbconsignername);
		FormField formcompanyname= fbuilder.setMandatory(true).setMandatoryMsg("Company Name is Mandatory").setColSpan(4).build();
		
//		fbuilder = new FormFieldBuilder("",fromacshippingcomposite);
//		FormField fromaddress= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(8).build();
		/**
		 * Add by Jayshree
		 */
		
		fbuilder = new FormFieldBuilder("Consigner Address",address);
		FormField lbaddress= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(8).build();
		
		fbuilder = new FormFieldBuilder("",fromacshipping);
		FormField fromaddress= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(8).build();
		
		fbuilder = new FormFieldBuilder();
		FormField fgroupingtoformation=fbuilder.setlabel("NAME AND CONSIGNEE ADDRESS").widgetType(FieldType.Grouping).setRowSpan(0).setColSpan(8).build();

		fbuilder = new FormFieldBuilder("* Company Name",tbconsignee);
		FormField tocompanyname= fbuilder.setMandatory(true).setMandatoryMsg("Company Name is Mandatory").setColSpan(4).build();
		
//		fbuilder = new FormFieldBuilder(" ",toacshippingcomposite);
//		FormField toaddress= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(8).build();

		/**
		 * Add By Jayshree
		 */
		
		fbuilder = new FormFieldBuilder("Consignee Address",address1);
		FormField lbaddress1= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(8).build();
		
		
		fbuilder = new FormFieldBuilder(" ",toacshipping);
		FormField toaddress= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(8).build();

		
		fbuilder = new FormFieldBuilder("",personInfoCONSIGNEEComposite);
		FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder(" ",addbyCONSIGNEEComposite);
		FormField faddbyComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		
		fbuilder = new FormFieldBuilder("",personInfoCONSIGNERComposite);
		FormField fpersonInfoCONSIGNERComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(6).build();
		
		fbuilder = new FormFieldBuilder(" ",addbyCONSIGNERComposite);
		FormField faddbyCONSIGNERComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder(" ",newFromCustomer);
		FormField fnewformCustomer= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder(" ",newToCustomer);
		FormField fnewtoCustomer= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();

		fbuilder = new FormFieldBuilder();
		FormField ADDINformation=fbuilder.setlabel("* ADDITIONAL DECLARATION").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(8).build();
		
		fbuilder = new FormFieldBuilder("Comments",tacomment);
		FormField commetfumigation= fbuilder.setMandatory(false).setMandatoryMsg("Comment is Mandatory").setColSpan(8).build();

		fbuilder = new FormFieldBuilder("",new Label("* Name of Fumigation"));
		FormField fumigationname= fbuilder.setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",lbnamefumigation);
		FormField flbfumigationname= fbuilder.setMandatory(true).setMandatoryMsg("Name of Fumigation is Mandatory").setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",new Label("* Date Of Fumigation"));
		FormField fumigationdate= fbuilder.setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",dbdatefumigation);
		FormField fdbfumigationdate= fbuilder.setMandatory(true).setMandatoryMsg("Date of Fumigation is Mandatory").setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",new Label("* Place of Fumigation"));
		FormField fumigationplace= fbuilder.setMandatory(true).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",tbPlaceOfFumigation);
		FormField folbfumigationplace= fbuilder.setMandatory(true).setMandatoryMsg("Place of Fumigation is Mandatory").setColSpan(2).build();
		
//		fbuilder = new FormFieldBuilder("Unit for ibQuantity",olbdoibQuantitydeclareUnit);
//		FormField folbdoibQuantitydeclareUnit= fbuilder.setMandatory(true).setMandatoryMsg("Place of Fumigation is Mandatory").setColSpan(0).build();
//		
		fbuilder = new FormFieldBuilder("",new Label("* Dosage Rate of Fumigation"));
		FormField dosefumigation= fbuilder.setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("", tbDosageOfFumigation);
		FormField folbdosefumigation= fbuilder.setMandatory(true).setMandatoryMsg("Dosage Rate of Fumigation is Mandatory").setColSpan(2).build(); 		

		fbuilder = new FormFieldBuilder("",new Label("* Duration of Fumigation(in Days/hrs)"));
		FormField durationfumigation= fbuilder.setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",tbbdurationoffumigation);
		FormField folbdurationfumigation= fbuilder.setMandatory(true).setMandatoryMsg("Duration of Mandatory").setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("",new Label("* Min. Air Temp.(in "+"\u00b0"+"C)"));
		FormField tempfumigation= fbuilder.setColSpan(2).build();

		fbuilder = new FormFieldBuilder("",olbminairtemp);
		FormField folbtempfumigation= fbuilder.setMandatory(true).setMandatoryMsg("Min. Air Temp. is Mandatory").setColSpan(2).build();
//		fbuilder = new FormFieldBuilder("Certificate No.",tbCertificateNo);
//		FormField tbCertificateNo= fbuilder.setMandatory(true).setMandatoryMsg("Certificate No. is Mandatory").setRowSpan(0).setColSpan(0).build();
//
		fbuilder = new FormFieldBuilder("Invoice No.",tbInvoiceNo);
		FormField tbInvoiceNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();

		fbuilder = new FormFieldBuilder("Invoice Date",invoiceDate);
		FormField finvoiceDate= fbuilder.setMandatory(false).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("Shipping Bill Date",shippingBillDate);
		FormField fshippingBillDate= fbuilder.setMandatory(false).setColSpan(2).build();
	
		fbuilder = new FormFieldBuilder("Shipping Bill No.",tbShippingBillNo);
		FormField ftbShippingBillNo= fbuilder.setMandatory(false).setColSpan(2).build();
//
//		fbuilder = new FormFieldBuilder("Port & Country of Loading",olbportcountyloading);
//		FormField countryloading= fbuilder.setMandatory(true).setMandatoryMsg("Port & Country of Loading is Mandatory").setRowSpan(0).setColSpan(0).build();
//		
//		
//		
//		
//		
//		fbuilder = new FormFieldBuilder();
//		FormField fgroupingTRITMENTformation=fbuilder.setlabel("* DETAILS OF TREATMENT").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(4).build();
//		
//		
//		
		fbuilder = new FormFieldBuilder(" ",note1);
		FormField labelnote1= fbuilder.setColSpan(3).build();
		
//		fbuilder = new FormFieldBuilder(" ",fumigationPerformed);
//		FormField lbnote123= fbuilder.setColSpan(0).build();
//		
		fbuilder = new FormFieldBuilder(" ",fumigationPerformed);//Add By Jayshree
		FormField lbfumigationPerformed= fbuilder.setColSpan(0).build();
		
		
		
		fbuilder = new FormFieldBuilder(" ",note2);
		FormField labelnote2= fbuilder.setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder(" ",lbnote1);
		FormField lbnote1= fbuilder.setColSpan(0).build();
		
		
		
		fbuilder = new FormFieldBuilder(" ",note3);
		FormField labelnote3= fbuilder.setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder(" ",lbnote2);
		FormField lbnote2= fbuilder.setColSpan(0).build();
		
		
		
		fbuilder = new FormFieldBuilder(" ",note4);
		FormField labelnote4= fbuilder.setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder(" ",lbnote3);
		FormField lbnote3= fbuilder.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",ventilated);//Add By Jayshree
		FormField lbventilated= fbuilder.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",note5);
		FormField labelnote5= fbuilder.setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder(" ",lbnote4);
		FormField lbnote4= fbuilder.setColSpan(0).build();
//		
//		
//
		fbuilder = new FormFieldBuilder();
		FormField wrapperformation=fbuilder.setlabel("WRAPPING AND TIMBER").widgetType(FieldType.HALFGROUPING).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder(" ",note11);
		FormField labelnote11= fbuilder.setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder(" ",lbnote11);
		FormField lbnote11= fbuilder.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",note12);
		FormField labelnote12= fbuilder.setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder(" ",lbnote12);
		FormField lbnote12= fbuilder.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",note13);
		FormField labelnote13= fbuilder.setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder(" ",lbnote13);
		FormField lbnote13= fbuilder.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",note14);
		FormField labelnote14= fbuilder.setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder(" ",lbnote14);
		FormField lbnote14= fbuilder.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder(" ",note15);
		FormField labelnote15= fbuilder.setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder(" ",lbnote15);
		FormField lbnote15= fbuilder.setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Select Declaration Message ",declarationMessageList);
	    FormField fdeclarationMessageList= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
	    fbuilder = new FormFieldBuilder(" ",addDeclarationButton);
		FormField faddDeclarationButton= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("* Comments",moreComment);
		FormField moreComment= fbuilder.setMandatory(true).setMandatoryMsg("Comment is Mandatory").setColSpan(8).build();
		
		fbuilder = new FormFieldBuilder(" ",editconsignee);
		FormField feditconsignee= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
			
		fbuilder = new FormFieldBuilder(" ",editconsigner);
		FormField feditconsigner= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();

		
		fbuilder = new FormFieldBuilder(" ",selectTemplateButton);
		FormField fselectTemplateButton= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		/** date 24.5.2018 added by komal **/
		fbuilder = new FormFieldBuilder("* Number Range",olbNumberRange);
		FormField folbNumberRange= fbuilder.setMandatory(true).setMandatoryMsg("Number Range is Mandatory").setRowSpan(0).setColSpan(1).build();
		
		FormField[][] formfield = {
				{fgroupingCustomerInformation},
				{fpersonInfoCompositeCustomer},
				
				{information},{contrctid,serviceid,contractstart,contractend},
				{servicedate , 	/** date 24.5.2018 added by komal **/folbNumberRange},
				{fgroupingfumigationInformation},
				{ffumigationId , ftbfumigationcount ,fserialNumber , ftbSerialNumber , fdateOfIssue ,fdbdateissue , fImportExport ,folbImportOrExport},
				{fbranch , folbBranch,fpqrsRegdNumber ,ftbDtePPQSRegistrationNumber ,fcertificateNumber , ftbCertificateNo ,fempty,fselectTemplateButton},
				
				{fgroupingGOODSformation},
				{fdescofgoods , ftaDescrioptionOfGoods , fcommodity , ftbNameOfCommodity , fqty , fibQuantity},
				//{fempty4 ,fgroupingdetailsOfTreatment},
				{fqtydeclare , ftbqtydeclare , fumigationname , flbfumigationname },
				{fempty4 , fumigationplace , folbfumigationplace },
				{fdistmark ,ftbdistmark , dosefumigation , folbdosefumigation },
				{fconsinglink , ftbconsinglink , fumigationdate , fdbfumigationdate },
				{fempty4 ,durationfumigation , folbdurationfumigation},
				{fcountryloading , folbcountryloading , fimportcountry , folbimportcountry ,tempfumigation , folbtempfumigation},
				{fnameofvessel , ftbnameofvessel ,labelnote1, lbfumigationPerformed},//lbnote123
				{fempty4 , labelnote2,lbnote1},
				
				{fcountrydesct , folbcountrydesct , fdeclareentry , folbdeclareentry ,labelnote3,lbnote2},
				{fempty4 , labelnote4,lbnote3  },
				{fempty4,lbventilated, labelnote5,lbnote4 },
				{fempty4,wrapperformation },
				{fempty4 , labelnote11,lbnote11 },
				{fempty4 ,	labelnote12,lbnote12},
			//	{fempty4,faddbyComposite},
				{fempty4,labelnote13,lbnote13},
				{fempty4 ,labelnote14,lbnote14},
				{fempty4 ,labelnote15,lbnote15},
				{fgroupingfromformation},
				{fpersonInfoCONSIGNERComposite,faddbyCONSIGNERComposite},
				{formcompanyname,fnewformCustomer/**, feditconsigner**/},
				{lbaddress},
				{fromaddress},
				{fgroupingtoformation},
				{fpersonInfoComposite,faddbyComposite},
				{tocompanyname,fnewtoCustomer/**, feditconsignee**/},
				{lbaddress1},
				{toaddress},

				{ADDINformation },
				{tbInvoiceNo,finvoiceDate,ftbShippingBillNo,fshippingBillDate},
				{commetfumigation},
				{fdeclarationMessageList,faddDeclarationButton},
				{moreComment}

				
				
				
				
//				{descofgoods,qtydeclare,folbdoibQuantitydeclareUnit,distmark},
//				{consinglink,countryloading,nameofvessel,declareentry},
//				{countrydesct,importcountry,fpacking,fvolumeOfContainer},
//				{ftotalNetWeight,fgrossWeight},
//				{fgroupingfromformation},
//				{fpersonInfoCONSIGNERComposite,faddbyCONSIGNERComposite},
//				{formcompanyname,fnewformCustomer,feditconsigner},
//				{fromaddress},
//				{fgroupingtoformation},
//				{fpersonInfoComposite,faddbyComposite},
//				{tocompanyname,fnewtoCustomer,feditconsignee},
//				{toaddress},
//				{fgroupingTRITMENTformation},
//				{fumigationname,fumigationdate,fumigationplace,dosefumigation},
//				{durationfumigation,tempfumigation},{labelnote1,lbnote123},{labelnote2,lbnote1},{labelnote3,lbnote2},
//				{labelnote4,lbnote3},{labelnote5,lbnote4},
//				{wrapperformation},{labelnote11,lbnote11},{labelnote12,lbnote12},{labelnote13,lbnote13},
//				{labelnote14,lbnote14},{labelnote15,lbnote15},
//				{ADDINformation},
//				{tbInvoiceNo,finvoiceDate,ftbShippingBillNo,fshippingBillDate},
//				{commetfumigation},
//				{fdeclarationMessageList,faddDeclarationButton},
//				{moreComment}
		};
		
		this.fields=formfield;
		
		
	}

	@Override
	public void updateModel(Fumigation model) {

		
		if(invoiceDate.getValue()!=null)
			model.setInvoiceDate(invoiceDate.getValue());
		
		if(shippingBillDate.getValue()!=null)
			model.setShippingBillDate(shippingBillDate.getValue());
		
			if(personInfoComposite.getValue()!=null)
				model.setcInfo(personInfoComposite.getValue());
		
		if(tbShippingBillNo.getValue()!=null)
			model.setShippingBillNo(tbShippingBillNo.getValue());
		
		if(tbtreatmentcardno.getValue()!=null)
			model.setTreatmentcardno(tbtreatmentcardno.getValue());
		if(dbdateofissue.getValue()!=null)
			model.setDateofissue(dbdateofissue.getValue());
		if(olbimportcountry.getValue()!=null)
			model.setImportcountry(olbimportcountry.getValue());
		if(taDescrioptionOfGoods.getValue()!=null)
			model.setDescriptionofgoods(taDescrioptionOfGoods.getValue());
		if(tbibQuantitydeclared.getValue()!=null)
			model.setQuantitydeclare(tbibQuantitydeclared.getValue());
		if(tbdistinguishingmarks.getValue()!=null)
			model.setDistiinguishingmarks(tbdistinguishingmarks.getValue());
		if(tbconsignmentlink.getValue()!=null)
			model.setConsignmentlink(tbconsignmentlink.getValue());

		
		
		
		if(tbInvoiceNo.getValue()!=null)
			model.setInvoiceNo(tbInvoiceNo.getValue());
		
		if(tbCertificateNo.getValue()!=null)
			model.setCertificateNo(tbCertificateNo.getValue());
		
		if(tbnameofvessel.getValue()!=null)
			model.setNameofvessel(tbnameofvessel.getValue());
		if(olbcountrydestinatoin.getValue()!=null)
			model.setCountryofdestination(olbcountrydestinatoin.getValue());
		if (tbPortOfEntry.getValue() != null) {
			model.setDeclarepointentry(tbPortOfEntry.getValue());
		}
		if(tbconsignername.getValue()!=null)
			model.setFromCompanyname(tbconsignername.getValue());
//		if (fromacshippingcomposite.getValue() != null) {
//			model.setFromaddress(fromacshippingcomposite.getValue());
//			}
		
		if (fromacshipping.getValue() != null) {
			model.setFromAddess(fromacshipping.getValue());
			}
		
		if(tbconsignee.getValue()!=null)
			model.setToCompanyname(tbconsignee.getValue());
//		if (toacshippingcomposite.getValue() != null) {
//			model.setTomaddress(toacshippingcomposite.getValue());
//		}

		if (toacshipping.getValue() != null) {
			model.setToAddress(toacshipping.getValue());
			}
		
			model.setNameoffumigation(lbnamefumigation.getItemText(lbnamefumigation.getSelectedIndex()));
			Console.log("date of fumigation before saving........"+dbdatefumigation.getValue());
		if(dbdatefumigation.getValue()!=null)
			model.setDateoffumigation(dbdatefumigation.getValue());
		
		Console.log("date of fumigation after saving in update model ........"+model.getDateoffumigation());

		
		
		
//		if(olbdoquantitydeclareUnit.getSelectedIndex()!=0)
//			model.setQuantitydeclareUnit(olbdoquantitydeclareUnit.getValue(olbdoquantitydeclareUnit.getSelectedIndex()));
//		
		
		if(tbPortOfLoading.getValue() != null)
			model.setPortncountryloading(tbPortOfLoading.getValue());
		
		if(tbPlaceOfFumigation.getValue() != null)
			model.setPlaceoffumigation(tbPlaceOfFumigation.getValue());
		if(tbDosageOfFumigation.getValue()!= null)
			model.setDoseratefumigation(tbDosageOfFumigation.getValue());
//		if(olbdurationoffumigation.getSelectedIndex()!=0)//comment by jayshree
//			model.setDurartionfumigation(olbdurationoffumigation.getValue(olbdurationoffumigation.getSelectedIndex()));
		
		if(tbbdurationoffumigation.getValue()!=null)//comment by jayshree
		model.setDuration(tbbdurationoffumigation.getValue());
	
		/**
		 * Updated By:Viraj 
		 * Date:12-01-2019 
		 * Description: Changed to textBox
		 */	
		if(olbminairtemp.getValue() != null)
			model.setMinairtemp(olbminairtemp.getValue());
		/** Ends **/
		
			model.setNote123(lbnote123.getItemText(lbnote123.getSelectedIndex()));
			model.setNote1(lbnote1.getItemText(lbnote1.getSelectedIndex()));
			model.setNote2(lbnote2.getItemText(lbnote2.getSelectedIndex()));
			model.setNote3(lbnote3.getItemText(lbnote3.getSelectedIndex()));
			model.setNote4(lbnote4.getItemText(lbnote4.getSelectedIndex()));
			model.setNote11(lbnote11.getItemText(lbnote11.getSelectedIndex()));
			model.setNote12(lbnote12.getItemText(lbnote12.getSelectedIndex()));
			model.setNote13(lbnote13.getItemText(lbnote13.getSelectedIndex()));
			model.setNote14(lbnote14.getItemText(lbnote14.getSelectedIndex()));
			model.setNote15(lbnote15.getItemText(lbnote15.getSelectedIndex()));
		
		
		Console.log("note1 update ==== "+lbnote1.getItemText(lbnote1.getSelectedIndex()));
		
		
		if(tacomment.getValue()!=null)
			model.setAdditionaldeclaration(tacomment.getValue());
		if(dbdateadd.getValue()!=null)
			model.setPdate(dbdateadd.getValue());
		if(tbplace.getValue()!=null)
			model.setPlace(tbplace.getValue());
		
		if(!tbcontractId.getValue().equals("")){
			model.setContractID(Integer.parseInt(tbcontractId.getValue()));
		}
			
		if(!tbserviceId.getValue().equals("")){
			model.setServiceID(Integer.parseInt(tbserviceId.getValue()));
		}
		
			
		if(dbcontractenddate.getValue()!=null)
			model.setContractEnddate(dbcontractenddate.getValue());
		if(dbcontractstartdate.getValue()!=null)
			model.setContractStartdate(dbcontractstartdate.getValue());
		if(dbservicedate.getValue()!=null)
			model.setServiceDate(dbservicedate.getValue());

		
		if(moreComment.getValue()!=null)
			model.setMoredecl(moreComment.getValue());
		
		
		if(consignerName!=null){
			model.setConsignerName(consignerName);
		}
		if(consignerPocName!=null){
			model.setConsignerPocName(consignerPocName);
		}
		if(consignerCellNo!=null){
			model.setConsignerCellNo(consignerCellNo);
		}
		if(consignerEmail!=null){
			model.setConsignerEmail(consignerEmail);
		}
		
		
		if(consigneeName!=null){
			model.setConsigneeName(consigneeName);
		}
		if(consigneePocName!=null){
			model.setConsigneePocName(consigneePocName);
		}
		if(consigneeCellNo!=null){
			model.setConsigneeCellNo(consigneeCellNo);
		}
		if(consigneeEmail!=null){
			model.setConsigneeEmail(consigneeEmail);
		}
		
//		if(totalNetWeight.getValue()!=null)
//			model.setTotalNetWeight(totalNetWeight.getValue());
//		
//		
//		if(grossWeight.getValue()!=null)
//			model.setGrossWeight(grossWeight.getValue());
//		
//		if(packing.getValue()!=null)
//			model.setPackingValue(packing.getValue());
//		if(volumeOfContainer.getValue()!=null)
//			model.setVolumeofcontainer(volumeOfContainer.getValue());
//		
		
			model.setConsigneeEditFlag(consigneeEditFlag);
			model.setConsignereditFlag(consignereditFlag);
		//komal
			if(tbSerialNumber.getValue()!=null){
				model.setSerialNumber(tbSerialNumber.getValue());
			}
			if(tbDtePPQSRegistrationNumber.getValue()!=null){
				model.setDtePPQSRegistrationNumber(tbDtePPQSRegistrationNumber.getValue());
			}
			if(olbImportOrExport.getSelectedIndex()!=0){
				model.setImportcountry(olbImportOrExport.getItemText(olbImportOrExport.getSelectedIndex()));
			}
			if(olbBranch.getSelectedIndex()!=0){
				model.setBranch(olbBranch.getValue());
			}
			if(tbNameOfCommodity.getValue()!=null){
				model.setNameOfCommodity(tbNameOfCommodity.getValue());
			}
			if(ibQuantity.getValue()!=null){
				model.setQuantity(ibQuantity.getValue());
			}
			/** date 24.5.2018 added by komal**/
			if(olbNumberRange.getSelectedIndex()!=0){
				model.setNumberRange(olbNumberRange.getValue(olbNumberRange.getSelectedIndex()));
			}
			
			
			/**
			 * Date 27-7-2018 by jayshree 
			 */
			
			if(fumigationPerformed.getValue()!=null){
				model.setFumigationPerformed(fumigationPerformed.getValue());
			}
			
			if(ventilated.getValue()!=null){
				model.setVentilated(ventilated.getValue());
			}
			
			
		fumigationObj=model;	
		presenter.setModel(model);

	}

	@Override
	public void updateView(Fumigation view) {

		
		fumigationObj=view;
		
		System.out.println("rohan in new state count"+view.getCount());
		
		this.tbcount.setValue(view.getCount()+"");
		
		if(view.getcInfo()!=null)
			personInfoComposite.setValue(view.getcInfo());
		
		if(view.getInvoiceDate()!=null)
			invoiceDate.setValue(view.getInvoiceDate());
		
		if(view.getShippingBillDate()!=null)
			shippingBillDate.setValue(view.getShippingBillDate());
		
		if(view.getShippingBillNo()!=null)
			tbShippingBillNo.setValue(view.getShippingBillNo());
		
		if(view.getMoredecl()!=null)
			moreComment.setValue(view.getMoredecl());
		if(view.getTreatmentcardno()!=null)
			tbtreatmentcardno.setValue(view.getTreatmentcardno());
		if(view.getDateofissue()!=null)
			dbdateofissue.setValue(view.getDateofissue());
		if(view.getImportcountry()!=null)
			olbimportcountry.setValue(view.getImportcountry());
		if(view.getDescriptionofgoods()!=null)
			taDescrioptionOfGoods.setValue(view.getDescriptionofgoods());
		if(view.getQuantitydeclare()!=null)
			tbibQuantitydeclared.setValue(view.getQuantitydeclare());
		if(view.getDistiinguishingmarks()!=null)
			tbdistinguishingmarks.setValue(view.getDistiinguishingmarks());
		if(view.getConsignmentlink()!=null)
			tbconsignmentlink.setValue(view.getConsignmentlink());
		if(view.getInvoiceNo()!=null)
			tbInvoiceNo.setValue(view.getInvoiceNo());
		
		if(view.getCertificateNo()!=null)
			tbCertificateNo.setValue(view.getCertificateNo());
		
		if(view.getPortncountryloading()!=null)
			tbPortOfLoading.setValue(view.getPortncountryloading());
		if(view.getNameofvessel()!=null)
			tbnameofvessel.setValue(view.getNameofvessel());
		
		if(view.getQuantitydeclareUnit()!=null)
			olbdoquantitydeclareUnit.setValue(view.getQuantitydeclareUnit());
		
		if(view.getCountryofdestination()!=null)
			olbcountrydestinatoin.setValue(view.getCountryofdestination());
		
		if(view.getDeclarepointentry()!=null)
			tbPortOfEntry.setValue(view.getDeclarepointentry());
		if(view.getFromCompanyname()!=null)
			tbconsignername.setValue(view.getFromCompanyname());
//		if (view.getFromaddress() != null)
//			fromacshippingcomposite.setValue(view.getFromaddress());
		
		/**
		 * Date 26-7-2018 
		 */
		if (view.getFromAddess() != null)
			fromacshipping.setValue(view.getFromAddess());
		
		if(view.getToCompanyname()!=null)
			tbconsignee.setValue(view.getToCompanyname());
//		if (view.getTomaddress() != null)
//			toacshippingcomposite.setValue(view.getTomaddress());
		
		if (view.getToAddress() != null)
			toacshipping.setValue(view.getToAddress());
		
		
		if(view.getNameoffumigation()!=null)
		{
			for(int i=0;i<lbnamefumigation.getItemCount();i++)
			{
				String data=lbnamefumigation.getItemText(i);
				if(data.equals(view.getNameoffumigation()))
				{
					lbnamefumigation.setSelectedIndex(i);
					break;
				}
			}
		}

		
		
		
		Console.log("date of fumigation after saving in update view ........"+view.getDateoffumigation());
		
		if (view.getDateoffumigation() != null)
			dbdatefumigation.setValue(view.getDateoffumigation());
		if (view.getPlaceoffumigation() != null)
			tbPlaceOfFumigation.setValue(view.getPlaceoffumigation());
		if (view.getDoseratefumigation() != null)
			tbDosageOfFumigation.setValue(view.getDoseratefumigation());
		if (view.getDuration() != null)
			tbbdurationoffumigation.setValue(view.getDuration());
		if (view.getMinairtemp() != null)
			olbminairtemp.setValue(view.getMinairtemp());
		if (view.getAdditionaldeclaration() != null)
			tacomment.setValue(view.getAdditionaldeclaration());
		
		
		if(view.getNote123()!=null)
			lbnote123.setTitle(view.getNote123());
		if(view.getNote1()!=null)
			lbnote1.setTitle(view.getNote1());
		if(view.getNote2()!=null)
			lbnote2.setTitle(view.getNote2());
		if(view.getNote3()!=null)
			lbnote3.setTitle(view.getNote3());
		if(view.getNote4()!=null)
			lbnote4.setTitle(view.getNote4());
		if(view.getNote11()!=null)
			lbnote11.setTitle(view.getNote11());
		if(view.getNote12()!=null)
			lbnote12.setTitle(view.getNote12());
		if(view.getNote13()!=null)
			lbnote13.setTitle(view.getNote13());
		if(view.getNote14()!=null)
			lbnote14.setTitle(view.getNote14());
		if(view.getNote15()!=null)
			lbnote15.setTitle(view.getNote15());
		
		if(view.getNote123()!=null)
		{
			for(int i=0;i<lbnote123.getItemCount();i++)
			{
				String data=lbnote123.getItemText(i);
				if(data.equals(view.getNote123()))
				{
					lbnote123.setSelectedIndex(i);
					break;
				}
			}
		}
		
		
		if(view.getNote1()!=null)
		{
			for(int i=0;i<lbnote1.getItemCount();i++)
			{
				String data=lbnote1.getItemText(i);
				if(data.equals(view.getNote1()))
				{
					lbnote1.setSelectedIndex(i);
					break;
				}
			}
		}
		if(view.getNote2()!=null)
		{
			for(int i=0;i<lbnote2.getItemCount();i++)
			{
				String data=lbnote2.getItemText(i);
				if(data.equals(view.getNote2()))
				{
					lbnote2.setSelectedIndex(i);
					break;
				}
			}
		}
		if(view.getNote3()!=null)
		{
			for(int i=0;i<lbnote3.getItemCount();i++)
			{
				String data=lbnote3.getItemText(i);
				if(data.equals(view.getNote3()))
				{
					lbnote3.setSelectedIndex(i);
					break;
				}
			}
		}
		if(view.getNote4()!=null)
		{
			for(int i=0;i<lbnote4.getItemCount();i++)
			{
				String data=lbnote4.getItemText(i);
				if(data.equals(view.getNote4()))
				{
					lbnote4.setSelectedIndex(i);
					break;
				}
			}
		}
		if(view.getNote11()!=null)
		{
			for(int i=0;i<lbnote11.getItemCount();i++)
			{
				String data=lbnote11.getItemText(i);
				if(data.equals(view.getNote11()))
				{
					lbnote11.setSelectedIndex(i);
					break;
				}
			}
		}
		if(view.getNote12()!=null)
		{
			for(int i=0;i<lbnote12.getItemCount();i++)
			{
				String data=lbnote12.getItemText(i);
				if(data.equals(view.getNote12()))
				{
					lbnote12.setSelectedIndex(i);
					break;
				}
			}
		}
		if(view.getNote13()!=null)
		{
			for(int i=0;i<lbnote13.getItemCount();i++)
			{
				String data=lbnote13.getItemText(i);
				if(data.equals(view.getNote13()))
				{
					lbnote13.setSelectedIndex(i);
					break;
				}
			}
		}
		if(view.getNote14()!=null)
		{
			for(int i=0;i<lbnote14.getItemCount();i++)
			{
				String data=lbnote14.getItemText(i);
				if(data.equals(view.getNote14()))
				{
					lbnote14.setSelectedIndex(i);
					break;
				}
			}
		}
		if(view.getNote15()!=null)
		{
			for(int i=0;i<lbnote15.getItemCount();i++)
			{
				String data=lbnote15.getItemText(i);
				if(data.equals(view.getNote15()))
				{
					lbnote15.setSelectedIndex(i);
					break;
				}
			}
		}
		
		
		
		
		
		System.out.println("note1===="+view.getNote1());
		
		
		if (view.getPdate() != null)
			dbdateadd.setValue(view.getPdate());
		if (view.getPlace() != null)
			tbplace.setValue(view.getPlace());
		
		if (view.getContractID() != null)
			tbcontractId.setValue(view.getContractID()+"");
		if (view.getServiceID() != null)
			tbserviceId.setValue(view.getServiceID()+"");
		if (view.getContractStartdate() != null)
			dbcontractstartdate.setValue(view.getContractStartdate());
		if (view.getContractEnddate()!= null)
			dbcontractenddate.setValue(view.getContractEnddate());
		if (view.getServiceDate()!= null)
			dbservicedate.setValue(view.getServiceDate());
		
		
		if (view.getGrossWeight()!= null)
			grossWeight.setValue(view.getGrossWeight());
		
		if (view.getTotalNetWeight()!= null)
			totalNetWeight.setValue(view.getTotalNetWeight());
		
		if (view.getVolumeofcontainer()!= null)
			volumeOfContainer.setValue(view.getVolumeofcontainer());
		
		if (view.getPackingValue()!= null)
			packing.setValue(view.getPackingValue());
		
		//komal
		if(view.getSerialNumber()!=0){
			tbSerialNumber.setValue(view.getSerialNumber());
		}
		if(view.getDtePPQSRegistrationNumber()!=null){
			tbDtePPQSRegistrationNumber.setValue(view.getDtePPQSRegistrationNumber());
		}
		if(view.getImportOrExport()!=null){
			olbImportOrExport.setValue(view.getImportOrExport());
		}
		if(view.getBranch()!=null){
			olbBranch.setValue(view.getBranch());
		}
		if(view.getNameOfCommodity()!=null){
			tbNameOfCommodity.setValue(view.getNameOfCommodity());;
		}
		if(view.getQuantity()!=null){
			ibQuantity.setValue(view.getQuantity());
		}
		/**24.5.2018 added by komal**/
		if(view.getNumberRange()!=null){
			olbNumberRange.setValue(view.getNumberRange());
		}
	
		
		/**
		 * Date 27-7-2018 by jayshree
		 */
		if(view.getFumigationPerformed()!=null){
			fumigationPerformed.setValue(view.getFumigationPerformed());
		}
		
		if(view.getVentilated()!=null){
			ventilated.setValue(view.getVentilated());
		}
		
		
		presenter.setModel(view);
		
	}
	
	

	

	

	@Override
	public void setToEditState() {
		// TODO Auto-generated method stub
		super.setToEditState();
		this.personInfoComposite.setEnable(false);
		this.processLevelBar.setVisibleFalse(false);
	}

	@Override
	public void toggleAppHeaderBarMenu() {
		
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			//InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * Date 14 Apr 2018 if condtion added by komal for GENERAL POPUP menus 
			 * and old code added to else block
			 */
			
			System.out.println("presenter.isPopUpAppMenubar()==== NEW "+isPopUpAppMenubar());
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

			}
			
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			//InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * Date 14 Apr 2018 if condtion added by komal for GENERAL POPUP menus 
			 * and old code added to else block
			 */			
			System.out.println("presenter.isPopUpAppMenubar()==== EDIT "+isPopUpAppMenubar());
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			//InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			/**
			 * Date 14 Apr 2018 if condtion added by komal for GENERAL POPUP menus 
			 * and old code added to else block
			 */
			
			System.out.println("presenter.isPopUpAppMenubar()==== VIEW "+isPopUpAppMenubar());
			InlineLabel[] menus;
			if(isPopUpAppMenubar()){
				 menus=GeneralViewDocumentPopup.applevelMenu;
			}else{
				 menus=AppMemory.getAppMemory().skeleton.getMenuLabels();

			}
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Print")||text.equals("Edit")||text.equals("Email")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		
		AuthorizationHelper.setAsPerAuthorization(Screen.FUMIGATION,LoginPresenter.currentModule.trim());
	}

	@Override
	public void setEnable(boolean state) {
	
		super.setEnable(state);
		this.tbcount.setEnabled(false);
		this.tbcontractId.setEnabled(false);
		this.tbserviceId.setEnabled(false);
		this.dbcontractenddate.setEnabled(false);
		this.dbcontractstartdate.setEnabled(false);
		this.dbservicedate.setEnabled(false);
		tbconsignername.setEnabled(false);
		tbconsignee.setEnabled(false);
		/** date 24.5.2018 added by komal**/
		tbCertificateNo.setEnabled(false);
	}

	@Override
	public void setToViewState() {
		super.setToViewState();
		
		SuperModel model=new Fumigation();
		model=fumigationObj;
		AppUtility.addDocumentToHistoryTable(AppConstants.SERVICEMODULE,AppConstants.FUMIGATION, fumigationObj.getCount(), null,null,null, false, model, null);
	}
	public void toggleProcessLevelMenu()
	{
		Fumigation entity=(Fumigation) presenter.getModel();
		for(int i=0;i<getProcesslevelBarNames().length;i++)
		{
			InlineLabel label=getProcessLevelBar().btnLabels[i];
			String text=label.getText().trim();

			if(entity.getCount()!=0)
			{    
				if(text.equals("Copy Certificate")){
					label.setVisible(true);
				}else{
					label.setVisible(false);
				}
					

			}
		}
	}
	/****************************************************Getter and Setter**************************************************/

	
	
	public TextBox getTbtreatmentcardno() {
		return tbtreatmentcardno;
	}

	public TextBox getPacking() {
		return packing;
	}

	public void setPacking(TextBox packing) {
		this.packing = packing;
	}

	public TextBox getVolumeOfContainer() {
		return volumeOfContainer;
	}

	public void setVolumeOfContainer(TextBox volumeOfContainer) {
		this.volumeOfContainer = volumeOfContainer;
	}

	public TextBox getTotalNetWeight() {
		return totalNetWeight;
	}

	public void setTotalNetWeight(TextBox totalNetWeight) {
		this.totalNetWeight = totalNetWeight;
	}

	public void setTbtreatmentcardno(TextBox tbtreatmentcardno) {
		this.tbtreatmentcardno = tbtreatmentcardno;
	}

	public DateBox getDbdateofissue() {
		return dbdateofissue;
	}

	public void setDbdateofissue(DateBox dbdateofissue) {
		this.dbdateofissue = dbdateofissue;
	}

	public ObjectListBox<Country> getOlbimportcountry() {
		return olbimportcountry;
	}

	public void setOlbimportcountry(ObjectListBox<Country> olbimportcountry) {
		this.olbimportcountry = olbimportcountry;
	}

	public TextBox getTbdescriptionofgoods() {
		return tbdescriptionofgoods;
	}

	public void setTbdescriptionofgoods(TextBox tbdescriptionofgoods) {
		this.tbdescriptionofgoods = tbdescriptionofgoods;
	}

	public TextBox getTbibQuantitydeclared() {
		return tbibQuantitydeclared;
	}

	public void setTbibQuantitydeclared(TextBox tbibQuantitydeclared) {
		this.tbibQuantitydeclared = tbibQuantitydeclared;
	}

	public TextBox getTbdistinguishingmarks() {
		return tbdistinguishingmarks;
	}

	public void setTbdistinguishingmarks(TextBox tbdistinguishingmarks) {
		this.tbdistinguishingmarks = tbdistinguishingmarks;
	}

	public TextBox getTbconsignmentlink() {
		return tbconsignmentlink;
	}

	public void setTbconsignmentlink(TextBox tbconsignmentlink) {
		this.tbconsignmentlink = tbconsignmentlink;
	}

//	public TextBox getTbContainerno() {
//		return tbContainerno;
//	}
//
//	public void setTbContainerno(TextBox tbContainerno) {
//		this.tbContainerno = tbContainerno;
//	}


	public TextBox getTbnameofvessel() {
		return tbnameofvessel;
	}

	public void setTbnameofvessel(TextBox tbnameofvessel) {
		this.tbnameofvessel = tbnameofvessel;
	}

	public ObjectListBox<Country> getOlbcountrydestinatoin() {
		return olbcountrydestinatoin;
	}

	public void setOlbcountrydestinatoin(
			ObjectListBox<Country> olbcountrydestinatoin) {
		this.olbcountrydestinatoin = olbcountrydestinatoin;
	}


	public TextBox getTbconsignername() {
		return tbconsignername;
	}

	public void setTbconsignername(TextBox tbconsignername) {
		this.tbconsignername = tbconsignername;
	}

//	public AddressComposite getFromacshippingcomposite() {
//		return fromacshippingcomposite;
//	}
//
//	public void setFromacshippingcomposite(AddressComposite fromacshippingcomposite) {
//		this.fromacshippingcomposite = fromacshippingcomposite;
//	}

	public TextBox getTbconsignee() {
		return tbconsignee;
	}

	public void setTbconsignee(TextBox tbconsignee) {
		this.tbconsignee = tbconsignee;
	}

//	public AddressComposite getToacshippingcomposite() {
//		return toacshippingcomposite;
//	}
//
//	public void setToacshippingcomposite(AddressComposite toacshippingcomposite) {
//		this.toacshippingcomposite = toacshippingcomposite;
//	}

	public ListBox getLbnamefumigation() {
		return lbnamefumigation;
	}

	public void setLbnamefumigation(ListBox lbnamefumigation) {
		this.lbnamefumigation = lbnamefumigation;
	}

	public DateBox getDbdatefumigation() {
		return dbdatefumigation;
	}

	public void setDbdatefumigation(DateBox dbdatefumigation) {
		this.dbdatefumigation = dbdatefumigation;
	}


//	public ObjectListBox<Config> getOlbdurationoffumigation() {
//		return olbdurationoffumigation;
//	}

//	public void setOlbdurationoffumigation(
//			ObjectListBox<Config> olbdurationoffumigation) {
//		this.olbdurationoffumigation = olbdurationoffumigation;
//	}
	/**
	 * Updated By:Viraj 
	 * Date:12-01-2019 
	 * Description: Changed to textBox
	 */	
	public TextBox getOlbminairtemp() {
		return olbminairtemp;
	}

	public void setOlbminairtemp(TextBox olbminairtemp) {
		this.olbminairtemp = olbminairtemp;
	}
	/** Ends **/
	public TextArea getTacomment() {
		return tacomment;
	}

	public void setTacomment(TextArea tacomment) {
		this.tacomment = tacomment;
	}

	public TextBox getTbplace() {
		return tbplace;
	}

	public void setTbplace(TextBox tbplace) {
		this.tbplace = tbplace;
	}

	public DateBox getDbdateadd() {
		return dbdateadd;
	}

	public void setDbdateadd(DateBox dbdateadd) {
		this.dbdateadd = dbdateadd;
	}

	public TextBox getTbcontractId() {
		return tbcontractId;
	}

	public void setTbcontractId(TextBox tbcontractId) {
		this.tbcontractId = tbcontractId;
	}

	public TextBox getTbserviceId() {
		return tbserviceId;
	}

	public void setTbserviceId(TextBox tbserviceId) {
		this.tbserviceId = tbserviceId;
	}

	public DateBox getDbcontractstartdate() {
		return dbcontractstartdate;
	}

	public void setDbcontractstartdate(DateBox dbcontractstartdate) {
		this.dbcontractstartdate = dbcontractstartdate;
	}

	public DateBox getDbcontractenddate() {
		return dbcontractenddate;
	}

	public void setDbcontractenddate(DateBox dbcontractenddate) {
		this.dbcontractenddate = dbcontractenddate;
	}

	public DateBox getDbservicedate() {
		return dbservicedate;
	}

	public void setDbservicedate(DateBox dbservicedate) {
		this.dbservicedate = dbservicedate;
	}

	public TextBox getTbcount() {
		return tbcount;
	}

	public void setTbcount(TextBox tbcount) {
		this.tbcount = tbcount;
	}

	public Label getNote1() {
		return note1;
	}

	public void setNote1(Label note1) {
		this.note1 = note1;
	}

	public Label getNote2() {
		return note2;
	}

	public void setNote2(Label note2) {
		this.note2 = note2;
	}

	public Label getNote3() {
		return note3;
	}

	public void setNote3(Label note3) {
		this.note3 = note3;
	}

	public Label getNote4() {
		return note4;
	}

	public void setNote4(Label note4) {
		this.note4 = note4;
	}

	public Label getNote5() {
		return note5;
	}

	public void setNote5(Label note5) {
		this.note5 = note5;
	}

	public Label getNote11() {
		return note11;
	}

	public void setNote11(Label note11) {
		this.note11 = note11;
	}

	public Label getNote12() {
		return note12;
	}

	public void setNote12(Label note12) {
		this.note12 = note12;
	}

	public Label getNote13() {
		return note13;
	}

	public void setNote13(Label note13) {
		this.note13 = note13;
	}

	public Label getNote14() {
		return note14;
	}

	public void setNote14(Label note14) {
		this.note14 = note14;
	}

	public Label getNote15() {
		return note15;
	}

	public void setNote15(Label note15) {
		this.note15 = note15;
	}

	public ListBox getLbnote1() {
		return lbnote1;
	}

	public void setLbnote1(ListBox lbnote1) {
		this.lbnote1 = lbnote1;
	}

	public ListBox getLbnote2() {
		return lbnote2;
	}

	public void setLbnote2(ListBox lbnote2) {
		this.lbnote2 = lbnote2;
	}

	public ListBox getLbnote3() {
		return lbnote3;
	}

	public void setLbnote3(ListBox lbnote3) {
		this.lbnote3 = lbnote3;
	}

	public ListBox getLbnote4() {
		return lbnote4;
	}

	public void setLbnote4(ListBox lbnote4) {
		this.lbnote4 = lbnote4;
	}

	public ListBox getLbnote11() {
		return lbnote11;
	}

	public void setLbnote11(ListBox lbnote11) {
		this.lbnote11 = lbnote11;
	}

	public ListBox getLbnote12() {
		return lbnote12;
	}

	public void setLbnote12(ListBox lbnote12) {
		this.lbnote12 = lbnote12;
	}

	public ListBox getLbnote13() {
		return lbnote13;
	}

	public void setLbnote13(ListBox lbnote13) {
		this.lbnote13 = lbnote13;
	}

	public ListBox getLbnote14() {
		return lbnote14;
	}

	public void setLbnote14(ListBox lbnote14) {
		this.lbnote14 = lbnote14;
	}

	public ListBox getLbnote15() {
		return lbnote15;
	}
	public void setLbnote15(ListBox lbnote15) {
		this.lbnote15 = lbnote15;
	}

	public TextBox getTbCertificateNo() {
		return tbCertificateNo;
	}

	public void setTbCertificateNo(TextBox tbCertificateNo) {
		this.tbCertificateNo = tbCertificateNo;
	}

	public ObjectListBox<Declaration> getDeclarationMessageList() {
		return declarationMessageList;
	}

	public void setDeclarationMessageList(
			ObjectListBox<Declaration> declarationMessageList) {
		this.declarationMessageList = declarationMessageList;
	}

	public Button getAddDeclarationButton() {
		return addDeclarationButton;
	}

	public void setAddDeclarationButton(Button addDeclarationButton) {
		this.addDeclarationButton = addDeclarationButton;
	}

	public Button getAddbyCONSIGNERComposite() {
		return addbyCONSIGNERComposite;
	}

	public void setAddbyCONSIGNERComposite(Button addbyCONSIGNERComposite) {
		this.addbyCONSIGNERComposite = addbyCONSIGNERComposite;
	}

	public Button getAddbyCONSIGNEEComposite() {
		return addbyCONSIGNEEComposite;
	}

	public void setAddbyCONSIGNEEComposite(Button addbyCONSIGNEEComposite) {
		this.addbyCONSIGNEEComposite = addbyCONSIGNEEComposite;
	}

	public PersonInfoComposite getPersonInfoCONSIGNERComposite() {
		return personInfoCONSIGNERComposite;
	}

	public void setPersonInfoCONSIGNERComposite(
			PersonInfoComposite personInfoCONSIGNERComposite) {
		this.personInfoCONSIGNERComposite = personInfoCONSIGNERComposite;
	}

	public PersonInfoComposite getPersonInfoCONSIGNEEComposite() {
		return personInfoCONSIGNEEComposite;
	}

	public void setPersonInfoCONSIGNEEComposite(
			PersonInfoComposite personInfoCONSIGNEEComposite) {
		this.personInfoCONSIGNEEComposite = personInfoCONSIGNEEComposite;
	}

	public Button getNewFromCustomer() {
		return newFromCustomer;
	}

	public void setNewFromCustomer(Button newFromCustomer) {
		this.newFromCustomer = newFromCustomer;
	}

	public Button getNewToCustomer() {
		return newToCustomer;
	}

	public void setNewToCustomer(Button newToCustomer) {
		this.newToCustomer = newToCustomer;
	}

	public String getConsignerName() {
		return consignerName;
	}

	public void setConsignerName(String consignerName) {
		this.consignerName = consignerName;
	}

	public String getConsignerPocName() {
		return consignerPocName;
	}

	public void setConsignerPocName(String consignerPocName) {
		this.consignerPocName = consignerPocName;
	}

	public String getConsignerEmail() {
		return consignerEmail;
	}

	public void setConsignerEmail(String consignerEmail) {
		this.consignerEmail = consignerEmail;
	}

	public Long getConsignerCellNo() {
		return consignerCellNo;
	}

	public void setConsignerCellNo(Long consignerCellNo) {
		this.consignerCellNo = consignerCellNo;
	}

	public String getConsigneeName() {
		return consigneeName;
	}

	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}

	public String getConsigneePocName() {
		return consigneePocName;
	}

	public void setConsigneePocName(String consigneePocName) {
		this.consigneePocName = consigneePocName;
	}

	public String getConsigneeEmail() {
		return consigneeEmail;
	}

	public void setConsigneeEmail(String consigneeEmail) {
		this.consigneeEmail = consigneeEmail;
	}

	public Long getConsigneeCellNo() {
		return consigneeCellNo;
	}

	public void setConsigneeCellNo(Long consigneeCellNo) {
		this.consigneeCellNo = consigneeCellNo;
	}

	public Button getEditconsignee() {
		return editconsignee;
	}

	public void setEditconsignee(Button editconsignee) {
		this.editconsignee = editconsignee;
	}

	public Button getEditconsigner() {
		return editconsigner;
	}

	public void setEditconsigner(Button editconsigner) {
		this.editconsigner = editconsigner;
	}

	
	public boolean isConsignereditFlag() {
		return consignereditFlag;
	}

	public void setConsignereditFlag(boolean consignereditFlag) {
		this.consignereditFlag = consignereditFlag;
	}

	public boolean isConsigneeEditFlag() {
		return consigneeEditFlag;
	}

	public void setConsigneeEditFlag(boolean consigneeEditFlag) {
		this.consigneeEditFlag = consigneeEditFlag;
	}


	public TextBox getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(TextBox grossWeight) {
		this.grossWeight = grossWeight;
	}

	public ObjectListBox<Config> getOlbdoquantitydeclareUnit() {
		return olbdoquantitydeclareUnit;
	}

	public void setOlbdoquantitydeclareUnit(
			ObjectListBox<Config> olbdoquantitydeclareUnit) {
		this.olbdoquantitydeclareUnit = olbdoquantitydeclareUnit;
	}
	
	public TextBox getTbbdurationoffumigation() {
		return tbbdurationoffumigation;
	}

	public void setTbbdurationoffumigation(TextBox tbbdurationoffumigation) {
		this.tbbdurationoffumigation = tbbdurationoffumigation;
	}
	

	@Override
	public void onChange(ChangeEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource().equals(olbBranch)){
			if(olbBranch.getSelectedIndex()!=0){
				Branch branch=olbBranch.getSelectedItem();
				/**
				 *  Updated By:Viraj
				 *  Date: 12-01-2019
				 *  Description: To take regNo defined in branch named regNoMBR
				 **/
				if(branch.getRegNoMBR() != null)
					tbDtePPQSRegistrationNumber.setValue(branch.getRegNoMBR());
				/** Ends **/
				else{
					tbDtePPQSRegistrationNumber.setValue("");
				}
//				String b = ""+olbBranch.getValue().charAt(0);
//				String certificateNumber = "PMI/"+b+"/"+"MB/";
//				if(tbSerialNumber.getValue()!=null){
//					certificateNumber += tbSerialNumber.getValue()+"/";
//				}
//				String year = AppUtility.GetCurrentFinancialYear();
//				certificateNumber += year;
//				tbCertificateNo.setValue(certificateNumber);
		}
	  }
	}

	public Button getSelectTemplateButton() {
		return selectTemplateButton;
	}

	public void setSelectTemplateButton(Button selectTemplateButton) {
		this.selectTemplateButton = selectTemplateButton;
	}

	public PersonInfoComposite getPersonInfoComposite() {
		return personInfoComposite;
	}

	public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
		this.personInfoComposite = personInfoComposite;
	}

	public TextBox getIbQuantity() {
		return ibQuantity;
	}

	public void setIbQuantity(TextBox ibQuantity) {
		this.ibQuantity = ibQuantity;
	}	
	
	
	public TextArea getFromacshipping() {
		return fromacshipping;
	}

	public void setFromacshipping(TextArea fromacshipping) {
		this.fromacshipping = fromacshipping;
	}
	
	public TextArea getToacshipping() {
		return toacshipping;
	}

	public void setToacshipping(TextArea toacshipping) {
		this.toacshipping = toacshipping;
	}
	
	
}

//package com.slicktechnologies.client.views.fumigationdetails;
//
//import java.util.Vector;
//
//import com.gargoylesoftware.htmlunit.WebConsole.Logger;
//import com.google.gwt.user.client.ui.Button;
//import com.google.gwt.user.client.ui.InlineLabel;
//import com.google.gwt.user.client.ui.Label;
//import com.google.gwt.user.client.ui.ListBox;
//import com.google.gwt.user.client.ui.TextArea;
//import com.google.gwt.user.client.ui.TextBox;
//import com.google.gwt.user.datepicker.client.DateBox;
//import com.simplesoftwares.client.library.FieldType;
//import com.simplesoftwares.client.library.FormField;
//import com.simplesoftwares.client.library.FormFieldBuilder;
//import com.simplesoftwares.client.library.FlexForm.FormStyle;
//import com.simplesoftwares.client.library.appskeleton.AppMemory;
//import com.simplesoftwares.client.library.appskeleton.ProcessLevelBar;
//import com.simplesoftwares.client.library.appskeleton.ScreeenState;
//import com.simplesoftwares.client.library.appstructure.SuperModel;
//import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
//import com.simplesoftwares.client.library.appstructure.search.Filter;
//import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
//import com.simplesoftwares.client.library.composite.AddressComposite;
//import com.simplesoftwares.client.library.composite.PersonInfoComposite;
//import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
//import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
//import com.slicktechnologies.client.login.LoginPresenter;
//import com.slicktechnologies.client.utility.AppConstants;
//import com.slicktechnologies.client.utility.AppUtility;
//import com.slicktechnologies.client.utility.Screen;
//import com.slicktechnologies.client.utils.Console;
//import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
//import com.slicktechnologies.shared.common.fumigation.Fumigation;
//import com.slicktechnologies.shared.common.helperlayer.Config;
//import com.slicktechnologies.shared.common.helperlayer.Declaration;
//import com.slicktechnologies.shared.common.humanresourcelayer.Country;
//import com.slicktechnologies.shared.common.role.AuthorizationHelper;
//
//public class FumigationForm extends FormScreen<Fumigation>{
//
//	TextBox tbtreatmentcardno;
//	DateBox dbdateofissue;
//	ObjectListBox<Country> olbimportcountry;
//	
//	TextBox tbdescriptionofgoods;
//	TextBox tbquantitydeclared;
//	TextBox tbdistinguishingmarks;
//	TextBox tbconsignmentlink;
//	
//	
//	//   rohan added this 3 fields for siddhivinay pest control 
//	
//	TextBox packing;
//	TextBox volumeOfContainer;
//	TextBox totalNetWeight;
//	TextBox grossWeight;
//	
//	//****************rohan chnages here ap  per suma pest cotrol changes ************
//	
//	/** The person info composite. */
//	PersonInfoComposite personInfoComposite;
//	
//	Button addbyCONSIGNERComposite;
//	PersonInfoComposite personInfoCONSIGNERComposite;
//	Button addbyCONSIGNEEComposite;
//	PersonInfoComposite personInfoCONSIGNEEComposite;
//	TextBox tbShippingBillNo;
//	TextBox tbInvoiceNo;
//	TextBox tbCertificateNo;
//	ObjectListBox<Declaration> declarationMessageList;
//	Button addDeclarationButton;
//	
//	
//	//***************button for editing customer ************* 
//	Button editconsignee,editconsigner;
// 	
//	ObjectListBox<Config> olbdoquantitydeclareUnit;
//	//************************changes ends here *************************************
//	
//	TextBox tbnameofvessel;
//	ObjectListBox<Country> olbcountrydestinatoin;
//	
//	ObjectListBox<Config> olbpointofentry;
//	TextBox tbconsignername;
//	AddressComposite fromacshippingcomposite;
//	TextBox tbconsignee;
//	AddressComposite toacshippingcomposite;
//	
//	ListBox lbnamefumigation;
//	DateBox dbdatefumigation; 
////	TextBox tbplaceoffumigation;
////	TextBox tbportcountyloading;
//	
//	ObjectListBox<Config> olbdosagerateoffumigation;
//	ObjectListBox<Config> olbdurationoffumigation;
//	
//	ObjectListBox<Config> olbportcountyloading;
//	
//	
//	ObjectListBox<Config> olplaceoffumigation;
//	ObjectListBox<Config> olbminairtemp;
//	
//	
//	
//	TextArea tacomment,moreComment;
//	TextBox tbplace;
//	DateBox dbdateadd;
//	
//	TextBox tbcontractId;
//	TextBox tbserviceId;
//	DateBox dbcontractstartdate;
//	DateBox dbcontractenddate;
//	DateBox dbservicedate;
//	
//	TextBox tbcount;
//	
//	Label note1,note2,note3,note4,note5;
//	Label note11,note12,note13,note14,note15;
//	ListBox lbnote1,lbnote2,lbnote3,lbnote4;
//	ListBox lbnote11,lbnote12,lbnote13,lbnote14,lbnote15;
//	ListBox lbnote123;
//	
//	Button newFromCustomer;
//	Button newToCustomer;
//	
//	String consignerName;
//	String consignerPocName;
//	String consignerEmail;
//	Long consignerCellNo;
//	
//	String consigneeName;
//	String consigneePocName;
//	String consigneeEmail;
//	Long consigneeCellNo;
//	
//	public boolean consignereditFlag=false;
//	public boolean consigneeEditFlag=false;
//	
//	DateBox invoiceDate;
//	DateBox shippingBillDate;
//	Fumigation fumigationObj;
//	
//	public FumigationForm() {
//		super();
//		createGui();
//	}
//	
//	public FumigationForm  (String[] processlevel, FormField[][] fields,
//			FormStyle formstyle) 
//	{
//		super(processlevel, fields, formstyle);
//		createGui();
//		
//		
//	}
//	
//	
//	private void initalizeWidget(){
//		
//		
//		declarationMessageList = new ObjectListBox<Declaration>();
//		MyQuerry querry123 = new MyQuerry(new Vector<Filter>(),new Declaration());
//		declarationMessageList.MakeLive(querry123);
//		
//		addDeclarationButton= new Button("Add Declaration");
//		
//		tbcount=new TextBox();
//		tbcount.setEnabled(false);
//		
//		tbtreatmentcardno=new TextBox();
//		dbdateofissue=new  DateBoxWithYearSelector();
//		
//		
//		tbdescriptionofgoods=new TextBox();
//		tbquantitydeclared=new TextBox();
//		tbdistinguishingmarks=new TextBox();
//		tbconsignmentlink=new TextBox();
//		
//		
//		 packing = new TextBox();
//		 volumeOfContainer =  new TextBox();
//		 totalNetWeight= new TextBox();
//		 grossWeight = new TextBox();
//		
//		//***************rohan changes 
//		
//		 addbyCONSIGNERComposite = new Button("Add");
//		 	MyQuerry custquerry1 = new MyQuerry();
//			Filter custfilteer1 = new Filter();
//			custfilteer1.setQuerryString("status");
//			custfilteer1.setBooleanvalue(true);
//			custquerry1.setQuerryObject(new Customer());
//			personInfoCONSIGNERComposite = new PersonInfoComposite(custquerry1, false);
//		 
//		
//		
//		addbyCONSIGNEEComposite= new Button("Add");
//		
//		MyQuerry custquerry = new MyQuerry();
//		Filter custfilteer = new Filter();
//		custfilteer.setQuerryString("status");
//		custfilteer.setBooleanvalue(true);
//		custquerry.setQuerryObject(new Customer());
//		personInfoCONSIGNEEComposite = new PersonInfoComposite(custquerry, false);
//		
//		
//		tbCertificateNo=new TextBox();
//		tbInvoiceNo = new TextBox();
//		tbShippingBillNo= new TextBox();
//		
////		tbportcountyloading=new TextBox();
//		tbnameofvessel=new TextBox();
//		
//		olbimportcountry=new ObjectListBox<Country>();
//		MyQuerry querry=new MyQuerry();
//		querry.setQuerryObject(new Country());
//		olbimportcountry.MakeLive(querry);
//		
//		olbcountrydestinatoin=new ObjectListBox<Country>();
//		olbcountrydestinatoin.MakeLive(querry);
//		
//		
//		
//		olbpointofentry=new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(olbpointofentry, Screen.FUMIGATIONDECLAREDPOE);
//		
//		tbconsignername=new TextBox();
//		tbconsignername.setEnabled(false);
//		fromacshippingcomposite=new AddressComposite(true);
//		
//		tbconsignee=new TextBox();
//		tbconsignee.setEnabled(false);
//		
//		toacshippingcomposite=new AddressComposite(true);
//		
//		lbnamefumigation=new ListBox();
//		lbnamefumigation.addItem("--SELECT--");
//		lbnamefumigation.addItem("Methyle Bromide");
//		lbnamefumigation.addItem("Aluminium Phosphide");
//		dbdatefumigation=new  DateBoxWithYearSelector();
////		tbplaceoffumigation=new TextBox();
//		
//		
//		personInfoComposite=AppUtility.customerInfoComposite(new Customer());
//		personInfoComposite.getCustomerId().getHeaderLabel().setText("* Customer ID");
//		personInfoComposite.getCustomerName().getHeaderLabel().setText("* Customer Name");
//		personInfoComposite.getCustomerCell().getHeaderLabel().setText("* Customer Cell");
//		
//		olbdosagerateoffumigation=new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(olbdosagerateoffumigation, Screen.DOSAGERATEOFFUMIGANT);
//		
//		
//		olbdurationoffumigation=new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(olbdurationoffumigation, Screen.DURATIONOFFUMIGATION);
//		
//		olbminairtemp=new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(olbminairtemp, Screen.MINIMUMAIRTEMP);
//		
//		olbportcountyloading= new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(olbportcountyloading, Screen.COUNTRYOFLOADING);
//
//		olplaceoffumigation= new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(olplaceoffumigation, Screen.PLACEOFFUMIGATION);
//		
//		//****************rohan *******************
//		olbdoquantitydeclareUnit =new ObjectListBox<Config>();
//		AppUtility.MakeLiveConfig(olbdoquantitydeclareUnit, Screen.QUANTITYDECLAREUNIT);
//		
//		tacomment=new TextArea();
//		moreComment= new TextArea();
//		tbplace=new TextBox();
//		dbdateadd=new  DateBoxWithYearSelector();
//		dbservicedate=new DateBoxWithYearSelector();
//		dbservicedate.setEnabled(false);
//		tbcontractId=new TextBox();
//		tbcontractId.setEnabled(false);
//		tbserviceId=new TextBox();
//		tbserviceId.setEnabled(false);
//		dbcontractstartdate=new DateBoxWithYearSelector();
//		dbcontractstartdate.setEnabled(false);
//		dbcontractenddate=new DateBoxWithYearSelector();
//		dbcontractenddate.setEnabled(false);
//		note1=new Label("Fumigation has been performed in a container / under Gas tight enclosure / sheet ");
////		note1.getElement().getStyle().setFontSize(15, Unit.PX);
//		note2=new Label("Container pressure test conducted");
//		note3=new Label("Container has 200 mm free air space at top of container ");
//		note4=new Label("In transit fumigation needs ventillation at port of discharge ");
//		note5=new Label("Container/Enclosure has been ventillated to below 5ppm v/v Methyl Bromide ");
//		
//		lbnote123= new ListBox();
//		lbnote123.addItem("--SELECT--");
//		lbnote123.addItem("YES");
//		lbnote123.addItem("NO");
//		lbnote123.addItem("N.A.");
//		lbnote123.setSelectedIndex(1);
//		
//		
//		lbnote1=new ListBox();
//		lbnote1.addItem("--SELECT--");
//		lbnote1.addItem("YES");
//		lbnote1.addItem("NO");
//		lbnote1.addItem("N.A.");
//		lbnote1.setSelectedIndex(3);
//		
//		lbnote2=new ListBox();
//		lbnote2.addItem("--SELECT--");
//		lbnote2.addItem("YES");
//		lbnote2.addItem("NO");
//		lbnote2.addItem("N.A.");
//		lbnote2.setSelectedIndex(1);
//		
//		lbnote3=new ListBox();
//		lbnote3.addItem("--SELECT--");
//		lbnote3.addItem("YES");
//		lbnote3.addItem("NO");
//		lbnote3.addItem("N.A.");
//		lbnote3.setSelectedIndex(3);
//		
//		lbnote4=new ListBox();
//		lbnote4.addItem("--SELECT--");
//		lbnote4.addItem("YES");
//		lbnote4.addItem("NO");
//		lbnote4.addItem("N.A.");
//		lbnote4.setSelectedIndex(1);
//		
//		
//		note11=new Label("Has the commodity been fumigated prior to lacquering,varnishing,painting or wrapping ?");
//		note12=new Label("Has plastic Wrapping been use in consignment ?");
//		note13=new Label("*If yes, has the consignment been fumigated prior wrapping ?");
//		note14=new Label("*Or has the plastic wrapping been slashed,opened or perforated in accordance with the wrapping and perforation standards ?");
//		note15=new Label("Is the timber in this consignment less than 200 mm thick in dimention and correctly spaced every 200 mm in height ?");
//		
//		lbnote11=new ListBox();
//		lbnote11.addItem("--SELECT--");
//		lbnote11.addItem("YES");
//		lbnote11.addItem("NO");
//		lbnote11.addItem("N.A.");
//		lbnote11.setSelectedIndex(2);
//		
//		lbnote12=new ListBox();
//		lbnote12.addItem("--SELECT--");
//		lbnote12.addItem("YES");
//		lbnote12.addItem("NO");
//		lbnote12.addItem("N.A.");
//		lbnote12.setSelectedIndex(3);
//		
//		lbnote13=new ListBox();
//		lbnote13.addItem("--SELECT--");
//		lbnote13.addItem("YES");
//		lbnote13.addItem("NO");
//		lbnote13.addItem("N.A.");
//		lbnote13.setSelectedIndex(3);
//		
//		lbnote14=new ListBox();
//		lbnote14.addItem("--SELECT--");
//		lbnote14.addItem("YES");
//		lbnote14.addItem("NO");
//		lbnote14.addItem("N.A.");
//		lbnote14.setSelectedIndex(3);
//		
//		lbnote15=new ListBox();
//		lbnote15.addItem("--SELECT--");
//		lbnote15.addItem("YES");
//		lbnote15.addItem("NO");
//		lbnote15.addItem("N.A.");
//		lbnote15.setSelectedIndex(1);
//		
//		newFromCustomer=new Button("New Consigner");
//		newToCustomer=new Button("New Consignee");
//		
//		invoiceDate =new DateBox();
//		shippingBillDate = new DateBox();
//		
//		editconsignee = new Button("Edit Consignee");
//		editconsigner = new Button("Edit Consigner");
//		
//	}
//
//
//	@Override
//	public void setCount(int count) {
//
//		tbcount.setValue(count+"");
//	
//	}
//
//	@Override
//	public void createScreen() {
//
//
//		initalizeWidget();
//	
//		this.processlevelBarNames = new String[] { "New" ,"Copy Certificate"};
//		FormFieldBuilder fbuilder;
//		fbuilder = new FormFieldBuilder();
//		
//		FormField fgroupingCustomerInformation=fbuilder.setlabel("CUSTOMER INFORMATION").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
//		fbuilder = new FormFieldBuilder("",personInfoComposite);
//		FormField fpersonInfoCompositeCustomer= fbuilder.setRowSpan(0).setColSpan(4).build();	
//		
//		FormField fgroupingfumigationInformation=fbuilder.setlabel("* FUMIGATION DETAILS").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder("Fumigation ID",tbcount);
//		FormField fumigationcount= fbuilder.setRowSpan(0).setColSpan(0).build();	
//		
////		fbuilder = new FormFieldBuilder(" Registration No.",tbtreatmentcardno);
////		FormField treatmentcardno= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();	
//		fbuilder = new FormFieldBuilder("  Date of Issue",dbdateofissue);
//		FormField dateissue= fbuilder.setMandatory(true).setMandatoryMsg("Date is Mandatory").setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Importing Country",olbimportcountry);
//		FormField importcountry= fbuilder.setRowSpan(0).setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder();
//		FormField fgroupingGOODSformation=fbuilder.setlabel("* DETAILS OF GOOD").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder("Description of Goods",tbdescriptionofgoods);
//		FormField descofgoods= fbuilder.setMandatory(true).setMandatoryMsg("Description is Mandatory").setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Quantity Declared",tbquantitydeclared);
//		FormField qtydeclare= fbuilder.setMandatory(true).setMandatoryMsg("Quantity Declared is Mandatory").setRowSpan(0).setColSpan(0).build();
//		
//		
//		fbuilder = new FormFieldBuilder("Distinguishing Marks",tbdistinguishingmarks);
//		FormField distmark= fbuilder.setMandatory(true).setMandatoryMsg("Distinguishing Marks is Mandatory").setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Consignment Link/Container No",tbconsignmentlink);
//		FormField consinglink= fbuilder.setMandatory(true).setMandatoryMsg("Consignment Link is Mandatory").setRowSpan(0).setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Certificate No.",tbCertificateNo);
//		FormField tbCertificateNo= fbuilder.setMandatory(true).setMandatoryMsg("Certificate No. is Mandatory").setRowSpan(0).setColSpan(0).build();
//
//		fbuilder = new FormFieldBuilder("Invoice No.",tbInvoiceNo);
//		FormField tbInvoiceNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//
//		fbuilder = new FormFieldBuilder("Invoice Date",invoiceDate);
//		FormField finvoiceDate= fbuilder.setMandatory(false).setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("Shipping Bill Date",shippingBillDate);
//		FormField fshippingBillDate= fbuilder.setMandatory(false).setColSpan(0).build();
//	
//		fbuilder = new FormFieldBuilder("Shipping Bill No.",tbShippingBillNo);
//		FormField ftbShippingBillNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//
//		fbuilder = new FormFieldBuilder("Port & Country of Loading",olbportcountyloading);
//		FormField countryloading= fbuilder.setMandatory(true).setMandatoryMsg("Port & Country of Loading is Mandatory").setRowSpan(0).setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder(" Name of Vessel/Ship",tbnameofvessel);
//		FormField nameofvessel= fbuilder.setMandatory(true).setMandatoryMsg(" Name of Vessel/Ship is Mandatory").setRowSpan(0).setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("Country of Destination",olbcountrydestinatoin);
//		FormField countrydesct= fbuilder.setMandatory(true).setMandatoryMsg("Country of Destination is Mandatory").setColSpan(0).build();
//
//		fbuilder = new FormFieldBuilder("Declare Point of Entry",olbpointofentry);
//		FormField declareentry= fbuilder.setMandatory(true).setMandatoryMsg("Declare Point of Entry is Mandatory").setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder();
//		FormField fgroupingfromformation=fbuilder.setlabel("NAME AND CONSIGNER ADDRESS").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder("* Company Name",tbconsignername);
//		FormField formcompanyname= fbuilder.setMandatory(true).setMandatoryMsg("Company Name is Mandatory").setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder(" ",fromacshippingcomposite);
//		FormField fromaddress= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder();
//		FormField fgroupingtoformation=fbuilder.setlabel("NAME AND CONSIGNEE ADDRESS").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder("* Company Name",tbconsignee);
//		FormField tocompanyname= fbuilder.setMandatory(true).setMandatoryMsg("Company Name is Mandatory").setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder(" ",toacshippingcomposite);
//		FormField toaddress= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(4).build();
//		
//		
//		fbuilder = new FormFieldBuilder();
//		FormField fgroupingTRITMENTformation=fbuilder.setlabel("* DETAILS OF TREATMENT").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder("Name of Fumigation",lbnamefumigation);
//		FormField fumigationname= fbuilder.setMandatory(true).setMandatoryMsg("Name of Fumigation is Mandatory").setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Date Of Fumigation",dbdatefumigation);
//		FormField fumigationdate= fbuilder.setMandatory(true).setMandatoryMsg("Date of Fumigation is Mandatory").setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Place of Fumigation",olplaceoffumigation);
//		FormField fumigationplace= fbuilder.setMandatory(true).setMandatoryMsg("Place of Fumigation is Mandatory").setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("Unit for Quantity",olbdoquantitydeclareUnit);
//		FormField folbdoquantitydeclareUnit= fbuilder.setMandatory(true).setMandatoryMsg("Place of Fumigation is Mandatory").setColSpan(0).build();
//		
//		
//		
//		fbuilder = new FormFieldBuilder("Dosage Rate of Fumigation",olbdosagerateoffumigation);
//		FormField dosefumigation= fbuilder.setMandatory(true).setMandatoryMsg("Dosage Rate of Fumigation is Mandatory").setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("Duration of Fumigation(in hrs)",olbdurationoffumigation);
//		FormField durationfumigation= fbuilder.setMandatory(true).setMandatoryMsg("Duration of Mandatory").setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("Min. Air Temp.(in "+"\u00b0"+"C)",olbminairtemp);
//		FormField tempfumigation= fbuilder.setMandatory(true).setMandatoryMsg("Min. Air Temp. is Mandatory").setColSpan(0).build();
//		
//		
//		fbuilder = new FormFieldBuilder(" ",note1);
//		FormField labelnote1= fbuilder.setColSpan(3).build();
//		
//		fbuilder = new FormFieldBuilder(" ",lbnote123);
//		FormField lbnote123= fbuilder.setColSpan(0).build();
//		
//		
//		fbuilder = new FormFieldBuilder(" ",note2);
//		FormField labelnote2= fbuilder.setColSpan(3).build();
//		
//		fbuilder = new FormFieldBuilder(" ",lbnote1);
//		FormField lbnote1= fbuilder.setColSpan(0).build();
//		
//		
//		
//		fbuilder = new FormFieldBuilder(" ",note3);
//		FormField labelnote3= fbuilder.setColSpan(3).build();
//		
//		fbuilder = new FormFieldBuilder(" ",lbnote2);
//		FormField lbnote2= fbuilder.setColSpan(0).build();
//		
//		
//		
//		fbuilder = new FormFieldBuilder(" ",note4);
//		FormField labelnote4= fbuilder.setColSpan(3).build();
//		
//		fbuilder = new FormFieldBuilder(" ",lbnote3);
//		FormField lbnote3= fbuilder.setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder(" ",note5);
//		FormField labelnote5= fbuilder.setColSpan(3).build();
//		
//		fbuilder = new FormFieldBuilder(" ",lbnote4);
//		FormField lbnote4= fbuilder.setColSpan(0).build();
//		
//		
//
//		fbuilder = new FormFieldBuilder();
//		FormField wrapperformation=fbuilder.setlabel("WRAPPING AND TIMBER").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder(" ",note11);
//		FormField labelnote11= fbuilder.setColSpan(3).build();
//		
//		fbuilder = new FormFieldBuilder(" ",lbnote11);
//		FormField lbnote11= fbuilder.setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder(" ",note12);
//		FormField labelnote12= fbuilder.setColSpan(3).build();
//		
//		fbuilder = new FormFieldBuilder(" ",lbnote12);
//		FormField lbnote12= fbuilder.setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder(" ",note13);
//		FormField labelnote13= fbuilder.setColSpan(3).build();
//		
//		fbuilder = new FormFieldBuilder(" ",lbnote13);
//		FormField lbnote13= fbuilder.setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder(" ",note14);
//		FormField labelnote14= fbuilder.setColSpan(3).build();
//		
//		fbuilder = new FormFieldBuilder(" ",lbnote14);
//		FormField lbnote14= fbuilder.setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder(" ",note15);
//		FormField labelnote15= fbuilder.setColSpan(3).build();
//		
//		fbuilder = new FormFieldBuilder(" ",lbnote15);
//		FormField lbnote15= fbuilder.setColSpan(0).build();
//		
//		
//		fbuilder = new FormFieldBuilder();
//		FormField ADDINformation=fbuilder.setlabel("* ADDITIONAL DECLARATION").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder("Comments",tacomment);
//		FormField commetfumigation= fbuilder.setMandatory(false).setMandatoryMsg("Comment is Mandatory").setColSpan(4).build();
//		fbuilder = new FormFieldBuilder("Comments",moreComment);
//		FormField moreComment= fbuilder.setMandatory(true).setMandatoryMsg("Comment is Mandatory").setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder("Place",tbplace);
//		FormField placefumigation= fbuilder.setMandatory(true).setMandatoryMsg("Place is Mandatory").setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Date",dbdateadd);
//		FormField datefumigation= fbuilder.setMandatory(true).setMandatoryMsg("Date is Madatory").setColSpan(0).build();
//		
//
//		fbuilder = new FormFieldBuilder();
//		FormField information=fbuilder.setlabel("REFERENCE INFORMATION").widgetType(FieldType.Grouping).setMandatory(true).setColSpan(4).build();
//		
//		fbuilder = new FormFieldBuilder("Contract ID",tbcontractId);
//		FormField contrctid= fbuilder.setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Service ID",tbserviceId);
//		FormField serviceid= fbuilder.setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Contract Start Date",dbcontractstartdate);
//		FormField contractstart= fbuilder.setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Contract End Date",dbcontractenddate);
//		FormField contractend= fbuilder.setColSpan(0).build();
//		fbuilder = new FormFieldBuilder("Service Date",dbservicedate);
//		FormField servicedate= fbuilder.setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder("Select Declaration Message ",declarationMessageList);
//	    FormField fdeclarationMessageList= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		
//	    fbuilder = new FormFieldBuilder(" ",addDeclarationButton);
//		FormField faddDeclarationButton= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
//		
//		fbuilder = new FormFieldBuilder("",personInfoCONSIGNEEComposite);
//		FormField fpersonInfoComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
//		
//		fbuilder = new FormFieldBuilder(" ",addbyCONSIGNEEComposite);
//		FormField faddbyComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		
//		
//		fbuilder = new FormFieldBuilder("",personInfoCONSIGNERComposite);
//		FormField fpersonInfoCONSIGNERComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(5).build();
//		
//		fbuilder = new FormFieldBuilder(" ",addbyCONSIGNERComposite);
//		FormField faddbyCONSIGNERComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder(" ",newFromCustomer);
//		FormField fnewformCustomer= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		
//		fbuilder = new FormFieldBuilder(" ",newToCustomer);
//		FormField fnewtoCustomer= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		
//		
//		
//		fbuilder = new FormFieldBuilder(" ",editconsignee);
//		FormField feditconsignee= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
//			
//		fbuilder = new FormFieldBuilder(" ",editconsigner);
//		FormField feditconsigner= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
//		
//		
//		
//		fbuilder = new FormFieldBuilder("Packing",packing);
//	    FormField fpacking= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//	    
//	    fbuilder = new FormFieldBuilder("Volume of container ",volumeOfContainer);
//	    FormField fvolumeOfContainer= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//	    
//	    fbuilder = new FormFieldBuilder("Total Net Weight",totalNetWeight);
//	    FormField ftotalNetWeight= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		
//	    fbuilder = new FormFieldBuilder("Total Gross Weight",grossWeight);
//	    FormField fgrossWeight= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//	    
//		
//		FormField[][] formfield = {
//				{fgroupingCustomerInformation},
//				{fpersonInfoCompositeCustomer},
//				{fgroupingfumigationInformation},
//				{fumigationcount,tbCertificateNo,dateissue},
//				{information},{contrctid,serviceid,contractstart,contractend},{servicedate},
//				{fgroupingGOODSformation},
//				{descofgoods,qtydeclare,folbdoquantitydeclareUnit,distmark},
//				{consinglink,countryloading,nameofvessel,declareentry},
//				{countrydesct,importcountry,fpacking,fvolumeOfContainer},
//				{ftotalNetWeight,fgrossWeight},
//				{fgroupingfromformation},
//				{fpersonInfoCONSIGNERComposite,faddbyCONSIGNERComposite},
//				{formcompanyname,fnewformCustomer,feditconsigner},
//				{fromaddress},
//				{fgroupingtoformation},
//				{fpersonInfoComposite,faddbyComposite},
//				{tocompanyname,fnewtoCustomer,feditconsignee},
//				{toaddress},
//				{fgroupingTRITMENTformation},
//				{fumigationname,fumigationdate,fumigationplace,dosefumigation},
//				{durationfumigation,tempfumigation},{labelnote1,lbnote123},{labelnote2,lbnote1},{labelnote3,lbnote2},
//				{labelnote4,lbnote3},{labelnote5,lbnote4},
//				{wrapperformation},{labelnote11,lbnote11},{labelnote12,lbnote12},{labelnote13,lbnote13},
//				{labelnote14,lbnote14},{labelnote15,lbnote15},
//				{ADDINformation},
//				{tbInvoiceNo,finvoiceDate,ftbShippingBillNo,fshippingBillDate},
//				{commetfumigation},
//				{fdeclarationMessageList,faddDeclarationButton},
//				{moreComment}
//		};
//		
//		this.fields=formfield;
//		
//		
//	}
//
//	@Override
//	public void updateModel(Fumigation model) {
//
//		
////		if(invoiceDate.getValue()!=null)
//			model.setInvoiceDate(invoiceDate.getValue());
//		
////		if(shippingBillDate.getValue()!=null)
//			model.setShippingBillDate(shippingBillDate.getValue());
//		
//			if(personInfoComposite.getValue()!=null)
//				model.setcInfo(personInfoComposite.getValue());
//		
//		if(tbShippingBillNo.getValue()!=null)
//			model.setShippingBillNo(tbShippingBillNo.getValue());
//		
//		if(tbtreatmentcardno.getValue()!=null)
//			model.setTreatmentcardno(tbtreatmentcardno.getValue());
//		if(dbdateofissue.getValue()!=null)
//			model.setDateofissue(dbdateofissue.getValue());
//		if(olbimportcountry.getValue()!=null)
//			model.setImportcountry(olbimportcountry.getValue());
//		if(tbdescriptionofgoods.getValue()!=null)
//			model.setDescriptionofgoods(tbdescriptionofgoods.getValue());
//		if(tbquantitydeclared.getValue()!=null)
//			model.setQuantitydeclare(tbquantitydeclared.getValue());
//		if(tbdistinguishingmarks.getValue()!=null)
//			model.setDistiinguishingmarks(tbdistinguishingmarks.getValue());
//		if(tbconsignmentlink.getValue()!=null)
//			model.setConsignmentlink(tbconsignmentlink.getValue());
//
//		
//		
//		
//		if(tbInvoiceNo.getValue()!=null)
//			model.setInvoiceNo(tbInvoiceNo.getValue());
//		
//		if(tbCertificateNo.getValue()!=null)
//			model.setCertificateNo(tbCertificateNo.getValue());
//		
//		if(tbnameofvessel.getValue()!=null)
//			model.setNameofvessel(tbnameofvessel.getValue());
//		if(olbcountrydestinatoin.getValue()!=null)
//			model.setCountryofdestination(olbcountrydestinatoin.getValue());
//		if (olbpointofentry.getValue() != null) {
//			model.setDeclarepointentry(olbpointofentry.getValue());
//		}
//		if(tbconsignername.getValue()!=null)
//			model.setFromCompanyname(tbconsignername.getValue());
//		if (fromacshippingcomposite.getValue() != null) {
//			model.setFromaddress(fromacshippingcomposite.getValue());
//			}
//		if(tbconsignee.getValue()!=null)
//			model.setToCompanyname(tbconsignee.getValue());
//		if (toacshippingcomposite.getValue() != null) {
//			model.setTomaddress(toacshippingcomposite.getValue());
//		}
//
//			model.setNameoffumigation(lbnamefumigation.getItemText(lbnamefumigation.getSelectedIndex()));
//			Console.log("date of fumigation before saving........"+dbdatefumigation.getValue());
//		if(dbdatefumigation.getValue()!=null)
//			model.setDateoffumigation(dbdatefumigation.getValue());
//		
//		Console.log("date of fumigation after saving in update model ........"+model.getDateoffumigation());
//
//		
//		
//		
//		if(olbdoquantitydeclareUnit.getSelectedIndex()!=0)
//			model.setQuantitydeclareUnit(olbdoquantitydeclareUnit.getValue(olbdoquantitydeclareUnit.getSelectedIndex()));
//		
//		
//		if(olbportcountyloading.getSelectedIndex()!=0)
//			model.setPortncountryloading(olbportcountyloading.getValue(olbportcountyloading.getSelectedIndex()));
//		
//		if(olplaceoffumigation.getSelectedIndex()!=0)
//			model.setPlaceoffumigation(olplaceoffumigation.getValue(olplaceoffumigation.getSelectedIndex()));
//		if(olbdosagerateoffumigation.getSelectedIndex()!=0)
//			model.setDoseratefumigation(olbdosagerateoffumigation.getValue(olbdosagerateoffumigation.getSelectedIndex()));
//		if(olbdurationoffumigation.getSelectedIndex()!=0)
//			model.setDurartionfumigation(olbdurationoffumigation.getValue(olbdurationoffumigation.getSelectedIndex()));
//		if(olbminairtemp.getSelectedIndex()!=0)
//			model.setMinairtemp(olbminairtemp.getValue(olbminairtemp.getSelectedIndex()));
//		
//			model.setNote123(lbnote123.getItemText(lbnote123.getSelectedIndex()));
//			model.setNote1(lbnote1.getItemText(lbnote1.getSelectedIndex()));
//			model.setNote2(lbnote2.getItemText(lbnote2.getSelectedIndex()));
//			model.setNote3(lbnote3.getItemText(lbnote3.getSelectedIndex()));
//			model.setNote4(lbnote4.getItemText(lbnote4.getSelectedIndex()));
//			model.setNote11(lbnote11.getItemText(lbnote11.getSelectedIndex()));
//			model.setNote12(lbnote12.getItemText(lbnote12.getSelectedIndex()));
//			model.setNote13(lbnote13.getItemText(lbnote13.getSelectedIndex()));
//			model.setNote14(lbnote14.getItemText(lbnote14.getSelectedIndex()));
//			model.setNote15(lbnote15.getItemText(lbnote15.getSelectedIndex()));
//		
//		
//		System.out.println("note1 update ==== "+lbnote1.getItemText(lbnote1.getSelectedIndex()));
//		
//		
//		if(tacomment.getValue()!=null)
//			model.setAdditionaldeclaration(tacomment.getValue());
//		if(dbdateadd.getValue()!=null)
//			model.setPdate(dbdateadd.getValue());
//		if(tbplace.getValue()!=null)
//			model.setPlace(tbplace.getValue());
//		
//		if(!tbcontractId.getValue().equals("")){
//			model.setContractID(Integer.parseInt(tbcontractId.getValue()));
//		}
//			
//		if(!tbserviceId.getValue().equals("")){
//			model.setServiceID(Integer.parseInt(tbserviceId.getValue()));
//		}
//		
//			
//		if(dbcontractenddate.getValue()!=null)
//			model.setContractEnddate(dbcontractenddate.getValue());
//		if(dbcontractstartdate.getValue()!=null)
//			model.setContractStartdate(dbcontractstartdate.getValue());
//		if(dbservicedate.getValue()!=null)
//			model.setServiceDate(dbservicedate.getValue());
//
//		
//		if(moreComment.getValue()!=null)
//			model.setMoredecl(moreComment.getValue());
//		
//		
//		if(consignerName!=null){
//			model.setConsignerName(consignerName);
//		}
//		if(consignerPocName!=null){
//			model.setConsignerPocName(consignerPocName);
//		}
//		if(consignerCellNo!=null){
//			model.setConsignerCellNo(consignerCellNo);
//		}
//		if(consignerEmail!=null){
//			model.setConsignerEmail(consignerEmail);
//		}
//		
//		
//		if(consigneeName!=null){
//			model.setConsigneeName(consigneeName);
//		}
//		if(consigneePocName!=null){
//			model.setConsigneePocName(consigneePocName);
//		}
//		if(consigneeCellNo!=null){
//			model.setConsigneeCellNo(consigneeCellNo);
//		}
//		if(consigneeEmail!=null){
//			model.setConsigneeEmail(consigneeEmail);
//		}
//		
//		if(totalNetWeight.getValue()!=null)
//			model.setTotalNetWeight(totalNetWeight.getValue());
//		
//		
//		if(grossWeight.getValue()!=null)
//			model.setGrossWeight(grossWeight.getValue());
//		
//		if(packing.getValue()!=null)
//			model.setPackingValue(packing.getValue());
//		if(volumeOfContainer.getValue()!=null)
//			model.setVolumeofcontainer(volumeOfContainer.getValue());
//		
//		
//			model.setConsigneeEditFlag(consigneeEditFlag);
//			model.setConsignereditFlag(consignereditFlag);
//		
//			
//		fumigationObj=model;	
//		presenter.setModel(model);
//
//	}
//
//	@Override
//	public void updateView(Fumigation view) {
//
//		
//		fumigationObj=view;
//		
//		System.out.println("rohan in new state count"+view.getCount());
//		
//		this.tbcount.setValue(view.getCount()+"");
//		
//		if(view.getcInfo()!=null)
//			personInfoComposite.setValue(view.getcInfo());
//		
//		if(view.getInvoiceDate()!=null)
//			invoiceDate.setValue(view.getInvoiceDate());
//		
//		if(view.getShippingBillDate()!=null)
//			shippingBillDate.setValue(view.getShippingBillDate());
//		
//		if(view.getShippingBillNo()!=null)
//			tbShippingBillNo.setValue(view.getShippingBillNo());
//		
//		if(view.getMoredecl()!=null)
//			moreComment.setValue(view.getMoredecl());
//		if(view.getTreatmentcardno()!=null)
//			tbtreatmentcardno.setValue(view.getTreatmentcardno());
//		if(view.getDateofissue()!=null)
//			dbdateofissue.setValue(view.getDateofissue());
//		if(view.getImportcountry()!=null)
//			olbimportcountry.setValue(view.getImportcountry());
//		if(view.getDescriptionofgoods()!=null)
//			tbdescriptionofgoods.setValue(view.getDescriptionofgoods());
//		if(view.getQuantitydeclare()!=null)
//			tbquantitydeclared.setValue(view.getQuantitydeclare());
//		if(view.getDistiinguishingmarks()!=null)
//			tbdistinguishingmarks.setValue(view.getDistiinguishingmarks());
//		if(view.getConsignmentlink()!=null)
//			tbconsignmentlink.setValue(view.getConsignmentlink());
//		if(view.getInvoiceNo()!=null)
//			tbInvoiceNo.setValue(view.getInvoiceNo());
//		
//		if(view.getCertificateNo()!=null)
//			tbCertificateNo.setValue(view.getCertificateNo());
//		
//		if(view.getPortncountryloading()!=null)
//			olbportcountyloading.setValue(view.getPortncountryloading());
//		if(view.getNameofvessel()!=null)
//			tbnameofvessel.setValue(view.getNameofvessel());
//		
//		if(view.getQuantitydeclareUnit()!=null)
//			olbdoquantitydeclareUnit.setValue(view.getQuantitydeclareUnit());
//		
//		if(view.getCountryofdestination()!=null)
//			olbcountrydestinatoin.setValue(view.getCountryofdestination());
//		
//		if(view.getDeclarepointentry()!=null)
//			olbpointofentry.setValue(view.getDeclarepointentry());
//		if(view.getFromCompanyname()!=null)
//			tbconsignername.setValue(view.getFromCompanyname());
//		if (view.getFromaddress() != null)
//			fromacshippingcomposite.setValue(view.getFromaddress());
//		if(view.getToCompanyname()!=null)
//			tbconsignee.setValue(view.getToCompanyname());
//		if (view.getTomaddress() != null)
//			toacshippingcomposite.setValue(view.getTomaddress());
//		
//		
//		
//		if(view.getNameoffumigation()!=null)
//		{
//			for(int i=0;i<lbnamefumigation.getItemCount();i++)
//			{
//				String data=lbnamefumigation.getItemText(i);
//				if(data.equals(view.getNameoffumigation()))
//				{
//					lbnamefumigation.setSelectedIndex(i);
//					break;
//				}
//			}
//		}
//
//		
//		
//		
//		Console.log("date of fumigation after saving in update view ........"+view.getDateoffumigation());
//		
//		if (view.getDateoffumigation() != null)
//			dbdatefumigation.setValue(view.getDateoffumigation());
//		if (view.getPlaceoffumigation() != null)
//			olplaceoffumigation.setValue(view.getPlaceoffumigation());
//		if (view.getDoseratefumigation() != null)
//			olbdosagerateoffumigation.setValue(view.getDoseratefumigation());
//		if (view.getDurartionfumigation() != null)
//			olbdurationoffumigation.setValue(view.getDurartionfumigation());
//		if (view.getMinairtemp() != null)
//			olbminairtemp.setValue(view.getMinairtemp());
//		if (view.getAdditionaldeclaration() != null)
//			tacomment.setValue(view.getAdditionaldeclaration());
//		
//		
//		if(view.getNote123()!=null)
//			lbnote123.setTitle(view.getNote123());
//		if(view.getNote1()!=null)
//			lbnote1.setTitle(view.getNote1());
//		if(view.getNote2()!=null)
//			lbnote2.setTitle(view.getNote2());
//		if(view.getNote3()!=null)
//			lbnote3.setTitle(view.getNote3());
//		if(view.getNote4()!=null)
//			lbnote4.setTitle(view.getNote4());
//		if(view.getNote11()!=null)
//			lbnote11.setTitle(view.getNote11());
//		if(view.getNote12()!=null)
//			lbnote12.setTitle(view.getNote12());
//		if(view.getNote13()!=null)
//			lbnote13.setTitle(view.getNote13());
//		if(view.getNote14()!=null)
//			lbnote14.setTitle(view.getNote14());
//		if(view.getNote15()!=null)
//			lbnote15.setTitle(view.getNote15());
//		
//		if(view.getNote123()!=null)
//		{
//			for(int i=0;i<lbnote123.getItemCount();i++)
//			{
//				String data=lbnote123.getItemText(i);
//				if(data.equals(view.getNote123()))
//				{
//					lbnote123.setSelectedIndex(i);
//					break;
//				}
//			}
//		}
//		
//		
//		if(view.getNote1()!=null)
//		{
//			for(int i=0;i<lbnote1.getItemCount();i++)
//			{
//				String data=lbnote1.getItemText(i);
//				if(data.equals(view.getNote1()))
//				{
//					lbnote1.setSelectedIndex(i);
//					break;
//				}
//			}
//		}
//		if(view.getNote2()!=null)
//		{
//			for(int i=0;i<lbnote2.getItemCount();i++)
//			{
//				String data=lbnote2.getItemText(i);
//				if(data.equals(view.getNote2()))
//				{
//					lbnote2.setSelectedIndex(i);
//					break;
//				}
//			}
//		}
//		if(view.getNote3()!=null)
//		{
//			for(int i=0;i<lbnote3.getItemCount();i++)
//			{
//				String data=lbnote3.getItemText(i);
//				if(data.equals(view.getNote3()))
//				{
//					lbnote3.setSelectedIndex(i);
//					break;
//				}
//			}
//		}
//		if(view.getNote4()!=null)
//		{
//			for(int i=0;i<lbnote4.getItemCount();i++)
//			{
//				String data=lbnote4.getItemText(i);
//				if(data.equals(view.getNote4()))
//				{
//					lbnote4.setSelectedIndex(i);
//					break;
//				}
//			}
//		}
//		if(view.getNote11()!=null)
//		{
//			for(int i=0;i<lbnote11.getItemCount();i++)
//			{
//				String data=lbnote11.getItemText(i);
//				if(data.equals(view.getNote11()))
//				{
//					lbnote11.setSelectedIndex(i);
//					break;
//				}
//			}
//		}
//		if(view.getNote12()!=null)
//		{
//			for(int i=0;i<lbnote12.getItemCount();i++)
//			{
//				String data=lbnote12.getItemText(i);
//				if(data.equals(view.getNote12()))
//				{
//					lbnote12.setSelectedIndex(i);
//					break;
//				}
//			}
//		}
//		if(view.getNote13()!=null)
//		{
//			for(int i=0;i<lbnote13.getItemCount();i++)
//			{
//				String data=lbnote13.getItemText(i);
//				if(data.equals(view.getNote13()))
//				{
//					lbnote13.setSelectedIndex(i);
//					break;
//				}
//			}
//		}
//		if(view.getNote14()!=null)
//		{
//			for(int i=0;i<lbnote14.getItemCount();i++)
//			{
//				String data=lbnote14.getItemText(i);
//				if(data.equals(view.getNote14()))
//				{
//					lbnote14.setSelectedIndex(i);
//					break;
//				}
//			}
//		}
//		if(view.getNote15()!=null)
//		{
//			for(int i=0;i<lbnote15.getItemCount();i++)
//			{
//				String data=lbnote15.getItemText(i);
//				if(data.equals(view.getNote15()))
//				{
//					lbnote15.setSelectedIndex(i);
//					break;
//				}
//			}
//		}
//		
//		
//		
//		
//		
//		System.out.println("note1===="+view.getNote1());
//		
//		
//		if (view.getPdate() != null)
//			dbdateadd.setValue(view.getPdate());
//		if (view.getPlace() != null)
//			tbplace.setValue(view.getPlace());
//		
//		if (view.getContractID() != null)
//			tbcontractId.setValue(view.getContractID()+"");
//		if (view.getServiceID() != null)
//			tbserviceId.setValue(view.getServiceID()+"");
//		if (view.getContractStartdate() != null)
//			dbcontractstartdate.setValue(view.getContractStartdate());
//		if (view.getContractEnddate()!= null)
//			dbcontractenddate.setValue(view.getContractEnddate());
//		if (view.getServiceDate()!= null)
//			dbservicedate.setValue(view.getServiceDate());
//		
//		
//		if (view.getGrossWeight()!= null)
//			grossWeight.setValue(view.getGrossWeight());
//		
//		if (view.getTotalNetWeight()!= null)
//			totalNetWeight.setValue(view.getTotalNetWeight());
//		
//		if (view.getVolumeofcontainer()!= null)
//			volumeOfContainer.setValue(view.getVolumeofcontainer());
//		
//		if (view.getPackingValue()!= null)
//			packing.setValue(view.getPackingValue());
//		
//		presenter.setModel(view);
//		
//	}
//	
//	
//
//	
//
//	
//
//	@Override
//	public void toggleAppHeaderBarMenu() {
//		
//		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
//		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
//			for(int k=0;k<menus.length;k++)
//			{
//				String text=menus[k].getText();
//				if(text.equals("Save")||text.equals("Discard")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
//				{
//					menus[k].setVisible(true); 
//				}
//				else
//					menus[k].setVisible(false);  		   
//			}
//		}
//
//		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
//		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
//			for(int k=0;k<menus.length;k++)
//			{
//				String text=menus[k].getText();
//				if(text.equals("Save")||text.equals("Discard")||text.equals(AppConstants.NAVIGATION))
//					menus[k].setVisible(true); 
//
//				else
//					menus[k].setVisible(false);  		   
//			}
//		}
//
//		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
//		{
//			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
//			for(int k=0;k<menus.length;k++)
//			{
//				String text=menus[k].getText();
//				if(text.equals("Discard")||text.equals("Print")||text.equals("Edit")||text.equals("Email")||text.equals("Search")||text.equals(AppConstants.NAVIGATION))
//					menus[k].setVisible(true); 
//
//				else
//					menus[k].setVisible(false);  		   
//			}
//		}
//		
//		AuthorizationHelper.setAsPerAuthorization(Screen.FUMIGATION,LoginPresenter.currentModule.trim());
//	}
//
//	@Override
//	public void setEnable(boolean state) {
//	
//		super.setEnable(state);
//		this.tbcount.setEnabled(false);
//		this.tbcontractId.setEnabled(false);
//		this.tbserviceId.setEnabled(false);
//		this.dbcontractenddate.setEnabled(false);
//		this.dbcontractstartdate.setEnabled(false);
//		this.dbservicedate.setEnabled(false);
//		tbconsignername.setEnabled(false);
//		tbconsignee.setEnabled(false);
//	}
//
//	@Override
//	public void setToViewState() {
//		super.setToViewState();
//		
//		SuperModel model=new Fumigation();
//		model=fumigationObj;
//		AppUtility.addDocumentToHistoryTable(AppConstants.SERVICEMODULE,AppConstants.FUMIGATION, fumigationObj.getCount(), null,null,null, false, model, null);
//	}
//	
//	/****************************************************Getter and Setter**************************************************/
//
//	
//	
//	public TextBox getTbtreatmentcardno() {
//		return tbtreatmentcardno;
//	}
//
//	public TextBox getPacking() {
//		return packing;
//	}
//
//	public void setPacking(TextBox packing) {
//		this.packing = packing;
//	}
//
//	public TextBox getVolumeOfContainer() {
//		return volumeOfContainer;
//	}
//
//	public void setVolumeOfContainer(TextBox volumeOfContainer) {
//		this.volumeOfContainer = volumeOfContainer;
//	}
//
//	public TextBox getTotalNetWeight() {
//		return totalNetWeight;
//	}
//
//	public void setTotalNetWeight(TextBox totalNetWeight) {
//		this.totalNetWeight = totalNetWeight;
//	}
//
//	public void setTbtreatmentcardno(TextBox tbtreatmentcardno) {
//		this.tbtreatmentcardno = tbtreatmentcardno;
//	}
//
//	public DateBox getDbdateofissue() {
//		return dbdateofissue;
//	}
//
//	public void setDbdateofissue(DateBox dbdateofissue) {
//		this.dbdateofissue = dbdateofissue;
//	}
//
//	public ObjectListBox<Country> getOlbimportcountry() {
//		return olbimportcountry;
//	}
//
//	public void setOlbimportcountry(ObjectListBox<Country> olbimportcountry) {
//		this.olbimportcountry = olbimportcountry;
//	}
//
//	public TextBox getTbdescriptionofgoods() {
//		return tbdescriptionofgoods;
//	}
//
//	public void setTbdescriptionofgoods(TextBox tbdescriptionofgoods) {
//		this.tbdescriptionofgoods = tbdescriptionofgoods;
//	}
//
//	public TextBox getTbquantitydeclared() {
//		return tbquantitydeclared;
//	}
//
//	public void setTbquantitydeclared(TextBox tbquantitydeclared) {
//		this.tbquantitydeclared = tbquantitydeclared;
//	}
//
//	public TextBox getTbdistinguishingmarks() {
//		return tbdistinguishingmarks;
//	}
//
//	public void setTbdistinguishingmarks(TextBox tbdistinguishingmarks) {
//		this.tbdistinguishingmarks = tbdistinguishingmarks;
//	}
//
//	public TextBox getTbconsignmentlink() {
//		return tbconsignmentlink;
//	}
//
//	public void setTbconsignmentlink(TextBox tbconsignmentlink) {
//		this.tbconsignmentlink = tbconsignmentlink;
//	}
//
////	public TextBox getTbContainerno() {
////		return tbContainerno;
////	}
////
////	public void setTbContainerno(TextBox tbContainerno) {
////		this.tbContainerno = tbContainerno;
////	}
//
//
//	public TextBox getTbnameofvessel() {
//		return tbnameofvessel;
//	}
//
//	public void setTbnameofvessel(TextBox tbnameofvessel) {
//		this.tbnameofvessel = tbnameofvessel;
//	}
//
//	public ObjectListBox<Country> getOlbcountrydestinatoin() {
//		return olbcountrydestinatoin;
//	}
//
//	public void setOlbcountrydestinatoin(
//			ObjectListBox<Country> olbcountrydestinatoin) {
//		this.olbcountrydestinatoin = olbcountrydestinatoin;
//	}
//
//
//	public ObjectListBox<Config> getOlbpointofentry() {
//		return olbpointofentry;
//	}
//
//	public void setOlbpointofentry(ObjectListBox<Config> olbpointofentry) {
//		this.olbpointofentry = olbpointofentry;
//	}
//
//	public TextBox getTbconsignername() {
//		return tbconsignername;
//	}
//
//	public void setTbconsignername(TextBox tbconsignername) {
//		this.tbconsignername = tbconsignername;
//	}
//
//	public AddressComposite getFromacshippingcomposite() {
//		return fromacshippingcomposite;
//	}
//
//	public void setFromacshippingcomposite(AddressComposite fromacshippingcomposite) {
//		this.fromacshippingcomposite = fromacshippingcomposite;
//	}
//
//	public TextBox getTbconsignee() {
//		return tbconsignee;
//	}
//
//	public void setTbconsignee(TextBox tbconsignee) {
//		this.tbconsignee = tbconsignee;
//	}
//
//	public AddressComposite getToacshippingcomposite() {
//		return toacshippingcomposite;
//	}
//
//	public void setToacshippingcomposite(AddressComposite toacshippingcomposite) {
//		this.toacshippingcomposite = toacshippingcomposite;
//	}
//
//	public ListBox getLbnamefumigation() {
//		return lbnamefumigation;
//	}
//
//	public void setLbnamefumigation(ListBox lbnamefumigation) {
//		this.lbnamefumigation = lbnamefumigation;
//	}
//
//	public DateBox getDbdatefumigation() {
//		return dbdatefumigation;
//	}
//
//	public void setDbdatefumigation(DateBox dbdatefumigation) {
//		this.dbdatefumigation = dbdatefumigation;
//	}
//
//	public ObjectListBox<Config> getOlbdosagerateoffumigation() {
//		return olbdosagerateoffumigation;
//	}
//
//	public void setOlbdosagerateoffumigation(
//			ObjectListBox<Config> olbdosagerateoffumigation) {
//		this.olbdosagerateoffumigation = olbdosagerateoffumigation;
//	}
//
//	public ObjectListBox<Config> getOlbdurationoffumigation() {
//		return olbdurationoffumigation;
//	}
//
//	public void setOlbdurationoffumigation(
//			ObjectListBox<Config> olbdurationoffumigation) {
//		this.olbdurationoffumigation = olbdurationoffumigation;
//	}
//
//	public ObjectListBox<Config> getOlbminairtemp() {
//		return olbminairtemp;
//	}
//
//	public void setOlbminairtemp(ObjectListBox<Config> olbminairtemp) {
//		this.olbminairtemp = olbminairtemp;
//	}
//
//	public TextArea getTacomment() {
//		return tacomment;
//	}
//
//	public void setTacomment(TextArea tacomment) {
//		this.tacomment = tacomment;
//	}
//
//	public TextBox getTbplace() {
//		return tbplace;
//	}
//
//	public void setTbplace(TextBox tbplace) {
//		this.tbplace = tbplace;
//	}
//
//	public DateBox getDbdateadd() {
//		return dbdateadd;
//	}
//
//	public void setDbdateadd(DateBox dbdateadd) {
//		this.dbdateadd = dbdateadd;
//	}
//
//	public TextBox getTbcontractId() {
//		return tbcontractId;
//	}
//
//	public void setTbcontractId(TextBox tbcontractId) {
//		this.tbcontractId = tbcontractId;
//	}
//
//	public TextBox getTbserviceId() {
//		return tbserviceId;
//	}
//
//	public void setTbserviceId(TextBox tbserviceId) {
//		this.tbserviceId = tbserviceId;
//	}
//
//	public DateBox getDbcontractstartdate() {
//		return dbcontractstartdate;
//	}
//
//	public void setDbcontractstartdate(DateBox dbcontractstartdate) {
//		this.dbcontractstartdate = dbcontractstartdate;
//	}
//
//	public DateBox getDbcontractenddate() {
//		return dbcontractenddate;
//	}
//
//	public void setDbcontractenddate(DateBox dbcontractenddate) {
//		this.dbcontractenddate = dbcontractenddate;
//	}
//
//	public DateBox getDbservicedate() {
//		return dbservicedate;
//	}
//
//	public void setDbservicedate(DateBox dbservicedate) {
//		this.dbservicedate = dbservicedate;
//	}
//
//	public TextBox getTbcount() {
//		return tbcount;
//	}
//
//	public void setTbcount(TextBox tbcount) {
//		this.tbcount = tbcount;
//	}
//
//	public Label getNote1() {
//		return note1;
//	}
//
//	public void setNote1(Label note1) {
//		this.note1 = note1;
//	}
//
//	public Label getNote2() {
//		return note2;
//	}
//
//	public void setNote2(Label note2) {
//		this.note2 = note2;
//	}
//
//	public Label getNote3() {
//		return note3;
//	}
//
//	public void setNote3(Label note3) {
//		this.note3 = note3;
//	}
//
//	public Label getNote4() {
//		return note4;
//	}
//
//	public void setNote4(Label note4) {
//		this.note4 = note4;
//	}
//
//	public Label getNote5() {
//		return note5;
//	}
//
//	public void setNote5(Label note5) {
//		this.note5 = note5;
//	}
//
//	public Label getNote11() {
//		return note11;
//	}
//
//	public void setNote11(Label note11) {
//		this.note11 = note11;
//	}
//
//	public Label getNote12() {
//		return note12;
//	}
//
//	public void setNote12(Label note12) {
//		this.note12 = note12;
//	}
//
//	public Label getNote13() {
//		return note13;
//	}
//
//	public void setNote13(Label note13) {
//		this.note13 = note13;
//	}
//
//	public Label getNote14() {
//		return note14;
//	}
//
//	public void setNote14(Label note14) {
//		this.note14 = note14;
//	}
//
//	public Label getNote15() {
//		return note15;
//	}
//
//	public void setNote15(Label note15) {
//		this.note15 = note15;
//	}
//
//	public ListBox getLbnote1() {
//		return lbnote1;
//	}
//
//	public void setLbnote1(ListBox lbnote1) {
//		this.lbnote1 = lbnote1;
//	}
//
//	public ListBox getLbnote2() {
//		return lbnote2;
//	}
//
//	public void setLbnote2(ListBox lbnote2) {
//		this.lbnote2 = lbnote2;
//	}
//
//	public ListBox getLbnote3() {
//		return lbnote3;
//	}
//
//	public void setLbnote3(ListBox lbnote3) {
//		this.lbnote3 = lbnote3;
//	}
//
//	public ListBox getLbnote4() {
//		return lbnote4;
//	}
//
//	public void setLbnote4(ListBox lbnote4) {
//		this.lbnote4 = lbnote4;
//	}
//
//	public ListBox getLbnote11() {
//		return lbnote11;
//	}
//
//	public void setLbnote11(ListBox lbnote11) {
//		this.lbnote11 = lbnote11;
//	}
//
//	public ListBox getLbnote12() {
//		return lbnote12;
//	}
//
//	public void setLbnote12(ListBox lbnote12) {
//		this.lbnote12 = lbnote12;
//	}
//
//	public ListBox getLbnote13() {
//		return lbnote13;
//	}
//
//	public void setLbnote13(ListBox lbnote13) {
//		this.lbnote13 = lbnote13;
//	}
//
//	public ListBox getLbnote14() {
//		return lbnote14;
//	}
//
//	public void setLbnote14(ListBox lbnote14) {
//		this.lbnote14 = lbnote14;
//	}
//
//	public ListBox getLbnote15() {
//		return lbnote15;
//	}
//	public void setLbnote15(ListBox lbnote15) {
//		this.lbnote15 = lbnote15;
//	}
//
//	public TextBox getTbCertificateNo() {
//		return tbCertificateNo;
//	}
//
//	public void setTbCertificateNo(TextBox tbCertificateNo) {
//		this.tbCertificateNo = tbCertificateNo;
//	}
//
//	public ObjectListBox<Declaration> getDeclarationMessageList() {
//		return declarationMessageList;
//	}
//
//	public void setDeclarationMessageList(
//			ObjectListBox<Declaration> declarationMessageList) {
//		this.declarationMessageList = declarationMessageList;
//	}
//
//	public Button getAddDeclarationButton() {
//		return addDeclarationButton;
//	}
//
//	public void setAddDeclarationButton(Button addDeclarationButton) {
//		this.addDeclarationButton = addDeclarationButton;
//	}
//
//	public Button getAddbyCONSIGNERComposite() {
//		return addbyCONSIGNERComposite;
//	}
//
//	public void setAddbyCONSIGNERComposite(Button addbyCONSIGNERComposite) {
//		this.addbyCONSIGNERComposite = addbyCONSIGNERComposite;
//	}
//
//	public Button getAddbyCONSIGNEEComposite() {
//		return addbyCONSIGNEEComposite;
//	}
//
//	public void setAddbyCONSIGNEEComposite(Button addbyCONSIGNEEComposite) {
//		this.addbyCONSIGNEEComposite = addbyCONSIGNEEComposite;
//	}
//
//	public PersonInfoComposite getPersonInfoCONSIGNERComposite() {
//		return personInfoCONSIGNERComposite;
//	}
//
//	public void setPersonInfoCONSIGNERComposite(
//			PersonInfoComposite personInfoCONSIGNERComposite) {
//		this.personInfoCONSIGNERComposite = personInfoCONSIGNERComposite;
//	}
//
//	public PersonInfoComposite getPersonInfoCONSIGNEEComposite() {
//		return personInfoCONSIGNEEComposite;
//	}
//
//	public void setPersonInfoCONSIGNEEComposite(
//			PersonInfoComposite personInfoCONSIGNEEComposite) {
//		this.personInfoCONSIGNEEComposite = personInfoCONSIGNEEComposite;
//	}
//
//	public Button getNewFromCustomer() {
//		return newFromCustomer;
//	}
//
//	public void setNewFromCustomer(Button newFromCustomer) {
//		this.newFromCustomer = newFromCustomer;
//	}
//
//	public Button getNewToCustomer() {
//		return newToCustomer;
//	}
//
//	public void setNewToCustomer(Button newToCustomer) {
//		this.newToCustomer = newToCustomer;
//	}
//
//	public String getConsignerName() {
//		return consignerName;
//	}
//
//	public void setConsignerName(String consignerName) {
//		this.consignerName = consignerName;
//	}
//
//	public String getConsignerPocName() {
//		return consignerPocName;
//	}
//
//	public void setConsignerPocName(String consignerPocName) {
//		this.consignerPocName = consignerPocName;
//	}
//
//	public String getConsignerEmail() {
//		return consignerEmail;
//	}
//
//	public void setConsignerEmail(String consignerEmail) {
//		this.consignerEmail = consignerEmail;
//	}
//
//	public Long getConsignerCellNo() {
//		return consignerCellNo;
//	}
//
//	public void setConsignerCellNo(Long consignerCellNo) {
//		this.consignerCellNo = consignerCellNo;
//	}
//
//	public String getConsigneeName() {
//		return consigneeName;
//	}
//
//	public void setConsigneeName(String consigneeName) {
//		this.consigneeName = consigneeName;
//	}
//
//	public String getConsigneePocName() {
//		return consigneePocName;
//	}
//
//	public void setConsigneePocName(String consigneePocName) {
//		this.consigneePocName = consigneePocName;
//	}
//
//	public String getConsigneeEmail() {
//		return consigneeEmail;
//	}
//
//	public void setConsigneeEmail(String consigneeEmail) {
//		this.consigneeEmail = consigneeEmail;
//	}
//
//	public Long getConsigneeCellNo() {
//		return consigneeCellNo;
//	}
//
//	public void setConsigneeCellNo(Long consigneeCellNo) {
//		this.consigneeCellNo = consigneeCellNo;
//	}
//
//	public Button getEditconsignee() {
//		return editconsignee;
//	}
//
//	public void setEditconsignee(Button editconsignee) {
//		this.editconsignee = editconsignee;
//	}
//
//	public Button getEditconsigner() {
//		return editconsigner;
//	}
//
//	public void setEditconsigner(Button editconsigner) {
//		this.editconsigner = editconsigner;
//	}
//
//	public ObjectListBox<Config> getOlbdoquantitydeclareUnit() {
//		return olbdoquantitydeclareUnit;
//	}
//
//	public void setOlbdoquantitydeclareUnit(
//			ObjectListBox<Config> olbdoquantitydeclareUnit) {
//		this.olbdoquantitydeclareUnit = olbdoquantitydeclareUnit;
//	}
//
//	public boolean isConsignereditFlag() {
//		return consignereditFlag;
//	}
//
//	public void setConsignereditFlag(boolean consignereditFlag) {
//		this.consignereditFlag = consignereditFlag;
//	}
//
//	public boolean isConsigneeEditFlag() {
//		return consigneeEditFlag;
//	}
//
//	public void setConsigneeEditFlag(boolean consigneeEditFlag) {
//		this.consigneeEditFlag = consigneeEditFlag;
//	}
//
//	public ObjectListBox<Config> getOlbportcountyloading() {
//		return olbportcountyloading;
//	}
//
//	public void setOlbportcountyloading(ObjectListBox<Config> olbportcountyloading) {
//		this.olbportcountyloading = olbportcountyloading;
//	}
//
//	public ObjectListBox<Config> getOlplaceoffumigation() {
//		return olplaceoffumigation;
//	}
//
//	public void setOlplaceoffumigation(ObjectListBox<Config> olplaceoffumigation) {
//		this.olplaceoffumigation = olplaceoffumigation;
//	}
//
//	public TextBox getGrossWeight() {
//		return grossWeight;
//	}
//
//	public void setGrossWeight(TextBox grossWeight) {
//		this.grossWeight = grossWeight;
//	}
//	
//	
//}
