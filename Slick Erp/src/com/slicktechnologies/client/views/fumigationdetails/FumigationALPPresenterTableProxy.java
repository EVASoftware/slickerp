package com.slicktechnologies.client.views.fumigationdetails;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.fumigationdetails.FumigationALPPresenter.FumigationALPPresenterTable;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.fumigation.Fumigation;

public class FumigationALPPresenterTableProxy extends FumigationALPPresenterTable {

	
	TextColumn<Fumigation>getColumnfumigationID;
//	TextColumn<Fumigation>getColumnTreatmentno;
	
	TextColumn<Fumigation>getColumnContractID;
	TextColumn<Fumigation>getColumnServiceID;
	TextColumn<Fumigation>getColumnIssueDate;
	TextColumn<Fumigation>getColumnserviceDate;
	//**************Date 28/08/2017 BY JAYSHREE***************//
	
	TextColumn<Fumigation>getColumnFumigationName;
	
	//**********end****************//
	//************rohan addded this asper suma pest control changes 
	TextColumn<Fumigation>getColumnCertificateNo;
	TextColumn<Fumigation>getColumnShippingBillNo;
	
	TextColumn<Fumigation>getColumnExporterName;
	TextColumn<Fumigation>getColumnImporterName;

	/** date 23.6.2018 added by komal for port of loading **/
	TextColumn<Fumigation>getColumnPortOfCountry;
	//private TextColumn<Fumigation> getColumnFumigationName;

	

	
	
	public FumigationALPPresenterTableProxy() {
		
		super();
	
	}
	
	@Override 	
	public void createTable() {
		createColumnFumigationID();
		/** date 23.6.2018 added by komal for port of loading **/
		createColumnPortOfCountry();
//		createColumntreatmentno();
		createColumnCertificateNo();
		createColumnShippingBillNo();
		createColumnIssudate();
		createColumnExporter();
		createColumnImporter();
		createColumnContractID();
		createColumnserviceId();
		createColumnservicedate();
		createColumnFumigationName();
		
		table.setColumnWidth(getColumnfumigationID, 90, Unit.PX);
//		table.setColumnWidth(getColumnTreatmentno, 150, Unit.PX);
		table.setColumnWidth(getColumnContractID, 110, Unit.PX);
		table.setColumnWidth(getColumnServiceID, 110, Unit.PX);
		table.setColumnWidth(getColumnIssueDate, 120, Unit.PX);
		table.setColumnWidth(getColumnserviceDate, 120, Unit.PX);
		table.setColumnWidth(getColumnCertificateNo, 150, Unit.PX);
		table.setColumnWidth(getColumnShippingBillNo, 150, Unit.PX);
		table.setColumnWidth(getColumnExporterName, 130, Unit.PX);
		table.setColumnWidth(getColumnImporterName, 130, Unit.PX);
		table.setColumnWidth(getColumnFumigationName, 130,Unit.PX);
		/** date 23.6.2018 added by komal for port of loading **/
		table.setColumnWidth(getColumnPortOfCountry, 120,Unit.PX);
	}


private void createColumnExporter() {
		

		getColumnExporterName=new TextColumn<Fumigation>() {
			
			@Override
			public String getValue(Fumigation object) {
				return object.getFromCompanyname();
			}
		};
		table.addColumn(getColumnExporterName, "Consigner Name");
		getColumnExporterName.setSortable(true);
	}

	private void createColumnImporter() {
	

	getColumnImporterName=new TextColumn<Fumigation>() {
		
		@Override
		public String getValue(Fumigation object) {
			return object.getToCompanyname();
		}
	};
	table.addColumn(getColumnImporterName, "Consignee Name");
	getColumnImporterName.setSortable(true);
	}
	private void createColumnShippingBillNo() {
		
		getColumnShippingBillNo=new TextColumn<Fumigation>() {
			
			@Override
			public String getValue(Fumigation object) {
				return object.getShippingBillNo();
			}
		};
		table.addColumn(getColumnShippingBillNo, "ShippingBill No.");
		getColumnShippingBillNo.setSortable(true);
		
	}

	private void createColumnCertificateNo() {
		

		getColumnCertificateNo=new TextColumn<Fumigation>() {
			
			@Override
			public String getValue(Fumigation object) {
				return object.getCertificateNo();
			}
		};
		table.addColumn(getColumnCertificateNo, "Certificate No.");
		getColumnCertificateNo.setSortable(true);
	}

	private void createColumnIssudate() {
		
		getColumnIssueDate=new TextColumn<Fumigation>() {
			
			@Override
			public String getValue(Fumigation object) {
				return AppUtility.parseDate(object.getDateofissue());
			}
		};
		table.addColumn(getColumnIssueDate, "Issue Date");
		getColumnIssueDate.setSortable(true);
		
	}

	private void createColumnservicedate() {
		
		
		getColumnserviceDate=new TextColumn<Fumigation>() {
			
			@Override
			public String getValue(Fumigation object) {
				if(object.getServiceDate()!=null){
				
					return AppUtility.parseDate(object.getServiceDate());
				}else{
					return null;
				}
				
			}
		};
		table.addColumn(getColumnserviceDate, "Service Date");
		getColumnserviceDate.setSortable(true);
		
		
		
	}

	private void createColumnFumigationID() {

		getColumnfumigationID=new TextColumn<Fumigation>() {
			
			@Override
			public String getValue(Fumigation object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getColumnfumigationID, "ID");
		getColumnfumigationID.setSortable(true);
		
	}
	//******************* Date 28/08/2017     to add the column in the table for fumigation name******************/
	private void createColumnFumigationName() {
		
		 getColumnFumigationName = new TextColumn<Fumigation>(){

			@Override
			public String getValue(Fumigation object) {
				
					return object.getNameoffumigation()+" ";
				}
				
				};
		table.addColumn(getColumnFumigationName,"Fumigation Name");
		getColumnFumigationName.setSortable(true);
	}
//*********************end******************

//	private void createColumntreatmentno() {
//
//		getColumnTreatmentno=new TextColumn<Fumigation>() {
//			
//			@Override
//			public String getValue(Fumigation object) {
//				return object.getTreatmentcardno();
//			}
//		};
//		table.addColumn(getColumnTreatmentno, "Registration No.");
//		getColumnTreatmentno.setSortable(true);
//		
//	}

	private void createColumnContractID() {

		getColumnContractID=new TextColumn<Fumigation>() {
			
			@Override
			public String getValue(Fumigation object) {
				
				if(object.getContractID()!=null){
					return object.getContractID()+" ";
				}else{
					return null;
				}
				
				
			}
		};
		table.addColumn(getColumnContractID, "Contract ID");
		getColumnContractID.setSortable(true);
		
	}

	private void createColumnserviceId() {

		getColumnServiceID=new TextColumn<Fumigation>() {
			
			@Override
			public String getValue(Fumigation object) {
				if(object.getServiceID()!=null){
					return object.getServiceID()+" ";
				}else{
					return null;
				}
			}
		};
		table.addColumn(getColumnServiceID, "Service ID");
		getColumnServiceID.setSortable(true);
	}
	
	
	
	public void addColumnSorting() {
//		addColumntreatmentnoSorting();
		addColumnCertificateNoSorting();
		addColumnShippingBillNoSorting();
		addColumnCountIDSorting();
		addColumnIssudateSorting();
		addColumncontractIDSorting();
		addColumnServiceIDSorting();
		addColumnServicedateSorting();
		addColumnExporterNameSorting();
		addColumnImporterNameSorting();
		
		//*****************Date:28/08/2017  the changes are done for sorting the table in nameof fumigation************************/
		addColumnFumigationNameSorting();
		/** date 23.6.2018 added by komal for port of loading **/
		addColumnPortOfCountrySorting();
		
		//*****************************end********************/
		}

	


private void addColumnExporterNameSorting() {
		
		List<Fumigation> list=getDataprovider().getList();
		columnSort=new ListHandler<Fumigation>(list);
		columnSort.setComparator(getColumnExporterName, new Comparator<Fumigation>()
				{
			@Override
			public int compare(Fumigation e1,Fumigation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getFromCompanyname()!=null && e2.getFromCompanyname()!=null){
						return e1.getFromCompanyname().compareTo(e2.getFromCompanyname());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	private void addColumnImporterNameSorting() {
		
		List<Fumigation> list=getDataprovider().getList();
		columnSort=new ListHandler<Fumigation>(list);
		columnSort.setComparator(getColumnImporterName, new Comparator<Fumigation>()
				{
			@Override
			public int compare(Fumigation e1,Fumigation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getToCompanyname()!=null && e2.getToCompanyname()!=null){
						return e1.getToCompanyname().compareTo(e2.getToCompanyname());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
	
	private void addColumnShippingBillNoSorting() {
		
		List<Fumigation> list=getDataprovider().getList();
		columnSort=new ListHandler<Fumigation>(list);
		columnSort.setComparator(getColumnShippingBillNo, new Comparator<Fumigation>()
				{
			@Override
			public int compare(Fumigation e1,Fumigation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getShippingBillNo()!=null && e2.getShippingBillNo()!=null){
						return e1.getShippingBillNo().compareTo(e2.getShippingBillNo());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnCertificateNoSorting() {
		
		List<Fumigation> list=getDataprovider().getList();
		columnSort=new ListHandler<Fumigation>(list);
		columnSort.setComparator(getColumnCertificateNo, new Comparator<Fumigation>()
				{
			@Override
			public int compare(Fumigation e1,Fumigation e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCertificateNo()!=null && e2.getCertificateNo()!=null){
						return e1.getCertificateNo().compareTo(e2.getCertificateNo());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnIssudateSorting() {
		List<Fumigation> list = getDataprovider().getList();
		columnSort = new ListHandler<Fumigation>(list);
		Comparator<Fumigation> quote = new Comparator<Fumigation>() {
			@Override
			public int compare(Fumigation e1, Fumigation e2) {
				if (e1 != null && e2 != null) {
					if(e1.getDateofissue()== e2.getDateofissue()){
					  return 0;}
				  if(e1.getDateofissue().after(e2.getDateofissue())){
					  return 1;}
				  else{
					  return -1;}} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnIssueDate, quote);
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnServicedateSorting() {
		
		
		List<Fumigation> list = getDataprovider().getList();
		columnSort = new ListHandler<Fumigation>(list);
		Comparator<Fumigation> quote = new Comparator<Fumigation>() {
			@Override
			public int compare(Fumigation e1, Fumigation e2) {
				if (e1 != null && e2 != null) {
					if(e1.getServiceDate()== e2.getServiceDate()){
					  return 0;}
				  if(e1.getServiceDate().after(e2.getServiceDate())){
					  return 1;}
				  else{
					  return -1;}} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnIssueDate, quote);
		table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnCountIDSorting() {



		List<Fumigation> list = getDataprovider().getList();
		columnSort = new ListHandler<Fumigation>(list);
		Comparator<Fumigation> quote = new Comparator<Fumigation>() {
			@Override
			public int compare(Fumigation e1, Fumigation e2) {
				if (e1 != null && e2 != null) {
					if(e1.getCount()== e2.getCount()){
					  return 0;}
				  if(e1.getCount()> e2.getCount()){
					  return 1;}
				  else{
					  return -1;}} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnfumigationID, quote);
		table.addColumnSortHandler(columnSort);
		
		
		
	}
	//***************************************Date:28/08/2017  changes are done for to sort the table according to the name of fumigation *****************///
	private void addColumnFumigationNameSorting() {
		// TODO Auto-generated method stub
		List<Fumigation> list = getDataprovider().getList();
		columnSort = new ListHandler<Fumigation>(list);
		Comparator<Fumigation> quote = new Comparator<Fumigation>() {
			@Override
			public int compare(Fumigation e1, Fumigation e2) {
				if (e1 != null && e2 != null) {
					if (e1.getNameoffumigation() != null&& e2.getNameoffumigation() != null)
						return e1.getNameoffumigation().compareTo(e2.getNameoffumigation());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnFumigationName, quote);
		table.addColumnSortHandler(columnSort);
		
	}
//************************************end********************************/
//	private void addColumntreatmentnoSorting() {
//
//		
//		List<Fumigation> list = getDataprovider().getList();
//		columnSort = new ListHandler<Fumigation>(list);
//		Comparator<Fumigation> quote = new Comparator<Fumigation>() {
//			@Override
//			public int compare(Fumigation e1, Fumigation e2) {
//				if (e1 != null && e2 != null) {
//					if (e1.getTreatmentcardno() != null
//							&& e2.getTreatmentcardno() != null)
//						return e1.getTreatmentcardno().compareTo(
//								e2.getTreatmentcardno());
//					return 0;
//				} else
//					return 0;
//			}
//		};
//		columnSort.setComparator(getColumnTreatmentno, quote);
//		table.addColumnSortHandler(columnSort);
//		
//		
//	}

	private void addColumncontractIDSorting() {


		List<Fumigation> list = getDataprovider().getList();
		columnSort = new ListHandler<Fumigation>(list);
		Comparator<Fumigation> quote = new Comparator<Fumigation>() {
			@Override
			public int compare(Fumigation e1, Fumigation e2) {
				if (e1 != null && e2 != null) {
					if(e1.getContractID()== e2.getContractID()){
					  return 0;}
				  if(e1.getContractID()> e2.getContractID()){
					  return 1;}
				  else{
					  return -1;}} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnContractID, quote);
		table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnServiceIDSorting() {

		
		List<Fumigation> list = getDataprovider().getList();
		columnSort = new ListHandler<Fumigation>(list);
		Comparator<Fumigation> quote = new Comparator<Fumigation>() {
			@Override
			public int compare(Fumigation e1, Fumigation e2) {
				if (e1 != null && e2 != null) {
					if(e1.getServiceID()== e2.getServiceID()){
					  return 0;}
				  if(e1.getServiceID()> e2.getServiceID()){
					  return 1;}
				  else{
					  return -1;}} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnServiceID, quote);
		table.addColumnSortHandler(columnSort);
		
	}
	
	/** date 23.6.2018 added by komal for port of country**/
	private void createColumnPortOfCountry(){
	getColumnPortOfCountry=new TextColumn<Fumigation>() {
		
		@Override
		public String getValue(Fumigation object) {
			return object.getPortncountryloading();
		}
	};
	table.addColumn(getColumnPortOfCountry, "Port and Country of Loading");
	getColumnPortOfCountry.setSortable(true);
	}
	private void addColumnPortOfCountrySorting() {
		// TODO Auto-generated method stub
		List<Fumigation> list = getDataprovider().getList();
		columnSort = new ListHandler<Fumigation>(list);
		Comparator<Fumigation> quote = new Comparator<Fumigation>() {
			@Override
			public int compare(Fumigation e1, Fumigation e2) {
				if (e1 != null && e2 != null) {
					if (e1.getPortncountryloading() != null&& e2.getPortncountryloading() != null)
						return e1.getPortncountryloading().compareTo(e2.getPortncountryloading());
					return 0;
				} else
					return 0;
			}
		};
		columnSort.setComparator(getColumnPortOfCountry, quote);
		table.addColumnSortHandler(columnSort);
		
	}
	
	
	
	
}
