package com.slicktechnologies.client.views.fumigationdetails;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.composite.EmployeeInfoComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.MyLongBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;

public class FumigationPresenterSearchProxy extends SearchPopUpScreen<Fumigation>{

	public PersonInfoComposite personInfo;
	IntegerBox ibcountId,ibcontractId,ibserviceId;
	TextBox tbTreatmentno;
	DateBox dbissuedate,dbCreationdate;
	
	TextBox tbfromCompanyName;
	TextBox tbtoCompanyName;
	
	//****************rohan added two fields in search (suma pest control changes  )
	TextBox tbCertificateNo;
	TextBox tbShippingBillNo;
	DateComparator dateComparator;
	
	
	/**
	 * 
	 */
	ListBox lbnamefumigation;
	/**
	 * 
	 */
	ObjectListBox<Branch> olbBranch;
	
	/** date 23.6.2018 added by komal for port or loading country **/
	ObjectListBox<Config> olbportcountyloading;

	public FumigationPresenterSearchProxy() {
		super();
		createGui();
		}
	
	
	public void initWidget()
	{
		ibcountId=new IntegerBox();
		tbTreatmentno=new TextBox();
		ibcontractId=new IntegerBox();
		ibserviceId=new IntegerBox();
		dbissuedate=new DateBoxWithYearSelector();
		dbCreationdate=new DateBoxWithYearSelector();
	
		
		//****************rohan added two fields in search (suma pest control changes  )
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		
		tbCertificateNo= new TextBox();
		 tbShippingBillNo = new TextBox();
		 
		 tbfromCompanyName=new TextBox();
		 tbtoCompanyName=new TextBox();
		 
		 dateComparator=new DateComparator("dateofissue",new Fumigation());
		 
		 
		 /**
		  * 
		  */
		 
		lbnamefumigation = new ListBox();
		lbnamefumigation.addItem("--SELECT--");
		lbnamefumigation.addItem("Methyle Bromide");
		lbnamefumigation.addItem("Aluminium Phosphide");
		olbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbBranch);
			
		/** date 23.6.2018 added by komal for port or loading country **/
		olbportcountyloading= new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(olbportcountyloading, Screen.COUNTRYOFLOADING);

	}


	public void createScreen() {
		initWidget();
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("Fumigation ID",ibcountId);
		FormField ID= fbuilder.setRowSpan(0).setColSpan(0).build();	
		
		
//		fbuilder = new FormFieldBuilder("Registration No.",tbTreatmentno);
//		FormField Treatment= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Contract ID",ibcontractId);
		FormField Contract= fbuilder.setRowSpan(0).setColSpan(0).build();
		 
		fbuilder = new FormFieldBuilder("Service ID",ibserviceId);
		FormField Service= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Issue Date",dbissuedate);
		FormField servicedate= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Creation Date",dbCreationdate);
		FormField creationdate= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Certificate No",tbCertificateNo);
		FormField ftbCertificateNo= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Shipping Bill No",tbShippingBillNo);
		FormField ftbShippingBillNo= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Consigner Name",tbfromCompanyName);
		FormField ftbfromCompanyName= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Consignee Name",tbtoCompanyName);
		FormField ftbtoCompanyName= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("From Date (Issue Date)",dateComparator.getFromDate());
		FormField fdateComparatorfrom= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("To Date (Issue Date)",dateComparator.getToDate());
		FormField fdateComparatorto= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		/**
		 * 
		 */
		fbuilder = new FormFieldBuilder("Name of Fumigation",lbnamefumigation);
		FormField fumigationname= fbuilder.setMandatory(false).setMandatoryMsg("Name of Fumigation is Mandatory").setColSpan(0).build();
	//for adding on form...	
		fbuilder = new FormFieldBuilder("Branch" ,olbBranch);
		FormField folbBranch = fbuilder.setColSpan(0).setRowSpan(0).build();
		
		/** date 23.6.2018 added by komal for port or loading country **/
		fbuilder = new FormFieldBuilder("Port & Country of Loading" ,olbportcountyloading);
		FormField folbportcountyloading = fbuilder.setColSpan(0).setRowSpan(0).build();
		
		FormField[][] formfield = {
//				{FumigationInformation},
				{ID,fdateComparatorfrom,fdateComparatorto},
//				{Contract,Service,servicedate},
//				{ftbCertificateNo,ftbShippingBillNo,ftbfromCompanyName},
//				{ftbtoCompanyName,creationdate,fumigationname},
				{folbBranch , folbportcountyloading},
				{fpersonInfo},
		};
		this.fields=formfield;
		}





	@Override
	public MyQuerry getQuerry() {

		Vector<Filter> filtervec=new Vector<Filter>();
		  Filter temp=null;
		  if(AppMemory.getAppMemory().currentScreen == Screen.FUMIGATIONAUSTRALIA){
			  temp=new Filter();
			  temp.setBooleanvalue(true);
			  temp.setQuerryString("australia");
			  filtervec.add(temp);
		
		  }
		  if(AppMemory.getAppMemory().currentScreen == Screen.FUMIGATION){
			  temp=new Filter();
			  temp.setBooleanvalue(false);
			  temp.setQuerryString("australia");
			  filtervec.add(temp);
			  
			  temp=new Filter();
			  temp.setStringValue("Methyle Bromide");
			  temp.setQuerryString("nameoffumigation");
			  filtervec.add(temp);
		  }
		
		  if(AppMemory.getAppMemory().currentScreen == Screen.FUMIGATIONALP){
			  temp=new Filter();
			  temp.setBooleanvalue(false);
			  temp.setQuerryString("australia");
			  filtervec.add(temp);
			  
			  temp=new Filter();
			  temp.setStringValue("Aluminium Phosphide");
			  temp.setQuerryString("nameoffumigation");
			  filtervec.add(temp);
		  }
		  if(dateComparator.getValue()!=null)
				filtervec.addAll(dateComparator.getValue());
		  
		  if(!tbTreatmentno.getValue().equals(""))
		  {
		  temp=new Filter();
		  temp.setStringValue(tbTreatmentno.getValue().trim());
		  temp.setQuerryString("treatmentcardno");
		  filtervec.add(temp);
		  }
		
		  if(!tbfromCompanyName.getValue().equals(""))
		  {
		  temp=new Filter();
		  temp.setStringValue(tbfromCompanyName.getValue().trim());
		  temp.setQuerryString("fromCompanyname");
		  filtervec.add(temp);
		  }
		  
		  if(!tbtoCompanyName.getValue().equals(""))
		  {
		  temp=new Filter();
		  temp.setStringValue(tbtoCompanyName.getValue().trim());
		  temp.setQuerryString("toCompanyname");
		  filtervec.add(temp);
		  }
		
		  if(ibcountId.getValue()!=null)
		  {
		  temp=new Filter();
		  temp.setIntValue(ibcountId.getValue());
		  temp.setQuerryString("count");
		  filtervec.add(temp);
		  }
		  
		  if(ibcontractId.getValue()!=null)
		  {
		  temp=new Filter();
		  temp.setIntValue(ibcontractId.getValue());
		  temp.setQuerryString("contractID");
		  filtervec.add(temp);
		  }
		  
		  if(ibserviceId.getValue()!=null)
		  {
		  temp=new Filter();
		  temp.setIntValue(ibserviceId.getValue());
		  temp.setQuerryString("serviceID");
		  filtervec.add(temp);
		  }
		  
		  if(dbissuedate.getValue()!=null)
		  {
		  temp=new Filter();
		  temp.setDateValue(dbissuedate.getValue());
		  temp.setQuerryString("dateofissue");
		  filtervec.add(temp);
		  }
		  
		  if(dbCreationdate.getValue()!=null)
		  {
		  temp=new Filter();
		  temp.setDateValue(dbCreationdate.getValue());
		  temp.setQuerryString("creationDate");
		  filtervec.add(temp);
		  }
		  
		  
		  if(!tbCertificateNo.getValue().equals(""))
		  {
		  temp=new Filter();
		  temp.setStringValue(tbCertificateNo.getValue());
		  temp.setQuerryString("certificateNo");
		  filtervec.add(temp);
		  }
		  
		  if(!tbShippingBillNo.getValue().equals(""))
		  {
		  temp=new Filter();
		  temp.setStringValue(tbShippingBillNo.getValue());
		  temp.setQuerryString("shippingBillNo");
		  filtervec.add(temp);
		  }
		  
		  if(personInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(personInfo.getIdValue());
		  temp.setQuerryString("cInfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(personInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(personInfo.getFullNameValue());
		  temp.setQuerryString("cInfo.fullName");
		  filtervec.add(temp);
		  }
		  if(personInfo.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(personInfo.getCellValue());
		  temp.setQuerryString("cInfo.cellNumber");
		  filtervec.add(temp);
		  }
		  
		  /**
		   * 
		   */
		  if(lbnamefumigation.getSelectedIndex()!=0){
			  temp=new Filter();
			  temp.setStringValue(lbnamefumigation.getValue(lbnamefumigation.getSelectedIndex()));
			  temp.setQuerryString("nameoffumigation");
			  filtervec.add(temp);
		  }
		  if(olbBranch.getSelectedIndex()!=0){
			  temp=new Filter();
			  temp.setStringValue(olbBranch.getValue(olbBranch.getSelectedIndex()));
			  temp.setQuerryString("branch");
			  filtervec.add(temp);
		  }
		  /** date 23.6.2018 added by komal for port of loading **/
		  if(olbportcountyloading.getSelectedIndex()!=0){
			  temp=new Filter();
			  temp.setStringValue(olbportcountyloading.getValue(olbportcountyloading.getSelectedIndex()));
			  temp.setQuerryString("portncountryloading");
			  filtervec.add(temp);
		  }
		  MyQuerry querry= new MyQuerry();
		  querry.setFilters(filtervec);
		  querry.setQuerryObject(new Fumigation());
		  return querry;
	}


	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}
}