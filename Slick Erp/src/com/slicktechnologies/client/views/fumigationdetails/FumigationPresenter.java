package com.slicktechnologies.client.views.fumigationdetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.reusabledata.ConfigPopup;
import com.slicktechnologies.client.reusabledata.ConfigPopupForCountry;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.complain.ServiceDateBoxPopup;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.device.DeviceForm;
import com.slicktechnologies.client.views.device.DevicePresenter;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsPresenter;
import com.slicktechnologies.client.views.service.ServiceTable;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.Declaration;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class FumigationPresenter extends FormScreenPresenter<Fumigation>
		implements ClickHandler, ChangeHandler {

	public static FumigationForm form;
	CsvServiceAsync csvservice = GWT.create(CsvService.class);

	// ************changes changes here ****************
	PopupPanel panel;
	ConditionDialogBox conditionPopup = new ConditionDialogBox(
			"Do you want to print on preprinted Stationary", AppConstants.YES,
			AppConstants.NO);
	
	EmailServiceAsync emailService=GWT.create(EmailService.class);
	// ****************ends here ***********

	ConfigPopup configPopup = new ConfigPopup("add your data here");
	ConfigPopupForCountry configCountryPopup = new ConfigPopupForCountry(
			"Country", "Language");
	
	
	FumigationNewCustomerPopup custPoppu=new FumigationNewCustomerPopup();
	FumigationNewCustomerPopup custEditPoppup=new FumigationNewCustomerPopup(" ");

	final GenricServiceAsync genasync = GWT.create(GenricService.class);
	
	static ServiceTable serviceTable = null;

	 SelectTemplatePopup selectTemplatePopup = new SelectTemplatePopup();
	public FumigationPresenter(FormScreen<Fumigation> view, Fumigation model) {
		super(view, model);
		form = (FumigationForm) view;
		form.addDeclarationButton.addClickHandler(this);

		configPopup.getBtnOk().addClickHandler(this);
		configPopup.getBtnCancel().addClickHandler(this);

		configCountryPopup.getBtnOk().addClickHandler(this);
		configCountryPopup.getBtnCancel().addClickHandler(this);
		// **********rohan ************************
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);

		form.getOlbminairtemp().addChangeHandler(this);
//		form.getOlbdosagerateoffumigation().addChangeHandler(this);
//		form.getOlbdurationoffumigation().addChangeHandler(this);//comment by jayshree

		form.olbcountrydestinatoin.addChangeHandler(this);
		form.olbimportcountry.addChangeHandler(this);
	//	form.olbpointofentry.addChangeHandler(this);
		
		
	//	form.olplaceoffumigation.addClickHandler(this);
	//	form.olplaceoffumigation.addChangeHandler(this);

		form.getNewFromCustomer().addClickHandler(this);
		form.getNewToCustomer().addClickHandler(this);
		form.getAddbyCONSIGNEEComposite().addClickHandler(this);
		form.getAddbyCONSIGNERComposite().addClickHandler(this);
		
		
		form.getEditconsignee().addClickHandler(this);
		form.getEditconsigner().addClickHandler(this);
		
		
		custEditPoppup.getBtnOk().addClickHandler(this);
		custEditPoppup.getBtnCancel().addClickHandler(this);
//komal
        form.getSelectTemplateButton().addClickHandler(this);
        selectTemplatePopup.getGoButton().addClickHandler(this);
        selectTemplatePopup.getSearchTable().applySelectionModle();
        setTableSelectionOnFumigation();
	}
	
	


	public static FumigationForm initalize() {

		AppMemory.getAppMemory().currentScreen.equals(ScreeenState.NEW);
		FumigationForm form = new FumigationForm();

		FumigationPresenterTable gentableSearch = new FumigationPresenterTableProxy();
		gentableSearch.setView(form);
		gentableSearch.applySelectionModle();
		FumigationPresenterSearchProxy.staticSuperTable = gentableSearch;
		FumigationPresenterSearchProxy searchpopup = new FumigationPresenterSearchProxy();
		form.setSearchpopupscreen(searchpopup);

		FumigationPresenter presenter = new FumigationPresenter(form,new Fumigation());
		AppMemory.getAppMemory().stickPnel(form);
		return form;

	}

	public static class FumigationPresenterTable extends SuperTable<Fumigation>
			implements GeneratedVariableRefrence {

		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void createTable() {
			// TODO Auto-generated method stub

		}

		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub

		}

		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub

		}

		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub

		}
	}

	
	private void getCustomerDetails(String name)
	{
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("companyName");
		filter.setStringValue(name);
		filtervec.add(filter);
		querry.setFilters(filtervec);

		querry.setQuerryObject(new Customer());

		service.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {

						System.out.println(" result size=="
								+ result.size());
						for (SuperModel model : result) {
							Customer cust = (Customer) model;
							
//							custEditPoppup=new FumigationNewCustomerPopup(" ");
							if(cust.getCompanyName()!=null)
							{
								System.out.println("cust.getCompanyName()"+cust.getCompanyName());
								custEditPoppup.getTbcompanyName().setValue(cust.getCompanyName());
							}
							else
							{
								custEditPoppup.getTbcompanyName().setValue(" ");
							}
						
							
							if(cust.getFullname()!=null)
							{
								System.out.println("cust.getFullname()"+cust.getFullname());
								custEditPoppup.getTbPocName().setValue(cust.getFullname());
							}
							else
							{
								custEditPoppup.getTbPocName().setValue(" ");
							}
							
							if(cust.getEmail()!=null)
							{
								System.out.println("cust.getEmail()"+cust.getEmail());
								custEditPoppup.getEtbEmail().setValue(cust.getEmail());
							}
							else
							{
								custEditPoppup.getEtbEmail().setValue(" ");
							}
							
							if(cust.getCellNumber1()!=0)
							{
								System.out.println("cust.getCellNumber1()"+cust.getCellNumber1());
								custEditPoppup.getPnbLandlineNo().setValue(cust.getCellNumber1()+"");
							}
							else
							{
								custEditPoppup.getPnbLandlineNo().setValue("");
							}
							
							
							panel=new PopupPanel(true);
							panel.add(custEditPoppup);
							panel.center();
							panel.show();
							
						}
					}
				});
	}
	
	
	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		System.out.println("inside on click .........");

		
		//*****************rohan added this for customer edition **************88
		if (event.getSource().equals(custEditPoppup.getBtnCancel()))
		{
			panel.hide();
			
		}
		
		
		if (event.getSource().equals(custEditPoppup.getBtnOk()))
		{
			if(!custEditPoppup.getTbcompanyName().getValue().equals("")){
				
//				form.getFromacshipping().clear();
				
				form.tbconsignername.setValue(custEditPoppup.getTbcompanyName().getValue());
				form.setConsignerName(custEditPoppup.getTbcompanyName().getValue());
				if(!custEditPoppup.getTbPocName().getValue().equals("")){
					form.setConsignerPocName(custEditPoppup.getTbPocName().getValue());
				}
				if(!custEditPoppup.getEtbEmail().getValue().equals("")){
					form.setConsignerEmail(custEditPoppup.getEtbEmail().getValue());
				}
				if(!custEditPoppup.getPnbLandlineNo().getValue().equals("")){
					form.setConsignerCellNo(Long.parseLong(custEditPoppup.getPnbLandlineNo().getValue()));
				}
				
				panel.hide();
			}else{
				form.showDialogMessage("Please fill company name !");
			}
		}
		
		
		
		if (event.getSource().equals(form.getEditconsignee())){

			getCustomerDetails(form.getTbconsignee().getValue().trim());
			form.consigneeEditFlag=true;
		}
		
		if(event.getSource().equals(form.getEditconsigner()))
		{
			getCustomerDetails(form.getTbconsignername().getValue().trim());
			form.consignereditFlag=true;
		}
		
		
		if (event.getSource().equals(form.getAddDeclarationButton())){

			if (form.declarationMessageList.getValue() != null) {
				MyQuerry querry = new MyQuerry();
				Company c = new Company();
				Vector<Filter> filtervec = new Vector<Filter>();
				Filter filter = null;
				filter = new Filter();
				filter.setQuerryString("companyId");
				filter.setLongValue(c.getCompanyId());
				filtervec.add(filter);
				System.out
						.println("this.declarationMessageList().getValue().trim()====="
								+ form.declarationMessageList.getValue().trim());
				filter = new Filter();
				filter.setQuerryString("title");
				filter.setStringValue(form.declarationMessageList.getValue()
						.trim());
				filtervec.add(filter);
				querry.setFilters(filtervec);

				querry.setQuerryObject(new Declaration());

				service.getSearchResult(querry,
						new AsyncCallback<ArrayList<SuperModel>>() {

							@Override
							public void onFailure(Throwable caught) {
							}

							@Override
							public void onSuccess(ArrayList<SuperModel> result) {

								System.out.println(" result size=="
										+ result.size());
								for (SuperModel model : result) {
									final Declaration delEntity = (Declaration) model;

									if (delEntity.getStatus() != null) {
										form.moreComment.setValue("");
										form.moreComment.setValue(delEntity
												.getDeclaratiomMsg());
									} else {
										form.showDialogMessage("Declaration Message status is inactive");
									}
								}
							}
						});

			} else {
				form.moreComment.setValue("");
				form.showDialogMessage("Select atlest one template from template list");
			}

		}

		/**
		 *Date : 13-08-2017 By JAYSHREE
		 *
		 */
		if (event.getSource().equals(conditionPopup.getBtnOne())) {
			System.out.println("in side one yes");
			
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Fumigation", "OnlyForNBHC")
					&&model.getNameoffumigation().equalsIgnoreCase("Methyle Bromide")){
				
				final String url = GWT.getModuleBaseURL() +"pdfprintservice"+"?Id="+model.getId()+"&type="+"MethyleBromide"+ "&"+ "preprint=" + "yes";
				Window.open(url, "test", "enabled");
				panel.hide();
				
			}else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Fumigation", "OnlyForNBHC")
					&&model.getNameoffumigation().equalsIgnoreCase("Aluminium Phosphide")){
				
				final String url = GWT.getModuleBaseURL() +"pdfprintservice"+"?Id="+model.getId()+"&type="+"ALPFUM"+ "&"+ "preprint=" + "yes";
				Window.open(url, "test", "enabled");
				panel.hide();
				
			}else{
				
				final String url = GWT.getModuleBaseURL() + "FumigationPrint"+ "?Id=" + model.getId() + "&" + "type=" + "f" + "&"+ "preprint=" + "yes";
				Window.open(url, "test", "enabled");
				panel.hide();
			}
		}

		if (event.getSource().equals(conditionPopup.getBtnTwo())) {
			System.out.println("inside two no");
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Fumigation", "OnlyForNBHC")
					&&model.getNameoffumigation().equalsIgnoreCase("Methyle Bromide")){
				
				final String url = GWT.getModuleBaseURL() +"pdfprintservice"+"?Id="+model.getId()+"&type="+"MethyleBromide" + "&"+ "preprint=" + "no";
				Window.open(url, "test", "enabled");
				panel.hide();
				
			}else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Fumigation", "OnlyForNBHC")
					&&model.getNameoffumigation().equalsIgnoreCase("Aluminium Phosphide")){
				
				final String url = GWT.getModuleBaseURL() +"pdfprintservice"+"?Id="+model.getId()+"&type="+"ALPFUM" + "&"+ "preprint=" + "no";
				Window.open(url, "test", "enabled");
				panel.hide();
				
			}else{
				
				final String url = GWT.getModuleBaseURL() + "FumigationPrint"+ "?Id=" + model.getId() + "&" + "type=" + "f" + "&"+ "preprint=" + "no";
				Window.open(url, "test", "enabled");
				panel.hide();
			}
		}
		
		/**
		 * End
		 */
		
		if (event.getSource().equals(configPopup.getBtnCancel())) {

//			if (form.getOlbminairtemp().getSelectedIndex() != 0) {
//				if (form.getOlbminairtemp()
//						.getValue(form.getOlbminairtemp().getSelectedIndex())
//						.trim().equals("Other")) {
//					form.getOlbminairtemp().setSelectedIndex(0);
//				}
//			}
			/**
			 * Coment by jayshre*/
			
			

			
//			if (form.getOlbdurationoffumigation().getSelectedIndex() != 0) {
//				if (form.getOlbdurationoffumigation()
//						.getValue(
//								form.getOlbdurationoffumigation()
//										.getSelectedIndex()).trim()
//						.equals("Other")) {
//					form.getOlbdurationoffumigation().setSelectedIndex(0);
//				}
//			}
			
			

//			if (form.getOlbpointofentry().getSelectedIndex() != 0) {
//				if (form.getOlbpointofentry()
//						.getValue(form.getOlbpointofentry().getSelectedIndex())
//						.trim().equals("Other")) {
//					form.getOlbpointofentry().setSelectedIndex(0);
//				}
//			}

			configPopup.getValueBox().setValue("");
			panel.hide();
		}

		if (event.getSource().equals(configPopup.getBtnOk())) {
			if (!configPopup.getValueBox().getValue().trim().equals("")) {
				System.out.println("in side on click");
//				if (form.getOlbminairtemp().getSelectedIndex() != 0) {
//					if (form.getOlbminairtemp().getValue(form.getOlbminairtemp().getSelectedIndex()).trim().equals("Other")) {
//
//						System.out.println("rohan in side condition");
//						boolean validateFlag = false;
//
//						for (int i = 0; i < form.getOlbminairtemp().getItemCount(); i++) {
//							if (form.getOlbminairtemp().getItemText(i).trim().equalsIgnoreCase(configPopup.getValueBox().getValue().trim())) {
//								validateFlag = true;
//								break;
//							}
//						}
//
//						if (validateFlag == false) {
//							final Config con = new Config();
//
//							con.setName(configPopup.getValueBox().getValue()
//									.trim());
//							con.setStatus(true);
//							con.setType(83);
//
//							genasync.save(con,
//									new AsyncCallback<ReturnFromServer>() {
//										@Override
//										public void onFailure(Throwable caught) {
//											form.showDialogMessage("An Unexpected Error occured !");
//										}
//
//										@Override
//										public void onSuccess(
//												ReturnFromServer result) {
//											System.out.println("in side success");
//											LoginPresenter.globalConfig.add(con);	
//											initializeMethod(83, configPopup.getValueBox().getValue().trim(), 3);
//										}
//									});
//							System.out.println("before hiding.....");
//							panel.hide();
//
//						} else {
//							form.showDialogMessage("Can't add duplicate data");
//						}
//
//						configPopup.getValueBox().setValue("");
//					}
//				}

				
//				if (form.getOlplaceoffumigation().getSelectedIndex() != 0) {
//					if (form.getOlplaceoffumigation().getValue(form.getOlplaceoffumigation().getSelectedIndex()).trim().equals("Other")) {
//
//						System.out.println("rohan in side getOlplaceoffumigation condition");
//						boolean validateFlag = false;
//
//						for (int i = 0; i < form.getOlplaceoffumigation().getItemCount(); i++) {
//							if (form.getOlplaceoffumigation().getItemText(i).trim().equalsIgnoreCase(configPopup.getValueBox().getValue().trim())) {
//								validateFlag = true;
//								break;
//							}
//						}
//
//						if (validateFlag == false) {
//							final Config con = new Config();
//
//							con.setName(configPopup.getValueBox().getValue()
//									.trim());
//							con.setStatus(true);
//							con.setType(86);
//
//							genasync.save(con,
//									new AsyncCallback<ReturnFromServer>() {
//										@Override
//										public void onFailure(Throwable caught) {
//											form.showDialogMessage("An Unexpected Error occured !");
//										}
//
//										@Override
//										public void onSuccess(
//												ReturnFromServer result) {
//											System.out.println("in side success");
//											LoginPresenter.globalConfig.add(con);	
//											initializeMethod(86, configPopup.getValueBox().getValue().trim(), 4);
//										}
//									});
//							System.out.println("before hiding.....");
//							panel.hide();
//
//						} else {
//							form.showDialogMessage("Can't add duplicate data");
//						}
//
//						configPopup.getValueBox().setValue("");
//					}
//				}
				
				
				//comment by jayshree
				
//				if (form.getOlbdurationoffumigation().getSelectedIndex() != 0) {
//					if (form.getOlbdurationoffumigation().getValue(form.getOlbdurationoffumigation().getSelectedIndex()).trim().equals("Other")) {
//
//						boolean validateFlag = false;
//
//						for (int i = 0; i < form.getOlbdurationoffumigation()
//								.getItemCount(); i++) {
//							if (form.getOlbdurationoffumigation()
//									.getItemText(i)
//									.trim()
//									.equalsIgnoreCase(
//											configPopup.getValueBox()
//													.getValue().trim())) {
//								validateFlag = true;
//								break;
//							}
//						}
//
//						if (validateFlag == false) {
//
//							final Config con = new Config();
//
//							con.setName(configPopup.getValueBox().getValue()
//									.trim());
//							con.setStatus(true);
//							con.setType(82);
//
//							genasync.save(con,
//									new AsyncCallback<ReturnFromServer>() {
//
//										@Override
//										public void onFailure(Throwable caught) {
//											form.showDialogMessage("An Unexpected Error occured !");
//										}
//
//										@Override
//										public void onSuccess(
//												ReturnFromServer result) {
//
//											System.out
//													.println("in side success");
//											
//											LoginPresenter.globalConfig.add(con);	
//											initializeMethod(82, configPopup
//													.getValueBox().getValue()
//													.trim(), 2);
//										}
//
//									});
//
//							System.out.println("before hiding.....");
//							panel.hide();
//						} else {
//							form.showDialogMessage("Can't add duplicate data");
//						}
//
//						configPopup.getValueBox().setValue("");
//					}
//				}

//				if (form.getOlbdosagerateoffumigation().getSelectedIndex() != 0) {
//					if (form.getOlbdosagerateoffumigation()
//							.getValue(
//									form.getOlbdosagerateoffumigation()
//											.getSelectedIndex()).trim()
//							.equals("Other")) {
//
//						boolean validateFlag = false;
//
//						for (int i = 0; i < form.getOlbdosagerateoffumigation()
//								.getItemCount(); i++) {
//							if (form.getOlbdosagerateoffumigation()
//									.getItemText(i)
//									.trim()
//									.equalsIgnoreCase(
//											configPopup.getValueBox()
//													.getValue().trim())) {
//								validateFlag = true;
//								break;
//							}
//						}
//
//						if (validateFlag == false) {
//
//							final Config con = new Config();
//
//							con.setName(configPopup.getValueBox().getValue()
//									.trim());
//							con.setStatus(true);
//							con.setType(81);
//
//							genasync.save(con,
//									new AsyncCallback<ReturnFromServer>() {
//
//										@Override
//										public void onFailure(Throwable caught) {
//											form.showDialogMessage("An Unexpected Error occured !");
//										}
//
//										@Override
//										public void onSuccess(
//												ReturnFromServer result) {
//
//											System.out
//													.println("in side success");
//											
//											LoginPresenter.globalConfig.add(con);	
//											initializeMethod(81, configPopup
//													.getValueBox().getValue()
//													.trim(), 1);
//										}
//
//									});
//
//							System.out.println("before hiding.....");
//							panel.hide();
//
//						} else {
//							form.showDialogMessage("Can't add duplicate data");
//						}
//						configPopup.getValueBox().setValue("");
//					}
//				}

//				if (form.getOlbpointofentry().getSelectedIndex() != 0) {
//					if (form.getOlbpointofentry()
//							.getValue(
//									form.getOlbpointofentry()
//											.getSelectedIndex()).trim()
//							.equals("Other")) {
//
//						boolean validateFlag = false;
//
//						for (int i = 0; i < form.getOlbpointofentry()
//								.getItemCount(); i++) {
//							if (form.getOlbpointofentry()
//									.getItemText(i)
//									.trim()
//									.equalsIgnoreCase(
//											configPopup.getValueBox()
//													.getValue().trim())) {
//								validateFlag = true;
//								break;
//							}
//						}
//
//						if (validateFlag == false) {
//
//							final Config con = new Config();
//
//							con.setName(configPopup.getValueBox().getValue()
//									.trim());
//							con.setStatus(true);
//							con.setType(80);
//
//							genasync.save(con,
//									new AsyncCallback<ReturnFromServer>() {
//
//										@Override
//										public void onFailure(Throwable caught) {
//											form.showDialogMessage("An Unexpected Error occured !");
//										}
//
//										@Override
//										public void onSuccess(
//												ReturnFromServer result) {
//
//											System.out
//													.println("in side success");
//											LoginPresenter.globalConfig.add(con);	
//											initializeMethod(80, configPopup
//													.getValueBox().getValue()
//													.trim(), 4);
//										}
//
//									});
//
//							System.out.println("before hiding.....");
//							panel.hide();
//						} else {
//							form.showDialogMessage("Can't add duplicate data");
//						}
//
//						configPopup.getValueBox().setValue("");
//					}
//				}
			} else {
				form.showDialogMessage("Value field can not be empty");
			}
		}

		if (event.getSource().equals(configCountryPopup.getBtnOk())) {
			if (form.getOlbimportcountry().getSelectedIndex() != 0) {
				if (form.getOlbimportcountry()
						.getValue(form.getOlbimportcountry().getSelectedIndex())
						.trim().equals("Other")) {

					boolean validateFlag = false;

					for (int i = 0; i < form.getOlbimportcountry()
							.getItemCount(); i++) {
						if (form.getOlbimportcountry()
								.getItemText(i)
								.trim()
								.equalsIgnoreCase(
										configCountryPopup.getCountryName()
												.getValue().trim())) {
							validateFlag = true;
							break;
						}
					}

					if (validateFlag == false) {
						Country con = new Country();

						con.setCountryName(configCountryPopup.getCountryName()
								.getValue().trim());
						con.setLanguage(configCountryPopup.getLanguage()
								.getValue().trim());

						genasync.save(con,
								new AsyncCallback<ReturnFromServer>() {

									@Override
									public void onFailure(Throwable caught) {
										form.showDialogMessage("An Unexpected Error occured !");
									}

									@Override
									public void onSuccess(
											ReturnFromServer result) {

										initializeCountryMethod(1,
												configCountryPopup
														.getCountryName()
														.getValue().trim());
									}

								});

						System.out.println("before hiding.....");
						panel.hide();
					} else {
						form.showDialogMessage("Can't add duplicate data");
					}
					configPopup.getValueBox().setValue("");
				}
			}

			if (form.getOlbcountrydestinatoin().getValue().trim()
					.equals("Other")) {

				boolean validateFlag = false;
				for (int i = 0; i < form.getOlbimportcountry().getItemCount(); i++) {
					if (form.getOlbimportcountry().getItemText(i).trim().equalsIgnoreCase(
							configCountryPopup.getCountryName().getValue().trim())) 
					{
						validateFlag = true;
						break;
					}
				}

				if (validateFlag == false) {

					Country con = new Country();

					con.setCountryName(configCountryPopup.getCountryName()
							.getValue().trim());
					con.setLanguage(configCountryPopup.getLanguage().getValue()
							.trim());

					genasync.save(con, new AsyncCallback<ReturnFromServer>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected Error occured !");
						}

						@Override
						public void onSuccess(ReturnFromServer result) {

							initializeCountryMethod(2, configCountryPopup
									.getCountryName().getValue().trim());
						}
					});

					System.out.println("before hiding.....");
					panel.hide();
				} else {
					form.showDialogMessage("Can't add duplicate data");
				}
				configCountryPopup.getCountryName().setValue("");
				configCountryPopup.getLanguage().setValue("");
			}
		}

		if (event.getSource().equals(configCountryPopup.getBtnCancel())) {
			panel.hide();
			configPopup.getValueBox().setValue("");
		}

		
		
		if (event.getSource().equals(form.getAddbyCONSIGNEEComposite())) {
			
			MyQuerry querry = new MyQuerry();
			Vector<Filter> filterve = new Vector<Filter>();
			Filter temp = null;
			
			temp = new Filter();
//			temp.setQuerryString("countryName");
			temp.setQuerryString("companyName");
			temp.setStringValue(form.getPersonInfoCONSIGNEEComposite().getName().getValue());
			filterve.add(temp);

			System.out.println("CONSIGNEE NAME :: "+form.getPersonInfoCONSIGNEEComposite().getName().getValue());
			temp = new Filter();
			temp.setQuerryString("companyId");
			temp.setLongValue(model.getCompanyId());
			filterve.add(temp);
			querry.setFilters(filterve);
			querry.setQuerryObject(new Customer());

			service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					String address="";
				System.out.println(" CONSIGNEE result set size +++++++"+ result.size());
					if (result.size() != 0) {
						for (SuperModel model : result) {
							Customer con = (Customer) model;
//							form.getFromacshippingcomposite().setValue(con.getAdress());
							form.getTbconsignee().setValue(form.getPersonInfoCONSIGNEEComposite().getName().getValue());
//							form.getToacshippingcomposite().setValue(con.getAdress());
//							//new
//							form.setConsigneeName(form.getPersonInfoCONSIGNEEComposite().getName().getValue());
//							form.setConsigneePocName(form.getPersonInfoCONSIGNEEComposite().getTbpocName().getValue());
//							form.setConsigneeCellNo(Long.parseLong(form.getPersonInfoCONSIGNEEComposite().getPhone().getValue()));
//						    form.setconsignee	
							
							address = con.getAdress().getAddrLine1()+","+con.getAdress().getAddrLine2()
									+","+con.getAdress().getLandmark()+","+con.getAdress().getLocality()
									+","+con.getAdress().getCity()+","+con.getAdress().getState()+","+con.getAdress().getPin();
						}
//						form.getFromacshipping().setValue(address);
						form.getToacshipping().setValue(address);
						
					}
				}
			});
		}

		if (event.getSource().equals(form.getAddbyCONSIGNERComposite())) {
			MyQuerry querry = new MyQuerry();
			Vector<Filter> filterve = new Vector<Filter>();
			
			System.out.println("CONSIGNER NAME :: "+form.getPersonInfoCONSIGNERComposite().getName().getValue());
			
			Filter temp = null;
			temp = new Filter();
//			temp.setQuerryString("countryName");
			temp.setQuerryString("companyName");
			temp.setStringValue(form.getPersonInfoCONSIGNERComposite().getName().getValue());
			filterve.add(temp);

			temp = new Filter();
			temp.setQuerryString("companyId");
			temp.setLongValue(model.getCompanyId());
			filterve.add(temp);
			querry.setFilters(filterve);
			querry.setQuerryObject(new Customer());

			service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
				}
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					String consigneraddress="";
					
					System.out.println(" CONSIGNER result set size +++++++"+ result.size());
						if (result.size() != 0) {
							for (SuperModel model : result) {
								Customer con = (Customer) model;
//								form.getToacshippingcomposite().setValue(con.getAdress());
								form.getTbconsignername().setValue(form.getPersonInfoCONSIGNERComposite().getName().getValue());
//								form.getFromacshippingcomposite().setValue(con.getAdress());
								
								consigneraddress = con.getAdress().getAddrLine1()+","+con.getAdress().getAddrLine2()
										+","+con.getAdress().getLandmark()+","+con.getAdress().getLocality()
										+","+con.getAdress().getCity()+","+con.getAdress().getState()+","+con.getAdress().getPin();
							}
							
							
							form.getFromacshipping().setValue(consigneraddress);
//							form.getToacshipping().setValue(address);
						}
					}
				});

		}
		
		//////////////////////////////8888888888/////////////////////////////////////////////
		
		if(event.getSource().equals(form.getNewFromCustomer())){
			System.out.println("NEW FORM CUSTOMER!!!!");
			custPoppu=new FumigationNewCustomerPopup("Consigner");
			panel=new PopupPanel(true);
			panel.add(custPoppu);
			panel.center();
			panel.show();
			custPoppu.getBtnOk().addClickHandler(this);
			custPoppu.getBtnCancel().addClickHandler(this);
		}
		
		if(event.getSource().equals(form.getNewToCustomer())){
			System.out.println("NEW TO CUSTOMER!!!!");
			custPoppu=new FumigationNewCustomerPopup("Consignee");
			panel=new PopupPanel(true);
			panel.add(custPoppu);
			panel.center();
			panel.show();
			custPoppu.getBtnOk().addClickHandler(this);
			custPoppu.getBtnCancel().addClickHandler(this);
		}
		
		if(event.getSource()==custPoppu.getBtnOk()){
			if(custPoppu.getLabel().getText().equals("Consigner")){
				
				if(!custPoppu.getTbcompanyName().getValue().equals("")){
					
//					form.getFromacshippingcomposite().clear();
					
					form.tbconsignername.setValue(custPoppu.getTbcompanyName().getValue());
					form.setConsignerName(custPoppu.getTbcompanyName().getValue());
					if(!custPoppu.getTbPocName().getValue().equals("")){
						form.setConsignerPocName(custPoppu.getTbPocName().getValue());
					}
					if(!custPoppu.getEtbEmail().getValue().equals("")){
						form.setConsignerEmail(custPoppu.getEtbEmail().getValue());
					}
					if(!custPoppu.getPnbLandlineNo().getValue().equals("")){
						form.setConsignerCellNo(Long.parseLong(custPoppu.getPnbLandlineNo().getValue()));
					}
					panel.hide();
				}else{
					form.showDialogMessage("Please fill company name !");
				}
				
			}
			
			if(custPoppu.getLabel().getText().equals("Consignee")){
				if(!custPoppu.getTbcompanyName().getText().equals("")){
					
//					form.getToacshippingcomposite().clear();
					
					form.tbconsignee.setValue(custPoppu.getTbcompanyName().getValue());
					form.setConsigneeName(custPoppu.getTbcompanyName().getValue());
					if(!custPoppu.getTbPocName().getValue().equals("")){
						form.setConsigneePocName(custPoppu.getTbPocName().getValue());
					}
					if(!custPoppu.getEtbEmail().getValue().equals("")){
						form.setConsigneeEmail(custPoppu.getEtbEmail().getValue());
					}
					if(!custPoppu.getPnbLandlineNo().getValue().equals("")){
						form.setConsigneeCellNo(Long.parseLong(custPoppu.getPnbLandlineNo().getValue()));
					}
					
					panel.hide();
				}else{
					form.showDialogMessage("Please fill company name !");
				}
			}
		}
		
		if(event.getSource()==custPoppu.getBtnCancel()){
			panel.hide();
		}
		
		 if(event.getSource()==form.getSelectTemplateButton()){
			 selectTemplatePopup.showPopUp();
			 }
		 
			if(event.getSource()==selectTemplatePopup.getGoButton()){
				selectTemplatePopup.getSearchTable().getDataprovider().getList().clear();
				MyQuerry querry = selectTemplatePopup.getQuerry();
				GenricServiceAsync service=GWT.create(GenricService.class);
				service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						final GWTCAlert alert = new GWTCAlert();
						List<Fumigation> fList = new ArrayList<Fumigation>();
						if(result.size() >0){
							for(SuperModel s : result){
								Fumigation fumigation = (Fumigation) s;
								fList.add(fumigation);
							}
							selectTemplatePopup.getSearchTable().getDataprovider().setList(fList);
						}else{
							alert.alert("No result found.");
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				});
			}

		//////////////////////////////////////////////////////////////////////////////////////

	}
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	private void initializeCountryMethod(final int configNo, final String value) {

		System.out.println("inside country");

		MyQuerry querry = new MyQuerry();
		Vector<Filter> filterve = new Vector<Filter>();

		Filter temp = null;

		temp = new Filter();
		temp.setQuerryString("countryName");
		temp.setStringValue(value);
		filterve.add(temp);

		temp = new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(UserConfiguration.getCompanyId());
		filterve.add(temp);

		querry.setQuerryObject(new Country());

		service.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {

						List<String> namelis = new ArrayList<String>();

						System.out.println(" result set size +++++++"
								+ result.size());

						if (result.size() != 0) {
							for (SuperModel model : result) {
								Country con = (Country) model;
								if (!con.getCountryName().equals("Other")) {

									namelis.add(con.getCountryName());
									// form.olbminairtemp.addItem(con.getName());
								}
							}

							Collections.sort(namelis, new Comparator<String>() {

								@Override
								public int compare(String arg1, String arg2) {
									return arg2.compareTo(arg1);
								}

							});

							System.out.println("inside temp 3");

							form.olbimportcountry.removeAllItems();
							form.olbimportcountry.addItem("--SELECT--");
							for (int i = 0; i < namelis.size(); i++) {
								form.olbimportcountry.addItem(namelis.get(i));
							}

							form.olbimportcountry.addItem("Other");

							System.out.println("inside temp 2");

							form.olbcountrydestinatoin.removeAllItems();
							form.olbcountrydestinatoin.addItem("--SELECT--");
							for (int i = 0; i < namelis.size(); i++) {
								form.olbcountrydestinatoin.addItem(namelis
										.get(i));
							}

							form.olbcountrydestinatoin.addItem("Other");

							
							//    inserting in to global config list  data ***********/ /
							
							LoginPresenter.globalCountry.clear();
							for (int i = 0; i <= namelis.size(); i++) {
							
								Country country = new Country(); 
							if(i<namelis.size())
							{
								country.setCountryName(namelis.get(i));
							}
							else
							{
								country.setCountryName("Other");
							}
							LoginPresenter.globalCountry.add(country);
							}
							
							//    chnages ends here *****************//  
							
							if (configNo == 1) {

//								for (int i = 0; i < form.olbpointofentry
//										.getItemCount(); i++) {
//									if (form.olbpointofentry.getItemText(i)
//											.equals(value)) {
//										form.olbpointofentry
//												.setSelectedIndex(i);
//										break;
//									}
//								}
							}

//							if (configNo == 2) {
//
//								for (int i = 0; i < form.olbcountrydestinatoin
//										.getItemCount(); i++) {
//									if (form.olbcountrydestinatoin.getItemText(
//											i).equals(value)) {
//										form.olbcountrydestinatoin
//												.setSelectedIndex(i);
//										break;
//									}
//								}
//
//							}
						}
					}
				});
	}

	private void initializeMethod(int type, final String value,
			final int configNo) {

		System.out.println("inside ");

		MyQuerry querry = new MyQuerry();
		Vector<Filter> filterve = new Vector<Filter>();

		Filter temp = null;
		temp = new Filter();
		temp.setQuerryString("type");
		temp.setIntValue(type);
		filterve.add(temp);

		temp = new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(UserConfiguration.getCompanyId());
		filterve.add(temp);

		temp = new Filter();
		temp.setQuerryString("status");
		temp.setBooleanvalue(true);
		filterve.add(temp);
		querry.setFilters(filterve);

		querry.setQuerryObject(new Config());

		service.getSearchResult(querry,
				new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {

						List<String> namelis = new ArrayList<String>();

						System.out.println(" result set size +++++++"
								+ result.size());

						if (result.size() != 0) {
							for (SuperModel model : result) {
								Config con = (Config) model;
								if (!con.getName().equals("Other")){

									namelis.add(con.getName());
									// form.olbminairtemp.addItem(con.getName());
								}
							}

							for (int k = 0; k < namelis.size(); k++) {
								if (value.equals(namelis.get(k))) {

								}
							}

							Collections.sort(namelis, new Comparator<String>() {

								@Override
								public int compare(String arg1, String arg2) {
									return arg2.compareTo(arg1);
								}

							});
							
//							if (configNo == 4) {
//								System.out.println("inside temp 4");
//
//								form.olplaceoffumigation.removeAllItems();
//								form.olplaceoffumigation.addItem("--SELECT--");
//								for (int i = 0; i < namelis.size(); i++) {
//									form.olplaceoffumigation.addItem(namelis.get(i));
//								}
//
//								form.olplaceoffumigation.addItem("Other");
//
//								for (int i = 0; i < form.olplaceoffumigation
//										.getItemCount(); i++) {
//									if (form.olplaceoffumigation.getItemText(i)
//											.equals(value)) {
//										form.olplaceoffumigation.setSelectedIndex(i);
//										break;
//									}
//								}
//							}
							
							
							if (configNo == 3) {
								System.out.println("inside temp 3");

//								form.olbminairtemp.removeAllItems();
//								form.olbminairtemp.addItem("--SELECT--");
//								for (int i = 0; i < namelis.size(); i++) {
//									form.olbminairtemp.addItem(namelis.get(i));
//								}
//
//								form.olbminairtemp.addItem("Other");
//
//								for (int i = 0; i < form.olbminairtemp
//										.getItemCount(); i++) {
//									if (form.olbminairtemp.getItemText(i)
//											.equals(value)) {
//										form.olbminairtemp.setSelectedIndex(i);
//										break;
//									}
//								}

							}

							//comment by jayshree
							
//							if (configNo == 2) {
//
//								System.out.println("inside temp 2");
//
//								form.olbdurationoffumigation.removeAllItems();
//								form.olbdurationoffumigation
//										.addItem("--SELECT--");
//								for (int i = 0; i < namelis.size(); i++) {
//									form.olbdurationoffumigation
//											.addItem(namelis.get(i));
//								}
//
//								form.olbdurationoffumigation.addItem("Other");
//
//								for (int i = 0; i < form.olbdurationoffumigation
//										.getItemCount(); i++) {
//									if (form.olbdurationoffumigation
//											.getItemText(i).equals(value)) {
//										form.olbdurationoffumigation
//												.setSelectedIndex(i);
//										break;
//									}
//								}
//
//							}

							if (configNo == 1) {

//								form.olbdosagerateoffumigation.removeAllItems();
//								form.olbdosagerateoffumigation
//										.addItem("--SELECT--");
//								for (int i = 0; i < namelis.size(); i++) {
//									form.olbdosagerateoffumigation
//											.addItem(namelis.get(i));
//								}

//								form.olbdosagerateoffumigation.addItem("Other");

	//							for (int i = 0; i < form.olbdosagerateoffumigation
//										.getItemCount(); i++) {
//									if (form.olbdosagerateoffumigation
//											.getItemText(i).equals(value)) {
//										form.olbdosagerateoffumigation
//												.setSelectedIndex(i);
//										break;
//									}
//								}
							}

							if (configNo == 4) {

//								form.olbpointofentry.removeAllItems();
//								form.olbpointofentry.addItem("--SELECT--");
//								for (int i = 0; i < namelis.size(); i++) {
//									form.olbpointofentry.addItem(namelis.get(i));
//								}

//								form.olbpointofentry.addItem("Other");

//								for (int i = 0; i < form.olbpointofentry
//										.getItemCount(); i++) {
//									if (form.olbpointofentry.getItemText(i)
//											.equals(value)) {
//										form.olbpointofentry
//												.setSelectedIndex(i);
//										break;
//									}
//								}
							}

						}
					}
				});
	}

	/*
	 * 
	 * private ObjectListBox<Config> commonMethodforall(List<String>
	 * namelis,String value) { System.out.println("inside common method");
	 * ObjectListBox<Config> list = new ObjectListBox<Config>();
	 * 
	 * // form.olbminairtemp.removeAllItems(); //
	 * form.olbminairtemp.addItem("--SELECT--"); list.addItem("--SELECT--");
	 * for(int i=0;i<namelis.size();i++) { //
	 * form.olbminairtemp.addItem(namelis.get(i)); list.addItem(namelis.get(i));
	 * }
	 * 
	 * // form.olbminairtemp.addItem("Other"); list.addItem("Other");
	 * 
	 * // for(int i=0;i<form.olbminairtemp.getItemCount();i++) // { //
	 * if(form.olbminairtemp.getItemText(i).equals(value)) // { //
	 * form.olbminairtemp.setSelectedIndex(i); // break; // } // }
	 * 
	 * // for(int i=0;i<list.getItemCount();i++) // { //
	 * if(list.getItemText(i).equals(value)) // { // list.setSelectedIndex(i);
	 * // break; // } // }
	 * System.out.println("object list box size"+list.getItemCount()); return
	 * list;
	 * 
	 * }
	 */

	@Override
	public void reactOnDownload() {
		ArrayList<Fumigation> fumigationarray = new ArrayList<Fumigation>();
		List<Fumigation> list = (List<Fumigation>) form.getSearchpopupscreen()
				.getSupertable().getDataprovider().getList();

		fumigationarray.addAll(list);

		csvservice.setFumigationReport(fumigationarray,
				new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						System.out.println("RPC call Failed" + caught);

					}

					@Override
					public void onSuccess(Void result) {

						String gwt = com.google.gwt.core.client.GWT
								.getModuleBaseURL();
						final String url = gwt + "csvservlet" + "?type=" + 92;
						Window.open(url, "test", "enabled");

					}
				});
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {

		// TODO Auto-generated method stub
		InlineLabel lbl = (InlineLabel) e.getSource();
		if (lbl.getText().contains("New")) {
			if (form.isPopUpAppMenubar()) {

				serviceTable.getGenralInvoicePopup().getDocVerticalPanel()
						.add(form.content);
				FumigationPresenter fumigationPresenter = new FumigationPresenter(
						form, new Fumigation());
				serviceTable.getGenralInvoicePopup().setPresenter(
						fumigationPresenter);
				serviceTable.getGenralInvoicePopup().getPresenter().setModel(model);
				form.setPopUpAppMenubar(true);
				form.setToNewState();

			}else{
				form.setToNewState();
				//this.initalize();
			}
		}
		
		if(lbl.getText().contains("Email")){
			reactOnEmail();
		}
		
		if(lbl.getText().contains("Copy Certificate")){
			reactOnCopyPreviousCertificate();
		}
		/** added by komal to generate invoice**/
		if(lbl.getText().contains("Invoice")){
			reactOnInvoice();
		}

	}
	
	private void reactOnCopyPreviousCertificate() {
		if (form.isPopUpAppMenubar()) {

			serviceTable.getGenralInvoicePopup().getDocVerticalPanel()
					.add(form.content);
			FumigationPresenter fumigationPresenter = new FumigationPresenter(
					form, new Fumigation());
			serviceTable.getGenralInvoicePopup().setPresenter(
					fumigationPresenter);
			serviceTable.getGenralInvoicePopup().getPresenter().setModel(model);
			form.setPopUpAppMenubar(true);

		} else {
			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName(
					"Service/Fumigation", Screen.FUMIGATION);
			final FumigationForm sform = FumigationPresenter.initalize();
		}
		form.showWaitSymbol();
		Timer t = new Timer() {
		      @Override
		      public void run() {
		
		form.setToNewState();
		
		Fumigation view = model;
		
		
		if(view.getcInfo()!=null)
			form.personInfoComposite.setValue(view.getcInfo());
		
		if(view.getInvoiceDate()!=null)
			form.invoiceDate.setValue(view.getInvoiceDate());
		
		if(view.getShippingBillDate()!=null)
			form.shippingBillDate.setValue(view.getShippingBillDate());
		
		if(view.getShippingBillNo()!=null)
			form.tbShippingBillNo.setValue(view.getShippingBillNo());
		
		if(view.getMoredecl()!=null)
			form.moreComment.setValue(view.getMoredecl());
		if(view.getTreatmentcardno()!=null)
			form.tbtreatmentcardno.setValue(view.getTreatmentcardno());
		if(view.getDateofissue()!=null)
			form.dbdateofissue.setValue(view.getDateofissue());
		if(view.getImportcountry()!=null)
			form.olbimportcountry.setValue(view.getImportcountry());
		if(view.getDescriptionofgoods()!=null)
			form.tbdescriptionofgoods.setValue(view.getDescriptionofgoods());
		if(view.getQuantitydeclare()!=null)
			form.tbibQuantitydeclared.setValue(view.getQuantitydeclare());
		if(view.getDistiinguishingmarks()!=null)
			form.tbdistinguishingmarks.setValue(view.getDistiinguishingmarks());
		if(view.getConsignmentlink()!=null)
			form.tbconsignmentlink.setValue(view.getConsignmentlink());
		if(view.getInvoiceNo()!=null)
			form.tbInvoiceNo.setValue(view.getInvoiceNo());
		
		if(view.getCertificateNo()!=null)
			form.tbCertificateNo.setValue(view.getCertificateNo());
		
		if(view.getPortncountryloading()!=null)
			form.tbPortOfLoading.setValue(view.getPortncountryloading());
		if(view.getNameofvessel()!=null)
			form.tbnameofvessel.setValue(view.getNameofvessel());
		
		if(view.getQuantitydeclareUnit()!=null)
			form.olbdoquantitydeclareUnit.setValue(view.getQuantitydeclareUnit());
		
		if(view.getCountryofdestination()!=null)
			form.olbcountrydestinatoin.setValue(view.getCountryofdestination());
		
		if(view.getDeclarepointentry()!=null)
			form.tbPortOfEntry.setValue(view.getDeclarepointentry());
		if(view.getFromCompanyname()!=null)
			form.tbconsignername.setValue(view.getFromCompanyname());
		if (view.getFromaddress() != null)
//			form.fromacshippingcomposite.setValue(view.getFromaddress());
		if(view.getToCompanyname()!=null)
			form.tbconsignee.setValue(view.getToCompanyname());
		if (view.getTomaddress() != null)
//			form.toacshippingcomposite.setValue(view.getTomaddress());
		
		
		
		if(view.getNameoffumigation()!=null)
		{
			for(int i=0;i<form.lbnamefumigation.getItemCount();i++)
			{
				String data=form.lbnamefumigation.getItemText(i);
				if(data.equals(view.getNameoffumigation()))
				{
					form.lbnamefumigation.setSelectedIndex(i);
					break;
				}
			}
		}

		
		
		
	//	Console.log("date of fumigation after saving in update view ........"+view.getDateoffumigation());
		
		if (view.getDateoffumigation() != null)
			form.dbdatefumigation.setValue(view.getDateoffumigation());
		if (view.getPlaceoffumigation() != null)
			form.tbPlaceOfFumigation.setValue(view.getPlaceoffumigation());
		if (view.getDoseratefumigation() != null)
			form.tbDosageOfFumigation.setValue(view.getDoseratefumigation());
		
		//comment by jayshree
//		if (view.getDurartionfumigation() != null)
//			form.olbdurationoffumigation.setValue(view.getDurartionfumigation());
		
		if (view.getDuration() != null)
		form.tbbdurationoffumigation.setValue(view.getDuration());
		if (view.getMinairtemp() != null)
			form.olbminairtemp.setValue(view.getMinairtemp());
		if (view.getAdditionaldeclaration() != null)
			form.tacomment.setValue(view.getAdditionaldeclaration());
		
		
		if(view.getNote123()!=null)
			form.lbnote123.setTitle(view.getNote123());
		if(view.getNote1()!=null)
			form.lbnote1.setTitle(view.getNote1());
		if(view.getNote2()!=null)
			form.lbnote2.setTitle(view.getNote2());
		if(view.getNote3()!=null)
			form.lbnote3.setTitle(view.getNote3());
		if(view.getNote4()!=null)
			form.lbnote4.setTitle(view.getNote4());
		if(view.getNote11()!=null)
			form.lbnote11.setTitle(view.getNote11());
		if(view.getNote12()!=null)
			form.lbnote12.setTitle(view.getNote12());
		if(view.getNote13()!=null)
			form.lbnote13.setTitle(view.getNote13());
		if(view.getNote14()!=null)
			form.lbnote14.setTitle(view.getNote14());
		if(view.getNote15()!=null)
			form.lbnote15.setTitle(view.getNote15());
		
		if(view.getNote123()!=null)
		{
			for(int i=0;i<form.lbnote123.getItemCount();i++)
			{
				String data=form.lbnote123.getItemText(i);
				if(data.equals(view.getNote123()))
				{
					form.lbnote123.setSelectedIndex(i);
					break;
				}
			}
		}
		
		
		if(view.getNote1()!=null)
		{
			for(int i=0;i<form.lbnote1.getItemCount();i++)
			{
				String data=form.lbnote1.getItemText(i);
				if(data.equals(view.getNote1()))
				{
					form.lbnote1.setSelectedIndex(i);
					break;
				}
			}
		}
		if(view.getNote2()!=null)
		{
			for(int i=0;i<form.lbnote2.getItemCount();i++)
			{
				String data=form.lbnote2.getItemText(i);
				if(data.equals(view.getNote2()))
				{
					form.lbnote2.setSelectedIndex(i);
					break;
				}
			}
		}
		if(view.getNote3()!=null)
		{
			for(int i=0;i<form.lbnote3.getItemCount();i++)
			{
				String data=form.lbnote3.getItemText(i);
				if(data.equals(view.getNote3()))
				{
					form.lbnote3.setSelectedIndex(i);
					break;
				}
			}
		}
		if(view.getNote4()!=null)
		{
			for(int i=0;i<form.lbnote4.getItemCount();i++)
			{
				String data=form.lbnote4.getItemText(i);
				if(data.equals(view.getNote4()))
				{
					form.lbnote4.setSelectedIndex(i);
					break;
				}
			}
		}
		if(view.getNote11()!=null)
		{
			for(int i=0;i<form.lbnote11.getItemCount();i++)
			{
				String data=form.lbnote11.getItemText(i);
				if(data.equals(view.getNote11()))
				{
					form.lbnote11.setSelectedIndex(i);
					break;
				}
			}
		}
		if(view.getNote12()!=null)
		{
			for(int i=0;i<form.lbnote12.getItemCount();i++)
			{
				String data=form.lbnote12.getItemText(i);
				if(data.equals(view.getNote12()))
				{
					form.lbnote12.setSelectedIndex(i);
					break;
				}
			}
		}
		if(view.getNote13()!=null)
		{
			for(int i=0;i<form.lbnote13.getItemCount();i++)
			{
				String data=form.lbnote13.getItemText(i);
				if(data.equals(view.getNote13()))
				{
					form.lbnote13.setSelectedIndex(i);
					break;
				}
			}
		}
		if(view.getNote14()!=null)
		{
			for(int i=0;i<form.lbnote14.getItemCount();i++)
			{
				String data=form.lbnote14.getItemText(i);
				if(data.equals(view.getNote14()))
				{
					form.lbnote14.setSelectedIndex(i);
					break;
				}
			}
		}
		if(view.getNote15()!=null)
		{
			for(int i=0;i<form.lbnote15.getItemCount();i++)
			{
				String data=form.lbnote15.getItemText(i);
				if(data.equals(view.getNote15()))
				{
					form.lbnote15.setSelectedIndex(i);
					break;
				}
			}
		}
		
		
		
		
		
	//	System.out.println("note1===="+view.getNote1());
		
		
		if (view.getPdate() != null)
			form.dbdateadd.setValue(view.getPdate());
		if (view.getPlace() != null)
			form.tbplace.setValue(view.getPlace());
		
//		if (view.getContractID() != null)
//			form.tbcontractId.setValue(view.getContractID()+"");
//		if (view.getServiceID() != null)
//			form.tbserviceId.setValue(view.getServiceID()+"");
//		if (view.getContractStartdate() != null)
//			form.dbcontractstartdate.setValue(view.getContractStartdate());
//		if (view.getContractEnddate()!= null)
//			form.dbcontractenddate.setValue(view.getContractEnddate());
//		if (view.getServiceDate()!= null)
//			form.dbservicedate.setValue(view.getServiceDate());
		
		
//		if (view.getGrossWeight()!= null)
//			form.grossWeight.setValue(view.getGrossWeight());
//		
//		if (view.getTotalNetWeight()!= null)
//			form.totalNetWeight.setValue(view.getTotalNetWeight());
//		
//		if (view.getVolumeofcontainer()!= null)
//			form.volumeOfContainer.setValue(view.getVolumeofcontainer());
		if(view.getDescriptionofgoods()!=null)
			form.taDescrioptionOfGoods.setValue(view.getDescriptionofgoods());

		if (view.getPackingValue()!= null)
			form.packing.setValue(view.getPackingValue());
		if(view.getSerialNumber()!=0){
			form.tbSerialNumber.setValue(view.getSerialNumber());
		}
		if(view.getDtePPQSRegistrationNumber()!=null){
			form.tbDtePPQSRegistrationNumber.setValue(view.getDtePPQSRegistrationNumber());
		}
		if(view.getImportOrExport()!=null){
			form.olbImportOrExport.setValue(view.getImportOrExport());
		}
		if(view.getBranch()!=null){
			form.olbBranch.setValue(view.getBranch());
		}
		if(view.getNameOfCommodity()!=null){
			form.tbNameOfCommodity.setValue(view.getNameOfCommodity());;
		}
		if(view.getQuantity()!=null){
			form.ibQuantity.setValue(view.getQuantity());
		}
		form.hideWaitSymbol();
		}
	};
		      
		 t.schedule(2000);
	}




	@Override
	protected void makeNewModel() {

		model = new Fumigation();

	}


	@Override
	public void reactOnPrint() {
		/**
		 * Date : 13-09-2017 By JAYSHREE
		 * For Printing Methyle Bromide and Aluminium Phosphide PDF for NBHC
		 */
		if(model!=null){
			System.out.println("hiiiii"+model.getNameoffumigation());
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Fumigation", "OnlyForNBHC")
					&&model.getNameoffumigation().trim().equalsIgnoreCase("Methyle Bromide")){
				final String url = GWT.getModuleBaseURL() +"pdfprintservice"+"?Id="+model.getId()+"&type="+"MethyleBromide";
				Window.open(url, "test", "enabled");
				
			}else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Fumigation", "OnlyForNBHC")
					&&model.getNameoffumigation().trim().equalsIgnoreCase("Aluminium Phosphide")){
				
				final String url = GWT.getModuleBaseURL() +"pdfprintservice"+"?Id="+model.getId()+"&type="+"ALPFUM";
				Window.open(url, "test", "enabled");
				
			}
			/***
			 * Date 10-4-2018
			 * By Jayshree
			 * to call the service certificate
			 */
			else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Fumigation", "FumigationCertificate")){
				final String url = GWT.getModuleBaseURL() +"FumigationCertificatePrint"+"?Id="+model.getId()+"&type="+"MB";
				Window.open(url, "test", "enabled");
				
//				final String url = GWT.getModuleBaseURL() +"FumigationCertificatePrint"+"?Id="+model.getId()+"&type="+"ALP";
//				Window.open(url, "test", "enabled");
			}else{
				int cnt = 0;
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Fumigation", "CompanyAsLetterHead")){
					cnt = cnt + 1;
				}else{
					final String url = GWT.getModuleBaseURL()+ "FumigationPrint" + "?Id="+ model.getId() + "&" + "type=" + "f" + "&"+ "preprint=" + "plane";
					Window.open(url, "test", "enabled");
				}
				
				if (cnt > 0) {
					System.out.println("in side react on prinnt");
					panel = new PopupPanel(true);
					panel.add(conditionPopup);
					panel.setGlassEnabled(true);
					panel.show();
					panel.center();
				} 
			}
		}
		/**
		 * End
		 */
		
		/**
		 * Date : 13-09-2017 By ANIL
		 * Old Code commented 
		 */
		
//		// final String url = GWT.getModuleBaseURL() +
//		// "FumigationPrint"+"?Id="+model.getId()+"&"+"type="+"f";
//		// Window.open(url, "test", "enabled");
//
//		// *************rohan changes ********************
//
//		MyQuerry querry = new MyQuerry();
//		Company c = new Company();
//		Vector<Filter> filtervec = new Vector<Filter>();
//		Filter filter = null;
//		filter = new Filter();
//		filter.setQuerryString("companyId");
//		filter.setLongValue(c.getCompanyId());
//		filtervec.add(filter);
//		filter = new Filter();
//		filter.setQuerryString("processName");
//		filter.setStringValue("Fumigation");
//		filtervec.add(filter);
//
//		filter = new Filter();
//		filter.setQuerryString("processList.status");
//		filter.setBooleanvalue(true);
//		filtervec.add(filter);
//
//		querry.setFilters(filtervec);
//		querry.setQuerryObject(new ProcessConfiguration());
//
//		service.getSearchResult(querry,
//				new AsyncCallback<ArrayList<SuperModel>>() {
//
//					@Override
//					public void onFailure(Throwable caught) {
//					}
//
//					@Override
//					public void onSuccess(ArrayList<SuperModel> result) {
//						System.out.println(" result set size +++++++"
//								+ result.size());
//
//						List<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();
//						int cnt = 0;
//						if (result.size() == 0) {
//
//							// final String url = GWT.getModuleBaseURL() +
//							// "pdfservice"+"?Id="+model.getId()+"&"+"type="+"q"+"&"+"preprint="+"plane";
//							// Window.open(url, "test", "enabled");
//
//							final String url = GWT.getModuleBaseURL()
//									+ "FumigationPrint" + "?Id="
//									+ model.getId() + "&" + "type=" + "f" + "&"
//									+ "preprint=" + "plane";
//							Window.open(url, "test", "enabled");
//						} else {
//
//							for (SuperModel model : result) {
//								ProcessConfiguration processConfig = (ProcessConfiguration) model;
//								processList.addAll(processConfig
//										.getProcessList());
//
//							}
//
//							for (int k = 0; k < processList.size(); k++) {
//								if (processList
//										.get(k)
//										.getProcessType()
//										.trim()
//										.equalsIgnoreCase("CompanyAsLetterHead")
//										&& processList.get(k).isStatus() == true) {
//
//									cnt = cnt + 1;
//
//								}
//							}
//							if (cnt > 0) {
//								System.out.println("in side react on prinnt");
//								panel = new PopupPanel(true);
//								panel.add(conditionPopup);
//								panel.setGlassEnabled(true);
//								panel.show();
//								panel.center();
//							} else {
//								final String url = GWT.getModuleBaseURL()
//										+ "FumigationPrint" + "?Id="
//										+ model.getId() + "&" + "type=" + "f"
//										+ "&" + "preprint=" + "plane";
//								Window.open(url, "test", "enabled");
//							}
//						}
//					}
//				});
//		// **************************changes ends here
//		// ********************************
//		
//		
	}

	@Override
	public void reactOnEmail() {
		
		boolean conf = Window.confirm("Do you really want to send email?");
		if (conf == true) {
		emailService.initiateFumigationEmail(model,new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Unable To Send Email");
				caught.printStackTrace();
			}

			@Override
			public void onSuccess(Void result) {
				Window.alert("Email Sent Sucessfully !");
			}
		});
		}
	}

	@Override
	public void onChange(ChangeEvent event) {

		System.out.println("asasasdasdasdasdasdsaasdssadasdsaassadsadsad");
		if (event.getSource().equals(form.getOlbminairtemp())) {
//			if (form.getOlbminairtemp().getSelectedIndex() != 0) {
//				if (form.getOlbminairtemp().getValue(form.getOlbminairtemp().getSelectedIndex()).trim().equals("Other")) {
//
//					panel = new PopupPanel(true);
//					panel.add(configPopup);
//					panel.show();
//					panel.center();
//				}
//			}
		}

//		if (event.getSource().equals(form.getOlbdosagerateoffumigation())) {
//			if (form.getOlbdosagerateoffumigation().getSelectedIndex() != 0) {
//				if (form.getOlbdosagerateoffumigation()
//						.getValue(
//								form.getOlbdosagerateoffumigation()
//										.getSelectedIndex()).trim()
//						.equals("Other")) {
//
//					panel = new PopupPanel(true);
//					panel.add(configPopup);
//					panel.show();
//					panel.center();
//				}
//			}
//		}

		//comment by jayshree
		
//		if (event.getSource().equals(form.getOlbdurationoffumigation())) {
//			if (form.getOlbdurationoffumigation().getSelectedIndex() != 0) {
//				if (form.getOlbdurationoffumigation()
//						.getValue(
//								form.getOlbdurationoffumigation()
//										.getSelectedIndex()).trim()
//						.equals("Other")) {
//
//					panel = new PopupPanel(true);
//					panel.add(configPopup);
//					panel.show();
//					panel.center();
//				}
//			}
//		}

//		if (event.getSource().equals(form.getOlbpointofentry())) {
//		if (form.getOlbpointofentry().getSelectedIndex() != 0) {
//			if (form.getOlbpointofentry()
//					.getValue(form.getOlbpointofentry().getSelectedIndex())
//					.trim().equals("Other")) {
//
//				panel = new PopupPanel(true);
//				panel.add(configPopup);
//				panel.show();
//				panel.center();
//			}
//		}
//		}
		
		
//		if (event.getSource().equals(form.getOlplaceoffumigation())) {
//			if (form.getOlplaceoffumigation().getSelectedIndex() != 0) {
//				if (form.getOlplaceoffumigation()
//						.getValue(form.getOlplaceoffumigation().getSelectedIndex())
//						.trim().equals("Other")) {
//
//					panel = new PopupPanel(true);
//					panel.add(configPopup);
//					panel.show();
//					panel.center();
//				}
//			}
//			}
		
		
		if (event.getSource().equals(form.getOlbimportcountry())) {
		if (form.getOlbimportcountry().getSelectedIndex() != 0) {
			
//			form.getOlbcountrydestinatoin().setSelectedIndex(form.getOlbimportcountry().getSelectedIndex());
			
			if (form.getOlbimportcountry()
					.getValue(form.getOlbimportcountry().getSelectedIndex())
					.trim().equals("Other")) {

				panel = new PopupPanel(true);
				panel.add(configCountryPopup);
				panel.show();
				panel.center();
			}
		}
		}

		if (event.getSource().equals(form.getOlbcountrydestinatoin())) {
		if (form.getOlbcountrydestinatoin().getSelectedIndex() != 0) {
			
			
//			form.getOlbimportcountry().setSelectedIndex(form.getOlbcountrydestinatoin().getSelectedIndex());
			
			if (form.getOlbcountrydestinatoin().getValue(form.getOlbcountrydestinatoin().getSelectedIndex()).trim().equals("Other")) {

				panel = new PopupPanel(true);
				panel.add(configCountryPopup);
				panel.show();
				panel.center();
			}
		}

	}
	}
private void reactOnInvoice(){
	GeneralServiceAsync genservice = GWT.create(GeneralService.class);
	Fumigation view = model;
	genservice.generateInvoiceForFumigation(UserConfiguration.getCompanyId(), view, new AsyncCallback<Invoice>() {

		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onSuccess(final Invoice result) {
			// TODO Auto-generated method stub
			final InvoiceDetailsForm form1;
			if (form.isPopUpAppMenubar()) {
				form1 = new InvoiceDetailsForm();
				serviceTable.getGenralInvoicePopup().getDocVerticalPanel()
						.add(form1.content);
				InvoiceDetailsPresenter invoiceDetailsPresenter = new InvoiceDetailsPresenter(
						form1, new Invoice());
				serviceTable.getGenralInvoicePopup().setPresenter(
						invoiceDetailsPresenter);
				serviceTable.getGenralInvoicePopup().getPresenter().setModel(model);
				form1.setPopUpAppMenubar(true);

			}else{
			AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Invoice Details",Screen.INVOICEDETAILS);
			form1 =InvoiceDetailsPresenter.initalize();
			}
			//form.showWaitSymbol();
			Timer t = new Timer() {
			      @Override
			      public void run() {
			
			form1.setToNewState();
			form1.updateView(result);
			}
			}; t.schedule(2000);


		}
	});
	
	}
//komal
public static void setObject(SuperTable s ) {
	FumigationForm form = new FumigationForm();
    if(s instanceof ServiceTable){
    ServiceTable sTable = (ServiceTable) s;
    serviceTable = sTable;
    }
}
private void setTableSelectionOnFumigation()
{
	 final NoSelectionModel<Fumigation> selectionModelMyObj = new NoSelectionModel<Fumigation>();
    
    SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
    {
        @Override
        public void onSelectionChange(SelectionChangeEvent event) 
        {
       	 Fumigation entity=selectionModelMyObj.getLastSelectedObject();
       	 selectTemplatePopup.hidePopUp();   
       
		if(entity.getInvoiceDate()!=null)
			form.invoiceDate.setValue(entity.getInvoiceDate());
		
		if(entity.getShippingBillDate()!=null)
			form.shippingBillDate.setValue(entity.getShippingBillDate());
		
		if(entity.getShippingBillNo()!=null)
			form.tbShippingBillNo.setValue(entity.getShippingBillNo());
		
		if(entity.getMoredecl()!=null)
			form.moreComment.setValue(entity.getMoredecl());
		if(entity.getTreatmentcardno()!=null)
			form.tbtreatmentcardno.setValue(entity.getTreatmentcardno());
//		if(entity.getDateofissue()!=null)
//			form.dbdateofissue.setValue(entity.getDateofissue());
//		if(entity.getImportcountry()!=null)
//			form.olbimportcountry.setValue(entity.getImportcountry());
		if(entity.getDescriptionofgoods()!=null)
			form.tbdescriptionofgoods.setValue(entity.getDescriptionofgoods());
		if(entity.getQuantitydeclare()!=null)
			form.tbibQuantitydeclared.setValue(entity.getQuantitydeclare());
		if(entity.getDistiinguishingmarks()!=null)
			form.tbdistinguishingmarks.setValue(entity.getDistiinguishingmarks());
		if(entity.getConsignmentlink()!=null)
			form.tbconsignmentlink.setValue(entity.getConsignmentlink());
		if(entity.getInvoiceNo()!=null)
			form.tbInvoiceNo.setValue(entity.getInvoiceNo());
		
//		if(entity.getCertificateNo()!=null)
//			form.tbCertificateNo.setValue(entity.getCertificateNo());
		
		if(entity.getPortncountryloading()!=null)
			form.tbPortOfLoading.setValue(entity.getPortncountryloading());
		if(entity.getNameofvessel()!=null)
			form.tbnameofvessel.setValue(entity.getNameofvessel());
		
		if(entity.getQuantitydeclareUnit()!=null)
			form.olbdoquantitydeclareUnit.setValue(entity.getQuantitydeclareUnit());
		
		if(entity.getCountryofdestination()!=null)
			form.olbcountrydestinatoin.setValue(entity.getCountryofdestination());
		
		if(entity.getDeclarepointentry()!=null)
			form.tbPortOfEntry.setValue(entity.getDeclarepointentry());
		if(entity.getFromCompanyname()!=null)
			form.tbconsignername.setValue(entity.getFromCompanyname());
		if (entity.getFromaddress() != null)
//			form.fromacshippingcomposite.setValue(entity.getFromaddress());
		if(entity.getToCompanyname()!=null)
			form.tbconsignee.setValue(entity.getToCompanyname());
		if (entity.getTomaddress() != null)
//			form.toacshippingcomposite.setValue(entity.getTomaddress());
		
		
		
		if(entity.getNameoffumigation()!=null)
		{
			for(int i=0;i<form.lbnamefumigation.getItemCount();i++)
			{
				String data=form.lbnamefumigation.getItemText(i);
				if(data.equals(entity.getNameoffumigation()))
				{
					form.lbnamefumigation.setSelectedIndex(i);
					break;
				}
			}
		}

		
		
		
	//	Console.log("date of fumigation after saving in update entity ........"+entity.getDateoffumigation());
		
		if (entity.getDateoffumigation() != null)
			form.dbdatefumigation.setValue(entity.getDateoffumigation());
		if (entity.getPlaceoffumigation() != null)
			form.tbPlaceOfFumigation.setValue(entity.getPlaceoffumigation());
		if (entity.getDoseratefumigation() != null)
			form.tbDosageOfFumigation.setValue(entity.getDoseratefumigation());
		
		//comment by jayshree
		
//		if (entity.getDurartionfumigation() != null)
//			form.olbdurationoffumigation.setValue(entity.getDurartionfumigation());
		
		if (entity.getDuration() != null)
			form.tbbdurationoffumigation.setValue(entity.getDuration());
		if (entity.getMinairtemp() != null)
			form.olbminairtemp.setValue(entity.getMinairtemp());
		if (entity.getAdditionaldeclaration() != null)
			form.tacomment.setValue(entity.getAdditionaldeclaration());
		
		
		if(entity.getNote123()!=null)
			form.lbnote123.setTitle(entity.getNote123());
		if(entity.getNote1()!=null)
			form.lbnote1.setTitle(entity.getNote1());
		if(entity.getNote2()!=null)
			form.lbnote2.setTitle(entity.getNote2());
		if(entity.getNote3()!=null)
			form.lbnote3.setTitle(entity.getNote3());
		if(entity.getNote4()!=null)
			form.lbnote4.setTitle(entity.getNote4());
		if(entity.getNote11()!=null)
			form.lbnote11.setTitle(entity.getNote11());
		if(entity.getNote12()!=null)
			form.lbnote12.setTitle(entity.getNote12());
		if(entity.getNote13()!=null)
			form.lbnote13.setTitle(entity.getNote13());
		if(entity.getNote14()!=null)
			form.lbnote14.setTitle(entity.getNote14());
		if(entity.getNote15()!=null)
			form.lbnote15.setTitle(entity.getNote15());
		
		if(entity.getNote123()!=null)
		{
			for(int i=0;i<form.lbnote123.getItemCount();i++)
			{
				String data=form.lbnote123.getItemText(i);
				if(data.equals(entity.getNote123()))
				{
					form.lbnote123.setSelectedIndex(i);
					break;
				}
			}
		}
		
		
		if(entity.getNote1()!=null)
		{
			for(int i=0;i<form.lbnote1.getItemCount();i++)
			{
				String data=form.lbnote1.getItemText(i);
				if(data.equals(entity.getNote1()))
				{
					form.lbnote1.setSelectedIndex(i);
					break;
				}
			}
		}
		if(entity.getNote2()!=null)
		{
			for(int i=0;i<form.lbnote2.getItemCount();i++)
			{
				String data=form.lbnote2.getItemText(i);
				if(data.equals(entity.getNote2()))
				{
					form.lbnote2.setSelectedIndex(i);
					break;
				}
			}
		}
		if(entity.getNote3()!=null)
		{
			for(int i=0;i<form.lbnote3.getItemCount();i++)
			{
				String data=form.lbnote3.getItemText(i);
				if(data.equals(entity.getNote3()))
				{
					form.lbnote3.setSelectedIndex(i);
					break;
				}
			}
		}
		if(entity.getNote4()!=null)
		{
			for(int i=0;i<form.lbnote4.getItemCount();i++)
			{
				String data=form.lbnote4.getItemText(i);
				if(data.equals(entity.getNote4()))
				{
					form.lbnote4.setSelectedIndex(i);
					break;
				}
			}
		}
		if(entity.getNote11()!=null)
		{
			for(int i=0;i<form.lbnote11.getItemCount();i++)
			{
				String data=form.lbnote11.getItemText(i);
				if(data.equals(entity.getNote11()))
				{
					form.lbnote11.setSelectedIndex(i);
					break;
				}
			}
		}
		if(entity.getNote12()!=null)
		{
			for(int i=0;i<form.lbnote12.getItemCount();i++)
			{
				String data=form.lbnote12.getItemText(i);
				if(data.equals(entity.getNote12()))
				{
					form.lbnote12.setSelectedIndex(i);
					break;
				}
			}
		}
		if(entity.getNote13()!=null)
		{
			for(int i=0;i<form.lbnote13.getItemCount();i++)
			{
				String data=form.lbnote13.getItemText(i);
				if(data.equals(entity.getNote13()))
				{
					form.lbnote13.setSelectedIndex(i);
					break;
				}
			}
		}
		if(entity.getNote14()!=null)
		{
			for(int i=0;i<form.lbnote14.getItemCount();i++)
			{
				String data=form.lbnote14.getItemText(i);
				if(data.equals(entity.getNote14()))
				{
					form.lbnote14.setSelectedIndex(i);
					break;
				}
			}
		}
		if(entity.getNote15()!=null)
		{
			for(int i=0;i<form.lbnote15.getItemCount();i++)
			{
				String data=form.lbnote15.getItemText(i);
				if(data.equals(entity.getNote15()))
				{
					form.lbnote15.setSelectedIndex(i);
					break;
				}
			}
		}
			if (entity.getPdate() != null)
			form.dbdateadd.setValue(entity.getPdate());
		if (entity.getPlace() != null)
			form.tbplace.setValue(entity.getPlace());
		
		if(entity.getDescriptionofgoods()!=null)
			form.taDescrioptionOfGoods.setValue(entity.getDescriptionofgoods());

		if (entity.getPackingValue()!= null)
			form.packing.setValue(entity.getPackingValue());
//		if(entity.getSerialNumber()!=0){
//			form.tbSerialNumber.setValue(entity.getSerialNumber());
//		}
//		if(entity.getDtePPQSRegistrationNumber()!=null){
//			form.tbDtePPQSRegistrationNumber.setValue(entity.getDtePPQSRegistrationNumber());
//		}
//		if(entity.getImportOrExport()!=null){
//			form.olbImportOrExport.setValue(entity.getImportOrExport());
//		}
//		if(entity.getBranch()!=null){
//			form.olbBranch.setValue(entity.getBranch());
//		}
		if(entity.getNameOfCommodity()!=null){
			form.tbNameOfCommodity.setValue(entity.getNameOfCommodity());;
		}
		if(entity.getQuantity()!=null){
			form.ibQuantity.setValue(entity.getQuantity());
		}
		//form.hideWaitSymbol();
		}
	

         //}
    };
    // Add the handler to the selection model
    selectionModelMyObj.addSelectionChangeHandler( tableHandler );
    // Add the selection model to the table

    	selectTemplatePopup.getSearchTable().getTable().setSelectionModel(selectionModelMyObj);

}

}
