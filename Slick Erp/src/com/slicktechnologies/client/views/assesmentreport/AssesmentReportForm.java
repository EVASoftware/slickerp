package com.slicktechnologies.client.views.assesmentreport;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.EmailTextBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.client.library.mywidgets.PhoneNumberBox;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.customerbranchdetails.servicelocation.Area;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.AssessmentReportService;
import com.slicktechnologies.shared.common.AssessmentReportUnit;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchServiceLocation;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class AssesmentReportForm extends FormScreen<AssesmentReport> implements ChangeHandler , ClickHandler{

	
	//  customer details
	PersonInfoComposite personInfoComposite;
	TextBox tbFullName, tbCompanyName;
	PhoneNumberBox pnbCellNo;
	EmailTextBox etbEmail;
	AddressComposite customerAddressComposite;
	boolean customerEditFlag=false;
	//  table details 
	TextBox assessmentDetailsCount;
	DateBox dbAssessmentDate;
//	TextBox tbArea;
	ObjectListBox<ConfigCategory> olbCategory;
	ObjectListBox<Type> olbDeficiencyType;
	TextBox tbstatus;

	AssesmentReport assesreportobj;
	
	FormField fgroupingCustomerInformation;
	AssessmentReportTable assessmentreportTable;
	Button addDetails;
	//  assessed by details 
	
	ObjectListBox<Employee> olbAssessedBy1,olbAssessedBy2,olbAssessedBy3;
	
	
	//   old code 
//	ObjectListBox<Employee> olbAccompainedBy1,olbAccompainedBy2,olbAccompainedBy3;
//	ObjectListBox<Config> olbActionForCompany;
//	ObjectListBox<Config> olbActionPlanforCustomer;
	
//	new code by roha as per Vaishali m'am requirement Date : 21/3/2017
	
	TextBox olbAccompainedBy1,olbAccompainedBy2,olbAccompainedBy3;
	
//  rohan added this upload documnet for NBHC
	UploadComposite ucUploadTAndCs;
	
	
	/****Date :20/11/2017 
    BY :Manisha
    Description : Incase of inspection quotation there should be some schedules for visiting person,
             create single service for assessment single service.**/

    AssessmentReportServiceTable assessmentreportservicetable;
    ProductInfoComposite prodInfoComposite;

    ObjectListBox<Employee> olbemployeeName;
    Button btn_addproducts;
    DateBox dbServiceDate;
    ObjectListBox<Branch> olbbBranch,olbbAssementBranch;
    
    Button btnNewCustomer;
	
    /**Ends**/
    
    
    /**
     * Date 11-07-2018 By Vijay
     * For NBHC CCPM Unit depedent for Deficiency
     */
    ObjectListBox<AssessmentReportUnit>olbDeficiencyUnit;
	ArrayList<AssessmentReportUnit> DefUnitlist = new ArrayList<AssessmentReportUnit>();
	final static GenricServiceAsync async=GWT.create(GenricService.class);

	/**
	 * ends here
	 */
	
	/**
	 * Date 23-05-2019 by Vijay
	 * Des :- NBHC CCPM complaint and service id must having one assessment so displaying service and complaint ID
	 */
	TextBox ibServiceId,ibComplaintId;
	
	TextBox ibContractId; //Ashwini patil Date: 1-06-2022 
	UploadComposite customerSignatureUpload;
	
	

	/**
	 * @author Anil , Date : 13-10-2021
	 * Added customer branch drop down list
	 * raised by Rahul Tiwari for Innovative
	 */
	ObjectListBox<CustomerBranchDetails> oblCustomerBranch;
	
	List<CustomerBranchDetails> customerBranchList=new ArrayList<CustomerBranchDetails>();
	
	TextArea olbActionForCompany,olbActionPlanforCustomer,tbLocaltion,tbConsequences;
	
//	TextBox tbRiskLevel;
	/**
	 *@author Anil @since 15-12-2021
	 *Capturing status, date and remark for task
	 *Raised by Nitin for Prominant
	 */
	UploadComposite uploadFile;
	ObjectListBox<Config> tbRiskLevel;
	int productSrNo;
	
	/**
	 * @author Anil @since 05-01-2022
	 * Adding service location drop down
	 * and making area textbox to listbox
	 * raised by Nitin Sir audit app
	 */
//	ListBox tbArea;
	ObjectListBox<CustomerBranchServiceLocation> oblServiceLocation;
	
	
	UploadComposite ucuploadimage1,ucuploadimage2,ucuploadimage3,ucuploadimage4,ucuploadimage5,ucuploadimage6,
					ucuploadimage7,ucuploadimage8,ucuploadimage9,ucuploadimage10;

	TextBox image1Label,image2Label,image3Label,image4Label,image5Label,image6Label,image7Label,image8Label,image9Label,image10Label;
	
	CheckBox cbheaderrepetative, cbDoNotPrintstatusCreationDate;
	TextBox tbAttentionName;
	
		ObjectListBox<CustomerBranchServiceLocation> oblArea;

	GeneralViewDocumentPopup generalviewdocumentpopup = new GeneralViewDocumentPopup(true);
	public PopupPanel generalPanel;
	
		
	AreaPoup areapopup = new AreaPoup();
	final GenricServiceAsync genasync=GWT.create(GenricService.class);

	public AssesmentReportForm() {
		super();
		createGui();
		generalviewdocumentpopup.getBtnClose().addClickHandler(this);
		areapopup.getLblOk().addClickHandler(this);
		areapopup.getLblCancel().addClickHandler(this);
	}

	public AssesmentReportForm(String[] processlevel, FormField[][] fields,
			FormStyle formstyle) {
		super(processlevel, fields, formstyle);
		createGui();
		generalviewdocumentpopup.getBtnClose().addClickHandler(this);
		areapopup.getLblOk().addClickHandler(this);
		areapopup.getLblCancel().addClickHandler(this);

	}

	private void initalizeWidget()
	{
	//  customer details
		MyQuerry custquerry1 = new MyQuerry();
		custquerry1.setQuerryObject(new Customer());
		personInfoComposite = new PersonInfoComposite(custquerry1, false,true);
		
		/****Date :20/11/2017 
	     BY :Manisha
	     Description : Incase of inspection quotation there should be some schedules for visiting person,
                      create single service for assessment single service.**/
      
			
	prodInfoComposite=AppUtility.initiateServiceProductComposite(new SuperProduct());
	btn_addproducts=new Button("ADD");
	btn_addproducts.addClickHandler(this);
	assessmentreportservicetable=new AssessmentReportServiceTable();
	olbemployeeName=new ObjectListBox<Employee>();
	olbemployeeName.makeEmployeeLive(AppConstants.SERVICEMODULE, AppConstants.AssessmentReport, "Employee Name");
	dbServiceDate=new DateBoxWithYearSelector();
	olbbBranch=new ObjectListBox<Branch>();
	AppUtility.makeBranchListBoxLive(olbbBranch);
	
	/**Ends**/
		
		tbFullName = new TextBox();
		tbCompanyName =new TextBox();
		pnbCellNo = new PhoneNumberBox();
		etbEmail = new EmailTextBox();
		
		customerAddressComposite = new AddressComposite(true);
		
	//  table details 
		dbAssessmentDate = new DateBoxWithYearSelector();
//		tbArea = new TextBox();
		tbLocaltion = new TextArea();
		tbConsequences = new TextArea();
		olbActionForCompany= new TextArea();
//		AppUtility.MakeLiveConfig(olbActionForCompany, Screen.ACTIONPLANFORCOMPANY);
		
		olbActionPlanforCustomer= new TextArea();
//		AppUtility.MakeLiveConfig(olbActionPlanforCustomer, Screen.ACTIONPLANFORCUSTOMER);
		
		olbCategory=new ObjectListBox<ConfigCategory>();
		AppUtility.MakeLiveCategoryConfig(olbCategory, Screen.ASSESSMENTREPORTCATEGORY);
		olbCategory.addChangeHandler(this);
		
		olbDeficiencyType=new ObjectListBox<Type>();
		AppUtility.makeTypeListBoxLive(olbDeficiencyType,Screen.ASSESSMENTREPORTDEFICIENCYTYPE);
		olbDeficiencyType.addChangeHandler(this);

		assessmentreportTable = new AssessmentReportTable();
		
		addDetails=new Button("ADD");
		addDetails.addClickHandler(this);
		
	//  assessed by details 
		assessmentDetailsCount = new TextBox();
		assessmentDetailsCount.setEnabled(false);
		
		olbAssessedBy1=new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(olbAssessedBy1,"AssesmentReport");
		
		olbAssessedBy2=new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(olbAssessedBy2,"AssesmentReport");
		
		olbAssessedBy3=new ObjectListBox<Employee>();
		AppUtility.makeApproverListBoxLive(olbAssessedBy3,"AssesmentReport");
		
		//  accompained by 
		
		
		olbAccompainedBy1=new TextBox();
//		AppUtility.makeApproverListBoxLive(olbAccompainedBy1,"AssesmentReport");
		
		olbAccompainedBy2=new TextBox();
//		AppUtility.makeApproverListBoxLive(olbAccompainedBy2,"AssesmentReport");
		
		olbAccompainedBy3=new TextBox();
//		AppUtility.makeApproverListBoxLive(olbAccompainedBy3,"AssesmentReport");
		
	//  rohan added this upload documnet for NBHC
		ucUploadTAndCs=new UploadComposite();
		
		customerSignatureUpload=new UploadComposite();
		 btnNewCustomer = new Button("New Customer");
		
		
		/**
	     * Date 11-07-2018 By Vijay
	     * For NBHC CCPM Unit depedent for Deficiency
	     */
		olbDeficiencyUnit = new ObjectListBox<AssessmentReportUnit>();
		olbDeficiencyUnit.addItem("-SELECT-");
		olbDeficiencyUnit.addChangeHandler(this);
		loadbDeficiencyUnit();
		/**
		 * ends here
		 */
		
		ibServiceId = new TextBox();
		ibComplaintId = new TextBox();
		ibContractId= new TextBox();
		tbstatus = new TextBox();
		tbstatus.setEnabled(false);
		tbstatus.setText(AssesmentReport.CREATED);
		
		olbbAssementBranch = new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbAssementBranch);
		
		oblCustomerBranch=new ObjectListBox<CustomerBranchDetails>();
//		oblCustomerBranch.addItem("--SELECT--");
//		oblCustomerBranch.addItem("New");
		MyQuerry querry1=new MyQuerry();
		Filter temp1=new Filter();
		temp1.setQuerryString("cinfo.count");
		temp1.setIntValue(-1);
		querry1.getFilters().add(temp1);
		querry1.setQuerryObject(new CustomerBranchDetails());
		oblCustomerBranch.MakeLive(querry1);
		oblCustomerBranch.addChangeHandler(this);
		oblCustomerBranch.addClickHandler(this);
		
		tbRiskLevel=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(tbRiskLevel, Screen.ASSESSMENTRISKLEVEL);
		
		uploadFile=new UploadComposite();
		
//		tbArea=new ListBox();
//		tbArea.addItem("--SELECT--");
//		tbArea.addItem("New");
//		tbArea.addChangeHandler(this);
		
		oblArea=new ObjectListBox<CustomerBranchServiceLocation>();
		oblArea.addItem("--SELECT--");
		oblArea.addItem("New");
		oblArea.addChangeHandler(this);
		oblArea.addClickHandler(this);

		oblServiceLocation=new ObjectListBox<CustomerBranchServiceLocation>();
		MyQuerry querry=new MyQuerry();
		Filter temp=new Filter();
		temp.setQuerryString("cinfo.count");
		temp.setIntValue(-1);
		querry.getFilters().add(temp);
		querry.setQuerryObject(new CustomerBranchServiceLocation());
		oblServiceLocation.MakeLive(querry);
		
//		oblServiceLocation.addItem("--SELECT--");
		oblServiceLocation.addChangeHandler(this);
		oblServiceLocation.addClickHandler(this);//Ashwini Patil
		
		ucuploadimage1 = new UploadComposite();
		ucuploadimage2 = new UploadComposite();
		ucuploadimage3 = new UploadComposite();
		ucuploadimage4 = new UploadComposite();
		ucuploadimage5 = new UploadComposite();
		ucuploadimage6 = new UploadComposite();
		ucuploadimage7 = new UploadComposite();
		ucuploadimage8 = new UploadComposite();
		ucuploadimage9 = new UploadComposite();
		ucuploadimage10 = new UploadComposite();
		
		cbheaderrepetative = new CheckBox();
		cbDoNotPrintstatusCreationDate = new CheckBox();
		tbAttentionName = new TextBox();
		
		image1Label = new TextBox();
		image2Label = new TextBox();
		image3Label = new TextBox();
		image4Label = new TextBox();
		image5Label = new TextBox();
		image6Label = new TextBox();
		image7Label = new TextBox();
		image8Label = new TextBox();
		image9Label = new TextBox();
		image10Label = new TextBox();
	}
	
	/**
     * Date 11-07-2018 By Vijay
     * For NBHC CCPM Unit depedent for Deficiency
     */
	private void loadbDeficiencyUnit() {

		MyQuerry querry= new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		querry.getFilters().add(filter);
		querry.setQuerryObject(new AssessmentReportUnit());
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				olbDeficiencyUnit.clear();
				olbDeficiencyUnit.addItem("--SELECT--");
				for(SuperModel model : result){
					AssessmentReportUnit assesmentReportUnit = (AssessmentReportUnit) model;
					DefUnitlist.add(assesmentReportUnit);
				}
				Comparator<AssessmentReportUnit> dropDownComparator = new Comparator<AssessmentReportUnit>() {
					public int compare(AssessmentReportUnit c1, AssessmentReportUnit c2) {
					String configName1=c1.toString();
					String configName2=c2.toString();
					
					return configName1.compareTo(configName2);
					}
				};
				Collections.sort(DefUnitlist,dropDownComparator);
				
				System.out.println("Loading all data =="+DefUnitlist.size());
				for(int i=0;i<DefUnitlist.size();i++)
				{
					olbDeficiencyUnit.addItem(DefUnitlist.get(i).toString());
				}
				olbDeficiencyUnit.setItems(DefUnitlist);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	/**
	 * ends here
	 */
	
	@Override
	public void createScreen() {
		
		initalizeWidget();
		
		/**
		 * @author Anil @since 16-12-2021
		 * Adding Schedule inspection button which will create assessment service
		 * Raised by Nitin Sir for Prominent
		 */
		this.processlevelBarNames=new String[]{"New","Schedule Assessment","Mark Close"};
//		this.processlevelBarNames=new String[]{"Create Visit","View Service","Mark Close"};
		
		String mainScreenLabel="Assessment Report";
		if(assesreportobj!=null&&assesreportobj.getCount()!=0){
			mainScreenLabel=assesreportobj.getCount()+" "+"/"+" "+assesreportobj.getStatus();/**+" "+ "/"+" "+AppUtility.parseDate(assesreportobj.getCreationDate());**/
		}
		
		//fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		fgroupingCustomerInformation=fbuilder.setlabel(mainScreenLabel).widgetType(FieldType.Grouping).setMandatory(false).setColSpan(5).setKeyField(true).build();
		//FormField fgroupingCustomerInformation=fbuilder.setlabel("Select Existing Customer ").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",personInfoComposite);
		FormField fpersonInfoComposite= fbuilder.setMandatory(true).setMandatoryMsg("Customer info mandatory").setRowSpan(0).setColSpan(4).build();
		
		/****Date :20/11/2017 
        BY :Manisha
        Description : Incase of inspection quotation there should be some schedules for visiting person,
                      create single service for assessment single service.**/
	
	FormField fgroupingProducts=fbuilder.setlabel("Product").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
	fbuilder = new FormFieldBuilder("",prodInfoComposite);
	FormField fprodInfoComposite= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(3).build();
	fbuilder = new FormFieldBuilder("",btn_addproducts);
	FormField fbtn_addproducts= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	fbuilder = new FormFieldBuilder("",assessmentreportservicetable.getTable());
	FormField fassessmentreportservicetable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	fbuilder = new FormFieldBuilder("Employee Name",olbemployeeName);
	FormField folbemployeeName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	fbuilder = new FormFieldBuilder("Visit Date",dbServiceDate);
	FormField dbServiceDate= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	fbuilder = new FormFieldBuilder("Branch",olbbBranch);
	FormField folbbBranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
	/**Ends**/
		

		fbuilder = new FormFieldBuilder();
		FormField fgroupingNewCustomerInformation=fbuilder.setlabel(" New Customer ").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Company Name",tbCompanyName);
		FormField ftbCompanyName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Customer Name",tbFullName);
		FormField ftbFullName= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Cell No",pnbCellNo);
		FormField fpnbCellNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Email",etbEmail);
		FormField fetbEmail= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",customerAddressComposite);
		FormField fcustomerAddressComposite= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		//  for table details 
		FormField fAssessedDetailsInformation=fbuilder.setlabel("Assessment Details").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		
		fbuilder = new FormFieldBuilder("Assessment Details ID",assessmentDetailsCount);
		FormField fassessmentDetailsCount= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("* Date of Assessment",dbAssessmentDate);
		FormField fdbAssessmentDate= fbuilder.setMandatory(true).setMandatoryMsg("Date of Assessment is Mandatory").setRowSpan(0).setColSpan(0).build();
		
//		fbuilder = new FormFieldBuilder("* Area",tbArea);
//		FormField ftbArea= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Area",oblArea);
		FormField ftbArea= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Category",olbCategory);
		FormField folbContractCategory= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
		/**
		 * @author Anil @since 13-10-2021
		 * As per requirement hide Deficiency
		 * Rename Likely Consequences to Impact
		 */
		String locLabel="* Observation";
		String consequencesLbl="* Impact";
		int colspan=4;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("AssesmentReport","onlyForNBHC")){
			locLabel="Location";
			consequencesLbl="Likely Consequences";
			colspan=0;
		}
		FormField ftbLocaltion,ftbConsequences;

			fbuilder = new FormFieldBuilder(locLabel,tbLocaltion);
			ftbLocaltion= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(colspan).build();			

		fbuilder = new FormFieldBuilder("Deficiency",olbDeficiencyType);
		FormField folbContractType= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		

			fbuilder = new FormFieldBuilder(consequencesLbl,tbConsequences);
			ftbConsequences= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(colspan).build();

		fbuilder = new FormFieldBuilder("Action Plan By Company",olbActionForCompany);
		FormField ftaActionForCompany= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(colspan).build();
		
		fbuilder = new FormFieldBuilder("Recommendation to Customer",olbActionPlanforCustomer);
		FormField ftaActionPlanforCustomer= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(colspan).build(); 
		
		fbuilder = new FormFieldBuilder("",assessmentreportTable.getTable());
		FormField fassessmentreportTable= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("",addDetails);
		FormField faddDetails= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		
	//  rohan added this upload documnet for NBHC
			fbuilder = new FormFieldBuilder();
			FormField fgroupingDocuments1=fbuilder.setlabel("Reference/General").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			FormField fgroupingDocuments=fbuilder.setlabel("Attachment").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("Upload Image",ucUploadTAndCs);
			FormField fucUploadTAndCs= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		
		
		FormField fAssessedByInformation=fbuilder.setlabel("Assessed By").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Assessed By",olbAssessedBy1);
		FormField folbAssessedBy1= fbuilder.setMandatory(false).setMandatoryMsg("Approver Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Assessed By 2",olbAssessedBy2);
		FormField folbAssessedBy2= fbuilder.setMandatory(false).setMandatoryMsg("Approver Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Assessed By 3",olbAssessedBy3);
		FormField folbAssessedBy3= fbuilder.setMandatory(false).setMandatoryMsg("Approver Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		
		FormField fAccompainedByInformation=fbuilder.setlabel("Accompanied By").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		
		fbuilder = new FormFieldBuilder("Accompanied By 1",olbAccompainedBy1);
		FormField folbAccompainedBy1= fbuilder.setMandatory(false).setMandatoryMsg("Approver Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Accompanied By 2",olbAccompainedBy2);
		FormField folbAccompainedBy2= fbuilder.setMandatory(false).setMandatoryMsg("Approver Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Accompanied By 3",olbAccompainedBy3);
		FormField folbAccompainedBy3= fbuilder.setMandatory(false).setMandatoryMsg("Approver Name is Mandatory").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Upload Customer Signature",customerSignatureUpload);
		FormField fcustomerSignatureUpload= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
	
		
		fbuilder = new FormFieldBuilder("Unit",olbDeficiencyUnit);
		FormField folbDeficiencyUnit= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Service Id",ibServiceId);
		FormField ftbReferenceNo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Complaint Id",ibComplaintId);
		FormField ftbReferenceNo2= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Contract Id",ibContractId);
		FormField ftbReferenceNo3= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Status",tbstatus);
		FormField tbstatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Branch",olbbAssementBranch);
		FormField folbbAssementBranch= fbuilder.setMandatory(true).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnNewCustomer);
		FormField fbtnNewCustomer = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Customer Branch",oblCustomerBranch);
		FormField flbCustomerBranch = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("* Risk Level",tbRiskLevel);
		FormField ftbRiskLevel = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Upload 1",uploadFile);
		FormField fuploadFile = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(2).build();
		
		fbuilder = new FormFieldBuilder("* Service Location",oblServiceLocation);
		FormField foblServiceLocation = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField fgroupingImages=fbuilder.setlabel("Images on print").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();

		fbuilder = new FormFieldBuilder("Upload Image 1",ucuploadimage1);
		FormField fucuploadimage1 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Upload Image 2",ucuploadimage2);
		FormField fucuploadimage2 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Upload Image 3",ucuploadimage3);
		FormField fucuploadimage3 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Upload Image 4",ucuploadimage4);
		FormField fucuploadimage4 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Upload Image 5",ucuploadimage5);
		FormField fucuploadimage5 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Upload Image 6",ucuploadimage6);
		FormField fucuploadimage6 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Upload Image 7",ucuploadimage7);
		FormField fucuploadimage7 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Upload Image 8",ucuploadimage8);
		FormField fucuploadimage8 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Upload Image 9",ucuploadimage9);
		FormField fucuploadimage9 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("Upload Image 10",ucuploadimage10);
		FormField ucuploadimage10 = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("Image1 Label",image1Label);
		FormField fimage1Label = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Image2 Label",image2Label);
		FormField fimage2Label = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Image3 Label",image3Label);
		FormField fimage3Label = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Image4 Label",image4Label);
		FormField fimage4Label = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Image5 Label",image5Label);
		FormField fimage5Label = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Image6 Label",image6Label);
		FormField fimage6Label = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Image7 Label",image7Label);
		FormField fimage7Label = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Image8 Label",image8Label);
		FormField fimage8Label = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Image9 Label",image9Label);
		FormField fimage9Label = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		fbuilder = new FormFieldBuilder("Image10 Label",image10Label);
		FormField fimage10Label = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
	
		
		fbuilder = new FormFieldBuilder("Print PDF Header Repetative",cbheaderrepetative);
		FormField fcbheaderrepetative = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Do not print status/Date",cbDoNotPrintstatusCreationDate);
		FormField fcbDoNotPrintstatusCreationDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Print in Attn on pdf",tbAttentionName);
		FormField ftbAttentionName = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
				
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("AssesmentReport","onlyForNBHC"))
		{
			
			FormField[][] formfield = {   
//					{fgroupingCustomerInformation},
//					{fpersonInfoComposite},
//					{fAssessedDetailsInformation},
//					{fassessmentDetailsCount,tbstatus,fdbAssessmentDate,ftbArea,} ,
//					{ftbLocaltion,folbContractCategory,folbContractType,folbDeficiencyUnit},
//					{ftbConsequences , ftaActionForCompany,ftaActionPlanforCustomer,faddDetails},
//					{fassessmentreportTable},
//					{fgroupingDocuments},
//					{fucUploadTAndCs},
//					{fAssessedByInformation},
//					{folbAssessedBy1,folbAssessedBy2,folbAssessedBy3},
//					{fAccompainedByInformation},
//					{folbAccompainedBy1,folbAccompainedBy2,folbAccompainedBy3},
//					{ftbReferenceNo,ftbReferenceNo2,folbbAssementBranch}
					
					/**Main Screen**/
					{fgroupingCustomerInformation},
					{fpersonInfoComposite},
					{flbCustomerBranch,foblServiceLocation,folbbAssementBranch,fdbAssessmentDate},
					
					/**
					 * @author Anil @since 13-10-2021
					 * Removed new customer functionality
					 */
					/**New Customer**/
					/*{fgroupingNewCustomerInformation},				
					{ftbCompanyName,ftbFullName,fpnbCellNo,fetbEmail},
					{fcustomerAddressComposite},*/
					
					/**Product**/
					{fgroupingProducts},
					{fprodInfoComposite,fbtn_addproducts},
					{folbemployeeName,dbServiceDate,folbbBranch},
					{fassessmentreportservicetable},
					
					/**Assessment Details**/
					{fAssessedDetailsInformation},
					{folbAssessedBy1,folbAssessedBy2,folbAssessedBy3},
					{folbAccompainedBy1,folbAccompainedBy2,folbAccompainedBy3},
					{ftbArea,ftbLocaltion,folbContractCategory},
					{folbContractType,ftbConsequences,ftaActionForCompany },
					{ftaActionPlanforCustomer ,ftbRiskLevel,faddDetails },
					{fassessmentreportTable},
					/**Reference**/
					{fgroupingDocuments1},
					
					{ftbReferenceNo,ftbReferenceNo2,ftbReferenceNo3},
					/**Attachment**/
					{fgroupingDocuments},
					{fucUploadTAndCs},
					{fcustomerSignatureUpload},
					
					
			};
			this.fields=formfield;
		}
		else{
			
			FormField[][] formfield = { 
					/**Main Screen**/
					{fgroupingCustomerInformation},
					{fpersonInfoComposite},
					{flbCustomerBranch,folbbAssementBranch,folbAssessedBy1,fdbAssessmentDate},
					{fcbheaderrepetative,fcbDoNotPrintstatusCreationDate},
					/**New Customer**/
					/*{fgroupingNewCustomerInformation},				
					{ftbCompanyName,ftbFullName,fpnbCellNo,fetbEmail},
					{fcustomerAddressComposite},*/
					
					
					/**Assessment Details**/
					{fAssessedDetailsInformation},
					
					{foblServiceLocation,ftbArea,ftbRiskLevel,folbContractCategory},
					{ftbLocaltion},
					{ftbConsequences},
					{ftaActionForCompany },
					{ftaActionPlanforCustomer} ,
					{fuploadFile,faddDetails },
					{fassessmentreportTable},
					
					/**Reference**/
					{fgroupingDocuments1},
					{folbAssessedBy2,folbAssessedBy3},
					{folbAccompainedBy1,folbAccompainedBy2,folbAccompainedBy3},
					{ftbReferenceNo,ftbReferenceNo2,ftbReferenceNo3},
					{ftbAttentionName},
					
					/**Attachment**/
					{fgroupingDocuments},
					{fucUploadTAndCs},
					{fcustomerSignatureUpload},
					
					/**Product**/
					{fgroupingProducts},
					{fprodInfoComposite,fbtn_addproducts},
					{folbemployeeName,dbServiceDate,folbbBranch},
					{fassessmentreportservicetable},
					
					/*** Upload Images ***/
					{fgroupingImages},
					{fucuploadimage1,fimage1Label,fucuploadimage2,fimage2Label},
					{fucuploadimage3,fimage3Label,fucuploadimage4,fimage4Label},
					{fucuploadimage5,fimage5Label,fucuploadimage6,fimage6Label},
					{fucuploadimage7,fimage7Label,fucuploadimage8,fimage8Label},
					{fucuploadimage9,fimage9Label,ucuploadimage10,fimage10Label}
				
		};
		this.fields=formfield;
		}
	}

	@Override
	public void updateModel(AssesmentReport model) {
		
		if(tbstatus.getValue()!=null && !tbstatus.getValue().equals("")){
			model.setStatus(tbstatus.getValue());
		}
		
		if(personInfoComposite.getValue()!=null){
			model.setCinfo(personInfoComposite.getValue());
		}
		
		if(tbFullName.getValue()!=null){
			System.out.println("mode .set "+tbFullName.getValue().trim());
			model.setNewcustomerfullName(tbFullName.getValue().trim());
		}
		
		if(pnbCellNo.getValue()!=null){
			model.setNewcustomercellNumber(pnbCellNo.getValue());
		}
		
		if(etbEmail.getValue()!=null){
			model.setNewcustomerEmail(etbEmail.getValue());
		}
		
		if(customerAddressComposite.getValue()!=null){
			model.setNewcustomerAddress(customerAddressComposite.getValue());
		}
		
		System.out.println("model.getNewcustomerfullName()"+model.getNewcustomerfullName());
		
		if(model.getNewcustomerfullName()!=null && !model.getNewcustomerfullName().equals("") && model.getCinfo()!= null){
			customerEditFlag = true;
		}
		
		if(model.getNewcompanyname()!=null && !model.getNewcompanyname().equals("") && model.getCinfo()!= null){
			customerEditFlag = true;
		}
		
		if(customerAddressComposite.getValue()!=null){
			model.setNewcustomerAddress(customerAddressComposite.getValue());
		}
		
		
		if(olbAccompainedBy1.getValue()!= null && !olbAccompainedBy1.getValue().equals("")){
			model.setAccompainedByPerson1(olbAccompainedBy1.getValue());
		}
		
		if(olbAccompainedBy2.getValue()!= null && !olbAccompainedBy2.getValue().equals("")){
			model.setAccompainedByPerson2(olbAccompainedBy2.getValue());
		}
		if(olbAccompainedBy3.getValue()!= null && !olbAccompainedBy3.getValue().equals("")){
			model.setAccompainedByPerson3(olbAccompainedBy3.getValue());
		}
		
		
		if(olbAssessedBy1.getSelectedIndex()!=0){
			model.setAssessedPeron1(olbAssessedBy1.getValue(olbAssessedBy1.getSelectedIndex()));
		}
		
		if(olbAssessedBy2.getSelectedIndex()!=0){
			model.setAssessedPeron2(olbAssessedBy2.getValue(olbAssessedBy2.getSelectedIndex()));
		}
		
		if(olbAssessedBy3.getSelectedIndex()!=0){
			model.setAssessedPeron3(olbAssessedBy3.getValue(olbAssessedBy3.getSelectedIndex()));
		}
		
		if(dbAssessmentDate.getValue()!= null)
		{
			model.setAssessmentDate(dbAssessmentDate.getValue());
		}
		
	//  rohan added this for NBHC
		model.setAssessmentUpload(ucUploadTAndCs.getValue());
		model.setCustomerSignature(customerSignatureUpload.getValue());
		List<AssesmentReportEmbedForTable> saleslist=this.assessmentreportTable.getDataprovider().getList();
		ArrayList<AssesmentReportEmbedForTable> arrItems=new ArrayList<AssesmentReportEmbedForTable>();
		arrItems.addAll(saleslist);
		model.setAssessmentDetailsLIst(arrItems);
		
		/****Date :20/11/2017 
        BY :Manisha
        Description :Incase of inspection quotation there should be some schedules for visiting person,
                      create single service for assessment single service.
       *********/
	
	
	  List<AssessmentReportService> servicelist=this.assessmentreportservicetable.getDataprovider().getList();
	  ArrayList<AssessmentReportService> serviceitem=new ArrayList<AssessmentReportService>();
	  serviceitem.addAll(servicelist);
	  model.setAssessmentreportServiceList(serviceitem);
	
	/**Ends**/
	  
	  if(ibServiceId.getValue()!=null && !ibServiceId.getValue().equals("")){
		  model.setServiceId(Integer.parseInt(ibServiceId.getValue()));

	  }
	  if(ibComplaintId.getValue()!=null && !ibComplaintId.getValue().equals("")){
		  model.setComplaintId(Integer.parseInt(ibComplaintId.getValue()));
	  }
	  if(ibContractId.getValue()!=null && !ibContractId.getValue().equals("")){
		  model.setContractId(Integer.parseInt(ibContractId.getValue()));
	  }
		
	  if(olbbAssementBranch.getSelectedIndex()!=0){
		  model.setBranch(olbbAssementBranch.getValue());
	  }
	  
	  if(oblCustomerBranch.getValue()!=null){
		  model.setCustomerBranch(oblCustomerBranch.getValue());
	  }else{
		  model.setCustomerBranch("");
	  }
	  
	  if(oblServiceLocation.getValue()!=null){
		  model.setServiceLocation(oblServiceLocation.getValue());
	  }else{
		  model.setServiceLocation("");
	  }
	 
	  if(oblCustomerBranch.getSelectedIndex()!=0){
		 CustomerBranchDetails customerBranch =  oblCustomerBranch.getSelectedItem();  
		 model.setCustomerBranchId(customerBranch.getCount());
	  }
	  if(oblServiceLocation.getSelectedIndex()!=0){
		 CustomerBranchServiceLocation customerbranchservicelocation = oblServiceLocation.getSelectedItem();
		 model.setServiceLocationId(customerbranchservicelocation.getCount());
	  }
	  	if(ucuploadimage1.getValue()!=null){
	  		model.setImage1(ucuploadimage1.getValue());
	  	}
	  	else{
	  		model.setImage1(null);
	  	}
	  	if(ucuploadimage2.getValue()!=null){
	  		model.setImage2(ucuploadimage2.getValue());
	  	}
	  	else{
	  		model.setImage2(null);
	  	}
	  	if(ucuploadimage3.getValue()!=null){
	  		model.setImage3(ucuploadimage3.getValue());
	  	}
	  	else{
	  		model.setImage4(null);
	  	}
	  	if(ucuploadimage4.getValue()!=null){
	  		model.setImage4(ucuploadimage4.getValue());
	  	}
	  	else{
	  		model.setImage4(null);
	  	}
	  	if(ucuploadimage5.getValue()!=null){
	  		model.setImage5(ucuploadimage5.getValue());
	  	}
	  	else{
	  		model.setImage5(null);
	  	}
	  	if(ucuploadimage6.getValue()!=null){
	  		model.setImage6(ucuploadimage6.getValue());
	  	}
	  	else{
	  		model.setImage6(null);
	  	}
	  	if(ucuploadimage7.getValue()!=null){
	  		model.setImage7(ucuploadimage7.getValue());
	  	}
	  	else{
	  		model.setImage7(null);
	  	}
	  	if(ucuploadimage8.getValue()!=null){
	  		model.setImage8(ucuploadimage8.getValue());
	  	}
	  	else{
	  		model.setImage8(null);
	  	}
	  	if(ucuploadimage9.getValue()!=null){
	  		model.setImage9(ucuploadimage9.getValue());
	  	}
	  	else{
	  		model.setImage9(null);
	  	}
	  	if(ucuploadimage10.getValue()!=null){
	  		model.setImage10(ucuploadimage10.getValue());
	  	}
	  	else{
	  		model.setImage10(null);
	  	}
	  	
	  	if(cbheaderrepetative.getValue()!=null){
	  		model.setPrintPdfHeaderRepetative(cbheaderrepetative.getValue());
	  	}
	  	if(cbDoNotPrintstatusCreationDate.getValue()!=null){
	  		model.setDonotprintstatuscreationDate(cbDoNotPrintstatusCreationDate.getValue());
	  	}
	  	
	  	if(tbAttentionName.getValue()!=null){
	  		model.setPrintAttn(tbAttentionName.getValue());
	  	}
	  	
	  	if(image1Label.getValue()!=null)
	  		model.setImage1label(image1Label.getValue());
	  	else
	  		model.setImage1label("");
	  	
	  	if(image2Label.getValue()!=null)
	  		model.setImage2label(image2Label.getValue());
	  	else
	  		model.setImage2label("");
	  	
	  	if(image3Label.getValue()!=null)
	  		model.setImage3label(image3Label.getValue());
	  	else
	  		model.setImage3label("");
	  	
	  	if(image4Label.getValue()!=null)
	  		model.setImage4label(image4Label.getValue());
	  	else
	  		model.setImage4label("");
	  	
	  	if(image5Label.getValue()!=null)
	  		model.setImage5label(image5Label.getValue());
	  	else
	  		model.setImage5label("");
	  	
	  	if(image6Label.getValue()!=null)
	  		model.setImage6label(image6Label.getValue());
	  	else
	  		model.setImage6label("");
	  	
	  	if(image7Label.getValue()!=null)
	  		model.setImage7label(image7Label.getValue());
	  	else
	  		model.setImage7label("");
	  	
	  	if(image8Label.getValue()!=null)
	  		model.setImage8label(image8Label.getValue());
	  	else
	  		model.setImage8label("");
	  	
	  	if(image9Label.getValue()!=null)
	  		model.setImage9label(image9Label.getValue());
	  	else
	  		model.setImage9label("");
	  	
	  	if(image10Label.getValue()!=null)
	  		model.setImage10label(image10Label.getValue());
	  	else
	  		model.setImage10label("");

	  /**Date - 30-12-2020 added by Priyanka issued raised by Vaishali mam as id and status not appears**/
	  assesreportobj=model;
	}

	
	
	@Override
	public void updateView(AssesmentReport view) {
		/**Date - 30-12-2020 added by Priyanka issued raised by Vaishali mam as id and status not appears**/
		assesreportobj=view;
		assessmentDetailsCount.setValue(view.getCount()+"");
		if(view.getCinfo().getCellNumber()!=null){
			personInfoComposite.setValue(view.getCinfo());

		}
		
		if(view.getCustomerBranch()!=null&&!view.getCustomerBranch().equals("")&&view.getCinfo()!=null&&view.getCinfo().getCount()!=0){
			/**
			 * @author Anil
			 * @since 03-06-2020
			 */
			loadCustomerBranch(view.getCinfo().getCount(), view.getCustomerBranch(),false);
			oblCustomerBranch.setValue(view.getCustomerBranch());
		}
		
		if(view.getCustomerBranch()!=null&&!view.getCustomerBranch().equals("")&&view.getCinfo()!=null&&view.getCinfo().getCount()!=0&&view.getServiceLocation()!=null&&!view.getServiceLocation().equals("")){
		
			loadCustomerBranchServiceLocation(view.getCinfo().getCount(), view.getCustomerBranch(),view.getServiceLocation(),null);
			oblServiceLocation.setValue(view.getServiceLocation());
		}
		
		assessmentreportTable.setValue(view.getAssessmentDetailsLIst());
		
		
		/****Date :20/11/2017 
        BY :Manisha
        Description :Incase of inspection quotation there should be some schedules for visiting person,
                    create single service for assessment single service.**/
	
	   assessmentreportservicetable.setValue(view.getAssessmentreportServiceList());
	
//	if(view.getBranch()!=null){
//		olbbBranch.setValue(view.getBranch());
//	}
	
	/**End**/
		
		if(view.getAssessmentDate()!= null)
		{
			dbAssessmentDate.setValue(view.getAssessmentDate());
		}
		
		if(view.getNewcompanyname()!=null){
			tbCompanyName.setValue(view.getNewcompanyname());
		}
		
		if(view.getNewcustomerfullName()!=null){
			tbFullName.setValue(view.getNewcustomerfullName());
		}
		
		if(view.getNewcustomercellNumber()!=null){
			pnbCellNo.setValue(view.getNewcustomercellNumber());
		}
		
		if(view.getNewcustomerEmail()!=null){
			etbEmail.setValue(view.getNewcustomerEmail());
		}
		
		if(view.getNewcustomerAddress()!=null){
			customerAddressComposite.setValue(view.getNewcustomerAddress());
		}
		
		
		
		if(view.getAssessedPeron1()!=null)
			olbAssessedBy1.setValue(view.getAssessedPeron1());
		
		if(view.getAssessedPeron2()!=null)
			olbAssessedBy2.setValue(view.getAssessedPeron2());
		
		if(view.getAssessedPeron3()!=null)
			olbAssessedBy3.setValue(view.getAssessedPeron3());
		
		if(view.getAccompainedByPerson1()!=null)
			olbAccompainedBy1.setValue(view.getAccompainedByPerson1());
		
		if(view.getAccompainedByPerson2()!=null)
			olbAccompainedBy2.setValue(view.getAccompainedByPerson2());
		
		if(view.getAccompainedByPerson3()!=null)
			olbAccompainedBy3.setValue(view.getAccompainedByPerson3());
		
	//  rohan added this for NBHC
		if(view.getAssessmentUpload()!=null){
			ucUploadTAndCs.setValue(view.getAssessmentUpload());
		}
		if(view.getCustomerSignature()!=null){
			customerSignatureUpload.setValue(view.getCustomerSignature());
		}
		
		ibServiceId.setValue(view.getServiceId()+"");
		ibComplaintId.setValue(view.getComplaintId()+"");
		ibContractId.setValue(view.getContractId()+"");
		  
		if(view.getBranch()!=null){
			olbbAssementBranch.setValue(view.getBranch());
		}
		if(view.getStatus()!=null){
			tbstatus.setValue(view.getStatus());
		}
		
		if(view.getImage1()!=null){
			ucuploadimage1.setValue(view.getImage1());
		}
		if(view.getImage2()!=null){
			ucuploadimage2.setValue(view.getImage2());
		}
		if(view.getImage3()!=null){
			ucuploadimage3.setValue(view.getImage3());
		}
		if(view.getImage4()!=null){
			ucuploadimage4.setValue(view.getImage4());
		}
		if(view.getImage5()!=null){
			ucuploadimage5.setValue(view.getImage5());
		}
		if(view.getImage6()!=null){
			ucuploadimage6.setValue(view.getImage6());
		}
		if(view.getImage7()!=null){
			ucuploadimage7.setValue(view.getImage7());
		}
		if(view.getImage8()!=null){
			ucuploadimage8.setValue(view.getImage8());
		}
		if(view.getImage9()!=null){
			ucuploadimage9.setValue(view.getImage9());
		}
		if(view.getImage10()!=null){
			ucuploadimage10.setValue(view.getImage10());
		}
		
		if(view.isPrintPdfHeaderRepetative()){
			cbheaderrepetative.setValue(view.isPrintPdfHeaderRepetative());
		}
		if(view.isDonotprintstatuscreationDate()){
			cbDoNotPrintstatusCreationDate.setValue(view.isDonotprintstatuscreationDate());
		}
		if(view.getPrintAttn()!=null){
			tbAttentionName.setValue(view.getPrintAttn());
		}
		
		if(view.getImage1label()!=null){
			image1Label.setValue(view.getImage1label());
		}
		if(view.getImage2label()!=null){
			image2Label.setValue(view.getImage2label());
		}
		if(view.getImage3label()!=null){
			image3Label.setValue(view.getImage3label());
		}
		if(view.getImage4label()!=null){
			image4Label.setValue(view.getImage4label());
		}
		if(view.getImage5label()!=null){
			image5Label.setValue(view.getImage5label());
		}
		if(view.getImage6label()!=null){
			image6Label.setValue(view.getImage6label());
		}
		if(view.getImage7label()!=null){
			image7Label.setValue(view.getImage7label());
		}
		if(view.getImage8label()!=null){
			image8Label.setValue(view.getImage8label());
		}
		if(view.getImage9label()!=null){
			image9Label.setValue(view.getImage9label());
		}
		if(view.getImage10label()!=null){
			image10Label.setValue(view.getImage10label());
		}
		presenter.setModel(view);
	}
	
	
	/**
	 * Toggles the app header bar menus as per screen state
	 */

	
	@Override
	public void toggleAppHeaderBarMenu() {
		

		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains("Search")||text.contains(AppConstants.NAVIGATION))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
				
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Save")||text.contains("Discard")||text.contains(AppConstants.NAVIGATION))
					menus[k].setVisible(true); 
				
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Edit")||text.contains("Discard")||text.contains("Search")||text.contains("Print")||text.contains(AppConstants.NAVIGATION)|| text.contains(AppConstants.MESSAGE)|| text.contains(AppConstants.EMAIL))
					/**
					 * @author Abhinav
					 * @since 16/11/2019
					 * when Assesment will closed edit button will not display
					 */
					if(tbstatus.getValue()!=null && !tbstatus.getValue().equals("") && tbstatus.getValue().equals(AssesmentReport.CLOSED) 
					  && text.contains("Edit") ){
						menus[k].setVisible(false); 
					}
					else{
						menus[k].setVisible(true); 
					}
				
				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.CONTRACT,LoginPresenter.currentModule.trim());
	
	}

	@Override
	public void onChange(ChangeEvent event) {
		Console.log("ON CHANGE");
		/**
		 * @author Anil @since 11-05-2021
		 */
		if (event.getSource().equals(olbCategory)) {
			if (olbCategory.getSelectedIndex() != 0) {
				ConfigCategory cat = olbCategory.getSelectedItem();
				if (cat != null) {
					AppUtility.makeLiveTypeDropDown(olbDeficiencyType,cat.getCategoryName(), cat.getCategoryCode(),cat.getInternalType());
				}
			}
		}
		if(event.getSource()==olbDeficiencyType){
			reactToOnChangeDeficiencyType();
		}
		
		
		if(event.getSource().equals(oblCustomerBranch)){
			if(oblCustomerBranch.getSelectedIndex()!=0){
				System.out.println("oblCustomerBranch");
//				if(oblCustomerBranch.getValue(oblCustomerBranch.getSelectedIndex()).equals("New")){
//					System.out.println("oblCustomerBranch=="+personInfoComposite.getIdValue());
//					Company comp = new Company();
//					generalviewdocumentpopup.setCustomerBranchModel(null, null, personInfoComposite.getValue());
//					generalPanel = new PopupPanel();
//					generalPanel.add(generalviewdocumentpopup);
//					generalPanel.show();
//					generalPanel.center();
//					
//					return;
//				}
				CustomerBranchDetails customerBranch=oblCustomerBranch.getSelectedItem();
				if(customerBranch!=null){
					oblServiceLocation.clear();
					String customerBranchName = oblCustomerBranch.getValue(oblCustomerBranch.getSelectedIndex());
					loadCustomerBranchServiceLocation(personInfoComposite.getIdValue(), customerBranchName,null,null);
				}
			}else{
				oblServiceLocation.clear();
				oblServiceLocation.addItem("--SELECT--");
//				tbArea.clear();;
//				tbArea.addItem("--SELECT--");
//				tbArea.addItem("New");
				
				oblArea.clear();;
				oblArea.addItem("--SELECT--");
				oblArea.addItem("New");
				
			}
		}
		
		if(event.getSource().equals(oblServiceLocation)){
			Console.log("Service location");	
			if(oblServiceLocation.getSelectedIndex()!=0){
				Console.log("Service location not zero");
//				tbArea.clear();;
//				tbArea.addItem("--SELECT--");
//				tbArea.addItem("New");
				
				oblArea.clear();;
				oblArea.addItem("--SELECT--");
				oblArea.addItem("New");
				
				CustomerBranchServiceLocation obj=oblServiceLocation.getSelectedItem();
				if(obj!=null){
					Console.log("Service location not null");
					if(obj.getAreaList2()!=null&&obj.getAreaList2().size()!=0){
						Console.log("AreaList2 not null");
						for(Area area:obj.getAreaList2()){
//							tbArea.addItem(area.getArea());
							oblArea.addItem(area.getArea());

						}
					}else if(obj.getAreaList()!=null&&obj.getAreaList().size()!=0){
							Console.log("Area not null");
							for(String area:obj.getAreaList()){
//								tbArea.addItem(area);
								oblArea.addItem(area);

							}
					}
				}
			}else{
				Console.log("Service location zero");
//				tbArea.clear();;
//				tbArea.addItem("--SELECT--");
//				tbArea.addItem("New");
				
				oblArea.clear();;
				oblArea.addItem("--SELECT--");
				oblArea.addItem("New");
			}
		}
		
//		if(event.getSource().equals(tbArea)){
//			if(tbArea.getSelectedIndex()!=0){
//				if(tbArea.getValue(tbArea.getSelectedIndex()).equals("New")){
//					areapopup.showPopUp();
//					areapopup.tbServiceLocation.setValue(oblServiceLocation.getValue(oblServiceLocation.getSelectedIndex()));
//				}
//			}	
//			
//		}

		
		if(event.getSource().equals(oblArea)){
			if(oblArea.getSelectedIndex()!=0){
				if(oblArea.getValue(oblArea.getSelectedIndex()).equals("New")){
					areapopup.showPopUp();
					areapopup.tbServiceLocation.setValue(oblServiceLocation.getValue(oblServiceLocation.getSelectedIndex()));
					if(areapopup.areaTbl!=null)
					areapopup.areaTbl.connectToLocal();
				}
			}	
			
		}
		

	}

	
	private void reactToOnChangeDeficiencyType() {
		if(olbDeficiencyUnit.getItemCount()==1){
			makeliveDeficiencyUnit(DefUnitlist);
		}
		String selectedItem = olbDeficiencyType.getValue(olbDeficiencyType.getSelectedIndex());
		ArrayList<AssessmentReportUnit> list = new ArrayList<AssessmentReportUnit>();
		for(int i=0;i<DefUnitlist.size();i++){
			if(selectedItem.equals(DefUnitlist.get(i).getDeficiency())){
				list.add(DefUnitlist.get(i));
			}
		}
		olbDeficiencyUnit.clear();
		olbDeficiencyUnit.addItem("--SELECT--");
		makeliveDeficiencyUnit(list);
	}

	private void makeliveDeficiencyUnit(ArrayList<AssessmentReportUnit> list) {

		for(int i=0;i<list.size();i++)
		{
			olbDeficiencyUnit.addItem(list.get(i).toString());
		}
		olbDeficiencyUnit.setItems(list);
	}
	
	@Override
	public void onClick(ClickEvent event) {
		
		if(event.getSource().equals(oblServiceLocation)){
			Console.log("Service location on click");
			if(oblCustomerBranch.getSelectedIndex()==0) {
				showDialogMessage("Please select Customer branch first");
	            return;			
			}
		}
		
		if(event.getSource().equals(this.addDetails)){
			if (validateForTable() == false) {
				addAssessmentDetailsToTable();
				System.out.println("in side onlcikc");
			} 
//			else {
//				this.showDialogMessage("Please select mandatory fields!");
//			}

		}
		
		/****Date :20/11/2017 
        BY :Manisha
        Description :Incase of inspection quotation there should be some schedules for visiting person,
                create single service for assessment single service.**/
	
	if(event.getSource().equals(this.btn_addproducts))
		{
			
			if(personInfoComposite.getId().getValue().equals("") && this.tbFullName.getValue().equals("") && this.tbCompanyName.getValue().equals("")){
				this.showDialogMessage("Please add customer information!");
				return;
			}
			if(!this.tbFullName.getValue().equals("") || !this.tbCompanyName.getValue().equals("")  ){
				System.out.println(" for address incomplete");
				System.out.println("add == "+this.customerAddressComposite.getAdressline1().getValue());
				
				if(this.customerAddressComposite.getAdressline1().getValue().equals("") || this.customerAddressComposite.getCountry().getSelectedIndex()==0 || this.customerAddressComposite.getState().getSelectedIndex()==0 || this.customerAddressComposite.getCity().getSelectedIndex()==0 ){
					this.showDialogMessage("Please add new customer complete address!");
					return;
				}
			}
			
			if(this.olbemployeeName.getValue()==null){
				this.showDialogMessage("Please add employee name!");
				return;
			}
			
			if(this.dbServiceDate.getValue()==null){
				this.showDialogMessage("Please add visit date!");
				return;
			}
			if(this.olbbBranch.getValue()==null){
				this.showDialogMessage("Please add branch!");
				return;
			}
			
			if(validateforservicetable()==true){
				addAssessmentServicesToTable();
				
			}
			else{
				showDialogMessage("Please insert product details..!");
			}
		
		/**End**/
		
			}
			this.prodInfoComposite.clear();
			
			if(event.getSource().equals(generalviewdocumentpopup.getBtnClose())){
				
//				oblCustomerBranch.clear();
				loadCustomerBranch(Integer.parseInt(getPersonInfoComposite().getId().getValue().trim()), null, true);
				
				oblServiceLocation.clear();
//				tbArea.clear();
//				tbArea.addItem("--SELECT--");
				
				oblArea.clear();
				oblArea.addItem("--SELECT--");
			}
			
			
			if(event.getSource().equals(areapopup.getLblOk())){
				if(oblServiceLocation.getSelectedIndex()!=0){
					CustomerBranchServiceLocation servicelocation = oblServiceLocation.getSelectedItem();
					ArrayList<Area> arealist = new ArrayList<Area>();
					if(servicelocation.getAreaList2()!=null){
						arealist.addAll(servicelocation.getAreaList2());

					}
//					final Area area = new Area();
//					if(areapopup.tbArea.getValue()!=null){
//						area.setArea(areapopup.tbArea.getValue());
//						System.out.println("areapopup.tbArea.getValue() "+areapopup.tbArea.getValue());
//					}
//					if(areapopup.tbFrequency.getValue()!=null)
//					area.setFrequency(areapopup.tbFrequency.getValue());
//					if(areapopup.dbnoOfServices.getValue()!=null)
//					area.setPlannedservices(areapopup.dbnoOfServices.getValue());
//					System.out.println("AREA "+area.getArea());
					
					final List<Area> list = areapopup.areaTbl.getDataprovider().getList();
					if(list.size()==0){
						showDialogMessage("Please add area details");
						return;
					}
					arealist.addAll(areapopup.areaTbl.getDataprovider().getList());
					servicelocation.setAreaList2(arealist);
					
					final String servicelocationName = servicelocation.getServiceLocation();
					genasync.save(servicelocation, new AsyncCallback<ReturnFromServer>() {
						
						@Override
						public void onSuccess(ReturnFromServer result) {
							// TODO Auto-generated method stub
							
							areapopup.hidePopUp();
							oblServiceLocation.clear();
							loadCustomerBranchServiceLocation(personInfoComposite.getIdValue(), oblCustomerBranch.getValue(oblCustomerBranch.getSelectedIndex()), servicelocationName, list);
			
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}
					});
				}
			}
			
			if(event.getSource().equals(oblCustomerBranch)){
				Console.log("customer branch on click"+oblCustomerBranch.getItems().size());//icic0001249
				if(oblCustomerBranch.getItems().size()==0) {
					showDialogMessage("Please select Customer first");
		            return;			
				}
			}
			if(event.getSource().equals(oblArea)){
				Console.log("area on click"+oblServiceLocation.getItems().size());
				if(oblServiceLocation.getSelectedIndex()==0) {
					showDialogMessage("Please select service location first");
		            return;			
				}
			}
			
			if(event.getSource().equals(areapopup.getLblCancel())){
				oblArea.setSelectedIndex(0);
				areapopup.hidePopUp();
			}
	}
	
	private boolean validateForTable() {
		
		if(this.olbCategory.getSelectedIndex()==0)
		{   this.showDialogMessage("Please select category");
			return true;
		}
		if(this.oblArea.getSelectedIndex()==0){
			this.showDialogMessage("Please select area");
			return true;
		}
		if(this.tbRiskLevel.getSelectedIndex()==0){
			this.showDialogMessage("Please select risk level");
			return true;
		}
		if(this.tbLocaltion.getValue().equals("")){
			this.showDialogMessage("Please enter Observation");
			return true;
		}
		if(this.tbConsequences.getValue().equals("")){
			this.showDialogMessage("Please enter Impact");
			return true;
		}
		if(this.oblServiceLocation.getSelectedIndex()==0){
			this.showDialogMessage("Please select service location");
			return true;
		}
		
//		else if(this.olbDeficiencyType.getSelectedIndex()==0)
//		{
//			return true;
//		}
//		else if(this.olbActionForCompany.getSelectedIndex()==0)
//		{
//			return true;
//		}
//		else if(this.olbActionPlanforCustomer.getSelectedIndex()==0)
//		{
//			return true;
//		}
		return false;
	}

	
	/****Date :20/11/2017 
    BY :Manisha
    Description :Incase of inspection quotation there should be some schedules for visiting person,
            create single service for assessment single service.**/
	
	public boolean validateforservicetable()
	{
	  if(!this.prodInfoComposite.getProdID().getValue().equals("")
	  && this.prodInfoComposite.getProdCode().getValue()!=null
	  && this.prodInfoComposite.getProdName().getValue()!=null){
		       return true;
		     
	     }
	
	return false;
	}
	/******end**********/
	@Override //annotation added by Ashwini Patil
	public boolean validate()
	{
		Console.log("validate() called");
		boolean supervalidate = super.validate();
		
		if(!supervalidate)
		{
			return false;
		}
		
		if(this.assessmentreportTable.getDataprovider().getList().size()==0){
			showDialogMessage("Please Fill Assessment Details!");
            return false;
		}
		/****Date :20/11/2017 
	    BY :Manisha
	    Description :Incase of inspection quotation there should be some schedules for visiting person,
	            create single service for assessment single service.**/
	
		if(this.personInfoComposite.getCustomerName()==null&&!this.personInfoComposite.getCustomerName().equals(""))
		{
			showDialogMessage("Please Fill Customer Details!");
            return false;
		}
		
		if(this.tbFullName.getValue().equalsIgnoreCase("") && this.tbFullName.getValue()==null){
			showDialogMessage("Please Fill Customer Details!");
			return false;
		}
		
		System.out.println("assessmentreportservicetable size"+assessmentreportservicetable.getDataprovider().getList().size());
		
		
		/*** Date 13-08-2019 by Vijay below validation not required for NBHC ***/
//		if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("AssesmentReport","onlyForNBHC"))
//		{
//			if(assessmentreportservicetable.getDataprovider().getList().size()==0)
//			{
//				showDialogMessage("Please Fill Product Details!");
//	            return false;
//			}
//		}
		
		/**
	     * @author Anil
	     * @since 07-01-2021
	     * Date of Assessment can not be greater than current date
	     */
	    DateTimeFormat format= DateTimeFormat.getFormat("dd-MM-yyyy");
	    if(dbAssessmentDate.getValue()!=null&&format.parse(format.format(dbAssessmentDate.getValue())).after(format.parse(format.format(new Date())))){
			showDialogMessage("Date of Assessment should not be greater than current date!");
			return false;
		}
		
		
		/**End**/
		return true;
	}

	private void addAssessmentDetailsToTable() {
		findMaxServiceSrNumber();
		AssesmentReportEmbedForTable report = new AssesmentReportEmbedForTable(); 
		productSrNo++;
		report.setCount(productSrNo);
		report.setStatus("Created");
		report.setCreationDate(new Date());//Ashwini Patil Date:22-09-2022
		
		if(uploadFile.getValue()!=null){
			DocumentUpload doc=uploadFile.getValue();
			String filename="";
			if(doc.getName()!=null&&!doc.getName().equals(""))
				filename=doc.getName();
			else
				filename="assessmentTask"+productSrNo+".jpg";
			
			doc.setUrl(doc.getUrl()+"&filename="+filename);
			report.setDocUpload(uploadFile.getValue());
		}
		
//		if(this.tbArea.getSelectedIndex()!=0){
//			report.setArea(this.tbArea.getValue(tbArea.getSelectedIndex()).trim());
//		}else{
//			report.setArea("NA");
//		}
		
		if(this.oblArea.getSelectedIndex()!=0){
			report.setArea(this.oblArea.getValue(oblArea.getSelectedIndex()).trim());
		}else{
			report.setArea("NA");
		}
		
		if(this.tbLocaltion.getValue().trim()!=null){
			report.setLocation(this.tbLocaltion.getValue().trim());
		}else{
			report.setLocation("NA");
		}
		
		if(this.tbConsequences.getValue().trim()!=null){
			report.setConsequences(this.tbConsequences.getValue().trim());
		}else{
			report.setConsequences("NA");
		}
		
		if(this.olbCategory.getValue(olbCategory.getSelectedIndex()).trim()!=null){
			report.setCategory(this.olbCategory.getValue(this.olbCategory.getSelectedIndex()).trim());
		}
		
		if(olbDeficiencyType.getSelectedIndex()!=0&&this.olbDeficiencyType.getValue(olbDeficiencyType.getSelectedIndex()).trim()!=null){
			report.setDeficiencyType(this.olbDeficiencyType.getValue(this.olbDeficiencyType.getSelectedIndex()).trim());
		}
		
		if(this.olbActionForCompany.getValue().trim()!=null && ! this.olbActionForCompany.getValue().equals("")){
			report.setActionPlanForCompany(this.olbActionForCompany.getValue().trim());
		}
		
		if(this.olbActionPlanforCustomer.getValue().trim()!=null && ! this.olbActionPlanforCustomer.getValue().trim().equals("")){
			report.setActionPlanForCustomer(this.olbActionPlanforCustomer.getValue().trim());
		}
		/**
		 * Date 13-07-2018 By Vijay 
		 * Des :- For CCPM depedent Deficiency 
		 */
		if(olbDeficiencyUnit.getSelectedIndex()!=0&&this.olbDeficiencyUnit.getValue(olbDeficiencyUnit.getSelectedIndex()).trim()!=null){
			report.setDeficiencyUnit(this.olbDeficiencyUnit.getValue(this.olbDeficiencyUnit.getSelectedIndex()).trim());
		}else{
			report.setDeficiencyUnit("NA");
		}
		
		if(tbRiskLevel.getValue()!=null){
			report.setRiskLevel(tbRiskLevel.getValue());
		}
		
		if(oblServiceLocation.getSelectedIndex()!=0){
			report.setServicelocation(oblServiceLocation.getValue(oblServiceLocation.getSelectedIndex()));
		}
		else{
			report.setServicelocation("");
		}
		
		this.assessmentreportTable.getDataprovider().getList().add(report);
		
		
//		this.olbCategory.setSelectedIndex(0);
//		this.olbActionPlanforCustomer.setSelectedIndex(0);
//		this.olbActionForCompany.setSelectedIndex(0);
//		this.olbDeficiencyType.setSelectedIndex(0);
		this.tbConsequences.setValue("");
//		this.tbArea.setValue("");
//		this.tbArea.setSelectedIndex(0);
		this.oblArea.setSelectedIndex(0);
		this.tbLocaltion.setValue("");
		this.tbRiskLevel.setSelectedIndex(0);
		olbActionPlanforCustomer.setValue("");
		olbActionForCompany.setValue("");
		olbCategory.setSelectedIndex(0);
		
		uploadFile.clear();
	}

	public PersonInfoComposite getPersonInfoComposite() {
		return personInfoComposite;
	}

	public void setPersonInfoComposite(PersonInfoComposite personInfoComposite) {
		this.personInfoComposite = personInfoComposite;
	}
	
	/**
	 * sets the id textbox with the passed count value. 
	 */
	@Override
	public void setCount(int count)
	{
      this.assessmentDetailsCount.setValue(count+"");
	}
	
	/****Date :20/11/2017 
    BY :Manisha
    Description :Incase of inspection quotation there should be some schedules for visiting person,
            create single service for assessment single service.**/
    
    public void setEnable(boolean state){
	    super.setEnable(state);
	    this.assessmentDetailsCount.setEnabled(false);
        assessmentreportservicetable.setEnable(state);
        assessmentreportTable.setEnable(state);
        tbstatus.setEnabled(false);
    }


   @Override
    public void clear() {
		super.clear();
		assessmentreportservicetable.clear();
        assessmentreportTable.clear();
        
        oblCustomerBranch.clear();
        oblServiceLocation.clear();
     }


/***********End**********/
	/****Date :20/11/2017 
    BY :Manisha
    Description :Incase of inspection quotation there should be some schedules for visiting person,
       create single service for assessment single service.**/

   private void addAssessmentServicesToTable()
   {
	   AssessmentReportService  pojo_obj=new AssessmentReportService();
	   
	   if(this.prodInfoComposite.getProdID().getValue()!=null)
	   {
		   String i=this.prodInfoComposite.getProdID().getValue();
		   int p=Integer.parseInt(i);
		   pojo_obj.setProductId(p);
	   }
	   
	   if(this.prodInfoComposite.getProdCode().getValue()!=null)
	   {
		   pojo_obj.setProductCode(this.prodInfoComposite.getProdCode().getValue());
		   
	   }
	   
	   if(this.olbemployeeName.getValue(olbemployeeName.getSelectedIndex()).trim()!=null)
	   {
		   pojo_obj.setEmployeeName(this.olbemployeeName.getValue(olbemployeeName.getSelectedIndex()).trim());
	   }
	   
	   if(this.prodInfoComposite.getProdName().getValue()!=null)
	   {
		   pojo_obj.setProductName(this.prodInfoComposite.getProdName().getValue());
	   }
	   
	   if(this.dbServiceDate.getValue() != null)
	   {
		   pojo_obj.setServiceDate(this.dbServiceDate.getValue());
	   }
	   
	   if(this.olbbBranch.getValue()!=null){
		   pojo_obj.setBranch(this.olbbBranch.getValue());
	   }
	   
	   this.assessmentreportservicetable.getDataprovider().getList().add(pojo_obj);
}
         /**End**/

	@Override
	public void setToViewState() {
		super.setToViewState();
		setMenuAsPerStatus();
		
		String mainScreenLabel="ASSESSMENT REPORT";
		if(assesreportobj!=null&&assesreportobj.getCount()!=0){
			mainScreenLabel=assesreportobj.getCount()+" "+"/"+" "+assesreportobj.getStatus();/**+" "+ "/"+" "+AppUtility.parseDate(assesreportobj.getCreationDate());**/
		}
		
		fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
	}
   
	/**
	 * The method is responsible for changing Application state as per 
	 * status
	 */
	public void setMenuAsPerStatus()
	{
		
		this.toggleProcessLevelMenu();
	}

	private void toggleProcessLevelMenu() {

		AssesmentReport entity = (AssesmentReport) presenter.getModel();
		String status = entity.getStatus();

		for (int i = 0; i < getProcesslevelBarNames().length; i++) {
			InlineLabel label = getProcessLevelBar().btnLabels[i];
			String text = label.getText().trim();
			if (status.equals(AssesmentReport.CREATED)) {
				
				if (text.equals("Create Visit"))
					label.setVisible(true);
				if (text.equals("View Service"))
					label.setVisible(true);
				if (text.equals("Mark Close"))
					label.setVisible(true);
				if (text.equals("New"))
					label.setVisible(true);
				
				
				if(entity.getServiceId()!=0){
					if (text.equals("Schedule Assessment"))
						label.setVisible(true);
				}else{
					if (text.equals("Schedule Assessment"))
						label.setVisible(false);
				}
				

			} else if (status.equals(AssesmentReport.CLOSED)) {

				if (text.equals("Create Visit"))
					label.setVisible(false);
				if (text.equals("View Service"))
					label.setVisible(true);
				if (text.equals("Mark Close"))
					label.setVisible(false);
				if (text.equals("Schedule Assessment"))
					label.setVisible(false);
				if (text.equals("New"))
					label.setVisible(true);
			}

		}

	
	}

	@Override
	public void setToEditState() {
		super.setToEditState();
		this.processLevelBar.setVisibleFalse(false);
		
		
		String mainScreenLabel="ASSESSMENT REPORT";
		if(assesreportobj!=null&&assesreportobj.getCount()!=0){
			mainScreenLabel=assesreportobj.getCount()+" "+"/"+" "+assesreportobj.getStatus();/**+" "+ "/"+" "+AppUtility.parseDate(assesreportobj.getCreationDate());**/
		}
		
		fgroupingCustomerInformation.getHeaderLabel().setText(mainScreenLabel);
	}

	

	@Override
	public void refreshTableData() {
		// TODO Auto-generated method stub
		super.refreshTableData();
		assessmentreportTable.getTable().redraw();
		assessmentreportservicetable.getTable().redraw();
		
	}
	
	
	
	public void loadCustomerBranch(final int custId,final String custBranch, final boolean newflag){
		
		
		customerBranchList=new ArrayList<CustomerBranchDetails>();
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(custId);
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerBranchDetails());

		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
//				oblCustomerBranch.clear();
//				oblCustomerBranch.addItem("--SELECT--");
//				oblCustomerBranch.addItem("New");

				showDialogMessage("failed." + caught);
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<CustomerBranchDetails> custBranchList=new ArrayList<CustomerBranchDetails>();
				if(result.size()!=0){
					for(SuperModel model:result){
						CustomerBranchDetails obj=(CustomerBranchDetails) model;
						custBranchList.add(obj);
						customerBranchList.add(obj);
					}
				}
				
				if(custBranchList.size()!=0){
					System.out.println("custBranchList"+custBranchList.get(0).getBusinessUnitName());
					System.out.println("custBranchList size"+custBranchList.size());

//					if(newflag){
//						
//						Comparator<CustomerBranchDetails> comp=new Comparator<CustomerBranchDetails>() {
//							@Override
//							public int compare(CustomerBranchDetails e1, CustomerBranchDetails e2) {
//								
//								if(e1!=null && e2!=null)
//								{
//									if(e2.getCount()== e1.getCount()){
//										return 0;
//									}
//									if(e2.getCount()> e1.getCount()){
//										return 1;
//									}
//									else{
//										return -1;}
//								}
//								else{
//									return 0;
//								}
//								
//							}
//						};
//						Collections.sort(custBranchList,comp);
//					}
					System.out.println("custBranchList"+custBranchList.get(0).getBusinessUnitName());
					String newcustomerBranch = custBranchList.get(0).getBusinessUnitName();
					oblCustomerBranch.setListItems(custBranchList);
					oblCustomerBranch.cinfo=personInfoComposite.getValue();

//					oblCustomerBranch.addItem("New");

					if(custBranch!=null){
////							oblCustomerBranch.customerBranchName=oblCustomerBranch.getValue();
////							oblCustomerBranch.customerBranchId=oblCustomerBranch.getSelectedItem().getCount();
//						}
					}else{
						oblCustomerBranch.setEnabled(true);
					}
					if(newflag){
//						oblCustomerBranch.setValue(newcustomerBranch);
//						System.out.println("customer branch == "+oblCustomerBranch.getValue(oblCustomerBranch.getSelectedIndex()));
						oblServiceLocation.clear();
						loadCustomerBranchServiceLocation(personInfoComposite.getIdValue(), custBranchList.get(0).getBusinessUnitName(), null,null);
					}
				}else{
					oblCustomerBranch.addItem("--SELECT--");
//					oblCustomerBranch.addItem("New");

				}
				
				
			}
		});
	}
	
	

	public void loadCustomerBranchServiceLocation(int custId,final String custBranch,final String serviceLocation, final List<Area> list){
//		customerBranchList=new ArrayList<CustomerBranchDetails>();
		MyQuerry querry = new MyQuerry();
		Company c = new Company();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter = null;
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(custId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("customerBranchName");
		filter.setStringValue(custBranch);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);;
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new CustomerBranchServiceLocation());

		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				oblServiceLocation.clear();
				oblServiceLocation.addItem("--SELECT--");
				showDialogMessage("failed." + caught);
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				ArrayList<CustomerBranchServiceLocation> custBranchList=new ArrayList<CustomerBranchServiceLocation>();
				if(result.size()!=0){
					for(SuperModel model:result){
						CustomerBranchServiceLocation obj=(CustomerBranchServiceLocation) model;
						custBranchList.add(obj);
//						customerBranchList.add(obj);
					}
				}
				oblServiceLocation.clear();
				oblServiceLocation.addItem("--SELECT--");
				
				if(custBranchList.size()!=0){
					oblServiceLocation.setListItems(custBranchList);
					if(serviceLocation!=null){
						oblServiceLocation.setValue(serviceLocation);
					}else{
						oblServiceLocation.setEnabled(true);
					}
					oblServiceLocation.customerBranchName=oblCustomerBranch.getValue();
					oblServiceLocation.customerBranchId=oblCustomerBranch.getSelectedItem().getCount();
					oblServiceLocation.cinfo=personInfoComposite.getValue();
				}else{
					oblServiceLocation.customerBranchName=oblCustomerBranch.getValue();
					oblServiceLocation.customerBranchId=oblCustomerBranch.getSelectedItem().getCount();
					oblServiceLocation.cinfo=personInfoComposite.getValue();
//					oblServiceLocation.addItem("--SELECT--"); //Commented on 25-07-2024 as if no location exist for a customer branch then select was coming twice
					oblServiceLocation.addItem("New");
				}
				
				if(serviceLocation!=null){
					oblServiceLocation.setValue(serviceLocation);
					oblArea.clear();
					oblArea.addItem("--SELECT--");
					oblArea.addItem("New");
					
					CustomerBranchServiceLocation servicelocation = oblServiceLocation.getSelectedItem();
					System.out.println("area 2 size after new created"+servicelocation.getAreaList2().size());
					for(Area area : servicelocation.getAreaList2()){
						oblArea.addItem(area.getArea());
					}
					oblArea.setValue(list.get(0).getArea());
				}
			}
		});
	}
	
	
	public void findMaxServiceSrNumber() {
		int maxSrNumber = 0;
		productSrNo = 0;
		List<AssesmentReportEmbedForTable> list = this.assessmentreportTable.getDataprovider().getList();
		Comparator<AssesmentReportEmbedForTable> comp =new Comparator<AssesmentReportEmbedForTable>() {
			@Override
			public int compare(AssesmentReportEmbedForTable item1,AssesmentReportEmbedForTable item2) {
				
				if (item1.getCount()== item2.getCount()) {
					return 0;
				}else if (item1.getCount()> item2.getCount()) {
					return 1;
				} else {
					return -1;
				}
			}
		};
		Collections.sort(list,comp);
		
		if(list!=null&&list.size()!=0){
			maxSrNumber = list.get(list.size()-1).getCount();
			productSrNo = maxSrNumber;
		}
		Console.log("product sr number "+ productSrNo);
	}
	
   
	
}
