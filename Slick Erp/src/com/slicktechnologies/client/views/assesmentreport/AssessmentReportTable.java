package com.slicktechnologies.client.views.assesmentreport;

import java.util.Date;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.ClickableTextCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.FromAndToDateBoxPopup;
import com.slicktechnologies.client.views.popups.NewEmailPopUp;
import com.slicktechnologies.client.views.project.tool.UploadAssetPopup;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;

public class AssessmentReportTable extends SuperTable<AssesmentReportEmbedForTable> implements ClickHandler{

	Column<AssesmentReportEmbedForTable, String> areaColumn,locationColumn,consequencesColumn;
	TextColumn<AssesmentReportEmbedForTable> categoryColumn,deficiencyTypeColumn,actionPlanByCompanyColumn,actionPlanByCustomerColumn,deficiencyUnitColumn,riskLevelCol;
	Column<AssesmentReportEmbedForTable,String> deleteColumn,uploadButtonCol,closeButtonCol;
	
	TextColumn<AssesmentReportEmbedForTable> viewareaColumn,viewlocationColumn,viewconsequencesColumn,viewSerialNoCol;
	
	UploadAssetPopup uploadPopup=new UploadAssetPopup("Upload Image");
	PopupPanel panel ;
	int rowIndex=-1;
	String locLabel="Observation";
	String consequencesLbl="Impact";
	
	/**
	 *@author Anil @since 15-12-2021
	 *Capturing status, date and remark for task
	 *Raised by Nitin for Prominant
	 */
	TextColumn<AssesmentReportEmbedForTable> viewSatusCol,viewRemarkCol,viewClosureDateCol;
	
	Column<AssesmentReportEmbedForTable, String> viewFileNameCol;
	
	FromAndToDateBoxPopup closurePopup=new FromAndToDateBoxPopup("Close Task","Close Task");
	
	public AssessmentReportTable() {
		super();
		// TODO Auto-generated constructor stub
		uploadPopup.getBtnOk().addClickHandler(this);
		uploadPopup.getBtnCancel().addClickHandler(this);
		
		closurePopup.getBtnOne().addClickHandler(this);
		closurePopup.getBtnTwo().addClickHandler(this);
		
		rowIndex=-1;
		locLabel="Observation";
		consequencesLbl="Impact";
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("AssesmentReport","onlyForNBHC")){
			locLabel="Location";
			consequencesLbl="Likely Consequences";
		}
	}

	@Override
	public void createTable() {
		
		locLabel="Observation";
		consequencesLbl="Impact";
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("AssesmentReport","onlyForNBHC")){
			locLabel="Location";
			consequencesLbl="Likely Consequences";
		}
		
		viewSerialNoCol();
		
		closeButtonCol();
		uploadButtonCol();
		
		viewSatusCol();
		
		
		
		createareaColumn();
		riskLevelCol();
		createcategoryColumn();
		
		createlocationColumn();
		createconsequencesColumn();
		
		if(locLabel.equals("Location")){
			createdeficiencyTypeColumn();
			createdeficiencyUnitColumn();
		}
		
		createactionPlanByCompanyColumn();
		createactionPlanByCustomerColumn();
		
		viewClosureDateCol();
		viewRemarkCol();
		
		createColumndeleteColumn();
		addFieldUpdater();
	}

	protected void uploadButtonCol() {
		ButtonCell btnCell = new ButtonCell();
		uploadButtonCol = new Column<AssesmentReportEmbedForTable, String>(btnCell) {
			@Override
			public String getValue(AssesmentReportEmbedForTable object) {
				return "Upload";
			}
			
			@Override
			public void render(Context context, AssesmentReportEmbedForTable object,SafeHtmlBuilder sb) {
				if(object.getStatus()!=null&&!object.getStatus().equals("")&&object.getStatus().equals("Created")){
					super.render(context, object, sb);
				}
			}
		};
		table.addColumn(uploadButtonCol, "");
		table.setColumnWidth(uploadButtonCol, 90, Unit.PX);
		
		uploadButtonCol.setFieldUpdater(new FieldUpdater<AssesmentReportEmbedForTable, String>() {
			@Override
			public void update(int index,AssesmentReportEmbedForTable object, String value) {
				rowIndex=index;
				uploadPopup.clear();
				
				if(object.getDocUpload()!=null){
					uploadPopup.getUpload().setValue(object.getDocUpload());
				}
				panel=new PopupPanel(true);
				panel.add(uploadPopup);
				panel.show();
				panel.center();
				
				
			}
		});
	}
	private void riskLevelCol() {
		riskLevelCol = new TextColumn<AssesmentReportEmbedForTable>() {
			@Override
			public String getValue(AssesmentReportEmbedForTable object) {
				if(object.getRiskLevel()!=null)
					return object.getRiskLevel();
				else
					return "";
			}
		};
		table.addColumn(riskLevelCol,"Risk Level");
		table.setColumnWidth(riskLevelCol, 100,Unit.PX);
	}


	private void createdeficiencyUnitColumn() {

		deficiencyUnitColumn = new TextColumn<AssesmentReportEmbedForTable>() {
			
			@Override
			public String getValue(AssesmentReportEmbedForTable object) {
				// TODO Auto-generated method stub
				if(object.getDeficiencyUnit()!=null)
					return object.getDeficiencyUnit();
				else
					return "N A";
			}
		};
		table.addColumn(deficiencyUnitColumn,"Unit");
		table.setColumnWidth(deficiencyUnitColumn, 100,Unit.PX);
	}
	
	protected void createColumndeleteColumn() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<AssesmentReportEmbedForTable, String>(btnCell) {
			@Override
			public String getValue(AssesmentReportEmbedForTable object) {
				return "Delete";
			}
		};
		table.addColumn(deleteColumn, "");
		table.setColumnWidth(deleteColumn, 100, Unit.PX);
	}

	protected void createareaColumn() {
		EditTextCell editCell = new EditTextCell();
		areaColumn = new Column<AssesmentReportEmbedForTable, String>(editCell) {
			@Override
			public String getValue(AssesmentReportEmbedForTable object) {
				if (object.getArea() != null) {
					return object.getArea();
				} else {
					return "";
				}
			}
		};
		table.addColumn(areaColumn, "#Area");
		table.setColumnWidth(areaColumn, 100, Unit.PX);
	}
	
	protected void viewareaColumn() {
		viewareaColumn = new TextColumn<AssesmentReportEmbedForTable>() {
			@Override
			public String getValue(AssesmentReportEmbedForTable object) {
				if (object.getArea() != null) {
					return object.getArea();
				} else {
					return "";
				}
			}
		};
		table.addColumn(viewareaColumn, "Area");
		table.setColumnWidth(viewareaColumn, 100, Unit.PX);
	}
	
	protected void createlocationColumn() {
		EditTextCell editCell = new EditTextCell();
		locationColumn = new Column<AssesmentReportEmbedForTable, String>(editCell) {
			@Override
			public String getValue(AssesmentReportEmbedForTable object) {
				if (object.getLocation() != null) {
					return object.getLocation();
				} else {
					return "N A";
				}
			}
		};
		table.addColumn(locationColumn, "#"+locLabel);
		table.setColumnWidth(locationColumn, 100, Unit.PX);
	}
	
	protected void viewlocationColumn() {
		viewlocationColumn = new TextColumn<AssesmentReportEmbedForTable>() {
			@Override
			public String getValue(AssesmentReportEmbedForTable object) {
				if (object.getLocation() != null) {
					return object.getLocation();
				} else {
					return "N A";
				}
			}
		};
		table.addColumn(viewlocationColumn, locLabel);
		table.setColumnWidth(viewlocationColumn, 100, Unit.PX);
	}
	
	protected void createconsequencesColumn() {
		EditTextCell editCell = new EditTextCell();
		consequencesColumn = new Column<AssesmentReportEmbedForTable, String>(editCell) {
			@Override
			public String getValue(AssesmentReportEmbedForTable object) {
				if (object.getConsequences() != null) {
					return object.getConsequences();
				} else {
					return "N A";
				}
			}
		};
		table.addColumn(consequencesColumn, "#"+consequencesLbl);
		table.setColumnWidth(consequencesColumn, 100, Unit.PX);
	}
	
	protected void viewconsequencesColumn() {
		viewconsequencesColumn = new TextColumn<AssesmentReportEmbedForTable>() {
			@Override
			public String getValue(AssesmentReportEmbedForTable object) {
				if (object.getConsequences() != null) {
					return object.getConsequences();
				} else {
					return "N A";
				}
			}
		};
		table.addColumn(viewconsequencesColumn, consequencesLbl);
		table.setColumnWidth(viewconsequencesColumn, 100, Unit.PX);
	}
	
	
	protected void createdeficiencyTypeColumn() {
		deficiencyTypeColumn = new TextColumn<AssesmentReportEmbedForTable>() {
			@Override
			public String getValue(AssesmentReportEmbedForTable object) {
				if (object.getDeficiencyType() != null) {
					return object.getDeficiencyType();
				} else {
					return "N A";
				}
			}
		};
		table.addColumn(deficiencyTypeColumn, "Deficiency");
		table.setColumnWidth(deficiencyTypeColumn, 100, Unit.PX);
	}
	
	
	protected void createcategoryColumn() {
		categoryColumn = new TextColumn<AssesmentReportEmbedForTable>() {
			@Override
			public String getValue(AssesmentReportEmbedForTable object) {
				if (object.getCategory() != null) {
					return object.getCategory();
				} else {
					return "N A";
				}
			}
		};
		table.addColumn(categoryColumn, "Category");
		table.setColumnWidth(categoryColumn, 100, Unit.PX);
	}
	
	
	protected void createactionPlanByCompanyColumn() {
		actionPlanByCompanyColumn = new TextColumn<AssesmentReportEmbedForTable>() {
			@Override
			public String getValue(AssesmentReportEmbedForTable object) {
				if (object.getActionPlanForCompany() != null) {
					return object.getActionPlanForCompany();
				} else {
					return "N A";
				}
			}
		};
		table.addColumn(actionPlanByCompanyColumn, "Action Plan By Company");
		table.setColumnWidth(actionPlanByCompanyColumn, 200, Unit.PX);
	}

	protected void createactionPlanByCustomerColumn() {
		actionPlanByCustomerColumn = new TextColumn<AssesmentReportEmbedForTable>() {
			@Override
			public String getValue(AssesmentReportEmbedForTable object) {
				if (object.getActionPlanForCustomer() != null) {
					return object.getActionPlanForCustomer();
				} else {
					return "N A";
				}
			}
		};
		table.addColumn(actionPlanByCustomerColumn,"Recommendation to Customer");
		table.setColumnWidth(actionPlanByCustomerColumn, 200, Unit.PX);
	}
	
	@Override
	protected void initializekeyprovider() {
		
	}

	@Override
	public void addFieldUpdater() {
		createFieldUpdaterOnAreaColumn();
		createFieldUpdaterOnLocationColumn();
		createFieldUpdaterOnConsequencesColumn();
		createFieldUpdaterdeleteColumn();
	}
	
	
	protected void createFieldUpdaterdeleteColumn(){
		deleteColumn.setFieldUpdater(new FieldUpdater<AssesmentReportEmbedForTable,String>(){
			@Override
			public void update(int index,AssesmentReportEmbedForTable object,String value){
				getDataprovider().getList().remove(object);
				table.redrawRow(index);
			}
		});
	}
	
	protected void createFieldUpdaterOnAreaColumn(){
		areaColumn.setFieldUpdater(new FieldUpdater<AssesmentReportEmbedForTable, String>(){
			@Override
			public void update(int index,AssesmentReportEmbedForTable object,String value){
				try{
					String val1=value.trim();
					object.setArea(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				}catch (NumberFormatException e){

				}
				table.redrawRow(index);
			}
		});
	}
	
	
	protected void createFieldUpdaterOnConsequencesColumn() {
		consequencesColumn.setFieldUpdater(new FieldUpdater<AssesmentReportEmbedForTable, String>() {
			@Override
			public void update(int index,AssesmentReportEmbedForTable object, String value) {

				try {
					String val1 = value.trim();
					object.setConsequences(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				} catch (NumberFormatException e) {

				}
				table.redrawRow(index);
			}
		});
	}
	
	protected void createFieldUpdaterOnLocationColumn() {
		locationColumn.setFieldUpdater(new FieldUpdater<AssesmentReportEmbedForTable, String>() {
			@Override
			public void update(int index,AssesmentReportEmbedForTable object, String value) {

				try {
					String val1 = value.trim();
					object.setLocation(val1);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);

				} catch (NumberFormatException e) {

				}
				table.redrawRow(index);
			}
		});
	}
	
	@Override
	public void setEnable(boolean state) {
		int tablecolcount = this.table.getColumnCount();
		for (int i = tablecolcount - 1; i > -1; i--)
			table.removeColumn(i);
		locLabel="Observation";
		consequencesLbl="Impact";
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("AssesmentReport","onlyForNBHC")){
			locLabel="Location";
			consequencesLbl="Likely Consequences";
		}
		
		
		if (state == true)
			addeditColumn();
		if (state == false)
			addViewColumn();

	}

	private void addViewColumn() {
		viewSerialNoCol();
		
		viewSatusCol();
		viewFileNameCol();
		
		viewareaColumn();
		riskLevelCol();
		createcategoryColumn();
		
		viewlocationColumn();
		viewconsequencesColumn();
		
		
		if(locLabel.equals("Location")){
			createdeficiencyTypeColumn();
			createdeficiencyUnitColumn();
		}
		
		createactionPlanByCompanyColumn();
		createactionPlanByCustomerColumn();
		
		
		viewClosureDateCol();
		viewRemarkCol();
	}

	private void addeditColumn() {
		viewSerialNoCol();
		
		closeButtonCol();
		uploadButtonCol();
		
		viewSatusCol();
		
		createareaColumn();
		riskLevelCol();
		createcategoryColumn();
		
		createlocationColumn();
		createconsequencesColumn();
		
		
		if(locLabel.equals("Location")){
			createdeficiencyTypeColumn();
			createdeficiencyUnitColumn();
		}
		
		createactionPlanByCompanyColumn();
		createactionPlanByCustomerColumn();
		
		viewFileNameCol();
		viewClosureDateCol();
		viewRemarkCol();
		
		createColumndeleteColumn();
		addFieldUpdater();
	}

	@Override
	public void applyStyle() {
		
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if(event.getSource()==uploadPopup.getBtnOk()){
			if(uploadPopup.getUpload().getValue()!=null){
				getDataprovider().getList().get(rowIndex).setDocUpload(uploadPopup.getUpload().getValue());
			}else{
				GWTCAlert alert=new GWTCAlert();
				alert.alert("Please upload file!");
			}
			table.redrawRow(rowIndex);
			panel.hide();
		}
		
		if(event.getSource()==uploadPopup.getBtnCancel()){
			panel.hide();
		}
		
		if(event.getSource()==closurePopup.getBtnOne()){
			
			if(closurePopup.getFromDate().getValue()!=null){
				getDataprovider().getList().get(rowIndex).setClosureDate(closurePopup.getFromDate().getValue());
				getDataprovider().getList().get(rowIndex).setRemark(closurePopup.taRemark.getValue());
				getDataprovider().getList().get(rowIndex).setStatus("Closed");
				table.redrawRow(rowIndex);
				panel.hide();
			}else{
				GWTCAlert alert=new GWTCAlert();
				alert.alert("Please select date!");
			}
			
		}
		
		if(event.getSource()==closurePopup.getBtnTwo()){
			panel.hide();
		}
	}
	
	private void closeButtonCol() {
		ButtonCell btnCell = new ButtonCell();
		closeButtonCol = new Column<AssesmentReportEmbedForTable, String>(btnCell) {
			@Override
			public String getValue(AssesmentReportEmbedForTable object) {
				return "Close";
			}
			
			@Override
			public void render(Context context, AssesmentReportEmbedForTable object,SafeHtmlBuilder sb) {
				if(object.getStatus()!=null&&!object.getStatus().equals("")&&object.getStatus().equals("Created")){
					super.render(context, object, sb);
				}
			}
		};
		table.addColumn(closeButtonCol, "");
		table.setColumnWidth(closeButtonCol, 90, Unit.PX);
		
		closeButtonCol.setFieldUpdater(new FieldUpdater<AssesmentReportEmbedForTable, String>() {
			@Override
			public void update(int index,AssesmentReportEmbedForTable object, String value) {
				rowIndex=index;
				closurePopup.fromDate.setValue(new Date());
				closurePopup.taRemark.setValue("");
				panel=new PopupPanel(true);
				panel.add(closurePopup);
				panel.show();
				panel.center();
				
				
			}
		});
		
	}
	
	protected void viewRemarkCol() {
		viewRemarkCol = new TextColumn<AssesmentReportEmbedForTable>() {
			@Override
			public String getValue(AssesmentReportEmbedForTable object) {
				if (object.getRemark() != null) {
					return object.getRemark();
				} else {
					return "";
				}
			}
		};
		table.addColumn(viewRemarkCol, "Closure Remark");
		table.setColumnWidth(viewRemarkCol, 100, Unit.PX);
	}

	private void viewClosureDateCol() {
		viewClosureDateCol = new TextColumn<AssesmentReportEmbedForTable>() {
			@Override
			public String getValue(AssesmentReportEmbedForTable object) {
				if (object.getClosureDate() != null) {
					return AppUtility.parseDate(object.getClosureDate());
				} else {
					return "";
				}
			}
		};
		table.addColumn(viewClosureDateCol, "Closure Date");
		table.setColumnWidth(viewClosureDateCol, 100, Unit.PX);
	}

	private void viewSatusCol() {
		viewSatusCol = new TextColumn<AssesmentReportEmbedForTable>() {
			@Override
			public String getValue(AssesmentReportEmbedForTable object) {
				if (object.getStatus() != null) {
					return object.getStatus();
				} else {
					return "";
				}
			}
		};
		table.addColumn(viewSatusCol, "Status");
		table.setColumnWidth(viewSatusCol, 100, Unit.PX);		
	}

	private void viewFileNameCol() {
		ClickableTextCell cell=new ClickableTextCell();
		viewFileNameCol = new Column<AssesmentReportEmbedForTable,String>(cell) {
			@Override
			public String getValue(AssesmentReportEmbedForTable object) {
				if (object.getDocUpload() != null&&object.getDocUpload().getName()!=null) {
					return object.getDocUpload().getName();
				} else {
					return "";
				}
			}

			@Override
			public String getCellStyleNames(Context context,AssesmentReportEmbedForTable object) {
				return "blue";
			}
			
			
		};
		table.addColumn(viewFileNameCol, "Uploaded File Name");
		table.setColumnWidth(viewFileNameCol, 180, Unit.PX);	
		
		viewFileNameCol.setFieldUpdater(new FieldUpdater<AssesmentReportEmbedForTable, String>() {

			@Override
			public void update(int index, AssesmentReportEmbedForTable object,String value) {
//				table.getRowElement(index).getStyle().setBackgroundColor("Blue");
				if (object.getDocUpload() != null&&object.getDocUpload().getName()!=null&&object.getDocUpload().getUrl()!=null) {
					final String url = object.getDocUpload().getUrl();
			 		Window.open(url, "test", "enabled");
				}
			}
		});
	}
	
	private void viewSerialNoCol() {
		viewSerialNoCol = new TextColumn<AssesmentReportEmbedForTable>() {
			@Override
			public String getValue(AssesmentReportEmbedForTable object) {
				if (object.getCount() != 0) {
					return object.getCount()+"";
				} else {
					return "";
				}
			}
		};
		table.addColumn(viewSerialNoCol, "Serial No.");
		table.setColumnWidth(viewSerialNoCol, 100, Unit.PX);		
	}

}
