package com.slicktechnologies.client.views.assesmentreport;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.popupscreen.PopupScreen;
import com.slicktechnologies.client.views.customerbranchdetails.servicelocation.Area;
import com.slicktechnologies.client.views.customerbranchdetails.servicelocation.AreaTable;

public class AreaPoup extends PopupScreen{

	
	TextBox tbServiceLocation,tbFrequency;
	TextBox tbArea;
	Button btnAdd;
	AreaTable areaTbl;
	DoubleBox dbnoOfServices;
	
	public AreaPoup(){
		super();
		createGui();
	}
	
	@Override
	public void createScreen() {
	
		tbServiceLocation=new TextBox();
		tbServiceLocation.setEnabled(false);
		
		tbFrequency = new TextBox();
		dbnoOfServices = new DoubleBox();
		tbArea=new TextBox();
		areaTbl=new AreaTable();
		btnAdd=new Button("Add");
		btnAdd.addClickHandler(this);
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder("* Service Location",tbServiceLocation);
		FormField ftbServiceLocation= fbuilder.setMandatory(true).setMandatoryMsg("Service Location is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Area",tbArea);
		FormField ftbArea= fbuilder.setMandatory(false).setMandatoryMsg("Cell No. is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",btnAdd);
		FormField fbtnAdd= fbuilder.setMandatory(false).setMandatoryMsg("Email is mandatory!").setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",areaTbl.getTable());
		FormField fareaTbl= fbuilder.setMandatory(false).setMandatoryMsg("Email is mandatory!").setRowSpan(0).setColSpan(3).build();
		
		fbuilder = new FormFieldBuilder("Planned Service",dbnoOfServices);
		FormField fdbnoOfServices= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("Frequency",tbFrequency);
		FormField ftbFrequency= fbuilder.setRowSpan(0).setColSpan(0).build();
	
		
		FormField[][] formfield = {{ftbServiceLocation,ftbArea,fdbnoOfServices,ftbFrequency,fbtnAdd},{fareaTbl}};
		this.fields=formfield;
		
	}	

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		
		if(event.getSource().equals(btnAdd)){
			if(tbArea.getValue().equals("")){
				showDialogMessage("Please enter area to add!");
				return;
			}
			if(areaTbl.getValue()!=null){
				for(Area obj:areaTbl.getValue()){
					if(obj.getArea().equals(tbArea.getValue())){
						showDialogMessage(obj.getArea() +" is already added.");
						return;
					}
				}
			}
			
			Area obj=new Area();
			obj.setArea(tbArea.getValue());
			
			if(dbnoOfServices.getValue()!=null)
				obj.setPlannedservices(dbnoOfServices.getValue());
			if(tbFrequency.getValue()!=null)
				obj.setFrequency(tbFrequency.getValue());
			
			areaTbl.getDataprovider().getList().add(obj);
			areaTbl.getTable().redraw();
			tbArea.setValue("");
		}
		
		
		if(event.getSource().equals(this.getLblCancel())){
			hidePopUp();
		}
	}
	
	
	

	

}
