package com.slicktechnologies.client.views.assesmentreport;

import java.util.Vector;

import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.fumigation.Fumigation;

public class AssesmentReportPresenterSearchProxy extends SearchPopUpScreen<AssesmentReport>{

	
	public PersonInfoComposite personInfo;
	TextBox tbAssessmentId;
	TextBox tbserviceid, tbcomplainid;
	 ObjectListBox<Branch> olbbBranch; 
	
	public AssesmentReportPresenterSearchProxy() {
		super();
		createGui();
	}

	public void initWidget()
	{
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Customer());
		personInfo=new PersonInfoComposite(querry,false);
		personInfo.getCustomerId().getHeaderLabel().setText("Customer ID");
		personInfo.getCustomerName().getHeaderLabel().setText("Customer Name");
		personInfo.getCustomerCell().getHeaderLabel().setText("Customer Cell");
		
		tbAssessmentId =  new TextBox();
		
		tbserviceid = new TextBox();
		tbcomplainid = new TextBox();
		
		olbbBranch=new ObjectListBox<Branch>();
		AppUtility.makeBranchListBoxLive(olbbBranch);
	}
	
	public void createScreen() {
		initWidget();
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder("",personInfo);
		FormField fpersonInfo= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();	
		
		fbuilder = new FormFieldBuilder("Assessment ID",tbAssessmentId);
		FormField ftbAssessmentId= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();	
		
		/** Date 13-08-2019 by Vijay for NBHC ccpm ***/
		fbuilder = new FormFieldBuilder("Service Id",tbserviceid);
		FormField ftbserviceid = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		fbuilder = new FormFieldBuilder("Complain Id",tbcomplainid);
		FormField ftbcomplainid = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(1).build();
		
		fbuilder = new FormFieldBuilder("Branch",olbbBranch);
		FormField folbbBranch= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		FormField[][] formfield = {
				{ftbAssessmentId,folbbBranch,ftbserviceid,ftbcomplainid},
				{fpersonInfo}
		};
		this.fields=formfield;
	}
	

	@Override
	public MyQuerry getQuerry() {
		

		Vector<Filter> filtervec=new Vector<Filter>();
		  Filter temp=null;

		
		if(personInfo.getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(personInfo.getIdValue());
		  temp.setQuerryString("cinfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(personInfo.getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(personInfo.getFullNameValue());
		  temp.setQuerryString("cinfo.fullName");
		  filtervec.add(temp);
		  }
		  if(personInfo.getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(personInfo.getCellValue());
		  temp.setQuerryString("cinfo.cellNumber");
		  filtervec.add(temp);
		  }
		  
		  if(tbAssessmentId.getValue()!=null && !tbAssessmentId.getValue().trim().equals("")){
				temp=new Filter();
				temp.setQuerryString("count");
				temp.setIntValue(Integer.parseInt(tbAssessmentId.getValue()));
				filtervec.add(temp);
			}
		/**
		 * Date 13-08-2019 by Vijay for NBHC CCPM Service id and complaint ID   
		 */
		if (tbserviceid.getValue() != null && !tbserviceid.getValue().equals("")) {
			temp = new Filter();
			temp.setQuerryString("serviceId");
//			temp.setStringValue(tbserviceid.getValue());
			temp.setIntValue(Integer.parseInt(tbserviceid.getValue()));
			filtervec.add(temp);
		}
		
		if (tbcomplainid.getValue() != null && !tbcomplainid.getValue().equals("")) {
			temp = new Filter();
			temp.setQuerryString("complaintId");
//			temp.setStringValue(tbcomplainid.getValue());
			temp.setIntValue(Integer.parseInt(tbcomplainid.getValue()));
			filtervec.add(temp);
		}
		 
		if(olbbBranch.getSelectedIndex()!=0){
			temp=new Filter();
			temp.setStringValue(olbbBranch.getValue().trim());
			temp.setQuerryString("branch");
			filtervec.add(temp);
		}
		
		  MyQuerry querry= new MyQuerry();
		  querry.setFilters(filtervec);
		  querry.setQuerryObject(new AssesmentReport());
		  return querry;
	}

	@Override
	public boolean validate() {
		
		
		//Ashwini Patil Date:25-01-2024 	
		boolean filterAppliedFlag=false;
		Console.log("filterAppliedFlag before="+filterAppliedFlag);				
		
		if(personInfo.getIdValue()!=-1||olbbBranch.getSelectedIndex()!=0 ||(tbAssessmentId.getValue()!=null && !tbAssessmentId.getValue().equals("")) ||(tbserviceid.getValue()!=null && !tbserviceid.getValue().equals("")) || (tbcomplainid.getValue()!=null && !tbcomplainid.getValue().equals(""))){
			
							filterAppliedFlag=true;
		}
		Console.log("filterAppliedFlag after="+filterAppliedFlag);				
				
		if(!filterAppliedFlag){
				showDialogMessage("Please apply at least one filter!");
				return false;
		}
		
		return true;
	}

}
