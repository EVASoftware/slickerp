package com.slicktechnologies.client.views.assesmentreport;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.slicktechnologies.client.views.assesmentreport.AssesmentReportPresenter.AssessmentSearchTable;
import com.slicktechnologies.shared.common.AssesmentReport;

public class AssesmentReportPresenterSearchTable extends AssessmentSearchTable{

	TextColumn<AssesmentReport> getCustomerCellNumberColumn;
	TextColumn<AssesmentReport> getCustomerFullNameColumn;
	TextColumn<AssesmentReport> getCustomerIdColumn;
	
	/** Date 13-08-2019 by Vijay for NBHC CCPM Service id and complaint Id mapping ****/
	TextColumn<AssesmentReport> getServiceIdColumn;
	TextColumn<AssesmentReport> getComplaintIdColumn;
	
	/**
	 * @author Anil
	 * @since 31-01-2022
	 * Add two more fields, id and status
	 */
	TextColumn<AssesmentReport> getAssessmentIdCol;
	TextColumn<AssesmentReport> getStatusCol;
	
	public AssesmentReportPresenterSearchTable() {
		super();
	}

	@Override
	public void createTable() {
		getAssessmentIdCol();
		addColumngetCustomerId();
		addColumngetCustomerFullName();
		addColumngetCustomerCellNumber();
		
		addColumnReferenceNo1();
		addColumnReferenceNo2();
		
		getStatusCol();
	}
	
	private void getStatusCol() {
		getStatusCol = new TextColumn<AssesmentReport>() {
			@Override
			public String getValue(AssesmentReport object) {
				return object.getStatus();
			}
		};
		table.addColumn(getStatusCol,"Status");
		table.setColumnWidth(getStatusCol,100,Unit.PX);
		getStatusCol.setSortable(true);	
	}

	private void getAssessmentIdCol() {
		getAssessmentIdCol = new TextColumn<AssesmentReport>() {
			@Override
			public String getValue(AssesmentReport object) {
				return object.getCount()+"";
			}
		};
		table.addColumn(getAssessmentIdCol,"Assessment Id");
		table.setColumnWidth(getAssessmentIdCol,90,Unit.PX);
		getAssessmentIdCol.setSortable(true);
	}

	private void addColumnReferenceNo1() {
		getServiceIdColumn = new TextColumn<AssesmentReport>() {
			
			@Override
			public String getValue(AssesmentReport object) {
					
				return object.getServiceId()+"";
				
			}
		};
		table.addColumn(getServiceIdColumn,"Service Id");
		table.setColumnWidth(getServiceIdColumn,110,Unit.PX);
		getServiceIdColumn.setSortable(true);
			
	}

	private void addColumnReferenceNo2() {

		getComplaintIdColumn = new TextColumn<AssesmentReport>() {
			
			@Override
			public String getValue(AssesmentReport object) {
				return object.getComplaintId()+"";
			}
		};
		table.addColumn(getComplaintIdColumn,"Complaint Id");
		table.setColumnWidth(getComplaintIdColumn,110,Unit.PX);
		getComplaintIdColumn.setSortable(true);
	}

	protected void addSortinggetCustomerId()
	{
		List<AssesmentReport> list=getDataprovider().getList();
		columnSort=new ListHandler<AssesmentReport>(list);
		columnSort.setComparator(getCustomerIdColumn, new Comparator<AssesmentReport>()
				{
			@Override
			public int compare(AssesmentReport e1,AssesmentReport e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCinfo().getCount()== e2.getCinfo().getCount()){
						return 0;}
					if(e1.getCinfo().getCount()> e2.getCinfo().getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addColumngetCustomerId()
	{
		getCustomerIdColumn=new TextColumn<AssesmentReport>()
				{
			@Override
			public String getValue(AssesmentReport object)
			{
				if( object.getCinfo().getCount()==-1)
					return "N.A";
				else return object.getCinfo().getCount()+"";
			}
				};
				table.addColumn(getCustomerIdColumn,"Customer ID");
				table.setColumnWidth(getCustomerIdColumn,110,Unit.PX);
				getCustomerIdColumn.setSortable(true);
	}
	
	protected void addSortinggetCustomerFullName()
	{
		List<AssesmentReport> list=getDataprovider().getList();
		columnSort=new ListHandler<AssesmentReport>(list);
		columnSort.setComparator(getCustomerFullNameColumn, new Comparator<AssesmentReport>()
				{
			@Override
			public int compare(AssesmentReport e1,AssesmentReport e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCinfo().getFullName()!=null && e2.getCinfo().getFullName()!=null){
						return e1.getCinfo().getFullName().compareTo(e2.getCinfo().getFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);

	}
	protected void addColumngetCustomerFullName()
	{
		getCustomerFullNameColumn=new TextColumn<AssesmentReport>()
				{
			@Override
			public String getValue(AssesmentReport object)
			{
				return object.getCinfo().getFullName();
			}
				};
				table.addColumn(getCustomerFullNameColumn,"Customer Name");
				table.setColumnWidth(getCustomerFullNameColumn,130,Unit.PX);
				getCustomerFullNameColumn.setSortable(true);
	}
	protected void addSortinggetCustomerCellNumber()
	{
		List<AssesmentReport> list=getDataprovider().getList();
		columnSort=new ListHandler<AssesmentReport>(list);
		columnSort.setComparator(getCustomerCellNumberColumn, new Comparator<AssesmentReport>()
				{
			@Override
			public int compare(AssesmentReport e1,AssesmentReport e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCinfo().getCellNumber()== e2.getCinfo().getCellNumber()){
						return 0;}
					if(e1.getCinfo().getCellNumber()> e2.getCinfo().getCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetCustomerCellNumber()
	{
		getCustomerCellNumberColumn=new TextColumn<AssesmentReport>()
				{
			@Override
			public String getValue(AssesmentReport object)
			{
				return object.getCinfo().getCellNumber()+"";
			}
				};
				table.addColumn(getCustomerCellNumberColumn,"Customer Cell");
				table.setColumnWidth(getCustomerCellNumberColumn,120,Unit.PX);
				getCustomerCellNumberColumn.setSortable(true);
	}


	
	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		super.setEnable(state);
	}

	@Override
	public void addColumnSorting() {
		addSortinggetCustomerId();
		addSortinggetCustomerFullName();
		addSortinggetCustomerCellNumber();
		
		addSortingOnAssessmentId();
		addSortingOnStatus();
	}
	
	private void addSortingOnAssessmentId() {
		List<AssesmentReport> list = getDataprovider().getList();
		columnSort = new ListHandler<AssesmentReport>(list);
		columnSort.setComparator(getAssessmentIdCol,new Comparator<AssesmentReport>() {
			@Override
			public int compare(AssesmentReport e1, AssesmentReport e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() > e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addSortingOnStatus() {
		List<AssesmentReport> list=getDataprovider().getList();
		columnSort=new ListHandler<AssesmentReport>(list);
		columnSort.setComparator(getStatusCol, new Comparator<AssesmentReport>(){
			@Override
			public int compare(AssesmentReport e1,AssesmentReport e2){
				if(e1!=null && e2!=null){
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());
					}
				}else{
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortingServiceId()
	{
		List<AssesmentReport> list=getDataprovider().getList();
		columnSort=new ListHandler<AssesmentReport>(list);
		columnSort.setComparator(getServiceIdColumn, new Comparator<AssesmentReport>()
				{
			@Override
			public int compare(AssesmentReport e1,AssesmentReport e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getServiceId()== e2.getServiceId()){
						return 0;}
					if(e1.getServiceId()> e2.getServiceId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		
		table.addColumnSortHandler(columnSort);
	}
	
	protected void addSortinggetComplaintId()
	{
		List<AssesmentReport> list=getDataprovider().getList();
		columnSort=new ListHandler<AssesmentReport>(list);
		columnSort.setComparator(getComplaintIdColumn, new Comparator<AssesmentReport>()
				{
			@Override
			public int compare(AssesmentReport e1,AssesmentReport e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getComplaintId()== e2.getComplaintId()){
						return 0;}
					if(e1.getComplaintId()> e2.getComplaintId()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	
	
}
