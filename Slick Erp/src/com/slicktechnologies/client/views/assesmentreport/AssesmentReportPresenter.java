package com.slicktechnologies.client.views.assesmentreport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.fumigationdetails.FumigationForm;
import com.slicktechnologies.client.views.fumigationdetails.FumigationPresenter;
import com.slicktechnologies.client.views.fumigationdetails.FumigationPresenterSearchProxy;
import com.slicktechnologies.client.views.fumigationdetails.FumigationPresenterTableProxy;
import com.slicktechnologies.client.views.fumigationdetails.FumigationPresenter.FumigationPresenterTable;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.FromAndToDateBoxPopup;
import com.slicktechnologies.client.views.popups.NewEmailPopUp;
import com.slicktechnologies.client.views.popups.SMSPopUp;
import com.slicktechnologies.client.views.technicianschedule.TechnicianScheduleListPresenter;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;

public class AssesmentReportPresenter extends FormScreenPresenter<AssesmentReport> implements SelectionHandler<Suggestion>,ClickHandler{
	AssesmentReportForm form;
	final GenricServiceAsync genasync=GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	ConditionDialogBox conditionPopup=new ConditionDialogBox("Do you want to print on preprinted Stationery",AppConstants.YES,AppConstants.NO);
	PopupPanel panel;
	
	GeneralServiceAsync genralservice = GWT.create(GeneralService.class);
	
	NewEmailPopUp emailpopup = new NewEmailPopUp();
	SMSPopUp smspopup = new SMSPopUp();
	FromAndToDateBoxPopup scheduleAssessmentPopup=new FromAndToDateBoxPopup("Schedule Assessment","Schedule Assessment");
	
	public AssesmentReportPresenter(UiScreen<AssesmentReport> view,AssesmentReport model) {
		super(view, model);
		form =(AssesmentReportForm)view;
		form.setPresenter(this);
		
		form.getPersonInfoComposite().getId().addSelectionHandler(this);
		form.getPersonInfoComposite().getName().addSelectionHandler(this);
		form.getPersonInfoComposite().getPhone().addSelectionHandler(this);
		
		conditionPopup.getBtnOne().addClickHandler(this);
		conditionPopup.getBtnTwo().addClickHandler(this);
		
		scheduleAssessmentPopup.getBtnOne().addClickHandler(this);
		scheduleAssessmentPopup.getBtnTwo().addClickHandler(this);
	}

	
	public static AssesmentReportForm initalize() {
		AssesmentReportForm  form=new  AssesmentReportForm();
		AssessmentSearchTable gentable = new AssesmentReportPresenterSearchTable();
		gentable.setView(form);
		gentable.applySelectionModle();
		AssesmentReportPresenterSearchProxy.staticSuperTable=gentable;
		AssesmentReportPresenterSearchProxy searchPopup = new AssesmentReportPresenterSearchProxy();
		form.setSearchpopupscreen(searchPopup);
				
	    AssesmentReportPresenter  presenter=new  AssesmentReportPresenter  (form,new AssesmentReport());
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}

	@EntityNameAnnotation(EntityName="com.slicktechnologies.shared.AssesmentReport")
	public static class AssessmentSearchTable extends SuperTable<AssesmentReport> implements GeneratedVariableRefrence
	{

		@Override
		public Object getVarRef(String varName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void createTable() {
			// TODO Auto-generated method stub
			
		}

		@Override
		protected void initializekeyprovider() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void addFieldUpdater() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setEnable(boolean state) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void applyStyle() {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub


        /****Date :20/11/2017 
             BY :Manisha
             Description :Incase of inspection quotation there should be some schedules for visiting person,
                          create single service for assessment single service.**/
		
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals("Create Visit")){
			if(model.getAssessmentreportServiceList().size()==0){
				form.showDialogMessage("Please add product details!!");
			}else{
				MyQuerry querry = new MyQuerry();
//				Company c = new Company();
				Vector<Filter> filtervec = new Vector<Filter>();
				Filter filter = null;
				
				filter = new Filter();
				filter.setQuerryString("companyId");
				filter.setLongValue(model.getCompanyId());
				filtervec.add(filter);
				
				filter = new Filter();
				filter.setQuerryString("contractCount");
				filter.setIntValue(model.getCount());
				filtervec.add(filter);
				
				querry.setFilters(filtervec);
				querry.setQuerryObject(new Service());
				System.out.println("querry completed......");
				genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						System.out.println("Exception occured");
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						System.out.println("22222");
//						System.out.println(result.toString());
						
						try {if (result.size()!=0) {
								System.out.println("try block");
								boolean conf = Window.confirm("Services already exist!! Do you want to create another service");
								if (conf == true) {
									methodtocreateservices();
								}
							} else {
								methodtocreateservices();}
						}
						catch(Exception e){
							System.out.println("Helloo");
							System.out.println(e);
						}
				   } });
     		}
		}
		
		if(text.equals("View Service")){
				AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Assessment Scheduling List",Screen.ASSESSMENTSCHEDULING);
				TechnicianScheduleListPresenter.initialize(true);
//				form.showWaitSymbol();
      	}
		/**End**/
		/**
		 * Date 20-09-2019 by Vijay
		 * Des :- NBHC CCPM Complaint Service Assessment report Mark Close
		 */
		if(text.equals("Mark Close")){
			if(model.getAssessmentDetailsLIst().size()!=0){
				reactonMarkClose();
			}
			else{
				form.showDialogMessage("Please fill all data");
			}
		}
		
		if(text.equals("Schedule Assessment")){
			scheduleAssessmentPopup.fromDate.setValue(null);
			scheduleAssessmentPopup.taRemark.setValue("");
			scheduleAssessmentPopup.tmbServiceTime.setValue(scheduleAssessmentPopup.getCreationTime(new Date()));
			panel=new PopupPanel(true);
			panel.add(scheduleAssessmentPopup);
			panel.show();
			panel.center();
		}
		
		if (text.equals("New")) {
			form.setToNewState();
			initalize();
		}
	}

	


	/****Date :20/11/2017 
    BY :This Method is Created by Manisha
    Description :Incase of inspection quotation there should be some schedules for visiting person,
                create single service for assessment single service.**/
   private void methodtocreateservices()
  {
	
	MyQuerry querry = new MyQuerry();
	Company c = new Company();
	Vector<Filter> filtervec = new Vector<Filter>();
	Filter filter = null;
	filter = new Filter();
	filter.setQuerryString("companyId");
	filter.setLongValue(c.getCompanyId());
	filtervec.add(filter);
	
	filter = new Filter();
	filter.setQuerryString("count");
	filter.setIntValue(model.getCount());
	filtervec.add(filter);
	
//	filter = new Filter();
//	filter.setQuerryString("typeOfOrder");
//	filter.setStringValue(AppConstants.ORDERTYPEFORSERVICE);
//	filtervec.add(filter);
	
	querry.setFilters(filtervec);
	querry.setQuerryObject(new AssesmentReport());
	System.out.println("querry completed......");
	
//	Service ser=new Service();
	
	genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onSuccess(ArrayList<SuperModel> result) {
			// TODO Auto-generated method stub
			for (SuperModel smodel : result) {
			         ServiceProduct serprod=new ServiceProduct();
			         AssesmentReport model=(AssesmentReport)smodel;
				     Service ser=new Service();
				     Long id=model.getCompanyId();
				     System.out.println(id);
				    
				     for(int i=0;i<model.getAssessmentreportServiceList().size();i++)
				     { try{
				        	 int j=model.getCount();
				   	         ser.setContractCount(j);
				   	         int k=model.getAssessmentreportServiceList().get(i).getProductId();
				   	       
				   	         serprod.setProductCode(model.getAssessmentreportServiceList().get(i).getProductCode());
				 	         serprod.setProductName(model.getAssessmentreportServiceList().get(i).getProductName());
				 	         serprod.setCount(k);
				 	         ser.setCompanyId(model.getCompanyId());
				 		     ser.setServiceSrNo(i);
				 		     ser.setServiceDate(model.getAssessmentreportServiceList().get(i).getServiceDate());
				 		     ser.setProduct(serprod);
				 		     ser.setEmployee(model.getAssessmentreportServiceList().get(i).getEmployeeName());
				 		     
				 		    PersonInfo personinfo=new PersonInfo();
					    	personinfo.setFullName(model.getCinfo().getFullName());
					    	personinfo.setEmail(model.getCinfo().getEmail());
					    	personinfo.setPocName(model.getCinfo().getPocName());
					    	personinfo.setCellNumber(model.getCinfo().getCellNumber());
					    	personinfo.setCount(model.getCinfo().getCount());
				    	    ser.setAddress(model.getNewcustomerAddress());
				    	    ser.setBranch(model.getAssessmentreportServiceList().get(i).getBranch());
				    	    ser.setPersonInfo(personinfo);
				    	    ser.setAssessmentServiceFlag(true);
				    	    ser.setStatus("Scheduled");
				         } 
				        catch(Exception e) {
				        	System.out.println(e);
				        	}
				      }
		
				 	  service.save(ser, new AsyncCallback<ReturnFromServer>() {
						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected error occurred!");
						}
						@Override
						public void onSuccess(ReturnFromServer result) {
							form.hideWaitSymbol();
							form.showDialogMessage("Service Created Successfully!");}
					   });
			 }  }
         });
	}

    /**End**/
	@Override
	public void reactOnPrint() {
		
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("AssesmentReport", "CompanyAsLetterhead")){
			panel=new PopupPanel(true);
			panel.add(conditionPopup);
			panel.setGlassEnabled(true);
			panel.show();
			panel.center();
		}
		else{
			final String url = GWT.getModuleBaseURL() + "assessmentReport"+"?Id="+model.getId()+"&"+"preprint="+"plane";
			Window.open(url, "test", "enabled");
		}
		
	}

	@Override
	public void reactOnDownload() {}


	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		emailpopup.showPopUp();
		setEmailPopUpData();
	}
	
	@Override
	public void reactOnSMS() {
		smspopup.showPopUp();
		loadSMSPopUpData();
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		
	}

	
	public void getCustomerAddress(int idValue) {
		// TODO Auto-generated method stub
		

		MyQuerry querry=new MyQuerry();
		Company c=new Company();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter temp=null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(c.getCompanyId());
		filtervec.add(temp);
		
		temp=new Filter();
		temp.setQuerryString("count");
		temp.setIntValue(idValue);
		filtervec.add(temp);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Customer());
		
		genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An Unexpected error occurred!");
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
					for(SuperModel model:result)
					{
						Customer custEntity = (Customer)model;
						
						form.customerAddressComposite.setValue(custEntity.getAdress());
						
						form.customerAddressComposite.setEnable(false);
						form.tbCompanyName.setValue("");
						form.tbCompanyName.setEnabled(false);
						form.tbFullName.setValue("");
						form.tbFullName.setEnabled(false);
						form.pnbCellNo.setValue(0l);
						form.pnbCellNo.setEnabled(false);
						form.etbEmail.setValue("");
						form.etbEmail.setEnabled(false);
					}
				}
			 });
	
	}


	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		if(event.getSource().equals(form.getPersonInfoComposite().getId())||event.getSource().equals(form.getPersonInfoComposite().getName())||event.getSource().equals(form.getPersonInfoComposite().getPhone())){
			getCustomerAddress(form.getPersonInfoComposite().getIdValue());
			
			form.oblCustomerBranch.clear();
			form.loadCustomerBranch(Integer.parseInt(form.getPersonInfoComposite().getId().getValue().trim()), null,false);
			
			form.oblServiceLocation.clear();
//			form.tbArea.clear();
//			form.tbArea.addItem("--SELECT--");
			
			form.oblArea.clear();
			form.oblArea.addItem("--SELECT--");
		}
		
	}
	
	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		
		if(event.getSource().equals(conditionPopup.getBtnOne()))
		{
			final String url = GWT.getModuleBaseURL() + "assessmentReport"+"?Id="+model.getId()+"&"+"preprint="+"yes";
			Window.open(url, "test", "enabled");
			
			panel.hide();
		}
		
		if(event.getSource().equals(conditionPopup.getBtnTwo()))
		{
			final String url = GWT.getModuleBaseURL() + "assessmentReport"+"?Id="+model.getId()+"&"+"preprint="+"no";
			Window.open(url, "test", "enabled");
			
			panel.hide();
		}
		
		if(event.getSource().equals(scheduleAssessmentPopup.getBtnOne())){
			
			if(scheduleAssessmentPopup.getFromDate().getValue()!=null){
				form.showWaitSymbol();
				genralservice.scheduleAssessment(model, scheduleAssessmentPopup.getFromDate().getValue(), scheduleAssessmentPopup.convert24Hrsto12HrsFormat(scheduleAssessmentPopup.tmbServiceTime.getValue()), scheduleAssessmentPopup.taRemark.getValue(), new AsyncCallback<AssesmentReport>() {
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}

					@Override
					public void onSuccess(AssesmentReport result) {
						form.hideWaitSymbol();
						
						if(result==null){
							form.showDialogMessage("Service not found!");
							return;
						}else{
							form.showDialogMessage("Assessment scheduled.");
							model=result;
						}
					}
				});
			}else{
				form.showDialogMessage("Please select assessment date!");
			}
			
			panel.hide();
		}

		if(event.getSource().equals(scheduleAssessmentPopup.getBtnTwo())){
			panel.hide();
		}
			
	}
	
	
	 private void reactonMarkClose() {
		 
		 if(form.assessmentreportTable.getValue()!=null){
			 for(AssesmentReportEmbedForTable object:form.assessmentreportTable.getValue()){
				 if(object.getStatus()!=null){
					 if(object.getStatus().equals("Created")){
						 form.showDialogMessage("Please close all task!");
						 return;
					 }
				 }
			 }
		 }
		 
		 form.showWaitSymbol();
		 genralservice.AssesmentMarkCloseAndSendEmail(model, new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(String result) {
				// TODO Auto-generated method stub
				if(result.equals("Success")){
					model.setStatus(AssesmentReport.CLOSED);
					form.tbstatus.setValue(AssesmentReport.CLOSED);
					form.setToViewState();
					form.showDialogMessage("Assessment mark Closed Sucessfully");
				}
				form.hideWaitSymbol();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				form.hideWaitSymbol();
			}
		});
	 }
	 
	 private void setEmailPopUpData() {
		form.showWaitSymbol();
		String customerEmail = "";

		String branchName = form.olbbAssementBranch.getValue();
		String fromEmailId = AppUtility.getFromEmailAddress(branchName,model.getAssessedPeron1());
		customerEmail = AppUtility.getCustomerEmailid(model.getCinfo().getCount());
		
		String customerName = model.getCinfo().getFullName();
		
		Console.log("Brnach Name : "+branchName+" From Email : "+fromEmailId+" Cust Email : "+customerEmail+" Cust Name : "+customerName);
		if(!customerName.equals("") && !customerEmail.equals("")){
			label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getCinfo().getCount(),customerName,customerEmail,null);
		}
		else{
	
			MyQuerry querry = AppUtility.getcustomerQuerry(model.getCinfo().getCount());
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					for(SuperModel smodel : result){
						Customer custEntity  = (Customer) smodel;
						System.out.println("customer Name = =="+custEntity.getFullname());
						System.out.println("Customer Email = == "+custEntity.getEmail());
						
						if(custEntity.getEmail()!=null){
							label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getCinfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),null);
						}
						else{
							label : AppUtility.loadContactPersonlist(emailpopup.olbContactlistEmailId,"Customer",model.getCinfo().getCount(),custEntity.getFullname(),custEntity.getEmail(),null);
						}
						break;
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
				}
			});
		}
		
		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		Console.log("screenName "+screenName);
		AppUtility.makeLiveSmsTemplateConfig(emailpopup.oblTemplateName,LoginPresenter.currentModule.trim(),screenName);
		emailpopup.taFromEmailId.setValue(fromEmailId);
		emailpopup.smodel = model;
		form.hideWaitSymbol();
		
	}
	 
	 private void loadSMSPopUpData() {
		form.showWaitSymbol();
		String customerCellNo = model.getCinfo().getCellNumber()+"";

		String customerName = model.getCinfo().getFullName();
		if(!customerName.equals("") && !customerCellNo.equals("")){
			AppUtility.loadContactPersonlist(smspopup.olbContactlistEmailId,"Customer",model.getCinfo().getCount(),customerName,customerCellNo,null,true);
		}
		Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
		Console.log("screenName "+screenName);
		AppUtility.makeLiveSmsTemplateConfig(smspopup.oblsmsTemplate,LoginPresenter.currentModule.trim(),screenName,true,AppConstants.SMS);
		smspopup.getRbSMS().setValue(true);
		smspopup.smodel = model;
		form.hideWaitSymbol();
	}
	 
}
