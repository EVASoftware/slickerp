package com.slicktechnologies.client.views.assesmentreport;

import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

@Embed
public class AssesmentReportEmbedForTable extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7096196980844560346L;
	protected String area;
	protected String location;
	@Index
	protected String category;
	@Index
	protected String deficiencyType;
	
	protected String consequences;
	protected String actionPlanForCustomer;
	protected String actionPlanForCompany;
	
	protected String deficiencyUnit;
	
	/**
	 * @author Anil @since 13-10-2021
	 */
	protected String riskLevel;
	protected DocumentUpload docUpload;
	
	
	/**
	 *@author Anil @since 15-12-2021
	 *Capturing status, date and remark for task
	 *Raised by Nitin for Prominant
	 */
	
	protected String status;
	protected Date closureDate;
	protected String remark;
	protected Date creationDate; //Ashwini Patil Date:22-09-2022
	
	protected String servicelocation;
//  getters and setters 
	
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDeficiencyType() {
		return deficiencyType;
	}
	public void setDeficiencyType(String deficiencyType) {
		this.deficiencyType = deficiencyType;
	}
	public String getConsequences() {
		return consequences;
	}
	public void setConsequences(String consequences) {
		this.consequences = consequences;
	}
	public String getActionPlanForCustomer() {
		return actionPlanForCustomer;
	}
	public void setActionPlanForCustomer(String actionPlanForCustomer) {
		this.actionPlanForCustomer = actionPlanForCustomer;
	}
	public String getActionPlanForCompany() {
		return actionPlanForCompany;
	}
	public void setActionPlanForCompany(String actionPlanForCompany) {
		this.actionPlanForCompany = actionPlanForCompany;
	}
	
	
	public String getDeficiencyUnit() {
		return deficiencyUnit;
	}
	public void setDeficiencyUnit(String deficiencyUnit) {
		this.deficiencyUnit = deficiencyUnit;
	}
	
	
	
	public String getRiskLevel() {
		return riskLevel;
	}
	public void setRiskLevel(String riskLevel) {
		this.riskLevel = riskLevel;
	}
	
	
	public DocumentUpload getDocUpload() {
		return docUpload;
	}
	public void setDocUpload(DocumentUpload docUpload) {
		this.docUpload = docUpload;
	}
	
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getClosureDate() {
		return closureDate;
	}
	public void setClosureDate(Date closureDate) {
		this.closureDate = closureDate;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}
	public String getServicelocation() {
		return servicelocation;
	}
	public void setServicelocation(String servicelocation) {
		this.servicelocation = servicelocation;
	}
	
	
	
	
	
	
}
