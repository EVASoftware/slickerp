package com.slicktechnologies.client.views.assesmentreport;

import java.util.Date;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.RowCountChangeEvent;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.AssessmentReportService;
import com.slicktechnologies.shared.common.salesperson.SalesPersonInfo;

          /****Date :20/11/2017 
               BY :This Class is Created by Manisha
               Description :Incase of inspection quotation there should be some schedules for visiting person,
                            create single service for assessment single service.**/

public class AssessmentReportServiceTable extends SuperTable<AssessmentReportService>
{
	
	TextColumn<AssessmentReportService> proIdColumn;
	TextColumn<AssessmentReportService> productCodeColumn;
	Column<AssessmentReportService, String> viewproductCodeColumn;
	
//	TextColumn<AssessmentReportService> productNameColumn;
	
	Column<AssessmentReportService, String>  productNameColumn;
	
//	Column<AssessmentReportService,String>commentColumn;
	TextColumn<AssessmentReportService> employee_nameColumn;
	Column<AssessmentReportService, Date> servicedatecolumn;
	TextColumn<AssessmentReportService> viewdColumn;
	
	Column<AssessmentReportService, String> getColumnserviceDeleteButton;
	TextColumn<AssessmentReportService> getbranchColumn;
	

	NumberFormat nf=NumberFormat.getFormat("0.00");

	@Override
	public void createTable() {
		
		addproIdColumn();
//		addproductCodeColumn();
		addproductNameColumn();
		addemployeeNameColumn();
		addservicedateColumn();
		addbranchColumn();
		addcolumnservicedelete();
		addFieldUpdater();
		

	}

	
	protected void createFieldUpdaterOnServiceDateColumn()
	{
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		servicedatecolumn.setFieldUpdater(new FieldUpdater<AssessmentReportService, Date>() {

			@Override
			public void update(int index, AssessmentReportService object,
					Date value) {
				try{
					object.setServiceDate(value);
					RowCountChangeEvent.fire(table, getDataprovider().getList().size(), true);
				}
				catch (NumberFormatException e)
				{

				}
				table.redrawRow(index);
				
			}
		});
	}
//	
	protected void createViewColumndateColumn()
	{
		viewdColumn=new TextColumn<AssessmentReportService>()
				{
			
			@Override
			public String getValue(AssessmentReportService object) {
				// TODO Auto-generated method stub
				if(object.getServiceDate()!=null){
					return AppUtility.parseDate(object.getServiceDate());
				}
				return "N.A";
			}
		};
				table.addColumn(viewdColumn,"#Visit Date");
				table.setColumnWidth(viewdColumn, 100,Unit.PX);
	}
	
	private void addservicedateColumn() {
      
		
		final DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
		DatePickerCell date = new DatePickerCell(fmt);
		servicedatecolumn=new Column<AssessmentReportService, Date>(date){

			@Override
			public Date getValue(AssessmentReportService object) {
				// TODO Auto-generated method stub
			
				return fmt.parse(fmt.format(object.getServiceDate()));
				
			
			}
		};
		table.addColumn(servicedatecolumn, "#Visit Date");
		table.setColumnWidth(servicedatecolumn, 100, Unit.PX);
	}
	
	
	private void addbranchColumn(){
		getbranchColumn=new TextColumn<AssessmentReportService>() {

			@Override
			public String getValue(AssessmentReportService object) {
				// TODO Auto-generated method stub
				return object.getBranch();
			}
		};
		table.addColumn(getbranchColumn,"Branch");
		table.setColumnWidth(getbranchColumn,100,Unit.PX);
	}

	private void addColumnDeleteUpdater() {
		getColumnserviceDeleteButton.setFieldUpdater(new FieldUpdater<AssessmentReportService, String>() {
		
			@Override
			public void update(int index, AssessmentReportService object,String value) {
				getDataprovider().getList().remove(index);
				table.redrawRow(index);
			}
		});
		
	}



	private void addcolumnservicedelete() {
		
	       ButtonCell btnCell = new ButtonCell();
	       getColumnserviceDeleteButton = new Column<AssessmentReportService, String>(btnCell) {
			
			@Override
			public String getValue(AssessmentReportService object) {
				return "Delete";
			}
		};
		table.addColumn(getColumnserviceDeleteButton, "Delete");
		table.setColumnWidth(getColumnserviceDeleteButton,100,Unit.PX);
	}



	private void addemployeeNameColumn() {
		
//		EditTextCell editCell=new EditTextCell();
		employee_nameColumn=new TextColumn<AssessmentReportService>(){

			@Override
			public String getValue(AssessmentReportService object) {
				return object.getEmployeeName();
			}
		
		};
		
		table.addColumn(employee_nameColumn,"Employee_Name");
		table.setColumnWidth(employee_nameColumn,100,Unit.PX);
	}
		
	
	private void addproductNameColumn() {
		
		productNameColumn=new TextColumn<AssessmentReportService>() {

			@Override
			public String getValue(AssessmentReportService object) {
				// TODO Auto-generated method stub
				return object.getProductName();
			}
		};
		table.addColumn(productNameColumn,"Product Name");
		table.setColumnWidth(productNameColumn,100,Unit.PX);
	}



	private void addproductCodeColumn() {
		productCodeColumn=new TextColumn<AssessmentReportService>() {

			@Override
			public String getValue(AssessmentReportService object) {
				// TODO Auto-generated method stub
				return object.getProductCode();
			}
		};
		
		table.addColumn(productCodeColumn,"PCode");
		table.setColumnWidth(productCodeColumn,100,Unit.PX);
		
	}



	private void addproIdColumn() {
		proIdColumn=new TextColumn<AssessmentReportService>() {

			@Override
			public String getValue(AssessmentReportService object) {
				// TODO Auto-generated method stub
				return Integer.toString(object.getProductId());
			}
		};
		table.addColumn(proIdColumn,"Product ID");
		table.setColumnWidth(proIdColumn,100,Unit.PX);
		// TODO Auto-generated method stub
	}



	
	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		createFieldUpdaterOnServiceDateColumn();
		addColumnDeleteUpdater();
	}

	@Override
	public void setEnable(boolean state) {
		
		for(int i=table.getColumnCount()-1;i>-1;i--)
	    	  table.removeColumn(i); 
	      
	          if(state==true)
	          {
	        	addproIdColumn();
//	      		addproductCodeColumn();
	      		addproductNameColumn();
	      		addemployeeNameColumn();
	      		addservicedateColumn();
	      		addbranchColumn();
	      		addcolumnservicedelete();
	      		addFieldUpdater();
	      		
	         }else
	          {
	        	addproIdColumn();
//	     		addproductCodeColumn();
	     		addproductNameColumn();
	     		addemployeeNameColumn();
	     		createViewColumndateColumn();
//	     		addcolumnservicedelete();
	     		addbranchColumn();
	     		addcolumnviewdelete();
	        }
	}

	private void addcolumnviewdelete() {
		getColumnserviceDeleteButton = new TextColumn<AssessmentReportService>() {
			
			@Override
			public String getValue(AssessmentReportService object) {
				return "";
			}
		};
		table.addColumn(getColumnserviceDeleteButton,"Delete");
		table.setColumnWidth(getColumnserviceDeleteButton,100,Unit.PX);
	 
	}



	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}
	
	
}
