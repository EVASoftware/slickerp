package com.slicktechnologies.client.views.contractpandlreport;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.services.MaterialConsumptionService;
import com.slicktechnologies.client.services.MaterialConsumptionServiceAsync;
import com.slicktechnologies.client.services.XlsxService;
import com.slicktechnologies.client.services.XlsxServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.materialconsumptionreport.MaterialConsumptionReportForm;
import com.slicktechnologies.client.views.materialconsumptionreport.MaterialConsumptionReportPresenter;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.DummyEntityOnlyForScreen;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.ExpenseCost;
import com.slicktechnologies.shared.common.inventory.LabourCost;
import com.slicktechnologies.shared.common.inventory.MaterialConsumptionReport;


public class ContractPAndLReportPresenter  extends FormScreenPresenter<DummyEntityOnlyForScreen> implements SelectionHandler<Suggestion> ,ClickHandler{
	

	
	ContractPAndLReportForm form;
	final GenricServiceAsync async =GWT.create(GenricService.class);
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	final MaterialConsumptionServiceAsync consumptionAsync=GWT.create(MaterialConsumptionService.class);
	
	 NumberFormat fmt = NumberFormat.getFormat("0.00");
	XlsxServiceAsync xlxsService = GWT.create(XlsxService.class);
	
	DateTimeFormat format1= DateTimeFormat.getFormat("dd-MMM-yyyy");
	
	ArrayList<MaterialConsumptionReport> consumptionList=new ArrayList<MaterialConsumptionReport>();
	ArrayList<MaterialConsumptionReport> billRevanueAndQuoList=new ArrayList<MaterialConsumptionReport>();
	public ContractPAndLReportPresenter(FormScreen<DummyEntityOnlyForScreen> view,DummyEntityOnlyForScreen model) {
		super(view, model);
		form =(ContractPAndLReportForm)view;
		form.personInfoComposite.getId().addSelectionHandler(this);
		form.personInfoComposite.getName().addSelectionHandler(this);
		form.personInfoComposite.getPhone().addSelectionHandler(this);
		form.getGobutton().addClickHandler(this);
		
		//  rohan added this on date :29-04-2017
		
		form.BranchLevel.addClickHandler(this);
		form.ContractLevel.addClickHandler(this);
		form.customerLevel.addClickHandler(this);
		form.dateWiseLevel.addClickHandler(this);
		form.oranisationLevel.addClickHandler(this);
		form.SegmentLevel.addClickHandler(this);
		//    ends here 
		form.btAdd.addClickHandler(this);
		form.btEmail.addClickHandler(this);
		
		form.setPresenter(this);
		
		
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		
	}
		

	@Override
	public void reactOnPrint() {
		
	}

	@Override
	public void reactOnEmail() {
		
	}
	
	@Override
	public void reactOnDownload()
	{
		
	}

	

	@Override
	protected void makeNewModel() {
		
	}
	
	public static ContractPAndLReportForm initialize(){
		
		ContractPAndLReportForm form = new ContractPAndLReportForm();
		
		ContractPAndLReportPresenter presenter=new ContractPAndLReportPresenter(form, new DummyEntityOnlyForScreen());		
		AppMemory.getAppMemory().stickPnel(form);
		return form;		 
		
		
	}

	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		if (event.getSource().equals(form.personInfoComposite.getId())||event.getSource().equals(form.personInfoComposite.getName())||
				event.getSource().equals(form.personInfoComposite.getPhone())) {
			
			form.contractCount.clear();
			form.table.connectToLocal();
			form.contractCount.addItem("Select");
			getcontractIdfomCustomerID();
		}
	}

	
	private void getcontractIdfomCustomerID() {
		
		MyQuerry querry = new MyQuerry();
	  	Company c = new Company();
	  	Vector<Filter> filtervec=new Vector<Filter>();
	  	Filter filter = null;
	  	filter = new Filter();
	  	filter.setQuerryString("companyId");
		filter.setLongValue(c.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("cinfo.count");
		filter.setIntValue(Integer.parseInt(form.personInfoComposite.getId().getValue().trim()));
		filtervec.add(filter);
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Contract());
	
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				if(result.size()==0){
					
					form.showDialogMessage("No contract exist for this customer");
					form.personInfoComposite.clear();
				}
				else{
				
				final List<String> contableList=  new ArrayList<String>();
				for(SuperModel model:result)
				{
					
					Contract conEntity = (Contract)model;
					contableList.add(conEntity.getCount()+"");
				}
				
				for(int i=0;i<contableList.size();i++){
				      form.getContractCount().addItem(contableList.get(i));
					 }
			}
			}
			});
	}

	
	double materialNetAmt=0;
	double labousrCostNetAmt=0;
	double expensecostNetAmt=0;
	
	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		
		if(event.getSource().equals(form.getGobutton()))
		{
			form.showWaitSymbol();
			String  id = "" , type = "";
			
			
			if(form.getContractLevel().getValue()){
				id =form.getContractCount().getItemText(form.getContractCount().getSelectedIndex());
				type = "Contract";
			}else if(form.getCustomerLevel().getValue()){
				id = form.getPersonInfoComposite().getIdValue()+"";
				type = "Customer";
			}else if(form.getBranchLevel().getValue()){
				id = form.getOlbbranch().getValue(form.getOlbbranch().getSelectedIndex());
				if(form.dbFromDate.getValue() == null){
					form.showDialogMessage("Please select from date.");
					return;
				}
				if(form.dbToDate.getValue() == null){
					form.showDialogMessage("Please select to date");
					return;
				}
				type = "Branch";
			}else if(form.getDateWiseLevel().getValue()){
				if(form.dbFromDate.getValue() == null){
					form.showDialogMessage("Please select from date.");
					return;
				}
				if(form.dbToDate.getValue() == null){
					form.showDialogMessage("Please select to date");
					return;
				}
				type = "Date";
			}
			final Company c=new Company();
		
			consumptionAsync.retrieveServiceDetails(c.getCompanyId(), id,type ,form.dbFromDate.getValue(),form.dbToDate.getValue(),null,new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onSuccess(ArrayList<MaterialConsumptionReport> result) {
					// TODO Auto-generated method stub
					
					// TODO Auto-generated method stub
					String  branch = "";
					 if(form.getBranchLevel().getValue()){
						 branch = form.getOlbbranch().getValue(form.getOlbbranch().getSelectedIndex());
					 }
					xlxsService.setContractPNLReport(result, form.dbFromDate.getValue(),form.dbToDate.getValue(), branch, model.getCompanyId(), new AsyncCallback<Void>() {
						
						@Override
						public void onSuccess(Void result) {
							String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
							final String url=gwt + "CreateXLXSServlet"+"?type="+6;
							Window.open(url, "test", "enabled");
							form.hideWaitSymbol();
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}
					});
				
				}
				
			});
		
		}
		
		
		if(event.getSource()==form.getCustomerLevel()){
			
			form.olbbranch.setSelectedIndex(0);
			form.olbbranch.setEnabled(false);
			
			form.olbsalesPerson.setSelectedIndex(0);
			form.olbsalesPerson.setEnabled(false);
			
			form.personInfoComposite.setEnable(true);
			
			form.contractCount.setSelectedIndex(0);
			form.contractCount.setEnabled(false);
			
			/**
			 *  nidhi
			 *  9-10-2017
			 *  
			 */
			form.olbbranch.setEnabled(false);
			form.tbSegment.setEnabled(false);
			form.olbSegment.setEnabled(false);
			/**
			 * end
			 */
			
		}
		if(event.getSource()==form.getContractLevel()){
			
			
			form.olbbranch.setSelectedIndex(0);
			form.olbbranch.setEnabled(false);
			
			form.olbsalesPerson.setSelectedIndex(0);
			form.olbsalesPerson.setEnabled(false);
			
			form.dbFromDate.setEnabled(false);
			form.dbFromDate.setValue(null);
			form.dbToDate.setEnabled(false);
			form.dbToDate.setValue(null);
			
			form.personInfoComposite.setEnable(true);
			form.contractCount.setEnabled(true);
			/**
			 *  nidhi
			 *  9-10-2017
			 *  
			 */
			form.olbbranch.setEnabled(false);
			form.tbSegment.setEnabled(false);
			form.olbSegment.setEnabled(false);
			/**
			 * end
			 */
		}
		
		if(event.getSource()==form.getBranchLevel()){
			
			
			form.olbsalesPerson.setSelectedIndex(0);
			form.olbsalesPerson.setEnabled(false);
			
			form.personInfoComposite.clear();
			form.personInfoComposite.setEnable(false);
			
			form.contractCount.setSelectedIndex(0);
			form.contractCount.setEnabled(false);
			
			form.olbbranch.setEnabled(true);
			
			/**
			 *  nidhi
			 *  9-10-2017
			 *  
			 */
			form.tbSegment.setEnabled(false);
			form.olbSegment.setEnabled(false);
			form.dbFromDate.setEnabled(true);
			form.dbFromDate.setValue(null);
			form.dbToDate.setEnabled(true);
			form.dbToDate.setValue(null);
			/**
			 * end
			 */
		}
		if(event.getSource().equals(form.btAdd)){
			if(form.tbEmailBox.getValue() != null){
				EmployeeBranch branch = new EmployeeBranch();
				branch.setBranchName(form.tbEmailBox.getValue());				
				form.emailTable.getDataprovider().getList().add(branch);
			}else{
				form.showDialogMessage("Please add emailId.");
			}
		}
		if(event.getSource().equals(form.btEmail)){
			if(form.emailTable.getDataprovider().getList().size() > 0){	
				ArrayList<String> emailIdList = new ArrayList<String>();
				for(EmployeeBranch branch : form.emailTable.getDataprovider().getList()){
					emailIdList.add(branch.getBranchName());
				}
				

				form.showWaitSymbol();
				String  id = "" , type = "";
				
				
				if(form.getContractLevel().getValue()){
					id =form.getContractCount().getItemText(form.getContractCount().getSelectedIndex());
					type = "Contract";
				}else if(form.getCustomerLevel().getValue()){
					id = form.getPersonInfoComposite().getIdValue()+"";
					type = "Customer";
				}else if(form.getBranchLevel().getValue()){
					id = form.getOlbbranch().getValue(form.getOlbbranch().getSelectedIndex());
			
					type = "Branch";
				}else if(form.getDateWiseLevel().getValue()){
					if(form.dbFromDate.getValue() == null){
						form.showDialogMessage("Please select from date.");
						return;
					}
					if(form.dbToDate.getValue() == null){
						form.showDialogMessage("Please select to date");
						return;
					}
					type = "Date";
				}
				final Company c=new Company();
			
				consumptionAsync.retrieveServiceDetails(c.getCompanyId(), id,type ,form.dbFromDate.getValue(),form.dbToDate.getValue(),emailIdList,new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onSuccess(ArrayList<MaterialConsumptionReport> result) {
						// TODO Auto-generated method stub
						form.hideWaitSymbol();
						if(result ==  null){
							form.showDialogMessage("Email sent successfully.");
						}
						
//						String  branch = "";
//						 if(form.getBranchLevel().getValue()){
//							 branch = form.getOlbbranch().getValue(form.getOlbbranch().getSelectedIndex());
//						 }
//						xlxsService.setContractPNLReport(result, form.dbFromDate.getValue(),form.dbToDate.getValue(), branch, model.getCompanyId(), new AsyncCallback<Void>() {
//							
//							@Override
//							public void onSuccess(Void result) {
//								String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
//								final String url=gwt + "CreateXLXSServlet"+"?type="+6;
//								Window.open(url, "test", "enabled");
//								form.hideWaitSymbol();
//							}
//							
//							@Override
//							public void onFailure(Throwable caught) {
//								// TODO Auto-generated method stub
//								
//							}
//						});
					
					}
					
				});
			
			
				
			}else{
				form.showDialogMessage("Please add emailId in email information.");
			}
		}
		if(event.getSource()==form.getDateWiseLevel()){
			
			
			form.olbsalesPerson.setSelectedIndex(0);
			form.olbsalesPerson.setEnabled(false);
			
			form.personInfoComposite.clear();
			form.personInfoComposite.setEnable(false);
			
			form.contractCount.setSelectedIndex(0);
			form.contractCount.setEnabled(false);
			form.tbSegment.setEnabled(false);
			form.olbSegment.setEnabled(false);
			form.dbFromDate.setEnabled(true);
			form.dbFromDate.setValue(null);
			form.dbToDate.setEnabled(true);
			form.dbToDate.setValue(null);
		}
	}
	

	/**
	 * rohan added this method o validate from and to date is of same month
	 * @return
	 */
	private boolean ValidateDate() {
		
		Date fromdate = form.dbFromDate.getValue();
		int fromMonth =fromdate.getMonth();
		Date todate = form.dbToDate.getValue();
		int toMonth =todate.getMonth();
		
		
		if(fromMonth==toMonth){
			return true;
		}else{
		
			form.showDialogMessage("From date and to date should be of same month..!");
			return false;
		}
		
	}

	private void getGrandTotal(String conOrcustCount, String type){

		System.out.println("In grand total");
		double grandTotal=0;
		
			 System.out.println("materal cost =="+form.getNetAmt().getValue());
			 System.out.println("Labour cost =="+form.getLabourcostNetAmt().getValue());
			 System.out.println("Expense cost =="+form.getExpesecostNetAmt().getValue());

			 if(form.getNetAmt().getValue()==null)
				 form.getNetAmt().setValue(0d);
			 if(form.getLabourcostNetAmt().getValue()==null)
				 form.getLabourcostNetAmt().setValue(0d);
			 if(form.getExpesecostNetAmt().getValue()==null)
				 form.getExpesecostNetAmt().setValue(0d);
			 if(form.getDbadministratorCost().getValue()==null)
				 form.getDbadministratorCost().setValue(0d);
			 
		grandTotal = form.getNetAmt().getValue()+form.getLabourcostNetAmt().getValue()+form.getExpesecostNetAmt().getValue()+form.getDbadministratorCost().getValue();
		form.getGrandTotal().setValue(grandTotal);
		System.out.println(" grand total === "+grandTotal);
		
		// summary total
		
		if(type.equalsIgnoreCase("Contract")){
			int contractid = Integer.parseInt(form.getContractCount().getItemText(form.getContractCount().getSelectedIndex()));
			System.out.println(" contract idd ==="+contractid);
			
			MyQuerry querry = new MyQuerry();
			Vector<Filter> filtervec = new Vector<Filter>();
			Filter filter =null;
					
			filter = new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(contractid);
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(model.getCompanyId());
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Contract());
			
			async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					System.out.println("in success "+result.size());
					for(SuperModel model: result){
						Contract cont = (Contract) model;
						
						form.getContractAmount().setValue(cont.getTotalAmount()+"");
						
					}
					System.out.println("  material cost "+form.getTbmaterialcost().getValue());
					System.out.println("labour cost =="+form.getTblabourcost().getValue());
					if(form.getTbmaterialcost().getValue().equals(""))
						form.getTbmaterialcost().setValue(0+"");
					if(form.getTblabourcost().getValue().equals(""))
						form.getTblabourcost().setValue(0+"");
					if(form.getTbexpensecost().getValue().equals(""))
						form.getTbexpensecost().setValue(0+"");
					if(form.getTbadmincost().getValue().equals(""))
						form.getTbadmincost().setValue(0+"");
					
					double materialcost = Double.parseDouble(form.getTbmaterialcost().getValue());
					double labourcost = Double.parseDouble(form.getTblabourcost().getValue());
					double expensecost = Double.parseDouble(form.getTbexpensecost().getValue());
					double admincost = Double.parseDouble(form.getTbadmincost().getValue());
					double total = materialcost+labourcost+expensecost+admincost;
					form.getTbfinaltotal().setValue(total+"");
					double profitloss = Double.parseDouble(form.getContractAmount().getValue()) - Double.parseDouble(form.getTbfinaltotal().getValue());
					String profitlossvalue = fmt.format(profitloss);
					form.getTbprofitloss().setValue(profitlossvalue);
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});
			
			

		}
		else{
			
		

		String contractid = conOrcustCount;
		System.out.println(" contract idd ==="+contractid);
		
		MyQuerry querry = new MyQuerry();
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter filter =null;
				
		if(type.equals("Customer")){
			filter = new Filter();
			filter.setQuerryString("cinfo.count");
			filter.setIntValue(Integer.parseInt(contractid));
			filtervec.add(filter);
		}else if(type.equals("segment")){
			filter = new Filter();
			filter.setQuerryString("segment");
			filter.setStringValue(contractid);
			filtervec.add(filter);
		}else if(type.equals("branch")){
			filter = new Filter();
			filter.setQuerryString("branch");
			filter.setStringValue(contractid);
			filtervec.add(filter);
		}
		
		
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(model.getCompanyId());
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setStringValue("Approved");
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Contract());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("in success "+result.size());
				
		List<Contract> conList=new ArrayList<Contract>();		
				for(SuperModel model: result){
					Contract cont = (Contract) model;
					conList.add(cont);
					
				}
				
				///  rohan added this 
				double conAmt=0;
				for (int i = 0; i < conList.size(); i++) {
					conAmt = conAmt + conList.get(i).getTotalAmount();
				}
				form.getContractAmount().setValue(conAmt+"");
				
				
				System.out.println("  material cost "+form.getTbmaterialcost().getValue());
				System.out.println("labour cost =="+form.getTblabourcost().getValue());
				if(form.getTbmaterialcost().getValue().equals(""))
					form.getTbmaterialcost().setValue(0+"");
				if(form.getTblabourcost().getValue().equals(""))
					form.getTblabourcost().setValue(0+"");
				if(form.getTbexpensecost().getValue().equals(""))
					form.getTbexpensecost().setValue(0+"");
				if(form.getTbadmincost().getValue().equals(""))
					form.getTbadmincost().setValue(0+"");
				
				double materialcost = Double.parseDouble(form.getTbmaterialcost().getValue());
				double labourcost = Double.parseDouble(form.getTblabourcost().getValue());
				double expensecost = Double.parseDouble(form.getTbexpensecost().getValue());
				double admincost = Double.parseDouble(form.getTbadmincost().getValue());
				double total = materialcost+labourcost+expensecost+admincost;
				form.getTbfinaltotal().setValue(total+"");
				double profitloss = Double.parseDouble(form.getContractAmount().getValue()) - Double.parseDouble(form.getTbfinaltotal().getValue());
				String profitlossvalue = fmt.format(profitloss);
				form.getTbprofitloss().setValue(profitlossvalue);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	
		}
	}
	
	
	private void getAdministrativeCost() {
		System.out.println(" in administrative cost");
		
		String contractdate =   form.getContractDate().getValue();
		System.out.println(" string contarct date ="+contractdate);
		
		
		consumptionAsync.retrieveAdministrativeCost(model.getCompanyId(), contractdate, new AsyncCallback<ArrayList<String>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<String> result) {
				// TODO Auto-generated method stub
				System.out.println(" value ===="+result);
//				if(result.size()==0){
//					form.showDialogMessage("No Financial Year found for this contract Date");
//				}else{
//					
//					form.dbadministratorCost.setValue(Double.parseDouble(result.get(0)));
//					form.tbadmincost.setValue(fmt.format(Double.parseDouble(result.get(0))));
//					form.tbfinancialYear.setValue(result.get(1));
//
//				}
				
			}
		});
		
		
		
	}
	
	
	ArrayList<ExpenseCost> expensecostlist = new ArrayList<ExpenseCost>();
	private void getExpenseCostList() {

		final Company c=new Company();
		consumptionAsync.retrieveExpenseCost(c.getCompanyId(), form.getContractCount().getItemText(form.getContractCount().getSelectedIndex()),"Contract" ,form.dbFromDate.getValue(),form.dbToDate.getValue(), new AsyncCallback<ArrayList<ExpenseCost>>() {

			@Override
			public void onFailure(Throwable caught){
				
			}

			@Override
			public void onSuccess(ArrayList<ExpenseCost> result) {

				if (result.size()!=0) {
					
					expensecostlist.addAll(result);
			//		form.getExpensetable().getDataprovider().setList(expensecostlist);
					double expenseCostNetAmt = 0;
					for(int i=0;i<expensecostlist.size();i++){
						expenseCostNetAmt+=expensecostlist.get(i).getExpenseAmt();
					}
					String expensecost = fmt.format(expenseCostNetAmt);

					form.getExpesecostNetAmt().setValue(Double.parseDouble(expensecost));
					form.getTbexpensecost().setValue(expensecost);
				}
			}
		} );
		
	}	
	
ArrayList<LabourCost> labourcostlist = new ArrayList<LabourCost>();
	
	private void getLabourCostList(String conOrCUstCount, String type) {
		final Company c=new Company();
		
		consumptionAsync.retrieveLabourCost(c.getCompanyId(),conOrCUstCount,type,form.dbFromDate.getValue(),form.dbToDate.getValue(), new AsyncCallback<ArrayList<LabourCost>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<LabourCost> result) {
				// TODO Auto-generated method stub
				System.out.println(" In successsss");
				
				if(result.size()!=0){
					labourcostlist.addAll(result);
	//				form.getCosttable().getDataprovider().setList(labourcostlist);
					double labourNetCost =0;
					for(int i=0;i<result.size();i++){
						
						 labourNetCost += result.get(i).getLabourCost();
					}
					
					String labourcost = fmt.format(labourNetCost);
					form.getLabourcostNetAmt().setValue(Double.parseDouble(labourcost));
					form.getTblabourcost().setValue(labourcost);
				}
			}
		});
		
	}
	
	
//	ArrayList<MaterialConsumptionReport> consumptionList=new ArrayList<MaterialConsumptionReport>();
	protected void getMinList()
	{
		final Company c=new Company();
		
		consumptionAsync.retriveMaterialDetails(c.getCompanyId(), form.getContractCount().getItemText(form.getContractCount().getSelectedIndex()),"Contract" ,form.dbFromDate.getValue(),form.dbToDate.getValue(),new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

			@Override
			public void onFailure(Throwable caught) {
				form.showDialogMessage("An unexpected error occurred. Please try again!");
			}

			@Override
			public void onSuccess(ArrayList<MaterialConsumptionReport> result) {
				if(result.size()==0)
				{
					form.showDialogMessage("Material is not issued yet!");
				}
				else
				{
					consumptionList.addAll(result);
					consumptionAsync.retriveMaterialMMNDetails(c.getCompanyId(), form.getContractCount().getItemText(form.getContractCount().getSelectedIndex()),"Contract",form.dbFromDate.getValue(),form.dbToDate.getValue(), new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An unexpected error occurred. Please try again!");
						}

						@Override
						public void onSuccess(ArrayList<MaterialConsumptionReport> result) {
							if(result.size()!=0)
							{
								consumptionList.addAll(result);
								
							}
							if(consumptionList.size()!=0)
							{
								form.getTable().getDataprovider().setList(consumptionList);
								double sum=0;
								double totVal=0;
								
								for(int f=0;f<consumptionList.size();f++)
								{
									if(consumptionList.get(f).getDocType().trim().equals("MMN"))
									{
										totVal=-(consumptionList.get(f).getPrice()*consumptionList.get(f).getQuantity());
									}
									else{
										totVal=consumptionList.get(f).getPrice()*consumptionList.get(f).getQuantity();
									}
									sum=sum+totVal;
								}
								form.getNetAmt().setValue(sum);
								String materialAmt = fmt.format(sum);
								form.getTbmaterialcost().setValue(materialAmt);
							}
						}
					});
				}
			}
		});
	}
	
	
	/**
	 * rohan added this method for calculating customer wise contract P & L 
	 */
		private void calcutatePAndLCustomerWsie(final String customerID) {
			
			final Company c=new Company();
			
			consumptionAsync.retriveMaterialDetails(c.getCompanyId(),customerID,"Customer",form.dbFromDate.getValue(),form.dbToDate.getValue(), new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

				@Override
				public void onFailure(Throwable caught) {
					form.showDialogMessage("An unexpected error occurred. Please try again!");
				}

				@Override
				public void onSuccess(ArrayList<MaterialConsumptionReport> result) {
					if(result.size()==0)
					{
						form.showDialogMessage("Material is not issued yet!");
					}
					else
					{
						consumptionList.addAll(result);
						consumptionAsync.retriveMaterialMMNDetails(c.getCompanyId(),customerID,"Customer",form.dbFromDate.getValue(),form.dbToDate.getValue(), new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An unexpected error occurred. Please try again!");
							}

							@Override
							public void onSuccess(ArrayList<MaterialConsumptionReport> result) {
								if(result.size()!=0)
								{
									consumptionList.addAll(result);
									
								}
								if(consumptionList.size()!=0)
								{
									form.getTable().getDataprovider().setList(consumptionList);
									double sum=0;
									double totVal=0;
									
									for(int f=0;f<consumptionList.size();f++)
									{
										if(consumptionList.get(f).getDocType().trim().equals("MMN"))
										{
											totVal=-(consumptionList.get(f).getPrice()*consumptionList.get(f).getQuantity());
										}
										else{
											totVal=consumptionList.get(f).getPrice()*consumptionList.get(f).getQuantity();
										}
										sum=sum+totVal;
									}
									form.getNetAmt().setValue(sum);
									String materialAmt = fmt.format(sum);
									form.getTbmaterialcost().setValue(materialAmt);
								}
							}
						});
					}
				}
			});
		
		}
		
		
	/**
	 * ends here 
	 */
		
		
		private void calculateExpenseDetailsCustomerWise(String customerID, String type) {
			
			final Company c=new Company();
			consumptionAsync.retrieveExpenseCost(c.getCompanyId(), customerID,type ,form.dbFromDate.getValue(),form.dbToDate.getValue(), new AsyncCallback<ArrayList<ExpenseCost>>() {

				@Override
				public void onFailure(Throwable caught){
					
				}

				@Override
				public void onSuccess(ArrayList<ExpenseCost> result) {

					if (result.size()!=0) {
						
						expensecostlist.addAll(result);
		//				form.getExpensetable().getDataprovider().setList(expensecostlist);
						double expenseCostNetAmt = 0;
						for(int i=0;i<expensecostlist.size();i++){
							expenseCostNetAmt+=expensecostlist.get(i).getExpenseAmt();
						}
						String expensecost = fmt.format(expenseCostNetAmt);

						form.getExpesecostNetAmt().setValue(Double.parseDouble(expensecost));
						form.getTbexpensecost().setValue(expensecost);
					}
				}
			} );
		}	
		
		
	void calcutatePAndLBranchWsie(String Branch,Date fromDate, Date endDate,String optType){
		try {

			
			final Company c=new Company();
			
			consumptionAsync.retriveMaterialDetails(c.getCompanyId(), Branch,optType,form.dbFromDate.getValue(),form.dbToDate.getValue(), new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

				@Override
				public void onFailure(Throwable caught) {
					form.showDialogMessage("An unexpected error occurred. Please try again!");
				}

				@Override
				public void onSuccess(ArrayList<MaterialConsumptionReport> result) {
					if(result.size()==0)
					{
						form.showDialogMessage("Material is not issued yet!");
					}
					else
					{
						consumptionList.addAll(result);
						consumptionAsync.retriveMaterialMMNDetails(c.getCompanyId(), form.getOlbbranch().getItemText(form.getOlbbranch().getSelectedIndex()),"branch",form.dbFromDate.getValue(),form.dbToDate.getValue(), new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An unexpected error occurred. Please try again!");
							}

							@Override
							public void onSuccess(ArrayList<MaterialConsumptionReport> result) {
								if(result.size()!=0)
								{
									consumptionList.addAll(result);
									
								}
								if(consumptionList.size()!=0)
								{
									form.getTable().getDataprovider().setList(consumptionList);
									double sum=0;
									double totVal=0;
									
									for(int f=0;f<consumptionList.size();f++)
									{
										if(consumptionList.get(f).getDocType().trim().equals("MMN"))
										{
											totVal=-(consumptionList.get(f).getPrice()*consumptionList.get(f).getQuantity());
										}
										else{
											totVal=consumptionList.get(f).getPrice()*consumptionList.get(f).getQuantity();
										}
										sum=sum+totVal;
									}
									form.getNetAmt().setValue(sum);
									String materialAmt = fmt.format(sum);
									form.getTbmaterialcost().setValue(materialAmt);
								}
							}
						});
					}
				}
			});
		
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
		
	void calcutatePAndLBranchWsieBomBase(final String Branch,Date fromDate, Date endDate,final String optType){
		try {

			
			final Company c=new Company();
//			consumptionAsync.retriveMaterialDetails(c.getCompanyId(),customerID,"Customer",form.dbFromDate.getValue(),form.dbToDate.getValue(), new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {
			consumptionAsync.retriveMaterialDetailsBaseOnBOM(c.getCompanyId(), Branch,optType,form.dbFromDate.getValue(),form.dbToDate.getValue(), new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

				@Override
				public void onFailure(Throwable caught) {
					form.showDialogMessage("An unexpected error occurred. Please try again!");
				}

				@Override
				public void onSuccess(ArrayList<MaterialConsumptionReport> result) {
					if(result.size()==0)
					{
						form.showDialogMessage("Material is not issued yet!");
					}
					else
					{
						consumptionList.addAll(result);
						consumptionAsync.retriveMaterialMMNDetails(c.getCompanyId(), Branch,optType,form.dbFromDate.getValue(),form.dbToDate.getValue(), new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

							@Override
							public void onFailure(Throwable caught) {
								form.showDialogMessage("An unexpected error occurred. Please try again!");
							}

							@Override
							public void onSuccess(ArrayList<MaterialConsumptionReport> result) {
								if(result.size()!=0)
								{
									consumptionList.addAll(result);
									
								}
								if(consumptionList.size()!=0)
								{
									form.getTable().getDataprovider().setList(consumptionList);
									double sum=0;
									double totVal=0;
									final HashSet<Integer> contract = new HashSet<Integer>();
									final HashSet<Integer> serSet = new HashSet<Integer>();
									final HashMap<Integer,HashSet<Integer>> contSer  = new HashMap<Integer, HashSet<Integer>>();
									for(int f=0;f<consumptionList.size();f++)
									{
										if(consumptionList.get(f).getDocType().trim().equals("MMN"))
										{
											totVal=-(consumptionList.get(f).getPrice()*consumptionList.get(f).getQuantity());
										}
										else{
											totVal=consumptionList.get(f).getPrice()*consumptionList.get(f).getQuantity();
										}
										contract.add(consumptionList.get(f).getContractId());
										if(consumptionList.get(f).getQuotationId()!=null 
												&& consumptionList.get(f).getQuotationId().size()>0){
											serSet.addAll(consumptionList.get(f).getQuotationId());
										}
										
										if(contSer.containsKey(consumptionList.get(f).getContractId())){
											contSer.get(consumptionList.get(f).getContractId()).add(consumptionList.get(f).getServiceId());
										}else{
											HashSet<Integer> serSet1 = new HashSet<Integer>();
											serSet1.add(consumptionList.get(f).getServiceId());
											if(consumptionList.get(f).getQuotationId()!=null 
													&& consumptionList.get(f).getQuotationId().size()>0){
												serSet.addAll(consumptionList.get(f).getQuotationId());
											}

											contSer.put(consumptionList.get(f).getContractId(), serSet1);
										}
										
										sum=sum+totVal;
									}
									form.getNetAmt().setValue(sum);
									String materialAmt = fmt.format(sum);
									form.getTbmaterialcost().setValue(materialAmt);
									
									if(serSet.size()>0){
										consumptionAsync.retriveServicePlannedRevanueAndBillingRevanue(null, contract, serSet, contSer, c.getCompanyId(),new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

											@Override
											public void onFailure(Throwable caught) {
												// TODO Auto-generated method stub
												
											}

											@Override
											public void onSuccess(ArrayList<MaterialConsumptionReport> result) {
												
												billRevanueAndQuoList.clear();
												
												if(result.size()!=0)
												{
													billRevanueAndQuoList.addAll(result);
													if(billRevanueAndQuoList.get(0).getQuotationId() !=null && 
															billRevanueAndQuoList.get(0).getQuotationId().size()>0)
													getPlannedLabourCost(billRevanueAndQuoList, contract, serSet, contSer, c.getCompanyId());
												}
												
											}
										
										});
									}
									
								}
							}
						});
					}
				}
			});
		
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	void getPlannedLabourCost(ArrayList<MaterialConsumptionReport> ServiceContractDetails
			,HashSet<Integer> contract,HashSet<Integer> serSet ,HashMap<Integer,HashSet<Integer>> contSer,long companyID){
		

		consumptionAsync.retrivelabourPlannedRevanueAndBillingRevanue(billRevanueAndQuoList, contract, serSet, contSer,companyID,new AsyncCallback<ArrayList<MaterialConsumptionReport>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<MaterialConsumptionReport> result) {
				
				if(result.size()!=0)
				{
					if(billRevanueAndQuoList.get(0).getQuotationId() !=null && 
							billRevanueAndQuoList.get(0).getQuotationId().size()>0){
						billRevanueAndQuoList.clear();
						billRevanueAndQuoList.addAll(result);
						for(int i=0; i<result.size();i++){
							for(Integer quoCnt : result.get(0).getPlannedQuoCost().keySet()){
								System.out.println("get" + quoCnt + " cost -- "+ result.get(0).getPlannedQuoCost().get(quoCnt));
							}
							
						}
					}
				}
				
			}
		
		});
	
			
	}




}