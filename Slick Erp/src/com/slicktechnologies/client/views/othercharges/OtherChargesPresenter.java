package com.slicktechnologies.client.views.othercharges;

import java.util.Vector;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.rebind.annatonations.EntityNameAnnotation;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.GeneratedVariableRefrence;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.OtherTaxCharges;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class OtherChargesPresenter extends FormTableScreenPresenter<OtherTaxCharges> implements  ValueChangeHandler<Double> {
	
	OtherChargesForm form;
	
	public OtherChargesPresenter(FormTableScreen<OtherTaxCharges> view, OtherTaxCharges model) {
		super(view, model);
		form=(OtherChargesForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getTaxConfigQuery());
		form.setPresenter(this);
		form.doocpercent.addValueChangeHandler(this);
		form.doocabsoluteval.addValueChangeHandler(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.OTHERCHARGES,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	/**
	 * Method template to set the processBar events
	 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) 
   {
		InlineLabel lbl= (InlineLabel) e.getSource();
		
   }
	
	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		model=new OtherTaxCharges();
	}
	
	/*
	 * Method template to set Myquerry object
	 */
	public MyQuerry getTaxConfigQuery()
	{
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new OtherTaxCharges());
		return quer;
	}
	
	public static void initalize()
	{
		OtherChargesPresenterTable gentableScreen=new OtherChargesTableProxy();
		OtherChargesForm form=new OtherChargesForm(gentableScreen,FormTableScreen.UPPER_MODE,true);
		gentableScreen.setView(form);
		gentableScreen.applySelectionModle();
		//OtherChargesPresenterSearch.staticSuperTable=gentableScreen;
		//OtherChargesSearchPopUp searchpopup=new OtherChargesSearchPopUp();
		//form.setSearchpopupscreen(searchpopup);
		OtherChargesPresenter presenter=new OtherChargesPresenter(form,new OtherTaxCharges());
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	
		 @EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.OtherCharges")
		 public static class OtherChargesPresenterTable extends SuperTable<OtherTaxCharges> implements GeneratedVariableRefrence{

			@Override
			public Object getVarRef(String varName) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void createTable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected void initializekeyprovider() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void addFieldUpdater() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnable(boolean state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void applyStyle() {
				// TODO Auto-generated method stub
				
			}};
			
			@EntityNameAnnotation(EntityName = "com.slicktechnologies.shared.OtherCharges")
			 public static class OtherChargesPresenterSearch extends SearchPopUpScreen<OtherTaxCharges>{

				@Override
				public MyQuerry getQuerry() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public boolean validate() {
					return true;
				}}

			@Override
			public void onValueChange(ValueChangeEvent<Double> event) {

				if(event.getSource()==form.doocpercent)
				{
					if(form.doocabsoluteval.getValue()!=null)
					{
						form.showDialogMessage("Please enter either Absolute Value or Percentage");
						form.doocabsoluteval.setValue(null);
					}
				}
				if(event.getSource()==form.doocabsoluteval)
				{
					if(form.doocpercent.getValue()!=null)
					{
						form.showDialogMessage("Please enter either Absolute Value or Percentage");
						form.doocpercent.setValue(null);
					}
				}
				
			};
				

}
