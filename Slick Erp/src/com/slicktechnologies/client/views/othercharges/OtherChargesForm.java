package com.slicktechnologies.client.views.othercharges;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.GLAccount;
import com.slicktechnologies.shared.OtherTaxCharges;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class OtherChargesForm extends FormTableScreen<OtherTaxCharges> {
	
	TextBox tbchargeId;
	TextBox tbotherchargename;
	DoubleBox doocabsoluteval,doocpercent;
	TextArea tadescription;
	CheckBox cbocstatus;
	ObjectListBox<GLAccount> olbglaccount;
	
	/**
	 * Date : 16-09-2017 BY ANIL
	 * This field is stores the sac code for particular charges
	 */
	TextBox tbSacCode;
	/**
	 * End
	 */
	
	public OtherChargesForm(SuperTable<OtherTaxCharges> table, int mode,
			boolean captionmode) {
		super(table, mode, captionmode);
		createGui();
	}

	private void initalizeWidget()
	{
		tbotherchargename=new TextBox();
		doocabsoluteval=new DoubleBox();
		doocpercent=new DoubleBox();
		cbocstatus=new CheckBox();
		cbocstatus.setValue(true);
		tadescription=new TextArea();
		olbglaccount=new ObjectListBox<GLAccount>();
		GLAccount.initializeGLAccount(olbglaccount);
		tbchargeId=new TextBox();
		tbchargeId.setEnabled(false);
		
		tbSacCode=new TextBox();
	}

	/*
	 * Method template to create the formtable screen
	 */

	@Override
	public void createScreen() {

		//Token to initialize the processlevel menus.
		initalizeWidget();

		//////////////////////////////////Form Field Declaration/////////////////////////////////////////////////

		//////////////////////////// Form Field Initialization////////////////////////////////////////////////////

		//Token to initialize formfield
		FormFieldBuilder fbuilder;
		fbuilder = new FormFieldBuilder();
		FormField fgroupingTaxInformation=fbuilder.setlabel("Other Charges Information").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("* Charge Name",tbotherchargename);
		FormField ftbotherchargename=fbuilder.setMandatory(true).setMandatoryMsg("Tax Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Percent",doocpercent);
		FormField fdoocpercent= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Absolute Value",doocabsoluteval);
		FormField fdoocabsoluteval= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("* GL Account Name",olbglaccount);
		FormField folbglaccount= fbuilder.setMandatory(true).setMandatoryMsg("GL Account Name is Mandatory!").setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Status",cbocstatus);
		FormField fcbocstatus= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		fbuilder = new FormFieldBuilder("Description",tadescription);
		FormField ftadescription=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
		fbuilder = new FormFieldBuilder("Other Charge ID",tbchargeId);
		FormField ftbchargeId=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		
		fbuilder = new FormFieldBuilder("SAC Code",tbSacCode);
		FormField ftbSacCode=fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////Lay Out Making Code///////////////////////////////////////////////////////////////


		FormField[][] formfield = {   {fgroupingTaxInformation},
				{ftbchargeId,ftbotherchargename,fdoocpercent,fdoocabsoluteval},
				{folbglaccount,ftbSacCode,fcbocstatus},
				{ftadescription},
		};


		this.fields=formfield;		
	}

	/**
	 * method template to update the model with token entity name
	 */
	@Override
	public void updateModel(OtherTaxCharges model) 
	{
		if(tbotherchargename.getValue()!=null)
			model.setOtherChargeName(tbotherchargename.getValue());
		if(doocpercent.getValue()!=null){
			model.setOtherChargePercent(doocpercent.getValue());
			model.setOtherChargeAbsValue(0);
		}
		if(doocabsoluteval.getValue()!=null){
			model.setOtherChargeAbsValue(doocabsoluteval.getValue());
			model.setOtherChargePercent(0);
		}
		if(olbglaccount.getValue()!=null)
			model.setGlAccountName(olbglaccount.getValue());
		if(cbocstatus.getValue()!=null)
			model.setOtherChargeStatus(cbocstatus.getValue());
		if(tadescription.getValue()!=null)
			model.setTaxDescription(tadescription.getValue());
		if(tbSacCode.getValue()!=null){
			model.setSacCode(tbSacCode.getValue());
		}
		presenter.setModel(model);
	}

	/**
	 * method template to update the view with token entity name
	 */
	@Override
	public void updateView(OtherTaxCharges view) 
	{
		tbchargeId.setValue(view.getCount()+"");
		if(view.getOtherChargeName()!=null)
			tbotherchargename.setValue(view.getOtherChargeName());
		if(view.getOtherChargePercent()!=null)
			doocpercent.setValue(view.getOtherChargePercent());
		if(view.getOtherChargeAbsValue()!=null)
			doocabsoluteval.setValue(view.getOtherChargeAbsValue());
		if(view.getGlAccountName()!=null)
			olbglaccount.setValue(view.getGlAccountName());
		if(view.getOtherChargeStatus()!=null)
			cbocstatus.setValue(view.getOtherChargeStatus());
		if(view.getTaxDescription()!=null)
			tadescription.setValue(view.getTaxDescription());
		if(view.getSacCode()!=null){
			tbSacCode.setValue(view.getSacCode());
		}

		presenter.setModel(view);

	}

	/**
	 * Toggles the app header bar menus as per screen state
	 */

	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   

			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.OTHERCHARGES,LoginPresenter.currentModule.trim());
	}

	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		cbocstatus.setValue(true);
		tbchargeId.setEnabled(false);
	}
	
	@Override
	public void setCount(int count)
	{
		tbchargeId.setValue(count+"");
	}



	@Override
	public boolean validate() {
		boolean superres=super.validate();
		boolean fieldval=true;
		
		if(this.doocpercent.getValue()==null&&this.doocabsoluteval.getValue()==null)
		{
			fieldval=false;
			showDialogMessage("Please enter either Percent or Absolute Value!");
		}
		
		return superres&&fieldval;
		
	}
	
	
	



}
