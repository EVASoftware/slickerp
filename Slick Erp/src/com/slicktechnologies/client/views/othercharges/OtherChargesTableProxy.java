package com.slicktechnologies.client.views.othercharges;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.views.othercharges.OtherChargesPresenter.OtherChargesPresenterTable;
import com.slicktechnologies.shared.OtherTaxCharges;

public class OtherChargesTableProxy extends OtherChargesPresenterTable {
	
	TextColumn<OtherTaxCharges> getCountColumn;
	TextColumn<OtherTaxCharges> getOtherChargeNameColumn;
	TextColumn<OtherTaxCharges> getOtherChargePercentColumn;
	TextColumn<OtherTaxCharges> getOtherChargeAbsValColumn;
	TextColumn<OtherTaxCharges> getGLAccountName;
	TextColumn<OtherTaxCharges> getOtherChargeStatusColumn;
	
	
	public OtherChargesTableProxy()
	{
		super();
	}
	
	@Override public void createTable() {
		addColumngetCount();
		addColumngetOtherChargeName();
		addColumngetOtherChargePercent();
		addColumngetOtherChargeAbsVal();
		addColumngetGLAccountName();
		addColumngetOtherChargeStatus();
		
	}
	
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<OtherTaxCharges>()
				{
			@Override
			public Object getKey(OtherTaxCharges item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}

	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetOtherChargeName();
		addSortinggetOtherChargePercent();
		addSortinggetOtherChargeAbsVal();
		addSortinggetGLAccount();
	}
	@Override public void addFieldUpdater() {
	}
	
	
	protected void addSortinggetCount()
	{
		List<OtherTaxCharges> list=getDataprovider().getList();
		columnSort=new ListHandler<OtherTaxCharges>(list);
		columnSort.setComparator(getCountColumn, new Comparator<OtherTaxCharges>()
				{
			@Override
			public int compare(OtherTaxCharges e1,OtherTaxCharges e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<OtherTaxCharges>()
				{
			@Override
			public String getValue(OtherTaxCharges object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"ID");
				getCountColumn.setSortable(true);
	}
	
	protected void addSortinggetOtherChargeName()
	{
		List<OtherTaxCharges> list=getDataprovider().getList();
		columnSort=new ListHandler<OtherTaxCharges>(list);
		columnSort.setComparator(getOtherChargeNameColumn, new Comparator<OtherTaxCharges>()
				{
			@Override
			public int compare(OtherTaxCharges e1,OtherTaxCharges e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getOtherChargeName()!=null && e2.getOtherChargeName()!=null){
						return e1.getOtherChargeName().compareTo(e2.getOtherChargeName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetOtherChargeName()
	{
		getOtherChargeNameColumn=new TextColumn<OtherTaxCharges>()
				{
			@Override
			public String getValue(OtherTaxCharges object)
			{
				return object.getOtherChargeName().trim();
			}
				};
				table.addColumn(getOtherChargeNameColumn,"Charge Name");
				getOtherChargeNameColumn.setSortable(true);
	}

	protected void addSortinggetOtherChargePercent()
	{
		List<OtherTaxCharges> list=getDataprovider().getList();
		columnSort=new ListHandler<OtherTaxCharges>(list);
		columnSort.setComparator(getOtherChargePercentColumn, new Comparator<OtherTaxCharges>()
				{
			@Override
			public int compare(OtherTaxCharges e1,OtherTaxCharges e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getOtherChargePercent()!=null && e2.getOtherChargePercent()!=null){
						return e1.getOtherChargePercent().compareTo(e2.getOtherChargePercent());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetOtherChargePercent()
	{
		getOtherChargePercentColumn=new TextColumn<OtherTaxCharges>()
				{
			@Override
			public String getValue(OtherTaxCharges object)
			{
				if(object.getOtherChargePercent()==null)
				{
					return "";
				}
				else
				{
				return object.getOtherChargePercent()+"";
				}
			}
				};
				table.addColumn(getOtherChargePercentColumn,"Percent");
				getOtherChargePercentColumn.setSortable(true);
	}

	protected void addSortinggetOtherChargeAbsVal()
	{
		List<OtherTaxCharges> list=getDataprovider().getList();
		columnSort=new ListHandler<OtherTaxCharges>(list);
		columnSort.setComparator(getOtherChargeAbsValColumn, new Comparator<OtherTaxCharges>()
				{
			@Override
			public int compare(OtherTaxCharges e1,OtherTaxCharges e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getOtherChargeAbsValue()!=null && e2.getOtherChargeAbsValue()!=null){
						return e1.getOtherChargeAbsValue().compareTo(e2.getOtherChargeAbsValue());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetOtherChargeAbsVal()
	{
		getOtherChargeAbsValColumn=new TextColumn<OtherTaxCharges>()
				{
			@Override
			public String getValue(OtherTaxCharges object)
			{
				if(object.getOtherChargeAbsValue()==null)
				{
					return "";
				}
				else
				{
				return object.getOtherChargeAbsValue()+"";
				}
			}
				};
				table.addColumn(getOtherChargeAbsValColumn,"Absolute Value");
				getOtherChargeAbsValColumn.setSortable(true);
	}
	
	protected void addSortinggetGLAccount()
	{
		List<OtherTaxCharges> list=getDataprovider().getList();
		columnSort=new ListHandler<OtherTaxCharges>(list);
		columnSort.setComparator(getGLAccountName, new Comparator<OtherTaxCharges>()
				{
			@Override
			public int compare(OtherTaxCharges e1,OtherTaxCharges e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getGlAccountName()!=null && e2.getGlAccountName()!=null){
						return e1.getGlAccountName().compareTo(e2.getGlAccountName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetGLAccountName()
	{
		getGLAccountName=new TextColumn<OtherTaxCharges>()
				{
			@Override
			public String getValue(OtherTaxCharges object)
			{
				return object.getGlAccountName().trim();
			}
				};
				table.addColumn(getGLAccountName,"GL Account");
				getGLAccountName.setSortable(true);
	}
	
	protected void addColumngetOtherChargeStatus()
	{
		getOtherChargeStatusColumn=new TextColumn<OtherTaxCharges>()
		{
			@Override
			public String getValue(OtherTaxCharges object)
			{
				if(object.getOtherChargeStatus()==true){
	              	return "Active";
				}
	             else{
	              	return "In Active";
	             }
			}
		};
		table.addColumn(getOtherChargeStatusColumn,"Status");
		getOtherChargeStatusColumn.setSortable(true);
	}


	

}
