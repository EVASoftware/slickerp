package com.slicktechnologies.client.views.processconfig;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

public class ProcessConfigurationTableProxy extends SuperTable<ProcessConfiguration> {
	
	TextColumn<ProcessConfiguration> countColumn;
	TextColumn<ProcessConfiguration> processNameColumn;
	
	public ProcessConfigurationTableProxy() {
		super();
	}

	@Override
	public void createTable() {
		addColumngetCount();
		addColumngetprocessName();
	}
	
	


	public void addColumnSorting(){
			
		addSortinggetCount();
		addSortinggetprocessName();
	}


		private void addSortinggetCount() {
			List<ProcessConfiguration> list=getDataprovider().getList();
			columnSort=new ListHandler<ProcessConfiguration>(list);
			columnSort.setComparator(countColumn, new Comparator<ProcessConfiguration>()
					{
				@Override
				public int compare(ProcessConfiguration e1,ProcessConfiguration e2)
				{
					if(e1!=null && e2!=null)
					{
						if(e1.getCount()== e2.getCount()){
							return 0;}
						if(e1.getCount()> e2.getCount()){
							return 1;}
						else{
							return -1;}
					}
					else{
						return 0;}
				}
					});
			table.addColumnSortHandler(columnSort);
		}
		
		private void addColumngetCount() {
			countColumn=new TextColumn<ProcessConfiguration>() {

				@Override
				public String getValue(ProcessConfiguration object) {
					if(object.getCount()==-1)
						return "N.A";
					else return object.getCount()+"";
				
				}
			};
			table.addColumn(countColumn, "ID");
			table.setColumnWidth(countColumn, 100, Unit.PX);
			countColumn.setSortable(true);
		}


	
	
			private void addSortinggetprocessName() {
				List<ProcessConfiguration> list=getDataprovider().getList();
				columnSort=new ListHandler<ProcessConfiguration>(list);
				columnSort.setComparator(processNameColumn, new Comparator<ProcessConfiguration>()
						{
					@Override
					public int compare(ProcessConfiguration e1,ProcessConfiguration e2)
					{
						if(e1!=null && e2!=null)
						{
							if( e1.getProcessName()!=null && e2.getProcessName()!=null){
								return e1.getProcessName().compareTo(e2.getProcessName());}
						}
						else{
							return 0;}
						return 0;
					}
						});
				table.addColumnSortHandler(columnSort);
		}
		
			private void addColumngetprocessName() {
				processNameColumn=new TextColumn<ProcessConfiguration>()
						{
					@Override
					public String getValue(ProcessConfiguration object)
					{
						return object.getProcessName().trim();
					}
						};
						table.addColumn(processNameColumn,"Process Name");
						table.setColumnWidth(processNameColumn, 130, Unit.PX);
						processNameColumn.setSortable(true);
			}
	
	
	
	

	@Override
	protected void initializekeyprovider() {
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

	

}
