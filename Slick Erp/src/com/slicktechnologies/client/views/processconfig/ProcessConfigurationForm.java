package com.slicktechnologies.client.views.processconfig;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.shared.GwtIncompatible;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.schedulingandrouting.SchedulingService;
import com.slicktechnologies.client.views.schedulingandrouting.SchedulingServiceAsync;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;

public class ProcessConfigurationForm extends FormTableScreen<ProcessConfiguration> implements ClickHandler{
	

	ObjectListBox<ProcessName>processName;
	ObjectListBox<Config> processType;
	ProcessTypeDetailsTable processTypeTable;
	CheckBox processStatus;
	Button addprocess;
	
	boolean datevalidate = false;
	
	SchedulingServiceAsync serviceAsync= GWT.create(SchedulingService.class);

	public ProcessConfigurationForm(SuperTable<ProcessConfiguration>table,int mode,boolean captionmode)
	{
		super(table, mode, captionmode);
		createGui();
		table.connectToLocal();
	}
	
	
	
	private void initializeWidget(){
		
		processName = new ObjectListBox<ProcessName>();
		ProcessName.initializeProcessName(processName);
		
		processType=new ObjectListBox<Config>();
		AppUtility.MakeLiveConfig(processType, Screen.PROCESSTYPE);
		
		processTypeTable = new ProcessTypeDetailsTable();
		
		processStatus=new CheckBox();
		processStatus.setValue(true); 
		
		addprocess = new Button("ADD");
		addprocess.addClickHandler(this);
	}
	
	
	
	@Override
	public void createScreen() {
		
		initializeWidget();
		
		FormFieldBuilder fbuilder;
		
		fbuilder = new FormFieldBuilder("Process Name",processName);
		FormField olbProcessName= fbuilder.setMandatory(true).setMandatoryMsg("Process Name is Mandatory").setRowSpan(0).setColSpan(0).build();

		
		fbuilder = new FormFieldBuilder("Process Type",processType);
		FormField olbType= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder=new FormFieldBuilder("",processTypeTable.getTable());
		FormField ftable=fbuilder.setColSpan(4).build();
		
		
		fbuilder=new FormFieldBuilder("Status",processStatus);
		FormField fcheckBoxStatus= fbuilder.setRowSpan(0).setColSpan(0).build();
		
		fbuilder = new FormFieldBuilder("",addprocess);
		FormField faddprocess= fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();

		FormField [][] formfield = {
				{olbProcessName,olbType,fcheckBoxStatus,faddprocess},
				{ftable},
			
				
		};
		
		this.fields = formfield;
	}
	
	@Override
	public void toggleAppHeaderBarMenu() {
		if(AppMemory.getAppMemory().currentState==ScreeenState.NEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.EDIT)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Save")||text.equals("Discard"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}

		else if(AppMemory.getAppMemory().currentState==ScreeenState.VIEW)
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.equals("Discard")||text.equals("Edit"))
					menus[k].setVisible(true); 

				else
					menus[k].setVisible(false);  		   
			}
		}
		AuthorizationHelper.setAsPerAuthorization(Screen.PROCESSCONFIGURATION,LoginPresenter.currentModule.trim());
	}
	
	@Override
	public void updateModel(ProcessConfiguration model) {
		
		if(processName.getValue()!=null){
			model.setProcessName(processName.getValue());
		}
		
		List<ProcessTypeDetails> processConfigType=this.processTypeTable.getDataprovider().getList();
		ArrayList<ProcessTypeDetails> arrItems=new ArrayList<ProcessTypeDetails>();
		arrItems.addAll(processConfigType);
		model.setProcessList(arrItems);
		model.setConfigStatus(processStatus.getValue());
		manageProcessConfig(model);
		
		
		presenter.setModel(model);
	}
	
	
	@Override
	public void updateView(ProcessConfiguration view) {
		
		processTypeTable.setValue(view.getProcessList());
		
		if(view.getProcessName()!=null)
			processName.setValue(view.getProcessName());
		
		if(view.isConfigStatus()!=null)
			processStatus.setValue(view.isConfigStatus());
		
		presenter.setModel(view);
	}
	
	
	@Override
	public void setEnable(boolean state) {
		super.setEnable(state);
		
		processTypeTable.setEnable(state);
	}
	
	
	
	@Override
	public void clear() {
			super.clear();
			processTypeTable.clear();
	}
	
	@Override
	public void onClick(ClickEvent event) {
		
		if(event.getSource().equals(addprocess)){
			
			if(processName.getSelectedIndex()==0)
			{
				showDialogMessage("Please fill Process Name");
			}
			
			if(processName.getSelectedIndex()!=0&&processType.getSelectedIndex()==0){
				showDialogMessage("Please fill Process Type");
			}
			
			if(processName.getSelectedIndex()!=0&&processType.getSelectedIndex()!=0)
			{
				List<ProcessTypeDetails> processLis=processTypeTable.getDataprovider().getList();
				System.out.println("Process Size"+processLis.size());
				if(processLis.size()!=0){
					if(validateProcess()==true){
						addToProcessTable();
					}
				}
				else{
					addToProcessTable();
				}
			}
			}
		}

	public boolean validateProcess()
	{
		List<ProcessTypeDetails> processLis=processTypeTable.getDataprovider().getList();
		
		System.out.println("Process Size"+processLis.size());
		
		if(processLis.size()!=0){
			for(int i=0;i<processLis.size();i++)
			{
				if(processLis.get(i).getProcessName().trim().equals(processName.getValue().trim())&&processLis.get(i).getProcessType().trim().equals(processType.getValue().trim()))
				{
					showDialogMessage("Process Name and Process Type already exists!");
					return false;
				}/**
				 *  nidhi for validation for date
				 */
				if(processName.getValue().trim().equalsIgnoreCase("AMCBillDateRestriction")){
					String datefmt  = processType.getValue().trim();
					DateTimeFormat formatter = DateTimeFormat.getFormat("dd/MM/yyyy");
					    try {
					    		serviceAsync.validateDateFormat(datefmt, new AsyncCallback<Boolean>() {
									
									@Override
									public void onSuccess(Boolean result) {
										// TODO Auto-generated method stub
										validateDate(result);
									}
									
									@Override
									public void onFailure(Throwable caught) {
										// TODO Auto-generated method stub
										validateDate(false);
									}
								});
					    	 	if(!datevalidate){
					    	 		showDialogMessage("Please select date as process type and it should be in dd/MM/yyyy format.");
					    	 	}
					    	 return datevalidate;
					    } catch (Exception e) {
					    	showDialogMessage("Please select date as process type.!");
					      return false;
					    }
					
				}else if(processName.getValue().trim().equalsIgnoreCase("QuotationValidateDuration")){
					//.matches("[0-9]+")
					String datefmt  = processType.getValue().trim();
					if(datefmt!=null && !datefmt.trim().matches("[0-9+]")){
						showDialogMessage("Please select numbers for dateduration");
						return false;
					}
					
				}
			}
		}
		return true;
		
	}
	
	protected void addToProcessTable(){
		
		ProcessTypeDetails processTypeEntity = new ProcessTypeDetails();
		
		processTypeEntity.setProcessName(processName.getValue());
		processTypeEntity.setProcessType(processType.getValue());
		processTypeEntity.setStatus(processStatus.getValue());
		
		processTypeTable.getDataprovider().getList().add(processTypeEntity);
		
	}
	
	public void manageProcessConfig(ProcessConfiguration pcModel)
	{
		ScreeenState scrState=AppMemory.getAppMemory().currentState;
		
		if(scrState.equals(ScreeenState.NEW))
		{
			LoginPresenter.globalProcessConfig.add(pcModel);
		}
		if(scrState.equals(ScreeenState.EDIT))
		{
			for(int i=0;i<LoginPresenter.globalProcessConfig.size();i++)
			{
				if(LoginPresenter.globalProcessConfig.get(i).getId().equals(pcModel.getId()))
				{
					LoginPresenter.globalProcessConfig.add(pcModel);
					LoginPresenter.globalProcessConfig.remove(i);
				}
			}
		}
	}
	
	/**
	 *  nidhi
	 * @param flag
	 */
	void validateDate(boolean flag){
		datevalidate  = flag;
	}
	@Override
	 public boolean validate(){
		
		if(processName.getValue().trim().equalsIgnoreCase("AMCBillDateRestriction")){
			int count =0;
			for(ProcessTypeDetails proTypeDt : processTypeTable.getDataprovider().getList()){
				if(proTypeDt.isStatus()){
					count++;
				}
				if(count>1){
					showDialogMessage("Only one process type(date) should be active.");
					return false;
				}
			}
		}
		
		return true;
	}
	/**
	 *  end
	 */
}
