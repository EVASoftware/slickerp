package com.slicktechnologies.client.views.processconfig;

import java.util.List;
import java.util.Vector;

import com.google.gwt.event.dom.client.ClickEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.role.UserRole;

public class ProcessConfigurationPresenter extends FormTableScreenPresenter<ProcessConfiguration> {
	

	
	ProcessConfigurationForm form;

	public ProcessConfigurationPresenter(FormTableScreen<ProcessConfiguration> view,ProcessConfiguration model) {
		super(view, model);
		form=(ProcessConfigurationForm) view;
		form.getSupertable().connectToLocal();
		form.retriveTable(getUserQuery());
		form.setPresenter(this);
		
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.PROCESSCONFIGURATION,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}

	
	
	private MyQuerry getUserQuery() {
		MyQuerry quer=new MyQuerry(new Vector<Filter>(), new ProcessConfiguration());
		return quer;
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		model = new ProcessConfiguration();
	}

	public void setModel(ProcessConfiguration entity) {
		model = entity;
	}



	public static void initalize() {
		ProcessConfigurationTableProxy gentable=new ProcessConfigurationTableProxy();
		ProcessConfigurationForm form=new ProcessConfigurationForm(gentable,FormTableScreen.UPPER_MODE,true);
		gentable.setView(form);
		gentable.applySelectionModle();
		ProcessConfigurationPresenter presenter=new ProcessConfigurationPresenter(form, new ProcessConfiguration());
		AppMemory.getAppMemory().stickPnel(form);
	}

}
