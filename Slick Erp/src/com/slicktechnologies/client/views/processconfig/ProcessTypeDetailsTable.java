package com.slicktechnologies.client.views.processconfig;

import java.util.Date;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.role.ScreenAuthorization;

public class ProcessTypeDetailsTable extends SuperTable<ProcessTypeDetails> {
	

	TextColumn<ProcessTypeDetails> processNameColumn;
	TextColumn<ProcessTypeDetails> processTypeColumn;
    Column<ProcessTypeDetails,String> deleteColumn;
    Column<ProcessTypeDetails, Boolean> statusColumn;
    TextColumn<ProcessTypeDetails> viewStatusColumn;
    
	public ProcessTypeDetailsTable(){
		super();
	}

	@Override
	public void createTable() {
		addColumngetprocessName();
		addColumngetprocessType();
		addColumnStatus();
		setFieldUpdaterOnStatus();
		addColumngetdelete();
		createFieldUpdaterdeleteColumn();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
	}

	

	private void addColumngetprocessName()
	{
		processNameColumn=new TextColumn<ProcessTypeDetails>()
				{
			@Override
			public String getValue(ProcessTypeDetails object)
			{
				return object.getProcessName().trim();
			}
				};
				table.addColumn(processNameColumn,"Process Name");
				table.setColumnWidth(processNameColumn,250, Unit.PX);
				processNameColumn.setSortable(true);	
	}
	
	private void addColumngetprocessType() 
	{
		processTypeColumn=new TextColumn<ProcessTypeDetails>()
				{
			@Override
			public String getValue(ProcessTypeDetails object)
			{
				return object.getProcessType().toUpperCase().trim();
			}
				};
				table.addColumn(processTypeColumn,"Type");
				table.setColumnWidth(processTypeColumn, 550, Unit.PX);/**Size of column is increased by sheetal on 08-12-2021**/
				processTypeColumn.setSortable(true);		
	}
	
	protected void addColumnViewgetstatus() {
		
		viewStatusColumn = new TextColumn<ProcessTypeDetails>() {
			@Override
			public String getValue(ProcessTypeDetails object) {
				return object.isStatus()+"";
			}
		};
		table.addColumn(viewStatusColumn, "Status");
		
	}
	
	private void addColumnStatus() {
		CheckboxCell cb = new CheckboxCell();
		statusColumn = new Column<ProcessTypeDetails, Boolean>(cb) {

			@Override
			public Boolean getValue(ProcessTypeDetails object) {
				return object.isStatus();
			}
		};
		table.addColumn(statusColumn, "Status");
		table.setColumnWidth(statusColumn, 200, Unit.PX);
		statusColumn.setSortable(true);		
	}
	
	private void addColumngetdelete() {
		ButtonCell btnCell = new ButtonCell();
		deleteColumn = new Column<ProcessTypeDetails,String>(btnCell) {

            public String getValue(ProcessTypeDetails object)
            {
                return "Delete";
            }
        };
        table.addColumn(deleteColumn, "Delete");		
	}
	
	
	
	
	private void createFieldUpdaterdeleteColumn() {
		 deleteColumn.setFieldUpdater(new FieldUpdater<ProcessTypeDetails,String>() {

	            public void update(int index, ProcessTypeDetails object, String value)
	            {
	                getDataprovider().getList().remove(index);
	                table.redrawRow(index);
	            }
	        }
	);		
	}
	
	
	protected void setFieldUpdaterOnStatus()
	{
		statusColumn.setFieldUpdater(new FieldUpdater<ProcessTypeDetails, Boolean>() {
			@Override
			public void update(int index, ProcessTypeDetails object, Boolean value) {
				object.setStatus(value);
				getDataprovider().getList();
				table.redrawRow(index);
			}
		});
	}
	
	
	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		
		
	for(int i=table.getColumnCount()-1;i>-1;i--){
		table.removeColumn(i);
	}
		if(state==true){
			
			addColumngetprocessName();
			addColumngetprocessType();
			addColumnStatus();
			setFieldUpdaterOnStatus();
			addColumngetdelete();
			createFieldUpdaterdeleteColumn();
			
		}
		
		else {
			
			addColumngetprocessName();
			addColumngetprocessType();
			addColumnViewgetstatus();
		}
	}

	@Override
	public void applyStyle() {
		
	}
	
	@Override
	protected void initializekeyprovider() {
		keyProvider = new ProvidesKey<ProcessTypeDetails>() {
			@Override
			public Object getKey(ProcessTypeDetails item) {
				if (item == null) {
					return null;
				} else
					return new Date().getTime();
			}
		};

	}



}
