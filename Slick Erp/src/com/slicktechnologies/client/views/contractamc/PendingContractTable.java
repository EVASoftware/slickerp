package com.slicktechnologies.client.views.contractamc;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.view.client.ProvidesKey;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.shared.Contract;


public class PendingContractTable extends SuperTable<Contract> {
	TextColumn<Contract> getCustomerCellNumberColumn;
	TextColumn<Contract> getCustomerFullNameColumn;
	TextColumn<Contract> getContractCountColumn;
	TextColumn<Contract> getBranchColumn;
	TextColumn<Contract> getStatusColumn;
	TextColumn<Contract> getPriceColumn;
	TextColumn<Contract> getTechnicianColumn;
	TextColumn<Contract> getCompanyNameColumn;
	public PendingContractTable()
	{
		super();
		getTable().setHeight("500px");
		getTable().setWidth("1125px");
	}
	@Override 
	public void createTable() {
		addColumngetContractCount();
		addColumngetCustomerFullName();
		addColumngetCustomerCellNumber();
		addColumngetBranch();
		//addColumngetCompanyName();
		addColumngetPrice();
		addColumngetTechnician();
		addColumngetStatus();
		addColumnSorting();
	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<Contract>()
				{
			@Override
			public Object getKey(Contract item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}
	@Override
	public void setEnable(boolean state)
	{
	}
	@Override
	public void applyStyle()
	{
	}
	public void addColumnSorting(){
		
		addSortinggetContractCount();
		addSortinggetCustomerFullName();
		addSortinggetCustomerCellNumber();
		addSortinggetBranch();
		addSortinggetCompanyName();
		addSortinggetPriceColumn();
		addSortinggetTechnician();
		addSortinggetStatus();
		
	}
	@Override public void addFieldUpdater() {
	}

	protected void addSortinggetBranch()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getBranchColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetBranch()
	{
		getBranchColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				return object.getBranch()+"";
			}
				};
				table.addColumn(getBranchColumn,"Branch");
				table.setColumnWidth(getBranchColumn, 80, Unit.PX);
				getBranchColumn.setSortable(true);
	}
	
	protected void addSortinggetStatus()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				return object.getStatus()+"";
			}
				};
				table.addColumn(getStatusColumn,"Status");
				getStatusColumn.setSortable(true);
				table.setColumnWidth(getStatusColumn,80,Unit.PX);

	}
	protected void addSortinggetCustomerFullName()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getCustomerFullNameColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCustomerFullName()!=null && e2.getCustomerFullName()!=null){
						return e1.getCustomerFullName().compareTo(e2.getCustomerFullName());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);

	}
	protected void addColumngetCustomerFullName()
	{
		getCustomerFullNameColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				return object.getCustomerFullName()+"";
			}
				};
				table.addColumn(getCustomerFullNameColumn,"Customer Name");
				table.setColumnWidth(getCustomerFullNameColumn,150,Unit.PX);
				getCustomerFullNameColumn.setSortable(true);
	}
	protected void addSortinggetCustomerCellNumber()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getCustomerCellNumberColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCustomerCellNumber()== e2.getCustomerCellNumber()){
						return 0;}
					if(e1.getCustomerCellNumber()> e2.getCustomerCellNumber()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetContractCount()
	{
		getContractCountColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				if( object.getCount()==-1)
					return "N.A";
				else return object.getCount()+"";
			}
				};
				table.addColumn(getContractCountColumn,"Contract Id");
				table.setColumnWidth(getContractCountColumn,150,Unit.PX);

				getContractCountColumn.setSortable(true);
	}
	protected void addColumngetCustomerCellNumber()
	{
		getCustomerCellNumberColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				return object.getCustomerCellNumber()+"";
			}
				};
				table.addColumn(getCustomerCellNumberColumn,"Customer Cell");
				table.setColumnWidth(getCustomerCellNumberColumn,150,Unit.PX);
				getCustomerCellNumberColumn.setSortable(true);
	}
	protected void addSortinggetContractCount()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getContractCountColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getCount()== e2.getCount()){
						return 0;}
					if(e1.getCount()> e2.getCount()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetPrice()
	{
		getPriceColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				return object.getNetpayable()+"";
			}
				};
				table.addColumn(getPriceColumn,"Price");
				table.setColumnWidth(getPriceColumn,100,Unit.PX);
				getPriceColumn.setSortable(true);
	}
	protected void addSortinggetPriceColumn()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getPriceColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if(e1.getNetpayable()== e2.getNetpayable()){
						return 0;}
					if(e1.getNetpayable()> e2.getNetpayable()){
						return 1;}
					else{
						return -1;}
				}
				else{
					return 0;}
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addSortinggetTechnician()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getTechnicianColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployee()!=null && e2.getEmployee()!=null){
						return e1.getEmployee().compareTo(e2.getEmployee());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	protected void addColumngetTechnician()
	{
		getTechnicianColumn=new TextColumn<Contract>()
		{
			@Override
			public String getValue(Contract object)
			{
				return object.getEmployee()+"";
			}
				};
				table.addColumn(getTechnicianColumn,"Technician");
				table.setColumnWidth(getTechnicianColumn, 80, Unit.PX);
				getTechnicianColumn.setSortable(true);
	}
	protected void addColumngetCompanyName()
	{
		getCompanyNameColumn=new TextColumn<Contract>()
				{
			@Override
			public String getValue(Contract object)
			{
				return object.getEmployee()+"";
			}
				};
				table.addColumn(getCompanyNameColumn,"Company Name");
				table.setColumnWidth(getCompanyNameColumn, 80, Unit.PX);
				getCompanyNameColumn.setSortable(true);
	}
	protected void addSortinggetCompanyName()
	{
		List<Contract> list=getDataprovider().getList();
		columnSort=new ListHandler<Contract>(list);
		columnSort.setComparator(getCompanyNameColumn, new Comparator<Contract>()
				{
			@Override
			public int compare(Contract e1,Contract e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployee()!=null && e2.getEmployee()!=null){
						return e1.getEmployee().compareTo(e2.getEmployee());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
}
