package com.slicktechnologies.client.views.contractamc;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;

public class PendingContractForm extends FormScreen<Contract> implements ClickHandler{
	    PendingContractTable contractTable ;
	    DateBox dbfromDate;
	    DateBox dbtoDate;
	    Button btgo;

		@Override
		public void createScreen() {
			// TODO Auto-generated method stub
			//super.createScreen();
			initalizeWidget();
			this.processlevelBarNames = new String[]{"View Contract"};
			FormFieldBuilder fbuilder;
			fbuilder = new FormFieldBuilder();
			contractTable = new PendingContractTable();
			FormField fgroupingPendingContract = fbuilder.setlabel("Pending Contracts").widgetType(FieldType.Grouping).setMandatory(false).setColSpan(4).build();
			fbuilder = new FormFieldBuilder("From Date", dbfromDate );
			FormField fdbfromDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("To Date", dbtoDate);
			FormField fdbtoDate = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder();
			FormField fempty = fbuilder.setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("", btgo);
			FormField fbtgo = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
			fbuilder = new FormFieldBuilder("", contractTable.getTable());
			FormField fsalesOrderWithhoutAMCTable = fbuilder.setMandatory(false).setRowSpan(0).setColSpan(4).build();
			FormField[][] formfield = {
					{fgroupingPendingContract},
					{fempty,fdbfromDate , fdbtoDate, fbtgo},
					{fsalesOrderWithhoutAMCTable}
			};
					this.fields = formfield;
		}

		protected GWTCGlassPanel glassPanel;

		public PendingContractForm(){
			createGui();
			 if(processLevelBar!=null)
				    processLevelBar.setVisibleFalse(true);
		}

		@Override
		public void toggleAppHeaderBarMenu() {
			// TODO Auto-generated method stub
			
		}


		@Override
		public void updateModel(Contract model) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateView(Contract model) {
			// TODO Auto-generated method stub
			
		}

		private void initalizeWidget(){
			dbfromDate = new DateBox();
			dbtoDate = new DateBox();
			btgo = new Button("GO");
		}
		
		@Override
		public void onClick(ClickEvent event) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void setToNewState() {
			if(processLevelBar!=null)
			    processLevelBar.setVisibleFalse(true);
		}

		public PendingContractTable getContractTable() {
			return contractTable;
		}

		public void setContractTable(PendingContractTable contractTable) {
			this.contractTable = contractTable;
		}
	
}
