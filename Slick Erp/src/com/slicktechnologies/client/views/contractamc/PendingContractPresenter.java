package com.slicktechnologies.client.views.contractamc;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.quickcontract.QuickContractForm;
import com.slicktechnologies.client.views.quickcontract.QuickContractPresentor;
import com.slicktechnologies.client.views.quicksalesorder.QuickSalesOrderForm;
import com.slicktechnologies.client.views.quicksalesorder.QuickSalesOrderPresenter;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;

public class PendingContractPresenter extends FormScreenPresenter<Contract>{

	PendingContractForm form;
	Contract contract;
	GenricServiceAsync genasync=GWT.create(GenricService.class); 
	public PendingContractPresenter(UiScreen<Contract> view,
			Contract model) {
		super(view, model);
		form = (PendingContractForm) view;
		form.setPresenter(this);
		// TODO Auto-generated constructor stub
		Filter filter = null;
		Vector<Filter> filtervec=new Vector<Filter>();
		MyQuerry querry;	
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(model.getCompanyId());
		filtervec.add(filter);
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setStringValue(Contract.CREATED);
		filtervec.add(filter);
		querry=new MyQuerry();
		querry.setQuerryObject(new Contract());
		querry.setFilters(filtervec);
		retriveTable(querry,form.getContractTable());	
		setTableSelectionOnContract();
	}

	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		// TODO Auto-generated method stub
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		if(text.equals("View Contract")){
			if(contract!=null){	
				viewSelectedDocument();
			}
			else{
				form.showDialogMessage("Please select atleast one record!");
			}
			
		}

	}

	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void makeNewModel() {
		// TODO Auto-generated method stub
		
	}
	public static PendingContractForm initalize()
	{
		PendingContractForm form = new PendingContractForm();
		PendingContractPresenter  presenter=new  PendingContractPresenter(form,new Contract());
		form.setToNewState();
		AppMemory.getAppMemory().stickPnel(form);
		return form;
	}
	 public <T> void retriveTable(MyQuerry querry,final SuperTable<T>table)
     {
    	 
    			table.connectToLocal();
    			genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
    				
    				@SuppressWarnings("unchecked")
    				@Override
    				public void onSuccess(ArrayList<SuperModel> result) {
    					
    					for(SuperModel model:result)
    					{
    						table.getListDataProvider().getList().add((T) model);
    					}
    				}
    				
    				@Override
    				public void onFailure(Throwable caught) {
    				caught.printStackTrace();
    					
    				}
    			}); 
     }

	 public void setTableSelectionOnContract()
	 {
		final NoSelectionModel<Contract> selectionModelMyObj = new NoSelectionModel<Contract>();

		SelectionChangeEvent.Handler tableHandler = new SelectionChangeEvent.Handler() {
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				Contract entity = selectionModelMyObj.getLastSelectedObject();
				contract=entity;
				System.out.println("selected object :"+ contract);
			}
		};
	    System.out.println("selected object :"+ selectionModelMyObj);
		selectionModelMyObj.addSelectionChangeHandler(tableHandler);
		form.getContractTable().getTable().setSelectionModel(selectionModelMyObj);
		 
	 }
	 private void viewSelectedDocument()
		{
			AppMemory.getAppMemory().currentState = ScreeenState.VIEW;
		    AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Quick Contract",Screen.QUICKCONTRACT);
			final QuickContractForm form=QuickContractPresentor.initalize();
			form.showWaitSymbol();
			AppMemory.getAppMemory().stickPnel(form);
			Timer timer = new Timer() {
				@Override
				public void run() {
					form.hideWaitSymbol();
					form.updateView(contract);
					form.setToViewState();
				}
			};
			timer.schedule(3000);
		}

}
