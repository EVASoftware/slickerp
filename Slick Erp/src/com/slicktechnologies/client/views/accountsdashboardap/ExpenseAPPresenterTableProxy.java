package com.slicktechnologies.client.views.accountsdashboardap;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.account.expensemanagment.ExpensemanagmentPresenter.ExpensemanagmentPresenterTable;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;

public class ExpenseAPPresenterTableProxy extends ExpensemanagmentPresenterTable {
	TextColumn<Expense> getInvoiceNumberColumn;
	TextColumn<Expense> getAmountColumn;
	TextColumn<Expense> getVendorColumn;
	TextColumn<Expense> getExpenseDateColumn;
	TextColumn<Expense> getBranchColumn;
	TextColumn<Expense> getEmployeeColumn;
	TextColumn<Expense> getStatusColumn;
	Column<Expense,Date> getCreationDateColumn;
	TextColumn<Expense> getCountColumn;

	public Object getVarRef(String varName)
	{
		if(varName.equals("getCountColumn"))
			return this.getCountColumn;
		if(varName.equals("getInvoiceNumberColumn"))
			return this.getInvoiceNumberColumn;
		if(varName.equals("getAmountColumn"))
			return this.getAmountColumn;
		if(varName.equals("getVendorColumn"))
			return this.getVendorColumn;
		if(varName.equals("getExpenseDateColumn"))
			return this.getExpenseDateColumn;
		if(varName.equals("getBranchColumn"))
			return this.getBranchColumn;
		if(varName.equals("getEmployeeColumn"))
			return this.getEmployeeColumn;
		if(varName.equals("getCreationDateColumn"))
			return this.getCreationDateColumn;
		if(varName.equals("getStatusColumn"))
			return this.getStatusColumn;
		return null ;
	}
	public ExpenseAPPresenterTableProxy()
	{
		super();
	}

	@Override 
	public void createTable() {
		addColumngetCount();
		addColumngetInvoiceNo();
		addColumngetAmount();
		addColumngetVendorName();
		addColumngetExpenseDate();
		addColumngetEmployee();
		addColumngetBranch();
		addColumngetCreationDate();
		addColumngetStatus();
		addColumnSorting();
	}
	@Override
	protected void initializekeyprovider() {
		keyProvider= new ProvidesKey<Expense>()
				{
			@Override
			public Object getKey(Expense item)
			{
				if(item==null)
				{
					return null;
				}
				else
					return item.getId();
			}
				};
	}

	@Override
	public void setEnable(boolean state)
	{
	}

	@Override
	public void applyStyle()
	{
	}

	public void addColumnSorting(){
		addSortinggetCount();
		addSortinggetInvoiceNo();
		addSortinggetAmount();
		addSortinggetVendorName();
		addSortinggetExpenseDate();
		addSortinggetEmployee();
		addSortinggetBranch();
		addSortinggetCreationDate();
		addSortinggetStatus();
	}
	@Override public void addFieldUpdater() {
	}


	protected void addColumngetCount()
	{
		getCountColumn=new TextColumn<Expense>()
				{
			@Override
			public String getValue(Expense object)
			{
				return object.getCount()+"";
			}
				};
				table.addColumn(getCountColumn,"ID");
				getCountColumn.setSortable(true);
	}

	protected void addSortinggetCount()
	  {
	  List<Expense> list=getDataprovider().getList();
	  columnSort=new ListHandler<Expense>(list);
	  columnSort.setComparator(getCountColumn, new Comparator<Expense>()
	  {
	  @Override
	  public int compare(Expense e1,Expense e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if(e1.getCount()== e2.getCount()){
	  return 0;}
	  if(e1.getCount()> e2.getCount()){
	  return 1;}
	  else{
	  return -1;}
	  }
	  else{
	  return 0;}
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  }

	protected void addColumngetInvoiceNo()
	{
		getInvoiceNumberColumn=new TextColumn<Expense>()
				{
			@Override
			public String getValue(Expense object)
			{
				return object.getInvoiceNumber();
			}
				};
				table.addColumn(getInvoiceNumberColumn,"Invoice No");
				getInvoiceNumberColumn.setSortable(true);
	}

	protected void addSortinggetInvoiceNo()
	{
		List<Expense> list=getDataprovider().getList();
		columnSort=new ListHandler<Expense>(list);
		columnSort.setComparator(getInvoiceNumberColumn, new Comparator<Expense>()
				{
			@Override
			public int compare(Expense e1,Expense e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getInvoiceNumber()!=null && e2.getInvoiceNumber()!=null){
						return e1.getInvoiceNumber().compareTo(e2.getInvoiceNumber());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetAmount()
	{
		getAmountColumn=new TextColumn<Expense>()
				{
			@Override
			public String getValue(Expense object)
			{
				return object.getAmount()+"";
			}
				};
				table.addColumn(getAmountColumn,"Amount");
				getAmountColumn.setSortable(true);
	}

	protected void addSortinggetAmount()
	  {
	  List<Expense> list=getDataprovider().getList();
	  columnSort=new ListHandler<Expense>(list);
	  columnSort.setComparator(getAmountColumn, new Comparator<Expense>()
	  {
	  @Override
	  public int compare(Expense e1,Expense e2)
	  {
	  if(e1!=null && e2!=null)
	  {
	  if(e1.getAmount()== e2.getAmount()){
	  return 0;}
	  if(e1.getAmount()> e2.getAmount()){
	  return 1;}
	  else{
	  return -1;}
	  }
	  else{
	  return 0;}
	  }
	  });
	  table.addColumnSortHandler(columnSort);
	  }

	protected void addColumngetVendorName()
	{
		getVendorColumn=new TextColumn<Expense>()
				{
			@Override
			public String getValue(Expense object)
			{
				return object.getVendor();
			}
				};
				table.addColumn(getVendorColumn,"Vendor Name");
				getVendorColumn.setSortable(true);
	}


	protected void addSortinggetVendorName()
	{
		List<Expense> list=getDataprovider().getList();
		columnSort=new ListHandler<Expense>(list);
		columnSort.setComparator(getVendorColumn, new Comparator<Expense>()
				{
			@Override
			public int compare(Expense e1,Expense e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getVendor()!=null && e2.getVendor()!=null){
						return e1.getVendor().compareTo(e2.getVendor());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetExpenseDate()
	{
		final Date todaysDate=new Date();
		CalendarUtil.addDaysToDate(todaysDate,-1);
		todaysDate.setHours(23);
		todaysDate.setMinutes(59);
		todaysDate.setSeconds(59);
		System.out.println("Todays Date Converted ::: "+todaysDate);
		
		getExpenseDateColumn=new TextColumn<Expense>()
		{
			@Override
			public String getCellStyleNames(Context context, Expense object) {
				if(object.getExpenseDate()!=null&&(object.getExpenseDate().before(todaysDate))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
					 return "red";
				 }
				 else if(object.getStatus().equals("Approved")){
					 return "green";
				 }
				 else{
					 return "black";
				 }
			}
			
			@Override
			public String getValue(Expense object)
			{
				if(object.getExpenseDate()!=null){
				return AppUtility.parseDate(object.getExpenseDate());
				}
				else{
					return null;
				}
			}
		};
		table.addColumn(getExpenseDateColumn,"Expense Date");
		getExpenseDateColumn.setSortable(true);
	}

	protected void addSortinggetExpenseDate()
	{
		List<Expense> list=getDataprovider().getList();
		columnSort=new ListHandler<Expense>(list);
		columnSort.setComparator(getExpenseDateColumn, new Comparator<Expense>()
				{
			@Override
			public int compare(Expense e1,Expense e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getExpenseDate()!=null && e2.getExpenseDate()!=null){
						return e1.getExpenseDate().compareTo(e2.getExpenseDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addSortinggetCreationDate()
	{
		List<Expense> list=getDataprovider().getList();
		columnSort=new ListHandler<Expense>(list);
		columnSort.setComparator(getCreationDateColumn, new Comparator<Expense>()
				{
			@Override
			public int compare(Expense e1,Expense e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getCreationDate()!=null && e2.getCreationDate()!=null){
						return e1.getCreationDate().compareTo(e2.getCreationDate());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}


	protected void addColumngetEmployee()
	{
		getEmployeeColumn=new TextColumn<Expense>()
				{
			@Override
			public String getValue(Expense object)
			{
				return object.getEmployee();
			}
				};
				table.addColumn(getEmployeeColumn,"Employee");
				getEmployeeColumn.setSortable(true);
	}


	protected void addSortinggetEmployee()
	{
		List<Expense> list=getDataprovider().getList();
		columnSort=new ListHandler<Expense>(list);
		columnSort.setComparator(getEmployeeColumn, new Comparator<Expense>()
				{
			@Override
			public int compare(Expense e1,Expense e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getEmployee()!=null && e2.getEmployee()!=null){
						return e1.getEmployee().compareTo(e2.getEmployee());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetBranch()
	{
		getBranchColumn=new TextColumn<Expense>()
				{
			@Override
			public String getValue(Expense object)
			{
				return object.getBranch();
			}
				};
				table.addColumn(getBranchColumn,"Branch");
				getBranchColumn.setSortable(true);
	}


	protected void addSortinggetBranch()
	{
		List<Expense> list=getDataprovider().getList();
		columnSort=new ListHandler<Expense>(list);
		columnSort.setComparator(getBranchColumn, new Comparator<Expense>()
				{
			@Override
			public int compare(Expense e1,Expense e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getBranch()!=null && e2.getBranch()!=null){
						return e1.getBranch().compareTo(e2.getBranch());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}

	protected void addColumngetCreationDate()
	{
		DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
		DatePickerCell date= new DatePickerCell(fmt);
		getCreationDateColumn=new Column<Expense,Date>(date)
				{
			@Override
			public Date getValue(Expense object)
			{
				if(object.getCreationDate()!=null){
					return object.getCreationDate();}
				else{
					object.setCreationDate(new Date());
					return new Date();
				}
			}
				};
				table.addColumn(getCreationDateColumn,"Creation Date");
				getCreationDateColumn.setSortable(true);
	}

	protected void addColumngetStatus()
	{
		getStatusColumn=new TextColumn<Expense>()
				{
			@Override
			public String getValue(Expense object)
			{
				return object.getStatus()+"";
			}
				};
				table.addColumn(getStatusColumn,"Status");
				getStatusColumn.setSortable(true);
	}


	protected void addSortinggetStatus()
	{
		List<Expense> list=getDataprovider().getList();
		columnSort=new ListHandler<Expense>(list);
		columnSort.setComparator(getStatusColumn, new Comparator<Expense>()
				{
			@Override
			public int compare(Expense e1,Expense e2)
			{
				if(e1!=null && e2!=null)
				{
					if( e1.getStatus()!=null && e2.getStatus()!=null){
						return e1.getStatus().compareTo(e2.getStatus());}
				}
				else{
					return 0;}
				return 0;
			}
				});
		table.addColumnSortHandler(columnSort);
	}
	}
