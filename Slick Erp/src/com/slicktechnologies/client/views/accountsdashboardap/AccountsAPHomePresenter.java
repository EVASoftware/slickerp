package com.slicktechnologies.client.views.accountsdashboardap;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.account.expensemanagment.ExpensemanagmentForm;
import com.slicktechnologies.client.views.account.expensemanagment.ExpensemanagmentPresenter;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.SalesOrderProductTable;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsPresenter;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

public class AccountsAPHomePresenter implements ClickHandler {AccountsAPHomeForm form;
AccountsAPHomePresenterSearchProxy searchpopup;
protected GenricServiceAsync service=GWT.create(GenricService.class);

public  AccountsAPHomePresenter(AccountsAPHomeForm form,AccountsAPHomePresenterSearchProxy searchpopup)
{
	this.form=form;
	this.searchpopup=searchpopup;
	SetEventHandling();
	searchpopup.applyHandler(this);
	form.getBtnGo().addClickHandler(this);
	
	Date date=new Date();
	System.out.println("Today Date ::::::::: "+date);
	// **** Here We Add 7 Days to current date and pass this date to default search.
	CalendarUtil.addDaysToDate(date,7);
    System.out.println("Changed Date + 7 ::::::::: "+date);
	
    Date todaysDate=new Date();
    System.out.println("Today Date ::::::::: "+todaysDate);
    CalendarUtil.addDaysToDate(todaysDate,-7);
    System.out.println("Changed Date - 7::::::::: "+todaysDate);
    
    Long compId=UserConfiguration.getCompanyId();
    System.out.println("Company Id :: "+compId);
    
    
    Vector<Filter> filtervec=null;
	Filter filter = null;
	MyQuerry querry;
	List<String> statusList;
	
    filtervec=new Vector<Filter>();
	statusList=new ArrayList<String>();
	statusList.add(BillingDocument.CREATED);
	statusList.add(BillingDocument.REQUESTED);
	statusList.add(BillingDocument.APPROVED);
	
	filter = new Filter();
	filter.setQuerryString("companyId");
	filter.setLongValue(compId);
	filtervec.add(filter);
	
	filter = new Filter();
	filter.setQuerryString("accountType");
	filter.setStringValue(AppConstants.BILLINGACCOUNTTYPEAP);
	filtervec.add(filter);
	
	filter = new Filter();
	filter.setQuerryString("status IN");
	filter.setList(statusList);
	filtervec.add(filter);
	
	filter = new Filter();
	filter.setQuerryString("billingDocumentInfo.billingDate <=");
	filter.setDateValue(date);
	filtervec.add(filter);
	
	//Create Querry For Billing
	
	querry=new MyQuerry();
	querry.setFilters(filtervec);
	querry.setQuerryObject(new BillingDocument());
	this.retriveTable(querry,form.getBillingTable());
    
	////////////////////////////////////Create Querry For Invoice///////////////////////////// 

	filtervec=new Vector<Filter>();
	statusList=new ArrayList<String>();
	statusList.add(Invoice.CREATED);
	statusList.add(Invoice.REQUESTED);
	
	filter = new Filter();
	filter.setQuerryString("companyId");
	filter.setLongValue(compId);
	filtervec.add(filter);
	
	filter = new Filter();
	filter.setQuerryString("accountType");
	filter.setStringValue(AppConstants.BILLINGACCOUNTTYPEAP);
	filtervec.add(filter);
	
	filter = new Filter();
	filter.setQuerryString("status IN");
	filter.setList(statusList);
	filtervec.add(filter);
	
	filter = new Filter();
	filter.setQuerryString("invoiceDate <=");
	filter.setDateValue(date);
	filtervec.add(filter);
	
    querry=new MyQuerry();
	querry.setQuerryObject(new Invoice());
	querry.setFilters(filtervec);
	retriveTable(querry,form.getInvoiceTable());
	///////////Create Querry For Payment/////////////////////////////////////////////////////
	
	
	filtervec=new Vector<Filter>();
	statusList=new ArrayList<String>();
	statusList.add(CustomerPayment.CREATED);
	
	filter = new Filter();
	filter.setQuerryString("companyId");
	filter.setLongValue(compId);
	filtervec.add(filter);
	
	filter = new Filter();
	filter.setQuerryString("accountType");
	filter.setStringValue(AppConstants.BILLINGACCOUNTTYPEAP);
	filtervec.add(filter);
	
	filter = new Filter();
	filter.setQuerryString("status IN");
	filter.setList(statusList);
	filtervec.add(filter);
	
	filter = new Filter();
	filter.setQuerryString("paymentDate <=");
	filter.setDateValue(date);
	filtervec.add(filter);
	
	
	
	querry=new MyQuerry();
	querry.setQuerryObject(new CustomerPayment());
	querry.setFilters(filtervec);
	retriveTable(querry,form.getPaymentTable());
	
	////////////////////Create Querry For Expense//////////////////////
	
	
//	filtervec=new Vector<Filter>();
//	statusList=new ArrayList<String>();
//	statusList.add(Expense.CREATED);
//	statusList.add(Expense.REQUESTED);
//	
//	filter = new Filter();
//	filter.setQuerryString("companyId");
//	filter.setLongValue(compId);
//	filtervec.add(filter);
//	
//	filter = new Filter();
//	filter.setQuerryString("status IN");
//	filter.setList(statusList);
//	filtervec.add(filter);
//	
//	filter = new Filter();
//	filter.setQuerryString("expenseDate <=");
//	filter.setDateValue(date);
//	filtervec.add(filter);
//	
//	querry=new MyQuerry();
//	querry.setQuerryObject(new Expense());
//	querry.setFilters(filtervec);
//	retriveTable(querry,form.getExpenseTable());
	
	
	setTableSelectionOnBilling();
	setTableSelectionOnInvoice();
	setTableSelectionOnPayment();
//	setTableSelectionOnExpense();
	}

public static void initalize()
{
	AccountsAPHomeForm accHomeForm = new AccountsAPHomeForm();
	AppMemory.getAppMemory().currentScreen=Screen.ACCOUNTSDASHBOARDAP;
	AppMemory.getAppMemory().skeleton.getProcessName().setText("Dashboard/Accounts (AP)");
	AccountsAPHomePresenterSearchProxy searchpopup=new AccountsAPHomePresenterSearchProxy();
	AccountsAPHomePresenter presenter= new AccountsAPHomePresenter(accHomeForm,searchpopup);
	AppMemory.getAppMemory().stickPnel(accHomeForm);	
}

@Override
public void onClick(ClickEvent event) {
	
	if(event.getSource() instanceof InlineLabel)
	{
	InlineLabel lbl= (InlineLabel) event.getSource();
	String text= lbl.getText();
	if(text.equals("Search"))
		searchpopup.showPopUp();
	
	if(text.equals("Go"))
		reactOnGo();
	}
	
	
//	if(event.getSource().equals(form.getBtnGo())){if(form.getDbFromDate().getValue()==null||form.getDbToDate().getValue()==null){
//		final GWTCAlert alert = new GWTCAlert();
//		alert.alert("Select From and To Date.");
//	}
//	if(form.getDbFromDate().getValue()!=null&&form.getDbToDate()!=null){
//		Date formDate=form.getDbFromDate().getValue();
//		Date toDate=form.getDbToDate().getValue();
//		if(toDate.equals(formDate)||toDate.after(formDate)){
//			System.out.println("From Date ::::::::: "+formDate);
//			CalendarUtil.addDaysToDate(formDate,7);
//			System.out.println("Changed Date ::::::::: "+formDate);
//			System.out.println("To Date ::::::::: "+toDate);
//		
//			if(toDate.before(formDate)){
//				System.out.println("Inside Date Condition"); 
//				searchByFromToDate();
//			}
//			else{
//				final GWTCAlert alert = new GWTCAlert();
//				alert.alert("From Date and To Date Difference Can Not Be More Than 7 Days.");
//			}
//		}
//		else{
//			final GWTCAlert alert = new GWTCAlert();
//			alert.alert("To Date should be greater than From Date.");
//		}
//	}}
	
	if(event.getSource().equals(form.getBtnGo())){
		if(form.getOlbbranch().getSelectedIndex()==0&&
		form.getDbFromDate().getValue()==null&&form.getDbToDate().getValue()==null){
	
		final GWTCAlert alert = new GWTCAlert();
		alert.alert("Select atleast one field to search");
	}
		
	else{	
		
		if(form.getOlbbranch().getSelectedIndex()!=0
				   &&( form.getDbFromDate().getValue()==null&&form.getDbToDate()!=null)){
			
			System.out.println("branch ");
			searchByFromToDate();
		}
		else{
		System.out.println("Remaining all situations ");
		
		if(form.getDbFromDate().getValue()==null||form.getDbToDate().getValue()==null){
			final GWTCAlert alert = new GWTCAlert();
			alert.alert("Select From and To Date.");
		}
		 if(form.getDbFromDate().getValue()!=null&&form.getDbToDate()!=null){
		Date formDate=form.getDbFromDate().getValue();
		Date toDate=form.getDbToDate().getValue();
		if(toDate.equals(formDate)||toDate.after(formDate)){
			System.out.println("From Date ::::::::: "+formDate);
			CalendarUtil.addDaysToDate(formDate,7);
			System.out.println("Changed Date ::::::::: "+formDate);
			System.out.println("To Date ::::::::: "+toDate);
		
			if(toDate.before(formDate)){
				System.out.println("Inside Date Condition"); 
				searchByFromToDate();
			}
			else{
				final GWTCAlert alert = new GWTCAlert();
				alert.alert("From Date and To Date Difference Can Not Be More Than 7 Days.");
			}
		}
		else{
			final GWTCAlert alert = new GWTCAlert();
			alert.alert("To Date should be greater than From Date.");
		}
		}
		}
	}
	}
}

public void SetEventHandling()
{
	 InlineLabel label[] =AppMemory.getAppMemory().skeleton.getMenuLabels();
	  for(int k=0;k<label.length;k++)
	  {
		 if( AppMemory.getAppMemory().skeleton.registration[k]!=null)
		    AppMemory.getAppMemory().skeleton.registration[k].removeHandler();
	  }
	 
	  for( int k=0;k<label.length;k++)
	  {
		  AppMemory.getAppMemory().skeleton.registration[k]= label[k].addClickHandler(this);
	  }
}

public void reactOnGo()
{ 
	
	if(!searchpopup.personInfo.getId().getValue().equals("")&&!searchpopup.personInfo.getName().getValue().equals("")&&!searchpopup.personInfo.getPhone().getValue().equals(""))
	{
		MyQuerry quer=createBillingFilter(new BillingDocument());
		 this.form.billingTable.connectToLocal();
		retriveTable(quer, form.getBillingTable());
		 
		quer=createInvoiceFilter();
		 this.form.invoiceTable.connectToLocal();
		 retriveTable(quer, form.getInvoiceTable());
		 
		 quer=createPaymentFilter();
		 this.form.paymentTable.connectToLocal();
		 retriveTable(quer, form.getPaymentTable());
		 
//		 quer=createExpenseFilter();
//		 this.form.expenseTable.connectToLocal();
//		 retriveTable(quer, form.getExpenseTable());
		 
		 searchpopup.hidePopUp();
	}

	if(searchpopup.personInfo.getId().getValue().equals("")&&searchpopup.personInfo.getName().getValue().equals("")&&searchpopup.personInfo.getPhone().getValue().equals(""))
	{
		GWTCAlert alertMsg=new GWTCAlert();
		alertMsg.alert("Please enter vendor information");
	}
	
	
	 
}

private MyQuerry createBillingFilter(BillingDocument biling)
{
	Vector<Filter>filterVec = new Vector<Filter>();
	 Filter temp;
	 
	 if(searchpopup.personInfo.getId().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getId().getValue()));
			temp.setQuerryString("personInfo.count");
			filterVec.add(temp);	
		}
		
		if((searchpopup.personInfo.getName().getText()).trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.personInfo.getName().getValue());
			temp.setQuerryString("personInfo.fullName");
			filterVec.add(temp);	
		}
		
		if(searchpopup.personInfo.getPhone().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getPhone().getValue()));
			temp.setQuerryString("personInfo.cellNumber");
			filterVec.add(temp);	
		}
		
		MyQuerry billing=new MyQuerry();
		billing.setQuerryObject(new BillingDocument());
		billing.setFilters(filterVec);
		
		return billing;
}

private MyQuerry createInvoiceFilter()
{
	Vector<Filter>filterVec = new Vector<Filter>();
	 Filter temp;
	 
	 if(searchpopup.personInfo.getId().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getId().getValue()));
			temp.setQuerryString("personInfo.count");
			filterVec.add(temp);	
		}
		
		if((searchpopup.personInfo.getName().getText()).trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.personInfo.getName().getValue());
			temp.setQuerryString("personInfo.fullName");
			filterVec.add(temp);	
		}
		
		if(searchpopup.personInfo.getPhone().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getPhone().getValue()));
			temp.setQuerryString("personInfo.cellNumber");
			filterVec.add(temp);	
		}
		
		MyQuerry invoicing=new MyQuerry();
		invoicing.setQuerryObject(new Invoice());
		invoicing.setFilters(filterVec);
		
		return invoicing;
}

private MyQuerry createPaymentFilter()
{
	Vector<Filter>filterVec = new Vector<Filter>();
	 Filter temp;
	 
	 if(searchpopup.personInfo.getId().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getId().getValue()));
			temp.setQuerryString("personInfo.count");
			filterVec.add(temp);	
		}
		
		if((searchpopup.personInfo.getName().getText()).trim().equals("")==false)
		{
			temp=new Filter();
			temp.setStringValue(searchpopup.personInfo.getName().getValue());
			temp.setQuerryString("personInfo.fullName");
			filterVec.add(temp);	
		}
		
		if(searchpopup.personInfo.getPhone().getText().trim().equals("")==false)
		{
			temp=new Filter();
			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getPhone().getValue()));
			temp.setQuerryString("personInfo.cellNumber");
			filterVec.add(temp);	
		}
		
		MyQuerry pay=new MyQuerry();
		pay.setQuerryObject(new CustomerPayment());
		pay.setFilters(filterVec);
		
		return pay;
}

//private MyQuerry createExpenseFilter()
//{
//	Vector<Filter>filterVec = new Vector<Filter>();
//	 Filter temp;
//	 
//	 if(searchpopup.personInfo.getId().getText().trim().equals("")==false)
//		{
//			temp=new Filter();
//			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getId().getValue()));
//			temp.setQuerryString("personInfo.count");
//			filterVec.add(temp);	
//		}
//		
//		if((searchpopup.personInfo.getName().getText()).trim().equals("")==false)
//		{
//			temp=new Filter();
//			temp.setStringValue(searchpopup.personInfo.getName().getValue());
//			temp.setQuerryString("personInfo.fullName");
//			filterVec.add(temp);	
//		}
//		
//		if(searchpopup.personInfo.getPhone().getText().trim().equals("")==false)
//		{
//			temp=new Filter();
//			temp.setLongValue(Long.parseLong(searchpopup.personInfo.getPhone().getValue()));
//			temp.setQuerryString("personInfo.cellNumber");
//			filterVec.add(temp);	
//		}
//		
//		MyQuerry pay=new MyQuerry();
//		pay.setQuerryObject(new Expense());
//		pay.setFilters(filterVec);
//		
//		return pay;
//}

public <T> void retriveTable(MyQuerry querry,final SuperTable<T>table)
 {
			table.connectToLocal();
			service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					for(SuperModel model:result)
					{
						table.getListDataProvider().getList().add((T) model);
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
				caught.printStackTrace();
					
				}
			}); 
 }


 public void setTableSelectionOnBilling()
 {
	 final NoSelectionModel<BillingDocument> selectionModelMyObj = new NoSelectionModel<BillingDocument>();
	 
     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
     {
         @Override
         public void onSelectionChange(SelectionChangeEvent event) 
         {
        	 final BillingDocument entity=selectionModelMyObj.getLastSelectedObject();
        	 AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
        	 BillingDetailsForm.flag=true;
        	 BillingDetailsForm.flag1=true;
        	 SalesOrderProductTable.newModeFlag=true;
        	 final BillingDetailsForm form = BillingDetailsPresenter.initalize();
        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Billing Details",Screen.BILLINGDETAILS);
        	
			 AppMemory.getAppMemory().stickPnel(form);
             form.showWaitSymbol();
			 Timer timer=new Timer() 
        	 {
 				@Override
 				public void run() {
 					 form.updateView(entity);
 					 form.setToViewState();
 					 BillingDetailsForm.flag=false;
 					 BillingDetailsForm.flag1=false;
 					
 					 /**
 					  * Steps For Retrieving Charges Based On Contract Id
 					  */
 					
 					MyQuerry querry=new MyQuerry();
 					Vector<Filter> filtervec=new Vector<Filter>();
 					Filter filter=null;
 					filter=new Filter();
 					filter.setQuerryString("contractId");
 					filter.setIntValue(entity.getContractCount());
 					filtervec.add(filter);
 					filter=new Filter();
 					filter.setQuerryString("companyId");
 					filter.setLongValue(entity.getCompanyId());
 					filtervec.add(filter);
 					querry.setFilters(filtervec);
 					querry.setQuerryObject(new TaxesAndCharges());
 					service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

 						@Override
 						public void onFailure(Throwable caught) {
 							form.showDialogMessage("An Unexpected Error occured !");
 							
 						}
 						@Override
 						public void onSuccess(ArrayList<SuperModel> result) {
 							final ArrayList<ContractCharges> arrConCharges=new ArrayList<ContractCharges>();
 							for(SuperModel bmodel:result)
 							{
 								TaxesAndCharges tncentity =(TaxesAndCharges)bmodel;
 								List<ContractCharges> lisConCharges=tncentity.getTaxesChargesList();
 								arrConCharges.addAll(lisConCharges);
 								System.out.println("Size Of Taxes And Charges"+arrConCharges.size());
 							}
 							if(entity.getBillingOtherCharges().size()!=0){
 								List<ContractCharges> updatedbalanceLis=updateChargesBalance(arrConCharges,entity.getBillingOtherCharges());
 								ArrayList<ContractCharges> updatedBalanceArr=new ArrayList<ContractCharges>();
 								updatedBalanceArr.addAll(updatedbalanceLis);
 								form.getBillingChargesTable().setValue(updatedBalanceArr);
 							}
 							if(entity.getBillingOtherCharges().size()==0){
 								form.getBillingChargesTable().setValue(arrConCharges);
 							}
 							
 						}
 					});

 			/**
 			 * End Of Retrieving Charges Table		
 			 */
 					 double retrieveTotalAmt=form.getSalesProductTable().calculateTotalBillAmt();
 					 retrieveTotalAmt=retrieveTotalAmt+calculateTotalTaxAmt(entity.getBillingTaxes());
 					 System.out.println("Total taxes"+form.getBillingTaxTable().calculateTotalTaxes());
 					System.out.println("Total taxes"+form.getBillingChargesTable().calculateTotalBillingCharges());
 					 System.out.println("Billing List Presenter Retrueve"+retrieveTotalAmt);
 					 BillingDocumentDetails billingdoc=new BillingDocumentDetails();
 					 
 					 //**************contrct id  mappig ****************
 					 if(entity.getContractCount()!=0)
 						billingdoc.setOrderId(entity.getContractCount());
 					 
 					 
 					 if(entity.getCount()!=0)
 						billingdoc.setBillId(entity.getCount());
 					 if(entity.getBillingDate()!=null)
 						billingdoc.setBillingDate(entity.getBillingDate());
 					 if(entity.getBillingCategory()!=null)
 						billingdoc.setBillingCategory(entity.getBillingCategory());
 					 if(entity.getBillingType()!=null)
 						billingdoc.setBillingType(entity.getBillingType());
 					 if(entity.getBillingGroup()!=null)
 						billingdoc.setBillingGroup(entity.getBillingGroup());
 					 if(entity.getTotalBillingAmount()!=null)
 						billingdoc.setBillAmount(retrieveTotalAmt);
 					 if(entity.getStatus()!=null)
 						billingdoc.setBillstatus(entity.getStatus());
 					 
 					 form.getBillingDocumentTable().getDataprovider().getList().add(billingdoc);
 					
				     BillingDetailsPresenter presenter=(BillingDetailsPresenter) form.getPresenter();
				     presenter.form.setMenus();
				     form.hideWaitSymbol();
 				}
 			};
             timer.schedule(3000); 
          }
     };
     // Add the handler to the selection model
     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
     // Add the selection model to the table
     form.billingTable.getTable().setSelectionModel(selectionModelMyObj);
 }
 
 public void setTableSelectionOnInvoice()
 {
	 final NoSelectionModel<Invoice> selectionModelMyObj = new NoSelectionModel<Invoice>();
	 
     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
     {
         @Override
         public void onSelectionChange(SelectionChangeEvent event) 
         {
        	 final Invoice inventity=selectionModelMyObj.getLastSelectedObject();
        	 final InvoiceDetailsForm form = new InvoiceDetailsForm();
        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Invoice Details",Screen.INVOICEDETAILS);
        	 InvoiceDetailsPresenter presenter=new InvoiceDetailsPresenter(form,inventity);
			 AppMemory.getAppMemory().stickPnel(form);
		
        	 
			 Timer timer=new Timer() 
        	 {
 				@Override
 				public void run() {
 					form.updateView(inventity);
 					 form.setToViewState();
				    InvoiceDetailsPresenter presenter=(InvoiceDetailsPresenter) form.getPresenter();
				    presenter.form.setAppHeaderBarAsPerStatus();
 				}
 			};
             timer.schedule(3700); 
          }
     };
     // Add the handler to the selection model
     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
     // Add the selection model to the table
     form.invoiceTable.getTable().setSelectionModel(selectionModelMyObj);
 }
 
 public void setTableSelectionOnPayment()
 {
	 final NoSelectionModel<CustomerPayment> selectionModelMyObj = new NoSelectionModel<CustomerPayment>();
	 
     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
     {
         @Override
         public void onSelectionChange(SelectionChangeEvent event) 
         {
        	 final CustomerPayment payentity=selectionModelMyObj.getLastSelectedObject();
        	 final PaymentDetailsForm form = new PaymentDetailsForm();
        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Payment Details",Screen.PAYMENTDETAILS);
        	 PaymentDetailsPresenter presenter=new PaymentDetailsPresenter(form,payentity);
			 AppMemory.getAppMemory().stickPnel(form);
			
        	 
			 Timer timer=new Timer() 
        	 {
 				@Override
 				public void run() {
 					form.updateView(payentity);
 					 form.setToViewState();
				    PaymentDetailsPresenter presenter=(PaymentDetailsPresenter) form.getPresenter();
				    presenter.form.setAppHeaderBarAsPerStatus();
 				}
 			};
             timer.schedule(3700); 
          }
     };
     // Add the handler to the selection model
     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
     // Add the selection model to the table
     form.paymentTable.getTable().setSelectionModel(selectionModelMyObj);
 }

// public void setTableSelectionOnExpense()
// {
//	 final NoSelectionModel<Expense> selectionModelMyObj = new NoSelectionModel<Expense>();
//	 
//     SelectionChangeEvent.Handler  tableHandler = new SelectionChangeEvent.Handler() 
//     {
//         @Override
//         public void onSelectionChange(SelectionChangeEvent event) 
//         {
//        	 final Expense expentity=selectionModelMyObj.getLastSelectedObject();
//        	 final ExpensemanagmentForm form = new ExpensemanagmentForm();
//        	 AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Accounts/Expense Management",Screen.EXPENSEMANAGMENT);
//        	 ExpensemanagmentPresenter presenter=new ExpensemanagmentPresenter(form,expentity);
//			 AppMemory.getAppMemory().stickPnel(form);
//			 form.setToViewState();
//        	 
//			 Timer timer=new Timer() 
//        	 {
// 				@Override
// 				public void run() {
// 					form.updateView(expentity);
//				    ExpensemanagmentPresenter presenter=(ExpensemanagmentPresenter) form.getPresenter();
// 				}
// 			};
//             timer.schedule(3700); 
//          }
//     };
//     // Add the handler to the selection model
//     selectionModelMyObj.addSelectionChangeHandler( tableHandler );
//     // Add the selection model to the table
//     form.expenseTable.getTable().setSelectionModel(selectionModelMyObj);
// }

 private List<ContractCharges> updateChargesBalance(List<ContractCharges> chargeLis,List<ContractCharges> formChargeLis)
 {
	 System.out.println("Charge List Retrieved "+chargeLis.size());
	 System.out.println("Form List Size"+formChargeLis.size());
	 ArrayList<ContractCharges> updatedBalArr=new ArrayList<ContractCharges>();
	 for(int i=0;i<formChargeLis.size();i++)
	 {
		 ContractCharges ccEntity=new ContractCharges();
		 ccEntity.setTaxChargeName(formChargeLis.get(i).getTaxChargeName());
		 ccEntity.setTaxChargePercent(formChargeLis.get(i).getTaxChargePercent());
		 ccEntity.setTaxChargeAbsVal(formChargeLis.get(i).getTaxChargeAbsVal());
		 ccEntity.setTaxChargeAssesVal(formChargeLis.get(i).getTaxChargeAssesVal());
		 ccEntity.setChargesBalanceAmt(chargeLis.get(i).getChargesBalanceAmt());
		 ccEntity.setPayableAmt(formChargeLis.get(i).getPayableAmt());
		 updatedBalArr.add(ccEntity);
	 }
	 return updatedBalArr;
 }


 private double calculateTotalTaxAmt(List<ContractCharges> listContarctCharge)
 {
	 double totalTax=0;
	 for(int i=0;i<listContarctCharge.size();i++)
	 {
		 totalTax=totalTax+(listContarctCharge.get(i).getTaxChargeAssesVal()*listContarctCharge.get(i).getTaxChargePercent()/100);
	 }
	 return totalTax;
 }
 
 
 private void searchByFromToDate(){
		
		
		System.out.println("Hiiiiiiiiiiii........");
		
		Vector<Filter> filtervec=null;
		Filter filter = null;
		MyQuerry querry;
		List<String> statusList;
		
		Long compId=UserConfiguration.getCompanyId();
	    System.out.println("Company Id :: "+compId);
	    
		
	    filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(BillingDocument.CREATED);
		statusList.add(BillingDocument.REQUESTED);
		statusList.add(BillingDocument.APPROVED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("accountType");
		filter.setStringValue(AppConstants.BILLINGACCOUNTTYPEAP);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		if(form.getDbFromDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("billingDocumentInfo.billingDate >=");
		filter.setDateValue(form.dbFromDate.getValue());
		filtervec.add(filter);
		}
		
		if(form.getDbToDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("billingDocumentInfo.billingDate <=");
		filter.setDateValue(form.getDbToDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
			System.out.println("inside lead***********");
				filter = new Filter();
				filter.setQuerryString("branch");
				filter.setStringValue(form.getOlbbranch().getValue());
				filtervec.add(filter);
				
	    }
		
		//Create Querry For Billing
		
		querry=new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new BillingDocument());
		form.getBillingTable().connectToLocal();
		this.retriveTable(querry,form.getBillingTable());
		
		////////////////////////////////////Create Querry For Invoice///////////////////////////// 
		
		
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(Invoice.CREATED);
		statusList.add(Invoice.REQUESTED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("accountType");
		filter.setStringValue(AppConstants.BILLINGACCOUNTTYPEAP);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		if(form.getDbFromDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("invoiceDate >=");
		filter.setDateValue(form.dbFromDate.getValue());
		filtervec.add(filter);
		}
		
		if(form.getDbToDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("invoiceDate <=");
		filter.setDateValue(form.getDbToDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
			System.out.println("inside lead***********");
				filter = new Filter();
				filter.setQuerryString("branch");
				filter.setStringValue(form.getOlbbranch().getValue());
				filtervec.add(filter);
				
	    }
		
		
	    querry=new MyQuerry();
		querry.setQuerryObject(new Invoice());
		querry.setFilters(filtervec);
		form.getInvoiceTable().connectToLocal();
		retriveTable(querry,form.getInvoiceTable());
		
		
		///////////Create Querry For Payment/////////////////////////////////////////////////////
		
		filtervec=new Vector<Filter>();
		statusList=new ArrayList<String>();
		statusList.add(CustomerPayment.CREATED);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(compId);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("accountType");
		filter.setStringValue(AppConstants.BILLINGACCOUNTTYPEAP);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("status IN");
		filter.setList(statusList);
		filtervec.add(filter);
		
		if(form.getDbFromDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("paymentDate >=");
		filter.setDateValue(form.dbFromDate.getValue());
		filtervec.add(filter);
		}
		
		if(form.getDbToDate().getValue()!=null){
		filter = new Filter();
		filter.setQuerryString("paymentDate <=");
		filter.setDateValue(form.getDbToDate().getValue());
		filtervec.add(filter);
		}
		
		if(form.getOlbbranch().getSelectedIndex()!=0){
			System.out.println("inside lead***********");
				filter = new Filter();
				filter.setQuerryString("branch");
				filter.setStringValue(form.getOlbbranch().getValue());
				filtervec.add(filter);
				
	    }
		querry=new MyQuerry();
		querry.setQuerryObject(new CustomerPayment());
		querry.setFilters(filtervec);
		form.getPaymentTable().connectToLocal();
		retriveTable(querry,form.getPaymentTable());
		
		////////////////////Create Querry For Expense//////////////////////
		
//		filtervec=new Vector<Filter>();
//		statusList=new ArrayList<String>();
//		statusList.add(Expense.CREATED);
//		statusList.add(Expense.REQUESTED);
//		
//		filter = new Filter();
//		filter.setQuerryString("companyId");
//		filter.setLongValue(compId);
//		filtervec.add(filter);
//		
//		filter = new Filter();
//		filter.setQuerryString("status IN");
//		filter.setList(statusList);
//		filtervec.add(filter);
//		
//		filter = new Filter();
//		filter.setQuerryString("expenseDate >=");
//		filter.setDateValue(form.dbFromDate.getValue());
//		filtervec.add(filter);
//
//		filter = new Filter();
//		filter.setQuerryString("expenseDate <=");
//		filter.setDateValue(form.getDbToDate().getValue());
//		filtervec.add(filter);
//		
//		querry=new MyQuerry();
//		querry.setQuerryObject(new Expense());
//		querry.setFilters(filtervec);
//		form.getExpenseTable().connectToLocal();
//		retriveTable(querry,form.getExpenseTable());
	}
}
