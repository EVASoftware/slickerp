package com.slicktechnologies.client.views.accountsdashboardap;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.view.client.ProvidesKey;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsPresenter.PaymentDetailsPresenterTable;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;

public class PaymentAPPresenterTableProxy extends PaymentDetailsPresenterTable {TextColumn<CustomerPayment> getpaymentIdColumn;
TextColumn<CustomerPayment> getpaymentDateColumn;
TextColumn<CustomerPayment> getcustomerIdColumn;
TextColumn<CustomerPayment> getcustomerNameColumn;
TextColumn<CustomerPayment> getcustomerCellColumn;
TextColumn<CustomerPayment> getcontractIdColumn;
TextColumn<CustomerPayment> getSalesAmtColumn;
TextColumn<CustomerPayment> getinvoiceAmtColumn;
TextColumn<CustomerPayment> getstatusColumn;
TextColumn<CustomerPayment> getbranchColumn;

TextColumn<CustomerPayment> getbalanceColumn;
TextColumn<CustomerPayment> getPaidAmtColumn;

public Object getVarRef(String varName)
{
	if(varName.equals("getpaymentIdColumn"))
		return this.getpaymentIdColumn;
	if(varName.equals("getpaymentDateColumn"))
		return this.getpaymentDateColumn;
	if(varName.equals("getcustomerIdColumn"))
		return this.getcustomerIdColumn;
	if(varName.equals("getcustomerNameColumn"))
		return this.getcustomerNameColumn;
	if(varName.equals("getcustomerCellColumn"))
		return this.getcustomerCellColumn;
	if(varName.equals("getcontractIdColumn"))
		return this.getcontractIdColumn;
	if(varName.equals("getSalesAmtColumn"))
		return this.getSalesAmtColumn;
	if(varName.equals("getinvoiceAmtColumn"))
		return this.getinvoiceAmtColumn;
	if(varName.equals("getstatusColumn"))
		return this.getstatusColumn;
	if(varName.equals("getbranchColumn"))
		return this.getbranchColumn;
	
	if(varName.equals("getbalanceColumn"))
		return this.getbalanceColumn;
	
	return null ;
}
public PaymentAPPresenterTableProxy()
{
	super();
}

@Override 
public void createTable() {
	addColumngetCount();
	addColumngetPaymentDate();
	addColumngetCustomerId();
	addColumngetCustomerName();
	addColumngetCustomerCell();
	addColumngetContractCount();
	addColumngetSalesAmount();
	addColumngetInvoiceAmount();
	addColumngetBalanceAmt();
	addColumngetPaidAmt();
	addColumngetStatus();
	addColumngetBranch();
	addColumnSorting();
}
private void addColumngetPaidAmt() {
	
	getPaidAmtColumn=new TextColumn<CustomerPayment>()
	{
	@Override
	public String getValue(CustomerPayment object)
	{
		return object.getInvoiceAmount()-object.getPaymentAmt()+"";
	}
		};
		table.addColumn(getPaidAmtColumn,"Paid Amt");
		getPaidAmtColumn.setSortable(true);	
	
}

private void addColumngetBalanceAmt() {
	getbalanceColumn=new TextColumn<CustomerPayment>()
			{
		@Override
		public String getValue(CustomerPayment object)
		{
			return object.getPaymentAmt()+"";
		}
			};
			table.addColumn(getbalanceColumn,"Bal Amt");
			getbalanceColumn.setSortable(true);	
}
@Override
protected void initializekeyprovider() {
	keyProvider= new ProvidesKey<CustomerPayment>()
			{
		@Override
		public Object getKey(CustomerPayment item)
		{
			if(item==null)
			{
				return null;
			}
			else
				return item.getId();
		}
			};
}

@Override
public void setEnable(boolean state)
{
}

@Override
public void applyStyle()
{
}

public void addColumnSorting(){
	addSortinggetCount();
	addSortinggetPaymentDate();
	addSortinggetCustomerId();
	addSortinggetCustomerName();
	addSortinggetCustomerCell();
	addSortinggetContractCount();
	addSortinggetSalesAmount();
	addSortinggetInvoiceAmount();
	addSortinggetStatus();
	addSortinggetBranch();
}
@Override public void addFieldUpdater() {
}



protected void addColumngetCount()
{
	getpaymentIdColumn=new TextColumn<CustomerPayment>()
			{
		@Override
		public String getValue(CustomerPayment object)
		{
			return object.getCount()+"";
		}
			};
			table.addColumn(getpaymentIdColumn,"ID");
			getpaymentIdColumn.setSortable(true);
}

protected void addSortinggetCount()
  {
  List<CustomerPayment> list=getDataprovider().getList();
  columnSort=new ListHandler<CustomerPayment>(list);
  columnSort.setComparator(getpaymentIdColumn, new Comparator<CustomerPayment>()
  {
  @Override
  public int compare(CustomerPayment e1,CustomerPayment e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getCount()== e2.getCount()){
  return 0;}
  if(e1.getCount()> e2.getCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }

protected void addColumngetPaymentDate()
{
	final Date todaysDate=new Date();
	CalendarUtil.addDaysToDate(todaysDate,-1);
	todaysDate.setHours(23);
	todaysDate.setMinutes(59);
	todaysDate.setSeconds(59);
	System.out.println("Todays Date Converted ::: "+todaysDate);
	
	getpaymentDateColumn=new TextColumn<CustomerPayment>()
	{
		@Override
		public String getCellStyleNames(Context context, CustomerPayment object) {
			if(object.getPaymentDate()!=null&&(object.getPaymentDate().before(todaysDate))&&(object.getStatus().equals("Created")||object.getStatus().equals("Requested"))){
				 return "red";
			 }
			 else if(object.getStatus().equals("Approved")||object.getStatus().equals("Closed")){
				 return "green";
			 }
			 else{
				 return "black";
			 }
		}
		
		@Override
		public String getValue(CustomerPayment object)
		{
			return AppUtility.parseDate(object.getPaymentDate());
		}
	};
	table.addColumn(getpaymentDateColumn,"Payment Date");
	getpaymentDateColumn.setSortable(true);
}

protected void addSortinggetPaymentDate()
{
	List<CustomerPayment> list=getDataprovider().getList();
	columnSort=new ListHandler<CustomerPayment>(list);
	columnSort.setComparator(getpaymentDateColumn, new Comparator<CustomerPayment>()
			{
		@Override
		public int compare(CustomerPayment e1,CustomerPayment e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getPaymentDate()!=null && e2.getPaymentDate()!=null){
					return e1.getPaymentDate().compareTo(e2.getPaymentDate());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}


protected void addColumngetCustomerId()
{
	getcustomerIdColumn=new TextColumn<CustomerPayment>()
			{
		@Override
		public String getValue(CustomerPayment object)
		{
			return object.getPersonInfo().getCount()+"";
		}
			};
			table.addColumn(getcustomerIdColumn,"Customer ID");
			getcustomerIdColumn.setSortable(true);
}

protected void addSortinggetCustomerId()
  {
  List<CustomerPayment> list=getDataprovider().getList();
  columnSort=new ListHandler<CustomerPayment>(list);
  columnSort.setComparator(getcustomerIdColumn, new Comparator<CustomerPayment>()
  {
  @Override
  public int compare(CustomerPayment e1,CustomerPayment e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getPersonInfo().getCount()== e2.getPersonInfo().getCount()){
  return 0;}
  if(e1.getPersonInfo().getCount()> e2.getPersonInfo().getCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }


protected void addColumngetCustomerName()
{
	getcustomerNameColumn=new TextColumn<CustomerPayment>()
			{
		@Override
		public String getValue(CustomerPayment object)
		{
			return object.getPersonInfo().getFullName()+"";
		}
			};
			table.addColumn(getcustomerNameColumn,"Customer Name");
			getcustomerNameColumn.setSortable(true);
}

protected void addSortinggetCustomerName()
{
	List<CustomerPayment> list=getDataprovider().getList();
	columnSort=new ListHandler<CustomerPayment>(list);
	columnSort.setComparator(getcustomerNameColumn, new Comparator<CustomerPayment>()
			{
		@Override
		public int compare(CustomerPayment e1,CustomerPayment e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getPersonInfo().getFullName()!=null && e2.getPersonInfo().getFullName()!=null){
					return e1.getPersonInfo().getFullName().compareTo(e2.getPersonInfo().getFullName());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetCustomerCell()
{
	
	getcustomerCellColumn=new TextColumn<CustomerPayment>()
			{
		@Override
		public String getValue(CustomerPayment object)
		{
			if(object.getPersonInfo().getCellNumber()==0){
				return 0+"";}
			else{
				return object.getPersonInfo().getCellNumber()+"";}
		}
			};
			table.addColumn(getcustomerCellColumn,"Customer Cell");
			getcustomerCellColumn.setSortable(true);
}

protected void addSortinggetCustomerCell()
{
	List<CustomerPayment> list=getDataprovider().getList();
	columnSort=new ListHandler<CustomerPayment>(list);
	columnSort.setComparator(getcustomerCellColumn, new Comparator<CustomerPayment>()
			{
		@Override
		public int compare(CustomerPayment e1,CustomerPayment e2)
		{
			if(e1!=null && e2!=null)
			{
				if(e1.getPersonInfo().getCellNumber()== e2.getPersonInfo().getCellNumber()){
					return 0;}
				if(e1.getPersonInfo().getCellNumber()> e2.getPersonInfo().getCellNumber()){
					return 1;}
				else{
					return -1;}
			}
			else{
				return 0;}
		}
			});
	table.addColumnSortHandler(columnSort);
}

protected void addColumngetContractCount()
{
	getcontractIdColumn=new TextColumn<CustomerPayment>()
			{
		@Override
		public String getValue(CustomerPayment object)
		{
			return object.getContractCount()+"";
		}
			};
			table.addColumn(getcontractIdColumn,"Contract ID");
			getcontractIdColumn.setSortable(true);
}

protected void addSortinggetContractCount()
  {
  List<CustomerPayment> list=getDataprovider().getList();
  columnSort=new ListHandler<CustomerPayment>(list);
  columnSort.setComparator(getcontractIdColumn, new Comparator<CustomerPayment>()
  {
  @Override
  public int compare(CustomerPayment e1,CustomerPayment e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getContractCount()== e2.getContractCount()){
  return 0;}
  if(e1.getContractCount()> e2.getContractCount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }


protected void addColumngetSalesAmount()
{
	getSalesAmtColumn=new TextColumn<CustomerPayment>()
			{
		@Override
		public String getValue(CustomerPayment object)
		{
			return object.getTotalSalesAmount()+"";
		}
			};
			table.addColumn(getSalesAmtColumn,"Sales Amount");
			getSalesAmtColumn.setSortable(true);
}

protected void addSortinggetSalesAmount()
  {
  List<CustomerPayment> list=getDataprovider().getList();
  columnSort=new ListHandler<CustomerPayment>(list);
  columnSort.setComparator(getSalesAmtColumn, new Comparator<CustomerPayment>()
  {
  @Override
  public int compare(CustomerPayment e1,CustomerPayment e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getTotalSalesAmount()== e2.getTotalSalesAmount()){
  return 0;}
  if(e1.getTotalSalesAmount()> e2.getTotalSalesAmount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }

protected void addColumngetInvoiceAmount()
{
	getinvoiceAmtColumn=new TextColumn<CustomerPayment>()
			{
		@Override
		public String getValue(CustomerPayment object)
		{
			return object.getInvoiceAmount()+"";
		}
			};
			table.addColumn(getinvoiceAmtColumn,"Invoice Amount");
			getinvoiceAmtColumn.setSortable(true);
}

protected void addSortinggetInvoiceAmount()
  {
  List<CustomerPayment> list=getDataprovider().getList();
  columnSort=new ListHandler<CustomerPayment>(list);
  columnSort.setComparator(getinvoiceAmtColumn, new Comparator<CustomerPayment>()
  {
  @Override
  public int compare(CustomerPayment e1,CustomerPayment e2)
  {
  if(e1!=null && e2!=null)
  {
  if(e1.getTotalBillingAmount()== e2.getTotalBillingAmount()){
  return 0;}
  if(e1.getTotalBillingAmount()> e2.getTotalBillingAmount()){
  return 1;}
  else{
  return -1;}
  }
  else{
  return 0;}
  }
  });
  table.addColumnSortHandler(columnSort);
  }

protected void addColumngetStatus()
{
	getstatusColumn=new TextColumn<CustomerPayment>()
			{
		@Override
		public String getValue(CustomerPayment object)
		{
			return object.getStatus()+"";
		}
			};
			table.addColumn(getstatusColumn,"Status");
			getstatusColumn.setSortable(true);
}


protected void addSortinggetStatus()
{
	List<CustomerPayment> list=getDataprovider().getList();
	columnSort=new ListHandler<CustomerPayment>(list);
	columnSort.setComparator(getstatusColumn, new Comparator<CustomerPayment>()
			{
		@Override
		public int compare(CustomerPayment e1,CustomerPayment e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getStatus()!=null && e2.getStatus()!=null){
					return e1.getStatus().compareTo(e2.getStatus());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}


protected void addColumngetBranch()
{
	getbranchColumn=new TextColumn<CustomerPayment>()
			{
		@Override
		public String getValue(CustomerPayment object)
		{
			return object.getBranch();
		}
			};
			table.addColumn(getbranchColumn,"Status");
			getbranchColumn.setSortable(true);
}


protected void addSortinggetBranch()
{
	List<CustomerPayment> list=getDataprovider().getList();
	columnSort=new ListHandler<CustomerPayment>(list);
	columnSort.setComparator(getbranchColumn, new Comparator<CustomerPayment>()
			{
		@Override
		public int compare(CustomerPayment e1,CustomerPayment e2)
		{
			if(e1!=null && e2!=null)
			{
				if( e1.getBranch()!=null && e2.getBranch()!=null){
					return e1.getBranch().compareTo(e2.getBranch());}
			}
			else{
				return 0;}
			return 0;
		}
			});
	table.addColumnSortHandler(columnSort);
}


}
