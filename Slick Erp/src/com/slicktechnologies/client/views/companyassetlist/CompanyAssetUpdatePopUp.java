package com.slicktechnologies.client.views.companyassetlist;

import java.util.Date;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;

public class CompanyAssetUpdatePopUp extends AbsolutePanel implements ValueChangeHandler<Date>{

	private DateBox issuedDate;
	private Button btnOk;
	private Button btnCancel;
	private TextBox IssuedAt,issuedBy;
	private IntegerBox validityinDays; 
	
	
	public CompanyAssetUpdatePopUp(){
		
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy"); 

		InlineLabel inlinelbIssuedDate = new InlineLabel("Issued Date:");
		add(inlinelbIssuedDate, 10, 10);
		
		issuedDate = new DateBoxWithYearSelector();
		issuedDate.addValueChangeHandler(this);
		issuedDate.setValue(new Date());

		add(issuedDate, 10, 30);
		issuedDate.setFormat(new DateBox.DefaultFormat(dateFormat));
		
		InlineLabel inlnlbValidityindays = new InlineLabel("Validity (In days):");
		add(inlnlbValidityindays, 200, 10);
		validityinDays = new IntegerBox();
		add(validityinDays,200,30);
		
		InlineLabel inlnlbIssuedAt = new InlineLabel("Issued At      :");
		add(inlnlbIssuedAt, 10, 80);
		IssuedAt = new TextBox();
		add(IssuedAt,10,100);
		
		InlineLabel inlnlbIssuedBy = new InlineLabel("Issued By      :");
		add(inlnlbIssuedBy, 200, 80);
		issuedBy = new TextBox();
		add(issuedBy,200,100);
		

		btnOk = new Button("Ok");
		add(btnOk, 120, 150);
		
		
		btnCancel = new Button("Cancel");
		add(btnCancel,180,150);
		
		setSize("400px", "200px");
		this.getElement().setId("form");
	}


	public DateBox getIssuedDate() {
		return issuedDate;
	}


	public void setIssuedDate(DateBox issuedDate) {
		this.issuedDate = issuedDate;
	}


	public Button getBtnOk() {
		return btnOk;
	}


	public void setBtnOk(Button btnOk) {
		this.btnOk = btnOk;
	}


	public Button getBtnCancel() {
		return btnCancel;
	}


	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}


	public TextBox getIssuedAt() {
		return IssuedAt;
	}


	public void setIssuedAt(TextBox issuedAt) {
		IssuedAt = issuedAt;
	}


	public TextBox getIssuedBy() {
		return issuedBy;
	}


	public void setIssuedBy(TextBox issuedBy) {
		this.issuedBy = issuedBy;
	}


	public IntegerBox getValidityinDays() {
		return validityinDays;
	}


	public void setValidityinDays(IntegerBox validityinDays) {
		this.validityinDays = validityinDays;
	}


	@Override
	public void onValueChange(ValueChangeEvent<Date> event) {
		// TODO Auto-generated method stub
		
	}
	
	public void clear(){
//		issuedDate.setValue(null);
		validityinDays.setValue(null);
		IssuedAt.setValue("");
		issuedBy.setValue("");
	}


	public boolean validate() {
		System.out.println("issud date =="+issuedDate.getValue());
		System.out.println(" Validity in days =="+validityinDays.getValue());
		System.out.println(" Issued At === "+IssuedAt.getValue());
		System.out.println("Issued By ==="+issuedBy.getValue());
		if(issuedDate.getValue()==null){
			showDialogeMessage("Please select Issue date !");
			return false;
		}
		if(validityinDays.getValue()==null){
			showDialogeMessage("Validity (in Days) can not be blank");
			return false;
		}
		if(IssuedAt.getValue().equals("")){
			showDialogeMessage("issued at can not be blank");
			return false;
		}
		if(issuedBy.getValue().equals("")){
			showDialogeMessage("issued by can not be blank");
			return false;
		}
		return true;
	}
	
	public void showDialogeMessage(String message){
		final GWTCAlert alert = new GWTCAlert(); 
	    alert.alert(message);
	}
}
