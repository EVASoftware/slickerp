package com.slicktechnologies.client.views.companyassetlist;

import java.util.Vector;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SearchPopUpScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.slicktechnologies.shared.common.CompanyAssetListArticleInformation;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;

public class CompanyAssetListSearchProxy extends SearchPopUpScreen<CompanyAssetListArticleInformation> {

	public DateComparator dateComparator;
//	public ObjectListBox<ConfigCategory> olbVehicleCategory;
//	public ObjectListBox<Type> olbVehicleType;
	public IntegerBox ibAssetId;
	public ListBox lbStatus;
	
	
	public CompanyAssetListSearchProxy() {
		super();
		createGui();
	}
	
	public CompanyAssetListSearchProxy(boolean b) {
		super(b);
		createGui();
	}
	
	public void initWidget()
	{
		dateComparator = new DateComparator("articleTypeDetails.validityuntil", new CompanyAsset());
	 
//		olbVehicleCategory=new ObjectListBox<ConfigCategory>();
//		AppUtility.MakeLiveCategoryConfig(olbVehicleCategory, Screen.VEHICLECATEGORY);
//		olbVehicleCategory.addChangeHandler(this);
//		
//		olbVehicleType=new ObjectListBox<Type>();
//		AppUtility.makeTypeListBoxLive(olbVehicleType,Screen.VEHICLETYPE);
			
		ibAssetId = new IntegerBox();
		
		lbStatus = new ListBox();
		lbStatus.addItem("--SELECT--");
		lbStatus.addItem("Active");
		lbStatus.addItem("Closed");
	}
	
	public void createScreen()
	{
		initWidget();
	
		FormFieldBuilder builder;
		
		builder = new FormFieldBuilder("From Date (Validiti Until Date)",dateComparator.getFromDate());
		FormField fdateComparatorfrom= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		builder = new FormFieldBuilder("To Date (Validiti Until Date)",dateComparator.getToDate());
		FormField fdateComparatorto= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
//		builder = new FormFieldBuilder("Vehicle Category",olbVehicleCategory);
//		FormField folbVehicleCategory= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
//		
//		builder = new FormFieldBuilder("Vehicle Type",olbVehicleType);
//		FormField folbVehicleType= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Asset Id",ibAssetId);
		FormField fibAssetId= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		builder = new FormFieldBuilder("Status",lbStatus);
		FormField flbStatus= builder.setMandatory(false).setRowSpan(0).setColSpan(0).build();
		
		this.fields=new FormField[][]{
				{fdateComparatorfrom,fdateComparatorto,fibAssetId,flbStatus},
		};
	}	
	
	@Override
	public MyQuerry getQuerry() {
		
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter=null;
		
			if(dateComparator.getValue()!=null)
				filtervec.addAll(dateComparator.getValue());
		
		
//			if(olbVehicleCategory.getSelectedIndex()!=0){
//				 filter=new Filter();
//				 filter.setStringValue(olbVehicleCategory.getValue().trim());
//				 filter.setQuerryString("vehicleCategory");
//				 filtervec.add(filter);
//			}
//			
//			if(olbVehicleType.getSelectedIndex()!=0){
//				 filter=new Filter();
//				 filter.setStringValue(olbVehicleType.getValue().trim());
//				 filter.setQuerryString("vehicleType");
//				 filtervec.add(filter);
//			}
			
			if(ibAssetId.getValue()!=null){
				 filter=new Filter();
				 filter.setIntValue(ibAssetId.getValue());
				 filter.setQuerryString("count");
				 filtervec.add(filter);
			}
			
			if(lbStatus.getSelectedIndex()!=0){
				 filter=new Filter();
				 filter.setStringValue(lbStatus.getValue(lbStatus.getSelectedIndex()));
				 filter.setQuerryString("articleTypeDetails.status");
				 filtervec.add(filter);
			}
		 
			MyQuerry querry= new MyQuerry();
			querry.setFilters(filtervec);
			querry.setQuerryObject(new CompanyAsset());
			return querry;
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return false;
	}

	



//	@Override
//	public void onChange(ChangeEvent event) {
//		if(event.getSource().equals(olbVehicleCategory))
//		{
//			if(olbVehicleCategory.getSelectedIndex()!=0){
//				ConfigCategory cat=olbVehicleCategory.getSelectedItem();
//				if(cat!=null){
//					AppUtility.makeLiveTypeDropDown(olbVehicleType, cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
//				}
//			}
//		}
//		
//	}
}
