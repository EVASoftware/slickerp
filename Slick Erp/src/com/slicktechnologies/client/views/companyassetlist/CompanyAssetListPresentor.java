package com.slicktechnologies.client.views.companyassetlist;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreen;
import com.simplesoftwares.client.library.appstructure.tablescreen.TableScreenPresenter;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CsvService;
import com.slicktechnologies.client.services.CsvServiceAsync;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.CompanyAssetListArticleInformation;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;

public class CompanyAssetListPresentor extends TableScreenPresenter<CompanyAssetListArticleInformation> {

	TableScreen<CompanyAssetListArticleInformation>form;
	CsvServiceAsync csvservice=GWT.create(CsvService.class);
	
	protected GenricServiceAsync genservice=GWT.create(GenricService.class);
	
	
	public CompanyAssetListPresentor(TableScreen<CompanyAssetListArticleInformation> view,CompanyAssetListArticleInformation model) {
		super(view, model);
		form=view;
//		retrivelist(); //Ashwini Patil Date:23-08-2024 to stop auto loading

		setCompanyAssetlistflag(true);
		form.getSearchpopupscreen().getDwnload().setVisible(false);
		boolean isDownload=AuthorizationHelper.getDownloadAuthorization(Screen.COMPANYASSETLIST,LoginPresenter.currentModule.trim());
		if(isDownload==false){
			form.getSearchpopupscreen().getDwnload().setVisible(false);
		}
	}
	
	
	private void retrivelist() {
		
		form.getSuperTable().connectToLocal();
		
		Date date=new Date();
		System.out.println("Today Date ::::::::: "+date);
		CalendarUtil.addDaysToDate(date,30);
	    System.out.println("Changed Date + 30 ::::::::: "+date);

	    final Date thirtyDaysDate = date;
	    
		 MyQuerry querry=new MyQuerry();
		 Vector<Filter> filtrvec=new Vector<Filter>();
		 Filter filterval=null;
		 
		 filterval=new Filter();
		 filterval.setQuerryString("articleTypeDetails.validityuntil <=");
		 filterval.setDateValue(date);
		 filtrvec.add(filterval);
		 
		 filterval=new Filter();
		 filterval.setQuerryString("companyId");
		 filterval.setLongValue(model.getCompanyId());
		 filtrvec.add(filterval);
		 
		 querry.setFilters(filtrvec);
		 querry.setQuerryObject(new CompanyAsset());
		 genservice.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("Result === "+result.size());
				if(result.size()!=0){
				ArrayList<CompanyAssetListArticleInformation> companyAssetlist = new ArrayList<CompanyAssetListArticleInformation>();

					for(int i=0;i<result.size();i++){
						
						CompanyAsset companyAsset = (CompanyAsset) result.get(i);
						
						for(int p=0;p<companyAsset.getArticleTypeDetails().size();p++){
							
							if(companyAsset.getArticleTypeDetails().get(p).getValidityuntil().before(thirtyDaysDate)&& companyAsset.getArticleTypeDetails().get(p).getStatus().equals("Active")){
								CompanyAssetListArticleInformation companyAssetinfo = new CompanyAssetListArticleInformation();
								companyAssetinfo.setAssetId(companyAsset.getCount());
//								companyAssetinfo.setVehicleCategory(companyAsset.getVehicleCategory());
//								companyAssetinfo.setVehicleType(companyAsset.getVehicleType());
								companyAssetinfo.setRegistrationNo(companyAsset.getName());
								companyAssetinfo.setArticleType(companyAsset.getArticleTypeDetails().get(p).getArticleTypeName());
								companyAssetinfo.setArticleNumber(companyAsset.getArticleTypeDetails().get(p).getArticleTypeValue());
								companyAssetinfo.setValidityInDays(companyAsset.getArticleTypeDetails().get(p).getValidity());
								companyAssetinfo.setIssueDate(companyAsset.getArticleTypeDetails().get(p).getIssueDate());
								companyAssetinfo.setIssuedAt(companyAsset.getArticleTypeDetails().get(p).getIssuedAt());
								companyAssetinfo.setIssuedBy(companyAsset.getArticleTypeDetails().get(p).getIssuedBy());
								companyAssetinfo.setValidityUntil(companyAsset.getArticleTypeDetails().get(p).getValidityuntil());
								companyAssetinfo.setStatus(companyAsset.getArticleTypeDetails().get(p).getStatus());
								companyAssetlist.add(companyAssetinfo);
							}
						}
						
						
					}
					
					form.getSuperTable().getDataprovider().setList(companyAssetlist);
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	
	private void retrivelist(final MyQuerry querry) {
		form.getSuperTable().connectToLocal();

		 genservice.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("Result === "+result.size());
				if(result.size()!=0){
				ArrayList<CompanyAssetListArticleInformation> companyAssetlist = new ArrayList<CompanyAssetListArticleInformation>();

				String status =null;
				for(int i=0;i<querry.getFilters().size();i++){
					
					if(querry.getFilters().get(i).getQuerryString().equals("articleTypeDetails.status")){
						status = querry.getFilters().get(i).getStringValue();
						System.out.println("Status === "+status);
					}
					
				}
					for(int i=0;i<result.size();i++){
						
						CompanyAsset companyAsset = (CompanyAsset) result.get(i);
						
						for(int p=0;p<companyAsset.getArticleTypeDetails().size();p++){if(status!=null){
							if(status.equals(companyAsset.getArticleTypeDetails().get(p).getStatus())){
							CompanyAssetListArticleInformation companyAssetinfo = new CompanyAssetListArticleInformation();
							companyAssetinfo.setAssetId(companyAsset.getCount());
//							companyAssetinfo.setVehicleCategory(companyAsset.getVehicleCategory());
//							companyAssetinfo.setVehicleType(companyAsset.getVehicleType());
							companyAssetinfo.setRegistrationNo(companyAsset.getName());
							companyAssetinfo.setArticleType(companyAsset.getArticleTypeDetails().get(p).getArticleTypeName());
							companyAssetinfo.setArticleNumber(companyAsset.getArticleTypeDetails().get(p).getArticleTypeValue());
							companyAssetinfo.setValidityInDays(companyAsset.getArticleTypeDetails().get(p).getValidity());
							companyAssetinfo.setIssueDate(companyAsset.getArticleTypeDetails().get(p).getIssueDate());
							companyAssetinfo.setIssuedAt(companyAsset.getArticleTypeDetails().get(p).getIssuedAt());
							companyAssetinfo.setIssuedBy(companyAsset.getArticleTypeDetails().get(p).getIssuedBy());
							companyAssetinfo.setValidityUntil(companyAsset.getArticleTypeDetails().get(p).getValidityuntil());
							companyAssetinfo.setStatus(companyAsset.getArticleTypeDetails().get(p).getStatus());

							companyAssetlist.add(companyAssetinfo);
							}
						
						}else{
							
							CompanyAssetListArticleInformation companyAssetinfo = new CompanyAssetListArticleInformation();
							companyAssetinfo.setAssetId(companyAsset.getCount());
//							companyAssetinfo.setVehicleCategory(companyAsset.getVehicleCategory());
//							companyAssetinfo.setVehicleType(companyAsset.getVehicleType());
							companyAssetinfo.setRegistrationNo(companyAsset.getName());
							companyAssetinfo.setArticleType(companyAsset.getArticleTypeDetails().get(p).getArticleTypeName());
							companyAssetinfo.setArticleNumber(companyAsset.getArticleTypeDetails().get(p).getArticleTypeValue());
							companyAssetinfo.setValidityInDays(companyAsset.getArticleTypeDetails().get(p).getValidity());
							companyAssetinfo.setIssueDate(companyAsset.getArticleTypeDetails().get(p).getIssueDate());
							companyAssetinfo.setIssuedAt(companyAsset.getArticleTypeDetails().get(p).getIssuedAt());
							companyAssetinfo.setIssuedBy(companyAsset.getArticleTypeDetails().get(p).getIssuedBy());
							companyAssetinfo.setValidityUntil(companyAsset.getArticleTypeDetails().get(p).getValidityuntil());
							companyAssetinfo.setStatus(companyAsset.getArticleTypeDetails().get(p).getStatus());

							companyAssetlist.add(companyAssetinfo);
						}}
						
						
					}
					
					form.getSuperTable().getDataprovider().setList(companyAssetlist);
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	public static void initalize()
	{
		CompanyAssetListTable table=new CompanyAssetListTable();
		CompanyAssetListForm form=new CompanyAssetListForm(table);
		
		CompanyAssetListSearchProxy.staticSuperTable=table;
		CompanyAssetListSearchProxy searchPopUp=new CompanyAssetListSearchProxy(false);
		form.setSearchpopupscreen(searchPopUp);
		
		CompanyAssetListPresentor presenter=new CompanyAssetListPresentor(form, new CompanyAssetListArticleInformation());
		form.setToViewState();
		AppMemory.getAppMemory().stickPnel(form);
	}
	
	@Override
	public void reactOnPrint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnEmail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reactOnDownload() {
		
		ArrayList<CompanyAssetListArticleInformation> companyAssetarticlearray=new ArrayList<CompanyAssetListArticleInformation>();
		List<CompanyAssetListArticleInformation> listbill=form.getSuperTable().getListDataProvider().getList();
		
		companyAssetarticlearray.addAll(listbill); 
		
		if(form.getSuperTable().getDataprovider().getList().size()==0)
		{
			form.showDialogMessage("No record in Company Asset list to download");
		}
		
		csvservice.setCompanyAssetList(companyAssetarticlearray, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				// TODO Auto-generated method stub
				String gwt=com.google.gwt.core.client.GWT.getModuleBaseURL();
				final String url=gwt + "csvservlet"+"?type="+117;
				Window.open(url, "test", "enabled");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("RPC call Failed"+caught);

			}
		});
		
	}

	@Override
	protected void reactOnGo() {
		super.reactOnGo();
//		form.getSearchpopupscreen().getQuerry();
		System.out.println("hi vijay =====");
		System.out.println("VIJAY   "+form.getSearchpopupscreen().getQuerry().getFilters().size());
		retrivelist(form.getSearchpopupscreen().getQuerry());
	}

}
