package com.slicktechnologies.client.views.companyassetlist;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.common.CompanyAssetListArticleInformation;
import com.slicktechnologies.shared.common.articletype.ArticleTypeTravel;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;

public class CompanyAssetListTable extends SuperTable<CompanyAssetListArticleInformation> implements ClickHandler{

	TextColumn<CompanyAssetListArticleInformation> getAsstId;
//	TextColumn<CompanyAssetListArticleInformation> getVehicleCategory;
//	TextColumn<CompanyAssetListArticleInformation> getVehicleType;
	TextColumn<CompanyAssetListArticleInformation> getRegistrationNumber;
	TextColumn<CompanyAssetListArticleInformation> getArticleType;
	TextColumn<CompanyAssetListArticleInformation> getArticleNumber;
	TextColumn<CompanyAssetListArticleInformation> getValidityInDays;
	TextColumn<CompanyAssetListArticleInformation> getIssuedDate;
	TextColumn<CompanyAssetListArticleInformation> getIssuedAt;
	TextColumn<CompanyAssetListArticleInformation> getIssuedBy;
	TextColumn<CompanyAssetListArticleInformation> getValidityUntil;
	TextColumn<CompanyAssetListArticleInformation> getStatus;
	Column<CompanyAssetListArticleInformation, String> updateButton;
	
	public final GenricServiceAsync genservice=GWT.create(GenricService.class);
	CompanyAssetUpdatePopUp renew =new CompanyAssetUpdatePopUp();
	
	int rowIndex;
	PopupPanel panel=new PopupPanel(true);
	protected GWTCGlassPanel glassPanel;
	
	public CompanyAssetListTable(){
		super();
		setHeight("1600px");
		renew.getBtnOk().addClickHandler(this);
		renew.getBtnCancel().addClickHandler(this);
	}
	
	@Override
	public void createTable() {

		CreateColumnAssetId();
//		CreateColumnVehicleCategory();
//		CreateColumnVehicleType();
		CreateColumnRegistrationNumber();
		CreateColumnArticleType();
		CreateColumnArticleNumber();
		CreateColumnValidityInDays();
		CreateColumnIssuedDate();
		CreateColumnIssuedAt();
		CreateColumnIssuedBy();
		CreateColumnValidityUntil();
		
		CreateColumnUpdate();
		CreateColumnStatus();
		UpdateColumnRenewButton();
	}
	
	
	

	private void CreateColumnStatus() {
		
		getStatus = new TextColumn<CompanyAssetListArticleInformation>() {
			
			@Override
			public String getValue(CompanyAssetListArticleInformation object) {
				return object.getStatus();
			}
		};
		table.addColumn(getStatus,"Status");
		table.setColumnWidth(getStatus, 90,Unit.PX);
		getStatus.setSortable(true);
	}

	private void UpdateColumnRenewButton() {
		updateButton.setFieldUpdater(new FieldUpdater<CompanyAssetListArticleInformation, String>() {
			
			@Override
			public void update(int index, CompanyAssetListArticleInformation object,String value) {
				System.out.println("Inside update button");
				renew.clear();
				panel=new PopupPanel(true);
				panel.add(renew);
				panel.show();
				panel.center();
				rowIndex=index;
			}
		});
		
	}

	private void CreateColumnUpdate() {
		
		ButtonCell buttoncCell = new ButtonCell();
		updateButton = new Column<CompanyAssetListArticleInformation, String>(buttoncCell) {
			
			@Override
			public String getValue(CompanyAssetListArticleInformation object) {
				return "Renew";
			}
			
			@Override
			public void render(Context context, CompanyAssetListArticleInformation object,SafeHtmlBuilder sb) {
				if(object.getStatus().equals("Active") ){
					super.render(context, object, sb);
				}
			}
		};
		
		
		table.addColumn(updateButton," ");
		table.setColumnWidth(updateButton, 90,Unit.PX);
	}

	private void CreateColumnAssetId() {
		getAsstId = new TextColumn<CompanyAssetListArticleInformation>() {
			
			@Override
			public String getValue(CompanyAssetListArticleInformation object) {
				return object.getAssetId()+"";
			}
		};
		table.addColumn(getAsstId,"Asset Id");
		table.setColumnWidth(getAsstId, 90,Unit.PX);
		getAsstId.setSortable(true);
	}


/**

	private void CreateColumnVehicleCategory() {
		getVehicleCategory = new TextColumn<CompanyAssetListArticleInformation>() {
			
			@Override
			public String getValue(CompanyAssetListArticleInformation object) {
				return object.getVehicleCategory();
			}
		};
		table.addColumn(getVehicleCategory,"Vehicle Category");
		table.setColumnWidth(getVehicleCategory, 90,Unit.PX);
		getVehicleCategory.setSortable(true);
	}




	private void CreateColumnVehicleType() {
		getVehicleType = new TextColumn<CompanyAssetListArticleInformation>() {
			
			@Override
			public String getValue(CompanyAssetListArticleInformation object) {
				return object.getVehicleType();
			}
		};
		table.addColumn(getVehicleType,"Vehicle Type");
		table.setColumnWidth(getVehicleType, 90,Unit.PX);
		getVehicleType.setSortable(true);
	}

**/


	private void CreateColumnRegistrationNumber() {

		getRegistrationNumber = new TextColumn<CompanyAssetListArticleInformation>() {
			
			@Override
			public String getValue(CompanyAssetListArticleInformation object) {
				return object.getRegistrationNo();
			}
		};
		table.addColumn(getRegistrationNumber,"Registration No");
		table.setColumnWidth(getRegistrationNumber, 90,Unit.PX);
		getRegistrationNumber.setSortable(true);
	}




	private void CreateColumnArticleType() {
		getArticleType = new TextColumn<CompanyAssetListArticleInformation>() {
			
			@Override
			public String getValue(CompanyAssetListArticleInformation object) {
				return object.getArticleType();
			}
		};
		table.addColumn(getArticleType,"Article Type");
		table.setColumnWidth(getArticleType, 90,Unit.PX);
		getArticleType.setSortable(true);
	}




	private void CreateColumnArticleNumber() {
		getArticleNumber = new TextColumn<CompanyAssetListArticleInformation>() {
			
			@Override
			public String getValue(CompanyAssetListArticleInformation object) {
				return object.getArticleNumber();
			}
		};
		table.addColumn(getArticleNumber,"Article Number");
		table.setColumnWidth(getArticleNumber, 90,Unit.PX);
		getArticleNumber.setSortable(true);
	}




	private void CreateColumnValidityInDays() {
		getValidityInDays = new TextColumn<CompanyAssetListArticleInformation>() {
			
			@Override
			public String getValue(CompanyAssetListArticleInformation object) {
				return object.getValidityInDays()+"";
			}
		};
		
		table.addColumn(getValidityInDays,"Validity(In days)");
		table.setColumnWidth(getValidityInDays, 90,Unit.PX);
		getValidityInDays.setSortable(true);
	}




	private void CreateColumnIssuedDate() {
		getIssuedDate = new TextColumn<CompanyAssetListArticleInformation>() {
			
			@Override
			public String getValue(CompanyAssetListArticleInformation object) {
				return AppUtility.parseDate(object.getIssueDate());
			}
		};
		table.addColumn(getIssuedDate,"Issued Date");
		table.setColumnWidth(getIssuedDate, 90,Unit.PX);
		getIssuedDate.setSortable(true);
	}




	private void CreateColumnIssuedAt() {

		getIssuedAt = new TextColumn<CompanyAssetListArticleInformation>() {
			
			@Override
			public String getValue(CompanyAssetListArticleInformation object) {
				return object.getIssuedAt();
			}
		};
		table.addColumn(getIssuedAt,"Issued At");
		table.setColumnWidth(getIssuedAt, 90,Unit.PX);
		getIssuedAt.setSortable(true);
	}




	private void CreateColumnIssuedBy() {
		getIssuedBy = new TextColumn<CompanyAssetListArticleInformation>() {
			
			@Override
			public String getValue(CompanyAssetListArticleInformation object) {
				return object.getIssuedBy();
			}
		};
		table.addColumn(getIssuedBy,"Issued By");
		table.setColumnWidth(getIssuedBy, 90,Unit.PX);
		getIssuedBy.setSortable(true);
	}




	private void CreateColumnValidityUntil() {
		getValidityUntil = new TextColumn<CompanyAssetListArticleInformation>() {
			
			@Override
			public String getValue(CompanyAssetListArticleInformation object) {
				return AppUtility.parseDate(object.getValidityUntil());
			}
		};
		table.addColumn(getValidityUntil,"Validity Until");
		table.setColumnWidth(getValidityUntil, 90,Unit.PX);
		getValidityUntil.setSortable(true);
	}


//	public void addColumnSorting() {
//		
//		createColumnSortingAssetId();
//		createColumnSortingVehicleCategory();
//		createColumnSortingVehicleType();
//		createColumnSortingRegistrationNo();
//		createColumnSortingArticleType();
//		createColumnSortingArticleNumber();
//		createColumnSortingValidityInDays();
//		createColumnSortingIssuedDate();
//		createColumnSortingIssuedAt();
//		createColumnSortingIssuedBy();
//		createColumnSortingValidityUntil();
//	}

		@Override
		public void addColumnSorting() {
			super.addColumnSorting();
			createColumnSortingAssetId();
//			createColumnSortingVehicleCategory();
//			createColumnSortingVehicleType();
			createColumnSortingRegistrationNo();
			createColumnSortingArticleType();
			createColumnSortingArticleNumber();
			createColumnSortingValidityInDays();
			createColumnSortingIssuedDate();
			createColumnSortingIssuedAt();
			createColumnSortingIssuedBy();
			createColumnSortingValidityUntil();
		}
	
	
	private void createColumnSortingAssetId() {
		List<CompanyAssetListArticleInformation> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyAssetListArticleInformation>(list);
		Comparator<CompanyAssetListArticleInformation> quote = new Comparator<CompanyAssetListArticleInformation>() {
			@Override
			public int compare(CompanyAssetListArticleInformation e1,CompanyAssetListArticleInformation e2) {
				if (e1 != null && e2 != null) {
					if(e1.getAssetId()== e2.getAssetId()){
					  return 0;}
				  if(e1.getAssetId()> e2.getAssetId()){
					  return 1;}
				  else{
					  return -1;}} else
					return 0;
			}
		};
		columnSort.setComparator(getAsstId, quote);
		table.addColumnSortHandler(columnSort);	
	}




	/**

	private void createColumnSortingVehicleCategory() {
		List<CompanyAssetListArticleInformation> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyAssetListArticleInformation>(list);
		columnSort.setComparator(getVehicleCategory, new Comparator<CompanyAssetListArticleInformation>() {
			@Override
			public int compare(CompanyAssetListArticleInformation e1,CompanyAssetListArticleInformation e2) {
				if(e1 !=null && e2!=null){
					if(e1.getVehicleCategory()!=null && e2.getVehicleCategory()!=null){
						return e1.getVehicleCategory().compareTo(e2.getVehicleCategory());
					}
				}else{
					 return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);		
	}




	private void createColumnSortingVehicleType() {
		List<CompanyAssetListArticleInformation> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyAssetListArticleInformation>(list);
		columnSort.setComparator(getVehicleType, new Comparator<CompanyAssetListArticleInformation>() {
			@Override
			public int compare(CompanyAssetListArticleInformation e1,CompanyAssetListArticleInformation e2) {
				if(e1 !=null && e2!=null){
					if(e1.getVehicleType()!=null && e2.getVehicleType()!=null){
						return e1.getVehicleType().compareTo(e2.getVehicleType());
					}
				}else{
					 return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);		
	}

**/


	private void createColumnSortingRegistrationNo() {
		List<CompanyAssetListArticleInformation> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyAssetListArticleInformation>(list);
		columnSort.setComparator(getRegistrationNumber, new Comparator<CompanyAssetListArticleInformation>() {
			@Override
			public int compare(CompanyAssetListArticleInformation e1,CompanyAssetListArticleInformation e2) {
				if(e1 !=null && e2!=null){
					if(e1.getRegistrationNo()!=null && e2.getRegistrationNo()!=null){
						return e1.getRegistrationNo().compareTo(e2.getRegistrationNo());
					}
				}else{
					 return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);		
	}




	private void createColumnSortingArticleType() {
		List<CompanyAssetListArticleInformation> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyAssetListArticleInformation>(list);
		columnSort.setComparator(getArticleType, new Comparator<CompanyAssetListArticleInformation>() {
			@Override
			public int compare(CompanyAssetListArticleInformation e1,CompanyAssetListArticleInformation e2) {
				if(e1 !=null && e2!=null){
					if(e1.getArticleType()!=null && e2.getArticleType()!=null){
						return e1.getArticleType().compareTo(e2.getArticleType());
					}
				}else{
					 return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);		
	}




	private void createColumnSortingArticleNumber() {

		List<CompanyAssetListArticleInformation> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyAssetListArticleInformation>(list);
		columnSort.setComparator(getArticleNumber, new Comparator<CompanyAssetListArticleInformation>() {
			
			@Override
			public int compare(CompanyAssetListArticleInformation e1,CompanyAssetListArticleInformation e2) {
				if(e1 !=null && e2!=null){
					if(e1.getArticleNumber()!=null && e2.getArticleNumber()!=null){
						return e1.getArticleNumber().compareTo(e2.getArticleNumber());
					}
				}else{
					 return 0;
				}
				return 0;
			}
		});
		
		table.addColumnSortHandler(columnSort);		
	
	
	}




	private void createColumnSortingValidityInDays() {

		List<CompanyAssetListArticleInformation> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyAssetListArticleInformation>(list);
		columnSort.setComparator(getValidityInDays, new Comparator<CompanyAssetListArticleInformation>() {
			
			@Override
			public int compare(CompanyAssetListArticleInformation e1,CompanyAssetListArticleInformation e2) {
				if(e1 !=null && e2!=null){
					if(e1.getValidityInDays()== e2.getValidityInDays()){
						return 0;
					}
					if(e1.getValidityInDays() > e2.getValidityInDays()){
						return 1;
					}else{
						return -1;
					}
				}else{
					 return 0;
				}
			}
		});
		
		table.addColumnSortHandler(columnSort);		
	
	
	}




	private void createColumnSortingIssuedDate() {

		List<CompanyAssetListArticleInformation> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyAssetListArticleInformation>(list);
		columnSort.setComparator(getIssuedDate, new Comparator<CompanyAssetListArticleInformation>() {
			
			@Override
			public int compare(CompanyAssetListArticleInformation e1,CompanyAssetListArticleInformation e2) {
				if(e1 !=null && e2!=null){
					if(e1.getIssueDate()!=null && e2.getIssueDate()!=null){
						return e1.getIssueDate().compareTo(e2.getIssueDate());
					}
				}else{
					 return 0;
				}
				return 0;
			}
		});
		
		table.addColumnSortHandler(columnSort);		
	
	}




	private void createColumnSortingIssuedAt() {
		List<CompanyAssetListArticleInformation> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyAssetListArticleInformation>(list);
		columnSort.setComparator(getIssuedAt, new Comparator<CompanyAssetListArticleInformation>() {
			
			@Override
			public int compare(CompanyAssetListArticleInformation e1,CompanyAssetListArticleInformation e2) {
				if(e1 !=null && e2!=null){
					if(e1.getIssuedAt()!=null && e2.getIssuedAt()!=null){
						return e1.getIssuedAt().compareTo(e2.getIssuedAt());
					}
				}else{
					 return 0;
				}
				return 0;
			}
		});
		
		table.addColumnSortHandler(columnSort);		
	}




	private void createColumnSortingIssuedBy() {
		
		List<CompanyAssetListArticleInformation> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyAssetListArticleInformation>(list);
		columnSort.setComparator(getIssuedBy, new Comparator<CompanyAssetListArticleInformation>() {
			
			@Override
			public int compare(CompanyAssetListArticleInformation e1,CompanyAssetListArticleInformation e2) {
				if(e1 !=null && e2!=null){
					if(e1.getIssuedBy()!=null && e2.getIssuedBy()!=null){
						return e1.getIssuedBy().compareTo(e2.getIssuedBy());
					}
				}else{
					 return 0;
				}
				return 0;
			}
		});
		
		table.addColumnSortHandler(columnSort);
	
	}




	private void createColumnSortingValidityUntil() {
		List<CompanyAssetListArticleInformation> list = getDataprovider().getList();
		columnSort = new ListHandler<CompanyAssetListArticleInformation>(list);
		columnSort.setComparator(getValidityUntil, new Comparator<CompanyAssetListArticleInformation>() {
			
			@Override
			public int compare(CompanyAssetListArticleInformation e1,CompanyAssetListArticleInformation e2) {
				if(e1 !=null && e2!=null){
					if(e1.getValidityUntil()!=null && e2.getValidityUntil()!=null){
						return e1.getValidityUntil().compareTo(e2.getValidityUntil());
					}
				}else{
					 return 0;
				}
				return 0;
			}
		});
		
		table.addColumnSortHandler(columnSort);
	}




	@Override
	protected void initializekeyprovider() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFieldUpdater() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyStyle() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(ClickEvent event) {

		if(event.getSource()==renew.getBtnOk()){
			
			ArrayList<CompanyAssetListArticleInformation> list = new ArrayList<CompanyAssetListArticleInformation>();
			list.addAll(getDataprovider().getList());
			if(renew.validate()){
				System.out.println(" hi vijay inside on click of ok button ");
				
				final String articleType = list.get(rowIndex).getArticleType();
				
				Vector<Filter> filtrvec = new Vector<Filter>();
				Filter filtr = null;
				
				filtr = new Filter();
				filtr.setIntValue(list.get(rowIndex).getAssetId());
				filtr.setQuerryString("count");
				filtrvec.add(filtr);
				
				filtr = new Filter();
				filtr.setLongValue(list.get(rowIndex).getCompanyId());
				filtr.setQuerryString("companyId");
				filtrvec.add(filtr);
				
				MyQuerry querry = new MyQuerry();
				querry.setFilters(filtrvec);
				querry.setQuerryObject(new CompanyAsset());
				
				glassPanel = new GWTCGlassPanel();
				glassPanel.show();
				
				genservice.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						System.out.println("RESULT SIZE =="+result.size());
						if(result.size()!=0){
							for(SuperModel model:result){
								CompanyAsset companyasset = (CompanyAsset) model;
								ArticleTypeTravel travel = new ArticleTypeTravel();
								
								for(int i=0;i<companyasset.getArticleTypeDetails().size();i++){
									
									if(companyasset.getArticleTypeDetails().get(i).getArticleTypeName().equals(articleType) && companyasset.getArticleTypeDetails().get(i).getStatus().equals("Active") ){
										System.out.println("article name accurate");
										companyasset.getArticleTypeDetails().get(i).setStatus("Closed");
										
										travel.setArticleTypeName(companyasset.getArticleTypeDetails().get(i).getArticleTypeName());
										travel.setArticleTypeValue(companyasset.getArticleTypeDetails().get(i).getArticleTypeValue());
										travel.setIssueDate(renew.getIssuedDate().getValue());
										travel.setValidity(renew.getValidityinDays().getValue());
										travel.setIssuedAt(renew.getIssuedAt().getValue());
										travel.setIssuedBy(renew.getIssuedBy().getValue());
										travel.setStatus("Active");
									    Date validityuntil = getValidityUntil(renew.getValidityinDays().getValue(),renew.getIssuedDate().getValue());
										 System.out.println("Validity until oin new entry =="+validityuntil);
										 travel.setValidityuntil(validityuntil);
										 
										 companyasset.getArticleTypeDetails().add(travel);
											
											genservice.save(companyasset, new AsyncCallback<ReturnFromServer>() {
												
												@Override
												public void onSuccess(ReturnFromServer result) {
													// TODO Auto-generated method stub
													System.out.println(" hi vijay updated");
													
													
													ArrayList<CompanyAssetListArticleInformation> list = new ArrayList<CompanyAssetListArticleInformation>();
													list.addAll(getDataprovider().getList());
													
													list.get(rowIndex).setStatus("Closed");
													getDataprovider().getList().clear();
													getDataprovider().getList().addAll(list);
													glassPanel.hide();
												}
												
												@Override
												public void onFailure(Throwable caught) {
													// TODO Auto-generated method stub
													
												}
											});
											
										 break;
									}
								}
							}
						}
						glassPanel.hide();

					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				});
			}
			panel.hide();
			System.out.println("Vijay asset IDD value === "+list.get(rowIndex).getAssetId());
		}
		
		if(event.getSource()==renew.getBtnCancel()){
			panel.hide();
		}
	}

	protected Date getValidityUntil(Integer value, Date issuedate) {
		CalendarUtil.addDaysToDate(issuedate, value);
		System.out.println("Expiry Date====" + issuedate);
		return issuedate;
	}

}
