package com.slicktechnologies.client.views.generalpopup;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ProcessLevelBar;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.approvalutility.ManageApprovals;
import com.slicktechnologies.client.config.adress.city.CityForm;
import com.slicktechnologies.client.config.adress.city.CityPresenter;
import com.slicktechnologies.client.config.adress.city.CityTable;
import com.slicktechnologies.client.config.adress.locality.LocalityForm;
import com.slicktechnologies.client.config.adress.locality.LocalityPresenter;
import com.slicktechnologies.client.config.adress.locality.LocalityTable;
import com.slicktechnologies.client.config.configurations.ConfigForm;
import com.slicktechnologies.client.config.configurations.ConfigPresenter;
import com.slicktechnologies.client.config.configurations.ConfigTabels;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.reusabledata.CopyDataConfig;
import com.slicktechnologies.client.services.CustomerProjectService;
import com.slicktechnologies.client.services.CustomerProjectServiceAsync;
import com.slicktechnologies.client.userauthorization.UserAuthorizationForm;
import com.slicktechnologies.client.userauthorization.UserAuthorizationPresenter;
import com.slicktechnologies.client.userauthorization.UserAuthorizeTable;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConnector;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.MultipleExpensemanagmentForm;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.MultipleExpensemanagmentPresenter;
import com.slicktechnologies.client.views.cnc.CNCForm;
import com.slicktechnologies.client.views.cnc.CNCPresenter;
import com.slicktechnologies.client.views.complain.ComplainForm;
import com.slicktechnologies.client.views.complain.ComplainPresenter;
import com.slicktechnologies.client.views.contactpersonidentification.contactpersondetails.ContactPersonForm;
import com.slicktechnologies.client.views.contactpersonidentification.contactpersondetails.ContactPersonPresenter;
import com.slicktechnologies.client.views.contactpersonidentification.contactpersondetails.ContactPersonPresenterTableProxy;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.contract.ContractService;
import com.slicktechnologies.client.views.contract.ContractServiceAsync;
import com.slicktechnologies.client.views.contract.SalesLineItemTable;
import com.slicktechnologies.client.views.contractrenewal.ContractRenewalForm;
import com.slicktechnologies.client.views.contractrenewal.ContractRenewalPresenter;
import com.slicktechnologies.client.views.customer.CustomerForm;
import com.slicktechnologies.client.views.customer.CustomerPresenter;
import com.slicktechnologies.client.views.customerbranchdetails.CustomerBranchForm;
import com.slicktechnologies.client.views.customerbranchdetails.CustomerBranchPresenter;
import com.slicktechnologies.client.views.customerbranchdetails.CustomerBranchTable;
import com.slicktechnologies.client.views.customerbranchdetails.CustomerBranchPresenter.CustomerBranchPresenterTable;
import com.slicktechnologies.client.views.customerbranchdetails.servicelocation.ServiceLocationForm;
import com.slicktechnologies.client.views.customerbranchdetails.servicelocation.ServiceLocationPresenter;
import com.slicktechnologies.client.views.customerbranchdetails.servicelocation.ServiceLocationTable;
import com.slicktechnologies.client.views.deliverynote.DeliveryNoteForm;
import com.slicktechnologies.client.views.deliverynote.DeliveryNotePresenter;
import com.slicktechnologies.client.views.device.DeviceForm;
import com.slicktechnologies.client.views.device.DevicePresenter;
import com.slicktechnologies.client.views.fumigationAustralia.FumigationAustraliaForm;
import com.slicktechnologies.client.views.fumigationAustralia.FumigationAustraliaPresenter;
import com.slicktechnologies.client.views.fumigationdetails.FumigationALPForm;
import com.slicktechnologies.client.views.fumigationdetails.FumigationALPPresenter;
import com.slicktechnologies.client.views.fumigationdetails.FumigationForm;
import com.slicktechnologies.client.views.fumigationdetails.FumigationPresenter;
import com.slicktechnologies.client.views.humanresource.employeeadditionaldetails.EmployeeAdditionalDetailsForm;
import com.slicktechnologies.client.views.humanresource.employeeadditionaldetails.EmployeeAdditionalDetailsPresenter;
import com.slicktechnologies.client.views.inventory.grnlist.GRNListForm;
import com.slicktechnologies.client.views.inventory.grnlist.GRNListPresenter;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNForm;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNPresenter;
import com.slicktechnologies.client.views.inventory.recievingnote.GRNSearchTable;
import com.slicktechnologies.client.views.lead.LeadForm;
import com.slicktechnologies.client.views.lead.LeadPresenter;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListForm;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListPresenter;
import com.slicktechnologies.client.views.paymentinfo.billinglist.BillingListTable;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.invoicelist.InvoiceListForm;
import com.slicktechnologies.client.views.paymentinfo.invoicelist.InvoiceListPresenter;
import com.slicktechnologies.client.views.paymentinfo.invoicelist.InvoiceListTable;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.paymentdetails.PaymentDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.paymentlist.PaymentListForm;
import com.slicktechnologies.client.views.paymentinfo.paymentlist.PaymentListPresenter;
import com.slicktechnologies.client.views.paymentinfo.paymentlist.PaymentListTable;
import com.slicktechnologies.client.views.paymentinfo.vendorinvoicedetails.VendorInvoiceDetailsForm;
import com.slicktechnologies.client.views.paymentinfo.vendorinvoicedetails.VendorInvoiceDetailsPresenter;
import com.slicktechnologies.client.views.paymentinfo.vendorinvoicelist.VendorInvoiceListPresenter;
import com.slicktechnologies.client.views.paymentmodes.companypayment.CompanyPaymentForm;
import com.slicktechnologies.client.views.paymentmodes.companypayment.CompanyPaymentPresenter;
import com.slicktechnologies.client.views.popups.SingleDropDownPopup;
import com.slicktechnologies.client.views.products.itemproduct.ItemproductForm;
import com.slicktechnologies.client.views.products.itemproduct.ItemproductPresenter;
import com.slicktechnologies.client.views.products.serviceproduct.ServiceproductForm;
import com.slicktechnologies.client.views.products.serviceproduct.ServiceproductPresenter;
import com.slicktechnologies.client.views.project.concproject.ProjectForm;
import com.slicktechnologies.client.views.project.concproject.ProjectPresenter;
import com.slicktechnologies.client.views.project.concproject.ProjectPresenterTableProxy;
import com.slicktechnologies.client.views.project.concproject.ProjectPresenter.ProjectPresenterTable;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderForm;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderPresenter;
import com.slicktechnologies.client.views.purchase.purchaserequisition.PurchaseRequisitionForm;
import com.slicktechnologies.client.views.purchase.purchaserequisition.PurchaseRequitionPresenter;
import com.slicktechnologies.client.views.purchase.quickpurchase.ViewInvoicePaymentGRNPopup;
import com.slicktechnologies.client.views.purchase.servicepo.ServicePoForm;
import com.slicktechnologies.client.views.purchase.servicepo.ServicePoPresenter;
import com.slicktechnologies.client.views.quickcontract.QuickContractForm;
import com.slicktechnologies.client.views.quickcontract.QuickContractPresentor;
import com.slicktechnologies.client.views.quickcontract.ViewInvoicePaymentServicePopUp;
import com.slicktechnologies.client.views.quicksalesorder.QuickSalesOrderForm;
import com.slicktechnologies.client.views.quicksalesorder.QuickSalesOrderPresenter;
import com.slicktechnologies.client.views.quicksalesorder.ViewInvoicePaymentDeliveryNotePopup;
import com.slicktechnologies.client.views.quotation.QuotationForm;
import com.slicktechnologies.client.views.quotation.QuotationPresenter;
import com.slicktechnologies.client.views.salesorder.SalesOrderForm;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenterSearchProxy;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenterTableProxy;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter.SalesOrderPresenterSearch;
import com.slicktechnologies.client.views.salesorder.SalesOrderPresenter.SalesOrderPresenterTable;
import com.slicktechnologies.client.views.salesquotation.SalesQuotationForm;
import com.slicktechnologies.client.views.salesquotation.SalesQuotationPresenter;
import com.slicktechnologies.client.views.service.ServiceForm;
import com.slicktechnologies.client.views.service.ServicePresenter;
import com.slicktechnologies.client.views.service.ServiceSchedulePopup;
import com.slicktechnologies.client.views.service.ServiceSearchPopUp;
import com.slicktechnologies.client.views.service.ServiceTable;
import com.slicktechnologies.client.views.settings.branch.BranchForm;
import com.slicktechnologies.client.views.settings.branch.BranchPresenter;
import com.slicktechnologies.client.views.settings.branch.BranchPresenterTableProxy;
import com.slicktechnologies.client.views.settings.company.CompanyForm;
import com.slicktechnologies.client.views.settings.company.CompanyPresenter;
import com.slicktechnologies.client.views.settings.cronjobconfig.CronJobConfigrationForm;
import com.slicktechnologies.client.views.settings.cronjobconfig.CronJobConfigrationPresenter;
import com.slicktechnologies.client.views.settings.cronjobconfig.CronJobConfigrationTable;
import com.slicktechnologies.client.views.settings.employee.QuickEmployeeForm;
import com.slicktechnologies.client.views.settings.employee.QuickEmployeePresenter;
import com.slicktechnologies.client.views.settings.employee.QuickEmployeePresenter.QuickEmployeePresenterTable;
import com.slicktechnologies.client.views.settings.user.UserForm;
import com.slicktechnologies.client.views.settings.user.UserPresenter;
import com.slicktechnologies.client.views.settings.user.UserPresenterTableProxy;
import com.slicktechnologies.client.views.technicianwarehousedetails.TechnicianServiceSchedulePopup;
import com.slicktechnologies.client.views.workorder.WorkOrderForm;
import com.slicktechnologies.client.views.workorder.WorkOrderPresenter;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Device;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.ApprovableProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigration;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchServiceLocation;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.role.UserRole;
import com.slicktechnologies.shared.common.salesorder.OtherChargesMargin;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesorder.VendorMargin;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
import com.slicktechnologies.shared.common.workorder.WorkOrder;

/**
 * Here i am showing popup with data based on setModel () method.
 * created by vijay chougule
 * @author Vijay  Chougule
 *
 */

public class GeneralViewDocumentPopup extends PopupEntityPresenter {
	
	protected GWTCGlassPanel glassPanel;
	
	//removed static keyword from popuppanel by ashwini
	public PopupPanel viewDocumentPanel = null;			//Updated By: Viraj Date:19-03-2019 Description: To access it throught project 
	boolean newmodeFlag = false;
	boolean summaryBillingPopupFlag= false; //Updated By: Ashwini Patil Date:22-12-2021 Description: To set processlevelBarNames for BillingDetails form in summary popup
	public boolean hideNavigationFlag=false;//Ashwini Patil Date:20-09-2022
	
	SalesOrder salesOrderModel=null;
	
	
	//Date: 10-1-2022 Added by: Ashwini Patil Description: following Dialog required for Create AMC navigate screen
	ConditionDialogBox conditionPopupforCreateAMC=new ConditionDialogBox("AMC has been already created for this Sales Order Do you want to create annother one",AppConstants.YES,AppConstants.NO);
	PopupPanel panel;
	
	final GenricServiceAsync genasync=GWT.create(GenricService.class);
	
	/**
	 * @author Anil
	 * @since 09-02-2022
	 * This field is used to map value to composite like if new customer is created at view state of any screen
	 * then we will not map it composite
	 */
	public ScreeenState screenState=null;
	
	public GeneralViewDocumentPopup(){
		super();
		
		glassPanel = new GWTCGlassPanel();
		
		viewDocumentPanel = new PopupPanel();
		viewDocumentPanel.add(docVerticalPanel);
		conditionPopupforCreateAMC.getBtnOne().addClickHandler(this);
		conditionPopupforCreateAMC.getBtnTwo().addClickHandler(this);
	}

	public GeneralViewDocumentPopup(boolean flag){
		super();
		
		glassPanel = new GWTCGlassPanel();
		
		viewDocumentPanel = new PopupPanel();
		viewDocumentPanel.add(docVerticalPanel);
		newmodeFlag = flag;
		
	}
	
	/**
	 * 
	 * @author Ashwini
	 * @since 22-12-21
	 * flag2=true parameter indicates popup is created from summary screen.
	 * This flag is used to setSummaryPopUpFLag further.
	 * SummaryPopUpFLag is used to changeProcessLevel() menus for summary popups.
	 */
	public GeneralViewDocumentPopup(boolean flag1,boolean flag2,boolean flag3){
		super();
		
		glassPanel = new GWTCGlassPanel();
		
		viewDocumentPanel = new PopupPanel(false,true);//added parameters by ashwini on 17-10-2022
		viewDocumentPanel.add(docVerticalPanel);
		newmodeFlag = flag1;
		summaryBillingPopupFlag=flag2;
		hideNavigationFlag=flag3;
		Console.log("In GeneralViewDocumentPopup constructor. summaryBillingPopupFlag="+summaryBillingPopupFlag);
		Console.log("In GeneralViewDocumentPopup constructor. hideNavigationFlag="+hideNavigationFlag);
	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);
		Console.log("in onclick of GeneralViewDocumentPopup");
		if(event.getSource() == btnClose){
			Console.log("inside event.getSource() == btnClose");
			viewDocumentPanel.hide();
			Console.log("after viewDocumentPanel.hide()");
			if(ViewInvoicePaymentDeliveryNotePopup.generalPanel!=null){
				ViewInvoicePaymentDeliveryNotePopup.generalPanel.hide();
				ViewInvoicePaymentDeliveryNotePopup.tabBillingTable.getTable().redraw();
				ViewInvoicePaymentDeliveryNotePopup.tabInvoiceTable.getTable().redraw();
				ViewInvoicePaymentDeliveryNotePopup.tabPaymentTable.getTable().redraw();
				ViewInvoicePaymentDeliveryNotePopup.tabDeliveryNoteTable.getTable().redraw();
			}
			//Date 14-08-2017 added by vijay for quick contract
			if(ViewInvoicePaymentServicePopUp.generalPanel!=null){
				ViewInvoicePaymentServicePopUp.generalPanel.hide();
				ViewInvoicePaymentServicePopUp.tabBillingTable.getTable().redraw();
				ViewInvoicePaymentServicePopUp.tabInvoiceTable.getTable().redraw();
				ViewInvoicePaymentServicePopUp.tabPaymentTable.getTable().redraw();
				ViewInvoicePaymentServicePopUp.tabServiceTable.getTable().redraw();
				}
			//Date 20-08-2017 added by vijay for quick purchase
			if(ViewInvoicePaymentGRNPopup.generalPanel!=null){
				ViewInvoicePaymentGRNPopup.generalPanel.hide(); 
				ViewInvoicePaymentGRNPopup.tabBillingTable.getTable().redraw();
				ViewInvoicePaymentGRNPopup.tabInvoiceTable.getTable().redraw();
				ViewInvoicePaymentGRNPopup.tabPaymentTable.getTable().redraw();
				ViewInvoicePaymentGRNPopup.tabGRNTable.getTable().redraw();
			}
		}
		/**
		 * @author Ashwini
		 * @since 10-01-2022
		 * code to handle conditionpopup for Create AMC
		 */
		if(event.getSource().equals(conditionPopupforCreateAMC.getBtnOne())){
			Console.log("in if(event.getSource().equals(conditionPopupforCreateAMC.getBtnOne()))");
			panel.hide();
			reactOnCreateAMC(salesOrderModel);				
		}
		if(event.getSource().equals(conditionPopupforCreateAMC.getBtnTwo())){
			Console.log("in if(event.getSource().equals(conditionPopupforCreateAMC.getBtnTwo()))");
			panel.hide();
		}
		
	}

	/**
	 * this method is used to load which form based on parameter 
	 * @param presenterName
	 * @param model
	 */

	public void setModel(String presenterName, final SuperModel model) {
		Console.log("In setModel presenterName="+presenterName+" / "+newmodeFlag);
		Console.log("setModel - summaryBillingPopupFlag="+summaryBillingPopupFlag );
		Console.log("hideNavigationFlag ="+hideNavigationFlag);
		removePopObjects();
		docVerticalPanel.add(btnClose);
		docVerticalPanel.add(appLevelMenuPanel);
		
		if(!newmodeFlag){
			Console.log("in if (!newmodeFlag)");
			
		//By Ashwini Patil Device, WorkOrder, Purchase Order, CUSTOMERSERVICELIST
		if(presenterName.equalsIgnoreCase("Device")){

			
			glassPanel.show();
			
			final DeviceForm form = new DeviceForm();
			
			/**
			 * @author Anil
			 * @since 07-01-2021
			 * Showing process level menus
			 */
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
			}
			docVerticalPanel.add(form.content);
			
			Timer timer = new Timer() {
				@Override
				public void run() {

					form.content.getElement().getStyle().setHeight(570, Unit.PX);
					form.content.getElement().getStyle().setWidth(950, Unit.PX);
					
					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					Service device = (Service) model;
					form.updateView(device);
					
					DevicePresenter dPresenter=new DevicePresenter(form,new Service());
					presenter = dPresenter;
					presenter.setModel(device);
					
					form.setPopUpAppMenubar(true);
					form.setToNewState();										
				}
			};
			timer.schedule(1000);
			glassPanel.hide();
			
		
		}
		else if(presenterName.equalsIgnoreCase(AppConstants.WORKORDER)){
			
			glassPanel.show();
			
			final WorkOrderForm form = new WorkOrderForm();
			
			/**
			 * @author Anil
			 * @since 07-01-2021
			 * Showing process level menus
			 */
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
			}
			docVerticalPanel.add(form.content);
			
			Timer timer = new Timer() {
				@Override
				public void run() {

					form.content.getElement().getStyle().setHeight(570, Unit.PX);
					form.content.getElement().getStyle().setWidth(950, Unit.PX);
					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					WorkOrder workOrder = (WorkOrder) model;
					form.updateView(workOrder);
					
					WorkOrderPresenter woPresenter=new WorkOrderPresenter(form,new WorkOrder());
					presenter = woPresenter;
					presenter.setModel(workOrder);
					
					form.setPopUpAppMenubar(true);
					form.setToNewState();										
				}
			};
			timer.schedule(1000);
			glassPanel.hide();
			
		}else if(presenterName.equalsIgnoreCase(AppConstants.PURCHASEORDER)){
			glassPanel.show();
			
			final PurchaseOrderForm form = new PurchaseOrderForm();
			
			/**
			 * @author Anil
			 * @since 07-01-2021
			 * Showing process level menus
			 */
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
			}
			docVerticalPanel.add(form.content);
			
			Timer timer = new Timer() {
				@Override
				public void run() {

					form.content.getElement().getStyle().setHeight(570, Unit.PX);
					form.content.getElement().getStyle().setWidth(950, Unit.PX);
					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					PurchaseOrder purchaseOrder = (PurchaseOrder) model;
					form.updateView(purchaseOrder);
					
					PurchaseOrderPresenter poPresenter=new PurchaseOrderPresenter(form,new PurchaseOrder());
					presenter = poPresenter;
					presenter.setModel(purchaseOrder);
					
					form.setPopUpAppMenubar(true);
					form.setToNewState();
					
				}
			};
			timer.schedule(1000);	
			glassPanel.hide();
		}else if(presenterName.equalsIgnoreCase(AppConstants.CUSTOMERSERVICELIST)){
			glassPanel.show();
			ServiceTable table=new ServiceTable();
			final ServiceForm form=new ServiceForm(table);
			/**
			 * @author Anil
			 * @since 07-01-2021
			 * Showing process level menus
			 */
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
			}
			docVerticalPanel.add(form.content);
			Timer timer = new Timer() {
				@Override
				public void run() {

					form.content.getElement().getStyle().setHeight(570, Unit.PX);
					form.content.getElement().getStyle().setWidth(950, Unit.PX);

				
					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					Contract serviceEntity = (Contract) model;
//					form.updateView(serviceEntity);
					
					Filter filter = new Filter();
					filter.setIntValue(model.getCount());
					filter.setQuerryString("contractCount");
					Vector<Filter> vecfilter = new Vector<Filter>();
					vecfilter.add(filter);
					MyQuerry query = new MyQuerry(vecfilter, new Service());
					
					ServicePresenter invoicePresenter=new ServicePresenter(form,new Service(),query);
//					presenter = invoicePresenter;
//					presenter.setModel(serviceEntity);
					
					form.setPopUpAppMenubar(true);

					form.setToViewState();
					
					glassPanel.hide();
				}
			};
			timer.schedule(1000);
			
			
		}else if(presenterName.equalsIgnoreCase(AppConstants.PROCESSCONFIGINV)){
			final InvoiceDetailsForm form = new InvoiceDetailsForm();
			/**
			 * @author Anil
			 * @since 07-01-2021
			 * Showing process level menus
			 */
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
			}
			docVerticalPanel.add(form.content);

			try {
				form.hideWaitSymbol();
			} catch (Exception e) {
			}
			
			glassPanel.show();

			Timer timer = new Timer() {
				@Override
				public void run() {

					form.content.getElement().getStyle().setHeight(570, Unit.PX);
					form.content.getElement().getStyle().setWidth(950, Unit.PX);

					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					Invoice invoiceEntity = (Invoice) model;
					form.updateView(invoiceEntity);
					
					InvoiceDetailsPresenter invoicePresenter=new InvoiceDetailsPresenter(form,new Invoice());
					presenter = invoicePresenter;
					presenter.setModel(invoiceEntity);
					
					form.setPopUpAppMenubar(true);
					form.setSummaryPopupFlag(summaryBillingPopupFlag); //Ashwini Patil
					form.setHideNavigationButtons(hideNavigationFlag);//Ashwini Patil Date:21-09-2022
					form.setToViewState();
					
					glassPanel.hide();
				}
			};
			timer.schedule(2000);
			
		}
		
		else if(presenterName.equalsIgnoreCase(AppConstants.BILLINGDETAILS)){
				glassPanel.show();
				
				final BillingDetailsForm form = new BillingDetailsForm();
				
				/**
				 * @author Anil
				 * @since 07-01-2021
				 * Showing process level menus
				 */
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
				}
				docVerticalPanel.add(form.content);
				
				Timer timer = new Timer() {
					@Override
					public void run() {

						form.content.getElement().getStyle().setHeight(570, Unit.PX);
						form.content.getElement().getStyle().setWidth(950, Unit.PX);
						viewDocumentPanel.setHeight("600px");
						viewDocumentPanel.show();
						viewDocumentPanel.center();
						
						BillingDocument billingEntity = (BillingDocument) model;
						form.updateView(billingEntity);
						
						BillingDetailsPresenter invoicePresenter=new BillingDetailsPresenter(form,new BillingDocument());
						presenter = invoicePresenter;
						presenter.setModel(billingEntity);
						
						form.setPopUpAppMenubar(true);
						form.setSummaryPopupFlag(summaryBillingPopupFlag); //Ashwini Patil
						Console.log("SummaryPopupFlag set to BillingForm");
						form.setHideNavigationButtons(hideNavigationFlag);//Ashwini Patil Date:21-09-2022
						form.setToViewState();
						
						glassPanel.hide();
					}
				};
				timer.schedule(1000);
		}
		else if(presenterName.equalsIgnoreCase(AppConstants.PAYMENT)){
			glassPanel.show();
			final PaymentDetailsForm form = new PaymentDetailsForm();
			
			/**
			 * @author Anil
			 * @since 07-01-2021
			 * Showing process level menus
			 */
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
			}
			
			docVerticalPanel.add(form.content);
			Timer timer = new Timer() {
				@Override
				public void run() {

					form.content.getElement().getStyle().setHeight(570, Unit.PX);
					form.content.getElement().getStyle().setWidth(950, Unit.PX);

					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					CustomerPayment paymentEntity = (CustomerPayment) model;
					form.updateView(paymentEntity);
					
					PaymentDetailsPresenter invoicePresenter=new PaymentDetailsPresenter(form,new CustomerPayment());
					presenter = invoicePresenter;
					presenter.setModel(paymentEntity);
					

					form.setPopUpAppMenubar(true);
					form.setSummaryPopupFlag(summaryBillingPopupFlag);//Ashwini Patil
					form.setHideNavigationButtons(hideNavigationFlag);//Ashwini Patil Date:21-09-2022
					form.setToViewState();
					glassPanel.hide();
					
				}
			};
			timer.schedule(1000);
		}
		
		else if(presenterName.equalsIgnoreCase(AppConstants.DELIVERYNOTE)){
			Console.log("inside set model delivery note");
			glassPanel.show();
			
			final DeliveryNoteForm form = new DeliveryNoteForm();
			/**
			 * @author Anil
			 * @since 07-01-2021
			 * Showing process level menus
			 */
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
			}
			docVerticalPanel.add(form.content);
			
			Timer timer = new Timer() {
				@Override
				public void run() {

					form.content.getElement().getStyle().setHeight(570, Unit.PX);
					form.content.getElement().getStyle().setWidth(950, Unit.PX);

					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					
					
					
					DeliveryNotePresenter invoicePresenter=new DeliveryNotePresenter(form,new DeliveryNote());
					presenter = invoicePresenter;
					
					if(model!=null){	
						DeliveryNote deliveryNoteEntity = (DeliveryNote) model;
						form.updateView(deliveryNoteEntity);
						presenter.setModel(deliveryNoteEntity);
						form.setPopUpAppMenubar(true);
						form.setHideNavigationButtons(hideNavigationFlag);//Ashwini Patil Date:21-09-2022
						form.setToViewState();
						Console.log("setting up view state");
					}else{
						Console.log("setting up new state");
						form.setPopUpAppMenubar(true);
						form.setHideNavigationButtons(hideNavigationFlag);//Ashwini Patil Date:21-09-2022
						form.setToNewState();
					}
									
					
				}
			};
			timer.schedule(1000);
			glassPanel.hide();
		}
		
		else if(presenterName.equalsIgnoreCase(AppConstants.CUSTOMERSERVICE)){
			glassPanel.show();
			
			final DeviceForm form = new DeviceForm();
			/**
			 * @author Anil
			 * @since 07-01-2021
			 * Showing process level menus
			 */
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
			}
			docVerticalPanel.add(form.content);
			
			Timer timer = new Timer() {
				@Override
				public void run() {

					form.content.getElement().getStyle().setHeight(770, Unit.PX);//570
					form.content.getElement().getStyle().setWidth(950, Unit.PX);

					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					Service serviceEntity = (Service) model;
					form.updateView(serviceEntity);
					
					DevicePresenter devicePresenter=new DevicePresenter(form,new Service());
					presenter = devicePresenter;
					presenter.setModel(serviceEntity);
					
					form.setPopUpAppMenubar(true);

					form.setToViewState();
					/**Date : 15/3/2018 Manisha
					 * Hide New and Manage Customer Project button from Service screen!!!
					 */
					
					for(int i=0;i<form.getProcesslevelBarNames().length;i++)
					{
						InlineLabel label=form.getProcessLevelBar().btnLabels[i];
						String text=label.getText().trim();
						
						if(text.equals("New")){
						label.setVisible(false);
						}
						if(text.equals("Manage Customer Project")){
							label.setVisible(false);
						}
						//By:Ashwini Patil Date:8-04-2022 Description: when we go through customer service list to Billing details then on billing details screen we are getting bunch of wrong options so we are hiding that option in popup
						if(text.equals(AppConstants.VIEWBILL)){
							label.setVisible(false);
							Console.log("view bill hidden by GeneralPopup");
						}						
					}
					/**End**/
					
					glassPanel.hide();
				}
			};
			timer.schedule(1000);
		}
		else if(presenterName.equalsIgnoreCase(AppConstants.GRN)){
			glassPanel.show();
			
			final GRNForm form = new GRNForm();
			/**
			 * @author Anil
			 * @since 07-01-2021
			 * Showing process level menus
			 */
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
			}
			docVerticalPanel.add(form.content);
			
			Timer timer = new Timer() {
				@Override
				public void run() {

					form.content.getElement().getStyle().setHeight(570, Unit.PX);
					form.content.getElement().getStyle().setWidth(950, Unit.PX);

					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					GRN grnEntity = (GRN) model;
					
					GRNPresenter grnPresenter=new GRNPresenter(form,new GRN());
					presenter = grnPresenter;
					presenter.setModel(grnEntity);
					
					form.updateView(grnEntity);
					
					form.setPopUpAppMenubar(true);

					form.setToViewState();
					
					glassPanel.hide();
				}
			};
			timer.schedule(1000);
		}else if(presenterName.equalsIgnoreCase(AppConstants.FUMIGATION)){
			/** date 5.4.2018 added by komal for fumigayion screen**/
		//	glassPanel.show();
			
			final FumigationForm form = new FumigationForm();
			docVerticalPanel.add(form.content);
			
			Timer timer = new Timer() {
				@Override
				public void run() {

					form.content.getElement().getStyle().setHeight(570, Unit.PX);
					form.content.getElement().getStyle().setWidth(950, Unit.PX);

					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					Fumigation fumigationEntity = (Fumigation) model;
					
					FumigationPresenter fumigationPresenter=new FumigationPresenter(form,new Fumigation());
					presenter = fumigationPresenter;
					presenter.setModel(fumigationEntity);
					form.updateView(fumigationEntity);
					form.setPopUpAppMenubar(true);

					form.setToEditState();			
					//glassPanel.hide();
				}
			};
			timer.schedule(1000);
		}else if(presenterName.equalsIgnoreCase(AppConstants.FUMIGTAIONALP)){
			/** date 5.4.2018 added by komal for fumigayion screen**/
			//glassPanel.show();
			
			final FumigationALPForm form = new FumigationALPForm();
			docVerticalPanel.add(form.content);
			
			Timer timer = new Timer() {
				@Override
				public void run() {

					form.content.getElement().getStyle().setHeight(570, Unit.PX);
					form.content.getElement().getStyle().setWidth(950, Unit.PX);

					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					Fumigation fumigationEntity = (Fumigation) model;
					
					
					FumigationALPPresenter fumigationPresenter=new FumigationALPPresenter(form,new Fumigation());
					presenter = fumigationPresenter;
					presenter.setModel(fumigationEntity);
					form.updateView(fumigationEntity);
					
					form.setPopUpAppMenubar(true);

					form.setToEditState();	
					
				//	glassPanel.hide();
				}
			};
			timer.schedule(1000);
		}
		else if(presenterName.equalsIgnoreCase(AppConstants.FUMIGATIONAUS)){
			/** date 5.4.2018 added by komal for fumigayion (Aus) screen**/
			//glassPanel.show();
			
			final FumigationAustraliaForm form = new FumigationAustraliaForm();
			docVerticalPanel.add(form.content);
			
			Timer timer = new Timer() {
				@Override
				public void run() {

					form.content.getElement().getStyle().setHeight(570, Unit.PX);
					form.content.getElement().getStyle().setWidth(950, Unit.PX);

					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					Fumigation fumigationEntity = (Fumigation) model;
					
					
					FumigationAustraliaPresenter fumigationAustraliaPresenter=new FumigationAustraliaPresenter(form,new Fumigation());
					presenter = fumigationAustraliaPresenter;
					presenter.setModel(fumigationEntity);
					form.updateView(fumigationEntity);
					
					form.setPopUpAppMenubar(true);

					form.setToEditState();	
					
				//	glassPanel.hide();
				}
			};
			timer.schedule(1000);
		}else if(presenterName.equalsIgnoreCase(AppConstants.PROCESSCONFIGINVVENDOR)){
			/** date 27.06.2018 added by komal for vendor invoice**/
			glassPanel.show();
			final VendorInvoiceDetailsForm form = new VendorInvoiceDetailsForm();
			/**
			 * @author Anil
			 * @since 07-01-2021
			 * Showing process level menus
			 */
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
			}
			docVerticalPanel.add(form.content);
			Timer timer = new Timer() {
				@Override
				public void run() {

					form.content.getElement().getStyle().setHeight(570, Unit.PX);
					form.content.getElement().getStyle().setWidth(950, Unit.PX);

					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					VendorInvoice invoiceEntity = (VendorInvoice) model;
					form.updateView(invoiceEntity);
					
					VendorInvoiceDetailsPresenter invoicePresenter=new VendorInvoiceDetailsPresenter(form,new VendorInvoice());
					presenter = invoicePresenter;
					presenter.setModel(invoiceEntity);
					
					form.setPopUpAppMenubar(true);

					form.setToViewState();
					
					glassPanel.hide();
				}
			};
			timer.schedule(1000);
			
		}else if(presenterName.equalsIgnoreCase(AppConstants.PROCESSCONFIGCPVENDOR)){
			/** date 27.06.2018 added by komal for vendor payment**/
			glassPanel.show();
			final PaymentDetailsForm form = new PaymentDetailsForm();
			/**
			 * @author Anil
			 * @since 07-01-2021
			 * Showing process level menus
			 */
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
			}
			docVerticalPanel.add(form.content);
			Timer timer = new Timer() {
				@Override
				public void run() {

					form.content.getElement().getStyle().setHeight(570, Unit.PX);
					form.content.getElement().getStyle().setWidth(950, Unit.PX);

					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					CustomerPayment paymentEntity = (CustomerPayment) model;
					form.updateView(paymentEntity);
					
					PaymentDetailsPresenter invoicePresenter=new PaymentDetailsPresenter(form,new CustomerPayment());
					presenter = invoicePresenter;
					presenter.setModel(paymentEntity);

					form.setPopUpAppMenubar(true);
					form.setToViewState();
					glassPanel.hide();
					
				}
			};
			timer.schedule(1000);
		}
		/********** Date 01-02-2019 by vijay for NBHC Inventory Management allow to edit PR For Revising PR  *****/
		else if(presenterName.equalsIgnoreCase(AppConstants.PURCHASEREQUISION)){
			glassPanel.show();
			
			final PurchaseRequisitionForm form = new PurchaseRequisitionForm();
			/**
			 * @author Anil
			 * @since 07-01-2021
			 * Showing process level menus
			 */
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
			}
			docVerticalPanel.add(form.content);
			
			Timer timer = new Timer() {
				@Override
				public void run() {

					form.content.getElement().getStyle().setHeight(570, Unit.PX);
					form.content.getElement().getStyle().setWidth(950, Unit.PX);

					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					PurchaseRequisition purchaseReq = (PurchaseRequisition) model;
					
					PurchaseRequitionPresenter purchaseReqPresenter=new PurchaseRequitionPresenter(form,new PurchaseRequisition());
					presenter = purchaseReqPresenter;
					presenter.setModel(purchaseReq);
					
					form.updateView(purchaseReq);
					
					form.setPopUpAppMenubar(true);

					form.setToViewState();
					
					form.getProcessLevelBar().setVisibleFalse(false);
					
					glassPanel.hide();
				}
			};
			timer.schedule(1000);
		}
		/**
		 * Updated by: Viraj
		 * Date: 16-03-2019
		 * Description: To allowing edit of lead
		 */
		else if(presenterName.equalsIgnoreCase(AppConstants.LEAD)){
			glassPanel.show();
			
			final LeadForm form = new LeadForm();
			/**
			 * @author Anil
			 * @since 07-01-2021
			 * Showing process level menus
			 */
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
			}
			docVerticalPanel.add(form.content);
			
			Timer timer = new Timer() {
				@Override
				public void run() {

					form.content.getElement().getStyle().setHeight(570, Unit.PX);
					form.content.getElement().getStyle().setWidth(950, Unit.PX);

					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.setStylePrimaryName("gwt-SuggestBoxPopup");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
				
					Lead lead = (Lead) model;
					
					LeadPresenter purchaseReqPresenter=new LeadPresenter(form,new Lead());
					presenter = purchaseReqPresenter;
					presenter.setModel(lead);
					
					form.updateView(lead);
					
					form.setPopUpAppMenubar(true);
					form.setHideNavigationButtons(hideNavigationFlag);//Ashwini Patil Date:21-09-2022
					form.setToViewState();
					
//					form.getProcessLevelBar().setVisibleFalse(false);
					
					glassPanel.hide();
				}
			};
			timer.schedule(1000);
		}
		/** Ends **/
		/**
		 * Updated By: Viraj 
		 * Date: 04-06-2019
		 * Description: To add and save data from employee screen directly to employeeAdditionalDetails 
		 */
		else if(presenterName.equalsIgnoreCase(AppConstants.EMPLOYEEADDITIONALDETAILS)) {
			glassPanel.show();
			
			final EmployeeAdditionalDetailsForm form = new EmployeeAdditionalDetailsForm();
			docVerticalPanel.add(form.content);
			
			Timer timer = new Timer() {
				@Override
				public void run() {

					form.content.getElement().getStyle().setHeight(570, Unit.PX);
					form.content.getElement().getStyle().setWidth(950, Unit.PX);

					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.setStylePrimaryName("gwt-SuggestBoxPopup");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
				
					EmployeeAdditionalDetails eAddDetails = (EmployeeAdditionalDetails) model;
					
					EmployeeAdditionalDetailsPresenter eAddDetailsPresenter=new EmployeeAdditionalDetailsPresenter(form,new EmployeeAdditionalDetails());
					presenter = eAddDetailsPresenter;
					form.setPopUpAppMenubar(true);//Ashwini Patil Date:21-06-2023
				
					if(eAddDetails.getCount() != 0) {
						Console.log("inside if condition of eAddDetails.getCount() view state: " +eAddDetails.getEmpInfo().getEmpCount());
						presenter.setModel(eAddDetails);
						form.updateView(eAddDetails);						
						form.setToViewState();						
						form.toggleAppHeaderBarMenu();//Ashwini Patil Date:21-06-2023
					}else {
						Console.log("inside else condition of eAddDetails.getCount() new state");
						form.setToNewState();
						form.updateView(eAddDetails);
					}
					
					glassPanel.hide();
				}
			};
			timer.schedule(1000);
		}
		/** Ends **/
		
		else if(presenterName.equalsIgnoreCase(AppConstants.SERVICELEAD)||presenterName.equalsIgnoreCase("Sales Lead")){
			glassPanel.show();
			
			if(presenterName.equalsIgnoreCase(AppConstants.SERVICELEAD)){
				AppMemory.getAppMemory().currentScreen=Screen.LEAD;
			}else{
				AppMemory.getAppMemory().currentScreen=Screen.SALESLEAD;
			}
			
			final LeadForm form = new LeadForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
			}
			docVerticalPanel.add(form.content);
			Timer timer = new Timer() {
				@Override
				public void run() {
//					form.content.getElement().getStyle().setHeight(570, Unit.PX);
//					form.content.getElement().getStyle().setWidth(950, Unit.PX);
//					viewDocumentPanel.setHeight("600px");
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					LeadPresenter leadPresenter=new LeadPresenter(form,new Lead());
					presenter = leadPresenter;
					
					if(model!=null){
						Lead leadObj = (Lead) model;
						form.updateView(leadObj);
						presenter.setModel(leadObj);
						form.setPopUpAppMenubar(true);
						form.setHideNavigationButtons(hideNavigationFlag);//Ashwini Patil Date:21-09-2022
						form.setToViewState();
					}else{
						form.setPopUpAppMenubar(true);
						form.setHideNavigationButtons(hideNavigationFlag);//Ashwini Patil Date:21-09-2022
						form.setToNewState();
					}
					
					glassPanel.hide();
				}
			};
			timer.schedule(1000);
		}
		
		else if(presenterName.equalsIgnoreCase(AppConstants.QUOTATION)){
			glassPanel.show();
			final QuotationForm form = new QuotationForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
			}
			docVerticalPanel.add(form.content);
			Timer timer = new Timer() {
				@Override
				public void run() {
//					form.content.getElement().getStyle().setHeight(570, Unit.PX);
//					form.content.getElement().getStyle().setWidth(950, Unit.PX);
//					viewDocumentPanel.setHeight("600px");
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					QuotationPresenter leadPresenter=new QuotationPresenter(form,new Quotation());
					presenter = leadPresenter;
					
					if(model!=null){
						Quotation leadObj = (Quotation) model;
						form.updateView(leadObj);
						presenter.setModel(leadObj);
						form.setPopUpAppMenubar(true);
						form.setHideNavigationButtons(hideNavigationFlag);//Ashwini Patil Date:21-09-2022
						form.setToViewState();
					}else{
						form.setPopUpAppMenubar(true);
						form.setHideNavigationButtons(hideNavigationFlag);//Ashwini Patil Date:21-09-2022
						form.setToNewState();
					}
					
					glassPanel.hide();
				}
			};
			timer.schedule(1000);
		}
		
		else if(presenterName.equalsIgnoreCase(AppConstants.CONTRACT)){
			glassPanel.show();
			final ContractForm form = new ContractForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				form.processLevelBar.content.setWidth("80%");
				docVerticalPanel.add(form.processLevelBar.content);
			}
			docVerticalPanel.add(form.content);
			Timer timer = new Timer() {
				@Override
				public void run() {
//					form.content.getElement().getStyle().setHeight(570, Unit.PX);
//					form.content.getElement().getStyle().setWidth(950, Unit.PX);
//					viewDocumentPanel.setHeight("600px");
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
										
					ContractPresenter leadPresenter=new ContractPresenter(form,new Contract());
					presenter = leadPresenter;
//					Console.log("applyRestrictionOnPopup in setModel="+applyRestrictionOnPopup);
//					if(applyRestrictionOnPopup)
//					{
//						form.setApplyRestrictedMode(applyRestrictionOnPopup);
//						Console.log("ApplyRestrictedMode set to form");
//					}
					
					if(model!=null){
						Contract leadObj = (Contract) model;
						form.updateView(leadObj);
						presenter.setModel(leadObj);
						form.setPopUpAppMenubar(true);
						form.setHideNavigationButtons(hideNavigationFlag);//Ashwini Patil Date:21-09-2022
						form.setToViewState();
					}else{
						form.setPopUpAppMenubar(true);
						form.setHideNavigationButtons(hideNavigationFlag);//Ashwini Patil Date:21-09-2022
						form.setToNewState();
					}
					
					
					glassPanel.hide();
				}
			};
			timer.schedule(1000);
		}
		else if(presenterName.equalsIgnoreCase("Sales Quotation")){
			glassPanel.show();
			final SalesQuotationForm form = new SalesQuotationForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			Timer timer = new Timer() {
				@Override
				public void run() {
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					SalesQuotationPresenter leadPresenter=new SalesQuotationPresenter(form,new SalesQuotation());
					presenter = leadPresenter;
					
					if(model!=null){
						SalesQuotation leadObj = (SalesQuotation) model;
						form.updateView(leadObj);
						presenter.setModel(leadObj);
						form.setPopUpAppMenubar(true);
						form.setHideNavigationButtons(hideNavigationFlag);//Ashwini Patil Date:21-09-2022
						form.setToViewState();
					}else{
						form.setPopUpAppMenubar(true);
						form.setHideNavigationButtons(hideNavigationFlag);//Ashwini Patil Date:21-09-2022
						form.setToNewState();
					}
					
					glassPanel.hide();
				}
			};
			timer.schedule(1000);
		}else if(presenterName.equalsIgnoreCase("Sales Order")){
			glassPanel.show();
			final SalesOrderForm form = new SalesOrderForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			Timer timer = new Timer() {
				@Override
				public void run() {
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					SalesOrderPresenter leadPresenter=new SalesOrderPresenter(form,new SalesOrder());
					presenter = leadPresenter;
					
					if(model!=null){
						SalesOrder leadObj = (SalesOrder) model;
						form.updateView(leadObj);
						presenter.setModel(leadObj);
						form.setPopUpAppMenubar(true);
						form.setHideNavigationButtons(hideNavigationFlag);//Ashwini Patil Date:21-09-2022
						form.setToViewState();
					}else{
						form.setPopUpAppMenubar(true);
						form.setHideNavigationButtons(hideNavigationFlag);//Ashwini Patil Date:21-09-2022
						form.setToNewState();
					}
					
					glassPanel.hide();
				}
			};
			timer.schedule(1000);
		}else if(presenterName.equalsIgnoreCase("Delivery Note")){
			glassPanel.show();
			final DeliveryNoteForm form = new DeliveryNoteForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			Timer timer = new Timer() {
				@Override
				public void run() {
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					DeliveryNotePresenter leadPresenter=new DeliveryNotePresenter(form,new DeliveryNote());
					presenter = leadPresenter;
					
					if(model!=null){
						DeliveryNote leadObj = (DeliveryNote) model;
						form.updateView(leadObj);
						presenter.setModel(leadObj);
						form.setPopUpAppMenubar(true);
						form.setHideNavigationButtons(hideNavigationFlag);//Ashwini Patil Date:21-09-2022
						form.setToViewState();
					}else{
						form.setPopUpAppMenubar(true);
						form.setHideNavigationButtons(hideNavigationFlag);//Ashwini Patil Date:21-09-2022
						form.setToNewState();
					}
					
					glassPanel.hide();
				}
			};
			timer.schedule(1000);
		}
		else if(presenterName.equalsIgnoreCase(AppConstants.MULTIPLEXPMANAGEMENT)){
			AppMemory.getAppMemory().currentScreen=Screen.MULTIPLEEXPENSEMANAGMENT;
			Console.log(" 1 ");
			glassPanel.show();
			final MultipleExpensemanagmentForm form = new MultipleExpensemanagmentForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			Timer timer = new Timer() {
				@Override
				public void run() {
					Console.log(" 2 ");
//					form.content.getElement().getStyle().setHeight(570, Unit.PX);
//					form.content.getElement().getStyle().setWidth(950, Unit.PX);
//					viewDocumentPanel.setHeight("600px");
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					MultipleExpensemanagmentPresenter leadPresenter=new MultipleExpensemanagmentPresenter(form,new MultipleExpenseMngt());
					presenter = leadPresenter;
					Console.log(" 3 ");
					if(model!=null){
						form.setPopUpAppMenubar(true);
						form.setToNewState();
						Console.log(" 4 ");
						MultipleExpenseMngt leadObj = (MultipleExpenseMngt) model;
						form.updateView(leadObj);
						
						if(leadObj.getPettyCashName()!=null&&!leadObj.getPettyCashName().equals("")){
							form.getCbexptype().setValue(true);
							form.getOlbpettycashname().setEnabled(true);
							form.getOlbpettycashname().setValue(leadObj.getPettyCashName());
							form.getOlbPaymentMethod().setValue("Petty Cash");
						}
						presenter.setModel(leadObj);
						form.setPopUpAppMenubar(true);
						form.setHideNavigationButtons(true);
//						form.setToViewState();
//					}else{
						Console.log(" 5 ");
					}
					
					glassPanel.hide();
				}
			};
			timer.schedule(3000);
		}
		else if(presenterName.equalsIgnoreCase(AppConstants.COMPANY)){
			glassPanel.show();
			final CompanyForm form = new CompanyForm(true);
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			Timer timer = new Timer() {
				@Override
				public void run() {
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					CompanyPresenter leadPresenter=new CompanyPresenter(form,new Company());
					presenter = leadPresenter;
					
					if(model!=null){
						Company company = (Company) model;
						form.updateView(company);
						presenter.setModel(company);
						form.setPopUpAppMenubar(true);
						form.setToViewState();
					}else{
						form.setPopUpAppMenubar(true);
						form.setToNewState();
					}
					form.toggleAppHeaderBarMenu();

					glassPanel.hide();
				}
			};
			timer.schedule(1000);
			
			
		}
		/**Added by sheetal**/
		else if(presenterName.equalsIgnoreCase(AppConstants.MULTIPLEEXPENSEMANAGEMET)){
			glassPanel.show();
			
			final MultipleExpensemanagmentForm  form = new MultipleExpensemanagmentForm ();
			/**
			 * @author Anil
			 * @since 07-01-2021
			 * Showing process level menus
			 */
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
			}
			docVerticalPanel.add(form.content);
			
			Timer timer = new Timer() {
				@Override
				public void run() {

					form.content.getElement().getStyle().setHeight(570, Unit.PX);
					form.content.getElement().getStyle().setWidth(950, Unit.PX);

					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					MultipleExpenseMngt entity = (MultipleExpenseMngt) model;
					form.updateView(entity);
					
					MultipleExpensemanagmentPresenter multipleExpensemanagmentPresenter=new MultipleExpensemanagmentPresenter (form,new MultipleExpenseMngt());
					presenter = multipleExpensemanagmentPresenter;
					presenter.setModel(entity);
					
					form.setPopUpAppMenubar(true);
					form.setHideNavigationButtons(true);
					form.setToViewState();
					
					glassPanel.hide();
				}
			};
			timer.schedule(1000);
		}
		else if(presenterName.equalsIgnoreCase(AppConstants.COMPLAIN)){
			glassPanel.show();
			
			final ComplainForm  form = new ComplainForm ();
			/**
			 * @author Anil
			 * @since 07-01-2021
			 * Showing process level menus
			 */
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
			}
			docVerticalPanel.add(form.content);
			
			Timer timer = new Timer() {
				@Override
				public void run() {

					form.content.getElement().getStyle().setHeight(570, Unit.PX);
					form.content.getElement().getStyle().setWidth(950, Unit.PX);

					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					Complain entity = (Complain) model;
					form.updateView(entity);
					
					ComplainPresenter complainpresenter=new ComplainPresenter (form,new Complain());
					presenter = complainpresenter;
					presenter.setModel(entity);
					
					form.setPopUpAppMenubar(true);

					form.setToViewState();
					
					glassPanel.hide();
				}
			};
			timer.schedule(1000);
		}
		
		}
		else{
			Console.log("in else of if(!newmodeFlag)");
			if(presenterName.equalsIgnoreCase(AppConstants.BRANCH)){
				glassPanel.show();
				BranchPresenterTableProxy gentableScreen=new BranchPresenterTableProxy();
				final BranchForm  form=new  BranchForm(gentableScreen,FormTableScreen.UPPER_MODE,true,true);
				form.setPopUpAppMenubar(true);

				/**
				 * @author Anil
				 * @since 07-01-2021
				 * Showing process level menus
				 */
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
				}
				docVerticalPanel.add(form.content);
				Timer timer = new Timer() {
					@Override
					public void run() {

						form.content.getElement().getStyle().setHeight(570, Unit.PX);
						form.content.getElement().getStyle().setWidth(950, Unit.PX);

						viewDocumentPanel.setHeight("600px");
						viewDocumentPanel.show();
						viewDocumentPanel.center();
						
						BranchPresenter  branchPresenter=new  BranchPresenter (form,new Branch());

						presenter = branchPresenter;
						presenter.setModel(new Branch());
						
						form.setToNewState();
						
						glassPanel.hide();
					}
				};
				timer.schedule(1000);
				
			}
			else if(presenterName.equalsIgnoreCase(AppConstants.SERVICEPRODUCT)){

				glassPanel.show();
				
				final ServiceproductForm  form= new ServiceproductForm(true);
				
				/**
				 * @author Anil
				 * @since 07-01-2021
				 * Showing process level menus
				 */
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
				}
				docVerticalPanel.add(form.content);
				Timer timer = new Timer() {
					@Override
					public void run() {

						form.content.getElement().getStyle().setHeight(570, Unit.PX);
						form.content.getElement().getStyle().setWidth(950, Unit.PX);

						viewDocumentPanel.setHeight("600px");
						viewDocumentPanel.show();
						viewDocumentPanel.center();
						
						ServiceproductPresenter  serviceProductPresenter=new  ServiceproductPresenter (form,new ServiceProduct());

						presenter = serviceProductPresenter;
						presenter.setModel(new ServiceProduct());
						
						form.setPopUpAppMenubar(true);

						form.setToNewState();
						
						glassPanel.hide();
					}
				};
				timer.schedule(1000);
				
			
			}
			else if(presenterName.equalsIgnoreCase(AppConstants.ITEMPRODUCT)){

				glassPanel.show();
				
//				final ItemproductForm  form= new ItemproductForm();
				final ItemproductForm  form= new ItemproductForm(true);
				/**
				 * @author Anil
				 * @since 07-01-2021
				 * Showing process level menus
				 */
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
				}
				docVerticalPanel.add(form.content);
				Timer timer = new Timer() {
					@Override
					public void run() {

						form.content.getElement().getStyle().setHeight(570, Unit.PX);
						form.content.getElement().getStyle().setWidth(950, Unit.PX);

						viewDocumentPanel.setHeight("600px");
						viewDocumentPanel.show();
						viewDocumentPanel.center();
						
						ItemproductPresenter  itemproductPresenter = new  ItemproductPresenter (form,new ItemProduct());

						presenter = itemproductPresenter;
						presenter.setModel(new ItemProduct());
						form.setPopUpAppMenubar(true);

						form.setToNewState();
						
						glassPanel.hide();
					}
				};
				timer.schedule(1000);
			}
			else if(presenterName.equalsIgnoreCase(AppConstants.USERAUTHORIZATION)){

				//Ashwini Patil Date:20-03-2024 setting current module and document as in popup screen, module drop down is not getting loaded properly
				AppMemory.getAppMemory().currentScreen = Screen.USERAUTHORIZE;
				LoginPresenter.currentModule="Settings";
				LoginPresenter.currentDocumentName="User Authorization";
				glassPanel.show();
				
				UserAuthorizeTable gentableScreen = new UserAuthorizeTable();
				final UserAuthorizationForm form = new UserAuthorizationForm(gentableScreen, FormTableScreen.UPPER_MODE, true, true);
				/**
				 * @author Anil
				 * @since 07-01-2021
				 * Showing process level menus
				 */
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
				}
				docVerticalPanel.add(form.content);
				Timer timer = new Timer() {
					@Override
					public void run() {

						form.content.getElement().getStyle().setHeight(570, Unit.PX);
						form.content.getElement().getStyle().setWidth(950, Unit.PX);

						viewDocumentPanel.setHeight("600px");
						viewDocumentPanel.show();
						viewDocumentPanel.center();
						
						UserAuthorizationPresenter  userAuthPresenter=new  UserAuthorizationPresenter (form,new UserRole());

						presenter = userAuthPresenter;
						presenter.setModel(new UserRole());
						
						form.setPopUpAppMenubar(true);

						form.setToNewState();
						
						glassPanel.hide();
					}
				};
				timer.schedule(1000);
			}
			else if(presenterName.equalsIgnoreCase(AppConstants.LOCALITY)){

				glassPanel.show();
				
				 LocalityTable gentableScreen=new LocalityTable();
				 final LocalityForm	form=new  LocalityForm(gentableScreen,FormTableScreen.UPPER_MODE,true,true);
				
				/**
				 * @author Anil
				 * @since 07-01-2021
				 * Showing process level menus
				 */
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
				}
				docVerticalPanel.add(form.content);
				Timer timer = new Timer() {
					@Override
					public void run() {

						form.content.getElement().getStyle().setHeight(570, Unit.PX);
						form.content.getElement().getStyle().setWidth(950, Unit.PX);

						viewDocumentPanel.setHeight("600px");
						viewDocumentPanel.show();
						viewDocumentPanel.center();
						
						LocalityPresenter  localityPresenter=new  LocalityPresenter (form,new Locality());

						presenter = localityPresenter;
						presenter.setModel(new Locality());
						
						form.setPopUpAppMenubar(true);

						form.setToNewState();
						
						glassPanel.hide();
					}
				};
				timer.schedule(1000);
			}
			else if(presenterName.equalsIgnoreCase(AppConstants.CITY)){

				glassPanel.show();
				
				 CityTable gentableScreen=new CityTable();
				 final CityForm	form=new  CityForm(gentableScreen,FormTableScreen.UPPER_MODE,true,true);
				
				/**
				 * @author Anil
				 * @since 07-01-2021
				 * Showing process level menus
				 */
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
				}
				docVerticalPanel.add(form.content);
				Timer timer = new Timer() {
					@Override
					public void run() {

						form.content.getElement().getStyle().setHeight(570, Unit.PX);
						form.content.getElement().getStyle().setWidth(950, Unit.PX);

						viewDocumentPanel.setHeight("600px");
						viewDocumentPanel.show();
						viewDocumentPanel.center();
						
						CityPresenter  cityPresenter=new  CityPresenter (form,new City());

						presenter = cityPresenter;
						presenter.setModel(new City());
						
						form.setPopUpAppMenubar(true);

						form.setToNewState();
						
						glassPanel.hide();
					}
				};
				timer.schedule(1000);
			}
			else if(presenterName.equalsIgnoreCase(AppConstants.USER)){

				Console.log("in setmodel line 1917");
				glassPanel.show();
				//Ashwini Patil Date:21-03-2024 
				AppMemory.getAppMemory().currentScreen = Screen.USER;
				LoginPresenter.currentModule="Settings";
				LoginPresenter.currentDocumentName="User";
				
				
				 UserPresenterTableProxy gentableScreen=new UserPresenterTableProxy(); 
				 final UserForm  form=new  UserForm(gentableScreen,FormTableScreen.UPPER_MODE,true,true);
				 AppMemory.getAppMemory().skeleton.setProcessName(new InlineLabel("User"));
				 form.captionpanel.setCaptionText("User");
				/**
				 * @author Anil
				 * @since 07-01-2021
				 * Showing process level menus
				 */
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
				}
				docVerticalPanel.add(form.content);
				Timer timer = new Timer() {
					@Override
					public void run() {

						form.content.getElement().getStyle().setHeight(570, Unit.PX);
						form.content.getElement().getStyle().setWidth(950, Unit.PX);

						viewDocumentPanel.setHeight("600px");
						viewDocumentPanel.show();
						viewDocumentPanel.center();
						
						UserPresenter  userpresenter=new  UserPresenter (form,new User());

						presenter = userpresenter;
						presenter.setModel(new User());
						
						form.setPopUpAppMenubar(true);

						form.setToNewState();
						
						glassPanel.hide();
					}
				};
				timer.schedule(1000);
			}
			
			else if(presenterName.equalsIgnoreCase(AppConstants.COMPANYPAYMENT)){

				glassPanel.show();
				
				 final CompanyPaymentForm form = new CompanyPaymentForm(true);
					
				/**
				 * @author Anil
				 * @since 07-01-2021
				 * Showing process level menus
				 */
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
				}
				docVerticalPanel.add(form.content);
				Timer timer = new Timer() {
					@Override
					public void run() {

						form.content.getElement().getStyle().setHeight(570, Unit.PX);
						form.content.getElement().getStyle().setWidth(950, Unit.PX);

						viewDocumentPanel.setHeight("600px");
						viewDocumentPanel.show();
						viewDocumentPanel.center();
						
						CompanyPaymentPresenter  companyPayment=new  CompanyPaymentPresenter (form,new CompanyPayment());

						presenter = companyPayment;
						presenter.setModel(new CompanyPayment());
						
						form.setPopUpAppMenubar(true);

						form.setToNewState();
						
						glassPanel.hide();
					}
				};
				timer.schedule(1000);
			}
			else if(presenterName.equalsIgnoreCase(AppConstants.DONOTRENEW)){
				glassPanel.show();
				
				ConfigTabels table = new ConfigTabels();
				final ConfigForm form = new ConfigForm(table, FormTableScreen.UPPER_MODE,
						true,true);
				AppMemory.getAppMemory().skeleton.setProcessName(new InlineLabel("Contract Loss Renewal Reasons"));

				/**
				 * @author Anil
				 * @since 07-01-2021
				 * Showing process level menus
				 */
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
				}
				docVerticalPanel.add(form.content);
				Timer timer = new Timer() {
					@Override
					public void run() {

						form.content.getElement().getStyle().setHeight(570, Unit.PX);
						form.content.getElement().getStyle().setWidth(950, Unit.PX);

						viewDocumentPanel.setHeight("600px");
						viewDocumentPanel.show();
						viewDocumentPanel.center();
						
						ConfigPresenter p = new ConfigPresenter(form, new Config());

						presenter = p;
						presenter.setModel(new Config());
						
						form.setPopUpAppMenubar(true);

						form.setToNewState();
						
						glassPanel.hide();
					}
				};
				timer.schedule(1000);
				
			}
			else if(presenterName.equalsIgnoreCase(AppConstants.SPECIFICREASONFORRESCHEDULING)){
				glassPanel.show();
				
				ConfigTabels table = new ConfigTabels();
				//Commented below code as Screen title was getting fixed to Settings/Implementation
//				AppMemory.getAppMemory().skeleton.setProcessName(new InlineLabel("Reschedule Reasons"));
				final ConfigForm form = new ConfigForm(table, FormTableScreen.UPPER_MODE,
						true,true);
				form.captionpanel.setCaptionText("Reschedule Reasons"); //Ashwini Patil
				/**
				 * @author Anil
				 * @since 07-01-2021
				 * Showing process level menus
				 */
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
				}
				docVerticalPanel.add(form.content);
				Timer timer = new Timer() {
					@Override
					public void run() {

						form.content.getElement().getStyle().setHeight(570, Unit.PX);
						form.content.getElement().getStyle().setWidth(950, Unit.PX);

						viewDocumentPanel.setHeight("600px");
						viewDocumentPanel.show();
						viewDocumentPanel.center();
						
						ConfigPresenter p = new ConfigPresenter(form, new Config());

						presenter = p;
						presenter.setModel(new Config());
						
						form.setPopUpAppMenubar(true);

						form.setToNewState();
						
						glassPanel.hide();
					}
				};
				timer.schedule(1000);
				
			}
			else if(presenterName.equalsIgnoreCase(AppConstants.SERVICECANCELLATIONREMARK)){
				glassPanel.show();
				
				ConfigTabels table = new ConfigTabels();
				//Commented below code as Screen title was getting fixed to Settings/Implementation
//				AppMemory.getAppMemory().skeleton.setProcessName(new InlineLabel("Service Cancellation Reasons"));
				final ConfigForm form = new ConfigForm(table, FormTableScreen.UPPER_MODE,
						true,true);
				form.captionpanel.setCaptionText("Service Cancellation Reasons"); //Ashwini Patil
				/**
				 * @author Anil
				 * @since 07-01-2021
				 * Showing process level menus
				 */
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
				}
				docVerticalPanel.add(form.content);
				Timer timer = new Timer() {
					@Override
					public void run() {

						form.content.getElement().getStyle().setHeight(570, Unit.PX);
						form.content.getElement().getStyle().setWidth(950, Unit.PX);

						viewDocumentPanel.setHeight("600px");
						viewDocumentPanel.show();
						viewDocumentPanel.center();
						
						ConfigPresenter p = new ConfigPresenter(form, new Config());

						presenter = p;
						presenter.setModel(new Config());
						
						form.setPopUpAppMenubar(true);

						form.setToNewState();
						
						glassPanel.hide();
					}
				};
				timer.schedule(1000);
				
			}
			else if(presenterName.equalsIgnoreCase(AppConstants.CRONJOBCONFIGRATION)){
				glassPanel.show();
				
				CronJobConfigrationTable gentable=new CronJobConfigrationTable();
				final CronJobConfigrationForm form=new CronJobConfigrationForm(gentable,FormTableScreen.UPPER_MODE,true,true);
				AppMemory.getAppMemory().skeleton.setProcessName(new InlineLabel("Cron Job Reminder Setting"));

				/**
				 * @author Anil
				 * @since 07-01-2021
				 * Showing process level menus
				 */
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
				}
				docVerticalPanel.add(form.content);
				Timer timer = new Timer() {
					@Override
					public void run() {

						form.content.getElement().getStyle().setHeight(570, Unit.PX);
						form.content.getElement().getStyle().setWidth(950, Unit.PX);

						viewDocumentPanel.setHeight("600px");
						viewDocumentPanel.show();
						viewDocumentPanel.center();
						
						CronJobConfigrationPresenter cronjobpresenter=new CronJobConfigrationPresenter(form, new CronJobConfigration());

						presenter = cronjobpresenter;
						presenter.setModel(new CronJobConfigration());
						
						form.setPopUpAppMenubar(true);

						form.setToNewState();
						
						glassPanel.hide();
					}
				};
				timer.schedule(1000);
				
			}
			else if(presenterName.equalsIgnoreCase(AppConstants.USER)){
					glassPanel.show();
					Console.log("in setmodel line 2192");
					UserPresenterTableProxy gentableScreen=new UserPresenterTableProxy(); 
					final UserForm  form=new  UserForm(gentableScreen,FormTableScreen.UPPER_MODE,true,true);
					form.setPopUpAppMenubar(true);
					
					
					/**
					 * @author Anil
					 * @since 07-01-2021
					 * Showing process level menus
					 */
					if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
						form.setAction(false);
						form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
						form.processLevelBar.setVisibleFalse(false);
						docVerticalPanel.add(form.processLevelBar.content);
					}
					docVerticalPanel.add(form.content);
					Timer timer = new Timer() {
						@Override
						public void run() {

							form.content.getElement().getStyle().setHeight(570, Unit.PX);
							form.content.getElement().getStyle().setWidth(950, Unit.PX);

							viewDocumentPanel.setHeight("600px");
							viewDocumentPanel.show();
							viewDocumentPanel.center();
							
							UserPresenter  userPresenter=new  UserPresenter (form,new User());

							presenter = userPresenter;
							presenter.setModel(new User());
							
							form.setToNewState();
							
							glassPanel.hide();
						}
					};
					timer.schedule(1000);
					
			}
		
		else if(presenterName.equalsIgnoreCase(AppConstants.MULTIPLEXPMANAGEMENT)){
			AppMemory.getAppMemory().currentScreen=Screen.MULTIPLEEXPENSEMANAGMENT;
			Console.log(" 1 ");
			glassPanel.show();
			final MultipleExpensemanagmentForm form = new MultipleExpensemanagmentForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			Timer timer = new Timer() {
				@Override
				public void run() {
					Console.log(" 2 ");
					form.content.getElement().getStyle().setHeight(570, Unit.PX);
					form.content.getElement().getStyle().setWidth(950, Unit.PX);
					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					MultipleExpensemanagmentPresenter leadPresenter=new MultipleExpensemanagmentPresenter(form,new MultipleExpenseMngt());
					presenter = leadPresenter;
					Console.log(" 3 ");
					if(model!=null){
						form.setPopUpAppMenubar(true);
						form.setToNewState();
						Console.log(" 4 ");
						MultipleExpenseMngt leadObj = (MultipleExpenseMngt) model;
						form.updateView(leadObj);
						
						if(leadObj.getPettyCashName()!=null&&!leadObj.getPettyCashName().equals("")){
							form.getCbexptype().setValue(true);
							form.getOlbpettycashname().setEnabled(true);
							form.getOlbpettycashname().setValue(leadObj.getPettyCashName());
							form.getOlbPaymentMethod().setValue("Petty Cash");
						}
						presenter.setModel(leadObj);
//						form.setPopUpAppMenubar(true);
//						form.setToViewState();
//					}else{
						Console.log(" 5 ");
					}
					
					glassPanel.hide();
				}
			};
			timer.schedule(3000);
		}
			
		else if(presenterName.equalsIgnoreCase(AppConstants.QUICKEMPLOYEE)){

			glassPanel.show();
			
			 final QuickEmployeeForm form = new QuickEmployeeForm(true);
			 AppMemory.getAppMemory().skeleton.setProcessName(new InlineLabel("Quick Employee"));

			/**
			 * @author Anil
			 * @since 07-01-2021
			 * Showing process level menus
			 */
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
			}
			docVerticalPanel.add(form.content);
			Timer timer = new Timer() {
				@Override
				public void run() {

					form.content.getElement().getStyle().setHeight(570, Unit.PX);
					form.content.getElement().getStyle().setWidth(950, Unit.PX);

					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					QuickEmployeePresenter quickEmployeepresenter = new QuickEmployeePresenter(form, new Employee());
					presenter = quickEmployeepresenter;
					presenter.setModel(new Employee());
					
					form.setPopUpAppMenubar(true);

					form.setToNewState();
					
					glassPanel.hide();
				}
			};
			timer.schedule(1000);
		}
		else if(presenterName.equalsIgnoreCase("TYPEOFTREATMENT")||presenterName.equalsIgnoreCase(AppConstants.FREQUENCY)||presenterName.equalsIgnoreCase("FindingType")||presenterName.equalsIgnoreCase("SERVICECHECKLIST")){
			glassPanel.show();
			
			ConfigTabels table = new ConfigTabels();
			
			
			final ConfigForm form = new ConfigForm(table, FormTableScreen.UPPER_MODE,
					true,true);
			if(presenterName.equalsIgnoreCase("TYPEOFTREATMENT"))
				form.captionpanel.setCaptionText("Type of Treatment");
			if(presenterName.equalsIgnoreCase("SERVICECHECKLIST"))
				form.captionpanel.setCaptionText("Service Checklist");						
			if(presenterName.equalsIgnoreCase(AppConstants.FREQUENCY))
				form.captionpanel.setCaptionText("Frequency");
			if(presenterName.equalsIgnoreCase("FindingType"))	
				form.captionpanel.setCaptionText("Finding Type");
			
		
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
			}
			docVerticalPanel.add(form.content);
			Timer timer = new Timer() {
				@Override
				public void run() {

					form.content.getElement().getStyle().setHeight(570, Unit.PX);
					form.content.getElement().getStyle().setWidth(950, Unit.PX);

					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					ConfigPresenter p = new ConfigPresenter(form, new Config());

					presenter = p;
					presenter.setModel(new Config());
					
					form.setPopUpAppMenubar(true);

					form.setToNewState();
					
					glassPanel.hide();
				}
			};
			timer.schedule(1000);
			
		}
		else if(presenterName.equalsIgnoreCase(AppConstants.EXPENSEMANAGEMET)){
			AppMemory.getAppMemory().currentScreen=Screen.MULTIPLEEXPENSEMANAGMENT;
			Console.log(" 1 ");
			glassPanel.show();
			final MultipleExpensemanagmentForm form = new MultipleExpensemanagmentForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			Timer timer = new Timer() {
				@Override
				public void run() {
					Console.log(" 2 ");
					form.content.getElement().getStyle().setHeight(570, Unit.PX);
					form.content.getElement().getStyle().setWidth(950, Unit.PX);
					viewDocumentPanel.setHeight("600px");
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					MultipleExpensemanagmentPresenter leadPresenter=new MultipleExpensemanagmentPresenter(form,new MultipleExpenseMngt());
					presenter = leadPresenter;
					Console.log(" 3 ");
					presenter.setModel(new MultipleExpenseMngt());
					form.setPopUpAppMenubar(true);
					form.setToNewState();
					
					if(model!=null){
						SalesOrder salesorder = (SalesOrder) model;
						form.pInfo.setValue(salesorder.getCinfo());
						form.pInfo.setEnable(false);
						form.tbSalesOrderId.setValue(salesorder.getCount()+"");
						form.tbSalesOrderId.setEnabled(false);
					}
					
					glassPanel.hide();
					
					
				}
			};
			timer.schedule(3000);
		
			
		}else if(presenterName.equalsIgnoreCase("View Expense")){
			AppMemory.getAppMemory().currentScreen=Screen.MULTIPLEEXPENSEMANAGMENT;
			Console.log(" 1 ");
			glassPanel.show();
			final MultipleExpensemanagmentForm form = new MultipleExpensemanagmentForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			Timer timer = new Timer() {
				@Override
				public void run() {
					Console.log(" 2 ");
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					MultipleExpensemanagmentPresenter leadPresenter=new MultipleExpensemanagmentPresenter(form,new MultipleExpenseMngt());
					presenter = leadPresenter;
					Console.log(" 3 ");
					if(model!=null){
						form.setPopUpAppMenubar(true);
//						form.setToNewState();
						Console.log(" 4 ");
						MultipleExpenseMngt leadObj = (MultipleExpenseMngt) model;
						form.updateView(leadObj);
						
						if(leadObj.getPettyCashName()!=null&&!leadObj.getPettyCashName().equals("")){
							form.getCbexptype().setValue(true);
							form.getOlbpettycashname().setEnabled(true);
							form.getOlbpettycashname().setValue(leadObj.getPettyCashName());
							form.getOlbPaymentMethod().setValue("Petty Cash");
						}
						presenter.setModel(leadObj);
						form.setPopUpAppMenubar(true);
						form.setHideNavigationButtons(true);
						form.setToViewState();
//					}else{
						Console.log(" 5 ");
					}
					
					glassPanel.hide();
				}
			};
			timer.schedule(3000);
		}
		
	}
	}
	
	
	/*
	 *  this method for remove PopUp objects
	 */
	void removePopObjects() {
		try {

			/**
			 * @author Anil @since 29-01-2021
			 * Clearing the vertical panel
			 */
//			for (Widget wi : docVerticalPanel) {
//				boolean flag = viewDocumentPanel.remove(wi);
//				wi.removeFromParent();
//				System.out.println("get item remove status " + flag);
//			}
			if(docVerticalPanel!=null){
				docVerticalPanel.clear();
			}
			

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	/*
	 *  end
	 */
	
	public void setBilling(String presenterName, final SuperModel model,final List<BillingDocument> listsel) {
		removePopObjects();
		docVerticalPanel.add(btnClose);
		docVerticalPanel.add(appLevelMenuPanel);
		
		if(presenterName.equalsIgnoreCase("Multiple Bill")){
			glassPanel.show();
			final BillingDetailsForm form = new BillingDetailsForm();
			BillingDetailsForm.flag1=false;
			BillingDetailsForm.flag=true;
			
			/**
			 * @author Anil
			 * @since 07-01-2021
			 * Showing process level menus
			 */
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
			}
			docVerticalPanel.add(form.content);
			
			Timer timer = new Timer() {
				@Override
				public void run() {
					Console.log("Bill Size : "+listsel.size());
//					form.content.getElement().getStyle().setHeight(570, Unit.PX);
//					form.content.getElement().getStyle().setWidth(950, Unit.PX);
//					viewDocumentPanel.setHeight("600px");
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					BillingDocument billingEntity = (BillingDocument) model;
					form.updateView(billingEntity);
					
					BillingDetailsPresenter invoicePresenter=new BillingDetailsPresenter(form,new BillingDocument());
					invoicePresenter.multipleContractBillingInvoiceFlag=true;
					presenter = invoicePresenter;
					presenter.setModel(billingEntity);
					
					List<BillingDocumentDetails> billingTableData=fillBillingDocumentTableOnClick(listsel);
					ArrayList<BillingDocumentDetails> multipleBillDocs=new ArrayList<BillingDocumentDetails>();
					multipleBillDocs.addAll(billingTableData);
					 
					 List<SalesOrderProductLineItem> billingProductTableData=fillBillingProductTableOnClick(listsel);
					 ArrayList<SalesOrderProductLineItem> multipleBillProdDocs=new ArrayList<SalesOrderProductLineItem>();
					 multipleBillProdDocs.addAll(billingProductTableData);
					
					form.getBillingDocumentTable().getDataprovider().getList().addAll(multipleBillDocs);
					System.out.println("BILLING DOCUMNET TABLE......! "+form.getBillingDocumentTable().getDataprovider().getList().size());
//					form.getSalesProductTable().connectToLocal();
					form.getSalesProductTable().getDataprovider().setList(multipleBillProdDocs);
					System.out.println("BILLING PRODUCT TABLE......! "+form.getSalesProductTable().getDataprovider().getList().size());
					form.getSalesProductTable().getTable().redraw();
					form.getSalesProductTable().setEnable(false);
					invoicePresenter.reactOnProductsChange();
					BillingDetailsForm.flag=false;
					BillingDetailsForm.flag1=true;
					form.setPopUpAppMenubar(true);
					form.setToViewState();
					
					
					

					
					glassPanel.hide();
				}
			};
			timer.schedule(1000);
		}
	}
	
	 private List<BillingDocumentDetails> fillBillingDocumentTableOnClick(List<BillingDocument> listsel){
//		 List<BillingDocument> listsel=getAllSelectedRecords();
		 ArrayList<BillingDocumentDetails> arrBilingDoc=new ArrayList<BillingDocumentDetails>();
		 for(int i=0;i<listsel.size();i++){
			 BillingDocumentDetails billingdoc=new BillingDocumentDetails();
			 billingdoc.setOrderId(listsel.get(i).getContractCount());
			 billingdoc.setBillId(listsel.get(i).getCount());
			 billingdoc.setBillingDate(listsel.get(i).getBillingDate());
			 billingdoc.setBillingCategory(listsel.get(i).getBillingCategory());
			 billingdoc.setBillingGroup(listsel.get(i).getBillingGroup());
			 billingdoc.setBillingType(listsel.get(i).getBillingType());
			 billingdoc.setBillAmount(listsel.get(i).getTotalBillingAmount());
			 billingdoc.setBillstatus(listsel.get(i).getStatus());
			 arrBilingDoc.add(billingdoc);
		 }
		 return arrBilingDoc;
	 }
	 
	 private List<SalesOrderProductLineItem> fillBillingProductTableOnClick(List<BillingDocument> listsel){
		 ArrayList<SalesOrderProductLineItem> arrBilingDoc=new ArrayList<SalesOrderProductLineItem>();
		 int productSrNo=0;
		 for(int i=0;i<listsel.size();i++){
			 for(SalesOrderProductLineItem item:listsel.get(i).getSalesOrderProducts()){
				 productSrNo++;
				 item.setProductSrNumber(productSrNo);
				 arrBilingDoc.add(item);
			 }
		 }
		 return arrBilingDoc;
	 }
	 
	 
	 public void setCompositeScreen(String presenterName, final SuperModel model,final PersonInfo personInfo,final boolean isCompany) {
		 	/**
			 * @author Anil @since 13-08-2021
			 * Simplification of customer composite
			 */
			removePopObjects();
			docVerticalPanel.add(btnClose);
			docVerticalPanel.add(appLevelMenuPanel);
			
			Console.log(" "+presenterName);
			
			screenState= AppMemory.getAppMemory().currentState;
			Console.log("screenState "+screenState);
			
			if(presenterName.equalsIgnoreCase(AppConstants.CUSTOMER)){
				glassPanel.show();
				final CustomerForm form = new CustomerForm(true);
//				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
//					form.setAction(false);
//					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
//					form.processLevelBar.setVisibleFalse(false);
//					docVerticalPanel.add(form.processLevelBar.content);
//					form.processLevelBar.content.setWidth("80%");
//				}
				
				/**
				 * @author Anil @since 30-08-2021
				 */
				if (form.getProcesslevelBarNames() != null){
					form.processLevelBar.setVisibleFalse(false);
				}
				Console.log("1 - "+presenterName);
				docVerticalPanel.add(form.content);
				Timer timer = new Timer() {
					@Override
					public void run() {
//						form.content.getElement().getStyle().setHeight(570, Unit.PX);
//						form.content.getElement().getStyle().setWidth(950, Unit.PX);
//						viewDocumentPanel.setHeight("600px");
						setSizeOfPanel(form);
						viewDocumentPanel.show();
						viewDocumentPanel.center();
						
						CustomerPresenter leadPresenter=new CustomerPresenter(form,new Customer());
						presenter = leadPresenter;
						
						if(model!=null){
							Customer leadObj = (Customer) model;
							form.updateView(leadObj);
							presenter.setModel(leadObj);
							form.setPopUpAppMenubar(true);
							form.setToViewState();
						}else{
							form.setPopUpAppMenubar(true);
							form.setToNewState();
							form.isCompany.setValue(isCompany);
							form.tbClientName.setEnabled(isCompany);
							form.isCompany.setEnabled(false);
						}
						
						glassPanel.hide();
					}
				};
				timer.schedule(1000);
			}else if(presenterName.equalsIgnoreCase(AppConstants.CUSTOMERBRANCH)){
				glassPanel.show();
				CustomerBranchPresenterTable gentableScreen=new CustomerBranchTable();
				final CustomerBranchForm  form=new  CustomerBranchForm(gentableScreen,FormTableScreen.UPPER_MODE,false,true);
				//Ashwini Patil Date:23-06-2022 On contract screen if we click customer branch icon in Person Info composite, screen was freezing. that was happening because of below two lines. so commented.
				//form.getCaptionpanel().setTitle(AppConstants.CUSTOMERBRANCH);
				//form.getCaptionpanel().setCaptionText(AppConstants.CUSTOMERBRANCH);

				if (form.getProcesslevelBarNames() != null){
					form.processLevelBar.setVisibleFalse(false);
				}
				Console.log("2 - "+presenterName);
				docVerticalPanel.add(form.content);
				Timer timer = new Timer() {
					@Override
					public void run() {
						setSizeOfPanel(form);
						viewDocumentPanel.show();
						viewDocumentPanel.center();
						
						
						MyQuerry query=new MyQuerry();
						Vector<Filter> vecFil=new Vector<Filter>();
						Filter temp=new Filter();
						temp.setQuerryString("companyId");
						temp.setLongValue(UserConfiguration.getCompanyId());
						vecFil.add(temp);
						
						temp=new Filter();
						temp.setQuerryString("cinfo.count");
						temp.setIntValue(personInfo.getCount());
						vecFil.add(temp);
						
						query.setFilters(vecFil);
						query.setQuerryObject(new CustomerBranchDetails());
						
						CustomerBranchPresenter leadPresenter=new CustomerBranchPresenter(form,new CustomerBranchDetails(),query);
						presenter = leadPresenter;
						
						if(model!=null){
							CustomerBranchDetails leadObj = (CustomerBranchDetails) model;
							form.updateView(leadObj);
							presenter.setModel(leadObj);
							form.setPopUpAppMenubar(true);
							form.setToViewState();
							if (form.getProcesslevelBarNames() != null){
								form.processLevelBar.setVisibleFalse(false);
							}
						}else{
							form.setPopUpAppMenubar(true);
							form.setToNewState();
							form.getPersonInfoComposite().setValue(personInfo);
							form.getPersonInfoComposite().setEnable(false);
							form.getPersonInfoComposite().hrButtonPanel.setVisible(false);
						}						
						glassPanel.hide();
					}
				};
				timer.schedule(1000);
			}else if(presenterName.equalsIgnoreCase(AppConstants.CONTACTPERSONDETAILS)){
				glassPanel.show();
				ContactPersonPresenterTableProxy gentableScreen=new ContactPersonPresenterTableProxy();
				final ContactPersonForm  form=new  ContactPersonForm(gentableScreen,FormTableScreen.UPPER_MODE,true,true);
//				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
//					form.setAction(false);
//					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
//					form.processLevelBar.setVisibleFalse(false);
//					docVerticalPanel.add(form.processLevelBar.content);
//					form.processLevelBar.content.setWidth("80%");
//				}
				Console.log("3 - "+presenterName);
				docVerticalPanel.add(form.content);
				Timer timer = new Timer() {
					@Override
					public void run() {
//						form.content.getElement().getStyle().setHeight(570, Unit.PX);
//						form.content.getElement().getStyle().setWidth(950, Unit.PX);
//						viewDocumentPanel.setHeight("600px");
						setSizeOfPanel(form);
						
						viewDocumentPanel.show();
						viewDocumentPanel.center();
						
						MyQuerry query=new MyQuerry();
						Vector<Filter> vecFil=new Vector<Filter>();
						Filter temp=new Filter();
						temp.setQuerryString("companyId");
						temp.setLongValue(UserConfiguration.getCompanyId());
						vecFil.add(temp);
						
						temp=new Filter();
						temp.setQuerryString("personInfo.count");
						temp.setIntValue(personInfo.getCount());
						vecFil.add(temp);
						
						query.setFilters(vecFil);
						query.setQuerryObject(new ContactPersonIdentification());
						
						ContactPersonPresenter leadPresenter=new ContactPersonPresenter(form,new ContactPersonIdentification(),query);
						presenter = leadPresenter;
						
						if(model!=null){
							ContactPersonIdentification leadObj = (ContactPersonIdentification) model;
							form.updateView(leadObj);
							presenter.setModel(leadObj);
							form.setPopUpAppMenubar(true);
							form.setToViewState();
						}else{
							form.setPopUpAppMenubar(true);
							form.setToNewState();
							
							form.Entype.setValue("Customer");
							form.Entype.setEnabled(false);
							form.clearComposit();
							form.employeeInfoComposite.setVisible(false);
							form.personInfoComposite.setVisible(true);
							form.personInfoVendorComposite.setVisible(false);

							form.personInfoComposite.setEnabled(false);
							form.employeeInfoComposite.setEnable(false);
							form.personInfoVendorComposite.setEnabled(false);
							form.acshippingcomposite.clear();
							
							form.personInfoComposite.setValue(personInfo);
						}
						glassPanel.hide();
					}
				};
				timer.schedule(1000);
			}
	 }
	 
	public void setSizeOfPanel(UiScreen view) {
		int height = Window.getClientHeight();
		Console.log("Window Height : " + height);
		height = height * 78 / 100;
		Console.log("Updated Height : " + height);
		
		int width = Window.getClientWidth();
		Console.log("Window Width : " + width);
		width = width * 80 / 100;
		Console.log("Updated Width : " + width);

		view.content.getElement().getStyle().setHeight(height-30, Unit.PX);
		view.content.getElement().getStyle().setWidth(width-30, Unit.PX);
		viewDocumentPanel.setHeight(height+"px");
		docVerticalPanel.setWidth(width+"px");
	}
	
	
	 public void navigateScreen(String presenterName, final SuperModel model1,final SuperModel model2,final boolean hideNavigationButtons) {
		removePopObjects();
		docVerticalPanel.add(btnClose);
		docVerticalPanel.add(appLevelMenuPanel);
		
		Console.log("navigateScreen "+presenterName);
		
		if (presenterName.equals("Sales Quotation")) {
			
			final Lead model=(Lead) model1;

			AppMemory.getAppMemory().currentScreen = Screen.SALESQUOTATION;
			LoginPresenter.currentModule="Sales";
			LoginPresenter.currentDocumentName="Sales Quotation";
			glassPanel.show();
			final SalesQuotationForm form = new SalesQuotationForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			
			
			final SalesQuotation squot = new SalesQuotation();
			squot.setBranch(model.getBranch());
			squot.setEmployee(model.getEmployee());
			squot.setCinfo(model.getPersonInfo());
			squot.setStatus(SalesQuotation.CREATED);
			squot.setLeadCount(model.getCount());

			Timer t = new Timer() {
				@Override
				public void run() {
					
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					SalesQuotationPresenter leadPresenter=new SalesQuotationPresenter(form,new SalesQuotation());
					presenter = leadPresenter;
					
					form.setToNewState();
					form.updateView(squot);
					presenter.setModel(squot);
					form.setPopUpAppMenubar(true);
					form.setHideNavigationButtons(hideNavigationButtons);//03-10-2022	
					
					MyQuerry querry=new MyQuerry();
					Vector<Filter> filtervec=new Vector<Filter>();
					Filter tempfilter=new Filter();
					tempfilter=new Filter();
					tempfilter.setQuerryString("count");
					tempfilter.setIntValue(model.getPersonInfo().getCount());
					filtervec.add(tempfilter);
					tempfilter=new Filter();
					tempfilter.setQuerryString("companyId");
					tempfilter.setLongValue(model.getCompanyId());
					filtervec.add(tempfilter);
					querry.setFilters(filtervec);
					querry.setQuerryObject(new Customer());

					genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected error occurred!");
							glassPanel.hide();
						}

						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							Console.log("Size Of Result In SalesQuotation Presenter"+ result.size());
							List<SalesLineItem> leadProductsLis =model.getLeadProducts();
							Console.log("Size For Lis"+ leadProductsLis.size());
							for (SuperModel model : result) {
								Customer custentity = (Customer) model;
								form.getCbcformlis().setEnabled(true);
								for (SalesLineItem saleObj : leadProductsLis) {
									saleObj.setQuantity(saleObj.getQty());
								}
								if (leadProductsLis.size() != 0) {
									form.getLineitemsalesquotationtable().setValue(leadProductsLis);
								}
								form.getAcshippingcomposite().setValue(custentity.getSecondaryAdress());
								form.cust = custentity;
							}
							
							
						}
					});

					form.getTbQuotatinId().setText("");
					form.getTbContractId().setText("");
					form.getDbdeliverydate().setValue(null);
					form.getAcshippingcomposite().setEnable(false);
					form.getPersonInfoComposite().setEnabled(false);
					if (model.getRoundOffAmt() != 0) {
						form.getTbroundoffAmt().setValue(model.getRoundOffAmt() + "");
					}
					AppUtility.makeCustomerBranchLive(form.olbcustBranch, squot.getCinfo().getCount());
					Console.log("end of sales lead in switchToSalesQuotationScreen");
					
					form.toggleAppHeaderBarMenu();
					glassPanel.hide();
				}
			};
			t.schedule(3000);
			
		}
		
		if (presenterName.equals("Sales Order")) {
			
			final Lead model=(Lead) model1;
			
			AppMemory.getAppMemory().currentScreen = Screen.SALESORDER;
			LoginPresenter.currentModule="Sales";
			LoginPresenter.currentDocumentName="Sales Order";
			glassPanel.show();
			final SalesOrderForm form = new SalesOrderForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			

			final SalesOrder squot = new SalesOrder();
			squot.setBranch(model.getBranch());
			squot.setEmployee(model.getEmployee());
			squot.setCinfo(model.getPersonInfo());
			squot.setStatus(SalesOrder.CREATED);
			squot.setLeadCount(model.getCount());

			Timer t = new Timer() {
				@Override
				public void run() {
					
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					SalesOrderPresenter leadPresenter=new SalesOrderPresenter(form,new SalesOrder());
					presenter = leadPresenter;
					
					
					form.setToNewState();
					form.updateView(squot);
					presenter.setModel(squot);
					form.setPopUpAppMenubar(true);
					form.setHideNavigationButtons(hideNavigationButtons);//03-10-2022	
					
					MyQuerry querry=new MyQuerry();
					Vector<Filter> filtervec=new Vector<Filter>();
					Filter tempfilter=new Filter();
					tempfilter=new Filter();
					tempfilter.setQuerryString("count");
					tempfilter.setIntValue(model.getPersonInfo().getCount());
					filtervec.add(tempfilter);
					tempfilter=new Filter();
					tempfilter.setQuerryString("companyId");
					tempfilter.setLongValue(model.getCompanyId());
					filtervec.add(tempfilter);
					querry.setFilters(filtervec);
					querry.setQuerryObject(new Customer());

					genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected error occurred!");
							glassPanel.hide();
						}

						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							Console.log("Size Of Result In SalesQuotation Presenter"+ result.size());
							List<SalesLineItem> leadProductsLis =model.getLeadProducts();
							Console.log("Size For Lis"+ leadProductsLis.size());
							for (SuperModel model : result) {
								Customer custentity = (Customer) model;
								form.getCbcformlis().setEnabled(true);
								for (SalesLineItem saleObj : leadProductsLis) {
									saleObj.setQuantity(saleObj.getQty());
									
								}
								if (leadProductsLis.size() != 0) {
									Console.log("Setting product on sales order screen");
									form.getSalesorderlineitemtable().setValue(leadProductsLis);
								}
								form.getAcshippingcomposite().setValue(custentity.getSecondaryAdress());
								form.cust = custentity;
							}
							
							
						}
					});

					form.getTbQuotatinId().setText("");
					form.getTbContractId().setText("");
					form.getDbdeliverydate().setValue(null);
					form.getAcshippingcomposite().setEnable(false);
					form.getPersonInfoComposite().setEnabled(false);
					if (model.getRoundOffAmt() != 0) {
						form.getTbroundoffAmt().setValue(model.getRoundOffAmt() + "");
					}
					AppUtility.makeCustomerBranchLive(form.oblCustomerBranch, squot.getCinfo().getCount());
					Console.log("end of sales lead in switchToSalesOrderScreen");
					
					form.toggleAppHeaderBarMenu();
					glassPanel.hide();
				}
			};
			t.schedule(3000);
			
		}
		
		if (presenterName.equals("Quick Sales Order")) {
			
			final Lead model=(Lead) model1;
			
			AppMemory.getAppMemory().currentScreen = Screen.QUICKSALESORDER;
			LoginPresenter.currentModule="Sales";
			LoginPresenter.currentDocumentName="Quick Order";
			glassPanel.show();
			final QuickSalesOrderForm form = new QuickSalesOrderForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			
			
			final SalesOrder squot = new SalesOrder();
			squot.setBranch(model.getBranch());
			squot.setEmployee(model.getEmployee());
			squot.setCinfo(model.getPersonInfo());
			squot.setStatus(SalesOrder.CREATED);
			squot.setLeadCount(model.getCount());

			Timer t = new Timer() {
				@Override
				public void run() {
					
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					QuickSalesOrderPresenter leadPresenter=new QuickSalesOrderPresenter(form,new SalesOrder());
					presenter = leadPresenter;
					
					form.setToNewState();
					form.updateView(squot);
					presenter.setModel(squot);
					form.setPopUpAppMenubar(true);
					form.setHideNavigationButtons(hideNavigationButtons);//03-10-2022	
					
					MyQuerry querry=new MyQuerry();
					Vector<Filter> filtervec=new Vector<Filter>();
					Filter tempfilter=new Filter();
					tempfilter=new Filter();
					tempfilter.setQuerryString("count");
					tempfilter.setIntValue(model.getPersonInfo().getCount());
					filtervec.add(tempfilter);
					tempfilter=new Filter();
					tempfilter.setQuerryString("companyId");
					tempfilter.setLongValue(model.getCompanyId());
					filtervec.add(tempfilter);
					querry.setFilters(filtervec);
					querry.setQuerryObject(new Customer());

					genasync.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							form.showDialogMessage("An Unexpected error occurred!");
							glassPanel.hide();
						}

						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							Console.log("Size Of Result In SalesQuotation Presenter"+ result.size());
							List<SalesLineItem> leadProductsLis =model.getLeadProducts();
							Console.log("Size For Lis"+ leadProductsLis.size());
							for (SuperModel model : result) {
								Customer custentity = (Customer) model;
								form.getCbcformlis().setEnabled(true);
								for (SalesLineItem saleObj : leadProductsLis) {
									saleObj.setQuantity(saleObj.getQty());
								}
								if (leadProductsLis.size() != 0) {
									form.getSalesorderlineitemtable().setValue(leadProductsLis);
								}
								form.getAcshippingcomposite().setValue(custentity.getSecondaryAdress());
								form.cust = custentity;
							}
							
							
						}
					});

					form.getTbQuotatinId().setText("");
					form.getTbContractId().setText("");
					form.getDbdeliverydate().setValue(null);
					form.getAcshippingcomposite().setEnable(false);
					form.getPersonInfoComposite().setEnabled(false);
//					if (model.getRoundOffAmt() != 0) {
//						form.gettb.setValue(model.getRoundOffAmt() + "");
//					}
//					AppUtility.makeCustomerBranchLive(form., squot.getCinfo().getCount());
					Console.log("end of sales lead in switchToQuickSalesOrderScreen");
					
					form.toggleAppHeaderBarMenu();
					glassPanel.hide();
				}
			};
			t.schedule(3000);
			
		}
		
		//16-12-2021 by Ashwini Patil
		if(presenterName.equals("Create Work Order From SalesOrder")){
			final SalesOrder model=(SalesOrder) model1;
			
			AppMemory.getAppMemory().currentScreen = Screen.WORKORDER;
			LoginPresenter.currentModule="Sales";
			LoginPresenter.currentDocumentName="Work Order";
			glassPanel.show();
			final WorkOrderForm form = new WorkOrderForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);

			final WorkOrder wo = new WorkOrder();
			
			wo.setOrderId(model.getCount());
			wo.setOrderDate(model.getSalesOrderDate());
			if(model.getRefNo()!=null)
				wo.setRefNum(model.getRefNo()+"");
			if(model.getReferenceDate()!=null)
				wo.setRefDate(model.getReferenceDate());
			wo.setOrderDeliveryDate(model.getDeliveryDate());
			wo.setBranch(model.getBranch());
			wo.setSalesPerson(model.getEmployee());
			wo.setApproverName(model.getApproverName());
			
			List<SalesLineItem> productList = model.getItems();
			wo.setBomTable(productList);

			form.showWaitSymbol();
			Timer t = new Timer() {
				@Override
				public void run() {
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					WorkOrderPresenter woPresenter=new WorkOrderPresenter(form,new WorkOrder());
					presenter = woPresenter;
					form.setToNewState();
					form.updateView(wo);
					presenter.setModel(wo);
					form.setPopUpAppMenubar(true);
					form.setHideNavigationButtons(hideNavigationButtons);//03-10-2022	
					form.getIbOrderId().setEnabled(false);
					form.getDbOrderDate().setEnabled(false);
					form.getOblSalesPerson().setEnabled(false);
					form.getOblWoBranch().setEnabled(false);
					form.getOblSalesPerson().setEnabled(false);
					form.getUpload().getCancel().setVisible(false);
					form.getOblWoApproverName().setEnabled(false);
					form.getProdInfoComp().setEnabled(false);
					if(model.getRefNo()!=null)
						form.getTbRefNumId().setEnabled(false);
					if(model.getReferenceDate()!=null)
						form.getDbRefDate().setEnabled(false);
					form.getLbComProdId().clear();
					form.getLbComProdId().addItem("--SELECT--");
					for(int i=0;i<model.getItems().size();i++){
						form.flage=false;
						form.getBillOfProductMaterial(model.getItems().get(i).getPrduct().getCount(), model.getItems().get(i).getProductName(),model.getItems().get(i).getQty(),false);
						form.getLbComProdId().addItem(model.getItems().get(i).getPrduct().getCount()+"");
					}
					form.hideWaitSymbol();
					form.toggleAppHeaderBarMenu();
					glassPanel.hide();
				}
			};
			t.schedule(3000);						
		}
		if(presenterName.equals("Create AMC")){
			final SalesOrder model=(SalesOrder) model1;
			
			
			MyQuerry querry = new MyQuerry();
			Vector<Filter> filtervec = new Vector<Filter>();
			Filter filter = new Filter();
			
			filter.setStringValue(model.getCount()+"");
			filter.setQuerryString("refNo");
			filtervec.add(filter);
			
			filter = new Filter();
			filter.setLongValue(model.getCompanyId());
			filter.setQuerryString("companyId");
			filtervec.add(filter);
			
			querry.setFilters(filtervec);
			querry.setQuerryObject(new Contract());
			
			genasync.getSearchResult(querry, new  AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					System.out.println("Result====="+result.size());
					
					if(result.size()>0){
						System.out.println("result size >0");
						salesOrderModel=model;
						panel=new PopupPanel(true);
						panel.add(conditionPopupforCreateAMC);
						panel.setGlassEnabled(true);
						panel.show();
						panel.center();
						glassPanel.hide();
					}
					
					if(result.size()==0){
						reactOnCreateAMC(model);
					}
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			});		
						
		}
		if(presenterName.equals("View AMC")){
			final SalesOrder model=(SalesOrder) model1;
			
			AppMemory.getAppMemory().currentScreen = Screen.CONTRACT;
			LoginPresenter.currentModule="Sales";
			LoginPresenter.currentDocumentName="Contract";
			glassPanel.show();
			final ContractForm form = new ContractForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			

			MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("refNo");
			filter.setStringValue(model.getCount()+"");
			temp.add(filter);
			
			querry.setFilters(temp);
			querry.setQuerryObject(new Contract());
			form.showWaitSymbol();
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					if(result.size()==0){
						form.showDialogMessage("No AMC document found.");
						glassPanel.hide();
						return;
					}
					if(result.size()>0){
						final Contract contract=(Contract) result.get(0);
						
						Timer timer=new Timer() {
							@Override
							public void run() {
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();
								
								ContractPresenter contractPresenter=new ContractPresenter(form,new Contract());
								presenter = contractPresenter;
								form.updateView(contract);
								form.setPopUpAppMenubar(true);
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022
								form.setToViewState();
								presenter.setModel(contract);
								
								form.toggleAppHeaderBarMenu();
								glassPanel.hide();
							}
						};
						timer.schedule(1000);
					}
				}
			});
		
						
		}
		if(presenterName.equals("Create Purchase Order")){
			final SalesOrder model=(SalesOrder) model1;
			
			AppMemory.getAppMemory().currentScreen = Screen.PURCHASEORDER;
			LoginPresenter.currentModule="Sales";
			LoginPresenter.currentDocumentName="Purchase Order";
			glassPanel.show();
			final PurchaseOrderForm form = new PurchaseOrderForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);

			PurchaseOrderPresenter.vendorState="";
			final PurchaseOrder po = new PurchaseOrder();
			
			po.setRefOrderNO(model.getCount()+"");
			po.setReferenceDate(model.getSalesOrderDate());
			po.setDeliveryDate(model.getDeliveryDate());
			po.setBranch(model.getBranch());
			po.setEmployee(model.getEmployee());
			po.setApproverName(model.getApproverName());
			form.ibLetterOfIntentID.setValue(null);
			form.ibreqforQuotation.setValue(null);
			form.ibPOId.setValue(null);
			
//			form.loadWareHouseList(po.getBranch());
			
			List<SalesLineItem> itemList = model.getItems();
			final List<ProductDetailsPO> poProductList = new ArrayList<ProductDetailsPO>();

			for (SalesLineItem temp : itemList) {
				ProductDetailsPO purch = new ProductDetailsPO();
				purch.setProdDate(new Date());
				purch.setProductID(temp.getPrduct().getCount());
				purch.setProductName(temp.getProductName());
				purch.setProductCode(temp.getProductCode());
				purch.setProductCategory(temp.getProductCategory());
				purch.setProdPrice(temp.getPrduct().getPrice());
				purch.setUnitOfmeasurement(temp.getPrduct().getUnitOfMeasurement());
				purch.setProductQuantity(temp.getQty());
				
				purch.setTax(temp.getServiceTax().getPercentage());
				purch.setVat(temp.getVatTax().getPercentage());
				
				purch.setPrduct(temp.getPrduct());
//				purch.setWarehouseName(temp.getWarehouseName());
//				purch.setAvailableStock(temp.get);			
//				purch.setWarehouseAddress(temp.getWarehouseAddress());
			
				/**
				 * @author Anil
				 * @since 21-07-2020
				 * Copying margin detail
				 */
				if(model.getOtherChargesMargins()!=null&&model.getOtherChargesMargins().size()!=0){
					for(OtherChargesMargin obj:model.getOtherChargesMargins()){
						if(temp.getPrduct().getCount()==obj.getProductId()&&temp.getProductSrNo()==obj.getProductSrNo()){
							purch.setOtherCharges(obj.getOtherCharges());
							break;
						}
					}
				}
				poProductList.add(purch);
			}
//			po.setProductDetails(poProductList);
			
			/**
			 * @author Anil
			 * @since 21-07-2020
			 * Copying margin detail
			 */
			po.setVendorMargins(model.getVendorMargins());

			Timer t = new Timer() {
				@Override
				public void run() {
					Console.log("Inside timer...");
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					PurchaseOrderPresenter poPresenter=new PurchaseOrderPresenter(form,new PurchaseOrder());
					presenter = poPresenter;
					
					form.setToNewState();
					form.updateView(po);
					presenter.setModel(po);
					form.setPopUpAppMenubar(true);
					form.setHideNavigationButtons(hideNavigationButtons);//03-10-2022	
										
					po.setProductDetails(poProductList);
					form.productTablePO.setValue(poProductList);
					form.addCommonVendorToVendorTable(poProductList);
					form.dbPODate.setValue(new Date());
					Double total = form.productTablePO.calculateTotal();
					form.getCbadds().setValue(false);
					form.dbTotal.setValue(total);
					form.setAllTable();
					form.getPurchaseRequisiteNo().setEnabled(false);
					form.getIbreqforQuotation().setEnabled(false);
					form.getIbLetterOfIntentID().setEnabled(false);
					form.getBranch().setEnabled(true);
					form.getOblEmployee().setEnabled(true);
					form.getTbApporverName().setEnabled(true);
					form.getDbExpdeliverydate().setEnabled(true);
					form.getTbpoName().setEnabled(true);
					form.getDbExpectedResposeDate().setEnabled(true);
					form.getHeader().setEnabled(true);
					form.getFooter().setEnabled(true);
					if(form.isWarehouseSelected()){
						form.getDeliveryadd().clear();
						form.getDeliveryadd().setEnable(false);
						form.getCbadds().setEnabled(false);
					}
					form.prodInfoComposite.setEnable(false);
					form.addproducts.setEnabled(false);	
					
					form.getIbRefOrderNo().setEnabled(false);
					form.getDbrefDate().setEnabled(false);
					form.toggleAppHeaderBarMenu();
					glassPanel.hide();
				}
			};
			t.schedule(3000);								
		}
		if(presenterName.equals("View Purchase Order")){
			final SalesOrder model=(SalesOrder) model1;
			
			AppMemory.getAppMemory().currentScreen = Screen.PURCHASEORDER;
			LoginPresenter.currentModule="Sales";
			LoginPresenter.currentDocumentName="Purchase Order";
			glassPanel.show();
			final PurchaseOrderForm form = new PurchaseOrderForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			

			MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("refOrderNO");
			filter.setStringValue(model.getCount()+"");
			temp.add(filter);
			
			querry.setFilters(temp);
			querry.setQuerryObject(new PurchaseOrder());
			form.showWaitSymbol();
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					if(result.size()==0){
						form.showDialogMessage("No PO document found.");
						glassPanel.hide();
						return;
					}
					if(result.size()>0){
						final PurchaseOrder contract=(PurchaseOrder) result.get(0);
						Timer timer=new Timer() {
							@Override
							public void run() {
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();
								
								PurchaseOrderPresenter poPresenter=new PurchaseOrderPresenter(form,new PurchaseOrder());
								presenter = poPresenter;
								
								form.updateView(contract);
								form.setPopUpAppMenubar(true);
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022
								form.setToViewState();
								presenter.setModel(contract);
								
								
								form.toggleAppHeaderBarMenu();
								glassPanel.hide();
								
							}
						};
						timer.schedule(1000);
					}
				}
			});
		
						
		}
		if(presenterName.equals("View Sales Bill")){
			final SalesOrder model=(SalesOrder) model1;
			
			AppMemory.getAppMemory().currentScreen = Screen.BILLINGDETAILS;
			LoginPresenter.currentModule="Sales";
			LoginPresenter.currentDocumentName="View Bill";
			glassPanel.show();
			final BillingDetailsForm form = new BillingDetailsForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			

			final MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("contractCount");
			filter.setIntValue(model.getCount());
			temp.add(filter);
			
			filter=new Filter();
			filter.setQuerryString("typeOfOrder");
			filter.setStringValue(AppConstants.ORDERTYPESALES);
			temp.add(filter);
			
			querry.setFilters(temp);
			querry.setQuerryObject(new BillingDocument());
			form.showWaitSymbol();
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					if(result.size()==0){
						form.showDialogMessage("No billing document found.");
						glassPanel.hide();
						return;
					}
					if(result.size()==1){
						final BillingDocument billDocument=(BillingDocument) result.get(0);
						Timer timer=new Timer() {
							@Override
							public void run() {
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();
								
								BillingDetailsPresenter billPresenter=new BillingDetailsPresenter(form,new BillingDocument());
								presenter = billPresenter;
								
								form.updateView(billDocument);
								form.setPopUpAppMenubar(true);
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022								
								form.setToViewState();
								presenter.setModel(billDocument);
								
								
								form.toggleAppHeaderBarMenu();
								glassPanel.hide();
							}
						};
						timer.schedule(1000);
					}else{
						//BillingListPresenter.initalize(querry);
						Console.log("calling Billing list");
						AppMemory.getAppMemory().currentScreen = Screen.BILLINGLIST;
						LoginPresenter.currentModule="Sales";
						LoginPresenter.currentDocumentName="Billing List";
						glassPanel.show();
						BillingListTable table=new BillingListTable();
						final BillingListForm form=new BillingListForm(table);
						/**
						 * @author Anil
						 * @since 07-01-2021
						 * Showing process level menus
						 */
						if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
							form.setAction(false);
							form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
							form.processLevelBar.setVisibleFalse(false);
							docVerticalPanel.add(form.processLevelBar.content);
						}
						docVerticalPanel.add(form.content);
						Timer timer = new Timer() {
							@Override
							public void run() {
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();

								
								SalesOrder serviceEntity = (SalesOrder) model;
//								form.updateView(serviceEntity);
																
								BillingListPresenter billingListpresenter=new BillingListPresenter(form, new BillingDocument(),querry);
								presenter = billingListpresenter;
								presenter.setModel(serviceEntity);
								
								form.setPopUpAppMenubar(true);
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022

								form.setToViewState();
								form.toggleAppHeaderBarMenu();
								glassPanel.hide();
							}
						};
						timer.schedule(1000);
					
					}
					
				}
			});					
						
		}
		if(presenterName.equals("View Delivery Note")){
			final SalesOrder model=(SalesOrder) model1;
			
			AppMemory.getAppMemory().currentScreen = Screen.DELIVERYNOTE;
			LoginPresenter.currentModule="Sales";
			LoginPresenter.currentDocumentName="Delivery Note";
			glassPanel.show();
			final DeliveryNoteForm form = new DeliveryNoteForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			
			MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("salesOrderCount");
			filter.setIntValue(model.getCount());
			temp.add(filter);
			
			querry.setFilters(temp);
			querry.setQuerryObject(new DeliveryNote());
			form.showWaitSymbol();
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					if(result.size()==0){
						form.showDialogMessage("Delivery note not found.");
						glassPanel.hide();
						return;
					}
					if(result.size()>0){
						final DeliveryNote entity=(DeliveryNote) result.get(0);
						Timer timer=new Timer() {
							@Override
							public void run() {
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();
								
								DeliveryNotePresenter dnPresenter=new DeliveryNotePresenter(form,new DeliveryNote());
								presenter = dnPresenter;
								form.updateView(entity);
								form.setPopUpAppMenubar(true);
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022																
								form.setToViewState();
								presenter.setModel(entity);
								
								form.toggleAppHeaderBarMenu();
								glassPanel.hide();								
							}
						};
						timer.schedule(3000);
					}
				}
			});
								
		}
		
		
		if (presenterName.equals("Service Quotation")) {
			
			final Lead model=(Lead) model1;

			AppMemory.getAppMemory().currentScreen = Screen.QUOTATION;
			LoginPresenter.currentModule="Service";
			LoginPresenter.currentDocumentName="Service Quotation";
			glassPanel.show();
			final QuotationForm form = new QuotationForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			
			final Quotation quot=new Quotation();
			quot.setBranch(model.getBranch());
			quot.setEmployee(model.getEmployee());
			quot.setCinfo(model.getPersonInfo());
			quot.setStatus(Quotation.CREATED);
			quot.setLeadCount(model.getCount());
			/***Date 12-9-2019 by Amol map type ,grop and category**/
			quot.setGroup(model.getGroup());
			quot.setCategory(model.getCategory());
			quot.setType(model.getType());
			
			/**
			 * Date : 24-08-2017 BY ANIL
			 * For loading customer branches if any
			 */
			form.checkCustomerBranch(model.getPersonInfo().getCount());
			/**
			 * End
			 */
			Timer t = new Timer() {
				@Override
				public void run() {
					//5 lines written below are not part of LeadPresenter. Added as per other if blocks.
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					QuotationPresenter leadPresenter=new QuotationPresenter(form,new Quotation());
					presenter = leadPresenter;
					
					form.setToNewState();
					form.updateView(quot);
					presenter.setModel(quot); //added as per other if blocks
					form.setPopUpAppMenubar(true);//added as per other if blocks
					form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022	
					//below code added as per other if blocks
					MyQuerry querry=new MyQuerry();
					Vector<Filter> filtervec=new Vector<Filter>();
					Filter tempfilter=new Filter();
					tempfilter=new Filter();
					tempfilter.setQuerryString("count");
					tempfilter.setIntValue(model.getPersonInfo().getCount());
					filtervec.add(tempfilter);
					tempfilter=new Filter();
					tempfilter.setQuerryString("companyId");
					tempfilter.setLongValue(model.getCompanyId());
					filtervec.add(tempfilter);
					querry.setFilters(filtervec);
					querry.setQuerryObject(new Customer());
					
					/**
					 * Date : 24-08-2017 BY ANIL
					 * For loading customer branches if any
					 */
					form.checkCustomerBranch(model.getPersonInfo().getCount());
					/**
					 * End
					 */
					
					form.checkCustomerBranchFromLead(form.getPersonInfoComposite().getIdValue());
			    	  List<SalesLineItem> lineItemLis=model.getLeadProducts();
//			    	  if(statusValue==true)
//			    	  {
//			    		  form.getOlbQuotationCategory().setValue(model.getCategory());
//			    		  form.getOlbQuotationGroup().setValue(model.getGroup());
//			    		  form.getOlbQuotationType().setValue(model.getType());
//			    		  form.getOlbcPriority().setValue(model.getPriority());
//			    	  }
			    	  QuotationPresenter.custcount=form.getPersonInfoComposite().getIdValue();

						/**
						 * Date 27/10/2017 added by komal to set lead quantity into
						 * area
						 **/
						for(SalesLineItem saleObj : lineItemLis) {
							if(saleObj.getQty()!=0){
								saleObj.setArea(saleObj.getQty() + "");
							}else{
								saleObj.setArea("NA");
							}
							saleObj.setTotalAmount(saleObj.getTotalAmount());
							
							/**
							 * @author Anil
							 * @since 12-06-2020
							 * Storing customer branch details in hashmap
							 */
							ArrayList<BranchWiseScheduling> branchlist = AppUtility.getCustomerBranchSchedulinglistInfo(form.customerbranchlist,model.getBranch(),"",0);
							Integer prodSrNo = saleObj.getProductSrNo();
							HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
							customerBranchlist.put(prodSrNo, branchlist);
							saleObj.setCustomerBranchSchedulingInfo(customerBranchlist);
						}
			    	  
			    	// vijay added for set quantity 1 becs we are not getting quantity in lead
			    	  for(int i=0;i<lineItemLis.size();i++){
			    		  lineItemLis.get(i).setQuantity(1.0);
			    	  }
			    	  
			    	  if(lineItemLis.size()!=0){
			    		  form.getSaleslineitemquotationtable().setValue(lineItemLis);
			    	  }
			    	 
//			    	  if(form.getSaleslineitemquotationtable().getDataprovider().getList().size() == 0){
//							
//							 List<ServiceSchedule> popuplist=reactfordefaultTable(lineItemLis,branch,form.customerbranchlist);
//							 form.getServiceScheduleTable().getDataprovider().setList(popuplist);
//							 
//					  }else{
//							 List<ServiceSchedule> privlist=form.getServiceScheduleTable().getDataprovider().getList();
//									 
//							 List<ServiceSchedule> popuplist=reactfordefaultTable(lineItemLis,branch,form.customerbranchlist);
//							 List<ServiceSchedule> popuplist1=new ArrayList<ServiceSchedule>();
//							 
//							 popuplist1.addAll(privlist);
//							 popuplist1.addAll(popuplist);
//							 form.getServiceScheduleTable().getDataprovider().setList(popuplist1);
//							 
//					  }
			    	  
			    	  
			    	  // vijay
			    	  form.getProdTaxTable().setValue(model.getProductTaxes());
			    	  
			    	  form.getTbQuotatinId().setText("");
			    	  form.getTbContractId().setText("");
			    	  form.getPersonInfoComposite().setEnabled(false);
//			    	  if(LeadPresenter.this.form.isSalesPersonRestrictionFlag())
//			    	  {
//			    		  form.getOlbeSalesPerson().setEnabled(false);
//			    	  }
//			    	  else
//			    	  {
//			    		  form.getOlbeSalesPerson().setEnabled(true);
//			    	  }
			    	  /** Date 05-09-2017 added by vijay for as per new code discount and round off ****/
			    	  form.getDbDiscountAmt().setValue(model.getDiscountAmt());
			    	  form.getTbroundOffAmt().setValue(model.getRoundOffAmt()+"");
			    	  
			    	  if(model.getNumberRange()!=null && !model.getNumberRange().equals("")){
				    	  form.getOlbcNumberRange().setValue(model.getNumberRange());
			    	  }
			    	  form.hideWaitSymbol();
					
					//as per other if blocks
					form.toggleAppHeaderBarMenu();
					glassPanel.hide();
					Console.log("End of CreateQuotation navigateScreen ");
				}
			};
			t.schedule(3000);			
		}
		if (presenterName.equals("Service Order")) {
			
			final Lead model=(Lead) model1;

			AppMemory.getAppMemory().currentScreen = Screen.CONTRACT;
			LoginPresenter.currentModule="Service";
			LoginPresenter.currentDocumentName="Service Order";
			glassPanel.show();
			final ContractForm form = new ContractForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			
			final Contract contrct=new Contract();
			contrct.setBranch(model.getBranch());
			contrct.setEmployee(model.getEmployee());
			contrct.setCinfo(model.getPersonInfo());
			contrct.setStatus(Contract.CREATED);
			contrct.setLeadCount(model.getCount());
			/***Date 12-9-2019 by Amol map type ,grop and category**/
			contrct.setGroup(model.getGroup());
			contrct.setCategory(model.getCategory());
			contrct.setType(model.getType());
			
			/**
			 * Date : 24-08-2017 BY ANIL
			 * For loading customer branches if any
			 */
			form.checkCustomerBranch(model.getPersonInfo().getCount());
			/**
			 * End
			 */
			Timer t = new Timer() {
				@Override
				public void run() {
					//5 lines written below are not part of LeadPresenter. Added as per other if blocks.
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					ContractPresenter leadPresenter=new ContractPresenter(form,new Contract());
					presenter = leadPresenter;
					
					form.setToNewState();
					form.updateView(contrct);
					presenter.setModel(contrct); //added as per other if blocks
					form.setPopUpAppMenubar(true);//added as per other if blocks
					form.setHideNavigationButtons(hideNavigationButtons);//03-10-2022	
					//below code added as per other if blocks
					MyQuerry querry=new MyQuerry();
					Vector<Filter> filtervec=new Vector<Filter>();
					Filter tempfilter=new Filter();
					tempfilter=new Filter();
					tempfilter.setQuerryString("count");
					tempfilter.setIntValue(model.getPersonInfo().getCount());
					filtervec.add(tempfilter);
					tempfilter=new Filter();
					tempfilter.setQuerryString("companyId");
					tempfilter.setLongValue(model.getCompanyId());
					filtervec.add(tempfilter);
					querry.setFilters(filtervec);
					querry.setQuerryObject(new Customer());
					
					/**
					 * Date : 24-08-2017 BY ANIL
					 * For loading customer branches if any
					 */
					form.checkCustomerBranch(model.getPersonInfo().getCount());
					/**
					 * End
					 */
					
//					form.checkCustomerBranchFromLead(form.getPersonInfoComposite().getIdValue());
			    	
					List<SalesLineItem> lineItemLis=model.getLeadProducts();
//			    	  if(statusValue==true)
//			    	  {
//			    		  form.getOlbQuotationCategory().setValue(model.getCategory());
//			    		  form.getOlbQuotationGroup().setValue(model.getGroup());
//			    		  form.getOlbQuotationType().setValue(model.getType());
//			    		  form.getOlbcPriority().setValue(model.getPriority());
//			    	  }
			    	  ContractPresenter.custcount=form.getPersonInfoComposite().getIdValue();

						/**
						 * Date 27/10/2017 added by komal to set lead quantity into
						 * area
						 **/
						for(SalesLineItem saleObj : lineItemLis) {
							if(saleObj.getQty()!=0){
								saleObj.setArea(saleObj.getQty() + "");
							}else{
								saleObj.setArea("NA");
							}
							saleObj.setTotalAmount(saleObj.getTotalAmount());
							
							/**
							 * @author Anil
							 * @since 12-06-2020
							 * Storing customer branch details in hashmap
							 */
							ArrayList<BranchWiseScheduling> branchlist = AppUtility.getCustomerBranchSchedulinglistInfo(form.customerbranchlist,model.getBranch(),"",0);
							Integer prodSrNo = saleObj.getProductSrNo();
							HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
							customerBranchlist.put(prodSrNo, branchlist);
							saleObj.setCustomerBranchSchedulingInfo(customerBranchlist);
						}
			    	  
			    	// vijay added for set quantity 1 becs we are not getting quantity in lead
			    	  for(int i=0;i<lineItemLis.size();i++){
			    		  lineItemLis.get(i).setQuantity(1.0);
			    	  }
			    	  
			    	  if(lineItemLis.size()!=0){
			    		  form.getSaleslineitemtable().setValue(lineItemLis);
			    	  }
			    	 
//			    	  if(form.getSaleslineitemquotationtable().getDataprovider().getList().size() == 0){
//							
//							 List<ServiceSchedule> popuplist=reactfordefaultTable(lineItemLis,branch,form.customerbranchlist);
//							 form.getServiceScheduleTable().getDataprovider().setList(popuplist);
//							 
//					  }else{
//							 List<ServiceSchedule> privlist=form.getServiceScheduleTable().getDataprovider().getList();
//									 
//							 List<ServiceSchedule> popuplist=reactfordefaultTable(lineItemLis,branch,form.customerbranchlist);
//							 List<ServiceSchedule> popuplist1=new ArrayList<ServiceSchedule>();
//							 
//							 popuplist1.addAll(privlist);
//							 popuplist1.addAll(popuplist);
//							 form.getServiceScheduleTable().getDataprovider().setList(popuplist1);
//							 
//					  }
			    	  
			    	  
			    	  // vijay
			    	  form.getProdTaxTable().setValue(model.getProductTaxes());
			    	  
			    	  form.getTbQuotatinId().setText("");
			    	  form.getTbContractId().setText("");
			    	  form.getPersonInfoComposite().setEnabled(false);
//			    	  if(LeadPresenter.this.form.isSalesPersonRestrictionFlag())
//			    	  {
//			    		  form.getOlbeSalesPerson().setEnabled(false);
//			    	  }
//			    	  else
//			    	  {
//			    		  form.getOlbeSalesPerson().setEnabled(true);
//			    	  }
			    	  /** Date 05-09-2017 added by vijay for as per new code discount and round off ****/
			    	  form.getDbDiscountAmt().setValue(model.getDiscountAmt());
			    	  form.getTbroundOffAmt().setValue(model.getRoundOffAmt()+"");
			    	  
			    	  if(model.getNumberRange()!=null && !model.getNumberRange().equals("")){
				    	  form.getOlbcNumberRange().setValue(model.getNumberRange());
			    	  }
			    	  form.hideWaitSymbol();
					
					//as per other if blocks
					form.toggleAppHeaderBarMenu();
					glassPanel.hide();
				}
			};
			t.schedule(3000);
			
		}
		
		if (presenterName.equals("Quick Service Order")) {
			
			final Lead model=(Lead) model1;

			AppMemory.getAppMemory().currentScreen = Screen.QUICKCONTRACT;
			LoginPresenter.currentModule="Service";
			LoginPresenter.currentDocumentName="Quick Service Order";
			glassPanel.show();
			final QuickContractForm form = new QuickContractForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			
			final Contract contrct=new Contract();
			contrct.setBranch(model.getBranch());
			contrct.setEmployee(model.getEmployee());
			contrct.setCinfo(model.getPersonInfo());
			contrct.setStatus(Contract.CREATED);
			contrct.setLeadCount(model.getCount());
			/***Date 12-9-2019 by Amol map type ,grop and category**/
			contrct.setGroup(model.getGroup());
			contrct.setCategory(model.getCategory());
			contrct.setType(model.getType());
			
			/**
			 * Date : 24-08-2017 BY ANIL
			 * For loading customer branches if any
			 */
			form.checkCustomerBranch(model.getPersonInfo().getCount());
			/**
			 * End
			 */
			Timer t = new Timer() {
				@Override
				public void run() {
					//5 lines written below are not part of LeadPresenter. Added as per other if blocks.
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					QuickContractPresentor leadPresenter=new QuickContractPresentor(form,new Contract());
					presenter = leadPresenter;
					
					form.setToNewState();
					form.updateView(contrct);
					presenter.setModel(contrct); //added as per other if blocks
					form.setPopUpAppMenubar(true);//added as per other if blocks
					form.setHideNavigationButtons(hideNavigationButtons);//03-10-2022	
					//below code added as per other if blocks
					MyQuerry querry=new MyQuerry();
					Vector<Filter> filtervec=new Vector<Filter>();
					Filter tempfilter=new Filter();
					tempfilter=new Filter();
					tempfilter.setQuerryString("count");
					tempfilter.setIntValue(model.getPersonInfo().getCount());
					filtervec.add(tempfilter);
					tempfilter=new Filter();
					tempfilter.setQuerryString("companyId");
					tempfilter.setLongValue(model.getCompanyId());
					filtervec.add(tempfilter);
					querry.setFilters(filtervec);
					querry.setQuerryObject(new Customer());
					
					/**
					 * Date : 24-08-2017 BY ANIL
					 * For loading customer branches if any
					 */
					form.checkCustomerBranch(model.getPersonInfo().getCount());
					/**
					 * End
					 */
					
//					form.checkCustomerBranchFromLead(form.getPersonInfoComposite().getIdValue());
			    	
					List<SalesLineItem> lineItemLis=model.getLeadProducts();
//			    	  if(statusValue==true)
//			    	  {
//			    		  form.getOlbQuotationCategory().setValue(model.getCategory());
//			    		  form.getOlbQuotationGroup().setValue(model.getGroup());
//			    		  form.getOlbQuotationType().setValue(model.getType());
//			    		  form.getOlbcPriority().setValue(model.getPriority());
//			    	  }
			    	  QuickContractPresentor.custcount=form.getPersonInfoComposite().getIdValue();

						/**
						 * Date 27/10/2017 added by komal to set lead quantity into
						 * area
						 **/
						for(SalesLineItem saleObj : lineItemLis) {
							if(saleObj.getQty()!=0){
								saleObj.setArea(saleObj.getQty() + "");
							}else{
								saleObj.setArea("NA");
							}
							saleObj.setTotalAmount(saleObj.getTotalAmount());
							
							/**
							 * @author Anil
							 * @since 12-06-2020
							 * Storing customer branch details in hashmap
							 */
							ArrayList<BranchWiseScheduling> branchlist = AppUtility.getCustomerBranchSchedulinglistInfo(form.customerbranchlist,model.getBranch(),"",0);
							Integer prodSrNo = saleObj.getProductSrNo();
							HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
							customerBranchlist.put(prodSrNo, branchlist);
							saleObj.setCustomerBranchSchedulingInfo(customerBranchlist);
						}
			    	  
			    	// vijay added for set quantity 1 becs we are not getting quantity in lead
			    	  for(int i=0;i<lineItemLis.size();i++){
			    		  lineItemLis.get(i).setQuantity(1.0);
			    	  }
			    	  
			    	  if(lineItemLis.size()!=0){
			    		  form.getSaleslineitemtable().setValue(lineItemLis);
			    	  }
			    	 
//			    	  if(form.getSaleslineitemquotationtable().getDataprovider().getList().size() == 0){
//							
//							 List<ServiceSchedule> popuplist=reactfordefaultTable(lineItemLis,branch,form.customerbranchlist);
//							 form.getServiceScheduleTable().getDataprovider().setList(popuplist);
//							 
//					  }else{
//							 List<ServiceSchedule> privlist=form.getServiceScheduleTable().getDataprovider().getList();
//									 
//							 List<ServiceSchedule> popuplist=reactfordefaultTable(lineItemLis,branch,form.customerbranchlist);
//							 List<ServiceSchedule> popuplist1=new ArrayList<ServiceSchedule>();
//							 
//							 popuplist1.addAll(privlist);
//							 popuplist1.addAll(popuplist);
//							 form.getServiceScheduleTable().getDataprovider().setList(popuplist1);
//							 
//					  }
			    	  
			    	  
			    	  // vijay
			    	  form.getProdTaxTable().setValue(model.getProductTaxes());
			    	  
			    	  form.getTbQuotatinId().setText("");
			    	  form.getTbContractId().setText("");
			    	  form.getPersonInfoComposite().setEnabled(false);
//			    	  if(LeadPresenter.this.form.isSalesPersonRestrictionFlag())
//			    	  {
//			    		  form.getOlbeSalesPerson().setEnabled(false);
//			    	  }
//			    	  else
//			    	  {
//			    		  form.getOlbeSalesPerson().setEnabled(true);
//			    	  }
			    	  /** Date 05-09-2017 added by vijay for as per new code discount and round off ****/
//			    	  form.getDbDiscountAmt().setValue(model.getDiscountAmt());
//			    	  form.getTbroundOffAmt().setValue(model.getRoundOffAmt()+"");
			    	  
			    	  if(model.getNumberRange()!=null && !model.getNumberRange().equals("")){
				    	  form.getOlbcNumberRange().setValue(model.getNumberRange());
			    	  }
			    	  form.hideWaitSymbol();
					
					//as per other if blocks
					form.toggleAppHeaderBarMenu();
					glassPanel.hide();
				}
			};
			t.schedule(3000);
			
		}
		
		if(presenterName.equals("Service Contract"))
		{
			final Quotation model=(Quotation) model1;

			AppMemory.getAppMemory().currentScreen = Screen.CONTRACT;
			LoginPresenter.currentModule="Service";
			LoginPresenter.currentDocumentName="Service Contract";
			glassPanel.show();
			final ContractForm form = new ContractForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			
			final Contract contrct=new Contract();
			contrct.setBranch(model.getBranch());
			contrct.setEmployee(model.getEmployee());
			contrct.setType(model.getType());
			contrct.setCategory(model.getCategory());
			contrct.setGroup(model.getGroup());
			
			form.checkCustomerBranch(model.getCinfo().getCount());
			
			Timer t = new Timer() {
				@Override
				public void run() {
					//5 lines written below are not part of LeadPresenter. Added as per other if blocks.
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					ContractPresenter contrctPresenter=new ContractPresenter(form,new Contract());
					presenter = contrctPresenter;
					
					form.setToNewState();
					form.updateView(contrct);
					presenter.setModel(contrct); //added as per other if blocks
					form.setPopUpAppMenubar(true);//added as per other if blocks
					form.setHideNavigationButtons(hideNavigationButtons);//03-10-2022	
					//below code added as per other if blocks
					MyQuerry querry=new MyQuerry();
					Vector<Filter> filtervec=new Vector<Filter>();
					Filter tempfilter=new Filter();
					tempfilter=new Filter();
					tempfilter.setQuerryString("count");
					tempfilter.setIntValue(model.getCinfo().getCount());
					filtervec.add(tempfilter);
					tempfilter=new Filter();
					tempfilter.setQuerryString("companyId");
					tempfilter.setLongValue(model.getCompanyId());
					filtervec.add(tempfilter);
					querry.setFilters(filtervec);
					querry.setQuerryObject(new Customer());
					
					/**
					 * Date : 24-08-2017 BY ANIL
					 * For loading customer branches if any
					 */
					form.checkCustomerBranch(model.getCinfo().getCount());
					/**
					 * End
					 */
					
					//Copying from Quotation presenter- switchToContract method- run block
					 form.getTbContractId().setText("");
			    	  if(model.getLeadCount()==-1)
			    		  form.getTbLeadId().setText("");
			    	  form.getPersonInfoComposite().setEnabled(false);
			    	  form.getTbReferenceNumber().setText("");
			    	  form.getPersonInfoComposite().setValue(model.getCinfo());
			    	  
			    	  //  rohan added this code for loading customer branch date :3/2/2017
			    	  
			    	  	ContractPresenter.custcount=model.getCinfo().getCount();
			    	  //   ends here 
			    	  	 /**
				    	   * Date : 18-07-2017 by Anil
				    	   * added this code for mapping customer category to segment field for NBHC CCPM
				    	   */
				    	  form.checkCustomerStatus(model.getCinfo().getCount(),true,true);
				    	  /**
				    	   * End
				    	   */
			    	  
			    	  form.getOlbbBranch().setValue(model.getBranch());
			    	  form.getOlbApproverName().setValue(model.getApproverName());
			    	  if(model.getLeadCount()!=-1){
			    		  form.getTbLeadId().setValue(model.getLeadCount()+"");
			    	  }
//			    	  if(model.getContractCount()!=-1){
//			    		  form.getTbContractId().setValue(model.getContractCount()+"");
//			    	  }
			    	  
			    	  if(model.getTicketNumber()!=-1){
			    		  System.out.println("SETTING TICKET NUMBER FROM QUOTATION !!!!!!!!!!!");
			    		  form.getTbTicketNumber().setValue(model.getTicketNumber()+"");
			    	  }
			    	  
			    	  
			    	  if(model.getCreditPeriod()!=null){
			    		  form.getIbCreditPeriod().setValue(model.getCreditPeriod());
			    	  }
			    	  
			    	  /**
				  		 * Date : 31-08-2017 BY Vijay
				  		 * If service schedule list is greater than 700 then we have to retrieve data from separate entity as 
				  		 * we are storing it in separate entity.
				  		 * we have to read schedule data from an entity and then add it contract shcedule popup
				  		 * old code added in else block and on success
				  		 */
				  		if(model.getServiceScheduleList().size()==0){
				  			Console.log("SERVICE SCHEDULE LIST ");
				  			Console.log("COM_ID "+model.getCompanyId()+" DOC_TYPE "+"Contract"+" DOC_ID "+model.getCount());
				  			final ContractServiceAsync conSer=GWT.create(ContractService.class);
				  			
				  			conSer.getScheduleServiceSearchResult(model.getCompanyId(), "Quotation", model.getCount(), new AsyncCallback<ArrayList<ServiceSchedule>>() {
				  				@Override
				  				public void onFailure(Throwable caught) {
				  					Console.log("SERVICE SCHEDULE FAILURE ");
				  				}
				  				@Override
				  				public void onSuccess(ArrayList<ServiceSchedule> result) {
				  					Console.log("SERVICE SCHEDULE SUCCESS "+result.size());
				  					if(result.size()!=0){
				  						/**
								    	   * Date : 12-05-2017 By Anil
								    	   * This method update item list by adding branch info at product level.
								    	   */
								    	  List<SalesLineItem>itemList=AppUtility.getItemList(model.getItems(),result,form.customerbranchlist,model.getBranch());
//								    	  List<SalesLineItem>itemList=null;
								    	  if(itemList!=null){
								    		  form.getSaleslineitemtable().setValue(itemList);
								    	  }else{
								    		 form.setToNewState();
								    		 form.showDialogMessage("No. of branches at product level and no. of selected branchs at service scheduling popup are different.");
								    		 glassPanel.hide();
								    		 return;
								    		 
								    	  }
								    	  /**
								    	   * End
								    	   */
								    	  
//								    	  form.getSaleslineitemtable().setValue(model.getItems());
								    	  
								    	  form.getTbQuotatinId().setValue(model.getCount()+"");
								    	  form.getOlbcPaymentMethods().setValue(model.getPaymentMethod());
								    	  form.getOlbeSalesPerson().setValue(model.getEmployee());
								    	  form.getTbQuotationStatus().setValue(Quotation.CREATED);
								    	  form.getProdTaxTable().setValue(model.getProductTaxes());
								    	  form.getChargesTable().setValue(model.getProductCharges());
//								    	  form.getChargesTable().setEnable(false);
								    	  form.getPaymentTermsTable().setValue(model.getPaymentTermsList());
								    	  form.getDototalamt().setValue(model.getTotalAmount());
								    	  form.getDonetpayamt().setValue(model.getNetpayable());
								    	  form.getServiceScheduleTable().setValue(result);
								    	  
								    	  if(model.getScheduleServiceDay()!=null){
								    		  form.setF_serviceDay(model.getScheduleServiceDay());
								    	  }
								    	  
								    	  form.getChkbillingatBranch().setValue(false);
								    	  form.getChkservicewithBilling().setValue(false);
								    	  if(model.getPremisesDesc()!=null){
								    	  form.getTbPremisesUnderContract().setValue(model.getPremisesDesc());
								    	 
								    	  //commented as it was giving error
//								    	  if(QuotationPresenter.this.form.isSalesPersonRestrictionFlag())
//								    	  {
//								    		  form.getOlbeSalesPerson().setEnabled(false);
//								    	  }
//								    	  else
//								    	  {
//								    		  form.getOlbeSalesPerson().setEnabled(true);
//								    	  }
								    	  }
								    	  
								    	  /** Date 05-09-2017 added by vijay for as per new code discount and round off ****/
								    	  form.getDbDiscountAmt().setValue(model.getDiscountAmt());
								    	  form.getTbroundOffAmt().setValue(model.getRoundOffAmt()+"");
								    	  
								    	  /**
								    	   * Date : 30-10-2017 BY ANIL
								    	   */
								    	  form.getCbServiceWiseBilling().setValue(false);
								    	  /**
								    	   * End
								    	   */
								    	  /**
								    	   * Date : 24-02-2018
								    	   * map  contact list
								    	   * 
								    	   */
								    	  form.getCustomerContactList(model.getCount(),null);
								    	  /**
								    	   * end
								    	   */
								    	  

								    	  /**
								    	   * Updated By: Viraj
								    	   * Date: 05-04-2019
								    	   * Description: To map consolidate amount checkbox value in contract
								    	   */
								    	  Console.log("cbConsolidatePrice: "+ model.isConsolidatePrice());
							    		  form.getCbConsolidatePrice().setValue(model.isConsolidatePrice());
								    	  
								    	  /** Ends **/
								    	  /**
								    	   * end
								    	   */
								    	  
								    	  /**
									  		 * Date 28-09-2018 By Vijay 
									  		 * Des:- NBHC CCPM for every contract this must be true by default it does not allow to change for user
									  		 */
									  		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC")){
									  			form.cbStartOfPeriod.setValue(true);
									  			form.cbStartOfPeriod.setEnabled(false);
									  		}
									  		/**
									  		 * ends here
									  		 */
									  		
									  		if(model.getBillingAddress()!=null && model.getBillingAddress().getAddrLine1()!=null 
									  				  && !model.getBillingAddress().getAddrLine1().equals("")) {
										  		form.customerBillingAddress = model.getBillingAddress();
									  		}
									  		if(model.getServiceAddress()!=null && model.getServiceAddress().getAddrLine1()!=null 
									  				  && !model.getServiceAddress().getAddrLine1().equals("")) {
										  		form.customerServiceAddress = model.getServiceAddress();
									  		}
									  		
									  		/**
									  		 * @author Vijay Date :- 28-01-2021
									  		 * Des :- if Free Material process config is active then stationed Service flag will true 
									  		 * or else it will false. if this flag is true then service will create with Automatic Scheduling 
									  		 */
									  		form.getChkStationedService().setValue(false);
									  		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICEMODULE, AppConstants.PC_FREEMATERIAL)){
										  		form.getChkStationedService().setValue(true);
											}
									  		else{
										  		form.getChkStationedService().setValue(false);
									  		}
				  					}
				  				}
				  			});
				  		}else{
				  			/**
					    	   * Date : 12-05-2017 By Anil
					    	   * This method update item list by adding branch info at product level.
					    	   */
					    	  List<SalesLineItem>itemList=AppUtility.getItemList(model.getItems(), model.getServiceScheduleList(),form.customerbranchlist,model.getBranch());
//					    	  List<SalesLineItem>itemList=null;
					    	  if(itemList!=null){
					    		  form.getSaleslineitemtable().setValue(itemList);
					    	  }else{
					    		 form.setToNewState();
					    		 form.showDialogMessage("No. of branches at product level and no. of selected branchs at service scheduling popup are different.");
					    		 glassPanel.hide();
					    		 return;
					    		 
					    	  }
					    	  /**
					    	   * End
					    	   */
					    	  
//					    	  form.getSaleslineitemtable().setValue(model.getItems());
					    	  
					    	  form.getTbQuotatinId().setValue(model.getCount()+"");
					    	  form.getOlbcPaymentMethods().setValue(model.getPaymentMethod());
					    	  form.getOlbeSalesPerson().setValue(model.getEmployee());
					    	  form.getTbQuotationStatus().setValue(Quotation.CREATED);
					    	  form.getProdTaxTable().setValue(model.getProductTaxes());
					    	  form.getChargesTable().setValue(model.getProductCharges());
//					    	  form.getChargesTable().setEnable(false);
					    	  form.getPaymentTermsTable().setValue(model.getPaymentTermsList());
					    	  form.getDototalamt().setValue(model.getTotalAmount());
					    	  form.getDonetpayamt().setValue(model.getNetpayable());
					    	  if(model.getServiceScheduleList().size()!=0){
					    		  form.getServiceScheduleTable().setValue(model.getServiceScheduleList());
					    	  }
					    	  
					    	  if(model.getScheduleServiceDay()!=null){
					    		  form.setF_serviceDay(model.getScheduleServiceDay());
					    	  }
					    	  
					    	  form.getChkbillingatBranch().setValue(false);
					    	  form.getChkservicewithBilling().setValue(false);
					    	  if(model.getPremisesDesc()!=null){
					    	  form.getTbPremisesUnderContract().setValue(model.getPremisesDesc());
//					    	  if(presenterName.this.form.isSalesPersonRestrictionFlag())
//					    	  {
//					    		  form.getOlbeSalesPerson().setEnabled(false);
//					    	  }
//					    	  else
//					    	  {
//					    		  form.getOlbeSalesPerson().setEnabled(true);
//					    	  }
					    	  }
					    	  
					    	  /** Date 05-09-2017 added by vijay for as per new code discount and round off ****/
					    	  form.getDbDiscountAmt().setValue(model.getDiscountAmt());
					    	  form.getTbroundOffAmt().setValue(model.getRoundOffAmt()+"");
					    	  
					    	  /**
					    	   * Date : 30-10-2017 BY ANIL
					    	   */
					    	  form.getCbServiceWiseBilling().setValue(false);
					    	  /**
					    	   * End
					    	   */
					    	  /**
					    	   * Date : 24-02-2018
					    	   * map  contact list
					    	   * 
					    	   */
					    	  form.getCustomerContactList(model.getCount(),null);
					    	  /**
					    	   * end
					    	   */
					    	  

					    	  /**
					    	   * Updated By: Viraj
					    	   * Date: 05-04-2019
					    	   * Description: To map consolidate amount checkbox value in contract
					    	   */
					    	  Console.log("cbConsolidatePrice: "+ model.isConsolidatePrice());
				    		  form.getCbConsolidatePrice().setValue(model.isConsolidatePrice());
					    	  
					    	  /** Ends **/
					    	  /**
					    	   * end
					    	   */
					    	  
					    	   /**
						  		 * Date 28-09-2018 By Vijay 
						  		 * Des:- NBHC CCPM for every contract this must be true by default it does not allow to change for user
						  		 */
						  		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC")){
						  			form.cbStartOfPeriod.setValue(true);
						  			form.cbStartOfPeriod.setEnabled(false);
						  		}
						  		/**
						  		 * ends here
						  		 */
						  		
						  		if(model.getBillingAddress()!=null && model.getBillingAddress().getAddrLine1()!=null 
						  				  && !model.getBillingAddress().getAddrLine1().equals("")) {
							  		form.customerBillingAddress = model.getBillingAddress();
							  		Console.log("quotation ==");
						  		}
						  		if(model.getServiceAddress()!=null && model.getServiceAddress().getAddrLine1()!=null 
						  				  && !model.getServiceAddress().getAddrLine1().equals("")) {
							  		form.customerServiceAddress = model.getServiceAddress();
							  		Console.log("quotation $ ==");
						  		}
						  		
						  		/**
						  		 * @author Vijay Date :- 28-01-2021
						  		 * Des :- if Free Material process config is active then stationed Service flag will true 
						  		 * or else it will false. if this flag is true then service will create with Automatic Scheduling 
						  		 */
						  		form.getChkStationedService().setValue(false);
//						  		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICEMODULE, AppConstants.PC_FREEMATERIAL)){
//							  		form.getChkStationedService().setValue(true);
//								}
//						  		else{
//							  		form.getChkStationedService().setValue(false);
//						  		}
						  		
						  		
						  		if(model.getQuotationNumberRange()!=null && !model.getQuotationNumberRange().equals("")){
							    	  form.getOlbcNumberRange().setValue(model.getQuotationNumberRange());
						    	  }
						  		
				  		}
				    	  form.hideWaitSymbol();
				    //End of copy	  
					//as per other if blocks
					form.toggleAppHeaderBarMenu();
					glassPanel.hide();
				}
			};
			t.schedule(3000);
		}
		if(presenterName.equals("Quick Service Contract")){

			final Quotation model=(Quotation) model1;

			AppMemory.getAppMemory().currentScreen = Screen.QUICKCONTRACT;
			LoginPresenter.currentModule="Service";
			LoginPresenter.currentDocumentName="Quick Service Contract";
			glassPanel.show();
			final QuickContractForm form = new QuickContractForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			
			final Contract contrct=new Contract();
			contrct.setBranch(model.getBranch());
			contrct.setEmployee(model.getEmployee());
			contrct.setType(model.getType());
			contrct.setCategory(model.getCategory());
			contrct.setGroup(model.getGroup());
			
			form.checkCustomerBranch(model.getCinfo().getCount());
			
			Timer t = new Timer() {
				@Override
				public void run() {
					//5 lines written below are not part of LeadPresenter. Added as per other if blocks.
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					QuickContractPresentor contrctPresenter=new QuickContractPresentor(form,new Contract());
					presenter = contrctPresenter;
					
					form.setToNewState();
					form.updateView(contrct);
					presenter.setModel(contrct); //added as per other if blocks
					form.setPopUpAppMenubar(true);//added as per other if blocks
					form.setHideNavigationButtons(hideNavigationButtons);//03-10-2022	
					//below code added as per other if blocks
					MyQuerry querry=new MyQuerry();
					Vector<Filter> filtervec=new Vector<Filter>();
					Filter tempfilter=new Filter();
					tempfilter=new Filter();
					tempfilter.setQuerryString("count");
					tempfilter.setIntValue(model.getCinfo().getCount());
					filtervec.add(tempfilter);
					tempfilter=new Filter();
					tempfilter.setQuerryString("companyId");
					tempfilter.setLongValue(model.getCompanyId());
					filtervec.add(tempfilter);
					querry.setFilters(filtervec);
					querry.setQuerryObject(new Customer());
					
					/**
					 * Date : 24-08-2017 BY ANIL
					 * For loading customer branches if any
					 */
					form.checkCustomerBranch(model.getCinfo().getCount());
					/**
					 * End
					 */
					
					//Copying from Quotation presenter- switchToQuickContract method- run block			    	  
					  form.setToNewState();
					  form.getTbContractId().setText("");
			    	  form.getPersonInfoComposite().setEnabled(false);
			    	  form.getPersonInfoComposite().setValue(model.getCinfo());
			    	  
			    	  //  rohan added this code for loading customer branch date :3/2/2017
			    	  
			    	  QuickContractPresentor.custcount=model.getCinfo().getCount();
			    	  //   ends here 
			    	  
			    	  form.getOlbbBranch().setValue(model.getBranch());
			    	  form.getOlbApproverName().setValue(model.getApproverName());

//			    	  if(model.getContractCount()!=-1){
//			    		  form.getTbContractId().setValue(model.getContractCount()+"");
//			    	  }
			    	  
			    	  
			    	  if(model.getCreditPeriod()!=null){
			    		  form.getIbCreditPeriod().setValue(model.getCreditPeriod());
			    	  }
//			    	  form.getCustomerAddress(model.getCinfo().getCount());
			    	  form.getTbQuotatinId().setValue(model.getCount()+"");
			    	  form.getTbQuotatinId().setEnabled(false);
			    	  
			    	  /**
			    	   * Date : 12-05-2017 By Anil
			    	   * This method update itemlist by adding branch info at product level.
			    	   */
			    	  List<SalesLineItem>itemList=AppUtility.getItemList(model.getItems(), model.getServiceScheduleList(),form.customerbranchlist,model.getBranch());
//			    	  List<SalesLineItem>itemList=null;
			    	  if(itemList!=null){
			    		  form.getSaleslineitemtable().setValue(itemList);
			    	  }else{
			    		 form.setToNewState();
			    		 form.showDialogMessage("No. of branches at product level and no. of selected branchs at service scheduling popup are different.");
			    		 glassPanel.hide();
			    		 return;
			    		 
			    	  }
			    	  /**
			    	   * End
			    	   */
			    	  
//			    	  form.getSaleslineitemtable().setValue(model.getItems());
			    	
			    	  form.getOlbcPaymentMethods().setValue(model.getPaymentMethod());
			    	  form.getOlbeSalesPerson().setValue(model.getEmployee());
			    	  form.getTbStatus().setValue(Quotation.CREATED);
			    	  form.getProdTaxTable().setValue(model.getProductTaxes());
			    	  form.getChargesTable().setValue(model.getProductCharges());
//			    	  form.getChargesTable().setEnable(false);
			    	  form.getDototalamt().setValue(model.getTotalAmount());
			    	  form.getDonetpayamt().setValue(model.getNetpayable());
			    	  if(model.getServiceScheduleList().size()!=0){
			    		  form.getServiceScheduleTable().setValue(model.getServiceScheduleList());
			    	  }
			    	  
//			    	  if(model.getScheduleServiceDay()!=null){
//			    		  form.setF_serviceDay(model.getScheduleServiceDay());
//			    	  }
//			    	  
//			    	  form.getChkbillingatBranch().setValue(false);
//			    	  form.getChkservicewithBilling().setValue(false);
//			    	  if(model.getPremisesDesc()!=null){
//			    	  form.getTbPremisesUnderContract().setValue(model.getPremisesDesc());
			    	  
			    	  //commented as it is giving error
//			    	  if(QuotationPresenter.this.form.isSalesPersonRestrictionFlag())
//			    	  {
//			    		  form.getOlbeSalesPerson().setEnabled(false);
//			    	  }
//			    	  else
//			    	  {
//			    		  form.getOlbeSalesPerson().setEnabled(true);
//			    	  }
			    	  
			    	  	form.getTbLeadId().setValue(model.getCount()+"");
						form.getTbLeadId().setEnabled(false);
				    	form.hideWaitSymbol();

				    	if(model.getQuotationNumberRange()!=null && !model.getQuotationNumberRange().equals("")){
					    	  form.getOlbcNumberRange().setValue(model.getQuotationNumberRange());
				    	}				    				    	  
				    //End of copy	  
					//as per other if blocks	
				    form.setHideNavigationButtons(hideNavigationButtons);//03-10-2022	
					form.toggleAppHeaderBarMenu();
					glassPanel.hide();
				}
			};
			t.schedule(3000);		
		}
		
		if (presenterName.equals("Customer Service List")) {
			try{
				Contract model=(Contract) model1;
	
				AppMemory.getAppMemory().currentScreen = Screen.SERVICE;
				LoginPresenter.currentModule="Service";
				LoginPresenter.currentDocumentName="Customer Service List";
				glassPanel.show();
				ServiceTable table=new ServiceTable();
				final ServiceForm form=new ServiceForm(table);
				/**
				 * @author Anil
				 * @since 07-01-2021
				 * Showing process level menus
				 */
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
				}
				docVerticalPanel.add(form.content);
				
				Filter filter = new Filter();
				filter.setIntValue(model.getCount());
				filter.setQuerryString("contractCount");
				Vector<Filter> vecfilter = new Vector<Filter>();
				vecfilter.add(filter);
				final MyQuerry query = new MyQuerry(vecfilter, new Service());
				
				Timer timer = new Timer() {
					@Override
					public void run() {
						form.content.getElement().getStyle().setHeight(570, Unit.PX);//06-10-2022
						form.content.getElement().getStyle().setWidth(950, Unit.PX);//06-10-2022
						setSizeOfPanel(form);
						viewDocumentPanel.show();
						viewDocumentPanel.center();
						ServicePresenter servicePresenter=new ServicePresenter(form,new Service(),query);
						presenter = servicePresenter;
						form.setPopUpAppMenubar(true);
						form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022											
						form.setToViewState();
						form.toggleAppHeaderBarMenu();
						glassPanel.hide();
					}
				};
				timer.schedule(1000);
			}		
			catch(Exception e)
			{
				e.printStackTrace();
				Console.log(e.getMessage());				
			}
									
		}
		if (presenterName.equals("View Bill")) {
			final Contract model=(Contract) model1;

			AppMemory.getAppMemory().currentScreen = Screen.BILLINGDETAILS;
			LoginPresenter.currentModule="Accounts";//changed from service to accounts
			LoginPresenter.currentDocumentName="Bill";
			glassPanel.show();
			final BillingDetailsForm form = new BillingDetailsForm();				
				

			final MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("contractCount");
			filter.setIntValue(model.getCount());
			temp.add(filter);
			
			querry.setFilters(temp);
			querry.setQuerryObject(new BillingDocument());
			form.showWaitSymbol();
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					if(result.size()==0){
						form.showDialogMessage("No billing document found.");
						glassPanel.hide();
						return;
					}
					if(result.size()==1){
						final BillingDocument billDocument=(BillingDocument) result.get(0);
						if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
							form.setAction(false);
							form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
							form.processLevelBar.setVisibleFalse(false);
							docVerticalPanel.add(form.processLevelBar.content);
							form.processLevelBar.content.setWidth("80%");
						}
						
						//final BillingDetailsForm form=BillingDetailsPresenter.initalize();					
						Timer timer=new Timer() {
							@Override
							public void run() {
								docVerticalPanel.add(form.content);
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();
								
								BillingDetailsPresenter billingPresenter=new BillingDetailsPresenter(form,new BillingDocument());
								presenter = billingPresenter;
								
								
								form.updateView(billDocument);
								presenter.setModel(billDocument); //added as per other if blocks								
								form.setPopUpAppMenubar(true);//added as per other if blocks
								form.setHideNavigationButtons(hideNavigationButtons);//03-10-2022	
								form.setToViewState();
								//form.toggleAppHeaderBarMenu();// commented on 19-10-2022 since it was overriding header menus set as per summary screen.
							}
						};
						timer.schedule(1000);
					}else{
						
						//BillingListPresenter.initalize(querry);
						final Contract model=(Contract) model1;
						Console.log("calling Billing list");

						AppMemory.getAppMemory().currentScreen = Screen.BILLINGLIST;
						LoginPresenter.currentModule="Accounts";
						LoginPresenter.currentDocumentName="Billing List";
						glassPanel.show();
						BillingListTable table=new BillingListTable();
						final BillingListForm form=new BillingListForm(table);
						/**
						 * @author Anil
						 * @since 07-01-2021
						 * Showing process level menus
						 */
						if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
							form.setAction(false);
							form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
							form.processLevelBar.setVisibleFalse(false);
							docVerticalPanel.add(form.processLevelBar.content);
						}
						docVerticalPanel.add(form.content);
						Timer timer = new Timer() {
							@Override
							public void run() {
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();

								Contract serviceEntity = (Contract) model;
//								form.updateView(serviceEntity);
								
								
								BillingListPresenter billingListpresenter=new BillingListPresenter(form, new BillingDocument(),querry);
								presenter = billingListpresenter;
								presenter.setModel(serviceEntity);
								
								form.setPopUpAppMenubar(true);
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022											
								form.setToViewState();
								
								form.toggleAppHeaderBarMenu();
								glassPanel.hide();
							}
						};
						timer.schedule(1000);
					}
					form.hideWaitSymbol();					
					glassPanel.hide();
				}
			});
			
		}
		if (presenterName.equals("Create Work Order")) {
			final Contract model=(Contract) model1;

			AppMemory.getAppMemory().currentScreen = Screen.WORKORDER;
			LoginPresenter.currentModule="Service";
			LoginPresenter.currentDocumentName="Create Work Order";
			glassPanel.show();
			final WorkOrderForm form = new WorkOrderForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			
			final WorkOrder wo = new WorkOrder();

			wo.setOrderId(model.getCount());
			wo.setOrderDate(model.getContractDate());
			if (model.getRefNo() != null)
				wo.setRefNum(model.getRefNo());
			if (model.getRefDate() != null)
				wo.setRefDate(model.getRefDate());
			wo.setBranch(model.getBranch());
			wo.setSalesPerson(model.getEmployee());
			wo.setApproverName(model.getApproverName());

			List<SalesLineItem> productList = model.getItems();
			wo.setBomTable(productList);

			form.showWaitSymbol();

			Timer t = new Timer() {
				@Override
				public void run() {
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					WorkOrderPresenter woPresenter=new WorkOrderPresenter(form,new WorkOrder());
					presenter = woPresenter;
					
					form.setToNewState();
					form.updateView(wo);
					presenter.setModel(wo); //added as per other if blocks
					form.setPopUpAppMenubar(true);//added as per other if blocks
					form.setHideNavigationButtons(hideNavigationButtons);//03-10-2022	
					
					form.getIbOrderId().setEnabled(false);
					form.getDbOrderDate().setEnabled(false);
					form.getOblSalesPerson().setEnabled(false);
					form.getOblWoBranch().setEnabled(false);
					form.getOblSalesPerson().setEnabled(false);
					form.getUpload().getCancel().setVisible(false);
					form.getOblWoApproverName().setEnabled(false);
					form.getProdInfoComp().setEnabled(false);
					if (model.getRefNo() != null)
						form.getTbRefNumId().setEnabled(false);
					if (model.getRefDate() != null)
						form.getDbRefDate().setEnabled(false);
					form.getLbComProdId().clear();
					form.getLbComProdId().addItem("--SELECT--");
					for (int i = 0; i < model.getItems().size(); i++) {
						form.flage = false;
						form.getBillOfProductMaterial(model.getItems().get(i)
								.getPrduct().getCount(), model.getItems().get(i)
								.getProductName(),
								model.getItems().get(i).getQty(), false);
						form.getLbComProdId()
								.addItem(
										model.getItems().get(i).getPrduct()
												.getCount()
												+ "");
					}
					form.hideWaitSymbol();
					form.toggleAppHeaderBarMenu();
					glassPanel.hide();
				}
			};
			t.schedule(5000);
			
		}
		
		if (presenterName.equals("Contract Renew")) {
			final Contract model=(Contract) model1;
			if(model.isRenewFlag()){
				boolean conf = Window.confirm("This contract is already renewed,Do you really want to renew it again?");
				if(conf==true){
				reactOnContractRenewal(model);
				}
			}else{
					reactOnContractRenewal(model);	
				}
			

			
		}
		if (presenterName.equals("View GRN")) {
			final BillingDocument model=(BillingDocument) model1;

			AppMemory.getAppMemory().currentScreen = Screen.GRN;
			LoginPresenter.currentModule="Accounts";
			LoginPresenter.currentDocumentName="View GRN";
			glassPanel.show();
			final GRNForm form = new GRNForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			

			final MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(Integer.parseInt(model.getRefNumber()));
			temp.add(filter);
		
			
			querry.setFilters(temp);
			querry.setQuerryObject(new GRN());
			form.showWaitSymbol();
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					if(result.size()==0){
						form.showDialogMessage("No GRN found.");
						glassPanel.hide();
						return;
					}
					if(result.size()==1){
						final GRN grn=(GRN) result.get(0);
						//final GRNForm form=GRNPresenter.initalize();
						Timer timer=new Timer() {
							@Override
							public void run() {
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();
								
								GRNPresenter grnPresenter=new GRNPresenter(form,new GRN());
								presenter = grnPresenter;
								
								form.updateView(grn);
								form.setToViewState();
								form.setPopUpAppMenubar(true);
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022
								presenter.setModel(grn);
								
							}
						};
						timer.schedule(1000);
						form.hideWaitSymbol();
						form.toggleAppHeaderBarMenu();
						glassPanel.hide();
					}else{
						//GRNListPresenter.initalize(querry);
						Console.log("calling GRN list");
						AppMemory.getAppMemory().currentScreen = Screen.GRNLIST;
						LoginPresenter.currentModule="Accounts";
						LoginPresenter.currentDocumentName="GRN List";
						glassPanel.show();
						GRNSearchTable table=new GRNSearchTable();
						final GRNListForm form=new GRNListForm(table);
						/**
						 * @author Anil
						 * @since 07-01-2021
						 * Showing process level menus
						 */
						if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
							form.setAction(false);
							form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
							form.processLevelBar.setVisibleFalse(false);
							docVerticalPanel.add(form.processLevelBar.content);
						}
						docVerticalPanel.add(form.content);
						Timer timer = new Timer() {
							@Override
							public void run() {
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();

								BillingDocument serviceEntity = (BillingDocument) model;
//								form.updateView(serviceEntity);
								
								GRNListPresenter grnpresenter=new GRNListPresenter(form,new GRN(),querry);
								presenter = grnpresenter;
								presenter.setModel(serviceEntity);
								
								form.setPopUpAppMenubar(true);
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022

								form.setToNewState(); //from original presenter
								//form.setToViewState(); - from code given by vijay sir
								form.toggleAppHeaderBarMenu();
								glassPanel.hide();
							}
						};
						timer.schedule(1000);
					}
					
				}
			});
		 }
		if(presenterName.equals("View Invoice")){
			final BillingDocument model=(BillingDocument) model1;

			AppMemory.getAppMemory().currentScreen = Screen.INVOICEDETAILS;//14-09-2022
			LoginPresenter.currentModule="Accounts";
			LoginPresenter.currentDocumentName="Invoice";
			glassPanel.show();
			
			final InvoiceDetailsForm form = new InvoiceDetailsForm();						
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			
			
			
			final MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(model.getInvoiceCount());
			temp.add(filter);
			
			querry.setFilters(temp);
//			if(model.getAccountType().equalsIgnoreCase(AppConstants.BILLINGACCOUNTTYPEAP)){
//				querry.setQuerryObject(new VendorInvoice());
//			}else{
//				querry.setQuerryObject(new Invoice());
//			}
			if(!model.getAccountType().equalsIgnoreCase(AppConstants.BILLINGACCOUNTTYPEAP))
				querry.setQuerryObject(new Invoice());
			
			
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					//form.hideWaitSymbol();
					glassPanel.hide();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					//form.hideWaitSymbol();
//					glassPanel.hide();
					if(result.size()==0){
						form.showDialogMessage("No invoice document found.");
						glassPanel.hide();
						return;
					}
					if(result.size()==1){						
//						if(model.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
//							final VendorInvoice billDocument=(VendorInvoice) result.get(0);
//							final VendorInvoiceDetailsForm vform=new VendorInvoiceDetailsForm();
//							if (vform.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
//								vform.setAction(false);
//								vform.processLevelBar = new ProcessLevelBar(5, vform.getProcesslevelBarNames(),vform.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
//								vform.processLevelBar.setVisibleFalse(false);
//								docVerticalPanel.add(vform.processLevelBar.content);
//								vform.processLevelBar.content.setWidth("80%");
//							}
//							docVerticalPanel.add(vform.content);
//							Timer timer=new Timer() {
//								@Override
//								public void run() {
//									setSizeOfPanel(vform);
//									viewDocumentPanel.show();
//									viewDocumentPanel.center();
//									
//									VendorInvoiceDetailsPresenter vendorInvoicePresenter=new VendorInvoiceDetailsPresenter(vform,new VendorInvoice());
//									presenter = vendorInvoicePresenter;
//									vform.updateView(billDocument);
//									vform.setToViewState();
//									presenter.setModel(billDocument);
//									vform.setPopUpAppMenubar(true);
//								}
//							};
//							timer.schedule(1000);
//							//vform.hideWaitSymbol();
//							vform.toggleAppHeaderBarMenu();
//							glassPanel.hide();
//						}
						
						if(!model.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
						final Invoice billDocument=(Invoice) result.get(0);
						//final InvoiceDetailsForm form=InvoiceDetailsPresenter.initalize();
						
						Timer timer=new Timer() {
							@Override
							public void run() {
								docVerticalPanel.add(form.content);
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();
								
								InvoiceDetailsPresenter invoicePresenter=new InvoiceDetailsPresenter(form,new Invoice());
								presenter = invoicePresenter;
								form.updateView(billDocument);
								form.setPopUpAppMenubar(true);	
								Console.log("hideNavigationButtons in navigate screen="+hideNavigationButtons);
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022								
								form.setToViewState();
								presenter.setModel(billDocument);
								glassPanel.hide();
							}
						};
						timer.schedule(5000);
						//form.hideWaitSymbol();
						form.toggleAppHeaderBarMenu();
						
						}}
						else{
						/** date 25.6.2018 added by komal for vendor invoice list **/
//						if(model.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
//							VendorInvoiceListPresenter.initalize(querry);
//						}
						if(!model.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
							//InvoiceListPresenter.initalize(querry);
							Console.log("calling Invoice list");
							AppMemory.getAppMemory().currentScreen = Screen.INVOICELIST;
							LoginPresenter.currentModule="Accounts";
							LoginPresenter.currentDocumentName="Invoice List";
							glassPanel.show();
							InvoiceListTable table=new InvoiceListTable();
							final InvoiceListForm form=new InvoiceListForm(table);
							/**
							 * @author Anil
							 * @since 07-01-2021
							 * Showing process level menus
							 */
							if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
								form.setAction(false);
								form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
								form.processLevelBar.setVisibleFalse(false);
								docVerticalPanel.add(form.processLevelBar.content);
							}
							docVerticalPanel.add(form.content);
							Timer timer = new Timer() {
								@Override
								public void run() {
									setSizeOfPanel(form);
									viewDocumentPanel.show();
									viewDocumentPanel.center();

									BillingDocument serviceEntity = (BillingDocument) model;
//									form.updateView(serviceEntity);
								
									InvoiceListPresenter invoicePresenter=new InvoiceListPresenter(form, new Invoice(),querry);
									presenter = invoicePresenter;
									presenter.setModel(serviceEntity);
									
									form.setPopUpAppMenubar(true);
									form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022

									form.setToViewState();
									form.toggleAppHeaderBarMenu();
									glassPanel.hide();
								}
							};
							timer.schedule(1000);
						}				
				}
				}
			});								
		}
		
		if (presenterName.equals("View Invoice Bill")) {
			Console.log("inside if (presenterName.equals(View Invoice Bill))");
			final Invoice model=(Invoice) model1;
			AppMemory.getAppMemory().currentScreen = Screen.BILLINGDETAILS;
			LoginPresenter.currentModule="Accounts";
			LoginPresenter.currentDocumentName="Bill";
			glassPanel.show();
			final BillingDetailsForm form = new BillingDetailsForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			
					
			int invoiceId=0;
			if(model.getProformaCount()!=null&&model.getProformaCount()!=0&&!model.getInvoiceType().equals("Proforma Invoice")){
				invoiceId=model.getProformaCount();
			}else{
				invoiceId=model.getCount();
			}
			
			final MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("invoiceCount");
//			filter.setIntValue(model.getCount());
			filter.setIntValue(invoiceId);
			temp.add(filter);
			
			querry.setFilters(temp);
			querry.setQuerryObject(new BillingDocument());
			form.showWaitSymbol();
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					if(result.size()==0){
						form.showDialogMessage("No billing document found.");
						glassPanel.hide();
						return;
					}
					if(result.size()==1){
						final BillingDocument billDocument=(BillingDocument) result.get(0);
						//final BillingDetailsForm form=BillingDetailsPresenter.initalize();					
						Timer timer=new Timer() {
							@Override
							public void run() {
								docVerticalPanel.add(form.content);
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();
								
								BillingDetailsPresenter billingPresenter=new BillingDetailsPresenter(form,new BillingDocument());
								presenter = billingPresenter;
								
//								form.setToNewState();
								form.updateView(billDocument);
								presenter.setModel(billDocument); //added as per other if blocks
								form.setPopUpAppMenubar(true);//added as per other if blocks
								form.setHideNavigationButtons(hideNavigationButtons);//03-10-2022	
								form.setToViewState();				
//								form.toggleAppHeaderBarMenu();//commented on 19-10-2022
							}
						};
						timer.schedule(1000);
					}else{
						//BillingListPresenter.initalize(querry);
						final Invoice model=(Invoice) model1;
						Console.log("calling Billing list");

						AppMemory.getAppMemory().currentScreen = Screen.BILLINGLIST;
						LoginPresenter.currentModule="Accounts";
						LoginPresenter.currentDocumentName="Billing List";
						glassPanel.show();
						BillingListTable table=new BillingListTable();
						final BillingListForm form=new BillingListForm(table);
						/**
						 * @author Anil
						 * @since 07-01-2021
						 * Showing process level menus
						 */
						if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
							form.setAction(false);
							form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
							form.processLevelBar.setVisibleFalse(false);
							docVerticalPanel.add(form.processLevelBar.content);
						}
						docVerticalPanel.add(form.content);
						Timer timer = new Timer() {
							@Override
							public void run() {
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();

//								viewDocumentPanel.setHeight("600px");
//								viewDocumentPanel.show();
//								viewDocumentPanel.center();
								
								Invoice serviceEntity = (Invoice) model;
//								form.updateView(serviceEntity);
								
								
								BillingListPresenter billingListpresenter=new BillingListPresenter(form, new BillingDocument(),querry);
								presenter = billingListpresenter;
								presenter.setModel(serviceEntity);
								
								form.setPopUpAppMenubar(true);
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022											
								form.setToViewState();
								form.toggleAppHeaderBarMenu();
								glassPanel.hide();
							}
						};
						timer.schedule(1000);
					
					}
					form.hideWaitSymbol();
					
					glassPanel.hide();
				}
			});
						
		}
		if (presenterName.equals("View Payment")) {
			final Invoice model=(Invoice) model1;
			AppMemory.getAppMemory().currentScreen = Screen.PAYMENTDETAILS;
			LoginPresenter.currentModule="Accounts";
			LoginPresenter.currentDocumentName="View Payment";
			glassPanel.show();
			final PaymentDetailsForm form = new PaymentDetailsForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			
					
			final MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("invoiceCount");
			filter.setIntValue(model.getCount());
			temp.add(filter);
			
			/**Date 26-9-2020 by Amol added a Acc Type Querry**/
			filter=new Filter();
			filter.setQuerryString("accountType");
			filter.setStringValue("AR");
			temp.add(filter);
			
			
			querry.setFilters(temp);
			querry.setQuerryObject(new CustomerPayment());
			form.showWaitSymbol();
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					if(result.size()==0){
						form.showDialogMessage("No payment document found.");
						glassPanel.hide();
						return;
					}
					if(result.size()==1){
						docVerticalPanel.add(form.content);
						final CustomerPayment billDocument=(CustomerPayment) result.get(0);
						//final PaymentDetailsForm form=PaymentDetailsPresenter.initalize();
						Timer timer=new Timer() {
							@Override
							public void run() {
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();
								
								PaymentDetailsPresenter paymentPresenter=new PaymentDetailsPresenter(form,new CustomerPayment());
								presenter = paymentPresenter;
								form.updateView(billDocument);
								form.setPopUpAppMenubar(true);
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022											
								form.setToViewState();
								presenter.setModel(billDocument);
								
							}
						};
						timer.schedule(1000);
					}else{
						//PaymentListPresenter.initalize(querry);
						Console.log("calling Payment list");
						AppMemory.getAppMemory().currentScreen = Screen.PAYMENTLIST;
						LoginPresenter.currentModule="Accounts";
						LoginPresenter.currentDocumentName="Payment List";
						glassPanel.show();
						PaymentListTable table=new PaymentListTable();
						final PaymentListForm form=new PaymentListForm(table);
						/**
						 * @author Anil
						 * @since 07-01-2021
						 * Showing process level menus
						 */
						if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
							form.setAction(false);
							form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
							form.processLevelBar.setVisibleFalse(false);
							docVerticalPanel.add(form.processLevelBar.content);
						}
						docVerticalPanel.add(form.content);
						Timer timer = new Timer() {
							@Override
							public void run() {
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();

								Invoice serviceEntity = (Invoice) model;
//								form.updateView(serviceEntity);
								
								PaymentListPresenter plpresenter=new PaymentListPresenter(form, new CustomerPayment(),querry);
								presenter = plpresenter;
								presenter.setModel(serviceEntity);
								
								form.setPopUpAppMenubar(true);
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022											
								form.setToViewState();
						
							}
						};
						timer.schedule(1000);
					}
					form.hideWaitSymbol();
					form.toggleAppHeaderBarMenu();
					glassPanel.hide();
					
				}
			});
						
		}
		
		if (presenterName.equals("View Payment Invoice")) {

			final CustomerPayment model=(CustomerPayment) model1;
			AppMemory.getAppMemory().currentScreen = Screen.INVOICEDETAILS;//14-09-2022
			LoginPresenter.currentModule="Accounts";
			LoginPresenter.currentDocumentName="Invoice";
			glassPanel.show();
			final InvoiceDetailsForm form = new InvoiceDetailsForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
					
			final MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(model.getInvoiceCount());
			temp.add(filter);
			
			querry.setFilters(temp);
//			if(model.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
//				querry.setQuerryObject(new VendorInvoice());
//			}else{
//				querry.setQuerryObject(new Invoice());
//			}
			if(!model.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
				querry.setQuerryObject(new Invoice());
			}
			form.showWaitSymbol();
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					if(result.size()==0){
						form.showDialogMessage("No invoice document found.");
						glassPanel.hide();
						return;
					}
					if(result.size()==1){
						/** date 25.6.2018 added by komal for vendor invoice form **/
//						if(model.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
//							final VendorInvoice billDocument=(VendorInvoice) result.get(0);
//							final VendorInvoiceDetailsForm form=VendorInvoiceDetailsPresenter.initalize();
//							Timer timer=new Timer() {
//								@Override
//								public void run() {
//									form.updateView(billDocument);
//									form.setToViewState();
//								}
//							};
//							timer.schedule(1000);
//						}else{
						if(!model.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){												
						final Invoice billDocument=(Invoice) result.get(0);
//						final InvoiceDetailsForm form=InvoiceDetailsPresenter.initalize();
						Timer timer=new Timer() {
							@Override
							public void run() {
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();
								
								InvoiceDetailsPresenter invoicePresenter=new InvoiceDetailsPresenter(form,new Invoice());
								presenter = invoicePresenter;
								form.updateView(billDocument);
								form.setPopUpAppMenubar(true);
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022
							
								form.setToViewState();
								presenter.setModel(billDocument);
								}
						};
						timer.schedule(1000);
						}
					}else if(!model.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
						/** date 25.6.2018 added by komal for vendor invoice list **/
//						if(model.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
//							VendorInvoiceListPresenter.initalize(querry);
//						}else{
//						InvoiceListPresenter.initalize(querry);
//						}
						//InvoiceListPresenter.initalize(querry);
						Console.log("calling Invoice list");
						AppMemory.getAppMemory().currentScreen = Screen.INVOICELIST;
						LoginPresenter.currentModule="Accounts";
						LoginPresenter.currentDocumentName="Invoice List";
						glassPanel.show();
						InvoiceListTable table=new InvoiceListTable();
						final InvoiceListForm form=new InvoiceListForm(table);
						/**
						 * @author Anil
						 * @since 07-01-2021
						 * Showing process level menus
						 */
						if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
							form.setAction(false);
							form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
							form.processLevelBar.setVisibleFalse(false);
							docVerticalPanel.add(form.processLevelBar.content);
						}
						docVerticalPanel.add(form.content);
						Timer timer = new Timer() {
							@Override
							public void run() {
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();

								CustomerPayment serviceEntity = (CustomerPayment) model;
//								form.updateView(serviceEntity);
							
								InvoiceListPresenter invoicePresenter=new InvoiceListPresenter(form, new Invoice(),querry);
								presenter = invoicePresenter;
								presenter.setModel(serviceEntity);
								
								form.setPopUpAppMenubar(true);
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022

								form.setToViewState();
								form.toggleAppHeaderBarMenu();
								glassPanel.hide();
							}
						};
						timer.schedule(1000);
			
					}
					
				}
			});
			form.hideWaitSymbol();
			form.toggleAppHeaderBarMenu();
			glassPanel.hide();
		}
		if (presenterName.equals("View Order")) {
			final BillingDocument model=(BillingDocument) model1;
			AppMemory.getAppMemory().currentScreen = Screen.SALESORDER;
			LoginPresenter.currentModule="Sales";
			LoginPresenter.currentDocumentName="Sales Order";
			glassPanel.show();
			final SalesOrderForm form = new SalesOrderForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);

			final MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(model.getContractCount());
			temp.add(filter);
			
			querry.setFilters(temp);
//			if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICE)){
//				querry.setQuerryObject(new Contract());
//			}else if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)){
//				querry.setQuerryObject(new SalesOrder());
//			}else if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPEPURCHASE)){
//				querry.setQuerryObject(new PurchaseOrder());
//			}else if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICEPO)){
//				querry.setQuerryObject(new ServicePo());
//			}
			if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)){
				querry.setQuerryObject(new SalesOrder());
			}
			
			/** date 26.10.2018 added by komal **/
			if(model.getSubBillType() != null && !model.getSubBillType().equals("")){
				if(checkBillSubType(model.getSubBillType())){
					querry.setQuerryObject(new CNC());
				}
			}
			form.showWaitSymbol();
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					if(result.size()==0){
						form.showDialogMessage("No order document found.");
						glassPanel.hide();
						return;
					}
					
//					if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICE)){
//						if(model.getSubBillType() != null && !model.getSubBillType().equals("")){
//							if(checkBillSubType(model.getSubBillType())){
//								final CNC billDocument=(CNC) result.get(0);
//								final CNCForm form=CNCPresenter.initalize();
//								Timer timer=new Timer() {
//									@Override
//									public void run() {
//										form.updateView(billDocument);
//										form.setToViewState();
//									}
//								};
//								timer.schedule(4000);
//							}
//						}else{
//						final Contract billDocument=(Contract) result.get(0);
//						final ContractForm form=ContractPresenter.initalize();
//						Timer timer=new Timer() {
//							@Override
//							public void run() {
//								form.updateView(billDocument);
//								form.setToViewState();
//							}
//						};
//						timer.schedule(1000);
//						}
//					}else if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)){
					if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)){
						final SalesOrder billDocument=(SalesOrder) result.get(0);
						Timer timer=new Timer() {
							@Override
							public void run() {
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();
								
								SalesOrderPresenter soPresenter=new SalesOrderPresenter(form,new SalesOrder());
								presenter = soPresenter;
								form.updateView(billDocument);
								
								form.setPopUpAppMenubar(true);
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022
							
								form.setToViewState();
								presenter.setModel(billDocument);
								
							}
						};
						timer.schedule(1000);
					}
//					else if(model.getTypeOfOrder().equals(AppConstants.ORDERTYPEPURCHASE)){
//						final PurchaseOrder billDocument=(PurchaseOrder) result.get(0);
//						final PurchaseOrderForm form=PurchaseOrderPresenter.initalize();
//						Timer timer=new Timer() {
//							@Override
//							public void run() {
//								form.updateView(billDocument);
//								form.setToViewState();
//							}
//						};
//						timer.schedule(1000);
//					}/***11-1-2019 added by amol***/
//					else if (model.getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICEPO)){
//						final ServicePo billDocument=(ServicePo) result.get(0);
//						final ServicePoForm form=ServicePoPresenter.initialize();
//						Timer timer=new Timer() {
//							@Override
//							public void run() {
//								form.updateView(billDocument);
//								form.setToViewState();
//							}
//						};
//						timer.schedule(1000);					
//					}										
				}
			});	
			form.toggleAppHeaderBarMenu();
			glassPanel.hide();
		}
		if(presenterName.equals("View Service")){
			final BillingDocument model=(BillingDocument) model1;
			AppMemory.getAppMemory().currentScreen = Screen.DEVICE;
			LoginPresenter.currentModule="Sales";
			LoginPresenter.currentDocumentName="Device";
			glassPanel.show();
			final DeviceForm form = new DeviceForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);

			final MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(model.getRateContractServiceId());
			temp.add(filter);
			
			querry.setFilters(temp);
			querry.setQuerryObject(new Service());
			form.showWaitSymbol();
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					if(result.size()==0){
						form.showDialogMessage("No service document found.");
						glassPanel.hide();
						return;
					}
					if(result.size()==1){
						final Service service=(Service) result.get(0);
						Timer timer=new Timer() {
							@Override
							public void run() {
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();
								
								DevicePresenter devicePresenter=new DevicePresenter(form,new Service());
								presenter = devicePresenter;
								form.updateView(service);
								form.setPopUpAppMenubar(true);
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022
							
								form.setToViewState();
								presenter.setModel(service);							
							}
						};
						timer.schedule(1000);
					}else{
						//ServicePresenter.initalize(querry);
	
						AppMemory.getAppMemory().currentScreen = Screen.SERVICE;
						LoginPresenter.currentModule="Sales";
						LoginPresenter.currentDocumentName="Service";
						glassPanel.show();
						ServiceTable table=new ServiceTable();
						final ServiceForm form=new ServiceForm(table);
						/**
						 * @author Anil
						 * @since 07-01-2021
						 * Showing process level menus
						 */
						if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
							form.setAction(false);
							form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
							form.processLevelBar.setVisibleFalse(false);
							docVerticalPanel.add(form.processLevelBar.content);
						}
						docVerticalPanel.add(form.content);
						Timer timer = new Timer() {
							@Override
							public void run() {
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();


								BillingDocument serviceEntity = (BillingDocument) model;
//								form.updateView(serviceEntity);
								
	
								ServicePresenter invoicePresenter=new ServicePresenter(form,new Service(),querry);
								presenter = invoicePresenter;
								presenter.setModel(serviceEntity);
								
								form.setPopUpAppMenubar(true);
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022							

								form.setToViewState();
//								form.toggleAppHeaderBarMenu();
//								glassPanel.hide();
							}
						};
						timer.schedule(1000);
					}
					
				}
			});
			
			form.toggleAppHeaderBarMenu();
			glassPanel.hide();
			
		}
		if(presenterName.equals("View Sales Order")){
			final DeliveryNote model=(DeliveryNote) model1;
			AppMemory.getAppMemory().currentScreen = Screen.SALESORDER;
			LoginPresenter.currentModule="Sales";
			LoginPresenter.currentDocumentName="Sales Order";
			glassPanel.show();
			final SalesOrderForm form = new SalesOrderForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);

			MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(model.getSalesOrderCount());
			temp.add(filter);
			
			querry.setFilters(temp);
			querry.setQuerryObject(new SalesOrder());
			form.showWaitSymbol();
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					if(result.size()==0){
						form.showDialogMessage("Sales order not found.");
						glassPanel.hide();
						return;
					}
					if(result.size()>0){
						final SalesOrder entity=(SalesOrder) result.get(0);
						
						Timer timer=new Timer() {
							@Override
							public void run() {
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();
								
								SalesOrderPresenter soPresenter=new SalesOrderPresenter(form,new SalesOrder());
								presenter = soPresenter;
								form.updateView(entity);
								form.setPopUpAppMenubar(true);
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022
							
								form.setToViewState();
								presenter.setModel(entity);
								
							}
						};
						timer.schedule(3000);
					}
				}
			});			
			form.toggleAppHeaderBarMenu();
			glassPanel.hide();			
		}
		if(presenterName.equals("Copy Order")){
			final SalesOrder model=(SalesOrder) model1;
			AppMemory.getAppMemory().currentScreen = Screen.SALESORDER;
			LoginPresenter.currentModule="Sales";
			LoginPresenter.currentDocumentName="Sales Order";
			glassPanel.show();
			final SalesOrderForm form = new SalesOrderForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);


			form.setToNewState();

			// form.showWaitSymbol();

			SalesLineItemTable.newBranchFlag = true;
			SalesOrderPresenterTable gentable = new SalesOrderPresenterTableProxy();
			gentable.setView(form);
			gentable.applySelectionModle();
			SalesOrderPresenterSearch.staticSuperTable = gentable;
			SalesOrderPresenterSearch searchpopup = new SalesOrderPresenterSearchProxy();
			form.setSearchpopupscreen(searchpopup);
			//AppMemory.getAppMemory().stickPnel(form);
			form.showWaitSymbol();
			Timer timer = new Timer() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					form.hideWaitSymbol();
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					SalesOrderPresenter soPresenter=new SalesOrderPresenter(form,new SalesOrder());
					presenter = soPresenter;

					SalesOrder salesOrder = new SalesOrder();

					salesOrder.setCount(0);
					salesOrder.setDescription("");
					salesOrder.setCreatedBy("");
					salesOrder.setStartDate(null);
					salesOrder.setEndDate(null);
					salesOrder.setStatus("Created");

					salesOrder.setCinfo(model.getCinfo());
					salesOrder.setRefNo(model.getRefNo());
					salesOrder.setReferedBy(model.getReferedBy());
					salesOrder.setDocument(model.getDocument());
					salesOrder.setDeliveryDate(model.getDeliveryDate());
					salesOrder.setSalesOrderDate(model.getSalesOrderDate());
					salesOrder.setApproverName(model.getApproverName());
					salesOrder.setTotalAmount(model.getTotalAmount());
					salesOrder.setNetpayable(model.getNetpayable());
					salesOrder.setLeadCount(model.getLeadCount());
					salesOrder.setContractCount(model.getContractCount());
					salesOrder.setQuotationCount(model.getQuotationCount());
					salesOrder.setCreditPeriod(model.getCreditPeriod());
					salesOrder.setPaymentMethod(model.getPaymentMethod());
					salesOrder.setEmployee(model.getEmployee());
					salesOrder.setPaymentTermsList(model.getPaymentTermsList());
					salesOrder.setProductTaxes(model.getProductTaxes());
					salesOrder.setProductCharges(model.getProductCharges());
					salesOrder.setDescription(model.getDescription());
					salesOrder.setDescriptiontwo(salesOrder.getDescriptiontwo());
					salesOrder.setCustomersaveflag(false);
					salesOrder.setNumberRange(salesOrder.getNumberRange());
					
					salesOrder.setInclutaxtotalAmount(model
							.getInclutaxtotalAmount());

					salesOrder.setShippingAddress(model.getShippingAddress());

					salesOrder.setBranch(model.getBranch());
					salesOrder.setEmployee(model.getEmployee());
					if (model.getPaymentMethod() != null)
						salesOrder.setPaymentMethod(model.getPaymentMethod());
					if (model.getNumberRange() != null)
						salesOrder.setNumberRange(model.getNumberRange());
					if (model.getCategory() != null)
						salesOrder.setCategory(model.getCategory());
					if (model.getType() != null)
						salesOrder.setType(model.getType());
					if (model.getGroup() != null)
						salesOrder.setGroup(model.getGroup());
					salesOrder.setApproverName(model.getApproverName());

					salesOrder.setItems(model.getItems());

					form.updateView(salesOrder);
					presenter.setModel(salesOrder);
					form.setPopUpAppMenubar(true);
					form.setHideNavigationButtons(hideNavigationButtons);//03-10-2022	
				}
			};
			timer.schedule(3000);
			
			form.toggleAppHeaderBarMenu();
			glassPanel.hide();		
			
		}
		if(presenterName.equals("Create Sales Order")){
			Console.log("inside if(presenterName.equals(Create Sales Order))");
			final SalesQuotation model=(SalesQuotation) model1;
			AppMemory.getAppMemory().currentScreen = Screen.SALESORDER;
			LoginPresenter.currentModule="Sales";
			LoginPresenter.currentDocumentName="Sales Order";
			glassPanel.show();
			final SalesOrderForm form = new SalesOrderForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);

			boolean validateCopyingForData=false;
			
			validateCopyingForData=CopyDataConfig.validateDataCopying(AppConstants.SALESORDERCOPYDATA);
						
			boolean categoryDataVal=false;
			boolean typeDataVal=false;
			boolean groupDataVal=false;			
			
			if(validateCopyingForData==true)
			{
				Console.log("in validateCopyingForData==true");
				categoryDataVal=CopyDataConfig.validateData(AppConstants.COPYCATEGORYDATA, model.getCategory(), Screen.SALESORDERCATEGORY);
				typeDataVal=CopyDataConfig.validateData(AppConstants.COPYTYPEDATA, model.getType(), Screen.SALESORDERTYPE);
				groupDataVal=CopyDataConfig.validateData(AppConstants.COPYGROUPDATA, model.getGroup(), Screen.SALESORDERGROUP);
												
				if(categoryDataVal==false){
					form.showDialogMessage("Sales Order Category Value does not exists in Quotation Category!");
					glassPanel.hide();
				}
				
				if(categoryDataVal==true&&typeDataVal==false){
					form.showDialogMessage("Sales Order Type value does not exists in Quotation Type!");
					glassPanel.hide();
				}
				
				if(groupDataVal==false&&typeDataVal==true&&categoryDataVal==true){
					form.showDialogMessage("Sales Order Group value does not exists in Quotation Group!");
					glassPanel.hide();
				}
				
				
				if(groupDataVal==true&&typeDataVal==true&&categoryDataVal==true){
					Console.log("in if(groupDataVal==true&&typeDataVal==true&&categoryDataVal==true)");
					switchToOrderScreen(validateCopyingForData,model,form);
				}
				
			}
			else{
				Console.log("in else - validateCopyingForData==false");
				switchToOrderScreen(validateCopyingForData,model,form);
			}					
		}
		if(presenterName.equals("Revised Quotation")){
			Console.log("Revised Quotation");
			final SalesQuotation model=(SalesQuotation) model1;
			AppMemory.getAppMemory().currentScreen = Screen.SALESQUOTATION;
			LoginPresenter.currentModule="Sales";
			LoginPresenter.currentDocumentName="Sales Quotation";
			glassPanel.show();
			final SalesQuotationForm form = new SalesQuotationForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			try {
				System.out.println("get id : " +model.getCount());
				//SalesQuotation quo = (SalesQuotation)model;
				final SalesQuotation salesQuotation=(SalesQuotation)getRevisedQuotationData(model);
				salesQuotation.setCount(0);
				salesQuotation.setContractCount(0);
				salesQuotation.setQuotationCount(0);
				salesQuotation.setQuotationDate(null);
				salesQuotation.setApprovalDate(null);
				salesQuotation.setCreationDate(null);
				salesQuotation.setValidUntill(null);
				salesQuotation.setStatus(SalesQuotation.CREATED);
				salesQuotation.setId(null);
				
				System.out.println("get id after : " +salesQuotation.getCount()+ " ID "+salesQuotation.getId() + " get value model  : "+ model.getCount() +" model Id ;;;; -- "+model.getId());
				Console.log("get id after : " +salesQuotation.getCount()+ " ID "+salesQuotation.getId() + " get value model  : "+ model.getCount() +" model Id ;;;; -- "+model.getId());
				AppMemory.getAppMemory().currentState=ScreeenState.NEW;
				form.setHideNavigationButtons(hideNavigationButtons);//03-10-2022	
				form.setToNewState();
				Console.log("after form.setToNewState()");
				 
				Timer timer = new Timer() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						
						/** Date 11-08-2018 by Vijay added for revised old quotation status change ***/
						salesQuotation.setOldQuotationId(model.getCount());
						
						setSizeOfPanel(form);
						viewDocumentPanel.show();
						viewDocumentPanel.center();
						glassPanel.hide();
						Console.log("glassPanel.hide() called");
						
						SalesQuotationPresenter sqPresenter=new SalesQuotationPresenter(form,new SalesQuotation());
						presenter = sqPresenter;
						sqPresenter.setModel(salesQuotation);
						
						ApprovableProcess businessprocess=(ApprovableProcess) form.getPresenter().getModel();
						ManageApprovals approvals=new ManageApprovals(businessprocess, form);
						form.setManageapproval(approvals);
						
						Console.log("after presenter obj creation");
						AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
						//form.updateView(salesQuotation);
						form.updateRevisedQuotationView(salesQuotation);
						Console.log("after updateRevisedQuotationView(salesQuotation)");
						
						presenter.setModel(salesQuotation);
						Console.log("presenter.setModel(salesQuotation)");
												
						form.setToEditState();
						Console.log("after form.setToEditState()");
			
						//form.toggleAppHeaderBarMenu();	
						form.getTbQuotatinId().setValue("");
						form.getTbContractId().setValue("");
						form.getTbQuotatinId().setValue("");
						form.getDbquotationDate().setValue(null);
						form.getDbValidUntill().setValue(null);
						form.getDbdeliverydate().setValue(null);
						
						
						form.setPopUpAppMenubar(true);
						form.toggleAppHeaderBarMenu();
						
												
					}
				};
				timer.schedule(3000);
			
			} 
			catch (Exception e) {
				System.out.println(e.getMessage());
				Console.log("catched error:"+e.toString());
				e.printStackTrace();
			}
	
			Console.log("end of revised quotation");
		}
		if(presenterName.equals("View Document")){
			try{
				Console.log("in view document");
				final Service model=(Service) model1;	  
							
				AppMemory.getAppMemory().currentScreen = Screen.DEVICE;
				LoginPresenter.currentModule="Service";
				LoginPresenter.currentDocumentName="Device";
				glassPanel.show();
				final DeviceForm form = new DeviceForm();
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
					form.processLevelBar.content.setWidth("80%");
				}
				docVerticalPanel.add(form.content);
				
				AppMemory.getAppMemory().currentState = ScreeenState.VIEW;
				//AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Customer Service", Screen.DEVICE);
				DeviceForm.flag = false;
				final Service serviceEntity = model;//selectedlist.get(0);
				Console.log("after final Service serviceEntity = selectedlist.get(0)");
				//final DeviceForm form = DevicePresenter.initalize();
				form.showWaitSymbol();
				//AppMemory.getAppMemory().stickPnel(form);
				Timer timer = new Timer() {
					@Override
					public void run() {
						
						form.hideWaitSymbol();
						setSizeOfPanel(form);
						viewDocumentPanel.show();
						viewDocumentPanel.center();
						
						DevicePresenter devicePresenter=new DevicePresenter(form,new Service());
						presenter = devicePresenter;
						Console.log("after presenter = devicePresenter"+presenter.getView().toString());						
						form.updateView(serviceEntity);
						presenter.setModel(serviceEntity);
						
						form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022
					
						form.setToViewState();
						for(int i=0;i<form.getProcesslevelBarNames().length;i++)
						{
							InlineLabel label=form.getProcessLevelBar().btnLabels[i];
							String text=label.getText().trim();
							
							if(text.equals("New")||text.equals("Manage Customer Project")||text.equals("View Bill")||text.equals("View Order")){
								label.setVisible(false);
							}
							
							Console.log("in process level bar names loop");
						}
						form.setPopUpAppMenubar(true);
						form.setSummaryPopupFlag(true);
						form.toggleAppHeaderBarMenu();						
						glassPanel.hide();
					}
				};
				timer.schedule(3000);
			}
			catch(Exception e){
				e.printStackTrace();
				Console.log("error"+e.getMessage());
			}
								 
		}
		if(presenterName.equals("Contract")){
			try{
				final Service model=(Service) model1;
				AppMemory.getAppMemory().currentScreen = Screen.CONTRACT;
				LoginPresenter.currentModule="Service";
				LoginPresenter.currentDocumentName="Contract";
				glassPanel.show();
				final ContractForm form = new ContractForm();
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
					form.processLevelBar.content.setWidth("80%");
				}
				docVerticalPanel.add(form.content);
				

				MyQuerry querry = new MyQuerry();
				Vector<Filter> filterVec = new Vector<Filter>();
				Filter filter;
				
				filter = new Filter();
				filter.setQuerryString("count");
				filter.setIntValue(model.getContractCount());//(selectedlist.get(0).getContractCount());
				filterVec.add(filter);
				
				querry.setFilters(filterVec);
				querry.setQuerryObject(new Contract());
				form.showWaitSymbol();
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						Console.log("in on success");
						form.hideWaitSymbol();					
						if(result.size()==0){
							form.showDialogMessage("Contract Document Not Found!");
//							glassPanel.hide();
						}else{
							Console.log("in else");
							final Contract contract = (Contract) result.get(0);
							//final ContractForm form = ContractPresenter.initalize();
							Timer timer = new Timer() {
								
								@Override
								public void run() {
									Console.log("in run");
									setSizeOfPanel(form);
									viewDocumentPanel.show();
									viewDocumentPanel.center();
									
									ContractPresenter contractPresenter=new ContractPresenter(form,new Contract());
									presenter = contractPresenter;
									Console.log("after presenter = contractPresenter"+presenter.getView().toString());
									form.updateView(contract);								
									presenter.setModel(contract);
									form.setPopUpAppMenubar(true);
									form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022
								
									form.setToViewState();
									form.toggleAppHeaderBarMenu();
									glassPanel.hide();
									
								}
							};
							timer.schedule(7000); // Changed timer from 1 sec to 7 sec to give proper initialization time
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}
				});
			
				
				
			}
			catch(Exception e){
				e.printStackTrace();
				Console.log("error"+e.getMessage());
			
			}
			
		}
		if(presenterName.equals("View Bill From Device")){
			final Service model=(Service) model1;
			AppMemory.getAppMemory().currentScreen = Screen.BILLINGDETAILS;
			LoginPresenter.currentModule="Service";
			LoginPresenter.currentDocumentName="Bill";
			glassPanel.show();
			final BillingDetailsForm form = new BillingDetailsForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
			
			final MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("rateContractServiceId");
			filter.setIntValue(model.getCount());
			temp.add(filter);
			
			querry.setFilters(temp);
			querry.setQuerryObject(new BillingDocument());
			form.showWaitSymbol();
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					if(result.size()==0){
						form.showDialogMessage("No billing document found.");
						glassPanel.hide();
						return;
					}
					if(result.size()==1){
						final BillingDocument billDocument=(BillingDocument) result.get(0);
						
						Timer timer=new Timer() {
							@Override
							public void run() {
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();
								
								BillingDetailsPresenter billPresenter=new BillingDetailsPresenter(form,new BillingDocument());
								presenter = billPresenter;
								
								form.updateView(billDocument);
								presenter.setModel(billDocument);
								form.setPopUpAppMenubar(true);
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022
							
								form.setToViewState();														
							
								
								form.toggleAppHeaderBarMenu();
								glassPanel.hide();
							}
						};
						timer.schedule(1000);
					}else{
					//BillingListPresenter.initalize(querry);
					Console.log("calling Billing list");
					AppMemory.getAppMemory().currentScreen = Screen.BILLINGLIST;
					LoginPresenter.currentModule="Accounts";
					LoginPresenter.currentDocumentName="Billing List";
					glassPanel.show();
					BillingListTable table=new BillingListTable();
					final BillingListForm form=new BillingListForm(table);
					/**
					 * @author Anil
					 * @since 07-01-2021
					 * Showing process level menus
					 */
					if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
						form.setAction(false);
						form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
						form.processLevelBar.setVisibleFalse(false);
						docVerticalPanel.add(form.processLevelBar.content);
					}
					docVerticalPanel.add(form.content);
					Timer timer = new Timer() {
						@Override
						public void run() {
							setSizeOfPanel(form);
							viewDocumentPanel.show();
							viewDocumentPanel.center();

							Service serviceEntity = (Service) model;
//							form.updateView(serviceEntity);
															
							BillingListPresenter billingListpresenter=new BillingListPresenter(form, new BillingDocument(),querry);
							presenter = billingListpresenter;
							presenter.setModel(serviceEntity);
							
							form.setPopUpAppMenubar(true);
							form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022
						

							form.setToViewState();
							form.toggleAppHeaderBarMenu();
							glassPanel.hide();
						}
					};
					timer.schedule(1000);
				
				
					}
					
				}
			});

		}
		if(presenterName.equals("Manage Customer Project")){
			Console.log("inside if(presenterName.equals(Manage Customer Project)");
			final Service model=(Service) model1;
			AppMemory.getAppMemory().currentScreen = Screen.PROJECT;
			LoginPresenter.currentModule="Service";
			LoginPresenter.currentDocumentName="Project";
			glassPanel.show();
			
			createProjectCorrespondingToService("Button", model);
   		
		}
		if(presenterName.equals("ViewOrderFromServiceLead")){
			try{
				Console.log("step1-navigated to ViewOrderFromServiceLead");
				final Lead model=(Lead) model1;
				AppMemory.getAppMemory().currentScreen = Screen.CONTRACT;
				LoginPresenter.currentModule="Service";
				LoginPresenter.currentDocumentName="Contract";
				glassPanel.show();
				final ContractForm form = new ContractForm();
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
					form.processLevelBar.content.setWidth("80%");
				}
				docVerticalPanel.add(form.content);
				
				Console.log("step2-before query");
				if(model.getContractCount()==null) {
					form.showDialogMessage("No order document found.");
					glassPanel.hide();
					return;
				}
				final MyQuerry querry=new MyQuerry();
				Vector<Filter> temp=new Vector<Filter>();
				Filter filter=null;
				
				filter=new Filter();
				filter.setQuerryString("count");
				filter.setIntValue(Integer.parseInt(model.getContractCount()));
				temp.add(filter);
				
				querry.setFilters(temp);
				querry.setQuerryObject(new Contract());
				Console.log("step3-Query executed");
				form.showWaitSymbol();
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						Console.log("step4-in on success");
						form.hideWaitSymbol();					
						if(result.size()==0){
							form.showDialogMessage("No order document found.");
							glassPanel.hide();
						}else{
							Console.log("in else");
							final Contract contract = (Contract) result.get(0);
							//final ContractForm form = ContractPresenter.initalize();
							Timer timer = new Timer() {
								
								@Override
								public void run() {
									Console.log("in run");
									setSizeOfPanel(form);
									viewDocumentPanel.show();
									viewDocumentPanel.center();
									
									ContractPresenter contractPresenter=new ContractPresenter(form,new Contract());
									presenter = contractPresenter;
									Console.log("after presenter = contractPresenter"+presenter.getView().toString());
									form.updateView(contract);								
									presenter.setModel(contract);
									form.setPopUpAppMenubar(true);
									form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022
								
									form.setToViewState();
									
									form.toggleAppHeaderBarMenu();
									glassPanel.hide();
									
								}
							};
							timer.schedule(7000); // Changed timer from 1 sec to 7 sec to give proper initialization time
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}
				});
			
				
				
			}
			catch(Exception e){
				e.printStackTrace();
				Console.log("error"+e.getMessage());
			
			}
			
		}
		
		if(presenterName.equals("ViewLeadFromServiceQuotation")){
			try{
				final Quotation model=(Quotation) model1;
				AppMemory.getAppMemory().currentScreen = Screen.LEAD;
				LoginPresenter.currentModule="Service";
				LoginPresenter.currentDocumentName="Lead";
				glassPanel.show();
				final LeadForm form = new LeadForm();
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
					form.processLevelBar.content.setWidth("80%");
				}
				docVerticalPanel.add(form.content);
				

				
				final MyQuerry querry=new MyQuerry();
				Vector<Filter> temp=new Vector<Filter>();
				Filter filter=null;
				
				filter=new Filter();
				filter.setQuerryString("count");
				filter.setIntValue(model.getLeadCount());
				temp.add(filter);

				querry.setFilters(temp);
				querry.setQuerryObject(new Lead());
				Console.log("Query executed");
				form.showWaitSymbol();
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						Console.log("ashiwini in on success");
						form.hideWaitSymbol();					
						if(result.size()==0){
							form.showDialogMessage("No Lead document found.");
							glassPanel.hide();
						}else{
							Console.log("ashiwini in else");
							final Lead lead = (Lead) result.get(0);
							//final ContractForm form = ContractPresenter.initalize();
							Timer timer = new Timer() {
								
								@Override
								public void run() {
									Console.log("ashiwini in run");
									setSizeOfPanel(form);
									viewDocumentPanel.show();
									viewDocumentPanel.center();
									
									LeadPresenter leadPresenter=new LeadPresenter(form,new Lead());
									presenter = leadPresenter;
									form.updateView(lead);								
									presenter.setModel(lead);
									form.setPopUpAppMenubar(true);
									form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022
								
									form.setToViewState();
									
									form.toggleAppHeaderBarMenu();
									Console.log("ashiwini before glassPanel.hide");
									glassPanel.hide();
									
								}
							};
							timer.schedule(5000); // Changed timer from 1 sec to 7 sec to give proper initialization time
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}
				});
			
				
				
			}
			catch(Exception e){
				e.printStackTrace();
				Console.log("error"+e.getMessage());
			
			}
			
		}
		if(presenterName.equals("ViewLeadFromServiceContract")){
			try{
				final Contract model=(Contract) model1;
				AppMemory.getAppMemory().currentScreen = Screen.LEAD;
				LoginPresenter.currentModule="Service";
				LoginPresenter.currentDocumentName="Lead";
				glassPanel.show();
				final LeadForm form = new LeadForm();
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
					form.processLevelBar.content.setWidth("80%");
				}
				docVerticalPanel.add(form.content);
				

				
				final MyQuerry querry=new MyQuerry();
				Vector<Filter> temp=new Vector<Filter>();
				Filter filter=null;
				
				
				filter=new Filter();
				filter.setQuerryString("count");
				filter.setIntValue(model.getLeadCount());
				temp.add(filter);
				
				Console.log("lead id"+model.getLeadCount());
				querry.setFilters(temp);
				querry.setQuerryObject(new Lead());
				Console.log("Query executed");
				form.showWaitSymbol();
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						Console.log("in on success");
						form.hideWaitSymbol();					
						if(result.size()==0){
							form.showDialogMessage("No Lead document found.");
							glassPanel.hide();
						}else{
							Console.log("in else");
							final Lead lead = (Lead) result.get(0);
							//final ContractForm form = ContractPresenter.initalize();
							Timer timer = new Timer() {
								
								@Override
								public void run() {
									Console.log("in run");
									setSizeOfPanel(form);
									viewDocumentPanel.show();
									viewDocumentPanel.center();
									
									LeadPresenter leadPresenter=new LeadPresenter(form,new Lead());
									presenter = leadPresenter;
									form.updateView(lead);	
									form.setPopUpAppMenubar(true);
									form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022
								
									presenter.setModel(lead);
									form.setToViewState();
									
									form.toggleAppHeaderBarMenu();
									glassPanel.hide();
									
								}
							};
							timer.schedule(5000); // Changed timer from 1 sec to 7 sec to give proper initialization time
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}
				});
			
				
				
			}
			catch(Exception e){
				e.printStackTrace();
				Console.log("error"+e.getMessage());
			
			}
			
		}
		
		if(presenterName.equals("ViewQuotationFromServiceContract")){
			try{
				final Contract model=(Contract) model1;
				AppMemory.getAppMemory().currentScreen = Screen.QUOTATION;
				LoginPresenter.currentModule="Service";
				LoginPresenter.currentDocumentName="Quotation";
				glassPanel.show();
				final QuotationForm form = new QuotationForm();
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
					form.processLevelBar.content.setWidth("80%");
				}
				docVerticalPanel.add(form.content);
				
				
				

				final MyQuerry querry=new MyQuerry();
				Vector<Filter> temp=new Vector<Filter>();
				Filter filter=null;
				
				filter=new Filter();
				filter.setQuerryString("count");
				filter.setIntValue(model.getQuotationCount());
				Console.log("quotation id : "+model.getQuotationCount());
				temp.add(filter);
				
				querry.setFilters(temp);
				querry.setQuerryObject(new Quotation());
				Console.log("Query executed");
				form.showWaitSymbol();
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						Console.log("in on success");
						form.hideWaitSymbol();					
						if(result.size()==0){
							form.showDialogMessage("No Quotation document found.");
							glassPanel.hide();
						}else{
							Console.log("in else");
							final Quotation quot = (Quotation) result.get(0);
							//final ContractForm form = ContractPresenter.initalize();
							Timer timer = new Timer() {
								
								@Override
								public void run() {
									Console.log("in run");
									setSizeOfPanel(form);
									viewDocumentPanel.show();
									viewDocumentPanel.center();
									
									QuotationPresenter quotPresenter=new QuotationPresenter(form,new Quotation());
									presenter = quotPresenter;
									form.updateView(quot);								
									presenter.setModel(quot);
									form.setPopUpAppMenubar(true);
									form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022
								
									form.setToViewState();
									
									form.toggleAppHeaderBarMenu();
									glassPanel.hide();
									
								}
							};
							timer.schedule(5000); // Changed timer from 1 sec to 7 sec to give proper initialization time
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}
				});
			
				
				
			}
			catch(Exception e){
				e.printStackTrace();
				Console.log("error"+e.getMessage());
			
			}
			
		}
		
		
		if(presenterName.equals("ViewOrderFromAccountsBill")){ //02-09-2022
			try{
				Console.log("step1-navigated to ViewOrderFromServiceLead");
				final BillingDocument model=(BillingDocument) model1;
				AppMemory.getAppMemory().currentScreen = Screen.CONTRACT;
				LoginPresenter.currentModule="Service";
				LoginPresenter.currentDocumentName="Contract";
				glassPanel.show();
				final ContractForm form = new ContractForm();
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
					form.processLevelBar.content.setWidth("80%");
				}
				docVerticalPanel.add(form.content);
				
				Console.log("step2-before query");
				if(model.getContractCount()==null) {
					form.showDialogMessage("No order document found.");
					glassPanel.hide();
					return;
				}
				final MyQuerry querry=new MyQuerry();
				Vector<Filter> temp=new Vector<Filter>();
				Filter filter=null;
				
				filter=new Filter();
				filter.setQuerryString("count");
				filter.setIntValue(model.getContractCount());
				temp.add(filter);
				
				querry.setFilters(temp);
				querry.setQuerryObject(new Contract());
				
				Console.log("step3-Query executed");
				form.showWaitSymbol();
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						Console.log("step4-in on success");
						form.hideWaitSymbol();					
						if(result.size()==0){
							form.showDialogMessage("No order document found.");
							glassPanel.hide();
						}else{
							Console.log("in else");
							final Contract contract = (Contract) result.get(0);
							//final ContractForm form = ContractPresenter.initalize();
							Timer timer = new Timer() {
								
								@Override
								public void run() {
									Console.log("in run");
									setSizeOfPanel(form);
									viewDocumentPanel.show();
									viewDocumentPanel.center();
									
									ContractPresenter contractPresenter=new ContractPresenter(form,new Contract());
									presenter = contractPresenter;
									form.updateView(contract);								
									presenter.setModel(contract);
									form.setPopUpAppMenubar(true);
									form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022
								
									form.setToViewState();
									
									form.toggleAppHeaderBarMenu();
									glassPanel.hide();
									
								}
							};
							timer.schedule(7000); // Changed timer from 1 sec to 7 sec to give proper initialization time
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}
				});
			
				
				
			}
			catch(Exception e){
				e.printStackTrace();
				Console.log("error"+e.getMessage());
			
			}
			
		}
		if(presenterName.equals("ViewOrderFromAccountsInvoice")){//02-09-2022
			try{
				Console.log("step1-navigated to ViewOrderFromServiceLead");
				final Invoice model=(Invoice) model1;
				AppMemory.getAppMemory().currentScreen = Screen.CONTRACT;
				LoginPresenter.currentModule="Service";
				LoginPresenter.currentDocumentName="Contract";
				glassPanel.show();
				final ContractForm form = new ContractForm();
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
					form.processLevelBar.content.setWidth("80%");
				}
				docVerticalPanel.add(form.content);
				
				Console.log("step2-before query");
				if(model.getContractCount()==null) {
					form.showDialogMessage("No order document found.");
					glassPanel.hide();
					return;
				}
				final MyQuerry querry=new MyQuerry();
				Vector<Filter> temp=new Vector<Filter>();
				Filter filter=null;
				
				filter=new Filter();
				filter.setQuerryString("count");
				filter.setIntValue(model.getContractCount());
				temp.add(filter);
				
				querry.setFilters(temp);
				querry.setQuerryObject(new Contract());
				
				Console.log("step3-Query executed");
				form.showWaitSymbol();
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						Console.log("step4-in on success");
						form.hideWaitSymbol();					
						if(result.size()==0){
							form.showDialogMessage("No order document found.");
//							glassPanel.hide();
						}else{
							Console.log("in else");
							final Contract contract = (Contract) result.get(0);
							//final ContractForm form = ContractPresenter.initalize();
							Timer timer = new Timer() {
								
								@Override
								public void run() {
									Console.log("in run");
									setSizeOfPanel(form);
									viewDocumentPanel.show();
									viewDocumentPanel.center();
									
									ContractPresenter contractPresenter=new ContractPresenter(form,new Contract());
									presenter = contractPresenter;
									Console.log("after presenter = contractPresenter"+presenter.getView().toString());
									form.updateView(contract);								
									presenter.setModel(contract);
									form.setPopUpAppMenubar(true);
									form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022
								
									form.setToViewState();
									
									form.toggleAppHeaderBarMenu();
									glassPanel.hide();
									
								}
							};
							timer.schedule(7000); // Changed timer from 1 sec to 7 sec to give proper initialization time
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}
				});
			
				
				
			}
			catch(Exception e){
				e.printStackTrace();
				Console.log("error"+e.getMessage());
			
			}
			
		}
		if(presenterName.equals("ViewOrderFromAccountsPayment")){//02-09-2022
			try{
				Console.log("step1-navigated to ViewOrderFromServiceLead");
				final CustomerPayment model=(CustomerPayment) model1;
				AppMemory.getAppMemory().currentScreen = Screen.CONTRACT;
				LoginPresenter.currentModule="Service";
				LoginPresenter.currentDocumentName="Contract";
				glassPanel.show();
				final ContractForm form = new ContractForm();
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
					form.processLevelBar.content.setWidth("80%");
				}
				docVerticalPanel.add(form.content);
				
				Console.log("step2-before query");
				if(model.getContractCount()==null) {
					form.showDialogMessage("No order document found.");
					glassPanel.hide();
					return;
				}
				final MyQuerry querry=new MyQuerry();
				Vector<Filter> temp=new Vector<Filter>();
				Filter filter=null;
				
				filter=new Filter();
				filter.setQuerryString("count");
				filter.setIntValue(model.getContractCount());
				temp.add(filter);
				
				querry.setFilters(temp);
				querry.setQuerryObject(new Contract());
				
				Console.log("step3-Query executed");
				form.showWaitSymbol();
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						Console.log("step4-in on success");
						form.hideWaitSymbol();					
						if(result.size()==0){
							form.showDialogMessage("No order document found.");
							glassPanel.hide();
						}else{
							Console.log("in else");
							final Contract contract = (Contract) result.get(0);
							//final ContractForm form = ContractPresenter.initalize();
							Timer timer = new Timer() {
								
								@Override
								public void run() {
									Console.log("in run");
									setSizeOfPanel(form);
									viewDocumentPanel.show();
									viewDocumentPanel.center();
									
									ContractPresenter contractPresenter=new ContractPresenter(form,new Contract());
									presenter = contractPresenter;
									Console.log("after presenter = contractPresenter"+presenter.getView().toString());
									form.updateView(contract);								
									presenter.setModel(contract);
									form.setPopUpAppMenubar(true);
									form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022
								
									form.setToViewState();
									
									form.toggleAppHeaderBarMenu();
									glassPanel.hide();
									
								}
							};
							timer.schedule(7000); // Changed timer from 1 sec to 7 sec to give proper initialization time
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}
				});
			
				
				
			}
			catch(Exception e){
				e.printStackTrace();
				Console.log("error"+e.getMessage());
			
			}
			
		}
		
		if (presenterName.equals("ViewPaymentFromAccountsBill")) {//02-09-2022
			final BillingDocument model=(BillingDocument) model1;
			AppMemory.getAppMemory().currentScreen = Screen.PAYMENTDETAILS;
			LoginPresenter.currentModule="Accounts";
			LoginPresenter.currentDocumentName="View Payment";
			glassPanel.show();
			final PaymentDetailsForm form = new PaymentDetailsForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);
					
			final MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("invoiceCount");
			filter.setIntValue(model.getInvoiceCount());
			temp.add(filter);
			
			filter=new Filter();
			filter.setQuerryString("accountType");
			filter.setStringValue("AR");
			temp.add(filter);
			
			
			querry.setFilters(temp);
			querry.setQuerryObject(new CustomerPayment());
			form.showWaitSymbol();
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					form.hideWaitSymbol();
					if(result.size()==0){
						form.showDialogMessage("No payment document found.");
						glassPanel.hide();
						return;
					}
					if(result.size()==1){
						final CustomerPayment billDocument=(CustomerPayment) result.get(0);
						//final PaymentDetailsForm form=PaymentDetailsPresenter.initalize();
						Timer timer=new Timer() {
							@Override
							public void run() {
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();
								
								PaymentDetailsPresenter paymentPresenter=new PaymentDetailsPresenter(form,new CustomerPayment());
								presenter = paymentPresenter;
								form.updateView(billDocument);
								form.setPopUpAppMenubar(true);
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022
							
								form.setToViewState();
								presenter.setModel(billDocument);
								
							}
						};
						timer.schedule(1000);
					}else{
						//PaymentListPresenter.initalize(querry);
						Console.log("calling Payment list");
						AppMemory.getAppMemory().currentScreen = Screen.PAYMENTLIST;
						LoginPresenter.currentModule="Accounts";
						LoginPresenter.currentDocumentName="Payment List";
						glassPanel.show();
						PaymentListTable table=new PaymentListTable();
						final PaymentListForm form=new PaymentListForm(table);
						/**
						 * @author Anil
						 * @since 07-01-2021
						 * Showing process level menus
						 */
						if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
							form.setAction(false);
							form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
							form.processLevelBar.setVisibleFalse(false);
							docVerticalPanel.add(form.processLevelBar.content);
						}
						docVerticalPanel.add(form.content);
						Timer timer = new Timer() {
							@Override
							public void run() {
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();

								BillingDocument serviceEntity = (BillingDocument) model;
//								form.updateView(serviceEntity);
								
								PaymentListPresenter plpresenter=new PaymentListPresenter(form, new CustomerPayment(),querry);
								presenter = plpresenter;
								presenter.setModel(serviceEntity);
								
								form.setPopUpAppMenubar(true);
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022
							

								form.setToViewState();
						
							}
						};
						timer.schedule(1000);
					}
					form.hideWaitSymbol();
					form.toggleAppHeaderBarMenu();
					glassPanel.hide();
					
				}
			});
						
		}
		if (presenterName.equals("ViewBillFromAccountsPayment")) {//02-09-2022
			
			final CustomerPayment model=(CustomerPayment) model1;

			AppMemory.getAppMemory().currentScreen = Screen.BILLINGDETAILS;
			LoginPresenter.currentModule="Accounts";//changed from service to accounts
			LoginPresenter.currentDocumentName="Bill";
			glassPanel.show();
			final BillingDetailsForm form = new BillingDetailsForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			
					
			
			final MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("invoiceCount");
			filter.setIntValue(model.getInvoiceCount());
			temp.add(filter);
			
			querry.setFilters(temp);
			querry.setQuerryObject(new BillingDocument());
			form.showWaitSymbol();
			
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					form.hideWaitSymbol();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					
					form.hideWaitSymbol();
					if(result.size()==0){
						form.showDialogMessage("No billing document found.");
						glassPanel.hide();
						return;
					}
					if(result.size()==1){
						final BillingDocument billDocument=(BillingDocument) result.get(0);
						//final BillingDetailsForm form=BillingDetailsPresenter.initalize();					
						Timer timer=new Timer() {
							@Override
							public void run() {
								docVerticalPanel.add(form.content);
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();
								
								BillingDetailsPresenter billingPresenter=new BillingDetailsPresenter(form,new BillingDocument());
								presenter = billingPresenter;
								form.setHideNavigationButtons(hideNavigationButtons);//03-10-2022	
//								form.setToNewState();
								form.updateView(billDocument);
								presenter.setModel(billDocument); //added as per other if blocks
								form.setPopUpAppMenubar(true);//added as per other if blocks
								form.setToViewState();//13-10-2022
//								form.toggleAppHeaderBarMenu();//commented on 19-10-2022
							}
						};
						timer.schedule(1000);
					}else{
						
						//BillingListPresenter.initalize(querry);
						final CustomerPayment model=(CustomerPayment) model1;
						Console.log("calling Billing list");

						AppMemory.getAppMemory().currentScreen = Screen.BILLINGLIST;
						LoginPresenter.currentModule="Accounts";
						LoginPresenter.currentDocumentName="Billing List";
						glassPanel.show();
						BillingListTable table=new BillingListTable();
						final BillingListForm form=new BillingListForm(table);
						/**
						 * @author Anil
						 * @since 07-01-2021
						 * Showing process level menus
						 */
						if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
							form.setAction(false);
							form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
							form.processLevelBar.setVisibleFalse(false);
							docVerticalPanel.add(form.processLevelBar.content);
						}
						docVerticalPanel.add(form.content);
						Timer timer = new Timer() {
							@Override
							public void run() {
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();

								CustomerPayment serviceEntity = (CustomerPayment) model;
//								form.updateView(serviceEntity);
								
								
								BillingListPresenter billingListpresenter=new BillingListPresenter(form, new BillingDocument(),querry);
								presenter = billingListpresenter;
								presenter.setModel(serviceEntity);
								
								form.setPopUpAppMenubar(true);
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022

								form.setToViewState();
								form.toggleAppHeaderBarMenu();
								glassPanel.hide();
							}
						};
						timer.schedule(1000);
					}
					form.hideWaitSymbol();
					
					glassPanel.hide();
				}
			});
			
		}
		
		if(presenterName.equals("ViewOrderFromDevice")){//05-09-2022
			try{
				Console.log("step1-navigated to ViewOrderFromDevice");
				final Service model=(Service) model1;
				AppMemory.getAppMemory().currentScreen = Screen.CONTRACT;
				LoginPresenter.currentModule="Service";
				LoginPresenter.currentDocumentName="Contract";
				glassPanel.show();
				final ContractForm form = new ContractForm();
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
					form.processLevelBar.content.setWidth("80%");
				}
				docVerticalPanel.add(form.content);
				
				Console.log("step2-before query");
				if(model.getContractCount()==null) {
					form.showDialogMessage("No order document found.");
					glassPanel.hide();
					return;
				}
				MyQuerry querry = new MyQuerry();
				Vector<Filter> filterVec = new Vector<Filter>();
				Filter filter;
				
				filter = new Filter();
				filter.setQuerryString("count");
				filter.setIntValue(model.getContractCount());
				filterVec.add(filter);
				
				querry.setFilters(filterVec);
				querry.setQuerryObject(new Contract());
				
				Console.log("step3-Query executed");
				form.showWaitSymbol();
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						Console.log("step4-in on success");
						form.hideWaitSymbol();					
						if(result.size()==0){
							form.showDialogMessage("Contract Document Not Found!");
//							glassPanel.hide();
						}else{
							Console.log("in else");
							final Contract contract = (Contract) result.get(0);
							//final ContractForm form = ContractPresenter.initalize();
							Timer timer = new Timer() {
								
								@Override
								public void run() {
									Console.log("in run");
									setSizeOfPanel(form);
									viewDocumentPanel.show();
									viewDocumentPanel.center();
									
									ContractPresenter contractPresenter=new ContractPresenter(form,new Contract());
									presenter = contractPresenter;
									Console.log("after presenter = contractPresenter"+presenter.getView().toString());
									form.updateView(contract);								
									presenter.setModel(contract);
									form.setPopUpAppMenubar(true);
									form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022
								
									form.setToViewState();
									
									form.toggleAppHeaderBarMenu();
									glassPanel.hide();
									
								}
							};
							timer.schedule(7000); // Changed timer from 1 sec to 7 sec to give proper initialization time
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}
				});
			
				
				
			}
			catch(Exception e){
				e.printStackTrace();
				Console.log("error"+e.getMessage());
			
			}
			
		}
		
		if (presenterName.equals("ViewServiceFromAccountsBill")) {
			try{
				BillingDocument model=(BillingDocument) model1;
	
				AppMemory.getAppMemory().currentScreen = Screen.SERVICE;
				LoginPresenter.currentModule="Service";
				LoginPresenter.currentDocumentName="Customer Service List";
				glassPanel.show();
				ServiceTable table=new ServiceTable();
				final ServiceForm form=new ServiceForm(table);
				
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
				}
				
		
				final MyQuerry querry=new MyQuerry();
				Vector<Filter> temp=new Vector<Filter>();
				Filter filter=null;
				
				filter=new Filter();
				filter.setQuerryString("count");
				filter.setIntValue(model.getRateContractServiceId());
				temp.add(filter);
				
				querry.setFilters(temp);
				querry.setQuerryObject(new Service());
				form.showWaitSymbol();
				
				genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						form.hideWaitSymbol();
					}

					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						form.hideWaitSymbol();
						if(result.size()==0){
							form.showDialogMessage("No service document found.");
							glassPanel.hide();
							return;
						}
						if(result.size()==1){
							final Service service=(Service) result.get(0);
							final DeviceForm form=new DeviceForm();
							if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
								form.setAction(false);
								form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
								form.processLevelBar.setVisibleFalse(false);
								docVerticalPanel.add(form.processLevelBar.content);
							}
							docVerticalPanel.add(form.content);
							Timer timer=new Timer() {
								@Override
								public void run() {									
									setSizeOfPanel(form);
									viewDocumentPanel.show();
									viewDocumentPanel.center();
									DevicePresenter devicePresenter=new DevicePresenter(form,new Service());
									presenter = devicePresenter;
									form.updateView(service);
									form.setPopUpAppMenubar(true);
									form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022											
									form.setToViewState();
									form.toggleAppHeaderBarMenu();
									glassPanel.hide();
								}
							};
							timer.schedule(1000);
						}else{
							Timer timer = new Timer() {
								@Override
								public void run() {
									docVerticalPanel.add(form.content);
									setSizeOfPanel(form);
									viewDocumentPanel.show();
									viewDocumentPanel.center();
									ServicePresenter servicePresenter=new ServicePresenter(form,new Service(),querry);
									presenter = servicePresenter;
									form.setPopUpAppMenubar(true);
									form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022											
									form.setToViewState();
									form.toggleAppHeaderBarMenu();
									glassPanel.hide();
								}
							};
							timer.schedule(1000);
						}
						
					}
				});
								
			}		
			catch(Exception e)
			{
				e.printStackTrace();
				Console.log(e.getMessage());				
			}
									
		}
		
		if(presenterName.equals("CopyContract")){//06-10-2022
				Console.log("in copy contract");
				final Contract model=(Contract) model1;
				AppMemory.getAppMemory().currentScreen = Screen.CONTRACT;
				LoginPresenter.currentModule="Service";
				LoginPresenter.currentDocumentName="Contract";
				glassPanel.show();
				final ContractForm form = new ContractForm();
				if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
					form.setAction(false);
					form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
					form.processLevelBar.setVisibleFalse(false);
					docVerticalPanel.add(form.processLevelBar.content);
					form.processLevelBar.content.setWidth("80%");
				}
				docVerticalPanel.add(form.content);
				
				final Contract object=new Contract();
				object.setCount(0);
				object.setContractCount(0);
				object.setQuotationCount(0);
				object.setQuotationDate(null);
				object.setApprovalDate(null);
				object.setCreationDate(null);
				object.setValidUntill(null);
				object.setStatus(Quotation.CREATED);
				object.setId(null);
				
				//Header
				object.setCinfo(model.getCinfo());
				object.setBranch(model.getBranch());
				object.setContractDate(new Date());//29-09-2022
				if(model.getEmployee()!=null)
					object.setEmployee(model.getEmployee());
				if(model.getNumberRange()!=null)
					object.setNumberRange(model.getNumberRange());
				object.setApproverName(model.getApproverName());
				if(model.getFollowUpDate()!=null)
					object.setFollowUpDate(model.getFollowUpDate());
				object.setStationedTechnician(model.isStationedTechnician());
				object.setContractRate(model.isContractRate());
				object.setServiceWiseBilling(model.isServiceWiseBilling());
				object.setBranchWiseBilling(model.isBranchWiseBilling());
				object.setConsolidatePrice(model.isConsolidatePrice());
				
				//classification
				if(model.getCategory()!=null)
					object.setCategory(model.getCategory());
				if(model.getGroup()!=null)
					object.setGroup(model.getGroup());
				if(model.getType()!=null)
					object.setType(model.getType());
				if(model.getSegment()!=null)
					object.setSegment(model.getSegment());
				
				//reference
				object.setStartDate(model.getStartDate());
				object.setEndDate(model.getEndDate());
				
				//payment terms
				object.setPaymentTermsList(model.getPaymentTermsList());
				if(model.getPaymentMethod()!=null)
					object.setPaymentMethod(model.getPaymentMethod());
				if(model.getPaymentMode()!=null)
					object.setPaymentMode(model.getPaymentMode());
				object.setCreditPeriod(model.getCreditPeriod());
				
				//Product table
				if(model.getPremisesDesc()!=null)
					object.setPremisesDesc(model.getPremisesDesc());
				if(model.getTechnicianName()!=null)
					object.setTechnicianName(model.getTechnicianName());
				object.setServiceScheduleList(model.getServiceScheduleList());
				List<SalesLineItem> items=model.getItems(); //29-09-2022
				for(SalesLineItem item:items) {
					item.setStartDate(new Date());
					int duration=item.getDuration()-1;
					Date enddate=new Date();
					CalendarUtil.addDaysToDate(enddate, duration);
					item.setEndDate(enddate);
				}
				object.setItems(items);
				
				
				
				object.setDiscountAmt(model.getDiscountAmt());
				object.setTotalAmount(model.getTotalAmount());
				object.setNetpayable(model.getNetpayable());
				object.setFinalTotalAmt(model.getFinalTotalAmt());
				object.setGrandTotalAmount(model.getGrandTotalAmount());
				object.setGrandTotal(model.getGrandTotal());
				object.setProductTaxes(model.getProductTaxes());	
				object.setProductCharges(model.getProductCharges());
				
//				System.out.println("get id after : " +model.getCount()+ " ID "+model.getId() + " get value model  : "+ model.getCount() +" model Id ;;;; -- "+model.getId());
				AppMemory.getAppMemory().currentState=ScreeenState.NEW;
				Console.log("before new state");
				form.setToNewState();
				
							Timer timer = new Timer() {
								
								@Override
								public void run() {
									Console.log("in run");
									setSizeOfPanel(form);
									viewDocumentPanel.show();
									viewDocumentPanel.center();
									
									ContractPresenter contractPresenter=new ContractPresenter(form,new Contract());
									presenter = contractPresenter;
									form.updateView(object);	
									Console.log("after updateView");
									form.getTbQuotatinId().setValue("");
									form.getTbContractId().setValue("");
									form.getTbQuotatinId().setValue("");
									form.setPopUpAppMenubar(true);
									form.setHideNavigationButtons(hideNavigationButtons);
//									form.setToEditState();
									Console.log("after setToEditState");
									form.toggleAppHeaderBarMenu();									
									glassPanel.hide();
									
								}
							};
							timer.schedule(4000); 
									
		}
		//18-10-2022
		if(presenterName.equals("View Tax Invoice")){
			final Invoice model=(Invoice) model1;

			AppMemory.getAppMemory().currentScreen = Screen.INVOICEDETAILS;//14-09-2022
			LoginPresenter.currentModule="Accounts";
			LoginPresenter.currentDocumentName="Invoice";
			glassPanel.show();
			
			final InvoiceDetailsForm form = new InvoiceDetailsForm();						
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			
			
			
			final MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(model.getProformaCount());
			temp.add(filter);
			
			querry.setFilters(temp);
			querry.setQuerryObject(new Invoice());
//			form.showWaitSymbol();
			
//			if(!model.getAccountType().equalsIgnoreCase(AppConstants.BILLINGACCOUNTTYPEAP))
//				querry.setQuerryObject(new Invoice());
			
			
			genasync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					//form.hideWaitSymbol();
					glassPanel.hide();
				}

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {

					if(result.size()==0){
						form.showDialogMessage("No invoice document found. ");
						glassPanel.hide();
						return;
					}
					if(result.size()==1){				

						
						if(!model.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
						final Invoice billDocument=(Invoice) result.get(0);
						
						Timer timer=new Timer() {
							@Override
							public void run() {
								docVerticalPanel.add(form.content);
								setSizeOfPanel(form);
								viewDocumentPanel.show();
								viewDocumentPanel.center();
								
								InvoiceDetailsPresenter invoicePresenter=new InvoiceDetailsPresenter(form,new Invoice());
								presenter = invoicePresenter;
								form.updateView(billDocument);
								Console.log("In oice after updateview");
								presenter.setModel(billDocument);
								Console.log("In oice after setmodel");
								form.setPopUpAppMenubar(true);	
								form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022								
								form.setToViewState();	
								Console.log("In oice after view state");
								form.toggleAppHeaderBarMenu();
								Console.log("In oice after toggleAppHeaderBarMenu");
								glassPanel.hide();
								Console.log("after glassPanel.hide()1");
							}
						};
						timer.schedule(5000);
						//form.hideWaitSymbol();						
						glassPanel.hide();
						Console.log("after glassPanel.hide()");
						}
						}
						else{
						if(!model.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
							//InvoiceListPresenter.initalize(querry);
							Console.log("calling Invoice list");
							AppMemory.getAppMemory().currentScreen = Screen.INVOICELIST;
							LoginPresenter.currentModule="Accounts";
							LoginPresenter.currentDocumentName="Invoice List";
							glassPanel.show();
							InvoiceListTable table=new InvoiceListTable();
							final InvoiceListForm form=new InvoiceListForm(table);
							/**
							 * @author Anil
							 * @since 07-01-2021
							 * Showing process level menus
							 */
							if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
								form.setAction(false);
								form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
								form.processLevelBar.setVisibleFalse(false);
								docVerticalPanel.add(form.processLevelBar.content);
							}
							docVerticalPanel.add(form.content);
							Timer timer = new Timer() {
								@Override
								public void run() {
									setSizeOfPanel(form);
									viewDocumentPanel.show();
									viewDocumentPanel.center();

									Invoice serviceEntity = (Invoice) model;
//									form.updateView(serviceEntity);
								
									InvoiceListPresenter invoicePresenter=new InvoiceListPresenter(form, new Invoice(),querry);
									presenter = invoicePresenter;
									presenter.setModel(serviceEntity);
									
									form.setPopUpAppMenubar(true);
									form.setHideNavigationButtons(hideNavigationButtons);//20-09-2022

									form.setToViewState();
									form.toggleAppHeaderBarMenu();
									glassPanel.hide();
								}
							};
							timer.schedule(1000);
						}				
				}
				}
			});								
		}
		
		Console.log("End of Navigate screen method");
	 }
	 
	 private void reactOnCreateAMC(SalesOrder model){
		 	AppMemory.getAppMemory().currentScreen = Screen.CONTRACT;
			LoginPresenter.currentModule="Sales";
			LoginPresenter.currentDocumentName="Contract";
			glassPanel.show();
			final ContractForm form = new ContractForm();
			if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form.setAction(false);
				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form.processLevelBar.content);
				form.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form.content);


			System.out.println("result size == 0");
			final Contract contract = new Contract();
			
			PersonInfo pinfo = new PersonInfo();
			
			pinfo.setCount(model.getCustomerId());
			pinfo.setFullName(model.getCustomerFullName());
			pinfo.setCellNumber(model.getCustomerCellNumber());
			pinfo.setPocName(model.getCustomerFullName());
			
			contract.setCinfo(pinfo);
			
			contract.setRefNo(model.getCount()+"");
			System.out.println("Hi =="+model.getCount()+"");
			contract.setRefDate(model.getSalesOrderDate());
			contract.setBranch(model.getBranch());
			contract.setEmployee(model.getEmployee());
			
			form.showWaitSymbol();
			Timer timer = new Timer() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					ContractPresenter contractPresenter=new ContractPresenter(form,new Contract());
					presenter = contractPresenter;
						
					form.setToNewState();
					form.updateView(contract);
					presenter.setModel(contract);
					form.setPopUpAppMenubar(true);
					form.getTbReferenceNumber().setEnabled(false);
					form.getDbrefernceDate().setEnabled(false);
					form.hideWaitSymbol();
					form.toggleAppHeaderBarMenu();
					glassPanel.hide();
				}
			};
			timer.schedule(3000);					
		
	 }
	 private SalesQuotation getRevisedQuotationData(SalesQuotation model){
			SalesQuotation salesQuotation = new SalesQuotation();
			
			salesQuotation.setCategory(model.getCategory());
			salesQuotation.setCinfo(model.getCinfo());
			salesQuotation.setCreatedBy(model.getCreatedBy());
			salesQuotation.setTotalAmount(model.getTotalAmount());
			salesQuotation.setSdRemark(model.getSdRemark());
			salesQuotation.setCustomerCellNumber(model.getCustomerCellNumber());
			salesQuotation.setCustomerFullName(model.getCustomerFullName());
			salesQuotation.setCstpercent(model.getCstpercent());
			salesQuotation.setCstName(model.getCstName());
			salesQuotation.setEmployee(model.getEmployee());
			salesQuotation.setSdBankAccNo(model.getSdBankAccNo());
			salesQuotation.setCategoryKey(model.getCategoryKey());
			salesQuotation.setCformstatus(model.getCformstatus());
			salesQuotation.setCreationDate(model.getCreationDate());
			salesQuotation.setCreatedBy(model.getCreatedBy());
			salesQuotation.setCreationTime(model.getCreationTime());
			salesQuotation.setCreditPeriod(model.getCreditPeriod());
			salesQuotation.setDescription(model.getDescription());
			salesQuotation.setDeleted(model.isDeleted());
			salesQuotation.setDepositReceived(model.isDepositReceived());
			salesQuotation.setValidUntill(model.getValidUntill());
			salesQuotation.setSdRemark(model.getSdRemark());
			salesQuotation.setSdDocDate(model.getSdDocDate());
			salesQuotation.setSdBankAccNo(model.getSdBankAccNo());
			salesQuotation.setSdBankBranch(model.getSdBankBranch());
			salesQuotation.setSdBankName(model.getSdBankName());
			salesQuotation.setGroup(model.getGroup());
			salesQuotation.setGroupKey(model.getGroupKey());
			salesQuotation.setBranch(model.getBranch());
			salesQuotation.setPriority(model.getPriority());
			salesQuotation.setPaymentTermsList(model.getPaymentTermsList());
			salesQuotation.setProductCharges(model.getProductCharges());
			salesQuotation.setProductTaxes(model.getProductTaxes());
			salesQuotation.setPaymentTermsList(model.getPaymentTermsList());
			salesQuotation.setItems(model.getItems());
			salesQuotation.setSdDocStatus(model.getSdDocStatus());
			salesQuotation.setTotalAmount(model.getTotalAmount());
			salesQuotation.setNetpayable(model.getNetpayable());
			salesQuotation.setSdPayableAt(model.getSdPayableAt());
			salesQuotation.setSdPaymentMethod(model.getSdPaymentMethod());
			salesQuotation.setSdReturnDate(model.getSdReturnDate());
			salesQuotation.setRemark(model.getRemark());
			salesQuotation.setShippingAddress(model.getShippingAddress());
			salesQuotation.setDocument(model.getDocument());
			salesQuotation.setInclutaxtotalAmount(model.getInclutaxtotalAmount());
			salesQuotation.setApproverName(model.getApproverName());
			salesQuotation.setPaymentMethod(model.getPaymentMethod());
			salesQuotation.setType(model.getType());
			salesQuotation.setLeadCount(model.getLeadCount());
		
			return salesQuotation;
		}

	 private void switchToOrderScreen(final boolean statusValue,final SalesQuotation model, SalesOrderForm soform)
		{
		 try{

			 	
				//AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Sales/Sales Order",Screen.SALESORDER);
				final SalesOrderForm form=soform;
				final SalesOrder soent=new SalesOrder();
				form.showWaitSymbol();
				Timer t = new Timer() {
				      @Override
				      public void run() {
				    	  setSizeOfPanel(form);
				    	  viewDocumentPanel.show();
				    	  viewDocumentPanel.center();
							
				    	  SalesOrderPresenter soPresenter=new SalesOrderPresenter(form,new SalesOrder());
				    	  presenter = soPresenter;
				    	  
				    	  form.setToNewState();
				    	  Console.log("after setToNewState()");
				    	  form.hideWaitSymbol();
				    	  form.getTbContractId().setText("");
				    	  if(model.getLeadCount()==-1)
				    		  form.getTbLeadId().setText("");
				    	  form.getPersonInfoComposite().setEnabled(false);
				    	  form.getTbReferenceNumber().setText("");
				    	  form.getPersonInfoComposite().setValue(model.getCinfo());
				    	  form.getOlbbBranch().setValue(model.getBranch());
				    	  form.getOlbApproverName().setValue(model.getApproverName());
				    	  if(model.getLeadCount()!=-1){
				    		  form.getTbLeadId().setValue(model.getLeadCount()+"");
				    	  }
				    	  if(model.getContractCount()!=-1){
				    		  form.getTbContractId().setValue(model.getContractCount()+"");
				    	  }
				    	  
				    	  if(statusValue==true)
				    	  {
				    		  form.getOlbSalesOrderCategory().setValue(model.getCategory());
				    		  form.getOlbSalesOrderGroup().setValue(model.getGroup());
				    		  form.getOlbSalesOrderType().setValue(model.getType());
				    	  }
				    	  /**date 20-6-2020 by Amol added description two and vendor price raised by **/
				    	  form.getTaDescriptionTwo().setValue(model.getDescriptiontwo());
				    	  form.getDbVendorPrice().setValue(model.getVendorPrice());
				    	  
				    	  form.getSalesorderlineitemtable().setValue(model.getItems());
				    	  form.getTbQuotatinId().setValue(model.getCount()+"");
				    	  form.getOlbcPaymentMethods().setValue(model.getPaymentMethod());
				    	  form.getOlbeSalesPerson().setValue(model.getEmployee());
				    	  form.getTbQuotationStatus().setValue(SalesQuotation.CREATED);
				    	  form.getProdTaxTable().setValue(model.getProductTaxes());
				    	  form.getChargesTable().setValue(model.getProductCharges());
//				    	  form.getChargesTable().setEnable(false);
				    	  form.getPaymentTermsTable().setValue(model.getPaymentTermsList());
				    	  form.getDototalamt().setValue(model.getTotalAmount());
				    	  form.getDonetpayamt().setValue(model.getNetpayable());
				    	  form.getCbcheckaddress().setValue(model.getCheckAddress());
				    	  form.getIbCreditPeriod().setValue(model.getCreditPeriod());
			    		  form.getAcshippingcomposite().setValue(model.getShippingAddress());
			    		  form.getAcshippingcomposite().setEnable(false);
			    		  form.getDbdeliverydate().setValue(model.getDeliveryDate());
			    		  if(model.getCformstatus()!=null)
			  			  {
			  				for(int i=0;i<form.getCbcformlis().getItemCount();i++)
			  				{
			  					String data=form.getCbcformlis().getItemText(i);
			  					if(data.equals(model.getCformstatus()))
			  					{
			  						form.getCbcformlis().setSelectedIndex(i);
			  						form.getCbcformlis().setEnabled(true);
			  						break;
			  					}
			  				}
			  			  }
			    		  if(model.getCstName()!=null){
			    			  if(model.getCformstatus().equals(AppConstants.NO)){
			    				  form.getOlbcstpercent().setEnabled(false);
			    			  }
			    			  if(model.getCformstatus().equals(AppConstants.YES)){
			    				  form.getOlbcstpercent().setEnabled(true);
			    			  }
			    			  form.getOlbcstpercent().setValue(model.getCstName());
			    			 
			    		  }
			    		  
			    		  /** Date 23-03-2018 By vijay for other charges table mapping **/
			    		  form.getTblOtherCharges().setValue(model.getOtherCharges());
			    		  
			    		  double amtInclTax=model.getTotalAmount()+form.getProdTaxTable().calculateTotalTaxes();
			    		  NumberFormat nf=NumberFormat.getFormat("0.00");
			    		  amtInclTax=Double.parseDouble(nf.format(amtInclTax));
			    		  form.getDoamtincltax().setValue(amtInclTax);
			    		  soent.setStatus(SalesOrder.CREATED);
			    		  Console.log("after soent.setStatus(SalesOrder.CREATED)");
//			    		  form.setMenuAsPerStatus();
			    		  
			    		  /**
				    	   * Date 09-06-2018
				    	   * By Vijay for round off amt
				    	   */
				    	  if(model.getRoundOffAmount()!=0){
				    		  form.getTbroundoffAmt().setValue(model.getRoundOffAmount()+"");
				    	  }
				    	  /**
				    	   * ends here
				    	   */
				    	  
				    	  /**
				    	   * @author Anil
				    	   * @since 20-07-2020
				    	   */
				    	  
				    	  if(model.getVendorMargins()!=null&&model.getVendorMargins().size()!=0){
				    		  form.vendorMargins=model.getVendorMargins();
				    		  form.totalVendorMargin=model.getTotalVendorMargin();
				    		  for(VendorMargin obj:form.vendorMargins){
				    			  obj.setReadOnly(true);
				    		  }
				    	  }
				    	  if(model.getOtherChargesMargins()!=null&&model.getOtherChargesMargins().size()!=0){
				    		  form.otherChargesMargins=model.getOtherChargesMargins();
				    		  form.totalOcMargin=model.getTotalOtherChargesMargin();
				    		  for(OtherChargesMargin obj:form.otherChargesMargins){
				    			  obj.setReadOnly(true);
				    		  }
				    	  }
				    	  
				    	  for(SalesLineItem item :form.getSalesorderlineitemtable().getValue() ){
				    		  item.setReadOnly(true);
				    	  }
				    	  
				    		if(model.getCustBillingAddress()!=null && model.getCustBillingAddress().getAddrLine1()!=null 
					  				  && !model.getCustBillingAddress().getAddrLine1().equals("")) {
						  		form.customerBillingAddress = model.getCustBillingAddress();
					  		}
					  		if(model.getShippingAddress()!=null && model.getShippingAddress().getAddrLine1()!=null 
					  				  && !model.getShippingAddress().getAddrLine1().equals("")) {
						  		form.customerServiceAddress = model.getShippingAddress();
					  		}
					  		
					  		if(model.getCustomerBranch()!=null && !model.getCustomerBranch().equals("")){
					  			form.oblCustomerBranch.setValue(model.getCustomerBranch());
								form.loadCustomerBranch(model.getCustomerBranch(),model.getCinfo().getCount());

					  		}
					  		form.checkCustomerStatus(model.getCinfo().getCount());
					  		
			    		  /** Date 23-03-2018 By vijay above line commented becs no need of that getting null pointer exception below line added **/
			    		  form.setAppHeaderBarAsPerStatus();
			    		  
//						 form.updateView(soent);
						 presenter.setModel(soent);
						 Console.log("presenter.setModel(soent)");
						 form.setPopUpAppMenubar(true);
						 form.toggleAppHeaderBarMenu();
						 glassPanel.hide();
						 Console.log("glassPanel.hide() called");
				      }
				    };
				    t.schedule(5000);	

		 }
		 catch(Exception e){
			 e.printStackTrace();
			 System.out.println(e.getMessage());
			 Console.log(e.getMessage());
		 }
		}
	 
	 private boolean checkBillSubType(String billType){
			
			switch(billType){
				case AppConstants.NATIONALHOLIDAY:
				case AppConstants.OVERTIME:
				case AppConstants.STAFFINGANDRENTAL:
				case AppConstants.OTHERSERVICES:
				case AppConstants.CONSUMABLES:
				case AppConstants.FIXEDCONSUMABLES :
				case AppConstants.EQUIPMENTRENTAL:
				case AppConstants.STAFFING:
				case AppConstants.CONSOLIDATEBILL:	
				case AppConstants.ARREARSBILL:	
				case AppConstants.PUBLICHOLIDAY:	
					return true;
			}
			return false;		
			}		

		
	 private void reactOnContractRenewal(Contract model1) {
		 	final Contract model=(Contract) model1;
		 	
		 	AppMemory.getAppMemory().currentScreen = Screen.CONTRACT;
			LoginPresenter.currentModule="Service";
			LoginPresenter.currentDocumentName="Contract";
			glassPanel.show();
			final ContractForm form1 = new ContractForm();
			if (form1.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
				form1.setAction(false);
				form1.processLevelBar = new ProcessLevelBar(5, form1.getProcesslevelBarNames(),form1.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
				form1.processLevelBar.setVisibleFalse(false);
				docVerticalPanel.add(form1.processLevelBar.content);
				form1.processLevelBar.content.setWidth("80%");
			}
			docVerticalPanel.add(form1.content);
			
			/**
			 * @author Anil , Date : 17-04-2020
			 * in case of large service contract we are storing scheduling list on different entity
			 * in that case we will get zero scheduling list size and system will not allow to renew the contract
			 */
			 List<SalesLineItem>itemList=null;
			 //error- form is referring to contractform
			if(model.getServiceScheduleList().size()==0){
				Console.log("4b- Old Contract 1- "+form1.getServiceScheduleTable().getValue().size());
				model.setServiceScheduleList(form1.getServiceScheduleTable().getValue());
				Console.log("4b- Old Contract 2- "+model.getServiceScheduleList().size());
			} 
			
			Console.log("1-Inside Contract Renewal");
			//AppMemory.getAppMemory().setCurrentScreenandChangeProcessName("Service/Contract", Screen.CONTRACT);
			
			//doubt- Is it correct to use here?
			//final ContractForm form1 = ContractPresenter.initalize();
			
			/**
			 * Date : 12-05-2017 By ANIl
			 */
			form1.checkCustomerBranch(model.getCinfo().getCount());
			/**
			 * End
			 */
			
			/**
			 * Date : 19-07-2017 By ANIl
			 */
			form1.checkCustomerStatus(model.getCinfo().getCount(),true,true);
			/**
			 * End
			 */
			
			form1.showWaitSymbol();
			Timer t = new Timer() {
				@Override
				public void run() {
					setSizeOfPanel(form1);
			    	viewDocumentPanel.show();
			    	viewDocumentPanel.center();
						
			    	ContractPresenter cPresenter=new ContractPresenter(form1,new Contract());
			    	presenter = cPresenter;
			    	  
					form1.setToNewState();
					form1.hideWaitSymbol();
					form1.getTbContractId().setText("");
					form1.getOlbContractCategory().setValue(model.getCategory());
					/**
			    	   * nidhi
			    	   * 16-01-2018
			    	   * unchecked all check box by default 
			    	   */
			    	  form1.getCbRenewFlag().setValue(false);
			    	  form1.getCbServiceWiseBilling().setValue(false);
			    	  form1.getChkbillingatBranch().setValue(false);
			    	  form1.getChkservicewithBilling().setValue(false);
			    	  form1.getCbStartOfPeriod().setValue(false);
			    	  form1.getCbConsolidatePrice().setValue(false);
			    	  /***
			    	   *  end
			    	   */
					/**
					 * nidhi
					 * 16-01-2018
					 * for map segment ref number and contract type 
					 */
					form1.getOlbContractCategory().setValue(model.getCategory());
					if (model.getRefNo() != null) {
						form1.getTbReferenceNumber().setValue(model.getRefNo());
					} else {
						form1.getTbReferenceNumber().setText("");
					}
					form1.getTbCustSegment().setValue(model.getSegment());
					if((form1.getTbReferenceNumber().getValue()== null || form1.getTbReferenceNumber().getValue().trim()=="") ||
			    			  (form1.getTbCustSegment().getValue()== null || form1.getTbCustSegment().getValue().trim()=="") ||
			    			  (form1.getOlbContractType().getValue()== null || form1.getOlbContractType().getValue().trim()=="")){
			    		  /**
				    		  * Date:10-07-2017 BY NIDHI
				    		  */
				    		 if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC")){
				    			 form1.checkCustomerStatus(model.getCustomerId(), true, false);
				    		 }
				    		 
				    		
				    		 /**
				    		  * End
				    		  */
			    	  }
					 if(form1.getOlbContractCategory().getSelectedIndex()!=0){
			 				ConfigCategory cat=form1.getOlbContractCategory().getSelectedItem();
							if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableRenewalContractTypeDefault")){
//								form.showDialogMessage("category -- " +cat.getCategoryName() );
				 				if(cat!=null){
				 					AppUtility.makeLiveTypeDropDown(form1.getOlbContractType(), cat.getCategoryName(), cat.getCategoryCode(), cat.getInternalType());
				 					 form1.getOlbContractType().setValue(model.getType());
				 				}
							}

			 			}
					/**
					 * 
					 *  end
					 */
					 form1.getCustomerContactList(model.getCinfo().getCount(),model.getPocName());
					 form1.getObjContactList().setValue(model.getPocName());
					 
					if (model.getLeadCount() == -1) {
						form1.getTbLeadId().setText("");
					}
					form1.getPersonInfoComposite().setEnabled(false);
					if (model.getRefNo() != null) {
						form1.getTbReferenceNumber().setValue(model.getRefNo());
					} else {
						form1.getTbReferenceNumber().setText("");
					}

					if (model.getReferedBy() != "") {
						form1.getTbRefeRRedBy().setValue(model.getReferedBy());
					} else {
						form1.getTbRefeRRedBy().setText("");
					}

					if (model.getRefDate() != null) {
						form1.getDbrefernceDate().setValue(model.getRefDate());
					} else {
						form1.getDbrefernceDate().setValue(null);
					}

					form1.getPersonInfoComposite().setValue(model.getCinfo());

					// rohan added this code for setting customer branches in
					// schedule popup
					ContractPresenter.custcount = model.getCinfo().getCount();
					// ends here

					form1.getOlbbBranch().setValue(model.getBranch());
					form1.getOlbApproverName().setValue(model.getApproverName());
				  /**
		    	   * @author Vijay Date :- 27-10-2020
		    	   * Des  Bug :- when contract renew it should not map lead id in new renewed contract
		    	   */
//					if (model.getLeadCount() != -1) {
//						form1.getTbLeadId().setValue(model.getLeadCount() + "");
//					}
					
					/**
					 * Commented old code
					 * By Anil Date : 13-05-2017
					 */
//					if (model.getContractCount() != -1) {
//						form.getTbContractId().setValue(model.getContractCount() + "");
//					}
					form1.getIbOldContract().setValue(model.getCount() + "");
					if (model.getQuotationCount() != -1) {
						form1.getTbQuotatinId().setValue(model.getQuotationCount() + "");
					}
					form1.getOlbcPaymentMethods().setValue(model.getPaymentMethod());
					form1.getOlbeSalesPerson().setValue(model.getEmployee());
					form1.getOlbContractGroup().setValue(model.getGroup());
					form1.getOlbContractCategory().setValue(model.getCategory());
					form1.getOlbContractType().setValue(model.getType());

					form1.getProdTaxTable().setValue(model.getProductTaxes());
					form1.getChargesTable().setValue(model.getProductCharges());
					/**
					 * Updated By: Viraj
					 * Date: 18-01-2019
					 * Description: to not map payment terms
					 */
//					form.getPaymentTermsTable().setValue(model.getPaymentTermsList());

					// rohan added this for setting flag value
					//commenting below lines as giving error
					form1.chkbillingatBranch.setValue(false);
					form1.chkservicewithBilling.setValue(false);

					ArrayList<SalesLineItem> arraylis = new ArrayList<SalesLineItem>();
					List<SalesLineItem> listProduct = new ArrayList<SalesLineItem>();
					listProduct.addAll(model.getItems());
					System.out.println("listproduct size===" + listProduct.size());

					for (int i = 0; i < listProduct.size(); i++) {
						
						SalesLineItem sales = new SalesLineItem();
						sales.setPrduct(listProduct.get(i).getPrduct());
						CalendarUtil.addDaysToDate(listProduct.get(i).getStartDate(),listProduct.get(i).getDuration() + 1);
						
						/**
						 * @author Anil
						 * @since 10-06-2020
						 * contract data get corrupt at the time of renwal becuase of product master document upload
						 */
						sales.setTermsAndConditions(new DocumentUpload());
						sales.setProductImage(new DocumentUpload());
						sales.getPrduct().setProductImage1(new DocumentUpload());
						sales.getPrduct().setProductImage2(new DocumentUpload());
						sales.getPrduct().setProductImage3(new DocumentUpload());
						sales.getPrduct().setComment("");
						sales.getPrduct().setCommentdesc("");
						sales.getPrduct().setCommentdesc1("");
						sales.getPrduct().setCommentdesc2("");
						
						
						/**
						 * Date : 09-06-2017 By ANIL
						 * PCAMB RENEWAL ERROR FOR NO. OF BRANCH ERROR
						 */
						//  rohan added this code for sessting service sr no while renew contract
						if(listProduct.get(i).getProductSrNo()==0){
							sales.setProductSrNo(i+1);
						}else{
							sales.setProductSrNo(listProduct.get(i).getProductSrNo());
						}
						
						/**
						 * End
						 */
						sales.setStartDate(listProduct.get(i).getStartDate());
						sales.setQuantity(listProduct.get(i).getQty());
						sales.setDuration(listProduct.get(i).getDuration());
						sales.setNumberOfService(listProduct.get(i).getNumberOfServices());
						sales.setPrice(listProduct.get(i).getPrice());
						sales.setVatTax(listProduct.get(i).getVatTax());
						sales.setServiceTax(listProduct.get(i).getServiceTax());
						sales.setPercentageDiscount(listProduct.get(i).getPercentageDiscount());
						sales.setServicingTime(listProduct.get(i).getServicingTIme());
						
						/**
						 * Date:16-06-2017 BY ANIL
						 */
						sales.setRemark(listProduct.get(i).getRemark());
						/**
						 * End
						 */
						
						/**
						 * Date:23/06/2018 BY Vijay
						 */
						if(listProduct.get(i).getArea()!=null && !listProduct.get(i).getArea().equals(""))
							sales.setArea(listProduct.get(i).getArea());
						/**
						 * ends here
						 */
						
						/**
						 * Date : 13-05-2017 By ANIL
						 */
						sales.setBranchSchedulingInfo(listProduct.get(i).getBranchSchedulingInfo());
						sales.setBranchSchedulingInfo1(listProduct.get(i).getBranchSchedulingInfo1());
						sales.setBranchSchedulingInfo2(listProduct.get(i).getBranchSchedulingInfo2());
						sales.setBranchSchedulingInfo3(listProduct.get(i).getBranchSchedulingInfo3());
						sales.setBranchSchedulingInfo4(listProduct.get(i).getBranchSchedulingInfo4());
						/**
						 * End
						 */

						
						/**
						 * Date : 12-06-2017 By ANIL
						 */
						sales.setBranchSchedulingInfo5(listProduct.get(i).getBranchSchedulingInfo5());
						sales.setBranchSchedulingInfo6(listProduct.get(i).getBranchSchedulingInfo6());
						sales.setBranchSchedulingInfo7(listProduct.get(i).getBranchSchedulingInfo7());
						sales.setBranchSchedulingInfo8(listProduct.get(i).getBranchSchedulingInfo8());
						sales.setBranchSchedulingInfo9(listProduct.get(i).getBranchSchedulingInfo9());
						/**
						 * End
						 */
						
						/**
						 * Date 02-05-2018 
						 * Developer : Vijay
						 * Des :- Below code is customer branches stored in string so it can not load more than 120 branches
						 * so i have updated below code using hashmap. So old contract i will get the branches from string and new contract from hashmpa if branches exist 
						 */
						if(listProduct.get(i).getCustomerBranchSchedulingInfo()!=null){
							sales.setCustomerBranchSchedulingInfo(listProduct.get(i).getCustomerBranchSchedulingInfo());
						}else{
							
							/**
							 * Date 21-07-2018 By Vijay
							 * Des :- Old contract updated through customerBranch hashmap new
							 */
							
							String[] branchArray=new String[10];
							if(listProduct.get(i).getBranchSchedulingInfo()!=null
									&&!listProduct.get(i).getBranchSchedulingInfo().equals("")){
								branchArray[0]=listProduct.get(i).getBranchSchedulingInfo();
							}
							if(listProduct.get(i).getBranchSchedulingInfo1()!=null
									&&!listProduct.get(i).getBranchSchedulingInfo1().equals("")){
								branchArray[1]=listProduct.get(i).getBranchSchedulingInfo1();
							}
							if(listProduct.get(i).getBranchSchedulingInfo2()!=null
									&&!listProduct.get(i).getBranchSchedulingInfo2().equals("")){
								branchArray[2]=listProduct.get(i).getBranchSchedulingInfo2();
							}
							if(listProduct.get(i).getBranchSchedulingInfo3()!=null
									&&!listProduct.get(i).getBranchSchedulingInfo3().equals("")){
								branchArray[3]=listProduct.get(i).getBranchSchedulingInfo3();
							}
							if(listProduct.get(i).getBranchSchedulingInfo4()!=null
									&&!listProduct.get(i).getBranchSchedulingInfo4().equals("")){
								branchArray[4]=listProduct.get(i).getBranchSchedulingInfo4();
							}
							
							if(listProduct.get(i).getBranchSchedulingInfo5()!=null
									&&!listProduct.get(i).getBranchSchedulingInfo5().equals("")){
								branchArray[5]=listProduct.get(i).getBranchSchedulingInfo5();
							}
							if(listProduct.get(i).getBranchSchedulingInfo6()!=null
									&&!listProduct.get(i).getBranchSchedulingInfo6().equals("")){
								branchArray[6]=listProduct.get(i).getBranchSchedulingInfo6();
							}
							if(listProduct.get(i).getBranchSchedulingInfo7()!=null
									&&!listProduct.get(i).getBranchSchedulingInfo7().equals("")){
								branchArray[7]=listProduct.get(i).getBranchSchedulingInfo7();
							}
							if(listProduct.get(i).getBranchSchedulingInfo8()!=null
									&&!listProduct.get(i).getBranchSchedulingInfo8().equals("")){
								branchArray[8]=listProduct.get(i).getBranchSchedulingInfo8();
							}
							if(listProduct.get(i).getBranchSchedulingInfo9()!=null
									&&!listProduct.get(i).getBranchSchedulingInfo9().equals("")){
								branchArray[9]=listProduct.get(i).getBranchSchedulingInfo9();
							}
							
							ArrayList<BranchWiseScheduling> branchSchedulingList=null;
							if(listProduct.get(i).getBranchSchedulingInfo()!=null
									&&!listProduct.get(i).getBranchSchedulingInfo().equals("")){
								branchSchedulingList = AppUtility.getBranchSchedulingList(branchArray);
							}else{
								if(listProduct.get(i).getCustomerBranchSchedulingInfo()!=null){
								branchSchedulingList = listProduct.get(i).getCustomerBranchSchedulingInfo().get(listProduct.get(i).getProductSrNo());
								}

							}
							if(branchSchedulingList!=null&&branchSchedulingList.size()!=0){
								String branchAray[]=AppUtility.getCustomersUpdatedBranchSchedulingList(branchSchedulingList);
								if(branchAray.length>0){
									ArrayList<BranchWiseScheduling> branchlist = AppUtility.getBranchSchedulingList(branchAray);
									Integer prodSrNo = listProduct.get(i).getProductSrNo();
									HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
									customerBranchlist.put(prodSrNo, branchlist);
									sales.setCustomerBranchSchedulingInfo(customerBranchlist);
							
								}
							}
						
						}
						/**
						 * ends here
						 */
						/** date 11.12.2018 added by komal for nbhc service branches button and popup **/
						if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableServiceBranch")){
							if(listProduct.get(i).getServiceBranchesInfo() != null && listProduct.get(i).getServiceBranchesInfo().size() > 0){
								sales.setServiceBranchesInfo(listProduct.get(i).getServiceBranchesInfo());
							}else{
								ArrayList<BranchWiseScheduling> branchArrayList = new ArrayList<BranchWiseScheduling>();
								for(Branch branch : LoginPresenter.customerScreenBranchList){
									BranchWiseScheduling branchWiseScheduling = new BranchWiseScheduling();
									branchWiseScheduling.setBranchName(branch.getBusinessUnitName());
									branchWiseScheduling.setArea(0);
									branchWiseScheduling.setCheck(false);
									branchArrayList.add(branchWiseScheduling);
								}
								HashMap<Integer, ArrayList<BranchWiseScheduling>> serviceBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
								serviceBranchlist.put(sales.getProductSrNo(), branchArrayList);
								sales.setServiceBranchesInfo(serviceBranchlist);
							}
						}
						
						/**
						 * end komal
						 */
						arraylis.add(sales);
					}
					
					Console.log("2-Line item size:  "+arraylis.size());
					 /**
			    	   * Date : 12-05-2017 By Anil
			    	   * This method update itemlist by adding branch info at product level.
			    	   */
					if(model.getItems().get(0).getBranchSchedulingInfo()!=null&&!model.getItems().get(0).getBranchSchedulingInfo().equals("")){
						Console.log("3- New and Updated Contract ");
						form1.getSaleslineitemtable().setValue(arraylis);
						List<ServiceSchedule> popuplist = form1.reactfordefaultTableOnRenewal(arraylis);
						form1.getServiceScheduleTable().getDataprovider().setList(popuplist);
					}else{
						Console.log("4- Old Contract ");
			    	  List<SalesLineItem>itemList=AppUtility.getItemList(arraylis, model.getServiceScheduleList(),form1.customerbranchlist,model.getBranch());
			    	  if(itemList!=null){
			    		  form1.getSaleslineitemtable().setValue(itemList);
			    		  ArrayList<SalesLineItem> items = new ArrayList<SalesLineItem>(itemList);
			    		  List<ServiceSchedule> popuplist = form1.reactfordefaultTableOnRenewal(items);
						  form1.getServiceScheduleTable().getDataprovider().setList(popuplist);
			    	  }else{
			    		  form1.showDialogMessage("No. of branches at product level and no. of selected branches at service scheduling popup are different.");
			    	  }
					}
					/**
		    	    * End
		    	    */
					form1.getDototalamt().setValue(model.getTotalAmount());
					form1.getDonetpayamt().setValue(model.getNetpayable());
					
					/**
					 * Date : 14-12-2017 BY ANIL
					 */
					form1.getCbServiceWiseBilling().setValue(false);
					form1.cbStartOfPeriod.setValue(false);
					/**
					 * nidhi
					 * 27-04-2018
					 * for renewal contract from old contract
					 */
					form1.contractRenewFlag =true;
					/**
					 * End
					 */
					form1.findMaxServiceSrNumber();
					/**
					 * Date 16-06-2018 by vijay 
					 * Des :- for contract renewal schedule services branch name. for old contract hashmap not exist so
					 */
					AppUtility.branchName=model.getBranch();
					
					/**
			  		 * Date 28-09-2018 By Vijay 
			  		 * Des:- NBHC CCPM for every contract this must be true by default it does not allow to change for user
			  		 */
			  		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC")){
			  			form1.cbStartOfPeriod.setValue(true);
			  			form1.cbStartOfPeriod.setEnabled(false);
			  		}
			  		/**
			  		 * ends here
			  		 */
			  		/**
			  		 * Date 20-03-2019 by vijay for NBHC CCPM for renewal contracts must have the quotation id
			  		 */
			  		//commenting below lines as giving error
			  		SingleDropDownPopup singledropdownpopup = new SingleDropDownPopup();

					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "EnableQuotationMandatoryForContractRenwal")){
				  		if(singledropdownpopup!=null && singledropdownpopup.getOlbQuotationId().getSelectedIndex()!=0){
					  		String quotationId[] = singledropdownpopup.getOlbQuotationId().getValue(singledropdownpopup.getOlbQuotationId().getSelectedIndex()).split("/");
					  		System.out.println("quotationId[] =="+quotationId[0]);
					  		form1.getTbQuotatinId().setValue(quotationId[0]);
				  		}
					}
					
					/**
					 * Date 26-07-2019 by Vijay 
					 * Des :- NBHC CCPM for renewal contract set default to AMC and Rate.
					 */
					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableRenewalContractTypeDefault")){
						if(model.isContractRate()){
							form1.getOlbContractType().setValue("Rate Contract(Renewal)");
							form1.getOlbContractType().setEnabled(false);
						}
						else{
							System.out.println("Contract Type =="+form1.getOlbContractType().getValue());
							form1.getOlbContractType().setValue("AMC(Renewal)");
							form1.getOlbContractType().setEnabled(false);
							System.out.println("Contract Type "+form1.getOlbContractType().getValue());

						}
					}
					/**
					 * ends here
					 */
					
					/**
					 * @author Vijay Date :- 18-11-2021
					 * Des :- As per Nitin sir payment terms should be copied to contract renewal time
					 */
					//commenting below lines as giving error
					if(model.getPayTerms()!=null && !model.getPayTerms().equals(""))
						form1.olbPaymentTerms.setValue(model.getPayTerms());
					
					if(model.getPaymentTermsList()!=null)
						form1.paymentTermsTable.getDataprovider().setList(model.getPaymentTermsList());
					if(model.getNumberRange()!=null && !model.getNumberRange().equals(""))
						form1.olbcNumberRange.setValue(model.getNumberRange());
					
			  		/**
			  		 * ends here
			  		 */
				}
			};
			/**
			 * @author Anil
			 * @since 18-06-2020
			 * Hodling screen for 10 sec for loading configuration details
			 * NBHC faces issues while renewing large contract of multiple location
			 * location details are not getting mapped on line item
			 */

			t.schedule(10000);
			form1.toggleAppHeaderBarMenu();
			form1.setPopUpAppMenubar(true);
			glassPanel.hide();
		
	 }

	 private void createProjectCorrespondingToService(final String typeOfTransaction,Service model1){ //,DeviceForm form1,DevicePresenter presenter1){
//		 	final DeviceForm form=form1;
//		 	final DevicePresenter presenter=presenter1;
		 	final Service model=model1;
		 	
			Console.log("Inside createProjectCorrespondingToService");
			CustomerProjectServiceAsync projectAsync=GWT.create(CustomerProjectService.class);
			
			
	      //      form.showWaitSymbol();
	            
	    		Timer timer=new Timer() 
	        	 {
	    				@Override
	    				public void run() {
	    					CustomerProjectServiceAsync projectAsync=GWT.create(CustomerProjectService.class);
							projectAsync.createCustomerProject(model.getCompanyId(), model.getContractCount(), model.getCount(),model.getStatus(), new AsyncCallback<Integer>(){
					
								@Override
								public void onFailure(Throwable caught) {
//									form.hideWaitSymbol();
//									form.showDialogMessage("An Unexpected Error occured !");
									Console.log("An Unexpected Error occured !");
								}
					
								@Override
								public void onSuccess(Integer result) {
									if(result==1){
										if(typeOfTransaction.trim().equals("Button"))
										{ 
											reactOnAddProject(model);
										}
										
									}
								//  vijay addded this code
									if(result==-1){
										if(typeOfTransaction.trim().equals("Button"))
										{
											//form.showDialogMessage("Project deos not exist for this Service Service already Completed!");
											Console.log("Project deos not exist for this Service Service already Completed!");

										}else{
											//form.showDialogMessage("Project deos not exist for this Service Can Not print Job Card!");
											Console.log("Project deos not exist for this Service Can Not print Job Card!");
										}
									}
									
									if(result==0){
										//form.showDialogMessage("An unexpected error occurred. Please try again!");
										Console.log("An unexpected error occurred. Please try again!");
									}
								}
							});
//							form.hideWaitSymbol();
	    				}
	  			};
	             timer.schedule(3000);
	 		
		
	 }
	 private void reactOnAddProject(Service model){
//		 Console.log("in reactOnAddProject");
//		 ProjectPresenterTable gentableScreen = new ProjectPresenterTable();
//		 ProjectForm form = new ProjectForm(gentableScreen,FormTableScreen.UPPER_MODE,true,true);
//		 Console.log("after form creation");
//		 if (form.getProcesslevelBarNames() != null&&AppMemory.getAppMemory().enableMenuBar) {
//				form.setAction(false);
//				form.processLevelBar = new ProcessLevelBar(5, form.getProcesslevelBarNames(),form.getNavigationProcessMenuBar(),FormStyle.DEFAULT);
//				form.processLevelBar.setVisibleFalse(false);
//				docVerticalPanel.add(form.processLevelBar.content);
//				form.processLevelBar.content.setWidth("80%");
//		 }
//		 docVerticalPanel.add(form.content);
//		 Console.log("after docVerticalPanel.add(form.content)");
//		 
//		  MyQuerry querry=new MyQuerry();
//		  Vector<Filter> filtervec=new Vector<Filter>();
//	      Filter filter=null;
//	      filter=new Filter();
//	      filter.setQuerryString("serviceId");
//	      filter.setIntValue(model.getCount());
//	      filtervec.add(filter);
//	      
//	      filter=new Filter();
//	      filter.setQuerryString("companyId");
//	      filter.setLongValue(model.getCompanyId());
//	      filtervec.add(filter);
//	      
//	      querry.setFilters(filtervec);
//	      querry.setQuerryObject(new ServiceProject());	      
//
//	      Console.log("after setting querry");
//	      
//	      setSizeOfPanel(form);
//	      viewDocumentPanel.show();
//	      viewDocumentPanel.center();
//			
//	      Console.log("after viewDocumentPanel.center()");
//	      ProjectPresenter projectpresenter=new ProjectPresenter(form, new ServiceProject(),querry);
//	      presenter = projectpresenter;
//	      //presenter.setModel(model);
//	      Console.log("after setting presenter");	      
//	      
//	      form.setToNewState();
//	        
//	      filter=new Filter();
//	      filter.setQuerryString("personInfo.count");
//	      filter.setIntValue(model.getPersonInfo().getCount());
//	      querry=new MyQuerry();
//	      querry.setQuerryObject(new ClientSideAsset());
//	      querry.getFilters().add(filter);
//	      form.olbClientSideAsset.MakeLive(querry);
//	      
//	      Console.log("after form.olbClientSideAsset.MakeLive(querry)");
//           
//           if(model.getStatus().equals(Service.SERVICESTATUSCOMPLETED)
//           		||model.getStatus().equals(Service.SERVICESTATUSCLOSED)
//           		||model.getStatus().equals(Service.SERVICESTATUSCANCELLED))
//           {
//           	form.setToNewState();
//           }
//          Console.log("after last if");
//          form.setPopUpAppMenubar(true);
//		  form.toggleAppHeaderBarMenu();
//		  glassPanel.hide();		
//		  Console.log("end of method");
	 }
	 
	  public void setServiceLocation(String presenterName, final SuperModel model) {
		removePopObjects();
		docVerticalPanel.add(btnClose);
		docVerticalPanel.add(appLevelMenuPanel);
		
		Console.log("presenterName "+presenterName);
		
		if (presenterName.equals("Service Location")) {
			glassPanel.show();
			ServiceLocationTable gentableScreen=new ServiceLocationTable();
			final ServiceLocationForm  form=new  ServiceLocationForm(gentableScreen,FormTableScreen.UPPER_MODE,false,true);
			if (form.getProcesslevelBarNames() != null){
				form.processLevelBar.setVisibleFalse(false);
			}
			Console.log("4--- "+presenterName);
			docVerticalPanel.add(form.content);
			Timer timer = new Timer() {
				@Override
				public void run() {
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					CustomerBranchDetails custBranch=(CustomerBranchDetails) model;
					
					MyQuerry query=new MyQuerry();
					Vector<Filter> vecFil=new Vector<Filter>();
					Filter temp=new Filter();
					temp.setQuerryString("companyId");
					temp.setLongValue(UserConfiguration.getCompanyId());
					vecFil.add(temp);
					
					temp=new Filter();
					temp.setQuerryString("cinfo.count");
					temp.setIntValue(custBranch.getCinfo().getCount());
					vecFil.add(temp);
					
					temp=new Filter();
					temp.setQuerryString("custBranchId");
					temp.setIntValue(custBranch.getCount());
					vecFil.add(temp);
					
					query.setFilters(vecFil);
					query.setQuerryObject(new CustomerBranchServiceLocation());
					
					ServiceLocationPresenter leadPresenter=new ServiceLocationPresenter(form,new CustomerBranchServiceLocation(),query);
					presenter = leadPresenter;
					
					form.setPopUpAppMenubar(true);
					form.setToNewState();
					
					form.getPersonInfoComposite().setValue(custBranch.getCinfo());
					form.getTbcustomerBranch().setValue(custBranch.getBusinessUnitName());
					form.setCustBranchId(custBranch.getCount());
					glassPanel.hide();
				}
			};
			timer.schedule(1000);
		}
	 }
	 
	 /**
	  * @author Anil
	  * @since 08-02-2022
	  * @param presenterName
	  * @param model
	  * @param productInfo
	  * @param isCompany
	  */
	 public void setProductCompositeScreen(String presenterName, final SuperModel model,final ProductInfo productInfo) {
	 	removePopObjects();
		docVerticalPanel.add(btnClose);
		docVerticalPanel.add(appLevelMenuPanel);
		
		Console.log(" "+presenterName);
		
		screenState= AppMemory.getAppMemory().currentState;
		Console.log("screenState "+screenState);
		
		if(presenterName.equalsIgnoreCase(AppConstants.PRODUCTCOMPOSITEITEM)){
			glassPanel.show();
			final ItemproductForm form = new ItemproductForm(true);
			if (form.getProcesslevelBarNames() != null){
				form.processLevelBar.setVisibleFalse(false);
			}
			Console.log("1 - "+presenterName);
			docVerticalPanel.add(form.content);
			Timer timer = new Timer() {
				@Override
				public void run() {
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					ItemproductPresenter leadPresenter=new ItemproductPresenter(form,new ItemProduct());
					presenter = leadPresenter;
					
					if(model!=null){
						ItemProduct entity = (ItemProduct) model;
						form.updateView(entity);
						presenter.setModel(entity);
						form.setPopUpAppMenubar(true);
						form.setToViewState();
					}else{
						form.setPopUpAppMenubar(true);
						form.setToNewState();
					}
					
					glassPanel.hide();
				}
			};
			timer.schedule(1000);
		}else if(presenterName.equalsIgnoreCase(AppConstants.PRODUCTCOMPOSITESERVICE)){
			glassPanel.show();
			final ServiceproductForm form = new ServiceproductForm(true);
			if (form.getProcesslevelBarNames() != null){
				form.processLevelBar.setVisibleFalse(false);
			}
			Console.log("1 - "+presenterName);
			docVerticalPanel.add(form.content);
			Timer timer = new Timer() {
				@Override
				public void run() {
					setSizeOfPanel(form);
					viewDocumentPanel.show();
					viewDocumentPanel.center();
					
					ServiceproductPresenter leadPresenter=new ServiceproductPresenter(form,new ServiceProduct());
					presenter = leadPresenter;
					
					if(model!=null){
						ServiceProduct entity = (ServiceProduct) model;
						form.updateView(entity);
						presenter.setModel(entity);
						form.setPopUpAppMenubar(true);
						form.setToViewState();
					}else{
						form.setPopUpAppMenubar(true);
						form.setToNewState();
					}
					
					glassPanel.hide();
				}
			};
			timer.schedule(1000);
		}
	 }
	 
	 
		
}

		

			



