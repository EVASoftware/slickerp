package com.slicktechnologies.client.views.generalpopup;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.EntityPresenter;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
/**
 * Here i am created popup menu bar
 * and calling menus respective methods
 * created by vijay chougule
 * @author Vijay  Chougule
 *
 */
public class PopupEntityPresenter extends AbsolutePanel implements ClickHandler{

	
	VerticalPanel docVerticalPanel = null;
	FlowPanel appLevelMenuPanel;
	FlowPanel btnClosePanel;
	
	public  InlineLabel[]menuLabels;
	public static InlineLabel[] applevelMenu=null;
	public Image btnClose;

	protected EntityPresenter presenter=null;
	public PopupEntityPresenter(){
	
		
		btnClose = new Image("/images/closebtnred.png");
		btnClose.getElement().setClassName("topRight");
		btnClose.addClickHandler(this);
		
		btnClosePanel = new FlowPanel();
		btnClosePanel.add(btnClose);
		
		appLevelMenuPanel = new FlowPanel();
		
		
		String[] appmenus= getApplevelMenus();
		createAppLevelMenuPanel(appmenus);
		
		setEventHandeling();
	
		docVerticalPanel = new VerticalPanel();
		docVerticalPanel.add(btnClosePanel);
		docVerticalPanel.add(appLevelMenuPanel);
	}
	
	/**
	 * The method sets the click handler.On the app level menus.
	 */
	private void setEventHandeling() {
		// TODO Auto-generated method stub
		
		for(int i=0;i<menuLabels.length;i++){
			
			menuLabels[i].addClickHandler(this);
		}
	}
	
	
	public String[] getApplevelMenus()
	{
		/**
		 * Updated By: Viraj
		 * Date: 06-06-2019
		 * Description: To get menu level bar with save and edit in EmployeeAdditionalDetails
		 * @author Anil @since 19-01-2021
		 * This was not required or wrongly done
		 */
//		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("EmployeeAdditionalDetails","menuNames")){
//			String[] menuNames ={"Save","Edit"};
//			return menuNames;
//		} else {
			String[] menuNames = {"Save","Print","Edit","Email","Message"};//Ashwini Patil Date:1-09-2022 Added "Message"
			return menuNames;
//		}
		/** Ends **/
	}

	
	private void createAppLevelMenuPanel(String[] menuNames) {
		
		menuLabels= new InlineLabel[menuNames.length];
		
		
		for(int i=0;i<menuNames.length;i++)
		{
			
			menuLabels[i] = new InlineLabel(menuNames[i]);
			System.out.println("Menu labels ====="+menuLabels[i]);
			appLevelMenuPanel.add(menuLabels[i]);
			
			if(i<1){
				menuLabels[i].getElement().setClassName("leftfloatbuttons");
			}
			else{
				menuLabels[i].getElement().setClassName("rightfloatbuttons");
			}
			
			menuLabels[i].getElement().getStyle().setMarginTop(25, Unit.PX);
			
			if(i==0){
				menuLabels[i].getElement().getStyle().setMarginLeft(20, Unit.PX);
			}
			if(i==2){
				menuLabels[i].getElement().getStyle().setMarginRight(20, Unit.PX);
			}
		}
		
		appLevelMenuPanel.setWidth("100%");	
		
		applevelMenu = menuLabels;

	}
	

	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource() instanceof InlineLabel)
		{
		
			InlineLabel lbl= (InlineLabel) event.getSource();
		
			if(lbl.getText().equals("Save")){
				presenter.reactOnSave();
			}
			if(lbl.getText().equals("Print")){
				presenter.reactOnPrint();
			}
			if(lbl.getText().equals("Edit")){
				presenter.reactOnEdit();
			}
			 /** Date 20.6.2018 added by komal for email**/
			if(lbl.getText().equals("Email")){
				presenter.reactOnEmail();
			}
			
			//Ashwini Patil Date:1-09-2022
			if(lbl.getText().equals("Message")){
				presenter.reactOnSMS();
			}
		}
		
	}
	
	public InlineLabel[] getMenuLabels() {
		return menuLabels;
	}

	public void setMenuLabels(InlineLabel[] menuLabels) {
		this.menuLabels = menuLabels;
	}

	public VerticalPanel getDocVerticalPanel() {
		return docVerticalPanel;
	}

	public void setDocVerticalPanel(VerticalPanel docVerticalPanel) {
		this.docVerticalPanel = docVerticalPanel;
	}

	public FlowPanel getAppLevelMenuPanel() {
		return appLevelMenuPanel;
	}

	public void setAppLevelMenuPanel(FlowPanel appLevelMenuPanel) {
		this.appLevelMenuPanel = appLevelMenuPanel;
	}

	public FlowPanel getBtnClosePanel() {
		return btnClosePanel;
	}

	public void setBtnClosePanel(FlowPanel btnClosePanel) {
		this.btnClosePanel = btnClosePanel;
	}

	public static InlineLabel[] getApplevelMenu() {
		return applevelMenu;
	}

	public static void setApplevelMenu(InlineLabel[] applevelMenu) {
		PopupEntityPresenter.applevelMenu = applevelMenu;
	}

	public Image getBtnClose() {
		return btnClose;
	}

	public void setBtnClose(Image btnClose) {
		this.btnClose = btnClose;
	}

	public EntityPresenter getPresenter() {
		return presenter;
	}

	public void setPresenter(EntityPresenter presenter) {
		this.presenter = presenter;
	}
	
	
	
	

}
