package com.slicktechnologies.client.notification;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface NotificationServiceAsync {

	void sendNotification(long id,String employeeName,String toApplication,String message, AsyncCallback<ArrayList<Integer>> callback);
	
}
