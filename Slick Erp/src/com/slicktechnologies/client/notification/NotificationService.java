package com.slicktechnologies.client.notification;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("notificationHandler")
public interface NotificationService extends RemoteService {

	ArrayList<Integer> sendNotification(long id, String userName,
			String sourceApplicationName,String message);

}
