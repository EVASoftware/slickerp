package com.slicktechnologies.client.approvalutility;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreenPresenter;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * Presenter for ApprovalForm Screens.
 * Contains reactToxxx() method specifec to Approvel Process.
 *
 * @param <T> the generic type
 */
public abstract class ApprovableFormScreenPresenter<T extends SuperModel> extends FormScreenPresenter<T>{

/** Reference of Approvable Form */
protected ApprovableFormScreen<T>form;
	
/**
 * Instantiates a new approvable form screen presenter.
 *
 * @param view the view
 * @param model the model
 */
public ApprovableFormScreenPresenter(FormScreen<T> view, T model) {
	super(view, model);
	form=(ApprovableFormScreen<T>) view;
	form.setPresenter(this);
	
}
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.EntityPresenter#reactToProcessBarEvents(com.google.gwt.event.dom.client.ClickEvent)
	 */
/**
 * Contains reactTo method for Approvable Processes.This method is responsible for 
 * handeling process Level bar clicks corresponding to Approvels.
 */
	@Override
	public void reactToProcessBarEvents(ClickEvent e) {
		InlineLabel label=(InlineLabel) e.getSource();
		String text=label.getText().trim();
		
		if(text.equals(ManageApprovals.APPROVALREQUEST))
			form.getManageapproval().reactToRequestForApproval();
		if(text.equals(ManageApprovals.CANCELAPPROVALREQUEST))
			form.getManageapproval().reactToApprovalRequestCancel();
		if(text.equals(ManageApprovals.SUBMIT))
			form.getManageapproval().reactToSubmit();
		if(text.equals(ManageApprovals.APPROVALSTATUS))
			form.getManageapproval().getApprovalStatus();
	}
}
