package com.slicktechnologies.client.approvalutility;

import com.google.gwt.user.client.ui.TextBox;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.ProcessLevelBar;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.common.businessprocesslayer.ApprovableProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;

// TODO: Auto-generated Javadoc
/**
 * Any Screen inheriting from this class will get Approver Flow.
 * Approver Flow :--->
 *   View mode process Level Bar :Reaquest          Status :Created
 *                                Cancel Request    Status :Requested Screen will be Uneditable Status Will Change To Created
 *                                None              Status : Approved Screen will be uneditable
 *                                None              Status : Rejected Screen will be uneditable
 *   
 *
 * @param <T> the generic type
 * @author Kamla
 */
public abstract class ApprovableFormScreen<T extends SuperModel> extends FormScreen<T> implements ApprovableScreen{

	/** Contains object which has approvel Functionality*/
	protected ManageApprovals manageapproval;
	
	/**
	 * Instantiates a new approvable form screen.
	 */
	protected ApprovableFormScreen()
	 {
		 this(DEFAULT_FORM_STYLE);
	 }
	 
	 /**
	 * Instantiates a new form screen with passed formstyle.
	 *	Instantiates the flexform with set fields & passed formstyle 
	 *& instantiates the processlevel bar if the prosesslevel menus are not null 
	 * @param formstyle the formstyle
	 */
	public ApprovableFormScreen(FormStyle formstyle)
	 {
		super(formstyle);
	
	}
	 
	 /**
 	 * Instantiates a new form screen with passed processlevel menus,
 	 * fields & formstyle.
 	 *
 	 * @param processlevel the processlevel menu names
 	 * @param fields the fields  The array of form field
 	 * @param formstyle the formstyle The default style of form
 	 */
	public ApprovableFormScreen( String[] processlevel,
			FormField[][] fields, FormStyle formstyle) 
	 {
		super(processlevel, fields, formstyle);
	}
	
	
	/**
	 * Once screen is in view state Manage Approval object is initalized.
	 * This object contains logic for Screen state and Process Level bar managment for Approval
	 * Processes.
	 */
	@Override
	public void setToViewState() 
	{
		super.setToViewState();
		ApprovableProcess businessprocess=(ApprovableProcess) getPresenter().getModel();
		ManageApprovals approvals=new ManageApprovals(businessprocess, this);
		
		/////////////////
		if(getPresenter().getModel().getCreatedBy()!=null){
			Console.log("DOCUMENT CREATED BY - "+getPresenter().getModel().getCreatedBy());
			approvals.setDocumentCreatedBy(getPresenter().getModel().getCreatedBy());
		}
		/**
		 *  nidhi
		 *  for customer and id set in approval list
		 */
		
		if(	getPresenter().getModel() instanceof Sales ||
				getPresenter().getModel() instanceof SalesQuotation ||
				getPresenter().getModel()instanceof Quotation ||
				getPresenter().getModel()instanceof Contract){
				Sales sales = (Sales) getPresenter().getModel();
				approvals.setBpId(sales.getCinfo().getCount()+"");
				approvals.setBpName(sales.getCinfo().getFullName());
			}
		/**
		 * @author Anil @since 27-04-2021
		 * Setting customer id and name for CNC documents
		 */
		if (getPresenter().getModel() instanceof CNC) {
			CNC cnc =(CNC) getPresenter().getModel();
			approvals.setBpId(cnc.getPersonInfo().getCount()+"");
			approvals.setBpName(cnc.getPersonInfo().getFullName());
		}
		//////////////
		
		setManageapproval(approvals);
		if(processLevelBar!=null)
		   manageapproval.toggleProcesslevelbar();
		
	}
	
	

	@Override
	public TextBox getstatustextbox() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void toggleAppHeaderBarMenu() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateModel(T model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateView(T model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setToEditState() {
		// TODO Auto-generated method stub
		super.setToEditState();
		if(processLevelBar!=null)
			   manageapproval.toggleProcesslevelbar();
	}

	/**
	 * Gets the manageapproval.
	 *
	 * @return the manageapproval
	 */
	public ManageApprovals getManageapproval() {
		return manageapproval;
	}

	/**
	 * Sets the manageapproval.
	 *
	 * @param manageapproval the new manageapproval
	 */
	public void setManageapproval(ManageApprovals manageapproval) {
		this.manageapproval = manageapproval;
	}

	
}
