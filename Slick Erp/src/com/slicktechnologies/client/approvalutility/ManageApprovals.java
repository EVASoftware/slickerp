package com.slicktechnologies.client.approvalutility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Vector;


import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppContainer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ProcessLevelBar;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.services.EmailServiceAsync;
import com.slicktechnologies.client.services.TaxInvoiceService;
import com.slicktechnologies.client.services.TaxInvoiceServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.approval.ApprovalService;
import com.slicktechnologies.client.views.approval.ApprovalServiceAsync;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.ApprovableProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.BusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;

/**
 * Handels Approvle Process.
 * Approvel Process :
 *            a) User selects approver from screen
 *            b) User {@link BusinessProcess} status is Created can send the approval request.
 *            c) Once user send the approval request {@link BusinessProcess} status is requested user cannot edit the form 
 *            d) In case of Approval rejection or sucessfull approval user can only see the screen he cannot edit it.
 *            e) Approver can see ony those document which he has been asked to approve.
 *            To Do : Pradnya implement the logic as in case of Login
 */
public class ManageApprovals 
{
	
	/** The businessprocess for which approval is being managed*/
	protected ApprovableProcess businessprocess;
	
	/** Current screen refrence a patch */
	protected UiScreen<?>currentScreen;
	
	/** The service to initate the approvel process*/
	public final GenricServiceAsync service=GWT.create(GenricService.class);
	
	/** The Constant APPROVALREQUEST. */
	public static final String APPROVALREQUEST="Request For Approval";
	
	/** The Constant CANCELAPPROVALREQUEST. */
	public static final String CANCELAPPROVALREQUEST="Cancel Approval Request";
	
	public static final String SUBMIT="Submit";
	
	/**
	 * Date : 03-03-2017 By ANIL
	 */
	public static final String APPROVALSTATUS="Approval Status";
	
	/**
	 * This variable stores the name of the person, who created particular approvable document.
	 * This is used in approval mail as CC so he can have track of document which he has created.
	 * Date : 14-10-2016 by Anil
	 * Release : 30 SEPT 2016
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	
	 String documentCreatedBy="";
	
	/**
	 * End
	 */
	 
	 /*
	  * rohan added this code on Date : 14-04-2017
	  * This is used for saving approval 
	  */
	
	 GenricServiceAsync genasync = GWT.create(GenricService.class);
	 /**
	  * ends here 
	  */
	
	EmailServiceAsync emailService=GWT.create(EmailService.class);
	ApprovalServiceAsync approvalService=GWT.create(ApprovalService.class);
	/**
	 * Instantiates a new manage approvals.
	 *
	 * @param businessprocess the businessprocess
	 * @param currentScreen the current screen
	 */
	/**
	 *  nidhi
	 *  6-10-2017
	 *  for display customer or company name in approval
	 *  screen
	 */
	String bpId = "";
	String bpName = "";
	
	/**
	 * @author Anil , Date : 28-05-2019
	 * On click of submit , changing label text from submit to processing..
	 * issue faced by NBHC ,double services and bills were created against contract
	 */
	public InlineLabel submitLbl;
	
	TaxInvoiceServiceAsync taxAsync = GWT.create(TaxInvoiceService.class);	
	
	/**
	 *  end
	 * @param businessprocess
	 * @param currentScreen
	 */
	public ManageApprovals(ApprovableProcess businessprocess,
			UiScreen<?> currentScreen) {
		super();
		this.businessprocess = businessprocess;
		this.currentScreen = currentScreen;
	}

	/**
	 * Toggels the Process Level Bar as Per the Status
	 */
	public void toggleProcesslevelbar()
	{
		
		AppContainer cont=AppMemory.getAppMemory().apl;
		String status=businessprocess.getStatus();
	
		ProcessLevelBar bar=currentScreen.getProcessLevelBar();
		InlineLabel[] lbls=bar.btnLabels;
		
		if(status.equals(ConcreteBusinessProcess.CREATED))
		{
			if(isSelfApproval()){
				for(InlineLabel temp:lbls)
				{
					String lbltext=temp.getText();
					if(lbltext.equals(ManageApprovals.APPROVALREQUEST))
						temp.setVisible(false);
					if(lbltext.equals(ManageApprovals.CANCELAPPROVALREQUEST))
						temp.setVisible(false);
					if(lbltext.equals(ManageApprovals.SUBMIT))
						temp.setVisible(true);
				}
			}else{
				for(InlineLabel temp:lbls)
				{
					String lbltext=temp.getText();
					if(lbltext.equals(ManageApprovals.APPROVALREQUEST))
						temp.setVisible(true);
					if(lbltext.equals(ManageApprovals.CANCELAPPROVALREQUEST))
						temp.setVisible(false);
					if(lbltext.equals(ManageApprovals.SUBMIT))
						temp.setVisible(false);
				}
			}
		}
		if(status.equals(ConcreteBusinessProcess.REQUESTED))
		{
			for(InlineLabel temp:lbls)
			{
				String lbltext=temp.getText();
				if(lbltext.equals(ManageApprovals.APPROVALREQUEST))
					temp.setVisible(false);
				if(lbltext.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					temp.setVisible(true);
			}
		}
		
		if(status.equals(ConcreteBusinessProcess.APPROVED))
		{
			for(InlineLabel temp:lbls)
			{
				String lbltext=temp.getText();
				if(lbltext.equals(ManageApprovals.APPROVALREQUEST))
					temp.setVisible(false);
				if(lbltext.equals(ManageApprovals.CANCELAPPROVALREQUEST))
					temp.setVisible(false);
			}
		}
		setAppHeaderBarAsPerStatus();
	}
	
	
	
	/**
	 * Sets the viewas per process.
	 */
	public void setAppHeaderBarAsPerStatus()
	{
		String status=businessprocess.getStatus();
		if(status.equals(ConcreteBusinessProcess.REQUESTED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Edit"))
					menus[k].setVisible(false); 
			}

		}

		if(status.equals(Quotation.APPROVED))
		{
			InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
			for(int k=0;k<menus.length;k++)
			{
				String text=menus[k].getText();
				if(text.contains("Discard")||text.contains("Search")||text.contains("Email")||text.contains("Print"))
				{
					menus[k].setVisible(true); 
				}
				else
					menus[k].setVisible(false);  		   
			}
		}
		
	}
	
	
	/**
	 * React to request for approval.
	 */
	public void reactToRequestForApproval() {
		currentScreen.showWaitSymbol();
		ApproverFactory appFactory=new ApproverFactory();
		/**
		 *  nidhi
		 *  7-10-2017
		 *   for customer approval name display
		 */
		Approvals approval=appFactory.getApprovals(businessprocess.getEmployee(), businessprocess.getApproverName()
				, businessprocess.getBranch(), businessprocess.getCount(),documentCreatedBy,bpId,bpName);

		/**
		 * end
		 */
		
		saveApproval(approval,ConcreteBusinessProcess.REQUESTED,"Request Sent!");
		reactOnApprovalEmail(approval);
	}

	/**
	 * Save approval.
	 *
	 * @param approval the approval
	 * @param status the status
	 * @param dialogmsg the dialogmsg
	 */
	private void saveApproval(final Approvals approval,final String status,final String dialogmsg)
	{
		service.save(approval, new AsyncCallback<ReturnFromServer>() {
			@Override
			public void onSuccess(ReturnFromServer result) 
			{
				businessprocess.setStatus(status);
				SuperModel model=(SuperModel) businessprocess;
				service.save(model, new AsyncCallback<ReturnFromServer>() {
					@Override
					public void onFailure(Throwable caught) {
						currentScreen.hideWaitSymbol();
					}
					@Override
					public void onSuccess(ReturnFromServer result) {
						ApprovableScreen ap=(ApprovableScreen) currentScreen;
						ap.getstatustextbox().setValue(status);
						currentScreen.setToViewState();
						currentScreen.showDialogMessage(dialogmsg);
						currentScreen.hideWaitSymbol();
						//Special for Form Table Screen
						if(ap instanceof FormTableScreen)
						{
							FormTableScreen<?>screen=(FormTableScreen<?>) ap;
							((FormTableScreen) ap).getSupertable().getDataprovider().refresh();
						}
					}
				});
			}
			
			@Override
			public void onFailure(Throwable caught) {
				currentScreen.showDialogMessage("Request Failed! Try Again!");
				currentScreen.hideWaitSymbol();
			}
		});
	}
	
	  /**
  	 * React to approval request cancel.
  	 */
  	public void reactToApprovalRequestCancel()
	  {
  		currentScreen.showWaitSymbol(); 
  		ApproverFactory approverfactory=new ApproverFactory();
		  
		  Filter filter=new Filter();
		  filter.setStringValue(approverfactory.getBuisnessProcessType());
		  filter.setQuerryString("businessprocesstype");
		  
		  Filter filterOnid=new Filter();
		  filterOnid.setIntValue(businessprocess.getCount());
		  filterOnid.setQuerryString("businessprocessId");
		  
		  Filter filterOnStatus=new Filter();
		  filterOnStatus.setStringValue(Approvals.PENDING);
		  filterOnStatus.setQuerryString("status");
		  
		  Vector<Filter> vecfilter=new Vector<Filter>();
		  vecfilter.add(filter);
		  vecfilter.add(filterOnid);
		  vecfilter.add(filterOnStatus);
		  MyQuerry querry=new MyQuerry(vecfilter, new Approvals());
		  final ArrayList<SuperModel> array = new ArrayList<SuperModel>();
		  service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				//  old code 
//				array.addAll(result);
//				Approvals approval=(Approvals) array.get(0);
//				approval.setStatus(Approvals.CANCELLED);
//				 
//				 saveApproval(approval, ConcreteBusinessProcess.CREATED, "Approval Request Cancelled!");
				
				
				/** New code
				 * Date : 11-02-2017 By Anil
				 * while canceling if approval result size is zero then we assume that some error has occurred
				 * and document is in processed state
				 * for this we have shown a display message
				 */
				if(result.size()!=0){
					array.addAll(result);
					Approvals approval=(Approvals) array.get(0);
					approval.setStatus(Approvals.CANCELLED);
					saveApproval(approval, ConcreteBusinessProcess.CREATED, "Approval Request Cancelled!");
				}else{
					currentScreen.hideWaitSymbol();
					currentScreen.showDialogMessage("This document is in process state.Please Contact Support! ");
				}
				/**
				 * End
				 */
				
			}
		});
	  }
  	
  	public void reactOnApprovalEmail(Approvals approvalEntity)
	{
		//Quotation Entity patch patch patch
		emailService.initiateApprovalEmail(approvalEntity,new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Unable To Send Email");
				caught.printStackTrace();
			}

			@Override
			public void onSuccess(Void result) {
//				Window.alert("Email Sent Sucessfully !");
			}
		});
	}
  	
  	
  	public void reactToSubmit(){
  		
  		
  		ApproverFactory appFactory=new ApproverFactory();
		/**
		 *6-10-2017
		 *  for display customer or company name in approval
		 */
//		final Approvals approval=appFactory.getApprovals(businessprocess.getEmployee(),businessprocess.getApproverName(), businessprocess.getBranch(), businessprocess.getCount(),documentCreatedBy);
  		final Approvals approval=appFactory.getApprovals(businessprocess.getEmployee(), businessprocess.getApproverName()
				, businessprocess.getBranch(), businessprocess.getCount(),documentCreatedBy,bpId,bpName);
  		approval.setStatus(ConcreteBusinessProcess.APPROVED);
		approval.setApprovalOrRejectDate(new Date());
		currentScreen.showWaitSymbol();	
		
		//  rohan added this code for saving approval entity and sendApproveRequest
		genasync.save(approval, new AsyncCallback<ReturnFromServer>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ReturnFromServer result) {
				approval.setId(result.id);
				System.out.println("rohan get id"+approval.getId());
				approvalService.sendApproveRequest(approval, new AsyncCallback<String>() {
					@Override
					public void onSuccess(String result) {
//						businessprocess.setStatus(ConcreteBusinessProcess.APPROVED);
//						ApprovableScreen ap=(ApprovableScreen) currentScreen;
//						ap.getstatustextbox().setValue(ConcreteBusinessProcess.APPROVED);
//						
//						currentScreen.setToViewState();
//						currentScreen.showDialogMessage("Submited successfully!");
//						currentScreen.hideWaitSymbol();
						/**
						 * @author Anil , Date : 28-05-2019
						 */
						if(submitLbl!=null){
							submitLbl.setText(AppConstants.SUBMIT);
							System.out.println("AFTER SUCESS "+submitLbl.getText());
						}
						
						currentScreen.hideWaitSymbol();
						if(result.equals("Success")){
							businessprocess.setStatus(ConcreteBusinessProcess.APPROVED);
							ApprovableScreen ap=(ApprovableScreen) currentScreen;
							ap.getstatustextbox().setValue(ConcreteBusinessProcess.APPROVED);
							currentScreen.setToViewState();
							/*
							 * Ashwini Patil
							 * Date:6-5-2024
							 * Rex wants to integrate eva invoices with zoho. Whenever invoice is getting approved in eva, need to create invoice in zoho books through api
							 */
							if(approval.getBusinessprocesstype().equals(ApproverFactory.INVOICEDETAILS)){
								if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableInvoiceIntegrationWithZohoBooks")) {
									Timer timer = new Timer() {
										@Override
										public void run() {
											
											taxAsync.createInvoiceInZohoBooks(approval.getBusinessprocessId(), approval.getCompanyId(),LoginPresenter.loggedInUser,new AsyncCallback<String>() {

												@Override
												public void onFailure(Throwable caught) {
													currentScreen.showDialogMessage("Invoice Submitted Successfully in EVA ERP but failed to create invoice in zoho books. Please sync manually.");
												}

												@Override
												public void onSuccess(String result) {
													if(result!=null) {
														if(result.equalsIgnoreCase("Success"))
															currentScreen.showDialogMessage("Invoice Submitted in EVA and created in Zoho books successfully");
														else
															currentScreen.showDialogMessage("Invoice Submitted in EVA Successfully but failed to create in zoho books. API result : "+result+"\n Contact EVA Support");
													}else
														currentScreen.showDialogMessage("Invoice Submitted Successfully in EVA ERP but failed to create invoice in zoho books! Please sync manually.");
													
												}
											});
										}
									};
									timer.schedule(1500);
						
								}else					
									currentScreen.showDialogMessage("Submited successfully!");
								
							}else
								currentScreen.showDialogMessage("Submited successfully!");
						}else{
							currentScreen.showDialogMessage(result);
						}
						
					}
					
					@Override
					public void onFailure(Throwable caught) {
						currentScreen.hideWaitSymbol();
						currentScreen.showDialogMessage("Unexpected error occured!");
						/**
						 * @author Anil , Date : 28-05-2019
						 */
						if(submitLbl!=null){
							submitLbl.setText(AppConstants.SUBMIT);
							System.out.println("AFTER FAILURE "+submitLbl.getText());
						}
					}
				});
			}
		});
  	}
  	
	public String getDocumentCreatedBy() {
		return documentCreatedBy;
	}

	public void setDocumentCreatedBy(String documentCreatedBy) {
		this.documentCreatedBy = documentCreatedBy;
	}
  	
	

	/**
	 * Date :12-02-2017 By  Anil
	 * This method checks whether self approval is active for particular document or not 
	 * depending upon that shows submit/request for approval button   
	 **/
	
	public boolean isSelfApproval(){
		
		if(!LoginPresenter.loggedInUser.equals(businessprocess.getApproverName()))
		{
			return false;
		}
		
		String sr[]=currentScreen.toString().split("\\.");
		String str = sr[sr.length-1];
		String sr1[]=str.split("\\@");
		String screenName = sr1[0];
		
		System.out.println("Screen Name : "+screenName);
		
		switch(screenName){
			case "MaterialRequestNoteForm":
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialRequestNote", "SelfApproval")){
					return true;
				}else{
					return false;
				}
			case "MaterialIssueNoteForm":
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialIssueNote", "SelfApproval")){
					return true;
				}else{
					return false;
				}
			case "MaterialMovementNoteForm":
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialMovementNote", "SelfApproval")){
					return true;
				}else{
					return false;
				}
			case "GRNForm":
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("GRN", "SelfApproval")){
					return true;
				}else{
					return false;
				}
			case "PurchaseRequisitionForm":
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition", "SelfApproval")){
					return true;
				}else{
					return false;
				}
			case "RequestForQuotationForm":
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("RequsestForQuotation", "SelfApproval")){
					return true;
				}else{
					return false;
				}
			case "LetterOfIntentForm":
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("LetterOfIntent", "SelfApproval")){
					return true;
				}else{
					return false;
				}
			case "PurchaseOrderForm":
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", "SelfApproval")){
					return true;
				}else{
					return false;
				}
			case "QuotationForm":
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Quotation", "SelfApproval")){
					return true;
				}else{
					return false;
				}
			case "ContractForm":
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "SelfApproval")){
					return true;
				}else{
					return false;
				}
			case "SalesQuotationForm":
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesQuotation", "SelfApproval")){
					return true;
				}else{
					return false;
				}
			case "SalesOrderForm":
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesOrder", "SelfApproval")){
					return true;
				}else{
					return false;
				}
			case "DeliveryNoteForm":
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("DeliveryNote", "SelfApproval")){
					return true;
				}else{
					return false;
				}
			case "WorkOrderForm":
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("WorkOrder", "SelfApproval")){
					return true;
				}else{
					return false;
				}
			case "BillingDetailsForm":
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "SelfApproval")){
					return true;
				}else{
					return false;
				}
			case "InvoiceDetailsForm":
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "SelfApproval")){
					return true;
				}else{
					return false;
				}	
				/** 21-07-2017 by sagar sore Self approval for multiple expense management **/
			case "MultipleExpensemanagmentForm":
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("MultipleExpenseMngt", "SelfApproval")){
					System.out.println("Self approval for MultipleExpenseMgnt true");
					return true;
				}else{
					System.out.println("Self approval for MultipleExpenseMgnt false");
					return false;
				}	
				/** date 22.8.2018 added by komal for cnc self approval**/
			case "CNCForm":
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "SelfApproval")){
					return true;
				}else{
					return false;
				}	

			case "VendorInvoiceDetailsForm":
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("VendorInvoice", "SelfApproval")){
					return true;
				}else{
					return false;
				}	

				/**
				 * end komal
				 */
			case "WareHouseDetailsForm":
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PhysicalInventoryMaintaince", "SelfApproval")){
					return true;
				}else{
					return false;
				}	 //ProductInventory
				/**
				 * @author Anil
				 * @since 05-01-2021
				 * Added self approval functionality on Service PO
				 */
			case "ServicePoForm":
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ServicePo", "SelfApproval")){
					return true;
				}else{
					return false;
				}	
			case "AdvanceForm":
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Advance", "SelfApproval")){
					return true;
				}else{
					return false;
				}	
			default :
				return false;
				
		}
		
	}
	
	/**
	 * Date : 03-03-2017 By ANIL
	 * This Method retrieves the document status which are pending for approval
	 */
	public void getApprovalStatus() {
		currentScreen.showWaitSymbol();
		ApproverFactory approverfactory = new ApproverFactory();

		Filter filter = new Filter();
		filter.setStringValue(approverfactory.getBuisnessProcessType());
		filter.setQuerryString("businessprocesstype");

		Filter filterOnid = new Filter();
		filterOnid.setIntValue(businessprocess.getCount());
		filterOnid.setQuerryString("businessprocessId");

		Vector<Filter> vecfilter = new Vector<Filter>();
		vecfilter.add(filter);
		vecfilter.add(filterOnid);
		MyQuerry querry = new MyQuerry(vecfilter, new Approvals());

		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
				currentScreen.hideWaitSymbol();
			}
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				currentScreen.hideWaitSymbol();
				ArrayList<Approvals> array = new ArrayList<Approvals>();
				if (result.size() != 0) {
					for (SuperModel model : result) {
						Approvals approval = (Approvals) model;
						array.add(approval);
					}
					
					Comparator<Approvals> comparator = new Comparator<Approvals>() {  
						@Override  
						public int compare(Approvals o1, Approvals o2) {
							Integer approvalCount1=o1.getCount();
							Integer approvalCount2=o2.getCount();
							return approvalCount1.compareTo(approvalCount2);  
						}  
					};  
					Collections.sort(array,comparator);
					
					ApprovalStatusPopup statusPopup=new ApprovalStatusPopup();
					PopupPanel panel=new PopupPanel(true);
					statusPopup.clear();
					statusPopup.getApprovalStatusTbl().getDataprovider().getList().addAll(array);
					panel.add(statusPopup);
					panel.center();
					panel.show();
					
	
				} else {
					currentScreen.showDialogMessage("No document found in approval.");
				}
			}
		});
	}

	public String getBpId() {
		return bpId;
	}

	public void setBpId(String bpId) {
		this.bpId = bpId;
	}

	public String getBpName() {
		return bpName;
	}

	public void setBpName(String bpName) {
		this.bpName = bpName;
	}
	
}
