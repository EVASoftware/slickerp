package com.slicktechnologies.client.approvalutility;

import com.google.gwt.user.client.ui.TextBox;

// TODO: Auto-generated Javadoc
/**
 * The Interface ApprovableScreen.It contains getstatusTextBox method 
 * which returns the Text Box Status.
 */
public interface ApprovableScreen {
	
	/**
	 * Gets the statustextbox.
	 *
	 * @return the statustextbox
	 */
	public TextBox getstatustextbox();
	

}
