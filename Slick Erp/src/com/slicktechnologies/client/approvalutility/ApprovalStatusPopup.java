package com.slicktechnologies.client.approvalutility;

import com.google.gwt.dom.client.Style.FontStyle;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.InlineLabel;

public class ApprovalStatusPopup extends AbsolutePanel{

ApprovalStatusTable approvalStatusTbl;
	
	private void initializeWidget(){
		approvalStatusTbl=new ApprovalStatusTable();
		approvalStatusTbl.getTable().setHeight("400px");
	}
	
	public ApprovalStatusPopup() {
		initializeWidget();
		
		InlineLabel label=new InlineLabel("Approval Status ");
		label.getElement().getStyle().setFontSize(20, Unit.PX);
		label.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
		label.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		
		add(label,370,10);//add(label,230,10);
		add(approvalStatusTbl.getTable(),10,30);
		
//		setSize("600px","220px");
		setSize("860px","420px");
		this.getElement().setId("form");
	}
	
	public void clear(){
		approvalStatusTbl.connectToLocal();
	}

	public ApprovalStatusTable getApprovalStatusTbl() {
		return approvalStatusTbl;
	}

	public void setApprovalStatusTbl(ApprovalStatusTable approvalStatusTbl) {
		this.approvalStatusTbl = approvalStatusTbl;
	}
	
}
