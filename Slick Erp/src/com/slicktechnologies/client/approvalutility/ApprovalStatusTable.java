package com.slicktechnologies.client.approvalutility;

import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Approvals;

public class ApprovalStatusTable extends SuperTable<Approvals>{


	TextColumn<Approvals> businessprocesstypeCol;
	TextColumn<Approvals> businessprocessidCol;
	TextColumn<Approvals> requestedbyCol;
	TextColumn<Approvals> requestdateCol;
	TextColumn<Approvals> statusCol;
	TextColumn<Approvals> branchCol;
	TextColumn<Approvals> approverNameCol;
	TextColumn<Approvals> requestCol;
	TextColumn<Approvals> remarkCol;
	TextColumn<Approvals> approvalOrRejectDateCol;
	TextColumn<Approvals> levelCol;


	public ApprovalStatusTable() {
		super();
	}
	
	@Override
	public void createTable() {
//		createColumnBusinessProcessType();
		createColumnBusinessprocessId();
		createColumnRequestedBy();
		createColumnRequestDate();
		createColumnLevel();
		createColumnApproverName();
//		createColumnBranch();
		createColumnStatus();
		addColumnRemark();
		addColumnApproveOrRejectDate();
	}
	
	private void createColumnBusinessProcessType() {
		businessprocesstypeCol = new TextColumn<Approvals>() {
			@Override
			public String getValue(Approvals object) {
				return object.getBusinessprocesstype();
			}
		};
		table.addColumn(businessprocesstypeCol, "Doc Type");
		businessprocesstypeCol.setSortable(true);
		table.setColumnWidth(businessprocesstypeCol, 120, Unit.PX);

	}

	private void createColumnBusinessprocessId() {
		businessprocessidCol = new TextColumn<Approvals>() {

			@Override
			public String getValue(Approvals object) {

				return object.getBusinessprocessId() + "";
			}
		};
		table.addColumn(businessprocessidCol, "Doc Id");
		businessprocessidCol.setSortable(true);
		table.setColumnWidth(businessprocessidCol, 100, Unit.PX);
	}

	private void createColumnRequestedBy() {
		requestedbyCol = new TextColumn<Approvals>() {

			@Override
			public String getValue(Approvals object) {

				return object.getRequestedBy();
			}

		};
		table.addColumn(requestedbyCol, "Requested By");
		requestedbyCol.setSortable(true);
		table.setColumnWidth(requestedbyCol, 120, Unit.PX);
	}

	private void createColumnBranch() {
		branchCol = new TextColumn<Approvals>() {

			@Override
			public String getValue(Approvals object) {

				return object.getBranchname();
			}

		};
		table.addColumn(branchCol, "Branch");
		branchCol.setSortable(true);
		table.setColumnWidth(branchCol, 100, Unit.PX);
	}

	private void createColumnStatus() {
		statusCol = new TextColumn<Approvals>() {

			@Override
			public String getValue(Approvals object) {

				return object.getStatus();
			}

		};
		table.addColumn(statusCol, "Status");
		statusCol.setSortable(true);
		table.setColumnWidth(statusCol, 100, Unit.PX);
	}

	private void createColumnApproverName() {
		approverNameCol = new TextColumn<Approvals>() {

			@Override
			public String getValue(Approvals object) {

				return object.getApproverName();
			}
		};
		table.addColumn(approverNameCol, "Approver Name");
		approverNameCol.setSortable(true);
		table.setColumnWidth(approverNameCol, 120, Unit.PX);
	}

	private void createColumnRequestDate() {
		requestdateCol = new TextColumn<Approvals>() {

			@Override
			public String getValue(Approvals object) {
				if (object.getCreationDate() != null) {
					return AppUtility.parseDate(object.getCreationDate());
				} else {
					return "";
				}
			}
		};
		table.addColumn(requestdateCol, "Requested Date");
		requestdateCol.setSortable(true);
		table.setColumnWidth(requestdateCol, 110, Unit.PX);
	}

	private void addColumnRemark() {

		remarkCol = new TextColumn<Approvals>() {

			@Override
			public String getValue(Approvals object) {

				return object.getRemark();
			}
		};
		table.addColumn(remarkCol, "Remark");
		remarkCol.setSortable(true);
		table.setColumnWidth(remarkCol, 110, Unit.PX);

	}

	private void addColumnApproveOrRejectDate() {

		approvalOrRejectDateCol = new TextColumn<Approvals>() {

			@Override
			public String getValue(Approvals object) {
				if (object.getApprovalOrRejectDate() != null) {
					return AppUtility.parseDate(object.getApprovalOrRejectDate());
				} else {
					return "";
				}
			}
		};
		table.addColumn(approvalOrRejectDateCol, "Approved/Reject Date");
		approvalOrRejectDateCol.setSortable(true);
		table.setColumnWidth(approvalOrRejectDateCol, 110, Unit.PX);
	}

	private void createColumnLevel() {

		levelCol = new TextColumn<Approvals>() {

			@Override
			public String getValue(Approvals object) {

				return object.getApprovalLevel() + "";
			}
		};
		table.addColumn(levelCol, "Approver Level");
		levelCol.setSortable(true);
		table.setColumnWidth(levelCol, 70, Unit.PX);

	}

	@Override
	protected void initializekeyprovider() {

	}

	@Override
	public void addFieldUpdater() {
	}

	public void addColumnSorting() {

		addColumnBusinessProcessTypeSorting();
		addColumnBusinessprocessIdSorting();
		addColumRequestedBySorting();
		addColumnApproverNameSorting();
		addColumnBranchSorting();
		addColumnStatusSorting();
		addColumnRequestDate();
		addColumnApprovalOrRejDateSorting();
		
		addSortingOnApprovalCol();
	}

	private void addSortingOnApprovalCol() {
		List<Approvals> list = getDataprovider().getList();
		columnSort = new ListHandler<Approvals>(list);
		columnSort.setComparator(levelCol,new Comparator<Approvals>() {
			@Override
			public int compare(Approvals e1, Approvals e2) {
				if (e1 != null && e2 != null) {
					if (e1.getApprovalLevel() == e2.getApprovalLevel()) {
						return 0;
					}
					if (e1.getApprovalLevel() > e2.getApprovalLevel()) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		});
		table.addColumnSortHandler(columnSort);
		
	}

	private void addColumnRequestDate() {

		List<Approvals> list = getDataprovider().getList();
		columnSort = new ListHandler<Approvals>(list);
		columnSort.setComparator(requestdateCol, new Comparator<Approvals>() {
			@Override
			public int compare(Approvals e1, Approvals e2) {
				if (e1 != null && e2 != null) {
					if (e1.getCreationDate() != null
							&& e2.getCreationDate() != null) {
						return e1.getCreationDate().compareTo(
								e2.getCreationDate());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);

	}

	private void addColumnStatusSorting() {

		List<Approvals> list = getDataprovider().getList();
		columnSort = new ListHandler<Approvals>(list);
		columnSort.setComparator(statusCol, new Comparator<Approvals>() {
			@Override
			public int compare(Approvals e1, Approvals e2) {
				if (e1 != null && e2 != null) {
					if (e1.getStatus() != null && e2.getStatus() != null) {
						return e1.getStatus().compareTo(e2.getStatus());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);

	}

	private void addColumnBranchSorting() {

		List<Approvals> list = getDataprovider().getList();
		columnSort = new ListHandler<Approvals>(list);
		columnSort.setComparator(branchCol, new Comparator<Approvals>() {
			@Override
			public int compare(Approvals e1, Approvals e2) {
				if (e1 != null && e2 != null) {
					if (e1.getBranchname() != null
							&& e2.getBranchname() != null) {
						return e1.getBranchname().compareTo(e2.getBranchname());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);
	}

	private void addColumnApproverNameSorting() {

		List<Approvals> list = getDataprovider().getList();
		columnSort = new ListHandler<Approvals>(list);
		columnSort.setComparator(approverNameCol, new Comparator<Approvals>() {
			@Override
			public int compare(Approvals e1, Approvals e2) {
				if (e1 != null && e2 != null) {
					if (e1.getApproverName() != null
							&& e2.getApproverName() != null) {
						return e1.getApproverName().compareTo(
								e2.getApproverName());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);

	}

	private void addColumRequestedBySorting() {

		List<Approvals> list = getDataprovider().getList();
		columnSort = new ListHandler<Approvals>(list);
		columnSort.setComparator(requestedbyCol, new Comparator<Approvals>() {
			@Override
			public int compare(Approvals e1, Approvals e2) {
				if (e1 != null && e2 != null) {
					if (e1.getRequestedBy() != null
							&& e2.getRequestedBy() != null) {
						return e1.getRequestedBy().compareTo(
								e2.getRequestedBy());
					}
				} else {
					return 0;
				}
				return 0;
			}
		});
		table.addColumnSortHandler(columnSort);

	}

	private void addColumnBusinessprocessIdSorting() {

		List<Approvals> list = getDataprovider().getList();
		columnSort = new ListHandler<Approvals>(list);
		columnSort.setComparator(businessprocessidCol,
				new Comparator<Approvals>() {
					@Override
					public int compare(Approvals e1, Approvals e2) {
						if (e1 != null && e2 != null) {
							if (e1.getBusinessprocessId() == e2
									.getBusinessprocessId()) {
								return 0;
							}
							if (e1.getBusinessprocessId() > e2
									.getBusinessprocessId()) {
								return 1;
							} else {
								return -1;
							}
						} else {
							return 0;
						}
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	private void addColumnBusinessProcessTypeSorting() {

		List<Approvals> list = getDataprovider().getList();
		columnSort = new ListHandler<Approvals>(list);
		columnSort.setComparator(businessprocesstypeCol,
				new Comparator<Approvals>() {
					@Override
					public int compare(Approvals e1, Approvals e2) {
						if (e1 != null && e2 != null) {
							if (e1.getBusinessprocesstype() != null
									&& e2.getBusinessprocesstype() != null) {
								return e1.getBusinessprocesstype().compareTo(
										e2.getBusinessprocesstype());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);

	}

	private void addColumnApprovalOrRejDateSorting() {

		List<Approvals> list = getDataprovider().getList();
		columnSort = new ListHandler<Approvals>(list);
		columnSort.setComparator(approvalOrRejectDateCol,
				new Comparator<Approvals>() {
					@Override
					public int compare(Approvals e1, Approvals e2) {
						if (e1 != null && e2 != null) {
							if (e1.getApprovalOrRejectDate() != null
									&& e2.getApprovalOrRejectDate() != null) {
								return e1.getApprovalOrRejectDate().compareTo(
										e2.getApprovalOrRejectDate());
							}
						} else {
							return 0;
						}
						return 0;
					}
				});
		table.addColumnSortHandler(columnSort);
	}

	@Override
	public void setEnable(boolean state) {

	}

	@Override
	public void applyStyle() {

	}
}
