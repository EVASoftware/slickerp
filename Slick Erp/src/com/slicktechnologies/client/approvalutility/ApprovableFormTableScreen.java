package com.slicktechnologies.client.approvalutility;

import com.google.gwt.user.client.ui.ScrollPanel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.slicktechnologies.shared.common.businessprocesslayer.ApprovableProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * Any Screen inheriting from this class will get Approver Flow.
 * Approver Flow :--->
 *   View mode process Level Bar :Reaquest          Status :Created
 *                                Cancel Request    Status :Requested Screen will be Uneditable Status Will Change To Created
 *                                None              Status : Approved Screen will be uneditable
 *                                None              Status : Rejected Screen will be uneditable
 *   
 *
 * @param <T> the generic type
 * @author Kamla
 */
public abstract class ApprovableFormTableScreen<T> extends FormTableScreen<T> implements ApprovableScreen{

	/** The manageapproval. */
	ManageApprovals manageapproval;
	
	/**
	 * Instantiates a new approvable form table screen.
	 */
	public  ApprovableFormTableScreen() {
		super();
		createGui();
	}
	
	/**
	 * Instantiates a new approvable form table screen.
	 *
	 * @param table the table
	 * @param mode the mode
	 * @param captionmode the captionmode
	 */
	public ApprovableFormTableScreen(SuperTable<T>table,int mode,boolean captionmode)
	{
		this(table);
		supertable.setView(this);
		supertable.applySelectionModle();
	    this.CAPTION_MODE=captionmode;
	    this.MODE=mode;
	}
	
	/**
	 * Instantiates a new approvable form table screen.
	 *
	 * @param table the table
	 */
	public ApprovableFormTableScreen(SuperTable<T>table)
	{
		super();
		this.supertable=table;
		scrollpanel= new ScrollPanel();
	
	
	}
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#toggleAppHeaderBarMenu()
	 */
	@Override
	public void toggleAppHeaderBarMenu() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#createScreen()
	 */
	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#updateModel(java.lang.Object)
	 */
	@Override
	public void updateModel(T model) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen#updateView(java.lang.Object)
	 */
	@Override
	public void updateView(T model) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Once screen is in view state Manage Approval object is initalized.
	 * This object contains logic for Screen state and Process Level bar managment for Approval
	 * Processes.
	 */
	@Override
	public void setToViewState() 
	{
		super.setToViewState();
		ApprovableProcess businessprocess=(ApprovableProcess) getPresenter().getModel();
		ManageApprovals approvals=new ManageApprovals(businessprocess, this);
		setManageapproval(approvals);
		if(processLevelBar!=null)
		   manageapproval.toggleProcesslevelbar();
		
	}
	
	@Override
	public void setToEditState() {
		// TODO Auto-generated method stub
		super.setToEditState();
		if(processLevelBar!=null)
			   manageapproval.toggleProcesslevelbar();
	}

	/**
	 * Gets the manageapproval.
	 *
	 * @return the manageapproval
	 */
	public ManageApprovals getManageapproval() {
		return manageapproval;
	}

	/**
	 * Sets the manageapproval.
	 *
	 * @param manageapproval the new manageapproval
	 */
	public void setManageapproval(ManageApprovals manageapproval) {
		this.manageapproval = manageapproval;
	}

}
