package com.slicktechnologies.client.services;

import java.util.Date;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

@RemoteServiceRelativePath("licenseupdate")
public interface LicenseUpdationService extends RemoteService {
	Integer updateLicenses(long companyId,int noOfUsers,Date licenseUpdateDate);
	Company retrieveCompanyInfo(long companyId);
}
