package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.common.helperlayer.LoggedIn;
import com.slicktechnologies.shared.common.helperlayer.User;

@RemoteServiceRelativePath("sessionserv")
public interface SessionService extends RemoteService {
	
	String getUserSessionTimeout(String userName,int sessionId,long companyId,String loggedInUser);
	Boolean isSessionAlive(String sessionIdbyserver, String user,int sessionCount,long companyId);
	Integer ping(String userName,int sessionId,boolean isPing,long companyId,String loggedInUser,String opeartion,int numberOfUser);
	Boolean getSessionArrayFromServer(User myUserEntity);
	void clearAllLoggedInUsers(ArrayList<LoggedIn> activeUsers,Long companyId);
	Boolean getUserStatusInactiveAndRemoveSessionFromServer(ArrayList<LoggedIn> activeUsers,Long companyId);

	/**   rohan added this method for removing user from session array where as User is Inactive in LoggedIN Entity
	 * Date : 27/03/2017
	 */
	 Boolean removeUserFromSessionArray(String userName, String employeeName);
}
