package com.slicktechnologies.client.services;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.common.helperlayer.User;

@RemoteServiceRelativePath("logoutservice")
public interface LogoutService extends RemoteService {
	public User  doLogout(Long companyId,String username,String remark,int sessionId);
	public User doLogoutonBrowserClose(Long companyId,String username,String remark,int sessionId);
}
