package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.personlayer.Employee;
public interface CustomerNameChangeServiceAsync {

	void SaveCustomerChangeInAllDocs(Customer model,boolean updateCustomerAddressFlag, AsyncCallback<ArrayList<Integer>> callback);
	
	/**Date 26-4-2019 
	 * @author Amol
	 * Added a method for to update the cell no of Employee when we updated it in employee master and it 
	 * should be reflect in salary slip ,CTC and Leave balance
	 *
	 */
	void SaveEmployeeChangeInAllDocs(Employee model,AsyncCallback<ArrayList<Integer>> callback);
	
	/**
	 * Date 22-05-2019 by Vijay For Complaint service creation
	 */
	void createComplaintService(Complain complain, int complaintId, AsyncCallback<String> callback);
	
	/**
	 * Date 04-11-2019 By Vijay for NBHC CCPM Service Revenue loss Report. Approved Contract Cancelled Services
	 */
	void getCancelledServiceOfApprovedContract(long companyId, Date fromDate, Date toDate,boolean flag, ArrayList<String> brnachlist,ArrayList<String>emailIdList, AsyncCallback<String> callback);
	
	void createServiceAssesmentForComplain(Complain complain,AsyncCallback<String>callback);

	void updateCustomerEmailIdInContract(long companyId,Date fromDate,Date toDate,AsyncCallback<String>callback);
	void updateServicesStatusOpen(long companyId,AsyncCallback<String>callback);
	void getInvoicePrefixDetails(long companyId,String branchName,AsyncCallback<String>callback);
	
	void updateCustomerBranchServiceAddress(long companyId,int contractId, String customerBranch, AsyncCallback<String> callback);
	void updateCustomerServiceAddress(long companyId, int customerId, AsyncCallback<String> callback);
	void updateVendor(Vendor model, AsyncCallback<ArrayList<Integer>> callback);
	
}
