package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApprovalDetails;

@RemoteServiceRelativePath("dropdownserv")
public interface DropDownService extends RemoteService {
	ArrayList<SuperModel> getDropDown(String type);
	ArrayList<String> getApprovalDropDown(long companyId,ArrayList<MultilevelApprovalDetails> approvalLis,int level);

	/**
	 * Date : 27-02-2017 By ANIL
	 * This method loads employees as per branch ,assigned to login person.
	 */
	ArrayList<SuperModel> getEmployeeDropDown(List<String> branchList,Long companyId);	
}
