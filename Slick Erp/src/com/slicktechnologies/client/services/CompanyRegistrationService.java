package com.slicktechnologies.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

@RemoteServiceRelativePath("registrationservice")
public interface CompanyRegistrationService extends RemoteService {
	
	public ReturnFromServer saveCompany(Company company);
	 public ArrayList<SuperModel> getSearchResult(MyQuerry quer);

	 /**
	  * Date 28 feb 2017
	  * added by vijay
	  * for creating configs on click in company presenter create configs button
	  */

	 public ArrayList<String> createConfigsData(long compnayId);
	 public String resetAppid(long companyId,String appId,String companyName,String loggedinuser,int otp,String approverCellNo);
}
