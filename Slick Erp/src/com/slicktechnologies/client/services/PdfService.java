package com.slicktechnologies.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;

@RemoteServiceRelativePath("printservice")
public interface PdfService extends RemoteService  {
	public void createPdf(Quotation quot);
	public void setServiceList(ArrayList<Service>aray) throws IllegalArgumentException;
	
}
