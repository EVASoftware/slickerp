package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;

public interface CustomerProjectServiceAsync {
	void createCustomerProject(long companyId,int contractCount,int serviceCount,String serviceStatus,AsyncCallback<Integer> callback);
	void checkCustomerProject(long companyId, int contractCount,int serviceCount,String serviceStatus,ArrayList<EmployeeInfo> technicians,Service serEntity,List<ProductGroupList> proGroupList,int serProjectID,AsyncCallback<Integer> callback);
	void projectTechnicianList(long companyId, int contractCount,int serviceCount,String serviceStatus,AsyncCallback<ArrayList<EmployeeInfo>> callback);

}
