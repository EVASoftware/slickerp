package com.slicktechnologies.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.common.salesperson.TargetInformation;
@RemoteServiceRelativePath("salespersonachievementservice")
public interface SalesPersonAchievementService extends RemoteService{

ArrayList<TargetInformation> getAchievementDetails ( int employeeId, String financialYear,long companyId,String employeeeName);
	
	ArrayList<TargetInformation> getProductCategoryDetails(String financialYear,String productCategory,long companyId);
	
	ArrayList<TargetInformation> getemployeeRoleDetails(String financialYear,String employeeRole,long companyId);
}
