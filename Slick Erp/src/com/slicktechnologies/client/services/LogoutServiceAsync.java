package com.slicktechnologies.client.services;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.common.helperlayer.User;

public interface LogoutServiceAsync {
	void doLogout(Long companyId,String username,String remark,int sessionId,AsyncCallback<User>callback);
	void doLogoutonBrowserClose(Long companyId,String username,String remark,int sessionId,AsyncCallback<User>callback);
}
