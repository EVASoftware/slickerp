package com.slicktechnologies.client.services;


import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.common.helperlayer.LoggedIn;
import com.slicktechnologies.shared.common.helperlayer.User;

public interface SessionServiceAsync {
	void getUserSessionTimeout(String userName,int sessionId,long companyId,String loggedInUser, AsyncCallback<String> callback);
	void isSessionAlive(String sessionIdByServer,String user,int sessionCount,long companyId,AsyncCallback<Boolean> callback);
	void ping(String userName,int sessionId,boolean isPing,long companyId,String loggedInUser,String opeartion,int numberOfUser,AsyncCallback<Integer> callback);
	
	void getSessionArrayFromServer(User myUserEntity,AsyncCallback<Boolean> callback);
	void clearAllLoggedInUsers(ArrayList<LoggedIn> activeUsers,Long companyId,AsyncCallback<Void> asyncCallback);
	void getUserStatusInactiveAndRemoveSessionFromServer(ArrayList<LoggedIn> activeUsers,Long companyId, AsyncCallback<Boolean> callback);
	
	
	/**   rohan added this method for removing user from session array where as User is Inactive in LoggedIN Entity
	 * Date : 27/03/2017
	 * @param string2 
	 * @param string 
	 */
	void removeUserFromSessionArray(String userName, String employeeName, AsyncCallback<Boolean> callback);
}
