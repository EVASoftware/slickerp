package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.attendance.AttandanceInfo;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.attendance.EmployeeLeaveBalance;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProjectOvertime;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;

public interface DataMigrationTaskQueueServiceAsync {

	
	void saveRecords(long id, String entityName, AsyncCallback<ArrayList<Integer>>callback);
	

	void saveAttandance(long id, String entityName, AsyncCallback<ArrayList<AttandanceInfo>>callback);
	
	void validateAttandance(ArrayList<AttandanceInfo> attandanceList, AsyncCallback<ArrayList<AttandanceInfo>>callback);
	void updateAttandance(ArrayList<AttandanceInfo> attandanceList, AsyncCallback<ArrayList<AttandanceInfo>>callback);
	
	/**
	 * Date : 11-10-2018 By ANIL
	 */
	void updateAndOverrideAttendance(Attendance attendance,AsyncCallback<Attendance> callback);
	
	/**
	 * Date : 24-12-2018 BY ANIL
	 * @param companyId
	 * @return
	 */
//	void getEmployeeLeaveDetails(long companyId,AsyncCallback<HashMap<Integer,ArrayList<com.slicktechnologies.shared.common.attendance.EmployeeLeaveBalance>>> callBack);
//	void validateEmployeeLeaveDetails(HashMap<Integer,ArrayList<com.slicktechnologies.shared.common.attendance.EmployeeLeaveBalance>> empLeaveMap,AsyncCallback<HashMap<Integer,ArrayList<com.slicktechnologies.shared.common.attendance.EmployeeLeaveBalance>>> callBack);
//	void updateLeaveBalance(HashMap<Integer,ArrayList<com.slicktechnologies.shared.common.attendance.EmployeeLeaveBalance>> empLeaveMap,AsyncCallback<String> callback);
//	
	
	
	void getEmployeeLeaveDetails(long companyId,AsyncCallback<ArrayList<EmployeeLeaveBalance>> callBack);
	void validateEmployeeLeaveDetails(ArrayList<EmployeeLeaveBalance> empLeaveMap,AsyncCallback<ArrayList<EmployeeLeaveBalance>> callBack);
	void updateLeaveBalance(ArrayList<EmployeeLeaveBalance> empLeaveMap,AsyncCallback<String> callback);


	void readStockUpdateExcel(String entityNameStockUpdate, Long companyId,
			String bloburl, AsyncCallback<ArrayList<String>> asyncCallback);


	void reactOnStockUpdateValidate(ArrayList<String> stockupdateExcel,
			Long companyId, AsyncCallback<ArrayList<String>> asyncCallback);


	void stockUpdateExcelUpload(String string, Long companyId,
			AsyncCallback<String> asyncCallback);
	
	/**
	 * End
	 */
	
	/**
	 * @author Vijay
	 * Des :- Attendance upload all validation part at client side
	 */
	void validateAttandancepart2(ArrayList<AttandanceInfo> attandanceList,AsyncCallback<ArrayList<SuperModel>>callback);
	void validateAttandancepart3(ArrayList<AttandanceInfo> attandanceList,ArrayList<Integer> empList, ArrayList<Integer> projectidlist, AsyncCallback<ArrayList<SuperModel>>callback);

	void loadLeaveBalanceEntity(ArrayList<Integer> empList, long companyId, AsyncCallback<ArrayList<LeaveBalance>>callback);
	void loadEmployeeInfoEntity(ArrayList<Integer> empList, long companyId,AsyncCallback<ArrayList<EmployeeInfo>>callback);
	void loadExistingAttendance(ArrayList<AttandanceInfo> attandanceList, ArrayList<Integer> empList,ArrayList<String> project, AsyncCallback<ArrayList<Attendance>>callback);

	void loadHRprojectEntity(ArrayList<String> projList, long companyId, AsyncCallback<ArrayList<HrProject>>callback);
	void loadHRprojectOvertimeEntity(ArrayList<AttandanceInfo> attandanceList,ArrayList<Integer> empList, ArrayList<Integer> projectidlist, AsyncCallback<ArrayList<HrProjectOvertime>>callback);

}
