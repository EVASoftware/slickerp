package com.slicktechnologies.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

@RemoteServiceRelativePath("autoinvoiceservice")
public interface AutoInvoiceService extends RemoteService {

//	public String createTaxInvoice(BillingDocument billingDoc , String invoiceType,String invoiceCategory, String invoiceType2, String invoiceGruop,String approverName,String userName);

	public String createTaxInvoice(BillingDocument billingDoc,Invoice invoice,  String loggedInUser, boolean autoinvoiceflag, ArrayList<Integer> billIdList, ArrayList<Integer> billOrderIdList);

	public String updateServiceBranchinServies(int contractId, long companyId);
}
