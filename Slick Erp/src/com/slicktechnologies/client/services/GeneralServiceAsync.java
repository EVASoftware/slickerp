package com.slicktechnologies.client.services;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.customerlog.MaterialRequired;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
public interface GeneralServiceAsync {

	void createBillingDocs(Contract model,AsyncCallback<ArrayList<Integer>> callback);
	
	/**
	 * Date 14 july 2017 added by vijay for Quick Sales submit Implementation
	 */
	
	void quickSalesSumbit(SalesOrder model, AsyncCallback<String> callback);
	/**
	 * ends here
	 */
	
	/**
	 * Date 05-07-2017 added by vijay for Return Expense amount from petty Cash
	 */
	
	void returnExpenseAmount( MultipleExpenseMngt multiexpenseModel, double expenseReturnAmt,String loginUser, AsyncCallback<MultipleExpenseMngt> callback);

	/**
	 * Date 18-08-2017 added by vijay for Quick Purchase submit Implementation
	 */
	void quickPurchaseOrderSubmit(PurchaseOrder model, AsyncCallback<String> callback);
	
	
	/**
	 * Date 05-0ct-2017 added by vijay for technician scheduling 
	 * @param materialInfo 
	 * @param technicianToollist 
	 * @param technicianlist 
	 * @param string 
	 * @param date 
	 */
	void TechnicianSchedulingPlan(Service model, ArrayList<EmployeeInfo> technicianlist, ArrayList<CompanyAsset> technicianToollist, ArrayList<ProductGroupList> materialInfo, Date serviceDate, String serviceTime,int invoiceId,String invoiceDate,String TechnicianName,int projectId, AsyncCallback<Integer> callback);
	
	/**
	 * Date 06 oct 2017 added by vijay selected service saving with one unique number
	 * @param companyId 
	 */
	void saveSelectedService(ArrayList<Service> servicelist, long companyId, AsyncCallback<String> callback);
	
	/**
	 * Date 25 oct 2017 added by vijay mark Complete service Status changing and project status changing and project mark complete 
	 */
	void saveServiceProjectStatus(Service service, AsyncCallback<String> callback);
	
	/**
	 * Date 08-11-2017 added by Komal 
	 */
	void updateLeadQuotationFollowUpDate(long companyId, AsyncCallback<String> callback);
	
	/**
	 * Date 22-12-2017 added by vijay for Updating all payment document with TDS flag as index 
	 */
	void updateAllPaymentDocumentWithTDSflagIndex(long companyId, AsyncCallback<Integer> callback);
	
//	date 01-02-2018 added by komal for nbhc  closing stock
	void getProductInventoryViewDetails(long companyId , String branch ,String warehouse ,AsyncCallback<ArrayList<ProductInventoryViewDetails>> callback );

//	date 02-02-2018 added by komal for nbhc purchase requisition 
	void getPurchaseRequisitionDetails(long companyId ,String bussinessType ,ArrayList<PurchaseRequisition> prList , AsyncCallback<ArrayList<PurchaseRequisition>> callback);

	/** date 19.4.2018 added by komal to create contrat(for pestMorterm )**/
	void createAndGetContractDetails(long companyId , Complain complain ,String user , AsyncCallback<Contract> callback);
	void createServiceFromComplain(long companyId , Complain complain , String user  ,int contractId ,AsyncCallback<Service> callback);
	 /** date 05/12/2017 added by komal to  save customer log details **/
		void saveCustomerCallLogDetails(CustomerLogDetails customerLog ,long companyId,AsyncCallback<CustomerLogDetails> callback);
		void createBilling(CustomerLogDetails log , int customerLog  ,String approverName ,ArrayList<MaterialRequired> serviceList,long companyId,AsyncCallback<String> callback);
		void sendDefective(ArrayList<MaterialMovementNote> selectedList , Long companyId ,String approverName , AsyncCallback<String> callback);
		void getGRNForDefective(Integer grnId , Long companyId , AsyncCallback<GRN> callback);
		void saveAndApproveDocument(SuperModel m,String customerId, String customerName, String documentName, AsyncCallback<String> callback);

	/**
	 * Date 09-05-2018
	 * Developer : Vijay
	 * Des :- Payment document update with follow up date for status created only because we load created status dashboard only
	 */
	void updatePaymentDocsWithFollowupDate(Long companyId, AsyncCallback<Integer> callback);
	/**
	 * ends here
	 */	
	/** date 25 .4.2018 added by komal for fumigation invoice generation **/
	void generateInvoiceForFumigation(long companyId , Fumigation fumigation ,AsyncCallback<Invoice> callback);

	/*** Date 01-06-2018 By vijay for Updating service address from contract only for non multilocation services ***/
	void updatedServiceAddress(Contract contract, boolean customerbranchservicesflag, AsyncCallback<Boolean> callback);

	/**
	 * Date 29 june 2018 added by Apeksha for sendPushNotificationToTechnician
	 */
	public void sendPushNotificationToTechnician(Service service, long companyId ,AsyncCallback<String> callback);
	
	/** Date 07-07-2018 By Vijay for Billiing Documents Cancellation ***/
	void MultipleBillingDocsCancellation(ArrayList<BillingDocument> billinglist,String remark, AsyncCallback<Boolean> callback);

	/****** Date 26-07-2018 By Vijay For Pepcopp Invoice edited by user sending email **************/
	void sendInvoiceEditedEmail(int invoiceId,String branch,String loggedInUser, double oldNetPayable,double newNetPayable, Date oldInvoiceDate, Date newInvoiceDate,long companyId,String customerName, AsyncCallback<Boolean>callback );
	
	/**
	 * Date 10-09-2018 by Vijay
	 * Des :- customer call log Product inventory master Transfer to warehouse defined or not checking validation
	 */
	void getProductInventoryDetails(ArrayList<Integer> productIdlist,long companyId, AsyncCallback<ArrayList<SuperModel>> callback);
	/**
	 * ends here
	 */
	
	/**
	 * Date : 21-09-2018 Merged by ANIL
	 */
	
	void setAttendance(long companyId, String branch,String month,int empId, String projectName,  AsyncCallback<ArrayList<Attendance>> callback);
	/** date 17.7.2018 added by komal for payroll taskqueue**/
	void paySlipAllocationTaskQueue(ArrayList<EmployeeInfo> empinfoarray,String payRollPeriod,Date fromDate,Date toDate, String projectName, AsyncCallback<Void> callback ); 
	
	void updateEmployeeInfoStatus(long companyId,String action,int empId,String payrollPeriod,String project,String branch,String roleName,AsyncCallback<String> callback);

	/**
	 * End
	 */
	/** Date 13.12.2018 added by komal for update accounting interface **/
	void updateAccountingInterface(SuperModel model , AsyncCallback<String> callback);
	
	/**
	 * Date 20-12-2018 By Vijay For Updating PR Status for NBHC :- Inventory Management 
	 */
	void updatePRStatus(int purchaseReqId, AsyncCallback<String> callback);
	
	
	/**
	 * Date 10-01-2019 By Vijay for Updated GRN for products to set prodinfo
	 */
	void updateGRNDocs(long companyId, AsyncCallback<String> callback);
	
	/**
	 * Date 15-01-2019 by Vijay for NBHC Inventory Management updating PR Doc 
	 */
	
	void updatePRDoc(Approvals approval,boolean documentflag, int vendorId, double vendorPrice, AsyncCallback<String> callback);
	
	/**
	 * Updated By: Viraj
	 * Date: 22-01-2019
	 * Description: To return list of billing, vendorInvoice and payment document in purchase order
	 */
	void getCancellingDocumentPurchaseOrder(Long companyId , int orderId , AsyncCallback<ArrayList<CancelContract>> callback);
	
	void savePOCancellationData(PurchaseOrder po,String remark,String loginUser ,ArrayList<CancelContract> cancelContractList ,AsyncCallback<Void> callback);
	/**
	 * Date 28-01-2019 By Vijay NBHC Inventory Management Updating product inventory with Warehouse type
	 * to identify warehouse and cluster warehouse
	 */
	void updateProductInventoryWithWarehouseType(long companyId, AsyncCallback<String> callback);
	
	/**
	 * Date 09-03-2019 By Vijay for NBHC if service is not completed then tax invoice not allowed
	 * with configuraion values
	 */
	void validateDueService(long companyid,Date startDate,Date dueDate,ArrayList<String> branchlist, ArrayList<Integer> listcontractId, AsyncCallback<String> calback);

	/**
	 * Date 10-05-2019 by Vijay
	 * Des :- creating Storage location and Storage Bin for the existing warehouse
	 */
	void createStoraLocationBin(long companyId,AsyncCallback<String> callback);
	/**
	 * Date 20-05-2019 by Vijay
	 * Des :- updating contract Discontinue Date with discontinue approval date
	 */
	void updateContractDiscontinued(long companyId,AsyncCallback<String> callback);
	
		
	/**
	 * @author Anil , Date : 29-05-2019
	 * deleteExtraServicesAgainstContract
	 */
	void deleteExtraServicesAgainstContract(Contract contract,int productId, AsyncCallback<String> calback);
	
	/**
	 * Updated By: Viraj
	 * Date: 06-04-2019
	 * Description: To fetch all the product inventory list data of the customer
	 * @param customerId
	 * @param callback
	 */
	void fetchCustomerProductViewList(long companyId, int customerId,
			AsyncCallback<ArrayList<ProductInventoryViewDetails>> callback);
	/** Ends **/
	
	/**
	 * Updated By: Viraj
	 * Date: 16-05-2019
	 * Description: To fetch sales register information from master franchise or franchise depending customer franchise type
	 */
	void fetchSalesRegisterList(long companyId, int customerId, Date fromDate,
			Date toDate, AsyncCallback<String> callback);
			
				/** date 10.4.2019 added by komal to update services within specific duration **/
	void updateServices(long companyId,Date startDate,Date endDate,int customerId,String branch,AsyncCallback<String> callback);
	/** date 27.5.2019 added by komal for combined material list **/
	void getCombinedMaterialList(long companyId , ArrayList<Integer> serviceIdList,ArrayList<Integer> contractIdList, AsyncCallback<ArrayList<ProductGroupList>> callback) ;
	void createMMNForTechnician(long companyId , ArrayList<ProductGroupList> productList,String user ,ArrayList<Integer> serviceIdList, AsyncCallback<String> callback);

	/**
	 * @author Anil , Date : 13-06-2019
	 */
	public void updateProductCode(long companyId,AsyncCallback<Void> callback);
	
	
	/*** Date 26-07-2019 by Vijay for NBHC CCPM Approved Contract Cancelled Services ****/
	void getCancelledServiceOfApprovedContract(long companyId, Date fromDate, Date toDate, ArrayList<String> brnachlist, AsyncCallback<String> callback);

	/**
	 * Date 22-07-2019 by Vijay
	 * Des :- for NBHC CCPM Contract Upload
	 */
	void contractUploadService(String entityName, long companyId,AsyncCallback<String> callback);
	
	/**
	 * Date 22-05-2019 by Vijay For Complaint service creation
	 */
	void createComplaintService(Complain complain, int complaintId, AsyncCallback<String> callback);
	
	/**
	 * Date 12-08-2019 by VIjay
	 * Des :- Complaint Mark Completion
	 */
	void markCompleteComplaint(long companid, int ticketid, AsyncCallback<String> callback);
	
	/**
	 * Date 27-08-2019 by Vijay 
	 * Des :- NBHC CCPM Rate contracts Service value updation for Contract PNL Report 
	 */
	void updateServiceValueForRateContract(Date fromDate, Date toDate,String branch, long companyId,AsyncCallback<String> callback);
	/**
	 * Date 02-09-2019 by Vijay
	 * Des :- NBHC Inventory Management Lock Seal Deviation Report
	 */
	void getLockSealDeviationReport(long companyId, Date fromDate, Date toDate, ArrayList<String> brnachlist,AsyncCallback<String> callback);




	
	/**
	 * @author Anil , Date : 16-09-2019
	 * moving invoice creation part from client side to server side
	 * multiple invoice created against single bill
	 */
	
	void createTaxInvoice(BillingDocument billingDoc, String invoiceType, String invoiceCategory, String invoiceType2, String invoiceGruop,String approverName,String userName,boolean approveInvoiceCreatePaymentDocFlag, AsyncCallback<String> callback);
		 /* @author Amol , Date : 19-09-2019
	 */
	public void updateInvoices(long companyId,String fromDate,String toDate,AsyncCallback<Void> callback);


    void AssesmentMarkCloseAndSendEmail(AssesmentReport assesmentreport, AsyncCallback<String> callback);


	void sendSrMail(long companyId , ArrayList<Integer> serviceIdList,AsyncCallback<String> callback);
	/** date 19.09.2019 added by komal to validate technician warehouse details and inventory checking **/
	void validateTechnicianWarehouse(ArrayList<Service> serviceList,List<ProductGroupList> groupList, AsyncCallback<ArrayList<String>> callback);
	void validateTechnicianWarehouseQuantity(long companyId , ArrayList<Service> serviceList, List<ProductGroupList> groupList ,AsyncCallback<ArrayList<String>> callback );



	
		/**
	 * @author Anil , Date : 27-09-2019
	 * createAttendance
	 */
	void createAttendance(ArrayList<Attendance> attendanceList,AsyncCallback<String> callback);
	void updatedServiceAddress(SalesOrder salesorder, boolean customerbranchservicesflag, AsyncCallback<Boolean> callback);
    public void updateCnc(long companyId,AsyncCallback<Void> callback);
    
    /**
	 * @author Anil , Date : 19-10-2019
	 */
	public void updateDuplicateServices(long companyId,Date fromDate,Date toDate,int serviceId,Date day1,Date day2,int contractId,int fromServiceId,int toServiceId,AsyncCallback<Void> callback);

	/**
	 * @author Anil , Date : 26-11-2019
	 */
	public void updateDuplicateCustomerBranch(long companyId,int custBranchId,AsyncCallback<Void> callback);

	/**
	 *@author Anil
	 *@since 17-06-2020
	 */
	public void updateComponentDetailsOnServices(Contract contract,AsyncCallback<Void> callback);
	
	/**
	 * @author Anil @since 17-02-2021
	 * This RPC is use to check Internet connection 
	 */
	public void ping(AsyncCallback<Void> callback);
	
	
	/**
	 * @author Anil @since 17-12-2021
	 * Schedule Assessment for prominent 
	 * raised by Nitin Sir
	 */
	public void scheduleAssessment(AssesmentReport report,Date serviceDate,String time,String description,AsyncCallback<AssesmentReport> callback);
	
	/**
	 * @author Sheetal @Since 17-05-2022
	 * This RPC is used to inactive the user after few attempts of incorrect password 
	 */
	public void inactiveUser(String userName, long companyId, AsyncCallback<Void> callback);
	
	public void getEmployeeinfolist(ArrayList<Integer> empidlist, long companyId, AsyncCallback<ArrayList<EmployeeInfo>> callback);

	public void updateFrequency(long companyId,Date contractStartDate,AsyncCallback<String> callback);//Ashwini Patil Date:10-07-2023 for orion

	public void checkIfLicensePresent(long companyId, String licenseCode,AsyncCallback<Boolean> callback); //Ashwini Patil Date:3-08-2023 for audit license

	public void updateEmployeeIdInUser(long companyId,AsyncCallback<String> callback); //Ashwini Patil Date:22-08-2023 

	public void deleteDuplicateLeaves(long companyId,Date fromDate,Date toDate, AsyncCallback<Void> callback); //Ashwini Patil Date:23-08-2023 for sunrise issue while payroll- number of leave days not matching. duplicate leave entries found in employee leave history

	public void rescheduleForPeriod(long companyId,Date fromDate,Date toDate,ArrayList<Service> serviceList,AsyncCallback<Void> callback);//Ashwini Patil Date:6-10-2023 for Orion- as there maximum services are falling on one  day of month and its difficult to rescedule and print the services report(dockets) from customer service list
}
