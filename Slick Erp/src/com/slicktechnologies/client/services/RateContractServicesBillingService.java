package com.slicktechnologies.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.client.views.ratecontractproductpopup.ProductInfo;
import com.slicktechnologies.shared.Contract;

@RemoteServiceRelativePath("RateContractServicesBilling")
public interface RateContractServicesBillingService extends RemoteService{

	ArrayList<String> CreateServiceAndBilling(Contract contractObj,ArrayList<ProductInfo> productlist,boolean validate);
	ArrayList<String> CreateServiceAndBillingForBranches(Contract contractObj,ArrayList<ProductInfo> productlist,boolean validate);	
	ArrayList<String> updateEmployees(long companyId);
}
