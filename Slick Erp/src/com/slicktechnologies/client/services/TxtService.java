package com.slicktechnologies.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;

@RemoteServiceRelativePath("txtservice")
public interface TxtService extends RemoteService {
	public void setPaySliplist(ArrayList<PaySlip> paySlipList);
}
