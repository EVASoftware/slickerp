package com.slicktechnologies.client.services;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface IPAddressServiceAsync {
	
	void retrieveClientIP(long companyId,AsyncCallback<String> callback);
	void retrieveUserIP(long companyId,AsyncCallback<String> callback);
}
