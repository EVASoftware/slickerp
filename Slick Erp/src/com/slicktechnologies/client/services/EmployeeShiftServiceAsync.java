package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.EmployeeShift;

public interface EmployeeShiftServiceAsync
{
	public void getEmployeeShifts(Date currentDate,AsyncCallback<ArrayList<EmployeeShift>> callback);
}
