package com.slicktechnologies.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

@RemoteServiceRelativePath("documentuploadservice")
public interface DocumentUploadService extends RemoteService{

	/**
	 * @author Vijay Chougule
	 * Des :- 18-10-2019 for NBHC CCPM Contract Upload
	 */
	ArrayList<String> readContractUploadExcelFile(String operationName,long companyId);
	ArrayList<String> reactonValidateContractUpload(ArrayList<String> contractExcellist, long companyId);
	String reactonContractUpload(String blobkey, long companyId);
	
	/**
	 * Date 22-10-2019 by vijay
	 * Des :- NBHC Auto submit Billing sending request for approval
	 */
	String sendApprovalRequest(Invoice invoiceEntity,String userName,int invoiceId);
	
	/**
	 * Date 21-11-2019 by Vijay
	 * Des :- NBHC Inventory Management Lock seal GRN MMN
	 * Reading Excel File
	 */
	ArrayList<String> readexcelFile();
	ArrayList<String> uploadGRN(long companyId,ArrayList<String> excelData);
	String uploadMMN(long companyId,ArrayList<String>excelData);
	/**
	 * ends here
	 */

	/**
	 * Date 27-11-2019
	 * Des :- NBHC Lock Seal IM Stock update directly
	 */
	String uploadLockSealStockUpdate(long companyId);
	/**
	 * ends here
	 */
	
	/**
	 * Date 28-11-2019
	 */
	String updateLockSealSerialNumberMaster(long companyId,String serialNumber);

      ArrayList<String> reactOnPurchaseRequisitionExcelValidation(
			boolean masterPriceFlag, ArrayList<String> excelData, Long companyId);
	String reactOnPurchaseRequisitionExcelUpload(boolean masterPriceFlag,
			String string, Long companyId);
     ArrayList<String> reactOnPurchaseRequisitionExcelRead(String entityName,
			Long companyId, String blobkeyURL);
     
     /**
 	 * @author Vijay Chougule Date 10-07-2020
 	 * Des :- To Update inhouse fumigation services properly in fumigation report and if any services missing it also create the same
 	 */
 	String updateInhouseFumigationServices(long companyId);
 	/**
 	 * @author Vijay Date :- 13-10-2020
 	 * Des :- Payment Upload
 	 */
 	ArrayList<CustomerPayment> validatePaymentUploader(long company);
 	String uploadPaymentDetails(long comapnyId, String taskName);
 	
 	ArrayList<CustomerPayment> validateTallyPaymentUploader(long companyId);
 	String uploadTallyPaymentDetails(long comapnyId, String taskName);

 	ArrayList<Service> validateServiceCancellationUpload(long companyId, String loggedInUser);
	String customerBranchUpload(long companyId);

	Integer validateCustomerBranchStatusUpdation(long company,boolean cancelServicesFlag);//Ashwini Patil Date:3-10-2023
	String updateCustomerBranchStatus(long comapnyId,boolean cancelServicesFlag);
	
	ArrayList<String> UpdateZohoCustomerId();
	ArrayList<String> ReverseServiceCompletionAndRemoveDuplicate();
}
