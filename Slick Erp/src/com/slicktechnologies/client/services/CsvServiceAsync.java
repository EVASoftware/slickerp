package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.views.approval.MaterialInfo;
import com.slicktechnologies.client.views.contractrenewal.RenewalResult;
import com.slicktechnologies.client.views.customerDetails.ContractTableValueProxy;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.client.views.inventory.productinventorytransaction.ClosingReport;
import com.slicktechnologies.client.views.salesperformance.SalesPersonValue;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.CreditNote;
import com.slicktechnologies.shared.EmployeeAsset;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.ServiceInvoiceDetails;
import com.slicktechnologies.shared.TallyInterfaceBean;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.BillUnbilledAr;
import com.slicktechnologies.shared.common.CompanyAssetListArticleInformation;
import com.slicktechnologies.shared.common.PettyCashTransaction;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.RoleDefinition;
import com.slicktechnologies.shared.common.android.RegisterDevice;
import com.slicktechnologies.shared.common.arrears.ArrearsDetails;
import com.slicktechnologies.shared.common.attendance.AttandanceError;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.attendance.AttendanceBean;
import com.slicktechnologies.shared.common.billofproductmaterial.BillOfProductMaterial;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorPriceListDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cancelsummary.CancelSummary;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.documenthistorydetails.DocumentHistory;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.LoggedIn;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.VoluntaryPf;
import com.slicktechnologies.shared.common.humanresourcelayer.VoluntaryPfHistory;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Loan;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.EmployeeLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveApplication;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CompanyPayrollRecord;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.EmployeeShift;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.EmployeeOvertime;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReport;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.inventory.ExpenseCost;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.inventory.LabourCost;
import com.slicktechnologies.shared.common.inventory.MaterialConsumptionReport;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.inventory.PhysicalInventoryMaintaince;
import com.slicktechnologies.shared.common.inventory.PhysicalInventoryProducts;
import com.slicktechnologies.shared.common.inventory.ProductInventoryTransaction;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.paymentmodelayer.vendorpayment.VendorPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesperson.SalesPersonTargets;
import com.slicktechnologies.shared.common.salesperson.TargetInformation;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.Liaison;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ToolGroup;
import com.slicktechnologies.shared.common.smshistory.SmsHistory;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;
import com.slicktechnologies.shared.common.workorder.WorkOrder;



public interface CsvServiceAsync {
	public void setcustomerlist(ArrayList<Customer> array,AsyncCallback<Void> callback);
	public void setquotationlist(ArrayList<Quotation> qarray,AsyncCallback<Void> callback);
	public void setleadslist(ArrayList<Lead> larray,AsyncCallback<Void> callback);
	public void setexpenselist(ArrayList<Expense> earray,AsyncCallback<Void> callback);
	public void setservice(ArrayList<Service> earray,AsyncCallback<Void> callback);

	public void setadvancelist(ArrayList<Loan> advarray,AsyncCallback<Void> callback);
	public void setCompanylist(ArrayList<Company> custarray,AsyncCallback<Void> asyncCallback);
	
	public void setcontractlist(ArrayList<Contract> custarray,AsyncCallback<Void> asyncCallback);
	public void setleaveapplicationlist(ArrayList<LeaveApplication> leavearray,AsyncCallback<Void> callback);
	public void setEmployeeShiftslist(ArrayList<EmployeeShift> custarray,AsyncCallback<Void> callback);

	public void setctclist(ArrayList<CTC> ctcarray,AsyncCallback<Void> callback);

	public void setleavebalancelist(ArrayList<LeaveBalance> leavebalancearray,AsyncCallback<Void> callback);
	
	public void setVendor(ArrayList<Vendor> custarray,AsyncCallback<Void> callback);
	void setEmployeelist(ArrayList<Employee> custarray,AsyncCallback<Void> callback);
	public void setpettycashlist(ArrayList<PettyCash> pettycasharray,AsyncCallback<Void> callback);
	public void setconbillinglist(ArrayList<BillingDocument> conbillingarray,AsyncCallback<Void> callback);
	public void setinvoicelist(ArrayList<Invoice> invoicingarray,AsyncCallback<Void> callback);
	public void setpaymentlist(ArrayList<CustomerPayment> paymentarray,AsyncCallback<Void> callback);

	public void setToolSetlist(ArrayList<ToolGroup> toolsetarray,AsyncCallback<Void> callback);

	public void setToollist(ArrayList<CompanyAsset> toolsetarray,AsyncCallback<Void> callback);
	public void setclientsideassetlist(ArrayList<ClientSideAsset> toolsetarray,AsyncCallback<Void> callback);

	public void setLeaveCalendar(ArrayList<Calendar> clientsideaseetarray,AsyncCallback<Void> callback);
	
	public void setVendorProduct(ArrayList<PriceList> VendorProductarray,AsyncCallback<Void> callback);
	public void setRequestForQuotation(	ArrayList<RequsestForQuotation> requestForQuotationarray,AsyncCallback<Void> callback);
	public void setLiaison(ArrayList<Liaison> liaisonarray,AsyncCallback<Void> callback);
	public void setSalesOrderList(ArrayList<SalesOrder> qarray,AsyncCallback<Void> callback);
	
	public void setsalesquotationlist(ArrayList<SalesQuotation> salesqarray,AsyncCallback<Void> callback);
	public void setdeliverynotelist(ArrayList<DeliveryNote> notearray,AsyncCallback<Void> callback);
	public void setLOI(ArrayList<LetterOfIntent> loiarray, AsyncCallback<Void> callback);
	public void setPO(ArrayList<PurchaseOrder> POarray, AsyncCallback<Void> callback);
	public void setPR(ArrayList<PurchaseRequisition> PRarray,AsyncCallback<Void> callback);
	public void setGRN(ArrayList<GRN> GRNarray, AsyncCallback<Void> callback);
	public void setMaterialRequestNote(ArrayList<MaterialRequestNote> mrnArrayList, AsyncCallback<Void> callback);
	public void setMaterialIssueNote(ArrayList<MaterialIssueNote> minArray,AsyncCallback<Void> asyncCallback);
	public void setProInvTranList(ArrayList<ProductInventoryTransaction> prodInvTranList,AsyncCallback<Void> asyncCallback);
	public void setMaterialMovementNote(ArrayList<MaterialMovementNote> mmnLlist,AsyncCallback<Void> asyncCallback);
	public void setProductInventoryView(ArrayList<ProductInventoryView> pivlist,AsyncCallback<Void> asyncCallback);
	public void setCompanyPaymentDetails(ArrayList<CompanyPayment> comppayarray, AsyncCallback<Void> callback);
	public void setTimeSheetList(ArrayList<TimeReport> timereport,AsyncCallback<Void> callback);
	public void setInteractionDetails(ArrayList<InteractionType> interactionarray,AsyncCallback<Void> callback);
	public void setInterfaceTallyDetails(ArrayList<AccountingInterface> interfacearray,AsyncCallback<Void> callback);
	public void setProductGrpList(ArrayList<ProductGroupList> Parray,AsyncCallback<Void> callback);
	public void setBillOfMaterial(ArrayList<BillOfMaterial>Barray,AsyncCallback<Void> callback);
	public void setContactPersonDetails(ArrayList<ContactPersonIdentification> contactpersonarray,AsyncCallback<Void> callback);
	public void setVendorPaymentDetails(ArrayList<VendorPayment> vendorpaymentarray,AsyncCallback<Void> callback);
	public void setServiceProductList(ArrayList<ServiceProduct> sparray,AsyncCallback<Void> callback);
	public void setItemProductList(ArrayList<ItemProduct> sparray,AsyncCallback<Void> callback);
	public void setApprovalsList(ArrayList<Approvals> apprarray,AsyncCallback<Void> callback);
	public void setSalesSecurityDeposit(ArrayList<SalesQuotation> salesdepositarray,AsyncCallback<Void> callback);
	public void setServiceSecurityDeposit(ArrayList<Quotation> servicedepositarray,AsyncCallback<Void> callback);
	public void setDocumentHistoryList(ArrayList<DocumentHistory> docarray,AsyncCallback<Void> callback);
	public void setInventoryListDetails(ArrayList<ProductInventoryViewDetails> invlistarray,AsyncCallback<Void> callback);
	public void setLoggedInHistoryDetails(ArrayList<LoggedIn> loginhistoryarray,AsyncCallback<Void>callback);
	public void setCancelSummaryDetails(ArrayList<CancelSummary> cancelSummaryarray,AsyncCallback<Void>callback);
	public void setInspectionDetails(ArrayList<Inspection> inspectionList,AsyncCallback<Void>callback);
	public void setBillOfProductMaterial(ArrayList<BillOfProductMaterial> billProdArray,AsyncCallback<Void> callback);
	public void setVendorProductList(ArrayList<VendorPriceListDetails> venPriceList,AsyncCallback<Void>callback);
	public void setWorkOrderDetails(ArrayList<WorkOrder>woarray,AsyncCallback<Void> callback);
	public void setCustomerBranchDetails(ArrayList<CustomerBranchDetails> customerbrancharray,AsyncCallback<Void> callback);
	public void setPhysicalInventoryList(ArrayList<PhysicalInventoryMaintaince> inventoryreportList,AsyncCallback<Void>callback);
	public void setStockAlertList(ArrayList<ProductInventoryViewDetails> stockalertList,AsyncCallback<Void>callback);
	public void setMaterialConsumptionReport(ArrayList<MaterialConsumptionReport> consumptionReport , ArrayList<LabourCost> labourcostlist, ArrayList<ExpenseCost> expensecostlist,ArrayList<String> summarylist,double administrativecost,AsyncCallback<Void> callback);
	public void setSmsHistoryList(ArrayList<SmsHistory> smsarray,AsyncCallback<Void> callback);
	public void setFumigationReport(ArrayList<Fumigation> fumigationReport,AsyncCallback<Void> callback);
	public void setEmpAdditionDetails(ArrayList<EmployeeAdditionalDetails>woarray,AsyncCallback<Void> callback);
	public void setPaySliplist(ArrayList<PaySlip> paySlipList,AsyncCallback<Void> callback);
	public void setFumigationAuslist(ArrayList<Fumigation> fumAusList,AsyncCallback<Void> callback);
	public void setComplainlist(ArrayList<Complain> paySlipList,AsyncCallback<Void> callback);
	public void setMateriallist(ArrayList<MaterialInfo> materailList,AsyncCallback<Void> callback);
	public void setScheduledServicelist(ArrayList<Service> scheduledServiceList,AsyncCallback<Void> callback);
	public void setCustomerHelpDesk(ArrayList<ContractTableValueProxy> helpdesklist,AsyncCallback<Void> callback);
	public void setContractRenewallist(ArrayList<RenewalResult> contractRenewalList,AsyncCallback<Void> callback);


	public void setPettyCashTrasactionList(ArrayList<PettyCashTransaction> pettycashtransactionlist,AsyncCallback<Void> callback);

	public void setSalesPersonPerformanceReport(ArrayList<String> personinfo,ArrayList<String> summarylist,ArrayList<SalesPersonValue> leadReport , ArrayList<SalesPersonValue> quotationReport, ArrayList<SalesPersonValue> contractReport, AsyncCallback<Void> callback);

	public void setSalesPersonTargetPerformanceReport(ArrayList<TargetInformation> salespersonTargetperformance,String financialyear,AsyncCallback<Void> callback);
	public void setCompanyAssetList(ArrayList<CompanyAssetListArticleInformation> companyAssetlist, AsyncCallback<Void> callback);
	public void setTechnicianInfoReport(ArrayList<String> personinfolist,ArrayList<Service> serviceArray,ArrayList<String> summarylist, AsyncCallback<Void> callback);
//	public void setAssessmentDetails(ArrayList<AssesmentReport> assessmentDetails,AsyncCallback<Void> asyncCallback);

	public void setMultiplExpenselist(ArrayList<MultipleExpenseMngt> expensearray,AsyncCallback<Void> callback);

	/**
	  * For Service Po Download
	  * Date : 14-10-2016 By Anil
	  * Release : 30 Sept 2016 
	  * Project: Purchase Modification(NBHC)
	  */
	public void setServicePoList(ArrayList<ServicePo> serPoList, AsyncCallback<Void> callback);
	
	/**
	  * To get the asset upload format
	  * Date : 22-10-2016 By Anil
	  * Release : 30 Sept 2016 
	  * Project: Purchase Modification(NBHC)
	  */
	public void getAssetUploadFormat(SuperModel model, AsyncCallback<Void> callback);
	
	
	
	/**
	  * For Warehouse,Storage Location,Storage Bin Master Download
	  * Date : 23-11-2016 By Anil
	  * Project: Purchase Modification(NBHC) DEADSTOCK
	  */
	public void setWarehouseList(ArrayList<WareHouse> whList, AsyncCallback<Void> callback);
	public void setStorageLocationList(ArrayList<StorageLocation> slList, AsyncCallback<Void> callback);
	public void setStorageBinList(ArrayList<Storagebin> sbList, AsyncCallback<Void> callback);
	
	/**
	 * Date : 14-02-2017 By ANIL
	 * For Role Definition Download
	 */
	public void setRoleDefinitionList(ArrayList<RoleDefinition> whList, AsyncCallback<Void> callback);
	
	/**
	 * Date 2-03-2017 added by vijay
	 * for User Download
	 */
	public void setUserList(ArrayList<User> userarray, AsyncCallback<Void> callback);
	
	/**
	 * Date : 03-02-2017 By ANIL
	 * For Closing Stock Report
	 */
	public void setClosingStockReport(ArrayList<ClosingReport> closingReport, AsyncCallback<Void> callback);
	
	/**
	 * Date 03/09/2017 by jayshree 
	 * 
	 */
	public void setSalesPersonTargetReport(ArrayList<SalesPersonTargets> SalesPersonarray, AsyncCallback<Void> callback);
	/**
	 * Date : 14-11-2017 BY komal
	 */
	public void setSchedulingList(String str, AsyncCallback<Void> callback);
	
	/**
	 * Nidhi added this
	 * Date : 18 Nov 2017
	 * Description : MIN and Contract upload error list
	 * @param errorlist
	 * @param callback
	 */
	public void setMinUploadError(ArrayList<String> errorlist, AsyncCallback<Void> callback);
	
	/** date 29/01/2018 added by komal for hvac call log download **/
	public void setCustomerCallLog(ArrayList<CustomerLogDetails> logList , AsyncCallback<Void> callback);
	
	/** date 28.3.2018 added by komal for new tally interface download **/
	public void setTallyInterfaceData(ArrayList<TallyInterfaceBean> tallyInterfaceList , AsyncCallback<Void> callback);

	/** date 9.7.2018 added by komal for vendor invoice download **/
//	public void setVendorinvoicelist(ArrayList<VendorInvoice> invoicingarray,AsyncCallback<Void> callback);
	/**
	 * Add by jayshree 5-7-2018
	 * @param paysliparray
	 * @param asyncCallback
	 */
	public void setPayslip(ArrayList<PaySlip> paysliparray,AsyncCallback<Void> asyncCallback);
	
	/** DATE 11.7.2018 added by komal for attendance download*/
	public void setAttendance(ArrayList<AttendanceBean> attendanceList , AsyncCallback<Void> callba);
	
	/**
	 * Date : 16-07-2018 BY ANIL
	 *
	 */
	public void setAttandanceError(ArrayList<AttandanceError> attandanceErrorList, AsyncCallback<Void> callback);
	
	/**
	 * Date : 04-08-2018 By ANIL
	 */
	
	public void setSalarySlipData(ArrayList<PaySlip> salarySlipList, AsyncCallback<Void> callback);
	/** date 9.7.2018 added by komal for vendor invoice download **/
	public void setVendorinvoicelist(ArrayList<VendorInvoice> invoicingarray,AsyncCallback<Void> callback);
	
	/**** Date 19-10-2018 by Vijay for Complain Report ******/
	public void setComplainReport(Date fromDate, Date toDate, AsyncCallback<Void> callback);
	
	
	public void setServiceInvoiceMapping(HashMap<Integer, ServiceInvoiceDetails> serInvDt,AsyncCallback<Void> callback);

	/**  22-11-2018 added by amol for service CNC download* */
	
	public void setCNClist(ArrayList<CNC> cncArray,AsyncCallback<Void>callback);
	
	
	/** 23-11-2018 added by amol for projectallocation download **/
	
	
	public void setProjectallocationlist(ArrayList<ProjectAllocation> projectallocationArray,AsyncCallback<Void>callback);
	
	/**23-11-2018 added by amol for Asset Allocation download **/
	
	public void setAssetallocationlist(ArrayList<EmployeeAsset> assetallocationArray,AsyncCallback<Void>callback);
	
	/** 26-11-2018 added by amol for adhoc download **/
	public void setAdhocsettlementlist(ArrayList<ArrearsDetails> adhocsettlementArray,AsyncCallback<Void>callback);
	
	
	/** 26-11-2018 added by amol for company contribution download **/ 
	public void setCompanycontributionlist(ArrayList<CompanyPayrollRecord> companycontributionArray,AsyncCallback<Void>callback);
	
	/**30-11-2018 added by amol for CTCtemplate download**/
	public void setCTCtemplatelist(ArrayList<CTCTemplate> ctctemplateArray,AsyncCallback<Void>callback);
	
	/**30-11-2018 added by amol for voluntrypf download**/
	public void setVoluntrypflist(ArrayList<VoluntaryPf> voluntrypfArray,AsyncCallback<Void>callback);
	
	/**30-11-2018 added by amol for voluntrypfhistory download**/
	public void  setVoluntrypfhistorylist(ArrayList<VoluntaryPfHistory> voluntrypfhistoryArray,AsyncCallback<Void>callback);
	
/**1-12-2018 added by amol for HRproject download**/
	public void setHRprojectlist(ArrayList<HrProject> hrprojectArray,AsyncCallback<Void>callback);


	/**
	 * @author Anil,Date: 07-02-2019
	 * This method is used to set invoice data for sales register download and also loading other data related which will be used for same
	 * earlier we are doing same with "setinvoicelist()";
	 */
	
	public void setSalesList(ArrayList<Invoice> invoicingarray,AsyncCallback<Void> callback);
	
	public void setcrminvoicelist(ArrayList<Invoice> invoicingarray,AsyncCallback<Void> callback);

/****23-2-2019 added by amol for  download the unallocated employee data  in process payroll ***/
	public void setUnallocatedemployeelist(ArrayList<AllocationResult> unallocatedemployeeArray,AsyncCallback<Void>callback);


	/** 12-03-2019 added by Anil for EmployeeLeave download**/
	public void setEmployeeLeaveList(ArrayList<EmployeeLeave> empLeaveArray,AsyncCallback<Void>callback);
	
	
	
	/**23-8-2019 added by Amol for Employee Overtime download **/
	public void setEmployeeOvertimeList(ArrayList<EmployeeOvertime> empOvertimeArray,AsyncCallback<Void>callback);

	/**
	 * Updated By: Viraj
	 * Date: 19-03-2019
	 * Description: To set billedUnbilled data for csv file
	 * @param unBilledList
	 */
	public void setBilledUnbilledList(ArrayList<BillUnbilledAr> unBilledList,AsyncCallback<Void> asyncCallback);
	/**29-3-2019 added by Amol for Form B Download**/
	public void setFormBDetailList(ArrayList<PaySlip>formBArray,AsyncCallback<Void>callback);
	
	/**
	 * Updated By: Viraj
	 * Date: 24-05-2019
	 * Description: To make string variable for sales Register requirment of SilverLining
	 * @return
	 */
	public void fetchStringOfSalesRegister(AsyncCallback<String> callback);
	/**@author Amol
	 * Date 7-7-2019
	 * Description added for form XIII
	 */
	public void setFormXIII(ArrayList<Employee>empDetailsarray ,AsyncCallback<Void>callback);
	
		/** date 26-07-2019 added by komal to download attendance **/
	public void setAttendanceList(ArrayList<Attendance> attendacneArray,AsyncCallback<Void>callback);
	/**@author Amol Date 26-2-2020  by Amol added download option for appderegister**/
	public void setRegisterDeviceList(ArrayList<RegisterDevice>appDeregesterArray,AsyncCallback<Void>callback);
	
	public void setCreditNotelist(ArrayList<CreditNote> creditnotelist, AsyncCallback<Void>callback);
	
	public void setSMSTemplatelist(ArrayList<SmsTemplate> smstemplatelist,AsyncCallback<Void>callback);
	
	public void setExcelData(ArrayList<String> list, int columnCount, String documentName, AsyncCallback<String> asyncCallback);
    public void setEmployeeAttendanceErrorlist(ArrayList<AttandanceError> list, AsyncCallback<Void> callback);
    
	public void getData(ArrayList<SuperModel> list, String documentName,  AsyncCallback<ArrayList<SuperModel>> asyncCallback);

}
