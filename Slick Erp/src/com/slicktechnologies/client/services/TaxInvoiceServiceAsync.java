package com.slicktechnologies.client.services;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public interface TaxInvoiceServiceAsync {


	/**
	 * @author Anil , Date : 16-09-2019
	 * moving invoice creation part from client side to server side
	 * multiple invoice created against single bill
	 */
	
	void createTaxInvoice(BillingDocument model, String invoiceType, String invoiceCategory, String invoiceType2, String invoiceGruop,String approverName,String userName,boolean approveInvoiceCreatePaymentDocFlag, AsyncCallback<String> callback);
	void createPayment(Invoice model, AsyncCallback<String> callback); //Ashwini Patil Date:19-03-2024 orion reported many approved invoices where payment has not created
	void validateInvoiceForZohoIntegration(Invoice model, AsyncCallback<String> callback);//Ashwini Patil Date:3-05-2024 for Rex - Invoice integration with zoho books
	void createInvoiceInZohoBooks(int invoiceId,long companyId,String loggedInUser, AsyncCallback<String> callback);//Ashwini Patil Date:3-05-2024 for Rex - Invoice integration with zoho books
	void updateInvoiceInZohoBooks(int invoiceId,long companyId,String loggedInUser, AsyncCallback<String> callback);//Ashwini Patil Date:3-05-2024 for Rex - Invoice integration with zoho books
	void validateCustomerForZohoIntegration(Customer model, AsyncCallback<String> callback);//Ashwini Patil Date:6-06-2024 for Rex - customer integration with zoho books
	void createCustomerInZohoBooks(int custId,long companyId, AsyncCallback<String> callback);//Ashwini Patil Date:6-06-2024 for Rex - customer integration with zoho books
	/*
	 * Ashwini Patil 
	 * Date:06-06-2024
	 * Ultima search has uploaded contracts using customer with service details 
	 * but for same company name they put different poc names and due to which, system created multipla customers
	 * now they want to keep one customer and discard duplicate customer
	 * This program will change customer id in all documents such as contract, bill, invoice,payment and service	 
	 */	
	void UpdateCustomerIdInAllDocs(Customer model,String custIdToBeSet,String loggedInUser,AsyncCallback<String> callback);

}
