package com.slicktechnologies.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;


public interface PdfServiceAsync {
	public void createPdf(Quotation quot,AsyncCallback<Void> callback);
	public void setServiceList(ArrayList<Service>aray,AsyncCallback<Void> callback);

}
