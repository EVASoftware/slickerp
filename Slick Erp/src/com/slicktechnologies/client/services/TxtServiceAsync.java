package com.slicktechnologies.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;

public interface TxtServiceAsync {
	public void setPaySliplist(ArrayList<PaySlip> paySlipList,AsyncCallback<Void> callback);
}
