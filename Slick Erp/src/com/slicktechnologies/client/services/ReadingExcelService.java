package com.slicktechnologies.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.common.ExcelRecordsList;


@RemoteServiceRelativePath("readingexcelserv")
public interface ReadingExcelService extends RemoteService{
	
	ArrayList<ExcelRecordsList> getAttendiesdetails(long companyId);
}
