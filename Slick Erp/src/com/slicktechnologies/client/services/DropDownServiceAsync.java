package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApprovalDetails;

public interface DropDownServiceAsync {
	
	void getDropDown(String type,AsyncCallback<ArrayList<SuperModel>>callback);
	void getApprovalDropDown(long companyId,ArrayList<MultilevelApprovalDetails> approvalLis,int level,AsyncCallback<ArrayList<String>> callback);
	
	/**
	 * Date : 27-02-2017 By ANIL
	 * This method loads employees as per branch ,assigned to login person.
	 */
	void getEmployeeDropDown(List<String> branchList,Long companyId,AsyncCallback<ArrayList<SuperModel>>callback);
}
