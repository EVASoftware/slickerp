package com.slicktechnologies.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
public interface CommunicationLogServiceAsync {

	void saveCommunicationLog(ArrayList<InteractionType> communicationloglist, AsyncCallback<Void> callback);
	
	/**
	 * Date 13-11-2019 
	 * @author Vijay Chougule 
	 * Des :- NBHC CCPM Rate Contract Service value updation when Billing document Approved
	 */
	void updateRateContractServiceValue(BillingDocument billingDocument, AsyncCallback<String> callback);
	
	
	/**
	 * Date 14-11-2019 
	 * @author Vijay Chougule
	 * Des :- NBHC CCPM complaint service validation for if Assessment is in created status
	 */
	void ValidateAssessmentForComplaintService(ArrayList<Integer> complaintServicelist, long companyId,AsyncCallback<String> callback);
}
