package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.CommodityFumigationDetails;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.inventory.MaterialConsumptionReport;

public interface XlsxServiceAsync {
	
	public void setServiceFumigatioList(ArrayList<CommodityFumigationDetails> qarray,AsyncCallback<Void> callback);
	public void setServiceValueList(ArrayList<CommodityFumigationDetails> qarray,AsyncCallback<Void> callback);
	/** date 08.08.2018 added by komal for cost sheet download **/
	public void setCostSheetData(CNC cnc , AsyncCallback<Void> callback);
	
	
	/***31-1-2-2019  added by Amol for payrollsheet download*****/
	public void setPayrollSheetData(CNC cnc1,AsyncCallback<Void>callback);
	
	/** nidhi 9-10-2018 *:*:* */
	public void setBomServiceList(ArrayList<Service> serList,AsyncCallback<Void> callback);
	
//	public void setContractPNLReport(ArrayList<MaterialConsumptionReport> reportList,String Date,String segment,AsyncCallback<Void> callback);
	public void setContractPNLReport(ArrayList<MaterialConsumptionReport> reportList, Date fromDate , Date toDate ,String segment,long companyId,AsyncCallback<Void> callback);
	/**
	 * Updated By: Viraj
	 * Date: 13-06-2019
	 * Description: To set from date and to date for displaying in download
	 */
	void setServiceFromToDate(Date fromdate, Date toDate,AsyncCallback<Void> callback);

}
