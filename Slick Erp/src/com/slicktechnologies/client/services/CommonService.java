package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.shared.AuditObservations;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ObservationImpactRecommendation;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProjectOvertime;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

@RemoteServiceRelativePath("commomService")
public interface CommonService extends RemoteService{

	void printSRCopy(long companyId ,Date fromDate , Date toDate , String branch , int serviceId ,ArrayList<String> emailIdList,Contract contract);
	
	
	public String deleteStaticDataFromHeapMemory();
	public String checkHeapMemoryStatus();
	
	public String updateWMSServicesFlag(long companyId);
	
	public String updateStock(DeliveryNote model);
	
	public String CallPaymentGatewayAPI(int documentId, String documentName, String AppURL,String appId, long companyId, int customerId, String pdfURLLink);
	
	public Quotation updateAddress(Quotation quotationEntity );
	
	public ArrayList<SuperModel> getSalesRegisterData(MyQuerry querry, Date fromDate, Date toDate, int invoiceid, long companyId);

	public String updateStationedContractIntoNormalContract(int contractId, long companyId);
	public String updateNormalContractIntoTechnicianContract(int contractId, long companyId);

	public String implementationScreenOperation(String actionTask, long companyId, boolean checkboxValue);
	
	public String enableDisableSelfApproval(ArrayList<ProcessName> list, long companyId, boolean checkboxValue,String processType);
	
	public String activateDeactivateProcessConfiguration(ArrayList<ProcessTypeDetails> list,long companyId);
	
	public String activateDeactivateProcessConfigs(String processName, String processType, boolean checkboxValue, long companyId, String name);

	public String validateMaterialQuantity(long companyId, List<ProductGroupList> groupList);

	public String getPDFURL(String documentName,SuperModel model, int documentId,long companyId,Company company, String moduleURL,String salaryslipMonth);

	/**
	 * @author Ashwini Patil 
	 * @since 12-02-2022
	 * Using below method user can send multiple SR copies as per required service status and document format(pdf/excel)
	 */
	void printSummarySRCopy(Long companyId, Date fromDate, Date toDate, String branch, int serviceId,
			ArrayList<String> emailIdList, Contract contract,String reportName, String reportFormat, Boolean completedFlag,
			Boolean cancelledFlag, Boolean otherFlag);
	
	/**
	 * @author Ashwini Patil 
	 * @since 31-03-2022
	 * Sunrise client has employees in thousands which are not getting loaded in a minute so sending employee details through email using taskqueue.
	 */
	void sendCTCEmail(Long companyId,ArrayList<String> emailIdList); 
	
	//Ashwini Patil 22-07-2022
	void callSRCopyEmailTaskQueue(long companyId ,Service serviceId,String branchEmail);
		
			
	public String validateUploadedFileSize(DocumentUpload uploadedFile, long companyId);

	public String updateServiceValue(int contractId,long companyId);
	
	public ArrayList<CancelContract> getServiceInvoiceDocument(int contractId, long companyId);
	public String updateContractDocuments(int contractId, long companyId,boolean serviceCancelFlag,boolean serviceDeleteFlag,boolean invoiceCancelFlag,boolean deleteinvoiceFlag );
	
	/**
	 * @author Vijay 
	 * Des :- storing hrproject overtime list in seperate entity when list size is greater than 150 
	 */
	public ArrayList<HrProjectOvertime> getHrProjectOvertimelist(int projectId, long companyId);
	public String updateHrProjectOvertime(ArrayList<HrProjectOvertime> list, String oprationName);
	public ArrayList<HrProjectOvertime> getHrProjectOvertimelist(ArrayList<Integer> projectIdlist, long companyId);
	public String updateHrProjectOvertimelist(ArrayList<Integer> hrprojectIdlist, long companyId, String operationName,int projectId);
	/**
	 * ends here
	 */
	
	/**
	 * @author Vijay Date :- 19-09-2022
	 * Des :- Employee project allocation storing in seperate entity
	 */
	public String updateEmployeeProjectAllocationList(ArrayList<EmployeeProjectAllocation> list, String oprationName);
	public String genrateHRProjectOnProcejectAllocationSave(ProjectAllocation projectAllocation);

	/**
	 * ends here
	 */
	//Ashwini Patil 15-09-2022
	public String deletePaymentDocumentsOnInvoiceReset(int invoiceId, long companyId, String username);
	
	public AuditObservations saveNewObservationImpactRecommendation(long companyId, ObservationImpactRecommendation observation);
		
	public String setupPedio(long companyId,String fromLink,String tolink);
	 
	public String deleteInvalidLogin(String userName, long companyId);
	 /**
		 * @author Ashwini Patil 
		 * @since 25-01-2023
		 * To share customer portal link with all active customers
		 */
	 public void shareCustomerPortalLink(Long companyId,boolean sms,boolean whatsapp,boolean email); 
	 public String checkCustomerPortalLicense(Long companyId,boolean sms,boolean whatsapp,boolean email); 
	
	 public String SendContractDataOnEmail(Long companyId, Date fromDate, Date toDate, boolean activeContractsFlag, ArrayList<String> emailIdlist, String url);

	 public String sendSRFormatVersion1CopyOnEmail(Date fromDate, Date toDate, String team, String technicianName, ArrayList<String> toemaillst, long companyId);
	 
	 public String createAccountingInterface(SuperModel model);
	 public String createSpecificDocumentAccountingInterface(SuperModel model);

	 public Invoice updateInvoiceOrderTypeInProduct(Invoice invoiceEntity);
	 
	 public String deleteEntryFromAccountingInterface(SuperModel model);
	 
	 public String suspendServices(List<Service> servicelist, String suspendRemark);
	 
	 public String validateNumberRange(String className, long numberrange, long companyId);
	 
	 public String updateAccountingInterface(SuperModel model);

	 public String deleteAndCreateAccountingInterfaceData(Date fromDate, Date toDate, long companyId,boolean paymentDocflag);
	 
	 public String cancelCustomerBranchServices(ArrayList<CustomerBranchDetails> customerbranchlist, long companyId, String Cancellationremark, String loggedinuser,int customerId);
	 
	 public String updateCustomerBranchServiceAddressInServices(int customerId,long companyId);
	 
	 public Integer getNumberOfHRLicense(long companyId);//Ashwini Patil Date:8-09-2023
		
	public Integer getNumberOfPayrollProcessedInMonth(long companyId, String salaryPeriod);//Ashwini Patil Date:8-09-2023

	 
}
