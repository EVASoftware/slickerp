package com.slicktechnologies.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.common.ExcelRecordsList;

public interface ReadingExcelServiceAsync {

	
	void getAttendiesdetails(long companyId,AsyncCallback<ArrayList<ExcelRecordsList>> callback);
}
