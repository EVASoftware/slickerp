package com.slicktechnologies.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.client.views.ratecontractproductpopup.ProductInfo;
import com.slicktechnologies.shared.Contract;
public interface RateContractServicesBillingServiceAsync {
	
	void CreateServiceAndBilling(Contract contractObj,ArrayList<ProductInfo> productlist,boolean validate, AsyncCallback<ArrayList<String>> callback );
	void updateEmployees(long companyId, AsyncCallback<ArrayList<String>> callback);
	void CreateServiceAndBillingForBranches(Contract contractObj,ArrayList<ProductInfo> productlist,boolean validate, AsyncCallback<ArrayList<String>> callback );
	
}
