package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.ProductInventoryTransaction;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;


@RemoteServiceRelativePath("updateserviceimpl")
public interface UpdateService extends RemoteService{

	ArrayList<ProductInventoryTransaction> updateInventoryProductListStock(ArrayList<ProductInventoryTransaction> productInventoryList,MaterialIssueNote minEntity);

	void updateProductInventoryListIndex(Long companyId);
	
	/**
	 * Date : 19-01-2017 By Anil
	 */
	public String cancelMIN(MaterialIssueNote min);
	
	/**
	 * Date : 13-02-2017 By ANIL
	 */
	public String cancelDocument(SuperModel model);
	
	/**
	 * rohan added this code for updating customer payment records 
	 * Date : 21/2/2017
	 */
	public void updatePaymentDetailsRecords(Long companyId);
	
	/**
	 * rohan added this code for Accounting interface
	 */
	
	public ArrayList<AccountingInterface> getAccountingInterfaceDataSynched(ArrayList<AccountingInterface> acclist, String syncStatus, String syncBy); 

	public void updateSynchDataWithSynchDate(ArrayList<AccountingInterface> acclist);
	
	public void cancelDeleteFromTallyRecord(ArrayList<AccountingInterface> acclist);
	
	/**
	 * rohan added this code Canceling Expense Mngt
	 */
	public void reactOnCancelExpenseMngt(MultipleExpenseMngt model, String remark,String loginUser);
	
	
	/**
	 * rohan added this code Canceling Contract
	 */
	
	public void reactOnCancelContract(Contract con,String remark,String loginUser);
	
	/**
	 * Date 3 march 2017
	 * added by Vijay
	 * for adding payment entry into petty cash
	 */
	public void depositepaymentAmtIntoPettyCash(int paymentdocId,long companyId, String personResponible, Date depositDate,String savetime,double payreceived, String pettyCashName);
	
	/**
	 * Date : 21-02-2017 By ANil
	 */

	public String cancelServices(ArrayList<Service> list, String remark , String loggedInUser);
	
	/**
	 * Date : 21-02-2017 By Anil
	 */
	
	public String cancelDocument(String taskName,Long companyId);
	
	/**
	 * Date 16-05-2017 by rohan for multiple payment at same time
	 */
	public String saveMultiplePaymentDetails(List<CustomerPayment> selectedPaymentList,String accntNumber ,String bankName, String branchName, String chequeNumber, Date chequeDt, Date transferDt, String refNumber, String issueBy,String paymentMethod);
	
	/**
	 * Date : 01-07-2017 by rohan for multiple payment at same time
	 */
	public String updateStateCode(Long companyId);
	
	/**
	 * Date : 14-07-2017 By Anil 
	 * For NBHC CCPM
	 * This is used to change the status of all customer to Active
	 */
	
	public String updateCustomer(Long companyId,String taskName);
	/**
	 * End
	 */
	
	/** 
	 * date 01/11/2017 added by komal for contract cancel (new)
	 * @author Anil , Date : 11-12-2019
	 * for nbhc we are not loading services at the time of fetching records
	 */
	
	public ArrayList<CancelContract> getRecordsForContractCancel(Long companyId , int orderId, boolean contractStatus,boolean serviceFlag, String callFrom);
	
	public void saveContractCancelData(Contract con,String remark,String loginUser ,ArrayList<CancelContract> cancelContractList);
	
	
	/**
	 *  Created By :nidhi
	 *  Date :10-11-2017
	 *  Description method add for commodity fumigation details report. 
	 * @param companyId
	 */
	public void dailyFumigationReportDetails(Long companyId,Date date);
	
	/**
	 *  end
	 */
	/**
	 * nidhi
	 * 16-11-2017
	 * fumigation download
	 * 
	 */
	public void dailyFumigationValueReportDetails(Long companyId,Date date);
	/**
	 *  end
	 */
	
	 /**
	  * nidhi
	  * 28-01-2018
	  * for date wise service fumigation report 
	  */
	public void dailyFumigationValueReportDetails(Long companyId, Date todayparse,Date duration, List<String> branchList);
	public void dailyFumigationReportDetails(Long companyId, Date todayparse,Date duration, List<String> branchList);
	
	Integer serviceInvoiceIdMapping(Long companyId, Date todayparse,Date duration, List<String> branchList);
	
	public String partialGRNCancellation(SuperModel model);
	public String updateSalesPerson(Long companyId,int contractId, String salesPerson);//Ashwini Patil Date:14-06-2024 for ultra pest- salesperson should get updated in bills, invoice and payment 
	
	public String createExtraServicesForCustomerBranches(ArrayList<BranchWiseScheduling> custBranchList, Contract con, String loggedinuser);
	
	public String updateFindingEntity(Long companyId);
}
