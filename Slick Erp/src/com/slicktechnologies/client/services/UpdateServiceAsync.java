package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.ProductInventoryTransaction;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

public interface UpdateServiceAsync {

	void updateInventoryProductListStock(ArrayList<ProductInventoryTransaction> productInventoryList,
			MaterialIssueNote minEntity, AsyncCallback<ArrayList<ProductInventoryTransaction>> asyncCallback);

	void updateProductInventoryListIndex(Long companyId, AsyncCallback<Void> asyncCallback);
	
	/**
	 * Date : 19-01-2017 By Anil
	 */
	public void cancelMIN(MaterialIssueNote min,AsyncCallback<String> callback);
	
	/**
	 * Date : 13-02-2017 By ANIL
	 */
	public void cancelDocument(SuperModel model,AsyncCallback<String> callback);
	
	
	/**
	 * Date : 13-02-2017 By Rohan
	 */
	public void updatePaymentDetailsRecords(Long companyId,AsyncCallback<Void> callback);
	
	/**
	 * Date : 23-02-2017 By Rohan
	 */
	void getAccountingInterfaceDataSynched(ArrayList<AccountingInterface> acclist, String syncStatus, String syncBy, AsyncCallback<ArrayList<AccountingInterface>> asyncCallback);
	
	void updateSynchDataWithSynchDate(ArrayList<AccountingInterface> acclist, AsyncCallback<Void> callback);
	
	void cancelDeleteFromTallyRecord(ArrayList<AccountingInterface> acclist, AsyncCallback<Void> callback);
	/**
	 * Date : 28/2/2017 by Rohan for canceling contract
	 */
	
	void reactOnCancelContract(Contract con,String remark,String loginUser ,AsyncCallback<Void> callback);
	
	/**
	 * Date 3 march 2017
	 * added by Vijay
	 * for adding payment entry into petty cash
	 */
	public void depositepaymentAmtIntoPettyCash(int paymentdocId, long companyId, String personResponible,Date depositDate,String savetime,double payreceived,String pettyCashName, AsyncCallback<Void> callback);
	
	/**
	 * Date : 21-02-2017 By ANil
	 */
	public void cancelServices(ArrayList<Service> list, String remark , String loggedInUser,AsyncCallback<String> callback);
	
	/**
	 * Date : 21-02-2017 By Anil
	 */
	
	public void cancelDocument(String taskName,Long companyId,AsyncCallback<String> callback);

	/**
	 * Date : 28/2/2017 by Rohan for canceling expense
	 */
	
	void reactOnCancelExpenseMngt(MultipleExpenseMngt model, String remark,String loginUser, AsyncCallback<Void> asyncCallback);
	
	/**
	 * Date : 16-05-2017 by rohan for multiple payment at same time
	 */
	void saveMultiplePaymentDetails(List<CustomerPayment> selectedPaymentList,String accntNumber ,String bankName, String branchName, String chequeNumber, Date chequeDt, Date transferDt, String refNumber, String issueBy,String paymentMethod, AsyncCallback<String> asyncCallback);
	
	/**
	 * Date : 01-07-2017 by rohan for multiple payment at same time
	 */
	void updateStateCode(Long companyId,AsyncCallback<String> asyncCallback);
	
	/**
	 * Date : 14-07-2017 By Anil 
	 * For NBHC CCPM
	 * This is used to change the status of all customer to Active
	 */
	
	void updateCustomer(Long companyId,String taskName,AsyncCallback<String> asyncCallback);
	/**
	 * End
	 */
	
	/** 
	 * date 01/11/2017 added by komal for contract cancel (new)
	 * @author Anil , Date : 11-12-2019
	 * for nbhc we are not loading services at the time of fetching records
	 */
	
	void getRecordsForContractCancel(Long companyId , int orderId , boolean contractStatus,boolean serviceFlag,String callFrom, AsyncCallback<ArrayList<CancelContract>> asyncCallback);
	
	void saveContractCancelData(Contract con,String remark,String loginUser ,ArrayList<CancelContract> cancelContractList ,AsyncCallback<Void> callback);

	/**
	 * nidhi
	 * 10-11-2017
	 * fumigation download
	 * 
	 */
	 void dailyFumigationReportDetails(Long companyId,Date date,AsyncCallback<Void> asyncCallback);
	/**
	 *  end
	 */
	 
	 /**
		 * nidhi
		 * 16-11-2017
		 * fumigation download
		 * 
		 */
		 void dailyFumigationValueReportDetails(Long companyId,Date date,AsyncCallback<Void> asyncCallback);
		/**
		 *  end
		 */
		 
		 /**
		  * nidhi
		  * 28-01-2018
		  * for date wise service fumigation report 
		  */
		 void dailyFumigationValueReportDetails(Long companyId, Date todayparse,Date duration, List<String> branchList,AsyncCallback<Void> asyncCallback);
		 void dailyFumigationReportDetails(Long companyId, Date todayparse,Date duration, List<String> branchList,AsyncCallback<Void> asyncCallback);

		 void serviceInvoiceIdMapping(Long companyId, Date todayparse,Date duration, List<String> branchList,AsyncCallback<Integer> asyncCallback);

		void partialGRNCancellation(SuperModel model, AsyncCallback<String> asyncCallback);
		
		void updateSalesPerson(Long companyId,int contractId, String salesPerson,AsyncCallback<String> asyncCallback);//Ashwini Patil Date:14-06-2024 for ultra pest- salesperson should get updated in bills, invoice and payment 

		void createExtraServicesForCustomerBranches(ArrayList<BranchWiseScheduling> custBranchList, Contract con, String loggedinuser,AsyncCallback<String> asyncCallback);
		
		void updateFindingEntity(Long companyId,AsyncCallback<String> asyncCallback); 
}
