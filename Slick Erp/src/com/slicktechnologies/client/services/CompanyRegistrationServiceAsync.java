package com.slicktechnologies.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.shared.common.businessunitlayer.Company;



public interface CompanyRegistrationServiceAsync{
	
	public void saveCompany(Company company,AsyncCallback<ReturnFromServer>call);
	
	public void getSearchResult(MyQuerry quer,AsyncCallback<ArrayList<SuperModel>>call);
	
	/**
	  * Date 28 feb 2017
	  * added by vijay
	  * for creating configs on click in company presenter create configs button
	  */

	 public void createConfigsData(long companyId, AsyncCallback<ArrayList<String>> asyncCallback);
	 public void resetAppid(long companyId,String appId,String companyName,String loggedinuser,int otp,String approverCellNo,AsyncCallback<String> asyncCallback);

}
