package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.views.contractrenewal.RenewalResult;
import com.slicktechnologies.client.views.customertraining.CustomerTrainingAttendies;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.sms.SMSDetails;

public interface SmsServiceAsync {
//	void sendSmsToClient(String msgTemplate,long clientCell,String accSid, String authtoken, String frmnumber, long companyId, AsyncCallback<Integer> callback);
	void sendSmsToClient(String msgTemplate,long clientCell, long companyId, AsyncCallback<Integer> callback);

	void sendSmsToCustomers(String msgTemplate,ArrayList<CustomerTrainingAttendies> custlist,String accSid, String authtoken, String frmnumber, boolean bhashSMSAPIFlag, AsyncCallback<Integer> callback);
	void sendContractRenewalSMS(String msgTemplate,ArrayList<RenewalResult> custlist,String accSid, String authtoken, String frmnumber,long companyId, boolean bhashSMSAPIFlag, AsyncCallback<Integer> callback);
	/**
	 * Updated By: Viraj
	 * Date: 12-04-2019
	 * Description: changed lead sms send method 
	 * @param companyId
	 * @param cellNumber
	 * @param leadId
	 * @param companyName
	 * @param bhashSMSAPIFlag
	 * @param callback
	 */
	void checkSMSConfigAndSendSMS(long companyId, long cellNumber, long leadId,
			String companyName,int customerId,
			AsyncCallback<ArrayList<Integer>> callback);
	
	/**
	 * Date 16-03-2017
	 * added by vijay
	 * for Sending SMS to customer for Contract Cancellation requirement from Petra pest
	 */
	void checkSMSConfigAndSendContractCancellationSMS(long companyId, String customerName, long custCellNo, int contractId, String companyName ,boolean bhashSMSAPIFlag,int customerId, AsyncCallback<ArrayList<Integer>> callback);
	
	/**
	 * Date 17-03-2017
	 * added by vijay
	 * for sending sms to customer for complaint raised requirement from Petra pest
	 */
	
	void checkSMSConfigAndSendComplaintRegisterSMS(long companyId, String serviceid, String customerName, int complaintId,String companyName,long custCellNo, int customerId, AsyncCallback<ArrayList<Integer>> callback);

	/**
	 * Date 17-03-2017
	 * added by vijay
	 * for sending sms to customer for complaint Resolved (complated) requirement from Petra pest
	 */
	void checkSMSConfigAndSendComplaintResolvedSMS(long companyId, Date serviceDate, String customerName, int complaintId, String companyName, long custCellNo, int customerId, AsyncCallback<ArrayList<Integer>> callback);
	
	void validateAndSendSMS(SMSDetails smsDetails, AsyncCallback<String> callback);
	
	/**Added by Sheetal : 15-03-2022
	 * Des : For sending SMS to customer on lead cancelled and lead unsuccessful events**/
	void sendSMSToCustomer(Lead lead, AsyncCallback<String> callback);

	void sendMessage(String moduleName, String documentName, String templateName, Long companyId, String actualMsg, long mobileNo,boolean messagefromPopupFlag, AsyncCallback<String> callback);

	void checkConfigAndSendContractDownloadOTPMessage(long companyId, String loggedInUser, int otp,AsyncCallback<String> callback); //Ashwini Patil Date:4-07-2023
	void checkConfigAndSendServiceDownloadOTPMessage(long companyId, String loggedInUser, int otp,AsyncCallback<String> callback); //Ashwini Patil Date:4-07-2023
	void checkConfigAndSendWrongOTPAttemptMessage(long companyId, String loggedInUser, int otp, String documentname,AsyncCallback<String> callback); //Ashwini Patil Date:4-07-2023
	void checkEmailConfigAndSendComplaintResolvedEmail(long companyId, Date serviceDate, String customerName, int complaintId, String companyName, long custCellNo, int customerId, AsyncCallback<String> callback);
	void sendMessageOnWhatsApp(long companyId, String mobileNo,String actualMessage,AsyncCallback<String> callback);//15-01-2024 
}
