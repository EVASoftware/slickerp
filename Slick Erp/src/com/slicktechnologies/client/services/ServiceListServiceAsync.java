package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
public interface ServiceListServiceAsync {

	public void setServicesSelectedArrayList(ArrayList<Service> serviceList,AsyncCallback<String> callback);
	public void setServicesSelectedListArrayList(ArrayList<Service> serviceList,AsyncCallback<String> callback);
	
	public void printMultipleServices(ArrayList<Service> serviceList,AsyncCallback<String> callback);
	
	/********* vijay for mark complete btton on list 
	 * @param date ********************/
	void markcompleteservice(Date serviceCompetionDate, Service serviceobj,AsyncCallback<ArrayList<Integer>> callback);
	
	/**** Date 10 oct 2017 added by vijay for completing service though Technician scheduling list
	 * @param loggedInUser ************/
	void TechnicianSchedulingListMarkcompleteservice(Service serviceobj,String loggedInUser,int projectId, ArrayList<ProductGroupList> materialList, AsyncCallback<ArrayList<Integer>> callback);

	
	/**
	 * Date 04-05-2018
	 * Developer :- Vijay
	 * Description :- services list complete services with bulk  
	 */
	void markCompleteServicesWithBulk(ArrayList<Service> serviceList,String remark, Date serviceCompletionDate, AsyncCallback<Integer> callback);
	/**
	 * ends here
	 */
	void checkScheduleServices(ArrayList<Integer> contractCount, Date serDate ,Date billDate, AsyncCallback<Integer> callback);
	/**
	 * Date 02-08-2018
	 * Developer :- nidhi
	 * Description :- services list for assgin service engineer
	 */
	void setServiceTechnicianToServices(ArrayList<Service> serviceList,String remark, 
				String techEmployee,Long companyId, AsyncCallback<Integer> callback);
	/**
	 * ends here
	 */
	
	/**
	 * Updated By:Viraj
	 * Date:17-06-2019
	 * Description: service list to assign branch
	 */
	void setSeviceBranchToService(ArrayList<Service> serviceList,
			String remark, String branch, Long companyId, AsyncCallback<Integer> callback);
	
	/**
	 * Updated By:Viraj
	 * Date:29-06-2019
	 * Description: service list to assign status
	 */
	void setServiceStatusToService(ArrayList<Service> serviceList,
			String remark, String status, Long companyId, AsyncCallback<Integer> callback);
	
	/**
	 * @author Anil
	 * @since 12-05-2020
	 */
	void updateComplainDocument(ArrayList<Service> serviceList,boolean isSchedule,boolean isAssignTechnician,boolean isScheduleTech,boolean isServiceCompletion,AsyncCallback<String> callback);

	/**
	 * @author Anil @since 23-03-2021
	 * adding RPC which will check and allocate material in case technician is changed and services are already scheduled
	 */
	void validateTechnicianStock(long companyId,String technicianName,ArrayList<Service> serviceList,AsyncCallback<ArrayList<String>> callback);
	
	public void sendServiceComletionSMS(Service service,AsyncCallback<String> callback);

	public void setServicesForScheduleOrCompleteViaExcelDonwload(ArrayList<Service> serviceList,AsyncCallback<String> callback); //Ashwini Patil Date:27-10-2023
	
	public void getContractServiceCount(Contract con,AsyncCallback<Integer> callback);//Ashwini Patil Date:11-04-2024 
	
	public void getContractBillingCount(Contract con,AsyncCallback<Integer> callback);//Ashwini Patil Date:16-04-2024
}
