package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.inventory.ExpenseCost;
import com.slicktechnologies.shared.common.inventory.LabourCost;
import com.slicktechnologies.shared.common.inventory.MaterialConsumptionReport;

public interface MaterialConsumptionServiceAsync {
	void retrieveMinList(long companyId,int contractCount,AsyncCallback<ArrayList<MaterialConsumptionReport>>callback);
	void retrieveMmnList(long companyId,int contractCount,AsyncCallback<ArrayList<MaterialConsumptionReport>>callback);


	void retrieveLabourCost(long companyId, String contractId,String typeOfPnL, Date fromDate, Date toDate,AsyncCallback<ArrayList<LabourCost>> callback);
	void retrieveExpenseCost(long companyId, String conOrCustCount,String typeOfPnL, Date fromDate, Date toDate, AsyncCallback<ArrayList<ExpenseCost>> callback );
	
	void retrieveAdministrativeCost(long companyId,String contractdate, AsyncCallback<ArrayList<String>> callback);
	
	/**
	 * rohan added this method to calculate contract p & l customer and contract wise for MIN 
	 * @param companyId --  company id
	 * @param parseInt  --  customer ID in case of Customer wise and contract id in case of contract wise
	 * @param string    --  this is how you want to calculate P & L
	 * @param asyncCallback
	 */
	
	void retriveMaterialDetails(long companyId,String conOrCustCount, String typeOfPnL, Date fromDate, Date toDate,AsyncCallback<ArrayList<MaterialConsumptionReport>>callback);
	
	/**
	 * rohan added this method to calculate contract p & l customer and contract wise for MMN 
	 * @param companyId --  company id
	 * @param parseInt  --  customer ID in case of Customer wise and contract id in case of contract wise
	 * @param string    --  this is how you want to calculate P & L
	 * @param asyncCallback
	 */
	
	void retriveMaterialMMNDetails(long companyId,String conOrCustCount, String typeOfPnL, Date fromDate, Date toDate,AsyncCallback<ArrayList<MaterialConsumptionReport>>callback);
	
	void  retrieveAdministrativeCost(long companyId,String contractOrCustCount, String type, Date fromDate, Date toDate, AsyncCallback<ArrayList<String>> callback);
	

	/**
	 * nidhi |*)
	 * 10-01-2018
	 * for bas on bom
	 */
	void retriveMaterialDetailsBaseOnBOM(long companyId,String conOrCustCount, String typeOfPnL, Date fromDate, Date toDate,AsyncCallback<ArrayList<MaterialConsumptionReport>>callback);
	
	
	void retriveMaterialMMNDetailsBaseOnBOM(long companyId,String conOrCustCount, String typeOfPnL, Date fromDate, Date toDate,AsyncCallback<ArrayList<MaterialConsumptionReport>>callback);
	
	void  retrieveAdministrativeCostBaseOnBOM(long companyId,String contractOrCustCount, String type, Date fromDate, Date toDate, AsyncCallback<ArrayList<String>> callback);
	/*
	 * HashSet<Integer> contract = new HashSet<Integer>();
									HashSet<Integer> serSet = new HashSet<Integer>();
									HashMap<Integer,HashSet<Integer>> contSer  = new HashMap<Integer, HashSet<Integer>>();
	 */
	void retriveServicePlannedRevanueAndBillingRevanue(ArrayList<MaterialConsumptionReport> ServiceContractDetails
			,HashSet<Integer> contract,HashSet<Integer> serSet ,HashMap<Integer,HashSet<Integer>> contSer,long companyID,AsyncCallback<ArrayList<MaterialConsumptionReport>>callback);

	void retrivelabourPlannedRevanueAndBillingRevanue(ArrayList<MaterialConsumptionReport> ServiceContractDetails
			,HashSet<Integer> contract,HashSet<Integer> serSet ,HashMap<Integer,HashSet<Integer>> contSer,long companyID,AsyncCallback<ArrayList<MaterialConsumptionReport>>callback);

	/** DATE 8.4.2019 added by komal for new contract p and l **/
	void  retrieveServiceDetails(long companyId,String contractid,String type, Date fromDate, Date toDate, ArrayList<String> emailList, AsyncCallback<ArrayList<MaterialConsumptionReport>>callback);
}
