package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.AmcAssuredRevenueReport;
import com.slicktechnologies.shared.BOMServiceListBean;
import com.slicktechnologies.shared.Service;

@RemoteServiceRelativePath("serverUnitConversionServices")
public interface ServerUnitConversionServices extends RemoteService{

	BOMServiceListBean getServiceBOMReport(Long companyId, Date date,Date endDate, String branch );

	BOMServiceListBean getServiceGetUpdate(BOMServiceListBean bomBean, Long company);
	BOMServiceListBean getServiceGetWithMrnUpdate(BOMServiceListBean bomBean,Long company);
}
