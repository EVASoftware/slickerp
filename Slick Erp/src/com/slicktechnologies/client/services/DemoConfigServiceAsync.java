package com.slicktechnologies.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.ScreenName;

public interface DemoConfigServiceAsync {
	void saveDemoConfig(Long companyId,String clientType,AsyncCallback<Integer> callback);
	void saveDemoConfigCategory(Long companyId,String clientType,AsyncCallback<Integer> callback);
	void saveDemoOtherDetails(Long companyId,String clientType,AsyncCallback<Integer> callback);
	void saveDemoAssetDetails(Long companyId,String clientType,AsyncCallback<Integer> callback);
	void saveDemoProductsDetails(Long companyId,String clientType,AsyncCallback<Integer> callback);
	void saveDemoInventoryDetails(Long companyId,String clientType,AsyncCallback<Integer> callback);
	void saveDefaultConfigs(Long companyId,String clientType,AsyncCallback<Integer> callback);
	
	void createselectedConfiguration(long companyId,ArrayList<String> selectedScreenName, AsyncCallback<String> callback);
	void createDefaultConfiguration(long companyId, AsyncCallback<String> callback);
	void createScreenMenuData(long companyId, AsyncCallback<String> callback);
	void createUserRoleData(long companyId, AsyncCallback<String> callback);
	void deleteAllScreenConfiguration(long companyId, AsyncCallback<String> callback);
	
	/**
	 * @author Anil @since 25-11-2021
	 */
	void copyConfiguration(long companyId,String fromLink,String toLink,ArrayList<ScreenName> entityList, AsyncCallback<String> callback);
}
