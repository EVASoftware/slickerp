package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.EmployeeShift;

@RemoteServiceRelativePath("csvservice")
public interface EmployeeShiftService extends RemoteService
{

	public ArrayList<EmployeeShift> getEmployeeShifts(Date currentDate);
	
}
