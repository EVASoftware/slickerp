package com.slicktechnologies.client.services;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("ipservice")
public interface IPAddressService extends RemoteService {
	String retrieveClientIP(long companyId);
	String retrieveUserIP(long companyId);
}
