package com.slicktechnologies.client.services;
import java.util.ArrayList;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.Service;

@RemoteServiceRelativePath("techniciandashboardserv")
public interface TechnicianDashboardService extends RemoteService
{

	Integer getReWorkdetails(long companyId, ArrayList<Service> servicelist);
}
