package com.slicktechnologies.client.services;

import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public interface LicenseUpdationServiceAsync {
	void updateLicenses(long companyId,int noOfUsers,Date licenseUpdateDate,AsyncCallback<Integer> callback);
	void retrieveCompanyInfo(long companyId,AsyncCallback<Company> callback);
}
