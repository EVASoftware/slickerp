package com.slicktechnologies.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.ScreenName;

@RemoteServiceRelativePath("democonfigservice")
public interface DemoConfigService extends RemoteService {
	public Integer saveDemoConfig(Long companyId,String clientType);
	public Integer saveDemoConfigCategory(Long companyId,String clientType);
	public Integer saveDemoOtherDetails(Long companyId,String clientType);
	public Integer saveDemoAssetDetails(Long companyId,String clientType);
	public Integer saveDemoProductsDetails(Long companyId,String clientType);
	public Integer saveDemoInventoryDetails(Long companyId,String clientType);
	public Integer saveDefaultConfigs(Long companyId,String clientType);
	
	public String createselectedConfiguration(long companyId, ArrayList<String> selectedScreenName);
	public String  createDefaultConfiguration(long companyId);
	public String createScreenMenuData(long companyId);
	public String createUserRoleData(long companyId);
	public String deleteAllScreenConfiguration(long companyId);
	
	/**
	 * @author Anil @since 25-11-2021
	 */
	public String copyConfiguration(long companyId,String fromLink,String toLink,ArrayList<ScreenName> entityList);

}
