package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.ContractUploadDetails;
import com.slicktechnologies.shared.CreditNote;
import com.slicktechnologies.shared.EmployeeAsset;
import com.slicktechnologies.shared.MINUploadExcelDetails;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.StackMaster;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorPriceListDetails;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.cnc.Consumables;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchServiceLocation;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.servicerelated.AssetMovementInfo;

public interface DataMigrationServiceAsync {
	
	/**
	 * Save Flag is added 
	 * Date:22-10-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project :Purchase Modification(NBHC)
	 */
	void savedRecordsDeatils(long id,String entityname,boolean saveFlag,String loggedinuser, AsyncCallback<ArrayList<Integer>> callback);
	
//	void savedRecordsDeatils(long id, String entityname, AsyncCallback<ArrayList<Integer>> callback);

	void savedConfigDetails(long id, String entityname,AsyncCallback<ArrayList<Integer>> callback);
	
	void savedTransactionDetails(long id,String entityname,AsyncCallback<ArrayList<Integer>> callback);
	
	/**
	 *  nidhi
	 *   for contraact service cycle upload process
	 * @param id
	 * @param entityname
	 * @param serviceStatus
	 * @param billingStatus
	 * @param loginPerson
	 * @return
	 */
	void savedContractTransactionDetails(long id, String entityname,Boolean serviceStatus, Boolean billingStatus, String loginPerson,AsyncCallback<ArrayList<String>> callback);
	void saveMinUploadProcess(MINUploadExcelDetails MinListDetails,AsyncCallback<MINUploadExcelDetails> callback);
	void savedUploadTransactionDetails(long id, String entityname,Boolean serviceStatus, Boolean billingStatus, String loginPerson,AsyncCallback<MINUploadExcelDetails> callback);
	/**
	 *  end
	 */
	
	void checkMinUploadServiceListDetails(MINUploadExcelDetails minDetailsMap,AsyncCallback<MINUploadExcelDetails> callback);
	
	void ServiceUploadToCompleteStatus(MINUploadExcelDetails minDetailsMap,AsyncCallback<MINUploadExcelDetails> callback);
/*	
 * nidhi for new contract upload process
 * void CheckUploadContractDetail(long companyId,AsyncCallback<ArrayList<String>> callback);
	void UploadContractDetails(long companyId,AsyncCallback<ArrayList<String>> callback);
	
	void finallyUploadContractDetails(long companyId,AsyncCallback<ArrayList<String>> callback);
	*/
	void updateProductInventoryViewList(MINUploadExcelDetails minDetailsMap,AsyncCallback<MINUploadExcelDetails> callback);

	/**
	 *  nidhi
	 *  19-03-2018
	 *  contract upload process for new way
	 */
	void UploadContractDetails(ContractUploadDetails contractDt,AsyncCallback<ContractUploadDetails> callback);
	
	void finallyUploadContractDetails(ContractUploadDetails contractDt,AsyncCallback<ArrayList<String>> callback);
	
	void savedContractUploadTransactionDetails(long id, String entityname, String loginPerson,AsyncCallback<ContractUploadDetails> callback);
	void checkContractUploadDetails(ContractUploadDetails contDetails,AsyncCallback<ContractUploadDetails> callBack);
	void CheckUploadContractDetail(ContractUploadDetails contDetails,AsyncCallback<ContractUploadDetails> callback);

	/**
	 * @author Anil , Date : 22-07-2019
	 * @param companyId
	 * @return
	 */
	void saveEmployeeAsset(long companyId,AsyncCallback<ArrayList<EmployeeAsset>> callBack);
	void validateEmployeeAsset(ArrayList<EmployeeAsset> list,AsyncCallback<ArrayList<EmployeeAsset>> callBack);
	void uploadEmployeeAsset(ArrayList<EmployeeAsset> list,AsyncCallback<ArrayList<EmployeeAsset>> callBack);
	void uploadCustomerBranch(long companyId,AsyncCallback<ArrayList<CustomerBranchDetails>> callBack);
	void  saveCustomerBranch(ArrayList<CustomerBranchDetails> list , AsyncCallback<String> callBack);
	
	
	/**
	 * @author Vijay Chougule Date :- 16-07-2020
	 * Des :- Vendor Product Price Upload
	 */
	void uploadVendorProductPrice(long companyId,AsyncCallback<ArrayList<VendorPriceListDetails>> callBack);
	void vednorProductPrice(long companyId,String taskName, AsyncCallback<String> callback);
	
	/**
	 * @author Vijay Chougule Date :- 29-07-2020
	 * Des :- CTC Template Upload
	 */
	void validateCTCTemplate(long companyId,AsyncCallback<ArrayList<CTCTemplate>> callback);
	void uploadCTCTemplate(long companyId, String taskName, AsyncCallback<String> callback);
	/**
	 * ends here
	 */
	
	/**
	 * @author Vijay Chougule Date - 28-09-2020
	 * Des :- To Update Assest Details in service requirement raised by Rahul Tiwari for PTSPL
	 */
	void validateUpdateAssetDetails(long companyId,AsyncCallback<ArrayList<AssetMovementInfo>> callback);
	void uploadAssetDetails(long companyId,String taskName, AsyncCallback<String> callback);
	/**
	 * ends here
	 */
	
	void validateCTCAllocation(long companyId,String loggedInUser, AsyncCallback<ArrayList<CTCTemplate>> callback);
	
	void validateAndUploadCreditNote(long companyId,AsyncCallback<ArrayList<CreditNote>> callback);

	void validateAndUploadInhouseSerivces(long companyId, ArrayList<String> strexcellist, AsyncCallback<ArrayList<StackMaster>> callback);
	void validateInhouseSerivces(long companyId, AsyncCallback<ArrayList<String>> callback);

	/**
	 * @author Vijay Chougule Date :- 12-08-2021
	 * Des :- Stock Upload and Update
	 */
	void validateStockUpload(long companyId,AsyncCallback<ArrayList<ProductInventoryView>> callback);
	
	/**
	 * @author Anil @since 11-11-2021
	 * Uploading consumables of CNC
	 * Sunrise SCM
	 */
	void validateConsumables(long companyId,int customerId,AsyncCallback<ArrayList<Consumables>> callback);
	/**Added by sheetal:31:01:2022 
	   Des: for user creation upload program**/
	void validateAndUploadUser(long companyId,AsyncCallback<ArrayList<Employee>> callback);
	
	void validateAndUploadServiceCreation(long companyId,AsyncCallback<ArrayList<Service>> callback);
	
	/**@author Sheetal : 02-06-2022
	 * Des : Upload program for customer branch with service location and area**/
	void uploadCustomerBranchWithLocationandArea(long companyId,AsyncCallback<ArrayList<CustomerBranchServiceLocation>> callback);


	
}
