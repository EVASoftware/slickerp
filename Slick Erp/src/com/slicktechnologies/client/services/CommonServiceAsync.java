package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.shared.AuditObservations;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ObservationImpactRecommendation;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProjectOvertime;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public interface CommonServiceAsync {

	void printSRCopy(long companyId ,Date fromDate , Date toDate , String branch , int serviceId, ArrayList<String> emailIdList,Contract contract ,AsyncCallback<Void> callback);
	
	//Ashwini Patil 12-02-2022
	void printSummarySRCopy(Long companyId, Date fromDate, Date toDate, String branch, int serviceId,
			ArrayList<String> emailIdList, Contract contract,String reportName, String reportFormat, Boolean completedFlag,
			Boolean cancelledFlag, Boolean otherFlag, AsyncCallback<Void> callback);
	
	//Ashwini Patil 22-07-2022
	void callSRCopyEmailTaskQueue(long companyId ,Service serviceId,String branchEmail,AsyncCallback<Void> callback);
	void deleteStaticDataFromHeapMemory(AsyncCallback<String> callback);
	void checkHeapMemoryStatus(AsyncCallback<String> callback);
	
	
	void updateWMSServicesFlag(long companyId, AsyncCallback<String> callback);
	
	void updateStock(DeliveryNote model, AsyncCallback<String> callback);

	void CallPaymentGatewayAPI(int documentId, String documentName, String AppURL,String appId, long companyId,int customerId,String pdfURLLink, AsyncCallback<String> callback);

	void updateAddress(Quotation quotationEntity,AsyncCallback<Quotation> callback);
	
	void getSalesRegisterData(MyQuerry quer, Date fromDate, Date toDate, int invoiceid, long companyId, AsyncCallback<ArrayList<SuperModel>>callback);

	void updateStationedContractIntoNormalContract(int contractId, long companyId, AsyncCallback<String> callback);
	void updateNormalContractIntoTechnicianContract(int contractId, long companyId, AsyncCallback<String> callback);
	
	void implementationScreenOperation(String actionTask, long companyId, boolean checkboxValue, AsyncCallback<String> callback);
	
	void enableDisableSelfApproval(ArrayList<ProcessName> list, long companyId, boolean checkboxValue,String processType, AsyncCallback<String> callback);
	void activateDeactivateProcessConfiguration(ArrayList<ProcessTypeDetails> list,long companyId, AsyncCallback<String> callback);

	void activateDeactivateProcessConfigs(String processName, String processType, boolean checkboxValue, long companyId, String name, AsyncCallback<String> callback);

	void validateMaterialQuantity(long companyId, List<ProductGroupList> groupList, AsyncCallback<String> callback);
	
	void getPDFURL(String documentName,SuperModel model, int documentId,long companyId,Company company, String moduleURL,String salaryslipMonth, AsyncCallback<String> callback);
	

	void validateUploadedFileSize(DocumentUpload uploadedFile, long companyId, AsyncCallback<String> callback);

	void sendCTCEmail(Long companyId, ArrayList<String> emailIdList,
			AsyncCallback<Void> callback);
			
	
	
	void updateServiceValue(int contractId,long companyId, AsyncCallback<String> callback);
	
	void getServiceInvoiceDocument(int contractId, long companyId,AsyncCallback<ArrayList<CancelContract>> callback);
	void updateContractDocuments(int contractId, long companyId,boolean serviceCancelFlag,boolean serviceDeleteFlag,boolean invoiceCancelFlag,boolean deleteinvoiceFlag, AsyncCallback<String> callback );

	void getHrProjectOvertimelist(int projectId, long companyId,AsyncCallback<ArrayList<HrProjectOvertime>> callback);
	void updateHrProjectOvertime(ArrayList<HrProjectOvertime> list, String oprationName,AsyncCallback<String> callback);
	void getHrProjectOvertimelist(ArrayList<Integer> projectislist, long companyId,AsyncCallback<ArrayList<HrProjectOvertime>> callback);
	void updateHrProjectOvertimelist(ArrayList<Integer> hrprojectIdlist, long companyId,String operationName,int projectId, AsyncCallback<String> callback);
	
	void updateEmployeeProjectAllocationList(ArrayList<EmployeeProjectAllocation> list, String oprationName, AsyncCallback<String> callback);
	void genrateHRProjectOnProcejectAllocationSave(ProjectAllocation projectAllocation, AsyncCallback<String> callback);

	//Ashwini Patil 15-09-2022
	void deletePaymentDocumentsOnInvoiceReset(int invoiceId, long companyId,String username, AsyncCallback<String> callback);

    void saveNewObservationImpactRecommendation(long companyId, ObservationImpactRecommendation observation, AsyncCallback<AuditObservations> callback);

    void setupPedio(long companyId,String fromLink,String tolink, AsyncCallback<String> callback);

	void deleteInvalidLogin(String userName, long companyId, AsyncCallback<String> callback);
	
	void shareCustomerPortalLink(Long companyId,boolean sms,boolean whatsapp,boolean email, AsyncCallback<Void> callback);

	void checkCustomerPortalLicense(Long companyId,boolean sms,boolean whatsapp,boolean email, AsyncCallback<String> callback);

	void SendContractDataOnEmail(Long companyId, Date fromDate, Date toDate, boolean activeContractsFlag, ArrayList<String> emailIdlist,  String url, AsyncCallback<String> callback);

	void sendSRFormatVersion1CopyOnEmail(Date fromDate, Date toDate, String team, String technicianName, ArrayList<String> toemaillst, long companyId, AsyncCallback<String> callback);

	void createAccountingInterface(SuperModel model, AsyncCallback<String> callback);
	void createSpecificDocumentAccountingInterface(SuperModel model, AsyncCallback<String> callback);

	void updateInvoiceOrderTypeInProduct(Invoice invoiceEntity, AsyncCallback<Invoice> callback);

	void deleteEntryFromAccountingInterface(SuperModel model,AsyncCallback<String> callback);

	void suspendServices(List<Service> servicelist, String suspendRemark, AsyncCallback<String> callback);

	void validateNumberRange(String className, long numberrange, long companyId, AsyncCallback<String> callback);

	void updateAccountingInterface(SuperModel model,AsyncCallback<String> callback);
	
	void deleteAndCreateAccountingInterfaceData(Date fromDate, Date toDate,long companyId,boolean paymentDocflag, AsyncCallback<String> callback);
	 
	void cancelCustomerBranchServices(ArrayList<CustomerBranchDetails> customerbranchlist, long companyId, String Cancellationremark,String loggedinuser,int customerId, AsyncCallback<String> callback);

	void updateCustomerBranchServiceAddressInServices(int customerId,long companyId, AsyncCallback<String> callback);
	
public void getNumberOfHRLicense(long companyId,AsyncCallback<Integer> callback);
	
	public void getNumberOfPayrollProcessedInMonth(long companyId,String salaryPeriod,AsyncCallback<Integer> callback);

}
