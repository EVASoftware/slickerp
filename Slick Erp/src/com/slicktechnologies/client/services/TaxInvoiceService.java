package com.slicktechnologies.client.services;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

@RemoteServiceRelativePath("taxinvoiceservice")
public interface TaxInvoiceService extends RemoteService {

	/**
	 * @author Anil , Date : 16-09-2019
	 * moving invoice creation part from client side to server side
	 * multiple invoice created against single bill
	 */
	public String createTaxInvoice(BillingDocument model , String invoiceType,String invoiceCategory, String invoiceType2, String invoiceGruop,String approverName,String userName,boolean approveInvoiceCreatePaymentDocFlag);
	public String createPayment(Invoice model); //Ashwini Patil Date:19-03-2024 orion reported many approved invoices where payment has not created
	public String validateInvoiceForZohoIntegration(Invoice model);//Ashwini Patil Date:3-05-2024 for Rex - Invoice integration with zoho books
	public String createInvoiceInZohoBooks(int invoiceId,long companyId,String loggedInUser);//Ashwini Patil Date:3-05-2024 for Rex - Invoice integration with zoho books
	public String updateInvoiceInZohoBooks(int invoiceId,long companyId,String loggedInUser);//Ashwini Patil Date:3-05-2024 for Rex - Invoice integration with zoho books
	public String validateCustomerForZohoIntegration(Customer model);//Ashwini Patil Date:6-06-2024 for Rex - customer integration with zoho books
	public String createCustomerInZohoBooks(int custId,long companyId);//Ashwini Patil Date:6-06-2024 for Rex - customer integration with zoho books
	/*
	 * Ashwini Patil 
	 * Date:06-06-2024
	 * Ultima search has uploaded contracts using customer with service details 
	 * but for same company name they put different poc names and due to which, system created multipla customers
	 * now they want to keep one customer and discard duplicate customer
	 * This program will change customer id in all documents such as contract, bill, invoice,payment and service	 
	 */	
	public String UpdateCustomerIdInAllDocs(Customer model,String custIdToBeSet,String loggedInUser);
}
