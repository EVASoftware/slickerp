package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.views.contractrenewal.RenewalResult;
import com.slicktechnologies.client.views.customertraining.CustomerTrainingAttendies;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.sms.SMSDetails;

@RemoteServiceRelativePath("smsservice")
public interface SmsService extends RemoteService {
	
//	Integer sendSmsToClient(String msgTemplate,long clientCell,String accSid, String authtoken, String frmnumber,long companyId);
	Integer sendSmsToClient(String msgTemplate,long clientCell,long companyId);

	Integer sendSmsToCustomers(String msgTemplate,ArrayList<CustomerTrainingAttendies> custlist,String accSid, String authtoken, String frmnumber,boolean bhashSMSAPIFlag);
	Integer sendContractRenewalSMS(String msgTemplate,ArrayList<RenewalResult> custlist,String accSid, String authtoken, String frmnumber,long companyId, boolean bhashSMSAPIFlag);

	/**
	 * Updated By: Viraj
	 * Date: 12-04-2019
	 * Description: changed lead sms send method 
	 * @param companyId
	 * @param cellNumber
	 * @param leadId
	 * @param companyName
	 * @param bhashSMSAPIFlag
	 * @param callback
	 */
	ArrayList<Integer> checkSMSConfigAndSendSMS(long companyId,long cellNumber,long leadId,String companyName,int customerId);

	/**
	 * Date 16-03-2017
	 * added by vijay
	 * for Sending SMS to customer for Contract Cancellation requirement from Petra pest
	 */
	ArrayList<Integer> checkSMSConfigAndSendContractCancellationSMS(long companyId, String customerName, long custCellNo, int contractId, String companyName, boolean bhashSMSAPIFlag,int customerId);
	
	
	/**
	 * Date 17-03-2017
	 * added by vijay
	 * for sending sms to customer for complaint raised requirement from Petra pest
	 */
	ArrayList<Integer> checkSMSConfigAndSendComplaintRegisterSMS(long companyId, String serviceid, String customerName, int complaintId, String companyName, long custCellNo, int customerId);
	

	/**
	 * Date 17-03-2017
	 * added by vijay
	 * for sending sms to customer for complaint Resolved (complated) requirement from Petra pest
	 */
	ArrayList<Integer> checkSMSConfigAndSendComplaintResolvedSMS(long companyId, Date serviceDate, String customerName, int complaintId, String companyName, long custCellNo, int customerId);
	
	
	String validateAndSendSMS(SMSDetails smsDetails);
	/**Added by Sheetal : 15-03-2022
	 * Des : For sending SMS to customer on lead cancelled and lead unsuccessful events**/
	String sendSMSToCustomer(Lead lead);

	String sendMessage(String moduleName, String documentName, String templateName, Long companyId, String actualMsg, long mobileNo,boolean messagefromPopupFlag);

	String checkConfigAndSendContractDownloadOTPMessage(long companyId, String loggedInUser, int otp); //Ashwini Patil Date:4-07-2023
	String checkConfigAndSendServiceDownloadOTPMessage(long companyId, String loggedInUser, int otp); //Ashwini Patil Date:4-07-2023
	String checkConfigAndSendWrongOTPAttemptMessage(long companyId, String loggedInUser, int otp, String documentname); //Ashwini Patil Date:4-07-2023
	String checkEmailConfigAndSendComplaintResolvedEmail(long companyId, Date serviceDate, String customerName, int complaintId, String companyName, long custCellNo, int customerId);//Ashwini Patil Date:10-08-2023
	String sendMessageOnWhatsApp(long companyId, String mobileNo,String actualMessage);//15-01-2024 
}

