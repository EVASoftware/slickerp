package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.common.CustomerTrainingDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.email.EmailDetails;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.LoggedIn;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.supportlayer.RaiseTicket;
import com.slicktechnologies.shared.common.workorder.WorkOrder;

public interface EmailServiceAsync {
//	 public void initiateEmail(Sales q,AsyncCallback<Void> callback);
	 public void initiateRFQEmail(RequsestForQuotation r,AsyncCallback<Void> callback);
//	 public void initiateSalesEmail(Sales salesq,AsyncCallback<Void> callback);
	 public void initiatePrEmail(PurchaseRequisition pr, AsyncCallback<Void> callback);
	 public void initiateSalesQuotationEmail(Sales salesquot, AsyncCallback<Void> callback);
	 public void initiateSalesOrderEmail(Sales salesord, AsyncCallback<Void> callback);
	 public void initiateServiceQuotationEmail(Sales servquot, AsyncCallback<Void> callback);
	 /**
	  * Updated Viraj 
	  * Date 15-11-2018
	  * Description to pass sales object for email
	  */
	 public void passServiceQuotationobject(Sales servquot, AsyncCallback<Void> callback);
	 
	 public void initiateServiceContractEmail(Sales servcont, AsyncCallback<Void> callback);
	 public void initiateLOIEmail(LetterOfIntent loi, AsyncCallback<Void> callback);
	 public void initiatePOEmail(PurchaseOrder po, AsyncCallback<Void> callback);
	 public void initiateMRNEmail(MaterialRequestNote mrn, AsyncCallback<Void> callback);
	 public void initiateMINEmail(MaterialIssueNote min, AsyncCallback<Void> callback);
	 public void initiateGRNEmail(GRN grn, AsyncCallback<Void> callback);
	 public void initiateMMNEmail(MaterialMovementNote mmn, AsyncCallback<Void> callback);
	 public void initiateSupportEmail(RaiseTicket ticket, AsyncCallback<Void> callback);
	 public void initiateInteractionEmail(InteractionType interaction, AsyncCallback<Void> callback);
	 public void initiateApprovalEmail(Approvals approve, AsyncCallback<Void> callback);
	 public void intiateOnApproveRequestEmail(Approvals approve, AsyncCallback<Void> callback);
	 public void initiateInvoiceEmail(Invoice invDetails, AsyncCallback<Void> callback);
	 public void initiateDeliveryNoteEmail(DeliveryNote deliveryNoteDetails, AsyncCallback<Void> callback);
	 public void initiateInspectionEmail(Inspection inspection, AsyncCallback<Void> callback);
	 public void initiateWorkOrderEmail(WorkOrder wo, AsyncCallback<Void> callback);
	 public void initiateCustomerListEmail(CustomerTrainingDetails custlis, AsyncCallback<Void> callback);
	 public void initiatePaymentReceiveEmail(CustomerPayment custPay, AsyncCallback<Void> callback);
	 public void initiateFumigationEmail(Fumigation fumigation, AsyncCallback<Void> callback);
	
	 
	 // rohan added this 
	 public void initiateLoginInfoEmail(User user, AsyncCallback<Void> callback);
	 /**
	  * For Ip Address Authorization
	  */
	 public void initiateIPAddressAuthorizationEmail(String userId,String passward,String ipaddress,String compEmail, AsyncCallback<Void> callback);
	 public void initiateContractRenewalEmail(ContractRenewal conRenList, AsyncCallback<Void> callback);
	 public void initiateRequestServiceEmail(Long companyId,Date date,String desc,PersonInfo info,int complainNo, AsyncCallback<Void> callback);
	 public void requestForApprovalEmailForMultilevelapproval(Approvals approve, ArrayList<String> toEmaiEmployeelList, ArrayList<String> ccEmailEmployeeList, AsyncCallback<Void> asyncCallback);
	 public void emailForMultilevelapproval(Approvals approve, ArrayList<String> toEmaiEmployeelList, ArrayList<String> ccEmailEmployeeList, AsyncCallback<Void> asyncCallback);
	 
	 /**
	  * rohan added this code for sending Email of lead to sales person  from lead screen
	  * Date : 20/12/2016
	  */

	 public void emailToSalesPersonFromLead(Lead lead, AsyncCallback<Void> asyncCallback);
	 
	 /**
	  * ends here 
	  */
	 
	 /**
	  * Common method to send mail
	  * Date : 14-10-2016 By Anil
	  * Release : 30 Sept 2016 
	  * Project: Purchase Modification (NBHC)
	  */
	 public void sendEmail(String mailSub,String mailMsg,ArrayList<String> toEmailLis,ArrayList<String> ccEmailLis,String fromEmail, AsyncCallback<Void> asyncCallback );


	 /**
	  * Send Service PO Mail to Vendor
	  * Date : 18-10-2016 By Anil
	  * Release : 30 Sept 2016 
	  * Project: Purchase Modification (NBHC)
	  */
	 
	 public void initiateServicePoEmail(ServicePo po, AsyncCallback<Void> asyncCallback);
	 
	 /**
	  * Date 27 Feb 2017
	  * by vijay
	  * for Send Email from Customer support To complaint Assigned 
	  */
	 
	 public void emailSend(long companyId,Complain model, ArrayList<String> toEmail,String mailSubject,String msgBody,ArrayList<String> table1_Header,ArrayList<String> table1, AsyncCallback<Void> asyncCallback );

	 /**
	  *  Created By : nidhi
	  *  Date : 20-11-2017
	  *  for NBHC send email introductory info to Customer
	  *  
	  */
	 public void emailToCustomerFromLead(Lead lead, AsyncCallback<Void> asyncCallback );
	 public void sendSalarySlipOnMail(PaySlip payslip,AsyncCallback<Void>asyncCallback);
	 public void initiateContractRenewalNewEmail(Contract contract, AsyncCallback<Void> callback);

	 /**
	  * @author Anil
	  * @Since 02-06-2020
	  * Complain Email
	  */
	 public void initiateComplainEmail(Complain complain, AsyncCallback<Void> callback);
	 public void sendCompanyProfileToLead(Lead lead, AsyncCallback<String> asyncCallback );
	 
	 
	 public void sendEmail(EmailDetails emailDetails, long companyId,  AsyncCallback<String> callback);
	 
	 /**Sheetal : 08-04-2022 , for sending email on resetting password**/
	 public void sendEmailOnPasswordReset(User userEntity, long companyId, AsyncCallback<String> callback);
	 public void sendEmailForIPAuthorisation(String userID, String ipaddress,long companyId,AsyncCallback<Void> callback);
	 public void sendEmailOnIncorrectPassword(LoggedIn loggedIn, String userID, long companyId, AsyncCallback<Void> callback);

}
