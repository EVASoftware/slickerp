package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.BOMServiceListBean;
import com.slicktechnologies.shared.Service;


public interface ServerUnitConversionServicesAsync {
	
	void getServiceBOMReport(Long companyId, Date date,Date endDate, String branch , AsyncCallback<BOMServiceListBean> callBack) ;
	
	void getServiceGetUpdate(BOMServiceListBean bomBean, Long company , AsyncCallback<BOMServiceListBean> callBack);
	void getServiceGetWithMrnUpdate(BOMServiceListBean bomBean,Long company, AsyncCallback<BOMServiceListBean> callBack);
}
