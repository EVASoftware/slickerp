package com.slicktechnologies.client.services;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("sendgridemailservice")
public interface SendGridEmailService extends RemoteService {
	public void sendGridEmail(String str);
}
