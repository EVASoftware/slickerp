package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.common.CustomerTrainingDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.email.EmailDetails;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.LoggedIn;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.supportlayer.RaiseTicket;
import com.slicktechnologies.shared.common.workorder.WorkOrder;

@RemoteServiceRelativePath("emailservice")
public interface EmailService extends RemoteService {
//	 public void initiateEmail(Sales q) throws IllegalArgumentException;
	 public void initiateSalesQuotationEmail(Sales salesquot) throws IllegalArgumentException;
	 public void initiateSalesOrderEmail(Sales salesord) throws IllegalArgumentException;
	 public void initiateServiceQuotationEmail(Sales servquot) throws IllegalArgumentException;
	 /**
	  * Updated Viraj 
	  * Date 15-11-2018
	  * Description to pass sales object for email
	  */
	 public void passServiceQuotationobject(Sales servquot) throws IllegalArgumentException;
	 
	 public void initiateServiceContractEmail(Sales servcont) throws IllegalArgumentException;
	 public void initiateRFQEmail(RequsestForQuotation r) throws IllegalArgumentException;
//	 public void initiateSalesEmail(Sales salesq) throws IllegalArgumentException;
	 public void initiatePrEmail(PurchaseRequisition pr) throws IllegalArgumentException;
	 public void initiateLOIEmail(LetterOfIntent loi) throws IllegalArgumentException;
	 public void initiatePOEmail(PurchaseOrder po) throws IllegalArgumentException;
	 public void initiateMINEmail(MaterialIssueNote min) throws IllegalArgumentException;
	 public void initiateMRNEmail(MaterialRequestNote mrn) throws IllegalArgumentException;
	 public void initiateGRNEmail(GRN grn) throws IllegalArgumentException;
	 public void initiateMMNEmail(MaterialMovementNote mmn) throws IllegalArgumentException;
	 public void initiateSupportEmail(RaiseTicket ticket) throws IllegalArgumentException;
	 public void initiateInteractionEmail(InteractionType interaction) throws IllegalArgumentException;
	 public void initiateApprovalEmail(Approvals approve) throws IllegalArgumentException;
	 public void intiateOnApproveRequestEmail(Approvals approve) throws IllegalArgumentException;
	 public void initiateInvoiceEmail(Invoice invDetails) throws IllegalArgumentException;
	 public void initiateDeliveryNoteEmail(DeliveryNote deliveryNoteDetails) throws IllegalArgumentException;
	 public void initiateInspectionEmail(Inspection inspection) throws IllegalArgumentException;
	 public void initiateWorkOrderEmail(WorkOrder workOrder) throws IllegalArgumentException;
	 public void initiateCustomerListEmail(CustomerTrainingDetails custList) throws IllegalArgumentException;

	 
	 public void initiatePaymentReceiveEmail(CustomerPayment custPay) throws IllegalArgumentException;
	 
	 public void initiateFumigationEmail(Fumigation fumigation) throws IllegalArgumentException;
	 
	 //   rohan added this 
	 
	 public void initiateLoginInfoEmail(User user) throws IllegalArgumentException;
	 
	 
	 /**
	  * For Ip Address Authorization
	  */
	 
	 public void initiateIPAddressAuthorizationEmail(String userID,String passward,String ipaddress,String compEmail) throws IllegalArgumentException;

	 public void initiateContractRenewalEmail(ContractRenewal conRenList) throws IllegalArgumentException;
	 public void initiateRequestServiceEmail(Long companyId,Date date,String desc,PersonInfo info,int complainNo)throws IllegalArgumentException;
	 
	 public void requestForApprovalEmailForMultilevelapproval(Approvals approve,ArrayList<String> toEmaiEmployeelList, ArrayList<String> ccEmailEmployeeList)throws IllegalArgumentException;
	 public void emailForMultilevelapproval(Approvals approve, ArrayList<String> toEmaiEmployeelList, ArrayList<String> ccEmailEmployeeList)throws IllegalArgumentException;

	 ///    lead email to sales person
	 public void emailToSalesPersonFromLead(Lead lead)throws IllegalArgumentException; 
	 
	 /**
	  * Common method to send mail
	  * Date : 14-10-2016 By Anil
	  * Release : 30 Sept 2016 
	  * Project: Purchase Modification (NBHC)
	  */
	 public void sendEmail(String mailSub,String mailMsg,ArrayList<String> toEmailLis,ArrayList<String> ccEmailLis,String fromEmail)throws IllegalArgumentException;

	 /**
	  * Send Service PO Mail to Vendor
	  * Date : 18-10-2016 By Anil
	  * Release : 30 Sept 2016 
	  * Project: Purchase Modification (NBHC)
	  */
	 
	 public void initiateServicePoEmail(ServicePo po)throws IllegalArgumentException;

	 /**
	  * Date 27 Feb 2017
	  * by vijay
	  * for Send Email from Customer support To complaint Assigned 
	  * Email in html context
	  */
	 
	 public void emailSend(long companyId,Complain model, ArrayList<String> toEmail,String mailSubject,String msgBody,ArrayList<String> table1_Header,ArrayList<String> table1 );

	 /**
	  *  Created By : nidhi
	  *  Date : 20-11-2017
	  *  for NBHC send email introductory info to Customer
	  *  
	  */
	 public void emailToCustomerFromLead(Lead lead);
	 
	 public void sendSalarySlipOnMail(PaySlip payslip);
	 /**
	  *  Created By : Amol
	  *  Date : 23-3-2020
	  *  for om pest send email new format of renewal letter.
	  *  
	  */
	 public void initiateContractRenewalNewEmail(Contract contract) throws IllegalArgumentException;
	 /**
	  * @author Anil
	  * @Since 02-06-2020
	  * Complain Email
	  */
	 public void initiateComplainEmail(Complain complain);
	 /**Date 14-11-2020 by Amol to send company profile to lead**/
	 public String sendCompanyProfileToLead(Lead lead);
	 
	 
	 public String sendEmail(EmailDetails emailDetails, long companyId);
	 
	 /**Sheetal : 08-04-2022 , for sending email on resetting password**/
	 public String sendEmailOnPasswordReset(User userEntity, long companyId);
	 public void sendEmailForIPAuthorisation(String userID, String ipaddress,long companyId);
	 public void sendEmailOnIncorrectPassword(LoggedIn loggedIn,String userID,long companyId);

}
