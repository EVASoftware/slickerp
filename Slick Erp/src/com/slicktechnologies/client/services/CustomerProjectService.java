package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;

@RemoteServiceRelativePath("custprojectserv")
public interface CustomerProjectService extends RemoteService {
	Integer createCustomerProject(long companyId,int contractCount,int serviceCount,String serviceStatus);
	Integer checkCustomerProject(long companyId, int contractCount,int serviceCount,String serviceStatus,ArrayList<EmployeeInfo> technicians,Service serEntity,List<ProductGroupList> proGroupList,int serProjectID);
	ArrayList<EmployeeInfo> projectTechnicianList(long companyId, int contractCount,int serviceCount,String serviceStatus);

}
