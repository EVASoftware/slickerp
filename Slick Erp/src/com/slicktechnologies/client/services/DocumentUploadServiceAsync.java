package com.slicktechnologies.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public interface DocumentUploadServiceAsync {
	
	/**
	 * @author Vijay Chougule
	 * Des :- 18-10-2019 for NBHC CCPM Contract Upload
	 */
	void readContractUploadExcelFile(String operationName,long companyId, AsyncCallback<ArrayList<String>> contractuploadexcel);
	void reactonValidateContractUpload(ArrayList<String> contractExcellist, long companyId, AsyncCallback<ArrayList<String>> contractuploadexcel);
	void reactonContractUpload(String blobkey, long companyId, AsyncCallback<String> callback);

	/**
	 * Date 22-10-2019 by vijay
	 * Des :- NBHC Auto submit Billing sending request for approval
	 */
	void sendApprovalRequest(Invoice invoiceEntity,String userName,int invoiceId ,AsyncCallback<String> callback);
	
	/**
	 * Date 21-11-2019 by Vijay
	 * Des :- NBHC Inventory Management Lock seal GRN MMN
	 * Reading Excel File
	 */
	void readexcelFile(AsyncCallback<ArrayList<String>> asyncCallback);
	void uploadGRN(long companyId,ArrayList<String>excelData,AsyncCallback<ArrayList<String>> asyncCallback);
	void uploadMMN(long companyId,ArrayList<String>excelData,AsyncCallback<String> asyncCallback);
	/**
	 * ends here
	 */
	/**
	 * Date 27-11-2019
	 * Des :- NBHC Lock Seal IM Stock update directly
	 */
	void uploadLockSealStockUpdate(long companyId,AsyncCallback<String> asyncCallback);
	
	/**
	 * Date 28-11-2019
	 */
	void updateLockSealSerialNumberMaster(long companyId,String serialNumber,AsyncCallback<String> asyncCallback);

       void reactOnPurchaseRequisitionExcelRead(String entityName, Long companyId,
			String blobkeyURL, AsyncCallback<ArrayList<String>> asyncCallback);
	void reactOnPurchaseRequisitionExcelValidation(boolean masterPriceFlag, ArrayList<String> excelData,
			Long companyId, AsyncCallback<ArrayList<String>> asyncCallback);
	void reactOnPurchaseRequisitionExcelUpload(boolean masterPriceFlag, String string, Long companyId,
			AsyncCallback<String> asyncCallback);
              

	/**
	 * @author Vijay Chougule Date 10-07-2020
	 * Des :- To Update inhouse fumigation services properly in fumigation report and if any services missing it also create the same
	 */
	void updateInhouseFumigationServices(long companyId,AsyncCallback<String> asyncCallback);

	/**
 	 * @author Vijay Date :- 13-10-2020
 	 * Des :- Payment Upload
 	 */
	void validatePaymentUploader(long companyId, AsyncCallback<ArrayList<CustomerPayment>> asyncCallback);
 	void uploadPaymentDetails(long comapnyId, String taskName, AsyncCallback<String> asyncCallback);

 	/**
 	 * @author Vijay Date :- 02-10-2020
 	 * Des :- Tally Payment Uploader Payment Upload
 	 */
	void validateTallyPaymentUploader(long companyId, AsyncCallback<ArrayList<CustomerPayment>> asyncCallback);
 	void uploadTallyPaymentDetails(long comapnyId, String taskName, AsyncCallback<String> asyncCallback);

	void validateServiceCancellationUpload(long companyId,String loggedInUser, AsyncCallback<ArrayList<Service>> asyncCallback);
	void customerBranchUpload(long companyId, AsyncCallback<String> asyncCallback);
	
	void validateCustomerBranchStatusUpdation(long company,boolean cancelServicesFlag,AsyncCallback<Integer> asyncCallback);
	void updateCustomerBranchStatus(long company,boolean cancelServicesFlag,AsyncCallback<String> asyncCallback);
	void UpdateZohoCustomerId(AsyncCallback<ArrayList<String>> asyncCallback);
	void ReverseServiceCompletionAndRemoveDuplicate(AsyncCallback<ArrayList<String>> asyncCallback);


}
