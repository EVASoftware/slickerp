package com.slicktechnologies.client.services;
import java.util.ArrayList;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.Service;
public interface TechnicianDashboardServiceAsync {

	void getReWorkdetails(long companyId, ArrayList<Service> servicelist, AsyncCallback<Integer> callback);
}
