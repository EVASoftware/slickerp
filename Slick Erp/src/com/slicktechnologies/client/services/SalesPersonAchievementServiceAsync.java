package com.slicktechnologies.client.services;
import java.util.ArrayList;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.common.salesperson.TargetInformation;


public interface SalesPersonAchievementServiceAsync {

	void getAchievementDetails ( int employeeId, String financialYear,long companyId,String employeeeName, AsyncCallback<ArrayList<TargetInformation>> callback);

	void getProductCategoryDetails (String financialYear,String productCategory,long companyId, AsyncCallback<ArrayList<TargetInformation>> callback);
	
	void getemployeeRoleDetails(String financialYear,String employeeRole,long companyId, AsyncCallback<ArrayList<TargetInformation>> callback);
}
