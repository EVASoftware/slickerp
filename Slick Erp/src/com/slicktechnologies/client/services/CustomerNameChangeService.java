package com.slicktechnologies.client.services;
import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.personlayer.Employee;
@RemoteServiceRelativePath("customernamechangeservice")
public interface CustomerNameChangeService extends RemoteService{

	ArrayList<Integer> SaveCustomerChangeInAllDocs(Customer model, boolean updateCustomerAddressFlag);
	
	/**Date 26-4-2019 
	 * @author Amol
	 * Added a method for to update the cell no of Employee when we updated it in employee master and it 
	 * should be reflect in salary slip ,CTC and Leave balance
	 *
	 */
	ArrayList<Integer> SaveEmployeeChangeInAllDocs(Employee model);
	
	/**
	 * Date 22-05-2019 by Vijay For Complaint service creation
	 */
	String createComplaintService(Complain complain,int complaintId);
	
	/**
	 * Date 04-11-2019 By Vijay for NBHC CCPM Service Revenue loss Report. Approved Contract Cancelled Services
	 */
	String getCancelledServiceOfApprovedContract(long companyId, Date fromDate, Date toDate,boolean flag, ArrayList<String> brnachlist,ArrayList<String>emailIdList);
	
	String createServiceAssesmentForComplain(Complain complain);
	
	String updateCustomerEmailIdInContract(long companyId,Date fromDate,Date toDate);
	/**Date 18-12-2019 by Amol to update service status to open**/
	String updateServicesStatusOpen(long companyId);
	/**Date 10-9-2020 by Amol for invoice prefix details**/
	String getInvoicePrefixDetails(long companyId,String branchName);
	
	String updateCustomerBranchServiceAddress(long companyId,int contractId,String customerBranch);
	
	String updateCustomerServiceAddress(long companyId, int customerId);
	ArrayList<Integer> updateVendor(Vendor model);
	
}
