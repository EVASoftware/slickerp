package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.CommodityFumigationDetails;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.inventory.MaterialConsumptionReport;


@RemoteServiceRelativePath("xlsxService")
public interface XlsxService  extends RemoteService{
	
	public void setServiceFumigatioList(ArrayList<CommodityFumigationDetails> qarray);
	public void setServiceValueList(ArrayList<CommodityFumigationDetails> qarray);
	/** date 08.08.2018 added by komal for cost sheet download **/
	public void setCostSheetData(CNC cnc );
	
	/*****31-1-2019 added by Amol for Payroll Sheet Download **/
	public void setPayrollSheetData(CNC cnc1);

	
	/** nidhi *:*:* */
	public void setBomServiceList(ArrayList<Service> serList);
//	public void setContractPNLReport(ArrayList<MaterialConsumptionReport> reportList,String Date,String segment);
	public void setContractPNLReport(ArrayList<MaterialConsumptionReport> reportList, Date fromdate, Date toDate,
			String segment1, long companyId);
	/**
	 * Updated By: Viraj
	 * Date: 13-06-2019
	 * Description: To set from date and to date for displaying in download
	 */
	public void setServiceFromToDate(Date fromdate, Date toDate);
}
