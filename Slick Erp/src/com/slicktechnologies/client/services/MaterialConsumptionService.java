package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.inventory.ExpenseCost;
import com.slicktechnologies.shared.common.inventory.LabourCost;
import com.slicktechnologies.shared.common.inventory.MaterialConsumptionReport;

@RemoteServiceRelativePath("consumptionserv")
public interface MaterialConsumptionService extends RemoteService {
	ArrayList<MaterialConsumptionReport> retrieveMinList(long companyId,int contractCount);
	ArrayList<MaterialConsumptionReport> retrieveMmnList(long companyId,int contractCount);
	
	ArrayList<LabourCost> retrieveLabourCost(long companyId, String contractid,String typeOfPnL, Date fromDate, Date toDate);
	ArrayList<ExpenseCost> retrieveExpenseCost(long companyId, String contractid,String typeOfPnL, Date fromDate, Date toDate);
	
	ArrayList<String> retrieveAdministrativeCost(long companyId,String contractdate);
	
	/**
	 * rohan added this method to calculate contract p & l customer and contract wise
	 * @param companyId --  company id
	 * @param parseInt  --  customer ID in case of Customer wise and contract id in case of contract wise
	 * @param string    --  this is how you want to calculate P & L
	 */
	ArrayList<MaterialConsumptionReport> retriveMaterialDetails(long companyId,String conOrCustCount, String typeOfPnL, Date fromDate, Date toDate);
	
	/**
	 * rohan added this method to calculate contract p & l customer and contract wise
	 * @param companyId --  company id
	 * @param parseInt  --  customer ID in case of Customer wise and contract id in case of contract wise
	 * @param string    --  this is how you want to calculate P & L
	 */
	ArrayList<MaterialConsumptionReport> retriveMaterialMMNDetails(long companyId,String conOrCustCount, String typeOfPnL,Date fromDate, Date  toDate);

	ArrayList<String>  retrieveAdministrativeCost(long companyId,String contractOrCustCount, String type, Date fromDate, Date toDate);
	
	/**
	 * nidhi 10-01-2019 |*) 
	 */
	ArrayList<MaterialConsumptionReport> retriveMaterialDetailsBaseOnBOM(long companyId,String conOrCustCount, String typeOfPnL, Date fromDate, Date toDate);
	
	ArrayList<MaterialConsumptionReport> retriveMaterialMMNDetailsBaseOnBOM(long companyId,String conOrCustCount, String typeOfPnL,Date fromDate, Date  toDate);

	ArrayList<String>  retrieveAdministrativeCostBaseOnBOM(long companyId,String contractOrCustCount, String type, Date fromDate, Date toDate);

	ArrayList<MaterialConsumptionReport> retriveServicePlannedRevanueAndBillingRevanue(ArrayList<MaterialConsumptionReport> ServiceContractDetails
			,HashSet<Integer> contract,HashSet<Integer> serSet ,HashMap<Integer,HashSet<Integer>> contSer,long companyID);

	ArrayList<MaterialConsumptionReport> retrivelabourPlannedRevanueAndBillingRevanue(ArrayList<MaterialConsumptionReport> ServiceContractDetails
			,HashSet<Integer> contract,HashSet<Integer> serSet ,HashMap<Integer,HashSet<Integer>> contSer,long companyID);
	/** DATE 8.4.2019 added by komal for new contract p and l **/
	public ArrayList<MaterialConsumptionReport> retrieveServiceDetails(long companyId,String contractid,String type, Date fromDate, Date toDate,ArrayList<String> emailList);
}
