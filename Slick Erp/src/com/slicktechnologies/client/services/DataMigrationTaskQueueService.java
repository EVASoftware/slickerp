package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.attendance.AttandanceInfo;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.attendance.EmployeeLeaveBalance;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProjectOvertime;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;

@RemoteServiceRelativePath("datamigrationtaskqueueserv")
public interface DataMigrationTaskQueueService extends RemoteService{

	ArrayList<Integer> saveRecords(long id, String entityName);
	
	ArrayList<AttandanceInfo> saveAttandance(long id, String entityName);
	
	ArrayList<AttandanceInfo> validateAttandance(ArrayList<AttandanceInfo> attandanceList);
	
	ArrayList<AttandanceInfo> updateAttandance(ArrayList<AttandanceInfo> attandanceList);
	
	/**
	 * Date : 11-10-2018 By ANIL
	 */
	Attendance updateAndOverrideAttendance(Attendance attendance);

	/**
	 * Date : 24-12-2018 BY ANIL
	 * @param companyId
	 * @return
	 */
//	HashMap<Integer,ArrayList<com.slicktechnologies.shared.common.attendance.EmployeeLeaveBalance>> getEmployeeLeaveDetails(long companyId);
//	HashMap<Integer,ArrayList<com.slicktechnologies.shared.common.attendance.EmployeeLeaveBalance>> validateEmployeeLeaveDetails(HashMap<Integer,ArrayList<com.slicktechnologies.shared.common.attendance.EmployeeLeaveBalance>> empLeaveMap);
//	String updateLeaveBalance(HashMap<Integer,ArrayList<com.slicktechnologies.shared.common.attendance.EmployeeLeaveBalance>> empLeaveMap);
	
	
	ArrayList<EmployeeLeaveBalance> getEmployeeLeaveDetails(long companyId);
	ArrayList<EmployeeLeaveBalance> validateEmployeeLeaveDetails(ArrayList<EmployeeLeaveBalance> empLeaveMap);
	String updateLeaveBalance(ArrayList<EmployeeLeaveBalance> empLeaveMap);
	/**
	 * End
	 */

	ArrayList<String> readStockUpdateExcel(String entityNameStockUpdate,
			Long companyId, String bloburl);

	ArrayList<String> reactOnStockUpdateValidate(
			ArrayList<String> stockupdateExcel, Long companyId);

	String stockUpdateExcelUpload(String string, Long companyId);
	
	/**
	 * @author Vijay
	 * Des :- Attendance upload all validation part at client side
	 */
	ArrayList<SuperModel> validateAttandancepart2(ArrayList<AttandanceInfo> attandanceList);
	ArrayList<SuperModel> validateAttandancepart3(ArrayList<AttandanceInfo> attandanceList,ArrayList<Integer> empList, ArrayList<Integer> projectidlist);

	ArrayList<LeaveBalance> loadLeaveBalanceEntity(ArrayList<Integer> empList, long companyId);
	ArrayList<EmployeeInfo> loadEmployeeInfoEntity(ArrayList<Integer> empList, long companyId);
	ArrayList<Attendance> loadExistingAttendance(ArrayList<AttandanceInfo> attandanceList, ArrayList<Integer> empList,ArrayList<String> project);
	ArrayList<HrProject> loadHRprojectEntity(ArrayList<String> projList, long companyId);
	ArrayList<HrProjectOvertime> loadHRprojectOvertimeEntity(ArrayList<AttandanceInfo> attandanceList, ArrayList<Integer> empList, ArrayList<Integer> projectidlist);

	
}
