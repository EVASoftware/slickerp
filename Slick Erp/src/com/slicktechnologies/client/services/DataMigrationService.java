package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.ContractUploadDetails;
import com.slicktechnologies.shared.CreditNote;
import com.slicktechnologies.shared.EmployeeAsset;
import com.slicktechnologies.shared.MINUploadExcelDetails;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.StackMaster;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorPriceListDetails;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.cnc.Consumables;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchServiceLocation;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.servicerelated.AssetMovementInfo;

@RemoteServiceRelativePath("datamigrationsev")
public interface DataMigrationService extends RemoteService{

	/**
	 * Save Flag is added 
	 * Date:22-10-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project :Purchase Modification(NBHC)
	 */
	ArrayList<Integer> savedRecordsDeatils(long id,String entityname,boolean saveFlag, String loggedinuser);
	
//	ArrayList<Integer> savedRecordsDeatils(long id, String entityname);
	ArrayList<Integer> savedConfigDetails(long id, String entityname);
	
	ArrayList<Integer> savedTransactionDetails(long id, String entityname);
	
	/**
	 *  nidhi
	 *   for contraact service cycle upload process
	 * @param id
	 * @param entityname
	 * @param serviceStatus
	 * @param billingStatus
	 * @param loginPerson
	 * @return
	 */
	ArrayList<String> savedContractTransactionDetails(long id, String entityname,Boolean serviceStatus, Boolean billingStatus, String loginPerson);
	MINUploadExcelDetails saveMinUploadProcess(MINUploadExcelDetails MinListDetails);
	MINUploadExcelDetails savedUploadTransactionDetails(long id, String entityname,Boolean serviceStatus, Boolean billingStatus, String loginPerson);
	/**
	 *  end
	 */
	

	MINUploadExcelDetails checkMinUploadServiceListDetails(MINUploadExcelDetails minDetails);
	MINUploadExcelDetails ServiceUploadToCompleteStatus(MINUploadExcelDetails minDetails);
/* nidhi commented for new upload process 
 * 26-03-2018
 * 	ArrayList<String> CheckUploadContractDetail(long companyId);
	ArrayList<String> UploadContractDetails(long companyId);
	ArrayList<String> finallyUploadContractDetails(long companyId);*/
	MINUploadExcelDetails updateProductInventoryViewList(MINUploadExcelDetails minDetailsMap);
	
	
	ContractUploadDetails UploadContractDetails(ContractUploadDetails contractDt);
	ArrayList<String> finallyUploadContractDetails(ContractUploadDetails contractDt);
	ContractUploadDetails savedContractUploadTransactionDetails(long id, String entityname, String loginPerson);
	ContractUploadDetails checkContractUploadDetails(ContractUploadDetails contDetails);
	ContractUploadDetails CheckUploadContractDetail(ContractUploadDetails contDetails);
	
	/**
	 * @author Anil , Date : 22-07-2019
	 * @param companyId
	 * @return
	 */
	ArrayList<EmployeeAsset> saveEmployeeAsset(long companyId);
	ArrayList<EmployeeAsset> validateEmployeeAsset(ArrayList<EmployeeAsset> list);
	ArrayList<EmployeeAsset> uploadEmployeeAsset(ArrayList<EmployeeAsset> list);
	ArrayList<CustomerBranchDetails> uploadCustomerBranch(long companyId);
	String saveCustomerBranch(ArrayList<CustomerBranchDetails> list);
	
	/**
	 * @author Vijay Chougule Date :- 16-07-2020
	 * Des :- Vendor Product Price Upload
	 */
	ArrayList<VendorPriceListDetails> uploadVendorProductPrice(long companyId);
	String vednorProductPrice(long companyId,String taskName);

	/**
	 * @author Vijay Chougule Date :- 29-07-2020
	 * Des :- CTC Template Upload
	 */
	ArrayList<CTCTemplate> validateCTCTemplate(long companyId);
	String uploadCTCTemplate(long companyId, String taskName);
	/**
	 * ends here
	 */

	/**
	 * @author Vijay Chougule Date - 28-09-2020
	 * Des :- To Update Assest Details in service requirement raised by Rahul Tiwari for PTSPL
	 */
	ArrayList<AssetMovementInfo> validateUpdateAssetDetails(long companyId);
	String uploadAssetDetails(long companyId,String taskName);
	/**
	 * ends here
	 */
	
	ArrayList<CTCTemplate> validateCTCAllocation(long companyId,String loggedInUser);
	
	ArrayList<CreditNote> validateAndUploadCreditNote(long companyId);
	
	ArrayList<StackMaster> validateAndUploadInhouseSerivces(long companyId,  ArrayList<String> strexcellist);
	ArrayList<String> validateInhouseSerivces(long companyId);

	ArrayList<ProductInventoryView> validateStockUpload(long companyId);
	
	/**
	 * @author Anil @since 11-11-2021
	 * Uploading consumables of CNC
	 * Sunrise SCM
	 */
	ArrayList<Consumables> validateConsumables(long companyId,int customerId);
	
	/**Added by sheetal:31:01:2022 
	 * Des: for user creation upload program**/
	ArrayList<Employee> validateAndUploadUser(long companyId);
	
	ArrayList<Service> validateAndUploadServiceCreation(long companyId);
	
	/**@author Sheetal : 02-06-2022
	 * Des : Upload program for customer branch with service location and area**/
	ArrayList<CustomerBranchServiceLocation> uploadCustomerBranchWithLocationandArea(long companyId);

}
