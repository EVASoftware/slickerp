package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.views.approval.MaterialInfo;
import com.slicktechnologies.client.views.contractrenewal.RenewalResult;
import com.slicktechnologies.client.views.customerDetails.ContractTableValueProxy;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.client.views.inventory.productinventorytransaction.ClosingReport;
import com.slicktechnologies.client.views.salesperformance.SalesPersonValue;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.CreditNote;
import com.slicktechnologies.shared.EmployeeAsset;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.ServiceInvoiceDetails;
import com.slicktechnologies.shared.TallyInterfaceBean;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.BillUnbilledAr;
import com.slicktechnologies.shared.common.CompanyAssetListArticleInformation;
import com.slicktechnologies.shared.common.PettyCashTransaction;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.RoleDefinition;
import com.slicktechnologies.shared.common.android.RegisterDevice;
import com.slicktechnologies.shared.common.arrears.ArrearsDetails;
import com.slicktechnologies.shared.common.attendance.AttandanceError;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.attendance.AttendanceBean;
import com.slicktechnologies.shared.common.billofproductmaterial.BillOfProductMaterial;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorPriceListDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cancelsummary.CancelSummary;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.documenthistorydetails.DocumentHistory;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.LoggedIn;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.VoluntaryPf;
import com.slicktechnologies.shared.common.humanresourcelayer.VoluntaryPfHistory;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Loan;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.EmployeeLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveApplication;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CompanyPayrollRecord;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.EmployeeShift;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.EmployeeOvertime;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReport;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.inventory.ExpenseCost;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.inventory.LabourCost;
import com.slicktechnologies.shared.common.inventory.MaterialConsumptionReport;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.inventory.PhysicalInventoryMaintaince;
import com.slicktechnologies.shared.common.inventory.PhysicalInventoryProducts;
import com.slicktechnologies.shared.common.inventory.ProductInventoryTransaction;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.paymentmodelayer.vendorpayment.VendorPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesperson.SalesPersonTargets;
import com.slicktechnologies.shared.common.salesperson.TargetInformation;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.Liaison;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ToolGroup;
import com.slicktechnologies.shared.common.smshistory.SmsHistory;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;
import com.slicktechnologies.shared.common.workorder.WorkOrder;



// TODO: Auto-generated Javadoc
/**
 * Provides add Hoc Download Functionality.
 */
@RemoteServiceRelativePath("csvservice")
public interface CsvService extends RemoteService{
	
	/**
	 * Sets the customerlist.
	 *
	 * @param array the new customerlist
	 * @throws IllegalArgumentException the illegal argument exception
	 */
	public void setcustomerlist(ArrayList<Customer> array)throws IllegalArgumentException;
	
	/**
	 * Sets the quotationlist.
	 *
	 * @param qarray the new quotationlist
	 */
	public void setquotationlist(ArrayList<Quotation> qarray);
	
	/**
	 * Sets the leadslist.
	 *
	 * @param larray the new leadslist
	 * @throws IllegalArgumentException the illegal argument exception
	 */
	public void setleadslist(ArrayList<Lead> larray) throws IllegalArgumentException;
	
	/**
	 * Sets the expenselist.
	 *
	 * @param earray the new expenselist
	 * @throws IllegalArgumentException the illegal argument exception
	 */
	public void setexpenselist(ArrayList<Expense> earray) throws IllegalArgumentException;
	
	/**
	 * Sets the service.
	 *
	 * @param earray the new service
	 */
	public void setservice(ArrayList<Service> earray);
	
	/**
	 * Sets the contractlist.
	 *
	 * @param custarray the new contractlist
	 */
	public void setcontractlist(ArrayList<Contract> custarray);
	
	
	
	public void setCompanylist(ArrayList<Company> custarray);
	

	public void setadvancelist(ArrayList<Loan> advarray);
	

	/**
	 * Sets the leaveapplicationlist.
	 *
	 * @param leavearray the new leaveapplicationlist
	 */
	public void setleaveapplicationlist(ArrayList<LeaveApplication> leavearray);

	void setEmployeeShiftslist(ArrayList<EmployeeShift> custarray);

	void setEmployeelist(ArrayList<Employee> custarray);

	public void setpettycashlist(ArrayList<PettyCash> pettycasharray);
	public void setconbillinglist(ArrayList<BillingDocument> conbillingarray);
	public void setinvoicelist(ArrayList<Invoice> invoicingarray);
	public void setpaymentlist(ArrayList<CustomerPayment> paymentarray);
	public void setctclist(ArrayList<CTC> ctcarray);

	public void setleavebalancelist(ArrayList<LeaveBalance> leavebalancearray);
	public void setToolSetlist(ArrayList<ToolGroup> toolsetarray);
	public void setToollist(ArrayList<CompanyAsset> toolarray);
	public void setclientsideassetlist(ArrayList<ClientSideAsset> clientsideaseetarray);

	public void setLeaveCalendar(ArrayList<Calendar> clientsideaseetarray);

	public void setVendor(ArrayList<Vendor> custarray);
	public void setVendorProduct(ArrayList<PriceList> VendorProductarray);
	public void setRequestForQuotation(ArrayList<RequsestForQuotation> requestForQuotationarray);

	public void setLiaison(ArrayList<Liaison> liaisonarray);
	public void setSalesOrderList(ArrayList<SalesOrder> qarray);
	
	public void setsalesquotationlist(ArrayList<SalesQuotation> salesqarray);
	public void setdeliverynotelist(ArrayList<DeliveryNote> notearray);
	public void setLOI(ArrayList<LetterOfIntent> loiarray);
	public void setPO(ArrayList<PurchaseOrder> POarray);
	public void setPR(ArrayList<PurchaseRequisition> PRarray);
	public void setGRN(ArrayList<GRN> GRNarray);
	
	
	public void setMaterialRequestNote(ArrayList<MaterialRequestNote> mrnArrayList);
	public void setMaterialIssueNote(ArrayList<MaterialIssueNote> minArray);
	public void setProInvTranList(ArrayList<ProductInventoryTransaction> prodInvTranList);
	public void setMaterialMovementNote(ArrayList<MaterialMovementNote> mmnLlist);
	public void setProductInventoryView(ArrayList<ProductInventoryView> pivlist);
	
	public void setCompanyPaymentDetails(ArrayList<CompanyPayment> comppayarray);
	public void setTimeSheetList(ArrayList<TimeReport> timereport);
	public void setInteractionDetails(ArrayList<InteractionType> interactionarray);
	public void setInterfaceTallyDetails(ArrayList<AccountingInterface> interfacearray);
	public void setProductGrpList(ArrayList<ProductGroupList> Parray); 
	public void setBillOfMaterial(ArrayList<BillOfMaterial>Barray);
	public void setContactPersonDetails(ArrayList<ContactPersonIdentification> contactpersonarray);
	public void setVendorPaymentDetails(ArrayList<VendorPayment> vendorpaymentarray);
	public void setServiceProductList(ArrayList<ServiceProduct> sparray);
	public void setItemProductList(ArrayList<ItemProduct> iparray);		
	public void setSalesSecurityDeposit(ArrayList<SalesQuotation> salesdepositarray);
	public void setServiceSecurityDeposit(ArrayList<Quotation> servicedepositarray);	
	
	public void setApprovalsList(ArrayList<Approvals>apprarray);
	public void setDocumentHistoryList(ArrayList<DocumentHistory>docarray);
	public void setInventoryListDetails(ArrayList<ProductInventoryViewDetails> invlistarray);
	public void setLoggedInHistoryDetails(ArrayList<LoggedIn> loginhistoryarray);
	public void setCancelSummaryDetails(ArrayList<CancelSummary> cancelSummaryarray);
	public void setInspectionDetails(ArrayList<Inspection> inspectionList);
	public void setBillOfProductMaterial(ArrayList<BillOfProductMaterial> billProdArray);
	public void setVendorProductList(ArrayList<VendorPriceListDetails> venPriceList);
	public void setWorkOrderDetails(ArrayList<WorkOrder> woList);
	public void setCustomerBranchDetails(ArrayList<CustomerBranchDetails> custBranchlist);
	public void setPhysicalInventoryList(ArrayList<PhysicalInventoryMaintaince> inventoryreportList);
	public void setStockAlertList(ArrayList<ProductInventoryViewDetails> stockalertList);
	public void setMaterialConsumptionReport(ArrayList<MaterialConsumptionReport> consumptionReport, ArrayList<LabourCost> labourcostlist, ArrayList<ExpenseCost> expensecostlist,ArrayList<String> summarylist,double administrativecost);
	public void setSmsHistoryList(ArrayList<SmsHistory> smsarray);
	public void setFumigationReport(ArrayList<Fumigation> fumigationReport);
	public void setEmpAdditionDetails(ArrayList<EmployeeAdditionalDetails> woList);
	public void setPaySliplist(ArrayList<PaySlip> paySlipList);
	public void setFumigationAuslist(ArrayList<Fumigation> fumAusList);
	public void setComplainlist(ArrayList<Complain> paySlipList);
	public void setMateriallist(ArrayList<MaterialInfo> materailList);
	public void setContractRenewallist(ArrayList<RenewalResult> contractRenewalList);
	public void setScheduledServicelist(ArrayList<Service> scheduledServiceList);
	public void setCustomerHelpDesk(ArrayList<ContractTableValueProxy> helpdesklist);
	public void setPettyCashTrasactionList(ArrayList<PettyCashTransaction> pettycashtransactionlist);
	public void setSalesPersonPerformanceReport(ArrayList<String> personinfo,ArrayList<String> summarylist,ArrayList<SalesPersonValue> leadReport , ArrayList<SalesPersonValue> quotationReport, ArrayList<SalesPersonValue> contractReport);
	public void setSalesPersonTargetPerformanceReport(ArrayList<TargetInformation> salespersonTargetperformancelist, String financialyear);
	public void setCompanyAssetList(ArrayList<CompanyAssetListArticleInformation> companyAssetList);
	public void setTechnicianInfoReport(ArrayList<String> personinfolist, ArrayList<Service> serviceArray,ArrayList<String> summarylist);
	
//	public void setAssessmentDetails(ArrayList<AssesmentReport> assessmentDetails);
	
	
	/**
	  * For Service Po Download
	  * Date : 14-10-2016 By Anil
	  * Release : 30 Sept 2016 
	  * Project: Purchase Modification(NBHC)
	  */
	public void setServicePoList(ArrayList<ServicePo> serPoList);
	
	/**
	  * To get the asset upload format
	  * Date : 22-10-2016 By Anil
	  * Release : 30 Sept 2016 
	  * Project: Purchase Modification(NBHC)
	  */
	public void getAssetUploadFormat(SuperModel model);
	
	/**
	  * For Warehouse,Storage Location,Storage Bin Master Download
	  * Date : 23-11-2016 By Anil
	  * Project: Purchase Modification(NBHC) DEADSTOCK
	  */
	public void setWarehouseList(ArrayList<WareHouse> whList);
	public void setStorageLocationList(ArrayList<StorageLocation> slList);
	public void setStorageBinList(ArrayList<Storagebin> sbList);
	
	/**
	 * Date : 14-02-2017 By ANIL
	 * For Role Definition Download
	 */
	public void setRoleDefinitionList(ArrayList<RoleDefinition> whList);
	
	public void setMultiplExpenselist(ArrayList<MultipleExpenseMngt> expensearray);
	
	/**
	 * Date 2-03-2017 added by vijay
	 * for User Download
	 */
	public void setUserList(ArrayList<User> userarray);
	
	/**
	 * Date : 03-02-2017 By ANIL
	 * For Closing Stock Report
	 */
	public void setClosingStockReport(ArrayList<ClosingReport> closingReport);
	
	/**
	 * Date 03/09/2017 by jayshree for download sales person target report*********
	 * 
	 */
	public void setSalesPersonTargetReport(ArrayList<SalesPersonTargets> SalesPersonarray);
	
	/**
	 * Date : 14-11-2017 BY Komal
	 */
	public void setSchedulingList(String str);
	
	public void setMinUploadError(ArrayList<String> errorlist);
	
	/** date 29/01/2018 added by komal for hvac call log download **/
	public void setCustomerCallLog(ArrayList<CustomerLogDetails> logList);
	
	/** date 28.3.2018 added by komal for new tally interface download **/
	public void setTallyInterfaceData(ArrayList<TallyInterfaceBean> tallyInterfaceList);
	
	
	/** date 9.7.2018 added by komal for vendor invoice download **/
//	public void setVendorinvoicelist(ArrayList<VendorInvoice> invoicingarray);
	/**
	 * Date 5-7-2018 by jayshree add
	 * @param paysliparray
	 */
	public void setPayslip(ArrayList<PaySlip> paysliparray);
	
	/** DATE 11.7.2018 added by komal for attendance download*/
	public void setAttendance(ArrayList<AttendanceBean> attendanceList);
	
	/**
	 * Date : 16-07-2018 BY ANIL
	 *
	 */
	public void setAttandanceError(ArrayList<AttandanceError> attandanceErrorList);
	
	
	/**
	 * Date : 04-08-2018 By ANIL
	 */
	
	public void setSalarySlipData(ArrayList<PaySlip> salarySlipList);

	/** date 9.7.2018 added by komal for vendor invoice download **/
	public void setVendorinvoicelist(ArrayList<VendorInvoice> invoicingarray);
	
	
	/**** Date 19-10-2018 by Vijay for Complain Report ******/
	public void setComplainReport(Date fromDate, Date toDate);
	
	
	public void setServiceInvoiceMapping(HashMap<Integer, ServiceInvoiceDetails> serInvDt);

	/** 22-11-2018 added by amol for service CNC download **/
	
	public void setCNClist(ArrayList<CNC> cncArray);
	
	/** 23-11-2018  added by amol for project allocation download**/
	public void setProjectallocationlist(ArrayList<ProjectAllocation> projectallocationArray);
	
	/**23-11-2018 added by amol for Asset Allocation download **/
	public void setAssetallocationlist(ArrayList<EmployeeAsset> assetallocationArray);
	
	
	/**  26-11-2018    added by amol for adhoc settlement download   **/
	public void setAdhocsettlementlist(ArrayList<ArrearsDetails> adhocsettlementArray);
	
	/**26-11- 2018 added by amol for company contribution download **/
	public void setCompanycontributionlist(ArrayList<CompanyPayrollRecord> companycontributionArray);
	
	/**30-11-2018 added by amol for CTCTemplate download**/
	public void setCTCtemplatelist(ArrayList<CTCTemplate> ctctemplateArray);
	
	/**30-11-2018 added by amol for voluntryPF download**/
	public void setVoluntrypflist(ArrayList<VoluntaryPf> voluntrypfArray);
	
	/**30-11-2018 added by amol for voluntryPF history download**/
	public void setVoluntrypfhistorylist(ArrayList<VoluntaryPfHistory> voluntrypfhistoryArray);
	
	/**1-12-2018 added by amol for hrproject download**/
	public void setHRprojectlist(ArrayList<HrProject> hrprojectArray);
	
	/****23-2-2019 added by amol for  download the unallocated employee data  in process payroll ***/
	public void setUnallocatedemployeelist(ArrayList<AllocationResult> unallocatedemployeeArray);
	
	/**
	 * @author Anil,Date: 07-02-2019
	 * This method is used to set invoice data for sales register download and also loading other data related which will be used for same
	 * earlier we are doing same with "setinvoicelist()";
	 */
	
	public void setSalesList(ArrayList<Invoice> invoicingarray);
	
	public void setcrminvoicelist(ArrayList<Invoice> invoicingarray);
	
	/** 12-03-2019 added by Anil for EmployeeLeave download**/
	public void setEmployeeLeaveList(ArrayList<EmployeeLeave> empLeaveArray);
	
	
	/**23-8-2019 added by Amol for Employee Overtime download **/
	public void setEmployeeOvertimeList(ArrayList<EmployeeOvertime> empOvertimeArray);
	
	/**
	 * Updated By: Viraj
	 * Date: 19-03-2019
	 * Description: To set billedUnbilled data for csv file
	 * @param unBilledList
	 */
	void setBilledUnbilledList(ArrayList<BillUnbilledAr> unBilledList);
	/**29-3-2019 added by Amol  for Form B Download**/
	public void setFormBDetailList(ArrayList<PaySlip>formBArray);

	/**
	 * Updated By: Viraj
	 * Date: 24-05-2019
	 * Description: To make string variable for sales Register requirment of SilverLining
	 * @return
	 */
	public String fetchStringOfSalesRegister();
	/**@author Amol
	 * Date 7-7-2019
	 * Description added for form XIII
	 */
	public void setFormXIII(ArrayList<Employee> empDetailsarray);
	
		/** date 26-07-2019 added by komal to download attendance **/
	public void setAttendanceList(ArrayList<Attendance> attendacneArray);
	/**@author Amol Date 26-2-2020 by Amol added download option for appderegister**/
	public void setRegisterDeviceList(ArrayList<RegisterDevice>appDeregesterArray);
	
	public void setCreditNotelist(ArrayList<CreditNote> creditnotelist);
	
	public void setSMSTemplatelist(ArrayList<SmsTemplate> smstemplatelist);
	
	public String setExcelData(ArrayList<String> list, int columnCount, String documentName);
	public void setEmployeeAttendanceErrorlist(ArrayList<AttandanceError> list);

	public ArrayList<SuperModel> getData(ArrayList<SuperModel> list, String documentName);


}
