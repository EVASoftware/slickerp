
package com.slicktechnologies.client.services;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;

@RemoteServiceRelativePath("servicelistservice")
public interface ServiceListService extends RemoteService
{
	public String setServicesSelectedArrayList(ArrayList<Service> serviceList);
	public String setServicesSelectedListArrayList(ArrayList<Service> serviceList);
	
	public String printMultipleServices(ArrayList<Service> serviceList);
	
	/********* vijay for mark complete btton on list ********************/
	ArrayList<Integer> markcompleteservice(Date serviceCompetionDate,Service object);
	
	/**** Date 10 oct 2017 added by vijay for completing service though Technician scheduling list************/
	ArrayList<Integer> TechnicianSchedulingListMarkcompleteservice(Service object,String loggedInUser,int projectId,ArrayList<ProductGroupList> materialList);
	
	/**
	 * Date 04-05-2018
	 * Developer :- Vijay
	 * Description :- services list complete services with bulk  
	 */
	Integer markCompleteServicesWithBulk(ArrayList<Service> serviceList, String remark, Date serviceCompletionDate);
	/**
	 * ends here
	 */
	Integer checkScheduleServices(ArrayList<Integer> contractCount, Date serDate ,Date billDate);
	/**
	 * Date 02-08-2018
	 * Developer :- nidhi
	 * Description :- services list for assgin service engineer
	 */
	Integer setServiceTechnicianToServices(ArrayList<Service> serviceList,String remark,
			String techEmployee,Long companyId);
	/**
	 * ends here
	 */
	/**
	 * Updated By:Viraj
	 * Date:17-06-2019
	 * Description: service list to assign branch
	 */
	Integer setSeviceBranchToService(ArrayList<Service> serviceList,String remark,
			String branch,Long companyId);
	
	/**
	 * Updated By:Viraj
	 * Date:29-06-2019
	 * Description: service list to assign status
	 */
	Integer setServiceStatusToService(ArrayList<Service> serviceList,String remark,
			String status,Long companyId);
	
	/**
	 * @author Anil
	 * @since 12-05-2020
	 */
	public String updateComplainDocument(ArrayList<Service> serviceList,boolean isSchedule,boolean isAssignTechnician,boolean isScheduleTech,boolean isServiceCompletion);

	/**
	 * @author Anil @since 23-03-2021
	 * adding RPC which will check and allocate material in case technician is changed and services are already scheduled
	 */
	public ArrayList<String> validateTechnicianStock(long companyId,String technicianName,ArrayList<Service> serviceList);
	
	
	public String sendServiceComletionSMS(Service service);
	
	public String setServicesForScheduleOrCompleteViaExcelDonwload(ArrayList<Service> serviceList); //Ashwini Patil Date:27-10-2023
	
	public Integer getContractServiceCount(Contract con);//Ashwini Patil Date:11-04-2024 
	public Integer getContractBillingCount(Contract con);//Ashwini Patil Date:16-04-2024
	
	}
