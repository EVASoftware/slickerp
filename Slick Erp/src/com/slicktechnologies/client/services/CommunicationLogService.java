package com.slicktechnologies.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;

@RemoteServiceRelativePath("communicationLogService")
public interface CommunicationLogService extends RemoteService {

	void saveCommunicationLog(ArrayList<InteractionType> communicationloglist);
	
	/**
	 * Date 13-11-2019 
	 * @author Vijay Chougule 
	 * Des :- NBHC CCPM Rate Contract Service value updation when Billing document Approved
	 */

	String updateRateContractServiceValue(BillingDocument billingDocument);
	
	/**
	 * Date 14-11-2019 
	 * @author Vijay Chougule
	 * Des :- NBHC CCPM complaint service validation for if Assessment is in created status
	 */
	String ValidateAssessmentForComplaintService(ArrayList<Integer> complaintServicelist, long companyId);
}
