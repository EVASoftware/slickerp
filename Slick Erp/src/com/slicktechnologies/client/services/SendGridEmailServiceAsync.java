package com.slicktechnologies.client.services;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface SendGridEmailServiceAsync {
	public void sendGridEmail(String str, AsyncCallback<Void> callback);
}
