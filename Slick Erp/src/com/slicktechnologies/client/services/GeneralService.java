package com.slicktechnologies.client.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.attendance.EmpAttendanceDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.customerlog.MaterialRequired;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
@RemoteServiceRelativePath("generalservice")
public interface GeneralService extends RemoteService{

	ArrayList<Integer> createBillingDocs(Contract model);
	
	/**
	 * Date 14 july 2017 added by vijay for Quick Sales submit Implementation
	 */
	
	String quickSalesSumbit(SalesOrder model);
	/**
	 * ends here
	 */
	
	/**
	 * Date 05-07-2017 added by vijay for Return Expense amount from petty Cash
	 */
	
	MultipleExpenseMngt returnExpenseAmount(MultipleExpenseMngt multiexpenseModel, double expenseReturnAmt,String loginUser);

	
	/**
	 * Date 18-08-2017 added by vijay for Quick Purchase submit Implementation
	 */
	String quickPurchaseOrderSubmit(PurchaseOrder model);
	
	/**
	 * Date 05-0ct-2017 added by vijay for technician scheduling 
	 */
	Integer TechnicianSchedulingPlan(Service model,ArrayList<EmployeeInfo> technicianlist, ArrayList<CompanyAsset> technicianToollist, ArrayList<ProductGroupList> materialInfo, Date serviceDate, String serviceTime, int invoiceId, String invoiceDate, String TechnicianName, int projectId);
	
	/**
	 * Date 06 oct 2017 added by vijay selected service saving with one unique number
	 */
	String saveSelectedService(ArrayList<Service> servicelist, long companyId);
	
	/**
	 * Date 25 oct 2017 added by vijay mark Complete service Status changing and project status changing and project mark complete 
	 */
	String saveServiceProjectStatus(Service service);
	
	/**
	 * Date 08-11-2017 added by Komal 
	 */
	String updateLeadQuotationFollowUpDate(long companyId);
	
	/**
	 * Date 22-12-2017 added by vijay for Updating all payment document with TDS flag as index 
	 */
	Integer updateAllPaymentDocumentWithTDSflagIndex(long companyId);
	
//	date 01-02-2018 added by komal for nbhc  closing stock
	
	ArrayList<ProductInventoryViewDetails> getProductInventoryViewDetails(long companyId , String branch ,String warehouse );
	
//	date 02-02-2018 added by komal for nbhc purchase requisition 
	ArrayList<PurchaseRequisition> getPurchaseRequisitionDetails(long companyId ,String bussinessType ,ArrayList<PurchaseRequisition> prList);
	
	/** date 19.4.2018 added by komal to create contrat(for pestMorterm )**/
	Contract createAndGetContractDetails(long companyId , Complain complain , String user);
	
	Service createServiceFromComplain(long companyId , Complain complain, String user,int contractId );
	
	 /** date 05/12/2017 added by komal to  save customer log details **/
		CustomerLogDetails saveCustomerCallLogDetails(CustomerLogDetails customerLog , long companyId);
		String createBilling(CustomerLogDetails log , int customerLog ,String approverName ,ArrayList<MaterialRequired> serviceList ,long companyId);
		String sendDefective(ArrayList<MaterialMovementNote> selectedList , Long companyId , String approverName);
		GRN getGRNForDefective(Integer grnId , Long companyId);
		String saveAndApproveDocument(SuperModel m,String customerId, String customerName, String documentName);
	
	/**
	 * Date 09-05-2018
	 * Developer : Vijay
	 * Des :- Payment document update with follow up date for status created only because we load created status dashboard only
	 */
	public Integer updatePaymentDocsWithFollowupDate(Long companyId);
	/**
	 * ends here
	 */		

	/** date 25 .4.2018 added by komal for fumigation invoice generation **/
	Invoice generateInvoiceForFumigation(long companyId , Fumigation fumigation );

	/*** Date 01-06-2018 By vijay for Updating service address from contract only for non multilocation services ***/
	boolean updatedServiceAddress(Contract contract, boolean customerbranchservicesflag);
	
	/*by Apeksha Gunjal on 29/06/2018 @19:31 for sendPushNotificationToTechnician*/
	public String sendPushNotificationToTechnician(Service service, long companyId);
	
	/** Date 07-07-2018 By Vijay for Billiing Documents Cancellation ***/
	boolean MultipleBillingDocsCancellation(ArrayList<BillingDocument> billinglist,String remark);
	
	/****** Date 26-07-2018 By Vijay For Pepcopp Invoice edited by user sending email **************/
	public boolean sendInvoiceEditedEmail(int invoiceId,String branch,String loggedInUser, double oldNetPayable,double newNetPayable, Date oldInvoiceDate, Date newInvoiceDate,long companyId,String customerName);
	
	/**
	 * Date 10-09-2018 by Vijay
	 * Des :- customer call log Product invcentory master Transfer to warehouse defined or not checking validation
	 */
	public ArrayList<SuperModel> getProductInventoryDetails(ArrayList<Integer> productIdlist,long companyId);
	/**
	 * ends here
	 */
	
	/**
	 * Date : 21-09-2018 Merged by ANIL
	 */
	
	ArrayList<Attendance> setAttendance(long companyId, String branch,String month,int empId, String projectName);

	/** date 17.7.2018 added by komal for payroll taskqueue**/
	void paySlipAllocationTaskQueue(ArrayList<EmployeeInfo> empinfoarray,String payRollPeriod,Date fromDate,Date toDate, String projectName); 
	
	
	String updateEmployeeInfoStatus(long companyId,String action,int empId,String payrollPeriod,String project,String branch,String roleName);

	/**
	 * End
	 */
	 /** Date 13.12.2018 added by komal for update accounting interface **/
	String updateAccountingInterface(SuperModel model);
	
	/**
	 * Date 20-12-2018 By Vijay For Updating PR Status for NBHC :- Inventory Management 
	 */
	String updatePRStatus(int purchaseReqId);
	
	/**
	 * Date 10-01-2019 By Vijay for Updated GRN for products to set prodinfo
	 */
	String updateGRNDocs(long companyId);
	
	/**
	 * Date 15-01-2019 by Vijay for NBHC Inventory Management updating PR Doc 
	 */
	
	String updatePRDoc(Approvals approval,boolean documentflag, int vendorId,double vendorPrice);
	
	/**
	 * Updated By: Viraj
	 * Date: 22-01-2019
	 * Description: To return list of billing, vendorInvoice and payment document in purchase order
	 */
	public ArrayList<CancelContract> getCancellingDocumentPurchaseOrder(Long companyId , int orderId);
	
	public void savePOCancellationData(PurchaseOrder po,String remark,String loginUser ,ArrayList<CancelContract> cancelContractList);

	/**
	 * Date 28-01-2019 By Vijay NBHC Inventory Management Updating product inventory with Warehouse type
	 * to identify warehouse and cluster warehouse
	 */
	String updateProductInventoryWithWarehouseType(long companyId);
	
	/**
	 * Date 09-03-2019 By Vijay for NBHC if service is not completed then tax invoice not allowed
	 * with configuraion values
	 */
	String validateDueService(long companyid,Date startDate,Date dueDate,ArrayList<String> branchlist,ArrayList<Integer> listcontractId);
	
	/**
	 * Date 10-05-2019 by Vijay
	 * Des :- creating Storage location and Storage Bin for the existing warehouse
	 */
	String createStoraLocationBin(long companyId);
	
	/**
	 * Date 20-05-2019 by Vijay
	 * Des :- updating contract Discontinue Date with discontinue approval date
	 */
	String updateContractDiscontinued(long companyId);

	/**
	 * @author Anil , Date : 29-05-2019
	 * deleteExtraServicesAgainstContract
	 * @author Anil , Date : 04-12-2019
	 * Deleting extra services contract and product wise(for large volume case)
	 */
	String deleteExtraServicesAgainstContract(Contract contract,int productId);

	/**
	 * Updated By: Viraj
	 * Date: 06-04-2019
	 * Description: To fetch all the product inventory list data of the customer
	 * @param customerId
	 * @param callback
	 */
	ArrayList<ProductInventoryViewDetails> fetchCustomerProductViewList(long companyId, int customerId);
	
	/**
	 * Updated By: Viraj
	 * Date: 16-05-2019
	 * Description: To fetch sales register information from master franchise or franchise depending customer franchise type
	 */
	String fetchSalesRegisterList(long companyId, int customerId,
			Date fromDate, Date toDate);
			
			/** date 10.4.2019 added by komal to update services within specific duration **/
	String updateServices(long companyId,Date startDate,Date endDate,int customerId,String branch);

	/** date 27.5.2019 added by komal for combined material list **/
	ArrayList<ProductGroupList> getCombinedMaterialList(long companyId , ArrayList<Integer> serviceIdList, ArrayList<Integer> contractIdList);
	String createMMNForTechnician(long companyId , ArrayList<ProductGroupList> productList,String user,ArrayList<Integer> serviceIdList);
	
	/**
	 * @author Anil , Date : 13-06-2019
	 */
	public void updateProductCode(long companyId);
	
	/*** Date 26-07-2019 by Vijay for NBHC CCPM Approved Contract Cancelled Services ****/
	String getCancelledServiceOfApprovedContract(long companyId, Date fromDate, Date toDate, ArrayList<String> brnachlist);
	
	/**
	 * Date 22-07-2019 by Vijay
	 * Des :- for NBHC CCPM Contract Upload
	 */
	String contractUploadService(String entityName,long companyId);
	
	/**
	 * Date 22-05-2019 by Vijay For Complaint service creation
	 */
	String createComplaintService(Complain complain,int complaintId);
	
	/**
	 * Date 12-08-2019 by VIjay
	 * Des :- Complaint Mark Completion
	 */
	String markCompleteComplaint(long companyId, int ticketId);
	
	/**
	 * Date 27-08-2019 by Vijay 
	 * Des :- NBHC CCPM Rate contracts Service value updation for Contract PNL Report 
	 */
	String updateServiceValueForRateContract(Date fromDate, Date toDate,String branch,long companyId);
	/**
	 * Date 02-09-2019 by Vijay
	 * Des :- NBHC Inventory Management Lock Seal Deviation Report
	 */
	
	String getLockSealDeviationReport(long companyId, Date fromDate, Date toDate, ArrayList<String> brnachlist);



	
	/**
	 * @author Anil , Date : 16-09-2019
	 * moving invoice creation part from client side to server side
	 * multiple invoice created against single bill
	 */
	
	public String createTaxInvoice(BillingDocument billingDoc , String invoiceType,String invoiceCategory, String invoiceType2, String invoiceGruop,String approverName,String userName,boolean approveInvoiceCreatePaymentDocFlag);
	 /**Date 20-9-2019 by AMol raised by Rohan sir For Update the emailid of old invoices for ultrapestthane**/
	public void updateInvoices(long companyId,String fromDate,String toDate);
	/**
	 * @author Vijay Chougule
	 * Des :- NBHC CCPM Assessment mark close and send an Email 
	 */
	public String AssesmentMarkCloseAndSendEmail(AssesmentReport assesmentreport);
	
	String sendSrMail(long companyId , ArrayList<Integer> serviceIdList);

/** date 19.09.2019 added by komal to validate technician warehouse details and inventory checking **/
	ArrayList<String> validateTechnicianWarehouse(ArrayList<Service> serviceList,List<ProductGroupList> groupList);
	ArrayList<String> validateTechnicianWarehouseQuantity(long companyId , ArrayList<Service> serviceList, List<ProductGroupList> groupList);

		
	/**
	 * @author Anil , Date : 27-09-2019
	 * createAttendance
	 */
	public String createAttendance(ArrayList<Attendance> attendanceList);
	boolean updatedServiceAddress(SalesOrder salesorder, boolean customerbranchservicesflag);
	public void updateCnc(long companyId);
	/**
	 * @author Anil , Date : 19-10-2019
	 */
	public void updateDuplicateServices(long companyId,Date fromDate,Date toDate,int serviceId,Date day1,Date day2,int contractId,int fromServiceId,int toServiceId);

	/**
	 * @author Anil , Date : 26-11-2019
	 */
	public void updateDuplicateCustomerBranch(long companyId,int custBranchId);
	
	/**
	 *@author Anil
	 *@since 17-06-2020
	 */
	public void updateComponentDetailsOnServices(Contract contract);
	
	/**
	 * @author Anil @since 17-02-2021
	 * This RPC is use to check Internet connection 
	 */
	public void ping();
	
	
	/**
	 * @author Anil @since 17-12-2021
	 * Schedule Assessment for prominent 
	 * raised by Nitin Sir
	 */
	public AssesmentReport scheduleAssessment(AssesmentReport report,Date serviceDate,String time,String description);
	
	void inactiveUser(String userName, long companyId);
	
	public ArrayList<EmployeeInfo> getEmployeeinfolist( ArrayList<Integer> empidlist, long companyId);

	public String updateFrequency(long companyId, Date contractStartDate);
	
	public boolean checkIfLicensePresent(long companyId, String licenseCode); //Ashwini Patil Date:3-08-2023 for audit license

	public String updateEmployeeIdInUser(long companyId); //Ashwini Patil Date:22-08-2023 for audit license

	public void deleteDuplicateLeaves(long companyId,Date fromDate,Date toDate); //Ashwini Patil Date:23-08-2023 for sunrise issue while payroll- number of leave days not matching. duplicate leave entries found in employee leave history

	public void rescheduleForPeriod(long companyId,Date fromDate,Date toDate,ArrayList<Service> serviceList);//Ashwini Patil Date:6-10-2023 for Orion- as there maximum services are falling on one  day of month and its difficult to rescedule and print the services report(dockets) from customer service list

}