package com.slicktechnologies.client.services;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public interface AutoInvoiceServiceAsync {
	
//	void createTaxInvoice(BillingDocument billingDoc, String invoiceType, String invoiceCategory, String invoiceType2, String invoiceGruop,String approverName,String userName, AsyncCallback<String> callback);

	void createTaxInvoice(BillingDocument billingDoc,Invoice invoice,  String loggedInUser, boolean autoinvoiceflag, ArrayList<Integer> billIdList, ArrayList<Integer> billOrderIdList, AsyncCallback<String> callback);

	void updateServiceBranchinServies(int contractId,long companyId, AsyncCallback<String> asyncCallback);
}
