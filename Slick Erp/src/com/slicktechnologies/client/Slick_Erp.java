package com.slicktechnologies.client;

import java.util.Date;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.libservice.RegisterService;
import com.simplesoftwares.client.library.libservice.RegisterServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.services.LogoutService;
import com.slicktechnologies.client.services.LogoutServiceAsync;
import com.slicktechnologies.client.services.SessionService;
import com.slicktechnologies.client.services.SessionServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConnector;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.Event.NativePreviewHandler;
import com.google.gwt.user.client.Window.ClosingEvent;
import com.google.gwt.user.client.Window.ClosingHandler;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Slick_Erp implements EntryPoint 
{
	public static String companyname="";
	public static String businessUnitName="";
	String finalurl="";
	public static String loginCompanyName="";
//	private final static SessionServiceAsync sessionService=GWT.create(SessionService.class);
	private LogoutServiceAsync logoutserviceasync=GWT.create(LogoutService.class);
	
	/**
	 * Developed By : Rohan Bhagde.
	 * Reason : wanted to give popup and terminate session if yes  else stay on page  
	 */
	
		ConditionDialogBox conditionPopup;
		PopupPanel panel;
	
	/**
	 * Ends here
	 */
	/***********************************************************************/
	/***********************************************************************/
	/************************* Deployment Steps ****************************/
	/***********************************************************************/
	/***********************************************************************/
	
	// NOTE: Please read comments below while deploying
	
	
	/** For normal deployment on existing application id */
	
	//  Add application ID in appengine xml and give version number
	//  Make changes in Login Form
	//  Make changes in this file. Below comments are written and follow the steps

	/******************************************************************************/
	
	/** For Deploying for first time on new application id follow the steps */
	
	/****** Read comment below for first time deployment and uncomment the code*****/
	/** Follow steps written in Appconnector  */
	
	
	/***********************************************************************/
		
	/**
	 * @author Anil , Date : 13-01-2020
	 */
	Date previousDateTime;
	Timer sessionTimer;
	public static boolean isSessionActive=false;
	
	public void onModuleLoad()
	{
		initializeSessionTimer();
		conditionPopup=new ConditionDialogBox("Do you want to Terminate Session..!!",AppConstants.YES,AppConstants.NO);
		
		RegisterServiceAsync registerservice=GWT.create(RegisterService.class);
		final NameServiceAsync service=GWT.create(NameService.class);
		
//		addclickHandlerOnPopUpButtons();
		
		Event.addNativePreviewHandler(new NativePreviewHandler() {
			  public void onPreviewNativeEvent(final NativePreviewEvent event) {
			    final int eventType = event.getTypeInt();
			    switch (eventType) {
			    case Event.ONKEYPRESS:
//			    	 updateSessionLastAccessedTime();
			    	break;
			    	
			    case Event.ONKEYDOWN:
//			    	 updateSessionLastAccessedTime();
				    	break;
			    	
			    case Event.ONKEYUP:
//			    	 updateSessionLastAccessedTime();
				    	break;
			    	
			    case Event.ONMOUSEWHEEL:
//			    	 updateSessionLastAccessedTime();
				    	break;
			    	
			    case Event.ONSCROLL:
//			    	 updateSessionLastAccessedTime();
				    	break;
			     
			    case Event.KEYEVENTS:
//			    	 updateSessionLastAccessedTime();
			    	checkSessionTimeOut();
				    	break;
			        //**************commented by rohan for removing session time out********
			      case Event.ONCLICK:
//			    	  updateSessionLastAccessedTime();
			    	  checkSessionTimeOut();
			        break;
			      default:
			        // not interested in other events
			    }
			  }
			});
		
		
		   Window.addCloseHandler(new CloseHandler<Window>() {

	            @Override
	            public void onClose(CloseEvent<Window> event) {
			    	
			        Company c=new Company();
					Long companyId=c.getCompanyId();
					String username = UserConfiguration.getUserconfig().getUser().getUserName();
					browsingEventClose(companyId, username);
			    }
	        });
		
//		/**
//		 * rohan added this for back button session termination
//		 */
//		   
//		 Window.addWindowClosingHandler(new ClosingHandler() {
//			
//			@Override
//			public void onWindowClosing(ClosingEvent event) {
//				panel=new PopupPanel(true);
//				panel.add(conditionPopup);
//				panel.setGlassEnabled(true);
//				panel.show();
//				panel.center();
//			}
//		}); 
	/***********************************LOCAL SERVER CODE******************************************/
	/***************************Comment This Code While Deploying(For Local)****************************/	
		
//		String[] spliturl;	
//		String url=Window.Location.getHref();
//		url=url.replaceAll("http://","");
//		
//		
//		if(url.contains("127")||url.contains("slicktechnologies-pc:"))
//			spliturl="http://www.ganesh.evasoftwaresolutions.com/".split("\\.");
//		else
//		   spliturl=url.split("\\.");
//		
//		
//		for(int i=0;i<spliturl.length;i++)
//		{
//			System.out.println("Url is "+spliturl[i]+" "+i);
//			if(spliturl[i].equals("evasoftwaresolutions"))
//			{
//				companyname=spliturl[i-1];
//				System.out.println("Company Name is "+companyname);
//				break;
//			}
//		}
		 /**  
		  * Date 20-04-2020 
		  * Des :- new local database created so added company name access url hardcoded here to run project
		  * locally.
		  */
		   
		 companyname = "my";
		
		registerservice.registerClass(new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) 
			{
				//Console.log(finalurl);
				service.getClientName(companyname,new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
					   Window.alert("Company Not Registered !");
					   Console.log("Failure Method");
					}

					@Override
					public void onSuccess(String result) {
						if(result==null)
						{
							Window.alert("Company Not Registered !");
						}
						else{
//							loginCompanyName=finalurl;
							RootPanel rp=RootPanel.get();
							rp.clear();
							businessUnitName=result;
							AppUtility.createLogin(result);
						}
					}
				});
		
//				AppConnector.initalizes();
//				AppMemory mem=AppMemory.getAppMemory();
//				mem.initialingapp();
			}
			
			@Override
			public void onFailure(Throwable caught) {
			}
		});
		
		/**************************************Comment End**************************************************/
		
		/*************************************DEPLOYMENT (ON SERVER)***************************************/
		
		
		/*****************************Uncomment this code while deploying**********************************/
		
		
////			String url=Window.Location.getHref();
////			System.out.println("URL-------- "+url);
////			String[] spliturl=url.split("\\.");
////			String companyname=spliturl[0];
////			companyname=companyname.replace("http://","");
////			finalurl=companyname;
//		
//		
//			/**
//			 * Date 14-11-2018 By Vijay
//			 * Des :- For https SSL i have added below if condition and old code added in else block
//			 */
//			String url=Window.Location.getHref();
//			String[] spliturl;
//			String companyname;
//			if(url.contains("-dot-")){
//				spliturl=url.split("\\-");
//				companyname=spliturl[0];
//				companyname=companyname.replace("https://","");
//				finalurl=companyname;
//			}else{
//				 spliturl=url.split("\\.");
//				 companyname=spliturl[0];
//				 companyname=companyname.replace("http://","");
//				 finalurl=companyname;
//			}
//			/**
//			 * ends here
//			 */
//	
//			registerservice.registerClass(new AsyncCallback<Void>() {
//						
//						@Override
//						public void onSuccess(Void result) 
//						{
//							//Console.log(finalurl);
//							service.getClientName(finalurl,new AsyncCallback<String>() {
//			
//								@Override
//								public void onFailure(Throwable caught) {
//								   Window.alert("Company Not Registered !");
//								   Console.log("Failure Method");
//								}
//			
//								@Override
//								public void onSuccess(String result) {
//									if(result==null)
//									{
//										Window.alert("Company Not Registered !");
//									}
//									else{
//										loginCompanyName=finalurl;
//										RootPanel rp=RootPanel.get();
//										rp.clear();
//										businessUnitName=result;
//										AppUtility.createLogin(result);
//									}
//								}
//							});
//					
//	//						AppConnector.initalizes();
//	//						AppMemory mem=AppMemory.getAppMemory();
//	//						mem.initialingapp();
//						}
//						
//						@Override
//						public void onFailure(Throwable caught) {
//						}
//					});
					

		/****************************************************************************************/
		/****************************************************************************************/
		
		/**********While Deploying on new application Id Uncomment the code below***************/
		
//		String url=Window.Location.getHref();
//		System.out.println("URL-------- "+url);
//		String[] spliturl=url.split("\\.");
//		String companyname=spliturl[0];
//		companyname=companyname.replace("http://","");
//		finalurl=companyname;
//		
//
//		registerservice.registerClass(new AsyncCallback<Void>() {
//					
//					@Override
//					public void onSuccess(Void result) 
//					{
//						AppConnector.initalizes();
//						AppMemory mem=AppMemory.getAppMemory();
//						mem.initialingapp();
//					}
//					
//					@Override
//					public void onFailure(Throwable caught) {
//					}
//				});

		
		
//		/*************************************Comment End***************************************/
	}
	
	
//	private void updateSessionLastAccessedTime() {
//		
//		if(LoginPresenter.sessionCountValue !=0){
//		Console.log("in side slick erp update session count="+LoginPresenter.sessionCountValue+"- userName"+LoginPresenter.proxyString+"- LoggedinUser"+LoginPresenter.loggedInUser);	
//		Company comp=new Company();
//		sessionService.ping(LoginPresenter.proxyString,LoginPresenter.sessionCountValue,true,comp.getCompanyId(),LoginPresenter.loggedInUser,new AsyncCallback<Integer>() {
//		public void onSuccess(Integer result) {
//			
//			if(result== -1){
//				LoginPresenter.displaySessionTimedOut("Session time out due to simultaneous login and exceed no of user ..!");
//			}
//			else if(result== -10){
//				LoginPresenter.displaySessionTimedOut("Session time out on server side..!");
//			}
//			else if(result== -20){
//				
//				LoginPresenter.displaySessionTimedOut("Your session has been terminated as <User Name= "+LoginPresenter.proxyString+"> has logged in  to other terminal or session..!");
//			}
//			else if(result== -2){
//				LoginPresenter.displaySessionTimedOut("You Have been forcefully terminated by Admin..!");
//			}
//		}
//		 
//		public void onFailure(Throwable caught) {
//		}
//		});
//		}
//	}
	
	public  void browsingEventClose(Long companyId,String username){
		
		logoutserviceasync.doLogoutonBrowserClose(companyId,username,"Browser Window Closed",LoginPresenter.sessionCountValue,new AsyncCallback<User>() {
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}
			@Override
			public void onSuccess(User result) {
			}
		});
	}
	
	
	/**
	 * ROhan added this code 
	 * 
	 */
	
//	private void addclickHandlerOnPopUpButtons() {
//		
//		conditionPopup.getBtnOne().addClickHandler(new ClickHandler() {
//			
//			@Override
//			public void onClick(ClickEvent event) {
//				panel.hide();
//			}
//		});
//		
//		
//		conditionPopup.getBtnTwo().addClickHandler(new ClickHandler() {
//			
//			@Override
//			public void onClick(ClickEvent event) {
//				panel.hide();
//			}
//		});
//	}
	
	
	public void checkSessionTimeOut() {
		
		if(isSessionActive){
			Console.log("Session Active.");
		}else{
			Console.log("Session not active.");
			return;
		}
		
		Date currentDateTime = new Date();
		Console.log("Inside Session Time Out Method .. Current Time : "+currentDateTime+" Previous Time : "+previousDateTime);
		if(previousDateTime!=null){
			long duration  = currentDateTime.getTime() - previousDateTime.getTime();
			
			long diffInMinutes =  duration / (60 * 1000) % 60;
			
			Console.log("Difference In Minutes : "+diffInMinutes);
			if(diffInMinutes>=10){
				Console.log("Log Out Here..!!");
				
				Company c=new Company();
				Long companyId=c.getCompanyId();
				String username = UserConfiguration.getUserconfig().getUser().getUserName();
				logoutserviceasync.doLogoutonBrowserClose(companyId,username,"Time out!",LoginPresenter.sessionCountValue,new AsyncCallback<User>() {
					@Override
					public void onFailure(Throwable caught) {
						caught.printStackTrace();
					}
					@Override
					public void onSuccess(User result) {
						Console.log("Log Out Successful..!!");
						Window.Location.reload();
						Window.alert("Session expired due to time out!");
					}
				});
				
			}else{
				previousDateTime=currentDateTime;
				if(sessionTimer!=null&&sessionTimer.isRunning()){
					Console.log("Session running..!!");
					sessionTimer.cancel();
					initializeSessionTimer();
				}else{
					initializeSessionTimer();
				}
				
			}
		}else{
			previousDateTime=currentDateTime;
			if(sessionTimer!=null&&sessionTimer.isRunning()){
				Console.log("Session running.1..!!");
				sessionTimer.cancel();
				initializeSessionTimer();
			}else{
				initializeSessionTimer();
			}
		}
	}
	
	
	public void initializeSessionTimer(){
		if(!isSessionActive){
			Console.log("Session is not active!!");
			return;
		}
		if(previousDateTime==null){
			previousDateTime=new Date();
		}
		Console.log("Session initialized..!!");
		sessionTimer = new Timer() {
		      @Override
		      public void run() {
		    	  checkSessionTimeOut();
		      }
	    };
	    sessionTimer.schedule(600000);
	}
	
	
	

}
