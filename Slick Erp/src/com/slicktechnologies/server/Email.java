package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.json.JSONException;

import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobInfoFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreInputStream;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.google.gwt.dev.jjs.impl.Pruner;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.ObjectifyService;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.client.services.EmailService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.customertraining.CustomerTrainingAttendies;
import com.slicktechnologies.client.views.customertraining.CustomerTrainingSpeaker;
import com.slicktechnologies.server.addhocprinting.AssessmentReportPdf;
import com.slicktechnologies.server.addhocprinting.ContractPdf;
import com.slicktechnologies.server.addhocprinting.ContractRenewalPdf;
import com.slicktechnologies.server.addhocprinting.ContractRenewalPdfversionOne;
import com.slicktechnologies.server.addhocprinting.CustomerPaySlipPdf;
import com.slicktechnologies.server.addhocprinting.CustomerServiceRecordPdf;
import com.slicktechnologies.server.addhocprinting.CustomerServiceRecordVersionOnePdf;
import com.slicktechnologies.server.addhocprinting.DeliveryNotePdf;
import com.slicktechnologies.server.addhocprinting.FumigationAusPdf;
import com.slicktechnologies.server.addhocprinting.FumigationPdf;
import com.slicktechnologies.server.addhocprinting.GSTQuotationPdf;
import com.slicktechnologies.server.addhocprinting.HygeiaContractPdf;
import com.slicktechnologies.server.addhocprinting.HygeiaInvoicePdf;
import com.slicktechnologies.server.addhocprinting.HygeiaPaymentReceiptPdf;
import com.slicktechnologies.server.addhocprinting.InspectionPdf;
import com.slicktechnologies.server.addhocprinting.InvoicePdf;
import com.slicktechnologies.server.addhocprinting.MultipleServicesRecordVersionOnePdf;
import com.slicktechnologies.server.addhocprinting.NBHCAssessmentReportPdf;
import com.slicktechnologies.server.addhocprinting.NBHCQuotationPdf;
import com.slicktechnologies.server.addhocprinting.NewContractRenewalPdf;
import com.slicktechnologies.server.addhocprinting.NewContractRenewalVersionOnePdf;
import com.slicktechnologies.server.addhocprinting.OrionServiceInvoicePdf;
import com.slicktechnologies.server.addhocprinting.PayrollPdfUpdated;
import com.slicktechnologies.server.addhocprinting.PcambContractpdf;
import com.slicktechnologies.server.addhocprinting.PcambInvoicepdf;
import com.slicktechnologies.server.addhocprinting.PdfCancelWatermark;
import com.slicktechnologies.server.addhocprinting.PdfUpdated;
import com.slicktechnologies.server.addhocprinting.PdfWatermark;
import com.slicktechnologies.server.addhocprinting.PestleFiveYearsQuotation;
import com.slicktechnologies.server.addhocprinting.PestleQuotationPdf;
import com.slicktechnologies.server.addhocprinting.PestoIndiaContractPdf;
import com.slicktechnologies.server.addhocprinting.PestoIndiaInvoicePdf;
import com.slicktechnologies.server.addhocprinting.PestoIndiaQuotationPdf;
import com.slicktechnologies.server.addhocprinting.PurchaseOrderPdf;
import com.slicktechnologies.server.addhocprinting.PurchaseOrderVersionOnePdf;
import com.slicktechnologies.server.addhocprinting.QuotationPdf;
import com.slicktechnologies.server.addhocprinting.QuotationVersionOnePdf;
import com.slicktechnologies.server.addhocprinting.SalesGSTInvoice;
import com.slicktechnologies.server.addhocprinting.SalesInvoicePdf;
import com.slicktechnologies.server.addhocprinting.SalesQuotationPdf;
import com.slicktechnologies.server.addhocprinting.ServiceGSTInvoice;
import com.slicktechnologies.server.addhocprinting.ServiceInvoicePdf;
import com.slicktechnologies.server.addhocprinting.ServicePoPdf;
import com.slicktechnologies.server.addhocprinting.ServiceQuotationPdf;
import com.slicktechnologies.server.addhocprinting.ServiceRecordPdf;
import com.slicktechnologies.server.addhocprinting.TaxInvoicePdf;
import com.slicktechnologies.server.addhocprinting.UniversalPestContractPdf;
import com.slicktechnologies.server.addhocprinting.UniversalPestQuotationPdf;
import com.slicktechnologies.server.addhocprinting.WorkOrderPdf;
import com.slicktechnologies.server.addhocprinting.reddysPestControlInvoice;
import com.slicktechnologies.server.addhocprinting.fumigation.FumigationCertificateAFAS;
import com.slicktechnologies.server.addhocprinting.fumigation.FumigationCertificateALP;
import com.slicktechnologies.server.addhocprinting.fumigation.FumigationCertificateMB;
import com.slicktechnologies.server.addhocprinting.nbhc.ServiceSchedulePdf;
import com.slicktechnologies.server.addhocprinting.pecopp.PecoppContractPdfPrint;
import com.slicktechnologies.server.addhocprinting.pecopp.PecoppInvoicePrintPdf;
import com.slicktechnologies.server.addhocprinting.premiumTech.ComplainPdf;
import com.slicktechnologies.server.addhocprinting.sasha.PurchaseGSTInvoice;
import com.slicktechnologies.server.email.SendGridEmailServlet;
import com.slicktechnologies.server.cronjobimpl.AutomaticPRCreationCronJobImpl;
import com.slicktechnologies.server.cronjobinteration.HrCronJobImpl.MissingAttendanceDetails;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.CustomerTrainingDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.CompanyProfileConfiguration;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.FeedbackUrlAsPerCustomerCategory;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.deliverynote.DeliveryLineItems;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.email.EmailDetails;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.LoggedIn;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.helperlayer.SocialInformation;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApproval;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;
import com.slicktechnologies.shared.common.supportlayer.RaiseTicket;
import com.slicktechnologies.shared.common.workorder.WorkOrder;
import com.slicktechnologies.server.addhocprinting.MultipleServicesRecordPdf;
public class Email extends RemoteServiceServlet implements EmailService {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");

	// Sales q;
	Sales salessq;
	Sales salesso;
	Sales servq;
	Sales servc;
	Customer c;
	Company comp;
	Vendor v;
	Employee emp;
	Approvals approve;
	SalesQuotation salesquot;
	SalesOrder salesord;
	Quotation servquot;
	Contract servcont;
	PurchaseRequisition pr;
	RequsestForQuotation rfq;
	LetterOfIntent loi;
	PurchaseOrder po;
	MaterialIssueNote min;
	MaterialRequestNote mrn;
	MaterialMovementNote mmn;
	GRN grn;
	Invoice invDetails;

	Fumigation fumigation;

	User userEntity;

	List<CustomerBranchDetails> custbranchlist;

	CustomerPayment customerPay;
	CustomerPaySlipPdf paymentPdf;
	FumigationPdf fumigationpdf;

	FumigationAusPdf fumigationAuspdf;

	DeliveryNote deliveryNoteDetails;

	// rohan added this code for customized pdfs
	QuotationPdf servicepdf;
	UniversalPestQuotationPdf universalQuotationPdf;
	UniversalPestContractPdf universalCotractPdf;
	PestoIndiaContractPdf pestoIndiaContractPdf;
	PcambContractpdf pcambContractPdf;
	ContractPdf vcareContractPdf;

	PurchaseOrderPdf  popdf;
	HygeiaContractPdf hygeiaContractPdf;
	NBHCQuotationPdf nbhcPdf;

	RaiseTicket ticket;
	byte[] attachmentbyte;
	public ByteArrayOutputStream pdfstream;
	BlobstoreInputStream quotationstream;
	SalesQuotationPdf sqpdf = null;
	//PurchaseOrderVersionOnePdf pepopdf = null;
	PdfUpdated salesQuotation = null;
	SalesInvoicePdf salesInvoicePdf;
	ServiceInvoicePdf serviceInvoicePdf;
	DeliveryNotePdf delNotePdf;
	CompanyPayment compPayModeDefault;
	InteractionType interaction;
	Inspection insp;
	BillingDocument billingEntity;

	/*
	 * Rohan added this classes for customization documents (Pdf)
	 */
	HygeiaInvoicePdf hygeiaInvoicePdf;
	TaxInvoicePdf vCareTaxinvoicePdf;
	InvoicePdf pmPestInvoicePdf;
	PestoIndiaInvoicePdf pestoIndiaInvoicePdf;

	HygeiaPaymentReceiptPdf hygeaPaymentReceiptPdf;

	// ***************rohan changes **********************
	CustomerTrainingDetails custTrainingDetails;

	InspectionPdf inspdf = null;
	WorkOrder wo;
	WorkOrderPdf wopdf = null;
	ContractRenewalPdf corewal = null;
	String note = "Please refer attached PDF document for more details.";

	/**
	 * This is for Upload Document
	 */
	ByteArrayOutputStream uploadStream;
	BlobstoreService blobstoreService;

	CompanyPayment compPayModeUndefault;
	ContractRenewal conRenewal;
    public Contract con;
    Contract oldCon;
    NewContractRenewalPdf conRenwalNewOne;
    NewContractRenewalVersionOnePdf conRenwalNew;
	/**
	 * 
	 * Date : 18-10-2016 By Anil Release : 30 Sept 2016 Project : PURCHASE
	 * MODIFICATION(NBHC)
	 */
	ServicePo servicPo;
	ServicePoPdf servicePoPdf;

	/**
	 * End
	 */

	/** 12-10-2017 sagar sore[ creating quotation pdf attachment with GST] **/
	GSTQuotationPdf serviceGSTpdf;

	/** 14-10-2017 sagar sore [ ] **/
	ByteArrayOutputStream customQuotUploadStream;

	/** 23-10-2017 sagar sore **/
	SalesGSTInvoice salesGSTInvoice;
	ServiceGSTInvoice serviceGSTInvoice;
	String uploadTandC = "";
	PecoppInvoicePrintPdf pecoppInvoicePdf;
	
	PurchaseOrderVersionOnePdf pecoppurchasepdf;
	PcambInvoicepdf pcambPdf;
	reddysPestControlInvoice rinvpdf;
	PecoppContractPdfPrint pecoppPdf;

 	/**
 	 *  Created By : Nidhi
 	 *  23-11-2017
 	 *  
 	 */
 		Lead introLead;
 	/**
 	 *  End
 	 */
 		/** date 20.6.2018 added by komal for  fumigation **/
 		FumigationCertificateMB fumigationMBpdf;
 		FumigationCertificateALP fumigationALPpdf;
 		FumigationCertificateAFAS fumigationAFASpdf;
 		/**
 	 	 *  Updated By : Viraj
 	 	 *  Date : 02-11-2017
 	 	 *  Description : upload stream for Ordient ventures
 	 	 */
 		ByteArrayOutputStream uploadTC;
 		ByteArrayOutputStream uploadCP;
 		
 		ByteArrayOutputStream uploadCompanyProfile;
 		
 		/** date 28.03.2019 added by komal for SR copy email **/
 		MultipleServicesRecordPdf serviceRecordPdf;
 		PaySlip payslip;
 		PayrollPdfUpdated payrollPdfUpdated;
 		
 		ByteArrayOutputStream uploadedDocument;
 		
 		PurchaseGSTInvoice purchaseGSTinvoice;

 		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		
	public Email() {

	}

	/******************************************** Sales Email Process ***************************************/

	Logger logger = Logger.getLogger("NameOfYourLogger");

	/************************************** Sales Quotation Email ***************************************/

	@Override
	public void initiateSalesQuotationEmail(Sales salesquot)
			throws IllegalArgumentException {
		setSalesQuotation(salesquot);
		initializeSalesQuotation();
		createSalesQuotationAttachment();
		createSalesQuotationUploadDocumentAttachment();
		initializeSalesQuot();
	}

	private void setSalesQuotation(Sales salessq) {
		this.salessq = salessq;
	}

	private void initializeSalesQuotation() {
		SalesQuotation salesquot1 = (SalesQuotation) salessq;
		this.salesquot = salesquot1;

		if (salessq.getCompanyId() != null) {
			comp = ofy().load().type(Company.class)
					.id(salesquot.getCompanyId()).now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		if (salesquot.getCompanyId() != null) {
			c = ofy().load().type(Customer.class)
					.filter("count", salesquot.getCinfo().getCount())
					.filter("companyId", salesquot.getCompanyId()).first()
					.now();
		} else {
			c = ofy().load().type(Customer.class)
					.filter("count", salesquot.getCinfo().getCount()).first()
					.now();
		}

		if (salesquot.getCompanyId() != null) {
			compPayModeDefault = ofy().load().type(CompanyPayment.class)
					.filter("companyId", salesquot.getCompanyId())
					.filter("paymentDefault", true).first().now();
		}

		if (salesquot.getCompanyId() != null) {
			compPayModeUndefault = ofy().load().type(CompanyPayment.class)
					.filter("companyId", salesquot.getCompanyId())
					.filter("paymentDefault", false).first().now();
		}
	}

	private void createSalesQuotationAttachment() {
		salesQuotation = new PdfUpdated();
		salesQuotation.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(salesQuotation.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		salesQuotation.document.open();
		/**
		 * 12-10-2017 sagar sore [ For email attachment in quotation of sales
		 * with GST commented old method and called new GST method]
		 **/
		// salesQuotation.createPdfForEmail(comp, c, salessq,
		// compPayModeDefault, compPayModeUndefault);
		salesQuotation.createPdfForEmailGST(comp, c, salessq,
				compPayModeDefault, compPayModeUndefault);
		salesQuotation.document.close();
	}

	private void createSalesQuotationUploadDocumentAttachment() {
		blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		uploadStream = new ByteArrayOutputStream();

		String urlData = "";
		String fileName = "";

		if (salesquot.getDocument() != null
				&& salesquot.getDocument().getUrl() != null) {
			urlData = salesquot.getDocument().getUrl();
			logger.log(Level.SEVERE, "Doc URL :::: " + urlData);

			fileName = salesquot.getDocument().getName();
			logger.log(Level.SEVERE, "Document Name :::: " + fileName);

			String[] res = urlData.split("blob-key=", 0);
			logger.log(Level.SEVERE, "Splitted Url :::: " + res);

			String blob = res[1].trim();
			logger.log(Level.SEVERE, "Splitted Url Assigned to string  :::: "
					+ blob);

			BlobKey blobKey = null;
			try {
				blobKey = new BlobKey(blob);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);

			// This method read bytes from blobstore
			long inxStart = 0;
			long inxEnd = 1024;
			boolean flag = false;
			byte[] bytes = null;
			do {
				try {
					bytes = blobstoreService.fetchData(blobKey, inxStart,
							inxEnd);
					uploadStream.write(bytes);

					if (bytes.length < 1024)
						flag = true;

					inxStart = inxEnd + 1;
					inxEnd += 1025;

				} catch (Exception e) {
					flag = true;
				}

			} while (!flag);
			logger.log(Level.SEVERE, "Calling Email Method  :::: ");
		}
	}

	private void initializeSalesQuot() {

		String custEmail = c.getEmail();
		System.out.println("Cust EMail" + custEmail);
		logger.log(Level.SEVERE, "Customer Email" + custEmail);

		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add(custEmail);

		// Table header 1
		ArrayList<String> tbl1_header = new ArrayList<String>();
		tbl1_header.add("Code");
		tbl1_header.add("Product Name");
		tbl1_header.add("Product Category");
		tbl1_header.add("Product Quantity");
		tbl1_header.add("Rate");
		// tbl1_header.add("VAT");
		// tbl1_header.add("Service Tax");
		System.out.println("Tbl1 header Size :" + tbl1_header.size());

		List<SalesLineItem> productList = new ArrayList<SalesLineItem>();
		productList = salesquot.getItems();
		// Table 1
		ArrayList<String> tbl1 = new ArrayList<String>();
		for (int i = 0; i < productList.size(); i++) {
			tbl1.add(productList.get(i).getProductCode());
			tbl1.add(productList.get(i).getProductName());
			tbl1.add(productList.get(i).getProductCategory());
			tbl1.add(productList.get(i).getQty() + "");
			tbl1.add(productList.get(i).getPrice() + "");
			// tbl1.add(productList.get(i).getVatTax().getPercentage()+"");
			// tbl1.add(productList.get(i).getServiceTax().getPercentage()+"");
		}
		System.out.println("Tbl1 Size :" + tbl1.size());
		// Table 2 header
		ArrayList<String> tbl2_header = new ArrayList<String>();
		tbl2_header.add("Sr. No.");
		tbl2_header.add("Days");
		tbl2_header.add("Percent");
		tbl2_header.add("Comment");
		System.out.println("Tbl2 header Size :" + tbl2_header.size());

		List<PaymentTerms> paymentTermsList = new ArrayList<PaymentTerms>();
		paymentTermsList = salesquot.getPaymentTermsList();
		// Table 2
		ArrayList<String> tbl2 = new ArrayList<String>();
		for (int i = 0; i < paymentTermsList.size(); i++) {
			tbl2.add((i + 1) + "");
			tbl2.add(paymentTermsList.get(i).getPayTermDays() + "");
			tbl2.add(paymentTermsList.get(i).getPayTermPercent() + "");
			tbl2.add(paymentTermsList.get(i).getPayTermComment());
		}
		System.out.println("Tbl2 Size :" + tbl2.size());

		String header = "Header";
		String footer = "Footer";
		String[] fileName = { "Quotation " + salesquot.getCount() + " "
				+ salesquot.getCinfo().getFullName() };

		String uploadDocName = null;
		if (salesquot.getDocument() != null
				&& salesquot.getDocument().getUrl() != null) {
			uploadDocName = salesquot.getDocument().getName();
		}


		/**
		 * @author Vijay Chougule Date 28-12-2020
		 * Des :- Email From Sales Person and CC in Approver Name and Branch Email id and Company Email Id
		 * send Emails From SendGrid
		 * 
		 */
		ArrayList<String> ccEmailList = new ArrayList<String>();
		String fromEmailId = "";
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.PC_ENABLESENDEMAILSFROMPERSONRESPONSIBLE, this.salessq.getCompanyId())) {
			ccEmailList.add(comp.getEmail());
			fromEmailId = getSalesPersonEmail(this.salessq.getEmployee(),this.salessq.getCompanyId(),this.salessq.getBranch());
			if(fromEmailId==null) {
				fromEmailId = comp.getEmail();
			}
			String approverEmailId = getApproverEmailId(this.salessq.getApproverName(),this.salessq.getCompanyId());
			if(approverEmailId!=null) {
				ccEmailList.add(approverEmailId);
			}
			String branchEmailId = getBranchEmailId(this.salessq.getBranch(), this.salessq.getCompanyId());
			if(branchEmailId!=null) {
				ccEmailList.add(branchEmailId);
			}
		}
		/**
		 * ends here
		 */

		
		
		System.out.println("Header: " + header + " Footer: " + footer);
		try {
			System.out.println("Inside try calling Method...");
			logger.log(Level.SEVERE, "Ready To Call Common Method");
			sendNewEmail(toEmailList, "Quotation", "Quotation",
					salesquot.getCount(), "Sales Quotation",
					salesquot.getCreationDate(), comp, "Product Details",
					tbl1_header, tbl1, "Payment Terms", tbl2_header, tbl2,
					note, null, null, fileName, uploadDocName,ccEmailList,fromEmailId);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/************************************ Sales Quotation End ***************************************/

	/******************************************** Sales Order **************************************************/

	@Override
	public void initiateSalesOrderEmail(Sales salesord)
			throws IllegalArgumentException {
		setSalesOrder(salesord);
		initializeSalesOrder();
		createSalesOrderAttachment();
		createSalesOrderUploadDocumentAttachment();
		initializeSalesOrd();
	}

	private void setSalesOrder(Sales salesso) {
		this.salesso = salesso;
	}

	private void initializeSalesOrder() {
		SalesOrder salesorder1 = (SalesOrder) salesso;
		this.salesord = salesorder1;

		if (salesord.getCompanyId() != null) {
			comp = ofy().load().type(Company.class).id(salesord.getCompanyId())
					.now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		if (salesord.getCompanyId() != null) {
			c = ofy().load().type(Customer.class)
					.filter("count", salesord.getCinfo().getCount())
					.filter("companyId", salesord.getCompanyId()).first().now();
		} else {
			c = ofy().load().type(Customer.class)
					.filter("count", salesord.getCinfo().getCount()).first()
					.now();
		}

		if (salesord.getCompanyId() != null) {
			compPayModeDefault = ofy().load().type(CompanyPayment.class)
					.filter("companyId", salesord.getCompanyId())
					.filter("paymentDefault", true).first().now();
		}

		if (salesord.getCompanyId() != null) {
			compPayModeUndefault = ofy().load().type(CompanyPayment.class)
					.filter("companyId", salesord.getCompanyId())
					.filter("paymentDefault", false).first().now();
		}
	}

	private void createSalesOrderAttachment() {
		salesQuotation = new PdfUpdated();
		salesQuotation.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(salesQuotation.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		salesQuotation.document.open();
		/** 12-10-2017 sagar sore **/
		salesQuotation.createPdfForEmailGST(comp, c, salesso,
				compPayModeDefault, compPayModeUndefault);
		salesQuotation.document.close();

	}

	private void createSalesOrderUploadDocumentAttachment() {
		blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		uploadStream = new ByteArrayOutputStream();

		String urlData = "";
		String fileName = "";

		if (salesord.getDocument() != null
				&& !salesord.getDocument().getUrl().equals("")) {
			urlData = salesord.getDocument().getUrl();
			logger.log(Level.SEVERE, "Doc URL :::: " + urlData);

			fileName = salesord.getDocument().getName();
			logger.log(Level.SEVERE, "Document Name :::: " + fileName);

			String[] res = urlData.split("blob-key=", 0);
			logger.log(Level.SEVERE, "Splitted Url :::: " + res);

			String blob = res[1].trim();
			logger.log(Level.SEVERE, "Splitted Url Assigned to string  :::: "
					+ blob);

			BlobKey blobKey = null;
			try {
				blobKey = new BlobKey(blob);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);

			// This method read bytes from blobstore
			long inxStart = 0;
			long inxEnd = 1024;
			boolean flag = false;
			byte[] bytes = null;
			do {
				try {
					bytes = blobstoreService.fetchData(blobKey, inxStart,
							inxEnd);
					uploadStream.write(bytes);

					if (bytes.length < 1024)
						flag = true;

					inxStart = inxEnd + 1;
					inxEnd += 1025;

				} catch (Exception e) {
					flag = true;
				}

			} while (!flag);
			logger.log(Level.SEVERE, "Calling Email Method  :::: ");
		}
	}

	private void initializeSalesOrd() {

		String custEmail = c.getEmail().trim();

		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add(custEmail);

		// Table header 1
		ArrayList<String> tbl1_header = new ArrayList<String>();
		tbl1_header.add("Code");
		tbl1_header.add("Product Name");
		tbl1_header.add("Product Category");
		tbl1_header.add("Product Quantity");
		tbl1_header.add("Rate");
		// tbl1_header.add("VAT");
		// tbl1_header.add("Service Tax");
		System.out.println("Tbl1 header Size :" + tbl1_header.size());

		List<SalesLineItem> productList = new ArrayList<SalesLineItem>();
		productList = salesord.getItems();
		// Table 1
		ArrayList<String> tbl1 = new ArrayList<String>();
		for (int i = 0; i < productList.size(); i++) {
			tbl1.add(productList.get(i).getProductCode());
			tbl1.add(productList.get(i).getProductName());
			tbl1.add(productList.get(i).getProductCategory());
			tbl1.add(productList.get(i).getQty() + "");
			tbl1.add(productList.get(i).getPrice() + "");
			// tbl1.add(productList.get(i).getVatTax().getPercentage()+"");
			// tbl1.add(productList.get(i).getServiceTax().getPercentage()+"");
		}
		System.out.println("Tbl1 Size :" + tbl1.size());
		// Table 2 header
		ArrayList<String> tbl2_header = new ArrayList<String>();
		tbl2_header.add("Sr. No.");
		tbl2_header.add("Days");
		tbl2_header.add("Percent");
		tbl2_header.add("Comment");
		System.out.println("Tbl2 header Size :" + tbl2_header.size());

		List<PaymentTerms> paymentTermsList = new ArrayList<PaymentTerms>();
		paymentTermsList = salesord.getPaymentTermsList();
		// Table 2
		ArrayList<String> tbl2 = new ArrayList<String>();
		for (int i = 0; i < paymentTermsList.size(); i++) {
			tbl2.add((i + 1) + "");
			tbl2.add(paymentTermsList.get(i).getPayTermDays() + "");
			tbl2.add(paymentTermsList.get(i).getPayTermPercent() + "");
			tbl2.add(paymentTermsList.get(i).getPayTermComment());
		}
		System.out.println("Tbl2 Size :" + tbl2.size());

		String uploadDocName = null;
		if (salesord.getDocument() != null
				&& salesord.getDocument().getUrl() != null) {
			uploadDocName = salesord.getDocument().getName();
		}

		String header = "Header";
		String footer = "Footer";
		String[] fileName = { salesord.getCount() + " "
				+ salesord.getCinfo().getFullName() };
		System.out.println("Header: " + header + " Footer: " + footer);
		
		/**
		 * @author Vijay Chougule Date 28-12-2020
		 * Des :- Email From Sales Person and CC in Approver Name and Branch Email id and Company Email Id
		 * Sending Emails from Sengrid
		 */
		ArrayList<String> ccEmailList = new ArrayList<String>();
		String fromEmailId = "";
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.PC_ENABLESENDEMAILSFROMPERSONRESPONSIBLE, this.salesord.getCompanyId())) {
			ccEmailList.add(comp.getEmail());
			fromEmailId = getSalesPersonEmail(this.salesord.getEmployee(),this.salesord.getCompanyId(),this.salesord.getBranch());
			if(fromEmailId==null) {
				fromEmailId = comp.getEmail();
			}
			String approverEmailId = getApproverEmailId(this.salesord.getApproverName(),this.salesord.getCompanyId());
			if(approverEmailId!=null) {
				ccEmailList.add(approverEmailId);
			}
			String branchEmailId = getBranchEmailId(this.salesord.getBranch(), this.salesord.getCompanyId());
			if(branchEmailId!=null) {
				ccEmailList.add(branchEmailId);
			}
		}
		/**
		 * ends here
		 */
		
		
		try {
			System.out.println("Inside try calling Method...");
			sendNewEmail(toEmailList, "Sales Order", "Sales Order",
					salesord.getCount(), "Sales Order",
					salesord.getCreationDate(), comp, "Product Details",
					tbl1_header, tbl1, "Payment Terms", tbl2_header, tbl2,
					note, null, null, fileName, uploadDocName,ccEmailList,fromEmailId);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/************************************ Sales Order End ***************************************/

	/************************************ Service Quotation *****************************************/

	@Override
	public void initiateServiceQuotationEmail(Sales servquot1)
			throws IllegalArgumentException {
		setServiceQuotation(servquot1);
		initializeServiceQuotation(servquot1);
		System.out.println("servquot.isCustomequotation() flag value ======"
				+ servquot.isCustomequotation());

		if (servquot.isCustomequotation() == true) {
			/** 9-11-2017 sagar sore **/
			createCustomeQuotationAttachment();
			createServiceQuotationUploadDocumentAttachment();
		} else {
			/**
			 * rohan addded this code for sending customized attachement in
			 * Email
			 */
			boolean processConfigurationCheckFlag = false;
			/**
			 * Date : 04-12-2017 By JAYSHREE
			 */
			boolean companyAsLetterHead=false;//By Jayshree
			ProcessConfiguration process = null;
			process = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", servq.getCompanyId())
					.filter("processName", "Quotation")
					.filter("configStatus", true).first().now();
			/**
			 * Date 1/11/2018
			 * Updated By Viraj
			 * Description Email Attachment of Terms And Conditions, Company Profile
			 */
			/** date 13.05.2019 added by komal as it done only for ordient venture *it is
			 * called for all clients and was given error**/
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Company","OnlyForOrdientVentures" , servq.getCompanyId())) {
				createTCUploadDocumentAttachment();
				createCPUploadDocumentAttachment();
			}
			if (process != null) {
				for (int i = 0; i < process.getProcessList().size(); i++) {

					if (process.getProcessList().get(i).getProcessType().trim()
							.equalsIgnoreCase("PestleQuotations")
							&& process.getProcessList().get(i).isStatus() == true) {
						logger.log(Level.SEVERE, "PestlePest quotation");
						createPestlePestQuotationPdf();
						createServiceQuotationUploadDocumentAttachment();
						processConfigurationCheckFlag = true;
						break;

					} else if (process.getProcessList().get(i).getProcessType()
							.trim().equalsIgnoreCase("PestoIndiaQuotations")
							&& process.getProcessList().get(i).isStatus() == true) {
						logger.log(Level.SEVERE, "PestToIndia");
						createPestoIndiaQuotationPdf();
						createServiceQuotationUploadDocumentAttachment();
						processConfigurationCheckFlag = true;
						break;
					} else if (process.getProcessList().get(i).getProcessType()
							.trim().equalsIgnoreCase("NBHCQuotation")
							&& process.getProcessList().get(i).isStatus() == true) {
						logger.log(Level.SEVERE, "NBHC quotation");
						createNBHCQuotationPdf();
						createServiceQuotationUploadDocumentAttachment();
						processConfigurationCheckFlag = true;
						break;
					} else if (process.getProcessList().get(i).getProcessType()
							.trim()
							.equalsIgnoreCase("UniversalPestCustomization")
							&& process.getProcessList().get(i).isStatus() == true) {
						logger.log(Level.SEVERE, "Universal Pest");
						createUniversalPestQuotationPdf();
						createServiceQuotationUploadDocumentAttachment();
						processConfigurationCheckFlag = true;
						break;
					}/**
					 * Date 4/12/2017
					 * By Jayshree
					 * Des.to check the process configration for letter head
					 */
					else if(process.getProcessList().get(i).getProcessType().trim().equalsIgnoreCase("COMPANYASLETTERHEAD")
							&& process.getProcessList().get(i).isStatus() == true){
						companyAsLetterHead=true;
					}
					// else
					// {
					// // This is EVA quotation format
					// createServiceQuotationAttachment();
					// createServiceQuotationUploadDocumentAttachment();
					// processConfigurationCheckFlag = true;
					// }
				}
				/**
				 * 30-10-2017 sagar sore [when confuguration status of processes
				 * is false will be executed]
				 **/
				if (processConfigurationCheckFlag == false) {
					logger.log(Level.SEVERE, "EVA pest");
					// This is EVA quotation format
					createServiceQuotationAttachment(companyAsLetterHead);
					createServiceQuotationUploadDocumentAttachment();

				}
				/**
				 * ends here
				 */

			} else {
				// This is EVA quotation format
				createServiceQuotationAttachment(companyAsLetterHead);
				createServiceQuotationUploadDocumentAttachment();
			}

		}
		initializeServQuot();
	}

/**
 * Date 2/11/2018
 * Updated By Viraj
 * Description Email Attachment of Terms And Conditions, Company Profile
 */
	private void createTCUploadDocumentAttachment() {
		
//		if (salessq.getId() != null) {
//			comp = ofy().load().type(Company.class)
//					.id(salesquot.getId()).now();
//		}
		
		blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		uploadTC = new ByteArrayOutputStream();

		String urlData = "";
		String fileName = "";
		
		if(comp.getTermsAndCondition() != null) {
			urlData = comp.getTermsAndCondition().getUrl();
			logger.log(Level.SEVERE, "Doc URL :::: " + urlData);

			fileName = comp.getTermsAndCondition().getName();
			logger.log(Level.SEVERE, "Document Name :::: " + fileName);

			String[] res = urlData.split("blob-key=", 0);
			logger.log(Level.SEVERE, "Splitted Url :::: " + res);

			String blob = res[1].trim();
			logger.log(Level.SEVERE, "Splitted Url Assigned to string  :::: "
					+ blob);

			BlobKey blobKey = null;
			try {
				blobKey = new BlobKey(blob);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);

			// This method read bytes from blobstore
			long inxStart = 0;
			long inxEnd = 1024;
			boolean flag = false;
			byte[] bytes = null;
			do {
				try {
					bytes = blobstoreService.fetchData(blobKey, inxStart,
							inxEnd);
					// uploadStream.write(bytes);
					logger.log(Level.SEVERE, "writing to custom upload stream");
					uploadTC.write(bytes);
					if (bytes.length < 1024)
						flag = true;

					inxStart = inxEnd + 1;
					inxEnd += 1025;

				} catch (Exception e) {
					flag = true;
				}

			} while (!flag);
			logger.log(Level.SEVERE, "Calling Email Method  :::: ");
		}
	}
	
	private void createCPUploadDocumentAttachment() {
		
//		if (salessq.getId() != null) {
//			comp = ofy().load().type(Company.class)
//					.id(salesquot.getId()).now();
//		}
		
		String urlData1 = "";
		String fileName1 = "";
		
		uploadCP = new ByteArrayOutputStream();
		blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		
		if(comp.getCompanyProfile() != null) {
			urlData1 = comp.getCompanyProfile().getUrl();
			logger.log(Level.SEVERE, "Doc URL :::: " + urlData1);

			fileName1 = comp.getCompanyProfile().getName();
			logger.log(Level.SEVERE, "Document Name :::: " + fileName1);

			String[] res1 = urlData1.split("blob-key=", 0);
			logger.log(Level.SEVERE, "Splitted Url :::: " + res1);

			String blob1 = res1[1].trim();
			logger.log(Level.SEVERE, "Splitted Url Assigned to string  :::: "
					+ blob1);

			BlobKey blobKey1 = null;
			try {
				blobKey1 = new BlobKey(blob1);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "BlobKey1  :::: " + blobKey1);

			// This method read bytes from blobstore
			long inxStart = 0;
			long inxEnd = 1024;
			boolean flag = false;
			byte[] bytes = null;
			do {
				try {
					bytes = blobstoreService.fetchData(blobKey1, inxStart,
							inxEnd);
					// uploadStream.write(bytes);
					logger.log(Level.SEVERE, "writing to custom upload stream");
					uploadCP.write(bytes);
					if (bytes.length < 1024)
						flag = true;

					inxStart = inxEnd + 1;
					inxEnd += 1025;

				} catch (Exception e) {
					flag = true;
				}

			} while (!flag);
			logger.log(Level.SEVERE, "Calling Email Method  :::: ");
		}
	}
/** Ends **/
	private void createPestlePestQuotationPdf() {

		if (servq.getGroup().equalsIgnoreCase("Normal Quotation")) {
			PestleQuotationPdf pdf = new PestleQuotationPdf();
			pdf = new PestleQuotationPdf();
			pdf.document = new Document();
			pdfstream = new ByteArrayOutputStream();
			try {
				PdfWriter.getInstance(pdf.document, pdfstream);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			pdf.document.open();
			pdf.getPestleQuotation(servq.getId());
			pdf.createPdf("no");
			pdf.document.close();
		} else if (servq.getGroup().equalsIgnoreCase("Five Years Quotation")) {

			PestleFiveYearsQuotation pdffive = new PestleFiveYearsQuotation();
			pdffive = new PestleFiveYearsQuotation();
			pdffive.document = new Document();
			pdfstream = new ByteArrayOutputStream();
			try {
				PdfWriter.getInstance(pdffive.document, pdfstream);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			pdffive.document.open();
			pdffive.getPestlefiveQuotation(servq.getId());
			pdffive.createPdf("no");
			pdffive.document.close();
		}
	}

	private void createPestoIndiaQuotationPdf() {

	}

	private void createNBHCQuotationPdf() {

		NBHCQuotationPdf nbhcPdf = new NBHCQuotationPdf();
		nbhcPdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(nbhcPdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		nbhcPdf.document.open();
		nbhcPdf.setNBHCQuotation(servq.getId());
		nbhcPdf.createPdf("no");

		// servicepdf.createPdfForEmail(comp, c, servq);
		nbhcPdf.document.close();
	}

	private void createUniversalPestQuotationPdf() {

		universalQuotationPdf = new UniversalPestQuotationPdf();
		universalQuotationPdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(universalQuotationPdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		universalQuotationPdf.document.open();
		universalQuotationPdf.setservicequotation(servq.getId());
		universalQuotationPdf.createPdf("no");

		// servicepdf.createPdfForEmail(comp, c, servq);
		universalQuotationPdf.document.close();
	}

	private void setServiceQuotation(Sales servq) {
		this.servq = servq;
	}

	private void createCustomeQuotationAttachment() {
		blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		/**
		 * 14-10-2017 sagar sore [To avoid conflict of streams while sending
		 * custom doc]
		 **/
		// uploadStream = new ByteArrayOutputStream();
		customQuotUploadStream = new ByteArrayOutputStream();

		String urlData = "";
		String fileName = "";

		if (servquot.getCustomeQuotationUpload() != null
				&& !servquot.getCustomeQuotationUpload().getUrl().equals("")) {
			urlData = servquot.getCustomeQuotationUpload().getUrl();
			logger.log(Level.SEVERE, "Doc URL :::: " + urlData);

			fileName = servquot.getCustomeQuotationUpload().getName();
			logger.log(Level.SEVERE, "Document Name :::: " + fileName);

			String[] res = urlData.split("blob-key=", 0);
			logger.log(Level.SEVERE, "Splitted Url :::: " + res);

			String blob = res[1].trim();
			logger.log(Level.SEVERE, "Splitted Url Assigned to string  :::: "
					+ blob);

			BlobKey blobKey = null;
			try {
				blobKey = new BlobKey(blob);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);

			// This method read bytes from blobstore
			long inxStart = 0;
			long inxEnd = 1024;
			boolean flag = false;
			byte[] bytes = null;
			do {
				try {
					bytes = blobstoreService.fetchData(blobKey, inxStart,
							inxEnd);

					/** 14-10-2017 sagar sore [To send customized doc] ***/
					// uploadStream.write(bytes);
					logger.log(Level.SEVERE, "writing to custom upload stream");
					customQuotUploadStream.write(bytes);
					if (bytes.length < 1024)
						flag = true;

					inxStart = inxEnd + 1;
					inxEnd += 1025;

				} catch (Exception e) {
					flag = true;
				}

			} while (!flag);
			logger.log(Level.SEVERE, "Calling Email Method  :::: ");
		}
	}

	private void initializeServiceQuotation(Sales servquot1) {
		Quotation servq1 = (Quotation) servquot1;
		this.servquot = servq1;

		if (servquot.getCompanyId() != null) {
			comp = ofy().load().type(Company.class).id(servquot.getCompanyId())
					.now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		if (servquot.getCompanyId() != null) {
			c = ofy().load().type(Customer.class)
					.filter("count", servquot.getCinfo().getCount())
					.filter("companyId", servquot.getCompanyId()).first().now();
		} else {
			c = ofy().load().type(Customer.class)
					.filter("count", servquot.getCinfo().getCount()).first()
					.now();
		}

		if (servquot.getCompanyId() != null) {
			compPayModeDefault = ofy().load().type(CompanyPayment.class)
					.filter("companyId", servquot.getCompanyId())
					.filter("paymentDefault", true).first().now();
		}

		if (servquot.getCompanyId() != null) {
			compPayModeUndefault = ofy().load().type(CompanyPayment.class)
					.filter("companyId", servquot.getCompanyId()).first().now();
		}

	}

	//By jayshree
	//Date 4/12/2017
	//Des.to pass the parameter to method
	private void createServiceQuotationAttachment(boolean companyAsLetterHead) {
		/**
		 * 1-11-2017 sagar sore [sending pdf quotation with gst by checking
		 * process configuration]
		 **/
		ProcessConfiguration processConfig = ofy().load()
				.type(ProcessConfiguration.class)
				.filter("companyId", servquot.getCompanyId())
				.filter("processName", "Quotation")
				.filter("configStatus", true).first().now();
		boolean flag = true;
		for (int k = 0; k < processConfig.getProcessList().size(); k++) {
			logger.log(Level.SEVERE, " Process type "
					+ processConfig.getProcessList().get(k).getProcessType());
			if (processConfig.getProcessList().get(k).getProcessType().trim()
					.equalsIgnoreCase("GSTQuotationPdf")
					&& processConfig.getProcessList().get(k).isStatus() == true) {
				logger.log(Level.SEVERE,
						"Creating quotation pdf with gst for mail attachment ");
				flag = false;
				serviceGSTpdf = new GSTQuotationPdf();
				serviceGSTpdf.document = new Document();

				pdfstream = new ByteArrayOutputStream();
				try {
					PdfWriter.getInstance(serviceGSTpdf.document, pdfstream);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
				serviceGSTpdf.document.open();
				/** 1-11-2017 sagar sore **/
				serviceGSTpdf.createPdfForEmailGST(comp, c, servquot);
				serviceGSTpdf.document.close();
				flag = false;
				break;
			}

			if (processConfig.getProcessList().get(k).getProcessType().trim()
					.equalsIgnoreCase("PestleQuotations")
					&& processConfig.getProcessList().get(k).isStatus() == true) {
				logger.log(Level.SEVERE,
						"Creating pestle quot pdf with gst for mail attachment ");

				PestleQuotationPdf pdf = new PestleQuotationPdf();
				pdf.document = new Document();
				Document document = pdf.document;
				pdfstream = new ByteArrayOutputStream();
				try {
					PdfWriter.getInstance(pdf.document, pdfstream);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
				document.open();
				pdf.getPestleQuotation(servquot.getId());
				pdf.createPdf("no");
				document.close();
				flag = false;
				break;
			}
			if (processConfig.getProcessList().get(k).getProcessType().trim()
					.equalsIgnoreCase("PestoIndiaQuotations")
					&& processConfig.getProcessList().get(k).isStatus() == true) {
				logger.log(Level.SEVERE,
						"Creating pestto india pdf with gst for mail attachment ");

				PestoIndiaQuotationPdf pdf = new PestoIndiaQuotationPdf();
				pdf.document = new Document();
				Document document = pdf.document;
				pdfstream = new ByteArrayOutputStream();
				try {
					PdfWriter.getInstance(pdf.document, pdfstream);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
				document.open();
				System.out.println("Document Opened");
				pdf.getQuotation(servquot.getId());
				pdf.createPdf("no");
				document.close();
				flag = false;
				break;
			}
			if (processConfig.getProcessList().get(k).getProcessType().trim()
					.equalsIgnoreCase("NBHCQuotation")
					&& processConfig.getProcessList().get(k).isStatus() == true) {
				logger.log(Level.SEVERE,
						"Creating pestto india pdf with gst for mail attachment ");

				NBHCQuotationPdf nbhcPdf = new NBHCQuotationPdf();
				nbhcPdf.document = new Document();
				Document document = nbhcPdf.document;

				pdfstream = new ByteArrayOutputStream();
				try {
					PdfWriter.getInstance(nbhcPdf.document, pdfstream);
				} catch (DocumentException e) {
					e.printStackTrace();
				}

				document.open();
				nbhcPdf.setNBHCQuotation(servquot.getId());
				nbhcPdf.createPdf("no");
				document.close();
				flag = false;
				break;
			}

			if (processConfig.getProcessList().get(k).getProcessType().trim()
					.equalsIgnoreCase("UniversalPestCustomization")
					&& processConfig.getProcessList().get(k).isStatus() == true) {
				logger.log(Level.SEVERE,
						"Creating pestto india pdf with gst for mail attachment ");

				UniversalPestQuotationPdf universalQuotationPdf = new UniversalPestQuotationPdf();
				universalQuotationPdf.document = new Document();
				Document document = universalQuotationPdf.document;
				pdfstream = new ByteArrayOutputStream();
				try {
					PdfWriter.getInstance(nbhcPdf.document, pdfstream);
				} catch (DocumentException e) {
					e.printStackTrace();
				}

				document.open();

				universalQuotationPdf.setservicequotation(servquot.getId());
				universalQuotationPdf.createPdf("no");
				document.close();
				flag = false;
				break;
			}

			if (processConfig.getProcessList().get(k).getProcessType().trim()
					.equalsIgnoreCase("QuotationForPecop")
					&& processConfig.getProcessList().get(k).isStatus() == true) {
				
				pecoppQuotationPDF();

			}
		}
		if (flag) {
			/** old code **/
			logger.log(Level.SEVERE,
					"creating quotation pdf without gst for mail attachment ");
			servicepdf = new QuotationPdf();
			servicepdf.document = new Document();
			pdfstream = new ByteArrayOutputStream();
			try {
				PdfWriter.getInstance(servicepdf.document, pdfstream);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			servicepdf.document.open();
			// servicepdf.createPdfForEmail(comp, c, servq);
			//By jayshree
			//Date 30/11/2017
			//Des.to call the create pdf for mail format
			
//			servicepdf.getQuotation(servquot.getId(), "plane");
//			servicepdf.createPdf("plane");
			
			if (companyAsLetterHead) {
				servicepdf.getQuotation(servquot.getId(), "no");
				servicepdf.createPdf("no");
			} else {
				servicepdf.getQuotation(servquot.getId(), "plane");
				servicepdf.createPdf("plane");
			}
			
			servicepdf.document.close();
			/** end **/
		}
	}

	private void pecoppQuotationPDF() {

		QuotationVersionOnePdf peq = new QuotationVersionOnePdf();
		peq.document = new Document();
		Document document = peq.document;
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(peq.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		document.open();
		peq.setPcservicequotation(servquot.getId(), "no");
		peq.createPdf("no");
		document.close();

	}

	private void createServiceQuotationUploadDocumentAttachment() {
		blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		uploadStream = new ByteArrayOutputStream();

		String urlData = "";
		String fileName = "";

		if (servquot.getDocument() != null
				&& !servquot.getDocument().getUrl().equals("")) {
			urlData = servquot.getDocument().getUrl();
			logger.log(Level.SEVERE, "Doc URL :::: " + urlData);

			/** 9-11 -2017 sagar sore[filename assigned at different place] **/
			// fileName=salesquot.getDocument().getName();
			// logger.log(Level.SEVERE,"Document Name :::: "+fileName);

			String[] res = urlData.split("blob-key=", 0);
			logger.log(Level.SEVERE, "Splitted Url :::: " + res);

			String blob = res[1].trim();
			logger.log(Level.SEVERE, "Splitted Url Assigned to string  :::: "
					+ blob);

			BlobKey blobKey = null;
			try {
				blobKey = new BlobKey(blob);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);

			// This method read bytes from blobstore
			long inxStart = 0;
			long inxEnd = 1024;
			boolean flag = false;
			byte[] bytes = null;
			do {
				try {
					bytes = blobstoreService.fetchData(blobKey, inxStart,
							inxEnd);
					uploadStream.write(bytes);

					if (bytes.length < 1024)
						flag = true;

					inxStart = inxEnd + 1;
					inxEnd += 1025;

				} catch (Exception e) {
					flag = true;
				}

			} while (!flag);
			logger.log(Level.SEVERE, "Calling Email Method  :::: ");
		}
	}

	private void initializeServQuot() {

		String custEmail = c.getEmail().trim();
		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add(custEmail);

		// Table header 1
		ArrayList<String> tbl1_header = new ArrayList<String>();
		tbl1_header.add("Code");
		tbl1_header.add("Product Name");
		tbl1_header.add("Product Category");
		tbl1_header.add("Product Quantity");
		tbl1_header.add("Rate");
		// tbl1_header.add("VAT");
		// tbl1_header.add("Service Tax");
		System.out.println("Tbl1 header Size :" + tbl1_header.size());

		List<SalesLineItem> productList = new ArrayList<SalesLineItem>();
		productList = servquot.getItems();
		// Table 1
		ArrayList<String> tbl1 = new ArrayList<String>();
		for (int i = 0; i < productList.size(); i++) {
			tbl1.add(productList.get(i).getProductCode());
			tbl1.add(productList.get(i).getProductName());
			tbl1.add(productList.get(i).getProductCategory());
			tbl1.add(productList.get(i).getQty() + "");
			tbl1.add(productList.get(i).getPrice() + "");
			// tbl1.add(productList.get(i).getVatTax().getPercentage()+"");
			// tbl1.add(productList.get(i).getServiceTax().getPercentage()+"");
		}
		System.out.println("Tbl1 Size :" + tbl1.size());
		// Table 2 header
		ArrayList<String> tbl2_header = new ArrayList<String>();
		tbl2_header.add("Sr. No.");
		tbl2_header.add("Days");
		tbl2_header.add("Percent");
		tbl2_header.add("Comment");
		System.out.println("Tbl2 header Size :" + tbl2_header.size());

		List<PaymentTerms> paymentTermsList = new ArrayList<PaymentTerms>();
		paymentTermsList = servquot.getPaymentTermsList();
		// Table 2
		ArrayList<String> tbl2 = new ArrayList<String>();
		for (int i = 0; i < paymentTermsList.size(); i++) {
			tbl2.add((i + 1) + "");
			tbl2.add(paymentTermsList.get(i).getPayTermDays() + "");
			tbl2.add(paymentTermsList.get(i).getPayTermPercent() + "");
			tbl2.add(paymentTermsList.get(i).getPayTermComment());
		}
		System.out.println("Tbl2 Size :" + tbl2.size());

		/**
		 * 4-11-2017 sagar sore[ commented and added new code to send
		 * attachments]
		 **/
		String uploadDocName = null;
		//
		// if(servquot.isCustomequotation()==true)
		// {
		// if(servquot.getCustomeQuotationUpload()!=null&&!servquot.getCustomeQuotationUpload().getUrl().equals("")){
		// uploadDocName=servquot.getCustomeQuotationUpload().getName();
		// }
		// }
		// else
		// {
		// if(servquot.getDocument()!=null&&servquot.getDocument().getUrl()!=null){
		// uploadDocName=servquot.getDocument().getName();
		// }
		// }
		// System.out.println("info**************"+servquot.getCount()+" "+servquot.getCinfo().getFullName());
		//
		//
		// String[]
		// fileName={servquot.getCount()+" "+servquot.getCinfo().getFullName()};
		//
		//
		// if(servquot.isCustomequotation()==true)
		// {
		// try {
		// System.out.println("Inside try calling Method...");
		// /**1-11-17 sagar sore**/
		// // sendNewEmail(toEmailList, "Quotation", "Quotation",
		// servquot.getCount(), "Service Quotation",servquot.getCreationDate(),
		// comp, "Product Details",tbl1_header, tbl1, "Payment Terms",
		// tbl2_header, tbl2,note, null, null, fileName,uploadDocName );
		// sendNewEmailForGST(toEmailList, "Quotation",
		// "Quotation",servquot.getCount(),
		// "Service Quotation",servquot.getCreationDate(), comp,
		// "Product Details",tbl1_header, tbl1, "Payment Terms", tbl2_header,
		// tbl2,note, null, null, null, uploadTandC,uploadDocName);
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		// }
		// else
		// {
		// try {
		// System.out.println("Inside try calling Method...");
		// sendNewEmail(toEmailList, "Quotation", "Quotation",
		// servquot.getCount(), "Service Quotation", servquot.getCreationDate(),
		// comp,
		// "Product Details",tbl1_header, tbl1, "Payment Terms",tbl2_header,
		// tbl2,note,null, null, fileName,uploadDocName);
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		// }
		//
		if (servquot.getCustomeQuotationUpload() != null
				&& !servquot.getCustomeQuotationUpload().getUrl().equals("")) {
			uploadDocName = servquot.getCustomeQuotationUpload().getName();
		}

		if (servquot.getDocument() != null
				&& servquot.getDocument().getUrl() != null) {
			uploadTandC = servquot.getDocument().getName();
		}
		logger.log(Level.SEVERE, "info**************" + servquot.getCount()
				+ " " + servquot.getCinfo().getFullName());
		
		

		/**
		 * @author Vijay Chougule Date 28-12-2020
		 * Des :- Email From Sales Person and CC in Approver Name and Branch Email id and Company Email Id
		 * send Emails From SendGrid
		 * 
		 */
		ArrayList<String> ccEmailList = new ArrayList<String>();
		String fromEmailId = "";
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.PC_ENABLESENDEMAILSFROMPERSONRESPONSIBLE, this.servquot.getCompanyId())) {
			ccEmailList.add(comp.getEmail());
			fromEmailId = getSalesPersonEmail(this.servquot.getEmployee(),this.servquot.getCompanyId(),this.servquot.getBranch());
			if(fromEmailId==null) {
				fromEmailId = comp.getEmail();
			}
			String approverEmailId = getApproverEmailId(this.servquot.getApproverName(),this.servquot.getCompanyId());
			if(approverEmailId!=null) {
				ccEmailList.add(approverEmailId);
			}
			String branchEmailId = getBranchEmailId(this.servquot.getBranch(), this.servquot.getCompanyId());
			if(branchEmailId!=null) {
				ccEmailList.add(branchEmailId);
			}
		}
		/**
		 * ends here
		 */
		
		

		String[] fileName = { servquot.getCount() + " "
				+ servquot.getCinfo().getFullName() };

		if (servquot.isCustomequotation() == true) {
			if (servquot.getDocument() != null) {
				try {
					logger.log(Level.SEVERE,
							"Inside try calling Method...  sending t&c attachment and custom quotation doc");
					fileName[0] = uploadTandC;
					// /**1-11-17 sagar sore**/
					// sendNewEmail(toEmailList, "Quotation", "Quotation",
					// servquot.getCount(), "Service Quotation",
					// servquot.getCreationDate(), comp, "Product Details",
					// tbl1_header, tbl1, "Payment Terms", tbl2_header, tbl2,
					// note, null, null, fileName,uploadDocName );
					sendNewEmailForGST(toEmailList, "Quotation", "Quotation",
							servquot.getCount(), "Service Quotation",
							servquot.getCreationDate(), comp,
							"Product Details", tbl1_header, tbl1,
							"Payment Terms", tbl2_header, tbl2, note, null,
							null, null, uploadTandC, uploadDocName,ccEmailList,fromEmailId);

				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				try {
					logger.log(Level.SEVERE,
							"Inside try calling Method...  sending custom quotation only");
					if (servquot.getCustomeQuotationUpload() != null
							&& !servquot.getCustomeQuotationUpload().getUrl()
									.equals(""))
						sendNewEmailForGST(toEmailList, "Quotation",
								"Quotation", servquot.getCount(),
								"Service Quotation",
								servquot.getCreationDate(), comp,
								"Product Details", tbl1_header, tbl1,
								"Payment Terms", tbl2_header, tbl2, note, null,
								null, null, null, uploadDocName,ccEmailList,fromEmailId);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		else {
			try {

				if (servquot.getDocument() != null) {
					logger.log(Level.SEVERE,
							"Inside try calling Method...  t&c with original doc");

					sendNewEmail(toEmailList, "Quotation", "Quotation",
							servquot.getCount(), "Service Quotation",
							servquot.getCreationDate(), comp,
							"Product Details", tbl1_header, tbl1,
							"Payment Terms", tbl2_header, tbl2, note, null,
							null, fileName, uploadTandC,ccEmailList,fromEmailId);
				} else {
					logger.log(Level.SEVERE,
							"Inside try calling Method...  original doc");
					sendNewEmail(toEmailList, "Quotation", "Quotation",
							servquot.getCount(), "Service Quotation",
							servquot.getCreationDate(), comp,
							"Product Details", tbl1_header, tbl1,
							"Payment Terms", tbl2_header, tbl2, note, null,
							null, fileName, null,ccEmailList,fromEmailId);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	/*********************************** Service Quotation End ****************************************/

	/************************************* Contract ***************************************************/

	@Override
	public void initiateServiceContractEmail(Sales servcont)
			throws IllegalArgumentException {
		setServiceContract(servcont);
		initializeServiceContract();

		initializeServCont();
		
	}

	private void sendCustomizedPdfAsperClient() {

		/**
		 * rohan addded this code for sending customized attachement in Email
		 */
		boolean flag = false;
		/**
		 * Date : 04-12-2017 BY JAYSHREE
		 */
		boolean companyAsLetterHead=false;
		ProcessConfiguration process = null;
		process = ofy().load().type(ProcessConfiguration.class)
				.filter("companyId", servcont.getCompanyId())
				.filter("processName", "Contract").filter("configStatus", true)
				.first().now();

		if (process != null) {
			for (int i = 0; i < process.getProcessList().size(); i++) {

				if (process.getProcessList().get(i).getProcessType().trim()
						.equalsIgnoreCase("PestoIndiaQuotations")
						&& process.getProcessList().get(i).isStatus() == true) {

					createPestoIndiaContractPdf();
					createContractUploadDocumentAttachment();
					flag = true;
					break;

				}

				else if (process.getProcessList().get(i).getProcessType()
						.trim().equalsIgnoreCase("VCareCustomization")
						&& process.getProcessList().get(i).isStatus() == true) {

					createVcareContractPdf();
					createContractUploadDocumentAttachment();
					flag = true;
					break;

				}

				else if (process.getProcessList().get(i).getProcessType()
						.trim().equalsIgnoreCase("HygeiaPestManagement")
						&& process.getProcessList().get(i).isStatus() == true) {
					createHygeiaPestPdf();
					createContractUploadDocumentAttachment();
					flag = true;
					break;
				}

				else if (process.getProcessList().get(i).getProcessType()
						.trim().equalsIgnoreCase("AMCForGenesis")
						&& process.getProcessList().get(i).isStatus() == true) {
					createGenesisAmcPdf();
					createContractUploadDocumentAttachment();
					flag = true;
					break;
				} else if (process.getProcessList().get(i).getProcessType()
						.trim().equalsIgnoreCase("UniversalPestCustomization")
						&& process.getProcessList().get(i).isStatus() == true) {
					createUniversalPestContractPdf();
					createContractUploadDocumentAttachment();
					flag = true;
					break;
				}

				else if (process.getProcessList().get(i).getProcessType()
						.trim().equalsIgnoreCase("PCAMBCustomization")
						&& process.getProcessList().get(i).isStatus() == true) {
					createPCAMBContractPdf();
					createContractUploadDocumentAttachment();
					flag = true;
					break;
				}
				/**
				 * 2-10-2017 sagar sore [sending mail of pdf for following
				 * processes]
				 **/
				else if (process.getProcessList().get(i).getProcessType()
						.trim().equalsIgnoreCase("OnlyForPecopp")
						&& process.getProcessList().get(i).isStatus() == true) {
					createPecoppContractAttachment();
					createContractUploadDocumentAttachment();
					flag = true;
					break;
				}/**
				 * Date 4/12/2017
				 * dev.By Jayshree 
				 * Des.To check the prosecc configration for letter haed
				 */
				else if(process.getProcessList().get(i).getProcessType()
						.trim().equalsIgnoreCase("COMPANYASLETTERHEAD")
						&& process.getProcessList().get(i).isStatus() == true){
					companyAsLetterHead=true;
				}
				
			}
			
			/**
			 * 2-10-2017 sagar sore [ for sending pdf if status of all
			 * processes is false]
			 **/
			if (!flag) {
				// This is EVA quotation format
				createContractAttachment(companyAsLetterHead);
				createContractUploadDocumentAttachment();
			}

			
		} else {
			// This is EVA Contract format
			createContractAttachment(companyAsLetterHead);
			createContractUploadDocumentAttachment();
		}
	}

	// pest V Care pdf
	private void createVcareContractPdf() {

		vcareContractPdf = new ContractPdf();
		vcareContractPdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(vcareContractPdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		vcareContractPdf.document.open();
		vcareContractPdf.setContract(servcont.getId());
		vcareContractPdf.createPdf("no");
		vcareContractPdf.document.close();
	}

	private void createHygeiaPestPdf() {

		pcambContractPdf = new PcambContractpdf();
		pcambContractPdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(pcambContractPdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		pcambContractPdf.document.open();
		pcambContractPdf.setContract(servcont.getId());
		pcambContractPdf.createPdf();
		pcambContractPdf.document.close();
	}

	// pest Genesis pdf
	private void createGenesisAmcPdf() {

		hygeiaContractPdf = new HygeiaContractPdf();
		hygeiaContractPdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(hygeiaContractPdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		hygeiaContractPdf.document.open();
		hygeiaContractPdf.setpdfcon(servcont.getId());
		hygeiaContractPdf.createPdf();
		hygeiaContractPdf.document.close();
	}

	private void createPCAMBContractPdf() {

		pcambContractPdf = new PcambContractpdf();
		pcambContractPdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(pcambContractPdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		pcambContractPdf.document.open();
		pcambContractPdf.setContract(servcont.getId());
		pcambContractPdf.createPdf();
		pcambContractPdf.document.close();
	}

	private void createPestoIndiaContractPdf() {

		pestoIndiaContractPdf = new PestoIndiaContractPdf();
		pestoIndiaContractPdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(pestoIndiaContractPdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		pestoIndiaContractPdf.document.open();
		pestoIndiaContractPdf.getContract(servcont.getId());
		pestoIndiaContractPdf.createPdf("no");
		pestoIndiaContractPdf.document.close();
	}

	// universal pdf
	private void createUniversalPestContractPdf() {

		universalCotractPdf = new UniversalPestContractPdf();
		universalCotractPdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(universalCotractPdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		universalCotractPdf.document.open();
		universalCotractPdf.setservicequotation(servcont.getId());
		universalCotractPdf.createPdf("no");
		universalCotractPdf.document.close();
	}

	/** 1-11-2017 sagar sore [To create pdf for pecopp] **/
	private void createPecoppContractAttachment() {
		Company companyEntity = comp;
		if(comp==null){
			 companyEntity = ofy().load().type(Company.class).filter("companyId", servcont.getCompanyId()).first().now();
		}
		pecoppPdf = new PecoppContractPdfPrint();
		pecoppPdf.document = new Document();
		Document document = pecoppPdf.document;
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(pecoppPdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		pecoppPdf.document.open();
		pecoppPdf.loadpecopp(servcont.getId());
//		pecoppPdf.createPrintSample();
		if(companyEntity!=null && companyEntity.getUploadHeader()!=null && companyEntity.getUploadFooter()!=null)
			pecoppPdf.createPrintPdf("no");
		else	
			pecoppPdf.createPrintPdf("yes");
		
		pecoppPdf.document.close();
	}

	public void setServiceContract(Sales servc) {
		this.servc = servc;
	}

	public void initializeServiceContract() {
		Contract serc1 = (Contract) servc;
		this.servcont = serc1;

		if (servcont.getCompanyId() != null) {
			comp = ofy().load().type(Company.class).id(servcont.getCompanyId())
					.now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		if (servcont.getCompanyId() != null) {
			c = ofy().load().type(Customer.class)
					.filter("count", servcont.getCinfo().getCount())
					.filter("companyId", servcont.getCompanyId()).first().now();
		} else {
			c = ofy().load().type(Customer.class)
					.filter("count", servcont.getCinfo().getCount()).first()
					.now();
		}

		if (servcont.getCompanyId() != null) {
			compPayModeDefault = ofy().load().type(CompanyPayment.class)
					.filter("companyId", servcont.getCompanyId())
					.filter("paymentDefault", true).first().now();
		}

		if (servcont.getCompanyId() != null) {
			compPayModeUndefault = ofy().load().type(CompanyPayment.class)
					.filter("companyId", servcont.getCompanyId()).first().now();
		}

		// rohan added this code on 21/3/2017 for sending customized pdf in
		// contract email
		sendCustomizedPdfAsperClient();
	}

	private void createContractAttachment(boolean companyAsLetterHead) {
		/**
		 * 12-10-2017 sagar sore [ checking process configuration to send GST
		 * quotation pdf as attachment in mail; old code commented]
		 **/
		boolean flag = false;
		ProcessConfiguration processConfig = ofy().load()
				.type(ProcessConfiguration.class)
				.filter("companyId", servcont.getCompanyId())
				.filter("processName", "Contract").filter("configStatus", true)
				.first().now();

		for (int k = 0; k < processConfig.getProcessList().size(); k++) {
			logger.log(Level.SEVERE, " Process type "
					+ processConfig.getProcessList().get(k).getProcessType());
			if (processConfig.getProcessList().get(k).getProcessType().trim()
					.equalsIgnoreCase("GSTQuotationPdf")
					&& processConfig.getProcessList().get(k).isStatus() == true) {
				logger.log(Level.SEVERE,
						"Creating quotation pdf with gst for mail attachment");
				serviceGSTpdf = new GSTQuotationPdf();
				serviceGSTpdf.document = new Document();
				pdfstream = new ByteArrayOutputStream();
				try {
					PdfWriter.getInstance(serviceGSTpdf.document, pdfstream);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
				serviceGSTpdf.document.open();
				serviceGSTpdf.createPdfForEmailGST(comp, c, servc);
				serviceGSTpdf.document.close();
				flag = true;
				break;
			}
			
		}

		// if
		// (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("QuotationPdf")
		// && processConfig.getProcessList().get(k).isStatus() == true) {
		if (!flag) {
			servicepdf = new QuotationPdf();
			servicepdf.document = new Document();
			pdfstream = new ByteArrayOutputStream();
			try {
				PdfWriter.getInstance(servicepdf.document, pdfstream);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			servicepdf.document.open();

			//Date 29/11/2017
			//By jayshree
			//Des, to Call the create pdf for mail format pdf
//			servicepdf.getContract(servcont.getId(),"plane");
//			servicepdf.createPdf("plane");
			if (companyAsLetterHead) {
				servicepdf.getContract(servcont.getId(),"no");
				servicepdf.createPdf("no");
			} else {
				servicepdf.getContract(servcont.getId(),"plane");
				servicepdf.createPdf("plane");
			}
//			servicepdf.createPdfForEmail(comp, c, servc);
			//End By jayshree

			servicepdf.document.close();

		}

	}

	private void createContractUploadDocumentAttachment() {
		blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		uploadStream = new ByteArrayOutputStream();

		String urlData = "";
		String fileName = "";

		if (servcont.getDocument() != null
				&& servcont.getDocument().getUrl() != null) {
			urlData = servcont.getDocument().getUrl();
			logger.log(Level.SEVERE, "Doc URL :::: " + urlData);

			fileName = servc.getDocument().getName();
			logger.log(Level.SEVERE, "Document Name :::: " + fileName);

			String[] res = urlData.split("blob-key=", 0);
			logger.log(Level.SEVERE, "Splitted Url :::: " + res);

			String blob = res[1].trim();
			logger.log(Level.SEVERE, "Splitted Url Assigned to string  :::: "
					+ blob);

			BlobKey blobKey = null;
			try {
				blobKey = new BlobKey(blob);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);

			// This method read bytes from blobstore
			long inxStart = 0;
			long inxEnd = 1024;
			boolean flag = false;
			byte[] bytes = null;
			do {
				try {
					bytes = blobstoreService.fetchData(blobKey, inxStart,
							inxEnd);
					uploadStream.write(bytes);

					if (bytes.length < 1024)
						flag = true;

					inxStart = inxEnd + 1;
					inxEnd += 1025;

				} catch (Exception e) {
					flag = true;
				}

			} while (!flag);
			logger.log(Level.SEVERE, "Calling Email Method  :::: ");
		}
	}

	public void initializeServCont() {

		if (servcont.getCompanyId() != null) {
			comp = ofy().load().type(Company.class).id(servcont.getCompanyId())
					.now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		if (servcont.getCompanyId() != null) {
			c = ofy().load().type(Customer.class)
					.filter("count", servcont.getCinfo().getCount())
					.filter("companyId", servcont.getCompanyId()).first().now();
		} else {
			c = ofy().load().type(Customer.class)
					.filter("count", servcont.getCinfo().getCount()).first()
					.now();
		}

		String custEmail = c.getEmail().trim();

		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add(custEmail);

		// Table header 1
		ArrayList<String> tbl1_header = new ArrayList<String>();
		tbl1_header.add("Code");
		tbl1_header.add("Product Name");
		tbl1_header.add("Product Category");
		tbl1_header.add("Product Quantity");
		tbl1_header.add("Rate");
		// tbl1_header.add("VAT");
		// tbl1_header.add("Service Tax");
		System.out.println("Tbl1 header Size :" + tbl1_header.size());

		List<SalesLineItem> productList = new ArrayList<SalesLineItem>();
		productList = servcont.getItems();
		// Table 1
		ArrayList<String> tbl1 = new ArrayList<String>();
		for (int i = 0; i < productList.size(); i++) {
			tbl1.add(productList.get(i).getProductCode());
			tbl1.add(productList.get(i).getProductName());
			tbl1.add(productList.get(i).getProductCategory());
			tbl1.add(productList.get(i).getQty() + "");
			tbl1.add(productList.get(i).getPrice() + "");
			// tbl1.add(productList.get(i).getVatTax().getPercentage()+"");
			// tbl1.add(productList.get(i).getServiceTax().getPercentage()+"");
		}
		System.out.println("Tbl1 Size :" + tbl1.size());
		// Table 2 header
		ArrayList<String> tbl2_header = new ArrayList<String>();
		tbl2_header.add("Sr. No.");
		tbl2_header.add("Days");
		tbl2_header.add("Percent");
		tbl2_header.add("Comment");
		System.out.println("Tbl2 header Size :" + tbl2_header.size());

		List<PaymentTerms> paymentTermsList = new ArrayList<PaymentTerms>();
		paymentTermsList = servcont.getPaymentTermsList();
		// Table 2
		ArrayList<String> tbl2 = new ArrayList<String>();
		for (int i = 0; i < paymentTermsList.size(); i++) {
			tbl2.add((i + 1) + "");
			tbl2.add(paymentTermsList.get(i).getPayTermDays() + "");
			tbl2.add(paymentTermsList.get(i).getPayTermPercent() + "");
			tbl2.add(paymentTermsList.get(i).getPayTermComment());
		}
		System.out.println("Tbl2 Size :" + tbl2.size());

		String header = "Header";
		String footer = "Footer";
		String[] fileName = { servcont.getCount() + " "
				+ servcont.getCinfo().getFullName() };

		String uploadDocName = null;
		if (servcont.getDocument() != null
				&& servcont.getDocument().getUrl() != null) {
			uploadDocName = servcont.getDocument().getName();
		}


		/**
		 * @author Vijay Chougule Date 28-12-2020
		 * Des :- Email From Sales Person and CC in Approver Name and Branch Email id and Company Email Id
		 * send Emails From SendGrid
		 * 
		 */
		ArrayList<String> ccEmailList = new ArrayList<String>();
		String fromEmailId = "";
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.PC_ENABLESENDEMAILSFROMPERSONRESPONSIBLE, this.servcont.getCompanyId())) {
			ccEmailList.add(comp.getEmail());
			fromEmailId = getSalesPersonEmail(this.servcont.getEmployee(),this.servcont.getCompanyId(),this.servcont.getBranch());
			if(fromEmailId==null) {
				fromEmailId = comp.getEmail();
			}
			String approverEmailId = getApproverEmailId(this.servcont.getApproverName(),this.servcont.getCompanyId());
			if(approverEmailId!=null) {
				ccEmailList.add(approverEmailId);
			}
			String branchEmailId = getBranchEmailId(this.servcont.getBranch(), this.servcont.getCompanyId());
			if(branchEmailId!=null) {
				ccEmailList.add(branchEmailId);
			}
		}
		/**
		 * ends here
		 */
		
		System.out.println("Header: " + header + " Footer: " + footer);
		try {
			System.out.println("Inside try calling Method...");
			sendNewEmail(toEmailList, "Contract", "Contract",
					servcont.getCount(), "Contract",
					servcont.getCreationDate(), comp, "Product Details",
					tbl1_header, tbl1, "Payment Terms", tbl2_header, tbl2,
					note, null, null, fileName, uploadDocName,ccEmailList,fromEmailId);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/************************************* Contract End *******************************************/

	/************************************* Raise Ticket Email *********************************************/

	@Override
	public void initiateSupportEmail(RaiseTicket ticket)
			throws IllegalArgumentException {
		setRaiseTicket(ticket);
		initializeRaiseTicket();
		sendingMail();

	}

	public void setRaiseTicket(RaiseTicket ticket) {
		this.ticket = ticket;
	}

	public void initializeRaiseTicket() {
		if (ticket.getCompanyId() != null) {
			comp = ofy().load().type(Company.class).id(ticket.getCompanyId())
					.now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

	}

	public void sendingMail() {
		String toEmail = "support@evasoftwaresolutions.com";
		String empName = "";
		String title = "";
		String desc = "";
		String moduleName = "";
		String documentName = "";
		int documentId = 0;
		String subjectLine = ticket.getRaiseTicketTitle();

		if (ticket.getRaiseTicketEmpName() != null) {
			empName = ticket.getRaiseTicketEmpName();
		}

		if (ticket.getRaiseTicketTitle() != null) {
			title = ticket.getRaiseTicketTitle();
		}

		if (ticket.getRaiseTicketDescription() != null) {
			desc = ticket.getRaiseTicketDescription();
		}

		if (ticket.getRaiseTicketModule() != null) {
			moduleName = ticket.getRaiseTicketModule();
		}

		if (ticket.getRaiseTicketDocumentName() != null) {
			documentName = ticket.getRaiseTicketDocumentName();
		}

		if (ticket.getRaiseTicketDocumentId() != 0) {
			documentId = ticket.getRaiseTicketDocumentId();
		}

		String msgBody = "" + "<b>" + "Ticket From "
				+ ticket.getRaiseTicketCompanyName() + "</b>" + "</br>"
				+ "</br>" + "<b>" + "Ticket ID:  " + "</b>" + ticket.getCount()
				+ "       " + "<b>" + "Date:  " + "</b>"
				+ fmt.format(ticket.getRaiseTicketDate()) + "</br>" + "</br>"
				+ "<b>" + "Raised By:  " + "</b>" + empName + "</br>" + "</br>"
				+ "<b>" + "Title  :  " + "</b>" + title + "</br>" + "</br>"
				+ "<b>" + "Description:  " + "</b>" + desc + "</br>" + "</br>"
				+ "<b>" + "Module Name:  " + "</b>" + moduleName + "  " + "<b>"
				+ "Document Name:  " + "</b>" + documentName + "  " + "<b>"
				+ "Document ID:  " + "</b>" + documentId + "</br>" + "</br>"
				+ "<b>" + "Ticket Category:  " + "</b>"
				+ ticket.getRaiseTicketCategory() + "   " + "<b>"
				+ "Ticket Type:  " + "</b>" + ticket.getRaiseTicketType()
				+ "</br>" + "</br>" + "<b>" + "Ticket Priority:  " + "</b>"
				+ ticket.getRaiseTicketPriority() + "   " + "<b>"
				+ "Ticket Level:  " + "</b>" + ticket.getRaiseTicketLevel();

		String urlData = "";
		String fileName = "";

		// if(ticket.getUploadDocument()!=null){
		// urlData=ticket.getUploadDocument().getUrl();
		// fileName=ticket.getUploadDocument().getName();
		// }

		BlobstoreService blobstoreService = BlobstoreServiceFactory
				.getBlobstoreService();
		pdfstream = new ByteArrayOutputStream();

		if (ticket.getUploadDocument() != null
				&& !ticket.getUploadDocument().getUrl().equals("")) {

			// BlobKey blobKey = new BlobKey(req.getParameter("blob-key"));
			// blobstoreService.serve(blobKey, res);

			// urlData=urlData+ticket.getUploadDocument().getUrl();

			urlData = ticket.getUploadDocument().getUrl();
			logger.log(Level.SEVERE, "Doc URL :::: " + urlData);

			fileName = ticket.getUploadDocument().getName();
			logger.log(Level.SEVERE, "Document Name :::: " + fileName);

			String[] res = urlData.split("blob-key=", 0);

			logger.log(Level.SEVERE, "Splitted Url :::: " + res);
			String blob = res[1].trim();
			logger.log(Level.SEVERE, "Splitted Url Assigned to string  :::: "
					+ blob);
			BlobKey blobKey = null;
			try {
				blobKey = new BlobKey(blob);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}

			logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);

			long inxStart = 0;
			long inxEnd = 1024;
			boolean flag = false;
			byte[] bytes = null;
			do {
				try {
					bytes = blobstoreService.fetchData(blobKey, inxStart,
							inxEnd);
					pdfstream.write(bytes);

					if (bytes.length < 1024)
						flag = true;

					inxStart = inxEnd + 1;
					inxEnd += 1025;

				} catch (Exception e) {
					flag = true;
				}

			} while (!flag);

			// BlobstoreInputStream readblob=new BlobstoreInputStream(blobKey);
			// byte[] bytes=blobstoreService.fetchData(blobKey, 0,
			// BlobstoreService.MAX_BLOB_FETCH_SIZE );
			// logger.log(Level.SEVERE,"bytes data  :::: "+bytes);
			// pdfstream=new ByteArrayOutputStream(bytes.length);
			// pdfstream.write(bytes,0,bytes.length);
			// logger.log(Level.SEVERE,"pdfstream  :::: "+pdfstream);
			// }
			// catch (IOException e) {
			// e.printStackTrace();
			// // blobKey = new BlobKey(urlData);
			// }
			// BlobKey blobKey = new BlobKey(blob);
			// String segment = new String(blobstoreService.fetchData(blobKey,
			// 30, 40));
			// urlData="C:\\Users\\JUGAL\\Desktop\\2.png";
			// urlData = urlData.replaceAll("\\", "/");
			// File f = new File("C:\\Users\\JUGAL\\Desktop\\2.png");
			// System.out.println(":::::::::::::::"+f.getPath());
			// urlData=f.getAbsolutePath();
			// System.out.println("Document URL :::: "+urlData);
			// System.out.println("Document Name :::: "+fileName);
		}

		sendSupportEmail(subjectLine, msgBody, comp.getEmail().trim(), toEmail,
				urlData, fileName);

	}

	/***************************************** Interaction Email ***************************************/

	@Override
	public void initiateInteractionEmail(InteractionType interaction)
			throws IllegalArgumentException {
		setInteraction(interaction);
		initializeInteractionType();
	}

	public void setInteraction(InteractionType interaction) {
		this.interaction = interaction;
	}

	public void initializeInteractionType() {
		if (interaction.getCompanyId() != null) {
			comp = ofy().load().type(Company.class)
					.id(interaction.getCompanyId()).now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		if (interaction.getInteractionPersonResponsible() != null) {
			emp = ofy()
					.load()
					.type(Employee.class)
					.filter("companyId", interaction.getCompanyId())
					.filter("fullname",
							interaction.getInteractionPersonResponsible()
									.trim()).first().now();
		} else {
			emp = ofy()
					.load()
					.type(Employee.class)
					.filter("fullname",
							interaction.getInteractionPersonResponsible()
									.trim()).first().now();
		}
		// Employee email id is added to emailList
		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add(emp.getEmail() + "");

		String msgBody = "" + "Dear "
				+ interaction.getInteractionPersonResponsible().trim()
				+ ","
				+ "</br>"
				+ "</br>"
				+ "An interaction has been assigned with the following actions:"
				+ "</br>" + "</br>" + "<b>" + "Interaction ID:  " + "</b>"
				+ interaction.getCount() + "  " + "<b>"
				+ "Interaction Title:  " + "</b>"
				+ interaction.getInteractionTitle() + "  " + "<b>"
				+ "Creation Date:  " + "</b>"
				+ fmt.format(interaction.getinteractionCreationDate())
				+ "</br>" + "</br>" + "<b>" + "Business Partner Type:  "
				+ "</b>" + interaction.getInteractionPersonType();

		sendInteractionEmail(toEmailList, "Interaction", "Interaction To Do",
				interaction.getCount(), interaction.getInteractionTitle(),
				interaction.getinteractionCreationDate(), comp, msgBody);

	}

	/******************************************* Initiate PR MAIL ********************************************************/

	@Override
	public void initiatePrEmail(PurchaseRequisition pr)
			throws IllegalArgumentException {
		setPR(pr);
		loadDataForPr();
		createPRUploadDocumentAttachment();
		initializePR();
	}

	public void setPR(PurchaseRequisition pr) {
		this.pr = pr;
		System.out.println("Setting PR..");
	}

	private void loadDataForPr() {
		if (pr.getCompanyId() != null) {
			comp = ofy().load().type(Company.class).id(pr.getCompanyId()).now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		if (pr.getEmployee() != null) {
			emp = ofy().load().type(Employee.class)
					.filter("companyId", pr.getCompanyId())
					.filter("fullname", pr.getEmployee().trim()).first().now();
		} else {
			emp = ofy().load().type(Employee.class)
					.filter("fullname", pr.getEmployee().trim()).first().now();
		}
	}

	private void createPRUploadDocumentAttachment() {
		blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		uploadStream = new ByteArrayOutputStream();

		String urlData = "";
		String fileName = "";

		if (pr.getUptestReport() != null
				&& !pr.getUptestReport().getUrl().equals("")) {
			urlData = pr.getUptestReport().getUrl();
			logger.log(Level.SEVERE, "Doc URL :::: " + urlData);

//			fileName = salesord.getDocument().getName();
			fileName = pr.getUptestReport().getName();
			logger.log(Level.SEVERE, "Document Name :::: " + fileName);

			String[] res = urlData.split("blob-key=", 0);
			logger.log(Level.SEVERE, "Splitted Url :::: " + res);

			String blob = res[1].trim();
			logger.log(Level.SEVERE, "Splitted Url Assigned to string  :::: "
					+ blob);

			BlobKey blobKey = null;
			try {
				blobKey = new BlobKey(blob);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);

			// This method read bytes from blobstore
			long inxStart = 0;
			long inxEnd = 1024;
			boolean flag = false;
			byte[] bytes = null;
			do {
				try {
					bytes = blobstoreService.fetchData(blobKey, inxStart,
							inxEnd);
					uploadStream.write(bytes);

					if (bytes.length < 1024)
						flag = true;

					inxStart = inxEnd + 1;
					inxEnd += 1025;

				} catch (Exception e) {
					flag = true;
				}

			} while (!flag);
			logger.log(Level.SEVERE, "Calling Email Method  :::: ");
		}
	}

	public void initializePR() {

		// Employee email id is added to emailList
		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add(emp.getEmail() + "");

		ArrayList<String> tbl1_header = new ArrayList<String>();
		tbl1_header.add("ID");
		tbl1_header.add("Code");
		tbl1_header.add("Product Name");
		tbl1_header.add("Product Category");
		tbl1_header.add("Product Quantity");
		tbl1_header.add("UOM");
		System.out.println("Tbl1 header Size :" + tbl1_header.size());

		List<ProductDetails> productList = new ArrayList<ProductDetails>();
		productList = pr.getPrproduct();

		ArrayList<String> tbl1 = new ArrayList<String>();
		for (int i = 0; i < productList.size(); i++) {
			tbl1.add(productList.get(i).getProductID() + "");
			tbl1.add(productList.get(i).getProductCode());
			tbl1.add(productList.get(i).getProductName());
			tbl1.add(productList.get(i).getProductCategory());
			tbl1.add(productList.get(i).getProductQuantity() + "");
			tbl1.add(productList.get(i).getUnitOfmeasurement());
		}

		String uploadDocName = null;
		if (pr.getUptestReport() != null
				&& pr.getUptestReport().getUrl() != null) {
			uploadDocName = pr.getUptestReport().getName();
		}

		System.out.println("Tbl1 Size :" + tbl1.size());
		String header = pr.getHeader();
		String footer = pr.getFooter();

		try {
			sendNewEmail(toEmailList, "PR", "Purchase Requisition",
					pr.getCount(), pr.getPrTitle(), pr.getCreationDate(), comp,
					"Product Details", tbl1_header, tbl1, null, null, null, "",
					header, footer, null, uploadDocName,null,null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/******************************************* Initiate RFQ MAIL *************************************************************/

	@Override
	public void initiateRFQEmail(RequsestForQuotation rfq)
			throws IllegalArgumentException {
		setRFQ(rfq);
		loadDataForRfq();
		initializeRFQ();
		createRFQUploadDocumentAttachment();

	}

	public void setRFQ(RequsestForQuotation rfq) {
		this.rfq = rfq;
	}

	private void loadDataForRfq() {
		if (rfq.getCompanyId() != null) {
			comp = ofy().load().type(Company.class).id(rfq.getCompanyId())
					.now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}
	}

	private void createRFQUploadDocumentAttachment() {
		blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		uploadStream = new ByteArrayOutputStream();

		String urlData = "";
		String fileName = "";

		if (rfq.getUptestReport() != null
				&& rfq.getUptestReport().getUrl() != null) {
			urlData = rfq.getUptestReport().getUrl();
			logger.log(Level.SEVERE, "Doc URL :::: " + urlData);

			fileName = rfq.getUptestReport().getName();
			logger.log(Level.SEVERE, "Document Name :::: " + fileName);

			String[] res = urlData.split("blob-key=", 0);
			logger.log(Level.SEVERE, "Splitted Url :::: " + res);

			String blob = res[1].trim();
			logger.log(Level.SEVERE, "Splitted Url Assigned to string  :::: "
					+ blob);

			BlobKey blobKey = null;
			try {
				blobKey = new BlobKey(blob);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);

			// This method read bytes from blobstore
			long inxStart = 0;
			long inxEnd = 1024;
			boolean flag = false;
			byte[] bytes = null;
			do {
				try {
					bytes = blobstoreService.fetchData(blobKey, inxStart,
							inxEnd);
					uploadStream.write(bytes);

					if (bytes.length < 1024)
						flag = true;

					inxStart = inxEnd + 1;
					inxEnd += 1025;

				} catch (Exception e) {
					flag = true;
				}

			} while (!flag);
			logger.log(Level.SEVERE, "Calling Email Method  :::: ");
		}
	}

	public void initializeRFQ() {
		List<VendorDetails> vendorList = new ArrayList<VendorDetails>();
		vendorList = rfq.getVendorinfo();

		// Vendor email id is added to emailList
		ArrayList<String> toEmailList = new ArrayList<String>();
		for (int i = 0; i < vendorList.size(); i++) {
			toEmailList.add(vendorList.get(i).getVendorEmailId());
			System.out.println("Email Id :" + toEmailList);
		}

		// vijay added for email to purchase engineer
		Employee employee = ofy().load().type(Employee.class)
				.filter("companyId", rfq.getCompanyId())
				.filter("fullname", rfq.getEmployee()).first().now();
		if (employee != null) {
			toEmailList.add(employee.getEmail());
		}

		ArrayList<String> tbl1_header = new ArrayList<String>();
		tbl1_header.add("ID");
		tbl1_header.add("Code");
		tbl1_header.add("Product Name");
		tbl1_header.add("Product Category");
		tbl1_header.add("Product Quantity");
		tbl1_header.add("UOM");
		System.out.println("Tbl1 header Size :" + tbl1_header.size());

		List<ProductDetails> productList = new ArrayList<ProductDetails>();
		productList = rfq.getProductinfo();

		ArrayList<String> tbl1 = new ArrayList<String>();
		for (int i = 0; i < productList.size(); i++) {
			tbl1.add(productList.get(i).getProductID() + "");
			tbl1.add(productList.get(i).getProductCode());
			tbl1.add(productList.get(i).getProductName());
			tbl1.add(productList.get(i).getProductCategory());
			tbl1.add(productList.get(i).getProductQuantity() + "");
			tbl1.add(productList.get(i).getUnitOfmeasurement());
		}

		String uploadDocName = null;
		if (rfq.getUptestReport() != null
				&& !rfq.getUptestReport().getUrl().equals("")) {
			uploadDocName = rfq.getUptestReport().getName();
		}

		System.out.println("Tbl1 Size :" + tbl1.size());
		String header = rfq.getHeader();
		String footer = rfq.getFooter();
		try {
			sendNewEmail(
					toEmailList,
					"RFQ",
					"Request For Quotation",
					rfq.getCount(),
					rfq.getQuotationName(),
					DateUtility.getDateWithTimeZone("IST",
							rfq.getCreationDate()), comp, "Product Details",
					tbl1_header, tbl1, null, null, null, "", header, footer,
					null, uploadDocName,null,null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/************************************************* Initiate LOI MAIL ************************************************/

	@Override
	public void initiateLOIEmail(LetterOfIntent loi)
			throws IllegalArgumentException {
		setLOI(loi);
		loadDataForLoi();
		createLoiUploadDocumentAttachment();
		initializeLOI();

	}

	public void setLOI(LetterOfIntent loi) {
		this.loi = loi;
	}

	private void loadDataForLoi() {
		if (loi.getCompanyId() != null) {
			comp = ofy().load().type(Company.class).id(loi.getCompanyId())
					.now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}
	}

	private void createLoiUploadDocumentAttachment() {
		blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		uploadStream = new ByteArrayOutputStream();

		String urlData = "";
		String fileName = "";

		if (loi.getUptestReport() != null
				&& !loi.getUptestReport().getUrl().equals("")) {
			urlData = loi.getUptestReport().getUrl();
			logger.log(Level.SEVERE, "Doc URL :::: " + urlData);

			fileName = salesord.getDocument().getName();
			logger.log(Level.SEVERE, "Document Name :::: " + fileName);

			String[] res = urlData.split("blob-key=", 0);
			logger.log(Level.SEVERE, "Splitted Url :::: " + res);

			String blob = res[1].trim();
			logger.log(Level.SEVERE, "Splitted Url Assigned to string  :::: "
					+ blob);

			BlobKey blobKey = null;
			try {
				blobKey = new BlobKey(blob);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);

			// This method read bytes from blobstore
			long inxStart = 0;
			long inxEnd = 1024;
			boolean flag = false;
			byte[] bytes = null;
			do {
				try {
					bytes = blobstoreService.fetchData(blobKey, inxStart,
							inxEnd);
					uploadStream.write(bytes);

					if (bytes.length < 1024)
						flag = true;

					inxStart = inxEnd + 1;
					inxEnd += 1025;

				} catch (Exception e) {
					flag = true;
				}

			} while (!flag);
			logger.log(Level.SEVERE, "Calling Email Method  :::: ");
		}
	}

	public void initializeLOI() {

		List<VendorDetails> vendorList = new ArrayList<VendorDetails>();
		vendorList = loi.getVendorinfo();

		// Vendor email id is added to emailList
		ArrayList<String> toEmailList = new ArrayList<String>();
		for (int i = 0; i < vendorList.size(); i++) {
			toEmailList.add(vendorList.get(i).getVendorEmailId());
			System.out.println("Email Id :" + toEmailList);
		}

		// vijay added for email to purchase engineer
		Employee employee = ofy().load().type(Employee.class)
				.filter("companyId", rfq.getCompanyId())
				.filter("fullname", rfq.getEmployee()).first().now();
		if (employee != null) {
			toEmailList.add(employee.getEmail());
		}

		ArrayList<String> tbl1_header = new ArrayList<String>();
		tbl1_header.add("ID");
		tbl1_header.add("Code");
		tbl1_header.add("Product Name");
		tbl1_header.add("Product Category");
		tbl1_header.add("Product Quantity");
		tbl1_header.add("UOM");
		System.out.println("Tbl1 header Size :" + tbl1_header.size());

		List<ProductDetails> productList = new ArrayList<ProductDetails>();
		productList = loi.getProductinfo();

		ArrayList<String> tbl1 = new ArrayList<String>();
		for (int i = 0; i < productList.size(); i++) {
			tbl1.add(productList.get(i).getProductID() + "");
			tbl1.add(productList.get(i).getProductCode());
			tbl1.add(productList.get(i).getProductName());
			tbl1.add(productList.get(i).getProductCategory());
			tbl1.add(productList.get(i).getProductQuantity() + "");
			tbl1.add(productList.get(i).getUnitOfmeasurement());
		}
		System.out.println("Tbl1 Size :" + tbl1.size());

		String uploadDocName = null;
		if (loi.getUptestReport() != null
				&& loi.getUptestReport().getUrl() != null) {
			uploadDocName = loi.getUptestReport().getName();
		}

		String header = loi.getHeader();
		String footer = loi.getFooter();
		try {
			sendNewEmail(toEmailList, "LOI", "Letter Of Intent",
					loi.getCount(), loi.getTitle(), loi.getCreationDate(),
					comp, "Product Details", tbl1_header, tbl1, null, null,
					null, "", header, footer, null, uploadDocName,null,null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/****************************************** Initiate PO EMAIL *********************************************************/
	@Override
	public void initiatePOEmail(PurchaseOrder po)
			throws IllegalArgumentException {
		setPO(po);
		initializePurchaseOrder();		
		logger.log(Level.SEVERE, "attachmnet id in PO " +po.getCompanyId());
		createPOAttachment(po.getCompanyId());
		createPOUploadDocumentAttachment();
		initializePO();
	}

	private void setPO(PurchaseOrder po) {
		this.po = po;
	}

	public void initializePurchaseOrder() {
		if (po.getCompanyId() != null) {
			comp = ofy().load().type(Company.class).id(po.getCompanyId()).now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		if (po.getCompanyId() == null)
			emp = ofy().load().type(Employee.class)
					.filter("fullname", po.getEmployee()).first().now();
		else
			emp = ofy().load().type(Employee.class)
					.filter("companyId", po.getCompanyId())
					.filter("fullname", po.getEmployee()).first().now();

		List<VendorDetails> listVendor = po.getVendorDetails();
		int selectedVendor = 0;
		if (listVendor.size() != 0) {
			for (int i = 0; i < listVendor.size(); i++) {
				if (listVendor.get(i).getStatus() == true) {
					selectedVendor = listVendor.get(i).getVendorId();
				}
			}
		}

		if (po.getCompanyId() != null && selectedVendor != 0) {
			v = ofy().load().type(Vendor.class)
					.filter("companyId", po.getCompanyId())
					.filter("count", selectedVendor)
					.filter("vendorStatus", true).first().now();
		}
		
		
		
		
  }

	/**
	 *     Added By : Priyanka Bhagwat
	 *     Date : 1/06/2021
	 *     Des : When Submit PO then SMS and EMAIL will be send Req by Pecopp raised By Ashwini.	
	 */
	public void createPOAttachment(Long companyid) {
		//popdf = new PurchaseOrderPdf();
		logger.log(Level.SEVERE, "Inside PO attachment Server ");
		pecoppurchasepdf = new PurchaseOrderVersionOnePdf();
		
		pecoppurchasepdf.document = new Document();
		Document document = pecoppurchasepdf.document;
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(pecoppurchasepdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.open();
		logger.log(Level.SEVERE, "PO id in attachment " +po.getId());
		pecoppurchasepdf.setPurchaseOrder(po.getId());
		
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", "CompanyAsLetterHead", companyid)){
			pecoppurchasepdf.createPdf("no");
			}
		else{
			pecoppurchasepdf.createPdf("plane");
		}
		
		document.close();
		
	/**
	 *  END
	 */
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", "OldVersionPurchaseOrderPdf", comp.getCompanyId())){
		popdf = new PurchaseOrderPdf();
		popdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(popdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		popdf.document.open();

		popdf.createPdfForEmail(comp, v, po, emp);
		popdf.document.close();
	  }
	}

	private void createPOUploadDocumentAttachment() {
		blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		uploadStream = new ByteArrayOutputStream();

		String urlData = "";
		String fileName = "";

		if (po.getUptestReport() != null
				&& !po.getUptestReport().getUrl().equals("")) {
			urlData = po.getUptestReport().getUrl();
			logger.log(Level.SEVERE, "Doc URL :::: " + urlData);

			fileName = po.getUptestReport().getName();
			logger.log(Level.SEVERE, "Document Name :::: " + fileName);

			String[] res = urlData.split("blob-key=", 0);
			logger.log(Level.SEVERE, "Splitted Url :::: " + res);

			String blob = res[1].trim();
			logger.log(Level.SEVERE, "Splitted Url Assigned to string  :::: "
					+ blob);

			BlobKey blobKey = null;
			try {
				blobKey = new BlobKey(blob);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);

			// This method read bytes from blobstore
			long inxStart = 0;
			long inxEnd = 1024;
			boolean flag = false;
			byte[] bytes = null;
			do {
				try {
					bytes = blobstoreService.fetchData(blobKey, inxStart,
							inxEnd);
					uploadStream.write(bytes);

					if (bytes.length < 1024)
						flag = true;

					inxStart = inxEnd + 1;
					inxEnd += 1025;

				} catch (Exception e) {
					flag = true;
				}

			} while (!flag);
			logger.log(Level.SEVERE, "Calling Email Method  :::: ");
		}
	}

	private void initializePO() {

		List<VendorDetails> vendorList = new ArrayList<VendorDetails>();
		vendorList = po.getVendorDetails();

		// vijay added for email to purchase engineer
		String purchaseEngineerEmailId = null;
		if (emp != null) {
			System.out.println(" hi vijay emploee loaded");
			purchaseEngineerEmailId = emp.getEmail();
		}

		// Vendor email id is added to emailList
		ArrayList<String> toEmailList = new ArrayList<String>();
		for (int i = 0; i < vendorList.size(); i++) {
			if(vendorList.get(i).getVendorEmailId()!=null && !vendorList.get(i).getVendorEmailId().equals("")) {
				toEmailList.add(vendorList.get(i).getVendorEmailId());
				System.out.println("Email Id :" + toEmailList);
			}
			
		}

		// vijay
		if (emp != null) {
			System.out.println(" purchase engineer id===="
					+ purchaseEngineerEmailId);
			toEmailList.add(purchaseEngineerEmailId);
		}

		// Table header 1
		ArrayList<String> tbl1_header = new ArrayList<String>();
		tbl1_header.add("ID");
		tbl1_header.add("Code");
		tbl1_header.add("Product Name");
		tbl1_header.add("Product Category");
		tbl1_header.add("Product Quantity");
		tbl1_header.add("UOM");
		System.out.println("Tbl1 header Size :" + tbl1_header.size());

		List<ProductDetailsPO> productList = new ArrayList<ProductDetailsPO>();
		productList = po.getProductDetails();
		// Table 1
		ArrayList<String> tbl1 = new ArrayList<String>();
		for (int i = 0; i < productList.size(); i++) {
			tbl1.add(productList.get(i).getProductID() + "");
			tbl1.add(productList.get(i).getProductCode());
			tbl1.add(productList.get(i).getProductName());
			tbl1.add(productList.get(i).getProductCategory());
			tbl1.add(productList.get(i).getProductQuantity() + "");
			tbl1.add(productList.get(i).getUnitOfmeasurement());
		}
		System.out.println("Tbl1 Size :" + tbl1.size());
		// Table 2 header
		ArrayList<String> tbl2_header = new ArrayList<String>();
		tbl2_header.add("Sr. No.");
		tbl2_header.add("Days");
		tbl2_header.add("Percent");
		tbl2_header.add("Comment");
		System.out.println("Tbl2 header Size :" + tbl2_header.size());

		List<PaymentTerms> paymentTermsList = new ArrayList<PaymentTerms>();
		paymentTermsList = po.getPaymentTermsList();
		// Table 2
		ArrayList<String> tbl2 = new ArrayList<String>();
		for (int i = 0; i < paymentTermsList.size(); i++) {
			tbl2.add((i + 1) + "");
			tbl2.add(paymentTermsList.get(i).getPayTermDays() + "");
			tbl2.add(paymentTermsList.get(i).getPayTermPercent() + "");
			tbl2.add(paymentTermsList.get(i).getPayTermComment());
		}
		System.out.println("Tbl2 Size :" + tbl2.size());

		String header = po.getHeader().toString().trim();
		String footer = po.getFooter().toString().trim();

		String uploadDocName = null;
		if (po.getUptestReport() != null
				&& !po.getUptestReport().getUrl().equals("")) {
			uploadDocName = po.getUptestReport().getName();
		}

		String[] fileName = { po.getCount() + "Purchase Order" };
		System.out.println("Header: " + header + " Footer: " + footer);
		
		/**
		 * @author Vijay Chougule Date 28-12-2020
		 * Des :- Email From Sales Person and CC in Approver Name and Branch Email id and Company Email Id
		 * send Emails From SendGrid
		 * 
		 */
		ArrayList<String> ccEmailList = new ArrayList<String>();
		String fromEmailId = "";
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.PC_ENABLESENDEMAILSFROMPERSONRESPONSIBLE, this.po.getCompanyId())) {
			ccEmailList.add(comp.getEmail());
			fromEmailId = getSalesPersonEmail(this.po.getEmployee(),this.po.getCompanyId(),this.po.getBranch());
			if(fromEmailId==null) {
				fromEmailId = comp.getEmail();
			}
			String approverEmailId = getApproverEmailId(this.po.getApproverName(),this.po.getCompanyId());
			if(approverEmailId!=null) {
				ccEmailList.add(approverEmailId);
			}
			String branchEmailId = getBranchEmailId(this.po.getBranch(), this.po.getCompanyId());
			if(branchEmailId!=null) {
				ccEmailList.add(branchEmailId);
			}
			if(po.getRefOrderNO()!=null && !po.getRefOrderNO().equals("")) {
				try {
					int salesorderId = Integer.parseInt(po.getRefOrderNO());
					SalesOrder salesorderEntity = ofy().load().type(SalesOrder.class).filter("companyId", po.getCompanyId())
										.filter("count", salesorderId).first().now();
					if(salesorderEntity!=null) {
						String salesPersonemailid = getApproverEmailId(salesorderEntity.getEmployee(),salesorderEntity.getCompanyId());
						if(salesPersonemailid!=null) {
							ccEmailList.add(salesPersonemailid);
						}
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}
		}
		/**
		 * ends here
		 */
		
		try {
			System.out.println("Inside try calling Method...");
			sendNewEmail(toEmailList, "PO", "Purchase Order", po.getCount(),
					po.getPOName(), po.getCreationDate(), comp,
					"Product Details", tbl1_header, tbl1, "Payment Terms",
					tbl2_header, tbl2, header, note, footer, fileName,
					uploadDocName,ccEmailList,fromEmailId);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/************************************** Initiate MIN Email **********************************************************/
	@Override
	public void initiateMINEmail(MaterialIssueNote min)
			throws IllegalArgumentException {
		setMin(min);
		initializeMINData();
		createMINUploadDocumentAttachment();
		intializeMIN();

	}

	private void setMin(MaterialIssueNote min) {
		this.min = min;
	}

	private void initializeMINData() {

		if (min.getCompanyId() != null) {
			comp = ofy().load().type(Company.class).id(min.getCompanyId())
					.now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		// For Employee Email

		if (min.getEmployee() != null) {
			emp = ofy().load().type(Employee.class)
					.filter("companyId", min.getCompanyId())
					.filter("fullname", min.getEmployee().trim()).first().now();
		} else {
			emp = ofy().load().type(Employee.class)
					.filter("fullname", min.getEmployee().trim()).first().now();
		}
	}

	private void createMINUploadDocumentAttachment() {
		blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		uploadStream = new ByteArrayOutputStream();

		String urlData = "";
		String fileName = "";

		if (min.getUpload() != null && !min.getUpload().getUrl().equals("")) {
			urlData = min.getUpload().getUrl();
			logger.log(Level.SEVERE, "Doc URL :::: " + urlData);

			fileName = min.getUpload().getName();
			logger.log(Level.SEVERE, "Document Name :::: " + fileName);

			String[] res = urlData.split("blob-key=", 0);
			logger.log(Level.SEVERE, "Splitted Url :::: " + res);

			String blob = res[1].trim();
			logger.log(Level.SEVERE, "Splitted Url Assigned to string  :::: "
					+ blob);

			BlobKey blobKey = null;
			try {
				blobKey = new BlobKey(blob);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);

			// This method read bytes from blobstore
			long inxStart = 0;
			long inxEnd = 1024;
			boolean flag = false;
			byte[] bytes = null;
			do {
				try {
					bytes = blobstoreService.fetchData(blobKey, inxStart,
							inxEnd);
					uploadStream.write(bytes);

					if (bytes.length < 1024)
						flag = true;

					inxStart = inxEnd + 1;
					inxEnd += 1025;

				} catch (Exception e) {
					flag = true;
				}

			} while (!flag);
			logger.log(Level.SEVERE, "Calling Email Method  :::: ");
		}
	}

	private void intializeMIN() {
		System.out.println("Eamil MIN: " + min.getCount());

		System.out.println("to Email:" + emp.getEmail());

		// Employee email id is added to emailList
		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add(emp.getEmail() + "");

		ArrayList<String> tbl1_header = new ArrayList<String>();
		tbl1_header.add("Code");
		tbl1_header.add("Product Name");
		tbl1_header.add("Product Category");
		tbl1_header.add("Product Quantity");
		tbl1_header.add("UOM");
		System.out.println("Tbl1 header Size :" + tbl1_header.size());

		List<SalesLineItem> productList = new ArrayList<SalesLineItem>();
		productList = min.getProductTablemin();

		ArrayList<String> tbl1 = new ArrayList<String>();
		for (int i = 0; i < productList.size(); i++) {
			tbl1.add(productList.get(i).getProductCode());
			tbl1.add(productList.get(i).getProductName());
			tbl1.add(productList.get(i).getProductCategory());
			tbl1.add(productList.get(i).getQty() + "");
			tbl1.add(productList.get(i).getUnitOfMeasurement());
		}
		System.out.println("Tbl1 Size :" + tbl1.size());

		ArrayList<String> tbl2_header = new ArrayList<String>();
		tbl2_header.add("Material ID");
		tbl2_header.add("Material Code");
		tbl2_header.add("Material Name");
		tbl2_header.add("Material Category");
		tbl2_header.add("Quantity");
		tbl2_header.add("Material Available Quantity");
		tbl2_header.add("UOM");
		tbl2_header.add("Remarks");
		System.out.println("Tbl2 header Size :" + tbl2_header.size());

		List<MaterialProduct> materiaList = new ArrayList<MaterialProduct>();
		materiaList = min.getSubProductTablemin();

		ArrayList<String> tbl2 = new ArrayList<String>();
		for (int i = 0; i < materiaList.size(); i++) {
			tbl2.add(materiaList.get(i).getMaterialProductId() + "");
			tbl2.add(materiaList.get(i).getMaterialProductCode());
			tbl2.add(materiaList.get(i).getMaterialProductName());
			tbl2.add(materiaList.get(i).getMaterialProductCategory());
			tbl2.add(materiaList.get(i).getMaterialProductRequiredQuantity()
					+ "");
			tbl2.add(materiaList.get(i).getMaterialProductAvailableQuantity()
					+ "");
			tbl2.add(materiaList.get(i).getMaterialProductUOM());
			tbl2.add(materiaList.get(i).getMaterialProductRemarks());
		}

		String uploadDocName = null;
		if (min.getUpload() != null && !mrn.getUpload().getUrl().equals("")) {
			uploadDocName = min.getUpload().getName();
		}

		System.out.println("Tbl1 Size :" + tbl2.size());
		try {
			sendNewEmail(toEmailList, "MIN", "Material Issue Note",
					min.getCount(), min.getMinTitle(), min.getMinDate(), comp,
					"Product Details", tbl1_header, tbl1,
					"Material Description", tbl2_header, tbl2, "", null, null,
					null, uploadDocName,null,comp.getEmail());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/************************************** Initiate MRN Email **********************************************************/
	@Override
	public void initiateMRNEmail(MaterialRequestNote mrn)
			throws IllegalArgumentException {
		setMrn(mrn);
		initializeMRNData();
		createMRNUploadDocumentAttachment();
		intializeMRN();
	}

	private void setMrn(MaterialRequestNote mrn) {
		this.mrn = mrn;
	}

	private void initializeMRNData() {
		if (mrn.getCompanyId() != null) {
			comp = ofy().load().type(Company.class).id(mrn.getCompanyId())
					.now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		// For Employee Email

		if (mrn.getEmployee() != null) {
			emp = ofy().load().type(Employee.class)
					.filter("companyId", mrn.getCompanyId())
					.filter("fullname", mrn.getEmployee().trim()).first().now();
		} else {
			emp = ofy().load().type(Employee.class)
					.filter("fullname", mrn.getEmployee().trim()).first().now();
		}
	}

	private void createMRNUploadDocumentAttachment() {
		blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		uploadStream = new ByteArrayOutputStream();

		String urlData = "";
		String fileName = "";

		if (mrn.getUpload() != null && !mrn.getUpload().getUrl().equals("")) {
			urlData = mrn.getUpload().getUrl();
			logger.log(Level.SEVERE, "Doc URL :::: " + urlData);

			fileName = mrn.getUpload().getName();
			logger.log(Level.SEVERE, "Document Name :::: " + fileName);

			String[] res = urlData.split("blob-key=", 0);
			logger.log(Level.SEVERE, "Splitted Url :::: " + res);

			String blob = res[1].trim();
			logger.log(Level.SEVERE, "Splitted Url Assigned to string  :::: "
					+ blob);

			BlobKey blobKey = null;
			try {
				blobKey = new BlobKey(blob);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);

			// This method read bytes from blobstore
			long inxStart = 0;
			long inxEnd = 1024;
			boolean flag = false;
			byte[] bytes = null;
			do {
				try {
					bytes = blobstoreService.fetchData(blobKey, inxStart,
							inxEnd);
					uploadStream.write(bytes);

					if (bytes.length < 1024)
						flag = true;

					inxStart = inxEnd + 1;
					inxEnd += 1025;

				} catch (Exception e) {
					flag = true;
				}

			} while (!flag);
			logger.log(Level.SEVERE, "Calling Email Method  :::: ");
		}
	}

	private void intializeMRN() {

		System.out.println("to Email:" + emp.getEmail());

		// Employee email id is added to emailList
		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add(emp.getEmail() + "");

		ArrayList<String> tbl1_header = new ArrayList<String>();
		tbl1_header.add("Code");
		tbl1_header.add("Product Name");
		tbl1_header.add("Product Category");
		tbl1_header.add("Product Quantity");
		tbl1_header.add("UOM");
		System.out.println("Tbl1 header Size :" + tbl1_header.size());

		List<SalesLineItem> productList = new ArrayList<SalesLineItem>();
		productList = mrn.getProductTableMrn();

		ArrayList<String> tbl1 = new ArrayList<String>();
		for (int i = 0; i < productList.size(); i++) {
			tbl1.add(productList.get(i).getProductCode());
			tbl1.add(productList.get(i).getProductName());
			tbl1.add(productList.get(i).getProductCategory());
			tbl1.add(productList.get(i).getQty() + "");
			tbl1.add(productList.get(i).getUnitOfMeasurement());
		}
		System.out.println("Tbl1 Size :" + tbl1.size());

		ArrayList<String> tbl2_header = new ArrayList<String>();
		tbl2_header.add("Material ID");
		tbl2_header.add("Material Code");
		tbl2_header.add("Material Name");
		tbl2_header.add("Material Category");
		tbl2_header.add("Quantity");
		tbl2_header.add("Material Available Quantity");
		tbl2_header.add("UOM");
		tbl2_header.add("Remarks");
		System.out.println("Tbl2 header Size :" + tbl2_header.size());

		List<MaterialProduct> materiaList = new ArrayList<MaterialProduct>();
		materiaList = mrn.getSubProductTableMrn();

		ArrayList<String> tbl2 = new ArrayList<String>();
		for (int i = 0; i < materiaList.size(); i++) {
			tbl2.add(materiaList.get(i).getMaterialProductId() + "");
			tbl2.add(materiaList.get(i).getMaterialProductCode());
			tbl2.add(materiaList.get(i).getMaterialProductName());
			tbl2.add(materiaList.get(i).getMaterialProductCategory());
			tbl2.add(materiaList.get(i).getMaterialProductRequiredQuantity()
					+ "");
			tbl2.add(materiaList.get(i).getMaterialProductAvailableQuantity()
					+ "");
			tbl2.add(materiaList.get(i).getMaterialProductUOM());
			tbl2.add(materiaList.get(i).getMaterialProductRemarks());
		}

		String uploadDocName = null;
		if (mrn.getUpload() != null && !mrn.getUpload().getUrl().equals("")) {
			uploadDocName = mrn.getUpload().getName();
		}

		System.out.println("Tbl1 Size :" + tbl2.size());
		try {
			sendNewEmail(toEmailList, "MRN", "Material Request Note",
					mrn.getCount(), mrn.getMrnTitle(), mrn.getMrnDate(), comp,
					"Product Description", tbl1_header, tbl1,
					"Material Description", tbl2_header, tbl2, "", null, null,
					null, uploadDocName,null,comp.getEmail());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/************************************** Initiate MMN Email **********************************************************/
	@Override
	public void initiateMMNEmail(MaterialMovementNote mmn)
			throws IllegalArgumentException {
		setMMN(mmn);
		initializeMMNData();
		createMMNUploadDocumentAttachment();
		initializeMMN();
	}

	private void setMMN(MaterialMovementNote mmn) {
		this.mmn = mmn;
	}

	private void initializeMMNData() {
		if (mmn.getCompanyId() != null) {
			comp = ofy().load().type(Company.class).id(mmn.getCompanyId())
					.now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		// For Employee Email

		if (mmn.getEmployee() != null) {
			emp = ofy().load().type(Employee.class)
					.filter("companyId", mmn.getCompanyId())
					.filter("fullname", mmn.getEmployee().trim()).first().now();
		} else {
			emp = ofy().load().type(Employee.class)
					.filter("fullname", mmn.getEmployee().trim()).first().now();
		}
	}

	private void createMMNUploadDocumentAttachment() {
		blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		uploadStream = new ByteArrayOutputStream();

		String urlData = "";
		String fileName = "";

		if (mmn.getUpload() != null && !mmn.getUpload().getUrl().equals("")) {
			urlData = mmn.getUpload().getUrl();
			logger.log(Level.SEVERE, "Doc URL :::: " + urlData);

			fileName = mmn.getUpload().getName();
			logger.log(Level.SEVERE, "Document Name :::: " + fileName);

			String[] res = urlData.split("blob-key=", 0);
			logger.log(Level.SEVERE, "Splitted Url :::: " + res);

			String blob = res[1].trim();
			logger.log(Level.SEVERE, "Splitted Url Assigned to string  :::: "
					+ blob);

			BlobKey blobKey = null;
			try {
				blobKey = new BlobKey(blob);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);

			// This method read bytes from blobstore
			long inxStart = 0;
			long inxEnd = 1024;
			boolean flag = false;
			byte[] bytes = null;
			do {
				try {
					bytes = blobstoreService.fetchData(blobKey, inxStart,
							inxEnd);
					uploadStream.write(bytes);

					if (bytes.length < 1024)
						flag = true;

					inxStart = inxEnd + 1;
					inxEnd += 1025;

				} catch (Exception e) {
					flag = true;
				}

			} while (!flag);
			logger.log(Level.SEVERE, "Calling Email Method  :::: ");
		}
	}

	private void initializeMMN() {

		// Employee email id is added to emailList
		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add(emp.getEmail() + "");

		ArrayList<String> tbl2_header = new ArrayList<String>();
		tbl2_header.add("Material ID");
		tbl2_header.add("Material Code");
		tbl2_header.add("Material Name");
		tbl2_header.add("Material Category");
		tbl2_header.add("Quantity");
		tbl2_header.add("Material Available Quantity");
		tbl2_header.add("UOM");
		tbl2_header.add("Remarks");
		System.out.println("Tbl2 header Size :" + tbl2_header.size());

		List<MaterialProduct> materiaList = new ArrayList<MaterialProduct>();
		materiaList = mmn.getSubProductTableMmn();

		ArrayList<String> tbl2 = new ArrayList<String>();
		for (int i = 0; i < materiaList.size(); i++) {
			tbl2.add(materiaList.get(i).getMaterialProductId() + "");
			tbl2.add(materiaList.get(i).getMaterialProductCode());
			tbl2.add(materiaList.get(i).getMaterialProductName());
			tbl2.add(materiaList.get(i).getMaterialProductCategory());
			tbl2.add(materiaList.get(i).getMaterialProductRequiredQuantity()
					+ "");
			tbl2.add(materiaList.get(i).getMaterialProductAvailableQuantity()
					+ "");
			tbl2.add(materiaList.get(i).getMaterialProductUOM());
			tbl2.add(materiaList.get(i).getMaterialProductRemarks());
		}
		System.out.println("Tbl1 Size :" + tbl2.size());

		String uploadDocName = null;
		if (mmn.getUpload() != null && !mmn.getUpload().getUrl().equals("")) {
			uploadDocName = mmn.getUpload().getName();
		}

		try {
			sendNewEmail(toEmailList, "MMN", "Material Movement Note",
					mmn.getCount(), mmn.getMmnTitle(), mmn.getMmnDate(), comp,
					null, null, null, "Material Description", tbl2_header,
					tbl2, "", null, null, null, uploadDocName,null,comp.getEmail());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/************************************************** GRN **************************************************************/
	@Override
	public void initiateGRNEmail(GRN grn) throws IllegalArgumentException {
		setGRN(grn);
		initializeGRN();
	}

	private void setGRN(GRN grn) {
		this.grn = grn;
	}

	private void initializeGRN() {
		System.out.println("Eamil grn: " + grn.getCount());

		if (grn.getCompanyId() != null) {
			comp = ofy().load().type(Company.class).id(grn.getCompanyId())
					.now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		// For Employee Email--Purchase Eng.

		if (grn.getEmployee() != null) {
			emp = ofy().load().type(Employee.class)
					.filter("companyId", grn.getCompanyId())
					.filter("fullname", grn.getEmployee().trim()).first().now();
		} else {
			emp = ofy().load().type(Employee.class)
					.filter("fullname", grn.getEmployee().trim()).first().now();
		}
		System.out.println("to Email:" + emp.getEmail());

		// Employee email id is added to emailList
		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add(emp.getEmail() + "");

		ArrayList<String> tbl1_header = new ArrayList<String>();
		tbl1_header.add("ID");
		tbl1_header.add("Code");
		tbl1_header.add("Product Name");
		tbl1_header.add("Product Category");
		tbl1_header.add("Product Quantity");
		tbl1_header.add("UOM");
		System.out.println("Tbl1 header Size :" + tbl1_header.size());

		List<GRNDetails> productList = new ArrayList<GRNDetails>();
		productList = grn.getInventoryProductItem();

		ArrayList<String> tbl1 = new ArrayList<String>();
		for (int i = 0; i < productList.size(); i++) {
			tbl1.add(productList.get(i).getProductID() + "");
			tbl1.add(productList.get(i).getProductCode());
			tbl1.add(productList.get(i).getProductName());
			tbl1.add(productList.get(i).getProductCategory());
			tbl1.add(productList.get(i).getProductQuantity() + "");
			tbl1.add(productList.get(i).getUnitOfmeasurement());
		}
		System.out.println("Tbl1 Size :" + tbl1.size());

		try {
			sendNewEmail(toEmailList, "GRN", "Goods Recived Note",
					grn.getCount(), grn.getTitle(), grn.getCreationDate(),
					comp, "Product Description", tbl1_header, tbl1, null, null,
					null, "", null, null, null, null,null,null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// *******************************added by rohan for customer campaign
	// ***************************

	@Override
	public void initiateCustomerListEmail(CustomerTrainingDetails custTraining)
			throws IllegalArgumentException {

		setCustomerList(custTraining);
		initializeCustomerTraining();
	}

	private void setCustTrainingDetails(CustomerTrainingDetails custTrain) {
		this.custTrainingDetails = custTrain;
	}

	private void setCustomerList(CustomerTrainingDetails custTraining) {
		this.custTrainingDetails = custTraining;
	}

	private void initializeCustomerTraining() {
		System.out.println("Eamil custTraining: "
				+ custTrainingDetails.getCount());

		if (custTrainingDetails.getCompanyId() != null) {
			comp = ofy().load().type(Company.class)
					.id(custTrainingDetails.getCompanyId()).now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		// Employee email id is added to emailList
		ArrayList<CustomerTrainingAttendies> custlis = new ArrayList<CustomerTrainingAttendies>();
		custlis.addAll(custTrainingDetails.getAttendiesLis());

		// Speaker details added to emailList
		ArrayList<CustomerTrainingSpeaker> speakerlis = new ArrayList<CustomerTrainingSpeaker>();
		speakerlis.addAll(custTrainingDetails.getSpreakersLis());

		// String message11 ="Dear Sir/ Madam,"+"\n"+"\n"+"\n"
		// +"We are happy to inform you that,we have organised a training compain for our customers."+"\n"
		// +"below are the program details :"+"\n"+"\n"
		// +"Date : "+custTrainingDetails.getFrom_dt()+"Time : "+custTrainingDetails.getFrom_time()+"\n"+"\n"
		// +"Speaker Details :"
		// +"Point of contact Details :"+"\n"+"\n"
		// +"Name :"+custTrainingDetails.getPocName()+" Contact no :"+custTrainingDetails.getPocContact()+" Email : "+custTrainingDetails.getPocEmail()
		// +"\n";

		System.out.println("desc====" + custTrainingDetails.getDescription());
		System.out.println("from date====" + custTrainingDetails.getFrom_dt());
		System.out.println("from time ======"
				+ custTrainingDetails.getFrom_time());
		System.out.println("address ===" + custTrainingDetails.getAddress());
		System.out.println("");

		String message11 = null;
		if (custTrainingDetails.getDescription() != null) {
			message11 = custTrainingDetails.getDescription() + "\n" + "\n";
		} else {
			message11 = "";
		}

		String programme = " ";
		if (custTrainingDetails.getFrom_dt() != null
				&& custTrainingDetails.getFrom_time() != null
				&& custTrainingDetails.getAddress() != null) {
			programme = "Program Details :" + "\n" + "\n" + "Date : "
					+ fmt.format(custTrainingDetails.getFrom_dt())
					+ "   Time : " + custTrainingDetails.getFrom_time() + "\n"

					+ "Address : " + custTrainingDetails.getAddress() + "\n"
					+ "\n" + "Speaker Details :" + "\n";
		} else {
			programme = " ";
		}
		String message2 = " ";

		if (speakerlis.size() != 0) {
			for (int i = 0; i < speakerlis.size(); i++) {
				message2 = message2 + "\n" + speakerlis.get(i).getSpeakerName()
						+ "(" + speakerlis.get(i).getSpeakerTitle() + ")     "
						+ speakerlis.get(i).getSpeakercellno() + "     "
						+ speakerlis.get(i).getSpeakerEmail();
			}
		} else {
			message2 = "";
		}

		String message3 = "\n" + "\n" + "Point of contact Details :" + "\n"
				+ "\n" + "Name :" + custTrainingDetails.getPocName() + "\n"
				+ "Contact no :" + custTrainingDetails.getPocContact() + "\n"
				+ "Email : " + custTrainingDetails.getPocEmail();

		try {
			System.out.println("Inside try calling Method...");
			logger.log(Level.SEVERE, "Ready To Call Common Method");
			sendEmailToCustomerList(custlis, custTrainingDetails.getTitle(),
					message11, programme, message2, message3, comp.getEmail());
		}

		catch (IOException e) {
			e.printStackTrace();
		}

	}

	// *************************changes ends here ******************************

	/*********************************** Approval Email Logic *****************************************/

	@Override
	public void initiateApprovalEmail(Approvals approve)
			throws IllegalArgumentException {

		setApprovals(approve);
		initializeApprovals();
		
		/**
		 * Date 02-02-2019 By Vijay 
		 * Des :- NBHC Inventory Management Automatic PR Approval Email send with new format using process configuration
		 * and this is only for AUtomatic PR Generated by Ststem
		 *  
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition", "EnableAutomaticPRNewEmailFormat", approve.getCompanyId())
				&& validateAutomaticPR(approve)){
			sendNewEmailFormatforPR(pr,approve.getApproverName());
		}
		else{
			
		System.out.println("company url==============" + comp.getCompanyURL());
		// ********** to
		// "+comp.getAccessUrl()+"."+SystemProperty.applicationId.get()+".appspot.com"+"
		
		/**
		 * @author Anil , Date : 31-03-2020 12:39 AM
		 * Changing line breaker for JAVAX mail and Send grid mail api
		 */
		String lb="\n";
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Approvals", "EnableSendGridEmailApi", approve.getCompanyId())){
			lb="<br>";
		}
		
		String message11 = "Dear " + approve.getApproverName() + "," + lb
				+  lb+ lb+ approve.getBusinessprocesstype() + "-"
				+ approve.getBusinessprocessId()
				+ " is awaiting for your approval." +lb+ lb+ lb
				+ "Follow below steps for Approval/Reject" + lb+ lb
				+ "1: Log in to " + comp.getCompanyURL()
				+ " with your username and passward." + lb+ lb
				+ "2: Click on approval menu." + lb+ lb
				+ "3: Again click on approval node at left." +  lb+ lb
				+ "4: Click approval." + lb+ lb + "5: Verify document."
				+ lb+ lb
				+ "6: Either approve or reject with appropriate remark." + lb
				+ lb+ lb + "Requester : " + approve.getRequestedBy()
				+lb+ lb+ lb+ lb;
		
		logger.log(Level.SEVERE, "Initiate approval mail  "+message11);
		/**
		 *  nidhi
		 *  15-1-2018
		 *  updated for Purchase Requisition details
		 */
		boolean processFlag =false;
		try {
			
			ArrayList<String> header = new ArrayList<String>();
			ArrayList<String> headerDetails = new ArrayList<String>();
			
			if(approve.getBusinessprocesstype().equals("Purchase Requisition")){
				 processFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition", "SENDPRDETAILINAPPROVALMAIL", approve.getCompanyId());
				
				
				if(processFlag){
					 message11 = "Dear " + approve.getApproverName() + "," + "<br>"
							+ "<br>" + "<br>" + approve.getBusinessprocesstype() + "-"
							+ approve.getBusinessprocessId()
							+ " is awaiting for your approval." + "<br>" + "<br>" + "<br>"
							+ "Follow below steps for Approval/Reject" + "<br>" + "<br>"
							+ "1: Log in to " + comp.getCompanyURL()
							+ " with your username and passward." + "<br>" + "<br>"
							+ "2: Click on approval menu." + "<br>" + "<br>"
							+ "3: Again click on approval node at left." + "<br>" + "<br>"
							+ "4: Click approval." + "<br>" + "<br>" + "5: Verify document."
							+ "<br>" + "<br>"
							+ "6: Either approve or reject with appropriate remark." + "\n"
							+ "<br>" + "<br>" + "Requester : " + approve.getRequestedBy()
							+"<br><br><br><br>";
					
					PurchaseRequisition purDetail = ofy().load().type(PurchaseRequisition.class)
							.filter("companyId", approve.getCompanyId())
							.filter("count", approve.getBusinessprocessId())
							.first().now();
					if(purDetail !=null){
						header.add("PR ID");
						header.add("PR TITLE");
						header.add("PR DATE");
						header.add("EXPECTED DELIVERY DATE");
						header.add("PROJECT");
						header.add("PR GROUP");
						header.add("PR CATEGORY");
						header.add("PR TYPE");
						header.add("BRANCH");
						header.add("DESCRIPTION");
						header.add("PRODUCT CODE");
						header.add("PRODUCT NAME");
						header.add("PRODUCT CATEGORY");
						header.add("QUANTITY");
						header.add("UOM");
						header.add("TOTAL");
						header.add("REMARK");
						
						
						SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy");
						
						for(ProductDetails pur : purDetail.getPrproduct()){
							headerDetails.add(purDetail.getCount()+"");
							headerDetails.add(purDetail.getPrTitle());
							if(purDetail.getCreationDate()!=null){
								headerDetails.add(""+ sd.format(DateUtility.getDateWithTimeZone("IST", purDetail.getCreationDate()))+"");
							}else{
								headerDetails.add("");
							}
							
							
							if(purDetail.getExpectedDeliveryDate()!=null){
								headerDetails.add(""+ sd.format(DateUtility.getDateWithTimeZone("IST", purDetail.getExpectedDeliveryDate()))+"");
							}else{
								headerDetails.add("");
							}
							
							headerDetails.add(purDetail.getProject());
							headerDetails.add(purDetail.getPrGroup());
							headerDetails.add(purDetail.getPrCategory());
							headerDetails.add(purDetail.getPrType());
							headerDetails.add(purDetail.getBranch());
							headerDetails.add(purDetail.getDescription());
							headerDetails.add(pur.getProductCode());
							headerDetails.add(pur.getProductName());
							headerDetails.add(pur.getProductCategory());
							headerDetails.add(pur.getProductQuantity()+"");
							headerDetails.add(pur.getUnitOfmeasurement());
							headerDetails.add(pur.getTotal()+"");
							headerDetails.add(purDetail.getRemark());
						}
						
						
						
					}
				}
			}
			
			/**
			 * end
			 * 
			 */
			
			System.out.println("Inside try calling Method...");
			logger.log(Level.SEVERE, "before To Call Common Method");

			/*
			 * date 15 sep 2016 Release Name :- 30 sep 2016 by - vijay
			 * Description - Approved email to To approver and CC sales person
			 * or purchase engineer or person responsible and CC to requester
			 * means loggedInuser
			 */

			Employee employee = null;
			Employee approveremployee = null;
			Employee requesteremployee = null;
            
			/**
			 * Loding the employee info For documentCreatedBy Date : 14-10-2016
			 * By ANIL Release :30 SEPT 2016 Project : PURCHASE MODIFICATION
			 * (NBHC)
			 */
			Employee documentCreatedByEmployee = null;
			if (approve.getDocumentCreatedBy() != null) {
				documentCreatedByEmployee = ofy().load().type(Employee.class)
						.filter("companyId", approve.getCompanyId())
						.filter("fullname", approve.getDocumentCreatedBy())
						.first().now();
			}
			/**
			 * End
			 */

			requesteremployee = ofy().load().type(Employee.class)
					.filter("companyId", approve.getCompanyId())
					.filter("fullname", approve.getRequestedBy()).first().now();
			approveremployee = ofy().load().type(Employee.class)
					.filter("companyId", approve.getCompanyId())
					.filter("fullname", approve.getApproverName()).first()
					.now();
			
			/**Date 8-9-2020 by Amol approval mail should sent on user email id (email on user screen)
			 * raised by Rahul T.
			 * 
			 */
			
			User user=ofy().load().type(User.class)
					.filter("companyId", approve.getCompanyId())
					.filter("employeeName", approve.getApproverName()).filter("status", true).first()
					.now();
			
			employee = ofy().load().type(Employee.class)
					.filter("companyId", approve.getCompanyId())
					.filter("fullname", approve.getPersonResponsible()).first()
					.now();

			ArrayList<String> toEmailList = new ArrayList<String>();
			ArrayList<String> ccEmailList = new ArrayList<String>();

//			if (approveremployee != null) {
//				if (!approveremployee.getEmail().trim().equals(""))
//					toEmailList.add(approveremployee.getEmail().trim());
//			}
			if (user != null) {
				if (!user.getEmail().trim().equals(""))
					toEmailList.add(user.getEmail().trim());
				logger.log(Level.SEVERE, "user email "+user.getEmail().trim());
			}
			
			
			if (employee != null) {
				if (!employee.getEmail().trim().equals(""))
					ccEmailList.add(employee.getEmail().trim());
			}

			if (requesteremployee != null) {
				if (!requesteremployee.getEmail().trim().equals(""))
					ccEmailList.add(requesteremployee.getEmail().trim());
			}

			logger.log(Level.SEVERE, "ccEmailList ====" + ccEmailList);

			/**
			 * Continue From The Above
			 */
			if (documentCreatedByEmployee != null) {
				if (!documentCreatedByEmployee.getEmail().trim().equals("")) {
					ccEmailList
							.add(documentCreatedByEmployee.getEmail().trim());
				}
			}

			/**
			 * End
			 */

			/*
			 * Date 21 sep - 2016 Release - 30 sep 2016 bellow code for checking
			 * multilevel approval is enable or not if enable then first we get
			 * all approver name then we add their email Id in cc list
			 */
			boolean flagmulitlevelApproval = false;
			List<MultilevelApproval> multilevelapprovalist = null;
			// List<MultilevelApprovalDetails> multiLeveleList=new
			// ArrayList<MultilevelApprovalDetails>();

			ProcessName processname = ofy().load().type(ProcessName.class)
					.filter("processName", "MultilevelApproval")
					.filter("status", true)
					.filter("companyId", approve.getCompanyId()).first().now();
			if (processname != null) {
				multilevelapprovalist = ofy().load()
						.type(MultilevelApproval.class)
						.filter("companyId", approve.getCompanyId()).list();
				for (int i = 0; i < multilevelapprovalist.size(); i++) {
					if (multilevelapprovalist.get(i).getDocumentType().trim()
							.equals(approve.getBusinessprocesstype().trim())
							&& multilevelapprovalist.get(i).isStatus() == true) {
						flagmulitlevelApproval = true;
						// multiLeveleList=multilevelapprovalist.get(i).getApprovalLevelDetails();
					}
				}
			}

		
			/** Date 28-04-2017 commented by vijay for this is no need deadstock release -II NBHC 
			 * cc will be last requester and purchase engineer and document created and To for Approver Name
			 */
			
//			ArrayList<String> ccEmployeeList = new ArrayList<String>();
//
//			logger.log(Level.SEVERE, "Multilevel approval flag ===="
//					+ flagmulitlevelApproval);
//
//			if (flagmulitlevelApproval = true) {
//
//				/**
//				 * Vijay commented code because here we sending email for all
//				 * approval level Date :4/3/2017
//				 */
//
//				// for(int i=0;i<multiLeveleList.size();i++){
//				//
//				// if(!approve.getApproverName().equals(multiLeveleList.get(i).getEmployeeName())){
//				// ccEmployeeList.add(multiLeveleList.get(i).getEmployeeName());
//				// logger.log(Level.SEVERE,"Employee Name of mulitlevel approval"+multiLeveleList.get(i).getEmployeeName());
//				//
//				// }
//				//
//				// }
//
//				/**
//				 * newcode by vijay on * Date :4/3/2017 for sending email to all
//				 * last approval levels
//				 */
//				
//				
//				 
////				for (int i = 0; i < approve.getRemarkList().size(); i++) {
////					if (!approve.getApproverName().equals(
////							approve.getRemarkList().get(i).getApproverName())) {
////						ccEmployeeList.add(approve.getRemarkList().get(i)
////								.getApproverName());
////					}
////				}
//
//			}
//
//			for (int p = 0; p < ccEmployeeList.size(); p++) {
//				Employee multilevlapprover = ofy().load().type(Employee.class)
//						.filter("companyId", approve.getCompanyId())
//						.filter("fullname", ccEmployeeList.get(p)).first()
//						.now();
//				if (multilevlapprover != null) {
//					ccEmailList.add(multilevlapprover.getEmail().trim());
//
//				}
//			}

			logger.log(Level.SEVERE, "CC Email  == " + ccEmailList);

			// end here

			logger.log(Level.SEVERE, "Ready To Call Common Method");
			if(processFlag){
				sendEmailOnRequestForApproval("Request for approval : "+ approve.getBusinessprocesstype() + "-"+ approve.getBusinessprocessId(),
						approve.getBusinessprocessId(),approve.getBusinessprocesstype(), null, null, message11,
						comp.getEmail(), toEmailList, ccEmailList,header,headerDetails);
			}else{
				sendEmailOnRequestForApproval(
						"Request for approval : "
								+ approve.getBusinessprocesstype() + "-"
								+ approve.getBusinessprocessId(),
						approve.getBusinessprocessId(),
						approve.getBusinessprocesstype(), null, null, message11,
						comp.getEmail(), toEmailList, ccEmailList);
			}
			
		}

		catch (IOException e) {
			e.printStackTrace();
		}
		
	   }
	}

	private void setApprovals(Approvals approve) {
		this.approve = approve;
	}

	private void initializeApprovals() {
		System.out.println("Eamil approvals: " + approve.getCount());

		if (approve.getCompanyId() != null) {
			comp = ofy().load().type(Company.class).id(approve.getCompanyId())
					.now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

	}

	// *****************************************************************************
	@Override
	public void intiateOnApproveRequestEmail(Approvals approve)
			throws IllegalArgumentException {

		logger.log(Level.SEVERE,
				"hiiiiii vijay ==  " + approve.getApprovalOrRejectDate());

		String message11 = "Dear " + approve.getApproverName() + "," + "\n"
				+ "\n" + "\n" + approve.getBusinessprocesstype() + "-"
				+ approve.getBusinessprocessId() + " has been "
				+ approve.getStatus() + "\n" + "\n" + "Remark : "
				+ approve.getRemark() + "\n" + "\n" + approve.getStatus()
				+ " Date : " + fmt.format(approve.getApprovalOrRejectDate())
				+ "\n" + "\n" + "\n" + "Approver : "
				+ approve.getApproverName();

		setApprovals1(approve);
		initializeApprovals1();
		try {
			System.out.println("Inside try calling Method...");
			logger.log(Level.SEVERE, "Ready To Call Common Method");

			/*
			 * date 15 sep 2016 Release Name :- 30 sep 2016 by - vijay
			 * Description - Approved email to To approver and CC sales person
			 * or purchase engineer or person responsible and CC to requester
			 * means loggedInuser
			 */

			Employee employee = null;
			Employee approveremployee = null;
			Employee requesteremployee = null;

			/**
			 * Loding the employee info For documentCreatedBy Date : 14-10-2016
			 * By ANIL Release :30 SEPT 2016 Project : PURCHASE MODIFICATION
			 * (NBHC)
			 */
			Employee documentCreatedByEmployee = null;
			if (approve.getDocumentCreatedBy() != null) {
				documentCreatedByEmployee = ofy().load().type(Employee.class)
						.filter("companyId", approve.getCompanyId())
						.filter("fullname", approve.getDocumentCreatedBy())
						.first().now();
			}
			/**
			 * End
			 */

			logger.log(Level.SEVERE,
					"Employeeeeee ==" + approve.getPersonResponsible());

			requesteremployee = ofy().load().type(Employee.class)
					.filter("companyId", approve.getCompanyId())
					.filter("fullname", approve.getRequestedBy()).first().now();
			approveremployee = ofy().load().type(Employee.class)
					.filter("companyId", approve.getCompanyId())
					.filter("fullname", approve.getApproverName()).first()
					.now();
			employee = ofy().load().type(Employee.class)
					.filter("companyId", approve.getCompanyId())
					.filter("fullname", approve.getPersonResponsible()).first()
					.now();

			ArrayList<String> toEmailList = new ArrayList<String>();
			ArrayList<String> ccEmailList = new ArrayList<String>();

			if (approveremployee != null) {
				if (!approveremployee.getEmail().trim().equals(""))
					toEmailList.add(approveremployee.getEmail().trim());
			}
			if (employee != null) {
				if (!employee.getEmail().trim().equals(""))
					ccEmailList.add(employee.getEmail().trim());
			}
			if (requesteremployee != null) {
				if (!requesteremployee.getEmail().trim().equals(""))
					ccEmailList.add(requesteremployee.getEmail().trim());
			}

			logger.log(Level.SEVERE,
					" hi vijay before adding multilevel approval Email ====="
							+ ccEmailList);

			/**
			 * Continue From The Above
			 */
			if (documentCreatedByEmployee != null) {
				if (!documentCreatedByEmployee.getEmail().trim().equals("")) {
					ccEmailList
							.add(documentCreatedByEmployee.getEmail().trim());
				}
			}

			/**
			 * End
			 */

			/*
			 * Date 21 sep - 2016 Release - 30 sep 2016 bellow code for checking
			 * multilevel approval is enable or not if enable then first we get
			 * all approver name then we add their email Id in cc list
			 */
			boolean flagmulitlevelApproval = false;
			List<MultilevelApproval> multilevelapprovalist = null;
			// List<MultilevelApprovalDetails> multiLeveleList=new
			// ArrayList<MultilevelApprovalDetails>();

			ProcessName processname = ofy().load().type(ProcessName.class)
					.filter("processName", "MultilevelApproval")
					.filter("status", true)
					.filter("companyId", approve.getCompanyId()).first().now();
			if (processname != null) {
				multilevelapprovalist = ofy().load()
						.type(MultilevelApproval.class)
						.filter("companyId", approve.getCompanyId()).list();
				for (int i = 0; i < multilevelapprovalist.size(); i++) {
					if (multilevelapprovalist.get(i).getDocumentType().trim()
							.equals(approve.getBusinessprocesstype().trim())
							&& multilevelapprovalist.get(i).isStatus() == true) {
						flagmulitlevelApproval = true;
						// multiLeveleList=multilevelapprovalist.get(i).getApprovalLevelDetails();
					}
				}
			}

			/**
			 * Date 28-04-2017 commented by vijay for this is no need deadstock release -II NBHC 
			 * cc will be last requester and purchase engineer and document created and To for Approver Name
			 */		
//			ArrayList<String> ccEmployeeList = new ArrayList<String>();
//
//			logger.log(Level.SEVERE, "Multilevel approval flag ===="
//					+ flagmulitlevelApproval);
//
//			if (flagmulitlevelApproval = true) {
//
//				/**
//				 * Vijay commented code because here we sending email for all
//				 * approval level Date :4/3/2017
//				 */
//
//				// for(int i=0;i<multiLeveleList.size();i++){
//				//
//				// if(!approve.getApproverName().equals(multiLeveleList.get(i).getEmployeeName())){
//				// ccEmployeeList.add(multiLeveleList.get(i).getEmployeeName());
//				// logger.log(Level.SEVERE,"Employee Name of mulitlevel approval"+multiLeveleList.get(i).getEmployeeName());
//				//
//				// }
//				//
//				// }
//
//				/**
//				 * newcode by vijay on Date :4/3/2017 for sending email to all
//				 * last approval levels
//				 */
//				for (int i = 0; i < approve.getRemarkList().size(); i++) {
//					if (!approve.getApproverName().equals(
//							approve.getRemarkList().get(i).getApproverName())) {
//						ccEmployeeList.add(approve.getRemarkList().get(i)
//								.getApproverName());
//					}
//				}
//
//			}
//
//			for (int p = 0; p < ccEmployeeList.size(); p++) {
//				Employee multilevlapprover = ofy().load().type(Employee.class)
//						.filter("companyId", approve.getCompanyId())
//						.filter("fullname", ccEmployeeList.get(p)).first()
//						.now();
//				if (multilevlapprover != null) {
//					ccEmailList.add(multilevlapprover.getEmail().trim());
//
//				}
//			}

			logger.log(Level.SEVERE, "CC Email  == " + ccEmailList);
			// end here

			logger.log(Level.SEVERE, "Ready To Call Common Method");

			sendEmailOnRequestForApproval(
					approve.getStatus() + " : "
							+ approve.getBusinessprocesstype() + "-"
							+ approve.getBusinessprocessId(),
					approve.getBusinessprocessId(),
					approve.getBusinessprocesstype(),
					approve.getApprovalOrRejectDate(), approve.getRemark(),
					message11, comp.getEmail(), toEmailList, ccEmailList);
		}

		catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void setApprovals1(Approvals approve) {
		this.approve = approve;
	}

	private void initializeApprovals1() {
		System.out.println("Eamil approvals: " + approve.getCount());

		if (approve.getCompanyId() != null) {
			comp = ofy().load().type(Company.class).id(approve.getCompanyId())
					.now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

	}

	/******************************************* Invoice Details ******************************************/

	@Override
	public void initiateInvoiceEmail(Invoice invDetails)
			throws IllegalArgumentException {

		setInvoice(invDetails);
		initializeInvoiceDetails();
		if (AppConstants.ORDERTYPESALES.equals(invDetails.getTypeOfOrder().trim())) {
			createSalesInvoiceAttachmentGST();
		}
		if (AppConstants.ORDERTYPEFORSERVICE.equals(invDetails.getTypeOfOrder().trim())) {

			/*
			 * Rohan added this code for Sending Customer Customization PDF
			 * prints in Email Date : 15-09-2016
			 */
			/**
			 * 2-11-2017 sagar sore [if condition for processconfiguration
			 * checking of gstquotation pdf added and boolean flag used to send
			 * attachment in case of status of all processes is false]
			 **/
			boolean flag = false;
			/**
			 * Date : 04-12-2017 By JAYSHREE
			 */
			boolean companyAsLetterHead=false;
			
			ProcessConfiguration process = null;
			process = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", invDetails.getCompanyId())
					.filter("processName", "Invoice")
					.filter("processList.status", true).first().now();

			if (process != null) {
				for (int i = 0; i < process.getProcessList().size(); i++) {

//					if (process.getProcessList().get(i).getProcessType().trim()
//							.equalsIgnoreCase("GSTQUOTATIONPDF")
//							&& process.getProcessList().get(i).isStatus() == true) {
//						logger.log(Level.SEVERE, "Gst");
//						createServiceInvoiceAttachmentGST();
//						flag = true;
//						break;
//
//					}else 
					if (process.getProcessList().get(i).getProcessType()
							.trim().equalsIgnoreCase("PestoIndiaQuotations")
							&& process.getProcessList().get(i).isStatus() == true) {
						flag = true;
						pestoIndiaInvoiceAttchment();
						break;

					} else if (process.getProcessList().get(i).getProcessType()
							.trim().equalsIgnoreCase("VCareCustomization")
							&& process.getProcessList().get(i).isStatus() == true) {
						flag = true;
						vCareInvoiceAttchment();
						break;

					}

					else if (process.getProcessList().get(i).getProcessType()
							.trim()
							.equalsIgnoreCase("InvoiceAndRefDocCustmization")
							&& process.getProcessList().get(i).isStatus() == true) {
						flag = true;
						omPestInvoiceInvoiceAttachment();
						break;

					}

					else if (process.getProcessList().get(i).getProcessType()
							.trim().equalsIgnoreCase("HygeiaPestManagement")
							&& process.getProcessList().get(i).isStatus() == true) {
						flag = true;
						hygeiaPestManagementInvoiceAttachment();
						break;

					} else if (process.getProcessList().get(i).getProcessType()
							.trim().equalsIgnoreCase("PCAMBCustomization")
							&& process.getProcessList().get(i).isStatus() == true) {
						logger.log(Level.SEVERE, "PCAMBCustomization");
						pcambInvoiceAttachment();
						flag = true;
						break;

					}

					else if (process.getProcessList().get(i).getProcessType()
							.trim()
							.equalsIgnoreCase("ReddysPestControlQuotations")
							&& process.getProcessList().get(i).isStatus() == true) {
						logger.log(Level.SEVERE, "ReddysPestControlQuotations");
						reddysPestControlInvoiceAttachment();
						flag = true;
						break;
					}

					else if (process.getProcessList().get(i).getProcessType()
							.trim().equalsIgnoreCase("OnlyForPecopp")
							&& process.getProcessList().get(i).isStatus() == true) {
						logger.log(Level.SEVERE, "OnlyForPecopp");
						pecoppInvoiceAttachment(invDetails.getCompanyId());
						flag = true;
						break;

					}else if (process.getProcessList().get(i).getProcessType()
							.trim().equalsIgnoreCase("OnlyForOrionPestSolutions")
							&& process.getProcessList().get(i).isStatus() == true) {
						/**
						 * @author Anil ,Date : 29-03-2019
						 * 
						 */
						
						logger.log(Level.SEVERE, "OnlyForOrionPestSolutions");
						orionInvoiceAttachment();
						flag = true;
						break;

					}
					
					/**Date 4/12/2017
					 * By Jayshree
					 * To check the process configration for letterhead
					 */
					else if(process.getProcessList().get(i).getProcessType()
							.trim().equalsIgnoreCase("COMPANYASLETTERHEAD")
							&& process.getProcessList().get(i).isStatus() == true){
						companyAsLetterHead=true;
						
					}//End By jayshree
				}
				if (!flag) {
					logger.log(Level.SEVERE, "eva pest");
//					createServiceInvoiceAttachment();
					/**Date 4/12/2017
					 * By Jayshree
					 * To call the method createServiceInvoiceAttachmentGST
					 */
					createServiceInvoiceAttachmentGST(companyAsLetterHead);
					//End By Jayshree
				}
			} else {
				logger.log(Level.SEVERE, "default invoice ");
//				createServiceInvoiceAttachment();
				/**Date 4/12/2017
				 * By Jayshree
				 * To call the method createServiceInvoiceAttachmentGST
				 */
				
				createServiceInvoiceAttachmentGST(companyAsLetterHead);
				//End By Jayshree
			}
		}
		if (AppConstants.ORDERTYPEPURCHASE.equals(invDetails.getTypeOfOrder()
				.trim())) {
			logger.log(Level.SEVERE, "purchase");
			createPurchaseInvoiceAttachment(invDetails.getCompanyId());
		}
		
		initializeInvoiceDetailsEmail();
	}

	private void setInvoice(Invoice invDetails) {
		this.invDetails = invDetails;
	}

	private void initializeInvoiceDetails() {

		if (invDetails.getCompanyId() != null) {
			comp = ofy().load().type(Company.class)
					.id(invDetails.getCompanyId()).now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		if (invDetails.getCompanyId() != null) {
			c = ofy().load().type(Customer.class)
					.filter("count", invDetails.getPersonInfo().getCount())
					.filter("companyId", invDetails.getCompanyId()).first()
					.now();
		} else {
			c = ofy().load().type(Customer.class)
					.filter("count", invDetails.getPersonInfo().getCount())
					.first().now();
		}

		if (invDetails.getCompanyId() != null) {
			servcont = ofy().load().type(Contract.class)
					.filter("count", invDetails.getContractCount())
					.filter("companyId", invDetails.getCompanyId()).first()
					.now();
		} else {
			servcont = ofy().load().type(Contract.class)
					.filter("count", invDetails.getContractCount()).first()
					.now();
		}
		if (invDetails.getCompanyId() != null) {
			salesord = ofy().load().type(SalesOrder.class)
					.filter("count", invDetails.getContractCount())
					.filter("companyId", invDetails.getCompanyId()).first()
					.now();
		} else {
			salesord = ofy().load().type(SalesOrder.class)
					.filter("count", invDetails.getContractCount()).first()
					.now();
		}

		if (invDetails.getCompanyId() != null) {
			po = ofy().load().type(PurchaseOrder.class)
					.filter("count", invDetails.getContractCount())
					.filter("companyId", invDetails.getCompanyId()).first()
					.now();
		} else {
			po = ofy().load().type(PurchaseOrder.class)
					.filter("count", invDetails.getContractCount()).first()
					.now();
		}

		if (invDetails.getCompanyId() != null
				&& invDetails.getTypeOfOrder().trim()
						.equals(AppConstants.ORDERTYPEPURCHASE)) {
			v = ofy().load().type(Vendor.class)
					.filter("count", invDetails.getPersonInfo().getCount())
					.filter("companyId", invDetails.getCompanyId()).first()
					.now();
		}

		if (invDetails.getCompanyId() != null)
			billingEntity = ofy().load().type(BillingDocument.class)
					.filter("companyId", invDetails.getCompanyId())
					.filter("contractCount", invDetails.getContractCount())
					.filter("invoiceCount", invDetails.getCount())
					.filter("typeOfOrder", invDetails.getTypeOfOrder().trim())
					.first().now();
		else
			billingEntity = ofy().load().type(BillingDocument.class)
					.filter("contractCount", invDetails.getContractCount())
					.filter("invoiceCount", invDetails.getCount())
					.filter("typeOfOrder", invDetails.getTypeOfOrder().trim())
					.first().now();

		if (invDetails.getCompanyId() == null)
			custbranchlist = ofy()
					.load()
					.type(CustomerBranchDetails.class)
					.filter("cinfo.count",
							invDetails.getPersonInfo().getCount()).list();
		else
			custbranchlist = ofy()
					.load()
					.type(CustomerBranchDetails.class)
					.filter("cinfo.count",
							invDetails.getPersonInfo().getCount())
					.filter("companyId", invDetails.getCompanyId()).list();

	}

	private void createSalesInvoiceAttachment() {
		salesInvoicePdf = new SalesInvoicePdf();
		salesInvoicePdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(salesInvoicePdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		salesInvoicePdf.document.open();
		salesInvoicePdf.createPdfForEmail(invDetails, comp, c, salesord, null,
				null);
		salesInvoicePdf.document.close();
	}

	/** 23-10-2017 sagar sore [to create pdf of invoice with GST] **/
	private void createSalesInvoiceAttachmentGST() {

		salesGSTInvoice = new SalesGSTInvoice();
		salesGSTInvoice.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(salesGSTInvoice.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		salesGSTInvoice.document.open();

		salesGSTInvoice.createPdfForEmailGST(invDetails, comp, c, salesord,
				null, null, salesGSTInvoice, salesGSTInvoice.document);
		salesGSTInvoice.document.close();
	}

//	private void createPurchaseInvoiceAttachment() {
//		salesInvoicePdf = new SalesInvoicePdf();
//		salesInvoicePdf.document = new Document();
//		pdfstream = new ByteArrayOutputStream();
//		try {
//			PdfWriter.getInstance(salesInvoicePdf.document, pdfstream);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//		salesInvoicePdf.document.open();
//		salesInvoicePdf.createPdfForEmail(invDetails, comp, c, null, po, v);
//		salesInvoicePdf.document.close();
//	}

	private void createServiceInvoiceAttachment() {
		serviceInvoicePdf = new ServiceInvoicePdf();
		serviceInvoicePdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(serviceInvoicePdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		serviceInvoicePdf.document.open();
		serviceInvoicePdf.createPdfForEmail(invDetails, comp, c, servcont,
				billingEntity, custbranchlist);
		serviceInvoicePdf.document.close();

	}

	private void pestoIndiaInvoiceAttchment() {

		pestoIndiaInvoicePdf = new PestoIndiaInvoicePdf();
		pestoIndiaInvoicePdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(pestoIndiaInvoicePdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		pestoIndiaInvoicePdf.document.open();
		pestoIndiaInvoicePdf.createPdfForEmail(invDetails, comp, c, servcont,
				billingEntity);
		pestoIndiaInvoicePdf.document.close();
	}

	private void hygeiaPestManagementInvoiceAttachment() {

		hygeiaInvoicePdf = new HygeiaInvoicePdf();
		hygeiaInvoicePdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(hygeiaInvoicePdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		hygeiaInvoicePdf.document.open();
		hygeiaInvoicePdf.createPdfForEmail(invDetails, comp, c, servcont);
		hygeiaInvoicePdf.document.close();
	}

	private void omPestInvoiceInvoiceAttachment() {

		pmPestInvoicePdf = new InvoicePdf();
		pmPestInvoicePdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(pmPestInvoicePdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		pmPestInvoicePdf.document.open();
		pmPestInvoicePdf.createPdfForEmail(invDetails, comp, c, servcont);
		pmPestInvoicePdf.document.close();
	}

	private void vCareInvoiceAttchment() {

		vCareTaxinvoicePdf = new TaxInvoicePdf();
		vCareTaxinvoicePdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(vCareTaxinvoicePdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		vCareTaxinvoicePdf.document.open();
		vCareTaxinvoicePdf.createPdfForEmail(invDetails, comp, c, servcont);
		vCareTaxinvoicePdf.document.close();
	}

	/** 23-10-2017 sagar sore 
	 * @param companyAsLetterHead **/
	private void createServiceInvoiceAttachmentGST(boolean companyAsLetterHead) {
		serviceGSTInvoice = new ServiceGSTInvoice();
		serviceGSTInvoice.document = new Document();
		pdfstream = new ByteArrayOutputStream();

		try {
			PdfWriter.getInstance(serviceGSTInvoice.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		serviceGSTInvoice.document.open();
//		serviceGSTInvoice.createPdfForEmailGST(invDetails, comp, c, servcont,
//				billingEntity, custbranchlist, serviceGSTInvoice,
//				serviceGSTInvoice.document);
		
		serviceGSTInvoice.setInvoice(invDetails.getId());
		if(companyAsLetterHead){
			serviceGSTInvoice.createPdf("no");
		}
		else{
			serviceGSTInvoice.createPdf("plane");
		}
		
		serviceGSTInvoice.document.close();

	}
	
	/**
	 * @author Anil ,Date : 29-03-2019
	 * invoice email attachment for orion
	 */
	private void orionInvoiceAttachment() {

		OrionServiceInvoicePdf pdf = new OrionServiceInvoicePdf();
		pdf.document = new Document(PageSize.A4, 20, 20, 4, 5);
		Document document = pdf.document;
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(pdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.open();
		logger.log(Level.SEVERE, "INvoice id " + invDetails.getId()+ " invoice id " + invDetails.getCount());
		pdf.setInvoice(invDetails.getId());
		pdf.createPdf("Plan");
		document.close();
	}
	
	/**
	 *     Added By : Priyanka Bhagwat
	 *     Date : 1/06/2021
	 *     Des : Header and footer not coming in mail Req by Entomist raised By Ashwini.	
	 */
	private void pecoppInvoiceAttachment(Long companyid ) {
		logger.log(Level.SEVERE, "Inside Pecopp Attachment pdf ");
		Company companyEntity = comp;
		if(companyEntity == null) {
			companyEntity = ofy().load().type(Company.class).filter("companyId", invDetails.getCompanyId()).first().now();
		}
		pecoppInvoicePdf = new PecoppInvoicePrintPdf();
		pecoppInvoicePdf.document = new Document();
		Document document = pecoppInvoicePdf.document;
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(pecoppInvoicePdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.open();
		logger.log(Level.SEVERE, "INvoice id " + invDetails.getId()
				+ " invoice id " + invDetails.getCount());
		pecoppInvoicePdf.loadPecoppInvoice(invDetails.getId());
		/** Date : 29-01-2021 by Priyanka. **/
		//logger.log(Level.SEVERE, "CompanyASletterhead in invoice " +companyAsLetterHead);
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "CompanyAsLetterHead", companyid)){
			pecoppInvoicePdf.createInvoicePdf("no");
			}
		else{
			pecoppInvoicePdf.createInvoicePdf("plane");
		}
		
		
		document.close();
	}

	private void pcambInvoiceAttachment() {

		pcambPdf = new PcambInvoicepdf();
		pcambPdf.document = new Document();
		Document document = pcambPdf.document;
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(pcambPdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.open();
		pcambPdf.setInvoice((long) invDetails.getId());
		pcambPdf.createPdf();
		document.close();
	}

	private void reddysPestControlInvoiceAttachment() {
		rinvpdf = new reddysPestControlInvoice();
		rinvpdf.document = new Document();
		Document document = rinvpdf.document;
		try {
			PdfWriter.getInstance(document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.open();
		rinvpdf.setpdfrinvoice((long) invDetails.getId());
		rinvpdf.createPdf();
		document.close();
	}

	/** end **/
	private void initializeInvoiceDetailsEmail() {
		String custEmail = "";
		if (invDetails.getTypeOfOrder().trim()
				.equals(AppConstants.ORDERTYPESALES)) {
			custEmail = c.getEmail().trim();
		}

		if (invDetails.getTypeOfOrder().trim()
				.equals(AppConstants.ORDERTYPESERVICE)) {
			custEmail = c.getEmail().trim();
		}

		if (invDetails.getTypeOfOrder().trim()
				.equals(AppConstants.ORDERTYPEPURCHASE)) {
			custEmail = v.getEmail().trim();
		}

		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add(custEmail);

		// Table header 1
		ArrayList<String> tbl1_header = new ArrayList<String>();
		tbl1_header.add("Code");
		tbl1_header.add("Product Name");
		tbl1_header.add("Product Category");
		tbl1_header.add("Product Quantity");
		tbl1_header.add("Rate");
		// tbl1_header.add("VAT");
		// tbl1_header.add("Service Tax");
		System.out.println("Tbl1 header Size :" + tbl1_header.size());

		List<SalesOrderProductLineItem> productList = new ArrayList<SalesOrderProductLineItem>();
		productList = invDetails.getSalesOrderProducts();
		// Table 1
		/**
		 * @author Anil
		 * @since 21-05-2020
		 */
		double consolidatedPrice=0;
		if(invDetails.isConsolidatePrice()){
			for (int i = 0; i < productList.size(); i++) {
				consolidatedPrice=consolidatedPrice+productList.get(i).getPrice();
			}
		}
		ArrayList<String> tbl1 = new ArrayList<String>();
		for (int i = 0; i < productList.size(); i++) {
			tbl1.add(productList.get(i).getProdCode());
			tbl1.add(productList.get(i).getProdName());
			tbl1.add(productList.get(i).getProdCategory());
			tbl1.add(productList.get(i).getQuantity() + "");
			
			if(invDetails.isConsolidatePrice()) {
				tbl1.add(consolidatedPrice + "");
			}else {
				tbl1.add(productList.get(i).getPrice() + "");
			}
			
			// tbl1.add(productList.get(i).getVatTax().getPercentage()+"");
			// tbl1.add(productList.get(i).getServiceTax().getPercentage()+"");
		}
		System.out.println("Tbl1 Size :" + tbl1.size());
		// Table 2 header
		ArrayList<String> tbl2_header = new ArrayList<String>();
		tbl2_header.add("Sr. No.");
		tbl2_header.add("Days");
		tbl2_header.add("Percent");
		tbl2_header.add("Comment");
		System.out.println("Tbl2 header Size :" + tbl2_header.size());

		List<PaymentTerms> paymentTermsList = new ArrayList<PaymentTerms>();
		paymentTermsList = invDetails.getArrPayTerms();
		// Table 2
		ArrayList<String> tbl2 = new ArrayList<String>();
		for (int i = 0; i < paymentTermsList.size(); i++) {
			int j = 1;
			tbl2.add(j + "");
			tbl2.add(paymentTermsList.get(i).getPayTermDays() + "");
			tbl2.add(paymentTermsList.get(i).getPayTermPercent() + "");
			tbl2.add(paymentTermsList.get(i).getPayTermComment());
			j++;
		}
		System.out.println("Tbl2 Size :" + tbl2.size());

	String header = "Header";
	String footer = "Footer";
	String[] fileName = { invDetails.getCount() + " "
			+ invDetails.getPersonInfo().getFullName() };
	System.out.println("Header: " + header + " Footer: " + footer);
	
	/***
	 * consolidate invoice details to be set in social info of company object
	 */
	
	if(invDetails.isConsolidatePrice()){
		SocialInformation socialInfo=new SocialInformation();
		socialInfo.setFaceBookId("Consolidated Invoice");
		socialInfo.setTwitterId(invDetails.getSalesOrderProducts().size()+"");
		socialInfo.setGooglePlusId(4+"");
		logger.log(Level.SEVERE, "consolidated invoice "+invDetails.getSalesOrderProducts().size());
		comp.setSocialInfo(socialInfo);
	}else{
		logger.log(Level.SEVERE, "consolidated invoice false!!!!");
		comp.setSocialInfo(null);
	}
	
	
	/**
	 * 
	 */
	try {
		System.out.println("Inside try calling Method...");
		sendNewEmail(toEmailList, "Invoice Details", "Invoice Details",
				invDetails.getCount(), "Invoice Details",
				invDetails.getCreationDate(), comp, "Product Details",
				tbl1_header, tbl1, "Payment Terms", tbl2_header, tbl2,
				note, null, null, fileName, null,null,null);
	} catch (IOException e) {
		e.printStackTrace();
	}
}

	/****************************** payment Email *****************************************************/

	@Override
	public void initiateFumigationEmail(Fumigation fumigation)
			throws IllegalArgumentException {

		setFumigation(fumigation);
		initializeFumigationDetails();
		if (fumigation.isAustralia() == false) {
			if(fumigation.getNameoffumigation().equalsIgnoreCase("Methyle Bromide")){
				createFumigationMBAttachment();
			}else if(fumigation.getNameoffumigation().equalsIgnoreCase("Aluminium Phosphide")){
				createFumigationALPAttachment();
			}
		}
		if (fumigation.isAustralia() == true) {
			//createFumigationAusDetailsAttachment();
			createFumigationAFASAttachment();
		}
		initializeFumigationDetailsEmail();
	}

	private void setFumigation(Fumigation fumigation) {
		this.fumigation = fumigation;
	}

	private void initializeFumigationDetails() {

		if (fumigation.getCompanyId() != null) {
			comp = ofy().load().type(Company.class)
					.id(fumigation.getCompanyId()).now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		if (fumigation.getCompanyId() != null) {//changed by komal
			c = ofy().load().type(Customer.class)
					.filter("count", fumigation.getcInfo().getCount())
					.filter("companyId", fumigation.getCompanyId()).first()
					.now();
		} else {
			c = ofy().load().type(Customer.class)
					.filter("count", fumigation.getcInfo().getCount())
					.first().now();
		}

	}

	private void createFumigationAusDetailsAttachment() {
		fumigationAuspdf = new FumigationAusPdf();
		fumigationAuspdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(fumigationAuspdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		fumigationAuspdf.document.open();
		fumigationAuspdf.createPdfForEmail(fumigation, comp, c);
		fumigationAuspdf.document.close();
	}

	private void createFumigationDetailsAttachment() {
		fumigationpdf = new FumigationPdf();
		fumigationpdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(fumigationpdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		fumigationpdf.document.open();
		fumigationpdf.createPdfForEmail(fumigation, comp, c);
		fumigationpdf.document.close();

	}

	private void initializeFumigationDetailsEmail() {
		String custEmail = "";

		if (fumigation.isAustralia() == false) {

			custEmail = c.getEmail().trim();
		}
		if (fumigation.isAustralia() == true) {
			custEmail = c.getEmail().trim();
		}

		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add(custEmail);

		String[] fileName = { "Fumigation Certificate -" +fumigation.getCount() + " "};

		try {
			System.out.println("Inside try calling Method...");
			sendNewEmail(toEmailList, "Fumigation Certificate",
					"Fumigation Certificate", fumigation.getCount(),
					"Fumigation Certificate", fumigation.getCreationDate(), comp,
					null, null, null, null, null, null, note, null, null,
					fileName, null,null,null);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	// *******************************payment received
	// email****************************************

	@Override
	public void initiatePaymentReceiveEmail(CustomerPayment custPay)
			throws IllegalArgumentException {

		setPayment(custPay);
		initializePaymentDetails();

		/*
		 * Rohan added this code for Sending Customer Customization PDF prints
		 * in Email Date : 15-09-2016
		 */
		ProcessConfiguration process = null;
		/** 1-11-2017 sagar sore **/
		boolean flag = false;
		process = ofy().load().type(ProcessConfiguration.class)
				.filter("companyId", invDetails.getCompanyId())
				.filter("processName", "CustomerPayment")
				.filter("processList.status", true).first().now();

		if (process != null) {
			for (int i = 0; i < process.getProcessList().size(); i++) {

				if (process.getProcessList().get(i).getProcessType().trim()
						.equalsIgnoreCase("CompanyAsLetterHead")
						&& process.getProcessList().get(i).isStatus() == true) {

					createPaymentDetailsAttachment();
					flag = true;
				}
				if (process.getProcessList().get(i).getProcessType().trim()
						.equalsIgnoreCase("HygeiaPestManagement")
						&& process.getProcessList().get(i).isStatus() == true) {

					createPaymentDetailsAttachmentforHygeiaPestManagement();
					flag = true;
				}
			}
			/**
			 * 1-10-2017 sagar sore [Email in case status of all processes
			 * false]
			 **/
			if (flag == false) {
				logger.log(Level.SEVERE, "process config not matched");
				createPaymentDetailsAttachment();
			}
		} else {
			createPaymentDetailsAttachment();
		}

		initializePaymentDetailsEmail();
	}

	private void setPayment(CustomerPayment custPay) {
		this.customerPay = custPay;
	}

	private void initializePaymentDetails() {

		if (customerPay.getCompanyId() != null) {
			comp = ofy().load().type(Company.class)
					.id(customerPay.getCompanyId()).now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		if (customerPay.getCompanyId() != null) {
			c = ofy().load().type(Customer.class)
					.filter("count", customerPay.getPersonInfo().getCount())
					.filter("companyId", customerPay.getCompanyId()).first()
					.now();
		} else {
			c = ofy().load().type(Customer.class)
					.filter("count", customerPay.getPersonInfo().getCount())
					.first().now();
		}

	}

	private void createPaymentDetailsAttachment() {
		paymentPdf = new CustomerPaySlipPdf();
		paymentPdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(paymentPdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		paymentPdf.document.open();
		/** 1-11-2017 sagar sore **/
		// paymentPdf.createPdfForEmail(customerPay,comp,c);
		paymentPdf.createPdfForEmailGST(customerPay, comp, c,
				paymentPdf.document);
		paymentPdf.document.close();

	}

	/**
	 * rohan added this method for creating customize attachment for hygia
	 */

	private void createPaymentDetailsAttachmentforHygeiaPestManagement() {

		hygeaPaymentReceiptPdf = new HygeiaPaymentReceiptPdf();
		hygeaPaymentReceiptPdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(hygeaPaymentReceiptPdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		hygeaPaymentReceiptPdf.document.open();
		hygeaPaymentReceiptPdf.createPdfForEmail(customerPay, comp, c);
		hygeaPaymentReceiptPdf.document.close();
	}

	private void initializePaymentDetailsEmail() {
		String custEmail = "";

		String msgBoddy = "";

		String tatalmsg = "";

		msgBoddy = "dear sir / madam" + "\n"
				+ "We have received your payment of Rs "
				+ customerPay.getPaymentReceived() + " of order number "
				+ customerPay.getContractCount() + "\n" + "\n";

		tatalmsg = msgBoddy + note;

		custEmail = c.getEmail().trim();

		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add(custEmail);

		String[] fileName = { customerPay.getCount() + " "
				+ customerPay.getPersonInfo().getFullName() };
		try {
			System.out.println("Inside try calling Method...");
			sendNewEmail(toEmailList, "Payment Details", "Payment Details",
					customerPay.getCount(), "Payment Details",
					customerPay.getCreationDate(), comp, "Product Details",
					null, null, "Payment Terms", null, null, tatalmsg, null,
					null, fileName, null,null,null);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/************************************ Delivery Note Details ****************************************/

	@Override
	public void initiateDeliveryNoteEmail(DeliveryNote deliveryNoteDetails)
			throws IllegalArgumentException {

		setDeliveryNote(deliveryNoteDetails);
		initializeDeliveryDetails();
		createDeliveryAttachment();
		createDeliveryNoteUploadAttachment();
		initializeDeliveryDetailsEmail();
	}

	private void setDeliveryNote(DeliveryNote deliveryNoteDetails) {
		this.deliveryNoteDetails = deliveryNoteDetails;
	}

	private void initializeDeliveryDetails() {

		if (deliveryNoteDetails.getCompanyId() != null) {
			comp = ofy().load().type(Company.class)
					.id(deliveryNoteDetails.getCompanyId()).now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		if (deliveryNoteDetails.getCompanyId() != null) {
			c = ofy().load().type(Customer.class)
					.filter("count", deliveryNoteDetails.getCinfo().getCount())
					.filter("companyId", deliveryNoteDetails.getCompanyId())
					.first().now();
		} else {
			c = ofy().load().type(Customer.class)
					.filter("count", deliveryNoteDetails.getCinfo().getCount())
					.first().now();
		}
	}

	private void createDeliveryAttachment() {
		delNotePdf = new DeliveryNotePdf();
		delNotePdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(delNotePdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		delNotePdf.document.open();
		delNotePdf.createDeliveryNotePdfForEmail(deliveryNoteDetails, comp, c);
		delNotePdf.document.close();
	}

	private void createDeliveryNoteUploadAttachment() {
		blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		uploadStream = new ByteArrayOutputStream();

		String urlData = "";
		String fileName = "";

		if (deliveryNoteDetails.getDeliveryDocument() != null
				&& !deliveryNoteDetails.getDeliveryDocument().getUrl()
						.equals("")) {
			urlData = deliveryNoteDetails.getDeliveryDocument().getUrl();
			logger.log(Level.SEVERE, "Doc URL :::: " + urlData);

			fileName = deliveryNoteDetails.getDeliveryDocument().getName();
			logger.log(Level.SEVERE, "Document Name :::: " + fileName);

			String[] res = urlData.split("blob-key=", 0);
			logger.log(Level.SEVERE, "Splitted Url :::: " + res);

			String blob = res[1].trim();
			logger.log(Level.SEVERE, "Splitted Url Assigned to string  :::: "
					+ blob);

			BlobKey blobKey = null;
			try {
				blobKey = new BlobKey(blob);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);

			// This method read bytes from blobstore
			long inxStart = 0;
			long inxEnd = 1024;
			boolean flag = false;
			byte[] bytes = null;
			do {
				try {
					bytes = blobstoreService.fetchData(blobKey, inxStart,
							inxEnd);
					uploadStream.write(bytes);

					if (bytes.length < 1024)
						flag = true;

					inxStart = inxEnd + 1;
					inxEnd += 1025;

				} catch (Exception e) {
					flag = true;
				}

			} while (!flag);
			logger.log(Level.SEVERE, "Calling Email Method  :::: ");
		}
	}

	private void initializeDeliveryDetailsEmail() {

		String custEmail = c.getEmail().trim();

		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add(custEmail);

		// Table header 1
		ArrayList<String> tbl1_header = new ArrayList<String>();
		tbl1_header.add("Code");
		tbl1_header.add("Product Name");
		tbl1_header.add("Product Category");
		tbl1_header.add("Product Quantity");
		tbl1_header.add("Rate");
		System.out.println("Tbl1 header Size :" + tbl1_header.size());

		List<DeliveryLineItems> productList = new ArrayList<DeliveryLineItems>();
		productList = deliveryNoteDetails.getDeliveryItems();
		// Table 1
		ArrayList<String> tbl1 = new ArrayList<String>();
		for (int i = 0; i < productList.size(); i++) {
			tbl1.add(productList.get(i).getProdCode());
			tbl1.add(productList.get(i).getProdName());
			tbl1.add(productList.get(i).getProdCategory());
			tbl1.add(productList.get(i).getQuantity() + "");
			tbl1.add(productList.get(i).getPrice() + "");
		}
		System.out.println("Tbl1 Size :" + tbl1.size());
		// Table 2 header
		// ArrayList<String> tbl2_header=new ArrayList<String>();
		// tbl2_header.add("Sr. No.");
		// tbl2_header.add("Days");
		// tbl2_header.add("Percent");
		// tbl2_header.add("Comment");
		// System.out.println("Tbl2 header Size :"+tbl2_header.size());
		//
		// List<PaymentTerms> paymentTermsList=new ArrayList<PaymentTerms>();
		// paymentTermsList=deliveryNoteDetails.get;
		// // Table 2
		// ArrayList<String> tbl2=new ArrayList<String>();
		// for(int i=0;i<paymentTermsList.size();i++){
		// int j=1;
		// tbl2.add( j+"");
		// tbl2.add(paymentTermsList.get(i).getPayTermDays()+"");
		// tbl2.add(paymentTermsList.get(i).getPayTermPercent()+"");
		// tbl2.add(paymentTermsList.get(i).getPayTermComment());
		// j++;
		// }
		// System.out.println("Tbl2 Size :"+tbl2.size());

		String uploadDocName = null;
		if (deliveryNoteDetails.getDeliveryDocument() != null
				&& !deliveryNoteDetails.getDeliveryDocument().getUrl()
						.equals("")) {
			uploadDocName = deliveryNoteDetails.getDeliveryDocument().getName();
		}

		String header = "Header";
		String footer = "Footer";
		String[] fileName = { deliveryNoteDetails.getCount() + " "
				+ deliveryNoteDetails.getCinfo().getFullName() };
		System.out.println("Header: " + header + " Footer: " + footer);
		try {
			System.out.println("Inside try calling Method...");
			sendNewEmail(toEmailList, "Delivery Note", "Delivery Note",
					deliveryNoteDetails.getCount(), "Delivery Note",
					deliveryNoteDetails.getCreationDate(), comp,
					"Product Details", tbl1_header, tbl1, null, null, null,
					note, null, null, fileName, uploadDocName,null,null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**************************************** Inspection Email ************************************/

	@Override
	public void initiateInspectionEmail(Inspection inspection)
			throws IllegalArgumentException {
		setInspection(inspection);
		initializeInspection();
	}

	private void setInspection(Inspection inspection) {
		this.insp = inspection;

	}

	private void initializeInspection() {
		if (insp.getCompanyId() != null) {
			comp = ofy().load().type(Company.class)
					.filter("companyId", insp.getCompanyId()).first().now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}
		if (insp.getCompanyId() != null) {
			v = ofy().load().type(Vendor.class)
					.filter("companyId", insp.getCompanyId()).first().now();
		} else {
			v = ofy().load().type(Vendor.class).first().now();
		}

		// Vendor email id is added to emailList
		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add(insp.getVendorInfo().getEmail().trim());
		System.out.println("Email Id :" + toEmailList);

		String uploadDocName = null;
		if (insp.getUpload() != null && !insp.getUpload().getUrl().equals("")) {
			uploadDocName = insp.getUpload().getName();
		}

		String[] fileName = { insp.getCount() + "Inspection" };

		createInspectionUploadDocumentAttachment();
		createInspectionAttachment();

		try {
			System.out.println("Inside try calling Method...");
			sendNewEmail(toEmailList, "Inspection", "Inspection",
					insp.getCount(), insp.getInspectionTitle(),
					insp.getCreationDate(), comp, null, null, null, null, null,
					null, note, null, null, fileName, uploadDocName,null,null);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void createInspectionUploadDocumentAttachment() {
		blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		uploadStream = new ByteArrayOutputStream();

		String urlData = "";
		String fileName = "";

		if (insp.getUpload() != null && !insp.getUpload().getUrl().equals("")) {
			urlData = insp.getUpload().getUrl();
			logger.log(Level.SEVERE, "Doc URL :::: " + urlData);

			fileName = insp.getUpload().getName();
			logger.log(Level.SEVERE, "Document Name :::: " + fileName);

			String[] res = urlData.split("blob-key=", 0);
			logger.log(Level.SEVERE, "Splitted Url :::: " + res);

			String blob = res[1].trim();
			logger.log(Level.SEVERE, "Splitted Url Assigned to string  :::: "
					+ blob);

			BlobKey blobKey = null;
			try {
				blobKey = new BlobKey(blob);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);

			// This method read bytes from blobstore
			long inxStart = 0;
			long inxEnd = 1024;
			boolean flag = false;
			byte[] bytes = null;
			do {
				try {
					bytes = blobstoreService.fetchData(blobKey, inxStart,
							inxEnd);
					uploadStream.write(bytes);

					if (bytes.length < 1024)
						flag = true;

					inxStart = inxEnd + 1;
					inxEnd += 1025;

				} catch (Exception e) {
					flag = true;
				}

			} while (!flag);
			logger.log(Level.SEVERE, "Calling Email Method  :::: ");
		}

	}

	private void createInspectionAttachment() {
		inspdf = new InspectionPdf();
		inspdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(inspdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		inspdf.document.open();

		inspdf.createPdfForEmail(comp, v, insp);
		inspdf.document.close();

	}

	/*********************************************** Work Order Email *******************************************/

	@Override
	public void initiateWorkOrderEmail(WorkOrder workOrder)
			throws IllegalArgumentException {
		setWorkOrder(workOrder);
		createWorkOrderAttachment();
		createWorkOrderUploadDocumentAttachment();
		initializeWorkOrder();
	}

	private void setWorkOrder(WorkOrder wo2) {
		this.wo = wo2;

		if (wo.getCompanyId() != null) {
			comp = ofy().load().type(Company.class)
					.filter("companyId", wo.getCompanyId()).first().now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		if (wo.getApproverName() != null) {
			emp = ofy().load().type(Employee.class)
					.filter("companyId", wo.getCompanyId())
					.filter("fullname", wo.getApproverName().trim()).first()
					.now();
		} else {
			emp = ofy().load().type(Employee.class)
					.filter("fullname", wo.getApproverName().trim()).first()
					.now();
		}

		// if(wo.getApproverName()!=null){
		// emp=ofy().load().type(Employee.class).filter("companyId",wo.getCompanyId()).filter("fullname",
		// wo.getApproverName().trim()).first().now();
		// }
		// else{
		// emp=ofy().load().type(Employee.class).filter("fullname",
		// wo.getApproverName().trim()).first().now();
		// }
	}

	private void createWorkOrderAttachment() {
		wopdf = new WorkOrderPdf();
		wopdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(wopdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		wopdf.document.open();

		wopdf.createPdfForEmail(comp, wo);
		wopdf.document.close();
	}

	private void createWorkOrderUploadDocumentAttachment() {
		blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		uploadStream = new ByteArrayOutputStream();

		String urlData = "";
		String fileName = "";

		if (!wo.getUpload().equals("") && !wo.getUpload().getUrl().equals("")) {
			urlData = wo.getUpload().getUrl();
			logger.log(Level.SEVERE, "Doc URL :::: " + urlData);

			fileName = wo.getUpload().getName();
			logger.log(Level.SEVERE, "Document Name :::: " + fileName);

			String[] res = urlData.split("blob-key=", 0);
			logger.log(Level.SEVERE, "Splitted Url :::: " + res);

			String blob = res[1].trim();
			logger.log(Level.SEVERE, "Splitted Url Assigned to string  :::: "
					+ blob);

			BlobKey blobKey = null;
			try {
				blobKey = new BlobKey(blob);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);

			// This method read bytes from blobstore
			long inxStart = 0;
			long inxEnd = 1024;
			boolean flag = false;
			byte[] bytes = null;
			do {
				try {
					bytes = blobstoreService.fetchData(blobKey, inxStart,
							inxEnd);
					uploadStream.write(bytes);

					if (bytes.length < 1024)
						flag = true;

					inxStart = inxEnd + 1;
					inxEnd += 1025;

				} catch (Exception e) {
					flag = true;
				}

			} while (!flag);
			logger.log(Level.SEVERE, "Calling Email Method  :::: ");
		}
	}

	private void initializeWorkOrder() {

		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add(emp.getEmail().trim());
		System.out.println("Email Id :" + toEmailList);

		String uploadDocName = null;
		if (!wo.getUpload().equals("") && !wo.getUpload().getUrl().equals("")) {
			uploadDocName = wo.getUpload().getName();
		}

		String[] fileName = { wo.getCount() + "Work Order" };

		try {
			System.out.println("Inside try calling Method...");
			sendNewEmail(toEmailList, "Work Order", "Work Order",
					wo.getCount(), wo.getWoTitle(), wo.getCreationDate(), comp,
					null, null, null, null, null, null, null, null, null,
					fileName, uploadDocName,null,null);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/************************************ IP Address Authorization ****************************************/

	@Override
	public void initiateIPAddressAuthorizationEmail(String userID,
			String passward, String ipaddress, String compEmail)
			throws IllegalArgumentException {

		String message11 = "Dear Sir/ Madam," + "\n" + "\n" + "\n"
				+ "Someone is trying to access your software" + "\n" + "\n"

				+ "with User ID : " + userID + " Password : " + passward + "\n"
				+ "\n"

				+ "with an IP address : " + ipaddress + "\n" + "\n"

				+ "on date : " + fmt.format(new Date());

		try {
			System.out.println("Inside try calling Method...");
			logger.log(Level.SEVERE, "Ready To Call Common Method");

			sendEmailForIPAddressAuthorization(
					"ALERT : An recognise IP address ", message11, compEmail);
		}

		catch (IOException e) {
			e.printStackTrace();
		}

	}

	/******************************************** Common Email Method 
	 * @param ccemailidlist 
	 * @param fromEmailId *********************************************/

	public void sendNewEmail(ArrayList<String> toEmailId, String mailSubject,
			String mailTitle, int documentId, String documentTitle,
			Date documnetDate, Company company, String heading1,
			ArrayList<String> table1_Header, ArrayList<String> table1,
			String heading2, ArrayList<String> table2_Header,
			ArrayList<String> table2, String noteVal, String mailHeader,
			String mailFooter, String[] attachFiles, String uploadedFile, ArrayList<String> ccemailidlist, String fromEmailId)
			throws IOException {

		logger.log(Level.SEVERE, "Entered In Common Method");

		// to Email id's in array
		String[] toEmailIdArray = new String[toEmailId.size()];
		toEmailId.toArray(toEmailIdArray);
		System.out.println("Mail Sub : " + mailSubject);
		System.out.println("Mail Title : " + mailTitle);
		System.out.println("Mail Doc title : " + documentTitle);
		System.out.println("Mail Doc Date : " + fmt.format(documnetDate));
		System.out.println("Mail Header : " + mailHeader);
		System.out.println("Mail Footer : " + mailFooter);

		logger.log(Level.SEVERE, "Mail Sub : " + mailSubject);
		logger.log(Level.SEVERE, "Mail Title : " + mailTitle);
		logger.log(Level.SEVERE, "Mail Doc title : " + documentTitle);
		logger.log(Level.SEVERE, "Mail Doc Date : " + fmt.format(documnetDate));
		logger.log(Level.SEVERE, "Mail Header : " + mailHeader);
		logger.log(Level.SEVERE, "Mail Footer : " + mailFooter);

		StringBuilder builder = new StringBuilder();

		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("<h3 class=\"ex\" align=\"center\">" + mailTitle
				+ "</h3></br></br>");
		
		/** date 30.3.2019 added by komal to hide details when documenId  = 0;**/
		if(documentId != 0){
			builder.append("<B>Document Id :</B>" + documentId
					+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
					+ "<B>Document Title :</B>" + documentTitle
					+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
					+ "<B>Document Date :</B>" + fmt.format(documnetDate)
					+ "</h3></br></br>");
		}
		// Mail header
		if (mailHeader != null) {
			builder.append("</br></br>");
			builder.append("<p>" + mailHeader + "</p>" + "</br></br>");
		}

		/************************ Table 1 ***************************************/

//		if (table1 != null && !table1.isEmpty()) {
//			System.out.println("Table  1");
//			builder.append("<h2>" + heading1 + "</h2>");
//
//			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
//			builder.append("<tr>");
//
//			// Product Table1 heading
//			for (String header : table1_Header)
//				builder.append("<th>" + header + "</th>");
//			builder.append("</tr>");
//
//			System.out.println("Size of table 1 " + table1.size());
//
//			builder.append("<tr>");
//			for (int i = 0; i < table1.size(); i++) {
//				builder.append("<td>" + table1.get(i) + "</td>");
//				System.out.println("Table Started 1" + table1.get(i));
//				if (i > 0 && (i + 1) % table1_Header.size() == 0) {
//					builder.append("</tr><tr>");
//				}
//			}
//			builder.append(" </tr>  ");
//			builder.append("</table></b></br>");
//		}
		
		
		if (table1 != null && !table1.isEmpty()) {
			System.out.println("Table  1");
			builder.append("<h2>" + heading1 + "</h2>");

			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
//			builder.append("<table border=\"2\">");
			builder.append("<tr>");

			// Product Table1 heading
			for (String header : table1_Header)
				builder.append("<th>" + header + "</th>");
			builder.append("</tr>");

			System.out.println("Size of table 1 " + table1.size());

			/**
			 * write your logic for rowspan of price column
			 */

			if (comp != null&& comp.getSocialInfo() != null&& comp.getSocialInfo().getFaceBookId().equalsIgnoreCase("Consolidated Invoice")) {

				logger.log(Level.SEVERE, "consolidated invoice True ");
				double sumOfInvoiceRate = 0;
				int col = 0;
				int actualCol = 0;
				int totalRow = 0;
				if (comp.getSocialInfo().getTwitterId() != null&& !comp.getSocialInfo().getTwitterId().equals("")) {
					totalRow = Integer.parseInt(comp.getSocialInfo().getTwitterId());
				}
				if (comp.getSocialInfo().getGooglePlusId() != null&& !comp.getSocialInfo().getGooglePlusId().equals("")) {
					actualCol = Integer.parseInt(comp.getSocialInfo().getGooglePlusId());
				}
				logger.log(Level.SEVERE, "TotalRow Size " + totalRow);
				logger.log(Level.SEVERE, "Col No " + actualCol);
				boolean rowspanFlag = false;

				builder.append("<tr>");
				for (int i = 0; i < table1.size(); i++) {
//					builder.append("<td></td>");
					if (actualCol == col) {
						logger.log(Level.SEVERE, "actualCol " + actualCol);
						logger.log(Level.SEVERE, "Col " + col);
						logger.log(Level.SEVERE, "rowspanFlag " + rowspanFlag);
						if (rowspanFlag == false) {
							builder.append("<td rowspan=\""+comp.getSocialInfo().getTwitterId()+"\">"+ table1.get(i) + "</td>");
							rowspanFlag = true;
						}
					} else {
						builder.append("<td>" + table1.get(i) + "</td>");
					}
					col++;

					logger.log(Level.SEVERE, "Coloumn count" + col);
					System.out.println("Table Started 1" + table1.get(i));

					if (i > 0 && (i + 1) % table1_Header.size() == 0) {
						builder.append("</tr><tr>");
						logger.log(Level.SEVERE, "Break ");
						col = 0;
					}

				}

				builder.append("</tr>");

			} else {
				logger.log(Level.SEVERE, "consolidated invoice false 2");
				builder.append("<tr>");
				for (int i = 0; i < table1.size(); i++) {
//					builder.append("<td></td>");
					builder.append("<td>" + table1.get(i) + "</td>");
					System.out.println("Table Started 1" + table1.get(i));
					if (i > 0 && (i + 1) % table1_Header.size() == 0) {
						builder.append("</tr><tr>");
					}
				}
				builder.append(" </tr>  ");
			}

			builder.append("</table></b></br>");
		}

		/************************************* Table 2 ************************************/

		if (table2 != null && !table2.isEmpty()) {
			System.out.println("Table 2");
			builder.append("<h2>" + heading2 + "</h2>");

			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"1\"  Style width=\"50%\" >");
			builder.append("<col width=\"1%\" >");
			builder.append("<col width=\"2%\" >");
			builder.append("<col width=\"3%\" >");
			builder.append("<col width=\"10%\">");

			builder.append("<tr>");
			// Product Table2 heading
			for (String header : table2_Header)
				builder.append("<th>" + header + "</th>");

			builder.append("</tr>");

			System.out.println("Size of list " + table2.size());
			builder.append("<tr>");
			for (int i = 0; i < table2.size(); i++) {
				builder.append("<td style=\"width:5%\">" + table2.get(i)
						+ "</td>");
				System.out.println("Table Started 2 :" + table2.get(i));
				if (i > 0 && (i + 1) % table2_Header.size() == 0) {
					builder.append("</tr><tr>");
				}
			}
			builder.append(" </tr>  ");
			builder.append("</table></b></br>");
		}

		/************************************ Note *******************************************/
		if (noteVal != null) {
			builder.append("</br></br><p>" + noteVal + "</p></br></br>");
		}

		/**************************** FOOTER Details *********************************/
		if (mailFooter != null) {
			builder.append("<p>" + mailFooter + "</p></br></b></br>");
		}

		// Company Details
		if(mailSubject.contains("SR")){
			
			try{
				if(company.getCompanyURL()!=null&&!company.getCompanyURL().equals("")
						&&company.getLogo().getUrl()!=null&&!company.getLogo().getUrl().equals("")
						&&company.getLogo().getName()!=null&&!company.getLogo().getName().equals("")){
					String img2=company.getCompanyURL().toString().trim()+company.getLogo().getUrl()+"&filename="+company.getLogo().getName();
					logger.log(Level.SEVERE,"img2 msg --"+img2);
					builder.append("<br>");
					builder.append("</br>");
					builder.append("<img src="+img2);
					builder.append(" alt= " + "SR copy");
					builder.append(" width = \"200\"  height=\"200\" >");
				}
			}catch(Exception e){
				logger.log(Level.SEVERE,"IMG EXCEP --"+e.getMessage());
			}
			
			
			
		}else if(documentTitle.equals("Service Lead")){
			//no need to add signature as {CompanySignature} tag already there in template which will add signature
		}else {
			
			builder.append("<br>");
			builder.append("</br>");
			builder.append("<b>" + company.getBusinessUnitName() + "</b></br>");
			builder.append("<table>");
	
			builder.append("<tr>");
			builder.append("<td>");
			builder.append("Address : ");
			builder.append("</td>");
			builder.append("<td>");
			if (company.getAddress()!=null&& company.getAddress().getAddrLine2() != null&&!company.getAddress().getAddrLine2().equals("")) 
				builder.append(company.getAddress().getAddrLine1() + " "+ company.getAddress().getAddrLine2());				
			else
				builder.append(company.getAddress().getAddrLine1() );
			
			if (company.getAddress()!=null&& company.getAddress().getLocality()!=null)
				builder.append(company.getAddress().getLocality());
			
			builder.append("</td>");
			builder.append("</tr>");
	
			builder.append("<tr>");
			builder.append("<td>");
			builder.append("</td>");
			builder.append("<td>");
			if (!company.getAddress().getLandmark().equals("")&& company.getAddress().getLandmark() != null)
				builder.append(company.getAddress().getLandmark() + ","
						+ company.getAddress().getCity() + ","
						+ company.getAddress().getPin()
						+ company.getAddress().getState() + ","
						+ company.getAddress().getCountry());
			else
				builder.append(company.getAddress().getCity() + ","
						+ company.getAddress().getPin() + ","
						+ company.getAddress().getState() + ","
						+ company.getAddress().getCountry());
			builder.append("</td>");
			builder.append("</tr>");
	
			builder.append("<tr>");
			builder.append("<td>");
			builder.append("Website : ");
			builder.append("</td>");
			builder.append("<td>");
			builder.append(company.getWebsite());
			builder.append("</td>");
			builder.append("</tr>");
	
			builder.append("<tr>");
			builder.append("<td>");
			builder.append("Phone No : ");
			builder.append("</td>");
			builder.append("<td>");
			builder.append(company.getCellNumber1());
			builder.append("</td>");
			builder.append("<tr>");
			
			if(company.getEmail()!=null&&!company.getEmail().equals("")) {
				builder.append("<tr>");
				builder.append("<td>");
				builder.append("Email : ");
				builder.append("</td>");
				builder.append("<td>");
				builder.append(company.getEmail());
				builder.append("</td>");
				builder.append("</tr>");
			}
	
			builder.append("</tr>");
			builder.append("</table>");
		}
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);
		
		/**
		 * @author Anil
		 * @since 19-06-2020
		 * using uploadTc and uploadCp stream for sending service summary report 1 and 2 for PTSPL
		 */
		
		String uploadTc="TermsAndConditions.pdf";
		String uploadCp="CompanyProfile.pdf";
		
		if(attachFiles != null&&attachFiles.length>1){
			if(attachFiles.length==3){
				uploadTc=attachFiles[1];
				uploadCp=attachFiles[2];
			}else{
				uploadTc=attachFiles[1];
			}
		}
		
		/**
		 * @author Anil , Date : 10-04-2020
		 * added display name for from email id
		 * ,company.getDisplayNameForCompanyEmailId()
		 */
		logger.log(Level.SEVERE," company email -" +company.getEmail());
		if(ccemailidlist==null) {
			ccemailidlist = new ArrayList<String>();
		}
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CronJob", "EnableSendGridEmailApi",company.getCompanyId())){
			logger.log(Level.SEVERE," company email --" +company.getEmail() +"  to email  " + toEmailIdArray.toString());
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.PC_ENABLESENDEMAILSFROMPERSONRESPONSIBLE, company.getCompanyId())) {
				 Company compEntity = ofy().load().type(Company.class)
							.filter("companyId", company.getCompanyId()).first().now();
					ObjectifyService.reset();
				    ofy().consistency(Consistency.STRONG);
					ofy().clear();
					if(ccemailidlist!=null && compEntity.getEmail()!=null && !compEntity.getEmail().equals("")) {
						logger.log(Level.SEVERE, "company.getEmail() == "+compEntity.getEmail());
						ccemailidlist.add(compEntity.getEmail());
					}
					logger.log(Level.SEVERE, "Email CC "+ccemailidlist);
					logger.log(Level.SEVERE, "Email To "+toEmailId);
					ArrayList<String> bcclist = new ArrayList<String>();
					bcclist.add(fromEmailId);
				SendGridEmailServlet sdEmail=new SendGridEmailServlet();
				sdEmail.sendMailWithSendGrid(fromEmailId, toEmailId, ccemailidlist, bcclist, mailSubject, contentInHtml, "text/html",pdfstream,attachFiles[0],"application/pdf",null);
				return;
			}

				
			/**
			 * Date 30-09-2019 By Vijay
			 * Des :- NBHC CCPM A creation send email cc to Technical Manager
			 */
			if(mailSubject.trim().equals("AssessmentReport")){
				ArrayList<String> cclist = new ArrayList<String>();
				List<Employee> employeeList = ofy().load().type(Employee.class).filter("companyId", company.getCompanyId())
						.filter("roleName", "Technical Manager").list();
				if(employeeList.size()!=0){
					for(Employee emp : employeeList){
						if(emp.getEmail()!=null)
						cclist.add(emp.getEmail());
					}
				}
				logger.log(Level.SEVERE, "Email CC "+cclist);
				
				SendGridEmailServlet sdEmail=new SendGridEmailServlet();
				sdEmail.sendMailWithSendGrid(company.getEmail(), toEmailId, cclist, null, mailSubject, contentInHtml, "text/html",pdfstream,attachFiles[0]+".pdf","application/pdf",company.getDisplayNameForCompanyEmailId());
			}
			else{
				SendGridEmailServlet sdEmail=new SendGridEmailServlet();
				/**
				 * @author Anil @since 19-10-2021
				 * service report were not sent through sendgrid email api
				 */
//				sdEmail.sendMailWithSendGrid(company.getEmail(), toEmailId, null, null, mailSubject, contentInHtml, "text/html",pdfstream,attachFiles[0],"application/pdf",company.getDisplayNameForCompanyEmailId());
				if(fromEmailId!=null&&!fromEmailId.equals(""))//Ashwini Patil Date:27-04-2023 Pecopp has "Cronjob","EnableSendGridEmailApi" config active and sr copy email is not getting sent through branch email id so managing this
					sdEmail.sendMailWithSendGrid(fromEmailId, toEmailId, null, null, mailSubject, contentInHtml, "text/html",pdfstream,attachFiles[0],"application/pdf", uploadTC, uploadTc, "application/pdf", uploadCP, uploadCp, "application/pdf", null, null, null, company.getDisplayNameForCompanyEmailId());
				else
					sdEmail.sendMailWithSendGrid(company.getEmail(), toEmailId, null, null, mailSubject, contentInHtml, "text/html",pdfstream,attachFiles[0],"application/pdf", uploadTC, uploadTc, "application/pdf", uploadCP, uploadCp, "application/pdf", null, null, null, company.getDisplayNameForCompanyEmailId());
				
			}
		
			return;
		}
		
		/**
		 * @author Vijay Chougule Date 28-12-2020
		 * Des :- Email From Sales Person and CC in Approver Name and Branch Email id and Company Email Id
		 * send Emails From SendGrid
		 * 
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.PC_ENABLESENDEMAILSFROMPERSONRESPONSIBLE, company.getCompanyId())) {
			 Company compEntity = ofy().load().type(Company.class)
						.filter("companyId", company.getCompanyId()).first().now();
				ObjectifyService.reset();
			    ofy().consistency(Consistency.STRONG);
				ofy().clear();
				
			 logger.log(Level.SEVERE, "company.getEmail() "+compEntity.getEmail());
				logger.log(Level.SEVERE, "company.getEmail() "+compEntity.getPocName());
				logger.log(Level.SEVERE, "company.getEmail() "+compEntity.getPocEmail());
				logger.log(Level.SEVERE, "company.getEmail() "+compEntity.getCompanyType());

				if(ccemailidlist!=null && company.getEmail()!=null && !company.getEmail().equals("")) {
					logger.log(Level.SEVERE, "company.getEmail() == "+company.getEmail());
					ccemailidlist.add(company.getEmail());
				}
				logger.log(Level.SEVERE, "Email CC "+ccemailidlist);
				logger.log(Level.SEVERE, "Email To "+toEmailId);
				ArrayList<String> bcclist = new ArrayList<String>();
				bcclist.add(fromEmailId);
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			sdEmail.sendMailWithSendGrid(fromEmailId, toEmailId, ccemailidlist, bcclist, mailSubject, contentInHtml, "text/html",pdfstream,attachFiles[0],"application/pdf",null);
			return;
		}
		/**
		 * ends here
		 */
		
		try {
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, company.getCompanyId())){
				logger.log(Level.SEVERE, "Inside Sendgird API wise Email ");

			 try {
					/**
					 * @author Vijay Date :- 09-08-2021
					 * Des :- To send email from sendgrid updated code for MRN MIN and MMN
					 */
					SendGridEmailServlet sdEmail=new SendGridEmailServlet();
					if(mailSubject.equalsIgnoreCase("MRN") || mailSubject.equalsIgnoreCase("MIN") || mailSubject.equalsIgnoreCase("MMN")) {
						if(uploadStream!=null) {
							sdEmail.sendMailWithSendGrid(fromEmailId, toEmailId, ccemailidlist, null, mailSubject, contentInHtml, "text/html",null,null,"application/pdf", null, null, "application/pdf", uploadStream, uploadedFile, "application/pdf", null, null, null, null);
						}
						else {
							sdEmail.sendMailWithSendGrid(fromEmailId, toEmailId, ccemailidlist, null, mailSubject, contentInHtml, "text/html",null,null,"application/pdf", null, null, "application/pdf", null, "", "application/pdf", null, null, null, null);
						}
					}
					else {
//						sdEmail.sendMailWithSendGrid(fromEmailId, toEmailId, ccemailidlist, null, mailSubject, contentInHtml, "text/html",pdfstream,attachFiles[0],"application/pdf", null, null, "application/pdf", uploadStream, "", "application/pdf", null, null, null, null);
						/**
						 * @author Anil @since 19-10-2021
						 * service report were not sent through sendgrid email api
						 */
						if(attachFiles!=null&&attachFiles.length>0)
							sdEmail.sendMailWithSendGrid(fromEmailId, toEmailId, ccemailidlist, null, mailSubject, contentInHtml, "text/html",pdfstream,attachFiles[0],"application/pdf", uploadTC, uploadTc, "application/pdf", uploadCP, uploadCp, "application/pdf", uploadStream, "", "application/pdf", company.getDisplayNameForCompanyEmailId());
						else
							sdEmail.sendMailWithSendGrid(fromEmailId, toEmailId, ccemailidlist, null, mailSubject, contentInHtml, "text/html",pdfstream,null,"application/pdf", uploadTC, uploadTc, "application/pdf", uploadCP, uploadCp, "application/pdf", uploadStream, "", "application/pdf", company.getDisplayNameForCompanyEmailId());
						
					}		

				} catch (Exception e) {
					e.printStackTrace();
				}
			 
			  return;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		 

		
		/************************************* Mail Send Logic *********************************/
		try {
			System.out.println("Inside Try Block");
			Properties props = new Properties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", "smtp.gmail.com");
			Session session = Session.getDefaultInstance(props);
			// Session session=Session.getDefaultInstance(props,null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company.getEmail());
			logger.log(Level.SEVERE, "Company Email" + company.getEmail());

			// To Mail Address
			message.setFrom(new InternetAddress(company.getEmail()));
			message.setReplyTo(new InternetAddress[] { new InternetAddress(
					company.getEmail()) });

			// Email to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);
				logger.log(Level.SEVERE, "Email id multipart "
						+ mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);
			message.setRecipient(Message.RecipientType.CC, new InternetAddress(
					company.getEmail()));

			// Mail Subject
			message.setSubject(mailSubject);

			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentInHtml, "text/html");
			multiPart.addBodyPart(htmlpart);
			
			
			
			/**
			 * Date 2/11/2018
			 * Updated By Viraj
			 * Description Email Attachment of Terms And Conditions, Company Profile
			 */
			if(uploadTC != null) {
				MimeBodyPart attachbodyDoc = new MimeBodyPart();
				logger.log(Level.SEVERE, "Inside Attachment TC upload  .........");
				attachbodyDoc.setFileName(uploadTc);
				DataSource src = new ByteArrayDataSource(
						uploadTC.toByteArray(), "application/pdf");
				attachbodyDoc.setDataHandler(new DataHandler(src));
				multiPart.addBodyPart(attachbodyDoc);
			}
			
			if(uploadCP != null) {
				MimeBodyPart attachbodyDoc = new MimeBodyPart();
				logger.log(Level.SEVERE, "Inside Attachment TC upload  .........");
				attachbodyDoc.setFileName(uploadCp);
				DataSource src = new ByteArrayDataSource(
						uploadCP.toByteArray(), "application/pdf");
				attachbodyDoc.setDataHandler(new DataHandler(src));
				multiPart.addBodyPart(attachbodyDoc);
			}
			
			if(uploadCompanyProfile != null) {
				MimeBodyPart attachbodyDoc = new MimeBodyPart();
				logger.log(Level.SEVERE, "Inside Attachment TC upload  .........");
				attachbodyDoc.setFileName("CompanyProfile" +".pdf");
				DataSource src = new ByteArrayDataSource(
						uploadCompanyProfile.toByteArray(), "application/pdf");
				attachbodyDoc.setDataHandler(new DataHandler(src));
				multiPart.addBodyPart(attachbodyDoc);
			}
			
			
			
			
			
			/** Ends **/

			// Mail Uploaded Document attachment
			if (uploadedFile != null) {
				MimeBodyPart attachbodyDoc = new MimeBodyPart();
				logger.log(Level.SEVERE, "Inside Attachment upload  .........");
				attachbodyDoc.setFileName(uploadedFile);
				DataSource src = new ByteArrayDataSource(
						uploadStream.toByteArray(), "image/jpeg");
				attachbodyDoc.setDataHandler(new DataHandler(src));
				multiPart.addBodyPart(attachbodyDoc);
			}

			// Mail attachment
			if (attachFiles != null) {
				MimeBodyPart attachbody = new MimeBodyPart();
				System.out.println("Inside Attachment.........");
				logger.log(Level.SEVERE, "Inside Attachment.........");
				attachbody.setFileName(attachFiles[0] + ".pdf");
				DataSource src = new ByteArrayDataSource(
						pdfstream.toByteArray(), "application/pdf");
				attachbody.setDataHandler(new DataHandler(src));
				multiPart.addBodyPart(attachbody);
			}

			System.out.println("Ready To Send Mail");
			logger.log(Level.SEVERE, "Ready To Send Mail" + multiPart);
			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			logger.log(Level.SEVERE, "Email sent Successfully !!!!");
			System.out.println("Email sent Successfully !!!!");
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Exception while sending Email" + e);
		}

	}

	/** 31-10-2017 sagar sore 
	 * @param fromEmailId 
	 * @param ccEmailList **/
	public void sendNewEmailForGST(ArrayList<String> toEmailId,
			String mailSubject, String mailTitle, int documentId,
			String documentTitle, Date documnetDate, Company company,
			String heading1, ArrayList<String> table1_Header,
			ArrayList<String> table1, String heading2,
			ArrayList<String> table2_Header, ArrayList<String> table2,
			String noteVal, String mailHeader, String mailFooter,
			String[] attachFiles, String uploadedFile, String customDoc, ArrayList<String> ccEmailList, String fromEmailId)
			throws IOException {

		logger.log(Level.SEVERE, "Entered In Common Method");

		// to Email id's in array
		String[] toEmailIdArray = new String[toEmailId.size()];
		toEmailId.toArray(toEmailIdArray);
		logger.log(Level.SEVERE, "Mail Sub : " + mailSubject);
		logger.log(Level.SEVERE, "Mail Title : " + mailTitle);
		logger.log(Level.SEVERE, "Mail Doc title : " + documentTitle);
		logger.log(Level.SEVERE, "Mail Doc Date : " + fmt.format(documnetDate));
		logger.log(Level.SEVERE, "Mail Header : " + mailHeader);
		logger.log(Level.SEVERE, "Mail Footer : " + mailFooter);

		logger.log(Level.SEVERE, "Mail Sub : " + mailSubject);
		logger.log(Level.SEVERE, "Mail Title : " + mailTitle);
		logger.log(Level.SEVERE, "Mail Doc title : " + documentTitle);
		logger.log(Level.SEVERE, "Mail Doc Date : " + fmt.format(documnetDate));
		logger.log(Level.SEVERE, "Mail Header : " + mailHeader);
		logger.log(Level.SEVERE, "Mail Footer : " + mailFooter);

		StringBuilder builder = new StringBuilder();

		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("<h3 class=\"ex\" align=\"center\">" + mailTitle
				+ "</h3></br></br>");
		builder.append("<B>Document Id :</B>" + documentId
				+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
				+ "<B>Document Title :</B>" + documentTitle
				+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
				+ "<B>Document Date :</B>" + fmt.format(documnetDate)
				+ "</h3></br></br>");

		// Mail header
		if (mailHeader != null) {
			builder.append("</br></br>");
			builder.append("<p>" + mailHeader + "</p>" + "</br></br>");
		}

		/************************ Table 1 ***************************************/

		if (table1 != null && !table1.isEmpty()) {
			logger.log(Level.SEVERE, "Table  1");
			builder.append("<h2>" + heading1 + "</h2>");

			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
			builder.append("<tr>");

			// Product Table1 heading
			for (String header : table1_Header)
				builder.append("<th>" + header + "</th>");
			builder.append("</tr>");

			logger.log(Level.SEVERE, "Size of table 1 " + table1.size());

			builder.append("<tr>");
			for (int i = 0; i < table1.size(); i++) {
				builder.append("<td>" + table1.get(i) + "</td>");
				logger.log(Level.SEVERE, "Table Started 1" + table1.get(i));
				if (i > 0 && (i + 1) % table1_Header.size() == 0) {
					builder.append("</tr><tr>");
				}
			}
			builder.append(" </tr>  ");
			builder.append("</table></b></br>");
		}

		/************************************* Table 2 ************************************/

		if (table2 != null && !table2.isEmpty()) {
			logger.log(Level.SEVERE, "Table 2");
			builder.append("<h2>" + heading2 + "</h2>");

			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"1\"  Style width=\"50%\" >");
			builder.append("<col width=\"1%\" >");
			builder.append("<col width=\"2%\" >");
			builder.append("<col width=\"3%\" >");
			builder.append("<col width=\"10%\">");

			builder.append("<tr>");
			// Product Table2 heading
			for (String header : table2_Header)
				builder.append("<th>" + header + "</th>");

			builder.append("</tr>");

			logger.log(Level.SEVERE, "Size of list " + table2.size());
			builder.append("<tr>");
			for (int i = 0; i < table2.size(); i++) {
				builder.append("<td style=\"width:5%\">" + table2.get(i)
						+ "</td>");
				logger.log(Level.SEVERE, "Table Started 2 :" + table2.get(i));
				if (i > 0 && (i + 1) % table2_Header.size() == 0) {
					builder.append("</tr><tr>");
				}
			}
			builder.append(" </tr>  ");
			builder.append("</table></b></br>");
		}

		/************************************ Note *******************************************/
		if (noteVal != null) {
			builder.append("</br></br><p>" + noteVal + "</p></br></br>");
		}

		/**************************** FOOTER Details *********************************/
		if (mailFooter != null) {
			builder.append("<p>" + mailFooter + "</p></br></b></br>");
		}

		// Company Details
		builder.append("<br>");
		builder.append("</br>");
		builder.append("<b>" + company.getBusinessUnitName() + "</b></br>");
		builder.append("<table>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Address : ");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getAddrLine2().equals("")
				&& company.getAddress().getAddrLine2() != null)
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getAddrLine2() + " "
					+ company.getAddress().getLocality());
		else
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getLocality());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getLandmark().equals("")
				&& company.getAddress().getLandmark() != null)
			builder.append(company.getAddress().getLandmark() + ","
					+ company.getAddress().getCity() + ","
					+ company.getAddress().getPin()
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		else
			builder.append(company.getAddress().getCity() + ","
					+ company.getAddress().getPin() + ","
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		builder.append("</td>");
		builder.append("</tr>");
		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Website : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getWebsite());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Phone No : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getCellNumber1());
		builder.append("</td>");
		builder.append("<tr>");

		builder.append("</tr>");
		builder.append("</table>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		logger.log(Level.SEVERE, contentInHtml);

		/**
		 * @author Vijay Chougule Date 28-12-2020
		 * Des :- Email From Sales Person and CC in Approver Name and Branch Email id and Company Email Id
		 * send Emails From SendGrid
		 * 
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.PC_ENABLESENDEMAILSFROMPERSONRESPONSIBLE, company.getCompanyId())) {
			 Company compEntity = ofy().load().type(Company.class).filter("companyId", company.getCompanyId()).first().now();
				logger.log(Level.SEVERE, "company.getEmail() "+compEntity.getEmail());
				if(compEntity.getEmail()!=null && !compEntity.getEmail().equals("")) {
					logger.log(Level.SEVERE, "company.getEmail() == "+compEntity.getEmail());
					ccEmailList.add(compEntity.getEmail());
				}
				logger.log(Level.SEVERE, "Email CC "+ccEmailList);
				logger.log(Level.SEVERE, "Email To "+toEmailId);
				
				ArrayList<String> bcclist = new ArrayList<String>();
				bcclist.add(fromEmailId);
				
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			sdEmail.sendMailWithSendGrid(fromEmailId, toEmailId, ccEmailList, bcclist, mailSubject, contentInHtml, "text/html",pdfstream,attachFiles[0],"application/pdf",null);
			return;
		}
		/**
		 * ends here
		 */
		
		/************************************* Mail Send Logic *********************************/
		try {
			logger.log(Level.SEVERE, "Inside Try Block");
			Properties props = new Properties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", "smtp.gmail.com");
			Session session = Session.getDefaultInstance(props);
			// Session session=Session.getDefaultInstance(props,null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			logger.log(Level.SEVERE, "Company Email: " + company.getEmail());
			logger.log(Level.SEVERE, "Company Email" + company.getEmail());

			// To Mail Address
			message.setFrom(new InternetAddress(company.getEmail()));
			message.setReplyTo(new InternetAddress[] { new InternetAddress(
					company.getEmail()) });

			// Email to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				logger.log(Level.SEVERE, "Email id multipart "
						+ mailToMultiple[i]);
				logger.log(Level.SEVERE, "Email id multipart "
						+ mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);
			message.setRecipient(Message.RecipientType.CC, new InternetAddress(
					company.getEmail()));

			// Mail Subject
			message.setSubject(mailSubject);

			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentInHtml, "text/html");
			multiPart.addBodyPart(htmlpart);

			logger.log(Level.SEVERE, "uploaded file name (t & c) "
					+ uploadedFile + " attachFiles "
					+ ((attachFiles != null) ? attachFiles[0] : "no files")
					+ " custom doc " + customDoc);

			logger.log(Level.SEVERE, "except service quotation mail");
			// Mail Uploaded Document attachment
			if (uploadedFile != null) {
				MimeBodyPart attachbodyDoc = new MimeBodyPart();
				logger.log(Level.SEVERE, "Inside Attachment upload  .........");
				attachbodyDoc.setFileName(uploadedFile);
				DataSource src = new ByteArrayDataSource(
						uploadStream.toByteArray(), "image/jpeg");
				attachbodyDoc.setDataHandler(new DataHandler(src));
				multiPart.addBodyPart(attachbodyDoc);
			}

			// Mail attachment
			if (attachFiles != null) {
				MimeBodyPart attachbody = new MimeBodyPart();
				System.out.println("Inside Attachment.........");
				logger.log(Level.SEVERE,
						"Inside Attachment......... File name "
								+ attachFiles[0]);
				attachbody.setFileName(attachFiles[0] + ".pdf");
				logger.log(Level.SEVERE, "Sending stream");
				DataSource src = new ByteArrayDataSource(
						pdfstream.toByteArray(), "application/pdf");
				logger.log(Level.SEVERE, "attaching mail body");
				attachbody.setDataHandler(new DataHandler(src));
				multiPart.addBodyPart(attachbody);

			}
			if (customDoc != null) {
				MimeBodyPart attachbody = new MimeBodyPart();
				logger.log(Level.SEVERE, "Inside Attachment.........");
				logger.log(Level.SEVERE, "Inside Attachment.........");
				attachbody.setFileName(customDoc);
				logger.log(Level.SEVERE, "Inside Attachment.........1");
				DataSource src = new ByteArrayDataSource(
						customQuotUploadStream.toByteArray(), "application/pdf");
				logger.log(Level.SEVERE, "Inside Attachment.........2");
				attachbody.setDataHandler(new DataHandler(src));
				logger.log(Level.SEVERE, "Inside Attachment.........3");
				multiPart.addBodyPart(attachbody);

			}

			logger.log(Level.SEVERE, "Ready To Send Mail");
			logger.log(Level.SEVERE, "Ready To Send Mail" + multiPart);
			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			logger.log(Level.SEVERE, "Email sent Successfully !!!!");
			logger.log(Level.SEVERE, "Email sent Successfully !!!!");
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Exception while sending Email" + e);
		}

	}

	/***/

	public void sendSupportEmail(String subject, String contentbody,
			String from, String to, String urlFile, String fileName) {
		try {
			System.out.println("Inside Try Block");
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			// To Mail Address
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					to));
			// Mail Subject
			message.setSubject(subject);

			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentbody, "text/html");
			multiPart.addBodyPart(htmlpart);
			// File fileAttach=new File(urlFile);
			// MimeBodyPart attachbody=new MimeBodyPart();
			// attachbody.setFileName(fileName);
			// DataSource source = new FileDataSource(fileAttach);
			// attachbody.setDataHandler(new DataHandler(source));
			// multiPart.addBodyPart(attachbody);

			MimeBodyPart attachbody = new MimeBodyPart();
			attachbody.setFileName(fileName);
			// DataSource source = new FileDataSource(fileAttach);
			DataSource source = new ByteArrayDataSource(
					pdfstream.toByteArray(), "image/jpeg");
			attachbody.setDataHandler(new DataHandler(source));
			multiPart.addBodyPart(attachbody);

			logger.log(Level.SEVERE, "Attach Body to multipart :::: "
					+ multiPart);

			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			System.out.println("Email sent Successfully !!!!");
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Rohan Inside Catch " + e);
			e.printStackTrace();
		}
	}

	/******************************************** Common Email Method End *********************************************/

	// *****************************for email on request for
	// approval*********************************************

	public void sendEmailOnRequestForApproval(String mailSub, int docId,
			String docType, Date approveDate, String remark, String message1,
			String company, ArrayList<String> toEmailList,
			ArrayList<String> ccEmailList) throws IOException {
		
		
		/**
		 * @author Anil , Date : 27-03-2020
		 * If send grid account is active then approval mail will go from send gris mail api
		 * requirement raised by NBHC,Nitin Sir
		 */
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Approvals", "EnableSendGridEmailApi",comp.getCompanyId())){
			logger.log(Level.SEVERE," company email --" +company +"  to email  " + toEmailList +"EnableSendGridEmailApi");
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			sdEmail.sendMailWithSendGrid(company, toEmailList, ccEmailList, null, mailSub, message1, "text/html",null,null,null,comp.getDisplayNameForCompanyEmailId());
			
			return;
		}
		logger.log(Level.SEVERE,"JAVAx mail API..." );
		/**
		 * 
		 */

		// ***************************************for request for
		// approval*********************

		for (int i = 0; i < toEmailList.size(); i++) {
			for (int j = 0; j < ccEmailList.size(); j++) {
				if (toEmailList.get(i).contains(ccEmailList.get(j))) {
					ccEmailList.remove(j);
				}
			}
		}

		String[] toEmailIdArray = new String[toEmailList.size()];
		toEmailList.toArray(toEmailIdArray);

		String[] ccEmailIdArray = new String[ccEmailList.size()];
		ccEmailList.toArray(ccEmailIdArray);

		try {
			System.out.println("Inside Try Block");
			Properties props = new Properties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", "smtp.gmail.com");
			Session session = Session.getDefaultInstance(props);
			// Session session=Session.getDefaultInstance(props,null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company);

			// To Mail Address
			/**
			 * @author Anil , Date : 10-04-2020
			 * added display name for from email id
			 * ,company.getDisplayNameForCompanyEmailId()
			 */
			if(comp.getDisplayNameForCompanyEmailId()!=null&&!comp.getDisplayNameForCompanyEmailId().equals("")){
				message.setFrom(new InternetAddress(company,comp.getDisplayNameForCompanyEmailId()));
			}else{
				message.setFrom(new InternetAddress(company));
			}
			
//			message.setFrom(new InternetAddress(company));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					company));

			// Email To to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);
				logger.log(Level.SEVERE, "To Email id multipart "
						+ mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);

			// Email CC to Multiple Recipients
			InternetAddress[] ccmailToMultiple = new InternetAddress[ccEmailIdArray.length];
			for (int i = 0; i < ccEmailIdArray.length; i++) {
				ccmailToMultiple[i] = new InternetAddress(ccEmailIdArray[i]);
				System.out.println("Email id multipart " + ccmailToMultiple[i]);
				logger.log(Level.SEVERE, "CC Email id multipart "
						+ ccmailToMultiple[i]);
			}
			logger.log(Level.SEVERE, "hi vijay 1");
			message.setRecipients(Message.RecipientType.CC, ccmailToMultiple);
			logger.log(Level.SEVERE, "hi vijay 2");
			// below line for add BCC if needed then uncomment
			// message.setRecipient(Message.RecipientType.BCC, new
			// InternetAddress(approverEmail));

			// Mail Subject
			logger.log(Level.SEVERE, "hi vijay 3");
			message.setSubject(mailSub);
			logger.log(Level.SEVERE, "hi vijay 4");
			message.setText(message1);
			logger.log(Level.SEVERE, "hi vijay 5");
			// // Mail Body Content In html Form
			// MimeBodyPart htmlpart=new MimeBodyPart();
			// htmlpart.setContent(message1, "text");
			// multiPart.addBodyPart(htmlpart);

			System.out.println("Ready To Send Mail");
			logger.log(Level.SEVERE, "hi vijay 6 ready to send email");
			Transport.send(message);
			System.out.println("Email sent Successfully !!!!");
			logger.log(Level.SEVERE, "Email sent Successfully !!!!");
		} catch (Exception e) {
			logger.log(Level.SEVERE, "excption asfollows " + e);
			e.printStackTrace();

		}

	}

	// ******************************************************************************************

	/********************************** Cron Job Email Implementation ************************************/

	public void cronSendEmail(ArrayList<String> toEmailId, String mailSubject,
			String mailTitle, Company company, ArrayList<String> table1_Header,
			ArrayList<String> table1, String heading2,
			ArrayList<String> table2_Header, ArrayList<String> table2,
			String[] attachFiles) throws IOException {

		logger.log(Level.SEVERE, "Entered In Common Method");

		String[] toEmailIdArray = new String[toEmailId.size()];
		toEmailId.toArray(toEmailIdArray);

		StringBuilder builder = new StringBuilder();

		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("<h3 class=\"ex\" align=\"left\">" + mailTitle
				+ "</h3></br></br>");

		/************************ Table 1 ***************************************/

		if (table1 != null && !table1.isEmpty()) {
			System.out.println("Table  1");
			// builder.append("<h2>"+heading1+"</h2>");

			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
			builder.append("<tr>");

			// Product Table1 heading
			for (String header : table1_Header)
				builder.append("<th>" + header + "</th>");
			builder.append("</tr>");

			System.out.println("Size of table 1 " + table1.size());

			builder.append("<tr>");
			for (int i = 0; i < table1.size(); i++) {
				builder.append("<td>" + table1.get(i) + "</td>");
				System.out.println("Table Started 1" + table1.get(i));
				if (i > 0 && (i + 1) % table1_Header.size() == 0) {
					builder.append("</tr><tr>");

				}
			}
			builder.append(" </tr>  ");
			builder.append("</table></b></br>");
		}

		/************************************* Table 2 ************************************/

		if (table2 != null && !table2.isEmpty()) {
			System.out.println("Table 2");
			builder.append("<h2>" + heading2 + "</h2>");

			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"1\"  Style width=\"50%\" >");
			builder.append("<col width=\"1%\" >");
			builder.append("<col width=\"2%\" >");
			builder.append("<col width=\"3%\" >");
			builder.append("<col width=\"10%\">");

			builder.append("<tr>");
			// Product Table2 heading
			for (String header : table2_Header)
				builder.append("<th>" + header + "</th>");

			builder.append("</tr>");

			System.out.println("Size of list " + table2.size());
			builder.append("<tr>");
			for (int i = 0; i < table2.size(); i++) {
				builder.append("<td style=\"width:5%\">" + table2.get(i)
						+ "</td>");
				// if(i==6){
				// builder.append("<td style=\"width:20%\">" + table2.get(i) +
				// "</td>");
				// }
				System.out.println("Table Started 2 :" + table2.get(i));
			}
			builder.append(" </tr>  ");
			builder.append("</table></b></br>");
		}

		/**************************** FOOTER Details *********************************/
		// if(mailFooter!=null){
		// builder.append("<p>"+mailFooter+"</p></br></b></br>");
		// }

		// Company Details
		builder.append("<br>");
		builder.append("<b>" + company.getBusinessUnitName() + "</b></br>");
		builder.append("<table>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Address : ");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getAddrLine2().equals("")
				&& company.getAddress().getAddrLine2() != null)
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getAddrLine2() + " "
					+ company.getAddress().getLocality());
		else
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getLocality());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getLandmark().equals("")
				&& company.getAddress().getLandmark() != null)
			builder.append(company.getAddress().getLandmark() + ","
					+ company.getAddress().getCity() + ","
					+ company.getAddress().getPin()
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		else
			builder.append(company.getAddress().getCity() + ","
					+ company.getAddress().getPin() + ","
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Website : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getWebsite());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Phone No : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getCellNumber1());
		builder.append("</td>");
		builder.append("<tr>");

		builder.append("</tr>");
		builder.append("</table>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CronJob", "EnableSendGridEmailApi",company.getCompanyId())){
			logger.log(Level.SEVERE," company email --" +company.getEmail() +"  to email  " + toEmailIdArray.toString());
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			sdEmail.sendMailWithSendGrid(company.getEmail(), toEmailId, null, null, mailSubject, contentInHtml, "text/html",null,null,null,company.getDisplayNameForCompanyEmailId());
			
			return;
		}
		/************************************* Mail Send Logic *********************************/
		try {
			// System.out.println("Inside Try Block");
			// Properties props=new Properties();
			// Session session=Session.getDefaultInstance(props,null);

			System.out.println("Inside Try Block");
			Properties props = new Properties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", "smtp.gmail.com");
			Session session = Session.getDefaultInstance(props);

			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company.getEmail());

			// To Mail Address
			message.setFrom(new InternetAddress(company.getEmail()));
			// message.setFrom(new
			// InternetAddress("evasoftwaresolutionserp@gmail.com"));
			message.setReplyTo(new InternetAddress[] { new InternetAddress(
					company.getEmail()) });

			// logger.log(Level.SEVERE,"To Email Address"+company.getEmail());

			// Email to Multiple Recipients
			if(toEmailIdArray!=null&&toEmailIdArray.length>0) {
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);

				logger.log(Level.SEVERE, "To Email Address" + mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);
			// message.setRecipient(Message.RecipientType.CC, new
			// InternetAddress(company.getEmail()));

			// logger.log(Level.SEVERE,"To Email Address"+company.getEmail());

			// Mail Subject
			message.setSubject(mailSubject);

			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentInHtml, "text/html");
			multiPart.addBodyPart(htmlpart);

			// Mail attachment
			// if(attachFiles!=null){
			// System.out.println("Inside Attachment.........");
			// MimeBodyPart attachbody=new MimeBodyPart();
			//
			// attachbody.setFileName(attachFiles[0]+".pdf");
			// DataSource src=new ByteArrayDataSource(pdfstream.toByteArray(),
			// "application/pdf");
			// attachbody.setDataHandler(new DataHandler(src));
			// System.out.println("Attach File Name");
			// multiPart.addBodyPart(attachbody);

			// adds attachments
			// if (attachFiles != null && attachFiles.length > 0) {
			// System.out.println("More");
			// for (String filePath : attachFiles) {
			// MimeBodyPart attachPart = new MimeBodyPart();
			// try {
			// attachPart.attachFile(filePath);
			// } catch (IOException ex) {
			// ex.printStackTrace();
			// }
			// multiPart.addBodyPart(attachPart);
			// }
			// }
			// }

			System.out.println("Ready To Send Mail");

			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			System.out.println("Email sent Successfully !!!!");
			logger.log(Level.SEVERE, "Email sent Successfully !!");
		}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/******************************* Interaction Send Email Code ************************************/

	public void sendInteractionEmail(ArrayList<String> toEmail,
			String mailSubject, String mailTitle, int documentId,
			String documentTitle, Date documnetDate, Company company,
			String msgBody) {

		// to Email id's in array
		String[] toEmailIdArray = new String[toEmail.size()];
		toEmail.toArray(toEmailIdArray);

		System.out.println("Mail Sub : " + mailSubject);
		System.out.println("Mail Title : " + mailTitle);
		System.out.println("Mail Doc title : " + documentTitle);
		System.out.println("Mail Doc Date : " + fmt.format(documnetDate));

		StringBuilder builder = new StringBuilder();

		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("<h3 class=\"ex\" align=\"center\">" + mailTitle
				+ "</h3></br></br>");
		builder.append("<B>Document Id :</B>" + documentId
				+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
				+ "<B>Document Title :</B>" + documentTitle
				+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
				+ "<B>Document Date :</B>" + fmt.format(documnetDate)
				+ "</h3></br></br>");

		builder.append(msgBody);

		// Company Details
		builder.append("<b>" + company.getBusinessUnitName() + "</b></br>");
		builder.append("<table>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Address : ");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getAddrLine2().equals("")
				&& company.getAddress().getAddrLine2() != null)
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getAddrLine2() + " "
					+ company.getAddress().getLocality());
		else
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getLocality());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getLandmark().equals("")
				&& company.getAddress().getLandmark() != null)
			builder.append(company.getAddress().getLandmark() + ","
					+ company.getAddress().getCity() + ","
					+ company.getAddress().getPin()
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		else
			builder.append(company.getAddress().getCity() + ","
					+ company.getAddress().getPin() + ","
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Website : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getWebsite());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Phone No : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getCellNumber1());
		builder.append("</td>");
		builder.append("<tr>");

		builder.append("</tr>");
		builder.append("</table>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);

		try {
			System.out.println("Inside Try Block");
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company.getEmail());

			// To Mail Address
			message.setFrom(new InternetAddress(company.getEmail()));
			message.setReplyTo(new InternetAddress[] { new InternetAddress(
					company.getEmail()) });

			// Email to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);
			message.setRecipient(Message.RecipientType.CC, new InternetAddress(
					company.getEmail()));

			// Mail Subject
			message.setSubject(mailSubject);

			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentInHtml, "text/html");
			multiPart.addBodyPart(htmlpart);

			// // Mail attachment
			// if(attachFiles!=null){
			// System.out.println("Inside Attachment.........");
			//
			// MimeBodyPart attachbody=new MimeBodyPart();
			// attachbody.setFileName(attachFiles[0]+".pdf");
			// DataSource src=new ByteArrayDataSource(pdfstream.toByteArray(),
			// "application/pdf");
			// attachbody.setDataHandler(new DataHandler(src));
			// multiPart.addBodyPart(attachbody);
			// }

			System.out.println("Ready To Send Mail");

			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			System.out.println("Email sent Successfully !!!!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/********************************* Send Email For Inventory Cron Job ****************************/

	public void cronInventorySendEmail(ArrayList<String> toEmailId,
			String mailSubject, String mailTitle, Company company,
			ArrayList<String> table1_Header, ArrayList<String> table1,
			String mailTitle2, String heading2,
			ArrayList<String> table2_Header, ArrayList<String> table2,
			String[] attachFiles) throws IOException {

		logger.log(Level.SEVERE, "Entered In Common Method");

		String[] toEmailIdArray = new String[toEmailId.size()];
		toEmailId.toArray(toEmailIdArray);

		StringBuilder builder = new StringBuilder();

		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("<h3 class=\"ex\" align=\"left\">" + mailTitle
				+ "</h3></br></br>");

		/************************ Table 1 ***************************************/

		if (table1 != null && !table1.isEmpty()) {
			System.out.println("Table  1");
			// builder.append("<h2>"+heading1+"</h2>");

			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
			builder.append("<tr>");

			// Product Table1 heading
			for (String header : table1_Header)
				builder.append("<th>" + header + "</th>");
			builder.append("</tr>");

			System.out.println("Size of table 1 " + table1.size());

			builder.append("<tr>");
			for (int i = 0; i < table1.size(); i++) {
				builder.append("<td>" + table1.get(i) + "</td>");
				System.out.println("Table Started 1" + table1.get(i));
				if (i > 0 && (i + 1) % table1_Header.size() == 0) {
					builder.append("</tr><tr>");

				}
			}
			builder.append(" </tr>  ");
			builder.append("</table></b></br>");
		}

		/*********************     ***************************/
		logger.log(Level.SEVERE, "Entered In Common Method  mailTitle2 ");
		if (mailTitle2 != null) {
			builder.append("<h3 class=\"ex\" align=\"left\">" + mailTitle2
					+ "</h3></br></br>");
		}
		if (table2 != null && !table2.isEmpty()) {

			logger.log(
					Level.SEVERE,
					"Entered In Common Method  mailTitle2 ============= table2!=null &&!table2.isEmpty() ");

			System.out.println("Table  2");
			// builder.append("<h2>"+heading1+"</h2>");

			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
			builder.append("<tr>");

			// Product Table1 heading
			for (String header : table2_Header)
				builder.append("<th>" + header + "</th>");
			builder.append("</tr>");

			System.out.println("Size of table 2 " + table2.size());
			logger.log(Level.SEVERE, "Size of table 2 :" + table2.size());

			builder.append("<tr>");
			for (int i = 0; i < table2.size(); i++) {
				builder.append("<td>" + table2.get(i) + "</td>");
				System.out.println("Table Started 1" + table2.get(i));
				if (i > 0 && (i + 1) % table2_Header.size() == 0) {
					builder.append("</tr><tr>");

				}
			}
			builder.append(" </tr>  ");
			builder.append("</table></b></br>");
		}

		/************************************* Table 2 ************************************/
		builder.append("<br>");
		builder.append("<br>");
		builder.append("<b>" + company.getBusinessUnitName() + "</b></br>");
		builder.append("<table>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Address : ");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getAddrLine2().equals("")
				&& company.getAddress().getAddrLine2() != null)
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getAddrLine2() + " "
					+ company.getAddress().getLocality());
		else
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getLocality());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getLandmark().equals("")
				&& company.getAddress().getLandmark() != null)
			builder.append(company.getAddress().getLandmark() + ","
					+ company.getAddress().getCity() + ","
					+ company.getAddress().getPin()
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		else
			builder.append(company.getAddress().getCity() + ","
					+ company.getAddress().getPin() + ","
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Website : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getWebsite());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Phone No : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getCellNumber1());
		builder.append("</td>");
		builder.append("<tr>");

		builder.append("</tr>");
		builder.append("</table>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);
		
		/**
		 * @author Anil,Date : 09-02-2019
		 * If Process Type - EnableSendGridEmailApi is active then we will send email through SendGrid else through default mail i.r. JavaMail
		 * As appengine has limited daily mail quota i.e. 100, to overcome this limited quota issue we are implementing SendGrid
		 * raised By Rohan for Pecopp
		 */
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CronJob", "EnableSendGridEmailApi",company.getCompanyId())){
			logger.log(Level.SEVERE," company email --" +company.getEmail() +"  to email  " + toEmailIdArray.toString());
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			sdEmail.sendMailWithSendGrid(company.getEmail(), toEmailId, null, null, mailSubject, contentInHtml, "text/html",null,null,null,company.getDisplayNameForCompanyEmailId());
			
			return;
		}

		/************************************* Mail Send Logic *********************************/
		try {
			System.out.println("Inside Try Block");
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company.getEmail());

			// To Mail Address
			message.setFrom(new InternetAddress(company.getEmail()));
			message.setReplyTo(new InternetAddress[] { new InternetAddress(
					company.getEmail()) });

			// logger.log(Level.SEVERE,"To Email Address"+company.getEmail());

			// Email to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);

				logger.log(Level.SEVERE, "To Email Address" + mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);
			// message.setRecipient(Message.RecipientType.CC, new
			// InternetAddress(company.getEmail()));

			// logger.log(Level.SEVERE,"To Email Address"+company.getEmail());

			// Mail Subject
			message.setSubject(mailSubject);

			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentInHtml, "text/html");
			multiPart.addBodyPart(htmlpart);

			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			System.out.println("Email sent Successfully !!!!");
			logger.log(Level.SEVERE, "Email sent Successfully !!");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// ********************rohan added here for customer compaign
	// *******************

	public void sendEmailToCustomerList(
			ArrayList<CustomerTrainingAttendies> custList, String mailSub,
			String message1, String programme, String message2,
			String message3, String company) throws IOException {

		ArrayList<String> emailIdsLis = new ArrayList<String>();
		for (int d = 0; d < custList.size(); d++) {
			emailIdsLis.add(custList.get(d).getCustEmail());
		}

		String[] toEmailIdArray = new String[emailIdsLis.size()];
		emailIdsLis.toArray(toEmailIdArray);

		System.out.println("in side send email====" + programme);
		System.out.println("message1====" + message1);
		System.out.println("message2 date====" + message2);
		System.out.println("message3 time ======" + message3);

		// ***************************************for request for
		// approval*********************

		try {
			System.out.println("Inside Try Block");
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company);

			// To Mail Address
			message.setFrom(new InternetAddress(company));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					company));

			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);

				// logger.log(Level.SEVERE,"To Email Address"+company.getEmail());
			}
			message.setRecipients(Message.RecipientType.BCC, mailToMultiple);

			// Mail Subject
			message.setSubject(mailSub);
			// message.setText();
			// message.setText(+message2);
			message.setText(message1 + programme + message2 + message3);
			// // Mail Body Content In html Form
			// MimeBodyPart htmlpart=new MimeBodyPart();
			// htmlpart.setContent(message1, "text");
			// multiPart.addBodyPart(htmlpart);

			System.out.println("Ready To Send Mail");

			Transport.send(message);
			System.out.println("Email sent Successfully !!!!");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// *******************************changes ends here
	// ******************************

	public void sendEmailForIPAddressAuthorization(String mailSub,
			String message1, String company) throws IOException {

		// ***************************************for request for
		// approval*********************

		try {
			System.out.println("Inside Try Block");
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company);

			// To Mail Address
			message.setFrom(new InternetAddress(company));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					company));

			// Mail Subject
			message.setSubject(mailSub);
			message.setText(message1);
			// // Mail Body Content In html Form
			// MimeBodyPart htmlpart=new MimeBodyPart();
			// htmlpart.setContent(message1, "text");
			// multiPart.addBodyPart(htmlpart);

			System.out.println("Ready To Send Mail");

			Transport.send(message);
			System.out.println("Email sent Successfully !!!!");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// ////////////////////////////CONTRACT RENEWAL EMAIL
	// /////////////////////////////////////////

	@Override
	public void initiateContractRenewalEmail(ContractRenewal conRenList)
			throws IllegalArgumentException {
		setContractRenewal(conRenList);

		initializeContractRenewal();
	}

	private void setContractRenewal(ContractRenewal conRenList) {
		conRenewal = conRenList;
	}

	private void initializeContractRenewal() {
		if (conRenewal.getCompanyId() != null) {
			comp = ofy().load().type(Company.class)
					.filter("companyId", conRenewal.getCompanyId()).first()
					.now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		if (conRenewal.getCompanyId() != null) {
			c = ofy().load().type(Customer.class)
					.filter("count", conRenewal.getCustomerId())
					.filter("companyId", conRenewal.getCompanyId()).first()
					.now();
		} else {
			c = ofy().load().type(Customer.class)
					.filter("count", conRenewal.getCustomerId()).first().now();
		}

		if (conRenewal.getCompanyId() != null) {
			servcont = ofy().load().type(Contract.class)
					.filter("count", conRenewal.getContractId())
					.filter("companyId", conRenewal.getCompanyId()).first()
					.now();
		} else {
			servcont = ofy().load().type(Contract.class)
					.filter("count", conRenewal.getContractId()).first().now();
		}

		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add(c.getEmail() + "");

		String msgBody = "</br><p> Dear "
				+ c.getFullname().trim()
				+ ",</p></br>"
				+ "<p>It has been our privilege to have served you over the past year and we truly appreciated & value our association with you. we trust you have found our services exemplery & to your complete satifaction. Your current pest management contract expires on <b>"
				+ fmt.format(servcont.getEndDate())
				+ "</b> and we request you to renew the same.</p></br></br>";

		String mailSubject = "Subject : Renewal of Contract Id "
				+ conRenewal.getContractId() + " end dated "
				+ fmt.format(servcont.getEndDate()) + "." + "</br></br>";

		String footerMsg = "";
		boolean flag = false;
		for (int i = 0; i < conRenewal.getItems().size(); i++) {
			if (conRenewal.getItems().get(i).getPrice() > conRenewal.getItems()
					.get(i).getOldProductPrice()) {
				flag = true;
				break;
			}
		}
		if (flag == true) {
			footerMsg = "<p>Kindly note that we have been compelled to increase our annual services charges due to increase in our operational cost. we reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon. Should you require further information on above.Please do give us a call and we would gladly assist you.<p></br></br> ";
		}

		// Table header 1
		ArrayList<String> tbl1_header = new ArrayList<String>();
		tbl1_header.add("CODE");
		tbl1_header.add("NAME");
		tbl1_header.add("DURATION");
		tbl1_header.add("SERVICES");
		tbl1_header.add("PRICE");
//		tbl1_header.add("S.TAX");
//		tbl1_header.add("VAT");
		
		tbl1_header.add("CGST/SGST");
		System.out.println("Tbl1 header Size :" + tbl1_header.size());

		List<SalesLineItem> productList = new ArrayList<SalesLineItem>();
		productList = conRenewal.getItems();
		// Table 1
		/**
		 * @author Anil
		 * @since 21-05-2020
		 */
		double consolidatedPrice=0;
		if(servcont.isConsolidatePrice()){
			for (int i = 0; i < productList.size(); i++) {
				consolidatedPrice=consolidatedPrice+productList.get(i).getPrice();
			}
		}
		ArrayList<String> tbl1 = new ArrayList<String>();
		for (int i = 0; i < productList.size(); i++) {
			tbl1.add(productList.get(i).getProductCode());
			tbl1.add(productList.get(i).getProductName());
			tbl1.add(productList.get(i).getDuration() + "");
			tbl1.add(productList.get(i).getNumberOfServices() + "");
//			tbl1.add(productList.get(i).getPrice() + "");
			if(servcont.isConsolidatePrice()) {
				tbl1.add(consolidatedPrice + "");
			}else {
				tbl1.add(productList.get(i).getPrice() + "");
			}
//			tbl1.add(productList.get(i).getServiceTax() + "");
//			tbl1.add(productList.get(i).getVatTax() + "");
			
			tbl1.add(productList.get(i).getServiceTax() + "/"+productList.get(i).getVatTax());
		}
		System.out.println("Tbl1 Size :" + tbl1.size());
		
		/***
		 * consolidate invoice details to be set in social info of company object
		 */
		
		if(servcont.isConsolidatePrice()){
			SocialInformation socialInfo=new SocialInformation();
			socialInfo.setFaceBookId("Consolidated Contract Renewal");
			socialInfo.setTwitterId(conRenewal.getItems().size()+"");
			socialInfo.setGooglePlusId(4+"");
			comp.setSocialInfo(socialInfo);
		}else{
			logger.log(Level.SEVERE, "Consolidated Contract Renewal false");
			comp.setSocialInfo(null);
		}

		createContractRenewalAttachment();
		String attachFile = "RenewalLetter";
		sendContractRenewalEmail(toEmailList, "Contract Renewal",
				"Contract Renewal", conRenewal.getContractId(),
				"Contract Renewal", servcont.getEndDate(), comp, mailSubject,
				msgBody, footerMsg, attachFile, tbl1_header, tbl1);

	}

	public void createContractRenewalAttachment() {
		corewal = new ContractRenewalPdf();
		corewal.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(corewal.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		corewal.document.open();

		corewal.createPdfAttachment(servcont, comp, conRenewal, c);
		corewal.document.close();
	}

	/******************************* Contract Renewal Email Code ************************************/

	public void sendContractRenewalEmail(ArrayList<String> toEmail,
			String mailSubject, String mailTitle, int documentId,
			String documentTitle, Date documnetDate, Company company,
			String msgSub, String msgBody, String footerMsg,
			String attachFiles, ArrayList<String> table1_Header,
			ArrayList<String> table1) {

		// to Email id's in array
		String[] toEmailIdArray = new String[toEmail.size()];
		toEmail.toArray(toEmailIdArray);

		System.out.println("Mail Sub : " + mailSubject);
		System.out.println("Mail Title : " + mailTitle);
		System.out.println("Mail Doc title : " + documentTitle);
		System.out.println("Mail Doc Date : " + fmt.format(documnetDate));

		StringBuilder builder = new StringBuilder();

		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("<h3 class=\"ex\" align=\"center\">" + mailTitle
				+ "</h3></br></br>");
		builder.append("<B>Contract Id :</B>" + documentId
				+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "<B>Title :</B>"
				+ documentTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
				+ "<B>Contract Date : </B>" + fmt.format(documnetDate)
				+ "</br></br>");

		builder.append(" <p>  </p> </br></br>");

		/************************ Table 1 ***************************************/

//		if (table1 != null && !table1.isEmpty()) {
//			System.out.println("Table  1");
//			// builder.append("<h2>"+heading1+"</h2>");
//
//			builder.append("</br></br></br><table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
//			builder.append("<tr>");
//
//			// Product Table1 heading
//			for (String header : table1_Header)
//				builder.append("<th>" + header + "</th>");
//			builder.append("</tr>");
//
//			System.out.println("Size of table 1 " + table1.size());
//
//			builder.append("<tr>");
//			for (int i = 0; i < table1.size(); i++) {
//				builder.append("<td>" + table1.get(i) + "</td>");
//				System.out.println("Table Started 1" + table1.get(i));
//				if (i > 0 && (i + 1) % table1_Header.size() == 0) {
//					builder.append("</tr><tr>");
//				}
//			}
//			builder.append(" </tr>  ");
//			builder.append("</table></br></br></br>");
//		}
		
		if (table1 != null && !table1.isEmpty()) {
			System.out.println("Table  1");
//			builder.append("<h2>" + heading1 + "</h2>");

			builder.append("</br></br></br><table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
			builder.append("<tr>");

			// Product Table1 heading
			for (String header : table1_Header)
				builder.append("<th>" + header + "</th>");
			builder.append("</tr>");

			System.out.println("Size of table 1 " + table1.size());

			/**
			 * write your logic for rowspan of price column
			 */

			if (comp != null&& comp.getSocialInfo() != null&& comp.getSocialInfo().getFaceBookId().equalsIgnoreCase("Consolidated Contract Renewal")) {

				logger.log(Level.SEVERE, "consolidated invoice True ");
				double sumOfInvoiceRate = 0;
				int col = 0;
				int actualCol = 0;
				int totalRow = 0;
				if (comp.getSocialInfo().getTwitterId() != null&& !comp.getSocialInfo().getTwitterId().equals("")) {
					totalRow = Integer.parseInt(comp.getSocialInfo().getTwitterId());
				}
				if (comp.getSocialInfo().getGooglePlusId() != null&& !comp.getSocialInfo().getGooglePlusId().equals("")) {
					actualCol = Integer.parseInt(comp.getSocialInfo().getGooglePlusId());
				}
				logger.log(Level.SEVERE, "TotalRow Size " + totalRow);
				logger.log(Level.SEVERE, "Col No " + actualCol);
				boolean rowspanFlag = false;
				boolean rowspanFlag1 = false;

				builder.append("<tr>");
				for (int i = 0; i < table1.size(); i++) {
//					builder.append("<td></td>");
					if (actualCol == col) {
						logger.log(Level.SEVERE, "actualCol " + actualCol);
						logger.log(Level.SEVERE, "Col " + col);
						logger.log(Level.SEVERE, "rowspanFlag " + rowspanFlag);
						if (rowspanFlag == false) {
							builder.append("<td rowspan=\""+comp.getSocialInfo().getTwitterId()+"\">"+ table1.get(i) + "</td>");
							rowspanFlag = true;
						}
					}else if((actualCol+1) == col){
						logger.log(Level.SEVERE, "Col " + col);
						logger.log(Level.SEVERE, "rowspanFlag " + rowspanFlag1);
						if (rowspanFlag1 == false) {
							builder.append("<td rowspan=\""+comp.getSocialInfo().getTwitterId()+"\">"+ table1.get(i) + "</td>");
							rowspanFlag1 = true;
						}
					}else {
						builder.append("<td>" + table1.get(i) + "</td>");
					}
					col++;

					logger.log(Level.SEVERE, "Coloumn count" + col);
					System.out.println("Table Started 1" + table1.get(i));

					if (i > 0 && (i + 1) % table1_Header.size() == 0) {
						builder.append("</tr><tr>");
						logger.log(Level.SEVERE, "Break ");
						col = 0;
					}

				}

				builder.append("</tr>");

			} else {
				logger.log(Level.SEVERE, "consolidated contract renewal false 2");
				builder.append("<tr>");
				for (int i = 0; i < table1.size(); i++) {
					builder.append("<td>" + table1.get(i) + "</td>");
					System.out.println("Table Started 1" + table1.get(i));
					if (i > 0 && (i + 1) % table1_Header.size() == 0) {
						builder.append("</tr><tr>");
					}
				}
				builder.append(" </tr>  ");
			}

//			builder.append("</table></b></br>");
			builder.append("</table></br></br></br>");
		}
		
		

		builder.append(msgSub);
		builder.append(" <B>   </br></br></B>");
		builder.append(msgBody);
		builder.append(" <B>   </br></br></B>");
		builder.append(footerMsg);
		builder.append(" <B>   </br></br></B>");
		// Company Details
		builder.append("</br><b>" + company.getBusinessUnitName() + "</b></br>");
		builder.append("<table>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Address : ");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getAddrLine2().equals("")
				&& company.getAddress().getAddrLine2() != null)
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getAddrLine2() + " "
					+ company.getAddress().getLocality());
		else
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getLocality());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getLandmark().equals("")
				&& company.getAddress().getLandmark() != null)
			builder.append(company.getAddress().getLandmark() + ","
					+ company.getAddress().getCity() + ","
					+ company.getAddress().getPin()
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		else
			builder.append(company.getAddress().getCity() + ","
					+ company.getAddress().getPin() + ","
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Website : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getWebsite());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Phone No : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getCellNumber1());
		builder.append("</td>");
		builder.append("<tr>");

		builder.append("</tr>");
		builder.append("</table>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);

		try {
			System.out.println("Inside Try Block");
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company.getEmail());

			// To Mail Address
			message.setFrom(new InternetAddress(company.getEmail()));
			message.setReplyTo(new InternetAddress[] { new InternetAddress(
					company.getEmail()) });

			// Email to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);
			message.setRecipient(Message.RecipientType.CC, new InternetAddress(
					company.getEmail()));

			// Mail Subject
			message.setSubject(mailSubject);

			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentInHtml, "text/html");
			multiPart.addBodyPart(htmlpart);

			// Mail attachment
			if (attachFiles != null) {
				System.out.println("Inside Attachment.........");

				MimeBodyPart attachbody = new MimeBodyPart();
				attachbody.setFileName(attachFiles + ".pdf");
				DataSource src = new ByteArrayDataSource(
						pdfstream.toByteArray(), "application/pdf");
				attachbody.setDataHandler(new DataHandler(src));
				multiPart.addBodyPart(attachbody);
			}

			System.out.println("Ready To Send Mail");

			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			System.out.println("Email sent Successfully !!!!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void customerUploadEmail(ArrayList<String> toEmailId,
			String mailSubject, String mailTitle, Company company,
			ArrayList<String> table1_Header, ArrayList<String> table1,
			String heading2, ArrayList<String> table2_Header,
			ArrayList<String> table2, String[] attachFiles) throws IOException {

		logger.log(Level.SEVERE, "Entered In Common Method");

		String[] toEmailIdArray = new String[toEmailId.size()];
		toEmailId.toArray(toEmailIdArray);

		StringBuilder builder = new StringBuilder();

		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		/************************ Table 1 ***************************************/

		if (table1 != null && !table1.isEmpty()) {
			System.out.println("Table  1");
			// builder.append("<h2>"+heading1+"</h2>");

			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
			builder.append("<tr>");

			// Product Table1 heading
			for (String header : table1_Header)
				builder.append("<th>" + header + "</th>");
			builder.append("</tr>");

			System.out.println("Size of table 1 " + table1.size());

			builder.append("<tr>");
			for (int i = 0; i < table1.size(); i++) {
				builder.append("<td>" + table1.get(i) + "</td>");
				System.out.println("Table Started 1" + table1.get(i));
				if (i > 0 && (i + 1) % table1_Header.size() == 0) {
					builder.append("</tr><tr>");

				}
			}
			builder.append(" </tr>  ");
			builder.append("</table></b></br>");
		}

		/************************************* Table 2 ************************************/

		if (table2 != null && !table2.isEmpty()) {
			System.out.println("Table 2");
			builder.append("<h2>" + heading2 + "</h2>");

			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"1\"  Style width=\"50%\" >");
			builder.append("<col width=\"1%\" >");
			builder.append("<col width=\"2%\" >");
			builder.append("<col width=\"3%\" >");
			builder.append("<col width=\"10%\">");

			builder.append("<tr>");
			// Product Table2 heading
			for (String header : table2_Header)
				builder.append("<th>" + header + "</th>");

			builder.append("</tr>");

			System.out.println("Size of list " + table2.size());
			builder.append("<tr>");
			for (int i = 0; i < table2.size(); i++) {
				builder.append("<td style=\"width:5%\">" + table2.get(i)
						+ "</td>");
				// if(i==6){
				// builder.append("<td style=\"width:20%\">" + table2.get(i) +
				// "</td>");
				// }
				System.out.println("Table Started 2 :" + table2.get(i));
			}
			builder.append(" </tr>  ");
			builder.append("</table></b></br>");
		}

		/**************************** FOOTER Details *********************************/
		// if(mailFooter!=null){
		// builder.append("<p>"+mailFooter+"</p></br></b></br>");
		// }

		// Company Details
		builder.append("<br>");
		builder.append("<b>" + company.getBusinessUnitName() + "</b></br>");
		builder.append("<table>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Address : ");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getAddrLine2().equals("")
				&& company.getAddress().getAddrLine2() != null)
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getAddrLine2() + " "
					+ company.getAddress().getLocality());
		else
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getLocality());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getLandmark().equals("")
				&& company.getAddress().getLandmark() != null)
			builder.append(company.getAddress().getLandmark() + ","
					+ company.getAddress().getCity() + ","
					+ company.getAddress().getPin()
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		else
			builder.append(company.getAddress().getCity() + ","
					+ company.getAddress().getPin() + ","
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Website : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getWebsite());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Phone No : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getCellNumber1());
		builder.append("</td>");
		builder.append("<tr>");

		builder.append("</tr>");
		builder.append("</table>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);

		/************************************* Mail Send Logic *********************************/
		try {
			System.out.println("Inside Try Block");
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company.getEmail());

			// To Mail Address
			message.setFrom(new InternetAddress(company.getEmail()));
			message.setReplyTo(new InternetAddress[] { new InternetAddress(
					company.getEmail()) });

			// logger.log(Level.SEVERE,"To Email Address"+company.getEmail());

			// Email to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);

				logger.log(Level.SEVERE, "To Email Address" + mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);
			// message.setRecipient(Message.RecipientType.CC, new
			// InternetAddress(company.getEmail()));

			// logger.log(Level.SEVERE,"To Email Address"+company.getEmail());

			// Mail Subject
			message.setSubject(mailSubject);

			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentInHtml, "text/html");
			multiPart.addBodyPart(htmlpart);

			// Mail attachment
			// if(attachFiles!=null){
			// System.out.println("Inside Attachment.........");
			// MimeBodyPart attachbody=new MimeBodyPart();
			//
			// attachbody.setFileName(attachFiles[0]+".pdf");
			// DataSource src=new ByteArrayDataSource(pdfstream.toByteArray(),
			// "application/pdf");
			// attachbody.setDataHandler(new DataHandler(src));
			// System.out.println("Attach File Name");
			// multiPart.addBodyPart(attachbody);

			// adds attachments
			// if (attachFiles != null && attachFiles.length > 0) {
			// System.out.println("More");
			// for (String filePath : attachFiles) {
			// MimeBodyPart attachPart = new MimeBodyPart();
			// try {
			// attachPart.attachFile(filePath);
			// } catch (IOException ex) {
			// ex.printStackTrace();
			// }
			// multiPart.addBodyPart(attachPart);
			// }
			// }
			// }

			System.out.println("Ready To Send Mail");

			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			System.out.println("Email sent Successfully !!!!");
			logger.log(Level.SEVERE, "Email sent Successfully !!");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**************************** contract renewal for customer email ***********************/

	/**
	 * To send email with HTML format below method is reusable Date :4/3/2017
	 * @param fromEmailId 
	 * @param ccEmailList 
	 */

	public void htmlformatsendEmail(ArrayList<String> toEmail,
			String mailSubject, String mailTitle, Company company,
			String msgBody, String footerMsg, String attachFiles,
			ArrayList<String> table1_Header, ArrayList<String> table1, ArrayList<String> ccEmailList, String fromEmailId) {

		// to Email id's in array
		String[] toEmailIdArray = new String[toEmail.size()];
		toEmail.toArray(toEmailIdArray);

		System.out.println("Mail Sub : " + mailSubject);
		System.out.println("Mail Title : " + mailTitle);

		StringBuilder builder = new StringBuilder();

		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("<h3 class=\"ex\" align=\"center\">" + mailTitle
				+ "</h3></br></br>");

		builder.append(" <p>  </p> </br></br>");

		builder.append(" <B>   </br></br></B>");
		builder.append(msgBody);
		builder.append(" <B>   </br></br></B>");

		/************************ Table 1 ***************************************/

		if (table1 != null && !table1.isEmpty()) {
			System.out.println("Table  1");
			// builder.append("<h2>"+heading1+"</h2>");

			builder.append("</br></br></br><table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
			builder.append("<tr>");

			// Product Table1 heading
			for (String header : table1_Header)
				builder.append("<th>" + header + "</th>");
			builder.append("</tr>");

			System.out.println("Size of table 1 " + table1.size());

			builder.append("<tr>");
			for (int i = 0; i < table1.size(); i++) {
				builder.append("<td>" + table1.get(i) + "</td>");
				System.out.println("Table Started 1" + table1.get(i));
				if (i > 0 && (i + 1) % table1_Header.size() == 0) {
					builder.append("</tr><tr>");
				}
			}
			builder.append(" </tr>  ");
			builder.append("</table></br></br></br>");
		}

		builder.append("  <br><br>");
		builder.append("  <br><br>");
		// Company Details
		builder.append("</br><b>" + company.getBusinessUnitName() + "</b></br>");
		builder.append("<table>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Address : ");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getAddrLine2().equals("")
				&& company.getAddress().getAddrLine2() != null)
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getAddrLine2() + " "
					+ company.getAddress().getLocality());
		else
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getLocality());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getLandmark().equals("")
				&& company.getAddress().getLandmark() != null)
			builder.append(company.getAddress().getLandmark() + ","
					+ company.getAddress().getCity() + ","
					+ company.getAddress().getPin()
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		else
			builder.append(company.getAddress().getCity() + ","
					+ company.getAddress().getPin() + ","
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Website : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getWebsite());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Phone No : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getCellNumber1());
		builder.append("</td>");
		builder.append("<tr>");

		builder.append("</tr>");
		builder.append("</table>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);
		
		
		/**
		 * @author Vijay Chougule Date 28-12-2020
		 * Des :- Email From Sales Person and CC in Approver Name and Branch Email id and Company Email Id
		 * send Emails From SendGrid
		 * 
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.PC_ENABLESENDEMAILSFROMPERSONRESPONSIBLE, company.getCompanyId())) {
			
			 Company compEntity = ofy().load().type(Company.class).filter("companyId", company.getCompanyId()).first().now();
			logger.log(Level.SEVERE, "company.getEmail() "+compEntity.getEmail());
			if(compEntity.getEmail()!=null && !compEntity.getEmail().equals("")) {
				logger.log(Level.SEVERE, "company.getEmail() == "+compEntity.getEmail());
				ccEmailList.add(compEntity.getEmail());
			}
			logger.log(Level.SEVERE, "Email CC "+ccEmailList);
			logger.log(Level.SEVERE, "Email To "+toEmail);

			ArrayList<String> bcclist = new ArrayList<String>();
			bcclist.add(fromEmailId);
			
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			sdEmail.sendMailWithSendGrid(fromEmailId, toEmail, ccEmailList, bcclist, mailSubject, contentInHtml, "text/html",pdfstream,attachFiles,"application/pdf",null);
			return;
		}
		/**
		 * ends here
		 */

		try {
			System.out.println("Inside Try Block");
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company.getEmail());

			// To Mail Address
			message.setFrom(new InternetAddress(company.getEmail()));
			message.setReplyTo(new InternetAddress[] { new InternetAddress(
					company.getEmail()) });

			// Email to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);
			// message.setRecipient(Message.RecipientType.CC, new
			// InternetAddress(company.getEmail()));

			// Mail Subject
			message.setSubject(mailSubject);

			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentInHtml, "text/html");
			multiPart.addBodyPart(htmlpart);

			// Mail attachment
			if (attachFiles != null) {
				System.out.println("Inside Attachment.........");

				MimeBodyPart attachbody = new MimeBodyPart();
				attachbody.setFileName(attachFiles + ".pdf");
				DataSource src = new ByteArrayDataSource(
						pdfstream.toByteArray(), "application/pdf");
				attachbody.setDataHandler(new DataHandler(src));
				multiPart.addBodyPart(attachbody);
			}

			System.out.println("Ready To Send Mail");

			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			System.out.println("Email sent Successfully !!!!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*************************************************************************************************/

	// @Override
	public void initiateRequestServiceEmail(Long companyId, Date date,
			String desc, PersonInfo info, int complainNo)
			throws IllegalArgumentException {
		comp = ofy().load().type(Company.class).filter("companyId", companyId)
				.first().now();
		String sub = "Service Request Ticket No. " + complainNo;
		String msg = info.getFullName() + "/" + info.getCount()
				+ "has requested a service on " + fmt.format(date) + " "
				+ " Service Description : " + desc;
		String email = comp.getEmail();
		try {
			sendEmailForRequestService(sub, msg, email);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void sendEmailForRequestService(String mailSub, String message1,
			String company) throws IOException {
		try {

			System.out.println("Inside Try Block");
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company);

			// To Mail Address
			message.setFrom(new InternetAddress(company));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					company));

			// Mail Subject
			message.setSubject(mailSub);
			message.setText(message1);
			// // Mail Body Content In html Form

			System.out.println("Ready To Send Mail");

			Transport.send(message);
			System.out.println("Email sent Successfully !!!!");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void initiateLoginInfoEmail(User user)
			throws IllegalArgumentException {

		logger.log(Level.SEVERE, "Rohan in side initiateLoginInfoEmail Method");

		setUserEntity(user);
		loadinfCompInfo();
		sendingMailForExtraLoginUser();

	}

	public void setUserEntity(User user) {
		this.userEntity = user;
	}

	public void loadinfCompInfo() {
		logger.log(Level.SEVERE, "Rohan in sidecompany load Method");
		if (userEntity.getCompanyId() != null) {
			comp = ofy().load().type(Company.class)
					.id(userEntity.getCompanyId()).now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

	}

	public void sendingMailForExtraLoginUser() {
		String toEmail = "support@evasoftwaresolutions.com";
		String subjectLine = "EVA SOFTWARE ALERT";

		String msgBody = ""

		+ "<b>" + "User Name:  " + "</b>" + userEntity.getEmployeeName()
				+ " from '" + comp.getBusinessUnitName()
				+ "' is trying to exceeds the limit of number of licenses on "
				+ "\n" + "<b>" + "Date:  " + "</b>" + fmt.format(new Date())
				+ "</br>" + "</br>" + "\n" + "With " + "<b>" + "User Id "
				+ userEntity.getUserId() + "</br>" + "</br>";
		logger.log(Level.SEVERE, "Rohan in side before sending mail Method"
				+ comp.getEmail().trim());
		sendSupportEmailForExtraLogin(subjectLine, msgBody, comp.getEmail()
				.trim(), toEmail);
		logger.log(Level.SEVERE, "Rohan in side before sending mail Method");
	}

	public void sendSupportEmailForExtraLogin(String subject,
			String contentbody, String from, String to) {
		try {
			System.out.println("Inside Try Block");
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			// To Mail Address
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					to));
			// message.setRecipient(Message.RecipientType.CC, new
			// InternetAddress(comp.getEmail()));
			// Mail Subject
			message.setSubject(subject);

			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentbody, "text/html");
			multiPart.addBodyPart(htmlpart);

			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			System.out.println("Email sent Successfully !!!!");
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Rohan Inside Catch " + e);
			e.printStackTrace();
		}
	}

	/**
	 * This method send pasword reset email to user Date : 17-09-2016 By Anil
	 * Release 30 Sept 2016 I/P :email subject,email message,form email id and
	 * to email id
	 */

	public void sendPasswordResetEmail(String emailSub, String emailMessage,
			String fromEmailId, String toEmailId) {
		try {
			System.out.println("Inside Try Block");
			logger.log(Level.SEVERE, "INSIDE PASSWORD RESET MAIL");
			logger.log(Level.SEVERE, "FROM EMAIL " + fromEmailId);
			logger.log(Level.SEVERE, "TO EMAIL " + toEmailId);
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			// To Mail Address
			message.setFrom(new InternetAddress(fromEmailId));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					toEmailId));

			// Mail Subject
			message.setSubject(emailSub);

			// Mail Body Content In html Form
			message.setText(emailMessage);

			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * date 15 sep 2016 Release Name :- 30 sep 2016 by - vijay Description :-
	 * bellow mehod called when multilevel approval process config is enable and
	 * when we approve the document then request send to next level approver To
	 * send Email Request for Approval To approver and CC all remaining approval
	 * and CC- Requester CC - sales person or purchase engineer or person
	 * responsible
	 */

	@Override
	public void requestForApprovalEmailForMultilevelapproval(Approvals approve,
			ArrayList<String> toEmaiEmployeelList,
			ArrayList<String> ccEmailEmployeeList)
			throws IllegalArgumentException {
		// TODO Auto-generated method stub

		setApprovals(approve);
		initializeApprovals();
		
		/**
		 * Date 02-02-2019 By Vijay 
		 * Des :- NBHC Inventory Management Automatic PR Approval Email send with new format using process configuration
		 * and this is only for AUtomatic PR Generated by Ststem
		 *  
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition", "EnableAutomaticPRNewEmailFormat", approve.getCompanyId())
				&& validateAutomaticPR(approve)){
			sendNewEmailFormatforPR(pr,approve.getApproverName());
		}
		else{
			

		System.out.println("company url==============" + comp.getCompanyURL());
		// ********** to
		// "+comp.getAccessUrl()+"."+SystemProperty.applicationId.get()+".appspot.com"+"Sir/
		// Madam,
		/**
		 * @author Anil , Date : 31-03-2020 12:39 AM
		 * Changing line breaker for JAVAX mail and Send grid mail api
		 */
		String lb="\n";
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Approvals", "EnableSendGridEmailApi", approve.getCompanyId())){
			lb="<br>";
		}
		String message11 = "Dear " + approve.getApproverName() + ","+ lb
				+  lb+ lb + approve.getBusinessprocesstype() + "-"
				+ approve.getBusinessprocessId()
				+ " is awaiting for your approval." + lb+ lb+ lb
				+ "Follow below steps for Approval/Reject" + lb+ lb
				+ "1: Log in to " + comp.getCompanyURL()
				+ " with your username and passward." + lb+ lb
				+ "2: Click on approval menu."+ lb+ lb
				+ "3: Again click on approval node at left." + lb+ lb
				+ "4: Click approval."  + lb+ lb+ "5: Verify document."
				+ lb+ lb
				+ "6: Either approve or reject with appropriate remark." + lb
				+  lb+ lb+ "Requester : " + approve.getRequestedBy();

		System.out.println(" vijay === MESSAGE ===" + message11);
		logger.log(Level.SEVERE, "Msg "+message11);
		/**
		 *  nidhi
		 *  15-1-2018
		 *  updated for Purchase Requisition details
		 */
		boolean processFlag =false;
		try {
			
			ArrayList<String> header = new ArrayList<String>();
			ArrayList<String> headerDetails = new ArrayList<String>();
			
			if(approve.getBusinessprocesstype().equals("Purchase Requisition")){
				 processFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Approvals", "SENDPRDETAILINAPPROVALMAIL", approve.getCompanyId());
				
				
				if(processFlag){
					
					 message11 = "Dear " + approve.getApproverName() + "," + "<br>"
								+ "<br>" + "<br>" + approve.getBusinessprocesstype() + "-"
								+ approve.getBusinessprocessId()
								+ " is awaiting for your approval." + "<br>" + "<br>" + "<br>"
								+ "Follow below steps for Approval/Reject" + "<br>" + "<br>"
								+ "1: Log in to " + comp.getCompanyURL()
								+ " with your username and passward." + "<br>" + "<br>"
								+ "2: Click on approval menu." + "<br>" + "<br>"
								+ "3: Again click on approval node at left." + "<br>" + "<br>"
								+ "4: Click approval." + "<br>" + "<br>" + "5: Verify document."
								+ "<br>" + "<br>"
								+ "6: Either approve or reject with appropriate remark." + "\n"
								+ "<br>" + "<br>" + "Requester : " + approve.getRequestedBy()
								+"<br><br><br><br>";
					PurchaseRequisition purDetail = ofy().load().type(PurchaseRequisition.class)
							.filter("companyId", approve.getCompanyId())
							.filter("count", approve.getBusinessprocessId())
							.first().now();
					if(purDetail !=null){
						header.add("PR ID");
						header.add("PR TITLE");
						header.add("PR DATE");
						header.add("EXPECTED DELIVERY DATE");
						header.add("PROJECT");
						header.add("PR GROUP");
						header.add("PR CATEGORY");
						header.add("PR TYPE");
						header.add("BRANCH");
						header.add("DESCRIPTION");
						header.add("PRODUCT CODE");
						header.add("PRODUCT NAME");
						header.add("PRODUCT CATEGORY");
						header.add("QUANTITY");
						header.add("UOM");
						header.add("TOTAL");
						header.add("REMARK");
						
						SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy");
						
						for(ProductDetails pur : purDetail.getPrproduct()){
							headerDetails.add(purDetail.getCount()+"");
							headerDetails.add(purDetail.getPrTitle());
							if(purDetail.getCreationDate()!=null){
								headerDetails.add(""+ sd.format(DateUtility.getDateWithTimeZone("IST", purDetail.getCreationDate()))+"");
							}else{
								headerDetails.add("");
							}
							
							
							if(purDetail.getExpectedDeliveryDate()!=null){
								headerDetails.add(""+ sd.format(DateUtility.getDateWithTimeZone("IST", purDetail.getExpectedDeliveryDate()))+"");
							}else{
								headerDetails.add("");
							}
							
							headerDetails.add(purDetail.getProject());
							headerDetails.add(purDetail.getPrGroup());
							headerDetails.add(purDetail.getPrCategory());
							headerDetails.add(purDetail.getPrType());
							headerDetails.add(purDetail.getBranch());
							headerDetails.add(purDetail.getDescription());
							headerDetails.add(pur.getProductCode());
							headerDetails.add(pur.getProductName());
							headerDetails.add(pur.getProductCategory());
							headerDetails.add(pur.getProductQuantity()+"");
							headerDetails.add(pur.getUnitOfmeasurement());
							headerDetails.add(pur.getTotal()+"");
							headerDetails.add(purDetail.getRemark());
						}
						
						
						
					}
				}
			}
			System.out.println("Inside try calling Method...");
			logger.log(Level.SEVERE, "before To Call Common Method");

			System.out
					.println(" hi vijay -- multilevel approval Requested By   -====="
							+ approve.getRequestedBy());
			System.out
					.println(" hi vijay -- multilevel approval - sales person ====="
							+ approve.getCreatedBy());

			Employee toemployee = null;
			Employee ccemployee = null;

			ArrayList<String> toEmailList = new ArrayList<String>();
			ArrayList<String> ccEmailList = new ArrayList<String>();

			for (int i = 0; i < toEmaiEmployeelList.size(); i++) {
				toemployee = ofy().load().type(Employee.class)
						.filter("companyId", approve.getCompanyId())
						.filter("fullname", toEmaiEmployeelList.get(i)).first()
						.now();
				if (toemployee != null) {
					if (!toemployee.getEmail().trim().equals(""))
						toEmailList.add(toemployee.getEmail().trim());

				}
			}

			for (int i = 0; i < ccEmailEmployeeList.size(); i++) {
				ccemployee = ofy().load().type(Employee.class)
						.filter("companyId", approve.getCompanyId())
						.filter("fullname", ccEmailEmployeeList.get(i)).first()
						.now();
				if (ccemployee != null) {
					if (!ccemployee.getEmail().trim().equals(""))
						ccEmailList.add(ccemployee.getEmail().trim());
				}
			}

			logger.log(Level.SEVERE, "Ready To Call Common Method");

			if(processFlag){
				sendEmailOnRequestForApproval(
						"Request for approval : "
								+ approve.getBusinessprocesstype() + "-"
								+ approve.getBusinessprocessId(),
						approve.getBusinessprocessId(),
						approve.getBusinessprocesstype(), null, null, message11,
						comp.getEmail(), toEmailList, ccEmailList,header,headerDetails);
			}else{
				sendEmailOnRequestForApproval(
						"Request for approval : "
								+ approve.getBusinessprocesstype() + "-"
								+ approve.getBusinessprocessId(),
						approve.getBusinessprocessId(),
						approve.getBusinessprocesstype(), null, null, message11,
						comp.getEmail(), toEmailList, ccEmailList);
			}
			
		
		
		}

		catch (IOException e) {
			e.printStackTrace();
		}

		}
	}

	

	
	/*
	 * date 15 sep 2016 Release Name :- 30 sep 2016 by - vijay Description :-
	 * bellow mehod called when multilevel approval process config is enable and
	 * when we approve the document To send Email While Approving Document Email
	 * To approver and CC all remaining approval and CC- Requester CC - sales
	 * person or purchase engineer or person responsible
	 */

	@Override
	public void emailForMultilevelapproval(Approvals approve,
			ArrayList<String> toEmaiEmployeelList,
			ArrayList<String> ccEmailEmployeeList)
			throws IllegalArgumentException {
		// TODO Auto-generated method stub

		// logger.log(Level.SEVERE,"hiiiiii vijay ==  "+approve.getApprovalOrRejectDate());

		String message11 = "Dear " + approve.getApproverName() + "," + "\n"
				+ "\n" + "\n" + approve.getBusinessprocesstype() + "-"
				+ approve.getBusinessprocessId() + " has been "
				+ approve.getStatus() + "\n" + "\n" + "Remark : "
				+ approve.getRemark() + "\n" + "\n" + approve.getStatus()
				+ " Date : " + fmt.format(approve.getApprovalOrRejectDate())
				+ "\n" + "\n" + "\n" + "Approver : "
				+ approve.getApproverName();

		setApprovals1(approve);
		initializeApprovals1();
		boolean processFlag =false;
		try {
			
			ArrayList<String> header = new ArrayList<String>();
			ArrayList<String> headerDetails = new ArrayList<String>();
			
			if(approve.getBusinessprocesstype().equals("Purchase Requisition")){
				 processFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Approvals", "SENDPRDETAILINAPPROVALMAIL", approve.getCompanyId());
				
				
				if(processFlag){
					 message11 = "Dear " + approve.getApproverName() + "," + "<br>"
								+ "<br>" + "<br>" + approve.getBusinessprocesstype() + "-"
								+ approve.getBusinessprocessId()
								+ " is awaiting for your approval." + "<br>" + "<br>" + "<br>"
								+ "Follow below steps for Approval/Reject" + "<br>" + "<br>"
								+ "1: Log in to " + comp.getCompanyURL()
								+ " with your username and passward." + "<br>" + "<br>"
								+ "2: Click on approval menu." + "<br>" + "<br>"
								+ "3: Again click on approval node at left." + "<br>" + "<br>"
								+ "4: Click approval." + "<br>" + "<br>" + "5: Verify document."
								+ "<br>" + "<br>"
								+ "6: Either approve or reject with appropriate remark." + "\n"
								+ "<br>" + "<br>" + "Requester : " + approve.getRequestedBy()
								+"<br><br><br><br>";
					PurchaseRequisition purDetail = ofy().load().type(PurchaseRequisition.class)
							.filter("companyId", approve.getCompanyId())
							.filter("count", approve.getBusinessprocessId())
							.first().now();
					if(purDetail !=null){
						header.add("PR ID");
						header.add("PR TITLE");
						header.add("PR DATE");
						header.add("EXPECTED DELIVERY DATE");
						header.add("PROJECT");
						header.add("PR GROUP");
						header.add("PR CATEGORY");
						header.add("PR TYPE");
						header.add("BRANCH");
						header.add("DESCRIPTION");
						header.add("PRODUCT CODE");
						header.add("PRODUCT NAME");
						header.add("PRODUCT CATEGORY");
						header.add("QUANTITY");
						header.add("UOM");
						header.add("TOTAL");
						header.add("REMARK");
						
						
						SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy");
						
						for(ProductDetails pur : purDetail.getPrproduct()){
							headerDetails.add(purDetail.getCount()+"");
							headerDetails.add(purDetail.getPrTitle());
							if(purDetail.getCreationDate()!=null){
								headerDetails.add(""+ sd.format(DateUtility.getDateWithTimeZone("IST", purDetail.getCreationDate()))+"");
							}else{
								headerDetails.add("");
							}
							
							
							if(purDetail.getExpectedDeliveryDate()!=null){
								headerDetails.add(""+ sd.format(DateUtility.getDateWithTimeZone("IST", purDetail.getExpectedDeliveryDate()))+"");
							}else{
								headerDetails.add("");
							}
							
							headerDetails.add(purDetail.getProject());
							headerDetails.add(purDetail.getPrGroup());
							headerDetails.add(purDetail.getPrCategory());
							headerDetails.add(purDetail.getPrType());
							headerDetails.add(purDetail.getBranch());
							headerDetails.add(purDetail.getDescription());
							headerDetails.add(pur.getProductCode());
							headerDetails.add(pur.getProductName());
							headerDetails.add(pur.getProductCategory());
							headerDetails.add(pur.getProductQuantity()+"");
							headerDetails.add(pur.getUnitOfmeasurement());
							headerDetails.add(pur.getTotal()+"");
							headerDetails.add(purDetail.getRemark());
						}
						
						
						
					}
				}
			}
			System.out.println("Inside try calling Method...");
			logger.log(Level.SEVERE, "before To Call Common Method");

			System.out
					.println(" hi vijay -- multilevel approval  Requested By -====="
							+ approve.getRequestedBy());
			System.out
					.println(" hi vijay -- multilevel approval sales person  -====="
							+ approve.getCreatedBy());

			Employee toemployee = null;
			Employee ccemployee = null;

			ArrayList<String> toEmailList = new ArrayList<String>();
			ArrayList<String> ccEmailList = new ArrayList<String>();

			for (int i = 0; i < toEmaiEmployeelList.size(); i++) {
				toemployee = ofy().load().type(Employee.class)
						.filter("companyId", approve.getCompanyId())
						.filter("fullname", toEmaiEmployeelList.get(i)).first()
						.now();
				if (toemployee != null) {
					if (!toemployee.getEmail().trim().equals(""))
						toEmailList.add(toemployee.getEmail().trim());

				}
			}

			for (int i = 0; i < ccEmailEmployeeList.size(); i++) {
				ccemployee = ofy().load().type(Employee.class)
						.filter("companyId", approve.getCompanyId())
						.filter("fullname", ccEmailEmployeeList.get(i)).first()
						.now();
				if (ccemployee != null) {
					if (!ccemployee.getEmail().trim().equals(""))
						ccEmailList.add(ccemployee.getEmail().trim());
				}
			}

			logger.log(Level.SEVERE, "Ready To Call Common Method");

			
			if(processFlag){
				sendEmailOnRequestForApproval(
						approve.getStatus() + " : "
								+ approve.getBusinessprocesstype() + "-"
								+ approve.getBusinessprocessId(),
						approve.getBusinessprocessId(),
						approve.getBusinessprocesstype(),
						approve.getApprovalOrRejectDate(), approve.getRemark(),
						message11, comp.getEmail(), toEmailList, ccEmailList,header,headerDetails);
			}else{
				sendEmailOnRequestForApproval(
						approve.getStatus() + " : "
								+ approve.getBusinessprocesstype() + "-"
								+ approve.getBusinessprocessId(),
						approve.getBusinessprocessId(),
						approve.getBusinessprocesstype(),
						approve.getApprovalOrRejectDate(), approve.getRemark(),
						message11, comp.getEmail(), toEmailList, ccEmailList);
			}
			
		}

		catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @param mailSub
	 * @param mailMsg
	 * @param toEmailLis
	 * @param ccEmailLis
	 * @param fromEmail
	 * 
	 *            This method is used to send Mail it can be used as common
	 *            method Date : 14-10-2016 by Anil Release : 30 Sept 2016
	 *            Project : PURCHASE MODIFICATION (NBHC)
	 */

	@Override
	public void sendEmail(String mailSub, String mailMsg,
			ArrayList<String> toEmailLis, ArrayList<String> ccEmailLis,
			String fromEmail) {
		try {

			System.out.println("Inside Try Block");
			logger.log(Level.SEVERE, "INSIDE SEND MAIL : EMAIL SERVICE ");
			logger.log(Level.SEVERE, "FROM EMAIL " + fromEmail);
			logger.log(Level.SEVERE, "To EMAIL " + toEmailLis);

			for (String to : toEmailLis) {
				if (ccEmailLis!=null && ccEmailLis.contains(to)) {
					ccEmailLis.remove(to);
				}
			}

			String[] toEmailIdArray = new String[toEmailLis.size()];
			toEmailLis.toArray(toEmailIdArray);

			String[] ccEmailIdArray =null;
			if(ccEmailLis!=null){
				ccEmailIdArray = new String[ccEmailLis.size()];
				ccEmailLis.toArray(ccEmailIdArray);
			}
			
			/*** Date 03-04-2019 by Vijay for NBHC CCPM Email quota exceeds so updated code send email through sendgrid ***/ 
			List<Company> compEntity = ofy().load().type(Company.class).list();
			if(compEntity.size()>0){
				for(Company company : compEntity){
					if(company.getAccessUrl().equals("my")){
						comp = company;
					}
				}
			}
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CronJob", "EnableSendGridEmailApi",comp.getCompanyId()) || 
					ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, comp.getCompanyId())){
				logger.log(Level.SEVERE," company email --" +comp.getEmail() +"  to email  " + toEmailIdArray.toString());
				SendGridEmailServlet sdEmail=new SendGridEmailServlet();
				sdEmail.sendMailWithSendGrid(fromEmail, toEmailLis, ccEmailLis, null, mailSub, mailMsg, "text/html",null,null,null,comp.getDisplayNameForCompanyEmailId());
				
				return;
			}
			/**
			 * ends here
			 */

			Properties props = new Properties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", "smtp.gmail.com");
			Session session = Session.getDefaultInstance(props);
			// Session session = Session.getDefaultInstance(props, null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			// To Mail Address
			message.setFrom(new InternetAddress(fromEmail));

			// Email To to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				logger.log(Level.SEVERE, "To Email id multipart "
						+ mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);

			if(ccEmailIdArray!=null){
			// Email CC to Multiple Recipients
			InternetAddress[] ccmailToMultiple = new InternetAddress[ccEmailIdArray.length];
				for (int i = 0; i < ccEmailIdArray.length; i++) {
					ccmailToMultiple[i] = new InternetAddress(ccEmailIdArray[i]);
					logger.log(Level.SEVERE, "CC Email id multipart "
							+ ccmailToMultiple[i]);
				}
				if (ccmailToMultiple.length != 0) {
					message.setRecipients(Message.RecipientType.CC,
							ccmailToMultiple);
				}
			}

			// Mail Subject
			message.setSubject(mailSub);

			// Mail Body Content In html Form
			message.setText(mailMsg);

			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			logger.log(Level.SEVERE, "Mail Sent");
		} catch (Exception e) {
			e.printStackTrace();

			logger.log(Level.SEVERE, "Mail Not Sent " + e);
		}
	}

	@Override
	public void emailToSalesPersonFromLead(Lead lead)
			throws IllegalArgumentException {

		if (lead.getCompanyId() != null) {
			comp = ofy().load().type(Company.class).id(lead.getCompanyId())
					.now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}
		/**
		 * Date : 04-11-2017 BY ANIL Added lead id in mail content of lead
		 */
		String message11 = "Lead Id : " + lead.getCount() + "\n" + "\n";
		/**
		 * End
		 */
		if (lead.getDescription() != null && !lead.getDescription().equals("")) {
			message11 = message11 + lead.getDescription() + "\n" + "\n"
					+ "Requester : " + lead.getCreatedBy();
		} else {
			message11 = message11 + "\n" + "\n" + "Requester : "
					+ lead.getCreatedBy();
		}

		

		try {
			logger.log(Level.SEVERE, "before To Call Common Method");
			Employee toemployee = null;
			ArrayList<String> toEmailList = new ArrayList<String>();
			toemployee = ofy().load().type(Employee.class)
					.filter("companyId", lead.getCompanyId())
					.filter("fullname", lead.getEmployee()).first().now();
			if (toemployee != null) {
				if (!toemployee.getEmail().trim().equals("")) {
					toEmailList.add(toemployee.getEmail().trim());
				}
			}
			logger.log(Level.SEVERE, "Ready To Call Common Method"
					+ toEmailList.size());
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.PC_ENABLESENDEMAILSFROMPERSONRESPONSIBLE, lead.getCompanyId())) {
				ArrayList<String> ccEmailList = new ArrayList<String>();
				ccEmailList.add(comp.getEmail());
				
				String salespersonEmail = getSalesPersonEmail(lead.getEmployee(),lead.getCompanyId(),lead.getBranch());
				if(salespersonEmail==null) {
					salespersonEmail = comp.getEmail();
				}
				String approverEmailId = getApproverEmailId(lead.getApproverName(),lead.getCompanyId());
				if(approverEmailId!=null) {
					ccEmailList.add(approverEmailId);
				}
				String branchEmailId = getBranchEmailId(lead.getBranch(), lead.getCompanyId());
				if(branchEmailId!=null) {
					ccEmailList.add(branchEmailId);
				}
				ArrayList<String> bcclist = new ArrayList<String>();
				bcclist.add(salespersonEmail);
				
				SendGridEmailServlet sdEmail=new SendGridEmailServlet();
				sdEmail.sendMailWithSendGrid(salespersonEmail, toEmailList, ccEmailList, bcclist, "Request for Lead : " + lead.getCount(), message11, "text/html",pdfstream,null,null,null);
	
				return;
			}

			sendLeadEmail("Request for Lead : " + lead.getCount(), 0, null,
					null, null, message11, comp.getEmail(), toEmailList, null);
		}

		catch (IOException e) {
			e.printStackTrace();
		}
	}

	

	private void sendLeadEmail(String mailSub, int docId, String docType,
			Date approveDate, String remark, String message1, String company,
			ArrayList<String> toEmailList, ArrayList<String> ccEmailList)
			throws IOException {

		// for(int j=0;j<ccEmailList.size();j++){
		// if(toEmailList.get(i).equals(ccEmailList.get(j))){
		// ccEmailList.remove(j);
		// }
		// }

		String[] toEmailIdArray = new String[toEmailList.size()];
		toEmailList.toArray(toEmailIdArray);

		// String []ccEmailIdArray=new String[ccEmailList.size()];
		// ccEmailList.toArray(ccEmailIdArray);

		try {
			System.out.println("Inside Try Block");
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company);

			// To Mail Address
			message.setFrom(new InternetAddress(company));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					company));

			// Email To to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);
				logger.log(Level.SEVERE, "To Email id multipart "
						+ mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);

			// Email CC to Multiple Recipients
			// InternetAddress[] ccmailToMultiple = new
			// InternetAddress[ccEmailIdArray.length];
			// for( int i=0; i<ccEmailIdArray.length; i++ ){
			// ccmailToMultiple[i] = new InternetAddress(ccEmailIdArray[i]);
			// System.out.println("Email id multipart "+ccmailToMultiple[i]);
			// logger.log(Level.SEVERE,"CC Email id multipart "+ccmailToMultiple[i]);
			// }

			// message.setRecipients(Message.RecipientType.CC,
			// ccmailToMultiple);

			// below line for add BCC if needed then uncomment
			// message.setRecipient(Message.RecipientType.BCC, new
			// InternetAddress(approverEmail));

			// Mail Subject
			message.setSubject(mailSub);
			message.setText(message1);
			// // Mail Body Content In html Form
			// MimeBodyPart htmlpart=new MimeBodyPart();
			// htmlpart.setContent(message1, "text");
			// multiPart.addBodyPart(htmlpart);

			System.out.println("Ready To Send Mail");

			Transport.send(message);
			System.out.println("Email sent Successfully !!!!");
			logger.log(Level.SEVERE, "Email sent Successfully !!!!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	

	/****************************** Initiate Service Po Mail *********************************/
	/**
	 * This method Date : 18-10-2016 By Anil Release : 30 Sept 2016 Project :
	 * Purchase Modification (NBHC)
	 */

	@Override
	public void initiateServicePoEmail(ServicePo po)
			throws IllegalArgumentException {
		servicPo = po;

		if (po.getCompanyId() != null) {
			comp = ofy().load().type(Company.class).id(po.getCompanyId()).now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		if (po.getCompanyId() == null) {
			emp = ofy().load().type(Employee.class)
					.filter("fullname", po.getEmployee()).first().now();
		} else {
			emp = ofy().load().type(Employee.class)
					.filter("companyId", po.getCompanyId())
					.filter("fullname", po.getEmployee()).first().now();
		}

		if (po.getCompanyId() != null) {
			v = ofy().load().type(Vendor.class)
					.filter("companyId", po.getCompanyId())
					.filter("count", po.getVinfo().getCount())
					.filter("vendorStatus", true).first().now();
		}

		createServicePoAttachment();
		createServicePoUploadDocumentAttachment();
		initializeServicePo();

	}

	public void createServicePoAttachment() {
		servicePoPdf = new ServicePoPdf();
		servicePoPdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(servicePoPdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		servicePoPdf.document.open();

		servicePoPdf.createPdfForEmail(comp, v, servicPo, emp);
		servicePoPdf.document.close();
	}

	private void createServicePoUploadDocumentAttachment() {
		blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		uploadStream = new ByteArrayOutputStream();

		String urlData = "";
		String fileName = "";

		if (servicPo.getUpload() != null
				&& !servicPo.getUpload().getUrl().equals("")) {
			urlData = servicPo.getUpload().getUrl();
			logger.log(Level.SEVERE, "Doc URL :::: " + urlData);

			fileName = servicPo.getUpload().getName();
			logger.log(Level.SEVERE, "Document Name :::: " + fileName);

			String[] res = urlData.split("blob-key=", 0);
			logger.log(Level.SEVERE, "Splitted Url :::: " + res);

			String blob = res[1].trim();
			logger.log(Level.SEVERE, "Splitted Url Assigned to string  :::: "
					+ blob);

			BlobKey blobKey = null;
			try {
				blobKey = new BlobKey(blob);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);

			// This method read bytes from blobstore
			long inxStart = 0;
			long inxEnd = 1024;
			boolean flag = false;
			byte[] bytes = null;
			do {
				try {
					bytes = blobstoreService.fetchData(blobKey, inxStart,
							inxEnd);
					uploadStream.write(bytes);

					if (bytes.length < 1024)
						flag = true;

					inxStart = inxEnd + 1;
					inxEnd += 1025;

				} catch (Exception e) {
					flag = true;
				}

			} while (!flag);
			logger.log(Level.SEVERE, "Calling Email Method  :::: ");
		}
	}

	private void initializeServicePo() {

		// Vendor email id is added to emailList
		ArrayList<String> toEmailList = new ArrayList<String>();
		String purchaseEngineerEmailId = null;
		if (emp != null) {
			purchaseEngineerEmailId = emp.getEmail();
		}
		if (emp != null) {
			System.out.println(" purchase engineer id===="
					+ purchaseEngineerEmailId);
			toEmailList.add(purchaseEngineerEmailId);
		}

		// Table header 1
		ArrayList<String> tbl1_header = new ArrayList<String>();
		tbl1_header.add("ID");
		tbl1_header.add("Code");
		tbl1_header.add("Product Name");
		tbl1_header.add("Product Category");
		tbl1_header.add("Product Quantity");
		tbl1_header.add("UOM");
		System.out.println("Tbl1 header Size :" + tbl1_header.size());

		List<SalesLineItem> productList = new ArrayList<SalesLineItem>();
		productList = servicPo.getItems();
		// Table 1
		ArrayList<String> tbl1 = new ArrayList<String>();
		for (int i = 0; i < productList.size(); i++) {
			tbl1.add(productList.get(i).getPrduct().getCount() + "");
			tbl1.add(productList.get(i).getProductCode());
			tbl1.add(productList.get(i).getProductName());
			tbl1.add(productList.get(i).getProductCategory());
			tbl1.add(productList.get(i).getQty() + "");
			tbl1.add(productList.get(i).getUnitOfMeasurement());
		}
		System.out.println("Tbl1 Size :" + tbl1.size());
		// Table 2 header
		ArrayList<String> tbl2_header = new ArrayList<String>();
		tbl2_header.add("Sr. No.");
		tbl2_header.add("Days");
		tbl2_header.add("Percent");
		tbl2_header.add("Comment");
		System.out.println("Tbl2 header Size :" + tbl2_header.size());

		List<PaymentTerms> paymentTermsList = new ArrayList<PaymentTerms>();
		paymentTermsList = servicPo.getPaymentTermsList();
		// Table 2
		ArrayList<String> tbl2 = new ArrayList<String>();
		for (int i = 0; i < paymentTermsList.size(); i++) {
			tbl2.add((i + 1) + "");
			tbl2.add(paymentTermsList.get(i).getPayTermDays() + "");
			tbl2.add(paymentTermsList.get(i).getPayTermPercent() + "");
			tbl2.add(paymentTermsList.get(i).getPayTermComment());
		}
		System.out.println("Tbl2 Size :" + tbl2.size());

		String header = "";
		String footer = "";
		// String header=po.getHeader().toString().trim();
		// String footer=po.getFooter().toString().trim();

		String uploadDocName = null;
		if (servicPo.getUpload() != null
				&& !servicPo.getUpload().getUrl().equals("")) {
			uploadDocName = servicPo.getUpload().getName();
		}

		String[] fileName = { servicPo.getCount() + "Service PO" };
		System.out.println("Header: " + header + " Footer: " + footer);
		try {
			System.out.println("Inside try calling Method...");
			sendNewEmail(toEmailList, "Service PO", "Service PO",
					servicPo.getCount(), servicPo.getServicePoTitle(),
					servicPo.getServicePoDate(), comp, "Product Details",
					tbl1_header, tbl1, "Payment Terms", tbl2_header, tbl2,
					header, note, footer, fileName, uploadDocName,null,null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Date 27 Feb 2017 by vijay for Send Email from Customer support To
	 * complaint Assigned Email in html context
	 */

	@Override
	public void emailSend(long companyId, Complain model, ArrayList<String> toEmail,
			String mailSubject, String msgBody,
			ArrayList<String> table1_Header, ArrayList<String> table1) {

		Company comp = ofy().load().type(Company.class)
				.filter("companyId", companyId).first().now();
		/**
		 * @author Vijay Chougule Date 28-12-2020
		 * Des :- Email From Sales Person and CC in Approver Name and Branch Email id and Company Email Id
		 * Sending Emails from Sengrid
		 */
		ArrayList<String> ccEmailList = new ArrayList<String>();
		String fromEmailId = "";
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.PC_ENABLESENDEMAILSFROMPERSONRESPONSIBLE, model.getCompanyId())) {
			ccEmailList.add(comp.getEmail());
			fromEmailId = getSalesPersonEmail(model.getSalesPerson(),model.getCompanyId(),model.getBranch());
			if(fromEmailId==null) {
				fromEmailId = comp.getEmail();
			}
			String branchEmailId = getBranchEmailId(model.getBranch(), model.getCompanyId());
			if(branchEmailId!=null) {
				ccEmailList.add(branchEmailId);
			}
			Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", model.getCompanyId())
									.filter("count", model.getExistingContractId()).first().now();
			logger.log(Level.SEVERE,"contractEntity"+contractEntity);
			if(contractEntity!=null) {
				if(contractEntity.getAccountManager()!=null) {
					logger.log(Level.SEVERE,"contractEntity getAccountManager "+contractEntity.getAccountManager());
					User userEntity = ofy().load().type(User.class).filter("companyId",model.getCompanyId())
										.filter("employeeName", contractEntity.getAccountManager()).first().now();
					logger.log(Level.SEVERE,"userEntity"+userEntity);
					if(userEntity!=null && userEntity.getEmail()!=null && !userEntity.getEmail().equals("")) {
						ccEmailList.add(userEntity.getEmail());
						logger.log(Level.SEVERE,"userEntity.getEmail()"+userEntity.getEmail());

					}
				}
			}
		}
		/**
		 * ends here
		 */
		
		
		
		htmlformatsendEmail(toEmail, mailSubject, "", comp, msgBody, null,
				null, table1_Header, table1,ccEmailList, fromEmailId);
	}

	/**
	 * Date 22 April 2017 added by vijay for EVA Clients Renewals Email send
	 */

	public void EVARenewalEmail(ArrayList<String> toEmailId,
			String mailSubject, String mailTitle, Company company, String msg,
			String msg2withRed, String msg3, ArrayList<String> table1_Header,
			ArrayList<String> table1, String paymnetinfo, String heading2,
			ArrayList<String> table2_Header, String msgbody2, String msgbody3,
			String mailHeader, String mailFooter, String[] attachFiles)
			throws IOException {

		logger.log(Level.SEVERE, "Entered In Common Method");

		ArrayList<String> toEmailIdForSendGrid= new ArrayList<String>(); //Ashwini Patil
		toEmailIdForSendGrid.addAll(toEmailId);
		logger.log(Level.SEVERE, "toEmailIdForSendGrid" + toEmailIdForSendGrid.toString());

		// to Email id's in array
		String[] toEmailIdArray = new String[toEmailId.size()];
		toEmailId.toArray(toEmailIdArray);
		System.out.println("Mail Sub : " + mailSubject);
		System.out.println("Mail Title : " + mailTitle);
		System.out.println("Mail Header : " + mailHeader);
		System.out.println("Mail Footer : " + mailFooter);

		logger.log(Level.SEVERE, "Mail Sub : " + mailSubject);
		logger.log(Level.SEVERE, "Mail Title : " + mailTitle);
		logger.log(Level.SEVERE, "Mail Footer : " + mailFooter);

		StringBuilder builder = new StringBuilder();

		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("<h3 class=\"ex\" align=\"center\">" + mailTitle
				+ "</h3></br></br>");

		// Mail header
		if (mailHeader != null) {
			builder.append("</br></br>");
			builder.append("<p>" + mailHeader + "</p>" + "</br></br>");
		}

		builder.append(" <p>  </p> </br></br>");
		builder.append(" <B>   </br></br></B>");
		builder.append(msg);
		builder.append(" <B>   </br></br></B>");

		if (msg2withRed != null) {
			builder.append("</br></br><p> <font color=red> " + msg2withRed
					+ " </font></p></br></br>");
		}

		if (msg3 != null)
			builder.append(msg3);

		/************************ Table 1 ***************************************/

		if (table1 != null && !table1.isEmpty()) {
			System.out.println("Table  1");
			// builder.append("<h2>"+heading1+"</h2>");

			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
			builder.append("<tr>");

			// Product Table1 heading
			for (String header : table1_Header)
				builder.append("<th>" + header + "</th>");
			builder.append("</tr>");

			System.out.println("Size of table 1 " + table1.size());

			builder.append("<tr>");
			for (int i = 0; i < table1.size(); i++) {

				if (i == table1.size() - 3) {
					builder.append("<td colspan=6>" + table1.get(i) + "</td>");
					System.out.println("Table Started 1" + table1.get(i));
					if (i > 0 && (i + 1) % table1_Header.size() == 0) {
						builder.append("</tr><tr>");
					}
				} else if (i == table1.size() - 2) {
					builder.append("<td>" + table1.get(i) + "</td>");
					System.out.println("Table Started 1" + table1.get(i));
					if (i > 0 && (i + 1) % table1_Header.size() == 0) {
						builder.append("</tr><tr>");
					}
				} else if (i == table1.size() - 1) {
					builder.append("<tr>");
					builder.append("<td colspan=7>" + table1.get(i) + "</td>");
					System.out.println("Table Started 1" + table1.get(i));
					if (i > 0 && (i + 1) % table1_Header.size() == 0) {
						builder.append("</tr><tr>");
					}
				} else {
					builder.append("<td>" + table1.get(i) + "</td>");
					System.out.println("Table Started 1" + table1.get(i));
					if (i > 0 && (i + 1) % table1_Header.size() == 0) {
						builder.append("</tr><tr>");
					}
				}
				// if(i==table1.size()-1){
				// builder.append("<tr>");
				// builder.append("<td> Net Payable </td>");
				// }
			}
			// if(k==productList.size()-1){
			builder.append(" </tr>  ");
			builder.append("</table></b></br>");
		}

		if (paymnetinfo != null) {
			builder.append(" <p>  </p> </br></br>");
			builder.append(" <B>   </br></br></B>");
			builder.append(paymnetinfo);
			builder.append(" <B>   </br></br></B>");
		}

		/************************************* Table 2 ************************************/

		// if(table2!=null &&!table2.isEmpty())
		// {
		System.out.println("Table 2");
		builder.append("<h2> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Payment Modes</h2>");

		builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\"  Style width=\"50%\" >");
		builder.append("<col width=\"1%\" >");
		builder.append("<col width=\"2%\" >");
		builder.append("<col width=\"3%\" >");
		builder.append("<col width=\"10%\">");

		builder.append("<tr>");
		// Product Table2 heading
		if (table2_Header != null) {
			for (String header : table2_Header)
				builder.append("<th>" + header + "</th>");

		}

		builder.append("</tr>");

		builder.append("<tr>");
		// for (int i = 0; i < table2.size(); i++) {
		// builder.append("<td style=\"width:5%\">" + table2.get(i) + "</td>");
		// System.out.println("Table Started 2 :"+table2.get(i));
		// if(i>0&&(i+1)%table2_Header.size()==0){
		// builder.append("</tr><tr>");
		// }
		// }
		// for (int i = 0; i < 6; i++) {
		builder.append("<td style=\"width:8%\">  Cheque/Demand Draft/Pay Order </td>");
		builder.append("<td style=\"width:8%\">  EVA SOFTWARE SOLUTIONS <br><br> Payable at MUMBAI </td>");
		builder.append("<td style=\"width:12%\">  1. Send via Courier to our office address <br>"
				+ "#1, Monginis Cake Shop, Hariyali Village,<br>"
				+ "Station Road,Tagore Nagar, Vikhroli East, MUM - 400083 <br><br>"
				+ "2.Ensure Your Company Name is mentioned on the back of the cheque </td>");
		builder.append("</tr><tr>");

		builder.append("<td style=\"width:8%\"> RTGS/NEFT </td>");
		builder.append("<td style=\"width:8%\"> EVA SOFTWARE SOLUTIONS </td>");
		builder.append("<td style=\"width:12%\"> <B>Account Number</B> 50200003745466 <br><br>"
				+ "<B> Bank Name </B> HDFC BANK<br><br>"
				+ "Dheeraj Kawal, Ground Floor, LBS Marg,<br> "
				+ "Vikhroli West, Mumbai - 400079 <br><br>"
				+ " IFSC/RTGS/NEFT Code : HDFC0000998 <br>"
				+ "MICR Code : 400240106 </td>");
		builder.append("</tr><tr>");
		builder.append("<td colspan=3> We request you to send the online transaction details to sales@evasoftwaresolutions.com immediately after making payments to help us track the same receipts instantly </td>");
		builder.append(" </tr>  ");
		builder.append("</table></b></br>");
		// }

		/************************************ Note *******************************************/
		if (msgbody2 != null) {
			builder.append("</br></br><p> <font color=red> " + msgbody2
					+ " </font></p></br></br>");
		}

		if (msgbody3 != null) {
			builder.append("</br></br><p>" + msgbody3 + "</p></br></br>");
		}
		/**************************** FOOTER Details *********************************/
		if (mailFooter != null) {
			builder.append("<p><B>" + mailFooter + "</B></p></br></b></br>");
		}

		// Company Details
		builder.append("<br>");
		builder.append("</br>");
		builder.append("<b>" + company.getBusinessUnitName() + "</b></br>");
		builder.append("<table>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Address : ");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getAddrLine2().equals("")
				&& company.getAddress().getAddrLine2() != null)
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getAddrLine2() + " "
					+ company.getAddress().getLocality());
		else
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getLocality());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getLandmark().equals("")
				&& company.getAddress().getLandmark() != null)
			builder.append(company.getAddress().getLandmark() + ","
					+ company.getAddress().getCity() + ","
					+ company.getAddress().getPin()
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		else
			builder.append(company.getAddress().getCity() + ","
					+ company.getAddress().getPin() + ","
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Website : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getWebsite());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Phone No : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getCellNumber1());
		builder.append("</td>");
		builder.append("<tr>");

		builder.append("</tr>");
		builder.append("</table>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);

		/************************************* Mail Send Logic *********************************/
		
		ArrayList<String> ccEmailList = new ArrayList<String>();
		/**20-5-2020 by Amol Comment this mail as per Nitin Sir****/
//		ccEmailList.add("sales@evasoftwaresolutions.com");
		/** Date 05-01-2019 by vijay as per nitin sir i have removed the the below gmail from cc ***/
//		ccEmailList.add("kshirsagarnitin@gmail.com");
//		ccEmailList.add("support@evasoftwaresolutions.com");
		ccEmailList.add("evasoftwaresolutionsfin@gmail.com");//Added by Ashwini Patil

		String[] ccEmailIdArray = new String[ccEmailList.size()];
		ccEmailList.toArray(ccEmailIdArray);
		
		
		/**
		 * @author Ashwini Patil
		 * @since 03-05-2022
		 * Contract renewal mails were not getting sent through gmail due to insufficient quota.
		 * So sending mails through send grid
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, company.getCompanyId())){
			logger.log(Level.SEVERE,"in Send Grid call: company email --" +company.getEmail() +"  to email size " + toEmailIdForSendGrid.size()+toEmailIdArray.toString()+"cclist size="+ccEmailList.size());
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			sdEmail.sendMailWithSendGrid(company.getEmail(),toEmailIdForSendGrid, ccEmailList, null, mailSubject, contentInHtml, "text/html",null,null,null,company.getDisplayNameForCompanyEmailId());	
			return;
		}

		try {
			System.out.println("Inside Try Block");
			Properties props = new Properties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", "smtp.gmail.com");
			Session session = Session.getDefaultInstance(props);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company.getEmail());
			logger.log(Level.SEVERE, "Company Email" + company.getEmail());

			// To Mail Address
			message.setFrom(new InternetAddress(company.getEmail()));
			message.setReplyTo(new InternetAddress[] { new InternetAddress(
					company.getEmail()) });

			// Email to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);
				logger.log(Level.SEVERE, "Email id multipart "
						+ mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);
			// Email CC to Multiple Recipients
			InternetAddress[] ccmailToMultiple = new InternetAddress[ccEmailIdArray.length];
			for (int i = 0; i < ccEmailIdArray.length; i++) {
				ccmailToMultiple[i] = new InternetAddress(ccEmailIdArray[i]);
				System.out.println("Email id multipart " + ccmailToMultiple[i]);
				logger.log(Level.SEVERE, "CC Email id multipart "
						+ ccmailToMultiple[i]);
			}

			// message.setRecipient(Message.RecipientType.CC, new
			// InternetAddress("sales@evasoftwaresolutions.com"));
			message.setRecipients(Message.RecipientType.CC, ccmailToMultiple);

			// Mail Subject
			message.setSubject(mailSubject);

			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentInHtml, "text/html");
			multiPart.addBodyPart(htmlpart);

			// Mail attachment
			if (attachFiles != null) {
				MimeBodyPart attachbody = new MimeBodyPart();
				System.out.println("Inside Attachment.........");
				logger.log(Level.SEVERE, "Inside Attachment.........");
				attachbody.setFileName(attachFiles[0] + ".pdf");
				DataSource src = new ByteArrayDataSource(
						pdfstream.toByteArray(), "application/pdf");
				attachbody.setDataHandler(new DataHandler(src));
				multiPart.addBodyPart(attachbody);

			}

			System.out.println("Ready To Send Mail");
			logger.log(Level.SEVERE, "Ready To Send Mail" + multiPart);
			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			logger.log(Level.SEVERE, "Email sent Successfully !!!!");
			System.out.println("Email sent Successfully !!!!");
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Exception while sending Email" + e);
		}

	}

	/**
	 * ends here
	 */

	/**
	 * Created By : nidhi Date : 18-11-2017 Dicription : Method updated for file
	 * attanchment
	 * 
	 */

	/**
	 * Created By : nidhi Date : 18-11-2017 Dicription : Method updated for file
	 * attanchment
	 * 
	 */

	public void cronSendEmail(ArrayList<String> toEmailId, String mailSubject,
			String mailTitle, Company company, ArrayList<String> table1_Header,
			ArrayList<String> table1, String heading2,
			ArrayList<String> table2_Header, ArrayList<String> table2,
			String[] attachFiles, ByteArrayOutputStream bytarr, String fileType,String footer)
			throws IOException {

		logger.log(Level.SEVERE, "Entered In Common Method");

		String[] toEmailIdArray = new String[toEmailId.size()];
		toEmailId.toArray(toEmailIdArray);

		StringBuilder builder = new StringBuilder();

		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("<h3 class=\"ex\" align=\"left\">" + mailTitle
				+ "</h3></br></br>");

		/************************ Table 1 ***************************************/

		if (table1 != null && !table1.isEmpty()) {
			System.out.println("Table  1");
			// builder.append("<h2>"+heading1+"</h2>");

			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
			builder.append("<tr>");

			for(int i=0;i<table1_Header.size();i++)
			{
				if(!table1_Header.get(i).equals("$#") && !table1_Header.get(i).equals("$##")){
					if(table1_Header.get(i+1).equals("$#")){
						
						builder.append("<th colspan=\"2\">" + table1_Header.get(i) + "</th>");
					}else if(table1_Header.get(i+1).equals("$##")){
						builder.append("<th rowspan=\"2\">" + table1_Header.get(i) + "</th>");
					}else{
					
						builder.append("<th>" +  table1_Header.get(i) + "</th>");
					}
				}
						
			}
			
			table1_Header.remove("$#");
			table1_Header.remove("$##");
				
			builder.append("</tr>");

			System.out.println("Size of table 1 " + table1.size());

			builder.append("<tr>");
			for (int i = 0; i < table1.size(); i++) {
				if(!table1.get(i).equals("##")){
					builder.append("<td>" + table1.get(i) + "</td>");
				}
				
				System.out.println("Table Started 1" + table1.get(i));
				if (i > 0 && (i + 1) % table1_Header.size() == 0) {
					builder.append("</tr><tr>");

				}
			}
			builder.append(" </tr>  ");
			builder.append("</table></b></br>");
		}

		/************************************* Table 2 ************************************/

		if (table2 != null && !table2.isEmpty()) {
			System.out.println("Table 2");
			builder.append("<h2>" + heading2 + "</h2>");

			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"1\"  Style width=\"50%\" >");
			builder.append("<col width=\"1%\" >");
			builder.append("<col width=\"2%\" >");
			builder.append("<col width=\"3%\" >");
			builder.append("<col width=\"10%\">");

			builder.append("<tr>");
			// Product Table2 heading
			for (String header : table2_Header)
				builder.append("<th>" + header + "</th>");

			builder.append("</tr>");

			System.out.println("Size of list " + table2.size());
			builder.append("<tr>");
			for (int i = 0; i < table2.size(); i++) {
				builder.append("<td style=\"width:5%\">" + table2.get(i)
						+ "</td>");
				// if(i==6){
				// builder.append("<td style=\"width:20%\">" + table2.get(i) +
				// "</td>");
				// }
				System.out.println("Table Started 2 :" + table2.get(i));
			}
			builder.append(" </tr>  ");
			builder.append("</table></b></br>");
		}

		/**************************** FOOTER Details *********************************/
		 if(footer!=null ){
		 builder.append("<p>"+footer+"</p></br></b></br>");
		 }

		// Company Details
		builder.append("<br>");
		builder.append("<b>" + company.getBusinessUnitName() + "</b></br>");
		builder.append("<table>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Address : ");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getAddrLine2().equals("")
				&& company.getAddress().getAddrLine2() != null)
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getAddrLine2() + " "
					+ company.getAddress().getLocality());
		else
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getLocality());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getLandmark().equals("")
				&& company.getAddress().getLandmark() != null)
			builder.append(company.getAddress().getLandmark() + ","
					+ company.getAddress().getCity() + ","
					+ company.getAddress().getPin()
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		else
			builder.append(company.getAddress().getCity() + ","
					+ company.getAddress().getPin() + ","
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Website : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getWebsite());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Phone No : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getCellNumber1());
		builder.append("</td>");
		builder.append("<tr>");

		builder.append("</tr>");
		builder.append("</table>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);
		
		/**
		 * @author Anil,Date : 09-02-2019
		 * If Process Type - EnableSendGridEmailApi is active then we will send email through SendGrid else through default mail i.r. JavaMail
		 * As appengine has limited daily mail quota i.e. 100, to overcome this limited quota issue we are implementing SendGrid
		 * raised By Rohan for Pecopp
		 */
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CronJob", "EnableSendGridEmailApi",company.getCompanyId())){
			logger.log(Level.SEVERE," company email --" +company.getEmail() +"  to email  " + toEmailIdArray.toString());
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			sdEmail.sendMailWithSendGrid(company.getEmail(), toEmailId, null, null, mailSubject, contentInHtml, "text/html",bytarr,attachFiles[0] + ".xlsx","application/vnd.ms-excel",company.getDisplayNameForCompanyEmailId());
			
			return;
		}

		/************************************* Mail Send Logic *********************************/
		try {
			// System.out.println("Inside Try Block");
			// Properties props=new Properties();
			// Session session=Session.getDefaultInstance(props,null);

			System.out.println("Inside Try Block");
			Properties props = new Properties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", "smtp.gmail.com");
			Session session = Session.getDefaultInstance(props);

			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company.getEmail());

			// To Mail Address
			message.setFrom(new InternetAddress(company.getEmail()));
			// message.setFrom(new
			// InternetAddress("evasoftwaresolutionserp@gmail.com"));
			message.setReplyTo(new InternetAddress[] { new InternetAddress(
					company.getEmail()) });

			// logger.log(Level.SEVERE,"To Email Address"+company.getEmail());

			// Email to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);

				logger.log(Level.SEVERE, "To Email Address" + mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);
			// message.setRecipient(Message.RecipientType.CC, new
			// InternetAddress(company.getEmail()));

			// logger.log(Level.SEVERE,"To Email Address"+company.getEmail());

			// Mail Subject
			message.setSubject(mailSubject);

			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentInHtml, "text/html");
			multiPart.addBodyPart(htmlpart);

			if (attachFiles != null) {

				MimeBodyPart attachbody = new MimeBodyPart();
				System.out.println("Inside Attachment.........");
				logger.log(Level.SEVERE, "Inside Attachment.........");
				attachbody.setFileName(attachFiles[0] + ".xlsx");
				DataSource src = new ByteArrayDataSource(bytarr.toByteArray(),
						"application/vnd.ms-excel");
				attachbody.setDataHandler(new DataHandler(src));
				multiPart.addBodyPart(attachbody);

			}

			// Mail attachment
			// if(attachFiles!=null){
			// System.out.println("Inside Attachment.........");
			// MimeBodyPart attachbody=new MimeBodyPart();
			//
			// attachbody.setFileName(attachFiles[0]+".pdf");
			// DataSource src=new ByteArrayDataSource(bytarr.toByteArray(),
			// "application/pdf");
			// attachbody.setDataHandler(new DataHandler(src));
			// System.out.println("Attach File Name");
			// multiPart.addBodyPart(attachbody);
			//
			// // adds attachments
			// if (attachFiles != null && attachFiles.length > 0) {
			// System.out.println("More");
			// for (String filePath : attachFiles) {
			// MimeBodyPart attachPart = new MimeBodyPart();
			// try {
			// attachPart.attachFile(filePath);
			// } catch (IOException ex) {
			// ex.printStackTrace();
			// }
			// multiPart.addBodyPart(attachPart);
			// }
			// }
			// }

			System.out.println("Ready To Send Mail");

			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			System.out.println("Email sent Successfully !!!!");
			logger.log(Level.SEVERE, "Email sent Successfully !!");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void emailToCustomerFromLead(Lead lead) {

		try {

			introLead  = lead;
			if (lead.getCompanyId() != null) {
				comp = ofy().load().type(Company.class).id(lead.getCompanyId())
						.now();
			} else {
				comp = ofy().load().type(Company.class).first().now();
			}

				logger.log(Level.SEVERE, "before To Call Common Method");

				

//				sendLeadEmail("Request for Lead : " + lead.getCount(), 0, null,null, null, message11, comp.getEmail(), toEmailList, null);
			
				createLeadUploadDocumentAttachment();
				initializeLeadIntroMailToEmail();
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Error in introductory mail sending.");
		}
	}
	
	private void createLeadUploadDocumentAttachment()
	{
		blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		uploadStream = new ByteArrayOutputStream();
		
		String urlData="";
		String fileName="";
		
		
		if(introLead.getDocument() != null && !introLead.getDocument().getUrl().equals("")){
			urlData=introLead.getDocument().getUrl();
			logger.log(Level.SEVERE,"Doc URL :::: "+urlData);
			
			/**9-11 -2017 sagar sore[filename assigned at different place]**/
//			fileName=salesquot.getDocument().getName();
//			logger.log(Level.SEVERE,"Document Name :::: "+fileName);
			
			String[] res=urlData.split("blob-key=",0);
			logger.log(Level.SEVERE,"Splitted Url :::: "+res);
			
			String blob =res[1].trim();
			logger.log(Level.SEVERE,"Splitted Url Assigned to string  :::: "+blob);
			
			BlobKey blobKey=null;
			try{
				blobKey = new BlobKey(blob);
			}
			catch(IllegalArgumentException e){
				e.printStackTrace();
			}
			logger.log(Level.SEVERE,"BlobKey  :::: "+blobKey);
			
			// This method read bytes from blobstore
			long inxStart = 0;
	        long inxEnd = 1024;
	        boolean flag = false;
	        byte[] bytes=null;
	        do {
	            try {
	                bytes = blobstoreService.fetchData(blobKey,inxStart,inxEnd);
	                uploadStream.write(bytes);

	                if (bytes.length < 1024)
	                    flag = true;

	                inxStart = inxEnd + 1;
	                inxEnd += 1025;

	            } catch (Exception e) {
	                flag = true;
	            }

	        } while (!flag);
		logger.log(Level.SEVERE,"Calling Email Method  :::: ");
		}
	}
	
	
	private void initializeLeadIntroMailToEmail() {

		ArrayList<String> toEmailList = new ArrayList<String>();
		
//		Customer cust =  ofy().load().type(Customer.class).id(introLead.getCompanyId())
//				.now();
		c = ofy().load().type(Customer.class)
				.filter("count", introLead.getPersonInfo().getCount())
				.filter("companyId", introLead.getCompanyId()).first()
				.now();
		toEmailList.add(c.getEmail());
		
		logger.log(Level.SEVERE, "Ready To Call Common Method"+ toEmailList.size());
		
		//Table header 1
		ArrayList<String> tbl1_header=new ArrayList<String>();
   	 	tbl1_header.add("Code");
   	 	tbl1_header.add("Product Name");
   	 	tbl1_header.add("Product Category");
   	 	tbl1_header.add("Product Quantity");
   	 	tbl1_header.add("Rate");
//   	 	tbl1_header.add("VAT");
//   	 	tbl1_header.add("Service Tax");
   	 	System.out.println("Tbl1 header Size :"+tbl1_header.size());
   	 	
		List<SalesLineItem> productList=new ArrayList<SalesLineItem>();
   	 	productList=introLead.getLeadProducts();
   	 	// Table 1
   	 	ArrayList<String> tbl1=new ArrayList<String>();
   	 	for(int i=0;i<productList.size();i++){
   	 		tbl1.add(productList.get(i).getProductCode());
   	 		tbl1.add(productList.get(i).getProductName());
   	 		tbl1.add(productList.get(i).getProductCategory());
   	 		tbl1.add(productList.get(i).getQty()+"");
   	 		tbl1.add(productList.get(i).getPrice()+"");
//   	 		tbl1.add(productList.get(i).getVatTax().getPercentage()+"");
//   	 		tbl1.add(productList.get(i).getServiceTax().getPercentage()+"");
   	 	}
   	 	System.out.println("Tbl1 Size :"+tbl1.size());
   	 	// Table 2 header
   	 	ArrayList<String> tbl2_header=new ArrayList<String>();
   	 	tbl2_header.add("Sr. No.");
   	 	tbl2_header.add("Days");
   	 	tbl2_header.add("Percent");
   	 	tbl2_header.add("Comment");
	 	System.out.println("Tbl2 header Size :"+tbl2_header.size());
	 	
	 	
	 	
	 	/**4-11-2017 sagar sore[ commented and added new code to send attachments]**/
	 	String uploadDocName=null;
	 	if (introLead.getDocument() != null && !introLead.getDocument().getUrl()
						.equals("")) {
	 		uploadTandC = introLead.getDocument().getName();
		}
		
//		if (servquot.getDocument() != null
//				&& servquot.getDocument().getUrl() != null) {
//			uploadTandC = servquot.getDocument().getName();
//		}
		logger.log(Level.SEVERE, "info**************" + introLead.getCount()
				+ " " + introLead.getPersonInfo().getFullName());

		String[] fileName = { introLead.getCount() + " "
				+ introLead.getPersonInfo().getFullName() };

			try {
				
				 if(introLead.getDocument() != null)
				 {
					 logger.log(Level.SEVERE, "Inside try calling Method...  t&c with original doc");
				 
				sendNewEmail(toEmailList, "Lead", "Lead",
						introLead.getCount(), "Service  Lead",
						introLead.getCreationDate(), comp, "Product Details",
						tbl1_header, tbl1, "Payment Terms", tbl2_header, null,
						null, null, null, null, uploadTandC,null,null);
				 }
				 else
				 {
					 logger.log(Level.SEVERE, "Inside try calling Method...  original doc");
					 sendNewEmail(toEmailList, "Lead", "Lead",
								introLead.getCount(), "Service  Lead",
								introLead.getCreationDate(), comp, "Product Details",
								tbl1_header, tbl1, "Payment Terms", tbl2_header, null,
								null, null, null, fileName, null,null,null);
				 }
			} catch (IOException e) {
				e.printStackTrace();
			}
		

	}
	
	

	/**
	 * Date 22-12-2017
	 *  added by vijay for Payment remainder and TDS remainder Email to client
	 */

	public void PaymentReminderCronJobEmailToClient(ArrayList<String> toEmailId,
			String mailSubject, String mailTitle, Company company, String msg,
			String msg2withRed, String msg3, ArrayList<String> table1_Header,
			ArrayList<String> table1, String paymnetinfo, String heading2,
			ArrayList<String> table2_Header, String msgbody2, String msgbody3,
			String mailHeader, String mailFooter, String[] attachFiles, boolean tbl1clspanfalg, boolean paymentmodeTableflag)
			throws IOException {

		logger.log(Level.SEVERE, "Entered In Common Method");

		// to Email id's in array
		String[] toEmailIdArray = new String[toEmailId.size()];
		toEmailId.toArray(toEmailIdArray);
		System.out.println("Mail Sub : " + mailSubject);
		System.out.println("Mail Title : " + mailTitle);
		System.out.println("Mail Header : " + mailHeader);
		System.out.println("Mail Footer : " + mailFooter);

		logger.log(Level.SEVERE, "Mail Sub : " + mailSubject);
		logger.log(Level.SEVERE, "Mail Title : " + mailTitle);
		logger.log(Level.SEVERE, "Mail Footer : " + mailFooter);

		StringBuilder builder = new StringBuilder();

		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

//		builder.append("<h3 class=\"ex\" align=\"center\">" + mailTitle
//				+ "</h3></br></br>");

		// Mail header
		if (mailHeader != null) {
			builder.append("</br></br>");
			builder.append("<p>" + mailHeader + "</p>" + "</br></br>");
		}

		builder.append(" <p>  </p> </br></br>");
		builder.append(" <B>   </br></br></B>");
		builder.append(msg);
		builder.append(" <B>   </br></br></B>");

		if (msg2withRed != null) {
			builder.append("</br></br><p> <font color=red> " + msg2withRed
					+ " </font></p></br></br>");
		}

		if (msg3 != null)
			builder.append(msg3);

		/************************ Table 1 ***************************************/

		if (table1 != null && !table1.isEmpty()) {
			System.out.println("Table  1");
			// builder.append("<h2>"+heading1+"</h2>");

			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
			builder.append("<tr>");

			// Product Table1 heading
			for (String header : table1_Header)
				builder.append("<th>" + header + "</th>");
			builder.append("</tr>");

			System.out.println("Size of table 1 " + table1.size());

			builder.append("<tr>");
			for (int i = 0; i < table1.size(); i++) {

				if(tbl1clspanfalg){ // for Payment reminder last row col span
					
					if (i == table1.size() - 2) {
						builder.append("<td colspan=6>" + table1.get(i) + "</td>");
						System.out.println("Table Started 1" + table1.get(i));
						if (i > 0 && (i + 1) % table1_Header.size() == 0) {
							builder.append("</tr><tr>");
						}
					}
					else {
						builder.append("<td>" + table1.get(i) + "</td>");
						System.out.println("Table Started 1" + table1.get(i));
						if (i > 0 && (i + 1) % table1_Header.size() == 0) {
							builder.append("</tr><tr>");
						}
					}
				}else{ // for TDS normal table 
					builder.append("<td>" + table1.get(i) + "</td>");
					System.out.println("Table Started 1" + table1.get(i));
					if (i > 0 && (i + 1) % table1_Header.size() == 0) {
						builder.append("</tr><tr>");
					}
				}
				
				// if(i==table1.size()-1){
				// builder.append("<tr>");
				// builder.append("<td> Net Payable </td>");
				// }
			}
			// if(k==productList.size()-1){
			builder.append(" </tr>  ");
			builder.append("</table></b></br>");
		}

		if (paymnetinfo != null) {
			builder.append(" <p>  </p> </br></br>");
			builder.append(" <B>   </br></br></B>");
			builder.append(paymnetinfo);
			builder.append(" <B>   </br></br></B>");
		}

		/************************************* Table 2 ************************************/

		 if(paymentmodeTableflag)
		 {
		System.out.println("Table 2");
		builder.append("<h2> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Payment Modes</h2>");

		builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\"  Style width=\"50%\" >");
		builder.append("<col width=\"1%\" >");
		builder.append("<col width=\"2%\" >");
		builder.append("<col width=\"3%\" >");
		builder.append("<col width=\"10%\">");

		builder.append("<tr>");
		// Product Table2 heading
		if (table2_Header != null) {
			for (String header : table2_Header)
				builder.append("<th>" + header + "</th>");

		}

		builder.append("</tr>");

		builder.append("<tr>");
		// for (int i = 0; i < table2.size(); i++) {
		// builder.append("<td style=\"width:5%\">" + table2.get(i) + "</td>");
		// System.out.println("Table Started 2 :"+table2.get(i));
		// if(i>0&&(i+1)%table2_Header.size()==0){
		// builder.append("</tr><tr>");
		// }
		// }
		// for (int i = 0; i < 6; i++) {
		builder.append("<td style=\"width:8%\">  Cheque/Demand Draft/Pay Order </td>");
		builder.append("<td style=\"width:8%\">  "+company.getBusinessUnitName()+" <br><br> Payable at "+company.getAddress().getCity()+" </td>");
		builder.append("<td style=\"width:12%\">  1. Send via Courier to our office address <br>"
				+ company.getAddress().getAddrLine1()+",<br>"
				+ company.getAddress().getAddrLine2()+","+company.getAddress().getLandmark()+","
				+company.getAddress().getLocality()+","+company.getAddress().getPin()+"<br><br>"
				+ "2.Ensure Your Company Name is mentioned on the back of the cheque </td>");
		builder.append("</tr><tr>");

		//Ashwini Patil Date:10-1-2023 Wrong payment details were getting printed so selecting default payment.
		CompanyPayment companyPayment = ofy().load().type(CompanyPayment.class)
				.filter("companyId", company.getCompanyId())
				.filter("paymentDefault", true).first().now();
		
		if(companyPayment.getPaymentAccountNo()!=null && !companyPayment.getPaymentAccountNo().equals("")){
			
			builder.append("<td style=\"width:8%\"> RTGS/NEFT </td>");
			builder.append("<td style=\"width:8%\">"+company.getBusinessUnitName()+"</td>");
			builder.append("<td style=\"width:12%\"> <B>Account Number</B> "+companyPayment.getPaymentAccountNo()+" <br><br>"
					+ "<B> Bank Name </B>"+ companyPayment.getPaymentBankName() +"<br><br>"
					+ companyPayment.getAddressInfo().getAddrLine1()+",<br> "
					+ companyPayment.getAddressInfo().getAddrLine2()+","+companyPayment.getAddressInfo().getLandmark()+","
					+companyPayment.getAddressInfo().getLocality()+","+companyPayment.getAddressInfo().getPin()+"<br><br>"
					+ " IFSC/RTGS/NEFT Code : "+companyPayment.getPaymentIFSCcode()+" <br>"
					+ "MICR Code : "+ companyPayment.getPaymentMICRcode()+ "</td>");
			builder.append("</tr><tr>");
			builder.append("<td colspan=3> We request you to send the online transaction details to "+company.getEmail()+" immediately after making payments to help us track the same receipts instantly </td>");
			builder.append(" </tr>  ");
			
		}
		
		builder.append("</table></b></br>");
		
		 }

		/************************************ Note *******************************************/
		if (msgbody2 != null) {
			builder.append("</br></br><p> <font color=red> " + msgbody2
					+ " </font></p></br></br>");
		}

		if (msgbody3 != null) {
			builder.append("</br></br><p>" + msgbody3 + "</p></br></br>");
		}
		/**************************** FOOTER Details *********************************/
		if (mailFooter != null) {
			builder.append("<p><B>" + mailFooter + "</B></p></br></b></br>");
		}

		// Company Details
		builder.append("<br>");
		builder.append("</br>");
		builder.append("<b>" + company.getBusinessUnitName() + "</b></br>");
		builder.append(ServerAppUtility.getCompanySignature(company,null));
//		builder.append("<table>");
//
//		builder.append("<tr>");
//		builder.append("<td>");
//		builder.append("Address : ");
//		builder.append("</td>");
//		builder.append("<td>");
//		if (!company.getAddress().getAddrLine2().equals("")
//				&& company.getAddress().getAddrLine2() != null)
//			builder.append(company.getAddress().getAddrLine1() + " "
//					+ company.getAddress().getAddrLine2() + " "
//					+ company.getAddress().getLocality());
//		else
//			builder.append(company.getAddress().getAddrLine1() + " "
//					+ company.getAddress().getLocality());
//		builder.append("</td>");
//		builder.append("</tr>");
//
//		builder.append("<tr>");
//		builder.append("<td>");
//		builder.append("</td>");
//		builder.append("<td>");
//		if (!company.getAddress().getLandmark().equals("")
//				&& company.getAddress().getLandmark() != null)
//			builder.append(company.getAddress().getLandmark() + ","
//					+ company.getAddress().getCity() + ","
//					+ company.getAddress().getPin()
//					+ company.getAddress().getState() + ","
//					+ company.getAddress().getCountry());
//		else
//			builder.append(company.getAddress().getCity() + ","
//					+ company.getAddress().getPin() + ","
//					+ company.getAddress().getState() + ","
//					+ company.getAddress().getCountry());
//		builder.append("</td>");
//		builder.append("</tr>");
//
//		builder.append("<tr>");
//		builder.append("<td>");
//		builder.append("Website : ");
//		builder.append("</td>");
//		builder.append("<td>");
//		builder.append(company.getWebsite());
//		builder.append("</td>");
//		builder.append("</tr>");
//
//		builder.append("<tr>");
//		builder.append("<td>");
//		builder.append("Phone No : ");
//		builder.append("</td>");
//		builder.append("<td>");
//		builder.append(company.getCellNumber1());
//		builder.append("</td>");
//		builder.append("<tr>");
//
//		builder.append("</tr>");
//		builder.append("</table>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);

		/************************************* Mail Send Logic *********************************/

		ArrayList<String> ccEmailList = new ArrayList<String>();
		ccEmailList.add(company.getEmail());

		String[] ccEmailIdArray = new String[ccEmailList.size()];
		ccEmailList.toArray(ccEmailIdArray);

		try {
			System.out.println("Inside Try Block");
			Properties props = new Properties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", "smtp.gmail.com");
			Session session = Session.getDefaultInstance(props);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company.getEmail());
			logger.log(Level.SEVERE, "Company Email" + company.getEmail());

			// To Mail Address
			message.setFrom(new InternetAddress(company.getEmail()));
			message.setReplyTo(new InternetAddress[] { new InternetAddress(
					company.getEmail()) });

			// Email to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);
				logger.log(Level.SEVERE, "Email id multipart "
						+ mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);
			// Email CC to Multiple Recipients
			InternetAddress[] ccmailToMultiple = new InternetAddress[ccEmailIdArray.length];
			for (int i = 0; i < ccEmailIdArray.length; i++) {
				ccmailToMultiple[i] = new InternetAddress(ccEmailIdArray[i]);
				System.out.println("Email id multipart " + ccmailToMultiple[i]);
				logger.log(Level.SEVERE, "CC Email id multipart "
						+ ccmailToMultiple[i]);
			}

			// message.setRecipient(Message.RecipientType.CC, new
			// InternetAddress("sales@evasoftwaresolutions.com"));
			message.setRecipients(Message.RecipientType.CC, ccmailToMultiple);

			// Mail Subject
			message.setSubject(mailSubject);

			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentInHtml, "text/html");
			multiPart.addBodyPart(htmlpart);

			// Mail attachment
			if (attachFiles != null) {
				MimeBodyPart attachbody = new MimeBodyPart();
				System.out.println("Inside Attachment.........");
				logger.log(Level.SEVERE, "Inside Attachment.........");
				attachbody.setFileName(attachFiles[0] + ".pdf");
				DataSource src = new ByteArrayDataSource(
						pdfstream.toByteArray(), "application/pdf");
				attachbody.setDataHandler(new DataHandler(src));
				multiPart.addBodyPart(attachbody);

			}

			System.out.println("Ready To Send Mail");
			logger.log(Level.SEVERE, "Ready To Send Mail" + multiPart);
			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			logger.log(Level.SEVERE, "Email sent Successfully !!!!");
			System.out.println("Email sent Successfully !!!!");
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Exception while sending Email" + e);
		}

	}

	/**
	 * ends here
	 */
	
	/******************************************** Common Email Method End *********************************************/

	// *****************************for email on request for
	// approval*********************************************

	public void sendEmailOnRequestForApproval(String mailSub, int docId,
			String docType, Date approveDate, String remark, String message1,
			String company, ArrayList<String> toEmailList,
			ArrayList<String> ccEmailList,ArrayList<String>header,ArrayList<String> tableDetails) throws IOException {

		// ***************************************for request for
		// approval*********************

		for (int i = 0; i < toEmailList.size(); i++) {
			for (int j = 0; j < ccEmailList.size(); j++) {
				if (toEmailList.get(i).contains(ccEmailList.get(j))) {
					ccEmailList.remove(j);
				}
			}
		}

		String[] toEmailIdArray = new String[toEmailList.size()];
		toEmailList.toArray(toEmailIdArray);

		String[] ccEmailIdArray = new String[ccEmailList.size()];
		ccEmailList.toArray(ccEmailIdArray);

		try {
			System.out.println("Inside Try Block");
			Properties props = new Properties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", "smtp.gmail.com");
			Session session = Session.getDefaultInstance(props);
			// Session session=Session.getDefaultInstance(props,null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company);

			// To Mail Address
//			message.setFrom(new InternetAddress(company));
			/**
			 * @author Anil , Date : 10-04-2020
			 * added display name for from email id
			 * ,company.getDisplayNameForCompanyEmailId()
			 */
			if(comp.getDisplayNameForCompanyEmailId()!=null&&!comp.getDisplayNameForCompanyEmailId().equals("")){
				message.setFrom(new InternetAddress(company,comp.getDisplayNameForCompanyEmailId()));
			}else{
				message.setFrom(new InternetAddress(company));
			}message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					company));

			// Email To to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);
				logger.log(Level.SEVERE, "To Email id multipart "
						+ mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);

			// Email CC to Multiple Recipients
			InternetAddress[] ccmailToMultiple = new InternetAddress[ccEmailIdArray.length];
			for (int i = 0; i < ccEmailIdArray.length; i++) {
				ccmailToMultiple[i] = new InternetAddress(ccEmailIdArray[i]);
				System.out.println("Email id multipart " + ccmailToMultiple[i]);
				logger.log(Level.SEVERE, "CC Email id multipart "
						+ ccmailToMultiple[i]);
			}
			logger.log(Level.SEVERE, "hi vijay 1");
			message.setRecipients(Message.RecipientType.CC, ccmailToMultiple);
			logger.log(Level.SEVERE, "hi vijay 2");
			// below line for add BCC if needed then uncomment
			// message.setRecipient(Message.RecipientType.BCC, new
			// InternetAddress(approverEmail));

			// Mail Subject
			logger.log(Level.SEVERE, "hi vijay 3");
			message.setSubject(mailSub);
			logger.log(Level.SEVERE, "hi vijay 4");
			
			
			StringBuilder builder =new StringBuilder();
			builder.append("<!DOCTYPE html>");
			builder.append("<html lang=\"en\">");
			builder.append("<head>");
			builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
			builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

			builder.append(message1);
			if (tableDetails != null && !tableDetails.isEmpty()) {
				System.out.println("Table  1");
				// builder.append("<h2>"+heading1+"</h2>");

				builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
				builder.append("<tr>");

				// Product Table1 heading
				for (String headerdt : header)
					builder.append("<th>" + headerdt + "</th>");
				builder.append("</tr>");

				System.out.println("Size of table 1 " + tableDetails.size());

				builder.append("<tr>");
				for (int i = 0; i < tableDetails.size(); i++) {
					builder.append("<td>" + tableDetails.get(i) + "</td>");
					System.out.println("Table Started 1" + tableDetails.get(i));
					if (i > 0 && (i + 1) % header.size() == 0) {
						builder.append("</tr><tr>");

					}
				}
				builder.append(" </tr>  ");
				builder.append("</table></b></br>");
			}
			
			
			
			builder.append("</body>");
			builder.append("</html>");
			
			/**
			 * @author Anil , Date : 27-03-2020
			 * If send grid account is active then approval mail will go from send gris mail api
			 * requirement raised by NBHC,Nitin Sir
			 */
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Approvals", "EnableSendGridEmailApi",comp.getCompanyId())){
				logger.log(Level.SEVERE," company email --" +company +"  to email  " + toEmailList +"EnableSendGridEmailApi");
				SendGridEmailServlet sdEmail=new SendGridEmailServlet();
				sdEmail.sendMailWithSendGrid(company, toEmailList, ccEmailList, null, mailSub, builder.toString(), "text/html",null,null,null,comp.getDisplayNameForCompanyEmailId());
				
				return;
			}
			logger.log(Level.SEVERE,"JAVAx mail API..." );
			/**
			 * 
			 */
			
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(builder.toString(), "text/html");
			multiPart.addBodyPart(htmlpart);
			message.setContent(multiPart);
			logger.log(Level.SEVERE, "hi vijay 5");
			// // Mail Body Content In html Form
			// MimeBodyPart htmlpart=new MimeBodyPart();
			// htmlpart.setContent(message1, "text");
			// multiPart.addBodyPart(htmlpart);

			System.out.println("Ready To Send Mail");
			logger.log(Level.SEVERE, "hi vijay 6 ready to send email");
			Transport.send(message);
			System.out.println("Email sent Successfully !!!!");
			logger.log(Level.SEVERE, "Email sent Successfully !!!!");
		} catch (Exception e) {
			logger.log(Level.SEVERE, "excption asfollows " + e);
			e.printStackTrace();

		}

	}
	
	public void cronSendEmail(ArrayList<String> toEmailId, String mailSubject,
			String mailTitle, Company company, ArrayList<String> table1_Header,
			ArrayList<String> table1, String heading2,
			ArrayList<String> table2_Header, ArrayList<String> table2,
			String[] attachFiles,String fromEmail,String footer) throws IOException {

		logger.log(Level.SEVERE, "Entered In Common Method");

		String[] toEmailIdArray = new String[toEmailId.size()];
		toEmailId.toArray(toEmailIdArray);

		StringBuilder builder = new StringBuilder();

		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:150px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("<h3 class=\"ex\" align=\"left\">" + mailTitle
				+ "</h3></br></br>");

		/************************ Table 1 ***************************************/

		if (table1 != null && !table1.isEmpty()) {
			System.out.println("Table  1");
			// builder.append("<h2>"+heading1+"</h2>");

			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
			builder.append("<tr>");

			// Product Table1 heading
			for (String header : table1_Header)
				builder.append("<th>" + header + "</th>");
			builder.append("</tr>");

			System.out.println("Size of table 1 " + table1.size());

			builder.append("<tr>");
			for (int i = 0; i < table1.size(); i++) {
				builder.append("<td>" + table1.get(i) + "</td>");
				System.out.println("Table Started 1" + table1.get(i));
				if (i > 0 && (i + 1) % table1_Header.size() == 0) {
					builder.append("</tr><tr>");

				}
			}
			builder.append(" </tr>  ");
			builder.append("</table></b></br>");
		}

		/************************************* Table 2 ************************************/

		if (table2 != null && !table2.isEmpty()) {
			System.out.println("Table 2");
			builder.append("<h2>" + heading2 + "</h2>");

			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"1\"  Style width=\"50%\" >");
			builder.append("<col width=\"1%\" >");
			builder.append("<col width=\"2%\" >");
			builder.append("<col width=\"3%\" >");
			builder.append("<col width=\"10%\">");

			builder.append("<tr>");
			// Product Table2 heading
			for (String header : table2_Header)
				builder.append("<th>" + header + "</th>");

			builder.append("</tr>");

			System.out.println("Size of list " + table2.size());
			builder.append("<tr>");
			for (int i = 0; i < table2.size(); i++) {
				builder.append("<td style=\"width:5%\">" + table2.get(i)
						+ "</td>");
				// if(i==6){
				// builder.append("<td style=\"width:20%\">" + table2.get(i) +
				// "</td>");
				// }
				System.out.println("Table Started 2 :" + table2.get(i));
			}
			builder.append(" </tr>  ");
			builder.append("</table></b></br>");
		}

		/**************************** FOOTER Details *********************************/
		 if(footer!=null){
		 builder.append("<p>"+footer+"</p></br></b></br>");
		 }

		// Company Details
		builder.append("<br>");
		builder.append("<b>" + company.getBusinessUnitName() + "</b></br>");
		builder.append("<table>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Address : ");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getAddrLine2().equals("")
				&& company.getAddress().getAddrLine2() != null)
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getAddrLine2() + " "
					+ company.getAddress().getLocality());
		else
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getLocality());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getLandmark().equals("")
				&& company.getAddress().getLandmark() != null)
			builder.append(company.getAddress().getLandmark() + ","
					+ company.getAddress().getCity() + ","
					+ company.getAddress().getPin()
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		else
			builder.append(company.getAddress().getCity() + ","
					+ company.getAddress().getPin() + ","
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Website : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getWebsite());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Phone No : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getCellNumber1());
		builder.append("</td>");
		builder.append("<tr>");

		builder.append("</tr>");
		builder.append("</table>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);
		
		/**
		 * @author Anil,Date : 09-02-2019
		 * If Process Type - EnableSendGridEmailApi is active then we will send email through SendGrid else through default mail i.r. JavaMail
		 * As appengine has limited daily mail quota i.e. 100, to overcome this limited quota issue we are implementing SendGrid
		 * raised By Rohan for Pecopp
		 */
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CronJob", "EnableSendGridEmailApi",company.getCompanyId())){
			logger.log(Level.SEVERE," company email --" +company.getEmail() +"  to email  " + toEmailIdArray.toString());
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			sdEmail.sendMailWithSendGrid(company.getEmail(), toEmailId, null, null, mailSubject, contentInHtml, "text/html",null,null,null,company.getDisplayNameForCompanyEmailId());
			
			return;
		}

		/************************************* Mail Send Logic *********************************/
		try {
			// System.out.println("Inside Try Block");
			// Properties props=new Properties();
			// Session session=Session.getDefaultInstance(props,null);

			System.out.println("Inside Try Block");
			Properties props = new Properties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", "smtp.gmail.com");
			Session session = Session.getDefaultInstance(props);

			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company.getEmail());
			logger.log(Level.SEVERE," company email --" +company.getEmail() +"  to email  " + toEmailIdArray.toString());
			if(fromEmail.trim().equals("")){
				message.setFrom(new InternetAddress(company.getEmail()));
			}else{
				message.setFrom(new InternetAddress(fromEmail));
			}
			// To Mail Address
		
			// message.setFrom(new
			// InternetAddress("evasoftwaresolutionserp@gmail.com"));
			message.setReplyTo(new InternetAddress[] { new InternetAddress(
					company.getEmail()) });

			// logger.log(Level.SEVERE,"To Email Address"+company.getEmail());

			// Email to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);

				logger.log(Level.SEVERE, "To Email Address" + mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);
			// message.setRecipient(Message.RecipientType.CC, new
			// InternetAddress(company.getEmail()));

			// logger.log(Level.SEVERE,"To Email Address"+company.getEmail());

			// Mail Subject
			message.setSubject(mailSubject);

			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentInHtml, "text/html");
			multiPart.addBodyPart(htmlpart);

			// Mail attachment
			// if(attachFiles!=null){
			// System.out.println("Inside Attachment.........");
			// MimeBodyPart attachbody=new MimeBodyPart();
			//
			// attachbody.setFileName(attachFiles[0]+".pdf");
			// DataSource src=new ByteArrayDataSource(pdfstream.toByteArray(),
			// "application/pdf");
			// attachbody.setDataHandler(new DataHandler(src));
			// System.out.println("Attach File Name");
			// multiPart.addBodyPart(attachbody);

			// adds attachments
			// if (attachFiles != null && attachFiles.length > 0) {
			// System.out.println("More");
			// for (String filePath : attachFiles) {
			// MimeBodyPart attachPart = new MimeBodyPart();
			// try {
			// attachPart.attachFile(filePath);
			// } catch (IOException ex) {
			// ex.printStackTrace();
			// }
			// multiPart.addBodyPart(attachPart);
			// }
			// }
			// }

			System.out.println("Ready To Send Mail");

			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			System.out.println("Email sent Successfully !!!!");
			logger.log(Level.SEVERE, "Email sent Successfully !!");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	public void cronSendEmailDueOverDue(ArrayList<String> toEmailId, String mailSubject,
			String mailTitle, Company company,String tbl_title, ArrayList<String> table1_Header,
			ArrayList<String> table1, String heading2,
			ArrayList<String> table2_Header, ArrayList<String> table2, String heading3,
			ArrayList<String> table3_Header, ArrayList<String> table3,
			String[] attachFiles,String fromEmail,String footer) throws IOException {

		logger.log(Level.SEVERE, "Entered In Common Method");

		String[] toEmailIdArray = new String[toEmailId.size()];
		toEmailId.toArray(toEmailIdArray);

		StringBuilder builder = new StringBuilder();

		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:150px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("<h3 class=\"ex\" align=\"left\">" + mailTitle
				+ "</h3></br></br>");
		
		
		if((table1==null||table1.size()==0)
				&&(table2==null||table2.size()==0)
				&&(table3==null||table3.size()==0)){
			builder.append("</br></br></br></br>");
		}

		/************************ Table 1 ***************************************/

		if (table1 != null && !table1.isEmpty()) {
			System.out.println("Table  1");
			 builder.append("<h2>"+tbl_title+"</h2>");

			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
			builder.append("<tr>");

			// Product Table1 heading
			for (String header : table1_Header){
				builder.append("<th>" + header + "</th>");
			}
			builder.append("</tr>");

			System.out.println("Size of table 1 " + table1.size());

			builder.append("<tr>");
			for (int i = 0; i < table1.size(); i++) {
				builder.append("<td>" + table1.get(i) + "</td>");
				System.out.println("Table Started 1" + table1.get(i));
				if (i > 0 && (i + 1) % table1_Header.size() == 0) {
					builder.append("</tr><tr>");

				}
			}
			builder.append(" </tr>  ");
			builder.append("</table></b></br>");
		}

		/************************************* Table 2 ************************************/

		if (table2 != null && !table2.isEmpty()) {
			System.out.println("Table 2");
			builder.append("<h2>" + heading2 + "</h2>");

			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\"  >");

			builder.append("<tr>");
			// Product Table2 heading
			for (String header : table2_Header)
				builder.append("<th>" + header + "</th>");

			builder.append("</tr>");

			System.out.println("Size of list " + table2.size());
			builder.append("<tr>");
			for (int i = 0; i < table2.size(); i++) {
				builder.append("<td>" + table2.get(i)
						+ "</td>");
				// if(i==6){
				// builder.append("<td style=\"width:20%\">" + table2.get(i) +
				// "</td>");
				// }
				if (i > 0 && (i + 1) % table2_Header.size() == 0) {
					builder.append("</tr><tr>");

				}
				System.out.println("Table Started 2 :" + table2.get(i));
			}
			builder.append(" </tr>  ");
			builder.append("</table></b></br>");
		}
		
		/**
		 * 
		 */
		
		/************************************* Table 2 ************************************/

		if (table3 != null && !table3.isEmpty()) {
			System.out.println("Table 3");
			builder.append("<h2>" + heading3 + "</h2>");

			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\"   >");

			builder.append("<tr>");
			// Product Table2 heading
			/*for (String header : table2_Header)
				builder.append("<th>" + header + "</th>");*/
			
			for(int i=0;i<table3_Header.size();i++)
			{
				if(!table3_Header.get(i).equals("$#") && !table3_Header.get(i).equals("$##") && !table3_Header.get(i).equals("$#$")){
					if(table3_Header.get(i+1).equals("$#")){
						
						builder.append("<th colspan=\"2\">" + table3_Header.get(i) + "</th>");
					}else if(table3_Header.get(i+1).equals("$#$")){
						
						builder.append("<th colspan=\"3\">" + table3_Header.get(i) + "</th>");
					}else if(table3_Header.get(i+1).equals("$##")){
						builder.append("<th rowspan=\"2\">" + table3_Header.get(i) + "</th>");
					}else{
					
						builder.append("<th>" +  table3_Header.get(i) + "</th>");
					}
				}
						
			}
			
		
			builder.append("</tr>");

			System.out.println("Size of list " + table3.size());
			builder.append("<tr>");
			for (int i = 0; i < table3.size(); i++) {
				builder.append("<td>" + table3.get(i)
						+ "</td>");
				// if(i==6){
				// builder.append("<td style=\"width:20%\">" + table2.get(i) +
				// "</td>");
				// }
				if (i > 0 && (i + 1) % table3_Header.size() == 0) {
					builder.append("</tr><tr>");

				}
				System.out.println("Table Started 3 :" + table3.get(i));
			}
			builder.append(" </tr>  ");
			builder.append("</table></b></br>");
			
//			table3_Header.remove("$#");
//			table3_Header.remove("$#$");
//			table3_Header.remove("$##");

		}
		
		/**
		 * 
		 */
		/**************************** FOOTER Details *********************************/
		 if(footer!=null&&!footer.equals("")){
			 builder.append("<p>"+footer+"</p></br></b></br>");
		 }else{
			 builder.append("<p></p></br></b></br>"); 
		 }

		builder.append("<b>" + "Thanks & Regards" + "</b>");
		builder.append("<table>");
			
		// Company Details
		builder.append("<br>");
		builder.append("<b>" + company.getBusinessUnitName() + "</b></br>");
		builder.append("<table>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Address : ");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getAddrLine2().equals("")
				&& company.getAddress().getAddrLine2() != null)
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getAddrLine2() + " "
					+ company.getAddress().getLocality());
		else
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getLocality());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getLandmark().equals("")
				&& company.getAddress().getLandmark() != null)
			builder.append(company.getAddress().getLandmark() + ","
					+ company.getAddress().getCity() + ","
					+ company.getAddress().getPin()
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		else
			builder.append(company.getAddress().getCity() + ","
					+ company.getAddress().getPin() + ","
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Website : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getWebsite());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Phone No : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getCellNumber1());
		builder.append("</td>");
		builder.append("<tr>");
		
		/**
		 * @author Anil @since 19-05-2021
		 * Add email id in company signature
		 * raised by Nitin sir for service reschedule from client task
		 */
		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Email : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getEmail());
		builder.append("</td>");
		builder.append("<tr>");

		builder.append("</tr>");
		builder.append("</table>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);
		
		
		/**
		 * @author Anil,Date : 09-02-2019
		 * If Process Type - EnableSendGridEmailApi is active then we will send email through SendGrid else through default mail i.r. JavaMail
		 * As appengine has limited daily mail quota i.e. 100, to overcome this limited quota issue we are implementing SendGrid
		 * raised By Rohan for Pecopp
		 */
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CronJob", "EnableSendGridEmailApi",company.getCompanyId())){
			logger.log(Level.SEVERE," company email --" +company.getEmail() +"  to email  " + toEmailIdArray.toString());
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			sdEmail.sendMailWithSendGrid(company.getEmail(), toEmailId, null, null, mailSubject, contentInHtml, "text/html",null,null,null,company.getDisplayNameForCompanyEmailId());
			
			return;
		}
		

		/************************************* Mail Send Logic *********************************/
		try {
			// System.out.println("Inside Try Block");
			// Properties props=new Properties();
			// Session session=Session.getDefaultInstance(props,null);

			System.out.println("Inside Try Block");
			Properties props = new Properties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", "smtp.gmail.com");
			Session session = Session.getDefaultInstance(props);

			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company.getEmail());
			logger.log(Level.SEVERE," company email --" +company.getEmail() +"  to email  " + toEmailIdArray.toString());
			if(fromEmail.trim().equals("")){
				message.setFrom(new InternetAddress(company.getEmail()));
			}else{
				message.setFrom(new InternetAddress(fromEmail));
			}
			// To Mail Address
		
			// message.setFrom(new
			// InternetAddress("evasoftwaresolutionserp@gmail.com"));
			message.setReplyTo(new InternetAddress[] { new InternetAddress(
					company.getEmail()) });

			// logger.log(Level.SEVERE,"To Email Address"+company.getEmail());

			// Email to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);

				logger.log(Level.SEVERE, "To Email Address" + mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);
			message.setSubject(mailSubject);

			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentInHtml, "text/html");
			multiPart.addBodyPart(htmlpart);


			System.out.println("Ready To Send Mail");

			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			System.out.println("Email sent Successfully !!!!");
			logger.log(Level.SEVERE, "Email sent Successfully !!");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	/**
	 * Date 15-03-2018 By vijay for customer upload Duplicate entries email list
	 */
	
	public void uploadDataSendEmail(ArrayList<String> toEmailId, String mailSubject,
			String mailTitle, Company company, ArrayList<String> table1_Header,
			ArrayList<String> table1, String heading2,
			ArrayList<String> table2_Header, ArrayList<String> table2,
			String[] attachFiles) throws IOException {

		logger.log(Level.SEVERE, "Entered In Common Method");

		String[] toEmailIdArray = new String[toEmailId.size()];
		toEmailId.toArray(toEmailIdArray);

		StringBuilder builder = new StringBuilder();

		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("<h3 class=\"ex\" align=\"left\">" + mailTitle
				+ "</h3></br></br>");

		/************************ Table 1 ***************************************/

		if (table1 != null && !table1.isEmpty()) {
			System.out.println("Table  1");
			// builder.append("<h2>"+heading1+"</h2>");

			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
			builder.append("<tr>");

			// Product Table1 heading
			for (String header : table1_Header)
				builder.append("<th>" + header + "</th>");
			builder.append("</tr>");

			System.out.println("Size of table 1 " + table1.size());

			builder.append("<tr>");
			for (int i = 0; i < table1.size(); i++) {
				builder.append("<td>" + table1.get(i) + "</td>");
				System.out.println("Table Started 1" + table1.get(i));
//				if (i > 0 && (i + 1) % table1_Header.size() == 0) {
					builder.append("</tr><tr>");

//				}
			}
			builder.append(" </tr>  ");
			builder.append("</table></b></br>");
		}

		/************************************* Table 2 ************************************/

		if (table2 != null && !table2.isEmpty()) {
			System.out.println("Table 2");
			builder.append("<h2>" + heading2 + "</h2>");

			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"1\"  Style width=\"50%\" >");
			builder.append("<col width=\"1%\" >");
			builder.append("<col width=\"2%\" >");
			builder.append("<col width=\"3%\" >");
			builder.append("<col width=\"10%\">");

			builder.append("<tr>");
			// Product Table2 heading
			for (String header : table2_Header)
				builder.append("<th>" + header + "</th>");

			builder.append("</tr>");

			System.out.println("Size of list " + table2.size());
			builder.append("<tr>");
			for (int i = 0; i < table2.size(); i++) {
				builder.append("<td style=\"width:5%\">" + table2.get(i)
						+ "</td>");
				// if(i==6){
				// builder.append("<td style=\"width:20%\">" + table2.get(i) +
				// "</td>");
				// }
				System.out.println("Table Started 2 :" + table2.get(i));
			}
			builder.append(" </tr>  ");
			builder.append("</table></b></br>");
		}

		/**************************** FOOTER Details *********************************/
		// if(mailFooter!=null){
		// builder.append("<p>"+mailFooter+"</p></br></b></br>");
		// }

		// Company Details
		builder.append("<br>");
		builder.append("<b>" + company.getBusinessUnitName() + "</b></br>");
		builder.append("<table>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Address : ");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getAddrLine2().equals("")
				&& company.getAddress().getAddrLine2() != null)
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getAddrLine2() + " "
					+ company.getAddress().getLocality());
		else
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getLocality());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getLandmark().equals("")
				&& company.getAddress().getLandmark() != null)
			builder.append(company.getAddress().getLandmark() + ","
					+ company.getAddress().getCity() + ","
					+ company.getAddress().getPin()
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		else
			builder.append(company.getAddress().getCity() + ","
					+ company.getAddress().getPin() + ","
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Website : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getWebsite());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Phone No : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getCellNumber1());
		builder.append("</td>");
		builder.append("<tr>");

		builder.append("</tr>");
		builder.append("</table>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);

		/************************************* Mail Send Logic *********************************/
		try {
			// System.out.println("Inside Try Block");
			// Properties props=new Properties();
			// Session session=Session.getDefaultInstance(props,null);

			System.out.println("Inside Try Block");
			Properties props = new Properties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", "smtp.gmail.com");
			Session session = Session.getDefaultInstance(props);

			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company.getEmail());

			// To Mail Address
			message.setFrom(new InternetAddress(company.getEmail()));
			// message.setFrom(new
			// InternetAddress("evasoftwaresolutionserp@gmail.com"));
			message.setReplyTo(new InternetAddress[] { new InternetAddress(
					company.getEmail()) });

			// logger.log(Level.SEVERE,"To Email Address"+company.getEmail());

			// Email to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);

				logger.log(Level.SEVERE, "To Email Address" + mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);
			// message.setRecipient(Message.RecipientType.CC, new
			// InternetAddress(company.getEmail()));

			// logger.log(Level.SEVERE,"To Email Address"+company.getEmail());

			// Mail Subject
			message.setSubject(mailSubject);

			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentInHtml, "text/html");
			multiPart.addBodyPart(htmlpart);

			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			System.out.println("Email sent Successfully !!!!");
			logger.log(Level.SEVERE, "Email sent Successfully !!");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	/**
	 * ends here
	 */
	private void createFumigationMBAttachment() {
		fumigationMBpdf = new FumigationCertificateMB();
		fumigationMBpdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(fumigationMBpdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		fumigationMBpdf.document.open();
		fumigationMBpdf.loadfugation(fumigation.getId());
		fumigationMBpdf.createPdf();
	//	.createPdfForEmail(fumigation, comp, c);
		fumigationMBpdf.document.close();

	}
	private void createFumigationALPAttachment() {
		fumigationALPpdf = new FumigationCertificateALP();
		fumigationALPpdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(fumigationALPpdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		fumigationALPpdf.document.open();
		fumigationALPpdf.loadfugation(fumigation.getId());
		fumigationALPpdf.createPdf();

		//fumigationALPpdf.createPdfForEmail(fumigation, comp, c);
		fumigationALPpdf.document.close();

	}
	private void createFumigationAFASAttachment() {
		fumigationAFASpdf = new FumigationCertificateAFAS();
		fumigationAFASpdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(fumigationAFASpdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		fumigationAFASpdf.document.open();
		fumigationAFASpdf.loadfugation(fumigation.getId());
		fumigationAFASpdf.createPdf();
		//fumigationAFASpdf.createPdfForEmail(fumigation, comp, c);
		fumigationAFASpdf.document.close();

	}
	
	
	public void sendEmailforhtml(String mailSub, String mailMsg,
			ArrayList<String> toEmailLis, ArrayList<String> ccEmailLis,
			String fromEmail) {
		try {

			System.out.println("Inside Try Block");
			logger.log(Level.SEVERE, "INSIDE SEND MAIL : EMAIL SERVICE ");
			logger.log(Level.SEVERE, "FROM EMAIL " + fromEmail);

			for (String to : toEmailLis) {
				if (ccEmailLis!=null && ccEmailLis.contains(to)) {
					ccEmailLis.remove(to);
				}
			}

			String[] toEmailIdArray = new String[toEmailLis.size()];
			toEmailLis.toArray(toEmailIdArray);

			String[] ccEmailIdArray =null;
			if(ccEmailLis!=null){
				ccEmailIdArray = new String[ccEmailLis.size()];
				ccEmailLis.toArray(ccEmailIdArray);
			}
			

			Properties props = new Properties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", "smtp.gmail.com");
			Session session = Session.getDefaultInstance(props);
			// Session session = Session.getDefaultInstance(props, null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(mailMsg, "text/html");
			multiPart.addBodyPart(htmlpart);
			// To Mail Address
			message.setFrom(new InternetAddress(fromEmail));

			// Email To to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				logger.log(Level.SEVERE, "To Email id multipart "
						+ mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);

			if(ccEmailIdArray!=null){
			// Email CC to Multiple Recipients
			InternetAddress[] ccmailToMultiple = new InternetAddress[ccEmailIdArray.length];
				for (int i = 0; i < ccEmailIdArray.length; i++) {
					ccmailToMultiple[i] = new InternetAddress(ccEmailIdArray[i]);
					logger.log(Level.SEVERE, "CC Email id multipart "
							+ ccmailToMultiple[i]);
				}
				if (ccmailToMultiple.length != 0) {
					message.setRecipients(Message.RecipientType.CC,
							ccmailToMultiple);
				}
			}

			// Mail Subject
			message.setSubject(mailSub);

			// Mail Body Content In html Form
//			message.setText(mailMsg);
			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			logger.log(Level.SEVERE, "Mail Sent");
		} catch (Exception e) {
			e.printStackTrace();

			logger.log(Level.SEVERE, "Mail Not Sent " + e);
		}
	}

	/**
	  * Updated Viraj 
	  * Date 15-11-2018
	  * Description to pass sales object for email
	  */
	@Override
	public void passServiceQuotationobject(Sales servquot)
			throws IllegalArgumentException {
		
		Quotation servq1 = (Quotation) servquot;
		String quotationId = servq1.getCount()+"";
		String companyId = servq1.getCompanyId()+"";
		
		Queue queue = QueueFactory.getQueue("EmailUploadTaskQueue-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/emailupload").param("quotationId", quotationId).param("companyId", companyId));
		
	}
	
	
	/**
	 * To send email with HTML format below method is reusable Date :01-02-2019 by Vijay
	 * For NBHC inventory management for
	 */

	public void htmlformatsendEmail(ArrayList<String> toEmail,
			String mailSubject, String mailTitle, Company company,
			String msgBody, String footerMsg, String attachFiles,
			ArrayList<String> table1_Header, ArrayList<String> table1,String productTitle,
			ArrayList<String> table3_Header, ArrayList<String> table3, String approvalTitle,
			ArrayList<String> table5_Header, ArrayList<String> table5) {

		// to Email id's in array
		String[] toEmailIdArray = new String[toEmail.size()];
		toEmail.toArray(toEmailIdArray);

		System.out.println("Mail Sub : " + mailSubject);
		System.out.println("Mail Title : " + mailTitle);

		StringBuilder builder = new StringBuilder();

		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("<h3 class=\"ex\" align=\"center\"> <td> <td> &nbsp; &nbsp; &nbsp; &nbsp;" + mailTitle
				+ "</h3></br></br>");

		builder.append(" <p>  </p> </br></br>");

		builder.append(" <B>   </br></br></B>");
		builder.append(msgBody);
		builder.append(" <B>   </br></br></B>");

		/************************ Table 1 ***************************************/

		if (table1 != null && !table1.isEmpty()) {
			System.out.println("Table  1");
			// builder.append("<h2>"+heading1+"</h2>");

			builder.append("</br></br></br><table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
			builder.append("<tr>");

			// Product Table1 heading
			for (String header : table1_Header)
				builder.append("<th>" + header + "</th>");
			builder.append("</tr>");

			System.out.println("Size of table 1 " + table1.size());

			builder.append("<tr>");
			for (int i = 0; i < table1.size(); i++) {
				builder.append("<td>" + table1.get(i) + "</td>");
				System.out.println("Table Started 1" + table1.get(i));
				if (i > 0 && (i + 1) % table1_Header.size() == 0) {
					builder.append("</tr><tr>");
				}
			}
			builder.append(" </tr>  ");
			builder.append("</table></br></br></br>");
		}

		builder.append("  <br><br>");
//		builder.append("  <br><br>");
		
//		for (String header : table2_Header)
//			builder.append("<td><td><td><td><th>" + header + "</th>");
		if(productTitle!=null){
		builder.append("<h3 class=\"ex\" align=\"center\"> <td> <td>" + productTitle
				+ "</h3></br></br>");
		
//		builder.append("  <br><br>");
//		builder.append("  <br><br>");
		}
		
		/************************ Table 2 ***************************************/

		if (table3 != null && !table3.isEmpty()) {
			System.out.println("Table  1");
			// builder.append("<h2>"+heading1+"</h2>");

			builder.append("</br></br></br><table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
			builder.append("<tr>");

			// Product Table1 heading
			for (String header : table3_Header)
				builder.append("<th>" + header + "</th>");
			builder.append("</tr>");

			System.out.println("Size of table 1 " + table3.size());

			builder.append("<tr>");
			for (int i = 0; i < table3.size(); i++) {
				builder.append("<td>" + table3.get(i) + "</td>");
				System.out.println("Table Started 1" + table3.get(i));
				if (i > 0 && (i + 1) % table3_Header.size() == 0) {
					builder.append("</tr><tr>");
				}
			}
			builder.append(" </tr>  ");
			builder.append("</table></br></br></br>");
		}

		builder.append("  <br><br>");
		builder.append("  <br><br>");
		
		if(approvalTitle!=null){
			builder.append("<h3 class=\"ex\" align=\"center\"> <td> <td>" + approvalTitle
					+ "</h3></br></br>");
		
		builder.append("  <br><br>");
		builder.append("  <br><br>");
		}
		/************************ Table 1 ***************************************/

		if (table5 != null && !table5.isEmpty()) {
			System.out.println("Table  1");
			// builder.append("<h2>"+heading1+"</h2>");

			builder.append("</br></br></br><table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
			builder.append("<tr>");

			// Product Table1 heading
			for (String header : table5_Header)
				builder.append("<th>" + header + "</th>");
			builder.append("</tr>");

			System.out.println("Size of table 1 " + table5.size());

			builder.append("<tr>");
			for (int i = 0; i < table5.size(); i++) {
				builder.append("<td>" + table5.get(i) + "</td>");
				System.out.println("Table Started 1" + table5.get(i));
				if (i > 0 && (i + 1) % table5_Header.size() == 0) {
					builder.append("</tr><tr>");
				}
			}
			builder.append(" </tr>  ");
			builder.append("</table></br></br></br>");
		}

		builder.append("  <br><br>");
		builder.append("  <br><br>");
		// Company Details
		builder.append("</br><b>" + company.getBusinessUnitName() + "</b></br>");
		builder.append("<table>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Address : ");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getAddrLine2().equals("")
				&& company.getAddress().getAddrLine2() != null)
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getAddrLine2() + " "
					+ company.getAddress().getLocality());
		else
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getLocality());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getLandmark().equals("")
				&& company.getAddress().getLandmark() != null)
			builder.append(company.getAddress().getLandmark() + ","
					+ company.getAddress().getCity() + ","
					+ company.getAddress().getPin()
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		else
			builder.append(company.getAddress().getCity() + ","
					+ company.getAddress().getPin() + ","
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Website : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getWebsite());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Phone No : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getCellNumber1());
		builder.append("</td>");
		builder.append("<tr>");

		builder.append("</tr>");
		builder.append("</table>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);

		try {
			System.out.println("Inside Try Block");
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company.getEmail());

			// To Mail Address
			message.setFrom(new InternetAddress(company.getEmail()));
			message.setReplyTo(new InternetAddress[] { new InternetAddress(
					company.getEmail()) });

			// Email to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);
			// message.setRecipient(Message.RecipientType.CC, new
			// InternetAddress(company.getEmail()));

			// Mail Subject
			message.setSubject(mailSubject);

			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentInHtml, "text/html");
			multiPart.addBodyPart(htmlpart);

			// Mail attachment
			if (attachFiles != null) {
				System.out.println("Inside Attachment.........");

				MimeBodyPart attachbody = new MimeBodyPart();
				attachbody.setFileName(attachFiles + ".pdf");
				DataSource src = new ByteArrayDataSource(
						pdfstream.toByteArray(), "application/pdf");
				attachbody.setDataHandler(new DataHandler(src));
				multiPart.addBodyPart(attachbody);
			}

			System.out.println("Ready To Send Mail");

			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			System.out.println("Email sent Successfully !!!!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Date 02-02-2019 by Vijay
	 * Des :- NBHC Inventory management if PR is automatic created by system PR then new email format will send
	 * so checking Automatic PR or Manual PR for send email
	 */
	private boolean validateAutomaticPR(Approvals approve2) {
		pr  = ofy().load().type(PurchaseRequisition.class).filter("companyId", approve.getCompanyId())
				.filter("count",approve.getBusinessprocessId()).filter("automaticPRFlag", true).first().now();
		
		logger.log(Level.SEVERE, "PR ===="+pr);

		if(pr!=null){
			return true;
		}
		return false;
	}
	
	
	/**
	 * Date 02-02-2019 By Vijay 
	 * Des :- NBHC Inventory Management Automatic PR Approval Email send with new format using process configuration
	 * and this is only for AUtomatic PR Generated by Ststem
	 *  
	 */
	private void sendNewEmailFormatforPR(PurchaseRequisition purchaseReq, String approverName) {
		ArrayList<Integer> prProduct = new ArrayList<Integer>();
		for(ProductDetails prodDetails : purchaseReq.getPrproduct()){
			prProduct.add(prodDetails.getProductID());
		}
		
		List<ProductInventoryView> productInventoryView = ofy().load().type(ProductInventoryView.class)
				.filter("productinfo.prodID IN", prProduct).filter("companyId", purchaseReq.getCompanyId()).list();
		ArrayList<ProductInventoryViewDetails> prodDetails = new ArrayList<ProductInventoryViewDetails>();
		for(ProductDetails prproduct : purchaseReq.getPrproduct()){
		for(ProductInventoryView prodinventory : productInventoryView){
			for(ProductInventoryViewDetails prodDetail : prodinventory.getDetails()){
					if(prodDetail.getProdid()==prproduct.getProductID() &&
							prodDetail.getWarehousename().trim().equals(prproduct.getWarehouseName().trim())){
						prodDetails.add(prodDetail);
					}
				}
			}
		}
		System.out.println("prodDetails =="+prodDetails.size());
		AutomaticPRCreationCronJobImpl automaticPREmail = new AutomaticPRCreationCronJobImpl();
		if(prodDetails.size()!=0){
			Company company = ofy().load().type(Company.class).filter("companyId",purchaseReq.getCompanyId()).first().now();
			if(company!=null){
				ArrayList<String> tableHeader = new ArrayList<String>();
				tableHeader.add("Sr.No.");
				tableHeader.add("User Name");
				tableHeader.add("Decision");
				tableHeader.add("Remarks");
				tableHeader.add("Decision Date");
				
				ArrayList<String> table1 = new ArrayList<String>();
				List<Approvals> approvalslist = ofy().load().type(Approvals.class)
						.filter("businessprocessId", purchaseReq.getCount()).filter("businessprocesstype", "Purchase Requisition").list();
				if(approvalslist.size()!=0){
					Comparator<Approvals> compApprovals = new Comparator<Approvals>() {
						
						@Override
						public int compare(Approvals a1, Approvals a2) {
							//ascending order
							Integer count1 = a1.getCount();
							Integer count2 = a2.getCount();
							return count1.compareTo(count2);
						}
					};
					Collections.sort(approvalslist,compApprovals);
					int count = 1;
				
					for(Approvals approval: approvalslist){
						table1.add(count+"");
						table1.add(approval.getApproverName());
						table1.add(approval.getStatus());
						table1.add(approval.getRemark());
						if(approval.getApprovalOrRejectDate()!=null)
						table1.add(fmt.format(approval.getApprovalOrRejectDate()));

					}
				}
				automaticPREmail.sendEmail(prodDetails, purchaseReq.getCompanyId(), purchaseReq.getEmployee(),
						purchaseReq.getCount(), purchaseReq,company,approverName,"Approval Status",tableHeader,table1);
			}

		}
	}

	/**
	 * ends here
	 */
	
	
	/**
	 * @author Anil ,Date : 26-03-2019
	 * Cron Job mail for missing Attendance
	 */
	public void cronJobEmailForMissingAttendance(ArrayList<String> toEmailId,
			String mailSubject,String mailTitle, Company company, 
			HashMap<String,TreeMap<Date,ArrayList<MissingAttendanceDetails>>> map) throws IOException {

		logger.log(Level.SEVERE, "Entered In Common Method");

		String[] toEmailIdArray = new String[toEmailId.size()];
		toEmailId.toArray(toEmailIdArray);

		StringBuilder builder = new StringBuilder();

		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("<h3 class=\"ex\" align=\"left\">" + mailTitle+ "</h3></br></br>");
		
		
		/**
		 * 
		 */
		if(map!=null&&map.size()!=0){
			for (Map.Entry<String,TreeMap<Date,ArrayList<MissingAttendanceDetails>>> entry : map.entrySet()) {
			    System.out.println(entry.getKey() + " = " + entry.getValue());
			    int counter=0;
			    builder.append("</br></br></br>");
			    int totalMissed=0;
			    for(Map.Entry<Date,ArrayList<MissingAttendanceDetails>>subentry:entry.getValue().entrySet()){
			    	System.out.println(subentry.getKey() + " = " + subentry.getValue());
//			    	builder.append("</br></br></br>");
			    	 totalMissed=totalMissed+subentry.getValue().size();
			    	if(counter==0){
			    		//Tbl 1
				    	builder.append("<table border=\"0\" cellspacing=\"1\" cellpadding=\"5\">");
				    	// Tbl 1 row 1
				    	builder.append("<tr>");
						builder.append("<td colspan=\"2\"><b>" + "Project Name: "+subentry.getValue().get(0).getProjectName()+ "</b></td>");
						builder.append("<td><b>" + "Branch: "+subentry.getValue().get(0).getBranchName()+ "</b></td>");
//						builder.append("<td><b>" + "Total Missed attendance :  "+subentry.getValue().size()+"</td>");
						builder.append("</tr>");
						// Tbl 1 row 2
						builder.append("<tr>");
						builder.append("<td><b>" + "Branch Manager: "+subentry.getValue().get(0).getBranchManager()+ "</b></td>");
						builder.append("<td><b>" + "Manager: "+subentry.getValue().get(0).getManager()+ "</b></td>");
						builder.append("<td><b>" + "Supervisor: "+subentry.getValue().get(0).getSupervisor()+ "</b></td>");
						builder.append("</tr>");
						builder.append("</table></b></br>");
						
						//Tbl 2
						builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
						//Tbl 2 row 1
				    	builder.append("<tr>");
						builder.append("<th>" + "Emp. Id"+"</th>");
						builder.append("<th>" + "Emp. Name"+"</th>");
						builder.append("<th>" + "Designation"+"</th>");
						builder.append("<th>" + "Remark"+"</th>");
						builder.append("</tr>");
			    	}
			    	counter++;
					builder.append("<tr>");
					builder.append("<th colspan=\"4\" align=\"center\">" + fmt.format(subentry.getKey())+"</th>");
					builder.append("</tr>");
					
					for(MissingAttendanceDetails misAtten:subentry.getValue()){
						//Tbl 2 row 1
				    	builder.append("<tr>");
						builder.append("<td>" + misAtten.getEmpId()+"</td>");
						builder.append("<td>" + misAtten.getEmpName()+"</td>");
						builder.append("<td>" + misAtten.getDesignation()+"</td>");
						builder.append("<td>" + misAtten.getRemark()+"</td>");
						builder.append("</tr>");
					}
					
			    }
			    builder.append("<tr>");
				builder.append("<th colspan=\"4\" align=\"right\">" + "Total Missed Attendance: "+totalMissed+ "</b></td>");
				builder.append("</tr>");
			    
			    builder.append("</table></b></br>");
			    builder.append("</br></br></br>");
			}
		}
		
		/**
		 * 
		 */

		/************************ Table 1 ***************************************/

//		if (table1 != null && !table1.isEmpty()) {
//			System.out.println("Table  1");
//			// builder.append("<h2>"+heading1+"</h2>");
//
//			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
//			builder.append("<tr>");
//
//			// Product Table1 heading
//			for (String header : table1_Header)
//				builder.append("<th>" + header + "</th>");
//			builder.append("</tr>");
//
//			System.out.println("Size of table 1 " + table1.size());
//
//			builder.append("<tr>");
//			for (int i = 0; i < table1.size(); i++) {
//				builder.append("<td>" + table1.get(i) + "</td>");
//				System.out.println("Table Started 1" + table1.get(i));
//				if (i > 0 && (i + 1) % table1_Header.size() == 0) {
//					builder.append("</tr><tr>");
//
//				}
//			}
//			builder.append(" </tr>  ");
//			builder.append("</table></b></br>");
//		}
//
//		/************************************* Table 2 ************************************/
//
//		if (table2 != null && !table2.isEmpty()) {
//			System.out.println("Table 2");
//			builder.append("<h2>" + heading2 + "</h2>");
//
//			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"1\"  Style width=\"50%\" >");
//			builder.append("<col width=\"1%\" >");
//			builder.append("<col width=\"2%\" >");
//			builder.append("<col width=\"3%\" >");
//			builder.append("<col width=\"10%\">");
//
//			builder.append("<tr>");
//			// Product Table2 heading
//			for (String header : table2_Header)
//				builder.append("<th>" + header + "</th>");
//
//			builder.append("</tr>");
//
//			System.out.println("Size of list " + table2.size());
//			builder.append("<tr>");
//			for (int i = 0; i < table2.size(); i++) {
//				builder.append("<td style=\"width:5%\">" + table2.get(i)
//						+ "</td>");
//				// if(i==6){
//				// builder.append("<td style=\"width:20%\">" + table2.get(i) +
//				// "</td>");
//				// }
//				System.out.println("Table Started 2 :" + table2.get(i));
//			}
//			builder.append(" </tr>  ");
//			builder.append("</table></b></br>");
//		}
		builder.append("</br></br></br>");
		

		/**************************** FOOTER Details *********************************/

		// Company Details
		builder.append("<br>");
		builder.append("<b>" + company.getBusinessUnitName() + "</b></br>");
		builder.append("<table>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Address : ");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getAddrLine2().equals("")&& company.getAddress().getAddrLine2() != null){
			builder.append(company.getAddress().getAddrLine1() + " "+ company.getAddress().getAddrLine2() + " "
					+ company.getAddress().getLocality());
		}else{
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getLocality());
		}
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("</td>");
		builder.append("<td>");
		
		if (!company.getAddress().getLandmark().equals("")&& company.getAddress().getLandmark() != null){
			builder.append(company.getAddress().getLandmark() + ","+ company.getAddress().getCity() + ","
					+ company.getAddress().getPin()+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		}else{
			builder.append(company.getAddress().getCity() + ","+ company.getAddress().getPin() + ","
					+ company.getAddress().getState() + ","+ company.getAddress().getCountry());
		}
		
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Website : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getWebsite());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Phone No : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getCellNumber1());
		builder.append("</td>");
		builder.append("<tr>");

		builder.append("</tr>");
		builder.append("</table>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CronJob", "EnableSendGridEmailApi",company.getCompanyId())){
			logger.log(Level.SEVERE," company email --" +company.getEmail() +"  to email  " + toEmailIdArray.toString());
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			sdEmail.sendMailWithSendGrid(company.getEmail(), toEmailId, null, null, mailSubject, contentInHtml, "text/html",null,null,null,company.getDisplayNameForCompanyEmailId());
			
			return;
		}
		/************************************* Mail Send Logic *********************************/
		try {

			System.out.println("Inside Try Block");
			Properties props = new Properties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", "smtp.gmail.com");
			Session session = Session.getDefaultInstance(props);

			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			// To Mail Address
			message.setFrom(new InternetAddress(company.getEmail()));
			// InternetAddress("evasoftwaresolutionserp@gmail.com"));
			message.setReplyTo(new InternetAddress[] { new InternetAddress(company.getEmail()) });

			logger.log(Level.SEVERE,"To Email Address"+company.getEmail());

			// Email to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				logger.log(Level.SEVERE, "To Email Address" + mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);
			// message.setRecipient(Message.RecipientType.CC, new
			// InternetAddress(company.getEmail()));
			// Mail Subject
			message.setSubject(mailSubject);
			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentInHtml, "text/html");
			multiPart.addBodyPart(htmlpart);
			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			System.out.println("Email sent Successfully !!!!");
			logger.log(Level.SEVERE, "Email sent Successfully !!");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void createSRCopyttachment(List<Service> servicelist) {
		Service service = servicelist.get(0);
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio", "CustomizedSrCopy", service.getCompanyId())){
			serviceRecordPdf = new MultipleServicesRecordPdf();
			serviceRecordPdf.document = new Document();
			pdfstream = new ByteArrayOutputStream();
			try {
				PdfWriter.getInstance(serviceRecordPdf.document, pdfstream);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			serviceRecordPdf.document.open();
			serviceRecordPdf.loadAll(service.getId(), service.getSrCopyNumber(),service.getCompanyId());
			serviceRecordPdf.createPdf("no");
			serviceRecordPdf.document.close();
		}
		/**
		 * @author Anil @since 10-08-2021
		 * Customized SR copy should go to client for Pecopp
		 * issue raised by Ashwini
		 */
		else if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "MailSRPecopp", service.getCompanyId())){
			logger.log(Level.SEVERE,"Inside pecopp specific SR Record PDf");
			MultipleServicesRecordVersionOnePdf serviceRecordPdf = new MultipleServicesRecordVersionOnePdf();
			serviceRecordPdf.document = new Document(); 
			pdfstream = new ByteArrayOutputStream();
			Document document = serviceRecordPdf.document;
			try {
				PdfWriter.getInstance(document, pdfstream);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
//			serviceRecordPdf.document.open();
			document.open();
//			serviceRecordPdf.loadAll(service.getId(), service.getCompanyId());
//			serviceRecordPdf.createPdf("plane");
			serviceRecordPdf.loadMultipleSRCopies(servicelist);

			boolean upcflag=false;
			if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","CompanyLetterHead", service.getCompanyId())) {
				upcflag = true;
			}
			
//			if(upcflag){
//				serviceRecordPdf.createPdf("no");
//			}else{
//				serviceRecordPdf.createPdf("plane");
//			}
////			serviceRecordPdf.document.close();
			document.close();
			/**
			 * @author Priyanka @since 23-08-2021
			 * Customized SR copy should go to client for Innovativetive
			 * issue raised by Rahul
			 */
		}else if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "PC_ServiceReportV3", service.getCompanyId())) {
			logger.log(Level.SEVERE,"Inside Innovative specific SR Record PDf");
			CustomerServiceRecordVersionOnePdf serviceRecordPdf = new CustomerServiceRecordVersionOnePdf();
			serviceRecordPdf.document = new Document();
			pdfstream = new ByteArrayOutputStream();
			try {
				PdfWriter.getInstance(serviceRecordPdf.document, pdfstream);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			serviceRecordPdf.document.open();
//			serviceRecordPdf.setpdfCustomerServRecord(service.getId());
			serviceRecordPdf.loadMultipleSRCopies(servicelist);

//			serviceRecordPdf.createPdf();
			serviceRecordPdf.document.close();
			
			
		}else{
			//Amols new service record pdf..
			
			CustomerServiceRecordPdf serviceRecordPdf = new CustomerServiceRecordPdf();
			serviceRecordPdf.document = new Document();
			pdfstream = new ByteArrayOutputStream();
			try {
				PdfWriter.getInstance(serviceRecordPdf.document, pdfstream);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			serviceRecordPdf.document.open();
//			serviceRecordPdf.setpdfCustomerServRecord(service.getId());
			logger.log(Level.SEVERE,"Inside specific SR Record PDf"+servicelist.size());
			serviceRecordPdf.loadMultipleSRCopies(servicelist);

//			serviceRecordPdf.createPdf();
			serviceRecordPdf.document.close();
			
		}
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EmailCustomizedAndGenericSRCopy", service.getCompanyId())){
			if (service.getUptestReport() != null && service.getUptestReport().getUrl() != null) {
				uploadTC=createAttachmentInStream(service.getUptestReport());
			}
			if(service.getServiceSummaryReport2()!=null && service.getServiceSummaryReport2().getUrl() != null){
				uploadCP=createAttachmentInStream(service.getServiceSummaryReport2());
			}
		}

	}
	
	public ByteArrayOutputStream createAttachmentInStream(DocumentUpload uplodedDoc){

		blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();

		String urlData = "";
		String fileName = "";

		urlData = uplodedDoc.getUrl();
		logger.log(Level.SEVERE, "Doc URL :::: " + urlData);

		fileName = uplodedDoc.getName();
		logger.log(Level.SEVERE, "Document Name :::: " + fileName);

		String[] res = null;
		/**
		 * @author Vijay Chougule
		 * Des :- Bug :- PTSPL EmailCustomizedAndGenericSRCopy email issue becuase blob key parameter contains as blobkey=
		 * so added if condition and old code added in else block;
		 */
		if(urlData.contains("blobkey=")) {
			res = urlData.split("blobkey=", 0);
		}
		else {
			res = urlData.split("blob-key=", 0);
		}
//		String[] res = urlData.split("blob-key=", 0);
		logger.log(Level.SEVERE, "Splitted Url :::: " + res);

		String blob = res[1].trim();
		logger.log(Level.SEVERE,"Splitted Url Assigned to string  :::: " + blob);

		BlobKey blobKey = null;
		try {
			blobKey = new BlobKey(blob);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);

		// This method read bytes from blobstore
		long inxStart = 0;
		long inxEnd = 1024;
		boolean flag = false;
		byte[] bytes = null;
		do {
			try {
				bytes = blobstoreService.fetchData(blobKey,inxStart, inxEnd);
				byteStream.write(bytes);

				if (bytes.length < 1024)
					flag = true;

				inxStart = inxEnd + 1;
				inxEnd += 1025;

			} catch (Exception e) {
				flag = true;
			}

		} while (!flag);
		
		return byteStream;
	
	}
	
	/** date 11.4.2019 added by komal for common method (gmail code)
	 * 
	 */
	public void sendMailWithGmail(String fromEmail,ArrayList<String> toList,ArrayList<String> ccList,
			ArrayList<String> bccList,String subject,String mailBody,String contentType,
			ByteArrayOutputStream attachmentInBytes,String attachmentName, String attachmentContentType){
		System.out.println("Inside Try Block");
		
		try{
		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.host", "smtp.gmail.com");
		Session session = Session.getDefaultInstance(props);
		// Session session=Session.getDefaultInstance(props,null);
		Message message = new MimeMessage(session);
		Multipart multiPart = new MimeMultipart();

		System.out.println("Company Email: " + fromEmail);
		logger.log(Level.SEVERE, "Company Email" + fromEmail);

		// To Mail Address
		message.setFrom(new InternetAddress(fromEmail));
		message.setReplyTo(new InternetAddress[] { new InternetAddress(
				fromEmail) });

		// Email to Multiple Recipients
		String[] toEmailIdArray = new String[toList.size()];
		toList.toArray(toEmailIdArray);
		InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
		for (int i = 0; i < toEmailIdArray.length; i++) {
			
			mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
			System.out.println("Email id multipart " + mailToMultiple[i]);
			logger.log(Level.SEVERE, "Email id multipart "
					+ mailToMultiple[i]);
		}
		message.setRecipients(Message.RecipientType.TO, mailToMultiple);
		
		for (int i = 0; i < toList.size(); i++) {
			if(ccList!=null){
				for (int j = 0; j < ccList.size(); j++) {
					if (toList.get(i).contains(ccList.get(j))) {
						ccList.remove(j);
					}
				}
			}
			
		}

		InternetAddress[] mailToCC = null;
		if(ccList!=null){
			String[] ccEmailIdArray = new String[ccList.size()];
			ccList.toArray(ccEmailIdArray);
			 mailToCC = new InternetAddress[ccEmailIdArray.length];
			for (int i = 0; i < ccEmailIdArray.length; i++) {
				
				mailToCC[i] = new InternetAddress(ccEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);
				logger.log(Level.SEVERE, "Email id multipart "
						+ mailToCC[i]);
			}
		}
		
		if(mailToCC!=null)
		message.setRecipients(Message.RecipientType.CC, mailToCC);

	
		message.setSubject(subject);

	
		MimeBodyPart htmlpart = new MimeBodyPart();
		htmlpart.setContent(mailBody, contentType);
		multiPart.addBodyPart(htmlpart);
		
	

		// Mail attachment
		if (attachmentName != null) {
			MimeBodyPart attachbody = new MimeBodyPart();
			System.out.println("Inside Attachment.........");
			logger.log(Level.SEVERE, "Inside Attachment.........");
			try {
				attachbody.setFileName(attachmentName);//file name should be abc.pdf
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			DataSource src = new ByteArrayDataSource(
					attachmentInBytes.toByteArray(), attachmentContentType);
			attachbody.setDataHandler(new DataHandler(src));
			multiPart.addBodyPart(attachbody);

			
		}

		System.out.println("Ready To Send Mail");
		logger.log(Level.SEVERE, "Ready To Send Mail" + multiPart);
		message.setContent(multiPart);
		logger.log(Level.SEVERE, "Ready To Send Mail");
		Transport.send(message);
		logger.log(Level.SEVERE, "Email sent Successfully !!!!");
		System.out.println("Email sent Successfully !!!!");
	}catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}

	
	/**Date 13-1-2020 by AMOL to Send Salary Slip Through Email to Employee**/
	
	
	@Override
	public void sendSalarySlipOnMail(PaySlip payslip)throws IllegalArgumentException {
		
		logger.log(Level.SEVERE, "Inside inititateSalarySlipEmail print Method");
		setSalarySlip(payslip);
	    createSalarySlipAttachment();
		initializeSalarySlip();
		
	}

	private void initializeSalarySlip() {
		logger.log(Level.SEVERE, "Inside initializeSalarySlip Method");
		emp = ofy().load().type(Employee.class).filter("companyId",payslip.getCompanyId()).filter("count", payslip.getEmpid()).first().now();
		
		comp = ofy().load().type(Company.class).filter("companyId", emp.getCompanyId()).first().now();
		String empEmail="";
		if(emp!=null&&emp.getEmail()!=null){
		empEmail = emp.getEmail().trim();
		}else{
			empEmail="";
		}
		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add(empEmail);
		
		
		String title="SalarySlip Of "+emp.getFullname()+ " For the Month Of "+payslip.getSalaryPeriod();
		
		String mailTitle="SalarySlip";
		
		
		String	salarySlip="SalarySlip For The "+payslip.getSalaryPeriod();
		
		String empname="Dear "+payslip.getEmployeeName()+","+"<br></br>";
		logger.log(Level.SEVERE, "EMPLOYEE NAME"+empname);
		
		String msg=empname+"      This is the System generated SalarySlip For the Month of "+payslip.getSalaryPeriod()+" From "+comp.getBusinessUnitName();
		logger.log(Level.SEVERE, "MESSAGE"+msg);	

		String thankyou="Kindly Find The atttachment";
		String[] uploadDocName = {payslip.getEmployeeName()+" for the Month of "+payslip.getSalaryPeriod()};
		
		try {
			logger.log(Level.SEVERE, "Inside Try");
			sendNewEmail(toEmailList,salarySlip,mailTitle,
					payslip.getCount(), title,
					payslip.getCreationDate(), comp, null,
					null, null,null , null, null,
					msg, null, thankyou, uploadDocName, null,null,null);
		} catch (IOException e) {
			e.printStackTrace();
		}}

	private void createSalarySlipAttachment() {
		logger.log(Level.SEVERE, "Inside createSalarySlipAttachment Method");	
		payrollPdfUpdated = new PayrollPdfUpdated();
		payrollPdfUpdated.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(payrollPdfUpdated.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		payrollPdfUpdated.document.open();
		payrollPdfUpdated.setPaySlip(payslip.getId(),payslip.getPaySlipPrintHelper());
		payrollPdfUpdated.createPdf();
		payrollPdfUpdated.document.close();
   }

	private void setSalarySlip(PaySlip payslip) {
		logger.log(Level.SEVERE, "Inside SetSalarySlip Method");
	     this.payslip = payslip;}

	@Override
	public void initiateContractRenewalNewEmail(Contract contract)throws IllegalArgumentException {
		logger.log(Level.SEVERE, "Inside initiateContractRenewalNewEmail Server ");
		setContractRenewalNew(contract);

		initializeContractRenewalNew();
		
	}

	private void initializeContractRenewalNew() {
		boolean companyAsLetterHead =false;
		logger.log(Level.SEVERE, "Inside initializeContractRenewalNew Server ");
		if (con.getCompanyId() != null) {
			comp = ofy().load().type(Company.class)
					.filter("companyId", con.getCompanyId()).first()
					.now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		if (con.getCompanyId() != null) {
			c = ofy().load().type(Customer.class)
					.filter("count", con.getCustomerId())
					.filter("companyId", con.getCompanyId()).first()
					.now();
		} else {
			c = ofy().load().type(Customer.class)
					.filter("count", con.getCustomerId()).first().now();
		}
		oldCon=ofy().load().type(Contract.class).filter("companyId", con.getCompanyId()).filter("count", con.getRefContractCount()).first().now();
		
		if (con.getCompanyId() != null) {
			con = ofy().load().type(Contract.class)
					.filter("count", con.getCount())
					.filter("companyId", con.getCompanyId()).first()
					.now();
		} else {
			con = ofy().load().type(Contract.class)
					.filter("count", con.getCount()).first().now();
		}

		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add(c.getEmail() + "");

		String msgBody = "</br><p> Dear "
				+ c.getFullname().trim()
				+ ",</p></br>"
				+ "<p>It has been our privilege to have served you over the past year and we truly appreciated & value our association with you. we trust you have found our services exemplery & to your complete satifaction. Your current pest management contract expires on <b>"
				+ fmt.format(oldCon.getEndDate())
				+ "</b> and we request you to renew the same.</p></br></br>";

		String mailSubject = "Subject : Renewal of Contract Id "
				+ oldCon.getCount() + "." + "</br></br>";

		String footerMsg = "";
		boolean flag = false;
		for (int i = 0; i < con.getItems().size(); i++) {
			if (con.getItems().get(i).getPrice() > con.getItems()
					.get(i).getOldProductPrice()) {
				flag = true;
				break;
			}
		}
		if (flag == true) {
			footerMsg = "<p>Kindly note that we have been compelled to increase our annual services charges due to increase in our operational cost. we reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon. Should you require further information on above.Please do give us a call and we would gladly assist you.<p></br></br> ";
		}

		// Table header 1
		ArrayList<String> tbl1_header = new ArrayList<String>();
		tbl1_header.add("CODE");
		tbl1_header.add("NAME");
		tbl1_header.add("DURATION");
		tbl1_header.add("SERVICES");
		tbl1_header.add("PRICE");
//		tbl1_header.add("S.TAX");
//		tbl1_header.add("VAT");
		tbl1_header.add("CGST/SGST");

		System.out.println("Tbl1 header Size :" + tbl1_header.size());

		List<SalesLineItem> productList = new ArrayList<SalesLineItem>();
		productList = con.getItems();
		
		double consolidatedPrice=0;
		if(con.isConsolidatePrice()){
			for (int i = 0; i < productList.size(); i++) {
				consolidatedPrice=consolidatedPrice+productList.get(i).getPrice();
			}
		}
		
		
		
		// Table 1
		ArrayList<String> tbl1 = new ArrayList<String>();
		for (int i = 0; i < productList.size(); i++) {
			tbl1.add(productList.get(i).getProductCode());
			tbl1.add(productList.get(i).getProductName());
			tbl1.add(productList.get(i).getDuration() + "");
			tbl1.add(productList.get(i).getNumberOfServices() + "");
//			tbl1.add(productList.get(i).getPrice() + "");
			if(con.isConsolidatePrice()) {
				tbl1.add(consolidatedPrice + "");
			}else {
				tbl1.add(productList.get(i).getPrice() + "");
			}
//			tbl1.add(productList.get(i).getServiceTax() + "");
//			tbl1.add(productList.get(i).getVatTax() + "");
			tbl1.add(productList.get(i).getServiceTax() + "/"+productList.get(i).getVatTax());
		}
		System.out.println("Tbl1 Size :" + tbl1.size());

		
		/***
		 * consolidate invoice details to be set in social info of company object
		 */
		
		if(con.isConsolidatePrice()){
			SocialInformation socialInfo=new SocialInformation();
			socialInfo.setFaceBookId("Consolidated Contract Renewal");
			socialInfo.setTwitterId(con.getItems().size()+"");
			socialInfo.setGooglePlusId(4+"");
			comp.setSocialInfo(socialInfo);
		}else{
			logger.log(Level.SEVERE, "Consolidated Contract Renewal false");
			comp.setSocialInfo(null);
		}
		 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "CompanyAsLetterHead", comp.getCompanyId())){
				companyAsLetterHead=true;
			}
       logger.log(Level.SEVERE, "companyAsLetterHead value11 "+companyAsLetterHead);
		createContractRenewalNewAttachment(companyAsLetterHead);
		String attachFile = "RenewalLetter";
		sendContractRenewalNewFormatEmail(toEmailList, "Contract Renewal",
				"Contract Renewal", oldCon.getCount(),
				"Contract Renewal", oldCon.getEndDate(), comp, mailSubject,
				msgBody, footerMsg, attachFile, tbl1_header, tbl1);

	}

	private void sendContractRenewalNewFormatEmail(ArrayList<String> toEmail,
			String mailSubject, String mailTitle, int documentId,
			String documentTitle, Date documnetDate, Company company,
			String msgSub, String msgBody, String footerMsg,
			String attachFiles, ArrayList<String> table1_Header,
			ArrayList<String> table1) {

		logger.log(Level.SEVERE, "Inside sendContractRenewalNewFormatEmail Server ");
		// to Email id's in array
		String[] toEmailIdArray = new String[toEmail.size()];
		toEmail.toArray(toEmailIdArray);

		System.out.println("Mail Sub : " + mailSubject);
		System.out.println("Mail Title : " + mailTitle);
		System.out.println("Mail Doc title : " + documentTitle);
		System.out.println("Mail Doc Date : " + fmt.format(documnetDate));

		StringBuilder builder = new StringBuilder();

		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("<h3 class=\"ex\" align=\"center\">" + mailTitle
				+ "</h3></br></br>");
		builder.append("<B>Contract Id :</B>" + documentId
				+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "<B>Title :</B>"
				+ documentTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
				+ "<B>Contract Date : </B>" + fmt.format(documnetDate)
				+ "</br></br>");

		builder.append(" <p>  </p> </br></br>");

		/************************ Table 1 ***************************************/

//		if (table1 != null && !table1.isEmpty()) {
//			System.out.println("Table  1");
//			// builder.append("<h2>"+heading1+"</h2>");
//
//			builder.append("</br></br></br><table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
//			builder.append("<tr>");
//
//			// Product Table1 heading
//			for (String header : table1_Header)
//				builder.append("<th>" + header + "</th>");
//			builder.append("</tr>");
//
//			System.out.println("Size of table 1 " + table1.size());
//
//			builder.append("<tr>");
//			for (int i = 0; i < table1.size(); i++) {
//				builder.append("<td>" + table1.get(i) + "</td>");
//				System.out.println("Table Started 1" + table1.get(i));
//				if (i > 0 && (i + 1) % table1_Header.size() == 0) {
//					builder.append("</tr><tr>");
//				}
//			}
//			builder.append(" </tr>  ");
//			builder.append("</table></br></br></br>");
//		}

if (table1 != null && !table1.isEmpty()) {
	System.out.println("Table  1");
//	builder.append("<h2>" + heading1 + "</h2>");

	builder.append("</br></br></br><table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
	builder.append("<tr>");

	// Product Table1 heading
	for (String header : table1_Header)
		builder.append("<th>" + header + "</th>");
	builder.append("</tr>");

	System.out.println("Size of table 1 " + table1.size());

	/**
	 * write your logic for rowspan of price column
	 */

	if (comp != null&& comp.getSocialInfo() != null&& comp.getSocialInfo().getFaceBookId().equalsIgnoreCase("Consolidated Contract Renewal")) {

		logger.log(Level.SEVERE, "consolidated invoice True ");
		double sumOfInvoiceRate = 0;
		int col = 0;
		int actualCol = 0;
		int totalRow = 0;
		if (comp.getSocialInfo().getTwitterId() != null&& !comp.getSocialInfo().getTwitterId().equals("")) {
			totalRow = Integer.parseInt(comp.getSocialInfo().getTwitterId());
		}
		if (comp.getSocialInfo().getGooglePlusId() != null&& !comp.getSocialInfo().getGooglePlusId().equals("")) {
			actualCol = Integer.parseInt(comp.getSocialInfo().getGooglePlusId());
		}
		logger.log(Level.SEVERE, "TotalRow Size " + totalRow);
		logger.log(Level.SEVERE, "Col No " + actualCol);
		boolean rowspanFlag = false;
		boolean rowspanFlag1 = false;

		builder.append("<tr>");
		for (int i = 0; i < table1.size(); i++) {
//			builder.append("<td></td>");
			if (actualCol == col) {
				logger.log(Level.SEVERE, "actualCol " + actualCol);
				logger.log(Level.SEVERE, "Col " + col);
				logger.log(Level.SEVERE, "rowspanFlag " + rowspanFlag);
				if (rowspanFlag == false) {
					builder.append("<td rowspan=\""+comp.getSocialInfo().getTwitterId()+"\">"+ table1.get(i) + "</td>");
					rowspanFlag = true;
				}
			}else if((actualCol+1) == col){
				logger.log(Level.SEVERE, "Col " + col);
				logger.log(Level.SEVERE, "rowspanFlag " + rowspanFlag1);
				if (rowspanFlag1 == false) {
					builder.append("<td rowspan=\""+comp.getSocialInfo().getTwitterId()+"\">"+ table1.get(i) + "</td>");
					rowspanFlag1 = true;
				}
			}else {
				builder.append("<td>" + table1.get(i) + "</td>");
			}
			col++;

			logger.log(Level.SEVERE, "Coloumn count" + col);
			System.out.println("Table Started 1" + table1.get(i));

			if (i > 0 && (i + 1) % table1_Header.size() == 0) {
				builder.append("</tr><tr>");
				logger.log(Level.SEVERE, "Break ");
				col = 0;
			}

		}

		builder.append("</tr>");

	} else {
		logger.log(Level.SEVERE, "consolidated contract renewal false 2");
		builder.append("<tr>");
		for (int i = 0; i < table1.size(); i++) {
			builder.append("<td>" + table1.get(i) + "</td>");
			System.out.println("Table Started 1" + table1.get(i));
			if (i > 0 && (i + 1) % table1_Header.size() == 0) {
				builder.append("</tr><tr>");
			}
		}
		builder.append(" </tr>  ");
	}

//	builder.append("</table></b></br>");
	builder.append("</table></br></br></br>");
}




		builder.append(msgSub);
		builder.append(" <B>   </br></br></B>");
		builder.append(msgBody);
		builder.append(" <B>   </br></br></B>");
		builder.append(footerMsg);
		builder.append(" <B>   </br></br></B>");
		// Company Details
		builder.append("</br><b>" + company.getBusinessUnitName() + "</b></br>");
		builder.append("<table>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Address : ");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getAddrLine2().equals("")
				&& company.getAddress().getAddrLine2() != null)
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getAddrLine2() + " "
					+ company.getAddress().getLocality());
		else
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getLocality());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getLandmark().equals("")
				&& company.getAddress().getLandmark() != null)
			builder.append(company.getAddress().getLandmark() + ","
					+ company.getAddress().getCity() + ","
					+ company.getAddress().getPin()
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		else
			builder.append(company.getAddress().getCity() + ","
					+ company.getAddress().getPin() + ","
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Website : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getWebsite());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Phone No : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getCellNumber1());
		builder.append("</td>");
		builder.append("<tr>");

		builder.append("</tr>");
		builder.append("</table>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);

		try {
			System.out.println("Inside Try Block");
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company.getEmail());

			// To Mail Address
			message.setFrom(new InternetAddress(company.getEmail()));
			message.setReplyTo(new InternetAddress[] { new InternetAddress(
					company.getEmail()) });

			// Email to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);
			message.setRecipient(Message.RecipientType.CC, new InternetAddress(
					company.getEmail()));

			// Mail Subject
			message.setSubject(mailSubject);

			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentInHtml, "text/html");
			multiPart.addBodyPart(htmlpart);

			// Mail attachment
			if (attachFiles != null) {
				System.out.println("Inside Attachment.........");

				MimeBodyPart attachbody = new MimeBodyPart();
				attachbody.setFileName(attachFiles + ".pdf");
				DataSource src = new ByteArrayDataSource(
						pdfstream.toByteArray(), "application/pdf");
				attachbody.setDataHandler(new DataHandler(src));
				multiPart.addBodyPart(attachbody);
			}

			System.out.println("Ready To Send Mail");

			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			logger.log(Level.SEVERE, " Contract renewal new format Email sent Successfully  ");
			System.out.println("Email sent Successfully !!!!");
		} catch (Exception e) {
			logger.log(Level.SEVERE, " Email Fail ");
			e.printStackTrace();
			
		}
	}

	public void createContractRenewalNewAttachment(boolean companyAsLetterHead) {
		/**
		 *  Date : 15-04-2021 Added By : Priyanka
		 *  Des : Conract renewal new  pdf email remainder option . 
		 */
		logger.log(Level.SEVERE, "Inside createContractRenewalNewAttachment Server ");
		conRenwalNew = new NewContractRenewalVersionOnePdf();
		conRenwalNew.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(conRenwalNew.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		conRenwalNew.document.open();
		oldCon=ofy().load().type(Contract.class).filter("companyId", con.getCompanyId()).filter("count", con.getRefContractCount()).first().now();
		
		logger.log(Level.SEVERE, "companyAsLetterHead value22  "+companyAsLetterHead);
		if(companyAsLetterHead){
			conRenwalNew.createPdfAttachment(con, comp, oldCon, c,"no");
		}else{
			conRenwalNew.createPdfAttachment(con, comp, oldCon, c,"plane");
		}
		
		conRenwalNew.document.close();
		
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "ContractRenewalNewFormat", comp.getCompanyId())){
		logger.log(Level.SEVERE, "Inside createContractRenewalNewAttachment Server ");
		conRenwalNewOne = new NewContractRenewalPdf();
		conRenwalNewOne.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(conRenwalNewOne.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		conRenwalNewOne.document.open();
		oldCon=ofy().load().type(Contract.class).filter("companyId", con.getCompanyId()).filter("count", con.getRefContractCount()).first().now();
		
		logger.log(Level.SEVERE, "companyAsLetterHead value22  "+companyAsLetterHead);
		if(companyAsLetterHead){
			conRenwalNewOne.createPdfAttachment(con, comp, oldCon, c,"no");
		}else{
			conRenwalNewOne.createPdfAttachment(con, comp, oldCon, c,"plane");
		}
		conRenwalNewOne.document.close();
		}
		
		/**
		 *  End
		 */
	}

	public void setContractRenewalNew(Contract contract) {
		logger.log(Level.SEVERE, "Inside setContractRenewalNew Server ");
      con=contract;		
	}

	@Override
	public void initiateComplainEmail(Complain complain) {
		// TODO Auto-generated method stub
		createComplainPdfAttachment(complain);
		sendComplainMail(complain);
	}			
	
	private void createComplainPdfAttachment(Complain complain) {

		ComplainPdf complainPdf = new ComplainPdf();
		complainPdf.document = new Document();
		pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(complainPdf.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		complainPdf.document.open();
		complainPdf.getComplain(complain.getId());
		complainPdf.createPdf();
		complainPdf.document.close();
	}
	
	public void sendComplainMail(Complain complain) {
		if (complain.getCompanyId() != null) {
			comp = ofy().load().type(Company.class).id(complain.getCompanyId()).now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}
		
		Customer customer=ofy().load().type(Customer.class).filter("companyId",complain.getCompanyId()).filter("count",complain.getPersoninfo().getCount()).first().now();
		CustomerBranchDetails customerBranchDetails=null;
		if(complain.getCustomerBranch()!=null&&!complain.getCustomerBranch().equals("")){
			customerBranchDetails=ofy().load().type(CustomerBranchDetails.class).filter("companyId",complain.getCompanyId()).filter("buisnessUnitName",complain.getCustomerBranch()).filter("cinfo.count",complain.getPersoninfo().getCount()).first().now();
		}

		
		// Employee email id is added to emailList
		ArrayList<String> toEmailList = new ArrayList<String>();
		if(customerBranchDetails!=null){
			if(customerBranchDetails.getEmail()!=null&&!customerBranchDetails.getEmail().equals("")){
				toEmailList.add(customerBranchDetails.getEmail()+"");
			}else{
				if(customer!=null){
					toEmailList.add(customer.getEmail()+"");
				}
			}
		}else{
			if(customer!=null){
				toEmailList.add(customer.getEmail()+"");
			}
		}
		
		String customerName="";
		String fname="";
		if(customer!=null){
			if(customer.getCustCorresponence()!=null&&!customer.getCustCorresponence().equals("")){
				customerName=customer.getCustCorresponence();
			}else{
				customerName=customer.getFullname();
			}
		}
		fname=customerName;
		
		if(customerBranchDetails!=null){
			if(customerBranchDetails.getPocName()!=null&&!customerBranchDetails.getPocName().equals("")
					&&customer.getEmail()!=null&&!customer.getEmail().equals("")){
				customerName=customerBranchDetails.getPocName();
			}
		}
		
		if(customer.isCompany()){
			fname=customer.getCompanyName();
		}
		/**
		 * @author Vijay Chougule Date 28-12-2020
		 * Des :- Email From Sales Person and CC in Approver Name and Branch Email id and Company Email Id
		 * Sending Emails from Sengrid
		 */
		ArrayList<String> ccEmailList = new ArrayList<String>();
		String fromEmailId = "";
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.PC_ENABLESENDEMAILSFROMPERSONRESPONSIBLE, complain.getCompanyId())) {
			ccEmailList.add(comp.getEmail());
			fromEmailId = getSalesPersonEmail(complain.getSalesPerson(),complain.getCompanyId(),complain.getBranch());
			if(fromEmailId==null) {
				fromEmailId = comp.getEmail();
			}
			String branchEmailId = getBranchEmailId(complain.getBranch(), complain.getCompanyId());
			if(branchEmailId!=null) {
				ccEmailList.add(branchEmailId);
			}
			Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", complain.getCompanyId())
									.filter("count", complain.getExistingContractId()).first().now();
			logger.log(Level.SEVERE,"contractEntity"+contractEntity);
			if(contractEntity!=null) {
				if(contractEntity.getAccountManager()!=null) {
					logger.log(Level.SEVERE,"contractEntity getAccountManager "+contractEntity.getAccountManager());
					User userEntity = ofy().load().type(User.class).filter("companyId",complain.getCompanyId())
										.filter("employeeName", contractEntity.getAccountManager()).first().now();
					logger.log(Level.SEVERE,"userEntity"+userEntity);
					if(userEntity!=null && userEntity.getEmail()!=null && !userEntity.getEmail().equals("")) {
						ccEmailList.add(userEntity.getEmail());
						logger.log(Level.SEVERE,"userEntity.getEmail()"+userEntity.getEmail());

					}
				}
			}
		}
		/**
		 * ends here
		 */
		
		
		
		String mailSub= "Customer Complaint No. "+complain.getCount();
		String mailTitle= "Complaint Details";
		String msgBody = "" + "Dear "
				+ customerName
				+ ","
				+ "<br>"+"</br>"
				+ "<br>"+"</br>"
				+ "We acknowledge receipt of your complaint and attached is the complaint ticket."
				+ "<br>"+"</br>"
				+ "We assure you of our priority attention at all time."
				+ "<br>"+"</br>" +"<br>"+ "</br>" +"<br>"+ "</br>"+ "Your Faithfully,"
				+ "<br>"+"</br>" + "Service Coordinator "+ "<br>"+"</br>";
		String fileName=fname+" - "+complain.getCount();

		sendComplainEmail(toEmailList,mailSub,mailTitle,comp,msgBody,fileName,ccEmailList,fromEmailId);

	}
	
	public void sendComplainEmail(ArrayList<String> toEmail,
			String mailSubject, String mailTitle, Company company,
			String msgBody,String fileName, ArrayList<String> ccEmailList, String fromEmailId) {

		// to Email id's in array
		String[] toEmailIdArray = new String[toEmail.size()];
		toEmail.toArray(toEmailIdArray);

		System.out.println("Mail Sub : " + mailSubject);
		System.out.println("Mail Title : " + mailTitle);
//		System.out.println("Mail Doc title : " + documentTitle);
//		System.out.println("Mail Doc Date : " + fmt.format(documnetDate));

		StringBuilder builder = new StringBuilder();

		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("<h3 class=\"ex\" align=\"center\">" + mailTitle
				+ "</h3></br></br>");
//		builder.append("<B>Document Id :</B>" + documentId
//				+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
//				+ "<B>Document Title :</B>" + documentTitle
//				+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
//				+ "<B>Document Date :</B>" + fmt.format(documnetDate)
//				+ "</h3></br></br>");

		builder.append(msgBody);

		// Company Details
		builder.append("<b>" + company.getBusinessUnitName() + "</b></br>");
		builder.append("<table>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Address : ");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getAddrLine2().equals("")
				&& company.getAddress().getAddrLine2() != null)
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getAddrLine2() + " "
					+ company.getAddress().getLocality());
		else
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getLocality());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getLandmark().equals("")
				&& company.getAddress().getLandmark() != null)
			builder.append(company.getAddress().getLandmark() + ","
					+ company.getAddress().getCity() + ","
					+ company.getAddress().getPin()
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		else
			builder.append(company.getAddress().getCity() + ","
					+ company.getAddress().getPin() + ","
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Website : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getWebsite());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Phone No : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getCellNumber1());
		builder.append("</td>");
		builder.append("<tr>");

		builder.append("</tr>");
		builder.append("</table>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);

		
		/**
		 * @author Vijay Chougule Date 28-12-2020
		 * Des :- Email From Sales Person and CC in Approver Name and Branch Email id and Company Email Id
		 * send Emails From SendGrid
		 * 
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.PC_ENABLESENDEMAILSFROMPERSONRESPONSIBLE, company.getCompanyId())) {
			
			 Company compEntity = ofy().load().type(Company.class).filter("companyId", company.getCompanyId()).first().now();
			logger.log(Level.SEVERE, "company.getEmail() "+compEntity.getEmail());
			if(compEntity.getEmail()!=null && !compEntity.getEmail().equals("")) {
				logger.log(Level.SEVERE, "company.getEmail() == "+compEntity.getEmail());
				ccEmailList.add(compEntity.getEmail());
			}
			logger.log(Level.SEVERE, "Email CC "+ccEmailList);
			logger.log(Level.SEVERE, "Email To "+toEmail);
			ArrayList<String> bcclist = new ArrayList<String>();
			bcclist.add(fromEmailId);
			
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			sdEmail.sendMailWithSendGrid(fromEmailId, toEmail, ccEmailList, bcclist, mailSubject, contentInHtml, "text/html",pdfstream,fileName,"application/pdf",null);
			return;
		}
		/**
		 * ends here
		 */
		
		try {
			System.out.println("Inside Try Block");
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company.getEmail());

			// To Mail Address
			message.setFrom(new InternetAddress(company.getEmail()));
			message.setReplyTo(new InternetAddress[] { new InternetAddress(
					company.getEmail()) });

			// Email to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);
			message.setRecipient(Message.RecipientType.CC, new InternetAddress(
					company.getEmail()));

			// Mail Subject
			message.setSubject(mailSubject);

			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentInHtml, "text/html");
			multiPart.addBodyPart(htmlpart);

			 // Mail attachment
			 if(fileName!=null){
				 System.out.println("Inside Attachment.........");
				 MimeBodyPart attachbody=new MimeBodyPart();
				 attachbody.setFileName(fileName+".pdf");
				 DataSource src=new ByteArrayDataSource(pdfstream.toByteArray(),"application/pdf");
				 attachbody.setDataHandler(new DataHandler(src));
				 multiPart.addBodyPart(attachbody);
			 }

			System.out.println("Ready To Send Mail");

			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			System.out.println("Email sent Successfully !!!!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public String sendCompanyProfileToLead(Lead lead) {
		logger.log(Level.SEVERE, "Inside sendCompanyProfileToLead Server ");
		EmailTemplate emailtemplate=ofy().load().type(EmailTemplate.class).filter("companyId", lead.getCompanyId()).filter("templateName", "Share Company Profile").filter("templateStatus",true).first().now();
        if(emailtemplate!=null){
        	createCompanyProfileAttachment(lead);
        	intializeSendToEmail(lead);
        	return "Email Sent Sucessfully !";
        }
        else
        	return "'Share Company Profile' email template is not defined or inactive.";
	}
	
	private String toCamelCase(String s) {
        String[] parts = s.split(" ");
        String camelCaseString = "";
        for (String part : parts){
            if(part!=null && part.trim().length()>0)
           camelCaseString = camelCaseString + toProperCase(part);
            else
                camelCaseString=camelCaseString+part+" ";   
        }
        return camelCaseString;
     }
	
	private String toProperCase(String s) {
        String temp=s.trim();
        String spaces="";
        if(temp.length()!=s.length())
        {
        int startCharIndex=s.charAt(temp.indexOf(0));
        spaces=s.substring(0,startCharIndex);
        }
        temp=temp.substring(0, 1).toUpperCase() +
        spaces+temp.substring(1).toLowerCase()+" ";
        return temp;

    }

	private void intializeSendToEmail(Lead lead) {
		
		logger.log(Level.SEVERE, "Inside intializeSendToEmail Server ");
		
		if (lead.getCompanyId() != null) {
			c = ofy().load().type(Customer.class)
					.filter("count", lead.getPersonInfo().getCount())
					.filter("companyId", lead.getCompanyId()).first()
					.now();
		} else {
			c = ofy().load().type(Customer.class)
					.filter("count", lead.getPersonInfo().getCount()).first()
					.now();
		}
		if (lead.getCompanyId() != null) {
			comp = ofy().load().type(Company.class).filter("companyId", lead.getCompanyId()).first().now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}
		
		String custEmail = c.getEmail().trim();
		logger.log(Level.SEVERE, "customer email  "+c.getEmail());
		
		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add(custEmail);
		
		String fromEmailId=comp.getEmail();
				
		String lb="\n";
		lb="<br>";
		
		String liOpen="<li>";
		String liClose="</li>";
		
		
//		String mailSub="Grow & Run Pest Control Business With Advanced Technology";;
//		
//		String mailBody="Dear Mr. "+toCamelCase(lead.getPersonInfo().getFullName())+","+lb+lb+
//		"We help visionary pest control corporate to put the business on auto-pilot with complete EVA Pest Control CRM/ERP suite. Currently, more than 100 pest controllers are using "+lb+"EVA Pest Pro. It would allow you to"+
//		
//		lb+lb+"<ul>"+liOpen+"Reduce Un-billed  & Billed outstanding"+liClose
//	   +lb+liOpen+"Complete 99.9 % Customer Services/ Delivery  On Time"+liClose
//	    +lb+liOpen+"Track Technician & His Performance"+"\n"+"Retain 99.98 % Of Your Orders"+liClose+
//	    lb+liOpen+"Increase Sale 25 % YoY"+liClose+
//	    lb+liOpen+" Optimize Inventory & Generate Stock Alert Automatically "+liClose
//	    +lb+liOpen+"Contract Profit & Loss Compliance with GST With Tally Integration Compliance with GST With Tally Integration "+liClose
//	    +lb+liOpen+"Report & Monitor Real-Time Attendance, Process Payroll In Just 1 Day & Generate Payroll Compliance Reports Automatically, "+liClose
//	    +lb+liOpen+"Process Payroll In Just 1 Day & Generate Payroll Compliance Reports Automatically,"+liClose
//	    +lb+liOpen+"Track & Manage Technician from your mobile phone - Technician Management App,"+liClose+"</ul>"
//		
//	    
//	    +lb+lb+"You can watch our software videos on YouTube by subscribing to our YouTube channel EVA Software Solutions." 
//		+lb+lb+"We have ERP/CRM comprehensive software suite to manage your business operations like sales, services, inventory, procurement,HR. We also provide website development,"+lb+" e-commerce & digital marketing. "
//		+lb+ lb+"The software has been designed with more than 20 years of IT experience with corporate like Accenture, Capgemini & L&T Infotech. Experience with Fortune 500 companies "+lb+"with SAP in the world has helped to come up with a robust & faster implementation methodology."
//        +lb+lb+ "I would be very happy to look at automating your business process to set the platform for exponential growth & reduce operating cost. Refer attached company introduction "+lb+"& our achievements so far."
//		+lb+lb+"Do let me know when we can walk you through the Software."
//		+lb+lb+lb+" Thanking you in advance, "+lb;
//		
		
		EmailTemplate emailtemplate=ofy().load().type(EmailTemplate.class).filter("companyId", comp.getCompanyId()).filter("templateName", "Share Company Profile").filter("templateStatus",true).first().now();
        if(emailtemplate!=null){
        	logger.log(Level.SEVERE, "In email template Share Company Profile");
        	String mailSub = emailtemplate.getSubject();
        	String companyName=ServerAppUtility.getCompanyName(lead.getBranch(), comp);
        	mailSub = mailSub.replace("{CompanyName}", companyName);
        	mailSub = mailSub.replace("{CustomerName}", lead.getPersonInfo().getFullName());
             
        	String mailBody = emailtemplate.getEmailBody();
        	mailBody = mailBody.replaceAll("[\n]", "<br>");
			mailBody = mailBody.replaceAll("[\\s]", "&nbsp;");
			
        	mailBody = mailBody.replace("{CompanyName}", companyName);
        	mailBody = mailBody.replace("{CustomerName}", lead.getPersonInfo().getFullName());
        	mailBody = mailBody.replace("{CompanySignature}", ServerAppUtility.getCompanySignature(lead.getBranch(),comp.getCompanyId() ));
        	mailBody = mailBody.replace("{DocumentID}",lead.getCount()+"");
        	mailBody = mailBody.replace("{DocumentDate}",dateFormat.format(lead.getCreationDate()));
        	
        	
        	try {
    			logger.log(Level.SEVERE,
    					"Inside try calling Method...  sending t&c attachment and custom quotation doc");
//    			fileName[0] = uploadTandC;
//    			sendNewEmail(toEmailList, mailSub, "Company Profile",
//    					lead.getCount(), "Service Lead",
//    					lead.getCreationDate(), comp,
//    					"Product Details", null, null,
//    					"Payment Terms", null, null, mailBody, null,
//    					null, null, null,null,fromEmailId);
    			sendNewEmail(toEmailList, mailSub, "",
    					0, "Service Lead",
    					lead.getCreationDate(), comp,
    					"Product Details", null, null,
    					"Payment Terms", null, null, mailBody, null,
    					null, null, null,null,fromEmailId);
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
        }else
        	logger.log(Level.SEVERE, "Email template 'Share Company Profile' not found");
    	

	}

	private void createCompanyProfileAttachment(Lead lead) {
		logger.log(Level.SEVERE, "Inside createCompanyProfileAttachment Server ");
		
		if (lead != null && lead.getCategory() != null
				&& !lead.getCategory().equals("")) {
			
			logger.log(Level.SEVERE, "Inside lead category not null ");

			CompanyProfileConfiguration compProfileConfig = ofy().load()
					.type(CompanyProfileConfiguration.class)
					.filter("leadCategory", lead.getCategory())
					.filter("companyId", lead.getCompanyId()).first().now();
			if (compProfileConfig != null) {
				logger.log(Level.SEVERE, "Inside compProfileConfig not null ");
				String urlData1 = "";
				String fileName1 = "";

				uploadCompanyProfile = new ByteArrayOutputStream();
				blobstoreService = BlobstoreServiceFactory
						.getBlobstoreService();

				if (compProfileConfig.getCompProfileConfig() != null) {
					urlData1 = compProfileConfig.getCompProfileConfig()
							.getUrl();
					logger.log(Level.SEVERE, "Doc URL :::: " + urlData1);

					fileName1 = compProfileConfig.getCompProfileConfig()
							.getName();
					logger.log(Level.SEVERE, "Document Name :::: " + fileName1);

					String[] res1 = urlData1.split("blob-key=", 0);
					logger.log(Level.SEVERE, "Splitted Url :::: " + res1);

					String blob1 = res1[1].trim();
					logger.log(Level.SEVERE,
							"Splitted Url Assigned to string  :::: " + blob1);

					BlobKey blobKey1 = null;
					try {
						blobKey1 = new BlobKey(blob1);
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					}
					logger.log(Level.SEVERE, "BlobKey1  :::: " + blobKey1);

					// This method read bytes from blobstore
					long inxStart = 0;
					long inxEnd = 1024;
					boolean flag = false;
					byte[] bytes = null;
					do {
						try {
							bytes = blobstoreService.fetchData(blobKey1,
									inxStart, inxEnd);
							// uploadStream.write(bytes);
							logger.log(Level.SEVERE,
									"writing to custom upload stream");
							uploadCompanyProfile.write(bytes);
							if (bytes.length < 1024)
								flag = true;

							inxStart = inxEnd + 1;
							inxEnd += 1025;

						} catch (Exception e) {
							flag = true;
						}

					} while (!flag);
					logger.log(Level.SEVERE, "Calling Email Method  :::: ");
				}
			}
		}
	}
	/**
	 * @author Vijay Chougule Date 28-12-2020
	 * Des :-
	 * @param employeeName
	 * @param companyId
	 * @param branchName
	 * @return
	 */
	
	public String getSalesPersonEmail(String employeeName, Long companyId, String branchName) {
		
		User userEntity = ofy().load().type(User.class).filter("companyId", companyId)
							.filter("employeeName", employeeName).first().now();
		logger.log(Level.SEVERE, "userEntity"+userEntity);
		if(userEntity!=null) {
			if(userEntity.getEmail()!=null && !userEntity.getEmail().equals("")) {
				return userEntity.getEmail();
			}
			else {
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", companyId)){
					
					String branchEmailId = getBranchEmailId(branchName,companyId);
					if(branchEmailId!=null) {
						return branchEmailId;
					}
				}
			}
		}
		return null;
	}
	
	public String getBranchEmailId(String branchName, Long companyId) {
		Branch branchEntity = ofy().load().type(Branch.class).filter("companyId", companyId)
				.filter("buisnessUnitName", branchName).first().now();
		logger.log(Level.SEVERE, "branchEntity"+branchEntity);
		if(branchEntity!=null) {
			if(branchEntity.getEmail()!=null && !branchEntity.getEmail().equals("")) {
				return branchEntity.getEmail();
			}
		}
		return null;
	}

	public String getApproverEmailId(String approverName, Long companyId) {
		User userEntity = ofy().load().type(User.class).filter("companyId", companyId)
				.filter("employeeName", approverName).first().now();
		logger.log(Level.SEVERE, "userEntity"+userEntity);
		if(userEntity!=null && userEntity.getEmail()!=null && !userEntity.getEmail().equals("")) {
			return userEntity.getEmail();
		}
		return null;
	}

	/**
	 * @author Vijay Date :- 19-03-2021
	 * Des :- Common Method to Send Email Using Gmail Google App Engine Email API
	 * and Using Sendgrid Email Fuctionality with process configuration.
	 * New Email Fuctionality with Email PopUp
	 */
	
	@Override
	public String sendEmail(EmailDetails emailDetails, long companyId) {
		logger.log(Level.SEVERE, "Inside sendEmail Server ");
		
		if(emailDetails.getUploadedDocument()!=null || emailDetails.getEntityName().trim().equals("Service")) {

			if(emailDetails.getUploadedDocument()!=null){
				
			blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
			ByteArrayOutputStream byteStream = new ByteArrayOutputStream();

			String urlData = "";
			String fileName = "";

			urlData = emailDetails.getUploadedDocument().getUrl();
			logger.log(Level.SEVERE, "Doc URL :::: " + urlData);

			fileName = emailDetails.getUploadedDocument().getName();
			logger.log(Level.SEVERE, "Document Name :::: " + fileName);

			String[] res = null;
			/**
			 * @author Vijay Chougule
			 * Des :- Bug :- PTSPL EmailCustomizedAndGenericSRCopy email issue becuase blob key parameter contains as blobkey=
			 * so added if condition and old code added in else block;
			 */
			if(urlData.contains("blobkey=")) {
				res = urlData.split("blobkey=", 0);
			}
			else {
				res = urlData.split("blob-key=", 0);
			}
//			String[] res = urlData.split("blob-key=", 0);
			logger.log(Level.SEVERE, "Splitted Url :::: " + res);

			String blob = res[1].trim();
			logger.log(Level.SEVERE,"Splitted Url Assigned to string  :::: " + blob);
			
			BlobKey blobKey = null;
			try {
				blobKey = new BlobKey(blob);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);
			logger.log(Level.SEVERE, "BlobKey  size :::: " + blobKey.toString().length());

			BlobInfoFactory blobInfoFactory = new BlobInfoFactory();
			BlobInfo blobInfo = blobInfoFactory.loadBlobInfo(new BlobKey(blob));
			Long blobSize = blobInfo.getSize();
			
			long blobsizeInKb = Math.round(blobSize/1024);
			logger.log(Level.SEVERE, "blobSize == " + blobSize);
			if(blobsizeInKb>10240) {
				return "Can not send email document attached file size greater than 10 mb. Attachment file size must be leass than 10 MB";
			}
			}
		
			Gson gson =  new Gson();

			String emaildetail = "";
			try {
				emaildetail=gson.toJson(emailDetails);
			}
			catch(Exception e) {
				e.printStackTrace();
			}

			String strtoemail = getEmailds(emailDetails.getToEmailId());
			String strccemail = getEmailds(emailDetails.getCcEmailId());
			String strbccemail = getEmailds(emailDetails.getBccEmailId());

			String pdfattachment = "no";
			if(emailDetails.isPdfAttachment()) {
				pdfattachment = "yes";
			}
			
			String uploadDocument = gson.toJson(emailDetails.getUploadedDocument());
			int documentId = 0;
			if(emailDetails.getModel()!=null){
				documentId = emailDetails.getModel().getCount();
			}
			if(emailDetails.getEntityName().trim().equals("Service")){
				documentId = emailDetails.getDocumentId();
			}
			
			Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
					.param("OperationName", "Send Email")
					.param("companyId", companyId+"").param("count", documentId+"")
					.param("fromEmail", emailDetails.getFromEmailid()).param("toEmail", strtoemail)
					.param("ccEmail", strccemail).param("bccEmail", strbccemail)
					.param("emailBody", emailDetails.getEmailBody()).param("subject", emailDetails.getSubject())
					.param("landingpageText", emailDetails.getLandingPageText()).param("appId", emailDetails.getAppId())
					.param("pdfattachment", pdfattachment).param("pdfURL", emailDetails.getPdfURL())
					.param("entityName", emailDetails.getEntityName()).param("uploadDocument", uploadDocument)
					.param("appURL", emailDetails.getAppURL())

					);
			logger.log(Level.SEVERE, "emaildetail"+emaildetail);
			
			return "Email sent successfully";
			
		}
		else {
			return  sendNewEmail(emailDetails,companyId,false);
		}
		
		
//		Company company = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
//		String uploadDocName = null;
//		
//		pdfstream = null;
//		boolean salesPersonEmailIdFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.PC_ENABLESENDEMAILSFROMPERSONRESPONSIBLE, companyId);
//		/**
//		 * @author Vijay Chougule Date 28-12-2020
//		 * Des :- Email From Sales Person and CC in Approver Name and Branch Email id and Company Email Id
//		 * send Emails From SendGrid
//		 * 
//		 */
//		if(salesPersonEmailIdFlag && (emailDetails.getEntityName().trim().equals("Quotation") || emailDetails.getEntityName().trim().equals("Contract") ||
//				emailDetails.getEntityName().trim().equals("SalesQuotation") || emailDetails.getEntityName().trim().equals("SalesOrder") ||
//				emailDetails.getEntityName().trim().equals("Complain") || emailDetails.getEntityName().trim().equals("PurchaseOrder"))){
//			logger.log(Level.SEVERE, "Inside cc bcc and EmailfromPersonResponsible");
//
//			ArrayList<String> ccEmailList = new ArrayList<String>();
//			ArrayList<String> bcclist = new ArrayList<String>();
//
//			if(company.getEmail()!=null){
//				ccEmailList.add(company.getEmail());
//			}
//			
//			 if(emailDetails.getEntityName().trim().equals("Quotation")){
//				Quotation quotation = (Quotation) emailDetails.getModel();
//				
//				String approverEmailId = getApproverEmailId(quotation.getApproverName(),quotation.getCompanyId());
//				if(approverEmailId!=null) {
//					ccEmailList.add(approverEmailId);
//				}
//				String branchEmailId = getBranchEmailId(quotation.getBranch(), quotation.getCompanyId());
//				if(branchEmailId!=null) {
//					ccEmailList.add(branchEmailId);
//				}
//				
//				bcclist.add(emailDetails.getFromEmailid());
//
//			 }
//			 else if(emailDetails.getEntityName().trim().equals("Contract")){
//				 Contract contract = (Contract) emailDetails.getModel();
//				 String approverEmailId = getApproverEmailId(contract.getApproverName(),contract.getCompanyId());
//					if(approverEmailId!=null) {
//						ccEmailList.add(approverEmailId);
//					}
//					String branchEmailId = getBranchEmailId(contract.getBranch(), contract.getCompanyId());
//					if(branchEmailId!=null) {
//						ccEmailList.add(branchEmailId);
//					}
//					
//					bcclist.add(emailDetails.getFromEmailid());
//			 }
//			 else if(emailDetails.getEntityName().trim().equals("SalesQuotation")){
//				 SalesQuotation salesQuotation = (SalesQuotation) emailDetails.getModel();
//				 String approverEmailId = getApproverEmailId(salesQuotation.getApproverName(),salesQuotation.getCompanyId());
//					if(approverEmailId!=null) {
//						ccEmailList.add(approverEmailId);
//					}
//					String branchEmailId = getBranchEmailId(salesQuotation.getBranch(), salesQuotation.getCompanyId());
//					if(branchEmailId!=null) {
//						ccEmailList.add(branchEmailId);
//					}
//					
//					bcclist.add(emailDetails.getFromEmailid());
//			 }
//			 else if(emailDetails.getEntityName().trim().equals("SalesOrder")) {
//			 		SalesOrder salesOrder = (SalesOrder) emailDetails.getModel();
//					String approverEmailId = getApproverEmailId(salesOrder.getApproverName(),salesOrder.getCompanyId());
//					if(approverEmailId!=null) {
//						ccEmailList.add(approverEmailId);
//					}
//					String branchEmailId = getBranchEmailId(salesOrder.getBranch(), salesOrder.getCompanyId());
//					if(branchEmailId!=null) {
//						ccEmailList.add(branchEmailId);
//					}
//			 }
//			 else if(emailDetails.getEntityName().trim().equals("Complain")){
//				 	Complain complain = (Complain) emailDetails.getModel();
//					String branchEmailId = getBranchEmailId(complain.getBranch(), complain.getCompanyId());
//					if(branchEmailId!=null) {
//						ccEmailList.add(branchEmailId);
//					}
//					Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", complain.getCompanyId())
//											.filter("count", complain.getExistingContractId()).first().now();
//					logger.log(Level.SEVERE,"contractEntity"+contractEntity);
//					if(contractEntity!=null) {
//						if(contractEntity.getAccountManager()!=null) {
//							logger.log(Level.SEVERE,"contractEntity getAccountManager "+contractEntity.getAccountManager());
//							User userEntity = ofy().load().type(User.class).filter("companyId",complain.getCompanyId())
//												.filter("employeeName", contractEntity.getAccountManager()).first().now();
//							logger.log(Level.SEVERE,"userEntity"+userEntity);
//							if(userEntity!=null && userEntity.getEmail()!=null && !userEntity.getEmail().equals("")) {
//								ccEmailList.add(userEntity.getEmail());
//								logger.log(Level.SEVERE,"userEntity.getEmail()"+userEntity.getEmail());
//
//							}
//						}
//					}
//					
//					bcclist.add(emailDetails.getFromEmailid());	
//			 }
//			 else if(emailDetails.getEntityName().trim().equals("PurchaseOrder")){
//				 PurchaseOrder po = (PurchaseOrder) emailDetails.getModel();
//				 String approverEmailId = getApproverEmailId(po.getApproverName(),po.getCompanyId());
//					if(approverEmailId!=null) {
//						ccEmailList.add(approverEmailId);
//					}
//					String branchEmailId = getBranchEmailId(po.getBranch(), po.getCompanyId());
//					if(branchEmailId!=null) {
//						ccEmailList.add(branchEmailId);
//					}
//					if(po.getRefOrderNO()!=null && !po.getRefOrderNO().equals("")) {
//						try {
//							int salesorderId = Integer.parseInt(po.getRefOrderNO());
//							SalesOrder salesorderEntity = ofy().load().type(SalesOrder.class).filter("companyId", po.getCompanyId())
//												.filter("count", salesorderId).first().now();
//							if(salesorderEntity!=null) {
//								String salesPersonemailid = getApproverEmailId(salesorderEntity.getEmployee(),salesorderEntity.getCompanyId());
//								if(salesPersonemailid!=null) {
//									ccEmailList.add(salesPersonemailid);
//								}
//							}
//						} catch (Exception e) {
//							// TODO: handle exception
//						}
//						
//					}
//					bcclist.add(emailDetails.getFromEmailid());	
//
//			 }
//			 else if(emailDetails.getEntityName().trim().equals(AppConstants.CONTRACT_RENEWAL)){
//					ContractRenewal contractRenewal = (ContractRenewal) emailDetails.getModel();
//					logger.log(Level.SEVERE, "Contract Renewal email attchment");
//					createContractRenewalEmailAttachment(contractRenewal);
//			}
//			
//			 if(ccEmailList.size()>0){
//				 emailDetails.getCcEmailId().addAll(ccEmailList);
//			 }
//			 if(bcclist.size()>0){
//				 emailDetails.getBccEmailId().addAll(bcclist);
//			 }
//				
//		}
//			
//
//		// Too Email id's in array
//		String[] toEmailIdArray = new String[emailDetails.getToEmailId().size()];
//		emailDetails.getToEmailId().toArray(toEmailIdArray);
//
//		// CC Email id's in array
//		String[] ccEmailIdArray = new String[emailDetails.getCcEmailId().size()];
//		emailDetails.getCcEmailId().toArray(ccEmailIdArray);
//		
//		// BCC Email id's in array
//		String[] bccEmailIdArray = new String[emailDetails.getBccEmailId().size()];
//		emailDetails.getBccEmailId().toArray(bccEmailIdArray);
//
//		String attachFiles = "Document";
//		String paymentGatewayUniqueId ="";
//		logger.log(Level.SEVERE,"emailDetails.getEntityName()"+emailDetails.getEntityName());
//		
//		String jsonData ="";
//		String strmsgBody = emailDetails.getEmailBody();
//		logger.log(Level.SEVERE,"strmsgBody"+strmsgBody);
//		boolean paymentGatewayRequestLinkFlag = strmsgBody.contains(AppConstants.EMAILPAYMENTGATEWAYREQUEST);
//		logger.log(Level.SEVERE,"paymentGatewayRequestLinkFlag"+paymentGatewayRequestLinkFlag);
//		if(paymentGatewayRequestLinkFlag==false){
//			paymentGatewayRequestLinkFlag = strmsgBody.contains(AppConstants.EMAILPAYMENTGATEWAYREQUESTLINK2);
//		}
//		logger.log(Level.SEVERE,"paymentGatewayRequestLinkFlag"+paymentGatewayRequestLinkFlag);
//
//		CommonServiceImpl commonserviceimpl = new CommonServiceImpl();
//		logger.log(Level.SEVERE,"emailDetails.isPdfAttachment()"+emailDetails.isPdfAttachment());
//
//			
//			boolean companyAsLetterHead = false;
//			if(emailDetails.isPdfAttachment()){
//				if(emailDetails.getEntityName().trim().equals("Lead")){
//					Lead sales = (Lead) emailDetails.getModel();
//				}
//				else if(emailDetails.getEntityName().trim().equals("Quotation")){
//					logger.log(Level.SEVERE, "Inside quotation == ");
//					Quotation quotation = (Quotation) emailDetails.getModel();
//					createQuotationAttchmentData(quotation);
//					attachFiles =  quotation.getCount() + " "+ quotation.getCinfo().getFullName();
//					if (quotation.getDocument() != null
//							&& quotation.getDocument().getUrl() != null) {
//						uploadDocName = quotation.getDocument().getName();
//					}
//				}
//				else if(emailDetails.getEntityName().trim().equals("Contract")){
//					Sales sales = (Sales) emailDetails.getModel();
//					attachFiles =  sales.getCount() + " "+ sales.getCinfo().getFullName();
//					setServiceContract(sales);
//					initializeServiceContract();
//					
//					Contract contract = (Contract) emailDetails.getModel();
//					if (contract.getDocument() != null
//							&& contract.getDocument().getUrl() != null) {
//						uploadDocName = contract.getDocument().getName();
//					}
//				}
//				else if(emailDetails.getEntityName().trim().equals("SalesQuotation")){
//					SalesQuotation salesquotation = (SalesQuotation) emailDetails.getModel();
//					attachFiles =  salesquotation.getCount() + " "+ salesquotation.getCinfo().getFullName();
//
//					setSalesQuotation(salesquotation);
//					initializeSalesQuotation();
//					createSalesQuotationAttachment();
//					createSalesQuotationUploadDocumentAttachment();
//					
//					if (salesquotation.getDocument() != null
//							&& salesquotation.getDocument().getUrl() != null) {
//						uploadDocName = salesquotation.getDocument().getName();
//					}
//				}
//				else if(emailDetails.getEntityName().trim().equals("SalesOrder")){
//					SalesOrder salesOrder = (SalesOrder) emailDetails.getModel();
//					attachFiles =  salesOrder.getCount() + " "+ salesOrder.getCinfo().getFullName();
//					setSalesOrder(salesOrder);
//					initializeSalesOrder();
//					createSalesOrderAttachment();
//					createSalesOrderUploadDocumentAttachment();
//					
//					if (salesOrder.getDocument() != null
//							&& salesOrder.getDocument().getUrl() != null) {
//						uploadDocName = salesOrder.getDocument().getName();
//					}
//					
//				}
//				else if(emailDetails.getEntityName().trim().equals("Invoice") || emailDetails.getEntityName().trim().equals("VendorInvoice") ){
//					Invoice invoice = (Invoice) emailDetails.getModel();
//					attachFiles =  invoice.getCount() + " "+ invoice.getPersonInfo().getFullName();
//					createInvoiceAttachmentDetails(invoice);
//				}
//				else if(emailDetails.getEntityName().trim().equals("CustomerPayment")){
//					CustomerPayment payment = (CustomerPayment) emailDetails.getModel();
//					attachFiles =  payment.getCount() + " "+ payment.getPersonInfo().getFullName();
//					createPaymentAttachmentDetails(payment);
//				}
//				else if(emailDetails.getEntityName().trim().equals("Complain")){
//					Complain complain = (Complain) emailDetails.getModel();
//					attachFiles =  complain.getCount() + " "+ complain.getPersoninfo().getFullName();
//					createComplainPdfAttachment(complain);
//				}
//				else if(emailDetails.getEntityName().trim().equals("DeliveryNote")){
//					DeliveryNote deliveryNote = (DeliveryNote) emailDetails.getModel();
//					attachFiles =  deliveryNote.getCount() + " "+ deliveryNote.getCinfo().getFullName();
//					
//					setDeliveryNote(deliveryNote);
//					initializeDeliveryDetails();
//					createDeliveryAttachment();
//					createDeliveryNoteUploadAttachment();
//					
//					if (deliveryNote.getDeliveryDocument() != null
//							&& !deliveryNote.getDeliveryDocument().getUrl()
//									.equals("")) {
//						uploadDocName = deliveryNote.getDeliveryDocument().getName();
//					}
//				}
//				else if(emailDetails.getEntityName().trim().equals("PurchaseRequisition")){
//					PurchaseRequisition purchaseReq = (PurchaseRequisition) emailDetails.getModel();
//					setPR(purchaseReq);
//					loadDataForPr();
//					createPRUploadDocumentAttachment();
//					
//					if (purchaseReq.getUptestReport() != null
//							&& purchaseReq.getUptestReport().getUrl() != null) {
//						uploadDocName = purchaseReq.getUptestReport().getName();
//					}
//					
//				}
//				else if(emailDetails.getEntityName().trim().equals("RequsestForQuotation")){
//					RequsestForQuotation rfq = (RequsestForQuotation) emailDetails.getModel();
//					setRFQ(rfq);
//					loadDataForRfq();
//					createRFQUploadDocumentAttachment();
//					if (rfq.getUptestReport() != null
//							&& !rfq.getUptestReport().getUrl().equals("")) {
//						uploadDocName = rfq.getUptestReport().getName();
//					}
//
//				}
//				else if(emailDetails.getEntityName().trim().equals("LetterOfIntent")){
//					LetterOfIntent loiEntity = (LetterOfIntent) emailDetails.getModel();
//					
//					setLOI(loiEntity);
//					loadDataForLoi();
//					createLoiUploadDocumentAttachment();
//					
//					if (loiEntity.getUptestReport() != null
//							&& loiEntity.getUptestReport().getUrl() != null) {
//						uploadDocName = loiEntity.getUptestReport().getName();
//					}
//					
//
//				}
//				else if(emailDetails.getEntityName().trim().equals("PurchaseOrder")){
//				//	Boolean CompanyAsLetterHead = false;
//					PurchaseOrder po = (PurchaseOrder) emailDetails.getModel();
//					attachFiles = "PurchaseOrder"+po.getCount()+"";
//					setPO(po);
//					initializePurchaseOrder();
//					
//					logger.log(Level.SEVERE, "attachmnet id in PO " +po.getCompanyId());
//					createPOAttachment(po.getCompanyId());
//					createPOUploadDocumentAttachment();
//					
//					if (po.getUptestReport() != null
//							&& !po.getUptestReport().getUrl().equals("")) {
//						uploadDocName = po.getUptestReport().getName();
//					}
//				}
//				else if(emailDetails.getEntityName().trim().equals("VendorInvoice")){
//					VendorInvoice vendorInvoice = (VendorInvoice) emailDetails.getModel();
//					attachFiles =  vendorInvoice.getCount() + " "+ vendorInvoice.getPersonInfo().getFullName();
//					createVednorInvoiceAttachmentDetails(vendorInvoice);
//				}
//				else if(emailDetails.getEntityName().trim().equals("PaySlip")){
//					PaySlip payslipEntity = (PaySlip) emailDetails.getModel();
//					attachFiles =  payslipEntity.getCount() + " "+ payslipEntity.getEmployeeName();
//
//					setSalarySlip(payslipEntity);
//				    createSalarySlipAttachment();
//				    
//				}
//				else if(emailDetails.getEntityName().trim().equals(AppConstants.CONTRACT_RENEWAL)){
//					ContractRenewal contractRenewal = (ContractRenewal) emailDetails.getModel();
//					logger.log(Level.SEVERE, "Contract Renewal email attchment");
//					attachFiles = "RenewalLetter";
//					createContractRenewalEmailAttachment(contractRenewal);
//				}
//					
//				
//			}
//
//		
//			if(paymentGatewayRequestLinkFlag){ 
//				
//				String str = commonserviceimpl.validatePaymentDocument(emailDetails.getEntityName(),emailDetails.getModel().getCount(),companyId);
//				if(str!=null && !str.equals("")){
//					return str;
//				}
//				
//				boolean paymentGatewayUniqueIdFlag = false;
//				
//				if(emailDetails.getEntityName().trim().equals("Lead")){
//					Lead lead = (Lead) emailDetails.getModel();
//
//					Lead leadEntity = ofy().load().type(Lead.class).filter("companyId",companyId).filter("count", emailDetails.getModel().getCount())
//										.first().now();
//					if(leadEntity!=null && (leadEntity.getNumberRange()==null || leadEntity.getNumberRange().equals(""))){
//						String numberRangeValidation = commonserviceimpl.validateNumberRange(companyId,AppConstants.LEAD);
//						if(!numberRangeValidation.equals("")){
//							return numberRangeValidation;
//						}
//					}
//					
//					if(leadEntity!=null && leadEntity.getPaymentGatewayUniqueId()!=null && !leadEntity.getPaymentGatewayUniqueId().equals("")){
//						paymentGatewayUniqueIdFlag = true;
//						paymentGatewayUniqueId = leadEntity.getPaymentGatewayUniqueId();
//					}
//					else{
//						Customer customer = ofy().load().type(Customer.class).filter("companyId", lead.getCompanyId())
//											.filter("count", lead.getPersonInfo().getCount()).first().now();
//						jsonData = commonserviceimpl.createJsonData(emailDetails.getModel(),AppConstants.LEAD, emailDetails.getAppURL(), emailDetails.getAppId(), company, customer,null,emailDetails.getLandingPageText());
//						
//					}
//							
//
//				}
//				else if(emailDetails.getEntityName().trim().equals("Quotation")){
//					logger.log(Level.SEVERE, "Inside quotation == ");
//					Quotation quotation = (Quotation) emailDetails.getModel();
//					Quotation quotationEntity = ofy().load().type(Quotation.class).filter("companyId",companyId).filter("count", emailDetails.getModel().getCount())
//							.first().now();
//					
//					if(quotationEntity!=null && (quotationEntity.getQuotationNumberRange()==null || quotationEntity.getQuotationNumberRange().equals(""))){
//						String numberRangeValidation = commonserviceimpl.validateNumberRange(companyId,AppConstants.QUOTATION);
//						if(!numberRangeValidation.equals("")){
//							return numberRangeValidation;
//						}
//					}
//					
//					if(quotationEntity!=null && quotationEntity.getPaymentGatewayUniqueId()!=null && !quotationEntity.getPaymentGatewayUniqueId().equals("")){
//						paymentGatewayUniqueIdFlag = true;
//						paymentGatewayUniqueId = quotationEntity.getPaymentGatewayUniqueId();
//					}
//					else{
////						String pdf = "http://my.evadev0005.appspot.com/slick_erp/pdfservice?Id=5615125533818880&type=q&preprint=yes";
//						String pdflink = commonserviceimpl.getPDFURL(AppConstants.QUOTATION, emailDetails.getModel(), emailDetails.getModel().getCount(), companyId, company, emailDetails.getPdfURL());
//						System.out.println("PDF Link "+pdflink);
//						Customer customer = ofy().load().type(Customer.class).filter("companyId", quotation.getCompanyId())
//											.filter("count", quotation.getCinfo().getCount()).first().now();
//						jsonData = commonserviceimpl.createJsonData(emailDetails.getModel(),AppConstants.QUOTATION, emailDetails.getAppURL(), emailDetails.getAppId(), company, customer,pdflink,emailDetails.getLandingPageText());
//
//					}
//									}
//				else if(emailDetails.getEntityName().trim().equals(AppConstants.CONTRACT)){
//					
//					Contract contract = (Contract) emailDetails.getModel();
//					Contract contractEntity = ofy().load().type(Contract.class).filter("companyId",companyId).filter("count", emailDetails.getModel().getCount())
//							.first().now();
//					if(contractEntity!=null && (contractEntity.getNumberRange()==null || contractEntity.getNumberRange().equals(""))){
//						String numberRangeValidation = commonserviceimpl.validateNumberRange(companyId,null);
//						if(!numberRangeValidation.equals("")){
//							return numberRangeValidation;
//						}
//					}
//					
//					if(contractEntity!=null && contractEntity.getPaymentGatewayUniqueId()!=null && !contractEntity.getPaymentGatewayUniqueId().equals("")){
//						paymentGatewayUniqueIdFlag = true;
//						paymentGatewayUniqueId = contractEntity.getPaymentGatewayUniqueId();
//					}
//					else{
//						Customer customer = ofy().load().type(Customer.class).filter("companyId", contract.getCompanyId())
//								.filter("count", contract.getCinfo().getCount()).first().now();
//						String pdflink = commonserviceimpl.getPDFURL(AppConstants.CONTRACT, emailDetails.getModel(), emailDetails.getModel().getCount(), companyId, company, emailDetails.getPdfURL());
//						System.out.println("PDF Link "+pdflink);
//						jsonData = commonserviceimpl.createJsonData(emailDetails.getModel(),AppConstants.CONTRACT, emailDetails.getAppURL(), emailDetails.getAppId(), company, customer,pdflink,emailDetails.getLandingPageText());
//			
//					}
//					
//				}
//				else if(emailDetails.getEntityName().trim().equals(AppConstants.CONTRACT_RENEWAL)){
//					ContractRenewal contractRenewal = (ContractRenewal) emailDetails.getModel();
//					
//					ContractRenewal contractRenewalEntity = ofy().load().type(ContractRenewal.class).filter("companyId",companyId).filter("contractId", emailDetails.getModel().getCount())
//							.first().now();
//					if(contractRenewalEntity!=null && contractRenewalEntity.getPaymentGatewayUniqueId()!=null && !contractRenewalEntity.getPaymentGatewayUniqueId().equals("")){
//						paymentGatewayUniqueIdFlag = true;
//						paymentGatewayUniqueId = contractRenewalEntity.getPaymentGatewayUniqueId();
//					}
//					else{
//						Customer customer = ofy().load().type(Customer.class).filter("companyId", companyId)
//								.filter("count", contractRenewal.getCustomerId()).first().now();
//						System.out.println("customer "+customer);
//						String pdflink = commonserviceimpl.getPDFURL(AppConstants.CONTRACT_RENEWAL, emailDetails.getModel(), emailDetails.getModel().getCount(), companyId, company, emailDetails.getPdfURL());
//						jsonData = commonserviceimpl.createJsonData(emailDetails.getModel(),AppConstants.CONTRACT_RENEWAL, emailDetails.getAppURL(), emailDetails.getAppId(), company, customer,pdflink,emailDetails.getLandingPageText());
//					}
//				
//				}
//				else if(emailDetails.getEntityName().trim().equals("Invoice") ){
//					Invoice invoice = (Invoice) emailDetails.getModel();
//					
//					Invoice invoiceEntity = ofy().load().type(Invoice.class).filter("companyId",companyId).filter("count", emailDetails.getModel().getCount())
//							.first().now();
//					if(invoiceEntity!=null && invoiceEntity.getPaymentGatewayUniqueId()!=null && !invoiceEntity.getPaymentGatewayUniqueId().equals("")){
//						paymentGatewayUniqueIdFlag = true;
//						paymentGatewayUniqueId = invoiceEntity.getPaymentGatewayUniqueId();
//					}
//					else{
//						Customer customer = ofy().load().type(Customer.class).filter("companyId", invoice.getCompanyId())
//											.filter("count", invoice.getPersonInfo().getCount()).first().now();
//						String pdflink = commonserviceimpl.getPDFURL("Invoice", emailDetails.getModel(), emailDetails.getModel().getCount(), companyId, company, emailDetails.getPdfURL());
//						logger.log(Level.SEVERE, "invoice pdflink "+pdflink);
//						jsonData = commonserviceimpl.createJsonData(emailDetails.getModel(),"Invoice", emailDetails.getAppURL(), emailDetails.getAppId(), company, customer,pdflink,emailDetails.getLandingPageText());
//						
//					}
//					
//				}
//
//			logger.log(Level.SEVERE, "msgBody"+strmsgBody);
//
//		
//			if(paymentGatewayUniqueIdFlag==false){
//				
//			logger.log(Level.SEVERE,"Inside digital payment request condition");
//
//
//			paymentGatewayUniqueId = commonserviceimpl.callPaymnetGatewayUniqueIdAPI(jsonData, emailDetails.getModel(), emailDetails.getEntityName());
//			
//			}
//			
//			String landingPagelink = AppConstants.PAYMENTGATEWAYSMSLINK+paymentGatewayUniqueId;
//			logger.log(Level.SEVERE, "landingPagelink"+landingPagelink);
//
//			StringBuilder sb = new StringBuilder(); // link will be landing page 
//			
//			if(emailDetails.getEntityName().trim().equals("Invoice")){
//				sb.append("<br><br> <p align=\"center\"> <a href= "+landingPagelink +">"+"<button>Pay Now</button></a> </p> <br><br> ");
//			}
//			else if(emailDetails.getEntityName().trim().equals("ContractRenewal")){
//				sb.append("<br><br> <p align=\"center\"> <a href= "+landingPagelink +">"+"<button>Renew Order</button></a> </p> <br><br> ");
//			}
//			else{
//				sb.append("<br><br> <p align=\"center\"> <a href= "+landingPagelink +">"+"<button class=\"btn\">View Order</button></a> </p> <br><br> ");
//			}
//			strmsgBody = strmsgBody.replace(AppConstants.EMAILPAYMENTGATEWAYREQUEST, sb.toString());
//			logger.log(Level.SEVERE, "msgBody after replacing add digital payment request to Place Order "+strmsgBody);
//			
//		}
//			
//		StringBuilder builder = new StringBuilder();
//		
//		builder.append("<!DOCTYPE html>");
//		builder.append("<html lang=\"en\">");
//		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
//		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");
//		
//		builder.append("<br>");
////		builder.append("<br>");
//
//		builder.append(strmsgBody);
//		builder.append("<br>");
//
//		builder.append("<br>");
//		/**
//		 * @author Vijay Date :- 08-06-2021
//		 * Des :- As per Nitin Sir Signature part user will add into email body part so below signature part commented
//		 */
////		builder.append("<b>" + "Thanks & Regards" + "</b><br>");
////		// Company Details
////		builder.append("<br>");
////		builder.append("<b>" + company.getBusinessUnitName() + "</b><br>");
////		builder.append("<table>");
////
////		builder.append("<tr>");
////		builder.append("<td>");
////		builder.append("Address : ");
////		builder.append("</td>");
////		builder.append("<td>");
////		if (!company.getAddress().getAddrLine2().equals("")
////				&& company.getAddress().getAddrLine2() != null)
////			builder.append(company.getAddress().getAddrLine1() + " "
////					+ company.getAddress().getAddrLine2() + " "
////					+ company.getAddress().getLocality());
////		else
////			builder.append(company.getAddress().getAddrLine1() + " "
////					+ company.getAddress().getLocality());
////		builder.append("</td>");
////		builder.append("</tr>");
////
////		builder.append("<tr>");
////		builder.append("<td>");
////		builder.append("</td>");
////		builder.append("<td>");
////		if (!company.getAddress().getLandmark().equals("")
////				&& company.getAddress().getLandmark() != null)
////			builder.append(company.getAddress().getLandmark() + ","
////					+ company.getAddress().getCity() + ","
////					+ company.getAddress().getPin()
////					+ company.getAddress().getState() + ","
////					+ company.getAddress().getCountry());
////		else
////			builder.append(company.getAddress().getCity() + ","
////					+ company.getAddress().getPin() + ","
////					+ company.getAddress().getState() + ","
////					+ company.getAddress().getCountry());
////		builder.append("</td>");
////		builder.append("</tr>");
////		builder.append("<tr>");
////		builder.append("<td>");
////		builder.append("Website : ");
////		builder.append("</td>");
////		builder.append("<td>");
////		builder.append(company.getWebsite());
////		builder.append("</td>");
////		builder.append("</tr>");
////
////		builder.append("<tr>");
////		builder.append("<td>");
////		builder.append("Phone No : ");
////		builder.append("</td>");
////		builder.append("<td>");
////		builder.append(company.getCellNumber1());
////		builder.append("</td>");
////		builder.append("<tr>");
////
////		builder.append("</tr>");
////		builder.append("</table>");
//		/**
//		 * ends here
//		 */
//		
//		builder.append("</body>");
//		builder.append("</html>");
//				
//		String contentInHtml = builder.toString();
//		System.out.println(contentInHtml);
//		
//		logger.log(Level.SEVERE, "contentInHtml"+contentInHtml);
//		
//		logger.log(Level.SEVERE, "salesPersonEmailIdFlag"+salesPersonEmailIdFlag);
//
//		/**
//		 * email popup uploaded document attachment
//		 */
//		String emailPoupUpload=null;
//		if(emailDetails.getUploadedDocument()!=null){
//			uploadedDocument = createAttachmentInStream(emailDetails.getUploadedDocument());
//			emailPoupUpload = emailDetails.getUploadedDocument().getName();
//		}
//		
//		
//		/***
//		 * Below code for managing PTSPL Requirement Sales person wise Email fuctionality
//		 */
//		if(salesPersonEmailIdFlag && (emailDetails.getEntityName().trim().equals("Lead") || emailDetails.getEntityName().trim().equals("Quotation") || emailDetails.getEntityName().trim().equals("Contract")
//				 || emailDetails.getEntityName().trim().equals("SalesQuotation") ||  emailDetails.getEntityName().trim().equals("SalesOrder") || emailDetails.getEntityName().trim().equals("Complain")
//				|| emailDetails.getEntityName().trim().equals("PurchaseOrder")) ){
//			logger.log(Level.SEVERE, "Sales person wise EMail for PTSPL "+salesPersonEmailIdFlag);
//
//			if(emailDetails.getEntityName().trim().equals("Lead")){
//				logger.log(Level.SEVERE, "Sales person wise EMail for PTSPL for Lead ");
//				try {
//					if(emailDetails.getEntityName().trim().equals("Lead")){
//						SendLeadEmail(emailDetails,contentInHtml,company);
//					}
//					return "Email sent Successfully";
//					
//				} catch (Exception e) {
//					return "Email not sent please contact EVA Support!";
//				}
//			}
//			else{
//				logger.log(Level.SEVERE, "Sales person wise EMail for PTSPL Other than Lead ");
//				try {
//					SendGridEmailServlet sdEmail=new SendGridEmailServlet();
////					sdEmail.sendMailWithSendGrid(emailDetails.getFromEmailid(), emailDetails.getToEmailId(), emailDetails.getCcEmailId(), emailDetails.getBccEmailId(), emailDetails.getSubject(), contentInHtml, "text/html",pdfstream,attachFiles,"application/pdf",null);
//					sdEmail.sendMailWithSendGrid(emailDetails.getFromEmailid(), emailDetails.getToEmailId(), emailDetails.getCcEmailId(), emailDetails.getBccEmailId(), emailDetails.getSubject(), contentInHtml, "text/html",pdfstream,attachFiles,"application/pdf", uploadedDocument, emailPoupUpload, "application/pdf", uploadStream, uploadDocName, "application/pdf", null, null, null, null);
//					return "Email sent Successfully";
//				} catch (Exception e) {
//					return "Email not sent please contact EVA Support!";
//				}
//			}
//			
//		}
//		
//		/**
//		 * ends here
//		 */
//		logger.log(Level.SEVERE, "After Salesperson wise email Email ");
//
//		
//		
//		
//		logger.log(Level.SEVERE, "Before Sendgrid API wise Email ");
//
//		 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, companyId)){
//				logger.log(Level.SEVERE, "Inside Sendgird API wise Email ");
//
//			 try {
//					SendGridEmailServlet sdEmail=new SendGridEmailServlet();
//					sdEmail.sendMailWithSendGrid(emailDetails.getFromEmailid(), emailDetails.getToEmailId(), emailDetails.getCcEmailId(), emailDetails.getBccEmailId(), emailDetails.getSubject(), contentInHtml, "text/html",pdfstream,attachFiles,"application/pdf", uploadedDocument, emailPoupUpload, "application/pdf", uploadStream, uploadDocName, "application/pdf", null, null, null, null);
//					return "Email sent Successfully";
//					
//				} catch (Exception e) {
//					return "Email not sent please contact EVA Support!";
//				}
//			 
//		 }
//
//		
//			logger.log(Level.SEVERE, "Before Calling Gmail Email API");
//
//		try {
//			System.out.println("Inside Try Block");
//			Properties props = new Properties();
//			Session session = Session.getDefaultInstance(props, null);
//			Message message = new MimeMessage(session);
//			Multipart multiPart = new MimeMultipart();
//
//			System.out.println("From Email: " +emailDetails.getFromEmailid());
//
//			// To Mail Address
//			message.setFrom(new InternetAddress(emailDetails.getFromEmailid()));
//			message.setReplyTo(new InternetAddress[] { new InternetAddress(
//					emailDetails.getFromEmailid()) });
//
//			// Email to Multiple Recipients
//			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
//			for (int i = 0; i < toEmailIdArray.length; i++) {
//				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
//				System.out.println("Email id multipart " + mailToMultiple[i]);
//			}
//			message.setRecipients(Message.RecipientType.TO, mailToMultiple);
//		
//			// Email CC Multiple Recipients
//			if(emailDetails.getCcEmailId().size()>0){
//				try {
//					InternetAddress[] mailCCMultiple = new InternetAddress[ccEmailIdArray.length];
//					for (int i = 0; i < ccEmailIdArray.length; i++) {
//						mailCCMultiple[i] = new InternetAddress(ccEmailIdArray[i]);
//						System.out.println("Email id multipart " + mailCCMultiple[i]);
//					}
//					message.setRecipients(Message.RecipientType.CC, mailCCMultiple);
//					
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				
//			}
//			
//			// Email BCC Multiple Recipients
//			if(emailDetails.getBccEmailId().size()>0){
//				try {
//					InternetAddress[] mailBCCMultiple = new InternetAddress[bccEmailIdArray.length];
//					for (int i = 0; i < bccEmailIdArray.length; i++) {
//						mailBCCMultiple[i] = new InternetAddress(bccEmailIdArray[i]);
//						System.out.println("Email id multipart " + mailBCCMultiple[i]);
//					}
//					message.setRecipients(Message.RecipientType.BCC, mailBCCMultiple);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				
//
//			}
//		
//			// Mail Subject
//			message.setSubject(emailDetails.getSubject());
//
//			// Mail Body Content In html Form
//			MimeBodyPart htmlpart = new MimeBodyPart();
//			htmlpart.setContent(contentInHtml, "text/html");
//			multiPart.addBodyPart(htmlpart);
//
//			
//			if (emailPoupUpload != null) {
//				System.out.println("Inside Attachment.........");
//
//				MimeBodyPart attachbody = new MimeBodyPart();
//				attachbody.setFileName(emailPoupUpload);
//				DataSource src = new ByteArrayDataSource(
//						uploadedDocument.toByteArray(), "application/pdf");
//				attachbody.setDataHandler(new DataHandler(src));
//				multiPart.addBodyPart(attachbody);
//				
//			}
//			
//			// Mail Uploaded Document attachment
//			if (uploadDocName != null) {
//				MimeBodyPart attachbodyDoc = new MimeBodyPart();
//				logger.log(Level.SEVERE, "Inside Attachment upload  .........");
//				attachbodyDoc.setFileName(uploadDocName);
//				DataSource src = new ByteArrayDataSource(
//						uploadStream.toByteArray(), "image/jpeg");
//				attachbodyDoc.setDataHandler(new DataHandler(src));
//				multiPart.addBodyPart(attachbodyDoc);
//			}
//			
//			if(emailDetails.isPdfAttachment()){
//				try {
//					// Mail attachment
////					String attachFiles = "Document";
//					if (attachFiles != null && pdfstream!=null) {
//						MimeBodyPart attachbody = new MimeBodyPart();
//						logger.log(Level.SEVERE, "Inside Attachment.........");
//						attachbody.setFileName(attachFiles + ".pdf");
//						DataSource src = new ByteArrayDataSource(
//								pdfstream.toByteArray(), "application/pdf");
//						attachbody.setDataHandler(new DataHandler(src));
//						multiPart.addBodyPart(attachbody);
//					}	
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				
//			}
//			
//			try {
//				message.setContent(multiPart);
//				logger.log(Level.SEVERE, "Ready To Send Mail");
//				Transport.send(message);
//				logger.log(Level.SEVERE, " Email sent Successfully  ");
//				System.out.println("Email sent Successfully !!!!");
//			} catch (IllegalArgumentException ex) {
//				return "Sender(From) Email id is does not register with App Id Please contact to EVA Support";
//			}
//
//		} catch (Exception e) {
//			logger.log(Level.SEVERE, " Email Fail ");
//			e.printStackTrace();
////			return "Sender Email id is not configured with server please cotact EVA Support";
//			return "Email not sent please contact to EVA Support!";
//		}
//		return "Email sent Successfully";
	
	}

	

	private void createPaymentAttachmentDetails(CustomerPayment customerPayment) {
		CustomerPayment payment = (CustomerPayment) customerPayment;
		setPayment(payment);
		initializePaymentDetails();

		/*
		 * Rohan added this code for Sending Customer Customization PDF prints
		 * in Email Date : 15-09-2016
		 */
		ProcessConfiguration process = null;
		/** 1-11-2017 sagar sore **/
		boolean flag = false;
		process = ofy().load().type(ProcessConfiguration.class)
				.filter("companyId", payment.getCompanyId())
				.filter("processName", "CustomerPayment")
				.filter("processList.status", true).first().now();

		if (process != null) {
			for (int i = 0; i < process.getProcessList().size(); i++) {

				if (process.getProcessList().get(i).getProcessType().trim()
						.equalsIgnoreCase("CompanyAsLetterHead")
						&& process.getProcessList().get(i).isStatus() == true) {

					createPaymentDetailsAttachment();
					flag = true;
				}
				if (process.getProcessList().get(i).getProcessType().trim()
						.equalsIgnoreCase("HygeiaPestManagement")
						&& process.getProcessList().get(i).isStatus() == true) {

					createPaymentDetailsAttachmentforHygeiaPestManagement();
					flag = true;
				}
			}
			/**
			 * 1-10-2017 sagar sore [Email in case status of all processes
			 * false]
			 **/
			if (flag == false) {
				logger.log(Level.SEVERE, "process config not matched");
				createPaymentDetailsAttachment();
			}
		} else {
			createPaymentDetailsAttachment();
		}
	}

	private void createInvoiceAttachmentDetails(Invoice invoiceEntity) {
		Invoice invoice = (Invoice) invoiceEntity;
		setInvoice(invoice);
		initializeInvoiceDetails();
		if (AppConstants.ORDERTYPESALES.equals(invDetails.getTypeOfOrder().trim())) {
			createSalesInvoiceAttachmentGST();
		}
		if (AppConstants.ORDERTYPEFORSERVICE.equals(invDetails.getTypeOfOrder().trim())) {

			/*
			 * Rohan added this code for Sending Customer Customization PDF
			 * prints in Email Date : 15-09-2016
			 */
			/**
			 * 2-11-2017 sagar sore [if condition for processconfiguration
			 * checking of gstquotation pdf added and boolean flag used to send
			 * attachment in case of status of all processes is false]
			 **/
			boolean flag = false;
			/**
			 * Date : 04-12-2017 By JAYSHREE
			 */
			boolean companyAsLetterHead=false;
			
			ProcessConfiguration process = null;
			process = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", invDetails.getCompanyId())
					.filter("processName", "Invoice")
					.filter("processList.status", true).first().now();

			if (process != null) {
				for (int i = 0; i < process.getProcessList().size(); i++) {

					if (process.getProcessList().get(i).getProcessType()
							.trim().equalsIgnoreCase("PestoIndiaQuotations")
							&& process.getProcessList().get(i).isStatus() == true) {
						flag = true;
						pestoIndiaInvoiceAttchment();
						break;

					} else if (process.getProcessList().get(i).getProcessType()
							.trim().equalsIgnoreCase("VCareCustomization")
							&& process.getProcessList().get(i).isStatus() == true) {
						flag = true;
						vCareInvoiceAttchment();
						break;

					}

					else if (process.getProcessList().get(i).getProcessType()
							.trim()
							.equalsIgnoreCase("InvoiceAndRefDocCustmization")
							&& process.getProcessList().get(i).isStatus() == true) {
						flag = true;
						omPestInvoiceInvoiceAttachment();
						break;

					}

					else if (process.getProcessList().get(i).getProcessType()
							.trim().equalsIgnoreCase("HygeiaPestManagement")
							&& process.getProcessList().get(i).isStatus() == true) {
						flag = true;
						hygeiaPestManagementInvoiceAttachment();
						break;

					} else if (process.getProcessList().get(i).getProcessType()
							.trim().equalsIgnoreCase("PCAMBCustomization")
							&& process.getProcessList().get(i).isStatus() == true) {
						logger.log(Level.SEVERE, "PCAMBCustomization");
						pcambInvoiceAttachment();
						flag = true;
						break;

					}

					else if (process.getProcessList().get(i).getProcessType()
							.trim()
							.equalsIgnoreCase("ReddysPestControlQuotations")
							&& process.getProcessList().get(i).isStatus() == true) {
						logger.log(Level.SEVERE, "ReddysPestControlQuotations");
						reddysPestControlInvoiceAttachment();
						flag = true;
						break;
					}

					else if (process.getProcessList().get(i).getProcessType()
							.trim().equalsIgnoreCase("OnlyForPecopp")
							&& process.getProcessList().get(i).isStatus() == true) {
						logger.log(Level.SEVERE, "OnlyForPecopp");
						logger.log(Level.SEVERE, "OnlyForPecopp"+invDetails.getCompanyId());
						
						pecoppInvoiceAttachment(invDetails.getCompanyId());
						flag = true;
						break;

					}else if (process.getProcessList().get(i).getProcessType()
							.trim().equalsIgnoreCase("OnlyForOrionPestSolutions")
							&& process.getProcessList().get(i).isStatus() == true) {
						/**
						 * @author Anil ,Date : 29-03-2019
						 * 
						 */
						
						logger.log(Level.SEVERE, "OnlyForOrionPestSolutions");
						orionInvoiceAttachment();
						flag = true;
						break;

					}
					
					/**Date 4/12/2017
					 * By Jayshree
					 * To check the process configration for letterhead
					 */
					else if(process.getProcessList().get(i).getProcessType()
							.trim().equalsIgnoreCase("CompanyAsLetterHead")
							&& process.getProcessList().get(i).isStatus() == true){
						companyAsLetterHead=true;
						logger.log(Level.SEVERE," pecopp invoice pdf"  +companyAsLetterHead);
						//pecoppInvoiceAttachment(companyAsLetterHead);
						
					}//End By jayshree
				}
				if (!flag) {
					logger.log(Level.SEVERE, "eva pest");
//					createServiceInvoiceAttachment();
					/**Date 4/12/2017
					 * By Jayshree
					 * To call the method createServiceInvoiceAttachmentGST
					 */
					createServiceInvoiceAttachmentGST(companyAsLetterHead);
					//End By Jayshree
				}
			} else {
				logger.log(Level.SEVERE, "default invoice ");
//				createServiceInvoiceAttachment();
				/**Date 4/12/2017
				 * By Jayshree
				 * To call the method createServiceInvoiceAttachmentGST
				 */
				
				createServiceInvoiceAttachmentGST(companyAsLetterHead);
				//End By Jayshree
			}
		}
		if (AppConstants.ORDERTYPEPURCHASE.equals(invDetails.getTypeOfOrder()
				.trim())) {
			logger.log(Level.SEVERE, "purchase");
			createPurchaseInvoiceAttachment(invDetails.getCompanyId());
		}
	}

	private void createQuotationAttchmentData(Quotation servquot1) {


		setServiceQuotation(servquot1);
		initializeServiceQuotation(servquot1);
		System.out.println("servquot.isCustomequotation() flag value ======"
				+ servquot.isCustomequotation());
		
		logger.log(Level.SEVERE, "Quotation Email");

		if (servquot.isCustomequotation() == true) {
			/** 9-11-2017 sagar sore **/
			createCustomeQuotationAttachment();
			createServiceQuotationUploadDocumentAttachment();
		} else {
			logger.log(Level.SEVERE, "Quotation Email checking peocess config");

			/**
			 * rohan addded this code for sending customized attachement in
			 * Email
			 */
			boolean processConfigurationCheckFlag = false;
			/**
			 * Date : 04-12-2017 By JAYSHREE
			 */
			boolean companyAsLetterHead=false;//By Jayshree
			ProcessConfiguration process = null;
			process = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", servq.getCompanyId())
					.filter("processName", "Quotation")
					.filter("configStatus", true).first().now();
			/**
			 * Date 1/11/2018
			 * Updated By Viraj
			 * Description Email Attachment of Terms And Conditions, Company Profile
			 */
			/** date 13.05.2019 added by komal as it done only for ordient venture *it is
			 * called for all clients and was given error**/
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Company","OnlyForOrdientVentures" , servq.getCompanyId())) {
				createTCUploadDocumentAttachment();
				createCPUploadDocumentAttachment();
			}
			if (process != null) {
				for (int i = 0; i < process.getProcessList().size(); i++) {

					if (process.getProcessList().get(i).getProcessType().trim()
							.equalsIgnoreCase("PestleQuotations")
							&& process.getProcessList().get(i).isStatus() == true) {
						logger.log(Level.SEVERE, "PestlePest quotation");
						createPestlePestQuotationPdf();
						createServiceQuotationUploadDocumentAttachment();
						processConfigurationCheckFlag = true;
						break;

					} else if (process.getProcessList().get(i).getProcessType()
							.trim().equalsIgnoreCase("PestoIndiaQuotations")
							&& process.getProcessList().get(i).isStatus() == true) {
						logger.log(Level.SEVERE, "PestToIndia");
						createPestoIndiaQuotationPdf();
						createServiceQuotationUploadDocumentAttachment();
						processConfigurationCheckFlag = true;
						break;
					} else if (process.getProcessList().get(i).getProcessType()
							.trim().equalsIgnoreCase("NBHCQuotation")
							&& process.getProcessList().get(i).isStatus() == true) {
						logger.log(Level.SEVERE, "NBHC quotation");
						createNBHCQuotationPdf();
						createServiceQuotationUploadDocumentAttachment();
						processConfigurationCheckFlag = true;
						break;
					} else if (process.getProcessList().get(i).getProcessType()
							.trim()
							.equalsIgnoreCase("UniversalPestCustomization")
							&& process.getProcessList().get(i).isStatus() == true) {
						logger.log(Level.SEVERE, "Universal Pest");
						createUniversalPestQuotationPdf();
						createServiceQuotationUploadDocumentAttachment();
						processConfigurationCheckFlag = true;
						break;
					}/**
					 * Date 4/12/2017
					 * By Jayshree
					 * Des.to check the process configration for letter head
					 */
					else if(process.getProcessList().get(i).getProcessType().trim().equalsIgnoreCase("COMPANYASLETTERHEAD")
							&& process.getProcessList().get(i).isStatus() == true){
						companyAsLetterHead=true;
					}
					else if(process.getProcessList().get(i).getProcessType().trim().equalsIgnoreCase("QuotationForPecop")
							&& process.getProcessList().get(i).isStatus() == true){
						logger.log(Level.SEVERE, "Quotation Email pecopp pdf");
						pecoppQuotationPDF();
						createServiceQuotationUploadDocumentAttachment();
						processConfigurationCheckFlag = true;
						break;
					}else if(process.getProcessList().get(i).getProcessType().trim().equalsIgnoreCase("QUOTATIONASLETTER")
							&& process.getProcessList().get(i).isStatus() == true) {
						logger.log(Level.SEVERE, "QUOTATION Email pepcopp pdf with config QUOTATIONASLETTER");
						ServiceQuotationPdf servicePsf = new ServiceQuotationPdf();
						  servicePsf.document=new Document();
						  Document document = servicePsf.document;
						 
						  pdfstream = new ByteArrayOutputStream();
							try {
								PdfWriter.getInstance(servicePsf.document, pdfstream);
							} catch (DocumentException e) {
								e.printStackTrace();
							}
						   	document.open();
						   	servicePsf.setservicequotation(servquot.getId(),"no");
							servicePsf.createPdf("no");
							document.close();
						processConfigurationCheckFlag = true;
					}
					// else
					// {
					// // This is EVA quotation format
					// createServiceQuotationAttachment();
					// createServiceQuotationUploadDocumentAttachment();
					// processConfigurationCheckFlag = true;
					// }
				}
				/**
				 * 30-10-2017 sagar sore [when confuguration status of processes
				 * is false will be executed]
				 **/
				if (processConfigurationCheckFlag == false) {
					logger.log(Level.SEVERE, "EVA pest");
					// This is EVA quotation format
					createServiceQuotationAttachment(companyAsLetterHead);
					createServiceQuotationUploadDocumentAttachment();

				}
				/**
				 * ends here
				 */

			} else {
				// This is EVA quotation format
				createServiceQuotationAttachment(companyAsLetterHead);
				createServiceQuotationUploadDocumentAttachment();
			}

		}
	
	}

	private void SendLeadEmail(EmailDetails emailDetails, String contentInHtml, Company comp) {

		Lead lead = (Lead) emailDetails.getModel();
		Employee toemployee = null;
		ArrayList<String> toEmailList = new ArrayList<String>();
		toemployee = ofy().load().type(Employee.class)
				.filter("companyId", lead.getCompanyId())
				.filter("fullname", lead.getEmployee()).first().now();
		if (toemployee != null) {
			if (!toemployee.getEmail().trim().equals("")) {
				toEmailList.add(toemployee.getEmail().trim());
			}
		}
		toEmailList.addAll(emailDetails.getToEmailId());
		logger.log(Level.SEVERE, "Ready To Call Common Method"
				+ toEmailList.size());
		
			ArrayList<String> ccEmailList = new ArrayList<String>();
			ccEmailList.add(comp.getEmail());
			
			String salespersonEmail = getSalesPersonEmail(lead.getEmployee(),lead.getCompanyId(),lead.getBranch());
			if(salespersonEmail==null) {
				salespersonEmail = comp.getEmail();
			}
			String approverEmailId = getApproverEmailId(lead.getApproverName(),lead.getCompanyId());
			if(approverEmailId!=null) {
				ccEmailList.add(approverEmailId);
			}
			String branchEmailId = getBranchEmailId(lead.getBranch(), lead.getCompanyId());
			if(branchEmailId!=null) {
				ccEmailList.add(branchEmailId);
			}
			ArrayList<String> bcclist = new ArrayList<String>();
			bcclist.add(salespersonEmail);
			
			if(emailDetails.getCcEmailId().size()>0){
				ccEmailList.addAll(emailDetails.getCcEmailId());
			}
			
			if(emailDetails.getBccEmailId().size()>0){
				bcclist.addAll(emailDetails.getBccEmailId());
			}
			
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			sdEmail.sendMailWithSendGrid(salespersonEmail, toEmailList, ccEmailList, bcclist, "Request for Lead : " + lead.getCount(), contentInHtml, "text/html",pdfstream,null,null,null);

	}

	private void createPurchaseInvoiceAttachment(Long companyid) {
		purchaseGSTinvoice = new PurchaseGSTInvoice();
		purchaseGSTinvoice.document = new Document();
		pdfstream = new ByteArrayOutputStream();

		try {
			PdfWriter.getInstance(purchaseGSTinvoice.document, pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		purchaseGSTinvoice.document.open();
		purchaseGSTinvoice.setInvoice(invDetails.getId());
		
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("VendorInvoice", "COMPANYASLETTERHEAD", companyid)){
			purchaseGSTinvoice.createPdf("no");
		}
		else{
			purchaseGSTinvoice.createPdf("plane");
		}
		
		purchaseGSTinvoice.document.close();
		
	}

	private void createVednorInvoiceAttachmentDetails(VendorInvoice vendorInvoice) {
		
	}
	
	public void sendMsgEmail(String mailSub, String mailMsg,
			ArrayList<String> toEmailLis, ArrayList<String> ccEmailLis,
			String fromEmail){

		try {

			System.out.println("Inside Try Block");
			logger.log(Level.SEVERE, "INSIDE SEND MAIL : EMAIL SERVICE ");
			logger.log(Level.SEVERE, "FROM EMAIL " + fromEmail);
			logger.log(Level.SEVERE, "To EMAIL " + toEmailLis);

			for (String to : toEmailLis) {
				if (ccEmailLis!=null && ccEmailLis.contains(to)) {
					ccEmailLis.remove(to);
				}
			}

			String[] toEmailIdArray = new String[toEmailLis.size()];
			toEmailLis.toArray(toEmailIdArray);

			String[] ccEmailIdArray =null;
			if(ccEmailLis!=null){
				ccEmailIdArray = new String[ccEmailLis.size()];
				ccEmailLis.toArray(ccEmailIdArray);
			}
			
			/*** Date 03-04-2019 by Vijay for NBHC CCPM Email quota exceeds so updated code send email through sendgrid ***/ 
			List<Company> compEntity = ofy().load().type(Company.class).list();
			if(compEntity.size()>0){
				for(Company company : compEntity){
					if(company.getAccessUrl().equals("my")){
						comp = company;
					}
				}
			}
			 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, comp.getCompanyId())){
				logger.log(Level.SEVERE," company email --" +comp.getEmail() +"  to email  " + toEmailIdArray.toString());
				SendGridEmailServlet sdEmail=new SendGridEmailServlet();
				sdEmail.sendMailWithSendGrid(fromEmail, toEmailLis, ccEmailLis, null, mailSub, mailMsg, "text/html",null,null,null,comp.getDisplayNameForCompanyEmailId());
				
				return;
			}
			/**
			 * ends here
			 */

			Properties props = new Properties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", "smtp.gmail.com");
			Session session = Session.getDefaultInstance(props);
			// Session session = Session.getDefaultInstance(props, null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			// To Mail Address
			message.setFrom(new InternetAddress(fromEmail));

			// Email To to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				logger.log(Level.SEVERE, "To Email id multipart "
						+ mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);

			if(ccEmailIdArray!=null){
			// Email CC to Multiple Recipients
			InternetAddress[] ccmailToMultiple = new InternetAddress[ccEmailIdArray.length];
				for (int i = 0; i < ccEmailIdArray.length; i++) {
					ccmailToMultiple[i] = new InternetAddress(ccEmailIdArray[i]);
					logger.log(Level.SEVERE, "CC Email id multipart "
							+ ccmailToMultiple[i]);
				}
				if (ccmailToMultiple.length != 0) {
					message.setRecipients(Message.RecipientType.CC,
							ccmailToMultiple);
				}
			}

			// Mail Subject
			message.setSubject(mailSub);

			// Mail Body Content In html Form
			message.setText(mailMsg);

			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			logger.log(Level.SEVERE, "Mail Sent");
		} catch (Exception e) {
			e.printStackTrace();

			logger.log(Level.SEVERE, "Mail Not Sent " + e);
		}
	
	}
	
	public void initializeRenewal(ContractRenewal conRenewal){
		
		if (conRenewal.getCompanyId() != null) {
			comp = ofy().load().type(Company.class)
					.filter("companyId", conRenewal.getCompanyId()).first()
					.now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		if (conRenewal.getCompanyId() != null) {
			c = ofy().load().type(Customer.class)
					.filter("count", conRenewal.getCustomerId())
					.filter("companyId", conRenewal.getCompanyId()).first()
					.now();
		} else {
			c = ofy().load().type(Customer.class)
					.filter("count", conRenewal.getCustomerId()).first().now();
		}

		if (conRenewal.getCompanyId() != null) {
			servcont = ofy().load().type(Contract.class)
					.filter("count", conRenewal.getContractId())
					.filter("companyId", conRenewal.getCompanyId()).first()
					.now();
		} else {
			servcont = ofy().load().type(Contract.class)
					.filter("count", conRenewal.getContractId()).first().now();
		}
		
	}
	
	public void createContractRenewalEmailAttachment(ContractRenewal contractRenewal,long companyId) {
		
		
		
//		ContractRenewalPdf conrenewalpdf = new ContractRenewalPdf();
//		conrenewalpdf.document = new Document();
//		Document document = conrenewalpdf.document;
//		pdfstream = new ByteArrayOutputStream();
//		
//		try {
//			PdfWriter.getInstance(conrenewalpdf.document, pdfstream);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//			
//		document.open();
//		conrenewalpdf.setContractRewnewal(contractRenewal);
//		conrenewalpdf.createPdf("no");
//		document.close();
//		logger.log(Level.SEVERE, "Contract Renewal email attchment method"+pdfstream);

		
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "ContractRenewalNewFormat", contractRenewal.getCompanyId())){
				
				Contract contract = ofy().load().type(Contract.class).filter("companyId", contractRenewal.getCompanyId()).filter("count", contractRenewal.getContractId()).first().now();
				
				NewContractRenewalPdf newConRenFormat=new NewContractRenewalPdf();
				
				 newConRenFormat.document = new Document();
				 Document document = newConRenFormat.document;
				 pdfstream = new ByteArrayOutputStream();
				 try {
						PdfWriter.getInstance(document, pdfstream);
					} catch (DocumentException e) {
						e.printStackTrace();
					}	
					 
					 document.open();
					 newConRenFormat.setContractRewnewal(contract);
					 newConRenFormat.createPdf("no");
					document.close();
				}
		    else if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "OnlyforPecopp", companyId)){
			
		    	ContractRenewalPdf wopdf = new ContractRenewalPdf();
		    	wopdf.document = new Document();
		    	Document document = wopdf.document;
		    	pdfstream = new ByteArrayOutputStream();
		    	try {
					PdfWriter.getInstance(document, pdfstream);
				} catch (DocumentException e) {
					e.printStackTrace();
				}	
			 	document.open();
				wopdf.setContractRewnewal(contractRenewal);
				wopdf.createPdf("no");
			   
			   document.close();
			}
		    else if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "PC_PrintRenewalLetterInEnglishAndThai", companyId)){
		    	
		    	ContractRenewalPdfversionOne crpdf = new ContractRenewalPdfversionOne();
		    	crpdf.document = new Document();
				Document document = crpdf.document;
				 pdfstream = new ByteArrayOutputStream();
				try {
					PdfWriter.getInstance(document, pdfstream);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
				document.open();
				crpdf.setContractRewnewal(contractRenewal, "no");
				crpdf.createPdf("English");
				
				document.close();
			}
		    else if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", AppConstants.PC_PDF_ContractRenewal_Old_New_Price_format,companyId)){
				
				ContractRenewalPdf wopdf = new ContractRenewalPdf();
				wopdf.document = new Document();
				Document document = wopdf.document;
				 pdfstream = new ByteArrayOutputStream();
				try {
					PdfWriter.getInstance(document, pdfstream);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
//					if(wo==null){
//							Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", companyId).filter("count", count).first().now();
//							if(contractEntity!=null){
//								wo = ofy().load().type(ContractRenewal.class).filter("companyId", companyId).filter("contractId", contractEntity.getRefContractCount()).first().now();
//							}
//					}
				 	document.open();
					wopdf.setContractRewnewal(contractRenewal);
				    wopdf.createPdf("no");
				   
				document.close();
				}
			/*end*/
		    else{
				
				Contract contract = ofy().load().type(Contract.class).filter("companyId", contractRenewal.getCompanyId()).filter("count", contractRenewal.getContractId()).first().now();
				NewContractRenewalVersionOnePdf ncrpdf = new NewContractRenewalVersionOnePdf();
				ncrpdf.document = new Document();
				Document document = ncrpdf.document;
				 pdfstream = new ByteArrayOutputStream();
				try {
					PdfWriter.getInstance(document, pdfstream);
				} catch (DocumentException e) {
					e.printStackTrace();
				} 
				 	document.open();
				 	ncrpdf.setContractRewnewal(contract,contractRenewal);
					ncrpdf.createPdf("no","ContractRenewal");
				   
				document.close();
				}
		
	}
	
public String sendNewEmail(EmailDetails emailDetails, long companyId, boolean taskqueueCallFlag) {
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		
		Company company = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		String uploadDocName = null;
		
		String emailfailedSubject = "Email sending failed - "+emailDetails.getSubject();
		String emailfailedBody = "Dear Sir, <br> Email sending failed. Please check log";
		
		
		ServerAppUtility serverutility = new ServerAppUtility();
		
		try {
			
		pdfstream = null;
		boolean salesPersonEmailIdFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.PC_ENABLESENDEMAILSFROMPERSONRESPONSIBLE, companyId);
		/**
		 * @author Vijay Chougule Date 28-12-2020
		 * Des :- Email From Sales Person and CC in Approver Name and Branch Email id and Company Email Id
		 * send Emails From SendGrid
		 * 
		 */
		if(salesPersonEmailIdFlag && (emailDetails.getEntityName().trim().equals("Quotation") || emailDetails.getEntityName().trim().equals("Contract") ||
				emailDetails.getEntityName().trim().equals("SalesQuotation") || emailDetails.getEntityName().trim().equals("SalesOrder") ||
				emailDetails.getEntityName().trim().equals("Complain") || emailDetails.getEntityName().trim().equals("PurchaseOrder"))){
			logger.log(Level.SEVERE, "Inside cc bcc and EmailfromPersonResponsible");

			ArrayList<String> ccEmailList = new ArrayList<String>();
			ArrayList<String> bcclist = new ArrayList<String>();

			if(company.getEmail()!=null){
				ccEmailList.add(company.getEmail());
			}
			
			 if(emailDetails.getEntityName().trim().equals("Quotation")){
				Quotation quotation = (Quotation) emailDetails.getModel();
				
				String approverEmailId = getApproverEmailId(quotation.getApproverName(),quotation.getCompanyId());
				if(approverEmailId!=null) {
					ccEmailList.add(approverEmailId);
				}
				String branchEmailId = getBranchEmailId(quotation.getBranch(), quotation.getCompanyId());
				if(branchEmailId!=null) {
					ccEmailList.add(branchEmailId);
				}
				
				bcclist.add(emailDetails.getFromEmailid());

			 }
			 else if(emailDetails.getEntityName().trim().equals("Contract")){
				 Contract contract = (Contract) emailDetails.getModel();
				 String approverEmailId = getApproverEmailId(contract.getApproverName(),contract.getCompanyId());
					if(approverEmailId!=null) {
						ccEmailList.add(approverEmailId);
					}
					String branchEmailId = getBranchEmailId(contract.getBranch(), contract.getCompanyId());
					if(branchEmailId!=null) {
						ccEmailList.add(branchEmailId);
					}
					
					bcclist.add(emailDetails.getFromEmailid());
			 }
			 else if(emailDetails.getEntityName().trim().equals("SalesQuotation")){
				 SalesQuotation salesQuotation = (SalesQuotation) emailDetails.getModel();
				 String approverEmailId = getApproverEmailId(salesQuotation.getApproverName(),salesQuotation.getCompanyId());
					if(approverEmailId!=null) {
						ccEmailList.add(approverEmailId);
					}
					String branchEmailId = getBranchEmailId(salesQuotation.getBranch(), salesQuotation.getCompanyId());
					if(branchEmailId!=null) {
						ccEmailList.add(branchEmailId);
					}
					
					bcclist.add(emailDetails.getFromEmailid());
			 }
			 else if(emailDetails.getEntityName().trim().equals("SalesOrder")) {
			 		SalesOrder salesOrder = (SalesOrder) emailDetails.getModel();
					String approverEmailId = getApproverEmailId(salesOrder.getApproverName(),salesOrder.getCompanyId());
					if(approverEmailId!=null) {
						ccEmailList.add(approverEmailId);
					}
					String branchEmailId = getBranchEmailId(salesOrder.getBranch(), salesOrder.getCompanyId());
					if(branchEmailId!=null) {
						ccEmailList.add(branchEmailId);
					}
			 }
			 else if(emailDetails.getEntityName().trim().equals("Complain")){
				 	Complain complain = (Complain) emailDetails.getModel();
					String branchEmailId = getBranchEmailId(complain.getBranch(), complain.getCompanyId());
					if(branchEmailId!=null) {
						ccEmailList.add(branchEmailId);
					}
					Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", complain.getCompanyId())
											.filter("count", complain.getExistingContractId()).first().now();
					logger.log(Level.SEVERE,"contractEntity"+contractEntity);
					if(contractEntity!=null) {
						if(contractEntity.getAccountManager()!=null) {
							logger.log(Level.SEVERE,"contractEntity getAccountManager "+contractEntity.getAccountManager());
							User userEntity = ofy().load().type(User.class).filter("companyId",complain.getCompanyId())
												.filter("employeeName", contractEntity.getAccountManager()).first().now();
							logger.log(Level.SEVERE,"userEntity"+userEntity);
							if(userEntity!=null && userEntity.getEmail()!=null && !userEntity.getEmail().equals("")) {
								ccEmailList.add(userEntity.getEmail());
								logger.log(Level.SEVERE,"userEntity.getEmail()"+userEntity.getEmail());

							}
						}
					}
					
					bcclist.add(emailDetails.getFromEmailid());	
			 }
			 else if(emailDetails.getEntityName().trim().equals("PurchaseOrder")){
				 PurchaseOrder po = (PurchaseOrder) emailDetails.getModel();
				 String approverEmailId = getApproverEmailId(po.getApproverName(),po.getCompanyId());
					if(approverEmailId!=null) {
						ccEmailList.add(approverEmailId);
					}
					String branchEmailId = getBranchEmailId(po.getBranch(), po.getCompanyId());
					if(branchEmailId!=null) {
						ccEmailList.add(branchEmailId);
					}
					if(po.getRefOrderNO()!=null && !po.getRefOrderNO().equals("")) {
						try {
							int salesorderId = Integer.parseInt(po.getRefOrderNO());
							SalesOrder salesorderEntity = ofy().load().type(SalesOrder.class).filter("companyId", po.getCompanyId())
												.filter("count", salesorderId).first().now();
							if(salesorderEntity!=null) {
								String salesPersonemailid = getApproverEmailId(salesorderEntity.getEmployee(),salesorderEntity.getCompanyId());
								if(salesPersonemailid!=null) {
									ccEmailList.add(salesPersonemailid);
								}
							}
						} catch (Exception e) {
							// TODO: handle exception
						}
						
					}
					bcclist.add(emailDetails.getFromEmailid());	

			 }
			 else if(emailDetails.getEntityName().trim().equals(AppConstants.CONTRACT_RENEWAL)){
					ContractRenewal contractRenewal = (ContractRenewal) emailDetails.getModel();
					logger.log(Level.SEVERE, "Contract Renewal email attchment");
					createContractRenewalEmailAttachment(contractRenewal,companyId);
			}
			 else if(emailDetails.getEntityName().trim().equals("Service")){
					Service serviceEntity = (Service) emailDetails.getModel();
					logger.log(Level.SEVERE, "SR Copy email attachment");
					
					String ssr1="";
					String ssr2="";

					
					if(serviceEntity.getUptestReport()!=null){
						ssr1=serviceEntity.getUptestReport().getName();
					}

					if(serviceEntity.getServiceSummaryReport2()!=null){
						ssr2=serviceEntity.getServiceSummaryReport2().getName();
					}
					

					if(serviceEntity.getServiceSummaryReport2()!=null) {
					logger.log(Level.SEVERE,"serviceObj.getServiceSummaryReport2().getName()"+ serviceEntity.getServiceSummaryReport2().getName());
					}
					logger.log(Level.SEVERE,"ssr1"+ ssr1);
					logger.log(Level.SEVERE,"ssr1"+ ssr2);
					ArrayList<Service> servicelist = new ArrayList<Service>();
					servicelist.add(serviceEntity);
					createSRCopyttachment(servicelist);
					
					if(emailDetails.getEmailBody().contains("{Feedback_Link}")){
						Company companyEntity = ofy().load().type(Company.class).filter("companyId", serviceEntity.getCompanyId()).first().now();

						if(companyEntity!=null&&companyEntity.getFeedbackUrlList()!=null&&companyEntity.getFeedbackUrlList().size()!=0){
							String feedbackFormUrl="";
//							Customer customer=ofy().load().type(Customer.class).filter("companyId",serviceEntity.getCompanyId()).filter("count",serviceEntity.getPersonInfo().getCount()).first().now();
//							if(customer!=null){
//								if(customer.getCategory()!=null&&!customer.getCategory().equals("")){
									for(FeedbackUrlAsPerCustomerCategory feedback:companyEntity.getFeedbackUrlList()){
//										if(feedback.getCustomerCategory().equals(customer.getCategory())){
											feedbackFormUrl=feedback.getFeedbackUrl();
											break;
//										}
									}
//								}
//							}
							
						final String serDate = dateFormat.format(serviceEntity.getServiceDate());
						logger.log(Level.SEVERE, "ZOHO FEEDBACK feedbackFormUrl " + feedbackFormUrl);

						if(feedbackFormUrl!=null&&!feedbackFormUrl.equals("")){
							logger.log(Level.SEVERE, "ZOHO FEEDBACK URL " + feedbackFormUrl);
							StringBuilder sbPostData= new StringBuilder(feedbackFormUrl);
					        sbPostData.append("?customerId="+serviceEntity.getPersonInfo().getCount());
					        sbPostData.append("&customerName="+serviceEntity.getPersonInfo().getFullName());
					        sbPostData.append("&serviceId="+serviceEntity.getCount());
					        sbPostData.append("&serviceDate="+serDate);
					        sbPostData.append("&serviceName="+serviceEntity.getProductName());
					        sbPostData.append("&technicianName="+serviceEntity.getEmployee());
					        sbPostData.append("&branch="+serviceEntity.getBranch());
					        feedbackFormUrl = sbPostData.toString();
					        feedbackFormUrl=feedbackFormUrl.replaceAll("\\s", "%20");
					        feedbackFormUrl=feedbackFormUrl.replaceAll("&", "%26");

					        feedbackFormUrl=ServerAppUtility.getTinyUrl(feedbackFormUrl,companyEntity.getCompanyId());
					        
					        logger.log(Level.SEVERE, "FINAL ZOHO FEEDBACK URL " + feedbackFormUrl);
					        String emailbody = emailDetails.getEmailBody();
							String updatedEmailbody=emailbody.replace("{Feedback_Link}", feedbackFormUrl);
							emailDetails.setEmailBody(updatedEmailbody);
						}
					}
					}
				}	
				
			
			 if(ccEmailList.size()>0){
				 emailDetails.getCcEmailId().addAll(ccEmailList);
			 }
			 if(bcclist.size()>0){
				 emailDetails.getBccEmailId().addAll(bcclist);
			 }
				
		}
			

		// Too Email id's in array
		String[] toEmailIdArray = new String[emailDetails.getToEmailId().size()];
		emailDetails.getToEmailId().toArray(toEmailIdArray);

		// CC Email id's in array
		String[] ccEmailIdArray = new String[emailDetails.getCcEmailId().size()];
		emailDetails.getCcEmailId().toArray(ccEmailIdArray);
		
		// BCC Email id's in array
		String[] bccEmailIdArray = new String[emailDetails.getBccEmailId().size()];
		emailDetails.getBccEmailId().toArray(bccEmailIdArray);

		String attachFiles = "Document";
		String paymentGatewayUniqueId ="";
		logger.log(Level.SEVERE,"emailDetails.getEntityName()"+emailDetails.getEntityName());
		
		String jsonData ="";
		String strmsgBody = emailDetails.getEmailBody();
		logger.log(Level.SEVERE,"strmsgBody"+strmsgBody);
		boolean paymentGatewayRequestLinkFlag = strmsgBody.contains(AppConstants.EMAILPAYMENTGATEWAYREQUEST);
		logger.log(Level.SEVERE,"paymentGatewayRequestLinkFlag"+paymentGatewayRequestLinkFlag);
		if(paymentGatewayRequestLinkFlag==false){
			paymentGatewayRequestLinkFlag = strmsgBody.contains(AppConstants.EMAILPAYMENTGATEWAYREQUESTLINK2);
		}
		logger.log(Level.SEVERE,"paymentGatewayRequestLinkFlag"+paymentGatewayRequestLinkFlag);

		CommonServiceImpl commonserviceimpl = new CommonServiceImpl();
		logger.log(Level.SEVERE,"emailDetails.isPdfAttachment()"+emailDetails.isPdfAttachment());

			
			boolean companyAsLetterHead = false;
			if(emailDetails.isPdfAttachment()){
				if(emailDetails.getEntityName().trim().equals("Lead")){
					Lead sales = (Lead) emailDetails.getModel();
				}
				else if(emailDetails.getEntityName().trim().equals("Quotation")){
					logger.log(Level.SEVERE, "Inside quotation == ");
					Quotation quotation = (Quotation) emailDetails.getModel();
					createQuotationAttchmentData(quotation);
					attachFiles =  quotation.getCount() + " "+ quotation.getCinfo().getFullName();
					if (quotation.getDocument() != null
							&& quotation.getDocument().getUrl() != null) {
						uploadDocName = quotation.getDocument().getName();
					}
				}
				else if(emailDetails.getEntityName().trim().equals("Contract")){
					Sales sales = (Sales) emailDetails.getModel();
					if(!emailDetails.isRenewalEmailReminder()){
						attachFiles =  sales.getCount() + " "+ sales.getCinfo().getFullName();
						setServiceContract(sales);
						initializeServiceContract();
						
						Contract contract = (Contract) emailDetails.getModel();
						if (contract.getDocument() != null
								&& contract.getDocument().getUrl() != null) {
							uploadDocName = contract.getDocument().getName();
						}
					}
					else{
						logger.log(Level.SEVERE,"Renewal Email reminder");
						Contract contract = (Contract) emailDetails.getModel();
						con = contract;
						loadData();
						setContractRenewalNew(contract);
						if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "CompanyAsLetterHead", contract.getCompanyId())){
								companyAsLetterHead=true;
						}
						logger.log(Level.SEVERE, "companyAsLetterHead value11 "+companyAsLetterHead);
						createContractRenewalNewAttachment(companyAsLetterHead);
						attachFiles = "RenewalLetter";
					}
					
				}
				else if(emailDetails.getEntityName().trim().equals("SalesQuotation")){
					SalesQuotation salesquotation = (SalesQuotation) emailDetails.getModel();
					attachFiles =  salesquotation.getCount() + " "+ salesquotation.getCinfo().getFullName();

					setSalesQuotation(salesquotation);
					initializeSalesQuotation();
					createSalesQuotationAttachment();
					createSalesQuotationUploadDocumentAttachment();
					
					if (salesquotation.getDocument() != null
							&& salesquotation.getDocument().getUrl() != null) {
						uploadDocName = salesquotation.getDocument().getName();
					}
				}
				else if(emailDetails.getEntityName().trim().equals("SalesOrder")){
					SalesOrder salesOrder = (SalesOrder) emailDetails.getModel();
					attachFiles =  salesOrder.getCount() + " "+ salesOrder.getCinfo().getFullName();
					setSalesOrder(salesOrder);
					initializeSalesOrder();
					createSalesOrderAttachment();
					createSalesOrderUploadDocumentAttachment();
					
					if (salesOrder.getDocument() != null
							&& salesOrder.getDocument().getUrl() != null) {
						uploadDocName = salesOrder.getDocument().getName();
					}
					
				}
				else if(emailDetails.getEntityName().trim().equals("Invoice") || emailDetails.getEntityName().trim().equals("VendorInvoice") ){
					Invoice invoice = (Invoice) emailDetails.getModel();
					attachFiles =  invoice.getCount() + " "+ invoice.getPersonInfo().getFullName();
					createInvoiceAttachmentDetails(invoice);
				}
				else if(emailDetails.getEntityName().trim().equals("CustomerPayment")){
					CustomerPayment payment = (CustomerPayment) emailDetails.getModel();
					attachFiles =  payment.getCount() + " "+ payment.getPersonInfo().getFullName();
					createPaymentAttachmentDetails(payment);
				}
				else if(emailDetails.getEntityName().trim().equals("Complain")){
					Complain complain = (Complain) emailDetails.getModel();
					attachFiles =  complain.getCount() + " "+ complain.getPersoninfo().getFullName();
					createComplainPdfAttachment(complain);
				}
				else if(emailDetails.getEntityName().trim().equals("DeliveryNote")){
					DeliveryNote deliveryNote = (DeliveryNote) emailDetails.getModel();
					attachFiles =  deliveryNote.getCount() + " "+ deliveryNote.getCinfo().getFullName();
					
					setDeliveryNote(deliveryNote);
					initializeDeliveryDetails();
					createDeliveryAttachment();
					createDeliveryNoteUploadAttachment();
					
					if (deliveryNote.getDeliveryDocument() != null
							&& !deliveryNote.getDeliveryDocument().getUrl()
									.equals("")) {
						uploadDocName = deliveryNote.getDeliveryDocument().getName();
					}
				}
				else if(emailDetails.getEntityName().trim().equals("PurchaseRequisition")){
					PurchaseRequisition purchaseReq = (PurchaseRequisition) emailDetails.getModel();
					setPR(purchaseReq);
					loadDataForPr();
					createPRUploadDocumentAttachment();
					
					if (purchaseReq.getUptestReport() != null
							&& purchaseReq.getUptestReport().getUrl() != null) {
						uploadDocName = purchaseReq.getUptestReport().getName();
					}
					
				}
				else if(emailDetails.getEntityName().trim().equals("RequsestForQuotation")){
					RequsestForQuotation rfq = (RequsestForQuotation) emailDetails.getModel();
					setRFQ(rfq);
					loadDataForRfq();
					createRFQUploadDocumentAttachment();
					if (rfq.getUptestReport() != null
							&& !rfq.getUptestReport().getUrl().equals("")) {
						uploadDocName = rfq.getUptestReport().getName();
					}

				}
				else if(emailDetails.getEntityName().trim().equals("LetterOfIntent")){
					LetterOfIntent loiEntity = (LetterOfIntent) emailDetails.getModel();
					
					setLOI(loiEntity);
					loadDataForLoi();
					createLoiUploadDocumentAttachment();
					
					if (loiEntity.getUptestReport() != null
							&& loiEntity.getUptestReport().getUrl() != null) {
						uploadDocName = loiEntity.getUptestReport().getName();
					}
					

				}
				else if(emailDetails.getEntityName().trim().equals("PurchaseOrder")){
				//	Boolean CompanyAsLetterHead = false;
					PurchaseOrder po = (PurchaseOrder) emailDetails.getModel();
					attachFiles = "PurchaseOrder"+po.getCount()+"";
					setPO(po);
					initializePurchaseOrder();
					
					logger.log(Level.SEVERE, "attachmnet id in PO " +po.getCompanyId());
					createPOAttachment(po.getCompanyId());
					createPOUploadDocumentAttachment();
					
					if (po.getUptestReport() != null
							&& !po.getUptestReport().getUrl().equals("")) {
						uploadDocName = po.getUptestReport().getName();
					}
				}
				else if(emailDetails.getEntityName().trim().equals("VendorInvoice")){
					VendorInvoice vendorInvoice = (VendorInvoice) emailDetails.getModel();
					attachFiles =  vendorInvoice.getCount() + " "+ vendorInvoice.getPersonInfo().getFullName();
					createVednorInvoiceAttachmentDetails(vendorInvoice);
				}
				else if(emailDetails.getEntityName().trim().equals("PaySlip")){
					PaySlip payslipEntity = (PaySlip) emailDetails.getModel();
					attachFiles =  payslipEntity.getCount() + " "+ payslipEntity.getEmployeeName();

					setSalarySlip(payslipEntity);
				    createSalarySlipAttachment();
				    
				}
				else if(emailDetails.getEntityName().trim().equals(AppConstants.CONTRACT_RENEWAL)){
					ContractRenewal contractRenewal = (ContractRenewal) emailDetails.getModel();
					logger.log(Level.SEVERE, "Contract Renewal email attchment");
					attachFiles = "RenewalLetter";
					createContractRenewalEmailAttachment(contractRenewal,companyId);
				}
				else if(emailDetails.getEntityName().trim().equals("Service")){
					Service serviceEntity = (Service) emailDetails.getModel();
					logger.log(Level.SEVERE, "SR Copy email attachment");
					
					String ssr1="";
					String ssr2="";

					
					if(serviceEntity.getUptestReport()!=null){
						ssr1=serviceEntity.getUptestReport().getName();
					}

					if(serviceEntity.getServiceSummaryReport2()!=null){
						ssr2=serviceEntity.getServiceSummaryReport2().getName();
					}
					

					if(serviceEntity.getServiceSummaryReport2()!=null) {
					logger.log(Level.SEVERE,"serviceObj.getServiceSummaryReport2().getName()"+ serviceEntity.getServiceSummaryReport2().getName());
					}
					logger.log(Level.SEVERE,"ssr1"+ ssr1);
					logger.log(Level.SEVERE,"ssr1"+ ssr2);
					
					attachFiles = "SR Copy -" +serviceEntity.getCount() + " "+","+ssr1+","+ssr2;
					ArrayList<Service> servicelist = new ArrayList<Service>();
					servicelist.add(serviceEntity);
					
					createSRCopyttachment(servicelist);
					
					
					if(strmsgBody.contains("{Feedback_Link}")||strmsgBody.contains("{Link}")){
						Company companyEntity = ofy().load().type(Company.class).filter("companyId", serviceEntity.getCompanyId()).first().now();

						if(companyEntity!=null&&companyEntity.getFeedbackUrlList()!=null&&companyEntity.getFeedbackUrlList().size()!=0){
							String feedbackFormUrl="";
//							Customer customer=ofy().load().type(Customer.class).filter("companyId",serviceEntity.getCompanyId()).filter("count",serviceEntity.getPersonInfo().getCount()).first().now();
//							if(customer!=null){
//								if(customer.getCategory()!=null&&!customer.getCategory().equals("")){
									for(FeedbackUrlAsPerCustomerCategory feedback:companyEntity.getFeedbackUrlList()){
//										if(feedback.getCustomerCategory().equals(customer.getCategory())){
											feedbackFormUrl=feedback.getFeedbackUrl();
											break;
//										}
//									}
//								}
							}
							
						final String serDate = dateFormat.format(serviceEntity.getServiceDate());
						logger.log(Level.SEVERE, "ZOHO FEEDBACK URL feedback Form Url " + feedbackFormUrl);
						if(feedbackFormUrl!=null&&!feedbackFormUrl.equals("")){
							logger.log(Level.SEVERE, "ZOHO FEEDBACK URL " + feedbackFormUrl);
							StringBuilder sbPostData= new StringBuilder(feedbackFormUrl);
					        sbPostData.append("?customerId="+serviceEntity.getPersonInfo().getCount());
					        sbPostData.append("&customerName="+serviceEntity.getPersonInfo().getFullName());
					        sbPostData.append("&serviceId="+serviceEntity.getCount());
					        sbPostData.append("&serviceDate="+serDate);
					        sbPostData.append("&serviceName="+serviceEntity.getProductName());
					        sbPostData.append("&technicianName="+serviceEntity.getEmployee());
					        sbPostData.append("&branch="+serviceEntity.getBranch());
					        feedbackFormUrl = sbPostData.toString();
					        feedbackFormUrl=feedbackFormUrl.replaceAll("\\s", "%20");
					        feedbackFormUrl=feedbackFormUrl.replaceAll("&", "%26");

					        feedbackFormUrl=ServerAppUtility.getTinyUrl(feedbackFormUrl,companyEntity.getCompanyId());
					        
					        logger.log(Level.SEVERE, "FINAL ZOHO FEEDBACK URL " + feedbackFormUrl);
							strmsgBody=strmsgBody.replace("{Feedback_Link}", feedbackFormUrl);
							strmsgBody=strmsgBody.replace("{Link}", feedbackFormUrl);
						}
					}
					}
					
					/*
					 * Ashwini Patil 
					 * Date:28-03-2024
					 * Eco Safe reported this issue
					 * Problem: When we configure automatic email templates for manual email popup, placeholders are not getting replaced as here code is checking template name.
					 * Solution: we are writting one else block which will get executed when not matching template found
					 * here we are trying to replace all the placeholders related to service
					 */
					final String prodName = serviceEntity.getProductName();
					final String serNo = serviceEntity.getServiceSerialNo()+"";
					final String serDate = dateFormat.format(serviceEntity.getServiceDate());
					String custName = serviceEntity.getPersonInfo().getFullName();
					String companyName = company.getBusinessUnitName();
					
					String replacedMsg = strmsgBody.replace("{CustomerName}", custName);
					replacedMsg = replacedMsg.replace("{ProductName}", prodName);
					replacedMsg = replacedMsg.replace("{ServiceNo}", serNo);
					replacedMsg =  replacedMsg.replace("{ServiceDate}", serDate);
					replacedMsg = replacedMsg.replace("{companyName}", companyName);
					replacedMsg = replacedMsg.replace("{CompanyName}", companyName);
													
					
					replacedMsg=replacedMsg.replace("{ServiceNumber}", serviceEntity.getCount()+"");
					replacedMsg=replacedMsg.replace("{DD-MM-YY}", sdf.format(serviceEntity.getServiceDate())+"");
					replacedMsg=replacedMsg.replace("{TechnicianName}", serviceEntity.getEmployee());
					replacedMsg=replacedMsg.replace("{Date}", sdf.format(serviceEntity.getServiceDate())+"");
					replacedMsg=replacedMsg.replace("{Time}", serviceEntity.getServiceTime()+"");
					replacedMsg=replacedMsg.replace("{companynumber}",company.getCellNumber1()+"");
					
					if(replacedMsg.contains("{ServiceCompletionDate}")) {
						if(serviceEntity!=null&&serviceEntity.getCompletedDate()!=null)
							replacedMsg=replacedMsg.replace("{ServiceCompletionDate}",sdf.format(serviceEntity.getCompletedDate()));	
						else
							replacedMsg=replacedMsg.replace("{ServiceCompletionDate}","");	
					}
			
					if(serviceEntity.getEmployee()!=null&&!serviceEntity.getEmployee().equals("")) {
					Employee emp = ofy().load().type(Employee.class).filter("companyId", company.getCompanyId()).filter("fullname", serviceEntity.getEmployee()).first().now();				
					replacedMsg=replacedMsg.replace("{Number}", emp.getContacts().get(0).getCellNo1()+"");
//					
					}
					
					if(replacedMsg.contains("{DocumentId}")){
						replacedMsg =  replacedMsg.replace("{DocumentId}", serviceEntity.getCount()+"");
					}
					if(replacedMsg.contains("{PDFLink}")){
						String pdflink = commonserviceimpl.getPDFURL("Service", serviceEntity, serviceEntity.getCount(), serviceEntity.getCompanyId(), company, commonserviceimpl.getCompleteURL(company),null);
						logger.log(Level.SEVERE, "Service record pdflink " + pdflink);
						String tinyurl = ServerAppUtility.getTinyUrl(pdflink,company.getCompanyId());
						replacedMsg =  replacedMsg.replace("{PDFLink}", tinyurl);
					}
					replacedMsg=replacedMsg.replace("{CompanySignature}",ServerAppUtility.getCompanySignature(serviceEntity.getBranch(), serviceEntity.getCompanyId()));		
					
					strmsgBody=replacedMsg;
					
				}	
				else if(emailDetails.getEntityName().trim().equals("ASSESMENTREPORT")) {					
					AssesmentReport assessmentReport = (AssesmentReport) emailDetails.getModel();
					attachFiles =  "AssessmentReport"+assessmentReport.getCount() + " "+ assessmentReport.getCinfo().getFullName();
					createAssessmentReportAttachmentDetails(assessmentReport);
				}
				
			}

		
			if(paymentGatewayRequestLinkFlag){ 
				
				String str = commonserviceimpl.validatePaymentDocument(emailDetails.getEntityName(),emailDetails.getModel().getCount(),companyId);
				if(str!=null && !str.equals("")){
					return str;
				}
				
				boolean paymentGatewayUniqueIdFlag = false;
				ContractInvoicePayment contractinvoice = new ContractInvoicePayment();
				
				if(emailDetails.getEntityName().trim().equals("Lead")){
					Lead lead = (Lead) emailDetails.getModel();

					Lead leadEntity = ofy().load().type(Lead.class).filter("companyId",companyId).filter("count", emailDetails.getModel().getCount())
										.first().now();
					if(leadEntity!=null && (leadEntity.getNumberRange()==null || leadEntity.getNumberRange().equals(""))){
						String numberRangeValidation = commonserviceimpl.validateNumberRange(companyId,AppConstants.LEAD);
						if(!numberRangeValidation.equals("")){
							return numberRangeValidation;
						}
						
						/**
						 * @author Vijay Date 11-02-2022
						 * Des :- if duplicate Sr no found in product list then here not processing payment document
						 * issue pecopp having some lead duplicate SR no so creating double services
						 */
						boolean duplicateSrNoflag = contractinvoice.validateDuplicateProductSrNo(leadEntity.getLeadProducts());
						logger.log(Level.SEVERE, "duplicateSrNoflag " + duplicateSrNoflag);
						if(duplicateSrNoflag==false) {
							return "Duplicate product sr no found please contact EVA support!";
						}
						logger.log(Level.SEVERE, "duplicateSrNoflag ==" + duplicateSrNoflag);
						
					}
					
					if(leadEntity!=null && leadEntity.getPaymentGatewayUniqueId()!=null && !leadEntity.getPaymentGatewayUniqueId().equals("")){
						paymentGatewayUniqueIdFlag = true;
						paymentGatewayUniqueId = leadEntity.getPaymentGatewayUniqueId();
					}
					else{
						Customer customer = ofy().load().type(Customer.class).filter("companyId", lead.getCompanyId())
											.filter("count", lead.getPersonInfo().getCount()).first().now();
						jsonData = commonserviceimpl.createJsonData(emailDetails.getModel(),AppConstants.LEAD, emailDetails.getAppURL(), emailDetails.getAppId(), company, customer,null,emailDetails.getLandingPageText());
						
					}
							

				}
				else if(emailDetails.getEntityName().trim().equals("Quotation")){
					logger.log(Level.SEVERE, "Inside quotation == ");
					Quotation quotation = (Quotation) emailDetails.getModel();
					Quotation quotationEntity = ofy().load().type(Quotation.class).filter("companyId",companyId).filter("count", emailDetails.getModel().getCount())
							.first().now();
					
					if(quotationEntity!=null && (quotationEntity.getQuotationNumberRange()==null || quotationEntity.getQuotationNumberRange().equals(""))){
						String numberRangeValidation = commonserviceimpl.validateNumberRange(companyId,AppConstants.QUOTATION);
						if(!numberRangeValidation.equals("")){
							return numberRangeValidation;
						}
						/**
						 * @author Vijay Date 11-02-2022
						 * Des :- if duplicate Sr no found in product list then here not processing payment document
						 * issue pecopp having some lead duplicate SR no so creating double services
						 */
						boolean duplicateSrNoflag = contractinvoice.validateDuplicateProductSrNo(quotationEntity.getItems());
						logger.log(Level.SEVERE, "duplicateSrNoflag " + duplicateSrNoflag);
						if(duplicateSrNoflag==false) {
							return "Duplicate product sr no found please contact EVA support!";
						}
						logger.log(Level.SEVERE, "duplicateSrNoflag ==" + duplicateSrNoflag);
					}
					
					if(quotationEntity!=null && quotationEntity.getPaymentGatewayUniqueId()!=null && !quotationEntity.getPaymentGatewayUniqueId().equals("")){
						paymentGatewayUniqueIdFlag = true;
						paymentGatewayUniqueId = quotationEntity.getPaymentGatewayUniqueId();
					}
					else{
//						String pdf = "http://my.evadev0005.appspot.com/slick_erp/pdfservice?Id=5615125533818880&type=q&preprint=yes";
						String pdflink = commonserviceimpl.getPDFURL(AppConstants.QUOTATION, emailDetails.getModel(), emailDetails.getModel().getCount(), companyId, company, emailDetails.getPdfURL(),null);
						System.out.println("PDF Link "+pdflink);
						Customer customer = ofy().load().type(Customer.class).filter("companyId", quotation.getCompanyId())
											.filter("count", quotation.getCinfo().getCount()).first().now();
						jsonData = commonserviceimpl.createJsonData(emailDetails.getModel(),AppConstants.QUOTATION, emailDetails.getAppURL(), emailDetails.getAppId(), company, customer,pdflink,emailDetails.getLandingPageText());

					}
									}
				else if(emailDetails.getEntityName().trim().equals(AppConstants.CONTRACT)){
					
					Contract contract = (Contract) emailDetails.getModel();
					Contract contractEntity = ofy().load().type(Contract.class).filter("companyId",companyId).filter("count", emailDetails.getModel().getCount())
							.first().now();
					if(contractEntity!=null && (contractEntity.getNumberRange()==null || contractEntity.getNumberRange().equals(""))){
						String numberRangeValidation = commonserviceimpl.validateNumberRange(companyId,null);
						if(!numberRangeValidation.equals("")){
							return numberRangeValidation;
						}
						/**
						 * @author Vijay Date 11-02-2022
						 * Des :- if duplicate Sr no found in product list then here not processing payment document
						 * issue pecopp having some lead duplicate SR no so creating double services
						 */
						boolean duplicateSrNoflag = contractinvoice.validateDuplicateProductSrNo(contractEntity.getItems());
						logger.log(Level.SEVERE, "duplicateSrNoflag " + duplicateSrNoflag);
						if(duplicateSrNoflag==false) {
							return "Duplicate product sr no found please contact EVA support!";
						}
						logger.log(Level.SEVERE, "duplicateSrNoflag ==" + duplicateSrNoflag);
					}
					
					if(contractEntity!=null && contractEntity.getPaymentGatewayUniqueId()!=null && !contractEntity.getPaymentGatewayUniqueId().equals("")){
						paymentGatewayUniqueIdFlag = true;
						paymentGatewayUniqueId = contractEntity.getPaymentGatewayUniqueId();
					}
					else{
						Customer customer = ofy().load().type(Customer.class).filter("companyId", contract.getCompanyId())
								.filter("count", contract.getCinfo().getCount()).first().now();
						String pdflink = commonserviceimpl.getPDFURL(AppConstants.CONTRACT, emailDetails.getModel(), emailDetails.getModel().getCount(), companyId, company, emailDetails.getPdfURL(),null);
						System.out.println("PDF Link "+pdflink);
						jsonData = commonserviceimpl.createJsonData(emailDetails.getModel(),AppConstants.CONTRACT, emailDetails.getAppURL(), emailDetails.getAppId(), company, customer,pdflink,emailDetails.getLandingPageText());
			
					}
					
				}
				else if(emailDetails.getEntityName().trim().equals(AppConstants.CONTRACT_RENEWAL)){
					ContractRenewal contractRenewal = (ContractRenewal) emailDetails.getModel();
					
					ContractRenewal contractRenewalEntity = ofy().load().type(ContractRenewal.class).filter("companyId",companyId).filter("contractId", emailDetails.getModel().getCount())
							.first().now();
					if(contractRenewalEntity!=null && contractRenewalEntity.getPaymentGatewayUniqueId()!=null && !contractRenewalEntity.getPaymentGatewayUniqueId().equals("")){
						paymentGatewayUniqueIdFlag = true;
						paymentGatewayUniqueId = contractRenewalEntity.getPaymentGatewayUniqueId();
					}
					else{
						Customer customer = ofy().load().type(Customer.class).filter("companyId", companyId)
								.filter("count", contractRenewal.getCustomerId()).first().now();
						System.out.println("customer "+customer);
						String pdflink = commonserviceimpl.getPDFURL(AppConstants.CONTRACT_RENEWAL, emailDetails.getModel(), emailDetails.getModel().getCount(), companyId, company, emailDetails.getPdfURL(),null);
						jsonData = commonserviceimpl.createJsonData(emailDetails.getModel(),AppConstants.CONTRACT_RENEWAL, emailDetails.getAppURL(), emailDetails.getAppId(), company, customer,pdflink,emailDetails.getLandingPageText());
					}
				
				}
				else if(emailDetails.getEntityName().trim().equals("Invoice") ){
					Invoice invoice = (Invoice) emailDetails.getModel();
					
					Invoice invoiceEntity = ofy().load().type(Invoice.class).filter("companyId",companyId).filter("count", emailDetails.getModel().getCount())
							.first().now();
					if(invoiceEntity!=null && invoiceEntity.getPaymentGatewayUniqueId()!=null && !invoiceEntity.getPaymentGatewayUniqueId().equals("")){
						paymentGatewayUniqueIdFlag = true;
						paymentGatewayUniqueId = invoiceEntity.getPaymentGatewayUniqueId();
					}
					else{
						Customer customer = ofy().load().type(Customer.class).filter("companyId", invoice.getCompanyId())
											.filter("count", invoice.getPersonInfo().getCount()).first().now();
						String pdflink = commonserviceimpl.getPDFURL("Invoice", emailDetails.getModel(), emailDetails.getModel().getCount(), companyId, company, emailDetails.getPdfURL(),null);
						logger.log(Level.SEVERE, "invoice pdflink "+pdflink);
						jsonData = commonserviceimpl.createJsonData(emailDetails.getModel(),"Invoice", emailDetails.getAppURL(), emailDetails.getAppId(), company, customer,pdflink,emailDetails.getLandingPageText());
						
					}
					
				}

			logger.log(Level.SEVERE, "msgBody"+strmsgBody);

		
			if(paymentGatewayUniqueIdFlag==false){
				
			logger.log(Level.SEVERE,"Inside digital payment request condition");


			paymentGatewayUniqueId = commonserviceimpl.callPaymnetGatewayUniqueIdAPI(jsonData, emailDetails.getModel(), emailDetails.getEntityName());
			
			/**
			 * @author Vijay  Date :- 10-12-2021
			 * Des :- if there is any data issueto handle error updated the code
			 *
			 */
			if(paymentGatewayUniqueId==null){
				return "can not send digital payment link there is issue in data please contact to EVA Support!";
			}
			}
			
			String landingPagelink = AppConstants.PAYMENTGATEWAYSMSLINK+paymentGatewayUniqueId;
			logger.log(Level.SEVERE, "landingPagelink"+landingPagelink);

			StringBuilder sb = new StringBuilder(); // link will be landing page 
			
			String strcolor = "<head>" +
	                "<style type=\"text/css\">" +
	                "  .btn {\r\n" + 
	                "  background: #3498db;\r\n" + 
	                "  background-image: -webkit-linear-gradient(top, #3498db, #2980b9);\r\n" + 
	                "  background-image: -moz-linear-gradient(top, #3498db, #2980b9);\r\n" + 
	                "  background-image: -ms-linear-gradient(top, #3498db, #2980b9);\r\n" + 
	                "  background-image: -o-linear-gradient(top, #3498db, #2980b9);\r\n" + 
	                "  background-image: linear-gradient(to bottom, #3498db, #2980b9);\r\n" + 
	                "  -webkit-border-radius: 28;\r\n" + 
	                "  -moz-border-radius: 28;\r\n" + 
	                "  border-radius: 28px;\r\n" + 
	                "  font-family: Arial;\r\n" + 
	                "  color: #ffffff;\r\n" + 
	                "  font-size: 20px;\r\n" + 
	                "  padding: 10px 20px 10px 20px;\r\n" + 
	                "  border: solid #75787a 2px; \r\n"+
	                "  text-decoration: none;\r\n" + 
	                "}\r\n" + 
	                "\r\n" + 
	                ".btn:hover {\r\n" + 
	                "  background: #3cb0fd;\r\n" + 
	                "  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);\r\n" + 
	                "  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);\r\n" + 
	                "  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);\r\n" + 
	                "  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);\r\n" + 
	                "  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);\r\n" + 
	                "  text-decoration: none;\r\n" + 
	                "}" +
	                "</style>" +
	                "</head>";
			
			if(emailDetails.getEntityName().trim().equals("Invoice")){
				sb.append(strcolor);
				sb.append("<br><br> <p align=\"center\"> <a href= "+landingPagelink +">"+"<button class=btn > Pay Now </button></a> </p> <br><br> ");
			}
			else if(emailDetails.getEntityName().trim().equals("ContractRenewal")){
				sb.append(strcolor);
				sb.append("<br><br> <p align=\"center\"> <a href= "+landingPagelink +">"+"<button class=btn > Renew Order </button></a> </p> <br><br> ");
			}
			else{
				sb.append(strcolor);
				sb.append("<br><br>  <p align=\"center\"> <a  href= "+landingPagelink +">"+"<button class=btn > View Order </button> </a> </p> <br><br> ");

			}
			//Ashwini Patil Date:14-05-2022 Digital payment link was not getting replaced
			if(strmsgBody.contains(AppConstants.EMAILPAYMENTGATEWAYREQUESTLINK2))
				strmsgBody = strmsgBody.replace(AppConstants.EMAILPAYMENTGATEWAYREQUESTLINK2, sb.toString());
			if(strmsgBody.contains(AppConstants.EMAILPAYMENTGATEWAYREQUEST))
				strmsgBody = strmsgBody.replace(AppConstants.EMAILPAYMENTGATEWAYREQUEST, sb.toString());
			logger.log(Level.SEVERE, "msgBody after replacing add digital payment request to Place Order "+strmsgBody);
			
		}
			
		//Ashwini Patil	Date:3-02-2023 to replace placeholders
		if(emailDetails.getEntityName().trim().equals("ASSESMENTREPORT")) {
			AssesmentReport assessmentReport = (AssesmentReport) emailDetails.getModel();			
			String subject=emailDetails.getSubject();
			subject=subject.replace("{CustomerBranch}", assessmentReport.getCustomerBranch());
			subject=subject.replace("{DateOfAssessment}",dateFormat.format(assessmentReport.getAssessmentDate()));		
			emailDetails.setSubject(subject);
			String msg=strmsgBody;
			msg=msg.replace("{CustomerName}", assessmentReport.getCinfo().getFullName());
			msg=msg.replace("{CustomerBranch}", assessmentReport.getCustomerBranch());
			if(assessmentReport.getAssessedPeron1()!=null)
				msg=msg.replace("{SalesPerson}", assessmentReport.getAssessedPeron1());
			else
				msg=msg.replace("{SalesPerson}", "");
			
			if(assessmentReport.getAssessedPeron1()!=null)
				msg=msg.replace("{PersonName1}", assessmentReport.getAssessedPeron1());
			else
				msg=msg.replace("{PersonName1}", "");
			
			if(assessmentReport.getAssessedPeron2()!=null)
				msg=msg.replace("{PersonName2}", assessmentReport.getAssessedPeron2());
			else
				msg=msg.replace("{PersonName2}", "");
			
			if(assessmentReport.getAssessedPeron3()!=null)
				msg=msg.replace("{PersonName3}", assessmentReport.getAssessedPeron3());
			else
				msg=msg.replace("{PersonName3}", "");
			
			msg=msg.replace("{CompanyEmail}", ServerAppUtility.getCompanyEmail(assessmentReport.getBranch(), assessmentReport.getCompanyId()));
			msg=msg.replace("{CompanySignature}", ServerAppUtility.getCompanySignature(assessmentReport.getBranch(), assessmentReport.getCompanyId()));			
			strmsgBody=msg;
		}
		if(emailDetails.getEntityName().trim().equals("Contract")) {
			Contract contract = (Contract) emailDetails.getModel();
			//Ashwini Patil added this code to replace tags for customer portal share link templace
			String emailBody=strmsgBody;
			if(emailBody.contains("{PortalLink}")) {
				String Url ="";
				String custportalLink="";
				custportalLink=ServerAppUtility.getCustomerPortalLink(company);
				logger.log(Level.SEVERE,"custportalLink="+custportalLink);
				if(!custportalLink.equalsIgnoreCase("failed")) 
					emailBody=emailBody.replace("{PortalLink}","<a href=\""+custportalLink+"\"><button> View Portal </button></a>");	
				else
					logger.log(Level.SEVERE,"failed to load customer portal link");
			}
			emailBody=emailBody.replace("{CustomerName}",contract.getCinfo().getFullName());
			emailBody=emailBody.replace("{CustomerMobile}",contract.getCinfo().getCellNumber()+"");
			emailBody=emailBody.replace("{ContractId}",contract.getCount()+"");
			emailBody=emailBody.replace("{ContractDate}",sdf.format(contract.getContractDate()));
			emailBody=emailBody.replace("{ContractStartDate}",sdf.format(contract.getStartDate()));
			emailBody=emailBody.replace("{ContractEndDate}",sdf.format(contract.getEndDate()));
			if(contract.getAccountManager()!=null)
				emailBody=emailBody.replace("{AccountManager}",contract.getAccountManager());
			else
				emailBody=emailBody.replace("{AccountManager}","");
			
			emailBody=emailBody.replace("{CompanyName}", ServerAppUtility.getCompanyName(contract.getBranch(), company));								 
			emailBody=emailBody.replace("{CompanySignature}",ServerAppUtility.getCompanySignature(contract.getBranch(), contract.getCompanyId()));		
			strmsgBody=emailBody;
		}
		
		/*
		 * Ashwini Patil 
		 * Date:28-03-2024
		 * Pepcopp reported issue 
		 * added this code as companySignature was not getting replaced when sending email from email popup from quotation screen 
		 * also added some extra standard tags
		 */
		if(emailDetails.getEntityName().trim().equals("Quotation")) {
			Quotation quotation = (Quotation) emailDetails.getModel();
			String emailBody=strmsgBody;
			
			emailBody=emailBody.replace("{CustomerName}",quotation.getCinfo().getFullName());
			emailBody=emailBody.replace("{CustomerMobile}",quotation.getCinfo().getCellNumber()+"");
			emailBody=emailBody.replace("{DcoumentId}",quotation.getCount()+"");
			emailBody=emailBody.replace("{DocumentId}",quotation.getCount()+"");
			emailBody=emailBody.replace("{DocumentDate}",sdf.format(quotation.getQuotationDate()));			
			emailBody=emailBody.replace("{CompanyName}", ServerAppUtility.getCompanyName(quotation.getBranch(), company));								 
			emailBody=emailBody.replace("{CompanySignature}",ServerAppUtility.getCompanySignature(quotation.getBranch(), quotation.getCompanyId()));		
			strmsgBody=emailBody;
		}
		/*
		 * Ashwini Patil 
		 * Date:16-12-2024
		 * Om pest reported issue 
		 * added this code as tags were not getting replaced when sending email from email popup from invoice screen 
		 * also added some extra standard tags
		 */
		if(emailDetails.getEntityName().trim().equals("Invoice")) {
			Invoice invoice = (Invoice) emailDetails.getModel();
			String emailBody=strmsgBody;
			
			emailBody=emailBody.replace("{CustomerName}",invoice.getPersonInfo().getFullName());
			emailBody=emailBody.replace("{CustomerMobile}",invoice.getPersonInfo().getCellNumber()+"");
			emailBody=emailBody.replace("{DcoumentId}",invoice.getCount()+"");
			emailBody=emailBody.replace("{DocumentId}",invoice.getCount()+"");
			emailBody=emailBody.replace("{DocumentDate}",sdf.format(invoice.getInvoiceDate()));			
			emailBody=emailBody.replace("{CompanyName}", ServerAppUtility.getCompanyName(invoice.getBranch(), company));								 
			emailBody=emailBody.replace("{CompanySignature}",ServerAppUtility.getCompanySignature(invoice.getBranch(), invoice.getCompanyId()));		
			strmsgBody=emailBody;
			
			
			String subject=emailDetails.getSubject();
			subject=subject.replace("{CompanyName}", ServerAppUtility.getCompanyName(invoice.getBranch(), company));								 
			subject=subject.replace("{DocumentId}",invoice.getCount()+"");
			emailDetails.setSubject(subject);
		}
		 if(emailDetails.getEntityName().trim().equals("Service")){
				Service serviceEntity = (Service) emailDetails.getModel();				
			
				if(strmsgBody.contains("{Feedback_Link}")||strmsgBody.contains("{Link}")){
					Company companyEntity = ofy().load().type(Company.class).filter("companyId", serviceEntity.getCompanyId()).first().now();

					if(companyEntity!=null&&companyEntity.getFeedbackUrlList()!=null&&companyEntity.getFeedbackUrlList().size()!=0){
						String feedbackFormUrl="";
//						Customer customer=ofy().load().type(Customer.class).filter("companyId",serviceEntity.getCompanyId()).filter("count",serviceEntity.getPersonInfo().getCount()).first().now();
//						if(customer!=null){
//							if(customer.getCategory()!=null&&!customer.getCategory().equals("")){
								for(FeedbackUrlAsPerCustomerCategory feedback:companyEntity.getFeedbackUrlList()){
//									if(feedback.getCustomerCategory().equals(customer.getCategory())){
										feedbackFormUrl=feedback.getFeedbackUrl();
										break;
//									}
//								}
//							}
						}
						
					final String serDate = dateFormat.format(serviceEntity.getServiceDate());
					logger.log(Level.SEVERE, "ZOHO FEEDBACK URL feedback Form Url " + feedbackFormUrl);
					if(feedbackFormUrl!=null&&!feedbackFormUrl.equals("")){
						logger.log(Level.SEVERE, "ZOHO FEEDBACK URL " + feedbackFormUrl);
						StringBuilder sbPostData= new StringBuilder(feedbackFormUrl);
				        sbPostData.append("?customerId="+serviceEntity.getPersonInfo().getCount());
				        sbPostData.append("&customerName="+serviceEntity.getPersonInfo().getFullName());
				        sbPostData.append("&serviceId="+serviceEntity.getCount());
				        sbPostData.append("&serviceDate="+serDate);
				        sbPostData.append("&serviceName="+serviceEntity.getProductName());
				        sbPostData.append("&technicianName="+serviceEntity.getEmployee());
				        sbPostData.append("&branch="+serviceEntity.getBranch());
				        feedbackFormUrl = sbPostData.toString();
				        feedbackFormUrl=feedbackFormUrl.replaceAll("\\s", "%20");
				        feedbackFormUrl=feedbackFormUrl.replaceAll("&", "%26");

				        feedbackFormUrl=ServerAppUtility.getTinyUrl(feedbackFormUrl,companyEntity.getCompanyId());
				        
				        logger.log(Level.SEVERE, "FINAL ZOHO FEEDBACK URL " + feedbackFormUrl);
						strmsgBody=strmsgBody.replace("{Feedback_Link}", feedbackFormUrl);
						strmsgBody=strmsgBody.replace("{Link}", feedbackFormUrl);
					}
				}
				}
				
				/*
				 * Ashwini Patil 
				 * Date:28-03-2024
				 * Eco Safe reported this issue
				 * Problem: When we configure automatic email templates for manual email popup, placeholders are not getting replaced as here code is checking template name.
				 * Solution: we are writting one else block which will get executed when not matching template found
				 * here we are trying to replace all the placeholders related to service
				 */
				final String prodName = serviceEntity.getProductName();
				final String serNo = serviceEntity.getServiceSerialNo()+"";
				final String serDate = dateFormat.format(serviceEntity.getServiceDate());
				String custName = serviceEntity.getPersonInfo().getFullName();
				String companyName = company.getBusinessUnitName();
				
				String replacedMsg = strmsgBody.replace("{CustomerName}", custName);
				replacedMsg = replacedMsg.replace("{ProductName}", prodName);
				replacedMsg = replacedMsg.replace("{ServiceNo}", serNo);
				replacedMsg =  replacedMsg.replace("{ServiceDate}", serDate);
				replacedMsg = replacedMsg.replace("{companyName}", companyName);
				replacedMsg = replacedMsg.replace("{CompanyName}", companyName);
				
				replacedMsg=replacedMsg.replace("{ServiceNumber}", serviceEntity.getCount()+"");
				replacedMsg=replacedMsg.replace("{DD-MM-YY}", sdf.format(serviceEntity.getServiceDate())+"");
				replacedMsg=replacedMsg.replace("{TechnicianName}", serviceEntity.getEmployee());
				replacedMsg=replacedMsg.replace("{Date}", sdf.format(serviceEntity.getServiceDate())+"");
				replacedMsg=replacedMsg.replace("{Time}", serviceEntity.getServiceTime()+"");
				replacedMsg=replacedMsg.replace("{companynumber}",company.getCellNumber1()+"");
				
				if(replacedMsg.contains("{ServiceCompletionDate}")) {
					if(serviceEntity!=null&&serviceEntity.getCompletedDate()!=null)
						replacedMsg=replacedMsg.replace("{ServiceCompletionDate}",sdf.format(serviceEntity.getCompletedDate()));	
					else
						replacedMsg=replacedMsg.replace("{ServiceCompletionDate}","");	
				}
		
				if(serviceEntity.getEmployee()!=null&&!serviceEntity.getEmployee().equals("")) {
				Employee emp = ofy().load().type(Employee.class).filter("companyId", company.getCompanyId()).filter("fullname", serviceEntity.getEmployee()).first().now();				
				replacedMsg=replacedMsg.replace("{Number}", emp.getContacts().get(0).getCellNo1()+"");
//				
				}
				
				if(replacedMsg.contains("{DocumentId}")){
					replacedMsg =  replacedMsg.replace("{DocumentId}", serviceEntity.getCount()+"");
				}
				if(replacedMsg.contains("{PDFLink}")){
					String pdflink = commonserviceimpl.getPDFURL("Service", serviceEntity, serviceEntity.getCount(), serviceEntity.getCompanyId(), company, commonserviceimpl.getCompleteURL(company),null);
					logger.log(Level.SEVERE, "Service record pdflink " + pdflink);
					String tinyurl = ServerAppUtility.getTinyUrl(pdflink,company.getCompanyId());
					replacedMsg =  replacedMsg.replace("{PDFLink}", tinyurl);
				}
				replacedMsg=replacedMsg.replace("{CompanySignature}",ServerAppUtility.getCompanySignature(serviceEntity.getBranch(), serviceEntity.getCompanyId()));		
				
				strmsgBody=replacedMsg;
				
		}
		 //Ashwini Patil Date:26-04-2024 Pepcopp reported that companySignature is not getting replaced when email send from following screens
		 if(emailDetails.getEntityName().trim().equals("Invoice")){
			 Invoice invoice = (Invoice) emailDetails.getModel();	
			 strmsgBody=strmsgBody.replace("{CompanySignature}",ServerAppUtility.getCompanySignature(invoice.getBranch(), invoice.getCompanyId()));		
		 }
		 if(emailDetails.getEntityName().trim().equals("VendorInvoice") ){
			 VendorInvoice invoice = (VendorInvoice) emailDetails.getModel();
			strmsgBody=strmsgBody.replace("{CompanySignature}",ServerAppUtility.getCompanySignature(invoice.getBranch(), invoice.getCompanyId()));		
		}
		if(emailDetails.getEntityName().trim().equals("CustomerPayment")){
			CustomerPayment payment = (CustomerPayment) emailDetails.getModel();
			strmsgBody=strmsgBody.replace("{CompanySignature}",ServerAppUtility.getCompanySignature(payment.getBranch(), payment.getCompanyId()));		
		}
		if(emailDetails.getEntityName().trim().equals(AppConstants.CONTRACT_RENEWAL)){
			ContractRenewal renewal = (ContractRenewal) emailDetails.getModel();
			strmsgBody=strmsgBody.replace("{CompanySignature}",ServerAppUtility.getCompanySignature(renewal.getBranch(), renewal.getCompanyId()));		
		} 
		if(emailDetails.getEntityName().trim().equals("Contract")){
			 Contract contract = (Contract) emailDetails.getModel();	
			 strmsgBody=strmsgBody.replace("{CompanySignature}",ServerAppUtility.getCompanySignature(contract.getBranch(), contract.getCompanyId()));		
		}
			
		StringBuilder builder = new StringBuilder();
		
		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");
		
		builder.append("<br>");
//		builder.append("<br>");
		
		builder.append(strmsgBody);
		builder.append("<br>");

		builder.append("<br>");
		/**
		 * @author Vijay Date :- 08-06-2021
		 * Des :- As per Nitin Sir Signature part user will add into email body part so below signature part commented
		 */
//		builder.append("<b>" + "Thanks & Regards" + "</b><br>");
//		// Company Details
//		builder.append("<br>");
//		builder.append("<b>" + company.getBusinessUnitName() + "</b><br>");
//		builder.append("<table>");
//
//		builder.append("<tr>");
//		builder.append("<td>");
//		builder.append("Address : ");
//		builder.append("</td>");
//		builder.append("<td>");
//		if (!company.getAddress().getAddrLine2().equals("")
//				&& company.getAddress().getAddrLine2() != null)
//			builder.append(company.getAddress().getAddrLine1() + " "
//					+ company.getAddress().getAddrLine2() + " "
//					+ company.getAddress().getLocality());
//		else
//			builder.append(company.getAddress().getAddrLine1() + " "
//					+ company.getAddress().getLocality());
//		builder.append("</td>");
//		builder.append("</tr>");
//
//		builder.append("<tr>");
//		builder.append("<td>");
//		builder.append("</td>");
//		builder.append("<td>");
//		if (!company.getAddress().getLandmark().equals("")
//				&& company.getAddress().getLandmark() != null)
//			builder.append(company.getAddress().getLandmark() + ","
//					+ company.getAddress().getCity() + ","
//					+ company.getAddress().getPin()
//					+ company.getAddress().getState() + ","
//					+ company.getAddress().getCountry());
//		else
//			builder.append(company.getAddress().getCity() + ","
//					+ company.getAddress().getPin() + ","
//					+ company.getAddress().getState() + ","
//					+ company.getAddress().getCountry());
//		builder.append("</td>");
//		builder.append("</tr>");
//		builder.append("<tr>");
//		builder.append("<td>");
//		builder.append("Website : ");
//		builder.append("</td>");
//		builder.append("<td>");
//		builder.append(company.getWebsite());
//		builder.append("</td>");
//		builder.append("</tr>");
//
//		builder.append("<tr>");
//		builder.append("<td>");
//		builder.append("Phone No : ");
//		builder.append("</td>");
//		builder.append("<td>");
//		builder.append(company.getCellNumber1());
//		builder.append("</td>");
//		builder.append("<tr>");
//
//		builder.append("</tr>");
//		builder.append("</table>");
		/**
		 * ends here
		 */
		
		builder.append("</body>");
		builder.append("</html>");
				
		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);
		
		logger.log(Level.SEVERE, "contentInHtml"+contentInHtml);
		
		logger.log(Level.SEVERE, "salesPersonEmailIdFlag"+salesPersonEmailIdFlag);

		/**
		 * email popup uploaded document attachment
		 */
		String emailPoupUpload=null;
		if(emailDetails.getUploadedDocument()!=null){
			uploadedDocument = createAttachmentInStream(emailDetails.getUploadedDocument());
			emailPoupUpload = emailDetails.getUploadedDocument().getName();
		}
	
		/***
		 * Below code for managing PTSPL Requirement Sales person wise Email fuctionality
		 */
		if(salesPersonEmailIdFlag && (emailDetails.getEntityName().trim().equals("Lead") || emailDetails.getEntityName().trim().equals("Quotation") || emailDetails.getEntityName().trim().equals("Contract")
				 || emailDetails.getEntityName().trim().equals("SalesQuotation") ||  emailDetails.getEntityName().trim().equals("SalesOrder") || emailDetails.getEntityName().trim().equals("Complain")
				|| emailDetails.getEntityName().trim().equals("PurchaseOrder")) ){
			logger.log(Level.SEVERE, "Sales person wise EMail for PTSPL "+salesPersonEmailIdFlag);

			if(emailDetails.getEntityName().trim().equals("Lead")){
				logger.log(Level.SEVERE, "Sales person wise EMail for PTSPL for Lead ");
				try {
					if(emailDetails.getEntityName().trim().equals("Lead")){
						SendLeadEmail(emailDetails,contentInHtml,company);
					}
					return "Email sent Successfully";
					
					
				} catch (Exception e) {
					if(taskqueueCallFlag) {
						emailfailedBody = emailfailedBody+" "+e.getMessage();
						serverutility.sendEmail(companyId, emailfailedSubject, emailfailedBody);
					}
					else {
						return "Email not sent please contact EVA Support!";
					}
				}
			}
			else{
				logger.log(Level.SEVERE, "Sales person wise EMail for PTSPL Other than Lead ");
				try {
					SendGridEmailServlet sdEmail=new SendGridEmailServlet();
//					sdEmail.sendMailWithSendGrid(emailDetails.getFromEmailid(), emailDetails.getToEmailId(), emailDetails.getCcEmailId(), emailDetails.getBccEmailId(), emailDetails.getSubject(), contentInHtml, "text/html",pdfstream,attachFiles,"application/pdf",null);
					sdEmail.sendMailWithSendGrid(emailDetails.getFromEmailid(), emailDetails.getToEmailId(), emailDetails.getCcEmailId(), emailDetails.getBccEmailId(), emailDetails.getSubject(), contentInHtml, "text/html",pdfstream,attachFiles,"application/pdf", uploadedDocument, emailPoupUpload, "application/pdf", uploadStream, uploadDocName, "application/pdf", null, null, null, null);
					return "Email sent Successfully";
				} catch (Exception e) {
					if(taskqueueCallFlag) {
						emailfailedBody = emailfailedBody+" "+e.getMessage();
						serverutility.sendEmail(companyId, emailfailedSubject, emailfailedBody);
					}
					else {
						return "Email not sent please contact EVA Support!";
					}
				}
			}
			
		}
		
		/**
		 * ends here
		 */
		logger.log(Level.SEVERE, "After Salesperson wise email Email ");

		
		
		
		logger.log(Level.SEVERE, "Before Sendgrid API wise Email ");

		 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, companyId)){
				logger.log(Level.SEVERE, "Inside Sendgird API wise Email ");

			 try {
					SendGridEmailServlet sdEmail=new SendGridEmailServlet();
					sdEmail.sendMailWithSendGrid(emailDetails.getFromEmailid(), emailDetails.getToEmailId(), emailDetails.getCcEmailId(), emailDetails.getBccEmailId(), emailDetails.getSubject(), contentInHtml, "text/html",pdfstream,attachFiles,"application/pdf", uploadedDocument, emailPoupUpload, "application/pdf", uploadStream, uploadDocName, "application/pdf", null, null, null, null);
					return "Email sent Successfully";
					
				} catch (Exception e) {
					if(taskqueueCallFlag) {
						emailfailedBody = emailfailedBody+" "+e.getMessage();
						serverutility.sendEmail(companyId, emailfailedSubject, emailfailedBody);
					}
					else {
						return "Email not sent please contact EVA Support!";
					}
				}
			 
		 }

		
		
		
			logger.log(Level.SEVERE, "Before Calling Gmail Email API");

		try {
			System.out.println("Inside Try Block");
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("From Email: " +emailDetails.getFromEmailid());

			// To Mail Address
			message.setFrom(new InternetAddress(emailDetails.getFromEmailid()));
			message.setReplyTo(new InternetAddress[] { new InternetAddress(
					emailDetails.getFromEmailid()) });

			// Email to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);
		
			// Email CC Multiple Recipients
			if(emailDetails.getCcEmailId().size()>0){
				try {
					InternetAddress[] mailCCMultiple = new InternetAddress[ccEmailIdArray.length];
					for (int i = 0; i < ccEmailIdArray.length; i++) {
						mailCCMultiple[i] = new InternetAddress(ccEmailIdArray[i]);
						System.out.println("Email id multipart " + mailCCMultiple[i]);
					}
					message.setRecipients(Message.RecipientType.CC, mailCCMultiple);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
			// Email BCC Multiple Recipients
			if(emailDetails.getBccEmailId().size()>0){
				try {
					InternetAddress[] mailBCCMultiple = new InternetAddress[bccEmailIdArray.length];
					for (int i = 0; i < bccEmailIdArray.length; i++) {
						mailBCCMultiple[i] = new InternetAddress(bccEmailIdArray[i]);
						System.out.println("Email id multipart " + mailBCCMultiple[i]);
					}
					message.setRecipients(Message.RecipientType.BCC, mailBCCMultiple);
				} catch (Exception e) {
					e.printStackTrace();
				}
				

			}
		
			// Mail Subject
			message.setSubject(emailDetails.getSubject());

			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentInHtml, "text/html");
			multiPart.addBodyPart(htmlpart);

			
			if (emailPoupUpload != null) {
				System.out.println("Inside Attachment.........");

				MimeBodyPart attachbody = new MimeBodyPart();
				attachbody.setFileName(emailPoupUpload);
				DataSource src = new ByteArrayDataSource(
						uploadedDocument.toByteArray(), "application/pdf");
				attachbody.setDataHandler(new DataHandler(src));
				multiPart.addBodyPart(attachbody);
				
			}
			
			// Mail Uploaded Document attachment
			if (uploadDocName != null) {
				MimeBodyPart attachbodyDoc = new MimeBodyPart();
				logger.log(Level.SEVERE, "Inside Attachment upload  .........");
				attachbodyDoc.setFileName(uploadDocName);
				DataSource src = new ByteArrayDataSource(
						uploadStream.toByteArray(), "image/jpeg");
				attachbodyDoc.setDataHandler(new DataHandler(src));
				multiPart.addBodyPart(attachbodyDoc);
			}
			
			if(emailDetails.isPdfAttachment()){
				try {
					// Mail attachment
//					String attachFiles = "Document";
					if (attachFiles != null && pdfstream!=null) {
						MimeBodyPart attachbody = new MimeBodyPart();
						logger.log(Level.SEVERE, "Inside Attachment.........");
						attachbody.setFileName(attachFiles + ".pdf");
						DataSource src = new ByteArrayDataSource(
								pdfstream.toByteArray(), "application/pdf");
						attachbody.setDataHandler(new DataHandler(src));
						multiPart.addBodyPart(attachbody);
					}	
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
			try {
				message.setContent(multiPart);
				logger.log(Level.SEVERE, "Ready To Send Mail");
				Transport.send(message);
				logger.log(Level.SEVERE, " Email sent Successfully  ");
				System.out.println("Email sent Successfully !!!!");
			} catch (IllegalArgumentException ex) {
				if(taskqueueCallFlag) {
					emailfailedBody = emailfailedBody+" "+ex.getMessage();
					serverutility.sendEmail(companyId, emailfailedSubject, emailfailedBody);
				}
				else {
					return "Sender(From) Email id is does not register with App Id Please contact to EVA Support";
				}
			}

		} catch (Exception e) {
			logger.log(Level.SEVERE, " Email Fail ");
			e.printStackTrace();
			if(taskqueueCallFlag) {
				emailfailedBody = emailfailedBody+" "+e.getMessage();
				serverutility.sendEmail(companyId, emailfailedSubject, emailfailedBody);
			}
			else {
				return "Email not sent please contact to EVA Support!";
			}
		}
		
		} catch (Exception e) {
			e.printStackTrace();
			if(taskqueueCallFlag) {
				emailfailedBody = emailfailedBody+" "+e.getMessage();
				serverutility.sendEmail(companyId, emailfailedSubject, emailfailedBody);
			}
			else {
				return "Email not sent please contact to EVA Support!";
			}
		}
		
		return "Email sent Successfully";
		
	}
	
	
	private String getEmailds(ArrayList<String> toEmailId) {
		if(toEmailId!=null && toEmailId.size()>0) {
			StringBuilder strbuilder = new StringBuilder();
			for(int i=0;i<toEmailId.size();i++) {
				strbuilder.append(toEmailId.get(i));
				strbuilder.append(",");
			}
			return strbuilder.toString();
		}
		return "";
	}

	
	public String sendEmailToReceipient(EmailDetails emailDetails, long companyId) {
		
		StringBuilder builder = new StringBuilder();

		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");
		
		builder.append("<br>");

		builder.append(emailDetails.getEmailBody());
		builder.append("<br>");

		builder.append("<br>");
		builder.append("</body>");
		builder.append("</html>");
				
		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);
		
		logger.log(Level.SEVERE, "contentInHtml"+contentInHtml);

		 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, companyId)){
				SendGridEmailServlet sdEmail=new SendGridEmailServlet();
				sdEmail.sendMailWithSendGrid(emailDetails.getFromEmailid(), emailDetails.getToEmailId(), emailDetails.getCcEmailId(), emailDetails.getBccEmailId(), emailDetails.getSubject(), contentInHtml, "text/html",null,null,"application/pdf",null);
				return "";
		 }

		 
		 	// Too Email id's in array
			String[] toEmailIdArray = new String[emailDetails.getToEmailId().size()];
			emailDetails.getToEmailId().toArray(toEmailIdArray);

			// CC Email id's in array
			String[] ccEmailIdArray = new String[emailDetails.getCcEmailId().size()];
			emailDetails.getCcEmailId().toArray(ccEmailIdArray);
			
			// BCC Email id's in array
			String[] bccEmailIdArray = new String[emailDetails.getBccEmailId().size()];
			emailDetails.getBccEmailId().toArray(bccEmailIdArray);

			
		 try {
				System.out.println("Inside Try Block");
				Properties props = new Properties();
				Session session = Session.getDefaultInstance(props, null);
				Message message = new MimeMessage(session);
				Multipart multiPart = new MimeMultipart();

				System.out.println("From Email: " +emailDetails.getFromEmailid());

				// To Mail Address
				message.setFrom(new InternetAddress(emailDetails.getFromEmailid()));
				message.setReplyTo(new InternetAddress[] { new InternetAddress(
						emailDetails.getFromEmailid()) });

				// Email to Multiple Recipients
				InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
				for (int i = 0; i < toEmailIdArray.length; i++) {
					mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
					System.out.println("Email id multipart " + mailToMultiple[i]);
				}
				message.setRecipients(Message.RecipientType.TO, mailToMultiple);
			
				// Email CC Multiple Recipients
				if(emailDetails.getCcEmailId().size()>0){
					try {
						InternetAddress[] mailCCMultiple = new InternetAddress[ccEmailIdArray.length];
						for (int i = 0; i < ccEmailIdArray.length; i++) {
							mailCCMultiple[i] = new InternetAddress(ccEmailIdArray[i]);
							System.out.println("Email id multipart " + mailCCMultiple[i]);
						}
						message.setRecipients(Message.RecipientType.CC, mailCCMultiple);
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
				
				// Email BCC Multiple Recipients
				if(emailDetails.getBccEmailId().size()>0){
					try {
						InternetAddress[] mailBCCMultiple = new InternetAddress[bccEmailIdArray.length];
						for (int i = 0; i < bccEmailIdArray.length; i++) {
							mailBCCMultiple[i] = new InternetAddress(bccEmailIdArray[i]);
							System.out.println("Email id multipart " + mailBCCMultiple[i]);
						}
						message.setRecipients(Message.RecipientType.BCC, mailBCCMultiple);
					} catch (Exception e) {
						e.printStackTrace();
					}
					

				}
			
				// Mail Subject
				message.setSubject(emailDetails.getSubject());

				// Mail Body Content In html Form
				MimeBodyPart htmlpart = new MimeBodyPart();
				htmlpart.setContent(contentInHtml, "text/html");
				multiPart.addBodyPart(htmlpart);

				
				
				try {
					message.setContent(multiPart);
					logger.log(Level.SEVERE, "Ready To Send Mail");
					Transport.send(message);
					logger.log(Level.SEVERE, " Email sent Successfully  ");
					System.out.println("Email sent Successfully !!!!");
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				}

			} catch (Exception e) {
				logger.log(Level.SEVERE, " Email Fail ");
				e.printStackTrace();
			}
			 
		return "";
	}
	
	public void loadData() {

		logger.log(Level.SEVERE, "Inside initializeContractRenewalNew Server ");
		if (con.getCompanyId() != null) {
			comp = ofy().load().type(Company.class)
					.filter("companyId", con.getCompanyId()).first()
					.now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}

		if (con.getCompanyId() != null) {
			c = ofy().load().type(Customer.class)
					.filter("count", con.getCustomerId())
					.filter("companyId", con.getCompanyId()).first()
					.now();
		} else {
			c = ofy().load().type(Customer.class)
					.filter("count", con.getCustomerId()).first().now();
		}
		oldCon=ofy().load().type(Contract.class).filter("companyId", con.getCompanyId()).filter("count", con.getRefContractCount()).first().now();
		
		if (con.getCompanyId() != null) {
			con = ofy().load().type(Contract.class)
					.filter("count", con.getCount())
					.filter("companyId", con.getCompanyId()).first()
					.now();
		} else {
			con = ofy().load().type(Contract.class)
					.filter("count", con.getCount()).first().now();
		}

	}
   /**@author Sheetal : 09-04-2022
    * Des : Email will be sent to user if password is resetted***/
	@Override
	public String sendEmailOnPasswordReset(User userEntity,long companyId) {
		
		EmailDetails email = new EmailDetails();
		EmailTemplate emailtemplate = new EmailTemplate();
		String fromEmail = "";
		String ToEmailAddress="";
		comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", companyId)){
			
			String branchName = userEntity.getBranch();
			Branch branch = ofy().load().type(Branch.class).filter("companyId", companyId).filter("buisnessUnitName",branchName).first().now();
			if(branch.getEmail()!=null&&!branch.getEmail().equals("")){
				fromEmail=branch.getEmail();
			}else{
				fromEmail=comp.getEmail();
			}
		}
		else{
			fromEmail=comp.getEmail();
		}
		logger.log(Level.SEVERE, "from mail== "+fromEmail);
		email.setFromEmailid(fromEmail);
		

		String employeeName=userEntity.getEmployeeName();
		User user=ofy().load().type(User.class).filter("companyId", companyId).filter("employeeName", employeeName).first().now();
		if(user!=null){
			if(userEntity.getEmail()!=null && !userEntity.getEmail().equals("")){
				ToEmailAddress = userEntity.getEmail();
			}
		}
		ArrayList<String> toemailId = new ArrayList<String>();
		toemailId.add(ToEmailAddress);
		email.setToEmailId(toemailId);
		logger.log(Level.SEVERE, "to email == "+ToEmailAddress);


		emailtemplate=ofy().load().type(EmailTemplate.class).filter("companyId", comp.getCompanyId()).filter("templateName", "Reset Password").filter("templateStatus",true).first().now();
        if(emailtemplate!=null){
        	logger.log(Level.SEVERE, "sheetal Email template == "+emailtemplate);
        	String emailsub = emailtemplate.getSubject();
        	logger.log(Level.SEVERE, "userid == "+userEntity.getUserName());
             emailsub = emailsub.replace("<user id>", userEntity.getUserName());
             email.setSubject(emailsub);
             
             String emailbody = emailtemplate.getEmailBody();
             emailbody = emailbody.replace("<user id>", userEntity.getUserName());
             
             StringBuilder builder = new StringBuilder();
     		
     		builder.append("<!DOCTYPE html>");
     		builder.append("<html lang=\"en\">");
     		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
     		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");
     		
     		builder.append("<br>");
     		
     		builder.append(emailbody);
     		builder.append("<br>");
     		builder.append("<br>");
     		
     		builder.append(ServerAppUtility.getCompanySignature(comp,null));
     		logger.log(Level.SEVERE, "company signature == "+ServerAppUtility.getCompanySignature(comp,null));
     		builder.append("</body>");
     		builder.append("</html>");
     				
     		String contentInHtml = builder.toString();
     		logger.log(Level.SEVERE, "contentInHtml=="+contentInHtml);
     		 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, companyId)){
 				logger.log(Level.SEVERE, "Inside Sendgird API wise Email ");

 			 try {
 					SendGridEmailServlet sdEmail=new SendGridEmailServlet();
 					sdEmail.sendMailWithSendGrid(email.getFromEmailid(), email.getToEmailId(), null, null, email.getSubject(), contentInHtml, "text/html",null,null,null, null, null, null, null, null, null, null, null, null, null);
 					return "Email sent Successfully";
 					

 				} catch (Exception e) {
 					
 				}
 			 
 		 }
     		 else{
     			logger.log(Level.SEVERE, "Inside without sendgrid API Email ");
     			 // Too Email id's in array
     			String[] toEmailIdArray = new String[email.getToEmailId().size()];
     			email.getToEmailId().toArray(toEmailIdArray);

    			// CC Email id's in array
    			String[] ccEmailIdArray = new String[email.getCcEmailId().size()];
    			email.getCcEmailId().toArray(ccEmailIdArray);
    			
    			// BCC Email id's in array
    			String[] bccEmailIdArray = new String[email.getBccEmailId().size()];
    			email.getBccEmailId().toArray(bccEmailIdArray);
    			try {
 				System.out.println("Inside Try Block");
 				Properties props = new Properties();
 				Session session = Session.getDefaultInstance(props, null);
 				Message message = new MimeMessage(session);
 				Multipart multiPart = new MimeMultipart();
 				
 				logger.log(Level.SEVERE, "from mail without sendgrid== "+email.getFromEmailid());
 				System.out.println("From Email: " +email.getFromEmailid());

 				// To Mail Address
 				message.setFrom(new InternetAddress(email.getFromEmailid()));
 				message.setReplyTo(new InternetAddress[] { new InternetAddress(
 						email.getFromEmailid()) });

 				// Email to Multiple Recipients
 				InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
 				for (int i = 0; i < toEmailIdArray.length; i++) {
 					mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
 					System.out.println("Email id multipart " + mailToMultiple[i]);
 				}
 				message.setRecipients(Message.RecipientType.TO, mailToMultiple);
 			
 				// Email CC Multiple Recipients
 				if(email.getCcEmailId().size()>0){
 					try {
 						InternetAddress[] mailCCMultiple = new InternetAddress[ccEmailIdArray.length];
 						for (int i = 0; i < ccEmailIdArray.length; i++) {
 							mailCCMultiple[i] = new InternetAddress(ccEmailIdArray[i]);
 							System.out.println("Email id multipart " + mailCCMultiple[i]);
 						}
 						message.setRecipients(Message.RecipientType.CC, mailCCMultiple);
 						
 					} catch (Exception e) {
 						e.printStackTrace();
 					}
 					
 				}
 				
 				// Email BCC Multiple Recipients
 				if(email.getBccEmailId().size()>0){
 					try {
 						InternetAddress[] mailBCCMultiple = new InternetAddress[bccEmailIdArray.length];
 						for (int i = 0; i < bccEmailIdArray.length; i++) {
 							mailBCCMultiple[i] = new InternetAddress(bccEmailIdArray[i]);
 							System.out.println("Email id multipart " + mailBCCMultiple[i]);
 						}
 						message.setRecipients(Message.RecipientType.BCC, mailBCCMultiple);
 					} catch (Exception e) {
 						e.printStackTrace();
 					}
 					

 				}
 			
 				// Mail Subject
 				message.setSubject(email.getSubject());

 				// Mail Body Content In html Form
 				MimeBodyPart htmlpart = new MimeBodyPart();
 				htmlpart.setContent(contentInHtml, "text/html");
 				multiPart.addBodyPart(htmlpart);

 				
 				
 				try {
 					message.setContent(multiPart);
 					logger.log(Level.SEVERE, "Ready To Send Mail");
 					Transport.send(message);
 					logger.log(Level.SEVERE, " Email sent Successfully  ");
 					System.out.println("Email sent Successfully !!!!");
 				} catch (IllegalArgumentException e) {
 					e.printStackTrace();
 				}

    			} catch (Exception e) {
    				logger.log(Level.SEVERE, " Email Fail ");
    				e.printStackTrace();
    			}
     		 }
          }
        else {
        //without email template
        logger.log(Level.SEVERE, "without email template");
        String EmailSubject="Password has been reset for user " + userEntity.getEmployeeName();
        email.setSubject(EmailSubject);
        
        String EmailBody="Dear Sir/Madam" + "," +"<br>" + 
        		"User " + userEntity.getEmployeeName() + " has reset the password. Kindly Keep your password secret & do not share with any one" + "<br><br>";
        StringBuilder builder = new StringBuilder();
 		
 		builder.append("<!DOCTYPE html>");
 		builder.append("<html lang=\"en\">");
 		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
 		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");
 		
 		builder.append("<br>");
 		
 		builder.append(EmailBody);
 		builder.append("<br>");
 		builder.append("<br>");
 		
 		builder.append(ServerAppUtility.getCompanySignature(comp,null));
 		
 		builder.append("</body>");
 		builder.append("</html>");
 				
 		String contentInHtml = builder.toString();
 		
 		 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, companyId)){
				logger.log(Level.SEVERE, "Inside Sendgird API wise Email ");

			 try {
					SendGridEmailServlet sdEmail=new SendGridEmailServlet();
					sdEmail.sendMailWithSendGrid(email.getFromEmailid(), email.getToEmailId(), null, null, email.getSubject(), contentInHtml, "text/html",null,null,null, null, null, null, null, null, null, null, null, null, null);
					return "Email sent Successfully";
					

				} catch (Exception e) {
					
				}
			 
		 }
 		 else {
 			logger.log(Level.SEVERE, "Inside without sendgrid API Email ,without email template");

 			 // Too Email id's in array
 			String[] toEmailIdArray = new String[email.getToEmailId().size()];
 			email.getToEmailId().toArray(toEmailIdArray);

			// CC Email id's in array
			String[] ccEmailIdArray = new String[email.getCcEmailId().size()];
			email.getCcEmailId().toArray(ccEmailIdArray);
			
			// BCC Email id's in array
			String[] bccEmailIdArray = new String[email.getBccEmailId().size()];
			email.getBccEmailId().toArray(bccEmailIdArray);
			try {
				System.out.println("Inside Try Block");
				Properties props = new Properties();
				Session session = Session.getDefaultInstance(props, null);
				Message message = new MimeMessage(session);
				Multipart multiPart = new MimeMultipart();
				
				logger.log(Level.SEVERE, "from mail without sendgrid== "+email.getFromEmailid());
				System.out.println("From Email: " +email.getFromEmailid());

				// To Mail Address
				message.setFrom(new InternetAddress(email.getFromEmailid()));
				message.setReplyTo(new InternetAddress[] { new InternetAddress(
						email.getFromEmailid()) });

				// Email to Multiple Recipients
				InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
				for (int i = 0; i < toEmailIdArray.length; i++) {
					mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
					System.out.println("Email id multipart " + mailToMultiple[i]);
				}
				message.setRecipients(Message.RecipientType.TO, mailToMultiple);
			
				// Email CC Multiple Recipients
				if(email.getCcEmailId().size()>0){
					try {
						InternetAddress[] mailCCMultiple = new InternetAddress[ccEmailIdArray.length];
						for (int i = 0; i < ccEmailIdArray.length; i++) {
							mailCCMultiple[i] = new InternetAddress(ccEmailIdArray[i]);
							System.out.println("Email id multipart " + mailCCMultiple[i]);
						}
						message.setRecipients(Message.RecipientType.CC, mailCCMultiple);
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
				
				// Email BCC Multiple Recipients
				if(email.getBccEmailId().size()>0){
					try {
						InternetAddress[] mailBCCMultiple = new InternetAddress[bccEmailIdArray.length];
						for (int i = 0; i < bccEmailIdArray.length; i++) {
							mailBCCMultiple[i] = new InternetAddress(bccEmailIdArray[i]);
							System.out.println("Email id multipart " + mailBCCMultiple[i]);
						}
						message.setRecipients(Message.RecipientType.BCC, mailBCCMultiple);
					} catch (Exception e) {
						e.printStackTrace();
					}
					

				}
			
				// Mail Subject
				message.setSubject(email.getSubject());

				// Mail Body Content In html Form
				MimeBodyPart htmlpart = new MimeBodyPart();
				htmlpart.setContent(contentInHtml, "text/html");
				multiPart.addBodyPart(htmlpart);

				
				
				try {
					message.setContent(multiPart);
					logger.log(Level.SEVERE, "Ready To Send Mail");
					Transport.send(message);
					logger.log(Level.SEVERE, " Email sent Successfully  ");
					System.out.println("Email sent Successfully !!!!");
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				}

			} catch (Exception e) {
				logger.log(Level.SEVERE, " Email Fail ");
				e.printStackTrace();
			}
 		 
 		 }
        }
		return null;
	}
   /**Added by Sheetal : 13-04-2022
    * Des : In case IP address is different from previous or stored IP address of that user, sending email to company**/
	@Override
	public void sendEmailForIPAuthorisation(String userID, String ipaddress,long companyId)
			throws IllegalArgumentException {
		
		comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		 String CompanyEmail=comp.getEmail();
		
		EmailTemplate emailtemplate = new EmailTemplate();
		emailtemplate=ofy().load().type(EmailTemplate.class).filter("companyId", companyId).filter("templateName", "User Login IP Address Change").filter("templateStatus",true).first().now();
		logger.log(Level.SEVERE, "IP Email template == "+emailtemplate);
		  if(emailtemplate!=null){
	        	
	        	String emailsub = emailtemplate.getSubject();
	        	logger.log(Level.SEVERE, "userid == "+userID);
	             emailsub = emailsub.replace("<user id>", userID);
	             String EmailSubject = emailsub.replace("<IP Address>", ipaddress);
	             
	             String emailbody = emailtemplate.getEmailBody();
	             emailbody = emailbody.replace("<user id>", userID);
	             String EmailBody = emailbody.replace("<IP Address>", ipaddress);
	             
	             StringBuilder builder = new StringBuilder();
	      		
	      		builder.append("<!DOCTYPE html>");
	      		builder.append("<html lang=\"en\">");
	      		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
	      		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");
	      		
	      		builder.append("<br>");
	      		
	      		builder.append(EmailBody);
	      		builder.append("<br>");
	      		builder.append("<br>");
	      		
	      		builder.append(ServerAppUtility.getCompanySignature(comp,null));
	      		builder.append("</body>");
	      		builder.append("</html>");
	      				
	      		String contentInHtml = builder.toString();
	             try {
	             System.out.println("Inside try calling Method...");
				logger.log(Level.SEVERE, "Ready To Call Common Method");

				EmailForAuthorizationOfIPAddress(EmailSubject, contentInHtml, CompanyEmail,comp.getCompanyId());
	             }catch(Exception e){
	            	 
	             }
		  }
		  logger.log(Level.SEVERE, "Without email template");
		  String EmailSubject="Alert - User " + userID + " has logged in from different IP address " + ipaddress;
		  
		  String EmailBody="Dear Sir/Madam" + "," + "<br>" +
				     "User " + userID + " has logged in from different IP address " + ipaddress + "." + 
				  " In case you, this is unauthorized logged location or unauthorized access, kindly de-activate the user id immediately." + "<br>" +
				  "To deactivate user id go to settings → user → deactivate " + userID ;
		  
		  StringBuilder builder = new StringBuilder();
    		
    		builder.append("<!DOCTYPE html>");
    		builder.append("<html lang=\"en\">");
    		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
    		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");
    		
    		builder.append("<br>");
    		
    		builder.append(EmailBody);
    		builder.append("<br>");
    		builder.append("<br>");
    		
    		builder.append(ServerAppUtility.getCompanySignature(comp,null));
    		builder.append("</body>");
    		builder.append("</html>");
    		String contentInHtml = builder.toString();
    				
    		  try {
 	             System.out.println("Inside try calling Method...");
 				logger.log(Level.SEVERE, "Ready To Call Common Method");

 				EmailForAuthorizationOfIPAddress(EmailSubject, contentInHtml, CompanyEmail,comp.getCompanyId());
 	             }catch(Exception e){
 	            	 
 	             }
		
	}

	private void EmailForAuthorizationOfIPAddress(String mailSub,
			String message1, String companyEmailId, Long companyId) {
		
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, companyId)){
			logger.log(Level.SEVERE, "Inside Sendgird API wise Email ");

		 try {
				SendGridEmailServlet sdEmail=new SendGridEmailServlet();
				ArrayList<String> toemaillist = new ArrayList<String>();
				toemaillist.add(companyEmailId);
				sdEmail.sendMailWithSendGrid(companyEmailId, toemaillist, null, null, mailSub, message1, "text/html",null,null,null, null, null, null, null, null, null, null, null, null, null);

			} catch (Exception e) {
				
			}
		 return;
		}
		
		try {
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();
			
			logger.log(Level.SEVERE, "Company Email: " + companyEmailId);

			// To Mail Address
			message.setFrom(new InternetAddress(companyEmailId));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					companyEmailId));

			// Mail Subject
			message.setSubject(mailSub);
			message.setText(message1);
			
		    // Mail Body Content In html Form
			MimeBodyPart htmlpart=new MimeBodyPart();
			htmlpart.setContent(message1, "text/html");
			multiPart.addBodyPart(htmlpart);
			
			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			System.out.println("Email sent Successfully !!!!");
		} catch (Exception e) {
			e.printStackTrace();
		}

	
		
	}
   /**@author Sheetal : 16-04-2022
    * Des : Email will be sent to company if user has entered 5 incorrect passwords for login**/
	@Override
	public void sendEmailOnIncorrectPassword(LoggedIn loggedIn, String userID, long companyId) {
		logger.log(Level.SEVERE, "sendEmailOnIncorrectPassword");
		if(loggedIn!=null){
			ofy().save().entity(loggedIn);
		}
		String ToEmail="";
		comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", companyId)){
	
			Branch branch = ofy().load().type(Branch.class).filter("companyId", companyId).first().now();
			if(branch.getEmail()!=null&&!branch.getEmail().equals("")){
				ToEmail=branch.getEmail();
			}else{
				ToEmail=comp.getEmail();
			}
		}
		else{
			ToEmail=comp.getEmail();
		}
		
		 String EmailSubject="User " + userID + "  is locked due to 5 incorrect attempts" ;
		 
		 String EmailBody="Dear Sir/Madam" + "," + "<br>" +
			     "User " + userID + " is locked due to 5 incorrect attempts." + 
			  " Kindly reset the password of the user by going to settings → User & change password. Kindly inform that to user.";
	  
	  StringBuilder builder = new StringBuilder();
		
		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");
		
		builder.append("<br>");
		
		builder.append(EmailBody);
		builder.append("<br>");
		builder.append("<br>");
		
		builder.append(ServerAppUtility.getCompanySignature(comp,null));
		builder.append("</body>");
		builder.append("</html>");
		String contentInHtml = builder.toString();
		
		try {
             System.out.println("Inside try calling Method...");
			logger.log(Level.SEVERE, "Ready To Call Common Method");

			EmailForAuthorizationOfIPAddress(EmailSubject, contentInHtml, ToEmail,comp.getCompanyId());
             }catch(Exception e){
            	 
             }
	}
	
	/**
	 * @author Ashwini Patil
	 * @since 27-04-2022
	 * to send payment due email using email template through cron job
	 */

	public void PaymentReminderCronJobTemplateEmailToClient(ArrayList<String> toEmailId,
			String mailSubject, String mailTitle, Company company, String msg,String attachFiles)
			throws IOException {

		logger.log(Level.SEVERE, "Entered In Common Method");

		// to Email id's in array
		String[] toEmailIdArray = new String[toEmailId.size()];
		toEmailId.toArray(toEmailIdArray);
		
		logger.log(Level.SEVERE, "Mail Sub : " + mailSubject);
		logger.log(Level.SEVERE, "Mail Title : " + mailTitle);
		
		StringBuilder builder = new StringBuilder();

		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

//		builder.append("<h3 class=\"ex\" align=\"center\">" + mailTitle
//				+ "</h3></br></br>");



		builder.append(" <p>  </p> </br></br>");
		builder.append(" <B>   </br></br></B>");
		builder.append(msg);
		builder.append(" <B>   </br></br></B>");
		builder.append("<br>");
		builder.append("</br>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);

		/************************************* Mail Send Logic *********************************/

		ArrayList<String> ccEmailList = new ArrayList<String>();
		ccEmailList.add(company.getEmail());

		String[] ccEmailIdArray = new String[ccEmailList.size()];
		ccEmailList.toArray(ccEmailIdArray);

		try {
			System.out.println("Inside Try Block");
			Properties props = new Properties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", "smtp.gmail.com");
			Session session = Session.getDefaultInstance(props);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company.getEmail());
			logger.log(Level.SEVERE, "Company Email" + company.getEmail());

			// To Mail Address
			message.setFrom(new InternetAddress(company.getEmail()));
			message.setReplyTo(new InternetAddress[] { new InternetAddress(
					company.getEmail()) });

			// Email to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);
				logger.log(Level.SEVERE, "Email id multipart "
						+ mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);
			// Email CC to Multiple Recipients
			InternetAddress[] ccmailToMultiple = new InternetAddress[ccEmailIdArray.length];
			for (int i = 0; i < ccEmailIdArray.length; i++) {
				ccmailToMultiple[i] = new InternetAddress(ccEmailIdArray[i]);
				System.out.println("Email id multipart " + ccmailToMultiple[i]);
				logger.log(Level.SEVERE, "CC Email id multipart "
						+ ccmailToMultiple[i]);
			}

			// message.setRecipient(Message.RecipientType.CC, new
			// InternetAddress("sales@evasoftwaresolutions.com"));
			message.setRecipients(Message.RecipientType.CC, ccmailToMultiple);

			// Mail Subject
			message.setSubject(mailSubject);

			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentInHtml, "text/html");
			multiPart.addBodyPart(htmlpart);

			// Mail attachment
			if (attachFiles != null) {
				MimeBodyPart attachbody = new MimeBodyPart();
				System.out.println("Inside Attachment.........");
				logger.log(Level.SEVERE, "Inside Attachment.........");
				attachbody.setFileName(attachFiles + ".pdf");
				DataSource src = new ByteArrayDataSource(
						pdfstream.toByteArray(), "application/pdf");
				attachbody.setDataHandler(new DataHandler(src));
				multiPart.addBodyPart(attachbody);

			}

			System.out.println("Ready To Send Mail");
			logger.log(Level.SEVERE, "Ready To Send Mail" + multiPart);
			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			logger.log(Level.SEVERE, "Email sent Successfully !!!!");
			System.out.println("Email sent Successfully !!!!");
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Exception while sending Email" + e);
		}

	}
	

	
	//Ashwini Patil Date:13-08-2022
	public void sendNewEmailFromEmailTemplate(ArrayList<String> toEmailId, String mailSubject,
			 Company company, String mailMsg, String[] attachFiles, String uploadedFile, ArrayList<String> ccemailidlist, String fromEmailId)
			throws IOException {

		logger.log(Level.SEVERE, "Entered In Common Method");

		// to Email id's in array
		String[] toEmailIdArray = new String[toEmailId.size()];
		toEmailId.toArray(toEmailIdArray);

		logger.log(Level.SEVERE, "Mail Sub : " + mailSubject);
		logger.log(Level.SEVERE, "Mail Body : " + mailMsg);
		

		StringBuilder builder = new StringBuilder();

		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
//		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
//		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");
		builder.append("<body");

		
		/************************************ Note *******************************************/
		if (mailMsg != null) {
			builder.append("</br></br><p>" + mailMsg + "</p></br></br>");
		}

	
//		 Company Details
		if(mailMsg.contains("{CompanyLogo}")){
			
			try{
				if(company.getCompanyURL()!=null&&!company.getCompanyURL().equals("")
						&&company.getLogo().getUrl()!=null&&!company.getLogo().getUrl().equals("")
						&&company.getLogo().getName()!=null&&!company.getLogo().getName().equals("")){
					String img2=company.getCompanyURL().toString().trim()+company.getLogo().getUrl()+"&filename="+company.getLogo().getName();
					logger.log(Level.SEVERE,"img2 msg --"+img2);
					builder.append("<br>");
					builder.append("</br>");
					builder.append("<img src=\""+img2+"\"");
					builder.append(" alt= \"SR Copy\"");
					builder.append(" width = \"200\"  height=\"200\" >");
				}
			}catch(Exception e){
				logger.log(Level.SEVERE,"IMG EXCEP --"+e.getMessage());
			}			
			
		}
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		logger.log(Level.SEVERE,"mail contentInHtml="+contentInHtml);
		
		/**
		 * @author Anil
		 * @since 19-06-2020
		 * using uploadTc and uploadCp stream for sending service summary report 1 and 2 for PTSPL
		 */
		
		String uploadTc="TermsAndConditions.pdf";
		String uploadCp="CompanyProfile.pdf";
		
		if(attachFiles != null&&attachFiles.length>1){
			if(attachFiles.length==3){
				uploadTc=attachFiles[1];
				uploadCp=attachFiles[2];
			}else{
				uploadTc=attachFiles[1];
			}
		}
		
		/**
		 * @author Anil , Date : 10-04-2020
		 * added display name for from email id
		 * ,company.getDisplayNameForCompanyEmailId()
		 */
		logger.log(Level.SEVERE," company email -" +company.getEmail());
		if(ccemailidlist==null) {
			ccemailidlist = new ArrayList<String>();
		}
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CronJob", "EnableSendGridEmailApi",company.getCompanyId())){
			logger.log(Level.SEVERE," company email --" +company.getEmail() +"  to email  " + toEmailIdArray.toString());
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.PC_ENABLESENDEMAILSFROMPERSONRESPONSIBLE, company.getCompanyId())) {
				 Company compEntity = ofy().load().type(Company.class)
							.filter("companyId", company.getCompanyId()).first().now();
					ObjectifyService.reset();
				    ofy().consistency(Consistency.STRONG);
					ofy().clear();
					if(ccemailidlist!=null && compEntity.getEmail()!=null && !compEntity.getEmail().equals("")) {
						logger.log(Level.SEVERE, "company.getEmail() == "+compEntity.getEmail());
						ccemailidlist.add(compEntity.getEmail());
					}
					logger.log(Level.SEVERE, "Email CC "+ccemailidlist);
					logger.log(Level.SEVERE, "Email To "+toEmailId);
					ArrayList<String> bcclist = new ArrayList<String>();
					bcclist.add(fromEmailId);
				SendGridEmailServlet sdEmail=new SendGridEmailServlet();
				sdEmail.sendMailWithSendGrid(fromEmailId, toEmailId, ccemailidlist, bcclist, mailSubject, contentInHtml, "text/html",pdfstream,attachFiles[0],"application/pdf",null);
				return;
			}

				
			/**
			 * Date 30-09-2019 By Vijay
			 * Des :- NBHC CCPM A creation send email cc to Technical Manager
			 */
			if(mailSubject.trim().equals("AssessmentReport")){
				ArrayList<String> cclist = new ArrayList<String>();
				List<Employee> employeeList = ofy().load().type(Employee.class).filter("companyId", company.getCompanyId())
						.filter("roleName", "Technical Manager").list();
				if(employeeList.size()!=0){
					for(Employee emp : employeeList){
						if(emp.getEmail()!=null)
						cclist.add(emp.getEmail());
					}
				}
				logger.log(Level.SEVERE, "Email CC "+cclist);
				
				SendGridEmailServlet sdEmail=new SendGridEmailServlet();
				sdEmail.sendMailWithSendGrid(company.getEmail(), toEmailId, cclist, null, mailSubject, contentInHtml, "text/html",pdfstream,attachFiles[0]+".pdf","application/pdf",company.getDisplayNameForCompanyEmailId());
			}
			else{
				SendGridEmailServlet sdEmail=new SendGridEmailServlet();
				/**
				 * @author Anil @since 19-10-2021
				 * service report were not sent through sendgrid email api
				 */
//				sdEmail.sendMailWithSendGrid(company.getEmail(), toEmailId, null, null, mailSubject, contentInHtml, "text/html",pdfstream,attachFiles[0],"application/pdf",company.getDisplayNameForCompanyEmailId());
				sdEmail.sendMailWithSendGrid(company.getEmail(), toEmailId, null, null, mailSubject, contentInHtml, "text/html",pdfstream,attachFiles[0],"application/pdf", uploadTC, uploadTc, "application/pdf", uploadCP, uploadCp, "application/pdf", null, null, null, company.getDisplayNameForCompanyEmailId());
			}
		
			return;
		}
		
		/**
		 * @author Vijay Chougule Date 28-12-2020
		 * Des :- Email From Sales Person and CC in Approver Name and Branch Email id and Company Email Id
		 * send Emails From SendGrid
		 * 
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.PC_ENABLESENDEMAILSFROMPERSONRESPONSIBLE, company.getCompanyId())) {
			 Company compEntity = ofy().load().type(Company.class)
						.filter("companyId", company.getCompanyId()).first().now();
				ObjectifyService.reset();
			    ofy().consistency(Consistency.STRONG);
				ofy().clear();
				
			 logger.log(Level.SEVERE, "company.getEmail() "+compEntity.getEmail());
				logger.log(Level.SEVERE, "company.getEmail() "+compEntity.getPocName());
				logger.log(Level.SEVERE, "company.getEmail() "+compEntity.getPocEmail());
				logger.log(Level.SEVERE, "company.getEmail() "+compEntity.getCompanyType());

				if(ccemailidlist!=null && company.getEmail()!=null && !company.getEmail().equals("")) {
					logger.log(Level.SEVERE, "company.getEmail() == "+company.getEmail());
					ccemailidlist.add(company.getEmail());
				}
				logger.log(Level.SEVERE, "Email CC "+ccemailidlist);
				logger.log(Level.SEVERE, "Email To "+toEmailId);
				ArrayList<String> bcclist = new ArrayList<String>();
				bcclist.add(fromEmailId);
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			sdEmail.sendMailWithSendGrid(fromEmailId, toEmailId, ccemailidlist, bcclist, mailSubject, contentInHtml, "text/html",pdfstream,attachFiles[0],"application/pdf",null);
			return;
		}
		/**
		 * ends here
		 */
		
		try {
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, company.getCompanyId())){
				logger.log(Level.SEVERE, "Inside Sendgird API wise Email ");

			 try {
					/**
					 * @author Vijay Date :- 09-08-2021
					 * Des :- To send email from sendgrid updated code for MRN MIN and MMN
					 */
					SendGridEmailServlet sdEmail=new SendGridEmailServlet();
					if(mailSubject.equalsIgnoreCase("MRN") || mailSubject.equalsIgnoreCase("MIN") || mailSubject.equalsIgnoreCase("MMN")) {
						if(uploadStream!=null) {
							sdEmail.sendMailWithSendGrid(fromEmailId, toEmailId, ccemailidlist, null, mailSubject, contentInHtml, "text/html",null,null,"application/pdf", null, null, "application/pdf", uploadStream, uploadedFile, "application/pdf", null, null, null, null);
						}
						else {
							sdEmail.sendMailWithSendGrid(fromEmailId, toEmailId, ccemailidlist, null, mailSubject, contentInHtml, "text/html",null,null,"application/pdf", null, null, "application/pdf", null, "", "application/pdf", null, null, null, null);
						}
					}
					else {
//						sdEmail.sendMailWithSendGrid(fromEmailId, toEmailId, ccemailidlist, null, mailSubject, contentInHtml, "text/html",pdfstream,attachFiles[0],"application/pdf", null, null, "application/pdf", uploadStream, "", "application/pdf", null, null, null, null);
						/**
						 * @author Anil @since 19-10-2021
						 * service report were not sent through sendgrid email api
						 */
						sdEmail.sendMailWithSendGrid(fromEmailId, toEmailId, ccemailidlist, null, mailSubject, contentInHtml, "text/html",pdfstream,attachFiles[0],"application/pdf", uploadTC, uploadTc, "application/pdf", uploadCP, uploadCp, "application/pdf", uploadStream, "", "application/pdf", company.getDisplayNameForCompanyEmailId());
					
					}		

				} catch (Exception e) {
					e.printStackTrace();
				}
			 
			  return;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		 

		
		/************************************* Mail Send Logic *********************************/
		try {
			System.out.println("Inside Try Block");
			Properties props = new Properties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", "smtp.gmail.com");
			Session session = Session.getDefaultInstance(props);
			// Session session=Session.getDefaultInstance(props,null);
			Message message = new MimeMessage(session);
			Multipart multiPart = new MimeMultipart();

			System.out.println("Company Email: " + company.getEmail());
			logger.log(Level.SEVERE, "Company Email" + company.getEmail());

			// To Mail Address
			message.setFrom(new InternetAddress(company.getEmail()));
			message.setReplyTo(new InternetAddress[] { new InternetAddress(
					company.getEmail()) });

			// Email to Multiple Recipients
			InternetAddress[] mailToMultiple = new InternetAddress[toEmailIdArray.length];
			for (int i = 0; i < toEmailIdArray.length; i++) {
				mailToMultiple[i] = new InternetAddress(toEmailIdArray[i]);
				System.out.println("Email id multipart " + mailToMultiple[i]);
				logger.log(Level.SEVERE, "Email id multipart "
						+ mailToMultiple[i]);
			}
			message.setRecipients(Message.RecipientType.TO, mailToMultiple);
			message.setRecipient(Message.RecipientType.CC, new InternetAddress(
					company.getEmail()));

			// Mail Subject
			message.setSubject(mailSubject);

			// Mail Body Content In html Form
			MimeBodyPart htmlpart = new MimeBodyPart();
			htmlpart.setContent(contentInHtml, "text/html");
			multiPart.addBodyPart(htmlpart);
			
			
			
			/**
			 * Date 2/11/2018
			 * Updated By Viraj
			 * Description Email Attachment of Terms And Conditions, Company Profile
			 */
			if(uploadTC != null) {
				MimeBodyPart attachbodyDoc = new MimeBodyPart();
				logger.log(Level.SEVERE, "Inside Attachment TC upload  .........");
				attachbodyDoc.setFileName(uploadTc);
				DataSource src = new ByteArrayDataSource(
						uploadTC.toByteArray(), "application/pdf");
				attachbodyDoc.setDataHandler(new DataHandler(src));
				multiPart.addBodyPart(attachbodyDoc);
			}
			
			if(uploadCP != null) {
				MimeBodyPart attachbodyDoc = new MimeBodyPart();
				logger.log(Level.SEVERE, "Inside Attachment TC upload  .........");
				attachbodyDoc.setFileName(uploadCp);
				DataSource src = new ByteArrayDataSource(
						uploadCP.toByteArray(), "application/pdf");
				attachbodyDoc.setDataHandler(new DataHandler(src));
				multiPart.addBodyPart(attachbodyDoc);
			}
			
			if(uploadCompanyProfile != null) {
				MimeBodyPart attachbodyDoc = new MimeBodyPart();
				logger.log(Level.SEVERE, "Inside Attachment TC upload  .........");
				attachbodyDoc.setFileName("CompanyProfile" +".pdf");
				DataSource src = new ByteArrayDataSource(
						uploadCompanyProfile.toByteArray(), "application/pdf");
				attachbodyDoc.setDataHandler(new DataHandler(src));
				multiPart.addBodyPart(attachbodyDoc);
			}
			
			
			
			
			
			/** Ends **/

			// Mail Uploaded Document attachment
			if (uploadedFile != null) {
				MimeBodyPart attachbodyDoc = new MimeBodyPart();
				logger.log(Level.SEVERE, "Inside Attachment upload  .........");
				attachbodyDoc.setFileName(uploadedFile);
				DataSource src = new ByteArrayDataSource(
						uploadStream.toByteArray(), "image/jpeg");
				attachbodyDoc.setDataHandler(new DataHandler(src));
				multiPart.addBodyPart(attachbodyDoc);
			}

			// Mail attachment
			if (attachFiles != null) {
				MimeBodyPart attachbody = new MimeBodyPart();
				System.out.println("Inside Attachment.........");
				logger.log(Level.SEVERE, "Inside Attachment.........");
				attachbody.setFileName(attachFiles[0] + ".pdf");
				DataSource src = new ByteArrayDataSource(
						pdfstream.toByteArray(), "application/pdf");
				attachbody.setDataHandler(new DataHandler(src));
				multiPart.addBodyPart(attachbody);
			}

			System.out.println("Ready To Send Mail");
			logger.log(Level.SEVERE, "Ready To Send Mail" + multiPart);
			message.setContent(multiPart);
			logger.log(Level.SEVERE, "Ready To Send Mail");
			Transport.send(message);
			logger.log(Level.SEVERE, "Email sent Successfully !!!!");
			System.out.println("Email sent Successfully !!!!");
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Exception while sending Email" + e);
		}

	}

	private void createAssessmentReportAttachmentDetails(AssesmentReport assessmentRepObj) {
		// TODO Auto-generated method stub
				
		logger.log(Level.SEVERE, "Inside createAssessmentReportAttachmentDetails Method");	
		if(assessmentRepObj!=null){
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("AssesmentReport","onlyForNBHC",assessmentRepObj.getCompanyId())){
				NBHCAssessmentReportPdf assessmentReportPdf = new NBHCAssessmentReportPdf();
				assessmentReportPdf.document = new Document();
				assessmentReportPdf.document = new Document(PageSize.A4.rotate());
				pdfstream = new ByteArrayOutputStream();
				try {
					PdfWriter.getInstance(assessmentReportPdf.document, pdfstream);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
				assessmentReportPdf.document.open();

				assessmentReportPdf.setAssessmentDetails((long) assessmentRepObj.getCount());
				assessmentReportPdf.createPdf("no");
				assessmentReportPdf.document.close();
			}else{
				SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
				fmt.setTimeZone(TimeZone.getTimeZone("IST"));
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));

				String filename=assessmentRepObj.getCinfo().getFullName()+" - "+assessmentRepObj.getCount()+" - "+fmt.format(assessmentRepObj.getAssessmentDate())+".pdf";
				AssessmentReportPdf assessmentReportPdf = new AssessmentReportPdf();
				assessmentReportPdf.document = new Document();
				pdfstream = new ByteArrayOutputStream();
				try {
					PdfWriter.getInstance(assessmentReportPdf.document, pdfstream);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
				assessmentReportPdf.document.open();				
				assessmentReportPdf.setAssessmentDetails((long) assessmentRepObj.getCount(),assessmentRepObj);
				assessmentReportPdf.createPdf("no");
				assessmentReportPdf.document.close();
			}
		}
	}
}
