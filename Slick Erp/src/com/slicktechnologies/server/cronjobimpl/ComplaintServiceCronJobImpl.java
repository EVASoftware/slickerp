package com.slicktechnologies.server.cronjobimpl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.googlecode.objectify.ObjectifyService;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class ComplaintServiceCronJobImpl extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -577595341314536255L;

	Logger logger = Logger.getLogger("NameOfYourLogger");
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private DateFormat fmt1 = new SimpleDateFormat("dd/MM/yyyy");
		
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
	}

public void servicecontactlist(List<CronJobConfigrationDetails> cronList,HashSet<String>  empRoleList) {
		
		logger.log(Level.SEVERE, "Cron list str --" + cronList);
			try {
				
				List<Company> compEntity = ofy().load().type(Company.class).list();
				 
				if(compEntity.size()>0)
				{
				
					logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
	
					for(int i=0;i<compEntity.size();i++){
										
						Company c=compEntity.get(i);
					/********************************Reading services from Service entity  ***********************/
						for(String empRoleName :empRoleList){
							/**
							 * @author Anil , Date : 25-04-2019
							 * while loading employee added filter ,its status should  be active 
							 * Because mails were triggered on in active employee ids also
							 */
							List<Employee>  employeeList=new ArrayList<Employee>();
							employeeList=ofy().load().type(Employee.class).filter("companyId", c.getCompanyId()).filter("roleName", empRoleName).filter("status", true).list();
							logger.log(Level.SEVERE,""+empRoleName+" "+"Employee List size::: "+employeeList.size());
							
								for (Employee employee : employeeList) {
									
									HashMap<String, Integer> empCount = new HashMap<String,Integer>();
									HashMap<String, Integer> branchCount = new HashMap<String, Integer>();
									
									int duetotal =0, overDue = 0, total=0;
									ArrayList<String> overHeader = new ArrayList<String>();
									ArrayList<String> dueHeader = new ArrayList<String>();
									ArrayList<String> overDetail = new ArrayList<String>();
									ArrayList<String> dueDetail = new ArrayList<String>();
									
									String header1 = "",header2 = "",title = "";
									
									
									 String todayDateString ="";
								 
									 String footer = "";
								 	
								 	/**
									 * @author Anil,Date : 13-02-2019
									 * Renaming mail subject as per rohan's instruction	
									 */
//									String emailSubject =  "Customer service Due As On Date"+" "+ todayDateString;
//									String mailTitl = "Customer service due As On Date";
									String emailSubject ="Customer services due and overdue as on date"+" "+ todayDateString;
									String mailTitl = "";
									
									String emailMailBody = mailTitl;
									
									
									ArrayList<String> toEmailListDt=new ArrayList<String>();
									toEmailListDt.add(employee.getEmail().trim());
									List<String> branchList=new ArrayList<String>();
									
									branchList.add(employee.getBranchName());
									for (int j = 0; j < employee.getEmpBranchList().size(); j++) {
										branchList.add(employee.getEmpBranchList().get(j).getBranchName());
										logger.log(Level.SEVERE, "BrnachName"+ employee.getEmpBranchList().get(j).getBranchName());
									}
									logger.log(Level.SEVERE,"toEmailList size::: "+toEmailListDt.size()+" Branch List SIze : "+branchList.size());
									
									if(branchList.size()!=0){
										for(int k=0;k<cronList.size();k++){
											
											if(cronList.get(k).getEmployeeRole().equalsIgnoreCase(empRoleName)){
												
												CronJobConfigrationDetails cronDetails =cronList.get(k);
												
												TimeZone.setDefault(TimeZone.getTimeZone("IST"));
												fmt.setTimeZone(TimeZone.getTimeZone("IST"));
												fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
													
											
													Date today=DateUtility.getDateWithTimeZone("IST", new Date());
													Calendar cal=Calendar.getInstance();
													cal.setTime(today);
													cal.add(Calendar.DATE, 0);
													
													Date dateForFilter=null;
													
													try {
														dateForFilter=fmt1.parse(fmt1.format(cal.getTime()));
														cal.set(Calendar.HOUR_OF_DAY,0);
														cal.set(Calendar.MINUTE,0);
														cal.set(Calendar.SECOND,0);
														cal.set(Calendar.MILLISECOND,0);
														dateForFilter=cal.getTime();
													} catch (ParseException e) { 
														e.printStackTrace();
													}
													
													cal.setTime(today);
													int diffDay = 0;
													 if(cronDetails.getOverdueDays()>0 && cronDetails.isOverdueStatus()){
														 diffDay = -cronDetails.getOverdueDays();
													 }else{
														 diffDay = cronDetails.getInterval();
													 }
													
													cal.add(Calendar.DATE, diffDay);
													
													Date duedateForFilter=null;
													
													try {
														duedateForFilter=fmt1.parse(fmt1.format(cal.getTime()));
														cal.set(Calendar.HOUR_OF_DAY,23);
														cal.set(Calendar.MINUTE,59);
														cal.set(Calendar.SECOND,59);
														cal.set(Calendar.MILLISECOND,999);
														duedateForFilter=cal.getTime();
													} catch (ParseException e) {
														e.printStackTrace();
													}
													
														
														logger.log(Level.SEVERE,"Date After setting time"+duedateForFilter);
														
														/*************************************End*********************************/
														
												 try{
													 
													 logger.log(Level.SEVERE,"In service List");	
									
													 
												/********************************Adding status in the list ***********************/
													 
													 ArrayList<String> obj = new ArrayList<String>();
													 obj.add("Scheduled");
													 obj.add("Pending");
													 obj.add("Rescheduled");
													 
												
												/******************************Converting todayDate to String format*****************/
													 
													 Date todaysDate = new Date();
													 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
													  todayDateString = df.format(todaysDate);
													 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);
									
													 
											   /********************************Adding Companies in the list ***********************/
													 	ObjectifyService.reset();
													 	ofy().consistency(Consistency.STRONG);
													 	ofy().clear();
													   
														List<Service> serEntity = new ArrayList<Service>();
														String serviceType ="Follow UP Service";
														if(cronDetails.isOverdueStatus()){
															try {
																cal.add(Calendar.DATE, -90);
																dateForFilter=fmt1.parse(fmt1.format(cal.getTime()));
																cal.set(Calendar.HOUR_OF_DAY,0);
																cal.set(Calendar.MINUTE,0);
																cal.set(Calendar.SECOND,0);
																cal.set(Calendar.MILLISECOND,0);
																dateForFilter=cal.getTime();
															} catch (ParseException e) {
																e.printStackTrace();
															}
															logger.log(Level.SEVERE,"Inside Overdue filter for querry");
															 serEntity = ofy().load().type(Service.class)
																		.filter("companyId",compEntity.get(i).getCompanyId())
																		.filter("serviceType",serviceType)
																		.filter("status IN",obj)
																		.filter("branch IN",branchList)
																		.filter("serviceDate >=",dateForFilter)
																		.filter("serviceDate <=",duedateForFilter).list();
														}else{
															logger.log(Level.SEVERE,"Inside without overdue filter for querry");
															 serEntity = ofy().load().type(Service.class)
																		.filter("companyId",compEntity.get(i).getCompanyId())
																		.filter("serviceType",serviceType)
																		.filter("status IN",obj)
																		.filter("branch IN",branchList)
																		.filter("serviceDate >=",dateForFilter)
																		.filter("serviceDate <=",duedateForFilter)
																		.list();
														}
														
														logger.log(Level.SEVERE,"companyId"+compEntity.get(i).getCompanyId());
														logger.log(Level.SEVERE,"serviceType"+serviceType);
														logger.log(Level.SEVERE,"status"+obj);
														logger.log(Level.SEVERE,"branch"+branchList);
														logger.log(Level.SEVERE,"dateForFilter"+dateForFilter);	
														logger.log(Level.SEVERE,"duedateForFilter"+duedateForFilter);
														logger.log(Level.SEVERE,"service entity size:"+serEntity.size());	

														  mailTitl = "Services Due As On Date";
														
														{
														
													
														ArrayList<String> tbl_header = new ArrayList<String>();
														
														
											/********************************Sorting table with Branch & Service Date ***********************/
													
														
														Comparator<Service> serviceDateComparator2 = new Comparator<Service>() {
															public int compare(Service s1, Service s2) {
															
															Date date1 = s1.getServiceDate();
															Date date2 = s2.getServiceDate();
															
															//ascending order
															return date1.compareTo(date2);
															}
															};
															Collections.sort(serEntity, serviceDateComparator2);
															
															Comparator<Service> serviceDateComparator = new Comparator<Service>() {
																public int compare(Service s1, Service s2) {
																
																String branch1 = s1.getBranch();
																String branch2 = s2.getBranch();
																
																//ascending order
																return branch1.compareTo(branch2);
																}
																
																};
																Collections.sort(serEntity, serviceDateComparator);
															
																tbl_header.add("Ser_No");
																tbl_header.add("Branch");
																tbl_header.add("Service Id");
																tbl_header.add("Service Date");
																tbl_header.add("Service Engineer");
																tbl_header.add("Customer Name");
																tbl_header.add("Customer Contact No");
																tbl_header.add("Crontact Id");
																tbl_header.add("Product Name");
																tbl_header.add("Service Address");
																tbl_header.add("Locality");
																tbl_header.add("City");
																tbl_header.add("Status");
																tbl_header.add("Ageing");
											/********************************Getting serviceEntity data and adding in the tbl1 List ***********************/
																	
														ArrayList<String> tbl1=new ArrayList<String>();
															
														for(int j=0;j<serEntity.size();j++){
															total++;
															tbl1.add(j+1+"");
												   	 		tbl1.add(serEntity.get(j).getBranch()+"");
													   	 	tbl1.add(serEntity.get(j).getCount()+"");
												   	 	    tbl1.add(fmt.format(serEntity.get(j).getServiceDate()) +"");
												   	 	    tbl1.add(serEntity.get(j).getEmployee()+""); 
												   	 		tbl1.add(serEntity.get(j).getPersonInfo().getFullName()+"");
												   	 		tbl1.add(serEntity.get(j).getPersonInfo().getCellNumber()+"");
												   	 		tbl1.add(serEntity.get(j).getContractCount()+"");
												   	 		
												 
												   	 	    /**************** for getting ageing for each service******/
												   	 	String StringServiceDate = fmt1.format(serEntity.get(j).getServiceDate());
												   	 	    
											//	   	 	   	String StringServiceDate = df.format(serviceDate);	
												   			
												   				Date d1 = null;
												   				Date d2 = null;
												   				
												   				d1 = df.parse(StringServiceDate);
												   				d2 = df.parse(todayDateString);
												   				long diff = d2.getTime() - d1.getTime();
																long diffDays = diff / (24 * 60 * 60 * 1000);
																
													
												   	 	    tbl1.add(serEntity.get(j).getProduct().getProductName()+"");
												   	 	    String address = "";
												   	 	    if(serEntity.get(j).getAddrLine1()!=null){
												   	 	    	address = serEntity.get(j).getAddrLine1();
												   	 	    }
												   	 	    
												   	 	    if(serEntity.get(j).getAddrLine2()!=null){
												   	 	    	address =address + " "+ serEntity.get(j).getAddrLine2();
												   	 	    }
												   	 	    tbl1.add(address);
												   	 	    
												   	 	    String locality = "";
													   	 	if(serEntity.get(j).getLocality()!=null){
													   	 		locality = serEntity.get(j).getLocality();
												   	 	    }
												   	 	    tbl1.add(locality);
												   	 	    tbl1.add(serEntity.get(j).getCity());
												   	 	    tbl1.add(serEntity.get(j).getStatus()+"");
												   	 	    tbl1.add(diffDays +"");
												   	 
														
												   	 	   if(empCount.containsKey(serEntity.get(j).getEmployee().trim())){
												   	 	    	int count = empCount.get(serEntity.get(j).getEmployee().trim());
												   	 	    			
												   	 	    	count++;
												   	 	    	empCount.put(serEntity.get(j).getEmployee().trim(), count);
												   	 	    }else{
												   	 	    	empCount.put(serEntity.get(j).getEmployee().trim(), 1);
												   	 	    }
														
												   	 	    if(branchCount.containsKey(serEntity.get(j).getBranch().trim())){
													   	 	    int count = branchCount.get(serEntity.get(j).getBranch().trim());
										   	 	    			
												   	 	    	count++;
												   	 	    	branchCount.put(serEntity.get(j).getBranch().trim(), count);
												   	 	  
												   	 	    }else{
												   	 	    	branchCount.put(serEntity.get(j).getBranch().trim(),1);
												   	 	    }
														
														}			
														
														logger.log(Level.SEVERE,"In the ProsConfig !=null   3 ");
														
											 /********************************Calling cronsendEmail Method ***********************/
														
											//			Email e=new Email();					
														try {   
															   
																System.out.println("Before send mail method");
																logger.log(Level.SEVERE,"Before send method call to send mail  ");						
																
																if(!cronDetails.getFooter().equals("") ){
																	footer = cronDetails.getFooter();
																}
																
																/**
																 * @author Anil,Date : 13-02-2019
																 * Renaming mail subject as per the rohan's instruction
																 */
//																 emailSubject = "Services due as on date"+" "+	todayDateString;
																 emailSubject = "Services due and overdue as on date"+" "+	todayDateString;
																
														   	 	
																 
																
																logger.log(Level.SEVERE," get record size ..- " + tbl1.size());
																if(cronDetails.isOverdueStatus()){
																	logger.log(Level.SEVERE,"over due recordes are  there.. ");
																	header2 = "Over due record upto date :"+todayDateString;

																	if(!cronDetails.getSubject().trim().equals("") && cronDetails.getSubject().trim().length() >0 ){
																		header2 = cronDetails.getSubject();
																	}
																	
																	if(!cronDetails.getMailBody().equals("") && cronDetails.getMailBody().trim().length() >0){
																		header2 = header2 + " <BR>"+ cronDetails.getMailBody();
																	}
																	
																	header2 = header2  +" <BR> Total Records - "+serEntity.size() ;
																	overHeader = new ArrayList<String>();
																	overHeader.addAll(tbl_header);
																	overDetail.addAll(tbl1);
																	overDue = serEntity.size();
																	
																	footer = footer +"<br>" + "#This mail contains last 3 months data. For more records please contact Email- support@evasoftwaresolutions.com";
																}else{
																	header1 = "Due record upto date : "+todayDateString;
																	if(!cronDetails.getSubject().trim().equals("")  && cronDetails.getSubject().trim().length() >0){
																		header1 = cronDetails.getSubject();
																	}
																	
																	if(!cronDetails.getMailBody().equals("")  && cronDetails.getMailBody().trim().length() >0){
																		header1 = header1 + " <BR>"+ cronDetails.getMailBody();
																	}
																	header1 = header1  +"<BR> Total Records - "+serEntity.size() ;
																	dueHeader = new ArrayList<String>();
																	dueHeader.addAll(tbl_header);
																	dueDetail = new ArrayList<String>();
																	dueDetail.addAll(tbl1);
																	duetotal= serEntity.size();
																}
																
																
																logger.log(Level.SEVERE,"After send mail method ");		
														} catch (Exception e1) {
															
															logger.log(Level.SEVERE,"In the catch block ");
															e1.printStackTrace();
													}
														
													}
													
													
												 }catch(Exception e){
													 e.printStackTrace();
													 logger.log(Level.SEVERE,"get error is  recores"+e.getMessage());
												 }
											}
											
											
										}
										
										Email cronEmail = new Email();
										if(dueDetail.size()>0 || overDetail.size()>0){
											

											logger.log(Level.SEVERE," get branch count --" + branchCount.size() + " emp count --" + empCount.size());
											logger.log(Level.SEVERE," get branch  --" + branchCount.toString() + " emp count --" + empCount.toString());
											ArrayList<String> summHeader  = new ArrayList<String>();
											ArrayList<String> summDetails = new ArrayList<String>();
											
											summHeader.add("Branch");
											summHeader.add("$#");

											summHeader.add("Service Engineer");
											summHeader.add("$#");
											
											int tblSize = 0;
											if(branchCount.size()>0){
												tblSize = branchCount.size();
											}
											
											if(tblSize<empCount.size()){
												tblSize = empCount.size();
											}

											Set<String> branchCntList = branchCount.keySet();
											ArrayList<String> branchDt = new ArrayList<String>();
											branchDt.addAll(branchCntList);
											
											
											Set<String> empCntList = empCount.keySet();
											ArrayList<String> empDt = new ArrayList<String>();
											empDt.addAll(empCntList);
											logger.log(Level.SEVERE,"get emp det -- " + empDt.size() + "tblSize " +tblSize);
											for(int ij=0;ij<tblSize;ij++){
												logger.log(Level.SEVERE,"ij " + ij +" branch0-- " + branchCount.size());
												if(branchCount.size()>ij){
													logger.log(Level.SEVERE,"ij " + ij  +" branch --" + branchDt.get(ij));
													summDetails.add(branchDt.get(ij)+"");
													summDetails.add(branchCount.get(branchDt.get(ij))+"");
												}else{
													summDetails.add("");
													summDetails.add("");
												}
												
												if(empCount.size()>ij){
													logger.log(Level.SEVERE,"ij " + ij  +" empDt --" + empDt.get(ij));
													summDetails.add(empDt.get(ij)+"");
													summDetails.add(empCount.get(empDt.get(ij))+"");
												}else{
													summDetails.add("");
													summDetails.add("");
												}
											}
											String summTitle = " Total Records - " + total ;
											logger.log(Level.SEVERE,"Before send method call to send mail  " + total);						
										
																
											
											cronEmail.cronSendEmailDueOverDue(toEmailListDt, emailSubject, "", c,header1, dueHeader, dueDetail,
													header2, overHeader, overDetail, summTitle,summHeader,summDetails,null, "", footer);
											logger.log(Level.SEVERE,"after send method call to send mail  ");	
											
										}else{							
												
											System.out.println("Sorry no services found for this company");
											logger.log(Level.SEVERE,"Sorry no services found for this company");
											mailTitl = "No Services Found Due";
								
											cronEmail.cronSendEmail(toEmailListDt, "Services Due As On Date"+" "+ todayDateString, mailTitl , c, null, null, null, null, null, null);
								
										}
									}
								
								}
							
						}
						
					}
				}
				
			
	}catch(Exception e){
		e.printStackTrace();
		logger.log(Level.SEVERE," get error in service cron job"+e.getMessage());
	}
		 
}
	
	
}
