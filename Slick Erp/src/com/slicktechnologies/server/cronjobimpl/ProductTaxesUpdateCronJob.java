package com.slicktechnologies.server.cronjobimpl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.server.democonfiguration.DemoConfigurationImpl;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;

public class ProductTaxesUpdateCronJob extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8230370626129863418L;

	Logger logger = Logger.getLogger("AcknowledgementCronJobImpl.class");
	long companyId=0;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doGet(req, resp);
		
		String urlcalled=req.getRequestURL().toString().trim();
		logger.log(Level.SEVERE,"urlcalled:::::"+urlcalled);
		
		String url1=urlcalled.replace("http://", "");
		logger.log(Level.SEVERE,"url1:::::"+url1);
		String[] splitUrl=url1.split("\\.");
		logger.log(Level.SEVERE,"splitUrl[0]:::::"+splitUrl[0]);
		
		Company company=ofy().load().type(Company.class).filter("accessUrl","my").first().now();
		companyId=company.getCompanyId();
		
		/**
		 * rahul added this code for process config 
		 */
//		boolean flag= false;
//		ProcessConfiguration process = ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId).
//				filter("processName", "CronJob").first().now();
//		
//		for (int i = 0; i < process.getProcessList().size(); i++) {
//			
//			if(process.getProcessList().get(i).processType.equalsIgnoreCase("PRODUCTUPDATECRONJOB")){
//				flag= true;
//			}
//		}
//		/**
//		 * ends here 
//		 */
//		if(flag){
			updateProductsTaxes(company,companyId);
			updateStateCode(company,companyId);
//		}
	}
	
	private void updateStateCode(Company company, long companyId) {
		DemoConfigurationImpl demoImpl=new DemoConfigurationImpl();
		List<State> stateList=ofy().load().type(State.class).filter("companyId", companyId).list();
		for(State obj:stateList){
			if(demoImpl.getStateCode(obj.getStateName().trim())!=null){
				obj.setStateCode(demoImpl.getStateCode(obj.getStateName().trim()));
			}
		}
		if(stateList.size()!=0){
			ofy().save().entities(stateList).now();
		}
		
	}
	

	private void updateProductsTaxes(Company company, long companyId) {
		// TODO Auto-generated method stub
//		createGSTTaxes(companyId);
		
		List<ItemProduct> itemProductList=ofy().load().type(ItemProduct.class).filter("companyId", companyId).list();
		List<ServiceProduct> serviceProductList=ofy().load().type(ServiceProduct.class).filter("companyId", companyId).list();
		
		for (ItemProduct itemProduct : itemProductList) {
			
			itemProduct.setServiceTax(getGSTTax("ServiceTax"));
			itemProduct.setVatTax(getGSTTax("VatTax"));
			
		}
		ofy().save().entities(itemProductList);
		
		for (ServiceProduct serviceProduct : serviceProductList) {
			serviceProduct.setServiceTax(getGSTTax("ServiceTax"));
			serviceProduct.setVatTax(getGSTTax("VatTax"));
		}
		ofy().save().entities(serviceProductList);
	}

	private Tax getGSTTax(String taxName) {
		Tax taxDetails=new Tax();
		String tax_name="";
		if(taxName.trim().equalsIgnoreCase("ServiceTax")){
			tax_name="SGST";
		}else if(taxName.trim().equalsIgnoreCase("VatTax")){
			tax_name="CGST";
		}
		taxDetails.setTaxConfigName(tax_name);
		taxDetails.setTaxName(tax_name);
		taxDetails.setPercentage(9);
		taxDetails.setInclusive(false);
		return taxDetails;
	}

	private void createGSTTaxes(long companyId) {
		// TODO Auto-generated method stub
		TaxDetails  taxDetailsforGL=ofy().load().type(TaxDetails.class).filter("companyId", companyId).first().now();
		NumberGeneration ng=ofy().load().type(NumberGeneration.class).filter("processName", "TaxDetails").first().now();
		long number=ng.getNumber();
		int count=(int) number;
		ArrayList<TaxDetails> taxDetailsList=new ArrayList<TaxDetails>();
		TaxDetails taxDetails=new TaxDetails();
		taxDetails.setTaxChargeName("SGST");
		taxDetails.setTaxChargePercent(9);
		taxDetails.setTaxChargeStatus(true);
		taxDetails.setVatTax(true);
		taxDetails.setCentralTax(false);
		taxDetails.setServiceTax(false);
		taxDetails.setCompanyId(companyId);
		taxDetails.setGlAccountName(taxDetailsforGL.getGlAccountName().trim());
		taxDetails.setCount(count+1);
		taxDetails.setTaxPrintName("SGST");
		
		taxDetailsList.add(taxDetails);
		
		taxDetails=new TaxDetails();
		taxDetails.setTaxChargeName("CGST");
		taxDetails.setTaxChargePercent(9);
		taxDetails.setTaxChargeStatus(true);
		taxDetails.setVatTax(true);
		taxDetails.setCentralTax(false);
		taxDetails.setServiceTax(false);
		taxDetails.setCompanyId(companyId);
		taxDetails.setGlAccountName(taxDetailsforGL.getGlAccountName().trim());
		taxDetails.setCount(count+2);
		taxDetails.setTaxPrintName("CGST");
		taxDetailsList.add(taxDetails);

		
		taxDetails=new TaxDetails();
		taxDetails.setTaxChargeName("IGST");
		taxDetails.setTaxChargePercent(18);
		taxDetails.setTaxChargeStatus(true);
		taxDetails.setVatTax(true);
		taxDetails.setCentralTax(false);
		taxDetails.setServiceTax(false);
		taxDetails.setCompanyId(companyId);
		taxDetails.setGlAccountName(taxDetailsforGL.getGlAccountName().trim());
		taxDetails.setCount(count+3);
		taxDetails.setTaxPrintName("IGST");
		taxDetailsList.add(taxDetails);
		
		ng.setNumber(count+3);
		ofy().save().entity(ng);
		ofy().save().entities(taxDetailsList);
		logger.log(Level.SEVERE,"Successfully saved Tax Details");
	}
}
