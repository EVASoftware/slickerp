package com.slicktechnologies.server.cronjobimpl;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

public class ApprovalCronJobImpl extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5133187487874146187L;
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmtAppRequestedDate = new SimpleDateFormat("dd/MM/yyyy");
	Logger logger = Logger.getLogger("NameOfYourLogger");
	  
	public void doGet(HttpServletRequest req, HttpServletResponse resp)	throws IOException 
	{
//		approvalList();
	}

	 public void approvalList(String cronList) 
	{

			logger.log(Level.SEVERE, "Cron list str --" + cronList);
			Gson gson = new Gson();
			JSONArray jsonarr = null;
			try {
				jsonarr=new JSONArray(cronList.trim());
			} catch (JSONException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			
			try {
				CronJobConfigrationDetails cronDetails =null;
				List<Company> compEntity = ofy().load().type(Company.class).list();
				for(int k=0;k<jsonarr.length();k++){
					
					JSONObject jsonObj = jsonarr.getJSONObject(k);
					cronDetails = new CronJobConfigrationDetails();
					
					cronDetails = gson.fromJson(jsonObj.toString(), CronJobConfigrationDetails.class);
					
					
					Email cronEmail = new Email();
					Date today=DateUtility.getDateWithTimeZone("IST", new Date());
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));
					fmt.setTimeZone(TimeZone.getTimeZone("IST"));
					fmtAppRequestedDate.setTimeZone(TimeZone.getTimeZone("IST"));
					
					try{
				 
						ArrayList<String> obj = new ArrayList<String>();
						obj.add("Pending");

			/******************************Converting todayDate to String format*****************/
						 Date todaysDate = new Date();
						 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
						 String todayDateString = df.format(todaysDate);
						 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);
			/********************************Adding Companies in the list ***********************/
				 
				
				if(compEntity.size()>0)
				{
					logger.log(Level.SEVERE,"If compEntity size > 0");
					logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());

					for(int i=0;i<compEntity.size();i++){
						
						Company c=compEntity.get(i);
						logger.log(Level.SEVERE,"In the for loop");
						logger.log(Level.SEVERE,"Company Name="+c);
						logger.log(Level.SEVERE,"The value of i is:" +i);
				
						
						/*************Checking prosname & prosconfig for each company *****************/
				
//						ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
//						ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
//						
//						boolean processconfigflag = false;
//						if(processconfig!=null){
//							if(processconfig.isConfigStatus()){
//								for(int l=0;l<processconfig.getProcessList().size();l++){
//									if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("ApprovalDailyEmail") && processconfig.getProcessList().get(l).isStatus()==true){
//										processconfigflag=true;
//									}
//								}
//						   }
//						}
//						logger.log(Level.SEVERE,"process config  SMS Flag =="+processconfigflag);
//					
//							if(processName!=null){
//								logger.log(Level.SEVERE,"In the prossName");
//								if(processconfigflag)
						

						List<Employee>  employeeList=new ArrayList<Employee>();
						employeeList=ofy().load().type(Employee.class).filter("companyId", c.getCompanyId()).filter("roleName", cronDetails.getEmployeeRole()).list();
						logger.log(Level.SEVERE,""+cronDetails.getEmployeeRole()+" "+"Employee List size::: "+employeeList.size());
						
							for (Employee employee : employeeList) {
								ArrayList<String> toEmailListDt=new ArrayList<String>();
								toEmailListDt.add(employee.getEmail().trim());
								List<String> branchList=new ArrayList<String>();
								for (int j = 0; j < employee.getEmpBranchList().size(); j++) {
									branchList.add(employee.getEmpBranchList().get(j).getBranchName());
									logger.log(Level.SEVERE, "  -- branch list --"+ employee.getEmpBranchList().get(j).getBranchName());
								}
								logger.log(Level.SEVERE,"toEmailList size::: "+toEmailListDt.size()+" Branch List SIze : "+branchList.size());
								
								if(branchList.size()!=0){
									{
										logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
									
										/*****************Reading ServiceQuotationList from SalesQuotation entity  ************/
									
									List<Approvals> approvalEntity = ofy().load().type(Approvals.class).filter("companyId",compEntity.get(i)
											.getCompanyId()).filter("status IN",obj).filter("branchname IN", branchList).list();
									logger.log(Level.SEVERE,"Approval entity size:"+approvalEntity.size());	
									
									
									// email id is added to emailList
									ArrayList<String> toEmailList=new ArrayList<String>();
									toEmailList.add(employee.getEmail());
									
									 String mailTitl = "Approval Due As On Date";
									
									if(approvalEntity.size()>0)
									{
										ArrayList<String> tbl_header = new ArrayList<String>();
										tbl_header.add("Serial No");
										tbl_header.add("Doc Type");
										tbl_header.add("Doc Id");
										tbl_header.add("Requested By");
										tbl_header.add("Requested Date");
										tbl_header.add("Approver Name");
										tbl_header.add("Branch");
										tbl_header.add("Status");
										tbl_header.add("Ageing");
									
						
						/********************************Getting service Approval data and adding in the tbl1 List ***********************/
												
									ArrayList<String> tbl1=new ArrayList<String>();
										
									for(int j=0;j<approvalEntity.size();j++){
										tbl1.add((j+1)+"");
							   	 		tbl1.add(approvalEntity.get(j).getBusinessprocesstype()+"");
							   	 		tbl1.add(approvalEntity.get(j).getBusinessprocessId()+"");
							   	 		tbl1.add(approvalEntity.get(j).getRequestedBy()+"");		   	 		
							   	 		tbl1.add(fmt.format(approvalEntity.get(j).getCreationDate())+"");
							   	 		tbl1.add(approvalEntity.get(j).getApproverName()+"");
							   	 		tbl1.add(approvalEntity.get(j).getBranchname()+"");		 
							   	 	    /**************** for getting ageing for each Approval ******/
							   	 	    String StringQuotationDate = fmtAppRequestedDate.format(approvalEntity.get(j).getCreationDate());
							   	 	    
							   				Date d1 = null;
							   				Date d2 = null;
							   				
							   				d1 = df.parse(StringQuotationDate);
							   				d2 = df.parse(todayDateString);
							   				long diff = d2.getTime() - d1.getTime();
											long diffDays = diff / (24 * 60 * 60 * 1000);
											
							   	 	    tbl1.add(approvalEntity.get(j).getStatus()+"");
							   	 	    tbl1.add(diffDays +"");
							   	 	}			
									
									logger.log(Level.SEVERE,"In the ProsConfig !=null   3 ");
									
						/********************************Calling cronsendEmail Method ***********************/
									
									try {   
											System.out.println("Before send mail method");
											logger.log(Level.SEVERE,"Before send method call to send mail  ");						
//											cronEmail.cronSendEmail(toEmailList, "Approval Due As On Date"+" "+ todayDateString, mailTitl +"  "+ todayDateString, c, tbl_header, tbl1, null, null, null, null);
											
											String footer = "";
											
											if(cronDetails.getFooter().trim()!= "" ){
												footer = cronDetails.getFooter();
											}
											
											
											String emailSubject =  "Approval Due As On Date"+ todayDateString;
											if(cronDetails.isOverdueStatus()){
												 emailSubject =  "Approval overdue As On Date";
											}
											if(!cronDetails.getSubject().trim().equals("") ){
												emailSubject = cronDetails.getSubject();
											}
											
											String emailMailBody = mailTitl;
											
											if(!cronDetails.getMailBody().trim().equals("")){
												emailMailBody = cronDetails.getMailBody();
											}
											
												System.out.println("Before send mail method");
												logger.log(Level.SEVERE,"Before send method call to send mail  ");						
//												cronEmail.cronSendEmail(toEmailList, "Invoice Due As On Date"+" "+ todayDateString, mailTitl +"  "+ todayDateString, c, tbl_header, tbl1, null, null, null, null);
												
												cronEmail.cronSendEmail(toEmailList, emailSubject, emailMailBody, c, tbl_header, tbl1,
														null, null, null, null, "", footer);
											
											logger.log(Level.SEVERE,"After send mail method ");		
													//+"  "+
									} catch (IOException e1) {
										
										logger.log(Level.SEVERE,"In the catch block ");
										e1.printStackTrace();
								}
									
								}
								else{					
										
									System.out.println("Sorry no Approval found for this company");
									logger.log(Level.SEVERE,"Sorry no Approval found for this company");
									
									mailTitl = "No Approval Found Due";
						
									cronEmail.cronSendEmail(toEmailList, "Approval Due As On Date"+" "+ todayDateString, mailTitl , c, null, null, null, null, null, null);
						
								}
									
									
								}
								}
							}
						
//							else
//							{					//else block for prosconfig
//								logger.log(Level.SEVERE,"ProcessConfiguration is null");
//								System.out.println("ProcessConfiguration is null");
//							}
//							
//							}
//						else
//						{        //else block for pross!=null
//							logger.log(Level.SEVERE,"Cron job status is Inactive");
//							System.out.println("Cron job status is Inactive");
//						}
						
										
					}	//end of for loop
				
					}
					else{			//else block for if(compEntity.size()>0)
						
						logger.log(Level.SEVERE,"No Company found from cron job completed");	
						System.out.println("No Company found from cron job completed");
					}
					
					
					 }
					 catch(Exception e2){
					
						 e2.printStackTrace();
					 	}
					
					
					
					
				}
			}catch(Exception e){
				
				logger.log(Level.SEVERE,"approvalList " + e);
			}
			
		 
		 
		 
		 
		
	}	
	 
}
