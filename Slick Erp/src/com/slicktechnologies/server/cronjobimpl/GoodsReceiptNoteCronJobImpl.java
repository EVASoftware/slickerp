package com.slicktechnologies.server.cronjobimpl;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.InventoryProductDetail;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class GoodsReceiptNoteCronJobImpl extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5882320898014420484L;
	
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmtGRNCreationDate = new SimpleDateFormat("dd/MM/yyyy");
	 Logger logger = Logger.getLogger("NameOfYourLogger");
	 
	 	public void doGet(HttpServletRequest req, HttpServletResponse resp)
				throws IOException {
					
						GoodsRecievedNote();
	 	}

		private void GoodsRecievedNote() {

			Email cronEmail = new Email();			
			Date today=DateUtility.getDateWithTimeZone("IST", new Date());
			
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			fmtGRNCreationDate.setTimeZone(TimeZone.getTimeZone("IST"));
			
			/**
			 *    i have added time in today's date
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding "+today);
//			DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
			Calendar cal=Calendar.getInstance();
			cal.setTime(today);
			cal.add(Calendar.DATE, 0);
			
			Date dateForFilter=null;
			
			try {
				dateForFilter=fmtGRNCreationDate.parse(fmtGRNCreationDate.format(cal.getTime()));
				cal.set(Calendar.HOUR_OF_DAY,23);
				cal.set(Calendar.MINUTE,59);
				cal.set(Calendar.SECOND,59);
				cal.set(Calendar.MILLISECOND,999);
				dateForFilter=cal.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Date After setting the Time"+dateForFilter);
			
			/*************************************End*********************************/
			
			
	 try{
		 
		 
	/********************************Adding status in the list ***********************/
		 
		 ArrayList<String> obj = new ArrayList<String>();
		 obj.add("Created");
		 obj.add("Requested");
		 
	
	/******************************Converting todayDate to String format*****************/
		 
		 Date todaysDate = new Date();
		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 String todayDateString = df.format(todaysDate);
		 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);

		 
/********************************Adding Companies in the list ***********************/
		 
	List<Company> compEntity = ofy().load().type(Company.class).list();
	if(compEntity.size()>0){
	
		logger.log(Level.SEVERE,"If compEntity size > 0");
		logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
		

		for(int i=0;i<compEntity.size();i++){
			
			Company c=compEntity.get(i);
			logger.log(Level.SEVERE,"In the for loop");
			logger.log(Level.SEVERE,"Company Name="+c);
			System.out.println("Company Name:"+c);
			logger.log(Level.SEVERE,"The value of i is:" +i);
			logger.log(Level.SEVERE,"Date After Adding One Date"+dateForFilter);

			
/********************************Checking prosname & prosconfig for each company ***********************/

			
			ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
			ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
			
			boolean processconfigflag = false;
			if(processconfig!=null){
				if(processconfig.isConfigStatus()){
					for(int l=0;l<processconfig.getProcessList().size();l++){
						if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("GRNDailyEmail") && processconfig.getProcessList().get(l).isStatus()==true){
							processconfigflag=true;
						}
					}
			   }
			}
			logger.log(Level.SEVERE,"process config  SMS Flag =="+processconfigflag);
			
			if(processName!=null){
				logger.log(Level.SEVERE,"In the prossName");
			if(processconfigflag){
							
				logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
				
				
	/********************************Reading GRN from GRN entity  ***********************/
				
				List<GRN> goodsRecivdNoteEntity = ofy().load().type(GRN.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("creationDate <=",dateForFilter).filter("status IN",obj).list();

				logger.log(Level.SEVERE,"Today date Is:"+dateForFilter);	

				logger.log(Level.SEVERE,"GRN entity size:"+goodsRecivdNoteEntity.size());	
				System.out.println("GRN Size:"+goodsRecivdNoteEntity.size());
				
				// email id is added to emailList
				ArrayList<String> toEmailList=new ArrayList<String>();
				toEmailList.add(compEntity.get(i).getPocEmail());
				
				 String mailTitl = "Goods Recieved Note Due As On Date";
				
				if(goodsRecivdNoteEntity.size()>0){
				
			
				ArrayList<String> tbl_header = new ArrayList<String>();
				tbl_header.add("Serial No");
				tbl_header.add("Branch");
				tbl_header.add("Vendor Id");
				tbl_header.add("Vendor Name");
				tbl_header.add("Vendor Cont No");
				tbl_header.add("Purchase Engineer");
				tbl_header.add("GRN Id");
				tbl_header.add("GRN Category");
				tbl_header.add("GRN Date");
				tbl_header.add("Status");
				tbl_header.add("Ageing");
				
/********************************Sorting table with Branch & GRN Date ***********************/
			
				
				Comparator<GRN> grnDateComparotor = new Comparator<GRN>() {
					public int compare(GRN s1, GRN s2) {
					
					Date date1 = s1.getCreationDate();
					Date date2 = s2.getCreationDate();
					
					//ascending order
					return date1.compareTo(date2);
					}
					};
					Collections.sort(goodsRecivdNoteEntity, grnDateComparotor);
					
					Comparator<GRN> grnDateComparotor2 = new Comparator<GRN>() {
						public int compare(GRN s1, GRN s2) {
						
						String branch1 = s1.getBranch();
						String branch2 = s2.getBranch();
						
						//ascending order
						return branch1.compareTo(branch2);
						}
						
						};
						Collections.sort(goodsRecivdNoteEntity, grnDateComparotor2);
					
/********************************Getting GRNEntity data and adding in the tbl1 List ***********************/
							
				ArrayList<String> tbl1=new ArrayList<String>();
					
				for(int j=0;j<goodsRecivdNoteEntity.size();j++){
					tbl1.add((j+1)+"");
		   	 		tbl1.add(goodsRecivdNoteEntity.get(j).getBranch() +"");
		   	 		tbl1.add(goodsRecivdNoteEntity.get(j).getVendorInfo().getCount() +"");
		   	 		tbl1.add(goodsRecivdNoteEntity.get(j).getVendorInfo().getFullName() +"");		   	 		
		   	 		tbl1.add(goodsRecivdNoteEntity.get(j).getVendorInfo().getCellNumber() +"");
		   	 		tbl1.add(goodsRecivdNoteEntity.get(j).getEmployee());
		   	 		tbl1.add(goodsRecivdNoteEntity.get(j).getCount()+"");
		   	 		tbl1.add(goodsRecivdNoteEntity.get(j).getGrnCategory());
		   	 	    tbl1.add(fmt.format(goodsRecivdNoteEntity.get(j).getCreationDate()) +"");
		 
		   	 	    /**************** for getting ageing for each GRN******/
		   	 	    String stringGrnDate = fmtGRNCreationDate.format(goodsRecivdNoteEntity.get(j).getCreationDate());
		   	 	    
		   				Date d1 = null;
		   				Date d2 = null;
		   				
		   				d1 = df.parse(stringGrnDate);
		   				d2 = df.parse(todayDateString);
		   				long diff = d2.getTime() - d1.getTime();
						long diffDays = diff / (24 * 60 * 60 * 1000);
						
//						System.out.println("Ageing:"+diffDays);
//				   	 	logger.log(Level.SEVERE,"Ageing:"+diffDays);
			
		   	 	    tbl1.add(goodsRecivdNoteEntity.get(j).getStatus()+"");
		   	 	    tbl1.add(diffDays +"");
		   	 	}			
				
				logger.log(Level.SEVERE,"In the ProsConfig !=null   3 ");
				
  /********************************Calling cronsendEmail Method ***********************/
				
				try {   
					   
						System.out.println("Before send mail method");
						logger.log(Level.SEVERE,"Before send method call to send mail  ");						
						cronEmail.cronSendEmail(toEmailList, "Goods Recieved Note Due As On Date"+" "+ todayDateString, mailTitl +"  "+ todayDateString, c, tbl_header, tbl1, null, null, null, null);
						logger.log(Level.SEVERE,"After send mail method ");		
								//+"  "+
				} catch (IOException e1) {
					
					logger.log(Level.SEVERE,"In the catch block ");
					e1.printStackTrace();
			}
				
			}
			else{						
					
				System.out.println("Sorry no GRN Note found due for this company");
				logger.log(Level.SEVERE,"Sorry GRN Note found due this company");	
				mailTitl = "No Goods Recieved Note Found Due";

				cronEmail.cronSendEmail(toEmailList, "Goods Recieved Note Due As On Date"+" "+ todayDateString, mailTitl , c, null, null, null, null, null, null);

			}
				
				
			}
			else{					//else block for prosconfig
					logger.log(Level.SEVERE,"ProcessConfiguration is null");
					System.out.println("ProcessConfiguration is null");
			}
			
			}else{        //else block for pross!=null
				logger.log(Level.SEVERE,"Cron job status is Inactive");
				System.out.println("Cron job status is Inactive");
			}
			
							
		}	//end of for loop
		
	}
	else{			//else block for if(compEntity.size()>0)
		
		logger.log(Level.SEVERE,"No Company found from cron job completed");	
		System.out.println("No Company found from cron job completed");
	}
	
	
	 }catch(Exception e2){
	
	e2.printStackTrace();
	 }


	 		
	 		
	 	
			
		}

		
		/**
		 *  nidhi
		 *  1-03-2018
		 *  cron job configration
		 */
		public void GoodsRecievedNote(List<CronJobConfigrationDetails> cronList,HashSet<String>  empRoleList) {
			
			
			
			logger.log(Level.SEVERE, "Cron list str --" + cronList);
			try {
				
				
				List<Company> compEntity = ofy().load().type(Company.class).list();
				if(compEntity.size()>0){
				
					logger.log(Level.SEVERE,"If compEntity size > 0");
					logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
					
	
					for(int i=0;i<compEntity.size();i++){
						
						Company c=compEntity.get(i);
						
						for(String emprole : empRoleList ){
							logger.log(Level.SEVERE,"emp "+emprole);
						}
						for(String empRoleName :empRoleList){
//							logger.log(Level.SEVERE," emp --" + empRoleName);
//						} 
//						for(String empRoleName1 :empRoleList){
							
							 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
							
							 ArrayList<String> obj = new ArrayList<String>();
							 obj.add("Created");
							 obj.add("Requested");
							 
							 List<Employee>  employeeList=new ArrayList<Employee>();
								employeeList=ofy().load().type(Employee.class).filter("companyId", c.getCompanyId()).filter("roleName",empRoleName).list();
//								logger.log(Level.SEVERE,empRoleName+" "+"Employee List size::: "+employeeList.size());
//								logger.log(leve);
								
								for (Employee employee : employeeList) {
//									HashMap<String, Integer> empCount = new HashMap()<String , Integer>();
									HashMap<String, Integer> empCount = new HashMap<String, Integer>();
									HashMap<String, Integer> branchCount = new HashMap<String, Integer>();
									
									int total =0;
									ArrayList<String> overHeader = new ArrayList<String>();
									ArrayList<String> dueHeader = new ArrayList<String>();
									ArrayList<String> overDetail = new ArrayList<String>();
									ArrayList<String> dueDetail = new ArrayList<String>();
									
									String header1 = "",header2 = "",title = "";
									
									
									 String todayDateString ="";
									 String mailTitl = "Goods Recieved Note As On Date";
								 
									 String footer = "";
										
										
								 	String emailSubject =  "Goods Recieved Note Due As On Date"+" "+ todayDateString;
									
									
									String emailMailBody = mailTitl;
									
									
									ArrayList<String> toEmailList=new ArrayList<String>();
									toEmailList.add(employee.getEmail());
									
									
									List<String> branchList=new ArrayList<String>();
									
									branchList.add(employee.getBranchName());
									for (int j = 0; j < employee.getEmpBranchList().size(); j++) {
										branchList.add(employee.getEmpBranchList().get(j).getBranchName());
										logger.log(Level.SEVERE, "  -- branch list --"+ employee.getEmpBranchList().get(j).getBranchName());
									}
									logger.log(Level.SEVERE,"toEmailList size::: "+toEmailList.size()+" Branch List SIze : "+branchList.size());
									
									if(branchList.size()!=0){
										
										
										for(int k=0;k<cronList.size();k++){
											
											if(cronList.get(k).getEmployeeRole().equalsIgnoreCase(empRoleName)){
												CronJobConfigrationDetails cronDetails =cronList.get(k);
												
													
												Date today=DateUtility.getDateWithTimeZone("IST", new Date());
												
												TimeZone.setDefault(TimeZone.getTimeZone("IST"));
												fmt.setTimeZone(TimeZone.getTimeZone("IST"));
												fmtGRNCreationDate.setTimeZone(TimeZone.getTimeZone("IST"));
												
												logger.log(Level.SEVERE,"Date Before Adding "+today);
												Calendar cal=Calendar.getInstance();
												cal.setTime(today);
												cal.add(Calendar.DATE, 0);
												
												Date dateForFilter=null;
							
												try {
													dateForFilter=fmtGRNCreationDate.parse(fmtGRNCreationDate.format(cal.getTime()));
													cal.set(Calendar.HOUR_OF_DAY,0);
													cal.set(Calendar.MINUTE,0);
													cal.set(Calendar.SECOND,0);
													cal.set(Calendar.MILLISECOND,0);
													dateForFilter=cal.getTime();
												} catch (ParseException e) {
													e.printStackTrace();
												}
												todayDateString = fmtGRNCreationDate.format(new Date());
												logger.log(Level.SEVERE,"Date After setting the todayDateString"+todayDateString);
												
												cal.setTime(today);
												int diffDay = 0;
												 if(cronDetails.getOverdueDays()>0 && cronDetails.isOverdueStatus()){
													 diffDay = -cronDetails.getOverdueDays();
												 }else{
													 diffDay = cronDetails.getInterval();
												 }
												
												cal.add(Calendar.DATE, diffDay);
												
												Date duedateForFilter=null;
												
												try {
													duedateForFilter=fmtGRNCreationDate.parse(fmtGRNCreationDate.format(cal.getTime()));
													cal.set(Calendar.HOUR_OF_DAY,23);
													cal.set(Calendar.MINUTE,59);
													cal.set(Calendar.SECOND,59);
													cal.set(Calendar.MILLISECOND,999);
													duedateForFilter=cal.getTime();
												} catch (ParseException e) {
													e.printStackTrace();
												}
												
												logger.log(Level.SEVERE,"Date After setting time"+duedateForFilter);
												

												List<GRN> goodsRecivdNoteEntity = new ArrayList<GRN>();
												
												if(cronDetails.isOverdueStatus()){
													 goodsRecivdNoteEntity = ofy().load().type(GRN.class)
																.filter("companyId",compEntity.get(i).getCompanyId())
																.filter("creationDate <=",duedateForFilter).filter("status IN",obj).list();
												}else{
													 goodsRecivdNoteEntity = ofy().load().type(GRN.class)
																.filter("companyId",compEntity.get(i).getCompanyId())
																.filter("branch IN",branchList).filter("creationDate >=",dateForFilter )
																.filter("creationDate <=",duedateForFilter).filter("status IN",obj).list();
												}
												
												
			
												logger.log(Level.SEVERE,"Today date Is:"+dateForFilter);	
			
												logger.log(Level.SEVERE,"GRN entity size:"+goodsRecivdNoteEntity.size());	
												
												
												if(goodsRecivdNoteEntity.size()>0){
												
											
												ArrayList<String> tbl_header = new ArrayList<String>();
											
												
								/********************************Sorting table with Branch & GRN Date ***********************/
											
												
												Comparator<GRN> grnDateComparotor = new Comparator<GRN>() {
													public int compare(GRN s1, GRN s2) {
													
													Date date1 = s1.getCreationDate();
													Date date2 = s2.getCreationDate();
													
													//ascending order
													return date1.compareTo(date2);
													}
													};
													Collections.sort(goodsRecivdNoteEntity, grnDateComparotor);
													
													Comparator<GRN> grnDateComparotor2 = new Comparator<GRN>() {
														public int compare(GRN s1, GRN s2) {
														
														String branch1 = s1.getBranch();
														String branch2 = s2.getBranch();
														
														//ascending order
														return branch1.compareTo(branch2);
														}
														
														};
														Collections.sort(goodsRecivdNoteEntity, grnDateComparotor2);
													logger.log(Level.SEVERE,"get sorted ");
								/********************************Getting GRNEntity data and adding in the tbl1 List ***********************/
															
													tbl_header.add("Serial No");
													tbl_header.add("Branch");
													tbl_header.add("GRN Id");
													tbl_header.add("GRN Date");
													tbl_header.add("GRN Title");
													tbl_header.add("Vendor Name");
													tbl_header.add("Vendor Cell No");
													tbl_header.add("Purchase Engineer");
													tbl_header.add("PO No.");
													tbl_header.add("PO Date");
													tbl_header.add("Value");
													tbl_header.add("GRN Category");
													tbl_header.add("Status");
													tbl_header.add("Ageing");
													
													
												ArrayList<String> tbl1=new ArrayList<String>();
													
												for(int j=0;j<goodsRecivdNoteEntity.size();j++){
													logger.log(Level.SEVERE,"get total "+total);
													total++;
													tbl1.add((j+1)+"");
										   	 		tbl1.add(goodsRecivdNoteEntity.get(j).getBranch() +"");
										   	 		tbl1.add(goodsRecivdNoteEntity.get(j).getCount()+"");
										   	 		tbl1.add(fmt.format(goodsRecivdNoteEntity.get(j).getCreationDate()) +"");
										   	 		if(goodsRecivdNoteEntity.get(j).getTitle() == null){
										   	 		tbl1.add("");
										   	 		}else{
										   	 			tbl1.add(goodsRecivdNoteEntity.get(j).getTitle());
										   	 		}
										   	 		
										   	 	logger.log(Level.SEVERE,"get diff --" + goodsRecivdNoteEntity.get(j).getPoCreationDate());
										   	 		tbl1.add(goodsRecivdNoteEntity.get(j).getVendorInfo().getFullName() +"");		   	 		
										   	 		tbl1.add(goodsRecivdNoteEntity.get(j).getVendorInfo().getCellNumber() +"");
										   	 		tbl1.add(goodsRecivdNoteEntity.get(j).getEmployee());
										   	 		tbl1.add(goodsRecivdNoteEntity.get(j).getPoNo()+"");
										   	 		if(goodsRecivdNoteEntity.get(j).getPoCreationDate()!=null){
										   	 			tbl1.add(fmt.format(goodsRecivdNoteEntity.get(j).getPoCreationDate()));
										   	 		}else{
										   	 			tbl1.add("");
										   	 		}
										   	 		
										   	 		
										 
										   	 	    /**************** for getting ageing for each GRN******/
										   	 	    String stringGrnDate = fmtGRNCreationDate.format(goodsRecivdNoteEntity.get(j).getCreationDate());
										   	 	    
										   				Date d1 = null;
										   				Date d2 = null;
										   				logger.log(Level.SEVERE,"get todayDateString --" +todayDateString);
										   				d1 = df.parse(stringGrnDate);
										   				d2 = df.parse(todayDateString);
										   				long diff = d2.getTime() - d1.getTime();
														long diffDays = diff / (24 * 60 * 60 * 1000);
														double totolamt = 0;
														for(InventoryProductDetail piv: goodsRecivdNoteEntity.get(j).getProductDetails()){
															totolamt = totolamt + piv.getTotal();
														}
														
														logger.log(Level.SEVERE,"get diff --" + diffDays);
														tbl1.add(totolamt+"");	
														if(goodsRecivdNoteEntity.get(j).getGrnCategory()==null){
											   	 			tbl1.add("");
											   	 		}else{
											   	 			tbl1.add(goodsRecivdNoteEntity.get(j).getGrnCategory());
											   	 		}
											   	 	    
										   	 	    tbl1.add(goodsRecivdNoteEntity.get(j).getStatus()+"");
										   	 	    tbl1.add(diffDays +"");
										   	 	    logger.log(Level.SEVERE,"get emp count --" + empCount.size() +" branch count --" + branchCount.size());
										   	 	    
										   	 	    if(goodsRecivdNoteEntity.get(j).getEmployee() != null && 
										   	 	    		empCount.containsKey(goodsRecivdNoteEntity.get(j).getEmployee().trim())){
										   	 	    	int count = empCount.get(goodsRecivdNoteEntity.get(j).getEmployee().trim());
										   	 	    			
										   	 	    	count++;
										   	 	    	empCount.put(goodsRecivdNoteEntity.get(j).getEmployee().trim(), count);
										   	 	    }else{
										   	 	    	empCount.put(goodsRecivdNoteEntity.get(j).getEmployee().trim(), 1);
										   	 	    }
												
										   	 	    if(goodsRecivdNoteEntity.get(j).getBranch()!= null && 
										   	 	    		branchCount.containsKey(goodsRecivdNoteEntity.get(j).getBranch().trim())){
										   	 	    	
											   	 	    int count = branchCount.get(goodsRecivdNoteEntity.get(j).getBranch().trim());
								   	 	    			
										   	 	    	count++;
										   	 	    	branchCount.put(goodsRecivdNoteEntity.get(j).getBranch().trim(), count);
										   	 	  
										   	 	    }else{
										   	 	    	branchCount.put(goodsRecivdNoteEntity.get(j).getBranch().trim(),1);
										   	 	    }
										   	 	    
										   	 	logger.log(Level.SEVERE,"get emp count --" + empCount +" branch count --" + branchCount);
												
												try {   
													   
														logger.log(Level.SEVERE,"Before send method call to send mail  ");						
													
														if(cronDetails.getFooter().trim() != "" ){
															footer = cronDetails.getFooter();
														}
														
														 emailSubject =  "Goods recieved note due as on date"+" "+ todayDateString;
														
														if(cronDetails.isOverdueStatus()){
															emailSubject = "Goods recieved note overdue as on date"+" "+ todayDateString;
														}
														
														 emailMailBody = "";
														logger.log(Level.SEVERE," get record size ..- " + tbl1.size());
														
														
														logger.log(Level.SEVERE,"loop end ");		
												} catch (Exception e1) {
													
													logger.log(Level.SEVERE,"In the catch block ");
													e1.printStackTrace();
											}
												
											}
												
												if(cronDetails.isOverdueStatus()){
													logger.log(Level.SEVERE,"over due recordes are  there.. ");
													header2 = "Over Due Record upto Date :" + todayDateString;
													
													if(!cronDetails.getSubject().trim().equals("") && cronDetails.getSubject().trim().length() >0){
														header2 = cronDetails.getSubject();
													}
													if(!cronDetails.getMailBody().equals("") && cronDetails.getMailBody().trim().length() >0){
														header2 =header2 + "<br>"+ cronDetails.getMailBody();
													}
													overHeader = new ArrayList<String>();
													overHeader.addAll(tbl_header);
													overDetail.addAll(tbl1);
												}else{
													header1 = "Due Record upto Date :";
													if(cronDetails.getSubject().trim().equals("")){
														header1 = cronDetails.getSubject();
													}
													if(cronDetails.getMailBody().equals("")){
														header1 =header1 + "<br>"+ cronDetails.getMailBody();
													}
													dueHeader = new ArrayList<String>();
													dueHeader.addAll(tbl_header);
													dueDetail = new ArrayList<String>();
													dueDetail.addAll(tbl1);
												}
											}
										}

										
									}
										
									try {   
										Email cronEmail = new Email();
										if(overDetail.size()>0 || dueDetail.size()>0){
											
											logger.log(Level.SEVERE," get branch count --" + branchCount.size() + " emp count --" + empCount.size());
											logger.log(Level.SEVERE," get branch  --" + branchCount.toString() + " emp count --" + empCount.toString());
											ArrayList<String> summHeader  = new ArrayList<String>();
											ArrayList<String> summDetails = new ArrayList<String>();
											
											summHeader.add("Branch");
											summHeader.add("$#");

											summHeader.add("Purchase Engineer");
											summHeader.add("$#");
											
											int tblSize = 0;
											if(branchCount.size()>0){
												tblSize = branchCount.size();
											}
											
											if(tblSize<empCount.size()){
												tblSize = empCount.size();
											}

											Set<String> branchCntList = branchCount.keySet();
											ArrayList<String> branchDt = new ArrayList<String>();
											branchDt.addAll(branchCntList);
											
											
											Set<String> empCntList = empCount.keySet();
											ArrayList<String> empDt = new ArrayList<String>();
											empDt.addAll(empCntList);
											logger.log(Level.SEVERE," get branch count --" + branchCount.size() + " tblSize --" + tblSize + " \n branchDt --" + branchDt + "  \n " +empDt);
											for(int ij=0;ij<tblSize;ij++){
												logger.log(Level.SEVERE," summ -- " + ij);
												if(branchCount.size()>ij){
													logger.log(Level.SEVERE," summ -- " + ij + " branch --" + branchDt.get(ij));
													summDetails.add(branchDt.get(ij)+"");
													summDetails.add(branchCount.get(branchDt.get(ij))+"");
												}else{
													summDetails.add("");
													summDetails.add("");
												}
												
												if(empCount.size()>ij){
													logger.log(Level.SEVERE," summ -- " + ij+ " empCount --" + empCount.get(empDt.get(ij)));
													summDetails.add(empDt.get(ij)+"");
													summDetails.add(empCount.get(empDt.get(ij))+"");
												}else{
													summDetails.add("");
													summDetails.add("");
												}
											}
											String summTitle = " Total Records - " + total ;
											logger.log(Level.SEVERE,"Before send method call to send mail  " + total);						
										
																
											
											cronEmail.cronSendEmailDueOverDue(toEmailList, emailSubject, emailMailBody, c,header1, dueHeader, dueDetail,
													header2, overHeader, overDetail, summTitle,summHeader,summDetails,null, "", footer);
											logger.log(Level.SEVERE,"after send method call to send mail  ");	
										}
										else{
											
											logger.log(Level.SEVERE,"Sorry GRN Note found due this company");	
											mailTitl = "No Goods Recieved Note Found Due";
		
											cronEmail.cronSendEmail(toEmailList, "Goods Recieved Note Due As On Date"+" "+ todayDateString, mailTitl , c, null, null, null, null, null, null);
		
										}
										
										logger.log(Level.SEVERE,"After send mail method ");		
												//+"  "+
								} catch (IOException e1) {
									
									logger.log(Level.SEVERE,"In the catch block ");
									e1.printStackTrace();
							}
						}
						
					}
				}
				
			}	
		}
			}catch(Exception e){
				logger.log(Level.SEVERE," GoodsRecievedNote -- "+e);
			}
				
	 	
			
		}
		/**
		 *  end
		 */
}
