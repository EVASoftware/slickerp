package com.slicktechnologies.server.cronjobimpl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class CustomerARPaymentTDSReminderEmailToClientCronJOb extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -569957876331665888L;
	
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private DateFormat fmtPaymentDate = new SimpleDateFormat("dd/MM/yyyy");
	 Logger logger = Logger.getLogger("NameOfYourLogger");
	 

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		customerPaymentARTDSlist();
	}


	private void customerPaymentARTDSlist() {
		// TODO Auto-generated method stub


		Email cronEmail = new Email();
		
		Date today=DateUtility.getDateWithTimeZone("IST", new Date());
		
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmtPaymentDate.setTimeZone(TimeZone.getTimeZone("IST"));
		
		/**
		 *    i have added time in today's date
		 */
		
		Calendar cal=Calendar.getInstance();
		cal.setTime(today);
		cal.add(Calendar.DATE, -31);
		
		Date dateForFilter=null;
		
		try {
			dateForFilter=fmtPaymentDate.parse(fmtPaymentDate.format(cal.getTime()));
			cal.set(Calendar.HOUR_OF_DAY,23);
			cal.set(Calendar.MINUTE,59);
			cal.set(Calendar.SECOND,59);
			cal.set(Calendar.MILLISECOND,999);
			dateForFilter=cal.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		logger.log(Level.SEVERE,"Date After setting the Time"+dateForFilter);
		
		/************************************* End *********************************/
		
		
 try{
	 
	 logger.log(Level.SEVERE,"In Payment AR List");	
	logger.log(Level.SEVERE,"Date After Adding One Date"+dateForFilter);

	 
/********************************Adding status in the list ***********************/
	 
	 ArrayList<String> obj = new ArrayList<String>();
	 obj.add("Created");
	 obj.add("Closed");
	 
/******************************Converting todayDate to String format*****************/
	 
	 Date todaysDate = new Date();
	 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	 String todayDateString = df.format(todaysDate);
	 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);

	 
/********************************Adding Companies in the list ***********************/
	 
List<Company> compEntity = ofy().load().type(Company.class).list();
if(compEntity.size()>0){

	logger.log(Level.SEVERE,"If compEntity size > 0");
	logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());

	for(int i=0;i<compEntity.size();i++){
		
		Company c=compEntity.get(i);
		logger.log(Level.SEVERE,"In the for loop");
		logger.log(Level.SEVERE,"Company Name="+c);
		logger.log(Level.SEVERE,"The value of i is:" +i);
		logger.log(Level.SEVERE,"Date After Adding One Date"+dateForFilter);

		
/********************************Checking prosname & prosconfig for each company ***********************/
	
		ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
		ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
		
		boolean processconfigflag = false;
		if(processconfig!=null){
			if(processconfig.isConfigStatus()){
				for(int l=0;l<processconfig.getProcessList().size();l++){
					if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("PaymentARTDSCertificateDueEmailToClient") && processconfig.getProcessList().get(l).isStatus()==true){
						processconfigflag=true;
					}
				}
		   }
		}
		logger.log(Level.SEVERE,"process config Flag =="+processconfigflag);
		
		if(processName!=null){
			logger.log(Level.SEVERE,"In the prossName");
		if(processconfigflag){
						
			logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
			
			
/********************************Reading PaymentList from CustomerPayment entity  ***********************/
			
			List<CustomerPayment> custPaymentEntity = ofy().load().type(CustomerPayment.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("paymentDate <=",dateForFilter).filter("tdsApplicable", true).filter("tdsCertificateReceived", false).filter("status IN",obj).filter("accountType", AppConstants.BILLINGACCOUNTTYPEAR).list();
			
			logger.log(Level.SEVERE,"Today date Is:"+dateForFilter);	

			logger.log(Level.SEVERE,"Paymnet entity size:"+custPaymentEntity.size());	
			
			 String mailTitl = "TDS Certificate Reminder";
			
				
/********************************Getting PaymnetEntity data and adding in the tbl1 List ***********************/
						
		HashMap<Integer, ArrayList<CustomerPayment>> paymenthashmap = new HashMap<Integer,ArrayList<CustomerPayment>>();

			for(int j=0;j<custPaymentEntity.size();j++){
				
				ArrayList<CustomerPayment> list = new ArrayList<CustomerPayment>();
				
				if(paymenthashmap.containsKey(custPaymentEntity.get(j).getCustomerCount())){
					list = paymenthashmap.get(custPaymentEntity.get(j).getCustomerCount());
					list.add(custPaymentEntity.get(j));
				}else{
					list.add(custPaymentEntity.get(j));
				}
				paymenthashmap.put(custPaymentEntity.get(j).getCustomerCount(), list);
				
				
	   	 	}
			
			Set s1 = paymenthashmap.entrySet();
			Iterator iterator =  s1.iterator();
			
			
			while(iterator.hasNext()){
				ArrayList<CustomerPayment> customerPaymentlist = new ArrayList<CustomerPayment>();

				System.out.println("hi vijay");
				Map.Entry<Integer, ArrayList<CustomerPayment>> m33 = (Map.Entry<Integer, ArrayList<CustomerPayment>>) iterator.next();
				customerPaymentlist.addAll(m33.getValue());
				System.out.println("hi vijay for customer id "+customerPaymentlist.get(0).getCustomerCount());
				System.out.println("listt size =="+customerPaymentlist);
				
				ArrayList<String> tbl1_header=new ArrayList<String>();
				ArrayList<String> tbl1=new ArrayList<String>();
				
//				Customer customerEntity=ofy().load().type(Customer.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("count", customerPaymentlist.get(0).getPersonInfo().getCount()).first().now();
//				
//				// email id is added to emailList
//				ArrayList<String> toEmailList1=new ArrayList<String>();
//				toEmailList1.add(customerEntity.getEmail());
//				
//				String customerName = getFirstLetterUpperCase(customerEntity.getFullname());
//				System.out.println("List customer email id ==="+toEmailList1);
				String msgBody="";
				
				ArrayList<String> toEmailList1 = null;
				String customerName="";
				
				logger.log(Level.SEVERE,"customerPaymentlist size for one client"+customerPaymentlist.size());
				
				
				for(int k=0;k<customerPaymentlist.size();k++){
		
					if(k==0){
						Customer customerEntity=ofy().load().type(Customer.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("count", customerPaymentlist.get(k).getPersonInfo().getCount()).first().now();
						// email id is added to emailList
						toEmailList1=new ArrayList<String>();
						toEmailList1.add(customerEntity.getEmail());
						customerName = getFirstLetterUpperCase(customerEntity.getFullname());
					}
					
					
					//Table header 1
					if(k==0){
						tbl1_header.add("Sr No");
						tbl1_header.add("Invoice Id");
						tbl1_header.add("Invoice Date");
						tbl1_header.add("Invoice Amount");
						tbl1_header.add("Payment Date");
						tbl1_header.add("TDS Deducted");
						tbl1_header.add("Reference No");
						tbl1_header.add("Remark");
					}
						
					tbl1.add(k+1+"");
					tbl1.add(customerPaymentlist.get(k).getInvoiceCount()+"");
					tbl1.add(fmt.format(customerPaymentlist.get(k).getInvoiceDate()));
					Invoice invoiceEntity = ofy().load().type(Invoice.class).filter("count", customerPaymentlist.get(k).getInvoiceCount()).filter("companyId", customerPaymentlist.get(k).getCompanyId()).first().now();
					tbl1.add(customerPaymentlist.get(k).getPaymentAmt()+"");
					tbl1.add(fmt.format(customerPaymentlist.get(k).getPaymentDate()));
					tbl1.add(customerPaymentlist.get(k).getTdsTaxValue()+"");
					
					if(invoiceEntity.getRefNumber()!=null){
						tbl1.add(invoiceEntity.getRefNumber());
					}else{
						tbl1.add(" ");
					}
					if(invoiceEntity.getRemark()!=null){
						tbl1.add(invoiceEntity.getRemark());
					}else{
						tbl1.add(" ");
					}
					
					logger.log(Level.SEVERE,"In the ProsConfig !=null   3 ");
				}
				
				msgBody ="</br></br> Dear "+customerName+ ", <br><br> Thank you for the payment made against following invoices. We also request you to send us the TDS certificate of the same at the earliest. <br><br>";

				String paymentTableTitle = null;
				String paymentTableTitle2 = null;
				
				String msgBodyTwo = null;
				String msgbodyThree = "Thanking you in advance,";
				
				String footermsg = null;
				
				String mailSubject=" Issue TDS Certificate - Reminder"+compEntity.get(i).getBusinessUnitName();;

				logger.log(Level.SEVERE,"Client Name =="+customerName);

				cronEmail.PaymentReminderCronJobEmailToClient(toEmailList1, mailSubject, mailTitl, c, msgBody,null,null, tbl1_header, tbl1,paymentTableTitle, paymentTableTitle2, null,  msgBodyTwo, msgbodyThree, null, footermsg, null, false, false);
				
			}
			
		}
		else{					//else block for prosconfig
				logger.log(Level.SEVERE,"ProcessConfiguration is null");
				System.out.println("ProcessConfiguration is null");
		}
		
		}else{        //else block for pross!=null
			logger.log(Level.SEVERE,"Cron job status is Inactive");
			System.out.println("Cron job status is Inactive");
		}
		
						
	}	//end of for loop
	
	}
	else{			//else block for if(compEntity.size()>0)
		
		logger.log(Level.SEVERE,"No Company found from cron job completed");	
		System.out.println("No Company found from cron job completed");
	}


	 }catch(Exception e2){
	
		 e2.printStackTrace();
	 }
	}
	
	
private String getFirstLetterUpperCase(String customerFullName) {
		
		String customerName="";
		String[] customerNameSpaceSpilt=customerFullName.split(" ");
		int count=0;
		for (String name : customerNameSpaceSpilt) 
		{
			String nameLowerCase=name.toLowerCase();
			if(count==0)
			{
				customerName=customerName+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);
			}
			else
			{
				customerName=customerName+" "+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);	
			}
			
			count=count+1;
			
		}
		return customerName;  
		
		}
	

}
