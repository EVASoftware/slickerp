package com.slicktechnologies.server.cronjobimpl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.simplesoftwares.client.library.ReturnFromServer;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.RoleDefinition;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.inventory.BranchDetails;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.UpdateStock;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApproval;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApprovalDetails;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;


public class AutomaticPRCreationCronJobImpl extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4284587483386704721L;
	
	 Logger logger = Logger.getLogger("NameOfYourLogger");
	 private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");


	 List<WareHouse> warehouselist=new ArrayList<WareHouse>();
	 
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		createAutomaticPR();
	}

	public void createAutomaticPR() {
		
		List<Company> compEntity = ofy().load().type(Company.class).list();
		if(compEntity.size()>0){
			logger.log(Level.SEVERE,"If compEntity size > 0");
			logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());

			for(int i=0;i<compEntity.size();i++){
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition", "EnableAutomaticPROrderQty", compEntity.get(i).getCompanyId())
					|| ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialRequestNote", "EnableAutomaticMRNOrderQty", compEntity.get(i).getCompanyId())){
				ArrayList<ProductInventoryViewDetails> productInventoryViewDetailsListForMRN = new ArrayList<ProductInventoryViewDetails>();
				ArrayList<ProductInventoryViewDetails> productInventoryViewDetailsListForPR = new ArrayList<ProductInventoryViewDetails>();

//				List<ProductInventoryView> productInventoryViewList = ofy().load().type(ProductInventoryView.class)
//						.filter("companyId", compEntity.get(i).getCompanyId()).list();
				
				List<ProductInventoryViewDetails> productInventoryViewDetailsList = ofy().load().type(ProductInventoryViewDetails.class).filter("companyId", compEntity.get(i).getCompanyId()).list();
				
				warehouselist = ofy().load().type(WareHouse.class).filter("companyId", compEntity.get(i).getCompanyId()).list();
				
//				for(ProductInventoryView productInventory : productInventoryViewList){
//					for(ProductInventoryViewDetails prodViewDetails : productInventory.getDetails()){
				for(ProductInventoryViewDetails prodViewDetails : productInventoryViewDetailsList){
						if(prodViewDetails.getAvgConsumption()!=0 &&
								prodViewDetails.getAvailableqty()<= prodViewDetails.getReorderlevel()){
							if(isClusterWarehouse(prodViewDetails.getWarehousename()) && checkWarehouseActiveandExpiry(prodViewDetails.getWarehousename())){
								prodViewDetails.setBranchName(getWarehouseBranchName(warehouselist,prodViewDetails));
								productInventoryViewDetailsListForPR.add(prodViewDetails);
							}
							else{
								if(checkWarehouseActiveandExpiry(prodViewDetails.getWarehousename())){
								prodViewDetails.setBranchName(getWarehouseBranchName(warehouselist,prodViewDetails));
								productInventoryViewDetailsListForMRN.add(prodViewDetails);
								}
							}
						}
					}
//				}
				
				/*** Here the logic for check and Create Purchase Requision for Cluster warehouse
				 * which are less than reorder level
				 */
				System.out.println("productInventoryViewDetailsListForPR =="+productInventoryViewDetailsListForPR.size());
				logger.log(Level.SEVERE,"productInventoryViewDetailsListForPR =="+productInventoryViewDetailsListForPR.size());

				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition", "EnableAutomaticPROrderQty", compEntity.get(i).getCompanyId())){
				ArrayList<String> statusList = new ArrayList<String>();
				statusList.add(PurchaseRequisition.CREATED);
				statusList.add(PurchaseRequisition.PROCESSED);
				statusList.add(PurchaseRequisition.REQUESTED);
				statusList.add(PurchaseRequisition.APPROVED);
				
					for(ProductInventoryViewDetails prodDetails : productInventoryViewDetailsListForPR){
							List<PurchaseRequisition>  purchaseReqList = ofy().load().type(PurchaseRequisition.class)
									.filter("prproduct.productID", prodDetails.getProdid())
									.filter("companyId", compEntity.get(i).getCompanyId()).filter("purchasedetails.status IN",statusList)
									.filter("purchasedetails.branch", prodDetails.getBranchName()).list();
							logger.log(Level.SEVERE,"purchaseReqList =="+purchaseReqList.size());
							logger.log(Level.SEVERE,"prproduct.productID"+prodDetails.getProdid());
							logger.log(Level.SEVERE,"purchasedetails.branch"+prodDetails.getBranchName());

							double prQuantity = 0;
							for(PurchaseRequisition purchaseReq : purchaseReqList){
								prQuantity += getPrQuantity(purchaseReq.getPrproduct(),prodDetails.getProdid(),prodDetails.getWarehousename());
							}
							double availableQty = prodDetails.getAvailableqty()+prQuantity;
							logger.log(Level.SEVERE,"availableQty =="+availableQty);
							if(availableQty<=prodDetails.getReorderlevel()){
								if(prodDetails.getAvgConsumption()!=0){
									logger.log(Level.SEVERE,"PR =============");
//								prodDetails.setOrderQty(UpdateStock.getOrderQty(availableQty, prodDetails.getForecastDemand(),
//										prodDetails.getConsumptiontillStockReceived(), prodDetails.getReorderlevel(),
//										prodDetails.getMinOrderQty(), prodDetails.getSafetyStockLevel()));
								
								double orderQty = 	UpdateStock.getOrderQty(availableQty, prodDetails.getForecastDemand(),
										prodDetails.getConsumptiontillStockReceived(), prodDetails.getReorderlevel(),
										prodDetails.getMinOrderQty(), prodDetails.getSafetyStockLevel());
								
								if(orderQty<prodDetails.getMinOrderQty()){
									prodDetails.setOrderQty(prodDetails.getMinOrderQty());
								}
								createPurchaseRequisition(prodDetails,compEntity.get(i).getCompanyId(),compEntity.get(i));

								}
							}
					}
					
				}
					/**
					 * PR Ends here
					 */
					
					/**
					 * Date 26-12-2018 By Vijay
					 * Des :- Creating MRN for warehouses those which are less than Reorder level
					 */
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialRequestNote", "EnableAutomaticMRNOrderQty", compEntity.get(i).getCompanyId())){
					logger.log(Level.SEVERE,"productInventoryViewDetailsListForMRN"+productInventoryViewDetailsListForMRN);
					logger.log(Level.SEVERE,"productInventoryViewDetailsListForMRN"+productInventoryViewDetailsListForMRN.size());

						HashSet<String> productInventoryHashSet = new HashSet<String>();
						for(ProductInventoryViewDetails prodDetails : productInventoryViewDetailsListForMRN){
							productInventoryHashSet.add(prodDetails.getWarehousename().trim()+"$"+prodDetails.getStoragelocation().trim()
									+"$"+prodDetails.getStoragebin().trim());
						}
	
						
					ArrayList<String> mrnStatuslist = new ArrayList<String>();
					mrnStatuslist.add(MaterialRequestNote.APPROVED);
					mrnStatuslist.add(MaterialRequestNote.CREATED);
					mrnStatuslist.add(MaterialRequestNote.REQUESTED);
					
					for(String warehouselocationbin :productInventoryHashSet){
						System.out.println("warehouselocationbin ==="+warehouselocationbin);
						ArrayList<ProductInventoryViewDetails> productinventoryViewDetails = new ArrayList<ProductInventoryViewDetails>();
						for(ProductInventoryViewDetails prodDetails : productInventoryViewDetailsListForMRN){
							if(warehouselocationbin.equals(prodDetails.getWarehousename().trim()+"$"+
															prodDetails.getStoragelocation().trim()+"$"+
															prodDetails.getStoragebin().trim())){
							logger.log(Level.SEVERE, "Product Code"+prodDetails.getProdcode());
								
							List<MaterialRequestNote> materialReqNoteList = ofy().load().type(MaterialRequestNote.class)
									.filter("companyId",compEntity.get(i).getCompanyId())
									.filter("status IN", mrnStatuslist)
									.filter("branch", prodDetails.getBranchName())
									.filter("subProductTableMrn.materialProductCode", prodDetails.getProdcode())
									.filter("subProductTableMrn.materialProductWarehouse", prodDetails.getWarehousename())
									.filter("subProductTableMrn.materialProductStorageLocation", prodDetails.getStoragelocation())
									.filter("subProductTableMrn.materialProductStorageBin", prodDetails.getStoragebin()).list();
							logger.log(Level.SEVERE,"materialReqNoteList size =="+materialReqNoteList.size());
							if(materialReqNoteList.size()==0){
								double mrnQty =0;
								for(MaterialRequestNote materialRequestnote : materialReqNoteList){
										mrnQty += getMRNQty(materialRequestnote.getSubProductTableMrn(),
													prodDetails);
								}
								double availableQty = prodDetails.getAvailableqty()+mrnQty;
								logger.log(Level.SEVERE,"MRN availableQty "+availableQty);
								logger.log(Level.SEVERE,"prodDetails.getReorderlevel()"+prodDetails.getReorderlevel());

								if(availableQty<=prodDetails.getReorderlevel()){
									if(prodDetails.getAvgConsumption()!=0){
										logger.log(Level.SEVERE,"Creating inventory MRN before method call");
										prodDetails.setOrderQty(UpdateStock.getOrderQty(availableQty, prodDetails.getForecastDemand(),
												prodDetails.getConsumptiontillStockReceived(), prodDetails.getReorderlevel(),
												prodDetails.getMinOrderQty(), prodDetails.getSafetyStockLevel()));
										logger.log(Level.SEVERE, "prodDetails ==="+prodDetails.getProdcode());
										productinventoryViewDetails.add(prodDetails);
									}
								}
							}
								
							}
						}
						if(productinventoryViewDetails.size()!=0){
							logger.log(Level.SEVERE,"Creating inventory MRN");
						createMRN(productinventoryViewDetails,compEntity.get(i).getCompanyId(),compEntity.get(i));
						}
					}
				}
			}
			
		   }

	   }
	}


	private void createMRN(ArrayList<ProductInventoryViewDetails> productinventoryViewDetails, Long companyId, Company company) {
		
		MaterialRequestNote mrn = new MaterialRequestNote();
		mrn.setMrnDate(new Date());
		mrn.setMrnRequiredDate(new Date()); 
		mrn.setCompanyId(companyId);
		mrn.setBranch(productinventoryViewDetails.get(0).getBranchName());
		mrn.setStatus(MaterialRequestNote.CREATED);
		ArrayList<MaterialProduct> materialProductlist = new ArrayList<MaterialProduct>();

		for(ProductInventoryViewDetails prodInventoryDetails : productinventoryViewDetails){
			MaterialProduct materialProduct = new MaterialProduct();
			materialProduct.setMaterialProductId(prodInventoryDetails.getProdid());
			materialProduct.setMaterialProductName(prodInventoryDetails.getProdname());
			logger.log(Level.SEVERE, "prodDetails $$$$"+prodInventoryDetails.getProdcode());
			materialProduct.setMaterialProductCode(prodInventoryDetails.getProdcode());
			materialProduct.setMaterialProductAvailableQuantity(prodInventoryDetails.getAvailableqty());
			materialProduct.setMaterialProductRequiredQuantity(prodInventoryDetails.getOrderQty());
			materialProduct.setMaterialProductWarehouse(prodInventoryDetails.getWarehousename());
			materialProduct.setMaterialProductStorageLocation(prodInventoryDetails.getStoragelocation());
			materialProduct.setMaterialProductStorageBin(prodInventoryDetails.getStoragebin());
			materialProduct.setMaterialProductRemarks("");
			materialProduct.setMaterialProductBalanceQty(materialProduct.getMaterialProductRequiredQuantity());
			ItemProduct product = ofy().load().type(ItemProduct.class)
						.filter("companyId", companyId)
						.filter("count", prodInventoryDetails.getProdid()).first().now();
			if(product!=null){
				materialProduct.setMaterialProductUOM(product.getUnitOfMeasurement());
			}
			materialProductlist.add(materialProduct);
		}
		
		
		if(materialProductlist.size()!=0)
		mrn.setSubProductTableMrn(materialProductlist);
		GenricServiceImpl genImpl = new GenricServiceImpl();
		ReturnFromServer server = genImpl.save(mrn);
		
		logger.log(Level.SEVERE, "MRN Saved Successfully");
		
		sendAutoMRNEmail(productinventoryViewDetails,companyId,server.count,mrn,company,"");
		
	}

	

	private double getMRNQty(ArrayList<MaterialProduct> subProductTableMrn,
			ProductInventoryViewDetails prodDetails) {
		double materialQty =0;
		for(MaterialProduct materialProduct : subProductTableMrn){
			if(materialProduct.getMaterialProductId()==prodDetails.getProdid()
					&& materialProduct.getMaterialProductWarehouse().trim().
					equals(prodDetails.getWarehousename().trim())
					&& materialProduct.getMaterialProductStorageLocation().trim().
					equals(prodDetails.getStoragelocation().trim())
					&& materialProduct.getMaterialProductStorageBin().trim().
					equals(prodDetails.getStoragebin().trim())){
				materialQty += materialProduct.getMaterialProductBalanceQty();
			}
		}
		
		return materialQty;
	}

	private boolean isClusterWarehouse(String warehouseName) {
		for(WareHouse warehouse : warehouselist){
			if(warehouse.getstatus() && warehouse.getWarehouseExpiryDate() !=null && (new Date().before(warehouse.getWarehouseExpiryDate())
					 || new Date().equals(warehouse.getWarehouseExpiryDate()))
					&& warehouse.getBusinessUnitName().trim().equals(warehouseName.trim())){
				if(warehouse.getWarehouseType().trim().equalsIgnoreCase("CLUSTER OFFICE") 
					&& warehouse.getParentWarehouse().trim().equalsIgnoreCase("NBHC HO")){
					return true;
				}
			}
		}
		return false;
	}

	private boolean checkWarehouseActiveandExpiry(String warehouseName) {
		logger.log(Level.SEVERE, "warehouseName for warehouse active expiry=="+warehouseName);
		for(WareHouse warehouse : warehouselist){
			if(warehouseName.trim().equals(warehouse.getBusinessUnitName().trim())
				&& warehouse.getstatus() && warehouse.getWarehouseExpiryDate()!=null 
				&& (new Date().before(warehouse.getWarehouseExpiryDate())
					|| new Date().equals(warehouse.getWarehouseExpiryDate()))){
				logger.log(Level.SEVERE, "OK =================="+warehouseName);
				return true;
			}
		}
		return false;
	}

	private String getWarehouseBranchName(List<WareHouse> warehouselist,
			ProductInventoryViewDetails prodViewDetails) {

		for(WareHouse warehouse :warehouselist){
			if(warehouse.getBusinessUnitName().trim().equals(prodViewDetails.getWarehousename().trim())){
				if(warehouse.getBranchdetails()!=null)
				return warehouse.getBranchdetails().get(0).getBranchName();
			}
		}
		return null;
	}

	private void createPurchaseRequisition(ProductInventoryViewDetails prodDetails, Long companyId, Company company) {
		logger.log(Level.SEVERE,"inside pr creation method ");

		PurchaseRequisition purchaseRequisition = new PurchaseRequisition();
		purchaseRequisition.setCompanyId(companyId);
		purchaseRequisition.setStatus(PurchaseRequisition.CREATED);
		purchaseRequisition.setAutomaticPRFlag(true);
		purchaseRequisition.setCreationDate(new Date());
		purchaseRequisition.setBranch(prodDetails.getBranchName());
		
		ArrayList<ProductDetails> productlist = new ArrayList<ProductDetails>();
			ItemProduct product = ofy().load().type(ItemProduct.class)
					.filter("companyId", companyId)
					.filter("count", prodDetails.getProdid()).first().now();
			logger.log(Level.SEVERE, "product"+product);

			if(product!=null){
			productlist.add(getproductInfo(product,prodDetails.getOrderQty(),
					prodDetails.getWarehousename(),prodDetails.getAvailableqty(),prodDetails.getLeadDays()));
			}
		if(productlist.size()!=0)	
		purchaseRequisition.setPrproduct(productlist);
		if(productlist.size()==1  && productlist.get(0).getProdDate()!=null){
			purchaseRequisition.setExpectedDeliveryDate(productlist.get(0).getProdDate());
		}
		logger.log(Level.SEVERE, "Befor Saving PR"+productlist.size());
		
		
		
		
		if(productlist.size()!=0){
			String approvarName = getApprovarName(companyId);
			if(approvarName!=null)
				purchaseRequisition.setApproverName(approvarName);
			
			String purchaseEngineer = getPurchaseEngineer(companyId);
			if(purchaseEngineer!=null){
				purchaseRequisition.setEmployee(purchaseEngineer);
			}
			
		GenricServiceImpl genricImpl = new GenricServiceImpl();
		ReturnFromServer server = genricImpl.save(purchaseRequisition);
		
		ArrayList<ProductInventoryViewDetails> prodDetailslist = new ArrayList<ProductInventoryViewDetails>();
		prodDetailslist.add(prodDetails);
		sendEmail(prodDetailslist,companyId,purchaseEngineer,server.count,purchaseRequisition,company,"","",null,null);
		
//		sendApprovalRequest(prodDetails.getBranchName(),server.count,companyId,approvarName,purchaseEngineer);
		}
	}

	public void sendEmail(ArrayList<ProductInventoryViewDetails> prodDetailslist, Long companyId, String purchaseEngineer, int count, PurchaseRequisition purchaseRequisition, 
			Company company, String approverName, String approvalStatusTitle, ArrayList<String> tableHeader, ArrayList<String> table1) {
		ArrayList<String> proddetailsBrnach = new ArrayList<String>();
		for(ProductInventoryViewDetails prodDetails : prodDetailslist){
			System.out.println("prodDetails.getBranchName() =="+prodDetails.getBranchName());
			proddetailsBrnach.add(prodDetails.getBranchName());
		}
		System.out.println("proddetailsBrnach =="+proddetailsBrnach);
		System.out.println(companyId);
//		List<Employee> employeelist = ofy().load().type(Employee.class).filter("branchName IN", proddetailsBrnach).filter("companyId", companyId)
//						.filter("roleName", "BO").filter("designation", "BACK OFFICE ASSISTANT").list();
		List<Employee> employeelist = ofy().load().type(Employee.class).filter("branchName IN", proddetailsBrnach).filter("companyId", companyId).list();
		ArrayList<String> toEmailList1=new ArrayList<String>();
		System.out.println("employeelist =="+employeelist.size());
		for(int i=0;i<employeelist.size();i++){
			toEmailList1.add(employeelist.get(i).getEmail());
		}
		
		
		String	msgBody ="</br></br> Dear Sir,"
				+ "<br><br>"
				+ "Following PR is raised automatically and waiting at your level for process/cancel "
				+ "<br><br>";

		
		String mailSubject="Subject : PR Request No."+count +"has been assigned to "+approverName +" for Approval";
		
		//Table header 1
		ArrayList<String> tbl1_header=new ArrayList<String>();
		tbl1_header.add("PR NO");
		tbl1_header.add("PR DATE");
		System.out.println("Tbl1 header Size :"+tbl1_header.size());
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		// Table 1
		ArrayList<String> tbl1=new ArrayList<String>();
		tbl1.add(count+"");
		tbl1.add(dateFormat.format(new Date()));
		
		String prTitle = "PR Details";
		
		//Table header 2
		ArrayList<String> tbl2_header=new ArrayList<String>();
		tbl2_header.add("Product");
		tbl2_header.add("Description");
		tbl2_header.add("Cluster");
		tbl2_header.add("Delivery Location");
		tbl2_header.add("Delivery Address");
		tbl2_header.add("Automatic / Manual");
		tbl2_header.add("Order Quantity");
		tbl2_header.add("Rate per Piece");
		tbl2_header.add("Total Value");
		tbl2_header.add("Stock in hand");
		tbl2_header.add("Forecast");
		tbl2_header.add("Safety Stock");
		tbl2_header.add("Reordering Level");
		tbl2_header.add("Min Order Qty");
		// Table 2
		ArrayList<String> tbl2=new ArrayList<String>();
		
		for(ProductDetails prProdDetail : purchaseRequisition.getPrproduct()){
			for(ProductInventoryViewDetails prodDetails : prodDetailslist){
				if(prodDetails.getWarehousename().trim().equalsIgnoreCase(prProdDetail.getWarehouseName().trim())){
					tbl2.add(prProdDetail.getProductName());
					tbl2.add(purchaseRequisition.getDescription());
					tbl2.add(prProdDetail.getWarehouseName());
					tbl2.add(prProdDetail.getWarehouseName());
					tbl2.add(prProdDetail.getWarehouseAddress());
					tbl2.add("Automatic");
					tbl2.add(prProdDetail.getProductQuantity()+"");
					tbl2.add(prProdDetail.getProdPrice()+"");
					tbl2.add(prProdDetail.getTotal()+"");
					tbl2.add(prodDetails.getAvailableqty()+"");
					tbl2.add(prodDetails.getForecastDemand()+"");
					tbl2.add(prodDetails.getSafetyStockLevel()+"");
					tbl2.add(prodDetails.getReorderlevel()+"");
					tbl2.add(prodDetails.getMinOrderQty()+"");
				}
			}
			
		}
				
		Email email = new Email();
			email.htmlformatsendEmail(toEmailList1, mailSubject, "", company, msgBody, null, null, tbl1_header, tbl1,
					prTitle,tbl2_header,tbl2,approvalStatusTitle,tableHeader,table1);
			logger.log(Level.SEVERE,"After send mail method ");		

	}

	private void sendApprovalRequest(String branch, int count, Long companyId, String approvarName, String purchaseEngineer) {
		Approvals approval=new Approvals();
		if(approvarName!=null)
		approval.setApproverName(approvarName);
		approval.setRequestedBy(approvarName);
		approval.setBranchname(branch);
		approval.setBusinessprocessId(count);
		if(UserConfiguration.getInfo()!=null && !UserConfiguration.getInfo().getFullName().equals("")){
			approval.setRequestedBy(UserConfiguration.getInfo().getFullName());
		}
		// here i am setting person responsible to send email in cc for person responsible
		if(purchaseEngineer!=null){
			approval.setPersonResponsible(purchaseEngineer);
		}
		approval.setApprovalLevel(1);
		approval.setStatus(Approvals.PENDING);
		approval.setBusinessprocesstype("Purchase Requisition");
		approval.setDocumentValidation(false);
		approval.setCompanyId(companyId);
		
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(approval);
	
	}

	private String getPurchaseEngineer(Long companyId) {
		RoleDefinition roleDefinition = ofy().load().type(RoleDefinition.class)
											.filter("moduleName", "Purchase").filter("documentName", "Purchase Requisition")
											.filter("roleType", "Purchase Engineer").filter("status", true).first().now();
		if(roleDefinition!=null){
			String employeeRole =  roleDefinition.getRoleList().get(0);
			Employee employee = ofy().load().type(Employee.class).filter("companyId", companyId)
								.filter("roleName", employeeRole).first().now();
			if(employee!=null){
				return employee.getFullName();
			}
		}
		return null;
	}

	private String getApprovarName(Long companyId) {
		
		List<MultilevelApproval> multilevelApprovallist = ofy().load().type(MultilevelApproval.class)
									.filter("companyId", companyId).filter("docStatus", true).list();
		if(multilevelApprovallist.size()!=0){
			for(MultilevelApproval multiApproval : multilevelApprovallist){
				for(MultilevelApprovalDetails approvalDetails : multiApproval.getApprovalLevelDetails()){
					if(approvalDetails.getDocumentName().trim().equals("Purchase Requisition")
							&& approvalDetails.getLevel().equals("1")){
						return approvalDetails.getEmployeeName();
					}
				}
			}
			
		}
		
		return null;
	}

	private ProductDetails getproductInfo(ItemProduct product, double orderQty, String warehouseName, double availableQty, int leadDays) {
			SalesLineItem lineItem=new SalesLineItem();
			ProductDetails poproducts=new ProductDetails();
			
				product.setTermsAndConditions(new DocumentUpload());
				product.setProductImage(new DocumentUpload());
				product.setProductImage1(new DocumentUpload());
				product.setProductImage2(new DocumentUpload());
				product.setProductImage3(new DocumentUpload());
				
				lineItem.setPrduct(product);
				poproducts.setProductID(lineItem.getPrduct().getCount());
				poproducts.setProductCategory(lineItem.getProductCategory());
				poproducts.setProductCode(lineItem.getProductCode());
				poproducts.setProductName(lineItem.getProductName());
				poproducts.setUnitOfmeasurement(lineItem.getUnitOfMeasurement());
				
				poproducts.setProdPrice(product.getPurchasePrice());
				poproducts.setServiceTax(product.getPurchaseTax1().getPercentage());
				poproducts.setVat(product.getPurchaseTax2().getPercentage());
				
				poproducts.setProductQuantity(orderQty);
				poproducts.setGrnBalancedQty(orderQty);
				poproducts.setWarehouseName(warehouseName);
				poproducts.setAvailableStock(availableQty);
				poproducts.setWarehouseAddress(getWarehouseAddress(warehouseName));
				poproducts.setProdDate(getDate(leadDays));
				
				poproducts.setTotal(product.getPurchasePrice()*orderQty);
				poproducts.setPrQuantity(orderQty);

				poproducts.setPrduct(product);
				
			return poproducts;
	}

	private Date getDate(int leadDays) {
		
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		Calendar cal=Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, +leadDays);
		
		Date date=null;
		
		try {
			date=dateFormat.parse(dateFormat.format(cal.getTime()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		logger.log(Level.SEVERE,"Today Date +  Days Date"+date);
		return date;
	}

	private String getWarehouseAddress(String warehouseName) {
		if(warehouselist.size()!=0){
		for(WareHouse warehouse : warehouselist){
			if(warehouse.getBusinessUnitName().trim().equals(warehouseName.trim())){
				return warehouse.getAddress().getCompleteAddress();
			}
		}
		}
		return "";
	}

	private double getPrQuantity(ArrayList<ProductDetails> prproduct, int prodId, String warehouseName) {
		double prQuantity = 0;
		for(ProductDetails proddetails : prproduct){
			if(proddetails.getProductID()==prodId 
					&& proddetails.getWarehouseName().trim().equals(warehouseName.trim())){
				if(proddetails.getGrnQtyReceived()>0){
					prQuantity += proddetails.getPoQty()-proddetails.getGrnQtyReceived();
				}
				else if(proddetails.getPoQty()>0){
					prQuantity += proddetails.getPoQty();
				}
				else{
//					prQuantity +=proddetails.getProductQuantity();
					prQuantity +=proddetails.getPrQuantity();
	
				}
			}
		}
		System.out.println("prQuantity ==="+prQuantity);
		return prQuantity;
	}
	
	private void sendAutoMRNEmail(ArrayList<ProductInventoryViewDetails> prodDetailslist, Long companyId,int count, MaterialRequestNote mrn, Company company, String approverName) {

//		List<Employee> employeelist = ofy().load().type(Employee.class).filter("branchName", mrn.getBranch()).filter("companyId", companyId)
//						.filter("roleName", "BO").filter("designation", "BACK OFFICE ASSISTANT").list();
		
		List<Employee> employeelist = ofy().load().type(Employee.class).filter("branchName", mrn.getBranch()).filter("companyId", companyId).list();
		
		ArrayList<String> toEmailList1=new ArrayList<String>();
		System.out.println("employeelist =="+employeelist.size());
		for(int i=0;i<employeelist.size();i++){
			toEmailList1.add(employeelist.get(i).getEmail());
		}
		
		String	msgBody ="</br></br> Dear Sir,"
				+ "<br><br>"
				+ "Following MRN is raised automatically and waiting at your level for process/cancel "
				+ "<br><br>";

		
		String mailSubject="Subject : MRN Request No."+count +"has been assigned to "+approverName +" for Approval";
		
		//Table header 1
		ArrayList<String> tbl1_header=new ArrayList<String>();
		tbl1_header.add("MRN NO");
		tbl1_header.add("MRN DATE");
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		// Table 1
		ArrayList<String> tbl1=new ArrayList<String>();
		tbl1.add(count+"");
		tbl1.add(dateFormat.format(new Date()));
		
		String prTitle = "MRN Details";
		
		//Table header 2
		ArrayList<String> tbl2_header=new ArrayList<String>();
		tbl2_header.add("Product");
		tbl2_header.add("Description");
		tbl2_header.add("Cluster");
		tbl2_header.add("Delivery Location");
		tbl2_header.add("Delivery Address");
		tbl2_header.add("Automatic / Manual");
		tbl2_header.add("Order Quantity");
		tbl2_header.add("Stock in hand");
		tbl2_header.add("Forecast");
		tbl2_header.add("Safety Stock");
		tbl2_header.add("Reordering Level");
		tbl2_header.add("Min Order Qty");
		// Table 2
		ArrayList<String> tbl2=new ArrayList<String>();
		
		
		
		for(MaterialProduct mrnProdDetail : mrn.getSubProductTableMrn()){
			for(ProductInventoryViewDetails prodDetails : prodDetailslist){
				if(prodDetails.getWarehousename().trim().equalsIgnoreCase(mrnProdDetail.getMaterialProductWarehouse().trim())){
					tbl2.add(mrnProdDetail.getMaterialProductName());
					if(mrn.getMrnDescription()!=null){
						tbl2.add(mrn.getMrnDescription());
					}
					else{
						tbl2.add("");
					}
					tbl2.add(mrn.getBranch());
					tbl2.add(mrnProdDetail.getMaterialProductWarehouse());
					WareHouse warehouseEntity = ofy().load().type(WareHouse.class).filter("companyId", companyId)
							.filter("BusinessUnit", mrnProdDetail.getMaterialProductWarehouse()).first().now();
					if(warehouseEntity!=null){
						tbl2.add(warehouseEntity.getAddress().getCompleteAddress());
					}
					else{
						tbl2.add("");

					}
					tbl2.add("Automatic");
					tbl2.add(mrnProdDetail.getMaterialProductRequiredQuantity()+"");
					tbl2.add(prodDetails.getAvailableqty()+"");
					tbl2.add(prodDetails.getForecastDemand()+"");
					tbl2.add(prodDetails.getSafetyStockLevel()+"");
					tbl2.add(prodDetails.getReorderlevel()+"");
					tbl2.add(prodDetails.getMinOrderQty()+"");
				}
			}
			
		}
				
		Email email = new Email();
			email.htmlformatsendEmail(toEmailList1, mailSubject, "", company, msgBody, null, null, tbl1_header, tbl1,
					prTitle,tbl2_header,tbl2,"",null,null);
			logger.log(Level.SEVERE,"After send mail method ");		

	
	}
	
}
