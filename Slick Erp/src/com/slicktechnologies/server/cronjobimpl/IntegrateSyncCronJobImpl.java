package com.slicktechnologies.server.cronjobimpl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

public class IntegrateSyncCronJobImpl extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3989925043656250064L;

Logger logger = Logger.getLogger("IntegrateSyncServiceImpl.class");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		List<String> statusList=new ArrayList<String>();
		statusList.add(AccountingInterface.TALLYCREATED);
		statusList.add(AccountingInterface.FAILED);
		
		List<AccountingInterface> accountingInterfaceList=ofy().load().type(AccountingInterface.class).filter("status IN",statusList).list();
		
		for (int i = 0; i < accountingInterfaceList.size(); i++) {
			AccountingInterface accInterface=new AccountingInterface();
			accInterface=accountingInterfaceList.get(i);
			String response=null;
			response=callInterFace(accInterface.getDocumentType().trim(), accInterface);
			
			if(response.toString().contains("1")&&response.toString().contains("Added Successfully")){
				accInterface.setRemark("Added Successfully.");
				accInterface.setStatus(AccountingInterface.TALLYSYNCED);
				
			}else if(response.toString().contains("0")){
				accInterface.setRemark("Error while adding data.");
				accInterface.setStatus(AccountingInterface.FAILED);
			}else if(response.toString().contains("-1")){
				accInterface.setRemark("Error while adding data.");
				accInterface.setStatus(AccountingInterface.FAILED);
			}else if(response.toString().contains("not Available")){
				accInterface.setRemark(response);
			}else{
				accInterface.setRemark("Unexpected Error");
				accInterface.setStatus(AccountingInterface.FAILED);
			}
			
			accInterface.setDateofSynch(new Date());
			accInterface.setSynchedBy("Auto Synched");
			
			ofy().save().entity(accInterface);
			
		}
		
	}
	
	public String callInterFace(String docType, AccountingInterface entityAI) {
		// TODO Auto-generated method stub
		try{
			String response="";
			if(docType.trim().equalsIgnoreCase("Contract")){
				response=callNBHCSalesOrder(entityAI);
			}else if(docType.trim().equalsIgnoreCase("Invoice")){
				response=callNBHCINVOICE(entityAI);
			}else{
				response="Interface of Document Type "+docType+" is not Available";
			}
			
			return response;
		}catch(Exception e){
			e.printStackTrace();
			return "Error::::::::: "+e;
		}
		
	}
	
	

public String callNBHCSalesOrder(AccountingInterface entityAI) {
	// TODO Auto-generated method stub
	final String SALESORDER=AppConstants.NBHCSALESORDERINTEGRATE;
	String data="";
	
	URL url = null;
	try {
		url = new URL(SALESORDER);
	} catch (MalformedURLException e) {
		// TODO Auto-generated catch block
		logger.log(Level.SEVERE,"MalformedURLException ERROR::::::::::"+e);
		e.printStackTrace();
	}
    HttpURLConnection con = null;
	try {
		con = (HttpURLConnection) url.openConnection();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
		e.printStackTrace();
	}
    con.setDoInput(true);
    con.setDoOutput(true);
    con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
    try {
		con.setRequestMethod("POST");
	} catch (ProtocolException e) {
		// TODO Auto-generated catch block
		logger.log(Level.SEVERE,"ProtocolException ERROR::::::::::"+e);
		e.printStackTrace();
	}
    OutputStream os = null;
	try {
		os = con.getOutputStream();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
		e.printStackTrace();
	}
    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
//    Log.d("Test", "From Get Post Method" + getPostData(values));
    try {
		writer.write(createSOJSONObject(entityAI));
	} catch (IOException e) {
		// TODO Auto-generated catch block
		logger.log(Level.SEVERE,"IOException2 ERROR::::::::::"+e);
		e.printStackTrace();
	}
    try {
		writer.flush();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		logger.log(Level.SEVERE,"IOException3 ERROR::::::::::"+e);
		e.printStackTrace();
	}
    
    InputStream is = null;
	try {
		is = con.getInputStream();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		logger.log(Level.SEVERE,"IOException4 ERROR::::::::::"+e);
		e.printStackTrace();
	}
    BufferedReader br = new BufferedReader(
            new InputStreamReader(is));
    String temp;

    try {
		while ((temp = br.readLine()) != null) {
		    data = data + temp;
		}
		logger.log(Level.SEVERE,"data data::::::::::"+data);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		logger.log(Level.SEVERE,"IOException5 ERROR::::::::::"+e);
		e.printStackTrace();
	}
    
    logger.log(Level.SEVERE,"Data::::::::::"+data);
    return data;
}

private String createSOJSONObject(AccountingInterface entityAI) {
	// TODO Auto-generated method stub
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
	sdf.setTimeZone(TimeZone.getTimeZone("IST"));
	TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	  
	JSONObject jObj=new JSONObject();
	jObj.put("SODocCode", entityAI.getDocumentID());
	jObj.put("CustomerCode", entityAI.getCustomerRefId());
	jObj.put("CondValue", entityAI.getNetPayable());
	jObj.put("Salesoffice", entityAI.getBranch().trim());
	jObj.put("ActionTask", "SalesOrder");
	jObj.put("EVA_IfId", entityAI.getCount()+"");
	jObj.put("EVA_DocID", entityAI.getDocumentID()+"");
	jObj.put("EVA_DocDate", sdf.format(entityAI.getDocumentDate()));
	jObj.put("EVA_ContractCategory", entityAI.getConractCategory());
	String textProdName=null;
	List<AccountingInterface> accountingInterfaceList=ofy().load().type(AccountingInterface.class).filter("companyId", entityAI.getCompanyId()).filter("documentType","Contract").filter("documentID", entityAI.getDocumentID()).list();
	int servicesInContract=0;
	for (int i = 0; i < accountingInterfaceList.size(); i++) {
		servicesInContract=servicesInContract+accountingInterfaceList.get(i).getProdServices();
		if(textProdName!=null){
			textProdName=textProdName+","+accountingInterfaceList.get(i).getProductName();
		}else{
			textProdName=accountingInterfaceList.get(i).getProductName();
		}
	}
	jObj.put("Eva_Services", servicesInContract);
	jObj.put("Text", textProdName);
	
	String jsonString=jObj.toJSONString().replaceAll("\\\\", "");
	logger.log(Level.SEVERE,"JSON SO DATA:::"+jsonString);
	return jsonString;
}

	
public String callNBHCINVOICE(AccountingInterface entityAI){
		logger.log(Level.SEVERE,"This is where i will call invoice URL");
		final String INVOICEURL=AppConstants.NBHCINVOICEINTEGRATE;
		String data="";
		
		URL url = null;
		try {
			url = new URL(INVOICEURL);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"MalformedURLException ERROR::::::::::"+e);
			e.printStackTrace();
		}
        HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
			e.printStackTrace();
		}
        con.setDoInput(true);
        con.setDoOutput(true);
        con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        try {
			con.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"ProtocolException ERROR::::::::::"+e);
			e.printStackTrace();
		}
        OutputStream os = null;
		try {
			os = con.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
			e.printStackTrace();
		}
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
//        Log.d("Test", "From Get Post Method" + getPostData(values));
        try {
			writer.write(createJSONObject(entityAI));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"IOException2 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        try {
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"IOException3 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        
        InputStream is = null;
		try {
			is = con.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"IOException4 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        BufferedReader br = new BufferedReader(
                new InputStreamReader(is));
        String temp;

        try {
			while ((temp = br.readLine()) != null) {
			    data = data + temp;
			}
			logger.log(Level.SEVERE,"data data::::::::::"+data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"IOException5 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        
        logger.log(Level.SEVERE,"Data::::::::::"+data);
		
		return data;
	}
	
private String createJSONObject(AccountingInterface entityAI) {
	// TODO Auto-generated method stub
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
	sdf.setTimeZone(TimeZone.getTimeZone("IST"));
	TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	  
	JSONObject jObj=new JSONObject();
	jObj.put("DocumentNo", entityAI.getDocumentID());
	jObj.put("BillDate", sdf.format(entityAI.getDocumentDate()));
	jObj.put("SoldTo", entityAI.getCustomerRefId()); //Customer Reference Id
	jObj.put("RefDoc", entityAI.getReferenceDocumentNo1());
	jObj.put("CondValue", entityAI.getNetPayable());
	jObj.put("ActionTask","InvoiceBillData" );
	jObj.put("EVA_IfId", entityAI.getCount());
	jObj.put("EVA_DocID", entityAI.getDocumentID());
	jObj.put("EVA_DocDate", sdf.format(entityAI.getDocumentDate()));
	
	
	List<AccountingInterface> accountingInterfaceList=ofy().load().type(AccountingInterface.class).filter("companyId", entityAI.getCompanyId()).filter("documentType","Invoice").filter("documentID", entityAI.getDocumentID()).list();
	int servicesInInvoice=0;
	for (int i = 0; i < accountingInterfaceList.size(); i++) {
		servicesInInvoice=servicesInInvoice+accountingInterfaceList.get(i).getProdServices();
		
	}
	jObj.put("Eva_Services", servicesInInvoice);
	
	
	String jsonString=jObj.toJSONString().replaceAll("\\\\", "");
	
	return jsonString;
}
}
