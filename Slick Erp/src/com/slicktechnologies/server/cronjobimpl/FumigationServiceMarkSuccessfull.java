package com.slicktechnologies.server.cronjobimpl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gwt.i18n.shared.DateTimeFormat;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.android.ReportedServiceDetailsSaveServlet;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.server.webservices.CreateServiceServlet;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

/**
 * 
 * @author Vijay Chougule
 * Des :- NBHC CCPM Fumigation completed services  
 *
 */
public class FumigationServiceMarkSuccessfull extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3286741112482965501L;

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	 Logger logger = Logger.getLogger("NameOfYourLogger");

	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		Date today=DateUtility.getDateWithTimeZone("IST", new Date());
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		
		logger.log(Level.SEVERE,"Date Before Adding One Day"+today);
		Calendar cal=Calendar.getInstance();
		cal.setTime(today);
		cal.add(Calendar.DATE, -3);
		
		Date minusThreeDaysDateWithZero=null;
		
		try {
			minusThreeDaysDateWithZero=dateFormat.parse(dateFormat.format(cal.getTime()));
			minusThreeDaysDateWithZero=cal.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		logger.log(Level.SEVERE,"Today Date -3 Days Date with time Zero "+minusThreeDaysDateWithZero);
		
		logger.log(Level.SEVERE,"Date Before Adding One Day"+today);
		Calendar cal2=Calendar.getInstance();
		cal2.setTime(today);
		cal2.add(Calendar.DATE, -3);
		
		Date minusThreeDaysDate=null;
		
		try {
			minusThreeDaysDate=dateFormat.parse(dateFormat.format(cal2.getTime()));
			cal2.set(Calendar.HOUR_OF_DAY,23);
			cal2.set(Calendar.MINUTE,59);
			cal2.set(Calendar.SECOND,59);
			cal2.set(Calendar.MILLISECOND,999);
			minusThreeDaysDate=cal2.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		logger.log(Level.SEVERE,"Today Date -3 Days Date "+minusThreeDaysDate);
		logger.log(Level.SEVERE,"Today Date "+today);
		
		List<Company> compEntity = ofy().load().type(Company.class).list();
		
		
		if(compEntity.size()>0){
			for(Company company : compEntity){
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableUpdateDegaasignService", company.getCompanyId())){
					updateDegassingServices(company); // for index updation on serviceCompletionDate
				}
				else{
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableNewFumigationServiceForOldRecords", company.getCompanyId())){
						createforOldRecords(company,minusThreeDaysDate);
					}
					else{
						
						try {
							
							if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableMarkSuccessfullWmsFumigationService", company.getCompanyId())){
								List<Service> deggasingServicelist = ofy().load().type(Service.class).filter("wmsServiceFlag", true).filter("status", Service.SERVICESTATUSCOMPLETED)
															.filter("companyId", company.getCompanyId()).filter("product.productCode", "DSSG")
															.filter("serviceCompletionDate >=", minusThreeDaysDateWithZero)
															.filter("serviceCompletionDate <=", minusThreeDaysDate).list();
								logger.log(Level.SEVERE, "deggasingServicelist "+deggasingServicelist.size());
								
								HashSet<Integer> hsfumigationId = new HashSet<Integer>();
								
								for(Service service : deggasingServicelist){
									hsfumigationId.add(service.getFumigationServiceId());
								}
								logger.log(Level.SEVERE, "hsfumigationId size"+hsfumigationId.size());

								if(hsfumigationId.size()>0){
									ArrayList<Integer> fumigationServiceId = new ArrayList<Integer>(hsfumigationId);
									
									List<Service> fumigationServiceList = ofy().load().type(Service.class)
													.filter("count IN", fumigationServiceId)
													.filter("companyId", company.getCompanyId()).list();
									
									logger.log(Level.SEVERE, "fumigationServiceList "+fumigationServiceList.size());
									CreateServiceServlet servicesucessfull = new CreateServiceServlet();
									for(Service serviceEntity : fumigationServiceList){
										logger.log(Level.SEVERE, "serviceEntity.getFumigationStatus "+serviceEntity.getFumigationStatus() +"-"+
														serviceEntity.getCount());
										if(serviceEntity.isWmsServiceFlag() && serviceEntity.getStatus().equals(Service.SERVICESTATUSCOMPLETED)
												&& !serviceEntity.getFumigationStatus().equals(Service.SERVICESTATUSSUCCESS)){
											logger.log(Level.SEVERE, "Fumigation Service Status  "+serviceEntity.getFumigationStatus());

											try {
												if(!serviceEntity.getFumigationStatus().equals(Service.SERVICESTATUSFAILURE)) {
													String response = servicesucessfull.createFumigationAndProphalacticService(serviceEntity,null,null,true);
													logger.log(Level.SEVERE, "response for automatic fumigation successful", response);
												}
											} catch (Exception e2) {
												e2.printStackTrace();
												sendEmailAutoSucessFumigationServiceCreationFailure(serviceEntity,"Auto Sucess");

											}
											

											
										}
									}
								}
								
								
							}
							
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						
					}
						
					
				}

				
			}
		}	
		
	}




	




	private void createforOldRecords(Company company, Date minusThreeDaysDate) {
		logger.log(Level.SEVERE, "For Old Services ");

		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableMarkSuccessfullWmsFumigationService", company.getCompanyId())){
			
			SimpleDateFormat smpDate = new SimpleDateFormat("dd/MM/yyyy");

			ServerAppUtility serverapp = new ServerAppUtility();
			String degassingStartDate = serverapp.getForProcessConfigurartionIsActiveOrNot("DegassingServiceStartDate", company.getCompanyId());
			String degassingEndDate = serverapp.getForProcessConfigurartionIsActiveOrNot("DegassingServiceEndDate", company.getCompanyId());
			Date startDate = null;
			try {
				startDate = smpDate.parse(degassingStartDate);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Date endDate = null;
			try {
				endDate = smpDate.parse(degassingEndDate);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			logger.log(Level.SEVERE, "startDate "+startDate);
			logger.log(Level.SEVERE, "endDate "+endDate);
	
			Calendar cal=Calendar.getInstance();
			cal.setTime(endDate);
			cal.add(Calendar.DATE, 0);
			
			Date deggasingEndDate=null;
			try {
			
			deggasingEndDate=smpDate.parse(smpDate.format(cal.getTime()));
			cal.set(Calendar.HOUR_OF_DAY,23);
			cal.set(Calendar.MINUTE,59);
			cal.set(Calendar.SECOND,59);
			cal.set(Calendar.MILLISECOND,999);
			deggasingEndDate=cal.getTime();
			} catch (Exception e) {
				// TODO: handle exception
			}
			logger.log(Level.SEVERE, "deggasingEndDate "+deggasingEndDate);

			List<Service> deggasingServicelist = ofy().load().type(Service.class).filter("wmsServiceFlag", true).filter("status", Service.SERVICESTATUSCOMPLETED)
					.filter("companyId", company.getCompanyId()).filter("product.productCode", "DSSG")
					.filter("serviceCompletionDate >=", startDate).filter("serviceCompletionDate <=", deggasingEndDate).list();
			logger.log(Level.SEVERE, "deggasingServicelist "+deggasingServicelist.size());

			HashSet<Integer> hsfumigationId = new HashSet<Integer>();
			
			for(Service service : deggasingServicelist){
				hsfumigationId.add(service.getFumigationServiceId());
			}
			
			ArrayList<Integer> fumigationServiceId = new ArrayList<Integer>(hsfumigationId);
			
			List<Service> fumigationServiceList = ofy().load().type(Service.class)
							.filter("count IN", fumigationServiceId)
							.filter("companyId", company.getCompanyId()).list();
			
			logger.log(Level.SEVERE, "fumigationServiceList "+fumigationServiceList.size());
			CreateServiceServlet servicesucessfull = new CreateServiceServlet();
			for(Service serviceEntity : fumigationServiceList){
				logger.log(Level.SEVERE, "serviceEntity.getFumigationStatus "+serviceEntity.getFumigationStatus() +"-"+
								serviceEntity.getCount());
				if(serviceEntity.isWmsServiceFlag() && serviceEntity.getStatus().equals(Service.SERVICESTATUSCOMPLETED)
						&& !serviceEntity.getFumigationStatus().equals(Service.SERVICESTATUSSUCCESS)){
					logger.log(Level.SEVERE, "Fumigation Service Status  "+serviceEntity.getFumigationStatus());
					
					if(!serviceEntity.getFumigationStatus().equals(Service.SERVICESTATUSFAILURE)) {
						String response;
						try {
							response = servicesucessfull.createFumigationAndProphalacticService(serviceEntity,null,null,true);
							logger.log(Level.SEVERE, "response for automatic fumigation successful", response);

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
				}
			}
			
		}
			
	}


	private void updateDegassingServices(Company company) {

		List<Service> deggasingServicelist = ofy().load().type(Service.class).filter("wmsServiceFlag", true)
				.filter("companyId", company.getCompanyId()).filter("product.productCode", "DSSG").filter("status", Service.SERVICESTATUSCOMPLETED)
				.list();
		logger.log(Level.SEVERE, "deggasingServicelist == "+deggasingServicelist.size());

		if(deggasingServicelist.size()!=0){
			ofy().save().entities(deggasingServicelist);
			logger.log(Level.SEVERE, "Deggassing services updated successfully");

		}
	}
	
	
	public void sendEmailAutoSucessFumigationServiceCreationFailure(Service serviceEntity, String strSubject) {

		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add("support@evasoftwaresolutions.com");
		try {
			logger.log(Level.SEVERE,"Inside Email Method");
			Company comp = ofy().load().type(Company.class).filter("companyId", serviceEntity.getCompanyId()).first().now();
			String mailBody = "Dear Sir, <br> "+strSubject +" Fumigation service creation failed. Fumigation service id - "+serviceEntity.getCount() +". Please check log";
			Email email = new Email();
			
			email.sendEmail(strSubject+"Fumigation service creation failed ", mailBody, toEmailList, null, comp.getEmail());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	

}
