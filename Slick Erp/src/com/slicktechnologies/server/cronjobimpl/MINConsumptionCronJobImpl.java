package com.slicktechnologies.server.cronjobimpl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.simple.JSONObject;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class MINConsumptionCronJobImpl extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1092089311165778135L;
	private DateFormat fmt = new SimpleDateFormat("dd/mm/yyy");
	Logger logger = Logger.getLogger("NameOfYourLogger");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		
		Date today=DateUtility.getDateWithTimeZone("IST", new Date());
		Calendar cal=Calendar.getInstance();
		cal.setTime(today);
		cal.add(Calendar.DATE, 0);
		Date todayDateFilter=null;
		try {
			todayDateFilter=fmt.parse(fmt.format(cal.getTime()));
			cal.set(Calendar.HOUR_OF_DAY,00);
			cal.set(Calendar.MINUTE,00);
			cal.set(Calendar.SECOND,00);
			cal.set(Calendar.MILLISECOND,000);
			todayDateFilter=cal.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"Date After setting time"+todayDateFilter);
		
		Calendar cal2=Calendar.getInstance();
		cal2.setTime(today);
		cal2.add(Calendar.DATE, 0);
		Date todayDateFilter2=null;
		try {
			todayDateFilter2=fmt.parse(fmt.format(cal2.getTime()));
			cal2.set(Calendar.HOUR_OF_DAY,23);
			cal2.set(Calendar.MINUTE,59);
			cal2.set(Calendar.SECOND,59);
			cal2.set(Calendar.MILLISECOND,999);
			todayDateFilter2=cal2.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"Date After setting time filter 2"+todayDateFilter2);
		ArrayList<String> statusList = new ArrayList<String>();
		statusList.add(MaterialIssueNote.APPROVED);
		statusList.add(MaterialIssueNote.CANCELLED);
		
		List<Company> companylist = ofy().load().type(Company.class).list();
		for(Company company: companylist){
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialIssueNote", "EnableMINConsumptionToSAP", company.getCompanyId())){
			
//			if(company.getAccessUrl().equalsIgnoreCase("my")){
				List<MaterialIssueNote> minList = ofy().load().type(MaterialIssueNote.class).filter("companyId", company.getCompanyId())
						.filter("approvalDate >=", todayDateFilter).filter("approvalDate <=", todayDateFilter2)
						.filter("status IN" , statusList).filter("createdFromApp",false).list();
				logger.log(Level.SEVERE, "MIN List Size "+minList.size());
				HashSet<String> hsBranch = new HashSet<String>();
				HashSet<Integer> hsproductId = new HashSet<Integer>();

				ArrayList<MaterialProduct> approvedminlist = new ArrayList<MaterialProduct>();
				ArrayList<MaterialProduct> cancelledminlist = new ArrayList<MaterialProduct>();
				if(minList.size()!=0){
					
					for(MaterialIssueNote min :minList){
						if(min.getStatus().equals(MaterialIssueNote.APPROVED)){
							for(MaterialProduct materialproduct: min.getSubProductTablemin()){
								materialproduct.setMaterialBranch(min.getBranch());
								approvedminlist.add(materialproduct);
								hsproductId.add(materialproduct.getMaterialProductId());
							}
						}
						else if(!min.getApprovalDate().equals(new Date())){
							for(MaterialProduct materialproduct: min.getSubProductTablemin()){
								materialproduct.setMaterialBranch(min.getBranch());
								cancelledminlist.add(materialproduct);
								hsproductId.add(materialproduct.getMaterialProductId());
							}
						}
						hsBranch.add(min.getBranch());
					}
					
					List<Branch> branchlist = ofy().load().type(Branch.class).filter("companyId", company.getCompanyId())
												.filter("buisnessUnitName IN", hsBranch).list();
					logger.log(Level.SEVERE,"branch list size");
					
					ArrayList<MaterialProduct> minconsumptionlist = new ArrayList<MaterialProduct>();
					for(String strBranch : hsBranch){
						logger.log(Level.SEVERE,"Branch Name"+strBranch);
						String costCenter= getcostCenter(strBranch,branchlist);
						for(Integer intproductId : hsproductId){
							double dayMaterialConsumptionQty = 0;
							boolean flag = false;
							String productCode = "";
							for(MaterialProduct mproduct :approvedminlist){
								if(intproductId==mproduct.getMaterialProductId() && strBranch.equals(mproduct.getMaterialBranch())){
									flag = true;
									dayMaterialConsumptionQty += mproduct.getMaterialProductRequiredQuantity();
									productCode = mproduct.getMaterialProductCode();
								}
							}
							logger.log(Level.SEVERE,"Product id"+intproductId+"Consumed qty for Day"+dayMaterialConsumptionQty);
							/*** for Cancelled min minus from consumed qty ***/
							for(MaterialProduct mproduct :cancelledminlist ){
								if(intproductId==mproduct.getMaterialProductId() && strBranch.equals(mproduct.getMaterialBranch())){
									flag = true;
									dayMaterialConsumptionQty -= mproduct.getMaterialProductRequiredQuantity();
									productCode = mproduct.getMaterialProductCode();
								}

							}
							logger.log(Level.SEVERE,"Product id"+intproductId+"after minus Cancelled consumed qty for Day"+dayMaterialConsumptionQty);

							if(flag && dayMaterialConsumptionQty!=0){
								MaterialProduct materialProduct = new MaterialProduct();
								materialProduct.setMaterialProductCode(productCode);
								materialProduct.setMaterialProductRequiredQuantity(dayMaterialConsumptionQty);
								materialProduct.setMaterialBranch(strBranch);
								materialProduct.setMaterialProductRemarks(costCenter);
								minconsumptionlist.add(materialProduct);
							}
							
						}
					}
					/******** here Data will send though JSON ************/
					if(minconsumptionlist.size()!=0){
						sendMINData(minconsumptionlist);
					}
				}
				
//			}
				
			}	
		}
		
		
	
	}

//	public void MINConsumption() {
//
//		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
//		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
//		
//		Date today=DateUtility.getDateWithTimeZone("IST", new Date());
//		Calendar cal=Calendar.getInstance();
//		cal.setTime(today);
//		cal.add(Calendar.DATE, 0);
//		Date todayDateFilter=null;
//		try {
//			todayDateFilter=fmt.parse(fmt.format(cal.getTime()));
//			cal.set(Calendar.HOUR_OF_DAY,00);
//			cal.set(Calendar.MINUTE,00);
//			cal.set(Calendar.SECOND,00);
//			cal.set(Calendar.MILLISECOND,000);
//			todayDateFilter=cal.getTime();
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		logger.log(Level.SEVERE,"Date After setting time"+todayDateFilter);
//		
//		Calendar cal2=Calendar.getInstance();
//		cal2.setTime(today);
//		cal2.add(Calendar.DATE, 0);
//		Date todayDateFilter2=null;
//		try {
//			todayDateFilter2=fmt.parse(fmt.format(cal2.getTime()));
//			cal2.set(Calendar.HOUR_OF_DAY,23);
//			cal2.set(Calendar.MINUTE,59);
//			cal2.set(Calendar.SECOND,59);
//			cal2.set(Calendar.MILLISECOND,999);
//			todayDateFilter2=cal2.getTime();
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		logger.log(Level.SEVERE,"Date After setting time filter 2"+todayDateFilter2);
//		ArrayList<String> statusList = new ArrayList<String>();
//		statusList.add(MaterialIssueNote.APPROVED);
//		statusList.add(MaterialIssueNote.CANCELLED);
//		
//		List<Company> companylist = ofy().load().type(Company.class).list();
//		for(Company company: companylist){
////			if(company.getAccessUrl().equalsIgnoreCase("my")){
//				List<MaterialIssueNote> minList = ofy().load().type(MaterialIssueNote.class).filter("companyId", company.getCompanyId())
//						.filter("approvalDate >=", todayDateFilter).filter("approvalDate <=", todayDateFilter2)
//						.filter("status IN" , statusList).list();
//				logger.log(Level.SEVERE, "MIN List Size "+minList.size());
//				HashSet<String> hsBranch = new HashSet<String>();
//				HashSet<Integer> hsproductId = new HashSet<Integer>();
//
//				ArrayList<MaterialProduct> approvedminlist = new ArrayList<MaterialProduct>();
//				ArrayList<MaterialProduct> cancelledminlist = new ArrayList<MaterialProduct>();
//				if(minList.size()!=0){
//					
//					for(MaterialIssueNote min :minList){
//						if(min.getStatus().equals(MaterialIssueNote.APPROVED)){
//							for(MaterialProduct materialproduct: min.getSubProductTablemin()){
//								materialproduct.setMaterialBranch(min.getBranch());
//								approvedminlist.add(materialproduct);
//								hsproductId.add(materialproduct.getMaterialProductId());
//							}
//						}
//						else if(!min.getApprovalDate().equals(new Date())){
//							for(MaterialProduct materialproduct: min.getSubProductTablemin()){
//								materialproduct.setMaterialBranch(min.getBranch());
//								cancelledminlist.add(materialproduct);
//								hsproductId.add(materialproduct.getMaterialProductId());
//							}
//						}
//						hsBranch.add(min.getBranch());
//					}
//					
//					List<Branch> branchlist = ofy().load().type(Branch.class).filter("companyId", company.getCompanyId())
//												.filter("buisnessUnitName IN", hsBranch).list();
//					logger.log(Level.SEVERE,"branch list size");
//					
//					ArrayList<MaterialProduct> minconsumptionlist = new ArrayList<MaterialProduct>();
//					for(String strBranch : hsBranch){
//						logger.log(Level.SEVERE,"Branch Name"+strBranch);
//						String costCenter= getcostCenter(strBranch,branchlist);
//						for(Integer intproductId : hsproductId){
//							double dayMaterialConsumptionQty = 0;
//							boolean flag = false;
//							String productCode = "";
//							for(MaterialProduct mproduct :approvedminlist){
//								if(intproductId==mproduct.getMaterialProductId() && strBranch.equals(mproduct.getMaterialBranch())){
//									flag = true;
//									dayMaterialConsumptionQty += mproduct.getMaterialProductRequiredQuantity();
//									productCode = mproduct.getMaterialProductCode();
//								}
//							}
//							logger.log(Level.SEVERE,"Product id"+intproductId+"Consumed qty for Day"+dayMaterialConsumptionQty);
//							/*** for Cancelled min minus from consumed qty ***/
//							for(MaterialProduct mproduct :cancelledminlist ){
//								if(intproductId==mproduct.getMaterialProductId() && strBranch.equals(mproduct.getMaterialBranch())){
//									flag = true;
//									dayMaterialConsumptionQty -= mproduct.getMaterialProductRequiredQuantity();
//								}
//
//							}
//							logger.log(Level.SEVERE,"Product id"+intproductId+"after minus Cancelled consumed qty for Day"+dayMaterialConsumptionQty);
//
//							if(flag && dayMaterialConsumptionQty!=0){
//								MaterialProduct materialProduct = new MaterialProduct();
//								materialProduct.setMaterialProductCode(productCode);
//								materialProduct.setMaterialProductRequiredQuantity(dayMaterialConsumptionQty);
//								materialProduct.setMaterialBranch(strBranch);
//								materialProduct.setMaterialProductRemarks(costCenter);
//								minconsumptionlist.add(materialProduct);
//							}
//							
//						}
//					}
//					/******** here Data will send though JSON ************/
//					if(minconsumptionlist.size()!=0){
//						sendMINData(minconsumptionlist);
//					}
//				}
//				
////			}
//		}
//		
//		
//	
//	}

	private String getcostCenter(String strBranch, List<Branch> branchlist) {
		for(Branch branch : branchlist){
			if(branch.getBusinessUnitName().trim().equals(strBranch.trim())){
				return branch.getReferenceNumber();
			}
		}
		return null;
	}
	
	private void sendMINData(ArrayList<MaterialProduct> minconsumptionlist) {
		logger.log(Level.SEVERE, "MIN Day consumption Json Data Sending");
//		final String minDayconsumptionURL = "my.evadev006.appspot.com";
		final String minDayconsumptionURL = AppConstants.NBHCMINOUTBOUNDINTEGRATE;

		String data ="";
		URL url = null;
		try {
			url = new URL(minDayconsumptionURL);
		} catch (MalformedURLException e) {
			logger.log(Level.SEVERE,"MalformedURLException"+e);
			e.printStackTrace();
		} 
		
		HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException in open connection "+e);
			e.printStackTrace();
		}
		con.setDoInput(true);
		con.setDoOutput(true);
		con.setRequestProperty("Content-Type","application/json; charset=UTF-8");
		try {
			con.setRequestMethod("POST");
		} catch (ProtocolException e) {
			logger.log(Level.SEVERE, "ProtocolException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		
		OutputStream os = null;
		try {
			os = con.getOutputStream();
		} catch (IOException e) {
			logger.log(Level.SEVERE, "IOException 2 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
		// Log.d("Test", "From Get Post Method" + getPostData(values));
		try {
			writer.write(createGRNJSONObject(minconsumptionlist));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException2 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		try {
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException3 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		InputStream is = null;
		try {
			is = con.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String temp;

		try {
			while ((temp = br.readLine()) != null) {
				data = data + temp;
			}
			logger.log(Level.SEVERE, "data data::::::::::" + data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		logger.log(Level.SEVERE, "Data::::::::::" + data);
		
//		createGRNJSONObject(minconsumptionlist);
		
	}

	private String createGRNJSONObject(ArrayList<MaterialProduct> minconsumptionlist) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		JSONObject jsonObj = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		for(MaterialProduct mproduct:minconsumptionlist){
			JSONObject jobj = new JSONObject();
			jobj.put("Document_Date", sdf.format(new Date()));
			jobj.put("Posting_Date", sdf.format(new Date()));
			jobj.put("Material_Code", mproduct.getMaterialProductCode());
			jobj.put("Quantity", mproduct.getMaterialProductRequiredQuantity());
			jobj.put("Branch", mproduct.getMaterialBranch());
			jobj.put("CostCenter", mproduct.getMaterialProductRemarks());
			jsonArray.put(jobj);
		}
		jsonObj.put("MINData", jsonArray);
		String jsonString = jsonObj.toJSONString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE, "JSON MIN DATA:::" + jsonString);

		return jsonString;
	}

	
	
	
	

}
