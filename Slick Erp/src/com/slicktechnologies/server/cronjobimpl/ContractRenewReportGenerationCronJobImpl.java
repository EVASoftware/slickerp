package com.slicktechnologies.server.cronjobimpl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;

import com.google.gson.Gson;
import com.google.gwt.logging.impl.LevelImplRegular;
import com.ibm.icu.text.DecimalFormat;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

public class ContractRenewReportGenerationCronJobImpl extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("ContractRenewReportGenerationCronJobImpl.class");
	long companyId=0;
	PrintWriter pw;
	CsvWriter csv;
	
	byte[] bb;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {/*
		super.doGet(req, resp);
		
//		 responseref=resp;
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		 Date d=DateUtility.getDateWithTimeZone("IST", new Date());
		 String dateString = sdf.format(d);
		
*//******************************Converting todayDate to String format*****************//*
		 
		 Date todaysDate = new Date();
		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 String todayDateString = df.format(todaysDate);
		 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);

		 
	*//********************************Adding Companies in the list ***********************//*
		
		Calendar calendar=Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY,23);
		calendar.set(Calendar.MINUTE,59);
		calendar.set(Calendar.SECOND,59);
		calendar.set(Calendar.MILLISECOND,999);
		calendar.setTime(new Date());
		calendar.add(Calendar.DATE, +10);
		Date nextDate = calendar.getTime();
		String todayDateinString=sdf.format(nextDate);
//		
		
		Date today=DateUtility.getDateWithTimeZone("IST", new Date());
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		Date todayDate=DateUtility.getDateWithTimeZone("IST", new Date());
		
		*//**
		 *   Date which is  -30 days with time 00 00 000
		 *//*
		
		Calendar cal=Calendar.getInstance();
		cal.setTime(today);
		cal.add(Calendar.DATE, -30);
		
		Date minusThirtyDaysDate=null;
		
		try {
			minusThirtyDaysDate=dateFormat.parse(dateFormat.format(cal.getTime()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		logger.log(Level.SEVERE,"Today Date -30 Days Date"+minusThirtyDaysDate);
		System.out.println("Today Date -30 Days Date: "+minusThirtyDaysDate);
		
		
		
		*//**
		 *   Date which is  +30 days  with time 23 59 59 999
		 *//*
		
		logger.log(Level.SEVERE,"Date Before Adding One Day"+today);
		Calendar cal3=Calendar.getInstance();
		cal3.setTime(today);
		cal3.add(Calendar.DATE, +30);
		
		Date plusThirtyDaysDate=null;
		
		try {
			plusThirtyDaysDate=dateFormat.parse(dateFormat.format(cal3.getTime()));
			cal3.set(Calendar.HOUR_OF_DAY,23);
			cal3.set(Calendar.MINUTE,59);
			cal3.set(Calendar.SECOND,59);
			cal3.set(Calendar.MILLISECOND,999);
			plusThirtyDaysDate=cal3.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		logger.log(Level.SEVERE,"Today Date +30 Days Date"+plusThirtyDaysDate);
		
		
		logger.log(Level.SEVERE,"Today Date +30 Days Date"+plusThirtyDaysDate);
		logger.log(Level.SEVERE,"Date ::"+todayDateinString + " start date --- " + dateString);
		Company company=ofy().load().type(Company.class).filter("accessUrl","my").first().now();
		companyId=company.getCompanyId();

		boolean flag= false;
		ProcessConfiguration process = ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId).
				filter("processName", "CronJob").first().now();
		
		for (int i = 0; i < process.getProcessList().size(); i++) {
			if(process.getProcessList().get(i).processType.equalsIgnoreCase("ContractRenewReportGenerationCronJobImpl")){
				flag= true;
			}
		}
		
		logger.log(Level.SEVERE,"Flag ::"+flag+"  "+companyId);
		if(flag){
			ArrayList<String> employeeRoleList=new ArrayList<String>();
			employeeRoleList.add("Branch Manager");
			employeeRoleList.add("Zonal Head");
			employeeRoleList.add("Business Head");
			employeeRoleList.add("Admin");
			employeeRoleList.add("Zonal Coordinator");
			employeeRoleList.add("Regional Manager");
			
			
			for(String roleName:employeeRoleList){
				List<Employee>  employeeList=new ArrayList<Employee>();
				employeeList=ofy().load().type(Employee.class).filter("companyId", companyId).filter("roleName", roleName).list();
				logger.log(Level.SEVERE,""+roleName+" "+"Employee List size::: "+employeeList.size());
				
				for (Employee employee : employeeList) {
					ArrayList<String> toEmailList=new ArrayList<String>();
					toEmailList.add(employee.getEmail().trim());
					List<String> branchList=new ArrayList<String>();
					for (int i = 0; i < employee.getEmpBranchList().size(); i++) {
						branchList.add(employee.getEmpBranchList().get(i).getBranchName());
						logger.log(Level.SEVERE, "  -- branch list --"+ employee.getEmpBranchList().get(i).getBranchName());
					}
					logger.log(Level.SEVERE,"toEmailList size::: "+toEmailList.size()+" Branch List SIze : "+branchList.size());
					
					List<Contract> contratList = new ArrayList<Contract>();
					try {
						if(branchList.size()!=0){
							
							logger.log(Level.SEVERE, DateUtility.getDateWithTimeZone("IST", new Date())+" start date--");
							logger.log(Level.SEVERE, DateUtility.getDateWithTimeZone("IST", nextDate)+" end  date--");
							
							
						contratList = ofy().load().type(Contract.class).filter("companyId", companyId).filter("branch IN",branchList)
								.filter("endDate >=", minusThirtyDaysDate).filter("endDate <=", plusThirtyDaysDate).filter("renewFlag", false)
								.filter("customerInterestFlag",false).filter("status", "Approved").list();
						}
					} catch (Exception e1) {
						e1.printStackTrace();
						logger.log(Level.SEVERE,"Error parsing date while querry");
					}
					logger.log(Level.SEVERE,"Contract List Size ::"+contratList.size());
					
					*//**
					 * Email Header 
					 *//*
					ArrayList<String> tbl_header = new ArrayList<String>();
					tbl_header.add("Contract Id");
					tbl_header.add("Contract Date");
					tbl_header.add("Contract Start Date");
					tbl_header.add("Contract End Date");
					tbl_header.add("Customer Id");
					tbl_header.add("Customer Name");
					tbl_header.add("Contract Type");
					tbl_header.add("Contract Category");
					tbl_header.add("Branch");
					tbl_header.add("Sales Person");
					tbl_header.add("Status");
					tbl_header.add("Agies");
					tbl_header.add("Contract Value");
					ArrayList<String> tbl1=new ArrayList<String>();
					int count=0;
					for (Contract obj : contratList) {
						logger.log(Level.SEVERE, obj.getCount()+" end date -- "+obj.getEndDate() );
						tbl1.add(obj.getCount()+"");
						tbl1.add(sdf.format(obj.getContractDate()));
						tbl1.add(sdf.format(obj.getStartDate()));
						tbl1.add(sdf.format(obj.getEndDate()));
						tbl1.add(obj.getCinfo().getCount()+"");
						tbl1.add(obj.getCinfo().getFullName());
						tbl1.add(obj.getType());
						tbl1.add(obj.getCategory());
						tbl1.add(obj.getBranch());
						tbl1.add(obj.getEmployee());
						tbl1.add(obj.getStatus());
						tbl1.add(obj.getNetpayable()+"");
						  *//**************** for getting ageing for each Contract ******//*
			   	 	    String StringContractEndDate = dateFormat.format(obj.getEndDate());
			   	 	    
			   				Date d1 = null;
			   				Date d2 = null;
			   				
			   				try {
								d1 = df.parse(StringContractEndDate);
								d2 = df.parse(todayDateString);
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
			   				
			   				long diff = d2.getTime() - d1.getTime();
							long diffDays = diff / (24 * 60 * 60 * 1000);
							
//							System.out.println("Ageing:"+diffDays);
//					   	 	logger.log(Level.SEVERE,"Ageing:"+diffDays);
				
			   	 	    tbl1.add(diffDays +"");
						count=count+1;
					}
					
					*//**
					 * Email Parts ends here
					 *//*
					Email cronEmail = new Email();
					
					try {
						if(contratList.size()!=0){
							cronEmail.cronSendEmail(toEmailList, "Contract Report As On Date"+" "+ todayDateinString, "Contract Report As On Date" +"  "+ todayDateinString, company, tbl_header, tbl1, null, null, null, null);
						}
					}catch (IOException e) {
						e.printStackTrace();
						logger.log(Level.SEVERE,"Error in cronEmail method:::"+e);
//						return "Failed to send emails";
					}
				}
			}
			
//		

		}
		
	*/}
	public void getContractRenewDetails(List<CronJobConfigrationDetails> cronList,HashSet<String>  empRoleList) {
//		String strCronDetails = req.getParameter("cronList");
		logger.log(Level.SEVERE, "Cron list str --" + cronList);
		Gson gson = new Gson();
		JSONArray jsonarr = null;
		try {
			
			 
			List<Company> compEntity = ofy().load().type(Company.class).list();
			if(compEntity.size()>0){
				
				logger.log(Level.SEVERE,"If compEntity size > 0");
				logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());

				for(int dd=0;dd<compEntity.size();dd++){
					Company c =compEntity.get(dd);
					companyId = c.getCompanyId();
					for(String empRoleName :empRoleList){
						logger.log(Level.SEVERE,"get c. id -- "+ c.getCompanyId() + " " + c.getAccessUrl() );
						/**
						 * @author Anil , Date : 25-04-2019
						 * while loading employee added filter ,its status should  be active 
						 * Because mails were triggered on in active employee ids also
						 */
						List<Employee>  employeeList=new ArrayList<Employee>();
						employeeList=ofy().load().type(Employee.class).filter("companyId", c.getCompanyId())
								.filter("roleName", empRoleName.trim()).filter("status", true).list();
						logger.log(Level.SEVERE,""+empRoleName+" "+"Employee List size::: "+employeeList.size());
						
						for (Employee employee : employeeList) {
							
							
							HashMap<String, String> empCount = new HashMap<String,String>();
							HashMap<String, String> branchCount = new HashMap<String, String>();
							
							int total =1;
							ArrayList<String> overHeader = new ArrayList<String>();
							ArrayList<String> dueHeader = new ArrayList<String>();
							ArrayList<String> overDetail = new ArrayList<String>();
							ArrayList<String> dueDetail = new ArrayList<String>();
							
							String header1 = "",header2 = "",title = "";
							
							
							 String todayDateString ="";
							 String mailTitl = "Contract Renew Due As On Date";
						 
							 String footer = "";
								
								
						 	String emailSubject =  "Contract Renew Due As On Date"+" "+ todayDateString;
							
							
							String emailMailBody = mailTitl;
							
							
							
							
							ArrayList<String> toEmailList=new ArrayList<String>();
							toEmailList.add(employee.getEmail().trim());
							List<String> branchList=new ArrayList<String>();
							branchList.add(employee.getBranchName());
							for (int j = 0; j < employee.getEmpBranchList().size(); j++) {
								branchList.add(employee.getEmpBranchList().get(j).getBranchName());
								logger.log(Level.SEVERE, "  -- branch list --"+ employee.getEmpBranchList().get(j).getBranchName());
							}
						
						
							try {
								if(branchList.size()!=0){
									
									for(int i=0;i<cronList.size();i++){
										
										if(cronList.get(i).getEmployeeRole().trim().equalsIgnoreCase(empRoleName.trim())){

											
											CronJobConfigrationDetails cronDetails =cronList.get(i);
											
											
											SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
											sdf.setTimeZone(TimeZone.getTimeZone("IST"));
											TimeZone.setDefault(TimeZone.getTimeZone("IST"));
											
											 Date d=DateUtility.getDateWithTimeZone("IST", new Date());
											
									/******************************Converting todayDate to String format*****************/
											 
											 Date todaysDate = new Date();
											 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
											 todayDateString = df.format(todaysDate);
											 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);

											 
										/********************************Adding Companies in the list ***********************/
											
											Calendar calendar=Calendar.getInstance();
											
											Date today=DateUtility.getDateWithTimeZone("IST", new Date());
											TimeZone.setDefault(TimeZone.getTimeZone("IST"));
											fmt.setTimeZone(TimeZone.getTimeZone("IST"));
											dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
											
											Date todayDate=DateUtility.getDateWithTimeZone("IST", new Date());
											
											/**
											 *   Date which is  -30 days with time 00 00 000
											 */
											
											Calendar cal=Calendar.getInstance();
											cal.setTime(today);
										
											Date minusThirtyDaysDate=null;
											
											try {
												minusThirtyDaysDate=dateFormat.parse(dateFormat.format(cal.getTime()));
												cal.set(Calendar.HOUR_OF_DAY,0);
												cal.set(Calendar.MINUTE,0);
												cal.set(Calendar.SECOND,0);
												cal.set(Calendar.MILLISECOND,0);
												minusThirtyDaysDate = cal.getTime();
											} catch (ParseException e) {
												e.printStackTrace();
											}
											
											logger.log(Level.SEVERE,"Today Date -30 Days Date"+minusThirtyDaysDate);
											
											
											logger.log(Level.SEVERE,"Date Before Adding One Day"+today);
											Calendar cal3=Calendar.getInstance();
											cal3.setTime(today);
											if(cronDetails.isOverdueStatus() && cronDetails.getOverdueDays()>0){
												cal3.add(Calendar.DATE, cronDetails.getOverdueDays() * -1);
											}else{
												cal3.add(Calendar.DATE, +cronDetails.getInterval());
											}
											
											
											Date plusThirtyDaysDate=null;
											
											try {
												plusThirtyDaysDate=dateFormat.parse(dateFormat.format(cal3.getTime()));
												cal3.set(Calendar.HOUR_OF_DAY,23);
												cal3.set(Calendar.MINUTE,59);
												cal3.set(Calendar.SECOND,59);
												cal3.set(Calendar.MILLISECOND,999);
												plusThirtyDaysDate=cal3.getTime();
											} catch (ParseException e) {
												e.printStackTrace();
											}
											
											logger.log(Level.SEVERE,"Today Date +30 Days Date"+plusThirtyDaysDate);
											
											
											List<Contract> contratList = new ArrayList<Contract>();
											
											
											if(cronDetails.getEmployeeRole().equalsIgnoreCase("Admin")){
												if(!cronDetails.isOverdueStatus()){
													
													contratList = ofy().load().type(Contract.class).filter("companyId", companyId)
															.filter("endDate >=", minusThirtyDaysDate).filter("endDate <=", plusThirtyDaysDate).filter("renewFlag", false)
															.filter("customerInterestFlag",false).filter("status", "Approved").list();
												
												}else{
													try {
														cal.add(Calendar.DATE, -90);
														minusThirtyDaysDate=dateFormat.parse(dateFormat.format(cal.getTime()));
														cal.set(Calendar.HOUR_OF_DAY,0);
														cal.set(Calendar.MINUTE,0);
														cal.set(Calendar.SECOND,0);
														cal.set(Calendar.MILLISECOND,0);
														minusThirtyDaysDate=cal.getTime();
													} catch (ParseException e) {
														e.printStackTrace();
													}
													contratList = ofy().load().type(Contract.class).filter("companyId", companyId)
															.filter("endDate <=", plusThirtyDaysDate).filter("renewFlag", false)
															.filter("endDate >=", minusThirtyDaysDate)
															.filter("customerInterestFlag",false).filter("status", "Approved").list();
												
												}
											}else{
												if(!cronDetails.isOverdueStatus()){
													contratList = ofy().load().type(Contract.class).filter("companyId", companyId).filter("branch IN",branchList)
															.filter("endDate >=", minusThirtyDaysDate).filter("endDate <=", plusThirtyDaysDate).filter("renewFlag", false)
															.filter("customerInterestFlag",false).filter("status", "Approved").list();
												
												}else{
													try {
														cal.add(Calendar.DATE, -90);
														minusThirtyDaysDate=dateFormat.parse(dateFormat.format(cal.getTime()));
														cal.set(Calendar.HOUR_OF_DAY,0);
														cal.set(Calendar.MINUTE,0);
														cal.set(Calendar.SECOND,0);
														cal.set(Calendar.MILLISECOND,0);
														minusThirtyDaysDate=cal.getTime();
													} catch (ParseException e) {
														e.printStackTrace();
													}
													contratList = ofy().load().type(Contract.class).filter("companyId", companyId).filter("branch IN",branchList)
															.filter("endDate <=", plusThirtyDaysDate).filter("renewFlag", false)
															.filter("endDate >=", minusThirtyDaysDate)
															.filter("customerInterestFlag",false).filter("status", "Approved").list();
												
												}	
											}
											
											
											
											logger.log(Level.SEVERE,"get "+contratList.size());
											ArrayList<String> tbl_header = new ArrayList<String>();
											tbl_header.add("Ser No.");
											tbl_header.add("Branch");
											
											tbl_header.add("Contract Id");
											tbl_header.add("Contract Start Date");
											tbl_header.add("Contract End Date");
											tbl_header.add("Contract Value");
											tbl_header.add("Customer Name");
											tbl_header.add("Cell No");
											tbl_header.add("Poc Name");
											tbl_header.add("Email");
											tbl_header.add("Sales Person");
											tbl_header.add("Contract Group");
											tbl_header.add("Contract Category");
											tbl_header.add("Contract Type");
											tbl_header.add("Ref_No");
											tbl_header.add("Ref_Date");
											tbl_header.add("Billing Address");
											tbl_header.add("Locality");
											tbl_header.add("City");
											tbl_header.add("Number Range");
											tbl_header.add("Status");
											tbl_header.add("Agies");
											
											ArrayList<String> tbl1=new ArrayList<String>();
											int count=1;
											/**
											 * @author Anil,Date : 16-02-2019
											 * Formatted contrat srtart and end date
											 */
											for (Contract obj : contratList) {
												tbl1.add(count+"");
												tbl1.add(obj.getBranch());
												logger.log(Level.SEVERE, obj.getCount()+" end date -- "+obj.getEndDate() +" count-- "+ count);
												tbl1.add(obj.getCount()+"");
//												tbl1.add(sdf.format(obj.getStartDate()));
//												tbl1.add(sdf.format(obj.getEndDate()));
												if(obj.getStartDate()!=null){
													tbl1.add(fmt.format(obj.getStartDate())+"");
												}else{
													tbl1.add("");
												}
												if(obj.getEndDate()!=null){
													tbl1.add(fmt.format(obj.getEndDate())+"");
												}else{
													tbl1.add("");
												}
											
												tbl1.add(obj.getNetpayable()+"");
												if (obj.getCinfo()!=null) {
													if(obj.getCinfo().getFullName()!=null){
														tbl1.add(obj.getCinfo().getFullName());
													}else{
														tbl1.add("");
													}
													if(obj.getCinfo().getCellNumber()!=null){
														tbl1.add(obj.getCinfo().getCellNumber()+"");
													}else{
														tbl1.add("");
													}
													if(obj.getCinfo().getPocName()!=null){
														tbl1.add(obj.getCinfo().getPocName()+"");
													}else{
														tbl1.add("");
													}
													if(obj.getCinfo().getEmail()!=null){
														tbl1.add(obj.getCinfo().getEmail()+"");
													}else{
														tbl1.add("");
													}
												}else{
													tbl1.add("");
													tbl1.add("");
													tbl1.add("");
													tbl1.add("");
												}
												if(obj.getEmployee()!=null){
													tbl1.add(obj.getEmployee());
												}else{
													tbl1.add("");
												}
//												tbl1.add(obj.getEmployee()); 
												logger.log(Level.SEVERE," get category -- " + obj.getCategory() + " type -- " + obj.getType() + "  group --" + obj.getGroup()) ;
												if(obj.getGroup()!=null){
													tbl1.add(obj.getGroup());
												}else{
													tbl1.add("");
												}
												
												if(obj.getCategory()!=null){
													tbl1.add(obj.getCategory());
												}else{
													tbl1.add("");
												}
												
												if(obj.getType()!=null){
													tbl1.add(obj.getType());
												}else{
													tbl1.add("");
												}
												if(obj.getReferenceNo()!=null){
													tbl1.add(obj.getReferenceNo());
												}else{
													tbl1.add("");
												}
												if(obj.getRefDate()!=null){
													tbl1.add(sdf.format(obj.getRefDate()));
												}else{
													tbl1.add("");
												}
												
												String add = "";
												if(obj.getNewcustomerAddress()!= null && obj.getNewcustomerAddress().getAddrLine1()!=null){
													add =add + " " + obj.getNewcustomerAddress().getAddrLine1();
												}
//												add = obj.getCustomerServiceAddress().getAddrLine1();
												if(obj.getNewcustomerAddress()!= null && obj.getNewcustomerAddress().getAddrLine2()!=null){
													add =add + " " + obj.getNewcustomerAddress().getAddrLine2();
												}
												
												tbl1.add(add);
												 add = "";
												 if(obj.getNewcustomerAddress()!= null && obj.getNewcustomerAddress().getLocality()!=null){
													 tbl1.add(obj.getNewcustomerAddress().getLocality());
												 }else{
													 
													 tbl1.add("");
												 }
												 
												 add = "";
												 if(obj.getNewcustomerAddress()!= null && obj.getNewcustomerAddress().getCity()!=null){
													
													 tbl1.add(obj.getNewcustomerAddress().getCity());
												 }else{
													 tbl1.add("");
												 }
												
												if(obj.getNumberRange()!=null){
													tbl1.add(obj.getNumberRange());
												}else{
													tbl1.add("");
												}
												logger.log(Level.SEVERE,obj.getStatus()+" end date -- "+obj.getEndDate() +" count-- "+ count);
												tbl1.add(obj.getStatus());
												
												  /**************** for getting ageing for each Contract ******/
									   	 	    String StringContractEndDate = dateFormat.format(obj.getEndDate());
									   	 	    
									   				Date d1 = null;
									   				Date d2 = null;
									   				
									   				try {
														d1 = df.parse(StringContractEndDate);
														d2 = df.parse(todayDateString);
													} catch (ParseException e) {
														// TODO Auto-generated catch block
														e.printStackTrace();
													}
									   				
									   				long diff = d2.getTime() - d1.getTime();
													long diffDays = diff / (24 * 60 * 60 * 1000);
													
										
									   	 	    tbl1.add(diffDays +"");
												count=count+1;
												total++;
												logger.log(Level.SEVERE,"empcount --  "+ empCount.size() + " branch count -- " + branchCount.size());
												   if(empCount.containsKey(obj.getEmployee().trim())){
													   
													   String[] countdata = empCount.get(obj.getEmployee().trim()).split("-");
	 												   ArrayList<String> countDetails = new ArrayList<String>(Arrays.asList(countdata));

	 												   int count1 = Integer.parseInt(countDetails.get(0));
										   	 	    			
										   	 	    	count1++;
										   	 	    	
										   	 	    	double sum = Double.parseDouble(countDetails.get(1))+obj.getNetpayable();
										   	 	    	
										   	 	    	empCount.put(obj.getEmployee().trim(), count1+"-"+sum);
										   	 	    }else{
										   	 	    	empCount.put(obj.getEmployee().trim(), 1+"-"+obj.getNetpayable());
										   	 	    }
												
										   	 	    if(branchCount.containsKey(obj.getBranch().trim())){
										   	 	    	
											   	 	    String[] countdata = branchCount.get(obj.getBranch().trim()).split("-");
														   ArrayList<String> countDetails = new ArrayList<String>(Arrays.asList(countdata));
		
														   int count1 = Integer.parseInt(countDetails.get(0));
											   	 	    			
											   	 	    	count1++;
											   	 	    	
											   	 	    	double sum = Double.parseDouble(countDetails.get(1))+obj.getNetpayable();
											   	 	    	
											   	 	    branchCount.put(obj.getBranch().trim(), count1+"-"+sum);
										   	 	    	
										   	 	    }else{
										   	 	    	branchCount.put(obj.getBranch().trim(), 1+"-"+obj.getNetpayable());
										   	 	    }
												
										   	 	if(cronDetails.getFooter().trim() != "" ){
													footer = cronDetails.getFooter();
												}
												
//										   	 emailSubject =  "Contract Renewal Report As On Date"+" "+ todayDateString;
										   	 
										   	 	/**
												 * @author Anil,Date : 13-02-2019
												 * setting mail subject from cron job configuration
												 * Renaming mail default subject as per rohan's instruction
												 * 
												 */
										   	
										   	 	if(cronDetails.getSubject()!=null&&!cronDetails.getSubject().equals("")){
										   	 		emailSubject=cronDetails.getSubject();
										   	 	}
										   	 emailSubject =  "Contract renewal due and overdue report as on date"+" "+ todayDateString;
												
												 emailMailBody = mailTitl;
												
												if(cronDetails.getMailBody().trim() != "" ){
													emailMailBody = cronDetails.getMailBody();
												}
												
											}
											
											if(cronDetails.isOverdueStatus()){
												logger.log(Level.SEVERE,"over due recordes are  there.. ");
												header2 = "Over due Record AS om Date :"+todayDateString;
												
												if(!cronDetails.getSubject().trim().equals("")){
													header2 = cronDetails.getSubject();
												}
												
												if(!cronDetails.getMailBody().trim().equals("")){
													header2 = header2 + " <BR>"+ cronDetails.getMailBody();
												}
												overHeader = new ArrayList<String>();
												overHeader.addAll(tbl_header);
												overDetail.addAll(tbl1);
												footer = footer +"<br>" + "#This mail contains last 3 months data. For more records please contact Email- support@evasoftwaresolutions.com";
											}else{
												header1 = "Due record upto Date :" +todayDateString;
												
												if(!cronDetails.getSubject().trim().equals("") ){
													header1 = cronDetails.getSubject();
												}
												
												if(!cronDetails.getMailBody().equals("")){
													header1 = header1 + " <BR> "+ cronDetails.getMailBody();
												}
												
												
												dueHeader = new ArrayList<String>();
												dueHeader.addAll(tbl_header);
												dueDetail = new ArrayList<String>();
												dueDetail.addAll(tbl1);
											}
										
										
										}
										
									}
									
																	
									
									try {  
										
										Email cronEmail = new Email();
										if(overDetail.size()>0 || dueDetail.size()>0){
											
											logger.log(Level.SEVERE," get branch count --" + branchCount.size() + " emp count --" + empCount.size());
											logger.log(Level.SEVERE," get branch  --" + branchCount.toString() + " emp count --" + empCount.toString());
											ArrayList<String> summHeader  = new ArrayList<String>();
											ArrayList<String> summDetails = new ArrayList<String>();
											
											summHeader.add("Branch");
											summHeader.add("$#$");
											summHeader.add("$#$");
											
											summHeader.add("Sales Person");
											summHeader.add("$#$");
											summHeader.add("$#$");
											int tblSize = 0;
											if(branchCount.size()>0){
												tblSize = branchCount.size();
											}
											
											if(tblSize<empCount.size()){
												tblSize = empCount.size();
											}

											Set<String> branchCntList = branchCount.keySet();
											ArrayList<String> branchDt = new ArrayList<String>();
											branchDt.addAll(branchCntList);
											
											
											Set<String> empCntList = empCount.keySet();
											ArrayList<String> empDt = new ArrayList<String>();
											empDt.addAll(empCntList);
											
											
											summDetails.add("Branch Name");
											summDetails.add("No of Records");
											summDetails.add("Total Amount");
											
											summDetails.add("Sales Person");
											summDetails.add("No of Records");
											summDetails.add("Total Amount");
											
											
											for(int ij=0;ij<tblSize;ij++){
												if(branchCount.size()>ij){
													
													summDetails.add(branchDt.get(ij)+"");
													
													   String[] countdata = branchCount.get(branchDt.get(ij)).split("-");
													   ArrayList<String> countDetails = new ArrayList<String>(Arrays.asList(countdata));
										   	 	    	
													summDetails.add(countDetails.get(0));
													
													
													
													summDetails.add(countDetails.get(1));
												}else{
													summDetails.add("");
													summDetails.add("");
													summDetails.add("");
												}
												
												if(empCount.size()>ij){
													
													summDetails.add(empDt.get(ij)+"");
													
													   String[] countdata = empCount.get(empDt.get(ij)).split("-");
													   ArrayList<String> countDetails = new ArrayList<String>(Arrays.asList(countdata));
										   	 	    	
													summDetails.add(countDetails.get(0));
										   	 	    
													
													summDetails.add(countDetails.get(1));
													
												}else{
													summDetails.add("");
													summDetails.add("");
													summDetails.add("");
												}
											}
											String summTitle = " Total Records - " + total ;
											logger.log(Level.SEVERE,"Before send method call to send mail  " + total);						
										
															
											
											cronEmail.cronSendEmailDueOverDue(toEmailList, emailSubject, "", c,header1, dueHeader, dueDetail,
													header2, overHeader, overDetail, summTitle,summHeader,summDetails,null, "", footer);
											logger.log(Level.SEVERE,"after send method call to send mail  ");	
										}
										else{
											
											logger.log(Level.SEVERE,"Sorry Contract renew Due As On Date this company");	
											mailTitl = "Contract renew Due As On Date";
		
//											cronEmail.cronSendEmail(toEmailList, "Contract renew Due As On Date"+" "+ todayDateString, mailTitl , c, null, null, null, null, null, null);
											cronEmail.cronSendEmail(toEmailList, emailSubject, emailMailBody , c, null, null, null, null, null, null);
										}
										
										logger.log(Level.SEVERE,"After send mail method ");		
												//+"  "+
								} catch (IOException e1) {
									
									logger.log(Level.SEVERE,"In the catch block ");
									e1.printStackTrace();
							}
									
									
								}
							}catch(Exception e){
								e.printStackTrace();
								logger.log(Level.SEVERE,"exception --" + e);
							}
							
						}
					}
				}
			}
			
			CronJobConfigrationDetails cronDetails =null;

			
			
		
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE,"get "+e);
		}finally{
//			resp.getWriter().write("Success");
		}
	}

}
