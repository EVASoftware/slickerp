package com.slicktechnologies.server.cronjobimpl;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class CustomerPaymentAPCronJobImpl extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3521686252767557166L;
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmtPaymentDate = new SimpleDateFormat("dd/MM/yyyy");

	 Logger logger = Logger.getLogger("NameOfYourLogger");
	  
		public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws IOException {
			/**
			 *  nidhi
			 *  1-03-2018
			 *  nidhi for cron job config
			 */
//				custPaymentAPlist();
		}

		private void custPaymentAPlist() {
			

			
			Email cronEmail = new Email();
			
			Date today=DateUtility.getDateWithTimeZone("IST", new Date());
			
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			
			fmtPaymentDate.setTimeZone(TimeZone.getTimeZone("IST"));
			
			/**
			 *    i have added time in today's date
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding "+today);
			Calendar cal=Calendar.getInstance();
			cal.setTime(today);
			cal.add(Calendar.DATE, 0);
			
			Date dateForFilter=null;
			
			try {
				dateForFilter=fmtPaymentDate.parse(fmtPaymentDate.format(cal.getTime()));
				cal.set(Calendar.HOUR_OF_DAY,23);
				cal.set(Calendar.MINUTE,59);
				cal.set(Calendar.SECOND,59);
				cal.set(Calendar.MILLISECOND,999);
				dateForFilter=cal.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Date  After setting the Time Date"+dateForFilter);
			
			/************************************* End *********************************/
			
			
	 try{
		 
		 logger.log(Level.SEVERE,"In Payment AP List");	
		logger.log(Level.SEVERE,"Date After Adding One Date"+dateForFilter);

		 
	/********************************Adding status in the list ***********************/
		 
		 ArrayList<String> obj = new ArrayList<String>();
		 obj.add("Created");
		 
		 
	
	/******************************Converting todayDate to String format*****************/
		 
		 Date todaysDate = new Date();
		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 String todayDateString = df.format(todaysDate);
		 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);

		 
 /********************************Adding Companies in the list ***********************/
		 
	List<Company> compEntity = ofy().load().type(Company.class).list();
	if(compEntity.size()>0){
	
		logger.log(Level.SEVERE,"If compEntity size > 0");
		logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
		

		for(int i=0;i<compEntity.size();i++){
			
			Company c=compEntity.get(i);
			logger.log(Level.SEVERE,"In the for loop");
			logger.log(Level.SEVERE,"Company Name="+c);
			logger.log(Level.SEVERE,"The value of i is:" +i);
			logger.log(Level.SEVERE,"Date After Adding One Date"+dateForFilter);

			
 /********************************Checking prosname & prosconfig for each company ***********************/

			ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
			ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
			
			boolean processconfigflag = false;
			if(processconfig!=null){
				if(processconfig.isConfigStatus()){
					for(int l=0;l<processconfig.getProcessList().size();l++){
						if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("PaymentAPDailyEmail") && processconfig.getProcessList().get(l).isStatus()==true){
							processconfigflag=true;
						}
					}
			   }
			}
			logger.log(Level.SEVERE,"process config  SMS Flag =="+processconfigflag);
			
			if(processName!=null){
				logger.log(Level.SEVERE,"In the prossName");
			if(processconfigflag){
							
				logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
				
				
	/********************************Reading PaymentList from CustomerPayment entity  ***********************/
				
				List<CustomerPayment> custPaymentEntity = ofy().load().type(CustomerPayment.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("paymentDate <=",dateForFilter).filter("status IN",obj).filter("accountType", AppConstants.BILLINGACCOUNTTYPEAP).list();
				
				logger.log(Level.SEVERE,"Today date Is:"+dateForFilter);	

				logger.log(Level.SEVERE,"Paymnet entity size:"+custPaymentEntity.size());	
				
				
				// email id is added to emailList
				ArrayList<String> toEmailList=new ArrayList<String>();
				toEmailList.add(compEntity.get(i).getPocEmail());
				
				 String mailTitl = "Payment Due As On Date";
				
				if(custPaymentEntity.size()>0){
				
			
				ArrayList<String> tbl_header = new ArrayList<String>();
				tbl_header.add("Serial No");
				tbl_header.add("Branch");
				tbl_header.add("Customer Id");
				tbl_header.add("Customer Name");
				tbl_header.add("Customer Cont No");
				tbl_header.add("POC Name");
				tbl_header.add("Email Id");
				tbl_header.add("Sales Person");
				tbl_header.add("Person Responsible");
				tbl_header.add("OrderId");
				tbl_header.add("Invoice Id");
				tbl_header.add("Invoice Date");
				tbl_header.add("Payment Doc Id");
				tbl_header.add("Payment Amt");
				tbl_header.add("Payment Date");
				tbl_header.add("Status");
				tbl_header.add("Ageing");
				
/********************************Sorting table with Branch & Payment Date ***********************/
			
				
				Comparator<CustomerPayment> custpaymentDateComparator2 = new Comparator<CustomerPayment>() {
					public int compare(CustomerPayment s1, CustomerPayment s2) {
					
					Date date1 = s1.getPaymentDate();
					Date date2 = s2.getPaymentDate();
					
					//ascending order
					return date1.compareTo(date2);
					}
					};
					Collections.sort(custPaymentEntity, custpaymentDateComparator2);
					
					Comparator<CustomerPayment> custpaymentDateComparator = new Comparator<CustomerPayment>() {
						public int compare(CustomerPayment s1, CustomerPayment s2) {
						
						String branch1 = s1.getBranch();
						String branch2 = s2.getBranch();
						
						//ascending order
						return branch1.compareTo(branch2);
						}
						
						};
						Collections.sort(custPaymentEntity, custpaymentDateComparator);
					
/********************************Getting PaymnetEntity data and adding in the tbl1 List ***********************/
							
				ArrayList<String> tbl1=new ArrayList<String>();
					
				for(int j=0;j<custPaymentEntity.size();j++){
					
					Customer customerEntity=ofy().load().type(Customer.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("count", custPaymentEntity.get(j).getPersonInfo().getCount()).first().now();
					
					tbl1.add((j+1)+"");
		   	 		tbl1.add(custPaymentEntity.get(j).getBranch()+"");
		   	 		tbl1.add(custPaymentEntity.get(j).getPersonInfo().getCount()+"");
		   	 		tbl1.add(custPaymentEntity.get(j).getPersonInfo().getFullName()+"");		   	 		
		   	 		tbl1.add(custPaymentEntity.get(j).getPersonInfo().getCellNumber()+"");
		   	 		tbl1.add(custPaymentEntity.get(j).getPersonInfo().getPocName());
		   	 		tbl1.add(customerEntity.getEmail());
		   	 		tbl1.add(custPaymentEntity.get(j).getEmployee());
		   	 		tbl1.add(custPaymentEntity.get(j).getEmployee());
		   	 		tbl1.add(custPaymentEntity.get(j).getContractCount()+"");
		   	 		tbl1.add(custPaymentEntity.get(j).getInvoiceCount()+"");
		   	 		tbl1.add(fmt.format(custPaymentEntity.get(j).getInvoiceDate())+""); 	 		
		   	 		tbl1.add(custPaymentEntity.get(j).getCount()+"");
		   	 	    tbl1.add(custPaymentEntity.get(j).getPaymentAmt()+"");
		   	 	    tbl1.add(fmt.format(custPaymentEntity.get(j).getPaymentDate()) +"");
		 
		   	 	    /**************** for getting ageing for each Payment ******/
		   	 	    String StringPaymnetDate = fmtPaymentDate.format(custPaymentEntity.get(j).getPaymentDate());
		   	 	    
//		   	 	   	String StringPaymnetDate = df.format(paymentDate);	
		   			
		   				Date d1 = null;
		   				Date d2 = null;
		   				
		   				d1 = df.parse(StringPaymnetDate);
		   				d2 = df.parse(todayDateString);
		   				long diff = d2.getTime() - d1.getTime();
						long diffDays = diff / (24 * 60 * 60 * 1000);
						
		   	 	    tbl1.add(custPaymentEntity.get(j).getStatus()+"");
		   	 	    tbl1.add(diffDays +"");
		   	 	}			
				
				logger.log(Level.SEVERE,"In the ProsConfig !=null   3 ");
				
   /********************************Calling cronsendEmail Method ***********************/
				
//				Email e=new Email();					
				try {   
					   
						System.out.println("Before send mail method");
						logger.log(Level.SEVERE,"Before send method call to send mail  ");						
						cronEmail.cronSendEmail(toEmailList, "Payment Due As On Date"+" "+ todayDateString, mailTitl +"  "+ todayDateString, c, tbl_header, tbl1, null, null, null, null);
						logger.log(Level.SEVERE,"After send mail method ");		
								//+"  "+
				} catch (IOException e1) {
					
					logger.log(Level.SEVERE,"In the catch block ");
					e1.printStackTrace();
			}
				
			}
			else{					// else block for if(serviceEntity.size()>0){ 		
					
				System.out.println("Sorry no Payment found for this company");
				logger.log(Level.SEVERE,"Sorry no Payment found for this company");
//				
				mailTitl = "No Payment Found Due";

				cronEmail.cronSendEmail(toEmailList, "Payment Due As On Date"+" "+ todayDateString, mailTitl , c, null, null, null, null, null, null);

			}
				
				
			}
			else{					//else block for prosconfig
					logger.log(Level.SEVERE,"ProcessConfiguration is null");
					System.out.println("ProcessConfiguration is null");
			}
			
			}else{        //else block for pross!=null
				logger.log(Level.SEVERE,"Cron job status is Inactive");
				System.out.println("Cron job status is Inactive");
			}
			
							
		}	//end of for loop
		
	}
	else{			//else block for if(compEntity.size()>0)
		
		logger.log(Level.SEVERE,"No Company found from cron job completed");	
		System.out.println("No Company found from cron job completed");
	}
	
	
	 }catch(Exception e2){
	
	e2.printStackTrace();
	 }
		
			
			
	}	
		
public void custPaymentAPlist(String cronList) {
			

//			String strCronDetails = req.getParameter("cronList");
			logger.log(Level.SEVERE, "Cron list str --" + cronList);
			Gson gson = new Gson();
			JSONArray jsonarr = null;
			try {
				jsonarr=new JSONArray(cronList.trim());
			} catch (JSONException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			
			try {
				
				 
				List<Company> compEntity = ofy().load().type(Company.class).list();
				if(compEntity.size()>0){
				
					logger.log(Level.SEVERE,"If compEntity size > 0");
					logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
					

					for(int i=0;i<compEntity.size();i++){
						
						Company c=compEntity.get(i);
						logger.log(Level.SEVERE,"In the for loop");
						logger.log(Level.SEVERE,"Company Name="+c);
						logger.log(Level.SEVERE,"The value of i is:" +i);
//						logger.log(Level.SEVERE,"Date After Adding One Date"+dateForFilter);

						CronJobConfigrationDetails cronDetails =null;
						for(int k=0;k<jsonarr.length();k++){
							JSONObject jsonObj = jsonarr.getJSONObject(k);
							cronDetails = new CronJobConfigrationDetails();
							
							cronDetails = gson.fromJson(jsonObj.toString(), CronJobConfigrationDetails.class);
							
							
							
							Email cronEmail = new Email();
							
							Date today=DateUtility.getDateWithTimeZone("IST", new Date());
							
							TimeZone.setDefault(TimeZone.getTimeZone("IST"));
							fmt.setTimeZone(TimeZone.getTimeZone("IST"));
							
							fmtPaymentDate.setTimeZone(TimeZone.getTimeZone("IST"));
							
							/**
							 *    i have added time in today's date
							 */
							
							logger.log(Level.SEVERE,"Date Before Adding "+today);
							Calendar cal=Calendar.getInstance();
							cal.setTime(today);
							
							int diffDay = 0;
							 if(cronDetails.getOverdueDays()>0 && cronDetails.isOverdueStatus()){
								 diffDay = -cronDetails.getOverdueDays();
							 }else{
								 diffDay = cronDetails.getInterval();
							 }
							
							cal.add(Calendar.DATE, diffDay);
							
							Date dateForFilter=null;
							
							try {
								dateForFilter=fmtPaymentDate.parse(fmtPaymentDate.format(cal.getTime()));
								cal.set(Calendar.HOUR_OF_DAY,23);
								cal.set(Calendar.MINUTE,59);
								cal.set(Calendar.SECOND,59);
								cal.set(Calendar.MILLISECOND,999);
								dateForFilter=cal.getTime();
							} catch (ParseException e) {
								e.printStackTrace();
							}
							
							logger.log(Level.SEVERE,"Date  After setting the Time Date"+dateForFilter);
							
							
							logger.log(Level.SEVERE,"Date Before Adding "+today);
							Calendar cal1=Calendar.getInstance();
							cal1.setTime(today);
							
							
							
							Date tadateForFilter=null;
							
							try {
								tadateForFilter=fmtPaymentDate.parse(fmtPaymentDate.format(cal1.getTime()));
								cal1.set(Calendar.HOUR_OF_DAY,0);
								cal1.set(Calendar.MINUTE,0);
								cal1.set(Calendar.SECOND,0);
								cal1.set(Calendar.MILLISECOND,0);
								tadateForFilter=cal1.getTime();
							} catch (ParseException e) {
								e.printStackTrace();
							}
							
							logger.log(Level.SEVERE,"Date  After setting the Time Date"+tadateForFilter);
							
							/************************************* End *********************************/
							
							
					 try{
						 
						 logger.log(Level.SEVERE,"In Payment AP List");	
						logger.log(Level.SEVERE,"Date After Adding One Date"+tadateForFilter);

						 
					/********************************Adding status in the list ***********************/
						 
						 ArrayList<String> obj = new ArrayList<String>();
						 obj.add("Created");
						 
						 
					
					/******************************Converting todayDate to String format*****************/
						 
						 Date todaysDate = new Date();
						 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
						 String todayDateString = df.format(todaysDate);
						 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);

						 
				 /********************************Adding Companies in the list ***********************/
						 
//					List<Company> compEntity = ofy().load().type(Company.class).list();
//					if(compEntity.size()>0)
					{
					
						logger.log(Level.SEVERE,"If compEntity size > 0");
						logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
						

//						for(int i=0;i<compEntity.size();i++)
						{
							
//							Company c=compEntity.get(i);
							logger.log(Level.SEVERE,"In the for loop");
							logger.log(Level.SEVERE,"Company Name="+c);
							logger.log(Level.SEVERE,"The value of i is:" +i);
							logger.log(Level.SEVERE,"Date After Adding One Date"+dateForFilter);

							
				 /********************************Checking prosname & prosconfig for each company ***********************/

//							ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
//							ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
//							
//							boolean processconfigflag = false;
//							if(processconfig!=null){
//								if(processconfig.isConfigStatus()){
//									for(int l=0;l<processconfig.getProcessList().size();l++){
//										if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("PaymentAPDailyEmail") && processconfig.getProcessList().get(l).isStatus()==true){
//											processconfigflag=true;
//										}
//									}
//							   }
//							}
//							logger.log(Level.SEVERE,"process config  SMS Flag =="+processconfigflag);
							
//							if(processName!=null)
							{
								logger.log(Level.SEVERE,"In the prossName");
//							if(processconfigflag)
							{
											
								logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
								
								
								List<Employee>  employeeList=new ArrayList<Employee>();
								employeeList=ofy().load().type(Employee.class).filter("companyId", c.getCompanyId()).filter("roleName", cronDetails.getEmployeeRole()).list();
								logger.log(Level.SEVERE,""+cronDetails.getEmployeeRole()+" "+"Employee List size::: "+employeeList.size());
								
									for (Employee employee : employeeList) {
										ArrayList<String> toEmailListDt=new ArrayList<String>();
										toEmailListDt.add(employee.getEmail().trim());
										List<String> branchList=new ArrayList<String>();
										for (int j = 0; j < employee.getEmpBranchList().size(); j++) {
											branchList.add(employee.getEmpBranchList().get(j).getBranchName());
											logger.log(Level.SEVERE, "  -- branch list --"+ employee.getEmpBranchList().get(j).getBranchName());
										}
										logger.log(Level.SEVERE,"toEmailList size::: "+toEmailListDt.size()+" Branch List SIze : "+branchList.size());
										
										if(branchList.size()!=0){
											{
												List<CustomerPayment> custPaymentEntity = new ArrayList<CustomerPayment>();
												logger.log(Level.SEVERE," payment date fil" + dateForFilter +" get inter --" + tadateForFilter);
												if(cronDetails.isOverdueStatus()){
													custPaymentEntity = ofy().load().type(CustomerPayment.class)
															.filter("companyId",compEntity.get(i).getCompanyId()).filter("paymentDate <=",dateForFilter)
															.filter("branch IN", branchList)
															.filter("status IN",obj).filter("accountType", AppConstants.BILLINGACCOUNTTYPEAP).list();
													
												}else{
													custPaymentEntity = ofy().load().type(CustomerPayment.class)
															.filter("companyId",compEntity.get(i).getCompanyId())
															.filter("paymentDate >=",tadateForFilter).filter("paymentDate <=",dateForFilter)
															.filter("branch IN", branchList)
															.filter("status IN",obj).filter("accountType", AppConstants.BILLINGACCOUNTTYPEAP).list();
													
												}
												
											
												logger.log(Level.SEVERE,"Today date Is:"+dateForFilter);	

												logger.log(Level.SEVERE,"Paymnet entity size:"+custPaymentEntity.size());	
												
												
												// email id is added to emailList
												ArrayList<String> toEmailList=new ArrayList<String>();
												toEmailList.add(employee.getEmail());
												
												 String mailTitl = "Payment Due As On Date ";
												
												 if(cronDetails.isOverdueStatus()){
													 mailTitl = "Payment overdue as on date ";
												 }
												 
												if(custPaymentEntity.size()>0){
												
											
												ArrayList<String> tbl_header = new ArrayList<String>();
												tbl_header.add("Serial No");
												tbl_header.add("Branch");
												tbl_header.add("Customer Id");
												tbl_header.add("Customer Name");
												tbl_header.add("Customer Cont No");
												tbl_header.add("POC Name");
												tbl_header.add("Email Id");
												tbl_header.add("Sales Person");
												tbl_header.add("Person Responsible");
												tbl_header.add("OrderId");
												tbl_header.add("Invoice Id");
												tbl_header.add("Invoice Date");
												tbl_header.add("Payment Doc Id");
												tbl_header.add("Payment Amt");
												tbl_header.add("Payment Date");
												tbl_header.add("Status");
												tbl_header.add("Ageing");
												
								/********************************Sorting table with Branch & Payment Date ***********************/
											
												
												Comparator<CustomerPayment> custpaymentDateComparator2 = new Comparator<CustomerPayment>() {
													public int compare(CustomerPayment s1, CustomerPayment s2) {
													
													Date date1 = s1.getPaymentDate();
													Date date2 = s2.getPaymentDate();
													
													//ascending order
													return date1.compareTo(date2);
													}
													};
													Collections.sort(custPaymentEntity, custpaymentDateComparator2);
													
													Comparator<CustomerPayment> custpaymentDateComparator = new Comparator<CustomerPayment>() {
														public int compare(CustomerPayment s1, CustomerPayment s2) {
														
														String branch1 = s1.getBranch();
														String branch2 = s2.getBranch();
														
														//ascending order
														return branch1.compareTo(branch2);
														}
														
														};
														Collections.sort(custPaymentEntity, custpaymentDateComparator);
													
								/********************************Getting PaymnetEntity data and adding in the tbl1 List ***********************/
															
												ArrayList<String> tbl1=new ArrayList<String>();
													
												for(int j=0;j<custPaymentEntity.size();j++){
													
													Customer customerEntity=ofy().load().type(Customer.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("count", custPaymentEntity.get(j).getPersonInfo().getCount()).first().now();
													
													tbl1.add((j+1)+"");
										   	 		tbl1.add(custPaymentEntity.get(j).getBranch()+"");
										   	 		tbl1.add(custPaymentEntity.get(j).getPersonInfo().getCount()+"");
										   	 		tbl1.add(custPaymentEntity.get(j).getPersonInfo().getFullName()+"");		   	 		
										   	 		tbl1.add(custPaymentEntity.get(j).getPersonInfo().getCellNumber()+"");
										   	 		tbl1.add(custPaymentEntity.get(j).getPersonInfo().getPocName());
										   	 		tbl1.add(customerEntity.getEmail());
										   	 		tbl1.add(custPaymentEntity.get(j).getEmployee());
										   	 		tbl1.add(custPaymentEntity.get(j).getEmployee());
										   	 		tbl1.add(custPaymentEntity.get(j).getContractCount()+"");
										   	 		tbl1.add(custPaymentEntity.get(j).getInvoiceCount()+"");
										   	 		tbl1.add(fmt.format(custPaymentEntity.get(j).getInvoiceDate())+""); 	 		
										   	 		tbl1.add(custPaymentEntity.get(j).getCount()+"");
										   	 	    tbl1.add(custPaymentEntity.get(j).getPaymentAmt()+"");
										   	 	    tbl1.add(fmt.format(custPaymentEntity.get(j).getPaymentDate()) +"");
										 
										   	 	    /**************** for getting ageing for each Payment ******/
										   	 	    String StringPaymnetDate = fmtPaymentDate.format(custPaymentEntity.get(j).getPaymentDate());
										   	 	    
//										   	 	   	String StringPaymnetDate = df.format(paymentDate);	
										   			
										   				Date d1 = null;
										   				Date d2 = null;
										   				
										   				d1 = df.parse(StringPaymnetDate);
										   				d2 = df.parse(todayDateString);
										   				long diff = d2.getTime() - d1.getTime();
														long diffDays = diff / (24 * 60 * 60 * 1000);
														
										   	 	    tbl1.add(custPaymentEntity.get(j).getStatus()+"");
										   	 	    tbl1.add(diffDays +"");
										   	 	}			
												
												logger.log(Level.SEVERE,"In the ProsConfig !=null   3 ");
												
								   /********************************Calling cronsendEmail Method ***********************/
												
//												Email e=new Email();					
												try {   
													   
														System.out.println("Before send mail method");
														logger.log(Level.SEVERE,"Before send method call to send mail  ");
														
														String footer = "";
														
														if(cronDetails.getFooter().trim()!= "" ){
															footer = cronDetails.getFooter();
														}
														
														
														String emailSubject =   "Payment due as on date"+" "+ todayDateString;
														 if(cronDetails.isOverdueStatus()){
															 emailSubject = "Payment overdue as on date ";
														 }
														 
														if(!cronDetails.getSubject().trim().equals("")  && cronDetails.getSubject().trim().length() >0){
															emailSubject = cronDetails.getSubject();
														}
														
														String emailMailBody =mailTitl +"  "+ todayDateString;
														
														if(!cronDetails.getMailBody().trim().equals("")  && cronDetails.getMailBody().trim().length() >0){
															emailMailBody = cronDetails.getMailBody() ;
														}
														
															logger.log(Level.SEVERE,"Before send method call to send mail  ");						
//															cronEmail.cronSendEmail(toEmailList, "Invoice Due As On Date"+" "+ todayDateString, mailTitl +"  "+ todayDateString, c, tbl_header, tbl1, null, null, null, null);
															
														
														cronEmail.cronSendEmail(toEmailList,emailSubject,
																emailMailBody, c, tbl_header, tbl1, null, null, null, null,"",footer);
														logger.log(Level.SEVERE,"After send mail method ");		
																//+"  "+
												} catch (IOException e1) {
													
													logger.log(Level.SEVERE,"In the catch block ");
													e1.printStackTrace();
											}
												
											}
											else{					// else block for if(serviceEntity.size()>0){ 		
													
												System.out.println("Sorry no Payment found for this company");
												logger.log(Level.SEVERE,"Sorry no Payment found for this company");
//												
												mailTitl = "No Payment Found Due";

												cronEmail.cronSendEmail(toEmailList, "Client Payment Due As On Date"+" "+ todayDateString, mailTitl , c, null, null, null, null, null, null);

											}
											
											}
										}
									}
					/********************************Reading PaymentList from CustomerPayment entity  ***********************/
									
								
							}
							
							}
//							else{        //else block for pross!=null
//								logger.log(Level.SEVERE,"Cron job status is Inactive");
//								System.out.println("Cron job status is Inactive");
//							}
							
											
						}	//end of for loop
						
					}
					
					
					
					 }catch(Exception e2){
					
					e2.printStackTrace();
					 }
						
							
						}
					}
				}
				
				
				
			}catch(Exception e){
				logger.log(Level.SEVERE,"get error in "+e);
			}
			
		
			
			
	}

}
