package com.slicktechnologies.server.cronjobimpl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;

public class CustomerARPaymentTDSCertificateListCronJob extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5777514557007378606L;
	
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private DateFormat fmtPaymentDate = new SimpleDateFormat("dd/MM/yyyy");
	 Logger logger = Logger.getLogger("NameOfYourLogger");
	 
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		customerARPaymentTDSCertificatelist();
	}

	private void customerARPaymentTDSCertificatelist() {


		Email cronEmail = new Email();
		
		Date today=DateUtility.getDateWithTimeZone("IST", new Date());
		
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmtPaymentDate.setTimeZone(TimeZone.getTimeZone("IST"));
		
 try{
	 
	 logger.log(Level.SEVERE,"In Payment AR List");	

	 
/********************************Adding status in the list ***********************/
	 
	 ArrayList<String> obj = new ArrayList<String>();
//	 obj.add("Created");
	 obj.add("Closed");
	 
/******************************Converting todayDate to String format*****************/
	 
	 Date todaysDate = new Date();
	 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	 String todayDateString = df.format(todaysDate);
	 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);

	 
/********************************Adding Companies in the list ***********************/
	 
List<Company> compEntity = ofy().load().type(Company.class).list();
if(compEntity.size()>0){

	logger.log(Level.SEVERE,"If compEntity size > 0");
	logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());

	for(int i=0;i<compEntity.size();i++){
		
		Company c=compEntity.get(i);
		logger.log(Level.SEVERE,"Company Name="+c);
		
/********************************Checking prosname & prosconfig for each company ***********************/
	
		ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
		ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
		
		boolean processconfigflag = false;
		if(processconfig!=null){
			if(processconfig.isConfigStatus()){
				for(int l=0;l<processconfig.getProcessList().size();l++){
					if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("PaymentARTDSCertificateDueList") && processconfig.getProcessList().get(l).isStatus()==true){
						processconfigflag=true;
					}
				}
		   }
		}
		logger.log(Level.SEVERE,"process config Flag =="+processconfigflag);
		
		if(processName!=null){
			logger.log(Level.SEVERE,"In the prossName");
		if(processconfigflag){
						
			logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
			
			
/********************************Reading PaymentList from CustomerPayment entity  ***********************/

			List<CustomerPayment> custPaymentEntity = ofy().load().type(CustomerPayment.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("tdsApplicable", true).filter("tdsCertificateReceived", false).filter("status IN",obj).filter("accountType", AppConstants.BILLINGACCOUNTTYPEAR).list();
			
			logger.log(Level.SEVERE,"Paymnet entity size:"+custPaymentEntity.size());	
			
			 String mailTitl = "TDS Certificate Due";
			
			// email id is added to emailList
			ArrayList<String> toEmailList=new ArrayList<String>();
			toEmailList.add(compEntity.get(i).getPocEmail());
				
				
/********************************Getting PaymnetEntity data and adding in the tbl1 List ***********************/
			ArrayList<String> tbl_header=new ArrayList<String>();
			ArrayList<String> tbl1=new ArrayList<String>();
			
			for(int j=0;j<custPaymentEntity.size();j++){
				
				// email id is added to emailList
				ArrayList<String> toEmailList1 =new ArrayList<String>();
				toEmailList1.add(compEntity.get(i).getPocEmail());
					
					//Table header 1
					if(j==0){
						tbl_header.add("Serial No");
						tbl_header.add("Branch");
						tbl_header.add("Customer Id");
						tbl_header.add("Customer Name");
						tbl_header.add("Customer Cell No");
						tbl_header.add("POC Name");
						tbl_header.add("Invoice Id");
						tbl_header.add("Invoice Date");
						tbl_header.add("Payment Id");
						tbl_header.add("Payment Date");
						tbl_header.add("TDS Deducted");
						tbl_header.add("Status");
						tbl_header.add("Ageing");
					}
						
					tbl1.add(j+1+"");
		   	 		tbl1.add(custPaymentEntity.get(j).getBranch()+"");
		   	 		tbl1.add(custPaymentEntity.get(j).getPersonInfo().getCount()+"");
		   	 		tbl1.add(custPaymentEntity.get(j).getPersonInfo().getFullName()+"");
		   	 		tbl1.add(custPaymentEntity.get(j).getPersonInfo().getCellNumber()+"");
		   	 		tbl1.add(custPaymentEntity.get(j).getPersonInfo().getPocName());
		   	 		tbl1.add(custPaymentEntity.get(j).getInvoiceCount()+"");
		   	 		tbl1.add(fmt.format(custPaymentEntity.get(j).getInvoiceDate()) +"");
		   	 		tbl1.add(custPaymentEntity.get(j).getCount()+"");
		   	 		tbl1.add(fmt.format(custPaymentEntity.get(j).getPaymentDate()) +"");
		   	 		if(custPaymentEntity.get(j).getTdsTaxValue()==0){
			   	 		tbl1.add(" ");
		   	 		}else{
			   	 		tbl1.add(custPaymentEntity.get(j).getTdsTaxValue()+"");
		   	 		}
		   	 		
		   	 		tbl1.add(custPaymentEntity.get(j).getStatus()+"");
		   	 		
		   	     /**************** for getting ageing for each Payment ******/
			   	 	String StringPaymentDate = fmtPaymentDate.format(custPaymentEntity.get(j).getPaymentDate());

	   				Date d1 = null;
	   				Date d2 = null;
	   				
	   				d1 = df.parse(StringPaymentDate);
	   				d2 = df.parse(todayDateString);
	   				long diff = d2.getTime() - d1.getTime();;
	   				long diffDays = diff / (24 * 60 * 60 * 1000);
	   				tbl1.add(diffDays +"");
				}
				logger.log(Level.SEVERE,"Before send method call to send mail  ");						
				cronEmail.cronSendEmail(toEmailList, "TDS Certificate Due ", mailTitl, c, tbl_header, tbl1, null, null, null, null);
		}
		else{					//else block for prosconfig
				logger.log(Level.SEVERE,"ProcessConfiguration is null");
				System.out.println("ProcessConfiguration is null");
		}
		
		}else{        //else block for pross!=null
			logger.log(Level.SEVERE,"Cron job status is Inactive");
			System.out.println("Cron job status is Inactive");
		}
		
						
	}	//end of for loop
	
	}
	else{			//else block for if(compEntity.size()>0)
		
		logger.log(Level.SEVERE,"No Company found from cron job completed");	
		System.out.println("No Company found from cron job completed");
	}


	 }catch(Exception e2){
	
		 e2.printStackTrace();
	 }
	
	}
	


}
