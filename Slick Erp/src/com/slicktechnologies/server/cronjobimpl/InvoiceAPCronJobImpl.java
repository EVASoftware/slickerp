package com.slicktechnologies.server.cronjobimpl;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class InvoiceAPCronJobImpl extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1888560732775130699L;
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmtInvoiceDate = new SimpleDateFormat("dd/MM/yyyy");
	 Logger logger = Logger.getLogger("NameOfYourLogger");
	  
		public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws IOException {
			
//				invoiceAPlist();
		}

		private void invoiceAPlist() {

			

			
			Email cronEmail = new Email();
			
			Date today=DateUtility.getDateWithTimeZone("IST", new Date());
			
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			fmtInvoiceDate.setTimeZone(TimeZone.getTimeZone("IST"));
			
			/**
			 *    i have added time in today's date
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding "+today);
			Calendar cal=Calendar.getInstance();
			cal.setTime(today);
			cal.add(Calendar.DATE, 0);
			
			Date dateForFilter=null;
			
			try {
				dateForFilter=fmtInvoiceDate.parse(fmtInvoiceDate.format(cal.getTime()));
				cal.set(Calendar.HOUR_OF_DAY,23);
				cal.set(Calendar.MINUTE,59);
				cal.set(Calendar.SECOND,59);
				cal.set(Calendar.MILLISECOND,999);
				dateForFilter=cal.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Date After setting the Time"+dateForFilter);
			
			/************************************* End *********************************/
			
			
	 try{
		 
		 logger.log(Level.SEVERE,"In Invoice AP List");	
		 
	/********************************Adding status in the list ***********************/
		 
		 ArrayList<String> obj = new ArrayList<String>();
		 obj.add("Created");
		 obj.add("Requested");
		 
		 
	
	/******************************Converting todayDate to String format*****************/
		 
		 Date todaysDate = new Date();
		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 String todayDateString = df.format(todaysDate);
		 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);

		 
 /********************************Adding Companies in the list ***********************/
		 
	List<Company> compEntity = ofy().load().type(Company.class).list();
	if(compEntity.size()>0){
	
		logger.log(Level.SEVERE,"If compEntity size > 0");
		logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
		

		for(int i=0;i<compEntity.size();i++){
			
			Company c=compEntity.get(i);
			logger.log(Level.SEVERE,"In the for loop");
			logger.log(Level.SEVERE,"Company Name="+c);
			logger.log(Level.SEVERE,"The value of i is:" +i);
			logger.log(Level.SEVERE,"Date After Adding One Date"+dateForFilter);

			
 /********************************Checking prosname & prosconfig for each company ***********************/

			ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
			ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
			
			boolean processconfigflag = false;
			if(processconfig!=null){
				if(processconfig.isConfigStatus()){
					for(int l=0;l<processconfig.getProcessList().size();l++){
						if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("InvoiceAPDailyEmail") && processconfig.getProcessList().get(l).isStatus()==true){
							processconfigflag=true;
						}
					}
			   }
			}
			logger.log(Level.SEVERE,"process config  SMS Flag =="+processconfigflag);
			
			if(processName!=null){
				logger.log(Level.SEVERE,"In the prossName");
			if(processconfigflag){
							
				logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
				
				
	/********************************Reading Invoice List from Invoice entity  ***********************/
				
				List<Invoice> invoiceEntity = ofy().load().type(Invoice.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("invoiceDate <=",dateForFilter).filter("status IN",obj).filter("accountType", AppConstants.BILLINGACCOUNTTYPEAP).list();
				
				logger.log(Level.SEVERE,"Today date Is:"+dateForFilter);	

				logger.log(Level.SEVERE,"Invoice entity size:"+invoiceEntity.size());	
				
				
				// email id is added to emailList
				ArrayList<String> toEmailList=new ArrayList<String>();
				toEmailList.add(compEntity.get(i).getPocEmail());
				
				 String mailTitl = "Invoice Due As On Date";
				
				if(invoiceEntity.size()>0){
				
			
				ArrayList<String> tbl_header = new ArrayList<String>();
				tbl_header.add("Serial No");
				tbl_header.add("Branch");
				tbl_header.add("Customer Id");
				tbl_header.add("Customer Name");
				tbl_header.add("Customer Cont No");
				tbl_header.add("POC Name");
				tbl_header.add("Email Id");
				tbl_header.add("Sales Person");
				tbl_header.add("Person Responsible");
				tbl_header.add("OrderId");
				tbl_header.add("Invoice Id");
				tbl_header.add("Invoice Amt");
				tbl_header.add("Invoice Date");
				tbl_header.add("Status");
				tbl_header.add("Ageing");
				
/********************************Sorting table with Branch & Invoice Date ***********************/
			
				
				Comparator<Invoice> invoiceDateComparator2 = new Comparator<Invoice>() {
					public int compare(Invoice s1, Invoice s2) {
					
					Date date1 = s1.getInvoiceDate();
					Date date2 = s2.getInvoiceDate();
					
					//ascending order
					return date1.compareTo(date2);
					}
					};
					Collections.sort(invoiceEntity, invoiceDateComparator2);
					
					Comparator<Invoice> invoiceDateComparator = new Comparator<Invoice>() {
						public int compare(Invoice s1, Invoice s2) {
						
						String branch1 = s1.getBranch();
						String branch2 = s2.getBranch();
						
						//ascending order
						return branch1.compareTo(branch2);
						}
						
						};
						Collections.sort(invoiceEntity, invoiceDateComparator);
					
/********************************Getting invoiceEntity data and adding in the tbl1 List ***********************/
							
				ArrayList<String> tbl1=new ArrayList<String>();
					
				for(int j=0;j<invoiceEntity.size();j++){
					
					Customer customerEntity=ofy().load().type(Customer.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("count", invoiceEntity.get(j).getPersonInfo().getCount()).first().now();
					
					tbl1.add((j+1)+"");
		   	 		tbl1.add(invoiceEntity.get(j).getBranch()+"");
		   	 		tbl1.add(invoiceEntity.get(j).getPersonInfo().getCount()+"");
		   	 		tbl1.add(invoiceEntity.get(j).getPersonInfo().getFullName()+"");		   	 		
		   	 		tbl1.add(invoiceEntity.get(j).getPersonInfo().getCellNumber()+"");
		   	 		tbl1.add(invoiceEntity.get(j).getPersonInfo().getPocName());
		   	 		tbl1.add(customerEntity.getEmail());
		   	 		tbl1.add(invoiceEntity.get(j).getEmployee());
		   	 		tbl1.add(invoiceEntity.get(j).getEmployee());
		   	 		tbl1.add(invoiceEntity.get(j).getContractCount()+"");
		   	 		tbl1.add(invoiceEntity.get(j).getCount()+"");
		   	 		tbl1.add(invoiceEntity.get(j).getInvoiceAmount()+"");
		   	 		tbl1.add(fmt.format(invoiceEntity.get(j).getInvoiceDate())+""); 	 		
		 
		   	 	    /**************** for getting ageing for each Invoice ******/
		   	 		String StringInvoiceDate = fmtInvoiceDate.format(invoiceEntity.get(j).getInvoiceDate());
		   	 	    
		   				Date d1 = null;
		   				Date d2 = null;
		   				
		   				d1 = df.parse(StringInvoiceDate);
		   				d2 = df.parse(todayDateString);
		   				long diff = d2.getTime() - d1.getTime();
						long diffDays = diff / (24 * 60 * 60 * 1000);
						
//						System.out.println("Ageing:"+diffDays);
//				   	 	logger.log(Level.SEVERE,"Ageing:"+diffDays);
			
		   	 	    tbl1.add(invoiceEntity.get(j).getStatus()+"");
		   	 	    tbl1.add(diffDays +"");
		   	 	}			
				
				logger.log(Level.SEVERE,"In the ProsConfig !=null   3 ");
				
   /********************************Calling cronsendEmail Method ***********************/
				
				try {   
					   
						System.out.println("Before send mail method");
						logger.log(Level.SEVERE,"Before send method call to send mail  ");						
						cronEmail.cronSendEmail(toEmailList, "Invoice Due As On Date"+" "+ todayDateString, mailTitl +"  "+ todayDateString, c, tbl_header, tbl1, null, null, null, null);
						logger.log(Level.SEVERE,"After send mail method ");		
								//+"  "+
				} catch (IOException e1) {
					
					logger.log(Level.SEVERE,"In the catch block ");
					e1.printStackTrace();
			}
				
			}
			else{					
					
				System.out.println("Sorry no Invoice found for this company");
				logger.log(Level.SEVERE,"Sorry no Invoice found for this company");
//				
				mailTitl = "No Invoice Found Due";

				cronEmail.cronSendEmail(toEmailList, "Invoice Due As On Date"+" "+ todayDateString, mailTitl , c, null, null, null, null, null, null);

			}
				
				
			}
			else{					//else block for prosconfig
					logger.log(Level.SEVERE,"ProcessConfiguration is null");
					System.out.println("ProcessConfiguration is null");
			}
			
			}else{        //else block for pross!=null
				logger.log(Level.SEVERE,"Cron job status is Inactive");
				System.out.println("Cron job status is Inactive");
			}
			
							
		}	//end of for loop
		
	}
	else{			//else block for if(compEntity.size()>0)
		
		logger.log(Level.SEVERE,"No Company found from cron job completed");	
		System.out.println("No Company found from cron job completed");
	}
	
	
	 }catch(Exception e2){
	
	e2.printStackTrace();
	 }
		
			
			
	
		
			
		}

		/**
		 *  nidhi
		 *  1-03-2018
		 *  
		 */
public void invoiceAPlist(String cronList) {

			

//			String strCronDetails = req.getParameter("cronList");
			logger.log(Level.SEVERE, "Cron list str --" + cronList);
			Gson gson = new Gson();
			JSONArray jsonarr = null;
			try {
				jsonarr=new JSONArray(cronList.trim());
			} catch (JSONException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			
			try {
				CronJobConfigrationDetails cronDetails =null;
				
				List<Company> compEntity = ofy().load().type(Company.class).list();
				if(compEntity.size()>0){
				
					logger.log(Level.SEVERE,"If compEntity size > 0");
					logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
					

					for(int i=0;i<compEntity.size();i++){
						
						Company c=compEntity.get(i);
						
						
						for(int k=0;k<jsonarr.length();k++){
							JSONObject jsonObj = jsonarr.getJSONObject(k);
							cronDetails = new CronJobConfigrationDetails();
							
							cronDetails = gson.fromJson(jsonObj.toString(), CronJobConfigrationDetails.class);
							

							
							Email cronEmail = new Email();
							
							Date today=DateUtility.getDateWithTimeZone("IST", new Date());
							
							TimeZone.setDefault(TimeZone.getTimeZone("IST"));
							fmt.setTimeZone(TimeZone.getTimeZone("IST"));
							fmtInvoiceDate.setTimeZone(TimeZone.getTimeZone("IST"));
							
//							/**
//							 *    i have added time in today's date
//							 */
//							
							logger.log(Level.SEVERE,"Date Before Adding "+today);
							Calendar cal=Calendar.getInstance();
							cal.setTime(today);
							cal.add(Calendar.DATE, 0);
							Date dateForFilter=null;
							
							try {
								dateForFilter=fmtInvoiceDate.parse(fmtInvoiceDate.format(cal.getTime()));
								cal.set(Calendar.HOUR_OF_DAY,0);
								cal.set(Calendar.MINUTE,0);
								cal.set(Calendar.SECOND,0);
								cal.set(Calendar.MILLISECOND,0);
								dateForFilter=cal.getTime();
							} catch (ParseException e) {
								e.printStackTrace();
							}
							
							
							
							logger.log(Level.SEVERE,"Date After setting the Time"+dateForFilter);
							
							cal.setTime(today);
							int diffDay = 0;
							 if(cronDetails.getOverdueDays()>0 && cronDetails.isOverdueStatus()){
								 diffDay = -cronDetails.getOverdueDays();
							 }else{
								 diffDay = cronDetails.getInterval();
							 }
							
							cal.add(Calendar.DATE, diffDay);
							
							Date duedateForFilter=null;
							
							try {
								duedateForFilter=fmtInvoiceDate.parse(fmtInvoiceDate.format(cal.getTime()));
								cal.set(Calendar.HOUR_OF_DAY,23);
								cal.set(Calendar.MINUTE,59);
								cal.set(Calendar.SECOND,59);
								cal.set(Calendar.MILLISECOND,999);
								duedateForFilter=cal.getTime();
							} catch (ParseException e) {
								e.printStackTrace();
							}
							
							
							logger.log(Level.SEVERE,"Date After setting time"+duedateForFilter);
							
							
							/************************************* End *********************************/
							
							
					 try{
						 
						 logger.log(Level.SEVERE,"In Invoice AP List");	
						 
					/********************************Adding status in the list ***********************/
						 
						 ArrayList<String> obj = new ArrayList<String>();
						 obj.add("Created");
						 obj.add("Requested");
						 
						 
					
					/******************************Converting todayDate to String format*****************/
						 
						 Date todaysDate = new Date();
						 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
						 String todayDateString = df.format(todaysDate);
						 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);

						 
				 /********************************Adding Companies in the list ***********************/
						 
//					List<Company> compEntity = ofy().load().type(Company.class).list();
//					if(compEntity.size()>0)
					{
					
						logger.log(Level.SEVERE,"If compEntity size > 0");
						logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
						

//						for(int i=0;i<compEntity.size();i++)
						{
							
//							Company c=compEntity.get(i);
							logger.log(Level.SEVERE,"In the for loop");
							logger.log(Level.SEVERE,"Company Name="+c);
							logger.log(Level.SEVERE,"The value of i is:" +i);
							logger.log(Level.SEVERE,"Date After Adding One Date"+dateForFilter);

							/********************************Reading services from Service entity  ***********************/
										
								List<Employee>  employeeList=new ArrayList<Employee>();
								employeeList=ofy().load().type(Employee.class).filter("companyId", c.getCompanyId()).filter("roleName", cronDetails.getEmployeeRole()).list();
								logger.log(Level.SEVERE,""+cronDetails.getEmployeeRole()+" "+"Employee List size::: "+employeeList.size());
								
									for (Employee employee : employeeList) {
										ArrayList<String> toEmailListDt=new ArrayList<String>();
										toEmailListDt.add(employee.getEmail().trim());
										List<String> branchList=new ArrayList<String>();
										for (int j = 0; j < employee.getEmpBranchList().size(); j++) {
											branchList.add(employee.getEmpBranchList().get(j).getBranchName());
											logger.log(Level.SEVERE, "  -- branch list --"+ employee.getEmpBranchList().get(j).getBranchName());
										}
										logger.log(Level.SEVERE,"toEmailList size::: "+toEmailListDt.size()+" Branch List SIze : "+branchList.size());
										
										if(branchList.size()!=0){

											
											logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
											
											
								/********************************Reading Invoice List from Invoice entity  ***********************/
											
											List<Invoice> invoiceEntity = new ArrayList<Invoice>();
											
											if(cronDetails.isOverdueStatus()){
												
												cal.set(Calendar.HOUR_OF_DAY,23);
												cal.set(Calendar.MINUTE,59);
												cal.set(Calendar.SECOND,59);
												cal.set(Calendar.MILLISECOND,999);
												dateForFilter=cal.getTime();
												invoiceEntity = ofy().load().type(Invoice.class)
														.filter("companyId",compEntity.get(i).getCompanyId())
														.filter("invoiceDate <=",dateForFilter).filter("status IN",obj)
														.filter("branch In", branchList)
														.filter("accountType", AppConstants.BILLINGACCOUNTTYPEAP).list();
											}else{
												invoiceEntity = ofy().load().type(Invoice.class)
														.filter("companyId",compEntity.get(i).getCompanyId())
														.filter("invoiceDate <=",duedateForFilter)
														.filter("invoiceDate >=", dateForFilter).filter("status IN",obj)
														.filter("branch In", branchList)
														.filter("accountType", AppConstants.BILLINGACCOUNTTYPEAP).list();
											}
											
											
											logger.log(Level.SEVERE,"Today date Is:"+dateForFilter);	

											logger.log(Level.SEVERE,"Invoice entity size:"+invoiceEntity.size());	
											
											
											// email id is added to emailList
											ArrayList<String> toEmailList=new ArrayList<String>();
											toEmailList.add(employee.getEmail());
											
											
											
											if(invoiceEntity.size()>0){
											
										
											ArrayList<String> tbl_header = new ArrayList<String>();
											tbl_header.add("Serial No");
											tbl_header.add("Branch");
											tbl_header.add("Customer Id");
											tbl_header.add("Customer Name");
											tbl_header.add("Customer Cont No");
											tbl_header.add("POC Name");
											tbl_header.add("Email Id");
											tbl_header.add("Sales Person");
											tbl_header.add("Person Responsible");
											tbl_header.add("OrderId");
											tbl_header.add("Invoice Id");
											tbl_header.add("Invoice Amt");
											tbl_header.add("Invoice Date");
											tbl_header.add("Status");
											tbl_header.add("Ageing");
											
							/********************************Sorting table with Branch & Invoice Date ***********************/
										
											
											Comparator<Invoice> invoiceDateComparator2 = new Comparator<Invoice>() {
												public int compare(Invoice s1, Invoice s2) {
												
												Date date1 = s1.getInvoiceDate();
												Date date2 = s2.getInvoiceDate();
												
												//ascending order
												return date1.compareTo(date2);
												}
												};
												Collections.sort(invoiceEntity, invoiceDateComparator2);
												
												Comparator<Invoice> invoiceDateComparator = new Comparator<Invoice>() {
													public int compare(Invoice s1, Invoice s2) {
													
													String branch1 = s1.getBranch();
													String branch2 = s2.getBranch();
													
													//ascending order
													return branch1.compareTo(branch2);
													}
													
													};
													Collections.sort(invoiceEntity, invoiceDateComparator);
												
							/********************************Getting invoiceEntity data and adding in the tbl1 List ***********************/
														
											ArrayList<String> tbl1=new ArrayList<String>();
												
											for(int j=0;j<invoiceEntity.size();j++){
												
												Customer customerEntity=ofy().load().type(Customer.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("count", invoiceEntity.get(j).getPersonInfo().getCount()).first().now();
												
												tbl1.add((j+1)+"");
									   	 		tbl1.add(invoiceEntity.get(j).getBranch()+"");
									   	 		tbl1.add(invoiceEntity.get(j).getPersonInfo().getCount()+"");
									   	 		tbl1.add(invoiceEntity.get(j).getPersonInfo().getFullName()+"");		   	 		
									   	 		tbl1.add(invoiceEntity.get(j).getPersonInfo().getCellNumber()+"");
									   	 		tbl1.add(invoiceEntity.get(j).getPersonInfo().getPocName());
									   	 		tbl1.add(customerEntity.getEmail());
									   	 		tbl1.add(invoiceEntity.get(j).getEmployee());
									   	 		tbl1.add(invoiceEntity.get(j).getEmployee());
									   	 		tbl1.add(invoiceEntity.get(j).getContractCount()+"");
									   	 		tbl1.add(invoiceEntity.get(j).getCount()+"");
									   	 		tbl1.add(invoiceEntity.get(j).getInvoiceAmount()+"");
									   	 		tbl1.add(fmt.format(invoiceEntity.get(j).getInvoiceDate())+""); 	 		
									 
									   	 	    /**************** for getting ageing for each Invoice ******/
									   	 		String StringInvoiceDate = fmtInvoiceDate.format(invoiceEntity.get(j).getInvoiceDate());
									   	 	    
									   				Date d1 = null;
									   				Date d2 = null;
									   				
									   				d1 = df.parse(StringInvoiceDate);
									   				d2 = df.parse(todayDateString);
									   				long diff = d2.getTime() - d1.getTime();
													long diffDays = diff / (24 * 60 * 60 * 1000);
													
//													System.out.println("Ageing:"+diffDays);
//											   	 	logger.log(Level.SEVERE,"Ageing:"+diffDays);
										
									   	 	    tbl1.add(invoiceEntity.get(j).getStatus()+"");
									   	 	    tbl1.add(diffDays +"");
									   	 	}			
											
											logger.log(Level.SEVERE,"In the ProsConfig !=null   3 ");
											
							   /********************************Calling cronsendEmail Method ***********************/
											
											try {   
												String footer = "";
												
												if(cronDetails.getFooter().trim()!= "" ){
													footer = cronDetails.getFooter();
												}
												
												
												String emailSubject =  "Invoice due as on date :"+" "+ todayDateString;
												
												if(cronDetails.isOverdueStatus()){
													emailSubject = "Invoice overdue as on date :"+" "+ todayDateString;
												}

												if(!cronDetails.getSubject().trim().equals(""))
												{
													emailSubject = cronDetails.getSubject();
												}
												String emailMailBody = "Invoice due as on date :"+" "+ todayDateString;;
												if(cronDetails.isOverdueStatus()){
													emailMailBody = "Invoice overdue as on date :"+" "+ todayDateString;
												}
												if(!cronDetails.getMailBody().trim().equals("") && cronDetails.getMailBody().trim().length()>0){
													emailMailBody = cronDetails.getMailBody();
												}
												
													System.out.println("Before send mail method");
													logger.log(Level.SEVERE,"Before send method call to send mail  ");						
													
													cronEmail.cronSendEmail(toEmailList, emailSubject, emailMailBody, c, tbl_header, tbl1,
															null, null, null, null, "", footer);
													logger.log(Level.SEVERE,"After send mail method ");		
															//+"  "+
											} catch (IOException e1) {
												
												logger.log(Level.SEVERE,"In the catch block ");
												e1.printStackTrace();
										}
											
										}
										else{					
												
											System.out.println("Sorry no Invoice found for this company");
											logger.log(Level.SEVERE,"Sorry no Invoice found for this company");
//											

											cronEmail.cronSendEmail(toEmailList, "Invoice Due As On Date"+" "+ todayDateString, "No Invoice Found Due" , c, null, null, null, null, null, null);

										}
											
											
										
										}
									}
				 /********************************Checking prosname & prosconfig for each company ***********************/

							
							
							
							
						}	//end of for loop
						
					}
//					else{			//else block for if(compEntity.size()>0)
//						
//						logger.log(Level.SEVERE,"No Company found from cron job completed");	
//						System.out.println("No Company found from cron job completed");
//					}
					
					
					 }catch(Exception e2){
					
					e2.printStackTrace();
					 }
						
							
							
						}
					}
				}
				
				
			}catch(Exception e){
				logger.log(Level.SEVERE," invoice acc payment cron job"+e);
			}

	
		
			
		}
}
