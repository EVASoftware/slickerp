package com.slicktechnologies.server.cronjobimpl;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.relationalLayer.CustomerRelation;

public class CustomerLogCronJobImpl extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -96935257077864058L;
	/**
	 * 
	 */
	
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmtDate = new SimpleDateFormat("dd/MM/yyyy");
	Logger logger = Logger.getLogger("NameOfYourLogger");
	  
	public void doGet(HttpServletRequest req, HttpServletResponse resp)	throws IOException 
	{
		callLogList();
	}

	private void callLogList() 
	{
		Email cronEmail = new Email();
		Date today=DateUtility.getDateWithTimeZone("IST", new Date());
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmtDate.setTimeZone(TimeZone.getTimeZone("IST"));
		
		try{
	 
/******************************Converting todayDate to String format*****************/
			 Date todaysDate = new Date();
			 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			 String todayDateString = df.format(todaysDate);
			 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);
			 
				Calendar cal=Calendar.getInstance();
				cal.setTime(today);
				cal.add(Calendar.DATE, 0);
				
				Date dateForFilter=null;
				
				try {
					dateForFilter=df.parse(df.format(cal.getTime()));
					cal.set(Calendar.HOUR_OF_DAY,00);
					cal.set(Calendar.MINUTE,00);
					cal.set(Calendar.SECOND,00);
					cal.set(Calendar.MILLISECOND,000);
					dateForFilter=cal.getTime();
				} catch (ParseException e) {
					e.printStackTrace();
				}
/********************************Adding Companies in the list ***********************/
	 
	List<Company> compEntity = ofy().load().type(Company.class).list();
	if(compEntity.size()>0)
	{
		logger.log(Level.SEVERE,"If compEntity size > 0");
		logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());

		for(int i=0;i<compEntity.size();i++){
			
			Company c=compEntity.get(i);
			logger.log(Level.SEVERE,"In the for loop");
			logger.log(Level.SEVERE,"Company Name="+c);
			logger.log(Level.SEVERE,"The value of i is:" +i);
	
			
			/*************Checking prosname & prosconfig for each company *****************/
	
			ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
			ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
			
			boolean processconfigflag = false;
			if(processconfig!=null){
				if(processconfig.isConfigStatus()){
					for(int l=0;l<processconfig.getProcessList().size();l++){
						if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("CustomerLogEmail") && processconfig.getProcessList().get(l).isStatus()==true){
							processconfigflag=true;
						}
					}
			   }
			}
			logger.log(Level.SEVERE,"process config  SMS Flag =="+processconfigflag);
		
				if(processName!=null){
					logger.log(Level.SEVERE,"In the prossName");
					if(processconfigflag){
						logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
					
						/*****************Reading ServiceQuotationList from SalesQuotation entity  ************/
//						List<Employee>  employeeList=new ArrayList<Employee>();
//						employeeList=ofy().load().type(Employee.class).filter("companyId", c.getCompanyId()).list();
//						
//						
//							for (Employee employee : employeeList) {
//								ArrayList<String> toEmailListDt=new ArrayList<String>();
//								toEmailListDt.add(employee.getEmail().trim());
								List<Branch> branches=new ArrayList<Branch>();
								List<String> branchList=new ArrayList<String>();
								branches=ofy().load().type(Branch.class).filter("companyId", c.getCompanyId()).list();
								for (int j = 0; j < branches.size(); j++) {
									branchList.add(branches.get(j).getBusinessUnitName());
									logger.log(Level.SEVERE, "  -- branch list --"+ branches.get(j).getBusinessUnitName());
								}
//								logger.log(Level.SEVERE,"toEmailList size::: "+toEmailListDt.size()+" Branch List SIze : "+branchList.size());
//								
								if(branchList.size()!=0){	
									for(String branch : branchList){
										List<String> statusList = new ArrayList<String>();
										statusList.add(CustomerLogDetails.CLOSED);
										logger.log(Level.SEVERE,"customer log");				
					//filter("closingDate >=", dateForFilter)
										List<CustomerLogDetails> customerLog = ofy().load().type(CustomerLogDetails.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("status IN",statusList).filter("companyName", branch).filter("closingDate >=", dateForFilter).list();
					logger.log(Level.SEVERE,"customer log size:"+customerLog.size());					
					//.filter("closingDate >=", dateForFilter)
					// email id is added to emailList
					ArrayList<String> toEmailList=new ArrayList<String>();
					toEmailList.add(compEntity.get(i).getPocEmail());
					
					 String mailTitl = "Customer Call Log";
					 List<Double> warrantyList = new ArrayList<Double>();
					 List<Double> outOfWarrantyList = new ArrayList<Double>();
					 List<Double> amcList = new ArrayList<Double>();
					 logger.log(Level.SEVERE ," before if");
					if(customerLog.size()>0)
					{
						logger.log(Level.SEVERE ," after if");
						ArrayList<String> tbl_header = new ArrayList<String>();
						tbl_header.add("Serial No");
						tbl_header.add("Company Name");
						tbl_header.add("Creation Date");
						tbl_header.add("Warranty Status");
						tbl_header.add("Call Type");
						tbl_header.add("Customer Name");
						tbl_header.add("Technician Name");
						tbl_header.add("Is Scheme");
						tbl_header.add("Scheme Amount");
						tbl_header.add("Customer Payment");
						tbl_header.add("Balance Payment");
						tbl_header.add("Status");
						
						ArrayList<String> tbl_header1 = new ArrayList<String>();
						tbl_header1.add("Warranty Status");
						tbl_header1.add("Installation");
						tbl_header1.add("Demo");
						tbl_header1.add("PMS");
						tbl_header1.add("BreakDown");
						tbl_header1.add("Total");
//						tbl_header1.add("");
//						tbl_header1.add("out Installation");
//						tbl_header1.add("out Warranty Demo");
//						tbl_header1.add("out Warranty PMS");
//						tbl_header1.add("out Warranty BreakDown");
//						tbl_header1.add("Total");
//						tbl_header1.add("");
//						tbl_header1.add("amc Installation");
//						tbl_header1.add("amc Demo");
//						tbl_header1.add("amc PMS");
//						tbl_header1.add("amc BreakDown");
//						tbl_header1.add("Total");
//						tbl_header1.add("");
//						tbl_header1.add("Total Call Log");
						tbl_header1.add("Total Amount Received");
						tbl_header1.add("Total Amount Balance");
						
		/********************************Getting service Approval data and adding in the tbl1 List ***********************/
								
					ArrayList<String> tbl=new ArrayList<String>();
					ArrayList<String> tbl1=new ArrayList<String>();
					double totalamount = 0.0d;
					double balanceamount = 0.0d;
					for(int j=0;j<customerLog.size();j++){logger.log(Level.SEVERE ," in for loop");
						tbl.add("<center>"+(j+1)+"</center>");
			   	 		tbl.add(customerLog.get(j).getCompanyName()+"");			   	 	
			   	 		tbl.add(fmtDate.format(customerLog.get(j).getCreatedOn()));			   	 
			   	 		tbl.add(customerLog.get(j).getWarrantyStatus()+"");					   	 
			   	 		tbl.add(customerLog.get(j).getCallType()+"");			   	 
			   	 		tbl.add(customerLog.get(j).getCinfo().getFullName()+"");			   	 
			   	 		tbl.add(customerLog.get(j).getTechnician()+"");
			   	 		tbl.add(customerLog.get(j).isSchemeFlag()+"");		   	 
			   	 		tbl.add(customerLog.get(j).getSchemeAmount()+"");			   	 
			   	 		tbl.add(customerLog.get(j).getPaymentInfo().getPaymentReceived()+"");		   	 	
			   	 		tbl.add(customerLog.get(j).getPaymentInfo().getBalancePayment()+"");			   	 	
			   	 	    tbl.add(customerLog.get(j).getStatus()+"");			   	 	
			   	 	    totalamount = totalamount + customerLog.get(j).getPaymentInfo().getPaymentReceived();
			   	 	    balanceamount = balanceamount + customerLog.get(j).getPaymentInfo().getBalancePayment();
			   	 
					for(int m=0;m<=6;m++){
						warrantyList.add(0.0d);
						outOfWarrantyList.add(0.0d);
						amcList.add(0.0d);
					}
						
						String status =customerLog.get(j).getWarrantyStatus();
						logger.log(Level.SEVERE,"table:"+tbl.toString() +"status :" +status);
					    if(status.equalsIgnoreCase("Warranty")){
							warrantyList.set(0 ,warrantyList.get(0)+1);
							if(customerLog.get(j).getCallType().equalsIgnoreCase("Installation")){
								warrantyList.set(1 ,warrantyList.get(1)+1);
							}
							if(customerLog.get(j).getCallType().equalsIgnoreCase("Demo")){
								warrantyList.set(2 ,warrantyList.get(2)+1);
							}
							if(customerLog.get(j).getCallType().equalsIgnoreCase("PMS")){
								warrantyList.set(3 ,warrantyList.get(3)+1);
							}
							if(customerLog.get(j).getCallType().equalsIgnoreCase("BreakDown")){
								warrantyList.set(4 ,warrantyList.get(4)+1);
							}
							warrantyList.set(5, warrantyList.get(5)+customerLog.get(j).getPaymentInfo().getPaymentReceived());
							warrantyList.set(6, warrantyList.get(6)+customerLog.get(j).getPaymentInfo().getBalancePayment());
				   	 
					    }
					    if(status.equalsIgnoreCase("Out Of Warranty")){
							outOfWarrantyList.set(0 ,outOfWarrantyList.get(0)+1);
							if(customerLog.get(j).getCallType().equalsIgnoreCase("Installation")){
								outOfWarrantyList.set(1 ,outOfWarrantyList.get(1)+1);
							}
							if(customerLog.get(j).getCallType().equalsIgnoreCase("Demo")){
								outOfWarrantyList.set(2 ,outOfWarrantyList.get(2)+1);
							}
							if(customerLog.get(j).getCallType().equalsIgnoreCase("PMS")){
								outOfWarrantyList.set(3 ,outOfWarrantyList.get(3)+1);
							}
							if(customerLog.get(j).getCallType().equalsIgnoreCase("BreakDown")){
								outOfWarrantyList.set(4 ,outOfWarrantyList.get(4)+1);
							}
							outOfWarrantyList.set(5, outOfWarrantyList.get(5)+customerLog.get(j).getPaymentInfo().getPaymentReceived());
							outOfWarrantyList.set(6, outOfWarrantyList.get(6)+customerLog.get(j).getPaymentInfo().getBalancePayment());
				   	 
					    }
					    if(status.equalsIgnoreCase("AMC")){
							amcList.set(0 ,amcList.get(0)+1);
							if(customerLog.get(j).getCallType().equalsIgnoreCase("Installation")){
								amcList.set(1 ,amcList.get(1)+1);
							}
							if(customerLog.get(j).getCallType().equalsIgnoreCase("Demo")){
								amcList.set(2 ,amcList.get(2)+1);
							}
							if(customerLog.get(j).getCallType().equalsIgnoreCase("PMS")){
								amcList.set(3 ,amcList.get(3)+1);
							}
							if(customerLog.get(j).getCallType().equalsIgnoreCase("BreakDown")){
								amcList.set(4 ,amcList.get(4)+1);
							}
							amcList.set(5, amcList.get(5)+customerLog.get(j).getPaymentInfo().getPaymentReceived());
							amcList.set(6, amcList.get(6)+customerLog.get(j).getPaymentInfo().getBalancePayment());
						
						}						
			   	 	}			
					tbl1.add("Warranty");
					tbl1.add(warrantyList.get(1)+"");
					tbl1.add(warrantyList.get(2)+"");
					tbl1.add(warrantyList.get(3)+"");
					tbl1.add(warrantyList.get(4)+"");
					tbl1.add(warrantyList.get(0)+"");
					tbl1.add(warrantyList.get(5)+"");
					tbl1.add(warrantyList.get(6)+"");
					
					tbl1.add("Out Of Warranty");
					tbl1.add(outOfWarrantyList.get(1)+"");
					tbl1.add(outOfWarrantyList.get(2)+"");
					tbl1.add(outOfWarrantyList.get(3)+"");
					tbl1.add(outOfWarrantyList.get(4)+"");
					tbl1.add(outOfWarrantyList.get(0)+"");
					tbl1.add(outOfWarrantyList.get(5)+"");
					tbl1.add(outOfWarrantyList.get(6)+"");
					
					tbl1.add("AMC");
					tbl1.add(amcList.get(1)+"");
					tbl1.add(amcList.get(2)+"");
					tbl1.add(amcList.get(3)+"");
					tbl1.add(amcList.get(4)+"");
					tbl1.add(amcList.get(0)+"");
					tbl1.add(amcList.get(5)+"");
					tbl1.add(amcList.get(6)+"");
					
					tbl1.add("Total");
					tbl1.add("");
					tbl1.add("");
					tbl1.add("");
					tbl1.add("");
					tbl1.add(customerLog.size()+"");
					tbl1.add(totalamount+"");
					tbl1.add(balanceamount+"");
					logger.log(Level.SEVERE,"table:"+tbl1.toString());
					logger.log(Level.SEVERE,"In the ProsConfig !=null   3 ");
					
		/********************************Calling cronsendEmail Method ***********************/
					
					try {   
							System.out.println("Before send mail method");
							logger.log(Level.SEVERE,"Before send method call to send mail  ");						
							//cronEmail.cronSendEmail(toEmailList, "Call Log Details as on"+" "+ todayDateString, "Call Log Summary" +"  "+ todayDateString, c, tbl_header1, tbl1, mailTitl +"  "+ todayDateString, tbl_header, tbl, null);
							cronEmail.cronInventorySendEmail(toEmailList, "Call Log Details as on"+" "+ todayDateString, "Call Log Summary as on " +"  "+ todayDateString + " for "+customerLog.get(0).getCompanyName()+"", c, tbl_header1, tbl1, mailTitl +"  "+ todayDateString, null, tbl_header, tbl, null);
							logger.log(Level.SEVERE,"After send mail method ");		
									//+"  "+
					} catch (IOException e1) {
						
						logger.log(Level.SEVERE,"In the catch block ");
						e1.printStackTrace();
				}
					
				}								
				else{					
						
					System.out.println("Sorry no call log found for this company");
					logger.log(Level.SEVERE,"Sorry no call log found for this company");
					
					mailTitl = "No Call Log Found for "+ branch;
		
					cronEmail.cronSendEmail(toEmailList, "Call Log Details as on"+" "+ todayDateString, mailTitl , c, null, null, null, null, null, null);
		
				}
			}
		 }
								 
	   }
	  }else{        //else block for pross!=null
				logger.log(Level.SEVERE,"Cron job status is Inactive");
				System.out.println("Cron job status is Inactive");
			}
			
							
		}	//end of for loop
	//}
	}
		else{			//else block for if(compEntity.size()>0)
			
			logger.log(Level.SEVERE,"No Company found from cron job completed");	
			System.out.println("No Company found from cron job completed");
		}
		
		
		 }
	
		
		 catch(Exception e2){
		
			 e2.printStackTrace();
		 	}
			}	
	}
