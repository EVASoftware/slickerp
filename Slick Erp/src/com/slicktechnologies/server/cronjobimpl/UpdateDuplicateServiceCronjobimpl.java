package com.slicktechnologies.server.cronjobimpl;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.server.taskqueue.DocumentCancellationTaskQueue;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class UpdateDuplicateServiceCronjobimpl extends HttpServlet {
	
	
	
	private static final long serialVersionUID = -7274506665123429154L;
	
	Logger logger =Logger.getLogger("logger");
	SimpleDateFormat monthSdf = new SimpleDateFormat("MMM-yyyy");
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	
	DocumentCancellationTaskQueue docCancellationtasque=new DocumentCancellationTaskQueue();
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		logger.log(Level.SEVERE,"Inside DOGET in update duplicate service cronjob");
		
		Date today=DateUtility.getDateWithTimeZone("IST", new Date());
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		/**
		 * @author Anil @since 07-10-2021
		 * IST time zone was not set for below formatter
		 */
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		format.setTimeZone(TimeZone.getTimeZone("IST"));
		timeFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		monthSdf.setTimeZone(TimeZone.getTimeZone("IST"));
		
		Calendar cal=Calendar.getInstance();
		cal.setTime(today);
		cal.add(Calendar.DATE, 0);
		
		Date currentDate=null;
		
		try {
			currentDate=dateFormat.parse(dateFormat.format(cal.getTime()));
			cal.set(Calendar.HOUR_OF_DAY,00);
			cal.set(Calendar.MINUTE,00);
			cal.set(Calendar.SECOND,00);
			cal.set(Calendar.MILLISECOND,000);
			currentDate=cal.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		logger.log(Level.SEVERE,"Current Date"+currentDate);
		
		Calendar cal2=Calendar.getInstance();
		cal2.setTime(today);
		cal2.add(Calendar.DATE, 0);
		
		Date todaysDate=null;
		
		try {
			todaysDate=dateFormat.parse(dateFormat.format(cal2.getTime()));
			cal2.set(Calendar.HOUR_OF_DAY,23);
			cal2.set(Calendar.MINUTE,59);
			cal2.set(Calendar.SECOND,59);
			cal2.set(Calendar.MILLISECOND,999);
			todaysDate=cal2.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		logger.log(Level.SEVERE,"Today Date"+todaysDate);
		
		List<Company> compEntity = ofy().load().type(Company.class).list();
		if (compEntity.size() > 0) {
			for (Company comp : compEntity) {
				if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","UpdateDuplicateServices", comp.getCompanyId())) {
//					ArrayList<String> statusList = new ArrayList<String>();
//					statusList.add(Service.SERVICESTATUSSCHEDULE);
//					statusList.add(Service.SERVICESTATUSRESCHEDULE);
//					statusList.add(Service.SERVICESTATUSCOMPLETED);
//					List<Service> serviceListForCronjob = ofy().load().type(Service.class)
//							.filter("companyId", comp.getCompanyId())
//							.filter("status IN", statusList)
//							.filter("creationDate >=", currentDate)
//							.filter("creationDate <=", todaysDate).list();
//					docCancellationtasque.updateDuplicateServicesForContract(serviceListForCronjob, comp.getCompanyId(),sdf.format(currentDate), sdf.format(todaysDate));

					docCancellationtasque.updateDuplicateServices(comp.getCompanyId(), sdf.format(currentDate), sdf.format(todaysDate), 0+"",0+"",0+"",0+"",0+"",0+"");
					logger.log(Level.SEVERE, "end of duplicate service cronjob");
				}
			}

		}
		
	}

	
}
