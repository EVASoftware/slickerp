package com.slicktechnologies.server.cronjobimpl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.googlecode.objectify.ObjectifyService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.FeedbackUrlAsPerCustomerCategory;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class ServiceFeedbackCronJobImpl extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -324162794129566515L;
	
	Logger logger = Logger.getLogger("NameOfYourLogger");
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private DateFormat fmt1 = new SimpleDateFormat("dd/MM/yyyy");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
	}
	

public void servicelist(List<CronJobConfigrationDetails> cronList, HashSet<String> empRoleList) {
		
		logger.log(Level.SEVERE, "Cron list str --" + cronList);
			try {
				
				List<Company> compEntity = ofy().load().type(Company.class).list();
				 
				if(compEntity.size()>0)
				{
				
					logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
	
					for(int i=0;i<compEntity.size();i++){
										
						Company comp=compEntity.get(i);
						
						for(String empRoleName :empRoleList){
							
							if(!empRoleName.equalsIgnoreCase(AppConstants.CUSTOMER)){
								
								List<Employee>  employeeList=new ArrayList<Employee>();
								employeeList=ofy().load().type(Employee.class).filter("companyId",comp.getCompanyId()).filter("roleName", empRoleName).filter("status", true).list();
								logger.log(Level.SEVERE,""+empRoleName+" "+"Employee List size::: "+employeeList.size());
								List<String> branchList=new ArrayList<String>();
								
								for (Employee employee : employeeList) {

									branchList.add(employee.getBranchName());
									for (int j = 0; j < employee.getEmpBranchList().size(); j++) {
										branchList.add(employee.getEmpBranchList().get(j).getBranchName());
										logger.log(Level.SEVERE, "  -- branch list --"+ employee.getEmpBranchList().get(j).getBranchName());
									}
									logger.log(Level.SEVERE," Branch List SIze : "+branchList.size());
						
									if(branchList.size()!=0){
										sendLargecustomerFeedback(cronList,employee,branchList,comp);

									}
								}
								
							}
							else{
								sendLargecustomerFeedback(cronList,null,null,comp);
							}
							
						}	
			
					}
				}
				
			
	}catch(Exception e){
		e.printStackTrace();
		logger.log(Level.SEVERE," get error in service cron job"+e.getMessage());
	}
		 
}



private void sendLargecustomerFeedback(	List<CronJobConfigrationDetails> cronList, Employee employee,
		List<String> branchList, Company comp) {

	for(int k=0;k<cronList.size();k++){
		
		CronJobConfigrationDetails cronDetails =cronList.get(k);
		
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
		
		List<Customer> customerlist = ofy().load().type(Customer.class).filter("companyId", comp.getCompanyId())
						.filter("businessProcess.group", cronDetails.getCustomerGroup()).list();
		logger.log(Level.SEVERE, "customerlist "+customerlist.size());
		ArrayList<Integer> customeridlist = getcustomerCountlist(customerlist);
		logger.log(Level.SEVERE, "customeridlist "+customeridlist.size());

		if(customeridlist.size()>0) {
			
			ConfigCategory configEntity = ofy().load().type(ConfigCategory.class).filter("companyId", comp.getCompanyId())
					.filter("internalType", 30).filter("categoryName", cronDetails.getFrequencyType()).first().now();
		int frequencyDays = 0;
		try {
			frequencyDays = Integer.parseInt(configEntity.getDescription());	

		} catch (Exception e) {
			// TODO: handle exception
		}
		logger.log(Level.SEVERE, "frequencyDays"+frequencyDays);

		Date today1=DateUtility.getDateWithTimeZone("IST", new Date());
		Calendar cal1=Calendar.getInstance();
		cal1.setTime(today1);
		cal1.add(Calendar.DATE, -frequencyDays);
		
		Date activecontractfilter=null;
		
		try {
			activecontractfilter=fmt1.parse(fmt1.format(cal1.getTime()));
			cal1.set(Calendar.HOUR_OF_DAY,23);
			cal1.set(Calendar.MINUTE,23);
			cal1.set(Calendar.SECOND,59);
			cal1.set(Calendar.MILLISECOND,999);
			activecontractfilter=cal1.getTime();
		} catch (ParseException e) { 
			e.printStackTrace();
		}
		logger.log(Level.SEVERE, "activecontractfilter -31 days contracts "+activecontractfilter);

		Date today=DateUtility.getDateWithTimeZone("IST", new Date());
		Calendar cal=Calendar.getInstance();
		cal.setTime(today);
		cal.add(Calendar.DATE, 0);
		
		Date dateforfilter=null;
		
		try {
			dateforfilter=fmt1.parse(fmt1.format(cal.getTime()));
			cal.set(Calendar.HOUR_OF_DAY,23);
			cal.set(Calendar.MINUTE,23);
			cal.set(Calendar.SECOND,59);
			cal.set(Calendar.MILLISECOND,999);
			dateforfilter=cal.getTime();
		} catch (ParseException e) { 
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"dateforfilter"+dateforfilter);

		
		List<Contract> contractlist = ofy().load().type(Contract.class).filter("companyId", comp.getCompanyId())
						.filter("endDate >=", activecontractfilter).filter("status", Contract.APPROVED)
						.filter("cinfo.count IN", customeridlist).list();
		logger.log(Level.SEVERE, "Active contract list size "+contractlist.size());
		logger.log(Level.SEVERE, "Active contract list  "+contractlist);
		logger.log(Level.SEVERE, "comp.getMinimumDuration() "+comp.getMinimumDuration());

			for(Contract contractEntity : contractlist){
				logger.log(Level.SEVERE, "contractEntity.getCount() "+contractEntity.getCount());

				ArrayList<String> productcodelist = new ArrayList<String>();
				
				for(SalesLineItem saleslineitem : contractEntity.getItems()){
					Service service = null;
					
					int frequency = saleslineitem.getDuration()/saleslineitem.getNumberOfServices();
					logger.log(Level.SEVERE, "frequency"+frequency);

					if(frequency < comp.getMinimumDuration()){
						productcodelist.add(saleslineitem.getProductCode());
						
						List<Service> servicelist = null;
						if(branchList!=null && branchList.size()>0){
							servicelist = ofy().load().type(Service.class).filter("companyId", comp.getCompanyId())
									.filter("contractCount", contractEntity.getCount()).filter("status", Service.SERVICESTATUSCOMPLETED)
									.filter("serviceDate >=", activecontractfilter).filter("serviceDate <=", dateforfilter)
									.filter("product.productCode", saleslineitem.getProductCode()).filter("branch IN", branchList).list();
						}
						else{
							 servicelist = ofy().load().type(Service.class).filter("companyId", comp.getCompanyId())
									.filter("contractCount", contractEntity.getCount()).filter("status", Service.SERVICESTATUSCOMPLETED)
									.filter("serviceDate >=", activecontractfilter).filter("serviceDate <=", dateforfilter)
									.filter("product.productCode", saleslineitem.getProductCode()).list();
						}

//						List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", comp.getCompanyId())
//								.filter("contractCount", contractEntity.getCount()).filter("status", Service.SERVICESTATUSCOMPLETED)
//								.filter("serviceDate >=", activecontractfilter).filter("serviceDate <=", dateforfilter)
//								.filter("product.productCode", saleslineitem.getProductCode()).list();
						logger.log(Level.SEVERE, "servicelist size "+servicelist.size() +" product code - "+saleslineitem.getProductCode());
						if(servicelist.size()>0) {
							service = getLastCompletedService(servicelist);
							String feedbackURL = getfeddbackURL(service,comp);
							logger.log(Level.SEVERE, "feedbackURL"+feedbackURL);
							logger.log(Level.SEVERE, "service.getCustomerFeedback()"+service.getCustomerFeedback());

							if(feedbackURL!=null && !feedbackURL.equals("") 
									&& (service.getCustomerFeedback()==null || service.getCustomerFeedback().equals(""))){
								
								long difference = dateforfilter.getTime()  - activecontractfilter.getTime();
								float daysBetween = (difference / (1000*60*60*24));
								logger.log(Level.SEVERE, "daysBetween "+daysBetween);	
								int  diffDays = (int) daysBetween;
								logger.log(Level.SEVERE, "diffDays "+diffDays);	
								
								if(cronDetails.getCommunicationChannel().equalsIgnoreCase("Email")){
									reactonSendEmail(feedbackURL,service,diffDays,cronDetails,comp,employee);
									
								}
								if(cronDetails.getCommunicationChannel().equalsIgnoreCase("SMS") || cronDetails.getCommunicationChannel().equalsIgnoreCase(AppConstants.WHATSAPP)){
									reactonSendSMS(feedbackURL,service,comp,cronDetails,employee);
								}
								       
							}
						}
						
						
					}
				}
				
		}
		
		}
				
		}	
	
}


private void reactonSendEmail(String feedbackURL, Service service, int diffDays, CronJobConfigrationDetails cronDetails, Company comp, Employee employee) {
	logger.log(Level.SEVERE,"diffDays "+diffDays);
	logger.log(Level.SEVERE,"cronDetails.getStartDay() "+cronDetails.getStartDay());
	logger.log(Level.SEVERE,"cronDetails.getEndDay() "+cronDetails.getInterval());
	logger.log(Level.SEVERE,"(cronDetails.getStartDay()> diffDays && cronDetails.getEndDay()< diffDays)"+(cronDetails.getStartDay()> diffDays && cronDetails.getInterval()< diffDays));

	try {
		
//	if(cronDetails.getStartDay()>= diffDays && cronDetails.getInterval()<= diffDays){
		EmailTemplate emailtemplate = ofy().load().type(EmailTemplate.class).filter("templateName", cronDetails.getTemplateName())
							.filter("companyId", comp.getCompanyId()).first().now();
		logger.log(Level.SEVERE,"emailtemplate"+emailtemplate);
		if(emailtemplate!=null){
			Email email = new Email();
			ArrayList<String> toEmailLis = new ArrayList<String>();
			if(!cronDetails.getEmployeeRole().equalsIgnoreCase(AppConstants.CUSTOMER)){
				toEmailLis.add(employee.getEmail());
			}
			else{
				
				if(service.getServiceBranch()!=null && !service.getServiceBranch().equalsIgnoreCase("Service Address")){
					CustomerBranchDetails customerBranch = ofy().load().type(CustomerBranchDetails.class)
										.filter("companyId", comp.getCompanyId()).filter("cinfo.count", service.getPersonInfo().getCount())
										.filter("buisnessUnitName", service.getServiceBranch()).first().now();
					if(customerBranch!=null && customerBranch.getEmail()!=null && !customerBranch.getEmail().equals("")){
						toEmailLis.add(customerBranch.getEmail());
					}
					else{
						Customer customerEntity = ofy().load().type(Customer.class).filter("companyId", comp.getCompanyId())
											.filter("count", service.getPersonInfo().getCount()).first().now();
						logger.log(Level.SEVERE,"customerEntity"+customerEntity);
						if(customerEntity!=null && customerEntity.getEmail()!=null && !customerEntity.equals("")){
							toEmailLis.add(customerEntity.getEmail());
						}
						else{
							logger.log(Level.SEVERE,"customerEntity.getEmail()"+customerEntity.getEmail());

						}
					}
				}
				else{
					Customer customerEntity = ofy().load().type(Customer.class).filter("companyId", comp.getCompanyId())
							.filter("count", service.getPersonInfo().getCount()).first().now();
					logger.log(Level.SEVERE,"customerEntity"+customerEntity);
					if(customerEntity!=null && customerEntity.getEmail()!=null && !customerEntity.equals("")){
						toEmailLis.add(customerEntity.getEmail());
					}
					else{
						logger.log(Level.SEVERE,"customerEntity.getEmail()"+customerEntity.getEmail());
					}
				}
				
			}
			
			
			String emailbody = emailtemplate.getEmailBody();

			logger.log(Level.SEVERE,"Email body="+emailbody);
			emailbody = emailbody.replaceAll("[\n]", "<br>");
			
			String servicedate = fmt1.format(service.getServiceDate());

			String mailSubject = emailtemplate.getSubject();
			if(mailSubject.contains("{Customer Name}")){
				mailSubject = mailSubject.replace("{Customer Name}", service.getPersonInfo().getFullName()+"");
			}
			if(mailSubject.contains("{ID}")){
				mailSubject = mailSubject.replace("{ID}",service.getCount()+" ");
			}
			if(mailSubject.contains("{Document Date}")){
				mailSubject = mailSubject.replace("{Document Date}",servicedate);
			}
			if(mailSubject.contains("{Service Name}")){
				mailSubject = mailSubject.replace("{Service Name}",service.getProductName());
			}
			if(mailSubject.contains("{Company Name>")){
				mailSubject = mailSubject.replace("{Company Name}",service.getProductName());
			}
			
			if(emailbody.contains("{Customer Name}")){
				emailbody = emailbody.replace("{Customer Name}", service.getPersonInfo().getFullName()+"");
			}
			if(emailbody.contains("{ID}")){
				emailbody = emailbody.replace("{ID}",service.getCount()+" ");
			}
			if(emailbody.contains("{Document Date}")){
				emailbody = emailbody.replace("{Document Date}",servicedate);
			}
			if(emailbody.contains("{Service Name}")){
				emailbody = emailbody.replace("{Service Name}",service.getProductName());
			}
			if(emailbody.contains("{Company Name}")){
				emailbody = emailbody.replace("{Company Name}",service.getProductName());
			}
			
			if(emailbody.contains("{Feedback_Link}")){
				emailbody = emailbody.replace("{Feedback_Link}",feedbackURL);
			}
			
			if(emailbody.contains("{Company Signature}")) {
				emailbody=emailbody.replace("{Company Signature}", ServerAppUtility.getCompanySignature(comp,null));
			}
			
			emailbody = emailbody.replaceAll("[\\s]", "&nbsp;");

			logger.log(Level.SEVERE,"feedbackURL "+feedbackURL);
			logger.log(Level.SEVERE,"emailBody "+emailbody);

			email.sendMsgEmail(mailSubject, emailbody, toEmailLis, null, comp.getEmail());
		}
		
	} catch (Exception e) {
		e.printStackTrace();
	}
//	}
	
}


private void reactonSendSMS(String feedbackFormUrl, Service service, Company company, CronJobConfigrationDetails cronDetails, Employee employee) {

		try {
			
		String companyName = company.getBusinessUnitName();
	
		
//		SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId",service.getCompanyId()).filter("event","Service Completion").filter("status",true).first().now();
		SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId",service.getCompanyId()).filter("event",cronDetails.getTemplateName()).filter("status",true).first().now();

		logger.log(Level.SEVERE, "smsEntity" + smsEntity);

		if(smsEntity!=null){
			
			final String templateMsgwithbraces = smsEntity.getMessage(); 
			final String prodName = service.getProductName();
			final String serNo = service.getServiceSerialNo()+"";
			final String serDate = ServerAppUtility.parseDate(service.getServiceDate());

			/**
			 * @author Vijay Date :- 09-08-2021
			 * Des :- added below method to check customer DND status if customer DND status is Active 
			 * then SMS will not send to customer
			 */
			ServerAppUtility serverapputility = new ServerAppUtility();
			boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(service.getPersonInfo().getCount(), service.getCompanyId());
	        logger.log(Level.SEVERE, "dndstatusFlag" + dndstatusFlag);
			if(!dndstatusFlag){
				
				String custName = service.getPersonInfo().getFullName();
				String cutomerName = templateMsgwithbraces.replace("{CustomerName}", custName);
				String productName = cutomerName.replace("{ProductName}", prodName);
				String serviceNo = productName.replace("{ServiceNo}", serNo);
				String serviceDate =  serviceNo.replace("{ServiceDate}", serDate);
				String actualMsg = serviceDate.replace("{companyName}", companyName);
				
				actualMsg=actualMsg.replace("{Link}", feedbackFormUrl);
				
				long customercell = service.getPersonInfo().getCellNumber();
				logger.log(Level.SEVERE, "actualMsg" + actualMsg);
				
				
				if(!cronDetails.getEmployeeRole().equalsIgnoreCase(AppConstants.CUSTOMER)){
					customercell = employee.getCellNumber1();
				}
				
				SmsServiceImpl smsimpl = new SmsServiceImpl();
				if(customercell!=0){
					logger.log(Level.SEVERE," cronDetails.getCommunicationChannel()  "+cronDetails.getCommunicationChannel());

					if(cronDetails.getCommunicationChannel().equals(AppConstants.SMS)){
						String smsResponse = smsimpl.sendSmsToClient(actualMsg, customercell+"", company.getCompanyId(), AppConstants.SMS);
						logger.log(Level.SEVERE,"for sending sms smsResponse"+smsResponse);
					}
					if(cronDetails.getCommunicationChannel().equals(AppConstants.WHATSAPP)){
						String response = smsimpl.sendMessageOnWhatsApp(company.getCompanyId(), customercell+"", actualMsg);
						logger.log(Level.SEVERE,"whats app response"+response);
					}
				}
				
			}
		
			
		}
	
	} catch (Exception e) {
		e.printStackTrace();
	}
}


private String getfeddbackURL(Service service, Company company) {

	String feedbackFormUrl = "";

	if(company!=null&&company.getFeedbackUrlList()!=null&&company.getFeedbackUrlList().size()!=0){
		Customer customer=ofy().load().type(Customer.class).filter("companyId",service.getCompanyId()).filter("count",service.getPersonInfo().getCount()).first().now();
		if(customer!=null){
			if(customer.getCategory()!=null&&!customer.getCategory().equals("")){
				for(FeedbackUrlAsPerCustomerCategory feedback:company.getFeedbackUrlList()){
					if(feedback.getCustomerCategory().equals(customer.getCategory())){
						feedbackFormUrl=feedback.getFeedbackUrl();
						break;
					}
				}
			}
		}
		
		if(feedbackFormUrl!=null&&!feedbackFormUrl.equals("")){
			logger.log(Level.SEVERE, "ZOHO FEEDBACK URL " + feedbackFormUrl);
			final String serDate = ServerAppUtility.parseDate(service.getServiceDate());

			
			StringBuilder sbPostData= new StringBuilder(feedbackFormUrl);
	        sbPostData.append("?customerId="+service.getPersonInfo().getCount());
	        sbPostData.append("&customerName="+service.getPersonInfo().getFullName());
	        sbPostData.append("&serviceId="+service.getCount());
	        sbPostData.append("&serviceDate="+serDate);
	        sbPostData.append("&serviceName="+service.getProductName());
	        sbPostData.append("&technicianName="+service.getEmployee());
	        sbPostData.append("&branch="+service.getBranch());
	        feedbackFormUrl = sbPostData.toString();
	        feedbackFormUrl=feedbackFormUrl.replaceAll("\\s", "%20");
	        feedbackFormUrl=feedbackFormUrl.replaceAll("&", "%26");
	       
	        feedbackFormUrl=ServerAppUtility.getTinyUrl(feedbackFormUrl,company.getCompanyId());
	        
	        logger.log(Level.SEVERE, "FINAL ZOHO FEEDBACK URL " + feedbackFormUrl);
	        feedbackFormUrl=feedbackFormUrl.replace("{Link}", feedbackFormUrl);
	        
	        logger.log(Level.SEVERE, "FINAL ZOHO FEEDBACK feedbackFormUrl  " + feedbackFormUrl);

		}
		
	}
	
	
	return feedbackFormUrl;
}


private Service getLastCompletedService(List<Service> servicelist) {
	
	Comparator<Service> serviceDateComparator = new Comparator<Service>() {
		public int compare(Service s1, Service s2) {
		
		Date date1 = s1.getServiceDate();
		Date date2 = s2.getServiceDate();
		
		//ascending order
		return date2.compareTo(date1);
		}
		};
		Collections.sort(servicelist, serviceDateComparator);
		logger.log(Level.SEVERE, "Last service date and service id"+servicelist.get(0).getCount() +"-"+ servicelist.get(0).getServiceDate());
	return servicelist.get(0);
}


private ArrayList<Integer> getcustomerCountlist(List<Customer> customerlist) {
	ArrayList<Integer> customeridlist = new ArrayList<Integer>();
	for(Customer customerEntity : customerlist){
		customeridlist.add(customerEntity.getCount());
	}
	return customeridlist;
}

}
