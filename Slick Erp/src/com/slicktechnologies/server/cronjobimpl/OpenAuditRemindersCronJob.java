package com.slicktechnologies.server.cronjobimpl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.assesmentreport.AssesmentReportEmbedForTable;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.email.SendGridEmailServlet;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;


public class OpenAuditRemindersCronJob extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4537840069276159696L;
	
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private DateFormat fmtPaymentDate = new SimpleDateFormat("dd/MM/yyyy");
	Company companyobj;
	
	 Logger logger = Logger.getLogger("OpenAuditRemindersCronJobImpl.class");
	 

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}


	
	private String getFirstLetterUpperCase(String customerFullName) {
		
		String customerName="";
		String[] customerNameSpaceSpilt=customerFullName.split(" ");
		int count=0;
		for (String name : customerNameSpaceSpilt) 
		{
			String nameLowerCase=name.toLowerCase();
			if(count==0)
			{
				customerName=customerName+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);
			}
			else
			{
				customerName=customerName+" "+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);	
			}
			
			count=count+1;
			
		}
		return customerName;  
		
		}
	
	
	public void auditlistForCronJob(String cronList, HashSet<String> empRoleList) {
		
		
		
		Date today=DateUtility.getDateWithTimeZone("IST", new Date());
		
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmtPaymentDate.setTimeZone(TimeZone.getTimeZone("IST"));
		
	
	try{
 
	/********************************Adding Companies in the list ***********************/
	 
		List<Company> compEntity = ofy().load().type(Company.class).list();
		if(compEntity.size()>0){
	
		logger.log(Level.SEVERE,"If compEntity size > 0");
		
		logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
		if(empRoleList!=null)
		logger.log(Level.SEVERE,"Size of empRoleList"+empRoleList.size());
		
			for(int i=0;i<compEntity.size();i++){
				
				for(String empRoleName :empRoleList){
				
					if(!empRoleName.equalsIgnoreCase(AppConstants.CUSTOMER)){
						
						AuditReminderToAssessedBy(cronList,null,null,compEntity.get(i),today);
					}
					else{					
						AuditReminderToCustomer(cronList,null,null,compEntity.get(i),today);		
					}
				}
			}
		}
		else{
			logger.log(Level.SEVERE,"No Company found from cron job ");	
			System.out.println("No Company found from cron job ");
		}
	 }catch(Exception e2){
		 e2.printStackTrace();
	 }
				
	}


	private void AuditReminderToCustomer(String cronList,
			Employee employee, List<String> branchList, Company company, Date today) {
		String logs="";
		try {
			companyobj=company;
		
		 /******************************Converting todayDate to String format*****************/
		 
		 Date todaysDate = new Date();
		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 String todayDateString = df.format(todaysDate);
		 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);
		 
		 
		Email cronEmail = new Email();
//		CompanyPayment compPayment=null;
		
		//Copied from another cron job
		Gson gson = new Gson();
		JSONArray jsonarr = null;
		try {
			jsonarr=new JSONArray(cronList.trim());
		} catch (JSONException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		CronJobConfigrationDetails cronDetails =null;
		
		
		for(int k=0;k<jsonarr.length();k++){
			logger.log(Level.SEVERE,"JSON ARRAY LENGTH :: "+jsonarr.length());
			logs+="<br/>"+"JSON ARRAY LENGTH :: "+jsonarr.length();
			JSONObject jsonObj = jsonarr.getJSONObject(k);
			cronDetails = new CronJobConfigrationDetails();
			
			cronDetails = gson.fromJson(jsonObj.toString(), CronJobConfigrationDetails.class);
			
			
			if(cronDetails.getEmployeeRole().equalsIgnoreCase(AppConstants.CUSTOMER)) {
			EmailTemplate emailTemplate = null;
			if(cronDetails.getTemplateName()!=null && !cronDetails.getTemplateName().equals("")){
				emailTemplate = ofy().load().type(EmailTemplate.class).filter("companyId", company.getCompanyId())
						.filter("templateName", cronDetails.getTemplateName())	
						.filter("templateStatus", true).first().now();
			}
			else{
				emailTemplate = ofy().load().type(EmailTemplate.class).filter("companyId", company.getCompanyId())
						.filter("templateName", "OpenAuditListReminderToCustomer")	
						.filter("templateStatus", true).first().now();
			}
			
			Calendar cal=Calendar.getInstance();
			cal.setTime(today);
			
			int diffday = 0;
			if(cronDetails.isOverdueStatus() && cronDetails.getOverdueDays()>0){
//				cal.add(Calendar.DATE,cronDetails.getOverdueDays() * -1);
				diffday = cronDetails.getOverdueDays()*-1;
			}else{
				diffday = cronDetails.getInterval();
			 }
			cal.add(Calendar.DATE, diffday);
			logger.log(Level.SEVERE,"diffday1="+diffday+"cal="+cal.getTime());
			logs+="<br/>"+"diffday1="+diffday+"cal="+cal.getTime();
			Date dateForFilter=null;
			
			try {
				dateForFilter=fmtPaymentDate.parse(fmtPaymentDate.format(cal.getTime()));
				cal.set(Calendar.HOUR_OF_DAY,23);
				cal.set(Calendar.MINUTE,59);
				cal.set(Calendar.SECOND,59);
				cal.set(Calendar.MILLISECOND,999);
				dateForFilter=cal.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Date After setting the Time"+dateForFilter);
			logs+="<br/>"+"Date After setting the Time"+dateForFilter;
			
			Calendar cal1=Calendar.getInstance();
			cal1.setTime(today);
			cal1.add(Calendar.DATE, 0);
			
			Date interdateForFilter=null;
			
			try {
				interdateForFilter=fmtPaymentDate.parse(fmtPaymentDate.format(cal1.getTime()));
				cal1.set(Calendar.HOUR_OF_DAY,0);
				cal1.set(Calendar.MINUTE,0);
				cal1.set(Calendar.SECOND,0);
				cal1.set(Calendar.MILLISECOND,0);
				interdateForFilter=cal1.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Date After setting the Time interdateForFilter"+interdateForFilter);
			logs+="<br/>"+"Date After setting the Time interdateForFilter"+interdateForFilter;
		//End of copy
		
		logger.log(Level.SEVERE,"In the for loop");
			logs+="<br/>"+"In the for loop";
		
	/********************************Reading AssessmentReport(Audit) entity  ***********************/
		List<AssesmentReport> tempauditlist;
		List<AssesmentReport> auditList=new ArrayList<AssesmentReport>();
		
		if(cronDetails.isOverdueStatus()){
			logger.log(Level.SEVERE,"In isOverdueStatus()");
			logs+="<br/>"+"In isOverdueStatus()";
			tempauditlist = ofy().load().type(AssesmentReport.class)
					.filter("companyId",company.getCompanyId())
					.filter("assessmentDate >=",dateForFilter)
					.filter("assessmentDate <=",interdateForFilter)
					.list();
		}else{
			tempauditlist = ofy().load().type(AssesmentReport.class)
					.filter("companyId",company.getCompanyId())
					.filter("assessmentDate >=",interdateForFilter)
					.filter("assessmentDate <=",dateForFilter)					
					.list();
		}	
		if(tempauditlist!=null) {
			for(AssesmentReport report:tempauditlist) {
				if(report.getStatus().equals("Created"))
					auditList.add(report);
			}
		}
			
		
		logger.log(Level.SEVERE,"over due date is date Is:"+dateForFilter + " todays date -- " + interdateForFilter);	
		logger.log(Level.SEVERE,"auditList size:"+auditList.size());	
		logger.log(Level.SEVERE,"Today date Is:"+dateForFilter);	
		logs+="<br/>"+"over due date is date Is:"+dateForFilter + " todays date -- " + interdateForFilter;
		logs+="<br/>"+"auditList size:"+auditList.size();
		logs+="<br/>"+"Today date Is:"+dateForFilter;
			boolean communicationChannelEmailFlag = false;
			if(cronDetails.getCommunicationChannel()!=null && !cronDetails.getCommunicationChannel().equals("")
					&& (cronDetails.getCommunicationChannel().equals(AppConstants.SMS) || cronDetails.getCommunicationChannel().equals(AppConstants.WHATSAPP) )){
				//no provision yet
//				long cellNumber=0;
//				if(!cronDetails.getEmployeeRole().equalsIgnoreCase(AppConstants.CUSTOMER)){
//					if(employee!=null)
//						cellNumber = employee.getCellNumber1();
//				}
//				sendPaymentReminderMessage(custPaymentEntity,cronDetails,company,cellNumber);
			}
			if(cronDetails.getCommunicationChannel()==null || cronDetails.getCommunicationChannel().equals("")){
				communicationChannelEmailFlag = true;
			}
			if(cronDetails.getCommunicationChannel()!=null && !cronDetails.getCommunicationChannel().equals("")
					&&  cronDetails.getCommunicationChannel().equals(AppConstants.EMAIL)){
				communicationChannelEmailFlag = true;
			}
			logger.log(Level.SEVERE,"communicationChannelEmailFlag "+communicationChannelEmailFlag);
			logs+="<br/>"+"communicationChannelEmailFlag "+communicationChannelEmailFlag;
			if(communicationChannelEmailFlag){
				
			if(auditList!=null&&auditList.size()>0) {
				
				//Loading unique customer id and customer branch id from audit list
				HashSet<Integer> customerSet=new HashSet<Integer>();
				HashSet<Integer> customerBranchSet=new HashSet<Integer>();
				for(AssesmentReport a:auditList) {
					customerSet.add(a.getCinfo().getCount());
					customerBranchSet.add(a.getCustomerBranchId());
				}
				
				//Loading customer and customer branches
				List<Customer> customerList=ofy().load().type(Customer.class).filter("companyId", company.getCompanyId()).filter("count IN",new ArrayList<Integer>(customerSet)).list();
				List<CustomerBranchDetails> customerBranchList=ofy().load().type(CustomerBranchDetails.class).filter("companyId", company.getCompanyId()).filter("count IN",new ArrayList<Integer>(customerBranchSet)).list();
				
				Map<Integer,Customer> customerMap= new HashMap<Integer, Customer>();
				if(customerList!=null)
					customerMap=(Map<Integer, Customer>) customerList.stream().collect(Collectors.toMap(Customer::getCount,Function.identity()));
				
				Map<Integer,CustomerBranchDetails> customerBranchMap= new HashMap<Integer, CustomerBranchDetails>();
				if(customerBranchList!=null)
					customerBranchMap= (Map<Integer, CustomerBranchDetails>) customerBranchList.stream().collect(Collectors.toMap(CustomerBranchDetails::getCount,Function.identity()));
				
				//Sorting audit list company branch wise
				Map<String,List<AssesmentReport>> branchwiseAuditReportMap= (Map<String, List<AssesmentReport>>) auditList.stream().collect(Collectors.groupingBy(AssesmentReport::getBranch,Collectors.toList()));
				
				
				HashSet<String> companyBranchSet=new HashSet<String>(branchwiseAuditReportMap.keySet());
				ArrayList<String> companyBranchNamesList=new ArrayList<String>(companyBranchSet);				
				HashMap<String, String> branchSignatureMap=new HashMap<String, String>();
				HashMap<String, String> branchOrcompanyNameMap=new HashMap<String, String>();
				HashMap<String, String> branchOrcompanyEmailMap=new HashMap<String, String>();
				String companyName=company.getBusinessUnitName();
				String companySignature=ServerAppUtility.getCompanySignature(company, null);
				String companyEmail=company.getEmail();
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", company.getCompanyId())){
					for(String bname:companyBranchNamesList) {
						branchOrcompanyNameMap.put(bname, ServerAppUtility.getCompanyName(bname, company));
						branchSignatureMap.put(bname, ServerAppUtility.getCompanySignature(bname, company.getCompanyId()));
						branchOrcompanyEmailMap.put(bname, ServerAppUtility.getCompanyEmail(bname, company));
					}								
				}
				
				logger.log(Level.SEVERE,"branchwiseAuditReportMap size="+branchwiseAuditReportMap.size());
				logs+="<br/>"+"branchwiseAuditReportMap size="+branchwiseAuditReportMap.size();
				Set s1 = branchwiseAuditReportMap.entrySet();
				Iterator iterator =  s1.iterator();
				
				while(iterator.hasNext()) {
				
					ArrayList<AssesmentReport> branchwiseAssesmentReportlist = new ArrayList<AssesmentReport>();

					Map.Entry<String,List<AssesmentReport>> m = (Map.Entry<String,List<AssesmentReport>>) iterator.next();
					branchwiseAssesmentReportlist.addAll(m.getValue());
					
					logger.log(Level.SEVERE,"branchwiseAssesmentReportlist size="+branchwiseAssesmentReportlist.size());
					logs+="<br/>"+"branchwiseAssesmentReportlist size="+branchwiseAssesmentReportlist.size();

					//sorting audit report customer branch wise
					Map<Integer, List<AssesmentReport>> customerbranchwiseAuditReportMap = branchwiseAssesmentReportlist.stream().collect(Collectors.groupingBy(AssesmentReport::getCustomerBranchId,Collectors.toList()));

//					HashMap<Integer, ArrayList<AssesmentReport>> customerbranchwiseAuditReportMap = new HashMap<Integer,ArrayList<AssesmentReport>>();
//					for(int j=0;j<branchwiseAssesmentReportlist.size();j++){
//						
//						ArrayList<AssesmentReport> list = new ArrayList<AssesmentReport>();
//						
//						if(customerbranchwiseAuditReportMap.containsKey(branchwiseAssesmentReportlist.get(j).getCustomerBranchId())){
//							list = customerbranchwiseAuditReportMap.get(branchwiseAssesmentReportlist.get(j).getCustomerBranchId());
//							list.add(branchwiseAssesmentReportlist.get(j));
//						}else{
//							list.add(branchwiseAssesmentReportlist.get(j));
//						}
//						customerbranchwiseAuditReportMap.put(branchwiseAssesmentReportlist.get(j).getCustomerBranchId(), list);
//			   	 	}
					Set s2 = customerbranchwiseAuditReportMap.entrySet();
					Iterator iterator2 =  s2.iterator();
					
					
					while(iterator2.hasNext()){
						ArrayList<AssesmentReport> customerAssesmentReportlist = new ArrayList<AssesmentReport>();

						Map.Entry<Integer, ArrayList<AssesmentReport>> m33 = (Map.Entry<Integer, ArrayList<AssesmentReport>>) iterator2.next();
						customerAssesmentReportlist.addAll(m33.getValue());
						
						
						ArrayList<String> toEmailList1 = null;
						String msgBody="";
						int totalAmount = 0;
						
						String customerName ="";
						logger.log(Level.SEVERE,"cronDetails.getEmployeeRole() "+cronDetails.getEmployeeRole());
						logs+="<br/>"+"cronDetails.getEmployeeRole() "+cronDetails.getEmployeeRole();
						Customer customerEntity=null;
						CustomerBranchDetails customerBranchEntity=null;
						if(customerAssesmentReportlist!=null &&customerAssesmentReportlist.size()>0){					
								logger.log(Level.SEVERE,"customerauditlist size for one client"+customerAssesmentReportlist.size());
								logs+="<br/>"+"customerauditlist size for one client"+customerAssesmentReportlist.size();
//								
								if(customerMap.containsKey(customerAssesmentReportlist.get(0).getCinfo().getCount()))
									customerEntity=(Customer) customerMap.get(customerAssesmentReportlist.get(0).getCinfo().getCount());
								else
									customerEntity=ofy().load().type(Customer.class).filter("companyId", company.getCompanyId()).filter("count", customerAssesmentReportlist.get(0).getCinfo().getCount()).first().now();
								
								if(customerBranchMap.containsKey(customerAssesmentReportlist.get(0).getCustomerBranchId()))
									customerBranchEntity=(CustomerBranchDetails) customerBranchMap.get(customerAssesmentReportlist.get(0).getCustomerBranchId());
								else
									customerBranchEntity=ofy().load().type(CustomerBranchDetails.class).filter("companyId", company.getCompanyId()).filter("count", customerAssesmentReportlist.get(0).getCustomerBranchId()).first().now();
								
								
								customerName = getFirstLetterUpperCase(customerEntity.getFullname());
								toEmailList1=new ArrayList<String>();	
								if(customerBranchEntity.getEmail()!=null&&!customerBranchEntity.getEmail().equals(""))
									toEmailList1.add(customerBranchEntity.getEmail());
								else if(customerEntity.getEmail()!=null&&!customerEntity.getEmail().equals("")) {
									toEmailList1.add(customerEntity.getEmail());
									logger.log(Level.SEVERE,"no email found in customer branch"+customerBranchEntity.getBusinessUnitName()+" - "+customerBranchEntity.getCount());							
									logs+="<br/>"+"no email found in customer branch"+customerBranchEntity.getBusinessUnitName()+" - "+customerBranchEntity.getCount();
								}else {
									logger.log(Level.SEVERE,"no email found in customer and customer branch. Customer id:"+customerEntity.getCount()+" customer branch id:"+ customerBranchEntity.getCount());
									logs+="<br/>"+"no email found in customer and customer branch. Customer id:"+customerEntity.getCount()+" customer branch id:"+ customerBranchEntity.getCount();
								}
								logger.log(Level.SEVERE,"toEmailList1 "+toEmailList1,"");
								logger.log(Level.SEVERE,"Customer full Name =="+customerName);
								logger.log(Level.SEVERE,"Customer id == "+customerEntity.getCount());
								logger.log(Level.SEVERE,"Customer Branch Name =="+customerBranchEntity.getBusinessUnitName());
								logs+="<br/>"+"toEmailList1 "+toEmailList1;
								logs+="<br/>"+"Customer full Name =="+customerName;
								logs+="<br/>"+"Customer id == "+customerEntity.getCount();
								logs+="<br/>"+"Customer Branch Name =="+customerBranchEntity.getBusinessUnitName();
								

						if(emailTemplate!=null){
//							logger.log(Level.SEVERE,"in emailTemplate "+emailTemplate.getTemplateName());
							String mailSubject = emailTemplate.getSubject();
							String emailbody = emailTemplate.getEmailBody();

//							logger.log(Level.SEVERE,"Email body="+emailbody);
							String resultString = emailbody.replaceAll("[\n]", "<br>");
							String resultString1 = resultString.replaceAll("[\\s]", "&nbsp;");
							
							
//							logger.log(Level.SEVERE,"Email body after replacement="+resultString1);
//							logger.log(Level.SEVERE,"toEmailList size::: "+toEmailList1.size());
//							logger.log(Level.SEVERE,"toEmailList size::: "+toEmailList1.size()+" Branch List SIze : "+branchList.size());

							msgBody=resultString1;
							
							msgBody=msgBody.replace("{CustomerName}", customerName);
							msgBody=msgBody.replace("{CustomerBranch}", customerBranchEntity.getBusinessUnitName());
							
							msgBody=msgBody.replace("{AuditTable}", getAssesmentReportTable(customerAssesmentReportlist,companyobj));
							
							if(branchSignatureMap.size()>0) {
								if(branchSignatureMap.get(customerEntity.getBranch())!=null)
									msgBody=msgBody.replace("{CompanySignature}", branchSignatureMap.get(customerEntity.getBranch()));
								else
									msgBody=msgBody.replace("{CompanySignature}", companySignature);
							}else
								msgBody=msgBody.replace("{CompanySignature}", companySignature);
							
							if(branchOrcompanyEmailMap.size()>0) {
								if(customerBranchEntity.getBranch()!=null&&!customerBranchEntity.getBranch().equals("")) {
									if(branchOrcompanyEmailMap.get(customerBranchEntity.getBranch())!=null)
										companyEmail=branchOrcompanyEmailMap.get(customerBranchEntity.getBranch());
									else
										companyEmail=company.getEmail();
									
								}else {
									if(branchOrcompanyEmailMap.get(customerEntity.getBranch())!=null)
										companyEmail=branchOrcompanyEmailMap.get(customerEntity.getBranch());
									else
										companyEmail=company.getEmail();
									}																						
							}
							
							if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, company.getCompanyId())){
								if(toEmailList1.size()>0&&companyEmail!=null&&!companyEmail.equals("")) {									
									SendGridEmailServlet sdEmail=new SendGridEmailServlet();
									sdEmail.sendMailWithSendGrid(companyEmail, toEmailList1, null, null, mailSubject, msgBody, "text/html",null,null,"application/pdf",null);	 	
								}else {
									logger.log(Level.SEVERE,"Customer or Company/Branch email missing");
									logs+="<br/>"+"Customer or Company/Branch email missing";
								}
							}
							else 
								logger.log(Level.SEVERE,"Could not send email as 'Email'-'EnableSendGridEmailApi' process config inactive.");																	
							logs+="<br/>"+"Could not send email as 'Email'-'EnableSendGridEmailApi' process config inactive.";
							
						}
						else {
							logger.log(Level.SEVERE,"No email template found");
							logs+="<br/>"+"No email template found";
						}
						}
						
					}
					
					}//branch while loop
					
					
				
				
			
			}else 
				logger.log(Level.SEVERE,"No open audit reports found as per cron job settings");
			logs+="<br/>"+"No open audit reports found as per cron job settings";
		 }else {
			 logger.log(Level.SEVERE,"communicationChannelEmailFlag false");
			 logs+="<br/>"+"communicationChannelEmailFlag false";
		 }
		}
		
		}	//end of for loop
		
		
		} catch (Exception e) {
			e.printStackTrace();
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			ArrayList<String> toEmailList=new ArrayList<String>();
			toEmailList.add("support@evasoftwaresolutions.com");
			if(companyobj.getCompanyURL()!=null&&!companyobj.getCompanyURL().equals(""))
				sdEmail.sendMailWithSendGrid("support@evasoftwaresolutions.com", toEmailList, null, null, "OpenAuditListCronJob for customer failed for "+companyobj.getCompanyURL(), logs+"</br>"+e.getMessage(), "text/html",null,null,"application/pdf",null);															
			else
				sdEmail.sendMailWithSendGrid("support@evasoftwaresolutions.com", toEmailList, null, null, "OpenAuditListCronJob for customer failed for "+companyobj.getBusinessUnitName(),logs+"</br>"+ e.getMessage(), "text/html",null,null,"application/pdf",null);															
			
		}
	}

	private void AuditReminderToAssessedBy(String cronList,
			Employee employee, List<String> branchList, Company company, Date today) {
		String logs="";
		try {
			companyobj=company;
		
		 /******************************Converting todayDate to String format*****************/
		 
		 Date todaysDate = new Date();
		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 String todayDateString = df.format(todaysDate);
		 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);
		 
		 
		Email cronEmail = new Email();
//		CompanyPayment compPayment=null;
		
		//Copied from another cron job
		Gson gson = new Gson();
		JSONArray jsonarr = null;
		try {
			jsonarr=new JSONArray(cronList.trim());
		} catch (JSONException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		CronJobConfigrationDetails cronDetails =null;
		
		
		for(int k=0;k<jsonarr.length();k++){
			logger.log(Level.SEVERE,"JSON ARRAY LENGTH :: "+jsonarr.length());
			logs+="<br/>"+"JSON ARRAY LENGTH :: "+jsonarr.length();
			JSONObject jsonObj = jsonarr.getJSONObject(k);
			cronDetails = new CronJobConfigrationDetails();
			
			cronDetails = gson.fromJson(jsonObj.toString(), CronJobConfigrationDetails.class);
			
			if(!cronDetails.getEmployeeRole().equalsIgnoreCase(AppConstants.CUSTOMER)) {
			
			EmailTemplate emailTemplate = null;
			if(cronDetails.getTemplateName()!=null && !cronDetails.getTemplateName().equals("")){
				emailTemplate = ofy().load().type(EmailTemplate.class).filter("companyId", company.getCompanyId())
						.filter("templateName", cronDetails.getTemplateName())	
						.filter("templateStatus", true).first().now();
			}
			else{
				emailTemplate = ofy().load().type(EmailTemplate.class).filter("companyId", company.getCompanyId())
						.filter("templateName", "OpenAuditListReminderToAssessedBy")	
						.filter("templateStatus", true).first().now();
			}
			
			Calendar cal=Calendar.getInstance();
			cal.setTime(today);
			
			int diffday = 0;
			if(cronDetails.isOverdueStatus() && cronDetails.getOverdueDays()>0){
//				cal.add(Calendar.DATE,cronDetails.getOverdueDays() * -1);
				diffday = cronDetails.getOverdueDays()*-1;
			}else{
				diffday = cronDetails.getInterval();
			 }
			cal.add(Calendar.DATE, diffday);
			logger.log(Level.SEVERE,"diffday1="+diffday+"cal="+cal.getTime());
			logs+="<br/>"+"diffday1="+diffday+"cal="+cal.getTime();
			Date dateForFilter=null;
			
			try {
				dateForFilter=fmtPaymentDate.parse(fmtPaymentDate.format(cal.getTime()));
				cal.set(Calendar.HOUR_OF_DAY,23);
				cal.set(Calendar.MINUTE,59);
				cal.set(Calendar.SECOND,59);
				cal.set(Calendar.MILLISECOND,999);
				dateForFilter=cal.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Date After setting the Time"+dateForFilter);
			logs+="<br/>"+"Date After setting the Time"+dateForFilter;
			
			Calendar cal1=Calendar.getInstance();
			cal1.setTime(today);
			cal1.add(Calendar.DATE, 0);
			
			Date interdateForFilter=null;
			
			try {
				interdateForFilter=fmtPaymentDate.parse(fmtPaymentDate.format(cal1.getTime()));
				cal1.set(Calendar.HOUR_OF_DAY,0);
				cal1.set(Calendar.MINUTE,0);
				cal1.set(Calendar.SECOND,0);
				cal1.set(Calendar.MILLISECOND,0);
				interdateForFilter=cal1.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Date After setting the Time interdateForFilter"+interdateForFilter);
			logs+="<br/>"+"Date After setting the Time interdateForFilter"+interdateForFilter;
		//End of copy
		
		logger.log(Level.SEVERE,"In the for loop");
			logs+="<br/>"+"In the for loop";
		
	/********************************Reading AssessmentReport(Audit) entity  ***********************/
		List<AssesmentReport> tempauditlist;
		List<AssesmentReport> auditList=new ArrayList<AssesmentReport>();
		List<AssesmentReport> auditListWithNoAssessedBy=new ArrayList<AssesmentReport>();
		
		if(cronDetails.isOverdueStatus()){
			logger.log(Level.SEVERE,"In isOverdueStatus()");
			logs+="<br/>"+"In isOverdueStatus()";
			tempauditlist = ofy().load().type(AssesmentReport.class)
					.filter("companyId",company.getCompanyId())
					.filter("assessmentDate >=",dateForFilter)
					.filter("assessmentDate <=",interdateForFilter)
					.list();
		}else{
			tempauditlist = ofy().load().type(AssesmentReport.class)
					.filter("companyId",company.getCompanyId())
					.filter("assessmentDate >=",interdateForFilter)
					.filter("assessmentDate <=",dateForFilter)					
					.list();
		}	
		if(tempauditlist!=null) {
			for(AssesmentReport report:tempauditlist) {
				if(report.getStatus().equals("Created")) {
					if(report.getAssessedPeron1()!=null&&!report.getAssessedPeron1().equals(""))
						auditList.add(report);
					else {
						logger.log(Level.SEVERE,"Assessed by is null in assessment report. Report for this will no be mailed."+report.getCount());
						logs+="<br/>"+"Assessed by is null in assessment report. Report for this will no be mailed."+report.getCount();
						auditListWithNoAssessedBy.add(report);
					}
				}
			}
		}
		logger.log(Level.SEVERE,"auditListWithNoAssessedBy size"+auditListWithNoAssessedBy.size());
		logs+="<br/>"+"auditListWithNoAssessedBy size"+auditListWithNoAssessedBy.size();
		
		logger.log(Level.SEVERE,"over due date is date Is:"+dateForFilter + " todays date -- " + interdateForFilter);	
		logger.log(Level.SEVERE,"auditList size:"+auditList.size());	
		logger.log(Level.SEVERE,"Today date Is:"+dateForFilter);	
		logs+="<br/>"+"over due date is date Is:"+dateForFilter + " todays date -- " + interdateForFilter;
		logs+="<br/>"+"auditList size:"+auditList.size();
		logs+="<br/>"+"Today date Is:"+dateForFilter;
			
			boolean communicationChannelEmailFlag = false;
			if(cronDetails.getCommunicationChannel()!=null && !cronDetails.getCommunicationChannel().equals("")
					&& (cronDetails.getCommunicationChannel().equals(AppConstants.SMS) || cronDetails.getCommunicationChannel().equals(AppConstants.WHATSAPP) )){
				//no provision yet
//				long cellNumber=0;
//				if(!cronDetails.getEmployeeRole().equalsIgnoreCase(AppConstants.CUSTOMER)){
//					if(employee!=null)
//						cellNumber = employee.getCellNumber1();
//				}
//				sendPaymentReminderMessage(custPaymentEntity,cronDetails,company,cellNumber);
			}
			if(cronDetails.getCommunicationChannel()==null || cronDetails.getCommunicationChannel().equals("")){
				communicationChannelEmailFlag = true;
			}
			if(cronDetails.getCommunicationChannel()!=null && !cronDetails.getCommunicationChannel().equals("")
					&&  cronDetails.getCommunicationChannel().equals(AppConstants.EMAIL)){
				communicationChannelEmailFlag = true;
			}
			logger.log(Level.SEVERE,"communicationChannelEmailFlag "+communicationChannelEmailFlag);
			logs+="<br/>"+"communicationChannelEmailFlag "+communicationChannelEmailFlag;
			if(communicationChannelEmailFlag){
				
			if(auditList!=null&&auditList.size()>0) {
				
				//Loading unique assessed by name from audit list
				HashSet<String> employeeSet=new HashSet<String>();
				for(AssesmentReport a:auditList) {
					if(a.getAssessedPeron1()!=null&&!a.getAssessedPeron1().equals(""))
						employeeSet.add(a.getAssessedPeron1());	
					else {
						logger.log(Level.SEVERE,"Assessed by is null in assessment report"+a.getCount());
						logs+="<br/>"+"Assessed by is null in assessment report"+a.getCount();
					}
				}
				
				//Loading employee
				List<Employee> employeeList=ofy().load().type(Employee.class).filter("companyId", company.getCompanyId()).filter("fullname IN",new ArrayList<String>(employeeSet)).list();
				
				Map<String,Employee> employeeMap= new HashMap<String,Employee>();
				if(employeeList!=null)
					employeeMap=(Map<String,Employee>) employeeList.stream().collect(Collectors.toMap(Employee::getFullname,Function.identity()));
				
				logger.log(Level.SEVERE,"employeeMap size="+employeeMap.size());
				logs+="<br/>"+"employeeMap size="+employeeMap.size();
				//Sorting audit list company branch wise
				Map<String,List<AssesmentReport>> branchwiseAuditReportMap= (Map<String, List<AssesmentReport>>) auditList.stream().collect(Collectors.groupingBy(AssesmentReport::getBranch,Collectors.toList()));
				
				
				HashSet<String> companyBranchSet=new HashSet<String>(branchwiseAuditReportMap.keySet());
				ArrayList<String> companyBranchNamesList=new ArrayList<String>(companyBranchSet);				
				HashMap<String, String> branchSignatureMap=new HashMap<String, String>();
				HashMap<String, String> branchOrcompanyNameMap=new HashMap<String, String>();
				HashMap<String, String> branchOrcompanyEmailMap=new HashMap<String, String>();
				String companyName=company.getBusinessUnitName();
				String companySignature=ServerAppUtility.getCompanySignature(company, null);
				String companyEmail=company.getEmail();
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", company.getCompanyId())){
					for(String bname:companyBranchNamesList) {
						branchOrcompanyNameMap.put(bname, ServerAppUtility.getCompanyName(bname, company));
						branchSignatureMap.put(bname, ServerAppUtility.getCompanySignature(bname, company.getCompanyId()));
						branchOrcompanyEmailMap.put(bname, ServerAppUtility.getCompanyEmail(bname, company));
					}								
				}
				
				logger.log(Level.SEVERE,"branchwiseAuditReportMap size="+branchwiseAuditReportMap.size());
				logs+="<br/>"+"branchwiseAuditReportMap size="+branchwiseAuditReportMap.size();
				Set s1 = branchwiseAuditReportMap.entrySet();
				Iterator iterator =  s1.iterator();
				
				while(iterator.hasNext()) {
				
					ArrayList<AssesmentReport> branchwiseAssesmentReportlist = new ArrayList<AssesmentReport>();

					Map.Entry<String,List<AssesmentReport>> m = (Map.Entry<String,List<AssesmentReport>>) iterator.next();
					branchwiseAssesmentReportlist.addAll(m.getValue());
					
					logger.log(Level.SEVERE,"branchwiseAssesmentReportlist size="+branchwiseAssesmentReportlist.size());
					logs+="<br/>"+"branchwiseAssesmentReportlist size="+branchwiseAssesmentReportlist.size();

					//sorting audit report customer branch wise
					Map<String, List<AssesmentReport>> employeewiseAuditReportMap = branchwiseAssesmentReportlist.stream().collect(Collectors.groupingBy(AssesmentReport::getAssessedPeron1,Collectors.toList()));


					Set s2 = employeewiseAuditReportMap.entrySet();
					Iterator iterator2 =  s2.iterator();
					
					
					while(iterator2.hasNext()){
						ArrayList<AssesmentReport> employeeAssesmentReportlist = new ArrayList<AssesmentReport>();

						Map.Entry<Integer, ArrayList<AssesmentReport>> m33 = (Map.Entry<Integer, ArrayList<AssesmentReport>>) iterator2.next();
						employeeAssesmentReportlist.addAll(m33.getValue());
						
						
						ArrayList<String> toEmailList1 = null;
						String msgBody="";
						int totalAmount = 0;
						Employee emp=null;
						String empName ="";
						logger.log(Level.SEVERE,"cronDetails.getEmployeeRole() "+cronDetails.getEmployeeRole());
						logs+="<br/>"+"cronDetails.getEmployeeRole() "+cronDetails.getEmployeeRole();
						if(employeeAssesmentReportlist!=null &&employeeAssesmentReportlist.size()>0){					
								logger.log(Level.SEVERE,"auditlist size for one employee"+employeeAssesmentReportlist.size());
								logs+="<br/>"+"auditlist size for one employee"+employeeAssesmentReportlist.size();
//								
								if(employeeAssesmentReportlist.get(0).getAssessedPeron1()!=null&&!employeeAssesmentReportlist.get(0).getAssessedPeron1().equals("")) {
									if(employeeMap.containsKey(employeeAssesmentReportlist.get(0).getAssessedPeron1()))
										emp=(Employee) employeeMap.get(employeeAssesmentReportlist.get(0).getAssessedPeron1());
									else
										emp=ofy().load().type(Employee.class).filter("companyId", company.getCompanyId()).filter("fullname", employeeAssesmentReportlist.get(0).getAssessedPeron1()).first().now();									
								}
								
								if(emp!=null) {

									empName = getFirstLetterUpperCase(emp.getFullname());
									toEmailList1=new ArrayList<String>();	
									if(emp.getEmail()!=null&&!emp.getEmail().equals(""))
										toEmailList1.add(emp.getEmail());
									else {
										logger.log(Level.SEVERE,"no email found in employee. Employee id:"+emp.getCount());
										logs+="<br/>"+"no email found in employee. Employee id:"+emp.getCount();
									}
									logger.log(Level.SEVERE,"toEmailList1 "+toEmailList1);
									logger.log(Level.SEVERE,"Employee full Name =="+emp.getFullname());
									logger.log(Level.SEVERE,"Employee id == "+emp.getCount());
									logs+="<br/>"+"toEmailList1 "+toEmailList1;
									logs+="<br/>"+"Employee full Name =="+emp.getFullname();
									logs+="<br/>"+"Employee id == "+emp.getCount();
											
									if(emailTemplate!=null){
//										logger.log(Level.SEVERE,"in emailTemplate "+emailTemplate.getTemplateName());
										String mailSubject = emailTemplate.getSubject();
										String emailbody = emailTemplate.getEmailBody();

//										logger.log(Level.SEVERE,"Email body="+emailbody);
										String resultString = emailbody.replaceAll("[\n]", "<br>");
										String resultString1 = resultString.replaceAll("[\\s]", "&nbsp;");
										
										
//										logger.log(Level.SEVERE,"Email body after replacement="+resultString1);
//										logger.log(Level.SEVERE,"toEmailList size::: "+toEmailList1.size());
//										logger.log(Level.SEVERE,"toEmailList size::: "+toEmailList1.size()+" Branch List SIze : "+branchList.size());

										msgBody=resultString1;
										
										msgBody=msgBody.replace("{AssessedByFirstName}", emp.getFullname());
										
										msgBody=msgBody.replace("{AuditTable}", getAssesmentReportTable(employeeAssesmentReportlist,companyobj));
										
										if(branchSignatureMap.size()>0) {
											if(branchSignatureMap.get(emp.getBranchName())!=null)
												msgBody=msgBody.replace("{CompanySignature}", branchSignatureMap.get(emp.getBranchName()));
											else
												msgBody=msgBody.replace("{CompanySignature}", companySignature);
										}else
											msgBody=msgBody.replace("{CompanySignature}", companySignature);
										
										if(branchOrcompanyEmailMap.size()>0) {
											if(emp.getBranchName()!=null&&!emp.getBranchName().equals("")) {
												if(branchOrcompanyEmailMap.get(emp.getBranchName())!=null)
													companyEmail=branchOrcompanyEmailMap.get(emp.getBranchName());
												else
													companyEmail=company.getEmail();
												
											}else {
												if(branchOrcompanyEmailMap.get(employeeAssesmentReportlist.get(0).getBranch())!=null)
													companyEmail=branchOrcompanyEmailMap.get(employeeAssesmentReportlist.get(0).getBranch());
												else
													companyEmail=company.getEmail();
												}																						
										}
										
										if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, company.getCompanyId())){
											if(toEmailList1.size()>0&&companyEmail!=null&&!companyEmail.equals("")) {
												
												SendGridEmailServlet sdEmail=new SendGridEmailServlet();
												sdEmail.sendMailWithSendGrid(companyEmail, toEmailList1, null, null, mailSubject, msgBody, "text/html",null,null,"application/pdf",null);	 	
											}else {
												logger.log(Level.SEVERE,"Customer or Company/Branch email missing");
												logs+="<br/>"+"Customer or Company/Branch email missing";
											}
										}
										else {
											logger.log(Level.SEVERE,"Could not send email as 'Email'-'EnableSendGridEmailApi' process config inactive.");																	
											logs+="<br/>"+"Could not send email as 'Email'-'EnableSendGridEmailApi' process config inactive.";
										}
									}
									else {
										logger.log(Level.SEVERE,"No email template found");
										logs+="<br/>"+"No email template found";
									}
							
							}else
								logger.log(Level.SEVERE,"Failed to load employee "+employeeAssesmentReportlist.get(0).getAssessedPeron1()+ "Mentioned as assessed by in assement report id:"+employeeAssesmentReportlist.get(0).getCount());
								logs+="<br/>"+"Failed to load employee "+employeeAssesmentReportlist.get(0).getAssessedPeron1()+ "Mentioned as assessed by in assement report id:"+employeeAssesmentReportlist.get(0).getCount();
						}
						
						
					}//employee while loop
					
					}//branch while loop
					
					
				
				
			
			}else 
				logger.log(Level.SEVERE,"No open audit reports found as per cron job settings");
				logs+="<br/>"+"No open audit reports found as per cron job settings";
		 }else {
			 logger.log(Level.SEVERE,"communicationChannelEmailFlag false");
			 logs+="<br/>"+"communicationChannelEmailFlag false";
		 }
		}
		
		}	//end of for loop
		
		} catch (Exception e) {
			e.printStackTrace();
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			ArrayList<String> toEmailList=new ArrayList<String>();
			toEmailList.add("support@evasoftwaresolutions.com");
			if(companyobj.getCompanyURL()!=null&&!companyobj.getCompanyURL().equals(""))
				sdEmail.sendMailWithSendGrid("support@evasoftwaresolutions.com", toEmailList, null, null, "OpenAuditListCronJob for assessed by failed for "+companyobj.getCompanyURL(), logs+"<br/>"+e.getMessage(), "text/html",null,null,"application/pdf",null);															
			else
				sdEmail.sendMailWithSendGrid("support@evasoftwaresolutions.com", toEmailList, null, null, "OpenAuditListCronJob for assessed by failed for "+companyobj.getBusinessUnitName(),logs+"<br/>"+e.getMessage(), "text/html",null,null,"application/pdf",null);															
			
		}
	
	}

	private String getCustomerName(String customerName, long companyId) {
		
		String custName;
		Customer customer = ofy().load().type(Customer.class).filter("companyId", companyId).filter("fullname", customerName).first().now();
		if(customer!=null){
			if(!customer.getCustPrintableName().equals("")){
				custName = customer.getCustPrintableName();
			}else{
				custName = customer.getFullname();
			}
			
		}else{
			custName = customerName;
		}
		
		return custName;
	}
	
	
	
	private String getAssesmentReportTable(ArrayList<AssesmentReport> customerAssesmentReportlist, Company comp) {
		SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
		StringBuilder tableBuilder = new StringBuilder();
		tableBuilder.append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\">");

		//finalURL += "assessmentReport"+"?Id="+assessmentReport.getId()+"&"+"preprint="+"no"; 
//		https://my-dot-evatesterp.appspot.com/slick_erp/assessmentReport?Id=4887808023986176&preprint=no
		String pdfurl="";
		if(comp!=null&&comp.getCompanyURL()!=null&&!comp.getCompanyURL().equals(""))
			pdfurl="https://"+comp.getCompanyURL()+"/slick_erp/assessmentReport?Id=";
		
	//	logger.log(Level.SEVERE,"pdfurl="+pdfurl);
		for(int a=0;a<customerAssesmentReportlist.size();a++){
			
			if(a==0){
				tableBuilder.append("<tr>");
				tableBuilder.append("<th rowspan=\"2\">Audit Id</th>");
				tableBuilder.append("<th rowspan=\"2\">Date</th>");
				tableBuilder.append("<th rowspan=\"2\">Assessed By</th>");
				tableBuilder.append("<th rowspan=\"2\">Customer Name</th>");
				tableBuilder.append("<th rowspan=\"2\">Customer Branch</th>");
				tableBuilder.append("<th rowspan=\"2\">Service Location</th>");
				tableBuilder.append("<th align=\"center\" colspan=\"3\">Observations</th>");
				tableBuilder.append("</tr><tr>");
				tableBuilder.append("<th>Total</th>");
				tableBuilder.append("<th>Open</th>");
				tableBuilder.append("<th>Closed</th>");
				tableBuilder.append("</tr>");
			}		
			AssesmentReport report= customerAssesmentReportlist.get(a);
			tableBuilder.append("<tr>");

			if(!pdfurl.equals("")) {
				logger.log(Level.SEVERE,"pdfurl not null");
				String finalurl=pdfurl+report.getId()+"&preprint=no";
				tableBuilder.append("<td><a href=\""+finalurl+"\">"+report.getCount()+"</a></td>");
			}else
				tableBuilder.append("<td>"+report.getCount()+"</td>");
			tableBuilder.append("<td>"+fmt.format(report.getAssessmentDate())+"</td>");
			if(report.getAssessedPeron1()!=null&&!report.getAssessedPeron1().equals("")&&!report.getAssessedPeron1().equalsIgnoreCase("null"))
				tableBuilder.append("<td>"+report.getAssessedPeron1()+"</td>");
			else
				tableBuilder.append("<td> </td>");
			tableBuilder.append("<td>"+report.getCinfo().getFullName()+"</td>");
			tableBuilder.append("<td>"+report.getCustomerBranch()+"</td>");
			tableBuilder.append("<td>"+report.getServiceLocation()+"</td>");
			int totalObv=0;
			int openObv=0;
			int closedObv=0;
			if(report.getAssessmentDetailsLIst()!=null) {			
				totalObv=report.getAssessmentDetailsLIst().size();
				for(AssesmentReportEmbedForTable ar:report.getAssessmentDetailsLIst()) {
					if(ar.getStatus().equals("Open")||ar.getStatus().equals("Created"))
						openObv++;
					if(ar.getStatus().equals("Closed"))
						closedObv++;
				}
			}
			tableBuilder.append("<td>"+totalObv+"</td>");
			tableBuilder.append("<td>"+openObv+"</td>");
			tableBuilder.append("<td>"+closedObv+"</td>");					
			tableBuilder.append("</tr>");
		
		}
		tableBuilder.append("</table>");
	//	logger.log(Level.SEVERE,"table content="+tableBuilder.toString());
		return tableBuilder.toString();
	}
}

