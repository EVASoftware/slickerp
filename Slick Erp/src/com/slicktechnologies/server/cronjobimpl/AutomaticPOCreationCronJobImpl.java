package com.slicktechnologies.server.cronjobimpl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GeneralServiceImpl;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

/**
 * @author Viraj
 * Date: 20-05-2019
 * Description: To create PO when the products availableQty gets below reOrderQty
 */
public class AutomaticPOCreationCronJobImpl extends HttpServlet{

	private static final long serialVersionUID = 8505065521075704036L;
	
	Logger logger = Logger.getLogger("AutomaticPOCreationCronJobImpl.class");
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

	List<WareHouse> warehouselist=new ArrayList<WareHouse>();
	ArrayList<ProductDetailsPO> Productlist = new ArrayList<ProductDetailsPO>();
	ArrayList<ProductOtherCharges> pChrgList = new ArrayList<ProductOtherCharges>();
	ArrayList<SalesLineItem> salesItem = new ArrayList<SalesLineItem>();
	ServerAppUtility serverapp = new ServerAppUtility();
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		createAutomaticPO();
	}

	private void createAutomaticPO() {
		List<Company> compEntity = ofy().load().type(Company.class).list();
		if(compEntity.size()>0){
			logger.log(Level.SEVERE,"If compEntity size > 0");
			logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
			
			for(int i=0;i<compEntity.size();i++){
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", "EnableAutomaticPOCreation", compEntity.get(i).getCompanyId())) {
					ArrayList<ProductInventoryViewDetails> productInventoryViewDetailsListForPO = new ArrayList<ProductInventoryViewDetails>();
					
					List<ProductInventoryView> productInventoryViewList = ofy().load().type(ProductInventoryView.class)
							.filter("companyId", compEntity.get(i).getCompanyId()).list();
					
					for(ProductInventoryView productInventory : productInventoryViewList){
						for(ProductInventoryViewDetails prodViewDetails : productInventory.getDetails()){
							if(prodViewDetails.getAvailableqty()<= prodViewDetails.getReorderlevel()) {
								productInventoryViewDetailsListForPO.add(prodViewDetails);
							}
						}
					}
					logger.log(Level.SEVERE,"productInventoryViewDetailsListForPO size: "+productInventoryViewDetailsListForPO.size());
					createPO(productInventoryViewDetailsListForPO, compEntity.get(i).getCompanyId());
				}
			}
			
		}
	}

	private void createPO(ArrayList<ProductInventoryViewDetails> prodList,
			Long companyId) {
		logger.log(Level.SEVERE,"Creating PO");
		
		PurchaseOrder po = new PurchaseOrder();
		Productlist.clear();
		double total = 0;
		
		po.setPODate(new Date());
		po.setStatus(PurchaseOrder.CREATED);
		
		for(ProductInventoryViewDetails pid : prodList){
			ProductDetailsPO ProductDetailsPO= new ProductDetailsPO();
			logger.log(Level.SEVERE,"product name: "+pid.getProdname());
			
			ItemProduct product = ofy().load().type(ItemProduct.class)
					.filter("companyId", companyId)
					.filter("count", pid.getProdid()).first().now();
			
			if(product != null) {
				
				logger.log(Level.SEVERE,"product name: "+product.getProductName());
				ProductDetailsPO.setPrduct(product);
				ProductDetailsPO.setProductID(pid.getProdid());
				ProductDetailsPO.setProductName(pid.getProdname());
				ProductDetailsPO.setProductCode(pid.getProdcode());
				ProductDetailsPO.setProductCategory(product.getProductCategory());
				ProductDetailsPO.setUnitOfmeasurement(product.getUnitOfMeasurement());
				
				ProductDetailsPO.setSalesPrice(product.getPrice());
				ProductDetailsPO.setSalesTax1(product.getVatTax());
				ProductDetailsPO.setSalesTax2(product.getServiceTax());
				ProductDetailsPO.setProdPrice(product.getPurchasePrice());
				
				ProductDetailsPO.setPurchaseTax1(product.getPurchaseTax1());
				ProductDetailsPO.setPurchaseTax2(product.getPurchaseTax2());
				
				
				ProductDetailsPO.setProductQuantity(pid.getReorderqty());
				
				if(pid.getReorderqty() != 0) {
					total += product.getPurchasePrice() * pid.getReorderqty();
				} else {
					total += product.getPurchasePrice();
				}
				
				SalesLineItem item=new SalesLineItem();
				item.setPrduct(product);
				item.setArea(pid.getReorderqty()+"");		
				item.setPrice(product.getPurchasePrice());
				item.setVatTax(product.getPurchaseTax1());
				item.setServiceTax(product.getPurchaseTax2());
			
				salesItem.add(item);
				logger.log(Level.SEVERE,"reorderqty: "+pid.getReorderqty());
				logger.log(Level.SEVERE,"VatTax: "+product.getPurchaseTax1().getPercentage());
				logger.log(Level.SEVERE,"ServiceTax: "+product.getPurchaseTax2().getPercentage());
				
				Productlist.add(ProductDetailsPO);
			}
			
		}
		
		List<ProductOtherCharges> list = new ArrayList<ProductOtherCharges>();
		List<ProductOtherCharges> taxList = new ArrayList<ProductOtherCharges>();
		try {
			list.addAll(serverapp.addProdTaxes(salesItem , taxList , AppConstants.PURCHASEORDER));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ArrayList<ProductOtherCharges> arrBillTax=new ArrayList<ProductOtherCharges>();
		double assessValue=0;
		double tax = 0;
		for(int i=0;i<list.size();i++){
			ProductOtherCharges taxDetails=new ProductOtherCharges();
			taxDetails.setChargeName(list.get(i).getChargeName());
			taxDetails.setChargePercent(list.get(i).getChargePercent());
			assessValue=list.get(i).getAssessableAmount();//*paymentRecieved/100;
			taxDetails.setAssessableAmount(assessValue);				
			taxDetails.setIndexCheck(list.get(i).getIndexCheck());
			tax = tax + taxDetails.getAssessableAmount()*taxDetails.getChargePercent()/100;
			arrBillTax.add(taxDetails);
		}
		
		if(Productlist.size() != 0) {
			po.setProductDetails(Productlist);
		}
		
		po.setTotal(total);
		po.setTotalAmount(total);
		po.setProductTaxes(arrBillTax);
		po.setTotalFinalAmount(Math.round(total+tax));
		po.setNetpayble(Math.round(total+tax));
		
		List<Vendor> vendorList = ofy().load().type(Vendor.class)
				.filter("companyId", companyId).list();
		
		Company comp = ofy().load().type(Company.class)
				.filter("companyId", companyId).first().now();
		
		String url = "";
		if(comp.getFranchiseType().equalsIgnoreCase("FRANCHISE")){
			url = comp.getMasterUrl();
		}else {
			url = comp.getBrandUrl();
		}
		ArrayList<VendorDetails> vendorDetailslist = new ArrayList<VendorDetails>();
		
		for(Vendor v: vendorList) {
			if(v.getVendorRefNo().equals(url.trim())) {
				VendorDetails vd = new VendorDetails();
				
				vd.setVendorId(v.getCount());
				vd.setVendorName(v.getVendorName());
				vd.setVendorPhone(v.getCellNumber1());
				vd.setVendorEmailId(v.getEmail());
				vd.setPocName(v.getfullName());
				vd.setStatus(true);
				vendorDetailslist.add(vd);
			}
		}
		
		ArrayList<PaymentTerms> paymentTermsList = new ArrayList<PaymentTerms>();
		PaymentTerms pt = new PaymentTerms();
		pt.setPayTermDays(1);
		pt.setPayTermPercent(100.00);
		pt.setPayTermComment("Advance Payment");
		paymentTermsList.add(pt);
		
		po.setVendorDetails(vendorDetailslist);
		po.setPaymentTermsList(paymentTermsList);
		
		Branch branch = ofy().load().type(Branch.class)
				.filter("companyId", companyId).first().now();
		
		po.setBranch(branch.getBusinessUnitName());
		po.setEmployee(comp.getPocName());
		po.setApproverName(comp.getPocName());
		po.setApprovalDate(new Date());
		po.setCreatedBy(comp.getPocName());
		po.setCompanyId(companyId);
		logger.log(Level.SEVERE,"companyId in Po: "+companyId);
		po.setAdress(comp.getAddress());
		
		logger.log(Level.SEVERE,"po: "+po);
		
		GeneralServiceImpl genImpl = new GeneralServiceImpl();
		genImpl.saveAndApproveDocument(po, "", "", AppConstants.PURCHASEORDER);
		
		logger.log(Level.SEVERE,"po ID after save: "+po.getCount());
		
		createSalesOrder(url,po);
	}

	private void createSalesOrder(String brandUrl,
			PurchaseOrder po) {
		
		final String CLIENTURL=brandUrl+AppConstants.FRANCHISETYPELIST.SALESORDERGENERATION_URL;
		logger.log(Level.SEVERE,"CLIENT'S URL : "+CLIENTURL);
		
		String data="";
		URL url = null;
		try {
			url = new URL(CLIENTURL);
		} catch (MalformedURLException e) {
			logger.log(Level.SEVERE,"MalformedURLException ERROR::::::::::"+e);
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"STAGE 1");
        HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
			con.setConnectTimeout(60000);
			con.setReadTimeout(60000);
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
			e.printStackTrace();					
		}
		logger.log(Level.SEVERE,"STAGE 2");
        con.setDoInput(true);
        con.setDoOutput(true);
        try {
			con.setRequestMethod("POST");
		} catch (ProtocolException e) {
			logger.log(Level.SEVERE,"ProtocolException ERROR::::::::::"+e);
			e.printStackTrace();					
		}
        logger.log(Level.SEVERE,"STAGE 3");
        OutputStream os = null;
		try {
			os = con.getOutputStream();
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"STAGE 4");
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
        try {
			writer.write(getDataTosendSalesOrder(po));
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException2 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        try {
			writer.flush();
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException3 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        logger.log(Level.SEVERE,"STAGE 5");
        
        
        
        InputStream is = null;
		try {
			is = con.getInputStream();
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException4 ERROR::::::::::"+e);
			e.printStackTrace();
		}
		
		 logger.log(Level.SEVERE,"STAGE 6");
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String temp;
        try {
			while ((temp = br.readLine()) != null) {
			    data = data + temp+"\n"+"\n";
			}
			logger.log(Level.SEVERE,"data data::::::::::"+data);
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException5 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        
        logger.log(Level.SEVERE,"STAGE 7");
        logger.log(Level.SEVERE,"Data::::::::::"+data);
		
	}

	private String getDataTosendSalesOrder(PurchaseOrder po) {
		StringBuilder strB=new StringBuilder();
		strB.append("jsonString");
		strB.append("=");
		strB.append(createJSONObjectFromSalesOrder(po));
		
		return strB.toString();
	}

	private Object createJSONObjectFromSalesOrder(
			PurchaseOrder po) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		Gson gson = new Gson();
		org.json.simple.JSONObject jObj=new org.json.simple.JSONObject();
		
		ArrayList<PurchaseOrder> poList=new ArrayList<PurchaseOrder>();
		poList.add(po);
		
		JSONArray jsonArray = null;
		try {
			jsonArray = new JSONArray(gson.toJson(poList));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.log(Level.SEVERE,"Json array exception : "+e);
		}
		
		jObj.put("Po_list",jsonArray);
		
		String jsonString=jObj.toJSONString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE,"vendor Data:: "+jsonString);
		
		return jsonString;
	}
}
