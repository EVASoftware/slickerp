package com.slicktechnologies.server.cronjobimpl;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

public class InboundCronJobImpl extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7286751452903428290L;
	/**
	 * 
	 */

	Logger logger = Logger.getLogger("PoCronJobInboundImpl.class");
	long companyId = 0;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String urlcalled = req.getRequestURL().toString().trim();
		logger.log(Level.SEVERE, "urlcalled:::::" + urlcalled);
		String urlContextPath = req.getContextPath().toString().trim();
		logger.log(Level.SEVERE, "urlContextPath:::::" + urlContextPath);
		String url1 = urlcalled.replace("http://", "");
		logger.log(Level.SEVERE, "url1:::::" + url1);
		String[] splitUrl = url1.split("\\.");
		logger.log(Level.SEVERE, "splitUrl[0]:::::" + splitUrl[0]);

		Company company = ofy().load().type(Company.class)
				.filter("accessUrl", "my").first().now();
		companyId = company.getCompanyId();
		// TODO Auto-generated method stub
		final String inboundPOAPI = AppConstants.NBHCPOINBOUNDINTEGRATE;
		String data = "";

		URL url = null;
		try {
			url = new URL(inboundPOAPI + "?Type=ZCPM"); // Type here will filter
														// data and fetch. ZCPM
														// is for CCPM and ZCSG
														// and ZTRD for Cold
														// Storage

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "MalformedURLException ERROR::::::::::"
					+ e);
			e.printStackTrace();
		}
		HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		con.setDoInput(true);
		con.setRequestProperty("Content-Type",
				"application/json; charset=UTF-8");
		try {
			con.setRequestMethod("GET");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "ProtocolException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		InputStream is = null;
		try {
			is = con.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String temp;

		try {
			while ((temp = br.readLine()) != null) {
				data = data + temp;
			}
			logger.log(Level.SEVERE, "data data::::::::::" + data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		logger.log(Level.SEVERE, "Data::::::::::" + data);
		updateAccountingInterfaceData(company, data);
	}

	private void updateAccountingInterfaceData(Company company, String data) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE, "Inside updateAccountingInterfaceData" + data);
		JSONArray jsonArray = null;
		try {
			jsonArray = new JSONArray(data);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// ArrayList<AccountingInterface> acctIntList=new
		// ArrayList<AccountingInterface>();
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject poJson = null;
			try {
				poJson = jsonArray.getJSONObject(i);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			JSONArray poProductArray = null;
			try {
				poProductArray = poJson.getJSONArray("liPOItems");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// Number of Interface Document to be created
			for (int j = 0; j < poProductArray.length(); j++) {
				JSONObject poProductObj = null;
				try {
					poProductObj = poProductArray.getJSONObject(j);
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				AccountingInterface acctInt = new AccountingInterface();
				SimpleDateFormat spf = new SimpleDateFormat("dd MMM yyyy");
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				spf.setTimeZone(TimeZone.getTimeZone("IST"));
				SimpleDateFormat sdf = new SimpleDateFormat(
						"dd-MM-yyyy hh:mm:ss");
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				spf.setTimeZone(TimeZone.getTimeZone("IST"));
				try {
					acctInt.setAccountingInterfaceCreationDate(spf.parse(spf
							.format(new Date())));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				acctInt.setStatus(AccountingInterface.TALLYCREATED);
				acctInt.setModule(AppConstants.PURCHASEMODULE);
				acctInt.setDocumentType(AppConstants.PURCHASEORDER);
				acctInt.setDocumentTitle(""); //
				acctInt.setReferenceDocumentNo1(poJson.optString("PO_NUMBER")
						.trim());
				try {
					acctInt.setReferenceDocumentDate1(sdf.parse(poJson
							.optString("DOC_DATE").trim()));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				acctInt.setVendorID(Integer.parseInt(poJson.optString("VENDOR")
						.trim()));
				acctInt.setVendorName(poJson.optString("VENDOR_NAME1").trim()
						+ poJson.optString("VENDOR_NAME2").trim());
				if (poJson.optString("VENDOR_TEL_NUMBER").trim().equals("")) {
					acctInt.setVendorCell(0);
				} else {
					acctInt.setVendorCell(Long.parseLong(poJson.optString(
							"VENDOR_TEL_NUMBER").trim()));
				}
				acctInt.setBranch(poJson.optString("BRANCH").trim());

				/*
				 * Setting Product
				 */

				acctInt.setProductCode(poProductObj.optString("MATERIAL")
						.trim());
				acctInt.setQuantity(Double.parseDouble(poProductObj.optString(
						"QUANTITY").trim()));
				acctInt.setUnitOfMeasurement(poProductObj.optString("UNIT")
						.trim());
				acctInt.setProductPrice(Double.parseDouble(poProductObj
						.optString("NET_PRICE").trim()));
				acctInt.setProductRefId(poProductObj.optString("PO_ITEM")
						.trim());
				acctInt.setWareHouseCode(poProductObj.optString("STORE_LOC"));
				acctInt.setDirection(AccountingInterface.INBOUNDDIRECTION);
				acctInt.setSourceSystem(AppConstants.NBHC);
				acctInt.setDestinationSystem(AppConstants.CCPM);
				acctInt.setCompanyId(company.getCompanyId());
				try {
					acctInt.setReferenceDocumentDate2(sdf.parse(poProductObj
							.optString("DELIV_DATE").trim()));
				} catch (ParseException e) {
					e.printStackTrace();
					logger.log(Level.SEVERE, "Error in parsing date 2");
				}
				GenricServiceImpl impl = new GenricServiceImpl();
				impl.save(acctInt);
			}
		}
	}
}
