package com.slicktechnologies.server.cronjobimpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.googlecode.objectify.ObjectifyService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.addhocprinting.SalesInvoicePdf;
import com.slicktechnologies.server.api.DigitalPaymentAPI;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.email.SendGridEmailServlet;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.email.EmailDetails;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.email.EmailTemplateConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.factory.SmsFactory;
import com.twilio.sdk.resource.instance.Account;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.smshistory.SmsHistory;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class ContractRenewalCronJobImpl extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7601398958528734453L;
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	private DateFormat fmtPaymentDate = new SimpleDateFormat("dd/MM/yyyy");


	 Logger logger = Logger.getLogger("NameOfYourLogger");
	  
		public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws IOException {
				ContractRenewallist();
				
		}

	private void ContractRenewallist() {
			
			
			Email cronEmail = new Email();
			
			Date today=DateUtility.getDateWithTimeZone("IST", new Date());
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			
			Date todayDate=DateUtility.getDateWithTimeZone("IST", new Date());

			
			/**
			 *   Date which is  +30 days with time 00 00 000 
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding  Day"+today);
			Calendar cal4=Calendar.getInstance();
			cal4.setTime(today);
			cal4.add(Calendar.DATE, +30);
			
			Date plusthirtydayswithzero=null;
			
			try {
				plusthirtydayswithzero=dateFormat.parse(dateFormat.format(cal4.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date +30 Days Date"+plusthirtydayswithzero);
			
			
			/**
			 *   Date which is  -30 days with time 00 00 000
			 */
			
			Calendar cal=Calendar.getInstance();
			cal.setTime(today);
			cal.add(Calendar.DATE, -30);
			
			Date minusThirtyDaysDate=null;
			
			try {
				minusThirtyDaysDate=dateFormat.parse(dateFormat.format(cal.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date -30 Days Date"+minusThirtyDaysDate);
			System.out.println("Today Date -30 Days Date: "+minusThirtyDaysDate);
			
			
			
			/**
			 *   Date which is  +30 days  with time 23 59 59 999
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding One Day"+today);
			Calendar cal3=Calendar.getInstance();
			cal3.setTime(today);
			cal3.add(Calendar.DATE, +30);
			
			Date plusThirtyDaysDate=null;
			
			try {
				plusThirtyDaysDate=dateFormat.parse(dateFormat.format(cal3.getTime()));
				cal3.set(Calendar.HOUR_OF_DAY,23);
				cal3.set(Calendar.MINUTE,59);
				cal3.set(Calendar.SECOND,59);
				cal3.set(Calendar.MILLISECOND,999);
				plusThirtyDaysDate=cal3.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date +30 Days Date"+plusThirtyDaysDate);
			
			
			/**
			 * Date 18-03-2017
			 * added by vijay
			 * for send emails to customer 4 weeks before contract end date
			 */
			
			/**
			 *   Date which is  +28 days  with time 23 59 59 999
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding "+today);
			Calendar cal11=Calendar.getInstance();
			cal11.setTime(today);
			cal11.add(Calendar.DATE, +28);
			
			Date plusTwentyEightDaysDate=null;
			
			try {
				plusTwentyEightDaysDate=dateFormat.parse(dateFormat.format(cal11.getTime()));
				cal11.set(Calendar.HOUR_OF_DAY,23);
				cal11.set(Calendar.MINUTE,59);
				cal11.set(Calendar.SECOND,59);
				cal11.set(Calendar.MILLISECOND,999);
				plusTwentyEightDaysDate=cal11.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date +28 Days Date"+plusTwentyEightDaysDate);
			
			
			
			
			/**
			 *   Date which is  +28 days with time 00 00 000 
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding  Day"+today);
			Calendar cal2=Calendar.getInstance();
			cal2.setTime(today);
			cal2.add(Calendar.DATE, +28);
			
			Date plustwentyEightdayswithzero=null;
			
			try {
				plustwentyEightdayswithzero=dateFormat.parse(dateFormat.format(cal2.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date +28 Days Date"+plustwentyEightdayswithzero);
			
			
			/**
			 *   Date which is  +21 days with time 00 00 000 
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding  Day"+today);
			Calendar cal10=Calendar.getInstance();
			cal10.setTime(today);
			cal10.add(Calendar.DATE, +21);
			
			Date plustwentyOnedayswithzero=null;
			
			try {
				plustwentyOnedayswithzero=dateFormat.parse(dateFormat.format(cal10.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date +21 Days Date"+plustwentyOnedayswithzero);
			
			
			/**
			 *   Date which is  +21 days  with time 23 59 59 999
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding "+today);
			Calendar cal5=Calendar.getInstance();
			cal5.setTime(today);
			cal5.add(Calendar.DATE, +21);
			
			Date plusTwentyOneDaysDate=null;
			
			try {
				plusTwentyOneDaysDate=dateFormat.parse(dateFormat.format(cal5.getTime()));
				cal5.set(Calendar.HOUR_OF_DAY,23);
				cal5.set(Calendar.MINUTE,59);
				cal5.set(Calendar.SECOND,59);
				cal5.set(Calendar.MILLISECOND,999);
				plusTwentyOneDaysDate=cal5.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date +21 Days Date"+plusTwentyOneDaysDate);
			
			
			
			/**
			 *   Date which is  +14 days with time 00 00 000 
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding  Day"+today);
			Calendar cal6=Calendar.getInstance();
			cal6.setTime(today);
			cal6.add(Calendar.DATE, +14);
			
			Date plusfourteendayswithzero=null;
			
			try {
				plusfourteendayswithzero=dateFormat.parse(dateFormat.format(cal6.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date +14 Days Date"+plusfourteendayswithzero);
			System.out.println("Today Date +14 Days Date: "+plusfourteendayswithzero);
			
			/**
			 *   Date which is  +14 days  with time 23 59 59 999
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding One Day"+today);
			Calendar cal7=Calendar.getInstance();
			cal7.setTime(today);
			cal7.add(Calendar.DATE, +14);
			
			Date plusfourteenDaysDate=null;
			
			try {
				plusfourteenDaysDate=dateFormat.parse(dateFormat.format(cal7.getTime()));
				cal7.set(Calendar.HOUR_OF_DAY,23);
				cal7.set(Calendar.MINUTE,59);
				cal7.set(Calendar.SECOND,59);
				cal7.set(Calendar.MILLISECOND,999);
				plusfourteenDaysDate=cal7.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date +14 Days Date"+plusfourteenDaysDate);
			
			
			
			/**
			 *   Date which is  +7 days with time 00 00 000 
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding  Day"+today);
			Calendar cal8=Calendar.getInstance();
			cal8.setTime(today);
			cal8.add(Calendar.DATE, +7);
			
			Date plussevendayswithzero=null;
			
			try {
				plussevendayswithzero=dateFormat.parse(dateFormat.format(cal8.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date +7 Days Date"+plussevendayswithzero);
			
			/**
			 *   Date which is  +7 days  with time 23 59 59 999
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding One Day"+today);
			Calendar cal9=Calendar.getInstance();
			cal9.setTime(today);
			cal9.add(Calendar.DATE, +7);
			
			Date plussevenDaysDate=null;
			
			try {
				plussevenDaysDate=dateFormat.parse(dateFormat.format(cal9.getTime()));
				cal9.set(Calendar.HOUR_OF_DAY,23);
				cal9.set(Calendar.MINUTE,59);
				cal9.set(Calendar.SECOND,59);
				cal9.set(Calendar.MILLISECOND,999);
				plussevenDaysDate=cal9.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date +7 Days Date"+plussevenDaysDate);
			
			/**
			 * Ends here
			 */
			
			
			logger.log(Level.SEVERE,"Date Before Adding One Day"+today);
			Calendar cal12=Calendar.getInstance();
			cal12.setTime(today);
			cal12.add(Calendar.DATE, -1);
			
			Date yesterday=null;
			
			try {
				yesterday=dateFormat.parse(dateFormat.format(cal12.getTime()));
				cal12.set(Calendar.HOUR_OF_DAY,23);
				cal12.set(Calendar.MINUTE,59);
				cal12.set(Calendar.SECOND,59);
				cal12.set(Calendar.MILLISECOND,999);
				yesterday=cal12.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE,"yesterday"+yesterday);

			
	 try{
		 
		 logger.log(Level.SEVERE,"In Contract List");	

		 
	/********************************Adding status in the list ***********************/
		 
//		 ArrayList<String> obj = new ArrayList<String>();
//		 obj.add("Approved");

	/******************************Converting todayDate to String format*****************/
		 
		 Date todaysDate = new Date();
		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 String todayDateString = df.format(todaysDate);
		 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);

		 
	/********************************Adding Companies in the list ***********************/
		 
	List<Company> compEntity = ofy().load().type(Company.class).list();
	if(compEntity.size()>0){

		logger.log(Level.SEVERE,"If compEntity size > 0");
		logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
		

		for(int i=0;i<compEntity.size();i++){
			
			Company c=compEntity.get(i);
			logger.log(Level.SEVERE,"Company Name="+c);
			
			
			/**
			 * @author Vijay Date  :- 18-08-2022
			 * Des :- SMS and whats message will work as per communication type and template selected in cron job reminder settings
			 * below old code commented
			 */
//			
//			/**************************** for SMS ***********************************/
//			
////			SmsConfiguration smsconfig = ofy().load().type(SmsConfiguration.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("status",true).first().now();
////			
////			if(smsconfig!=null){
////				logger.log(Level.SEVERE,"in the smsconfig !=null for SMS========"+smsconfig);
////				
////				String accountsid = smsconfig.getAccountSID();
////				String authotoken = smsconfig.getAuthToken();
////				String fromnumber = smsconfig.getPassword();
//			
//			ProcessName smsprocessName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
//			ProcessConfiguration smsprocessconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
//				
//			boolean prossconfigSMSflag = false;
//			if(smsprocessconfig!=null){
//				if(smsprocessconfig.isConfigStatus()){
//					for(int l=0;l<smsprocessconfig.getProcessList().size();l++){
//						if(smsprocessconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && smsprocessconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("ContractRenewalSMS") && smsprocessconfig.getProcessList().get(l).isStatus()==true){
//							prossconfigSMSflag=true;
//						}
//					}
//			   }
//			}
//			logger.log(Level.SEVERE,"process config  SMS Flag =="+prossconfigSMSflag);
//			
////			/**
////			 * Date 16 jun 2017 added by vijay
////			 * below process config is used for calling BHASH SMS API 
////			 */
////			ProcessConfiguration smsprocessconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","SMS").first().now();				
////			
////			boolean bhashSMSAPIFlag = false;
////			if(smsprocessconfig!=null){
////				if(smsprocessconfig.isConfigStatus()){
////					for(int l=0;l<smsprocessconfig.getProcessList().size();l++){
////						if(smsprocessconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("SMS") && smsprocessconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("BhashSMSAPI") && smsprocessconfig.getProcessList().get(l).isStatus()==true){
////							bhashSMSAPIFlag=true;
////						}
////					}
////			   }
////			}
////			
////			logger.log(Level.SEVERE,"process config BHASH SMS API Flag =="+bhashSMSAPIFlag);
////			/**
////			 * ends here
////			 */
//			
//			if(smsprocessName!=null){
//				logger.log(Level.SEVERE,"in the processName !=null for SMS========");
//				
//				if(prossconfigSMSflag){
//					logger.log(Level.SEVERE,"in the process Configuration !=null");
//					
//					SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("event", "Contract Renewal Cron Job" ).filter("status",true).first().now();
//
//					logger.log(Level.SEVERE,"SMSEnitity========"+smsEntity);
//
//					int day = smsEntity.getDay();
//					
//					String templatemsgwithbraces = smsEntity.getMessage();
//					logger.log(Level.SEVERE,"Day ========"+day);
//
//					logger.log(Level.SEVERE,"Message with braces========"+templatemsgwithbraces);
//					
//					
//					/************************************ *****************************/
//					
//					Calendar enddatecal = Calendar.getInstance();
//					enddatecal.setTime(today);
//					enddatecal.add(Calendar.DATE,+day);
//					
//					Date dateforenddatefilter = null;
//					try {
//						dateforenddatefilter = dateFormat.parse(dateFormat.format(enddatecal.getTime()));
//					} catch (ParseException e) {
//							e.printStackTrace();
//					}
//					
//					logger.log(Level.SEVERE,"dateforenddatefilter1========:"+dateforenddatefilter);
//
//					
//					Calendar endatecal2 = Calendar.getInstance();
//					endatecal2.setTime(today);
//					endatecal2.add(Calendar.DATE, +day);
//					Date dateforenddatefilter2 = null;
//					
//					try {
//						
//						dateforenddatefilter2 = dateFormat.parse(dateFormat.format(endatecal2.getTime()));
//						endatecal2.set(Calendar.HOUR_OF_DAY,23);
//						endatecal2.set(Calendar.SECOND,59);
//						endatecal2.set(Calendar.MINUTE,59);
//						endatecal2.set(Calendar.MILLISECOND,999);
//						dateforenddatefilter2 = endatecal2.getTime();
//						
//					} catch (ParseException e) {
//						e.printStackTrace();
//					}
//					logger.log(Level.SEVERE,"dateforenddatefilter2========:"+dateforenddatefilter2);
//						
//						List<Contract> contractentity = ofy().load().type(Contract.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("endDate >=", dateforenddatefilter).filter("endDate <=", dateforenddatefilter2).filter("status","Approved").list();
//						logger.log(Level.SEVERE,"contractEntity:"+contractentity.size());
//					
//						for(int m=0; m<contractentity.size(); m++){
//							
//							/**
//							 * @author Vijay Date :- 09-08-2021
//							 * Des :- added below method to check customer DND status if customer DND status is Active 
//							 * then SMS will not send to customer
//							 */
//							ServerAppUtility serverapputility = new ServerAppUtility();
//							boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(contractentity.get(m).getCinfo().getCount(), contractentity.get(m).getCompanyId());
//							if(!dndstatusFlag){
//								sendSms(compEntity.get(i).getCompanyId(),contractentity.get(m).getCinfo().getFullName(),contractentity.get(m).getCount(),contractentity.get(m).getEndDate(),contractentity.get(m).getCinfo().getCellNumber(),compEntity.get(i).getBusinessUnitName(),templatemsgwithbraces,smsEntity.getEvent());
//
//							}
//							
//						}
//					
//				}
//			}
			
//		  } 

			
	/*************************************** SMS End *******************************************************/		
	/**
	 * Date 18-03-2017
	 * added by vijay
	 * email sent to customer four times last four weeks as per contract end date
	 * 
	 */
			
			ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
			ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
			
			boolean prossconfigCustomerEmailWithoutProcessedflag = false;
			if(processconfig!=null){
				if(processconfig.isConfigStatus()){
					for(int l=0;l<processconfig.getProcessList().size();l++){
						if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("ContractRenewalWithoutProcessedCustomerEmail") && processconfig.getProcessList().get(l).isStatus()==true){
							prossconfigCustomerEmailWithoutProcessedflag=true;
						}
					}
			   }
			}
			logger.log(Level.SEVERE,"process config  without processed contract renewal Flag =="+prossconfigCustomerEmailWithoutProcessedflag);
			
			if(processName!=null){
				logger.log(Level.SEVERE,"In the prossName");
			if(prossconfigCustomerEmailWithoutProcessedflag){
				logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
				
	/********************************Reading ContractList from Contract entity for customer  ***********************/
				
				List<Contract> contractEntityforcustomerContractRenwal = ofy().load().type(Contract.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("endDate >=", plustwentyEightdayswithzero).filter("endDate <=", plusTwentyEightDaysDate).filter("status","Approved").filter("renewFlag", false).filter("customerInterestFlag", false).list();

				
				List<Contract> contractRenewalThirdWeek = ofy().load().type(Contract.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("endDate >=", plustwentyOnedayswithzero).filter("endDate <=", plusTwentyOneDaysDate).filter("status","Approved").filter("renewFlag", false).filter("customerInterestFlag", false).list();
				if(contractRenewalThirdWeek.size()!=0){
					contractEntityforcustomerContractRenwal.addAll(contractRenewalThirdWeek);
				}
				
				List<Contract> contractRenewalSecondWeek = ofy().load().type(Contract.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("endDate >=", plusfourteendayswithzero).filter("endDate <=", plusfourteenDaysDate).filter("status","Approved").filter("renewFlag", false).filter("customerInterestFlag", false).list();
				if(contractRenewalSecondWeek.size()!=0){
					contractEntityforcustomerContractRenwal.addAll(contractRenewalSecondWeek);
				}
				List<Contract> contractRenewalFirstWeek = ofy().load().type(Contract.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("endDate >=", plussevendayswithzero).filter("endDate <=", plussevenDaysDate).filter("status","Approved").filter("renewFlag", false).filter("customerInterestFlag", false).list();
				if(contractRenewalFirstWeek.size()!=0){
					contractEntityforcustomerContractRenwal.addAll(contractRenewalFirstWeek);
				}
				
				
				logger.log(Level.SEVERE, "contractEntityforcustomerRenewal Size:"+contractEntityforcustomerContractRenwal.size());

				for(int j=0;j<contractEntityforcustomerContractRenwal.size();j++){
					
					ArrayList<String> toEmailList1=new ArrayList<String>();
					toEmailList1.add(contractEntityforcustomerContractRenwal.get(j).getCinfo().getEmail()+"");
					
					logger.log(Level.SEVERE, "Contract id" +contractEntityforcustomerContractRenwal.get(j).getCount());
					
					Customer customer = ofy().load().type(Customer.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("count", contractEntityforcustomerContractRenwal.get(j).getCustomerId()).first().now();
					if(customer!=null)
					toEmailList1.add(customer.getEmail());
					String msgBody;
					if(customer.isCompany()){
						msgBody ="</br></br> Dear "+customer.getCompanyName().trim()+ ", <br><br> It has been our privilege to have served you over the past year and we truly appreciated & value our association with you. </br> we trust you have found our services exemplery & to your complete satifaction. Your current contract expires as below on <b>"+fmt.format(contractEntityforcustomerContractRenwal.get(j).getEndDate())+"</b> and we request you to renew the same on or before expiry date.<br><br>";

					}else{
						msgBody ="</br></br> Dear "+customer.getFullname().trim()+ ", <br><br> It has been our privilege to have served you over the past year and we truly appreciated & value our association with you. <br> we trust you have found our services exemplery & to your complete satifaction. Your current contract expires as below on <b>"+fmt.format(contractEntityforcustomerContractRenwal.get(j).getEndDate())+"</b> and we request you to renew the same on or before expiry date.<br><br>";

					}
					
					String mailSubject="Subject : Renewal of Contract Id "+contractEntityforcustomerContractRenwal.get(j).getCount()+" end on "+fmt.format(contractEntityforcustomerContractRenwal.get(j).getEndDate())+".";
					
					//Table header 1
					ArrayList<String> tbl1_header=new ArrayList<String>();
					tbl1_header.add("CODE");
					tbl1_header.add("NAME");
					tbl1_header.add("DURATION");
					tbl1_header.add("SERVICES");
					tbl1_header.add("PRICE");
					tbl1_header.add("TAX 1");
					tbl1_header.add("TAX 2");
					System.out.println("Tbl1 header Size :"+tbl1_header.size());

					List<SalesLineItem> productList=new ArrayList<SalesLineItem>();
					productList=contractEntityforcustomerContractRenwal.get(j).getItems();
					// Table 1
					ArrayList<String> tbl1=new ArrayList<String>();
					for(int k=0;k<productList.size();k++){
					tbl1.add(productList.get(k).getProductCode());
					tbl1.add(productList.get(k).getProductName());
					tbl1.add(productList.get(k).getDuration()+"");
					tbl1.add(productList.get(k).getNumberOfServices()+"");
					tbl1.add(productList.get(k).getPrice()+"");
					
					ServiceProduct serviceProduct = ofy().load().type(ServiceProduct.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("count", productList.get(k).getPrduct().getCount()).first().now();
					logger.log(Level.SEVERE,"Service product ==="+serviceProduct);		
					if(serviceProduct!=null){
						logger.log(Level.SEVERE,"Service product");
						tbl1.add(serviceProduct.getServiceTax()+"");
						tbl1.add(serviceProduct.getVatTax()+"");
					}
					
					}
					System.out.println("Tbl1 Size :"+tbl1.size());
					
					if(!contractEntityforcustomerContractRenwal.get(j).getCinfo().getEmail().equalsIgnoreCase("xyz@abc.com")){
						cronEmail.htmlformatsendEmail(toEmailList1, mailSubject, "Contract Renewal", c, msgBody, null, null, tbl1_header, tbl1,null,null);
						logger.log(Level.SEVERE,"After send mail method ");		

					}
				}
			 }
			
			}
			
	/**
	 * ends here	
	 */
	
	/**
	 * Date 18-03-2017
	 * added by vijay
	 * this email is sent to customer if we processed the contract in contract renewal screen
	 *  Email sent four time before contract end date last four week		
	 */
			boolean processedRenewalconfigflag = false;
			if(processconfig!=null){
				if(processconfig.isConfigStatus()){
					for(int l=0;l<processconfig.getProcessList().size();l++){
						if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("ProcessedContractRenewalEmail") && processconfig.getProcessList().get(l).isStatus()==true){
							processedRenewalconfigflag=true;
						}
					}
			   }
			}
			logger.log(Level.SEVERE,"With Contract Renewal processed config Flag =="+processedRenewalconfigflag);
			
			if(processName!=null){
				logger.log(Level.SEVERE,"In the prossName");
			if(processedRenewalconfigflag){
							
				logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
				
			logger.log(Level.SEVERE, "Before filter contractEntityforcustomer" );
			List<ContractRenewal> contractRenewalEntity = ofy().load().type(ContractRenewal.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("contractEndDate >=", plustwentyEightdayswithzero).filter("contractEndDate <=", plusTwentyEightDaysDate).filter("status","Processed").list();

			
			List<ContractRenewal> contractRenewalThirdWeek = ofy().load().type(ContractRenewal.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("contractEndDate >=", plustwentyOnedayswithzero).filter("contractEndDate <=", plusTwentyOneDaysDate).filter("status","Processed").list();
			if(contractRenewalThirdWeek.size()!=0){
				contractRenewalEntity.addAll(contractRenewalThirdWeek);
			}
			
			List<ContractRenewal> contractRenewalSecondWeek = ofy().load().type(ContractRenewal.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("contractEndDate >=", plusfourteendayswithzero).filter("contractEndDate <=", plusfourteenDaysDate).filter("status","Processed").list();
			if(contractRenewalSecondWeek.size()!=0){
				contractRenewalEntity.addAll(contractRenewalSecondWeek);
			}
			List<ContractRenewal> contractRenewalFirstWeek = ofy().load().type(ContractRenewal.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("contractEndDate >=", plussevendayswithzero).filter("contractEndDate <=", plussevenDaysDate).filter("status","Processed").list();
			if(contractRenewalFirstWeek.size()!=0){
				contractRenewalEntity.addAll(contractRenewalFirstWeek);
			}
			logger.log(Level.SEVERE, "contractRenewalEntity for customer Size:"+contractRenewalEntity.size());

			System.out.println(contractRenewalEntity.size());
			for(int j=0;j<contractRenewalEntity.size();j++){
				ArrayList<String> toEmailList1=new ArrayList<String>();
				
				Customer customer = ofy().load().type(Customer.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("count", contractRenewalEntity.get(j).getCustomerId()).first().now();
				if(customer!=null)
				toEmailList1.add(customer.getEmail());
				String msgBody;
				if(customer.isCompany()){
					msgBody ="</br></br> Dear "+customer.getCompanyName().trim()+ ", <br><br> It has been our privilege to have served you over the past year and we truly appreciated & value our association with you. <br> we trust you have found our services exemplery & to your complete satifaction. Your current contract expires as below on <b>"+fmt.format(contractRenewalEntity.get(j).getContractEndDate())+"</b> and we request you to renew the same on or before expiry date.<br><br>";

				}else{
					msgBody ="</br></br> Dear "+customer.getFullname().trim()+ ", <br><br> It has been our privilege to have served you over the past year and we truly appreciated & value our association with you. <br> we trust you have found our services exemplery & to your complete satifaction. Your current contract expires as below on <b>"+fmt.format(contractRenewalEntity.get(j).getContractEndDate())+"</b> and we request you to renew the same on or before expiry date.<br><br>";

				}
				
				String mailSubject="Subject : Renewal of Contract Id "+contractRenewalEntity.get(j).getContractId()+" end on "+fmt.format(contractRenewalEntity.get(j).getContractEndDate())+".";
				
				//Table header 1
				ArrayList<String> tbl1_header=new ArrayList<String>();
				tbl1_header.add("CODE");
				tbl1_header.add("NAME");
				tbl1_header.add("DURATION");
				tbl1_header.add("SERVICES");
				tbl1_header.add("PRICE");
				tbl1_header.add("TAX 1");
				tbl1_header.add("TAX 2");
				System.out.println("Tbl1 header Size :"+tbl1_header.size());

				List<SalesLineItem> productList=new ArrayList<SalesLineItem>();
				productList=contractRenewalEntity.get(j).getItems();
				// Table 1
				ArrayList<String> tbl1=new ArrayList<String>();
				for(int k=0;k<productList.size();k++){
				tbl1.add(productList.get(k).getProductCode());
				tbl1.add(productList.get(k).getProductName());
				tbl1.add(productList.get(k).getDuration()+"");
				tbl1.add(productList.get(k).getNumberOfServices()+"");
				tbl1.add(productList.get(k).getPrice()+"");
				tbl1.add(productList.get(k).getServiceTax()+"");
				tbl1.add(productList.get(k).getVatTax()+"");
				}
				System.out.println("Tbl1 Size :"+tbl1.size());
				
				if(!customer.getEmail().equalsIgnoreCase("xyz@abc.com")){
					cronEmail.htmlformatsendEmail(toEmailList1, mailSubject, "Contract Renewal", c, msgBody, null, null, tbl1_header, tbl1,null,null);

				}
			}
			logger.log(Level.SEVERE, "After filter 9" );
			}
			
			}
	/**
	 * ends here
	 */
			
		
	/**
	 * Date 21 April 2017 added by vijay for contract renewal remainder before 30 days daily to EVA CLIENTS RENEWAL
	 * 		
	 */
			ProcessName processNameEVA = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
			ProcessConfiguration processConfigEVA = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
			boolean productflag = false;
			productflag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CronJob", "GetProductPriceFromContractRenewal",compEntity.get(i).getCompanyId());
			boolean renewalsRemainders = false;
			if(processConfigEVA!=null){
				if(processConfigEVA.isConfigStatus()){
					for(int l=0;l<processConfigEVA.getProcessList().size();l++){
						if(processConfigEVA.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processConfigEVA.getProcessList().get(l).getProcessType().equalsIgnoreCase("EVALicenseReminder") && processConfigEVA.getProcessList().get(l).isStatus()==true){
							renewalsRemainders=true;
						}
					}
			   }
			}
			logger.log(Level.SEVERE,"renewalsRemainders"+renewalsRemainders);
			
			if(processNameEVA!=null){
				logger.log(Level.SEVERE,"In the prossName");
			if(renewalsRemainders){
				logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
				
				List<Contract> contractRenewalslist = ofy().load().type(Contract.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("endDate >=", todayDate).filter("endDate <=", plusThirtyDaysDate).filter("status","Approved").filter("renewFlag", false).filter("customerInterestFlag", false).list();
				logger.log(Level.SEVERE,"contractRenewalslist size=="+contractRenewalslist.size());
                
				
				for(int m =0;m<contractRenewalslist.size();m++){
					
					ArrayList<String> toEmailList1=new ArrayList<String>();
					
					Customer customer = ofy().load().type(Customer.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("count", contractRenewalslist.get(m).getCustomerId()).first().now();
					
					
					/**Date 7-11-2020 by Amol pick the mail id from contact persom detail if customer has 2 POC and 
					 * then mail should be sent on particular selected POC in Contract .raised by Nitin Sir.
					 */
					
					ContactPersonIdentification conPersonIden=null;
					if(contractRenewalslist.get(m).getPocName()!=null){
					logger.log(Level.SEVERE,"poc in contract "+contractRenewalslist.get(m).getPocName());
					conPersonIden= ofy().load().type(ContactPersonIdentification.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("name", contractRenewalslist.get(m).getPocName()).first().now();
//					logger.log(Level.SEVERE,"ContactPersonIdentification poc name "+conPersonIden.getName());
					}
					
					if(conPersonIden!=null){
						if(conPersonIden.getEmail()!=null&&!conPersonIden.getEmail().equals("")){
							toEmailList1.add(conPersonIden.getEmail());
							logger.log(Level.SEVERE, "contact person email "+conPersonIden.getEmail());
						}else if(conPersonIden.getEmail1()!=null&&!conPersonIden.getEmail1().equals("")){
							toEmailList1.add(conPersonIden.getEmail1());
							logger.log(Level.SEVERE, "contact person email1111 "+conPersonIden.getEmail1());
						}else{
							if(customer!=null)
								toEmailList1.add(customer.getEmail());
							logger.log(Level.SEVERE, "customer master email "+customer.getEmail());
						}
					}else{
						if(customer!=null)
							toEmailList1.add(customer.getEmail());
						logger.log(Level.SEVERE, "inside else customer master email "+customer.getEmail());
					}
					
					
					String customerName = getFirstLetterUpperCase(customer.getFullname());
					
					String msgBody;
						msgBody ="</br></br> Dear "+customerName+ ", <br><br> Your EVA ERP License is expiring on "+fmt.format(contractRenewalslist.get(m).getEndDate())+". We request you to make your payment on <br> or before "+fmt.format(contractRenewalslist.get(m).getEndDate())+" to get the uninterrupted services. <br><br>";

					
					 String StringContractEndDate = dateFormat.format(contractRenewalslist.get(m).getEndDate());
			   	 	    
		   				Date d1 = null;
		   				Date d2 = null;
		   				
		   				d1 = df.parse(StringContractEndDate);
		   				d2 = df.parse(todayDateString);
		   				long diff = d2.getTime() - d1.getTime();
						long diffDays = Math.abs(diff / (24 * 60 * 60 * 1000));
						
					String mailSubject="EVA ERP License Reminder Days Left "+diffDays;
					
					int contractId = contractRenewalslist.get(m).getCount();
					String referenceNumber = contractRenewalslist.get(m).getRefNo();
					String title="";
					String clientName = customer.getCompanyName();
					if(referenceNumber.equals("")){
						title="EVA ERP License Reminder - Days Left "+diffDays+" Client Name - "+clientName+" Contract Id - "+contractId;

					}else{
						
						title="EVA ERP License Reminder - Days Left "+diffDays+" Client Name - "+clientName+" Contract Id - "+contractId+" Reference Number - "+referenceNumber;

					}
					
					//Table header 1
					ArrayList<String> tbl1_header=new ArrayList<String>();
					tbl1_header.add("CODE");
					tbl1_header.add("NAME");
					tbl1_header.add("DURATION");
					tbl1_header.add("PRICE");
					tbl1_header.add("TAX 1");
					tbl1_header.add("TAX 2");
					tbl1_header.add("Total");
					System.out.println("Tbl1 header Size :"+tbl1_header.size());

					List<SalesLineItem> productList=new ArrayList<SalesLineItem>();
					productList=contractRenewalslist.get(m).getItems();
					// Table 1
					ArrayList<String> tbl1=new ArrayList<String>();
					
					double netpayable=0;
					
					boolean licensedFlag = false;

					for(int k=0;k<productList.size();k++){
						
					/**
					 * Date 13-08-2018 By Vijay
					 * Des :- renewal reminder email should go only for product which classification is " Licensed ".
					 * 	
					 */
					ServiceProduct serviceProduct = ofy().load().type(ServiceProduct.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("count", productList.get(k).getPrduct().getCount()).first().now();
					logger.log(Level.SEVERE,"Service product ==="+serviceProduct);		

					if(serviceProduct!=null){
						if(serviceProduct.getProductClassification().equals("Licensed")){
							licensedFlag = true;
								
					tbl1.add(productList.get(k).getProductCode());
					tbl1.add(productList.get(k).getProductName());
					tbl1.add(productList.get(k).getDuration()+"");
					
					
//					ServiceProduct serviceProduct = ofy().load().type(ServiceProduct.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("count", productList.get(k).getPrduct().getCount()).first().now();
//					logger.log(Level.SEVERE,"Service product ==="+serviceProduct);		
//					if(serviceProduct!=null){
						logger.log(Level.SEVERE,"Service product");
					//   rohan added this on date : 11 feb 2019 for product price to be taken from contract renewal list
						double vatTaxAmt;
						double serviceTax;
						double totalAmt;
						if(productflag){
							tbl1.add(productList.get(k).getPrice()+"");
							vatTaxAmt = productList.get(k).getPrice()*productList.get(k).getVatTax().getPercentage()/100;
							serviceTax = productList.get(k).getPrice()*productList.get(k).getServiceTax().getPercentage()/100;
							totalAmt = productList.get(k).getPrice()+vatTaxAmt+serviceTax;
						}
						else{
							tbl1.add(serviceProduct.getPrice()+"");
							vatTaxAmt = serviceProduct.getPrice()*serviceProduct.getVatTax().getPercentage()/100;
							serviceTax = serviceProduct.getPrice()*serviceProduct.getServiceTax().getPercentage()/100;
							totalAmt = serviceProduct.getPrice()+vatTaxAmt+serviceTax;
						}
						
						tbl1.add(serviceProduct.getServiceTax()+"");
						tbl1.add(serviceProduct.getVatTax()+"");
					
//					vatTaxAmt = serviceProduct.getPrice()*serviceProduct.getVatTax().getPercentage()/100;
////					double productpricetotal = serviceProduct.getPrice()+vatTaxAmt;
//					serviceTax = serviceProduct.getPrice()*serviceProduct.getServiceTax().getPercentage()/100;
//					totalAmt = serviceProduct.getPrice()+vatTaxAmt+serviceTax;
					tbl1.add(Math.round(totalAmt)+"");
					netpayable += totalAmt;
					System.out.println("Net payable ="+netpayable);
//					}
					
						if(k==productList.size()-1){
							k++;
							tbl1.add("Net Payable");
							tbl1.add(Math.round(netpayable)+"");
							k++;
							String netpayableamtInwords = SalesInvoicePdf.convert(netpayable);
							tbl1.add(netpayableamtInwords);
						}
	
						}
						
					  }
					}
					
					String paymentTableTitle = "Please find below payment options" ;
					String paymentTableTitle2 = "Payment Modes";
					ArrayList<String> tbl2_header=new ArrayList<String>();
					tbl2_header.add("Modes");
					tbl2_header.add("Favoring");
					tbl2_header.add("Details");
					
					Date endDatePlusOneDay = getDate(contractRenewalslist.get(m).getEndDate());
					
					String msgBodyTwo = "As this is automated process & your login to the EVA ERP will be disabled on "+fmt.format(endDatePlusOneDay)+" </br> in case payment is not received by "
					+fmt.format(contractRenewalslist.get(m).getEndDate())+". Post this date data would be retained for next 7 days.</br> After 7 days of expiry your data will be deleted  automatically from the system by background job.</br> ";
							
					String msgbodyThree =  "In case you do not wish to renew the license, request you to revert back to us with the mail confirmation.</br></br>"
							+ " Also download all the data from each screen download option before  7 days of license expiry. </br></br></br>";
					
					String footermsg = "All other terms & conditions remains same as earlier.";
					
					if(!customer.getEmail().equalsIgnoreCase("xyz@abc.com") && licensedFlag){
						cronEmail.EVARenewalEmail(toEmailList1, mailSubject, title, c, msgBody,null,null, tbl1_header, tbl1,paymentTableTitle, paymentTableTitle2, tbl2_header,  msgBodyTwo, msgbodyThree, null, footermsg, null);
					}
				}
				
				/**
				 * Date 22-04-2017
				 * added by vijay for after renewal expiry Email
				 */
				logger.log(Level.SEVERE,"TODAY DATE"+today);
//				Date yesterday = getYesterdayDateWithTime(today);
				Date minusSevenDateWithZero = getsevenDaysminusDateTimeZero(today);
				logger.log(Level.SEVERE,"yesterday"+yesterday);
				logger.log(Level.SEVERE,"minusSevenDateWithZero"+minusSevenDateWithZero);

				List<Contract> contractEntity = ofy().load().type(Contract.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("endDate >=", minusSevenDateWithZero).filter("endDate <=", yesterday).filter("status","Approved").filter("renewFlag", false).filter("customerInterestFlag", false).list();
				logger.log(Level.SEVERE,"After renewal EMAIL Contractentity size"+contractEntity.size());
				for(int p=0;p<contractEntity.size();p++){
					
					ArrayList<String> toEmailList1=new ArrayList<String>();
					
					Customer customer = ofy().load().type(Customer.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("count", contractEntity.get(p).getCustomerId()).first().now();
					if(customer!=null){
					toEmailList1.add(customer.getEmail());
					
					/**
					 * Date 13-08-2018 By Vijay
					 * Des :- renewal expired reminder email should go only for product(Master) which classification is " Licensed ".
					 */
					boolean licensedFlag=false;
					for(int k=0;k<contractEntity.get(p).getItems().size();k++){
						
						ServiceProduct serviceProduct = ofy().load().type(ServiceProduct.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("count", contractEntity.get(p).getItems().get(k).getPrduct().getCount()).first().now();
						if(serviceProduct!=null){
							if(serviceProduct.getProductClassification().equals("Licensed")){
								licensedFlag = true;
							}
						}
					}	
					/**
					 * ends here
					 */
					String customerName = getFirstLetterUpperCase(customer.getFullname());
					String msgBody;
						msgBody ="</br></br> Dear "+customerName+ ", <br><br> Your EVA ERP License has expired. After 7 days of license expiry your data will be deleted from<br> the system by background job <br><br>";
					 String msg2withRed ="As this is automated process & your login to the EVA ERP has been disabled on "+ dateFormat.format(todaysDate) +" as no payment is received."
								+ "<br><br> Data  would be retained for next 7 days. After 7 days of license expiry your data will be deleted automatically from the system by background job. We recommend you to download the data as soon as possible.<br><br> ";
					 String msg3 = "In case you would like to renew the license kindly make the payment as soon as possible. <br> Payment details as follows";

					 String StringContractEndDate = dateFormat.format(contractEntity.get(p).getEndDate());
						String clientName = customer.getCompanyName();
						int contractId = contractEntity.get(p).getCount();
						String referenceNumber = contractEntity.get(p).getRefNo();
						String title="";
						if(referenceNumber.equals("")){
							title="EVA ERP License Expired on "+StringContractEndDate+" Client Name - "+clientName+" Contract Id - "+contractId;

						}else{
							
							title="EVA ERP License Expired on "+StringContractEndDate+" Client Name - "+clientName+" Contract Id - "+contractId+" Reference Number - "+referenceNumber;

						}
						String mailSubject="EVA ERP License Expired on "+StringContractEndDate;
					
						if(!customer.getEmail().equalsIgnoreCase("xyz@abc.com") && licensedFlag){
							cronEmail.EVARenewalEmail(toEmailList1, mailSubject, title, c, msgBody, msg2withRed,msg3, null, null,null, null, null,  null, null, null, null, null);
	
						}
					}
				}
				
				/**
				 * ends here
				 */
				
				
			}
			
			}
			
			
	/**
	 * Ends here		
	 */
			
			
	/********************************Checking prosname & prosconfig for each company ***********************/

			boolean ContractRenewalDailyEmailFlag = false;
			if(processconfig!=null){
				if(processconfig.isConfigStatus()){
					for(int l=0;l<processconfig.getProcessList().size();l++){
						if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("ContractRenewalDailyEmail") && processconfig.getProcessList().get(l).isStatus()==true){
							ContractRenewalDailyEmailFlag=true;
						}
					}
			   }
			}
			logger.log(Level.SEVERE,"process config Flag =="+ContractRenewalDailyEmailFlag);
			
			logger.log(Level.SEVERE,"after 3 filters for ProcessConfigurations");
		
			if(processName!=null){
				logger.log(Level.SEVERE,"In the prossName");
			if(ContractRenewalDailyEmailFlag){
							
				logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
				
				/********************************Reading ContractList from Contract entity  ***********************/
				
				logger.log(Level.SEVERE, "Before filter" );
				List<Contract> contractEntity = ofy().load().type(Contract.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("endDate >=", minusThirtyDaysDate).filter("endDate <=", plusThirtyDaysDate).filter("status","Approved").filter("renewFlag", false).filter("customerInterestFlag", false).list();
				logger.log(Level.SEVERE, "After filter 4" );
			
				logger.log(Level.SEVERE, "Contract Entity Size:"+contractEntity.size());

				// email id is added to emailList
				ArrayList<String> toEmailList=new ArrayList<String>();
				toEmailList.add(compEntity.get(i).getPocEmail());
				
				System.out.println("POC EMIAL"+compEntity.get(i).getPocEmail());
				
				 String mailTitl = "Contract Renewal Due As On Date";
					logger.log(Level.SEVERE, "After filter 5" );

				if(contractEntity.size()>0){
					logger.log(Level.SEVERE, "After filter 6" );

				ArrayList<String> tbl_header = new ArrayList<String>();
				tbl_header.add("Serial No");
				tbl_header.add("Branch");
				tbl_header.add("Customer Id");
				tbl_header.add("Customer Name");
				tbl_header.add("Customer Cont No");
				tbl_header.add("POC Name");
				tbl_header.add("Email Id");
				tbl_header.add("Sales Person");
				tbl_header.add("Contract Id");
				tbl_header.add("Start Date");
				tbl_header.add("End Date");
				tbl_header.add("Status");
				tbl_header.add("Ageing");
				
				logger.log(Level.SEVERE, "After filter 7" );

	/********************************Sorting table with Branch & Contract End Date ***********************/
				logger.log(Level.SEVERE, "After filter 8" );

				
				Comparator<Contract> contractEndDateComparator2 = new Comparator<Contract>() {
					public int compare(Contract s1, Contract s2) {
					
					Date date1 = s1.getEndDate();
					Date date2 = s2.getEndDate();
					
					//ascending order
					return date1.compareTo(date2);
					}
					};
					Collections.sort(contractEntity, contractEndDateComparator2);
					
					Comparator<Contract> contractEndDateComparator = new Comparator<Contract>() {
						public int compare(Contract s1, Contract s2) {
						
						String branch1 = s1.getBranch();
						String branch2 = s2.getBranch();
						
						//ascending order
						return branch1.compareTo(branch2);
						}
						
						};
						Collections.sort(contractEntity, contractEndDateComparator);
					
	/********************************Getting ContractEntity data and adding in the tbl1 List ***********************/
						logger.log(Level.SEVERE, "After filter 9" );

				ArrayList<String> tbl1=new ArrayList<String>();
					
				for(int j=0;j<contractEntity.size();j++){
					tbl1.add((j+1)+"");
		   	 		tbl1.add(contractEntity.get(j).getBranch()+"");
		   	 		tbl1.add(contractEntity.get(j).getCinfo().getCount()+"");
		   	 		tbl1.add(contractEntity.get(j).getCinfo().getFullName()+"");		   	 		
		   	 		tbl1.add(contractEntity.get(j).getCinfo().getCellNumber()+"");
		   	 		tbl1.add(contractEntity.get(j).getCinfo().getPocName()+"");
		   	 		tbl1.add(contractEntity.get(j).getCinfo().getEmail());
		   	 		tbl1.add(contractEntity.get(j).getEmployee());
		   	 		tbl1.add(contractEntity.get(j).getCount()+"");
		   	 		tbl1.add(fmt.format(contractEntity.get(j).getStartDate())+"");
		   	 	    tbl1.add(fmt.format(contractEntity.get(j).getEndDate()) +"");
		 
		   	 	    /**************** for getting ageing for each Contract ******/
		   	 	    String StringContractEndDate = dateFormat.format(contractEntity.get(j).getEndDate());
		   	 	    
		   				Date d1 = null;
		   				Date d2 = null;
		   				
		   				d1 = df.parse(StringContractEndDate);
		   				d2 = df.parse(todayDateString);
		   				long diff = d2.getTime() - d1.getTime();
						long diffDays = diff / (24 * 60 * 60 * 1000);
						
//						System.out.println("Ageing:"+diffDays);
//				   	 	logger.log(Level.SEVERE,"Ageing:"+diffDays);
			
		   	 	    tbl1.add(contractEntity.get(j).getStatus()+"");
		   	 	    tbl1.add(diffDays +"");
		   	 	}			
				
				logger.log(Level.SEVERE,"In the ProsConfig !=null   3 ");
				
	/********************************Calling cronsendEmail Method ***********************/
				
				try {   
					   
						System.out.println("Before send mail method");
						logger.log(Level.SEVERE,"Before send method call to send mail  ");						
						cronEmail.cronSendEmail(toEmailList, "Contract Renewal Due As On Date"+" "+ todayDateString, mailTitl +"  "+ todayDateString, c, tbl_header, tbl1, null, null, null, null);
						logger.log(Level.SEVERE,"After send mail method ");		
								//+"  "+
				} catch (IOException e1) {
					
					logger.log(Level.SEVERE,"In the catch block ");
					e1.printStackTrace();
			}
				
			}
			else{						
					
				System.out.println("Sorry no ContractRenewal found for this company");
				logger.log(Level.SEVERE,"Sorry no ContractRenewal found for this company");
//				
				mailTitl = "No Contract Renewal Found Due";

				cronEmail.cronSendEmail(toEmailList, "Contract Renewal Due As On Date"+" "+ todayDateString, mailTitl , c, null, null, null, null, null, null);

			}
				
				
			}
			else{					//else block for prosconfig
					logger.log(Level.SEVERE,"ProcessConfiguration is null");
					System.out.println("ProcessConfiguration is null");
			}
			
			}else{        //else block for pross!=null
				logger.log(Level.SEVERE,"Cron job status is Inactive");
				System.out.println("Cron job status is Inactive");
			}
			

			/**
			 * @author Vijay Date :- 16-11-2021
			 * Des ;- Renewal Email to client with Defined Email Template for Renewal_Final_Reminder and Renewal_Overdue_Reminder
			 */

			boolean ContractRenewalDailyEmailToClient = false;
			if(processconfig!=null){
				if(processconfig.isConfigStatus()){
					for(int l=0;l<processconfig.getProcessList().size();l++){
						if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase(AppConstants.CONTRACTRENEWALEMAILTOCLIENT) && processconfig.getProcessList().get(l).isStatus()==true){
							ContractRenewalDailyEmailToClient=true;
						}
					}
			   }
			}
			logger.log(Level.SEVERE,"ContractRenewalDailyEmailToClient process config Flag =="+ContractRenewalDailyEmailToClient);
			logger.log(Level.SEVERE,"ContractRenewalDailyEmailToClient todayDate=="+todayDate);
			logger.log(Level.SEVERE,"ContractRenewalDailyEmailToClient plusThirtyDaysDate=="+plusThirtyDaysDate);

			if(processName!=null){
				logger.log(Level.SEVERE,"In the prossName");
			if(ContractRenewalDailyEmailToClient){
				logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
				List<Contract> contractRenewalslist = ofy().load().type(Contract.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("endDate >=", todayDate).filter("endDate <=", plusThirtyDaysDate).filter("status","Approved").filter("renewFlag", false).filter("customerInterestFlag", false).list();
				logger.log(Level.SEVERE,"contractRenewalslist size == "+contractRenewalslist.size());
                
				logger.log(Level.SEVERE,"Contract renewal email template email to client"+contractRenewalslist.size());

				for(int m =0;m<contractRenewalslist.size();m++){
					
					
					Customer customer = ofy().load().type(Customer.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("count", contractRenewalslist.get(m).getCustomerId()).first().now();
					
					String StringContractEndDate = dateFormat.format(contractRenewalslist.get(m).getEndDate());
	   				Date d1 = null;
	   				Date d2 = null;
	   				
	   				d1 = df.parse(StringContractEndDate);
	   				d2 = df.parse(todayDateString);
	   				long diff = d2.getTime() - d1.getTime();
					long diffDays = Math.abs(diff / (24 * 60 * 60 * 1000));

					EmailDetails emaildetails = new EmailDetails();
					ArrayList<String> toEmailId = new ArrayList<String>();
					toEmailId.add(customer.getEmail());
					
					Branch branchEntity = ofy().load().type(Branch.class).filter("companyId", contractRenewalslist.get(m).getCompanyId())
							.filter("buisnessUnitName", contractRenewalslist.get(m).getBranch()).filter("status", true).first().now();
					ArrayList<String> ccEmailId = new ArrayList<String>();
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", contractRenewalslist.get(m).getCompanyId()) && branchEntity!=null && branchEntity.getEmail()!=null && !branchEntity.getEmail().equals("")){
						ccEmailId.add(branchEntity.getEmail());
					}
					else{
						if(compEntity.get(i).getEmail()!=null)
						ccEmailId.add(compEntity.get(i).getEmail());
					}
					
					emaildetails.setFromEmailid(compEntity.get(i).getEmail());
					emaildetails.setToEmailId(toEmailId);
					emaildetails.setCcEmailId(ccEmailId);
					
					EmailTemplate emailTemplate = ofy().load().type(EmailTemplate.class).filter("companyId", contractRenewalslist.get(m).getCompanyId())
							.filter("templateName", AppConstants.CONTRACTRENEWALREMINDER)	
							.filter("templateStatus", true).first().now();
					if(emailTemplate!=null){
						String emailbody = emailTemplate.getEmailBody();
						String resultString = emailbody.replaceAll("[\n]", "<br>");
						String resultString1 = resultString.replaceAll("[\\s]", "&nbsp;");
						
						String updatedemailbody= resultString1;
						updatedemailbody = getUpdateContractRenewalFinalReminderEmailBody(updatedemailbody,customer,contractRenewalslist.get(m),diffDays,StringContractEndDate);
						
						
						
						String emailSubject = emailTemplate.getSubject();
						if(emailSubject.contains("{Contract_Renewal_Days_Left}")){
							String updatedemailSubject = emailSubject.replace("{Contract_Renewal_Days_Left}", diffDays+"");
							emaildetails.setSubject(updatedemailSubject);
						}
						else{
							emaildetails.setSubject(emailTemplate.getSubject());
						}
						emaildetails.setPdfAttachment(true);
						emaildetails.setRenewalEmailReminder(true);
						emaildetails.setModel(contractRenewalslist.get(m));
						emaildetails.setEntityName("Contract");
						logger.log(Level.SEVERE,"Sending email renewal to client email body "+emaildetails.getEmailBody());
						Email email = new Email();
						String emailresponse = email.sendEmail(emaildetails, compEntity.get(i).getCompanyId());
						logger.log(Level.SEVERE,"emailresponse "+emailresponse);

//						sendEmail
					}

					
				}
				
				logger.log(Level.SEVERE,"TODAY DATE"+today);
				Date minusSevenDateWithZero = getsevenDaysminusDateTimeZero(today);
				logger.log(Level.SEVERE,"yesterday"+yesterday);
				logger.log(Level.SEVERE,"minusSevenDateWithZero"+minusSevenDateWithZero);

				List<Contract> contractEntity = ofy().load().type(Contract.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("endDate >=", minusSevenDateWithZero).filter("endDate <=", yesterday).filter("status","Approved").filter("renewFlag", false).filter("customerInterestFlag", false).list();
				logger.log(Level.SEVERE,"renewal overdue renewal EMAIL to client Contractentity size"+contractEntity.size());
				

				
				for(int p=0;p<contractEntity.size();p++){
					
					Customer customer = ofy().load().type(Customer.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("count", contractEntity.get(p).getCustomerId()).first().now();

					String StringContractEndDate = dateFormat.format(contractEntity.get(p).getEndDate());

					Date d1 = null;
	   				Date d2 = null;
	   				
	   				d1 = df.parse(StringContractEndDate);
	   				d2 = df.parse(todayDateString);
	   				long diff = d2.getTime() - d1.getTime();
					long diffDays = Math.abs(diff / (24 * 60 * 60 * 1000));
					
					EmailDetails emaildetails = new EmailDetails();
					ArrayList<String> toEmailId = new ArrayList<String>();
					toEmailId.add(customer.getEmail());
					
					Branch branchEntity = ofy().load().type(Branch.class).filter("companyId", contractEntity.get(p).getCompanyId())
							.filter("buisnessUnitName", contractEntity.get(p).getBranch()).filter("status", true).first().now();
					ArrayList<String> ccEmailId = new ArrayList<String>();
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", contractRenewalslist.get(p).getCompanyId()) && branchEntity!=null && branchEntity.getEmail()!=null && !branchEntity.getEmail().equals("")){
						ccEmailId.add(branchEntity.getEmail());
					}
					else{
						if(compEntity.get(i).getEmail()!=null)
						ccEmailId.add(compEntity.get(i).getEmail());
					}
					emaildetails.setFromEmailid(compEntity.get(i).getEmail());
					emaildetails.setToEmailId(toEmailId);
					emaildetails.setCcEmailId(ccEmailId);
					
					EmailTemplate emailTemplate = ofy().load().type(EmailTemplate.class).filter("companyId", contractEntity.get(p).getCompanyId())
							.filter("templateName", AppConstants.CONTRACTRENEWALOVERDUEREMINDER)	
							.filter("templateStatus", true).first().now();
					if(emailTemplate!=null){
						String emailbody = emailTemplate.getEmailBody();
						String resultString = emailbody.replaceAll("[\n]", "<br>");
						String resultString1 = resultString.replaceAll("[\\s]", "&nbsp;");
						if(resultString1.contains("{Contract_Renewal_Days_Left}")){
							String updatedemailbody = resultString1.replace("{Contract_Renewal_Days_Left}", diffDays+"");
							emaildetails.setEmailBody(updatedemailbody);
						}
						else{
							emaildetails.setEmailBody(resultString1);
						}
						String emailSubject = emailTemplate.getSubject();
						if(emailSubject.contains("{Contract_Renewal_Days_Left}")){
							String updatedemailSubject = emailSubject.replace("{Contract_Renewal_Days_Left}", diffDays+"");
							emaildetails.setSubject(updatedemailSubject);
						}
						else{
							emaildetails.setSubject(emailTemplate.getSubject());
						}
						emaildetails.setPdfAttachment(true);
						emaildetails.setRenewalEmailReminder(true);
						emaildetails.setModel(contractEntity.get(p));
						emaildetails.setEntityName("Contract");
						logger.log(Level.SEVERE,"Sending email ovedue renewal to client email body "+emaildetails.getEmailBody());
						Email email = new Email();
						String emailresponse = email.sendEmail(emaildetails, compEntity.get(i).getCompanyId());
						logger.log(Level.SEVERE,"emailresponse "+emailresponse);
					}
					
					
					}
				}
				else{					//else block for prosconfig
					logger.log(Level.SEVERE,"ProcessConfiguration is null");
					System.out.println("ProcessConfiguration is null");
				}				
				
			
			
			}else{        //else block for pross!=null
				logger.log(Level.SEVERE,"Cron job status is Inactive");
				System.out.println("Cron job status is Inactive");
			}
		
			
							
		}	//end of for loop
		
	}
	else{			//else block for if(compEntity.size()>0)
		
		logger.log(Level.SEVERE,"No Company found from cron job completed");	
		System.out.println("No Company found from cron job completed");
	}


	 }catch(Exception e2){

	e2.printStackTrace();
	 }
		

			
		}
		
		
	
		private Date getYesterdayDateWithTime(Date today) {
			Calendar cal11=Calendar.getInstance();
			cal11.setTime(today);
			cal11.add(Calendar.DATE, -1);
			
			Date sevenDaysAddingToDate=null;
			
			try {
				sevenDaysAddingToDate=dateFormat.parse(dateFormat.format(cal11.getTime()));
				cal11.set(Calendar.HOUR_OF_DAY,23);
				cal11.set(Calendar.MINUTE,59);
				cal11.set(Calendar.SECOND,99);
				cal11.set(Calendar.MILLISECOND, 999);
				sevenDaysAddingToDate=cal11.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			return sevenDaysAddingToDate;
	}


		private Date getsevenDaysminusDateTimeZero(Date todaysDate) {

			Calendar cal11=Calendar.getInstance();
			cal11.setTime(todaysDate);
			cal11.add(Calendar.DATE, -7);
			
			Date oneDayAddingToDate=null;
			
			try {
				oneDayAddingToDate=dateFormat.parse(dateFormat.format(cal11.getTime()));
				oneDayAddingToDate=cal11.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			return oneDayAddingToDate;
	}

		private Date getDate(Date endDate) {

			Calendar cal11=Calendar.getInstance();
			cal11.setTime(endDate);
			cal11.add(Calendar.DATE, +1);
			
			Date oneDayAddingToDate=null;
			
			try {
				oneDayAddingToDate=dateFormat.parse(dateFormat.format(cal11.getTime()));
				cal11.set(Calendar.HOUR_OF_DAY,23);
				cal11.set(Calendar.MINUTE,59);
				cal11.set(Calendar.SECOND,59);
				cal11.set(Calendar.MILLISECOND,999);
				oneDayAddingToDate=cal11.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			return oneDayAddingToDate;
	}

		/*************************************** SMS Method ********************************************/

			
		private void sendSms(long companyId,String fullName, int count, Date endDate,	Long cellNumber, String businessUnitName,
									String templatemsgwithbraces, String templateName) 
		{

			System.out.println("Customer Name :"+fullName);
			System.out.println("EndDate"+endDate);
			System.out.println("cellNumber"+cellNumber);
			System.out.println("Company Name"+businessUnitName);
			System.out.println("Message:"+templatemsgwithbraces);
			
			


			String enddate = dateFormat.format(endDate);
			logger.log(Level.SEVERE,"Message"+templatemsgwithbraces);	
			
			
			/**
			 * Date 29 jun 2017 added by vijay for Eco friendly
			 */
			
			ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","SMS").filter("status",true).filter("companyId", companyId).first().now();
			ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId).filter("processName","SMS").first().now();				
				
			boolean prossconfigBhashSMSflag = false;
			if(processName!=null){
			if(processConfig!=null){
				if(processConfig.isConfigStatus()){
					for(int l=0;l<processConfig.getProcessList().size();l++){
						if(processConfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("SMS") && processConfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("BHASHSMSAPI") && processConfig.getProcessList().get(l).isStatus()==true){
							prossconfigBhashSMSflag=true;
						}
					}
			   }
			}
			}
			logger.log(Level.SEVERE,"process config  BHASH SMS Flag =="+prossconfigBhashSMSflag);
			
			if(prossconfigBhashSMSflag){
				fullName = getCustomerName(fullName,companyId);
			}
			/**
			 * ends here
			 */

			String customerName = templatemsgwithbraces.replace("{CustomerName}",fullName+"");
			String contractId = customerName.replace("{ContractId}", count+"");
			String conendDate = contractId.replace("{endDate}", enddate);
			conendDate = conendDate.replace("{EndDate}", enddate);
			String companyName = businessUnitName;
			String actualMsg = conendDate.replace("{companyName}", companyName);
			actualMsg = actualMsg.replace("{CompanyName}", companyName);
			System.out.println("Actual MSG"+actualMsg);
			String mobileNo = cellNumber +"";
			
			
			/************ New API Code From (Bhavana) Date 09 Feb 2017 *****************************/

	        logger.log(Level.SEVERE,"Hi vijay SMS Mobile No ===="+mobileNo);
	        logger.log(Level.SEVERE,"SMS ==== "+actualMsg);
	        
			SmsServiceImpl smsimpl = new SmsServiceImpl();
//			int value =  smsimpl.sendSmsToClient(actualMsg, cellNumber, companyId);
//			saveSmsHistory(companyId,count,actualMsg,fullName,cellNumber);
			
			/**
			 * @author Vijay Date :- 29-04-2022
			 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
			 * whats and sms based on communication channel configuration.
			 */
			smsimpl.sendMessage(AppConstants.CRONJOB,AppConstants.CRONJOB,templateName,companyId,actualMsg,cellNumber,false);
			logger.log(Level.SEVERE,"after sendMessage method");
		}

		private void saveSmsHistory(long companyId, int docId, String actualMsg, String fullName, long mobileNo) {
			SmsHistory smshistory = new SmsHistory();
			smshistory.setDocumentId(docId);
			smshistory.setDocumentType("Contract Renewal Due");
			smshistory.setSmsText(actualMsg);
			smshistory.setName(fullName);
			smshistory.setCellNo(mobileNo);
			smshistory.setUserId("Cron Job");
			smshistory.setCompanyId(companyId);
			
			logger.log(Level.SEVERE,"Data 22222222222222222");

			ofy().save().entity(smshistory).now();
			
			logger.log(Level.SEVERE,"Data Saved Successfully========");

			
		}
		
		
		/************************************** SMS Method End  *************************************/

		private String getFirstLetterUpperCase(String customerFullName) {
			
			String customerName="";
			String[] customerNameSpaceSpilt=customerFullName.split(" ");
			int count=0;
			for (String name : customerNameSpaceSpilt) 
			{
				String nameLowerCase=name.toLowerCase();
				if(count==0)
				{
					customerName=customerName+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);
				}
				else
				{
					customerName=customerName+" "+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);	
				}
				
				count=count+1;
				
			}
			return customerName;  
			
			}
		
		
		/**
		 * Date 29 jun 2017 added by vijay for gtetting customer correspondence Name or full name
		 * @param customerName
		 * @param companyId
		 * @return
		 */
		
		private String getCustomerName(String customerName, long companyId) {
			
			String custName;
			
			Customer customer = ofy().load().type(Customer.class).filter("companyId", companyId).filter("fullname", customerName).first().now();
			
			if(customer!=null){
				
				if(!customer.getCustPrintableName().equals("")){
					custName = customer.getCustPrintableName();
				}else{
					custName = customer.getFullname();
				}
				
			}else{
				custName = customerName;
			}
			
			
			return custName;
		}
		
		/**
		 * ends here
		 */
		
	public void ContractRnewDetails(String cronDetails){

			
			
			Email cronEmail = new Email();
			
			Date today=DateUtility.getDateWithTimeZone("IST", new Date());
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			
			Date todayDate=DateUtility.getDateWithTimeZone("IST", new Date());

			
			/**
			 *   Date which is  +30 days with time 00 00 000 
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding  Day"+today);
			Calendar cal4=Calendar.getInstance();
			cal4.setTime(today);
			cal4.add(Calendar.DATE, +30);
			
			Date plusthirtydayswithzero=null;
			
			try {
				plusthirtydayswithzero=dateFormat.parse(dateFormat.format(cal4.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date +30 Days Date"+plusthirtydayswithzero);
			
			
			/**
			 *   Date which is  -30 days with time 00 00 000
			 */
			
			Calendar cal=Calendar.getInstance();
			cal.setTime(today);
			cal.add(Calendar.DATE, -30);
			
			Date minusThirtyDaysDate=null;
			
			try {
				minusThirtyDaysDate=dateFormat.parse(dateFormat.format(cal.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date -30 Days Date"+minusThirtyDaysDate);
			System.out.println("Today Date -30 Days Date: "+minusThirtyDaysDate);
			
			
			
			/**
			 *   Date which is  +30 days  with time 23 59 59 999
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding One Day"+today);
			Calendar cal3=Calendar.getInstance();
			cal3.setTime(today);
			cal3.add(Calendar.DATE, +30);
			
			Date plusThirtyDaysDate=null;
			
			try {
				plusThirtyDaysDate=dateFormat.parse(dateFormat.format(cal3.getTime()));
				cal3.set(Calendar.HOUR_OF_DAY,23);
				cal3.set(Calendar.MINUTE,59);
				cal3.set(Calendar.SECOND,59);
				cal3.set(Calendar.MILLISECOND,999);
				plusThirtyDaysDate=cal3.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date +30 Days Date"+plusThirtyDaysDate);
			
			
			/*
			 * Date:4/08/2018
			 * Developer:Ashwini
			 * Des:Date which is  +60 days 
			 */
			
			logger.log (Level.SEVERE,"Date Before Adding One Day"+today);
			Calendar cal60=Calendar.getInstance();
			cal60.setTime(today);
			cal60.add(Calendar.DATE, +60);
			
			Date PlusSixtyDaysDate=null;
			
			try{
				PlusSixtyDaysDate=dateFormat.parse(dateFormat.format(cal3.getTime()));
				cal60.set(Calendar.HOUR_OF_DAY,23);
				cal60.set(Calendar.MINUTE,59);
				cal60.set(Calendar.SECOND,59);
				cal60.set(Calendar.MILLISECOND,999);
				PlusSixtyDaysDate=cal60.getTime();
			}catch(ParseException e){
				
				e.printStackTrace();
			}
			logger.log(Level.SEVERE,"Today Date +60 Days Date"+PlusSixtyDaysDate);
			
			/*
			 * End by Ashwini
			 */
			
			/**
			 * Date 18-03-2017
			 * added by vijay
			 * for send emails to customer 4 weeks before contract end date
			 */
			
			/**
			 *   Date which is  +28 days  with time 23 59 59 999
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding "+today);
			Calendar cal11=Calendar.getInstance();
			cal11.setTime(today);
			cal11.add(Calendar.DATE, +28);
			
			Date plusTwentyEightDaysDate=null;
			
			try {
				plusTwentyEightDaysDate=dateFormat.parse(dateFormat.format(cal11.getTime()));
				cal11.set(Calendar.HOUR_OF_DAY,23);
				cal11.set(Calendar.MINUTE,59);
				cal11.set(Calendar.SECOND,59);
				cal11.set(Calendar.MILLISECOND,999);
				plusTwentyEightDaysDate=cal11.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date +28 Days Date"+plusTwentyEightDaysDate);
			
			
			
			
			/**
			 *   Date which is  +28 days with time 00 00 000 
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding  Day"+today);
			Calendar cal2=Calendar.getInstance();
			cal2.setTime(today);
			cal2.add(Calendar.DATE, +28);
			
			Date plustwentyEightdayswithzero=null;
			
			try {
				plustwentyEightdayswithzero=dateFormat.parse(dateFormat.format(cal2.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date +28 Days Date"+plustwentyEightdayswithzero);
			
			
			/**
			 *   Date which is  +21 days with time 00 00 000 
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding  Day"+today);
			Calendar cal10=Calendar.getInstance();
			cal10.setTime(today);
			cal10.add(Calendar.DATE, +21);
			
			Date plustwentyOnedayswithzero=null;
			
			try {
				plustwentyOnedayswithzero=dateFormat.parse(dateFormat.format(cal10.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date +21 Days Date"+plustwentyOnedayswithzero);
			
			
			/**
			 *   Date which is  +21 days  with time 23 59 59 999
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding "+today);
			Calendar cal5=Calendar.getInstance();
			cal5.setTime(today);
			cal5.add(Calendar.DATE, +21);
			
			Date plusTwentyOneDaysDate=null;
			
			try {
				plusTwentyOneDaysDate=dateFormat.parse(dateFormat.format(cal5.getTime()));
				cal5.set(Calendar.HOUR_OF_DAY,23);
				cal5.set(Calendar.MINUTE,59);
				cal5.set(Calendar.SECOND,59);
				cal5.set(Calendar.MILLISECOND,999);
				plusTwentyOneDaysDate=cal5.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date +21 Days Date"+plusTwentyOneDaysDate);
			
			
			
			/**
			 *   Date which is  +14 days with time 00 00 000 
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding  Day"+today);
			Calendar cal6=Calendar.getInstance();
			cal6.setTime(today);
			cal6.add(Calendar.DATE, +14);
			
			Date plusfourteendayswithzero=null;
			
			try {
				plusfourteendayswithzero=dateFormat.parse(dateFormat.format(cal6.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date +14 Days Date"+plusfourteendayswithzero);
			System.out.println("Today Date +14 Days Date: "+plusfourteendayswithzero);
			
			/**
			 *   Date which is  +14 days  with time 23 59 59 999
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding One Day"+today);
			Calendar cal7=Calendar.getInstance();
			cal7.setTime(today);
			cal7.add(Calendar.DATE, +14);
			
			Date plusfourteenDaysDate=null;
			
			try {
				plusfourteenDaysDate=dateFormat.parse(dateFormat.format(cal7.getTime()));
				cal7.set(Calendar.HOUR_OF_DAY,23);
				cal7.set(Calendar.MINUTE,59);
				cal7.set(Calendar.SECOND,59);
				cal7.set(Calendar.MILLISECOND,999);
				plusfourteenDaysDate=cal7.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date +14 Days Date"+plusfourteenDaysDate);
			
			
			
			/**
			 *   Date which is  +7 days with time 00 00 000 
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding  Day"+today);
			Calendar cal8=Calendar.getInstance();
			cal8.setTime(today);
			cal8.add(Calendar.DATE, +7);
			
			Date plussevendayswithzero=null;
			
			try {
				plussevendayswithzero=dateFormat.parse(dateFormat.format(cal8.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date +7 Days Date"+plussevendayswithzero);
			
			/**
			 *   Date which is  +7 days  with time 23 59 59 999
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding One Day"+today);
			Calendar cal9=Calendar.getInstance();
			cal9.setTime(today);
			cal9.add(Calendar.DATE, +7);
			
			Date plussevenDaysDate=null;
			
			try {
				plussevenDaysDate=dateFormat.parse(dateFormat.format(cal9.getTime()));
				cal9.set(Calendar.HOUR_OF_DAY,23);
				cal9.set(Calendar.MINUTE,59);
				cal9.set(Calendar.SECOND,59);
				cal9.set(Calendar.MILLISECOND,999);
				plussevenDaysDate=cal9.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date +7 Days Date"+plussevenDaysDate);
			
			/**
			 * Ends here
			 */
			
			
			logger.log(Level.SEVERE,"Date Before Adding One Day"+today);
			Calendar cal12=Calendar.getInstance();
			cal12.setTime(today);
			cal12.add(Calendar.DATE, -1);
			
			Date yesterday=null;
			
			try {
				yesterday=dateFormat.parse(dateFormat.format(cal12.getTime()));
				cal12.set(Calendar.HOUR_OF_DAY,23);
				cal12.set(Calendar.MINUTE,59);
				cal12.set(Calendar.SECOND,59);
				cal12.set(Calendar.MILLISECOND,999);
				yesterday=cal12.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE,"yesterday"+yesterday);

			
	 try{
		 
		 logger.log(Level.SEVERE,"In Contract List");	

		 
	/********************************Adding status in the list ***********************/
		 
//		 ArrayList<String> obj = new ArrayList<String>();
//		 obj.add("Approved");

	/******************************Converting todayDate to String format*****************/
		 
		 Date todaysDate = new Date();
		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 String todayDateString = df.format(todaysDate);
		 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);

		 
	/********************************Adding Companies in the list ***********************/
		 
	List<Company> compEntity = ofy().load().type(Company.class).list();
	if(compEntity.size()>0){

		logger.log(Level.SEVERE,"If compEntity size > 0");
		logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
		

		for(int i=0;i<compEntity.size();i++){
			
			Company c=compEntity.get(i);
			logger.log(Level.SEVERE,"Company Name="+c);
			
			/**
			 * @author Vijay Date  :- 18-08-2022
			 * Des :- SMS and whats message will work as per communication type and template selected in cron job reminder settings
			 * below old code commented
			 */
//			/**************************** for SMS ***********************************/
//			
////			EmailTemplateConfiguration emailTemplateconfigurationEntity = ofy().load().type(EmailTemplateConfiguration.class)
////					.filter("moduleName", AppConstants.CRONJOB).filter("documentName", AppConstants.CRONJOB)
////					.filter("communicationChannel", AppConstants.SMS).filter("templateName", "ContractRenewalSMS").first().now();
////			if(emailTemplateconfigurationEntity==null){
//////				
////				SmsConfiguration smsconfig = ofy().load().type(SmsConfiguration.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("status",true).first().now();
////				
////				if(smsconfig!=null){
////					logger.log(Level.SEVERE,"in the smsconfig !=null for SMS========"+smsconfig);
////					
////					String accountsid = smsconfig.getAccountSID();
////					String authotoken = smsconfig.getAuthToken();
////					String fromnumber = smsconfig.getPassword();
//				
//				ProcessName smsprocessName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
//				ProcessConfiguration smsprocessconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
//					
//				boolean prossconfigSMSflag = false;
//				if(smsprocessconfig!=null){
//					if(smsprocessconfig.isConfigStatus()){
//						for(int l=0;l<smsprocessconfig.getProcessList().size();l++){
//							if(smsprocessconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && smsprocessconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("ContractRenewalSMS") && smsprocessconfig.getProcessList().get(l).isStatus()==true){
//								prossconfigSMSflag=true;
//							}
//						}
//				   }
//				}
//				logger.log(Level.SEVERE,"process config  SMS Flag =="+prossconfigSMSflag);
//				
////				/**
////				 * Date 16 jun 2017 added by vijay
////				 * below process config is used for calling BHASH SMS API 
////				 */
////				ProcessConfiguration smsprocessconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","SMS").first().now();				
////				
////				boolean bhashSMSAPIFlag = false;
////				if(smsprocessconfig!=null){
////					if(smsprocessconfig.isConfigStatus()){
////						for(int l=0;l<smsprocessconfig.getProcessList().size();l++){
////							if(smsprocessconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("SMS") && smsprocessconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("BhashSMSAPI") && smsprocessconfig.getProcessList().get(l).isStatus()==true){
////								bhashSMSAPIFlag=true;
////							}
////						}
////				   }
////				}
////				
////				logger.log(Level.SEVERE,"process config BHASH SMS API Flag =="+bhashSMSAPIFlag);
////				/**
////				 * ends here
////				 */
//				
//				if(smsprocessName!=null){
//					logger.log(Level.SEVERE,"in the processName !=null for SMS========");
//					
//					if(prossconfigSMSflag){
//						logger.log(Level.SEVERE,"in the process Configuration !=null");
//						
//						SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("event", "Contract Renewal Cron Job" ).filter("status",true).first().now();
//
//						logger.log(Level.SEVERE,"SMSEnitity========"+smsEntity);
//
//						int day = smsEntity.getDay();
//						
//						String templatemsgwithbraces = smsEntity.getMessage();
//						logger.log(Level.SEVERE,"Day ========"+day);
//
//						logger.log(Level.SEVERE,"Message with braces========"+templatemsgwithbraces);
//						
//						
//						/************************************ *****************************/
//						
//						Calendar enddatecal = Calendar.getInstance();
//						enddatecal.setTime(today);
//						enddatecal.add(Calendar.DATE,+day);
//						
//						Date dateforenddatefilter = null;
//						try {
//							dateforenddatefilter = dateFormat.parse(dateFormat.format(enddatecal.getTime()));
//						} catch (ParseException e) {
//								e.printStackTrace();
//						}
//						
//						logger.log(Level.SEVERE,"dateforenddatefilter1========:"+dateforenddatefilter);
//
//						
//						Calendar endatecal2 = Calendar.getInstance();
//						endatecal2.setTime(today);
//						endatecal2.add(Calendar.DATE, +day);
//						Date dateforenddatefilter2 = null;
//						
//						try {
//							
//							dateforenddatefilter2 = dateFormat.parse(dateFormat.format(endatecal2.getTime()));
//							endatecal2.set(Calendar.HOUR_OF_DAY,23);
//							endatecal2.set(Calendar.SECOND,59);
//							endatecal2.set(Calendar.MINUTE,59);
//							endatecal2.set(Calendar.MILLISECOND,999);
//							dateforenddatefilter2 = endatecal2.getTime();
//							
//						} catch (ParseException e) {
//							e.printStackTrace();
//						}
//						logger.log(Level.SEVERE,"dateforenddatefilter2========:"+dateforenddatefilter2);
//							
//							List<Contract> contractentity = ofy().load().type(Contract.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("endDate >=", dateforenddatefilter).filter("endDate <=", dateforenddatefilter2).filter("status","Approved").list();
//							logger.log(Level.SEVERE,"contractEntity:"+contractentity.size());
//						
//							for(int m=0; m<contractentity.size(); m++){
//								
//								sendSms(compEntity.get(i).getCompanyId(),contractentity.get(m).getCinfo().getFullName(),contractentity.get(m).getCount(),contractentity.get(m).getEndDate(),contractentity.get(m).getCinfo().getCellNumber(),compEntity.get(i).getBusinessUnitName(),templatemsgwithbraces,smsEntity.getEvent());
//								
//							}
//						
//					}
//				}
			
				/**
				 * ends here
				 */
				
//			  }
				
//			}
			
			 

			
	/*************************************** SMS End *******************************************************/		
	/**
	 * Date 18-03-2017
	 * added by vijay
	 * email sent to customer four times last four weeks as per contract end date
	 * 
	 */
			
			ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
			ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
			
			boolean prossconfigCustomerEmailWithoutProcessedflag = false;
			if(processconfig!=null){
				if(processconfig.isConfigStatus()){
					for(int l=0;l<processconfig.getProcessList().size();l++){
						if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("ContractRenewalWithoutProcessedCustomerEmail") && processconfig.getProcessList().get(l).isStatus()==true){
							prossconfigCustomerEmailWithoutProcessedflag=true;
						}
					}
			   }
			}
			logger.log(Level.SEVERE,"process config  without processed contract renewal Flag =="+prossconfigCustomerEmailWithoutProcessedflag);
			
			if(processName!=null){
				logger.log(Level.SEVERE,"In the prossName");
			if(prossconfigCustomerEmailWithoutProcessedflag){
				logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
				
	/********************************Reading ContractList from Contract entity for customer  ***********************/
				
				List<Contract> contractEntityforcustomerContractRenwal = ofy().load().type(Contract.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("endDate >=", plustwentyEightdayswithzero).filter("endDate <=", plusTwentyEightDaysDate).filter("status","Approved").filter("renewFlag", false).filter("customerInterestFlag", false).list();

				
				List<Contract> contractRenewalThirdWeek = ofy().load().type(Contract.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("endDate >=", plustwentyOnedayswithzero).filter("endDate <=", plusTwentyOneDaysDate).filter("status","Approved").filter("renewFlag", false).filter("customerInterestFlag", false).list();
				if(contractRenewalThirdWeek.size()!=0){
					contractEntityforcustomerContractRenwal.addAll(contractRenewalThirdWeek);
				}
				
				List<Contract> contractRenewalSecondWeek = ofy().load().type(Contract.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("endDate >=", plusfourteendayswithzero).filter("endDate <=", plusfourteenDaysDate).filter("status","Approved").filter("renewFlag", false).filter("customerInterestFlag", false).list();
				if(contractRenewalSecondWeek.size()!=0){
					contractEntityforcustomerContractRenwal.addAll(contractRenewalSecondWeek);
				}
				List<Contract> contractRenewalFirstWeek = ofy().load().type(Contract.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("endDate >=", plussevendayswithzero).filter("endDate <=", plussevenDaysDate).filter("status","Approved").filter("renewFlag", false).filter("customerInterestFlag", false).list();
				if(contractRenewalFirstWeek.size()!=0){
					contractEntityforcustomerContractRenwal.addAll(contractRenewalFirstWeek);
				}
				
				
				logger.log(Level.SEVERE, "contractEntityforcustomerRenewal Size:"+contractEntityforcustomerContractRenwal.size());

				for(int j=0;j<contractEntityforcustomerContractRenwal.size();j++){
					
					ArrayList<String> toEmailList1=new ArrayList<String>();
					toEmailList1.add(contractEntityforcustomerContractRenwal.get(j).getCinfo().getEmail()+"");
					
					logger.log(Level.SEVERE, "Contract id" +contractEntityforcustomerContractRenwal.get(j).getCount());
					
					Customer customer = ofy().load().type(Customer.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("count", contractEntityforcustomerContractRenwal.get(j).getCustomerId()).first().now();
					if(customer!=null)
					toEmailList1.add(customer.getEmail());
					String msgBody;
					if(customer.isCompany()){
						msgBody ="</br></br> Dear "+customer.getCompanyName().trim()+ ", <br><br> It has been our privilege to have served you over the past year and we truly appreciated & value our association with you. </br> we trust you have found our services exemplery & to your complete satifaction. Your current contract expires as below on <b>"+fmt.format(contractEntityforcustomerContractRenwal.get(j).getEndDate())+"</b> and we request you to renew the same on or before expiry date.<br><br>";

					}else{
						msgBody ="</br></br> Dear "+customer.getFullname().trim()+ ", <br><br> It has been our privilege to have served you over the past year and we truly appreciated & value our association with you. <br> we trust you have found our services exemplery & to your complete satifaction. Your current contract expires as below on <b>"+fmt.format(contractEntityforcustomerContractRenwal.get(j).getEndDate())+"</b> and we request you to renew the same on or before expiry date.<br><br>";

					}
					
					String mailSubject="Subject : Renewal of Contract Id "+contractEntityforcustomerContractRenwal.get(j).getCount()+" end on "+fmt.format(contractEntityforcustomerContractRenwal.get(j).getEndDate())+".";
					
					//Table header 1
					ArrayList<String> tbl1_header=new ArrayList<String>();
					tbl1_header.add("CODE");
					tbl1_header.add("NAME");
					tbl1_header.add("DURATION");
					tbl1_header.add("SERVICES");
					tbl1_header.add("PRICE");
					tbl1_header.add("TAX 1");
					tbl1_header.add("TAX 2");
					System.out.println("Tbl1 header Size :"+tbl1_header.size());

					List<SalesLineItem> productList=new ArrayList<SalesLineItem>();
					productList=contractEntityforcustomerContractRenwal.get(j).getItems();
					// Table 1
					ArrayList<String> tbl1=new ArrayList<String>();
					for(int k=0;k<productList.size();k++){
					tbl1.add(productList.get(k).getProductCode());
					tbl1.add(productList.get(k).getProductName());
					tbl1.add(productList.get(k).getDuration()+"");
					tbl1.add(productList.get(k).getNumberOfServices()+"");
					tbl1.add(productList.get(k).getPrice()+"");
					
					ServiceProduct serviceProduct = ofy().load().type(ServiceProduct.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("count", productList.get(k).getPrduct().getCount()).first().now();
					logger.log(Level.SEVERE,"Service product ==="+serviceProduct);		
					if(serviceProduct!=null){
						logger.log(Level.SEVERE,"Service product");
						tbl1.add(serviceProduct.getServiceTax()+"");
						tbl1.add(serviceProduct.getVatTax()+"");
					}
					
					}
					System.out.println("Tbl1 Size :"+tbl1.size());
					
					if(!contractEntityforcustomerContractRenwal.get(j).getCinfo().getEmail().equalsIgnoreCase("xyz@abc.com")){
						cronEmail.htmlformatsendEmail(toEmailList1, mailSubject, "Contract Renewal", c, msgBody, null, null, tbl1_header, tbl1,null,null);
						logger.log(Level.SEVERE,"After send mail method ");		

					}
				}
			 }
			
			}
			
	/**
	 * ends here	
	 */
	
	/**
	 * Date 18-03-2017
	 * added by vijay
	 * this email is sent to customer if we processed the contract in contract renewal screen
	 *  Email sent four time before contract end date last four week		
	 */
			boolean processedRenewalconfigflag = false;
			if(processconfig!=null){
				if(processconfig.isConfigStatus()){
					for(int l=0;l<processconfig.getProcessList().size();l++){
						if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("ProcessedContractRenewalEmail") && processconfig.getProcessList().get(l).isStatus()==true){
							processedRenewalconfigflag=true;
						}
					}
			   }
			}
			logger.log(Level.SEVERE,"With Contract Renewal processed config Flag =="+processedRenewalconfigflag);
			
			if(processName!=null){
				logger.log(Level.SEVERE,"In the prossName");
			if(processedRenewalconfigflag){
							
				logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
				
			logger.log(Level.SEVERE, "Before filter contractEntityforcustomer" );
			List<ContractRenewal> contractRenewalEntity = ofy().load().type(ContractRenewal.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("contractEndDate >=", plustwentyEightdayswithzero).filter("contractEndDate <=", plusTwentyEightDaysDate).filter("status","Processed").list();

			
			List<ContractRenewal> contractRenewalThirdWeek = ofy().load().type(ContractRenewal.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("contractEndDate >=", plustwentyOnedayswithzero).filter("contractEndDate <=", plusTwentyOneDaysDate).filter("status","Processed").list();
			if(contractRenewalThirdWeek.size()!=0){
				contractRenewalEntity.addAll(contractRenewalThirdWeek);
			}
			
			List<ContractRenewal> contractRenewalSecondWeek = ofy().load().type(ContractRenewal.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("contractEndDate >=", plusfourteendayswithzero).filter("contractEndDate <=", plusfourteenDaysDate).filter("status","Processed").list();
			if(contractRenewalSecondWeek.size()!=0){
				contractRenewalEntity.addAll(contractRenewalSecondWeek);
			}
			List<ContractRenewal> contractRenewalFirstWeek = ofy().load().type(ContractRenewal.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("contractEndDate >=", plussevendayswithzero).filter("contractEndDate <=", plussevenDaysDate).filter("status","Processed").list();
			if(contractRenewalFirstWeek.size()!=0){
				contractRenewalEntity.addAll(contractRenewalFirstWeek);
			}
			logger.log(Level.SEVERE, "contractRenewalEntity for customer Size:"+contractRenewalEntity.size());

			System.out.println(contractRenewalEntity.size());
			for(int j=0;j<contractRenewalEntity.size();j++){
				ArrayList<String> toEmailList1=new ArrayList<String>();
				
				Customer customer = ofy().load().type(Customer.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("count", contractRenewalEntity.get(j).getCustomerId()).first().now();
				if(customer!=null)
				toEmailList1.add(customer.getEmail());
				String msgBody;
				if(customer.isCompany()){
					msgBody ="</br></br> Dear "+customer.getCompanyName().trim()+ ", <br><br> It has been our privilege to have served you over the past year and we truly appreciated & value our association with you. <br> we trust you have found our services exemplery & to your complete satifaction. Your current contract expires as below on <b>"+fmt.format(contractRenewalEntity.get(j).getContractEndDate())+"</b> and we request you to renew the same on or before expiry date.<br><br>";

				}else{
					msgBody ="</br></br> Dear "+customer.getFullname().trim()+ ", <br><br> It has been our privilege to have served you over the past year and we truly appreciated & value our association with you. <br> we trust you have found our services exemplery & to your complete satifaction. Your current contract expires as below on <b>"+fmt.format(contractRenewalEntity.get(j).getContractEndDate())+"</b> and we request you to renew the same on or before expiry date.<br><br>";

				}
				
				String mailSubject="Subject : Renewal of Contract Id "+contractRenewalEntity.get(j).getContractId()+" end on "+fmt.format(contractRenewalEntity.get(j).getContractEndDate())+".";
				
				//Table header 1
				ArrayList<String> tbl1_header=new ArrayList<String>();
				tbl1_header.add("CODE");
				tbl1_header.add("NAME");
				tbl1_header.add("DURATION");
				tbl1_header.add("SERVICES");
				tbl1_header.add("PRICE");
				tbl1_header.add("TAX 1");
				tbl1_header.add("TAX 2");
				System.out.println("Tbl1 header Size :"+tbl1_header.size());

				List<SalesLineItem> productList=new ArrayList<SalesLineItem>();
				productList=contractRenewalEntity.get(j).getItems();
				// Table 1
				ArrayList<String> tbl1=new ArrayList<String>();
				for(int k=0;k<productList.size();k++){
				tbl1.add(productList.get(k).getProductCode());
				tbl1.add(productList.get(k).getProductName());
				tbl1.add(productList.get(k).getDuration()+"");
				tbl1.add(productList.get(k).getNumberOfServices()+"");
				tbl1.add(productList.get(k).getPrice()+"");
				tbl1.add(productList.get(k).getServiceTax()+"");
				tbl1.add(productList.get(k).getVatTax()+"");
				}
				System.out.println("Tbl1 Size :"+tbl1.size());
				
				if(!customer.getEmail().equalsIgnoreCase("xyz@abc.com")){
					cronEmail.htmlformatsendEmail(toEmailList1, mailSubject, "Contract Renewal", c, msgBody, null, null, tbl1_header, tbl1,null,null);

				}
			}
			logger.log(Level.SEVERE, "After filter 9" );
			}
			
			}
	/**
	 * ends here
	 */
			
		
	/**
	 * Date 21 April 2017 added by vijay for contract renewal remainder before 30 days daily to EVA CLIENTS RENEWAL
	 * 		
	 */
			ProcessName processNameEVA = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
			ProcessConfiguration processConfigEVA = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
			
			boolean renewalsRemainders = false;
			if(processConfigEVA!=null){
				if(processConfigEVA.isConfigStatus()){
					for(int l=0;l<processConfigEVA.getProcessList().size();l++){
						if(processConfigEVA.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processConfigEVA.getProcessList().get(l).getProcessType().equalsIgnoreCase("EVALicenseReminder") && processConfigEVA.getProcessList().get(l).isStatus()==true){
							renewalsRemainders=true;
						}
					}
			   }
			}
			logger.log(Level.SEVERE,"renewalsRemainders"+renewalsRemainders);
			
			if(processNameEVA!=null){
				logger.log(Level.SEVERE,"In the prossName");
			if(renewalsRemainders){
				logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
				
				List<Contract> contractRenewalslist = ofy().load().type(Contract.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("endDate >=", todayDate).filter("endDate <=", plusThirtyDaysDate).filter("status","Approved").filter("renewFlag", false).filter("customerInterestFlag", false).list();
				logger.log(Level.SEVERE,"contractRenewalslist size=="+contractRenewalslist.size());

				for(int m =0;m<contractRenewalslist.size();m++){
					
					ArrayList<String> toEmailList1=new ArrayList<String>();
					
					Customer customer = ofy().load().type(Customer.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("count", contractRenewalslist.get(m).getCustomerId()).first().now();
					if(customer!=null)
					toEmailList1.add(customer.getEmail());
					String customerName = getFirstLetterUpperCase(customer.getFullname());
					
					String msgBody;
						msgBody ="</br></br> Dear "+customerName+ ", <br><br> Your EVA ERP License is expiring on "+fmt.format(contractRenewalslist.get(m).getEndDate())+". We request you to make your payment on <br> or before "+fmt.format(contractRenewalslist.get(m).getEndDate())+" to get the uninterrupted services. <br><br>";

					 String StringContractEndDate = dateFormat.format(contractRenewalslist.get(m).getEndDate());
			   	 	    
		   				Date d1 = null;
		   				Date d2 = null;
		   				
		   				d1 = df.parse(StringContractEndDate);
		   				d2 = df.parse(todayDateString);
		   				long diff = d2.getTime() - d1.getTime();
						long diffDays = Math.abs(diff / (24 * 60 * 60 * 1000));
						
					String mailSubject="EVA ERP License Reminder Days Left "+diffDays;
					
					int contractId = contractRenewalslist.get(m).getCount();
					String referenceNumber = contractRenewalslist.get(m).getRefNo();
					String title="";
					String clientName = customer.getCompanyName();
					if(referenceNumber.equals("")){
						title="EVA ERP License Reminder - Days Left "+diffDays+" Client Name - "+clientName+" Contract Id - "+contractId;

					}else{
						
						title="EVA ERP License Reminder - Days Left "+diffDays+" Client Name - "+clientName+" Contract Id - "+contractId+" Reference Number - "+referenceNumber;

					}
					
					//Table header 1
					ArrayList<String> tbl1_header=new ArrayList<String>();
					tbl1_header.add("CODE");
					tbl1_header.add("NAME");
					tbl1_header.add("DURATION");
					tbl1_header.add("PRICE");
					tbl1_header.add("TAX 1");
					tbl1_header.add("TAX 2");
					tbl1_header.add("Total");
					System.out.println("Tbl1 header Size :"+tbl1_header.size());

					List<SalesLineItem> productList=new ArrayList<SalesLineItem>();
					productList=contractRenewalslist.get(m).getItems();
					// Table 1
					ArrayList<String> tbl1=new ArrayList<String>();
					
					double netpayable=0;
					
					for(int k=0;k<productList.size();k++){
					tbl1.add(productList.get(k).getProductCode());
					tbl1.add(productList.get(k).getProductName());
					tbl1.add(productList.get(k).getDuration()+"");
					
					
					ServiceProduct serviceProduct = ofy().load().type(ServiceProduct.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("count", productList.get(k).getPrduct().getCount()).first().now();
					logger.log(Level.SEVERE,"Service product ==="+serviceProduct);		
					if(serviceProduct!=null){
						logger.log(Level.SEVERE,"Service product");
						tbl1.add(serviceProduct.getPrice()+"");
						tbl1.add(serviceProduct.getServiceTax()+"");
						tbl1.add(serviceProduct.getVatTax()+"");
					
					double vatTaxAmt = serviceProduct.getPrice()*serviceProduct.getVatTax().getPercentage()/100;
//					double productpricetotal = serviceProduct.getPrice()+vatTaxAmt;
					double serviceTax = serviceProduct.getPrice()*serviceProduct.getServiceTax().getPercentage()/100;
					double totalAmt = serviceProduct.getPrice()+vatTaxAmt+serviceTax;
					tbl1.add(Math.round(totalAmt)+"");
					netpayable += totalAmt;
					System.out.println("Net payable ="+netpayable);
					}
					
					if(k==productList.size()-1){
						k++;
						tbl1.add("Net Payable");
						tbl1.add(Math.round(netpayable)+"");
						k++;
						String netpayableamtInwords = SalesInvoicePdf.convert(netpayable);
						tbl1.add(netpayableamtInwords);
					}

					}
					
					String paymentTableTitle = "Please find below payment options" ;
					String paymentTableTitle2 = "Payment Modes";
					ArrayList<String> tbl2_header=new ArrayList<String>();
					tbl2_header.add("Modes");
					tbl2_header.add("Favoring");
					tbl2_header.add("Details");
					
					Date endDatePlusOneDay = getDate(contractRenewalslist.get(m).getEndDate());
					
					String msgBodyTwo = "As this is automated process & your login to the EVA ERP will be disabled on "+fmt.format(endDatePlusOneDay)+" </br> in case payment is not received by "
					+fmt.format(contractRenewalslist.get(m).getEndDate())+". Post this date data would be retained for next 7 days.</br> After 7 days of expiry your data will be deleted  automatically from the system by background job.</br> ";
							
					String msgbodyThree =  "In case you do not wish to renew the license, request you to revert back to us with the mail confirmation.</br></br>"
							+ " Also download all the data from each screen download option before  7 days of license expiry. </br></br></br>";
					
					String footermsg = "All other terms & conditions remains same as earlier.";
					
					if(!customer.getEmail().equalsIgnoreCase("xyz@abc.com")){
						cronEmail.EVARenewalEmail(toEmailList1, mailSubject, title, c, msgBody,null,null, tbl1_header, tbl1,paymentTableTitle, paymentTableTitle2, tbl2_header,  msgBodyTwo, msgbodyThree, null, footermsg, null);
					}
				}
				
				/**
				 * Date 22-04-2017
				 * added by vijay for after renewal expiry Email
				 */
				logger.log(Level.SEVERE,"TODAY DATE"+today);
//				Date yesterday = getYesterdayDateWithTime(today);
				Date minusSevenDateWithZero = getsevenDaysminusDateTimeZero(today);
				logger.log(Level.SEVERE,"yesterday"+yesterday);
				logger.log(Level.SEVERE,"minusSevenDateWithZero"+minusSevenDateWithZero);

				List<Contract> contractEntity = ofy().load().type(Contract.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("endDate >=", minusSevenDateWithZero).filter("endDate <=", yesterday).filter("status","Approved").filter("renewFlag", false).filter("customerInterestFlag", false).list();
				logger.log(Level.SEVERE,"After renewal EMAIL Contractentity size"+contractEntity.size());
				for(int p=0;p<contractEntity.size();p++){
					
					ArrayList<String> toEmailList1=new ArrayList<String>();
					
					Customer customer = ofy().load().type(Customer.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("count", contractEntity.get(p).getCustomerId()).first().now();
					if(customer!=null){
					toEmailList1.add(customer.getEmail());
					String customerName = getFirstLetterUpperCase(customer.getFullname());
					String msgBody;
						msgBody ="</br></br> Dear "+customerName+ ", <br><br> Your EVA ERP License has expired. After 7 days of license expiry your data will be deleted from<br> the system by background job <br><br>";
					 String msg2withRed ="As this is automated process & your login to the EVA ERP has been disabled on "+ dateFormat.format(todaysDate) +" as no payment is received."
								+ "<br><br> Data  would be retained for next 7 days. After 7 days of license expiry your data will be deleted automatically from the system by background job. We recommend you to download the data as soon as possible.<br><br> ";
					 String msg3 = "In case you would like to renew the license kindly make the payment as soon as possible. <br> Payment details as follows";

					 String StringContractEndDate = dateFormat.format(contractEntity.get(p).getEndDate());
						String clientName = customer.getCompanyName();
						int contractId = contractEntity.get(p).getCount();
						String referenceNumber = contractEntity.get(p).getRefNo();
						String title="";
						if(referenceNumber.equals("")){
							title="EVA ERP License Expired on "+StringContractEndDate+" Client Name - "+clientName+" Contract Id - "+contractId;

						}else{
							
							title="EVA ERP License Expired on "+StringContractEndDate+" Client Name - "+clientName+" Contract Id - "+contractId+" Reference Number - "+referenceNumber;

						}
						String mailSubject="EVA ERP License Expired on "+StringContractEndDate;
					
						if(!customer.getEmail().equalsIgnoreCase("xyz@abc.com")){
							cronEmail.EVARenewalEmail(toEmailList1, mailSubject, title, c, msgBody, msg2withRed,msg3, null, null,null, null, null,  null, null, null, null, null);
	
						}
					}
				}
				
				/**
				 * ends here
				 */
				
				
			}
			
			}
			
			
	/**
	 * Ends here		
	 */
			
			
	/********************************Checking prosname & prosconfig for each company ***********************/

			boolean ContractRenewalDailyEmailFlag = false;
			if(processconfig!=null){
				if(processconfig.isConfigStatus()){
					for(int l=0;l<processconfig.getProcessList().size();l++){
						if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("ContractRenewalDailyEmail") && processconfig.getProcessList().get(l).isStatus()==true){
							ContractRenewalDailyEmailFlag=true;
						}
					}
			   }
			}
			logger.log(Level.SEVERE,"process config Flag =="+ContractRenewalDailyEmailFlag);
			
			logger.log(Level.SEVERE,"after 3 filters for ProcessConfigurations");
		
			if(processName!=null){
				logger.log(Level.SEVERE,"In the prossName");
			if(ContractRenewalDailyEmailFlag){
							
				logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
				
				/********************************Reading ContractList from Contract entity  ***********************/
				
				logger.log(Level.SEVERE, "Before filter" );
				//List<Contract> contractEntity = ofy().load().type(Contract.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("endDate >=", minusThirtyDaysDate).filter("endDate <=", plusThirtyDaysDate).filter("status","Approved").filter("renewFlag", false).filter("customerInterestFlag", false).list();//commented by Ashwini
				logger.log(Level.SEVERE, "After filter 4" );
				
				/*
				 * Date:04/08/2018
				 * Developer:Ashwini
				 * Des:contract renewal reminder before 60 days to client renewal 
				 */
				List<Contract> contractEntity = ofy().load().type(Contract.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("endDate <=", PlusSixtyDaysDate).filter("status","Approved").filter("renewFlag", false).filter("customerInterestFlag", false).list();
			
				logger.log(Level.SEVERE, "Contract Entity Size:"+contractEntity.size());

				// email id is added to emailList
				ArrayList<String> toEmailList=new ArrayList<String>();
				toEmailList.add(compEntity.get(i).getPocEmail());
				
				System.out.println("POC EMIAL"+compEntity.get(i).getPocEmail());
				
				 String mailTitl = "Contract Renewal Due As On Date";
					logger.log(Level.SEVERE, "After filter 5" );

				if(contractEntity.size()>0){
					logger.log(Level.SEVERE, "After filter 6" );

				ArrayList<String> tbl_header = new ArrayList<String>();
				tbl_header.add("Serial No");
				tbl_header.add("Branch");
				tbl_header.add("Customer Id");
				tbl_header.add("Customer Name");
				tbl_header.add("Customer Cont No");
				tbl_header.add("POC Name");
				tbl_header.add("Email Id");
				tbl_header.add("Sales Person");
				tbl_header.add("Contract Id");
				tbl_header.add("Start Date");
				tbl_header.add("End Date");
				tbl_header.add("Status");
				tbl_header.add("Ageing");
				
				logger.log(Level.SEVERE, "After filter 7" );

	/********************************Sorting table with Branch & Contract End Date ***********************/
				logger.log(Level.SEVERE, "After filter 8" );

				
				Comparator<Contract> contractEndDateComparator2 = new Comparator<Contract>() {
					public int compare(Contract s1, Contract s2) {
					
					Date date1 = s1.getEndDate();
					Date date2 = s2.getEndDate();
					
					//ascending order
					return date1.compareTo(date2);
					}
					};
					Collections.sort(contractEntity, contractEndDateComparator2);
					
					Comparator<Contract> contractEndDateComparator = new Comparator<Contract>() {
						public int compare(Contract s1, Contract s2) {
						
						String branch1 = s1.getBranch();
						String branch2 = s2.getBranch();
						
						//ascending order
						return branch1.compareTo(branch2);
						}
						
						};
						Collections.sort(contractEntity, contractEndDateComparator);
					
	/********************************Getting ContractEntity data and adding in the tbl1 List ***********************/
						logger.log(Level.SEVERE, "After filter 9" );

				ArrayList<String> tbl1=new ArrayList<String>();
					
				for(int j=0;j<contractEntity.size();j++){
					tbl1.add((j+1)+"");
		   	 		tbl1.add(contractEntity.get(j).getBranch()+"");
		   	 		tbl1.add(contractEntity.get(j).getCinfo().getCount()+"");
		   	 		tbl1.add(contractEntity.get(j).getCinfo().getFullName()+"");		   	 		
		   	 		tbl1.add(contractEntity.get(j).getCinfo().getCellNumber()+"");
		   	 		tbl1.add(contractEntity.get(j).getCinfo().getPocName()+"");
		   	 		tbl1.add(contractEntity.get(j).getCinfo().getEmail());
		   	 		tbl1.add(contractEntity.get(j).getEmployee());
		   	 		tbl1.add(contractEntity.get(j).getCount()+"");
		   	 		tbl1.add(fmt.format(contractEntity.get(j).getStartDate())+"");
		   	 	    tbl1.add(fmt.format(contractEntity.get(j).getEndDate()) +"");
		 
		   	 	    /**************** for getting ageing for each Contract ******/
		   	 	    String StringContractEndDate = dateFormat.format(contractEntity.get(j).getEndDate());
		   	 	    
		   				Date d1 = null;
		   				Date d2 = null;
		   				
		   				d1 = df.parse(StringContractEndDate);
		   				d2 = df.parse(todayDateString);
		   				long diff = d2.getTime() - d1.getTime();
						long diffDays = diff / (24 * 60 * 60 * 1000);
						
//						System.out.println("Ageing:"+diffDays);
//				   	 	logger.log(Level.SEVERE,"Ageing:"+diffDays);
			
		   	 	    tbl1.add(contractEntity.get(j).getStatus()+"");
		   	 	    tbl1.add(diffDays +"");
		   	 	}			
				
				logger.log(Level.SEVERE,"In the ProsConfig !=null   3 ");
				
	/********************************Calling cronsendEmail Method ***********************/
				
				try {   
					   
						System.out.println("Before send mail method");
						logger.log(Level.SEVERE,"Before send method call to send mail  ");						
						cronEmail.cronSendEmail(toEmailList, "Contract Renewal Due As On Date"+" "+ todayDateString, mailTitl +"  "+ todayDateString, c, tbl_header, tbl1, null, null, null, null);
						logger.log(Level.SEVERE,"After send mail method ");		
								//+"  "+
				} catch (IOException e1) {
					
					logger.log(Level.SEVERE,"In the catch block ");
					e1.printStackTrace();
			}
				
			}
			else{						
					
				System.out.println("Sorry no ContractRenewal found for this company");
				logger.log(Level.SEVERE,"Sorry no ContractRenewal found for this company");
//				
				mailTitl = "No Contract Renewal Found Due";

				cronEmail.cronSendEmail(toEmailList, "Contract Renewal Due As On Date"+" "+ todayDateString, mailTitl , c, null, null, null, null, null, null);

			}
				
				
			}
			else{					//else block for prosconfig
					logger.log(Level.SEVERE,"ProcessConfiguration is null");
					System.out.println("ProcessConfiguration is null");
			}
			
			}else{        //else block for pross!=null
				logger.log(Level.SEVERE,"Cron job status is Inactive");
				System.out.println("Cron job status is Inactive");
			}
			
							
		}	//end of for loop
		
	}
	else{			//else block for if(compEntity.size()>0)
		
		logger.log(Level.SEVERE,"No Company found from cron job completed");	
		System.out.println("No Company found from cron job completed");
	}


	 }catch(Exception e2){

	e2.printStackTrace();
	 }
	}
	
	private String getUpdateContractRenewalFinalReminderEmailBody(String updatedemailbody,Customer customer, Contract contract, long diffDays, String stringContractEndDate) {

		if(updatedemailbody.contains("{Contract_Renewal_Days_Left}")){
			updatedemailbody = updatedemailbody.replace("{Contract_Renewal_Days_Left}", diffDays+"");
//			emaildetails.setEmailBody(updatedemailbody);
		}
		if(updatedemailbody.contains("<PoC First Name>")){
			updatedemailbody = updatedemailbody.replace("<PoC First Name>", customer.getFullname());
		}
		if(updatedemailbody.contains("<end date>") && stringContractEndDate!=null){
			updatedemailbody = updatedemailbody.replace("<end date>", stringContractEndDate);
		}
		
		
		return updatedemailbody;
	}

	
	
	    /**@author Sheetal
	     * @param employeeRole 
	     * @since 02-05-2022
	     * Des : contract renewal reminder to client cron job			 
	     * */
	
		public void contractRenewal(String cronList,HttpServletRequest req, HashSet<String> empRoleList) {
			
			   String updatedurlCalled = null;
			   String Appid = "";
				long companyId = 0;
				String[] splitURL2 = null;
			try {
				String urlCalled = req.getRequestURL().toString().trim();
				logger.log(Level.SEVERE, "urlCalled" + urlCalled);
				String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
				logger.log(Level.SEVERE, "splitUrl::" + splitUrl);
				
				Company comp = ofy().load().type(Company.class)
						.filter("accessUrl", splitUrl[0]).first().now();

			   splitURL2 = urlCalled.split("slick_erp/");
				logger.log(Level.SEVERE, "splitURL2::" + splitURL2[0]);

				 updatedurlCalled = splitURL2[0]+"slick_erp/";
				 logger.log(Level.SEVERE, "updated urlCalled" + updatedurlCalled);
				 
					if(urlCalled.contains("-dot-")){
						String [] urlarray = urlCalled.split("-dot-");
						 Appid = urlarray[1];
					}
					else{
						String [] urlarray = urlCalled.split("\\.");
						 Appid = urlarray[1];
					}
				logger.log(Level.SEVERE, "Appid " + Appid);
			   companyId = comp.getCompanyId();
				}catch(Exception e) {
					e.printStackTrace();
				}
			
			
			Date today=DateUtility.getDateWithTimeZone("IST", new Date());
			
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			fmtPaymentDate.setTimeZone(TimeZone.getTimeZone("IST"));
		
		try{
		 
		/******************************Converting todayDate to String format*****************/
		 
		 Date todaysDate = new Date();
		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 String todayDateString = df.format(todaysDate);
		 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);
		 
		/********************************Adding Companies in the list ***********************/
		 
		List<Company> compEntity = ofy().load().type(Company.class).list();
		if(compEntity.size()>0){

		logger.log(Level.SEVERE,"If compEntity size > 0");
		logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());

		for(int i=0;i<compEntity.size();i++){
			
			for(String empRoleName :empRoleList){
				
				if(!empRoleName.equalsIgnoreCase(AppConstants.CUSTOMER)){
					List<Employee>  employeeList=new ArrayList<Employee>();
					employeeList=ofy().load().type(Employee.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("roleName", empRoleName).filter("status", true).list();
					logger.log(Level.SEVERE,""+empRoleName+" "+"Employee List size::: "+employeeList.size());
					
						for (Employee employee : employeeList) {
								
								List<String> branchList=new ArrayList<String>();
								
								branchList.add(employee.getBranchName());
								for (int j = 0; j < employee.getEmpBranchList().size(); j++) {
									branchList.add(employee.getEmpBranchList().get(j).getBranchName());
									logger.log(Level.SEVERE, "  -- branch list --"+ employee.getEmpBranchList().get(j).getBranchName());
								}
								if(branchList.size()!=0){
									contractRenewalToclient(cronList,employee,branchList,compEntity.get(i),today,updatedurlCalled,splitURL2,Appid);
								}
						}
						
				}
				else{
					logger.log(Level.SEVERE,"for customer");
					contractRenewalToclient(cronList,null,null,compEntity.get(i),today,updatedurlCalled,splitURL2,Appid);
				}
			
			}
		
		}
		}
	  }catch(Exception e2){
			 e2.printStackTrace();
	  }
	}

		

		private void contractRenewalToclient(String cronList, Employee employee,
			List<String> branchList, Company company, Date today, String updatedurlCalled, String[] splitURL2, String Appid) {

			try {
				
			DigitalPaymentAPI digitalPaymentApi = new DigitalPaymentAPI();

			Email cronEmail = new Email();
			CompanyPayment compPayment=null;
			
			//Copied from another cron job
			Gson gson = new Gson();
			JSONArray jsonarr = null;
			try {
				jsonarr=new JSONArray(cronList.trim());
			} catch (JSONException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			
			CronJobConfigrationDetails cronDetails =null;
			for(int k=0;k<jsonarr.length();k++){
				logger.log(Level.SEVERE,"JSON ARRAY LENGTH :: "+jsonarr.length());
				JSONObject jsonObj = null;
				try {
					jsonObj = jsonarr.getJSONObject(k);
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				cronDetails = new CronJobConfigrationDetails();
				
				cronDetails = gson.fromJson(jsonObj.toString(), CronJobConfigrationDetails.class);
				
				Calendar cal=Calendar.getInstance();
				cal.setTime(today);
				
				int diffday = 0;
				if(cronDetails.isOverdueStatus() && cronDetails.getOverdueDays()>0){
//					cal.add(Calendar.DATE,cronDetails.getOverdueDays() * -1);
					diffday = cronDetails.getOverdueDays()*-1;
				}else{
					diffday = cronDetails.getInterval();
				 }
				cal.add(Calendar.DATE, diffday);
				logger.log(Level.SEVERE,"diffday1="+diffday+"cal="+cal.getTime());
				
				Date dateForFilter=null;
				
				try {
					dateForFilter=fmtPaymentDate.parse(fmtPaymentDate.format(cal.getTime()));
					cal.set(Calendar.HOUR_OF_DAY,23);
					cal.set(Calendar.MINUTE,59);
					cal.set(Calendar.SECOND,59);
					cal.set(Calendar.MILLISECOND,999);
					dateForFilter=cal.getTime();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				logger.log(Level.SEVERE,"Date After setting the Time"+dateForFilter);
				
				Calendar cal1=Calendar.getInstance();
				cal1.setTime(today);
				cal1.add(Calendar.DATE, 0);
				
				Date interdateForFilter=null;
				
				try {
					interdateForFilter=fmtPaymentDate.parse(fmtPaymentDate.format(cal1.getTime()));
					cal1.set(Calendar.HOUR_OF_DAY,0);
					cal1.set(Calendar.MINUTE,0);
					cal1.set(Calendar.SECOND,0);
					cal1.set(Calendar.MILLISECOND,0);
					interdateForFilter=cal1.getTime();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				logger.log(Level.SEVERE,"Date After setting the Time interdateForFilter"+interdateForFilter);
				
			//End of copy
			
			logger.log(Level.SEVERE,"In the for loop");
			
			/********************************Reading ContractList from Contract entity  ***********************/
			
			List<Contract> contractEntity = null;
			boolean OverduestatusFlag = false;
			
				if(branchList!=null && branchList.size()>0){
					try {
					if(cronDetails.isOverdueStatus()){
						logger.log(Level.SEVERE,"In isOverdueStatus()");
						contractEntity = ofy().load().type(Contract.class)
								.filter("companyId",company.getCompanyId()).filter("endDate >=",dateForFilter)
								.filter("endDate <=",interdateForFilter)
								.filter("status","Approved").filter("renewFlag", false).filter("customerInterestFlag", false)
								.filter("branch IN", branchList).list();
						OverduestatusFlag=true;
					}else{
						contractEntity = ofy().load().type(Contract.class)
								.filter("companyId",company.getCompanyId()).filter("endDate >=",interdateForFilter)
								.filter("endDate <=",dateForFilter).filter("status","Approved").filter("renewFlag", false)
								.filter("customerInterestFlag", false).filter("branch IN", branchList).list();
						OverduestatusFlag=false;
					}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				else{
					
					try {
						if(cronDetails.isOverdueStatus()){
							logger.log(Level.SEVERE,"In isOverdueStatus()");
							contractEntity = ofy().load().type(Contract.class)
									.filter("companyId",company.getCompanyId()).filter("endDate >=",dateForFilter)
									.filter("endDate <=",interdateForFilter)
									.filter("status","Approved").filter("renewFlag", false).filter("customerInterestFlag", false)
									.list();
							OverduestatusFlag=true;
						}else{
							contractEntity = ofy().load().type(Contract.class)
									.filter("companyId",company.getCompanyId()).filter("endDate >=",interdateForFilter)
									.filter("endDate <=",dateForFilter).filter("status","Approved").filter("renewFlag", false)
									.filter("customerInterestFlag", false).list();
							OverduestatusFlag=false;
						}
						}catch(Exception e){
							e.printStackTrace();
						}
					
				}
			
			
			
			logger.log(Level.SEVERE,"contractEntity.size()=="+contractEntity.size());

			logger.log(Level.SEVERE,"OverduestatusFlag=="+OverduestatusFlag);
			
			
			boolean communicationChannelEmailFlag = false;
			if(cronDetails.getCommunicationChannel()!=null && !cronDetails.getCommunicationChannel().equals("")
					&& (cronDetails.getCommunicationChannel().equals(AppConstants.SMS) || cronDetails.getCommunicationChannel().equals(AppConstants.WHATSAPP) )){
				
				long cellNumber=0;
				if(!cronDetails.getEmployeeRole().equalsIgnoreCase(AppConstants.CUSTOMER)){
					cellNumber = employee.getCellNumber1();
				}
				sendRenewalReminderMessage(contractEntity,cronDetails,company,cellNumber);
			}
			if(cronDetails.getCommunicationChannel()==null || cronDetails.getCommunicationChannel().equals("")){
				communicationChannelEmailFlag = true;
			}
			if(cronDetails.getCommunicationChannel()!=null && !cronDetails.getCommunicationChannel().equals("")
					&&  cronDetails.getCommunicationChannel().equals(AppConstants.EMAIL)){
				communicationChannelEmailFlag = true;
			}
			logger.log(Level.SEVERE,"communicationChannelEmailFlag "+communicationChannelEmailFlag);
			
			if(communicationChannelEmailFlag){
				
			for(int p=0;p<contractEntity.size();p++){
				
				Customer customer = ofy().load().type(Customer.class).filter("companyId",company.getCompanyId()).filter("count", contractEntity.get(p).getCinfo().getCount()).first().now();
				logger.log(Level.SEVERE,"Customer=="+customer+"Contract id="+contractEntity.get(p).getCount());
				ArrayList<String> toEmailList1=new ArrayList<String>();
//				if(customer!=null)
//				toEmailList1.add(customer.getEmail());
				
				if(!cronDetails.getEmployeeRole().equalsIgnoreCase(AppConstants.CUSTOMER)){
					toEmailList1.add(employee.getEmail());
				}
				else{
					toEmailList1.add(customer.getEmail());
				}
				logger.log(Level.SEVERE,"toEmailList1 "+toEmailList1);
				
				String msgBody;
//				EmailTemplate emailTemplate = new EmailTemplate();
		
				compPayment=ofy().load().type(CompanyPayment.class)
				            .filter("companyId", contractEntity.get(p).getCompanyId())
				            .filter("paymentDefault", true).first().now();
				
				if(OverduestatusFlag) {
//				emailTemplate = ofy().load().type(EmailTemplate.class).filter("companyId", compEntity.get(i).getCompanyId())
//						.filter("templateName", AppConstants.ContractRenewalOverdueEmail)	
//						.filter("templateStatus", true).first().now();
				EmailTemplate emailTemplate = null;
				if(cronDetails.getTemplateName()!=null && !cronDetails.getTemplateName().equals("")){
					emailTemplate = ofy().load().type(EmailTemplate.class).filter("companyId", company.getCompanyId())
							.filter("templateName", cronDetails.getTemplateName())	
							.filter("templateStatus", true).first().now();
				}
				else{
					emailTemplate = ofy().load().type(EmailTemplate.class).filter("companyId", company.getCompanyId())
							.filter("templateName", AppConstants.ContractRenewalOverdueEmail)	
							.filter("templateStatus", true).first().now();
				}
				logger.log(Level.SEVERE,"emailTemplate"+emailTemplate);
	
				if(emailTemplate!=null){					 
					logger.log(Level.SEVERE,"in emailTemplate Contract Renewal Overdue Reminder");
					String mailSubject = emailTemplate.getSubject();
					String emailbody = emailTemplate.getEmailBody();
					logger.log(Level.SEVERE,"Email body="+emailbody);
					String resultString = emailbody.replaceAll("[\n]", "<br>");
					String resultString1 = resultString.replaceAll("[\\s]", "&nbsp;");
					if(resultString1.contains("<Company>")) {
						resultString1=resultString1.replace("<Company>",company.getBusinessUnitName() );	
					}
					int contractid = contractEntity.get(p).getCount();
					String ContractId = Integer.toString(contractid);
				
					if(resultString1.contains("<Contract-id>")) {
						resultString1=resultString1.replace("<Contract-id>",ContractId);	
					}
					String contractEndDate=fmtPaymentDate.format(contractEntity.get(p).getEndDate());
					if(resultString1.contains("<contractEndDate>")) {
						resultString1=resultString1.replace("<contractEndDate>",contractEndDate);	
					}
					
					List<SalesLineItem> productList=new ArrayList<SalesLineItem>();
					productList=contractEntity.get(p).getItems();
					if(resultString1.contains("<NetPayable>")) {
						resultString1=resultString1.replace("<NetPayable>",contractEntity.get(p).getNetpayable()+"");	
					}

					 
					if(resultString1.contains("<ProductName>")) {
						String productsName = getproductNames(productList);
						resultString1=resultString1.replace("<ProductName>",productsName);	
//						resultString1=resultString1.replace("<ProductName>",productList.get(k).getProductName());	
					}
					
					
//					String name;
//				   if(customer.isCompany()){
//					    name = customer.getCompanyName().trim();
//				   }else {
//					    name = customer.getFullname().trim();
//				   }
					if(resultString1.contains("<Name>")) {
						resultString1=resultString1.replace("<Name>",contractEntity.get(p).getCinfo().getFullName());	
					}
					
					Branch branchEntity = ofy().load().type(Branch.class).filter("companyId", contractEntity.get(p).getCompanyId())
							.filter("buisnessUnitName", contractEntity.get(p).getBranch()).filter("status", true).first().now();
					String Email = null;
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", contractEntity.get(p).getCompanyId()) && branchEntity!=null && branchEntity.getEmail()!=null && !branchEntity.getEmail().equals("")){
						Email = branchEntity.getEmail();
					}
					else{
						if(company.getEmail()!=null)
						Email = company.getEmail();
					}
					if(resultString1.contains("<branch/company&nbsp;email&nbsp;id>")) {
						resultString1=resultString1.replace("<branch/company&nbsp;email&nbsp;id>",Email);	
					}
					String paynowlink =digitalPaymentApi.getLandingpageDetails(company, contractid,company.getCompanyId(),updatedurlCalled,splitURL2,Appid);
					String PaynowButton="<a href= "+paynowlink +">"+"<button class=btn > Pay Now </button></a>";
					  if(resultString1.contains("<PayNowlink>")) {
							resultString1=resultString1.replace("<PayNowlink>",PaynowButton);	
						}
					if(resultString1.contains("<Bank&nbsp;Details>")){
					
						if(compPayment!=null) {
							logger.log(Level.SEVERE,"in if(compPayment!=null)");
							String bankDetails = "<table border=0>";
							
							bankDetails+="<tr><td>Account Name</td><td>:" +compPayment.getPaymentComName()+"</td></tr>";
							bankDetails+="<tr><td>Account Number</td><td>:" +compPayment.getPaymentAccountNo()+"</td></tr>";
							bankDetails+="<tr><td>Bank Name</td><td>:" +compPayment.getPaymentBankName()+"</td></tr>";
							bankDetails+="<tr><td>Address</td><td>:" +compPayment.getAddressInfo().getCompleteAddress()+"</td></tr>";
							if(compPayment.getPaymentIFSCcode() != null&&!compPayment.getPaymentIFSCcode().equals(""))
								bankDetails+="<tr><td>IFSC/RTGS/NEFT Code</td><td>:" +compPayment.getPaymentIFSCcode()+"</td></tr>";
							if(compPayment.getPaymentMICRcode() != null&&!compPayment.getPaymentMICRcode().equals(""))
								bankDetails+="<tr><td>MICR Code</td><td>:" +compPayment.getPaymentMICRcode()+"</td></tr>";
							if(compPayment.getPaymentSWIFTcode() != null&&!compPayment.getPaymentSWIFTcode().equals(""))
								bankDetails+="<tr><td>Swift code</td><td>:" +compPayment.getPaymentSWIFTcode()+"</td></tr>";
							if(compPayment.getGooglePhonePayNo()!=0)
							bankDetails+="<tr><td>GPay and PhonePay</td><td>:"+compPayment.getGooglePhonePayNo()+"</td></tr>";						

							bankDetails += "</table>";
							resultString1=resultString1.replace("<Bank&nbsp;Details>", bankDetails);
							
						}
						else {
							resultString1=resultString1.replace("<bankdetails>", "");
						}						
					}
					
					if(resultString1.contains("<Company&nbsp;Signature>")) {
						if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", contractEntity.get(p).getCompanyId())){
							resultString1=resultString1.replace("<Company&nbsp;Signature>", ServerAppUtility.getCompanySignature(null,branchEntity));							
						}else
							resultString1=resultString1.replace("<Company&nbsp;Signature>", ServerAppUtility.getCompanySignature(company,null));
					}
					 
					msgBody=resultString1;
					StringBuilder builder = new StringBuilder();
		    		builder.append("<!DOCTYPE html>");
		    		builder.append("<html lang=\"en\">");
		    		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		    		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");
		    		
		    		builder.append("<br>");
		    		
		    		builder.append(msgBody);
		    		builder.append("<br>");
		    		builder.append("<br>");
		    		
		    		builder.append("</body>");
		    		builder.append("</html>");
		    		String contentInHtml = builder.toString();
		    		
		    		String attachFiles = "RenewalLetter";
					
		    		if(emailTemplate.isPdfattachment()){
						cronEmail.con = contractEntity.get(p);
			    		cronEmail.loadData();
			    		cronEmail.setContractRenewalNew(contractEntity.get(p));
			    		boolean companyAsLetterHead = false;
						if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "CompanyAsLetterHead", company.getCompanyId())){
								companyAsLetterHead=true;
						}
						logger.log(Level.SEVERE, "companyAsLetterHead value11 "+companyAsLetterHead);
						cronEmail.createContractRenewalNewAttachment(companyAsLetterHead);
						
					}

		    		logger.log(Level.SEVERE,"msgBody OverDue Email "+msgBody);
		    		if(msgBody!=null && !msgBody.equals("") && msgBody.length()>0){
		    			
		    			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, company.getCompanyId())){
							logger.log(Level.SEVERE,"in Send Grid call: company email --" +company.getEmail() +"  to email size " + toEmailList1.size());
							SendGridEmailServlet sdEmail=new SendGridEmailServlet();
							
							if(emailTemplate.isPdfattachment()&&Email!=null&&!Email.equals("")&&toEmailList1!=null&&toEmailList1.size()>0){
								sdEmail.sendMailWithSendGrid(Email,toEmailList1, null, null, mailSubject, contentInHtml, "text/html",cronEmail.pdfstream,attachFiles,"application/pdf",null);
							}
							else if(Email!=null&&!Email.equals("")&&toEmailList1!=null&&toEmailList1.size()>0){
								sdEmail.sendMailWithSendGrid(Email,toEmailList1, null, null, mailSubject, contentInHtml, "text/html",cronEmail.pdfstream,attachFiles,"application/pdf",null);
							}
						}else {
					
							try {
								cronEmail.PaymentReminderCronJobTemplateEmailToClient(toEmailList1, mailSubject, null, company, msgBody,attachFiles);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}			
							logger.log(Level.SEVERE,"Email sent using template");
						}
		    		}
		    	
				
				}
				}
			else {
					
//				EmailTemplate emailTemplateFinalRemainder = ofy().load().type(EmailTemplate.class).filter("companyId", compEntity.get(i).getCompanyId())
//						.filter("templateName", AppConstants.ContractRenewalFinalRemainder)	
//						.filter("templateStatus", true).first().now();
				
				EmailTemplate emailTemplateFinalRemainder = null;
				logger.log(Level.SEVERE,"cronDetails.getTemplateName() "+cronDetails.getTemplateName());

				if(cronDetails.getTemplateName()!=null && !cronDetails.getTemplateName().equals("")){
					emailTemplateFinalRemainder = ofy().load().type(EmailTemplate.class).filter("companyId", company.getCompanyId())
					.filter("templateName", cronDetails.getTemplateName())	
					.filter("templateStatus", true).first().now(); 
				}
				else{
					emailTemplateFinalRemainder = ofy().load().type(EmailTemplate.class).filter("companyId", company.getCompanyId())
							.filter("templateName", AppConstants.ContractRenewalFinalRemainder)	
							.filter("templateStatus", true).first().now();
				}
				
				logger.log(Level.SEVERE,"emailTemplateFinalRemainder "+emailTemplateFinalRemainder);

				if(emailTemplateFinalRemainder!=null) {
					
					logger.log(Level.SEVERE,"in emailTemplate Contract Renewal Final Reminder");
					String mailSubject = emailTemplateFinalRemainder.getSubject();
					String emailbody = emailTemplateFinalRemainder.getEmailBody();
					logger.log(Level.SEVERE,"Email body="+emailbody);
					String resultString = emailbody.replaceAll("[\n]", "<br>");
					String resultString1 = resultString.replaceAll("[\\s]", "&nbsp;");
					
					
					if(resultString1.contains("<Company>")) {
						resultString1=resultString1.replace("<Company>",company.getBusinessUnitName() );	
					}
					int contractid = contractEntity.get(p).getCount();
					String ContractId = Integer.toString(contractid);
				
					if(resultString1.contains("<Contract-id>")) {
						resultString1=resultString1.replace("<Contract-id>",ContractId);	
					}
					List<SalesLineItem> productList=new ArrayList<SalesLineItem>();
					productList=contractEntity.get(p).getItems();
					if(resultString1.contains("<NetPayable>")) {
						resultString1=resultString1.replace("<NetPayable>",contractEntity.get(p).getNetpayable()+"");	
					}
					 
					if(resultString1.contains("<ProductName>")) {
						String productsName = getproductNames(productList);
						resultString1=resultString1.replace("<ProductName>",productsName);	
					}
//					String name;
//				   if(customer.isCompany()){
//					    name = customer.getCompanyName().trim();
//				   }else {
//					    name = customer.getFullname().trim();
//				   }
					if(resultString1.contains("<Name>")) {
						resultString1=resultString1.replace("<Name>",contractEntity.get(p).getCinfo().getFullName());	
					}
					
					
					if(resultString1.contains("<PocFirstName>")) {
						resultString1=resultString1.replace("<PocFirstName>",contractEntity.get(p).getCinfo().getPocName());	
					}
					String contractEndDate=fmtPaymentDate.format(contractEntity.get(p).getEndDate());
					if(resultString1.contains("<contractEndDate>")) {
						resultString1=resultString1.replace("<contractEndDate>",contractEndDate);	
					}
					Branch branchEntity = ofy().load().type(Branch.class).filter("companyId", contractEntity.get(p).getCompanyId())
							.filter("buisnessUnitName", contractEntity.get(p).getBranch()).filter("status", true).first().now();
					String Email = null;
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", contractEntity.get(p).getCompanyId()) && branchEntity!=null && branchEntity.getEmail()!=null && !branchEntity.getEmail().equals("")){
						Email = branchEntity.getEmail();
					}
					else{
						if(company.getEmail()!=null)
						Email = company.getEmail();
					}
					if(resultString1.contains("<branch/company&nbsp;email&nbsp;id>")) {
						resultString1=resultString1.replace("<branch/company&nbsp;email&nbsp;id>",Email);	
					}
					String paynowlink =digitalPaymentApi.getLandingpageDetails(company, contractid,company.getCompanyId(),updatedurlCalled,splitURL2,Appid);
					String PaynowButton="<a href= "+paynowlink +">"+"<button class=btn > Pay Now </button></a>";
					  if(resultString1.contains("<PayNowlink>")) {
							resultString1=resultString1.replace("<PayNowlink>",PaynowButton);	
						}
					if(resultString1.contains("<Bank&nbsp;Details>")){
						logger.log(Level.SEVERE,"in if(resultString1.contains(<bankdetails>)");
						if(compPayment!=null) {
							logger.log(Level.SEVERE,"in if(compPayment!=null)");
							String bankDetails = "<table border=0>";
							
							bankDetails+="<tr><td>Account Name</td><td>:" +compPayment.getPaymentComName()+"</td></tr>";
							bankDetails+="<tr><td>Account Number</td><td>:" +compPayment.getPaymentAccountNo()+"</td></tr>";
							bankDetails+="<tr><td>Bank Name</td><td>:" +compPayment.getPaymentBankName()+"</td></tr>";
							bankDetails+="<tr><td>Address</td><td>:" +compPayment.getAddressInfo().getCompleteAddress()+"</td></tr>";
							if(compPayment.getPaymentIFSCcode() != null&&!compPayment.getPaymentIFSCcode().equals(""))
								bankDetails+="<tr><td>IFSC/RTGS/NEFT Code</td><td>:" +compPayment.getPaymentIFSCcode()+"</td></tr>";
							if(compPayment.getPaymentMICRcode() != null&&!compPayment.getPaymentMICRcode().equals(""))
								bankDetails+="<tr><td>MICR Code</td><td>:" +compPayment.getPaymentMICRcode()+"</td></tr>";
							if(compPayment.getPaymentSWIFTcode() != null&&!compPayment.getPaymentSWIFTcode().equals(""))
								bankDetails+="<tr><td>Swift code</td><td>:" +compPayment.getPaymentSWIFTcode()+"</td></tr>";
							if(compPayment.getGooglePhonePayNo()!=0)
								bankDetails+="<tr><td>GPay and PhonePay</td><td>:"+compPayment.getGooglePhonePayNo()+"</td></tr>";

							bankDetails += "</table>";
							resultString1=resultString1.replace("<Bank&nbsp;Details>", bankDetails);
							logger.log(Level.SEVERE,"result after bankdetails="+	resultString1);
						}
						else {
							resultString1=resultString1.replace("<bankdetails>", "");
						}						
					}
					if(resultString1.contains("<Company&nbsp;Signature>")) {
						if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", contractEntity.get(p).getCompanyId())){
							resultString1=resultString1.replace("<Company&nbsp;Signature>", ServerAppUtility.getCompanySignature(null,branchEntity));							
						}else
							resultString1=resultString1.replace("<Company&nbsp;Signature>", ServerAppUtility.getCompanySignature(company,null));
					}
					
					msgBody=resultString1;
					StringBuilder builder = new StringBuilder();
		    		
		    		builder.append("<!DOCTYPE html>");
		    		builder.append("<html lang=\"en\">");
		    		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		    		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");
		    		
		    		builder.append("<br>");
		    		
		    		builder.append(msgBody);
		    		builder.append("<br>");
		    		builder.append("<br>");
		    		
		    		builder.append("</body>");
		    		builder.append("</html>");
		    		String contentInHtml = builder.toString();
		    		
		    		

		    		String attachFiles = "RenewalLetter";
					
		    		if(emailTemplateFinalRemainder.isPdfattachment()){
						cronEmail.con = contractEntity.get(p);
			    		cronEmail.loadData();
			    		cronEmail.setContractRenewalNew(contractEntity.get(p));
			    		boolean companyAsLetterHead = false;
						if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "CompanyAsLetterHead", company.getCompanyId())){
								companyAsLetterHead=true;
						}
						logger.log(Level.SEVERE, "companyAsLetterHead value11 "+companyAsLetterHead);
						cronEmail.createContractRenewalNewAttachment(companyAsLetterHead);
						
					}

		    		logger.log(Level.SEVERE,"msgBody Due Email "+msgBody);
		    		if(msgBody!=null && !msgBody.equals("") && msgBody.length()>0){
		    			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, company.getCompanyId())){
							logger.log(Level.SEVERE,"in Send Grid call: company email --" +company.getEmail() +"  to email size " + toEmailList1.size());
							SendGridEmailServlet sdEmail=new SendGridEmailServlet();
//							sdEmail.sendMailWithSendGrid(company.getEmail(),toEmailList1, null, null, mailSubject, contentInHtml, "text/html",null,null,null,null);
				    		if(emailTemplateFinalRemainder.isPdfattachment()&&Email!=null&&!Email.equals("")&&toEmailList1!=null&&toEmailList1.size()>0){
								sdEmail.sendMailWithSendGrid(Email,toEmailList1, null, null, mailSubject, contentInHtml, "text/html",cronEmail.pdfstream,attachFiles,"application/pdf",null);
				    		}
				    		else if(Email!=null&&!Email.equals("")&&toEmailList1!=null&&toEmailList1.size()>0){
								sdEmail.sendMailWithSendGrid(Email,toEmailList1, null, null, mailSubject, contentInHtml, "text/html",null,null,null,null);
				    		}

						}else {
							
							try {
								cronEmail.PaymentReminderCronJobTemplateEmailToClient(toEmailList1, mailSubject, null, company, msgBody,attachFiles);
							} catch (IOException e) {
								e.printStackTrace();
							}			
						logger.log(Level.SEVERE,"Email sent using final template");
						}
		    		}
		    		
					
				}
				
				}
				}
			
			}
				
			}
			
			} catch (Exception e) {
				e.printStackTrace();
			}
		}


		
		
		private String getproductNames(List<SalesLineItem> productList) {
			String productName="";
			for(int i=0;i<productList.size();i++) {
				productName = productName + productList.get(i).getProductName();
				if(i==productList.size()-1) {
				}
				else {
					productName +=",";
				}
			}
			return productName;
		}

		private void sendRenewalReminderMessage(List<Contract> contractEntity,CronJobConfigrationDetails cronDetails, Company company,
				Long cellNumber) {
				
			SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId", company.getCompanyId()).filter("event", cronDetails.getTemplateName()).filter("status",true).first().now();
			if(smsEntity!=null){
				
				for(Contract contract : contractEntity){
					ServerAppUtility serverapputility = new ServerAppUtility();
					boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(contract.getCinfo().getCount(), contract.getCompanyId());
					if(!dndstatusFlag){
						
						
						String enddate = dateFormat.format(contract.getEndDate());
						logger.log(Level.SEVERE,"Message"+smsEntity.getMessage());	
						
						
						/**
						 * Date 29 jun 2017 added by vijay for Eco friendly
						 */
						
						ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","SMS").filter("status",true).filter("companyId", company.getCompanyId()).first().now();
						ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", company.getCompanyId()).filter("processName","SMS").first().now();				
							
						boolean prossconfigBhashSMSflag = false;
						if(processName!=null){
						if(processConfig!=null){
							if(processConfig.isConfigStatus()){
								for(int l=0;l<processConfig.getProcessList().size();l++){
									if(processConfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("SMS") && processConfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("BHASHSMSAPI") && processConfig.getProcessList().get(l).isStatus()==true){
										prossconfigBhashSMSflag=true;
									}
								}
						   }
						}
						}
						logger.log(Level.SEVERE,"process config  BHASH SMS Flag =="+prossconfigBhashSMSflag);
						String custName = contract.getCinfo().getFullName();
						if(prossconfigBhashSMSflag){
							custName = getCustomerName(contract.getCinfo().getFullName(),contract.getCompanyId());
						}
						/**
						 * ends here
						 */
						String templatemsgwithbraces = smsEntity.getMessage();
						String customerName = templatemsgwithbraces.replace("{CustomerName}",custName+"");
						String contractId = customerName.replace("{ContractId}", contract.getCount()+"");
						String conendDate = contractId.replace("{endDate}", enddate);
						conendDate = conendDate.replace("{EndDate}", enddate);
						String companyName = company.getBusinessUnitName();
						conendDate = conendDate.replace("{companyName}", companyName);
						conendDate = conendDate.replace("{CompanyName}", companyName);
						//Ashwini Patil Date:28-12-2023 as per Nitin sir's instruction adding renewal letter link in the whatsapp msg
						String tinyurl="";
						if(conendDate.contains("{ContractRenewalPdfLink}")){
							CommonServiceImpl commonserviceimpl = new CommonServiceImpl();
							String companyurl=commonserviceimpl.getCompleteURL(company);
							String pdflink = commonserviceimpl.getPDFURL(AppConstants.CONTRACT_RENEWAL, null, contract.getCount(), contract.getCompanyId(), company, companyurl,null);
							logger.log(Level.SEVERE, "ContractRenewal pdflink " + pdflink);
							tinyurl = ServerAppUtility.getTinyUrl(pdflink,company.getCompanyId());
						}

						String actualMsg = conendDate.replace("{ContractRenewalPdfLink}", tinyurl);
						
						System.out.println("Actual MSG"+actualMsg);

				        logger.log(Level.SEVERE,"Hi vijay SMS Mobile No ===="+cellNumber);
				        logger.log(Level.SEVERE,"SMS ==== "+actualMsg);

						if(cronDetails.getEmployeeRole().equalsIgnoreCase(AppConstants.CUSTOMER)){
							cellNumber = contract.getCinfo().getCellNumber();
						}
						logger.log(Level.SEVERE,"cellNumber  "+cellNumber);
						
						//Ashwini Patil Date:16-08-2024 ultra reported that renewal msgs are getting sent even if contract is marked as do not renew
						if(cellNumber!=0&&!contract.isCustomerInterestFlag()){
							SmsServiceImpl smsimpl = new SmsServiceImpl();
							logger.log(Level.SEVERE," cronDetails.getCommunicationChannel()  "+cronDetails.getCommunicationChannel());

							if(cronDetails.getCommunicationChannel().equals(AppConstants.SMS)){
								String smsResponse = smsimpl.sendSmsToClient(actualMsg, cellNumber+"", company.getCompanyId(), AppConstants.SMS);
								logger.log(Level.SEVERE,"for sending sms smsResponse"+smsResponse);
							}
							if(cronDetails.getCommunicationChannel().equals(AppConstants.WHATSAPP)){
								String response = smsimpl.sendMessageOnWhatsApp(company.getCompanyId(), cellNumber+"", actualMsg);
								logger.log(Level.SEVERE,"whats app response"+response);
							}
						}
						

					}
				}
				
				
			}
		}
		
}
