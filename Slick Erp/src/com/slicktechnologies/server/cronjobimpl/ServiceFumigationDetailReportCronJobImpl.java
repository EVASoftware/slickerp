package com.slicktechnologies.server.cronjobimpl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.UpdateServiceImpl;
import com.slicktechnologies.server.addhocdownload.XlsxWriter;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.CommodityFumigationDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

/**
 * Created By - nidhi Dated : 18-11-2017 Description :For sending emails via
 * cron job for service fumigation details
 */


public class ServiceFumigationDetailReportCronJobImpl extends HttpServlet {
	private static final long serialVersionUID = -38785486050163436L;
	Logger logger = Logger
			.getLogger("ServiceFumigationDetailReportCronJobImpl.class");
	long companyId = 0;
	Company company;
	public static List<CommodityFumigationDetails> commodityFumigation = new ArrayList<CommodityFumigationDetails>();
	public static List<String> branchList = new ArrayList<String>();
	public static TreeMap<String, String> proName = new TreeMap<String, String>();

	HttpServletResponse responseref;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");

	private SimpleDateFormat fmtServCreationDate = new SimpleDateFormat("dd/MM/yyyy");
	XSSFWorkbook workbook = new XSSFWorkbook();

	ByteArrayOutputStream outByteStream;
	boolean flag = false;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE, " daily fumigation datail cron job -- ");
		try {

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));

			Date d = DateUtility.getDateWithTimeZone("IST", new Date());

			Calendar cal = Calendar.getInstance();
			cal.setTime(d);
			cal.add(Calendar.DATE, -1);

			Date DaysDate = null;

			try {
				DaysDate = sdf.parse(sdf.format(cal.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}

			try {
				resp.setContentType("application/vnd.ms-excel");

				String reqtype = req.getParameter("type");
				responseref = resp;
				System.out.println("Type is " + reqtype);

				// int type=Integer.parseInt(reqtype);

				UpdateServiceImpl updateSer = new UpdateServiceImpl();

				// company=ofy().load().type(Company.class).filter("companyId",
				// Long.parseLong("5348024557502464")).first().now();
				// filter("accessUrl","my")
				company = ofy().load().type(Company.class)
						.filter("accessUrl", "my").first().now();
				companyId = company.getCompanyId();

				/**
						 */

				ProcessConfiguration process = ofy().load()
						.type(ProcessConfiguration.class)
						.filter("companyId", companyId)
						.filter("processName", "CronJob").first().now();

				for (int i = 0; i < process.getProcessList().size(); i++) {

					if (process.getProcessList().get(i).processType
							.equalsIgnoreCase("CommidutyFumigation")) {
						flag = true;
					}
				}
				// flag= true;
				if (flag) {
					updateSer.dailyFumigationReportDetails(companyId,
							new Date());
				}

				XlsxWriter xlsx = new XlsxWriter();
				Date d1 = new Date();
				String dateString = fmt.format(d1);

				xlsx.getFumigationReportDetails(workbook);

				outByteStream = new ByteArrayOutputStream();
				workbook.write(outByteStream);
				byte[] outArray = outByteStream.toByteArray();

				logger.log(Level.SEVERE, "outArray  -  size -- "
						+ outArray.length);

				// doPost(req, resp);
				// OutputStream out = resp.getOutputStream();
				// resp.getOutputStream();
				// out.write(outArray);
				// out.flush();
				// out.close();

			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e.getMessage());
			}

			{
				if (commodityFumigation.size() > 0) {

					List<CommodityFumigationDetails> commodityFumigationList = commodityFumigation;
					List<String> brnahcList = branchList;
					TreeMap<String, String> proName1 = proName;
					int srno = 0;

					TreeMap<String, Double> proTotal = new TreeMap<String, Double>();

					TreeMap<String, Double> prodayTotal = new TreeMap<String, Double>();

					for (String pro : proName1.keySet()) {
						proTotal.put(pro, 0.0);
						prodayTotal.put(pro, 0.0);
					}

					/**
					 * Email Parts ends here
					 */

					ArrayList<String> employeeRoleList = new ArrayList<String>();
//					employeeRoleList.add("Branch Manager");
//					employeeRoleList.add("Zonal Head");
//					employeeRoleList.add("Business Head");
					employeeRoleList.add("Admin");
//					employeeRoleList.add("Zonal Coordinator");
//					employeeRoleList.add("Regional Manager");
					ArrayList<String> toEmailList = new ArrayList<String>();

					for (String roleName : employeeRoleList) {
						List<Employee> employeeList = new ArrayList<Employee>();
						employeeList = ofy().load().type(Employee.class)
								.filter("companyId", companyId)
								.filter("roleName", roleName).list();
						logger.log(Level.SEVERE,
								"" + roleName + " " + "Employee List size::: "
										+ employeeList.size());

						for (Employee employee : employeeList) {

							toEmailList.add(employee.getEmail().trim());
						}
					}

					String[] attachFiles = new String[1];
					attachFiles[0] = "Fumigation_FIle";
					Email cronEmail = new Email();

					try {
						if (commodityFumigationList.size() != 0) {
							cronEmail.cronSendEmail(toEmailList,
									"Commiduty fumigation Daily Report As On Date"
											+ " " + DaysDate,
									"Commiduty fumigation Daily Report As On Date"
											+ "  " + DaysDate +" Please Find Attachment", company, null,
									null, null, null, null, attachFiles,
									outByteStream, "Fumigation_FIle","");
						}
					} catch (IOException e) {
						e.printStackTrace();
						logger.log(Level.SEVERE, "Error in cronEmail method:::"
								+ e);
					}

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			logger.log(Level.SEVERE,
					"fumigation detail cron job --  " + e.getLocalizedMessage());
		}
	}

	/**
	 *  nidhi
	 *  1-03-2018
	 *  cron job configration
	 * @param cronList
	 */

	public void getServiceFumigation(String cronList){
		
		
		

//		String strCronDetails = req.getParameter("cronList");
		logger.log(Level.SEVERE, "Cron list str --" + cronList);
		Gson gson = new Gson();
		JSONArray jsonarr = null;
		try {
			jsonarr=new JSONArray(cronList.trim());
		} catch (JSONException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		try {
			CronJobConfigrationDetails cronDetails =null;

			List<Company> compEntity = ofy().load().type(Company.class).list();
			if(compEntity.size()>0){
			
				logger.log(Level.SEVERE,"If compEntity size > 0");
				logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
				

				for(int i=0;i<compEntity.size();i++){
					
					company = compEntity.get(i);
					companyId = company.getCompanyId();

					
					for(int k=0;k<jsonarr.length();k++){
						JSONObject jsonObj = jsonarr.getJSONObject(k);
						cronDetails = new CronJobConfigrationDetails();
						
						cronDetails = gson.fromJson(jsonObj.toString(), CronJobConfigrationDetails.class);
						
						logger.log(Level.SEVERE, " daily fumigation datail cron job -- ");
						try {

							Date today=DateUtility.getDateWithTimeZone("IST", new Date());
							
							SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
							sdf.setTimeZone(TimeZone.getTimeZone("IST"));
							TimeZone.setDefault(TimeZone.getTimeZone("IST"));

							Date d = DateUtility.getDateWithTimeZone("IST", new Date());

							logger.log(Level.SEVERE,"Date Before Adding "+today);
							Calendar cal=Calendar.getInstance();
							cal.setTime(today);
							cal.add(Calendar.DATE, 0);
							
							Date dateForFilter=null;
							


							try {
								dateForFilter=fmtServCreationDate.parse(fmtServCreationDate.format(cal.getTime()));
								cal.set(Calendar.HOUR_OF_DAY,0);
								cal.set(Calendar.MINUTE,0);
								cal.set(Calendar.SECOND,0);
								cal.set(Calendar.MILLISECOND,0);
								dateForFilter=cal.getTime();
							} catch (ParseException e) {
								e.printStackTrace();
							}
							
							
							
							logger.log(Level.SEVERE,"Date After setting the Time"+dateForFilter);
							
							cal.setTime(today);
							int diffDay = 0;
							 if(cronDetails.getOverdueDays()>0 && cronDetails.isOverdueStatus()){
								 diffDay = -cronDetails.getOverdueDays();
							 }else{
								 diffDay = cronDetails.getInterval();
							 }
							
							cal.add(Calendar.DATE, diffDay);
							
							Date duedateForFilter=null;
							
							try {
								duedateForFilter=fmtServCreationDate.parse(fmtServCreationDate.format(cal.getTime()));
								cal.set(Calendar.HOUR_OF_DAY,23);
								cal.set(Calendar.MINUTE,59);
								cal.set(Calendar.SECOND,59);
								cal.set(Calendar.MILLISECOND,999);
								duedateForFilter=cal.getTime();
							} catch (ParseException e) {
								e.printStackTrace();
							}
							
							
							logger.log(Level.SEVERE,"Date After setting time"+duedateForFilter);
							
							

							try {

								// int type=Integer.parseInt(reqtype);

								UpdateServiceImpl updateSer = new UpdateServiceImpl();

								// company=ofy().load().type(Company.class).filter("companyId",
								// Long.parseLong("5348024557502464")).first().now();
								// filter("accessUrl","my")
							
								
//								for (String roleName : employeeRoleList) {
									List<Employee> employeeList = new ArrayList<Employee>();
									employeeList = ofy().load().type(Employee.class)
											.filter("companyId", companyId)
											.filter("roleName", cronDetails.getEmployeeRole()).list();
									logger.log(Level.SEVERE,	"" + cronDetails.getEmployeeRole() + " " + "Employee List size::: "+ employeeList.size());

								
									for (Employee employee : employeeList) {
										ArrayList<String> toEmailListDt=new ArrayList<String>();
										toEmailListDt.add(employee.getEmail().trim());
										List<String> branchList=new ArrayList<String>();
										for (int j = 0; j < employee.getEmpBranchList().size(); j++) {
											branchList.add(employee.getEmpBranchList().get(j).getBranchName());
											logger.log(Level.SEVERE, "  -- branch list --"+ employee.getEmpBranchList().get(j).getBranchName());
										}
										logger.log(Level.SEVERE,"toEmailList size::: "+toEmailListDt.size()+" Branch List SIze : "+branchList.size());
										
										if(branchList.size()!=0){
											
											{
												
												if( cronDetails.isOverdueStatus()){
													updateSer.dailyFumigationReportDetails(companyId,null,duedateForFilter,branchList);
												 }else{
													 updateSer.dailyFumigationReportDetails(companyId,dateForFilter,duedateForFilter,branchList);
												 }
											}

											XlsxWriter xlsx = new XlsxWriter();
											Date d1 = new Date();
											String dateString = fmt.format(d1);
											XSSFWorkbook workbook = new XSSFWorkbook();
											xlsx.getFumigationReportDetails(workbook);

											outByteStream = new ByteArrayOutputStream();
											workbook.write(outByteStream);
											byte[] outArray = outByteStream.toByteArray();

											logger.log(Level.SEVERE, "outArray  -  size -- "
													+ outArray.length);

											// doPost(req, resp);
											// OutputStream out = resp.getOutputStream();
											// resp.getOutputStream();
											// out.write(outArray);
											// out.flush();
											// out.close();

											
											{
												if (commodityFumigation.size() > 0) {

													
													ArrayList<String> headDetails = xlsx.getServiceFumigationReportHeader(commodityFumigation.get(0).getDate(), workbook);
													ArrayList<String> serviceFumiDetailList = xlsx.getFumigationReportDetailsList(workbook);
													List<CommodityFumigationDetails> commodityFumigationList = commodityFumigation;
													List<String> brnahcList = branchList;
													TreeMap<String, String> proName1 = proName;
													int srno = 0;

													TreeMap<String, Double> proTotal = new TreeMap<String, Double>();

													TreeMap<String, Double> prodayTotal = new TreeMap<String, Double>();

													for (String pro : proName1.keySet()) {
														proTotal.put(pro, 0.0);
														prodayTotal.put(pro, 0.0);
													}

													/**
													 * Email Parts ends here
													 */

													ArrayList<String> toEmailList = new ArrayList<String>();

															toEmailList.add(employee.getEmail().trim());
															
															

															String[] attachFiles = new String[1];
															attachFiles[0] = "Fumigation_FIle";
															Email cronEmail = new Email();

															try {
																if (commodityFumigationList.size() != 0) {
																	logger.log(Level.SEVERE," header --" + headDetails.size());
																	logger.log(Level.SEVERE," details -- "+serviceFumiDetailList.size());
																	String footer = "";
																	
																	if(cronDetails.getFooter().trim() != "" ){
																		footer = cronDetails.getFooter();
																	}
																	
																	
																	String emailSubject = "Commodity fumigation report As On Date"
																			+ " " + fmtServCreationDate.format(dateForFilter);
																	if(cronDetails.isOverdueStatus()){
																		emailSubject = "Commodity fumigation overdue report As On Date"
																				+ " " + fmtServCreationDate.format(dateForFilter);
																	}
																	if(!cronDetails.getSubject().trim().equals("")  && cronDetails.getSubject().trim().length() >0){
																		emailSubject = cronDetails.getSubject();
																	}
																	
																	String emailMailBody = "Commodity fumigation report As On Date "+fmtServCreationDate.format(dateForFilter);
																	if(cronDetails.isOverdueStatus()){
																		emailMailBody = "Commodity fumigation overdue report As On Date"
																				+ " " + fmtServCreationDate.format(dateForFilter);
																	}
																	if(!cronDetails.getMailBody().trim().equals("")  && cronDetails.getMailBody().trim().length() >0 ){
																		emailMailBody = cronDetails.getMailBody();
																	}
																	
																	
																	
																	cronEmail.cronSendEmail(toEmailList,emailSubject,emailMailBody, company, headDetails,
																			serviceFumiDetailList, null, null, null, attachFiles,
																			outByteStream, "Fumigation_FIle",footer);
																	
																	
																	
																}
															} catch (IOException e) {
																e.printStackTrace();
																logger.log(Level.SEVERE, "Error in cronEmail method:::"
																		+ e);
															}
//														}
//													}


												}

										}
									}
									
								

								}
								
							} catch (Exception e) {
								e.printStackTrace();
								System.out.println(e.getMessage());
							}

							

						} catch (Exception e) {
							e.printStackTrace();
							System.out.println(e.getMessage());
							logger.log(Level.SEVERE,
									"fumigation detail cron job --  " + e.getLocalizedMessage());
						}

						
					}
					
				}
			}
			
		
			
		}catch(Exception e){
			logger.log(Level.SEVERE,"getServiceFumigation -- "+ e);
		}
		
	
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
		// try {
		// resp.setContentType("application/vnd.ms-excel");
		//
		// String reqtype=req.getParameter("type");
		// responseref = resp;
		// System.out.println("Type is "+reqtype);
		//
		// // int type=Integer.parseInt(reqtype);
		//
		//
		// UpdateServiceImpl updateSer = new UpdateServiceImpl();
		//
		// company=ofy().load().type(Company.class).filter("accessUrl","my").first().now();
		// companyId=company.getCompanyId();
		//
		// /**
		// */
		//
		// ProcessConfiguration process =
		// ofy().load().type(ProcessConfiguration.class).filter("companyId",
		// companyId).
		// filter("processName", "CronJob").first().now();
		//
		// for (int i = 0; i < process.getProcessList().size(); i++) {
		//
		// if(process.getProcessList().get(i).processType.equalsIgnoreCase("CommidutyFumigation")){
		// flag= true;
		// }
		// }
		// flag= true;
		// if(flag){
		// updateSer.dailyFumigationReportDetails(companyId, new Date());
		// }
		//
		//
		//
		// // CsvWriter csv=new CsvWriter();
		// XlsxWriter xlsx = new XlsxWriter();
		// Date d=new Date();
		// String dateString = fmt.format(d);
		//
		//
		// xlsx.getFumigationReportDetails(workbook);
		//
		//
		// outByteStream = new ByteArrayOutputStream();
		// workbook.write(outByteStream);
		// byte [] outArray = outByteStream.toByteArray();
		//
		// logger.log(Level.SEVERE,"outArray  -  size -- " + outArray.length);
		//
		// doPost(req, resp);
		// // OutputStream out = resp.getOutputStream();
		// // resp.getOutputStream();
		// // out.write(outArray);
		// // out.flush();
		// // out.close();
		//
		// } catch (Exception e) {
		// e.printStackTrace();
		// System.out.println(e.getMessage());
		// }
	}

}
