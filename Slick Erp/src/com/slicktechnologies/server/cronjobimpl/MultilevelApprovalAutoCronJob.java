package com.slicktechnologies.server.cronjobimpl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.ApproversRemark;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApproval;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApprovalDetails;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;


/**
 * Date 27-04-2017
 * Created by vijay 
 * this cron job is for Deadstock NBHC
 * this is for multilevel purpose
 * if approval is pending from less than yesterday date then directly sent to next level
 * by checking remark 
 * this automatic approval applicable to max level-1
 * 
 */

public class MultilevelApprovalAutoCronJob extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7790903713871032918L;
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

	DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	

	 Logger logger = Logger.getLogger("NameOfYourLogger");
	  
		public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws IOException {
			
				MultilevelApprovalList();
		}

	private void MultilevelApprovalList() {
		Email cronEmail = new Email();
		
		Date today=DateUtility.getDateWithTimeZone("IST", new Date());
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		df.setTimeZone(TimeZone.getTimeZone("IST"));
		
		/**
		 *   Date which is  -1 days with time 23 59 999 
		 */
		
		logger.log(Level.SEVERE,"Date Before Adding  Day"+today);
		Calendar cal=Calendar.getInstance();
		cal.setTime(today);
		cal.add(Calendar.DATE, -1);
		
		Date yesterdayDate=null;
		
		try {
			yesterdayDate=dateFormat.parse(dateFormat.format(cal.getTime()));
			cal.set(Calendar.HOUR_OF_DAY,23);
			cal.set(Calendar.MINUTE,59);
			cal.set(Calendar.SECOND,59);
			cal.set(Calendar.MILLISECOND,999);
			yesterdayDate=cal.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"Yesterday Date"+yesterdayDate);
		
		 
		List<Company> compEntity = ofy().load().type(Company.class).list();
		if(compEntity.size()>0){

			logger.log(Level.SEVERE,"If compEntity size > 0");
			logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());

			for(int i=0;i<compEntity.size();i++){
				
				ProcessName multilevelapprovalprocess = ofy().load().type(ProcessName.class).filter("processName","MultilevelApproval").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();

				ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
				ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
				
				boolean mulilevelApprovalAutoFlag = false;
				if(processconfig!=null){
					if(processconfig.isConfigStatus()){
						for(int l=0;l<processconfig.getProcessList().size();l++){
							if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("AutoMultilevelApproval") && processconfig.getProcessList().get(l).isStatus()==true){
								mulilevelApprovalAutoFlag=true;
							}
						}
				   }
				}
				logger.log(Level.SEVERE,"mulilevelApprovalAutoFlag ="+mulilevelApprovalAutoFlag);
				
				if(processName!=null && multilevelapprovalprocess!=null){
					logger.log(Level.SEVERE,"In the prossName");
				if(mulilevelApprovalAutoFlag){
					logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
					
					List<Approvals> approvalEntity = ofy().load().type(Approvals.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("creationDate <=",yesterdayDate).filter("status","Pending").list();
					logger.log(Level.SEVERE,"Approval entity size:"+approvalEntity.size());	
					
					List<MultilevelApproval> multiLevelApprovalLis= ofy().load().type(MultilevelApproval.class).filter("companyId", compEntity.get(i).getCompanyId()).list();
					System.out.println("multiLevelApprovalLis"+multiLevelApprovalLis.size());
				
					
					for(int j=0;j<approvalEntity.size();j++){
						
						
						/**
						 * Here i am checking the next level approver Name
						 */
						List<MultilevelApprovalDetails> multiLevelTableLis=new ArrayList<MultilevelApprovalDetails>();
						
						for(int d=0;d<multiLevelApprovalLis.size();d++)
						{
							System.out.println("Condtion=="+(multiLevelApprovalLis.get(d).getDocumentType().trim().equals(approvalEntity.get(j).getBusinessprocesstype().trim())&&multiLevelApprovalLis.get(d).isStatus()==true));
							System.out.println("1"+multiLevelApprovalLis.get(d).getDocumentType());
							System.out.println("2 "+approvalEntity.get(j).getBusinessprocesstype().trim());
							System.out.println("3"+multiLevelApprovalLis.get(d).isStatus());
							if(multiLevelApprovalLis.get(d).getDocumentType().trim().equals(approvalEntity.get(j).getBusinessprocesstype().trim())&&multiLevelApprovalLis.get(d).isStatus()==true)
							{
								multiLevelTableLis=multiLevelApprovalLis.get(d).getApprovalLevelDetails();
							}
						}
						System.out.println("list  = "+multiLevelTableLis.size());
						logger.log(Level.SEVERE,"list"+multiLevelTableLis.size());	

						int maxLevel=0;
						int temp=0;
						String approverName="";
						logger.log(Level.SEVERE,"approvalEntity.get(j).getApprovalLevel()"+approvalEntity.get(j).getApprovalLevel());	
						int nextlevel = approvalEntity.get(j).getApprovalLevel()+1;
						logger.log(Level.SEVERE," nextlevel "+nextlevel);	
						logger.log(Level.SEVERE,"multiLevelTableLis.size()"+multiLevelTableLis.size());	

						for(int p=0;p<multiLevelTableLis.size();p++){
							System.out.println("multiLevelTableLis.get(p).getLevel()"+multiLevelTableLis.get(p).getLevel());
							logger.log(Level.SEVERE,"multiLevelTableLis.get(p).getLevel()"+multiLevelTableLis.get(p).getLevel());	

							if(Integer.parseInt(multiLevelTableLis.get(p).getLevel()) == nextlevel && multiLevelTableLis.get(p).getRemark().trim().equals(approvalEntity.get(j).getBranchname().trim()) ){
								approverName = multiLevelTableLis.get(p).getEmployeeName();
								System.out.println("Approver Name=="+approverName);
							}
							
							if(i==0){
								maxLevel=Integer.parseInt(multiLevelTableLis.get(p).getLevel());
							}else{
								temp=Integer.parseInt(multiLevelTableLis.get(p).getLevel());
								if(temp>maxLevel){
									maxLevel=temp;
								}
							}
						}
						System.out.println("Next Approver Name"+approverName);
						logger.log(Level.SEVERE,"Next Approver Name"+approverName);	

						/**
						 * ends here
						 */
						
						if(!approverName.equals("")){
							
						if(approvalEntity.get(j).getApprovalLevel()<maxLevel){
							logger.log(Level.SEVERE,"Next Approver Name < max level ");	

							/**
							 * here below if condition for if approver level is 1 then directly sending to next level i.e.2
							 * with Auto Remark
							 */
							if(approvalEntity.get(j).getApprovalLevel()==1){
								logger.log(Level.SEVERE," level 1");	

								Approvals approval = new Approvals();
								approval.setApproverName(approverName);
								approval.setBranchname(approvalEntity.get(j).getBranchname());
								approval.setBusinessprocessId(approvalEntity.get(j).getBusinessprocessId());
								approval.setCreationDate(new Date());
								approval.setRequestedBy(approvalEntity.get(j).getApproverName());
								approval.setApprovalLevel(approvalEntity.get(j).getApprovalLevel()+1);
								approval.setStatus(Approvals.PENDING);
								approval.setBusinessprocesstype(approvalEntity.get(j).getBusinessprocesstype());
								approval.setCompanyId(approvalEntity.get(j).getCompanyId());
								approval.setPersonResponsible(approvalEntity.get(j).getPersonResponsible());
								
								if(approvalEntity.get(j).getDocumentCreatedBy()!=null)
									approval.setDocumentCreatedBy(approvalEntity.get(j).getDocumentCreatedBy());
								
									ArrayList<ApproversRemark> list=new ArrayList<ApproversRemark>();
									ApproversRemark apprem=new ApproversRemark();
									apprem.setApproverLevel(approvalEntity.get(j).getApprovalLevel());
									apprem.setApproverName(approvalEntity.get(j).getApproverName());
									apprem.setApproverRemark("Level increased automatically on "+fmt.format(today)+" it was pending at previous level for long");
									list.add(apprem);
									approvalEntity.get(j).setRemarkList(list);
									approval.setRemarkList(approvalEntity.get(j).getRemarkList());
									
									GenricServiceImpl impl = new GenricServiceImpl();
									impl.save(approval);
									
									logger.log(Level.SEVERE,"sent to next level");	
									// Date 08 jun 2017 added for sending Email
									cronEmail.initiateApprovalEmail(approval);
									
									
									//for changing document status as Approved
									approvalEntity.get(j).setRemark("Level increased automatically on "+fmt.format(today)+" it was pending at previous level for long");
									approvalEntity.get(j).setStatus(Approvals.APPROVED);
									
									Date todayDate = new Date();
									try {
										Date approvalRejectDate = dateFormat.parse(dateFormat.format(todayDate));
										approvalEntity.get(j).setApprovalOrRejectDate(approvalRejectDate);

									} catch (ParseException e) {
										// TODO Auto-generated catch block
										approvalEntity.get(j).setApprovalOrRejectDate(today);
										e.printStackTrace();
									}
									
									GenricServiceImpl impl2 = new GenricServiceImpl();
									impl2.save(approvalEntity.get(j));
									logger.log(Level.SEVERE,"changed status from last level");	
									//Date 08 jun 2017 below for email to approved the document
									cronEmail.intiateOnApproveRequestEmail(approvalEntity.get(j));
									
								}else{
							
							/**
							 * here else block is for approval level greater than 1
							 * i am checking its previous 1 level remark if 1 remark is Auto Remark then i am not sending to next level
							 * if previous all level remark is without Auto remark then i will send to next level remark 
							 * with Auto Remark
							 */
							   logger.log(Level.SEVERE," level 2 onwards");	

								Approvals approval = new Approvals();
								approval.setApproverName(approverName);
								approval.setBranchname(approvalEntity.get(j).getBranchname());
								approval.setBusinessprocessId(approvalEntity.get(j).getBusinessprocessId());
								approval.setCreationDate(new Date());
								approval.setRequestedBy(approvalEntity.get(j).getApproverName());
								approval.setApprovalLevel(approvalEntity.get(j).getApprovalLevel()+1);
								approval.setStatus(Approvals.PENDING);
								approval.setBusinessprocesstype(approvalEntity.get(j).getBusinessprocesstype());
								approval.setCompanyId(approvalEntity.get(j).getCompanyId());
								approval.setPersonResponsible(approvalEntity.get(j).getPersonResponsible());
								
								if(approvalEntity.get(j).getDocumentCreatedBy()!=null)
									approval.setDocumentCreatedBy(approvalEntity.get(j).getDocumentCreatedBy());
								
								
									ArrayList<ApproversRemark> list=new ArrayList<ApproversRemark>();
									boolean flag =true;
									for(int l=0;l<approvalEntity.get(j).getRemarkList().size();l++){
										ApproversRemark apprem=new ApproversRemark();
										apprem.setApproverLevel(approvalEntity.get(j).getRemarkList().get(l).getApproverLevel());
										apprem.setApproverName(approvalEntity.get(j).getRemarkList().get(l).getApproverName());
										if(approvalEntity.get(j).getRemarkList().get(l).getApproverRemark().contains("Level increased automatically on ")){
											flag =false;
											break;
										}else{
											apprem.setApproverRemark(approvalEntity.get(j).getRemarkList().get(l).getApproverRemark());

										}
										list.add(apprem);
									}
									System.out.println("list ===="+list.size());
								
									if(flag){
										System.out.println("auto matic next level");
										ApproversRemark apprem=new ApproversRemark();
										apprem.setApproverLevel(approvalEntity.get(j).getApprovalLevel());
										apprem.setApproverName(approvalEntity.get(j).getApproverName());
										apprem.setApproverRemark("Level increased automatically on "+fmt.format(today)+" it was pending at previous level for long");
										list.add(apprem);
										System.out.println("list ===="+list.size());
										approval.setRemarkList(list);
										System.out.println("list hi vijay  ===="+list.size());


										GenricServiceImpl impl = new GenricServiceImpl();
										impl.save(approval);
										logger.log(Level.SEVERE,"sent to next level");
										// Date 08 jun 2017 added for sending Email
										cronEmail.initiateApprovalEmail(approval);
										
										
										//for changing document status as Approved
										approvalEntity.get(j).setRemark("Level increased automatically on "+fmt.format(today)+" it was pending at previous level for long");
										approvalEntity.get(j).setStatus(Approvals.APPROVED);
										Date todayDate = new Date();
										try {
											Date approvalRejectDate = dateFormat.parse(dateFormat.format(todayDate));
											approvalEntity.get(j).setApprovalOrRejectDate(approvalRejectDate);

										} catch (ParseException e) {
											// TODO Auto-generated catch block
											approvalEntity.get(j).setApprovalOrRejectDate(today);
											e.printStackTrace();
										}
										GenricServiceImpl impl2 = new GenricServiceImpl();
										impl2.save(approvalEntity.get(j));
										logger.log(Level.SEVERE,"changed status from last level");	
										
										//Date 08 jun 2017 below for email to approved the document
										cronEmail.intiateOnApproveRequestEmail(approvalEntity.get(j));

									}
									
								}
							
							   /**
							    * ends here
							    */
								
							}
						
					      }
						
						}
					}// processconfiguration end block
					
				 }// processName end block
				
				} // end of company loop
				
			}	
		
	}

}
