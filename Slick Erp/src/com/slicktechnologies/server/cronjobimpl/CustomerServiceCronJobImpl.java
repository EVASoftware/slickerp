package com.slicktechnologies.server.cronjobimpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.factory.SmsFactory;
import com.twilio.sdk.resource.instance.Account;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.email.SendGridEmailServlet;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.email.EmailDetails;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.smshistory.SmsHistory;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class CustomerServiceCronJobImpl extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6268357776825855510L;
	
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private DateFormat fmt1 = new SimpleDateFormat("dd/MM/yyyy");

	
	 Logger logger = Logger.getLogger("NameOfYourLogger");
	  
		public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws IOException {
			
//				servicecontactlist();
				
		}

		private void servicecontactlist() {
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
			Email cronEmail = new Email();	
			
					Date today=DateUtility.getDateWithTimeZone("IST", new Date());
					Calendar cal=Calendar.getInstance();
					cal.setTime(today);
					cal.add(Calendar.DATE, 0);
					
					Date dateForFilter=null;
					
					try {
						dateForFilter=fmt1.parse(fmt1.format(cal.getTime()));
						cal.set(Calendar.HOUR_OF_DAY,23);
						cal.set(Calendar.MINUTE,59);
						cal.set(Calendar.SECOND,59);
						cal.set(Calendar.MILLISECOND,999);
						dateForFilter=cal.getTime();
					} catch (ParseException e) {
						e.printStackTrace();
					}
					
					logger.log(Level.SEVERE,"Date After setting time"+dateForFilter);
					
					/*************************************End*********************************/
					
			 try{
				 
				 logger.log(Level.SEVERE,"In service List");	

				 
			/********************************Adding status in the list ***********************/
				 
				 ArrayList<String> obj = new ArrayList<String>();
				 obj.add("Scheduled");
				 obj.add("Pending");
				 obj.add("Rescheduled");
				 
			
			/******************************Converting todayDate to String format*****************/
				 
				 Date todaysDate = new Date();
				 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				 String todayDateString = df.format(todaysDate);
				 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);

				 
		   /********************************Adding Companies in the list ***********************/
				 
			List<Company> compEntity = ofy().load().type(Company.class).list();
			if(compEntity.size()>0){
			
				logger.log(Level.SEVERE,"If compEntity size > 0");
				logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
				

				for(int i=0;i<compEntity.size();i++){
					
					Company c=compEntity.get(i);
					logger.log(Level.SEVERE,"In the for loop");
					logger.log(Level.SEVERE,"Company Name="+c);
					logger.log(Level.SEVERE,"The value of i is:" +i);

					
					/**
					 * @author Vijay Date  :- 18-08-2022
					 * Des :- SMS and whats message will work as per communication type and template selected in cron job reminder settings
					 * below old code commented
					 */
					
//					/************************** for SMS Cron Job   ***************************************/
//					
////					SmsConfiguration smsconfig = ofy().load().type(SmsConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("status", true).first().now();
////					
////					if(smsconfig!=null){
////						
////					String accountSid = smsconfig.getAccountSID();
////					String authoToken = smsconfig.getAuthToken();
////					String frmNumber = smsconfig.getPassword();
////					
//					ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
//					ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
//						
//					boolean prossconfigSMSflag = false;
//					if(processconfig!=null){
//						if(processconfig.isConfigStatus()){
//							for(int l=0;l<processconfig.getProcessList().size();l++){
//								if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("CustomerServiceSms") && processconfig.getProcessList().get(l).isStatus()==true){
//									prossconfigSMSflag=true;
//								}
//							}
//					   }
//					}
//					logger.log(Level.SEVERE,"process config  SMS Flag =="+prossconfigSMSflag);
//					
////					/**
////					 * Date 16 jun 2017 added by vijay
////					 * below process config is used for calling BHASH SMS API 
////					 */
////					ProcessConfiguration smsprocessconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","SMS").first().now();				
////					
////					boolean bhashSMSAPIFlag = false;
////					if(smsprocessconfig!=null){
////						if(smsprocessconfig.isConfigStatus()){
////							for(int l=0;l<smsprocessconfig.getProcessList().size();l++){
////								if(smsprocessconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("SMS") && smsprocessconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("BhashSMSAPI") && smsprocessconfig.getProcessList().get(l).isStatus()==true){
////									bhashSMSAPIFlag=true;
////								}
////							}
////					   }
////					}
////					
////					logger.log(Level.SEVERE,"process config BHASH SMS API Flag =="+bhashSMSAPIFlag);
////					/**
////					 * ends here
////					 */
//					
//					if(processName!=null){
//						logger.log(Level.SEVERE,"In the prossName for SMS ===========");
//					if(prossconfigSMSflag){
//									
//						logger.log(Level.SEVERE,"In the ProsConfifg !=null for SMS ==================");
//						
//						SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("event","Service Due Cron Job").filter("status",true).first().now();
//						
//						if(smsEntity!=null){
//							
//							
//							int day = smsEntity.getDay();
//							logger.log(Level.SEVERE,"Day -------- ==== "+day);
//
//							String templateMsgWithBraces = smsEntity.getMessage();
//							System.out.println("MSG:"+templateMsgWithBraces);
//							
//							Calendar cal1=Calendar.getInstance();
//							cal1.setTime(today);
//							cal1.add(Calendar.DATE, +day);
//							
//							Date dateForFiltersms=null;
//							
//							try {
//								dateForFiltersms=fmt1.parse(fmt1.format(cal1.getTime()));
//							} catch (ParseException e) {
//								e.printStackTrace();
//							}
//							
//							logger.log(Level.SEVERE,"For SMS filterDate time with 00 00 00  ==== "+dateForFiltersms);
//							
//
//							Calendar cal2 = Calendar.getInstance();
//							cal2.setTime(today);
//							cal2.add(Calendar.DATE, +day);
//							
//							Date dateforsmsfilter2 = null;
//							
//							try {
//								
//								dateforsmsfilter2 = fmt1.parse(fmt1.format(cal2.getTime()));
//								cal2.set(Calendar.HOUR_OF_DAY,23);
//								cal2.set(Calendar.MINUTE,59);
//								cal2.set(Calendar.SECOND,59);
//								cal2.set(Calendar.MILLISECOND,999);
//								dateforsmsfilter2=cal2.getTime();
//								
//							} catch (ParseException e) {
//								e.printStackTrace();
//							}
//							
//							logger.log(Level.SEVERE,"Date for SMS 1"+dateforsmsfilter2);
//							System.out.println("dateforsmsfilter2"+dateforsmsfilter2);
//							
//							List<Service> serviceEntity = ofy().load().type(Service.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("serviceDate >=",dateForFiltersms).filter("serviceDate <=",dateforsmsfilter2).filter("status IN",obj).list();
//							System.out.println("serviceEntity========="+serviceEntity);
//							logger.log(Level.SEVERE,"serviceEntity ===="+serviceEntity.size());
//
//							for(int l=0; l<serviceEntity.size(); l++)
//							{
//				
//								/**
//								 * Date 22-11-2020 added by Priyanka Bhagwat.
//								 * Des - As per requirement add ProductName in sms template insted of ServiceID.
//							     */
//								
//								sendsms(compEntity.get(i).getCompanyId(),compEntity.get(i).getBusinessUnitName(),serviceEntity.get(l).getCount(),serviceEntity.get(l).getServiceDate(), serviceEntity.get(l).getCellNumber(),serviceEntity.get(l).getName(),templateMsgWithBraces,serviceEntity.get(l).getProductName(),smsEntity.getEvent());
//							}
//							
//						}
//						
//						}else{
//							
//							logger.log(Level.SEVERE,"Else block of ProsConfifg !=null ");
//
//							
//						}
//					
//					}else{
//						
//						logger.log(Level.SEVERE,"Else block of ProcessName !=null ");
//
//					}
					
//				}else{
//					
//					logger.log(Level.SEVERE,"Else block for SMS config ");
//
//				}
					
				/*************************   SMS cron Job End   ***************************************/			
					

					
					
		   /********************************Checking prosname & prosconfig for each company ***********************/
				
					ProcessName processName2 = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
					ProcessConfiguration processconfig2 = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
					
					boolean processconfigflag = false;
					if(processconfig2!=null){
						if(processconfig2.isConfigStatus()){
							for(int l=0;l<processconfig2.getProcessList().size();l++){
								if(processconfig2.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig2.getProcessList().get(l).getProcessType().equalsIgnoreCase("ServiceDailyMail") && processconfig2.getProcessList().get(l).isStatus()==true){
									processconfigflag=true;
								}
							}
					   }
					}
					logger.log(Level.SEVERE,"process config  SMS Flag =="+processconfigflag);
					
					if(processName2!=null){
						logger.log(Level.SEVERE,"In the prossName");
					if(processconfigflag){
									
						logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
						
						
			/********************************Reading services from Service entity  ***********************/
						
						List<Service> serEntity = ofy().load().type(Service.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("serviceDate <=",dateForFilter).filter("status IN",obj).list();
						
						logger.log(Level.SEVERE,"Today date Is:"+dateForFilter);	
	
						logger.log(Level.SEVERE,"service entity size:"+serEntity.size());	
						
						
						// email id is added to emailList
						ArrayList<String> toEmailList=new ArrayList<String>();
						toEmailList.add(compEntity.get(i).getPocEmail());
						
						 String mailTitl = "Services Due As On Date";
						
						if(serEntity.size()>0){
						
					
						ArrayList<String> tbl_header = new ArrayList<String>();
						tbl_header.add("Branch");
						tbl_header.add("Customer Id");
						tbl_header.add("Customer Name");
						tbl_header.add("Customer Contact No");
						tbl_header.add("Crontact Id");
						tbl_header.add("Service Id");
						tbl_header.add("Service Date");
						tbl_header.add("Service Engineer");
						tbl_header.add("Product Name");
						tbl_header.add("Status");
						tbl_header.add("Ageing");
						
		/********************************Sorting table with Branch & Service Date ***********************/
					
						
						Comparator<Service> serviceDateComparator2 = new Comparator<Service>() {
							public int compare(Service s1, Service s2) {
							
							Date date1 = s1.getServiceDate();
							Date date2 = s2.getServiceDate();
							
							//ascending order
							return date1.compareTo(date2);
							}
							};
							Collections.sort(serEntity, serviceDateComparator2);
							
							Comparator<Service> serviceDateComparator = new Comparator<Service>() {
								public int compare(Service s1, Service s2) {
								
								String branch1 = s1.getBranch();
								String branch2 = s2.getBranch();
								
								//ascending order
								return branch1.compareTo(branch2);
								}
								
								};
								Collections.sort(serEntity, serviceDateComparator);
							
	/********************************Getting serviceEntity data and adding in the tbl1 List ***********************/
									
						ArrayList<String> tbl1=new ArrayList<String>();
							
						for(int j=0;j<serEntity.size();j++){
				   	 		tbl1.add(serEntity.get(j).getBranch()+"");
				   	 		tbl1.add(serEntity.get(j).getPersonInfo().getCount()+"");
				   	 		tbl1.add(serEntity.get(j).getPersonInfo().getFullName()+"");
				   	 		tbl1.add(serEntity.get(j).getPersonInfo().getCellNumber()+"");
				   	 		tbl1.add(serEntity.get(j).getContractCount()+"");
				   	 		tbl1.add(serEntity.get(j).getCount()+"");
				   	 	    tbl1.add(fmt.format(serEntity.get(j).getServiceDate()) +"");
				 
				   	 	    /**************** for getting ageing for each service******/
				   	 	String StringServiceDate = fmt1.format(serEntity.get(j).getServiceDate());
				   	 	    
//				   	 	   	String StringServiceDate = df.format(serviceDate);	
				   			
				   				Date d1 = null;
				   				Date d2 = null;
				   				
				   				d1 = df.parse(StringServiceDate);
				   				d2 = df.parse(todayDateString);
				   				long diff = d2.getTime() - d1.getTime();
								long diffDays = diff / (24 * 60 * 60 * 1000);
								
//								System.out.println("Ageing:"+diffDays);
//						   	 	logger.log(Level.SEVERE,"Ageing:"+diffDays);
					
				   	 	    tbl1.add(serEntity.get(j).getEmployee()+""); 
				   	 	    tbl1.add(serEntity.get(j).getProduct().getProductName()+"");
				   	 	    tbl1.add(serEntity.get(j).getStatus()+"");
				   	 	    tbl1.add(diffDays +"");
				   	 	}			
						
						logger.log(Level.SEVERE,"In the ProsConfig !=null   3 ");
						
		     /********************************Calling cronsendEmail Method ***********************/
						
//						Email e=new Email();					
						try {   
							   
								System.out.println("Before send mail method");
								logger.log(Level.SEVERE,"Before send method call to send mail  ");						
								cronEmail.cronSendEmail(toEmailList, "Services Due As On Date"+" "+ todayDateString, mailTitl +"  "+ todayDateString, c, tbl_header, tbl1, null, null, null, null);
								logger.log(Level.SEVERE,"After send mail method ");		
										//+"  "+
						} catch (IOException e1) {
							
							logger.log(Level.SEVERE,"In the catch block ");
							e1.printStackTrace();
					}
						
					}
					else{							
							
						System.out.println("Sorry no services found for this company");
						logger.log(Level.SEVERE,"Sorry no services found for this company");
						mailTitl = "No Services Found Due";
	
						cronEmail.cronSendEmail(toEmailList, "Services Due As On Date"+" "+ todayDateString, mailTitl , c, null, null, null, null, null, null);
	
					}
						
						
					}
					else{					//else block for prosconfig
							logger.log(Level.SEVERE,"ProcessConfiguration is null");
							System.out.println("ProcessConfiguration is null");
					}
					
					}else{        //else block for pross!=null
						logger.log(Level.SEVERE,"Cron job status is Inactive");
						System.out.println("Cron job status is Inactive");
					}
					
									
				}	//end of for loop
				
			}
			else{			//else block for if(compEntity.size()>0)
				
				logger.log(Level.SEVERE,"No Company found from cron job completed");	
				System.out.println("No Company found from cron job completed");
			}
			
			
		}catch(Exception e2){
			
			e2.printStackTrace();
		}
		
		}
		
	

	/************************************************** SMS Method **************************************/	

		private void sendsms(long companyId, String businessUnitName, int serviceId,Date serviceDate, Long cellNo, String custName,
									String templateMsgWithBraces, String prodName, String templateName) 
		{


			  	String companyName = businessUnitName;
				
				System.out.println("Company Name"+companyName);
				System.out.println("Customer Name:"+custName);
				System.out.println("Product Name:"+prodName);   //Added by Priyanka -As per requirement add ProductName in sms template insted of ServiceID.
				//System.out.println("Service Id"+serviceId);
				System.out.println("Service Date"+serviceDate);
				System.out.println("Cell No"+cellNo);
				System.out.println("Msg "+templateMsgWithBraces);
				
				String servicedate = fmt1.format(serviceDate);
				

				/**
				 * Date 29 jun 2017 added by vijay for Eco friendly
				 */
				
				ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","SMS").filter("status",true).filter("companyId", companyId).first().now();
				ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId).filter("processName","SMS").first().now();				
					
				boolean prossconfigBhashSMSflag = false;
				if(processName!=null){
				if(processConfig!=null){
					if(processConfig.isConfigStatus()){
						for(int l=0;l<processConfig.getProcessList().size();l++){
							if(processConfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("SMS") && processConfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("BHASHSMSAPI") && processConfig.getProcessList().get(l).isStatus()==true){
								prossconfigBhashSMSflag=true;
							}
						}
				   }
				}
				}
				logger.log(Level.SEVERE,"process config  BHASH SMS Flag =="+prossconfigBhashSMSflag);
				
				String actualMsg="";
				if(prossconfigBhashSMSflag){
					custName = getCustomerName(custName,companyId);
					
					String customerName = templateMsgWithBraces.replace("{CustomerName}", custName+"");
					String serviceDatemsg = customerName.replace("{ServiceDate}",servicedate);
					 actualMsg = serviceDatemsg.replace("{companyName}", companyName);

				}
				/**
				 * ends here
				 * else for bhavana api for ours standard sms template
				 */
				else{
					
					String customerName = templateMsgWithBraces.replace("{CustomerName}", custName+"");
					/**
					 * Date 26-11-2020 added by Priyanka-As per requirement add ProductName in sms template insted of ServiceID.
					 */
					String serviceID = customerName.replace("{ServiceId}",prodName+" ");   
					/**
					 * end here.
					 */
					String serviceDatemsg = serviceID.replace("{ServiceDate}",servicedate);
					actualMsg = serviceDatemsg.replace("{companyName}", companyName);
				}
				System.out.println("actualMsg"+actualMsg);
				logger.log(Level.SEVERE,"Actual MSG:"+actualMsg);	
				
				String mobileNo = cellNo +"";
				

				
				
				/**
				 * Date 16 jun 2017 added by vijay for Bhash SMS API Calling
				 */
				
				SmsServiceImpl smsimpl = new SmsServiceImpl();
//				int value =  smsimpl.sendSmsToClient(actualMsg, cellNo, companyId);
//				saveSmsHistory(companyId,serviceId,actualMsg,custName,cellNo);

				/**
				 * @author Vijay Date :- 29-04-2022
				 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
				 * whats and sms based on communication channel configuration.
				 */
				smsimpl.sendMessage(AppConstants.CRONJOB,AppConstants.CRONJOB,templateName,companyId,actualMsg,cellNo,false);
				logger.log(Level.SEVERE,"after sendMessage method");
			
				
		}



	private void saveSmsHistory(long companyId,int serviceId, String actualMsg,String custName, Long cellNo) {
		

		SmsHistory smshistory = new SmsHistory();
		smshistory.setDocumentId(serviceId);
		smshistory.setDocumentType("Customer Services Due");
		smshistory.setSmsText(actualMsg);
		smshistory.setName(custName);
		smshistory.setCellNo(cellNo);
		smshistory.setUserId("Cron Job");
		smshistory.setCompanyId(companyId);
		logger.log(Level.SEVERE,"Data 22222222222222222");

		ofy().save().entity(smshistory).now();
		
		
		logger.log(Level.SEVERE,"Data Saved Successfully========");

		
	}

	/**
	 * Date 29 jun 2017 added by vijay for gtetting customer correspondence Name or full name
	 * @param customerName
	 * @param companyId
	 * @return
	 */
	
	private String getCustomerName(String customerName, long companyId) {
		
		String custName;
		
		Customer customer = ofy().load().type(Customer.class).filter("companyId", companyId).filter("fullname", customerName).first().now();
		
		if(customer!=null){
			
			if(!customer.getCustPrintableName().equals("")){
				custName = customer.getCustPrintableName();
			}else{
				custName = customer.getFullname();
			}
			
		}else{
			custName = customerName;
		}
		
		
		return custName;
	}
	
	/**
	 * ends here
	 */

		/********************************** SMS method **********************************************/
		
			
		/**
		 *  nidhi
		 *  1-03-2018
		 *  for cron job configration change
		 * 
		 */

	public void servicesmscontactlist(){

		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
		Email cronEmail = new Email();	
		
				Date today=DateUtility.getDateWithTimeZone("IST", new Date());
				Calendar cal=Calendar.getInstance();
				cal.setTime(today);
				cal.add(Calendar.DATE, 0);
				
				Date dateForFilter=null;
				
				try {
					dateForFilter=fmt1.parse(fmt1.format(cal.getTime()));
					cal.set(Calendar.HOUR_OF_DAY,23);
					cal.set(Calendar.MINUTE,59);
					cal.set(Calendar.SECOND,59);
					cal.set(Calendar.MILLISECOND,999);
					dateForFilter=cal.getTime();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				logger.log(Level.SEVERE,"Date After setting time"+dateForFilter);
				
				/*************************************End*********************************/
				
		 try{
			 
			 logger.log(Level.SEVERE,"In service List");	

			 
		/********************************Adding status in the list ***********************/
			 
			 ArrayList<String> obj = new ArrayList<String>();
			 obj.add("Scheduled");
			 obj.add("Pending");
			 obj.add("Rescheduled");
			 
		
		/******************************Converting todayDate to String format*****************/
			 
			 Date todaysDate = new Date();
			 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			 String todayDateString = df.format(todaysDate);
			 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);

			 
	   /********************************Adding Companies in the list ***********************/
			 
		List<Company> compEntity = ofy().load().type(Company.class).list();
		if(compEntity.size()>0)
		{
		
			logger.log(Level.SEVERE,"If compEntity size > 0");
			logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
			

			for(int i=0;i<compEntity.size();i++){
				
				Company c=compEntity.get(i);
				logger.log(Level.SEVERE,"In the for loop");
				logger.log(Level.SEVERE,"Company Name="+c);
				logger.log(Level.SEVERE,"The value of i is:" +i);

				
				/**
				 * @author Vijay Date  :- 18-08-2022
				 * Des :- SMS and whats message will work as per communication type and template selected in cron job reminder settings
				 * below old code commented
				 */
				
//				/************************** for SMS Cron Job   ***************************************/
//				
////				SmsConfiguration smsconfig = ofy().load().type(SmsConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("status", true).first().now();
////				
////				if(smsconfig!=null){
////				
////				
////				String accountSid = smsconfig.getAccountSID();
////				String authoToken = smsconfig.getAuthToken();
////				String frmNumber = smsconfig.getPassword();
////				
//				ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
//				ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
//					
//				boolean prossconfigSMSflag = false;
//				if(processconfig!=null){
//					if(processconfig.isConfigStatus()){
//						for(int l=0;l<processconfig.getProcessList().size();l++){
//							if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("CustomerServiceSms") && processconfig.getProcessList().get(l).isStatus()==true){
//								prossconfigSMSflag=true;
//							}
//						}
//				   }
//				}
//				logger.log(Level.SEVERE,"process config  SMS Flag =="+prossconfigSMSflag);
//				
//				
//				if(processName!=null){
//					logger.log(Level.SEVERE,"In the prossName for SMS ===========");
//				if(prossconfigSMSflag){
//								
//					logger.log(Level.SEVERE,"In the ProsConfifg !=null for SMS ==================");
//					
//					SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("event","Service Due Cron Job").filter("status",true).first().now();
//					
//					if(smsEntity!=null){
//						
//						
//						int day = smsEntity.getDay();
//						logger.log(Level.SEVERE,"Day -------- ==== "+day);
//
//						String templateMsgWithBraces = smsEntity.getMessage();
//						System.out.println("MSG:"+templateMsgWithBraces);
//						
//						Calendar cal1=Calendar.getInstance();
//						cal1.setTime(today);
//						cal1.add(Calendar.DATE, +day);
//						
//						Date dateForFiltersms=null;
//						
//						try {
//							dateForFiltersms=fmt1.parse(fmt1.format(cal1.getTime()));
//						} catch (ParseException e) {
//							e.printStackTrace();
//						}
//						
//						logger.log(Level.SEVERE,"For SMS filterDate time with 00 00 00  ==== "+dateForFiltersms);
//						
//
//						Calendar cal2 = Calendar.getInstance();
//						cal2.setTime(today);
//						cal2.add(Calendar.DATE, +day);
//						
//						Date dateforsmsfilter2 = null;
//						
//						try {
//							
//							dateforsmsfilter2 = fmt1.parse(fmt1.format(cal2.getTime()));
//							cal2.set(Calendar.HOUR_OF_DAY,23);
//							cal2.set(Calendar.MINUTE,59);
//							cal2.set(Calendar.SECOND,59);
//							cal2.set(Calendar.MILLISECOND,999);
//							dateforsmsfilter2=cal2.getTime();
//							
//						} catch (ParseException e) {
//							e.printStackTrace();
//						}
//						
//						logger.log(Level.SEVERE,"Date for SMS 1"+dateforsmsfilter2);
//						System.out.println("dateforsmsfilter2"+dateforsmsfilter2);
//						
//						List<Service> serviceEntity = ofy().load().type(Service.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("serviceDate >=",dateForFiltersms).filter("serviceDate <=",dateforsmsfilter2).filter("status IN",obj).list();
//						System.out.println("serviceEntity========="+serviceEntity);
//						logger.log(Level.SEVERE,"serviceEntity ===="+serviceEntity.size());
//
//						for(int l=0; l<serviceEntity.size(); l++)
//						{
//							
//							/**
//							 * @author Vijay Date :- 09-08-2021
//							 * Des :- added below method to check customer DND status if customer DND status is Active 
//							 * then SMS will not send to customer
//							 */
//							ServerAppUtility serverapputility = new ServerAppUtility();
//							boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(serviceEntity.get(l).getPersonInfo().getCount(), serviceEntity.get(l).getCompanyId());
//							if(!dndstatusFlag){
//								
//								/**
//								 * Date 22-11-2020 added by Priyanka Bhagwat.
//								 * Des - As per requirement add ProductName in sms template insted of ServiceID.
//							     */
//								
//								sendsms(compEntity.get(i).getCompanyId(),compEntity.get(i).getBusinessUnitName(),serviceEntity.get(l).getCount(),serviceEntity.get(l).getServiceDate(), serviceEntity.get(l).getCellNumber(),serviceEntity.get(l).getName(),templateMsgWithBraces, serviceEntity.get(l).getProductName(),smsEntity.getEvent());
//							
//							}
//							
//							
//						}
//						
//					}
//					
//					}else{
//						
//						logger.log(Level.SEVERE,"Else block of ProsConfifg !=null ");
//
//						
//					}
//				
//				}else{
//					
//					logger.log(Level.SEVERE,"Else block of ProcessName !=null ");
//
//				}
				
				
//				
//			}else{
//				
//				logger.log(Level.SEVERE,"Else block for SMS config ");
//
//			}
				
				
			/*************************   SMS cron Job End   ***************************************/			
				

				
				
	   /********************************Checking prosname & prosconfig for each company ***********************/
				
								
			}	//end of for loop
			
		}
		else{			//else block for if(compEntity.size()>0)
			
			logger.log(Level.SEVERE,"No Company found from cron job completed");	
			System.out.println("No Company found from cron job completed");
		}
		
		

		
		logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
		
		
	
		
	}catch(Exception e2){
		
		e2.printStackTrace();
	}
	
	
	}
public void servicecontactlist(List<CronJobConfigrationDetails> cronList,HashSet<String>  empRoleList) {
		
//		servicesmscontactlist();
		
		logger.log(Level.SEVERE, "Cron list str --" + cronList);
			try {
				
				List<Company> compEntity = ofy().load().type(Company.class).list();
				 
				if(compEntity.size()>0)
				{
				
					logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
	
					for(int i=0;i<compEntity.size();i++){
										
						Company c=compEntity.get(i);
					/********************************Reading services from Service entity  ***********************/
						for(String empRoleName :empRoleList){
							
							
							/**
							 * @author Vijay Date :- 04-05-2022
							 * Des :- as per new logic sms will send as per communication channel if it does not exist then sms 
							 * will send as per old logic
							 */
							try {
								
								for(int p=0;p<cronList.size();p++){
									
									CronJobConfigrationDetails cronDetails =cronList.get(p);
									
									if(cronDetails.getCommunicationChannel()!=null && !cronDetails.getCommunicationChannel().equals("")
											&& (cronDetails.getCommunicationChannel().equals(AppConstants.SMS) || cronDetails.getCommunicationChannel().equals(AppConstants.WHATSAPP) )){

										SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("event","Service Due Cron Job").filter("status",true).first().now();
										
										if(smsEntity!=null){
											
											TimeZone.setDefault(TimeZone.getTimeZone("IST"));
											fmt.setTimeZone(TimeZone.getTimeZone("IST"));
											fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
											
											Date today=DateUtility.getDateWithTimeZone("IST", new Date());
											Calendar cal=Calendar.getInstance();
											cal.setTime(today);
											cal.add(Calendar.DATE, 0);
											
											Date dateForFilter=null;
											
											try {
												dateForFilter=fmt1.parse(fmt1.format(cal.getTime()));
												cal.set(Calendar.HOUR_OF_DAY,0);
												cal.set(Calendar.MINUTE,0);
												cal.set(Calendar.SECOND,0);
												cal.set(Calendar.MILLISECOND,0);
												dateForFilter=cal.getTime();
											} catch (ParseException e) {
												e.printStackTrace();
											}
											
											cal.setTime(today);
											int diffDay = 0;
											 if(cronDetails.getOverdueDays()>0 && cronDetails.isOverdueStatus()){
												 diffDay = -cronDetails.getOverdueDays();
											 }else{
												 diffDay = cronDetails.getInterval();
											 }
											
											cal.add(Calendar.DATE, diffDay);
											
											Date duedateForFilter=null;
											
											try {
												duedateForFilter=fmt1.parse(fmt1.format(cal.getTime()));
												cal.set(Calendar.HOUR_OF_DAY,23);
												cal.set(Calendar.MINUTE,59);
												cal.set(Calendar.SECOND,59);
												cal.set(Calendar.MILLISECOND,999);
												duedateForFilter=cal.getTime();
											} catch (ParseException e) {
												e.printStackTrace();
											}
												
											logger.log(Level.SEVERE,"Date After setting time"+duedateForFilter);
											logger.log(Level.SEVERE,"In service List");	
											 
										/********************************Adding status in the list ***********************/
											 
											 ArrayList<String> obj = new ArrayList<String>();
											 obj.add("Scheduled");
											 obj.add("Pending");
											 obj.add("Rescheduled");
										
											 /******************************Converting todayDate to String format*****************/
											 
											 Date todaysDate = new Date();
											 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
											 String todayDateString = df.format(todaysDate);
											 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);
							
											 /********************************Adding Companies in the list ***********************/
											 
												List<Service> serEntity = new ArrayList<Service>();
												
												if(cronDetails.isOverdueStatus()){
													
													try {
														cal.add(Calendar.DATE, -90);
														dateForFilter=fmt1.parse(fmt1.format(cal.getTime()));
														cal.set(Calendar.HOUR_OF_DAY,0);
														cal.set(Calendar.MINUTE,0);
														cal.set(Calendar.SECOND,0);
														cal.set(Calendar.MILLISECOND,0);
														dateForFilter=cal.getTime();
													} catch (ParseException e) {
														e.printStackTrace();
													}
													 serEntity = ofy().load().type(Service.class)
																.filter("companyId",compEntity.get(i).getCompanyId())
																.filter("serviceDate <=",duedateForFilter).filter("serviceDate >=",dateForFilter)
																.filter("status IN",obj).list();
												}else{
													 serEntity = ofy().load().type(Service.class)
																.filter("companyId",compEntity.get(i).getCompanyId()).filter("serviceDate >=",dateForFilter)
																.filter("serviceDate <=",duedateForFilter)
																.filter("status IN",obj).list();
												}
												logger.log(Level.SEVERE,"Today date Is:"+dateForFilter);	
												logger.log(Level.SEVERE,"service entity size:"+serEntity.size());	
												
												boolean prossconfigBhashSMSflag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("SMS", "BHASHSMSAPI", compEntity.get(i).getCompanyId());
												SmsServiceImpl smsserviceimpl = new SmsServiceImpl();
												for(Service service : serEntity){
													
													String actualMsg="";
													if(prossconfigBhashSMSflag){
														String custName = getCustomerName(service.getName(),service.getCompanyId());
														String customerName = smsEntity.getMessage().replace("{CustomerName}", custName+"");
														String serviceDatemsg = customerName.replace("{ServiceDate}",fmt1.format(service.getServiceDate()));
														 actualMsg = serviceDatemsg.replace("{companyName}", compEntity.get(i).getBusinessUnitName());

													}
													else{
														String customerName = smsEntity.getMessage().replace("{CustomerName}", service.getName()+"");
														/**
														 * Date 26-11-2020 added by Priyanka-As per requirement add ProductName in sms template insted of ServiceID.
														 */
														String serviceID = customerName.replace("{ServiceId}",service.getProductName()+" ");   
														/**
														 * end here.
														 */
														String serviceDatemsg = serviceID.replace("{ServiceDate}",fmt1.format(service.getServiceDate()));
														actualMsg = serviceDatemsg.replace("{companyName}", compEntity.get(i).getBusinessUnitName());

													}
												

													/**
													 * @author Vijay Date :- 09-08-2021
													 * Des :- added below method to check customer DND status if customer DND status is Active 
													 * then SMS will not send to customer
													 */
													ServerAppUtility serverapputility = new ServerAppUtility();
													boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(service.getPersonInfo().getCount(), service.getCompanyId());
													
													if(!dndstatusFlag){
														smsserviceimpl.sendMessage(AppConstants.CRONJOB, AppConstants.CRONJOB, smsEntity.getEvent(), service.getCompanyId(), actualMsg, service.getCellNumber(),false);
													}
																								}
										}
									}
									else{
										if(p==0){
											servicesmscontactlist();
										}
									}
								
									
								}
								
							} catch (Exception e) {
								e.printStackTrace();
							}
							
							
							
							/**
							 * @author Anil , Date : 25-04-2019
							 * while loading employee added filter ,its status should  be active 
							 * Because mails were triggered on in active employee ids also
							 */
							List<Employee>  employeeList=new ArrayList<Employee>();
							employeeList=ofy().load().type(Employee.class).filter("companyId", c.getCompanyId()).filter("roleName", empRoleName).filter("status", true).list();
							logger.log(Level.SEVERE,""+empRoleName+" "+"Employee List size::: "+employeeList.size());
							
								for (Employee employee : employeeList) {
									
									HashMap<String, Integer> empCount = new HashMap<String,Integer>();
									HashMap<String, Integer> branchCount = new HashMap<String, Integer>();
									
									int duetotal =0, overDue = 0, total=0;
									ArrayList<String> overHeader = new ArrayList<String>();
									ArrayList<String> dueHeader = new ArrayList<String>();
									ArrayList<String> overDetail = new ArrayList<String>();
									ArrayList<String> dueDetail = new ArrayList<String>();
									
									String header1 = "",header2 = "",title = "";
									
									
									 String todayDateString ="";
								 
									 String footer = "";
								 	
								 	/**
									 * @author Anil,Date : 13-02-2019
									 * Renaming mail subject as per rohan's instruction	
									 */
//									String emailSubject =  "Customer service Due As On Date"+" "+ todayDateString;
//									String mailTitl = "Customer service due As On Date";
									String emailSubject =  "Customer services due and overdue as on date"+" "+ todayDateString;
									String mailTitl = "";
									
									String emailMailBody = mailTitl;
									
									
									ArrayList<String> toEmailListDt=new ArrayList<String>();
									toEmailListDt.add(employee.getEmail().trim());
									List<String> branchList=new ArrayList<String>();
									
									branchList.add(employee.getBranchName());
									for (int j = 0; j < employee.getEmpBranchList().size(); j++) {
										branchList.add(employee.getEmpBranchList().get(j).getBranchName());
										logger.log(Level.SEVERE, "  -- branch list --"+ employee.getEmpBranchList().get(j).getBranchName());
									}
									logger.log(Level.SEVERE,"toEmailList size::: "+toEmailListDt.size()+" Branch List SIze : "+branchList.size());
									
									if(branchList.size()!=0){
										for(int k=0;k<cronList.size();k++){
											
											if(cronList.get(k).getEmployeeRole().equalsIgnoreCase(empRoleName)){
												
												CronJobConfigrationDetails cronDetails =cronList.get(k);
												
												TimeZone.setDefault(TimeZone.getTimeZone("IST"));
												fmt.setTimeZone(TimeZone.getTimeZone("IST"));
												fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
													
											
													Date today=DateUtility.getDateWithTimeZone("IST", new Date());
													Calendar cal=Calendar.getInstance();
													cal.setTime(today);
													cal.add(Calendar.DATE, 0);
													
													Date dateForFilter=null;
													
													try {
														dateForFilter=fmt1.parse(fmt1.format(cal.getTime()));
														cal.set(Calendar.HOUR_OF_DAY,0);
														cal.set(Calendar.MINUTE,0);
														cal.set(Calendar.SECOND,0);
														cal.set(Calendar.MILLISECOND,0);
														dateForFilter=cal.getTime();
													} catch (ParseException e) {
														e.printStackTrace();
													}
													
													cal.setTime(today);
													int diffDay = 0;
													 if(cronDetails.getOverdueDays()>0 && cronDetails.isOverdueStatus()){
														 diffDay = -cronDetails.getOverdueDays();
													 }else{
														 diffDay = cronDetails.getInterval();
													 }
													
													cal.add(Calendar.DATE, diffDay);
													
													Date duedateForFilter=null;
													
													try {
														duedateForFilter=fmt1.parse(fmt1.format(cal.getTime()));
														cal.set(Calendar.HOUR_OF_DAY,23);
														cal.set(Calendar.MINUTE,59);
														cal.set(Calendar.SECOND,59);
														cal.set(Calendar.MILLISECOND,999);
														duedateForFilter=cal.getTime();
													} catch (ParseException e) {
														e.printStackTrace();
													}
													
														
														logger.log(Level.SEVERE,"Date After setting time"+duedateForFilter);
														
														/*************************************End*********************************/
														
												 try{
													 
													 logger.log(Level.SEVERE,"In service List");	
									
													 
												/********************************Adding status in the list ***********************/
													 
													 ArrayList<String> obj = new ArrayList<String>();
													 obj.add("Scheduled");
													 obj.add("Pending");
													 obj.add("Rescheduled");
													 
												
												/******************************Converting todayDate to String format*****************/
													 
													 Date todaysDate = new Date();
													 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
													  todayDateString = df.format(todaysDate);
													 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);
									
													 
											   /********************************Adding Companies in the list ***********************/
													 
														List<Service> serEntity = new ArrayList<Service>();
														
														if(cronDetails.isOverdueStatus()){
															
															try {
																cal.add(Calendar.DATE, -90);
																dateForFilter=fmt1.parse(fmt1.format(cal.getTime()));
																cal.set(Calendar.HOUR_OF_DAY,0);
																cal.set(Calendar.MINUTE,0);
																cal.set(Calendar.SECOND,0);
																cal.set(Calendar.MILLISECOND,0);
																dateForFilter=cal.getTime();
															} catch (ParseException e) {
																e.printStackTrace();
															}
															 serEntity = ofy().load().type(Service.class)
																		.filter("companyId",compEntity.get(i).getCompanyId())
																		.filter("serviceDate <=",duedateForFilter).filter("serviceDate >=",dateForFilter)
																		.filter("status IN",obj).filter("branch IN", branchList).list();
														}else{
															 serEntity = ofy().load().type(Service.class)
																		.filter("companyId",compEntity.get(i).getCompanyId()).filter("serviceDate >=",dateForFilter)
																		.filter("serviceDate <=",duedateForFilter).filter("branch IN", branchList)
																		.filter("status IN",obj).list();
														}
														
														logger.log(Level.SEVERE,"Today date Is:"+dateForFilter);	
											
														logger.log(Level.SEVERE,"service entity size:"+serEntity.size());	
														
														
														
														  mailTitl = "Services Due As On Date";
														
														{
														
													
														ArrayList<String> tbl_header = new ArrayList<String>();
														
														
											/********************************Sorting table with Branch & Service Date ***********************/
													
														
														Comparator<Service> serviceDateComparator2 = new Comparator<Service>() {
															public int compare(Service s1, Service s2) {
															
															Date date1 = s1.getServiceDate();
															Date date2 = s2.getServiceDate();
															
															//ascending order
															return date1.compareTo(date2);
															}
															};
															Collections.sort(serEntity, serviceDateComparator2);
															
															Comparator<Service> serviceDateComparator = new Comparator<Service>() {
																public int compare(Service s1, Service s2) {
																
																String branch1 = s1.getBranch();
																String branch2 = s2.getBranch();
																
																//ascending order
																return branch1.compareTo(branch2);
																}
																
																};
																Collections.sort(serEntity, serviceDateComparator);
															
																tbl_header.add("Ser_No");
																tbl_header.add("Branch");
																tbl_header.add("Service Id");
																tbl_header.add("Service Date");
																tbl_header.add("Service Engineer");
																tbl_header.add("Customer Name");
																tbl_header.add("Customer Contact No");
																tbl_header.add("Crontact Id");
																tbl_header.add("Product Name");
																tbl_header.add("Service Address");
																tbl_header.add("Locality");
																tbl_header.add("City");
																tbl_header.add("Status");
																tbl_header.add("Ageing");
											/********************************Getting serviceEntity data and adding in the tbl1 List ***********************/
																	
														ArrayList<String> tbl1=new ArrayList<String>();
															
														for(int j=0;j<serEntity.size();j++){
															total++;
															tbl1.add(j+1+"");
												   	 		tbl1.add(serEntity.get(j).getBranch()+"");
													   	 	tbl1.add(serEntity.get(j).getCount()+"");
												   	 	    tbl1.add(fmt.format(serEntity.get(j).getServiceDate()) +"");
												   	 	    tbl1.add(serEntity.get(j).getEmployee()+""); 
												   	 		tbl1.add(serEntity.get(j).getPersonInfo().getFullName()+"");
												   	 		tbl1.add(serEntity.get(j).getPersonInfo().getCellNumber()+"");
												   	 		tbl1.add(serEntity.get(j).getContractCount()+"");
												   	 		
												 
												   	 	    /**************** for getting ageing for each service******/
												   	 	String StringServiceDate = fmt1.format(serEntity.get(j).getServiceDate());
												   	 	    
											//	   	 	   	String StringServiceDate = df.format(serviceDate);	
												   			
												   				Date d1 = null;
												   				Date d2 = null;
												   				
												   				d1 = df.parse(StringServiceDate);
												   				d2 = df.parse(todayDateString);
												   				long diff = d2.getTime() - d1.getTime();
																long diffDays = diff / (24 * 60 * 60 * 1000);
																
													
												   	 	    tbl1.add(serEntity.get(j).getProduct().getProductName()+"");
												   	 	    String address = "";
												   	 	    if(serEntity.get(j).getAddrLine1()!=null){
												   	 	    	address = serEntity.get(j).getAddrLine1();
												   	 	    }
												   	 	    
												   	 	    if(serEntity.get(j).getAddrLine2()!=null){
												   	 	    	address =address + " "+ serEntity.get(j).getAddrLine2();
												   	 	    }
												   	 	    tbl1.add(address);
												   	 	    
												   	 	    String locality = "";
													   	 	if(serEntity.get(j).getLocality()!=null){
													   	 		locality = serEntity.get(j).getLocality();
												   	 	    }
												   	 	    tbl1.add(locality);
												   	 	    tbl1.add(serEntity.get(j).getCity());
												   	 	    tbl1.add(serEntity.get(j).getStatus()+"");
												   	 	    tbl1.add(diffDays +"");
												   	 
														
												   	 	   if(empCount.containsKey(serEntity.get(j).getEmployee().trim())){
												   	 	    	int count = empCount.get(serEntity.get(j).getEmployee().trim());
												   	 	    			
												   	 	    	count++;
												   	 	    	empCount.put(serEntity.get(j).getEmployee().trim(), count);
												   	 	    }else{
												   	 	    	empCount.put(serEntity.get(j).getEmployee().trim(), 1);
												   	 	    }
														
												   	 	    if(branchCount.containsKey(serEntity.get(j).getBranch().trim())){
													   	 	    int count = branchCount.get(serEntity.get(j).getBranch().trim());
										   	 	    			
												   	 	    	count++;
												   	 	    	branchCount.put(serEntity.get(j).getBranch().trim(), count);
												   	 	  
												   	 	    }else{
												   	 	    	branchCount.put(serEntity.get(j).getBranch().trim(),1);
												   	 	    }
														
														}			
														
														logger.log(Level.SEVERE,"In the ProsConfig !=null   3 ");
														
											 /********************************Calling cronsendEmail Method ***********************/
														
											//			Email e=new Email();					
														try {   
															   
																System.out.println("Before send mail method");
																logger.log(Level.SEVERE,"Before send method call to send mail  ");						
																
																if(!cronDetails.getFooter().equals("") ){
																	footer = cronDetails.getFooter();
																}
																
																/**
																 * @author Anil,Date : 13-02-2019
																 * Renaming mail subject as per the rohan's instruction
																 */
//																 emailSubject = "Services due as on date"+" "+	todayDateString;
																 emailSubject = "Services due and overdue as on date"+" "+	todayDateString;
																
														   	 	
																 
																
																logger.log(Level.SEVERE," get record size ..- " + tbl1.size());
																if(cronDetails.isOverdueStatus()){
																	logger.log(Level.SEVERE,"over due recordes are  there.. ");
																	header2 = "Over due record upto date :"+todayDateString;

																	if(!cronDetails.getSubject().trim().equals("") && cronDetails.getSubject().trim().length() >0 ){
																		header2 = cronDetails.getSubject();
																	}
																	
																	if(!cronDetails.getMailBody().equals("") && cronDetails.getMailBody().trim().length() >0){
																		header2 = header2 + " <BR>"+ cronDetails.getMailBody();
																	}
																	
																	header2 = header2  +" <BR> Total Records - "+serEntity.size() ;
																	overHeader = new ArrayList<String>();
																	overHeader.addAll(tbl_header);
																	overDetail.addAll(tbl1);
																	overDue = serEntity.size();
																	
																	footer = footer +"<br>" + "#This mail contains last 3 months data. For more records please contact Email- support@evasoftwaresolutions.com";
																}else{
																	header1 = "Due record upto date : "+todayDateString;
																	if(!cronDetails.getSubject().trim().equals("")  && cronDetails.getSubject().trim().length() >0){
																		header1 = cronDetails.getSubject();
																	}
																	
																	if(!cronDetails.getMailBody().equals("")  && cronDetails.getMailBody().trim().length() >0){
																		header1 = header1 + " <BR>"+ cronDetails.getMailBody();
																	}
																	header1 = header1  +"<BR> Total Records - "+serEntity.size() ;
																	dueHeader = new ArrayList<String>();
																	dueHeader.addAll(tbl_header);
																	dueDetail = new ArrayList<String>();
																	dueDetail.addAll(tbl1);
																	duetotal= serEntity.size();
																}
																
																
																logger.log(Level.SEVERE,"After send mail method ");		
														} catch (Exception e1) {
															
															logger.log(Level.SEVERE,"In the catch block ");
															e1.printStackTrace();
													}
														
													}
													
													
												 }catch(Exception e){
													 e.printStackTrace();
													 logger.log(Level.SEVERE,"get error is  recores"+e.getMessage());
												 }
											}
											
											
										}
										
										Email cronEmail = new Email();
										if(dueDetail.size()>0 || overDetail.size()>0){
											

											logger.log(Level.SEVERE," get branch count --" + branchCount.size() + " emp count --" + empCount.size());
											logger.log(Level.SEVERE," get branch  --" + branchCount.toString() + " emp count --" + empCount.toString());
											ArrayList<String> summHeader  = new ArrayList<String>();
											ArrayList<String> summDetails = new ArrayList<String>();
											
											summHeader.add("Branch");
											summHeader.add("$#");

											summHeader.add("Service Engineer");
											summHeader.add("$#");
											
											int tblSize = 0;
											if(branchCount.size()>0){
												tblSize = branchCount.size();
											}
											
											if(tblSize<empCount.size()){
												tblSize = empCount.size();
											}

											Set<String> branchCntList = branchCount.keySet();
											ArrayList<String> branchDt = new ArrayList<String>();
											branchDt.addAll(branchCntList);
											
											
											Set<String> empCntList = empCount.keySet();
											ArrayList<String> empDt = new ArrayList<String>();
											empDt.addAll(empCntList);
											logger.log(Level.SEVERE,"get emp det -- " + empDt.size() + "tblSize " +tblSize);
											for(int ij=0;ij<tblSize;ij++){
												logger.log(Level.SEVERE,"ij " + ij +" branch0-- " + branchCount.size());
												if(branchCount.size()>ij){
													logger.log(Level.SEVERE,"ij " + ij  +" branch --" + branchDt.get(ij));
													summDetails.add(branchDt.get(ij)+"");
													summDetails.add(branchCount.get(branchDt.get(ij))+"");
												}else{
													summDetails.add("");
													summDetails.add("");
												}
												
												if(empCount.size()>ij){
													logger.log(Level.SEVERE,"ij " + ij  +" empDt --" + empDt.get(ij));
													summDetails.add(empDt.get(ij)+"");
													summDetails.add(empCount.get(empDt.get(ij))+"");
												}else{
													summDetails.add("");
													summDetails.add("");
												}
											}
											String summTitle = " Total Records - " + total ;
											logger.log(Level.SEVERE,"Before send method call to send mail  " + total);						
										
																
											
											cronEmail.cronSendEmailDueOverDue(toEmailListDt, emailSubject, "", c,header1, dueHeader, dueDetail,
													header2, overHeader, overDetail, summTitle,summHeader,summDetails,null, "", footer);
											logger.log(Level.SEVERE,"after send method call to send mail  ");	
											
										}else{							
												
											System.out.println("Sorry no services found for this company");
											logger.log(Level.SEVERE,"Sorry no services found for this company");
											mailTitl = "No Services Found Due";
								
											cronEmail.cronSendEmail(toEmailListDt, "Services Due As On Date"+" "+ todayDateString, mailTitl , c, null, null, null, null, null, null);
								
										}
									}
								
								}
							
						}
						
					}
				}
				
			
	}catch(Exception e){
		e.printStackTrace();
		logger.log(Level.SEVERE," get error in service cron job"+e.getMessage());
	}
		 
}

public void scheduleService(Company company, List<CronJobConfigrationDetails> cronJobDetailList, HashSet<String> empRoleList) {
	
	if(company.getSchedulingUrl()==null||company.getSchedulingUrl().equals("")){
		logger.log(Level.SEVERE, "Scheduling URL is missing");
		return;
	}
	
	if(company.getMinimumDuration()==0){
		logger.log(Level.SEVERE, "Minimum scheduling duration is missing");
		return;
	}
	
	TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
	

	Date today = DateUtility.getDateWithTimeZone("IST", new Date());
//	Calendar cal = Calendar.getInstance();
//	cal.setTime(today);
//	cal.add(Calendar.DATE, 0);
//
//	Date modifiedTodaysDate = null;
//
//	try {
//		modifiedTodaysDate = fmt1.parse(fmt1.format(cal.getTime()));
//		cal.set(Calendar.HOUR_OF_DAY, 23);
//		cal.set(Calendar.MINUTE, 59);
//		cal.set(Calendar.SECOND, 59);
//		cal.set(Calendar.MILLISECOND, 999);
//		modifiedTodaysDate = cal.getTime();
//	} catch (ParseException e) {
//		e.printStackTrace();
//	}
//
//	logger.log(Level.SEVERE, "modifiedTodaysDate " + modifiedTodaysDate);


	try {
		
		
		for(String empRoleName :empRoleList){
			
			
			if(!empRoleName.equalsIgnoreCase(AppConstants.CUSTOMER)){
				
				List<Employee>  employeeList=new ArrayList<Employee>();
				employeeList=ofy().load().type(Employee.class).filter("companyId",company.getCompanyId()).filter("roleName", empRoleName).filter("status", true).list();
				logger.log(Level.SEVERE,""+empRoleName+" "+"Employee List size::: "+employeeList.size());
				List<String> branchList=new ArrayList<String>();
				
				for (Employee employee : employeeList) {

					branchList.add(employee.getBranchName());
					for (int j = 0; j < employee.getEmpBranchList().size(); j++) {
						branchList.add(employee.getEmpBranchList().get(j).getBranchName());
						logger.log(Level.SEVERE, "  -- branch list --"+ employee.getEmpBranchList().get(j).getBranchName());
					}
					logger.log(Level.SEVERE," Branch List SIze : "+branchList.size());
					
					if(branchList.size()!=0){
					
						serviceScheduleToClient(cronJobDetailList,employee,branchList,today,company);
					}
				}
				
			}
			else{
				serviceScheduleToClient(cronJobDetailList,null,null,today,company);

			}

			
		}

	} catch (Exception e2) {
		e2.printStackTrace();
	}

}





private boolean validDuration(Service service, int minimumDuration) {
	try{
		ServiceProduct product=(ServiceProduct) service.getProduct();
		int duration=product.getDuration()/product.getNumberOfServices();
		if(duration>=minimumDuration){
			return true;
		}
		logger.log(Level.SEVERE, "Service scheduled - "+service.getPersonInfo().getFullName()+" / "+service.getCount()+" Duration : "+duration+" minimumDurtion "+minimumDuration+" prod Duration "+product.getDuration()+" services "+product.getNumberOfServices());
	}catch(Exception e){
		
	}
	
	return false;
}

	private void serviceScheduleToClient(List<CronJobConfigrationDetails> cronJobDetailList, Employee employee,
			List<String> branchList, Date today, Company company) {
	
		try {
			
		String schedulingLink=company.getSchedulingUrl();
		int minimumDuration=company.getMinimumDuration();
		
		Email cronEmail = new Email();

		ArrayList<String> obj = new ArrayList<String>();
		obj.add("Scheduled");
		obj.add("Pending");
		obj.add("Rescheduled");
		
		for (CronJobConfigrationDetails cronDetails:cronJobDetailList) {
			int day = 0;
			if (cronDetails.getOverdueDays() > 0&& cronDetails.isOverdueStatus()) {
				day = -cronDetails.getOverdueDays();
			} else {
				day = cronDetails.getInterval();
			}
		
			Calendar cal1 = Calendar.getInstance();
			cal1.setTime(today);
//			cal1.add(Calendar.DATE, +day);
	
			Date fromServiceDate = null;
			try {
				fromServiceDate = fmt1.parse(fmt1.format(cal1.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
	
			logger.log(Level.SEVERE,"fromServiceDate : "+ fromServiceDate);
	
			Calendar cal2 = Calendar.getInstance();
			cal2.setTime(today);
			cal2.add(Calendar.DATE, +day);
	
			Date toServiceDate = null;
	
			try {
				toServiceDate = fmt1.parse(fmt1.format(cal2.getTime()));
				cal2.set(Calendar.HOUR_OF_DAY, 23);
				cal2.set(Calendar.MINUTE, 59);
				cal2.set(Calendar.SECOND, 59);
				cal2.set(Calendar.MILLISECOND, 999);
				toServiceDate = cal2.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE, "toServiceDate : "+ toServiceDate);
			List<Service> serviceEntity=new ArrayList<Service>();
			
			if(branchList!=null && branchList.size()>0){
				
				if (cronDetails.getOverdueDays() > 0&& cronDetails.isOverdueStatus()) {
					serviceEntity = ofy().load().type(Service.class)
							.filter("companyId",company.getCompanyId())
							.filter("serviceDate >=",toServiceDate)
							.filter("serviceDate <=",fromServiceDate).filter("status IN", obj).filter("branch IN", branchList).list();
				}else{
					serviceEntity = ofy().load().type(Service.class)
						.filter("companyId",company.getCompanyId())
						.filter("serviceDate >=",fromServiceDate)
						.filter("serviceDate <=",toServiceDate).filter("status IN", obj).filter("branch IN", branchList).list();
				}
				
			}
			else{
				
				if (cronDetails.getOverdueDays() > 0&& cronDetails.isOverdueStatus()) {
					serviceEntity = ofy().load().type(Service.class)
							.filter("companyId",company.getCompanyId())
							.filter("serviceDate >=",toServiceDate)
							.filter("serviceDate <=",fromServiceDate).filter("status IN", obj).list();
				}else{
					serviceEntity = ofy().load().type(Service.class)
						.filter("companyId",company.getCompanyId())
						.filter("serviceDate >=",fromServiceDate)
						.filter("serviceDate <=",toServiceDate).filter("status IN", obj).list();
				}
				
			}
			
			
			if(serviceEntity==null||serviceEntity.size()==0){
				logger.log(Level.SEVERE,"No Services found for the given duration");
				return;
			}
			
			Comparator<Service> serviceDateComparator2 = new Comparator<Service>() {
				public int compare(Service s1, Service s2) {

					Date date1 = s1.getServiceDate();
					Date date2 = s2.getServiceDate();
					// ascending order
					return date1.compareTo(date2);
				}
			};
			Collections.sort(serviceEntity,serviceDateComparator2);

			Comparator<Service> serviceDateComparator = new Comparator<Service>() {
				public int compare(Service s1, Service s2) {
					String branch1 = s1.getBranch();
					String branch2 = s2.getBranch();
					// ascending order
					return branch1.compareTo(branch2);
				}

			};
			Collections.sort(serviceEntity,serviceDateComparator);
			
			logger.log(Level.SEVERE,"serviceEntity ===="+ serviceEntity.size());
	
				
			boolean communicationChannelEmailFlag = false;
			if(cronDetails.getCommunicationChannel()!=null && !cronDetails.getCommunicationChannel().equals("")
					&& (cronDetails.getCommunicationChannel().equals(AppConstants.SMS) || cronDetails.getCommunicationChannel().equals(AppConstants.WHATSAPP) )){
				long cellNumber=0;
				if(!cronDetails.getEmployeeRole().equalsIgnoreCase(AppConstants.CUSTOMER)){
					cellNumber = employee.getCellNumber1();
				}
				sendServiceSchedulingMessage(serviceEntity,cronDetails,company,cellNumber);
			}
			if(cronDetails.getCommunicationChannel()==null || cronDetails.getCommunicationChannel().equals("")){
				communicationChannelEmailFlag = true;
			}
			if(cronDetails.getCommunicationChannel()!=null && !cronDetails.getCommunicationChannel().equals("")
					&&  cronDetails.getCommunicationChannel().equals(AppConstants.EMAIL)){
				communicationChannelEmailFlag = true;
			}
			logger.log(Level.SEVERE,"communicationChannelEmailFlag "+communicationChannelEmailFlag);
			
					
			if(communicationChannelEmailFlag){
				
			for (Service service:serviceEntity) {
				if(service.getPersonInfo().getEmail()==null||service.getPersonInfo().getEmail().equals("")){
					logger.log(Level.SEVERE, "Email Id not found- "+service.getPersonInfo().getFullName()+" / "+service.getCount());
					continue;
				}
				
				if(service.isServiceScheduled()==true){
					logger.log(Level.SEVERE, "Service already scheduled- "+service.getPersonInfo().getFullName()+" / "+service.getCount());
					continue;
				}
				
				if(service.isScheduledByClient()==true){
					logger.log(Level.SEVERE, "Service scheduled by client- "+service.getPersonInfo().getFullName()+" / "+service.getCount());
					continue;
				}
				
				if(!validDuration(service, minimumDuration)){
					continue;
				}
				
				ArrayList<String> toEmailList = new ArrayList<String>();
//				toEmailList.add(service.getPersonInfo().getEmail());
				
				if(!cronDetails.getEmployeeRole().equalsIgnoreCase(AppConstants.CUSTOMER)){
					toEmailList.add(employee.getEmail());
				}
				else{
					toEmailList.add(service.getPersonInfo().getEmail());
				}
				logger.log(Level.SEVERE,"toEmailList "+toEmailList);

				
				
				try {   
					logger.log(Level.SEVERE,"Before send method call to send mail  ");	
					String updatedSchedulingLink="";
					if(schedulingLink!=null&&!schedulingLink.equals("")){
						logger.log(Level.SEVERE, "ZOHO SCHEDULING URL " + schedulingLink);
						StringBuilder sbPostData= new StringBuilder(schedulingLink);
				        sbPostData.append("?customerId="+service.getPersonInfo().getCount());
				        sbPostData.append("&customerName="+service.getPersonInfo().getFullName());
				        sbPostData.append("&serviceId="+service.getCount());
				        sbPostData.append("&serviceDate="+fmt.format(service.getServiceDate()));
				        sbPostData.append("&serviceName="+service.getProductName());
				        sbPostData.append("&companyId="+service.getCompanyId());
//				        sbPostData.append("&technicianName="+service.getEmployee());
//				        sbPostData.append("&branch="+service.getBranch());
				        updatedSchedulingLink = sbPostData.toString();
				        updatedSchedulingLink=updatedSchedulingLink.replaceAll("\\s", "%20");
				        updatedSchedulingLink=updatedSchedulingLink.replaceAll("&", "%26");
				        updatedSchedulingLink=ServerAppUtility.getTinyUrl(updatedSchedulingLink,service.getCompanyId());
				        
				        logger.log(Level.SEVERE, "FINAL ZOHO SCHEDULING URL " + updatedSchedulingLink);
					}
					
//					String footer="";
//					String emailSubject="";
//					String header2="";
//					String header1="";
//					String str="";
//					String strcolor1=ServerAppUtility.getCss(header1);
//					ArrayList<String> overHeader=null;
//					ArrayList<String> dueHeader=null;
//					
//					if(!cronDetails.getFooter().equals("") ){
//						footer = cronDetails.getFooter();
//					}
//					emailSubject = "Services Schedule - "+service.getProductName()+" / "+service.getCount() ;
//					
//					if(!cronDetails.getSubject().trim().equals("")  && cronDetails.getSubject().trim().length() >0){
//						emailSubject = cronDetails.getSubject();
//					}
//					
//					if(!cronDetails.getMailBody().equals("")  && cronDetails.getMailBody().trim().length() >0){
//						header1 = header1 +"<BR>"+ cronDetails.getMailBody();
//					}
//					
//					/**
//					 * @author Anil @since 19-05-2021
//					 * Implementing Email template
//					 */
//					try{
						String dateTime=fmt.format(service.getServiceDate());
						if(service.getServiceTime()!=null&&!service.getServiceTime().equals("")&&!service.getServiceTime().equalsIgnoreCase("Flexible")){
							dateTime=dateTime+" "+service.getServiceTime();
						}
//						
//						header1=header1.replace("<Customer Name>,", service.getPersonInfo().getFullName()+","+ " <BR>"+ " <BR>");
//						header1=header1.replace("<service name>", service.getProductName());
//						header1=header1.replace("<date:time>", dateTime);
////						header1=header1.replace("<link>", "<a href="+updatedSchedulingLink+">"+updatedSchedulingLink+"</a>");
//						/**Added by sheetal : 09-12-2021
//						  Des : Replacing rescheduling with push button Reschedule Now**/
//						
//						header1=header1.replace("<link>", " <BR> <p align=\"center\"> <a href= "+updatedSchedulingLink +">"+"<button class=btn > Reschedule Now </button></a> </p>");
//						logger.log(Level.SEVERE, "Reschdule button" + header1 );
////						header1=header1+"<BR>"+"<BR>"+"<BR>";
//						header1=header1+ " ";
//						header1=header1+ strcolor1+"<BR>"+"<BR>"+"<BR>";
//					}catch(Exception e){
//						
//					}
//					
//					
////					footer = footer + " <BR>"+ updatedSchedulingLink;
//					
//					if(!header1.contains("Reschedule Now")){
////						footer = footer + " <BR>"+ "<a href="+updatedSchedulingLink+">"+updatedSchedulingLink+"</a>";
//						footer = footer + " <BR> <p align=\"center\"> <a href= "+updatedSchedulingLink +">"+"<button class=btn > Reschedule Now </button></a> </p>";
//						logger.log(Level.SEVERE, "footer"  + footer);
//                        footer = footer + strcolor1;
//					}
//						
//					cronEmail.cronSendEmailDueOverDue(toEmailList, emailSubject, header1, company,"", dueHeader, null,
//							header2, overHeader, null, null,null,null,null, "", footer);
					
					

					
					if(!cronDetails.getEmployeeRole().equalsIgnoreCase(AppConstants.CUSTOMER)){
						toEmailList.add(employee.getEmail());
					}
					else{
						Customer customer = ofy().load().type(Customer.class).filter("companyId",company.getCompanyId()).filter("count", service.getPersonInfo().getCount()).first().now();
						toEmailList.add(customer.getEmail());
					}
					logger.log(Level.SEVERE,"toEmailList1 "+toEmailList);

					
					try {   
						logger.log(Level.SEVERE,"Before send method call to send mail  ");	
						
						EmailTemplate emailTemplate = ofy().load().type(EmailTemplate.class).filter("templateName", cronDetails.getTemplateName())
										.filter("templateStatus", true).first().now();
						logger.log(Level.SEVERE,"emailTemplate "+emailTemplate);
						if(emailTemplate!=null){
							String servicedate = fmt1.format(service.getServiceDate());

							String mailSubject = emailTemplate.getSubject();
							if(mailSubject.contains("{Customer Name}")){
								mailSubject = mailSubject.replace("{Customer Name}", service.getPersonInfo().getFullName()+"");
							}
							if(mailSubject.contains("{ID}")){
								mailSubject = mailSubject.replace("{ID}",service.getCount()+" ");
							}
							if(mailSubject.contains("{Document Date}")){
								mailSubject = mailSubject.replace("{Document Date}",servicedate);
							}
							if(mailSubject.contains("{Service Name}")){
								mailSubject = mailSubject.replace("{Service Name}",service.getProductName());
							}
							if(mailSubject.contains("{Company Name>")){
								mailSubject = mailSubject.replace("{Company Name}",service.getProductName());
							}
							
							
							String emailbody = emailTemplate.getEmailBody();
							logger.log(Level.SEVERE,"Email body="+emailbody);
							emailbody = emailbody.replaceAll("[\n]", "<br>");
							

							if(emailbody.contains("{Customer Name}")){
								emailbody = emailbody.replace("{Customer Name}", service.getPersonInfo().getFullName()+"");
							}
							if(emailbody.contains("{ID}")){
								emailbody = emailbody.replace("{ID}",service.getCount()+" ");
							}
							if(emailbody.contains("{Document Date}")){
								emailbody = emailbody.replace("{Document Date}",servicedate);
							}
							if(emailbody.contains("{Service Name}")){
								emailbody = emailbody.replace("{Service Name}",service.getProductName());
							}
							if(emailbody.contains("{Company Name}")){
								emailbody = emailbody.replace("{Company Name}",service.getProductName());
							}
							if(emailbody.contains("{date:time}")){
								emailbody = emailbody.replace("{date:time}",dateTime);
							}
							if(emailbody.contains("{link}")){
								emailbody = emailbody.replace("{link}"," <BR> <p align=\"center\"> <a href= "+updatedSchedulingLink +">"+"<button class=btn > Reschedule Now </button></a> </p>");
							}
							
							
							if(!emailbody.contains("Reschedule Now")){
								emailbody = emailbody + " <BR> <p align=\"center\"> <a href= "+updatedSchedulingLink +">"+"<button class=btn > Reschedule Now </button></a> </p>";
							}
							
							if(emailbody.contains("{Company Signature}")) {
								emailbody=emailbody.replace("{Company Signature}", ServerAppUtility.getCompanySignature(company,null));
							}
							
							
							Email email = new Email();
							EmailDetails emaildetails = new EmailDetails();
							emaildetails.setToEmailId(toEmailList);
							emaildetails.setFromEmailid(company.getEmail());

							Branch branchEntity = ofy().load().type(Branch.class).filter("companyId", service.getCompanyId())
							.filter("buisnessUnitName", service.getBranch()).filter("status", true).first().now();
							ArrayList<String> ccEmailId = new ArrayList<String>();
							if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", service.getCompanyId()) && branchEntity!=null && branchEntity.getEmail()!=null && !branchEntity.getEmail().equals("")){
								ccEmailId.add(branchEntity.getEmail());
							}
							else{
								ccEmailId.add(branchEntity.getEmail());
							}
							emaildetails.setCcEmailId(ccEmailId);
							
							emaildetails.setSubject(mailSubject);
							emaildetails.setEmailBody(emailbody);
							emailbody = emailbody.replaceAll("[\\s]", "&nbsp;");

							email.sendEmailToReceipient(emaildetails, company.getCompanyId());

						}
						else{
							
							// for existing logic if template not exist
							
							ArrayList<String> toEmailList1 = new ArrayList<String>();
							toEmailList1.add(service.getPersonInfo().getEmail());
							
							String footer="";
							String emailSubject="";
							String header2="";
							String header1="";
							String str="";
							String strcolor1=ServerAppUtility.getCss(header1);
							ArrayList<String> overHeader=null;
							ArrayList<String> dueHeader=null;
							
							if(!cronDetails.getFooter().equals("") ){
								footer = cronDetails.getFooter();
							}
							emailSubject = "Services Schedule - "+service.getProductName()+" / "+service.getCount() ;
							
							if(!cronDetails.getSubject().trim().equals("")  && cronDetails.getSubject().trim().length() >0){
								emailSubject = cronDetails.getSubject();
							}
							
							if(!cronDetails.getMailBody().equals("")  && cronDetails.getMailBody().trim().length() >0){
								header1 = header1 +"<BR>"+ cronDetails.getMailBody();
							}
							
							/**
							 * @author Anil @since 19-05-2021
							 * Implementing Email template
							 */
							try{
								String dateTime2=fmt.format(service.getServiceDate());
								if(service.getServiceTime()!=null&&!service.getServiceTime().equals("")&&!service.getServiceTime().equalsIgnoreCase("Flexible")){
									dateTime2=dateTime2+" "+service.getServiceTime();
								}
								
								header1=header1.replace("<Customer Name>,", service.getPersonInfo().getFullName()+","+ " <BR>"+ " <BR>");
								header1=header1.replace("<service name>", service.getProductName());
								header1=header1.replace("<date:time>", dateTime2);
//								header1=header1.replace("<link>", "<a href="+updatedSchedulingLink+">"+updatedSchedulingLink+"</a>");
								/**Added by sheetal : 09-12-2021
								  Des : Replacing rescheduling with push button Reschedule Now**/
								
								header1=header1.replace("<link>", " <BR> <p align=\"center\"> <a href= "+updatedSchedulingLink +">"+"<button class=btn > Reschedule Now </button></a> </p>");
								logger.log(Level.SEVERE, "Reschdule button" + header1 );
//								header1=header1+"<BR>"+"<BR>"+"<BR>";
								header1=header1+ " ";
								header1=header1+ strcolor1+"<BR>"+"<BR>"+"<BR>";
							}catch(Exception e){
								
							}
							
							
//							footer = footer + " <BR>"+ updatedSchedulingLink;
							
							if(!header1.contains("Reschedule Now")){
//								footer = footer + " <BR>"+ "<a href="+updatedSchedulingLink+">"+updatedSchedulingLink+"</a>";
								footer = footer + " <BR> <p align=\"center\"> <a href= "+updatedSchedulingLink +">"+"<button class=btn > Reschedule Now </button></a> </p>";
								logger.log(Level.SEVERE, "footer"  + footer);
		                        footer = footer + strcolor1;
							}
								
							cronEmail.cronSendEmailDueOverDue(toEmailList1, emailSubject, header1, company,"", dueHeader, null,
									header2, overHeader, null, null,null,null,null, "", footer);
						}
						
						logger.log(Level.SEVERE,"after send method call to send mail  ");	
						
				} catch (Exception e1) {
					logger.log(Level.SEVERE,"In the catch block ");
					e1.printStackTrace();
				}
				
				
					
					logger.log(Level.SEVERE,"after send method call to send mail  ");	
					
			} catch (Exception e1) {
				logger.log(Level.SEVERE,"In the catch block ");
				e1.printStackTrace();
			}
			
			}
		}
		}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private void sendServiceSchedulingMessage(List<Service> serviceEntity,CronJobConfigrationDetails cronDetails, Company company,
		Long cellNumber) {
	
		String schedulingLink=company.getSchedulingUrl();
		int minimumDuration=company.getMinimumDuration();
		
		SmsTemplate smsEntity = null;
		if(cronDetails.getTemplateName()!=null && !cronDetails.getTemplateName().equals("")){
			smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId",company.getCompanyId())
					.filter("event", cronDetails.getTemplateName()).filter("status", true).first().now();
		}
		else{
			smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId",company.getCompanyId())
					.filter("event", "ServiceSchedulingByCustomer").filter("status", true).first().now();
		}
        logger.log(Level.SEVERE, "cronDetails.getTemplateName()" + cronDetails.getTemplateName());
        logger.log(Level.SEVERE, "smsEntity" + smsEntity);

		if (smsEntity != null) {
			String templateMsgWithBraces = smsEntity.getMessage();
			System.out.println("MSG:"+ templateMsgWithBraces);
			
			for (int l = 0; l < serviceEntity.size(); l++) {
				
				/**
				 * @author Vijay Date :- 09-08-2021
				 * Des :- added below method to check customer DND status if customer DND status is Active 
				 * then SMS will not send to customer
				 */
				ServerAppUtility serverapputility = new ServerAppUtility();
				boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(serviceEntity.get(l).getPersonInfo().getCount(), serviceEntity.get(l).getCompanyId());
		        logger.log(Level.SEVERE, "dndstatusFlag" + dndstatusFlag);
				if(!dndstatusFlag){
					
				String updatedSchedulingLink="";
				String smsTemplate=templateMsgWithBraces;
				if(serviceEntity.get(l).isServiceScheduled()==true){
					logger.log(Level.SEVERE, "Service already scheduled- "+serviceEntity.get(l).getPersonInfo().getFullName()+" / "+serviceEntity.get(l).getCount());
					continue;
				}
				if(serviceEntity.get(l).isScheduledByClient()==true){
					logger.log(Level.SEVERE, "Service scheduled by client- "+serviceEntity.get(l).getPersonInfo().getFullName()+" / "+serviceEntity.get(l).getCount());
					continue;
				}
				
				if(!validDuration(serviceEntity.get(l),minimumDuration)){
					continue;
				}
				
				if(schedulingLink!=null&&!schedulingLink.equals("")){
					logger.log(Level.SEVERE, "ZOHO SCHEDULING URL " + schedulingLink);
					StringBuilder sbPostData= new StringBuilder(schedulingLink);
			        sbPostData.append("?customerId="+serviceEntity.get(l).getPersonInfo().getCount());
			        sbPostData.append("&customerName="+serviceEntity.get(l).getPersonInfo().getFullName());
			        sbPostData.append("&serviceId="+serviceEntity.get(l).getCount());
			        sbPostData.append("&serviceDate="+fmt.format(serviceEntity.get(l).getServiceDate()));
			        sbPostData.append("&serviceName="+serviceEntity.get(l).getProductName());
			        sbPostData.append("&companyId="+serviceEntity.get(l).getCompanyId());
//					        sbPostData.append("&technicianName="+service.getEmployee());
//					        sbPostData.append("&branch="+service.getBranch());
			        updatedSchedulingLink = sbPostData.toString();
			        updatedSchedulingLink=updatedSchedulingLink.replaceAll("\\s", "%20");
			        updatedSchedulingLink=updatedSchedulingLink.replaceAll("&", "%26");
			        updatedSchedulingLink=ServerAppUtility.getTinyUrl(updatedSchedulingLink,serviceEntity.get(l).getCompanyId());
			        
			        logger.log(Level.SEVERE, "FINAL ZOHO SCHEDULING URL " + updatedSchedulingLink);
			        smsTemplate=smsTemplate.replace("{Link}", updatedSchedulingLink);
				}
				
				
				try{
					String dateTime=fmt.format(serviceEntity.get(l).getServiceDate());
					if(serviceEntity.get(l).getServiceTime()!=null&&!serviceEntity.get(l).getServiceTime().equals("")&&!serviceEntity.get(l).getServiceTime().equalsIgnoreCase("Flexible")){
						dateTime=dateTime+" "+serviceEntity.get(l).getServiceTime();
					}
					
					smsTemplate=smsTemplate.replace("<service name>", serviceEntity.get(l).getProductName());
					smsTemplate=smsTemplate.replace("<date:time>", dateTime);
					smsTemplate=smsTemplate.replace("<link>", updatedSchedulingLink);
				}catch(Exception e){
					
				}
				
				
				if(cronDetails.getEmployeeRole().equalsIgnoreCase(AppConstants.CUSTOMER)){
					cellNumber = serviceEntity.get(l).getPersonInfo().getCellNumber();
				}
				logger.log(Level.SEVERE,"cellNumber  "+cellNumber);

				String servicedate = fmt1.format(serviceEntity.get(l).getServiceDate());

				String customerName = smsTemplate.replace("{CustomerName}", serviceEntity.get(l).getPersonInfo().getFullName()+"");
				String serviceID = customerName.replace("{ServiceId}",serviceEntity.get(l).getCount()+" ");   
				String serviceDatemsg = serviceID.replace("{ServiceDate}",servicedate);
				String actualMsg = serviceDatemsg.replace("{companyName}", company.getBusinessUnitName());
				logger.log(Level.SEVERE,"actualMsg  "+actualMsg);

				/**
				 * @author Vijay Date :- 29-04-2022
				 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
				 * whats and sms based on communication channel configuration.
				 */
				if(cellNumber!=0){
					
					SmsServiceImpl smsimpl = new SmsServiceImpl();
					if(cronDetails.getCommunicationChannel().equals(AppConstants.SMS)){
						String smsResponse = smsimpl.sendSmsToClient(actualMsg, cellNumber+"", company.getCompanyId(), AppConstants.SMS);
						logger.log(Level.SEVERE,"for sending sms smsResponse"+smsResponse);
					}
					if(cronDetails.getCommunicationChannel().equals(AppConstants.WHATSAPP)){
						String response = smsimpl.sendMessageOnWhatsApp(company.getCompanyId(), cellNumber+"", actualMsg);
						logger.log(Level.SEVERE,"whats app response"+response);
					}
					
				}
				
				
//				sendsms(company.getCompanyId(),
//						company.getBusinessUnitName(),
//						serviceEntity.get(l).getCount(),
//						serviceEntity.get(l).getServiceDate(),
//						serviceEntity.get(l).getCellNumber(),
//						serviceEntity.get(l).getName(),
//						smsTemplate,
//						serviceEntity.get(l).getProductName(),smsEntity.getEvent());
//				serviceEntity.get(l).getPersonInfo().getEmail();
				}
			}
		
	}
		
		
	}

	public void serviceReminderToClient(Company company, List<CronJobConfigrationDetails> cronJobDetailList, HashSet<String> empRoleList) {
	
		
		try {
			
			
			for(String empRoleName :empRoleList){
				
				
				if(!empRoleName.equalsIgnoreCase(AppConstants.CUSTOMER)){
					
					List<Employee>  employeeList=new ArrayList<Employee>();
					employeeList=ofy().load().type(Employee.class).filter("companyId",company.getCompanyId()).filter("roleName", empRoleName).filter("status", true).list();
					logger.log(Level.SEVERE,""+empRoleName+" "+"Employee List size::: "+employeeList.size());
					List<String> branchList=new ArrayList<String>();
					
					for (Employee employee : employeeList) {

						branchList.add(employee.getBranchName());
						for (int j = 0; j < employee.getEmpBranchList().size(); j++) {
							branchList.add(employee.getEmpBranchList().get(j).getBranchName());
							logger.log(Level.SEVERE, "  -- branch list --"+ employee.getEmpBranchList().get(j).getBranchName());
						}
						logger.log(Level.SEVERE," Branch List SIze : "+branchList.size());
						
						if(branchList.size()!=0){
							serviceDueReminderToClient(cronJobDetailList,employee,branchList,company);
						}
					}
					
				}
				else{
					
					serviceDueReminderToClient(cronJobDetailList,null,null,company);

				}

				
			}

		} catch (Exception e2) {
			e2.printStackTrace();
		}
		
	}

	private void serviceDueReminderToClient(List<CronJobConfigrationDetails> cronJobDetailList,
			Employee employee, List<String> branchList, Company company) {
		
		try {
			
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
		
		Date today = DateUtility.getDateWithTimeZone("IST", new Date());
		
		ArrayList<String> obj = new ArrayList<String>();
		obj.add("Scheduled");
		obj.add("Pending");
		obj.add("Rescheduled");
		
		for (CronJobConfigrationDetails cronDetails:cronJobDetailList) {
			int day = 0;
			if (cronDetails.getOverdueDays() > 0&& cronDetails.isOverdueStatus()) {
				day = -cronDetails.getOverdueDays();
			} else {
				day = cronDetails.getInterval();
			}
		
			Calendar cal1 = Calendar.getInstance();
			cal1.setTime(today);
//			cal1.add(Calendar.DATE, +day);
	
			Date fromServiceDate = null;
			try {
				fromServiceDate = fmt1.parse(fmt1.format(cal1.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
	
			logger.log(Level.SEVERE,"fromServiceDate : "+ fromServiceDate);
	
			Calendar cal2 = Calendar.getInstance();
			cal2.setTime(today);
			cal2.add(Calendar.DATE, +day);
	
			Date toServiceDate = null;
	
			try {
				toServiceDate = fmt1.parse(fmt1.format(cal2.getTime()));
				cal2.set(Calendar.HOUR_OF_DAY, 23);
				cal2.set(Calendar.MINUTE, 59);
				cal2.set(Calendar.SECOND, 59);
				cal2.set(Calendar.MILLISECOND, 999);
				toServiceDate = cal2.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE, "toServiceDate : "+ toServiceDate);
			List<Service> serviceEntity=new ArrayList<Service>();
			
			if(branchList!=null && branchList.size()>0){
				if (cronDetails.getOverdueDays() > 0&& cronDetails.isOverdueStatus()) {
					serviceEntity = ofy().load().type(Service.class)
							.filter("companyId",company.getCompanyId())
							.filter("serviceDate >=",toServiceDate)
							.filter("serviceDate <=",fromServiceDate).filter("status IN", obj).filter("branch IN", branchList).list();
				}else{
					serviceEntity = ofy().load().type(Service.class)
						.filter("companyId",company.getCompanyId())
						.filter("serviceDate >=",fromServiceDate)
						.filter("serviceDate <=",toServiceDate).filter("status IN", obj).filter("branch IN", branchList).list();
				}
				
			}
			else{
				
				if (cronDetails.getOverdueDays() > 0&& cronDetails.isOverdueStatus()) {
					serviceEntity = ofy().load().type(Service.class)
							.filter("companyId",company.getCompanyId())
							.filter("serviceDate >=",toServiceDate)
							.filter("serviceDate <=",fromServiceDate).filter("status IN", obj).list();
				}else{
					serviceEntity = ofy().load().type(Service.class)
						.filter("companyId",company.getCompanyId())
						.filter("serviceDate >=",fromServiceDate)
						.filter("serviceDate <=",toServiceDate).filter("status IN", obj).list();
				}
				
			}
			
			
			if(serviceEntity==null||serviceEntity.size()==0){
				logger.log(Level.SEVERE,"No Services found for the given duration");
				return;
			}
			
			logger.log(Level.SEVERE,"serviceEntity ===="+ serviceEntity.size());
	
				
			boolean communicationChannelEmailFlag = false;
			if(cronDetails.getCommunicationChannel()!=null && !cronDetails.getCommunicationChannel().equals("")
					&& (cronDetails.getCommunicationChannel().equals(AppConstants.SMS) || cronDetails.getCommunicationChannel().equals(AppConstants.WHATSAPP) )){
				long cellNumber=0;
				if(!cronDetails.getEmployeeRole().equalsIgnoreCase(AppConstants.CUSTOMER)){
					cellNumber = employee.getCellNumber1();
				}
				sendServiceDueReminderMessage(serviceEntity,cronDetails,company,cellNumber);
			}
			if(cronDetails.getCommunicationChannel()==null || cronDetails.getCommunicationChannel().equals("")){
				communicationChannelEmailFlag = true;
			}
			if(cronDetails.getCommunicationChannel()!=null && !cronDetails.getCommunicationChannel().equals("")
					&&  cronDetails.getCommunicationChannel().equals(AppConstants.EMAIL)){
				communicationChannelEmailFlag = true;
			}
			logger.log(Level.SEVERE,"communicationChannelEmailFlag "+communicationChannelEmailFlag);
			
					
			if(communicationChannelEmailFlag){
			for (Service service:serviceEntity) {
				
				ArrayList<String> toEmailList = new ArrayList<String>();
				if(!cronDetails.getEmployeeRole().equalsIgnoreCase(AppConstants.CUSTOMER)){
					toEmailList.add(employee.getEmail());
				}
				else{
					Customer customer = ofy().load().type(Customer.class).filter("companyId",company.getCompanyId()).filter("count", service.getPersonInfo().getCount()).first().now();
					toEmailList.add(customer.getEmail());
				}
				logger.log(Level.SEVERE,"toEmailList1 "+toEmailList);

				
				try {   
					logger.log(Level.SEVERE,"Before send method call to send mail  ");	
					
					EmailTemplate emailTemplate = ofy().load().type(EmailTemplate.class).filter("templateName", cronDetails.getTemplateName())
									.filter("templateStatus", true).first().now();
					logger.log(Level.SEVERE,"emailTemplate "+emailTemplate);
					if(emailTemplate!=null){
						String servicedate = fmt1.format(service.getServiceDate());

						String mailSubject = emailTemplate.getSubject();
						if(mailSubject.contains("{Customer Name}")){
							mailSubject = mailSubject.replace("{Customer Name}", service.getPersonInfo().getFullName()+"");
						}
						if(mailSubject.contains("{ID}")){
							mailSubject = mailSubject.replace("{ID}",service.getCount()+" ");
						}
						if(mailSubject.contains("{Document Date}")){
							mailSubject = mailSubject.replace("{Document Date}",servicedate);
						}
						if(mailSubject.contains("{Service Name}")){
							mailSubject = mailSubject.replace("{Service Name}",service.getProductName());
						}
						if(mailSubject.contains("{Company Name}")){
							mailSubject = mailSubject.replace("{Company Name}",service.getProductName());
						}
						
						
						String emailbody = emailTemplate.getEmailBody();
						logger.log(Level.SEVERE,"Email body="+emailbody);
						emailbody = emailbody.replaceAll("[\n]", "<br>");
						

						if(emailbody.contains("{Customer Name}")){
							emailbody = emailbody.replace("{Customer Name}", service.getPersonInfo().getFullName()+"");
						}
						if(emailbody.contains("{ID}")){
							emailbody = emailbody.replace("{ID}",service.getCount()+" ");
						}
						if(emailbody.contains("{Document Date}")){
							emailbody = emailbody.replace("{Document Date}",servicedate);
						}
						if(emailbody.contains("{Service Name}")){
							emailbody = emailbody.replace("{Service Name}",service.getProductName());
						}
						if(emailbody.contains("{Company Name}")){
							emailbody = emailbody.replace("{Company Name}",service.getProductName());
						}
						
						if(emailbody.contains("{Company Signature}")) {
							emailbody=emailbody.replace("{Company Signature}", ServerAppUtility.getCompanySignature(company,null));
						}
						
						Email email = new Email();
						EmailDetails emaildetails = new EmailDetails();
						emaildetails.setToEmailId(toEmailList);
						emaildetails.setFromEmailid(company.getEmail());
						
						
						Branch branchEntity = ofy().load().type(Branch.class).filter("companyId", service.getCompanyId())
						.filter("buisnessUnitName", service.getBranch()).filter("status", true).first().now();
						ArrayList<String> ccEmailId = new ArrayList<String>();
						if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", service.getCompanyId()) && branchEntity!=null && branchEntity.getEmail()!=null && !branchEntity.getEmail().equals("")){
							ccEmailId.add(branchEntity.getEmail());
						}
						else{
							ccEmailId.add(branchEntity.getEmail());
						}
						emaildetails.setCcEmailId(ccEmailId);
						
						emaildetails.setSubject(mailSubject);
						emaildetails.setEmailBody(emailbody);
						emailbody = emailbody.replaceAll("[\\s]", "&nbsp;");

						email.sendEmailToReceipient(emaildetails, company.getCompanyId());

					}
					
					logger.log(Level.SEVERE,"after send method call to send mail  ");	
					
			} catch (Exception e1) {
				logger.log(Level.SEVERE,"In the catch block ");
				e1.printStackTrace();
			}
			
			}
		}
		}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendServiceDueReminderMessage(List<Service> serviceEntity,CronJobConfigrationDetails cronDetails, Company company,
			Long cellNumber) {

		SmsTemplate smsEntity = null;
		if(cronDetails.getTemplateName()!=null && !cronDetails.getTemplateName().equals("")){
			smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId",company.getCompanyId()).filter("event",cronDetails.getTemplateName()).filter("status",true).first().now();
		}
		else{
			smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId",company.getCompanyId()).filter("event","Service Due Cron Job").filter("status",true).first().now();
		}
		logger.log(Level.SEVERE,"smsEntity"+smsEntity);
		if(smsEntity!=null){
			
			for(Service service : serviceEntity){
				
				String companyName = company.getBusinessUnitName();
				String servicedate = fmt1.format(service.getServiceDate());

				/**
				 * Date 29 jun 2017 added by vijay for Eco friendly
				 */
				
				ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","SMS").filter("status",true).filter("companyId", company.getCompanyId()).first().now();
				ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", company.getCompanyId()).filter("processName","SMS").first().now();				
					
				boolean prossconfigBhashSMSflag = false;
				if(processName!=null){
				if(processConfig!=null){
					if(processConfig.isConfigStatus()){
						for(int l=0;l<processConfig.getProcessList().size();l++){
							if(processConfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("SMS") && processConfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("BHASHSMSAPI") && processConfig.getProcessList().get(l).isStatus()==true){
								prossconfigBhashSMSflag=true;
							}
						}
				   }
				}
				}
				logger.log(Level.SEVERE,"process config  BHASH SMS Flag =="+prossconfigBhashSMSflag);
				
				String actualMsg="";
				String custName = service.getPersonInfo().getFullName();
				if(prossconfigBhashSMSflag){
					custName = getCustomerName(custName,company.getCompanyId());
					
					String customerName = smsEntity.getMessage().replace("{CustomerName}", custName+"");
					String serviceDatemsg = customerName.replace("{ServiceDate}",servicedate);
					 actualMsg = serviceDatemsg.replace("{companyName}", companyName);

				}
				/**
				 * ends here
				 * else for bhavana api for ours standard sms template
				 */
				else{
					
					String customerName = smsEntity.getMessage().replace("{CustomerName}", custName+"");
					/**
					 * Date 26-11-2020 added by Priyanka-As per requirement add ProductName in sms template insted of ServiceID.
					 */
//					String serviceID = customerName.replace("{ServiceId}",prodName+" ");   
					/**
					 * end here.
					 */
					String serviceID = customerName.replace("{ServiceId}",service.getCount()+"");   

					String serviceDatemsg = serviceID.replace("{ServiceDate}",servicedate);
					actualMsg = serviceDatemsg.replace("{companyName}", companyName);
				}
				System.out.println("actualMsg"+actualMsg);
				logger.log(Level.SEVERE,"Actual MSG:"+actualMsg);	
				
				if(cronDetails.getEmployeeRole().equalsIgnoreCase(AppConstants.CUSTOMER)){
					cellNumber = service.getPersonInfo().getCellNumber();
				}
				logger.log(Level.SEVERE," cellNumber  "+cellNumber);

				
				/**
				 * @author Vijay Date :- 09-08-2021
				 * Des :- added below method to check customer DND status if customer DND status is Active 
				 * then SMS will not send to customer
				 */
				ServerAppUtility serverapputility = new ServerAppUtility();
				boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(service.getPersonInfo().getCount(), service.getCompanyId());
				
				if(!dndstatusFlag && cellNumber!=0){
					SmsServiceImpl smsimpl = new SmsServiceImpl();
					logger.log(Level.SEVERE," cronDetails.getCommunicationChannel()  "+cronDetails.getCommunicationChannel());

					if(cronDetails.getCommunicationChannel().equals(AppConstants.SMS)){
						String smsResponse = smsimpl.sendSmsToClient(actualMsg, cellNumber+"", company.getCompanyId(), AppConstants.SMS);
						logger.log(Level.SEVERE,"for sending sms smsResponse"+smsResponse);
					}
					if(cronDetails.getCommunicationChannel().equals(AppConstants.WHATSAPP)){
						String response = smsimpl.sendMessageOnWhatsApp(company.getCompanyId(), cellNumber+"", actualMsg);
						logger.log(Level.SEVERE,"whats app response"+response);
					}
				}
			
			  }
		}
		
	}

}
