package com.slicktechnologies.server.cronjobimpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.factory.SmsFactory;
import com.twilio.sdk.resource.instance.Account;
import com.google.gson.Gson;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.smshistory.SmsHistory;
import com.twilio.sdk.TwilioRestClient;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class CustomerPaymentARCronJobImpl extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1352789385430511259L;
	
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private DateFormat fmtPaymentDate = new SimpleDateFormat("dd/MM/yyyy");
	 Logger logger = Logger.getLogger("NameOfYourLogger");
	 

	  
		public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws IOException {
			/**
			 *  nidhi
			 *  1-03-2018
			 *   for cron job confgration
			 */
//				custPaymentARlist();
		}

		private void custPaymentARlist() {

			Email cronEmail = new Email();
			
			Date today=DateUtility.getDateWithTimeZone("IST", new Date());
			
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			fmtPaymentDate.setTimeZone(TimeZone.getTimeZone("IST"));
			
			/**
			 *    i have added time in today's date
			 */
			
			Calendar cal=Calendar.getInstance();
			cal.setTime(today);
			cal.add(Calendar.DATE, 0);
			
			Date dateForFilter=null;
			
			try {
				dateForFilter=fmtPaymentDate.parse(fmtPaymentDate.format(cal.getTime()));
				cal.set(Calendar.HOUR_OF_DAY,23);
				cal.set(Calendar.MINUTE,59);
				cal.set(Calendar.SECOND,59);
				cal.set(Calendar.MILLISECOND,999);
				dateForFilter=cal.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Date After setting the Time"+dateForFilter);
			
			/************************************* End *********************************/
			
			
	 try{
		 
		 logger.log(Level.SEVERE,"In Payment AR List");	
		logger.log(Level.SEVERE,"Date After Adding One Date"+dateForFilter);

		 
	/********************************Adding status in the list ***********************/
		 
		 ArrayList<String> obj = new ArrayList<String>();
		 obj.add("Created");
		 
	/******************************Converting todayDate to String format*****************/
		 
		 Date todaysDate = new Date();
		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 String todayDateString = df.format(todaysDate);
		 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);

		 
 /********************************Adding Companies in the list ***********************/
		 
	List<Company> compEntity = ofy().load().type(Company.class).list();
	if(compEntity.size()>0){
	
		logger.log(Level.SEVERE,"If compEntity size > 0");
		logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());

		for(int i=0;i<compEntity.size();i++){
			
			Company c=compEntity.get(i);
			logger.log(Level.SEVERE,"In the for loop");
			logger.log(Level.SEVERE,"Company Name="+c);
			logger.log(Level.SEVERE,"The value of i is:" +i);
			logger.log(Level.SEVERE,"Date After Adding One Date"+dateForFilter);

	 /****************************************** For SMS *********************************/		
	
//			SmsConfiguration smsconfig = ofy().load().type(SmsConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("status",true).first().now();
//			if(smsconfig!=null){
//				
//				String accountSid = smsconfig.getAccountSID();
//				String authotoken = smsconfig.getAuthToken();
//				String frmnumber = smsconfig.getPassword();
//			
				ProcessName smsprocessName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
				ProcessConfiguration smsprocessconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
					
				boolean prossconfigSMSflag = false;
				if(smsprocessconfig!=null){
					if(smsprocessconfig.isConfigStatus()){
						for(int l=0;l<smsprocessconfig.getProcessList().size();l++){
							if(smsprocessconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && smsprocessconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("CustomerPaymentARSMS") && smsprocessconfig.getProcessList().get(l).isStatus()==true){
								prossconfigSMSflag=true;
							}
						}
				   }
				}
				logger.log(Level.SEVERE,"process config  SMS Flag =="+prossconfigSMSflag);

//				/**
//				 * Date 16 jun 2017 added by vijay
//				 * below process config is used for calling BHASH SMS API 
//				 */
//				ProcessConfiguration smsprocessconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","SMS").first().now();				
//				
//				boolean bhashSMSAPIFlag = false;
//				if(smsprocessconfig!=null){
//					if(smsprocessconfig.isConfigStatus()){
//						for(int l=0;l<smsprocessconfig.getProcessList().size();l++){
//							if(smsprocessconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("SMS") && smsprocessconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("BhashSMSAPI") && smsprocessconfig.getProcessList().get(l).isStatus()==true){
//								bhashSMSAPIFlag=true;
//							}
//						}
//				   }
//				}
//				
//				logger.log(Level.SEVERE,"process config BHASH SMS API Flag =="+bhashSMSAPIFlag);
//				/**
//				 * ends here
//				 */
				
			if(smsprocessName!=null){
				logger.log(Level.SEVERE,"In the prossName for ======= SMS ");

				if(prossconfigSMSflag){
					
					logger.log(Level.SEVERE,"In the ProsConfifg !=null ======= for SMS ");
					System.out.println("In the ProsConfifg !=null ");
					
					SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("event","Amount Receivable Due Cron Job").filter("status", true).first().now();
					System.out.println("smsEntity"+smsEntity);
					logger.log(Level.SEVERE,"smsEntity ======= SMS "+smsEntity);

					if(smsEntity!=null){
						
						int day = smsEntity.getDay(); 
						
						String templateMsgwithBraces = smsEntity.getMessage();
						System.out.println("Template Msg========:"+templateMsgwithBraces);
						
						System.out.println("Day"+day);
			
						
						Calendar cal3 = Calendar.getInstance();
						cal3.setTime(today);
						cal3.add(Calendar.DATE, +day);
						
						Date dateforsmsfilter = null;
						
						try {
							
							dateforsmsfilter = fmtPaymentDate.parse(fmtPaymentDate.format(cal3.getTime()));
							
						} catch (ParseException e) {
							e.printStackTrace();
						}
						
						logger.log(Level.SEVERE,"Date for SMS 1"+dateforsmsfilter);
						System.out.println("dateforsmsfilter2"+dateforsmsfilter);

						Calendar cal2 = Calendar.getInstance();
						cal2.setTime(today);
						cal2.add(Calendar.DATE, +day);
						
						Date dateforsmsfilter2 = null;
						
						try {
							
							dateforsmsfilter2 = fmtPaymentDate.parse(fmtPaymentDate.format(cal2.getTime()));
							cal2.set(Calendar.HOUR_OF_DAY,23);
							cal2.set(Calendar.MINUTE,59);
							cal2.set(Calendar.SECOND,59);
							cal2.set(Calendar.MILLISECOND,999);
							dateforsmsfilter2=cal2.getTime();
							
						} catch (ParseException e) {
							e.printStackTrace();
						}
						
						logger.log(Level.SEVERE,"Date for SMS 1"+dateforsmsfilter2);
						System.out.println("dateforsmsfilter2"+dateforsmsfilter2);
						
						
						List<CustomerPayment> customerpayment = ofy().load().type(CustomerPayment.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("paymentDate >=", dateforsmsfilter).filter("paymentDate <=", dateforsmsfilter2).filter("status", "Created").filter("accountType", "AR").list();
						logger.log(Level.SEVERE,"customerpayment=========="+customerpayment.size());

						for(int l=0;l<customerpayment.size();l++){
							
							sendSMS(compEntity.get(i).getCompanyId(),customerpayment.get(l).getCount(),customerpayment.get(l).getName(),customerpayment.get(l).getPaymentAmt(),customerpayment.get(l).getPaymentDate(),customerpayment.get(l).getCellNumber(),compEntity.get(i).getBusinessUnitName(),templateMsgwithBraces,smsEntity.getEvent() );
						}
					}
				}
			}
			
//		 }
			
	/****************************************     SMS End     ***********************************************/		

			
			
 /********************************Checking prosname & prosconfig for each company ***********************/
		
			ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
			ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
			
			boolean processconfigflag = false;
			if(processconfig!=null){
				if(processconfig.isConfigStatus()){
					for(int l=0;l<processconfig.getProcessList().size();l++){
						if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("PaymentARDailyEmail") && processconfig.getProcessList().get(l).isStatus()==true){
							processconfigflag=true;
						}
					}
			   }
			}
			logger.log(Level.SEVERE,"process config  SMS Flag =="+processconfigflag);
			
			if(processName!=null){
				logger.log(Level.SEVERE,"In the prossName");
			if(processconfigflag){
							
				logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
				
				
	/********************************Reading PaymentList from CustomerPayment entity  ***********************/
				
				List<CustomerPayment> custPaymentEntity = ofy().load().type(CustomerPayment.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("paymentDate <=",dateForFilter).filter("status IN",obj).filter("accountType", AppConstants.BILLINGACCOUNTTYPEAR).list();
				
				logger.log(Level.SEVERE,"Today date Is:"+dateForFilter);	

				logger.log(Level.SEVERE,"Paymnet entity size:"+custPaymentEntity.size());	
				
				
				// email id is added to emailList
				ArrayList<String> toEmailList=new ArrayList<String>();
				toEmailList.add(compEntity.get(i).getPocEmail());
				
				 String mailTitl = "Payment Due As On Date";
				
				if(custPaymentEntity.size()>0){
				
			
				ArrayList<String> tbl_header = new ArrayList<String>();
				tbl_header.add("Serial No");
				tbl_header.add("Branch");
				tbl_header.add("Customer Id");
				tbl_header.add("Customer Name");
				tbl_header.add("Customer Cont No");
				tbl_header.add("POC Name");
				tbl_header.add("Email Id");
				tbl_header.add("Sales Person");
				tbl_header.add("Person Responsible");
				tbl_header.add("OrderId");
				tbl_header.add("Invoice Id");
				tbl_header.add("Invoice Date");
				tbl_header.add("Payment Doc Id");
				tbl_header.add("Payment Amt");
				tbl_header.add("Payment Date");
				tbl_header.add("Status");
				tbl_header.add("Ageing");
				
/********************************Sorting table with Branch & Payment Date ***********************/
			
				
				Comparator<CustomerPayment> custpaymentDateComparator2 = new Comparator<CustomerPayment>() {
					public int compare(CustomerPayment s1, CustomerPayment s2) {
					
					Date date1 = s1.getPaymentDate();
					Date date2 = s2.getPaymentDate();
					
					//ascending order
					return date1.compareTo(date2);
					}
					};
					Collections.sort(custPaymentEntity, custpaymentDateComparator2);
					
					Comparator<CustomerPayment> custpaymentDateComparator = new Comparator<CustomerPayment>() {
						public int compare(CustomerPayment s1, CustomerPayment s2) {
						
						String branch1 = s1.getBranch();
						String branch2 = s2.getBranch();
						
						//ascending order
						return branch1.compareTo(branch2);
						}
						
						};
						Collections.sort(custPaymentEntity, custpaymentDateComparator);
					
/********************************Getting PaymnetEntity data and adding in the tbl1 List ***********************/
							
				ArrayList<String> tbl1=new ArrayList<String>();
					
				for(int j=0;j<custPaymentEntity.size();j++){
					
					Customer customerEntity=ofy().load().type(Customer.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("count", custPaymentEntity.get(j).getPersonInfo().getCount()).first().now();
					
					tbl1.add((j+1)+"");
		   	 		tbl1.add(custPaymentEntity.get(j).getBranch()+"");
		   	 		tbl1.add(custPaymentEntity.get(j).getPersonInfo().getCount()+"");
		   	 		tbl1.add(custPaymentEntity.get(j).getPersonInfo().getFullName()+"");		   	 		
		   	 		tbl1.add(custPaymentEntity.get(j).getPersonInfo().getCellNumber()+"");
		   	 		tbl1.add(custPaymentEntity.get(j).getPersonInfo().getPocName());
		   	 		tbl1.add(customerEntity.getEmail());
		   	 		tbl1.add(custPaymentEntity.get(j).getEmployee());
		   	 		tbl1.add(custPaymentEntity.get(j).getEmployee());
		   	 		tbl1.add(custPaymentEntity.get(j).getContractCount()+"");
		   	 		tbl1.add(custPaymentEntity.get(j).getInvoiceCount()+"");
		   	 		tbl1.add(fmt.format(custPaymentEntity.get(j).getInvoiceDate())+""); 	 		
		   	 		tbl1.add(custPaymentEntity.get(j).getCount()+"");
		   	 	    tbl1.add(custPaymentEntity.get(j).getPaymentAmt()+""); 
		   	 	    tbl1.add(fmt.format(custPaymentEntity.get(j).getPaymentDate()) +"");
		 
		   	 	    /**************** for getting ageing for each Payment ******/
		   	 	    String StringPaymnetDate = fmtPaymentDate.format(custPaymentEntity.get(j).getPaymentDate());
		   	 	    
//		   	 	   	String StringPaymnetDate = df.format(paymentDate);	
		   			
		   				Date d1 = null;
		   				Date d2 = null;
		   				
		   				d1 = df.parse(StringPaymnetDate);
		   				d2 = df.parse(todayDateString);
		   				long diff = d2.getTime() - d1.getTime();
						long diffDays = diff / (24 * 60 * 60 * 1000);
						
//						System.out.println("Ageing:"+diffDays);
//				   	 	logger.log(Level.SEVERE,"Ageing:"+diffDays);
			
		   	 	    tbl1.add(custPaymentEntity.get(j).getStatus()+"");
		   	 	    tbl1.add(diffDays +"");
		   	 	}			
				
				logger.log(Level.SEVERE,"In the ProsConfig !=null   3 ");
				
   /********************************Calling cronsendEmail Method ***********************/
				
				try {   
					   
						System.out.println("Before send mail method");
						logger.log(Level.SEVERE,"Before send method call to send mail  ");						
						cronEmail.cronSendEmail(toEmailList, "Payment Due As On Date"+" "+ todayDateString, mailTitl +"  "+ todayDateString, c, tbl_header, tbl1, null, null, null, null);
						logger.log(Level.SEVERE,"After send mail method ");		
								//+"  "+
				} catch (IOException e1) {
					
					logger.log(Level.SEVERE,"In the catch block ");
					e1.printStackTrace();
			}
				
			}
			else{					
					
				System.out.println("Sorry no Payment found for this company");
				logger.log(Level.SEVERE,"Sorry no Payment found for this company");

				mailTitl = "No Payment Found Due";

				cronEmail.cronSendEmail(toEmailList, "Payment Due As On Date"+" "+ todayDateString, mailTitl , c, null, null, null, null, null, null);

			}
				
				
			}
			else{					//else block for prosconfig
					logger.log(Level.SEVERE,"ProcessConfiguration is null");
					System.out.println("ProcessConfiguration is null");
			}
			
			}else{        //else block for pross!=null
				logger.log(Level.SEVERE,"Cron job status is Inactive");
				System.out.println("Cron job status is Inactive");
			}
			
							
		}	//end of for loop
		
	}
	else{			//else block for if(compEntity.size()>0)
		
		logger.log(Level.SEVERE,"No Company found from cron job completed");	
		System.out.println("No Company found from cron job completed");
	}
	
	
	 }catch(Exception e2){
	
	e2.printStackTrace();
	 }
		
			
				
		}
		
		
		
		
/***************************************  SMS Method         *************************************/
		
		
		private void sendSMS(long companyId,int paymentId,String name, int paymentAmt, Date paymentDate,Long cellNo, String businessUnitName,
				String templateMsgwithBraces, String templateName) 
		{
			

			System.out.println("Customer Name"+name);
			System.out.println("payable Amt:"+paymentAmt);
			System.out.println("date"+paymentDate);
			System.out.println("Cell No"+cellNo);
			System.out.println("templateMsg:"+templateMsgwithBraces);
			
			

			String paymentdate = fmtPaymentDate.format(paymentDate);
			String companyName = businessUnitName;
			
			/**
			 * Date 29 jun 2017 added by vijay for Eco friendly
			 */
			
			ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","SMS").filter("status",true).filter("companyId", companyId).first().now();
			ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId).filter("processName","SMS").first().now();				
				
			boolean prossconfigBhashSMSflag = false;
			if(processName!=null){
			if(processConfig!=null){
				if(processConfig.isConfigStatus()){
					for(int l=0;l<processConfig.getProcessList().size();l++){
						if(processConfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("SMS") && processConfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("BHASHSMSAPI") && processConfig.getProcessList().get(l).isStatus()==true){
							prossconfigBhashSMSflag=true;
						}
					}
			   }
			}
			}
			logger.log(Level.SEVERE,"process config  BHASH SMS Flag =="+prossconfigBhashSMSflag);
			
			if(prossconfigBhashSMSflag){
				name = getCustomerName(name,companyId);
			}
			/**
			 * ends here
			 */
			String customerName = templateMsgwithBraces.replace("{CustomerName}",name +"" );
			String paymentAmount = customerName.replace("{PayableAmt}",paymentAmt +"" );
			String paymentDatemsg = paymentAmount.replace("{PaymentDate}", paymentdate );
			String actualMsg = paymentDatemsg.replace("{companyName}", companyName);
			
			SmsServiceImpl smsimpl = new SmsServiceImpl();
//			int value =  smsimpl.sendSmsToClient(actualMsg, cellNo, companyId);
//			saveSmsHistory(companyId,paymentId,actualMsg,name,cellNo);
			
			/**
			 * @author Vijay Date :- 29-04-2022
			 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
			 * whats and sms based on communication channel configuration.
			 */
			smsimpl.sendMessage(AppConstants.CRONJOB,AppConstants.CRONJOB,templateName,companyId,actualMsg,cellNo,false);
			logger.log(Level.SEVERE,"after sendMessage method");
			
			
		}

		private void saveSmsHistory(long companyId, int paymentId, String actualMsg, String name,Long cellNo) {
			
			SmsHistory smshistory = new SmsHistory();
			smshistory.setDocumentId(paymentId);
			smshistory.setDocumentType("Customer Payment Due");
			smshistory.setSmsText(actualMsg);
			smshistory.setName(name);
			smshistory.setCellNo(cellNo);
			smshistory.setUserId("Cron Job");
			smshistory.setCompanyId(companyId);
			
			logger.log(Level.SEVERE,"Data 22222222222222222");

			ofy().save().entity(smshistory).now();

			logger.log(Level.SEVERE,"Data Saved Successfully========");

	
}	
		
		/**
		 * Date 29 jun 2017 added by vijay for gtetting customer correspondence Name or full name
		 * @param customerName
		 * @param companyId
		 * @return
		 */
		
		private String getCustomerName(String customerName, long companyId) {
			
			String custName;
			
			Customer customer = ofy().load().type(Customer.class).filter("companyId", companyId).filter("fullname", customerName).first().now();
			
			if(customer!=null){
				
				if(!customer.getCustPrintableName().equals("")){
					custName = customer.getCustPrintableName();
				}else{
					custName = customer.getFullname();
				}
				
			}else{
				custName = customerName;
			}
			
			
			return custName;
		}
		
		/**
		 * ends here
		 */
		
		/********************************************** SMS Method End here ***************************************/

		/**
		 *  nidhi
		 *  1-03-2018
		 *  cronjob configration
		 */
		public void custPaymentARlist(String cronList) {

			Email cronEmail = new Email();
			
			Date today=DateUtility.getDateWithTimeZone("IST", new Date());
			
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			fmtPaymentDate.setTimeZone(TimeZone.getTimeZone("IST"));
			
			/**
			 *    i have added time in today's date
			 */
			
			
			/************************************* End *********************************/
			
			
	 try{
		 
		 logger.log(Level.SEVERE,"In Payment AR List");	
//		logger.log(Level.SEVERE,"Date After Adding One Date"+dateForFilter);

		 
	/********************************Adding status in the list ***********************/
		 
		 ArrayList<String> obj = new ArrayList<String>();
		 obj.add("Created");
		 
	/******************************Converting todayDate to String format*****************/
		 
		 Date todaysDate = new Date();
		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 String todayDateString = df.format(todaysDate);
		 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);

		 
 /********************************Adding Companies in the list ***********************/
		 
	List<Company> compEntity = ofy().load().type(Company.class).list();
	if(compEntity.size()>0){
	
		logger.log(Level.SEVERE,"If compEntity size > 0");
		logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());

		for(int i=0;i<compEntity.size();i++){
			
			Company c=compEntity.get(i);
			logger.log(Level.SEVERE,"In the for loop");
			logger.log(Level.SEVERE,"Company Name="+c);
			logger.log(Level.SEVERE,"The value of i is:" +i);
//			logger.log(Level.SEVERE,"Date After Adding One Date"+dateForFilter);

	 /****************************************** For SMS *********************************/		
//	
//			SmsConfiguration smsconfig = ofy().load().type(SmsConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("status",true).first().now();
//			if(smsconfig!=null){
//				
//				String accountSid = smsconfig.getAccountSID();
//				String authotoken = smsconfig.getAuthToken();
//				String frmnumber = smsconfig.getPassword();
//			
//				ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
//				ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
//					
//				boolean prossconfigSMSflag = false;
//				if(processconfig!=null){
//					if(processconfig.isConfigStatus()){
//						for(int l=0;l<processconfig.getProcessList().size();l++){
//							if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("CustomerPaymentARSMS") && processconfig.getProcessList().get(l).isStatus()==true){
//								prossconfigSMSflag=true;
//							}
//						}
//				   }
//				}
//				logger.log(Level.SEVERE,"process config  SMS Flag =="+prossconfigSMSflag);
//
//				
//			if(processName!=null)
//			{
//				logger.log(Level.SEVERE,"In the prossName for ======= SMS ");
//
//				if(prossconfigSMSflag){
//					
//					logger.log(Level.SEVERE,"In the ProsConfifg !=null ======= for SMS ");
//					System.out.println("In the ProsConfifg !=null ");
//					
//					SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId", compEntity.get(i)
//							.getCompanyId()).filter("event","Amount Receivable Due Cron Job").filter("status", true).first().now();
//					System.out.println("smsEntity"+smsEntity);
//					logger.log(Level.SEVERE,"smsEntity ======= SMS "+smsEntity);
//
//					if(smsEntity!=null){
//						
//						int day = smsEntity.getDay(); 
//						
//						String templateMsgwithBraces = smsEntity.getMessage();
//						System.out.println("Template Msg========:"+templateMsgwithBraces);
//						
//						System.out.println("Day"+day);
//			
//						
//						Calendar cal3 = Calendar.getInstance();
//						cal3.setTime(today);
//						cal3.add(Calendar.DATE, +day);
//						
//						Date dateforsmsfilter = null;
//						
//						try {
//							
//							dateforsmsfilter = fmtPaymentDate.parse(fmtPaymentDate.format(cal3.getTime()));
//							
//						} catch (ParseException e) {
//							e.printStackTrace();
//						}
//						
//						logger.log(Level.SEVERE,"Date for SMS 1"+dateforsmsfilter);
//						System.out.println("dateforsmsfilter2"+dateforsmsfilter);
//
//						Calendar cal2 = Calendar.getInstance();
//						cal2.setTime(today);
//						cal2.add(Calendar.DATE, +day);
//						
//						Date dateforsmsfilter2 = null;
//						
//						try {
//							
//							dateforsmsfilter2 = fmtPaymentDate.parse(fmtPaymentDate.format(cal2.getTime()));
//							cal2.set(Calendar.HOUR_OF_DAY,23);
//							cal2.set(Calendar.MINUTE,59);
//							cal2.set(Calendar.SECOND,59);
//							cal2.set(Calendar.MILLISECOND,999);
//							dateforsmsfilter2=cal2.getTime();
//							
//						} catch (ParseException e) {
//							e.printStackTrace();
//						}
//						
//						logger.log(Level.SEVERE,"Date for SMS 1"+dateforsmsfilter2);
//						System.out.println("dateforsmsfilter2"+dateforsmsfilter2);
//						
//						
//						List<CustomerPayment> customerpayment = ofy().load().type(CustomerPayment.class)
//								.filter("companyId", compEntity.get(i).getCompanyId())
//								.filter("paymentDate >=", dateforsmsfilter)
//								.filter("paymentDate <=", dateforsmsfilter2).filter("status", "Created").filter("accountType", "AR").list();
//						logger.log(Level.SEVERE,"customerpayment=========="+customerpayment.size());
//
//						for(int l=0;l<customerpayment.size();l++){
//							
//							/**
//							 * @author Vijay Date :- 09-08-2021
//							 * Des :- added below method to check customer DND status if customer DND status is Active 
//							 * then SMS will not send to customer
//							 */
//							ServerAppUtility serverapputility = new ServerAppUtility();
//							boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(customerpayment.get(l).getPersonInfo().getCount(), customerpayment.get(l).getCompanyId());
//							if(!dndstatusFlag){
//								sendSMS(compEntity.get(i).getCompanyId(),customerpayment.get(l).getCount(),customerpayment.get(l).getName(),customerpayment.get(l).getPaymentAmt(),customerpayment.get(l).getPaymentDate(),customerpayment.get(l).getCellNumber(),compEntity.get(i).getBusinessUnitName(),templateMsgwithBraces,accountSid,authotoken,frmnumber );
//							}
//							
//						}
//					}
//				}
//			}
//			
//		 }
			
	/****************************************     SMS End     ***********************************************/		

			
			
 /********************************Checking prosname & prosconfig for each company ***********************/
		
//			ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
//			ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
//			
//			boolean processconfigflag = false;
//			if(processconfig!=null){
//				if(processconfig.isConfigStatus()){
//					for(int l=0;l<processconfig.getProcessList().size();l++){
//						if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("PaymentARDailyEmail") && processconfig.getProcessList().get(l).isStatus()==true){
//							processconfigflag=true;
//						}
//					}
//			   }
//			}
//			logger.log(Level.SEVERE,"process config  SMS Flag =="+processconfigflag);
							
				logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
				
				
				Gson gson = new Gson();
				JSONArray jsonarr = null;
				try {
					jsonarr=new JSONArray(cronList.trim());
				} catch (JSONException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				
//				try
				{
					CronJobConfigrationDetails cronDetails =null;
					for(int k=0;k<jsonarr.length();k++){
						logger.log(Level.SEVERE,"JSON ARRAY LENGTH :: "+jsonarr.length());
						JSONObject jsonObj = jsonarr.getJSONObject(k);
						cronDetails = new CronJobConfigrationDetails();
						
						cronDetails = gson.fromJson(jsonObj.toString(), CronJobConfigrationDetails.class);
						
						
						
						Calendar cal=Calendar.getInstance();
						cal.setTime(today);
						
						int diffday = 0;
						if(cronDetails.isOverdueStatus() && cronDetails.getOverdueDays()>0){
//							cal.add(Calendar.DATE,cronDetails.getOverdueDays() * -1);
							diffday = cronDetails.getOverdueDays()*-1;
						}else{
							diffday = cronDetails.getInterval();
						 }
						cal.add(Calendar.DATE, diffday);
						
						
						Date dateForFilter=null;
						
						try {
							dateForFilter=fmtPaymentDate.parse(fmtPaymentDate.format(cal.getTime()));
							cal.set(Calendar.HOUR_OF_DAY,23);
							cal.set(Calendar.MINUTE,59);
							cal.set(Calendar.SECOND,59);
							cal.set(Calendar.MILLISECOND,999);
							dateForFilter=cal.getTime();
						} catch (ParseException e) {
							e.printStackTrace();
						}
						
						logger.log(Level.SEVERE,"Date After setting the Time"+dateForFilter);
						
						
						Calendar cal1=Calendar.getInstance();
						cal1.setTime(today);
						cal1.add(Calendar.DATE, 0);
						
						Date interdateForFilter=null;
						
						try {
							interdateForFilter=fmtPaymentDate.parse(fmtPaymentDate.format(cal1.getTime()));
							cal1.set(Calendar.HOUR_OF_DAY,0);
							cal1.set(Calendar.MINUTE,0);
							cal1.set(Calendar.SECOND,0);
							cal1.set(Calendar.MILLISECOND,0);
							interdateForFilter=cal1.getTime();
						} catch (ParseException e) {
							e.printStackTrace();
						}
						
						logger.log(Level.SEVERE,"Date After setting the Time"+dateForFilter);
						
						/**
						 * @author Vijay Date :- 04-05-2022
						 * Des :- as per new logic sms will send as per communication channel if it does not exist then sms 
						 * will send as per old logic
						 */
						try {
							
							
						if(cronDetails.getCommunicationChannel()!=null && !cronDetails.getCommunicationChannel().equals("")
								&& (cronDetails.getCommunicationChannel().equals(AppConstants.SMS) || cronDetails.getCommunicationChannel().equals(AppConstants.WHATSAPP) )){
					
							SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("event","Amount Receivable Due Cron Job").filter("status", true).first().now();
							System.out.println("smsEntity"+smsEntity);
							logger.log(Level.SEVERE,"smsEntity ======= SMS "+smsEntity);

							if(smsEntity!=null){
								
								List<CustomerPayment> custPaymentEntity  = new ArrayList<CustomerPayment>();
								logger.log(Level.SEVERE,"Over status  -- " + cronDetails.isOverdueStatus());
								if(cronDetails.isOverdueStatus()){
									custPaymentEntity = ofy().load().type(CustomerPayment.class)
											.filter("companyId",compEntity.get(i).getCompanyId())
											.filter("paymentDate <=",dateForFilter).filter("status IN",obj)
											.filter("accountType", AppConstants.BILLINGACCOUNTTYPEAR).list();
								}else{
									custPaymentEntity = ofy().load().type(CustomerPayment.class)
											.filter("companyId",compEntity.get(i).getCompanyId()).filter("paymentDate >=",interdateForFilter)
											.filter("paymentDate <=",dateForFilter).filter("status IN",obj)
											.filter("accountType", AppConstants.BILLINGACCOUNTTYPEAR).list();
								}
								
								SmsServiceImpl smsserviceimpl = new SmsServiceImpl();
								logger.log(Level.SEVERE,"custPaymentEntity size "+custPaymentEntity.size());

								boolean prossconfigBhashSMSflag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("SMS", "BHASHSMSAPI", compEntity.get(i).getCompanyId());

								
								for(CustomerPayment customerpayment : custPaymentEntity){
									
									String name = customerpayment.getName();
									if(prossconfigBhashSMSflag){
										name = getCustomerName(name,customerpayment.getCompanyId());
									}
									
									String customerName = smsEntity.getMessage().replace("{CustomerName}",customerpayment.getPersonInfo().getFullName() +"" );
									String paymentAmount = customerName.replace("{PayableAmt}",customerpayment.getPaymentAmt() +"" );
									String paymentDatemsg = paymentAmount.replace("{PaymentDate}", fmtPaymentDate.format(customerpayment.getPaymentDate()) );
									String actualMsg = paymentDatemsg.replace("{companyName}", compEntity.get(i).getBusinessUnitName());
									
									/**
									 * @author Vijay Date :- 09-08-2021
									 * Des :- added below method to check customer DND status if customer DND status is Active 
									 * then SMS will not send to customer
									 */
									ServerAppUtility serverapputility = new ServerAppUtility();
									boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(customerpayment.getPersonInfo().getCount(), customerpayment.getCompanyId());
									if(!dndstatusFlag){
										smsserviceimpl.sendMessage(AppConstants.CRONJOB, AppConstants.CRONJOB, smsEntity.getEvent(), customerpayment.getCompanyId(), actualMsg, customerpayment.getCellNumber(),false);
									}
									
								}
								
							}
							
						}
						else{
							
							/**
							 * @author Vijay Date  :- 18-08-2022
							 * Des :- SMS and whats message will work as per communication type and template selected in cron job reminder settings
							 * below old code commented
							 */
							
//							// this is as per old logic
//							if(k==0){
//								checkSMSConfigurationAndSendSMS(compEntity.get(i).getCompanyId(),compEntity.get(i).getBusinessUnitName());
//							}
						}
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
						
						/**
						 * @author Anil , Date : 25-04-2019
						 * while loading employee added filter ,its status should  be active 
						 * Because mails were triggered on in active employee ids also
						 */
						List<Employee>  employeeList=new ArrayList<Employee>();
						employeeList=ofy().load().type(Employee.class).filter("companyId", c.getCompanyId()).filter("roleName", cronDetails.getEmployeeRole()).filter("status", true).list();
						logger.log(Level.SEVERE,""+cronDetails.getEmployeeRole()+" "+"Employee List size::: "+employeeList.size());
						
							for (Employee employee : employeeList) {
								ArrayList<String> toEmailListDt=new ArrayList<String>();
								toEmailListDt.add(employee.getEmail().trim());
								List<String> branchList=new ArrayList<String>();
								for (int j = 0; j < employee.getEmpBranchList().size(); j++) {
									branchList.add(employee.getEmpBranchList().get(j).getBranchName());
									logger.log(Level.SEVERE, "  -- branch list --"+ employee.getEmpBranchList().get(j).getBranchName());
								}
								logger.log(Level.SEVERE,"toEmailList size::: "+toEmailListDt.size()+" Branch List SIze : "+branchList.size());
								try{
									logger.log(Level.SEVERE,"TO EMAIL :: "+toEmailListDt);
									logger.log(Level.SEVERE,"BRANCH :: "+branchList);
								}catch(Exception e){
									
								}
								/**
								 * @author Anil, Date : 18-02-2019
								 * If branches are not added in employee branch list then , mail was not getting send for those employee
								 */
								if(branchList.size()==0){
									branchList.add(employee.getBranchName());
								}
								
								
								if(branchList.size()!=0){
									{
										
										/********************************Reading PaymentList from CustomerPayment entity  ***********************/
													
													List<CustomerPayment> custPaymentEntity  = new ArrayList<CustomerPayment>();
													logger.log(Level.SEVERE,"Over status  -- " + cronDetails.isOverdueStatus());
//													if(cronList)
													if(cronDetails.isOverdueStatus()){
														custPaymentEntity = ofy().load().type(CustomerPayment.class)
																.filter("companyId",compEntity.get(i).getCompanyId()).filter("branch In", branchList)
																.filter("paymentDate <=",dateForFilter).filter("status IN",obj)
																.filter("accountType", AppConstants.BILLINGACCOUNTTYPEAR).list();
													}else{
														custPaymentEntity = ofy().load().type(CustomerPayment.class)
																.filter("companyId",compEntity.get(i).getCompanyId()).filter("paymentDate >=",interdateForFilter)
																.filter("paymentDate <=",dateForFilter).filter("status IN",obj).filter("branch IN", branchList)
																.filter("accountType", AppConstants.BILLINGACCOUNTTYPEAR).list();
													}
													
													
													logger.log(Level.SEVERE,"over due date is date Is:"+dateForFilter + " todays date -- " + interdateForFilter);	

													logger.log(Level.SEVERE,"Paymnet entity size:"+custPaymentEntity.size());	
													
													
													// email id is added to emailList
													ArrayList<String> toEmailList=new ArrayList<String>();
													toEmailList.add(compEntity.get(i).getPocEmail());
													
													 String mailTitl = "Payment Due As On Date";
													
													if(custPaymentEntity.size()>0){
													
												
													ArrayList<String> tbl_header = new ArrayList<String>();
													tbl_header.add("Serial No");
													tbl_header.add("Branch");
													tbl_header.add("Customer Id");
													tbl_header.add("Customer Name");
													tbl_header.add("Customer Cont No");
													tbl_header.add("POC Name");
//													tbl_header.add("Email Id");
													tbl_header.add("Sales Person");
													tbl_header.add("Person Responsible");
													tbl_header.add("OrderId");
													tbl_header.add("Invoice Id");
													tbl_header.add("Invoice Date");
													tbl_header.add("Payment Doc Id");
													tbl_header.add("Payment Amt");
													tbl_header.add("Payment Date");
													tbl_header.add("Status");
													tbl_header.add("Ageing");
													
									/********************************Sorting table with Branch & Payment Date ***********************/
												
													logger.log(Level.SEVERE," before heading Paymnet entity size:"+custPaymentEntity.size());
											/**
											 * @author Anil,Date : 18-02-2019
											 * Due to null payment date cron job mail were not getting send
											 * For Pecopp Raised by Rohan
											 */
											Comparator<CustomerPayment> custpaymentDateComparator2 = new Comparator<CustomerPayment>() {
												public int compare(CustomerPayment s1,CustomerPayment s2) {
													Date date1 = s1.getPaymentDate();
													Date date2 = s2.getPaymentDate();
													
													if(date1==null||date2==null){
														return 0;
													}
													// ascending order
													return date1.compareTo(date2);
												}
											};
											Collections.sort(custPaymentEntity,custpaymentDateComparator2);

											Comparator<CustomerPayment> custpaymentDateComparator = new Comparator<CustomerPayment>() {
												public int compare(CustomerPayment s1,CustomerPayment s2) {
													String branch1 = s1.getBranch();
													String branch2 = s2.getBranch();
													// ascending order
													return branch1.compareTo(branch2);
												}

											};
											Collections.sort(custPaymentEntity,custpaymentDateComparator);
														
									/********************************Getting PaymnetEntity data and adding in the tbl1 List ***********************/
															logger.log(Level.SEVERE," after heading Paymnet entity size:"+custPaymentEntity.size());
													ArrayList<String> tbl1=new ArrayList<String>();
														
													for(int j=0;j<custPaymentEntity.size();j++){
														
//														Customer customerEntity=ofy().load().type(Customer.class)
//																.filter("companyId", compEntity.get(i).getCompanyId()).filter("count", custPaymentEntity.get(j).getPersonInfo().getCount()).first().now();
//														logger.log(Level.SEVERE," count -- " + custPaymentEntity.get(j).getCount()+ " sequance -- "+ j+1);
														tbl1.add((j+1)+"");
											   	 		tbl1.add(custPaymentEntity.get(j).getBranch()+"");
											   	 		tbl1.add(custPaymentEntity.get(j).getPersonInfo().getCount()+"");
											   	 		tbl1.add(custPaymentEntity.get(j).getPersonInfo().getFullName()+"");		   	 		
											   	 		tbl1.add(custPaymentEntity.get(j).getPersonInfo().getCellNumber()+"");
											   	 		tbl1.add(custPaymentEntity.get(j).getPersonInfo().getPocName());
//											   	 		tbl1.add(customerEntity.getEmail());
											   	 		tbl1.add(custPaymentEntity.get(j).getEmployee());
											   	 		tbl1.add(custPaymentEntity.get(j).getEmployee());
											   	 		tbl1.add(custPaymentEntity.get(j).getContractCount()+"");
											   	 		tbl1.add(custPaymentEntity.get(j).getInvoiceCount()+"");
											   	 		tbl1.add(fmt.format(custPaymentEntity.get(j).getInvoiceDate())+""); 	 		
											   	 		tbl1.add(custPaymentEntity.get(j).getCount()+"");
											   	 	    tbl1.add(custPaymentEntity.get(j).getPaymentAmt()+"");
												   	 	long diff = 0;
														long diffDays =0;
											   	 	    if(custPaymentEntity.get(j).getPaymentDate()!=null){
											   	 	    	
											   	 	    	String StringPaymnetDate = fmtPaymentDate.format(custPaymentEntity.get(j).getPaymentDate());
											   	 	    	tbl1.add(fmt.format(custPaymentEntity.get(j).getPaymentDate()) +"");
												   	 	    Date d1 = null;
											   				Date d2 = null;
											   				
											   				d1 = df.parse(StringPaymnetDate);
											   				d2 = df.parse(todayDateString);
											   				 diff = d2.getTime() - d1.getTime();
															 diffDays = diff / (24 * 60 * 60 * 1000);
											   	 	    }else {
											   	 	    	tbl1.add("");
														}
											   	 	    
											 
											   	 	    /**************** for getting ageing for each Payment ******/
											   	 	  
											   	 	    
//											   	 	   	String StringPaymnetDate = df.format(paymentDate);	
											   			
											   				
															
//															System.out.println("Ageing:"+diffDays);
//													   	 	logger.log(Level.SEVERE,"Ageing:"+diffDays);
												
											   	 	    tbl1.add(custPaymentEntity.get(j).getStatus()+"");
											   	 	    tbl1.add(diffDays +"");
											   	 	}			
													
													logger.log(Level.SEVERE,"In the ProsConfig !=null   3 ");
													
														
													
												
													
									   /********************************Calling cronsendEmail Method ***********************/
													
													try {   
														   
															System.out.println("Before send mail method");
															logger.log(Level.SEVERE,"Before send method call to send mail  ");	
															
															String footer = "";
															
															if(cronDetails.getFooter().trim()!= "" ){
																footer = cronDetails.getFooter();
															}
															
															
															String emailSubject =   "Payment Due As On Date"+" "+ todayDateString;
															
															if(cronDetails.isOverdueStatus()){
																emailSubject =   "Payment Overdue As On Date";
															}
															
															
															if(cronDetails.getSubject().trim() != ""  && cronDetails.getSubject().trim().length() >0){
																emailSubject = cronDetails.getSubject();
															}
															
															String emailMailBody =mailTitl +"  "+ todayDateString;
															
															if(cronDetails.getMailBody().trim() != ""  && cronDetails.getMailBody().trim().length() >0){
																emailMailBody = cronDetails.getMailBody();
															}
															
																System.out.println("Before send mail method");
																logger.log(Level.SEVERE,"Before send method call to send mail  ");						
															
															cronEmail.cronSendEmail(toEmailListDt, emailSubject, emailMailBody,
																	c, tbl_header, tbl1, null, null, null, null,"",footer);
															logger.log(Level.SEVERE,"After send mail method ");		
																	//+"  "+
													} catch (IOException e1) {
														
														logger.log(Level.SEVERE,"In the catch block ");
														e1.printStackTrace();
												}
													
												}
												else{					
														
													System.out.println("Sorry no Payment found for this company");
													logger.log(Level.SEVERE,"Sorry no Payment found for this company");

													mailTitl = "No Payment Found Due";

													cronEmail.cronSendEmail(toEmailListDt, "Customer Payment Due As On Date"+" "+ todayDateString, mailTitl , c, null, null, null, null, null, null);

												}
									}
								}
							}
	
				
					
							
		}
				
				}
		/*			catch(Exception e){
			logger.log(Level.SEVERE,"get error --" + e);
		}*/
		}
	//end of for loop
		
				
		
	}
	else{			//else block for if(compEntity.size()>0)
		
		logger.log(Level.SEVERE,"No Company found from cron job completed");	
		System.out.println("No Company found from cron job completed");
	}
	
	
	 }catch(Exception e2){
	
	e2.printStackTrace();
	 }
		
			
				
		}

//		private void checkSMSConfigurationAndSendSMS(Long companyId, String companyName) {
//
////			SmsConfiguration smsconfig = ofy().load().type(SmsConfiguration.class).filter("companyId", companyId).filter("status",true).first().now();
////			if(smsconfig!=null){
////				
////				String accountSid = smsconfig.getAccountSID();
////				String authotoken = smsconfig.getAuthToken();
////				String frmnumber = smsconfig.getPassword();
//			
//				ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", companyId).first().now();
//				ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId).filter("processName","CronJob").first().now();				
//					
//				boolean prossconfigSMSflag = false;
//				if(processconfig!=null){
//					if(processconfig.isConfigStatus()){
//						for(int l=0;l<processconfig.getProcessList().size();l++){
//							if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("CustomerPaymentARSMS") && processconfig.getProcessList().get(l).isStatus()==true){
//								prossconfigSMSflag=true;
//							}
//						}
//				   }
//				}
//				logger.log(Level.SEVERE,"process config  SMS Flag =="+prossconfigSMSflag);
//
//				
//			if(processName!=null)
//			{
//				logger.log(Level.SEVERE,"In the prossName for ======= SMS ");
//
//				if(prossconfigSMSflag){
//					
//					logger.log(Level.SEVERE,"In the ProsConfifg !=null ======= for SMS ");
//					System.out.println("In the ProsConfifg !=null ");
//					
//					SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId", companyId).filter("event","Amount Receivable Due Cron Job").filter("status", true).first().now();
//					System.out.println("smsEntity"+smsEntity);
//					logger.log(Level.SEVERE,"smsEntity ======= SMS "+smsEntity);
//
//					if(smsEntity!=null){
//						
//						int day = smsEntity.getDay(); 
//						
//						String templateMsgwithBraces = smsEntity.getMessage();
//						System.out.println("Template Msg========:"+templateMsgwithBraces);
//						
//						System.out.println("Day"+day);
//			
//						
//						Calendar cal3 = Calendar.getInstance();
//						cal3.setTime(new Date());
//						cal3.add(Calendar.DATE, +day);
//						
//						Date dateforsmsfilter = null;
//						
//						try {
//							
//							dateforsmsfilter = fmtPaymentDate.parse(fmtPaymentDate.format(cal3.getTime()));
//							
//						} catch (ParseException e) {
//							e.printStackTrace();
//						}
//						
//						logger.log(Level.SEVERE,"Date for SMS 1"+dateforsmsfilter);
//						System.out.println("dateforsmsfilter2"+dateforsmsfilter);
//
//						Calendar cal2 = Calendar.getInstance();
//						cal2.setTime(new Date());
//						cal2.add(Calendar.DATE, +day);
//						
//						Date dateforsmsfilter2 = null;
//						
//						try {
//							
//							dateforsmsfilter2 = fmtPaymentDate.parse(fmtPaymentDate.format(cal2.getTime()));
//							cal2.set(Calendar.HOUR_OF_DAY,23);
//							cal2.set(Calendar.MINUTE,59);
//							cal2.set(Calendar.SECOND,59);
//							cal2.set(Calendar.MILLISECOND,999);
//							dateforsmsfilter2=cal2.getTime();
//							
//						} catch (ParseException e) {
//							e.printStackTrace();
//						}
//						
//						logger.log(Level.SEVERE,"Date for SMS 1"+dateforsmsfilter2);
//						System.out.println("dateforsmsfilter2"+dateforsmsfilter2);
//						
//						
//						List<CustomerPayment> customerpayment = ofy().load().type(CustomerPayment.class)
//								.filter("companyId", companyId)
//								.filter("paymentDate >=", dateforsmsfilter)
//								.filter("paymentDate <=", dateforsmsfilter2).filter("status", "Created").filter("accountType", "AR").list();
//						logger.log(Level.SEVERE,"customerpayment=========="+customerpayment.size());
//
//						for(int l=0;l<customerpayment.size();l++){
//							
//							/**
//							 * @author Vijay Date :- 09-08-2021
//							 * Des :- added below method to check customer DND status if customer DND status is Active 
//							 * then SMS will not send to customer
//							 */
//							ServerAppUtility serverapputility = new ServerAppUtility();
//							boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(customerpayment.get(l).getPersonInfo().getCount(), customerpayment.get(l).getCompanyId());
//							if(!dndstatusFlag){
//								sendSMS(companyId,customerpayment.get(l).getCount(),customerpayment.get(l).getName(),customerpayment.get(l).getPaymentAmt(),customerpayment.get(l).getPaymentDate(),customerpayment.get(l).getCellNumber(),companyName,templateMsgwithBraces,smsEntity.getEvent() );
//							}
//							
//						}
//					}
//				}
//			}
////		}
//	}

}
