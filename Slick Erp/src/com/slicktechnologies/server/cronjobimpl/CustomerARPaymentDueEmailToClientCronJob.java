package com.slicktechnologies.server.cronjobimpl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.email.SendGridEmailServlet;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;


public class CustomerARPaymentDueEmailToClientCronJob extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4537840069276159696L;
	
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private DateFormat fmtPaymentDate = new SimpleDateFormat("dd/MM/yyyy");
	
	 Logger logger = Logger.getLogger("NameOfYourLogger");
	 

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		customerPaymentARlist();
	}


	public void customerPaymentARlist() {
		
	Email cronEmail = new Email();
	
	Date today=DateUtility.getDateWithTimeZone("IST", new Date());
	
	TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	fmtPaymentDate.setTimeZone(TimeZone.getTimeZone("IST"));
	
	/**
	 *    i have added -3 days in today's date
	 */
	
	Calendar cal=Calendar.getInstance();
	cal.setTime(today);
	cal.add(Calendar.DATE, -3);
	
	Date dateForFilter=null;
	
	try {
		dateForFilter=fmtPaymentDate.parse(fmtPaymentDate.format(cal.getTime()));
		cal.set(Calendar.HOUR_OF_DAY,23);
		cal.set(Calendar.MINUTE,59);
		cal.set(Calendar.SECOND,59);
		cal.set(Calendar.MILLISECOND,999);
		dateForFilter=cal.getTime();
	} catch (ParseException e) {
		e.printStackTrace();
	}
	
	logger.log(Level.SEVERE,"Date After setting the Time"+dateForFilter);
	
	/************************************* End *********************************/
	
	
try{
 
// logger.log(Level.SEVERE,"In Payment AR List");	
//logger.log(Level.SEVERE,"Date After -3 days Date"+dateForFilter);

 
/********************************Adding status in the list ***********************/
 
 ArrayList<String> obj = new ArrayList<String>();
 obj.add("Created");
 
/******************************Converting todayDate to String format*****************/
 
 Date todaysDate = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
 String todayDateString = df.format(todaysDate);
 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);

 
/********************************Adding Companies in the list ***********************/
 
List<Company> compEntity = ofy().load().type(Company.class).list();
if(compEntity.size()>0){

//logger.log(Level.SEVERE,"If compEntity size > 0");
//logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());

for(int i=0;i<compEntity.size();i++){
	
	Company c=compEntity.get(i);
//	logger.log(Level.SEVERE,"In the for loop");

	
/********************************Checking prosname & prosconfig for each company ***********************/

	ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
	ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
	
	boolean processconfigflag = false;
	if(processconfig!=null){
		if(processconfig.isConfigStatus()){
			for(int l=0;l<processconfig.getProcessList().size();l++){
				if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("PaymentARDueDailyEmailToClient") && processconfig.getProcessList().get(l).isStatus()==true){
					processconfigflag=true;
				}
			}
	   }
	}
	logger.log(Level.SEVERE,"process config Flag =="+processconfigflag);
	
	if(processName!=null){
		logger.log(Level.SEVERE,"In the prossName");
	if(processconfigflag){
					
//		logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
		
		
/********************************Reading PaymentList from CustomerPayment entity  ***********************/
		
		List<CustomerPayment> custPaymentEntity = ofy().load().type(CustomerPayment.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("paymentDate <=",dateForFilter).filter("status IN",obj).filter("accountType", AppConstants.BILLINGACCOUNTTYPEAR).list();
		
//		logger.log(Level.SEVERE,"Today date Is:"+dateForFilter);	

		logger.log(Level.SEVERE,"Paymnet entity size:"+custPaymentEntity.size());	
		
		 String mailTitl = "Payment Due Reminder";
		 
//		if(custPaymentEntity.size()>0){
			
/********************************getting client specific all payment document and added into hashmap ***********************/
					
	HashMap<Integer, ArrayList<CustomerPayment>> paymenthashmap = new HashMap<Integer,ArrayList<CustomerPayment>>();

		for(int j=0;j<custPaymentEntity.size();j++){
			
			ArrayList<CustomerPayment> list = new ArrayList<CustomerPayment>();
			
			if(paymenthashmap.containsKey(custPaymentEntity.get(j).getCustomerCount())){
				list = paymenthashmap.get(custPaymentEntity.get(j).getCustomerCount());
				list.add(custPaymentEntity.get(j));
			}else{
				list.add(custPaymentEntity.get(j));
			}
			paymenthashmap.put(custPaymentEntity.get(j).getCustomerCount(), list);
   	 	}
		
		Set s1 = paymenthashmap.entrySet();
		Iterator iterator =  s1.iterator();
		
		
		while(iterator.hasNext()){
			ArrayList<CustomerPayment> customerPaymentlist = new ArrayList<CustomerPayment>();

			Map.Entry<Integer, ArrayList<CustomerPayment>> m33 = (Map.Entry<Integer, ArrayList<CustomerPayment>>) iterator.next();
			customerPaymentlist.addAll(m33.getValue());
			
			ArrayList<String> tbl1_header=new ArrayList<String>();
			ArrayList<String> tbl1=new ArrayList<String>();
			
//			Customer customerEntity=ofy().load().type(Customer.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("count", customerPaymentlist.get(0).getPersonInfo().getCount()).first().now();
//			
//			// email id is added to emailList
//			ArrayList<String> toEmailList1=new ArrayList<String>();
//			toEmailList1.add(customerEntity.getEmail());
//			
//			String customerName = getFirstLetterUpperCase(customerEntity.getFullname());
			
			ArrayList<String> toEmailList1 = null;
			String msgBody="";
			int totalAmount = 0;
			
			String customerName ="";
			logger.log(Level.SEVERE,"customerPaymentlist size for one client"+customerPaymentlist.size());
			for(int k=0;k<customerPaymentlist.size();k++){
				
				totalAmount += customerPaymentlist.get(k).getPaymentAmt();
				if(k==0){
					Customer customerEntity=ofy().load().type(Customer.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("count", customerPaymentlist.get(k).getPersonInfo().getCount()).first().now();
					// email id is added to emailList
					 toEmailList1=new ArrayList<String>();
					toEmailList1.add(customerEntity.getEmail());
					customerName = getFirstLetterUpperCase(customerEntity.getFullname());
					logger.log(Level.SEVERE,"customerEntity count == "+customerPaymentlist.get(k).getPersonInfo().getCount()+"Payment count == "+customerPaymentlist.get(k).getCount());
//					logger.log(Level.SEVERE,"Payment count == "+customerPaymentlist.get(k).getCount());


				}
				
	
				//Table header 1
				if(k==0){
					tbl1_header.add("Sr No");
					tbl1_header.add("Invoice Id");
					tbl1_header.add("Invoice Date");
					tbl1_header.add("Payment Date");
					tbl1_header.add("Reference No");
					tbl1_header.add("Remark");
					tbl1_header.add("Amount");
					tbl1_header.add("Ageing");
				}
					
				tbl1.add(k+1+"");
				tbl1.add(customerPaymentlist.get(k).getInvoiceCount()+"");
				tbl1.add(fmt.format(customerPaymentlist.get(k).getInvoiceDate()));
				tbl1.add(fmt.format(customerPaymentlist.get(k).getPaymentDate()));
				Invoice invoiceEntity = ofy().load().type(Invoice.class).filter("count", customerPaymentlist.get(k).getInvoiceCount()).filter("companyId", customerPaymentlist.get(k).getCompanyId()).first().now();
				if(invoiceEntity.getRefNumber()!=null){
					tbl1.add(invoiceEntity.getRefNumber());
				}else{
					tbl1.add(" ");
				}
				if(invoiceEntity.getRemark()!=null){
					tbl1.add(invoiceEntity.getRemark());
				}else{
					tbl1.add(" ");
				}
				tbl1.add(customerPaymentlist.get(k).getPaymentAmt()+"");
				
				
				String StringPaymentDate = fmtPaymentDate.format(customerPaymentlist.get(k).getPaymentDate());
   				Date d1 = null;
   				Date d2 = null;
   				
   				d1 = df.parse(StringPaymentDate);
   				d2 = df.parse(todayDateString);
   				long diff = d2.getTime() - d1.getTime();
				long diffDays = diff / (24 * 60 * 60 * 1000);
				tbl1.add(diffDays+"");
				if(k==customerPaymentlist.size()-1){
					k++;
					tbl1.add("Total");
					tbl1.add(totalAmount+"");
				}
//				logger.log(Level.SEVERE,"In the ProsConfig !=null   3 ");
			}
			
//			logger.log(Level.SEVERE,"Client Name =="+customerName);
//
//			logger.log(Level.SEVERE,"tbl1 size =="+tbl1.size());
			
			msgBody ="</br></br> Dear "+customerName+ ", <br><br> As per our record following payment Rs. "+totalAmount+" overdue from you. We request you to make the payment at the earliest. <br><br>";

			String paymentTableTitle = "Below are the payment modes which are available to you to make the payment";
			String paymentTableTitle2 = "Payment Modes";
			
			ArrayList<String> tbl2_header=new ArrayList<String>();
			tbl2_header.add("Modes");
			tbl2_header.add("Favoring");
			tbl2_header.add("Details");
			
			String msgBodyTwo = null;
			String msgbodyThree = null;
			
			String footermsg = null;
			
			String mailSubject="Payment Due Reminder - "+compEntity.get(i).getBusinessUnitName();

			cronEmail.PaymentReminderCronJobEmailToClient(toEmailList1, mailSubject, mailTitl, c, msgBody,null,null, tbl1_header, tbl1,paymentTableTitle, paymentTableTitle2, tbl2_header,  msgBodyTwo, msgbodyThree, null, footermsg, null, true, true);
			
		}
		
		}
		else{					//else block for prosconfig
				logger.log(Level.SEVERE,"ProcessConfiguration is null");
				System.out.println("ProcessConfiguration is null");
		}
		
		}else{        //else block for pross!=null
			logger.log(Level.SEVERE,"Cron job status is Inactive");
			System.out.println("Cron job status is Inactive");
		}
		
						
	}	//end of for loop
	
	}
	else{			//else block for if(compEntity.size()>0)
		
		logger.log(Level.SEVERE,"No Company found from cron job completed");	
		System.out.println("No Company found from cron job completed");
	}
	
	
	 }catch(Exception e2){
	
		 e2.printStackTrace();
	 }
	
	}
	
	
	private String getFirstLetterUpperCase(String customerFullName) {
		
		String customerName="";
		String[] customerNameSpaceSpilt=customerFullName.split(" ");
		int count=0;
		for (String name : customerNameSpaceSpilt) 
		{
			String nameLowerCase=name.toLowerCase();
			if(count==0)
			{
				customerName=customerName+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);
			}
			else
			{
				customerName=customerName+" "+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);	
			}
			
			count=count+1;
			
		}
		return customerName;  
		
		}
	
	/**
	 * @author Ashwini Patil
	 * @param employeeRole 
	 * @since 23-04-2022
	 * Payment outstanding reminder are currently hardcoded in the system, and their mail is sent to the client automatically; 
	 * however, we now want this cronjob's message to be defined in the following mail format; we do not want to hardcode it into the code, and the mail should be sent in the following mail format.
	 */
	public void customerPaymentARlistForCronJob(String cronList, HashSet<String> empRoleList) {
		
		
		
		Date today=DateUtility.getDateWithTimeZone("IST", new Date());
		
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmtPaymentDate.setTimeZone(TimeZone.getTimeZone("IST"));
		
	
	try{
 
	/********************************Adding Companies in the list ***********************/
	 
		List<Company> compEntity = ofy().load().type(Company.class).list();
		if(compEntity.size()>0){
	
//		logger.log(Level.SEVERE,"If compEntity size > 0");
//		logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
		if(empRoleList!=null)
		logger.log(Level.SEVERE,"Size of empRoleList"+empRoleList.size());
			for(int i=0;i<compEntity.size();i++){
				Customer c= ofy().load().type(Customer.class).filter("companyId",compEntity.get(i).getCompanyId()).first().now();
				if(c!=null){//Ashwini Patil Date:4-1-2023 if there is no customer for this company id then skip to call further code.
				
				
				for(String empRoleName :empRoleList){
					logger.log(Level.SEVERE,"selected empRoleName for execution="+empRoleName);
					if(!empRoleName.equalsIgnoreCase(AppConstants.CUSTOMER)){
						
						List<Employee>  employeeList=new ArrayList<Employee>();
						employeeList=ofy().load().type(Employee.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("roleName", empRoleName).filter("status", true).list();
						logger.log(Level.SEVERE,""+empRoleName+" "+"Employee List size::: "+employeeList.size());
						
							for (Employee employee : employeeList) {
								
									List<String> branchList=new ArrayList<String>();
									
									branchList.add(employee.getBranchName());
									for (int j = 0; j < employee.getEmpBranchList().size(); j++) {
										branchList.add(employee.getEmpBranchList().get(j).getBranchName());
										logger.log(Level.SEVERE, "  -- branch list --"+ employee.getEmpBranchList().get(j).getBranchName());
									}
									logger.log(Level.SEVERE," Branch List SIze : "+branchList.size());
		
									if(branchList.size()!=0){
										CustomerPaymentReminderToclient(cronList,employee,branchList,compEntity.get(i),today,"NonCustomer");
									}
							}
					}
					else{
						CustomerPaymentReminderToclient(cronList,null,null,compEntity.get(i),today,"Customer");
		
					}
				}
				}
			}
		}
		else{
			logger.log(Level.SEVERE,"No Company found from cron job completed");	
			System.out.println("No Company found from cron job completed");
		}
	 }catch(Exception e2){
		 e2.printStackTrace();
	 }
				
	}


	private void CustomerPaymentReminderToclient(String cronList,
			Employee employee, List<String> branchList, Company company, Date today,String callFor) {
		try {
			
		/********************************Adding status in the list ***********************/
		 ArrayList<String> obj = new ArrayList<String>();
		 obj.add("Created");
		 
		 /******************************Converting todayDate to String format*****************/
		 
		 Date todaysDate = new Date();
		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 String todayDateString = df.format(todaysDate);
		 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);
		 
		 
		Email cronEmail = new Email();
		CompanyPayment compPayment=null;
		
		//Copied from another cron job
		Gson gson = new Gson();
		JSONArray jsonarr = null;
		try {
			jsonarr=new JSONArray(cronList.trim());
		} catch (JSONException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		CronJobConfigrationDetails cronDetails =null;
		for(int k=0;k<jsonarr.length();k++){
//			logger.log(Level.SEVERE,"company id="+company.getCompanyId()+" JSON ARRAY LENGTH :: "+jsonarr.length()+ " callFor="+callFor);
			JSONObject jsonObj = jsonarr.getJSONObject(k);
			cronDetails = new CronJobConfigrationDetails();
			
			cronDetails = gson.fromJson(jsonObj.toString(), CronJobConfigrationDetails.class);
			
			Calendar cal=Calendar.getInstance();
			cal.setTime(today);
			
			int diffday = 0;
			if(cronDetails.isOverdueStatus() && cronDetails.getOverdueDays()>0){
//				cal.add(Calendar.DATE,cronDetails.getOverdueDays() * -1);
				diffday = cronDetails.getOverdueDays()*-1;
			}else{
				diffday = cronDetails.getInterval();
			 }
			cal.add(Calendar.DATE, diffday);
			logger.log(Level.SEVERE,"diffday1="+diffday+"cal="+cal.getTime());
			
			Date dateForFilter=null;
			
			try {
				dateForFilter=fmtPaymentDate.parse(fmtPaymentDate.format(cal.getTime()));
				cal.set(Calendar.HOUR_OF_DAY,23);
				cal.set(Calendar.MINUTE,59);
				cal.set(Calendar.SECOND,59);
				cal.set(Calendar.MILLISECOND,999);
				dateForFilter=cal.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
//			logger.log(Level.SEVERE,"Date After setting the Time"+dateForFilter);
			
			
			Calendar cal1=Calendar.getInstance();
			cal1.setTime(today);
			cal1.add(Calendar.DATE, 0);
			
			Date interdateForFilter=null;
			
			try {
				interdateForFilter=fmtPaymentDate.parse(fmtPaymentDate.format(cal1.getTime()));
				cal1.set(Calendar.HOUR_OF_DAY,0);
				cal1.set(Calendar.MINUTE,0);
				cal1.set(Calendar.SECOND,0);
				cal1.set(Calendar.MILLISECOND,0);
				interdateForFilter=cal1.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
//			logger.log(Level.SEVERE,"Date After setting the Time interdateForFilter"+interdateForFilter);
			
		//End of copy
		
//		logger.log(Level.SEVERE,"In the for loop");

		
	/********************************Reading PaymentList from CustomerPayment entity  ***********************/
		List<CustomerPayment> custPaymentEntity;
		
		if(branchList!=null && branchList.size()>0){
			
			if(cronDetails.isOverdueStatus()){
				logger.log(Level.SEVERE,"In isOverdueStatus()");
				custPaymentEntity = ofy().load().type(CustomerPayment.class)
						.filter("companyId",company.getCompanyId())
						.filter("paymentDate >=",dateForFilter).filter("paymentDate <=",interdateForFilter).filter("status IN",obj)
						.filter("accountType", AppConstants.BILLINGACCOUNTTYPEAR).list();
			}else{
				custPaymentEntity = ofy().load().type(CustomerPayment.class)
						.filter("companyId",company.getCompanyId()).filter("paymentDate >=",interdateForFilter)
						.filter("paymentDate <=",dateForFilter).filter("status IN",obj)
						.filter("accountType", AppConstants.BILLINGACCOUNTTYPEAR).list();
			}
			
		}
		else{
			
			if(cronDetails.isOverdueStatus()){
				logger.log(Level.SEVERE,"In isOverdueStatus()");
				custPaymentEntity = ofy().load().type(CustomerPayment.class)
						.filter("companyId",company.getCompanyId())
						.filter("paymentDate >=",dateForFilter).filter("paymentDate <=",interdateForFilter).filter("status IN",obj)
						.filter("accountType", AppConstants.BILLINGACCOUNTTYPEAR).list();
			}else{
				custPaymentEntity = ofy().load().type(CustomerPayment.class)
						.filter("companyId",company.getCompanyId()).filter("paymentDate >=",interdateForFilter)
						.filter("paymentDate <=",dateForFilter).filter("status IN",obj)
						.filter("accountType", AppConstants.BILLINGACCOUNTTYPEAR).list();
			}
		}

		
		
		logger.log(Level.SEVERE,"over due date is date Is:"+dateForFilter + " todays date -- " + interdateForFilter);	
		logger.log(Level.SEVERE,"Paymnet entity size:"+custPaymentEntity.size());	
//		logger.log(Level.SEVERE,"Today date Is:"+dateForFilter);	
//		logger.log(Level.SEVERE,"Paymnet entity size:"+custPaymentEntity.size());	
			
			boolean communicationChannelEmailFlag = false;
			if(cronDetails.getCommunicationChannel()!=null && !cronDetails.getCommunicationChannel().equals("")
					&& (cronDetails.getCommunicationChannel().equals(AppConstants.SMS) || cronDetails.getCommunicationChannel().equals(AppConstants.WHATSAPP) )){
				
				long cellNumber=0;
				if(!cronDetails.getEmployeeRole().equalsIgnoreCase(AppConstants.CUSTOMER)){
					if(employee!=null)
						cellNumber = employee.getCellNumber1();
				}
				sendPaymentReminderMessage(custPaymentEntity,cronDetails,company,cellNumber);
			}
			if(cronDetails.getCommunicationChannel()==null || cronDetails.getCommunicationChannel().equals("")){
				communicationChannelEmailFlag = true;
			}
			if(cronDetails.getCommunicationChannel()!=null && !cronDetails.getCommunicationChannel().equals("")
					&&  cronDetails.getCommunicationChannel().equals(AppConstants.EMAIL)){
				communicationChannelEmailFlag = true;
			}
			logger.log(Level.SEVERE,"communicationChannelEmailFlag "+communicationChannelEmailFlag);
			
//			ArrayList<String> toEmailListDt=new ArrayList<String>();
//			toEmailListDt.add(employee.getEmail().trim());
			
			
			if(communicationChannelEmailFlag){
				HashSet<String> companyBranchNamesList=new HashSet<String>();
				ArrayList<Branch>  companyBranchesList=new ArrayList<Branch>();
				HashMap<String, String> branchOrcompanyEmailMap=new HashMap<String, String>();
				HashMap<String, String> branchSignatureMap=new HashMap<String, String>();
				
				boolean branchascompany=false;
//				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){	
			
				
			 String mailTitl = "Payment Due Reminder";
				
			 /********************************getting client specific all payment document and added into hashmap ***********************/
						
			 HashMap<Integer, ArrayList<CustomerPayment>> paymenthashmap = new HashMap<Integer,ArrayList<CustomerPayment>>();

			for(int j=0;j<custPaymentEntity.size();j++){
				
				ArrayList<CustomerPayment> list = new ArrayList<CustomerPayment>();
				
				if(paymenthashmap.containsKey(custPaymentEntity.get(j).getCustomerCount())){
					list = paymenthashmap.get(custPaymentEntity.get(j).getCustomerCount());
					list.add(custPaymentEntity.get(j));
				}else{
					list.add(custPaymentEntity.get(j));
				}
				paymenthashmap.put(custPaymentEntity.get(j).getCustomerCount(), list);
				
				companyBranchNamesList.add(custPaymentEntity.get(j).getBranch());
	   	 	}
			logger.log(Level.SEVERE,"companyBranchNamesList size="+companyBranchNamesList.size());
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", company.getCompanyId())){
//				logger.log(Level.SEVERE,"BranchAsCompany active");
				branchascompany=true;
				List<Branch> blist= ofy().load().type(Branch.class).filter("companyId",company.getCompanyId()).filter("buisnessUnitName IN",companyBranchNamesList).list();
				
//				logger.log(Level.SEVERE,"branchlist size="+blist.size());
				companyBranchesList.addAll(blist);
				for(Branch branch:companyBranchesList) {
					branchOrcompanyEmailMap.put(branch.getBusinessUnitName(), ServerAppUtility.getCompanyEmail(branch.getBusinessUnitName(), company));
					branchSignatureMap.put(branch.getBusinessUnitName(), ServerAppUtility.getCompanySignature(company, branch));					
				}
				logger.log(Level.SEVERE,"branchOrcompanyEmailMap size="+branchOrcompanyEmailMap.size());
			}

			logger.log(Level.SEVERE,"paymenthashmap size="+paymenthashmap.size());
			Set s1 = paymenthashmap.entrySet();
			Iterator iterator =  s1.iterator();
			
			
			while(iterator.hasNext()){
				ArrayList<CustomerPayment> customerPaymentlist = new ArrayList<CustomerPayment>();

				Map.Entry<Integer, ArrayList<CustomerPayment>> m33 = (Map.Entry<Integer, ArrayList<CustomerPayment>>) iterator.next();
				customerPaymentlist.addAll(m33.getValue());
				
				ArrayList<String> tbl1_header=new ArrayList<String>();
				ArrayList<String> tbl1=new ArrayList<String>();
				
//				Customer customerEntity=ofy().load().type(Customer.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("count", customerPaymentlist.get(0).getPersonInfo().getCount()).first().now();
//				
//				// email id is added to emailList
//				ArrayList<String> toEmailList1=new ArrayList<String>();
//				toEmailList1.add(customerEntity.getEmail());
//				
//				String customerName = getFirstLetterUpperCase(customerEntity.getFullname());
				
				ArrayList<String> toEmailList1 = null;
				String companyEmail="";
				String msgBody="";
				int totalAmount = 0;
				
				String customerName ="";
				if(customerPaymentlist!=null&&customerPaymentlist.size()>0)
					logger.log(Level.SEVERE,customerPaymentlist.get(0).getPersonInfo().getCount()+ " customerPaymentlist size for one client"+customerPaymentlist.size());
				else
					logger.log(Level.SEVERE,"customerPaymentlist size for one client"+customerPaymentlist.size());
				
				logger.log(Level.SEVERE,"cronDetails.getEmployeeRole() "+cronDetails.getEmployeeRole());

				if(!branchascompany) {
				for(int p=0;p<customerPaymentlist.size();p++){					

						
						totalAmount += customerPaymentlist.get(p).getPaymentAmt();
						if(p==0){
							Customer customerEntity=ofy().load().type(Customer.class).filter("companyId", company.getCompanyId()).filter("count", customerPaymentlist.get(p).getPersonInfo().getCount()).first().now();
							// email id is added to emailList
							toEmailList1=new ArrayList<String>();
//							toEmailList1.add(customerEntity.getEmail());
							
							/*
							 * Ashwini Patil 
							 * Date:12-1-2023
							 * Emails were getting sent multiple times to customer as well as branch
							 * Since crondetails are getting run in loop twice. First time for employee role in method customerPaymentARlistForCronJob and then again in current method
							 * This issue happens only when there are more than one configuration for this cron job
							 * so while calling this method added one more parameter "callFrom" to stop sending multiple emails 
							 */
							if(!cronDetails.getEmployeeRole().equalsIgnoreCase(AppConstants.CUSTOMER)&&callFor.equalsIgnoreCase("NonCustomer")){
								if(employee!=null)
									toEmailList1.add(employee.getEmail());
							}
							else if(callFor.equalsIgnoreCase(AppConstants.CUSTOMER)){  
								toEmailList1.add(customerEntity.getEmail());
									
								if(customerEntity!=null&&ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Company", "SendAutoEmailToAllContactsOfCustomer", company.getCompanyId())) {
									
									/*
									 * Ashwini Patil
									 * Date:9-07-2024
									 * Ultra pest requirement - send payment reminder to all contacts of the customer
									 */
									try {
									List<ContactPersonIdentification> contactList= ofy().load().type(ContactPersonIdentification.class).filter("companyId", company.getCompanyId())
											.filter("personInfo.count", customerEntity.getCount()).list();
									if(contactList!=null&&contactList.size()>0) {
										for(ContactPersonIdentification contact:contactList) {
											if(contact.getEmail()!=null&&!contact.getEmail().equals("")&&!toEmailList1.contains(contact.getEmail())) {
												toEmailList1.add(contact.getEmail());
											}
											if(contact.getEmail1()!=null&&!contact.getEmail1().equals("")&&!toEmailList1.contains(contact.getEmail1())) {
												toEmailList1.add(contact.getEmail1());
											}
										}
										
									}
									}catch(Exception e) {
										
									}
								}
								
								
								
							}
							logger.log(Level.SEVERE,"toEmailList1 "+toEmailList1);
//							logger.log(Level.SEVERE,"Payment count == "+customerPaymentlist.get(p).getCount());
							logger.log(Level.SEVERE,"customerEntity count == "+customerPaymentlist.get(p).getPersonInfo().getCount());
							
							try{
								customerName = getFirstLetterUpperCase(customerEntity.getFullname());
							}catch(Exception e){
								customerName = customerEntity.getFullname();
							}
							}				
			
						//Table header 1
						if(p==0){
							tbl1_header.add("Sr No");
							tbl1_header.add("Invoice Id");
							tbl1_header.add("Invoice Date");
							tbl1_header.add("Payment Due Date");
							tbl1_header.add("Reference No");
							tbl1_header.add("Remark");
							tbl1_header.add("Amount");
							tbl1_header.add("Ageing");
						}
							
						tbl1.add(p+1+"");
						tbl1.add(customerPaymentlist.get(p).getInvoiceCount()+"");
						tbl1.add(fmt.format(customerPaymentlist.get(p).getInvoiceDate()));
						tbl1.add(fmt.format(customerPaymentlist.get(p).getPaymentDate()));
						Invoice invoiceEntity = ofy().load().type(Invoice.class).filter("count", customerPaymentlist.get(p).getInvoiceCount()).filter("companyId", customerPaymentlist.get(p).getCompanyId()).first().now();
						if(invoiceEntity.getRefNumber()!=null){
							tbl1.add(invoiceEntity.getRefNumber());
						}else{
							tbl1.add(" ");
						}
						if(invoiceEntity.getRemark()!=null){
							tbl1.add(invoiceEntity.getRemark());
						}else{
							tbl1.add(" ");
						}
						tbl1.add(customerPaymentlist.get(p).getPaymentAmt()+"");
						
						
						String StringPaymentDate = fmtPaymentDate.format(customerPaymentlist.get(p).getPaymentDate());
		   				Date d1 = null;
		   				Date d2 = null;
		   				try {
		   					d1 = df.parse(StringPaymentDate);
			   				d2 = df.parse(todayDateString);
						} catch (Exception e) {
							e.printStackTrace();
						}
		   				
		   				long diff = d2.getTime() - d1.getTime();
						long diffDays = diff / (24 * 60 * 60 * 1000);
						tbl1.add(diffDays+"");
						
						ServerAppUtility serverAppUtility=new ServerAppUtility();
						logger.log(Level.SEVERE,"invoice id="+customerPaymentlist.get(p).getInvoiceCount());
						compPayment=serverAppUtility.loadCompanyPaymentByDocument("invoice", customerPaymentlist.get(p).getInvoiceCount(), customerPaymentlist.get(p).getCompanyId());
				
						if(p==customerPaymentlist.size()-1){
							p++;
							tbl1.add("Total");
							tbl1.add(totalAmount+"");
						}
//						logger.log(Level.SEVERE,"In the ProsConfig !=null   3 ");
					
						}
					if(toEmailList1!=null&&toEmailList1.size()>0)
						callEmailSendingCode(cronDetails, company, compPayment, tbl1, tbl1_header, customerName, company.getEmail(), toEmailList1, msgBody, cronEmail, mailTitl, branchList,null);
		
				}
				/*
				 * If branch as company active then need to sort customer payments as per branch.
				 * Need to send email and signature of branch.
				 */
				
				else {
					Map<String,List<CustomerPayment>> branchwisePaymentMap= (Map<String, List<CustomerPayment>>) customerPaymentlist.stream().collect(Collectors.groupingBy(CustomerPayment::getBranch,Collectors.toList()));
					Set s2 = branchwisePaymentMap.entrySet();
					Iterator iterator2 =  s2.iterator();
					
					
					while(iterator2.hasNext()){
						ArrayList<CustomerPayment> branchwisecustomerPaymentlist = new ArrayList<CustomerPayment>();
						Map.Entry<Integer, ArrayList<CustomerPayment>> entry = (Map.Entry<Integer, ArrayList<CustomerPayment>>) iterator2.next();
						branchwisecustomerPaymentlist.addAll(entry.getValue());
						logger.log(Level.SEVERE,"branchwisecustomerPaymentlist size "+branchwisecustomerPaymentlist.size());
						
						tbl1_header=new ArrayList<String>();
						tbl1=new ArrayList<String>();
						for(int p=0;p<branchwisecustomerPaymentlist.size();p++){
							
							totalAmount += branchwisecustomerPaymentlist.get(p).getPaymentAmt();
							if(p==0){
								Customer customerEntity=ofy().load().type(Customer.class).filter("companyId", company.getCompanyId()).filter("count", branchwisecustomerPaymentlist.get(p).getPersonInfo().getCount()).first().now();
								// email id is added to emailList
								toEmailList1=new ArrayList<String>();
//								toEmailList1.add(customerEntity.getEmail());
								
								/*
								 * Ashwini Patil 
								 * Date:12-1-2023
								 * Emails were getting sent multiple times to customer as well as branch
								 * Since crondetails are getting run in loop twice. First time for employee role in method customerPaymentARlistForCronJob and then again in current method
								 * This issue happens only when there are more than one configuration for this cron job
								 * so while calling this method added one more parameter "callFrom" to stop sending multiple emails 
								 */
								if(!cronDetails.getEmployeeRole().equalsIgnoreCase(AppConstants.CUSTOMER)&&callFor.equalsIgnoreCase("NonCustomer")){
									if(employee!=null)
										toEmailList1.add(employee.getEmail());
								}
								else if(callFor.equalsIgnoreCase(AppConstants.CUSTOMER)){  
									toEmailList1.add(customerEntity.getEmail());
									
									if(customerEntity!=null&&ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Company", "SendAutoEmailToAllContactsOfCustomer", company.getCompanyId())) {
										
										/*
										 * Ashwini Patil
										 * Date:9-07-2024
										 * Ultra pest requirement - send payment reminder to all contacts of the customer
										 */
										try {
										List<ContactPersonIdentification> contactList= ofy().load().type(ContactPersonIdentification.class).filter("companyId", company.getCompanyId())
												.filter("personInfo.count", customerEntity.getCount()).list();
										if(contactList!=null&&contactList.size()>0) {
											for(ContactPersonIdentification contact:contactList) {
												if(contact.getEmail()!=null&&!contact.getEmail().equals("")&&!toEmailList1.contains(contact.getEmail())) {
													toEmailList1.add(contact.getEmail());
												}
												if(contact.getEmail1()!=null&&!contact.getEmail1().equals("")&&!toEmailList1.contains(contact.getEmail1())) {
													toEmailList1.add(contact.getEmail1());
												}
											}
											
										}
										}catch(Exception e) {
											
										}
									}
								}
								logger.log(Level.SEVERE,"toEmailList1 "+toEmailList1);
								logger.log(Level.SEVERE,"Payment count == "+branchwisecustomerPaymentlist.get(p).getCount());
								logger.log(Level.SEVERE,"customerEntity count == "+branchwisecustomerPaymentlist.get(p).getPersonInfo().getCount());
								
								try{
									customerName = getFirstLetterUpperCase(customerEntity.getFullname());
								}catch(Exception e){
									customerName = customerEntity.getFullname();
								}
							}				
				
							//Table header 1
							if(p==0){
								tbl1_header.add("Sr No");
								tbl1_header.add("Invoice Id");
								tbl1_header.add("Invoice Date");
								tbl1_header.add("Payment Due Date");
								tbl1_header.add("Reference No");
								tbl1_header.add("Remark");
								tbl1_header.add("Amount");
								tbl1_header.add("Ageing");
							}
								
							tbl1.add(p+1+"");
							tbl1.add(branchwisecustomerPaymentlist.get(p).getInvoiceCount()+"");
							tbl1.add(fmt.format(branchwisecustomerPaymentlist.get(p).getInvoiceDate()));
							tbl1.add(fmt.format(branchwisecustomerPaymentlist.get(p).getPaymentDate()));
							Invoice invoiceEntity = ofy().load().type(Invoice.class).filter("count", branchwisecustomerPaymentlist.get(p).getInvoiceCount()).filter("companyId", customerPaymentlist.get(p).getCompanyId()).first().now();
							if(invoiceEntity.getRefNumber()!=null){
								tbl1.add(invoiceEntity.getRefNumber());
							}else{
								tbl1.add(" ");
							}
							if(invoiceEntity.getRemark()!=null){
								tbl1.add(invoiceEntity.getRemark());
							}else{
								tbl1.add(" ");
							}
							tbl1.add(branchwisecustomerPaymentlist.get(p).getPaymentAmt()+"");
							
							
							String StringPaymentDate = fmtPaymentDate.format(branchwisecustomerPaymentlist.get(p).getPaymentDate());
			   				Date d1 = null;
			   				Date d2 = null;
			   				try {
			   					d1 = df.parse(StringPaymentDate);
				   				d2 = df.parse(todayDateString);
							} catch (Exception e) {
								e.printStackTrace();
							}
			   				
			   				long diff = d2.getTime() - d1.getTime();
							long diffDays = diff / (24 * 60 * 60 * 1000);
							tbl1.add(diffDays+"");
							
							ServerAppUtility serverAppUtility=new ServerAppUtility();
							logger.log(Level.SEVERE,"invoice id="+branchwisecustomerPaymentlist.get(p).getInvoiceCount());
							compPayment=serverAppUtility.loadCompanyPaymentByDocument("invoice", branchwisecustomerPaymentlist.get(p).getInvoiceCount(), branchwisecustomerPaymentlist.get(p).getCompanyId());
					
							if(p==branchwisecustomerPaymentlist.size()-1){
								p++;
								tbl1.add("Total");
								tbl1.add(totalAmount+"");
							}
//							logger.log(Level.SEVERE,"In the ProsConfig !=null   3 ");
						
						}
						if(toEmailList1!=null&&toEmailList1.size()>0)
							callEmailSendingCode(cronDetails, company, compPayment, tbl1, tbl1_header, customerName, branchOrcompanyEmailMap.get(branchwisecustomerPaymentlist.get(0).getBranch()), toEmailList1, msgBody, cronEmail, mailTitl, branchList,branchSignatureMap.get(branchwisecustomerPaymentlist.get(0).getBranch()));
					
					}
				}
			
			}
		 }
		
		}	//end of for loop
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void sendPaymentReminderMessage(List<CustomerPayment> custPaymentEntity,CronJobConfigrationDetails cronDetails, Company company, Long cellNumber) {

		
		for(CustomerPayment customerPayment : custPaymentEntity){
			/**
			 * @author Vijay Date :- 09-08-2021
			 * Des :- added below method to check customer DND status if customer DND status is Active 
			 * then SMS will not send to customer
			 */
			ServerAppUtility serverapputility = new ServerAppUtility();
			boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(customerPayment.getPersonInfo().getCount(),customerPayment.getCompanyId());
			logger.log(Level.SEVERE,"dndstatusFlag "+dndstatusFlag);

			if(!dndstatusFlag){
			
				
				String paymentdate = fmtPaymentDate.format(customerPayment.getPaymentDate());
				String companyName = company.getBusinessUnitName();
				
				/**
				 * Date 29 jun 2017 added by vijay for Eco friendly
				 */
				
				ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","SMS").filter("status",true).filter("companyId", company.getCompanyId()).first().now();
				ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", company.getCompanyId()).filter("processName","SMS").first().now();				
					
				boolean prossconfigBhashSMSflag = false;
				if(processName!=null){
				if(processConfig!=null){
					if(processConfig.isConfigStatus()){
						for(int l=0;l<processConfig.getProcessList().size();l++){
							if(processConfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("SMS") && processConfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("BHASHSMSAPI") && processConfig.getProcessList().get(l).isStatus()==true){
								prossconfigBhashSMSflag=true;
							}
						}
				   }
				}
				}
				logger.log(Level.SEVERE,"process config  BHASH SMS Flag =="+prossconfigBhashSMSflag);
				String strcustomerName=customerPayment.getPersonInfo().getFullName();
				if(prossconfigBhashSMSflag){
					strcustomerName = getCustomerName(strcustomerName,company.getCompanyId());
				}
				/**
				 * ends here
				 */
				logger.log(Level.SEVERE,"cronDetails.getTemplateName()"+cronDetails.getTemplateName());

//				SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId", company.getCompanyId()).filter("event","Amount Receivable Due Cron Job").filter("status", true).first().now();
				SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId", company.getCompanyId()).filter("event",cronDetails.getTemplateName()).filter("status", true).first().now();
				logger.log(Level.SEVERE,"smsEntity ======= SMS "+smsEntity);

				
				String templateMsgwithBraces = smsEntity.getMessage();
				String customerName = templateMsgwithBraces.replace("{CustomerName}",strcustomerName +"" );
				String paymentAmount = customerName.replace("{PayableAmt}",customerPayment.getPaymentAmt() +"" );
				String paymentDatemsg = paymentAmount.replace("{PaymentDate}", paymentdate );
				String actualMsg = paymentDatemsg.replace("{companyName}", companyName);
				
				
				logger.log(Level.SEVERE,"actualMsg "+actualMsg);

				if(cronDetails.getEmployeeRole().equalsIgnoreCase(AppConstants.CUSTOMER)){
					cellNumber = customerPayment.getCellNumber();
				}
				logger.log(Level.SEVERE,"cellNumber  "+cellNumber);
				
				if(cellNumber!=0){
					logger.log(Level.SEVERE," cronDetails.getCommunicationChannel()  "+cronDetails.getCommunicationChannel());

					SmsServiceImpl smsimpl = new SmsServiceImpl();
					if(cronDetails.getCommunicationChannel().equals(AppConstants.SMS)){
						String smsResponse = smsimpl.sendSmsToClient(actualMsg, cellNumber+"", company.getCompanyId(), AppConstants.SMS);
						logger.log(Level.SEVERE,"for sending sms smsResponse"+smsResponse);
					}
					if(cronDetails.getCommunicationChannel().equals(AppConstants.WHATSAPP)){
						String response = smsimpl.sendMessageOnWhatsApp(company.getCompanyId(), cellNumber+"", actualMsg);
						logger.log(Level.SEVERE,"whats app response"+response);
					}
					
				}
				
				
			}
			
			
		}
		
		
	}
	private String getCustomerName(String customerName, long companyId) {
		
		String custName;
		Customer customer = ofy().load().type(Customer.class).filter("companyId", companyId).filter("fullname", customerName).first().now();
		if(customer!=null){
			if(!customer.getCustPrintableName().equals("")){
				custName = customer.getCustPrintableName();
			}else{
				custName = customer.getFullname();
			}
			
		}else{
			custName = customerName;
		}
		
		return custName;
	}
	
	/**
	 * Issue reported : cron job email not getting sent through branch email id if branchascompany active
	 * 
	 */
	private void callEmailSendingCode(CronJobConfigrationDetails cronDetails,Company company,CompanyPayment compPayment ,ArrayList<String> tbl1,ArrayList<String> tbl1_header, String customerName,String companyEmail, ArrayList<String> toEmailList1,	String msgBody,Email cronEmail, String mailTitl,List<String> branchList,String signature)
	{
	
		logger.log(Level.SEVERE,"Client Name =="+customerName);

		logger.log(Level.SEVERE,"tbl1 size =="+tbl1.size());
		EmailTemplate emailTemplate = null;
		if(cronDetails.getTemplateName()!=null && !cronDetails.getTemplateName().equals("")){
			emailTemplate = ofy().load().type(EmailTemplate.class).filter("companyId", company.getCompanyId())
					.filter("templateName", cronDetails.getTemplateName())	
					.filter("templateStatus", true).first().now();
		}
		else{
			emailTemplate = ofy().load().type(EmailTemplate.class).filter("companyId", company.getCompanyId())
					.filter("templateName", "PaymentDueEmail")	
					.filter("templateStatus", true).first().now();
		}

		if(emailTemplate!=null){
			logger.log(Level.SEVERE,"in emailTemplate PaymentDueEmail");
			String mailSubject = emailTemplate.getSubject();
			if(mailSubject.contains("<Customer>")) //added on 4-1-2024
				mailSubject=mailSubject.replace("<Customer>",customerName );
			String emailbody = emailTemplate.getEmailBody();
			logger.log(Level.SEVERE,"Email body="+emailbody);
			String resultString = emailbody.replaceAll("[\n]", "<br>");
			String resultString1 = resultString.replaceAll("[\\s]", "&nbsp;");
			if(resultString1.contains("<Customer>")) {
				resultString1=resultString1.replace("<Customer>",customerName );	
			}
			if(resultString1.contains("<Table>")){
				logger.log(Level.SEVERE,"in if(resultString1.contains(<table>)");
				StringBuilder builder = new StringBuilder();
				
				builder.append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\">");
				builder.append("<tr>");
				for (String header : tbl1_header)
					builder.append("<th>" + header + "</th>");
				builder.append("</tr>");

				System.out.println("Size of table 1 " + tbl1.size());

				builder.append("<tr>");
				for (int r = 0; r < tbl1.size(); r++) {
					if (r == tbl1.size() - 2) {
							builder.append("<td colspan=6>" + tbl1.get(r) + "</td>");
							System.out.println("Table Started 1" + tbl1.get(r));
							if (r > 0 && (r + 1) % tbl1_header.size() == 0) {
								builder.append("</tr><tr>");
							}
						}
						else {
							builder.append("<td>" + tbl1.get(r) + "</td>");
							System.out.println("Table Started 1" + tbl1.get(r));
							if (r > 0 && (r + 1) % tbl1_header.size() == 0) {
								builder.append("</tr><tr>");
							}
						}
				}
				builder.append(" </tr>  ");
				builder.append("</table></b></br>");
				resultString1=resultString1.replace("<Table>", builder.toString());	
				logger.log(Level.SEVERE,"result after table="+	resultString1);	
			}
			if(resultString1.contains("<Bank&nbsp;Details>")){
				logger.log(Level.SEVERE,"in if(resultString1.contains(<bankdetails>)");
				if(compPayment!=null) {
					logger.log(Level.SEVERE,"in if(compPayment!=null)");
					String bankDetails = "<table border=0>";
					
					bankDetails+="<tr><td>Account Name</td><td>:"+compPayment.getPaymentComName()+"</td></tr>";
					bankDetails+="<tr><td>Account Number</td><td>:"+compPayment.getPaymentAccountNo()+"</td></tr>";
					bankDetails+="<tr><td>Bank Name</td><td>:"+compPayment.getPaymentBankName()+"</td></tr>";
					bankDetails+="<tr><td>Address</td><td>:"+compPayment.getAddressInfo().getCompleteAddress()+"</td></tr>";
					if(compPayment.getPaymentIFSCcode() != null&&!compPayment.getPaymentIFSCcode().equals(""))
						bankDetails+="<tr><td>IFSC/RTGS/NEFT Code</td><td>:"+compPayment.getPaymentIFSCcode()+"</td></tr>";
					if(compPayment.getPaymentMICRcode() != null&&!compPayment.getPaymentMICRcode().equals(""))
						bankDetails+="<tr><td>MICR Code</td><td>:"+compPayment.getPaymentMICRcode()+"</td></tr>";
					if(compPayment.getPaymentSWIFTcode() != null&&!compPayment.getPaymentSWIFTcode().equals(""))
						bankDetails+="<tr><td>Swift code</td><td>:"+compPayment.getPaymentSWIFTcode()+"</td></tr>";
//					if(compPayment.get != null&&!compPayment.getPaymentSWIFTcode().equals(""))
//						bankDetails+="<tr><td>GPay and PhonePay</td><td>:"+compPayment.getPaymentAccountNo()+"</td></tr>";
//					bankDetails+="<tr><td>UPI ID</td><td>:"+compPayment.getPaymentAccountNo()+"</td></tr>";

					bankDetails += "</table>";
					resultString1=resultString1.replace("<Bank&nbsp;Details>", bankDetails);
					logger.log(Level.SEVERE,"result after bankdetails="+	resultString1);
				}
				else {
					resultString1=resultString1.replace("<bankdetails>", "");
				}						
			}
			if(resultString1.contains("<Company&nbsp;Signature>")) {
				if(signature!=null&&!signature.equals(""))
					resultString1=resultString1.replace("<Company&nbsp;Signature>",signature);							
				else
					resultString1=resultString1.replace("<Company&nbsp;Signature>", ServerAppUtility.getCompanySignature(company,null));			
			}
			logger.log(Level.SEVERE,"Email body after replacement="+resultString1);
			logger.log(Level.SEVERE,"toEmailList size::: "+toEmailList1.size());
//			logger.log(Level.SEVERE,"toEmailList size::: "+toEmailList1.size()+" Branch List SIze : "+branchList.size());

			msgBody=resultString1;
			
			 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, company.getCompanyId())){
					SendGridEmailServlet sdEmail=new SendGridEmailServlet();
					sdEmail.sendMailWithSendGrid(companyEmail, toEmailList1, null, null, mailSubject, msgBody, "text/html",null,null,"application/pdf",null);

			 }
			 else {
			try {
				cronEmail.PaymentReminderCronJobTemplateEmailToClient(toEmailList1, mailSubject, mailTitl, company, msgBody,null);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
			 }
			logger.log(Level.SEVERE,"Email sent using template");
		}else {
			msgBody ="</br></br> Dear "+customerName+ ", <br><br> As per our record following payments are due from you. We request you to pay the dues at the earliest. <br><br>";

			String paymentTableTitle = "Below are the payment modes which are available to you to make the payment";
			String paymentTableTitle2 = "Payment Modes";
			
			ArrayList<String> tbl2_header=new ArrayList<String>();
			tbl2_header.add("Modes");
			tbl2_header.add("Favoring");
			tbl2_header.add("Details");
			
			String msgBodyTwo = null;
			String msgbodyThree = null;
			
			String footermsg = null;
			
			String mailSubject="Payment Due Reminder - "+company.getBusinessUnitName();
			logger.log(Level.SEVERE,"toEmailList size::: "+toEmailList1.size()+" Branch List SIze : "+branchList.size());

			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, company.getCompanyId())){
				SendGridEmailServlet sdEmail=new SendGridEmailServlet();
				sdEmail.sendMailWithSendGrid(companyEmail, toEmailList1, null, null, mailSubject, msgBody, "text/html",null,null,"application/pdf",null);

			}
			else {
				try {
					cronEmail.PaymentReminderCronJobEmailToClient(toEmailList1, mailSubject, mailTitl, company, msgBody,null,null, tbl1_header, tbl1,paymentTableTitle, paymentTableTitle2, tbl2_header,  msgBodyTwo, msgbodyThree, null, footermsg, null, true, true);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
			
			}
		
		}
	}

}
