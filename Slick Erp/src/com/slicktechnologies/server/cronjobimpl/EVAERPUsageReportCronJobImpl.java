package com.slicktechnologies.server.cronjobimpl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.assesmentreport.AssesmentReportEmbedForTable;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.email.SendGridEmailServlet;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;


public class EVAERPUsageReportCronJobImpl extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4537840069276159696L;
	
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private DateFormat fmtPaymentDate = new SimpleDateFormat("dd/MM/yyyy");
	Company companyobj;
	
	 Logger logger = Logger.getLogger("EVAERPUsageReportCronJobImpl.class");
	 

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}


	
	private String getFirstLetterUpperCase(String customerFullName) {
		
		String customerName="";
		String[] customerNameSpaceSpilt=customerFullName.split(" ");
		int count=0;
		for (String name : customerNameSpaceSpilt) 
		{
			String nameLowerCase=name.toLowerCase();
			if(count==0)
			{
				customerName=customerName+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);
			}
			else
			{
				customerName=customerName+" "+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);	
			}
			
			count=count+1;
			
		}
		return customerName;  
		
		}
	
	
	public void processlistForCronJob(String cronList, HashSet<String> empRoleList) {
		
		
		
		Date today=DateUtility.getDateWithTimeZone("IST", new Date());
		
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmtPaymentDate.setTimeZone(TimeZone.getTimeZone("IST"));
		
	
	try{
 
	/********************************Adding Companies in the list ***********************/
	 
		List<Company> compEntity = ofy().load().type(Company.class).list();
		if(compEntity.size()>0){
	
		logger.log(Level.SEVERE,"If compEntity size > 0");
		
		logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
		if(empRoleList!=null)
		logger.log(Level.SEVERE,"Size of empRoleList"+empRoleList.size());
		
			for(int i=0;i<compEntity.size();i++){
				boolean emailInitiatedFlag=false;
				for(String empRoleName :empRoleList){
				
					if(empRoleName.equalsIgnoreCase(AppConstants.ADMINUSER)){
						List<Employee>  employeeList=new ArrayList<Employee>();
						employeeList=ofy().load().type(Employee.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("roleName", empRoleName).filter("status", true).list();
						
						if(employeeList!=null) {							
							logger.log(Level.SEVERE,""+empRoleName+" "+"Employee List size::: "+employeeList.size());
							for(Employee emp:employeeList) {
								if(emp.getEmail()!=null&&!emp.getEmail().equals("")) {
									sendReportToADMINAndEVASpport(cronList,emp,compEntity.get(i),today);
									emailInitiatedFlag=true;
								}
							}
							}
						else
							logger.log(Level.SEVERE,"No employee found with role "+empRoleName);
					}
				}
				if(!emailInitiatedFlag) {
					sendReportToADMINAndEVASpport(cronList,null,compEntity.get(i),today);					
				}
			}
		}
		else{
			logger.log(Level.SEVERE,"No Company found from cron job ");	
			System.out.println("No Company found from cron job ");
		}
	 }catch(Exception e2){
		 e2.printStackTrace();
	 }
				
	}


	
	private void sendReportToADMINAndEVASpport(String cronList, Employee emp, Company company, Date today) {
		String logs="";
		try {
			companyobj=company;
		
		 /******************************Converting todayDate to String format*****************/
		 
		 Date todaysDate = new Date();
		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 String todayDateString = df.format(todaysDate);
		 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);
		 
		 
		Email cronEmail = new Email();
//		CompanyPayment compPayment=null;
		
		//Copied from another cron job
		Gson gson = new Gson();
		JSONArray jsonarr = null;
		try {
			jsonarr=new JSONArray(cronList.trim());
		} catch (JSONException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		CronJobConfigrationDetails cronDetails =null;
		
		
		for(int k=0;k<jsonarr.length();k++){
			logger.log(Level.SEVERE,"JSON ARRAY LENGTH :: "+jsonarr.length());
			logs+="<br/>"+"JSON ARRAY LENGTH :: "+jsonarr.length();
			JSONObject jsonObj = jsonarr.getJSONObject(k);
			cronDetails = new CronJobConfigrationDetails();
			
			cronDetails = gson.fromJson(jsonObj.toString(), CronJobConfigrationDetails.class);
			
			if(cronDetails.getEmployeeRole().equalsIgnoreCase(AppConstants.ADMINUSER)) {
			
			
			Calendar cal=Calendar.getInstance();
			cal.setTime(today);
			
			int diffday = 0;
			if(cronDetails.isOverdueStatus() && cronDetails.getOverdueDays()>0){
//				cal.add(Calendar.DATE,cronDetails.getOverdueDays() * -1);
				diffday = cronDetails.getOverdueDays()*-1;
			}else{
				diffday = cronDetails.getInterval();
			 }
			cal.add(Calendar.DATE, diffday);
			logger.log(Level.SEVERE,"diffday1="+diffday+"cal="+cal.getTime());
			logs+="<br/>"+"diffday1="+diffday+"cal="+cal.getTime();
			Date dateForFilter=null;
			
			try {
				dateForFilter=fmtPaymentDate.parse(fmtPaymentDate.format(cal.getTime()));
				cal.set(Calendar.HOUR_OF_DAY,23);
				cal.set(Calendar.MINUTE,59);
				cal.set(Calendar.SECOND,59);
				cal.set(Calendar.MILLISECOND,999);
				dateForFilter=cal.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Date After setting the Time"+dateForFilter);
			logs+="<br/>"+"Date After setting the Time"+dateForFilter;
			
			Calendar cal1=Calendar.getInstance();
			cal1.setTime(today);
			cal1.add(Calendar.DATE, 0);
			
			Date interdateForFilter=null;
			
			try {
				interdateForFilter=fmtPaymentDate.parse(fmtPaymentDate.format(cal1.getTime()));
				cal1.set(Calendar.HOUR_OF_DAY,0);
				cal1.set(Calendar.MINUTE,0);
				cal1.set(Calendar.SECOND,0);
				cal1.set(Calendar.MILLISECOND,0);
				interdateForFilter=cal1.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Date After setting the Time interdateForFilter"+interdateForFilter);
			logs+="<br/>"+"Date After setting the Time interdateForFilter"+interdateForFilter;
		//End of copy
		
		logger.log(Level.SEVERE,"In the for loop");
			logs+="<br/>"+"In the for loop";
		
	/********************************Reading records ***********************/
		List<Contract> contractList=new ArrayList<Contract>();
		List<Contract> tempcontractList=null;
		List<Invoice> invoiceList=null;
		List<Service> tempserviceList=null;
		List<Service> serviceList=new ArrayList<Service>();
		
		if(cronDetails.isOverdueStatus()){
			logger.log(Level.SEVERE,"In isOverdueStatus()");
			logs+="<br/>"+"In isOverdueStatus()";
			
			//since there is no index for approvalDate, using creation date
			try {
			tempcontractList = ofy().load().type(Contract.class)
					.filter("companyId",company.getCompanyId())
					.filter("creationDate >=",dateForFilter)
					.filter("creationDate <=",interdateForFilter)
					.list();
			}catch(Exception e) {
				e.printStackTrace();
			}
			if(tempcontractList!=null) {
				for(Contract c:tempcontractList) {
					if(c.getStatus().equals(Contract.APPROVED))
						contractList.add(c);
				}
			}
			try {
			invoiceList = ofy().load().type(Invoice.class)
					.filter("companyId",company.getCompanyId())
					.filter("invoiceDate >=",dateForFilter)
					.filter("invoiceDate <=",interdateForFilter)
					.list();
			}catch(Exception e) {
				e.printStackTrace();
			}
			try {
			tempserviceList = ofy().load().type(Service.class)
					.filter("companyId",company.getCompanyId())
					.filter("serviceDate >=",dateForFilter)
					.filter("serviceDate <=",interdateForFilter)
					.list();
			}catch(Exception e) {
				e.printStackTrace();
			}
			if(tempserviceList!=null) {
				for(Service s:tempserviceList) {
					if(s.getStatus().equals(Service.SERVICESTATUSCOMPLETED))
						serviceList.add(s);
				}
			}
			
			
			
		}else{
			try {
				tempcontractList = ofy().load().type(Contract.class)
					.filter("companyId",company.getCompanyId())
					.filter("creationDate >=",interdateForFilter)
					.filter("creationDate <=",dateForFilter)					
					.list();
			}catch(Exception e) {
				e.printStackTrace();
			}
			if(tempcontractList!=null) {
				for(Contract c:tempcontractList) {
					if(c.getStatus().equals(Contract.APPROVED))
						contractList.add(c);
				}
			}
			try {
			invoiceList = ofy().load().type(Invoice.class)
					.filter("companyId",company.getCompanyId())
					.filter("invoiceDate >=",interdateForFilter)
					.filter("invoiceDate <=",dateForFilter)					
					.list();
			}catch(Exception e) {
				e.printStackTrace();
			}
			try {
				tempserviceList = ofy().load().type(Service.class)
					.filter("companyId",company.getCompanyId())
					.filter("serviceDate >=",interdateForFilter)
					.filter("serviceDate <=",dateForFilter)					
					.list();
			}catch(Exception e) {
				e.printStackTrace();
			}
			if(tempserviceList!=null) {
				for(Service s:tempserviceList) {
					if(s.getStatus().equals(Service.SERVICESTATUSCOMPLETED))
						serviceList.add(s);
				}
			}
		}	
		
			
		logger.log(Level.SEVERE,"over due date is date Is:"+dateForFilter + " todays date -- " + interdateForFilter);	
		logger.log(Level.SEVERE,"Today date Is:"+dateForFilter);	
		logs+="<br/>"+"Today date Is:"+dateForFilter;
			
			boolean communicationChannelEmailFlag = false;

			if(cronDetails.getCommunicationChannel()==null || cronDetails.getCommunicationChannel().equals("")){
				communicationChannelEmailFlag = true;
			}
			if(cronDetails.getCommunicationChannel()!=null && !cronDetails.getCommunicationChannel().equals("")
					&&  cronDetails.getCommunicationChannel().equals(AppConstants.EMAIL)){
				communicationChannelEmailFlag = true;
			}
			logger.log(Level.SEVERE,"communicationChannelEmailFlag "+communicationChannelEmailFlag);
			logs+="<br/>"+"communicationChannelEmailFlag "+communicationChannelEmailFlag;
			if(communicationChannelEmailFlag){
				
				ArrayList<String> toEmailList=new ArrayList<String>();	
				
				if(emp!=null&&emp.getEmail()!=null&&!emp.getEmail().equals("")) {
					logger.log(Level.SEVERE,"emp not null. adding "+emp.getEmail()+" to toemaillist");
					toEmailList.add(emp.getEmail());
				
				}else{
					logger.log(Level.SEVERE,"no email found in employees of specified role.");
					logs+="<br/>"+"no email found in employees";
					toEmailList.add("support@evasoftwaresolutions.com");
				}
				
				logger.log(Level.SEVERE,"toEmailList "+toEmailList);
				logs+="<br/>"+"toEmailList "+toEmailList;
				
				if(contractList!=null) {
					if(contractList.size()<=5)
						sendlowUsageReport(company,emp,toEmailList,contractList,invoiceList,serviceList,dateForFilter,interdateForFilter);
					else
						sendUsageReport(company,emp,toEmailList,contractList,invoiceList,serviceList,dateForFilter,interdateForFilter);
				}else {
					sendlowUsageReport(company,emp,toEmailList,contractList,invoiceList,serviceList,dateForFilter,interdateForFilter);
				}
				
		}
		
		}	//end of for loop
		
		} 
		}catch (Exception e) {
			logger.log(Level.SEVERE,"in exception sending failure email to support");
			e.printStackTrace();
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			ArrayList<String> toEmailList=new ArrayList<String>();
			toEmailList.add("support@evasoftwaresolutions.com");
			if(companyobj.getCompanyURL()!=null&&!companyobj.getCompanyURL().equals(""))
				sdEmail.sendMailWithSendGrid("support@evasoftwaresolutions.com", toEmailList, null, null, AppConstants.EVAERPUsageReport+" cronjob failed for "+companyobj.getCompanyURL(), logs+"<br/>"+e.getMessage(), "text/html",null,null,"application/pdf",null);															
			else
				sdEmail.sendMailWithSendGrid("support@evasoftwaresolutions.com", toEmailList, null, null, AppConstants.EVAERPUsageReport+" cronjob failed for "+companyobj.getBusinessUnitName(),logs+"<br/>"+e.getMessage(), "text/html",null,null,"application/pdf",null);															
			
		}
	
	}

	private String getCustomerName(String customerName, long companyId) {
		
		String custName;
		Customer customer = ofy().load().type(Customer.class).filter("companyId", companyId).filter("fullname", customerName).first().now();
		if(customer!=null){
			if(!customer.getCustPrintableName().equals("")){
				custName = customer.getCustPrintableName();
			}else{
				custName = customer.getFullname();
			}
			
		}else{
			custName = customerName;
		}
		
		return custName;
	}
	
	private void sendUsageReport(Company company,Employee emp,ArrayList<String> toEmailList,List<Contract> contractList,List<Invoice> invoiceList,	List<Service> serviceList,Date fromDate,Date toDate) {
		logger.log(Level.SEVERE,"in sendUsageReport");
		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 ArrayList<String> ccList =new ArrayList<String>();
		 ccList.add("support@evasoftwaresolutions.com");
		 
		String companyEmail=null;
		if(company.getEmail()!=null&&!company.getEmail().equals("")) {
					companyEmail=company.getEmail();
					logger.log(Level.SEVERE,"companyEmail "+companyEmail);
		}
			 
		EmailTemplate emailTemplate = null;
		emailTemplate = ofy().load().type(EmailTemplate.class).filter("companyId", company.getCompanyId())
				.filter("templateName", "EVAERPUsageReportEmail")	
				.filter("templateStatus", true).first().now();
		if(emailTemplate!=null){
			logger.log(Level.SEVERE,"in emailTemplate "+emailTemplate.getTemplateName());
			String mailSubject = emailTemplate.getSubject();
			String emailbody = emailTemplate.getEmailBody();

			mailSubject=mailSubject.replace("{CompanyName}", company.getBusinessUnitName());
			mailSubject=mailSubject.replace("{CurrentDate}", df.format(new Date()));
			
			String resultString = emailbody.replaceAll("[\n]", "<br>");
			String resultString1 = resultString.replaceAll("[\\s]", "&nbsp;");
			String msgBody=resultString1;
			
			if(emp!=null)
				msgBody=msgBody.replace("{RecipientName}",emp.getFullname());
			else
				msgBody=msgBody.replace("{RecipientName}","Sir/Madam,");
			
			if(contractList!=null)
				msgBody=msgBody.replace("{ContractCount}",contractList.size()+"");
			else
				msgBody=msgBody.replace("{ContractCount}","0");
			
			if(invoiceList!=null)
				msgBody=msgBody.replace("{InvoiceCount}",invoiceList.size()+"");
			else
				msgBody=msgBody.replace("{InvoiceCount}","0");
			
			if(serviceList!=null)
				msgBody=msgBody.replace("{ServiceCount}",serviceList.size()+"");
			else
				msgBody=msgBody.replace("{ServiceCount}","0");
			
			msgBody=msgBody.replace("{FromDate}",df.format(fromDate));
			msgBody=msgBody.replace("{ToDate}",df.format(toDate));
			msgBody=msgBody.replace("{CompanyName}", company.getBusinessUnitName());
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, company.getCompanyId())){
				if(toEmailList.size()>0&&companyEmail!=null&&!companyEmail.equals("")) {
					
					SendGridEmailServlet sdEmail=new SendGridEmailServlet();
					sdEmail.sendMailWithSendGrid(companyEmail, toEmailList, ccList, null, mailSubject, msgBody, "text/html",null,null,"application/pdf",null);	 	
				}else {
					logger.log(Level.SEVERE,"Customer or Company/Branch email missing");
//					logs+="<br/>"+"Customer or Company/Branch email missing";
				}
			}
			else {
				logger.log(Level.SEVERE,"Could not send email as 'Email'-'EnableSendGridEmailApi' process config inactive.");																	
//				logs+="<br/>"+"Could not send email as 'Email'-'EnableSendGridEmailApi' process config inactive.";
			}
		}else {
			

			logger.log(Level.SEVERE,"in emailTemplate null");
			String mailSubject = "EVA ERP System Usage Report of  "+company.getBusinessUnitName()+" as on "+df.format(new Date());
			String emailbody = "Dear {RecipientName},<br>"
					+ "<br>"
					+ "I hope this email finds you well. I am writing to bring to your attention the current status of the {CompanyName} subscription assigned to your account.<br>"
					+ "<br>"
					+ "As of our records,  find below usage report for your system. <br>"
					+ "subscription that was provided to you. <br>"
					+ "<br>"
					+ "Usage duration ({FromDate} - {ToDate})<br>"
					+ "<br>"
					+ "No. of contracts Approved: {ContractCount}<br>"
					+ "No. of Invoices Approved: {InvoiceCount}<br>"
					+ "No. of Services Completed: {ServiceCount}<br>"
					+ "<br>"
					+ "It is extremely important that you stop all your manual contract/invoice/service completion or from your old system & start generating those in EVA ERP system. This will help you to achieve goal of transforming your business to run on its own.<br>"
					+ "<br>"
					+ "Your feedback is valuable to us, and we want you to drive the implementation to get the long term benefit & free up your key time.<br>"
					+ "<br>"
					+ "Thank you for your attention to this matter. We appreciate your cooperation in optimizing the use of our resources.<br>"
					+ "<br>"
					+ "Kindly note all the trainings shall be completed within first 30 days from the date of sign up.<br>"
					+ "<br>"
					+ "If you have any questions or concerns, please feel free to reply to this email or contact support@evasoftwaresolutions.com<br>"
					+ "<br><br>"
					+ "Thanks & Regards, <br>"
					+ "EVA Support Team<br>"
					+ "Your Technology Partner<br>"
					+ "EVA Software Solutions<br>"
					+ "ERP CRM Solutions | Website | e-commerce | Mobile Applications | Custom Development<br>"
					+ "Mobile - +91 9619390370<br>"
					+ "nitin.kshirsagar@evasoftwaresolutions.com<br>"
					+ "kshirsagarnitin@gmail.com<br>"
					+ "www.evasoftwaresolutions.com<br>"
					+ "A223, Eastern Business District Neptune Mall,<br>"
					+ "LBS Marg, Bhandup (W), Mumbai - 400078<br>"
					+ "https://goo.gl/maps/JTx9ZZSDb5M2<br>"
					+ "Mon - Sat (9:00 AM - 6:00 PM)";

				
			String resultString = emailbody.replaceAll("[\n]", "<br>");
			String resultString1 = resultString.replaceAll("[\\s]", "&nbsp;");
			String msgBody=resultString1;
			
			if(emp!=null)
				msgBody=msgBody.replace("{RecipientName}",emp.getFullname());
			else
				msgBody=msgBody.replace("{RecipientName}","Sir/Madam,");
			
			if(contractList!=null)
				msgBody=msgBody.replace("{ContractCount}",contractList.size()+"");
			else
				msgBody=msgBody.replace("{ContractCount}","0");
			
			if(invoiceList!=null)
				msgBody=msgBody.replace("{InvoiceCount}",invoiceList.size()+"");
			else
				msgBody=msgBody.replace("{InvoiceCount}","0");
			
			if(serviceList!=null)
				msgBody=msgBody.replace("{ServiceCount}",serviceList.size()+"");
			else
				msgBody=msgBody.replace("{ServiceCount}","0");
		
			msgBody=msgBody.replace("{FromDate}",df.format(fromDate));
			msgBody=msgBody.replace("{ToDate}",df.format(toDate));
			msgBody=msgBody.replace("{CompanyName}", company.getBusinessUnitName());
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, company.getCompanyId())){
				if(toEmailList.size()>0&&companyEmail!=null&&!companyEmail.equals("")) {
					
					SendGridEmailServlet sdEmail=new SendGridEmailServlet();
					sdEmail.sendMailWithSendGrid(companyEmail, toEmailList, ccList, null, mailSubject, msgBody, "text/html",null,null,"application/pdf",null);	 	
				}else {
					logger.log(Level.SEVERE,"Customer or Company/Branch email missing");
//					logs+="<br/>"+"Customer or Company/Branch email missing";
				}
			}
			else {
				logger.log(Level.SEVERE,"Could not send email as 'Email'-'EnableSendGridEmailApi' process config inactive.");																	
//				logs+="<br/>"+"Could not send email as 'Email'-'EnableSendGridEmailApi' process config inactive.";
			}
		
		}
	
	}
	
	private void sendlowUsageReport(Company company,Employee emp,ArrayList<String> toEmailList,List<Contract> contractList,List<Invoice> invoiceList,	List<Service> serviceList,Date fromDate,Date toDate) {
		logger.log(Level.SEVERE,"in sendlowUsageReport"); 
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 ArrayList<String> ccList =new ArrayList<String>();
		 ccList.add("support@evasoftwaresolutions.com");
		 
		String companyEmail=null;
		if(company.getEmail()!=null&&!company.getEmail().equals("")) {
				companyEmail=company.getEmail();
				logger.log(Level.SEVERE,"companyEmail "+companyEmail);
		}
		 
		EmailTemplate emailTemplate = null;
		emailTemplate = ofy().load().type(EmailTemplate.class).filter("companyId", company.getCompanyId())
				.filter("templateName", "EVAERPLowUsageReportEmail")	
				.filter("templateStatus", true).first().now();
		if(emailTemplate!=null){
			logger.log(Level.SEVERE,"in emailTemplate "+emailTemplate.getTemplateName());
			String mailSubject = emailTemplate.getSubject();
			String emailbody = emailTemplate.getEmailBody();

			mailSubject=mailSubject.replace("{CompanyName}", company.getBusinessUnitName());
			mailSubject=mailSubject.replace("{CurrentDate}", df.format(new Date()));
			
			String resultString = emailbody.replaceAll("[\n]", "<br>");
			String resultString1 = resultString.replaceAll("[\\s]", "&nbsp;");
			String msgBody=resultString1;
			
			if(emp!=null)
				msgBody=msgBody.replace("{RecipientName}",emp.getFullname());
			else
				msgBody=msgBody.replace("{RecipientName}","Sir/Madam,");
			
			if(contractList!=null)
				msgBody=msgBody.replace("{ContractCount}",contractList.size()+"");
			else
				msgBody=msgBody.replace("{ContractCount}","0");
			
			if(invoiceList!=null)
				msgBody=msgBody.replace("{InvoiceCount}",invoiceList.size()+"");
			else
				msgBody=msgBody.replace("{InvoiceCount}","0");
			
			if(serviceList!=null)
				msgBody=msgBody.replace("{ServiceCount}",serviceList.size()+"");
			else
				msgBody=msgBody.replace("{ServiceCount}","0");
			
			msgBody=msgBody.replace("{FromDate}",df.format(fromDate));
			msgBody=msgBody.replace("{ToDate}",df.format(toDate));
			msgBody=msgBody.replace("{CompanyName}", company.getBusinessUnitName());
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, company.getCompanyId())){
				if(toEmailList.size()>0&&companyEmail!=null&&!companyEmail.equals("")) {
					
					SendGridEmailServlet sdEmail=new SendGridEmailServlet();
					sdEmail.sendMailWithSendGrid(companyEmail, toEmailList, ccList, null, mailSubject, msgBody, "text/html",null,null,"application/pdf",null);	 	
				}else {
					logger.log(Level.SEVERE,"Customer or Company/Branch email missing");
//					logs+="<br/>"+"Customer or Company/Branch email missing";
				}
			}
			else {
				logger.log(Level.SEVERE,"Could not send email as 'Email'-'EnableSendGridEmailApi' process config inactive.");																	
//				logs+="<br/>"+"Could not send email as 'Email'-'EnableSendGridEmailApi' process config inactive.";
			}
		}else {
			

			logger.log(Level.SEVERE,"in emailTemplate null");
			String mailSubject = "Low/No EVA ERP System Usage Report of "+company.getBusinessUnitName()+" as on "+df.format(new Date());
			String emailbody = "Dear {RecipientName},<br>"
					+ "<br>"
					+ "I hope this email finds you well. I am writing to bring to your attention the current status of the [Client Name] subscription assigned to your account.<br>"
					+ "<br>"
					+ "As of our records, it appears that there has been little to no utilization of the EVA ERP License<br>"
					+ "subscription that was provided to you. <br>"
					+ "<br>"
					+ "Usage duration ({FromDate} - {ToDate})<br>"
					+ "<br>"
					+ "No. of contracts Approved: {ContractCount}<br>"
					+ "No. of Invoices Approved: {InvoiceCount}<br>"
					+ "No. of Services Completed: {ServiceCount}<br>"
					+ "<br>"
					+ "It's very important for us to ensure that our resources are optimally allocated, and we want to make sure that the tools and resources available to you are being used effectively.<br>"
					+ "<br>"
					+ "For any software implementation there are two key points <br>"
					+ "<br>"
					+ "A good quality time - Ensure you have a leader who understands your business process & your vision. This should be a senior person.<br>"
					+ "Your involvement - We request you to get involve into the project as you know your business & objective. Review the progress for next weeks. We need only 30 min from you for next 15 days to be successful. <br>"
					+ "<br>"
					+ "<br>"
					+ "If you are facing any challenges or require assistance in using the software, please don't hesitate to reach out to our support team. We are here to help and provide any necessary training or guidance to maximize the benefits of the software for your work.<br>"
					+ "<br>"
					+ "Your feedback is valuable to us, and we want you to drive the implementation to get the long term benefit & free up your key time.<br>"
					+ "<br>"
					+ "Thank you for your attention to this matter. We appreciate your cooperation in optimizing the use of our resources.<br>"
					+ "<br>"
					+ "Kindly note all the trainings shall be completed within first 30 days from the date of sign up.<br>"
					+ "<br>"
					+ "If you have any questions or concerns, please feel free to reply to this email or contact support@evasoftwaresolutions.com<br><br>"+"Thanks & Regards, <br>"
					+ "EVA Support Team<br>"
					+ "Your Technology Partner<br>"
					+ "EVA Software Solutions<br>"
					+ "ERP CRM Solutions | Website | e-commerce | Mobile Applications | Custom Development<br>"
					+ "Mobile - +91 9619390370<br>"
					+ "nitin.kshirsagar@evasoftwaresolutions.com<br>"
					+ "kshirsagarnitin@gmail.com<br>"
					+ "www.evasoftwaresolutions.com<br>"
					+ "A223, Eastern Business District Neptune Mall,<br>"
					+ "LBS Marg, Bhandup (W), Mumbai - 400078<br>"
					+ "https://goo.gl/maps/JTx9ZZSDb5M2<br>"
					+ "Mon - Sat (9:00 AM - 6:00 PM)";

				
			
			String resultString = emailbody.replaceAll("[\n]", "<br>");
			String resultString1 = resultString.replaceAll("[\\s]", "&nbsp;");
			String msgBody=resultString1;
			
			if(emp!=null)
				msgBody=msgBody.replace("{RecipientName}",emp.getFullname());
			else
				msgBody=msgBody.replace("{RecipientName}","Sir/Madam,");
			
			if(contractList!=null)
				msgBody=msgBody.replace("{ContractCount}",contractList.size()+"");
			else
				msgBody=msgBody.replace("{ContractCount}","0");
			
			if(invoiceList!=null)
				msgBody=msgBody.replace("{InvoiceCount}",invoiceList.size()+"");
			else
				msgBody=msgBody.replace("{InvoiceCount}","0");
			
			if(serviceList!=null)
				msgBody=msgBody.replace("{ServiceCount}",serviceList.size()+"");
			else
				msgBody=msgBody.replace("{ServiceCount}","0");
		
			msgBody=msgBody.replace("{FromDate}",df.format(fromDate));
			msgBody=msgBody.replace("{ToDate}",df.format(toDate));
			msgBody=msgBody.replace("{CompanyName}", company.getBusinessUnitName());
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, company.getCompanyId())){
				if(toEmailList.size()>0&&companyEmail!=null&&!companyEmail.equals("")) {
					
					SendGridEmailServlet sdEmail=new SendGridEmailServlet();
					sdEmail.sendMailWithSendGrid(companyEmail, toEmailList, ccList, null, mailSubject, msgBody, "text/html",null,null,"application/pdf",null);	 	
				}else {
					logger.log(Level.SEVERE,"Customer or Company/Branch email missing");
//					logs+="<br/>"+"Customer or Company/Branch email missing";
				}
			}
			else {
				logger.log(Level.SEVERE,"Could not send email as 'Email'-'EnableSendGridEmailApi' process config inactive.");																	
//				logs+="<br/>"+"Could not send email as 'Email'-'EnableSendGridEmailApi' process config inactive.";
			}
		
		}
	}
	
}

