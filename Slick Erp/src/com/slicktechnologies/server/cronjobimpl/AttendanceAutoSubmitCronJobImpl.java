package com.slicktechnologies.server.cronjobimpl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;







import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.DataMigrationTaskQueueImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

/** Cron job to submit pprevioue date attendace automatically 
 */
public class AttendanceAutoSubmitCronJobImpl extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7274506665123429154L;
	Logger logger =Logger.getLogger("logger");
	SimpleDateFormat monthSdf = new SimpleDateFormat("MMM-yyyy");
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
	DataMigrationTaskQueueImpl impl = new DataMigrationTaskQueueImpl();
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)	throws ServletException, IOException
	{
		saveAttendance();
	}
	
	private void saveAttendance(){
		
		try{
	 
/******************************Converting todayDate to String format*****************/
			
			
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			monthSdf.setTimeZone(TimeZone.getTimeZone("IST"));
			/**
			 * @author Anil 
			 * @since 11-02-2022
			 * some time attendance it time is stored as 4:30 that mean as per the UTC
			 */
			format.setTimeZone(TimeZone.getTimeZone("IST"));
			timeFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			
			Date date = new Date();
			Date newDate = DateUtility.addDaysToDate(date, -1);
			Calendar c = Calendar.getInstance();
			c.setTime(newDate);
			c.set(Calendar.HOUR, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
			Date attendanceDate = c.getTime();
			//logger.log(Level.SEVERE , "attendance date :-" + attendanceDate + "  "+sdf.parse(sdf.format(attendanceDate)));
/********************************Adding Companies in the list ***********************/
	 
	List<Company> compEntity = ofy().load().type(Company.class).list();
	if(compEntity.size()>0)
	{
		//logger.log(Level.SEVERE,"If compEntity size > 0");
		//logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());

		for(int i=0;i<compEntity.size();i++){			
			Company comp=compEntity.get(i);
		//	logger.log(Level.SEVERE,"In the for loop");
		//	logger.log(Level.SEVERE,"Company Name="+c);
		//	logger.log(Level.SEVERE,"The value of i is:" +i);
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CronJob", "AutoSubmitAttendance", comp.getCompanyId())){
				HashMap<String , ArrayList<Attendance>> attendanceMap = new HashMap<String , ArrayList<Attendance>>();			
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance", "LoadHrProjectInApp", comp.getCompanyId())){
					List<HrProject> hrProjectList = ofy().load().type(HrProject.class).filter("companyId", comp.getCompanyId()).
							filter("status", true).list();
				//	logger.log(Level.SEVERE,"hrProjectList size="+hrProjectList.size()+ " companyId="+comp.getCompanyId());
                    createAttendanceFromHrProject(hrProjectList , sdf.parse(sdf.format(attendanceDate)) , comp.getCompanyId());
												
				}else{
					List<ProjectAllocation> projectAllocationList = ofy().load().type(ProjectAllocation.class).filter("companyId", comp.getCompanyId()).
							filter("projectStatus", true).list();
					createAttendanceFromCNCProject(projectAllocationList , sdf.parse(sdf.format(attendanceDate)), comp.getCompanyId());
					
				}
			
		}
		}
	}
	}catch(Exception e){
		
	}
		
		

	}	
	private void createAttendanceFromCNCProject(List<ProjectAllocation> projectAllocationList, Date attendanceDate,long companyId){
		logger.log(Level.SEVERE, "attendance date :" + attendanceDate);
		DataMigrationTaskQueueImpl impl = new DataMigrationTaskQueueImpl();
		HashMap<String , EmployeeProjectAllocation> projectAllocationMap = new HashMap<String , EmployeeProjectAllocation>();
		Set<Integer> empIdSet = new HashSet<Integer>();
		for(ProjectAllocation projectAllocation : projectAllocationList){
			
			/**
			 * @author Vijay @Since 21-09-2022
			 * Loading employee project allocation from different entity
			 */
				try {
					 CommonServiceImpl commonservice = new CommonServiceImpl();
					 ArrayList<EmployeeProjectAllocation> empprojectallocationlist = commonservice.getEmployeeProjectAllocationlist(projectAllocation);
				
			/**
		     * ends here
			 */
//			for(EmployeeProjectAllocation empAllocation : projectAllocation.getEmployeeProjectAllocationList()){
					for(EmployeeProjectAllocation empAllocation : empprojectallocationlist){
						empIdSet.add(Integer.parseInt(empAllocation.getEmployeeId()));
						String key = projectAllocation.getProjectName()+"/-"+empAllocation.getEmployeeId();
						projectAllocationMap.put(key, empAllocation);
						
					}
			
				} catch (Exception e) {
					e.printStackTrace();
				}	
		}
		logger.log(Level.SEVERE, "map before attendance :" + projectAllocationMap.size());
		List<Integer> empIdList = new ArrayList<Integer>(empIdSet);
		List<Attendance> attendanceList=ofy().load().type(Attendance.class)
				.filter("companyId",companyId)
				.filter("attendanceDate",attendanceDate)
				.filter("empId IN", empIdList).list();
		JSONArray array = new JSONArray();
		if(attendanceList != null){
			for(Attendance attendance : attendanceList){	
				String key = attendance.getProjectName()+"/-"+attendance.getEmpId();
				if(projectAllocationMap.containsKey(key)){
					projectAllocationMap.remove(key);
				}
				if(attendance.getStatus().equalsIgnoreCase(AppConstants.APPROVED)){
					continue;
				}
				JSONObject obj = new JSONObject();
			 try {
			 obj.put("count" ,attendance.getCount());	
			 obj.put("createdBy" ,attendance.getCreatedBy());
			 obj.put("markInAddress" , attendance.getMarkInAddress());
			 obj.put("markOutAddress" , attendance.getMarkOutAddress());
			 obj.put("status" , attendance.getStatus());
			
			 obj.put("updatedEmployeeRole" , attendance.getUpdatedEmployeeRole());
			 obj.put("updatedEmployeeName" , attendance.getUpdatedEmployeeName());
			 obj.put("attendanceDate" , sdf.format(attendance.getAttendanceDate()));
			 obj.put("overtimeType" , attendance.getOvertimeType());
			 if(attendance.getInTime() != null && !attendance.getInTime().equals("")){
				 obj.put("inTime" , format.format(attendance.getInTime()));
			 }else{
				 
				 obj.put("inTime" , "");
			 }
			 if(attendance.getOutTime() != null && !attendance.getOutTime().equals("")){
				 obj.put("outTime" , format.format(attendance.getOutTime()));
			 }else{
				 obj.put("outTime" , "");
			 }
			 obj.put("empId" , attendance.getEmpId());
			 obj.put("actionLabel" , attendance.getActionLabel());
			 obj.put("shift" , attendance.getShift());
			 obj.put("projectName" , attendance.getProjectName());
			 obj.put("month" , attendance.getMonth());
			 
			 if(attendance.getTargetInTime() != null && !attendance.getTargetInTime().equals("")){
				 obj.put("targetInTime" , timeFormat.format(attendance.getTargetInTime()));
			 }else{
				 obj.put("targetInTime" , "");
			 }
			 if(attendance.getTargetOutTime() != null && !attendance.getTargetOutTime().equals("")){
				 obj.put("targetOutTime" , timeFormat.format(attendance.getTargetOutTime()));
			 }else{
				 obj.put("targetOutTime" , "");
			 }
			 obj.put("OTStatus" , attendance.getOtStatus());
			 obj.put("requestedHours" , attendance.getRequestedOTHours());
			 obj.put("supervisorName" , "");
			 array.put(obj);
			 } catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		logger.log(Level.SEVERE, "map after attendance :" + projectAllocationMap.size());
		for(Map.Entry<String, EmployeeProjectAllocation> entry :projectAllocationMap.entrySet() ){
			logger.log(Level.SEVERE , "Key set : " + entry.getKey() + " value :" +entry.getValue().getEmployeeId());
			JSONObject obj = new JSONObject();
			 try {
			 obj.put("count" ,0);	
			 obj.put("createdBy" ,"AutoSubmit");
			 obj.put("markInAddress" , "");
			 obj.put("markOutAddress" , "");
			 obj.put("status" , Attendance.ATTENDANCECREATED);
			
			 obj.put("updatedEmployeeRole" , "AutoSubmit");
			 obj.put("updatedEmployeeName" , "AutoSubmit");
			 obj.put("attendanceDate" , sdf.format(attendanceDate));
			 obj.put("overtimeType" , "");
			 obj.put("inTime" , "");
			 obj.put("outTime" , "");
			 obj.put("empId" , entry.getValue().getEmployeeId());
			 obj.put("actionLabel" , "");
			 obj.put("shift" , entry.getValue().getShift());
			 obj.put("projectName" , entry.getKey().split("/-")[0]);
			 obj.put("month" , monthSdf.format(attendanceDate));
			 obj.put("targetInTime" , "");
			 obj.put("targetOutTime" , "");
			 obj.put("OTStatus" , "");
			 obj.put("requestedHours" , 0);
			 obj.put("supervisorName" , "");
			 array.put(obj);
			 } catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
//		DataMigrationTaskQueueImpl impl = new DataMigrationTaskQueueImpl();
		impl.employeeAttendance(companyId, array, Attendance.ATTENDANCEAPPROVED);
		
	}
	private void createAttendanceFromHrProject(List<HrProject> hrProjectList ,Date attendanceDate,long companyId){
		try {
		HashMap<String ,Overtime> hrProjectMap = new HashMap<String , Overtime>();
		
		Set<Integer> empIdSet = new HashSet<Integer>();
		for(HrProject hrProject : hrProjectList){
			logger.log(Level.SEVERE,"Project name:"+hrProject.getProjectName());
			hrProjectMap = new HashMap<String , Overtime>();
			
			/**
			 * @author Vijay Date :- 08-09-2022
			 * Des :- used common method to read OT from project or new entity HrProjectOvertime`
			 */
			ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
			CommonServiceImpl commonservice = new CommonServiceImpl();
			overtimelist = commonservice.getHRProjectOvertimelist(hrProject);
			/**
			 * ends here
			 */
//			for(Overtime ot : project.getOtList()){
			for(Overtime empAllocation : overtimelist){
				empIdSet.add(empAllocation.getEmpId());
//				logger.log(Level.SEVERE,"Ot allocation Empid="+empAllocation.getEmpId());
				if(empAllocation.getEmpId() != 0){
//					logger.log(Level.SEVERE,"adding to hrProjectMap");
				String key = hrProject.getProjectName()+"/-"+empAllocation.getEmpId();				
					hrProjectMap.put(key, empAllocation);
				}
				
			}			
		
		logger.log(Level.SEVERE, "map before attendance :" + hrProjectMap.size() + " " +attendanceDate);
		JSONArray array = new JSONArray();
		List<Integer> empIdList = new ArrayList<Integer>();
		List<Attendance> attendanceList = null;
		if(empIdSet != null && empIdSet.size() >0){
			empIdList = new ArrayList<Integer>(empIdSet);
		
			attendanceList=ofy().load().type(Attendance.class)
				.filter("companyId",companyId)
				.filter("attendanceDate",attendanceDate)
				.filter("empId IN", empIdList)
				.filter("projectName", hrProject.getProjectName())
				.list();
		}
		if(attendanceList != null){
			for(Attendance attendance : attendanceList){
				
				String key = attendance.getProjectName()+"/-"+attendance.getEmpId();
				if(hrProjectMap.containsKey(key)){
					hrProjectMap.remove(key);
				}
				if(attendance.getStatus().equalsIgnoreCase(AppConstants.APPROVED)){
					continue;
				}
				JSONObject obj = new JSONObject();
			 try {
			 obj.put("count" ,attendance.getCount());	
			 obj.put("createdBy" ,attendance.getCreatedBy());
			 obj.put("markInAddress" , attendance.getMarkInAddress());
			 obj.put("markOutAddress" , attendance.getMarkOutAddress());
			 obj.put("status" , attendance.getStatus());
			
			 obj.put("updatedEmployeeRole" , attendance.getUpdatedEmployeeRole());
			 obj.put("updatedEmployeeName" , attendance.getUpdatedEmployeeName());
			 obj.put("attendanceDate" , sdf.format(attendance.getAttendanceDate()));
			 obj.put("overtimeType" , attendance.getOvertimeType());
			 if(attendance.getInTime() != null && !attendance.getInTime().equals("")){
				 obj.put("inTime" , format.format(attendance.getInTime()));
			 }else{
				 
				 obj.put("inTime" , "");
			 }
			 if(attendance.getOutTime() != null && !attendance.getOutTime().equals("")){
				 obj.put("outTime" , format.format(attendance.getOutTime()));
			 }else{
				 obj.put("outTime" , attendance.getOutTime());
			 }
			 obj.put("empId" , attendance.getEmpId());
			 obj.put("actionLabel" , attendance.getActionLabel());
			 obj.put("shift" , attendance.getShift());
			 obj.put("projectName" , attendance.getProjectName());
			 obj.put("month" , attendance.getMonth());
			 
			 if(attendance.getTargetInTime() != null && !attendance.getTargetInTime().equals("")){
				 obj.put("targetInTime" , timeFormat.format(attendance.getTargetInTime()));
			 }else{
				 obj.put("targetInTime" , "");
			 }
			 if(attendance.getTargetOutTime() != null && !attendance.getTargetOutTime().equals("")){
				 obj.put("targetOutTime" , timeFormat.format(attendance.getTargetOutTime()));
			 }else{
				 obj.put("targetOutTime" , "");
			 }
			 obj.put("OTStatus" , attendance.getOtStatus());
			 obj.put("requestedHours" , attendance.getRequestedOTHours());
			 obj.put("supervisorName" , "");
			 array.put(obj);
			 } catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			}
		}
		logger.log(Level.SEVERE, "map after attendance :" + hrProjectMap.size());
		
		for(Map.Entry<String, Overtime> entry :hrProjectMap.entrySet() ){
//			logger.log(Level.SEVERE , "Key set : " + entry.getKey() + " value :" +entry.getValue().getEmpId());
			JSONObject obj = new JSONObject();
			 try {
			 obj.put("count" ,0);	
			 obj.put("createdBy" ,"AutoSubmit");
			 obj.put("markInAddress" , "");
			 obj.put("markOutAddress" , "");
			 obj.put("status" , Attendance.ATTENDANCECREATED);
			
			 obj.put("updatedEmployeeRole" , "AutoSubmit");
			 obj.put("updatedEmployeeName" , "AutoSubmit");
			 obj.put("attendanceDate" , sdf.format(attendanceDate));
			 obj.put("overtimeType" , "");
			 obj.put("inTime" , "");
			 obj.put("outTime" , "");
			 obj.put("empId" , entry.getValue().getEmpId());
			 obj.put("actionLabel" , "");
			 obj.put("shift" , entry.getValue().getShift());
			 obj.put("projectName" , entry.getKey().split("/-")[0]);
			 obj.put("month" , monthSdf.format(attendanceDate));
			 obj.put("targetInTime" , "");
			 obj.put("targetOutTime" , "");
			 obj.put("OTStatus" , "");
			 obj.put("requestedHours" , 0);
			 obj.put("supervisorName" , "");
			 obj.put("approverName" , "AutoSubmit");
			 array.put(obj);
			 } catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		if(array != null && array.length() > 0){
			String response = impl.employeeAttendance(companyId, array, Attendance.ATTENDANCEAPPROVED);
		logger.log(Level.SEVERE , "Result : "+hrProject.getProjectName()+" "+response);
		}
		}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
		

}
