package com.slicktechnologies.server.cronjobimpl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

public class SendSrCopyMailCronJob extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3058986330701402503L;
	Logger logger =Logger.getLogger("logger");
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		
		logger.log(Level.SEVERE,"Inside DOGET in update duplicate service cronjob");
		
		Date today=DateUtility.getDateWithTimeZone("IST", new Date());
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		Calendar cal=Calendar.getInstance();
		cal.setTime(today);
		cal.add(Calendar.DATE, -1);
		
		Date currentDate=null;
		
		try {
			currentDate=dateFormat.parse(dateFormat.format(cal.getTime()));
			cal.set(Calendar.HOUR_OF_DAY,00);
			cal.set(Calendar.MINUTE,00);
			cal.set(Calendar.SECOND,00);
			cal.set(Calendar.MILLISECOND,000);
			currentDate=cal.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		logger.log(Level.SEVERE,"Current Date"+currentDate);
		logger.log(Level.SEVERE,"temp Date"+today);
		
		Calendar cal2=Calendar.getInstance();
		cal2.setTime(today);
		cal2.add(Calendar.DATE, -1);
		
		Date todaysDate=null;
		
		try {
			todaysDate=dateFormat.parse(dateFormat.format(cal2.getTime()));
			cal2.set(Calendar.HOUR_OF_DAY,23);
			cal2.set(Calendar.MINUTE,59);
			cal2.set(Calendar.SECOND,59);
			cal2.set(Calendar.MILLISECOND,999);
			todaysDate=cal2.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		logger.log(Level.SEVERE,"Today Date"+todaysDate);
		
		List<Company> compEntity = ofy().load().type(Company.class).list();
		if (compEntity.size() > 0) {
			for (Company comp : compEntity) {
				
				/**
				 * @author Anil
				 * @since 15-10-2020
				 */
				if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CronJob", "PC_EnableSRCopyCronJob", comp.getCompanyId())){
					continue;
				}
					
				List<ProcessConfiguration> processList = ofy().load().type(ProcessConfiguration.class)
						.filter("companyId", comp.getCompanyId()).filter("processName", "SRCopyDate").filter("configStatus", true).list();
				
				if(processList!=null){
					for (int k = 0; k < processList.size(); k++) {
						if (processList.get(k).getProcessName().trim().equalsIgnoreCase("SRCopyDate")) {
							for (int i = 0; i < processList.get(k).getProcessList().size(); i++) {
								if (processList.get(k).getProcessList().get(i).isStatus() == true) {
									
									try{
										Date date=dateFormat.parse(processList.get(k).getProcessList().get(i).getProcessType().trim());
									
										Calendar c1=Calendar.getInstance();
										c1.setTime(date);
										c1.set(Calendar.HOUR_OF_DAY,00);
										c1.set(Calendar.MINUTE,00);
										c1.set(Calendar.SECOND,00);
										c1.set(Calendar.MILLISECOND,000);
										currentDate=c1.getTime();
										
										Calendar c2=Calendar.getInstance();
										c2.setTime(date);
//										c2.add(Calendar.DATE, -1);
										c2.set(Calendar.HOUR_OF_DAY,23);
										c2.set(Calendar.MINUTE,59);
										c2.set(Calendar.SECOND,59);
										c2.set(Calendar.MILLISECOND,999);
										todaysDate=c2.getTime();
										
										
										logger.log(Level.SEVERE,"currentDate "+currentDate+" todaysDate "+todaysDate);
										
									}catch(Exception e){
										return;
									}
								}
							}
						}
					}
				}
					
					
				
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "SendSRCopyOnServiceCompletion", comp.getCompanyId())){
					
					List<Service> serviceList = ofy().load().type(Service.class)
							.filter("companyId",comp.getCompanyId())
							.filter("status", Service.SERVICESTATUSCOMPLETED)
							.filter("completedByApp", true)
							.filter("serviceDate >=", currentDate)
							.filter("serviceDate <=", todaysDate).list();
					
					if(serviceList!=null){
						logger.log(Level.SEVERE,"serviceList "+serviceList.size());
						
						Comparator<Service> srCopyNumberComp=new Comparator<Service>() {
							@Override
							public int compare(Service arg0, Service arg1) {
								// TODO Auto-generated method stub
								return arg0.getSrCopyNumber().compareTo(arg1.getSrCopyNumber());
							}
						};
						
						Collections.sort(serviceList, srCopyNumberComp);
						
						String prevSrCopyNum=null;
						for(Service serObject:serviceList){
							
							if(serObject.isSrCopyStatus()==true){
								continue;
							}
							serObject.setSrCopyStatus(true);
							String currSrCopyNum=serObject.getSrCopyNumber();
							if(prevSrCopyNum!=null){
								if(prevSrCopyNum.equals(currSrCopyNum)){
									continue;
								}
							}
							String[] strArr=serObject.getSrCopyNumber().split("-");
							logger.log(Level.SEVERE,"strArr  "+strArr.toString());
							String srNumber=strArr[0];
							logger.log(Level.SEVERE,"srNumber  "+srNumber);
							String taskName="SendSRCopyEmail"+"$"+serObject.getCompanyId()+"$"+serObject.getCount()+"$"+srNumber+"$"+serObject.getBranchEmail();
							Queue queue = QueueFactory.getQueue("SRCopyEmailQueueRevised-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/SRCopyEmailTaskQueue").param("taskKeyAndValue", taskName));
							
							prevSrCopyNum=currSrCopyNum;
							
						}
						
						if(serviceList!=null&&serviceList.size()!=0){
							ofy().save().entities(serviceList).now();
						}
					}
					
				}
				
			}

		}
		
	}
	
	
	
}

