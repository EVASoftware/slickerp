package com.slicktechnologies.server.cronjobimpl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.client.config.categoryconfig.CategoryTypes;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.democonfiguration.DemoConfigurationImpl;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;

public class UpdateDocumentCronJobImpl extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8105302123198187722L;


	Logger logger = Logger.getLogger("NameOfYourLogger");
	DemoConfigurationImpl demo=new DemoConfigurationImpl();
	List<Company> compEntity;
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)throws IOException {
		compEntity = ofy().load().type(Company.class).list();
		logger.log(Level.SEVERE,"Size of compEntity "+compEntity.size());
		updateDocumentName(null);
		updateProcessNameAndGenerator();
	}
	

	private void updateProcessNameAndGenerator() {
		
		
		
		for(int i=0;i<compEntity.size();i++){
			
			ArrayList<String> processNameList=demo.classesList();
			logger.log(Level.SEVERE,"Size of Process name list LOCAL  "+processNameList.size());
			
			List<ProcessName> processList=ofy().load().type(ProcessName.class).filter("companyId", compEntity.get(i).getCompanyId()).list();
			logger.log(Level.SEVERE,"Size of Process name list DB  "+processList.size());
			
			if(processList.size()!=0){
				for(int j=0;j<processList.size();j++){
					if(processNameList.contains(processList.get(j).getProcessName())){
						processNameList.remove(processList.get(j).getProcessName());
					}
				}
			}
			
			logger.log(Level.SEVERE,"Size of Process name list LOCAL AFTER  "+processNameList.size());
			if(processNameList.size()!=0){
				
				for(int j=0;j<processNameList.size();j++){
				ProcessName processName=getProcessName(compEntity.get(i).getCompanyId(), processNameList.get(j));
				
				GenricServiceImpl impl =new GenricServiceImpl();
				impl.save(processName);
				}
			}
			
			if(processNameList.size()!=0){
				for(int j=0;j<processNameList.size();j++){
				NumberGeneration numberGen=getNumberGenerator(compEntity.get(i).getCompanyId(), processNameList.get(j));
				
				GenricServiceImpl impl =new GenricServiceImpl();
				impl.save(numberGen);
				}
			}
			
			
			
			
		}
		
		
		
		
		
	}

	public void updateDocumentName(String accessURL) {
//		ArrayList<String>moduleNameList=demo.moduleNames();
//		logger.log(Level.SEVERE,"Size of Module List LOCAL "+moduleNameList.size());
		
		
		if(accessURL!=null && compEntity==null) {
			compEntity = ofy().load().type(Company.class).filter("accessUrl", accessURL).list();
			logger.log(Level.SEVERE,"Size of compEntity "+compEntity.size());
		}
		
		for(int i=0;i<compEntity.size();i++){
			
			
			
			ArrayList<String>moduleNameList=demo.moduleNames();
			logger.log(Level.SEVERE,"Size of Module List LOCAL "+moduleNameList.size());
			System.out.println("Size of Module List LOCAL "+moduleNameList.size());
			System.out.println("MODULE COUNT :: "+CategoryTypes.getCategoryInternalType(Screen.MODULENAME));
			
			List<ConfigCategory> configCategoryList=ofy().load().type(ConfigCategory.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("internalType", CategoryTypes.getCategoryInternalType(Screen.MODULENAME)).list();
			logger.log(Level.SEVERE,"Size of Module List DB "+configCategoryList.size());
			System.out.println("Size of Module List DB "+configCategoryList.size());
			
			if(configCategoryList.size()!=0){
				for(int j=0;j<configCategoryList.size();j++){
					System.out.println("DB Module Name ::: "+configCategoryList.get(j).getCategoryName());
					if(moduleNameList.contains(configCategoryList.get(j).getCategoryName().trim())){
						System.out.println("MACHted.........");
						moduleNameList.remove(configCategoryList.get(j).getCategoryName().trim());
					}
				}
			}
			
			System.out.println("Module name list size after ::: "+moduleNameList.size());
			
			if(moduleNameList.size()!=0){
				for(int j=0;j<moduleNameList.size();j++){
					System.out.println("Updating Module ::: "+moduleNameList.get(j));
					ConfigCategory configCategory = demo.getConfigCategoryData(CategoryTypes.getCategoryInternalType(Screen.MODULENAME),moduleNameList.get(j).trim(),"CC"+100000000+j,compEntity.get(i).getCompanyId());
					demo.saveConfig(configCategory);
					
					
					
					/*************** Date 24-08-2017 added by vijay by correcting issue *******************/
					

					ArrayList<String> documentNameList=getDocumentNameList(moduleNameList.get(j));
					System.out.println();
					logger.log(Level.SEVERE,"Size of Document  List LOCAL "+moduleNameList.get(j) +" "+documentNameList.size());
					
					List<Type> types=ofy().load().type(Type.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("internalType", CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME)).filter("categoryName",moduleNameList.get(j)).list();
					
					if(types.size()!=0){
						for(int k=0;k<types.size();k++){
							if(documentNameList.contains(types.get(k).getTypeName().trim())){
								documentNameList.remove(types.get(k).getTypeName().trim());
							}
						}
					}
					
					System.out.println("Documnet List Size ::: "+documentNameList.size());
					if(documentNameList.size()!=0){
						for(int k=0;k<documentNameList.size();k++){
							Type type = DemoConfigurationImpl.getTypesData(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME), moduleNameList.get(j), documentNameList.get(k).trim(), "TT"+100000000+k,"CC"+configCategory.getCount(), compEntity.get(i).getCompanyId());
							demo.saveConfig(type);
						}
					}
					
					/*************  ends here *******************/
					
				}
			}
			
			
			/**
			 * Date 24-08-2017 This is old code commented by vijay this is done by wrong way not proper linkage between category and type
			 * means module and document name. And with this code key is not getting generated
			 * new code added above
			 */
//			ArrayList<String>moduleDocList=demo.moduleNames();
//			
//			for(int j=0;j<moduleDocList.size();j++){
//				ArrayList<String> documentNameList=getDocumentNameList(moduleDocList.get(j));
//				System.out.println();
//				logger.log(Level.SEVERE,"Size of Document  List LOCAL "+moduleDocList.get(j) +" "+documentNameList.size());
//				
//				List<Type> types=ofy().load().type(Type.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("internalType", CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME)).filter("categoryName",moduleDocList.get(j)).list();
//				
//				if(types.size()!=0){
//					for(int k=0;k<types.size();k++){
//						if(documentNameList.contains(types.get(k).getTypeName().trim())){
//							documentNameList.remove(types.get(k).getTypeName().trim());
//						}
//					}
//				}
//				
//				System.out.println("Documnet List Size ::: "+documentNameList.size());
//				if(documentNameList.size()!=0){
//					for(int k=0;k<documentNameList.size();k++){
//						Type type = demo.getTypesData(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME), moduleDocList.get(j), documentNameList.get(k).trim(), "TT"+100000000+k,"CC"+100000000+j, compEntity.get(i).getCompanyId());
//						demo.saveConfig(type);
//					}
//				}
//				
//			}

			/**
			 * ends here
			 */
			
			
			
			
//			for(int j=0;j<moduleNameList.size();j++){
//				ConfigCategory configCategory=ofy().load().type(ConfigCategory.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("internalType", Screen.MODULENAME).filter("categoryName",moduleNameList.get(j)).first().now();
//				
//				if(configCategory!=null){
//					
//				}else{
//					configCategory = demo.getConfigCategoryData(CategoryTypes.getCategoryInternalType(Screen.MODULENAME),moduleNameList.get(j).trim(),"CC"+100000000+j,compEntity.get(i).getCompanyId());
//					demo.saveConfig(configCategory);
//				}
//				
//				ArrayList<String> documentNameList=getDocumentNameList(moduleNameList.get(j));
//				
//				logger.log(Level.SEVERE,"Size of Document  List LOCAL "+moduleNameList.get(j) +" "+documentNameList.size());
//				
//				for(int k=0;k<documentNameList.size();k++){
//					Type types=ofy().load().type(Type.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("internalType", Screen.DOCUMENTNAME).filter("categoryName",moduleNameList.get(j)).filter("typeName",documentNameList.get(k)).first().now();
//					if(types!=null){
//						
//					}else{
//						types = demo.getTypesData(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME), moduleNameList.get(j), documentNameList.get(k).trim(), "TT"+100000000+k,"CC"+100000000+j, compEntity.get(i).getCompanyId());
//						demo.saveConfig(types);
//					}
//				}
//			}
			
			
			
		} 
		
	}


	private ArrayList<String> getDocumentNameList(String moduleName) {
		ArrayList<String> list=new ArrayList<String>();
		
		if(moduleName.trim().equals("Sales"))
		{
			list=demo.salesDocumentNames();
		}
		
		if(moduleName.trim().equals(AppConstants.SALESCONFIG))
		{
			list=demo.salesConfigDocumentNames();
		}
		
		if(moduleName.trim().equals("Service"))
		{
			list=demo.serviceDocumentNames();
		}
		
		if(moduleName.trim().equals(AppConstants.SERVICECONFIG))
		{
			list=demo.serviceConfigDocumentNames();
		}
		
		if(moduleName.trim().equals("Accounts"))
		{
			list=demo.accountsDocumentNames();
		}
		
		if(moduleName.trim().equals(AppConstants.ACCOUNTCONFIG))
		{
			list=demo.accountsConfigDocumentNames();
		}
		
		if(moduleName.trim().equals("Product"))
		{
			list=demo.productDocumentNames();
		}
		
		if(moduleName.trim().equals(AppConstants.PRODUCTCONFIG))
		{
			list=demo.productConfigDocumentNames();
		}
		
		if(moduleName.trim().equals("Purchase"))
		{
			list=demo.purchaseDocumentNames();
		}
		
		if(moduleName.trim().equals(AppConstants.PURCHASECONFIG))
		{
			list=demo.purchaseConfigDocumentNames();
		}
		
		if(moduleName.trim().equals("Inventory"))
		{
			list=demo.inventoryDocumentNames();
		}
		if(moduleName.trim().equals(AppConstants.INVENTORYCONFIG))
		{
			list=demo.inventoryConfigDocumentNames();
		}
		
		if(moduleName.trim().equals("HR Admin"))
		{
			list=demo.hrAdminDocumentNames();
		}
		if(moduleName.trim().equals(AppConstants.HRCONFIG))
		{
			list=demo.hrAdminConfigDocumentNames();
		}
		/**Date 24-8-2019 added by Amol***/
		if(moduleName.trim().equals(AppConstants.HRSTAUTORYDEDUCTION)){
			list=demo.hrAdminStatutoryDeductionNames();
		}
		
		
		
		
		if(moduleName.trim().equals("Employee Self Service"))
		{
			list=demo.employeeSelfServiceDocumentNames();
		}
		if(moduleName.trim().equals("Asset Management"))
		{
			list=demo.assetManagementDocumentNames();
		}
		if(moduleName.trim().equals(AppConstants.ASSETMANAGEMENTCONFIG))
		{
			list=demo.assetManagementConfigDocumentNames();
		}
		
		if(moduleName.trim().equals("Settings"))
		{
			list=demo.settingDocumentNames();
		}
		if(moduleName.trim().equals("Approval"))
		{
			list=demo.approvalDocumentNames();
		}
		
		return list;
	}


	private List<ConfigCategory> checkAndUpdateModuleList(List<ConfigCategory> configCategory, Long companyId) {
		ArrayList<String>moduleNameList=demo.moduleNames();
		logger.log(Level.SEVERE,"Size of Module List LOCAL "+moduleNameList.size());
		
		List<ConfigCategory> confCateList=new ArrayList<ConfigCategory>();
		
		
		
		return confCateList;
	}

	public ProcessName getProcessName(long companyId,String processName){
		
		ProcessName processEntity=new ProcessName();
		processEntity.setCompanyId(companyId);
		processEntity.setProcessName(processName);
		processEntity.setStatus(true);
		
		return processEntity;
	}
	
	
	public NumberGeneration getNumberGenerator(long companyId,String processName){
		
		
			NumberGeneration ngEntity=new NumberGeneration();
			ngEntity.setCompanyId(companyId);
			ngEntity.setProcessName(processName);

			if(processName.trim().equals("ItemProduct")){
				ngEntity.setNumber(100000000);
			}
			else if(processName.trim().equals("ServiceProduct")){
				ngEntity.setNumber(500000000);
			}
			else if(processName.trim().equals("Quotation")){
				ngEntity.setNumber(500000000);
			}
			else if(processName.trim().equals("Contract")){
				ngEntity.setNumber(500000000);
			}
			else{
				ngEntity.setNumber(100000000);
			}
			
			ngEntity.setStatus(true);
		
		return ngEntity;
	}
}
