package com.slicktechnologies.server.cronjobimpl;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.google.gwt.user.client.Window;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.LoggedIn;

public class LoggedInEntityClearCronImpl extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1165147456367228595L;

	LoggedIn loggedInEntity;
	Logger logger = Logger.getLogger("NameOfYourLogger");

	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
	
		DeleteLoggedInEntity(req,resp);
	}

	private void DeleteLoggedInEntity(HttpServletRequest req, HttpServletResponse resp){
		
		
		List<Company> compEntity = ofy().load().type(Company.class).list();
		
		

		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		Date todayDate=DateUtility.getDateWithTimeZone("IST", new Date());
		
		
		for (int i = 0; i < compEntity.size(); i++) {
			List<LoggedIn> loginList = new ArrayList<LoggedIn>();
			
			logger.log(Level.SEVERE,"Company id"+compEntity.get(i).getCompanyId());
			
			
//			Date todayDate = new Date();
//			Calendar c = Calendar.getInstance();
//			c.setTime(todayDate);
//			c.add(c.DATE, -5);
//	
			/** Date 18-01-2018 By vijay last 15 days logged in history maintainig other than those clear
			 *   Date which is  -15 days with time 00 00 000 
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding  Day"+todayDate);
			Calendar cal4=Calendar.getInstance();
			cal4.setTime(todayDate);
			cal4.add(Calendar.DATE, -15);
			
			Date minusthirtydayswithzero=null;
			
			try {
				minusthirtydayswithzero=dateFormat.parse(dateFormat.format(cal4.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Today Date -15 Days Date"+minusthirtydayswithzero);
			
			logger.log(Level.SEVERE,"new Date after 5 addition "+todayDate);
			
			loginList = ofy().load().type(LoggedIn.class).filter("companyId", compEntity.get(i).getCompanyId())
					.filter("loginDate <=", todayDate).list();
			
			logger.log(Level.SEVERE,"login List size ="+loginList.size());
			
			ofy().delete().entities(loginList);
		}
			
			
			
	}
}
