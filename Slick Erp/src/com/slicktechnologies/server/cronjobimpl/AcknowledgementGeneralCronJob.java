package com.slicktechnologies.server.cronjobimpl;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

public class AcknowledgementGeneralCronJob extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -760885271154972003L;

	Logger logger = Logger.getLogger("AcknowledgementGeneralCronJob.class");
	long companyId = 0;
	SimpleDateFormat spf;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		spf = new SimpleDateFormat("dd MMM yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		spf.setTimeZone(TimeZone.getTimeZone("IST"));
		String urlcalled = req.getRequestURL().toString().trim();
		logger.log(Level.SEVERE, "urlcalled:::::" + urlcalled);

		String url1 = urlcalled.replace("http://", "");
		logger.log(Level.SEVERE, "url1:::::" + url1);
		String[] splitUrl = url1.split("\\.");
		logger.log(Level.SEVERE, "splitUrl[0]:::::" + splitUrl[0]);
		RegisterServiceImpl registerImpl = new RegisterServiceImpl();
		registerImpl.getClass();
		Company company = ofy().load().type(Company.class)
				.filter("accessUrl", "my").first().now();
		companyId = company.getCompanyId();
		final String ACK = AppConstants.NBHCACKNNOWLEDGEMENTGENINTEGRATE;
		String data = "";
		/**
		 * Rahul Verma added this code for process config
		 */
		boolean flag = false;
		ProcessConfiguration process = ofy().load()
				.type(ProcessConfiguration.class)
				.filter("companyId", companyId)
				.filter("processName", "CronJob").first().now();

		for (int i = 0; i < process.getProcessList().size(); i++) {

			if (process.getProcessList().get(i).processType
					.equalsIgnoreCase("ACKFORALLINTERFACE")) {
				flag = true;
			}
		}
		/**
		 * ends here
		 */
		if (flag) {
			URL url = null;
			try {
				url = new URL(ACK);
			} catch (MalformedURLException e) {
				logger.log(Level.SEVERE,
						"MalformedURLException ERROR::::::::::" + e);
				e.printStackTrace();
			}
			HttpURLConnection con = null;
			try {
				con = (HttpURLConnection) url.openConnection();
			} catch (IOException e) {
				logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
				e.printStackTrace();
			}
			con.setDoInput(true);
			con.setRequestProperty("Content-Type",
					"application/json; charset=UTF-8");
			try {
				con.setRequestMethod("GET");
			} catch (ProtocolException e) {
				logger.log(Level.SEVERE, "ProtocolException ERROR::::::::::"
						+ e);
				e.printStackTrace();
			}
			InputStream is = null;
			try {
				is = con.getInputStream();
			} catch (IOException e) {
				logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
				e.printStackTrace();
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String temp;

			try {
				while ((temp = br.readLine()) != null) {
					data = data + temp;
				}
				logger.log(Level.SEVERE, "data data::::::::::" + data);
			} catch (IOException e) {
				logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
				e.printStackTrace();
			}

			logger.log(Level.SEVERE, "Data::::::::::" + data);
			updateData(data);
		}
	}

	private void updateData(String data) {
		JSONArray jArray = null;
		try {
			jArray = new JSONArray(data);
		} catch (JSONException e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "parsing Error!!");
		}
		
		/**
		 * Date 06-04-2019 by Vijay to load globally data and update
		 */
		
		HashSet<Integer>hsGRN = new HashSet<Integer>();
		HashSet<Integer>hsMMN = new HashSet<Integer>();
		
		for (int i = 0; i < jArray.length(); i++) {
			JSONObject jObject = null;
			try {
				jObject = jArray.getJSONObject(i);
			} catch (JSONException e) {
				e.printStackTrace();
				logger.log(Level.SEVERE, "Error parsing jObject");
			}
					int documentID = Integer
					.parseInt(jObject.optString("DocumentId").trim());
					if(jObject.optString("ActionTask").trim().equals("GRNAck")){
						hsGRN.add(documentID);
					}
					if(jObject.optString("ActionTask").trim().equals("MMNAck")){
						hsMMN.add(documentID);
					}
					
		}			
		ArrayList<Integer> grnDocIdList = new ArrayList<Integer>();
		ArrayList<Integer> mmnDocIdList = new ArrayList<Integer>();
		for(Integer docId : hsGRN){
			grnDocIdList.add(docId);
		}
		for(Integer DocId : hsMMN){
			mmnDocIdList.add(DocId);
		}
		List<GRN> grnList = null;
		if(grnDocIdList.size()!=0){
		 grnList = ofy().load().type(GRN.class).filter("companyId", companyId).filter("count IN", grnDocIdList).list();
			logger.log(Level.SEVERE, "GRN List Size "+grnList.size());
		}
		List<MaterialMovementNote> mmnList = null;
		if(mmnDocIdList.size()!=0){
			mmnList = ofy().load().type(MaterialMovementNote.class).filter("companyId", companyId).filter("count IN", mmnDocIdList).list();
			logger.log(Level.SEVERE, "MMN List Size "+mmnList.size());

		}
		
		for (int i = 0; i < jArray.length(); i++) {
			JSONObject jObject = null;
			try {
				jObject = jArray.getJSONObject(i);
			} catch (JSONException e) {
				e.printStackTrace();
				logger.log(Level.SEVERE, "Error parsing jObject");
			}
//			int documentID = Integer
//					.parseInt(jObject.optString("DocId").trim());DocumentId
					int documentID = Integer
					.parseInt(jObject.optString("DocumentId").trim());

					
			List<AccountingInterface> accIntList = ofy()
					.load()
					.type(AccountingInterface.class)
					.filter("companyId", companyId)
					.filter("documentType", jObject.optString("DocType").trim())
					.filter("direction", AccountingInterface.OUTBOUNDDIRECTION)
					.filter("status", AccountingInterface.PENDING)
					.filter("documentID", documentID).list();
//			logger.log(Level.SEVERE, "accIntList"+accIntList.size());

			for (AccountingInterface accountingInterface : accIntList) {
				if (jObject.optString("Status").trim()
						.equalsIgnoreCase(AccountingInterface.FAILED)) {
					accountingInterface.setStatus(AccountingInterface.FAILED);
					accountingInterface.setRemark(jObject.optString("Remarks")
							.trim());
					try {
						accountingInterface.setDateofSynch(spf.parse(spf
								.format(new Date())));
					} catch (ParseException e) {
						e.printStackTrace();
					}
				} else if (jObject.optString("Status").trim()
						.equalsIgnoreCase(AccountingInterface.TALLYSYNCED)) {
					accountingInterface
							.setStatus(AccountingInterface.TALLYSYNCED);
					accountingInterface.setRemark(jObject.optString("Remarks")
							.trim());
					try {
						accountingInterface.setDateofSynch(spf.parse(spf
								.format(new Date())));
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				


			}
			ofy().save().entities(accIntList);
//			logger.log(Level.SEVERE, "Updated successfully");
			
			
			
			/*** Date 05-04-2019 by Vijay for NBHC CCPM acknowledge SAP id updated ****/ 
			/**
			 * if SAPDeleteFlag is 0 then it will update GRN SAP ID for GRN entry confirmation.
			 */
			if(jObject.optString("DocType").trim().equals("GRN") && jObject.optString("SAPDeleteFlag").trim().equals("0")  && jObject.optString("Status").trim()
						.equalsIgnoreCase(AccountingInterface.TALLYSYNCED)  ){
				if(grnList!=null && grnList.size()!=0){
				for(GRN grnEntity : grnList){
					if(grnEntity.getCount()==documentID){
						String grnproductSAPID = jObject.optString("POItemNumber").trim(); 
						for(GRNDetails grndetails : grnEntity.getInventoryProductItem()){
							if(grndetails.getProductRefId()!=null && !grndetails.getProductRefId().equals("")){
								if(grndetails.getProductRefId().trim().equals(grnproductSAPID)){
									grndetails.setReferenceNo(jObject.optString("SAPTranId").trim());
									logger.log(Level.SEVERE, "GRN Acknowledge updated sucessfully");
								}
							}

						}
					}
				}
				}
			}
			/**
			 * if SAPDeleteFlag is 1 then it will update GRN SAP ID for GRN cancellation confirmation.
			 */
			if(jObject.optString("DocType").trim().equals("GRN") && jObject.optString("SAPDeleteFlag").trim().equals("1") &&
					jObject.optString("Status").trim().equalsIgnoreCase(AccountingInterface.TALLYSYNCED)){
				if(grnList!=null && grnList.size()!=0){
				for(GRN grnEntity : grnList){
					if(grnEntity.getCount()==documentID){
						String grnproductSAPID = jObject.optString("POItemNumber").trim(); 
						for(GRNDetails grndetails : grnEntity.getInventoryProductItem()){
							if(grndetails.getProductRefId()!=null && !grndetails.getProductRefId().equals("")){
								if(grndetails.getProductRefId().trim().equals(grnproductSAPID)){
									grndetails.setReferenceNo2(jObject.optString("SAPTranId").trim());
									logger.log(Level.SEVERE, "GRN cancelled Acknowledge updated sucessfully");
								}
							}
						}
					}
				}
				}
				
			}
			if(jObject.optString("DocType").trim().equals("MMN") ){
				if(mmnList!=null && mmnList.size()!=0){
					for(MaterialMovementNote mmn : mmnList){
						if(mmn.getCount()==documentID){
							mmn.setReferenceNo(jObject.optString("SAPTranId").trim());
						}
					}
				}
			}
		}
		
		if(mmnList!=null &&mmnList.size()!=0){
		ofy().save().entities(mmnList);
		logger.log(Level.SEVERE, "MMN Updated Sucessfully");
		}
		if(grnList!=null && grnList.size()!=0){
			ofy().save().entities(grnList);
			logger.log(Level.SEVERE, "GRN Updated Sucessfully");
		}
	}
}
