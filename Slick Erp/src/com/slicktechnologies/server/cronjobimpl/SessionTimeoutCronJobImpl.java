package com.slicktechnologies.server.cronjobimpl;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.server.sessionmanagement.SessionServiceImpl;

public class SessionTimeoutCronJobImpl extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5548701100872992820L;
	
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		sessionManagement();
	}
	
	public void sessionManagement()
	{
		SessionServiceImpl.cronSessionHandling();
	}

}
