package com.slicktechnologies.server.cronjobimpl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.googlecode.objectify.ObjectifyService;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.personlayer.EmployeeCTCTemplate;

public class EmployeeEarningComponentRenewalCronJob extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4938663135460870315L;
	Logger logger = Logger.getLogger("NameOfYourLogger");

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private DateFormat fmt1 = new SimpleDateFormat("dd/MM/yyyy");
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
//		loadDuecomponent(null,null);
	}

	public void loadDuecomponent(List<CronJobConfigrationDetails> cronList,HashSet<String>  empRoleList) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE, "Cron list str --" + cronList);
		try {
			
			List<Company> compEntity = ofy().load().type(Company.class).list();
			
			if(compEntity.size()>0)
			{
			
				logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());

	for(int i=0;i<compEntity.size();i++){
					
	Company c=compEntity.get(i);
	
	if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "UpdateAllEmployee", c.getCompanyId())){
		List<Employee> employeelist = ofy().load().type(Employee.class).filter("companyId", c.getCompanyId()).filter("status", true).list();
		logger.log(Level.SEVERE,"employeelist "+employeelist.size());
		if(employeelist.size()!=0){
			ofy().save().entities(employeelist);
			logger.log(Level.SEVERE,"All employee updated");
		}
	}
	
	List<User> userlist = ofy().load().type(User.class).filter("companyId", c.getCompanyId()).filter("status", true).list();
	logger.log(Level.SEVERE,"userlist "+userlist.size());

	
	for(String empRoleName :empRoleList){
		List<Employee>  employeeList=new ArrayList<Employee>();
		employeeList=ofy().load().type(Employee.class).filter("companyId", c.getCompanyId()).filter("roleName", empRoleName).filter("status", true).list();
		logger.log(Level.SEVERE,""+empRoleName+" "+"Employee List size::: "+employeeList.size());
			for (Employee employee : employeeList) {
				 
				String todayDateString ="";
			 
				String footer = "";
				 
				String emailSubject ="Earning component renewal due / overdue as on date Date"+" "+ todayDateString;
				String mailTitl = "Earning component renewal due / overdue as on date Date";
				
				String emailMailBody = mailTitl;
				
				ArrayList<String> toEmailListDt=new ArrayList<String>();
				
				String userEmailId = getEmployeeUserEmailId(employee,userlist);
				logger.log(Level.SEVERE,"userEmailId "+userEmailId);
				if(!userEmailId.equals("")){
					toEmailListDt.add(userEmailId);
				}

//				toEmailListDt.add(employee.getEmail().trim());
				List<String> branchList=new ArrayList<String>();
				
				branchList.add(employee.getBranchName());
				for (int j = 0; j < employee.getEmpBranchList().size(); j++) {
					branchList.add(employee.getEmpBranchList().get(j).getBranchName());
					logger.log(Level.SEVERE, "BrnachName"+ employee.getEmpBranchList().get(j).getBranchName());
				}
				logger.log(Level.SEVERE,"toEmailList size::: "+toEmailListDt.size()+" Branch List SIze : "+branchList.size());
				

			if(branchList.size()!=0){
			for(int k=0;k<cronList.size();k++){
				
				if(cronList.get(k).getEmployeeRole().equalsIgnoreCase(empRoleName)){
					CronJobConfigrationDetails cronDetails =cronList.get(k);
					Email cronEmail = new Email();

					TimeZone.setDefault(TimeZone.getTimeZone("IST"));
					fmt.setTimeZone(TimeZone.getTimeZone("IST"));
					fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
						
				
					Date today=DateUtility.getDateWithTimeZone("IST", new Date());
					Calendar cal=Calendar.getInstance();
					cal.setTime(today);
					cal.add(Calendar.DATE, 0);
					
					Date dateForFilter=null;
					
					try {
						dateForFilter=fmt1.parse(fmt1.format(cal.getTime()));
						cal.set(Calendar.HOUR_OF_DAY,0);
						cal.set(Calendar.MINUTE,0);
						cal.set(Calendar.SECOND,0);
						cal.set(Calendar.MILLISECOND,0);
						dateForFilter=cal.getTime();
					} catch (ParseException e) { 
						e.printStackTrace();
					}
					
					cal.setTime(today);
					int diffDay = 0;
					 if(cronDetails.getOverdueDays()>0 && cronDetails.isOverdueStatus()){
						 diffDay = - cronDetails.getOverdueDays();
					 }else{
						 diffDay = cronDetails.getInterval();
					 }
					logger.log(Level.SEVERE,"diffDay "+diffDay);

					cal.add(Calendar.DATE, diffDay);
					
					Date duedateForFilter=null;
					
					try {
						duedateForFilter=fmt1.parse(fmt1.format(cal.getTime()));
						cal.set(Calendar.HOUR_OF_DAY,23);
						cal.set(Calendar.MINUTE,59);
						cal.set(Calendar.SECOND,59);
						cal.set(Calendar.MILLISECOND,999);
						duedateForFilter=cal.getTime();
					} catch (ParseException e) {
						e.printStackTrace();
					}
					
					
					logger.log(Level.SEVERE,"Date After setting time"+duedateForFilter);
					
					/*************************************End*********************************/
							
					 try{
						 
				 		  logger.log(Level.SEVERE,"In service List");	
						  Date todaysDate = new Date();
						  SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
						  todayDateString = df.format(todaysDate);
						  System.out.println("String in dd/MM/yyyy format is: " + todayDateString);
		
					 
					 	ObjectifyService.reset();
					 	ofy().consistency(Consistency.STRONG);
					 	ofy().clear();
						   
						List<Employee> employeeEntity = new ArrayList<Employee>();
						if(cronDetails.isOverdueStatus()){
							emailSubject ="Earning component renewal overdue as on date Date"+" "+ todayDateString;
							mailTitl = "Earning component renewal overdue as on date Date";
							
							logger.log(Level.SEVERE,"Inside Overdue filter for querry");
							 employeeEntity = ofy().load().type(Employee.class)
										.filter("companyId",compEntity.get(i).getCompanyId())
										.filter("status",true)
										.filter("branchName IN",branchList)
										.filter("employeeCTCList.endDate <=",duedateForFilter).list();
						}else{
							mailTitl = "Earning component renewal due as on date Date";
							emailSubject ="Earning component renewal due as on date Date"+" "+ todayDateString;
							
							logger.log(Level.SEVERE,"Inside without overdue filter for querry");
							employeeEntity = ofy().load().type(Employee.class)
										.filter("companyId",compEntity.get(i).getCompanyId())
										.filter("status",true)
										.filter("branchName IN",branchList)
										.filter("employeeCTCList.endDate >=",dateForFilter)
										.filter("employeeCTCList.endDate <=",duedateForFilter)
										.list();
						}
							
						logger.log(Level.SEVERE,"companyId"+compEntity.get(i).getCompanyId());
						logger.log(Level.SEVERE,"branch"+branchList);
						logger.log(Level.SEVERE,"dateForFilter"+dateForFilter);	
						logger.log(Level.SEVERE,"duedateForFilter"+duedateForFilter);
						logger.log(Level.SEVERE,"employeeEntity size:"+employeeEntity.size());	

							  
						ArrayList<Employee> updatedEmployeelist = new ArrayList<Employee>();
									  
					  for(Employee emp : employeeEntity){
						
							  
							  for(EmployeeCTCTemplate employeectcTemplate : emp.getEmployeeCTCList()){
								  
								  if(cronDetails.isOverdueStatus()) {
									  if(employeectcTemplate.getEndDate().before(duedateForFilter)
											  ||employeectcTemplate.getEndDate().equals(duedateForFilter)) {
										  
										  Employee employeInfo = new Employee();
										  employeInfo.setCount(emp.getCount());
										  employeInfo.setFullname(emp.getFullname());
										  employeInfo.setBranchName(emp.getBranchName());
										  employeInfo.setProjectName(emp.getProjectName());
										  // for Name of component
										  employeInfo.setRefEmployee(employeectcTemplate.getCtcTemplateName());
										  // for End Date of Component
										  employeInfo.setDateOfRetirement(employeectcTemplate.getEndDate());
										 
										  updatedEmployeelist.add(employeInfo);
									  }
								  }
								  else {
									  if(employeectcTemplate.getEndDate().after(dateForFilter)
											  || employeectcTemplate.getEndDate().equals(dateForFilter)
											  && employeectcTemplate.getEndDate().before(duedateForFilter)
											  ||employeectcTemplate.getEndDate().equals(duedateForFilter)){
										  Employee employeInfo = new Employee();
										  employeInfo.setCount(emp.getCount());
										  employeInfo.setFullname(emp.getFullname());
										  employeInfo.setBranchName(emp.getBranchName());
										  employeInfo.setProjectName(emp.getProjectName());
										  // for Name of component
										  employeInfo.setRefEmployee(employeectcTemplate.getCtcTemplateName());
										  // for End Date of Component
										  employeInfo.setDateOfRetirement(employeectcTemplate.getEndDate());
										 
										  updatedEmployeelist.add(employeInfo);
									  }
								  }
									  
								 
							  }
						  
					  }
						
						logger.log(Level.SEVERE,"updatedEmployeelist size"+updatedEmployeelist.size());	
						ArrayList<String> tbl_header = new ArrayList<String>();
						
						Comparator<Employee> employeeComponentEndDateComparator2 = new Comparator<Employee>() {
							public int compare(Employee s1, Employee s2) {
							
							Date date1 = s1.getDateOfRetirement();
							Date date2 = s2.getDateOfRetirement();
							
							//ascending order
							return date1.compareTo(date2);
							}
							};
							Collections.sort(updatedEmployeelist, employeeComponentEndDateComparator2);
							
							Comparator<Employee> employeeComponentEndDateComparator = new Comparator<Employee>() {
								public int compare(Employee s1, Employee s2) {
								
								String branch1 = s1.getBranchName();
								String branch2 = s2.getBranchName();
								
								//ascending order
								return branch1.compareTo(branch2);
								}
								
								};
								Collections.sort(updatedEmployeelist, employeeComponentEndDateComparator);
							
								if(updatedEmployeelist.size()>0){
									tbl_header.add("Ser_No");
									tbl_header.add("Employee Id");
									tbl_header.add("Employee Name");
									tbl_header.add("Branch");
									tbl_header.add("Project");
									tbl_header.add("Component Name");
									tbl_header.add("End Date");
									tbl_header.add("Ageing");
										
							ArrayList<String> tbl1=new ArrayList<String>();
								
							for(int j=0;j<updatedEmployeelist.size();j++){
								tbl1.add(j+1+"");
						   	 	tbl1.add(updatedEmployeelist.get(j).getCount()+"");
					   	 	    tbl1.add(updatedEmployeelist.get(j).getFullname()+""); 
					   	 		tbl1.add(updatedEmployeelist.get(j).getBranchName());
					   	 		tbl1.add(updatedEmployeelist.get(j).getProjectName());
					   	 		tbl1.add(updatedEmployeelist.get(j).getRefEmployee()+"");
					   	 		tbl1.add(fmt1.format(updatedEmployeelist.get(j).getDateOfRetirement())+"");

					   	 		String StringServiceDate = fmt1.format(updatedEmployeelist.get(j).getDateOfRetirement());

					   	 			Date d1 = null;
					   				Date d2 = null;
					   				
					   				d1 = df.parse(StringServiceDate);
					   				d2 = df.parse(todayDateString);
					   				long diff = d2.getTime() - d1.getTime();
									long diffDays = diff / (24 * 60 * 60 * 1000);
									
					   	 	    tbl1.add(diffDays +"");
							
							}			
								System.out.println("Before send mail method");
								logger.log(Level.SEVERE,"Before send method call to send mail  ");	
								
								
								if(cronDetails.getFooter().trim()!= "" ){
									footer = cronDetails.getFooter();
								}
								
								
								
								if(cronDetails.getSubject()!=null && !cronDetails.getSubject().trim().equals("") && cronDetails.getSubject().trim().length() >0){
									emailSubject = cronDetails.getSubject();
									logger.log(Level.SEVERE,"emailSubject "+emailSubject);						

								}
								
								 emailMailBody =mailTitl +"  "+ todayDateString;
								
								if(cronDetails.getMailBody().trim() != ""  && cronDetails.getMailBody().trim().length() >0){
									emailMailBody = cronDetails.getMailBody();
								}
								
									System.out.println("Before send mail method");
									logger.log(Level.SEVERE,"Before send method call to send mail  ");						
								
									logger.log(Level.SEVERE,"toEmailListDt size"+toEmailListDt.size());						

									if(toEmailListDt.size()!=0){
										cronEmail.cronSendEmail(toEmailListDt, emailSubject, emailMailBody,
												c, tbl_header, tbl1, null, null, null, null,"",footer);
										logger.log(Level.SEVERE,"After send mail method ");		
									}
								
							
							}
//							else{
//									System.out.println("Sorry no services found for this company");
//									logger.log(Level.SEVERE,"Sorry no services found for this company");
//									mailTitl = "No Employee CTC Allocation Found Due";
//						
//									cronEmail.cronSendEmail(toEmailListDt, "Employee CTC Allocation due and overdue as on date"+" "+ todayDateString, mailTitl , c, null, null, null, null, null, null);
//						
//							}		
										
					 }catch(Exception e){
						 e.printStackTrace();
						 logger.log(Level.SEVERE,"get error is  recores"+e.getMessage());
					 }
					 
						 
				}
			}
		}	
		}
		}
		}
	}	
	}
	catch(Exception e){
		
	}
		
	}
	

	private String getEmployeeUserEmailId(Employee employee, List<User> userlist) {
		for(User userEntity : userlist){
			if(userEntity.getEmployeeName().trim().equals(employee.getFullName().trim())){
				if(userEntity.getEmail()!=null){
					return userEntity.getEmail();
				}
			}
		}
		return "";
	}
}
