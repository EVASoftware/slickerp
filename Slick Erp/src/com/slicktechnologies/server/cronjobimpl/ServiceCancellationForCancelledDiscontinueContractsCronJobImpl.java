package com.slicktechnologies.server.cronjobimpl;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

import static com.googlecode.objectify.ObjectifyService.ofy;
/**
 * @author Vijay Chougule
 * Des :- NBHC CCPM for Discontinue and Cancelled contracts sometimes services remaining in Scheduled/Rescheduled status only
 * So this is used for Cancelled/Discontinue scheduled/Rescheduled services will cancelled by this cron job.
 */
public class ServiceCancellationForCancelledDiscontinueContractsCronJobImpl extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6348046956898885767L;

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	 Logger logger = Logger.getLogger("NameOfYourLogger");
	  
	 
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		contractCancelAndDiscontinueServicesUpdation();
	}

	private void contractCancelAndDiscontinueServicesUpdation() {

		Date today=DateUtility.getDateWithTimeZone("IST", new Date());
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		
		logger.log(Level.SEVERE,"Date Before Adding One Day"+today);
		Calendar cal=Calendar.getInstance();
		cal.setTime(today);
		cal.add(Calendar.DATE, -1);
		
		Date minusOneDaysDate=null;
		
		try {
			minusOneDaysDate=dateFormat.parse(dateFormat.format(cal.getTime()));
			cal.set(Calendar.HOUR_OF_DAY,00);
			cal.set(Calendar.MINUTE,00);
			cal.set(Calendar.SECOND,00);
			cal.set(Calendar.MILLISECOND,000);
			minusOneDaysDate=cal.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		logger.log(Level.SEVERE,"Today Date -1 Days Date"+minusOneDaysDate);
		
		Calendar cal2=Calendar.getInstance();
		cal2.setTime(today);
		cal2.add(Calendar.DATE, 0);
		
		Date todaysDate=null;
		
		try {
			todaysDate=dateFormat.parse(dateFormat.format(cal2.getTime()));
			cal2.set(Calendar.HOUR_OF_DAY,23);
			cal2.set(Calendar.MINUTE,59);
			cal2.set(Calendar.SECOND,59);
			cal2.set(Calendar.MILLISECOND,999);
			todaysDate=cal2.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		logger.log(Level.SEVERE,"Today Date "+todaysDate);
		logger.log(Level.SEVERE,"Today Date "+today);
		
		List<Company> compEntity = ofy().load().type(Company.class).list();
		
		if(compEntity.size()>0){
			for(Company company : compEntity){
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableCancelServiceOfCancelledContract", company.getCompanyId())){
					ArrayList<String> statuslist = new ArrayList<String>();
					statuslist.add(Contract.CANCELLED);
					statuslist.add(Contract.CONTRACTDISCOUNTINED);
					List<Contract> contractlist = ofy().load().type(Contract.class).filter("companyId",company.getCompanyId()).filter("status IN", statuslist)
													.filter("cancellationDate >=", minusOneDaysDate).filter("cancellationDate <=", todaysDate).list();
					
					logger.log(Level.SEVERE, "contractlist Size"+contractlist.size());
					ArrayList<String> servicestatuslist = new ArrayList<String>();
					servicestatuslist.add(Service.SERVICESTATUSSCHEDULE);
					servicestatuslist.add(Service.SERVICESTATUSRESCHEDULE);
					
					for(Contract contract : contractlist){
						List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", company.getCompanyId()).filter("contractCount", contract.getCount())
													.filter("status IN", servicestatuslist).list();
						logger.log(Level.SEVERE, "servicelist Size"+servicelist.size());
						
						for(Service serviceentity : servicelist){
							serviceentity.setComment(contract.getDescription());
							serviceentity.setRemark(contract.getRemark());
							serviceentity.setCancellationDate(contract.getCancellationDate());
							serviceentity.setStatus(Service.CANCELLED);
						}
						if(servicelist.size()!=0){
							ofy().save().entities(servicelist).now();
							logger.log(Level.SEVERE, "Services updated successfully");	
						}
						
						ArrayList<Service> newServicelist = new ArrayList<Service>();
						for(Service service : servicelist){
							if(service.getServiceDate().after(today)){
								newServicelist.add(service);
							}
						}
						logger.log(Level.SEVERE, "Future Service list"+newServicelist.size());
						if(newServicelist.size()!=0){
							ofy().delete().entities(newServicelist);
							logger.log(Level.SEVERE, "Future services deleted successfully");
						}
					}
				}
				
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableDeleteOldRecords", company.getCompanyId())){
					SimpleDateFormat format = new SimpleDateFormat("mm/dd/yyyy");
					Date myDefaultDate = null;
					try {
						myDefaultDate = format.parse("31/03/2018");
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if(myDefaultDate!=null){
						List<Service> servicelist = ofy().load().type(Service.class).filter("serviceDate <=", myDefaultDate)
													.filter("companyId", company.getCompanyId()).list();
						logger.log(Level.SEVERE, " old servicelist "+servicelist.size());
						if(servicelist.size()!=0){
							ofy().delete().entities(servicelist);
						}
						List<ServiceProject> projectlist = ofy().load().type(ServiceProject.class).filter("serviceDate <=", myDefaultDate)
								.filter("companyId", company.getCompanyId()).list();
						logger.log(Level.SEVERE, " old servicelist "+projectlist.size());
						if(projectlist.size()!=0){
							ofy().delete().entities(projectlist);
						}
						List<MaterialIssueNote> minlist = ofy().load().type(MaterialIssueNote.class).filter("minDate <=", myDefaultDate)
								.filter("companyId", company.getCompanyId()).list();
						logger.log(Level.SEVERE, " old minlist "+minlist.size());
						if(minlist.size()!=0){
							ofy().delete().entities(minlist);
						}
						
					}
				}
			}
		}

		
	}
	
	

}
