package com.slicktechnologies.server.cronjobimpl;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class InvoiceARCronJobImpl extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8336094715145802028L;
	
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmtInvoiceDate = new SimpleDateFormat("dd/MM/yyyy");
	 Logger logger = Logger.getLogger("NameOfYourLogger");
	  
		public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws IOException {
			/**
			 *  nidhi
			 *  1-03-2018
			 *  cron j0b configration
			 */
//				invoiceARlist();
		}
		

		private void invoiceARlist() {

			Email cronEmail = new Email();
			
			Date today=DateUtility.getDateWithTimeZone("IST", new Date());
			
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			fmtInvoiceDate.setTimeZone(TimeZone.getTimeZone("IST"));
			
			
			/**
			 *   i have added time in today's date
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding "+today);
			Calendar cal=Calendar.getInstance();
			cal.setTime(today);
			cal.add(Calendar.DATE, 0);
			
			Date dateForFilter=null;
			
			try {
				dateForFilter=fmtInvoiceDate.parse(fmtInvoiceDate.format(cal.getTime()));
				cal.set(Calendar.HOUR_OF_DAY,23);
				cal.set(Calendar.MINUTE,59);
				cal.set(Calendar.SECOND,59);
				cal.set(Calendar.MILLISECOND,999);
				dateForFilter=cal.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Date After setting the Time"+dateForFilter);
			
			/************************************* End *********************************/
			
	 try{
		 
		 logger.log(Level.SEVERE,"In Invoice AR List");	
		logger.log(Level.SEVERE,"Date After Adding One Date"+dateForFilter);

		 
	/********************************Adding status in the list ***********************/
		 
		 ArrayList<String> obj = new ArrayList<String>();
		 obj.add("Created");
		 obj.add("Requested");
	/******************************Converting todayDate to String format*****************/
		 
		 Date todaysDate = new Date();
		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 String todayDateString = df.format(todaysDate);
		 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);

		 
 /********************************Adding Companies in the list ***********************/
		 
	List<Company> compEntity = ofy().load().type(Company.class).list();
	if(compEntity.size()>0){
	
		logger.log(Level.SEVERE,"If compEntity size > 0");
		logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
		

		for(int i=0;i<compEntity.size();i++){
			
			Company c=compEntity.get(i);
			logger.log(Level.SEVERE,"In the for loop");
			logger.log(Level.SEVERE,"Company Name="+c);

			
 /********************************Checking prosname & prosconfig for each company ***********************/

//			ProcessName prosName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
//			ProcessConfiguration prosconfig = ofy().load().type(ProcessConfiguration.class).filter("processList.processName","CronJob").filter("processList.processType","InvoiceARDailyEmail").filter("processList.status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();					

			ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
			ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
			
			boolean processconfigflag = false;
			if(processconfig!=null){
				if(processconfig.isConfigStatus()){
					for(int l=0;l<processconfig.getProcessList().size();l++){
						if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("InvoiceARDailyEmail") && processconfig.getProcessList().get(l).isStatus()==true){
							processconfigflag=true;
						}
					}
			   }
			}
			logger.log(Level.SEVERE,"process config  SMS Flag =="+processconfigflag);
			
			if(processName!=null){
				logger.log(Level.SEVERE,"In the prossName");
			if(processconfigflag){
							
				logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
				
				
	/********************************Reading Invoice List from Invoice entity  ***********************/
				
				List<Invoice> invoiceEntity = ofy().load().type(Invoice.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("invoiceDate <=",dateForFilter).filter("status IN",obj).filter("accountType", AppConstants.BILLINGACCOUNTTYPEAR).list();
				
				logger.log(Level.SEVERE,"Today date Is:"+dateForFilter);	

				logger.log(Level.SEVERE,"Invoice entity size:"+invoiceEntity.size());	
				
				
				// email id is added to emailList
				ArrayList<String> toEmailList=new ArrayList<String>();
				toEmailList.add(compEntity.get(i).getPocEmail());
				
				 String mailTitl = "Invoice Due As On Date";
				
				if(invoiceEntity.size()>0){
				
			
				ArrayList<String> tbl_header = new ArrayList<String>();
				tbl_header.add("Serial No");
				tbl_header.add("Branch");
				tbl_header.add("Customer Id");
				tbl_header.add("Customer Name");
				tbl_header.add("Customer Cont No");
				tbl_header.add("POC Name");
				tbl_header.add("Email Id");
				tbl_header.add("Sales Person");
				tbl_header.add("Person Responsible");
				tbl_header.add("OrderId");
				tbl_header.add("Invoice Id");
				tbl_header.add("Invoice Amt");
				tbl_header.add("Invoice Date");
				tbl_header.add("Status");
				tbl_header.add("Ageing");
				
/********************************Sorting table with Branch & Invoice Date ***********************/
			
				
				Comparator<Invoice> invoiceDateComparator2 = new Comparator<Invoice>() {
					public int compare(Invoice s1, Invoice s2) {
					
					Date date1 = s1.getInvoiceDate();
					Date date2 = s2.getInvoiceDate();
					
					//ascending order
					return date1.compareTo(date2);
					}
					};
					Collections.sort(invoiceEntity, invoiceDateComparator2);
					
					Comparator<Invoice> invoiceDateComparator = new Comparator<Invoice>() {
						public int compare(Invoice s1, Invoice s2) {
						
						String branch1 = s1.getBranch();
						String branch2 = s2.getBranch();
						
						//ascending order
						return branch1.compareTo(branch2);
						}
						
						};
						Collections.sort(invoiceEntity, invoiceDateComparator);
					
/********************************Getting invoiceEntity data and adding in the tbl1 List ***********************/
							
				ArrayList<String> tbl1=new ArrayList<String>();
					
				for(int j=0;j<invoiceEntity.size();j++){
					
					Customer customerEntity=ofy().load().type(Customer.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("count", invoiceEntity.get(j).getPersonInfo().getCount()).first().now();
					
					tbl1.add((j+1)+"");
		   	 		tbl1.add(invoiceEntity.get(j).getBranch()+"");
		   	 		tbl1.add(invoiceEntity.get(j).getPersonInfo().getCount()+"");
		   	 		tbl1.add(invoiceEntity.get(j).getPersonInfo().getFullName()+"");		   	 		
		   	 		tbl1.add(invoiceEntity.get(j).getPersonInfo().getCellNumber()+"");
		   	 		tbl1.add(invoiceEntity.get(j).getPersonInfo().getPocName());
		   	 		tbl1.add(customerEntity.getEmail());
		   	 		tbl1.add(invoiceEntity.get(j).getEmployee());
		   	 		tbl1.add(invoiceEntity.get(j).getEmployee());
		   	 		tbl1.add(invoiceEntity.get(j).getContractCount()+"");
		   	 		tbl1.add(invoiceEntity.get(j).getCount()+"");
		   	 		tbl1.add(invoiceEntity.get(j).getInvoiceAmount()+"");
		   	 		tbl1.add(fmt.format(invoiceEntity.get(j).getInvoiceDate())+""); 	 		
		 
		   	 	    /**************** for getting ageing for each Invoice ******/
		   	 		String StringInvoiceDate = fmtInvoiceDate.format(invoiceEntity.get(j).getInvoiceDate());
		   	 	    
		   				Date d1 = null;
		   				Date d2 = null;
		   				
		   				d1 = df.parse(StringInvoiceDate);
		   				d2 = df.parse(todayDateString);
		   				long diff = d2.getTime() - d1.getTime();
						long diffDays = diff / (24 * 60 * 60 * 1000);
						
//						System.out.println("Ageing:"+diffDays);
//				   	 	logger.log(Level.SEVERE,"Ageing:"+diffDays);
			
		   	 	    tbl1.add(invoiceEntity.get(j).getStatus()+"");
		   	 	    tbl1.add(diffDays +"");
		   	 	}			
				
				logger.log(Level.SEVERE,"In the ProsConfig !=null   3 ");
				
   /********************************Calling cronsendEmail Method ***********************/
				
				try {   
					   
						System.out.println("Before send mail method");
						logger.log(Level.SEVERE,"Before send method call to send mail  ");						
						cronEmail.cronSendEmail(toEmailList, "Invoice Due As On Date"+" "+ todayDateString, mailTitl +"  "+ todayDateString, c, tbl_header, tbl1, null, null, null, null);
						logger.log(Level.SEVERE,"After send mail method ");		
								//+"  "+
				} catch (IOException e1) {
					
					logger.log(Level.SEVERE,"In the catch block ");
					e1.printStackTrace();
			}
				
			}
			else{						
					
				System.out.println("Sorry no Invoice found for this company");
				logger.log(Level.SEVERE,"Sorry no Invoice found for this company");
				
				mailTitl = "No Invoice Found Due";

				cronEmail.cronSendEmail(toEmailList, "Invoice Due As On Date"+" "+ todayDateString, mailTitl , c, null, null, null, null, null, null);

			}
				
				
			}
			else{					//else block for prosconfig
					logger.log(Level.SEVERE,"ProcessConfiguration is null");
					System.out.println("ProcessConfiguration is null");
			}
			
			}else{        //else block for pross!=null
				logger.log(Level.SEVERE,"Cron job status is Inactive");
				System.out.println("Cron job status is Inactive");
			}
			
							
		}	//end of for loop
		
	}
	else{			//else block for if(compEntity.size()>0)
		
		logger.log(Level.SEVERE,"No Company found from cron job completed");	
		System.out.println("No Company found from cron job completed");
	}
	
	
	 }catch(Exception e2){
	
	e2.printStackTrace();
	 }
		
			
			
	
		
			
		
			
		}
		
		/**
		 *  nidhi
		 *  1-03-2018
		 *  cron job configration
		 * @param cronList
		 * @param empRoleList
		 */
public void invoiceARlist(List<CronJobConfigrationDetails> cronList,HashSet<String>  empRoleList) {
	
	//String strCronDetails = req.getParameter("cronList");
	logger.log(Level.SEVERE, "Cron list str --" + cronList);
	Gson gson = new Gson();
	JSONArray jsonarr = null;
	
	try {
		
		List<Company> compEntity = ofy().load().type(Company.class).list();
		if(compEntity.size()>0){
		
			logger.log(Level.SEVERE,"If compEntity size > 0");
			logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
			

			for(int k=0;k<compEntity.size();k++){
				
				Company c=compEntity.get(k);
		
				for(String empRoleName :empRoleList){
					/**
					 * @author Anil , Date : 25-04-2019
					 * while loading employee added filter ,its status should  be active 
					 * Because mails were triggered on in active employee ids also
					 */
					List<Employee>  employeeList=new ArrayList<Employee>();
					employeeList=ofy().load().type(Employee.class).filter("companyId", c.getCompanyId()).filter("roleName", empRoleName).filter("status", true).list();
					logger.log(Level.SEVERE,""+empRoleName+" "+"Employee List size::: "+employeeList.size());
					
						for (Employee employee : employeeList) {

							HashMap<String, String> empCount = new HashMap<String,String>();
							HashMap<String, String> branchCount = new HashMap<String, String>();
							
							int total =0;
							ArrayList<String> overHeader = new ArrayList<String>();
							ArrayList<String> dueHeader = new ArrayList<String>();
							ArrayList<String> overDetail = new ArrayList<String>();
							ArrayList<String> dueDetail = new ArrayList<String>();
							
							String header1 = "",header2 = "",title = "";
							String todayDateString ="";
							String footer = "";
								
							/**
							 * @author Anil,Date : 13-02-2019
							 * Renaming mail subject as per rohan's instruction	
							 */
							 
							 /******************************Converting todayDate to String format*****************/
							 
							Date todaysDate = new Date();
							SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
							todayDateString = df.format(todaysDate);
							  
//							String mailTitl = "Amount Recievable As On Date";
//							String emailSubject =  "Amount Recievable As On Date"+" "+ todayDateString;
							String emailSubject =  "Unbilled outstanding due and overdue report as on date"+" "+ todayDateString;
							String mailTitl = "";
							String emailMailBody = mailTitl;
							
							
							ArrayList<String> toEmailListDt=new ArrayList<String>();
							toEmailListDt.add(employee.getEmail().trim());
							List<String> branchList=new ArrayList<String>();
							branchList.add(employee.getBranchName());
							for (int j = 0; j < employee.getEmpBranchList().size(); j++) {
								branchList.add(employee.getEmpBranchList().get(j).getBranchName());
								logger.log(Level.SEVERE, "  -- branch list --"+ employee.getEmpBranchList().get(j).getBranchName());
							}
							logger.log(Level.SEVERE,"toEmailList size::: "+toEmailListDt.size()+" Branch List SIze : "+branchList.size());
							logger.log(Level.SEVERE,"toEmailList size::: "+branchList.toString());
							
							if(branchList.size()!=0){
								for(int i=0;i<cronList.size();i++){
									if(empRoleName.equalsIgnoreCase(cronList.get(i).getEmployeeRole())){
									
										CronJobConfigrationDetails cronDetails =cronList.get(i);
										
										Date today=DateUtility.getDateWithTimeZone("IST", new Date());
										TimeZone.setDefault(TimeZone.getTimeZone("IST"));
										fmt.setTimeZone(TimeZone.getTimeZone("IST"));
										fmtInvoiceDate.setTimeZone(TimeZone.getTimeZone("IST"));
										
										/**
										 *   i have added time in today's date
										 */
										
										logger.log(Level.SEVERE,"Date Before Adding "+today);
										Calendar cal=Calendar.getInstance();
										cal.setTime(today);
										cal.add(Calendar.DATE, 0);
										
										Date dateForFilter=null;
										
										
										try {
											dateForFilter=fmtInvoiceDate.parse(fmtInvoiceDate.format(cal.getTime()));
											cal.set(Calendar.HOUR_OF_DAY,0);
											cal.set(Calendar.MINUTE,0);
											cal.set(Calendar.SECOND,0);
											cal.set(Calendar.MILLISECOND,0);
											dateForFilter=cal.getTime();
										} catch (ParseException e) {
											e.printStackTrace();
										}
										
										logger.log(Level.SEVERE,"Date After setting the Time"+dateForFilter);
										
										cal.setTime(today);
										int diffDay = 0;
										 if(cronDetails.getOverdueDays()>0 && cronDetails.isOverdueStatus()){
											 diffDay = -cronDetails.getOverdueDays();
										 }else{
											 diffDay = cronDetails.getInterval();
										 }
										logger.log(Level.SEVERE," get diff days -- " + diffDay);
										cal.add(Calendar.DATE, diffDay);
										
										Date duedateForFilter=null;
										
										try {
											duedateForFilter=fmtInvoiceDate.parse(fmtInvoiceDate.format(cal.getTime()));
											cal.set(Calendar.HOUR_OF_DAY,23);
											cal.set(Calendar.MINUTE,59);
											cal.set(Calendar.SECOND,59);
											cal.set(Calendar.MILLISECOND,999);
											duedateForFilter=cal.getTime();
											
										} catch (ParseException e) {
											e.printStackTrace();
										}
										
										
										logger.log(Level.SEVERE,"Date After setting time"+duedateForFilter);
										/************************************* End *********************************/
										
								 try{
									 
									 logger.log(Level.SEVERE,"In Invoice AR List" + cronDetails.isOverdueStatus());	
									logger.log(Level.SEVERE,"Date After Adding One Date"+dateForFilter);
					
									 
								/********************************Adding status in the list ***********************/
									 
									 ArrayList<String> obj = new ArrayList<String>();
									 obj.add("Created");
									 obj.add("Requested");
									 obj.add("Rejected");    //Added by Ashwini
									 obj.add(Invoice.PROFORMAINVOICE);
								
					
									 

										List<Invoice> invoiceEntity = new ArrayList<Invoice>(); 
										
										if(cronDetails.isOverdueStatus()){
											
											/**
											 * @author Anil,Date : 16-02-2019
											 */
											emailSubject =  "Unbilled outstanding overdue report as on date"+" "+ todayDateString;
											
											 logger.log(Level.SEVERE,"in over due" + cronDetails.isOverdueStatus() +" id -- " + compEntity.get(k).getCompanyId());	
											logger.log(Level.SEVERE,"in over due" + cronDetails.isOverdueStatus() +" duedateForFilter" + duedateForFilter);
											invoiceEntity = ofy().load().type(Invoice.class)
													.filter("companyId",compEntity.get(k).getCompanyId())
													.filter("invoiceDate <=",duedateForFilter)
													.filter("branch IN", branchList)
													.filter("status IN",obj).filter("accountType","AR").list();
											logger.log(Level.SEVERE,"get over due -- " + invoiceEntity.size());
										}else{
											/**
											 * @author Anil,Date : 16-02-2019
											 */
											emailSubject =  "Unbilled outstanding due report as on date"+" "+ todayDateString;
											logger.log(Level.SEVERE,"in  due" + cronDetails.isOverdueStatus());
											invoiceEntity = ofy().load().type(Invoice.class)
													.filter("companyId",compEntity.get(k).getCompanyId())
													.filter("invoiceDate <=",duedateForFilter)
													.filter("invoiceDate >=",dateForFilter)
													.filter("branch IN", branchList)
													.filter("status IN",obj).filter("accountType", "AR").list();
										}
						
										
										
										
										logger.log(Level.SEVERE,"Today date Is:"+dateForFilter);	
	
										logger.log(Level.SEVERE,"Invoice entity size:"+invoiceEntity.size());	
										
										
										// email id is added to emailList
										 String mailTitl1 = "Invoice Due As On Date";
										
										if(invoiceEntity.size()>0){
										
									
										
										
						/********************************Sorting table with Branch & Invoice Date ***********************/
									
										
										Comparator<Invoice> invoiceDateComparator2 = new Comparator<Invoice>() {
											public int compare(Invoice s1, Invoice s2) {
											
											Date date1 = s1.getInvoiceDate();
											Date date2 = s2.getInvoiceDate();
											
											//ascending order
											return date1.compareTo(date2);
											}
											};
											Collections.sort(invoiceEntity, invoiceDateComparator2);
											
											Comparator<Invoice> invoiceDateComparator = new Comparator<Invoice>() {
												public int compare(Invoice s1, Invoice s2) {
												
												String branch1 = s1.getBranch();
												String branch2 = s2.getBranch();
												
												//ascending order
												return branch1.compareTo(branch2);
												}
												
												};
												Collections.sort(invoiceEntity, invoiceDateComparator);
											logger.log(Level.SEVERE," get sort succesfully.. " + invoiceEntity.size());
						/********************************Getting invoiceEntity data and adding in the tbl1 List ***********************/
													
												ArrayList<String> tbl_header = new ArrayList<String>();
												tbl_header.add("Serial No");
												tbl_header.add("Branch");
												tbl_header.add("Invoice Id");
												tbl_header.add("Invoice Date");
												tbl_header.add("Invoice Amt");
												tbl_header.add("Customer Name");
												tbl_header.add("Cell No");
												tbl_header.add("POC Name");
												tbl_header.add("Email Id");
												tbl_header.add("Sales Person");
												tbl_header.add("Person Responsible");
												tbl_header.add("OrderId");
												tbl_header.add("Ref_No");
												
												tbl_header.add("Status");
												tbl_header.add("Ageing");
												
										ArrayList<String> tbl1=new ArrayList<String>();
											
										for(int j=0;j<invoiceEntity.size();j++){
											
											Customer customerEntity=ofy().load().type(Customer.class).filter("companyId", compEntity.get(k).getCompanyId())
													.filter("count", invoiceEntity.get(j).getPersonInfo().getCount()).first().now();
									
											logger.log(Level.SEVERE,"get emp count--- " + invoiceEntity.get(j).getCount()+" branch count --" +invoiceEntity.get(j).getBranch());
											
											tbl1.add((j+1)+"");
								   	 		tbl1.add(invoiceEntity.get(j).getBranch()+"");
								   	 		tbl1.add(invoiceEntity.get(j).getCount()+"");
								   	 		if(invoiceEntity.get(j).getInvoiceDate()!=null){
								   	 			tbl1.add(fmt.format(invoiceEntity.get(j).getInvoiceDate())+"");
								   	 		}else{
								   	 			tbl1.add("");
								   	 		}
								   	 		tbl1.add(invoiceEntity.get(j).getInvoiceAmount()+"");
								   	 		tbl1.add(invoiceEntity.get(j).getPersonInfo().getFullName()+"");		   	 		
								   	 		tbl1.add(invoiceEntity.get(j).getPersonInfo().getCellNumber()+"");
								   	 		tbl1.add(invoiceEntity.get(j).getPersonInfo().getPocName());
								   	 		if(customerEntity != null){
								   	 			tbl1.add(customerEntity.getEmail());
								   	 		}else{
								   	 			tbl1.add("");
								   	 		}
								   	 		
								   	 		tbl1.add(invoiceEntity.get(j).getEmployee());
								   	 		tbl1.add(invoiceEntity.get(j).getEmployee());
								   	 		if(invoiceEntity.get(j).getContractCount() == null){
								   	 			String str = "";
								   	 			for(BillingDocumentDetails billdoc : invoiceEntity.get(j).getArrayBillingDocument()){
								   	 				str = billdoc.getOrderId() +",";
								   	 			}
								   	 			if(str.length()>0){
									   	 			str = str.substring(str.length()-1);
									   	 			tbl1.add(str);
								   	 			}else{
								   	 				tbl1.add("");
								   	 			}
								   	 			
								   	 		}else{
								   	 			tbl1.add(invoiceEntity.get(j).getContractCount()+"");
								   	 		}
								   	 		
								   	 		if(invoiceEntity.get(j).getRefNumber()!=null){
								   	 			tbl1.add(invoiceEntity.get(j).getRefNumber()+"");
								   	 		}else{
								   	 			tbl1.add("");
								   	 		}
								   	 	    /**************** for getting ageing for each Invoice ******/
								   	 		String StringInvoiceDate = fmtInvoiceDate.format(invoiceEntity.get(j).getInvoiceDate());
								   	 	    
								   				Date d1 = null;
								   				Date d2 = null;
								   				
								   				d1 = df.parse(StringInvoiceDate);
								   				d2 = df.parse(todayDateString);
								   				long diff = d2.getTime() - d1.getTime();
												long diffDays = diff / (24 * 60 * 60 * 1000);
												
	//											System.out.println("Ageing:"+diffDays);
	//									   	 	logger.log(Level.SEVERE,"Ageing:"+diffDays);
									
								   	 	    tbl1.add(invoiceEntity.get(j).getStatus()+"");
								   	 	    tbl1.add(diffDays +"");
								   	 	    
								   	 	  if(empCount.containsKey(invoiceEntity.get(j).getEmployee().trim())){
											   String[] countdata = empCount.get(invoiceEntity.get(j).getEmployee().trim()).split("-");
											   ArrayList<String> countDetails = new ArrayList<String>(Arrays.asList(countdata));

											   int count1 = Integer.parseInt(countDetails.get(0));
								   	 	    			
								   	 	    	count1++;
								   	 	    	
								   	 	    	double sum = Double.parseDouble(countDetails.get(1))+invoiceEntity.get(j).getInvoiceAmount();
								   	 	    	
								   	 	    	empCount.put(invoiceEntity.get(j).getEmployee().trim(), count1+"-"+sum);
								   	 	    }else{
								   	 	    	empCount.put(invoiceEntity.get(j).getEmployee().trim(), 1+"-"+invoiceEntity.get(j).getInvoiceAmount());
								   	 	    }
										
								   	 	    if(branchCount.containsKey(invoiceEntity.get(j).getBranch().trim())){
								   	 	    	
									   	 	    String[] countdata = branchCount.get(invoiceEntity.get(j).getBranch().trim()).split("-");
												   ArrayList<String> countDetails = new ArrayList<String>(Arrays.asList(countdata));

												   int count1 = Integer.parseInt(countDetails.get(0));
									   	 	    			
									   	 	    	count1++;
									   	 	    	
									   	 	    	double sum = Double.parseDouble(countDetails.get(1))+invoiceEntity.get(j).getInvoiceAmount();
									   	 	    	
									   	 	    branchCount.put(invoiceEntity.get(j).getBranch().trim(), count1+"-"+sum);
								   	 	    	
								   	 	    }else{
								   	 	    	branchCount.put(invoiceEntity.get(j).getBranch().trim(), 1+"-"+invoiceEntity.get(j).getInvoiceAmount());
								   	 	    }
								   	 	total++;
								   	 	}
										
										
										header1 = "Invoice amount recievable due as date on :" +todayDateString;
										if(cronDetails.isOverdueStatus()){
											header1 = "Invoice amount recievable overdue as date on :" +todayDateString;
										}
										
										if(!cronDetails.getSubject().trim().equals("")){
											header1 =  cronDetails.getSubject();
										}
										if(!cronDetails.getMailBody().equals("")){
											header1 = header1 + "<br>"+ cronDetails.getMailBody();
										}
										dueHeader = new ArrayList<String>();
										dueHeader.addAll(tbl_header);
										dueDetail = new ArrayList<String>();
										dueDetail.addAll(tbl1);
									}	
										
										logger.log(Level.SEVERE,"get billing detailss-- ");
										
										List<BillingDocument> billingEntity = new ArrayList<BillingDocument>(); 
										 obj = new ArrayList<String>();
										 obj.add(BillingDocument.APPROVED);
										 
										 /*
										  * date:10/08/2018
										  * Developer:Ashwini
										  * Des:to add created and rejected status
										  */
									     obj.add(BillingDocument.CREATED);
									     obj.add(BillingDocument.REJECTED);
									     obj.add(BillingDocument.REQUESTED);
									     /*
									      * end by Ashwini
									      */
									     
									     /**
									      * @author Anil,Date : 13-02-2019
									      * updated status for loading bills as per the rohan's instruction
									      */
									     
									     ArrayList<String> billingStatus = new ArrayList<String>();
										 billingStatus.add("Created");
										 billingStatus.add("Requested");
										 billingStatus.add("Rejected");    
										 billingStatus.add("Approved");
									     
										 if(cronDetails.isOverdueStatus()){
											 /**
											  * @author Anil,Date : 16-02-2019
											  */
											 emailSubject =  "Unbilled outstanding overdue report as on date"+" "+ todayDateString;
											
											cal.set(Calendar.HOUR_OF_DAY,23);
											cal.set(Calendar.MINUTE,59);
											cal.set(Calendar.SECOND,59);
											cal.set(Calendar.MILLISECOND,999);
											duedateForFilter=cal.getTime();
											
											billingEntity = ofy().load().type(BillingDocument.class)
													.filter("companyId",compEntity.get(k).getCompanyId())
													.filter("branch IN", branchList)
													.filter("billingDocumentInfo.billingDate <=",duedateForFilter)
													.filter("status IN",billingStatus).filter("accountType", "AR").list();
										}else{
											 /**
											  * @author Anil,Date : 16-02-2019
											  */
											 emailSubject =  "Unbilled outstanding due report as on date"+" "+ todayDateString;
											
											billingEntity = ofy().load().type(BillingDocument.class)
													.filter("branch IN", branchList)
													.filter("companyId",compEntity.get(k).getCompanyId()).filter("billingDocumentInfo.billingDate <=",duedateForFilter)
													.filter("billingDocumentInfo.billingDate >=",dateForFilter)
													.filter("status IN",billingStatus).filter("accountType", "AR").list();
										}
						
										
										
										
										logger.log(Level.SEVERE,"Today date Is:"+dateForFilter);	
	
										logger.log(Level.SEVERE,"billing entity size:"+invoiceEntity.size());	
										
										
										// email id is added to emailList
										
										  mailTitl1 = "Billing Due As On Date";
										  if(cronDetails.isOverdueStatus()){
											  mailTitl1 = "Billing Over Due as Date on :" +todayDateString;
											}
										if(billingEntity.size()>0){
										
									
										ArrayList<String> tbl_header1 = new ArrayList<String>();
																						
										
										
						/********************************Sorting table with Branch & Invoice Date ***********************/
									
										
										Comparator<BillingDocument> billingDateComparator2 = new Comparator<BillingDocument>() {
											public int compare(BillingDocument s1, BillingDocument s2) {
											
											Date date1 = s1.getBillingDate();
											Date date2 = s2.getBillingDate();
											
											//ascending order
											return date1.compareTo(date2);
											}
											};
											Collections.sort(billingEntity, billingDateComparator2);
											
											Comparator<BillingDocument> billingDateComparator = new Comparator<BillingDocument>() {
												public int compare(BillingDocument s1, BillingDocument s2) {
												
												String branch1 = s1.getBranch();
												String branch2 = s2.getBranch();
												
												//ascending order
												return branch1.compareTo(branch2);
												}
												
												};
												Collections.sort(billingEntity, billingDateComparator);
											
						/********************************Getting invoiceEntity data and adding in the tbl1 List ***********************/
													
										ArrayList<String> billingtbl1=new ArrayList<String>();
										tbl_header1.add("Serial No");
										tbl_header1.add("Branch");
										tbl_header1.add("Billing Id");
										tbl_header1.add("Billing Date");
										tbl_header1.add("Billing Amt");
										tbl_header1.add("Customer Name");
										tbl_header1.add("Cell No");
										tbl_header1.add("POC Name");
										tbl_header1.add("Email Id");
										tbl_header1.add("Sales Person");
										tbl_header1.add("Person Responsible");
										tbl_header1.add("OrderId");
										tbl_header1.add("Ref_No");
										
										tbl_header1.add("Status");
										tbl_header1.add("Ageing");
										
										for(int j=0;j<billingEntity.size();j++){
											
											Customer customerEntity=ofy().load().type(Customer.class).filter("companyId", compEntity.get(k).getCompanyId())
													.filter("count", billingEntity.get(j).getPersonInfo().getCount()).first().now();
											logger.log(Level.SEVERE," j-- " + j + "  count -- " +billingEntity.get(j).getCount());
											billingtbl1.add((j+1)+"");
											billingtbl1.add(billingEntity.get(j).getBranch()+"");
											billingtbl1.add(billingEntity.get(j).getCount()+"");
											billingtbl1.add(fmt.format(billingEntity.get(j).getBillingDate())+""); 
											billingtbl1.add(billingEntity.get(j).getTotalBillingAmount()+"");
											billingtbl1.add(billingEntity.get(j).getPersonInfo().getFullName()+"");		   	 		
											billingtbl1.add(billingEntity.get(j).getPersonInfo().getCellNumber()+"");
											billingtbl1.add(billingEntity.get(j).getPersonInfo().getPocName());
//																billingtbl1.add(customerEntity.getEmail());
											if(customerEntity != null){
												billingtbl1.add(customerEntity.getEmail());
								   	 		}else{
								   	 			billingtbl1.add("");
								   	 		}
											billingtbl1.add(billingEntity.get(j).getEmployee());
											billingtbl1.add(billingEntity.get(j).getEmployee());
											billingtbl1.add(billingEntity.get(j).getContractCount()+"");
											billingtbl1.add(billingEntity.get(j).getRefNumber());
								 
								   	 	    /**************** for getting ageing for each Invoice ******/
								   	 		String StringInvoiceDate = fmtInvoiceDate.format(billingEntity.get(j).getBillingDate());
								   	 	    
								   				Date d1 = null;
								   				Date d2 = null;
								   				
								   				d1 = df.parse(StringInvoiceDate);
								   				d2 = df.parse(todayDateString);
								   				long diff = d2.getTime() - d1.getTime();
												long diffDays = diff / (24 * 60 * 60 * 1000);
												
									
											billingtbl1.add(billingEntity.get(j).getStatus()+"");
											billingtbl1.add(diffDays +"");
											
											
											 if(empCount.containsKey(billingEntity.get(j).getEmployee().trim())){
												   String[] countdata = empCount.get(billingEntity.get(j).getEmployee().trim()).split("-");
 												   ArrayList<String> countDetails = new ArrayList<String>(Arrays.asList(countdata));

 												   int count1 = Integer.parseInt(countDetails.get(0));
									   	 	    			
									   	 	    	count1++;
									   	 	    	
									   	 	    	double sum = Double.parseDouble(countDetails.get(1))+billingEntity.get(j).getTotalBillingAmount();
									   	 	    	
									   	 	    	empCount.put(billingEntity.get(j).getEmployee().trim(), count1+"-"+sum);
									   	 	    }else{
									   	 	    	empCount.put(billingEntity.get(j).getEmployee().trim(), 1+"-"+billingEntity.get(j).getTotalBillingAmount());
									   	 	    }
											
									   	 	    if(branchCount.containsKey(billingEntity.get(j).getBranch().trim())){
									   	 	    	
										   	 	    String[] countdata = branchCount.get(billingEntity.get(j).getBranch().trim()).split("-");
													   ArrayList<String> countDetails = new ArrayList<String>(Arrays.asList(countdata));
	
													   int count1 = Integer.parseInt(countDetails.get(0));
										   	 	    			
										   	 	    	count1++;
										   	 	    	
										   	 	    	double sum = Double.parseDouble(countDetails.get(1))+billingEntity.get(j).getTotalBillingAmount();
										   	 	    	
										   	 	    branchCount.put(billingEntity.get(j).getBranch().trim(), count1+"-"+sum);
									   	 	    	
									   	 	    }else{
									   	 	    	branchCount.put(billingEntity.get(j).getBranch().trim(), 1+"-"+billingEntity.get(j).getTotalBillingAmount());
									   	 	    }
									   	 	total++;
								   	 	}	
										
										header2 = "Billing amount recievable due as date on :" +todayDateString;
										if(cronDetails.isOverdueStatus()){
											header2 = "Billing amount recievable overdue as date on :" +todayDateString;
										}
										
										if(!cronDetails.getSubject().trim().equals("")){
											header2 =  cronDetails.getSubject();
										}
										if(!cronDetails.getMailBody().equals("")){
											header2 = header2 + "<br>"+ cronDetails.getMailBody();
										}
										
										overHeader = new ArrayList<String>();
										overDetail = new ArrayList<String>();
										overHeader.addAll(tbl_header1);
										overDetail.addAll(billingtbl1);
									}
								 }catch(Exception e){
									 e.printStackTrace();
									 logger.log(Level.SEVERE," get Inv Re"+e);
									 logger.log(Level.SEVERE," get Inv Re"+e.getMessage());
								 }
									
										try {   
											Email cronEmail = new Email();
											if(overDetail.size()>0 || dueDetail.size()>0){
												
												logger.log(Level.SEVERE," get branch count --" + branchCount.size() + " emp count --" + empCount.size());
												logger.log(Level.SEVERE," get branch  --" + branchCount.toString() + " emp count --" + empCount.toString());
												ArrayList<String> summHeader  = new ArrayList<String>();
												ArrayList<String> summDetails = new ArrayList<String>();
												
												summHeader.add("Branch");
												summHeader.add("$#$");
												summHeader.add("$#$");
												summHeader.add("Sales Person");
												summHeader.add("$#$");
												summHeader.add("$#$");
												int tblSize = 0;
												if(branchCount.size()>0){
													tblSize = branchCount.size();
												}
												
												if(tblSize<empCount.size()){
													tblSize = empCount.size();
												}

												Set<String> branchCntList = branchCount.keySet();
												ArrayList<String> branchDt = new ArrayList<String>();
												branchDt.addAll(branchCntList);
												
												Set<String> empCntList = empCount.keySet();
												ArrayList<String> empDt = new ArrayList<String>();
												empDt.addAll(empCntList);
												
												
												summDetails.add("Branch Name");
												summDetails.add("No of Records");
												summDetails.add("Total Amount");
												
												summDetails.add("Sales Person");
												summDetails.add("No of Records");
												summDetails.add("Total Amount");

												/**
												 * @author Anil,Date : 16-02-2019
												 * Addin decimal format for total amount
												 */
												
												DecimalFormat dff=new DecimalFormat("0.00");

												for(int ij=0;ij<tblSize;ij++){
													if(branchCount.size()>ij){
														
														summDetails.add(branchDt.get(ij)+"");
														
														   String[] countdata = branchCount.get(branchDt.get(ij)).split("-");
														   ArrayList<String> countDetails = new ArrayList<String>(Arrays.asList(countdata));
											   	 	    	
														summDetails.add(countDetails.get(0));
														
														summDetails.add(dff.format(dff.parse(countDetails.get(1))));
													}else{
														summDetails.add("");
														summDetails.add("");
														summDetails.add("");
													}
													
													if(empCount.size()>ij){
														
														
														summDetails.add(empDt.get(ij)+"");
														
														   String[] countdata = empCount.get(empDt.get(ij)).split("-");
														   ArrayList<String> countDetails = new ArrayList<String>(Arrays.asList(countdata));
											   	 	    	
														summDetails.add(countDetails.get(0));
											   	 	    
														
														summDetails.add(dff.format(dff.parse(countDetails.get(1))));
														
													}else{
														summDetails.add("");
														summDetails.add("");
														summDetails.add("");
													}
												}
												String summTitle = " Total Records - " + total ;
												logger.log(Level.SEVERE,"Before send method call to send mail  " + total);						
											
																	
												
												cronEmail.cronSendEmailDueOverDue(toEmailListDt, emailSubject, "", c,header1, dueHeader, dueDetail,
														header2, overHeader, overDetail, summTitle,summHeader,summDetails,null, "", footer);
												logger.log(Level.SEVERE,"after send method call to send mail  ");	
											}
											else{
												
//																	logger.log(Level.SEVERE,"Sorry GRN Note found due this company");	
//																	mailTitl = "No Goods Recieved Note Found Due";
//								
//																	cronEmail.cronSendEmail(toEmailListDt, "Contract renew Due As On Date"+" "+ todayDateString, mailTitl , c, null, null, null, null, null, null);
												System.out.println("Sorry no Invoice found for this company");
												logger.log(Level.SEVERE,"Sorry no Invoice found for this company");
												
												mailTitl = "No Invoice Found Due";

												cronEmail.cronSendEmail(toEmailListDt, "Invoice Due As On Date"+" "+ todayDateString, mailTitl , c, null, null, null, null, null, null);

											}
											
											logger.log(Level.SEVERE,"After send mail method ");		
													//+"  "+
									} catch (IOException e1) {
										
										logger.log(Level.SEVERE,"In the catch block ");
										e1.printStackTrace();
								}
									
								}
									
								}
								
								/**
								 * 
								 */
								
							}
				
						}
				
				}
			
			
			}
		}
		
	} catch (Exception e) {
		e.printStackTrace();
		logger.log(Level.SEVERE,"get error in main --" + e);
	}
	
	
	

	
}

}
