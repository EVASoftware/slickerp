package com.slicktechnologies.server.cronjobimpl;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class SalesQuotationCronJobImpl extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5978385349296411641L;
	
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmtQuotationDate = new SimpleDateFormat("dd/MM/yyyy");
	 Logger logger = Logger.getLogger("NameOfYourLogger");
	  
		public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws IOException {
			
				SalesQuotationList();
		}

		private void SalesQuotationList() {

			

			
			Email cronEmail = new Email();
			
			Date today=DateUtility.getDateWithTimeZone("IST", new Date());
			
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			fmtQuotationDate.setTimeZone(TimeZone.getTimeZone("IST"));
			
			/**
			 *    i have added time in today's date
			 */
			
			logger.log(Level.SEVERE,"Date Before Adding "+today);
			Calendar cal=Calendar.getInstance();
			cal.setTime(today);
			cal.add(Calendar.DATE, 0);
			
			Date dateForFilter=null;
			
			try {
				dateForFilter=fmtQuotationDate.parse(fmtQuotationDate.format(cal.getTime()));
				cal.set(Calendar.HOUR_OF_DAY,23);
				cal.set(Calendar.MINUTE,59);
				cal.set(Calendar.SECOND,59);
				cal.set(Calendar.MILLISECOND,999);
				dateForFilter=cal.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Date After setting the Time"+dateForFilter);
			
			/************************************* End *********************************/
			
			
	 try{
		 
		 logger.log(Level.SEVERE,"In Sales Quotation List");	

		 
	/********************************Adding status in the list ***********************/
		 
		 ArrayList<String> obj = new ArrayList<String>();
		 obj.add("Created");
		 obj.add("Requested");
		 obj.add("Approved");

		 
	
	/******************************Converting todayDate to String format*****************/
		 
		 Date todaysDate = new Date();
		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 String todayDateString = df.format(todaysDate);
		 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);

		 
 /********************************Adding Companies in the list ***********************/
		 
	List<Company> compEntity = ofy().load().type(Company.class).list();
	if(compEntity.size()>0){
	
		logger.log(Level.SEVERE,"If compEntity size > 0");
		logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
		

		for(int i=0;i<compEntity.size();i++){
			
			Company c=compEntity.get(i);
			logger.log(Level.SEVERE,"In the for loop");
			logger.log(Level.SEVERE,"Company Name="+c);
			logger.log(Level.SEVERE,"The value of i is:" +i);
			logger.log(Level.SEVERE,"Date After Adding One Date"+dateForFilter);

			
 /********************************Checking prosname & prosconfig for each company ***********************/
		
			ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
			ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
			
			boolean processconfigflag = false;
			if(processconfig!=null){
				if(processconfig.isConfigStatus()){
					for(int l=0;l<processconfig.getProcessList().size();l++){
						if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("SalesQuotationDailyEmail") && processconfig.getProcessList().get(l).isStatus()==true){
							processconfigflag=true;
						}
					}
			   }
			}
			logger.log(Level.SEVERE,"process config  SMS Flag =="+processconfigflag);
			
			if(processName!=null){
				logger.log(Level.SEVERE,"In the prossName");
			if(processconfigflag){
							
				logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
				
				
	/********************************Reading SalesQuotationList from SalesQuotation entity  ***********************/
				
				/**
				 * Date 13-03-2018 BY KOMAL
				 * change date filter from quotationDate to followUpDate
				 */
				
				List<SalesQuotation> salesQuotationEntity = ofy().load().type(SalesQuotation.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("followUpDate <=",dateForFilter).filter("status IN",obj).list();
				
				logger.log(Level.SEVERE,"Today date Is:"+dateForFilter);	

				logger.log(Level.SEVERE,"SalesQuotation entity size:"+salesQuotationEntity.size());	
				
				
				// email id is added to emailList
				ArrayList<String> toEmailList=new ArrayList<String>();
				toEmailList.add(compEntity.get(i).getPocEmail());
				
				 String mailTitl = "Sales Quotation Due As On Date";
				
				if(salesQuotationEntity.size()>0){
				
			
				ArrayList<String> tbl_header = new ArrayList<String>();
				tbl_header.add("Serial No");
				tbl_header.add("Branch");
				tbl_header.add("Customer Id");
				tbl_header.add("Customer Name");
				tbl_header.add("Customer Cont No");
				tbl_header.add("POC Name");
				tbl_header.add("Email Id");
				tbl_header.add("Sales Person");
				tbl_header.add("Valid Until");
				tbl_header.add("Priority");
				tbl_header.add("Category");
				tbl_header.add("Type");
				tbl_header.add("Group");
				tbl_header.add("Net Value");
				tbl_header.add("Quotation Id");
				tbl_header.add("Quotation Date");
				tbl_header.add("Status");
				tbl_header.add("Ageing");
				
/********************************Sorting table with Branch & Sales QuotationDate Date ***********************/
			
				
				Comparator<SalesQuotation> quotationDateComparator2 = new Comparator<SalesQuotation>() {
					public int compare(SalesQuotation s1, SalesQuotation s2) {
					
					Date date1 = s1.getQuotationDate();
					Date date2 = s2.getQuotationDate();
					
					//ascending order
					return date1.compareTo(date2);
					}
					};
					Collections.sort(salesQuotationEntity, quotationDateComparator2);
					
					Comparator<SalesQuotation> quotationDateComparator = new Comparator<SalesQuotation>() {
						public int compare(SalesQuotation s1, SalesQuotation s2) {
						
						String branch1 = s1.getBranch();
						String branch2 = s2.getBranch();
						
						//ascending order
						return branch1.compareTo(branch2);
						}
						
						};
						Collections.sort(salesQuotationEntity, quotationDateComparator);
					
/********************************Getting salesQuotationEntity data and adding in the tbl1 List ***********************/
							
				ArrayList<String> tbl1=new ArrayList<String>();
					
				for(int j=0;j<salesQuotationEntity.size();j++){
					
					Customer customerEntity=ofy().load().type(Customer.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("count", salesQuotationEntity.get(j).getCinfo().getCount()).first().now();
					
					tbl1.add((j+1)+"");
		   	 		tbl1.add(salesQuotationEntity.get(j).getBranch()+"");
		   	 		tbl1.add(salesQuotationEntity.get(j).getCinfo().getCount()+"");
		   	 		tbl1.add(salesQuotationEntity.get(j).getCinfo().getFullName()+"");		   	 		
		   	 		tbl1.add(salesQuotationEntity.get(j).getCinfo().getCellNumber()+"");
		   	 		tbl1.add(salesQuotationEntity.get(j).getCinfo().getPocName());
		   	 		tbl1.add(customerEntity.getEmail());
		   	 		tbl1.add(salesQuotationEntity.get(j).getEmployee());
			   	 	tbl1.add(fmt.format(salesQuotationEntity.get(j).getValidUntill())+"");
		   	 		tbl1.add(salesQuotationEntity.get(j).getPriority()+"");
		   	 		tbl1.add(salesQuotationEntity.get(j).getCategory()+"");
		   	 		tbl1.add(salesQuotationEntity.get(j).getType()+"");
		   	 		tbl1.add(salesQuotationEntity.get(j).getGroup()+"");
		   	 		tbl1.add(salesQuotationEntity.get(j).getNetpayable()+"");
		   	 		tbl1.add(salesQuotationEntity.get(j).getCount() +"");
		   	 	    tbl1.add(fmt.format(salesQuotationEntity.get(j).getQuotationDate()) +"");
		 
		   	 	    /**************** for getting ageing for each Sales Quotation ******/
		   	 	    String StringQuotationDate = fmtQuotationDate.format(salesQuotationEntity.get(j).getQuotationDate());
		   	 	    
		   			
		   				Date d1 = null;
		   				Date d2 = null;
		   				
		   				d1 = df.parse(StringQuotationDate);
		   				d2 = df.parse(todayDateString);
		   				long diff = d2.getTime() - d1.getTime();
						long diffDays = diff / (24 * 60 * 60 * 1000);
						
		   	 	    tbl1.add(salesQuotationEntity.get(j).getStatus()+"");
		   	 	    tbl1.add(diffDays +"");
		   	 	}			
				
				logger.log(Level.SEVERE,"In the ProsConfig !=null   3 ");
				
   /********************************Calling cronsendEmail Method ***********************/
				
				try {   
					   
						System.out.println("Before send mail method");
						logger.log(Level.SEVERE,"Before send method call to send mail  ");						
						cronEmail.cronSendEmail(toEmailList, "Quotation Due As On Date"+" "+ todayDateString, mailTitl +"  "+ todayDateString, c, tbl_header, tbl1, null, null, null, null);
						logger.log(Level.SEVERE,"After send mail method ");		
								//+"  "+
				} catch (IOException e1) {
					
					logger.log(Level.SEVERE,"In the catch block ");
					e1.printStackTrace();
			}
				
			}
			else{					// else block for if(serviceEntity.size()>0){ 		
					
				System.out.println("Sorry no Quotation found for this company");
				logger.log(Level.SEVERE,"Sorry no Quotation found for this company");
//				
				mailTitl = "No Quotation Found Due";

				cronEmail.cronSendEmail(toEmailList, "Sales Quotation Due As On Date"+" "+ todayDateString, mailTitl , c, null, null, null, null, null, null);

			}
				
				
			}
			else{					//else block for prosconfig
					logger.log(Level.SEVERE,"ProcessConfiguration is null");
					System.out.println("ProcessConfiguration is null");
			}
			
			}else{        //else block for pross!=null
				logger.log(Level.SEVERE,"Cron job status is Inactive");
				System.out.println("Cron job status is Inactive");
			}
			
							
		}	//end of for loop
		
	}
	else{			//else block for if(compEntity.size()>0)
		
		logger.log(Level.SEVERE,"No Company found from cron job completed");	
		System.out.println("No Company found from cron job completed");
	}
	
	
	 }catch(Exception e2){
	
	e2.printStackTrace();
	 }
		
			
			
	
			
		}
	
public void SalesQuotationList(String cronList) {

			
			

			logger.log(Level.SEVERE, "Cron list str --" + cronList);
			Gson gson = new Gson();
			JSONArray jsonarr = null;
			try {
				jsonarr=new JSONArray(cronList.trim());
			} catch (JSONException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			
			try {
				
				
				
				CronJobConfigrationDetails cronDetails =null;
				
				List<Company> compEntity = ofy().load().type(Company.class).list();
				if(compEntity.size()>0){
				
					logger.log(Level.SEVERE,"If compEntity size > 0");
					logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
					

					for(int i=0;i<compEntity.size();i++){
						
						Company c=compEntity.get(i);
						
						for(int k=0;k<jsonarr.length();k++){
							
							JSONObject jsonObj = jsonarr.getJSONObject(k);
							cronDetails = new CronJobConfigrationDetails();
							
							cronDetails = gson.fromJson(jsonObj.toString(), CronJobConfigrationDetails.class);
							

							Email cronEmail = new Email();
							
							Date today=DateUtility.getDateWithTimeZone("IST", new Date());
							
							TimeZone.setDefault(TimeZone.getTimeZone("IST"));
							fmt.setTimeZone(TimeZone.getTimeZone("IST"));
							fmtQuotationDate.setTimeZone(TimeZone.getTimeZone("IST"));
							
							/**
							 *    i have added time in today's date
							 */
							
							logger.log(Level.SEVERE,"Date Before Adding "+today);
							Calendar cal=Calendar.getInstance();
							cal.setTime(today);
							cal.add(Calendar.DATE, 0);
							
							Date dateForFilter=null;
							
//							try {
//								dateForFilter=fmtQuotationDate.parse(fmtQuotationDate.format(cal.getTime()));
//								cal.set(Calendar.HOUR_OF_DAY,23);
//								cal.set(Calendar.MINUTE,59);
//								cal.set(Calendar.SECOND,59);
//								cal.set(Calendar.MILLISECOND,999);
//								dateForFilter=cal.getTime();
//							} catch (ParseException e) {
//								e.printStackTrace();
//							}
//							
//							logger.log(Level.SEVERE,"Date After setting the Time"+dateForFilter);
							
							
							try {
								dateForFilter=fmtQuotationDate.parse(fmtQuotationDate.format(cal.getTime()));
								cal.set(Calendar.HOUR_OF_DAY,0);
								cal.set(Calendar.MINUTE,0);
								cal.set(Calendar.SECOND,0);
								cal.set(Calendar.MILLISECOND,0);
								dateForFilter=cal.getTime();
							} catch (ParseException e) {
								e.printStackTrace();
							}
							
							
							
							logger.log(Level.SEVERE,"Date After setting the Time"+dateForFilter);
							
							cal.setTime(today);
							int diffDay = 0;
							 if(cronDetails.getOverdueDays()>0 && cronDetails.isOverdueStatus()){
								 diffDay = -cronDetails.getOverdueDays();
							 }else{
								 diffDay = cronDetails.getInterval();
							 }
							
							cal.add(Calendar.DATE, diffDay);
							
							Date duedateForFilter=null;
							
							try {
								duedateForFilter=fmtQuotationDate.parse(fmtQuotationDate.format(cal.getTime()));
								cal.set(Calendar.HOUR_OF_DAY,23);
								cal.set(Calendar.MINUTE,59);
								cal.set(Calendar.SECOND,59);
								cal.set(Calendar.MILLISECOND,999);
								duedateForFilter=cal.getTime();
							} catch (ParseException e) {
								e.printStackTrace();
							}
							
							
							logger.log(Level.SEVERE,"Date After setting time"+duedateForFilter);
							
							/************************************* End *********************************/
							
							
					 try{
						 
						 logger.log(Level.SEVERE,"In Sales Quotation List");	

						 
					/********************************Adding status in the list ***********************/
						 
						 ArrayList<String> obj = new ArrayList<String>();
						 obj.add("Created");
						 obj.add("Requested");
						 obj.add("Approved");

						 
					
					/******************************Converting todayDate to String format*****************/
						 
						 Date todaysDate = new Date();
						 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
						 String todayDateString = df.format(todaysDate);
						 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);

						 
				 /********************************Adding Companies in the list ***********************/
						 
//					List<Company> compEntity = ofy().load().type(Company.class).list();
//					if(compEntity.size()>0)
					{
					
						logger.log(Level.SEVERE,"If compEntity size > 0");
						logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
						

//						for(int i=0;i<compEntity.size();i++)
						{
							
//							Company c=compEntity.get(i);
							logger.log(Level.SEVERE,"In the for loop");
							logger.log(Level.SEVERE,"Company Name="+c);
							logger.log(Level.SEVERE,"The value of i is:" +i);
							logger.log(Level.SEVERE,"Date After Adding One Date"+dateForFilter);

							
				 /********************************Checking prosname & prosconfig for each company ***********************/
						
//							ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
//							ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
//							
//							boolean processconfigflag = false;
//							if(processconfig!=null){
//								if(processconfig.isConfigStatus()){
//									for(int l=0;l<processconfig.getProcessList().size();l++){
//										if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("SalesQuotationDailyEmail") && processconfig.getProcessList().get(l).isStatus()==true){
//											processconfigflag=true;
//										}
//									}
//							   }
//							}
//							logger.log(Level.SEVERE,"process config  SMS Flag =="+processconfigflag);
//							
//							if(processName!=null){
//								logger.log(Level.SEVERE,"In the prossName");
//							if(processconfigflag)
							
							
							
							List<Employee>  employeeList=new ArrayList<Employee>();
							employeeList=ofy().load().type(Employee.class).filter("companyId", c.getCompanyId()).filter("roleName", cronDetails.getEmployeeRole()).list();
							logger.log(Level.SEVERE,""+cronDetails.getEmployeeRole()+" "+"Employee List size::: "+employeeList.size());
							
								for (Employee employee : employeeList) {
									ArrayList<String> toEmailListDt=new ArrayList<String>();
									toEmailListDt.add(employee.getEmail().trim());
									List<String> branchList=new ArrayList<String>();
									for (int j = 0; j < employee.getEmpBranchList().size(); j++) {
										branchList.add(employee.getEmpBranchList().get(j).getBranchName());
										logger.log(Level.SEVERE, "  -- branch list --"+ employee.getEmpBranchList().get(j).getBranchName());
									}
									logger.log(Level.SEVERE,"toEmailList size::: "+toEmailListDt.size()+" Branch List SIze : "+branchList.size());
									
									if(branchList.size()!=0){
										{
											
											/********************************Reading SalesQuotationList from SalesQuotation entity  ***********************/
											List<SalesQuotation> salesQuotationEntity = new ArrayList<SalesQuotation>();
											
											if(cronDetails.isOverdueStatus()){
												cal.set(Calendar.HOUR_OF_DAY,23);
												cal.set(Calendar.MINUTE,59);
												cal.set(Calendar.SECOND,59);
												cal.set(Calendar.MILLISECOND,999);
												dateForFilter=cal.getTime();
												 salesQuotationEntity = ofy().load().type(SalesQuotation.class)
														 .filter("companyId",compEntity.get(i).getCompanyId()).filter("branch IN", branchList)
														 .filter("quotationDate <=",duedateForFilter).filter("status IN",obj).list();
												 
											}else{
												
												 salesQuotationEntity = ofy().load().type(SalesQuotation.class)
														 .filter("companyId",compEntity.get(i).getCompanyId())
														 .filter("quotationDate <=",dateForFilter).filter("branch IN", branchList)
														 .filter("quotationDate >=",dateForFilter).filter("status IN",obj).list();
											}
											
											
											
														
														logger.log(Level.SEVERE,"Today date Is:"+dateForFilter);	

														logger.log(Level.SEVERE,"SalesQuotation entity size:"+salesQuotationEntity.size());	
														
														
														// email id is added to emailList
														ArrayList<String> toEmailList=new ArrayList<String>();
														toEmailList.add(employee.getEmail());
														
														 String mailTitl = "Sales Quotation Due As On Date";
														
														if(salesQuotationEntity.size()>0){
														
													
														ArrayList<String> tbl_header = new ArrayList<String>();
														tbl_header.add("Serial No");
														tbl_header.add("Branch");
														tbl_header.add("Customer Id");
														tbl_header.add("Customer Name");
														tbl_header.add("Customer Cont No");
														tbl_header.add("POC Name");
														tbl_header.add("Email Id");
														tbl_header.add("Sales Person");
														tbl_header.add("Valid Until");
														tbl_header.add("Priority");
														tbl_header.add("Category");
														tbl_header.add("Type");
														tbl_header.add("Group");
														tbl_header.add("Net Value");
														tbl_header.add("Quotation Id");
														tbl_header.add("Quotation Date");
														tbl_header.add("Status");
														tbl_header.add("Ageing");
														
										/********************************Sorting table with Branch & Sales QuotationDate Date ***********************/
													
														
														Comparator<SalesQuotation> quotationDateComparator2 = new Comparator<SalesQuotation>() {
															public int compare(SalesQuotation s1, SalesQuotation s2) {
															
															Date date1 = s1.getQuotationDate();
															Date date2 = s2.getQuotationDate();
															
															//ascending order
															return date1.compareTo(date2);
															}
															};
															Collections.sort(salesQuotationEntity, quotationDateComparator2);
															
															Comparator<SalesQuotation> quotationDateComparator = new Comparator<SalesQuotation>() {
																public int compare(SalesQuotation s1, SalesQuotation s2) {
																
																String branch1 = s1.getBranch();
																String branch2 = s2.getBranch();
																
																//ascending order
																return branch1.compareTo(branch2);
																}
																
																};
																Collections.sort(salesQuotationEntity, quotationDateComparator);
															
										/********************************Getting salesQuotationEntity data and adding in the tbl1 List ***********************/
																	
														ArrayList<String> tbl1=new ArrayList<String>();
															
														for(int j=0;j<salesQuotationEntity.size();j++){
															
															Customer customerEntity=ofy().load().type(Customer.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("count", salesQuotationEntity.get(j).getCinfo().getCount()).first().now();
															
															tbl1.add((j+1)+"");
												   	 		tbl1.add(salesQuotationEntity.get(j).getBranch()+"");
												   	 		tbl1.add(salesQuotationEntity.get(j).getCinfo().getCount()+"");
												   	 		tbl1.add(salesQuotationEntity.get(j).getCinfo().getFullName()+"");		   	 		
												   	 		tbl1.add(salesQuotationEntity.get(j).getCinfo().getCellNumber()+"");
												   	 		tbl1.add(salesQuotationEntity.get(j).getCinfo().getPocName());
												   	 		tbl1.add(customerEntity.getEmail());
												   	 		tbl1.add(salesQuotationEntity.get(j).getEmployee());
													   	 	tbl1.add(fmt.format(salesQuotationEntity.get(j).getValidUntill())+"");
												   	 		tbl1.add(salesQuotationEntity.get(j).getPriority()+"");
												   	 		tbl1.add(salesQuotationEntity.get(j).getCategory()+"");
												   	 		tbl1.add(salesQuotationEntity.get(j).getType()+"");
												   	 		tbl1.add(salesQuotationEntity.get(j).getGroup()+"");
												   	 		tbl1.add(salesQuotationEntity.get(j).getNetpayable()+"");
												   	 		tbl1.add(salesQuotationEntity.get(j).getCount() +"");
												   	 	    tbl1.add(fmt.format(salesQuotationEntity.get(j).getQuotationDate()) +"");
												 
												   	 	    /**************** for getting ageing for each Sales Quotation ******/
												   	 	    String StringQuotationDate = fmtQuotationDate.format(salesQuotationEntity.get(j).getQuotationDate());
												   	 	    
												   			
												   				Date d1 = null;
												   				Date d2 = null;
												   				
												   				d1 = df.parse(StringQuotationDate);
												   				d2 = df.parse(todayDateString);
												   				long diff = d2.getTime() - d1.getTime();
																long diffDays = diff / (24 * 60 * 60 * 1000);
																
												   	 	    tbl1.add(salesQuotationEntity.get(j).getStatus()+"");
												   	 	    tbl1.add(diffDays +"");
												   	 	}			
														
														logger.log(Level.SEVERE,"In the ProsConfig !=null   3 ");
														
										   /********************************Calling cronsendEmail Method ***********************/
														
														try {   
															   
																System.out.println("Before send mail method");
																logger.log(Level.SEVERE,"Before send method call to send mail  ");						
																
//																cronEmail.cronSendEmail(toEmailList, "Quotation Due As On Date"+" "+ todayDateString, mailTitl +"  "+ todayDateString, c, tbl_header, tbl1, null, null, null, null);
																
																String footer = "";
																
																if(cronDetails.getFooter().trim() != "" ){
																	footer = cronDetails.getFooter();
																}
																
																
																String emailSubject =  "Quotation Due As On Date"+" "+ todayDateString;
																
																if(cronDetails.getSubject().trim() != "" ){
																	emailSubject = cronDetails.getSubject() + "  As On Date"+" "+ todayDateString;
																}
																
																String emailMailBody = mailTitl;
																
																if(cronDetails.getMailBody().trim() != "" ){
																	emailMailBody = cronDetails.getMailBody();
																}
																
																	System.out.println("Before send mail method");
																	logger.log(Level.SEVERE,"Before send method call to send mail  ");						
																	
																	cronEmail.cronSendEmail(toEmailList, emailSubject, emailMailBody, c, tbl_header, tbl1,
																			null, null, null, null, "", footer);
																	logger.log(Level.SEVERE,"After send mail method ");		
																
														
																logger.log(Level.SEVERE,"After send mail method ");		
																		//+"  "+
														} catch (IOException e1) {
															
															logger.log(Level.SEVERE,"In the catch block ");
															e1.printStackTrace();
													}
														
													}
													else{					// else block for if(serviceEntity.size()>0){ 		
															
														System.out.println("Sorry no Quotation found for this company");
														logger.log(Level.SEVERE,"Sorry no Quotation found for this company");
//														
														mailTitl = "No Quotation Found Due";

														cronEmail.cronSendEmail(toEmailList, "Sales Quotation Due As On Date"+" "+ todayDateString, mailTitl , c, null, null, null, null, null, null);

													}
														
														
													}
									}
								}
							
							
//							else{					//else block for prosconfig
//									logger.log(Level.SEVERE,"ProcessConfiguration is null");
//									System.out.println("ProcessConfiguration is null");
//							}
//							
//							}else{        //else block for pross!=null
//								logger.log(Level.SEVERE,"Cron job status is Inactive");
//								System.out.println("Cron job status is Inactive");
//							}
							
											
						}	//end of for loop
						
					}
//					else{			//else block for if(compEntity.size()>0)
//						
//						logger.log(Level.SEVERE,"No Company found from cron job completed");	
//						System.out.println("No Company found from cron job completed");
//					}
					
					
					 }catch(Exception e2){
					
					e2.printStackTrace();
					 }
						
							
							
							
							
						}
					}
				}
				
				
			}catch(Exception e){
				logger.log(Level.SEVERE,"sales quotation p-- "+ e);
			}
			
			
	
			
		}
	

}
