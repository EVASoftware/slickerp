package com.slicktechnologies.server.cronjobimpl;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class LeadCronJobImpl extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6651714220402175411L;

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmtLeadDate = new SimpleDateFormat("dd/MM/yyyy");

	
	 Logger logger = Logger.getLogger("NameOfYourLogger");
	  
		public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws IOException {
			
//				Leadlist();
		}

		private void Leadlist() {
			Email cronEmail = new Email();
			
			
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			
			fmtLeadDate.setTimeZone(TimeZone.getTimeZone("IST"));
			
	 try{
		 
		 logger.log(Level.SEVERE,"In Lead List");	
		 
	
	/******************************Converting todayDate to String format*****************/
		 
		 Date todaysDate = new Date();
		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 String todayDateString = df.format(todaysDate);
		 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);
		 
	/********************************Adding Companies in the list ***********************/
		 
	List<Company> compEntity = ofy().load().type(Company.class).list();
	if(compEntity.size()>0){

		logger.log(Level.SEVERE,"If compEntity size > 0");
		logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());

		for(int i=0;i<compEntity.size();i++){
			
			Company c=compEntity.get(i);
			logger.log(Level.SEVERE,"Company Name="+c);
			
	/********************************Checking prosname & prosconfig for each company ***********************/

			ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
			ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
			
			boolean processconfigflag = false;
			if(processconfig!=null){
				if(processconfig.isConfigStatus()){
					for(int l=0;l<processconfig.getProcessList().size();l++){
						if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("LeadDailyEmail") && processconfig.getProcessList().get(l).isStatus()==true){
							processconfigflag=true;
						}
					}
			   }
			}
			logger.log(Level.SEVERE,"process config  SMS Flag =="+processconfigflag);
			
			if(processName!=null){
				logger.log(Level.SEVERE,"In the prossName");
			if(processconfigflag){
				logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
				
	/********************************Reading LeadList from Lead entity  ***********************/
				
				/**
				 * Date 08-11-2017 BY ANIL
				 * change date filter from creationDate to followUpDate
				 */
				/**
				 * Date 5-01-2017 By Vijay
				 * as per nitin sir instrcution all open lead without status successful unsuccessful and cancelled status filter
				 */
				
				List<Config> leadstatuslist = ofy().load().type(Config.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("type", 6).list();
				
				/********************************Adding status in the list ***********************/
				 
				 ArrayList<String> leadstatus = new ArrayList<String>();
				 for(int p=0;p<leadstatuslist.size();p++){
					 if(!leadstatuslist.get(p).getName().equalsIgnoreCase("unsuccessful") &&
						!leadstatuslist.get(p).getName().equalsIgnoreCase("successful") &&
						!leadstatuslist.get(p).getName().equalsIgnoreCase("cancelled")){
						 leadstatus.add(leadstatuslist.get(p).getName());
					 }
				 }

				 logger.log(Level.SEVERE,"leadstatus:"+leadstatus.size());

				List<Lead> leadEntity = ofy().load().type(Lead.class).filter("companyId",compEntity.get(i).getCompanyId()).filter("status IN",leadstatus).list();
				
				logger.log(Level.SEVERE,"LeadEntity:"+leadEntity.size());

				
				// email id is added to emailList
				ArrayList<String> toEmailList=new ArrayList<String>();
				toEmailList.add(compEntity.get(i).getPocEmail());
				
				 String mailTitl = "Lead Due As On Date";
				

				if(leadEntity.size()>0){
				

				ArrayList<String> tbl_header = new ArrayList<String>();
				
				
	/********************************Sorting table with Branch & Lead Date ***********************/
			
				logger.log(Level.SEVERE,"44444444444");

				Comparator<Lead> leadDateComparator2 = new Comparator<Lead>() {
					public int compare(Lead s1, Lead s2) {
					
					Date date1 = s1.getFollowUpDate();
					Date date2 = s2.getFollowUpDate();
					
					//ascending order
					return date1.compareTo(date2);
					}
					};
					Collections.sort(leadEntity, leadDateComparator2);

					Comparator<Lead> leadDateComparator = new Comparator<Lead>() {
						public int compare(Lead s1, Lead s2) {
						
						String branch1 = s1.getBranch();
						String branch2 = s2.getBranch();
						
						//ascending order
						return branch1.compareTo(branch2);
						}
						
						};
						Collections.sort(leadEntity, leadDateComparator);
					
	/********************************Getting LeadEntity data and adding in the tbl1 List ***********************/
						tbl_header.add("Serial No");
						tbl_header.add("Branch");
						tbl_header.add("Customer Id");
						tbl_header.add("Customer Name");
						tbl_header.add("Customer Cell No");
						tbl_header.add("POC Name");
						tbl_header.add("Email Id");
						tbl_header.add("Sales Person");
						tbl_header.add("Lead Id");
						tbl_header.add("Lead Title");
						tbl_header.add("FollowUp Date");
						tbl_header.add("Category");
						tbl_header.add("Type");
						tbl_header.add("Group");
						tbl_header.add("Status");
						tbl_header.add("Ageing");
						tbl_header.add("Communication Log");
				ArrayList<String> tbl1=new ArrayList<String>();

				for(int j=0;j<leadEntity.size();j++){
					
					try{

					tbl1.add((j+1)+"");
		   	 		tbl1.add(leadEntity.get(j).getBranch()+"");
		   	 		tbl1.add(leadEntity.get(j).getPersonInfo().getCount()+"");
		   	 		tbl1.add(leadEntity.get(j).getPersonInfo().getFullName()+"");
		   	 		tbl1.add(leadEntity.get(j).getPersonInfo().getCellNumber()+"");
		   	 		tbl1.add(leadEntity.get(j).getPersonInfo().getPocName());
		   	 		tbl1.add(leadEntity.get(j).getPersonInfo().getEmail());
		   	 		tbl1.add(leadEntity.get(j).getEmployee());
		   	 		tbl1.add(leadEntity.get(j).getCount()+"");
		   	 		tbl1.add(leadEntity.get(j).getTitle()+"");
		   	 		tbl1.add(fmt.format(leadEntity.get(j).getFollowUpDate()) +"");
		   	 		tbl1.add(leadEntity.get(j).getCategory()+"");
		   	 		tbl1.add(leadEntity.get(j).getType()+"");
		   	 		tbl1.add(leadEntity.get(j).getGroup()+"");
		   	 			
		   	 	    /**************** for getting ageing for each Lead ******/
		   	 	String StringLeadDate = fmtLeadDate.format(leadEntity.get(j).getFollowUpDate());
		   	 	logger.log(Level.SEVERE,"In the for loop j value 3.4:"+j);

		   				Date d1 = null;
		   				Date d2 = null;
		   				
		   				d1 = df.parse(StringLeadDate);
		   				d2 = df.parse(todayDateString);
		   				long diff;
		   				long diffDays;
		   				
				   	 	logger.log(Level.SEVERE,"getFollowUpDate:"+d1);

		   					diff = d2.getTime() - d1.getTime();
		   					diffDays=diff / (24 * 60 * 60 * 1000);
						
		   	 	    tbl1.add(leadEntity.get(j).getStatus()+"");
		   	 	    
		   	 	    	tbl1.add(diffDays +"");
				   	 	logger.log(Level.SEVERE,"diffDays:" +diffDays);

				   	 /**
				   	  * Date 06-01-2017 By Vijay
				   	  * for getting lead latest interaction 	
				   	  */
				   	 
				   	 List<InteractionType> interactionlist = ofy().load().type(InteractionType.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("interactionDocumentId", leadEntity.get(j).getCount())
				   	 .filter("interactionModuleName", "Service").filter("interactionDocumentName", "Lead").list();
				   
				   	 logger.log(Level.SEVERE,"interactionlist size :" +interactionlist.size());
				   	
				   	 if(interactionlist.size()!=0){
				   		 
				   		 Comparator<InteractionType> interaction = new Comparator<InteractionType>() {
							
							@Override
							public int compare(InteractionType I1, InteractionType I2) {
								// TODO Auto-generated method stub
								Integer count = I1.getCount();
								Integer count2 = I2.getCount();
									
								return count.compareTo(count2) ;
							}
						};
						Collections.sort(interactionlist,interaction);
						
		   	 	    for(int k=0;k<interactionlist.size();k++){
				   		 if(k==0){
					   			tbl1.add(interactionlist.get(k).getInteractionPurpose()+" Date:"+fmt.format(interactionlist.get(k).getinteractionCreationDate())+" Time: "+interactionlist.get(k).getInteractionTime());
				   			break;
				   		 }
				   	 }
		   	 	    }else{
		   	 	    	tbl1.add(" ");
		   	 	    }
				   	 /**
				   	  * ends here
				   	  */
				   	 
				   	 
					}catch(Exception e)
					{
						e.printStackTrace();
					}
		   	 	    
			   	 	logger.log(Level.SEVERE,"**************************** :");

		   	 	}			
				
				logger.log(Level.SEVERE,"In the ProsConfig !=null   3 ");
				
	/********************************Calling cronsendEmail Method ***********************/
				
				try {   
					   
						System.out.println("Before send mail method");
						logger.log(Level.SEVERE,"Before send method call to send mail  ");						
						cronEmail.cronSendEmail(toEmailList, "Lead Due As On Date"+" "+ todayDateString, mailTitl +"  "+ todayDateString, c, tbl_header, tbl1, null, null, null, null);
						logger.log(Level.SEVERE,"After send mail method ");		
								//+"  "+
				} catch (IOException e1) {
					
					logger.log(Level.SEVERE,"In the catch block ");
					e1.printStackTrace();
			}
				
			}
			else{					
					
				System.out.println("Sorry no Lead found for this company");
				logger.log(Level.SEVERE,"Sorry no Lead found for this company");
//				
				mailTitl = "No Lead Found Due";

				cronEmail.cronSendEmail(toEmailList, "Lead Due As On Date"+" "+ todayDateString, mailTitl , c, null, null, null, null, null, null);

			}
				
				
			}
			else{					//else block for prosconfig
					logger.log(Level.SEVERE,"ProcessConfiguration is null");
					System.out.println("ProcessConfiguration is null");
			}
			
			}else{        //else block for pross!=null
				logger.log(Level.SEVERE,"Cron job status is Inactive");
				System.out.println("Cron job status is Inactive");
			}
			
							
		}	//end of for loop
		
	}
	else{			//else block for if(compEntity.size()>0)
		
		logger.log(Level.SEVERE,"No Company found from cron job completed");	
		System.out.println("No Company found from cron job completed");
	}


	 }catch(Exception e2){

		 	e2.printStackTrace();
	 }
		
			
	}	
		
		public void getLeadReport(String contractDetail){
			Gson gson = new Gson();
			JSONArray jsonarr = null;
			Long companyId;
			try {
				jsonarr=new JSONArray(contractDetail.trim());
			} catch (JSONException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			
			try {
				
				
				
				List<Company> compEntity = ofy().load().type(Company.class).list();
				if(compEntity.size()>0){

					logger.log(Level.SEVERE,"If compEntity size > 0");
					logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());

					for(int jk=0;jk<compEntity.size();jk++){
						
						
						
//						Company c=compEntity.get(jk);
						Company company=compEntity.get(jk);
//						Company company=ofy().load().type(Company.class).filter("accessUrl","ganesh").first().now();
						companyId=company.getCompanyId();
						
						List<Config> leadstatuslist = ofy().load().type(Config.class).filter("companyId",companyId).filter("type", 6).list();
						
						/********************************Adding status in the list ***********************/
						 
						 ArrayList<String> leadstatus = new ArrayList<String>();
						 for(int p=0;p<leadstatuslist.size();p++){
							 if(!leadstatuslist.get(p).getName().equalsIgnoreCase("unsuccessful") &&
								!leadstatuslist.get(p).getName().equalsIgnoreCase("successful") &&
								!leadstatuslist.get(p).getName().equalsIgnoreCase("cancelled")){
								 leadstatus.add(leadstatuslist.get(p).getName());
							 }
						 }

						
						
						CronJobConfigrationDetails cronDetails =null;
						for(int jj=0;jj<jsonarr.length();jj++){
							JSONObject jsonObj = jsonarr.getJSONObject(jj);
							cronDetails = new CronJobConfigrationDetails();
							
							cronDetails = gson.fromJson(jsonObj.toString(), CronJobConfigrationDetails.class);
							
							
							SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
							sdf.setTimeZone(TimeZone.getTimeZone("IST"));
							TimeZone.setDefault(TimeZone.getTimeZone("IST"));
							
							 Date d=new Date();
							 String dateString = sdf.format(d);
							int diff1 = 0;
							 if(cronDetails.isOverdueStatus() && cronDetails.getOverdueDays()>0){
								 diff1 = cronDetails.getOverdueDays() * -1;
							 }else{
								 diff1 = cronDetails.getInterval();
							 }
							logger.log(Level.SEVERE,"get diff days for contract -- " + diff1);
							Calendar calendar=Calendar.getInstance();
							calendar.setTime(new Date());
							calendar.add(Calendar.DATE, diff1);
							
							Date previousDate = calendar.getTime();
							String todayDateinString=sdf.format(previousDate);
//							
							logger.log(Level.SEVERE,"Date ::"+todayDateinString);
						

							
							logger.log(Level.SEVERE,"Date Before Adding One Day"+previousDate);
							Calendar cal3=Calendar.getInstance();
							cal3.setTime(previousDate);
							
							
							Date plusDaysDate=null;
							
							try {
								plusDaysDate=fmt.parse(fmt.format(cal3.getTime()));
								cal3.set(Calendar.HOUR_OF_DAY,23);
								cal3.set(Calendar.MINUTE,59);
								cal3.set(Calendar.SECOND,59);
								cal3.set(Calendar.MILLISECOND,999);
								plusDaysDate=cal3.getTime();
							} catch (ParseException e) {
								e.printStackTrace();
							}
							
							
							Date minusDaysDate=null;
							
							try {
								calendar.setTime( new Date());
								minusDaysDate=sdf.parse(sdf.format(new Date()));
								calendar.set(Calendar.HOUR_OF_DAY,0);
								calendar.set(Calendar.MINUTE,0);
								calendar.set(Calendar.SECOND,0);
								calendar.set(Calendar.MILLISECOND,0);
								minusDaysDate = calendar.getTime();
							} catch (ParseException e) {
								e.printStackTrace();
							}
							
							{
								logger.log(Level.SEVERE,"get lead status --" + leadstatus.toString());
								
								logger.log(Level.SEVERE," minum date -- " + minusDaysDate + " max days --"+plusDaysDate);
								
								{
									/**
									 * @author Anil , Date : 25-04-2019
									 * while loading employee added filter ,its status should  be active 
									 * Because mails were triggered on in active employee ids also
									 */
									List<Employee>  employeeList=new ArrayList<Employee>();
									employeeList=ofy().load().type(Employee.class).filter("companyId", companyId).filter("roleName", cronDetails.getEmployeeRole()).filter("status", true).list();
									logger.log(Level.SEVERE,""+cronDetails.getEmployeeRole()+" "+"Employee List size::: "+employeeList.size());
									
									for (Employee employee : employeeList) {
										ArrayList<String> toEmailList=new ArrayList<String>();
										toEmailList.add(employee.getEmail().trim());
										List<String> branchList=new ArrayList<String>();
										for (int i = 0; i < employee.getEmpBranchList().size(); i++) {
											branchList.add(employee.getEmpBranchList().get(i).getBranchName());
										}
										branchList.add(employee.getBranchName());
										logger.log(Level.SEVERE,"toEmailList size::: "+toEmailList.size()+" Branch List SIze : "+branchList.size());
										
										List<Lead> leadEntity = new ArrayList<Lead>();
										try {
											if(branchList.size()!=0){
												
												if(!cronDetails.isOverdueStatus()){
													leadEntity = ofy().load().type(Lead.class).filter("companyId", companyId)
															.filter("followUpDate >=", minusDaysDate).filter("followUpDate <=", plusDaysDate)
															.filter("branch IN",branchList).filter("status IN",leadstatus).list();
													
												}else{
													leadEntity = ofy().load().type(Lead.class).filter("companyId", companyId).filter("status IN",leadstatus)
															.filter("followUpDate <=", plusDaysDate).filter("branch IN",branchList).list();
													
												}
											}
										} catch (Exception e1) {
											e1.printStackTrace();
											logger.log(Level.SEVERE,"Error parsing date while querry");
										}
										logger.log(Level.SEVERE,"Contract List Size ::"+leadEntity.size());
										
										/**
										 * Email Header 
										 */
										ArrayList<String> tbl_header = new ArrayList<String>();
										/*tbl_header.add("Ser No.");
										tbl_header.add("Branch");
										tbl_header.add("Contract Id");
										tbl_header.add("Contract Date");
										tbl_header.add("Contract Start Date");
										tbl_header.add("Contract End Date");
										tbl_header.add("Customer Id");
										tbl_header.add("Customer Name");
										tbl_header.add("Contract Type");
										
										
										tbl_header.add("Sales Person");*//** Date : 24-10-2017 BY ANIL**//*
										tbl_header.add("Status");*/

										tbl_header.add("Serial No");
										tbl_header.add("Branch");
										tbl_header.add("Lead Id");
										tbl_header.add("Lead Title");
										tbl_header.add("FollowUp Date");
										tbl_header.add("Sales Person");
										tbl_header.add("Contrat Value");
										tbl_header.add("Customer Name");
										tbl_header.add("Customer Cell No");
										tbl_header.add("POC Name");
										tbl_header.add("Email Id");
										tbl_header.add("Category");
										tbl_header.add("Type");
										tbl_header.add("Status");
										tbl_header.add("Ageing");
										ArrayList<String> tbl1=new ArrayList<String>();
										
										for(int j=0;j<leadEntity.size();j++){
											
											try{
									   	 	logger.log(Level.SEVERE,"In the for loop j value 1:"+leadEntity.get(j).getCount() + " status --  " + leadEntity.get(j).getStatus());
									   	 	logger.log(Level.SEVERE,"In the for loop j value 2:"+j);

											tbl1.add((j+1)+"");
								   	 		tbl1.add(leadEntity.get(j).getBranch()+"");
								   	 		tbl1.add(leadEntity.get(j).getCount()+"");
								   	 		tbl1.add(leadEntity.get(j).getTitle());
								   	 		if(leadEntity.get(j).getFollowUpDate()!=null){
								   	 		tbl1.add(fmt.format(leadEntity.get(j).getFollowUpDate()) +"");
								   	 		}else{
								   	 			tbl1.add("");
								   	 		}
								   	 		tbl1.add(leadEntity.get(j).getEmployee());
								   	 		tbl1.add(leadEntity.get(j).getTotalAmount()+"");
								   	 		tbl1.add(leadEntity.get(j).getPersonInfo().getFullName()+"");
								   	 		tbl1.add(leadEntity.get(j).getPersonInfo().getCellNumber()+"");
								   	 		tbl1.add(leadEntity.get(j).getPersonInfo().getPocName());
								   	 		tbl1.add(leadEntity.get(j).getPersonInfo().getEmail());
								   	 		
								   	 		
								   	 		tbl1.add(leadEntity.get(j).getCategory()+"");
								   	 		tbl1.add(leadEntity.get(j).getType()+"");
								   	     tbl1.add(leadEntity.get(j).getStatus()+"");
								   	 	    /**************** for getting ageing for each Lead ******/
								   	 	String StringLeadDate = fmtLeadDate.format(leadEntity.get(j).getCreationDate());
								   	 	logger.log(Level.SEVERE,"In the for loop j value 3.4:"+j);

								   				Date d1 = null;
								   				Date d2 = null;
								   				
								   				d1 = sdf.parse(StringLeadDate);
								   				d2 = sdf.parse(fmtLeadDate.format(minusDaysDate));
								   				long diff;
								   				long diffDays;
								   				
										   	 	logger.log(Level.SEVERE,"CreationDate:"+d1);
										   	 	
										   	 	/**
										   	 	 * Date 10-08-2018 By Vijay
										   	 	 * Des :- Ageing must be positive or negative number 
										   	 	 * it should not  be NA so below code commneted
										   	 	 */

//								   				if(plusDaysDate.before(d1))
//								   				{
								   					diff = d2.getTime() - d1.getTime();
								   					diffDays=diff / (24 * 60 * 60 * 1000);
											   	 	logger.log(Level.SEVERE,"In the minusThirtyDaysDate.before(d1) :");

//								   				}
//								   				else{
//								   					diff=-1;
//								   					diffDays=-1;
//											   	 	logger.log(Level.SEVERE,"In Else block Of if ( minusThirtyDaysDate.before(d1) ):");
//
//								   				}
												
//								   	 	    if(diffDays==-1)
//								   	 	    {
//										   	 	logger.log(Level.SEVERE,"In the diffDays==-1 :");
//								   	 	    	tbl1.add("N.A");
//								   	 	    }
//								   	 	    else
//								   	 	    {
								   	 	    	tbl1.add(diffDays +"");
										   	 	logger.log(Level.SEVERE,"diffDays:" +diffDays);
//
//								   	 	    }
								   	 	    
											}catch(Exception e)
											{
												e.printStackTrace();
											}
								   	 	    
									   	 	logger.log(Level.SEVERE,"**************************** :");

								   	 	}	
										
										/**
										 * Email Parts ends here
										 */
										Email cronEmail = new Email();
										
										try {
											String footer = "";
											
											if(cronDetails.getFooter().trim()!= "" ){
												footer = cronDetails.getFooter();
											}
											
											
											String emailSubject = "Lead Report As On Date"+" "+	sdf.format(new Date());
											
											if(!cronDetails.getSubject().trim().equals("") && cronDetails.getSubject().trim().length() >0){
												emailSubject = cronDetails.getSubject();
											}
											
											String emailMailBody = "Lead Report As On Date" +"  "+ sdf.format(new Date());
											
											if(!cronDetails.getMailBody().trim().equals("") && cronDetails.getMailBody().trim().length() >0){
												emailMailBody = cronDetails.getMailBody() ;
											}
											
											if(leadEntity.size()!=0){
												logger.log(Level.SEVERE,"get emp to email --" + toEmailList);
												logger.log(Level.SEVERE,"get tbl1 size --" + tbl1.size());
												cronEmail.cronSendEmail(toEmailList, emailSubject, emailMailBody
														, company, tbl_header, tbl1,
														null, null, null, null,"",footer);
											}else{
												/**
												 * @author Anil,Date : 16-02-2019
												 * If no records found then mail body should be like no records found
												 */
												emailMailBody="No records found.";
												logger.log(Level.SEVERE,"get emp to email -- no data is there for " +emailSubject );
												
												cronEmail.cronSendEmail(toEmailList, emailSubject, emailMailBody , company, null, null, null, null, null, null);
											}
										}catch (IOException e) {
											e.printStackTrace();
											logger.log(Level.SEVERE,"Error in cronEmail method:::"+e);
//											return "Failed to send emails";
										}
									}
								}
								
								
								

							}
						}
					}
				}
				
				
			
			}catch(Exception e){
				logger.log(Level.SEVERE,"get error in  -- " +e);
			}
			
			
		}
}
