package com.slicktechnologies.server.cronjobimpl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

public class AcknowledgementCronJobImpl extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 167250899434130658L;


	Logger logger = Logger.getLogger("AcknowledgementCronJobImpl.class");
	long companyId=0;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		

		String urlcalled=req.getRequestURL().toString().trim();
		logger.log(Level.SEVERE,"urlcalled:::::"+urlcalled);
		
		String url1=urlcalled.replace("http://", "");
		logger.log(Level.SEVERE,"url1:::::"+url1);
		String[] splitUrl=url1.split("\\.");
		logger.log(Level.SEVERE,"splitUrl[0]:::::"+splitUrl[0]);
		
		Company company=ofy().load().type(Company.class).filter("accessUrl","my").first().now();
		companyId=company.getCompanyId();
		
		/**
		 * rahul added this code for process config 
		 */
		boolean flag= false;
		ProcessConfiguration process = ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId).
				filter("processName", "CronJob").first().now();
		
		for (int i = 0; i < process.getProcessList().size(); i++) {
			
			if(process.getProcessList().get(i).processType.equalsIgnoreCase("ACKINVOICE")){
				flag= true;
			}
		}
		/**
		 * ends here 
		 */
		
		
		if(flag){
		
		// TODO Auto-generated method stub
		final String ACK=AppConstants.NBHCACKNNOWLEDGEMENTINTEGRATE;
		String data="";
		
		URL url = null;
		try {
			url = new URL(ACK);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"MalformedURLException ERROR::::::::::"+e);
			e.printStackTrace();
		}
	    HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
			e.printStackTrace();
		}
	    con.setDoInput(true);
	    con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	    try {
			con.setRequestMethod("GET");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"ProtocolException ERROR::::::::::"+e);
			e.printStackTrace();
		}
	    InputStream is = null;
		try {
			is = con.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"IOException4 ERROR::::::::::"+e);
			e.printStackTrace();
		}
	    BufferedReader br = new BufferedReader(
	            new InputStreamReader(is));
	    String temp;

	    try {
			while ((temp = br.readLine()) != null) {
			    data = data + temp;
			}
			logger.log(Level.SEVERE,"data data::::::::::"+data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"IOException5 ERROR::::::::::"+e);
			e.printStackTrace();
		}
	    
	    logger.log(Level.SEVERE,"Data::::::::::"+data);
	    updateData(data);
	}
	}
	
	
	private void updateData(String data) {
		// TODO Auto-generated method stub
		//Update Accounting Interface and Invoice Table also
		JSONArray jArray = null;
		try {
			jArray = new JSONArray(data);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.log(Level.SEVERE,"parsing Error!!");
		}
		
		for (int i = 0; i < jArray.length(); i++) {
			JSONObject jObj = null;
			try {
				jObj = jArray.getJSONObject(i);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String status=jObj.optString("status");
			if(status.trim().equalsIgnoreCase("SYNCHED")){
				List<AccountingInterface> accountingInterfaceList=ofy().load().type(AccountingInterface.class).filter("companyId",companyId).filter("documentType","Invoice").filter("documentID",Integer.parseInt(jObj.optString("evaInvoiceId")) ).list();
				for (AccountingInterface accountingInterface : accountingInterfaceList) {
					if(!accountingInterface.getStatus().equals(AccountingInterface.CANCELLED)||!accountingInterface.getStatus().equals(AccountingInterface.TALLYSYNCED)){
						accountingInterface.setRemark(jObj.optString("remarks").trim());
						accountingInterface.setStatus(AccountingInterface.TALLYSYNCED);
					}
				}

				ofy().save().entities(accountingInterfaceList);
				Invoice invoice=ofy().load().type(Invoice.class).filter("companyId", companyId).filter("count", Integer.parseInt(jObj.optString("evaInvoiceId"))).first().now();
				/**
				 * Date 29-08-2019 by Vijay 
				 * Des :- NBHC CCPM getting null pointer exception at jObj.optString("sapInvoiceId").trim()
				 * so added if and else condition and try catch
				 */
				try {
				
				if(jObj.isNull("sapInvoiceId") || jObj.isNull("NetValue") || jObj.isNull("TaxAmount") || jObj.isNull("TotalAmount")){
					
				}
				else{
					invoice.setSapInvoiceId(jObj.optString("sapInvoiceId").trim());
					invoice.setSAPNetValue(Double.parseDouble(jObj.optString("NetValue").trim()));
					invoice.setSAPTaxAmount(Double.parseDouble(jObj.optString("TaxAmount").trim()));
					invoice.setSAPTotalAmount(Double.parseDouble(jObj.optString("TotalAmount").trim()));
					ofy().save().entity(invoice);
				}
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}else{
				List<AccountingInterface> accountingInterfaceList=ofy().load().type(AccountingInterface.class).filter("companyId",companyId).filter("documentType","Invoice").filter("documentID",Integer.parseInt(jObj.optString("evaInvoiceId")) ).list();
				for (AccountingInterface accountingInterface : accountingInterfaceList) {
					if(!accountingInterface.getStatus().equals(AccountingInterface.CANCELLED)||!accountingInterface.getStatus().equals(AccountingInterface.TALLYSYNCED)){
						accountingInterface.setRemark(jObj.optString("remarks").trim());
						accountingInterface.setStatus(AccountingInterface.FAILED);
					}
				}
				ofy().save().entities(accountingInterfaceList);
			}
		}
	}
	
}
