package com.slicktechnologies.server.cronjobimpl;
import static com.googlecode.objectify.ObjectifyService.ofy;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

public class DueReminderCronJobImpl extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2659932368951920849L;

	Logger logger = Logger.getLogger("DueReminderCronJobImpl.class");
	long companyId=0;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doGet(req, resp);
		
		String urlcalled=req.getRequestURL().toString().trim();
		logger.log(Level.SEVERE,"urlcalled:::::"+urlcalled);
		
		String url1=urlcalled.replace("http://", "");
		logger.log(Level.SEVERE,"url1:::::"+url1);
		String[] splitUrl=url1.split("\\.");
		logger.log(Level.SEVERE,"splitUrl[0]:::::"+splitUrl[0]);
		
		Company company=ofy().load().type(Company.class).filter("accessUrl","my").first().now();
		companyId=company.getCompanyId();
		

		/**
		 * rahul added this code for process config 
		 */
		boolean flag= false;
		ProcessConfiguration process = ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId).
				filter("processName", "CronJob").first().now();
		
		for (int i = 0; i < process.getProcessList().size(); i++) {
			
			if(process.getProcessList().get(i).processType.equalsIgnoreCase("DueReminderCronJob")){
				flag= true;
			}
		}
		/**
		 * ends here 
		 */
		String response="";
		if(flag){
			response=sendServiceDueReminderToEmployees(companyId,company);
		}
		
		if(response.equals("Successfull")){
			logger.log(Level.SEVERE,"Response::"+response);
		}else{
			logger.log(Level.SEVERE,"Response::"+response);
		}
	}

	private String sendServiceDueReminderToEmployees(long companyId, Company company) {
		// TODO Auto-generated method stub
		ArrayList<String> employeeRoleList=new ArrayList<String>();
		employeeRoleList.add("Branch Manager"); //72>Hrs>=48       3>Days>=2
		employeeRoleList.add("Zonal Head");//  168>Hrs>=72      7>Days>=3
		employeeRoleList.add("Business Head");//  >Hrs>=168			Days>=7
		employeeRoleList.add("Admin");//  >Hrs>=168			Days>=7
		
		logger.log(Level.SEVERE,"employeeRoleList size:::"+employeeRoleList.size());
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		
		for (String roleName : employeeRoleList) {
			int preNoOfDays=0,todayNoOfDays=0;
			if(roleName.equalsIgnoreCase("Branch Manager")){
				preNoOfDays=3;
				todayNoOfDays=2;
			}else if(roleName.equalsIgnoreCase("Zonal Head")){
				preNoOfDays=7;
				todayNoOfDays=3;
			}else  if(roleName.equalsIgnoreCase("Business Head")||roleName.equalsIgnoreCase("Admin")){
				preNoOfDays=10;
				todayNoOfDays=7;
			}
			Calendar calendar=Calendar.getInstance();
			calendar.setTime(new Date());
			calendar.add(Calendar.DATE, -preNoOfDays);
			Date afterDate = calendar.getTime();
			String previousDateinString=sdf.format(afterDate);
			
			Calendar calendarstart=Calendar.getInstance();
			calendarstart.setTime(new Date());
			calendarstart.add(Calendar.DATE, -todayNoOfDays);
			Date todayDate = calendarstart.getTime();
			String todayDateinString=sdf.format(todayDate);
			
			logger.log(Level.SEVERE,"previousDateinString:: "+previousDateinString+" todayDateinString::: "+todayDateinString+" "+roleName);
			
			List<Employee>  employeeList=new ArrayList<Employee>();
			employeeList=ofy().load().type(Employee.class).filter("companyId", companyId).filter("roleName", roleName).list();
			logger.log(Level.SEVERE,""+roleName+" "+"Employee List size::: "+employeeList.size());
			for (Employee employee : employeeList) {
				ArrayList<String> toEmailList=new ArrayList<String>();
				toEmailList.add(employee.getEmail().trim());
				List<String> branchList=new ArrayList<String>();
				for (int i = 0; i < employee.getEmpBranchList().size(); i++) {
					branchList.add(employee.getEmpBranchList().get(i).getBranchName());
				}
				logger.log(Level.SEVERE,"toEmailList size::: "+toEmailList.size()+" Branch List SIze : "+branchList.size());
				List<Service> serviceList=new ArrayList<Service>();
				try {
					if(branchList.size()!=0){
						serviceList=ofy().load().type(Service.class).filter("companyId", companyId).filter("serviceDate >=",sdf.parse(previousDateinString)).filter("serviceDate <",sdf.parse(todayDateinString)).filter("branch IN",branchList).filter("status", Service.SERVICESTATUSSCHEDULE).list();
					}
					logger.log(Level.SEVERE,"serviceList size::: "+serviceList.size());
				} catch (ParseException e) {
					e.printStackTrace();
					return "Error in parsing date";
				}
				
				ArrayList<String> tbl_header = new ArrayList<String>();
				tbl_header.add("Serial No");
				tbl_header.add("Service Id");
				tbl_header.add("Service Name");
				tbl_header.add("Service Date");
				tbl_header.add("Contract Id");
				tbl_header.add("Customer Id");
				tbl_header.add("Customer Name");
				tbl_header.add("Branch");
				tbl_header.add("Status");
				
				ArrayList<String> tbl1=new ArrayList<String>();
				int count=0;
				for (Service service : serviceList) {
					tbl1.add(count+1+"");
					tbl1.add(service.getCount()+"");
					tbl1.add(service.getProductName().trim());
					tbl1.add(sdf.format(service.getServiceDate()));
					tbl1.add(service.getContractCount()+"");
					tbl1.add(service.getPersonInfo().getCount()+"");
					tbl1.add(service.getPersonInfo().getFullName());
					tbl1.add(service.getBranch());
					tbl1.add(service.getStatus());
					count=count+1;
				}
				
				Email cronEmail = new Email();
				try {
					if(serviceList.size()!=0){
						cronEmail.cronSendEmail(toEmailList,"Services of your branch as on"+" "+ todayDateinString+" that were not completed"+" ("+roleName+")", "Services of your branch as on" +"  "+ todayDateinString+" that were not completed", company, tbl_header, tbl1, null, null, null, null);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.log(Level.SEVERE,"Error in cronEmail method:::"+e);
					return "Failed to send emails";
				}
			}
		}
		
		return "Successfull";
	}
}
