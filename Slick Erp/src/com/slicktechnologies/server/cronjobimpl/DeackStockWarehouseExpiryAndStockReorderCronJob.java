package com.slicktechnologies.server.cronjobimpl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

public class DeackStockWarehouseExpiryAndStockReorderCronJob extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6515975898480645769L;
	/**
	 * 
	 */
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	 Logger logger = Logger.getLogger("NameOfYourLogger");
	 
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
				
					physicalInventory();
			}

	private void physicalInventory() {
		

		Email cronEmail = new Email();			
		Date today=DateUtility.getDateWithTimeZone("IST", new Date());
		
		DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		/**
		 *   Setting the time to date
		 */
		
		logger.log(Level.SEVERE,"Date Before Adding One Day"+today);
		
		Calendar cal=Calendar.getInstance();
		cal.setTime(today);
		cal.add(Calendar.DATE, 0);
		
		Date dateForFilter=null;
		
		try {
			dateForFilter=dateFormat.parse(dateFormat.format(cal.getTime()));
			cal.set(Calendar.HOUR_OF_DAY,23);
			cal.set(Calendar.MINUTE,59);
			cal.set(Calendar.SECOND,59);
			cal.set(Calendar.MILLISECOND,999);
			dateForFilter=cal.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		logger.log(Level.SEVERE,"Date After Adding One Date"+dateForFilter);
		
		/*************************************End*********************************/
		
		
 try{
	 
	logger.log(Level.SEVERE,"Date After Adding One Date"+dateForFilter);

	 
/********************************Adding status in the list ***********************/
//	 
//	 ArrayList<String> obj = new ArrayList<String>();
//	 obj.add("Created");
//	 obj.add("Approved");
//	 

/******************************Converting todayDate to String format*****************/
	 
	 Date todaysDate = new Date();
	 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	 String todayDateString = df.format(todaysDate);
	 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);

	 
/********************************Adding Companies in the list ***********************/
	 
List<Company> compEntity = ofy().load().type(Company.class).list();
if(compEntity.size()>0){

	logger.log(Level.SEVERE,"If compEntity size > 0");
	logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
	

	for(int i=0;i<compEntity.size();i++){
		
		Company c=compEntity.get(i);
		logger.log(Level.SEVERE,"In the for loop");
		logger.log(Level.SEVERE,"Company Name="+c);
		System.out.println("Company Name:"+c);
		logger.log(Level.SEVERE,"The value of i is:" +i);
		logger.log(Level.SEVERE,"Date After Adding One Date"+dateForFilter);

		
/********************************Checking prosname & prosconfig for each company ***********************/


		ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
		ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
		
		boolean processconfigflag = false;
	
		
		/************************ Reorder level Alert email and Warehouse expiry Email alert EMAIL and Re DEADSTOCK NBHC Starts code here  *************************/
		
		/**
		 * Date 02-05-2017 added by vijay for NBHC Deadstock Reorder EMAIL and Warehouse expiry Email to branch poc
		 */
		boolean processConfigNBHCReorderDeadstock = false;
		boolean processConfigWarehouseExpiry =false;
		if(processconfig!=null){
			if(processconfig.isConfigStatus()){
				for(int l=0;l<processconfig.getProcessList().size();l++){
					if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("ReorderLevelEmail") && processconfig.getProcessList().get(l).isStatus()==true){
						processConfigNBHCReorderDeadstock=true;
					}
					if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("WarehouseExpiryEmail") && processconfig.getProcessList().get(l).isStatus()==true){
						processConfigWarehouseExpiry=true;
					}
				}
		   }
		}
		logger.log(Level.SEVERE,"process config  Reorder level Flag =="+processConfigNBHCReorderDeadstock);
		logger.log(Level.SEVERE,"process config  Warehouse Expiry Flag =="+processConfigWarehouseExpiry);

		/**
		 * Date 02-05-2017 added by vijay for NBHC DEADSTOCK Reorder Email sent to warehouse branch POC email id
		 */
		
		if(processConfigNBHCReorderDeadstock){
			
		List<WareHouse> warehouselist = ofy().load().type(WareHouse.class).filter("companyId", compEntity.get(i).getCompanyId()).list();
		

		
		for(int p=0;p<warehouselist.size();p++){
			
			List<ProductInventoryViewDetails> productInvenViewDetailsList = new  ArrayList<ProductInventoryViewDetails>();

			
			List<ProductInventoryViewDetails> warehousewiseproductInventoryViewDetailsEntity = ofy().load().type(ProductInventoryViewDetails.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("warehousename",warehouselist.get(p).getBusinessUnitName()).list();

			for(int l=0;l<warehousewiseproductInventoryViewDetailsEntity.size();l++){
				
				
				if(warehousewiseproductInventoryViewDetailsEntity.get(l).getAvailableqty()<=warehousewiseproductInventoryViewDetailsEntity.get(l).getReorderlevel())
				{
					logger.log(Level.SEVERE,"Warehouse Name =="+warehousewiseproductInventoryViewDetailsEntity.get(l).getWarehousename());
					
					ProductInventoryViewDetails productinventoy = new ProductInventoryViewDetails();
					productinventoy.setProdid(warehousewiseproductInventoryViewDetailsEntity.get(l).getProdid());
					productinventoy.setProdcode(warehousewiseproductInventoryViewDetailsEntity.get(l).getProdcode());
					productinventoy.setProdname(warehousewiseproductInventoryViewDetailsEntity.get(l).getProdname());
					productinventoy.setWarehousename(warehousewiseproductInventoryViewDetailsEntity.get(l).getWarehousename());
					productinventoy.setStoragelocation(warehousewiseproductInventoryViewDetailsEntity.get(l).getStoragelocation());
					productinventoy.setStoragebin(warehousewiseproductInventoryViewDetailsEntity.get(l).getStoragebin());
					productinventoy.setMinqty(warehousewiseproductInventoryViewDetailsEntity.get(l).getMinqty());
					productinventoy.setReorderlevel(warehousewiseproductInventoryViewDetailsEntity.get(l).getReorderlevel());
					productinventoy.setAvailableqty(warehousewiseproductInventoryViewDetailsEntity.get(l).getAvailableqty());
					productInvenViewDetailsList.add(productinventoy);

				}
			}
			
			//send email code here
			if(productInvenViewDetailsList.size()>0){
				logger.log(Level.SEVERE,"List Size=="+productInvenViewDetailsList.size());

				Branch branch = ofy().load().type(Branch.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("count", warehouselist.get(p).getBranchdetails().get(0).getBranchID()).first().now();
				
				// email id is added to emailList
				ArrayList<String> toEmail=new ArrayList<String>();
				toEmail.add(branch.getPocEmail());
				
				 String title = "Stock Below Reorder Level As On Date";
				 
				ArrayList<String> tbl_header = new ArrayList<String>();
				tbl_header.add("Serial No");
				tbl_header.add("Product Id");
				tbl_header.add("Product Code");
				tbl_header.add("Product Name");
				tbl_header.add("Warehouse");
				tbl_header.add("Storage Location");
				tbl_header.add("Storage Bin");
//				tbl_header.add("Minimun Qty");
				tbl_header.add("Reorder Level");
				tbl_header.add("Available Qty");
				tbl_header.add("Shortfall");
				
				
				ArrayList<String> tbl1=new ArrayList<String>();
				
				
				for(int l=0;l<productInvenViewDetailsList.size();l++)
				{
					tbl1.add((l+1)+"");
					tbl1.add(productInvenViewDetailsList.get(l).getProdid()+"");
					tbl1.add(productInvenViewDetailsList.get(l).getProdcode()+"");
					tbl1.add(productInvenViewDetailsList.get(l).getProdname()+"");
					tbl1.add(productInvenViewDetailsList.get(l).getWarehousename()+"");
					tbl1.add(productInvenViewDetailsList.get(l).getStoragelocation()+"");
					tbl1.add(productInvenViewDetailsList.get(l).getStoragebin()+"");
//					tbl1.add(productInvenViewDetailsList.get(l).getMinqty()+"");
					tbl1.add(productInvenViewDetailsList.get(l).getReorderlevel()+"");
					tbl1.add(productInvenViewDetailsList.get(l).getAvailableqty()+"");
					tbl1.add(productInvenViewDetailsList.get(l).getAvailableqty() - productInvenViewDetailsList.get(l).getReorderlevel() +"");
				}
				
				cronEmail.cronInventorySendEmail(toEmail, "Stock Below Reorder Level As On"+" "+ todayDateString, title +"  "+ todayDateString, c, tbl_header, tbl1, null, null, null, null, null);
			}
		}
		
		}
		
		/**
		 * ends here
		 */
		
		/**
		 * Date 02-05-2017 added by vijay for NBHC Deadstock for warehouse expiry Email to warehouse branch POC Email 
		 */
		
		if(processConfigWarehouseExpiry){
			logger.log(Level.SEVERE,"Warehosue Expiry process config true");
			List<WareHouse> warehouselist = ofy().load().type(WareHouse.class).filter("companyId", compEntity.get(i).getCompanyId()).list();
			
			logger.log(Level.SEVERE,"warehouselist size =="+warehouselist.size());

			for(int m=0;m<warehouselist.size();m++){
				logger.log(Level.SEVERE,"warehouse before getWarehouseExpiryDate !=null =");
				if(warehouselist.get(m).getstatus()){
				if(warehouselist.get(m).getWarehouseExpiryDate()!=null){
					logger.log(Level.SEVERE,"warehouse after inside condtion getWarehouseExpiryDate !=null =");
				if(today.after(warehouselist.get(m).getWarehouseExpiryDate())){
					logger.log(Level.SEVERE,"Before branch query");
					if(!warehouselist.get(m).getBranchdetails().get(0).getBranchName().equals("")){
						
					Branch branch = ofy().load().type(Branch.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("count", warehouselist.get(m).getBranchdetails().get(0).getBranchID()).first().now();
					logger.log(Level.SEVERE,"warehouse expired === branch poc email =="+branch.getPocEmail());
					if(!branch.getPocEmail().equals("")){
					// email id is added to emailList
					ArrayList<String> toEmail=new ArrayList<String>();
					toEmail.add(branch.getPocEmail());
					
					String StringContractEndDate = dateFormat.format(warehouselist.get(m).getWarehouseExpiryDate());
					
					 String title = "Warehouse Expired on"+StringContractEndDate;
					
					String msgBody ="</br></br> Dear "+branch.getPocName().trim()+ ", <br><br> The Warehouse - "+warehouselist.get(m).getBusinessUnitName()+" Warehouse Code - "+warehouselist.get(m).getWarehouseCode()+" has expired. Please extend the same warehouse or transfer materials to branch or hub <br> or diffenrent warehouse under same branch or make warehouse inactive";
					logger.log(Level.SEVERE,"Before call send email method");
					cronEmail.htmlformatsendEmail(toEmail, title, "Warehouse Expired "+StringContractEndDate, c, msgBody, null, null, null, null,null,null);
					}
				  }
				 }
				}
			  }
			}
		}
		
		/** NBHC Deadstock reorder level email and 
		 * warehouse expiry email
		 * Ends here
		 */
		
		
						
	}	//end of for loop
	
}
else{			//else block for if(compEntity.size()>0)
	
	logger.log(Level.SEVERE,"No Company found from cron job completed");	
	System.out.println("No Company found from cron job completed");
}


 }catch(Exception e2){

e2.printStackTrace();
 }


 		
 		
 	}
}
