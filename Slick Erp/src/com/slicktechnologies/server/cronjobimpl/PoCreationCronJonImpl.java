package com.slicktechnologies.server.cronjobimpl;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.JSONArray;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

public class PoCreationCronJonImpl extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4984189815793258388L;


	long companyId=0;
	Logger logger = Logger.getLogger("PoCronJobInboundImpl.class");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
		
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		SimpleDateFormat spf = new SimpleDateFormat("dd MMM yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		spf.setTimeZone(TimeZone.getTimeZone("IST"));
		
		Company company=ofy().load().type(Company.class).filter("accessUrl","my").first().now();
//		Company company=ofy().load().type(Company.class).filter("accessUrl","nbhcwarehouse").first().now();

		companyId=company.getCompanyId();
		ArrayList<String> statusList=new ArrayList<String>();
		statusList.add(AccountingInterface.TALLYCREATED);
		statusList.add(AccountingInterface.FAILED);
		List<AccountingInterface> accountingInterfaceList=ofy().load().type(AccountingInterface.class).filter("companyId", companyId).filter("status IN", statusList).filter("documentType", "Purchase Order").filter("direction", AccountingInterface.INBOUNDDIRECTION).filter("sourceSystem", AppConstants.NBHC).filter("destinationSystem", AppConstants.CCPM).list();
		HashSet<String> refDoc1=new HashSet<String>();
		/*
		 * Finding unique order Id
		 */
		for (AccountingInterface acctInt : accountingInterfaceList) {
			refDoc1.add(acctInt.getReferenceDocumentNo1().trim());
		}
		Object[] refDocArray=refDoc1.toArray();
		logger.log(Level.SEVERE,"refDoc1 size::::"+refDoc1.size());
		JSONArray poArray=new JSONArray();
		for (int j=0;j<refDocArray.length;j++){
			String refDocId=refDocArray[j].toString().trim();
			JSONObject poObj=new JSONObject();
			boolean sameRecord=false;
			JSONArray poProductArray=new JSONArray();
			for (int i = 0; i < accountingInterfaceList.size(); i++) {
				if(refDocId.trim().equalsIgnoreCase(accountingInterfaceList.get(i).getReferenceDocumentNo1().trim())){
					if(!sameRecord){
						try {
							poObj.put("refDocNo", accountingInterfaceList.get(i).getReferenceDocumentNo1().trim());
							poObj.put("refDocDate", spf.format(accountingInterfaceList.get(i).getReferenceDocumentDate1()));
							poObj.put("poDate", spf.format(new Date()));
							poObj.put("poTitle",accountingInterfaceList.get(i).getBranch().trim()+" "+spf.format(accountingInterfaceList.get(i).getAccountingInterfaceCreationDate()));
							poObj.put("branch",accountingInterfaceList.get(i).getBranch().trim());
							poObj.put("vendorRefId",accountingInterfaceList.get(i).getVendorID()+"");
							poObj.put("vendorName",accountingInterfaceList.get(i).getVendorName().trim());
							poObj.put("vendorCell",accountingInterfaceList.get(i).getVendorCell()+"");
							
							JSONObject productObj=new JSONObject();
							productObj.put("productCode", accountingInterfaceList.get(i).getProductCode().trim());
							productObj.put("warehouseCode", accountingInterfaceList.get(i).getWareHouseCode().trim());
							productObj.put("price", accountingInterfaceList.get(i).getProductPrice());
							productObj.put("quantity", accountingInterfaceList.get(i).getQuantity());
							productObj.put("uom", accountingInterfaceList.get(i).getUnitOfMeasurement().trim());
							productObj.put("deliveryDate",  spf.format(accountingInterfaceList.get(i).getReferenceDocumentDate2()));
							productObj.put("productRefId",accountingInterfaceList.get(i).getProductRefId().trim());
							poProductArray.add(productObj);
							poObj.put("productList", poProductArray);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							logger.log(Level.SEVERE,"ERROR 1::::::::::::::"+e);
						}
						sameRecord=true;
					}else{
						try{
						JSONObject productObj=new JSONObject();
						productObj.put("productCode", accountingInterfaceList.get(i).getProductCode().trim());
						productObj.put("warehouseCode", accountingInterfaceList.get(i).getWareHouseCode().trim());
						productObj.put("price", accountingInterfaceList.get(i).getProductPrice());
						productObj.put("quantity", accountingInterfaceList.get(i).getQuantity());
						productObj.put("uom", accountingInterfaceList.get(i).getUnitOfMeasurement().trim());
						productObj.put("deliveryDate",  spf.format(accountingInterfaceList.get(i).getReferenceDocumentDate2()));
						productObj.put("productRefId",accountingInterfaceList.get(i).getProductRefId().trim());
						poProductArray.add(productObj);
						poObj.put("productList", poProductArray);
						}catch(Exception e){
							e.printStackTrace();
							logger.log(Level.SEVERE,"ERROR::::::::::::::"+e);
						}
					}
				}
			}
			poArray.add(poObj);
			
		}
		/*
		 * Task Queue Call
		 */
		String jsonString=poArray.toJSONString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE,jsonString);
		Queue queue = QueueFactory.getQueue("POnGRNCreation-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/poCreationInterfaceTaskQueue").param("poData",jsonString+"$"+companyId));
		
	}
}
