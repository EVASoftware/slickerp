package com.slicktechnologies.server.cronjobimpl;

import javax.servlet.http.HttpServlet;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.common.WhatsNew;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
public class WhatsNewCronJobImpl extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8344568494260573815L;
	

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmtQuotationDate = new SimpleDateFormat("dd/MM/yyyy");
	 Logger logger = Logger.getLogger("NameOfYourLogger");
	  
		public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws IOException {
			
				whatsNewList();
		}
		
		

		private void whatsNewList() {
			
		List<Company> compEntity = ofy().load().type(Company.class).list();
		
		logger.log(Level.SEVERE,"Comapny Size"+compEntity.size());
		
		
		for(int i=0;i<compEntity.size();i++){
			
			WhatsNew whatsnewentity = ofy().load().type(WhatsNew.class).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
			logger.log(Level.SEVERE,"whatsnewentity"+whatsnewentity);

			if(whatsnewentity==null){
			
				
			GenricServiceImpl genserimpl = new GenricServiceImpl();
			logger.log(Level.SEVERE,"for first time");

			WhatsNew whatsnew = new WhatsNew();
			
			whatsnew.setTitle("Manage Contract Renewal:-");
			whatsnew.setSubject("Now contract renewal is very easy with Contract Renewal process.");
			whatsnew.setPath("Follow path Service --> Service --> Contract Renewal. Key Features");
			whatsnew.setDescriptionone(" * Revise rate before sending reminder.");
			whatsnew.setDescriptiontwo(" * Get Contracts due for renewal any time");
			whatsnew.setDescriptionthree(" * send renewal reminder by SMS,Email print out to customer");
			whatsnew.setDescriptionfour(" * Mark renewal not required");
//			whatsnew.setDescriptionfive("");
			whatsnew.setMoreDetails("Click here to more details"); 
			whatsnew.setGoogleDriveLink("https://drive.google.com/file/d/0B9SJRoRsdEG_bkx4NmY2NkVQNlk/view?usp=sharing");
			

			whatsnew.setCompanyId(compEntity.get(i).getCompanyId());
			System.out.println("hi whatsnew"+whatsnew);
			
			genserimpl.save(whatsnew);
			
			logger.log(Level.SEVERE,"Data Saved Successfully");
			
			}
			else{
						logger.log(Level.SEVERE,"1111111111111");

						logger.log(Level.SEVERE,"for overtime");

						GenricServiceImpl genricimpl = new GenricServiceImpl();

						whatsnewentity.setTitle("Manage Contract Renewal:-");
						whatsnewentity.setSubject("Now contract renewal is very easy with Contract Renewal process.");
						whatsnewentity.setPath("Follow path Service --> Service --> Contract Renewal. Key Features");
						whatsnewentity.setDescriptionone(" * Revise rate before sending reminder.");
						whatsnewentity.setDescriptiontwo(" * Get Contracts due for renewal any time");
						whatsnewentity.setDescriptionthree(" * send renewal reminder by SMS,Email print out to customer");
						whatsnewentity.setDescriptionfour(" * Mark renewal not required");
//						whatsnewentity.setDescriptionfive("");						whatsnewentity.setMoreDetails("Click here to more details");
						whatsnewentity.setGoogleDriveLink("https://drive.google.com/file/d/0B9SJRoRsdEG_bkx4NmY2NkVQNlk/view?usp=sharing");
						whatsnewentity.setCompanyId(compEntity.get(i).getCompanyId());
												
					genricimpl.save(whatsnewentity);
					
					logger.log(Level.SEVERE,"Data Saved Successfully");

					
				}
			
		  }
		
		}

}
