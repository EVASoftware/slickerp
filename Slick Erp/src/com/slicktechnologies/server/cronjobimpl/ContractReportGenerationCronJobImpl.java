package com.slicktechnologies.server.cronjobimpl;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.output.WriterOutputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ContractReportGenerationCronJobImpl extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5678253456181615483L;

	Logger logger = Logger.getLogger("ContractReportGenerationCronJobImpl.class");
	long companyId=0;
	PrintWriter pw;
	CsvWriter csv;
	byte[] bb;
	/**
	 *  nidhi
	 *   3-01-2018
	 *   
	 */
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
//	HttpServletResponse responseref;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		super.doGet(req, resp);
		/*
//		 responseref=resp;
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		 Date d=new Date();
		 String dateString = sdf.format(d);
		
//		Calendar calendar=Calendar.getInstance();
//		calendar.setTime(new Date());
//		calendar.add(Calendar.DAY_OF_YEAR, -2);
//		Date afterDate = calendar.getTime();
//		String previousDateinString=sdf.format(afterDate);
//		String todayDateinString=sdf.format(new Date());
//		logger.log(Level.SEVERE,"previousDateinString::"+previousDateinString+"todayDateinString:::"+todayDateinString);
		
		Calendar calendar=Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.DATE, -1);
		Date previousDate = calendar.getTime();
		String todayDateinString=sdf.format(previousDate);
//		
		logger.log(Level.SEVERE,"Date ::"+todayDateinString);
		Company company=ofy().load().type(Company.class).filter("accessUrl","my").first().now();
		companyId=company.getCompanyId();

		boolean flag= false;
		ProcessConfiguration process = ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId).
				filter("processName", "CronJob").first().now();
		
		for (int i = 0; i < process.getProcessList().size(); i++) {
			if(process.getProcessList().get(i).processType.equalsIgnoreCase("ContractReportGenerationCronJobImpl")){
				flag= true;
			}
		}
		
		logger.log(Level.SEVERE,"Flag ::"+flag+"  "+companyId);
		if(flag){
			ArrayList<String> employeeRoleList=new ArrayList<String>();
			employeeRoleList.add("Branch Manager");
			employeeRoleList.add("Zonal Head");
			employeeRoleList.add("Business Head");
			employeeRoleList.add("Admin");
			employeeRoleList.add("Zonal Coordinator");
			employeeRoleList.add("Regional Manager");
			
			
			for(String roleName:employeeRoleList){
				List<Employee>  employeeList=new ArrayList<Employee>();
				employeeList=ofy().load().type(Employee.class).filter("companyId", companyId).filter("roleName", roleName).list();
				logger.log(Level.SEVERE,""+roleName+" "+"Employee List size::: "+employeeList.size());
				
				for (Employee employee : employeeList) {
					ArrayList<String> toEmailList=new ArrayList<String>();
					toEmailList.add(employee.getEmail().trim());
					List<String> branchList=new ArrayList<String>();
					for (int i = 0; i < employee.getEmpBranchList().size(); i++) {
						branchList.add(employee.getEmpBranchList().get(i).getBranchName());
					}
					logger.log(Level.SEVERE,"toEmailList size::: "+toEmailList.size()+" Branch List SIze : "+branchList.size());
					
					List<Contract> contratList = new ArrayList<Contract>();
					try {
						if(branchList.size()!=0){
						contratList = ofy().load().type(Contract.class).filter("companyId", companyId).filter("creationDate", sdf.parse(todayDateinString)).filter("branch IN",branchList).list();
						}
					} catch (ParseException e1) {
						e1.printStackTrace();
						logger.log(Level.SEVERE,"Error parsing date while querry");
					}
					logger.log(Level.SEVERE,"Contract List Size ::"+contratList.size());
					
					*//**
					 * Email Header 
					 *//*
					ArrayList<String> tbl_header = new ArrayList<String>();
					tbl_header.add("Contract Id");
					tbl_header.add("Contract Date");
					tbl_header.add("Contract Start Date");
					tbl_header.add("Contract End Date");
					tbl_header.add("Customer Id");
					tbl_header.add("Customer Name");
					tbl_header.add("Contract Type");
					tbl_header.add("Contrat Value");
					tbl_header.add("Branch");
					tbl_header.add("Sales Person");*//** Date : 24-10-2017 BY ANIL**//*
					tbl_header.add("Status");
					
					ArrayList<String> tbl1=new ArrayList<String>();
					int count=0;
					for (Contract obj : contratList) {
						tbl1.add(obj.getCount()+"");
						tbl1.add(sdf.format(obj.getContractDate()));
						tbl1.add(sdf.format(obj.getStartDate()));
						tbl1.add(sdf.format(obj.getEndDate()));
						tbl1.add(obj.getCinfo().getCount()+"");
						tbl1.add(obj.getCinfo().getFullName());
						tbl1.add(obj.getType());
						tbl1.add(obj.getNetpayable()+"");
						tbl1.add(obj.getBranch());
						tbl1.add(obj.getEmployee());*//** Date : 24-10-2017 BY ANIL**//*
						tbl1.add(obj.getStatus());
						count=count+1;
					}
					
					*//**
					 * Email Parts ends here
					 *//*
					Email cronEmail = new Email();
					
					try {
						if(contratList.size()!=0){
							cronEmail.cronSendEmail(toEmailList, "Contract Report As On Date"+" "+ todayDateinString, "Contract Report As On Date" +"  "+ todayDateinString, company, tbl_header, tbl1, null, null, null, null);
						}else{
							*//**
							 * Date : 24-10-2017 BY ANIL
							 * If no contract is created then we have to send 
							 * mail as no contract has created
							 *//*
							cronEmail.cronSendEmail(toEmailList, "Contract Report As On Date"+" "+ todayDateinString, "No contract for day " , company, null, null, null, null, null, null);
						}
					}catch (IOException e) {
						e.printStackTrace();
						logger.log(Level.SEVERE,"Error in cronEmail method:::"+e);
//						return "Failed to send emails";
					}
				}
			}
			
//			ArrayList<String> toEmailList=new ArrayList<String>();
//			toEmailList.add(company.getPocEmail());
//			
//			List<Contract> contratList = null;
//			try {
//				contratList = ofy().load().type(Contract.class).filter("companyId", companyId).filter("creationDate", sdf.parse(todayDateinString)).list();
//			} catch (ParseException e1) {
//				e1.printStackTrace();
//				logger.log(Level.SEVERE,"Error parsing date while querry");
//			}
//			logger.log(Level.SEVERE,"Contract List Size ::"+contratList.size());
			
			

		}*/
		
	}
	
	
	public void getContractReport(String contractDetail){
		Gson gson = new Gson();
		JSONArray jsonarr = null;
		try {
			jsonarr=new JSONArray(contractDetail.trim());
		} catch (JSONException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		try {
			CronJobConfigrationDetails cronDetails =null;
			for(int j=0;j<jsonarr.length();j++){
				JSONObject jsonObj = jsonarr.getJSONObject(j);
				cronDetails = new CronJobConfigrationDetails();
				
				cronDetails = gson.fromJson(jsonObj.toString(), CronJobConfigrationDetails.class);
				
				
				SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
				sdf.setTimeZone(TimeZone.getTimeZone("IST"));
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				
				 Date d=new Date();
				 String dateString = sdf.format(d);
				int diff = 0;
				 if(cronDetails.isOverdueStatus() && cronDetails.getOverdueDays()>0){
					 diff = cronDetails.getOverdueDays() * -1;
				 }else{
					 diff = cronDetails.getInterval();
				 }
				logger.log(Level.SEVERE,"get diff days for contract -- " + diff);
				Calendar calendar=Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DATE, diff);
				Date previousDate = calendar.getTime();
				String todayDateinString=sdf.format(previousDate);
////				
//				logger.log(Level.SEVERE,"Date ::"+todayDateinString);
//				Company company=ofy().load().type(Company.class).filter("accessUrl","my").first().now();
////				Company company=ofy().load().type(Company.class).filter("accessUrl","ganesh").first().now();
				

				List<Company> compEntity = ofy().load().type(Company.class).list();
				
				 
				if(compEntity.size()>0)
				{
				
					logger.log(Level.SEVERE,"If compEntity size > 0");
					logger.log(Level.SEVERE,"Size of compEntity"+compEntity.size());
					
	
				for(int dd=0;dd<compEntity.size();dd++){
									
							Company c=compEntity.get(dd);
			
				
							companyId=c.getCompanyId();

							
							logger.log(Level.SEVERE,"Date Before Adding One Day"+previousDate);
							Calendar cal3=Calendar.getInstance();
							cal3.setTime(previousDate);
							
							
							Date plusDaysDate=null;
							
							try {
								plusDaysDate=dateFormat.parse(dateFormat.format(cal3.getTime()));
								cal3.set(Calendar.HOUR_OF_DAY,23);
								cal3.set(Calendar.MINUTE,59);
								cal3.set(Calendar.SECOND,59);
								cal3.set(Calendar.MILLISECOND,999);
								plusDaysDate=cal3.getTime();
							} catch (ParseException e) {
								e.printStackTrace();
							}
							
							
							Date minusDaysDate=null;
							
							try {
								minusDaysDate=dateFormat.parse(dateFormat.format(new Date()));
								calendar.set(Calendar.HOUR_OF_DAY,0);
								calendar.set(Calendar.MINUTE,0);
								calendar.set(Calendar.SECOND,0);
								calendar.set(Calendar.MILLISECOND,0);
								minusDaysDate = calendar.getTime();
							} catch (ParseException e) {
								e.printStackTrace();
							}
							
							{
								
								logger.log(Level.SEVERE," minum date -- " + minusDaysDate + " max days --"+plusDaysDate);
								
								{
									List<Employee>  employeeList=new ArrayList<Employee>();
									employeeList=ofy().load().type(Employee.class).filter("companyId", companyId).filter("roleName", cronDetails.getEmployeeRole()).list();
									logger.log(Level.SEVERE,""+cronDetails.getEmployeeRole()+" "+"Employee List size::: "+employeeList.size());
									
									for (Employee employee : employeeList) {
										ArrayList<String> toEmailList=new ArrayList<String>();
										toEmailList.add(employee.getEmail().trim());
										List<String> branchList=new ArrayList<String>();
										for (int i = 0; i < employee.getEmpBranchList().size(); i++) {
											branchList.add(employee.getEmpBranchList().get(i).getBranchName());
										}
										logger.log(Level.SEVERE,"toEmailList size::: "+toEmailList.size()+" Branch List SIze : "+branchList.size());
										
										ArrayList<String> status = new ArrayList<String>();
										status.add(Contract.CREATED);
										status.add(Contract.CANCELLED);
										status.add(Contract.REJECTED);
										status.add(Contract.REQUESTED);
										List<Contract> contratList = new ArrayList<Contract>();
										try {
											if(branchList.size()!=0){
												if(!cronDetails.isOverdueStatus()){
													contratList = ofy().load().type(Contract.class).filter("companyId", companyId)
															.filter("creationDate >=", minusDaysDate).filter("creationDate <=", plusDaysDate)
															.filter("branch IN",branchList).filter("status IN", status).list();
													
												}else{
													try {
														calendar.add(Calendar.DATE, -90);
														minusDaysDate=dateFormat.parse(dateFormat.format(new Date()));
														calendar.set(Calendar.HOUR_OF_DAY,0);
														calendar.set(Calendar.MINUTE,0);
														calendar.set(Calendar.SECOND,0);
														calendar.set(Calendar.MILLISECOND,0);
														minusDaysDate = calendar.getTime();
													} catch (ParseException e) {
														e.printStackTrace();
													}
													
													contratList = ofy().load().type(Contract.class).filter("companyId", companyId).filter("creationDate >=", minusDaysDate)
															.filter("creationDate <=", plusDaysDate).filter("status IN", status).filter("branch IN",branchList).list();
													
												}
											}
										} catch (Exception e1) {
											e1.printStackTrace();
											logger.log(Level.SEVERE,"Error parsing date while querry");
										}
										logger.log(Level.SEVERE,"Contract List Size ::"+contratList.size());
										
										/**
										 * Email Header 
										 */
										ArrayList<String> tbl_header = new ArrayList<String>();
										tbl_header.add("Contract Id");
										tbl_header.add("Contract Date");
										tbl_header.add("Contract Start Date");
										tbl_header.add("Contract End Date");
										tbl_header.add("Customer Id");
										tbl_header.add("Customer Name");
										tbl_header.add("Contract Type");
										tbl_header.add("Contrat Value");
										tbl_header.add("Branch");
										tbl_header.add("Sales Person");/** Date : 24-10-2017 BY ANIL**/
										tbl_header.add("Status");
										
										ArrayList<String> tbl1=new ArrayList<String>();
										int count=0;
										for (Contract obj : contratList) {
											tbl1.add(obj.getCount()+"");
											tbl1.add(sdf.format(obj.getContractDate()));
											tbl1.add(sdf.format(obj.getStartDate()));
											tbl1.add(sdf.format(obj.getEndDate()));
											tbl1.add(obj.getCinfo().getCount()+"");
											tbl1.add(obj.getCinfo().getFullName());
											tbl1.add(obj.getType());
											tbl1.add(obj.getNetpayable()+"");
											tbl1.add(obj.getBranch());
											tbl1.add(obj.getEmployee());/** Date : 24-10-2017 BY ANIL**/
											tbl1.add(obj.getStatus());
											count=count+1;
										}
										
										/**
										 * Email Parts ends here
										 */
										Email cronEmail = new Email();
										
										try {
											if(contratList.size()!=0){
												
												
												String footer = "";
												
												if(cronDetails.getFooter().trim()!= "" ){
													footer = cronDetails.getFooter();
												}
												
												String emailSubject = "Contract Report As On Date "+todayDateinString;
												
												if(!cronDetails.getSubject().trim().equals("")   && cronDetails.getSubject().trim().length() >0){
													emailSubject = cronDetails.getSubject();
												}
												
												String emailMailBody = "Contract Report As On Date" ;
												
												if(!cronDetails.getMailBody().trim().equals("") && cronDetails.getMailBody().trim().length() >0){
													emailMailBody = cronDetails.getMailBody() ;
												}
												
												if(cronDetails.isOverdueStatus()){
													footer = footer +"<br>" + "<Br>#This mail contains last 3 months data. For more records please contact Email- support@evasoftwaresolutions.com";
												}
												logger.log(Level.SEVERE,"subject -- "+emailSubject);
												cronEmail.cronSendEmail(toEmailList, emailSubject, emailMailBody, c, tbl_header, tbl1,
														null, null, null, null,"",footer);
											}else{
												/**
												 * Date : 24-10-2017 BY ANIL
												 * If no contract is created then we have to send 
												 * mail as no contract has created
												 */
												cronEmail.cronSendEmail(toEmailList, "Contract Report As On Date"+" "+ todayDateinString, "No contract for day " , c, null, null, null, null, null, null);
											}
										}catch (IOException e) {
											e.printStackTrace();
											logger.log(Level.SEVERE,"Error in cronEmail method:::"+e);
//											return "Failed to send emails";
										}
									}
								}
								
								
								

							}
						}
				}
				}
		}catch(Exception e){
			logger.log(Level.SEVERE,"get error in  -- " +e);
		}
		
		
	}
}
