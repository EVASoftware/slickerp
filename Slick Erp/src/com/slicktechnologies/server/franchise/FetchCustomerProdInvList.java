package com.slicktechnologies.server.franchise;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;

public class FetchCustomerProdInvList extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8383842980898590643L;

	Logger logger=Logger.getLogger("FetchCustomerProdInvList.class");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		logger.log(Level.SEVERE,"INSIDE DO GET: ");
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		logger.log(Level.SEVERE,"INSIDE DO POST: ");
		
		String urlCalled = req.getRequestURL().toString().trim();
		String url = urlCalled.replace("http://", "");
		String[] splitUrl = url.split("\\.");
		System.out.println("SplitURL : "+splitUrl[0]);
		logger.log(Level.SEVERE,"SplitURL : "+splitUrl[0]);
		
		Company comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
		logger.log(Level.SEVERE,"comp in FetchCustomerProdInvList: "+comp);
		
		List<ProductInventoryViewDetails> piv = ofy().load().type(ProductInventoryViewDetails.class).filter("companyId",comp.getCompanyId()).list();
		
		Gson gson = new Gson();  
		JSONObject jObj=new JSONObject();
		
		JSONArray jsonArray = null;
		try {
			jsonArray = new JSONArray(gson.toJson(piv));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.log(Level.SEVERE,"Json array exception : "+e);
			
		}
		jObj.put("piv_list",jsonArray);
		
		resp.getWriter().print(jObj);
	}
	
	
}
