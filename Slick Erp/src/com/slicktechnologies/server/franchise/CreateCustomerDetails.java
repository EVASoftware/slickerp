package com.slicktechnologies.server.franchise;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.LicenseDetails;

public class CreateCustomerDetails extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8112686541845827694L;
	
	Logger logger=Logger.getLogger("CreateCustomerDetails.class");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		logger.log(Level.SEVERE,"INSIDE DO GET: ");
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		logger.log(Level.SEVERE,"INSIDE DO POST: ");
		
		
		String data = req.getParameter("jsonString");
		System.out.println("DATA : "+data);
		String screenData=data.trim().replace("#and", "&");
		String urlCalled = req.getRequestURL().toString().trim();
		String url = urlCalled.replace("http://", "");
		String[] splitUrl = url.split("\\.");
		System.out.println("SplitURL : "+splitUrl[0]);
		Exception exep = null;
		// try {
		Company comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
		logger.log(Level.SEVERE, "Stage One"+screenData);
		
		Gson gson = new Gson();
		JSONObject object = null;
		try {
			object = new JSONObject(screenData.trim());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		String brandUrl=object.optString("comp_brandUrl").trim();
		String compName=object.optString("comp_name").trim();
		
		JSONArray cust_array = null;
		try {
			cust_array = object.getJSONArray("cust_list");
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		Type type = new TypeToken<ArrayList<Customer>>(){}.getType();
		ArrayList<Customer> custList=new ArrayList<Customer>();
		custList=gson.fromJson(cust_array.toString(), type);
		
		Customer cust = new Customer();
		
		for(Customer obj1:custList){
			cust.setCompanyId(comp.getCompanyId());
			cust.setFullname(obj1.getFullname());
			cust.setCellNumber1(obj1.getCellNumber1());
			cust.setBranch(obj1.getBranch());
			cust.setEmployee(obj1.getEmployee());
			cust.setCategory(obj1.getCategory());
			cust.setType(obj1.getType());
			cust.getAdress().setAddrLine1(obj1.getAdress().getAddrLine1());
			cust.getAdress().setCity(obj1.getAdress().getCity());
			cust.getAdress().setState(obj1.getAdress().getState());
			cust.getAdress().setCountry(obj1.getAdress().getCountry());
			cust.getAdress().setAddrLine1(obj1.getAdress().getAddrLine1());
			cust.getAdress().setCity(obj1.getAdress().getCity());
			cust.getAdress().setState(obj1.getAdress().getState());
			cust.getAdress().setCountry(obj1.getAdress().getCountry());
			cust.setDescription("This customer was created by"+compName+"on date"+ new Date());
			cust.setFranchiseType(obj1.getFranchiseType());
			cust.setFranchiseUrl(obj1.getFranchiseUrl());
			cust.setMasterFranchiseUrl(obj1.getMasterFranchiseUrl());
		}
		
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(cust);
		
		resp.getWriter().print(cust);
	}

}
