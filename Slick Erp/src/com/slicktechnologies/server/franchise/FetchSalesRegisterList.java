package com.slicktechnologies.server.franchise;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

/** 
 * @author Viraj
 * Date: 16-05-2019
 * Description: To fetch Sales Register List
 */
public class FetchSalesRegisterList extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3568739190049155800L;
	
	Logger logger=Logger.getLogger("FetchSalesRegisterList.class");
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		logger.log(Level.SEVERE,"INSIDE DO GET: ");
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		logger.log(Level.SEVERE,"INSIDE DO POST: ");
		
		SimpleDateFormat fmt= new SimpleDateFormat("dd/MMM/yyyy");
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		String urlCalled = req.getRequestURL().toString().trim();
		String url = urlCalled.replace("http://", "");
		String[] splitUrl = url.split("\\.");
		System.out.println("SplitURL : "+splitUrl[0]);
		logger.log(Level.SEVERE,"SplitURL : "+splitUrl[0]);
		
		String data = req.getParameter("jsonString");
		System.out.println("DATA : "+data);
		String screenData=data.trim().replace("#and", "&");
		
		CsvWriter csvwriter = new CsvWriter();
		
		Gson gson = new Gson();
		JSONObject object = null;
		try {
			object = new JSONObject(screenData.trim());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		String fromDate = object.optString("from_Date").trim();
		String toDate = object.optString("to_Date").trim();
		logger.log(Level.SEVERE,"fromDate in FetchSalesRegisterList: "+fromDate);
		logger.log(Level.SEVERE,"toDate in FetchSalesRegisterList: "+toDate);
		Date from_Date = null;
		Date to_Date = null;
		try {
				from_Date = fmt.parse(fromDate);
				to_Date = fmt.parse(toDate);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Company comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
		logger.log(Level.SEVERE,"comp in FetchSalesRegisterList: "+comp);
		
		List<Invoice> invoiceList = ofy().load().type(Invoice.class).filter("companyId",comp.getCompanyId())
				.filter("invoiceDate >=",from_Date).filter("invoiceDate <=",to_Date).list();
		 
		HashSet<Integer> servOdrSet=new HashSet<Integer>();
		HashSet<Integer> salesOdrSet = new HashSet<Integer>();
		
		HashSet<Integer> invoiceIdSet=new HashSet<Integer>();
		HashSet<Integer> clientIdSet=new HashSet<Integer>();
		
		for(Invoice inv : invoiceList){
			if(inv.getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICE)){
				servOdrSet.add(inv.getContractCount());
			}else if(inv.getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)){
				salesOdrSet.add(inv.getContractCount());
			}
			
			invoiceIdSet.add(inv.getCount());
			clientIdSet.add(inv.getPersonInfo().getCount());
			logger.log(Level.SEVERE, "clientIdSet in FetchSalesRegisterList:" + inv.getPersonInfo().getCount());
		}
		
		ArrayList<Integer> servOdrList=new ArrayList<Integer>(servOdrSet);
		List<Contract> ContList=new ArrayList<Contract>();
		if(servOdrSet.size()!=0){
			ContList=ofy().load().type(Contract.class).filter("companyId",invoiceList.get(0).getCompanyId()).filter("count IN", servOdrList).list();
		}
		
		ArrayList<Integer> salesOdrList = new ArrayList<Integer>(salesOdrSet);
		List<SalesOrder> SoList = new ArrayList<SalesOrder>();
		if(salesOdrSet.size()!=0){
			SoList = ofy().load().type(SalesOrder.class).filter("companyId", invoiceList.get(0).getCompanyId()).filter("count IN", salesOdrList).list();
		}
		
		ArrayList<Integer> invoiceIdList=new ArrayList<Integer>(invoiceIdSet);
		ArrayList<Integer> clientIdList=new ArrayList<Integer>(clientIdSet);
		
		logger.log(Level.SEVERE, "clientIdList in FetchSalesRegisterList:" + clientIdList.get(0).intValue());
		List<CustomerPayment> CustPayList = new ArrayList<CustomerPayment>();
		if(invoiceIdList.size()!=0){
			CustPayList = ofy().load().type(CustomerPayment.class).filter("companyId",invoiceList.get(0).getCompanyId()).filter("invoiceCount IN", invoiceIdList).list();
		}
		logger.log(Level.SEVERE, "clientIdSet size in FetchSalesRegisterList:" + clientIdSet.size());
		logger.log(Level.SEVERE, "companyId size in FetchSalesRegisterList:" + invoiceList.get(0).getCompanyId());
		List<Customer> CustList= new ArrayList<Customer>();
		if(clientIdList.size()!=0){
			CustList=ofy().load().type(Customer.class).filter("companyId",invoiceList.get(0).getCompanyId()).filter("count IN", clientIdList).list();
		}
		logger.log(Level.SEVERE, "srCustList size in FetchSalesRegisterList:" + CustList.size());
		
		ArrayList<Invoice> invList = new ArrayList<Invoice>();
		for(Invoice inv : invoiceList) {
			invList.add(inv);
			logger.log(Level.SEVERE, "invList size in FetchSalesRegisterList Invoice Name: " + invList.get(0).getPersonInfo().getFullName());
		}
		logger.log(Level.SEVERE, "invList size in FetchSalesRegisterList:" + invList.size());
		
		CsvWriter.srContList = ContList;
		CsvWriter.srCustList = CustList;
		CsvWriter.srCustPayList = CustPayList;
		CsvWriter.srSoList = SoList;
		CsvWriter.invoicelis = invList;
		
		String salesRegString = csvwriter.fetchStringOfSalesRegister();
		logger.log(Level.SEVERE, "salesRegString size in FetchSalesRegisterList:" + salesRegString);
		
		JSONObject jObj=new JSONObject();
		
		JSONArray jsonArray = null;
		try {
			jsonArray = new JSONArray(gson.toJson(invoiceList));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.log(Level.SEVERE,"Json array exception : "+e);
			
		}
		try {
			jObj.put("sr_list",jsonArray);
			jObj.put("sr_String",salesRegString);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		resp.getWriter().print(jObj);
	}

}
/** Ends **/