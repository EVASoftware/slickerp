package com.slicktechnologies.server.franchise;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class FetchBrandDetails extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2364912644488469072L;
	
	Logger logger=Logger.getLogger("FetchBrandDetails.class");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		logger.log(Level.SEVERE,"INSIDE DO GET: ");
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		logger.log(Level.SEVERE,"INSIDE DO POST: ");
		
		String url1 = req.getParameter("jsonString");
		
		String urlCalled = req.getRequestURL().toString().trim();
		String url = urlCalled.replace("http://", "");
		String[] splitUrl = url.split("\\.");
		System.out.println("SplitURL : "+splitUrl[0]);
		logger.log(Level.SEVERE,"SplitURL : "+splitUrl[0]);
		Company comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
		logger.log(Level.SEVERE,"comp in FetchBrandDetails: "+comp);
		Gson gson = new Gson();  
		JSONObject jObj=new JSONObject();
		if(comp!=null) {
			jObj.put("comp_id", comp.getId());
			jObj.put("comp_name", comp.getBusinessUnitName());
			jObj.put("comp_pocname", comp.getPocName());
			jObj.put("comp_email", comp.getEmail());
			jObj.put("comp_no", comp.getCellNumber1());
			jObj.put("comp_addLine1", comp.getAddress().getAddrLine1());
			jObj.put("comp_city", comp.getAddress().getCity());
			jObj.put("comp_state", comp.getAddress().getState());
			jObj.put("comp_country", comp.getAddress().getCountry());
			jObj.put("comp_url", comp.getCompanyURL());
			logger.log(Level.SEVERE,"Json object: "+jObj);
		}
		
		resp.getWriter().print(jObj);
		
	}

}
