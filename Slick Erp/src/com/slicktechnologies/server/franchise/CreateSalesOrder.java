package com.slicktechnologies.server.franchise;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GeneralServiceImpl;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
/**
 * @author Viraj
 * Date: 28-05-2019
 * Description: To automatically create sales order and deliveryNote of approved status and ProfomaInvoice in created status
 */
public class CreateSalesOrder extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6993531531704597961L;
	
	Logger logger=Logger.getLogger("CreateSalesOrder.class");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		logger.log(Level.SEVERE,"INSIDE DO GET: ");
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		logger.log(Level.SEVERE,"INSIDE DO POST: ");
		
		RegisterServiceImpl regImpl = new RegisterServiceImpl();
		regImpl.getClass();
		ServerAppUtility serverapp = new ServerAppUtility();
		
		SimpleDateFormat fmt= new SimpleDateFormat("dd/MMM/yyyy");
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		String urlCalled = req.getRequestURL().toString().trim();
		String url = urlCalled.replace("http://", "");
		String[] splitUrl = url.split("\\.");
		System.out.println("SplitURL : "+splitUrl[0]);
		logger.log(Level.SEVERE,"SplitURL : "+splitUrl[0]);
		
		String data = req.getParameter("jsonString");
		System.out.println("DATA : "+data);
		String screenData=data.trim().replace("#and", "&");
		
		Gson gson = new Gson();
		JSONObject object = null;
		try {
			object = new JSONObject(screenData.trim());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		JSONArray salesorder_array = null;
		try {
			salesorder_array = object.getJSONArray("Po_list");
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		
		Type type = new TypeToken<ArrayList<PurchaseOrder>>(){}.getType();
		ArrayList<PurchaseOrder> poList=new ArrayList<PurchaseOrder>();
		poList=gson.fromJson(salesorder_array.toString(), type);
		logger.log(Level.SEVERE,"poList to be returned::::::::::"+poList);
		
		Company comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
		logger.log(Level.SEVERE,"comp in CreateSalesOrder: "+comp);
		
		List<Customer> custList = ofy().load().type(Customer.class).filter("companyId", comp.getCompanyId()).list();
		logger.log(Level.SEVERE,"custList size in CreateSalesOrder: "+custList.size());
		
		List<ItemProduct> prodList = ofy().load().type(ItemProduct.class).filter("companyId", comp.getCompanyId()).list();
		logger.log(Level.SEVERE,"prodList size in CreateSalesOrder: "+prodList.size());
		
		SalesOrder so  = new SalesOrder();
		double total = 0.0;
		ArrayList<SalesLineItem> sItem = new ArrayList<SalesLineItem>();
		sItem.clear();
		for(int i=0;i<poList.size();i++){
			for(ProductDetailsPO pDetails: poList.get(i).getProductDetails()){
				logger.log(Level.SEVERE,"pDetails.getProductCode(): "+pDetails.getPrduct().getProductCode());
				for(ItemProduct prod: prodList){
					logger.log(Level.SEVERE,"prod.getProductCode(): "+prod.getProductCode());
					if(pDetails.getPrduct().getProductCode().equals(prod.getProductCode())) {
						SalesLineItem salesLineItem = new SalesLineItem();
						logger.log(Level.SEVERE,"Inside pcode if");
						salesLineItem.setPrduct(prod);
						salesLineItem.setProductCode(prod.getProductCode());
						salesLineItem.setProductName(prod.getProductName());
						salesLineItem.setQuantity(pDetails.getProductQuantity());
						salesLineItem.setVatTax(prod.getVatTax());
						salesLineItem.setServiceTax(prod.getServiceTax());
						salesLineItem.setVatTaxEdit(prod.getVatTax().getTaxName());
						salesLineItem.setServiceTaxEdit(prod.getServiceTax().getTaxName());
						salesLineItem.setArea(pDetails.getProductQuantity()+"");
						salesLineItem.setPrice(prod.getPrice());
						if(pDetails.getProductQuantity() != 0) {
							total += prod.getPrice() * pDetails.getProductQuantity();
						}else{
							total += prod.getPrice();
						}
						
						sItem.add(salesLineItem);
					}
				}
			}
			so.setPaymentTermsList(poList.get(i).getPaymentTermsList());
			so.setApproverName(poList.get(i).getApproverName());
			so.setEmployee(poList.get(i).getEmployee());
			so.setBranch(poList.get(i).getBranch());
			so.setShippingAddress(poList.get(i).getAdress());
			
			logger.log(Level.SEVERE,"VendorList size: "+poList.get(i).getVendorDetails().size());
			for(int j=0;j<poList.get(i).getVendorDetails().size();j++) {
				
				for(Customer c: custList) {
					logger.log(Level.SEVERE,"Customer name in customer loop: "+c.getCustomerName());
					logger.log(Level.SEVERE,"Customer name of po in customer loop: "+poList.get(i).getVendorDetails().get(j).getVendorName());
					if(poList.get(i).getVendorDetails().get(j).getVendorName().equalsIgnoreCase(c.getCustomerName())) {
						logger.log(Level.SEVERE,"Inside customer if");
						so.getCinfo().setFullName(c.getCustomerName());
						so.getCinfo().setCellNumber(c.getCellNumber1());
						so.getCinfo().setCount(c.getCount());
					}
				}
			}
			
			List<ProductOtherCharges> list = new ArrayList<ProductOtherCharges>();
			List<ProductOtherCharges> taxList = new ArrayList<ProductOtherCharges>();
			try {
				list.addAll(serverapp.addProdTaxes(sItem , taxList , AppConstants.SALESORDER));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			ArrayList<ProductOtherCharges> arrBillTax=new ArrayList<ProductOtherCharges>();
			double assessValue=0;
			double tax = 0.0;
			for(int j=0;j<list.size();j++){
				ProductOtherCharges taxDetails=new ProductOtherCharges();
				taxDetails.setChargeName(list.get(j).getChargeName());
				taxDetails.setChargePercent(list.get(j).getChargePercent());
				assessValue=list.get(j).getAssessableAmount();//*paymentRecieved/100;
				taxDetails.setAssessableAmount(assessValue);				
				taxDetails.setIndexCheck(list.get(j).getIndexCheck());
				tax = tax + taxDetails.getAssessableAmount()*taxDetails.getChargePercent()/100;
				arrBillTax.add(taxDetails);
			}
			double netPayable = Math.round(total+tax);
			so.setProductTaxes(arrBillTax);
			so.setTotalFinalAmount(Math.round(total+tax));
			so.setNetpayable(netPayable);
			so.setTotalAmount(total);
		}
		
		so.setCompanyId(comp.getCompanyId());
		logger.log(Level.SEVERE,"CompanyID in CreateSalesOrder: "+comp.getCompanyId());
		so.setSalesOrderDate(new Date());
		so.setItems(sItem);
		logger.log(Level.SEVERE,"sItem in CreateSalesOrder: "+sItem.size());
		SuperModel sup = (SuperModel) so;
		GeneralServiceImpl genImpl = new GeneralServiceImpl();
		String count = genImpl.saveAndApproveDocument(sup, "", "", AppConstants.SALESORDER);
		int salesCount = Integer.parseInt(count);
		logger.log(Level.SEVERE,"salesCount in CreateSalesOrder: "+salesCount);
		
		BillingDocument billDoc = ofy().load().type(BillingDocument.class).filter("contractCount", salesCount).first().now();
		billDoc.setStatus(BillingDocument.APPROVED);
		
		GenricServiceImpl gImpl = new GenricServiceImpl();
		gImpl.save(billDoc);
		logger.log(Level.SEVERE,"billDoc in CreateSalesOrder: "+billDoc.getCount());
		
		createProformaInviove(billDoc);
	}

	private void createProformaInviove(BillingDocument billDoc) {
		
		String invoiceType=AppConstants.CREATEPROFORMAINVOICE;
		try {
			createNewInvoice(invoiceType,billDoc);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private void createNewInvoice(String invoiceType, BillingDocument billDoc) {
		Invoice invo =  new Invoice();
		
		PersonInfo pinfo = new PersonInfo();
		
		pinfo.setCount(billDoc.getPersonInfo().getCount());
		pinfo.setCellNumber(billDoc.getPersonInfo().getCellNumber());
		pinfo.setFullName(billDoc.getPersonInfo().getFullName());
		
		invo.setPersonInfo(pinfo);
		invo.setContractCount(billDoc.getContractCount());
		invo.setTotalSalesAmount(billDoc.getTotalSalesAmount());
		invo.setAccountType(AppConstants.ACCOUNTTYPE);
		invo.setInvoiceType(invoiceType);
		invo.setStatus(AppConstants.PROFORMAINVOICE);
		
		BillingDocumentDetails billDocDetails = new BillingDocumentDetails();
		List<BillingDocumentDetails> billtablelis = new ArrayList<BillingDocumentDetails>();
		
		billDocDetails.setOrderId(billDoc.getBillingDocumentInfo().getOrderId());
		billDocDetails.setBillId(billDoc.getBillingDocumentInfo().getBillId());
		billDocDetails.setBillingDate(billDoc.getBillingDocumentInfo().getBillingDate());
		billDocDetails.setBillAmount(billDoc.getBillingDocumentInfo().getBillAmount());
		billtablelis.add(billDocDetails);
		
		invo.setArrayBillingDocument(billtablelis);
		invo.setSalesOrderProducts(billDoc.getSalesOrderProducts());
		invo.setTotalAmtExcludingTax(billDoc.getTotalAmount());
		invo.setDiscountAmt(billDoc.getDiscountAmt());
		invo.setFinalTotalAmt(billDoc.getFinalTotalAmt());
		invo.setTotalAmtIncludingTax(billDoc.getTotalAmtIncludingTax());
		invo.setTotalBillingAmount(billDoc.getTotalBillingAmount());
		invo.setDiscount(billDoc.getRoundOffAmt());
		
		invo.setPaymentDate(billDoc.getPaymentDate());
		invo.setApproverName(billDoc.getApproverName());
		invo.setEmployee(billDoc.getEmployee());
		invo.setBranch(billDoc.getBranch());
		invo.setInvoiceDate(billDoc.getInvoiceDate());
		invo.setCompanyId(billDoc.getCompanyId());
		invo.setBillingTaxes(billDoc.getBillingTaxes());
		invo.setBillingOtherCharges(billDoc.getBillingOtherCharges());
		invo.setArrPayTerms(billDoc.getArrPayTerms());
		
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(invo);
		
		logger.log(Level.SEVERE,"invoice after save: "+invo.getCount());
	}

}
