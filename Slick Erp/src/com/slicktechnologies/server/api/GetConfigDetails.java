package com.slicktechnologies.server.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.JSONArray;

import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;

public class GetConfigDetails extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3089309450792166706L;
	
	
	Logger logger = Logger.getLogger("Logger");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		resp.setHeader("Access-Control-Allow-Origin", "*");

		String urlCalled = req.getRequestURL().toString().trim();
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl::" + splitUrl);
		logger.log(Level.SEVERE, "splitUrl[0] " + splitUrl[0]);

		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		
		long companyId = comp.getCompanyId();
		logger.log(Level.SEVERE, "companyId"+companyId);

		ServerAppUtility apputility = new ServerAppUtility();
	
		String strinternalType = "";
		int internalType=0;
		try {
			strinternalType = req.getParameter("internalType");
			internalType = Integer.parseInt(strinternalType);
		} catch (Exception e) {
			
		}
		
		String authcode="";
		long authcompanyId=0;
		try {
			authcode = req.getParameter("authcode");
			authcode = apputility.decodeText(authcode);
			logger.log(Level.SEVERE, "authcode"+authcode);
			authcompanyId = Long.parseLong(authcode);
		} catch (Exception e) {
			
		}
		
		JSONObject jsonobjmsg = new JSONObject();

		if(strinternalType.equals("")){
			logger.log(Level.SEVERE,"Internal type can not be blank");
			resp.getWriter().println(getMessageinJson(jsonobjmsg,"Internal type can not be blank"));
//			resp.getWriter().println("");
		}
		
		if(authcode.equals("")){
//			resp.getWriter().println("");
//			getMessageinJson(jsonobjmsg,"Auth code can not be blank");
			logger.log(Level.SEVERE,"Auth code can not be blank");
			resp.getWriter().println(getMessageinJson(jsonobjmsg,"Auth code can not be blank"));

		}
		//Removing validation as per Vijay sir's instruction Date:28-12-2022
//		if(authcompanyId!=0 && companyId!=authcompanyId){
////			resp.getWriter().println();
//			logger.log(Level.SEVERE,"Authentication failed");
//			resp.getWriter().println(getMessageinJson(jsonobjmsg,"Authentication failed"));
//
//		}
//		else{
			List<Config> configEntitylist = ofy().load().type(Config.class).filter("companyId", companyId).filter("type", internalType)
									.filter("status", true).list();
			logger.log(Level.SEVERE, "configEntitylist size "+configEntitylist.size());
			
			JSONObject jsonobj = new JSONObject();

			JSONArray dataarray = new JSONArray();
			if(configEntitylist!=null && configEntitylist.size()>0){
				for(Config configentity : configEntitylist){
					JSONObject obj = new JSONObject();
					try {
						obj.put("Name", configentity.getName());
					} catch (JSONException e) {
						e.printStackTrace();
					}
					dataarray.add(obj);

				}
				try {
					jsonobj.put("Data", dataarray);
				} catch (JSONException e) {
					e.printStackTrace();
				}

				String jsonstring = jsonobj.toString().replaceAll("\\\\", "");
				logger.log(Level.SEVERE, "Json"+jsonstring);
				resp.getWriter().println(jsonstring);
			}
			else{
//				resp.getWriter().println();
				resp.getWriter().println(getMessageinJson(jsonobjmsg,"No data found"));

			}
//		}
	}	

	
	private String getMessageinJson(JSONObject msgjsonobj, String msg) {
		
		try {
			msgjsonobj.put("message", msg);
		} catch (JSONException e) {
		}
		
		String jsonstring = msgjsonobj.toString().replaceAll("\\\\", "");
		
		return jsonstring;
		
	}

}
