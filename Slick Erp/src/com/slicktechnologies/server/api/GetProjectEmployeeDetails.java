package com.slicktechnologies.server.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class GetProjectEmployeeDetails extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1828552158615642021L;
	
	Logger logger = Logger.getLogger("Logger");
	SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		resp.setHeader("Access-Control-Allow-Origin", "*");
		dateformat.setTimeZone(TimeZone.getTimeZone("IST"));

		String urlCalled = req.getRequestURL().toString().trim();
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl[0]" + splitUrl[0]);
		
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		
		String strcustomerId = req.getParameter("customerId").trim();
		
		if(strcustomerId==null || strcustomerId.equals("")) {
			resp.getWriter().println("Customer Id can not be blank");
		}
		else if(strcustomerId.equals("0")) {
			resp.getWriter().println("Customer Id can not be zero");
		}
		else {
			int customerId = 0;
			logger.log(Level.SEVERE, "strcustomerId"+strcustomerId);
			boolean checkerflag=false;
			try {
				customerId = Integer.parseInt(strcustomerId);
			} catch (Exception e) {
				e.printStackTrace();
				checkerflag = true;
			}
			
			if(checkerflag) {
				resp.getWriter().println("Customer Id is not valid");
			}
			else {
				Customer customer = ofy().load().type(Customer.class).filter("count", customerId).filter("companyId", comp.getCompanyId()).first().now();
				if(customer==null) {
					resp.getWriter().println("Customer Id does not exist");
				}
				else {
					List<HrProject> projectlist = ofy().load().type(HrProject.class).filter("companyId", comp.getCompanyId()).filter("personinfo.count", customerId)
												  .filter("status", true).list();
					logger.log(Level.SEVERE, "projectlist size "+projectlist.size());
					if(projectlist.size()==0) {
						resp.getWriter().println("Project does not exist for customer id - "+customerId);
					}
					else {
						
						ArrayList<String> projectNamelist = new ArrayList<String>(); 
						for(HrProject hrproject : projectlist) {
							projectNamelist.add(hrproject.getProjectName());
						}
						List<Employee> employeeList = ofy().load().type(Employee.class).filter("projectName IN", projectNamelist).
										filter("companyId", comp.getCompanyId()).filter("status", true).list();
								
						JSONObject mainjsonobj = new JSONObject();

						JSONArray projectjsonarray = new JSONArray();
						for(HrProject hrproject : projectlist) {
							
							JSONObject jsonobj = new JSONObject();
							try {
								jsonobj.put("projectName", hrproject.getProjectName());
								jsonobj.put("projectId", hrproject.getCount());
								if(hrproject.getFromDate()!=null) {
									jsonobj.put("fromDate", dateformat.format(hrproject.getFromDate()));
								}
								else {
									jsonobj.put("fromDate", "");
								}
								if(hrproject.getFromDate()!=null) {
									jsonobj.put("toDate", dateformat.format(hrproject.getToDate()));
								}
								else {
									jsonobj.put("toDate", "");
								}
								jsonobj.put("branch", hrproject.getBranch());

							} catch (JSONException e) {
								e.printStackTrace();
							}

							List<Employee> projectemployeelist = getEmployee(employeeList,hrproject.getProjectName());
							JSONArray employeejsonarray = new JSONArray();

							for(Employee employee : projectemployeelist) {
								JSONObject jobj = new JSONObject();
								try {
									jobj.put("employeeId", employee.getCount());
									jobj.put("employeeName", employee.getFullname());
									if(employee.getProjectName()!=null) {
										jobj.put("projectName", employee.getProjectName());
									}
									else {
										jobj.put("projectName", "");
									}
									jobj.put("employeeBranch", employee.getBranchName());
									
									employeejsonarray.put(jobj);

								} catch (Exception e) {
									e.printStackTrace();
								}
								
							}
							
							try {
								jsonobj.put("projectEmployeeDetails", employeejsonarray);
								projectjsonarray.put(jsonobj);
								
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						
						try {
							mainjsonobj.put("data", projectjsonarray);
							
							String jsonstring = mainjsonobj.toString().replaceAll("\\\\", "");
							logger.log(Level.SEVERE, "Json "+jsonstring);
							resp.getWriter().println(jsonstring);
							
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				
				}
				
				
			}
		}
	}

	private List<Employee> getEmployee(List<Employee> employeeList, String projectName) {
		List<Employee> list = new ArrayList<Employee>();
		for(Employee employee : employeeList) {
			if(employee.getProjectName().trim().equals(projectName.trim())) {
				list.add(employee);
			}
		}
		return list;
	}

}
