package com.slicktechnologies.server.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.JSONArray;

import com.google.gson.Gson;
import com.slicktechnologies.server.android.analytics.AnalyticsOperations;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;

public class ContractRenewalAPI extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -317423770248201681L;

	Logger logger = Logger.getLogger("Logger");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		resp.setHeader("Access-Control-Allow-Origin", "*");

		String urlCalled = req.getRequestURL().toString().trim();
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl::" + splitUrl);
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		
		long companyId = comp.getCompanyId();
		ServerAppUtility utility = new ServerAppUtility();
		
		String fromDateStr="";
		try {
			fromDateStr = req.getParameter("fromDate");
		} catch (Exception e) {
			fromDateStr="";
		}
		String toDateStr="";
		try {
			toDateStr = req.getParameter("toDate");
		} catch (Exception e) {
			toDateStr="";
		}
		String customerCellNo = "";
		try {
			customerCellNo = req.getParameter("customerCellNo");
		} catch (Exception e) {
			// TODO: handle exception
		}
		String customerEmailId = "";
		try {
			customerEmailId = req.getParameter("customerEmail");
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		AnalyticsOperations analytics = new AnalyticsOperations();
		
		String strcustomer = analytics.validateCustomerCelllNoEmailId(customerCellNo,customerEmailId); 
		Customer customer =null;
		ServerAppUtility apputility = new ServerAppUtility();

		if(strcustomer.equals("")){
			customer = loadCustomer(customerCellNo,customerEmailId,comp.getCompanyId());
			Date fromDate = null;
			Date todate = null;
			try {
				 fromDate = sdf.parse(fromDateStr);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				 todate = sdf.parse(toDateStr);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "comp.getCompanyId()"+comp.getCompanyId());
			logger.log(Level.SEVERE, "customer.getCount()"+customer.getCount());

			List<ContractRenewal> contractRenewallist = ofy().load().type(ContractRenewal.class).filter("companyId", comp.getCompanyId())
								.filter("status", "Processed").filter("customerId", customer.getCount()).list();
			logger.log(Level.SEVERE, "contractRenewallist size "+contractRenewallist.size());
			
			List<ContractRenewal> updatedcontractrenewallist = new ArrayList<ContractRenewal>();
			List<Integer> contractidlist = new ArrayList<Integer>();
			for(ContractRenewal contractRenewal : contractRenewallist){
				if((contractRenewal.getContractEndDate().after(fromDate) && contractRenewal.getContractEndDate().before(todate)) || (contractRenewal.getContractEndDate().equals(fromDate) || contractRenewal.getContractEndDate().equals(todate)) ){
					updatedcontractrenewallist.add(contractRenewal);
					contractidlist.add(contractRenewal.getContractId());
				}
			}
			logger.log(Level.SEVERE, "updatedcontractrenewallist size "+updatedcontractrenewallist.size());

			JSONArray jArray = new JSONArray();
			JSONObject jObj = new JSONObject();

			if(updatedcontractrenewallist.size()==0) {
				
				JSONObject obj = new JSONObject();
				try {
					obj.put("Message", "No data found");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				jArray.add(obj);
				
				String jsonStr = jArray.toString().replaceAll("\\\\", "");
				logger.log(Level.SEVERE, "No data Json"+jsonStr);
				resp.getWriter().println(jsonStr);
			}
			else {
				
				List<Contract> cotractlist = ofy().load().type(Contract.class).filter("companyId", comp.getCompanyId()).filter("count IN", contractidlist).list();
				
				//Ashwini Patil Date:19-06-2023
					if(updatedcontractrenewallist!=null&&updatedcontractrenewallist.size()>1) {
						Comparator<ContractRenewal> dateComp=new Comparator<ContractRenewal>() {
							@Override
							public int compare(ContractRenewal arg0, ContractRenewal arg1) {
								return arg0.getContractEndDate().compareTo(arg1.getContractEndDate());
							}
						};
						Collections.sort(updatedcontractrenewallist, dateComp);								
					}
				
				
				Gson gson = new Gson();
				for(ContractRenewal contractrenewal : updatedcontractrenewallist){
					JSONObject jsonobj = new JSONObject();
					try {
						jsonobj.put("CustomerName", contractrenewal.getCustomerName());
						jsonobj.put("contractId", contractrenewal.getContractId());
						jsonobj.put("contractDate", sdf.format(contractrenewal.getContractDate()));
						jsonobj.put("contractEndDate", sdf.format(contractrenewal.getContractEndDate()));
						jsonobj.put("netPayable", contractrenewal.getNetPayable());
						jsonobj.put("branch", contractrenewal.getBranch());
						Date contractStartdDate = getContractStartDate(contractrenewal.getContractId(), cotractlist);
						if(contractStartdDate!=null) {
							jsonobj.put("contractStartDate", sdf.format(contractStartdDate));
						}
						else {
							jsonobj.put("contractStartDate", "");
						}
						
						JSONArray serviceProductarrayJson = new JSONArray();
						List<SalesLineItem> serviceProductList =contractrenewal.getItems();
						for (int j = 0; j < serviceProductList.size(); j++) {
							JSONObject jobj = new JSONObject();
							jobj.put("id", serviceProductList.get(j).getPrduct().getCount());
							String productName=serviceProductList.get(j).getPrduct().getProductName();

//							if (productName.contains("\"")) {
//								errorObj = new JSONObject();
//								errorObj.put("Message","Remove double quote from product name. Contract id:"+ contractList.get(i).getCount()+". Contact system administrator.");
//								String jsonString = errorObj.toJSONString();
//								logger.log(Level.SEVERE,"error message sent for product name "+ productName+ " Contract id:"+contractList.get(i).getCount());
//								return jsonString;
//							}
							jobj.put("productName",productName);
							jobj.put("price", serviceProductList.get(j).getPrduct().getPrice());
							jobj.put("noOfServices", serviceProductList.get(j).getNumberOfServices());
							jobj.put("productType",serviceProductList.get(j).getPrduct().getProductType());

//							if (serviceProductList.get(j).getSpecification()!=null&&serviceProductList.get(j).getSpecification().contains("\"")) {
//								errorObj = new JSONObject();
//								errorObj.put("Message","Remove double quote from product specification. Contract id:"+ contractList.get(i).getCount()+". Contact system administrator.");
//								String jsonString = errorObj.toJSONString();
//								logger.log(Level.SEVERE,"error message sent for product specification "+ serviceProductList.get(j).getSpecification()+ " Contract id:"+contractList.get(i).getCount());
//								return jsonString;
//							}						
							jobj.put("specification",serviceProductList.get(j).getSpecification());
							
							jobj.put("specifications",serviceProductList.get(j).getSpecification());						
							jobj.put("duration",serviceProductList.get(j).getDuration());
							if(serviceProductList.get(j).getProductCategory()!=null)
								jobj.put("productCategory",serviceProductList.get(j).getProductCategory());
							else
								jobj.put("productCategory","");

							serviceProductarrayJson.add(jobj);
						}
						jsonobj.put("productList",serviceProductarrayJson);
						jArray.add(jsonobj);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
//				try {
//					jObj.put("data", jArray);
//				} catch (JSONException e) {
//					e.printStackTrace();
//				}
				
				String jsonStr = jArray.toString().replaceAll("\\\\", "");

//				String jsonstring = jObj.toString().replaceAll("\\\\", "");
				logger.log(Level.SEVERE, "Json"+jsonStr);
				resp.getWriter().println(jsonStr);
				
			}
			
		}
		else{
			 apputility.getMessageInJson("Customer cell no or email id does not exist or not match in ERP system!");
		}
	}
	
	
	
	private Date getContractStartDate(int contractId, List<Contract> cotractlist) {
		for(Contract contractEntity : cotractlist) {
			if(contractEntity.getCount() == contractId) {
				return contractEntity.getStartDate();
			}
		}
		return null;
	}

	public Customer loadCustomer(String customerCellNo, String customerEmailId, Long companyId) {
		logger.log(Level.SEVERE, "customerCellNo "+customerCellNo);
		logger.log(Level.SEVERE, "customerEmailId "+customerEmailId);
		long customerCellNumber = 0;
		try {
			customerCellNumber = Long.parseLong(customerCellNo);
			logger.log(Level.SEVERE, "customerCellNumber "+customerCellNumber);

		} catch (Exception e) {
			// TODO: handle exception
		}
		
		Customer customer = null;
		try {
			if(customerCellNo!=null && !customerCellNo.equals("")){
				customer = ofy().load().type(Customer.class).filter("companyId", companyId).filter("contacts.cellNo1", customerCellNumber).first().now();
				logger.log(Level.SEVERE, "customer "+customer.getCount());
			}
			else if(customerEmailId!=null && !customerEmailId.equals("")){
				customer = ofy().load().type(Customer.class).filter("companyId", companyId).filter("contacts.email", customerEmailId).first().now();
				logger.log(Level.SEVERE, "customer = "+customer.getCount());
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return customer;
	}
	
}
