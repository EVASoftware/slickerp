package com.slicktechnologies.server.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class CustomerDetails extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8539236241972223729L;

	Logger logger = Logger.getLogger("Logger");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		resp.setHeader("Access-Control-Allow-Origin", "*");

		String urlCalled = req.getRequestURL().toString().trim();
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl::" + splitUrl);
		
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		
		long companyId = comp.getCompanyId();
		ServerAppUtility utility = new ServerAppUtility();
		
//		String authcode = utility.encodeText(companyId+"");
		String authcode = companyId+"";
		String applicationName = "";
		
			String strcustomerNo = "";
			long customerCellNo = 0;
			try {
				strcustomerNo = req.getParameter("customerCellNo");
				customerCellNo = Long.parseLong(strcustomerNo);
			} catch (Exception e) {
				
			}
			
			String strcustomerEmail = "";
			try {
				strcustomerEmail = req.getParameter("customerEmail");
			} catch (Exception e) {
				
			}
			try {
				applicationName = req.getParameter("applicationName");
			}catch (Exception e) {
				
			}
			
			if(applicationName!=null&&applicationName.equals(AppConstants.CUSTOMERPORTAL) ) {
				GetUserRegistrationOtp api=new GetUserRegistrationOtp();
				String result=api.validateLicense(comp,AppConstants.LICENSETYPELIST.CUSTOMERPORTAL);
				if(!result.equalsIgnoreCase("success")) {
					resp.getWriter().println(result);
					return;
				}
			}

			Customer customerEntity = null;
			if(strcustomerNo!=null && !strcustomerNo.equals("")){
				logger.log(Level.SEVERE, "customerCellNo"+customerCellNo);
				customerEntity = ofy().load().type(Customer.class).filter("companyId", companyId).filter("contacts.cellNo1", customerCellNo).first().now();
			
			}
			else if(strcustomerEmail!=null && !strcustomerEmail.equals("")){
				logger.log(Level.SEVERE, "strcustomerEmail"+strcustomerEmail);
				customerEntity = ofy().load().type(Customer.class).filter("companyId", companyId).filter("contacts.email", strcustomerEmail).first().now();
			}
			else{
				resp.getWriter().println(utility.getMessageInJson("Please send customer cell no or email id"));
			}

			if(customerEntity==null){
				logger.log(Level.SEVERE, "customerEntity"+customerEntity);
				resp.getWriter().println(utility.getMessageInJson("Customer does not exist!"));
				return;
			}else if(applicationName!=null&&applicationName.equals(AppConstants.CUSTOMERPORTAL)){
				if(customerEntity.isDisableCustomerPortal()) {
					Branch branchDt=null;
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
					
						logger.log(Level.SEVERE,"Process active --");
						if(customerEntity.getBranch() != null && customerEntity.getBranch().trim().length()>0){
							branchDt = ofy().load().type(Branch.class).filter("companyId",customerEntity.getCompanyId()).filter("buisnessUnitName", customerEntity.getBranch()).first().now();
						}
					}
					if(branchDt!=null) {
						resp.getWriter().println("Customer Portal has not been enabled for you. Please contact service provider. "+branchDt.getCellNumber1());
						return;
						
					}else {
						resp.getWriter().println("Customer Portal has not been enabled for you. Please contact service provider. "+comp.getCellNumber1());
						return;				
					}
					
				}						
			}
			
			JSONObject jsonobj = new JSONObject();
			try {
				jsonobj.put("customerId", customerEntity.getCount()+"");
				
				if(customerEntity.isCompany()){
					jsonobj.put("customerName", customerEntity.getCompanyName());
				}
				else{
					jsonobj.put("customerName", customerEntity.getFullname());
				}
				jsonobj.put("customerCellNo", customerEntity.getCellNumber1()+"");
				
				if(customerEntity.getEmail()!=null)
					jsonobj.put("customerEmail", customerEntity.getEmail());
				else
					jsonobj.put("customerEmail", "");
				
				jsonobj.put("address", customerEntity.getAdress().getCompleteAddress());
				if(customerEntity.getArticleTypeDetails()!=null && customerEntity.getArticleTypeDetails().size()>0){
					for(ArticleType articleType : customerEntity.getArticleTypeDetails()){
						if(articleType.getArticleTypeName().equalsIgnoreCase("GSTIN")){
							jsonobj.put("customerGSTNo",articleType.getArticleTypeValue());
						}
					}
				}
				else{
					jsonobj.put("customerGSTNo","");

				}
				
				Company company = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
				if(company!=null) {
					if(company.getWebsite()!=null)
						jsonobj.put("companyWebsite", company.getWebsite());
					else
						jsonobj.put("companyWebsite", "");
					
					
					if(company.getMerchantId()!=null&&!company.getMerchantId().equals("")&&company.getPaymentGatewayId()!=null&&!company.getPaymentGatewayId().equals("")&&company.getPaymentGatewayKey()!=null&&!company.getPaymentGatewayKey().equals("")) {
						jsonobj.put("isPaymentGatewayEnabled", "yes");
					}else
						jsonobj.put("isPaymentGatewayEnabled", "no");

				}
				
				if(customerEntity.getBranch()!=null)
					jsonobj.put("branch", customerEntity.getBranch());
				else
					jsonobj.put("branch", "");
				
				jsonobj.put("companyId", authcode);

				if(customerEntity.getDocumentLink()!=null&&!customerEntity.getDocumentLink().equals("")) {
					jsonobj.put("documentLink", customerEntity.getDocumentLink());
				}else
					jsonobj.put("documentLink", "");
				
				String jsonstring ="";
				boolean reportExistFlag=false;
				if(customerEntity.getReportLink()!=null&&!customerEntity.getReportLink().equals("")) {
					logger.log(Level.SEVERE, "report link before escape "+customerEntity.getReportLink());
					String escapedString = customerEntity.getReportLink().replace("\"", "\\\"");
					jsonobj.put("reportLink", escapedString);
					logger.log(Level.SEVERE, "report link after escape "+escapedString);
					reportExistFlag=true;
				}
				logger.log(Level.SEVERE, "fetching 2nd report link");
				
				if(customerEntity.getReport2Link()!=null&&!customerEntity.getReport2Link().equals("")) {
					logger.log(Level.SEVERE, "report2 link before escape "+customerEntity.getReport2Link());
					String escapedString = customerEntity.getReport2Link().replace("\"", "\\\"");
					jsonobj.put("report2Link", escapedString);
					logger.log(Level.SEVERE, "report2 link after escape "+escapedString);
					reportExistFlag=true;
				}
				
				
				if(reportExistFlag) {
					jsonstring=jsonobj.toString();
				}else {
					jsonobj.put("reportLink", "");
					jsonstring=jsonobj.toString().replaceAll("\\\\", "");
					
				}
				
				
				
				logger.log(Level.SEVERE, "Json"+jsonstring);
				resp.getWriter().println(jsonstring);
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
	}
	
}
