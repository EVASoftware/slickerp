package com.slicktechnologies.server.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.LicenseDetails;
import com.slicktechnologies.shared.common.email.EmailDetails;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class GetUserRegistrationOtp extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5135680226167800049L;

	

	Logger logger = Logger.getLogger("Logger");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		resp.setHeader("Access-Control-Allow-Origin", "*");

		String urlCalled = req.getRequestURL().toString().trim();
		logger.log(Level.SEVERE, "urlCalled " + urlCalled );
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl::" + splitUrl[0]);
		
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		long companyId = comp.getCompanyId();


		String strmobileNo = "";
		long mobileNo = 0;
		try {
			strmobileNo = req.getParameter("mobileNo");
			mobileNo = Long.parseLong(strmobileNo);
		} catch (Exception e) {
			
		}
		
		String emailId = "";
		try {
			emailId = req.getParameter("emailId");
		} catch (Exception e) {
			
		}
		String applicationName = req.getParameter("applicationName");
		
		String validationMsg ="";
		
		if(applicationName==null || applicationName.equals("")) {
			validationMsg += " Application name can not be blank."; 
		}
		
		String otpnumber = "";
		try {
			otpnumber = req.getParameter("OTP");
		} catch (Exception e) {
		}
		
//		if(applicationName.equals(AppConstants.CUSTOMERPORTAL) ) {
//			String strcompanyId = req.getParameter("authCode");
//			ServerAppUtility apputility = new ServerAppUtility();
//			boolean authonticationflag = apputility.validateAuthCode(strcompanyId,companyId);
//			logger.log(Level.SEVERE, "authonticationflag"+authonticationflag);
//			if(!authonticationflag) {
//				resp.getWriter().println("Authentication Failed!");
//				return;
//			}
//		}
		
		if(applicationName.equals(AppConstants.PEDIO) || applicationName.equals(AppConstants.PRIORA)) {
			Employee employee = null;
			
			if(strmobileNo!=null && !strmobileNo.equals("")){
				employee = ofy().load().type(Employee.class).filter("companyId", companyId).filter("contacts.cellNo1", mobileNo).first().now();
				if(employee==null) {
					validationMsg += " Mobile number not registered, contact your system administrator.";
				}
			}
			else if(emailId!=null && !emailId.equals("")){
				employee = ofy().load().type(Employee.class).filter("companyId", companyId).filter("contacts.email", emailId).first().now();
				if(employee==null) {
					validationMsg += " Email id not registered, contact your system administrator.";
				}
			}
			else {
				validationMsg += " Please send mobile number or email id."; 
			}
			
		}
		else if(applicationName.equals(AppConstants.CUSTOMERPORTAL) ) {
			
			
			Customer customerEntity = null;
			String checkLicense=validateLicense(comp,AppConstants.LICENSETYPELIST.CUSTOMERPORTAL);
			if(!checkLicense.equalsIgnoreCase("success")) {
				validationMsg +=checkLicense;
			}
			else if(strmobileNo!=null && !strmobileNo.equals("")){
				customerEntity = ofy().load().type(Customer.class).filter("companyId", companyId).filter("contacts.cellNo1", mobileNo).first().now();
				if(customerEntity!=null) {
					if(customerEntity.isDisableCustomerPortal()) {
						Branch branchDt=null;
						if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
						
							logger.log(Level.SEVERE,"Process active --");
							if(customerEntity.getBranch() != null && customerEntity.getBranch().trim().length()>0){
								branchDt = ofy().load().type(Branch.class).filter("companyId",customerEntity.getCompanyId()).filter("buisnessUnitName", customerEntity.getBranch()).first().now();
							}
						}
						if(branchDt!=null)
							validationMsg += "Customer Portal has not been enabled for you. Please contact service provider."+branchDt.getCellNumber1();		
						else
							validationMsg += "Customer Portal has not been enabled for you. Please contact service provider. "+comp.getCellNumber1();					
						
					}
				}
				else {
					validationMsg += " Mobile number not registered, contact your system administrator.";
				}						
			}
			else if(emailId!=null && !emailId.equals("")){
				customerEntity = ofy().load().type(Customer.class).filter("companyId", companyId).filter("contacts.email", emailId).first().now();
				if(customerEntity!=null) {
					if(customerEntity.isDisableCustomerPortal()) {
						Branch branchDt=null;
						if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
						
							logger.log(Level.SEVERE,"Process active --");
							if(customerEntity.getBranch() != null && customerEntity.getBranch().trim().length()>0){
								branchDt = ofy().load().type(Branch.class).filter("companyId",customerEntity.getCompanyId()).filter("buisnessUnitName", customerEntity.getBranch()).first().now();
							}
						}
						if(branchDt!=null)
							validationMsg += "Customer Portal has not been enabled for you. Please contact service provider."+branchDt.getCellNumber1();		
						else
							validationMsg += "Customer Portal has not been enabled for you. Please contact service provider. "+comp.getCellNumber1();					
						
					}
				}
				else {
					validationMsg += " Email id not registered, contact your system administrator.";
				}
				if(comp.getEmail()==null || comp.getEmail().equals("")) {
					validationMsg += " Company email id does not exist in company master in the ERP."; 			

				}
			}
			else{
				validationMsg += "Please send mobile number or email id."; 			
			}
		}
		else {
			validationMsg += "Application name does not exist."; 
		}
		
		logger.log(Level.SEVERE, "validationMsg"+validationMsg+"msg done here");
		
		if(validationMsg.equals("")) {
			SmsTemplate smstemplateEntity = ofy().load().type(SmsTemplate.class).filter("event", AppConstants.USERAPPREGISTRATIONOTP).filter("companyId", companyId).first().now();
			if(smstemplateEntity==null) {
				resp.getWriter().println("User Resgistration OTP template does no exist communication template!");
			}
			else if(!smstemplateEntity.getStatus()) {
				resp.getWriter().println("User Resgistration OTP template status is inactive!");

			}
			else {
				String smsTemplate = smstemplateEntity.getMessage();
				
				String companyName =smsTemplate.replace("{CompanyName}", comp.getBusinessUnitName());
				String message = companyName.replace("{OTP}", otpnumber);
				logger.log(Level.SEVERE, "message "+message);
				if(strmobileNo!=null && !strmobileNo.equals("")){
					SmsServiceImpl smsserviceimpl = new SmsServiceImpl();
					smsserviceimpl.sendSmsToClient(message, strmobileNo, companyId, AppConstants.SMS);
					
					smsserviceimpl.sendMessageOnWhatsApp(companyId, strmobileNo, message);

					resp.getWriter().println("OTP sent successfully!");

				}
				if(emailId!=null && !emailId.equals("")) {
					Email email = new Email();
					
					try {
						
						String resultString = message.replaceAll("[\n]", "<br>");
						String resultString1 = resultString.replaceAll("[\\s]", "&nbsp;");
						logger.log(Level.SEVERE, "message resultString1 "+resultString1);

						EmailDetails emaildetails = new EmailDetails();
						
						ArrayList<String> toemailidlist = new ArrayList<String>();
						toemailidlist.add(emailId);
						
						emaildetails.setSubject("Registration OTP");
						emaildetails.setToEmailId(toemailidlist);
						emaildetails.setFromEmailid(comp.getEmail());
						emaildetails.setEmailBody(resultString1);
						
						
						
						email.sendEmailToReceipient(emaildetails, companyId);
						
						resp.getWriter().println("OTP sent successfully!");

						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					
				}

				
			}
		}
		else {
			resp.getWriter().println(validationMsg);
		}
		
		
	}
	
	public String validateLicense(Company c,String licenceCode) {

		if(c!=null) {
			String returnValue = "Success";
			Date todaydate = new Date();
			ArrayList<LicenseDetails> licenseDetailsList = new ArrayList<LicenseDetails>();
			if (c.getLicenseDetailsList() != null&& c.getLicenseDetailsList().size() != 0) {
				logger.log(Level.SEVERE, "TOTAL EVA LICENSE LIST : "+ c.getLicenseDetailsList().size());
				// Console.log("TOTAL EVA LICENSE LIST : "+c.getLicenseDetailsList().size());
				for (LicenseDetails license : c.getLicenseDetailsList()) {
						if (licenceCode.equalsIgnoreCase(license.getLicenseType())
								&& license.getStatus() == true) {
							licenseDetailsList.add(license);
						}				

				}
				logger.log(Level.SEVERE, "ACTIVE "+licenceCode+" LICENSE LIST SIZE : "+ licenseDetailsList.size());
			}
			else{
				return "No licenses are allocated to you contact support. "+c.getCellNumber1();
			}


			int noOfLicense = 0;
			int expiredLicenseCount = 0;
			int notActiveLicenseCount = 0;
			boolean validLicense = false;
			if (licenseDetailsList.size() != 0) {
				for (LicenseDetails license : licenseDetailsList) {
					logger.log(Level.SEVERE, license.getLicenseType()
							+ " No. Of License : " + license.getNoOfLicense()
							+ " LICENSE START DATE : " + license.getStartDate()
							+ " LICENSE END DATE : " + (license.getEndDate()));
					
					SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
					logger.log(Level.SEVERE,"Today="+sdf.format(todaydate)+"license start day="+sdf.format(license.getStartDate()));
					//Ashwini Patil Date:20-12-2022 added one more condition so that license should work from the currect day
					if (todaydate.after(license.getStartDate())||sdf.format(todaydate).equals(sdf.format(license.getStartDate()))) {
						if (todaydate.before(license.getEndDate())) {
							noOfLicense = noOfLicense + license.getNoOfLicense();
							validLicense = true;
						} else {
							expiredLicenseCount++;
						}
					} else {
						notActiveLicenseCount++;
					}
				}

				// Console.log("TOTAL NO. OF ACTIVE LICENSE : "+noOfLicense);
				logger.log(Level.SEVERE, "TOTAL NO. OF ACTIVE LICENSE : "
						+ noOfLicense);
				c.setNoOfUser(noOfLicense);
				if (validLicense) {
					return "Success";
				} else if (validLicense == false && notActiveLicenseCount > 0) {
					returnValue = licenceCode+" license is not active. Please Contact your service provider."+c.getCellNumber1();
				} else if (validLicense == false && expiredLicenseCount > 0) {
					returnValue = licenceCode+" license has expired. Please Contact your service provider. "+c.getCellNumber1();
				}
			}
			else{
				return licenceCode+" is not enabled. Please Contact your service provider. "+c.getCellNumber1();
			}
			return returnValue;
		
		}else {
			return "Company not found";
		}
	}
	
}
