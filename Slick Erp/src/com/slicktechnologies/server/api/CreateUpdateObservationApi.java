package com.slicktechnologies.server.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.AuditObservations;
import com.slicktechnologies.shared.ObservationImpactRecommendation;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class CreateUpdateObservationApi extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5617261643063523969L;

	
	Logger logger = Logger.getLogger("Logger");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String urlCalled = req.getRequestURL().toString().trim();
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl::" + splitUrl);
		
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		
		long companyId = comp.getCompanyId();
		//for local
//		long companyId = 5348024557502464l;
		String validationMsg="";
		
		String jsonString = req.getParameter("observationDetails");
		logger.log(Level.SEVERE, "jsonString::::::::::" + jsonString);
		
		try {
			JSONObject	jsonobj = new JSONObject(jsonString);
			
			String actionTask = "";
			if(jsonobj.optString("actionTask").trim()!=null){
				actionTask = jsonobj.optString("actionTask").trim();
			}
			
			String observationName="";
			if(jsonobj.optString("observationName").trim()!=null){
				observationName = jsonobj.optString("observationName").trim(); 
			}
			String impact="";
			if(jsonobj.optString("impact").trim()!=null){
				impact = jsonobj.optString("impact").trim(); 
			}
			String action="";
			if(jsonobj.optString("action").trim()!=null){
				action = jsonobj.optString("action").trim(); 
			}
			String recommendation="";
			if(jsonobj.optString("recommendation").trim()!=null){
				recommendation = jsonobj.optString("recommendation").trim(); 
			}
			if(actionTask==null || actionTask.equals("")){
				validationMsg += "actionTask can not be blank"; 
			}
			if(observationName==null || observationName.equals("")){
				validationMsg += "observationName can not be blank"; 
			}
			logger.log(Level.SEVERE, "validationMsg"+validationMsg+" done here");

			JSONObject jsonobjresponse = new JSONObject();

			if(validationMsg.trim().equals("")){
				logger.log(Level.SEVERE, "no validation msg");
				if(actionTask.equals("Create")){
					
					String responsemsg = saveAndUpdateNewObservation(observationName,impact,action,recommendation,companyId);
					logger.log(Level.SEVERE, "responsemsg "+responsemsg);

					jsonobjresponse.put("Message", responsemsg);
					String jsonstring = jsonobjresponse.toString().replaceAll("\\\\", "");
					logger.log(Level.SEVERE, "Json"+jsonstring);
					resp.getWriter().println(jsonstring);
					
				
				}
				else if(actionTask.equals("Delete")){
					
					AuditObservations auditobservation = ofy().load().type(AuditObservations.class).filter("companyId", companyId)
							.filter("observationName", observationName).first().now();
					logger.log(Level.SEVERE, "auditobservation "+auditobservation);

					if(auditobservation!=null){
						ArrayList<ObservationImpactRecommendation> updatedlist = new ArrayList<ObservationImpactRecommendation>();
						updatedlist.addAll(auditobservation.getObservationlist());
						for(ObservationImpactRecommendation auditapp : auditobservation.getObservationlist()){
							if(auditapp.getObservationName().equals(observationName) && auditapp.getImpact().equals(impact)
									&& auditapp.getAction().equals(action) && auditapp.getRecommendation().equals(recommendation)){
								updatedlist.remove(auditapp);
								logger.log(Level.SEVERE, "observation removed successfully"+auditapp.getObservationName()+" - " +auditapp.getImpact()
										+" - "+ auditapp.getAction()+" - "+auditapp.getRecommendation());

							}
						}
						auditobservation.setObservationlist(updatedlist);
						ofy().save().entities(auditobservation);
						
						jsonobjresponse.put("Message", "Obseration deleted successfully");
						String jsonstring = jsonobjresponse.toString().replaceAll("\\\\", "");
						logger.log(Level.SEVERE, "Json"+jsonstring);
						resp.getWriter().println(jsonstring);
					}
					else{
//						resp.getWriter().write("obsevation not found!");
						jsonobjresponse.put("Message", "obsevation not found!");
						String jsonstring = jsonobjresponse.toString().replaceAll("\\\\", "");
						logger.log(Level.SEVERE, "Json"+jsonstring);
						resp.getWriter().println(jsonstring);
					}
		
				}
				else{
					logger.log(Level.SEVERE, "no action task found"+actionTask);

				}
				
			}
			else{
				jsonobjresponse.put("Message", "obsevation not found!");
				String jsonstring = jsonobjresponse.toString().replaceAll("\\\\", "");
				logger.log(Level.SEVERE, "Json"+jsonstring);
//				resp.getWriter().write(validationMsg);
				resp.getWriter().write(jsonstring);

			}
			
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		
	}

		public String saveAndUpdateNewObservation(String observationName,
				String impact, String action, String recommendation, long companyId) {
			
			AuditObservations auditobservation = ofy().load().type(AuditObservations.class).filter("companyId", companyId)
					.filter("observationName", observationName).first().now();
			logger.log(Level.SEVERE, "auditobservation "+auditobservation);
			
			if(auditobservation!=null){
				ArrayList<ObservationImpactRecommendation> list  = new ArrayList<ObservationImpactRecommendation>();
				list.addAll(auditobservation.getObservationlist());
				
				ObservationImpactRecommendation observation = new ObservationImpactRecommendation();
				observation.setObservationName(observationName);
				observation.setImpact(impact);
				observation.setAction(action);
				observation.setRecommendation(recommendation);
				list.add(observation);
				
				auditobservation.setObservationlist(list);
				auditobservation.setStatus(true);
				
				ofy().save().entities(auditobservation);
				logger.log(Level.SEVERE, "observation updated successfully");
				return "Obseration created successfully";
			
			}
			else{
				
				logger.log(Level.SEVERE, "First time new observation entry in the entity");
			
				AuditObservations auditobservationimpact = new AuditObservations();
				auditobservationimpact.setObservationName(observationName);
				
				auditobservationimpact.setCompanyId(companyId);
				
				ArrayList<ObservationImpactRecommendation> list  = new ArrayList<ObservationImpactRecommendation>();
				ObservationImpactRecommendation observation = new ObservationImpactRecommendation();
				observation.setObservationName(observationName);
				observation.setImpact(impact);
				observation.setAction(action);
				observation.setRecommendation(recommendation);
				list.add(observation);
				
				auditobservationimpact.setObservationlist(list);
				auditobservationimpact.setStatus(true);
			
				
				GenricServiceImpl genimpl = new GenricServiceImpl();
				genimpl.save(auditobservationimpact);
				logger.log(Level.SEVERE, "observation created successfully");
				return "Obseration created successfully";
			}
			
		}	
}
