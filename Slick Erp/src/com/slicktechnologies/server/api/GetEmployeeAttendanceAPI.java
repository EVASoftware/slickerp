package com.slicktechnologies.server.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.attendance.AttendanceBean;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;

public class GetEmployeeAttendanceAPI  extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3677090983114612208L;

	Logger logger = Logger.getLogger("Logger");
	SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat datefmt = new SimpleDateFormat("dd");


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		resp.setHeader("Access-Control-Allow-Origin", "*");
	
		dateformat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		String urlCalled = req.getRequestURL().toString().trim();
		logger.log(Level.SEVERE, "urlCalled" + urlCalled);
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl::" + splitUrl);
		
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		
		long companyId = comp.getCompanyId();
		String validationMsg="";
		
		String validationmsgforcustomerportal="";
		
		String stremployeeId = "";
		try {
			stremployeeId = req.getParameter("employeeId").trim();
		} catch (Exception e) {
		}
		
		
		/**
		 * @author Vijay Date :- 10-06-2022
		 * Des :- below parameter used when API will call from customer portal
		 */
		String strcustomerId = "";
		String projectName = "";
		String apicallfrom = "";

		try {
			strcustomerId = req.getParameter("customerId").trim();
		} catch (Exception e) {
		}
		try {
			projectName = req.getParameter("projectName").trim();
		} catch (Exception e) {
		}
		try {
			apicallfrom = req.getParameter("apiCallFrom").trim();
		} catch (Exception e) {
		}
		/**
		 * ends here
		 */
		logger.log(Level.SEVERE, "strcustomerId" + strcustomerId);
		logger.log(Level.SEVERE, "projectName" + projectName);

		if(projectName.equals("") && strcustomerId.equals("")) {
				validationmsgforcustomerportal = "Project Name and Customer id can not be blank";
		}
			
		int customerid =0;
		try {
			customerid = Integer.parseInt(strcustomerId);
		} catch (Exception e) {
		}
		
		int employeeId = 0;
		
		try {
			if(!stremployeeId.equals(""))
			employeeId = Integer.parseInt(stremployeeId);
		} catch (Exception e) {
			validationMsg +="employee id can not be blank";
		}
		if(employeeId==0) {
			validationMsg +="employee id can not be zero";
		}
		
		String startDateStr="";
		try {
			startDateStr = req.getParameter("startDate").trim();
		} catch (Exception e) {
			startDateStr="";
		}
		String endDateStr="";
		try {
			endDateStr = req.getParameter("endDate").trim();
		} catch (Exception e) {
			endDateStr="";
		}
		
		if(startDateStr.equals("")) {
			validationMsg +="Start date can not be blank";
			validationmsgforcustomerportal +="Start date can not be blank";
		}
		if(endDateStr.equals("")) {
			validationMsg +="End date can not be blank";
			validationmsgforcustomerportal  +="End date can not be blank";

		}
		
		Date startDate = null;
		try {
			startDate = dateformat.parse(startDateStr);
		} catch (Exception e) {
			e.printStackTrace();
			validationMsg +="Start date should be dd/mm/yyyy format";
			validationmsgforcustomerportal+="Start date should be dd/mm/yyyy format";

		}
		
		Date endDate = null;
		try {
			endDate = dateformat.parse(endDateStr);
		} catch (Exception e) {
			e.printStackTrace();
			validationMsg +="End date should be dd/mm/yyyy format";
			validationmsgforcustomerportal+="End date should be dd/mm/yyyy format";
		}
		logger.log(Level.SEVERE, "validationMsg" + validationMsg+"msg completed here");
		logger.log(Level.SEVERE, "startDate" + startDate);
		logger.log(Level.SEVERE, "endDate" + endDate);
		logger.log(Level.SEVERE, "apicallfrom" + apicallfrom);

		if((apicallfrom==null || apicallfrom.equals("")) && validationMsg.equals("")) {
			List<Attendance> attendancelist = ofy().load().type(Attendance.class).filter("empId", employeeId)
					.filter("attendanceDate >=", startDate).filter("attendanceDate <=", endDate).filter("companyId", companyId).list();
			logger.log(Level.SEVERE, "Employee id wise attendancelist size" + attendancelist.size());
			if(attendancelist.size()>0) {
				String attendancejsonData = getAttendanceData(attendancelist);
				resp.getWriter().println(attendancejsonData);
				
//				List<AttendanceBean> attendancebeanlist = serverappUtility.getAttendaceBeanList(attendancelist);
//				JSONObject jobj = new JSONObject();
//				JSONArray jsonarray = new JSONArray();
//				 for(int i=0;i<attendancebeanlist.size();i++){
//					 AttendanceBean attBean = attendancebeanlist.get(i);
//					 
//						JSONObject jsonobj = new JSONObject();
//
//						try {
//							
//							jsonobj.put("EmpID", attBean.getEmpId());
//
//							if(attBean.getEmployeeName()!=null){
//								 jsonobj.put("EmployeeName", attBean.getEmployeeName());
//							}else{
//								 jsonobj.put("EmployeeName","");
//							}
//							
//							if(attBean.getEmpDesignation()!=null){
//								 jsonobj.put("EmpDesignation", attBean.getEmployeeName());
//
//							}else{
//								 jsonobj.put("EmpDesignation","");
//
//							}
//							
//							if(attBean.getProjectName()!=null){
//								 jsonobj.put("ProjectName",attBean.getProjectName());
//
//							}else{
//								 jsonobj.put("ProjectName","");
//							}
//						
//							if(attBean.getSiteLocation()!=null){
//								 jsonobj.put("siteLocation",attBean.getSiteLocation());
//
//							}else{
//								 jsonobj.put("siteLocation","");
//
//							}
//							
//							if(attBean.getShift()!=null){
//								 jsonobj.put("Shift",attBean.getShift());
//
//							}else{
//								 jsonobj.put("Shift","");
//
//							}
//							
//							if(attBean.getOvertimeType()!=null){
//								 jsonobj.put("Month",attBean.getMonth());
//
//							}else{
//								 jsonobj.put("Month","");
//
//							}
//							
//				            int j=1;
//							for (Map.Entry<Date, Attendance> entry : attBean.getAttendanceObjectMap().entrySet()) {
//							    Date key = entry.getKey();
//							    String value = entry.getValue().getGroup();
//							    int day = Integer.parseInt(datefmt.format(key));
//								logger.log(Level.SEVERE, "j =" + j +" "+"date ="+day+" "+value);
//
//								 jsonobj.put(day+"", value);
//
//							    j++;
//							}
//							/**
//							    * @author Anil,Date :24-01-2019
//							    * Added total OT hours column
//							    * 
//							    */
//							if(attBean.getOvertimeType()!=null){
//								 jsonobj.put("otHours",attBean.getOvertimeType());
//
//							}else{
//								 jsonobj.put("otHours","");
//
//							}
//							
//							if(attBean.getWorkedDays()!=null){
//								 jsonobj.put("workedDays",attBean.getWorkedDays());
//
//							}else{
//								 jsonobj.put("workedDays","0");
//
//							}
//							if(attBean.getLeaveDays()!=null){
//								 jsonobj.put("leaveDays",attBean.getLeaveDays());
//							}else{
//								 jsonobj.put("leaveDays","0");
//							}
//							
//							jsonarray.put(jsonobj);
//							
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
//						
//						
//					}
//				 
//				 try {
//					jobj.put("data", jsonarray);
//					jobj.put("LWP", "Unpaid");
//					jobj.put("A", "Absent");
//					jobj.put("HD", "Half Day");
//					jobj.put("P", "Present");
//					jobj.put("NA", "before joining period");
//					jobj.put("PH", "Public Holiday");
//					jobj.put("NH", "National Holiday");
//
//					String jsonstring = jobj.toString().replaceAll("\\\\", "");
//					logger.log(Level.SEVERE, "Json"+jsonstring);
//					resp.getWriter().println(jsonstring);
//					
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
				 
			}
			else {
				resp.getWriter().println("Attendance does not exist");

			}
		}
		else if(validationmsgforcustomerportal.equals("") && apicallfrom.equals(AppConstants.CUSTOMERPORTAL)) {
			logger.log(Level.SEVERE, "API call from customer portal");
			List<Attendance> attendancelist = null;
			if(projectName!=null && !projectName.equals("")) {
				attendancelist = ofy().load().type(Attendance.class).filter("projectName", projectName)
						.filter("attendanceDate >=", startDate).filter("attendanceDate <=", endDate).filter("companyId", companyId).list();
				logger.log(Level.SEVERE, "Project Name wise attendancelist size" + attendancelist.size());
			}
			else {
				
				List<HrProject> projectlist = ofy().load().type(HrProject.class).filter("companyId", comp.getCompanyId()).filter("personinfo.count", customerid)
						  .filter("status", true).list();
				logger.log(Level.SEVERE, "customer projectlist size "+projectlist.size());
				ArrayList<String> projectNamearray = new ArrayList<String>();
				for(HrProject hrproject : projectlist) {
					projectNamearray.add(hrproject.getProjectName());
				}
				logger.log(Level.SEVERE, "projectNamearray"+projectNamearray);

				attendancelist = ofy().load().type(Attendance.class).filter("projectName IN", projectNamearray)
						.filter("attendanceDate >=", startDate).filter("attendanceDate <=", endDate).filter("companyId", companyId).list();
				logger.log(Level.SEVERE, "Project Name wise attendancelist size" + attendancelist.size());
				
			}
			
			if(attendancelist.size()>0) {
				String attendancejsonData = getAttendanceData(attendancelist);
				resp.getWriter().println(attendancejsonData);
			}
			else {
				resp.getWriter().println("Attendance does not exist");
			}
		}
		else {
			if(apicallfrom==null || apicallfrom.equals("")) {
				resp.getWriter().println(validationMsg);
			}
			else {
				resp.getWriter().println(validationmsgforcustomerportal);
			}
		}
		
		
	}

	private String getAttendanceData(List<Attendance> attendancelist) {
		ServerAppUtility serverappUtility = new ServerAppUtility();
		List<AttendanceBean> attendancebeanlist = serverappUtility.getAttendaceBeanList(attendancelist);
		JSONObject jobj = new JSONObject();
		JSONArray jsonarray = new JSONArray();
		 for(int i=0;i<attendancebeanlist.size();i++){
			 AttendanceBean attBean = attendancebeanlist.get(i);
			 
				JSONObject jsonobj = new JSONObject();

				try {
					
					jsonobj.put("EmpID", attBean.getEmpId());

					if(attBean.getEmployeeName()!=null){
						 jsonobj.put("EmployeeName", attBean.getEmployeeName());
					}else{
						 jsonobj.put("EmployeeName","");
					}
					
					if(attBean.getEmpDesignation()!=null){
						 jsonobj.put("EmpDesignation", attBean.getEmployeeName());

					}else{
						 jsonobj.put("EmpDesignation","");

					}
					
					if(attBean.getProjectName()!=null){
						 jsonobj.put("ProjectName",attBean.getProjectName());

					}else{
						 jsonobj.put("ProjectName","");
					}
				
					if(attBean.getSiteLocation()!=null){
						 jsonobj.put("siteLocation",attBean.getSiteLocation());

					}else{
						 jsonobj.put("siteLocation","");

					}
					
					if(attBean.getShift()!=null){
						 jsonobj.put("Shift",attBean.getShift());

					}else{
						 jsonobj.put("Shift","");

					}
					
					if(attBean.getOvertimeType()!=null){
						 jsonobj.put("Month",attBean.getMonth());

					}else{
						 jsonobj.put("Month","");

					}
					
		            int j=1;
					for (Map.Entry<Date, Attendance> entry : attBean.getAttendanceObjectMap().entrySet()) {
					    Date key = entry.getKey();
					    String value = entry.getValue().getGroup();
					    int day = Integer.parseInt(datefmt.format(key));
						logger.log(Level.SEVERE, "j =" + j +" "+"date ="+day+" "+value);

						 jsonobj.put(day+"", value);

					    j++;
					}
					/**
					    * @author Anil,Date :24-01-2019
					    * Added total OT hours column
					    * 
					    */
					if(attBean.getOvertimeType()!=null){
						 jsonobj.put("otHours",attBean.getOvertimeType());

					}else{
						 jsonobj.put("otHours","");

					}
					
					if(attBean.getWorkedDays()!=null){
						 jsonobj.put("workedDays",attBean.getWorkedDays());

					}else{
						 jsonobj.put("workedDays","0");

					}
					if(attBean.getLeaveDays()!=null){
						 jsonobj.put("leaveDays",attBean.getLeaveDays());
					}else{
						 jsonobj.put("leaveDays","0");
					}
					
					jsonarray.put(jsonobj);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
			}
		 
		 try {
			jobj.put("data", jsonarray);
			jobj.put("LWP", "Unpaid");
			jobj.put("A", "Absent");
			jobj.put("HD", "Half Day");
			jobj.put("P", "Present");
			jobj.put("NA", "before joining period");
			jobj.put("PH", "Public Holiday");
			jobj.put("NH", "National Holiday");

			String jsonstring = jobj.toString().replaceAll("\\\\", "");
			logger.log(Level.SEVERE, "Json"+jsonstring);
			return jsonstring;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 return null;
	}

}
