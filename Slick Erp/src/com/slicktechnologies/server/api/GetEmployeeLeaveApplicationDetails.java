package com.slicktechnologies.server.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveApplication;

public class GetEmployeeLeaveApplicationDetails extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9132949905514561156L;

	
	Logger logger = Logger.getLogger("Logger");
	SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		resp.setHeader("Access-Control-Allow-Origin", "*");
		dateformat.setTimeZone(TimeZone.getTimeZone("IST"));

		String urlCalled = req.getRequestURL().toString().trim();
		logger.log(Level.SEVERE, "urlCalled" + urlCalled);
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl::" + splitUrl);
		
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		
		String validationMsg="";
		
		
		String stremployeeId = req.getParameter("employeeId").trim();
		int employeeId = 0;
		
		try {
			employeeId = Integer.parseInt(stremployeeId);
		} catch (Exception e) {
			validationMsg +=" Employee id can not be blank.";
		}
		if(employeeId==0) {
			validationMsg +=" Employee id can not be zero.";
		}

		String fromDateStr="";
		try {
			fromDateStr = req.getParameter("fromtDate").trim();
		} catch (Exception e) {
			fromDateStr="";
		}
		String toDateStr="";
		try {
			toDateStr = req.getParameter("toDate").trim();
		} catch (Exception e) {
			toDateStr="";
		}
		
		if(fromDateStr.equals("")) {
			validationMsg +=" From date can not be blank.";
		}
		if(toDateStr.equals("")) {
			validationMsg +=" To date can not be blank.";
		}
		
		Date fromDate = null;
		try {
			fromDate = dateformat.parse(fromDateStr);
		} catch (Exception e) {
			e.printStackTrace();
			validationMsg +=" From date should be dd/mm/yyyy format.";
		}
		
		Date toDate = null;
		try {
			toDate = dateformat.parse(toDateStr);
		} catch (Exception e) {
			e.printStackTrace();
			validationMsg +=" To date should be dd/mm/yyyy format.";
		}
		
	
		if(validationMsg.equals("") || validationMsg.equals(" ")) {
			
			List<LeaveApplication> leaveApplicationlist = ofy().load().type(LeaveApplication.class).filter("companyId", comp.getCompanyId())
									.filter("empid", employeeId).filter("fromdate >=", fromDate).filter("todate <=", toDate).list();
			
			JSONObject jsonobj = new JSONObject();
			JSONArray jsonarray = new JSONArray();

			if(leaveApplicationlist.size()==0) {
				try {
					jsonobj.put("Message", "No leave application found");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				jsonarray.put(jsonobj);

			}
			else {
				
				
				for(LeaveApplication leaveapplication : leaveApplicationlist) {
					JSONObject jobj = new  JSONObject();
					
					try {
						
						jobj.put("EmployeeId", leaveapplication.getEmpid());
						jobj.put("employeeName", leaveapplication.getEmployeeName());
						if(leaveapplication.getFromdate()!=null) {
							jobj.put("fromDate", dateformat.format(leaveapplication.getFromdate()));
						}
						if(leaveapplication.getTodate()!=null) {
							jobj.put("toDate", dateformat.format(leaveapplication.getTodate()));
						}
						if(leaveapplication.getLeaveReason()!=null) {
							jobj.put("reason",leaveapplication.getLeaveReason());
						}
						if(leaveapplication.getLeaveType()!=null) {
							jobj.put("leaveType", leaveapplication.getLeaveType());
						}
						if(leaveapplication.getLeaveType()!=null) {
							jobj.put("status", leaveapplication.getStatus());
						}
						
						jsonarray.put(jobj);
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				
					
				}
			}
				
			try {
				jsonobj.put("data", jsonarray);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			String jsonstring = jsonobj.toString().replaceAll("\\\\", "");
			logger.log(Level.SEVERE, "Json"+jsonstring);
			resp.getWriter().println(jsonstring);
			
		}
		else {
			resp.getWriter().println(validationMsg);
		}
	}
}
