package com.slicktechnologies.server.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.email.EmailDetails;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.UserRole;

public class CreateERPTrailLink extends HttpServlet{

	

	/**
	 * 
	 */
	private static final long serialVersionUID = -2737998694297354121L;
	Logger logger = Logger.getLogger("Logger");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		resp.setHeader("Access-Control-Allow-Origin", "*");

		String urlCalled = req.getRequestURL().toString().trim();
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl::" + splitUrl);
		
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		long companyId = comp.getCompanyId();

		String companyName = "";
		try {
			companyName = req.getParameter("companyName");
		} catch (Exception e) {
			
		}
		
		String name = "";
		try {
			name = req.getParameter("Name");
		} catch (Exception e) {
			
		}
		String emailId="";
		try {
			 emailId = req.getParameter("emailId");
		} catch (Exception e) {
		}
		
		String strmobileNo="";
		long mobileNo=0;
		try {
			strmobileNo = req.getParameter("mobileNumber");
		} catch (Exception e) {
		}
		
		String validationMsg ="";
		
		if(companyName==null || companyName.equals("")) {
			validationMsg += " Company name can not be blank."; 
		}
		
		if(name==null || name.equals("")) {
			validationMsg += " Name can not be blank."; 
		}
		
		if(emailId==null || emailId.equals("")) {
			validationMsg += " Email can not be blank."; 
		}
		
		if(validationMsg.equals("")) {
			String dataValidationmsg = "";
			Branch branchEntity = ofy().load().type(Branch.class).filter("buisnessUnitName", companyName).filter("companyId", companyId).first().now();
			if(branchEntity!=null) {
				dataValidationmsg = "Branch name already exist with this company name";
			}
			
			UserRole zonaluserRole = ofy().load().type(UserRole.class).filter("roleName", AppConstants.ZONALCOORDINATOR).filter("roleStatus", true).first().now();
			if(zonaluserRole==null) {
				dataValidationmsg += "Zonal Coordinator user role does not exist";
			}
			UserRole operatoruserRole = ofy().load().type(UserRole.class).filter("roleName", AppConstants.OPERATOR).filter("roleStatus", true).first().now();
			if(operatoruserRole==null) {
				dataValidationmsg += "Zonal Coordinator user role does not exist";
			}
			UserRole salesUserRole = ofy().load().type(UserRole.class).filter("roleName", AppConstants.SALES).filter("roleStatus", true).first().now();
			if(salesUserRole==null) {
				dataValidationmsg += "Sales user role does not exist";
			}
			
			logger.log(Level.SEVERE, "dataValidationmsg"+dataValidationmsg);
			if(dataValidationmsg.equals("")) {
				logger.log(Level.SEVERE, "inside no validation");
				try {
					
				Branch branch = new Branch();
				branch.setCompanyId(companyId);
				branch.setBusinessUnitName(companyName);
				if(mobileNo!=0)
				branch.setCellNumber1(mobileNo);
				branch.setPocName(name);
				branch.setPocEmail(emailId);
				branch.setstatus(true);
				GenricServiceImpl genimpl = new GenricServiceImpl();
				genimpl.save(branch);
				
				
				
				String zonaluseremployee = CreateEmployee(name,companyId,branch.getBusinessUnitName(),"Zonal Coordinator");
				logger.log(Level.SEVERE, "zonaluseremployee "+zonaluseremployee);

				String zonaluser = createUser(name,name,zonaluserRole,companyId,name);
				logger.log(Level.SEVERE, "zonaluser "+zonaluser);

			
				String technician = name+"_Technician";
				
				String technicianEmployee = CreateEmployee(technician,companyId,branch.getBusinessUnitName(),"Operator");
				logger.log(Level.SEVERE, "technician "+technicianEmployee);

				String operatoruser = createUser(technician,"pass123",operatoruserRole,companyId,technician);
				logger.log(Level.SEVERE, "operatoruser "+operatoruser);

				String salesPerson = name+"_Salesperson";
				String salesPersonEmployee = CreateEmployee(salesPerson,companyId,branch.getBusinessUnitName(),"Sales");
				logger.log(Level.SEVERE, "salesPersonEmployee "+salesPersonEmployee);

				String saleuser = createUser(salesPerson,"pass123",salesUserRole,companyId,salesPerson);
				logger.log(Level.SEVERE, "saleuser "+saleuser);

				
				
				Email email = new Email();
				StringBuilder sb = new StringBuilder();
				
				String emailmsg = "Dear Mr. "+name+", \n  Thank you so much for selecting EVA Software for evaluation &"
						+ " taking the first step towards transforming your business. We help visionary corporate to put the business on auto-pilot"
						+ " with a complete EVA CRM/ERP suite. Currently, more than 300 clients are successfully running their business using EVA Software \n \n";
				
				sb.append(emailmsg);
				sb.append("Watch <a href ='https://www.youtube.com/watch?v=SusKh1qtDnE&t=6s'> 7 Steps To Set Up Paperless Pest Control Office </a> \n \n");
				sb.append("&nbsp;&nbsp;  1. Watch <a href ='https://www.youtube.com/watch?v=gn1FA75vDgc' > Track and Manage Technician From Anywhere </a> \n");
				sb.append("&nbsp;&nbsp;  2. Read <a href =https://docs.google.com/presentation/d/e/2PACX-1vSuBRxlUZ-g5GhjyMsbkIagYecg-beAexzugIidp3UIS0rmU0pQcX4Lb41bXp7oSFSSu9lvm1YnBZPP/pub?start=true&loop=false&delayms=5000#slide=id.g4df03395ac_0_450 > EVA Pest Pro - Pest Control Software Complete Profile </a> \n \n");
				sb.append("It would allow you to \n\n");
				sb.append("&nbsp;&nbsp;  1. <a href =https://www.youtube.com/playlist?list=PLeJE9HlytVxRRuMRz-_XMcdg03uhwLbmC >Reduce Un-billed & Billed outstanding</a> \n");
				sb.append("&nbsp;&nbsp;  2. <a href =https://www.youtube.com/playlist?list=PLeJE9HlytVxS-S1z2ftGYEP4pzaB9CLnQ >Complete 99.9 % Customer Services </a> \n");
				sb.append("&nbsp;&nbsp;  3. <a href =https://www.youtube.com/watch?v=S8dHKPdERqk&t=2s >Track Technician & His Performance - Technician Management Mobile App </a> \n");
				sb.append("&nbsp;&nbsp;  4. <a href =https://www.youtube.com/playlist?list=PLeJE9HlytVxTYvBBbKOl8h2QYGgGpMbCJ >Retain 99.98 % Of Your Orders </a> \n");
				sb.append("&nbsp;&nbsp;  5. <a href =https://www.youtube.com/playlist?list=PLeJE9HlytVxSXTVp_wiM_R2RLyGd1l6iG >Increase Sale 25 % YoY </a> \n");
				sb.append("&nbsp;&nbsp;  6. <a href =https://www.youtube.com/playlist?list=PLeJE9HlytVxQ_etwiVcJwbmY4xq2Bo-zP >Optimize Inventory & Generate Stock Alert Automatically</a> \n");
				sb.append("&nbsp;&nbsp;  7. <a href =https://www.youtube.com/playlist?list=PLeJE9HlytVxRT11U2UzfU1V9ss_dAtukyhttps://www.youtube.com/playlist?list=PLeJE9HlytVxRT11U2UzfU1V9ss_dAtuky >No More Complaints! </a> \n");
				sb.append("&nbsp;&nbsp;  8. <a href =https://www.youtube.com/playlist?list=PLeJE9HlytVxQ-aWtFl-gNjK6fDEi8uObo >Report & Monitor Real-Time Attendance, Process Payroll In Just 1 Day </a> \n");
				sb.append("&nbsp;&nbsp;  9. <a href =https://www.youtube.com/watch?v=S8dHKPdERqk&t=2s >Track & Manage Technician from your mobile phone -&nbsp;Technician Management App</a> \n \n \n");

				sb.append("I would be very happy to look at automating your business process to set the platform for exponential growth & reduce operating cost. \n \n ");
				sb.append("15 days&nbsp;trial period is activated on the below link \n \n");
				sb.append("ERP\n");
				sb.append("Your Login URL: my-dot-evaerp282915.appspot.com \n");
				sb.append("&nbsp;&nbsp;	 * Your Login User Id - "+name +"\n");
				sb.append("&nbsp;&nbsp;	 * Your Login Password - "+name +"\n\n");
				
				sb.append("Technician App\n");
				sb.append("&nbsp;&nbsp;  * <a href = https://play.google.com/store/apps/details?id=com.evasoftwaresolutions.pestapp>Install</a>&nbsp;EVA Pedio - Technician App\n");
				sb.append("&nbsp;&nbsp;	 * Registration code - evaerp282915\n");
				sb.append("&nbsp;&nbsp;	 * Admin User id - scheduler\n");
				sb.append("&nbsp;&nbsp;	 * password - 123\n\n");

				sb.append("Technician Login\n");
				sb.append("&nbsp;&nbsp;	 * User id - "+technician+"\n");
				sb.append("&nbsp;&nbsp;	 * password - pass123 \n\n");

				sb.append("Salesperson Login\n");
				sb.append("&nbsp;&nbsp;	 * User id - "+salesPerson+"\n");
				sb.append("&nbsp;&nbsp;	 * password - pass123 \n \n \n");

				
				sb.append("Visit&nbsp;EVA Help Desk - <a href =https://evasoftwaresolutions.freshdesk.com/support/home>https://evasoftwaresolutions.freshdesk.com/support/home</a>&nbsp;for any help required. Alternatively, you can get in touch with me. ");
				sb.append("Looking forward to growing together \n \n \n");
				sb.append("thanks & regards, \n ");
				sb.append("EVA Support\n");
				sb.append("+91 9619390370 \n");
				sb.append("<a href =support@evasoftwaresolutions.com>support@evasoftwaresolutions.com </a> \n");
				sb.append("<a href =www.evasoftwaresolutions.com>www.evasoftwaresolutions.com </a>\n");

				String emailbody = sb.toString();
				
				String resultString = emailbody.replaceAll("[\n]", "<br>");
//				String finalemailbody = resultString.replaceAll("[\\s]", "&nbsp;");
				
				logger.log(Level.SEVERE, "emailbody"+resultString);

				EmailDetails emaildeatils = new EmailDetails();
				emaildeatils.setFromEmailid(comp.getEmail());
				ArrayList<String> toemaillist = new ArrayList<String>();
				toemaillist.add(emailId);
				emaildeatils.setToEmailId(toemaillist);
				emaildeatils.setEmailBody(resultString);
//				emaildeatils.setEmailBody(finalemailbody);
				emaildeatils.setSubject("EVA Demo System Request Email - trial for 15 days");
				email.sendEmailToReceipient(emaildeatils, companyId);
				
				
				
				JSONObject jsonobj = new JSONObject();
				try {
					jsonobj.put("Company Name", companyName);
					jsonobj.put("ERP user / password", name+" / "+name);
					jsonobj.put("Technician user / password", technician+" / pass123");
					jsonobj.put("Salesperson user / password", salesPerson+" / pass123");
					jsonobj.put("Status", "Successful");

				} catch (JSONException e) {
					e.printStackTrace();
				}
				
					String jsonstring = jsonobj.toString().replaceAll("\\\\", "");
					logger.log(Level.SEVERE, "Json"+jsonstring);
					resp.getWriter().println(jsonstring);
				
				} catch (Exception e) {
					JSONObject jsonobj = new JSONObject();
					try {
						jsonobj.put("Company Name", companyName);
						jsonobj.put("Status", "Successful");
					} catch (JSONException e1) {
						e1.printStackTrace();
					}
					String jsonstring = jsonobj.toString().replaceAll("\\\\", "");
					logger.log(Level.SEVERE, "Json"+jsonstring);
					resp.getWriter().println(jsonstring);
				}

			}
			else {
				resp.getWriter().println(dataValidationmsg);

			}
			
		}
		else {
			resp.getWriter().println(validationMsg);
		}
	
	}

	private String CreateEmployee(String employeeName, long companyId, String branchName, String employeeRole) {
		Employee employee = new Employee();
		employee.setCompanyId(companyId);
		employee.setBranchName(branchName);
		employee.setFullname(employeeName);
		employee.setStatus(true);
		employee.setRoleName(employeeRole);;
		GenricServiceImpl genimpl2 = new GenricServiceImpl();
		genimpl2.save(employee);
		return "Employee created successfully";

	}

	private String createUser(String userName, String password,UserRole userRole, long companyId, String employeeName ) {
		GenricServiceImpl genimpl = new GenricServiceImpl();

		User user = new User();
		user.setUserName(userName);
		user.setPassword(password);
		user.setRole(userRole);
		user.setEmployeeName(employeeName);
		user.setCompanyId(companyId);
		user.setStatus(true);
		genimpl.save(user);
		
		return "User created successfully";
	}
	
}
