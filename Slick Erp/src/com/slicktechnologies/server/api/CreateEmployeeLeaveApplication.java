package com.slicktechnologies.server.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.simplesoftwares.client.library.ReturnFromServer;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveApplication;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class CreateEmployeeLeaveApplication extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2432458834533998898L;
	Logger logger = Logger.getLogger("Logger");
	SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		resp.setHeader("Access-Control-Allow-Origin", "*");
		dateformat.setTimeZone(TimeZone.getTimeZone("IST"));

		String urlCalled = req.getRequestURL().toString().trim();
		logger.log(Level.SEVERE, "urlCalled" + urlCalled);
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl::" + splitUrl);
		
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		
		String validationMsg="";
		
		String stremployeeId = req.getParameter("employeeId").trim();
		int employeeId = 0;
		
		try {
			employeeId = Integer.parseInt(stremployeeId);
		} catch (Exception e) {
			validationMsg +=" Employee id can not be blank.";
		}
		if(employeeId==0) {
			validationMsg +=" Employee id can not be zero.";
		}

		String fromDateStr="";
		try {
			fromDateStr = req.getParameter("fromtDate").trim();
		} catch (Exception e) {
			fromDateStr="";
		}
		String toDateStr="";
		try {
			toDateStr = req.getParameter("toDate").trim();
		} catch (Exception e) {
			toDateStr="";
		}
		
		if(fromDateStr.equals("")) {
			validationMsg +=" From date can not be blank.";
		}
		if(toDateStr.equals("")) {
			validationMsg +=" To date can not be blank.";
		}
		
		Date fromDate = null;
		try {
			fromDate = dateformat.parse(fromDateStr);
		} catch (Exception e) {
			e.printStackTrace();
			validationMsg +=" From date should be dd/mm/yyyy format.";
		}
		
		Date toDate = null;
		try {
			toDate = dateformat.parse(toDateStr);
		} catch (Exception e) {
			e.printStackTrace();
			validationMsg +=" To date should be dd/mm/yyyy format.";
		}
		
		String leavetype = req.getParameter("leaveType").trim();
		String reason = req.getParameter("reason").trim();
		if(leavetype==null || leavetype.equals("")) {
			validationMsg +=" Leave type can not be blank.";
		}
		if(reason==null || reason.equals("")) {
			validationMsg +=" Reason can not be blank.";
		}
		Employee employee = null;
		if(employeeId!=0) {
			employee = ofy().load().type(Employee.class).filter("companyId", comp.getCompanyId()).filter("count", employeeId).first().now();
							
		}
		if(employee==null) {
			validationMsg +=" Employee id "+ employeeId +" does not exist";
		}
				
		logger.log(Level.SEVERE, "validationMsg" + validationMsg+"msg completed here");
		logger.log(Level.SEVERE, "fromDate" + fromDate);
		logger.log(Level.SEVERE, "toDate" + toDate);
		
		if(validationMsg.trim().equals("") || validationMsg.trim().equals("")) {
			JSONObject jsonobj = new JSONObject();

			try {

				LeaveApplication leaveApplication = new LeaveApplication();
				leaveApplication.setCompanyId(comp.getCompanyId());
				HrProject hrproject = null;
				
				if(employee!=null) {
					leaveApplication.setEmpid(employee.getCount());
					leaveApplication.setEmpCellNo(employee.getCellNumber1());
					leaveApplication.setEmployeeName(employee.getFullname());
					if(employee.getDesignation()!=null)
					leaveApplication.setEmployeedDesignation(employee.getDesignation());
					if(employee.getRoleName()!=null)
					leaveApplication.setEmployeeRole(employee.getRoleName());
					if(employee.getEmployeeType()!=null)
					leaveApplication.setEmployeeType(employee.getEmployeeType());
					leaveApplication.setBranch(employee.getBranchName());
					
					if(employee.getProjectName()!=null && !employee.getProjectName().equals(""))
					hrproject = ofy().load().type(HrProject.class).filter("companyId", comp.getCompanyId()).filter("projectName", employee.getProjectName()).first().now();
					
				}
				leaveApplication.setLeaveType(leavetype);
				leaveApplication.setFromdate(fromDate);
				leaveApplication.setTodate(toDate);
				leaveApplication.setRemark(reason);
				leaveApplication.setStatus(LeaveApplication.CREATED);
				
				User userEntity = null;
				ofy().load().type(User.class).filter("companyId", comp.getCompanyId()).filter("role.roleName", "Admin").first().now();
				
				if(hrproject!=null) {
					if(hrproject.getSupervisor()!=null && !hrproject.getSupervisor().equals("")) {
						leaveApplication.setApproverName(hrproject.getSupervisor());
					}
					else if(hrproject.getManager()!=null && !hrproject.getManager().equals("")) {
						leaveApplication.setApproverName(hrproject.getManager());
					}
					else {
						userEntity = ofy().load().type(User.class).filter("companyId", comp.getCompanyId()).filter("role.roleName", "Admin").first().now();
						leaveApplication.setApproverName(userEntity.getEmployeeName());
					}
				}
				else {
					userEntity = ofy().load().type(User.class).filter("companyId", comp.getCompanyId()).filter("role.roleName", "Admin").first().now();
					leaveApplication.setApproverName(userEntity.getEmployeeName());
				}
				
				GenricServiceImpl genimpl = new GenricServiceImpl();
				ReturnFromServer server = genimpl.save(leaveApplication);
				logger.log(Level.SEVERE, "Leave application created sucessfully");
				String msg = sendApprovalRequest(leaveApplication, server.count);
				logger.log(Level.SEVERE, "msg "+msg);

				
				try {
					jsonobj.put("Leave Request Id", server.count);
					jsonobj.put("Status", "Successful");
					jsonobj.put("Approver Name", leaveApplication.getApproverName());
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				try {
					jsonobj.put("Leave Request Id", "");
					jsonobj.put("Status", "Unsuccessful");
					jsonobj.put("Approver Name", "");
				} catch (Exception e2) {
					// TODO: handle exception
				}
				
			}
			
			String jsonstring = jsonobj.toString().replaceAll("\\\\", "");
			logger.log(Level.SEVERE, "Json"+jsonstring);
			resp.getWriter().println(jsonstring);
			
		}
		else {
			resp.getWriter().println(validationMsg);
		}
		
	}
	
	
	public String sendApprovalRequest(LeaveApplication leaveApplication,int leaveapplicationId) {
		logger.log(Level.SEVERE, "Approval Sending == leaveapplicationId =="+leaveapplicationId);
		
		try {
		Approvals approval = new Approvals();
		approval.setApproverName(leaveApplication.getApproverName());
		approval.setBranchname(leaveApplication.getBranch());
		approval.setBusinessprocessId(leaveapplicationId);
		approval.setRequestedBy(leaveApplication.getEmployee());
		approval.setPersonResponsible(leaveApplication.getEmployee());
		approval.setDocumentCreatedBy(leaveApplication.getEmployee());
		approval.setApprovalLevel(1);
		approval.setStatus(Approvals.PENDING);
		approval.setBusinessprocesstype("Leave Application");
		approval.setBpId(leaveApplication.getEmpid()+"");
		approval.setBpName(leaveApplication.getEmployee());
		approval.setDocumentValidation(false);
		approval.setCompanyId(leaveApplication.getCompanyId());
		logger.log(Level.SEVERE, "Approval Sending request");

		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(approval);
		logger.log(Level.SEVERE, "Auto Invoice Approval Sent successfully");
		
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Exception"+e.getMessage());
		}
		
		return "success";
	}
	

}
