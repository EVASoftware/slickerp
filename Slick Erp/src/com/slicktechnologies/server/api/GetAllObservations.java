package com.slicktechnologies.server.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.JSONArray;

import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.AuditObservations;
import com.slicktechnologies.shared.ObservationImpactRecommendation;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class GetAllObservations extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6205546311479417801L;

	
	Logger logger = Logger.getLogger("Logger");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String urlCalled = req.getRequestURL().toString().trim();
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl::" + splitUrl);
		
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		
		long companyId = comp.getCompanyId();
		

		List<AuditObservations> observationEntitylist = ofy().load().type(AuditObservations.class).filter("companyId", companyId)
													.filter("status", true).list();
		
		logger.log(Level.SEVERE, "observationEntitylist size "+observationEntitylist.size());
		
		JSONObject mainjsonobj = new JSONObject();
		JSONArray observationarray = new JSONArray();
		for(AuditObservations auditobservation : observationEntitylist){
			
			JSONObject jsonobj = new JSONObject();
			try {
				if(auditobservation.getObservationName().contains("\"")) {
					JSONObject errorObj= new JSONObject();
					errorObj.put("Failed", "Remove double quote from Observation Name"+". Contact system administrator.");
					String jsonString = errorObj.toString();
					resp.getWriter().println(jsonString);
					logger.log(Level.SEVERE, "error message sent for Observation name "+auditobservation.getObservationName());
					return;				
				}
				jsonobj.put("observationName", auditobservation.getObservationName());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			JSONArray observationimpactrecommarray = new JSONArray();

			for(ObservationImpactRecommendation ObservationimpactRecom : auditobservation.getObservationlist()){
				
				JSONObject jsonobj2 = new JSONObject();
				
				try {
					if(ObservationimpactRecom.getObservationName().contains("\"")) {
						JSONObject errorObj= new JSONObject();
						errorObj.put("Failed", "Remove double quote from Observation Name"+". Contact system administrator.");
						String jsonString = errorObj.toString();
						resp.getWriter().println(jsonString);
						logger.log(Level.SEVERE, "error message sent for Observation name "+ObservationimpactRecom.getObservationName());
						return;				
					}
					jsonobj2.put("observation", ObservationimpactRecom.getObservationName());
				} catch (JSONException e) {
					e.printStackTrace();
				}
				try {
					if(ObservationimpactRecom.getImpact().contains("\"")) {
						JSONObject errorObj= new JSONObject();
						errorObj.put("Failed", "Remove double quote from impact of Observation Name"+ObservationimpactRecom.getObservationName()+". Contact system administrator.");
						String jsonString = errorObj.toString();
						resp.getWriter().println(jsonString);
						logger.log(Level.SEVERE, "error message sent for Observation impact "+ObservationimpactRecom.getImpact()+" of observation name"+ObservationimpactRecom.getObservationName());
						return;				
					}
					jsonobj2.put("impact", ObservationimpactRecom.getImpact());
				} catch (JSONException e) {
					e.printStackTrace();
				}
				try {
					if(ObservationimpactRecom.getAction().contains("\"")) {
						JSONObject errorObj= new JSONObject();
						errorObj.put("Failed", "Remove double quote from action of Observation Name"+ObservationimpactRecom.getObservationName()+". Contact system administrator.");
						String jsonString = errorObj.toString();
						resp.getWriter().println(jsonString);
						logger.log(Level.SEVERE, "error message sent for Observation action "+ObservationimpactRecom.getAction()+" of observation name"+ObservationimpactRecom.getObservationName());
						return;				
					}
					jsonobj2.put("action", ObservationimpactRecom.getAction());
				} catch (JSONException e) {
					e.printStackTrace();
				}
				try {
					if(ObservationimpactRecom.getRecommendation().contains("\"")) {
						JSONObject errorObj= new JSONObject();
						errorObj.put("Failed", "Remove double quote from recommendation of Observation Name"+ObservationimpactRecom.getObservationName()+". Contact system administrator.");
						String jsonString = errorObj.toString();
						resp.getWriter().println(jsonString);
						logger.log(Level.SEVERE, "error message sent for Observation recommendation "+ObservationimpactRecom.getRecommendation()+" of observation name"+ObservationimpactRecom.getObservationName());
						return;				
					}
					jsonobj2.put("recommendation", ObservationimpactRecom.getRecommendation());
				} catch (JSONException e) {
					e.printStackTrace();
				}
				
				observationimpactrecommarray.add(jsonobj2);

			}
			
			try {
				jsonobj.put("observationlist", observationimpactrecommarray);
				observationarray.add(jsonobj);

				
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		
		try {
			mainjsonobj.put("observationwiselist", observationarray);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			String jsonstring = mainjsonobj.toString().replaceAll("\\\\", "");
			logger.log(Level.SEVERE, "Json"+jsonstring);
			resp.getWriter().println(jsonstring);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}	
}
