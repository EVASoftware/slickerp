package com.slicktechnologies.server.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.LicenseDetails;

public class ResetSystemAPI extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3802884521080408865L;
	Logger logger = Logger.getLogger("Logger");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		resp.setHeader("Access-Control-Allow-Origin", "*");

		String urlCalled = req.getRequestURL().toString().trim();
		logger.log(Level.SEVERE, "urlCalled" + urlCalled);
//		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
//		logger.log(Level.SEVERE, "splitUrl::" + splitUrl);
		
//		Company comp = ofy().load().type(Company.class)
//				.filter("accessUrl", splitUrl[0]).first().now();
//		
//		long companyId = comp.getCompanyId();
		
		String[] splitURL2 = urlCalled.split("slick_erp/");
		logger.log(Level.SEVERE, "splitURL2::" + splitURL2[0]);

		String updatedurlCalled = splitURL2[0]+"slick_erp/";
		logger.log(Level.SEVERE, "updated urlCalled" + updatedurlCalled);
		 
		try {
			Long companyId=Long.parseLong(req.getParameter("companyId"));
			String appId = req.getParameter("appId");
			String companyName = req.getParameter("companyName");
			String loggedinuser=req.getParameter("loggedinuser");
			String otp=req.getParameter("otp");
			String approverCellNo=req.getParameter("approverCellNo");
			logger.log(Level.SEVERE,"in resetAppid api company id= "+companyId+" appid="+appId+" companyName="+companyName+" loggedinuser="+loggedinuser+" otp="+otp);
			Company company=ofy().load().type(Company.class).filter("companyId", companyId).first().now();
			if(company==null) {
				resp.getWriter().write("Wrong Company Id");
				return;
			}else {
//				if(!company.getBusinessUnitName().equalsIgnoreCase(companyName)) {
//					resp.getWriter().write("Company Name Entered is wrong");
//					logger.log(Level.SEVERE,"company.getBusinessUnitName()="+company.getBusinessUnitName());
//
//					return;
//				}
					
				if(company.getCompanyURL()!=null&&!company.getCompanyURL().equals("")) {
					if(!company.getCompanyURL().contains(appId)) {
						resp.getWriter().write("Appid entered is wrong");
						return;
					}
				}
			}
			
			ServerAppUtility utility=new ServerAppUtility();
			int licCount=utility.getNumberOfActiveLicense(company, AppConstants.LICENSETYPELIST.EVA_ERP_LICENSE);
			if(licCount>0) {
				resp.getWriter().write("Client still have active erp license. You cannot reset the appid.");
				return;
			}
			
			ArrayList<LicenseDetails> licenseDetailsList=company.getLicenseDetailsList();
			
			Comparator<LicenseDetails> endDateComparator=new Comparator<LicenseDetails>() {
				@Override
				public int compare(LicenseDetails arg0, LicenseDetails arg1) {
					return arg0.getEndDate().compareTo(arg1.getEndDate());
				}
			};

			Collections.sort(licenseDetailsList, endDateComparator);
						
			
			Queue queue = QueueFactory.getQueue("documentCancellation-queue");	
			String taskName="ResetAppid"+"$"+companyId+"$"+appId+"$"+companyName+"$"+loggedinuser+"$"+otp+"$"+approverCellNo;
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));
			
			resp.getWriter().write("Data deletion Process Started!");
		}
		catch(Exception e) {
			e.printStackTrace();
			resp.getWriter().write("Failed");
		}
			
		
			
	}	
}
