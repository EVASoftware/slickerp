package com.slicktechnologies.server.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class PDFLinkURLAPI extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3802884521080408865L;
	Logger logger = Logger.getLogger("Logger");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		resp.setHeader("Access-Control-Allow-Origin", "*");

		String urlCalled = req.getRequestURL().toString().trim();
		logger.log(Level.SEVERE, "urlCalled" + urlCalled);
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl::" + splitUrl);
		
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		
		long companyId = comp.getCompanyId();
		
		String[] splitURL2 = urlCalled.split("slick_erp/");
		logger.log(Level.SEVERE, "splitURL2::" + splitURL2[0]);

		 String updatedurlCalled = splitURL2[0]+"slick_erp/";
		 logger.log(Level.SEVERE, "updated urlCalled" + updatedurlCalled);
		 
		   /**
			 * @author Vijay Date :- 15-12-2022
			 * Des :- when API call from customer portal then auth code will validate
			 * if auth code is validated then only process the API 
			 */

//		 		try {
//		 			String strcompanyId = req.getParameter("authCode");
//					ServerAppUtility apputility = new ServerAppUtility();
//					boolean authonticationflag = apputility.validateAuthCode(strcompanyId,comp.getCompanyId());
//					logger.log(Level.SEVERE, "authonticationflag"+authonticationflag);
//					if(!authonticationflag) {
//						resp.getWriter().println("Authentication Failed!");
//						return;
//					}
//				} catch (Exception e) {
//					 TODO: handle exception
//				}
				
			/**
			 * ends here
			 */
			
			
			String documentName = req.getParameter("documentName");
			String strDocumentId = req.getParameter("documentId");
			
			String salarySlipMonth="";
			if(documentName.equals("Salary Slip") && req.getParameter("month")!=null) {
				try {
					salarySlipMonth = req.getParameter("month");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			int documentId = Integer.parseInt(strDocumentId);
			CommonServiceImpl commonserviceimpl = new CommonServiceImpl();
			String pdflink = commonserviceimpl.getPDFURL(documentName, null, documentId, companyId, comp, updatedurlCalled,salarySlipMonth);
			logger.log(Level.SEVERE,"pdflink "+pdflink);
			JSONObject jsonobj = new JSONObject();
			try {
				jsonobj.put("pdfUrl", pdflink);
				
				String jsonstring = jsonobj.toString().replaceAll("\\\\", "");
				logger.log(Level.SEVERE, "Json"+jsonstring);
				resp.getWriter().println(jsonstring);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
	}	
}
