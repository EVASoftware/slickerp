package com.slicktechnologies.server.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.simplesoftwares.client.library.ReturnFromServer;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class DigitalPaymentAPI  extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 964281848125558407L;
	
	Logger logger = Logger.getLogger("Logger");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		resp.setHeader("Access-Control-Allow-Origin", "*");
		
		String urlCalled = req.getRequestURL().toString().trim();
		logger.log(Level.SEVERE, "urlCalled" + urlCalled);
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl::" + splitUrl);
		
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();

		String[] splitURL2 = urlCalled.split("slick_erp/");
		logger.log(Level.SEVERE, "splitURL2::" + splitURL2[0]);

		 String updatedurlCalled = splitURL2[0]+"slick_erp/";
		 logger.log(Level.SEVERE, "updated urlCalled" + updatedurlCalled);
		 
		 String Appid = "";
			if(urlCalled.contains("-dot-")){
				String [] urlarray = urlCalled.split("-dot-");
				 Appid = urlarray[1];
			}
			else{
				String [] urlarray = urlCalled.split("\\.");
				 Appid = urlarray[1];
			}
		logger.log(Level.SEVERE, "Appid " + Appid);
		long companyId = comp.getCompanyId();
		

		/**
		 * @author Vijay Date :- 15-12-2022
		 * Des :- when API call from customer portal then auth code will validate
		 * if auth code is validated then only process the API 
		 */
//		String strcompanyId = req.getParameter("authCode");
		ServerAppUtility apputility = new ServerAppUtility();
//		boolean authonticationflag = apputility.validateAuthCode(strcompanyId,comp.getCompanyId());
//		logger.log(Level.SEVERE, "authonticationflag"+authonticationflag);
//		if(!authonticationflag) {
//			resp.getWriter().println("Authentication Failed!");
//			return;
//		}
		/**
		 * ends here
		 */
			try {
				
			String documentName = req.getParameter("documentName");
			String strDocumentId = req.getParameter("documentId");
			int documentId = Integer.parseInt(strDocumentId);
			

			
			if(documentName.trim().equals(AppConstants.CONTRACTRENEWAL)) {
				String strresponse = getLandingpageDetails(comp,documentId,companyId,updatedurlCalled,splitURL2,Appid);
				
				resp.getWriter().println(strresponse);
				
//				ContractRenewal contractRenewalEnitity = ofy().load().type(ContractRenewal.class).filter("contractId", documentId)
//								.filter("companyId", comp.getCompanyId()).first().now();
//				if(contractRenewalEnitity!=null && contractRenewalEnitity.getStatus().equals(AppConstants.PROCESSED)) {
//					String landingPagelink = AppConstants.PAYMENTGATEWAYSMSLINK+contractRenewalEnitity.getPaymentGatewayUniqueId();
//					resp.getWriter().println(landingPagelink);
//				}
//				else {
//					SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
//
//					Contract contractEnity = ofy().load().type(Contract.class).filter("companyId", comp.getCompanyId()).filter("count", documentId)
//							.first().now();
//					if(contractEnity!=null) {
//						Date newcontractStartDate=null;
//
//						for(SalesLineItem lineItem : contractEnity.getItems()){
//							
//							Date previousEndDate = contractEnity.getItems().get(0).getEndDate();
//							Calendar cal=Calendar.getInstance();
//							cal.setTime(previousEndDate);
//							cal.add(Calendar.DATE, +1);
//							
//							try {
//								newcontractStartDate=dateFormat.parse(dateFormat.format(cal.getTime()));
//								
//								Calendar cal2=Calendar.getInstance();
//								cal2.setTime(newcontractStartDate);
//								cal2.add(Calendar.DATE, lineItem.getDuration());
//								
//								Date newcontractEndDate=null;
//								
//								try {
//									newcontractEndDate=dateFormat.parse(dateFormat.format(cal2.getTime()));
//									
//									lineItem.setStartDate(newcontractStartDate);
//									lineItem.setEndDate(newcontractEndDate);
//									
//								} catch (ParseException e) {
//									e.printStackTrace();
//								}
//								
//							} catch (ParseException e) {
//								e.printStackTrace();
//							}
//							
//						}
//						
//						ContractRenewal contractRenewalEntity = new ContractRenewal();
//						contractRenewalEntity.setBranch(contractEnity.getBranch());
//						contractRenewalEntity.setCompanyId(contractEnity.getCompanyId());
//						contractRenewalEntity.setContractDate(contractEnity.getContractDate());
//						contractRenewalEntity.setContractId(contractEnity.getCount());
//						contractRenewalEntity.setContractEndDate(contractEnity.getEndDate());
//						contractRenewalEntity.setContractType(contractEnity.getType());
//						contractRenewalEntity.setCustomerId(contractEnity.getCinfo().getCount());
//						contractRenewalEntity.setCustomerName(contractEnity.getCinfo().getFullName());
//						contractRenewalEntity.setCustomerCell(contractEnity.getCinfo().getCellNumber());
//						contractRenewalEntity.setDate(newcontractStartDate);
//						contractRenewalEntity.setItems(contractEnity.getItems());
//						contractRenewalEntity.setNetPayable(contractEnity.getNetpayable());
//						contractRenewalEntity.setSalesPerson(contractEnity.getEmployee());
//						contractRenewalEntity.setStatus(AppConstants.PROCESSED);
//						
//						Customer cust=ofy().load().type(Customer.class).filter("companyId", contractEnity.getCompanyId())
//											.filter("count",contractEnity.getCinfo().getCount()).first().now();
//
//						if(cust!=null){
//							contractRenewalEntity.setCity(cust.getSecondaryAdress().getCity());
//							if(cust.getSecondaryAdress().getLocality()!=null){
//								contractRenewalEntity.setLocality(cust.getSecondaryAdress().getLocality());
//							}else{
//								contractRenewalEntity.setLocality("");
//							}
//						}else{
//							contractRenewalEntity.setCity("");
//							contractRenewalEntity.setLocality("");
//						}
//						
//						GenricServiceImpl genimpl = new GenricServiceImpl();
//						ReturnFromServer server = genimpl.save(contractRenewalEntity);
//						int contractRenewalId = server.count;
//						
//						if(contractEnity!=null){
//							contractEnity.setRenewProcessed(AppConstants.YES);
//							ofy().save().entity(contractEnity).now();
//						}
//						
//						CommonServiceImpl commonserviceimpl = new CommonServiceImpl();
//						
//						String pdflink = commonserviceimpl.getPDFURL(AppConstants.CONTRACT_RENEWAL, contractRenewalEntity, contractRenewalId, companyId, comp,updatedurlCalled);
//						logger.log(Level.SEVERE," pdflink "+pdflink);
//						String jsonData = commonserviceimpl.createJsonData(contractRenewalEntity,AppConstants.CONTRACT_RENEWAL, splitURL2[0], Appid, comp, cust,pdflink,"");
//						logger.log(Level.SEVERE," jsonData "+jsonData);
//						String paymentGatewayUniqueId = commonserviceimpl.callPaymnetGatewayUniqueIdAPI(jsonData, contractRenewalEntity, "ContractRenewal");
//						contractRenewalEntity.setPaymentGatewayUniqueId(paymentGatewayUniqueId);
//						ofy().save().entity(contractRenewalEntity);
//						
//						String landingPagelink = AppConstants.PAYMENTGATEWAYSMSLINK+paymentGatewayUniqueId;
//						logger.log(Level.SEVERE, "landingPagelink"+landingPagelink);
//						
//						resp.getWriter().println(landingPagelink);
//
//					}
//					else {
//						resp.getWriter().println("Contract not found!");
//					}
//				
//				}
//				
			}
			else if(documentName.equals("Invoice")){
				String strresponse = getInvoiceLandingPageDetails(comp,documentId,companyId,updatedurlCalled,splitURL2,Appid);
				resp.getWriter().println(strresponse);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	private String getInvoiceLandingPageDetails(Company comp, int documentId,
			long companyId, String updatedurlCalled, String[] splitURL2,String appid) {
		
		CommonServiceImpl commonserviceimpl = new CommonServiceImpl();

		Invoice invoiceEntity = ofy().load().type(Invoice.class).filter("companyId",companyId).filter("count", documentId)
				.first().now();
		if(invoiceEntity!=null){
			if(invoiceEntity!=null && invoiceEntity.getPaymentGatewayUniqueId()!=null && !invoiceEntity.getPaymentGatewayUniqueId().equals("")){
				String landingPagelink = AppConstants.PAYMENTGATEWAYSMSLINK+invoiceEntity.getPaymentGatewayUniqueId();
				return landingPagelink;
			}
			else{
				Customer customer = ofy().load().type(Customer.class).filter("companyId", companyId).filter("count", invoiceEntity.getPersonInfo().getCount()).first().now();
				String pdflink = commonserviceimpl.getPDFURL("Invoice", invoiceEntity, invoiceEntity.getCount(), companyId, comp, updatedurlCalled,null);
				logger.log(Level.SEVERE, "invoice pdflink "+pdflink);
				String jsonData = commonserviceimpl.createJsonData(invoiceEntity,"Invoice", splitURL2[0], appid, comp, customer,pdflink,"");
				logger.log(Level.SEVERE," jsonData "+jsonData);
				
				String paymentGatewayUniqueId = commonserviceimpl.callPaymnetGatewayUniqueIdAPI(jsonData, invoiceEntity, "Invoice");
				invoiceEntity.setPaymentGatewayUniqueId(paymentGatewayUniqueId);
				ofy().save().entity(invoiceEntity);
				
				String landingPagelink = AppConstants.PAYMENTGATEWAYSMSLINK+paymentGatewayUniqueId;
				logger.log(Level.SEVERE, "landingPagelink"+landingPagelink);
				return landingPagelink;
			}
		}
		else{
			ServerAppUtility apputility = new ServerAppUtility();
			return apputility.getMessageInJson("Document id does not exist!");
		}
		
		
	}

	/**@author Sheetal
     * @since 13-05-2022
     * Des : Making Common method to get digitalpaymentlink			 
     * */

	public String getLandingpageDetails(Company comp, int documentId, long companyId, String updatedurlCalled, String[] splitURL2, String Appid) {
		

		ContractRenewal contractRenewalEnitity = ofy().load().type(ContractRenewal.class).filter("contractId", documentId)
						.filter("companyId", comp.getCompanyId()).first().now();
		if(contractRenewalEnitity!=null && contractRenewalEnitity.getStatus().equals(AppConstants.PROCESSED)) {
			String landingPagelink = AppConstants.PAYMENTGATEWAYSMSLINK+contractRenewalEnitity.getPaymentGatewayUniqueId();
			return landingPagelink;
		}
		else {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

			Contract contractEnity = ofy().load().type(Contract.class).filter("companyId", comp.getCompanyId()).filter("count", documentId)
					.first().now();
			if(contractEnity!=null) {
				Date newcontractStartDate=null;

				ArrayList<SalesLineItem> renewalItemList=new ArrayList<SalesLineItem>();
				for(SalesLineItem salesItem : contractEnity.getItems()){
					logger.log(Level.SEVERE, salesItem.getProductName()+" startdate="+salesItem.getStartDate());
					/**
					 * @author Vijay Date - 08-05-2023
					 * Des :- contract start date end date changed so updated code 
					 */
					SalesLineItem lineItem = new SalesLineItem();
					lineItem = getcopyofSalesLineItem(salesItem);
					/**
					 * ends here
					 */
					
					Date previousEndDate = lineItem.getEndDate();
					Calendar cal=Calendar.getInstance();
					cal.setTime(previousEndDate);
					cal.add(Calendar.DATE, +1);
					
					try {
						newcontractStartDate=dateFormat.parse(dateFormat.format(cal.getTime()));
						
						Calendar cal2=Calendar.getInstance();
						cal2.setTime(newcontractStartDate);
						cal2.add(Calendar.DATE, lineItem.getDuration());
						
						Date newcontractEndDate=null;
						
						try {
							newcontractEndDate=dateFormat.parse(dateFormat.format(cal2.getTime()));
							
							lineItem.setStartDate(newcontractStartDate);
							logger.log(Level.SEVERE, lineItem.getProductName()+"new startdate="+lineItem.getStartDate());
							
							lineItem.setEndDate(newcontractEndDate);
							renewalItemList.add(lineItem);
						} catch (ParseException e) {
							e.printStackTrace();
						}
						
					} catch (ParseException e) {
						e.printStackTrace();
					}
					
				}
				
				ContractRenewal contractRenewalEntity = new ContractRenewal();
				contractRenewalEntity.setBranch(contractEnity.getBranch());
				contractRenewalEntity.setCompanyId(contractEnity.getCompanyId());
				contractRenewalEntity.setContractDate(contractEnity.getContractDate());
				contractRenewalEntity.setContractId(contractEnity.getCount());
				contractRenewalEntity.setContractEndDate(contractEnity.getEndDate());
				contractRenewalEntity.setContractType(contractEnity.getType());
				contractRenewalEntity.setCustomerId(contractEnity.getCinfo().getCount());
				contractRenewalEntity.setCustomerName(contractEnity.getCinfo().getFullName());
				contractRenewalEntity.setCustomerCell(contractEnity.getCinfo().getCellNumber());
				contractRenewalEntity.setDate(newcontractStartDate);
				contractRenewalEntity.setItems(renewalItemList);
				contractRenewalEntity.setNetPayable(contractEnity.getNetpayable());
				contractRenewalEntity.setSalesPerson(contractEnity.getEmployee());
				contractRenewalEntity.setStatus(AppConstants.PROCESSED);
				
				Customer cust=ofy().load().type(Customer.class).filter("companyId", contractEnity.getCompanyId())
									.filter("count",contractEnity.getCinfo().getCount()).first().now();

				if(cust!=null){
					contractRenewalEntity.setCity(cust.getSecondaryAdress().getCity());
					if(cust.getSecondaryAdress().getLocality()!=null){
						contractRenewalEntity.setLocality(cust.getSecondaryAdress().getLocality());
					}else{
						contractRenewalEntity.setLocality("");
					}
				}else{
					contractRenewalEntity.setCity("");
					contractRenewalEntity.setLocality("");
				}
				
				GenricServiceImpl genimpl = new GenricServiceImpl();
				ReturnFromServer server = genimpl.save(contractRenewalEntity);
				int contractRenewalId = server.count;
				
				if(contractEnity!=null){
					contractEnity.setRenewProcessed(AppConstants.YES);
					ofy().save().entity(contractEnity).now();
				}
				
				CommonServiceImpl commonserviceimpl = new CommonServiceImpl();
				
				String pdflink = commonserviceimpl.getPDFURL(AppConstants.CONTRACT_RENEWAL, contractRenewalEntity, contractRenewalId, companyId, comp,updatedurlCalled,null);
				logger.log(Level.SEVERE," pdflink "+pdflink);
				String jsonData = commonserviceimpl.createJsonData(contractRenewalEntity,AppConstants.CONTRACT_RENEWAL, splitURL2[0], Appid, comp, cust,pdflink,"");
				logger.log(Level.SEVERE," jsonData "+jsonData);
				String paymentGatewayUniqueId = commonserviceimpl.callPaymnetGatewayUniqueIdAPI(jsonData, contractRenewalEntity, "ContractRenewal");
				contractRenewalEntity.setPaymentGatewayUniqueId(paymentGatewayUniqueId);
				ofy().save().entity(contractRenewalEntity);
				
				String landingPagelink = AppConstants.PAYMENTGATEWAYSMSLINK+paymentGatewayUniqueId;
				logger.log(Level.SEVERE, "landingPagelink"+landingPagelink);
				
				return landingPagelink;

			}
			else {
				ServerAppUtility apputility = new ServerAppUtility();
				return apputility.getMessageInJson("Contract not found!");
			}
		
		}
		

	}

	/**
	 * @author Vijay
	 * Des :- getting contract start end date changed by contract renewal cronjob
	 */
	private SalesLineItem getcopyofSalesLineItem(SalesLineItem salesItem) {
		
		SalesLineItem item = new SalesLineItem();
		
		item.setPrduct(salesItem.getPrduct());
		item.setProductName(salesItem.getProductName());
		item.setPrice(salesItem.getPrice());
		item.setProductSrNo(salesItem.getProductSrNo()); 
		item.setDuration(salesItem.getDuration());
		
		item.setPercentageDiscount(salesItem.getPercentageDiscount());
		item.setFlatDiscount(salesItem.getFlatDiscount());
		item.setOldProductPrice(salesItem.getOldProductPrice());
		item.setPremisesDetails(salesItem.getPremisesDetails());
		item.setArea(salesItem.getArea());
		item.setRemark(salesItem.getRemark());
		item.setInclusive(salesItem.isInclusive());
		item.setAmcNumber(salesItem.getAmcNumber());
		item.setComplainService(salesItem.isComplainService());
		item.setAreaUnit(salesItem.getAreaUnit());
		item.setAreaUnitEditValue(salesItem.getAreaUnitEditValue());
		item.setServiceProdGroupList(salesItem.getServiceProdGroupList());
		item.setProSerialNoDetails(salesItem.getProSerialNoDetails());
		item.setServiceBranchesInfo(salesItem.getServiceBranchesInfo());
		item.setServiceRate(salesItem.isServiceRate());
		item.setRefproductmasterTax1(salesItem.getRefproductmasterTax1());
		item.setRefproductmasterTax2(salesItem.getRefproductmasterTax2());
		item.setTermsoftreatment(salesItem.getTermsoftreatment());
		item.setProfrequency(salesItem.getProfrequency());											
		item.setNumberOfService(salesItem.getNumberOfServices());
		item.setStartDate(salesItem.getStartDate());
		item.setEndDate(salesItem.getEndDate());
		item.setQuantity(salesItem.getQty());
		item.setVatTax(salesItem.getVatTax());
		item.setTotalAmount(salesItem.getTotalAmount());
		item.setServiceTax(salesItem.getServiceTax());
		item.setVatTaxEdit(salesItem.getVatTaxEdit());
		item.setServiceTaxEdit(salesItem.getServiceTaxEdit());
		
		item.setBranchSchedulingInfo(salesItem.getBranchSchedulingInfo());

		item.setProModelNo(salesItem.getProModelNo());
		item.setProSerialNo(salesItem.getProSerialNo());
		
		return item;
	}

	private int getMaxDuration(List<SalesLineItem> items) {
		
			int duration = items.get(0).getDuration();
			for(SalesLineItem salesline : items){
				if(salesline.getDuration()>duration){
					duration = salesline.getDuration();
				}
			}
			return duration;
	}
		

}
