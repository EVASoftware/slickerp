package com.slicktechnologies.server.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.FeedbackUrlAsPerCustomerCategory;

public class ServiceFeedbackAPI  extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2946353115380162483L;


	Logger logger = Logger.getLogger("Logger");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		resp.setHeader("Access-Control-Allow-Origin", "*");
		
		String urlCalled = req.getRequestURL().toString().trim();
		logger.log(Level.SEVERE, "urlCalled" + urlCalled);
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl::" + splitUrl);
		
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		
		long companyId = comp.getCompanyId();
		
		/**
		 * @author Vijay Date :- 15-12-2022
		 * Des :- when API call from customer portal then auth code will validate
		 * if auth code is validated then only process the API 
		 */
//		String strcompanyId = req.getParameter("authCode");
		ServerAppUtility apputility = new ServerAppUtility();
//		boolean authonticationflag = apputility.validateAuthCode(strcompanyId,comp.getCompanyId());
//		logger.log(Level.SEVERE, "authonticationflag"+authonticationflag);
//		if(!authonticationflag) {
//			resp.getWriter().println("Authentication Failed!");
//			return;
//		}
		
			String strDocumentId = req.getParameter("documentId");
			int documentId = Integer.parseInt(strDocumentId);
			Service serviceEntity = ofy().load().type(Service.class).filter("companyId", comp.getCompanyId()).filter("count", documentId).first().now();
			logger.log(Level.SEVERE, "serviceEntity " + serviceEntity);
			if(serviceEntity!=null) {
				if(serviceEntity.getStatus().equals(Service.SERVICESTATUSCOMPLETED)) {
					if(comp!=null&&comp.getFeedbackUrlList()!=null&&comp.getFeedbackUrlList().size()!=0){
						
						String feedbackFormUrl="";
						Customer customer=ofy().load().type(Customer.class).filter("companyId",serviceEntity.getCompanyId()).filter("count",serviceEntity.getPersonInfo().getCount()).first().now();
						if(customer!=null){
							if(customer.getCategory()!=null&&!customer.getCategory().equals("")){
								for(FeedbackUrlAsPerCustomerCategory feedback:comp.getFeedbackUrlList()){
									if(feedback.getCustomerCategory().equals(customer.getCategory())){
										feedbackFormUrl=feedback.getFeedbackUrl();
										break;
									}
								}
							}
						}
						
						if(feedbackFormUrl!=null&&!feedbackFormUrl.equals("")){
							logger.log(Level.SEVERE, "ZOHO FEEDBACK URL " + feedbackFormUrl);
							StringBuilder sbPostData= new StringBuilder(feedbackFormUrl);
					        sbPostData.append("?customerId="+serviceEntity.getPersonInfo().getCount());
					        sbPostData.append("&customerName="+serviceEntity.getPersonInfo().getFullName());
					        sbPostData.append("&serviceId="+serviceEntity.getCount());
					        sbPostData.append("&serviceDate="+serviceEntity);
					        sbPostData.append("&serviceName="+serviceEntity.getProductName());
					        sbPostData.append("&technicianName="+serviceEntity.getEmployee());
					        sbPostData.append("&branch="+serviceEntity.getBranch());
					        feedbackFormUrl = sbPostData.toString();
					        feedbackFormUrl=feedbackFormUrl.replaceAll("\\s", "%20");
					        feedbackFormUrl=feedbackFormUrl.replaceAll("&", "%26");
					        /**
					         * @author Anil @since 10-03-2021
					         * Modified feedback Url i.e. tiny utl
					         */
					        feedbackFormUrl=ServerAppUtility.getTinyUrl(feedbackFormUrl,comp.getCompanyId());
					        
					        logger.log(Level.SEVERE, "FINAL ZOHO FEEDBACK URL " + feedbackFormUrl);
							resp.getWriter().println(feedbackFormUrl);

						}

						
					}
					else {
						resp.getWriter().println(apputility.getMessageInJson("Feedback url is not configured in the company screen"));
					}

				}
				else {
					resp.getWriter().println(apputility.getMessageInJson("service status should be completed!"));

				}
			}
			else {
				resp.getWriter().println(apputility.getMessageInJson("service not found!"));

			}
									
	}
}
