package com.slicktechnologies.server.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;

public class GetEmployeeLeaveBalance  extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2848312687979636532L;

	Logger logger = Logger.getLogger("Logger");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		resp.setHeader("Access-Control-Allow-Origin", "*");
	
		String urlCalled = req.getRequestURL().toString().trim();
		logger.log(Level.SEVERE, "urlCalled" + urlCalled);
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl::" + splitUrl);
		
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		
		String validationMsg="";
		
		String stremployeeId = req.getParameter("employeeId").trim();
		int employeeId = 0;
		
		try {
			employeeId = Integer.parseInt(stremployeeId);
		} catch (Exception e) {
			validationMsg +="employee id can not be blank";
		}
		if(employeeId==0) {
			validationMsg +="employee id can not be zero";
		}
		logger.log(Level.SEVERE, "validationMsg" + validationMsg);
		
		if(validationMsg.equals("")) {
			LeaveBalance leaveBalanceEntity = ofy().load().type(LeaveBalance.class).filter("companyId", comp.getCompanyId())
											.filter("empInfo.empCount", employeeId).first().now();
			logger.log(Level.SEVERE, "leaveBalanceEntity" + leaveBalanceEntity);
			if(leaveBalanceEntity!=null) {

				JSONObject jobj = new JSONObject();
				JSONArray jsonarray = new JSONArray();

				for(AllocatedLeaves allocatedleave : leaveBalanceEntity.getAllocatedLeaves()) {
					JSONObject jsonobj = new JSONObject();

					try {
						
						if(allocatedleave.getName()!=null){
							 jsonobj.put("Leave Name", allocatedleave.getName());
						}else{
							 jsonobj.put("Leave Name","");
						}
						
						if(allocatedleave.getShortName()!=null){
							 jsonobj.put("Short Name", allocatedleave.getShortName());
						}else{
							 jsonobj.put("Short Name","");
						}
						
						if(allocatedleave.getShortName()!=null){
							 jsonobj.put("Leave Balance", allocatedleave.getBalance());
						}else{
							 jsonobj.put("Leave Balance","");
						}
						
						jsonarray.put(jsonobj);
						
						
					} catch (Exception e) {
					}
					
				}
				
				try {
					
					jobj.put("Data", jsonarray);
					String jsonstring = jobj.toString().replaceAll("\\\\", "");
					logger.log(Level.SEVERE, "Json"+jsonstring);
					resp.getWriter().println(jsonstring);
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
			else {
				resp.getWriter().println("Leave Balance not found for this employee id "+employeeId+"");
	
			}
			
		}
		else {
			resp.getWriter().println(validationMsg);
		}
	}
}
