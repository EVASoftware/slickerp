package com.slicktechnologies.server.licenseupdation;

import java.util.Date;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.slicktechnologies.client.services.LicenseUpdationService;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import static com.googlecode.objectify.ObjectifyService.ofy;

public class LicenseUpdationServiceImpl extends RemoteServiceServlet implements LicenseUpdationService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -129275843728554193L;

	@Override
	public Integer updateLicenses(long companyId, int noOfUsers,Date licenseUpdateDate) {
		Company companyEntity=ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		
		System.out.println("lic upadte s imp ");
		
		if(companyEntity!=null)
		{
			System.out.println("update == "+licenseUpdateDate);
			System.out.println("company id  = === "+companyId);
			
			companyEntity.setLicenseEndingDate(licenseUpdateDate);
			companyEntity.setNoOfUser(noOfUsers);
			ofy().save().entity(companyEntity).now();
			
			return 1;
		}
		else{
			return 0;
		}
	}

	@Override
	public Company retrieveCompanyInfo(long companyId) {
		Company companyData=ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		if(companyData!=null)
		{
			return companyData;
		}
		
		return null;
	}

}
