package com.slicktechnologies.server.compositeimplementor;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.cmd.QueryKeys;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.OneToManyComposite;
import com.simplesoftwares.client.library.libservice.OneToManyCompositeService;
import com.slicktechnologies.server.GenricServiceImpl;
// TODO: Auto-generated Javadoc
/**
 * Provides functionality to {@link OneToManyComposite} .
 * TO DO: Can U think data model independent implementation design of LoadOneToManyHashMap
 * 
 * @param <T> One Type One relation kind
 * @param <C> Many Type Many relation kind
 */
@SuppressWarnings("rawtypes")
public class OneToManyCompositeImplementor< T extends SuperModel,C extends SuperModel>   extends RemoteServiceServlet implements OneToManyCompositeService
{

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 163882473205308674L;



	
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.libservice.OneToManyCompositeService#LoadOneToManyHashMap(com.simplesoftwares.client.library.appstructure.search.MyQuerry, com.simplesoftwares.client.library.appstructure.search.MyQuerry)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public HashMap<T,ArrayList<C>> LoadOneToManyHashMap(MyQuerry oneQuerry,MyQuerry manyQuerry) {
		
		
		GenricServiceImpl impl=new GenricServiceImpl();
		//Holds the one type keys;
		ArrayList<Key<?>> keyarraylists=new ArrayList<Key<?>>();
		//Get all keys of one
		System.out.println("One----------------------------------------------------------");
		ArrayList<SuperModel> onelist=impl.getSearchResult(oneQuerry);
		
		//System.out.println("Size of OneList **********************************  "+onelist.size());
	
		for(SuperModel temp:onelist)
		{
			Key<?>key=Key.create(temp);
			keyarraylists.add(key);
		}
				
		//This hash map we have to fill
		
		HashMap<T,ArrayList<C>>hashmap=new HashMap<T,ArrayList<C>>();
		
		//create Hash Map
		
		ArrayList<C>manylist=new ArrayList<C>();
		int i=0;
		System.out.println("Many Querry Inside--------------+ "+manylist.size());
		for(SuperModel model:onelist)
		{
			
			manyQuerry=createMyQerry(manyQuerry,keyarraylists.get(i));
			
			ArrayList<SuperModel>templist=impl.getSearchResult(manyQuerry);
			System.out.println("Many Querry Size "+templist.size());
			
			manylist=new ArrayList<C>();
			for(SuperModel m:templist)
				manylist.add((C) m);
			
            
			
			hashmap.put((T) model,manylist);
		
			i++;
			
		}
		return hashmap;
	}



/**
 * Creates the my qerry.
 *
 * @param querry many querry which contains only connecting string
 * @param key key of one
 * @return the my querry returns many querry with value i.e is key
 */
private MyQuerry createMyQerry(MyQuerry querry,Key<?>key)
{
	Vector<Filter>myfiltervalue=querry.getFilters();
	Filter filtervalue=myfiltervalue.get(0);
	String connector=filtervalue.getQuerryString();
	Vector<Filter> temp=new Vector<Filter>();
	Filter value=new Filter();
	value.setQuerryString(connector);
	value.setObjectifykey(key);
	temp.add(value);
	querry.setFilters(temp);
	System.out.println("Value set objectify key "+value.geFilterValue());
	return querry;
}




}