package com.slicktechnologies.server.compositeimplementor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.ObjectifyService;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.PersonInfoService;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.personlayer.Person;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class PersonInfoCompositeImplementor extends RemoteServiceServlet implements PersonInfoService
{

	Logger logger=Logger.getLogger("Person Info Composite Logger");
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4080349707473338405L;

	@Override
	public ArrayList<PersonInfo> LoadVinfo(MyQuerry querry, String loggedinUserName)
	{
		logger.log(Level.SEVERE,"Executing On Server Side");
		
		/**
		 * DATE : 15-05-2017 By ANIL
		 * Checking ,is customer approval process active or not?
		 */
//		boolean custAppFlag=false;
//		ProcessConfiguration processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", querry.getFilters().get(0).getLongValue()).filter("processName", "Customer").filter("configStatus", true).first().now();
//		if(processConfig!=null){
//			for(int k=0;k<processConfig.getProcessList().size();k++){
//				if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("CustomerApprovalProcess")
//						&&processConfig.getProcessList().get(k).isStatus()==true){
//					custAppFlag=true;
//				}
//			}
//		}
		/**
		 * End
		 */
		
		ArrayList<PersonInfo> personinfoarray=new ArrayList<PersonInfo>();

		
		/**
		 * Changed By Dipak For Composite Issue on 14 May 2015
		 */
		
		try {
			
		String[] classType=querry.getQuerryObject().getClass().toString().trim().split("\\s+");
		logger.log(Level.SEVERE,"Class Type In Querry"+classType);
		String strcname=classType[1];
		logger.log(Level.SEVERE,"Class Path"+strcname);
		String classNameArray[]=strcname.split("\\.");
		String classNameValue=classNameArray[classNameArray.length-1];
		logger.log(Level.SEVERE,"Name Of Class Loaded"+classNameValue);
		
		
		List<Customer> custList=new ArrayList<Customer>();
		List<Vendor> venList=new ArrayList<Vendor>();
		ArrayList<SuperModel> arraylis=new ArrayList<SuperModel>();
		if(!classNameValue.equals("")||classNameValue!=null)
		{
			logger.log(Level.SEVERE,"Retrieving Data On Server Side");
			if(AppConstants.PERSONCOMPOSITECUSTOMER.equals(classNameValue.trim())){
				
				try{
					logger.log(Level.SEVERE,"Try Block");
					/**
					 * Date : 15-05-2017 By ANIL
					 */
//					if(custAppFlag){
//						custList=ofy().load().type(Customer.class).filter("companyId", querry.getFilters().get(0).getLongValue()).filter("businessProcess.status", "Active").list();
//					}else{
//						custList=ofy().load().type(Customer.class).filter("companyId", querry.getFilters().get(0).getLongValue()).list();
//					}
					/**
					 * End
					 */
					logger.log(Level.SEVERE,"loggedinUserName "+loggedinUserName);
					long companyId = querry.getFilters().get(0).getLongValue();
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BRANCHLEVELRESTRICTION", companyId) &&
							ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", AppConstants.PC_LOADCUSTOMERWITHBRANCHLEVELRESTRICTION,companyId) && !loggedinUserName.equals("") ){
						
						User userEntity = ofy().load().type(User.class).filter("companyId", companyId).filter("employeeName", loggedinUserName)
											.first().now();
						logger.log(Level.SEVERE,"userEntity "+userEntity);
						logger.log(Level.SEVERE,"userEntity.getRole().getRoleName() "+userEntity.getRole().getRoleName());
						logger.log(Level.SEVERE,"userEntity =="+(!userEntity.getRole().getRoleName().equalsIgnoreCase("Admin")));

						if(!userEntity.getRole().getRoleName().equalsIgnoreCase("Admin")){
							Employee employeEntity = ofy().load().type(Employee.class).filter("companyId", companyId)
													.filter("fullname", loggedinUserName).first().now();
							logger.log(Level.SEVERE,"employeEntity "+employeEntity);
							HashSet<String> hsbranchName = new HashSet<String>();
							hsbranchName.add(employeEntity.getBranchName());
							if(employeEntity.getEmpBranchList().size()!=0){
								for(EmployeeBranch empbranch : employeEntity.getEmpBranchList() ){
									hsbranchName.add(empbranch.getBranchName());
								}
							}
							logger.log(Level.SEVERE,"hsbranchName "+hsbranchName.size());
							ArrayList<String> branchlist = new ArrayList<String>(hsbranchName);
							logger.log(Level.SEVERE,"branchlist "+branchlist);
	
							custList=ofy().load().type(Customer.class).filter("businessProcess.branch IN", branchlist)
									.filter("companyId", companyId).list();
							logger.log(Level.SEVERE,"custList size in branch restriction  "+custList.size());
						}
						else{
							custList=ofy().load().type(Customer.class).filter("companyId", querry.getFilters().get(0).getLongValue()).list();
							logger.log(Level.SEVERE,"user not loaded "+custList.size());


						}
					}
					else{
						custList=ofy().load().type(Customer.class).filter("companyId", querry.getFilters().get(0).getLongValue()).list();
						logger.log(Level.SEVERE,"normal flow "+custList.size());

					}
				}
				catch(Exception e){
					
					logger.log(Level.SEVERE,"Catch Block");
					e.printStackTrace();
				}
				
				logger.log(Level.SEVERE,"After OFY");
				ObjectifyService.reset();
				logger.log(Level.SEVERE,"After Reset");
			    ofy().consistency(Consistency.STRONG);
				logger.log(Level.SEVERE,"After====");
			    ofy().clear();
				logger.log(Level.SEVERE,"After Clear");
				arraylis.addAll(custList);
				logger.log(Level.SEVERE,"Customer Querry Size For Composite"+custList.size());
			}
			if(AppConstants.PERSONCOMPOSITEVENDOR.equals(classNameValue.trim())){
				venList=ofy().load().type(Vendor.class).filter("companyId", querry.getFilters().get(0).getLongValue()).list();
				ObjectifyService.reset();
			    ofy().consistency(Consistency.STRONG);
			    ofy().clear();
				arraylis.addAll(venList);
				logger.log(Level.SEVERE,"Vendor Querry Size For Composite"+venList.size());
			}
		}
		
		logger.log(Level.SEVERE,"Array Being Passed to Person Info"+arraylis.size());
		
	    for(SuperModel model:arraylis)
	    {
	    	PersonInfo perinfo=new PersonInfo();
	    	if(model instanceof Person)
	    	{
		    	Person per=(Person) model;
		    	
		    	//For Customers Please load company Name instead of Full Name
		    	if(model instanceof Customer)
		    	{
		    		Customer customer=(Customer) model;
		    		perinfo.setFullName(customer.getCompanyName());
		    		perinfo.setCount(per.getCount());
			    	perinfo.setCellNumber(per.getContact().get(0).getCellNo1());
			    	perinfo.setEmail(per.getContact().get(0).getEmail());
			    	
			    	/******************************Changes 20april2015**********************************/
			    	
			    	String pocNameVal=customer.getFullname();
	//		    	if(customer.getMiddleName()!=null){
	//		    		pocNameVal=customer.getFirstName().trim()+" "+customer.getMiddleName().trim()+" "+customer.getLastName().trim();
	//		    	}
	//		    	else{
	//		    		pocNameVal=customer.getFirstName().trim()+" "+customer.getLastName().trim();
	//		    	}
			    	perinfo.setPocName(pocNameVal);
			    	/**********************************************************************************/
			    	
			    	/**
			    	 * Date : 15-06-2017 BY ANIL
			    	 */
			    	
			    	perinfo.setStatus(customer.getStatus());
			    	/**
			    	 * End
			    	 */
			    	
			    	personinfoarray.add(perinfo);
		    	}
		    	else{
			    	perinfo.setFullName(per.getFullname());
			    	perinfo.setCount(per.getCount());
			    	perinfo.setCellNumber(per.getContact().get(0).getCellNo1());
			    	perinfo.setEmail(per.getContact().get(0).getEmail());
			    	
			    	personinfoarray.add(perinfo);
		    	}
	    	}
	    	else if(model instanceof Vendor)
	    	{
	    		Vendor customer=(Vendor) model;
	    		perinfo.setFullName(customer.getVendorName());
	    		perinfo.setCount(customer.getCount());
		    	perinfo.setCellNumber(customer.getCellNumber1());
		    	perinfo.setEmail(customer.getEmail());
		    	
		    	// vijay added to load vendor poc in composite
		    	String pocNameVal=customer.getfullName();
		    	perinfo.setPocName(pocNameVal);
		    	
		    	/**
		    	 * Date 10 April 2017 added by vijay
		    	 * for diffentiate the personinfo types is customer or vendor type
		    	 */
		    	perinfo.setVendor(true);
		    	/**
		    	 * ends here
		    	 */
		    	
		    	personinfoarray.add(perinfo);
	    	}
	    	
	    }
	    

	    
	    
	    
	    /***********************************End Of Changes**********************************/
//	    GenricServiceImpl impl=new GenricServiceImpl();
//	    ArrayList<SuperModel>arraylis=impl.getSearchResult(querry);
	    
//	    logger.log(Level.SEVERE,"After Get Search Result");
//	    logger.log(Level.SEVERE,"Size After Get Search Result"+arraylis.size());
//	    ArrayList<PersonInfo> personinfoarray=new ArrayList<PersonInfo>();
//	    for(SuperModel model:arraylis)
//	    {
//	    	PersonInfo perinfo=new PersonInfo();
//	    	if(model instanceof Person)
//	    	{
//	    	
//	    	Person per=(Person) model;
//	    	
//	    	//For Customers Please load company Name instead of Full Name
//	    	if(model instanceof Customer)
//	    	{
//	    		Customer customer=(Customer) model;
//	    		perinfo.setFullName(customer.getCompanyName());
//	    		perinfo.setCount(per.getCount());
//		    	perinfo.setCellNumber(per.getContact().get(0).getCellNo1());
//		    	perinfo.setEmail(per.getContact().get(0).getEmail());
//		    	
//		    	/******************************Changes 20april2015**********************************/
//		    	
//		    	String pocNameVal="";
//		    	if(customer.getMiddleName()!=null){
//		    		pocNameVal=customer.getFirstName().trim()+" "+customer.getMiddleName().trim()+" "+customer.getLastName().trim();
//		    	}
//		    	else{
//		    		pocNameVal=customer.getFirstName().trim()+" "+customer.getLastName().trim();
//		    	}
//		    	perinfo.setPocName(pocNameVal);
//		    	/**********************************************************************************/
//		    	
//		    	personinfoarray.add(perinfo);
//	    	}
//	    	else{
//	    	perinfo.setFullName(per.getFullname());
//	    	perinfo.setCount(per.getCount());
//	    	perinfo.setCellNumber(per.getContact().get(0).getCellNo1());
//	    	perinfo.setEmail(per.getContact().get(0).getEmail());
//	    	personinfoarray.add(perinfo);
//	    	}
//	    	}
//	    	else if(model instanceof Vendor)
//	    	{
//	    		Vendor customer=(Vendor) model;
//	    		perinfo.setFullName(customer.getVendorName());
//	    		perinfo.setCount(customer.getCount());
//		    	perinfo.setCellNumber(customer.getCellNumber1());
//		    	perinfo.setEmail(customer.getEmail());
//		    	personinfoarray.add(perinfo);
//	    	}
//	    	
//	    }
//	    
	    
		} catch (Exception e) {
			e.printStackTrace();
		}
	    logger.log(Level.SEVERE,"Returning Person Info Array"+personinfoarray.size());
	    
	    return personinfoarray;
		
	}

	@Override
	public ArrayList<PersonInfo> LoadPinfo(Long companyId, int countVal,String entityType) {
		
		/**
		 * DATE : 15-05-2017 By ANIL
		 * Checking ,is customer approval process active or not?
		 */
//		boolean custAppFlag=false;
//		ProcessConfiguration processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId).filter("processName", "Customer").filter("configStatus", true).first().now();
//		if(processConfig!=null){
//			for(int k=0;k<processConfig.getProcessList().size();k++){
//				if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("CustomerApprovalProcess")
//						&&processConfig.getProcessList().get(k).isStatus()==true){
//					custAppFlag=true;
//				}
//			}
//		}
		/**
		 * End
		 */
		
//		logger.log(Level.SEVERE,"Executing On Server Side In pInfo"+custAppFlag);
		
		
		logger.log(Level.SEVERE,"Parameters Company Id"+companyId);
		logger.log(Level.SEVERE,"Parameters Count Val"+countVal);
		logger.log(Level.SEVERE,"Parameters Entity Type"+entityType);
		ArrayList<PersonInfo> personinfoarray=new ArrayList<PersonInfo>();

		try {
			
		List<Customer> customerList=new ArrayList<Customer>();
		List<Vendor> vendorList=new ArrayList<Vendor>();
		ArrayList<SuperModel> supermodellis=new ArrayList<SuperModel>();
		
		if(AppConstants.PERSONCOMPOSITECUSTOMER.equals(entityType.trim()))
		{

			logger.log(Level.SEVERE,"Retrieving Data On Server Side");
			
			/**
			 * Date : 15-05-2017 By ANIL
			 */
//				if(custAppFlag){
//					customerList=ofy().load().type(Customer.class).filter("companyId", companyId).filter("businessProcess.status", "Active").filter("count >",countVal).list();
//				}else{
					customerList=ofy().load().type(Customer.class).filter("companyId", companyId).filter("count >",countVal).list();
//				}
				
				/**
				 * End
				 */
			
				logger.log(Level.SEVERE,"After OFY");
				ObjectifyService.reset();
				logger.log(Level.SEVERE,"After Reset");
			    ofy().consistency(Consistency.STRONG);
				logger.log(Level.SEVERE,"After====");
			    ofy().clear();
				logger.log(Level.SEVERE,"After Clear");
				supermodellis.addAll(customerList);
				logger.log(Level.SEVERE,"Customer Querry Size For Composite"+customerList.size());
		}
		
		if(AppConstants.PERSONCOMPOSITEVENDOR.equals(entityType.trim()))
		{

			logger.log(Level.SEVERE,"Retrieving Data On Server Side");
				
				vendorList=ofy().load().type(Vendor.class).filter("companyId", companyId).filter("count >",countVal).list();
				logger.log(Level.SEVERE,"After OFY");
				ObjectifyService.reset();
				logger.log(Level.SEVERE,"After Reset");
			    ofy().consistency(Consistency.STRONG);
				logger.log(Level.SEVERE,"After====");
			    ofy().clear();
				logger.log(Level.SEVERE,"After Clear");
				supermodellis.addAll(vendorList);
				logger.log(Level.SEVERE,"Customer Querry Size For Composite"+vendorList.size());
		}
		
		
	    for(SuperModel model:supermodellis)
	    {
	    	PersonInfo perinfo=new PersonInfo();
	    	if(model instanceof Person)
	    	{
	    	Person per=(Person) model;
	    	
	    	//For Customers Please load company Name instead of Full Name
	    	if(model instanceof Customer)
	    	{
	    		Customer customer=(Customer) model;
	    		perinfo.setFullName(customer.getCompanyName());
	    		perinfo.setCount(per.getCount());
		    	perinfo.setCellNumber(per.getContact().get(0).getCellNo1());
		    	perinfo.setEmail(per.getContact().get(0).getEmail());
		    	
		    	/******************************Changes 20april2015**********************************/
		    	String pocNameVal=customer.getFullname();
//		    	String pocNameVal="";
//		    	if(customer.getMiddleName()!=null){
//		    		pocNameVal=customer.getFirstName().trim()+" "+customer.getMiddleName().trim()+" "+customer.getLastName().trim();
//		    	}
//		    	else{
//		    		pocNameVal=customer.getFirstName().trim()+" "+customer.getLastName().trim();
//		    	}
		    	perinfo.setPocName(pocNameVal);
		    	/**********************************************************************************/
		    	
		    	/**
		    	 * Date : 15-06-2017 BY ANIL
		    	 */
		    	
		    	perinfo.setStatus(customer.getStatus());
		    	/**
		    	 * End
		    	 */
		    	
		    	personinfoarray.add(perinfo);
	    	}
	    	else{
	    	perinfo.setFullName(per.getFullname());
	    	perinfo.setCount(per.getCount());
	    	perinfo.setCellNumber(per.getContact().get(0).getCellNo1());
	    	perinfo.setEmail(per.getContact().get(0).getEmail());
	    	personinfoarray.add(perinfo);
	    	}
	    	}
	    	else if(model instanceof Vendor)
	    	{
	    		Vendor customer=(Vendor) model;
	    		perinfo.setFullName(customer.getVendorName());
	    		perinfo.setCount(customer.getCount());
		    	perinfo.setCellNumber(customer.getCellNumber1());
		    	perinfo.setEmail(customer.getEmail());
		    	personinfoarray.add(perinfo);
	    	}
	    }
		
	    logger.log(Level.SEVERE,"Returning Person Info Array"+personinfoarray.size());
	    
		} catch (Exception e) {
			e.printStackTrace();
		}
	    return personinfoarray;
	}
	

	/**
	 * Date 02-11-2018 by Vijay
	 * Des:- To load large customer Data with process config 
	 * by Using Recursive fuction. 
	 */
	
	@Override
	public ArrayList<PersonInfo> LoadCustomerPinfo(MyQuerry querry,	int customerCount) {
		logger.log(Level.SEVERE,"Executing On Server Side here we go ");
		ArrayList<PersonInfo> personinfoarray=new ArrayList<PersonInfo>();

		/**
		 * Changed By Dipak For Composite Issue on 14 May 2015
		 */
		try {
			
		String[] classType=querry.getQuerryObject().getClass().toString().trim().split("\\s+");
		logger.log(Level.SEVERE,"Class Type In Querry"+classType);
		String strcname=classType[1];
		logger.log(Level.SEVERE,"Class Path"+strcname);
		String classNameArray[]=strcname.split("\\.");
		String classNameValue=classNameArray[classNameArray.length-1];
		logger.log(Level.SEVERE,"Name Of Class Loaded"+classNameValue);
		
		
		List<Customer> custList=new ArrayList<Customer>();
		List<Vendor> venList=new ArrayList<Vendor>();
		ArrayList<SuperModel> arraylis=new ArrayList<SuperModel>();
		if(!classNameValue.equals("")||classNameValue!=null)
		{
			logger.log(Level.SEVERE,"Retrieving Data On Server Side");
			if(AppConstants.PERSONCOMPOSITECUSTOMER.equals(classNameValue.trim())){
				
//				try{
					logger.log(Level.SEVERE,"Try Block");
					
					custList=ofy().load().type(Customer.class).filter("count >", customerCount).filter("companyId", querry.getFilters().get(0).getLongValue()).limit(5000).list();
					logger.log(Level.SEVERE,"Customer Array list size ======= ## "+custList.size());

						
//				}
//				catch(Exception e){
//					
//					logger.log(Level.SEVERE,"Catch Block");
//					e.printStackTrace();
//				}
				
				logger.log(Level.SEVERE,"After OFY");
				ObjectifyService.reset();
				logger.log(Level.SEVERE,"After Reset");
			    ofy().consistency(Consistency.STRONG);
				logger.log(Level.SEVERE,"After====");
			    ofy().clear();
				logger.log(Level.SEVERE,"After Clear");
				arraylis.addAll(custList);
				logger.log(Level.SEVERE,"Customer Querry Size For Composite"+custList.size());
			}
		}
		
		logger.log(Level.SEVERE,"Array Being Passed to Person Info"+arraylis.size());
		
	    for(SuperModel model:arraylis)
	    {
	    	PersonInfo perinfo=new PersonInfo();
	    	if(model instanceof Person)
	    	{
		    	Person per=(Person) model;
		    	
		    	//For Customers Please load company Name instead of Full Name
		    	if(model instanceof Customer)
		    	{
		    		Customer customer=(Customer) model;
		    		perinfo.setFullName(customer.getCompanyName());
		    		perinfo.setCount(per.getCount());
			    	perinfo.setCellNumber(per.getContact().get(0).getCellNo1());
			    	perinfo.setEmail(per.getContact().get(0).getEmail());
			    	
			    	/******************************Changes 20april2015**********************************/
			    	
			    	String pocNameVal=customer.getFullname();
			    	perinfo.setPocName(pocNameVal);
			    	/**********************************************************************************/
			    	
			    	/**
			    	 * Date : 15-06-2017 BY ANIL
			    	 */
			    	
			    	perinfo.setStatus(customer.getStatus());
			    	/**
			    	 * End
			    	 */
			    	
			    	personinfoarray.add(perinfo);
		    	}
		    	else{
			    	perinfo.setFullName(per.getFullname());
			    	perinfo.setCount(per.getCount());
			    	perinfo.setCellNumber(per.getContact().get(0).getCellNo1());
			    	perinfo.setEmail(per.getContact().get(0).getEmail());
			    	
			    	personinfoarray.add(perinfo);
		    	}
	    	}
	    	else if(model instanceof Vendor)
	    	{
	    		Vendor customer=(Vendor) model;
	    		perinfo.setFullName(customer.getVendorName());
	    		perinfo.setCount(customer.getCount());
		    	perinfo.setCellNumber(customer.getCellNumber1());
		    	perinfo.setEmail(customer.getEmail());
		    	
		    	// vijay added to load vendor poc in composite
		    	String pocNameVal=customer.getfullName();
		    	perinfo.setPocName(pocNameVal);
		    	
		    	/**
		    	 * Date 10 April 2017 added by vijay
		    	 * for diffentiate the personinfo types is customer or vendor type
		    	 */
		    	perinfo.setVendor(true);
		    	/**
		    	 * ends here
		    	 */
		    	
		    	personinfoarray.add(perinfo);
	    	}
	    	
	    }
	    
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    logger.log(Level.SEVERE,"Returning Person Info Array"+personinfoarray.size());
	    
	    return personinfoarray;
		
	}

}
