package com.slicktechnologies.server.compositeimplementor;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.ObjectifyService;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.purchase.ProductInfoService;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.personlayer.Person;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class ProductInfoCompositeImplementor extends RemoteServiceServlet implements ProductInfoService
{
	Logger logger=Logger.getLogger("Product Info Composite Logger");
	/**
	 * 
	 */
	private static final long serialVersionUID = -5965033658584209476L;

	@Override
	public ArrayList<ProductInfo> LoadVinfo(long companyId, String typeOfEntity) {
//		  GenricServiceImpl impl=new GenricServiceImpl();
		    ArrayList<SuperModel>arraylis=new ArrayList<SuperModel>();
		    Logger logger=Logger.getLogger("Product Composite Logger");
		    
		    logger.log(Level.SEVERE,"Parameter One Main Method"+typeOfEntity);
		    logger.log(Level.SEVERE,"Parameter Two Main Method"+companyId);
		    
		if(AppConstants.PRODUCTCOMPOSITEITEM.equals(typeOfEntity.trim()))
		{
			logger.log(Level.SEVERE,"Before Item Product");
			List<ItemProduct> itemEntity=ofy().load().type(ItemProduct.class).filter("companyId", companyId).filter("status", true).list();
			logger.log(Level.SEVERE,"After Item Product");
			arraylis.addAll(itemEntity);
			logger.log(Level.SEVERE,"Size Item Product"+arraylis.size());
		}
		
		if(AppConstants.PRODUCTCOMPOSITESERVICE.equals(typeOfEntity.trim()))
		{
			logger.log(Level.SEVERE,"Before Service Product");
			List<ServiceProduct> serviceEntity=ofy().load().type(ServiceProduct.class).filter("companyId", companyId).filter("status", true).list();
			logger.log(Level.SEVERE,"After Service Product");
			arraylis.addAll(serviceEntity);
			logger.log(Level.SEVERE,"Size Service Product"+arraylis.size());
		}
		
//		if(AppConstants.PRODUCTCOMPOSITESUPER.equals(typeOfEntity.trim()))
//		{
//			logger.log(Level.SEVERE,"Before Super Product");
//			List<SuperProduct> superEntity=ofy().load().type(SuperProduct.class).filter("companyId", companyId).list();
//			logger.log(Level.SEVERE,"After Super Product");
//			arraylis.addAll(superEntity);
//			logger.log(Level.SEVERE,"Size Super Product"+arraylis.size());
//		}
		
		    logger.log(Level.SEVERE,"Executing On Server Side");
		    
		    ArrayList<ProductInfo> productinfoarray=new ArrayList<ProductInfo>();
		    for(SuperModel model:arraylis)
		    {
		    	SuperProduct per=(SuperProduct) model;
		    	ProductInfo prodinfo=new ProductInfo();
		    	prodinfo.setProdID(per.getCount());
		    	prodinfo.setProductName(per.getProductName());
		    	prodinfo.setProductCode(per.getProductCode());
		    	prodinfo.setProductCategory(per.getProductCategory());
		    	prodinfo.setProductPrice(per.getPrice());
		    	prodinfo.setUnitofMeasure(per.getUnitOfMeasurement());
		    //	prodinfo.setServiceTax(per.get);
		    	productinfoarray.add(prodinfo);
		    	prodinfo.setServiceTax(per.getServiceTax().getPercentage());
		    	prodinfo.setVat(per.getVatTax().getPercentage());
		    	
		    	/**
		    	 * Date : 10-07-2018 BY ANIL
		    	 * setting purchase price
		    	 */
		    	if(per instanceof ItemProduct){
		    		ItemProduct product=(ItemProduct) per;
		    		prodinfo.setPurchasePrice(product.getPurchasePrice());
		    		
		    		/**
		    		 * 
		    		 */
		    		prodinfo.setStatus(product.isStatus());
		    	}
		    }
		    
		    return productinfoarray;
	}

	@Override
	public ArrayList<ProductInfo> LoadProdinfo(long companyId,int latestId, String typeOfEntity) {
		
		

		
		logger.log(Level.SEVERE,"Executing On Server Side In Retrieve > Products");
		logger.log(Level.SEVERE,"Parameter One"+typeOfEntity);
		logger.log(Level.SEVERE,"Parameter Two"+companyId);
		logger.log(Level.SEVERE,"Parameter Three"+latestId);
		
		List<ItemProduct> itemList=new ArrayList<ItemProduct>();
		List<ServiceProduct> serviceList=new ArrayList<ServiceProduct>();
		List<SuperProduct> superList=new ArrayList<SuperProduct>();
		List<SuperModel> supermodelLis=new ArrayList<SuperModel>();
		
		if(AppConstants.PRODUCTCOMPOSITEITEM.equals(typeOfEntity.trim()))
		{

			logger.log(Level.SEVERE,"Retrieving Data On Server Side");
				
			itemList=ofy().load().type(ItemProduct.class).filter("companyId", companyId).filter("count >",latestId).list();
				logger.log(Level.SEVERE,"After OFY"+itemList.size());
				ObjectifyService.reset();
				logger.log(Level.SEVERE,"After Reset");
			    ofy().consistency(Consistency.STRONG);
				logger.log(Level.SEVERE,"After====");
			    ofy().clear();
				logger.log(Level.SEVERE,"After Clear");
				supermodelLis.addAll(itemList);
				logger.log(Level.SEVERE,"Item Product Querry Size For Composite"+itemList.size());
		}
		
		if(AppConstants.PRODUCTCOMPOSITESERVICE.equals(typeOfEntity.trim()))
		{

			logger.log(Level.SEVERE,"Retrieving Data On Server Side");
				
				serviceList=ofy().load().type(ServiceProduct.class).filter("companyId", companyId).filter("count >",latestId).list();
				logger.log(Level.SEVERE,"After OFY");
				ObjectifyService.reset();
				logger.log(Level.SEVERE,"After Reset");
			    ofy().consistency(Consistency.STRONG);
				logger.log(Level.SEVERE,"After====");
			    ofy().clear();
				logger.log(Level.SEVERE,"After Clear");
				supermodelLis.addAll(serviceList);
				logger.log(Level.SEVERE,"Service Product Querry Size For Composite"+serviceList.size());
		}
		
		if(AppConstants.PRODUCTCOMPOSITESUPER.equals(typeOfEntity.trim()))
		{

			logger.log(Level.SEVERE,"Retrieving Data On Server Side");
				
			superList=ofy().load().type(SuperProduct.class).filter("companyId", companyId).filter("count >",latestId).list();
				logger.log(Level.SEVERE,"After OFY");
				ObjectifyService.reset();
				logger.log(Level.SEVERE,"After Reset");
			    ofy().consistency(Consistency.STRONG);
				logger.log(Level.SEVERE,"After====");
			    ofy().clear();
				logger.log(Level.SEVERE,"After Clear");
				supermodelLis.addAll(superList);
				logger.log(Level.SEVERE,"Super Product Querry Size For Composite"+superList.size());
		}
		
		ArrayList<ProductInfo> productinfoarray=new ArrayList<ProductInfo>();
	    for(SuperModel model:supermodelLis)
	    {
	    	SuperProduct per=(SuperProduct) model;
	    	ProductInfo prodinfo=new ProductInfo();
	    	prodinfo.setProdID(per.getCount());
	    	prodinfo.setProductName(per.getProductName());
	    	prodinfo.setProductCode(per.getProductCode());
	    	prodinfo.setProductCategory(per.getProductCategory());
	    	prodinfo.setProductPrice(per.getPrice());
	    	prodinfo.setUnitofMeasure(per.getUnitOfMeasurement());
	    //	prodinfo.setServiceTax(per.get);
	    	productinfoarray.add(prodinfo);
	    	prodinfo.setServiceTax(per.getServiceTax().getPercentage());
	    	prodinfo.setVat(per.getVatTax().getPercentage());
	    	
	    	/**
	    	 * Date : 10-07-2018 BY ANIL
	    	 * setting purchase price
	    	 */
	    	if(per instanceof ItemProduct){
	    		ItemProduct product=(ItemProduct) per;
	    		prodinfo.setPurchasePrice(product.getPurchasePrice());
	    		/**
	    		 * 
	    		 */
	    		prodinfo.setStatus(product.isStatus());
	    	}
	    	
	    }
	    logger.log(Level.SEVERE,"Returning Person Info Array"+productinfoarray.size());
	    return productinfoarray;
	
	}

	@Override
	public ArrayList<ProductInfo> LoadSuperProd(int itemLatestId,int serviceLatestId, long companyId) {
		
		logger.log(Level.SEVERE,"Loading Super Product In Server Side");
		List<ItemProduct> itemList=new ArrayList<ItemProduct>();
		List<ServiceProduct> serviceList=new ArrayList<ServiceProduct>();
		List<SuperModel> supermodelLis=new ArrayList<SuperModel>();
		
		if(itemLatestId==0)
		{
			logger.log(Level.SEVERE,"Loading Super Product Item Latest Id As 0");
			List<ItemProduct> itemEntityLis=ofy().load().type(ItemProduct.class).filter("companyId", companyId).filter("status", true).list();
			itemList.addAll(itemEntityLis);
		}
		
		if(itemLatestId!=0)
		{
			logger.log(Level.SEVERE,"Loading Super Product Item Latest Id As Not 0");
			List<ItemProduct> itemEntityLis=ofy().load().type(ItemProduct.class).filter("companyId", companyId).filter("count >", itemLatestId).list();
			itemList.addAll(itemEntityLis);
		}
		
		if(serviceLatestId==0)
		{
			logger.log(Level.SEVERE,"Loading Super Product Service Latest Id As 0");
			List<ServiceProduct> servEntityLis=ofy().load().type(ServiceProduct.class).filter("companyId", companyId).filter("status", true).list();
			serviceList.addAll(servEntityLis);
		}
		
		if(serviceLatestId!=0)
		{
			logger.log(Level.SEVERE,"Loading Super Product Service Latest Id As Not 0");
			List<ServiceProduct> servEntityLis=ofy().load().type(ServiceProduct.class).filter("companyId", companyId).filter("count >", serviceLatestId).list();
			serviceList.addAll(servEntityLis);
		}
		logger.log(Level.SEVERE,"Loading Super Product Service Item List Size After Execution"+itemList.size());
		logger.log(Level.SEVERE,"Loading Super Product Service Service List Size After Execution"+serviceList.size());
		supermodelLis.addAll(itemList);
		supermodelLis.addAll(serviceList);
		
		logger.log(Level.SEVERE,"Loading Super Final List"+supermodelLis.size());
		
		ArrayList<ProductInfo> productinfoarray=new ArrayList<ProductInfo>();
	    for(SuperModel model:supermodelLis)
	    {
	    	SuperProduct per=(SuperProduct) model;
	    	ProductInfo prodinfo=new ProductInfo();
	    	prodinfo.setProdID(per.getCount());
	    	prodinfo.setProductName(per.getProductName());
	    	prodinfo.setProductCode(per.getProductCode());
	    	prodinfo.setProductCategory(per.getProductCategory());
	    	prodinfo.setProductPrice(per.getPrice());
	    	prodinfo.setUnitofMeasure(per.getUnitOfMeasurement());
	    //	prodinfo.setServiceTax(per.get);
	    	productinfoarray.add(prodinfo);
	    	prodinfo.setServiceTax(per.getServiceTax().getPercentage());
	    	prodinfo.setVat(per.getVatTax().getPercentage());
	    	
	    	/**
	    	 * Date : 10-07-2018 BY ANIL
	    	 * setting purchase price
	    	 */
	    	if(per instanceof ItemProduct){
	    		ItemProduct product=(ItemProduct) per;
	    		prodinfo.setPurchasePrice(product.getPurchasePrice());
	    		/**
	    		 * 
	    		 */
	    		prodinfo.setStatus(product.isStatus());
	    	}
	    }
		
		
		return productinfoarray;
	}

	@Override
	public ArrayList<ProductInfo> LoadAssetProd(long companyId) {
		// TODO Auto-generated method stub
		ArrayList<SuperModel>arraylis=new ArrayList<SuperModel>();
		logger.log(Level.SEVERE,"Before Item Product");
		List<ItemProduct> itemEntity=ofy().load().type(ItemProduct.class).filter("companyId", companyId).filter("productGroup", "Asset").filter("status", true).list();
		logger.log(Level.SEVERE,"After Item Product");
		arraylis.addAll(itemEntity);
		logger.log(Level.SEVERE,"Size Item Product"+arraylis.size());
		ObjectifyService.reset();
		logger.log(Level.SEVERE,"After Reset");
	    ofy().consistency(Consistency.STRONG);
		logger.log(Level.SEVERE,"After====");
	    ofy().clear();
		logger.log(Level.SEVERE,"After Clear");
		
		ArrayList<ProductInfo> productinfoarray=new ArrayList<ProductInfo>();
	    for(SuperModel model:arraylis)
	    {
	    	SuperProduct per=(SuperProduct) model;
	    	ProductInfo prodinfo=new ProductInfo();
	    	prodinfo.setProdID(per.getCount());
	    	prodinfo.setProductName(per.getProductName());
	    	prodinfo.setProductCode(per.getProductCode());
	    	prodinfo.setProductCategory(per.getProductCategory());
	    	prodinfo.setProductPrice(per.getPrice());
	    	prodinfo.setUnitofMeasure(per.getUnitOfMeasurement());
	    	productinfoarray.add(prodinfo);
	    	prodinfo.setServiceTax(per.getServiceTax().getPercentage());
	    	prodinfo.setVat(per.getVatTax().getPercentage());
	    	if(per instanceof ItemProduct){
	    		ItemProduct product=(ItemProduct) per;
	    		prodinfo.setPurchasePrice(product.getPurchasePrice());
	    		prodinfo.setStatus(product.isStatus());
	    	}
	    }
	    return productinfoarray;
	}

}
