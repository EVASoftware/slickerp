package com.slicktechnologies.server.compositeimplementor;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.cmd.Query;
import com.itextpdf.text.log.SysoCounter;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.EmployeeInfoService;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.personlayer.Employee;

import static com.googlecode.objectify.ObjectifyService.ofy;;



public class EmployeeInfoCompositeImplementor extends RemoteServiceServlet implements EmployeeInfoService{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8450437072796417900L;

	@Override
	public ArrayList<EmployeeInfo> LoadVinfo(MyQuerry querry) {
		
		
		 GenricServiceImpl impl=new GenricServiceImpl();
		    ArrayList<SuperModel>arraylis=impl.getSearchResult(querry);
		    
		    ArrayList<EmployeeInfo> employeeinfoarray=new ArrayList<EmployeeInfo>();
		    for(SuperModel model:arraylis)
		    {
		    	EmployeeInfo emp=(EmployeeInfo) model;
//		    	EmployeeInfo empinfo=new EmployeeInfo();
//		    	Department dep=new Department();
//		    	
//		    	empinfo.setFullName(emp.getFullname());
//		    	empinfo.setCount(emp.getCount());
//		    	empinfo.setCellNumber(emp.getContact().get(0).getCellNo1());
//		    	empinfo.setDesignation(emp.getDesignation());
//		    	empinfo.setDepartment(emp.getDepartMent());
//		    	empinfo.setEmployeerole(emp.getRoleName());
//		    	empinfo.setBranch(emp.getBranchName());
//		    	empinfo.setEmployeeType(emp.getEmployeeType());
		    	
		    	employeeinfoarray.add(emp);
		    	
		    	
		    }
		    
		    
		     return employeeinfoarray;
			
		
	}
	
	
	public ArrayList<Key<EmployeeInfo>> LoadEmployeeInfoRef(MyQuerry quer) {
		
		       //Clear the session
			   ArrayList<Ref<EmployeeInfo>> list=new ArrayList<Ref<EmployeeInfo>>();
			   try{
			   ofy().clear();
			   Query<? extends SuperModel> query = null;
			   //Get the querry
			   query=ofy().load().type(quer.getQuerryObject().getClass());
			   Logger logger = Logger.getLogger("NameOfYourLogger");
			   
			   //Get the 
			   if(quer.getFilters()!=null)
				{
			      for(int i=0;i<quer.getFilters().size();i++)
					{
					      String conditionField=quer.getFilters().get(i).getQuerryString();
					     
					      Object value=quer.getFilters().get(i).geFilterValue();
					    
					      if(value instanceof Date)
					        {
					    	  
					    	  logger.log(Level.SEVERE,"Condition Field "+conditionField);
						        logger.log(Level.SEVERE,"Condition Value "+value);
						        logger.log(Level.SEVERE,"Size of Querry "+query.count());
						        GWT.log("Condition Field "+conditionField);
						        GWT.log("Condition Value "+value);
					    	  logger.log(Level.SEVERE,"Condition Value before parsed "+value.toString());
					    	  Date condDate= GenricServiceImpl.parseDate((Date) value);
					    	 // Very dangerous patch learn internationalization and recode
					    	 Calendar c = Calendar.getInstance(); 
					    	 c.setTime(condDate); 
					    	 c.add(Calendar.DATE, 1);
					    	 condDate = c.getTime();
					    	 condDate= GenricServiceImpl.parseDate((Date) condDate);
					    	 logger.log(Level.SEVERE,"Condition Value When parsed "+condDate);
					    	 logger.log(Level.SEVERE,"Condition Field When parsed "+conditionField);
					    	 query=query.filter(conditionField,condDate);
					    	}
					      else
					      {
					    	    
					           
					           logger.log(Level.SEVERE,"Condition Field "+conditionField);
						        logger.log(Level.SEVERE,"Condition Value "+value);
						        query=query.filter(conditionField,value);
						        logger.log(Level.SEVERE,"Size of Querry "+query.count());
					        
					      }
					        
					      
					       
					       
				}
			      
				}    
			      ObjectifyService.reset();
			      ofy().consistency(Consistency.STRONG);
			      logger.log(Level.SEVERE,"Size of Querry "+query.count());
			      logger.log(Level.SEVERE,"Type Of"+quer.getQuerryObject().getClass());
			      
				   ofy().clear();
				   
					for(SuperModel i:query)
					{
						EmployeeInfo temp =(EmployeeInfo) i;  
						Ref<EmployeeInfo>emp=Ref.create(temp);
						System.out.println("Employee---"+emp.get());
						list.add(emp);
					}
			   }
			   catch(Exception e)
			   {
				   e.printStackTrace();
			   }
			      	
				
			return null;
		   }
		
	

}
