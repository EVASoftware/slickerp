package com.slicktechnologies.server;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.slicktechnologies.client.services.IPAddressService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class RetrieveClientsIPAddress extends RemoteServiceServlet implements IPAddressService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6284931602145387990L;

	@Override
	public String retrieveClientIP(long companyId) {
		String ipOfClient=retriveIPAddress();
		String validateConfig=AppConstants.NO;
		ProcessName processEntity=ofy().load().type(ProcessName.class).filter("companyId", companyId).filter("processName", AppConstants.IPADDRESSPROCESSNAME).filter("status", true).first().now();
		if(processEntity!=null)
		{
			ProcessConfiguration configAuth=ofy().load().type(ProcessConfiguration.class).filter("companyId",companyId).filter("processName", AppConstants.IPADDRESSPROCESSNAME).filter("configStatus", true).first().now();
			if(configAuth!=null)
			{
				for(int y=0;y<configAuth.getProcessList().size();y++)
				{
					if(configAuth.getProcessList().get(y).getProcessType().trim().equalsIgnoreCase("IPAuthorization")&&configAuth.getProcessList().get(y).isStatus()==true)
					{
						validateConfig=AppConstants.YES;
					}
				}
			}
		}
		
		if(validateConfig.trim().equals(AppConstants.YES)){
			Company compEntity=ofy().load().type(Company.class).filter("companyId", companyId).first().now();
			if(compEntity!=null){
				ipOfClient=ipOfClient+"-"+AppConstants.YES+"-"+compEntity.getEmail();
			}
			else{
				ipOfClient=ipOfClient+"-"+AppConstants.YES+"-"+"IGNORE";
			}
		}
		else{
			ipOfClient=ipOfClient+"-"+AppConstants.NO+"-"+"IGNORE";
		}
		return ipOfClient;
		}
	
	public String retriveIPAddress(){
		
		Logger log = Logger.getLogger("ip logger");
		log.log(Level.SEVERE,"Current print ip 11111111111111111111111"+this.getThreadLocalRequest().getRemoteAddr());
		return getThreadLocalRequest().getRemoteAddr();
	}
	
	@Override
	public String retrieveUserIP(long companyId) {
		
		System.out.println("== 1");
		
		String ipOfUser = retriveIPAddress();
		/**
		 * @author Vijay 
		 * As per  nitin sir IP restriction should work if ip address added in the user screen
		 * so below process configuration code commented
		 */
		
//		String validateProcessConfig=AppConstants.NO;
//
//		ProcessName processEntity = ofy().load().type(ProcessName.class).filter("companyId", companyId).filter("processName",AppConstants.USERWISEIPADDRESSPROCESSNAME).filter("status",true).first().now();
//		
//		if(processEntity!=null){
//			
//			ProcessConfiguration processconfigEntity = ofy().load().type(ProcessConfiguration.class).filter("companyId",companyId).filter("processName", AppConstants.USERWISEIPADDRESSPROCESSNAME).filter("configStatus", true).first().now();
//			
//			if(processconfigEntity!=null){
//				
//				for(int y=0;y<processconfigEntity.getProcessList().size();y++)
//				{
//					if(processconfigEntity.getProcessList().get(y).getProcessType().trim().equalsIgnoreCase("UserWiseIPAuthorization")&&processconfigEntity.getProcessList().get(y).isStatus()==true)
//					{
//						validateProcessConfig=AppConstants.YES;
//					}
//				}
//			}
//			
//		}
//		
//		if(validateProcessConfig.trim().equals(AppConstants.YES)){
//			
//				System.out.println("== 4");
//
//				ipOfUser = ipOfUser+"-"+AppConstants.YES;
//		}
//		else{
//				System.out.println("== 5");
//
//				ipOfUser = ipOfUser+"-"+AppConstants.NO;
//		}
		
		return ipOfUser;
	}

}
