package com.slicktechnologies.server.android.assessment;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.googlecode.objectify.ObjectifyService;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.slicktechnologies.client.views.customerbranchdetails.servicelocation.Area;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchServiceLocation;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;

public class CreateServiceLocation extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1584423299095218095L;
	
	Logger logger = Logger.getLogger("CreateServiceLocation.class");
	Company comp;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		RegisterServiceImpl impl = new RegisterServiceImpl();
		impl.getClass();
		
		resp.setContentType("text/plain; charset=UTF-8");
		resp.setCharacterEncoding("UTF-8");
		
		String urlCalled = req.getRequestURL().toString().trim();
	
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl::" + splitUrl);
		
		GenricServiceImpl genimpl=new GenricServiceImpl();
		
		JSONObject errorObj  = new JSONObject();
		
		try {
			ObjectifyService.reset();
			ofy().consistency(Consistency.STRONG);
			ofy().clear();
			
			comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
			if(comp!=null){
				
				
				String serviceLocationData=req.getParameter("serviceLocationData").trim();
				logger.log(Level.SEVERE, "data::::::::::" + serviceLocationData);
				
				JSONObject servLocJSONObject = new JSONObject(serviceLocationData);
				JSONArray areaArray = null;
				try {
					areaArray = servLocJSONObject.getJSONArray("areaList");
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
				
				String companyId = comp.getCompanyId() + "";
				String authCode = servLocJSONObject.optString("authCode").trim();
				String customerId = servLocJSONObject.optString("customerId").trim();
				String customerBranchId=servLocJSONObject.optString("customerBranchId").trim();
				
				logger.log(Level.SEVERE, "companyId::::::::::" + companyId);
				logger.log(Level.SEVERE, "authCode::::::::::" + authCode);
				logger.log(Level.SEVERE, "customerId::::::::::" + customerId);
				logger.log(Level.SEVERE, "customerBranchId::::::::::" + customerBranchId);
				
				if(!companyId.trim().equals(authCode.trim())){
					errorObj.put("Failed","Authentication failed!");
					resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
					return;
				}
				
				/**
				 * Action should be either create/update
				 * if create then serviceLocationId will be zero
				 * and for update we need serviceLocationId
				 */
				String action = servLocJSONObject.optString("action").trim();
				String serviceLocationName = servLocJSONObject.optString("serviceLocationName").trim();
				String serviceLocationId = servLocJSONObject.optString("serviceLocationId").trim();
				
				logger.log(Level.SEVERE, "action::::::::::" + action);
				logger.log(Level.SEVERE, "serviceLocationName::::::::::" + serviceLocationName);
				logger.log(Level.SEVERE, "serviceLocationId::::::::::" + serviceLocationId);
				
				List<Area>areaList=new ArrayList<Area>();
//				ArrayList<String>areaList=new ArrayList<String>();
				if(areaArray!=null){
					for(int m =0 ; m<areaArray.length() ; m++){
						JSONObject obj = areaArray.getJSONObject(m);
						String area = obj.optString("area").trim();
						String plannedServices = obj.optString("plannedService").trim();
						String frequency = obj.optString("frequency").trim();
						
						Area areaObj = new Area();

						if(area!=null&&!area.equals("")){
//							areaList.add(area);
							areaObj.setArea(area);
						}
						if(plannedServices!=null && !plannedServices.equals("")){
							areaObj.setPlannedservices(Integer.parseInt(plannedServices));
						}
						if(frequency!=null && !frequency.equals("")){
							areaObj.setFrequency(frequency);
						}
						areaList.add(areaObj);
					}
				}
				logger.log(Level.SEVERE, "areaList::::::::::" + areaList);
				
				
				if(action!=null&&!action.equals("")){
					
					Customer customer=ofy().load().type(Customer.class).filter("companyId", Long.parseLong(companyId.trim())).filter("count", Integer.parseInt(customerId.trim())).first().now();
					if(customer==null){
						errorObj.put("Failed","Customer id is invalid!");
						resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
						return;
					}
					
					if(action.equals("Create service location")){
						
						if(serviceLocationName==null||serviceLocationName.equals("")){
							errorObj.put("Failed","Service location value is missing!");
							resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
							return;
						}
						
						CustomerBranchDetails customerBranch=ofy().load().type(CustomerBranchDetails.class).filter("companyId", Long.parseLong(companyId.trim())).filter("cinfo.count", Integer.parseInt(customerId.trim())).filter("count", Integer.parseInt(customerBranchId.trim())).first().now();
						
						if(customerBranch!=null){
							
							CustomerBranchServiceLocation cbsl1=ofy().load().type(CustomerBranchServiceLocation.class).filter("companyId", Long.parseLong(companyId.trim())).filter("cinfo.count", Integer.parseInt(customerId.trim())).filter("serviceLocation", serviceLocationName).first().now();
							if(cbsl1!=null){
								errorObj.put("Failed","Service location with name "+serviceLocationName+" already exist!");
								resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
								return;
							}
							
							CustomerBranchServiceLocation cbsl=new CustomerBranchServiceLocation();
							cbsl.setCompanyId(Long.parseLong(companyId.trim()));
							cbsl.setCinfo(customerBranch.getCinfo());
							cbsl.setCustBranchId(customerBranch.getCount());
							cbsl.setCustomerBranchName(customerBranch.getBusinessUnitName());
							cbsl.setServiceLocation(serviceLocationName);
//							cbsl.setAreaList(areaList);
							try {
								cbsl.setAreaList2(areaList);
							} catch (Exception e1) {
								e1.printStackTrace();
							}							
							cbsl.setStatus(true);
							
							ReturnFromServer rfs=genimpl.save(cbsl);
							
							servLocJSONObject.put("serviceLocationId", rfs.count);
							servLocJSONObject.put("remark", "Service location saved successfully.");
							
							errorObj.put("Success",rfs.count);
							resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
							return;
						}else{
							errorObj.put("Failed","No customer branch found!");
							resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
							return;
						}
					}else if(action.equals("Update service location")){
						
						if(serviceLocationName==null||serviceLocationName.equals("")){
							errorObj.put("Failed","Service location value is missing!");
							resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
							return;
						}
						
						if(serviceLocationId!=null&&!serviceLocationId.equals("")){
							CustomerBranchServiceLocation cbsl=ofy().load().type(CustomerBranchServiceLocation.class).filter("companyId", Long.parseLong(companyId.trim())).filter("count", Integer.parseInt(serviceLocationId.trim())).first().now();
							if(cbsl!=null){
								cbsl.setServiceLocation(serviceLocationName);
//								cbsl.setAreaList(areaList);
								try {
									cbsl.setAreaList2(areaList);
								} catch (Exception e1) {
									e1.printStackTrace();
								}	
								
								ReturnFromServer rfs=genimpl.save(cbsl);
								
								errorObj.put("Success","Updated successful.");
								resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
								return;
							}else{
								errorObj.put("Failed","No Service location found!");
								resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
								return;
							}
						}else{
							errorObj.put("Failed","Service location id is missing!");
							resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
							return;
						}
						
					}else if(action.equals("Create customer branch")){
						
						JSONArray servAddArray = null;
						try {
							servAddArray = servLocJSONObject.getJSONArray("serviceAddress");
						} catch (JSONException e1) {
							e1.printStackTrace();
						}
						
						Address serviceAddress=null;
						if(servAddArray!=null){
							for(int i=0;i<servAddArray.length() ;i++){
								try {
									JSONObject obj = servAddArray.getJSONObject(i);
									serviceAddress=new Address();
									serviceAddress.setAddrLine1(obj.optString("addressLine1"));
									serviceAddress.setAddrLine2(obj.optString("addressLine2"));
									serviceAddress.setLandmark(obj.optString("landmark"));
									serviceAddress.setLocality(obj.optString("locality"));
									serviceAddress.setCity(obj.optString("city"));
									serviceAddress.setState(obj.optString("state"));
									serviceAddress.setCountry(obj.optString("country"));
									
									try{
										serviceAddress.setPin(Long.parseLong(obj.optString("pin")));
									}catch(Exception e){
										
									}
								}catch(Exception e){
									
								}
							}
						}
						if(serviceAddress==null){
							serviceAddress=customer.getSecondaryAdress();
						}
						
						JSONArray billAddArray = null;
						try {
							billAddArray = servLocJSONObject.getJSONArray("billingAddress");
						} catch (JSONException e1) {
							e1.printStackTrace();
						}
						
						Address billingAddress=null;
						if(billAddArray!=null){
							for(int i=0;i<billAddArray.length() ;i++){
								try {
									JSONObject obj = billAddArray.getJSONObject(i);
									billingAddress=new Address();
									billingAddress.setAddrLine1(obj.optString("addressLine1"));
									billingAddress.setAddrLine2(obj.optString("addressLine2"));
									billingAddress.setLandmark(obj.optString("landmark"));
									billingAddress.setLocality(obj.optString("locality"));
									billingAddress.setCity(obj.optString("city"));
									billingAddress.setState(obj.optString("state"));
									billingAddress.setCountry(obj.optString("country"));
									
									try{
										billingAddress.setPin(Long.parseLong(obj.optString("pin")));
									}catch(Exception e){
										
									}
								}catch(Exception e){
									
								}
							}
						}
						if(billingAddress==null){
							billingAddress=customer.getAdress();
						}
						
						String customerBranchName=servLocJSONObject.optString("customerBranchName").trim();
						String customerBranchCellNo=servLocJSONObject.optString("customerBranchCellNo").trim();
						String customerBranchEmail=servLocJSONObject.optString("customerBranchEmail").trim();
						String gstin=servLocJSONObject.optString("gstin").trim();
						String serviceBranch=servLocJSONObject.optString("serviceBranch").trim();
						
						logger.log(Level.SEVERE, "customerBranchName::::::::::" + customerBranchName);
						logger.log(Level.SEVERE, "customerBranchCellNo::::::::::" + customerBranchCellNo);
						logger.log(Level.SEVERE, "customerBranchEmail::::::::::" + customerBranchEmail);
						logger.log(Level.SEVERE, "gstin::::::::::" + gstin);
						logger.log(Level.SEVERE, "serviceBranch::::::::::" + serviceBranch);
						
						
						if(customerBranchName==null||customerBranchName.equals("")){
							errorObj.put("Failed","Customer branch name value is missing!");
							resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
							return;
						}
						
						CustomerBranchDetails customerBranch=ofy().load().type(CustomerBranchDetails.class).filter("companyId", Long.parseLong(companyId.trim())).filter("cinfo.count", Integer.parseInt(customerId.trim())).filter("buisnessUnitName", customerBranchName).first().now();
						
						if(customerBranch!=null){
							errorObj.put("Failed","Customer branch name "+customerBranchName+" already exist!");
							resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
							return;
						}
						
						PersonInfo info=new PersonInfo();
						info.setCount(customer.getCount());
						info.setFullName(customer.getCompanyName());
						info.setPocName(customer.getFullname());
						info.setCellNumber(customer.getCellNumber1());
						info.setEmail(customer.getEmail());
						
						
						CustomerBranchDetails entity=new CustomerBranchDetails();
						entity.setCinfo(info);
						entity.setCompanyId(Long.parseLong(companyId.trim()));
						entity.setBusinessUnitName(customerBranchName);
						entity.setEmail(customerBranchEmail);
						try{
							entity.setCellNumber1(Long.parseLong(customerBranchCellNo));
						}catch(Exception e){
							
						}
						entity.setBranch(serviceBranch);
						entity.setGSTINNumber(gstin);
						
						entity.setAddress(serviceAddress);
						entity.setBillingAddress(billingAddress);
						entity.setStatus(true);
						
						ReturnFromServer rfs=genimpl.save(entity);
						errorObj.put("Success",rfs.count);
						resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
						return;
						
					}else if(action.equals("Update customer branch")){
						
						customerBranchId=servLocJSONObject.optString("customerBranchId").trim();
						
						if(customerBranchId==null||customerBranchId.equals("")){
							errorObj.put("Failed","Customer branch id value is missing!");
							resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
							return;
						}
						
						int custBranchId=0;
						try{
							custBranchId=Integer.parseInt(customerBranchId.trim());
						}catch(Exception e){
							
						}
						
						if(custBranchId==0){
							errorObj.put("Failed","Customer branch id value should be numeric!");
							resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
							return;
						}
						
						JSONArray servAddArray = null;
						try {
							servAddArray = servLocJSONObject.getJSONArray("serviceAddress");
						} catch (JSONException e1) {
							e1.printStackTrace();
						}
						
						Address serviceAddress=null;
						if(servAddArray!=null){
							for(int i=0;i<servAddArray.length() ;i++){
								try {
									JSONObject obj = servAddArray.getJSONObject(i);
									serviceAddress=new Address();
									serviceAddress.setAddrLine1(obj.optString("addressLine1"));
									serviceAddress.setAddrLine2(obj.optString("addressLine2"));
									serviceAddress.setLandmark(obj.optString("landmark"));
									serviceAddress.setLocality(obj.optString("locality"));
									serviceAddress.setCity(obj.optString("city"));
									serviceAddress.setState(obj.optString("state"));
									serviceAddress.setCountry(obj.optString("country"));
									
									try{
										serviceAddress.setPin(Long.parseLong(obj.optString("pin")));
									}catch(Exception e){
										
									}
								}catch(Exception e){
									
								}
							}
						}
						
						JSONArray billAddArray = null;
						try {
							billAddArray = servLocJSONObject.getJSONArray("billingAddress");
						} catch (JSONException e1) {
							e1.printStackTrace();
						}
						
						Address billingAddress=null;
						if(billAddArray!=null){
							for(int i=0;i<billAddArray.length() ;i++){
								try {
									JSONObject obj = billAddArray.getJSONObject(i);
									billingAddress=new Address();
									billingAddress.setAddrLine1(obj.optString("addressLine1"));
									billingAddress.setAddrLine2(obj.optString("addressLine2"));
									billingAddress.setLandmark(obj.optString("landmark"));
									billingAddress.setLocality(obj.optString("locality"));
									billingAddress.setCity(obj.optString("city"));
									billingAddress.setState(obj.optString("state"));
									billingAddress.setCountry(obj.optString("country"));
									
									try{
										billingAddress.setPin(Long.parseLong(obj.optString("pin")));
									}catch(Exception e){
										
									}
								}catch(Exception e){
									
								}
							}
						}
						
						
						String customerBranchName=servLocJSONObject.optString("customerBranchName").trim();
						String customerBranchCellNo=servLocJSONObject.optString("customerBranchCellNo").trim();
						String customerBranchEmail=servLocJSONObject.optString("customerBranchEmail").trim();
						String gstin=servLocJSONObject.optString("gstin").trim();
						String serviceBranch=servLocJSONObject.optString("serviceBranch").trim();
						
						logger.log(Level.SEVERE, "customerBranchName::::::::::" + customerBranchName);
						logger.log(Level.SEVERE, "customerBranchCellNo::::::::::" + customerBranchCellNo);
						logger.log(Level.SEVERE, "customerBranchEmail::::::::::" + customerBranchEmail);
						logger.log(Level.SEVERE, "gstin::::::::::" + gstin);
						logger.log(Level.SEVERE, "serviceBranch::::::::::" + serviceBranch);
						
						
						if(customerBranchName==null||customerBranchName.equals("")){
							errorObj.put("Failed","Customer branch name value is missing!");
							resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
							return;
						}
						
						CustomerBranchDetails entity=ofy().load().type(CustomerBranchDetails.class).filter("companyId", Long.parseLong(companyId.trim())).filter("cinfo.count", Integer.parseInt(customerId.trim())).filter("count", custBranchId).first().now();
						if(entity==null){
							errorObj.put("Failed","Customer branch not found!");
							resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
							return;
						}
						
						PersonInfo info=new PersonInfo();
						info.setCount(customer.getCount());
						info.setFullName(customer.getCompanyName());
						info.setPocName(customer.getFullname());
						info.setCellNumber(customer.getCellNumber1());
						info.setEmail(customer.getEmail());
						
						entity.setCinfo(info);
						
						entity.setBusinessUnitName(customerBranchName);
						entity.setEmail(customerBranchEmail);
						try{
							entity.setCellNumber1(Long.parseLong(customerBranchCellNo));
						}catch(Exception e){
							
						}
						entity.setBranch(serviceBranch);
						entity.setGSTINNumber(gstin);
						
						if(serviceAddress!=null){
							entity.setAddress(serviceAddress);
						}
						
						if(billingAddress!=null){
							entity.setBillingAddress(billingAddress);
						}
						entity.setStatus(true);
						
						ReturnFromServer rfs=genimpl.save(entity);
						errorObj.put("Success",rfs.count);
						resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
						return;
						
					}else{
						errorObj.put("Failed","Invalid action!");
						resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
						return;
					}
					
				}else{
					errorObj.put("Failed","Action value is missing!");
					resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
					return;
				}
				
			}else{
				errorObj.put("Failed","Company not found!");
				resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
				return;
			}
			
		}catch(Exception e){
			e.printStackTrace();
			try {
				errorObj.put("Failed",e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
			return;
		}
	}
	
	

}
