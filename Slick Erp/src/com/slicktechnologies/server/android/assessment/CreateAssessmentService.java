package com.slicktechnologies.server.android.assessment;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.googlecode.objectify.ObjectifyService;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.slicktechnologies.client.views.assesmentreport.AssesmentReportEmbedForTable;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchServiceLocation;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;

public class CreateAssessmentService extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8846438919301885783L;
	
	Logger logger = Logger.getLogger("CreateAssessmentService.class");
	Company comp;
	
	JSONObject errorObj  = new JSONObject();
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@SuppressWarnings("unused")
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		RegisterServiceImpl impl = new RegisterServiceImpl();
		impl.getClass();
		
		resp.setContentType("text/plain; charset=UTF-8");
		resp.setCharacterEncoding("UTF-8");
		
		String urlCalled = req.getRequestURL().toString().trim();
	
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl::" + splitUrl);
		
		JSONObject errorObj  = new JSONObject();
		
		GenricServiceImpl genImpl=new GenricServiceImpl();
		
		try {
			ObjectifyService.reset();
			ofy().consistency(Consistency.STRONG);
			ofy().clear();
			
			comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
			
			if(comp!=null){
				
				String assessmentData=req.getParameter("assessmentData").trim();
				logger.log(Level.SEVERE, "data::::::::::" + assessmentData);
				
				JSONObject assessmentJObj = new JSONObject(assessmentData);
				JSONArray taskArray = null;
				try {
					taskArray = assessmentJObj.getJSONArray("taskList");
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
				
				String companyId = comp.getCompanyId() + "";
				
				String authCode = assessmentJObj.optString("authCode").trim();
				String customerId = assessmentJObj.optString("customerId").trim();
				String customerBranchName=assessmentJObj.optString("customerBranchName").trim();
				
				String action = assessmentJObj.optString("action").trim();
				String branch = assessmentJObj.optString("branch").trim();
				String status = assessmentJObj.optString("status").trim();
				
				String assessmentId = assessmentJObj.optString("assessmentId").trim();
				String assessmentDate = assessmentJObj.optString("assessmentDate").trim();
				
				String assessedBy1 = assessmentJObj.optString("assessedBy1").trim();
				String assessedBy2 = assessmentJObj.optString("assessedBy2").trim();
				String assessedBy3 = assessmentJObj.optString("assessedBy3").trim();
				
				String accompainedBy1 = assessmentJObj.optString("accompainedBy1").trim();
				String accompainedBy2 = assessmentJObj.optString("accompainedBy2").trim();
				String accompainedBy3 = assessmentJObj.optString("accompainedBy3").trim();
				
				String serviceLocation = "";
				try {
					serviceLocation = 	assessmentJObj.optString("serviceLocation").trim();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				Date assessmentDt=null;
				
				boolean assessmentTaskFlag=false;
				ArrayList<AssesmentReportEmbedForTable>taskList=new ArrayList<AssesmentReportEmbedForTable>();
				if(taskArray!=null){
					for(int m =0 ; m<taskArray.length() ; m++){
						JSONObject obj = taskArray.getJSONObject(m);
						
						String serialNo = obj.optString("serialNo").trim();
						String area = obj.optString("area").trim();
						String riskLevel = obj.optString("riskLevel").trim();
						String category = obj.optString("category").trim();
						String observation = obj.optString("observation").trim();
						String impact = obj.optString("impact").trim();
						String actionPlanByCompany = obj.optString("actionPlanByCompany").trim();
						String recommendationToCustomer = obj.optString("recommendationToCustomer").trim();
						String tstatus = obj.optString("status").trim();
						String closureDate = obj.optString("closureDate").trim();
						String remark = obj.optString("remark").trim();
						String url=obj.optString("imageURL").trim();//Ashwini Patil
						if(serialNo!=null&&!serialNo.equals("")&&!serialNo.equals("0")){
							assessmentTaskFlag=true;
						}
						
						Date date=null;
						if(closureDate!=null&&!closureDate.equals("")){
							try{
								date=sdf.parse(closureDate);
							}catch(Exception e){
								
							}
						}
						
						AssesmentReportEmbedForTable task=new AssesmentReportEmbedForTable();
//						task.setCount(m+1);
						task.setLocation(observation);
						task.setConsequences(impact);
						task.setRiskLevel(riskLevel);
						task.setCategory(category);
						task.setArea(area);
						task.setActionPlanForCompany(actionPlanByCompany);
						task.setActionPlanForCustomer(recommendationToCustomer);
						task.setClosureDate(date);
						task.setRemark(remark);
						
						if(tstatus!=null && !tstatus.equals(""))
							task.setStatus(tstatus);
						
						if(action.equals("Create assessment")){
							task.setCount(m+1);
							obj.put("serialNo", task.getCount());
						}else if(action.equals("Create assessment task")){
							task.setCount(0);
						}else{
							if(serialNo!=null&&!serialNo.equals("")){
								task.setCount(Integer.parseInt(serialNo));
							}
						}
						
						task.setCompanyId(Long.parseLong(companyId.trim()));
						task.setCreatedBy("");
						task.setDeficiencyType("");
						task.setDeficiencyUnit("");
						task.setDeleted(false);
						task.setDocUpload(new DocumentUpload());
						task.setId(null);
						task.setUserId("");
						
						taskList.add(task);
					}
				}
				
				logger.log(Level.SEVERE, "companyId::::::::::" + companyId);
				logger.log(Level.SEVERE, "authCode::::::::::" + authCode);
				logger.log(Level.SEVERE, "customerId::::::::::" + customerId);
				logger.log(Level.SEVERE, "customerBranchName::::::::::" + customerBranchName);
				
				logger.log(Level.SEVERE, "action::::::::::" + action);
				logger.log(Level.SEVERE, "branch::::::::::" + branch);
				logger.log(Level.SEVERE, "status::::::::::" + status);
				
				logger.log(Level.SEVERE, "taskList::::::::::" + taskList);
				
				if(!companyId.trim().equals(authCode.trim())){
					errorObj.put("Failed","Authentication failed!");
					resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
					return;
				}
				
				if(action==null||action.equals("")){
					errorObj.put("Failed","Action value is missing!");
					resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
					return;
				}
				
				if(action.contains("Update assessment")){
					if(assessmentId==null||assessmentId.equals("")||Integer.parseInt(assessmentId.trim())==0){
						errorObj.put("Failed","Assessment id is missing!");
						resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
						return;
					}
				}
				
				if(action.contains("Update assessment task")){
					if(!assessmentTaskFlag){
						errorObj.put("Failed","Assessment task serial no is missing!");
						resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
						return;
					}
				}
				
				if(assessmentDate!=null&&!assessmentDate.equals("")){
					try{
						assessmentDt=sdf.parse(assessmentDate);
					}catch(Exception e){
						errorObj.put("Failed","Date formate should be dd/MM/yyyy.");
						resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
						return;
					}
				}
				
				Customer customer=null;
				if(customerId!=null&&!customerId.equals("")){
					customer=ofy().load().type(Customer.class).filter("companyId", Long.parseLong(companyId.trim())).filter("count", Integer.parseInt(customerId.trim())).first().now();
					if(customer==null){
						errorObj.put("Failed","Customer id is invalid!");
						resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
						return;
					}
				}
				
				int customerbranchId = 0;
				int servicelocationId = 0;
				if(customerBranchName!=null&&!customerBranchName.equals("")){
					CustomerBranchDetails customerBranch=ofy().load().type(CustomerBranchDetails.class).filter("companyId", Long.parseLong(companyId.trim())).filter("buisnessUnitName", customerBranchName).first().now();
					if(customerBranch==null){
						errorObj.put("Failed","Customer branch name not found!");
						resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
						return;
					}
					else {
						customerbranchId = customerBranch.getCount();
					}
				}
				
				if(action.equals("Create assessment")){
					
					PersonInfo info=new PersonInfo();
					info.setCount(customer.getCount());
					if(customer.isCompany()) //Ashwini Patil
						info.setFullName(customer.getCompanyName());
					else
						info.setFullName(customer.getFullname());
					info.setPocName(customer.getFullname());//customer.getCustomerName()
					info.setCellNumber(customer.getCellNumber1());
					info.setEmail(customer.getEmail());
					
					AssesmentReport report=new AssesmentReport();
					
					report.setCompanyId(Long.parseLong(companyId.trim()));
					report.setCinfo(info);
					report.setCustomerBranch(customerBranchName);
					report.setBranch(branch);
					report.setAssessmentDate(assessmentDt);
					report.setStatus("Created");
					
					report.setAssessedPeron1(assessedBy1);
					report.setAssessedPeron2(assessedBy2);
					report.setAssessedPeron3(assessedBy3);
					
					report.setAccompainedByPerson1(accompainedBy1);
					report.setAccompainedByPerson2(accompainedBy2);
					report.setAccompainedByPerson3(accompainedBy3);
					
					if(serviceLocation!=null)
					report.setServiceLocation(serviceLocation);
					
					report.setAssessmentDetailsLIst(taskList);
					
					report.setCustomerBranchId(customerbranchId);
					if(customerbranchId!=0 && serviceLocation!=null) {
						CustomerBranchServiceLocation servicelocation = ofy().load().type(CustomerBranchServiceLocation.class)
														.filter("companyId", Long.parseLong(companyId.trim())).filter("custBranchId", customerbranchId)
														.filter("serviceLocation", serviceLocation).first().now();
						if(servicelocation!=null) {
							report.setServiceLocationId(servicelocation.getCount());
						}
					}
					ReturnFromServer rfs=genImpl.save(report);
					
					errorObj.put("Success",rfs.count);
					errorObj.put("taskList",taskArray);
					resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
					return;
					
					
				}else if(action.equals("Update assessment")){
					
					AssesmentReport entity=ofy().load().type(AssesmentReport.class).filter("companyId", Long.parseLong(companyId.trim())).filter("count", Integer.parseInt(assessmentId.trim())).first().now();
					if(entity!=null){
						//Commented by Ashwini Patil
//						PersonInfo info=new PersonInfo();
//						info.setCount(customer.getCount());
//						if(customer.isCompany()) //Added by Ashwini Patil
//							info.setFullName(customer.getCompanyName());
//						else
//							info.setFullName(customer.getFullname());
//						info.setPocName(customer.getFullname());//customer.getCustomerName()
//						info.setCellNumber(customer.getCellNumber1());
//						info.setEmail(customer.getEmail());
//						
//						entity.setCinfo(info);
//						entity.setCustomerBranch(customerBranchName);
//						entity.setBranch(branch);
//						entity.setAssessmentDate(assessmentDt);
						
//						entity.setAssessedPeron1(assessedBy1);
//						entity.setAssessedPeron2(assessedBy2);
//						entity.setAssessedPeron3(assessedBy3);
						
//						entity.setAccompainedByPerson1(accompainedBy1);
//						entity.setAccompainedByPerson2(accompainedBy2);
//						entity.setAccompainedByPerson3(accompainedBy3);
						
//						if(serviceLocation!=null)
//							entity.setServiceLocation(serviceLocation);
//							
//						entity.setCustomerBranchId(customerbranchId);

						ReturnFromServer rfs=genImpl.save(entity);
						errorObj.put("Success","Update Successful!");
						resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
						
					}else{
						errorObj.put("Failed","No Assessment found!");
						resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
						return;
					}
					
				}else if(action.equals("Update assessment status")){
					AssesmentReport entity=ofy().load().type(AssesmentReport.class).filter("companyId", Long.parseLong(companyId.trim())).filter("count", Integer.parseInt(assessmentId.trim())).first().now();
					if(entity!=null){
						entity.setStatus(status);
						entity.setSubmissionDate(new Date());//Ashwini Patil Date:20-09-2022
						ReturnFromServer rfs=genImpl.save(entity);
						errorObj.put("Success","Update Successful!");
						resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
						
					}else{
						errorObj.put("Failed","No Assessment found!");
						resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
						return;
					}
					
				}else if(action.equals("Create assessment task")){
					
					if(assessmentId==null||assessmentId.equals("")||Integer.parseInt(assessmentId.trim())==0){
						errorObj.put("Failed","Assessment id is missing!");
						resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
						return;
					}
					AssesmentReport entity=ofy().load().type(AssesmentReport.class).filter("companyId", Long.parseLong(companyId.trim())).filter("count", Integer.parseInt(assessmentId.trim())).first().now();
					if(entity!=null){
						int serialNoCtr=0;
						if(entity.getAssessmentDetailsLIst()!=null&&entity.getAssessmentDetailsLIst().size()!=0){
							
							logger.log(Level.SEVERE, "getAssessmentDetailsLIst ::::::::::" + entity.getAssessmentDetailsLIst().size());
							Comparator<AssesmentReportEmbedForTable> comp=new Comparator<AssesmentReportEmbedForTable>() {
								@Override
								public int compare(AssesmentReportEmbedForTable o1,AssesmentReportEmbedForTable o2) {
									Integer count1=o1.getCount();
									Integer count2=o2.getCount();
									return count1.compareTo(count2);
								}
							};
							Collections.sort(entity.getAssessmentDetailsLIst(),comp);
							
							serialNoCtr = entity.getAssessmentDetailsLIst().get(entity.getAssessmentDetailsLIst().size()-1).getCount();
							logger.log(Level.SEVERE, "serialNoCtr::::::::::" + serialNoCtr);
						}
						
						taskList=new ArrayList<AssesmentReportEmbedForTable>();
						if(taskArray!=null){
							for(int m =0 ; m<taskArray.length() ; m++){
								JSONObject obj = taskArray.getJSONObject(m);
								
								String serialNo = obj.optString("serialNo").trim();
								String area = obj.optString("area").trim();
								String riskLevel = obj.optString("riskLevel").trim();
								String category = obj.optString("category").trim();
								String observation = obj.optString("observation").trim();
								String impact = obj.optString("impact").trim();
								String actionPlanByCompany = obj.optString("actionPlanByCompany").trim();
								String recommendationToCustomer = obj.optString("recommendationToCustomer").trim();
								String tstatus = obj.optString("status").trim();
								String closureDate = obj.optString("closureDate").trim();
								String remark = obj.optString("remark").trim();
								String url=obj.optString("imageURL").trim();//Ashwini Patil 12-07-2022
								
								Date date=null;
								if(closureDate!=null&&!closureDate.equals("")){
									try{
										date=sdf.parse(closureDate);
									}catch(Exception e){
										
									}
								}
								
								AssesmentReportEmbedForTable task=new AssesmentReportEmbedForTable();
								task.setCount(++serialNoCtr);
								task.setLocation(observation);
								task.setConsequences(impact);
								task.setRiskLevel(riskLevel);
								task.setCategory(category);
								task.setArea(area);
								task.setActionPlanForCompany(actionPlanByCompany);
								task.setActionPlanForCustomer(recommendationToCustomer);
								task.setClosureDate(date);
								task.setRemark(remark);
								task.setStatus(tstatus);//("Created"); Ashwini Patil
								task.setCreationDate(new Date());//Ashwini Patil Date:22-09-2022
						
								obj.put("serialNo", task.getCount());
								
								task.setCompanyId(Long.parseLong(companyId.trim()));
								task.setCreatedBy("");
								task.setDeficiencyType("");
								task.setDeficiencyUnit("");
								task.setDeleted(false);
//								task.setDocUpload(new DocumentUpload());
								task.setId(null);
								task.setUserId("");
								
//								--------------------
//								Ashwini Patil Date:28-06-2022
								String fileName="assessmentTask"+obj.optString("serialNo")+".jpg";
								DocumentUpload upload=new DocumentUpload();
								String finalurl=url+"&filename="+fileName;
								logger.log(Level.SEVERE,"imageURL="+url+"final url="+finalurl);
								upload.setUrl(finalurl);
								upload.setName(fileName);
								upload.setStatus(true);
								logger.log(Level.SEVERE, "upload is set to task");
								task.setDocUpload(upload);
//								--------------------
								taskList.add(task);
							}
						}
						
						if(serialNoCtr!=0){
							entity.getAssessmentDetailsLIst().addAll(taskList);
						}else{
							entity.setAssessmentDetailsLIst(taskList);
						}
						
						ReturnFromServer rfs=genImpl.save(entity);
						errorObj.put("Success","Update Successful!");
						errorObj.put("taskList",taskArray);
						resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
						return;
						
					}else{
						errorObj.put("Failed","No Assessment found!");
						resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
						return;
					}
					
				}else if(action.equals("Update assessment task")){
					if(taskList.size()==0){
						errorObj.put("Failed","Please add assessment task list to update!");
						resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
						return;
					}
					AssesmentReport entity=ofy().load().type(AssesmentReport.class).filter("companyId", Long.parseLong(companyId.trim())).filter("count", Integer.parseInt(assessmentId.trim())).first().now();
					if(entity!=null){
						
						if(entity.getAssessmentDetailsLIst()!=null&&entity.getAssessmentDetailsLIst().size()!=0){
							for(AssesmentReportEmbedForTable task:taskList){
								boolean updateFlag=false;
								for(AssesmentReportEmbedForTable object:entity.getAssessmentDetailsLIst()){
									if(task.getCount()==object.getCount()){
										updateFlag=true;
										object.setArea(task.getArea());
										object.setCategory(task.getCategory());
										object.setRiskLevel(task.getRiskLevel());
										object.setLocation(task.getLocation());
										object.setConsequences(task.getConsequences());
										object.setActionPlanForCustomer(task.getActionPlanForCustomer());
										object.setActionPlanForCompany(task.getActionPlanForCompany());
										object.setClosureDate(task.getClosureDate());
										object.setRemark(task.getRemark());
										
										object.setCompanyId(Long.parseLong(companyId.trim()));
										object.setCreatedBy("");
										object.setDeficiencyType("");
										object.setDeficiencyUnit("");
										object.setDeleted(false);
//										if(object.getDocUpload()==null){
//											object.setDocUpload(new DocumentUpload());
//										}
										object.setId(null);
										object.setUserId("");
										if(task.getStatus()!=null && !task.getStatus().equals("")) {
											object.setStatus(task.getStatus());
											logger.log(Level.SEVERE, "task.getStatus() "+task.getStatus());
										}
//										--------------------
//										Ashwini Patil Date:28-06-2022
										DocumentUpload upload=task.getDocUpload();
										String fileName="";
										if(upload.getName()!=null&&!upload.getName().equals(""))
											fileName=upload.getName();
										else
											fileName="assessmentTask"+task.getCount()+".jpg";
										
										
										String url=upload.getUrl();//Ashwini Patil Date:12-07-2022
										if(url!=null&&!url.equals("")) {
											String finalurl=url+"&filename="+fileName;
											logger.log(Level.SEVERE,"final url="+finalurl);
											upload.setUrl(finalurl);
											upload.setName(fileName);
											upload.setStatus(true);
											object.setDocUpload(upload);				
										}
										logger.log(Level.SEVERE, "upload is set to task");
										
//										--------------------
										
									}
								}
								if(!updateFlag){
									errorObj.put("Failed","No Assessment task found for serial no. "+task.getCount());
								}
							}
							
							if(errorObj.length()==0){
								ReturnFromServer rfs=genImpl.save(entity);
								logger.log(Level.SEVERE, "Assessment task updated sucessfully");
								errorObj.put("Success","Update Successful!");
							}
							resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
							return;
						}else{
							errorObj.put("Failed","No Assessment task found for update!");
							resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
							return;
						}
						
					}else{
						errorObj.put("Failed","No Assessment found!");
						resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
						return;
					}
					
				}else if(action.equals("Update assessment task status")){
					if(taskList.size()==0){
						errorObj.put("Failed","Please add assessment task list to update!");
						resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
						return;
					}
					AssesmentReport entity=ofy().load().type(AssesmentReport.class).filter("companyId", Long.parseLong(companyId.trim())).filter("count", Integer.parseInt(assessmentId.trim())).first().now();
					if(entity!=null){
						if(entity.getAssessmentDetailsLIst()!=null&&entity.getAssessmentDetailsLIst().size()!=0){
							for(AssesmentReportEmbedForTable task:taskList){
								boolean updateFlag=false;
								for(AssesmentReportEmbedForTable object:entity.getAssessmentDetailsLIst()){
									if(task.getCount()==object.getCount()){
										updateFlag=true;
//										object.setArea(task.getArea());
//										object.setCategory(task.getCategory());
//										object.setRiskLevel(task.getRiskLevel());
//										object.setLocation(task.getLocation());
//										object.setConsequences(task.getConsequences());
//										object.setActionPlanForCustomer(task.getActionPlanForCustomer());
//										object.setActionPlanForCompany(task.getActionPlanForCompany());
										object.setClosureDate(task.getClosureDate());
										object.setRemark(task.getRemark());
//										object.setStatus(status);
										if(task.getStatus()!=null && !task.getStatus().equals("")) {
											object.setStatus(task.getStatus());
											logger.log(Level.SEVERE, "task status"+task.getStatus());
										}
										
										object.setCompanyId(Long.parseLong(companyId.trim()));
										object.setCreatedBy("");
										object.setDeficiencyType("");
										object.setDeficiencyUnit("");
										object.setDeleted(false);
										if(object.getDocUpload()==null){
											object.setDocUpload(new DocumentUpload());
										}
										object.setId(null);
										object.setUserId("");
									}
								}
								if(!updateFlag){
									errorObj.put("Failed","No Assessment task found for serial no. "+task.getCount());
								}
							}
							
							if(errorObj.length()==0){
								ReturnFromServer rfs=genImpl.save(entity);
								errorObj.put("Success","Update Successful!");
							}
							resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
							return;
						}else{
							errorObj.put("Failed","No Assessment task found for update!");
							resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
							return;
						}
						
					}else{
						errorObj.put("Failed","No Assessment found!");
						resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
						return;
					}
					
				}else{
					errorObj.put("Failed","Invalid action!");
					resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
					return;
				}
			
			}else{
				errorObj.put("Failed","Company not found!");
				resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
				return;
			}
			
			
			
		}catch(Exception e){
			e.printStackTrace();
			try {
				errorObj.put("Failed",e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
			return;
		}
	}

}
