package com.slicktechnologies.server.android.assessment;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.googlecode.objectify.ObjectifyService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.assesmentreport.AssesmentReportEmbedForTable;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;

public class FetchAssessmentService extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7096906471415908083L;
	
	Logger logger = Logger.getLogger("FetchAssessmentService.class");
	Company comp;
	JSONObject errorObj  = new JSONObject();
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}
	@SuppressWarnings("unchecked")
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setHeader("Access-Control-Allow-Origin", "*"); //Ashwini Patil added this for customer portal
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		RegisterServiceImpl impl = new RegisterServiceImpl();
		impl.getClass();
		
		resp.setContentType("text/plain; charset=UTF-8");
		resp.setCharacterEncoding("UTF-8");
		
		String urlCalled = req.getRequestURL().toString().trim();
	
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl::" + splitUrl);
		
		try {
			ObjectifyService.reset();
			ofy().consistency(Consistency.STRONG);
			ofy().clear();
			
			comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
			if(comp!=null){
				String companyId = comp.getCompanyId() + "";
				String authCode = req.getParameter("authCode").trim();
				String customerBranchId = null;
				try {
					customerBranchId = req.getParameter("customerBranchId").trim();
				} catch (Exception e) {

				}
				String customerId = null;
				try {
					customerId = req.getParameter("customerId").trim();
				} catch (Exception e) {

				}
				String branch = null;
				try {
					branch = req.getParameter("branch").trim();
				} catch (Exception e) {

				}
				String status = null;
				try {
					status = req.getParameter("status").trim();
				} catch (Exception e) {

				}
				String salesPerson = null;
				try {
					salesPerson = req.getParameter("salesPerson").trim();
				} catch (Exception e) {

				}
				String fromDate = null;
				try {
					fromDate = req.getParameter("fromDate").trim();
				} catch (Exception e) {

				}
				String toDate = null;
				try{
					toDate=req.getParameter("toDate").trim();
				}catch(Exception e){
					
				}
				
				String assessmentId = null;
				try{
					assessmentId=req.getParameter("assessmentId").trim();
				}catch(Exception e){
					
				}
				
				//Ashwini Patil Date:24-02-2023
				String apiCallFrom = null;
				try {
					apiCallFrom = req.getParameter("apiCallFrom").trim();
				} catch (Exception e) {

				}
				
				Date assessmentFrmDt=null;
				Date assessmentToDt=null;
				
				
				
				logger.log(Level.SEVERE, "companyId::::::::::" + companyId);
				logger.log(Level.SEVERE, "authCode::::::::::" + authCode);
				logger.log(Level.SEVERE, "customerBranchId::::::::::" + customerBranchId);
				
				logger.log(Level.SEVERE, "customerId::::::::::" + customerId);
				logger.log(Level.SEVERE, "branch::::::::::" + branch);
				logger.log(Level.SEVERE, "status::::::::::" + status);
				logger.log(Level.SEVERE, "salesPerson::::::::::" + salesPerson);
				
				logger.log(Level.SEVERE, "fromDate::::::::::" + fromDate);
				logger.log(Level.SEVERE, "toDate::::::::::" + toDate);
				
				logger.log(Level.SEVERE, "assessmentId::::::::::" + assessmentId);
				
				
				if(fromDate!=null&&!fromDate.equals("")&&toDate!=null&&!toDate.equals("")){
					try{
						assessmentFrmDt=sdf.parse(fromDate);
						assessmentToDt=sdf.parse(toDate);
					}catch(Exception e){
						errorObj.put("Failed","Date formate should be dd/MM/yyyy.");
						resp.getWriter().write(errorObj.toJSONString().replaceAll("\\\\", ""));
						return;
					}
				}
				
				List<AssesmentReport> assessmentList = null;
				
				if(companyId.trim().equals(authCode.trim())){
					
					Customer customer=null;
					if((customerId!=null&&!customerId.equals(""))){
						/**
						 * @author Anil
						 * @since 15-02-2022
						 * if customer id comes as zero then do not throw error
						 * raised by Hasrshal
						 */
						int custId=0;
						try{
							custId=Integer.parseInt(customerId);
						}catch(Exception e){
							customerId = null;
							errorObj.put("Failed","Customer id should be numeric!");
							resp.getWriter().write(errorObj.toJSONString().replaceAll("\\\\", ""));
							return;
						}
						if(custId!=0){
							customer=ofy().load().type(Customer.class).filter("companyId", Long.parseLong(companyId.trim())).filter("count", Integer.parseInt(customerId.trim())).first().now();
							if(customer==null){
								errorObj.put("Failed","Customer id is invalid!");
								resp.getWriter().write(errorObj.toJSONString().replaceAll("\\\\", ""));
								return;
							}
						}
					}
					
					if((customerBranchId!=null&&!customerBranchId.equals(""))){
						/**
						 * @author Anil
						 * @since 15-02-2022
						 * if customer branch id comes as zero then do not throw error
						 * raised by Hasrshal
						 */
						int custBranchId=0;
						try{
							custBranchId=Integer.parseInt(customerBranchId.trim());
						}catch(Exception e){
							customerBranchId = null;
							errorObj.put("Failed","Customer branch id should be numeric!");
							resp.getWriter().write(errorObj.toJSONString().replaceAll("\\\\", ""));
							return;
						}
						
						if(custBranchId!=0){
							CustomerBranchDetails customerBranch=ofy().load().type(CustomerBranchDetails.class).filter("companyId", Long.parseLong(companyId.trim())).filter("count", Integer.parseInt(customerBranchId.trim())).first().now();
							if(customerBranch==null){
								errorObj.put("Failed","Customer branch id is invalid!");
								resp.getWriter().write(errorObj.toJSONString().replaceAll("\\\\", ""));
								return;
							}
						}
					}
					
					if(assessmentFrmDt!=null&&assessmentToDt!=null&&(assessmentId==null||assessmentId.equals(""))){
						logger.log(Level.SEVERE, "Filter condition 1::::::::::");
						
						List<AssesmentReport> assessmentList1;
						if(customer!=null){
							assessmentList1=ofy().load().type(AssesmentReport.class).filter("companyId", Long.parseLong(companyId))
									.filter("cinfo.count", Integer.parseInt(customerId.trim()))
									.filter("assessmentDate >=", assessmentFrmDt)
									.filter("assessmentDate <=", assessmentToDt)
									.list();
						}else{
							assessmentList1=ofy().load().type(AssesmentReport.class).filter("companyId", Long.parseLong(companyId))
									.filter("assessmentDate >=", assessmentFrmDt)
									.filter("assessmentDate <=", assessmentToDt)
									.list();
						}
						
						
						if(assessmentList1!=null&&assessmentList1.size()!=0){
							
							logger.log(Level.SEVERE, "Before Filtering result size : "+assessmentList1.size());
							assessmentList=new ArrayList<AssesmentReport>();
							
							for(AssesmentReport entity:assessmentList1){
								if(branch!=null&&!branch.equals("")){
									if(!branch.equals(entity.getBranch())){
										continue;
									}
								}
								if(status!=null&&!status.equals("")){
									if(!status.equals(entity.getStatus())){
										continue;
									}
								}
								if(salesPerson!=null&&!salesPerson.equals("")){
									if(entity.getAssessedPeron1()==null||entity.getAssessedPeron1().equals("")){
										continue;
									}
									if(!salesPerson.equals(entity.getAssessedPeron1())){
										continue;
									}
								}
								assessmentList.add(entity);
								
							}
							
							logger.log(Level.SEVERE, "After Filtering result size : "+assessmentList.size());
						}
						
						
					}else if(assessmentId!=null&&!assessmentId.equals("")){
						logger.log(Level.SEVERE, "Filter condition 2::::::::::");
						
						assessmentList=ofy().load().type(AssesmentReport.class).filter("companyId", Long.parseLong(companyId))
								.filter("count", Integer.parseInt(assessmentId))
								.list();
					}else{
						if((customerId!=null&&!customerId.equals("")&&!customerId.equals("0"))&&(branch!=null&&!branch.equals(""))
								&&(status!=null&&!status.equals(""))&&(salesPerson!=null&&!salesPerson.equals(""))){
							logger.log(Level.SEVERE, "Filter condition 3::::::::::");
							
							assessmentList=ofy().load().type(AssesmentReport.class).filter("companyId", Long.parseLong(companyId))
									.filter("cinfo.count", Integer.parseInt(customerId))
									.filter("branch", branch)
									.filter("status", status)
									.filter("assessedPeron1", salesPerson)
									.list();
							
						}else if((customerId!=null&&!customerId.equals("")&&!customerId.equals("0"))&&(branch!=null&&!branch.equals(""))&&(status!=null&&!status.equals(""))){
							logger.log(Level.SEVERE, "Filter condition 4::::::::::");
							
							assessmentList=ofy().load().type(AssesmentReport.class).filter("companyId", Long.parseLong(companyId))
									.filter("cinfo.count", Integer.parseInt(customerId))
									.filter("branch", branch)
									.filter("status", status)
									.list();
						}else if((customerId!=null&&!customerId.equals("")&&!customerId.equals("0"))&&(branch!=null&&!branch.equals(""))&&(salesPerson!=null&&!salesPerson.equals(""))){
							logger.log(Level.SEVERE, "Filter condition 5::::::::::");
							
							assessmentList=ofy().load().type(AssesmentReport.class).filter("companyId", Long.parseLong(companyId))
									.filter("cinfo.count", Integer.parseInt(customerId))
									.filter("branch", branch)
									.filter("assessedPeron1", salesPerson)
									.list();
						}else if((customerId!=null&&!customerId.equals("")&&!customerId.equals("0"))&&(status!=null&&!status.equals(""))&&(salesPerson!=null&&!salesPerson.equals(""))){
							logger.log(Level.SEVERE, "Filter condition 6::::::::::");
							
							assessmentList=ofy().load().type(AssesmentReport.class).filter("companyId", Long.parseLong(companyId))
									.filter("cinfo.count", Integer.parseInt(customerId))
									.filter("status", status)
									.filter("assessedPeron1", salesPerson)
									.list();
						}else if((customerId!=null&&!customerId.equals("")&&!customerId.equals("0"))&&(branch!=null&&!branch.equals(""))){
							logger.log(Level.SEVERE, "Filter condition 7::::::::::");
							
							assessmentList=ofy().load().type(AssesmentReport.class).filter("companyId", Long.parseLong(companyId))
									.filter("cinfo.count", Integer.parseInt(customerId))
									.filter("branch", branch)
									.list();
						}else if((customerId!=null&&!customerId.equals("")&&!customerId.equals("0"))&&(salesPerson!=null&&!salesPerson.equals(""))){
							logger.log(Level.SEVERE, "Filter condition 8::::::::::");
							
							assessmentList=ofy().load().type(AssesmentReport.class).filter("companyId", Long.parseLong(companyId))
									.filter("cinfo.count", Integer.parseInt(customerId))
									.filter("assessedPeron1", salesPerson)
									.list();
						}else if((customerId!=null&&!customerId.equals("")&&!customerId.equals("0"))&&(status!=null&&!status.equals(""))){
							logger.log(Level.SEVERE, "Filter condition 9::::::::::");
							
							assessmentList=ofy().load().type(AssesmentReport.class).filter("companyId", Long.parseLong(companyId))
									.filter("cinfo.count", Integer.parseInt(customerId))
									.filter("status", status)
									.list();
						}else if((salesPerson!=null&&!salesPerson.equals(""))&&(branch!=null&&!branch.equals(""))){
							logger.log(Level.SEVERE, "Filter condition 10::::::::::");
							
							assessmentList=ofy().load().type(AssesmentReport.class).filter("companyId", Long.parseLong(companyId))
									.filter("branch", branch)
									.filter("assessedPeron1", salesPerson)
									.list();
						}else if((status!=null&&!status.equals(""))&&(salesPerson!=null&&!salesPerson.equals(""))){
							logger.log(Level.SEVERE, "Filter condition 11::::::::::");
							
							assessmentList=ofy().load().type(AssesmentReport.class).filter("companyId", Long.parseLong(companyId))
									.filter("status", status)
									.filter("assessedPeron1", salesPerson)
									.list();
						}else if((branch!=null&&!branch.equals(""))&&(status!=null&&!status.equals(""))){
							logger.log(Level.SEVERE, "Filter condition 12::::::::::");
							
							assessmentList=ofy().load().type(AssesmentReport.class).filter("companyId", Long.parseLong(companyId))
									.filter("branch", branch)
									.filter("status", status)
									.list();
						}else if((salesPerson!=null&&!salesPerson.equals(""))){
							logger.log(Level.SEVERE, "Filter condition 13::::::::::");
							
							assessmentList=ofy().load().type(AssesmentReport.class).filter("companyId", Long.parseLong(companyId))
									.filter("assessedPeron1", salesPerson)
									.list();
						}else if((status!=null&&!status.equals(""))){
							logger.log(Level.SEVERE, "Filter condition 14::::::::::");
							
							assessmentList=ofy().load().type(AssesmentReport.class).filter("companyId", Long.parseLong(companyId))
									.filter("status", status)
									.list();
						}else if((branch!=null&&!branch.equals(""))){
							logger.log(Level.SEVERE, "Filter condition 15::::::::::");
							
							assessmentList=ofy().load().type(AssesmentReport.class).filter("companyId", Long.parseLong(companyId))
									.filter("branch", branch)
									.list();
						}else if((customerId!=null&&!customerId.equals("")&&!customerId.equals("0"))){
							logger.log(Level.SEVERE, "Filter condition 16::::::::::");
							
							assessmentList=ofy().load().type(AssesmentReport.class).filter("companyId", Long.parseLong(companyId))
									.filter("cinfo.count", Integer.parseInt(customerId))
									.list();
						}else{
							logger.log(Level.SEVERE, "Filter condition 17::::::::::");
							
							errorObj.put("Failed","Please select proper filters!");
							resp.getWriter().write(errorObj.toJSONString().replaceAll("\\\\", ""));
							return;
						}
						
					}
					
					if(assessmentList!=null&&assessmentList.size()!=0){
						
						//Ashwini Patil Date:19-06-2023
						if(apiCallFrom!=null&&apiCallFrom.equals("CustomerPortal")) {
							if(assessmentList.size()>1) {
								Comparator<AssesmentReport> dateComp=new Comparator<AssesmentReport>() {
									@Override
									public int compare(AssesmentReport arg0, AssesmentReport arg1) {
										return arg0.getAssessmentDate().compareTo(arg1.getAssessmentDate());
									}
								};
								Collections.sort(assessmentList, dateComp);								
							}
						}
						
						JSONArray mainJArray = new JSONArray();
						JSONObject jsonObj  = null;
						JSONArray jArray =null;
						
						for(AssesmentReport entity:assessmentList){
							jsonObj=new JSONObject();
							
							jsonObj.put("companyId",companyId);
							jsonObj.put("customerId",entity.getCinfo().getCount()+"");
							
							if(entity.getCinfo().getFullName().contains("\"")) {
								errorObj= new JSONObject();
								errorObj.put("Failed", "Remove double quote from customer name. Assessment id:"+entity.getCount()+". Contact system administrator.");
								String jsonString = errorObj.toJSONString();
								resp.getWriter().println(jsonString);
								logger.log(Level.SEVERE, "error message sent for customer name :"+entity.getCinfo().getFullName()+" Assessment id:"+entity.getCount());
								return;				
							}
							jsonObj.put("customerName",entity.getCinfo().getFullName());
							jsonObj.put("customerCell", entity.getCinfo().getCellNumber()+"");
							
							if(entity.getCustomerBranch().contains("\"")) {
								errorObj= new JSONObject();
								errorObj.put("Failed", "Remove double quote from customer branch name. Assessment id:"+entity.getCount()+". Contact system administrator.");
								String jsonString = errorObj.toJSONString();
								resp.getWriter().println(jsonString);
								logger.log(Level.SEVERE, "error message sent for customer branch :"+entity.getCustomerBranch()+" Customer id:"+entity.getCinfo().getCount()+"Assessment id:"+entity.getCount());
								return;				
							}
							jsonObj.put("customerBranch",entity.getCustomerBranch());
							jsonObj.put("assessmentDate",sdf.format(entity.getAssessmentDate()));
							jsonObj.put("assessmentId",entity.getCount()+"");
							jsonObj.put("branch",entity.getBranch());
							jsonObj.put("status",entity.getStatus());
							
							if(entity.getAssessedPeron1()!=null&&entity.getAssessedPeron1().contains("\"")) {
								errorObj= new JSONObject();
								errorObj.put("Failed", "Remove double quote from Assessed by 1. Assessment id:"+entity.getCount()+". Contact system administrator.");
								String jsonString = errorObj.toJSONString();
								resp.getWriter().println(jsonString);
								logger.log(Level.SEVERE, "error message sent for Assessed by 1:"+entity.getAssessedPeron1()+" Assessment id:"+entity.getCount());
								return;				
							}
							jsonObj.put("assessedBy1",entity.getAssessedPeron1());
							
							if(entity.getAssessedPeron2()!=null&&entity.getAssessedPeron2().contains("\"")) {
								errorObj= new JSONObject();
								errorObj.put("Failed", "Remove double quote from Assessed by 2. Assessment id:"+entity.getCount()+". Contact system administrator.");
								String jsonString = errorObj.toJSONString();
								resp.getWriter().println(jsonString);
								logger.log(Level.SEVERE, "error message sent for Assessed by 2:"+entity.getAssessedPeron2()+" Assessment id:"+entity.getCount());
								return;				
							}
							jsonObj.put("assessedBy2",entity.getAssessedPeron2());
							
							if(entity.getAssessedPeron3()!=null&&entity.getAssessedPeron3().contains("\"")) {
								errorObj= new JSONObject();
								errorObj.put("Failed", "Remove double quote from Assessed by 3. Assessment id:"+entity.getCount()+". Contact system administrator.");
								String jsonString = errorObj.toJSONString();
								resp.getWriter().println(jsonString);
								logger.log(Level.SEVERE, "error message sent for Assessed by 3:"+entity.getAssessedPeron3()+" Assessment id:"+entity.getCount());
								return;				
							}
							jsonObj.put("assessedBy3",entity.getAssessedPeron3());
							
							if(entity.getAccompainedByPerson1()!=null&&entity.getAccompainedByPerson1().contains("\"")) {
								errorObj= new JSONObject();
								errorObj.put("Failed", "Remove double quote from Accompanied by 1. Assessment id:"+entity.getCount()+". Contact system administrator.");
								String jsonString = errorObj.toJSONString();
								resp.getWriter().println(jsonString);
								logger.log(Level.SEVERE, "error message sent for Accompanied by 1:"+entity.getAccompainedByPerson1()+" Assessment id:"+entity.getCount());
								return;				
							}
							jsonObj.put("accompainedBy1",entity.getAccompainedByPerson1());
							
							if(entity.getAccompainedByPerson2()!=null&&entity.getAccompainedByPerson2().contains("\"")) {
								errorObj= new JSONObject();
								errorObj.put("Failed", "Remove double quote from Accompanied by 2. Assessment id:"+entity.getCount()+". Contact system administrator.");
								String jsonString = errorObj.toJSONString();
								resp.getWriter().println(jsonString);
								logger.log(Level.SEVERE, "error message sent for Accompanied by 2:"+entity.getAccompainedByPerson2()+" Assessment id:"+entity.getCount());
								return;				
							}
							jsonObj.put("accompainedBy2",entity.getAccompainedByPerson2());
							
							if(entity.getAccompainedByPerson3()!=null&&entity.getAccompainedByPerson3().contains("\"")) {
								errorObj= new JSONObject();
								errorObj.put("Failed", "Remove double quote from Accompanied by 3. Assessment id:"+entity.getCount()+". Contact system administrator.");
								String jsonString = errorObj.toJSONString();
								resp.getWriter().println(jsonString);
								logger.log(Level.SEVERE, "error message sent for Accompanied by 3:"+entity.getAccompainedByPerson3()+" Assessment id:"+entity.getCount());
								return;				
							}
							jsonObj.put("accompainedBy3",entity.getAccompainedByPerson3());
							
							if(entity.getServiceLocation()!=null) {
								if(entity.getServiceLocation().contains("\"")) {
									errorObj= new JSONObject();
									errorObj.put("Failed", "Remove double quote from service location. Assessment id:"+entity.getCount()+". Contact system administrator.");
									String jsonString = errorObj.toJSONString();
									resp.getWriter().println(jsonString);
									logger.log(Level.SEVERE, "error message sent for service location:"+entity.getServiceLocation()+" Assessment id:"+entity.getCount());
									return;				
								}
								jsonObj.put("serviceLocation", entity.getServiceLocation());
							}else
								jsonObj.put("serviceLocation", "");
							
							if(entity.getServiceLocationId()!=0)
								jsonObj.put("serviceLocationId", entity.getServiceLocationId());
							else
								jsonObj.put("serviceLocationId", "");
							
							if(entity.getCustomerBranchId()!=0)
								jsonObj.put("customerBranchId", entity.getCustomerBranchId());
							else
								jsonObj.put("customerBranchId", "");
							
							int totalObv=0;
							int openObv=0;
							int closedObv=0;
							
							
							//Ashwini Patil Date:24-02-2023
							if(apiCallFrom!=null&&apiCallFrom.equals("CustomerPortal")) {
								if(entity.getAssessmentDetailsLIst()!=null) {			
									totalObv=entity.getAssessmentDetailsLIst().size();
									for(AssesmentReportEmbedForTable ar:entity.getAssessmentDetailsLIst()) {
										if(ar.getStatus().equals("Open")||ar.getStatus().equals("Created"))
											openObv++;
										if(ar.getStatus().equals("Closed"))
											closedObv++;
									}
								}
								jsonObj.put("totalObservations", totalObv);
								jsonObj.put("openObservations", openObv);
								jsonObj.put("closedObservations", closedObv);
							}

							
							if(entity.getAssessmentDetailsLIst()!=null&&entity.getAssessmentDetailsLIst().size()!=0){
								jArray=new JSONArray();
								for(AssesmentReportEmbedForTable task:entity.getAssessmentDetailsLIst()){
									JSONObject taskJObj=new JSONObject();
									taskJObj.put("serialNo", task.getCount()+"");
									
									if(task.getArea().contains("\"")) {
										errorObj= new JSONObject();
										errorObj.put("Failed", "Remove double quote from area. Assessment id:"+entity.getCount()+". Contact system administrator.");
										String jsonString = errorObj.toJSONString();
										resp.getWriter().println(jsonString);
										logger.log(Level.SEVERE, "error message sent for area:"+task.getArea()+" Assessment id:"+entity.getCount());
										return;				
									}
									taskJObj.put("area", task.getArea());									
									
									taskJObj.put("riskLevel", task.getRiskLevel());
									
									if(task.getCategory().contains("\"")) {
										errorObj= new JSONObject();
										errorObj.put("Failed", "Remove double quote from category. Assessment id:"+entity.getCount()+". Contact system administrator.");
										String jsonString = errorObj.toJSONString();
										resp.getWriter().println(jsonString);
										logger.log(Level.SEVERE, "error message sent for category:"+task.getCategory()+" Assessment id:"+entity.getCount());
										return;				
									}
									taskJObj.put("category", task.getCategory());
									
									if(task.getLocation().contains("\"")) {
										errorObj= new JSONObject();
										errorObj.put("Failed", "Remove double quote from observation. Assessment id:"+entity.getCount()+". Contact system administrator.");
										String jsonString = errorObj.toJSONString();
										resp.getWriter().println(jsonString);
										logger.log(Level.SEVERE, "error message sent for observation:"+task.getLocation()+" Assessment id:"+entity.getCount());
										return;				
									}
									taskJObj.put("observation", task.getLocation());
									
									if(task.getConsequences().contains("\"")) {
										errorObj= new JSONObject();
										errorObj.put("Failed", "Remove double quote from impact. Assessment id:"+entity.getCount()+". Contact system administrator.");
										String jsonString = errorObj.toJSONString();
										resp.getWriter().println(jsonString);
										logger.log(Level.SEVERE, "error message sent for impact:"+task.getConsequences()+" Assessment id:"+entity.getCount());
										return;				
									}
									taskJObj.put("impact", task.getConsequences());
									
									if(task.getActionPlanForCompany().contains("\"")) {
										errorObj= new JSONObject();
										errorObj.put("Failed", "Remove double quote from Action plan by company. Assessment id:"+entity.getCount()+". Contact system administrator.");
										String jsonString = errorObj.toJSONString();
										resp.getWriter().println(jsonString);
										logger.log(Level.SEVERE, "error message sent for Action plan by company:"+task.getActionPlanForCompany()+" Assessment id:"+entity.getCount());
										return;				
									}
									taskJObj.put("actionPlanByCompany", task.getActionPlanForCompany());
									
									if(task.getActionPlanForCustomer().contains("\"")) {
										errorObj= new JSONObject();
										errorObj.put("Failed", "Remove double quote from Recommendation to Customer. Assessment id:"+entity.getCount()+". Contact system administrator.");
										String jsonString = errorObj.toJSONString();
										resp.getWriter().println(jsonString);
										logger.log(Level.SEVERE, "error message sent for Recommendation to Customer:"+task.getActionPlanForCustomer()+" Assessment id:"+entity.getCount());
										return;				
									}
									taskJObj.put("recommendationToCustomer", task.getActionPlanForCustomer());
									taskJObj.put("status", task.getStatus());
									taskJObj.put("imageURL",task.getDocUpload().getUrl());//Ashwini Patil Date: 12-07-2022
									
									String date="";
									if(task.getClosureDate()!=null){
										date=sdf.format(task.getClosureDate());
									}
									taskJObj.put("closureDate", date);
									
									if(task.getRemark()!=null&&task.getRemark().contains("\"")) {
										errorObj= new JSONObject();
										errorObj.put("Failed", "Remove double quote from Closure Remark. Assessment id:"+entity.getCount()+". Contact system administrator.");
										String jsonString = errorObj.toJSONString();
										resp.getWriter().println(jsonString);
										logger.log(Level.SEVERE, "error message sent for Closure Remark:"+task.getRemark()+" Assessment id:"+entity.getCount());
										return;				
									}
									taskJObj.put("remark", task.getRemark());
									jArray.add(taskJObj);
								}
								jsonObj.put("taskList", jArray);
							}
							
							
							mainJArray.add(jsonObj);
						}
						
						logger.log(Level.SEVERE, "json array:::::::::::::::::::::" + mainJArray);
						String jsonString = mainJArray.toJSONString().replaceAll("\\\\", "");
						logger.log(Level.SEVERE, jsonString);
						resp.getWriter().println(jsonString);
						
					}else{
						errorObj.put("Failed","No result found!");
						resp.getWriter().write(errorObj.toJSONString().replaceAll("\\\\", ""));
						return;
					}
				}else{
					errorObj.put("Failed","Authentication failed!");
					resp.getWriter().write(errorObj.toJSONString().replaceAll("\\\\", ""));
					return;
				}
			}else{
				errorObj.put("Failed","Company not found!");
				resp.getWriter().write(errorObj.toJSONString().replaceAll("\\\\", ""));
				return;
			}
			
		}catch(Exception e){
			e.printStackTrace();
			errorObj.put("Failed",e.getMessage());
			resp.getWriter().write(errorObj.toJSONString().replaceAll("\\\\", ""));
			return;
		}
	}

}
