package com.slicktechnologies.server.android.assessment;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.googlecode.objectify.ObjectifyService;
import com.slicktechnologies.client.views.customerbranchdetails.servicelocation.Area;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchServiceLocation;

public class FetchServiceLocation extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5509329975870916954L;
	
	Logger logger = Logger.getLogger("FetchServiceLocation.class");
	Company comp;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		resp.setHeader("Access-Control-Allow-Origin", "*");

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		RegisterServiceImpl impl = new RegisterServiceImpl();
		impl.getClass();
		
		resp.setContentType("text/plain; charset=UTF-8");
		resp.setCharacterEncoding("UTF-8");
		
		String urlCalled = req.getRequestURL().toString().trim();
	
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl::" + splitUrl);
		
		JSONObject errorObj  = new JSONObject(); 
		
		
		try {
			ObjectifyService.reset();
			ofy().consistency(Consistency.STRONG);
			ofy().clear();
			
			comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
			if(comp!=null){
				String companyId = comp.getCompanyId() + "";
				String authCode = req.getParameter("authCode").trim();
				String customerId = req.getParameter("customerId").trim();
				
				String customerBranchId="";
				try{
					customerBranchId=req.getParameter("customerBranchId").trim();
				}catch(Exception e){
					
				}
				
				String action="";
				try{
					action=req.getParameter("action").trim();
				}catch(Exception e){
					
				}

				
				logger.log(Level.SEVERE, "companyId::::::::::" + companyId);
				logger.log(Level.SEVERE, "authCode::::::::::" + authCode);
				logger.log(Level.SEVERE, "customerId::::::::::" + customerId);
				logger.log(Level.SEVERE, "customerBranchId::::::::::" + customerBranchId);
				if(companyId.trim().equals(authCode.trim())){
					
					Customer customer=ofy().load().type(Customer.class).filter("companyId", Long.parseLong(companyId.trim())).filter("count", Integer.parseInt(customerId.trim())).first().now();
					if(customer==null){
						errorObj.put("Failed","Customer id is invalid!");
						resp.getWriter().write(errorObj.toJSONString().replaceAll("\\\\", ""));
						return;
					}
					
					if(action!=null&&!action.equals("")){
						if(action.equals("Fetch customer branch")){
							
							List<CustomerBranchDetails> customerBranchList=null;
							if(customerBranchId!=null&&!customerBranchId.equals("")&&Integer.parseInt(customerBranchId.trim())>0){
								customerBranchList=ofy().load().type(CustomerBranchDetails.class).filter("companyId", Long.parseLong(companyId.trim())).filter("count", Integer.parseInt(customerBranchId.trim())).list();
							}else{
								customerBranchList=ofy().load().type(CustomerBranchDetails.class).filter("companyId", Long.parseLong(companyId.trim())).filter("cinfo.count", Integer.parseInt(customerId.trim())).list();
							}

							if (customerBranchList != null && customerBranchList.size() != 0) {

								// Ashwini Patil Date:2-05-2023 PSIPL want sorted customer branches in pedio
								Comparator<CustomerBranchDetails> nameComp = new Comparator<CustomerBranchDetails>() {
									@Override
									public int compare(CustomerBranchDetails arg0, CustomerBranchDetails arg1) {
										return arg0.getBusinessUnitName().compareTo(arg1.getBusinessUnitName());
									}
								};
								Collections.sort(customerBranchList, nameComp);

								JSONArray mainJArray = new JSONArray();
								JSONObject jsonObj  = null;
								
								for(CustomerBranchDetails entity:customerBranchList){
									jsonObj=new JSONObject();
									jsonObj.put("companyId", entity.getCompanyId());
									jsonObj.put("customerId", entity.getCinfo().getCount());

									if (entity.getCinfo().getFullName().contains("\"")) {
										errorObj = new JSONObject();
										errorObj.put("Failed",
												"Remove double quote from customer name. Customer id:"
														+ entity.getCinfo().getCount()+". Contact system administrator.");
										String jsonString = errorObj.toJSONString();
										resp.getWriter().println(jsonString);
										logger.log(Level.SEVERE,
												"error message sent for customer name "
														+ entity.getCinfo().getFullName() + "Customer branch id:"
														+ entity.getCount());
										return;
									}
									jsonObj.put("customerName", entity.getCinfo().getFullName());
									jsonObj.put("customerCell", entity.getCinfo().getCellNumber());
									if (entity.getCinfo().getPocName().contains("\"")) {
										errorObj = new JSONObject();
										errorObj.put("Failed",
												"Remove double quote from customer full name. Customer id:"
														+ entity.getCinfo().getCount()+". Contact system administrator.");
										String jsonString = errorObj.toJSONString();
										resp.getWriter().println(jsonString);
										logger.log(Level.SEVERE,
												"error message sent for customer full name "
														+ entity.getCinfo().getPocName() + "Customer branch id:"
														+ entity.getCount());
										return;
									}
									jsonObj.put("customerPoc", entity.getCinfo().getPocName());

									jsonObj.put("customerBranchId", entity.getCount());
									if (entity.getBusinessUnitName().contains("\"")) {
										errorObj = new JSONObject();
										errorObj.put("Failed",
												"Remove double quote from customer branch name. Customer branch id:"
														+ entity.getCount()+". Contact system administrator.");
										String jsonString = errorObj.toJSONString();
										resp.getWriter().println(jsonString);
										logger.log(Level.SEVERE,
												"error message sent for customer branch name "
														+ entity.getBusinessUnitName() + "Customer branch id:"
														+ entity.getCount());
										return;
									}
									jsonObj.put("customerBranchName", entity.getBusinessUnitName());
									jsonObj.put("gstin", entity.getGSTINNumber());
									jsonObj.put("customerBranchCellNo", entity.getCellNumber1());
									jsonObj.put("customerBranchEmail", entity.getEmail());
									jsonObj.put("serviceBranch", entity.getBranch());

									JSONArray billAddArray = new JSONArray();
									if (entity.getBillingAddress() != null) {
										JSONObject billObj = new JSONObject();
										if (entity.getBillingAddress().getAddrLine1().contains("\"")) {
											errorObj = new JSONObject();
											errorObj.put("Failed",
													"Remove double quote from customer branch billing address line1. Customer branch id:"
															+ entity.getCount()+". Contact system administrator.");
											String jsonString = errorObj.toJSONString();
											resp.getWriter().println(jsonString);
											logger.log(Level.SEVERE,
													"error message sent for customer branch billing address line 1 :"
															+ entity.getBillingAddress().getAddrLine1()
															+ " customer branch id:" + entity.getCount());
											return;
										}
										billObj.put("addressLine1", entity.getBillingAddress().getAddrLine1());

										if (entity.getBillingAddress().getAddrLine2().contains("\"")) {
											errorObj = new JSONObject();
											errorObj.put("Failed",
													"Remove double quote from customer branch billing address line2. Customer branch id:"
															+ entity.getCount()+". Contact system administrator.");
											String jsonString = errorObj.toJSONString();
											resp.getWriter().println(jsonString);
											logger.log(Level.SEVERE,
													"error message sent for customer branch billing address line 2 :"
															+ entity.getBillingAddress().getAddrLine2()
															+ " customer branch id:" + entity.getCount());
											return;
										}
										billObj.put("addressLine2", entity.getBillingAddress().getAddrLine2());

										if (entity.getBillingAddress().getLandmark().contains("\"")) {
											errorObj = new JSONObject();
											errorObj.put("Failed",
													"Remove double quote from customer branch billing address landmark. Customer branch id:"
															+ entity.getCount()+". Contact system administrator.");
											String jsonString = errorObj.toJSONString();
											resp.getWriter().println(jsonString);
											logger.log(Level.SEVERE,
													"error message sent for customer branch billing address landmark :"
															+ entity.getBillingAddress().getLandmark()
															+ " customer branch id:" + entity.getCount());
											return;
										}
										billObj.put("landmark", entity.getBillingAddress().getLandmark());

										if (entity.getBillingAddress().getLocality().contains("\"")) {
											errorObj = new JSONObject();
											errorObj.put("Failed",
													"Remove double quote from customer branch billing address locality. Customer branch id:"
															+ entity.getCount()+". Contact system administrator.");
											String jsonString = errorObj.toJSONString();
											resp.getWriter().println(jsonString);
											logger.log(Level.SEVERE,
													"error message sent for customer branch billing address locality :"
															+ entity.getBillingAddress().getLocality()
															+ " customer branch id:" + entity.getCount());
											return;
										}
										billObj.put("locality", entity.getBillingAddress().getLocality());

										if (entity.getBillingAddress().getCity().contains("\"")) {
											errorObj = new JSONObject();
											errorObj.put("Failed",
													"Remove double quote from customer branch billing address city. Customer branch id:"
															+ entity.getCount()+". Contact system administrator.");
											String jsonString = errorObj.toJSONString();
											resp.getWriter().println(jsonString);
											logger.log(Level.SEVERE,
													"error message sent for customer branch billing address city :"
															+ entity.getBillingAddress().getCity()
															+ " customer branch id:" + entity.getCount());
											return;
										}
										billObj.put("city", entity.getBillingAddress().getCity());
										billObj.put("state", entity.getBillingAddress().getState());
										billObj.put("country", entity.getBillingAddress().getCountry());
										billObj.put("pin", entity.getBillingAddress().getPin());
										billAddArray.add(billObj);
									}
									jsonObj.put("billingAddress", billAddArray);

									JSONArray serAddArray = new JSONArray();
									if (entity.getAddress() != null) {
										JSONObject billObj = new JSONObject();
										if (entity.getAddress().getAddrLine1().contains("\"")) {
											errorObj = new JSONObject();
											errorObj.put("Failed",
													"Remove double quote from customer branch service address line1. Customer branch id:"
															+ entity.getCount()+". Contact system administrator.");
											String jsonString = errorObj.toJSONString();
											resp.getWriter().println(jsonString);
											logger.log(Level.SEVERE,
													"error message sent for customer branch service address line 1 :"
															+ entity.getAddress().getAddrLine1()
															+ " customer branch id:" + entity.getCount());
											return;
										}
										billObj.put("addressLine1", entity.getAddress().getAddrLine1());

										if (entity.getAddress().getAddrLine2().contains("\"")) {
											errorObj = new JSONObject();
											errorObj.put("Failed",
													"Remove double quote from customer branch service address line2. Customer branch id:"
															+ entity.getCount()+". Contact system administrator.");
											String jsonString = errorObj.toJSONString();
											resp.getWriter().println(jsonString);
											logger.log(Level.SEVERE,
													"error message sent for customer branch service address line 2 :"
															+ entity.getAddress().getAddrLine2()
															+ " customer branch id:" + entity.getCount());
											return;
										}
										billObj.put("addressLine2", entity.getAddress().getAddrLine2());

										if (entity.getAddress().getLandmark().contains("\"")) {
											errorObj = new JSONObject();
											errorObj.put("Failed",
													"Remove double quote from customer branch service address landmark. Customer branch id:"
															+ entity.getCount()+". Contact system administrator.");
											String jsonString = errorObj.toJSONString();
											resp.getWriter().println(jsonString);
											logger.log(Level.SEVERE,
													"error message sent for customer branch service address landmark :"
															+ entity.getAddress().getLandmark() + " customer branch id:"
															+ entity.getCount());
											return;
										}
										billObj.put("landmark", entity.getAddress().getLandmark());

										if (entity.getAddress().getLocality().contains("\"")) {
											errorObj = new JSONObject();
											errorObj.put("Failed",
													"Remove double quote from customer branch service address locality. Customer branch id:"
															+ entity.getCount()+". Contact system administrator.");
											String jsonString = errorObj.toJSONString();
											resp.getWriter().println(jsonString);
											logger.log(Level.SEVERE,
													"error message sent for customer branch service address locality :"
															+ entity.getAddress().getLocality() + " customer branch id:"
															+ entity.getCount());
											return;
										}
										billObj.put("locality", entity.getAddress().getLocality());

										if (entity.getAddress().getCity().contains("\"")) {
											errorObj = new JSONObject();
											errorObj.put("Failed",
													"Remove double quote from customer branch service address city. Customer branch id:"
															+ entity.getCount()+". Contact system administrator.");
											String jsonString = errorObj.toJSONString();
											resp.getWriter().println(jsonString);
											logger.log(Level.SEVERE,
													"error message sent for customer branch service address city :"
															+ entity.getAddress().getCity() + " customer branch id:"
															+ entity.getCount());
											return;
										}
										billObj.put("city", entity.getAddress().getCity());
										billObj.put("state", entity.getAddress().getState());
										billObj.put("country", entity.getAddress().getCountry());
										billObj.put("pin", entity.getAddress().getPin());
										serAddArray.add(billObj);
									}
									jsonObj.put("serviceAddress",serAddArray);	
									mainJArray.add(jsonObj);
								}
								
								logger.log(Level.SEVERE, "json array:::::::::::::::::::::" + mainJArray);
								String jsonString = mainJArray.toJSONString().replaceAll("\\\\", "");
								logger.log(Level.SEVERE, jsonString);
								resp.getWriter().println(jsonString);
							} else {
								errorObj.put("Failed", "No customer branch found!");
								resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
								return;
							}

						} else if (action.equals("Fetch service location")) {

							CustomerBranchDetails customerBranch = ofy().load().type(CustomerBranchDetails.class)
									.filter("companyId", Long.parseLong(companyId.trim()))
									.filter("count", Integer.parseInt(customerBranchId.trim())).first().now();
							if (customerBranch == null) {
								errorObj.put("Failed", "Customer branch id is invalid!");
								resp.getWriter().write(errorObj.toJSONString().replaceAll("\\\\", ""));
								return;
							}

							List<CustomerBranchServiceLocation> serviceLocationList = ofy().load()
									.type(CustomerBranchServiceLocation.class)
									.filter("companyId", Long.parseLong(companyId.trim()))
									.filter("cinfo.count", Integer.parseInt(customerId.trim()))
									.filter("custBranchId", Integer.parseInt(customerBranchId.trim()))
									.filter("status", true).list();
							if (serviceLocationList != null && serviceLocationList.size() != 0) {

								JSONArray mainJArray = new JSONArray();
								JSONObject jsonObj = null;
								JSONArray jArray = null;

								for (CustomerBranchServiceLocation entity : serviceLocationList) {
									jsonObj = new JSONObject();
									jsonObj.put("companyId", companyId);
									jsonObj.put("customerId", customerId);
									jsonObj.put("customerBranchId", customerBranchId);
									jsonObj.put("serviceLocationId", entity.getCount() + "");

									if (entity.getServiceLocation().contains("\"")) {
										errorObj = new JSONObject();
										errorObj.put("Failed",
												"Remove double quote from service location name. Customer branch id:"
														+ customerBranchId + " and serviceLocationId"
														+ entity.getCount()+". Contact system administrator.");
										String jsonString = errorObj.toJSONString();
										resp.getWriter().println(jsonString);
										logger.log(Level.SEVERE,
												"error message sent for customer branch service location name :"
														+ entity.getServiceLocation() + " customer branch id:"
														+ customerBranchId + " and serviceLocationId:"
														+ entity.getCount());
										return;
									}
									jsonObj.put("serviceLocationName", entity.getServiceLocation());

									if (entity.getAreaList2() != null && entity.getAreaList2().size() > 0) {
										jArray = new JSONArray();
										for (Area area : entity.getAreaList2()) {
											JSONObject areaJsonObj = new JSONObject();
											if (area.getArea().contains("\"")) {
												errorObj = new JSONObject();
												errorObj.put("Failed",
														"Remove double quote from service location area. Customer branch id:"
																+ customerBranchId + " and serviceLocationId"
																+ entity.getCount()+". Contact system administrator.");
												String jsonString = errorObj.toJSONString();
												resp.getWriter().println(jsonString);
												logger.log(Level.SEVERE,
														"error message sent for customer branch service location area :"
																+ area.getArea() + " customer branch id:"
																+ customerBranchId + " and serviceLocationId:"
																+ entity.getCount());
												return;
											}
											areaJsonObj.put("area", area.getArea());
											areaJsonObj.put("plannedServices", area.getPlannedservices());
											if (area.getFrequency() != null && !area.getFrequency().equals("")) {
												if (area.getFrequency().contains("\"")) {
													errorObj = new JSONObject();
													errorObj.put("Failed",
															"Remove double quote from service location area frequency. Customer branch id:"
																	+ customerBranchId + " and serviceLocationId"
																	+ entity.getCount()+". Contact system administrator.");
													String jsonString = errorObj.toJSONString();
													resp.getWriter().println(jsonString);
													logger.log(Level.SEVERE,
															"error message sent for customer branch service location area frequency :"
																	+ area.getFrequency() + " customer branch id:"
																	+ customerBranchId + " and serviceLocationId:"
																	+ entity.getCount());
													return;
												}
												areaJsonObj.put("frequency", area.getFrequency());
											}

											jArray.add(areaJsonObj);
										}
										jsonObj.put("areaList", jArray);
									} else {
										if (entity.getAreaList() != null && entity.getAreaList().size() != 0) {
											jArray = new JSONArray();
											for (String area : entity.getAreaList()) {
												JSONObject areaJsonObj = new JSONObject();
												areaJsonObj.put("area", area);
												jArray.add(areaJsonObj);
											}
											jsonObj.put("areaList", jArray);
										}
									}

									mainJArray.add(jsonObj);
								}

								logger.log(Level.SEVERE, "json array:::::::::::::::::::::" + mainJArray);
								String jsonString = mainJArray.toJSONString().replaceAll("\\\\", "");
								logger.log(Level.SEVERE, jsonString);
								resp.getWriter().println(jsonString);

							} else {
								errorObj.put("Failed", "No Service locations found in "+customerBranch.getBusinessUnitName()+" branch!");
								resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
								return;
							}
						} else {
							errorObj.put("Failed", "Action value is missing!");
							resp.getWriter().write(errorObj.toString().replaceAll("\\\\", ""));
							return;
						}

//					CustomerBranchDetails customerBranch=ofy().load().type(CustomerBranchDetails.class).filter("companyId", Long.parseLong(companyId.trim())).filter("count", Integer.parseInt(customerBranchId.trim())).first().now();
//					if(customerBranch==null){
//						errorObj.put("Failed","Customer branch id is invalid!");
//						resp.getWriter().write(errorObj.toJSONString().replaceAll("\\\\", ""));
//						return;
//					}
//					
//					List<CustomerBranchServiceLocation> serviceLocationList=ofy().load().type(CustomerBranchServiceLocation.class).filter("companyId", Long.parseLong(companyId.trim())).filter("cinfo.count", Integer.parseInt(customerId.trim())).filter("custBranchId", Integer.parseInt(customerBranchId.trim())).filter("status", true).list();
//					if(serviceLocationList!=null&&serviceLocationList.size()!=0){
//						
//						JSONArray mainJArray = new JSONArray();
//						JSONObject jsonObj  = null;
//						JSONArray jArray =null;
//						
//						for(CustomerBranchServiceLocation entity:serviceLocationList){
//							jsonObj=new JSONObject();
//							jsonObj.put("companyId", companyId);
//							jsonObj.put("customerId", customerId);
//							jsonObj.put("customerBranchId", customerBranchId);
//							jsonObj.put("serviceLocationId", entity.getCount()+"");
//							jsonObj.put("serviceLocationName", entity.getServiceLocation());
//							
//							if(entity.getAreaList()!=null&&entity.getAreaList().size()!=0){
//								jArray=new JSONArray();
//								for(String area:entity.getAreaList()){
//									JSONObject areaJsonObj=new JSONObject();
//									areaJsonObj.put("area", area);
//									jArray.add(areaJsonObj);
//								}
//								jsonObj.put("areaList", jArray);
//							}
//							
//							mainJArray.add(jsonObj);
//						}
//						
//						logger.log(Level.SEVERE, "json array:::::::::::::::::::::" + mainJArray);
//						String jsonString = mainJArray.toJSONString().replaceAll("\\\\", "");
//						logger.log(Level.SEVERE, jsonString);
//						resp.getWriter().println(jsonString);
						
					}else{
						errorObj.put("Failed","Action value is missing!");
						resp.getWriter().write(errorObj.toJSONString().replaceAll("\\\\", ""));
						return;
					}
				}else{
					errorObj.put("Failed","Authentication failed!");
					resp.getWriter().write(errorObj.toJSONString().replaceAll("\\\\", ""));
					return;
				}
			}else{
				errorObj.put("Failed","Company not found!");
				resp.getWriter().write(errorObj.toJSONString().replaceAll("\\\\", ""));
				return;
			}
			
			
			
		}catch(Exception e){
			e.printStackTrace();
			errorObj.put("Failed",e.getMessage());
			resp.getWriter().write(errorObj.toJSONString().replaceAll("\\\\", ""));
			return;
		}
	}

}
