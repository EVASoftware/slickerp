package com.slicktechnologies.server.android.master;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;

public class BranchCreation extends HttpServlet {

	/**Date 1-2-2020 by AMOL
	 * This API Is use For Branch Creation, Read , and Update Operation
	 * 
	 */
	private static final long serialVersionUID = 7841531282716533377L;
	Logger logger = Logger.getLogger("BranchCreation.class");
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String data=req.getParameter("data");
		
		String screenData = data.trim().replace("#and", "&");
		String urlCalled = req.getRequestURL().toString().trim();
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		logger.log(Level.SEVERE, "Stage One" + screenData);
		
		long companyId = 0l;
		if (comp != null) {
			companyId = comp.getCompanyId();
		} else {
			companyId = Long.parseLong("5348024557502464");
		}
		JSONObject object = null;
		try {
			object = new JSONObject(screenData.trim());
			logger.log(Level.SEVERE, "JSON OBJECT" + object);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String screenName = object.optString("screenName").trim();
		String actionTask = object.optString("actionTask");
		int branchId=0;
		
		if (screenName.equalsIgnoreCase("branchCreation")) {
			if (actionTask.trim().equalsIgnoreCase("createBranch")) {
				logger.log(Level.SEVERE, "Inside branchCraetion");

				String response = createBranch(comp, object);
				resp.getWriter().println(response + "");
				logger.log(Level.SEVERE, "Inside branch create JSON Response"
						+ response);

			} else if (actionTask.trim().equalsIgnoreCase("updateBranch")) {
				logger.log(Level.SEVERE, "Inside update  Branch");
				String branchCode="";
				String branchCity="";
				String branchState="";
				String branchCountry="";
				String branchAddressline2="";
				String branchAddressline1="";
				String branchGstin="";
				String branchEmail="";
				long branchPocCellNo =0l;
				String branchPocName="";
				
				long cellNo=0l;
				boolean branchStatus=false;
				branchId = Integer.parseInt(object.optString("branchId"));
				String branchName = object.optString("branchName");
				try{
				cellNo = Long.parseLong(object.optString("cellNumber"));
				}catch(Exception e){
				cellNo=0l;
				}
				try{
				branchPocName = object.optString("pointOfContactName");
				}catch(Exception e){
				branchPocName="";
				}
				try{
				branchPocCellNo = Long.parseLong(object.optString("pocCellNumber"));
				}catch(Exception e){
				branchPocCellNo=0l;
				}
				
				try{
				branchStatus = Boolean.parseBoolean(object.optString("branchStatus"));
				}catch(Exception e){
				branchStatus=false;
				}
				try{
				branchEmail= object.optString("branchEmail");
				}catch(Exception e){
				branchEmail="";
				}
				
				try{
				branchGstin=object.optString("branchGSTIN");
				}catch(Exception e){
				branchGstin="";
				}
				
				try{
				branchAddressline1=object.optString("addressLine1");
				}catch(Exception e){
				branchAddressline1="";
				}
				
				try{
				branchAddressline2=object.optString("addressLine2");
				}catch(Exception e){
				branchAddressline2="";
				}
				
				try{
				branchCountry=object.optString("branchCountry");
				}catch(Exception e){
				branchCountry="";
				}
				try{
				branchState=object.optString("branchState");
				}catch(Exception e){
				branchState="";
				}
				
				try{
				branchCity=object.optString("branchCity");
				}catch(Exception e){
			    branchCity="";
				}
				
				try{
				branchCode=object.optString("branchCode");
				}catch(Exception e){
				branchCode="";
				}
				Branch branchValidate = ofy().load().type(Branch.class)
						.filter("companyId", companyId)
						.filter("count", branchId)
						.filter("buisnessUnitName", branchName).first().now();

				if (branchValidate != null) {
					if (branchId != branchValidate.getCount()) {
						resp.getWriter().println("Branch "+ object.optString("branchName").trim().toUpperCase()+ " already exist!");
						return;
					}
				}

				Branch branch =ofy().load().type(Branch.class).filter("companyId",companyId).filter("count", branchId).first().now();
				
				branch.setBusinessUnitName(branchName);
				if(cellNo!=0){
				branch.setCellNumber1(cellNo);
				}else{
				branch.setCellNumber1(0);	
				}
				
				if(!branchPocName.equals("")){
				branch.setPocName(branchPocName);
				}else{
				branch.setPocName("");	
				}
				if(branchPocCellNo!=0){
				branch.setPocCell(branchPocCellNo);
				}else{
				branch.setPocCell(0);	
				}
				
				branch.setstatus(branchStatus);
				
				
				if(!branchEmail.equals("")){
				branch.setEmail(branchEmail);
				}else{
				branch.setEmail("");	
				}
				if(!branchGstin.equals("")){
				branch.setGSTINNumber(branchGstin);
				}else{
				branch.setGSTINNumber("");	
				}
				if(!branchCode.equals("")){
				branch.setBranchCode(branchCode);
				}else{
				branch.setBranchCode("");	
				}
				if(!branchAddressline1.equals("")){
				branch.getAddress().setAddrLine1(branchAddressline1);
				}else{
				branch.getAddress().setAddrLine1("");	
				}
				if(!branchAddressline2.equals("")){
				branch.getAddress().setAddrLine2(branchAddressline2);
				}else{
				branch.getAddress().setAddrLine2("");	
				}
				if(!branchCountry.equals("")){
				branch.getAddress().setCountry(branchCountry);
				}else{
				branch.getAddress().setCountry("");
				}
				
				if(!branchState.equals("")){
				branch.getAddress().setState(branchState);
				}else{
				branch.getAddress().setState("");	
				}
				
				if(!branchCity.equals("")){
				branch.getAddress().setCity(branchCity);
				}else{
				branch.getAddress().setCity("");
				}
				

				ofy().save().entity(branch);

				resp.getWriter().println("Branch updated successfully!");

			} else if (actionTask.trim().equalsIgnoreCase("readBranch")) {
				logger.log(Level.SEVERE, "Inside Read Branch " );
				
				List<Branch> branchList = ofy().load().type(Branch.class)
						.filter("companyId", comp.getCompanyId()).list();
				
				JSONObject mainObject = new JSONObject();
				JSONArray brancharrayJson = new JSONArray();
				
				try{
					Comparator<Branch> comparator=new Comparator<Branch>() {
						@Override
						public int compare(Branch e1, Branch e2) {
							return e1.getBusinessUnitName().compareTo(e2.getBusinessUnitName());
						}
					};
					Collections.sort(branchList,comparator);
				
					for (Branch branch : branchList) {

						JSONObject jobj = new JSONObject();
						jobj.put("branchId", branch.getCount());
						if( branch.getBusinessUnitName()!=null){
						jobj.put("branchName", branch.getBusinessUnitName());
						}else{
							jobj.put("branchName", "");	
						}
						if(branch.getCellNumber1()!=null){
						jobj.put("cellNumber", branch.getCellNumber1());
						}else{
						jobj.put("cellNumber", "");
						}
						if(branch.getPocName()!=null){
						jobj.put("pointOfContactName", branch.getPocName());
						}else{
						jobj.put("pointOfContactName","");
						}
						if(branch.getPocCell()!=null){
						jobj.put("pocCellNumber", branch.getPocCell());
						}else{
							jobj.put("pocCellNumber", "");	
						}
						if(branch.getstatus()!=null){
						jobj.put("branchStatus",branch.getstatus());
						}else{
							jobj.put("branchStatus","");	
						}
						if(branch.getEmail()!=null){
						jobj.put("branchEmail",branch.getEmail());
						}else{
						jobj.put("branchEmail","");
						}
						if(branch.getGSTINNumber()!=null){
						jobj.put("branchGSTIN",branch.getGSTINNumber());
						}else{
							jobj.put("branchGSTIN","");	
						}
						
						if(branch.getAddress().getAddrLine1()!=null){
						jobj.put("addressLine1",branch.getAddress().getAddrLine1());
						}else
						{
						jobj.put("addressLine1","");
						}
						if(branch.getAddress().getAddrLine2()!=null){
						jobj.put("addressLine2",branch.getAddress().getAddrLine2());
						}else{
						jobj.put("addressLine2","");	
						}
						if(branch.getAddress().getCountry()!=null){
						jobj.put("branchCountry",branch.getAddress().getCountry());
						}else{
						jobj.put("branchCountry","");
						}
						if(branch.getAddress().getState()!=null){
						jobj.put("branchState",branch.getAddress().getState());
						}else{
						jobj.put("branchState","");	
						}
						if(branch.getAddress().getCity()!=null){
						jobj.put("branchCity",branch.getAddress().getCity());
						}else{
						jobj.put("branchCity","");	
						}
						if(branch.getBranchCode()!=null){
						jobj.put("branchCode",branch.getBranchCode());
						}else{
						jobj.put("branchCode","");	
						}
						brancharrayJson.put(jobj);
					}
				mainObject.put("branchList", brancharrayJson);
				}catch(Exception e){
					e.printStackTrace();
				}
				String jsonString=mainObject.toString().replaceAll("\\\\", "");
				resp.getWriter().println(jsonString);
				logger.log(Level.SEVERE, "read branches Successfully"+jsonString);
			}
		}
	}

	private String createBranch(Company comp, JSONObject object) {
		logger.log(Level.SEVERE, "Inside Branch CREATE Method" );
		
		long companyId = 0l;
		int branchId=0;
		String branchCode="";
		String branchCity="";
		String branchState="";
		String branchCountry="";
		String branchAddressline2="";
		String branchAddressline1="";
		String branchGstin="";
		String branchEmail="";
		long branchPocCellNo =0l;
		String branchPocName="";
		
		long cellNo=0l;
		boolean branchStatus=false;
		
		if (comp != null) {
			companyId = comp.getCompanyId();
		} else {
			companyId = Long.parseLong("5348024557502464");
		}
		
		
		String branchName=object.optString("branchName");
		
		try{
	    cellNo=Long.parseLong(object.optString("cellNumber"));
		}catch(Exception e){
		cellNo=0;	
		}
		
		try{
		branchPocName=object.optString("pointOfContactName");
		}catch(Exception e){
		branchPocName="";	
		}
		
		try{
		branchPocCellNo=Long.parseLong(object.optString("pocCellNumber"));
		}catch(Exception e){
		branchPocCellNo=0;
		}
		try{
		branchStatus=Boolean.parseBoolean(object.optString("branchStatus"));
		}catch(Exception e){
		branchStatus=false;
		}
		try{
		branchEmail= object.optString("branchEmail");
		}catch(Exception e){
		branchEmail="";	
		}
		try{
		branchGstin=object.optString("branchGSTIN");
		}catch(Exception e){
		branchGstin="";
		}
		try{
		branchCode=object.optString("branchCode");
		}catch(Exception e){
		branchCode="";
		}
		try{
		branchAddressline1=object.optString("addressLine1");
		}catch(Exception e){
		branchAddressline1="";	
		}
		try{
		branchAddressline2=object.optString("addressLine2");
		}catch(Exception e){
		branchAddressline2="";	
		}
		try{
		branchCountry=object.optString("branchCountry");
		}catch(Exception e){
		branchCountry="";	
		}
		try{
		branchState=object.optString("branchState");
		}catch(Exception e){
		branchState	="";
		}
		try{
		branchCity=object.optString("branchCity");
		}catch(Exception e){
		branchCity="";	
		}
		
		Branch branch=new Branch();
		
		int count = 0;
		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class)
				.filter("companyId", comp.getCompanyId())
				.filter("processName", "Branch").filter("status", true)
				.first().now();

		long number = ng.getNumber();
		count = (int) number;
		ng.setNumber(count + 1);
		ofy().save().entity(ng);
		
		Branch branchValidate = ofy().load().type(Branch.class)
				.filter("companyId", companyId).filter("buisnessUnitName", branchName).first().now();

		if (branchValidate != null) {
			return "Branch "+object.optString("branchName").trim()+" already exist!";
		}
		
		branch.setCompanyId(companyId);
		branch.setCount(count + 1);
		branch.setBusinessUnitName(branchName);
		if(cellNo!=0){
		branch.setCellNumber1(cellNo);
		}else{
		branch.setCellNumber1(0);	
		}
		
		if(!branchPocName.equals("")){
		branch.setPocName(branchPocName);
		}else{
		branch.setPocName("");	
		}
		if(branchPocCellNo!=0){
		branch.setPocCell(branchPocCellNo);
		}else{
		branch.setPocCell(0);	
		}
		
		branch.setstatus(branchStatus);
		
		
		if(!branchEmail.equals("")){
		branch.setEmail(branchEmail);
		}else{
		branch.setEmail("");	
		}
		if(!branchGstin.equals("")){
		branch.setGSTINNumber(branchGstin);
		}else{
		branch.setGSTINNumber("");	
		}
		if(!branchCode.equals("")){
		branch.setBranchCode(branchCode);
		}else{
		branch.setBranchCode("");	
		}
		if(!branchAddressline1.equals("")){
		branch.getAddress().setAddrLine1(branchAddressline1);
		}else{
		branch.getAddress().setAddrLine1("");	
		}
		if(!branchAddressline2.equals("")){
		branch.getAddress().setAddrLine2(branchAddressline2);
		}else{
		branch.getAddress().setAddrLine2("");	
		}
		if(!branchCountry.equals("")){
		branch.getAddress().setCountry(branchCountry);
		}else{
		branch.getAddress().setCountry("");
		}
		
		if(!branchState.equals("")){
		branch.getAddress().setState(branchState);
		}else{
		branch.getAddress().setState("");	
		}
		
		if(!branchCity.equals("")){
		branch.getAddress().setCity(branchCity);
		}else{
		branch.getAddress().setCity("");
		}
		
		
		
		branchId=count + 1;
		ofy().save().entity(branch);
		logger.log(Level.SEVERE, "Branch CREATED SUCCESSFULLY" );
		return branchId +"- Branch created successfully";
	}
}
