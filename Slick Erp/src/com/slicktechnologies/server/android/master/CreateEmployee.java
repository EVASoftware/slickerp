package com.slicktechnologies.server.android.master;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.rowset.Predicate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.role.UserRole;

public class CreateEmployee extends HttpServlet{

	/**
	 * Description: created by AMOL   Date 10-1-2020
	 * This API is used to "Create New Employee","Update Employee","Read Employee"In All APP
	 */
	private static final long serialVersionUID = 3898074438798302789L;
	Logger logger = Logger.getLogger("CreateEmployee.class");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
//		super.doGet(req, resp);
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String data = req.getParameter("data");
		String screenData = data.trim().replace("#and", "&");
		String urlCalled = req.getRequestURL().toString().trim();
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		
		
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		logger.log(Level.SEVERE, "Stage One" + screenData);
		long companyId = 0l;
		if (comp != null) {
			companyId = comp.getCompanyId();
		} else {
			companyId = Long.parseLong("5348024557502464");
		}
		JSONObject object = null;
		try {
			object = new JSONObject(screenData.trim());
			logger.log(Level.SEVERE, "JSON OBJECT" + object);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String screenName = object.optString("screenName").trim();


		if (screenName.trim().equalsIgnoreCase("createEmployee")) {

			String actionTask = object.optString("actionTask");
			int empId=0;
			
			if (actionTask.trim().equalsIgnoreCase("createEmployee")) {
				String response = createEmployee(comp, object);
				logger.log(Level.SEVERE, "Inside createEmployee CONDITIONNNNN");
				resp.getWriter().println(response + "");

			}else if(actionTask.trim().equalsIgnoreCase("updateEmployee")){
				logger.log(Level.SEVERE, "INSIDE UPDATE EMPLOYEE");
				
				empId=Integer.parseInt(object.optString("employeeId"));
				String userId=object.optString("userId").trim();
				String empName=object.optString("employeeName").trim();
				long cellNumber =0;
				try{
					cellNumber  = Long.parseLong(object.optString("employeeCell"));	
				}catch(Exception e){
					cellNumber  = 0;
				}
				String branch="";
				try{
					branch=object.optString("branch").trim();
				}catch(Exception e){
					branch="";
				}
				
				String userRole=object.optString("userRole").trim();
				String userPass=object.optString("password").trim();
				boolean status= Boolean.parseBoolean(object.optString("employeeStatus").trim());
				
				Employee employee=ofy().load().type(Employee.class).filter("companyId", comp.getCompanyId()).filter("count", empId).first().now();
				
				User user=ofy().load().type(User.class).filter("companyId", comp.getCompanyId()).filter("userName",userId).filter("employeeName",employee.getFullname()).first().now();
				
				logger.log(Level.SEVERE, "USER value"+user);
				
				
				
				
				///
				
	           Employee empCellValidate=ofy().load().type(Employee.class).filter("count",empId).filter("companyId",companyId).filter("contacts.cellNo1", cellNumber).first().now();
				if(empCellValidate!=null){
					if(empId!=empCellValidate.getCount()){
					logger.log(Level.SEVERE, "cell number Validation in EDIT STATE");
					resp.getWriter().println(" Cell number already exist!");
					return ;
					}
				}
				
				Employee empNameValidate=ofy().load().type(Employee.class).filter("count",empId).filter("companyId", companyId).filter("contacts.cellNo1", cellNumber).filter("fullname", empName)
						.first().now();
				
				if(empNameValidate!=null){
					if(empId!=empNameValidate.getCount()){
					logger.log(Level.SEVERE, "INSIDE DUPLICATE EMPLOYEE VALIDATE");
					resp.getWriter().println("Employee "+object.optString("employeeName").trim().toUpperCase()+ " already exist!");
					return ;
					}
				}
				
//				Branch branches=ofy().load().type(Branch.class).filter("companyId",companyId).filter("buisnessUnitName", branch).first().now();
				
//				logger.log(Level.SEVERE, "branch List "+branches);
//				if(branches==null){
//					logger.log(Level.SEVERE, "No Branch is present");
//					resp.getWriter().println("No Such a Branch is Exist in System");
//					return ;
//				}
				logger.log(Level.SEVERE, "employee status set"+status);
				employee.setFullname(empName);
				employee.setCellNumber1(cellNumber);
				
				if(!branch.equals("")){
					employee.setBranchName(branch);	
				}else{
					employee.setBranchName("");	
				}
				
				employee.setStatus(status);
				
				/**
				 * @author Vijay Chougule Date :- 12-11-2020
				 * Des :- as per Nitin sir instruction user role hardcoed as Employee from android so i have changed the message
				 */
				//This need to be loaded
				UserRole userrole=ofy().load().type(UserRole.class).filter("companyId", companyId).filter("roleName",userRole).filter("roleStatus",true).first().now();
				
				logger.log(Level.SEVERE, "USERRROLL"+userrole);
				if(userrole!=null){
					user.setRole(userrole);
					employee.setRoleName(userrole.getRoleName()); // Vijay Date 11-11-2020 role Updated in Employee
				}else{
					resp.getWriter().println("No such role found in ERP. Role Name - "+userRole);
					return ;
				}
				/**
				 * ends here
				 */
				user.setPassword(userPass); 
				user.setStatus(status);
				logger.log(Level.SEVERE, "user status set"+status);
				user.setEmployeeName(empName);
				if(!branch.equals("")){
					user.setBranch(branch);
				}else{
					user.setBranch("");	
				}
				
				ofy().save().entities(user);
				
				ofy().save().entities(employee);
				
				resp.getWriter().println("Employee updated successfully!");
				logger.log(Level.SEVERE, "employee updated Successfully");
				
				
				
			} else if(actionTask.trim().equalsIgnoreCase("readEmployee")){
				
				
				List<Employee> employeeList = ofy().load().type(Employee.class)
						.filter("companyId", comp.getCompanyId()).list();
				
				List<User> userList=ofy().load().type(User.class).filter("companyId", comp.getCompanyId()).list();
				
				JSONObject mainObject = new JSONObject();
				JSONArray employeearrayJson = new JSONArray();
				try{
					Comparator<Employee> comparator=new Comparator<Employee>() {
						@Override
						public int compare(Employee e1, Employee e2) {
							return e1.getFullname().compareTo(e2.getFullname());
						}
					};
					Collections.sort(employeeList,comparator);
					
					for (Employee emp : employeeList) {
						User obj = getUserDetails(emp.getFullname(), userList);

						JSONObject jobj = new JSONObject();
						jobj.put("employeeId", emp.getCount());
						jobj.put("employeeName", emp.getFullname());
						if(emp.getCellNumber1()!=0){
							jobj.put("employeeCell", emp.getCellNumber1());	
						}else{
							jobj.put("employeeCell", "");
						}
						if(emp.getBranchName()!=null){
							jobj.put("branch", emp.getBranchName());
						}else{
							jobj.put("branch", "");
						}
						
						jobj.put("employeeStatus", emp.isStatus());
						if (obj != null) {
							jobj.put("userRole", obj.getRole());
							jobj.put("userId", obj.getUserName());
							jobj.put("password", obj.getPassword());
						} else {
							jobj.put("userRole", "");
							jobj.put("userId", "");
							jobj.put("password", "");
						}

						employeearrayJson.put(jobj);
					}
				mainObject.put("employeeList", employeearrayJson);
				}catch(Exception e){
					e.printStackTrace();
				}
				String jsonString=mainObject.toString().replaceAll("\\\\", "");
				resp.getWriter().println(jsonString);
		}

		}
		
		
		
		
		
	}
	
	private User getUserDetails(String employeeName, List<User> userList) {
		for(User obj:userList){
			if(obj.getEmployeeName().equals(employeeName)){
				return obj;
			}
		}
		return null;
	}
	
	private String createEmployee(Company comp, JSONObject object) {
		logger.log(Level.SEVERE, "Inside createEmployee" );
		long companyId = 0l;
		int empId=0;
		if (comp != null) {
			companyId = comp.getCompanyId();
		} else {
			companyId = Long.parseLong("5348024557502464");
		}
		
		try{
			String empName=object.optString("employeeName").trim();
			
			long cellNumber = 0l;
			try {
				cellNumber  = Long.parseLong(object.optString("employeeCell"));
			} catch (Exception e) {
				e.printStackTrace();
				logger.log(Level.SEVERE, "ERROR cell No is not Long:::::" + e);
			}
			
			Employee empCellValidate=ofy().load().type(Employee.class).filter("companyId",companyId).filter("contacts.cellNo1", cellNumber).first().now();
			if(empCellValidate!=null){
				return " Cell number already exist!";
			}
			
//			Employee empNameValidate=null;
			Employee empNameValidate=ofy().load().type(Employee.class).filter("companyId", companyId).filter("contacts.cellNo1", cellNumber).filter("fullname", empName)
					.first().now();
			
			if(empNameValidate!=null){
				logger.log(Level.SEVERE, "INSIDE DUPLICATE EMPLOYEE VALIDATE");
				return "Employee"+object.optString("employeeName").trim().toUpperCase()+ " already exist!!";
			}
			String branch="";
			try{
				branch=object.optString("branch").trim();
			}catch(Exception e){
				branch="";
			}
			
			
			Branch branches=ofy().load().type(Branch.class).filter("companyId",companyId).filter("buisnessUnitName", branch).first().now();
			
			logger.log(Level.SEVERE, "branch List "+branches);
//			if(branches==null){
//				logger.log(Level.SEVERE, "No Branch is present");
//				return "No Such a Branch is Exist in System";
//			}
			
			String userRole=object.optString("userRole").trim();
			String userId=object.optString("userId").trim();
			String userPass=object.optString("password").trim();
//			boolean status= Boolean.parseBoolean(object.optString("employeeStatus").trim());
			boolean status=true;

			logger.log(Level.SEVERE, "after Password " );  
	
			Employee emp=new Employee();
			
			
			
			emp.setCompanyId(companyId);
			emp.setFullname(empName);
			if(cellNumber!=0){
			emp.setCellNumber1(cellNumber);
			}else{
			emp.setCellNumber1(0l);	
			}
			
			if(!branch.equals("")){
				emp.setBranchName(branch);	
			}else{
				emp.setBranchName("");
			}
			
			emp.setStatus(status);
			
			int count = 0;
			NumberGeneration ng = new NumberGeneration();
			ng = ofy().load().type(NumberGeneration.class)
					.filter("companyId", comp.getCompanyId())
					.filter("processName", "Employee").filter("status", true)
					.first().now();

			long number = ng.getNumber();
			count = (int) number;
			ng.setNumber(count + 1);
			
			ofy().save().entity(ng);
			
			logger.log(Level.SEVERE, "Number Generation" );  
			
			User user =new User();
			user.setCompanyId(companyId);
			/**
			 * @author Vijay Chougule Date :- 12-11-2020
			 * Des :- as per Nitin sir instruction user role hardcoed as Employee from android so i have changed the message
			 *  role must me defined in ERP 
			 */
			UserRole userrole=ofy().load().type(UserRole.class).filter("roleName",userRole).filter("roleStatus",true).first().now();
			if(userrole!=null){
				user.setRole(userrole);
			}else{
				return "No such role found in ERP. Role Name - "+userRole;
			}
			/**
			 * ends here
			 */
			
			user.setUserName(userId);
			 
			User user1=ofy().load().type(User.class).filter("companyId", companyId)
					.filter("userName", userId).first().now(); 
			if(user1!=null){
				return "UserId "+userId+" already exist";
			}
			logger.log(Level.SEVERE, "load USER" ); 
			
			int count1 = 0;
			NumberGeneration ng1 = new NumberGeneration();
			ng1 = ofy().load().type(NumberGeneration.class)
					.filter("companyId", comp.getCompanyId())
					.filter("processName", "User").filter("status", true)
					.first().now();

			long number1 = ng1.getNumber();
			count1 = (int) number1;
			ng1.setNumber(count1 + 1);
			user.setPassword(userPass);
			user.setEmployeeName(empName);
			user.setStatus(status);
			if(!branch.equals("")){
				user.setBranch(branch);	
			}else{
				user.setBranch("");
			}
			
			
			
			user.setCount(count + 1);
			
			ofy().save().entity(ng1);
			
		
			
			emp.setCount(count + 1);
			empId=count + 1;
			ofy().save().entities(user);
			ofy().save().entities(emp);
			
	       logger.log(Level.SEVERE, "END OF METHOD CREATE EMPLOYEE" );  
		   return empId+"- Employee created successfully";
		  
		
		
		}catch(Exception e){
			logger.log(Level.SEVERE, "INSIDE CATCH" );  
			return	e.getLocalizedMessage();
		}
		
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
