package com.slicktechnologies.server.android.master;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;

public class ServiceProductCreation extends HttpServlet {

	/**DESCRIPTION: created by AMOL Date 11-1-2020 
	 * This API is Used For "Create Service Product" IN PRIORA APP
	 * 
	 */
	private static final long serialVersionUID = 3606131762307221468L;
	Logger logger = Logger.getLogger("ServiceProductCreation.class");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doGet(req, resp);
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		logger.log(Level.SEVERE, "INSIDE SERVICE PRODUCT DOPOST");
		String data = req.getParameter("data");
		String screenData = data.trim().replace("#and", "&");
		String urlCalled = req.getRequestURL().toString().trim();
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		
		
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		logger.log(Level.SEVERE, "Stage One" + screenData);
		long companyId = 0l;
		if (comp != null) {
			companyId = comp.getCompanyId();
		} else {
			companyId = Long.parseLong("5348024557502464");
		}
		JSONObject object = null;
		try {
			object = new JSONObject(screenData.trim());
			logger.log(Level.SEVERE, "JSON OBJECT" + object);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String screenName = object.optString("screenName").trim();
		

		if (screenName.trim().equalsIgnoreCase("createServiceProduct")) {
           
			String actionTask = object.optString("actionTask");
			int prodId=0;
			
			if(actionTask.trim().equalsIgnoreCase("createServiceProduct")){
				
			String response = createServiceProduct(comp, object);
			logger.log(Level.SEVERE, "Inside create Service Product" );
			resp.getWriter().println(response + "");
			
			}else if(actionTask.trim().equalsIgnoreCase("updateServiceProduct")){
				logger.log(Level.SEVERE, "Inside updateServiceProduct" );
				double productPrice=0d;                                       
				prodId=Integer.parseInt(object.optString("productId"));
				String productName=object.optString("productName").trim();
				try{
				productPrice=Double.parseDouble(object.optString("productPrice").trim());
				}catch(Exception e){
				productPrice=0;	
				}
				String sacCode="";
				try{
				   sacCode=object.optString("SACCode").trim();
				}catch(Exception e){
					sacCode="";
				}
				double cgstPer=0;
				double sgstPer=0;
				try{
					cgstPer=Double.parseDouble(object.optString("CGST").trim())	;	
				}catch(Exception e){
					cgstPer=0;
				}
				try{
					 sgstPer=Double.parseDouble(object.optString("SGST").trim())	;
				}catch(Exception e){
					sgstPer=0;
				}
//				double cgstPer=Double.parseDouble(object.optString("CGST").trim())	;
//				double sgstPer=Double.parseDouble(object.optString("SGST").trim())	;
				boolean prodStatus=Boolean.parseBoolean(object.optString("productStatus").trim());
				
				ServiceProduct servProd=ofy().load().type(ServiceProduct.class).filter("companyId", companyId).filter("count", prodId).first().now();
				
				ServiceProduct servProduct=ofy().load().type(ServiceProduct.class).filter("companyId", companyId).filter("productName", productName).filter("count", prodId).first().now();		
				if(servProduct!=null){
					if(prodId!=servProduct.getCount()){
						resp.getWriter().println("Product "+object.optString("productName").trim()+" already exist!");
					    return;
					}
				}
				ArrayList<Double> taxPercentage=new ArrayList<Double>();
				taxPercentage.add(cgstPer);
				taxPercentage.add(sgstPer);
				
				List<TaxDetails> taxList=ofy().load().type(TaxDetails.class).filter("companyId", companyId).filter("taxChargePercent IN",taxPercentage).list();
				
				
				for(TaxDetails taxes:taxList){
					logger.log(Level.SEVERE, "inside taxlist loop");
					logger.log(Level.SEVERE, "Tax Charge Percent "+taxes.getTaxChargePercent());
					logger.log(Level.SEVERE, "Tax cgstPer "+cgstPer);
					logger.log(Level.SEVERE, "getTaxChargeName"+taxes.getTaxChargeName());
					if(taxes.getTaxChargePercent()==cgstPer&&taxes.getTaxPrintName().equalsIgnoreCase("CGST")){
						Tax tax=new Tax();
						
						tax.setTaxName(taxes.getTaxChargeName());
						tax.setTaxPrintName(taxes.getTaxPrintName());
						tax.setTaxConfigName(taxes.getTaxChargeName());
						tax.setPercentage(cgstPer);
						logger.log(Level.SEVERE, "CGST PERCENTAGE"+cgstPer);
						servProd.setVatTax(tax);
						continue;
					}else if(taxes.getTaxChargePercent()==sgstPer&&taxes.getTaxPrintName().equalsIgnoreCase("SGST")){
						Tax tax=new Tax();
						
						tax.setTaxName(taxes.getTaxChargeName());
						tax.setTaxPrintName(taxes.getTaxPrintName());
						tax.setTaxConfigName(taxes.getTaxChargeName());
						tax.setPercentage(sgstPer);
						logger.log(Level.SEVERE, "SGST PERCENTAGE"+sgstPer );
						servProd.setServiceTax(tax);
						continue;
					}else{
						break ;
					}
				}
				
				logger.log(Level.SEVERE, "Service product status"+prodStatus );
				servProd.setProductName(productName);
				if(productPrice!=0){
					servProd.setPrice(productPrice);	
				}else{
					servProd.setPrice(0);
				}
				if(!sacCode.equals("")){
					servProd.setHsnNumber(sacCode);
				}else{
					servProd.setHsnNumber("");
				}
				
				servProd.setStatus(prodStatus);
				
				ofy().save().entities(servProd);
				resp.getWriter().println("Product updated successfully!");
				logger.log(Level.SEVERE, "Product updated Successfully");
				
				
				
				
			}else if(actionTask.trim().equalsIgnoreCase("readServiceProduct")){
				logger.log(Level.SEVERE, "Inside readServiceProduct" );
				
				List<ServiceProduct> servProdList = ofy().load().type(ServiceProduct.class)
						.filter("companyId", comp.getCompanyId()).list();
				
				logger.log(Level.SEVERE, "Inside servProdList SIZE"+servProdList.size() );
				JSONObject mainObject = new JSONObject();
				JSONArray prodarrayJson = new JSONArray();
				
				try{
					Comparator<ServiceProduct> comparator=new Comparator<ServiceProduct>() {
						@Override
						public int compare(ServiceProduct e1, ServiceProduct e2) {
							return e1.getProductName().compareTo(e2.getProductName());
						}
					};
					Collections.sort(servProdList,comparator);
				
					for (ServiceProduct servprod : servProdList) {

						JSONObject jobj = new JSONObject();
						jobj.put("productId", servprod.getCount());
						jobj.put("productName", servprod.getProductName());
						jobj.put("productPrice", servprod.getPrice());
						jobj.put("SACCode", servprod.getHsnNumber());
						jobj.put("productStatus", servprod.isStatus());
						jobj.put("CGST",servprod.getVatTax().getPercentage());
						jobj.put("SGST",servprod.getServiceTax().getPercentage());
						
						prodarrayJson.put(jobj);
					}
				mainObject.put("productList", prodarrayJson);
				}catch(Exception e){
					e.printStackTrace();
				}
				String jsonString=mainObject.toString().replaceAll("\\\\", "");
				resp.getWriter().println(jsonString);
				logger.log(Level.SEVERE, "read Service products Successfully"+jsonString);
				
			}
		}
		
		
	}

	private String createServiceProduct(Company comp, JSONObject object) {
		logger.log(Level.SEVERE, "INSIDE CREATE SERVICE PRODUCT METHOD" );
		long companyId = 0l;
		if (comp != null) {
			companyId = comp.getCompanyId();
		} else {
			companyId = Long.parseLong("5348024557502464");
		}
		try{
		    int prodId=0;
		    int count=0;
			String productName=object.optString("productName").trim();
			double productPrice=0;
			try{
				productPrice=Double.parseDouble(object.optString("productPrice").trim());	
			}catch(Exception e){
				productPrice=0;
			}
			String sacCode="";
			try{
				sacCode=object.optString("SACCode").trim();
			}catch(Exception e){
				sacCode="";
			}
//			String cgst=object.optString("CGST").trim();
//			String sgst=object.optString("SGST").trim();
			logger.log(Level.SEVERE, "AFTER SGST " );
			boolean prodStatus=Boolean.parseBoolean(object.optString("productStatus").trim());
			
			ServiceProduct serProduct=new ServiceProduct();
			serProduct.setCompanyId(companyId);
			serProduct.setProductName(productName);
			
			if(productPrice!=0){
			serProduct.setPrice(productPrice);
			}else{
			serProduct.setPrice(0);	
			}
			if(!sacCode.equals("")){
			serProduct.setHsnNumber(sacCode);
			}else{
			serProduct.setHsnNumber("");	
			}
			
			serProduct.setStatus(prodStatus);
			
			double cgstPer=0;
			double sgstPer=0;
			
			try{
				cgstPer=Double.parseDouble(object.optString("CGST").trim())	;
				sgstPer=Double.parseDouble(object.optString("SGST").trim())	;
			}catch(Exception e){
				cgstPer=0;
				sgstPer=0;
			}
			
			logger.log(Level.SEVERE, "AFTER SAC CODE " );
			
			ServiceProduct servprd=null;
			servprd=ofy().load().type(ServiceProduct.class).filter("companyId", companyId).filter("productName", productName).first().now();
			
			if(servprd!=null){
				return "Product "+object.optString("productName").trim()+" already exist!";
			}
			
			
			NumberGeneration ng = new NumberGeneration();
			ng = ofy().load().type(NumberGeneration.class)
					.filter("companyId", comp.getCompanyId())
					.filter("processName", "ServiceProduct").filter("status", true)
					.first().now();
			logger.log(Level.SEVERE, "AFTER NG LOAD" );
			long number = ng.getNumber();
			count = (int) number;
			ng.setNumber(count + 1);
			ofy().save().entity(ng);
			logger.log(Level.SEVERE, "SAVE SAC CODE " );
			
			ArrayList<String>taxNameList=new ArrayList<String>();
			taxNameList.add("CGST@9");
			taxNameList.add("SGST@9");
			taxNameList.add("cgst@9");
			taxNameList.add("sgst@9");
			
			List<TaxDetails> taxList=ofy().load().type(TaxDetails.class).filter("companyId", companyId).filter("taxChargeStatus", true).list();
			logger.log(Level.SEVERE, "Tax Details List "+taxList.size());
			
			for(TaxDetails taxes:taxList){
				logger.log(Level.SEVERE, "inside taxlist loop");
				
				logger.log(Level.SEVERE, "TaxPercent "+taxes.getTaxChargePercent());
				logger.log(Level.SEVERE, "cgst "+cgstPer);
				logger.log(Level.SEVERE, "Tax Print Name"+taxes.getTaxChargeName());
				
				logger.log(Level.SEVERE, "inside taxlist loop");
				if(taxes.getTaxChargePercent()==cgstPer&&taxes.getTaxPrintName().equalsIgnoreCase("CGST")){
					logger.log(Level.SEVERE, "INSIDE IFF ");
					Tax tax=new Tax();
					
					tax.setTaxName(taxes.getTaxChargeName());
					tax.setTaxPrintName(taxes.getTaxPrintName());
					tax.setTaxConfigName(taxes.getTaxChargeName());
					tax.setPercentage(cgstPer);
					logger.log(Level.SEVERE, "CGST PERCENTAGE"+cgstPer);
					serProduct.setVatTax(tax);
					continue;
				}else if(taxes.getTaxChargePercent()==sgstPer&&taxes.getTaxPrintName().equalsIgnoreCase("SGST")){
					Tax tax=new Tax();
					
					tax.setTaxName(taxes.getTaxChargeName());
					tax.setTaxPrintName(taxes.getTaxPrintName());
					tax.setTaxConfigName(taxes.getTaxChargeName());
					tax.setPercentage(sgstPer);
					logger.log(Level.SEVERE, "SGST PERCENTAGE"+sgstPer );
					serProduct.setServiceTax(tax);
					continue;
				}else{
					break ;
				}
			}
			serProduct.setCount(count+1);
			serProduct.setProductCode(count+1+"");
			prodId=count+1;
			ofy().save().entities(serProduct);
			logger.log(Level.SEVERE, "PRODUCT CREATED SUCCESSFULLY" );
			return prodId+"- Product created successfully";
			
		}catch(Exception e){
			logger.log(Level.SEVERE,"INSIDE CATCH"+e);
			return e.getLocalizedMessage();
			
		}
		
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
