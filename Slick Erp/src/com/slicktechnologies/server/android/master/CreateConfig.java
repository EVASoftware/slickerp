package com.slicktechnologies.server.android.master;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;

public class CreateConfig extends HttpServlet{

	/**Date 4-2-2020 by Amol 
	 * This API Is Used To create Configs Group,Type,Category.
	 */
	private static final long serialVersionUID = 1764617688862142609L;
	Logger logger = Logger.getLogger("CreateConfig.class");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
//		super.doGet(req, resp);
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
	String data=req.getParameter("data");
		
		String screenData = data.trim().replace("#and", "&");
		String urlCalled = req.getRequestURL().toString().trim();
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		logger.log(Level.SEVERE, "Stage One" + screenData);
		
		long companyId = 0l;
		if (comp != null) {
			companyId = comp.getCompanyId();
		} else {
			companyId = Long.parseLong("5348024557502464");
		}
		JSONObject object = null;
		try {
			object = new JSONObject(screenData.trim());
			logger.log(Level.SEVERE, "JSON OBJECT" + object);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String configName = object.optString("configName").trim();
		String screenName= object.optString("screenName").trim();
		String actionTask= object.optString("actionTask").trim();
		
		if (configName.trim().equalsIgnoreCase("configGroup")) {
			  int count=0;
			if (screenName.trim().equalsIgnoreCase("Customer")) {
				int groupid=0;
				if (actionTask.trim().equalsIgnoreCase("create")) {

					boolean status = Boolean.parseBoolean(object.optString(
							"groupStatus").trim());
					String name = object.optString("groupName").trim();

					Config configValidate = ofy().load().type(Config.class)
							.filter("companyId", companyId).filter("name", name).filter("type", 29).first().now();

					if (configValidate != null) {
						resp.getWriter().println("Group "+object.optString("groupName").trim()+" already exist!");
					    return;
					}	
					
					NumberGeneration ng = new NumberGeneration();
					ng = ofy().load().type(NumberGeneration.class)
							.filter("companyId", comp.getCompanyId())
							.filter("processName", "Config")
							.filter("status", true).first().now();
					logger.log(Level.SEVERE, "AFTER NG LOAD");
					long number = ng.getNumber();
					count = (int) number;
					ng.setNumber(count + 1);
					ofy().save().entity(ng);

					Config config = new Config();
					config.setCompanyId(companyId);
					config.setCount(count + 1);
					config.setStatus(status);
					config.setName(name);
					config.setType(29);
					groupid = count + 1;
					ofy().save().entity(config);
					resp.getWriter().println(
							groupid + "- Group created successfully");

				} else if (actionTask.trim().equalsIgnoreCase("update")) {

				   count = Integer.parseInt(object.optString("count"));

					boolean status = Boolean.parseBoolean(object.optString(
							"groupStatus").trim());
					String name = object.optString("groupName").trim();

					Config config = ofy().load().type(Config.class)
							.filter("companyId", companyId)
							.filter("count", count).first().now();
					config.setStatus(status);
					config.setName(name);
					config.setType(29);
					ofy().save().entity(config);
					resp.getWriter().println("Group updated successfully");

				} else if (actionTask.trim().equalsIgnoreCase("read")) {

					List<Config> configList = ofy().load().type(Config.class)
							.filter("companyId", companyId).filter("type", 29).list();

					JSONObject mainObject = new JSONObject();
					JSONArray configarrayJson = new JSONArray();

					for (Config confi : configList) {

						JSONObject jobj = new JSONObject();
						
						try {
							jobj.put("count", confi.getCount());
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						try {
							jobj.put("groupName", confi.getName());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							jobj.put("status", confi.isStatus());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						configarrayJson.put(jobj);
					}

					try {
						mainObject.put("configGroupList", configarrayJson);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					String jsonString = mainObject.toString().replaceAll(
							"\\\\", "");
					logger.log(Level.SEVERE, "configList Data" + jsonString);
					resp.getWriter().println(jsonString);
				}

			} 
			else if (screenName.trim().equalsIgnoreCase("Lead")) {
				int groupid=0;
				if (actionTask.trim().equalsIgnoreCase("create")) {

					boolean status = Boolean.parseBoolean(object.optString(
							"groupStatus").trim());
					String name = object.optString("groupName").trim();

					Config configValidate = ofy().load().type(Config.class)
							.filter("companyId", companyId).filter("name", name).filter("type", 32).first().now();

					if (configValidate != null) {
						resp.getWriter().println("Group "+object.optString("groupName").trim()+" already exist!");
						 return;
					}
					
					NumberGeneration ng = new NumberGeneration();
					ng = ofy().load().type(NumberGeneration.class)
							.filter("companyId", comp.getCompanyId())
							.filter("processName", "Config").filter("status", true)
							.first().now();
					logger.log(Level.SEVERE, "AFTER NG LOAD" );
					long number = ng.getNumber();
					count = (int) number;
					ng.setNumber(count + 1);
					ofy().save().entity(ng);
					
					
					Config config = new Config();
					config.setCompanyId(companyId);
					config.setCount(count+1);
					config.setStatus(status);
					config.setName(name);
					config.setType(32);
					groupid=count+1;
					ofy().save().entity(config);
					resp.getWriter().println(groupid+"- Group created successfully");

				
				} else if (actionTask.trim().equalsIgnoreCase("update")) {

				   count = Integer.parseInt(object.optString("count"));

					boolean status = Boolean.parseBoolean(object.optString(
							"groupStatus").trim());
					String name = object.optString("groupName").trim();

					Config config = ofy().load().type(Config.class)
							.filter("companyId", companyId)
							.filter("count", count).first().now();
					config.setStatus(status);
					config.setName(name);
					config.setType(32);
					ofy().save().entity(config);
					resp.getWriter().println("Group updated successfully");

				} else if (actionTask.trim().equalsIgnoreCase("read")) {

					List<Config> configList = ofy().load().type(Config.class)
							.filter("companyId", companyId).filter("type", 32).list();

					JSONObject mainObject = new JSONObject();
					JSONArray configarrayJson = new JSONArray();

					for (Config confi : configList) {

						JSONObject jobj = new JSONObject();
						
						try {
							jobj.put("count", confi.getCount());
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						try {
							jobj.put("groupName", confi.getName());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							jobj.put("status", confi.isStatus());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						configarrayJson.put(jobj);
					}

					try {
						mainObject.put("configGroupList", configarrayJson);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					String jsonString = mainObject.toString().replaceAll(
							"\\\\", "");
					logger.log(Level.SEVERE, "configList Data" + jsonString);
					resp.getWriter().println(jsonString);
				}

			}else if(screenName.trim().equalsIgnoreCase("Contract")){
				int groupid=0;
				if (actionTask.trim().equalsIgnoreCase("create")) {

					boolean status = Boolean.parseBoolean(object.optString(
							"groupStatus").trim());
					String name = object.optString("groupName").trim();

					Config configValidate = ofy().load().type(Config.class)
							.filter("companyId", companyId).filter("name", name).filter("type", 31).first().now();

					if (configValidate != null) {
						resp.getWriter().println("Group "+object.optString("groupName").trim()+" already exist!");
						 return;
					}
					
					
					NumberGeneration ng = new NumberGeneration();
					ng = ofy().load().type(NumberGeneration.class)
							.filter("companyId", comp.getCompanyId())
							.filter("processName", "Config").filter("status", true)
							.first().now();
					logger.log(Level.SEVERE, "AFTER NG LOAD" );
					long number = ng.getNumber();
					count = (int) number;
					ng.setNumber(count + 1);
					ofy().save().entity(ng);
					
					
					Config config = new Config();
					config.setCompanyId(companyId);
					config.setCount(count+1);
					config.setStatus(status);
					config.setName(name);
					config.setType(31);
					groupid=count+1;
					ofy().save().entity(config);
					resp.getWriter().println(groupid+"-Group created successfully");

				
				} else if (actionTask.trim().equalsIgnoreCase("update")) {

				   count = Integer.parseInt(object.optString("count"));

					boolean status = Boolean.parseBoolean(object.optString(
							"groupStatus").trim());
					String name = object.optString("groupName").trim();

					Config config = ofy().load().type(Config.class)
							.filter("companyId", companyId)
							.filter("count", count).first().now();
					config.setStatus(status);
					config.setName(name);
					config.setType(31);
					ofy().save().entity(config);
					resp.getWriter().println("Group updated successfully");

				} else if (actionTask.trim().equalsIgnoreCase("read")) {

					List<Config> configList = ofy().load().type(Config.class)
							.filter("companyId", companyId).filter("type", 31).list();

					JSONObject mainObject = new JSONObject();
					JSONArray configarrayJson = new JSONArray();

					for (Config confi : configList) {

						JSONObject jobj = new JSONObject();
						
						try {
							jobj.put("count", confi.getCount());
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						try {
							jobj.put("groupName", confi.getName());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							jobj.put("status", confi.isStatus());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						configarrayJson.put(jobj);
					}

					try {
						mainObject.put("configGroupList", configarrayJson);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					String jsonString = mainObject.toString().replaceAll(
							"\\\\", "");
					logger.log(Level.SEVERE, "configList Data" + jsonString);
					resp.getWriter().println(jsonString);
				}

			}else if(screenName.trim().equalsIgnoreCase("Quotation")){
				int groupid=0;
				if (actionTask.trim().equalsIgnoreCase("create")) {

					boolean status = Boolean.parseBoolean(object.optString(
							"groupStatus").trim());
					String name = object.optString("groupName").trim();

					
					Config configValidate = ofy().load().type(Config.class)
							.filter("companyId", companyId).filter("name", name).filter("type", 30).first().now();

					if (configValidate != null) {
						resp.getWriter().println("Group "+object.optString("groupName").trim()+" already exist!");
						 return;
					}
					
					
					
					NumberGeneration ng = new NumberGeneration();
					ng = ofy().load().type(NumberGeneration.class)
							.filter("companyId", comp.getCompanyId())
							.filter("processName", "Config").filter("status", true)
							.first().now();
					logger.log(Level.SEVERE, "AFTER NG LOAD" );
					long number = ng.getNumber();
					count = (int) number;
					ng.setNumber(count + 1);
					ofy().save().entity(ng);
					
					
					Config config = new Config();
					config.setCompanyId(companyId);
					config.setCount(count+1);
					config.setStatus(status);
					config.setName(name);
					config.setType(30);
					groupid=count+1;
					ofy().save().entity(config);
					resp.getWriter().println(groupid+"-Group created successfully");

				
				} else if (actionTask.trim().equalsIgnoreCase("update")) {

				   count = Integer.parseInt(object.optString("count"));

					boolean status = Boolean.parseBoolean(object.optString(
							"groupStatus").trim());
					String name = object.optString("groupName").trim();

					Config config = ofy().load().type(Config.class)
							.filter("companyId", companyId)
							.filter("count", count).first().now();
					config.setStatus(status);
					config.setName(name);
					config.setType(30);
					ofy().save().entity(config);
					resp.getWriter().println("Group updated successfully");

				} else if (actionTask.trim().equalsIgnoreCase("read")) {

					List<Config> configList = ofy().load().type(Config.class)
							.filter("companyId", companyId).filter("type", 30).list();

					JSONObject mainObject = new JSONObject();
					JSONArray configarrayJson = new JSONArray();

					for (Config confi : configList) {

						JSONObject jobj = new JSONObject();
						
						try {
							jobj.put("count", confi.getCount());
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						try {
							jobj.put("groupName", confi.getName());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							jobj.put("status", confi.isStatus());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						configarrayJson.put(jobj);
					}

					try {
						mainObject.put("configGroupList", configarrayJson);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					String jsonString = mainObject.toString().replaceAll(
							"\\\\", "");
					logger.log(Level.SEVERE, "configList Data" + jsonString);
					resp.getWriter().println(jsonString);
				}

			}

		} 
		
		
		/*******************************CONFIG CATEGORY*******************************************/
		
		else if (configName.trim().equalsIgnoreCase("configCategory")) {
           int count=0;
			if (screenName.trim().equalsIgnoreCase("Customer")) {
				int catid=0;
				if (actionTask.trim().equalsIgnoreCase("create")) {
					boolean status = Boolean.parseBoolean(object.optString("categoryStatus").trim());
					String catName = object.optString("categoryName").trim();
					String catDescription="";
					try{
					catDescription = object.optString("categoryDescription").trim();
					}catch(Exception e){
					catDescription="";	
					}
					
					ConfigCategory configCategoryValidate = ofy().load().type(ConfigCategory.class)
							.filter("companyId", companyId).filter("categoryName", catName).filter("internalType", 1).first().now();

					if (configCategoryValidate != null) {
						resp.getWriter().println("Category "+object.optString("categoryName").trim()+" already exist!");
						 return;
					}
					
					
					
					
					
					ConfigCategory configCat=new ConfigCategory();
					
					NumberGeneration ng = new NumberGeneration();
					ng = ofy().load().type(NumberGeneration.class)
							.filter("companyId", comp.getCompanyId())
							.filter("processName", "ConfigCategory").filter("status", true)
							.first().now();
					logger.log(Level.SEVERE, "AFTER NG LOAD" );
					long number = ng.getNumber();
					count = (int) number;
					ng.setNumber(count + 1);
					ofy().save().entity(ng);
					String cc="CC";
					configCat.setCount(count + 1);
					configCat.setCategoryCode(cc.concat(count + 1+""));
					configCat.setCompanyId(companyId);
					configCat.setStatus(status);
					configCat.setCategoryName(catName);
					if(!catDescription.equals("")){
					configCat.setDescription(catDescription);
					}else{
					configCat.setDescription("");	
					}
					configCat.setInternalType(1);
					catid=count+1;
					ofy().save().entity(configCat);
					resp.getWriter().println(catid+"- Category created successfully");

				} else if (actionTask.trim().equalsIgnoreCase("update")) {
					count = Integer.parseInt(object.optString("count"));
					
					
					boolean status = Boolean.parseBoolean(object.optString("categoryStatus").trim());
					String catName = object.optString("categoryName").trim();
					String catDescription="";
					try{
						catDescription = object.optString("categoryDescription").trim();	
					}catch(Exception e){
						catDescription = "";
					}
					
					
					ConfigCategory configCat=ofy().load().type(ConfigCategory.class).filter("companyId", companyId).filter("count",count).first().now();	
//					configCat.setCompanyId(companyId);
					configCat.setStatus(status);
					configCat.setCategoryName(catName);
					configCat.setDescription(catDescription);
					
					
					ofy().save().entity(configCat);
					resp.getWriter().println("Category updated successfully");

				} else if (actionTask.trim().equalsIgnoreCase("read")) {

					List<ConfigCategory> configList = ofy().load().type(ConfigCategory.class)
							.filter("companyId", companyId).filter("internalType", 1).list();

					JSONObject mainObject = new JSONObject();
					JSONArray confiCategorygarrayJson = new JSONArray();

					for (ConfigCategory configCat : configList) {

						JSONObject jobj = new JSONObject();
						
						
						
						try {
							jobj.put("count", configCat.getCount());
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						try {
							jobj.put("categoryName", configCat.getCategoryName());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							jobj.put("categoryDescription", configCat.getDescription());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try{
						jobj.put("categoryStatus", configCat.getStatus());
						}catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						confiCategorygarrayJson.put(jobj);
					}

					try {
						mainObject.put("configCategoryList", confiCategorygarrayJson);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					String jsonString = mainObject.toString().replaceAll(
							"\\\\", "");
					logger.log(Level.SEVERE, "configCategoryList Data" + jsonString);
					resp.getWriter().println(jsonString);
				}
			} 
			else if (screenName.trim().equalsIgnoreCase("Lead")) {
				int catid=0;
				if (actionTask.trim().equalsIgnoreCase("create")) {
					boolean status = Boolean.parseBoolean(object.optString("categoryStatus").trim());
					String catName = object.optString("categoryName").trim();
					String catDescription="";
					try{
					catDescription = object.optString("categoryDescription").trim();
					}catch(Exception e){
					catDescription="";	
					}
					
					ConfigCategory configCategoryValidate = ofy().load().type(ConfigCategory.class)
							.filter("companyId", companyId).filter("categoryName", catName).filter("internalType",2).first().now();

					if (configCategoryValidate != null) {
						resp.getWriter().println("Category "+object.optString("categoryName").trim()+" already exist!");
						 return;
					}
					
					ConfigCategory configCat=new ConfigCategory();
					
					NumberGeneration ng = new NumberGeneration();
					ng = ofy().load().type(NumberGeneration.class)
							.filter("companyId", comp.getCompanyId())
							.filter("processName", "ConfigCategory").filter("status", true)
							.first().now();
					logger.log(Level.SEVERE, "AFTER NG LOAD" );
					long number = ng.getNumber();
					count = (int) number;
					ng.setNumber(count + 1);
					ofy().save().entity(ng);
					String cc="CC";
					configCat.setCount(count + 1);
					configCat.setCategoryCode(cc.concat(count + 1+""));
					configCat.setCompanyId(companyId);
					configCat.setStatus(status);
					configCat.setCategoryName(catName);
					if(!catDescription.equals("")){
					configCat.setDescription(catDescription);
					}else{
					configCat.setDescription("");	
					}
					configCat.setInternalType(2);
					catid=count+1;
					ofy().save().entity(configCat);
					resp.getWriter().println(catid+"- Category created successfully");

				} else if (actionTask.trim().equalsIgnoreCase("update")) {
					count = Integer.parseInt(object.optString("count"));
					
					
					boolean status = Boolean.parseBoolean(object.optString("categoryStatus").trim());
					String catName = object.optString("categoryName").trim();
					String catDescription = object.optString("categoryDescription").trim();
					
					ConfigCategory configCat=ofy().load().type(ConfigCategory.class).filter("companyId", companyId).filter("count",count).first().now();	
//					configCat.setCompanyId(companyId);
					configCat.setStatus(status);
					configCat.setCategoryName(catName);
					if(!catDescription.equals("")){
						configCat.setDescription(catDescription);
					}else{
						configCat.setDescription("");
					}
					
					
					
					ofy().save().entity(configCat);
					resp.getWriter().println("Category updated successfully");

				} else if (actionTask.trim().equalsIgnoreCase("read")) {

					List<ConfigCategory> configList = ofy().load().type(ConfigCategory.class)
							.filter("companyId", companyId).filter("internalType",2).list();

					JSONObject mainObject = new JSONObject();
					JSONArray confiCategorygarrayJson = new JSONArray();

					for (ConfigCategory configCat : configList) {

						JSONObject jobj = new JSONObject();
						
						
						
						try {
							jobj.put("count", configCat.getCount());
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						try {
							jobj.put("categoryName", configCat.getCategoryName());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							jobj.put("categoryDescription", configCat.getDescription());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try{
						jobj.put("categoryStatus", configCat.getStatus());
						}catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						confiCategorygarrayJson.put(jobj);
					}

					try {
						mainObject.put("configCategoryList", confiCategorygarrayJson);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					String jsonString = mainObject.toString().replaceAll(
							"\\\\", "");
					logger.log(Level.SEVERE, "configCategoryList Data" + jsonString);
					resp.getWriter().println(jsonString);
				}
			}else if(screenName.trim().equalsIgnoreCase("Contract")){
				int catid=0;
				if (actionTask.trim().equalsIgnoreCase("create")) {
					boolean status = Boolean.parseBoolean(object.optString("categoryStatus").trim());
					String catName = object.optString("categoryName").trim();
					String catDescription="";
					try{
					catDescription = object.optString("categoryDescription").trim();
					}catch(Exception e){
					catDescription="";	
					}
					ConfigCategory configCategoryValidate = ofy().load().type(ConfigCategory.class)
							.filter("companyId", companyId).filter("categoryName", catName).filter("internalType", 4).first().now();

					if (configCategoryValidate != null) {
						resp.getWriter().println("Category "+object.optString("categoryName").trim()+" already exist!");
						 return;
					}
					
					
					ConfigCategory configCat=new ConfigCategory();
					
					NumberGeneration ng = new NumberGeneration();
					ng = ofy().load().type(NumberGeneration.class)
							.filter("companyId", comp.getCompanyId())
							.filter("processName", "ConfigCategory").filter("status", true)
							.first().now();
					logger.log(Level.SEVERE, "AFTER NG LOAD" );
					long number = ng.getNumber();
					count = (int) number;
					ng.setNumber(count + 1);
					ofy().save().entity(ng);
					String cc="CC";
					configCat.setCount(count + 1);
					configCat.setCategoryCode(cc.concat(count + 1+""));
					configCat.setCompanyId(companyId);
					configCat.setStatus(status);
					configCat.setCategoryName(catName);
					if(!catDescription.equals("")){
					configCat.setDescription(catDescription);
					}else{
					configCat.setDescription("");	
					}
					configCat.setInternalType(4);
					catid=count+1;
					ofy().save().entity(configCat);
					resp.getWriter().println(catid+"- Category created successfully");

				} else if (actionTask.trim().equalsIgnoreCase("update")) {
					
					count = Integer.parseInt(object.optString("count"));
					boolean status = Boolean.parseBoolean(object.optString("categoryStatus").trim());
					String catName = object.optString("categoryName").trim();
					String catDescription="";
					try{
					catDescription = object.optString("categoryDescription").trim();
					}catch(Exception e){
					catDescription ="";
					}
					ConfigCategory configCat=ofy().load().type(ConfigCategory.class).filter("companyId", companyId).filter("count",count).first().now();	
//					configCat.setCompanyId(companyId);
					configCat.setStatus(status);
					configCat.setCategoryName(catName);
					if(!catDescription.equals("")){
					configCat.setDescription(catDescription);
					}else{
					configCat.setDescription("");	
					}
					ofy().save().entity(configCat);
					resp.getWriter().println("Category updated successfully");

				} else if (actionTask.trim().equalsIgnoreCase("read")) {

					List<ConfigCategory> configList = ofy().load().type(ConfigCategory.class)
							.filter("companyId", companyId).filter("internalType", 4).list();

					JSONObject mainObject = new JSONObject();
					JSONArray confiCategorygarrayJson = new JSONArray();

					for (ConfigCategory configCat : configList) {

						JSONObject jobj = new JSONObject();
						
						try {
							jobj.put("count", configCat.getCount());
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						try {
							jobj.put("categoryName", configCat.getCategoryName());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							jobj.put("categoryDescription", configCat.getDescription());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try{
						jobj.put("categoryStatus", configCat.getStatus());
						}catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						confiCategorygarrayJson.put(jobj);
					}

					try {
						mainObject.put("configCategoryList", confiCategorygarrayJson);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					String jsonString = mainObject.toString().replaceAll(
							"\\\\", "");
					logger.log(Level.SEVERE, "configCategoryList Data" + jsonString);
					resp.getWriter().println(jsonString);
				}
			}else if(screenName.trim().equalsIgnoreCase("Quotation")){
				int catid=0;
				if (actionTask.trim().equalsIgnoreCase("create")) {
					boolean status = Boolean.parseBoolean(object.optString("categoryStatus").trim());
					String catName = object.optString("categoryName").trim();
					String catDescription="";
					try{
					catDescription = object.optString("categoryDescription").trim();
					}catch(Exception e){
					catDescription="";	
					}
					
					ConfigCategory configCategoryValidate = ofy().load().type(ConfigCategory.class)
							.filter("companyId", companyId).filter("categoryName", catName).filter("internalType", 3).first().now();

					if (configCategoryValidate != null) {
						resp.getWriter().println("Category "+object.optString("categoryName").trim()+" already exist!");
						 return;
					}
					ConfigCategory configCat=new ConfigCategory();
					
					NumberGeneration ng = new NumberGeneration();
					ng = ofy().load().type(NumberGeneration.class)
							.filter("companyId", comp.getCompanyId())
							.filter("processName", "ConfigCategory").filter("status", true)
							.first().now();
					logger.log(Level.SEVERE, "AFTER NG LOAD" );
					long number = ng.getNumber();
					count = (int) number;
					ng.setNumber(count + 1);
					ofy().save().entity(ng);
					String cc="CC";
					configCat.setCount(count + 1);
					configCat.setCategoryCode(cc.concat(count + 1+""));
					configCat.setCompanyId(companyId);
					configCat.setStatus(status);
					configCat.setCategoryName(catName);
					if(!catDescription.equals("")){
					configCat.setDescription(catDescription);
					}else{
					configCat.setDescription("");	
					}
					configCat.setInternalType(3);
					catid=count+1;
					ofy().save().entity(configCat);
					resp.getWriter().println(catid+"- Category created successfully");

				} else if (actionTask.trim().equalsIgnoreCase("update")) {
					
					count = Integer.parseInt(object.optString("count"));
					boolean status = Boolean.parseBoolean(object.optString("categoryStatus").trim());
					String catName = object.optString("categoryName").trim();
					String catDescription ="";
					try{
						catDescription = object.optString("categoryDescription").trim();	
					}catch(Exception e){
						catDescription ="";
					}
					
					
					ConfigCategory configCat=ofy().load().type(ConfigCategory.class).filter("companyId", companyId).filter("count",count).first().now();	
//					configCat.setCompanyId(companyId);
					configCat.setStatus(status);
					configCat.setCategoryName(catName);
					if(!catDescription.equals("")){
					configCat.setDescription(catDescription);
					}else{
					configCat.setDescription("");	
					}
					
					
					ofy().save().entity(configCat);
					resp.getWriter().println("Category updated successfully");

				} else if (actionTask.trim().equalsIgnoreCase("read")) {

					List<ConfigCategory> configList = ofy().load().type(ConfigCategory.class)
							.filter("companyId", companyId).filter("internalType", 3).list();

					JSONObject mainObject = new JSONObject();
					JSONArray confiCategorygarrayJson = new JSONArray();

					for (ConfigCategory configCat : configList) {

						JSONObject jobj = new JSONObject();
						
						try {
							jobj.put("count", configCat.getCount());
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						try {
							jobj.put("categoryName", configCat.getCategoryName());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							jobj.put("categoryDescription", configCat.getDescription());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try{
						jobj.put("categoryStatus", configCat.getStatus());
						}catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						confiCategorygarrayJson.put(jobj);
					}

					try {
						mainObject.put("configCategoryList", confiCategorygarrayJson);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					String jsonString = mainObject.toString().replaceAll(
							"\\\\", "");
					logger.log(Level.SEVERE, "configCategoryList Data" + jsonString);
					resp.getWriter().println(jsonString);
				}
			}

		} 
		
      /*************************************CONFIG TYPE******************************************/		
		
		else if (configName.trim().equalsIgnoreCase("configType")) {
            int count=0;
           int typeId=0;
			if (screenName.trim().equalsIgnoreCase("Customer")) {
				if (actionTask.trim().equalsIgnoreCase("create")) {
					
					boolean status = Boolean.parseBoolean(object.optString(
							"typeStatus").trim());
					String typeName = object.optString("typeName").trim();
					String typeDescription="";
					try{
					typeDescription = object.optString("typeDescription").trim();
					}catch(Exception e){
					typeDescription="";
					}
					String typeCategory =object.optString("typeCategory").trim();
					
					Type typeValidate = ofy().load().type(Type.class)
							.filter("companyId", companyId).filter("typeName", typeName).filter("internalType", 1).first().now();

					if (typeValidate != null) {
						resp.getWriter().println("Type "+object.optString("typeName").trim()+" already exist!");
						 return;
					}
					
					
					
					
					Type type= new Type();
					NumberGeneration ng = new NumberGeneration();
					ng = ofy().load().type(NumberGeneration.class)
							.filter("companyId", comp.getCompanyId())
							.filter("processName", "Type").filter("status", true)
							.first().now();
					logger.log(Level.SEVERE, "AFTER NG LOAD" );
					long number = ng.getNumber();
					count = (int) number;
					ng.setNumber(count + 1);
					ofy().save().entity(ng);
					String cc="CC";
					type.setTypeCode(cc.concat(count + 1+""));
					type.setCompanyId(companyId);
					type.setCount(count + 1);
					
					type.setCategoryName(typeCategory);
					type.setStatus(status);
					type.setTypeName(typeName);
					
					if(!typeDescription.equals("")){
					type.setDescription(typeDescription);
					}else{
					type.setDescription("");	
					}
					type.setInternalType(1);
					typeId=count + 1;
					ofy().save().entity(type);
					resp.getWriter().println(typeId+"- Type created successfully");

				} else if (actionTask.trim().equalsIgnoreCase("update")) {
					
				count = Integer.parseInt(object.optString("count"));
				boolean status = Boolean.parseBoolean(object.optString(
						"typeStatus").trim());
				String typeName = object.optString("typeName").trim();
				String typeDescription="";
				try{
				typeDescription = object.optString("typeDescription").trim();	
				}catch(Exception e){
				typeDescription="";
				}
				String typeCategory =object.optString("typeCategory").trim();
				Type type=ofy().load().type(Type.class).filter("companyId", companyId).filter("count", count).first().now();
				
				type.setStatus(status);
				type.setTypeName(typeName);
				type.setCategoryName(typeCategory);
				if(!typeDescription.equals("")){
				type.setDescription(typeDescription);
				}else{
				type.setDescription("");	
				}
				ofy().save().entity(type);
				resp.getWriter().println("Type updated successfully");
				
				} else if (actionTask.trim().equalsIgnoreCase("read")) {

					List<Type> typeList = ofy().load().type(Type.class)
							.filter("companyId", companyId).filter("internalType", 1).list();

					JSONObject mainObject = new JSONObject();
					JSONArray typeListarrayJson = new JSONArray();
//					JSONObject jobj = null;
					for (Type configCat : typeList) {

						JSONObject jobj = new JSONObject();
						
						try {
							jobj.put("count", configCat.getCount());
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						try {
							jobj.put("typeCategory", configCat.getCategoryName());
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						try {
							jobj.put("typeName", configCat.getTypeName());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							jobj.put("typeDescription", configCat.getDescription());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try{
						jobj.put("typeStatus", configCat.getStatus());
						}catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						typeListarrayJson.put(jobj);
					}

					try {
						mainObject.put("typeList", typeListarrayJson);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					/**Date 15-2-2020 as per discuss with priyanka need a categoryname List**/
					List<ConfigCategory> configCatList = ofy().load().type(ConfigCategory.class)
							.filter("companyId", companyId).filter("internalType", 1).list();
					
					logger.log(Level.SEVERE, "configCategoryList in customer Type Data" + configCatList.size());
					JSONArray configCategoryListarrayJson = new JSONArray();
					
					for(ConfigCategory configCat:configCatList){
						JSONObject jobj1 = new JSONObject();
						try {
							jobj1.put("categoryName", configCat.getCategoryName());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						configCategoryListarrayJson.put(jobj1);
					}
					try {
						mainObject.put("categoryNameList", configCategoryListarrayJson);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					/**
					 * end 
					 */
					String jsonString = mainObject.toString().replaceAll(
							"\\\\", "");
					logger.log(Level.SEVERE, "TypeList Data" + jsonString);
					resp.getWriter().println(jsonString);
					
				}
			} 
			
	/*********************************LEAD CONFIG TYPE**************************************/		
			else if (screenName.trim().equalsIgnoreCase("Lead")) {
				if (actionTask.trim().equalsIgnoreCase("create")) {
					
					boolean status = Boolean.parseBoolean(object.optString(
							"typeStatus").trim());
					String typeName = object.optString("typeName").trim();
					String typeDescription ="";
					try{
					typeDescription = object.optString("typeDescription").trim();	
					}catch(Exception e){
					typeDescription ="";
					}
					String typeCategory =object.optString("typeCategory").trim();
					
					Type typeValidate = ofy().load().type(Type.class)
							.filter("companyId", companyId).filter("typeName", typeName).filter("internalType", 2).first().now();

					if (typeValidate != null) {
						resp.getWriter().println("Type "+object.optString("typeName").trim()+" already exist!");
						 return;
					}
					Type type= new Type();
					NumberGeneration ng = new NumberGeneration();
					ng = ofy().load().type(NumberGeneration.class)
							.filter("companyId", comp.getCompanyId())
							.filter("processName", "Type").filter("status", true)
							.first().now();
					logger.log(Level.SEVERE, "AFTER NG LOAD" );
					long number = ng.getNumber();
					count = (int) number;
					ng.setNumber(count + 1);
					ofy().save().entity(ng);
					
					type.setCompanyId(companyId);
					String cc="CC";
					type.setTypeCode(cc.concat(count + 1+""));
					type.setCount(count + 1);
					type.setCategoryName(typeCategory);
					type.setStatus(status);
					type.setTypeName(typeName);
					if(!typeDescription.equals("")){
						type.setDescription(typeDescription);	
					}else{
						type.setDescription("");	
					}
					type.setInternalType(2);
					typeId=count + 1;
					ofy().save().entity(type);
					resp.getWriter().println(typeId+"- Type created successfully");

				} else if (actionTask.trim().equalsIgnoreCase("update")) {
					
				count = Integer.parseInt(object.optString("count"));
				boolean status = Boolean.parseBoolean(object.optString(
						"typeStatus").trim());
				String typeName = object.optString("typeName").trim();
				String typeDescription ="";
				try{
				typeDescription = object.optString("typeDescription").trim();	
				}catch(Exception e){
				typeDescription ="";
				}	
				String typeCategory =object.optString("typeCategory").trim();
				Type type=ofy().load().type(Type.class).filter("companyId", companyId).filter("count", count).first().now();
				
				type.setStatus(status);
				type.setTypeName(typeName);
				if(!typeDescription.equals("")){
					type.setDescription(typeDescription);	
				}else{
					type.setDescription("");	
				}
				type.setCategoryName(typeCategory);
				ofy().save().entity(type);
				resp.getWriter().println("Type updated successfully");
				
				} else if (actionTask.trim().equalsIgnoreCase("read")) {

					List<Type> typeList = ofy().load().type(Type.class)
							.filter("companyId", companyId).filter("internalType", 2).list();

					JSONObject mainObject = new JSONObject();
					JSONArray typeListarrayJson = new JSONArray();

					for (Type configCat : typeList) {

						JSONObject jobj = new JSONObject();
						
						try {
							jobj.put("count", configCat.getCount());
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						try {
							jobj.put("typeCategory", configCat.getCategoryName());
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						
						
						try {
							jobj.put("typeName", configCat.getTypeName());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							jobj.put("typeDescription", configCat.getDescription());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try{
						jobj.put("typeStatus", configCat.getStatus());
						}catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						typeListarrayJson.put(jobj);
					}

					try {
						mainObject.put("typeList", typeListarrayJson);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					List<ConfigCategory> configCatList = ofy().load().type(ConfigCategory.class)
							.filter("companyId", companyId).filter("internalType", 2).list();
					
					logger.log(Level.SEVERE, "configCategoryList in customer Type Data" + configCatList.size());
					JSONArray configCategoryListarrayJson = new JSONArray();
					
					for(ConfigCategory configCat:configCatList){
						JSONObject jobj1 = new JSONObject();
						try {
							jobj1.put("categoryName", configCat.getCategoryName());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						configCategoryListarrayJson.put(jobj1);
					}
					try {
						mainObject.put("categoryNameList", configCategoryListarrayJson);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					String jsonString = mainObject.toString().replaceAll(
							"\\\\", "");
					logger.log(Level.SEVERE, "TypeList Data" + jsonString);
					resp.getWriter().println(jsonString);
					
				}
			}
			
			
	/*********************************CONTRACT CONFIG TYPE**************************************/	
			
			else if(screenName.trim().equalsIgnoreCase("Contract")){
				if (actionTask.trim().equalsIgnoreCase("create")) {
					
					boolean status = Boolean.parseBoolean(object.optString(
							"typeStatus").trim());
					String typeName = object.optString("typeName").trim();
					String typeDescription ="";
					try{
					typeDescription = object.optString("typeDescription").trim();	
					}catch(Exception e){
					typeDescription ="";
					}
					String typeCategory =object.optString("typeCategory").trim();
					
					Type typeValidate = ofy().load().type(Type.class)
							.filter("companyId", companyId).filter("typeName", typeName).filter("internalType", 4).first().now();

					if (typeValidate != null) {
						resp.getWriter().println("Type "+object.optString("typeName").trim()+" already exist!");
						 return;
					}
					
					Type type= new Type();
					NumberGeneration ng = new NumberGeneration();
					ng = ofy().load().type(NumberGeneration.class)
							.filter("companyId", comp.getCompanyId())
							.filter("processName", "Type").filter("status", true)
							.first().now();
					logger.log(Level.SEVERE, "AFTER NG LOAD" );
					long number = ng.getNumber();
					count = (int) number;
					ng.setNumber(count + 1);
					ofy().save().entity(ng);
					
					type.setCompanyId(companyId);
					type.setCount(count + 1);
					String cc="CC";
					type.setTypeCode(cc.concat(count + 1+""));
					type.setCategoryName(typeCategory);
					type.setStatus(status);
					type.setTypeName(typeName);
					
					if(!typeDescription.equals("")){
						type.setDescription(typeDescription);	
					}else{
						type.setDescription("");	
					}
					type.setInternalType(4);
					typeId=count + 1;
					ofy().save().entity(type);
					resp.getWriter().println(typeId+"- Type created successfully");

				} else if (actionTask.trim().equalsIgnoreCase("update")) {
					
				count = Integer.parseInt(object.optString("count"));
				boolean status = Boolean.parseBoolean(object.optString(
						"typeStatus").trim());
				String typeName = object.optString("typeName").trim();
				String typeDescription ="";
				try{
				typeDescription = object.optString("typeDescription").trim();	
				}catch(Exception e){
				typeDescription ="";
				}
				String typeCategory =object.optString("typeCategory").trim();
				Type type=ofy().load().type(Type.class).filter("companyId", companyId).filter("count", count).first().now();
				
				type.setStatus(status);
				type.setTypeName(typeName);
				if(!typeDescription.equals("")){
					type.setDescription(typeDescription);	
				}else{
					type.setDescription("");	
				}
				type.setCategoryName(typeCategory);
				ofy().save().entity(type);
				resp.getWriter().println("Type updated successfully");
				
				} else if (actionTask.trim().equalsIgnoreCase("read")) {

					List<Type> typeList = ofy().load().type(Type.class)
							.filter("companyId", companyId).filter("internalType", 4).list();

					JSONObject mainObject = new JSONObject();
					JSONArray typeListarrayJson = new JSONArray();

					for (Type configCat : typeList) {

						JSONObject jobj = new JSONObject();
						
						try {
							jobj.put("count", configCat.getCount());
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						try {
							jobj.put("typeCategory", configCat.getCategoryName());
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						
						
						try {
							jobj.put("typeName", configCat.getTypeName());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							jobj.put("typeDescription", configCat.getDescription());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try{
						jobj.put("typeStatus", configCat.getStatus());
						}catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						typeListarrayJson.put(jobj);
					}

					try {
						mainObject.put("typeList", typeListarrayJson);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					List<ConfigCategory> configCatList = ofy().load().type(ConfigCategory.class)
							.filter("companyId", companyId).filter("internalType", 4).list();
					
					logger.log(Level.SEVERE, "configCategoryList in customer Type Data" + configCatList.size());
					JSONArray configCategoryListarrayJson = new JSONArray();
					
					for(ConfigCategory configCat:configCatList){
						JSONObject jobj1 = new JSONObject();
						try {
							jobj1.put("categoryName", configCat.getCategoryName());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						configCategoryListarrayJson.put(jobj1);
					}
					try {
						mainObject.put("categoryNameList", configCategoryListarrayJson);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					String jsonString = mainObject.toString().replaceAll(
							"\\\\", "");
					logger.log(Level.SEVERE, "TypeList Data" + jsonString);
					resp.getWriter().println(jsonString);
					
				}
			}
			
			/*********************************QUTATION CONFIG TYPE**************************************/
			
			else if(screenName.trim().equalsIgnoreCase("Quotation")){
				if (actionTask.trim().equalsIgnoreCase("create")) {
					
					boolean status = Boolean.parseBoolean(object.optString(
							"typeStatus").trim());
					String typeName = object.optString("typeName").trim();
					
					
					
					String typeDescription="";
					try{
						typeDescription = object.optString("typeDescription").trim();	
					}catch(Exception e){
						typeDescription ="";
					}
					
					String typeCategory =object.optString("typeCategory").trim();
					
					Type typeValidate = ofy().load().type(Type.class)
							.filter("companyId", companyId).filter("typeName", typeName).filter("internalType", 3).first().now();

					if (typeValidate != null) {
						resp.getWriter().println("Type "+object.optString("typeName").trim()+" already exist!");
						 return;
					}
					Type type= new Type();
					NumberGeneration ng = new NumberGeneration();
					ng = ofy().load().type(NumberGeneration.class)
							.filter("companyId", comp.getCompanyId())
							.filter("processName", "Type").filter("status", true)
							.first().now();
					logger.log(Level.SEVERE, "AFTER NG LOAD" );
					long number = ng.getNumber();
					count = (int) number;
					ng.setNumber(count + 1);
					ofy().save().entity(ng);
					
					type.setCompanyId(companyId);
					type.setCount(count + 1);
					String cc="CC";
					type.setTypeCode(cc.concat(count + 1+""));
					type.setCategoryName(typeCategory);
					type.setStatus(status);
					type.setTypeName(typeName);
					if(!typeDescription.equals("")){
					type.setDescription(typeDescription);
					}else{
					type.setDescription("");	
					}
					
					type.setInternalType(3);
					typeId=count + 1;
					ofy().save().entity(type);
					resp.getWriter().println(typeId+"- Type created successfully");

				} else if (actionTask.trim().equalsIgnoreCase("update")) {
					
				count = Integer.parseInt(object.optString("count"));
				boolean status = Boolean.parseBoolean(object.optString(
						"typeStatus").trim());
				String typeName = object.optString("typeName").trim();
				
				String typeDescription="";
				try{
				typeDescription = object.optString("typeDescription").trim();	
				}catch(Exception e){
				typeDescription="";
				}
				String typeCategory =object.optString("typeCategory").trim();
				Type type=ofy().load().type(Type.class).filter("companyId", companyId).filter("count", count).first().now();
				
				type.setStatus(status);
				type.setTypeName(typeName);
				if(!typeDescription.equals("")){
				type.setDescription(typeDescription);
				}else{
				type.setDescription("");	
				}
				type.setCategoryName(typeCategory);
				ofy().save().entity(type);
				resp.getWriter().println("Type updated successfully");
				
				} else if (actionTask.trim().equalsIgnoreCase("read")) {

					List<Type> typeList = ofy().load().type(Type.class)
							.filter("companyId", companyId).filter("internalType", 3).list();

					JSONObject mainObject = new JSONObject();
					JSONArray typeListarrayJson = new JSONArray();

					for (Type configCat : typeList) {

						JSONObject jobj = new JSONObject();
						
						try {
							jobj.put("count", configCat.getCount());
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						try {
							jobj.put("typeCategory", configCat.getCategoryName());
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						
						
						try {
							jobj.put("typeName", configCat.getTypeName());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							jobj.put("typeDescription", configCat.getDescription());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try{
						jobj.put("typeStatus", configCat.getStatus());
						}catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						typeListarrayJson.put(jobj);
					}

					try {
						mainObject.put("typeList", typeListarrayJson);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					List<ConfigCategory> configCatList = ofy().load().type(ConfigCategory.class)
							.filter("companyId", companyId).filter("internalType", 3).list();
					
					logger.log(Level.SEVERE, "configCategoryList in customer Type Data" + configCatList.size());
					JSONArray configCategoryListarrayJson = new JSONArray();
					
					for(ConfigCategory configCat:configCatList){
						JSONObject jobj1 = new JSONObject();
						try {
							jobj1.put("categoryName", configCat.getCategoryName());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						configCategoryListarrayJson.put(jobj1);
					}
					try {
						mainObject.put("categoryNameList", configCategoryListarrayJson);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					String jsonString = mainObject.toString().replaceAll(
							"\\\\", "");
					logger.log(Level.SEVERE, "TypeList Data" + jsonString);
					resp.getWriter().println(jsonString);
					
				}
			}

		}
		
		
		
		
	}

}
