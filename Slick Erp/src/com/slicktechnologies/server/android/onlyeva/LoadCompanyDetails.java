package com.slicktechnologies.server.android.onlyeva;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class LoadCompanyDetails extends HttpServlet{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2351328415295783734L;
	/**
	 * 
	 */
	Logger logger = Logger.getLogger("LoadCompanyDetails.class");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doGet(req, resp);
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		resp.setContentType("text/plain");
		RegisterServiceImpl impl=new RegisterServiceImpl();
		impl.getClass();
		String urlCalled=req.getRequestURL().toString().trim();
		String url=urlCalled.replace("http://", "");
		String[] splitUrl=url.split("\\.");
		Company comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();	
		long companyId=comp.getCompanyId();
		String companyCode=req.getParameter("companyCode").trim();
		
		Customer customer=ofy().load().type(Customer.class).filter("companyId", companyId).filter("refrNumber1", companyCode).first().now();
		
		JSONObject jObj=new JSONObject();
		jObj.put("count", customer.getCount()+"");
		jObj.put("companyName", customer.getCompanyName()+"");
		jObj.put("link", customer.getDescription());
		jObj.put("email", customer.getEmail());
		jObj.put("number", customer.getLandline()+"/"+customer.getCellNumber1());
		jObj.put("address", customer.getAdress().getCompleteAddress());
		String jsonString = jObj.toJSONString().replace("\\\\", "");
		
		logger.log(Level.SEVERE, jsonString);
		resp.getWriter().println(jsonString);
		
	}
	
	

}
