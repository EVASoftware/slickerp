package com.slicktechnologies.server.android.onlyeva;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.logging.Level;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.android.RegisterDevice;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.CustomerUser;

public class NotificationHandler extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5249546552622709556L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		sendPushNotification(companyId, applicationName, userName, response, service, comp2);
	}
	
	
	private void sendPushNotification(long companyId, String applicationName,
			String userName, String response, Service service, Company comp2) {
		response="event:Mark Completed"+"$"+service.getProductName()+" service has been completed by "+service.getEmployee()+". Would you like to rate?";//getJSONResponse(service);
		CustomerUser customerUser=ofy().load().type(CustomerUser.class).filter("companyId",companyId).filter("cinfo.fullName",service.getPersonInfo().getFullName()).first().now(); 
		RegisterDevice device = ofy().load().type(RegisterDevice.class)
				.filter("companyId", companyId)
				.filter("applicationName", applicationName)
				.filter("userName", customerUser.getUserName()).first().now();
		String deviceName = device.getRegId().trim();
//		logger.log(Level.SEVERE,"deviceName"+deviceName);
		URL url = null;
		try {
			url = new URL("https://fcm.googleapis.com/fcm/send");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HttpURLConnection conn = null;
		try {
			conn = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		conn.setDoOutput(true);
		try {
			conn.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		conn.setRequestProperty("Content-Type",
				"application/json; charset=UTF-8");
		conn.setRequestProperty("Authorization", "key="
				+ comp2.getFcmServerKeyForKreto().trim());
		conn.setDoOutput(true);
		conn.setDoInput(true);

		try {
			conn.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
//			logger.log(Level.SEVERE, "ProtocolException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		OutputStream os = null;
		try {
			os = conn.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
//			logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
		// Log.d("Test", "From Get Post Method" + getPostData(values));
		try {
//			writer.write(createJSONData(response, deviceName));
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			logger.log(Level.SEVERE, "IOException2 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		try {
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
//			logger.log(Level.SEVERE, "IOException3 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		InputStream is = null;
		try {
			is = conn.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
//			logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String temp;
		String data = null;
		try {
			while ((temp = br.readLine()) != null) {
				data = data + temp;
			}
//			logger.log(Level.SEVERE, "data data::::::::::" + data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
//			logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
			e.printStackTrace();
		}

//		logger.log(Level.SEVERE, "Data::::::::::" + data);
	}

}
