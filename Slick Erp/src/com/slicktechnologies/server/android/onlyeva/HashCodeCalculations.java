package com.slicktechnologies.server.android.onlyeva;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;


public class HashCodeCalculations extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3133942738129002482L;
	/**
	 * 
	 */
	Logger logger = Logger.getLogger("HashCodeCalculations.class");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doGet(req, resp);
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doPost(req, resp);
//		key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|salt;
		String hashSequenceReq =req.getParameter("hashSequence").trim();
		logger.log(Level.SEVERE,"hashSequenceReq"+hashSequenceReq);
		String hashSequence=hashSequenceReq.trim().toLowerCase();
		logger.log(Level.SEVERE,"hashSequence"+hashSequence);
		String serverCalculatedHash= hashCal("SHA-512", hashSequence);
//		String serverCalculatedHash= hashCal("sha512", hashSequence);
		
		logger.log(Level.SEVERE,"serverCalculatedHash::::::::::"+serverCalculatedHash);
		JSONObject jsonObj=new JSONObject();
		jsonObj.put("status", "1");
		jsonObj.put("payment_hash", serverCalculatedHash);
		String jsonString=jsonObj.toJSONString().replace("\\\\", "");
		logger.log(Level.SEVERE,"jsonString::::"+jsonString);
		resp.getWriter().print(jsonString);
	}
	

	public static String hashCal(String type, String str) {
		byte[] hashseq = str.getBytes();
		StringBuffer hexString = new StringBuffer(); 
		try
		{
		MessageDigest algorithm = MessageDigest.getInstance(type);
		algorithm.reset();
		algorithm.update(hashseq);
		byte messageDigest[] = algorithm.digest(); 
		for(int i = 0; i<messageDigest.length; i++) {
		String hex = Integer.toHexString(0xFF &messageDigest[i]); 
		if(hex.length() == 1) {
		hexString.append("0");
		}
		hexString.append(hex);
		}
		} catch (NoSuchAlgorithmException nsae) {
			
		}
		return hexString.toString();
		}

	
	private String getSHA(String str) {

		 MessageDigest md;
		String out = "";
		try {
			md = MessageDigest.getInstance("SHA-512");
			md.update(str.getBytes());
			byte[] mb = md.digest();

			for (int i = 0; i < mb.length; i++) {
				byte temp = mb[i];
				String s = Integer.toHexString(new Byte(temp));
				while (s.length() < 2) {
					s = "0" + s;
				}
				s = s.substring(s.length() - 2);
				out += s;
			}

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return out; 

	}

}
