package com.slicktechnologies.server.android;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.simple.JSONObject;

import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.device.TrackTableDetails;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.ServiceImplementor;
import com.slicktechnologies.server.SingletoneNumberGeneration;
import com.slicktechnologies.server.android.technicianapp.TechnicianAppOperation;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
//import com.slicktechnologies.shared.CheckList;
import com.slicktechnologies.shared.FumigationServiceReport;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.ModificationHistory;
import com.slicktechnologies.shared.common.android.RegisterDevice;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.CustomerUser;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
/**
 * Description :  This API is used to mark service individually as �Reported� and �TCompleted�.
 * @author pc1
 *
 */
public class ReportedServiceDetailsSaveServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2558449433504212668L;
	/**
	 * 
	 */
	
	
	Logger logger = Logger.getLogger("ReportedServiceDetailsSaveServlet.class");
	Company comp;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doGet(req, resp);
		doPost(req, resp);
	}

	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Enumeration<String> parameterNames = req.getParameterNames();
		logger.log(Level.SEVERE,"parameterNames ::::::::"+parameterNames);
		// TODO Auto-generated method stub
		String urlCalled=req.getRequestURL().toString().trim();
//		String url=urlCalled.replace("http://", "");
//		String[] splitUrl=url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		/**
		 * Updated By: Viraj
		 * Date: 02-03-2019
		 * Description: To store latitude and longitude
		 */
		String lat = null;
		String lon = null;
		/** Ends **/
		/**
		 * Updated By: komal
		 * Date: 01-04-2019
		 * Description: To get and save location 
		 */
		String location = "";
		double km = 0;
		Exception exep = null;
		ArrayList<Service> wmsFumigationservicelist = new ArrayList<Service>();

		try {

			
			comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();	
			
			/**
			 * @author Anil @since 14-06-2021
			 * Thai character was not getting download in proper format
			 * raised by Rahul Tiwari
			 */
//			resp.setContentType("text/plain");
			resp.setContentType("text/plain; charset=UTF-8");
			resp.setCharacterEncoding("UTF-8");
		
		String companyId=comp.getCompanyId()+"";
		String status=req.getParameter("status").trim();
		String serviceId=req.getParameter("serviceId").trim();
		boolean syncFlag=Boolean.parseBoolean(req.getParameter("syncFlag"));
		String rescheduleData="";
		if(syncFlag){
			rescheduleData=req.getParameter("rescheduledData").trim();
		}
		logger.log(Level.SEVERE,"companyId::::::::"+companyId);
		logger.log(Level.SEVERE,"status:::::::::::"+status);
		logger.log(Level.SEVERE,"serviceId::::::::"+serviceId);
		Service service=ofy().load().type(Service.class).filter("companyId", Long.parseLong(companyId)).filter("count",Long.parseLong(serviceId)).first().now();
		logger.log(Level.SEVERE,"service::::::::"+service+" technician="+service.getEmployee());
		
		/*
		 * Ashwini Patil 
		 * Date:23-02-2024
		 * Pest cure, Rex and Pecopp reported km travelled issue
		 * Found that system shows wrong kilometer only when there is reported and tcompleted called multiple times
		 * Since finding issue in pedio will take time
		 * managing this flow from ERP
		 * If service is already reported or tcompleted then duplicate entry will not be created
		 */
		boolean validCallFlag=true;
		if(service!=null) {
			ArrayList<TrackTableDetails> trackDetails= service.getTrackServiceTabledetails();
			if(trackDetails!=null) {
				for(TrackTableDetails detail:trackDetails) {
					if(detail.getStatus().equals(status)) {
						validCallFlag=false;
						break;
					}
				}
			}
		}
		
		logger.log(Level.SEVERE,"validCallFlag == "+validCallFlag);
		
		if(validCallFlag) {

			/**
			 * @author Vijay Chougule Date 01-10-2020
			 * Des :- Sevice started reported tcompleted and completed Time issue in other country so now mapping timing from app not
			 * from the Date
			 */
			String serviceActionTime = "";
			if(req.getParameter("serviceTime")!=null) {
				serviceActionTime = req.getParameter("serviceTime");
			}
			logger.log(Level.SEVERE,"time == "+serviceActionTime);
			
			SimpleDateFormat format = new SimpleDateFormat("HH:mm");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			format.setTimeZone(TimeZone.getTimeZone("IST"));
			
			SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
			timeFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			
			/**
			 * @author Anil @since 02-03-2021
			 * New formatter is added to send correct date in sms which is getting sent to customer at the time of starting/reporting of services
			 */
			SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			
			/**
			 * @author Vijay Date :- 24-10-2020
			 * Des :- As per nitin sir instruction started reported tcompleted and Completed must store in Date 
			 * format with date and time 
			 */
			String strActionDateTime = "";
			Date actionDateTime = null;
			try {
				strActionDateTime = req.getParameter("serviceActionDateTime");
				actionDateTime = sdf.parse(strActionDateTime);
			} catch (Exception e) {
				// TODO: handle exception
			}
			logger.log(Level.SEVERE,"str actionDateTime "+strActionDateTime);
			logger.log(Level.SEVERE,"actionDateTime "+actionDateTime);
			/**
			 * ends here
			 */
			
//			Service service=ofy().load().type(Service.class).filter("companyId", Long.parseLong(companyId)).filter("count",Long.parseLong(serviceId)).first().now();
//			logger.log(Level.SEVERE,"service::::::::"+service);
			
			/**
			 * @author Vijay Date :- 23-10-2020
			 * Des :- if service is completed then it should not update for started reported again added checker here
			 */
			boolean valdateflag = true;
			if(service.getStatus().trim().equalsIgnoreCase(Service.SERVICESTATUSCOMPLETED) && 
					(status.trim().equalsIgnoreCase(Service.SERVICESTATUSREPORTED) || status.trim().equalsIgnoreCase(Service.SERVICESTATUSSTARTED) 
							|| status.trim().equalsIgnoreCase(Service.SERVICETCOMPLETED))){
				valdateflag = false;
				resp.getWriter().print("Service status is completed in ERP System so you can not mark as Started or Reported or Tcompleted");
			}
			logger.log(Level.SEVERE,"valdateflag"+valdateflag);
			if(valdateflag){
				
			if(status.trim().equalsIgnoreCase(Service.SERVICESTATUSREPORTED)){
				lat=req.getParameter("lat").trim();
				lon=req.getParameter("lon").trim();
				/**
				 * Updated By: komal
				 * Date: 01-04-2019
				 * Description: To get and save location
				 */
				try{
					location = req.getParameter("location").trim();
				}catch(Exception e){
					location = "";
					e.printStackTrace();
				}
				try{
					km = Double.parseDouble(req.getParameter("km"));
				}catch(Exception e){
					e.printStackTrace();
				}
				service.setReportLatnLong(lat+"$"+lon);
				service.setFromTime(format.format(new Date())+"");
				service.setStatus(Service.SERVICESTATUSREPORTED);
				if(strActionDateTime!=null && !strActionDateTime.equals("")){
					service.setReportedDate_time(strActionDateTime);
				}
				else{
					service.setReportedDate_time(sdf.format(new Date())+"");
				}
//				service.setReportedDate_time(sdf.format(new Date())+"");
				service.setReportedLatitude(lat);
				service.setReportedLongitude(lon);
				if(!serviceActionTime.equals("")){
					service.setReportedTime(serviceActionTime);
				}
				else{
					service.setReportedTime(timeFormat.format(new Date()));
				}

				service.setReportedLocationName(location);
				if(actionDateTime!=null){
					service.setReportedDate(actionDateTime);
				}
				
				/**
				 * @author Vijay Date :- 09-07-2022
				 * Des :- adding KM travelled
				 */
				try {
					service.setKmTravelled(km);

				} catch (Exception e) {
				}
				/**
				 * ends here
				 */
				
			}else if(status.trim().equalsIgnoreCase(Service.SERVICESTATUSSTARTED)){
				lat=req.getParameter("lat").trim();
				lon=req.getParameter("lon").trim();
				/**
				 * Updated By: komal
				 * Date: 01-04-2019
				 * Description: To get and save location
				 */
				try{
					location = req.getParameter("location").trim();
				}catch(Exception e){
					location = "";
					e.printStackTrace();
				}
				service.setStartLatnLong(lat+"$"+lon);
				
				service.setStatus(Service.SERVICESTATUSSTARTED);
				if(strActionDateTime!=null && !strActionDateTime.equals("")){
					service.setStartedDate_time(strActionDateTime);
				}
				else{
					service.setStartedDate_time(sdf.format(new Date())+"");
				}
//				service.setStartedDate_time(sdf.format(new Date())+"");
				service.setStartedLatitude(lat);
				service.setStartedLongitude(lon);
				if(!serviceActionTime.equals("")){
					service.setStartedTime(serviceActionTime);
				}
				else{
					service.setStartedTime(timeFormat.format(new Date()));
				}
				
				service.setStartedLocationName(location);
				if(actionDateTime!=null){
					service.setStartedDate(actionDateTime);
				}
				
			}else if(status.trim().equalsIgnoreCase(Service.SERVICETCOMPLETED)){
				lat=req.getParameter("lat").trim();
				lon=req.getParameter("lon").trim();
				/**
				 * Updated By: komal
				 * Date: 01-04-2019
				 * Description: To get and save location
				 */
				try{
					location = req.getParameter("location").trim();
				}catch(Exception e){
					location = "";
					e.printStackTrace();
				}
				service.setStartLatnLong(lat+"$"+lon);
				
				service.setStatus(Service.SERVICETCOMPLETED);
				
				if(strActionDateTime!=null && !strActionDateTime.equals("")){
					service.settCompletedDate_time(strActionDateTime);
				}
				else{
					service.settCompletedDate_time(sdf.format(new Date())+"");
				}
//				service.settCompletedDate_time(sdf.format(new Date())+"");
				service.settCompletedLatitude(lat);
				service.settCompletedLongitude(lon);
				if(!serviceActionTime.equals("")){
					service.settCompletedTime(serviceActionTime);
				}
				else{
					service.settCompletedTime(timeFormat.format(new Date()));
				}
				
				
				service.settCompletedLocationName(location);
				
				if(actionDateTime!=null){
					service.setTcompletedDate(actionDateTime);
				}
				
			}
			
			logger.log(Level.SEVERE,"Stage::::::::1");
			TrackTableDetails tracktabledetails=new TrackTableDetails();
			logger.log(Level.SEVERE,"Stage::::::::2");
			
			
			
			if(strActionDateTime!=null && !strActionDateTime.equals("")){
				tracktabledetails.setDate_time(strActionDateTime);
			}
			else{
				tracktabledetails.setDate_time(sdf.format(new Date())+"");
			}
//			tracktabledetails.setDate_time(sdf.format(new Date())+"");
			
			logger.log(Level.SEVERE,"Stage::::::::3");
			if(status.trim().equalsIgnoreCase(Service.SERVICESTATUSREPORTED)){
			tracktabledetails.setStatus(Service.SERVICESTATUSREPORTED);
			}else if(status.trim().equalsIgnoreCase(Service.SERVICESTATUSSTARTED)){
			tracktabledetails.setStatus(Service.SERVICESTATUSSTARTED);
			}else if(status.trim().equalsIgnoreCase(Service.SERVICETCOMPLETED)){
				tracktabledetails.setStatus(Service.SERVICETCOMPLETED);
				TechnicianAppOperation app = new TechnicianAppOperation();
					try {
						String trapDetailsdata=req.getParameter("saveTrapDetails").trim();
						logger.log(Level.SEVERE , "trapDetailsdata : " + trapDetailsdata);
						org.json.JSONObject jsonData = new org.json.JSONObject(trapDetailsdata);
						String tranResponse = app.saveTrapDetails(jsonData, comp, req, resp,service);
						logger.log(Level.SEVERE , "Trap response :" + tranResponse);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
//					/** @UAT
//					 * Date 13-01-2020 by vijay
//					 * Des :- storing checklist data separately
//					 */
//					try {
//						if(req.getParameter("saveCheckListDetails")!=null){
//						String checklistData=req.getParameter("saveCheckListDetails").trim();
//						org.json.JSONObject jsonData = new org.json.JSONObject(checklistData);
//						String tranResponse = saveChecklistData(service,jsonData);
//						logger.log(Level.SEVERE , "Trap response :" + tranResponse);
//						}
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//					if(req.getParameter("checklistRemark")!=null){
//						service.setChecklistRemark(req.getParameter("checklistRemark"));
//					}
//					/**
//					 * ends here
//					 */
					
					try{
						String materialdata=req.getParameter("materialReturn").trim();
						org.json.JSONObject jsonMaterialData = new org.json.JSONObject(materialdata);
						String materialResponse = app.createMaterialReturnDoc(comp, jsonMaterialData, req, resp);
						logger.log(Level.SEVERE , "Material response :" + materialResponse);
					}catch(Exception e){
						e.printStackTrace();
					}
					
					/**
					 * Date 09-11-2019 
					 * @author Vijay Chougule for WMS Fumigation Service
					 */
					if(service.isWmsServiceFlag() && service.getProduct()!=null && service.getProduct().getProductCode().trim().equals("STK-01")){
						wmsFumigationservicelist.add(service);
						
					}
					/**
					 * Date 10-12-2019 by Vijay 
					 * Des :- Degassing Services Tcompleted then updated this status in its fumigation Service
					 */
					if(service.isWmsServiceFlag() && service.getProduct()!=null && service.getProduct().getProductCode().trim().equals("DSSG")){
						if(service.getFumigationServiceId()!=0){
							Service fumigationService = ofy().load().type(Service.class).filter("companyId", service.getCompanyId())
									.filter("count", service.getFumigationServiceId()).first().now();
							if(fumigationService!=null){
								fumigationService.setDegassingServiceStatus(Service.SERVICETCOMPLETED);
								ofy().save().entities(fumigationService);
								logger.log(Level.SEVERE, "Deggasing Service status updated in its fumigation service");
							}
						}
						
					}
					
					/**
					 * @author Anil
					 * @since 04-07-2020
					 * Storing checklist and pest treatment details 
					 */
					try {
						String checklistDetails=req.getParameter("saveChecklistDetails").trim();
						logger.log(Level.SEVERE , "checklistDetails : " + checklistDetails);
						org.json.JSONObject jsonData = new org.json.JSONObject(checklistDetails);
						String checklistResponse = app.saveChecklistDetails(jsonData, comp, req, resp,service);
						logger.log(Level.SEVERE , "Checklist response :" + checklistResponse);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					
					try {
						String pestTreatmentDetails=req.getParameter("savePestTreatmentDetails").trim();
						logger.log(Level.SEVERE , "pestTreatmentDetails : " + pestTreatmentDetails);
						org.json.JSONObject jsonData = new org.json.JSONObject(pestTreatmentDetails);
						String pestTreatmentResponse = app.savePestTreatmentDetails(jsonData, comp, req, resp,service);
						logger.log(Level.SEVERE , "Pest response :" + pestTreatmentResponse);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			logger.log(Level.SEVERE,"Stage::::::::4");
			
			//   rahul added this for null pointer exp in table 
//			if(service.getTrackServiceTabledetails()!=null){
			tracktabledetails.setSrNo(service.getTrackServiceTabledetails().size()+1);
//			}
//			else
//			{
//				tracktabledetails.setSrNo(1);
//			}
			/**
			 * Updated By: Viraj
			 * Date: 02-03-2019
			 * Description: To store latitude and longitude
			 */
			tracktabledetails.setLatitude(lat);
			tracktabledetails.setLongitude(lon);
			/**
			 * Updated By: komal
			 * Date: 01-04-2019
			 * Description: To get and save location and time seperately
			 */
			tracktabledetails.setLocationName(location);
			if(!serviceActionTime.equals("")){
				tracktabledetails.setTime(serviceActionTime);
			}
			else{
				tracktabledetails.setTime(timeFormat.format(new Date()));
			}
			tracktabledetails.setKiloMeters(km);
			/** Ends **/
			service.getTrackServiceTabledetails().add(tracktabledetails);
			logger.log(Level.SEVERE,"Stage::::::::5"+service.getTrackServiceTabledetails().size());
			if(syncFlag){
				//Reschedule
				org.json.JSONObject rescheduleHistory=new org.json.JSONObject(rescheduleData);
				ModificationHistory modHistory = new ModificationHistory();
				modHistory.setOldServiceDate(sdf.parse(rescheduleHistory.optString("previousServiceDate")));
				modHistory.setResheduleDate(sdf.parse(rescheduleHistory.optString("rescheduleDate")));

				// SimpleDateFormat sdf1=new
				// SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
				// sdf1.setTimeZone(TimeZone.getTimeZone("IST"));
				modHistory.setSystemDate(new Date());
				String[] time = rescheduleHistory.optString("rescheduleTime").trim().split(":");
				int hours = Integer.parseInt(time[0]);
				int mins = Integer.parseInt(time[1].substring(0, 2));
				if (hours >= 12) {
					int hoursDecrease = 0;
					if (hours != 12) {
						hoursDecrease = 12;
					}
					if(mins<10){
						modHistory.setResheduleTime((hours - hoursDecrease) + ":"+"0"+ mins + " PM");
					}else{
						modHistory.setResheduleTime((hours - hoursDecrease) + ":"
								+ mins + " PM");
					}
					
					logger.log(Level.SEVERE, "Service Time::::::"
							+ (hours - hoursDecrease) + ":" + mins + " PM");

				} else {
					logger.log(Level.SEVERE, "Service Time::::::" + hours + ":"
							+ mins + " AM");
					modHistory.setResheduleTime(hours + ":" + mins + " AM");
				}
				modHistory.setReason(rescheduleHistory.optString("rescheduledReason"));
				/** date 23.3.2019 added by komal to store employee name**/
				modHistory.setUserName(rescheduleHistory.optString("employeeName"));
//				List<ModificationHistory> modList=new ArrayList<ModificationHistory>();
//				modList.add(modHistory);
//				ModificationHistoryTable table = new ModificationHistoryTable();
//				table.getDataprovider().getList().add(modHistory);
				service.getListHistory().add(modHistory);
			}
			
			ofy().save().entity(service).now();
			
			logger.log(Level.SEVERE, "Service Saved successfully");
			/**
			 * Date 09-11-2019
			 * @author Vijay Chougule
			 * Des :- NBHC CCPM WMS Fumigation service TCompletetion creating Deggasing Service
			 */
			if(wmsFumigationservicelist.size()!=0){
				String strTcompletionDate = sdf.format(new Date());
				Date TcompletionDate = sdf.parse(strTcompletionDate);
				String degassingservice = createFumigationDeggassingService(wmsFumigationservicelist,"DSSG",TcompletionDate);
				logger.log(Level.SEVERE, "degassing ServiceResponse"+degassingservice);

			}
			logger.log(Level.SEVERE, "here we are now");
			/**
			 * ends here
			 */
			
			/**
			 * Date 14-11-2019 
			 * @author Vijay Chougule
			 * Des :- NBHC CCPM complaint service also closing comlaint
			 */
			if(service.getTicketNumber()!=-1 && ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableCompleteComplaintService", service.getCompanyId())){
				ServiceImplementor serviceimpl = new ServiceImplementor();
				serviceimpl.reactonCompleteComplaintService(service);
			}
			
			logger.log(Level.SEVERE, "here we are now 2");
			//Added by Apeksha for sending notification and sms to customerr after service reported successfully.
			try{
//				sendMessageToCustomer(service);
				CustomerUser customerUser = ofy().load().type(CustomerUser.class)
						.filter("companyId", Long.parseLong(companyId))
						.filter("cinfo.count",service.getPersonInfo().getCount()).first().now();
				String response = "Service Reported";
				logger.log(Level.SEVERE,"AppConstants.IAndroid.EVA_KRETU:"+AppConstants.IAndroid.EVA_KRETO);
				
				SmsServiceImpl sms=new SmsServiceImpl();
				String event="";
				if(status.trim().equalsIgnoreCase(Service.SERVICESTATUSREPORTED)){
					event="Technician Reported";
				}else if(status.trim().equalsIgnoreCase(Service.SERVICESTATUSSTARTED)){
					event="Technician Started";
				}
				
				/**
				 * @author Anil @since 10-03-2021
				 * getting technician mobile no. from technician list in service 
				 */
				long technicianCellNo=0;
				if(service.getTechnicians()!=null&&service.getTechnicians().size()!=0){
					for(EmployeeInfo info:service.getTechnicians()){
						if(info.getFullName().equals(service.getEmployee())){
							if(info.getCellNumber()!=null){
								technicianCellNo=info.getCellNumber();
							}
							break;
						}
					}
				}
				
				if(technicianCellNo==0){
					Employee employee=ofy().load().type(Employee.class).filter("companyId",comp.getCompanyId()).filter("fullname", service.getEmployee()).first().now();
					if(employee!=null){
						if(employee.getCellNumber1()!=null){
							technicianCellNo=employee.getCellNumber1();
						}
					}
				}
				
				
				try{
					String smsResponse=sms.sendSMSFromTechnician(comp, comp.getCompanyId(), event, service.getPersonInfo().getFullName(), service.getEmployee(), fmt.format(service.getServiceDate()), service.getPersonInfo().getCellNumber(), service.getProductName(),technicianCellNo,service.getPersonInfo().getCount());
					logger.log(Level.SEVERE,"smsResponse"+smsResponse);
				}catch(Exception e){
					logger.log(Level.SEVERE,"error in sms"+e);
				}
				sendPushNotification(Long.parseLong(companyId),event, AppConstants.IAndroid.EVA_KRETO, 
						customerUser.getUserName().trim(), response,service);
			} catch(Exception e) {
				logger.log(Level.SEVERE,"Error::::::::"+e);
				e.printStackTrace();
			}
			
			}
			
			logger.log(Level.SEVERE,"Stage::::::::6");
			
		}
		
		}catch(Exception e){
			e.printStackTrace();
			logger.log(Level.SEVERE,"Error::::::::"+e);
			exep=e;
			resp.getWriter().print("Failed");
		}
		if(exep==null){
			resp.getWriter().print("Success");
		}
	
		
	}
	
	/*
	 * Date: 29 Jul 2017
	 * By: Apeksha Gunjal
	 * send Message to Custmer when Service is reported successfully. 
	 */
	
	private void sendMessageToCustomer(Service service) {
		// TODO Auto-generated method stub
		SmsServiceImpl smsServiceImpl = new SmsServiceImpl();
		smsServiceImpl.sendSmsToCustomers(null, null, null, null, null, false);
	}
	
	/*
	 * Date: 31 Jul 2017
	 * By: Apeksha Gunjal
	 * send Push Notification to Custmer when Service is reported successfully. 
	 */
	
	private void sendPushNotification(long companyId,String event, String applicationName,String customerUserName,String response, Service service) {
		if(event.trim().equalsIgnoreCase("Technician Reported")){
			response="event:Technician Reported"+"$"+service.getEmployee()+"has reported to your location for "+service.getProductName()+" service";
		}else if(event.trim().equalsIgnoreCase("Technician Started")){
			response="event:Technician Started"+"$"+service.getEmployee()+"has started to your location for "+service.getProductName()+" service";
		}
		;//getJSONResponse(service);
		CustomerUser customerUser=ofy().load().type(CustomerUser.class).filter("companyId",companyId).filter("cinfo.fullName",service.getPersonInfo().getFullName()).first().now(); 
		RegisterDevice device = ofy().load().type(RegisterDevice.class)
				.filter("companyId", companyId)
				.filter("applicationName", applicationName)
				.filter("userName", customerUser.getUserName()).first().now();
		String deviceName = device.getRegId().trim();
		logger.log(Level.SEVERE,"deviceName"+deviceName);
		URL url = null;
		try {
			url = new URL("https://fcm.googleapis.com/fcm/send");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HttpURLConnection conn = null;
		try {
			conn = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		conn.setDoOutput(true);
		try {
			conn.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		conn.setRequestProperty("Content-Type",
				"application/json; charset=UTF-8");
		conn.setRequestProperty("Authorization", "key="
				+ comp.getFcmServerKeyForKreto().trim());
		conn.setDoOutput(true);
		conn.setDoInput(true);

		try {
			conn.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "ProtocolException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		OutputStream os = null;
		try {
			os = conn.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
		// Log.d("Test", "From Get Post Method" + getPostData(values));
		try {
			writer.write(createJSONData(response, deviceName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException2 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		try {
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException3 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		InputStream is = null;
		try {
			is = conn.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String temp;
		String data = null;
		try {
			while ((temp = br.readLine()) != null) {
				data = data + temp;
			}
			logger.log(Level.SEVERE, "data data::::::::::" + data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		logger.log(Level.SEVERE, "Data::::::::::" + data);
	}
	
	
	private String createJSONData(String message, String deviceName) {
		// TODO Auto-generated method stub
		JSONObject jObj1 = new JSONObject();
		jObj1.put("message", message);

		JSONObject jObj = new JSONObject();
		jObj.put("data", jObj1);
		jObj.put("to", deviceName);

		String jsonString = jObj.toJSONString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE, "JSON Data Sent:::::::" + jsonString);

		return jsonString;
	}
	
	
	/**
	 * Date 09-11-2019
	 * @author Vijay Chougule
	 * Des :- NBHC CCPM WMS Fumigation service completetion creating Deggasing Service
	 * @param productCode 
	 * @param tcompletionDate 
	 * @return 
	 */
	public String createFumigationDeggassingService(List<Service> servicelist, String productCode, Date tcompletionDate) {
		String strresponse = "";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		ArrayList<SuperModel> deggassingServicelist = new ArrayList<SuperModel>();
		ServiceProduct serviceProd = ofy().load().type(ServiceProduct.class).filter("productCode", productCode)
				.filter("companyId", servicelist.get(0).getCompanyId()).first().now();
		logger.log(Level.SEVERE, "serviceProd"+serviceProd);
		try {
			
		if(serviceProd!=null){
			for(Service serviceEntity : servicelist){
				logger.log(Level.SEVERE, "serviceProd");
				Service temp = new Service();
				temp.setRefNo(serviceEntity.getRefNo()); // inward transaction id
				temp.setPersonInfo(serviceEntity.getPersonInfo());
				temp.setContractCount(serviceEntity.getContractCount());
				temp.setProduct(serviceProd);
				temp.setBranch(serviceEntity.getBranch());
				temp.setStatus(Service.SERVICESTATUSSCHEDULE);
				temp.setContractStartDate(serviceEntity.getContractStartDate());
				temp.setContractEndDate(serviceEntity.getContractEndDate());
				temp.setAdHocService(false);
				temp.setServiceIndexNo(serviceEntity.getServiceIndexNo()+1);
				temp.setServiceType(serviceEntity.getServiceType());
				temp.setServiceSerialNo(serviceEntity.getServiceSerialNo());
				Date servicecompletionDateWit7Days = null;
				if(tcompletionDate!=null){
					servicecompletionDateWit7Days = tcompletionDate;
				}
				else{
					servicecompletionDateWit7Days = serviceEntity.getServiceCompletionDate();
				}
				Calendar c4 = Calendar.getInstance();
				c4.setTime(servicecompletionDateWit7Days);
				c4.add(Calendar.DATE, +7);
				logger.log(Level.SEVERE, "Date after 7 Days===" + servicecompletionDateWit7Days);
				servicecompletionDateWit7Days = sdf.parse(sdf.format(c4.getTime()));
				logger.log(Level.SEVERE, "Date after 7 Days" + servicecompletionDateWit7Days);

				temp.setServiceDate(servicecompletionDateWit7Days);
				temp.setOldServiceDate(servicecompletionDateWit7Days);
				temp.setAddress(serviceEntity.getAddress());

				temp.setServiceTime(serviceEntity.getServiceTime());
				temp.setServiceBranch(serviceEntity.getServiceBranch());
				temp.setServiceDateDay(serviceEntity.getServiceDateDay());

				temp.setBranch(serviceEntity.getBranch());
				temp.setServicingTime(serviceEntity.getServicingTime());

				temp.setServiceSrNo(serviceEntity.getServiceSerialNo());
				temp.setCompanyId(serviceEntity.getCompanyId());
				/**
				 * Date 01-08-2019 by Vijay for NBHC CCPM Fumigation
				 * Services for every Stack This code only executed from
				 * Web Services of NBHC Fumigation service
				 */
				temp.setWareHouse(serviceEntity.getWareHouse());
				temp.setRefNo2(serviceEntity.getRefNo2());
				temp.setShade(serviceEntity.getShade());
				temp.setCompartment(serviceEntity.getCompartment());
				temp.setStackNo(serviceEntity.getStackNo());
				temp.setCommodityName(serviceEntity.getCommodityName());
				temp.setStackQty(serviceEntity.getStackQty());
				if (serviceEntity.getDepositeDate() != null)
					temp.setDepositeDate(serviceEntity.getDepositeDate());
				temp.setWmsServiceFlag(true);
				temp.setFumigationServiceId(serviceEntity.getCount());
				SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();
//				int degassingServiceId=(int) numGen.getLastUpdatedNumber("Service", serviceEntity.getCompanyId(), 1);
				int degassingServiceId=(int) numGen.getLastUpdatedNumber(AppConstants.INHOUSESERVICE, serviceEntity.getCompanyId(), 1);
				temp.setCount(degassingServiceId);
				ofy().save().entity(temp);
				
//				GenricServiceImpl impl = new GenricServiceImpl();
//				ReturnFromServer server = new ReturnFromServer();
//				server = impl.save(temp);
//				logger.log(Level.SEVERE, "Deggassing Service Id"+server.count);
				serviceEntity.setDeggassingServiceId(degassingServiceId);
				ofy().save().entity(serviceEntity);
				
				
				logger.log(Level.SEVERE, "New service Deggassing scheduled successfully with 7 Days");
				updateFumigationReport(serviceEntity,degassingServiceId,temp.getOldServiceDate());
				strresponse = "New service Deggassing scheduled successfully with 7 Days";
			}
//			logger.log(Level.SEVERE, "deggassingServicelist"+deggassingServicelist.size());
//			if(deggassingServicelist.size()!=0){
//				GenricServiceImpl impl = new GenricServiceImpl();
//				impl.save(deggassingServicelist);
//				logger.log(Level.SEVERE, "New service Deggassing scheduled successfully with 7 Days");
//			}
		
		}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception"+e.getMessage());
			strresponse = e.getMessage();
		}
		return strresponse;
	}

/**
 * @author Vijay Chougule
 * Des :- here i am updating fumigation report entity with degassing service id and degassing service Date
 * @param serviceEntity
 * @param degassingServiceId
 * @param degassingServiceDate
 */
	private void updateFumigationReport(Service serviceEntity, int degassingServiceId, Date degassingServiceDate) {
		logger.log(Level.SEVERE, "Warehouse Code"+serviceEntity.getRefNo2());
		logger.log(Level.SEVERE, "compartment"+serviceEntity.getCompartment());
		logger.log(Level.SEVERE, "contractId"+serviceEntity.getContractCount());
		logger.log(Level.SEVERE, "shade"+serviceEntity.getShade());
		logger.log(Level.SEVERE, "fumigationServiceId "+serviceEntity.getCount());

		FumigationServiceReport fumigationReport = ofy().load().type(FumigationServiceReport.class)
												.filter("companyId", serviceEntity.getCompanyId()).filter("contractId", serviceEntity.getContractCount())
												.filter("warehouseCode", serviceEntity.getRefNo2()).filter("compartment", serviceEntity.getCompartment())
												.filter("shade", serviceEntity.getShade()).filter("fumigationServiceId", serviceEntity.getCount()).first().now();
		if(fumigationReport!=null){
			logger.log(Level.SEVERE, "fumigationReport for updating degassing Service id and Service Date"+fumigationReport);
			fumigationReport.setDeggasingServiceId(degassingServiceId);
			fumigationReport.setDeggasingServiceDate(degassingServiceDate);
			fumigationReport.setFumigationServiceStatus(Service.SERVICETCOMPLETED);
			ofy().save().entity(fumigationReport);
			logger.log(Level.SEVERE, "Degassing Service id and date updated in fumigation report");
		}
												
	}
	
	/**
	 * Date 13-01-2020 by vijay @UAT
	 * Des :- storing checklist data separately
	 */
//	private String saveChecklistData(Service service, org.json.JSONObject jsonData) {
//		
//		JSONArray checklistArray = null;
//		try {
//			checklistArray = jsonData.getJSONArray("saveCheckListDetails");
//		} catch (JSONException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		ArrayList<CheckList> checklistData = new ArrayList<CheckList>();
//		org.json.JSONObject obj = null;
//		for(int i =0 ; i<checklistArray.length() ; i++){
//			try {
//				   obj = checklistArray.getJSONObject(i);
//					String pestName = obj.optString("name");
//					CheckList chklist = new CheckList();
//					chklist.setName(pestName);
//					checklistData.add(chklist);
//			}catch(Exception e){
//				
//			}
//		}
//		if(checklistData.size()!=0){
//			service.setChecklist(checklistData);
//			ofy().save().entity(service);
//			logger.log(Level.SEVERE, "Service Data updated for checklist");
//		}
//		
//		return "Success";
//	}
}
