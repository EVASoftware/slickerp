package com.slicktechnologies.server.android;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class CustomerCompletedServiceDetails extends HttpServlet{


	/**
	 * 
	 */

	/**
	 * 
	 */
	private static final long serialVersionUID = 2289802941327235027L;

	/**
	 * 
	 */

	Logger logger = Logger.getLogger("CustomerCompletedServiceDetails.class");

	Company comp;
	SimpleDateFormat sdf;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String response="";
		String urlCalled=req.getRequestURL().toString().trim();
//		String url=urlCalled.replace("http://", "");
//		String[] splitUrl=url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		Exception exep = null;
		try{
			comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();	
			long companyId=comp.getCompanyId();
			String customerId=req.getParameter("customerId");
			sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			calendar.add(Calendar.DAY_OF_YEAR, 30);
			Date date = calendar.getTime();
			
			Calendar serviceCalend = Calendar.getInstance();
			serviceCalend.setTime(new Date());
			serviceCalend.add(Calendar.DAY_OF_YEAR, 7);
			Date date1 = serviceCalend.getTime();
			
			List<Service> serviceList = new ArrayList<Service>();
			serviceList = ofy().load().type(Service.class)
					.filter("companyId", comp.getCompanyId())
					.filter("personInfo.count", Integer.parseInt(customerId))
					.filter("status", Service.SERVICESTATUSCOMPLETED)
					.filter("serviceDate <=", date1).list();
			
			JSONObject globalJObj = new JSONObject();

			globalJObj.put("serviceList", getServiceList(serviceList));

			String jsonString = globalJObj.toJSONString().replace("\\\\", "");
			logger.log(Level.SEVERE, jsonString);
			resp.getWriter().println(jsonString);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	private JSONArray getServiceList(List<Service> serviceList) {
		// TODO Auto-generated method stub

		try {
			JSONArray jArray = new JSONArray();
			for (int i = 0; i < serviceList.size(); i++) {
				JSONObject obj = new JSONObject();
				obj.put("contractId", serviceList.get(i).getContractCount());
				obj.put("startDate", getFormattedDate(serviceList.get(i)
						.getContractStartDate()));
				obj.put("endDate", getFormattedDate(serviceList.get(i)
						.getContractEndDate()));
				obj.put("serviceName", serviceList.get(i).getProductName());
				obj.put("serviceNo", serviceList.get(i).getServiceSerialNo());
				obj.put("serviceEngineer", serviceList.get(i).getEmployee());
				obj.put("serviceDate", getFormattedDate(serviceList.get(i)
						.getServiceDate()));
				obj.put("serviceBranch", serviceList.get(i).getServiceBranch());
				obj.put("status", serviceList.get(i).getStatus().trim());
				obj.put("serviceId",serviceList.get(i).getCount());
				// obj.put("serviceAddress", serviceList.get(i).get)
				jArray.add(obj);
			}
			return jArray;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getFormattedDate(Date date) {

		return sdf.format(date);

	}
}
