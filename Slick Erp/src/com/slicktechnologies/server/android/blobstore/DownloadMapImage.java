package com.slicktechnologies.server.android.blobstore;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

public class DownloadMapImage extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6892253129739881317L;

	/**
	 * 
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
        doPost(req, resp);
    }
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		String blobkeyString = req.getParameter("blobkey");
        if (blobkeyString != null) {
            BlobKey blobKey = new BlobKey(blobkeyString);
            /** @author Vijay Chougule Date 06-01-2019
             *  Des :- When click on link download the images
             **/
            String filename=req.getParameter("filename");
		    resp.setHeader("Content-Disposition", "Attachment;filename="+filename);
		    /**
		     * ends here
		     */
            BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
            blobstoreService.serve(blobKey,  resp);
           
        }
        else {
            resp.sendError(400, "One or more parameters are not set");
        }
	}

}
