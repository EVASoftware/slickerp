package com.slicktechnologies.server.android.blobstore;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.images.Image;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.Transform;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class GetImage extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5692689044394553519L;
	
	Logger logger = Logger.getLogger("GetImageUrl.class");

	Company company;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		resp.setHeader("Access-Control-Allow-Origin", "*");
		resp.setContentType("image/gif");
		
		String urlCalled = req.getRequestURL().toString().trim();
		logger.log(Level.SEVERE, "urlCalled " + urlCalled);
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		
		int width =200;
		int height = 1024;
		try {
			String strwidth = req.getParameter("width");
			String strheight = req.getParameter("height");
			logger.log(Level.SEVERE, "width " + width);
			logger.log(Level.SEVERE, "height" + height);
			width = Integer.parseInt(strwidth);
			height = Integer.parseInt(strheight);
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			logger.log(Level.SEVERE, "accessUrl " + splitUrl[0]);
			company = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
			
			if(company!=null){
				if(company.getLogo()!=null&&!company.getLogo().getUrl().equals("")){
					String imageUrl = company.getLogo().getUrl();
					String[] onlyimagekey = imageUrl.split("blob-key=");
					String imageKey = onlyimagekey[1];
					logger.log(Level.SEVERE, "imageKey"+imageKey);
					BlobKey blobkey = new BlobKey(imageKey);
				
					ImagesService imagesService = ImagesServiceFactory.getImagesService();

					Image blobImage = ImagesServiceFactory.makeImageFromBlob(blobkey);
					Transform rotate = ImagesServiceFactory.makeResize(width, height);
					Image rotatedImage = imagesService.applyTransform(rotate, blobImage);

					resp.getOutputStream().write(rotatedImage.getImageData());


				}else{
					logger.log(Level.SEVERE, "Logo is not uploaded");
					resp.getWriter().println("Logo is not uploaded");
				}
			}
			else{
				logger.log(Level.SEVERE, "Unable to load company");
				resp.getWriter().println("Unable to load company");

			}
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "ERROR---->" + e);

		}
	}

}
