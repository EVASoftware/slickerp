package com.slicktechnologies.server.android.blobstore;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class GetImageUrl extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1226399252641123036L;
	Logger logger = Logger.getLogger("GetImageUrl.class");

	Company company;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
        doPost(req, resp);
    }
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

//		resp.setHeader("Access-Control-Allow-Origin", AppConstants.TRENDZWEBSOLUTION);
		resp.setHeader("Access-Control-Allow-Origin", "*");
		
		RegisterServiceImpl impl = new RegisterServiceImpl();
		String urlCalled = req.getRequestURL().toString().trim();
		logger.log(Level.SEVERE, "urlCalled " + urlCalled);
		String url="";
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		String	httpString = "https://";

		try {
			logger.log(Level.SEVERE, "accessUrl " + splitUrl[0]);
			company = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
			
			if(company!=null){
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio", "DisplayClientLogo", company.getCompanyId())){
					if(company.getCompanyURL()!=null&&!company.getCompanyURL().equals("")){
						String companyUrl="";
						if(company.getCompanyURL().contains("http://")){
							companyUrl=company.getCompanyURL().replace("http://", " ");
						}else{
							companyUrl=company.getCompanyURL();
						}
						if(company.getCompanyURL().contains("/")){
							companyUrl=companyUrl.replace("/", "").trim();
						}else{
							companyUrl=companyUrl.trim();
						}
						
						logger.log(Level.SEVERE, "Company URL : "+ companyUrl);
						if(!companyUrl.contains("-dot-")){
							companyUrl = companyUrl.replaceFirst("\\.", "-dot-");
						}
						
//						if(companyUrl.contains("-dot-")){
//							companyUrl = companyUrl.replaceFirst("-dot-","\\.");
//						}
						if(company.getLogo()!=null&&!company.getLogo().getUrl().equals("")){
							url=httpString+""+companyUrl;
							url=url+company.getLogo().getUrl()+ "&" + "filename="+company.getLogo().getName();
							logger.log(Level.SEVERE, "Logo URL : "+url);
						}else{
							logger.log(Level.SEVERE, "Logo is not uploaded");
						}
					}else{
						logger.log(Level.SEVERE, "Process configuration is not active");
					}
				}else{
					logger.log(Level.SEVERE, "Process configuration is not active");
				}
			}else{
				logger.log(Level.SEVERE, "Unable to load company");
			}
			
			resp.getWriter().println(url);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "ERROR---->" + e);
		}

	}

}
