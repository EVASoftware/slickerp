package com.slicktechnologies.server.android.blobstore;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.blobstore.UploadOptions;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
/**
 * Description : This API is used to create url of image which you wnat to save into database.
 * @author pc1
 *
 */
public class BlobUrlGet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 786478957292181094L;
	/**
	 * 
	 */
	
	BlobstoreService blServ = BlobstoreServiceFactory.getBlobstoreService();
	Logger logger=Logger.getLogger("BlobUrlGet.class");

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		logger.log(Level.SEVERE,"Inside Get");
		try {
			doPost(req, resp);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE,"Inside Post");
		String urlCalled = req.getRequestURL().toString().trim();
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
		logger.log(Level.SEVERE,"Inside Post" + urlCalled);
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE,"Inside Post" + splitUrl[0]);
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		
		logger.log(Level.SEVERE,"Inside Post" + comp.getBusinessUnitName());
		
		BlobstoreService blobstoreService = BlobstoreServiceFactory
				.getBlobstoreService();
		UploadOptions uploadOptions = null;
		String bucket = comp.getBucketName();//"evadev0001.appspot.com";
		uploadOptions = UploadOptions.Builder
				.withGoogleStorageBucketName(bucket);
		resp.getWriter().write(
				blobstoreService.createUploadUrl("/slick_erp/blobUpload",
						uploadOptions));
		logger.log(Level.SEVERE,"url Obtained:::::"+
				blobstoreService.createUploadUrl("/slick_erp/blobUpload",
						uploadOptions));
		

	}
}