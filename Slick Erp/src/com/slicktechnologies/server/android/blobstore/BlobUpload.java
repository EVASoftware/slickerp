package com.slicktechnologies.server.android.blobstore;

import static com.googlecode.objectify.ObjectifyService.ofy;
/** 
 * Description : This API is used to save image into database.This API is called to save 
 * all service images , customer signature from pedio app . 
 */



import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobInfoFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.googlecode.objectify.ObjectifyService;
import com.slicktechnologies.client.views.assesmentreport.AssesmentReportEmbedForTable;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.android.EmployeeFingerDB;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class BlobUpload extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5503391538725965097L;
	/**
	 * 
	 */
	BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
	Logger logger = Logger.getLogger("BlobUpload.class");
	public static String PARAMETER_TYPE_PHOTO = "photo";
	public static String PARAMETER_EXPENSE_TYPE_PHOTO = "expensePhoto";
	public static String PARAMETER_ASSESMENT_TYPE_PHOTO = "assesmentPhoto";
	public static String PARAMETER_FINGER_DATA= "fingerData";
	public static String IMAGE_TYPE_IMAGE = "filename";
	
	public static String PARAMETER_TYPE_ISSUEID = "issueid";
	public static String PARAMETER_TYPE_TYPE = "type";
	public static String PARAMETER_TYPE_BOUNDARY = "boundary";
	
	/**
	 * @author Anil ,Date : 15-03-2019
	 * adding type for storing service images from pedio app
	 */
	public static String PARAMETER_SERVICE_PHOTO1 = "servicePhoto1";
	public static String PARAMETER_SERVICE_PHOTO2 = "servicePhoto2";
	public static String PARAMETER_SERVICE_PHOTO3 = "servicePhoto3";
	public static String PARAMETER_SERVICE_PHOTO4 = "servicePhoto4";
	
	/**
	 * @author Anil
	 * @since 19-06-2020
	 * adding key field name for storing service summary 1 and 2 in service entity from pedio app
	 */
	public static String PARAMETER_SERVICE_SUMMARY1 = "serviceSummary1";
	public static String PARAMETER_SERVICE_SUMMARY2 = "serviceSummary2";
	
	/**
	 * @author Anil
	 * @since 19-03-2021
	 * Adding constant for uploading employeePhoto from attendance app
	 * raised by Nitin/Rahul for Alkosh
	 */
	public static String EMPLOYEE_PHOTO = "employeePhoto";
	
	/**
	 * @author Anil @since 19-10-2021
	 * Adding fields to capture before and after service image
	 * earlier we were storing only 4, now as per the new requirement we neen to store 5 more
	 * Raised by Rahul Tiwari and Nitin Sir
	 */
	
	public static String PARAMETER_SERVICE_PHOTO5 = "servicePhoto5";
	public static String PARAMETER_SERVICE_PHOTO6 = "servicePhoto6";
	public static String PARAMETER_SERVICE_PHOTO7 = "servicePhoto7";
	public static String PARAMETER_SERVICE_PHOTO8 = "servicePhoto8";
	public static String PARAMETER_SERVICE_PHOTO9 = "servicePhoto9";
	public static String PARAMETER_SERVICE_PHOTO10 = "servicePhoto10";
	
	/**
	 * @author Anil @since 06-01-2022
	 * Adding fields to capture assessment task image for audit app
	 * raised by Nitin sir
	 */
	public static String ASSESSMENT_TASK_PHOTO = "assessmentTask";
	public static String CUSTOMER_SIGNATURE_FOR_AUDIT = "customerSignatureForAudit";//Ashwini Patil Date:21-12-2022
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res)throws IOException {
		String urlCalled = req.getRequestURL().toString().trim();
//		String httpString = "http://";
//		if(urlCalled.contains("-dot-")){
			String	httpString = "https://";
//		}
//		List<Company> compList=ofy().load().type(Company.class).list();
		Company company=new Company();
		long companyId=0l;
//		for (int i = 0; i < compList.size(); i++) {
//			if(!compList.get(i).getAccessUrl().trim().equalsIgnoreCase("ess")){
//				companyId=compList.get(i).getCompanyId();
//				company=compList.get(i);
//			}
//		}
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "SPLIT URL::::::::::::" + splitUrl[0]);
		company = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
		companyId =  company.getCompanyId();
		logger.log(Level.SEVERE, "COMPANTiD::::::::::::" + company.getCompanyId());
		logger.warning("UploadHandlerServlet doPost START");
		String issueId = req.getParameter(PARAMETER_TYPE_ISSUEID);
		logger.log(Level.SEVERE, "issueId::::::::::::" + issueId);
		String type = req.getParameter(PARAMETER_TYPE_TYPE);
		logger.log(Level.SEVERE, "type::::::::::::" + type);
		String boundary = req.getParameter(PARAMETER_TYPE_BOUNDARY);
		logger.log(Level.SEVERE, "boundary::::::::::::" + boundary);
		
		String multipleServiceIds ="";
		try {
			multipleServiceIds = req.getParameter("multipleServiceId");
		} catch (Exception e) {
			multipleServiceIds="";
		}
		logger.log(Level.SEVERE, "multipleServiceIds" + multipleServiceIds);

		BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(req);
		List<BlobKey> keys = null;
		if (type.trim().equalsIgnoreCase(PARAMETER_TYPE_PHOTO.trim())) {
			keys = blobs.get(PARAMETER_TYPE_PHOTO.trim());
			logger.log(Level.SEVERE, "Type Photo BlobKey::::::::::::"+ keys.get(0).getKeyString());
		}else if(type.trim().equalsIgnoreCase(PARAMETER_EXPENSE_TYPE_PHOTO.trim())){
			keys = blobs.get(PARAMETER_EXPENSE_TYPE_PHOTO.trim());
			logger.log(Level.SEVERE,"BlobKey::::::::::::"+keys.get(0).getKeyString());
		}else if(type.trim().equalsIgnoreCase(PARAMETER_ASSESMENT_TYPE_PHOTO.trim())){
			keys = blobs.get(PARAMETER_ASSESMENT_TYPE_PHOTO.trim());
		}else if(type.trim().equalsIgnoreCase(PARAMETER_FINGER_DATA.trim())){
			keys = blobs.get(PARAMETER_FINGER_DATA.trim());
		}
		/**
		 * @author Anil ,Date : 15-03-2019
		 */
		else if(type.trim().equalsIgnoreCase(PARAMETER_SERVICE_PHOTO1.trim())){
			keys = blobs.get(PARAMETER_SERVICE_PHOTO1.trim());
			logger.log(Level.SEVERE, "Type Photo1 BlobKey::::::::::::"+ keys.get(0).getKeyString());
		}else if(type.trim().equalsIgnoreCase(PARAMETER_SERVICE_PHOTO2.trim())){
			keys = blobs.get(PARAMETER_SERVICE_PHOTO2.trim());
			logger.log(Level.SEVERE, "Type Photo2 BlobKey::::::::::::"+ keys.get(0).getKeyString());
		}else if(type.trim().equalsIgnoreCase(PARAMETER_SERVICE_PHOTO3.trim())){
			keys = blobs.get(PARAMETER_SERVICE_PHOTO3.trim());
			logger.log(Level.SEVERE, "Type Photo3 BlobKey::::::::::::"+ keys.get(0).getKeyString());
		}else if(type.trim().equalsIgnoreCase(PARAMETER_SERVICE_PHOTO4.trim())){
			keys = blobs.get(PARAMETER_SERVICE_PHOTO4.trim());
			logger.log(Level.SEVERE, "Type Photo4 BlobKey::::::::::::"+ keys.get(0).getKeyString());
		}
		
		
		else if(type.trim().equalsIgnoreCase(PARAMETER_SERVICE_PHOTO5.trim())){
			keys = blobs.get(PARAMETER_SERVICE_PHOTO5.trim());
			logger.log(Level.SEVERE, "Type Photo5 BlobKey::::::::::::"+ keys.get(0).getKeyString());
		}else if(type.trim().equalsIgnoreCase(PARAMETER_SERVICE_PHOTO6.trim())){
			keys = blobs.get(PARAMETER_SERVICE_PHOTO6.trim());
			logger.log(Level.SEVERE, "Type Photo6 BlobKey::::::::::::"+ keys.get(0).getKeyString());
		}else if(type.trim().equalsIgnoreCase(PARAMETER_SERVICE_PHOTO7.trim())){
			keys = blobs.get(PARAMETER_SERVICE_PHOTO7.trim());
			logger.log(Level.SEVERE, "Type Photo7 BlobKey::::::::::::"+ keys.get(0).getKeyString());
		}else if(type.trim().equalsIgnoreCase(PARAMETER_SERVICE_PHOTO8.trim())){
			keys = blobs.get(PARAMETER_SERVICE_PHOTO8.trim());
			logger.log(Level.SEVERE, "Type Photo8 BlobKey::::::::::::"+ keys.get(0).getKeyString());
		}else if(type.trim().equalsIgnoreCase(PARAMETER_SERVICE_PHOTO9.trim())){
			keys = blobs.get(PARAMETER_SERVICE_PHOTO9.trim());
			logger.log(Level.SEVERE, "Type Photo9 BlobKey::::::::::::"+ keys.get(0).getKeyString());
		}else if(type.trim().equalsIgnoreCase(PARAMETER_SERVICE_PHOTO10.trim())){
			keys = blobs.get(PARAMETER_SERVICE_PHOTO10.trim());
			logger.log(Level.SEVERE, "Type Photo10 BlobKey::::::::::::"+ keys.get(0).getKeyString());
		}
		
		
		/**
		 * End
		 */
		else if(type.trim().equalsIgnoreCase(PARAMETER_SERVICE_SUMMARY1.trim())){
			keys = blobs.get(PARAMETER_SERVICE_SUMMARY1.trim());
			logger.log(Level.SEVERE, "Type summary 1 BlobKey::::::::::::"+ keys.get(0).getKeyString());
		}else if(type.trim().equalsIgnoreCase(PARAMETER_SERVICE_SUMMARY2.trim())){
			keys = blobs.get(PARAMETER_SERVICE_SUMMARY2.trim());
			logger.log(Level.SEVERE, "Type summary 1 BlobKey::::::::::::"+ keys.get(0).getKeyString());
		}else if(type.trim().equalsIgnoreCase(EMPLOYEE_PHOTO.trim())){
			keys = blobs.get(EMPLOYEE_PHOTO.trim());
			logger.log(Level.SEVERE, "EMPLOYEE_PHOTO BlobKey::::::::::::"+ keys.get(0).getKeyString());
		}else if(type.trim().equalsIgnoreCase(ASSESSMENT_TASK_PHOTO.trim())){
			keys = blobs.get(ASSESSMENT_TASK_PHOTO.trim());
			logger.log(Level.SEVERE, "ASSESSMENT_TASK_PHOTO BlobKey::::::::::::"+ keys.get(0).getKeyString());
		}
		else if(type.trim().equalsIgnoreCase(CUSTOMER_SIGNATURE_FOR_AUDIT.trim())){
			keys = blobs.get(CUSTOMER_SIGNATURE_FOR_AUDIT.trim());
			logger.log(Level.SEVERE, "CUSTOMER_SIGNATURE_FOR_AUDIT BlobKey::::::::::::"+ keys.get(0).getKeyString());
		}
		
		
		
		String blobKey=keys.get(0).getKeyString();

		Iterator<BlobInfo> iterator = null;
		iterator = new BlobInfoFactory().queryBlobInfos();

		// List<BlobKey> keys1 = blobs
		// .get("123456");
		// logger.log(Level.SEVERE,keys1.get(0).getKeyString());
		

		
		/**
		 * @author Anil , Date : 09-07-2019
		 */
		ObjectifyService.reset();
		logger.log(Level.SEVERE, "Objectify Consistency Strong");
		ofy().consistency(Consistency.STRONG);
		ofy().clear();
		
		
		/***
		 * 
		 */
		
		if (type!=null && type.equals(PARAMETER_TYPE_PHOTO)) {
			
			String fileName=req.getParameter(IMAGE_TYPE_IMAGE.trim());
			logger.log(Level.SEVERE, "Inside "+ PARAMETER_TYPE_PHOTO);

				
				DocumentUpload upload=new DocumentUpload();
				try {
				String companyUrl="";
				if(company.getCompanyURL().contains("http://")){
					companyUrl=company.getCompanyURL().replace("http://", " ");
				}else{
					companyUrl=company.getCompanyURL();
				}
				if(company.getCompanyURL().contains("/")){
					companyUrl=companyUrl.replace("/", "").trim();
				}else{
					companyUrl=companyUrl.trim();
				}
				/**
				 * @author Anil , Date:17-01-2020
				 * if company URl already contains '-dot-' then we should not replace first '.' by '-dot-'
				 */
				logger.log(Level.SEVERE, "Company URL : "+ companyUrl);
				if(!companyUrl.contains("-dot-")){
					companyUrl = companyUrl.replaceFirst("\\.", "-dot-");
				}
			//	logger.log(Level.SEVERE,"URL::::::::::::"+"http://"+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			//	upload.setUrl(httpString+""+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			//	upload.setName(fileName);
			//	upload.setStatus(true);
			String url=httpString+""+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey;
			logger.log(Level.SEVERE,"URL::::::::::::"+url);
				
				if(multipleServiceIds!=null && !multipleServiceIds.equals("")) {
					Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
							.param("OperationName", "updateCustomerSignatureInService")
							.param("companyId", companyId+ "")
							.param("url", url )
							.param("filename", fileName)
							.param("multipleServiceIds", multipleServiceIds).etaMillis(System.currentTimeMillis()+60000));
					logger.log(Level.SEVERE,"task queue called");	
				}else {
					Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
							.param("OperationName", "updateCustomerSignatureInService")
							.param("companyId", companyId+ "")
							.param("url", url )
							.param("filename", fileName)
							.param("serviceId", issueId).etaMillis(System.currentTimeMillis()+60000));
					logger.log(Level.SEVERE,"task queue called");	
				}
		
			} catch (Exception e) {
				e.printStackTrace();
			}
//			
//				
//			if(multipleServiceIds!=null && !multipleServiceIds.equals("")) {
//				logger.log(Level.SEVERE, "For multiple service completion");
//
//				ArrayList<Integer> serviceidarray = new ArrayList<Integer>();
////				List<String> serviceidlist = Arrays.asList(multipleServiceIds); 
//				String [] serviceidlist = multipleServiceIds.split("\\,");
//				for(int i=0; i<serviceidlist.length;i++) {
//					logger.log(Level.SEVERE,"serviceidlist"+serviceidlist[i]);
//					serviceidlist[i].trim().replaceAll("\"", "");
//					String str = serviceidlist[i].trim().replaceAll("^\\[|]$", "");
//					logger.log(Level.SEVERE,"str"+str.trim());
//					serviceidarray.add(Integer.parseInt(str.trim()));
//				}
//				List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId).filter("count IN", serviceidarray).list();
//				logger.log(Level.SEVERE, "servicelist size "+servicelist.size());
//				if(servicelist.size()>0) {
//					for(Service serviceEntity : servicelist) {
//						serviceEntity.setCustomerSignature(upload);
//					}
//					ofy().save().entities(servicelist);
//					logger.log(Level.SEVERE, "Services updated with customer signature");
//					logger.log(Level.SEVERE, "SERVICE UPDATED "+ new Date());
//				}
//			}
//			else {
//				logger.log(Level.SEVERE, "inside Single service completion");
//				Service service =
//						 ofy().load().type(Service.class).filter("companyId", companyId).filter("count", Integer.parseInt(issueId)).first().now();
//				logger.log(Level.SEVERE,"service::::::::::::"+service);
//				service.setCustomerSignature(upload);
//				ofy().save().entity(service);
//				logger.log(Level.SEVERE, "SERVICE UPDATED : "+ new Date());
//				
//			}
			
			logger.log(Level.SEVERE, "end here");

		}else if(type!=null && type.equals(PARAMETER_EXPENSE_TYPE_PHOTO)){
			String fileName=req.getParameter(IMAGE_TYPE_IMAGE.trim());
			MultipleExpenseMngt expense=ofy().load().type(MultipleExpenseMngt.class).filter("companyId", companyId).filter("count",Integer.parseInt(issueId)).first().now();
			logger.log(Level.SEVERE,"expense::::::::::::"+expense);
			logger.log(Level.SEVERE,"fileName::::::::::::"+fileName);
			
			DocumentUpload upload=new DocumentUpload();
			String companyUrl="";
			if(company.getCompanyURL().contains("http://")){
				companyUrl=company.getCompanyURL().replace("http://", " ");
			}else{
				companyUrl=company.getCompanyURL();
			}
			if(company.getCompanyURL().contains("/")){
				companyUrl=companyUrl.replace("/", "").trim();
			}else{
				companyUrl=companyUrl.trim();
			}
			/**
			 * @author Anil , Date:17-01-2020
			 * if company URl already contains '-dot-' then we should not replace first '.' by '-dot-'
			 */
			if(!companyUrl.contains("-dot-")){
				companyUrl = companyUrl.replaceFirst("\\.", "-dot-");
			}
			logger.log(Level.SEVERE,"URL::::::::::::"+"http://"+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setUrl(httpString+""+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setName(fileName);
			upload.setStatus(true);
			
			expense.setExpenseDocument(upload);
			ofy().save().entity(expense);
		}else if(type!=null && type.equals(PARAMETER_ASSESMENT_TYPE_PHOTO)){
			String fileName=req.getParameter(IMAGE_TYPE_IMAGE.trim());
			AssesmentReport assesmentReport=ofy().load().type(AssesmentReport.class).filter("companyId", companyId).filter("count",Integer.parseInt(issueId)).first().now();
			logger.log(Level.SEVERE,"assesmentReport::::::::::::"+assesmentReport);
			logger.log(Level.SEVERE,"fileName::::::::::::"+fileName);
			
			DocumentUpload upload=new DocumentUpload();
			String companyUrl="";
			if(company.getCompanyURL().contains("http://")){
				companyUrl=company.getCompanyURL().replace("http://", " ");
			}else{
				companyUrl=company.getCompanyURL();
			}
			if(company.getCompanyURL().contains("/")){
				companyUrl=companyUrl.replace("/", "").trim();
			}else{
				companyUrl=companyUrl.trim();
			}
			/**
			 * @author Anil , Date:17-01-2020
			 * if company URl already contains '-dot-' then we should not replace first '.' by '-dot-'
			 */
			if(!companyUrl.contains("-dot-")){
				companyUrl = companyUrl.replaceFirst("\\.", "-dot-");
			}
			logger.log(Level.SEVERE,"URL::::::::::::"+"http://"+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setUrl(httpString+""+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setName(fileName);
			upload.setStatus(true);
			
			assesmentReport.setAssessmentUpload(upload);
			ofy().save().entity(assesmentReport);
		}else if(type!=null && type.equals(PARAMETER_FINGER_DATA)){
			String fileName=req.getParameter(IMAGE_TYPE_IMAGE.trim());
			String companyUrl="";
			if(company.getCompanyURL().contains("http://")){
				companyUrl=company.getCompanyURL().replace("http://", " ");
			}else{
				companyUrl=company.getCompanyURL();
			}
			if(company.getCompanyURL().contains("/")){
				companyUrl=companyUrl.replace("/", "").trim();
			}else{
				companyUrl=companyUrl.trim();
			}
			/**
			 * @author Anil , Date:17-01-2020
			 * if company URl already contains '-dot-' then we should not replace first '.' by '-dot-'
			 */
			if(!companyUrl.contains("-dot-")){
				companyUrl = companyUrl.replaceFirst("\\.", "-dot-");
			}
			EmployeeFingerDB employeeDB=ofy().load().type(EmployeeFingerDB.class).filter("companyId", company.getCompanyId()).filter("empId",Integer.parseInt(issueId)).first().now();
			if(employeeDB!=null){
				employeeDB.setIsoTemplateUrl(httpString+""+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
				Employee employee=ofy().load().type(Employee.class).filter("companyId", company.getCompanyId()).filter("count", Integer.parseInt(issueId)).first().now();
				employee.setFingerDataPresent(true);
				ofy().save().entity(employee);
				ofy().save().entity(employeeDB);
			}else{
				employeeDB=new EmployeeFingerDB();
				employeeDB.setCompanyId(company.getCompanyId());
				employeeDB.setEmpId(Integer.parseInt(issueId));
				employeeDB.setIsoTemplateUrl("http://"+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
				Employee employee=ofy().load().type(Employee.class).filter("companyId", company.getCompanyId()).filter("count", Integer.parseInt(issueId)).first().now();
				employee.setFingerDataPresent(true);
				ofy().save().entity(employee);
				ofy().save().entity(employeeDB);
			}
			logger.log(Level.SEVERE,"URL::::::::::::"+"http://"+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);			
		}
		/**
		 * 
		 * 
		 */
		else if (type!=null && type.equals(PARAMETER_SERVICE_PHOTO1)) {
			String fileName=req.getParameter(IMAGE_TYPE_IMAGE.trim());
			Service service =ofy().load().type(Service.class).filter("companyId", companyId).filter("count", Integer.parseInt(issueId)).first().now();
			logger.log(Level.SEVERE,"service::::::::::::"+service+" :: Service Photo 1");
			DocumentUpload upload=new DocumentUpload();
			String companyUrl="";
			if(company.getCompanyURL().contains("http://")){
				companyUrl=company.getCompanyURL().replace("http://", " ");
			}else{
				companyUrl=company.getCompanyURL();
			}
			if(company.getCompanyURL().contains("/")){
				companyUrl=companyUrl.replace("/", "").trim();
			}else{
				companyUrl=companyUrl.trim();
			}
			/**
			 * @author Anil , Date:17-01-2020
			 * if company URl already contains '-dot-' then we should not replace first '.' by '-dot-'
			 */
			if(!companyUrl.contains("-dot-")){
				companyUrl = companyUrl.replaceFirst("\\.", "-dot-");
			}
			logger.log(Level.SEVERE,"URL::::::::::::"+"http://"+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setUrl(httpString+""+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setName(fileName);
			upload.setStatus(true);
			service.setServiceImage1(upload);
			
			ofy().save().entity(service); 
		}else if (type!=null && type.equals(PARAMETER_SERVICE_PHOTO2)) {
			String fileName=req.getParameter(IMAGE_TYPE_IMAGE.trim());
			Service service =ofy().load().type(Service.class).filter("companyId", companyId).filter("count", Integer.parseInt(issueId)).first().now();
			logger.log(Level.SEVERE,"service::::::::::::"+service+" :: Service Photo 2");
			DocumentUpload upload=new DocumentUpload();
			String companyUrl="";
			if(company.getCompanyURL().contains("http://")){
				companyUrl=company.getCompanyURL().replace("http://", " ");
			}else{
				companyUrl=company.getCompanyURL();
			}
			if(company.getCompanyURL().contains("/")){
				companyUrl=companyUrl.replace("/", "").trim();
			}else{
				companyUrl=companyUrl.trim();
			}
			/**
			 * @author Anil , Date:17-01-2020
			 * if company URl already contains '-dot-' then we should not replace first '.' by '-dot-'
			 */
			if(!companyUrl.contains("-dot-")){
				companyUrl = companyUrl.replaceFirst("\\.", "-dot-");
			}
			logger.log(Level.SEVERE,"URL::::::::::::"+"http://"+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setUrl(httpString+""+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setName(fileName);
			upload.setStatus(true);
			service.setServiceImage2(upload);
			
			ofy().save().entity(service); 
		}else if (type!=null && type.equals(PARAMETER_SERVICE_PHOTO3)) {
			String fileName=req.getParameter(IMAGE_TYPE_IMAGE.trim());
			Service service =ofy().load().type(Service.class).filter("companyId", companyId).filter("count", Integer.parseInt(issueId)).first().now();
			logger.log(Level.SEVERE,"service::::::::::::"+service+" :: Service Photo 3");
			DocumentUpload upload=new DocumentUpload();
			String companyUrl="";
			if(company.getCompanyURL().contains("http://")){
				companyUrl=company.getCompanyURL().replace("http://", " ");
			}else{
				companyUrl=company.getCompanyURL();
			}
			if(company.getCompanyURL().contains("/")){
				companyUrl=companyUrl.replace("/", "").trim();
			}else{
				companyUrl=companyUrl.trim();
			}
			/**
			 * @author Anil , Date:17-01-2020
			 * if company URl already contains '-dot-' then we should not replace first '.' by '-dot-'
			 */
			if(!companyUrl.contains("-dot-")){
				companyUrl = companyUrl.replaceFirst("\\.", "-dot-");
			}
			logger.log(Level.SEVERE,"URL::::::::::::"+"http://"+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setUrl(httpString+""+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setName(fileName);
			upload.setStatus(true);
			service.setServiceImage3(upload);
			
			ofy().save().entity(service); 
		}else if (type!=null && type.equals(PARAMETER_SERVICE_PHOTO4)) {
			String fileName=req.getParameter(IMAGE_TYPE_IMAGE.trim());
			Service service =ofy().load().type(Service.class).filter("companyId", companyId).filter("count", Integer.parseInt(issueId)).first().now();
			logger.log(Level.SEVERE,"service::::::::::::"+service+" :: Service Photo 4");
			DocumentUpload upload=new DocumentUpload();
			String companyUrl="";
			if(company.getCompanyURL().contains("http://")){
				companyUrl=company.getCompanyURL().replace("http://", " ");
			}else{
				companyUrl=company.getCompanyURL();
			}
			if(company.getCompanyURL().contains("/")){
				companyUrl=companyUrl.replace("/", "").trim();
			}else{
				companyUrl=companyUrl.trim();
			}
			/**
			 * @author Anil , Date:17-01-2020
			 * if company URl already contains '-dot-' then we should not replace first '.' by '-dot-'
			 */
			if(!companyUrl.contains("-dot-")){
				companyUrl = companyUrl.replaceFirst("\\.", "-dot-");
			}
			logger.log(Level.SEVERE,"URL::::::::::::"+"http://"+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setUrl(httpString+""+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setName(fileName);
			upload.setStatus(true);
			service.setServiceImage4(upload);
			
			ofy().save().entity(service); 
		}
		
		
		else if (type!=null && type.equals(PARAMETER_SERVICE_PHOTO5)) {
			String fileName=req.getParameter(IMAGE_TYPE_IMAGE.trim());
			Service service =ofy().load().type(Service.class).filter("companyId", companyId).filter("count", Integer.parseInt(issueId)).first().now();
			logger.log(Level.SEVERE,"service::::::::::::"+service+" :: Service Photo 5");
			DocumentUpload upload=new DocumentUpload();
			String companyUrl="";
			if(company.getCompanyURL().contains("http://")){
				companyUrl=company.getCompanyURL().replace("http://", " ");
			}else{
				companyUrl=company.getCompanyURL();
			}
			if(company.getCompanyURL().contains("/")){
				companyUrl=companyUrl.replace("/", "").trim();
			}else{
				companyUrl=companyUrl.trim();
			}
			/**
			 * @author Anil , Date:17-01-2020
			 * if company URl already contains '-dot-' then we should not replace first '.' by '-dot-'
			 */
			if(!companyUrl.contains("-dot-")){
				companyUrl = companyUrl.replaceFirst("\\.", "-dot-");
			}
			logger.log(Level.SEVERE,"URL::::::::::::"+"http://"+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setUrl(httpString+""+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setName(fileName);
			upload.setStatus(true);
			service.setServiceImage5(upload);
			
			ofy().save().entity(service); 
		}else if (type!=null && type.equals(PARAMETER_SERVICE_PHOTO6)) {
			String fileName=req.getParameter(IMAGE_TYPE_IMAGE.trim());
			Service service =ofy().load().type(Service.class).filter("companyId", companyId).filter("count", Integer.parseInt(issueId)).first().now();
			logger.log(Level.SEVERE,"service::::::::::::"+service+" :: Service Photo 6");
			DocumentUpload upload=new DocumentUpload();
			String companyUrl="";
			if(company.getCompanyURL().contains("http://")){
				companyUrl=company.getCompanyURL().replace("http://", " ");
			}else{
				companyUrl=company.getCompanyURL();
			}
			if(company.getCompanyURL().contains("/")){
				companyUrl=companyUrl.replace("/", "").trim();
			}else{
				companyUrl=companyUrl.trim();
			}
			/**
			 * @author Anil , Date:17-01-2020
			 * if company URl already contains '-dot-' then we should not replace first '.' by '-dot-'
			 */
			if(!companyUrl.contains("-dot-")){
				companyUrl = companyUrl.replaceFirst("\\.", "-dot-");
			}
			logger.log(Level.SEVERE,"URL::::::::::::"+"http://"+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setUrl(httpString+""+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setName(fileName);
			upload.setStatus(true);
			service.setServiceImage6(upload);
			
			ofy().save().entity(service); 
		}else if (type!=null && type.equals(PARAMETER_SERVICE_PHOTO7)) {
			String fileName=req.getParameter(IMAGE_TYPE_IMAGE.trim());
			Service service =ofy().load().type(Service.class).filter("companyId", companyId).filter("count", Integer.parseInt(issueId)).first().now();
			logger.log(Level.SEVERE,"service::::::::::::"+service+" :: Service Photo 7");
			DocumentUpload upload=new DocumentUpload();
			String companyUrl="";
			if(company.getCompanyURL().contains("http://")){
				companyUrl=company.getCompanyURL().replace("http://", " ");
			}else{
				companyUrl=company.getCompanyURL();
			}
			if(company.getCompanyURL().contains("/")){
				companyUrl=companyUrl.replace("/", "").trim();
			}else{
				companyUrl=companyUrl.trim();
			}
			/**
			 * @author Anil , Date:17-01-2020
			 * if company URl already contains '-dot-' then we should not replace first '.' by '-dot-'
			 */
			if(!companyUrl.contains("-dot-")){
				companyUrl = companyUrl.replaceFirst("\\.", "-dot-");
			}
			logger.log(Level.SEVERE,"URL::::::::::::"+"http://"+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setUrl(httpString+""+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setName(fileName);
			upload.setStatus(true);
			service.setServiceImage7(upload);
			
			ofy().save().entity(service); 
		}else if (type!=null && type.equals(PARAMETER_SERVICE_PHOTO8)) {
			String fileName=req.getParameter(IMAGE_TYPE_IMAGE.trim());
			Service service =ofy().load().type(Service.class).filter("companyId", companyId).filter("count", Integer.parseInt(issueId)).first().now();
			logger.log(Level.SEVERE,"service::::::::::::"+service+" :: Service Photo 8");
			DocumentUpload upload=new DocumentUpload();
			String companyUrl="";
			if(company.getCompanyURL().contains("http://")){
				companyUrl=company.getCompanyURL().replace("http://", " ");
			}else{
				companyUrl=company.getCompanyURL();
			}
			if(company.getCompanyURL().contains("/")){
				companyUrl=companyUrl.replace("/", "").trim();
			}else{
				companyUrl=companyUrl.trim();
			}
			/**
			 * @author Anil , Date:17-01-2020
			 * if company URl already contains '-dot-' then we should not replace first '.' by '-dot-'
			 */
			if(!companyUrl.contains("-dot-")){
				companyUrl = companyUrl.replaceFirst("\\.", "-dot-");
			}
			logger.log(Level.SEVERE,"URL::::::::::::"+"http://"+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setUrl(httpString+""+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setName(fileName);
			upload.setStatus(true);
			service.setServiceImage8(upload);
			
			ofy().save().entity(service); 
		}else if (type!=null && type.equals(PARAMETER_SERVICE_PHOTO10)) {
			String fileName=req.getParameter(IMAGE_TYPE_IMAGE.trim());
			Service service =ofy().load().type(Service.class).filter("companyId", companyId).filter("count", Integer.parseInt(issueId)).first().now();
			logger.log(Level.SEVERE,"service::::::::::::"+service+" :: Service Photo 10");
			DocumentUpload upload=new DocumentUpload();
			String companyUrl="";
			if(company.getCompanyURL().contains("http://")){
				companyUrl=company.getCompanyURL().replace("http://", " ");
			}else{
				companyUrl=company.getCompanyURL();
			}
			if(company.getCompanyURL().contains("/")){
				companyUrl=companyUrl.replace("/", "").trim();
			}else{
				companyUrl=companyUrl.trim();
			}
			/**
			 * @author Anil , Date:17-01-2020
			 * if company URl already contains '-dot-' then we should not replace first '.' by '-dot-'
			 */
			if(!companyUrl.contains("-dot-")){
				companyUrl = companyUrl.replaceFirst("\\.", "-dot-");
			}
			logger.log(Level.SEVERE,"URL::::::::::::"+"http://"+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setUrl(httpString+""+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setName(fileName);
			upload.setStatus(true);
			service.setServiceImage10(upload);
			
			ofy().save().entity(service); 
		}else if (type!=null && type.equals(PARAMETER_SERVICE_PHOTO9)) {
			String fileName=req.getParameter(IMAGE_TYPE_IMAGE.trim());
			Service service =ofy().load().type(Service.class).filter("companyId", companyId).filter("count", Integer.parseInt(issueId)).first().now();
			logger.log(Level.SEVERE,"service::::::::::::"+service+" :: Service Photo 9");
			DocumentUpload upload=new DocumentUpload();
			String companyUrl="";
			if(company.getCompanyURL().contains("http://")){
				companyUrl=company.getCompanyURL().replace("http://", " ");
			}else{
				companyUrl=company.getCompanyURL();
			}
			if(company.getCompanyURL().contains("/")){
				companyUrl=companyUrl.replace("/", "").trim();
			}else{
				companyUrl=companyUrl.trim();
			}
			/**
			 * @author Anil , Date:17-01-2020
			 * if company URl already contains '-dot-' then we should not replace first '.' by '-dot-'
			 */
			if(!companyUrl.contains("-dot-")){
				companyUrl = companyUrl.replaceFirst("\\.", "-dot-");
			}
			logger.log(Level.SEVERE,"URL::::::::::::"+"http://"+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setUrl(httpString+""+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setName(fileName);
			upload.setStatus(true);
			service.setServiceImage9(upload);
			
			ofy().save().entity(service); 
		}
		
		else if (type!=null && (type.equals(PARAMETER_SERVICE_SUMMARY1)||type.equals(PARAMETER_SERVICE_SUMMARY2))) {
			String fileName=req.getParameter(IMAGE_TYPE_IMAGE.trim());
			Service service =ofy().load().type(Service.class).filter("companyId", companyId).filter("count", Integer.parseInt(issueId)).first().now();
			logger.log(Level.SEVERE,"service::::::::::::"+service+" :: Service Summary");
			DocumentUpload upload=new DocumentUpload();
			String companyUrl="";
			if(company.getCompanyURL().contains("http://")){
				companyUrl=company.getCompanyURL().replace("http://", " ");
			}else{
				companyUrl=company.getCompanyURL();
			}
			if(company.getCompanyURL().contains("/")){
				companyUrl=companyUrl.replace("/", "").trim();
			}else{
				companyUrl=companyUrl.trim();
			}
			/**
			 * @author Anil , Date:17-01-2020
			 * if company URl already contains '-dot-' then we should not replace first '.' by '-dot-'
			 */
			if(!companyUrl.contains("-dot-")){
				companyUrl = companyUrl.replaceFirst("\\.", "-dot-");
			}
			logger.log(Level.SEVERE,"URL::::::::::::"+"http://"+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setUrl(httpString+""+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setName(fileName);
			upload.setStatus(true);
			
			if(type.equals(PARAMETER_SERVICE_SUMMARY1)){
				service.setUptestReport(upload);
				service.setDocketUploadedByApp(true);
			}else if(type.equals(PARAMETER_SERVICE_SUMMARY2)){
				service.setServiceSummaryReport2(upload);
			}
			
			ofy().save().entity(service); 
		}else if(type!=null && type.equals(EMPLOYEE_PHOTO)){
			String fileName=req.getParameter(IMAGE_TYPE_IMAGE.trim());
			String companyUrl="";
			if(company.getCompanyURL().contains("http://")){
				companyUrl=company.getCompanyURL().replace("http://", " ");
			}else{
				companyUrl=company.getCompanyURL();
			}
			if(company.getCompanyURL().contains("/")){
				companyUrl=companyUrl.replace("/", "").trim();
			}else{
				companyUrl=companyUrl.trim();
			}
			/**
			 * @author Anil , Date:17-01-2020
			 * if company URl already contains '-dot-' then we should not replace first '.' by '-dot-'
			 */
			if(!companyUrl.contains("-dot-")){
				companyUrl = companyUrl.replaceFirst("\\.", "-dot-");
			}
			Employee employee=ofy().load().type(Employee.class).filter("companyId", company.getCompanyId()).filter("count", Integer.parseInt(issueId)).first().now();
			if(employee!=null){
				logger.log(Level.SEVERE,"URL::::::::::::"+"http://"+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);	
				
				DocumentUpload upload=new DocumentUpload();
				upload.setUrl(httpString+""+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
				upload.setName(fileName);
				upload.setStatus(true);
				employee.setPhoto(upload);
				ofy().save().entity(employee);
				
			}
		}else if(type!=null && type.equals(ASSESSMENT_TASK_PHOTO)){
			String fileName=req.getParameter(IMAGE_TYPE_IMAGE.trim());
			AssesmentReport assesmentReport=ofy().load().type(AssesmentReport.class).filter("companyId", companyId).filter("count",Integer.parseInt(issueId)).first().now();
			logger.log(Level.SEVERE,"assesmentReport::::::::::::"+assesmentReport);
			logger.log(Level.SEVERE,"fileName::::::::::::"+fileName);
			
			DocumentUpload upload=new DocumentUpload();
			String companyUrl="";
			if(company.getCompanyURL().contains("http://")){
				companyUrl=company.getCompanyURL().replace("http://", " ");
			}else{
				companyUrl=company.getCompanyURL();
			}
			if(company.getCompanyURL().contains("/")){
				companyUrl=companyUrl.replace("/", "").trim();
			}else{
				companyUrl=companyUrl.trim();
			}
			/**
			 * @author Anil , Date:17-01-2020
			 * if company URl already contains '-dot-' then we should not replace first '.' by '-dot-'
			 */
			if(!companyUrl.contains("-dot-")){
				companyUrl = companyUrl.replaceFirst("\\.", "-dot-");
			}
			logger.log(Level.SEVERE,"URL::::::::::::"+"http://"+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setUrl(httpString+""+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setName(fileName);
			upload.setStatus(true);
			
			String finalURL=httpString+""+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey;
			int index=Integer.parseInt(boundary);
			
//			if(assesmentReport.getAssessmentDetailsLIst()!=null&&assesmentReport.getAssessmentDetailsLIst().size()!=0){
//				logger.log(Level.SEVERE,"AssessmentDetailsLIst size="+assesmentReport.getAssessmentDetailsLIst().size());
//				for(AssesmentReportEmbedForTable obj:assesmentReport.getAssessmentDetailsLIst()){
//					logger.log(Level.SEVERE,"in for");
//					if(obj.getCount()==index){
//						logger.log(Level.SEVERE,"in if");
//						obj.setDocUpload(upload);
//						logger.log(Level.SEVERE,"doc upload set");
//						break;
//					}
//				}
//			}
			res.getWriter().write(finalURL);
			logger.log(Level.SEVERE,"final url sent to app");
			
			ofy().save().entity(assesmentReport);
			logger.log(Level.SEVERE,"image saved");
		}
		else if(type!=null && type.equals(CUSTOMER_SIGNATURE_FOR_AUDIT)){
			String fileName=req.getParameter(IMAGE_TYPE_IMAGE.trim());
			AssesmentReport assesmentReport=ofy().load().type(AssesmentReport.class).filter("companyId", companyId).filter("count",Integer.parseInt(issueId)).first().now();
			logger.log(Level.SEVERE,"assesmentReport::::::::::::"+assesmentReport);
			logger.log(Level.SEVERE,"fileName::::::::::::"+fileName);
			
			DocumentUpload upload=new DocumentUpload();
			String companyUrl="";
			if(company.getCompanyURL().contains("http://")){
				companyUrl=company.getCompanyURL().replace("http://", " ");
			}else{
				companyUrl=company.getCompanyURL();
			}
			if(company.getCompanyURL().contains("/")){
				companyUrl=companyUrl.replace("/", "").trim();
			}else{
				companyUrl=companyUrl.trim();
			}
			/**
			 * @author Anil , Date:17-01-2020
			 * if company URl already contains '-dot-' then we should not replace first '.' by '-dot-'
			 */
			if(!companyUrl.contains("-dot-")){
				companyUrl = companyUrl.replaceFirst("\\.", "-dot-");
			}
			logger.log(Level.SEVERE,"URL::::::::::::"+"http://"+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setUrl(httpString+""+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey);
			upload.setName(fileName);
			upload.setStatus(true);
			assesmentReport.setCustomerSignature(upload);
//			String finalURL=httpString+""+companyUrl+"/slick_erp/downloadMapImage?blobkey="+blobKey;
//			int index=Integer.parseInt(boundary);
			

//			res.getWriter().write(finalURL);
//			logger.log(Level.SEVERE,"final url sent to app");
			
			ofy().save().entity(assesmentReport);
			logger.log(Level.SEVERE,"customer signature saved in assessment report!");
		}
		
	}
	
	

}
