package com.slicktechnologies.server.android;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;

public class SendSMS extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1522643088907923863L;
	
	Logger logger = Logger.getLogger("Logger");
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		resp.setHeader("Access-Control-Allow-Origin", "*");

		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		String jsonString = req.getParameter("smsDetails");
		logger.log(Level.SEVERE, "jsonString::::::::::" + jsonString);
		
		String urlcalled=req.getRequestURL().toString().trim();
		logger.log(Level.SEVERE,"urlcalled:::::"+urlcalled);
		
		String url1=urlcalled.replace("http://", "");
		logger.log(Level.SEVERE,"url1:::::"+url1);
		String[] splitUrl=url1.split("\\.");
		logger.log(Level.SEVERE,"splitUrl[0]:::::"+splitUrl[0]);
		
		Company company=ofy().load().type(Company.class).filter("accessUrl","my").first().now();
		logger.log(Level.SEVERE,"company "+company);

		try {
			JSONObject	jsonobj = new JSONObject(jsonString);
			String smsEvent = jsonobj.optString("SMSEvent");
//			long companyId = jsonobj.optLong("CompanyId");
			long MobileNo = jsonobj.optLong("MobileNo");
			
			long companyId = company.getCompanyId();

			int customerId = 0;
			try {
				customerId = jsonobj.optInt("customerID");
			} catch (Exception e) {
			}
			
//			SmsConfiguration smsconfig = ofy().load().type(SmsConfiguration.class).filter("companyId",companyId).filter("status", true).first().now();
//			logger.log(Level.SEVERE, "smsconfig "+ smsconfig);
//
//			if(smsconfig!=null){
				
				if(smsEvent!=null && !smsEvent.equals("")){
					
					/**
					 * @author Vijay Date :- 09-08-2021
					 * Des :- added below method to check customer DND status if customer DND status is Active 
					 * then SMS will not send to customer
					 */
					ServerAppUtility serverapputility = new ServerAppUtility();
					boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(customerId, companyId);
					logger.log(Level.SEVERE, "dndstatusFlag "+ dndstatusFlag);
					if(!dndstatusFlag){
						
					SmsTemplate smsTemplateEntity = ofy().load().type(SmsTemplate.class).filter("companyId",companyId).filter("event",smsEvent).filter("status",true).first().now();
					logger.log(Level.SEVERE, "smsTemplateEntity "+ smsTemplateEntity);
					if(smsTemplateEntity!=null){
						String actualmsg = getSMSMessage(smsEvent,smsTemplateEntity,jsonobj);
						logger.log(Level.SEVERE, "actualmsg "+ actualmsg);
						if(actualmsg!=null){
							SmsServiceImpl smsimpl = new SmsServiceImpl();
//							int value =  smsimpl.sendSmsToClient(actualmsg, MobileNo, smsconfig.getAccountSID(),  smsconfig.getAuthToken(),  smsconfig.getPassword(), companyId);
//							if(value==1) {
//								resp.getWriter().println("SMS Sent Successfully");
//							}
							
							/**
							 * @author Vijay Date :- 29-04-2022
							 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
							 * whats and sms based on communication channel configuration.
							 */
							smsimpl.sendMessage(AppConstants.PEDIO,AppConstants.PEDIO,smsTemplateEntity.getEvent(),smsTemplateEntity.getCompanyId(),actualmsg,MobileNo,false);
							logger.log(Level.SEVERE,"after sendMessage method");
							resp.getWriter().println("Message Sent Successfully");

						}

					}
					else{
						resp.getWriter().println("SMS template either not defined in the ERP System or SMS template status is Inactive in the ERP system");
					}
					}
					else{
						resp.getWriter().println("Can not send SMS customer DND status is active");
					}
					
				}
				else{
					resp.getWriter().println("SMSEvent can not be blank");
				}
//			}
//			else{
//				resp.getWriter().println("SMS configuration is not active in ERP System");
//			}
			
			
		} catch (JSONException e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Exception =="+ e.getLocalizedMessage());

		}
	}

	private String getSMSMessage(String smsEvent, SmsTemplate smsTemplateEntity,JSONObject jsonobj) {
		
		String smstemplatemsg = smsTemplateEntity.getMessage();
		
		if(smsEvent.equals("Service Completion OTP")){
			String flag = jsonobj.optString("Flag");
			String actualMsg = "";
			
			if(flag.trim().equalsIgnoreCase("NBHC")) {
				String OTP = jsonobj.optString("OTP");
				String strlink = jsonobj.optString("Link");
				logger.log(Level.SEVERE, "strlink"+strlink);
				strlink = strlink.replace("fslash", "/");
				logger.log(Level.SEVERE, "strlink = "+strlink);
				String tinyurl = "https://"+strlink;
				logger.log(Level.SEVERE, "tinyurl"+tinyurl);

				String msgOtp = smstemplatemsg.replace("{OTP}",OTP);
				actualMsg = msgOtp.replace("{Link}",tinyurl);
			}
			else {
				String customerName = jsonobj.optString("CustomerName");
				String OTP = jsonobj.optString("OTP");
				String companyName = jsonobj.optString("CompanyName");
				
				String msgCustomerName = smstemplatemsg.replace("{CustomerName}", customerName);
				String msgOtp = msgCustomerName.replace("{OTP}",OTP);
				actualMsg = msgOtp.replace("{CompanyName}",companyName);
			}
			
			
			return actualMsg;
		}
		else if(smsEvent.equals("Service Reschedule OTP")){
			
			String serviceId = jsonobj.optString("ServiceId");
			String customerName = jsonobj.optString("CustomerName");
			String CustomerCellNo = jsonobj.optString("CustomerCellNo");
			String RescheduleDate = jsonobj.optString("RescheduleDate");
			String OTP = jsonobj.optString("OTP");
			String TechnicianName = jsonobj.optString("TechnicianName");
			
			String msgserviceId = smstemplatemsg.replace("{ServiceId}", serviceId);
			String msgcustomerName = msgserviceId.replace("{CustomerName}",customerName);
			String msgCustomerCellNo = msgcustomerName.replace("{CustomerCellNo}",CustomerCellNo);
			String msgRescheduleDate = msgCustomerCellNo.replace("{RescheduleDate}",RescheduleDate);
			String msgOTP = msgRescheduleDate.replace("{OTP}",OTP);
			String actualMsg = msgOTP.replace("{TechnicianName}",TechnicianName);

			return actualMsg;
		}
		else if(smsEvent.equals("StartServiceCustomer")){
			
			String customerName = jsonobj.optString("CustomerName");
			String TechnicianName = jsonobj.optString("TechnicianName");
			String serviceId = jsonobj.optString("ServiceId");

			String ProductName = jsonobj.optString("ProductName");
			String ServiceNumber = jsonobj.optString("ServiceNumber");
			String CompanyCellNo = jsonobj.optString("CompanyCellNo");
			
			
			String msgcustomerName = smstemplatemsg.replace("{CustomerName}", customerName);
			String msgTechnicianName = msgcustomerName.replace("{TechnicianName}",TechnicianName);
			String msgServiceId = msgTechnicianName.replace("{ServiceId}",serviceId);
			String msgProductName = msgServiceId.replace("{ProductName}",ProductName);
			String msgServiceNumber = msgProductName.replace("{ServiceNumber}",ServiceNumber);
			String actualMsg = msgServiceNumber.replace("{CompanyCellNo}",CompanyCellNo);
			
			return actualMsg;
		}
		else if(smsEvent.equals("CompleteServiceCustomer")){
			String customerName = jsonobj.optString("CustomerName");
			String serviceId = jsonobj.optString("ServiceId");
			String ProductName = jsonobj.optString("ProductName");
			String TechnicianName = jsonobj.optString("TechnicianName");
			String serviceDate = jsonobj.optString("Date");
			String CompanyCellNo = jsonobj.optString("CompanyCellNo");
			
			
			String msgcustomerName = smstemplatemsg.replace("{CustomerName}", customerName);
			String msgServiceId = msgcustomerName.replace("{ServiceId}",serviceId);
			String msgProductName = msgServiceId.replace("{ProductName}",ProductName);
			String msgTechnicianName = msgProductName.replace("{TechnicianName}",TechnicianName);
			String msgserviceDate = msgTechnicianName.replace("{Date}",serviceDate);
			String actualMsg = msgserviceDate.replace("{CompanyCellNo}",CompanyCellNo);
			
			return actualMsg;
		}
		else if(smsEvent.equals("Logout OTP")){
			String OperatorName = jsonobj.optString("OperatorName");
			String OTP = jsonobj.optString("OTP");
			
			String msgOperatorName = smstemplatemsg.replace("{OperatorName}", OperatorName);
			String actualMsg = msgOperatorName.replace("{OTP}",OTP);
			return actualMsg;
		}
		return null;
	}

}
