package com.slicktechnologies.server.android;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.email.EmailDetails;
import com.slicktechnologies.shared.common.email.EmailTemplate;

public class EmailAPI extends HttpServlet {

	
	private static final long serialVersionUID = -8747003444060631650L;
	Logger logger = Logger.getLogger("EmailAPI");
	

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	
		logger.log(Level.SEVERE, "inside email api doGet");
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
			resp.setHeader("Access-Control-Allow-Origin", "*");
		
			logger.log(Level.SEVERE, "inside email api doPost");
			long companyId =0;
			try {
				companyId =Long.parseLong(req.getParameter("companyId"));
			}catch(Exception e) {
				JSONObject errorObj = new JSONObject();
				errorObj.put("Message","Parameter companyId missing in api call");
				String jsonString = errorObj.toJSONString();
				resp.getWriter().println(jsonString);
			}
			
			int assessmentID =0;
			try {
				assessmentID =Integer.parseInt(req.getParameter("documentID"));
			}catch(Exception e) {
				JSONObject errorObj = new JSONObject();
				errorObj.put("Message","Parameter assessmentID missing in api call");
				String jsonString = errorObj.toJSONString();
				resp.getWriter().println(jsonString);
			}
			
			String emailTemplateName = req.getParameter("emailTemplate");
			
			
			String entity=req.getParameter("entity");
			if(entity==null||entity.equals("")) {
				JSONObject errorObj = new JSONObject();
				errorObj.put("Message","Parameter entity missing in api call");
				String jsonString = errorObj.toJSONString();
				resp.getWriter().println(jsonString);				
			}
			
			Company comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
			if(comp==null) {
				
				JSONObject errorObj = new JSONObject();
				errorObj.put("Message","Unable to load Company");
				String jsonString = errorObj.toJSONString();
				resp.getWriter().println(jsonString);
			}else
				logger.log(Level.SEVERE, "company not null");
			
			AssesmentReport report = ofy().load().type(AssesmentReport.class).filter("companyId", companyId).filter("count", assessmentID).first().now();
			
			
			EmailDetails email = new EmailDetails();
			Customer cust=null;
			if(report!=null) {
				logger.log(Level.SEVERE, "report not null");
				cust = ofy().load().type(Customer.class).filter("companyId", companyId)
						.filter("count",report.getCinfo().getCount()).first().now();
			}else {
				JSONObject errorObj = new JSONObject();
				errorObj.put("Message","Unable to load Assessment, check assessmentId");
				String jsonString = errorObj.toJSONString();
				resp.getWriter().println(jsonString);
			}
			
			if(cust==null) {
				JSONObject errorObj = new JSONObject();
				errorObj.put("Message","Unable to load customer");
				String jsonString = errorObj.toJSONString();
				resp.getWriter().println(jsonString);
			}else
				logger.log(Level.SEVERE, "customer not null");
			EmailTemplate emailTemplate=null;
			if(emailTemplateName!=null&&!emailTemplateName.equals("")) {
				emailTemplate = ofy().load().type(EmailTemplate.class).filter("companyId", companyId)
						.filter("templateName", emailTemplateName)	
						.filter("templateStatus", true).first().now();
			}else {
				JSONObject errorObj = new JSONObject();
				errorObj.put("Message","Parameter emailTemplate missing in api call");
				String jsonString = errorObj.toJSONString();
				resp.getWriter().println(jsonString);
			}
				
			
			if(emailTemplate!=null) {
				logger.log(Level.SEVERE, "emailTemplate not null");
				email.setFromEmailid(comp.getEmail());
				ArrayList<String> toemailId = new ArrayList<String>();
				if(cust.getEmail()!=null&&!cust.getEmail().equals(""))
					toemailId.add(cust.getEmail());
				else {
						JSONObject errorObj = new JSONObject();
						errorObj.put("Message","Email id missing in customer");
						String jsonString = errorObj.toJSONString();
						resp.getWriter().println(jsonString);					
				}
					
				email.setToEmailId(toemailId);
				
				ArrayList<String> ccemailId = new ArrayList<String>();
				ArrayList<String> bccEmailId =new ArrayList<String>();
				if(ccemailId!=null && ccemailId.size()>0){
					System.out.println("inside cc >0"+ccemailId);
					email.setCcEmailId(ccemailId);
				}
				if(bccEmailId!=null && bccEmailId.size()>0){
					email.setBccEmailId(bccEmailId);
				}
				
				if(emailTemplate.getSubject()!=null && !emailTemplate.getSubject().equals("")){
					email.setSubject(emailTemplate.getSubject());
				}
				
				boolean digitalPaymentRequestLink = false;
				if(emailTemplate.getEmailBody()!=null && !emailTemplate.getEmailBody().equals("")){
					email.setEmailBody(emailTemplate.getEmailBody());
					String emailbody = emailTemplate.getEmailBody();
					String landingPageText = "";
					if(emailbody.contains("<Start_LandingPage_Text>") && emailbody.contains("</End_LandingPage_Text>")){
						try {
							String[] startLnadingpageText = emailbody.split("<Start_LandingPage_Text>");
							String[] endlandingPageText = startLnadingpageText[1].split("</End_LandingPage_Text>");
							landingPageText = endlandingPageText[0];
							emailbody = emailbody.replace("<Start_LandingPage_Text>", "");
							emailbody = emailbody.replace("</End_LandingPage_Text>", "");
							emailbody = emailbody.replace(landingPageText, "");
							
							email.setLandingPageText(landingPageText);
						} catch (Exception e) {
							// TODO: handle exception
						}

					}
					
					String resultString = emailbody.replaceAll("[\n]", "<br>");
					String resultString1 = resultString.replaceAll("[\\s]", "&nbsp;");
					System.out.println("resultString1 "+resultString1);
					email.setEmailBody(resultString1);
					if(resultString1.contains(AppConstants.EMAILPAYMENTGATEWAYREQUEST)||resultString1.contains(AppConstants.EMAILPAYMENTGATEWAYREQUESTLINK2)){
						Console.log("Email body contains digitalpaymentlink");
						digitalPaymentRequestLink = true;
						
						String Url = com.google.gwt.core.client.GWT.getModuleBaseURL(); 
						String Appid = "";
						if(Url.contains("-dot-")){
							String [] urlarray = Url.split("-dot-");
							 Appid = urlarray[1];
						}
						else{
							String [] urlarray = Url.split("\\.");
							 Appid = urlarray[1];
						}
						email.setAppId(Appid);
						if(!Url.contains("-dot-") && Url.contains(".")) {
							StringBuilder str = new StringBuilder();
							String [] strurl = Url.split("\\.");
							str.append(strurl[0]);
							str.append("-dot-");
							str.append(strurl[1]);
							str.append(".");
							str.append(strurl[2]);
							str.append(".");
							str.append(strurl[3]);
							Console.log("str == "+str);

							Console.log("strurl[1] "+strurl[1]);
							Console.log("strurl[2] "+strurl[2]);
							Console.log("strurl[3] "+strurl[3]);
							Console.log("strurl[3] "+strurl[4]);
							Console.log("strurl[3] "+strurl[5]);

							Url = str.toString();
							
							Url = Url.replace("http", "https");
							
							Console.log("final Url "+Url);

						}
						Console.log("Url =="+Url);
						email.setAppURL(Url);
					}
				}
				
			
				email.setPdfAttachment(true);
				email.setEntityName(entity);//("ASSESMENTREPORT");
				
				
				if(report!=null){
					email.setModel(report);
				}
				
				
				if(digitalPaymentRequestLink){
					final String pdfModuleBaseURL = com.google.gwt.core.client.GWT.getModuleBaseURL();
					email.setPdfURL(pdfModuleBaseURL);
				}
				Email emailAsync=new Email();
				emailAsync.sendEmail(email, comp.getCompanyId());
				
			}else {
				JSONObject errorObj = new JSONObject();
				errorObj.put("Message","Email template not found or inactive");
				String jsonString = errorObj.toJSONString();
				resp.getWriter().println(jsonString);
			}
			JSONObject errorObj = new JSONObject();
			errorObj.put("Message","Email Sent Successfully");
			String jsonString = errorObj.toJSONString();
			resp.getWriter().println(jsonString);
		}

}
