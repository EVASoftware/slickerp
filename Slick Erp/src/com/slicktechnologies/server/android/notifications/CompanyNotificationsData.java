package com.slicktechnologies.server.android.notifications;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class CompanyNotificationsData extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7078581085994985119L;

	Company comp;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String urlCalled=req.getRequestURL().toString().trim();
//		String url=urlCalled.replace("http://", "");
//		String[] splitUrl=url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		Exception exep = null;
		comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
		resp.setContentType("text/plain");
		JSONObject jsonData=new JSONObject();
		String appName=req.getParameter("applicationName");
		String response="";
		
		if(appName.trim().equalsIgnoreCase(AppConstants.IAndroid.EVA_PEDIO)){
			try {
				jsonData.put("fcmServerCode", comp.getFcmServerKeyForPedio());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				jsonData.put("fcmJSONFile", comp.getUploadPedioJson().getUrl());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(appName.trim().equalsIgnoreCase(AppConstants.IAndroid.EVA_KRETO)){
			try {
				jsonData.put("fcmServerCode", comp.getFcmServerKeyForKreto());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				jsonData.put("fcmJSONFile", comp.getUploadKretoJson().getUrl());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(appName.trim().equalsIgnoreCase(AppConstants.IAndroid.EVA_PRIORA)){
			try {
				jsonData.put("fcmServerCode", comp.getFcmServerKeyForPriora());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				jsonData.put("fcmJSONFile", comp.getUploadPrioraJson().getUrl());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			response="Notification will not work proper";
		}
		if(response.equals("")){
		response=jsonData.toString().replace("\\\\", "");
		}
		resp.getWriter().println(response);
	}
}
