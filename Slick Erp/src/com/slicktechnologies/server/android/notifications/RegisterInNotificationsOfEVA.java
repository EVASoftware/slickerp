//package com.slicktechnologies.server.android.notifications;
//
//import static com.googlecode.objectify.ObjectifyService.ofy;
//
//import java.io.IOException;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import com.slicktechnologies.server.RegisterServiceImpl;
//import com.slicktechnologies.shared.common.businessunitlayer.Company;
//import com.slicktechnologies.shared.common.notification.NotificationManager;
//import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
//
//public class RegisterInNotificationsOfEVA extends HttpServlet {
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 2751833147241482517L;
//	
//	@Override
//	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
//			throws ServletException, IOException {
//		doPost(req, resp);
//	}
//	
//	@Override
//	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
//			throws ServletException, IOException {
//		RegisterServiceImpl impl=new RegisterServiceImpl();
//		
//		String urlCalled=req.getRequestURL().toString().trim();
//		String url=urlCalled.replace("http://", "");
//		String[] splitUrl=url.split("\\.");
//		
//		Company comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();resp.setContentType("text/plain");
//		
//		String data=req.getParameter("data");
//		
//		JSONObject object = null;
//		try {
//			object = new JSONObject(data);
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		NotificationManager notificationManager=new NotificationManager();
//		NumberGeneration ng = new NumberGeneration();
//		ng = ofy().load().type(NumberGeneration.class)
//				.filter("companyId", comp.getCompanyId())
//				.filter("processName", "NotificationManager").filter("status", true)
//				.first().now();
//
//		long number = ng.getNumber();
//		int count = (int) number;
//		ng.setNumber(count+1);
//		ofy().save().entity(ng);
//		notificationManager.setApplicationName(object.optString(""));
//		notificationManager.setCount(count+1);
//		notificationManager.setRegisterDeviceCount(Integer.parseInt(object.optString("count").trim()));
//		notificationManager.setImeiNumber(Long.parseLong(object.optString("imeiNumber")));
//		notificationManager.setUserName(object.optString("userName"));
//		notificationManager.setRegId(object.optString("regId"));
//		notificationManager.setCompanyId(comp.getCompanyId());
//		notificationManager.setCompanyIdOfCompany(Long.parseLong(object.optString("companyId")));
//		notificationManager.setApplicationName(object.optString("applicationName"));
//		notificationManager.setEmployeeName(object.optString("employeeName"));
//		ofy().save().entity(notificationManager);
//		
//	}
//
//}
