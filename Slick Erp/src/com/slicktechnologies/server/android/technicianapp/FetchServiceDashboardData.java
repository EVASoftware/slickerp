package com.slicktechnologies.server.android.technicianapp;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.android.analytics.AnalyticsOperations;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
/**
 * Description : It fetch all services based on status i.e. Planned , Unplanned ,In Process , Completed and return
 					query result in json format.
 * @author pc1
 *
 */

public class FetchServiceDashboardData extends HttpServlet {

	Logger logger = Logger.getLogger("saveservicestatus.class");
	Company comp;
	
	
	private static final long serialVersionUID = -2557493125249724754L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
//		resp.setHeader("Access-Control-Allow-Origin", AppConstants.TRENDZWEBSOLUTION);
		resp.setHeader("Access-Control-Allow-Origin", "*");
		
		String urlCalled=req.getRequestURL().toString().trim();
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		try {
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			
			comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();	
			resp.setContentType("text/plain");
			String companyId=comp.getCompanyId()+"";
			String data=req.getParameter("data").trim();
			logger.log(Level.SEVERE, "data ::"+data);
			List<String> customerNameList = new ArrayList<String>();
			List<Integer> customerIdList = new ArrayList<Integer>();
			List<Long> customerCellList = new ArrayList<Long>();
			List<String> statusList = new ArrayList<String>();
			List<String> technicianList = new ArrayList<String>();
			List<String> branchList = new ArrayList<String>();
			JSONObject jsonData = null;
			
			try {
				jsonData = new JSONObject(data);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			JSONArray customerArray = null;
			try {
				customerArray = jsonData.getJSONArray("customer");
				for(int i=0; i< customerArray.length(); i++){
					JSONObject jObj = null;
					try {
						jObj = customerArray.getJSONObject(i);
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					customerIdList.add(Integer.parseInt(jObj.getString("id")));
					customerNameList.add(jObj.getString("name"));
					customerCellList.add(Long.parseLong(jObj.getString("cell")));					
				}
				
				/*
				 * Ashwini Patil
				 * Date:20-08-2024
				 * PSIPL reported issue that when they search services of L&T client, services are not getting searched due to & symbol.
				 * Harshal is going to replace & symbol with "and"
				 * so we need to fetch customer and get correct name from it.
				 */
				if(customerNameList!=null&&customerNameList.size()>0) {
					boolean fetchCustomerFlag=false;
					for(String name:customerNameList) {
						if(name.contains("and")||name.contains("And")) {
							fetchCustomerFlag=true;
							break;
						}
					}
					if(fetchCustomerFlag&&customerIdList!=null&&customerIdList.size()>0) {
						List<Customer> custList=ofy().load().type(Customer.class).filter("companyId", comp.getCompanyId()).filter("count IN",customerIdList).list();
						if(custList!=null&&custList.size()>0) {
							customerNameList.clear();
							for(Customer c:custList) {
								if(c.isCompany()) {
									customerNameList.add(c.getCompanyName());
								}else {
									customerNameList.add(c.getFullname());
								}
							}
						}
					}
				}
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			JSONArray statusArray = null;
			try {
				statusArray = jsonData.getJSONArray("status");
				for(int i=0; i< statusArray.length(); i++){
					JSONObject jObj = null;
					try {
						jObj = statusArray.getJSONObject(i);
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					statusList.add(jObj.getString((i+1)+""));
				}
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			JSONArray technicianArray = null;
			try {
				technicianArray = jsonData.getJSONArray("technician");
				for(int i=0; i< technicianArray.length(); i++){
					JSONObject jObj = null;
					try {
						jObj = technicianArray.getJSONObject(i);
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					technicianList.add(jObj.getString((i+1)+""));
				}
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			Date fromDate = null;
			Date toDate = null;
			try{
				fromDate = sdf.parse(jsonData.optString("fromDate").trim());
			}catch(Exception e){
				fromDate = null;
			}
			try{
				toDate = sdf.parse(jsonData.optString("toDate").trim());
			}catch(Exception e){
				toDate = null;
			}
			JSONArray branchArray = null;
			try {
				branchArray = jsonData.getJSONArray("branch");
				for(int i=0; i< branchArray.length(); i++){
					JSONObject jObj = null;
					try {
						jObj = branchArray.getJSONObject(i);
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					branchList.add(jObj.getString((i+1)+""));
				}
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			List<Service> serviceList = null;
			if(fromDate != null && toDate != null && customerIdList.size() == 0
					&& technicianList.size() == 0 && statusList.size() == 0 && branchList.size()==0){
				logger.log(Level.SEVERE, "condition 1");
				serviceList = ofy().load().type(Service.class)
						.filter("companyId", comp.getCompanyId())
						.filter("serviceDate >=",fromDate)
						.filter("serviceDate <",toDate).list();
			}else if(fromDate != null && toDate != null && customerIdList.size() > 0
					&& technicianList.size() == 0 && statusList.size() == 0 && branchList.size()==0){
				logger.log(Level.SEVERE, "condition 2");
				serviceList = ofy().load().type(Service.class)
						.filter("companyId", comp.getCompanyId())
						.filter("serviceDate >=",fromDate)
						.filter("serviceDate <",toDate)
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() > 0
					&& technicianList.size() > 0 && statusList.size() == 0 && branchList.size()==0){
				logger.log(Level.SEVERE, "condition 3");
				serviceList = ofy().load().type(Service.class)
						.filter("companyId", comp.getCompanyId())
						.filter("serviceDate >=",fromDate)
						.filter("serviceDate <",toDate)
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.filter("employee IN", technicianList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() > 0
					&& technicianList.size() > 0 && statusList.size() > 0 && branchList.size()==0){
				logger.log(Level.SEVERE, "condition 4");
				serviceList = ofy().load().type(Service.class)
						.filter("companyId", comp.getCompanyId())
						.filter("serviceDate >=",fromDate)
						.filter("serviceDate <",toDate)
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.filter("employee IN", technicianList)
						.filter("status IN", statusList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() == 0
					&& technicianList.size() > 0 && statusList.size() > 0 && branchList.size()==0){
				logger.log(Level.SEVERE, "condition 5");
				serviceList = ofy().load().type(Service.class)
						.filter("companyId", comp.getCompanyId())
						.filter("serviceDate >=",fromDate)
						.filter("serviceDate <",toDate)
						.filter("employee IN", technicianList)
						.filter("status IN", statusList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() > 0
					&& technicianList.size() == 0 && statusList.size() > 0 && branchList.size()==0){
				logger.log(Level.SEVERE, "condition 6");
				serviceList = ofy().load().type(Service.class)
						.filter("companyId", comp.getCompanyId())
						.filter("serviceDate >=",fromDate)
						.filter("serviceDate <",toDate)
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.filter("status IN", statusList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() == 0
					&& technicianList.size() == 0 && statusList.size() > 0 && branchList.size()==0){
				logger.log(Level.SEVERE, "condition 7");
				serviceList = ofy().load().type(Service.class)
						.filter("companyId", comp.getCompanyId())
						.filter("serviceDate >=",fromDate)
						.filter("serviceDate <",toDate)
						.filter("status IN", statusList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() == 0
					&& technicianList.size() > 0 && statusList.size() == 0 && branchList.size()==0){
				logger.log(Level.SEVERE, "condition 8");
				serviceList = ofy().load().type(Service.class)
						.filter("companyId", comp.getCompanyId())
						.filter("serviceDate >=",fromDate)
						.filter("serviceDate <",toDate)
						.filter("employee IN", technicianList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() == 0
					&& technicianList.size() == 0 && statusList.size() == 0 && branchList.size() > 0){
				logger.log(Level.SEVERE, "condition 9");
				serviceList = ofy().load().type(Service.class)
						.filter("companyId", comp.getCompanyId())
						.filter("serviceDate >=",fromDate)
						.filter("serviceDate <",toDate)
						.filter("branch IN", branchList).list();
			}else if(fromDate != null && toDate != null && customerIdList.size() > 0
					&& technicianList.size() == 0 && statusList.size() == 0 && branchList.size() > 0){
				logger.log(Level.SEVERE, "condition 10");
				serviceList = ofy().load().type(Service.class)
						.filter("companyId", comp.getCompanyId())
						.filter("serviceDate >=",fromDate)
						.filter("serviceDate <",toDate)
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.filter("branch IN", branchList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() > 0
					&& technicianList.size() > 0 && statusList.size() == 0 && branchList.size() > 0){
				logger.log(Level.SEVERE, "condition 11");
				serviceList = ofy().load().type(Service.class)
						.filter("companyId", comp.getCompanyId())
						.filter("serviceDate >=",fromDate)
						.filter("serviceDate <",toDate)
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.filter("employee IN", technicianList)
						.filter("branch IN", branchList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() > 0
					&& technicianList.size() > 0 && statusList.size() > 0 && branchList.size() > 0){
				logger.log(Level.SEVERE, "condition 12");
				serviceList = ofy().load().type(Service.class)
						.filter("companyId", comp.getCompanyId())
						.filter("serviceDate >=",fromDate)
						.filter("serviceDate <",toDate)
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.filter("employee IN", technicianList)
						.filter("status IN", statusList)
						.filter("branch IN", branchList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() == 0
					&& technicianList.size() > 0 && statusList.size() > 0 && branchList.size() > 0){
				logger.log(Level.SEVERE, "condition 13");
				serviceList = ofy().load().type(Service.class)
						.filter("companyId", comp.getCompanyId())
						.filter("serviceDate >=",fromDate)
						.filter("serviceDate <",toDate)
						.filter("employee IN", technicianList)
						.filter("status IN", statusList)
						.filter("branch IN", branchList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() > 0
					&& technicianList.size() == 0 && statusList.size() > 0 && branchList.size() > 0){
				logger.log(Level.SEVERE, "condition 14");
				serviceList = ofy().load().type(Service.class)
						.filter("companyId", comp.getCompanyId())
						.filter("serviceDate >=",fromDate)
						.filter("serviceDate <",toDate)
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.filter("status IN", statusList)
						.filter("branch IN", branchList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() == 0
					&& technicianList.size() == 0 && statusList.size() > 0 && branchList.size() > 0){
				logger.log(Level.SEVERE, "condition 15");
				serviceList = ofy().load().type(Service.class)
						.filter("companyId", comp.getCompanyId())
						.filter("serviceDate >=",fromDate)
						.filter("serviceDate <",toDate)
						.filter("status IN", statusList)
						.filter("branch IN", branchList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() == 0
					&& technicianList.size() > 0 && statusList.size() == 0 && branchList.size() > 0){
				logger.log(Level.SEVERE, "condition 16");
				serviceList = ofy().load().type(Service.class)
						.filter("companyId", comp.getCompanyId())
						.filter("serviceDate >=",fromDate)
						.filter("serviceDate <",toDate)
						.filter("employee IN", technicianList)
						.filter("branch IN", branchList)
						.list();
			}
			/**Date 29-1-2020 by Amol added else if for only Customer name Search***/
			else if(fromDate == null && toDate == null && customerIdList.size() >0
					&& technicianList.size() == 0 && statusList.size()==0 && branchList.size()==0){
				logger.log(Level.SEVERE, "condition 17");
				serviceList = ofy().load().type(Service.class)
						.filter("companyId", comp.getCompanyId())
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.list();
			}
			
			if(serviceList != null && serviceList.size() > 0){
				AnalyticsOperations operations = new AnalyticsOperations();
				org.json.simple.JSONArray jArray = operations.getServiceArray(serviceList,null,null);
				String jsonString=jArray.toString().replace("\\\\", "");
				logger.log(Level.SEVERE, jsonString);
				resp.getWriter().write(jsonString);
			}else{
				resp.getWriter().write("No Data Found.");
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	

}
