package com.slicktechnologies.server.android.technicianapp;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.complain.Complain;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
/**
 * Name: Apeksha Gunjal
 * Date: 16-09-2017
 * Api for calculating monthly performance of technician
 */

public class TechnicianMonthlyPerformance extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 63202583753505484L;
	
	Logger logger = Logger.getLogger("TechnicianMonthlyPerformance.class");
	Company comp;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doGet(req, resp);
		doGet(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doPost(req, resp);
		
		String urlCalled=req.getRequestURL().toString().trim();
//		String url=urlCalled.replace("http://", "");
//		String[] splitUrl=url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		
		int totalServices = 0;
		int completedServices = 0, rescheduleServices = 0, cancelledServices = 0, reworkServices = 0,scheduleServices=0;
		
		try {

			comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
			long companyId = comp.getCompanyId();
			logger.log(Level.SEVERE, "companyId::::::::::" + companyId);
			
			String username = req.getParameter("username").trim();
			String employee = req.getParameter("employee").trim();
			logger.log(Level.SEVERE, "username::" + username + " employee::" + employee);
			
			
			SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			Calendar calendar=Calendar.getInstance();
//			calendar.setTime(new Date());
			calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
//			calendar.add(Calendar.DAY_OF_YEAR, 1);
			Date afterDate = calendar.getTime();
			
			String aftrDateinString=sdf.format(afterDate);
			
			logger.log(Level.SEVERE, "Calendar afterDate:::::::"+aftrDateinString);	
					
			String date=sdf.format(new Date());
			logger.log(Level.SEVERE, "String new Date::::::::"+date);
			
			
			List<Service> services = ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("serviceDate >=",sdf.parse(aftrDateinString)).filter("serviceDate <=",sdf.parse(date))
					.filter("employee", employee.trim()).list();

			logger.log(Level.SEVERE, "services size::::::::::" + services.size());
			
			totalServices = services.size();
			
			for(Service service : services){
				
				if(service.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCOMPLETED)){
					completedServices = completedServices + 1;
				} else if(service.getStatus().equalsIgnoreCase(Service.SERVICESTATUSRESCHEDULE)){
					rescheduleServices = rescheduleServices + 1;
				} else if(service.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCANCELLED)){
					cancelledServices = cancelledServices + 1;
				} else if(service.getStatus().equalsIgnoreCase(Service.SERVICESTATUSSCHEDULE)){
					scheduleServices=scheduleServices+1;
				}
				
				List<Complain> complainlist = ofy().load().type(Complain.class).filter("companyId", companyId).filter("existingServiceId", service.getCount()).list();
				reworkServices+=complainlist.size();
			}
			
			if(totalServices > 0){
				String jsonString = createResponce(totalServices, completedServices, 
						cancelledServices, rescheduleServices, reworkServices,scheduleServices);
				logger.log(Level.SEVERE, "jsonString:::::::" + jsonString);
				resp.getWriter().println(jsonString);
			} else {
				logger.log(Level.SEVERE, "Failed::::::::::");
				resp.getWriter().println("Failed");
			}
			
		}catch(Exception e){
			logger.log(Level.SEVERE, "Error::::::::::" + e.getMessage());
		}
	}

	
	private String createResponce(int total, int completed, int cancelled, int rescheduled, int rework, int scheduleServices) {
		// TODO Auto-generated method stub
		JSONObject obj = null;
		try {
			obj = new JSONObject();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			obj.put("status", "Successful");
			obj.put("totalServices", total);
			obj.put("completedServices",completed );
			obj.put("cancelledServices",cancelled );
			obj.put("rescheduledServices",rescheduled );
			obj.put("reworkServices",rework );
			obj.put("scheduledService", scheduleServices);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	
		String jsonString = obj.toString().replace("\\\\", "");
		logger.log(Level.SEVERE, jsonString);
		
		return jsonString;
	}
}
