package com.slicktechnologies.server.android.technicianapp;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.googlecode.objectify.ObjectifyService;
import com.slicktechnologies.server.android.contractwiseservice.MarkCompletedMultipleServices;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class SendSrMail extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2166478208417301529L;

	Logger logger = Logger.getLogger("MarkCompletedSubmitServlet.class");

	Company comp;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// super.doGet(req, resp);
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String serviceLatnLong = "";
		String urlCalled = req.getRequestURL().toString().trim();
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		Exception exep = null;
		try {
			ObjectifyService.reset();
			logger.log(Level.SEVERE, "Objectify Consistency Strong");
			ofy().consistency(Consistency.STRONG);
			ofy().clear();

			SimpleDateFormat format = new SimpleDateFormat("HH:mm");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			format.setTimeZone(TimeZone.getTimeZone("IST"));
			SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			format1.setTimeZone(TimeZone.getTimeZone("IST"));
			String srNumber = format1.format(new Date());

			comp = ofy().load().type(Company.class)
					.filter("accessUrl", splitUrl[0]).first().now();
			logger.log(Level.SEVERE, "Stage One");
			String companyId = comp.getCompanyId() + "";
			logger.log(Level.SEVERE, "companyId::::::::::" + companyId);

			String servicesData = req.getParameter("servicesData").trim();
			logger.log(Level.SEVERE, "data::::::::::" + servicesData);
			JSONObject serviceJSONObject = new JSONObject(servicesData);
			JSONArray servicesArray = null;
			try {
				servicesArray = serviceJSONObject.getJSONArray("service");

			} catch (JSONException e1) {
				// TODO Auto-generated catch blocks
				e1.printStackTrace();
			}
			JSONObject obj = null;
			String serviceEngineer = "";
			Service serObject = null;
			String branchEmailId = null;
			for (int m = 0; m < servicesArray.length(); m++) {
				try {
					obj = servicesArray.getJSONObject(m);

					String serviceId = obj.optString("serviceId").trim();

					Service service = ofy().load().type(Service.class)
							.filter("companyId", Long.parseLong(companyId))
							.filter("count", Long.parseLong(serviceId)).first()
							.now();
					logger.log(Level.SEVERE, "Service::::::" + service);
					// Adding in table
					SimpleDateFormat sdf = new SimpleDateFormat(
							"dd/MM/yyyy HH:mm:ss");
					sdf.setTimeZone(TimeZone.getTimeZone("IST"));
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));
					SimpleDateFormat timeFormat = new SimpleDateFormat(
							"HH:mm:ss");
					timeFormat.setTimeZone(TimeZone.getTimeZone("IST"));
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));
					String newDate = sdf.format(new Date());

					// serObject = service;
					// ofy().save().entity(service).now();

					/** date 16/07/2018 added by komal to send email **/
					if (!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(
									"Service", "SendSRCopyOnServiceCompletion",
									service.getCompanyId())) {
						MarkCompletedMultipleServices object=new MarkCompletedMultipleServices();
						object.sendEmail(service);
					}
					if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(
									"Service", "SendSRCopyOnServiceCompletion",comp.getCompanyId())) {

						String taskName = "SendSRCopyEmail" + "$"
								+ service.getCompanyId() + "$"
								+ service.getCount() + "$" + srNumber + "$"
								+ branchEmailId;
						Queue queue = QueueFactory.getQueue("SRCopyEmailQueueRevised-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/SRCopyEmailTaskQueue").param("taskKeyAndValue", taskName));
					}

				} catch (Exception e) {
					e.printStackTrace();
					exep = e;
					resp.getWriter().println("Failed");
					logger.log(Level.SEVERE, "Error::::::::" + e);
				}

				if (exep == null) {
					resp.getWriter().println("Success");
				}
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch blocks
			e1.printStackTrace();
		}
	}
}
