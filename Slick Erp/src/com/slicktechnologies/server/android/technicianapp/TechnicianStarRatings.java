package com.slicktechnologies.server.android.technicianapp;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.PettyCashTransaction;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.personlayer.Employee;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Name: Apeksha Gunjal Date: 15-09-2017 Api for calculating star rating,
 * companyName, comapnyLogo, profileImage of technician
 */
public class TechnicianStarRatings extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4679248970518656487L;
	Logger logger = Logger.getLogger("TechnicianMonthlyRatings.class");
	Company comp;
	Employee emp;
	PettyCash pettyCash;
	String companyLogo = "", companyName = "", empProfilePhoto = "",
			pettyCashName = "";
	long companyOfficePhoneNumber1 = 0, companyOfficePhoneNumber2 = 0;
	int employeeId, companyCount;
	double employeeBalance;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doPost(req, resp);
		String urlCalled = req.getRequestURL().toString().trim();
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		Exception exep = null;
		try {

			comp = ofy().load().type(Company.class)
					.filter("accessUrl", splitUrl[0]).first().now();
			long companyId = comp.getCompanyId();
			logger.log(Level.SEVERE, "companyId::::::::::" + companyId);

			String username = req.getParameter("username").trim();
			String employee = req.getParameter("employee").trim();

			if (comp.getLogo() != null)
				companyLogo = comp.getLogo().getUrl();

			emp = ofy().load().type(Employee.class)
					.filter("companyId", companyId)
					.filter("fullname", employee.trim()).first().now();
			employeeId = emp.getCount();

			companyOfficePhoneNumber1 = comp.getCellNumber1();
			companyOfficePhoneNumber2 = comp.getCellNumber2();
			companyCount = comp.getCount();

			pettyCash = ofy().load().type(PettyCash.class)
					.filter("employee", employee).first().now();

			List<Service> services = ofy().load().type(Service.class)
					.filter("companyId", companyId)
					.filter("employee", employee)
					.filter("status", Service.SERVICESTATUSCOMPLETED).list();

			logger.log(Level.SEVERE,
					"services size::::::::::" + services.size());
			logger.log(Level.SEVERE, "username::" + username + " employee::"
					+ employee);
			int total = services.size();
			float baseValue = 0;
			float average = 0;

			for (Service service : services) {
				logger.log(Level.SEVERE, "service::::" + service.getCount());
				String feedback = service.getCustomerFeedback();
				if (service.getCustomerFeedback() != null) {
					if (feedback.equalsIgnoreCase("Poor")) {
						baseValue = baseValue + 1;
					} else if (feedback.equalsIgnoreCase("Average")) {
						baseValue = baseValue + 2;
					} else if (feedback.equalsIgnoreCase("Good")) {
						baseValue = baseValue + 3;
					} else if (feedback.equalsIgnoreCase("Very Good")) {
						baseValue = baseValue + 4;
					} else if (feedback.equalsIgnoreCase("Excellent")) {
						baseValue = baseValue + 5;
					}
				}
			}
			if (total != 0) {
				average = baseValue / total;
			}

			// if( average > 0){

			companyName = comp.getBusinessUnitName();
			if (comp.getLogo() != null)
				companyLogo = comp.getLogo().getUrl();
			if (emp.getPhoto() != null)
				empProfilePhoto = emp.getPhoto().getUrl();
			if (pettyCash != null) {
				employeeBalance = pettyCash.getPettyCashBalance();
				pettyCashName = pettyCash.getPettyCashName();
			}

			logger.log(Level.SEVERE, "comapnyName: " + companyName);
			logger.log(Level.SEVERE, "companyLogo: " + companyLogo);
			logger.log(Level.SEVERE, "employeeId: " + employeeId);
			logger.log(Level.SEVERE, "empProfilePhoto: " + empProfilePhoto);
			logger.log(Level.SEVERE, "employeeBalance: " + employeeBalance);
			logger.log(Level.SEVERE, "pettyCashName: " + pettyCashName);
			String jsonString = createResponce(average, companyName,
					companyLogo, empProfilePhoto, pettyCashName,
					employeeBalance, companyOfficePhoneNumber1,
					companyOfficePhoneNumber2, employee);
			logger.log(Level.SEVERE, "jsonString:::::::" + jsonString);
			resp.getWriter().println(jsonString);
			// } else {
			// logger.log(Level.SEVERE, "Failed::::::::::");
			// resp.getWriter().println("Failed");
			// }

		} catch (Exception e) {
			logger.log(Level.SEVERE, "error::::::::::" + e.getMessage());
		}
	}

	private String createResponce(float rating, String companyName,
			String companyLogo, String empPhoto, String pettyCashName,
			double employeeBalance, long compNo1, long compNo2, String employee) {
		// TODO Auto-generated method stub
		JSONObject obj = null;
		try {
			obj = new JSONObject();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			obj.put("status", "Successful");
			obj.put("rating", rating + "/5");
			obj.put("companyName", companyName);
			obj.put("companyLogo", companyLogo);
			obj.put("employeeProfilePhoto", empPhoto);
			obj.put("pettyCashName", pettyCashName);
			obj.put("employeeBalance", employeeBalance);
			List<String> statusList = new ArrayList<String>();
			statusList.add(MultipleExpenseMngt.CREATED);
			statusList.add(MultipleExpenseMngt.REQUESTED);
			List<MultipleExpenseMngt> expenseList = ofy().load()
					.type(MultipleExpenseMngt.class)
					.filter("companyId", comp.getCompanyId())
					.filter("pettyCashName", pettyCashName.trim())
					.filter("status IN", statusList).list();
			double unApprovedAmount = 0;
			for (MultipleExpenseMngt multipleExpenseMngt : expenseList) {
				unApprovedAmount = unApprovedAmount
						+ multipleExpenseMngt.getTotalAmount();
			}
			obj.put("unApprovedAmount", unApprovedAmount);
			obj.put("balanceAmount", employeeBalance - unApprovedAmount);
			obj.put("companyOfficePhoneNumber1", compNo1);
			obj.put("companyOfficePhoneNumber2", compNo2);
			obj.put("companyId", companyCount);
			List<PettyCashTransaction> pettyCashTransactionList = ofy().load()
					.type(PettyCashTransaction.class)
					.filter("companyId", comp.getCompanyId())
					.filter("pettyCashName", pettyCashName).list();
			Gson gson = new Gson();
			String str = gson.toJson(pettyCashTransactionList).replace("\\\\",
					"");
			JSONArray jsonArray = new JSONArray(str);
			obj.put("pettyCashTransactions", jsonArray);

			List<Config> rescheduleReasonList = ofy().load().type(Config.class)
					.filter("companyId", comp.getCompanyId())
					.filter("type", 89).list();

			org.json.simple.JSONArray jArray = new org.json.simple.JSONArray();

			for (Config config : rescheduleReasonList) {
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("reason", config.getName());
				jArray.add(jsonObj);
			}
			obj.put("reasonList", jArray);

		} catch (JSONException e) {
			e.printStackTrace();
		}

		String jsonString = obj.toString().replace("\\\\", "");
		logger.log(Level.SEVERE, jsonString);

		return jsonString;
	}

}
