package com.slicktechnologies.server.android.technicianapp;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

/**
 * Description : It is used to fetch all 4 images stored in particular service using service ID.
 * @author pc1
 *
 */
public class FetchServiceImages extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3608210459941290693L;
	Logger logger = Logger.getLogger("FetchServiceImages.class");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doGet(req, resp);
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doPost(req, resp);
		RegisterServiceImpl impl=new RegisterServiceImpl();
		impl.getClass();
		
		String urlCalled = req.getRequestURL().toString().trim();
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
//		
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);

		Company company = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		if (company == null) {
			company = ofy().load().type(Company.class)
					.filter("accessUrl", "127").first().now();
		}
		
		String service_Id=req.getParameter("service_Id");
		String imageType=req.getParameter("image_Type");
		int serviceId=0;
		Exception serviceIdExp=null;
		try{
			serviceId=Integer.parseInt(service_Id.trim());
		}catch(Exception e){
			e.printStackTrace();
			serviceIdExp=e;
		}
		Service customerService=null;
		if(serviceIdExp==null){
			customerService=ofy().load().type(Service.class).filter("companyId", company.getCompanyId()).filter("count", serviceId).first().now();
		}else{
			customerService=ofy().load().type(Service.class).filter("companyId", company.getCompanyId()).filter("fullname", service_Id.trim()).first().now();
		}
		if(customerService!=null){
			SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			spf.setTimeZone(TimeZone.getTimeZone("IST"));
			
			JSONObject jsonObject=new JSONObject();
			
			String serImgUrl1="";
			String serImgUrl2="";
			String serImgUrl3="";
			String serImgUrl4="";
			
			if(customerService.getServiceImage1()!=null&&customerService.getServiceImage1().getUrl()!=null){
				serImgUrl1=customerService.getServiceImage1().getUrl();
			}
			if(customerService.getServiceImage2()!=null&&customerService.getServiceImage2().getUrl()!=null){
				serImgUrl2=customerService.getServiceImage2().getUrl();
			}
			if(customerService.getServiceImage3()!=null&&customerService.getServiceImage3().getUrl()!=null){
				serImgUrl3=customerService.getServiceImage3().getUrl();
			}
			if(customerService.getServiceImage4()!=null&&customerService.getServiceImage4().getUrl()!=null){
				serImgUrl4=customerService.getServiceImage4().getUrl();
			}
			
			
			try {
				jsonObject.put("serviceId", customerService.getCount());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				jsonObject.put("serviceImage1", serImgUrl1);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				jsonObject.put("serviceImage2", serImgUrl2);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				jsonObject.put("serviceImage3", serImgUrl3);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				jsonObject.put("serviceImage4", serImgUrl4);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
//			String applicationId = System.getProperty("com.google.appengine.application.id");
//		    String version = System.getProperty("com.google.appengine.application.version");
//		    String hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
//			try {
//				jsonObject.put("employeeProfilePhoto_url", hostUrl+customerService.getPhoto().getUrl());
//			} catch (JSONException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			
			String serDate=jsonObject.toString();
			serDate=serDate.toString().replaceAll("\\\\", "");
			resp.getWriter().print(serDate);
		}else{
			resp.getWriter().print("No Service found for "+service_Id);
		}
		
		
	}
}
