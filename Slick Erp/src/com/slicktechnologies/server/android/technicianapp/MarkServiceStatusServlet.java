package com.slicktechnologies.server.android.technicianapp;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class MarkServiceStatusServlet extends HttpServlet {

	/**
	 * Description : set service status whichever sent from Application
	 */
	private static final long serialVersionUID = 3599435763123918235L;

	Logger logger = Logger.getLogger("saveservicestatus.class");
	Company comp;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
//		resp.setHeader("Access-Control-Allow-Origin", AppConstants.TRENDZWEBSOLUTION);
		resp.setHeader("Access-Control-Allow-Origin", "*");
		String urlCalled=req.getRequestURL().toString().trim();
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			
			comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();	
			resp.setContentType("text/plain");
			String companyId=comp.getCompanyId()+"";
			String status=req.getParameter("status").trim();
			String serviceId=req.getParameter("serviceId").trim();
			String user=req.getParameter("user").trim();
			String remark= "";
			
			try{
				remark = req.getParameter("remark").trim();
			}catch(Exception e){
					
			}
			Service service=ofy().load().type(Service.class).filter("companyId", Long.parseLong(companyId)).filter("count",Long.parseLong(serviceId)).first().now();
				
			service.setStatus(status);
			service.setRemark(remark);	
			if(status.equalsIgnoreCase(Service.CANCELLED)){					
			service.setReasonForChange("Service ID ="+serviceId+" "+"Service status ="+status+"\n"
					+"has been "+status.toLowerCase() +" by "+user+" with remark "+"\n"
					+"Remark ="+remark+" "+" Date ="+sdf.format(new Date()));
			}
			ofy().save().entity(service);
			
			resp.getWriter().write("Status Updated Successfully.");
		}catch(Exception e){
				
			}
		}

}
