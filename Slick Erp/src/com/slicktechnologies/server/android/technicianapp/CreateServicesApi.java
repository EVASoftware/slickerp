package com.slicktechnologies.server.android.technicianapp;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.device.TrackTableDetails;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.SingletoneNumberGeneration;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;

public class CreateServicesApi extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5061654512189768733L;
	Logger logger = Logger.getLogger("Logger");

	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm");//"dd/MM/yyyy hh:mm:ss"

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setHeader("Access-Control-Allow-Origin", "*");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		dateTimeFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		SimpleDateFormat timeformat = new SimpleDateFormat("HH:mm");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		timeformat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		String urlCalled=req.getRequestURL().toString().trim();
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		
		logger.log(Level.SEVERE, "createServicesApi call");
		
		
	//	try{
		Company comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();	
		resp.setContentType("text/plain");
		
		String servicesData=req.getParameter("servicesData").trim();
		logger.log(Level.SEVERE, "data::::::::::" + servicesData);
		JSONObject serviceJSONObject = null;
		try {
			serviceJSONObject = new JSONObject(servicesData);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONArray servicesArray = null;
		try {
			servicesArray = serviceJSONObject.getJSONArray("services");
			
		} catch (JSONException e1) {
			// TODO Auto-generated catch blocks
			e1.printStackTrace();
		}
		JSONObject jsonData = null;
		for(int i=0 ; i<servicesArray.length() ; i++){
			
			String validationmessage = "";
			try {
				jsonData = servicesArray.getJSONObject(i);
				int serviceId=0;
				if(jsonData.optInt("Service Id")!=0) 
					serviceId = jsonData.optInt("Service Id");
					
				String serviceName = "";
				if(jsonData.optString("Service Name")!=null) {
					serviceName = jsonData.optString("Service Name");
				}
				int productId=0;
				if(jsonData.optInt("Product Id")!=0) {
					productId = jsonData.optInt("Product Id");
				}
				String customerName = "";
				if(jsonData.optString("Customer Name")!=null) {
					customerName = jsonData.optString("Customer Name");
				}
				int customerId = 0;
				if(jsonData.optInt("Customer Id")!=0) {
					customerId = jsonData.optInt("Customer Id");
				}
				
				int customerBranchId =0;
				if(jsonData.optInt("Customer Branch Id")!=0) {
					customerBranchId = jsonData.optInt("Customer Branch Id");
				}
				String customerBranch = "Service Address";
				if(jsonData.optString("Customer Branch")!=null && !jsonData.optString("Customer Branch").equals("")) {
					customerBranch = jsonData.optString("Customer Branch");
				}
				String serviceLocation = "";
				if(jsonData.optString("Service Location")!=null) {
					serviceLocation = jsonData.optString("Service Location");
				}
				String area = "";
				if(jsonData.optString("Area")!=null) {
					area = jsonData.optString("Area");
				}
				String premise = "";
				if(jsonData.optString("Premise")!=null) {
					premise = jsonData.optString("Premise");
				}
				
				String technician = "";
				if(jsonData.optString("Technician")!=null) {
					technician = jsonData.optString("Technician");
				}
				String serviceDate ="";
				if(jsonData.optString("Service Date")!=null) {
					serviceDate = jsonData.optString("Service Date");
				}
				String serviceTime = "";
				if(jsonData.optString("Service Time")!=null) {
					serviceTime = jsonData.optString("Service Time");
				}
				int assetId = 0;
				if(jsonData.optInt("Asset Id")!=0) {
					assetId = jsonData.optInt("Asset Id");
				}
				String componentName = "";
				if(jsonData.optString("Component Name")!=null) {
					componentName = jsonData.optString("Component Name");
				}
				String mfgNumber = "";
				if(jsonData.optString("MFG Number")!=null) {
					mfgNumber = jsonData.optString("MFG Number");
				}
				String mfgDate = "";
				if(jsonData.optString("MFG Date")!=null) {
					mfgDate = jsonData.optString("MFG Date");
				}
				String replacementDate = "";
				if(jsonData.optString("Replacement Date")!=null) {
					replacementDate = jsonData.optString("Replacement Date");
				}
				String branch = "";
				if(jsonData.optString("Branch")!=null) {
					branch = jsonData.optString("Branch");
				}
				
				String completionDateTime="";
				if(jsonData.optString("Completion Date And Time")!=null) {
					completionDateTime = jsonData.optString("Completion Date And Time");
				}
				
				
				/*
				 * @author Ashwini Patil 
				 * Date:27-07-2023
				 * In eva pedio checklist, we are adding one close button, so when stationed technician will click close button 
				 * while creating service, service will get marked as completed and it will appear in history.
				 * we need to store all completion information in the service entity
				 */
				String lat="";
				String lon="";
				double km=0;
				String location="";
				String serviceType="";
				

				try {
					logger.log(Level.SEVERE,"in new try");
					if(jsonData.has("lat")&&jsonData.optString("lat")!=null) {
						lat = jsonData.optString("lat");
						logger.log(Level.SEVERE,"lat::::::::"+lat);
					}
					if(jsonData.has("lon")&&jsonData.optString("lon")!=null) {
						lon = jsonData.optString("lon");
						logger.log(Level.SEVERE,"lon ::::::::"+lon);
					}
					if(jsonData.has("km")&&jsonData.optString("km")!=null) {	
						km = Double.parseDouble(jsonData.optString("km"));
						logger.log(Level.SEVERE,"km ::::::::"+km);
					}
					if(jsonData.has("location")&&jsonData.optString("location")!=null) {
						location = jsonData.optString("location");
						logger.log(Level.SEVERE,"location ::::::::"+location);
					}
					if(jsonData.has("serviceType")&&jsonData.optString("serviceType")!=null) {
						serviceType = jsonData.optString("serviceType");
						logger.log(Level.SEVERE,"serviceType ::::::::"+serviceType);
					}					
				
				}catch(Exception e) {
					e.printStackTrace();
				}
				
				
				String strcontractId = "";
				if(jsonData.optString("Contract Id")!=null) {
					strcontractId = jsonData.optString("Contract Id");
				}
				if(strcontractId==null || strcontractId.equals("") || strcontractId.equals("0")) {
					validationmessage += "Contract id cant be blank or zero";
				}
				int contractId = 0;
				if(!strcontractId.equals("") &&  !strcontractId.equals("0")) {
					contractId = Integer.parseInt(strcontractId);
				}
				boolean complainServiceFlag = false;
				complainServiceFlag = jsonData.optBoolean("complaintServiceFlag");
				
				
				String serviceRemark = "";
				if(jsonData.optString("remark")!=null) {
					serviceRemark = jsonData.optString("remark");
				}
				
				
				Customer customerEntity = null;
				if(customerId!=0 || !customerName.equals("")) {
					if(customerId!=0) {
						customerEntity = ofy().load().type(Customer.class).filter("count", customerId).filter("companyId", comp.getCompanyId()).first().now();
					}
					else {
						if(!customerName.equals("")) {
							customerEntity = ofy().load().type(Customer.class).filter("fullname", customerName).filter("companyId",  comp.getCompanyId()).first().now();
							if(customerEntity==null) {
								customerEntity = ofy().load().type(Customer.class).filter("companyName", customerName).filter("companyId",  comp.getCompanyId()).first().now();
							}
						}
						
					}
				}
				
				CustomerBranchDetails customerBranchDetails = null;
				if(!customerBranch.equals("Service Address") || customerBranchId!=0) {
					if(customerBranchId!=0)
					customerBranchDetails = ofy().load().type(CustomerBranchDetails.class).filter("count", customerBranchId).filter("companyId", comp.getCompanyId()).first().now();
					else {
						if(!customerBranch.equals("Service Address"))
						customerBranchDetails = ofy().load().type(CustomerBranchDetails.class).filter("buisnessUnitName", customerBranch).filter("companyId", comp.getCompanyId()).first().now();
					}
				}
				
				Branch branchEntity = null;
				if(!branch.equals("")) {
					branchEntity = ofy().load().type(Branch.class).filter("buisnessUnitName", branch).filter("companyId", comp.getCompanyId()).first().now();
					if(branchEntity==null) {
						validationmessage += " Branch does not exist.";
					}
				}
				
				ServiceProduct serviceproduct = null;
				if(productId!=0 || !serviceName.equals("")) {
					if(productId!=0) {
						 serviceproduct = ofy().load().type(ServiceProduct.class)
									.filter("companyId", comp.getCompanyId())
									.filter("count", productId).first().now(); //Ashwini Patil Date:8-5-2024 changed filter to productid instead of servicename
					}
					else {
						if(!serviceName.equals("")) {
							serviceproduct = ofy().load().type(ServiceProduct.class)
									.filter("companyId", comp.getCompanyId())
									.filter("productName", serviceName).first().now();
						}
						
					}
					
				}
				
				logger.log(Level.SEVERE, "technician name "+technician);
				User user = null;
				if(technician!=null && !technician.equals("")) {
					
					user = ofy().load().type(User.class)
							.filter("companyId", comp.getCompanyId())
							.filter("userName", technician).first().now();
					if(user==null) {
						validationmessage += "Technician employee details not found in the user details!";
					}
					else {
						technician = user.getEmployeeName();
					}
					
				}
				logger.log(Level.SEVERE, "technician employee name "+technician);

				
				if(serviceId!=0) {
					logger.log(Level.SEVERE, "For service updation");
					
					if(!validationmessage.equals("")) {
					    resp.getWriter().write(validationmessage);
					}
					else {
						String updationreponse = updateService(serviceLocation,area,premise,technician,serviceDate,serviceTime,assetId,componentName,mfgNumber,mfgDate,
								 replacementDate,branch,serviceId,comp.getCompanyId(),customerEntity,customerBranchDetails,branchEntity,serviceproduct,completionDateTime,contractId,complainServiceFlag,serviceRemark);
					
					    resp.getWriter().write(updationreponse);
					}
					return;
				}
				
				
				
				
				logger.log(Level.SEVERE, "For new service creation");
				
				
				if(customerId!=0 || !customerName.equals("")) {
					if(customerId!=0) {
						customerEntity = ofy().load().type(Customer.class).filter("count", customerId).filter("companyId", comp.getCompanyId()).first().now();
					}
					else {
						customerEntity = ofy().load().type(Customer.class).filter("fullname", customerName).filter("companyId",  comp.getCompanyId()).first().now();
						if(customerEntity==null) {
							customerEntity = ofy().load().type(Customer.class).filter("companyName", customerName).filter("companyId",  comp.getCompanyId()).first().now();
						}
					}
				}
				
				if(customerId==0 && customerName.equals("")) {
					validationmessage += " Please send customer id or customer name.";
				}
				
				if(customerEntity==null) {
					validationmessage += " Customer does not exist.";
				}
				
				if(!customerBranch.equals("Service Address") || customerBranchId!=0) {
					if(customerBranchId!=0)
					customerBranchDetails = ofy().load().type(CustomerBranchDetails.class).filter("count", customerBranchId).filter("companyId", comp.getCompanyId()).first().now();
					else {
						customerBranchDetails = ofy().load().type(CustomerBranchDetails.class).filter("buisnessUnitName", customerBranch).filter("companyId", comp.getCompanyId()).first().now();
					}
				}
				if(!customerBranch.equals("Service Address")  && customerBranchDetails==null) {
					validationmessage += " Customer branch does not exist.";
				}
				
				if(branch.equals("")) {
					validationmessage += " Please send branch name.";
				}
				
				if(serviceId==0 && serviceName.equals("")) {
					validationmessage += " Please send service id or service name.";
				}

				
				if(serviceproduct==null) {
					validationmessage += " Service product does not exist.";
				}
				if(technician.equals("")) {
					validationmessage += " Technician name can not blank";
				}
				logger.log(Level.SEVERE, "validationmessage "+validationmessage);
				logger.log(Level.SEVERE, "validationmessage "+validationmessage.trim()+"for checker");
				logger.log(Level.SEVERE, "validationmessage 1st condition"+(!validationmessage.trim().equals(" ")));
				logger.log(Level.SEVERE, "validationmessage 2st condition "+(!validationmessage.trim().equals("")));

				if(validationmessage.trim().equals("") || validationmessage.trim().equals(" ")) {
					logger.log(Level.SEVERE, "no validation inside service creation"+validationmessage+"logger");

					Service serviceEntity = new Service();
					
					PersonInfo personinfo = new PersonInfo();
					if(customerEntity.isCompany()){
						personinfo.setFullName(customerEntity.getCompanyName());
						personinfo.setPocName(customerEntity.getFullname());
					}
					else{
						personinfo.setFullName(customerEntity.getFullname());
						personinfo.setPocName(customerEntity.getFullname());
					}
					personinfo.setCount(customerEntity.getCount());
					personinfo.setCellNumber(customerEntity.getCellNumber1());
					if(customerEntity.getEmail()!=null)
					personinfo.setEmail(customerEntity.getEmail());
					
					serviceEntity.setCompanyId(comp.getCompanyId());
					serviceEntity.setPersonInfo(personinfo);
//					serviceEntity.setContractCount(con.getCount());
					serviceproduct.setProductName(serviceName);//Ashwini Patil Date:10-05-2024 Pest Mortem reported that when they create service through checklist, service name is not coming same as there in contract
					serviceEntity.setProduct(serviceproduct);
					serviceEntity.setBranch(branch);
					serviceEntity.setStatus(Service.SERVICESTATUSSCHEDULE);
					serviceEntity.setAdHocService(false);
					serviceEntity.setServiceType(AppConstants.PERIODIC);
					serviceEntity.setServiceLocation(serviceLocation);
					serviceEntity.setServiceLocationArea(area);
					serviceEntity.setPremises(premise);
					serviceEntity.setAssetId(assetId);
					serviceEntity.setComponentName(componentName);
					if(mfgDate!=null && !mfgDate.equals(""))
					serviceEntity.setMfgDate(dateFormat.parse(mfgDate));
					serviceEntity.setMfgNo(mfgNumber);
					if(replacementDate!=null && !replacementDate.equals(""))
					serviceEntity.setReplacementDate(dateFormat.parse(replacementDate));
					serviceEntity.setProduct(serviceproduct);
					if(serviceDate!=null && !serviceDate.equals(""))
					serviceEntity.setServiceDate(dateFormat.parse(serviceDate));
					serviceEntity.setAddress(customerEntity.getSecondaryAdress());
			    	
			    	serviceEntity.setServiceTime(serviceTime);
			    	serviceEntity.setServiceBranch(customerBranch);
					
			    	serviceEntity.setEmployee(technician);
			    	serviceEntity.setOnsiteTechnician(true);
			    	SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();
					int newserviceId=(int) numGen.getLastUpdatedNumber(AppConstants.SERVICE, serviceEntity.getCompanyId(), (long) 1);
					serviceEntity.setCount(newserviceId);
					
					logger.log(Level.SEVERE, "newserviceId "+newserviceId);
					logger.log(Level.SEVERE, "contractId "+contractId);
					serviceEntity.setContractCount(contractId);
					if(contractId!=0) {
						Contract contractEntity = ofy().load().type(Contract.class).filter("count", contractId).filter("companyId", comp.getCompanyId()).first().now();
						if(contractEntity!=null) {
							serviceEntity.setContractStartDate(contractEntity.getStartDate());
							serviceEntity.setContractEndDate(contractEntity.getEndDate());
						}
											
					}
					String customerbBranchLocationArea = customerBranch;
					if(serviceLocation!=null && !serviceLocation.equals("")) {
						customerbBranchLocationArea=customerbBranchLocationArea+" / "+serviceLocation;
					}
					if(area!=null && !area.trim().equals("")) {
						customerbBranchLocationArea=customerbBranchLocationArea+" / "+area;
					}
					serviceEntity.setDescription(customerbBranchLocationArea);
					
					
					if(serviceEntity.getDescription()!=null && !serviceEntity.getDescription().equals("")){
					
						String description = serviceEntity.getDescription() +" "+serviceRemark;
						serviceEntity.setDescription(description);
					}
					else{
						serviceEntity.setDescription(serviceRemark);
					}
					boolean completedFlag=false; //Ashwini Patil Date:27-07-2023 need to modify message when service gets completed
					if(completionDateTime!=null && !completionDateTime.equals("")) {
						try {
							logger.log(Level.SEVERE, "completionDateTime ::::"+completionDateTime);
							Date completionDate = dateTimeFormat.parse(completionDateTime);
							logger.log(Level.SEVERE, "completionDate ::::"+completionDate);
							if(completionDate!=null) {
								logger.log(Level.SEVERE, "completionDate not null");
								String time=timeformat.format(completionDate);
								serviceEntity.setServiceCompletionDate(completionDate);
								serviceEntity.setSystemCompletionDate(completionDate);
								
								completedFlag=true;
								serviceEntity.setCompletedDate(completionDate);
								serviceEntity.setCompletedDate_time(completionDateTime);
								serviceEntity.setCompletedTime(time);
								serviceEntity.setStatus(Service.SERVICESTATUSCOMPLETED);
								
								
								if(lat!=null&&!lat.equals("")&&lon!=null&&!lon.equals("")&&location!=null&&!location.equals("")) {
									logger.log(Level.SEVERE, "lat lon location not null");
									serviceEntity.setStartedDate(completionDate);
									serviceEntity.setStartedDate_time(completionDateTime);
									serviceEntity.setStartedTime(time);	
									
									serviceEntity.setReportedDate(completionDate);
									serviceEntity.setReportedDate_time(completionDateTime);
									serviceEntity.setReportedTime(time);	
									
									serviceEntity.setTcompletedDate(completionDate);
									serviceEntity.settCompletedDate_time(completionDateTime);
									serviceEntity.settCompletedTime(time);
									
									TrackTableDetails tracktabledetailsStarted=new TrackTableDetails();
									tracktabledetailsStarted.setDate_time(completionDateTime);
									tracktabledetailsStarted.setTime(time);
									TrackTableDetails tracktabledetailsReported=new TrackTableDetails();
									tracktabledetailsReported.setDate_time(completionDateTime);
									tracktabledetailsReported.setTime(time);
									TrackTableDetails tracktabledetailsTCompleted=new TrackTableDetails();
									tracktabledetailsTCompleted.setDate_time(completionDateTime);
									tracktabledetailsTCompleted.setTime(time);
									TrackTableDetails tracktabledetailsCompleted=new TrackTableDetails();
									tracktabledetailsCompleted.setDate_time(completionDateTime);
									tracktabledetailsCompleted.setTime(time);
									
									serviceEntity.setStartLatnLong(lat+"$"+lon);
									serviceEntity.setStartedLatitude(lat);
									serviceEntity.setStartedLongitude(lon);
									
									serviceEntity.setReportLatnLong(lat+"$"+lon);
									serviceEntity.setReportedLatitude(lat);
									serviceEntity.setReportedLongitude(lon);
									
									serviceEntity.settCompletedLatitude(lat);
									serviceEntity.settCompletedLongitude(lon);
									
									serviceEntity.setCompletedLatitude(lat);	
									serviceEntity.setCompletedLongitude(lon);
									serviceEntity.setCompleteLatnLong(lat+"$"+lon);
									
									serviceEntity.setCompletedByApp(true);
									
									tracktabledetailsStarted.setLatitude(lat);
									tracktabledetailsStarted.setLongitude(lon);
									tracktabledetailsReported.setLatitude(lat);
									tracktabledetailsReported.setLongitude(lon);
									tracktabledetailsTCompleted.setLatitude(lat);
									tracktabledetailsTCompleted.setLongitude(lon);
									tracktabledetailsCompleted.setLatitude(lat);
									tracktabledetailsCompleted.setLongitude(lon);
									
									serviceEntity.setStartedLocationName(location);
									serviceEntity.setReportedLocationName(location);
									serviceEntity.settCompletedLocationName(location);
									serviceEntity.setCompletedLocationName(location);
									tracktabledetailsStarted.setLocationName(location);
									tracktabledetailsReported.setLocationName(location);
									tracktabledetailsTCompleted.setLocationName(location);
									tracktabledetailsCompleted.setLocationName(location);
								
									tracktabledetailsStarted.setKiloMeters(km);
									tracktabledetailsReported.setKiloMeters(km);
									tracktabledetailsTCompleted.setKiloMeters(km);
									tracktabledetailsCompleted.setKiloMeters(km);
									
									
									tracktabledetailsStarted.setSrNo(1);
									tracktabledetailsStarted.setStatus("Started");
									serviceEntity.getTrackServiceTabledetails().add(tracktabledetailsStarted);
									
									tracktabledetailsReported.setSrNo(2);
									tracktabledetailsReported.setStatus("Reported");
									serviceEntity.getTrackServiceTabledetails().add(tracktabledetailsReported);
									
									tracktabledetailsTCompleted.setSrNo(3);
									tracktabledetailsTCompleted.setStatus("TCompleted");
									serviceEntity.getTrackServiceTabledetails().add(tracktabledetailsTCompleted);
									
									tracktabledetailsCompleted.setSrNo(4);
									tracktabledetailsCompleted.setStatus("Completed");
									serviceEntity.getTrackServiceTabledetails().add(tracktabledetailsCompleted);
								}
								
								logger.log(Level.SEVERE, "all set for service completion");
								
							}
							
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
					}
					
					serviceEntity.setServiceScheduled(true);
					if(complainServiceFlag) {
						serviceEntity.setServiceType(AppConstants.SERVICETYPECOMPLAIN);
					}
					if(serviceType!=null&&!serviceType.equals("")) {
						
						if(user!=null) {
							String msg= "Service has been created from "+serviceType+" by "+user.getEmployeeName()+" on "+dateTimeFormat.format(new Date());
							if(serviceEntity.getServiceBranch()!=null&&!serviceEntity.getServiceBranch().equals("Service Address"))
								msg+=" Branch: "+serviceEntity.getServiceBranch();
							if(serviceEntity.getServiceLocation()!=null&&!serviceEntity.getServiceLocation().equals(""))
								msg+=" Service Location: "+serviceEntity.getServiceLocation();
							if(serviceEntity.getServiceLocationArea()!=null&&!serviceEntity.getServiceLocationArea().equals(""))
								msg+=" Area: "+serviceEntity.getServiceLocationArea();
		
							if(serviceEntity.getDescription()!=null&&!serviceEntity.getDescription().equals(""))
								serviceEntity.setDescription(serviceEntity.getDescription()+" "+msg);
							else
								serviceEntity.setDescription(msg);						
						}
					
					}
					
					
					
					
			    	GenricServiceImpl impl = new GenricServiceImpl();
			    	impl.save(serviceEntity);
					logger.log(Level.SEVERE, "new service created successfully ");
					logger.log(Level.SEVERE, "newserviceId "+newserviceId);

					JSONObject jsonobj = new JSONObject();
					jsonobj.put("serviceId", serviceEntity.getCount()+"");
					jsonobj.put("serviceDate", dateFormat.format(serviceEntity.getServiceDate()));
					if(completedFlag)
						jsonobj.put("message", "Service created and completed successfully");
					else
						jsonobj.put("message", "Service created successfully");
					
					String jsonstring = jsonobj.toString().replaceAll("\\\\", "");
					logger.log(Level.SEVERE, "Json"+jsonstring);
					resp.getWriter().println(jsonstring);

				}
				else {
				    resp.getWriter().write(validationmessage);

				}


			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}	
	}

	private String checkCustomerDetails(int customerId, String customerName, Long companyId) {

		Customer customerEntity = null;
		if(customerId!=0) {
			customerEntity = ofy().load().type(Customer.class).filter("count", customerId).filter("companyId", companyId).first().now();
		}
		else {
			customerEntity = ofy().load().type(Customer.class).filter("fullname", customerName).filter("companyId", companyId).first().now();
			if(customerEntity==null) {
				customerEntity = ofy().load().type(Customer.class).filter("companyName", customerName).filter("companyId", companyId).first().now();
			}
		}
		if(customerEntity==null) {
			return " Customer does not exist.";
		}
		return "";
		
	}

	private String updateService(String serviceLocation, String area, String premise, String technician,
			String serviceDate, String serviceTime, int assetId, String componentName, String mfgNumber, String mfgDate,
			String replacementDate, String branch, int serviceId, Long companyId, Customer customerEntity, CustomerBranchDetails customerBranchDetails, Branch branchEntity, ServiceProduct serviceproduct, String completionDateTime, int contractId, boolean complainServiceFlag, String serviceRemark) {
		
		Service serviceEntity = ofy().load().type(Service.class).filter("count", serviceId).filter("contractCount", contractId).filter("companyId", companyId).first().now();
		if(serviceEntity!=null) {
			if(serviceEntity.getStatus().equals(Service.SERVICESTATUSCOMPLETED)) {
				return "Can not updated service service status is completed";
			}
			logger.log(Level.SEVERE, "Inside service updation method");

			if(serviceLocation!=null && !serviceLocation.equals(""))
			serviceEntity.setServiceLocation(serviceLocation);
			if(area!=null && !area.equals(""))
			serviceEntity.setServiceLocationArea(area);
			if(premise!=null && !premise.equals(""))
			serviceEntity.setPremises(premise);
			if(technician!=null && !technician.equals(""))
	    	serviceEntity.setEmployee(technician);
			try {
				if(serviceDate!=null && !serviceDate.equals(""))
				serviceEntity.setServiceDate(dateFormat.parse(serviceDate));
				if(mfgDate!=null && !mfgDate.equals(""))
				serviceEntity.setMfgDate(dateFormat.parse(mfgDate));
				if(replacementDate!=null && !replacementDate.equals(""))
				serviceEntity.setReplacementDate(dateFormat.parse(replacementDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			if(serviceTime!=null && !serviceTime.equals(""))
	    	serviceEntity.setServiceTime(serviceTime);
			if(assetId!=0)
			serviceEntity.setAssetId(assetId);
			if(componentName!=null && !componentName.equals(""))
			serviceEntity.setComponentName(componentName);
			if(mfgNumber!=null && !mfgNumber.equals(""))
			serviceEntity.setMfgNo(mfgNumber);
			
			if(customerEntity!=null) {
				
				PersonInfo personinfo = new PersonInfo();
				if(customerEntity.isCompany()){
					personinfo.setFullName(customerEntity.getCompanyName());
					personinfo.setPocName(customerEntity.getFullname());
				}
				else{
					personinfo.setFullName(customerEntity.getFullname());
					personinfo.setPocName(customerEntity.getFullname());
				}
				personinfo.setCount(customerEntity.getCount());
				personinfo.setCellNumber(customerEntity.getCellNumber1());
				if(customerEntity.getEmail()!=null)
				personinfo.setEmail(customerEntity.getEmail());
				
				
			}
			if(customerBranchDetails!=null) {
				serviceEntity.setServiceBranch(customerBranchDetails.getBusinessUnitName());
			}
			if(branchEntity!=null) {
				serviceEntity.setBranch(branchEntity.getBusinessUnitName());
			}
			if(serviceproduct!=null) {
				serviceEntity.setProduct(serviceproduct);
			}
			if(completionDateTime!=null && !completionDateTime.equals("")) {
				try {
					Date completionDate = dateTimeFormat.parse(completionDateTime);
					if(completionDate!=null) {
										
						serviceEntity.setCompletedDate_time(completionDateTime);
						serviceEntity.setServiceCompletionDate(completionDate);
						serviceEntity.setStatus(Service.SERVICESTATUSCOMPLETED);
						
					}
					
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			
			if(complainServiceFlag) {
				serviceEntity.setServiceType(AppConstants.SERVICETYPECOMPLAIN);
			}
			if(serviceRemark!=null){
				serviceEntity.setRemark(serviceRemark);
			}
			
			ofy().save().entity(serviceEntity);
			logger.log(Level.SEVERE, "Service updated scuccessfully");
		}
		else {
			return "Service does not exist";
		}
		return "Service "+serviceEntity.getCount()+" updated successfully";
	}	

		
}
