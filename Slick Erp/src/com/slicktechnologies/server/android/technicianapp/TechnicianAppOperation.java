package com.slicktechnologies.server.android.technicianapp;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.ExpenseManagement;
import com.slicktechnologies.client.views.device.CatchTraps;
import com.slicktechnologies.client.views.device.PestTreatmentDetails;
import com.slicktechnologies.server.ApprovableServiceImplementor;
import com.slicktechnologies.server.GeneralServiceImpl;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.SingletoneNumberGeneration;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.EmployeeTrackingDetails;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.PettyCash.ApproverAmount;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.ServiceFinding;
import com.slicktechnologies.shared.TechnicianWareHouseDetailsList;
import com.slicktechnologies.shared.TechnicianWarehouseDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApproval;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApprovalDetails;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productgroup.ServiceProductGroupList;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TechnicianAppOperation extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 342457059044026847L;
	int expenseId=0;
	
	Logger logger = Logger.getLogger("TechnicianAppOperation.class");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		RegisterServiceImpl impl=new RegisterServiceImpl();
		impl.getClass();
		
		String urlCalled = req.getRequestURL().toString().trim();
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		
		String data=req.getParameter("data").trim();
		JSONObject jsonData = null;
		
		/**
		 * @author Anil @since 15-06-2021
		 * Thai character was not getting download in proper format
		 * raised by Rahul Tiwari
		 */
		resp.setContentType("text/plain; charset=UTF-8");
		resp.setCharacterEncoding("UTF-8");
		
		try {
			jsonData = new JSONObject(data);
			logger.log(Level.SEVERE , "jsonData " + jsonData);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String action=jsonData.optString("actionTask").trim();
		logger.log(Level.SEVERE , "task name " + action.trim());
		String response="";
		if(action.trim().equalsIgnoreCase("expenseCreation")){
			response=createExpenseDocument(jsonData,comp,req,resp);
		}else if(action.trim().equalsIgnoreCase("materialReturn")){
			response=createMaterialReturnDoc(comp,jsonData,req,resp);
		}else if(action.trim().equalsIgnoreCase("changeUsernameandPassword")){
			response=changeUsernameandPassword(comp,jsonData,req,resp);
		}else if(action.trim().equalsIgnoreCase("saveEmployeeDailyRoute")){ 
		//	response=saveEmployeeDailyRoute(comp,jsonData,req,resp);
		}else if(action.trim().equalsIgnoreCase("fecthEmployeeDailyRoute")){
		//	response=fetchEmployeeDailyRoute(comp,jsonData,req,resp);
		}else if(action.trim().equalsIgnoreCase("fecthUnapprovedExpense")){
			response=fecthUnapprovedExpense(comp,jsonData,req,resp);
		}else if(action.trim().contains("smsConfiguration")){
			/** date 9.3.2018 added by komal to get sms configuration **/
			response = fetchSmsData(jsonData ,comp , req , resp);
		}else if(action.trim().contains("loadTechnicianStock")){
			/** date 9.3.2018 added by komal to get technician stock **/
			logger.log(Level.SEVERE , "task name " + action.trim());
			response = fetchTechnicianStock(jsonData ,comp , req , resp);
		}else if(action.trim().contains("saveTrapDetails")){
			logger.log(Level.SEVERE , "task name " + action.trim());
			response = saveTrapDetails(jsonData ,comp , req , resp,null);
		}else if(action.trim().equalsIgnoreCase("saveEmployeeRoute")){ 
		//	response=saveEmployeeRoute(comp,jsonData,req,resp);
		}else if(action.trim().equalsIgnoreCase("fecthEmployeeRoute")){
		//	response=fetchEmployeeRoute(comp,jsonData,req,resp);
		}else{
			response="actionTask "+action+" is not present";
		}
		resp.getWriter().println(response);
		
	}

	private String fecthUnapprovedExpense(Company comp, JSONObject jsonData,
			HttpServletRequest req, HttpServletResponse resp) {
		List<MultipleExpenseMngt> expenseList;
		PettyCash pettyCash = ofy().load().type(PettyCash.class)
				.filter("employee", jsonData.optString("employeeName")).first()
				.now();
		expenseList=ofy().load().type(MultipleExpenseMngt.class).filter("companyId", comp.getCompanyId()).filter("pettyCashName", pettyCash.getPettyCashName()).list();
		logger.log(Level.SEVERE,"expenseList.size"+expenseList.size());
		JSONArray jArray=new JSONArray();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		for (MultipleExpenseMngt multipleExpenseMngt : expenseList) {
			JSONObject json=new JSONObject();
			try {
				json.put("expenseId",multipleExpenseMngt.getCount());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				json.put("expenseDate",sdf.format(multipleExpenseMngt.getCreationDate()));
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				json.put("expenseAmount",multipleExpenseMngt.getTotalAmount());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			JSONArray jsonArray=new JSONArray();
			for (ExpenseManagement expense : multipleExpenseMngt.getExpenseList()) {
				JSONObject obj=new JSONObject();
				try {
					obj.put("expenseAmt",expense.getAmount());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					obj.put("expenseType",expense.getExpenseType());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					obj.put("expenseGroup",expense.getExpenseGroup());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				jsonArray.put(obj);
			}
			try {
				json.put("expenseList",jsonArray);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			jArray.put(json);
		}
		String unApproveExpense=jArray.toString().replace("\\\\", "");
		return unApproveExpense;
	}

	private String fetchEmployeeDailyRoute(Company comp, JSONObject jsonData,
			HttpServletRequest req, HttpServletResponse resp) {
		Employee employee=ofy().load().type(Employee.class).filter("companyId", comp.getCompanyId()).filter("fullname", jsonData.optString("employeeName").trim()).first().now();
		logger.log(Level.SEVERE, "Employee Details: "+employee.getFullname()+" Route: "+employee.getDailyRoute());
		String employeeStr=employee.getDailyRoute();
		
		return employeeStr;
	}

	private String saveEmployeeDailyRoute(Company comp, JSONObject jsonData,
			HttpServletRequest req, HttpServletResponse resp) {
		// TODO Auto-generated method stub

		logger.log(Level.SEVERE, "jsonData::::::::::" + jsonData);
		Employee employee=ofy().load().type(Employee.class)
				.filter("companyId", comp.getCompanyId())
				.filter("fullname", jsonData.optString("employeeName").trim()).first().now();
		String employeeStr=employee.getDailyRoute();
		if(employeeStr!=null&&!employeeStr.equals("")){
			/*
			 * Name: Apeksha Gunjal
			 * Date: 06/09/2018 @ 14:44
			 * Note: changed if and else code, earlier it was placing on wrong place
			 */
			logger.log(Level.SEVERE, "employeeStr::::::::::" + employeeStr);
			JSONArray jArray = null;
			try {
				jArray = new JSONArray(employeeStr.trim());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block'
				e1.printStackTrace();
				logger.log(Level.SEVERE, "Error::::::::::" + e1);
			}
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			JSONObject jObj=new JSONObject();
			try {
				/*
				 * Name: Apeksha Gunjal
				 * Date: 06/09/2018 @ 14:44
				 * Added date and data separately in JsonObject.
				 */
				jObj.put("date",sdf.format(new Date()));
				jObj.put("data", jsonData.optJSONArray("data"));
				logger.log(Level.SEVERE, "jObj::::::::::" + jObj);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log(Level.SEVERE, "Error::::::::::" + e);
			}
			jArray.put(jObj);
			employee.setDailyRoute(jArray.toString().replace("\\\\", ""));
			ofy().save().entity(employee);
		}else{
			JSONArray jsonArray=new JSONArray();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			JSONObject jObj=new JSONObject();
			try {
				/*
				 * Name: Apeksha Gunjal
				 * Date: 06/09/2018 @ 14:44
				 * Added date and data separately in JsonObject.
				 */
				jObj.put("date",sdf.format(new Date()));
				jObj.put("data", jsonData.optJSONArray("data"));
				logger.log(Level.SEVERE, "jObj::::::::::" + jObj);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log(Level.SEVERE, "Error::::::::::" + e);
			}
			jsonArray.put(jObj);
			employee.setDailyRoute(jsonArray.toString().replace("\\\\", ""));
			ofy().save().entity(employee);
		}
		return null;
	}

	private String changeUsernameandPassword(Company comp, JSONObject jsonData,
			HttpServletRequest req, HttpServletResponse resp) {
		try{
			String username=jsonData.optString("username");
			boolean isUsernameChanged=jsonData.optBoolean("isUsernameChanged");
			boolean isPasswordChanged=jsonData.optBoolean("isPasswordChanged");
			String changedUsername=jsonData.optString("changedUsername");
			String changedPassword=jsonData.optString("changedPassword");
			User user=ofy().load().type(User.class).filter("userName", username.trim()).first().now();
			if(user!=null){
			if(isUsernameChanged){
				user.setUserName(changedUsername.trim());
				ofy().save().entity(user);
			}else if(isPasswordChanged){
				user.setPassword(changedPassword.trim());
				ofy().save().entity(user);
			}
			}else{
				return "No User avialable with username : "+username;
			}
			
			return "Successfull";
		}catch (Exception e){
			logger.log(Level.SEVERE, "Error::::::::::" + e);
			return "Failed";
		}
	}

	public String createMaterialReturnDoc(Company comp, JSONObject jsonData,
			HttpServletRequest req, HttpServletResponse resp) {
		ServerAppUtility appUtility=new ServerAppUtility();
		logger.log(Level.SEVERE , "createMaterialReturnDoc : 1" );
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		String user = jsonData.optString("employeeName").trim();
		String serviceId=jsonData.optString("serviceId").trim();
		Service service=ofy().load().type(Service.class).filter("companyId", comp.getCompanyId()).filter("count",Integer.parseInt(serviceId.trim())).first().now();
		String branch = service.getBranch();
		
		String jsonstring = jsonData.toString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE, "JSON DATA"+jsonstring);
		
		
		JSONArray materialArray = null;
		try {
			materialArray = jsonData.getJSONArray("materialDetails");
			logger.log(Level.SEVERE , "createMaterialReturnDoc : 2" );
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//		ServiceProject serviceProject=ofy().load().type(ServiceProject.class).filter("companyId", comp.getCompanyId()).filter("serviceId",Integer.parseInt(serviceId.trim())).first().now();
//		MaterialIssueNote min=ofy().load().type(MaterialIssueNote.class).filter("companyId", comp.getCompanyId()).filter("serviceId",Integer.parseInt(serviceId.trim())).first().now();
//		
//		User user=ofy().load().type(User.class).filter("companyId",comp.getCompanyId()).filter("userName",jsonData.optString("userName").trim()).first().now();
//		
//		if(min!=null){
//			MaterialMovementNote mmnEntity=ofy().load().type(MaterialMovementNote.class).filter("companyId", min.getCompanyId()).filter("mmnMinId", min.getCount()).first().now();
//			if(mmnEntity==null){
//			MaterialMovementNote mmn=new MaterialMovementNote();
//			mmn.setCompanyId(comp.getCompanyId());
//			mmn.setMmnMinId(min.getCount());
//			mmn.setMmnMinDate(min.getMinDate());
//			mmn.setOrderID(serviceProject.getContractId()+"");
//			mmn.setMmnTitle("Service ID-"+serviceId);
//			try {
//				mmn.setMmnDate(sdf.parse(sdf.format(new Date())));
//			} catch (ParseException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			mmn.setBranch(service.getBranch().trim());
//			if(user!=null){
//				mmn.setEmployee(user.getEmployeeName().trim());
//			}else{
//				mmn.setEmployee("");
//			}
//			mmn.setStatus(MaterialMovementNote.CREATED);
//			mmn.setMmnDescription("Ref MIN ID-"+min.getCount()+"/EVAPedio");
			
//			List<MaterialProduct> matList=new ArrayList<MaterialProduct>();
//			log("min.getProductTablemin().size()"+min.getSubProductTablemin().size());
//			
//			for (int i = 0; i < min.getSubProductTablemin().size(); i++) {
//				JSONObject obj = null;
//				try {
//					obj = materialArray.getJSONObject(i);
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				MaterialProduct matPrdt=new MaterialProduct();
//				matPrdt.setMaterialProductId(min.getSubProductTablemin().get(i).getMaterialProductId());
//				matPrdt.setMaterialProductCode(min.getSubProductTablemin().get(i).getMaterialProductCode());
//				matPrdt.setMaterialProductName(min.getSubProductTablemin().get(i).getMaterialProductName());
//				matPrdt.setMaterialProductAvailableQuantity(min.getSubProductTablemin().get(i).getMaterialProductAvailableQuantity());
//				matPrdt.setMatReqReOdrLvlQty(Double.parseDouble(obj.optString("materialReturnQty")));
//				matPrdt.setMaterialProductUOM(min.getSubProductTablemin().get(i).getMaterialProductUOM());
//				matPrdt.setMaterialProductWarehouse(min.getSubProductTablemin().get(i).getMaterialProductWarehouse());
//				matPrdt.setMaterialProductStorageLocation(min.getSubProductTablemin().get(i).getMaterialProductStorageLocation());
//				matPrdt.setMaterialProductStorageBin(min.getSubProductTablemin().get(i).getMaterialProductStorageBin());
//				
//			}
//			mmn.setSubProductTableMmn(matList);
//			
//			NumberGeneration ng = new NumberGeneration();
//			ng = ofy().load().type(NumberGeneration.class)
//					.filter("companyId", comp.getCompanyId())
//					.filter("processName", "MaterialMovementNote").filter("status", true)
//					.first().now();
//
//			long number = ng.getNumber();
//			int count = (int) number;
//			
//			mmn.setCount(count+1);
//			
//			ofy().save().entity(mmn);
//			ng.setNumber(number+1);
//			
//			ofy().save().entity(ng);
//			
//		}else{
//			return "Expense already registered for this service!!";
//		}
//		}else{
//			return "MIN not created for this service!!";
//		}
		String msgString = "";
		ServiceProject serProj = ofy().load().type(ServiceProject.class).filter("companyId", service.getCompanyId()).filter("serviceId", service.getCount())
				.filter("contractId", service.getContractCount()).first().now();

		if (serProj != null) {
			logger.log(Level.SEVERE , "createMaterialReturnDoc : 3" );
			if(serProj.getProdDetailsList().size()!=0){
				logger.log(Level.SEVERE , "createMaterialReturnDoc : 4" );
				MaterialIssueNote minObj = ofy().load().type(MaterialIssueNote.class).filter("companyId", service.getCompanyId()).filter("serviceId", service.getCount())
							.filter("minSoId", service.getContractCount() ).first().now();
				if(minObj!=null){
					msgString=msgString+"Min is already created for Service Id - "+service.getCount()+"\n";
					logger.log(Level.SEVERE , "createMaterialReturnDoc : 5" );
				}else{
					
					ReadMaterialAndDoMIN(serProj,materialArray,branch,user);
					
					logger.log(Level.SEVERE , "createMaterialReturnDoc : 6" );
					/**
					 * @author Vijay Chougule Date 30-09-2020
					 * Des :- below code added in above method to call from normal and Free material Scenarios
					 * 
					 */
//					HashMap<String, Double> productMap = new HashMap<String , Double>();
//						JSONObject obj = null;
//						for(int i =0 ; i<materialArray.length() ; i++){
//						try {
//							obj = materialArray.getJSONObject(i);
//							logger.log(Level.SEVERE , "material qty :" + obj.optString("returnQty") +" "+
//									obj.optString("materialName"));
//							double qty = 0;
//							try{
//								qty = Double.parseDouble(obj.optString("returnQty"));
//							}catch(Exception e){
//								qty = 0;
//								logger.log(Level.SEVERE , "material qty :" + qty);
//							}
//							
//							String productName = obj.optString("materialName").trim();
//							/**
//							 * @author Anil , Date : 28-01-2020
//							 */
//							logger.log(Level.SEVERE , "Befor Material Name :" + productName);
//							productName=appUtility.decodeText(productName);
//							logger.log(Level.SEVERE , "After Material Name :" + productName);
//							/**
//							 * End
//							 */
//							
//							
//							productMap.put(productName, qty);
//						} catch (JSONException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
//					 }
//					List<ProductGroupList> list = new ArrayList<ProductGroupList>();
//					for(ProductGroupList details :serProj.getProdDetailsList())	{
//						if(productMap.containsKey(details.getName().trim())){
//							//details.setPlannedQty(details.getQuantity());
//							details.setReturnQuantity(productMap.get(details.getName().trim()));
//							//details.setQuantity(productMap.get(details.getName().trim()));
////							ProductInventoryViewDetails pvDetails = ofy().load().type(ProductInventoryViewDetails.class).filter("companyId", service.getCompanyId())
////									                               .filter("prodname", details.getName().trim())
////									                               .filter("warehousename", details.getParentWarentwarehouse())
////									                               .filter("storagelocation", details.getParentStorageLocation())
////									                               .filter("storagebin", details.getParentStorageBin()).first().now();
////							
////							if(pvDetails != null){
////								double quantity = productMap.get(details.getName().trim());
////								if((pvDetails.getAvailableqty() - quantity) < 0){
////									error += "Insufficient available quantity for product "+ details.getName().trim() + " /";
////								}
////							}
//						}
//						list.add(details);
//					}
//					serProj.setProdDetailsList(list);
//					ofy().save().entity(serProj);
//					
//					
//					Gson gson = new GsonBuilder().create();
//					String str = gson.toJson(serProj, ServiceProject.class);
//					
////			        Queue queue = QueueFactory.getQueue("documentCancellation-queue");
//					 Queue queue = QueueFactory.getQueue("CreateMINQueue-queue");
//			        String typeducumentName = "CreateMIN"+"$"+serProj.getCompanyId()+"$"+str+"$"+branch+"$"+user;
//			   	  	logger.log(Level.SEVERE, "Company Id "	+ typeducumentName);
//			   	  	queue.add(TaskOptions.Builder.withUrl("/slick_erp/CreateMINTaskQueue").param("taskKeyAndValue", typeducumentName));
//					
				}					
				
			}else{
				
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICEMODULE,AppConstants.PC_FREEMATERIAL , comp.getCompanyId()) ){
					logger.log(Level.SEVERE , "Project does not having material so project will created for Service Free material" );
					ofy().delete().entity(serProj).now();
					createProject(service,materialArray,branch,user);
				}
				else{
					msgString=msgString+"Service Id - "+service.getCount()+" / Product is not added in Project Id -"+serProj.getCount()+"\n";
					return msgString;
				}
			}
			
			
		} else {
			logger.log(Level.SEVERE , "Project is not created for Service" );

			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICEMODULE,AppConstants.PC_FREEMATERIAL , comp.getCompanyId()) ){
				logger.log(Level.SEVERE , "Project is not created for Service Free material" );
				createProject(service,materialArray,branch,user);
			}
			else{
				msgString=msgString+"Project is not created for Service Id - "+service.getCount()+"\n";
				return msgString;
			}
//			msgString=msgString+"Project is not created for Service Id - "+service.getCount()+"\n";
//			return msgString;
		}
		return "Successfull";
	}

	

	

	private String createExpenseDocument(JSONObject jsonData, Company comp, HttpServletRequest req, HttpServletResponse resp) {
		Exception exp=null;
		double allocationAmount=0;
		String pettyCashName="";
		boolean flag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("MultipleExpenseMngt", "DirectApprovalRequest", comp.getCompanyId());
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));

			MultipleExpenseMngt expense = new MultipleExpenseMngt();
			if(jsonData.optString("innerActionTask").trim().equals("update")){
				expense=ofy().load().type(MultipleExpenseMngt.class).filter("companyId", comp.getCompanyId()).filter("count", Integer.parseInt(jsonData.optString("expenseId"))).first().now();
			}
			expense.setCompanyId(comp.getCompanyId());
			expense.setBranch(jsonData.optString("branch"));
			expense.setServiceId(Integer.parseInt(jsonData.optString(
					"serviceId").trim()));
			expense.setServiceId(Integer.parseInt(jsonData.optString(
					"serviceId").trim()));
			User user = ofy().load().type(User.class)
					.filter("companyId", comp.getCompanyId())
					.filter("userName", jsonData.optString("userName").trim())
					.first().now();

			if(expense.getBranch() == null || expense.getBranch().equals("")){
				if(user != null){
					expense.setBranch(user.getBranch());
				}
			}	
			JSONArray multipleExpenseArray;
			try {
				multipleExpenseArray = jsonData.getJSONArray("multipleExpense");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "Error : Parsing Expense Details";
			}
			ArrayList<ExpenseManagement> expenseManagementList = new ArrayList<ExpenseManagement>();
			for (int i = 0; i < multipleExpenseArray.length(); i++) {
				ExpenseManagement expenseMan = new ExpenseManagement();
				
				JSONObject jsonObject = null;
				try {
					jsonObject = multipleExpenseArray.getJSONObject(i);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if (!jsonObject.optString("invoiceId").trim().equals(""))
					expenseMan.setInvoiceNumber(jsonObject.optString(
							"invoiceId").trim());

				if (!jsonObject.optString("expenseDate").trim().equals("")) {
					try {
						expenseMan.setExpenseDate(sdf.parse(jsonObject
								.optString("expenseDate").trim()));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						return "Error parsing Expense Date!!";
					}
				}

				if (!jsonObject.optString("expenseGroup").trim().equals(""))
					expenseMan.setExpenseGroup(jsonObject.optString(
							"expenseGroup").trim());

				if (!jsonObject.optString("expenseCategory").trim().equals(""))
					expenseMan.setExpenseCategory(jsonObject.optString(
							"expenseCategory").trim());

				if (!jsonObject.optString("expenseType").trim().equals(""))
					expenseMan.setExpenseType(jsonObject.optString("expenseType").trim());

				// if(!jsonObject.optString("expenseGroup").trim().equals(""))
				// expenseMan.setExpenseCategory(jsonObject.optString("expenseType").trim());

				if (!jsonObject.optString("paymentMethod").trim().equals(""))
					expenseMan.setPaymentMethod(jsonObject.optString(
							"paymentMethod").trim());

				if (!jsonObject.optString("vendor").trim().equals(""))
					expenseMan.setVendor(jsonObject.optString("vendor").trim());

				logger.log(Level.SEVERE , "expense amount 1 : " + jsonObject.optString("amount").trim());
				if (!jsonObject.optString("amount").trim().equals(""))
					expenseMan.setAmount(jsonObject.optDouble("amount"));
				logger.log(Level.SEVERE , "expense amount : " + expenseMan.getAmount());
				// if(!jsonObject.optString("expenseGroup").trim().equals(""))
				expenseMan.setEmployee(user.getEmployeeName().trim());
				
				/**
				 * @author Anil , Date : 24-08-2019
				 */
				
				if (!jsonObject.optString("deptDate").trim().equals("")){
					try {
						expenseMan.setFromDate(sdf.parse(jsonObject.optString("deptDate").trim()));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						return "Error parsing Dept Date!!";
					}
				}
				if (!jsonObject.optString("deptCity").trim().equals(""))
					expenseMan.setFromCity(jsonObject.optString("deptCity"));
				if (!jsonObject.optString("arrvDate").trim().equals("")){
					try {
						expenseMan.setToDate(sdf.parse(jsonObject.optString("arrvDate").trim()));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						return "Error parsing Arrv Date!!";
					}
				}
				if (!jsonObject.optString("arrvCity").trim().equals(""))
					expenseMan.setToCity(jsonObject.optString("arrvCity"));
				if (!jsonObject.optString("remark").trim().equals(""))
					expenseMan.setRemark(jsonObject.optString("remark"));
				if (!jsonObject.optString("kmTravelled").trim().equals(""))
					expenseMan.setKmTravelled(jsonObject.optDouble("kmTravelled"));
				if (!jsonObject.optString("employeeName").trim().equals(""))
					expenseMan.setEmployee(jsonObject.optString("employeeName"));
				if (!jsonObject.optString("employeeRole").trim().equals(""))
					expenseMan.setEmployeeRole(jsonObject.optString("employeeRole"));
				if (!jsonObject.optString("employeeId").trim().equals(""))
					expenseMan.setEmployeeId(jsonObject.optInt("employeeId"));
				if (!jsonObject.optString("employeeCell").trim().equals(""))
					expenseMan.setEmployeeCell(jsonObject.optLong("employeeCell"));
				
				expenseManagementList.add(expenseMan);
			}
			expense.setExpenseList(expenseManagementList);
			expense.setTotalAmount(jsonData.optDouble("totalAmount"));
			expense.setStatus(MultipleExpenseMngt.CREATED);
			expense.setExpTypeAsPettyCash(true);
			
			expense.setEmployee(expenseManagementList.get(0).getEmployee());
			expense.setEmpId(expenseManagementList.get(0).getEmployeeId());
			expense.setEmpCell(expenseManagementList.get(0).getEmployeeCell());
			expense.setRemark(expenseManagementList.get(0).getRemark());
			expense.setEmployeeRole(expenseManagementList.get(0).getEmployeeRole());
			if(flag){
				expense.setStatus(MultipleExpenseMngt.REQUESTED);
			}
			PettyCash pettyCash = ofy().load().type(PettyCash.class)
					.filter("employee", user.getEmployeeName().trim()).first()
					.now();
			if (pettyCash != null) {
				allocationAmount=pettyCash.getPettyCashBalance();
				pettyCashName=pettyCash.getPettyCashName();			
				expense.setPettyCashName(pettyCash.getPettyCashName());				
			} else {
				return "No Petty Cash Found!!";
			}
			
			
			double amount=jsonData.optDouble("totalAmount");
			
			Approvals approval=null;
			if(!jsonData.optString("actionTask").trim().equals("update")){
				
				NumberGeneration ng = new NumberGeneration();
				ng = ofy().load().type(NumberGeneration.class)
						.filter("companyId", comp.getCompanyId())
						.filter("processName", "MultipleExpenseMngt")
						.filter("status", true).first().now();

				long number = ng.getNumber();
				int count = (int) number;
				expenseId=count+1;
				expense.setCount(count + 1);
				
				approval=SendApprovalRequestAndReturnApproverName(expense, pettyCash, amount);
				if(approval!=null) {
					expense.setApproverName(approval.getApproverName());
					logger.log(Level.SEVERE ,"Approval name set to expense");
				}

				ofy().save().entity(expense);
				logger.log(Level.SEVERE ,"Expense created");
				ng.setNumber(number + 1);

				ofy().save().entity(ng);
			}else{
				ofy().save().entity(expense);
			}
			
			if(flag){
				if(approval!=null) {
					GenricServiceImpl impl = new GenricServiceImpl();
					impl.save(approval);
					logger.log(Level.SEVERE ,"Approval request sent");
				}else {
					logger.log(Level.SEVERE ,"Approval request cannot be sent as there is error creating approval request");
				}
//				SendExpeneseApproval(expense, null);
			}
		}catch(Exception e){
			exp=e;
			e.printStackTrace();
			logger.log(Level.SEVERE,"Error::"+e);
			return "Failed";
		}
		if(exp==null){
			JSONObject obj=new JSONObject();
			double unApprovedAmount=0;
			List<MultipleExpenseMngt> expenseList=ofy().load().type(MultipleExpenseMngt.class).filter("companyId", comp.getCompanyId()).filter("pettyCashName", pettyCashName.trim()).filter("status", MultipleExpenseMngt.CREATED).list();
			for (MultipleExpenseMngt multipleExpenseMngt : expenseList) {
				unApprovedAmount=unApprovedAmount+multipleExpenseMngt.getTotalAmount();
			}
			try {
				obj.put("allocatedAmount", allocationAmount);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				obj.put("balanceAmount", allocationAmount-unApprovedAmount);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				obj.put("unApprovedAmount", unApprovedAmount);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				obj.put("expenseId",expenseId+"");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String jsonString=obj.toString().replace("\\\\", "");
			return jsonString;
		}else{
			return "Failed";
		}
	}
	
	/** date 9.3.2018 added by komal to get sms configuration **/
	private String  fetchSmsData(JSONObject jsonData , Company comp, HttpServletRequest req, HttpServletResponse resp){

		JSONObject obj=new JSONObject();
		SmsConfiguration smsConfig = ofy().load().type(SmsConfiguration.class).filter("companyId", comp.getCompanyId()).filter("status", true).first().now();
		Employee employee=ofy().load().type(Employee.class)
				.filter("companyId", comp.getCompanyId())
				.filter("fullname", jsonData.optString("employeeName").trim()).first().now();
		Branch branch = null;
		if(employee != null){
			branch = ofy().load().type(Branch.class)
					.filter("companyId", comp.getCompanyId()).filter("buisnessUnitName", employee.getBranchName()).first().now();
		}
		if(smsConfig != null){
		try {
			obj.put("fromNumber", comp.getCellNumber1());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			if(branch != null){
			obj.put("toNumber", branch.getPocCell());
			}else{
				obj.put("toNumber", comp.getPocCell());
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			obj.put("accKey", smsConfig.getAccountSID());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			obj.put("username",smsConfig.getAuthToken());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			obj.put("password",smsConfig.getPassword());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			obj.put("companyName",comp.getBusinessUnitName());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String jsonString=obj.toString().replace("\\\\", "");
		return jsonString;
		}else{
			return "Please add sms configurations in ERP.";
		}
	
	}
	/** date 9.3.2018 added by komal to get technician stock **/
	private String fetchTechnicianStock(JSONObject jsonData , Company comp, HttpServletRequest req, HttpServletResponse resp){
		
		TechnicianWareHouseDetailsList technicianWareHouseDetailsList = ofy().load().type(TechnicianWareHouseDetailsList.class).
				filter("companyId", comp.getCompanyId()).filter("wareHouseList.technicianName",  jsonData.optString("employeeName").trim()).first().now();
		
		if(technicianWareHouseDetailsList != null){
			String warehouseName = "" ,storageLocation = "", storageBin = "";
			for(TechnicianWarehouseDetails details : technicianWareHouseDetailsList.getWareHouseList()){
				if(details.getTechnicianName().equalsIgnoreCase(jsonData.optString("employeeName").trim())){
					warehouseName = details.getTechnicianWareHouse();
					storageLocation = details.getTechnicianStorageLocation();
					storageBin = details.getTechnicianStorageBin();
					break;
				}
			}
			
			List<ProductInventoryViewDetails> productDetailsList = ofy().load().type(ProductInventoryViewDetails.class).
					filter("companyId", comp.getCompanyId()).filter("warehousename", warehouseName).
					filter("storagelocation", storageLocation).filter("storagebin", storageBin).list();
			JSONArray array = new JSONArray();
			for(ProductInventoryViewDetails view :productDetailsList ){
				SuperProduct product = ofy().load().type(SuperProduct.class).filter("companyId", comp.getCompanyId()).
						filter("count", view.getProdid()).first().now();
				JSONObject obj=new JSONObject();
				if(view.getAvailableqty() > 0){
				try {
					obj.put("productName", view.getProdname());
				}catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				try {
					if(product != null){
						obj.put("UOM", product.getUnitOfMeasurement());
					}else{
						obj.put("UOM", "");
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				try {
					obj.put("qty", view.getAvailableqty());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/**
				 * @author Vijay Chougule
				 * Des :- 25-09-2020 needed for Free material product code and Id
				 */
				try {
					obj.put("productCode", view.getProdcode());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					obj.put("productId", view.getProdid());
				} catch (Exception e) {
					// TODO: handle exception
				}
				/**
				 * ends here
				 */
				
				array.put(obj);
				}
			}
			if(array.length() >0){
				String jsonString=array.toString().replace("\\\\", "");
				return jsonString;
			}else{
				return "No result found.";
			}
			
		}else{
			return "Please assign warehouse to technician.";
		}
	}
	/**
	 * @author Anil , Date : 07-02-2020
	 * Added new parameter Service object ,as same method is called while completing service
	 * in that case it loads data in new object and update trap details in it and save but trap details were not 
	 * updated on existing object.
	 */
	public String saveTrapDetails(JSONObject jsonData , Company comp, HttpServletRequest req, HttpServletResponse resp, Service serviceObj){
		String serviceId=jsonData.optString("serviceId").trim();
		String type = jsonData.optString("type");
		Service service=null;
		if(serviceObj==null){
			service=ofy().load().type(Service.class).filter("companyId", comp.getCompanyId()).filter("count",Integer.parseInt(serviceId.trim())).first().now();
		}
		JSONArray catchTrapArray = null;
		try {
			catchTrapArray = jsonData.getJSONArray("trapDetails");
			
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		List<CatchTraps> catchTrapList = new ArrayList<CatchTraps>();
//		if(service.getCatchtrapList() != null && service.getCatchtrapList().size() > 0){
//			catchTrapList = service.getCatchtrapList();
//		}
		JSONObject obj = null;
		for(int i =0 ; i<catchTrapArray.length() ; i++){
			try {
				obj = catchTrapArray.getJSONObject(i);
				if(type.equalsIgnoreCase("Fumigation")){
					String pestName = obj.optString("name");
					String number = obj.optString("number");
					String size = obj.optString("size");				
					CatchTraps trap = new CatchTraps();
					trap.setPestName(pestName);
					trap.setContainerNo(number);
					trap.setContainerSize(size);
					catchTrapList.add(trap);
				} else {
					String pestName = obj.optString("name");					
					int count = 0;
					if(obj.optString("count")!=null&&!obj.optString("count").equals(""))
						count=Integer.parseInt(obj.optString("count"));
					
					String location = obj.optString("location");
					String catchLevel="";
					try {
						catchLevel=obj.optString("catchLevel");
					}catch(Exception e){
						e.printStackTrace();
					}
					CatchTraps trap = new CatchTraps();
					trap.setPestName(pestName);
					trap.setCount(count);
					trap.setLocation(location);
					trap.setCatchLevel(catchLevel);

					catchTrapList.add(trap);
					logger.log(Level.SEVERE, "pestName="+pestName+" catchTrapList size="+catchTrapList.size());
					
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		/*
		 * Ashwini Patil 
		 * Date: 4-12-2024
		 * Pest o touch pest trend GDS is showing duplicate rows as finding details are stored in list format in service entity
		 * So we have created new entity for serviceFindings
		 */
		
		if(serviceObj==null){
			service.setCatchtrapList(catchTrapList);
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "PC_PedioFindingAsServiceQuantityInBilling", service.getCompanyId())) {
				if(catchTrapList!=null && catchTrapList.size()>0) {
					service.setQuantity(catchTrapList.get(0).getCount());
					updateFindingsEntity(service, catchTrapList);
				}
			}
			ofy().save().entity(service);
		}else{
			serviceObj.setCatchtrapList(catchTrapList);
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "PC_PedioFindingAsServiceQuantityInBilling", serviceObj.getCompanyId())) {
				if(catchTrapList!=null && catchTrapList.size()>0) {
					serviceObj.setQuantity(catchTrapList.get(0).getCount());
					updateFindingsEntity(serviceObj, catchTrapList);
				}
			}
		}
		
		return "Success";
	}
	private String fetchEmployeeRoute(Company comp, JSONObject jsonData,
			HttpServletRequest req, HttpServletResponse resp) {
//		Employee employee=ofy().load().type(Employee.class).filter("companyId", comp.getCompanyId()).filter("fullname", jsonData.optString("employeeName").trim()).first().now();
//		logger.log(Level.SEVERE, "Employee Details: "+employee.getFullname()+" Route: "+employee.getDailyRoute());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		format.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		Date fromDate = null , toDate = null;
		String technicianName = "";
		String employeeStr= "";
		try{
			fromDate = sdf.parse(jsonData.optString("fromDate"));
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
			toDate = sdf.parse(jsonData.optString("toDate"));
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
			technicianName = jsonData.optString("technicianName");
		}catch(Exception e){
			technicianName = "";
			e.printStackTrace();
		}
		List<EmployeeTrackingDetails> trackList = new ArrayList<EmployeeTrackingDetails>();
		if(fromDate != null && toDate == null){
			toDate = fromDate;
		}
		if(fromDate == null && toDate != null){
			fromDate = toDate;
		}
		if(fromDate != null && toDate != null && !technicianName.equals("")){
			logger.log(Level.SEVERE, "CONDITION1");
			trackList = ofy().load().type(EmployeeTrackingDetails.class).filter("companyId", comp.getCompanyId()).
					filter("timeFormat >=" , fromDate).filter("timeFormat <=", toDate).filter("employeeName", technicianName).list();
		}else if(fromDate == null && toDate == null && !technicianName.equals("")){
			logger.log(Level.SEVERE, "CONDITION2");
			trackList = ofy().load().type(EmployeeTrackingDetails.class).filter("companyId", comp.getCompanyId()).
				filter("employeeName", technicianName).list();
		}else if(fromDate != null && toDate != null && technicianName.equals("")){
			
			logger.log(Level.SEVERE, "CONDITION3");
			trackList = ofy().load().type(EmployeeTrackingDetails.class).filter("companyId", comp.getCompanyId()).
					filter("timeFormat >=" , fromDate).filter("timeFormat <=", toDate).list();
		}
		JSONArray array = new JSONArray();
		JSONObject obj = new JSONObject();
		
		
		
		if(trackList != null && trackList.size() > 0){
			Comparator<EmployeeTrackingDetails> compServicelist = new Comparator<EmployeeTrackingDetails>() {
				
				@Override
				public int compare(EmployeeTrackingDetails s1, EmployeeTrackingDetails s2) {
					
					Date date = s1.getTimeFormat();
					Date date2 = s2.getTimeFormat();
					
					return date.compareTo(date2);
				}
			};
			Collections.sort(trackList,compServicelist);
			
			
			for(EmployeeTrackingDetails details : trackList){
				obj = new JSONObject();
				try {
					obj.put("employeeId", details.getEmployeeId()+"");
					obj.put("serviceId", details.getServiceId()+"");
					obj.put("contractId", details.getContractId()+"");
					obj.put("employeeName", details.getEmployeeName());
					obj.put("latitude", details.getLatitude());
					obj.put("longitude", details.getLongitude());
					obj.put("address", details.getAddress());
					obj.put("time", details.getTime());
					obj.put("branch", details.getBranch());
					obj.put("mobileNo", details.getMobileNo()+"");
					obj.put("imeiNumber", details.getImeiNumber());
					obj.put("date",sdf.format(details.getDate())+"");
					obj.put("timeFormat",format.format(details.getTimeFormat()));
					array.put(obj);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
		JSONObject jobj = new JSONObject();
		try {
			jobj.put("employeeRoute", array);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		employeeStr = jobj.toString().replace("\\\\", "");
		return employeeStr;
	}

	private String saveEmployeeRoute(Company comp, JSONObject jsonData,
			HttpServletRequest req, HttpServletResponse resp) {
		// TODO Auto-generated method stub
		GenricServiceImpl impl = new GenricServiceImpl();
		logger.log(Level.SEVERE, "jsonData::::::::::" + jsonData);
		
		
	//		logger.log(Level.SEVERE, "employeeStr::::::::::" + employeeStr);
			JSONArray jArray = null;
			try {
				jArray =jsonData.getJSONArray("employeeRoute");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block'
				e1.printStackTrace();
				logger.log(Level.SEVERE, "Error::::::::::" + e1);
			}
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			format.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			for(int i =0 ; i < jArray.length() ; i++){
				JSONObject jObj = null;
				try {
					jObj = jArray.getJSONObject(i);
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				int employeeId = 0 , serviceId = 0 , contractId = 0;
				String employeeName = "" , latitude = "" ,longitude = "" , address = "" , time = "" , branch = "";
				long mobileNo = 0 , imeiNumber = 0;
				Date date = null , timeFormat = null;

				EmployeeTrackingDetails details = new EmployeeTrackingDetails();
				try{
					employeeId = Integer.parseInt(jObj.optString("employeeId"));
				}catch(Exception e){
					e.printStackTrace();
				}
				details.setEmployeeId(employeeId);

				try{
					serviceId = Integer.parseInt(jObj.optString("serviceId"));
				}catch(Exception e){
					e.printStackTrace();
				}
				details.setServiceId(serviceId);
				
				try{
					contractId = Integer.parseInt(jObj.optString("contractId"));
				}catch(Exception e){
					e.printStackTrace();
				}
				details.setContractId(contractId);
				
				try{
					employeeName = jObj.optString("employeeName");
				}catch(Exception e){
					e.printStackTrace();
				}
				details.setEmployeeName(employeeName);

				try{
					latitude = jObj.optString("latitude");
				}catch(Exception e){
					e.printStackTrace();
				}
				details.setLatitude(latitude);
				
				try{
					longitude = jObj.optString("longitude");
				}catch(Exception e){
					e.printStackTrace();
				}
				details.setLongitude(longitude);
				try{
					address = jObj.optString("address");
				}catch(Exception e){
					e.printStackTrace();
				}
				details.setAddress(address);
						
				try{
					time = jObj.optString("time");
				}catch(Exception e){
					e.printStackTrace();
				}
				details.setTime(time);
				
				try{
					branch = jObj.optString("branch");
				}catch(Exception e){
					e.printStackTrace();
				}
				details.setBranch(branch);
				try{
					mobileNo = Long.parseLong(jObj.optString("mobileNo"));
				}catch(Exception e){
					e.printStackTrace();
				}
				details.setMobileNo(mobileNo);
				
				try{
					imeiNumber = Long.parseLong(jObj.optString("imeiNumber"));
				}catch(Exception e){
					e.printStackTrace();
				}
				details.setImeiNumber(imeiNumber);
				
				try{
					date = sdf.parse(jObj.optString("date"));
				}catch(Exception e){
					e.printStackTrace();
				}
				
				try{
					timeFormat = format.parse(jObj.optString("timeFormat"));
				}catch(Exception e){
					e.printStackTrace();
				}
				details.setTimeFormat(timeFormat);
				details.setDate(date);
				details.setCompanyId(comp.getCompanyId());
				impl.save(details);
			}
			
			
		return "success";
	}
	
	 public void SendExpeneseApproval(MultipleExpenseMngt mngt , Approvals approval){

		MultilevelApproval multilevelApproval = ofy().load().type(MultilevelApproval.class)
				.filter("companyId",mngt.getCompanyId()).filter("documentType",AppConstants.EXPENSEMANAGEMET)
				.first().now();
		String approvarName = "";
		int approvaleLevel = 1;
		logger.log(Level.SEVERE, "app " +multilevelApproval ) ;
		if(multilevelApproval != null){
			if(approval == null){
				for(MultilevelApprovalDetails details: multilevelApproval.getApprovalLevelDetails()){
					if(details.getLevel().equalsIgnoreCase("1")){
					//	approvarName = details.getEmployeeName();
						Employee employee=ofy().load().type(Employee.class)
								.filter("companyId", mngt.getCompanyId())
								.filter("fullname",  mngt.getEmployee()).first().now();
						if(employee != null && employee.getReportsTo() != null && !employee.getReportsTo().equalsIgnoreCase("")){
							approvarName = employee.getReportsTo();
							logger.log(Level.SEVERE, "app 1 " +approvarName ) ;
						}else{
							approvarName = details.getEmployeeName();
						}
						logger.log(Level.SEVERE, "app 2 " +approvarName ) ;
						break;
					}
				}
			}else{
				approvaleLevel = approval.getApprovalLevel() + 1;
				for(MultilevelApprovalDetails details: multilevelApproval.getApprovalLevelDetails()){
					logger.log(Level.SEVERE, "app 2 " +details.getLevel()) ;
					Employee employee=ofy().load().type(Employee.class)
							.filter("companyId", mngt.getCompanyId())
							.filter("fullname",  approval.getApproverName()).first().now();
					if(employee != null && employee.getReportsTo() != null && !employee.getReportsTo().equalsIgnoreCase("")){
						approvarName = employee.getReportsTo();
						logger.log(Level.SEVERE, "app 1 " +approvarName ) ;
					}else{
						if(details.getLevel().equalsIgnoreCase(approvaleLevel+"")){
							approvarName = details.getEmployeeName();
							logger.log(Level.SEVERE, "app 3 " +approvarName ) ;
							break;
						}
					}
				}
			}
		}
		if(approval != null){
			approval.setStatus(MultipleExpenseMngt.APPROVED);
			approval.setRemark(approval.getRemark());
			ofy().save().entity(approval);
		}
	//	if(multilevelApproval == null || approval == null){
			if(!approvarName.equals("")){
				approval=new Approvals();
				approval.setCompanyId(mngt.getCompanyId());
				approval.setApproverName(approvarName);
				approval.setBranchname(mngt.getBranch());
				approval.setRequestedBy(mngt.getEmployee());
				approval.setPersonResponsible(mngt.getEmployee());
				approval.setDocumentCreatedBy(mngt.getEmployee());		
				approval.setApprovalLevel(approvaleLevel);
				approval.setStatus(Approvals.PENDING);
				approval.setBusinessprocesstype(AppConstants.EXPENSEMANAGEMET);
				approval.setBpId(mngt.getEmpId()+"");
				approval.setBpName(mngt.getEmployee());
				approval.setDocumentValidation(false);
				approval.setBusinessprocessId(mngt.getCount());
				GenricServiceImpl impl = new GenricServiceImpl();
				impl.save(approval);
			}else{
				ApprovableServiceImplementor impl=new ApprovableServiceImplementor();
				String result=impl.sendApproveRequest(approval);
				logger.log(Level.SEVERE,"Notification:result: "+result);
				//resp.getWriter().println(result);
			}		
	}
	 
	 
	 /**
	  * @author Anil
	  * @since 04-07-2020
	  * @param jsonData
	  * @param comp
	  * @param req
	  * @param resp
	  * @param serviceObj
	  * @return
	  */
	 public String savePestTreatmentDetails(JSONObject jsonData , Company comp, HttpServletRequest req, HttpServletResponse resp, Service serviceObj){
		 JSONArray pestTreatmentArray = null;
			try {
				pestTreatmentArray = jsonData.getJSONArray("pestTreatmentDetails");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			try {
			List<PestTreatmentDetails> pestTreatmentList = new ArrayList<PestTreatmentDetails>();
			JSONObject obj = null;
			for(int i =0 ; i<pestTreatmentArray.length() ; i++){
				try {
					obj = pestTreatmentArray.getJSONObject(i);
					String treatmentArea = obj.optString("treatmentArea");
					String methodOfApplication = obj.optString("methodOfApplication");
					String pestTarget = obj.optString("pestTarget");
					PestTreatmentDetails pest = new PestTreatmentDetails();
					pest.setTreatmentArea(treatmentArea);
					pest.setMethodOfApplication(methodOfApplication);
					pest.setPestTarget(pestTarget);
					pestTreatmentList.add(pest);
				}catch(Exception e){
					
				}
			}
			serviceObj.setPestTreatmentList(pestTreatmentList);
			
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			return "Success";
		}
	 
	 
	 /**
	  * @author Anil
	  * @since 04-07-2020
	  * @param jsonData
	  * @param comp
	  * @param req
	  * @param resp
	  * @param serviceObj
	  * @return
	  */
	 public String saveChecklistDetails(JSONObject jsonData , Company comp, HttpServletRequest req, HttpServletResponse resp, Service serviceObj){
			JSONArray checklistArray = null;
			logger.log(Level.SEVERE, "inside cheklist save details method");
			try {
				checklistArray = jsonData.getJSONArray("checkListDetails");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			List<CatchTraps> checklist = new ArrayList<CatchTraps>();
			JSONObject obj = null;
			for(int i =0 ; i<checklistArray.length() ; i++){
				try {
					obj = checklistArray.getJSONObject(i);
					String name = obj.optString("name");
					String remark = obj.optString("remark");
					CatchTraps check = new CatchTraps();
					check.setPestName(name);
					check.setLocation(remark);
					
					try {
						
						String status = obj.optString("status");
						logger.log(Level.SEVERE, "status"+status);
						if(status!=null && !status.equals("") && status.equals("TRUE")){
							check.setStatus(true);
						}
						else{
							check.setStatus(false);
						}
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					
					checklist.add(check);
				}catch(Exception e){
					
				}
			}
			serviceObj.setCheckList(checklist);
			return "Success";
	}
	 
	 private void createProject(Service serviceEntity, JSONArray materialArray, String branch, String user) {

			if(serviceEntity!=null){
				
				ArrayList<EmployeeInfo> technicianlist = new ArrayList<EmployeeInfo>();
				Employee empEntity = ofy().load().type(Employee.class).filter("companyId", serviceEntity.getCompanyId()).filter("fullname", serviceEntity.getEmployee())
										.first().now();
				logger.log(Level.SEVERE,"empEntity "+empEntity);
				if(empEntity!=null){
					EmployeeInfo empInfoEntity = new EmployeeInfo();
					empInfoEntity.setEmpCount(empEntity.getCount());
			  		empInfoEntity.setFullName(empEntity.getFullName());
			      	empInfoEntity.setCellNumber(empEntity.getCellNumber1());
			      	empInfoEntity.setDesignation(empEntity.getDesignation());
			      	empInfoEntity.setDepartment(empEntity.getDepartMent());
			      	empInfoEntity.setEmployeeType(empEntity.getEmployeeType());
			      	empInfoEntity.setEmployeerole(empEntity.getRoleName());
			      	empInfoEntity.setBranch(empEntity.getBranchName());
			      	empInfoEntity.setCountry(empEntity.getCountry());		
			      	technicianlist.add(empInfoEntity);
			      	
			      	logger.log(Level.SEVERE,"user "+user);

			    	List<TechnicianWareHouseDetailsList> technicianWarehouselist = ofy().load().type(TechnicianWareHouseDetailsList.class)
	      					.filter("wareHouseList.technicianName", user).list();
			      	logger.log(Level.SEVERE,"technicianWarehouselist "+technicianWarehouselist.size());
			      	
			      	String warehouseName = "" ,storageLocation = "", storageBin = "",
							parentWareHouse = "" , parentStorageLocation = "" , parentStorageBin = "";
					if(technicianWarehouselist.size()!=0){
					
						for(TechnicianWarehouseDetails details : technicianWarehouselist.get(0).getWareHouseList()){
							if(details.getTechnicianName().equalsIgnoreCase(serviceEntity.getEmployee())){
								warehouseName = details.getTechnicianWareHouse();
								storageLocation = details.getTechnicianStorageLocation();
								storageBin = details.getTechnicianStorageBin();
								parentWareHouse = details.getParentWareHouse();
								parentStorageLocation = details.getParentStorageLocation();
								parentStorageBin = details.getPearentStorageBin();
								break;
							}
						}
					}
			
					ServerAppUtility appUtility=new ServerAppUtility();
					ArrayList<ProductGroupList> materialInfoList = new ArrayList<ProductGroupList>();

			    	for(int i =0 ; i<materialArray.length() ; i++){
						try {
							JSONObject obj = materialArray.getJSONObject(i);
							
							String jsonstring = obj.toString().replaceAll("\\\\", "");
							logger.log(Level.SEVERE, "material Json"+jsonstring);
							
							String productName = obj.optString("materialName").trim();
							productName=appUtility.decodeText(productName);
							
							ProductGroupList technicianMaterialIfno  = new ProductGroupList();
							technicianMaterialIfno.setName(productName);
							technicianMaterialIfno.setQuantity(obj.optDouble("returnQty"));
							technicianMaterialIfno.setPlannedQty(obj.optDouble("returnQty"));
							technicianMaterialIfno.setProActualQty(obj.optDouble("returnQty"));
							technicianMaterialIfno.setProduct_id(obj.optInt("materialId"));
							technicianMaterialIfno.setCode(obj.optString("materialCode"));
							technicianMaterialIfno.setUnit(obj.optString("unit"));
							technicianMaterialIfno.setWarehouse(warehouseName);
							technicianMaterialIfno.setStorageLocation(storageLocation);
							technicianMaterialIfno.setStorageBin(storageBin);
							technicianMaterialIfno.setParentWarentwarehouse(parentWareHouse);
							technicianMaterialIfno.setParentStorageLocation(parentStorageLocation);
							technicianMaterialIfno.setParentStorageBin(parentStorageBin);
							
							/**
							 * @author Anil @since 30-09-2021
							 * opting mixing ratio and lot no 
							 * requirement raised by Rahul Tiwari for Innovative
							 */
							String mixingRatio="";
							String lotNo="";
							try{
								mixingRatio=obj.optString("mixingRatio");
							}catch(Exception e){
								
							}
							
							try{
								lotNo=obj.optString("lotNo");
							}catch(Exception e){
								
							}
							
							try{
								technicianMaterialIfno.setMixingRatio(mixingRatio);
								technicianMaterialIfno.setLotNo(lotNo);
							}catch(Exception e){
								
							}
							
							materialInfoList.add(technicianMaterialIfno);
							
							
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					 }
			      	
				      	logger.log(Level.SEVERE,"materialInfoList"+materialInfoList.size());
				      	if(materialInfoList.size()!=0){
				      		GeneralServiceImpl genral = new GeneralServiceImpl();
							String invoiceDate = null;
							int id = genral.createProject(serviceEntity,technicianlist,
									null,materialInfoList,serviceEntity.getServiceDate(),serviceEntity.getServiceTime(), 0, invoiceDate, serviceEntity.getEmployee(),0);
							logger.log(Level.SEVERE,"id "+id);
							
							String msgString = "";
							ServiceProject serProj = ofy().load().type(ServiceProject.class).filter("companyId", serviceEntity.getCompanyId()).filter("serviceId", serviceEntity.getCount())
									.filter("contractId", serviceEntity.getContractCount()).first().now();
							logger.log(Level.SEVERE,"serProj "+serProj);

							if (serProj != null) {
								logger.log(Level.SEVERE , "createMaterialReturnDoc : 3" );
								if(serProj.getProdDetailsList().size()!=0){
									logger.log(Level.SEVERE , "createMaterialReturnDoc : 4" );
									MaterialIssueNote minObj = ofy().load().type(MaterialIssueNote.class).filter("companyId", serviceEntity.getCompanyId()).filter("serviceId", serviceEntity.getCount())
												.filter("minSoId", serviceEntity.getContractCount() ).first().now();
									if(minObj!=null){
										msgString=msgString+"Min is already created for Service Id - "+serviceEntity.getCount()+"\n";
										logger.log(Level.SEVERE , "createMaterialReturnDoc : 5" );
									}else{
										
										ReadMaterialAndDoMIN(serProj,materialArray,branch,user);
										
										logger.log(Level.SEVERE , "createMaterialReturnDoc : 6" );
									}					
									
								}
								
							} 
				      	}
						
					
				}

			}
	}
	 
	 public void ReadMaterialAndDoMIN(ServiceProject serProj,JSONArray materialArray, String branch, String user) {
		 	/**
			 * @author Anil @since 30-09-2021
			 * opting mixing ratio and lot no 
			 * requirement raised by Rahul Tiwari for Innovative
			 */
			HashMap<String, String> mixingRationMap = new HashMap<String , String>();
			HashMap<String, String> lotNoMap = new HashMap<String , String>();
			
		 	ServerAppUtility appUtility=new ServerAppUtility();
			HashMap<String, Double> productMap = new HashMap<String , Double>();
			JSONObject obj = null;
			for(int i =0 ; i<materialArray.length() ; i++){
			try {
				obj = materialArray.getJSONObject(i);
				logger.log(Level.SEVERE , "material qty :" + obj.optString("returnQty") +" "+
						obj.optString("materialName"));
				double qty = 0;
				try{
					qty = Double.parseDouble(obj.optString("returnQty"));
				}catch(Exception e){
					qty = 0;
					logger.log(Level.SEVERE , "material qty :" + qty);
				}
				
				String productName = obj.optString("materialName").trim();
				/**
				 * @author Anil , Date : 28-01-2020
				 */
				logger.log(Level.SEVERE , "Befor Material Name :" + productName);
				productName=appUtility.decodeText(productName);
				logger.log(Level.SEVERE , "After Material Name :" + productName);
				/**
				 * End
				 */
				
				productMap.put(productName, qty);
				
				
				
				/**
				 * @author Anil @since 30-09-2021
				 * opting mixing ratio and lot no 
				 * requirement raised by Rahul Tiwari for Innovative
				 */
				String mixingRatio="";
				String lotNo="";
				try{
					mixingRatio=obj.optString("mixingRatio");
				}catch(Exception e){
					
				}
				
				try{
					lotNo=obj.optString("lotNo");
				}catch(Exception e){
					
				}
				
				try{
					mixingRationMap.put(productName, mixingRatio);
					lotNoMap.put(productName, lotNo);
				}catch(Exception e){
					
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }
		List<ProductGroupList> list = new ArrayList<ProductGroupList>();
		for(ProductGroupList details :serProj.getProdDetailsList())	{
			if(productMap.containsKey(details.getName().trim())){
				//details.setPlannedQty(details.getQuantity());
				details.setReturnQuantity(productMap.get(details.getName().trim()));
				//details.setQuantity(productMap.get(details.getName().trim()));
//				ProductInventoryViewDetails pvDetails = ofy().load().type(ProductInventoryViewDetails.class).filter("companyId", service.getCompanyId())
//						                               .filter("prodname", details.getName().trim())
//						                               .filter("warehousename", details.getParentWarentwarehouse())
//						                               .filter("storagelocation", details.getParentStorageLocation())
//						                               .filter("storagebin", details.getParentStorageBin()).first().now();
//				
//				if(pvDetails != null){
//					double quantity = productMap.get(details.getName().trim());
//					if((pvDetails.getAvailableqty() - quantity) < 0){
//						error += "Insufficient available quantity for product "+ details.getName().trim() + " /";
//					}
//				}
				try{
					details.setMixingRatio(mixingRationMap.get(details.getName().trim()));
					details.setLotNo(lotNoMap.get(details.getName().trim()));
				}catch(Exception e){
					
				}
			}
			list.add(details);
		}
		serProj.setProdDetailsList(list);
		ofy().save().entity(serProj);
		
		
		Gson gson = new GsonBuilder().create();
		String str = gson.toJson(serProj, ServiceProject.class);
		
//	    Queue queue = QueueFactory.getQueue("documentCancellation-queue");
		    Queue queue = QueueFactory.getQueue("CreateMINQueue-queue");
	        String typeducumentName = "CreateMIN"+"$"+serProj.getCompanyId()+"$"+str+"$"+branch+"$"+user;
		  	logger.log(Level.SEVERE, "Company Id "	+ typeducumentName);
		  	queue.add(TaskOptions.Builder.withUrl("/slick_erp/CreateMINTaskQueue").param("taskKeyAndValue", typeducumentName));
		
		}
	 
	 //Ashwini Patil Date:14-10-2022
	 public Approvals SendApprovalRequestAndReturnApproverName(MultipleExpenseMngt mngt,PettyCash pettyCash,double amount){


			MultilevelApproval multilevelApproval = ofy().load().type(MultilevelApproval.class)
					.filter("companyId",mngt.getCompanyId()).filter("documentType",AppConstants.EXPENSEMANAGEMET)
					.first().now();
			String approvarName = "";
			int approvaleLevel = 1;
			if(multilevelApproval != null && multilevelApproval.isDocStatus()){
				logger.log(Level.SEVERE, "checking multilevelApproval" ) ;
					for(MultilevelApprovalDetails details: multilevelApproval.getApprovalLevelDetails()){
						if(details.getLevel().equalsIgnoreCase("1")){
						
							Employee employee=ofy().load().type(Employee.class)
									.filter("companyId", mngt.getCompanyId())
									.filter("fullname",  mngt.getEmployee()).first().now();
							if(employee != null && employee.getReportsTo() != null && !employee.getReportsTo().equalsIgnoreCase("")){
								approvarName = employee.getReportsTo();
								logger.log(Level.SEVERE, "approvarName set from reportsTo -" +approvarName ) ;
							}else{
								approvarName = details.getEmployeeName();
								logger.log(Level.SEVERE, "approvarName set from MultilevelApprovalDetails -" +approvarName ) ;
							}							
							break;
						}					
					}				
			}else if(pettyCash != null) {	
				logger.log(Level.SEVERE, "checking pettycash" ) ;
					ArrayList<ApproverAmount> amtApproverList=pettyCash.getAmountApproved();
					Comparator<ApproverAmount> comparator=new Comparator<ApproverAmount>() {
						@Override
						public int compare(ApproverAmount arg0, ApproverAmount arg1) {
							if(arg0.amount==arg1.amount)
								return 0;
							else if(arg0.amount<arg1.amount)
								return -1;
							else
								return 1;
						}
					};
					Collections.sort(amtApproverList, comparator);
					logger.log(Level.SEVERE ,"amtApproverList size"+amtApproverList.size());
					for(ApproverAmount aa:amtApproverList) {
						if(amount<=aa.amount) {
							approvarName=aa.employeeName;
							logger.log(Level.SEVERE ,"approvarName set from pettycash -"+approvarName);
							break;
						}
					}			
				
			}else {
				return null;
			}
			
			Approvals approval=null;
			if(!approvarName.equals("")){	
					approval=new Approvals();
					logger.log(Level.SEVERE ,"Creating Approval with approvarName="+approvarName);
					approval.setCompanyId(mngt.getCompanyId());
					approval.setApproverName(approvarName);
					approval.setBranchname(mngt.getBranch());
					approval.setRequestedBy(mngt.getEmployee());
					approval.setPersonResponsible(mngt.getEmployee());
					approval.setDocumentCreatedBy(mngt.getEmployee());		
					approval.setApprovalLevel(approvaleLevel);
					approval.setStatus(Approvals.PENDING);
					approval.setBusinessprocesstype(AppConstants.MULTIPLEEXPENSEMANAGEMET);//AppConstants.EXPENSEMANAGEMET
					approval.setBpId(mngt.getEmpId()+"");
					approval.setBpName(mngt.getEmployee());
					approval.setDocumentValidation(false);
					approval.setBusinessprocessId(mngt.getCount());		
					logger.log(Level.SEVERE ,"Document id set to"+mngt.getCount());
			}else{
					logger.log(Level.SEVERE ,"No approval request created as no approver name found in MultilevelApproval or PettyCash!");
					return null;				
				}				
			
		 return approval;
	 }
	 
	 
	 public void updateFindingsEntity(Service s,List<CatchTraps> catchTrapList){
		// logger.log(Level.SEVERE ,"updateFindingsEntity called for service"+s.getCount());
		 
		 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "UpdateFindingEntity", s.getCompanyId())) {
			 logger.log(Level.SEVERE ,"UpdateFindingEntity config active");
			 
			 List<ServiceFinding> findinglist=ofy().load().type(ServiceFinding.class).filter("serviceCount", s.getCount()).list();
		//	 logger.log(Level.SEVERE ,"step 1");
			 if(findinglist!=null) {
				 ofy().delete().entities(findinglist);
				 logger.log(Level.SEVERE ,"old findings deleted");
				 
			 }
			 //logger.log(Level.SEVERE ,"step 1.1");
			 
			 ArrayList<ServiceFinding> serviceFindingList=new ArrayList<ServiceFinding>();
			 SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();
			 int count=0;
			count=(int) numGen.getLastUpdatedNumber("ServiceFinding", s.getCompanyId(), (long)1);

			// logger.log(Level.SEVERE ,"step 2");
				
			 if(catchTrapList!=null&&catchTrapList.size()>0&&s!=null) {
				 logger.log(Level.SEVERE ,"catchTrapList not null in Service "+s.getCount());
				 
				 for(CatchTraps trap:catchTrapList) {
					 ServiceFinding findings=new ServiceFinding();
					 findings.setCompanyId(s.getCompanyId());
					 findings.setCatchCount(trap.getCount());
					 findings.setCatchLocation(trap.getLocation());
					 findings.setCatchPestName(trap.getPestName());
					 if(trap.getCatchLevel()!=null&&!trap.getCatchLevel().equals(""))
						 findings.setCatchLevel(trap.getCatchLevel());
					 findings.setServiceCount(s.getCount());
					 findings.setContractCount(s.getContractCount());
					 if(s.getServiceBranch()!=null)
						 findings.setServiceBranch(s.getServiceBranch());
					 findings.setBranch(s.getBranch());
					 if(s.getAddress()!=null)
						 findings.setAddress(s.getAddress());
					 findings.setServiceDate(s.getServiceDate());
					 if(s.getServiceDateDay()!=null)
						 findings.setServiceDateDay(s.getServiceDateDay());
					 if(s.getServiceDay()!=null)
						 findings.setServiceDay(s.getServiceDay());
					 if(s.getServiceTime()!=null)
						 findings.setServiceTime(s.getServiceTime());
					 if(s.getServiceCompletionDate()!=null)
						 findings.setServiceCompletionDate(s.getServiceCompletionDate());
					 if(s.getComment()!=null)
						 findings.setComment(s.getComment());
					 findings.setCompletedByApp(s.isCompletedByApp());
					 if(s.getCompletedDate()!=null)
						 findings.setCompletedDate(s.getCompletedDate());
					 if(s.getCompletedDate_time()!=null)
						 findings.setCompletedDate_time(s.getCompletedDate_time());
					 if(s.getCompletedTime()!=null)
						 findings.setCompletedTime(s.getCompletedTime());
					 if(s.getSystemCompletionDate()!=null)
						 findings.setSystemCompletionDate(s.getSystemCompletionDate());
					 if(s.getPersonInfo()!=null)
						 findings.setPersonInfo(s.getPersonInfo());
					 findings.setProduct((ServiceProduct)s.getProduct());
					 if(s.getRefNo()!=null)
						 findings.setRefNo(s.getRefNo());
					 findings.setStatus(s.getStatus());
					 if(s.getEmployee()!=null)
						 findings.setEmployee(s.getEmployee());
					 if(s.getTeam()!=null)
						 findings.setTeam(s.getTeam());
					 if(s.getTechnicians()!=null)
						 findings.setTechnicians(s.getTechnicians());
					 if(s.getPremises()!=null)
						 findings.setPremises(s.getPremises());
					 findings.setCount(count);
					 serviceFindingList.add(findings);
					 count++;
				 }
			 }
			 if(serviceFindingList!=null&&serviceFindingList.size()>0) {
				 logger.log(Level.SEVERE ,"serviceFindingList size="+serviceFindingList.size());			 
				 ofy().save().entities(serviceFindingList);
				 logger.log(Level.SEVERE ,"serviceFindingList saved");			 
			 }
			 
		 }
	 }
	 
}
