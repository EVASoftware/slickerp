package com.slicktechnologies.server.android.technicianapp;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.DefaultEditorKit.CutAction;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

/**
 * Description : It is used to create services directly from app without material.(for nayara)
 			It also creates service wise bill contract at the backend.
   
 * @author pc1
 *
 */
public class CreateServices extends HttpServlet {

	/**
	 * 
	 */
	Company comp = null;
	Logger logger = Logger.getLogger("createservice.class");
	
	private static final long serialVersionUID = 6706536094025511326L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		resp.setHeader("Access-Control-Allow-Origin", AppConstants.TRENDZWEBSOLUTION);
		resp.setHeader("Access-Control-Allow-Origin", "*");
		String urlCalled=req.getRequestURL().toString().trim();
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
	//	try{
		comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();	
		resp.setContentType("text/plain");
		String companyId=comp.getCompanyId()+"";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		String data=req.getParameter("data").trim();
		JSONObject jsonData = null;
		
		try {
			jsonData = new JSONObject(data);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String branch=jsonData.optString("branch").trim();
		int customerId = Integer.parseInt(jsonData.optString("customerId").trim());
		String customerName= jsonData.optString("customerName").trim();
		String serviceDate=jsonData.optString("serviceDate").trim();
		String serviceTime = jsonData.optString("serviceTime").trim();
		int freqOfServices= Integer.parseInt(jsonData.optString("frequency").trim());
		int noOfServices = Integer.parseInt(jsonData.optString("count").trim());
		int productId= Integer.parseInt(jsonData.optString("productId").trim());
		String productName = jsonData.optString("productName").trim();
		String technicianName= jsonData.optString("technician").trim();
		String approverName= jsonData.optString("approverName").trim();
		String createdBy= jsonData.optString("createdBy").trim();
		
		Customer customer = ofy().load().type(Customer.class)
				.filter("companyId", comp.getCompanyId())
				.filter("count", customerId).first().now();
		
		logger.log(Level.SEVERE, "customer :" + customer);
		
		ServiceProduct product = ofy().load().type(ServiceProduct.class)
				.filter("companyId", comp.getCompanyId())
				.filter("count", productId).first().now();
		
		logger.log(Level.SEVERE, "product :" + product);
		
		Contract contract = new Contract();
		PersonInfo info = new PersonInfo();
		info.setCount(customer.getCount());
		info.setCellNumber(customer.getCellNumber1());
		if(customer.isCompany()){
			info.setFullName(customer.getCompanyName());
			info.setPocName(customer.getFullname());
		}else{
			info.setFullName(customer.getFullname());
			info.setPocName(customer.getFullname());
		}
		
		contract.setCinfo(info);
		contract.setBranch(branch);
		contract.setEmployee(technicianName);
		contract.setServiceWiseBilling(true);
		contract.setComplaintFlag(true);
		contract.setIsQuotation(false);
		contract.setStatus(contract.APPROVED);
		contract.setApproverName(approverName);
		contract.setCreatedBy(createdBy);
		try {
			contract.setContractDate(sdf.parse(serviceDate));
			contract.setStartDate(sdf.parse(serviceDate));
			Date date = DateUtility.addDaysToDate(sdf.parse(serviceDate), (freqOfServices*noOfServices) -1);
			contract.setEndDate(date);
			contract.setApprovalDate(sdf.parse(serviceDate));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<SalesLineItem> list = new ArrayList<SalesLineItem>();
		
		SalesLineItem lineItem=new SalesLineItem();
		lineItem.setPrduct(product);
		lineItem.setQuantity(1.0);
		lineItem.setStartDate(contract.getStartDate());
		lineItem.setEndDate(contract.getEndDate());
		lineItem.setNumberOfService(noOfServices);
		lineItem.setDuration(freqOfServices*noOfServices);
		lineItem.setPrice(product.getPrice());
		list.add(lineItem);
		contract.setItems(list);
		contract.setCompanyId(comp.getCompanyId());
		NumberGeneration ng=ofy().load().type(NumberGeneration.class).filter("companyId" , comp.getCompanyId()).filter("processName", "Contract").filter("status", true).first().now();
		long number = ng.getNumber();
	    ng.setNumber(number + 1);
	    ofy().save().entity(ng);
	    contract.setCount((int)(number + 1));
	    ofy().save().entity(contract);
	  
	    NumberGeneration numbergen = ofy().load().type(NumberGeneration.class)
				.filter("companyId", comp.getCompanyId())
				.filter("processName", "Service")
				.filter("status", true).first().now();
	    
	    long number1 = numbergen.getNumber();

		logger.log(Level.SEVERE, "Last Number====" + number);
		int firstCount = (int) number1;

		number1 = number1 + (int) noOfServices;
		numbergen.setNumber(number1);
		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(numbergen);
	    

    	firstCount = firstCount + 1;
    	Date servicingDate = new Date();
		try {
			
			servicingDate = DateUtility.getDateWithTimeZone("IST", sdf.parse(serviceDate));
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	String	serviceString1= contract.getCinfo().getCount()+"$"+contract.getCinfo().getEmail()+"$"+contract.getCinfo().getFullName()+"$"+contract.getCinfo().getPocName()+"$"+contract.getCinfo().getCellNumber()
				+"$"+contract.getCount()+"$"+branch+"$"+Service.SERVICESTATUSSCHEDULE+"$"+contract.getStartDate()+"$"+contract.getEndDate()+"$"+false+
				"$"+1+"$"+"Periodic"+"$"+1+"$"+servicingDate+
				"$"+customer.getSecondaryAdress().getAddrLine1()+"$"+customer.getSecondaryAdress().getAddrLine2()+"$"+customer.getSecondaryAdress().getCountry()+"$"+customer.getSecondaryAdress().getState()+"$"+customer.getSecondaryAdress().getCity()+"$"+customer.getSecondaryAdress().getLocality()+"$"+customer.getSecondaryAdress().getPin()+"$"+customer.getSecondaryAdress().getLandmark()+"$"+contract.getScheduleServiceDay()+"$"+serviceTime+
				"$"+branch+"$"+new SimpleDateFormat("EEEE").format(servicingDate)+
				"$"+contract.getCompanyId()+"$"+product.getComment()+"$"+product.getCommentdesc()+"$"+product.getCompanyId()+"$"+product.getCount()+"$"
				+product.getCreatedBy()+"$"+product.isDeleted()+"$"+lineItem.getDuration()+"$"+product.getId()+"$"+lineItem.getNumberOfServices()+"$"+lineItem.getPrice()+"$"
				+product.getProductCategory()+"$"+product.getProductClassification()+"$"+product.getProductCode()+"$"
				+product.getProductGroup()+"$"+product.getProductName()+"$"+product.getProductType()+"$"+product.getRefNumber1()+"$"+product.getRefNumber2()+"$"
				+product.getServiceTax().isInclusive()+"$"+product.getServiceTax().getPercentage()+"$"+product.getServiceTax().getTaxConfigName()+"$"
				+product.getServiceTax().getTaxName()+"$"+product.getSpecifications()+"$"+product.isStatus()+"$"
				+product.getUnitOfMeasurement()+"$"+product.getUserId()+"$"+product.getVatTax().isInclusive()+"$"+product.getVatTax().getPercentage()+"$"
				+product.getVatTax().getTaxConfigName()+"$"+product.getVatTax().getTaxName()
				+"$"+firstCount+"$"+contract.getNumberRange()+"$"+false+"$"+0+"$"+0+"$"+""
				+"$"+0+"$"+technicianName+"$"+0+
				"$"+true+"$"+0
				+"$"+false +"$"+false
				 +"$"+contract.getPocName()+
				 "$"+0+"$"+"na"+"$"+0+"$"+"na"
				 +"$"+contract.isRenewContractFlag()+"$"+0+"$"+"ServiceThroughApp"+"$"+0
				 +"$" + null;


		
		Queue queue = QueueFactory.getQueue("ContractServices-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/contractservicestaskqueue").param("servicekey", serviceString1));
				
    
		Date serDate= servicingDate;
	    for(int i =2; i <= noOfServices;i++){
	    	firstCount = firstCount + 1;
	    	
			//try {
				Date servicingDate1 = new Date();
				servicingDate1 = DateUtility.getDateWithTimeZone("IST", serDate);
				serDate = DateUtility.addDaysToDate(servicingDate1, freqOfServices);
			//} 
	    	String	serviceString= contract.getCinfo().getCount()+"$"+contract.getCinfo().getEmail()+"$"+contract.getCinfo().getFullName()+"$"+contract.getCinfo().getPocName()+"$"+contract.getCinfo().getCellNumber()
					+"$"+contract.getCount()+"$"+branch+"$"+Service.SERVICESTATUSSCHEDULE+"$"+contract.getStartDate()+"$"+contract.getEndDate()+"$"+false+
					"$"+i+"$"+"Periodic"+"$"+i+"$"+serDate+
					"$"+customer.getSecondaryAdress().getAddrLine1()+"$"+customer.getSecondaryAdress().getAddrLine2()+"$"+customer.getSecondaryAdress().getCountry()+"$"+customer.getSecondaryAdress().getState()+"$"+customer.getSecondaryAdress().getCity()+"$"+customer.getSecondaryAdress().getLocality()+"$"+customer.getSecondaryAdress().getPin()+"$"+customer.getSecondaryAdress().getLandmark()+"$"+contract.getScheduleServiceDay()+"$"+serviceTime+
					"$"+branch+"$"+new SimpleDateFormat("EEEE").format(serDate)+
					"$"+contract.getCompanyId()+"$"+product.getComment()+"$"+product.getCommentdesc()+"$"+product.getCompanyId()+"$"+product.getCount()+"$"
					+product.getCreatedBy()+"$"+product.isDeleted()+"$"+lineItem.getDuration()+"$"+product.getId()+"$"+lineItem.getNumberOfServices()+"$"+lineItem.getPrice()+"$"
					+product.getProductCategory()+"$"+product.getProductClassification()+"$"+product.getProductCode()+"$"
					+product.getProductGroup()+"$"+product.getProductName()+"$"+product.getProductType()+"$"+product.getRefNumber1()+"$"+product.getRefNumber2()+"$"
					+product.getServiceTax().isInclusive()+"$"+product.getServiceTax().getPercentage()+"$"+product.getServiceTax().getTaxConfigName()+"$"
					+product.getServiceTax().getTaxName()+"$"+product.getSpecifications()+"$"+product.isStatus()+"$"
					+product.getUnitOfMeasurement()+"$"+product.getUserId()+"$"+product.getVatTax().isInclusive()+"$"+product.getVatTax().getPercentage()+"$"
					+product.getVatTax().getTaxConfigName()+"$"+product.getVatTax().getTaxName()
					+"$"+firstCount+"$"+contract.getNumberRange()+"$"+false+"$"+0+"$"+0+"$"+""
					+"$"+0+"$"+technicianName+"$"+0+
					"$"+true+"$"+0
					+"$"+false +"$"+false
					+"$"+contract.getPocName()+
					"$"+0+"$"+"na"+"$"+0+"$"+"na"
					+"$"+contract.isRenewContractFlag()+"$"+0+"$"+"ServiceThroughApp"+"$"+0
					+"$"+null;


			
			Queue queue1 = QueueFactory.getQueue("ContractServices-queue");
			queue1.add(TaskOptions.Builder.withUrl("/slick_erp/contractservicestaskqueue").param("servicekey", serviceString));
					
	    }
	    resp.getWriter().write("Services Created Successfully.");
//		}catch(Exception e){
//			resp.getWriter().write("Failed");
//		}
	}

}
