package com.slicktechnologies.server.android.technicianapp;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.User;

public class ServiceSchedulingNotification {
	Logger logger = Logger.getLogger("Name of logger");
	
	public void sendSchedulingNotificationToTechnician(List<Service> sList) {
		long companyId=sList.get(0).getCompanyId();
		logger.log(Level.SEVERE , "SCHEDULE LIST SIZE "+sList.size());
		if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableServiceScheduleNotification", companyId)){
			logger.log(Level.SEVERE , "Notification process not active");
			return;
		}
		
		try{
			
			logger.log(Level.SEVERE , "Schedule Service Notification");
			SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MMM");
			isoFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			logger.log(Level.SEVERE , "CURRENT DATE : "+cal.getTime());
			Date srtDt=null;
			Date endDt=null;
			srtDt=DateUtility.getDateWithTimeZone("IST", cal.getTime());
			logger.log(Level.SEVERE , "START DATE : "+srtDt);
			cal.add(cal.DATE, 1);
//			endDt=DateUtility.addDaysToDate(cal.getTime(), 1);
			endDt=DateUtility.getDateWithTimeZone("IST", cal.getTime());
			logger.log(Level.SEVERE , "END DATE : "+endDt);
			
			ArrayList<Service> serListForNotification=new ArrayList<Service>();
			
			for(Service service:sList){
				if((service.getServiceDate().after(srtDt)||service.getServiceDate().equals(srtDt))
						&&(service.getServiceDate().before(endDt)||service.getServiceDate().equals(endDt))){
					serListForNotification.add(service);
				}
			}
			logger.log(Level.SEVERE , "NOTIFICATION SCHEDULE LIST SIZE "+serListForNotification.size());
			HashMap<String, ArrayList<Service>> technicianMap=new HashMap<String, ArrayList<Service>>();
			for(Service service:serListForNotification){
				if(technicianMap!=null&&technicianMap.size()!=0){
					if(technicianMap.containsKey(service.getEmployee())){
						technicianMap.get(service.getEmployee()).add(service);
					}else{
						ArrayList<Service> list=new ArrayList<Service>();
						list.add(service);
						technicianMap.put(service.getEmployee(), list);
					}
				}else{
					ArrayList<Service> list=new ArrayList<Service>();
					list.add(service);
					technicianMap.put(service.getEmployee(), list);
				}
			}
			logger.log(Level.SEVERE , "TECH MAP SIZE "+technicianMap.size());
			
			Set<String> techUserIdSet=technicianMap.keySet();
			ArrayList<String> techUserIdList=new ArrayList<String>(techUserIdSet);
			logger.log(Level.SEVERE , "techUserIdList SIZE "+techUserIdList.size()+" "+techUserIdSet);
			if(techUserIdList.size()!=0){
				String companyUrl="";
				Company company=ofy().load().type(Company.class).filter("companyId", companyId).first().now();
				if(company!=null){
					companyUrl=company.getCompanyURL();
				}
				logger.log(Level.SEVERE , "Company Not Null"+companyUrl);
				if(companyUrl.equals("")){
					return;
				}
				logger.log(Level.SEVERE , "Company Not Null 1"+companyUrl);
				List<User> userList=ofy().load().type(User.class).filter("companyId", companyId).filter("employeeName IN", techUserIdList).filter("status", true).list();
				for(String key:technicianMap.keySet()){
					
					String userName=getUserNameFormUserList(key,userList);
					logger.log(Level.SEVERE , "USER NAME "+userName);
					if(userName==null){
						continue;
					}
					/**
					 * @author Anil ,Date : 17-01-2020
					 * if url contains '.' then we will replace it by'-dot-' to avoid security patch issue
					 */
					if(!companyUrl.contains("-dot-")){
						companyUrl = companyUrl.replaceFirst("\\.", "-dot-");
					}
					logger.log(Level.SEVERE , "Company Not Null 1"+companyUrl);
					
					ArrayList<Service> serviceList=technicianMap.get(key);
					String userUniqueId=companyUrl+"_"+userName;
					String title="Service Schedule";
//					String titleBrief=serviceList.size()+ " service has been scheduled.";
					String titleBrief="";
					
					/**
					 * @author Anil , Date :16-01-2020
					 */
					if(serviceList.size()==1){
						Service obj=serviceList.get(0);
						if(obj.getAddress()!=null&&obj.getAddress().getLocality()!=null&&!obj.getAddress().getLocality().equals("")){
							titleBrief=titleBrief+"New Service of "+obj.getCustomerName()+" "+obj.getAddress().getLocality()+" has been assigned to you.";
						}else{
							titleBrief=titleBrief+"New Service of "+obj.getCustomerName()+" has been assigned to you.";
						}
					}else if(serviceList.size()>1){
						titleBrief=titleBrief+serviceList.size()+" New Service has been assigned to you.";
					}
					
					
					
					JSONObject jObj=new JSONObject();
					jObj.put("title",title);
					jObj.put("brief",titleBrief);
					jObj.put("userUniqueId",userUniqueId);
					String jsonString=jObj.toJSONString().replaceAll("\\\\", "");
					logger.log(Level.SEVERE,"jsonString Data:: "+jsonString);
					
					callNotificationApi(jsonString);
				}
			}
			
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}



	private void callNotificationApi(String jsonString) {
				
//		String apiUrl="https://eva.enthralingmatrix.com/api/notify";
//		String apiUrl="http://159.89.167.66:4060/api/notify";
		String apiUrl="http://165.22.220.44:4050/api/notify";//Ashwini Patil Date:10-10-2022
		
		logger.log(Level.SEVERE,"API URL : "+apiUrl);
		
		String data="";
		URL url = null;
		try {
			url = new URL(apiUrl);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"STAGE 1");
        HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"STAGE 2");
        con.setDoInput(true);
        con.setDoOutput(true);
        con.setRequestProperty("Content-Type","application/json; charset=UTF-8");
        try {
			con.setRequestMethod("POST");
		} catch (ProtocolException e) {
			e.printStackTrace();
		}
        logger.log(Level.SEVERE,"STAGE 3");
        OutputStream os = null;
		try {
			os = con.getOutputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"STAGE 4");
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
        try {
			writer.write(jsonString);
		} catch (IOException e) {
			e.printStackTrace();
		}
        try {
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
        logger.log(Level.SEVERE,"STAGE 5");
        
        
        InputStream is = null;
		try {
			is = con.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		logger.log(Level.SEVERE,"STAGE 6");
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String temp;
        try {
			while ((temp = br.readLine()) != null) {
			    data = data + temp+"\n"+"\n";
			}
			logger.log(Level.SEVERE,"data data::::::::::"+data);
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        logger.log(Level.SEVERE,"STAGE 7");
	}



	private String getUserNameFormUserList(String key, List<User> userList) {
		// TODO Auto-generated method stub
		
		if(userList!=null){
			for(User obj:userList){
				if(obj.getEmployeeName().equals(key)){
					return obj.getUserName();
				}
			}
		}
		return null;
	}
	
	
}
