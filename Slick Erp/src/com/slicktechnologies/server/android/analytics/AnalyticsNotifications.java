package com.slicktechnologies.server.android.analytics;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.ApprovableServiceImplementor;
import com.slicktechnologies.server.android.technicianapp.TechnicianAppOperation;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
/**
 * Description  : This API is used to send approval request , after approval of document or after 
 * rejection of document. 
 * task name = "APPROVE" , "REJECT" , "REQUESTEDBYME else it loads all approal request.
 */
public class AnalyticsNotifications extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9122738527110126078L;
	
	Logger logger = Logger.getLogger("AnalyticsNotifications.class");
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
//		resp.setHeader("Access-Control-Allow-Origin", AppConstants.TRENDZWEBSOLUTION);
		resp.setHeader("Access-Control-Allow-Origin", "*");
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		String urlCalled=req.getRequestURL().toString().trim();
//		String url=urlCalled.replace("http://", "");
//		String[] splitUrl=url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		String employeeName=req.getParameter("employeeName").trim();
		String taskName=req.getParameter("taskToPerform").trim();
		String data=req.getParameter("data").trim();
		
		Company comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();

		//Loggers added by Apeksha on 04/09/2018 @12:20 to check data.
		logger.log(Level.SEVERE,"Notification:employeeName: "+employeeName+" taskName: "+taskName
				+"companyId"+ comp.getCompanyId());
		logger.log(Level.SEVERE,"Notification:data: "+data);
		
		if(taskName.trim().equalsIgnoreCase("APPROVE")){
			org.json.JSONObject obj = null;
			try {
				obj = new org.json.JSONObject(data);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE,"Notification:Error: "+e);
			}
			Approvals approvals=ofy().load().type(Approvals.class)
					.filter("companyId", comp.getCompanyId())
					.filter("count",Long.parseLong(obj.optString("count")))
					.filter("approverName", employeeName)
					.first().now();
			logger.log(Level.SEVERE,"Notification:approvals count: "+approvals.getCount()
					+" status: "+approvals.getStatus());
			logger.log(Level.SEVERE,"Notification:approvals getBusinessprocesstype: "+approvals.getBusinessprocesstype()
					+" getBusinessprocessId: "+approvals.getBusinessprocessId());
			logger.log(Level.SEVERE,"Notification:approvals getCompanyId: "+approvals.getCompanyId()
					+" approverName: "+approvals.getApproverName());
			approvals.setStatus(Approvals.APPROVED);
			/*
			 * Name: Apeksha Gunjal
			 * Date: 09/10/2018 @ 16:15
			 * Note: added remark to approval.
			 */
			try{
			approvals.setRemark(obj.optString("remark"));
			}catch(Exception e){
				logger.log(Level.SEVERE,"Notification:error: "+e);
			}
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("MultipleExpenseMngt", "DirectApprovalRequest", approvals.getCompanyId())){
				TechnicianAppOperation opp = new TechnicianAppOperation();
				MultipleExpenseMngt mngt = ofy().load().type(MultipleExpenseMngt.class)
						.filter("companyId", comp.getCompanyId())
						.filter("count",approvals.getBusinessprocessId()).first().now();
				opp.SendExpeneseApproval(mngt, approvals);
				resp.getWriter().println("success");
			}else{
				ApprovableServiceImplementor impl=new ApprovableServiceImplementor();
				String result=impl.sendApproveRequest(approvals);
				logger.log(Level.SEVERE,"Notification:result: "+result);
				resp.getWriter().println(result);
			}
			
			
		}else if(taskName.trim().equalsIgnoreCase("REJECT")){
			org.json.JSONObject obj = null;
			try {
				obj = new org.json.JSONObject(data);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Approvals approvals=ofy().load().type(Approvals.class).filter("companyId", comp.getCompanyId()).filter("count",Long.parseLong(obj.optString("count"))).first().now();
			approvals.setStatus(Approvals.REJECTED);
			/*
			 * Name: Apeksha Gunjal
			 * Date: 09/10/2018 @ 16:15
			 * Note: added remark to approval.
			 */
			approvals.setRemark(obj.optString("remark"));
			ApprovableServiceImplementor impl=new ApprovableServiceImplementor();
			String result=impl.sendApproveRequest(approvals);
			resp.getWriter().println(result);
		}else if(taskName.trim().equalsIgnoreCase("REQUESTEDBYME")){
			/*
			 * Name: Apeksha Gunjal
			 * Date: 04/09/2018 @ 12:14
			 * Note: Filter approvals by requestedBy and status as pending and requested only.
			 */
			ArrayList<String> approvalStatuses = new ArrayList<>();
			approvalStatuses.add(Approvals.PENDING);
			approvalStatuses.add(Approvals.REQUESTED);
			List<Approvals> approvalsList =ofy().load().type(Approvals.class)
					.filter("companyId", comp.getCompanyId())
					.filter("status IN ", approvalStatuses)
					.filter("requestedBy", employeeName.trim()).list();
			
			logger.log(Level.SEVERE,"REQUESTEDBYME approvalsList: "+ approvalsList.size());
			
			JSONArray jsonArray=new JSONArray();
			for (Approvals approval: approvalsList) {
				JSONObject jsonObj=new JSONObject();
				jsonObj.put("docType",approval.getBusinessprocesstype());
				String companyUrl="";
				if(comp.getCompanyURL().contains("http://")){
					companyUrl=comp.getCompanyURL().replace("http://", " ");
				}else{
					companyUrl=comp.getCompanyURL();
				}
				if(comp.getCompanyURL().contains("/")){
					companyUrl=companyUrl.replace("/", "").trim();
				}else{
					companyUrl=companyUrl.trim();
				}
				String docType=approval.getBusinessprocesstype().trim();
				String linkToCall="";
				try{
				if(docType.equals(AppConstants.APPROVALCONTRACT)){
					Contract contract=ofy().load().type(Contract.class).filter("companyId",comp.getCompanyId()).filter("count", approval.getBusinessprocessId()).first().now();
					linkToCall="/slick_erp/pdfservice"+"?Id="+contract.getId()+"&"+"type="+"c"+"&"+"preprint="+"plane";
				}else if(docType.equals(AppConstants.APPROVALDELIVERYNOTE)){
					DeliveryNote dN=ofy().load().type(DeliveryNote.class).filter("companyId", comp.getCompanyId()).filter("count", approval.getBusinessprocessId()).first().now();
					linkToCall="/slick_erp/deliverynotepdf"+"?Id="+dN.getId();
				}else if(docType.equals(AppConstants.APPROVALSALESORDER)){
					SalesOrder salesOrder=ofy().load().type(SalesOrder.class).filter("companyId", comp.getCompanyId()).filter("count", approval.getBusinessprocessId()).first().now();
					linkToCall="/slick_erp/pdfsales"+"?Id="+salesOrder.getId()+"&"+"type="+"so";
				}else if(docType.equals(AppConstants.APPROVALQUOTATION)){
					Quotation quotation=ofy().load().type(Quotation.class).filter("companyId", comp.getCompanyId()).filter("count", approval.getBusinessprocessId()).first().now();
					linkToCall="/slick_erp/pdfservice"+"?Id="+quotation.getId()+"&"+"type="+"q"+"&"+"preprint="+"plane";
				}else if(docType.equals(AppConstants.APPROVALSALESQUOTATION)){
					SalesQuotation salesquotation=ofy().load().type(SalesQuotation.class).filter("companyId", comp.getCompanyId()).filter("count", approval.getBusinessprocessId()).first().now();
					linkToCall="/slick_erp/pdfsales"+"?Id="+salesquotation.getId()+"&"+"type="+"sq"+"&"+"preprint="+"plane";
				}else if(docType.equals(AppConstants.APPROVALINVOICEDETAILS)){
					Invoice invoiceEntity=ofy().load().type(Invoice.class).filter("companyId", comp.getCompanyId()).filter("count", approval.getBusinessprocessId()).first().now();
					if(!invoiceEntity.isMultipleOrderBilling()){
						linkToCall="/slick_erp/serviceinvoiceprint"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"plane"+"&"+"type=SingleBilling";
					}else{
						linkToCall="/slick_erp/serviceinvoiceprint"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"plane"+"&"+"type=MultipleBilling";
					}
					
				}else if(docType.equals(AppConstants.APPROVALEXPENSEMANAGMENT)){
					MultipleExpenseMngt expense=ofy().load().type(MultipleExpenseMngt.class).filter("companyId", comp.getCompanyId()).filter("count", approval.getBusinessprocessId()).first().now();
					linkToCall="/slick_erp/pdfexpense"+"?Id="+expense.getId()+ "&" + "type=" + "multiple"+"&"+"companyId="+expense.getCompanyId();
				}
				}catch(Exception e){
					e.printStackTrace();
					logger.log(Level.SEVERE,"Error in link formation for print");
				}
				jsonObj.put("linkToCall","http://"+companyUrl+linkToCall);
				jsonObj.put("docId",approval.getBusinessprocessId());
				jsonObj.put("requestedBy", approval.getRequestedBy());
				try{
				jsonObj.put("requestedDate", sdf.format(approval.getCreationDate()));
				}catch(Exception e){
					e.printStackTrace();
				}
				jsonObj.put("approverName",approval.getApproverName());
				jsonObj.put("branch",approval.getBranchname());
				jsonObj.put("status",approval.getStatus());
				jsonObj.put("approvalLevel",approval.getApprovalLevel()+"");
				jsonObj.put("approvalCount",approval.getCount()+"");
				jsonObj.put("name", approval.getBpName());
				try{
				if(approval.getApprovalOrRejectDate()!=null){
					jsonObj.put("approveRejectDate", sdf.format(approval.getApprovalOrRejectDate()));
				}else{
					jsonObj.put("approveRejectDate","");
				}
				}catch(Exception e){
					e.printStackTrace();
				}
				jsonArray.add(jsonObj);
			}
			
			String jsonString=jsonArray.toString().replace("\\\\", "");
			logger.log(Level.SEVERE, jsonString);
			resp.getWriter().println(jsonString);
			
		}else{//taskName="REQUESTEDTOME"
			/*
			 * Name: Apeksha Gunjal
			 * Date: 04/09/2018 @ 12:14
			 * Note: Filter approvals by approverName and status as pending and requested only.
			 */
			ArrayList<String> approvalStatuses = new ArrayList<>();
			approvalStatuses.add(Approvals.PENDING);
			approvalStatuses.add(Approvals.REQUESTED);
			
			List<Approvals> approvalsList = ofy().load().type(Approvals.class)
					.filter("companyId", comp.getCompanyId())
					.filter("status IN ", approvalStatuses)
					.filter("approverName", employeeName.trim()).list();
			
			logger.log(Level.SEVERE,"REQUESTEDTOME approvalsList: "+ approvalsList.size());
			
			JSONArray jsonArray=new JSONArray();
			for (Approvals approval: approvalsList) {
				JSONObject jsonObj=new JSONObject();
				jsonObj.put("docType",approval.getBusinessprocesstype());
				String companyUrl="";
				if(comp.getCompanyURL().contains("http://")){
					companyUrl=comp.getCompanyURL().replace("http://", " ");
				}else{
					companyUrl=comp.getCompanyURL();
				}
				if(comp.getCompanyURL().contains("/")){
					companyUrl=companyUrl.replace("/", "").trim();
				}else{
					companyUrl=companyUrl.trim();
				}
				String docType=approval.getBusinessprocesstype().trim();
				String linkToCall="";
				try{
				if(docType.equals(AppConstants.APPROVALCONTRACT)){
					Contract contract=ofy().load().type(Contract.class).filter("companyId",comp.getCompanyId()).filter("count", approval.getBusinessprocessId()).first().now();
					linkToCall="/slick_erp/pdfservice"+"?Id="+contract.getId()+"&"+"type="+"c"+"&"+"preprint="+"plane";
				}else if(docType.equals(AppConstants.APPROVALDELIVERYNOTE)){
					DeliveryNote dN=ofy().load().type(DeliveryNote.class).filter("companyId", comp.getCompanyId()).filter("count", approval.getBusinessprocessId()).first().now();
					linkToCall="/slick_erp/deliverynotepdf"+"?Id="+dN.getId();
				}else if(docType.equals(AppConstants.APPROVALSALESORDER)){
					SalesOrder salesOrder=ofy().load().type(SalesOrder.class).filter("companyId", comp.getCompanyId()).filter("count", approval.getBusinessprocessId()).first().now();
					linkToCall="/slick_erp/pdfsales"+"?Id="+salesOrder.getId()+"&"+"type="+"so";
				}else if(docType.equals(AppConstants.APPROVALQUOTATION)){
					Quotation quotation=ofy().load().type(Quotation.class).filter("companyId", comp.getCompanyId()).filter("count", approval.getBusinessprocessId()).first().now();
					linkToCall="/slick_erp/pdfservice"+"?Id="+quotation.getId()+"&"+"type="+"q"+"&"+"preprint="+"plane";
				}else if(docType.equals(AppConstants.APPROVALSALESQUOTATION)){
					SalesQuotation salesquotation=ofy().load().type(SalesQuotation.class).filter("companyId", comp.getCompanyId()).filter("count", approval.getBusinessprocessId()).first().now();
					linkToCall="/slick_erp/pdfsales"+"?Id="+salesquotation.getId()+"&"+"type="+"sq"+ "&preprint=plane";
				}else if(docType.equals(AppConstants.APPROVALINVOICEDETAILS)){
					Invoice invoiceEntity=ofy().load().type(Invoice.class).filter("companyId", comp.getCompanyId()).filter("count", approval.getBusinessprocessId()).first().now();
					if(!invoiceEntity.isMultipleOrderBilling()){
						linkToCall="/slick_erp/serviceinvoiceprint"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"plane"+"&"+"type=SingleBilling";
					}else{
						linkToCall="/slick_erp/serviceinvoiceprint"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"plane"+"&"+"type=MultipleBilling";
					}
				}else if(docType.equals(AppConstants.APPROVALEXPENSEMANAGMENT)){
					MultipleExpenseMngt expense=ofy().load().type(MultipleExpenseMngt.class).filter("companyId", comp.getCompanyId()).filter("count", approval.getBusinessprocessId()).first().now();
					linkToCall="/slick_erp/pdfexpense"+"?Id="+expense.getId()+ "&" + "type=" + "multiple"+"&"+"companyId="+expense.getCompanyId();
				}
				}catch(Exception e){
					e.printStackTrace();
					logger.log(Level.SEVERE,"Error in link formation for print");
				}
				jsonObj.put("linkToCall","http://"+companyUrl+linkToCall);
				jsonObj.put("docId",approval.getBusinessprocessId());
				jsonObj.put("requestedBy", approval.getRequestedBy());
				try{
				jsonObj.put("requestedDate", sdf.format(approval.getCreationDate()));
				}catch(Exception e){
					e.printStackTrace();
				}
				jsonObj.put("approverName",approval.getApproverName());
				jsonObj.put("branch",approval.getBranchname());
				jsonObj.put("status",approval.getStatus());
				jsonObj.put("approvalLevel",approval.getApprovalLevel()+"");
				jsonObj.put("approvalCount",approval.getCount()+"");
				jsonObj.put("name", approval.getBpName());
				try{
				if(approval.getApprovalOrRejectDate()!=null){
					jsonObj.put("approveRejectDate", sdf.format(approval.getApprovalOrRejectDate()));
				}else{
					jsonObj.put("approveRejectDate","");
				}
				}catch(Exception e){
					e.printStackTrace();
				}
				jsonArray.add(jsonObj);
			}
		
			String jsonString=jsonArray.toString().replace("\\\\", "");
			logger.log(Level.SEVERE, jsonString);
			resp.getWriter().println(jsonString);
		}
	}
}

