package com.slicktechnologies.server.android.analytics;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpSchemes;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.simple.JSONObject;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.googlecode.objectify.ObjectifyService;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.server.CommunicationLogServiceImpl;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.GeneralServiceImpl;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.android.RegisterDevice;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.helperlayer.CustomerUser;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;

/**
 * This API is used to perform operations which are performed at individual lead, 
 * quotation , contract level such as lead successful , unsuccessful , change followup , 
 * change salesperson etc.
 * screename = name of entity
 * menuname = nme of operation which need to perform on that entity
 * @author pc1
 *
 */
public class AnalyticsMenuUpdate extends HttpServlet{

	/**
	 * 
	 */
	Company comp;
	private static final long serialVersionUID = 3974600993595923388L;

	static Logger logger=Logger.getLogger("AnalyticsStatusUpdate.class");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
//		resp.setHeader("Access-Control-Allow-Origin", AppConstants.TRENDZWEBSOLUTION);
		resp.setHeader("Access-Control-Allow-Origin", "*");
		String screenName = req.getParameter("screenName");
		String menuName= req.getParameter("menuName");
		String docId=req.getParameter("docId");
		String urlCalled = req.getRequestURL().toString().trim();
		logger.log(Level.SEVERE,"screenName="+screenName+" menuName="+menuName+" docId="+docId );
				
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		Exception exep = null;
		// try {
		comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		logger.log(Level.SEVERE, "Stage One");
		long companyId=comp.getCompanyId();
		
		String response=performActionAccordingToMenu(screenName,menuName,companyId,docId,req);
		
		resp.getWriter().print(response);
	}


	private String performActionAccordingToMenu(String screenName,
			String menuName, long companyId, String docId, HttpServletRequest req) {
		String response="Failed";
		/**
		 * Date : 17-01-2020 BY ANIL
		 * for clearing all loaded data from appengine cache memory
		 */
		ObjectifyService.reset();
		ofy().consistency(Consistency.STRONG);
		ofy().clear();
		/**
		 * End
		 */
		logger.log(Level.SEVERE, "Stage Two "+menuName);
		
		if(screenName.trim().equalsIgnoreCase(AppConstants.ANDROIDSCREENLIST.LEAD)){
			Lead lead=ofy().load().type(Lead.class).filter("companyId", companyId).filter("count",Integer.parseInt(docId)).first().now();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			if(menuName.trim().equalsIgnoreCase(AppConstants.ANDROIDMENULIST.LEAD_SUCCESSFULL)){
				//lead.setStatus(Lead.SUCCESSFULL);
				/** date 1.2.2019 added by komal to set status as successful **/
				lead.setStatus(AppConstants.Successful);
				ofy().save().entity(lead);
				return "Successfull";
			}else if(menuName.trim().equalsIgnoreCase(AppConstants.ANDROIDMENULIST.LEAD_UNCESSFULL)){
				/*
				 * Name: Apeksha Gunjal
				 * Date: 01/08/2018 @18:15
				 * Note: Adding unsuccessfulReason and unsuccessfulRemark for lead.
				 */
				String reason = "", remark = "";
				try{
					reason = req.getParameter("reason");
					if(reason == null)
						reason = "";
				}catch(Exception e){
					e.printStackTrace();
					reason = "";
				}
				try{
					remark = req.getParameter("remark");
					if (remark == null)
						remark = "";
				}catch(Exception e){
					e.printStackTrace();
					remark = "";
				}
				if(reason.trim().isEmpty())
					reason = remark;
				
				//lead.setStatus(Lead.UNSUCCESSFULL);
				/** date 1.2.2019 added by komal to set status as unsuccessful **/
			//	lead.setStatus(AppConstants.UNSUCCESSFUL);
				String remark1 = "";
				lead.setDocStatusReason(reason);
				if(!remark.trim().isEmpty())
					lead.setRemark(remark);
				
				remark1 = lead.getDescription()+"\n"+"Lead ID ="+lead.getCount()+" "+"Lead status ="+lead.getStatus()+"\n"
				+"has been "+AppConstants.UNSUCCESSFUL+" with remark "+"\n"+"Remark ="+remark
				+" "+" Date ="+DateUtility.addDaysToDate(new Date(), 0)+"Reason ="+"\n"+reason;
				lead.setRemark(remark1);
				
				lead.setStatus(AppConstants.UNSUCCESSFUL);
				
				ofy().save().entity(lead);
				return "Successfull";
			}else if(menuName.trim().equalsIgnoreCase(AppConstants.ANDROIDMENULIST.LEAD_COMMUNICATIONLOG)){
				List<InteractionType> interactionTypeList=ofy().load().type(InteractionType.class).filter("interactionDocumentName","Lead").filter("interactionDocumentId",lead.getCount()).list();
				Comparator<InteractionType> comp=new Comparator<InteractionType>() {
					@Override
					public int compare(InteractionType e1, InteractionType e2) {
						if (e1.getCount() == e2.getCount()) {
							return 0;
						}
						if (e1.getCount() < e2.getCount()) {
							return 1;
						} else {
							return -1;
						}
					}
				};
				Collections.sort(interactionTypeList,comp);
				
				
				JSONArray jsonArray=new JSONArray();
				
				for (InteractionType interactionType : interactionTypeList) {
					JSONObject object=new JSONObject();
					object.put("interactionDueDate", sdf.format(interactionType.getInteractionDueDate()));
					object.put("title", interactionType.getInteractionTitle());
					object.put("interactionTime", interactionType.getInteractionTime());
					try{
					object.put("interactionDueTime", interactionType.getInteractionDueTime());
					}catch(Exception e){
						e.printStackTrace();
					}
					object.put("creationDate",sdf.format(interactionType.getinteractionCreationDate()));
					object.put("user", interactionType.getCreatedBy());
					object.put("interactionPurpose", interactionType.getInteractionPurpose());
					object.put("interactionOutcome", interactionType.getInteractionOutcome());
					object.put("interactionID", interactionType.getCount());
					jsonArray.put(object);
				}
				String jsonString=jsonArray.toString().replace("\\\\", "");
				logger.log(Level.SEVERE, jsonString);
				return jsonString;
			}else if(menuName.trim().equalsIgnoreCase(AppConstants.ANDROIDMENULIST.LEAD_CHANGEFOLLOWUPDATE)){
				
				String data=req.getParameter("data");
				org.json.JSONObject jsonObject = null;
				try {
					jsonObject = new org.json.JSONObject(data);
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				String followUpDate=jsonObject.optString("followUpDate");
				String commitMessage=jsonObject.optString("commitMessage");
				String employeeName=jsonObject.optString("employeeName");
				String interactionGroup=jsonObject.optString("interactionGroup");
				String followUpTime=jsonObject.optString("followUpTime");
				NumberGeneration ng = new NumberGeneration();
				ng = ofy().load().type(NumberGeneration.class)
						.filter("companyId", comp.getCompanyId())
						.filter("processName", "InteractionType").filter("status", true)
						.first().now();
				
				List<InteractionType> interactionTypeList=ofy().load().type(InteractionType.class).filter("interactionDocumentName","Lead").filter("interactionDocumentId",lead.getCount()).list();
				ArrayList<InteractionType> communicationList=new ArrayList<InteractionType>();
				if(interactionTypeList.size()!=0){
					communicationList.addAll(interactionTypeList);
				}
				//Ashwini Patil added 4th parameter as personResponsible was not getting set
				InteractionType interactionType=getCommunicationLog("Service", "Lead", Integer.parseInt(docId), employeeName, commitMessage, followUpDate, lead.getPersonInfo(),null, null, lead.getBranch(), employeeName,lead,interactionGroup,followUpTime);
				communicationList.add(interactionType);
				CommunicationLogServiceImpl impl=new CommunicationLogServiceImpl();
				impl.saveCommunicationLog(communicationList);
				
				/**
				 * @author Anil , Date : 17-01-2020
				 * as lead's follow up date and time is also updated from saveCommuincationLog method
				 * but as instructed from nitin sir follow up date should  not be updated if lead status is created,cancelled,successful and unsuccessful
				 * so in that case we will update follow up date from here	
				 */
//				logger.log(Level.SEVERE, "LEAD status : "+lead.getStatus()+"/"+lead.getCount());
//				if(!lead.getStatus().equalsIgnoreCase(Lead.CREATED)
//						&&!lead.getStatus().equalsIgnoreCase(Lead.CANCELLED)
//						&&!lead.getStatus().equalsIgnoreCase("Successful")
//						&&!lead.getStatus().equalsIgnoreCase("Unsuccessful")){
//					try {
//						lead.setFollowUpDate(sdf.parse(followUpDate));
//						lead.setFollowUpTime(followUpTime);
//					} catch (ParseException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//						logger.log(Level.SEVERE,"Error in Follow Up"+e);
//					}
//					ofy().save().entity(lead).now();
//				}
				return "Successfull/"+(ng.getNumber()+1);
			}else if(menuName.trim().equalsIgnoreCase("Update Status")){
				String status=req.getParameter("data");
				lead.setStatus(status);
				ofy().save().entity(lead).now();
				logger.log(Level.SEVERE, "LEAD status : "+lead.getStatus()+"/"+lead.getCount());
				return "Successfull";
			}else if(menuName.trim().equalsIgnoreCase("Assign SalesPerson")){
				String employeeName=req.getParameter("data");
				lead.setEmployee(employeeName);
				ofy().save().entity(lead).now();
				logger.log(Level.SEVERE,lead.getEmployee()+"/"+lead.getCount());
				return "Successfull";
			}else if(menuName.trim().equalsIgnoreCase("Update FollowUpDate")){
				/**
				 * @author Anil , Date :17-01-2020
				 * for updating follow up date, we were not having separate method/call
				 * same method which was used to update communication log was called to update 
				 * lead follow up date
				 */
				try {
					lead.setFollowUpDate(sdf.parse(req.getParameter("followUpDate")));
					lead.setFollowUpTime(req.getParameter("followUpTime"));
				} catch (ParseException e) {
					e.printStackTrace();
					logger.log(Level.SEVERE,"Error in Follow Up"+e);
				}
				ofy().save().entity(lead).now();
				logger.log(Level.SEVERE, "LEAD FollowupDate  : "+req.getParameter("followUpDate")+"/"+lead.getCount());
				return "Successfull";
			}
			ofy().save().entity(lead);
			return response;
		}else if(screenName.trim().equalsIgnoreCase(AppConstants.ANDROIDSCREENLIST.QUOTATION)){
			Email email=new Email();
			Quotation quotation=ofy().load().type(Quotation.class).
					filter("companyId", companyId).filter("count",Integer.parseInt(docId)).first().now();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			if(menuName.trim().equalsIgnoreCase(AppConstants.ANDROIDMENULIST.QUOTATION_COMMUNICATIONLOG)){

				List<InteractionType> interactionTypeList=ofy().load().type(InteractionType.class).filter("interactionDocumentName","Quotation").filter("interactionDocumentId",quotation.getCount()).list();
				JSONArray jsonArray=new JSONArray();
				for (InteractionType interactionType : interactionTypeList) {
					JSONObject object=new JSONObject();
					object.put("interactionDueDate", sdf.format(interactionType.getInteractionDueDate()));
					object.put("title", interactionType.getInteractionTitle());
					object.put("interactionTime", interactionType.getInteractionTime());
					try{
					object.put("interactionDueTime", interactionType.getInteractionDueTime());
					}catch(Exception e){
						e.printStackTrace();
					}
					object.put("creationDate",interactionType.getinteractionCreationDate());
					object.put("user", interactionType.getCreatedBy());
					object.put("interactionPurpose", interactionType.getInteractionPurpose());
					object.put("interactionOutcome", interactionType.getInteractionOutcome());
					jsonArray.put(object);
				}
//				for (InteractionType interactionType : interactionTypeList) {
//					JSONObject object=new JSONObject();
//					object.put("interactionDueDate", sdf.format(interactionType.getInteractionDueDate()));
//					object.put("title", sdf.format(interactionType.getInteractionTitle()));
//					object.put("interactionTime", interactionType.getInteractionTime());
//					object.put("interactionPurpose", interactionType.getInteractionPurpose());
//					object.put("interactionOutcome", interactionType.getInteractionOutcome());
//					jsonArray.put(object);
//				}
				String jsonString=jsonArray.toString().replace("\\\\", "");
				logger.log(Level.SEVERE, jsonString);
				return jsonString;
			
			}else if(menuName.trim().equalsIgnoreCase(AppConstants.ANDROIDMENULIST.QUOTATION_FOLLOWDATE)){
				
				String data=req.getParameter("data");
				org.json.JSONObject jsonObject = null;
				try {
					jsonObject = new org.json.JSONObject(data);
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				String followUpDate=jsonObject.optString("followUpDate");
				String commitMessage=jsonObject.optString("commitMessage");
				String employeeName=jsonObject.optString("employeeName");
				String interactionGroup=jsonObject.optString("interactionGroup");
				String followUpTime=jsonObject.optString("followUpTime");
				NumberGeneration ng = new NumberGeneration();
				ng = ofy().load().type(NumberGeneration.class)
						.filter("companyId", comp.getCompanyId())
						.filter("processName", "InteractionType").filter("status", true)
						.first().now();
				
				List<InteractionType> interactionTypeList=ofy().load().type(InteractionType.class).filter("interactionDocumentName","Quotation").filter("interactionDocumentId",quotation.getCount()).list();
				ArrayList<InteractionType> communicationList=new ArrayList<InteractionType>();
				if(interactionTypeList.size()!=0){
					communicationList.addAll(interactionTypeList);
				}
				InteractionType interactionType=getCommunicationLog("Service", "Quotation", Integer.parseInt(docId), null, commitMessage, followUpDate, quotation.getCinfo(),null, null, quotation.getBranch(), employeeName,quotation,interactionGroup,followUpTime);
				communicationList.add(interactionType);
				CommunicationLogServiceImpl impl=new CommunicationLogServiceImpl();
				impl.saveCommunicationLog(communicationList);
//				try {
//					lead.setFollowUpDate(sdf.parse(followUpDate));
//				} catch (ParseException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//					logger.log(Level.SEVERE,"Error in Follow Up"+e);
//				}
//				ofy().save().entity(lead);
				return "Successfull/"+(ng.getNumber()+1);
			}else if(menuName.trim().equalsIgnoreCase("Assign SalesPerson")){
				/*
				 * Name: Apeksha Gunjal
				 * Date: 20/07/2018 @ 19:10
				 * Note: Changed "Assign Sales Person" to "Assign SalesPerson"
				 */
				String employeeName=req.getParameter("data");
				String data = req.getParameter("data");
				if(data.trim().equalsIgnoreCase("SalesQuotation")){
					SalesQuotation salesQuotation=ofy().load().type(SalesQuotation.class).
							filter("companyId", companyId).filter("count",Integer.parseInt(docId)).first().now();
					salesQuotation.setEmployee(employeeName);
					ofy().save().entity(salesQuotation);
				}else{
					quotation.setEmployee(employeeName);
					ofy().save().entity(quotation);
				}
				return "Successfull";
			}else if(menuName.trim().equalsIgnoreCase(AppConstants.ANDROIDMENULIST.QUOTATION_EMAIL)){
				/*
				 * Name: Apeksha Gunjal
				 * Date: 28/06/2018
				 * Note: Differentiate SalesQuotation and Servicequotation data for sending email.*/
				String data = req.getParameter("data");
				if(data.trim().equalsIgnoreCase("SalesQuotation")){
					SalesQuotation salesQuotation=ofy().load().type(SalesQuotation.class).
							filter("companyId", companyId).filter("count",Integer.parseInt(docId)).first().now();
					email.initiateSalesQuotationEmail(salesQuotation);
					return "Successfully Sent Email!!";
				}else{
					email.initiateServiceQuotationEmail(quotation);
					return "Successfully Sent Email!!";
				}
			}else if(menuName.trim().equalsIgnoreCase(AppConstants.ANDROIDMENULIST.LEAD_UNCESSFULL)){
				/*
				 * Name: Apeksha Gunjal
				 * Date: 01/08/2018 @18:15
				 * Note: Adding unsuccessful status for quotation with reason and remark.
				 */
				String reason = "", remark = "";
				try{
					reason = req.getParameter("reason");
					if(reason == null)
						reason = "";
				}catch(Exception e){
					e.printStackTrace();
					reason = "";
				}
				try{
					remark = req.getParameter("remark");
					if (remark == null)
						remark = "";
				}catch(Exception e){
					e.printStackTrace();
					remark = "";
				}
				if(reason.trim().isEmpty())
					reason = remark;
				
				String data = req.getParameter("data");
				if(data.trim().equalsIgnoreCase("SalesQuotation")){
					SalesQuotation salesQuotation=ofy().load().type(SalesQuotation.class).
							filter("companyId", companyId).filter("count",Integer.parseInt(docId)).first().now();
				//	salesQuotation.setStatus(Lead.UNSUCCESSFULL);
					/** date 1.2.2019 added by komal to set status as unsuccessful **/
					salesQuotation.setStatus(AppConstants.UNSUCCESSFUL);
					
					salesQuotation.setDocStatusReason(reason);
					if(!remark.trim().isEmpty())
						salesQuotation.setRemark(remark);
					
					ofy().save().entity(salesQuotation);
				}else{
				//	quotation.setStatus(Lead.UNSUCCESSFULL);
					/** date 1.2.2019 added by komal to set status as unsuccessful **/
					quotation.setStatus(AppConstants.UNSUCCESSFUL);
					
					quotation.setDocStatusReason(reason);
					if(!remark.trim().isEmpty())
						quotation.setRemark(remark);
					
					ofy().save().entity(quotation);
				}
				return "Successfull";
			}
			return response;
		}else if(screenName.trim().equalsIgnoreCase(AppConstants.ANDROIDSCREENLIST.CONTRACT)){
			
			return response;
		}else if(screenName.trim().equalsIgnoreCase(AppConstants.ANDROIDSCREENLIST.COMPLAIN_DASHBOARD)){
			Complain complain=ofy().load().type(Complain.class).filter("companyId",comp.getCompanyId()).filter("count", Integer.parseInt(docId)).first().now();
			if(menuName.trim().equalsIgnoreCase(AppConstants.ANDROIDMENULIST.COMPLAIN_DASHBOARD_ASSIGN_TO)){
				String data=req.getParameter("data");
				complain.setAssignto(data);
				ofy().save().entity(complain);
				return "Successfully assigned to "+data;
			}else if(menuName.trim().equalsIgnoreCase(AppConstants.ANDROIDMENULIST.COMPLAIN_STATUS_UPDATE)){
				String data=req.getParameter("data");
				complain.setCompStatus(data);
				ofy().save().entity(complain);
				
				return "Successfully changed status to "+data;
			}
			return response;
		}else if(screenName.trim().equalsIgnoreCase(AppConstants.ANDROIDSCREENLIST.CUSTOMER_SERVICE)){
			Service service=ofy().load().type(Service.class).filter("companyId", companyId).filter("count", Integer.parseInt(docId)).first().now();
			if(menuName.trim().equalsIgnoreCase(AppConstants.ANDROIDMENULIST.CUSTOMER_SERVICE_ASSIGN_TECHNICIAN)){
				String data=req.getParameter("data");
				org.json.JSONObject jsonObject = null;
				try {
					jsonObject = new org.json.JSONObject(data.trim());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				service.setEmployee(jsonObject.optString("technicianName"));
				String previousDescription="";
				if(service.getDescription()!=null){
				previousDescription=service.getDescription();
				}
				
				service.setDescription(previousDescription+jsonObject.optString("serviceDescription"));
				
				ofy().save().entity(service);
				try{
					sendSms(service,comp);
				}catch(Exception e){
					logger.log(Level.SEVERE,"Error in sending sms");
				}
				try{
					sendPushNotificationToCustomer(service,comp);
				}catch(Exception e){
					logger.log(Level.SEVERE,"Error in sending notifications to customer");
				}
				try{
					sendPushNotificationToTechnician(service,comp);
				}catch(Exception e){
					logger.log(Level.SEVERE,"Error in sending notifications to technician");
				}
				return "Successfull";
			}else if(menuName.trim().equalsIgnoreCase(AppConstants.ANDROIDMENULIST.CUSTOMER_SERVICE_RESCHEDULE)){
				
			}
			/**Date 20-1-2020 by AMOL Added a Service Schedule Menu Name ***/
			else if(menuName.trim().equalsIgnoreCase(AppConstants.ANDROIDMENULIST.CUSTOMER_SERVICE_SCHEDULE)){
				logger.log(Level.SEVERE, "INSIDE SERVICE SCHEDULE");
			    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				sdf.setTimeZone(TimeZone.getTimeZone("IST"));
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				String data=req.getParameter("data");
				
				GeneralServiceImpl generalServiceImpl=new GeneralServiceImpl();
				
				org.json.JSONObject jsonObject = null;
				try {
					jsonObject = new org.json.JSONObject(data.trim());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//				try{
				logger.log(Level.SEVERE, "INSIDE try of Service SCHEDULE data="+data);	
			    docId=jsonObject.optString("serviceId");
				String serDate=jsonObject.optString("serviceDate");
				String serviceTime=jsonObject.optString("serviceTime");
				String technicianName=jsonObject.optString("technicianName");
				String userName=jsonObject.optString("userName");
				String serviceDesc=null;//Ashwini Patil Date:6-02-2022
				try {
					serviceDesc=jsonObject.optString("serviceDescription");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				Date serviceDate=null;
				if(serDate!=null){
					try {
						serviceDate=sdf.parse(serDate);
						logger.log(Level.SEVERE, "Service Date"+serviceDate);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						logger.log(Level.SEVERE, "INSIDE catch of serviceDate");
					}
				}
				
				service.setCount(Integer.parseInt(docId));
				service.setServiceDate(serviceDate);
				service.setServiceTime(serviceTime);
				service.setEmployee(technicianName);
				if(serviceDesc!=null&&!serviceDesc.equals("")) {
					String oldDesc="";
					if(service.getDescription()!=null&&!service.getDescription().equals("")) {
						oldDesc="1. "+service.getDescription();
						service.setDescription(oldDesc+" 2. "+serviceDesc);
					}else {
						service.setDescription(serviceDesc);
					}
				}
				
				ArrayList<Integer> serviceIdList=new ArrayList<Integer>();
				serviceIdList.add(service.getCount());
				
				ArrayList<Integer> contractIdList=new ArrayList<Integer>();
				contractIdList.add(service.getContractCount());
				
				ArrayList<Service> serviceList=new ArrayList<Service>();
				serviceList.add(service);
				
				
				ArrayList<ProductGroupList>groupList =generalServiceImpl.getCombinedMaterialList(companyId, serviceIdList, contractIdList);
				
//				if(groupList==null||groupList.size()==0){
//					return "Product Group List is null";
//				}
				
				ArrayList<String>errorList =generalServiceImpl.validateTechnicianWarehouseQuantity(companyId, serviceList, groupList);
				if(errorList!=null&&errorList.size()!=0){
					
					String errorMsg="";
					
					for(String error:errorList){
						errorMsg=errorMsg+ error+ " " ;
					}
					
					return errorMsg;
				}
				ofy().save().entity(service).now();
				generalServiceImpl.createMMNForTechnician(companyId, groupList,userName, serviceIdList);
				
				
				logger.log(Level.SEVERE, "SUCCESSFULLY ");
				return "Successfull";
//				}
//				catch(Exception e){
//					e.printStackTrace();
//					logger.log(Level.SEVERE, "ERROR IN TRY "+e.getMessage());
//				}
			}
			else if(menuName.trim().equalsIgnoreCase(AppConstants.ANDROIDMENULIST.CUSTOMER_SERVICE_PLANNED)){
				logger.log(Level.SEVERE, "InSide Service Status Plan");   
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					sdf.setTimeZone(TimeZone.getTimeZone("IST"));
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));
					
					String data=req.getParameter("data");
					
					logger.log(Level.SEVERE, "Data Value"+data);	
					
					
					org.json.JSONObject jsonObject = null;
					try {
						jsonObject = new org.json.JSONObject(data.trim());
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try{
					logger.log(Level.SEVERE, "INSIDE try of Service SCHEDULE");	
				    docId=jsonObject.optString("serviceId");
					String serDate=jsonObject.optString("serviceDate");
					String serviceTime=jsonObject.optString("serviceTime");
					String technicianName=jsonObject.optString("technicianName");
					String userName=jsonObject.optString("userName");
					
					logger.log(Level.SEVERE, "Service Date INPUT"+serDate);
					Date serviceDate=null;
					if(serDate!=null){
						try {
							serviceDate=sdf.parse(serDate);
							logger.log(Level.SEVERE, "Service Date"+serviceDate);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							logger.log(Level.SEVERE, "INSIDE catch of serviceDate");
						}
					}
					
					service.setCount(Integer.parseInt(docId));
					service.setServiceDate(serviceDate);
					service.setServiceTime(serviceTime);
					service.setEmployee(technicianName);
					logger.log(Level.SEVERE, "Service Save Successfully ");
					ofy().save().entity(service);
					
					return "Successfull";
					}catch(Exception e){
						e.printStackTrace();
						logger.log(Level.SEVERE, "In Catch of  Plan"+e.getMessage());
					}
			}
			return response;
		}
		
		return response;
	}

	private void sendPushNotificationToTechnician(Service service, Company comp2) {
		// TODO Auto-generated method stub
		/*
		 * Name: Apeksha Gunjal
		 * Date: 29/06/2018 @ 17:30
		 * Note: sendPushNotificationToTechnician , added service date and time in notification
		 */
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String response="Service Id : "+service.getCount()+" is assigned to you!!on "
				+sdf.format(service.getServiceDate()) + " "+service.getServiceTime();

		logger.log(Level.SEVERE,"response: "+response);
		logger.log(Level.SEVERE,"serviceId: "+service.getCount());
		
		User user=ofy().load().type(User.class).filter("companyId", comp.getCompanyId()).filter("employeeName", service.getEmployee().trim()).first().now();
		logger.log(Level.SEVERE,"user: "+user.getUserName());
		
	 	RegisterDevice registerDevice=ofy().load().type(RegisterDevice.class).filter("companyId",comp.getCompanyId()).filter("applicationName",AppConstants.IAndroid.EVA_PEDIO).filter("userName", user.getUserName()).first().now();
	 	logger.log(Level.SEVERE,"registerDevice: "+registerDevice.getRegId());
	 	
		String deviceName = registerDevice.getRegId().trim();
		logger.log(Level.SEVERE,"deviceName"+deviceName);
		
		URL url = null;
		try {
			url = new URL("https://fcm.googleapis.com/fcm/send");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HttpURLConnection conn = null;
		try {
			conn = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		conn.setDoOutput(true);
		try {
			conn.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		conn.setRequestProperty("Content-Type",
				"application/json; charset=UTF-8");
		conn.setRequestProperty("Authorization", "key="
				+ comp2.getFcmServerKeyForPriora().trim());
		conn.setDoOutput(true);
		conn.setDoInput(true);

		try {
			conn.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "ProtocolException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		OutputStream os = null;
		try {
			os = conn.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
		// Log.d("Test", "From Get Post Method" + getPostData(values));
		try {
			writer.write(createJSONData(response, deviceName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException2 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		try {
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException3 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		InputStream is = null;
		try {
			is = conn.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String temp;
		String data = null;
		try {
			while ((temp = br.readLine()) != null) {
				data = data + temp;
			}
			logger.log(Level.SEVERE, "data data::::::::::" + data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		logger.log(Level.SEVERE, "Data::::::::::" + data);	
	
		}catch(Exception e){
			logger.log(Level.SEVERE,"error: "+e.getMessage());
		}
		
	}

	private String sendSms(Service service, Company comp) {
		// TODO Auto-generated method stub

		
		logger.log(Level.SEVERE,"Inside sendSMSFromTechnicianOnStarted SMS send");
		
//			SmsConfiguration smsConfig = ofy().load().type(SmsConfiguration.class).filter("companyId", comp.getCompanyId()).first().now();
//			
//			if(smsConfig!=null){
//				if(smsConfig.getStatus()==false){
//					return "SMS Config Not Active";
//				}
//				String senderId = smsConfig.getAccountSID();
//				String userName = smsConfig.getAuthToken();
//				String password = smsConfig.getPassword();
				
				SmsTemplate smsTemplate = ofy().load().type(SmsTemplate.class).filter("companyId", comp.getCompanyId()).filter("event", "Assign Technician").first().now();
				if(smsTemplate==null){
					return "SMS Template not found!";
				}
				if (smsTemplate != null) {
					if (smsTemplate.getStatus() == false) {
						return "SMS Template not Active!!";
					} else {
						
						String templateMsgwithbraces = smsTemplate.getMessage();
						System.out.println("SMS==="+templateMsgwithbraces);
						
						/**
						 * Date 29 jun 2017 added by vijay this is for Eco friendly	
						 */
//						if(bhashSMSAPIFlag){
//						String 	customerName = getCustomerName(service.getCustomerName(),comp.getCompanyId());
//						}
						/**
						 * ends here
						 */
						SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
						sdf.setTimeZone(TimeZone.getTimeZone("IST"));
						TimeZone.setDefault(TimeZone.getTimeZone("IST"));
//						if(event.equalsIgnoreCase("Technician Started")){
							String cutomerName = templateMsgwithbraces.replace("{CustomerName}", service.getCustomerName());
							String technician = cutomerName.replace("{TechnicianName}", service.getEmployee());
							String productName = technician.replace("{ProductName}", service.getProductName());
							String actaulMsg = productName.replace("{CompanyName}", comp.getBusinessUnitName());
							System.out.println("actaulMsg"+actaulMsg);
							logger.log(Level.SEVERE,"Calling SMS API method to send SMS");
							long phoneNumber = service.getPersonInfo().getCellNumber();
							SmsServiceImpl smsimpl=new SmsServiceImpl();
//							int value =  sms.sendSmsToClient(actaulMsg, phoneNumber, senderId,  userName,  password, comp.getCompanyId());
//							System.out.println(" Valuee==="+value);
//							if(value==1){
//								return "SMS sent successfully!!";
//							}
						   /**
							 * @author Vijay Date :- 03-05-2022
							 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
							 * whats and sms based on communication channel configuration.
							 */
							smsimpl.sendMessage(AppConstants.PEDIO,AppConstants.PEDIO,smsTemplate.getEvent(),smsTemplate.getCompanyId(),actaulMsg,phoneNumber,false);
							logger.log(Level.SEVERE,"after sendMessage method");
	
					}
	
				}
//			}
		
		return "Failed to send sms!!";
	
		
	}

	private void sendPushNotificationToCustomer(Service service, Company comp) {
		
		String response="event:Assign Technician"+"$"+"We have assigned "+service.getEmployee()+" as your technician for "+service.getProductName()+" service. He will shortly assist you.";//getJSONResponse(service);
		CustomerUser customerUser=ofy().load().type(CustomerUser.class).filter("companyId",comp.getCompanyId()).filter("cinfo.fullName",service.getPersonInfo().getFullName()).first().now(); 
		RegisterDevice device = ofy().load().type(RegisterDevice.class)
				.filter("companyId", comp.getCompanyId())
				.filter("userName", customerUser.getUserName()).first().now();
		String deviceName = device.getRegId().trim();
		logger.log(Level.SEVERE,"deviceName"+deviceName);
		URL url = null;
		try {
			url = new URL("https://fcm.googleapis.com/fcm/send");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HttpURLConnection conn = null;
		try {
			conn = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		conn.setDoOutput(true);
		try {
			conn.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		conn.setRequestProperty("Content-Type",
				"application/json; charset=UTF-8");
		conn.setRequestProperty("Authorization", "key="
				+ comp.getFcmServerKeyForKreto().trim());
		conn.setDoOutput(true);
		conn.setDoInput(true);

		try {
			conn.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "ProtocolException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		OutputStream os = null;
		try {
			os = conn.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
		// Log.d("Test", "From Get Post Method" + getPostData(values));
		try {
			writer.write(createJSONData(response, deviceName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException2 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		try {
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException3 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		InputStream is = null;
		try {
			is = conn.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String temp;
		String data = null;
		try {
			while ((temp = br.readLine()) != null) {
				data = data + temp;
			}
			logger.log(Level.SEVERE, "data data::::::::::" + data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		logger.log(Level.SEVERE, "Data::::::::::" + data);
	}
	
	private String createJSONData(String message, String deviceName) {
		// TODO Auto-generated method stub
		org.json.simple.JSONObject jObj1 = new org.json.simple.JSONObject();
		jObj1.put("message", message);

		org.json.simple.JSONObject jObj = new org.json.simple.JSONObject();
		jObj.put("data", jObj1);
		jObj.put("to", deviceName);

		String jsonString = jObj.toJSONString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE, "JSON Data Sent:::::::" + jsonString);

		return jsonString;
	}
	
	public static InteractionType getCommunicationLog(String moduleName, String documentName, int documentId,
			  String personResponible, String remark, String dueDate, PersonInfo personInfo, EmployeeInfo employeeInfo, String interactiongGroup, String Branch,String employeeName, Object object, String interactionGroup, String followUpTime) {

		InteractionType interaction = new InteractionType();
		
		SimpleDateFormat hours = new SimpleDateFormat("HH:mm:ss");
		hours.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		Date date = new Date();
	    String time=new String(hours.format(date).toString());

	    try {
			interaction.setinteractionCreationDate(sdf.parse(sdf.format(date)));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    interaction.setInteractionTime(time);
	    interaction.setCreatedBy(employeeName);
	    interaction.setInteractionPurpose(remark);
	    if(remark.trim().length()>0){
	    	interaction.setInteractionDueTime(followUpTime);
	    	interaction.setInteractionGroup(interactionGroup);
	    }else{
	    	interaction.setInteractionDueTime(interactionGroup);
	    	interaction.setInteractionGroup(interactionGroup);
	    	interaction.setInteractionPurpose(interactionGroup);
	    }
	    try {
			interaction.setInteractionDueDate(sdf.parse(dueDate));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    interaction.setCommunicationlogFlag(true);
	    
	    interaction.setInteractionDocumentId(documentId);
	    interaction.setInteractionModuleName(moduleName);
	    interaction.setInteractionDocumentName(documentName);
	    if(interactiongGroup!=null)
	    interaction.setInteractionGroup(interactiongGroup);	
	    if(Branch!=null)
	    interaction.setInteractionBranch(Branch);
	    logger.log(Level.SEVERE,"personResponible setting to "+personResponible);
	    if(personResponible!=null) {
	    interaction.setInteractionPersonResponsible(personResponible);
	    logger.log(Level.SEVERE,"personResponible set");
	    }
	    
	    
	    if(object instanceof Lead){
		    Lead lead=(Lead) object;
		    interaction.setCompanyId(lead.getCompanyId());
	    }else if(object instanceof Quotation){
	    	Quotation quotation=(Quotation) object;
		    interaction.setCompanyId(quotation.getCompanyId());
	    }
	    if(personInfo!=null){
	    	if(personInfo.isVendor()){
	    		System.out.println("Vendor");
		    	interaction.setInteractionPersonType("Vendor");
		    	interaction.setPersonVendorInfo(personInfo);
	    	}else{
	    		System.out.println("Customer");
		    	interaction.setInteractionPersonType("Customer");
		    	interaction.setPersonInfo(personInfo);
	    	}
	    }
	    if(employeeInfo!=null){
	    	interaction.setInteractionPersonType("Employee");
	    	interaction.setEmployeeInfo(employeeInfo);
	    }
		return interaction;
	}
}
