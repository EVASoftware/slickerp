package com.slicktechnologies.server.android.analytics;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
/**Description : Date 27.11.2019 : Currently we are not using this.
 * 
 * @author pc1
 *
 */
public class TodoList extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4466672273182279609L;
	Logger logger = Logger.getLogger("TodoList.class");
	Company comp;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String urlCalled = req.getRequestURL().toString().trim();
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");

		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();

		String loadType = req.getParameter("loadType");
		String toDateStr = req.getParameter("toDate");

		String jsonString = null;
		jsonString = createJSONData(loadType, toDateStr);

		resp.getWriter().println(jsonString);
	}

	private String createJSONData(String loadType, String toDateStr) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

		JSONArray jArray = null;
		if (loadType.equalsIgnoreCase("Lead")) {
			try {
				jArray = new JSONArray();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Gson gson = new Gson();
			List<Lead> leadList = null;
			try {
				if (toDateStr.trim().length() > 0) {
					leadList = ofy().load().type(Lead.class)
							.filter("companyId", comp.getCompanyId())
							.filter("followUpDate <=", sdf.parse(toDateStr))
							.list();
				} else {
					leadList = ofy().load().type(Lead.class)
							.filter("companyId", comp.getCompanyId())
							.filter("followUpDate <=", sdf.parse(toDateStr))
							.list();
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			for (int i = 0; i < leadList.size(); i++) {
				JSONObject obj = new JSONObject();
				obj.put("group", leadList.get(i).getGroup().trim());
				obj.put("type", leadList.get(i).getType().trim());
				obj.put("category", leadList.get(i).getCategory().trim());
				obj.put("status", leadList.get(i).getStatus().trim());
				obj.put("leadTitle", leadList.get(i).getTitle().trim());
				obj.put("leadDate",
						sdf.format(leadList.get(i).getCreationDate()).trim());
				obj.put("branch", leadList.get(i).getBranch().trim());
				obj.put("priority", leadList.get(i).getPriority().trim());
				obj.put("netPayable", leadList.get(i).getNetpayable() + "");
				obj.put("employeeName", leadList.get(i).getEmployee().trim());
				obj.put("leadId", leadList.get(i).getCount() + "");
				obj.put("customerName", leadList.get(i).getPersonInfo()
						.getFullName());
				obj.put("customerMobileNo", leadList.get(i).getPersonInfo()
						.getCellNumber()
						+ "");
				obj.put("custPocName", leadList.get(i).getPersonInfo()
						.getPocName());
				obj.put("customerId", leadList.get(i).getPersonInfo()
						.getCount()
						+ "");
				obj.put("description", leadList.get(i).getDescription() + "");
				if (leadList.get(i).getFollowUpDate() != null) {
					obj.put("followUpDate",
							sdf.format(leadList.get(i).getFollowUpDate()));

					List<InteractionType> interactionTypeList = ofy()
							.load()
							.type(InteractionType.class)
							.filter("interactionDocumentName", "Lead")
							.filter("interactionDocumentId",
									leadList.get(i).getCount()).list();
					if (interactionTypeList.size() != 0) {
						ArrayList<InteractionType> communicationList = new ArrayList<InteractionType>();
						if (interactionTypeList.size() != 0) {
							communicationList.addAll(interactionTypeList);
						}
						/**
						 * Date : 14 March 2018 BY Rahul Verma
						 */
						Comparator<InteractionType> comp = new Comparator<InteractionType>() {
							@Override
							public int compare(InteractionType e1,
									InteractionType e2) {
								// Integer coutn1=o1.getCount();
								// Integer count2=o2.getCount();
								// return ;
								if (e1.getCount() == e2.getCount()) {
									return 0;
								}
								if (e1.getCount() > e2.getCount()) {
									return 1;
								} else {
									return -1;
								}
							}
						};
						Collections.sort(communicationList, comp);
						/**
						 * End
						 */
						obj.put("lastCommunication",
								communicationList.get(
										communicationList.size() - 1)
										.getInteractionPurpose());
					} else {
						obj.put("lastCommunication", "");
					}
				}

				obj.put("productList",
						gson.toJson(leadList.get(i).getLeadProducts())
								.toString());
				obj.put("uniqueId", leadList.get(i).getId());
				jArray.add(obj);
			}
		}
	
			String jsonString = jArray.toString().replace("\\\\", "");
			logger.log(Level.SEVERE, jsonString);

			return jsonString;
	}
		
}
