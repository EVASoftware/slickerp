package com.slicktechnologies.server.android.analytics;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;





import com.slicktechnologies.server.utility.ServerAppUtility;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.complain.Complain;

/**Description : Date 27.11.2019 : Currently we are not using this.
 * 
 * @author pc1
 *
 */
public class ComplainDashboard extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9135187622456396753L;
	Company comp;

	Logger logger=Logger.getLogger("ComplainDashboard.class");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String urlCalled=req.getRequestURL().toString().trim();
	//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		
		comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
		
		List<Complain> complainList=ofy().load().type(Complain.class)
				.filter("companyId",comp.getCompanyId()).list();
//				.filter("compStatus", Contract.CREATED)

		/*
		 * Name: Apeksha Gunjal
		 * Date: 27/07/2018 @12:53
		 * Note: Adding JsonArray of complains 
		 */
		logger.log(Level.SEVERE, "complainList: "+complainList.size());
		JSONArray array = new JSONArray();
		
		if(complainList != null){
			for(int i =0; i < complainList.size(); i++){
				JSONObject jsonObject = new JSONObject();
				Complain complain = complainList.get(i);
				jsonObject.put("amountReceived", ""+complain.getAmountReceived());
				jsonObject.put("assignTo", ""+complain.getAssignto());
				jsonObject.put("balance", ""+complain.getBalance());
				jsonObject.put("billable", ""+complain.getBbillable());
				jsonObject.put("branch", ""+complain.getBranch());
				jsonObject.put("brandName", ""+complain.getBrandName());
				jsonObject.put("callerCellNo", ""+complain.getCallerCellNo());
				jsonObject.put("callerName", ""+complain.getCallerName());
				jsonObject.put("companyId", ""+complain.getCompanyId());
				jsonObject.put("complainDate", ""+complain.getComplainDate());
				jsonObject.put("complainTime", ""+complain.getComplaintime());
				jsonObject.put("complainTitle", ""+complain.getComplainTitle());
				jsonObject.put("compStatus", ""+complain.getCompStatus());
				jsonObject.put("contractId", ""+complain.getContrtactId());
				jsonObject.put("count", ""+complain.getCount());
				jsonObject.put("createdBy", ""+complain.getCreatedBy());
				jsonObject.put("customerFeedback", ""+complain.getCustomerFeedback());
				jsonObject.put("description", ""+complain.getDestription());
				jsonObject.put("modelNumber", ""+complain.getModelNumber());
				jsonObject.put("netPayble", ""+complain.getNetPayable());
				try{
				if(complain.getPic() != null){
					if(complain.getPic().getProductName() != null)
						jsonObject.put("productName", ""+complain.getPic().getProductName());
					else
						jsonObject.put("productName", "");
				}else
					jsonObject.put("productName", "");
				}catch(Exception e){
					logger.log(Level.SEVERE, "Error: "+e.getMessage());
					jsonObject.put("productName", "");
				}
				jsonObject.put("fullName", ""+complain.getPersoninfo().getFullName());
				jsonObject.put("remark", ""+complain.getRemark());
				jsonObject.put("salesPerson", ""+complain.getSalesPerson());
				jsonObject.put("serialNumber", ""+complain.getSerialNumber());
				jsonObject.put("userId", ""+complain.getUserId());
				
				array.add(jsonObject);
			}
		}
		
//		Gson gson=new Gson();
		logger.log(Level.SEVERE, "complainList json: "+array);
		String jsonStr= array.toString().replaceAll("\\\\", "");
				//gson.toJson(complainList).replaceAll("\\\\", "");
		resp.getWriter().println(jsonStr);
	}
}
