/**
 * 
 */
package com.slicktechnologies.server.android.analytics;
/** Description : This API is called at Lead Dashboard in the application. It filters Data based in 
 * the status In Process , Successful, UnSuccessful.
 * 
 */
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.server.android.analytics.AnalyticsOperations;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class FetchLeadFilterData extends HttpServlet {

	Logger logger = Logger.getLogger("FetchLeadFilterData.class");
	Company comp;
	
	
	private static final long serialVersionUID = -2557493125249724754L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String urlCalled=req.getRequestURL().toString().trim();
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		try {
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			
			comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();	
			resp.setContentType("text/plain");
			String companyId=comp.getCompanyId()+"";
			String data=req.getParameter("data").trim();
			List<String> customerNameList = new ArrayList<String>();
			List<Integer> customerIdList = new ArrayList<Integer>();
			List<Long> customerCellList = new ArrayList<Long>();
			List<String> statusList = new ArrayList<String>();
			List<String> technicianList = new ArrayList<String>();
			List<String> branchList = new ArrayList<String>();
			JSONObject jsonData = null;
			
			try {
				jsonData = new JSONObject(data);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
			
			JSONArray customerArray = null;
			try {
				customerArray = jsonData.getJSONArray("customer");
				for(int i=0; i< customerArray.length(); i++){
					JSONObject jObj = null;
					try {
						jObj = customerArray.getJSONObject(i);
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					customerIdList.add(Integer.parseInt(jObj.getString("id")));
					customerNameList.add(jObj.getString("name"));
					customerCellList.add(Long.parseLong(jObj.getString("cell")));					
				}
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				//e1.printStackTrace();
			}
			JSONArray statusArray = null;
			try {
				statusArray = jsonData.getJSONArray("status");
				for(int i=0; i< statusArray.length(); i++){
					JSONObject jObj = null;
					try {
						jObj = statusArray.getJSONObject(i);
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					if(jObj.getString((i+1)+"").equalsIgnoreCase("Successful")){
						statusList.add("Successful");
						statusList.add("Sucessful");
						statusList.add("Successfull");
						statusList.add("Sucessfull");
					}else if(jObj.getString((i+1)+"").equalsIgnoreCase("UnSuccessful")){
						statusList.add("UnSuccessful");
						statusList.add("UnSucessful");
						statusList.add("UnSuccessfull");
						statusList.add("UnSucessfull");
						statusList.add("Unsuccessful");
						statusList.add("Unsucessful");
						statusList.add("Unsuccessfull");
						statusList.add("Unsucessfull");
					}else{
						statusList.add(jObj.getString((i+1)+""));
					}
				}
			} catch (JSONException e1) {
				/// TODO Auto-generated catch block
				//e1.printStackTrace();
			}
			JSONArray technicianArray = null;
			try {
				technicianArray = jsonData.getJSONArray("technician");
				for(int i=0; i< technicianArray.length(); i++){
					JSONObject jObj = null;
					try {
						jObj = technicianArray.getJSONObject(i);
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					technicianList.add(jObj.getString((i+1)+""));
				}
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				//e1.printStackTrace();
			}
			
			Date fromDate = null;
			Date toDate = null;
			try{
				fromDate = sdf.parse(jsonData.optString("fromDate").trim());
			}catch(Exception e){
				fromDate = null;
			}
			try{
				toDate = sdf.parse(jsonData.optString("toDate").trim());
			}catch(Exception e){
				toDate = null;
			}
			logger.log(Level.SEVERE, "from : "+ fromDate + " to"+ toDate);
			for(String status : statusList){
				logger.log(Level.SEVERE,"Status  : "+ status);
			}
			JSONArray branchArray = null;
			try {
				branchArray = jsonData.getJSONArray("branch");
				for(int i=0; i< branchArray.length(); i++){
					JSONObject jObj = null;
					try {
						jObj = branchArray.getJSONObject(i);
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					branchList.add(jObj.getString((i+1)+""));
				}
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				//e1.printStackTrace();
			}
			List<Lead> leadList = null;
			if(fromDate != null && toDate != null && customerIdList.size() == 0
					&& technicianList.size() == 0 && statusList.size() == 0 && branchList.size()==0){
				logger.log(Level.SEVERE, "condition 1");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("followUpDate >=",fromDate)
						.filter("followUpDate <",toDate).list();
			}else if(fromDate != null && toDate != null && customerIdList.size() > 0
					&& technicianList.size() == 0 && statusList.size() == 0 && branchList.size()==0){
				logger.log(Level.SEVERE, "condition 2");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("followUpDate >=",fromDate)
						.filter("followUpDate <",toDate)
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() > 0
					&& technicianList.size() > 0 && statusList.size() == 0 && branchList.size()==0){
				logger.log(Level.SEVERE, "condition 3");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("followUpDate >=",fromDate)
						.filter("followUpDate <",toDate)
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.filter("employee IN", technicianList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() > 0
					&& technicianList.size() > 0 && statusList.size() > 0 && branchList.size()==0){
				logger.log(Level.SEVERE, "condition 4");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("followUpDate >=",fromDate)
						.filter("followUpDate <",toDate)
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.filter("employee IN", technicianList)
						.filter("status IN", statusList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() == 0
					&& technicianList.size() > 0 && statusList.size() > 0 && branchList.size()==0){
				logger.log(Level.SEVERE, "condition 5");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("followUpDate >=",fromDate)
						.filter("followUpDate <",toDate)
						.filter("employee IN", technicianList)
						.filter("status IN", statusList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() > 0
					&& technicianList.size() == 0 && statusList.size() > 0 && branchList.size()==0){
				logger.log(Level.SEVERE, "condition 6");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("followUpDate >=",fromDate)
						.filter("followUpDate <",toDate)
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.filter("status IN", statusList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() == 0
					&& technicianList.size() == 0 && statusList.size() > 0 && branchList.size()==0){
				logger.log(Level.SEVERE, "condition 7");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("followUpDate >=",fromDate)
						.filter("followUpDate <",toDate)
						.filter("status IN", statusList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() == 0
					&& technicianList.size() > 0 && statusList.size() == 0 && branchList.size()==0){
				logger.log(Level.SEVERE, "condition 8");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("followUpDate >=",fromDate)
						.filter("followUpDate <",toDate)
						.filter("employee IN", technicianList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() == 0
					&& technicianList.size() == 0 && statusList.size() == 0 && branchList.size() > 0){
				logger.log(Level.SEVERE, "condition 9");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("followUpDate >=",fromDate)
						.filter("followUpDate <",toDate)
						.filter("branch IN", branchList).list();
			}else if(fromDate != null && toDate != null && customerIdList.size() > 0
					&& technicianList.size() == 0 && statusList.size() == 0 && branchList.size() > 0){
				logger.log(Level.SEVERE, "condition 10");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("followUpDate >=",fromDate)
						.filter("followUpDate <",toDate)
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.filter("branch IN", branchList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() > 0
					&& technicianList.size() > 0 && statusList.size() == 0 && branchList.size() > 0){
				logger.log(Level.SEVERE, "condition 11");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("followUpDate >=",fromDate)
						.filter("followUpDate <",toDate)
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.filter("employee IN", technicianList)
						.filter("branch IN", branchList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() > 0
					&& technicianList.size() > 0 && statusList.size() > 0 && branchList.size() > 0){
				logger.log(Level.SEVERE, "condition 12");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("followUpDate >=",fromDate)
						.filter("followUpDate <",toDate)
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.filter("employee IN", technicianList)
						.filter("status IN", statusList)
						.filter("branch IN", branchList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() == 0
					&& technicianList.size() > 0 && statusList.size() > 0 && branchList.size() > 0){
				logger.log(Level.SEVERE, "condition 13");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("followUpDate >=",fromDate)
						.filter("followUpDate <",toDate)
						.filter("employee IN", technicianList)
						.filter("status IN", statusList)
						.filter("branch IN", branchList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() > 0
					&& technicianList.size() == 0 && statusList.size() > 0 && branchList.size() > 0){
				logger.log(Level.SEVERE, "condition 14");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("followUpDate >=",fromDate)
						.filter("followUpDate <",toDate)
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.filter("status IN", statusList)
						.filter("branch IN", branchList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() == 0
					&& technicianList.size() == 0 && statusList.size() > 0 && branchList.size() > 0){
				logger.log(Level.SEVERE, "condition 15");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("followUpDate >=",fromDate)
						.filter("followUpDate <",toDate)
						.filter("status IN", statusList)
						.filter("branch IN", branchList)
						.list();
			}else if(fromDate != null && toDate != null && customerIdList.size() == 0
					&& technicianList.size() > 0 && statusList.size() == 0 && branchList.size() > 0){
				logger.log(Level.SEVERE, "condition 16");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("followUpDate >=",fromDate)
						.filter("followUpDate <",toDate)
						.filter("employee IN", technicianList)
						.filter("branch IN", branchList)
						.list();
			}else if(fromDate == null && toDate == null && customerIdList.size() > 0
					&& technicianList.size() == 0 && statusList.size() == 0 && branchList.size()==0){
				logger.log(Level.SEVERE, "condition 17");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.list();
			}else if(fromDate == null && toDate == null && customerIdList.size() > 0
					&& technicianList.size() > 0 && statusList.size() == 0 && branchList.size()==0){
				logger.log(Level.SEVERE, "condition 18");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.filter("employee IN", technicianList)
						.list();
			}else if(fromDate == null && toDate == null && customerIdList.size() > 0
					&& technicianList.size() > 0 && statusList.size() > 0 && branchList.size()==0){
				logger.log(Level.SEVERE, "condition 19");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.filter("employee IN", technicianList)
						.filter("status IN", statusList)
						.list();
			}else if(fromDate == null && toDate == null && customerIdList.size() > 0
					&& technicianList.size() > 0 && statusList.size() > 0 && branchList.size()>0){
				logger.log(Level.SEVERE, "condition 20");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.filter("employee IN", technicianList)
						.filter("status IN", statusList)
						.filter("branch IN", branchList)
						.list();
			}else if(fromDate == null && toDate == null && customerIdList.size() > 0
					&& technicianList.size() > 0 && statusList.size() == 0 && branchList.size()>0){
				logger.log(Level.SEVERE, "condition 21");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.filter("employee IN", technicianList)
						.filter("branch IN", branchList)
						.list();
			}else if(fromDate == null && toDate == null && customerIdList.size() > 0
					&& technicianList.size() == 0 && statusList.size() == 0 && branchList.size()>0){
				logger.log(Level.SEVERE, "condition 22");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.filter("branch IN", branchList)
						.list();
			}else if(fromDate == null && toDate == null && customerIdList.size() > 0
					&& technicianList.size() == 0 && statusList.size() > 0 && branchList.size() == 0){
				logger.log(Level.SEVERE, "condition 23");
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("personInfo.count IN", customerIdList)
						.filter("personInfo.fullName IN", customerNameList)
						.filter("personInfo.cellNumber IN", customerCellList)
						.filter("status IN", statusList)
						.list();
			}
			if(leadList != null && leadList.size() > 0){
				AnalyticsOperations operations = new AnalyticsOperations();
				org.json.simple.JSONArray jArray = operations.getLeadArray(leadList);
				String jsonString=jArray.toString().replace("\\\\", "");
				logger.log(Level.SEVERE, jsonString);
				resp.getWriter().write(jsonString);
			}else{
				resp.getWriter().write("No Data Found.");
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	

}
