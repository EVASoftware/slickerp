package com.slicktechnologies.server.android.analytics;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;

public class LoadLeadDetails extends HttpServlet{

	/**
	 * DESCRIPTION: created by Amol  
	 *   This API is used to Load Lead Details On "LeadID" In PRIORA APP
	 */
	private static final long serialVersionUID = 1413913458799889412L;
	Logger logger = Logger.getLogger("LoadleadDetails.class");
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	
	Company comp;
	DecimalFormat formatter; 
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE,"Inside DOGET");
//		super.doGet(req, resp);
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
//		super.doPost(req, resp);
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		String urlCalled=req.getRequestURL().toString().trim();
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
         comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
         logger.log(Level.SEVERE,"COMPANY URL"+splitUrl[0]);
		logger.log(Level.SEVERE,"Inside DOPOST");
		String loadType=req.getParameter("loadType");
		
		String jsonString=null;
		
		if(loadType.trim().equalsIgnoreCase("LeadId")){
			logger.log(Level.SEVERE,"Inside LOADTYPE");
			int leadId = Integer.parseInt(req.getParameter("count"));
			logger.log(Level.SEVERE,"Inside LeadID");
			JSONObject jsonObject = getLeadInfo(comp.getCompanyId(),leadId);
			logger.log(Level.SEVERE, "getLeadData in  jsonObject: "+jsonObject);
			jsonString = ""+jsonObject.toString().replaceAll("\\\\", "");
		}
		
		
		resp.getWriter().println(jsonString);
		logger.log(Level.SEVERE,"END OF DOPOST");
	}

	private JSONObject getLeadInfo(Long companyId, int leadId) {
		
		logger.log(Level.SEVERE,"Inside LeadInfo Method");
		
		JSONArray jArray = new JSONArray();
		JSONObject object = new JSONObject();
		try{
//	        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	        Lead lead = ofy().load().type(Lead.class)
					.filter("companyId", companyId)
					.filter("count", leadId).first().now();
	       
	        if(lead!=null){
	        	
	        
	        
	        
			object.put("count", leadId);
			object.put("custId",lead.getPersonInfo().getCount());
			object.put("cellNumber", lead.getPersonInfo().getCellNumber());
			
			
			object.put("creationDate",(sdf.format(lead.getCreationDate())));
			
			object.put("branch", lead.getBranch());
			object.put("type", lead.getType());
			object.put("description", lead.getDescription());
			object.put("netpayable", lead.getNetpayable());
			if(lead.getFollowUpDate()!=null){
				try {
			object.put("followUpDate", sdf.format(sdf.parse(sdf.format(lead.getFollowUpDate()))));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				List<InteractionType> interactionTypeList=ofy().load().type(InteractionType.class).filter("interactionDocumentName","Lead").filter("interactionDocumentId",lead.getCount()).list();
				if(interactionTypeList.size()!=0){
				ArrayList<InteractionType> communicationList=new ArrayList<InteractionType>();
				if(interactionTypeList.size()!=0){
					communicationList.addAll(interactionTypeList);
				}
			
				Comparator<InteractionType> comp=new Comparator<InteractionType>() {
					@Override
					public int compare(InteractionType e1, InteractionType e2) {
						if (e1.getCount() == e2.getCount()) {
							return 0;
						}
						if (e1.getCount() < e2.getCount()) {
							return 1;
						} else {
							return -1;
						}
					}
				};
				Collections.sort(communicationList,comp);
				
				JSONArray jsonArray=new JSONArray();
				boolean greaterThenFive=false;
				int sizeOfCommunication=0;
				if(communicationList.size()>5){
					sizeOfCommunication=5;
					greaterThenFive=true;
				}else{
					greaterThenFive=false;
					sizeOfCommunication=communicationList.size();
				}
//				sdf = new SimpleDateFormat("dd MMM yyyy");
//				sdf.setTimeZone(TimeZone.getTimeZone("IST"));
//				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				for (int j = 0; j < sizeOfCommunication; j++) {
					JSONObject objInteractype=new JSONObject();
					objInteractype.put("communicationId",communicationList.get(j).getCount());
					objInteractype.put("communicationCreationDate",sdf.format(communicationList.get(j).getinteractionCreationDate()));
					objInteractype.put("communicationTime",communicationList.get(j).getInteractionTime());
					objInteractype.put("communicationUsername",communicationList.get(j).getCreatedBy());
					objInteractype.put("communicationRemark",communicationList.get(j).getInteractionPurpose());
					objInteractype.put("communicationDueDate",sdf.format(communicationList.get(j).getInteractionDueDate()));
					objInteractype.put("communicationDueTime",communicationList.get(j).getInteractionDueTime());
					objInteractype.put("greaterThenFive",greaterThenFive);
					jsonArray.add(objInteractype);
				}
				object.put("lastFiveCommunication",jsonArray);
				object.put("lastCommunication",communicationList.get(0).getInteractionPurpose());
				
				}else{
					object.put("lastCommunication", "");
					object.put("lastFiveCommunication", "");
				}
				}
			    object.put("group", lead.getGroup());
			    object.put("count", lead.getCount());
			    object.put("pocName", lead.getPersonInfo().getPocName());
			    object.put("createdBy", lead.getCreatedBy());
			    object.put("unsuccessfulRemark", lead.getRemark());
			    object.put("unsuccessfulReason", lead.getDocStatusReason());
			    object.put("priority", lead.getPriority());
			    object.put("fullName", lead.getPersonInfo().getFullName());
			    object.put("followUpDate", sdf.format(lead.getFollowUpDate()));
			    object.put("title", lead.getTitle());
			    object.put("salesPerson", lead.getEmployee());
			    object.put("category", lead.getCategory());
			    
			    boolean flag =true;
				if(lead.getLeadProducts() != null && lead.getLeadProducts().size() > 0){
					if(lead.getLeadProducts().get(0).getPrduct() instanceof ServiceProduct){
						flag = true;
					}else{
						flag = false;
					}
				}
			    
			    
			    
			    object.put("isServiceProduct", flag);
			    Gson gson = new Gson();
			    
			    JSONArray jsonArray1=new JSONArray();
			    for(SalesLineItem item:lead.getLeadProducts()){
					JSONObject objInteractype=new JSONObject();
					objInteractype.put("id",item.getPrduct().getCount());
					objInteractype.put("productName",item.getProductName());
					objInteractype.put("productType",item.getPrduct().getProductType());
					objInteractype.put("price",item.getPrice());
					objInteractype.put("noOfServices",item.getNo_Of_Services());
					objInteractype.put("specification",item.getSpecification());
					objInteractype.put("duration",item.getDuration());
					objInteractype.put("specifications",item.getSpecification());
					objInteractype.put("quantity",item.getQty());
					jsonArray1.add(objInteractype);
			    }
			    object.put("productList", jsonArray1);
				
			    
			    
			    object.put("uniqueId", lead.getId());
			    object.put("status", lead.getStatus());
			    if(lead.getFollowUpTime()!=null){
			    object.put("followUpTime", lead.getFollowUpTime());
			    }else{
			    	 object.put("followUpTime", "");
			    }
//			    /**Date 6-1-2020 by Amol Added a ContractCount field to check whether ContractId is present in lead or not
//			     if it ContractCount is present there then in lead screen every field should be Non-Editable**/
//			     if(lead.getContractCount()!=null){
//			      object.put("contractCount", lead.getContractCount());
//			     }else{
//			      object.put("contractCount", "");	 
//			     }
			    
	        }
		}
		catch(Exception e){
			logger.log(Level.SEVERE, "getLeadinfo: "+e);
		}
		logger.log(Level.SEVERE,"END OF LEADLOADDATA");
		return object;
	}

}
