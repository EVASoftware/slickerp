package com.slicktechnologies.server.android.analytics;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.StyledEditorKit.BoldAction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.approvalutility.ApprovableScreen;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.views.assesmentreport.AssesmentReportEmbedForTable;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.addhocprinting.CreateCustomerServicePdfServlet;
import com.slicktechnologies.server.email.SendGridEmailServlet;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.email.EmailTemplateConfiguration;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
/**
 * Description : This API is used to create and save entity which is sent in screenName from application.
 * Example : Lead , Quotation , Contract.
 * @author pc1
 *
 */
public class AnylaticsDataCreation extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6622852881711955879L;
	Logger logger = Logger.getLogger("AnylaticsDataCreation.class");
	int contractPeriod = 0;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}
 
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doPost(req, resp);
//		resp.setHeader("Access-Control-Allow-Origin", AppConstants.TRENDZWEBSOLUTION);
		resp.setHeader("Access-Control-Allow-Origin", "*");

		String screenData="";
		try {
			String data = req.getParameter("data");
			screenData = data.trim().replace("#and", "&");
			logger.log(Level.SEVERE, "data=" + data);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		String urlCalled = req.getRequestURL().toString().trim();
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		Exception exep = null;
		// try {
		
		long companyId = 0l;
		
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		logger.log(Level.SEVERE, "Stage One" + screenData);
		
		if (comp != null) {
			companyId = comp.getCompanyId();
		} else {
			companyId = Long.parseLong("5348024557502464");
		}
		JSONObject object = null;
		try {
			object = new JSONObject(screenData.trim());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		try {
			companyId=Long.parseLong(object.optString("companyId").trim());
			if(comp==null&&companyId>0) {
				comp=ofy().load().type(Company.class).filter("companyId", companyId).first().now();				
			}
		} catch (Exception e) {
//			e.printStackTrace();
		}
		String screenName = object.optString("screenName").trim();

		String apiCallfrom="";
		try {
			apiCallfrom = object.optString("apiCallFrom"); //portal
		} catch (Exception e) {
			// TODO: handle exception
		}
		/**
		 * ends here
		 */
		
		/**
		 * @author Vijay Date :- 15-12-2022
		 * Des :- when API call from customer portal then auth code will validate
		 * if auth code is validated then only process the API 
		 */
		ServerAppUtility apputility = new ServerAppUtility();

//		try {
//			if(apiCallfrom!=null && !apiCallfrom.equals("")) {
//				String strcompanyId = object.optString("authCode");
//				ServerAppUtility apputility = new ServerAppUtility();
//				boolean authonticationflag = apputility.validateAuthCode(strcompanyId,comp.getCompanyId());
//				logger.log(Level.SEVERE, "authonticationflag"+authonticationflag);
//				if(!authonticationflag) {
//					resp.getWriter().println("Authentication Failed!");
//					return;
//				}
//			}	
//		} catch (Exception e) {
//		}
		
		/**
		 * ends here
		 */
		
		if (screenName.trim().equalsIgnoreCase(
				AppConstants.ANDROIDSCREENLIST.LEAD)) {

			Lead lead = new Lead();
			// TODO Auto-generated method stub
			JSONArray productArray = null;
			try {
				productArray = object.getJSONArray("productArray");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}// --id/productName/noOfServices/price
			String title = object.optString("title").trim();
			String description = object.optString("description").trim();
			String status = object.optString("status").trim();
			String category = object.optString("category").trim();
			String type = object.optString("type").trim();
			String customerId = object.optString("customerId").trim();
			String customerCell = object.optString("customerCell").trim();
			String customerName = object.optString("customerName").trim();
			String customerPOCName = object.optString("customerPOCName").trim();
			String branch = object.optString("branch").trim();
			// String paymentTerms=object.optString("empl").trim();
			String employeeName = object.optString("employeeName").trim();
			String createdBy = object.optString("createdBy").trim();
			String followUpDate = object.optString("followUpDate").trim();
			String group = object.optString("group").trim();
			// String screenName=object.optString("screenName");
			/** date 16.4.2019 added by komal to check product type**/
			String isServiceProduct = object.optString("isServiceProduct").trim();
			/*8Date 26-12-2019 added a FollowupTime by Amol**/
			String followUpTime=object.optString("followUpTime").trim();
			logger.log(Level.SEVERE, "Inside Create Lead");
			String actionTask = object.optString("actionTask").trim();
			if (actionTask.trim().equalsIgnoreCase("Update")) {
				lead = ofy()
						.load()
						.type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("count",
								Long.parseLong(object.optString("count")))
						.first().now();
			}
			List<Integer> intIdList = new ArrayList<Integer>();
			List<Integer> intNoOfServicesList = new ArrayList<Integer>();
			List<Double> priceOfServicesList = new ArrayList<Double>();
			/*
			 * Name: Apeksha Gunjal Date: 07/09/2018 @ 19:16 Note: Added
			 * duration while adding service product in Lead.
			 */
			List<Integer> duration = new ArrayList<Integer>();
			Map<Integer , Double> quantityMap = new HashMap<Integer , Double>();
			Map<Integer , Double> priceMap = new HashMap<Integer , Double>();
			Map<Integer , Integer> durationMap = new HashMap<Integer , Integer>();
			double total = 0;
			if (productArray != null) {

				if (productArray.length() > 0) {
					for (int i = 0; i < productArray.length(); i++) {
						JSONObject obj = null;
						try {
							obj = productArray.getJSONObject(i);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						intIdList.add(Integer.parseInt(obj.optString("id")
								.trim()));
						quantityMap.put(Integer.parseInt(obj.optString("id")
								.trim()) ,Double.parseDouble(obj.optString(
										"quantity").trim()));
						intNoOfServicesList.add(Integer.parseInt(obj.optString(
								"noOfServices").trim()));
						
						priceMap.put(Integer.parseInt(obj.optString("id")
								.trim()) ,Double.parseDouble(obj.optString(
										"price").trim()));
						priceOfServicesList.add(Double.parseDouble(obj
								.optString("price").trim()));
						/*
						 * Name: Apeksha Gunjal Date: 07/09/2018 @ 19:16 Note:
						 * Added duration while adding service product in Lead.
						 */
						try {
							duration.add(Integer.parseInt(obj.optString(
									"duration").trim()));
							durationMap.put(Integer.parseInt(obj.optString("id")
									.trim()) ,Integer.parseInt(obj.optString(
											"duration").trim()));
						} catch (Exception e) {
							logger.log(Level.SEVERE, "error: " + e);
						}
					}
					
					List<SalesLineItem> listofSaleLineItem = new ArrayList<SalesLineItem>();
					if(isServiceProduct.equalsIgnoreCase("true")){
					List<ServiceProduct> serviceProductList = null;
					if (comp != null) {
						serviceProductList = ofy().load()
								.type(ServiceProduct.class)
								.filter("companyId", companyId)
								.filter("count IN", intIdList).list();
					} else {
						serviceProductList = ofy().load()
								.type(ServiceProduct.class)
								.filter("companyId", companyId)
								.filter("count IN", intIdList).list();
					}
					
					for (int i = 0; i < serviceProductList.size(); i++) {
						SalesLineItem items = new SalesLineItem();
						items.setPrduct(serviceProductList.get(i));
						items.setTermsAndConditions(new DocumentUpload());
						items.setProductImage(new DocumentUpload());
						items.getPrduct().setProductImage1(new DocumentUpload());
						items.getPrduct().setProductImage2(new DocumentUpload());
						items.getPrduct().setProductImage3(new DocumentUpload());
						   
						 
						items.getPrduct().setComment("");
						items.getPrduct().setCommentdesc("");
						items.getPrduct().setCommentdesc1("");
						items.getPrduct().setCommentdesc2("");
						items.setProductSrNo(i + 1);
						logger.log(Level.SEVERE,
								"serviceProductList.get(i).getProductCode()--->>>"
										+ serviceProductList.get(i)
												.getProductCode());
						items.setProductCode(serviceProductList.get(i)
								.getProductCode());
						items.setProductName(serviceProductList.get(i)
								.getProductName().trim());
						items.setProductCategory(serviceProductList.get(i)
								.getProductCategory().trim());
						if (serviceProductList.get(i).getUnitOfMeasurement() != null
								&& !serviceProductList.get(i)
										.getUnitOfMeasurement().equals("")) {
							items.setUnitOfMeasurement(serviceProductList
									.get(i).getUnitOfMeasurement().trim());
						}
						items.setQuantity(quantityMap.get(serviceProductList.get(i).getCount()));
						items.setArea("NA");
						items.setPrice(priceMap.get(serviceProductList.get(i).getCount()));
						items.setNo_Of_Services(intNoOfServicesList.get(i));
						items.setNumberOfService(intNoOfServicesList.get(i));//Ashwini Patil Date:11-11-2024 no of services are not getting mapped were lead created from priora app
							
						/*
						 * Name: Apeksha Gunjal Date: 07/09/2018 @ 19:16 Note:
						 * Added duration while adding service product in Lead.
						 */
						try {
							items.setDuration(durationMap.get(serviceProductList.get(i).getCount()));
						} catch (Exception e) {
							items.setDuration(serviceProductList.get(i)
									.getDuration());
						}
						items.setPremisesDetails("");
						items.setServicingTime(serviceProductList.get(i)
								.getServiceTime());
						items.setVatTax(serviceProductList.get(i).getVatTax());
						items.setServiceTax(serviceProductList.get(i)
								.getServiceTax());
						if(serviceProductList.get(i)
								.getServiceTax() != null){
						items.setServiceTaxEdit(serviceProductList.get(i)
								.getServiceTax().getTaxConfigName());
						}
						if(serviceProductList.get(i)
								.getVatTax() != null){
							items.setVatTaxEdit(serviceProductList.get(i)
								.getVatTax().getTaxConfigName());
						}
						total += quantityMap.get(serviceProductList.get(i).getCount()) *
									priceMap.get(serviceProductList.get(i).getCount());
						listofSaleLineItem.add(items);
					}
				}else{
					List<ItemProduct> itemProductList = null;
					if (comp != null) {
						itemProductList = ofy().load()
								.type(ItemProduct.class)
								.filter("companyId", companyId)
								.filter("count IN", intIdList).list();
					} else {
						itemProductList = ofy().load()
								.type(ItemProduct.class)
								.filter("companyId", companyId)
								.filter("count IN", intIdList).list();
					}
					
					for (int i = 0; i < itemProductList.size(); i++) {
						SalesLineItem items = new SalesLineItem();
						items.setPrduct(itemProductList.get(i));
						items.setTermsAndConditions(new DocumentUpload());
						items.setProductImage(new DocumentUpload());
						items.getPrduct().setProductImage1(new DocumentUpload());
						items.getPrduct().setProductImage2(new DocumentUpload());
						items.getPrduct().setProductImage3(new DocumentUpload());
						   
						 
						items.getPrduct().setComment("");
						items.getPrduct().setCommentdesc("");
						items.getPrduct().setCommentdesc1("");
						items.getPrduct().setCommentdesc2("");
						items.setProductSrNo(i + 1);
						logger.log(Level.SEVERE,
								"itemProductList.get(i).getProductCode()--->>>"
										+ itemProductList.get(i)
												.getProductCode());
						items.setProductCode(itemProductList.get(i)
								.getProductCode());
						items.setProductName(itemProductList.get(i)
								.getProductName().trim());
						items.setProductCategory(itemProductList.get(i)
								.getProductCategory().trim());
						if (itemProductList.get(i).getUnitOfMeasurement() != null
								&& !itemProductList.get(i)
										.getUnitOfMeasurement().equals("")) {
							items.setUnitOfMeasurement(itemProductList
									.get(i).getUnitOfMeasurement().trim());
						}
						items.setQuantity(quantityMap.get(itemProductList.get(i).getCount()));
						items.setArea("NA");
						items.setPrice(priceMap.get(itemProductList.get(i).getCount()));
						
						items.setPremisesDetails("");
						items.setVatTax(itemProductList.get(i).getVatTax());
						items.setServiceTax(itemProductList.get(i)
								.getServiceTax());
						if(itemProductList.get(i)
								.getServiceTax() != null){
						items.setServiceTaxEdit(itemProductList.get(i)
								.getServiceTax().getTaxConfigName());
						}
						if(itemProductList.get(i)
								.getVatTax() != null){
							items.setVatTaxEdit(itemProductList.get(i)
								.getVatTax().getTaxConfigName());
						}
						total += quantityMap.get(itemProductList.get(i).getCount()) *
								priceMap.get(itemProductList.get(i).getCount());
						listofSaleLineItem.add(items);
					}
				}
				
					lead.setLeadProducts(listofSaleLineItem);
				}
			}
			lead.setTitle(title);
			lead.setDescription(description);
			lead.setStatus(status);
			lead.setPriority("High");
			lead.setCategory(category);
			lead.setType(type);
			lead.setEmployee(employeeName);
			lead.setCreatedBy(createdBy);
			lead.setBranch(branch);
			lead.setGroup(group);
			lead.setNetpayable(getNetPayable(lead.getLeadProducts()));
			PersonInfo personInfo = new PersonInfo();
			personInfo.setCount(Integer.parseInt(customerId));
			// if (customer.isCompany()) {
			// personInfo.setFullName(customer.getCompanyName());
			// } else {
			personInfo.setFullName(customerName);
			// }
			personInfo.setCellNumber(Long.parseLong(customerCell));
			personInfo.setPocName(customerPOCName);
			lead.setCompanyId(companyId);
			lead.setPersonInfo(personInfo);

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			try {
				lead.setCreationDate(sdf.parse(sdf.format(new Date())));
				lead.setFollowUpDate(sdf.parse(followUpDate));
				/**Date 26-12-2019 by AMol set followuptime**/
				lead.setFollowUpTime(followUpTime);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ServerAppUtility serverapp=new ServerAppUtility();
			 List<ProductOtherCharges> list = new ArrayList<ProductOtherCharges>();
			  List<ProductOtherCharges> taxList = new ArrayList<ProductOtherCharges>();
			  ArrayList<SalesLineItem> salesLine = new ArrayList<SalesLineItem>();
			  salesLine.addAll(lead.getLeadProducts());
			  try {
				list.addAll(serverapp.addProdTaxes(salesLine , taxList , AppConstants.ACCOUNTTYPE));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			  ArrayList<ProductOtherCharges> arrBillTax=new ArrayList<ProductOtherCharges>();
				double assessValue=0;
				double tax = 0;
				for(int i=0;i<list.size();i++){
					ProductOtherCharges taxDetails=new ProductOtherCharges();
					taxDetails.setChargeName(list.get(i).getChargeName());
					taxDetails.setChargePercent(list.get(i).getChargePercent());
					assessValue=list.get(i).getAssessableAmount();//*paymentRecieved/100;
					taxDetails.setAssessableAmount(assessValue);				
					taxDetails.setIndexCheck(list.get(i).getIndexCheck());
					tax = tax + taxDetails.getAssessableAmount()*taxDetails.getChargePercent()/100;
					arrBillTax.add(taxDetails);
				}
			  lead.setProductTaxes(arrBillTax);		 
			  lead.setFinalTotalAmt(Math.round(total));
			  lead.setTotalAmount(Math.round(total));
			if (actionTask.trim().equalsIgnoreCase("Update")) {

			} else {
				NumberGeneration ng = new NumberGeneration();
				ng = ofy().load().type(NumberGeneration.class)
						.filter("companyId", comp.getCompanyId())
						.filter("processName", "Lead").filter("status", true)
						.first().now();

				long number = ng.getNumber();
				int count = (int) number;
				lead.setCount(count + 1);
				ng.setNumber(count + 1);
				ofy().save().entity(ng);
			}

			ofy().save().entity(lead);
			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject.put("docId", lead.getCount());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				jsonObject.put("netPayable", lead.getNetpayable() + "");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String responseStr = jsonObject.toString().replace("\\\\", "");
			resp.getWriter().println(responseStr + "");

		} else if (screenName.trim().equalsIgnoreCase(
				AppConstants.ANDROIDSCREENLIST.QUOTATION)) { 
			Quotation quotation =null; 
			boolean newQuotationFlag=true;
			
			String zohoQuotID="";
			try {
				zohoQuotID=object.optString("zohoQuotID").trim();
			}catch(Exception e){
				
			}
			if(zohoQuotID!=null&&!zohoQuotID.equals(""))
			{				
				quotation=ofy().load().type(Quotation.class).filter("companyId",comp.getCompanyId()).filter("zohoQuotID", zohoQuotID).first().now();
				if(quotation!=null) {
					logger.log(Level.SEVERE, "found existing quotation against zohoQuotID "+zohoQuotID+" updating same");
					newQuotationFlag=false;
				}else
					quotation=new Quotation();
				quotation.setZohoQuotID(zohoQuotID);
			}else
				quotation=new Quotation();
			

			/*
			 * Name: Apeksha Gunjal Date: 18/06/2018 Note: Create and Update
			 * Sales Quotation.
			 */
			SalesQuotation salesQuotation = new SalesQuotation();

			boolean isSalesQuotation = false;
			try {
				if (object.optString("quotationType").trim()
						.equalsIgnoreCase("serviceQuotation")) {
					isSalesQuotation = false;
				} else
					isSalesQuotation = true;
			} catch (Exception e) {
				e.printStackTrace();
			}
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));

			JSONArray productArray = null;
			try {
				productArray = object.getJSONArray("productArray");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}// --id/productName/noOfServices/price

			// String title=object.optString("title").trim();
			String numberRange="";
			try {
				numberRange=object.optString("NumberRange").trim();
			}catch(Exception e){
				
			}
			
			String quotationDate = object.optString("date").trim();
			String followUpDate = null;
			try {
				followUpDate=object.optString("followUpDate").trim();
			}catch(Exception e){
				
			}
			
			String validUntilDate = object.optString("validUntil").trim();
			String status = object.optString("status").trim();
			String category = object.optString("category").trim();
			String type = object.optString("type").trim();
			String group = object.optString("group").trim();
			String customerId = object.optString("customerId").trim();
			String customerCell = object.optString("customerCell").trim();
			String customerName = object.optString("customerName").trim();
			String customerPOCName = object.optString("customerPOCName").trim();
			String branch = object.optString("branch").trim();
			String paymentterms = object.optString("paymentTerms").trim();
			String employeeName = object.optString("employeeName").trim();
			String approverName = object.optString("approverName").trim();
			String actionTask = object.optString("actionTask").trim();
			;
			
			Customer customer = null;
			if(Integer.parseInt(customerId)>0) {
				customer= ofy().load().type(Customer.class)
						.filter("companyId", companyId)
						.filter("count", Integer.parseInt(customerId)).first()
						.now();			
			}
			

			if (!isSalesQuotation) {
				if (actionTask.trim().equalsIgnoreCase("Update")) {
					quotation = ofy()
							.load()
							.type(Quotation.class)
							.filter("companyId", comp.getCompanyId())
							.filter("count",
									Long.parseLong(object.optString("count")))
							.first().now();
				}
				
				/*
				 * Ashwini Patil
				 * Date:24-06-2024
				 * Pecopp wants to create quotation in eva when quotation gets submitted from zoho.
				 * Standard quotation api needs customerId to create quotation but in pecopp's case we cannot send eva customer id from zoho.
				 * So we will send entire customer details from zoho to this api call. Program will check if customer with this name exist or not.
				 * If exist then it will use that customer to create quotation
				 * else it will first create new customer and then it will create quotation
				 */
				if(customer==null)
					customer=createOrFetchCustomer(comp, object); 
				
				if(customer!=null) {
				
				PersonInfo personInfo = new PersonInfo();
				personInfo.setCount(customer.getCount());
				/*
				 * Name: Apeksha Gunjal Date: 21/07/2018 @ 15:34 Note: If
				 * customer is a company then set full name as a company name
				 */
				try {
					if (customer.isCompany()) {
						personInfo.setFullName(customer.getCompanyName());
					} else
						personInfo.setFullName(customer.getFullname());
				} catch (Exception e) {
					personInfo.setFullName(customer.getFullname());
				}
				personInfo.setCellNumber(customer.getCellNumber1());
				personInfo.setPocName(customer.getFullname());
				quotation.setCinfo(personInfo);
				// quotation.setCreatedBy();
				try {
					quotation.setValidUntill(sdf.parse(validUntilDate));
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				/** From Android Application **/
				try {
					quotation.setQuotationDate(sdf.parse(quotationDate));
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					quotation.setFollowUpDate(sdf.parse(followUpDate));
				} catch (ParseException e1) {
				}
				/** from Android App **/
				quotation.setCompanyId(companyId);
				if(apiCallfrom.equalsIgnoreCase("ZOHO"))
					quotation.setStatus(Quotation.APPROVED);
				else
					quotation.setStatus(Quotation.CREATED);
				
				if(!branch.equals("")){
					Branch branchEntity = ofy().load().type(Branch.class).filter("companyId", comp.getCompanyId())
											.filter("buisnessUnitName", branch).first().now();
					logger.log(Level.SEVERE, "branchEntity " + branchEntity);
					if(branchEntity==null){		
						resp.getWriter().println("Branch Name "+branch +" does not exist in the ERP System, Please define first."); 
						return;
					}
				}
				else{
					resp.getWriter().println("Branch Name should not be blank");
					return;
				}
				quotation.setBranch(branch);
				
				if(!approverName.equals("")){
					Employee employee = ofy().load().type(Employee.class).filter("companyId", comp.getCompanyId())
											.filter("fullname", approverName).first().now();
					logger.log(Level.SEVERE, "employee " + employee);
					if(employee==null){
							resp.getWriter().println(" Approver Name "+approverName +" does not exist in the ERP System. Please define first.");
							return;
					}
				}
				else{
					resp.getWriter().println(" Approver Name should not be blank.");
					return;
				}
				
				if(!employeeName.equals("")){
					Employee employee = ofy().load().type(Employee.class).filter("companyId", comp.getCompanyId())
											.filter("fullname", employeeName).first().now();
					logger.log(Level.SEVERE, "employee " + employee);
					if(employee==null){
							resp.getWriter().println(" Sales Person Name "+employeeName +" does not exist in the ERP System. Please define first.");
							return;
					}
				}
				quotation.setEmployee(employeeName);
				
				
				quotation.setApproverName(approverName);
				quotation.setType(type);
				quotation.setCategory(category);
				if(group!=null)
					quotation.setGroup(group);
				// quotation.setApproverName("Rohan Bhagde");
				/** Payment Terms **/
				List<PaymentTerms> paymentTermsList = new ArrayList<PaymentTerms>();
				if(paymentterms!=null&&!paymentterms.equals("")) {

					if (paymentterms.trim().equalsIgnoreCase("Monthly")||paymentterms.trim().contains("Monthly")) {
						int days = 0;
						for (int i = 0; i < 12; i++) {
							if (i + 1 == 12) {
								PaymentTerms payTerms = new PaymentTerms();
								payTerms.setPayTermDays(days);
								payTerms.setPayTermPercent(8.37);
								payTerms.setPayTermComment(i + 1 + " Payment");
								paymentTermsList.add(payTerms);
							} else {
								PaymentTerms payTerms = new PaymentTerms();
								payTerms.setPayTermDays(days);
								payTerms.setPayTermPercent(8.33);
								payTerms.setPayTermComment(i + 1 + " Payment");
								paymentTermsList.add(payTerms);
							}
							days = days + 30;
						}
					} else if (paymentterms.trim().equalsIgnoreCase("Quaterly")||paymentterms.trim().contains("Quaterly")||paymentterms.trim().contains("Quarterly")) {

						int days = 0;
						for (int i = 0; i < 4; i++) {
							PaymentTerms payTerms = new PaymentTerms();
							payTerms.setPayTermDays(days);
							payTerms.setPayTermPercent(25d);
							payTerms.setPayTermComment(i + 1 + " Payment");
							days = days + 90;
							paymentTermsList.add(payTerms);
						}

					} else if (paymentterms.trim().equalsIgnoreCase("Half Yearly")||paymentterms.trim().contains("Half Yearly")) {
						int days = 0;
						for (int i = 0; i < 2; i++) {
							PaymentTerms payTerms = new PaymentTerms();
							payTerms.setPayTermDays(days);
							payTerms.setPayTermPercent(50d);
							payTerms.setPayTermComment(i + 1 + " Payment");
							paymentTermsList.add(payTerms);
							days = days + 180;
						}
					} else if (paymentterms.trim().equalsIgnoreCase("Yearly")||paymentterms.trim().contains("Yearly")) {
						PaymentTerms payTerms = new PaymentTerms();
						payTerms.setPayTermDays(0);
						payTerms.setPayTermPercent(100d);
						payTerms.setPayTermComment("Full Payment");
						paymentTermsList.add(payTerms);
					} else {
						PaymentTerms payTerms = new PaymentTerms();
						payTerms.setPayTermDays(0);
						payTerms.setPayTermPercent(100d);
						payTerms.setPayTermComment("Default");
						paymentTermsList.add(payTerms);
					}
					quotation.setPayTerms(paymentterms);
					
				}else{
					PaymentTerms payTerms = new PaymentTerms();
					payTerms.setPayTermDays(0);
					payTerms.setPayTermPercent(100d);
					payTerms.setPayTermComment("Default");
					paymentTermsList.add(payTerms);
				}
					
				quotation.setPaymentTermsList(paymentTermsList);
				/** Done with Payment Terms **/

				/** Product **/
				JSONArray jsonArray = null;
				try {
					jsonArray = new JSONArray(req.getParameter("productArray"));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				List<Integer> intIdList = new ArrayList<Integer>();
				List<String> productCodeList = new ArrayList<String>();//Ashwini Patil Date:24-06-2024
				List<Integer> intNoOfServicesList = new ArrayList<Integer>();
				List<Double> priceOfServicesList = new ArrayList<Double>();
				/*
				 * Name: Apeksha Gunjal Date: 08/09/2018 @ 11:30 Note: Added
				 * duration while adding service product in Quotation.
				 */
				List<Integer> duration = new ArrayList<Integer>();
				ArrayList<String> startDate=new ArrayList<String>();
				ArrayList<String> endDate=new ArrayList<String>();
				for (int i = 0; i < productArray.length(); i++) {
					JSONObject obj = null;
					try {
						obj = productArray.getJSONObject(i);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if(Integer.parseInt(obj.optString("id").trim())>0)
						intIdList.add(Integer.parseInt(obj.optString("id").trim()));
					else
						productCodeList.add(obj.optString("productName").trim());
					intNoOfServicesList.add(Integer.parseInt(obj.optString(
							"noOfServices").trim()));
					priceOfServicesList.add(Double.parseDouble(obj.optString(
							"price").trim()));
					/*
					 * Name: Apeksha Gunjal Date: 08/09/2018 @ 11:30 Note: Added
					 * duration while adding service product in Quotation.
					 */
					try {
						duration.add(Integer.parseInt(obj.optString("duration")
								.trim()));
					} catch (Exception e) {
						logger.log(Level.SEVERE, "error: " + e);
					}
					
					try {
						startDate.add(obj.optString("startDate").trim());
					} catch (Exception e) {
					}
					try {
						endDate.add(obj.optString("endDate").trim());
					} catch (Exception e) {
					}
					
					
				}
				List<ServiceProduct> serviceProductList = null;
				if (comp != null) {
					if(intIdList.size()>0){
					serviceProductList = ofy().load()
							.type(ServiceProduct.class)
							.filter("companyId", comp.getCompanyId())
							.filter("count IN", intIdList).list();
					}else {
						serviceProductList = ofy().load()
								.type(ServiceProduct.class)
								.filter("companyId", comp.getCompanyId())
								.filter("productCode IN", productCodeList).list();
					}
						
				}
				if(serviceProductList==null)
					resp.getWriter().println("No Products found");
				else if(serviceProductList.size()!=productCodeList.size()&&productCodeList.size()>0)
				{
					String error="";
					for(String code:productCodeList) {
						boolean found=false;
						for(ServiceProduct prod:serviceProductList) {
							if(code.equals(prod.getProductCode())) {
								found=true;
							}
						}
						if(!found)
							error+="Product code "+code+" not found in system. ";
					}
					if(!error.equals("")) {
						resp.getWriter().println(error);
						return;
					}
				}
				
				List<SalesLineItem> listofSaleLineItem = new ArrayList<SalesLineItem>();
				for (int i = 0; i < serviceProductList.size(); i++) {
					SalesLineItem items = new SalesLineItem();
					items.setPrduct(serviceProductList.get(i));
					items.setProductSrNo(i + 1);
					logger.log(Level.SEVERE,
							"serviceProductList.get(i).getProductCode()--->>>"
									+ serviceProductList.get(i)
											.getProductCode());
					items.setProductCode(serviceProductList.get(i)
							.getProductCode());
					items.setProductName(serviceProductList.get(i)
							.getProductName().trim());
					items.setProductName(serviceProductList.get(i)
							.getProductName().trim());
					
					try {
						if(startDate.size()>0&&startDate.get(i)!=null&&!startDate.get(i).equals(""))
							items.setStartDate(sdf.parse(startDate.get(i)));
						else
							items.setStartDate(sdf.parse(quotationDate));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
					Calendar c = Calendar.getInstance();
					try {
						c.setTime(sdf.parse(quotationDate));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					c.add(Calendar.DATE, serviceProductList.get(i)
							.getDuration());
					Date date1 = c.getTime();
					
					try {
						if(endDate.size()>0&&endDate.get(i)!=null&&!endDate.get(i).equals(""))
							items.setEndDate(sdf.parse(endDate.get(i)));
						else
							items.setEndDate(date1);
					}catch(Exception e) {
						
					}
					
					/*
					 * Name: Apeksha Gunjal Date: 08/09/2018 @ 11:30 Note: Added
					 * duration while adding service product in Quotation.
					 */
					try {
						items.setDuration(duration.get(i));
					} catch (Exception e) {
						items.setDuration(serviceProductList.get(i)
								.getDuration());
					}
					items.setNumberOfService(intNoOfServicesList.get(i));
					items.setQuantity(1d);
					items.setArea("NA");
					items.setPrice(priceOfServicesList.get(i));
					items.setPremisesDetails("");
					items.setServicingTime(serviceProductList.get(i)
							.getServiceTime());
					items.setVatTax(serviceProductList.get(i).getVatTax());
					items.setServiceTax(serviceProductList.get(i)
							.getServiceTax());

					if(serviceProductList.get(i)
							.getServiceTax() != null){
					items.setServiceTaxEdit(serviceProductList.get(i)
							.getServiceTax().getTaxConfigName());
					}
					if(serviceProductList.get(i)
							.getVatTax() != null){
						items.setVatTaxEdit(serviceProductList.get(i)
							.getVatTax().getTaxConfigName());
					}
					
					
					/**
					 * Date: 08 Sept 2018 Name: Rahul Verma Description: Since
					 * we have changed datatype of scheduling Cust Branch logic.
					 * New fields have been added . We are passing hard core
					 * value as "Service Address"
					 */
					try {
						ArrayList<BranchWiseScheduling> customerBranchList = new ArrayList<BranchWiseScheduling>();

						BranchWiseScheduling cbObj1 = new BranchWiseScheduling();
						cbObj1.setBranchName("Service Address");
						cbObj1.setServicingBranch(branch);
						cbObj1.setCheck(true);
						customerBranchList.add(cbObj1);

						Integer prodSrNo = items.getProductSrNo();
						HashMap<Integer, ArrayList<BranchWiseScheduling>> hsmpcustomerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
						hsmpcustomerBranchlist
								.put(prodSrNo, customerBranchList);
						items.setCustomerBranchSchedulingInfo(hsmpcustomerBranchlist);
						/**
						 * Ends for Rahul Verma
						 */
					} catch (Exception e) {
						logger.log(Level.SEVERE, "" + e);
					}
					listofSaleLineItem.add(items);
				}
				quotation.setItems(listofSaleLineItem);
				/** Done Adding Product **/

				List<ServiceSchedule> serviceScheduleList = getServiceSceduleList(
						listofSaleLineItem,
						sdf.format(quotation.getQuotationDate()));
				quotation.setServiceScheduleList(serviceScheduleList);
				quotation.setCreatedBy(employeeName);
				quotation.setNetpayable(getNetPayable(quotation.getItems()));
				
				if(newQuotationFlag) {
				if (actionTask.trim().equalsIgnoreCase("Update")) {

				}else if(numberRange != null && !numberRange.equals("")) { 
						String prefixselectedNumberRange = "Q_"+ numberRange;
//						count=(int) numGen.getLastUpdatedNumber(prefixselectedNumberRange, con.getCompanyId(), (long) totalservices);
						NumberGeneration ng = new NumberGeneration();
						ng = ofy().load().type(NumberGeneration.class)
								.filter("companyId", comp.getCompanyId())
								.filter("processName", prefixselectedNumberRange)
								.filter("status", true).first().now();

						if(ng==null){
							resp.getWriter().println("Number generation "+prefixselectedNumberRange+" not found");
							return;
						}
						long number = ng.getNumber();
						int count = (int) number;
						quotation.setCount(count + 1);
						quotation.setQuotationNumberRange(numberRange);
						logger.log(Level.SEVERE, "Quotation Count::::-->>>>"
								+ (count + 1));
						ng.setNumber(count + 1);
						ofy().save().entity(ng);

				}else {

					NumberGeneration ng = new NumberGeneration();
					ng = ofy().load().type(NumberGeneration.class)
							.filter("companyId", comp.getCompanyId())
							.filter("processName", "Quotation")
							.filter("status", true).first().now();

					long number = ng.getNumber();
					int count = (int) number;
					quotation.setCount(count + 1);
					logger.log(Level.SEVERE, "Quotation Count::::-->>>>"
							+ (count + 1));
					ng.setNumber(count + 1);
					ofy().save().entity(ng);

				}
				}
				ofy().save().entity(quotation);
				
				
				
				/*
				 * Name: Apeksha Gunjal Date: 05/07/2018 @15:55 Note: Send
				 * Request for approval after creating Quotation automatically.
				 */
				boolean isApprovalSent = false;
				if(!apiCallfrom.equalsIgnoreCase("ZOHO")) {
					try {
						isApprovalSent = reactToRequestForApprovalForQuotation(
								quotation, employeeName, comp.getCompanyId());
					} catch (Exception e) {
						logger.log(
								Level.SEVERE,
								"Error sending approval: Quotation: "
										+ e.getMessage());
					}				
				}
				JSONObject jsonObject = new JSONObject();
				/*
				 * Name: Apeksha Gunjal Date: 05/07/2018 @18:27 Note: Status is
				 * changed to Requested after sending approval.
				 */
				try {
					if(apiCallfrom.equalsIgnoreCase("ZOHO"))
						jsonObject.put("status",
								ConcreteBusinessProcess.APPROVED);
					else if (isApprovalSent)
						jsonObject.put("status",
								ConcreteBusinessProcess.REQUESTED);
					else
						jsonObject.put("status",
								ConcreteBusinessProcess.CREATED);
				} catch (Exception e) {
					logger.log(Level.SEVERE, e.getMessage());
				}
				try {
					jsonObject.put("docId", quotation.getCount());
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					jsonObject
							.put("netPayable", quotation.getNetpayable() + "");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String responseStr = jsonObject.toString().replace("\\\\", "");
				resp.getWriter().println(responseStr + "");
				
				}else {
					resp.getWriter().println("Failed to create/load customer");
				}
				
			} else {

				if (actionTask.trim().equalsIgnoreCase("Update")) {
					salesQuotation = ofy()
							.load()
							.type(SalesQuotation.class)
							.filter("companyId", comp.getCompanyId())
							.filter("count",
									Long.parseLong(object.optString("count")))
							.first().now();
				}

				PersonInfo personInfo = new PersonInfo();
				personInfo.setCount(customer.getCount());
				/*
				 * Name: Apeksha Gunjal Date: 21/07/2018 @ 15:34 Note: If
				 * customer is a company then set full name as a company name
				 */
				try {
					if (customer.isCompany()) {
						personInfo.setFullName(customer.getCompanyName());
					} else
						personInfo.setFullName(customer.getFullname());
				} catch (Exception e) {
					personInfo.setFullName(customer.getFullname());
				}
				personInfo.setCellNumber(customer.getCellNumber1());
				personInfo.setPocName(customer.getFullname());
				salesQuotation.setCinfo(personInfo);
				// quotation.setCreatedBy();
				try {
					salesQuotation.setValidUntill(sdf.parse(validUntilDate));
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				/** From Android Application **/
				try {
					salesQuotation.setQuotationDate(sdf.parse(quotationDate));
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				/** from Android App **/
				salesQuotation.setCompanyId(companyId);
				salesQuotation.setStatus(Quotation.CREATED);
				salesQuotation.setBranch(branch);
				/** from Android App **/
				salesQuotation.setEmployee(employeeName);
				salesQuotation.setApproverName(approverName);
				salesQuotation.setType(type);
				salesQuotation.setCategory(category);
				// quotation.setApproverName("Rohan Bhagde");
				/** Payment Terms **/
				List<PaymentTerms> paymentTermsList = new ArrayList<PaymentTerms>();
				if (paymentterms.trim().equalsIgnoreCase("Monthly")) {
					int days = 0;
					for (int i = 0; i < 12; i++) {
						if (i + 1 == 12) {
							PaymentTerms payTerms = new PaymentTerms();
							payTerms.setPayTermDays(days);
							payTerms.setPayTermPercent(8.37);
							payTerms.setPayTermComment(i + 1 + " Payment");
							paymentTermsList.add(payTerms);
						} else {
							PaymentTerms payTerms = new PaymentTerms();
							payTerms.setPayTermDays(days);
							payTerms.setPayTermPercent(8.33);
							payTerms.setPayTermComment(i + 1 + " Payment");
							paymentTermsList.add(payTerms);
						}
						days = days + 30;
					}
				} else if (paymentterms.trim().equalsIgnoreCase("Quaterly")) {

					int days = 0;
					for (int i = 0; i < 4; i++) {
						PaymentTerms payTerms = new PaymentTerms();
						payTerms.setPayTermDays(days);
						payTerms.setPayTermPercent(25d);
						payTerms.setPayTermComment(i + 1 + " Payment");
						days = days + 90;
						paymentTermsList.add(payTerms);
					}

				} else if (paymentterms.trim().equalsIgnoreCase("Half Yearly")) {
					int days = 0;
					for (int i = 0; i < 2; i++) {
						PaymentTerms payTerms = new PaymentTerms();
						payTerms.setPayTermDays(days);
						payTerms.setPayTermPercent(50d);
						payTerms.setPayTermComment(i + 1 + " Payment");
						paymentTermsList.add(payTerms);
						days = days + 180;
					}
				} else if (paymentterms.trim().equalsIgnoreCase("Yearly")) {
					PaymentTerms payTerms = new PaymentTerms();
					payTerms.setPayTermDays(0);
					payTerms.setPayTermPercent(100d);
					payTerms.setPayTermComment("Full Payment");
					paymentTermsList.add(payTerms);
				} else {
					PaymentTerms payTerms = new PaymentTerms();
					payTerms.setPayTermDays(0);
					payTerms.setPayTermPercent(100d);
					payTerms.setPayTermComment("Default");
					paymentTermsList.add(payTerms);
				}
				salesQuotation.setPaymentTermsList(paymentTermsList);
				/** Done with Payment Terms **/

				/** Product **/
				JSONArray jsonArray = null;
				try {
					jsonArray = new JSONArray(req.getParameter("productArray"));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				List<Integer> intIdList = new ArrayList<Integer>();
				List<Integer> intNoOfServicesList = new ArrayList<Integer>();
				List<Double> priceOfServicesList = new ArrayList<Double>();

				for (int i = 0; i < productArray.length(); i++) {
					JSONObject obj = null;
					try {
						obj = productArray.getJSONObject(i);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					intIdList.add(Integer.parseInt(obj.optString("id").trim()));
					intNoOfServicesList.add(Integer.parseInt(obj.optString(
							"noOfServices").trim()));
					priceOfServicesList.add(Double.parseDouble(obj.optString(
							"price").trim()));
				}

				List<ItemProduct> itemProductList = null;
				if (comp != null) {
					itemProductList = ofy().load().type(ItemProduct.class)
							.filter("companyId", comp.getCompanyId())
							.filter("count IN", intIdList).list();
				}
				List<SalesLineItem> listofSaleLineItem = new ArrayList<SalesLineItem>();
				for (int i = 0; i < itemProductList.size(); i++) {
					SalesLineItem items = new SalesLineItem();
					items.setPrduct(itemProductList.get(i));
					items.setProductSrNo(i + 1);
					logger.log(Level.SEVERE,
							"serviceProductList.get(i).getProductCode()--->>>"
									+ itemProductList.get(i).getProductCode());
					items.setProductCode(itemProductList.get(i)
							.getProductCode());
					items.setProductName(itemProductList.get(i)
							.getProductName().trim());
					items.setProductName(itemProductList.get(i)
							.getProductName().trim());
					try {
						items.setStartDate(sdf.parse(quotationDate));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// Calendar c = Calendar.getInstance();
					// try {
					// c.setTime(sdf.parse(quotationDate));
					// } catch (Exception e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
					// }
					// c.add(Calendar.DATE, itemProductList.get(i).get());
					// Date date1 = c.getTime();
					// items.setEndDate(date1);
					// items.setDuration(itemProductList.get(i).getDuration());
					items.setCount(intNoOfServicesList.get(i));
					items.setQuantity(1d);
					items.setArea("NA");
					items.setPrice(priceOfServicesList.get(i));
					items.setPremisesDetails("");
					// items.setServicingTime(itemProductList.get(i).getServiceTime());
					items.setVatTax(itemProductList.get(i).getVatTax());
					items.setServiceTax(itemProductList.get(i).getServiceTax());
					listofSaleLineItem.add(items);
				}
				salesQuotation.setItems(listofSaleLineItem);
				/** Done Adding Product **/

				// List<ServiceSchedule>
				// serviceScheduleList=getServiceSceduleList(listofSaleLineItem,sdf.format(salesQuotation.getQuotationDate()));
				// salesQuotation.setServiceScheduleList(serviceScheduleList);
				salesQuotation.setCreatedBy(employeeName);
				salesQuotation.setNetpayable(getNetPayable(salesQuotation
						.getItems()));
				if (actionTask.trim().equalsIgnoreCase("Update")) {

				} else {

					NumberGeneration ng = new NumberGeneration();
					ng = ofy().load().type(NumberGeneration.class)
							.filter("companyId", comp.getCompanyId())
							.filter("processName", "SalesQuotation")
							.filter("status", true).first().now();

					long number = ng.getNumber();
					int count = (int) number;
					salesQuotation.setCount(count + 1);
					logger.log(Level.SEVERE, "Sales Quotation Count::::-->>>>"
							+ (count + 1));
					ng.setNumber(count + 1);
					ofy().save().entity(ng);

				}

				ofy().save().entity(salesQuotation);
				/*
				 * Name: Apeksha Gunjal Date: 05/07/2018 @15:55 Note: Send
				 * Request for approval after creating SalesQuotation
				 * automatically.
				 */
				boolean isApprovalSent = false;
				try {
					isApprovalSent = reactToRequestForApprovalForSalesQuotation(
							salesQuotation, employeeName, comp.getCompanyId());
				} catch (Exception e) {
					logger.log(
							Level.SEVERE,
							"Error sending approval: sales Quotation: "
									+ e.getMessage());
				}
				JSONObject jsonObject = new JSONObject();
				/*
				 * Name: Apeksha Gunjal Date: 05/07/2018 @18:27 Note: Status is
				 * changed to Requested after sending approval.
				 */
				try {
					if (isApprovalSent)
						jsonObject.put("status",
								ConcreteBusinessProcess.REQUESTED);
					else
						jsonObject.put("status",
								ConcreteBusinessProcess.CREATED);
				} catch (Exception e) {
					logger.log(Level.SEVERE, e.getMessage());
				}
				try {
					jsonObject.put("docId", salesQuotation.getCount());
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					jsonObject.put("netPayable", salesQuotation.getNetpayable()
							+ "");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String responseStr = jsonObject.toString().replace("\\\\", "");
				resp.getWriter().println(responseStr + "");
			}
			

		} else if (screenName.trim().equalsIgnoreCase(
				AppConstants.ANDROIDSCREENLIST.CONTRACT)) {
			JSONArray productArray = null;
			try {
				productArray = object.getJSONArray("productArray");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// Added by Apeksha Gunjal on 21/07/2018 16:50
			logger.log(Level.SEVERE, "Contract: productArray1--->"
					+ productArray);

			// Added by Apeksha Gunjal on 21/07/2018 16:50
			try {
				productArray = new JSONArray(object.optString("productArray"));
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			logger.log(Level.SEVERE, "Contract: productArray2--->"
					+ productArray);

			String contractDate = object.optString("date").trim();
			String status = object.optString("status").trim();
			String category = object.optString("category").trim();
			String type = object.optString("type").trim();
			String customerId = object.optString("customerId").trim();
			String customerCell = object.optString("customerCell").trim();
			String customerName = object.optString("customerName").trim();
			String customerPOCName = object.optString("customerPOCName").trim();
			String branch = object.optString("branch").trim();
			String paymentTerms = object.optString("paymentTerms").trim();
			String employeeName = object.optString("employeeName").trim();
			String approverName = object.optString("approverName").trim();
			String actionTask = object.optString("actionTask").trim();

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));

			Customer customer = null;
			if (comp != null) {
				customer = ofy().load().type(Customer.class)
						.filter("companyId", comp.getCompanyId())
						.filter("count", Integer.parseInt(customerId)).first()
						.now();
			}
			Contract contract = new Contract();
			if (actionTask.trim().equalsIgnoreCase("Update")) {
				contract = ofy()
						.load()
						.type(Contract.class)
						.filter("companyId", comp.getCompanyId())
						.filter("count",
								Integer.parseInt(object.optString("count")
										.trim())).first().now();
			}
			PersonInfo personInfo = new PersonInfo();
			personInfo.setCount(customer.getCount());
			/*
			 * Name: Apeksha Gunjal Date: 21/07/2018 @ 15:34 Note: If customer
			 * is a company then set full name as a company name
			 */
			try {
				if (customer.isCompany()) {
					personInfo.setFullName(customer.getCompanyName());
				} else
					personInfo.setFullName(customer.getFullname());
			} catch (Exception e) {
				personInfo.setFullName(customer.getFullname());
			}
			personInfo.setCellNumber(customer.getCellNumber1());

			contract.setCompanyId(comp.getCompanyId());
			contract.setCinfo(personInfo);
			contract.setBranch(branch);
			contract.setStatus(Contract.CREATED);
			contract.setEmployee(employeeName);
			contract.setCreatedBy(employeeName);

			// Added by Apeksha Gunjal on 21/07/2018 16:50
			logger.log(Level.SEVERE, "Contract: contractDate--->"
					+ contractDate);
			try {
				contract.setStartDate(sdf.parse(contractDate));
			} catch (ParseException e2) {
				// TODO Auto-generated catch block
				// Added by Apeksha Gunjal on 21/07/2018 16:50
				logger.log(Level.SEVERE, "Error Contract: contractDate--->"
						+ contractDate);
				e2.printStackTrace();
			}
			try {
				contract.setContractDate(sdf.parse(contractDate));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/** Android Percentage */
			List<PaymentTerms> paymentTermsList = new ArrayList<PaymentTerms>();
			if (paymentTerms.trim().equalsIgnoreCase("Monthly")) {
				int days = 0;
				for (int i = 0; i < 12; i++) {
					if (i + 1 == 12) {
						PaymentTerms payTerms = new PaymentTerms();
						payTerms.setPayTermDays(days);
						payTerms.setPayTermPercent(8.37);
						payTerms.setPayTermComment(i + 1 + " Payment");
						paymentTermsList.add(payTerms);
					} else {
						PaymentTerms payTerms = new PaymentTerms();
						payTerms.setPayTermDays(days);
						payTerms.setPayTermPercent(8.33);
						payTerms.setPayTermComment(i + 1 + " Payment");
						paymentTermsList.add(payTerms);
					}
					days = days + 30;
				}
			} else if (paymentTerms.trim().equalsIgnoreCase("Quaterly")) {

				int days = 0;
				for (int i = 0; i < 4; i++) {
					PaymentTerms payTerms = new PaymentTerms();
					payTerms.setPayTermDays(days);
					payTerms.setPayTermPercent(25d);
					payTerms.setPayTermComment(i + 1 + " Payment");
					paymentTermsList.add(payTerms);
					days = days + 90;
				}

			} else if (paymentTerms.trim().equalsIgnoreCase("Half Yearly")) {
				int days = 0;
				for (int i = 0; i < 2; i++) {
					PaymentTerms payTerms = new PaymentTerms();
					payTerms.setPayTermDays(days);
					payTerms.setPayTermPercent(50d);
					payTerms.setPayTermComment(i + 1 + " Payment");
					paymentTermsList.add(payTerms);
					days = days + 180;
				}
			} else if (paymentTerms.trim().equalsIgnoreCase("Yearly")) {
				PaymentTerms payTerms = new PaymentTerms();
				payTerms.setPayTermDays(0);
				payTerms.setPayTermPercent(100d);
				payTerms.setPayTermComment("Full Payment");
				paymentTermsList.add(payTerms);
			} else {
				PaymentTerms payTerms = new PaymentTerms();
				payTerms.setPayTermDays(0);
				payTerms.setPayTermPercent(100d);
				payTerms.setPayTermComment("Default");
				paymentTermsList.add(payTerms);
			}

			contract.setPaymentTermsList(paymentTermsList);
			contract.setQuotationCount(-1);
			contract.setTicketNumber(-1);
			contract.setLeadCount(-1);
			contract.setApproverName(approverName);
			/** Android Percentage Done **/

			/** Adding Product **/
			// JSONArray jsonArray = null;
			// try {
			// jsonArray = new JSONArray(req.getParameter("productArray"));
			// } catch (Exception e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			List<Integer> intIdList = new ArrayList<Integer>();
			List<Integer> intNoOfServicesList = new ArrayList<Integer>();
			List<Double> priceOfServicesList = new ArrayList<Double>();
			/*
			 * Name: Apeksha Gunjal Date: 08/09/2018 @ 11:30 Note: Added
			 * duration while adding service product in Contract.
			 */
			List<Integer> duration = new ArrayList<Integer>();
			for (int i = 0; i < productArray.length(); i++) {
				JSONObject obj = null;
				try {
					obj = productArray.getJSONObject(i);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				intIdList.add(Integer.parseInt(obj.optString("id").trim()));
				intNoOfServicesList.add(Integer.parseInt(obj.optString(
						"noOfServices").trim()));
				priceOfServicesList.add(Double.parseDouble(obj.optString(
						"price").trim()));
				/*
				 * Name: Apeksha Gunjal Date: 08/09/2018 @ 11:30 Note: Added
				 * duration while adding service product in Contract.
				 */
				try {
					duration.add(Integer.parseInt(obj.optString("duration")
							.trim()));
				} catch (Exception e) {
					logger.log(Level.SEVERE, "error: " + e);
				}
			}
			/** Group/Category/Type **/
			contract.setCategory(category);
			contract.setGroup("");
			contract.setType(type);

			/** Done Adding Group/Category/Type **/
			// Added by Apeksha Gunjal on 21/07/2018 16:50
			List<ServiceProduct> serviceProductList = new ArrayList<>();
			if (comp != null) {
				serviceProductList = ofy().load().type(ServiceProduct.class)
						.filter("companyId", comp.getCompanyId())
						.filter("count IN", intIdList).list();
			}

			// Added by Apeksha Gunjal on 21/07/2018 16:50
			logger.log(Level.SEVERE, "serviceProductList--->>>"
					+ serviceProductList.size());

			List<SalesLineItem> listofSaleLineItem = new ArrayList<SalesLineItem>();
			for (int i = 0; i < serviceProductList.size(); i++) {
				SalesLineItem items = new SalesLineItem();
				items.setPrduct(serviceProductList.get(i));
				items.setProductSrNo(i + 1);
				logger.log(Level.SEVERE,
						"serviceProductList.get(i).getProductCode()--->>>"
								+ serviceProductList.get(i).getProductCode());
				items.setProductCode(serviceProductList.get(i).getProductCode());
				items.setProductName(serviceProductList.get(i).getProductName()
						.trim());
				try {
					items.setStartDate(sdf.parse(contractDate));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Calendar c = Calendar.getInstance();
				try {
					c.setTime(sdf.parse(contractDate));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				c.add(Calendar.DATE, serviceProductList.get(i).getDuration());
				Date date1 = c.getTime();
				items.setEndDate(date1);
				/*
				 * Name: Apeksha Gunjal Date: 08/09/2018 @ 11:30 Note: Added
				 * duration while adding service product in Lead.
				 */
				try {
					items.setDuration(duration.get(i));
				} catch (Exception e) {
					items.setDuration(serviceProductList.get(i).getDuration());
				}
				// items.setDuration(serviceProductList.get(i).getDuration());
				items.setNumberOfService(intNoOfServicesList.get(i));
				items.setQuantity(1d);
				items.setArea("NA");
				items.setPrice(priceOfServicesList.get(i));
				items.setPremisesDetails("");
				items.setServicingTime(serviceProductList.get(i)
						.getServiceTime());
				items.setVatTax(serviceProductList.get(i).getVatTax());
				items.setServiceTax(serviceProductList.get(i).getServiceTax());
				if (contractPeriod < serviceProductList.get(i).getDuration()) {
					contractPeriod = serviceProductList.get(i).getDuration();
				}
				/**
				 * Date: 08 Sept 2018 Name: Rahul Verma Description: Since we
				 * have changed datatype of scheduling Cust Branch logic. New
				 * fields have been added . We are passing hard core value as
				 * "Service Address"
				 */
				try {
					ArrayList<BranchWiseScheduling> customerBranchList = new ArrayList<BranchWiseScheduling>();

					BranchWiseScheduling cbObj1 = new BranchWiseScheduling();
					cbObj1.setBranchName("Service Address");
					cbObj1.setServicingBranch(branch);
					cbObj1.setCheck(true);
					customerBranchList.add(cbObj1);

					Integer prodSrNo = items.getProductSrNo();
					HashMap<Integer, ArrayList<BranchWiseScheduling>> hsmpcustomerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
					hsmpcustomerBranchlist.put(prodSrNo, customerBranchList);
					items.setCustomerBranchSchedulingInfo(hsmpcustomerBranchlist);
				} catch (Exception e) {
					logger.log(Level.SEVERE, "" + e);
				}
				/**
				 * Ends for Rahul Verma
				 */
				listofSaleLineItem.add(items);
			}
			logger.log(Level.SEVERE, "listofSaleLineItem--->>>"
					+ listofSaleLineItem.size());
			contract.setItems(listofSaleLineItem);

			/** Done Adding Product **/

			List<ServiceSchedule> serviceScheduleList = getServiceSceduleList(
					listofSaleLineItem, sdf.format(contract.getContractDate()));
			contract.setServiceScheduleList(serviceScheduleList);

			// Added by Apeksha Gunjal on 20/06/2018 10:15 AM
			try {
				contract.setNetpayable(getNetPayable(contract.getItems()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			// SimpleDateFormat sdfContractDate = new SimpleDateFormat(
			// "dd-MM-yyyy");
			// sdfContractDate
			// .setTimeZone(TimeZone.getTimeZone("IST"));
			// TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			Calendar c = Calendar.getInstance();
			c.setTime(contract.getStartDate());
			try {
				c.add(Calendar.DATE, contractPeriod);
				Date date1 = c.getTime();
				contract.setEndDate(sdf.parse(sdf.format(date1)));
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (actionTask.trim().equalsIgnoreCase("Update")) {

			} else {
				NumberGeneration ng = new NumberGeneration();
				ng = ofy().load().type(NumberGeneration.class)
						.filter("companyId", comp.getCompanyId())
						.filter("processName", "Contract")
						.filter("status", true).first().now();

				long number = ng.getNumber();
				int count = (int) number;
				contract.setCount(count + 1);
				logger.log(Level.SEVERE, "Contract Count::::-->>>>"
						+ (count + 1));
				ng.setNumber(count + 1);
				ofy().save().entity(ng);

			}
			ofy().save().entity(contract);
			/*
			 * Name: Apeksha Gunjal Date: 05/07/2018 @15:55 Note: Send Request
			 * for approval after creating Contract automatically.
			 */
			boolean isApprovalSent = false;
			try {
				isApprovalSent = reactToRequestForApprovalForContract(contract,
						employeeName, comp.getCompanyId());
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Error sending approval: contract: "
						+ e.getMessage());
			}
			JSONObject jsonObject = new JSONObject();
			/*
			 * Name: Apeksha Gunjal Date: 05/07/2018 @18:27 Note: Status is
			 * changed to Requested after sending approval.
			 */
			try {
				if (isApprovalSent)
					jsonObject.put("status", ConcreteBusinessProcess.REQUESTED);
				else
					jsonObject.put("status", ConcreteBusinessProcess.CREATED);
			} catch (Exception e) {
				logger.log(Level.SEVERE, e.getMessage());
			}
			try {
				jsonObject.put("docId", contract.getCount());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				jsonObject.put("netPayable", contract.getNetpayable() + "");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String responseStr = jsonObject.toString().replace("\\\\", "");
			resp.getWriter().println(responseStr + "");
		} else if (screenName.trim().equalsIgnoreCase(
				AppConstants.ANDROIDSCREENLIST.INVOICE)) {

		} else if (screenName.trim().equalsIgnoreCase(
				AppConstants.ANDROIDSCREENLIST.PAYMENT)) {

		} /*
		 * else if (screenName.trim().equalsIgnoreCase(
		 * AppConstants.ANDROIDSCREENLIST.CUSTOMER_SERVICE)) {
		 * 
		 * }
		 */else if (screenName.trim().equalsIgnoreCase(
				AppConstants.ANDROIDSCREENLIST.SALES_ORDER)) {
			JSONArray productArray = null;
			try {
				productArray = object.getJSONArray("productArray");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));

			String deliveryDate = object.optString("deliveryDate").trim();
			String salesOrderDate = object.optString("date").trim();
			String status = object.optString("status").trim();
			String category = object.optString("category").trim();
			String type = object.optString("type").trim();
			String customerId = object.optString("customerId").trim();
			String customerCell = object.optString("customerCell").trim();
			String customerName = object.optString("customerName").trim();
			String customerPOCName = object.optString("customerPOCName").trim();
			String branch = object.optString("branch").trim();
			String paymentTerms = object.optString("paymentTerms").trim();
			String employeeName = object.optString("employeeName").trim();
			String approverName = object.optString("approverName").trim();
			String actionTask = object.optString("actionTask").trim();
			SalesOrder salesOrder = new SalesOrder();

			if (actionTask.trim().equalsIgnoreCase("Update")) {
				salesOrder = ofy().load().type(SalesOrder.class)
						.filter("companyId", comp.getCompanyId())
						.filter("count", Integer.parseInt(customerId)).first()
						.now();
			}
			Customer customer = null;
			if (comp != null) {
				customer = ofy().load().type(Customer.class)
						.filter("companyId", comp.getCompanyId())
						.filter("count", Integer.parseInt(customerId)).first()
						.now();
			}
			try {
				salesOrder.setSalesOrderDate(sdf.parse(salesOrderDate));
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			salesOrder.setCompanyId(companyId);
			salesOrder.setBranch(branch);
			salesOrder.setEmployee(employeeName);
			salesOrder.setApproverName(approverName);
			salesOrder.setCategory(category);
			salesOrder.setType(type);
			PersonInfo personInfo = new PersonInfo();
			personInfo.setCount(customer.getCount());
			personInfo.setFullName(customerName);
			personInfo.setCellNumber(Long.parseLong(customerCell));
			personInfo.setPocName(customerPOCName);

			salesOrder.setCinfo(personInfo);
			salesOrder.setCategory(category);
			salesOrder.setType(type);
			/** Android **/
			List<PaymentTerms> paymentTermsList = new ArrayList<PaymentTerms>();
			if (paymentTerms.trim().equalsIgnoreCase("Monthly")) {
				int days = 0;
				for (int i = 0; i < 12; i++) {
					if (i + 1 == 12) {
						PaymentTerms payTerms = new PaymentTerms();
						payTerms.setPayTermDays(days);
						payTerms.setPayTermPercent(8.37);
						payTerms.setPayTermComment(i + 1 + " Payment");
						paymentTermsList.add(payTerms);
					} else {
						PaymentTerms payTerms = new PaymentTerms();
						payTerms.setPayTermDays(days);
						payTerms.setPayTermPercent(8.33);
						payTerms.setPayTermComment(i + 1 + " Payment");
						paymentTermsList.add(payTerms);
					}
					days = days + 30;
				}
			} else if (paymentTerms.trim().equalsIgnoreCase("Quaterly")) {

				int days = 0;
				for (int i = 0; i < 4; i++) {
					PaymentTerms payTerms = new PaymentTerms();
					payTerms.setPayTermDays(days);
					payTerms.setPayTermPercent(25d);
					payTerms.setPayTermComment(i + 1 + " Payment");
					paymentTermsList.add(payTerms);
					days = days + 90;
				}

			} else if (paymentTerms.trim().equalsIgnoreCase("Half Yearly")) {
				int days = 0;
				for (int i = 0; i < 2; i++) {
					PaymentTerms payTerms = new PaymentTerms();
					payTerms.setPayTermDays(days);
					payTerms.setPayTermPercent(50d);
					payTerms.setPayTermComment(i + 1 + " Payment");
					paymentTermsList.add(payTerms);
					days = days + 180;
				}
			} else if (paymentTerms.trim().equalsIgnoreCase("Yearly")) {
				PaymentTerms payTerms = new PaymentTerms();
				payTerms.setPayTermDays(0);
				payTerms.setPayTermPercent(100d);
				payTerms.setPayTermComment("Full Payment");
				paymentTermsList.add(payTerms);
			} else {
				PaymentTerms payTerms = new PaymentTerms();
				payTerms.setPayTermDays(0);
				payTerms.setPayTermPercent(100d);
				payTerms.setPayTermComment("Default");
				paymentTermsList.add(payTerms);
			}
			salesOrder.setPaymentTermsList(paymentTermsList);
			try {
				salesOrder.setDeliveryDate(sdf.parse(deliveryDate));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			salesOrder.setShippingAddress(customer.getSecondaryAdress());
			List<Integer> intIdList = new ArrayList<Integer>();
			List<Double> doubleQuantityList = new ArrayList<Double>();
			List<Double> priceOfServicesList = new ArrayList<Double>();

			for (int i = 0; i < productArray.length(); i++) {
				JSONObject obj = null;
				try {
					obj = productArray.getJSONObject(i);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				intIdList.add(Integer.parseInt(obj.optString("id").trim()));
				doubleQuantityList.add(Double.parseDouble(obj.optString(
						"quantity").trim()));
				priceOfServicesList.add(Double.parseDouble(obj.optString(
						"price").trim()));
			}
			List<ItemProduct> itemProductList = null;
			if (comp != null) {
				itemProductList = ofy().load().type(ItemProduct.class)
						.filter("companyId", comp.getCompanyId())
						.filter("count IN", intIdList).list();
			}
			List<SalesLineItem> listofSaleLineItem = new ArrayList<SalesLineItem>();
			for (int i = 0; i < itemProductList.size(); i++) {

				SalesLineItem items = new SalesLineItem();
				items.setPrduct(itemProductList.get(i));
				items.setProductSrNo(i + 1);
				logger.log(Level.SEVERE,
						"serviceProductList.get(i).getProductCode()--->>>"
								+ itemProductList.get(i).getProductCode());
				items.setProductCode(itemProductList.get(i).getProductCode());
				items.setProductName(itemProductList.get(i).getProductName()
						.trim());
				// try {
				// items.setStartDate(sdf.parse(salesOrderDate));
				// } catch (ParseException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
				// Calendar c = Calendar.getInstance();
				// try {
				// c.setTime(sdf.parse(salesOrderDate));
				// } catch (ParseException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
				// c.add(Calendar.DATE, itemProductList.get(i).getDuration());
				// Date date1 = c.getTime();
				// items.setEndDate(date1);
				// items.setDuration(itemProductList.get(i).getDuration());
				items.setQuantity(doubleQuantityList.get(i));
				// items.setNumberOfService(d.get(i));
				// items.setQuantity(1d);
				items.setUnitOfMeasurement(itemProductList.get(i)
						.getUnitOfMeasurement());
				items.setArea("NA");
				items.setPrice(priceOfServicesList.get(i));
				items.setPremisesDetails("");
				// items.setServicingTime(itemProductList.get(i).getServiceTime());
				items.setVatTax(itemProductList.get(i).getVatTax());
				items.setServiceTax(itemProductList.get(i).getServiceTax());
				listofSaleLineItem.add(items);

			}
			salesOrder.setItems(listofSaleLineItem);
			try {
				salesOrder.setDeliveryDate(sdf.parse(deliveryDate));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			salesOrder.setShippingAddress(customer.getSecondaryAdress());
			if (actionTask.trim().equalsIgnoreCase("Update")) {

			} else {

				NumberGeneration ng = new NumberGeneration();
				ng = ofy().load().type(NumberGeneration.class)
						.filter("companyId", comp.getCompanyId())
						.filter("processName", "SalesOrder")
						.filter("status", true).first().now();

				long number = ng.getNumber();
				int count = (int) number;
				salesOrder.setCount(count + 1);
				logger.log(Level.SEVERE, "SalesOrder Count::::-->>>>"
						+ (count + 1));
				ng.setNumber(count + 1);
				ofy().save().entity(ng);

			}
			ofy().save().entity(salesOrder);
			/*
			 * Name: Apeksha Gunjal Date: 05/07/2018 @15:55 Note: Send Request
			 * for approval after creating Sales Order automatically.
			 */
			boolean isApprovalSent = false;
			try {
				isApprovalSent = reactToRequestForApprovalForSalesOrder(
						salesOrder, employeeName, comp.getCompanyId());
			} catch (Exception e) {
				logger.log(
						Level.SEVERE,
						"Error sending approval: sales Order: "
								+ e.getMessage());
			}
			JSONObject jsonObject = new JSONObject();
			/*
			 * Name: Apeksha Gunjal Date: 05/07/2018 @18:27 Note: Status is
			 * changed to Requested after sending approval.
			 */
			try {
				if (isApprovalSent)
					jsonObject.put("status", ConcreteBusinessProcess.REQUESTED);
				else
					jsonObject.put("status", ConcreteBusinessProcess.CREATED);
			} catch (Exception e) {
				logger.log(Level.SEVERE, e.getMessage());
			}
			try {
				jsonObject.put("docId", salesOrder.getCount());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				jsonObject.put("netPayable", salesOrder.getNetpayable() + "");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String responseStr = jsonObject.toString().replace("\\\\", "");
			resp.getWriter().println(responseStr + "");

		} else if (screenName.trim().equalsIgnoreCase("createCustomer")) {

			String response = createNewCustomer(comp, object);

			resp.getWriter().println(response + "");

		} else if (screenName.trim().equalsIgnoreCase(
				AppConstants.ANDROIDSCREENLIST.COMPLAIN_DASHBOARD)) {
			Complain complain = new Complain();
			// complain.set

		} else if (screenName.trim().equalsIgnoreCase("createAssesmentReport")) {
			AssesmentReport assesmentReport = new AssesmentReport();

			PersonInfo personInfo = new PersonInfo();
			personInfo
					.setCount(Integer.parseInt(object.optString("customerId")));
			personInfo.setFullName(object.optString("customerName"));
			personInfo.setCellNumber(Long.parseLong(object
					.optString("customerCell")));
			personInfo.setPocName(object.optString("customerPOC"));
			assesmentReport.setCinfo(personInfo);
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));

			try {
				assesmentReport.setAssessmentDate(sdf.parse(sdf
						.format(new Date())));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			ArrayList<AssesmentReportEmbedForTable> arrItems = new ArrayList<AssesmentReportEmbedForTable>();
			JSONArray jsonArray = null;
			try {
				jsonArray = object.getJSONArray("assesmentReportTable");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jObj = null;
				try {
					jObj = jsonArray.getJSONObject(i);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				AssesmentReportEmbedForTable report = new AssesmentReportEmbedForTable();

				report.setArea(jObj.optString("area"));

				report.setLocation(jObj.optString("location"));
				report.setConsequences(jObj.optString("consequences"));

				report.setCategory(jObj.optString("category"));

				report.setDeficiencyType(jObj.optString("deficiency"));

				report.setActionPlanForCompany(jObj
						.optString("actionPlanForCompany"));

				report.setActionPlanForCustomer(jObj
						.optString("actionPlanForCustomer"));

				arrItems.add(report);
			}
			assesmentReport.setAssessmentDetailsLIst(arrItems);

			assesmentReport.setAssessedPeron1(object
					.optString("assessedPerson1"));
			assesmentReport.setAssessedPeron2(object
					.optString("assessedPerson2"));
			assesmentReport.setAssessedPeron3(object
					.optString("assessedPerson3"));
			assesmentReport.setAccompainedByPerson1(object
					.optString("accompainedPerson1"));
			assesmentReport.setAccompainedByPerson1(object
					.optString("accompainedPerson2"));
			assesmentReport.setAccompainedByPerson1(object
					.optString("accompainedPerson3"));

			assesmentReport.setCompanyId(companyId);
			NumberGeneration ng = new NumberGeneration();
			ng = ofy().load().type(NumberGeneration.class)
					.filter("companyId", comp.getCompanyId())
					.filter("processName", "AssesmentReport")
					.filter("status", true).first().now();

			long number = ng.getNumber();
			int count = (int) number;
			assesmentReport.setCount(count + 1);
			ng.setNumber(count + 1);
			ofy().save().entity(ng);

			ofy().save().entities(assesmentReport);

			resp.getWriter().println((count + 1) + "");
			logger.log(Level.SEVERE, "count of customer:::::::" + (count + 1));
		} else if (screenName.trim().equalsIgnoreCase("createComplain")) {
			Complain complain = new Complain();
			complain.setCompanyId(companyId);
			 Customer customer = null;
			if(apiCallfrom==null || apiCallfrom.equals("")){
				PersonInfo personInfo = new PersonInfo();
				personInfo
						.setCount(Integer.parseInt(object.optString("customerId")));
				personInfo.setFullName(object.optString("customerName"));
				personInfo.setCellNumber(Long.parseLong(object
						.optString("customerCell")));
				personInfo.setPocName(object.optString("customerPOC"));
				complain.setPersoninfo(personInfo);

			}
			else {
				 int customerId = Integer.parseInt(object.optString("customerId"));
				 customer = ofy().load().type(Customer.class).filter("companyId", comp.getCompanyId())
						 	.filter("count", customerId).first().now();
				 if(customer!=null) {
					 PersonInfo personInfo = new PersonInfo();
						personInfo.setCount(customer.getCount());
						if(customer.isCompany()) {
							personInfo.setFullName(customer.getCompanyName());
						}
						else {
							personInfo.setFullName(customer.getFullname());
						}
						personInfo.setPocName(customer.getFullname());
						personInfo.setCellNumber(customer.getCellNumber1());
						complain.setPersoninfo(personInfo);
				 }
			}
			if(object.optString("description")!=null && !object.optString("description").equals(""))
			complain.setDestription(object.optString("description"));
			if(object.optString("personResponsible")!=null && !object.optString("personResponsible").equals(""))
			complain.setSalesPerson(object.optString("personResponsible"));
			if(object.optString("branch")!=null && !object.optString("branch").equals(""))
			complain.setBranch(object.optString("branch"));
			if(object.optString("assignTo")!=null && !object.optString("assignTo").equals(""))
			complain.setAssignto(object.optString("assignTo"));
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			try {
				if(object.optString("dueDate")!=null && !object.optString("dueDate").equals(""))
				complain.setDueDate(sdf.parse(object.optString("dueDate")));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if(object.optString("CustomerBranch")!=null && !object.optString("CustomerBranch").equals(""))
				complain.setCustomerBranch(object.optString("CustomerBranch"));
			} catch (Exception e) {
//				e.printStackTrace();
			}
			
			try {
				complain.setDate(sdf.parse(sdf.format(new Date())));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(object.optString("status")!=null && !object.optString("status").equals(""))
			complain.setCompStatus(object.optString("status"));
			if(object.optString("callerName")!=null && !object.optString("callerName").equals(""))
			complain.setCallerName(object.optString("callerName"));
			if(object.optString("callerNo")!=null && !object.optString("callerNo").equals(""))
			complain.setCallerCellNo(Long.parseLong(object.optString("callerNo")));
			if(object.optString("callerEmail")!=null && !object.optString("callerEmail").equals(""))
				complain.setEmail(object.optString("callerEmail"));
			if(object.optString("category")!=null && !object.optString("category").equals(""))
				complain.setCategory(object.optString("category"));
			
			ServiceProduct serviceproduct=null;
			if(object.optString("productId")!=null && !object.optString("productId").equals("")){
				try {
					String strproductId = object.optString("productId");
					int productId = Integer.parseInt(strproductId);
					serviceproduct = ofy().load().type(ServiceProduct.class).filter("companyId", companyId)
										.filter("count", productId).first().now();
				} catch (Exception e) {
				}
				
			}
			if(object.optString("productName")!=null && !object.optString("productName").equals("")){
				if(serviceproduct==null){
					serviceproduct = ofy().load().type(ServiceProduct.class).filter("companyId", companyId)
							.filter("productName", object.optString("productName")).first().now();
				}
			}
			if(serviceproduct!=null){
				ProductInfo productInfo=new ProductInfo();
				productInfo.setProdID(serviceproduct.getCount());
				productInfo.setProductCode(serviceproduct.getProductCode());
				productInfo.setProductName(serviceproduct.getProductName());
				if(serviceproduct.getProductCategory()!=null)
				productInfo.setProductCategory(serviceproduct.getProductCategory());
				complain.setPic(productInfo);
			}
			if(object.optString("customerBranch")!=null && !object.optString("customerBranch").equals("")){
				complain.setCustomerBranch(object.optString("customerBranch"));
			}
			if(complain.getBranch()==null || complain.getBranch().equals("")){
				if(complain.getPersoninfo()!=null && complain.getPersoninfo().getCount()!=0){
					if(customer==null) {
						customer = ofy().load().type(Customer.class).filter("count", complain.getPersoninfo().getCount())
								.filter("companyId", complain.getCompanyId()).first().now();
					}
					if(customer!=null){
						complain.setBranch(customer.getBranch());
					}
				}
			}
			
			if(object.optString("uploadDocument")!=null) {
				logger.log(Level.SEVERE, "uploadDocument "+object.optString("uploadDocument"));
				logger.log(Level.SEVERE, "uploadDocument "+object.opt("uploadDocument"));
//				complain.setDocument(object.optString("uploadDocument"));
			}
//			if(apiCallfrom!=null && !apiCallfrom.equals(AppConstants.CUSTOMERPORTAL)){
				complain.setCompStatus(Service.CREATED);
				complain.setComplainDate(new Date());
//			}
				
			int contracId = 0;
			try {
				if(object.optString("contractId")!=null && !object.optString("contractId").equals("")) {
					contracId = Integer.parseInt(object.optString("contractId"));
					if(contracId>0) {
						complain.setExistingContractId(contracId);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
				
			/**
			 * @author Vijay Date :- 07-03-2022 
			 * Des :- added serviec id for Customer Portal
			 */
			Service serviceentity =null;
			try {
				if(object.optString("serviceId")!=null && !object.optString("serviceId").equals("")) {
					int serviceId = Integer.parseInt(object.optString("serviceId"));
					
					if(serviceId>0) {
						
						if(contracId!=0){
							 serviceentity = ofy().load().type(Service.class).filter("companyId", companyId)
									.filter("count", serviceId).filter("contractCount", contracId).first().now();
						}
						else{
							 serviceentity = ofy().load().type(Service.class).filter("companyId", companyId)
									.filter("count", serviceId).first().now();
						}
						if(serviceentity!=null){
							String serviceDetails=serviceentity.getProductName()+"/"+sdf.format(serviceentity.getServiceDate())+"/"+serviceentity.getCount();
							complain.setServiceDetails(serviceDetails);
							if(complain.getSalesPerson()==null || complain.getSalesPerson().equals("") ) {
								complain.setSalesPerson(serviceentity.getEmployee()); 
								logger.log(Level.SEVERE, "serviceentity.getEmployee()"+serviceentity.getEmployee());
								complain.setServiceDate(serviceentity.getServiceDate());//19-06-2023
								complain.setNewServiceDate(new Date());//19-06-2023
							}
						}

					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "complain.getSalesPerson()"+complain.getSalesPerson());
			logger.log(Level.SEVERE, "customer.getEmployee()"+customer.getEmployee());

			if(complain.getSalesPerson()==null || complain.getSalesPerson().equals("") ) {
				if(customer!=null) {
					complain.setSalesPerson(customer.getEmployee()); 
					logger.log(Level.SEVERE, "customer.getEmployee()");
				}
			}
			
			
			/**
			 * @author Vijay Date :- 06-12-2022
			 * Des :- updated code to call standard save method to execute SMS/Whats app send msg when complain is created 
			 */
			GenricServiceImpl impl = new GenricServiceImpl();
			ReturnFromServer server = impl.save(complain);
			complain.setTicketId(server.count);//19-06-2023
			impl.save(complain);
			logger.log(Level.SEVERE, "ticket id set to "+complain.getTicketId());
//			NumberGeneration ng = new NumberGeneration();
//			ng = ofy().load().type(NumberGeneration.class)
//					.filter("companyId", comp.getCompanyId())
//					.filter("processName", "Complain").filter("status", true)
//					.first().now();
//
//			
//			long number = ng.getNumber();
//			int count = (int) number;
//			complain.setCount(count + 1);
//			ng.setNumber(count + 1);
//			ofy().save().entity(ng);
//
//			ofy().save().entities(complain);			

			if(apiCallfrom!=null && !apiCallfrom.equals(AppConstants.CUSTOMERPORTAL)){
				resp.getWriter().println(apputility.getMessageInJson(server.count+""));				
			}
			else {
				resp.getWriter().println(server.count);
			}
			
			
			logger.log(Level.SEVERE, "count of complain:::::::" +server.count);

		} else if (screenName.trim().equalsIgnoreCase(
				AppConstants.ANDROIDSCREENLIST.CUSTOMER_SERVICE)) {
			logger.log(Level.SEVERE, "Inside Schedule Customer Service");
			try {
				Service service = new Service();

				String status = object.optString("status").trim();
				/*
				 * String customerId=object.optString("customerId").trim();
				 * String customerCell=object.optString("customerCell").trim();
				 * String customerName=object.optString("customerName").trim();
				 * String
				 * customerPOCName=object.optString("customerPOCName").trim();
				 * String branch=object.optString("branch").trim();
				 */
				// String employeeName=object.optString("employeeName").trim();
				// String createdBy=object.optString("createdBy").trim();
				String actionTask = object.optString("actionTask").trim();

				if (actionTask.trim().equalsIgnoreCase("Update")) {
					logger.log(
							Level.SEVERE,
							"Service Count: "
									+ Long.parseLong(object.optString("count")));

					service = ofy()
							.load()
							.type(Service.class)
							.filter("companyId", comp.getCompanyId())
							.filter("count",
									Long.parseLong(object.optString("count")))
							.first().now();
				}
				// service.setEmployee(employeeName);
				service.setStatus(status);
				service.setServiceTime(object.optString("serviceTime").trim());

				/*
				 * List<String> technicians = new ArrayList<>();
				 * technicians.add(object.optString("technician").trim());
				 * service.setTechnicians(technicians);
				 */
				service.setTeam(object.optString("technician").trim());
				service.setEmployee(object.optString("technician").trim());
				try {
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					service.setServiceDate(sdf.parse(object.optString(
							"serviceDate").trim()));
				} catch (Exception e) {
					logger.log(Level.SEVERE,
							"Customer Service Date Error:" + e.getMessage());
				}

				ofy().save().entity(service);

				JSONObject jsonObject = new JSONObject();
				try {
					jsonObject.put("docId", service.getCount());
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					jsonObject
							.put("netPayable", service.getServiceValue() + "");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String responseStr = jsonObject.toString().replace("\\\\", "");
				resp.getWriter().println(responseStr + "");

			} catch (Exception e) {
				logger.log(Level.SEVERE,
						"Customer Service Updation Error:" + e.getMessage());
			}
		}else if (screenName.trim().equalsIgnoreCase("updateServiceRemark")){
			String remark = object.optString("remark");
			String serviceId = object.optString("serviceId");
			String result = updateServiceRemark(companyId, serviceId, remark);
			resp.getWriter().println(result + "");
		}
	}

	/*
	 * Name: Apeksha Gunjal
	 * Date: 21/09/2018 @ 13:15
	 * Note: Update service remark.
	 */
	private String updateServiceRemark(long companyId, String serviceId, String remark){
		try{
			logger.log(Level.SEVERE, "updateServiceRemark serviceId:" + serviceId +" remark: "+remark);
			Service service = ofy().load().type(Service.class)
					.filter("companyId", companyId)
					.filter("count", Integer.parseInt(serviceId)).first().now();

			logger.log(Level.SEVERE, "updateServiceRemark ProductName:" + service.getProductName());
			service.setRemark(remark);
			ofy().save().entities(service);
			logger.log(Level.SEVERE, "updateServiceRemark Service remark updated successfully.");
			return "Successful";
		}catch(Exception e){
			logger.log(Level.SEVERE, "updateServiceRemark Error:" + e.getMessage());
			return "Failed";
		}
	}
	
	private double getNetPayable(List<SalesLineItem> leadProducts) {
		// TODO Auto-generated method stub
		double netPayable = 0;
		int actualNetPayable = 0;
		double productPrice = 0;
		for (int i = 0; i < leadProducts.size(); i++) {
			productPrice = productPrice + (leadProducts.get(i).getPrice() * leadProducts.get(i).getQty());
			double taxAmount1 = getTaxAmout(leadProducts.get(i).getVatTax()
					.getPercentage(), leadProducts.get(i).getPrice()*leadProducts.get(i).getQty());
			netPayable = netPayable + taxAmount1;
			double taxAmount2 = getTaxAmout(leadProducts.get(i).getServiceTax()
					.getPercentage(), leadProducts.get(i).getPrice()*leadProducts.get(i).getQty());
			netPayable = netPayable + taxAmount2;
		}
		actualNetPayable = (int) Math.round(netPayable + productPrice);

		return actualNetPayable;
	}

	private double getTaxAmout(double percentage, double price) {
		// TODO Auto-generated method stub
		double taxAmount = (price / 100) * percentage;
		return taxAmount;
	}

	public double removeAllTaxes(SuperProduct entity) {
		double vat = 0, service = 0;
		double tax = 0, retrVat = 0, retrServ = 0;
		if (entity instanceof ServiceProduct) {
			ServiceProduct prod = (ServiceProduct) entity;
			if (prod.getServiceTax() != null
					&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
			if (prod.getVatTax() != null
					&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
		}

		if (entity instanceof ItemProduct) {
			ItemProduct prod = (ItemProduct) entity;
			if (prod.getVatTax() != null
					&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
			if (prod.getServiceTax() != null
					&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
		}

		if (vat != 0 && service == 0) {
			retrVat = entity.getPrice() / (1 + (vat / 100));
			retrVat = entity.getPrice() - retrVat;
		}
		if (service != 0 && vat == 0) {
			retrServ = entity.getPrice() / (1 + service / 100);
			retrServ = entity.getPrice() - retrServ;
		}
		if (service != 0 && vat != 0) {
			// // Here if both are inclusive then first remove service tax and
			// then on that amount
			// // calculate vat.
			// double removeServiceTax=(entity.getPrice()/(1+service/100));
			//
			// //double taxPerc=service+vat;
			// //retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed
			// below
			// retrServ=(removeServiceTax/(1+vat/100));
			// retrServ=entity.getPrice()-retrServ;

			if (entity instanceof ItemProduct) {
				ItemProduct prod = (ItemProduct) entity;
				if (prod.getServiceTax().getTaxPrintName() != null
						&& !prod.getServiceTax().getTaxPrintName().equals("")
						&& prod.getVatTax().getTaxPrintName() != null
						&& !prod.getVatTax().getTaxPrintName().equals("")) {

					double dot = service + vat;
					retrServ = (entity.getPrice() / (1 + dot / 100));
					retrServ = entity.getPrice() - retrServ;
					// retrVat=0;

				} else {
					// Here if both are inclusive then first remove service tax
					// and then on that amount
					// calculate vat.
					double removeServiceTax = (entity.getPrice() / (1 + service / 100));
					// double taxPerc=service+vat;
					// retrServ=(entity.getPrice()/(1+taxPerc/100)); //line
					// changed below
					retrServ = (removeServiceTax / (1 + vat / 100));
					retrServ = entity.getPrice() - retrServ;
				}
			}

			if (entity instanceof ServiceProduct) {
				ServiceProduct prod = (ServiceProduct) entity;
				if (prod.getServiceTax().getTaxPrintName() != null
						&& !prod.getServiceTax().getTaxPrintName().equals("")
						&& prod.getVatTax().getTaxPrintName() != null
						&& !prod.getVatTax().getTaxPrintName().equals("")) {
					double dot = service + vat;
					retrServ = (entity.getPrice() / (1 + dot / 100));
					retrServ = entity.getPrice() - retrServ;
				} else {
					// Here if both are inclusive then first remove service tax
					// and then on that amount
					// calculate vat.
					double removeServiceTax = (entity.getPrice() / (1 + service / 100));
					// double taxPerc=service+vat;
					// retrServ=(entity.getPrice()/(1+taxPerc/100)); //line
					// changed below
					retrServ = (removeServiceTax / (1 + vat / 100));
					retrServ = entity.getPrice() - retrServ;
				}
			}
		}
		tax = retrVat + retrServ;
		return tax;
	}

	private String createNewCustomer(Company comp, JSONObject object) {

		long companyId = 0l;
		if (comp != null) {
			companyId = comp.getCompanyId();
		} else {
			companyId = Long.parseLong("5348024557502464");
		}
		/*
		 * Name: Apeksha Gunjal Date: 08/09/2018 @ 17:57 Note: Update details of
		 * existing customer from EVA Priora.
		 */
		boolean isUpdate = false;
		int customerId = 0;
		logger.log(Level.SEVERE, "companyId: " + companyId);
		Customer customer = null;
		try {
			if (object.has("actionTask")) {
				String actionTask = object.optString("actionTask");
				logger.log(Level.SEVERE, "actionTask: " + actionTask);
				if (actionTask.trim().equalsIgnoreCase("Update")) {
					isUpdate = true;
					customerId = Integer.parseInt(object.optString("count"));
					logger.log(Level.SEVERE, "customerId: " + customerId);
				} else {
					logger.log(Level.SEVERE, "actionTask is diff: "
							+ actionTask);
					isUpdate = false;
				}
			} else {
				isUpdate = false;
			}
			logger.log(Level.SEVERE, "isUpdate: "+ isUpdate);
			if (isUpdate) {
				customer = ofy().load().type(Customer.class)
						.filter("companyId", comp.getCompanyId())
						.filter("count", customerId).first().now();
				logger.log(Level.SEVERE, "customer: " + customer.getFullname());
			} else {
				long cellNumber = 0l;
				try {
					cellNumber  = Long.parseLong(object
							.optString("cellNumber1"));
				} catch (Exception e) {
					e.printStackTrace();
					logger.log(Level.SEVERE, "ERROR cell No is not Long:::::" + e);
				}
				logger.log(Level.SEVERE,
						"no actionTask. creating new customer.");
				customer = new Customer();
				Customer customerValidate = null;
				if (!object.optString("companyName").trim().equals("")) {
					customerValidate = ofy()
							.load()
							.type(Customer.class)
							.filter("companyId", comp.getCompanyId())
							.filter("companyName",
									object.optString("companyName").trim().toUpperCase())
							.first().now();
					if (customerValidate == null) {
						customerValidate = ofy()
								.load()
								.type(Customer.class)
								.filter("companyId", comp.getCompanyId())
								.filter("fullName",
										object.optString("companyName").trim().toUpperCase())
								.first().now();
					}
					if (customerValidate != null && cellNumber == customerValidate.getCellNumber1()) {
						return "Company name "
								+ object.optString("companyName").trim().toUpperCase()
								+ " already exist!!";
					}
				} else {
					customerValidate = ofy()
							.load()
							.type(Customer.class)
							.filter("companyId", comp.getCompanyId())
							.filter("fullname",
									object.optString("fullName").trim())
							.first().now();
					if (customerValidate != null && cellNumber == customerValidate.getCellNumber1()) {
						return "Customer name "
								+ object.optString("fullName").trim().toUpperCase()
								+ " already exist!!";
					}
				}
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Customer error: " + e);
		}

		if (!object.optString("companyName").trim().equals("")) {
			customer.setCompany(true);
			customer.setCompanyName(object.optString("companyName").toUpperCase());
			customer.setFullname(object.optString("fullName").toUpperCase());
		} else {
			customer.setFullname(object.optString("fullName").toUpperCase());
		}
		customer.setEmail(object.optString("email"));
		try {
			customer.setCellNumber1(Long.parseLong(object
					.optString("cellNumber1")));
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "ERROR cell No is not Long:::::" + e);
		}
		try {
			/**date 2-1-2020 by Amol as per discussion with Priyanka It is updated SalesPerson to Employee Name**/
//			customer.setEmployee(object.optString("salesPerson"));
			customer.setEmployee(object.optString("employeeName"));
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "ERROR in sales person:::::" + e);
		}
		String gstinNumber = object.optString("gstNo");
		customer.setBranch(object.optString("branch"));
	//	customer.setEmployee(object.optString("employee"));
		Address address = new Address();
		address.setAddrLine1(object.optString("addressLine1"));
		address.setAddrLine2(object.optString("addressLine2")); //Ashwini Patil Date:9-06-2022 Address line 2 was not getting saved
		address.setLandmark(object.optString("landmark"));
		address.setLocality(object.optString("locality"));
		address.setState(object.optString("state"));
		address.setCity(object.optString("city"));
		address.setCountry(object.optString("country"));

		try {
			if (!object.optString("pin").trim().isEmpty())
				address.setPin(Long.parseLong(object.optString("pin")));
			else
				address.setPin(0);
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "ERROR Primary Pin is not Long:::::" + e);
		}
		
		
		Address serviceaddress = new Address();
		boolean isServiceAddressDataReceived=false;
		try {
			serviceaddress.setAddrLine1(object.optString("serviceAddressLine1"));
			isServiceAddressDataReceived=true;
		}catch(Exception e) {
		}
		try {
			serviceaddress.setAddrLine2(object.optString("serviceAddressLine2")); //Ashwini Patil Date:9-06-2022 Address line 2 was not getting saved
		}catch(Exception e) {
		}
		try {
			serviceaddress.setLandmark(object.optString("serviceAddressLandmark"));
		}catch(Exception e) {
		}
		try {
			serviceaddress.setLocality(object.optString("serviceAddressLocality"));
		}catch(Exception e) {
		}
		try {	serviceaddress.setState(object.optString("serviceAddressState"));
			serviceaddress.setCity(object.optString("serviceAddressCity"));
		}catch(Exception e) {
		}
		try {
			serviceaddress.setCountry(object.optString("serviceAddressCountry"));
		}catch(Exception e) {
		}
		try {
			if (!object.optString("serviceAddressPin").trim().isEmpty())
				serviceaddress.setPin(Long.parseLong(object.optString("serviceAddressPin")));
			else
				serviceaddress.setPin(0);
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "ERROR Primary Pin is not Long:::::" + e);
		}
		
		
		
		ArrayList<ArticleType> articleTypeDetails = new ArrayList<ArticleType>();
		if(gstinNumber!=null&&!gstinNumber.equals("")) {
			ArticleType articleType = new ArticleType();
			articleType.setDocumentName("Service");
			articleType.setArticleTypeName("GSTIN");
			articleType.setArticleTypeValue(gstinNumber);
			articleTypeDetails.add(articleType);
			customer.setArticleTypeDetails(articleTypeDetails);		
		}
		
		
		customer.setAdress(address);
		if(isServiceAddressDataReceived)
			customer.setSecondaryAdress(serviceaddress);
		else
			customer.setSecondaryAdress(address);
		customer.setStatus(Customer.CUSTOMERCREATED);
		customer.setCompanyId(companyId);
		String customerType = "" , customerCategory = "";
		try{
			customerType =object.optString("type");//Ashwini Patil Date:9-06-2022 customer type was not getting saved
		}catch(Exception e){
			
		}
		try{
			customerCategory = object.optString("category");//Ashwini Patil Date:9-06-2022 customer category was not getting saved
		}catch(Exception e){
			
		}
		customer.setType(customerType);
		customer.setCategory(customerCategory);
		int count = 0;
		if (isUpdate) {
			count = customerId;
			logger.log(Level.SEVERE, "customerId: " + count);
		} else {
			NumberGeneration ng = new NumberGeneration();
			ng = ofy().load().type(NumberGeneration.class)
					.filter("companyId", comp.getCompanyId())
					.filter("processName", "Customer").filter("status", true)
					.first().now();

			long number = ng.getNumber();
			count = (int) number;
			ng.setNumber(count + 1);
			ofy().save().entity(ng);

			customer.setCount(count + 1);
			customerId = count + 1;
		}

		ofy().save().entities(customer);

		logger.log(Level.SEVERE, "count of customer:::::::" + customerId);
		return "" + customerId;
	}

	private List<ServiceSchedule> getServiceSceduleList(
			List<SalesLineItem> listofSaleLineItem, String givenDate) {
		/**
		 * Service Schedule Pop Up
		 */
		List<ServiceSchedule> serviceScheduleList = new ArrayList<ServiceSchedule>();

		try {
			for (int i = 0; i < listofSaleLineItem.size(); i++) {
				String previousDate = "";
				int gapOfService = (int) (listofSaleLineItem.get(i)
						.getDuration() / (listofSaleLineItem.get(i)
						.getNumberOfServices() * listofSaleLineItem.get(i)
						.getQty()));
				logger.log(Level.SEVERE, "gapOfService:::::::" + gapOfService);
				for (int j = 0; j < listofSaleLineItem.get(i)
						.getNumberOfServices()
						* listofSaleLineItem.get(i).getQty(); j++) {
//					logger.log(Level.SEVERE, "inside No of Services:::::::::::"
//							+ j);
//					logger.log(Level.SEVERE,
//							"listofSaleLineItem.get(i).getProductSrNo()::::"
//									+ listofSaleLineItem.get(i)
//											.getProductSrNo());
//					logger.log(Level.SEVERE,
//							"listofSaleLineItem.get(i).getPrduct().getCount():::::"
//									+ listofSaleLineItem.get(i).getPrduct()
//											.getCount());
//					logger.log(Level.SEVERE,
//							"listofSaleLineItem.get(i).getPrduct().getProductName()::"
//									+ listofSaleLineItem.get(i).getPrduct()
//											.getProductName());
//					logger.log(Level.SEVERE,
//							"listofSaleLineItem.get(i).getPrduct().getProductName()::"
//									+ listofSaleLineItem.get(i).getPrduct()
//											.getProductName());
//					logger.log(Level.SEVERE,
//							"listofSaleLineItem.get(i).getDuration()::"
//									+ listofSaleLineItem.get(i).getDuration());
//					logger.log(Level.SEVERE,
//							"listofSaleLineItem.get(i).getNumberOfServices()::::::::"
//									+ listofSaleLineItem.get(i)
//											.getNumberOfServices());
//					logger.log(Level.SEVERE, "j+1:::::::::::" + j + 1);

					ServiceSchedule serviceSchedule = new ServiceSchedule();
					serviceSchedule.setSerSrNo(listofSaleLineItem.get(i)
							.getProductSrNo());
					serviceSchedule.setScheduleProdId(listofSaleLineItem.get(i)
							.getPrduct().getCount());
					serviceSchedule.setScheduleProQty((int) listofSaleLineItem
							.get(i).getQty());
					serviceSchedule.setScheduleProdName(listofSaleLineItem
							.get(i).getPrduct().getProductName());
					serviceSchedule.setScheduleDuration(listofSaleLineItem.get(
							i).getDuration());
					serviceSchedule.setScheduleNoOfServices(listofSaleLineItem
							.get(i).getNumberOfServices());
					serviceSchedule.setScheduleServiceNo(j + 1);
					logger.log(Level.SEVERE, "gapOfService:::::::"
							+ gapOfService);

					// ***************Used for marking service date as red/black
					// Date : 2/1/2017********

					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
					sdf.setTimeZone(TimeZone.getTimeZone("IST"));
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));
					Date dateProdEnd = sdf.parse(sdf.format(listofSaleLineItem
							.get(i).getStartDate()));
					Calendar c2 = Calendar.getInstance();
					try {
						c2.setTime(dateProdEnd);
						c2.add(Calendar.DATE, listofSaleLineItem.get(i)
								.getDuration());
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						logger.log(Level.SEVERE, "Error inn j>1:::::::" + e1);

						e1.printStackTrace();
					}
					System.out.println("dateProdEnd:::::::::::" + dateProdEnd
							+ "cal::::::::" + c2.getTime());
					serviceSchedule.setScheduleProdEndDate(c2.getTime());

					// **************ends here ******************************

					if (j + 1 == 1) {
						SimpleDateFormat sdfDateP = new SimpleDateFormat(
								"dd/MM/yyyy");
						sdfDateP.setTimeZone(TimeZone.getTimeZone("IST"));
						TimeZone.setDefault(TimeZone.getTimeZone("IST"));

						try {
							serviceSchedule.setScheduleServiceDate(sdfDateP
									.parse(givenDate));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							logger.log(Level.SEVERE, "Error in j==1" + e);
							e.printStackTrace();
						}
						previousDate = givenDate;
					} else {
						SimpleDateFormat sdfDateS = new SimpleDateFormat(
								"dd/MM/yyyy");
						sdfDateS.setTimeZone(TimeZone.getTimeZone("IST"));
						TimeZone.setDefault(TimeZone.getTimeZone("IST"));

						Calendar c1 = Calendar.getInstance();
						try {
							c1.setTime(sdfDateS.parse(previousDate));
							// } catch (ParseException e1) {
							// // TODO Auto-generated catch block
							// logger.log(Level.SEVERE, "Error inn j>1:::::::"
							// + e1);
							//
							// e1.printStackTrace();
							// }
							logger.log(Level.SEVERE, "gapOfService:::::::"
									+ gapOfService);
							// Calendar c = Calendar.getInstance();
							// dcs
							// try {
							c1.add(Calendar.DATE, gapOfService);
							Date date1 = c1.getTime();
							// serviceSchedule2.setScheduleServiceDay();

							serviceSchedule.setScheduleServiceDate(sdfDateS
									.parse(sdfDateS.format(date1)));
							previousDate = sdfDateS.format(date1);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							logger.log(Level.SEVERE,
									"Error in Adding days:::::::" + e);
							e.printStackTrace();
						}
					}
					serviceSchedule.setScheduleServiceTime("Flexible");
					serviceSchedule.setTotalServicingTime(0.0);
					serviceSchedule.setScheduleProBranch("Service Address");
					serviceScheduleList.add(serviceSchedule);
					logger.log(Level.SEVERE, "serviceScheduleList:::::::::::"
							+ serviceScheduleList.size());
				}
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "ERROR in Method:::::::::::" + e);
			e.printStackTrace();
		}
		logger.log(Level.SEVERE, "serviceScheduleList.size()::::::::::::"
				+ serviceScheduleList.size());
		return serviceScheduleList;
	}

	/*
	 * Name: Apeksha Gunjal Date: 05/07/2018 @15:42 Note: Send Request after
	 * creating Contract
	 */
	private boolean reactToRequestForApprovalForContract(Contract contract,
			String documentCreatedBy, long companyId) {
		try {
			logger.log(
					Level.SEVERE,
					"reactToRequestForApprovalForContract:documentCreatedBy: "
							+ documentCreatedBy + " count: "
							+ contract.getCount());

			Approvals approval = getApprovals("Contract",
					contract.getEmployee(), contract.getApproverName(),
					contract.getBranch(), contract.getCount(),
					documentCreatedBy, "" + contract.getCustomerId(),
					contract.getCustomerFullName(), companyId);
			boolean isSuccessful = saveApproval(approval,
					ConcreteBusinessProcess.REQUESTED, "Contract",
					contract.getCompanyId(), contract.getCount());
			return isSuccessful;
		} catch (Exception e) {
			logger.log(
					Level.SEVERE,
					"Error: reactToRequestForApprovalForContract: "
							+ e.getMessage());
			return false;
		}
	}

	/*
	 * Name: Apeksha Gunjal Date: 05/07/2018 @15:42 Note: Send Request after
	 * creating Quotation
	 */
	private boolean reactToRequestForApprovalForQuotation(Quotation quotation,
			String documentCreatedBy, long companyId) {
		try {
			logger.log(
					Level.SEVERE,
					"reactToRequestForApprovalForQuotation:documentCreatedBy: "
							+ documentCreatedBy + " count: "
							+ quotation.getCount());

			Approvals approval = getApprovals("Quotation",
					quotation.getEmployee(), quotation.getApproverName(),
					quotation.getBranch(), quotation.getCount(),
					documentCreatedBy, "" + quotation.getCustomerId(),
					quotation.getCustomerFullName(), companyId);
			logger.log(Level.SEVERE,
					"approval getApproverName: " + approval.getApproverName());

			boolean isSuccessful = saveApproval(approval,
					ConcreteBusinessProcess.REQUESTED, "Quotation",
					quotation.getCompanyId(), quotation.getCount());
			return isSuccessful;
		} catch (Exception e) {
			logger.log(
					Level.SEVERE,
					"Error: reactToRequestForApprovalForQuotation: "
							+ e.getMessage());
			return false;
		}
	}

	/*
	 * Name: Apeksha Gunjal Date: 04/07/2018 @19:39 Note: Send Request after
	 * creating Sales Quotation
	 */
	private boolean reactToRequestForApprovalForSalesQuotation(
			SalesQuotation salesQuotation, String documentCreatedBy, 
			long companyId) {
		try {
			logger.log(
					Level.SEVERE,
					"reactToRequestForApprovalForSalesQuotation:documentCreatedBy: "
							+ documentCreatedBy + " count: "
							+ salesQuotation.getCount());

			Approvals approval = getApprovals("Sales Quotation",
					salesQuotation.getEmployee(),
					salesQuotation.getApproverName(),
					salesQuotation.getBranch(), salesQuotation.getCount(),
					documentCreatedBy, "" + salesQuotation.getCustomerId(),
					salesQuotation.getCustomerFullName(), companyId);
			boolean isSuccessful = saveApproval(approval,
					ConcreteBusinessProcess.REQUESTED, "Sales Quotation",
					salesQuotation.getCompanyId(), salesQuotation.getCount());
			return isSuccessful;
		} catch (Exception e) {
			logger.log(
					Level.SEVERE,
					"Error: reactToRequestForApprovalForSalesQuotation: "
							+ e.getMessage());
			return false;
		}
	}

	/*
	 * Name: Apeksha Gunjal Date: 05/07/2018 @15:42 Note: Send Request after
	 * creating SalesOrder
	 */
	private boolean reactToRequestForApprovalForSalesOrder(
			SalesOrder salesOrder, String documentCreatedBy, long companyId) {
		try {
			logger.log(
					Level.SEVERE,
					"reactToRequestForApprovalForSalesOrder:documentCreatedBy: "
							+ documentCreatedBy + " count: "
							+ salesOrder.getCount());

			Approvals approval = getApprovals("Sales Order",
					salesOrder.getEmployee(), salesOrder.getApproverName(),
					salesOrder.getBranch(), salesOrder.getCount(),
					documentCreatedBy, "" + salesOrder.getCustomerId(),
					salesOrder.getCustomerFullName(), companyId);
			boolean isSuccessful = saveApproval(approval,
					ConcreteBusinessProcess.REQUESTED, "Sales Order",
					salesOrder.getCompanyId(), salesOrder.getCount());
			return isSuccessful;
		} catch (Exception e) {
			logger.log(
					Level.SEVERE,
					"Error: reactToRequestForApprovalForSalesOrder: "
							+ e.getMessage());
			return false;
		}
	}

	/*
	 * Name: Apeksha Gunjal Date: 05/07/2018 @11:30 Note: Save Approval and
	 * respective entities after sending approval.
	 */
	private boolean saveApproval(Approvals approval, String status,
			String screenName, long companyId, int count) {
		try {
			logger.log(Level.SEVERE, "screenName: " + screenName
					+ " companyId: " + companyId + " count; " + count);

			approval.setCompanyId(companyId);
			ofy().save().entities(approval);
			logger.log(Level.SEVERE, screenName
					+ " approval send successfully..");

			if (screenName.trim().equalsIgnoreCase("Sales Quotation")) {
				SalesQuotation salesQuotation = ofy().load()
						.type(SalesQuotation.class)
						.filter("companyId", companyId).filter("count", count)
						.first().now();
				salesQuotation.setStatus(status);
				ofy().save().entities(salesQuotation);
				logger.log(Level.SEVERE,
						"SalesQuotation save successfully... after sending approval");
			} else if (screenName.trim().equalsIgnoreCase("Quotation")) {
				Quotation quotation = ofy().load().type(Quotation.class)
						.filter("companyId", companyId).filter("count", count)
						.first().now();
				quotation.setStatus(status);
				ofy().save().entities(quotation);
				logger.log(Level.SEVERE,
						"Quotation save successfully... after sending approval");
			} else if (screenName.trim().equalsIgnoreCase("Contract")) {
				Contract contract = ofy().load().type(Contract.class)
						.filter("companyId", companyId).filter("count", count)
						.first().now();
				contract.setStatus(status);
				ofy().save().entity(contract);
				logger.log(Level.SEVERE,
						"Contract save successfully... after sending approval");
			} else if (screenName.trim().equalsIgnoreCase("Sales Order")) {
				SalesOrder order = ofy().load().type(SalesOrder.class)
						.filter("companyId", companyId).filter("count", count)
						.first().now();
				order.setStatus(status);
				ofy().save().entity(order);
				logger.log(Level.SEVERE,
						"SalesOrder save successfully... after sending approval");
			}
			return true;
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error: saveApproval: " + e.getMessage());
			return false;
		}
	}

	/*
	 * Name: Apeksha Gunjal Date: 05/07/2018 @17:47 Note: get Approval for
	 * server side
	 */
	private Approvals getApprovals(String screenName, String emplyeeName,
			String approverName, String branch, int count,
			String documentCreatedBy, String customerId, String customerName,
			long companyId) {
		Approvals approval = new Approvals();
		try {
			logger.log(Level.SEVERE, "ApprovalsDetails: " + screenName + " "
					+ emplyeeName + " " + approverName + " " + branch + " "
					+ count + " " + documentCreatedBy + " " + customerId + " "
					+ customerName);

			/*
			 * Name: Apeksha Gunjal
			 * Date: 27/09/2018 @ 16:33
			 * Note: Added number generation for setting count of a approval.
			 */
			NumberGeneration ng = new NumberGeneration();
			ng = ofy().load().type(NumberGeneration.class)
					.filter("companyId", companyId)
					.filter("processName", "Approvals").filter("status", true)
					.first().now();

			long number = ng.getNumber();
			int c = (int) number;
			logger.log(Level.SEVERE, "Approval existing count: " +c);
			c = c + 1;
			ng.setNumber(c);
			ofy().save().entity(ng);
			logger.log(Level.SEVERE, "Approval new count: " +c);
			
			approval.setCount(c);
			/*
			 * end setting approval count.
			 */
			approval.setApproverName(approverName);
			approval.setBranchname(branch);
			approval.setBusinessprocessId(count);

			if (UserConfiguration.getInfo() != null
					&& !UserConfiguration.getInfo().getFullName().equals("")) {
				approval.setRequestedBy(UserConfiguration.getInfo()
						.getFullName());
			}

			// here i am setting person responsible to send email in cc for
			// person responsible
			approval.setPersonResponsible(emplyeeName);
			approval.setDocumentCreatedBy(documentCreatedBy);
			approval.setApprovalLevel(1);
			approval.setStatus(Approvals.PENDING);
			approval.setBusinessprocesstype(getBuisnessProcessType(screenName));
			approval.setBpId(customerId);
			approval.setBpName(customerName);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error:getApprovals: " + e.getMessage());
		}
		return approval;
	}

	/*
	 * Name: Apeksha Gunjal Date: 05/07/2018 @17:47 Note: get
	 * getBuisnessProcessType for server side
	 */
	private String getBuisnessProcessType(String screenName) {
		logger.log(Level.SEVERE, "getBuisnessProcessType screenName: "
				+ screenName);
		switch (screenName) {
		case "Quotation":
			return "Quotation";
		case "Contract":
			return "Contract";
		case "Sales Order":
			return "Sales Order";
		case "Sales Quotation":
			return "Sales Quotation";
		}
		return "N.A";
	}
	
	//Ashwini Patil Date:9-06-2023
	public void sendComplainCreationEmail(Complain complain,Customer customer,Service service){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
	    
			String fromEmail="";
			Company comp=ofy().load().type(Company.class).filter("companyId", complain.getCompanyId()).first().now();
			logger.log(Level.SEVERE,"In sendComplainCreationEmail");
			ArrayList<String> toEmailList = new ArrayList<String>();
			
			if(customer!=null&&customer.getEmail()!=null&&!customer.getEmail().equals(""))
				toEmailList.add(customer.getEmail());
			if(comp!=null){

				fromEmail=ServerAppUtility.getCompanyEmail(complain.getBranch(), comp);
				
				String mailSub=""; 
				String mailMsg="";
				
				EmailTemplateConfiguration emailtemplateconfig = ofy().load().type(EmailTemplateConfiguration.class)
						.filter("companyId", comp.getCompanyId()).filter("moduleName", "Service").filter("documentName", "Customer Support")
						.filter("communicationChannel", "Email").filter("templateName", AppConstants.COMPLAINCREATION)
						.filter("status", true).first().now();
				
				
				EmailTemplate emailTemplate = ofy().load().type(EmailTemplate.class).filter("companyId", complain.getCompanyId())
						.filter("templateName", "ComplainCreation")	
						.filter("templateStatus", true).first().now();
				
				if(emailtemplateconfig!=null&&emailTemplate!=null) {
					mailSub = emailTemplate.getSubject();
					mailMsg = emailTemplate.getEmailBody();
					
					String resultString = mailMsg.replaceAll("[\n]", "<br>");
					String resultString1 = resultString.replaceAll("[\\s]", "&nbsp;");
					if(resultString1.contains("{CustomerName}")) {
						resultString1=resultString1.replace("{CustomerName}",complain.getPersoninfo().getFullName());	
					}
					if(resultString1.contains("{ComplainID}")) {
						resultString1=resultString1.replace("{ComplainID}","Complain ID: "+complain.getCount()+"");	
					}
					if(resultString1.contains("{DateTime}")) {
						resultString1=resultString1.replace("{DateTime}","Date and Time: "+sdf.format(complain.getDate())+"");	
					}
					if(resultString1.contains("{Description}")) {
						if(complain.getDestription()!=null&&!complain.getDestription().equals(""))
							resultString1=resultString1.replace("{Description}","Description: "+complain.getDestription());	
						else
							resultString1=resultString1.replace("{Description}","");	
					}
					if(resultString1.contains("{Status}")) {
						resultString1=resultString1.replace("{Status}","Status: "+complain.getCompStatus());	
					}
					if(resultString1.contains("{CustomerId}")) {
						resultString1=resultString1.replace("{CustomerId}","Customer ID: "+complain.getPersoninfo().getCount()+"");	
					}
					if(resultString1.contains("{ServiceId}")) {
						if(service!=null)
							resultString1=resultString1.replace("{ServiceId}","Service ID: "+service.getCount());	
						else
							resultString1=resultString1.replace("{ServiceId}","");
					}
					if(resultString1.contains("{ProductName}")) {
						if(complain.getPic()!=null)
							resultString1=resultString1.replace("{ProductName}","Product Name: "+complain.getPic().getProductName());	
						else
							resultString1=resultString1.replace("{ProductName}","");	
					}
					if(resultString1.contains("{CompletionDate}")) {
						if(service!=null&&service.getCompletedDate()!=null)
							resultString1=resultString1.replace("{CompletionDate}","Completion Date: "+sdf.format(service.getCompletedDate()));	
						else
							resultString1=resultString1.replace("{CompletionDate}","");	
					}
					if(resultString1.contains("{CompanySignature}")) {
						resultString1=resultString1.replace("{CompanySignature}",ServerAppUtility.getCompanySignature(complain.getBranch(),complain.getCompanyId()));	
					}
					
					mailMsg=resultString1;
					
					logger.log(Level.SEVERE,"email subject: "+mailSub);
					logger.log(Level.SEVERE,"email body: "+mailMsg);
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, complain.getCompanyId())){
						logger.log(Level.SEVERE,"In send grid condition");
						SendGridEmailServlet sdEmail=new SendGridEmailServlet();
						sdEmail.sendMailWithSendGrid(fromEmail, toEmailList, null, null, mailSub, mailMsg, "text/html",null,null,"application/pdf",null);

					}else {
						toEmailList.add(fromEmail);
						Email obj=new Email();
						obj.sendEmailforhtml(mailSub, mailMsg, toEmailList, new ArrayList<String>(), fromEmail);
						logger.log(Level.SEVERE,"EMAIL Sending --"+fromEmail);
					}

				}

				
				
				
			
			
			}
			
		
		}
	private Customer createOrFetchCustomer(Company comp, JSONObject object) {

		long companyId = 0l;
		if (comp != null) {
			companyId = comp.getCompanyId();
		} else {
			companyId = Long.parseLong("5348024557502464");
		}
		boolean isUpdate = false;
		int customerId = 0;
		logger.log(Level.SEVERE, "companyId: " + companyId);
		Customer customer = null;
		try {
			if (object.has("actionTask")) {
				String actionTask = object.optString("actionTask");
				logger.log(Level.SEVERE, "actionTask: " + actionTask);
				if (actionTask.trim().equalsIgnoreCase("Update")) {
					isUpdate = true;
					customerId = Integer.parseInt(object.optString("count"));
					logger.log(Level.SEVERE, "customerId: " + customerId);
				} else {
					logger.log(Level.SEVERE, "actionTask is diff: "
							+ actionTask);
					isUpdate = false;
				}
			} else {
				isUpdate = false;
			}
			logger.log(Level.SEVERE, "isUpdate: "+ isUpdate);
			if (isUpdate) {
				customer = ofy().load().type(Customer.class)
						.filter("companyId", comp.getCompanyId())
						.filter("count", customerId).first().now();
				logger.log(Level.SEVERE, "customer: " + customer.getFullname());
			} else {
				long cellNumber = 0l;
				try {
					cellNumber  = Long.parseLong(object
							.optString("cellNumber1"));
				} catch (Exception e) {
					e.printStackTrace();
					logger.log(Level.SEVERE, "ERROR cell No is not Long:::::" + e);
				}
				logger.log(Level.SEVERE,
						"no actionTask. creating new customer.");
				customer = new Customer();
				Customer customerValidate = null;
				if (!object.optString("companyName").trim().equals("")) {
					customerValidate = ofy()
							.load()
							.type(Customer.class)
							.filter("companyId", comp.getCompanyId())
							.filter("companyName",
									object.optString("companyName").trim().toUpperCase())
							.first().now();
					if (customerValidate == null) {
						customerValidate = ofy()
								.load()
								.type(Customer.class)
								.filter("companyId", comp.getCompanyId())
								.filter("fullName",
										object.optString("companyName").trim().toUpperCase())
								.first().now();
					}
					if (customerValidate != null && cellNumber == customerValidate.getCellNumber1()) {
						return customerValidate;
					}
				} else {
					customerValidate = ofy()
							.load()
							.type(Customer.class)
							.filter("companyId", comp.getCompanyId())
							.filter("fullname",
									object.optString("fullName").trim())
							.first().now();
					if (customerValidate != null && cellNumber == customerValidate.getCellNumber1()) {
						return customerValidate;
					}
				}
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Customer error: " + e);
		}

		if (!object.optString("companyName").trim().equals("")) {
			customer.setCompany(true);
			customer.setCompanyName(object.optString("companyName").toUpperCase());
			customer.setFullname(object.optString("fullName").toUpperCase());
		} else {
			customer.setFullname(object.optString("fullName").toUpperCase());
		}
		customer.setEmail(object.optString("email"));
		try {
			customer.setCellNumber1(Long.parseLong(object
					.optString("cellNumber1")));
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "ERROR cell No is not Long:::::" + e);
		}
		try {
			customer.setEmployee(object.optString("employeeName"));
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "ERROR in sales person:::::" + e);
		}
		String gstinNumber = object.optString("gstNo");
		customer.setBranch(object.optString("branch"));
	//	customer.setEmployee(object.optString("employee"));
		Address address = new Address();
		address.setAddrLine1(object.optString("addressLine1"));
		address.setAddrLine2(object.optString("addressLine2")); 
		address.setLandmark(object.optString("landmark"));
		address.setLocality(object.optString("locality"));
		address.setState(object.optString("state"));
		address.setCity(object.optString("city"));
		address.setCountry(object.optString("country"));

		try {
			if (!object.optString("pin").trim().isEmpty()&&!object.optString("pin").trim().equalsIgnoreCase("null"))
				address.setPin(Long.parseLong(object.optString("pin")));
			else
				address.setPin(0);
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "ERROR Primary Pin is not Long:::::" + e);
		}
		
		
		Address serviceaddress = new Address();
		boolean isServiceAddressDataReceived=false;
		try {
			serviceaddress.setAddrLine1(object.optString("serviceAddressLine1"));
			isServiceAddressDataReceived=true;
		}catch(Exception e) {
		}
		try {
			serviceaddress.setAddrLine2(object.optString("serviceAddressLine2")); 
		}catch(Exception e) {
		}
		try {
			serviceaddress.setLandmark(object.optString("serviceAddressLandmark"));
		}catch(Exception e) {
		}
		try {
			serviceaddress.setLocality(object.optString("serviceAddressLocality"));
		}catch(Exception e) {
		}
		try {	serviceaddress.setState(object.optString("serviceAddressState"));
			serviceaddress.setCity(object.optString("serviceAddressCity"));
		}catch(Exception e) {
		}
		try {
			serviceaddress.setCountry(object.optString("serviceAddressCountry"));
		}catch(Exception e) {
		}
		try {
			if (!object.optString("serviceAddressPin").trim().isEmpty()&&!object.optString("serviceAddressPin").trim().equalsIgnoreCase("null"))
				serviceaddress.setPin(Long.parseLong(object.optString("serviceAddressPin")));
			else
				serviceaddress.setPin(0);
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "ERROR Primary Pin is not Long:::::" + e);
		}
		
		
		
		ArrayList<ArticleType> articleTypeDetails = new ArrayList<ArticleType>();
		ArticleType articleType = new ArticleType();
		articleType.setDocumentName("Service");
		articleType.setArticleTypeName("GSTIN");
		articleType.setArticleTypeValue(gstinNumber);
		articleTypeDetails.add(articleType);
		customer.setArticleTypeDetails(articleTypeDetails);
		customer.setAdress(address);
		if(isServiceAddressDataReceived)
			customer.setSecondaryAdress(serviceaddress);
		else
			customer.setSecondaryAdress(address);
		customer.setStatus(Customer.CUSTOMERCREATED);
		customer.setCompanyId(companyId);
		String customerType = "" , customerCategory = "",customerGroup="";
		try{
			customerType =object.optString("customertype");
		}catch(Exception e){
			
		}
		try{
			customerCategory = object.optString("customercategory");
		}catch(Exception e){
			
		}
		try{
			customerGroup = object.optString("customerGroup");
		}catch(Exception e){
			
		}
		customer.setType(customerType);
		customer.setCategory(customerCategory);
		customer.setGroup(customerGroup);
		int count = 0;
		if (isUpdate) {
			count = customerId;
			logger.log(Level.SEVERE, "customerId: " + count);
		} else {
			NumberGeneration ng = new NumberGeneration();
			ng = ofy().load().type(NumberGeneration.class)
					.filter("companyId", comp.getCompanyId())
					.filter("processName", "Customer").filter("status", true)
					.first().now();

			long number = ng.getNumber();
			count = (int) number;
			ng.setNumber(count + 1);
			ofy().save().entity(ng);

			customer.setCount(count + 1);
			customerId = count + 1;
		}

		ofy().save().entities(customer);

		logger.log(Level.SEVERE, "count of customer:::::::" + customerId);
		return customer;
	}

}
