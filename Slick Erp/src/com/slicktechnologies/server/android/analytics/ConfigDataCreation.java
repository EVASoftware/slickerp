package com.slicktechnologies.server.android.analytics;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.ExpensePolicies;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;

import org.json.JSONArray;
import org.json.JSONException;
/**
 * Description : This API is used to give all configurations when user logged in into the application.
 * @author pc1
 *
 */
public class ConfigDataCreation extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2162418121719109126L;

	Logger logger = Logger.getLogger("ConfigDataCreation.class");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		RegisterServiceImpl impl = new RegisterServiceImpl();
		impl.getClass();

//		resp.setHeader("Access-Control-Allow-Origin", AppConstants.TRENDZWEBSOLUTION);

		resp.setHeader("Access-Control-Allow-Origin", "*");
		resp.setContentType("application/json;charset=utf-8"); //Ashwini Patil Date:1-11-2022
		boolean errorFlag=false;

		String urlCalled = req.getRequestURL().toString().trim();
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
		
		String employeeGroup = "";
		try{
			employeeGroup = req.getParameter("employeeGroup");
		}catch(Exception e){
			employeeGroup = "";
		}
		String employeeName = "";
		try{
			employeeName = req.getParameter("employeeName");
		}catch(Exception e){
			employeeName = "";
		}
		
		int employeeId = 0;
		try{
			employeeId = Integer.parseInt(req.getParameter("employeeId"));
		}catch(Exception e){
			employeeId = 0;
		}
		
		
		String applicationName="";
		
		
		try{
			applicationName=req.getParameter("applicationName");
		}catch(Exception e){
			applicationName="";
		}
		logger.log(Level.SEVERE, "applicationName "+applicationName);
		
		
		boolean serviceChecklistFlag = false;
		try{
			String strServiceChecklistFlag=req.getParameter("serviceCheckListFlag");
			if(strServiceChecklistFlag!=null && !strServiceChecklistFlag.trim().equals("") && strServiceChecklistFlag.trim().equals("true")) {
				serviceChecklistFlag = true;
			}
		}catch(Exception e){
		}
	
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();

		List<ItemProduct> itemProductList = ofy().load()
				.type(ItemProduct.class)
				.filter("companyId", comp.getCompanyId()).list();
		List<ServiceProduct> serviceProductList = ofy().load()
				.type(ServiceProduct.class)
				.filter("companyId", comp.getCompanyId()).list();//.filter("productCategory","ANDROID").list();
		List<Branch> branchList = ofy().load().type(Branch.class)
				.filter("companyId", comp.getCompanyId()).list();
		
//		/**
//		 * @author Anil , Date : 20-02-2020
//		 * Loading only logged in person's customer 
//		 * Requirement raised by Nitin Sir and Sonu for pune conference
//		 * @author Anil @since 19-01-2021
//		 * For priora app also we are loading sales person wise customer
//		 * Raised by Ashwini ,Nitin for pecop
//		 */
//		logger.log(Level.SEVERE, "Logged In Employee Name" + employeeName);
//		List<Customer> customerList=null;
//		if(!employeeName.equals("")&&(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio", "LoadLoggedInUserWiseCustomer", comp.getCompanyId())
//				||ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Priora", "LoadLoggedInUserWiseCustomer", comp.getCompanyId()))){
//			customerList = ofy().load().type(Customer.class)
//					.filter("businessProcess.employee", employeeName).filter("companyId", comp.getCompanyId()).list();
//		}else{
//			customerList = ofy().load().type(Customer.class)
//					.filter("companyId", comp.getCompanyId()).list();
//		}
		
		/**
		 * @author Vijay Date :- 30-07-2022
		 * Des :- Customer loading as per employee branches by default
		 * if servicechecklist flag is true then active contract customer data loading and
		 * if PC_LOADALLCUSTOMER process config is active then loading all customer 
		 */
		List<Customer> customerList=new ArrayList<Customer>();
		logger.log(Level.SEVERE, "employeeId "+employeeId);

		if(!employeeName.equals("") && serviceChecklistFlag) {
			
			logger.log(Level.SEVERE, "for checklist "+serviceChecklistFlag);

			boolean loadallcustomerFlag = true;
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio", AppConstants.PC_LOADALLCUSTOMER, comp.getCompanyId()) || 
					ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Priora", AppConstants.PC_LOADALLCUSTOMER, comp.getCompanyId())) {
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio", AppConstants.PC_LOADALLCUSTOMER, comp.getCompanyId())) {
					customerList = ofy().load().type(Customer.class)
							.filter("companyId", comp.getCompanyId()).list();
				}
				else if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Priora", AppConstants.PC_LOADALLCUSTOMER, comp.getCompanyId())) {
					customerList = ofy().load().type(Customer.class)
							.filter("companyId", comp.getCompanyId()).list();
				}
				loadallcustomerFlag = false;
			}
			//Commented on Date:2-11-2022 By: Ashwini Patil
			//Pecopp is having 2 technicians for one contract but in contract we can specify only one technician. because of below logic only that one technician was receiving customer data. So we are discarding this logic and keeping same logic for audit as well as checklist.
//			else if(loadallcustomerFlag) {
//			
//				List<Contract> contractList1 = ofy().load().type(Contract.class)
//						.filter("companyId", comp.getCompanyId()).filter("technicianName", employeeName)
//						.filter("status", Contract.APPROVED).list();
//				if(contractList1!=null)
//					logger.log(Level.SEVERE, "contractList1 size="+contractList1.size());
////				//Ashwini Patil Date:4-10-2022 Since index was missing and all indexes are occupied filtering data manually
////				List<Contract> contractList1 = ofy().load().type(Contract.class)
////						.filter("companyId", comp.getCompanyId()).filter("endDate >=", new Date())
////						.filter("status", Contract.APPROVED).list();
//				
//				List<Contract> contractList=new ArrayList<Contract>();
//				for(Contract c:contractList1) {					
//					if(c.getEndDate().after(new Date()))
//						contractList.add(c);
//				}
//				logger.log(Level.SEVERE, "For Active contract contractList size = "+contractList.size());
//				if(contractList.size()>0) {
//					ArrayList<Integer> customerIdList = new ArrayList<Integer>();
//					for(Contract contractEntity : contractList) {
//						customerIdList.add(contractEntity.getCinfo().getCount());
//					}
//					customerList = ofy().load().type(Customer.class).filter("companyId", comp.getCompanyId()).filter("count IN", customerIdList).list();
//					logger.log(Level.SEVERE, "For Active contract customerList size = "+customerList.size());
//				}
//			}
			else {
				customerList = loadBranchWiseCustomer(employeeId,employeeName,comp.getCompanyId());
			}
		}
		else {
			logger.log(Level.SEVERE, "else block "+employeeName);

			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio", AppConstants.PC_LOADALLCUSTOMER, comp.getCompanyId())) {
				customerList = ofy().load().type(Customer.class).filter("companyId", comp.getCompanyId()).list();
			}
			else if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Priora", AppConstants.PC_LOADALLCUSTOMER, comp.getCompanyId())) {
				customerList = ofy().load().type(Customer.class).filter("companyId", comp.getCompanyId()).list();
			}
			else {
				customerList = loadBranchWiseCustomer(employeeId,employeeName,comp.getCompanyId());
			}
		}
		/**
		 * ends here
		 */
		
//		List<Customer> customerList = ofy().load().type(Customer.class)
//				.filter("companyId", comp.getCompanyId()).list();

		/**Date 30-1-2020 by Amol ,only Operator role Employee Should be Load in Pedio
		 * and in Priora only Sales role Employee Should be Load  Raised by Nitin Sir.
		 * 
		 */
		List<Employee> employeeList =null;
		ArrayList<String> prioraRoleNameList=new ArrayList<String>();
		prioraRoleNameList.add("Sales");
		logger.log(Level.SEVERE,"Priora Role NAme LIst : " + prioraRoleNameList.size());
		
		ArrayList<String> pedioRoleNameList=new ArrayList<String>();
		pedioRoleNameList.add("Field Engineer");
		pedioRoleNameList.add("Operator");
		pedioRoleNameList.add("Technician");
		logger.log(Level.SEVERE,"pedioRole NAme LIst : " + pedioRoleNameList.size());
		
		if(applicationName!=null&&!applicationName.equals("")){
		if (applicationName.equalsIgnoreCase("EVA Priora")) {
			logger.log(Level.SEVERE, "Inside Priora");
		  /** @Sheetal : 25-02-2022
			   Des : Commenting condition of loading data only when logged in person's role is sales role 
			       in priora app, raised by nitin sir**/
			List<User> userList1 = ofy().load().type(User.class)
					.filter("companyId", comp.getCompanyId())
//					.filter("role.roleName IN", prioraRoleNameList)
					.filter("status", true).list();
			logger.log(Level.SEVERE,"User list size of Priora : " + userList1.size());
			
			HashSet<String> userNameHs = new HashSet<String>();
			if(userList1!=null&&userList1.size()!=0){
				for (User user : userList1) {
					userNameHs.add(user.getEmployeeName());
				}
			}else{
				resp.getWriter().println("User role is Not Define");
			}
			
			ArrayList<String> userNameList = new ArrayList<String>(userNameHs);
			logger.log(Level.SEVERE, "User Name Set size of Priora : "+ userNameList.size());

			employeeList = ofy().load().type(Employee.class)
					.filter("companyId", comp.getCompanyId())
					.filter("fullname IN", userNameList).filter("status", true)
					.list();
			logger.log(Level.SEVERE, "employeeList size of Priora : "+ employeeList.size());

		} else if (applicationName.equalsIgnoreCase("EVA Pedio")) {
			logger.log(Level.SEVERE, "Inside pedio");
			List<User> userList2 = ofy().load().type(User.class)
					.filter("companyId", comp.getCompanyId())
					.filter("role.roleName IN", pedioRoleNameList)
					.filter("status", true).list();
			
			logger.log(Level.SEVERE, "userList2 size of Pedio : " +  userList2.size());

			HashSet<String> userNameHs = new HashSet<String>();
			if(userList2!=null&&userList2.size()!=0){
			for (User user : userList2) {
				userNameHs.add(user.getEmployeeName());
			}
			}else{
				resp.getWriter().println("User role is Not Define");
			}
			ArrayList<String>userNameList=new ArrayList<String>(userNameHs);
			logger.log(Level.SEVERE, "user hashset size of Pedio : " +  userNameList.size());
			
			employeeList = ofy().load().type(Employee.class)
					.filter("companyId", comp.getCompanyId())
					.filter("fullname IN", userNameList).filter("status", true)
					.list();
			logger.log(Level.SEVERE, "employeeList size of Pedio : " +  employeeList.size());
			
		} 
		}
		
		else {
			logger.log(Level.SEVERE, "Inside ELSE OF Load All Employee");
			employeeList = ofy().load().type(Employee.class)
					.filter("companyId", comp.getCompanyId())
					.filter("status", true).list();
		}
		List<Type> configTypeList=ofy().load().type(Type.class)
				.filter("companyId", comp.getCompanyId()).filter("status", true).list();
		List<ConfigCategory> categoryList=ofy().load().type(ConfigCategory.class)
				.filter("companyId", comp.getCompanyId()).filter("status", true).list();
		List<User> userList=ofy().load().type(User.class)
				.filter("companyId", comp.getCompanyId()).filter("status", true).list();
		
		List<Config> leadStatusList=ofy().load().type(Config.class).filter("companyId", comp.getCompanyId()).filter("type",6).list();
		
		List<Config> leadGroupList=ofy().load().type(Config.class).filter("companyId", comp.getCompanyId()).filter("type",32).list();
		
		List<Config> complainStatusList=ofy().load().type(Config.class).filter("companyId", comp.getCompanyId()).filter("type", 23).list();
		
		List<Config> interactionTypeList=ofy().load().type(Config.class).filter("companyId", comp.getCompanyId()).filter("type", 66).list();
		
		List<Config> complainPriorityList=ofy().load().type(Config.class).filter("companyId", comp.getCompanyId()).filter("type", 21).list();
		
		/*
		 * Name: Apeksha Gunjal
		 * Date: 20/06/2018 15:30
		 * Note: need to add lead unsuccessful reasons list. 
		 */
		List<Config> leadUnsuccessfulReasonsList = new ArrayList<>();
		try{
		leadUnsuccessfulReasonsList = ofy().load().type(Config.class)
				.filter("companyId", comp.getCompanyId()).filter("type", 109).list();
		}catch(Exception e){
			e.printStackTrace();
		}
		JSONObject mainObject = new JSONObject();

		JSONArray itemProductarrayJson = new JSONArray();
		for (int i = 0; i < itemProductList.size(); i++) {
			JSONObject jobj = new JSONObject();
			jobj.put("id", itemProductList.get(i).getCount());
			if(itemProductList.get(i).getProductName().contains("\"")) {
				JSONObject errorObj= new JSONObject();
				errorObj.put("errorMessage", "Ask your system administrator to remove double quote from Product Name. Product id:"+itemProductList.get(i).getCount());
				String jsonString = errorObj.toJSONString();
				resp.getWriter().println(jsonString);
				logger.log(Level.SEVERE, "error message sent for product name "+itemProductList.get(i).getProductName());
				return;				
			}
			String pname=itemProductList.get(i).getProductName();
			jobj.put("productName", pname.trim());
			jobj.put("price", itemProductList.get(i).getPrice());
			itemProductarrayJson.put(jobj);
		}
		
		JSONArray complainPriorityArray = new JSONArray();
		for (int i = 0; i < complainPriorityList.size(); i++) {
			JSONObject jobj = new JSONObject();
			jobj.put("priorityName", complainPriorityList.get(i).getName());
			complainPriorityArray.put(jobj);
		}
		

		JSONArray serviceProductarrayJson = new JSONArray();
		for (int i = 0; i < serviceProductList.size(); i++) {
			JSONObject jobj = new JSONObject();
			jobj.put("id", serviceProductList.get(i).getCount());
			String productName=serviceProductList.get(i).getProductName();
			if(serviceProductList.get(i).getProductName().contains("\"")) {
				JSONObject errorObj= new JSONObject();
				errorObj.put("errorMessage", "Ask your system administrator to remove double quote from Product Name. Product id:"+serviceProductList.get(i).getCount());
				String jsonString = errorObj.toJSONString();
				resp.getWriter().println(jsonString);
				logger.log(Level.SEVERE, "error message sent for product name "+productName);
				return;				
			}
			jobj.put("productName",productName);
//			jobj.put("productName", serviceProductList.get(i).getProductName());
			jobj.put("price", serviceProductList.get(i).getPrice());
			jobj.put("noOfServices", serviceProductList.get(i)
					.getNumberOfServices());
			jobj.put("productType",serviceProductList.get(i).getProductType());
			if(serviceProductList.get(i).getSpecification().contains("\"")) {
				JSONObject errorObj= new JSONObject();
				errorObj.put("errorMessage", "Ask your system administrator to remove double quote from Product Specification. Product id:"+serviceProductList.get(i).getCount());
				String jsonString = errorObj.toJSONString();
				resp.getWriter().println(jsonString);
				logger.log(Level.SEVERE, "error message sent for product Specification for product "+productName);
				return;				
			}
			jobj.put("specification",serviceProductList.get(i).getSpecification());
			
			jobj.put("specifications",serviceProductList.get(i).getSpecification());
			/*
			 * Name: Apeksha Gunjal
			 * Date: 06/09/2018 @ 19:30
			 * Note: Added duration in json object of service product.
			 */
			jobj.put("duration",serviceProductList.get(i).getDuration());
			if(serviceProductList.get(i).getProductCategory()!=null)
				jobj.put("productCategory",serviceProductList.get(i).getProductCategory());
			else
				jobj.put("productCategory","");

			serviceProductarrayJson.put(jobj);
		}

		JSONArray branchListarrayJson = new JSONArray();
		JSONObject jobj1 = null;
		for (Branch b: branchList) {
			jobj1 = new JSONObject();
			jobj1.put("branchName", b.getBusinessUnitName());
			branchListarrayJson.put(jobj1);
		}

		JSONArray customerListarrayJson = new JSONArray();
		for (int i = 0; i < customerList.size(); i++) {
			JSONObject jobj = new JSONObject();
			jobj.put("customerId", customerList.get(i).getCount());
			if (customerList.get(i).isCompany()) {
				if(customerList.get(i).getCompanyName().contains("\"")) {
					JSONObject errorObj= new JSONObject();
					errorObj.put("errorMessage", "Ask your system administrator to remove double quote from Customer's Company Name. Customer id:"+customerList.get(i).getCount());
					String jsonString = errorObj.toJSONString();
					resp.getWriter().println(jsonString);
					logger.log(Level.SEVERE, "error message sent for customer's company name "+customerList.get(i).getCompanyName());
					return;				
				}
				jobj.put("customerName", customerList.get(i).getCompanyName());
			} else {
				if(customerList.get(i).getFullname().contains("\"")) {
					JSONObject errorObj= new JSONObject();
					errorObj.put("errorMessage", "Ask your system administrator to remove double quote from Customer's Full Name. Customer id:"+customerList.get(i).getCount());
					String jsonString = errorObj.toJSONString();
					resp.getWriter().println(jsonString);
					logger.log(Level.SEVERE, "error message sent for customer's full name "+customerList.get(i).getFullname());
					return;				
				}
				jobj.put("customerName", customerList.get(i).getFullname());
			}
			jobj.put("customerCell", customerList.get(i).getCellNumber1());
			if(customerList.get(i).getFullname().contains("\"")) {
				JSONObject errorObj= new JSONObject();
				errorObj.put("errorMessage", "Ask your system administrator to remove double quote from Customer's Full Name. Customer id:"+customerList.get(i).getCount());
				String jsonString = errorObj.toJSONString();
				resp.getWriter().println(jsonString);
				logger.log(Level.SEVERE, "error message sent for customer's full name "+customerList.get(i).getFullname());
				return;				
			}
			jobj.put("customerPOCName", customerList.get(i).getFullname());
			jobj.put("branch", customerList.get(i).getBranch());
			jobj.put("salesPerson", customerList.get(i).getEmployee());

			customerListarrayJson.put(jobj);
		}

		JSONArray approverNamearrayJson = new JSONArray();
		for (int i = 0; i < employeeList.size(); i++) {
			JSONObject jobj = new JSONObject();
			jobj.put("approverName", employeeList.get(i).getFullname());

			approverNamearrayJson.put(jobj);
		}
		/** date 28.1.2019 added by komal for employee List **/
		JSONArray employeearrayJson = new JSONArray();
		for (int i = 0; i < employeeList.size(); i++) {
			User obj = getUserDetails(employeeList.get(i).getFullname(), userList);
			JSONObject jobj = new JSONObject();
			jobj.put("employeeId", employeeList.get(i).getCount());
			jobj.put("employeeName", employeeList.get(i).getFullname());
			jobj.put("employeeCell", employeeList.get(i).getCellNumber1());
			/**Date 10-2-2020 by Amol send userName Along With Employee**/
			if(obj!=null){
				jobj.put("userName", obj.getUserName());	
			}else{
				jobj.put("userName", "");
			}
			employeearrayJson.put(jobj);
		}
		
		
		JSONArray userarrayJson = new JSONArray();
		for (int i = 0; i < userList.size(); i++) {
			JSONObject jobj = new JSONObject();
			jobj.put("employeeName", userList.get(i).getEmployeeName());
			jobj.put("userName", userList.get(i).getUserName());
			jobj.put("userId", userList.get(i).getUserId());
			userarrayJson.put(jobj);
		}
		
		/*
		 * Date: 21/05/2018 
		 * Apeksha Gunjal 
		 * Added All technicians.
		 */
		JSONArray technicianNamearrayJson = new JSONArray();
		try {
			logger.log(Level.SEVERE, "employeeList size: " + employeeList.size());
			for (int i = 0; i < employeeList.size(); i++) {
				
				logger.log(Level.SEVERE, "employeeList.get(i).getRoleName(): " 
				+ employeeList.get(i).getRoleName());
				
//				if (employeeList.get(i).getRoleName()
//						.equalsIgnoreCase("Technician")) {
					JSONObject jobj = new JSONObject();
					jobj.put("employeeId", employeeList.get(i).getCount());
					jobj.put("technicianName", employeeList.get(i)
							.getFullname());
					jobj.put("branchName", employeeList.get(i).getBranchName());
					technicianNamearrayJson.put(jobj);
//				}
			}
			logger.log(Level.SEVERE, "technicians: " + technicianNamearrayJson);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error: " + e.getMessage());
		}
		
		mainObject.put("serviceProductArray", serviceProductarrayJson);
		mainObject.put("itemProductArray", itemProductarrayJson);
		mainObject.put("branchList", branchListarrayJson);
		mainObject.put("customerList", customerListarrayJson);
		mainObject.put("approverNameList", approverNamearrayJson);
		mainObject.put("userDetailsList", userarrayJson);
		
		//Date: 21/05/2018 Apeksha Gunjal. Added technicianList
//		mainObject.put("technicianNameList", technicianNamearrayJson);
//		/** date 28.1.2019 added by komal for employee List **/
		mainObject.put("employeeList", employeearrayJson);
		
//		mainObject.put("assesmentCategory", )
		
		JSONArray leadTypeArray=new JSONArray();
		JSONArray salesOrderTypeArray=new JSONArray();
		JSONArray contractTypeArray=new JSONArray();
		JSONArray quotationTypeArray=new JSONArray();
		JSONArray expenseTypeArray=new JSONArray();
		JSONArray assesmentDeficiencyTypeArray=new JSONArray();
		//Added by Apeksha Gunjal on 04/07/2018 @16:49
		JSONArray salesQuotationTypeArray=new JSONArray();
		JSONArray customerTypesArray = new JSONArray();
		
		for (int i = 0; i < configTypeList.size(); i++) {
			if(configTypeList.get(i).getInternalType()==2){
				//Lead Type
				JSONObject jObj=new JSONObject();
				jObj.put("type",configTypeList.get(i).getTypeName());
				jObj.put("status",configTypeList.get(i).getStatus());
				jObj.put("category", configTypeList.get(i).getCategoryName());
				leadTypeArray.put(jObj);
				
			}else if(configTypeList.get(i).getInternalType()==10){
				JSONObject jObj=new JSONObject();
				jObj.put("type",configTypeList.get(i).getTypeName());
				jObj.put("status",configTypeList.get(i).getStatus());
				jObj.put("category", configTypeList.get(i).getCategoryName());
				salesOrderTypeArray.put(jObj);
			}else if(configTypeList.get(i).getInternalType()==4){
				//contractType
				JSONObject jObj=new JSONObject();
				jObj.put("type",configTypeList.get(i).getTypeName());
				jObj.put("status",configTypeList.get(i).getStatus());
				jObj.put("category", configTypeList.get(i).getCategoryName());
				contractTypeArray.put(jObj);
//				customerTypesArray.put(jObj);
			}else if(configTypeList.get(i).getInternalType()==3){
				//contractType
				JSONObject jObj=new JSONObject();
				jObj.put("type",configTypeList.get(i).getTypeName());
				jObj.put("status",configTypeList.get(i).getStatus());
				jObj.put("category", configTypeList.get(i).getCategoryName());
				quotationTypeArray.put(jObj);
			}else if(configTypeList.get(i).getInternalType()==7){
				
				JSONObject jObj=new JSONObject();
				jObj.put("type",configTypeList.get(i).getTypeName());
				jObj.put("status",configTypeList.get(i).getStatus());
				jObj.put("category", configTypeList.get(i).getCategoryName());
				expenseTypeArray.put(jObj);
			}else if(configTypeList.get(i).getInternalType()==26){
				
				JSONObject jObj=new JSONObject();
				jObj.put("type",configTypeList.get(i).getTypeName());
				jObj.put("status",configTypeList.get(i).getStatus());
				jObj.put("category", configTypeList.get(i).getCategoryName());
				assesmentDeficiencyTypeArray.put(jObj);
			}else if(configTypeList.get(i).getInternalType()==9){
				/* 
				 * Apeksha Gunjal 
				 * Date: 04/07/2018 16:49
				 * Added Types of Sales Quotation
				 */
				JSONObject jObj=new JSONObject();
				jObj.put("type",configTypeList.get(i).getTypeName());
				jObj.put("status",configTypeList.get(i).getStatus());
				jObj.put("category", configTypeList.get(i).getCategoryName());
				salesQuotationTypeArray.put(jObj);
			}else if(configTypeList.get(i).getInternalType()==1){//Ashwini Patil Date:10-02-2023
				
				JSONObject jObj=new JSONObject();
				jObj.put("type",configTypeList.get(i).getTypeName());
				jObj.put("status",configTypeList.get(i).getStatus());
				jObj.put("category", configTypeList.get(i).getCategoryName());
				customerTypesArray.put(jObj);
			}
		}
		logger.log(Level.SEVERE, "salesQuotationTypeArray: "+salesQuotationTypeArray);
		
		JSONArray leadCategoryArray=new JSONArray();
		JSONArray salesOrderCategoryArray=new JSONArray();
		JSONArray contractCategoryArray=new JSONArray();
		JSONArray quotationCategoryArray=new JSONArray();
		JSONArray leadStatusArray=new JSONArray();
		JSONArray leadGroupArray=new JSONArray();
		JSONArray expenseCategoryArray=new JSONArray();
		JSONArray assesmentCategoryArray=new JSONArray();
		JSONArray complainStatusArray=new JSONArray();
		JSONArray interactionGroupArray=new JSONArray();
		//Added by Apeksha Gunjal on 04/07/2018 @16:50
		JSONArray salesQuotationCategoryArray=new JSONArray();
//		JSONArray complainPriorityArray=new JSONArray();

		//added by Apeksha Gunjal on 20/06/2018 @15:38
		JSONArray leadUnsuccessfulReasonsArray = new JSONArray();
		JSONArray customerCategoryArray =new JSONArray();
		JSONArray leadInProcessStatusArray=new JSONArray();
		
		for (int i = 0; i < categoryList.size(); i++) {
			if(categoryList.get(i).getInternalType()==2){
				//Lead Type
				JSONObject jObj=new JSONObject();
				jObj.put("category", categoryList.get(i).getCategoryName());
				jObj.put("status",categoryList.get(i).getStatus());
				leadCategoryArray.put(jObj);
				
			}else if(categoryList.get(i).getInternalType()==10){
				JSONObject jObj=new JSONObject();
				jObj.put("status",categoryList.get(i).getStatus());
				jObj.put("category", categoryList.get(i).getCategoryName());
				salesOrderCategoryArray.put(jObj);
			}else if(categoryList.get(i).getInternalType()==4){
				//contractType
				JSONObject jObj=new JSONObject();
				jObj.put("status",categoryList.get(i).getStatus());
				jObj.put("category", categoryList.get(i).getCategoryName());
				contractCategoryArray.put(jObj);
			}else if(categoryList.get(i).getInternalType()==3){
				//contractType
				JSONObject jObj=new JSONObject();
				jObj.put("status",categoryList.get(i).getStatus());
				jObj.put("category", categoryList.get(i).getCategoryName());
				quotationCategoryArray.put(jObj);
			}else if(categoryList.get(i).getInternalType()==7){
				//contractTypes
				JSONObject jObj=new JSONObject();
				jObj.put("status",categoryList.get(i).getStatus());
				jObj.put("category", categoryList.get(i).getCategoryName());
				expenseCategoryArray.put(jObj);
			}else if(categoryList.get(i).getInternalType()==26){
				JSONObject jObj=new JSONObject();
				jObj.put("status",categoryList.get(i).getStatus());
				jObj.put("category", categoryList.get(i).getCategoryName());
				assesmentCategoryArray.put(jObj);
			}else if(categoryList.get(i).getInternalType()==9){
				/* 
				 * Apeksha Gunjal 
				 * Date: 04/07/2018 16:52
				 * Added Category of Sales Quotation
				 */
				JSONObject jObj=new JSONObject();
				jObj.put("status",categoryList.get(i).getStatus());
				jObj.put("category", categoryList.get(i).getCategoryName());
				salesQuotationCategoryArray.put(jObj);
			}else if(categoryList.get(i).getInternalType() == 1){
				JSONObject jObj=new JSONObject();
				jObj.put("status",categoryList.get(i).getStatus());
				jObj.put("category", categoryList.get(i).getCategoryName());
				customerCategoryArray.put(jObj);
			}
		}
		logger.log(Level.SEVERE, "salesQuotationCategoryArray: "+salesQuotationCategoryArray);
		
		for (int i = 0; i < leadStatusList.size(); i++) {
			JSONObject jObj=new JSONObject();
			jObj.put("statusName",leadStatusList.get(i).getName());
			jObj.put("status",leadStatusList.get(i).isStatus());
			
			leadStatusArray.put(jObj);
			if(leadStatusList.get(i).getName().trim().equalsIgnoreCase("Successful")||                   //lead.getStatus().trim().equalsIgnoreCase("Approved")||
					leadStatusList.get(i).getName().trim().equalsIgnoreCase("Successfull")||
					leadStatusList.get(i).getName().trim().equalsIgnoreCase("Sucessfull")||
					leadStatusList.get(i).getName().trim().equalsIgnoreCase("Sucessful")){

			}else if(leadStatusList.get(i).getName().trim().equalsIgnoreCase("Unsucessfull")||
					leadStatusList.get(i).getName().trim().equalsIgnoreCase("Unsucessful")||
					leadStatusList.get(i).getName().trim().equalsIgnoreCase("Unsuccessfull")||
					leadStatusList.get(i).getName().trim().equalsIgnoreCase("Unsuccessful")||
					leadStatusList.get(i).getName().trim().equalsIgnoreCase("Cancelled")){
				
			}else{
				leadInProcessStatusArray.put(jObj);
			}
		}
		
		
		for (int i = 0; i < leadGroupList.size(); i++) {
			JSONObject jObj=new JSONObject();
			jObj.put("groupName",leadGroupList.get(i).getName());
			jObj.put("status",leadGroupList.get(i).isStatus());
			leadGroupArray.put(jObj);
		}
		
		for (int i = 0; i < complainStatusList.size(); i++) {
			JSONObject jObj=new JSONObject();
			jObj.put("statusName",complainStatusList.get(i).getName());
			jObj.put("status",complainStatusList.get(i).isStatus());
			
			complainStatusArray.put(jObj);
		}
		
		for (int i = 0; i < interactionTypeList.size(); i++) {
			JSONObject jObj=new JSONObject();
			jObj.put("groupName",interactionTypeList.get(i).getName());
			jObj.put("groupStatus",interactionTypeList.get(i).isStatus());
			interactionGroupArray.put(jObj);
		}
		
		/*
		 * Name: Apeksha Gunjal
		 * Date: 20/06/2018 15:45
		 * Note: need to add lead unsuccessful reasons list. 
		 */
		for(int i = 0; i < leadUnsuccessfulReasonsList.size(); i++){
			JSONObject jObj = new JSONObject();
			jObj.put("reasonName",leadUnsuccessfulReasonsList.get(i).getName());
			jObj.put("reasonStatus",leadUnsuccessfulReasonsList.get(i).isStatus());
			leadUnsuccessfulReasonsArray.put(jObj);
		}
		
		JSONObject obj=new JSONObject();
		obj.put("type",leadTypeArray);
		obj.put("category",leadCategoryArray);
		obj.put("group",leadGroupArray);		
		obj.put("status",leadStatusArray);
		
		//added by Apeksha Gunjal on 20/06/2018 @15:47
		obj.put("leadUnsuccessfulReasons", leadUnsuccessfulReasonsArray);
		obj.put("InProcessStatus", leadInProcessStatusArray);
		mainObject.put("Lead",obj);
		
		obj=new JSONObject();
		obj.put("status",complainStatusArray);
		obj.put("priority",complainPriorityArray);
		mainObject.put("Complain",obj);
		
		obj=new JSONObject();
		obj.put("groupName",interactionGroupArray);
		mainObject.put("Interaction",obj);
		
		
		obj=new JSONObject();
		obj.put("type",quotationTypeArray);
		obj.put("category",quotationCategoryArray);
		mainObject.put("Quotation",obj);
		/* 
		 * Apeksha Gunjal 
		 * Date: 04/07/2018 16:49
		 * Added Types and Category of Sales Quotation
		 */
		obj = new JSONObject();
		obj.put("type", salesQuotationTypeArray);
		obj.put("category", salesQuotationCategoryArray);
		mainObject.put("SalesQuotation",obj);
		logger.log(Level.SEVERE, "SalesQuotation obj: "+obj);
		
		obj=new JSONObject();
		obj.put("type",contractTypeArray);
		obj.put("category",contractCategoryArray);
		mainObject.put("Contract",obj);
		
		obj=new JSONObject();
		obj.put("type",salesOrderTypeArray);
		obj.put("category",salesOrderCategoryArray);
		mainObject.put("Sales Order",obj);
		
		List<Config> expenseGroupTypeList=ofy().load().type(Config.class).filter("companyId", comp.getCompanyId()).filter("type",40)
				.filter("status", true).list();
		JSONArray expenseGroupArrray=new JSONArray();
		for (int i = 0; i < expenseGroupTypeList.size(); i++) {
			JSONObject jObj=new JSONObject();
			jObj.put("groupName",expenseGroupTypeList.get(i).getName());
			jObj.put("status",expenseGroupTypeList.get(i).isStatus());
			expenseGroupArrray.put(jObj);
		}
		
		obj=new JSONObject();
		obj.put("type",expenseTypeArray);
		obj.put("category",expenseCategoryArray);
		obj.put("groups",expenseGroupArrray);
		mainObject.put("Expense",obj);
		
		obj=new JSONObject();
		obj.put("category",assesmentCategoryArray);
		obj.put("deficiencyType",assesmentDeficiencyTypeArray);
		mainObject.put("AssesmentReport",obj);
		
		
		List<Country> countryList=ofy().load().type(Country.class).filter("companyId", comp.getCompanyId()).list();
		JSONArray countryArray=new JSONArray();
		for (int i = 0; i < countryList.size(); i++) {
			JSONObject jObj=new JSONObject();
			jObj.put("countryName",countryList.get(i).getCountryName());
			countryArray.put(jObj);
		}
		mainObject.put("countries",countryArray);
		
		List<State> stateList=ofy().load().type(State.class).filter("companyId", comp.getCompanyId()).filter("status", true).list();
		JSONArray stateArray=new JSONArray();
		for (int i = 0; i < stateList.size(); i++) {
			JSONObject jObj=new JSONObject();
			if(stateList.get(i).getCountry().contains("\"")) {
				JSONObject errorObj= new JSONObject();
				errorObj.put("errorMessage", "Ask your system administrator to remove double quote from country name "+stateList.get(i).getCountry());
				String jsonString = errorObj.toJSONString();
				resp.getWriter().println(jsonString);
				logger.log(Level.SEVERE, "error message sent for country name for state "+stateList.get(i).getStateName());
				return;
			}
			jObj.put("countryName",stateList.get(i).getCountry());
			if(stateList.get(i).getStateName().contains("\"")) {
				JSONObject errorObj= new JSONObject();
				errorObj.put("errorMessage", "Ask your system administrator to remove double quote from state name "+stateList.get(i).getStateName());
				String jsonString = errorObj.toJSONString();
				resp.getWriter().println(jsonString);
				logger.log(Level.SEVERE, "error message sent for state name"+stateList.get(i).getStateName());
				return;
			}
			jObj.put("stateName",stateList.get(i).getStateName());
			stateArray.put(jObj);
		}
		mainObject.put("state",stateArray);
		
		List<City> cityList=ofy().load().type(City.class).filter("companyId", comp.getCompanyId())
				.filter("status", true).list();
		JSONArray cityArray=new JSONArray();
		for (int i = 0; i < cityList.size(); i++) {
			JSONObject jObj=new JSONObject();
			if(cityList.get(i).getStateName().contains("\"")) {
				JSONObject errorObj= new JSONObject();
				errorObj.put("errorMessage", "Ask your system administrator to remove double quote from state name "+cityList.get(i).getStateName());
				String jsonString = errorObj.toJSONString();
				resp.getWriter().println(jsonString);
				logger.log(Level.SEVERE, "error message sent for state name in city "+cityList.get(i).getCityName());
				return;
			}
			jObj.put("stateName",cityList.get(i).getStateName());
			if(cityList.get(i).getStateName().contains("\"")) {
				JSONObject errorObj= new JSONObject();
				errorObj.put("errorMessage", "Ask your system administrator to remove double quote from city name "+cityList.get(i).getCityName());
				String jsonString = errorObj.toJSONString();
				resp.getWriter().println(jsonString);
				logger.log(Level.SEVERE, "error message sent for city name  "+cityList.get(i).getCityName());
				return;
			}
			jObj.put("cityName",cityList.get(i).getCityName());
			/**
			 * @author Anil , Date : 24-08-2019
			 * setting city class ,for validating expense 
			 */
			String cityClass="";
			if(cityList.get(i).getClassName()!=null){
				cityClass=cityList.get(i).getClassName();
			}
			jObj.put("cityClass",cityClass);
			cityArray.put(jObj);
		}
		mainObject.put("city",cityArray);
		
		
		/**
		 * @author Anil , Date  : 24-08-2019
		 * Adding Expense Policies for validating expense amount while putting expenses from app
		 */
		
		List<ExpensePolicies> expensePoliciesList=ofy().load().type(ExpensePolicies.class).filter("companyId", comp.getCompanyId())
				.filter("status", true).list();
		if(expensePoliciesList!=null){
			JSONArray expensePoliciesArray=new JSONArray();
			for (int i = 0; i < expensePoliciesList.size(); i++) {
				JSONObject jObj=new JSONObject();
				jObj.put("employeeRole",expensePoliciesList.get(i).getEmployeeRole());
				jObj.put("cityClass",expensePoliciesList.get(i).getCityClass());
				jObj.put("expenseGroup",expensePoliciesList.get(i).getExpenseGrp());
				jObj.put("expenseCategory",expensePoliciesList.get(i).getExpenseCategory());
				jObj.put("expenseLimit",expensePoliciesList.get(i).getExpenseLimit());
				jObj.put("atActual",expensePoliciesList.get(i).isAtActual());
				jObj.put("perKm",expensePoliciesList.get(i).isPerKm());
				jObj.put("ratePerKm",expensePoliciesList.get(i).getRatePerKm());
				expensePoliciesArray.put(jObj);
			}
			mainObject.put("expensePolicies",expensePoliciesArray);
		}

		obj = new JSONObject();
		obj.put("type", customerTypesArray);
		obj.put("category", customerCategoryArray);
		mainObject.put("customer",obj);
		
		
		List<Locality> localityList=ofy().load().type(Locality.class).filter("companyId", comp.getCompanyId())
				.filter("status", true).list();
		JSONArray localyArray=new JSONArray();
		for (int i = 0; i < localityList.size(); i++) {
			JSONObject jObj=new JSONObject();
			if(localityList.get(i).getLocality().contains("\"")) {
				JSONObject errorObj= new JSONObject();
				errorObj.put("errorMessage", "Ask your system administrator to remove double quote from locality "+localityList.get(i).getLocality());
				String jsonString = errorObj.toJSONString();
				resp.getWriter().println(jsonString);
				logger.log(Level.SEVERE, "error message sent for locality  "+localityList.get(i).getLocality());
				return;
			}
			jObj.put("localityName",localityList.get(i).getLocality());
			jObj.put("cityName",localityList.get(i).getCityName());
			localyArray.put(jObj);
		}
		mainObject.put("locality",localyArray);
		
		if(employeeGroup != null && !employeeGroup.equals("") && employeeGroup.equalsIgnoreCase("Manager")){
		Employee employee=ofy().load().type(Employee.class).filter("companyId", comp.getCompanyId()).filter("count", employeeId).first().now();
		logger.log(Level.SEVERE , "employee " + employee);
		List<String> branchList1 = new ArrayList<String>();
		if(employee.getEmpBranchList().size()>0){
			for(EmployeeBranch b : employee.getEmpBranchList()){
				branchList1.add(b.getBranchName());
			}
		}else{
			branchList1.add(employee.getBranchName());
		}	
		logger.log(Level.SEVERE , "size " + branchList1.size());
		if(branchList1.size() > 0){
			employeeList = ofy().load().type(Employee.class)
				.filter("companyId", comp.getCompanyId()).filter("branchName IN", branchList1).list();
		}
		JSONArray employeeArray = new JSONArray();
		JSONArray employeearrayJson1 = new JSONArray();
		for (Employee emp : employeeList) {
			JSONObject jobj = new JSONObject();
			jobj.put("id", emp.getCount());
			jobj.put("technicianName", emp.getFullname());
			jobj.put("branchName", emp.getBranchName());

			employeeArray.put(jobj);

			/**
			 * @author Anil , Date : 16-03-2020
			 * If logged in person in manager then restricting employee list
			 */
			User userobj = getUserDetails(emp.getFullname(), userList);
			JSONObject empArrObj = new JSONObject();
			empArrObj.put("employeeId", emp.getCount());
			empArrObj.put("employeeName", emp.getFullname());
			empArrObj.put("employeeCell", emp.getCellNumber1());
			/**Date 10-2-2020 by Amol send userName Along With Employee**/
			if(userobj!=null){
				empArrObj.put("userName", userobj.getUserName());	
			}else{
				empArrObj.put("userName", "");
			}
			employeearrayJson1.put(empArrObj);
		}
			mainObject.put("technicianNameList", employeeArray);
			
			mainObject.put("employeeList", employeearrayJson1);
			
		}else{
			mainObject.put("technicianNameList", technicianNamearrayJson);
		}
	//	logger.log(Level.SEVERE , "empList " + );
		String jsonString = mainObject.toJSONString().replaceAll("\\\\", "");		
		logger.log(Level.SEVERE, jsonString);
		
		resp.getWriter().println(jsonString);

	}

	private List<Customer> loadBranchWiseCustomer(int employeeId, String employeeName, Long companyId) {
		List<Customer> customerList =null;
		Employee employeeEntity = null;
		if(employeeId!=0) {
			employeeEntity = ofy().load().type(Employee.class).filter("count", employeeId).filter("companyId", companyId).first().now();
		}
		if(employeeEntity==null) {
			employeeEntity = ofy().load().type(Employee.class).filter("fullname", employeeName).filter("companyId", companyId).first().now();
		}
		logger.log(Level.SEVERE, "employeeEntity "+employeeEntity);

		if(employeeEntity!=null) {
			ArrayList<String> branchlist = new ArrayList<String>();
			if(employeeEntity.getEmpBranchList().size()>0) {
				for(EmployeeBranch branch : employeeEntity.getEmpBranchList()) {
					branchlist.add(branch.getBranchName());
				}
			}
			logger.log(Level.SEVERE, "branchlist "+branchlist);
			logger.log(Level.SEVERE, "companyId "+companyId);

			if(branchlist!=null && branchlist.size()>0) {
				customerList = ofy().load().type(Customer.class).filter("businessProcess.branch IN", branchlist)
						.filter("companyId", companyId).list();
				logger.log(Level.SEVERE, "customerList size using branch filter "+customerList.size());
			}
			else {
				customerList = ofy().load().type(Customer.class)
						.filter("companyId", companyId).list();
				logger.log(Level.SEVERE, "no branch assigned to employee so loading all customer "+customerList.size());

			}

		}
		
		return customerList;
	}

	private User getUserDetails(String fullname, List<User> userList) {
		for(User obj:userList){
			if(obj.getEmployeeName().equals(fullname)){
				return obj;
			}
		}
		return null;
	}

}
