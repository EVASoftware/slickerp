package com.slicktechnologies.server.android.analytics;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.simple.JSONObject;

import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;

/**
 * Description : This API is used to load service product and item product data.
 * @author pc1
 *
 */
public class AnalyticsProductData extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4816467753577515953L;

	Logger logger = Logger.getLogger("AnalyticsProductData.class");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		JSONObject mainObject = new JSONObject();
		
		RegisterServiceImpl impl = new RegisterServiceImpl();
		impl.getClass();

		String urlCalled = req.getRequestURL().toString().trim();
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		
		List<ItemProduct> itemProductList = ofy().load()
				.type(ItemProduct.class)
				.filter("companyId", comp.getCompanyId()).list();
		List<ServiceProduct> serviceProductList = ofy().load()
				.type(ServiceProduct.class)
				.filter("companyId", comp.getCompanyId()).list();
		
		
		JSONArray itemProductarrayJson = new JSONArray();
		for (int i = 0; i < itemProductList.size(); i++) {
			JSONObject jobj = new JSONObject();
			jobj.put("id", itemProductList.get(i).getCount());
			jobj.put("productName", itemProductList.get(i).getProductName());
			jobj.put("price", itemProductList.get(i).getPrice());
			jobj.put("productType",itemProductList.get(i).getProductType());
			jobj.put("specification",itemProductList.get(i).getSpecification());
			itemProductarrayJson.put(jobj);
		}
		
		JSONArray serviceProductarrayJson = new JSONArray();
		for (int i = 0; i < serviceProductList.size(); i++) {
			JSONObject jobj = new JSONObject();
			jobj.put("id", serviceProductList.get(i).getCount());
			jobj.put("productName", serviceProductList.get(i).getProductName());
			jobj.put("price", serviceProductList.get(i).getPrice());
			jobj.put("noOfServices", serviceProductList.get(i)
					.getNumberOfServices());
			jobj.put("productType",serviceProductList.get(i).getProductType());
			jobj.put("specification",serviceProductList.get(i).getSpecification());
			
			serviceProductarrayJson.put(jobj);
		}
		
		mainObject.put("serviceProductArray", serviceProductarrayJson);
		mainObject.put("itemProductArray", itemProductarrayJson);
		
		String jsonString = mainObject.toJSONString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE, jsonString);
		resp.getWriter().println(jsonString);
		
	}
}
