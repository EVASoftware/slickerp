	package com.slicktechnologies.server.android.analytics;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppConstants.ANDROIDSCREENLIST;
import com.slicktechnologies.client.views.device.TrackTableDetails;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.FeedbackUrlAsPerCustomerCategory;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.role.UserRole;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
/**
 * Description : This API is used to load all screens data i.e. 
 * all win , loss, in process count of all entities on firdt dashboard.
 * This API is also used to load all data based on click of first dashboard.
 * if role =" Sales" then it loads all data of that particular sales person only.
 * @author pc1
 *
 */
public class AnalyticsOperations extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4700568676657371766L;
	
	Logger logger = Logger.getLogger("AnalyticsOperations.class");
	Company comp;
	DecimalFormat formatter;
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
//		resp.setHeader("Access-Control-Allow-Origin", AppConstants.TRENDZWEBSOLUTION);
		resp.setHeader("Access-Control-Allow-Origin", "*");
		
		String urlCalled=req.getRequestURL().toString().trim();
//		String url=urlCalled.replace("http://", "");
//		String[] splitUrl=url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
		
		String loadType=req.getParameter("loadType");
		logger.log(Level.SEVERE, "loadType : "+loadType);
		String employeeName = null;
		try{
			employeeName = req.getParameter("employeeName");
			logger.log(Level.SEVERE, "Employee : "+employeeName);
		}catch(Exception e){
			employeeName = null;
		}
		
//		/**
//		 * @author Vijay Date :- 08-12-2021
//		 * Des :- added authentication for Customer Portal API Call
//		 */
//		try {
//			String strcompanyId = req.getParameter("authCode");
//			if(strcompanyId!=null){
//				if(!strcompanyId.equals("")){
//					long companyId = Long.parseLong(strcompanyId);
//					if(comp.getCompanyId()!=companyId){
//						resp.getWriter().println("Authentication failed!");
//						return;
//					}
//				}
//			}
//		} catch (Exception e) {
//			
//		}
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		
		String jsonString=null;
		/*
		 * Name:Apeksha Gunjal
		 * Date: 11/09/2018 @15:50
		 * Note: Added code for getting customer info from customerId.
		 */
		if(loadType.trim().equalsIgnoreCase("CustomerInfo")){
			Long customerId = Long.parseLong(req.getParameter("customerId"));
			logger.log(Level.SEVERE, "getIndividualCustomerInfo customerId : "+customerId);
			JSONObject jsonObject = getIndividualCustomerInfo(comp.getCompanyId(), customerId);
			logger.log(Level.SEVERE, "getIndividualCustomerInfo jsonObject: "+jsonObject);
			jsonString = ""+jsonObject.toString().replaceAll("\\\\", "");
		}else if(loadType.trim().contains("Priora")){
			String role = "";
			try{
				role = req.getParameter("role");
			}catch(Exception e){
				role = "";
			}
			String fromDateStr=req.getParameter("fromDate");
			String toDateStr=req.getParameter("toDate");
			jsonString=createwrapperJSONData(loadType,role,fromDateStr,toDateStr,employeeName);
		}else{		
			String role = "";
			try{
				role = req.getParameter("role");
			}catch(Exception e){
				role = "";
			}
			String employeeGroup = "";
			try{
				employeeGroup = req.getParameter("employeeGroup");
			}catch(Exception e){
				employeeGroup = "";
			}
			String status = "";
			try{
				status = req.getParameter("status");
				logger.log(Level.SEVERE, "status : "+status);
			}catch(Exception e){
				
			}
//			String fromDateStr=req.getParameter("fromDate");
//			String toDateStr=req.getParameter("toDate");
			
			String fromDateStr="";
			try {
				fromDateStr = req.getParameter("fromDate");
			} catch (Exception e) {
				fromDateStr="";
			}
			String toDateStr="";
			try {
				toDateStr = req.getParameter("toDate");
			} catch (Exception e) {
				toDateStr="";
			}
			String customerCellNo = "";
			try {
				customerCellNo = req.getParameter("customerCellNo");
			} catch (Exception e) {
				// TODO: handle exception
			}
			String customerEmailId = "";
			try {
				customerEmailId = req.getParameter("customerEmail");
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			String apiCallfrom="";
			try {
				apiCallfrom = req.getParameter("apiCallFrom");
				logger.log(Level.SEVERE, "apiCallfrom : "+apiCallfrom);
			} catch (Exception e) {
				// TODO: handle exception
			}
			/**
			 * @author Vijay Date :- 12-05-20222
			 * Des :- For Audit App load only active contract of specific customer
			 */
			String loadactiveContract="";
			try {
				loadactiveContract = req.getParameter("loadActiveContractOnly");
			} catch (Exception e) {
			}
			String customerId = "";
			try {
				customerId = req.getParameter("customerId");
			} catch (Exception e) {
				// TODO: handle exception
			}
			/**
			 * ends here
			 */
			
			String actiontask="";
			try {
				actiontask = req.getParameter("actiontask");
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			String contractStatus="";
			try {
				contractStatus = req.getParameter("contractStatus");
			} catch (Exception e) {
				contractStatus="Active";
			}
			
			/**
			 * @author Vijay Date :- 15-12-2022
			 * Des :- when API call from customer portal then auth code will validate
			 * if auth code is validated then only process the API 
			 */
//			if(apiCallfrom!=null && !apiCallfrom.equals("")) {
//				String strcompanyId = req.getParameter("authCode");
//				ServerAppUtility apputility = new ServerAppUtility();
//				boolean authonticationflag = apputility.validateAuthCode(strcompanyId,comp.getCompanyId());
//				logger.log(Level.SEVERE, "authonticationflag"+authonticationflag);
//				if(!authonticationflag) {
//					resp.getWriter().println("Authentication Failed!");
//					return;
//				}
//			}
			/**
			 * ends here
			 */
			
			if(loadType.trim().equalsIgnoreCase("Contract")&&apiCallfrom!=null && apiCallfrom.equals(AppConstants.CUSTOMERPORTAL)){
				logger.log(Level.SEVERE, "calling createContractJSONData");
				jsonString=createContractJSONData(loadType,fromDateStr,toDateStr,employeeName, role , employeeGroup,status,customerCellNo,customerEmailId,apiCallfrom,loadactiveContract,customerId,actiontask,contractStatus);				
			}else {	
				logger.log(Level.SEVERE, "calling createJSONData");
				jsonString=createJSONData(loadType,fromDateStr,toDateStr,employeeName, role , employeeGroup,status,customerCellNo,customerEmailId,apiCallfrom,loadactiveContract,customerId,actiontask);
			}
		}
		resp.getWriter().println(jsonString);
	}

	private String createwrapperJSONData(String loadType,String role, String fromDateStr, String toDateStr, String employeeName) {
		// TODO Auto-generated method stub
		formatter = new DecimalFormat("#0.00");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		Calendar calendar=Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
//		Date afterDate = calendar.getTime();
//		
//		String aftrDateinString=sdf.format(afterDate);
//		
//		logger.log(Level.SEVERE, "Calendar afterDate:::::::"+aftrDateinString);	
				
		String currentDate=sdf.format(new Date());
		logger.log(Level.SEVERE, "String new Date::::::::"+currentDate);
		JSONArray jArray = null;
		try {
			jArray=new JSONArray();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"Lead Loading...");
		List<Employee> employeeList = null;
		try {
			employeeList = ofy().load().type(Employee.class).filter("companyId", comp.getCompanyId()).list();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		UserRole roleAuth=ofy().load().type(UserRole.class).filter("companyId", comp.getCompanyId()).filter("roleName", role)
				.filter("authorization.module", loadType).first().now();
		JSONObject jsonObject=new JSONObject();
for (int j = 0; j < roleAuth.getAuthorization().size(); j++) {
	if(roleAuth.getAuthorization().get(j).isAndroid()){
		if(roleAuth.getAuthorization().get(j).getScreens().equalsIgnoreCase(ANDROIDSCREENLIST.LEAD)){
			List<Lead> leadList = null;
			try {
				if(role.equalsIgnoreCase("ADMIN")){
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId())
						.filter("creationDate >=", sdf.parse(fromDateStr))
						.filter("creationDate <", sdf.parse(toDateStr)).list();
				}else{
					leadList = ofy().load().type(Lead.class)
							.filter("companyId", comp.getCompanyId())
							.filter("creationDate >=", sdf.parse(fromDateStr))
							.filter("creationDate <", sdf.parse(toDateStr))
							.filter("employee", employeeName).list();
					
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE,"Done Lead Loading...");
			/**
			 * Leads
			 */
			double leadOutStandingBalance=0d,leadSuccessfullBalance=0d,inProcessBalance=0d;
			if(leadList!=null){
			for (Lead lead:leadList) {
				if(lead.getStatus().trim().equalsIgnoreCase("Successful")||                   //lead.getStatus().trim().equalsIgnoreCase("Approved")||
						lead.getStatus().trim().equalsIgnoreCase("Successfull")||
						lead.getStatus().trim().equalsIgnoreCase("Sucessfull")||
						lead.getStatus().trim().equalsIgnoreCase("Sucessful")){
					leadSuccessfullBalance=leadSuccessfullBalance+lead.getNetpayable();
				}else if(lead.getStatus().trim().equalsIgnoreCase("Unsucessfull")||
						lead.getStatus().trim().equalsIgnoreCase("Unsucessful")||
						lead.getStatus().trim().equalsIgnoreCase("Unsuccessfull")||
						lead.getStatus().trim().equalsIgnoreCase("Unsuccessful")||
						lead.getStatus().trim().equalsIgnoreCase("Cancelled")){
					//Checking status "unsuccessful" and "cancelled" by Apeksha Gunjal on 30/07/2018 19:52
					leadOutStandingBalance=leadOutStandingBalance+lead.getNetpayable();
				}else{
					inProcessBalance=inProcessBalance+lead.getNetpayable();
				}
			}
			}
			/**
			 * Ends for Leads
			 */
			//Lead
			
			JSONObject jsonValues=new JSONObject();
			jsonValues.put("outstanding", leadOutStandingBalance);
			jsonValues.put("succesfull", leadSuccessfullBalance);
			jsonValues.put("inProcess",inProcessBalance);
			jsonObject.put("Lead",jsonValues);
		}
		if(roleAuth.getAuthorization().get(j).getScreens().equalsIgnoreCase(ANDROIDSCREENLIST.QUOTATION)){
			logger.log(Level.SEVERE,"Quotation Loading...");
			List<Quotation> quoatationList = null;
			try {
				if(role.equalsIgnoreCase("ADMIN")){
					quoatationList = ofy().load().type(Quotation.class)
							.filter("companyId", comp.getCompanyId())
							.filter("creationDate >=", sdf.parse(fromDateStr))
							.filter("creationDate <", sdf.parse(toDateStr)).list();
					}else{
						quoatationList = ofy().load().type(Quotation.class)
								.filter("companyId", comp.getCompanyId())
								.filter("creationDate >=", sdf.parse(fromDateStr))
								.filter("creationDate <", sdf.parse(toDateStr))
								.filter("employee", employeeName).list();					
					}
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/**
			 * Quotation
			 */
			double quotOutStandingBalance=0d,quotSuccessfullBalance=0d,quotinProcessBalance=0d;
			
			if(quoatationList!=null){
			for (Quotation quot:quoatationList) {
				if(quot.getStatus().trim().equalsIgnoreCase("Approved")||
						quot.getStatus().trim().equalsIgnoreCase("Successfull")||
						quot.getStatus().trim().equalsIgnoreCase("Successful")||quot.getStatus().trim().equalsIgnoreCase("Sucessfull")||quot.getStatus().trim().equalsIgnoreCase("Sucessful")){
					quotSuccessfullBalance=quotSuccessfullBalance+quot.getNetpayable();
				}else if(quot.getStatus().trim().equalsIgnoreCase("Unsucessfull")||
						quot.getStatus().trim().equalsIgnoreCase("Unsucessful")
						||quot.getStatus().trim().equalsIgnoreCase("Unsuccessfull")||
						quot.getStatus().trim().equalsIgnoreCase("Unsuccessful")||
						quot.getStatus().trim().equalsIgnoreCase("Cancelled")){
					//Checking status "unsuccessful" and "cancelled" by Apeksha Gunjal on 30/07/2018 19:52
					quotOutStandingBalance=quotOutStandingBalance+quot.getNetpayable();
				}else{
					quotinProcessBalance=quotinProcessBalance+quot.getNetpayable();
				}
			}
			}
			//Quotation
			JSONObject jsonValues=new JSONObject();
			jsonValues.put("outstanding", quotOutStandingBalance);
			jsonValues.put("succesfull", quotSuccessfullBalance);
			jsonValues.put("inProcess", quotinProcessBalance);
			jsonObject.put("Quotation",jsonValues);
		}
		if(roleAuth.getAuthorization().get(j).getScreens().equalsIgnoreCase(ANDROIDSCREENLIST.SALES_QUOTATION)){
			/*Name: Apeksha Gunjal
			 * Date: 16/06/2018 16:00
			 * Note: Add Sales Quotation and Service Quotation in one screen as a Quotation.
			 * */
			List<SalesQuotation> salesQuotationList = null;
			try{
				if(role.equalsIgnoreCase("ADMIN")){
					salesQuotationList = ofy().load().type(SalesQuotation.class)
							.filter("companyId", comp.getCompanyId())
							.filter("creationDate >=", sdf.parse(fromDateStr))
							.filter("creationDate <", sdf.parse(toDateStr)).list();
				}else{
					salesQuotationList = ofy().load().type(SalesQuotation.class)
							.filter("companyId", comp.getCompanyId())
							.filter("creationDate >=", sdf.parse(fromDateStr))
							.filter("creationDate <", sdf.parse(toDateStr))
							.filter("employee", employeeName).list();
				}					
			}catch(Exception e){
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Done Quotation Loading...");

			/*Name: Apeksha Gunjal
			 * Date: 16/06/2018 16:00
			 * Note: Add Sales Quotation and Service Quotation in one screen as a Quotation.
			 * */
			double quotSuccessfullBalance = 0, quotOutStandingBalance= 0 ,quotinProcessBalance = 0;
			if(salesQuotationList != null){
			for(SalesQuotation salesQuo : salesQuotationList){
				if(salesQuo.getStatus().trim().equalsIgnoreCase("Approved")||salesQuo.getStatus().trim().equalsIgnoreCase("Successfull")||salesQuo.getStatus().trim().equalsIgnoreCase("Successful")||salesQuo.getStatus().trim().equalsIgnoreCase("Sucessfull")||salesQuo.getStatus().trim().equalsIgnoreCase("Sucessful")){
					quotSuccessfullBalance=quotSuccessfullBalance+salesQuo.getNetpayable();
				}else if(salesQuo.getStatus().trim().equalsIgnoreCase("Unsucessfull")||
						salesQuo.getStatus().trim().equalsIgnoreCase("Unsucessful")||
						salesQuo.getStatus().trim().equalsIgnoreCase("Unsuccessfull")||
						salesQuo.getStatus().trim().equalsIgnoreCase("Unsuccessful")||
						salesQuo.getStatus().trim().equalsIgnoreCase("Cancelled")){
					//Checking status "unsuccessful" and "cancelled" by Apeksha Gunjal on 30/07/2018 19:52
					quotOutStandingBalance=quotOutStandingBalance+salesQuo.getNetpayable();
				}else{
					quotinProcessBalance=quotinProcessBalance+salesQuo.getNetpayable();
				}
			}
		}
			/**
			 * Ends for Quotation
			 */
			//Quotation
			JSONObject jsonValues=new JSONObject();
			jsonValues.put("outstanding", quotOutStandingBalance);
			jsonValues.put("succesfull", quotSuccessfullBalance);
			jsonValues.put("inProcess", quotinProcessBalance);
			jsonObject.put("Sales Quotation",jsonValues);
		}
		if(roleAuth.getAuthorization().get(j).getScreens().equalsIgnoreCase(ANDROIDSCREENLIST.CONTRACT)){
			logger.log(Level.SEVERE,"Contract Loading...");
			List<Contract> contractList = null;
			try {
				if(role.equalsIgnoreCase("ADMIN")){
					contractList = ofy().load().type(Contract.class)
							.filter("companyId", comp.getCompanyId())
							.filter("startDate >=", sdf.parse(fromDateStr))
							.filter("startDate <", sdf.parse(toDateStr)).list();
				}else{
					contractList = ofy().load().type(Contract.class)
							.filter("companyId", comp.getCompanyId())
							.filter("startDate >=", sdf.parse(fromDateStr))
							.filter("startDate <", sdf.parse(toDateStr))
							.filter("employee", employeeName).list();
				}
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logger.log(Level.SEVERE,"Done Contract Loading...");
			/**
			 * Contract
			 */
			double contractOutStandingBalance=0d,contractSuccessfullBalance=0d,contractinProcessBalance=0d;
			if(contractList != null){
			for (Contract contract:contractList) {
				if(contract.getStatus().trim().equalsIgnoreCase(Contract.APPROVED)){
					contractSuccessfullBalance=contractSuccessfullBalance+contract.getNetpayable();
				}else if(contract.getStatus().trim().equalsIgnoreCase(Contract.CREATED)||contract.getStatus().trim().equalsIgnoreCase(Contract.REQUESTED)||contract.getStatus().trim().equalsIgnoreCase(Contract.REJECTED)){
					contractinProcessBalance=contractinProcessBalance+contract.getNetpayable();
				}else if(contract.getStatus().trim().equalsIgnoreCase(Contract.CANCELLED)){
					contractOutStandingBalance=contractOutStandingBalance+contract.getNetpayable();
				}
			}
			}
			/**
			 * Ends for Contract
			 */
			//Contract
			JSONObject jsonValues=new JSONObject();
			jsonValues.put("outstanding", contractOutStandingBalance);
			jsonValues.put("succesfull", contractSuccessfullBalance);
			jsonValues.put("inProcess", contractinProcessBalance);
			jsonObject.put("Contract",jsonValues);
			
			
		}
		if(roleAuth.getAuthorization().get(j).getScreens().equalsIgnoreCase(ANDROIDSCREENLIST.INVOICE)){
			logger.log(Level.SEVERE,"Invoice Loading...");
			List<Invoice> invoiceList = null;
			try {
				invoiceList = ofy().load().type(Invoice.class)
						.filter("companyId", comp.getCompanyId())
						.filter("invoiceDate >=", sdf.parse(fromDateStr))
						.filter("invoiceDate <", sdf.parse(toDateStr)).list();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logger.log(Level.SEVERE,"Done Invoice Loading...");
			/**
			 * Invoices
			 */
			double invoiceOutStandingBalance=0d,invoiceBilled=0d,invoiceUnBilled=0d;
			if(invoiceList != null){
			for (Invoice invoice:invoiceList) {
				if(invoice.getStatus().trim().equalsIgnoreCase(Invoice.CREATED)||invoice.getStatus().trim().equalsIgnoreCase(Invoice.REJECTED)||invoice.getStatus().trim().equalsIgnoreCase(Invoice.REQUESTED)||invoice.getStatus().trim().equalsIgnoreCase(Invoice.PROFORMAINVOICE)){
					invoiceUnBilled=invoiceUnBilled+invoice.getNetPayable();
				}else if(invoice.getStatus().trim().equalsIgnoreCase(Invoice.APPROVED)){
					invoiceBilled=invoiceBilled+invoice.getNetPayable();
				}else if(invoice.getStatus().trim().equalsIgnoreCase(Invoice.CANCELLED)){
					invoiceOutStandingBalance=invoiceOutStandingBalance+invoice.getNetPayable();
				}
			}
			}

			/**
			 * Ends for Invoices
			 */
			//invoice
			JSONObject jsonValues=new JSONObject();
			jsonValues.put("outstanding", invoiceOutStandingBalance);
			jsonValues.put("invoiceUnBilled", invoiceUnBilled);
			jsonValues.put("invoiceBilled", invoiceBilled);
			jsonObject.put("Invoice Details",jsonValues);
		}
		if(roleAuth.getAuthorization().get(j).getScreens().equalsIgnoreCase(ANDROIDSCREENLIST.PAYMENT)){
			logger.log(Level.SEVERE,"CustomerPayment Loading...");
			List<CustomerPayment> paymentList = null;
			try {
				paymentList = ofy().load().type(CustomerPayment.class).filter("companyId", comp.getCompanyId()).filter("paymentDate >=", sdf.parse(fromDateStr)).filter("paymentDate <", sdf.parse(toDateStr)).list();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logger.log(Level.SEVERE,"Done CustomerPayment Loading...");
			/**
			 * Payments 
			 */
			double paymentOutStandingBalance=0d,paymentSuccessfullBalance=0d,paymentCancelled=0d;
			/*
			 * Name: Apeksha Gunjal
			 * Date: 04/10/2018 @ 15:25
			 * Note: Added actual payment amount received for android dashboard.
			 */
			double paymentActualOutStandingBalance=0d, paymentActualSuccessfullBalance=0d,
					paymentActualCancelledBalance=0d;
			if(paymentList != null){
			for (CustomerPayment payment:paymentList) {
				if(payment.getStatus().trim().equalsIgnoreCase(CustomerPayment.CLOSED)){
					paymentSuccessfullBalance=paymentSuccessfullBalance+payment.getPaymentAmt();
					paymentActualSuccessfullBalance = paymentActualSuccessfullBalance + payment.getPaymentReceived();
				}else if(payment.getStatus().trim().equalsIgnoreCase(CustomerPayment.CREATED)){
					paymentOutStandingBalance=paymentOutStandingBalance+payment.getPaymentAmt();
					paymentActualOutStandingBalance = paymentActualOutStandingBalance + payment.getPaymentReceived();
				}else if(payment.getStatus().trim().equalsIgnoreCase(CustomerPayment.CANCELLED)){
					paymentCancelled=paymentCancelled+payment.getPaymentAmt();
					paymentActualCancelledBalance = paymentActualCancelledBalance + payment.getPaymentReceived();
				}
			}
			}
			/**
			 * Ends for Payments
			 */
			//Payment
			JSONObject jsonValues=new JSONObject();
			jsonValues.put("outstanding", paymentOutStandingBalance);
			jsonValues.put("succesfull", paymentSuccessfullBalance);
			jsonValues.put("cancelled", paymentCancelled);
			jsonValues.put("actualCancelled", formatter.format(paymentActualCancelledBalance));
			jsonValues.put("actualSuccessful", formatter.format(paymentActualSuccessfullBalance));
			jsonValues.put("actualOutstanding", formatter.format(paymentActualOutStandingBalance));
			jsonObject.put("Payment Details",jsonValues);
			
		}
		if(roleAuth.getAuthorization().get(j).getScreens().equalsIgnoreCase(ANDROIDSCREENLIST.CUSTOMER_SERVICE)){
			logger.log(Level.SEVERE,"Service Loading...");
			List<Service> serviceList = null;
			try {
				serviceList = ofy().load().type(Service.class)
						.filter("companyId", comp.getCompanyId())
						.filter("serviceDate >=", sdf.parse(fromDateStr))
						.filter("serviceDate <", sdf.parse(toDateStr)).list();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logger.log(Level.SEVERE,"Done Service Loading...");
			/**
			 * Service 
			 */
			int serviceDue=0,serviceCompleted=0,serviceCancelled=0;
			double serviceDueValue=0,serviceCompletedValue=0,serviceCancelledValue=0;
			if(serviceList != null){
			for (Service service:serviceList) {
				if(service.getStatus().trim().equalsIgnoreCase(Service.SERVICESTATUSSCHEDULE)||service.getStatus().trim().equalsIgnoreCase(Service.SERVICESTATUSRESCHEDULE)){
					serviceDue=serviceDue+1;
					serviceDueValue=serviceDueValue+service.getServiceValue();
				} else
					try {
						if(service.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCOMPLETED)||service.getStatus().equalsIgnoreCase(Service.SERVICESTATUSTECHCOMPLETED)){
							serviceCompleted=serviceCompleted+1;
							serviceCompletedValue=serviceCompletedValue+service.getServiceValue();
						}else if(service.getStatus().trim().equalsIgnoreCase(Service.CANCELLED)){
							serviceCancelled=serviceCancelled+1;
							serviceCancelledValue=serviceCancelledValue+service.getServiceValue();
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			}
				/**
			 * Ends for Service
			 */
			/*
			 * Name: Apeksha Gunjal
			 * Date: 04/10/2018 @ 15:25
			 * Note: Added actual payment amount received for android dashboard.
			 */
			
			
			JSONObject jsonValues=new JSONObject();
			jsonValues.put("due", serviceDue);
			jsonValues.put("completed", serviceCompleted);
			jsonValues.put("cancelled",serviceCancelled);
			
			jsonObject.put("Customer Service",jsonValues);
			jsonValues=new JSONObject();
			jsonValues.put("due", formatter.format(serviceDueValue));
			jsonValues.put("completed", formatter.format(serviceCompletedValue));
			jsonValues.put("cancelled",formatter.format(serviceCancelledValue));
			
			jsonObject.put("Customer ServiceRevenue",jsonValues);
		}
		if(roleAuth.getAuthorization().get(j).getScreens().equalsIgnoreCase(ANDROIDSCREENLIST.SALES_ORDER)){
			logger.log(Level.SEVERE,"SalesOrder Loading...");
			List<SalesOrder> salesOrderList = null;
			try {
				if(role.equalsIgnoreCase("ADMIN")){
				salesOrderList = ofy().load().type(SalesOrder.class)
						.filter("companyId", comp.getCompanyId())
						.filter("creationDate >=", sdf.parse(fromDateStr))
						.filter("creationDate <", sdf.parse(toDateStr)).list();
				}else{
					salesOrderList = ofy().load().type(SalesOrder.class)
							.filter("companyId", comp.getCompanyId())
							.filter("creationDate >=", sdf.parse(fromDateStr))
							.filter("creationDate <", sdf.parse(toDateStr))
							.filter("employee", employeeName).list();
				}
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/** * Sales Order
			 */
			double salesOrderOutStandingBalance=0d,salesOrderSuccessfullBalance=0d,salesOrderCancelled=0;
			if(salesOrderList != null){
			for (SalesOrder salesOrder:salesOrderList) {
				if(salesOrder.getStatus().trim().equalsIgnoreCase("Approved")||salesOrder.getStatus().trim().equalsIgnoreCase("Successfull")){
					salesOrderSuccessfullBalance=salesOrderSuccessfullBalance+salesOrder.getNetpayable();
				}else if(salesOrder.getStatus().trim().equalsIgnoreCase(SalesOrder.CANCELLED)){
					salesOrderCancelled=salesOrderCancelled+salesOrder.getNetpayable();
				}else{
					salesOrderOutStandingBalance=salesOrderOutStandingBalance+salesOrder.getNetpayable();
				}
			}
			}
			/**
			 * Ends for Sales Order
			 */
			JSONObject jsonValues=new JSONObject();
			jsonValues.put("outstanding", salesOrderOutStandingBalance);
			jsonValues.put("succesfull", salesOrderSuccessfullBalance);
			jsonValues.put("cancelled", salesOrderCancelled);
			
			jsonObject.put("Sales Order",jsonValues);
		}
		if(roleAuth.getAuthorization().get(j).getScreens().equalsIgnoreCase("Billing Details")){
			List<BillingDocument> billDocList = null;
			try {
				billDocList = ofy().load().type(BillingDocument.class)
						.filter("companyId", comp.getCompanyId())
						.filter("creationDate >=", sdf.parse(fromDateStr))
						.filter("creationDate <", sdf.parse(toDateStr)).list();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logger.log(Level.SEVERE,"Done billing Loading...");
		}
		if(roleAuth.getAuthorization().get(j).getScreens().equalsIgnoreCase(ANDROIDSCREENLIST.CONTRACT_RENEWAL) ||
				roleAuth.getAuthorization().get(j).getScreens().equalsIgnoreCase(" Renewal Revenue")){
			/**
			 * Contract Renewal
			 */
			double contractRenewalSingUpBalance=0d,contractRenewalCancelled=0d,contractRenewalinProcess=0d;
			double contractRenewalSingUpBalanceCount=0d,contractRenewalCancelledCount=0d,contractRenewalinProcessCount=0d;
			
			List<Contract> contractRenewalList = null;
			try {
				contractRenewalList = ofy().load().type(Contract.class).filter("companyId", comp.getCompanyId()).filter("endDate >=",sdf.parse(fromDateStr)).filter("endDate <",sdf.parse(toDateStr)).list();
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if(contractRenewalList != null){
			for (Contract contract : contractRenewalList) {
				/*
				 * Name: Apeksha Gunjal
				 * Date: 15/06/2018 17:04
				 * Note: If Contract has cancelled status already then it will be go in cancelled count*/
				if(contract.getStatus().trim().equalsIgnoreCase("Cancelled".trim())){
					contractRenewalCancelled=contractRenewalCancelled+contract.getNetpayable();
					contractRenewalCancelledCount=contractRenewalCancelledCount+1;
				}else if(contract.isRenewFlag()){
					contractRenewalSingUpBalance=contractRenewalSingUpBalance+contract.getNetpayable();
					contractRenewalSingUpBalanceCount=contractRenewalSingUpBalanceCount+1;
				}else if(contract.isCustomerInterestFlag()){
					contractRenewalCancelled=contractRenewalCancelled+contract.getNetpayable();
					contractRenewalCancelledCount=contractRenewalCancelledCount+1;
				}else if(!contract.isRenewFlag()){
					contractRenewalinProcess=contractRenewalinProcess+contract.getNetpayable();
					contractRenewalinProcessCount=contractRenewalinProcessCount+1;
				}		
			}
			}
			/**
			 * Ends
			 */

			//Contract Renewal
			JSONObject jsonValues=new JSONObject();
			jsonValues.put("signUp", contractRenewalSingUpBalance);
			jsonValues.put("cancelled", contractRenewalCancelled);
			jsonValues.put("inProcess", contractRenewalinProcess);
			jsonObject.put("Contract Renewal Revenue",jsonValues);
			
			jsonValues=new JSONObject();
			jsonValues.put("signUp", contractRenewalSingUpBalanceCount);
			jsonValues.put("cancelled", contractRenewalCancelledCount);
			jsonValues.put("inProcess", contractRenewalinProcessCount);
			jsonObject.put("Contract Renewal",jsonValues);
			
		}
	}
}
		

		JSONArray jsonArray=new JSONArray();
		
		for (Employee employee : employeeList) {
			JSONObject jsonValues=new JSONObject();
			jsonValues.put("employeeName", employee.getFullname());
			jsonValues.put("employeeBranch", employee.getBranchName());
			jsonArray.add(jsonValues);
		}
	
		
		jsonObject.put("employeeList",jsonArray);
		
		/*
		 * Name: Apeksha Gunjal
		 * Date: 22/07/2018 @13:05 (Sunday)
		 * Note: InProcess data for all screens show in wrapper screen of EVA Priora
		 * 			which includes all data which are in inProcess / pending state 
		 * 			starting from beginning to till date.
		 */
		JSONObject inProcessDataOfWrapperScreen = null;
		try{
			inProcessDataOfWrapperScreen = getInprocessDataOfWrapperScreen(comp.getCompanyId(), toDateStr , roleAuth , employeeName);
		}catch(Exception e){
			logger.log(Level.SEVERE, "Error in creating inProcess data for wrapper screen: "+e.getMessage());
		}
		jsonObject.put("inProcessDataOfWrapperScreen", inProcessDataOfWrapperScreen);
		
		String jsonString=jsonObject.toString().replace("\\\\", "");
		logger.log(Level.SEVERE, jsonString);
		return jsonString;
	}

	@SuppressWarnings("unchecked")
	private String createJSONData(String loadType, String fromDateStr, String toDateStr, String employeeName, String role , String employeeGroup, String status, String customerCellNo, String customerEmailId, String apiCallfrom, String loadActiveContract, String customerId, String actionTask) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		DecimalFormat df = new DecimalFormat("0.00");
		
		JSONArray jArray = null;		
		
		if(loadType.equalsIgnoreCase("Lead")){
			try {
				jArray=new JSONArray();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Gson gson = new Gson();
			List<Lead> leadList = null;
			try {
				if(toDateStr.trim().length()>0){
					
					if(role.equalsIgnoreCase("ADMIN")){
						leadList = ofy().load().type(Lead.class).filter("companyId", comp.getCompanyId())
								.filter("creationDate >=",sdf.parse(fromDateStr))
								.filter("creationDate <",sdf.parse(toDateStr)).list();
					}else{
						leadList = ofy().load().type(Lead.class).filter("companyId", comp.getCompanyId())
								.filter("creationDate >=",sdf.parse(fromDateStr))
								.filter("creationDate <",sdf.parse(toDateStr))
								.filter("employee", employeeName).list();
					}
				}else{
					if(role.equalsIgnoreCase("ADMIN")){
						leadList = ofy().load().type(Lead.class).filter("companyId", comp.getCompanyId())
							.filter("creationDate <",sdf.parse(toDateStr)).list();
					}else{
						leadList = ofy().load().type(Lead.class).filter("companyId", comp.getCompanyId())
								.filter("creationDate <",sdf.parse(toDateStr))
								.filter("employee", employeeName).list();
					}
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			jArray = getLeadArray(leadList);
			/** date 2.2.2019 added by komal to sort list based on followupdate(Ascending)**/
//			Collections.sort(leadList ,getFollowupDateComparator());
//			for (int i = 0; i < leadList.size(); i++) {
//				JSONObject obj=new JSONObject();
//				obj.put("group", leadList.get(i).getGroup().trim());
//				obj.put("type", leadList.get(i).getType().trim());
//				obj.put("category", leadList.get(i).getCategory().trim());
//				obj.put("status", leadList.get(i).getStatus().trim());
//				obj.put("leadTitle", leadList.get(i).getTitle().trim());
//				obj.put("leadDate", sdf.format(leadList.get(i).getCreationDate()).trim());
//				obj.put("branch", leadList.get(i).getBranch().trim());
//				obj.put("priority", leadList.get(i).getPriority().trim());
//				obj.put("netPayable", leadList.get(i).getNetpayable()+"");
//				obj.put("employeeName", leadList.get(i).getEmployee().trim());
//				obj.put("leadId", leadList.get(i).getCount()+"");
//				obj.put("customerName", leadList.get(i).getPersonInfo().getFullName());
//				obj.put("customerMobileNo",leadList.get(i).getPersonInfo().getCellNumber()+"");
//				obj.put("custPocName", leadList.get(i).getPersonInfo().getPocName());
//				obj.put("customerId", leadList.get(i).getPersonInfo().getCount()+"");
//				obj.put("description", leadList.get(i).getDescription()+"");
//				if(leadList.get(i).getFollowUpDate()!=null){
//				obj.put("followUpDate", sdf.format(leadList.get(i).getFollowUpDate()));
//				
//				List<InteractionType> interactionTypeList=ofy().load().type(InteractionType.class).filter("interactionDocumentName","Lead").filter("interactionDocumentId",leadList.get(i).getCount()).list();
//				if(interactionTypeList.size()!=0){
//				ArrayList<InteractionType> communicationList=new ArrayList<InteractionType>();
//				if(interactionTypeList.size()!=0){
//					communicationList.addAll(interactionTypeList);
//				}
//				/**
//				 * Date : 14 March 2018 BY Rahul Verma
//				 */
//				Comparator<InteractionType> comp=new Comparator<InteractionType>() {
//					@Override
//					public int compare(InteractionType e1, InteractionType e2) {
////						Integer coutn1=o1.getCount();
////						Integer count2=o2.getCount();
////						return ;
//						if (e1.getCount() == e2.getCount()) {
//							return 0;
//						}
//						if (e1.getCount() < e2.getCount()) {
//							return 1;
//						} else {
//							return -1;
//						}
//					}
//				};
//				Collections.sort(communicationList,comp);
//				/**
//				 * End
//				 */
//				JSONArray jsonArray=new JSONArray();
//				boolean greaterThenFive=false;
//				int sizeOfCommunication=0;
//				if(communicationList.size()>5){
//					sizeOfCommunication=5;
//					greaterThenFive=true;
//				}else{
//					greaterThenFive=false;
//					sizeOfCommunication=communicationList.size();
//				}
//				sdf = new SimpleDateFormat("dd MMM yyyy");
//				sdf.setTimeZone(TimeZone.getTimeZone("IST"));
//				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
//				for (int j = 0; j < sizeOfCommunication; j++) {
//					JSONObject objInteractype=new JSONObject();
//					objInteractype.put("communicationId",communicationList.get(j).getCount());
//					objInteractype.put("communicationCreationDate",sdf.format(communicationList.get(j).getinteractionCreationDate()));
//					objInteractype.put("communicationTime",communicationList.get(j).getInteractionTime());
//					objInteractype.put("communicationUsername",communicationList.get(j).getCreatedBy());
//					objInteractype.put("communicationRemark",communicationList.get(j).getInteractionPurpose());
//					objInteractype.put("communicationDueDate",sdf.format(communicationList.get(j).getInteractionDueDate()));
//					objInteractype.put("communicationDueTime",communicationList.get(j).getInteractionDueTime());
//					objInteractype.put("greaterThenFive",greaterThenFive);
//					jsonArray.add(objInteractype);
//				}
//				obj.put("lastFiveCommunication", ""+jsonArray);
//				obj.put("lastCommunication",""+communicationList.get(communicationList.size()-1).getInteractionPurpose());
//				
//				}else{
//					obj.put("lastCommunication", "");
//					obj.put("lastFiveCommunication", "");
//				}
//				}
//				/*
//				 * Name: Apeksha Gunjal
//				 * Date: 10/10/2018 @ 18:08
//				 * Note: Repeated: Added unsuccessful remark and unsuccessful reason in json object.
//				 * This task is done 1-1.5 months before.. but tha is not reflecting now in git project
//				 * so adding again.
//				 */
//					
//				obj.put("unsuccessfulReason", leadList.get(i).getDocStatusReason());
//				obj.put("unsuccessfulRemark", leadList.get(i).getRemark());
//				/** date 16.4.2019 added by komal to seperate item and service product**/
//				boolean flag =true;
//				if(leadList.get(i).getLeadProducts() != null && leadList.get(i).getLeadProducts().size() > 0){
//					if(leadList.get(i).getLeadProducts().get(0).getPrduct() instanceof ServiceProduct){
//						flag = true;
//					}else{
//						flag = false;
//					}
//				}
//				obj.put("isServiceProduct", flag);
//				obj.put("productList",
//						gson.toJson(leadList.get(i).getLeadProducts())
//								.toString());
//				obj.put("uniqueId", leadList.get(i).getId());
//				jArray.add(obj);
//			}
		}else if(loadType.trim().equalsIgnoreCase("Quotation")){
			Gson gson = new Gson();
			try {
				jArray=new JSONArray();
				
				List<Quotation> quotationList= new ArrayList<Quotation>();
						
				if(role.equalsIgnoreCase("ADMIN")){		
					quotationList = ofy().load().type(Quotation.class).filter("companyId", comp.getCompanyId()).filter("creationDate >=",sdf.parse(fromDateStr)).filter("creationDate <",sdf.parse(toDateStr)).list();
						
				}else{
					quotationList = ofy().load().type(Quotation.class).filter("companyId", comp.getCompanyId()).filter("creationDate >=",sdf.parse(fromDateStr)).filter("creationDate <",sdf.parse(toDateStr))
							.filter("employee", employeeName).list();
				}
				for (int i = 0; i < quotationList.size(); i++) {
					JSONObject obj=new JSONObject();
					//Added by Apeksha Gunjal on 16/06/2018 16:10 to differentiate sales and service quotation.
					
					obj.put("quotationType", "serviceQuotation");
					obj.put("group", quotationList.get(i).getGroup().trim());
					obj.put("type", quotationList.get(i).getType().trim());
					obj.put("category", quotationList.get(i).getCategory().trim());
					obj.put("status", quotationList.get(i).getStatus().trim());
					obj.put("quotationDate", sdf.format(quotationList.get(i).getQuotationDate()));
					obj.put("branch",quotationList.get(i).getBranch().trim());
					obj.put("priority", quotationList.get(i).getPriority().trim());
					obj.put("netPayable", quotationList.get(i).getNetpayable()+"");
					obj.put("quotationId", quotationList.get(i).getCount()+"");
					obj.put("employeeName", quotationList.get(i).getEmployee().trim());
					obj.put("customerName", quotationList.get(i).getCinfo().getFullName().trim());
					obj.put("customerMobileNo",quotationList.get(i).getCinfo().getCellNumber()+"");
					obj.put("customerId", quotationList.get(i).getCinfo().getCount()+"");
					obj.put("customerPOC", quotationList.get(i).getCinfo().getPocName()+"");
					obj.put("approverName", quotationList.get(i).getApproverName());
					obj.put("productList",
							gson.toJson(quotationList.get(i).getItems())
									.toString());
					
					
					
					obj.put("uniqueId", quotationList.get(i).getId());
					if(quotationList.get(i).getFollowUpDate()!=null){
					obj.put("followUpDate", sdf.format(quotationList.get(i).getFollowUpDate()));
					List<InteractionType> interactionTypeList=ofy().load().type(InteractionType.class).filter("interactionDocumentName","Quotation").filter("interactionDocumentId",quotationList.get(i).getCount()).list();
					if(interactionTypeList.size()!=0){
					ArrayList<InteractionType> communicationList=new ArrayList<InteractionType>();
					if(interactionTypeList.size()!=0){
						communicationList.addAll(interactionTypeList);
					}
					/**
					 * Date : 14 March 2018 BY Rahul Verma
					 */
					Comparator<InteractionType> comp=new Comparator<InteractionType>() {
						@Override
						public int compare(InteractionType e1, InteractionType e2) {
//							Integer coutn1=o1.getCount();
//							Integer count2=o2.getCount();
//							return ;
							if (e1.getCount() == e2.getCount()) {
								return 0;
							}
							if (e1.getCount() < e2.getCount()) {
								return 1;
							} else {
								return -1;
							}
						}
					};
					Collections.sort(communicationList,comp);
					/**
					 * End
					 */
					JSONArray jsonArray=new JSONArray();
					boolean greaterThenFive=false;
					int sizeOfCommunication=0;
					if(communicationList.size()>5){
						sizeOfCommunication=5;
						greaterThenFive=true;
					}else{
						greaterThenFive=false;
						sizeOfCommunication=communicationList.size();
					}
					sdf = new SimpleDateFormat("dd MMM yyyy");
					sdf.setTimeZone(TimeZone.getTimeZone("IST"));
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));
					for (int j = 0; j < sizeOfCommunication; j++) {
						JSONObject objInteractype=new JSONObject();
						objInteractype.put("communicationId",communicationList.get(j).getCount());
						objInteractype.put("communicationCreationDate",sdf.format(communicationList.get(j).getinteractionCreationDate()));
						objInteractype.put("communicationTime",communicationList.get(j).getInteractionTime());
						objInteractype.put("communicationUsername",communicationList.get(j).getCreatedBy());
						objInteractype.put("communicationRemark",communicationList.get(j).getInteractionPurpose());
						objInteractype.put("communicationDueDate",sdf.format(communicationList.get(j).getInteractionDueDate()));
						objInteractype.put("communicationDueTime",communicationList.get(j).getInteractionDueTime());
						objInteractype.put("greaterThenFive",greaterThenFive);
						jsonArray.add(objInteractype);
					}
					obj.put("lastFiveCommunication", ""+jsonArray);
					obj.put("lastCommunication",""+communicationList.get(communicationList.size()-1).getInteractionPurpose());
					
					}else{
						obj.put("lastCommunication", "");
						obj.put("lastFiveCommunication", "");
					}
					}
					/*
					 * Name: Apeksha Gunjal
					 * Date: 01/09/2018 19:25
					 * Note: Add Quotation end date as a valid until date in android
					 * */
					try{
						obj.put("validUntil", sdf.format(quotationList.get(i).getValidUntill()));
					}catch(Exception e){
						logger.log(Level.SEVERE, "Error;validUntil: "+e);
						obj.put("validUntil", "");
					}
					jArray.add(obj);
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else if(loadType.trim().equalsIgnoreCase("Sales Quotation")){

			Gson gson = new Gson();
			jArray=new JSONArray();
			try {
				
				/*
				 * Name: Apeksha Gunjal
				 * Date: 16/06/2018 16:10
				 * Note: Add Sales Quotation and Service Quotation in one screen as a Quotation.
				 * */

				try{
					List<SalesQuotation> salesQuotationList = new ArrayList<SalesQuotation>();
					
					if(role.equalsIgnoreCase("ADMIN")){
						salesQuotationList = ofy().load()
							.type(SalesQuotation.class)
							.filter("companyId", comp.getCompanyId())
							.filter("creationDate >=",sdf.parse(fromDateStr))
							.filter("creationDate <",sdf.parse(toDateStr)).list();
					}else{
						salesQuotationList = ofy().load()
								.type(SalesQuotation.class)
								.filter("companyId", comp.getCompanyId())
								.filter("creationDate >=",sdf.parse(fromDateStr))
								.filter("creationDate <",sdf.parse(toDateStr))
								.filter("employee", employeeName).list();
					}
					
					for (int i = 0; i < salesQuotationList.size(); i++) {
						JSONObject obj = new JSONObject();
						obj.put("quotationType", "salesQuotation");
						obj.put("group", salesQuotationList.get(i).getGroup().trim());
						obj.put("type", salesQuotationList.get(i).getType().trim());
						obj.put("category", salesQuotationList.get(i).getCategory().trim());
						obj.put("status", salesQuotationList.get(i).getStatus().trim());
						obj.put("quotationDate", sdf.format(salesQuotationList.get(i).getQuotationDate()));
						obj.put("branch",salesQuotationList.get(i).getBranch().trim());
						obj.put("priority", salesQuotationList.get(i).getPriority().trim());
						obj.put("netPayable", salesQuotationList.get(i).getNetpayable()+"");
						obj.put("quotationId", salesQuotationList.get(i).getCount()+"");
						obj.put("employeeName", salesQuotationList.get(i).getEmployee().trim());
						obj.put("customerName", salesQuotationList.get(i).getCinfo().getFullName().trim());
						obj.put("customerMobileNo",salesQuotationList.get(i).getCinfo().getCellNumber()+"");
						obj.put("customerId", salesQuotationList.get(i).getCinfo().getCount()+"");
						obj.put("customerPOC", salesQuotationList.get(i).getCinfo().getPocName()+"");
						
						obj.put("productList",
								gson.toJson(salesQuotationList.get(i).getItems())
										.toString());
						obj.put("uniqueId", salesQuotationList.get(i).getId());
						if(salesQuotationList.get(i).getFollowUpDate()!=null){
						obj.put("followUpDate", sdf.format(salesQuotationList.get(i).getFollowUpDate()));
						List<InteractionType> interactionTypeList=ofy().load().type(InteractionType.class).filter("interactionDocumentName","SalesQuotation").filter("interactionDocumentId",salesQuotationList.get(i).getCount()).list();
						if(interactionTypeList.size()!=0){
						ArrayList<InteractionType> communicationList=new ArrayList<InteractionType>();
						if(interactionTypeList.size()!=0){
							communicationList.addAll(interactionTypeList);
						}
						
						Comparator<InteractionType> comp=new Comparator<InteractionType>() {
							@Override
							public int compare(InteractionType e1, InteractionType e2) {
	//							Integer coutn1=o1.getCount();
	//							Integer count2=o2.getCount();
	//							return ;
								if (e1.getCount() == e2.getCount()) {
									return 0;
								}
								if (e1.getCount() < e2.getCount()) {
									return 1;
								} else {
									return -1;
								}
							}
						};
						Collections.sort(communicationList,comp);
						/**
						 * End
						 */
						JSONArray jsonArray=new JSONArray();
						boolean greaterThenFive=false;
						int sizeOfCommunication=0;
						if(communicationList.size()>5){
							sizeOfCommunication=5;
							greaterThenFive=true;
						}else{
							greaterThenFive=false;
							sizeOfCommunication=communicationList.size();
						}
						sdf = new SimpleDateFormat("dd MMM yyyy");
						sdf.setTimeZone(TimeZone.getTimeZone("IST"));
						TimeZone.setDefault(TimeZone.getTimeZone("IST"));
						for (int j = 0; j < sizeOfCommunication; j++) {
							JSONObject objInteractype=new JSONObject();
							objInteractype.put("communicationId",communicationList.get(j).getCount());
							objInteractype.put("communicationCreationDate",sdf.format(communicationList.get(j).getinteractionCreationDate()));
							objInteractype.put("communicationTime",communicationList.get(j).getInteractionTime());
							objInteractype.put("communicationUsername",communicationList.get(j).getCreatedBy());
							objInteractype.put("communicationRemark",communicationList.get(j).getInteractionPurpose());
							objInteractype.put("communicationDueDate",sdf.format(communicationList.get(j).getInteractionDueDate()));
							objInteractype.put("communicationDueTime",communicationList.get(j).getInteractionDueTime());
							objInteractype.put("greaterThenFive",greaterThenFive);
							jsonArray.add(objInteractype);
						}
						obj.put("lastFiveCommunication", ""+jsonArray);
						obj.put("lastCommunication",""+communicationList.get(communicationList.size()-1).getInteractionPurpose());
						
						}else{
							obj.put("lastCommunication", "");
							obj.put("lastFiveCommunication", "");
						}
						}
						/*
						 * Name: Apeksha Gunjal
						 * Date: 01/09/2018 19:25
						 * Note: Add Sales Quotation end date as a valid until date in android
						 * */
						try{
							obj.put("validUntil", sdf.format(salesQuotationList.get(i).getValidUntill()));
						}catch(Exception e){
							logger.log(Level.SEVERE, "Error;validUntil: "+e);
							obj.put("validUntil", "");
						}
						jArray.add(obj);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
		}
		else if (loadType.trim().equalsIgnoreCase("Contract")) {
			
			try {
				jArray = new JSONArray();
				List<Contract> contractList = new ArrayList<Contract>();
				logger.log(Level.SEVERE, "apiCallfrom "+apiCallfrom);
				if(loadActiveContract!=null && !loadActiveContract.equals("") && loadActiveContract.equals(AppConstants.LOADACTIVECONTRACTSONLY)) {
						logger.log(Level.SEVERE, "inside Active contract condition ");
						int customerid = 0;
						try {
							customerid = Integer.parseInt(customerId);
						} catch (Exception e) {
							return "please send valid customer id";
						}
						if(customerid!=0) {
							Customer customerEntity = ofy().load().type(Customer.class).filter("count", customerid).filter("companyId",comp.getCompanyId()).first().now();
							if(customerEntity!=null) {
//								contractList = ofy().load().type(Contract.class)
//										.filter("companyId", comp.getCompanyId()).filter("endDate >=", new Date()).filter("cinfo.count", customerEntity.getCount())
//										.filter("status", Contract.APPROVED).list();
								
								List<Contract> customerwisecontractList = ofy().load().type(Contract.class)
										.filter("companyId", comp.getCompanyId()).filter("cinfo.count", customerEntity.getCount())
										.filter("status", Contract.APPROVED).list();
								logger.log(Level.SEVERE, "customerwisecontractList size "+customerwisecontractList.size());
								
								Date todaysDate = new Date();
								Calendar cal4=Calendar.getInstance();
								cal4.setTime(todaysDate);
								cal4.add(Calendar.DATE, 0);
								
								Date todayDate=null;
								
								try {
									todayDate=dateFormat.parse(dateFormat.format(cal4.getTime()));
								} catch (ParseException e) {
									e.printStackTrace();
								}
								logger.log(Level.SEVERE, "todaysDate"+todayDate);

								if(customerwisecontractList.size()>0){

									for(Contract contractEnity : customerwisecontractList){
										if(contractEnity.getEndDate().after(todayDate) || contractEnity.getEndDate().equals(todayDate)){
											contractList.add(contractEnity);
										}
									}
								}
								logger.log(Level.SEVERE, "For Active contract contractList size = "+contractList.size());
							}
							else {
								return "Customer does no exist";
							}
						}
						else {
							return "Customer id can not be zero";
						}
						
					
				}
				else {
					
					if(apiCallfrom==null || apiCallfrom.equals("")){
						if(role.equalsIgnoreCase("ADMIN")){
							contractList = ofy().load().type(Contract.class)
								.filter("companyId", comp.getCompanyId()).filter("startDate >=",sdf.parse(fromDateStr)).filter("startDate <",sdf.parse(toDateStr)).list();
						}else{
							contractList = ofy().load().type(Contract.class)
									.filter("companyId", comp.getCompanyId()).filter("startDate >=",sdf.parse(fromDateStr)).filter("startDate <",sdf.parse(toDateStr))
									.filter("employee", employeeName).list();
						}
					}
					else{
						/** 
						 * @author Vijay Date : 15-12-2021 
						 * Des :- This condition will execute this API will get call from Customer Portal
						 * to load specific customer Contract Details
						 */
						logger.log(Level.SEVERE, "Call from customer portal");
						if(apiCallfrom!=null && apiCallfrom.equals(AppConstants.CUSTOMERPORTAL)){
							String strcustomer = validateCustomerCelllNoEmailId(customerCellNo,customerEmailId); 
							Customer customer =null;
							ServerAppUtility apputility = new ServerAppUtility();

							if(strcustomer.equals("")){
								customer = loadCustomer(customerCellNo,customerEmailId);
							}
							else{
								return apputility.getMessageInJson("Customer cell no or email id does not exist or not match in ERP system!");
							}
							
							if(actionTask!=null && !actionTask.equals("") && actionTask.equals("ContractRenewal")){
								logger.log(Level.SEVERE, "For Renewal Data from contract");
								if(fromDateStr!=null && !fromDateStr.equals("") && toDateStr!=null && !toDateStr.equals("")){
//									.
									contractList = ofy().load().type(Contract.class)
											.filter("companyId", comp.getCompanyId()).filter("cinfo.count", customer.getCount()).list();
									logger.log(Level.SEVERE, "contractList size"+contractList.size());
//									.filter("endDate >=",sdf.parse(fromDateStr)).filter("endDate <=",sdf.parse(toDateStr))

								}
//								else{
//									contractList = ofy().load().type(Contract.class)
//											.filter("companyId", comp.getCompanyId()).filter("endDate >=", new Date()).filter("cinfo.count", customer.getCount()).list();
//									logger.log(Level.SEVERE, "contractList size = "+contractList.size());
//								}
								if(contractList!=null && contractList.size()>0) {
									Date fromDate = sdf.parse(fromDateStr);
									Date todate = sdf.parse(toDateStr);
									
									List<Contract> updatedContractlist = new ArrayList<Contract>();
									for(Contract contract : contractList) {
										if((contract.getEndDate().after(fromDate) && contract.getEndDate().before(todate)) || 
												((contract.getEndDate().equals(fromDate) || contract.getEndDate().equals(todate)))) {
											if(contract.isCustomerInterestFlag()==false && contract.isRenewFlag()==false) {
												updatedContractlist.add(contract);
											}
										}
										
									}
									contractList = updatedContractlist;
									logger.log(Level.SEVERE, "contractList size for renewal"+contractList.size());

								}
								
							}
							else{
								
								logger.log(Level.SEVERE, "For contract Data as per start Date");

								contractList = ofy().load().type(Contract.class).filter("companyId", comp.getCompanyId())
										.filter("cinfo.count", customer.getCount()).filter("status", Contract.APPROVED).list();
								logger.log(Level.SEVERE, "contractList size = "+contractList.size());
								
								List<Contract> updatedContractlist = new ArrayList<Contract>();
								if(fromDateStr!=null && !fromDateStr.equals("") && toDateStr!=null && !toDateStr.equals("")){
									Date fromDate = sdf.parse(fromDateStr);
									Date todate = sdf.parse(toDateStr);
									for(Contract contractEntity : contractList){
										logger.log(Level.SEVERE, "contractEntity.getStartDate()"+contractEntity.getStartDate());
										if((contractEntity.getStartDate().after(fromDate) && contractEntity.getStartDate().before(todate)) || (
												(fromDate.equals(contractEntity.getStartDate())|| todate.equals(contractEntity.getStartDate())))){
											updatedContractlist.add(contractEntity);
										}
									}
									contractList = updatedContractlist;
									logger.log(Level.SEVERE, "updatedContractlist size "+updatedContractlist.size());

								}
								

							}
							
								
						}
						/**
						 * ends here
						 */
					}
				}
				
				if(contractList.size()==0 && apiCallfrom!=null && apiCallfrom.equals(AppConstants.CUSTOMERPORTAL) ) {
					JSONObject obj = new JSONObject();
					obj.put("Message", "No data found");
					jArray.add(obj);

				}

				
				Gson gson = new Gson();
				JSONObject errorObj = new JSONObject();
				for (int i = 0; i < contractList.size(); i++) {
					JSONObject obj = new JSONObject();
					obj.put("group", contractList.get(i).getGroup().trim());
					obj.put("type", contractList.get(i).getType().trim());
					obj.put("category", contractList.get(i).getCategory()
							.trim());
					obj.put("status", contractList.get(i).getStatus().trim());
					obj.put("contractDate",
							sdf.format(contractList.get(i).getContractDate()));
					obj.put("branch", contractList.get(i).getBranch().trim());
					obj.put("priority", contractList.get(i).getPriority()
							.trim());
					obj.put("netPayable", contractList.get(i).getNetpayable()
							+ "");
					obj.put("contractId", contractList.get(i).getCount() + "");
					obj.put("employeeName", contractList.get(i).getEmployee()
							.trim());
					
					if (contractList.get(i).getCinfo().getFullName().contains("\"")) {
						errorObj = new JSONObject();
						errorObj.put("Message","Remove double quote from customer name. Customer id:"+ contractList.get(i).getCinfo().getCount()+". Contact system administrator.");
						String jsonString = errorObj.toJSONString();
						logger.log(Level.SEVERE,"error message sent for customer name "+ contractList.get(i).getCinfo().getFullName() + "Customer id:"
										+ contractList.get(i).getCinfo().getCount()+ " Contract id:"+contractList.get(i).getCount());
						return jsonString;
					}
					obj.put("customerName", contractList.get(i).getCinfo()
							.getFullName().trim());
					obj.put("customerMobileNo", contractList.get(i).getCinfo()
							.getCellNumber()
							+ "");
					obj.put("customerId", contractList.get(i).getCinfo()
							.getCount()
							+ "");
					
					if(apiCallfrom!=null && apiCallfrom.equals(AppConstants.CUSTOMERPORTAL)){
						obj.put("productList",
								gson.toJson(contractList.get(i).getItems())
										.toString());
						logger.log(
								Level.SEVERE,
								"productList::::"
										+ gson.toJson(
												contractList.get(i).getItems())
												.toString());					
					}else {

						//Ashwini Patil Date:18-04-2023
						JSONArray serviceProductarrayJson = new JSONArray();
						List<SalesLineItem> serviceProductList =contractList.get(i).getItems();
						for (int j = 0; j < serviceProductList.size(); j++) {
							JSONObject jobj = new JSONObject();
							jobj.put("id", serviceProductList.get(j).getPrduct().getCount());
							String productName=serviceProductList.get(j).getPrduct().getProductName();

							if (productName.contains("\"")) {
								errorObj = new JSONObject();
								errorObj.put("Message","Remove double quote from product name. Contract id:"+ contractList.get(i).getCount()+". Contact system administrator.");
								String jsonString = errorObj.toJSONString();
								logger.log(Level.SEVERE,"error message sent for product name "+ productName+ " Contract id:"+contractList.get(i).getCount());
								return jsonString;
							}
							jobj.put("productName",productName);
							jobj.put("price", serviceProductList.get(j).getPrduct().getPrice());
							jobj.put("noOfServices", serviceProductList.get(j).getNumberOfServices());
							jobj.put("productType",serviceProductList.get(j).getPrduct().getProductType());

							if (serviceProductList.get(j).getSpecification()!=null&&serviceProductList.get(j).getSpecification().contains("\"")) {
								errorObj = new JSONObject();
								errorObj.put("Message","Remove double quote from product specification. Contract id:"+ contractList.get(i).getCount()+". Contact system administrator.");
								String jsonString = errorObj.toJSONString();
								logger.log(Level.SEVERE,"error message sent for product specification "+ serviceProductList.get(j).getSpecification()+ " Contract id:"+contractList.get(i).getCount());
								return jsonString;
							}						
							jobj.put("specification",serviceProductList.get(j).getSpecification());
							
							jobj.put("specifications",serviceProductList.get(j).getSpecification());						
							jobj.put("duration",serviceProductList.get(j).getDuration());
							if(serviceProductList.get(j).getProductCategory()!=null)
								jobj.put("productCategory",serviceProductList.get(j).getProductCategory());
							else
								jobj.put("productCategory","");

							serviceProductarrayJson.add(jobj);
						}
						obj.put("productList",serviceProductarrayJson);
						
					}
					obj.put("totalNoOfServices", contractList.get(i).getItems()
							.size()
							+ "");
					
					if (contractList.get(i).getRemark()!=null&&contractList.get(i).getRemark().contains("\"")) {
						errorObj = new JSONObject();
						errorObj.put("Message","Remove double quote from Remark. Contract id:"+ contractList.get(i).getCount()+". Contact system administrator.");
						String jsonString = errorObj.toJSONString();
						logger.log(Level.SEVERE,"error message sent for remark "+ contractList.get(i).getRemark()+ " Contract id:"+contractList.get(i).getCount());
						return jsonString;
					}	
					obj.put("approveRejectionRemark", contractList.get(i)
							.getRemark() + "");
					obj.put("contrctStartDate",
							sdf.format(contractList.get(i).getStartDate()) + "");
					obj.put("contractEndDate",
							sdf.format(contractList.get(i).getEndDate()) + "");
					obj.put("approverName", contractList.get(i)
							.getApproverName() + "");
					obj.put("uniqueId", contractList.get(i).getId());
					jArray.add(obj);
				}
				

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if (loadType.trim().equalsIgnoreCase("Invoice Details")) {
			try {
				jArray = new JSONArray();
				List<Invoice> invoiceList = ofy().load().type(Invoice.class)
						.filter("companyId", comp.getCompanyId()).filter("invoiceDate >=",sdf.parse(fromDateStr)).filter("invoiceDate <",sdf.parse(toDateStr)).list();
				Gson gson = new Gson();
				for (int i = 0; i < invoiceList.size(); i++) {
					JSONObject obj = new JSONObject();
					obj.put("group", invoiceList.get(i).getGroup().trim());
					obj.put("type", invoiceList.get(i).getType().trim());
					obj.put("category", invoiceList.get(i).getCategory().trim());
					obj.put("status", invoiceList.get(i).getStatus().trim());
					obj.put("invoiceDate",
							sdf.format(invoiceList.get(i).getInvoiceDate()));
					obj.put("branch", invoiceList.get(i).getBranch().trim());
					obj.put("netPayable", invoiceList.get(i).getNetPayable()
							+ "");
					obj.put("invoiceId", invoiceList.get(i).getCount() + "");
					obj.put("customerName", invoiceList.get(i).getPersonInfo()
							.getFullName().trim());
					obj.put("customerMobileNo", invoiceList.get(i)
							.getPersonInfo().getCellNumber()
							+ "");
					obj.put("uniqueId", invoiceList.get(i).getId());
					obj.put("customerId", invoiceList.get(i).getPersonInfo()
							.getCount()
							+ "");
					obj.put("invoiceProductList",
							gson.toJson(
									invoiceList.get(i).getSalesOrderProducts())
									.toString());
					logger.log(
							Level.SEVERE,
							"productList::::"
									+ gson.toJson(
											invoiceList.get(i)
													.getSalesOrderProducts())
											.toString());
					obj.put("orderAmount", invoiceList.get(i)
							.getSalesOrderProducts().size()
							+ "");
					obj.put("orderId", invoiceList.get(i).getRemark() + "");
					obj.put("employeeName", invoiceList.get(i).getEmployee() + "");
					obj.put("approverName", invoiceList.get(i).getApproverName() + "");
					obj.put("productList",gson.toJson(invoiceList.get(i).getSalesOrderProducts())
									.toString());

					//Apeksha on 21/07/2018 17:30
					String linkToCall="";
					try{
						logger.log(Level.SEVERE,"Invoice:isMultipleOrderBilling: "+invoiceList.get(i).isMultipleOrderBilling());
						
						if(!invoiceList.get(i).isMultipleOrderBilling()){
							linkToCall="/slick_erp/serviceinvoiceprint"+"?Id="+invoiceList.get(i).getId()+"&"+"preprint="+"plane"+"&"+"type=SingleBilling";
						}else{
							linkToCall="/slick_erp/serviceinvoiceprint"+"?Id="+invoiceList.get(i).getId()+"&"+"preprint="+"plane"+"&"+"type=MultipleBilling";
						}
	
						obj.put("linkToCall", linkToCall);
					}catch(Exception e){
						e.printStackTrace();
						try{
							linkToCall="/slick_erp/serviceinvoiceprint"+"?Id="+invoiceList.get(i).getId()+"&"+"preprint="+"plane"+"&"+"type=SingleBilling";
							obj.put("linkToCall", linkToCall);
						}catch(Exception e1){
							e1.printStackTrace();
							obj.put("linkToCall", linkToCall);
						}
					}
					jArray.add(obj);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else if(loadType.trim().equalsIgnoreCase("Payment Details")){
			
//			try {
				jArray = new JSONArray();
				List<CustomerPayment> paymentList = null;
				if(apiCallfrom==null || apiCallfrom.equals("")){
					try {
						paymentList = ofy().load().type(CustomerPayment.class)
								.filter("companyId", comp.getCompanyId()).filter("paymentDate >=",sdf.parse(fromDateStr)).filter("paymentDate <",sdf.parse(toDateStr)).list();
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				/** 
				 * @author Vijay Date : 15-12-2021 
				 * Des :- This condition will execute this API will get call from Customer Portal
				 * to load specific customer Payment Details
				 */
				logger.log(Level.SEVERE, "Call from customer portal");
				ServerAppUtility apputility = new ServerAppUtility();

				if(apiCallfrom!=null && apiCallfrom.equals(AppConstants.CUSTOMERPORTAL)){

					String strcustomer = validateCustomerCelllNoEmailId(customerCellNo,customerEmailId); 
					Customer customer =null;
					if(strcustomer.equals("")){
						customer = loadCustomer(customerCellNo,customerEmailId);
					}
					else{
						return apputility.getMessageInJson("Customer cell no or email id does not exist or not match in ERP system!");
					}
					try {
						paymentList = ofy().load().type(CustomerPayment.class).filter("companyId", comp.getCompanyId())
								.filter("personInfo.count", customer.getCount()).list();
						logger.log(Level.SEVERE, "paymentList size = "+paymentList.size());
						
						List<CustomerPayment> updatedPaymentlist = new ArrayList<CustomerPayment>();
						if(fromDateStr!=null && !fromDateStr.equals("") && toDateStr!=null && !toDateStr.equals("")){
							logger.log(Level.SEVERE, "fromDateStr="+fromDateStr+" toDateStr="+toDateStr);
							logger.log(Level.SEVERE, "parsed fromDateStr="+sdf.parse(fromDateStr)+" parsed toDateStr="+sdf.parse(toDateStr));
							
							Date fromDate = sdf.parse(fromDateStr);
							Date todate = sdf.parse(toDateStr);
							
							for(CustomerPayment paymentEntity : paymentList){
								if(paymentEntity.getPaymentDate()!=null){
									if((paymentEntity.getPaymentDate().after(fromDate) && paymentEntity.getPaymentDate().before(todate)) || (
											(fromDate.equals(paymentEntity.getPaymentDate())|| todate.equals(paymentEntity.getPaymentDate())))){
										updatedPaymentlist.add(paymentEntity);
									}
								}
							}
							paymentList = updatedPaymentlist;
							logger.log(Level.SEVERE, "updatedPaymentlist size "+updatedPaymentlist.size());

						}
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					
						
				}
				/**
				 * ends here
				 */
				
				//Ashwini Patil Date:19-06-2023
				if(apiCallfrom!=null&&apiCallfrom.equals("CustomerPortal")) {
					if(paymentList!=null&&paymentList.size()>1) {
						Comparator<CustomerPayment> dateComp=new Comparator<CustomerPayment>() {
							@Override
							public int compare(CustomerPayment arg0, CustomerPayment arg1) {
								return arg0.getPaymentDate().compareTo(arg1.getPaymentDate());
							}
						};
						Collections.sort(paymentList, dateComp);
						
					}
				}
				
				
				Gson gson = new Gson();
				for (int i = 0; i < paymentList.size(); i++) {
					JSONObject obj = new JSONObject();
					obj.put("group", paymentList.get(i).getGroup().trim());
					obj.put("type", paymentList.get(i).getType().trim());
					obj.put("category", paymentList.get(i).getCategory()
							.trim());
					obj.put("status", paymentList.get(i).getStatus().trim());
					obj.put("paymentDate", sdf.format(paymentList.get(i).getPaymentDate()));
					obj.put("invoiceDate",
							sdf.format(paymentList.get(i).getInvoiceDate()));
					obj.put("uniqueId", paymentList.get(i).getId());
					obj.put("branch", paymentList.get(i).getBranch().trim());
					obj.put("paymentAmount", paymentList.get(i).getPaymentAmt());
					obj.put("amountRecieved", paymentList.get(i).getPaymentReceived());
					obj.put("paymentId", paymentList.get(i).getCount()+ "");
					obj.put("customerName", paymentList.get(i).getPersonInfo()
							.getFullName().trim());
					obj.put("customerMobileNo", paymentList.get(i).getPersonInfo()
							.getCellNumber()
							+ "");
					obj.put("customerId", paymentList.get(i).getPersonInfo()
							.getCount()
							+ "");
					obj.put("paymentProductList",
							gson.toJson(paymentList.get(i).getSalesOrderProducts())
									.toString());
					logger.log(
							Level.SEVERE,
							"productList::::"
									+ gson.toJson(
											paymentList.get(i).getSalesOrderProducts())
											.toString());
					obj.put("orderAmount", paymentList.get(i).getSalesOrderProducts()
							.size()
							+ "");
					obj.put("orderId", paymentList.get(i)
							.getRemark() + "");
					obj.put("employeeName", paymentList.get(i).getEmployee().trim()+"");
					obj.put("productList",gson.toJson(paymentList.get(i).getSalesOrderProducts())
							.toString());
					// added by Vijay for to download Invoice from customer portal
					obj.put("invoiceId",  paymentList.get(i).getInvoiceCount()+"");
					
					jArray.add(obj);
				}

				if(paymentList.size()==0 && apiCallfrom!=null && apiCallfrom.equals(AppConstants.CUSTOMERPORTAL)) {
					JSONObject obj = new JSONObject();
					obj.put("Message", "No data found");
					jArray.add(obj);
				}

//			} catch (Exception e) {
//				logger.log(Level.SEVERE,"Error in py");
//				e.printStackTrace();
//			}
		
		}else if(loadType.trim().equalsIgnoreCase("Customer Service")){
//			try{
				jArray=new JSONArray();
				List<Service> serviceList = null;
				
				ArrayList<String> branchList =  new ArrayList<String>();
				ArrayList<String> statusList =  new ArrayList<String>();
				boolean scheduleStatus = false;
				if(status != null && !status.equalsIgnoreCase("")){
					if(status.equalsIgnoreCase("UnPlanned")){
						statusList.add(Service.SERVICESTATUSOPEN);
						statusList.add(Service.SERVICESTATUSREOPEN);
						statusList.add(Service.SERVICESTATUSSCHEDULE);
						statusList.add(Service.SERVICESTATUSRESCHEDULE);
					}else if(status.equalsIgnoreCase("Scheduled")){
						statusList.add(Service.SERVICESTATUSPLANNED);
						statusList.add(Service.SERVICESTATUSSCHEDULE);
						statusList.add(Service.SERVICESTATUSRESCHEDULE);
						scheduleStatus = true;
					}else if(status.equalsIgnoreCase("Completed")){
						statusList.add(Service.SERVICESTATUSCOMPLETED);
						statusList.add(Service.SERVICESTATUSTECHCOMPLETED);
						boolean flag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MarkServiceScheduledStatusTrue", comp.getCompanyId());
						if(flag){
							scheduleStatus = true;
						}
					}else if(status.equalsIgnoreCase("InProcess")){
						statusList.add(Service.SERVICESTATUSSTARTED);
						statusList.add(Service.SERVICESTATUSREPORTED);
						statusList.add(Service.SERVICETCOMPLETED);
						scheduleStatus = true;
					}
				}
				if(employeeGroup != null && !employeeGroup.equals("") && employeeGroup.equalsIgnoreCase("Manager")){
					
						Employee employee=ofy().load().type(Employee.class).filter("companyId", comp.getCompanyId()).filter("fullname", employeeName).first().now();
						if(employee != null){
							branchList.add(employee.getBranchName());
						}
						if(employee != null && employee.getEmpBranchList() != null &&  employee.getEmpBranchList() .size() > 0){
							for(EmployeeBranch b : employee.getEmpBranchList()){
								branchList.add(b.getBranchName());
							}
						}
					try {
						if(branchList.size() > 0){
							if(statusList != null && statusList.size() > 0){
								serviceList = ofy().load().type(Service.class)
										.filter("companyId", comp.getCompanyId()).filter("serviceDate >=",sdf.parse(fromDateStr)).filter("serviceDate <",sdf.parse(toDateStr))
										.filter("branch IN", branchList).filter("status IN", statusList).
										filter("isServiceScheduled", scheduleStatus).list();
							}else{
								serviceList = ofy().load().type(Service.class)
									.filter("companyId", comp.getCompanyId()).filter("serviceDate >=",sdf.parse(fromDateStr)).filter("serviceDate <",sdf.parse(toDateStr))
									.filter("branch IN", branchList).list();
							}
						}else{
							if(statusList != null && statusList.size() > 0){
								serviceList = ofy().load().type(Service.class)
									.filter("companyId", comp.getCompanyId()).filter("serviceDate >=",sdf.parse(fromDateStr)).filter("serviceDate <",sdf.parse(toDateStr))
									.filter("status IN", statusList).
									filter("isServiceScheduled", scheduleStatus).list();
							}else{
								serviceList = ofy().load().type(Service.class)
									.filter("companyId", comp.getCompanyId()).filter("serviceDate >=",sdf.parse(fromDateStr)).filter("serviceDate <",sdf.parse(toDateStr))
									.list();
							}
						}
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					
					
					try {
						
						/**
						 * @author Vijay Date :- 01-07-2021
						 * Des :- As per nitin sir instruction if user role is Admin or Scheduler then services will show 
						 * as per brances assigned to Employee. if branches not assigned then else condition will execute to show all services
						 * as per old code
						 */
						Employee employeeEntity=ofy().load().type(Employee.class).filter("companyId", comp.getCompanyId()).filter("fullname", employeeName).first().now();
						logger.log(Level.SEVERE,"employeeEntity"+employeeEntity);

						User userEntity = ofy().load().type(User.class).filter("employeeName", employeeName).filter("companyId", comp.getCompanyId()).first().now();
											
								
						if(userEntity!=null && userEntity.getRole()!=null && (userEntity.getRole().getRoleName().equalsIgnoreCase("ADMIN") || userEntity.getRole().getRoleName().equalsIgnoreCase("Scheduler")) && employeeEntity!=null && employeeEntity.getRoleName()!=null && employeeEntity.getRoleName()!=null &&  employeeEntity.getEmpBranchList()!=null && employeeEntity.getEmpBranchList().size()>0) {
							logger.log(Level.SEVERE,"Inside Admin or shceduler for branches assigned accessable ");

							ArrayList<EmployeeBranch> employeeBranchlist = new ArrayList<EmployeeBranch>();
							ArrayList<String> strEmpBranchlist = new ArrayList<String>();
							
							if(employeeEntity.getEmpBranchList()!=null)
								employeeBranchlist.addAll(employeeEntity.getEmpBranchList());
								
							
							HashSet<EmployeeBranch> hsempBranch = new HashSet<EmployeeBranch>(employeeBranchlist);
							ArrayList<EmployeeBranch> empBranchlist = new ArrayList<EmployeeBranch>(hsempBranch);
							logger.log(Level.SEVERE,"empBranchlist size "+empBranchlist.size());
							
						
							for(EmployeeBranch branch : empBranchlist) {
								if(branch.getBranchName()!=null) {
									strEmpBranchlist.add(branch.getBranchName());
								}
							}
							
							if(statusList != null && statusList.size() > 0){
								serviceList = ofy().load().type(Service.class)
										.filter("companyId", comp.getCompanyId()).filter("serviceDate >=",sdf.parse(fromDateStr)).filter("serviceDate <",sdf.parse(toDateStr))
										.filter("status IN", statusList).filter("branch IN", strEmpBranchlist).
										filter("isServiceScheduled", scheduleStatus).list();
							}else{
							serviceList = ofy().load().type(Service.class)
									.filter("companyId", comp.getCompanyId()).filter("branch IN", strEmpBranchlist).filter("serviceDate >=",sdf.parse(fromDateStr)).filter("serviceDate <",sdf.parse(toDateStr)).list();
							}
							logger.log(Level.SEVERE,"serviceList size "+serviceList.size());

						}
						else {
							if(apiCallfrom != null)
								logger.log(Level.SEVERE, "apiCallfrom "+apiCallfrom);
							
							if(apiCallfrom == null || apiCallfrom.equals("")){
								if(statusList != null && statusList.size() > 0){
									serviceList = ofy().load().type(Service.class)
											.filter("companyId", comp.getCompanyId()).filter("serviceDate >=",sdf.parse(fromDateStr)).filter("serviceDate <",sdf.parse(toDateStr))
											.filter("status IN", statusList).
											filter("isServiceScheduled", scheduleStatus).list();
								}else{
								serviceList = ofy().load().type(Service.class)
										.filter("companyId", comp.getCompanyId()).filter("serviceDate >=",sdf.parse(fromDateStr)).filter("serviceDate <",sdf.parse(toDateStr)).list();
								}
							}
							else{
								
								/** 
								 * @author Vijay Date : 10-12-2021 
								 * Des :- This condition will execute this API will get call from Customer Portal
								 * to load specific customer service details
								 */
								ServerAppUtility utility = new ServerAppUtility();

								if(apiCallfrom!=null && apiCallfrom.equals(AppConstants.CUSTOMERPORTAL)){
									String strcustomer = validateCustomerCelllNoEmailId(customerCellNo,customerEmailId); 
									Customer customer =null;

									if(strcustomer.equals("")){
										customer = loadCustomer(customerCellNo,customerEmailId);
									}
									else{
										return utility.getMessageInJson("Customer cell no or email id does not exist or not match in ERP system!");
									}
									if(fromDateStr!=null && !fromDateStr.equals("") && toDateStr!=null && !toDateStr.equals("")){
										logger.log(Level.SEVERE, "fromDateStr="+fromDateStr+" toDateStr="+toDateStr);
										logger.log(Level.SEVERE, "parsed fromDateStr="+sdf.parse(fromDateStr)+" parsed toDateStr="+sdf.parse(toDateStr));
										serviceList = ofy().load().type(Service.class)
												.filter("companyId", comp.getCompanyId()).filter("serviceDate >=",sdf.parse(fromDateStr)).filter("serviceDate <=",sdf.parse(toDateStr))
												.filter("personInfo.count", customer.getCount()).list();
									}
									else{
										serviceList = ofy().load().type(Service.class)
												.filter("companyId", comp.getCompanyId())
												.filter("personInfo.count", customer.getCount()) .list();
									}
									if(serviceList!=null)
										logger.log(Level.SEVERE, "serviceList size"+serviceList.size());
								}
								/**
								 * ends here
								 */
								
							}
							
							
								
							
							
						}
						
//						if(statusList != null && statusList.size() > 0){
//							serviceList = ofy().load().type(Service.class)
//									.filter("companyId", comp.getCompanyId()).filter("serviceDate >=",sdf.parse(fromDateStr)).filter("serviceDate <",sdf.parse(toDateStr))
//									.filter("status IN", statusList).
//									filter("isServiceScheduled", scheduleStatus).list();
//						}else{
//						serviceList = ofy().load().type(Service.class)
//								.filter("companyId", comp.getCompanyId()).filter("serviceDate >=",sdf.parse(fromDateStr)).filter("serviceDate <",sdf.parse(toDateStr)).list();
//						}
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				//Ashwini Patil Date:19-06-2023
				if(apiCallfrom!=null&&apiCallfrom.equals("CustomerPortal")) {
					if(serviceList!=null&&serviceList.size()>1) {
					Comparator<Service> dateComp=new Comparator<Service>() {
						@Override
						public int compare(Service arg0, Service arg1) {
							return arg0.getServiceDate().compareTo(arg1.getServiceDate());
						}
					};
					Collections.sort(serviceList, dateComp);	
					}
				}
				
				Gson gson = new Gson();
				jArray = getServiceArray(serviceList,apiCallfrom,comp);
//				for (Service service : serviceList) {
//					JSONObject obj = new JSONObject();
//					obj.put("group", service.getGroup().trim());
//					obj.put("type", service.getType().trim());
//					obj.put("category", service.getCategory().trim());
//					obj.put("status", service.getStatus().trim());
//					obj.put("serviceId", service.getCount());
//					obj.put("contractId", service.getContractCount());
//					if(service.getContractStartDate()!=null){
//					obj.put("contractStartDate", sdf.format(service.getContractStartDate()));
//					}
//					obj.put("contractEndDate", sdf.format(service.getContractEndDate()));
//					obj.put("customerId", service.getCustomerId());
//					obj.put("customerName", service.getCustomerName());
//					obj.put("customerMobNo", service.getPersonInfo().getCellNumber());
//					obj.put("servicingTimeInHrs", service.getServicingTime());
//					obj.put("productId", service.getProduct().getCount());
//					obj.put("productCode", service.getProduct().getProductCode().trim());
//					obj.put("productName", service.getProduct().getProductName().trim());
//					obj.put("serviceSrNo", service.getServiceSerialNo());
//					obj.put("serviceEngineer",service.getEmployee().trim());
//					obj.put("branch", service.getBranch().trim());
//					obj.put("serviceDate", sdf.format(service.getServiceDate()));
//					obj.put("serviceDay", service.getServiceDay().trim());
//					obj.put("serviceTime", service.getServiceTime().trim());
//					obj.put("serviceBranch", service.getServiceBranch().trim());
//					obj.put("serviceType", service.getServiceType().trim());
//					if(service.getServiceCompletionDate()!=null){
//						obj.put("completionDate", sdf.format(service.getServiceCompletionDate()));
//					}else{
//						obj.put("completionDate","");
//					}
//					obj.put("completionDuration", service.getServiceCompleteDuration());
//					obj.put("completionRemark", service.getServiceCompleteRemark());
//					obj.put("customerFeedback", service.getCustomerFeedback());
//					obj.put("team", service.getTeam());
//					obj.put("premisesDetails", service.getPremises());
//					obj.put("quantity", service.getQuantity());
//					obj.put("serviceValue", df.format(service.getServiceValue()));
//					obj.put("serviceAddress", service.getAddress().getCompleteAddress());
//					obj.put("uniqueId", service.getId());
//					obj.put("locality", service.getAddress().getLocality());
//					//Added by Apeksha Gunjal on 20/09/2018 @ 19:44. Added service number.
//					obj.put("serviceNumber", service.getServiceSrNo());
//					//Added by Apeksha Gunjal on 21/09/2018 @ 13:10. Added service remark.
//					obj.put("remark", service.getRemark());
//					JSONArray statusArray = new JSONArray();
//					JSONObject statusObject = null;
//					Map<String , TrackTableDetails> trackMap = new HashMap<String , TrackTableDetails>();
//					for(TrackTableDetails trackDetails : service.getTrackServiceTabledetails()){
//						if(trackMap.containsKey(trackDetails.getStatus())){
//							if(trackMap.get(trackDetails.getStatus()).getSrNo() 
//									< trackDetails.getSrNo()){
//								trackMap.put(trackDetails.getStatus(), trackDetails);
//							}
//						}else{
//							trackMap.put(trackDetails.getStatus(), trackDetails);
//						}
//					}
//					for(Map.Entry<String, TrackTableDetails> entry : trackMap.entrySet()){
//						TrackTableDetails trackDetails = entry.getValue();
//						statusObject= new JSONObject();
//						statusObject.put("dateTime", trackDetails.getDate_time());
//						statusObject.put("status", trackDetails.getStatus());
//						statusObject.put("location" , trackDetails.getLocationName());
//						statusObject.put("latitude" , trackDetails.getLatitude());
//						statusObject.put("longitude" , trackDetails.getLongitude());
//						statusObject.put("km" , trackDetails.getKiloMeters());
//						statusObject.put("time" , trackDetails.getTime());
//						statusArray.add(statusObject);
//					}
//					obj.put("trackingDetais", statusArray);
//					jArray.add(obj);
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//				logger.log(Level.SEVERE,"Error in customer Service");
//			}
		}else if(loadType.trim().equalsIgnoreCase("Sales Order")){
			jArray=new JSONArray();
			List<SalesOrder> salesOrderList = null;
			try {
				
				if(role.equalsIgnoreCase("ADMIN")){
					salesOrderList = ofy().load().type(SalesOrder.class)
							.filter("companyId", comp.getCompanyId()).filter("creationDate >=",sdf.parse(fromDateStr)).filter("creationDate <",sdf.parse(toDateStr)).list();
				}else{
					salesOrderList = ofy().load().type(SalesOrder.class)
							.filter("companyId", comp.getCompanyId()).filter("creationDate >=",sdf.parse(fromDateStr)).filter("creationDate <",sdf.parse(toDateStr))
							.filter("employee", employeeName).list();
				}
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Gson gson = new Gson();
			for (SalesOrder salesOrder : salesOrderList) {
				JSONObject obj = new JSONObject();
				obj.put("group", salesOrder.getGroup().trim());
				obj.put("type", salesOrder.getType().trim());
				obj.put("category", salesOrder.getCategory().trim());
				obj.put("status", salesOrder.getStatus().trim());
				obj.put("salesOrderId", salesOrder.getCount());
				obj.put("customerId", salesOrder.getCinfo().getCount());
				obj.put("customerName", salesOrder.getCinfo().getFullName());
				obj.put("customerCell", salesOrder.getCinfo().getCellNumber());
				obj.put("salesOrderDate", sdf.format(salesOrder.getSalesOrderDate()));
				obj.put("branch", salesOrder.getBranch());
				obj.put("employeeName", salesOrder.getEmployee());
				obj.put("approverName", salesOrder.getApproverName());
				obj.put("paymentMethods", salesOrder.getPaymentMethod());
				obj.put("productArray", gson.toJson(salesOrder.getItems()));
				obj.put("deliveryDate", sdf.format(salesOrder.getDeliveryDate()));
				obj.put("isShippingAddressDifferent", salesOrder.getCheckAddress());
				obj.put("shippingAddress",salesOrder.getShippingAddress().getCompleteAddress());
				obj.put("netPayable", salesOrder.getNetpayable());
				obj.put("uniqueId", salesOrder.getId());
				
				jArray.add(obj);
			}
		}else if(loadType.trim().equalsIgnoreCase("Contract Renewal")){
			try {
				jArray = new JSONArray();
				List<Contract> contractList = ofy().load().type(Contract.class)
						.filter("companyId", comp.getCompanyId()).filter("endDate >=",sdf.parse(fromDateStr)).filter("endDate <",sdf.parse(toDateStr)).list();
				Gson gson = new Gson();
				for (int i = 0; i < contractList.size(); i++) {
					JSONObject obj = new JSONObject();
					obj.put("group", contractList.get(i).getGroup().trim());
					obj.put("type", contractList.get(i).getType().trim());
					obj.put("category", contractList.get(i).getCategory()
							.trim());
					obj.put("status", contractList.get(i).getStatus().trim());
					obj.put("contractDate",
							sdf.format(contractList.get(i).getContractDate()));
					obj.put("branch", contractList.get(i).getBranch().trim());
					obj.put("priority", contractList.get(i).getPriority()
							.trim());
					obj.put("netPayable", contractList.get(i).getNetpayable()
							+ "");
					obj.put("contractId", contractList.get(i).getCount() + "");
					obj.put("employeeName", contractList.get(i).getEmployee()
							.trim());
					obj.put("customerName", contractList.get(i).getCinfo()
							.getFullName().trim());
					obj.put("customerMobileNo", contractList.get(i).getCinfo()
							.getCellNumber()
							+ "");
					obj.put("customerId", contractList.get(i).getCinfo()
							.getCount()
							+ "");
					obj.put("productList",
							gson.toJson(contractList.get(i).getItems())
									.toString());
					logger.log(
							Level.SEVERE,
							"productList::::"
									+ gson.toJson(
											contractList.get(i).getItems())
											.toString());
					obj.put("totalNoOfServices", contractList.get(i).getItems()
							.size()
							+ "");
					obj.put("approveRejectionRemark", contractList.get(i)
							.getRemark() + "");
					obj.put("contrctStartDate",
							sdf.format(contractList.get(i).getStartDate()) + "");
					obj.put("contractEndDate",
							sdf.format(contractList.get(i).getEndDate()) + "");
					obj.put("approverName", contractList.get(i)
							.getApproverName() + "");
					obj.put("uniqueId", contractList.get(i).getId());
					obj.put("doNotRenew", contractList.get(i).isCustomerInterestFlag());
					obj.put("isRenew",contractList.get(i).isRenewFlag());
					
					jArray.add(obj);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}else if(loadType.equalsIgnoreCase("Complain Dashboard")){

			List<Complain> complainList = null;
			if(apiCallfrom==null || apiCallfrom.equals("")){
				try {
					complainList = ofy().load().type(Complain.class).filter("companyId",comp.getCompanyId()).filter("complainDate >=",sdf.parse(fromDateStr)).filter("complainDate <",sdf.parse(toDateStr)).list();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else{
				
				/** 
				 * @author Vijay Date : 17-12-2021 
				 * Des :- This condition will execute this API will get call from Customer Portal
				 * to load specific customer complaint details
				 */
				if(apiCallfrom!=null && apiCallfrom.equals(AppConstants.CUSTOMERPORTAL)){
					logger.log(Level.SEVERE, "Inside Customer Portal call");
					ServerAppUtility apputility = new ServerAppUtility();

					String strcustomer = validateCustomerCelllNoEmailId(customerCellNo,customerEmailId); 
					Customer customer =null;

					if(strcustomer.equals("")){
						customer = loadCustomer(customerCellNo,customerEmailId);
					}
					else{
						return apputility.getMessageInJson("Customer cell no or email id does not exist or not match in ERP system!");
					}
					try {
						if(fromDateStr!=null && !fromDateStr.equals("") && toDateStr!=null && !toDateStr.equals("")){
							complainList = ofy().load().type(Complain.class).filter("companyId",comp.getCompanyId()).filter("personinfo.count", customer.getCount()).list();
//							.filter("complainDate >=",sdf.parse(fromDateStr))
//							.filter("complainDate <=",sdf.parse(toDateStr)).
							if(complainList!=null)
							logger.log(Level.SEVERE, "complainList size "+complainList.size());
							logger.log(Level.SEVERE, "fromDateStr="+fromDateStr+" toDateStr="+toDateStr);
							logger.log(Level.SEVERE, "parsed fromDateStr="+sdf.parse(fromDateStr)+" parsed toDateStr="+sdf.parse(toDateStr));
							
							Date fromDate = sdf.parse(fromDateStr);
							Date todate = sdf.parse(toDateStr);
							
							ArrayList<Complain> updatedcomplainlist = new ArrayList<Complain>();
							for(Complain complainEntity : complainList) {
								logger.log(Level.SEVERE, "complain date "+complainList.size());
								
								if((complainEntity.getComplainDate().after(fromDate) && complainEntity.getComplainDate().before(todate)) || 
										((complainEntity.getComplainDate().equals(fromDate) || complainEntity.getComplainDate().equals(todate)))) {
									updatedcomplainlist.add(complainEntity);
								}
							}
							complainList = updatedcomplainlist;
						}
						else{
							complainList = ofy().load().type(Complain.class).filter("companyId",comp.getCompanyId())
									.filter("personinfo.count", customer.getCount()).list();
						}
						logger.log(Level.SEVERE, "complainList size "+complainList.size());

					} catch (Exception e) {
						e.printStackTrace();
					}
					
						
				}
				
				
				
				/**
				 * ends here
				 */
			}
			
			/*
			 * Name: Apeksha Gunjal
			 * Date: 27/07/2018 @16:20 (Friday)
			 * Note: Add Complains in JsonArray.
			 */
//			Gson gson=new Gson();
			
			
			//Ashwini Patil Date:19-06-2023
			if(apiCallfrom!=null&&apiCallfrom.equals("CustomerPortal")) {
				if(complainList!=null&&complainList.size()>1) {
					Comparator<Complain> dateComp=new Comparator<Complain>() {
						@Override
						public int compare(Complain arg0, Complain arg1) {
							return arg0.getComplainDate().compareTo(arg1.getComplainDate());
						}
					};
					Collections.sort(complainList, dateComp);
					
				}
			}
			
			
			JSONArray array =  getComplainJsonArray(complainList,apiCallfrom);
			String jsonStr = array.toString().replaceAll("\\\\", "");
					//gson.toJson(complainList).replaceAll("\\\\", "");
			
			return jsonStr;
		}else if(loadType.equalsIgnoreCase("Overdue Service")){
			/*
			 * Date: 21/05/2018 
			 * Apeksha Gunjal 
			 * Added Overdue Services.
			 */
			jArray=new JSONArray();
			List<Service> serviceList = null;
			ArrayList<String> statuses = new ArrayList<>();
			statuses.add(Service.SERVICESTATUSSCHEDULE);
			statuses.add(Service.SERVICESTATUSRESCHEDULE);

			logger.log(Level.SEVERE, "fromDateStr: " + fromDateStr+
					" toDateStr: "+toDateStr);
			logger.log(Level.SEVERE, "companyId: " + comp.getCompanyId());
			logger.log(Level.SEVERE, "statuses: " + statuses);
			try {
				serviceList = ofy().load().type(Service.class)
						.filter("companyId", comp.getCompanyId())
						.filter("status IN", statuses)
						.filter("serviceDate >=",sdf.parse(fromDateStr))//FirstDate of month
						.filter("serviceDate <",sdf.parse(toDateStr)).list();//today's date

				

				logger.log(Level.SEVERE, "serviceList size: " + serviceList.size());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log(Level.SEVERE, "overdue error: " + e.getMessage());
			}
			Gson gson = new Gson();
			for (Service service : serviceList) {
				JSONObject obj = new JSONObject();
				obj.put("group", service.getGroup().trim());
				obj.put("type", service.getType().trim());
				obj.put("category", service.getCategory().trim());
				obj.put("status", service.getStatus().trim());
				obj.put("serviceId", service.getCount());
				obj.put("contractId", service.getContractCount());
				try{
					if(service.getContractStartDate()!=null){
					obj.put("contractStartDate", sdf.format(service.getContractStartDate()));
					}
					obj.put("contractEndDate", sdf.format(service.getContractEndDate()));
				}catch(Exception e){
					logger.log(Level.SEVERE, "contractStartDate error: " +
							e.getMessage());
					if(service.getContractStartDate()!=null){
						obj.put("contractStartDate", service.getContractStartDate());
						}
						obj.put("contractEndDate", service.getContractEndDate());
				}
				obj.put("customerId", service.getCustomerId());
				obj.put("customerName", service.getCustomerName());
				obj.put("customerMobNo", service.getPersonInfo().getCellNumber());
				obj.put("servicingTimeInHrs", service.getServicingTime());
				obj.put("productId", service.getProduct().getCount());
				obj.put("productCode", service.getProduct().getProductCode().trim());
				obj.put("productName", service.getProduct().getProductName().trim());
				obj.put("serviceSrNo", service.getServiceSerialNo());
				obj.put("serviceEngineer",service.getEmployee().trim());
				obj.put("branch", service.getBranch().trim());
				obj.put("serviceDate", sdf.format(service.getServiceDate()));
				obj.put("serviceDay", service.getServiceDay().trim());
				obj.put("serviceTime", service.getServiceTime().trim());
				obj.put("serviceBranch", service.getServiceBranch().trim());
				obj.put("serviceType", service.getServiceType().trim());
				if(service.getServiceCompletionDate()!=null){
					obj.put("completionDate", sdf.format(service.getServiceCompletionDate()));
				}else{
					obj.put("completionDate","");
				}
				obj.put("completionDuration", service.getServiceCompleteDuration());
				obj.put("completionRemark", service.getServiceCompleteRemark());
				obj.put("customerFeedback", service.getCustomerFeedback());
				obj.put("team", service.getTeam());
				obj.put("premisesDetails", service.getPremises());
				obj.put("quantity", service.getQuantity());
				obj.put("serviceValue", service.getServiceValue());
				obj.put("serviceAddress", service.getAddress().getCompleteAddress());
				obj.put("uniqueId", service.getId());
				obj.put("locality", service.getAddress().getLocality());

				jArray.add(obj);
			}
		}else if (loadType.equalsIgnoreCase("InprocessLead")){
			/*
			 * Name: Apeksha Gunjal
			 * Date: 23/07/2018 @15:05 (Monday)
			 * Note: InProcess data of Lead which includes leads which are in inProcess / pending state 
			 * 			starting from beginning to till date.
			 */
			try{
				jArray = getInProcessLeads(fromDateStr, toDateStr, employeeName , role);
			}catch(Exception e){
				logger.log(Level.SEVERE, "Error in getInProcessLeads: "+e.getMessage());
				jArray = new JSONArray();
			}
		}else if (loadType.equalsIgnoreCase("InprocessQuotation")){
			/*
			 * Name: Apeksha Gunjal
			 * Date: 23/07/2018 @15:05 (Monday)
			 * Note: InProcess data of Quotation which includes quotations which are in inProcess / pending state 
			 * 			starting from beginning to till date.
			 */
			try{
				jArray = getInProcessQuotation(fromDateStr, toDateStr, employeeName, role);
			}catch(Exception e){
				logger.log(Level.SEVERE, "Error in getInProcessQuotation: "+e.getMessage());
				jArray = new JSONArray();
			}
		}else if (loadType.equalsIgnoreCase("InprocessContract")){
			/*
			 * Name: Apeksha Gunjal
			 * Date: 24/07/2018 @15:05 (Tuesday)
			 * Note: InProcess data of Contract which includes contracts which are in inProcess / pending state 
			 * 			starting from beginning to till date.
			 */
			try{
				jArray = getInProcessContract(fromDateStr, toDateStr , employeeName , role);
			}catch(Exception e){
				logger.log(Level.SEVERE, "Error in getInProcessContract: "+e.getMessage());
				jArray = new JSONArray();
			}
		}else if (loadType.equalsIgnoreCase("InprocessContractRenewal")){
			/*
			 * Name: Apeksha Gunjal
			 * Date: 24/07/2018 @15:05 (Monday)
			 * Note: InProcess data of ContractRenewal which includes contractRenewals which are in inProcess / pending state 
			 * 			starting from beginning to till date.
			 */
			try{
				jArray = getInProcessContractRenewal(fromDateStr, toDateStr);
			}catch(Exception e){
				logger.log(Level.SEVERE, "Error in getInProcessContractRenewal: "+e.getMessage());
				jArray = new JSONArray();
			}
		}else if (loadType.equalsIgnoreCase("InprocessSalesOrder")){
			/*
			 * Name: Apeksha Gunjal
			 * Date: 24/07/2018 @15:05 (Tuesday)
			 * Note: InProcess data of Sales Order which includes salesOrders which are in inProcess / pending state 
			 * 			starting from beginning to till date.
			 */
			try{
				jArray = getInProcessSalesOrder(fromDateStr, toDateStr , employeeName ,role);
			}catch(Exception e){
				logger.log(Level.SEVERE, "Error in getInProcessSalesOrder: "+e.getMessage());
				jArray = new JSONArray();
			}
		}else if (loadType.equalsIgnoreCase("InprocessCustomerService")){
			/*
			 * Name: Apeksha Gunjal
			 * Date: 24/07/2018 @15:05 (Tuesday)
			 * Note: InProcess data of Service which includes services which are in inProcess / pending state 
			 * 			starting from beginning to till date.
			 */
			try{
				jArray = getInProcessService(fromDateStr, toDateStr);
			}catch(Exception e){
				logger.log(Level.SEVERE, "Error in getInProcessService: "+e.getMessage());
				jArray = new JSONArray();
			}
		}else if (loadType.equalsIgnoreCase("InprocessInvoice")){
			/*
			 * Name: Apeksha Gunjal
			 * Date: 24/07/2018 @15:05 (Tuesday)
			 * Note: InProcess data of Invoice which includes Invoices which are in inProcess / pending state 
			 * 			starting from beginning to till date.
			 */
			try{
				jArray = getInProcessInvoice(fromDateStr, toDateStr);
			}catch(Exception e){
				logger.log(Level.SEVERE, "Error in getInProcessInvoice: "+e.getMessage());
				jArray = new JSONArray();
			}
		}else if (loadType.equalsIgnoreCase("InprocessPayment")){
			/*
			 * Name: Apeksha Gunjal
			 * Date: 24/07/2018 @15:05 (Tuesday)
			 * Note: InProcess data of Payment which includes Payments which are in inProcess / pending state 
			 * 			starting from beginning to till date.
			 */
			try{
				jArray = getInProcessPayment(fromDateStr, toDateStr);
			}catch(Exception e){
				logger.log(Level.SEVERE, "Error in getInProcessPayment: "+e.getMessage());
				jArray = new JSONArray();
			}
		}
		String jsonString=jArray.toString().replace("\\\\", "");
		logger.log(Level.SEVERE, jsonString);
		
		return jsonString;
	}

	

	

	/*
	 * Name: Apeksha Gunjal
	 * Date: 23/07/2018 @15:05 (Monday)
	 * Note: InProcess data of Lead which includes leads which are in inProcess / pending state 
	 * 			starting from beginning to till date.
	 */
	private JSONArray getInProcessLeads(String fromDateStr, String toDateStr, String employeeName, String role){
		JSONArray jArray=new JSONArray();
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
			Gson gson = new Gson();
			List<Lead> leadList = null;
			try {
				if(role.equalsIgnoreCase("ADMIN")){
				leadList = ofy().load().type(Lead.class)
						.filter("companyId", comp.getCompanyId()).list();
	//					.filter("creationDate >=",sdf.parse(fromDateStr))
//						.filter("creationDate <",sdf.parse(toDateStr)).list();
				}else{
					leadList = ofy().load().type(Lead.class)
							.filter("companyId", comp.getCompanyId())
							.filter("employee", employeeName).list();
				}
				/*
				 * Name: Apeksha Gunjal
				 * Date: 30/07/2018 @ 19:23
				 * remove date filter for getting all inprocess data...
				 */
				logger.log(Level.SEVERE, "getInProcessLead: "+leadList.size());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/** date 2.2.2019 added by komal to sort list based on followupdate(Ascending)**/
			Collections.sort(leadList ,getFollowupDateComparator());
			List<Lead> newLeadList = new ArrayList<Lead>();
			for (int i = 0; i < leadList.size(); i++) {
				if(leadList.get(i).getStatus().trim().equalsIgnoreCase("Successful")||//leadList.get(i).getStatus().trim().equalsIgnoreCase("Approved")||
						leadList.get(i).getStatus().trim().equalsIgnoreCase("Successfull")||
						leadList.get(i).getStatus().trim().equalsIgnoreCase("Sucessfull")||
						leadList.get(i).getStatus().trim().equalsIgnoreCase("Sucessful")){
	//				successful leads...
				}else if(leadList.get(i).getStatus().trim().equalsIgnoreCase("Unsucessfull")||
						leadList.get(i).getStatus().trim().equalsIgnoreCase("Unsucessful")||
						leadList.get(i).getStatus().trim().equalsIgnoreCase("Unsuccessfull")||
						leadList.get(i).getStatus().trim().equalsIgnoreCase("Unsuccessful")||
						leadList.get(i).getStatus().trim().equalsIgnoreCase("Cancelled")){
	//				unsuccessful leads...
				}
				else{
					newLeadList.add(leadList.get(i));
				}
//					JSONObject obj=new JSONObject();
//					obj.put("group", leadList.get(i).getGroup().trim());
//					obj.put("type", leadList.get(i).getType().trim());
//					obj.put("category", leadList.get(i).getCategory().trim());
//					obj.put("status", leadList.get(i).getStatus().trim());
//					obj.put("leadTitle", leadList.get(i).getTitle().trim());
//					obj.put("leadDate", sdf.format(leadList.get(i).getCreationDate()).trim());
//					obj.put("branch", leadList.get(i).getBranch().trim());
//					obj.put("priority", leadList.get(i).getPriority().trim());
//					obj.put("netPayable", leadList.get(i).getNetpayable()+"");
//					obj.put("employeeName", leadList.get(i).getEmployee().trim());
//					obj.put("leadId", leadList.get(i).getCount()+"");
//					obj.put("customerName", leadList.get(i).getPersonInfo().getFullName());
//					obj.put("customerMobileNo",leadList.get(i).getPersonInfo().getCellNumber()+"");
//					obj.put("custPocName", leadList.get(i).getPersonInfo().getPocName());
//					obj.put("customerId", leadList.get(i).getPersonInfo().getCount()+"");
//					obj.put("description", leadList.get(i).getDescription()+"");
//					if(leadList.get(i).getFollowUpDate()!=null){
//					obj.put("followUpDate", sdf.format(leadList.get(i).getFollowUpDate()));
//					
//					List<InteractionType> interactionTypeList=ofy().load().type(InteractionType.class).filter("interactionDocumentName","Lead").filter("interactionDocumentId",leadList.get(i).getCount()).list();
//					if(interactionTypeList.size()!=0){
//					ArrayList<InteractionType> communicationList=new ArrayList<InteractionType>();
//					if(interactionTypeList.size()!=0){
//						communicationList.addAll(interactionTypeList);
//					}
//				
//					Comparator<InteractionType> comp=new Comparator<InteractionType>() {
//						@Override
//						public int compare(InteractionType e1, InteractionType e2) {
//		//					Integer coutn1=o1.getCount();
//		//					Integer count2=o2.getCount();
//		//					return ;
//							if (e1.getCount() == e2.getCount()) {
//								return 0;
//							}
//							if (e1.getCount() < e2.getCount()) {
//								return 1;
//							} else {
//								return -1;
//							}
//						}
//					};
//					Collections.sort(communicationList,comp);
//					
//					JSONArray jsonArray=new JSONArray();
//					boolean greaterThenFive=false;
//					int sizeOfCommunication=0;
//					if(communicationList.size()>5){
//						sizeOfCommunication=5;
//						greaterThenFive=true;
//					}else{
//						greaterThenFive=false;
//						sizeOfCommunication=communicationList.size();
//					}
//					sdf = new SimpleDateFormat("dd MMM yyyy");
//					sdf.setTimeZone(TimeZone.getTimeZone("IST"));
//					TimeZone.setDefault(TimeZone.getTimeZone("IST"));
//					for (int j = 0; j < sizeOfCommunication; j++) {
//						JSONObject objInteractype=new JSONObject();
//						objInteractype.put("communicationId",communicationList.get(j).getCount());
//						objInteractype.put("communicationCreationDate",sdf.format(communicationList.get(j).getinteractionCreationDate()));
//						objInteractype.put("communicationTime",communicationList.get(j).getInteractionTime());
//						objInteractype.put("communicationUsername",communicationList.get(j).getCreatedBy());
//						objInteractype.put("communicationRemark",communicationList.get(j).getInteractionPurpose());
//						objInteractype.put("communicationDueDate",sdf.format(communicationList.get(j).getInteractionDueDate()));
//						objInteractype.put("communicationDueTime",communicationList.get(j).getInteractionDueTime());
//						objInteractype.put("greaterThenFive",greaterThenFive);
//						jsonArray.add(objInteractype);
//					}
//					obj.put("lastFiveCommunication", ""+jsonArray);
//					obj.put("lastCommunication",""+communicationList.get(communicationList.size()-1).getInteractionPurpose());
//					
//					}else{
//						obj.put("lastCommunication", "");
//						obj.put("lastFiveCommunication", "");
//					}
//					}
//					
//					obj.put("productList",
//							gson.toJson(leadList.get(i).getLeadProducts())
//									.toString());
//					obj.put("uniqueId", leadList.get(i).getId());
//					jArray.add(obj);
//				}
			}
			jArray = getLeadArray(newLeadList);
			logger.log(Level.SEVERE, "getInProcessLead jArray: "+jArray.size());
		}catch(Exception e){
			logger.log(Level.SEVERE, "Error in getInProcessLead: "+e.getMessage());
		}
		return jArray;
	}
	
	/*
	 * Name: Apeksha Gunjal
	 * Date: 23/07/2018 @15:05 (Monday)
	 * Note: InProcess data of Quotation which includes quotation which are in inProcess / pending state 
	 * 			starting from beginning to till date.
	 */
	private JSONArray getInProcessQuotation(String fromDateStr, String toDateStr, String employeeName , String role){
		JSONArray jArray = new JSONArray();
		Gson gson = new Gson();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
		List<Quotation> quotationList = null;
		try{
			if(role.equalsIgnoreCase("ADMIN")){
				quotationList =ofy().load().type(Quotation.class)
					.filter("companyId", comp.getCompanyId()).list();
//					.filter("creationDate >=",sdf.parse(fromDateStr))
//					.filter("creationDate <",sdf.parse(toDateStr)).list();
		}else{
			quotationList =ofy().load().type(Quotation.class)
					.filter("companyId", comp.getCompanyId())
					.filter("employee", employeeName).list();
			
		}
			/*
			 * Name: Apeksha Gunjal
			 * Date: 30/07/2018 @ 19:23
			 * remove date filter for getting all inprocess data...
			 */
			logger.log(Level.SEVERE, "getInProcessQuotation quotationList: "+quotationList.size());
			
			for (int i = 0; i < quotationList.size(); i++) {
				JSONObject obj=new JSONObject();
				if(quotationList.get(i).getStatus().trim().equalsIgnoreCase("Approved")||
						quotationList.get(i).getStatus().trim().equalsIgnoreCase("Successfull")||
						quotationList.get(i).getStatus().trim().equalsIgnoreCase("Successful")||
						quotationList.get(i).getStatus().trim().equalsIgnoreCase("Sucessfull")||
						quotationList.get(i).getStatus().trim().equalsIgnoreCase("Sucessful")){
//					successful quotations
				}else if(quotationList.get(i).getStatus().trim().equalsIgnoreCase("Unsucessfull")
						||quotationList.get(i).getStatus().trim().equalsIgnoreCase("Unsucessful")
						||quotationList.get(i).getStatus().trim().equalsIgnoreCase("Unsuccessfull")
						||quotationList.get(i).getStatus().trim().equalsIgnoreCase("Unsuccessful")
						||quotationList.get(i).getStatus().trim().equalsIgnoreCase("Cancelled")){//
//					unsuccessful quotations
				}else{
					logger.log(Level.SEVERE, "getInProcessQuotation quot inside else: "+quotationList.get(i).getStatus());
					obj.put("quotationType", "serviceQuotation");
					obj.put("group", quotationList.get(i).getGroup().trim());
					obj.put("type", quotationList.get(i).getType().trim());
					obj.put("category", quotationList.get(i).getCategory().trim());
					obj.put("status", quotationList.get(i).getStatus().trim());
					obj.put("quotationDate", sdf.format(quotationList.get(i).getQuotationDate()));
					obj.put("branch",quotationList.get(i).getBranch().trim());
					obj.put("priority", quotationList.get(i).getPriority().trim());
					obj.put("netPayable", quotationList.get(i).getNetpayable()+"");
					obj.put("quotationId", quotationList.get(i).getCount()+"");
					obj.put("employeeName", quotationList.get(i).getEmployee().trim());
					obj.put("customerName", quotationList.get(i).getCinfo().getFullName().trim());
					obj.put("customerMobileNo",quotationList.get(i).getCinfo().getCellNumber()+"");
					obj.put("customerId", quotationList.get(i).getCinfo().getCount()+"");
					obj.put("customerPOC", quotationList.get(i).getCinfo().getPocName()+"");
					
					obj.put("productList",
							gson.toJson(quotationList.get(i).getItems())
									.toString());
					obj.put("uniqueId", quotationList.get(i).getId());
					if(quotationList.get(i).getFollowUpDate()!=null){
					obj.put("followUpDate", sdf.format(quotationList.get(i).getFollowUpDate()));
					List<InteractionType> interactionTypeList=ofy().load().type(InteractionType.class).filter("interactionDocumentName","Quotation").filter("interactionDocumentId",quotationList.get(i).getCount()).list();
					if(interactionTypeList.size()!=0){
					ArrayList<InteractionType> communicationList=new ArrayList<InteractionType>();
					if(interactionTypeList.size()!=0){
						communicationList.addAll(interactionTypeList);
					}
					
					Comparator<InteractionType> comp=new Comparator<InteractionType>() {
						@Override
						public int compare(InteractionType e1, InteractionType e2) {
	//						Integer coutn1=o1.getCount();
	//						Integer count2=o2.getCount();
	//						return ;
							if (e1.getCount() == e2.getCount()) {
								return 0;
							}
							if (e1.getCount() < e2.getCount()) {
								return 1;
							} else {
								return -1;
							}
						}
					};
					Collections.sort(communicationList,comp);
					
					JSONArray jsonArray=new JSONArray();
					boolean greaterThenFive=false;
					int sizeOfCommunication=0;
					if(communicationList.size()>5){
						sizeOfCommunication=5;
						greaterThenFive=true;
					}else{
						greaterThenFive=false;
						sizeOfCommunication=communicationList.size();
					}
					sdf = new SimpleDateFormat("dd MMM yyyy");
					sdf.setTimeZone(TimeZone.getTimeZone("IST"));
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));
					for (int j = 0; j < sizeOfCommunication; j++) {
						JSONObject objInteractype=new JSONObject();
						objInteractype.put("communicationId",communicationList.get(j).getCount());
						objInteractype.put("communicationCreationDate",sdf.format(communicationList.get(j).getinteractionCreationDate()));
						objInteractype.put("communicationTime",communicationList.get(j).getInteractionTime());
						objInteractype.put("communicationUsername",communicationList.get(j).getCreatedBy());
						objInteractype.put("communicationRemark",communicationList.get(j).getInteractionPurpose());
						objInteractype.put("communicationDueDate",sdf.format(communicationList.get(j).getInteractionDueDate()));
						objInteractype.put("communicationDueTime",communicationList.get(j).getInteractionDueTime());
						objInteractype.put("greaterThenFive",greaterThenFive);
						jsonArray.add(objInteractype);
					}
					obj.put("lastFiveCommunication", ""+jsonArray);
					obj.put("lastCommunication",""+communicationList.get(communicationList.size()-1).getInteractionPurpose());
					
					}else{
						obj.put("lastCommunication", "");
						obj.put("lastFiveCommunication", "");
					}
					}
					jArray.add(obj);
				}
			}
			/*
			 * Note: Add Sales Quotation and Service Quotation in one screen as a Quotation.
			 * */

			logger.log(Level.SEVERE, "getInProcessQuotation Loading sales quotation...");

			try{
				List<SalesQuotation> salesQuotationList = new ArrayList<>();
				try{
					 if(role.equalsIgnoreCase("ADMIN")){
						 salesQuotationList = ofy().load()
									.type(SalesQuotation.class)
									.filter("companyId", comp.getCompanyId()).list();
					}else{
						salesQuotationList = ofy().load()
								.type(SalesQuotation.class)
								.filter("companyId", comp.getCompanyId())
								.filter("employee", employeeName).list();
						
					}
					 /*
						 * Name: Apeksha Gunjal
						 * Date: 30/07/2018 @ 19:23
						 * remove date filter for getting all inprocess data...
						 */
						logger.log(Level.SEVERE, "getInProcessQuotation no error salesQuotationList: "+salesQuotationList.size());
				}catch(Exception e){
					logger.log(Level.SEVERE, "Error getInProcessQuotation salesQuotationList: "+e.getMessage());
				}
				logger.log(Level.SEVERE, "getInProcessQuotation salesQuotationList: "+salesQuotationList.size());
				
				for (int i = 0; i < salesQuotationList.size(); i++) {
					if(salesQuotationList.get(i).getStatus().trim().equalsIgnoreCase("Approved")||salesQuotationList.get(i).getStatus().trim().equalsIgnoreCase("Successfull")||salesQuotationList.get(i).getStatus().trim().equalsIgnoreCase("Successful")||salesQuotationList.get(i).getStatus().trim().equalsIgnoreCase("Sucessfull")||salesQuotationList.get(i).getStatus().trim().equalsIgnoreCase("Sucessful")){
//						successful quotations
					}else if(salesQuotationList.get(i).getStatus().trim().equalsIgnoreCase("Unsucessfull")||salesQuotationList.get(i).getStatus().trim().equalsIgnoreCase("Unsucessful")||salesQuotationList.get(i).getStatus().trim().equalsIgnoreCase("Unsuccessfull")
							||salesQuotationList.get(i).getStatus().trim().equalsIgnoreCase("Unsuccessful")||salesQuotationList.get(i).getStatus().trim().equalsIgnoreCase("Cancelled")){
//						unsuccessful quotations
					}else{
						JSONObject obj = new JSONObject();
						obj.put("quotationType", "salesQuotation");
						obj.put("group", salesQuotationList.get(i).getGroup().trim());
						obj.put("type", salesQuotationList.get(i).getType().trim());
						obj.put("category", salesQuotationList.get(i).getCategory().trim());
						obj.put("status", salesQuotationList.get(i).getStatus().trim());
						obj.put("quotationDate", sdf.format(salesQuotationList.get(i).getQuotationDate()));
						obj.put("branch",salesQuotationList.get(i).getBranch().trim());
						obj.put("priority", salesQuotationList.get(i).getPriority().trim());
						obj.put("netPayable", salesQuotationList.get(i).getNetpayable()+"");
						obj.put("quotationId", salesQuotationList.get(i).getCount()+"");
						obj.put("employeeName", salesQuotationList.get(i).getEmployee().trim());
						obj.put("customerName", salesQuotationList.get(i).getCinfo().getFullName().trim());
						obj.put("customerMobileNo",salesQuotationList.get(i).getCinfo().getCellNumber()+"");
						obj.put("customerId", salesQuotationList.get(i).getCinfo().getCount()+"");
						obj.put("customerPOC", salesQuotationList.get(i).getCinfo().getPocName()+"");
						
						obj.put("productList",
								gson.toJson(salesQuotationList.get(i).getItems())
										.toString());
						obj.put("uniqueId", salesQuotationList.get(i).getId());
						if(salesQuotationList.get(i).getFollowUpDate()!=null){
						obj.put("followUpDate", sdf.format(salesQuotationList.get(i).getFollowUpDate()));
						List<InteractionType> interactionTypeList=ofy().load().type(InteractionType.class).filter("interactionDocumentName","SalesQuotation").filter("interactionDocumentId",salesQuotationList.get(i).getCount()).list();
						if(interactionTypeList.size()!=0){
						ArrayList<InteractionType> communicationList=new ArrayList<InteractionType>();
						if(interactionTypeList.size()!=0){
							communicationList.addAll(interactionTypeList);
						}
						
						Comparator<InteractionType> comp=new Comparator<InteractionType>() {
							@Override
							public int compare(InteractionType e1, InteractionType e2) {
	//							Integer coutn1=o1.getCount();
	//							Integer count2=o2.getCount();
	//							return ;
								if (e1.getCount() == e2.getCount()) {
									return 0;
								}
								if (e1.getCount() < e2.getCount()) {
									return 1;
								} else {
									return -1;
								}
							}
						};
						Collections.sort(communicationList,comp);
					
						JSONArray jsonArray=new JSONArray();
						boolean greaterThenFive=false;
						int sizeOfCommunication=0;
						if(communicationList.size()>5){
							sizeOfCommunication=5;
							greaterThenFive=true;
						}else{
							greaterThenFive=false;
							sizeOfCommunication=communicationList.size();
						}
						sdf = new SimpleDateFormat("dd MMM yyyy");
						sdf.setTimeZone(TimeZone.getTimeZone("IST"));
						TimeZone.setDefault(TimeZone.getTimeZone("IST"));
						for (int j = 0; j < sizeOfCommunication; j++) {
							JSONObject objInteractype=new JSONObject();
							objInteractype.put("communicationId",communicationList.get(j).getCount());
							objInteractype.put("communicationCreationDate",sdf.format(communicationList.get(j).getinteractionCreationDate()));
							objInteractype.put("communicationTime",communicationList.get(j).getInteractionTime());
							objInteractype.put("communicationUsername",communicationList.get(j).getCreatedBy());
							objInteractype.put("communicationRemark",communicationList.get(j).getInteractionPurpose());
							objInteractype.put("communicationDueDate",sdf.format(communicationList.get(j).getInteractionDueDate()));
							objInteractype.put("communicationDueTime",communicationList.get(j).getInteractionDueTime());
							objInteractype.put("greaterThenFive",greaterThenFive);
							jsonArray.add(objInteractype);
						}
						obj.put("lastFiveCommunication", ""+jsonArray);
						obj.put("lastCommunication",""+communicationList.get(communicationList.size()-1).getInteractionPurpose());
						
						}else{
							obj.put("lastCommunication", "");
							obj.put("lastFiveCommunication", "");
						}
						}
						jArray.add(obj);
					}
				}
			}catch(Exception e){
				logger.log(Level.SEVERE, "getInProcessQuotation sales quot error: "+e.getMessage());
			}
			logger.log(Level.SEVERE, "getInProcessQuotation jArray: "+jArray.size());
		}catch(Exception e){
			logger.log(Level.SEVERE, "Error in getInProcessQuotation: "+e.getMessage());
		}
		return jArray;
	}
	/*
	 * Name: Apeksha Gunjal
	 * Date: 22/07/2018 @13:05 (Sunday)
	 * Note: InProcess data for all screens show in wrapper screen of EVA Priora
	 * 			which includes all data which are in inProcess / pending state 
	 * 			starting from beginning to till date.
	 */
	
	private JSONObject getInprocessDataOfWrapperScreen(long companyId, String toDateStr, UserRole roleAuth , String employeeName){
		JSONObject inProcessDataOfWrapperScreen = new JSONObject();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
		for (int j = 0; j < roleAuth.getAuthorization().size(); j++) {
			if(roleAuth.getAuthorization().get(j).isAndroid()){
				if(roleAuth.getAuthorization().get(j).getScreens().equalsIgnoreCase(ANDROIDSCREENLIST.LEAD)){
					logger.log(Level.SEVERE,"Lead Loading...");
					List<Lead> leadList = null;
					/*List<String> leadStatusList = new ArrayList<>();
					leadStatusList.add("Approved"); leadStatusList.add("Successful");
					leadStatusList.add("Successfull"); leadStatusList.add("Sucessfull");
					leadStatusList.add("Sucessful"); leadStatusList.add("Unsucessfull");
					leadStatusList.add("Unsucessful"); leadStatusList.add("Unsuccessfull");
					leadStatusList.add("Unsuccessful");*/
					//Proposal Sent, 
					try {
						
						if(roleAuth.getRoleName().equalsIgnoreCase("ADMIN")){
							leadList = ofy().load().type(Lead.class).
									filter("companyId", companyId).list();
						}else{
							leadList = ofy().load().type(Lead.class).
									filter("companyId", companyId)
									.filter("employee", employeeName).list();
						}
						
//								.filter("creationDate >=", sdf.parse("01/01/2000"))
//								.filter("creationDate <", sdf.parse(toDateStr)).list();
						/*
						 * Name: Apeksha Gunjal
						 * Date: 30/07/2018 @ 19:23
						 * remove date filter for getting all inprocess data...
						 */
//						.filter("status NOT IN ", leadStatusList)
						logger.log(Level.SEVERE,"Lead List..."+leadList.size());
					} catch (Exception e) {
						logger.log(Level.SEVERE,"Error in Lead Loading..."+e.getMessage());
					}
					double leadInProcessBalance=0d;
					if(leadList!=null){
						for (Lead lead:leadList) {
//							leadInProcessBalance = leadInProcessBalance + lead.getNetpayable();
							if(lead.getStatus().trim().equalsIgnoreCase("Successful")||       //lead.getStatus().trim().equalsIgnoreCase("Approved")||
									lead.getStatus().trim().equalsIgnoreCase("Successfull")||
									lead.getStatus().trim().equalsIgnoreCase("Sucessfull")||
									lead.getStatus().trim().equalsIgnoreCase("Sucessful")){
//								leadSuccessfullBalance=leadSuccessfullBalance+lead.getNetpayable();
//								logger.log(Level.SEVERE,"Lead status 1..."+lead.getStatus());
							}else if(lead.getStatus().trim().equalsIgnoreCase("Unsucessfull")||
									lead.getStatus().trim().equalsIgnoreCase("Unsucessful")||
									lead.getStatus().trim().equalsIgnoreCase("Unsuccessfull")||
									lead.getStatus().trim().equalsIgnoreCase("Unsuccessful")||
									lead.getStatus().trim().equalsIgnoreCase("Cancelled")){
								//check status unsuccessful on 30/07/2018 @18:46 by Apeksha Gunjal
//								leadOutStandingBalance=leadOutStandingBalance+lead.getNetpayable();
//								logger.log(Level.SEVERE,"Lead status 2..."+lead.getStatus());
							}else{
								leadInProcessBalance = leadInProcessBalance + lead.getNetpayable();
//								logger.log(Level.SEVERE,"Lead status leadInProcessBalance..."+lead.getStatus());
							}
						}
					}
					logger.log(Level.SEVERE,"leadInProcessBalance..."+leadInProcessBalance);
					inProcessDataOfWrapperScreen.put("leadInprocessBalance", formatter.format(leadInProcessBalance));
				}
				if(roleAuth.getAuthorization().get(j).getScreens().equalsIgnoreCase(ANDROIDSCREENLIST.QUOTATION)){
					logger.log(Level.SEVERE,"Quotation Loading...");
					List<Quotation> quoatationList = null;
					/*List<String> quotationStatusList = new ArrayList<>();
					quotationStatusList.add("Approved"); quotationStatusList.add("Successful");
					quotationStatusList.add("Successfull"); quotationStatusList.add("Sucessfull");
					quotationStatusList.add("Sucessful"); quotationStatusList.add("Unsucessfull");
					quotationStatusList.add("Unsucessful"); quotationStatusList.add("Unsuccessfull");
					quotationStatusList.add("Unsuccessful");*/
					try {
						if(roleAuth.getRoleName().equalsIgnoreCase("ADMIN")){
						quoatationList = ofy().load().type(Quotation.class)
								.filter("companyId", comp.getCompanyId()).list();
//								.filter("creationDate >=", sdf.parse("01/01/2000"))
//								.filter("creationDate <", sdf.parse(toDateStr)).list();
						}else{
						quoatationList = ofy().load().type(Quotation.class)
									.filter("companyId", comp.getCompanyId())
									.filter("employee", employeeName).list();
						}
								/*
								 * Name: Apeksha Gunjal
								 * Date: 30/07/2018 @ 19:23
								 * remove date filter for getting all inprocess data...
								 */
//						.filter("status IN ", quotationStatusList)
						logger.log(Level.SEVERE,"quoatationList..."+quoatationList.size());
					} catch (Exception e) {
						logger.log(Level.SEVERE,"Error in Quotation Loading..."+e.getMessage());
					}
					double quotinProcessBalance=0d;
					if(quoatationList != null){
						for (Quotation quot:quoatationList) {
//							quotinProcessBalance=quotinProcessBalance+quot.getNetpayable();
							if(quot.getStatus().trim().equalsIgnoreCase("Approved")||quot.getStatus().trim().equalsIgnoreCase("Successfull")||quot.getStatus().trim().equalsIgnoreCase("Successful")||quot.getStatus().trim().equalsIgnoreCase("Sucessfull")||quot.getStatus().trim().equalsIgnoreCase("Sucessful")){
//								quotSuccessfullBalance=quotSuccessfullBalance+quot.getNetpayable();
//								logger.log(Level.SEVERE,"quot status 1..."+quot.getStatus());
							}else if(quot.getStatus().trim().equalsIgnoreCase("Unsucessfull")||quot.getStatus().trim().equalsIgnoreCase("Unsucessful")||
									quot.getStatus().trim().equalsIgnoreCase("Unsuccessfull")||quot.getStatus().trim().equalsIgnoreCase("Unsuccessful")
									||quot.getStatus().trim().equalsIgnoreCase("Cancelled")){
//								quotOutStandingBalance=quotOutStandingBalance+quot.getNetpayable();
//								logger.log(Level.SEVERE,"quot status 2..."+quot.getStatus());
							}else{
								quotinProcessBalance=quotinProcessBalance+quot.getNetpayable();
								logger.log(Level.SEVERE,"quotinProcessBalance..."+quot.getStatus()+" net: "+quot.getNetpayable()+"  "+quotinProcessBalance);
							}
						}
					}
					logger.log(Level.SEVERE,"quotinProcessBalance1..."+quotinProcessBalance);
					
				}
				if(roleAuth.getAuthorization().get(j).getScreens().equalsIgnoreCase(ANDROIDSCREENLIST.SALES_QUOTATION)){
					logger.log(Level.SEVERE,"Sales Quotation Loading...");
					List<SalesQuotation> salesQuotationList = null;
					try{
						if(roleAuth.getRoleName().equalsIgnoreCase("ADMIN")){
						salesQuotationList = ofy().load().type(SalesQuotation.class)
								.filter("companyId", comp.getCompanyId()).list();
//								.filter("creationDate >=", sdf.parse("01/01/2000"))
//								.filter("creationDate <", sdf.parse(toDateStr)).list();
//								.filter("status NOT IN ", quotationStatusList)
						}else{
							salesQuotationList = ofy().load().type(SalesQuotation.class)
									.filter("companyId", comp.getCompanyId())
									.filter("employee", employeeName).list();
						}
						/*
						 * Name: Apeksha Gunjal
						 * Date: 30/07/2018 @ 19:23
						 * remove date filter for getting all inprocess data...
						 */
						logger.log(Level.SEVERE,"salesQuotationList..."+salesQuotationList.size());
					}catch(Exception e){
						logger.log(Level.SEVERE,"Error in SalesQuotation Loading..."+e.getMessage());
					}
					double quotinProcessBalance = 0;
					if(salesQuotationList != null){
						
						for(SalesQuotation salesQuo : salesQuotationList){
//							logger.log(Level.SEVERE,"salesQuo status..."+salesQuo.getStatus());
//							quotinProcessBalance=quotinProcessBalance+salesQuo.getNetpayable();
							if(salesQuo.getStatus().trim().equalsIgnoreCase("Approved")||salesQuo.getStatus().trim().equalsIgnoreCase("Successfull")||salesQuo.getStatus().trim().equalsIgnoreCase("Successful")||salesQuo.getStatus().trim().equalsIgnoreCase("Sucessfull")||salesQuo.getStatus().trim().equalsIgnoreCase("Sucessful")){
//								quotSuccessfullBalance=quotSuccessfullBalance+quot.getNetpayable();
//								logger.log(Level.SEVERE,"salesQuo status 1..."+salesQuo.getStatus());
							}else if(salesQuo.getStatus().trim().equalsIgnoreCase("Unsucessfull")||salesQuo.getStatus().trim().equalsIgnoreCase("Unsucessful")
									||salesQuo.getStatus().trim().equalsIgnoreCase("Unsuccessfull")||salesQuo.getStatus().trim().equalsIgnoreCase("Unsuccessful")||salesQuo.getStatus().trim().equalsIgnoreCase("Unsuccessfull")
									||salesQuo.getStatus().trim().equalsIgnoreCase("Cancelled")){
//								quotOutStandingBalance=quotOutStandingBalance+quot.getNetpayable();
//								logger.log(Level.SEVERE,"salesQuo status 2..."+salesQuo.getStatus());
							}else{
								quotinProcessBalance=quotinProcessBalance+salesQuo.getNetpayable();
								logger.log(Level.SEVERE,"salesQuotinProcessBalance..."+salesQuo.getStatus()+" net: "+salesQuo.getNetpayable()+"  "+quotinProcessBalance);
							}

						}
					}
					logger.log(Level.SEVERE,"quotinProcessBalance2..."+quotinProcessBalance);
					inProcessDataOfWrapperScreen.put("quotationInprocessBalance", formatter.format(quotinProcessBalance));
					
				}
				if(roleAuth.getAuthorization().get(j).getScreens().equalsIgnoreCase(ANDROIDSCREENLIST.CONTRACT)){
					logger.log(Level.SEVERE,"Contract Loading...");
					List<Contract> contractList = null;
					/*List<String> contractStatusList = new ArrayList<>();
					contractStatusList.add(Contract.APPROVED); 
					contractStatusList.add(Contract.CANCELLED);*/
					//contractStatusList.add(Contract.CREATED); Contract.REQUESTED Contract.REJECTED
					try {
						if(roleAuth.getRoleName().equalsIgnoreCase("ADMIN")){
							contractList = ofy().load().type(Contract.class)
									.filter("companyId", comp.getCompanyId()).list();
//									.filter("startDate >=", sdf.parse("01/01/2000"))
//									.filter("startDate <", sdf.parse(toDateStr)).list();
//									.filter("status NOT IN ", contractStatusList)
						}else{
							contractList = ofy().load().type(Contract.class)
									.filter("companyId", comp.getCompanyId())
									.filter("employee", employeeName).list();
							
						}
						
						/*
						 * Name: Apeksha Gunjal
						 * Date: 30/07/2018 @ 19:23
						 * remove date filter for getting all inprocess data...
						 */
						logger.log(Level.SEVERE,"contractList..."+contractList.size());
					} catch (Exception e) {
						logger.log(Level.SEVERE,"Error in Contract Loading..."+e.getMessage());
					}
					double contractinProcessBalance=0d;
					if(contractList != null){
						for (Contract contract:contractList) {
							if(contract.getStatus().trim().equalsIgnoreCase(Contract.APPROVED)||
									contract.getStatus().trim().equalsIgnoreCase(Contract.CANCELLED)){
							}else {
//								logger.log(Level.SEVERE,"contract status..."+contract.getStatus());
								contractinProcessBalance=contractinProcessBalance+contract.getNetpayable();
							}
						}
					}
					logger.log(Level.SEVERE,"contractinProcessBalance..."+contractinProcessBalance);
					inProcessDataOfWrapperScreen.put("contractInprocessBalance", formatter.format(contractinProcessBalance));
					
				}
				if(roleAuth.getAuthorization().get(j).getScreens().equalsIgnoreCase(ANDROIDSCREENLIST.CONTRACT_RENEWAL) 
						|| roleAuth.getAuthorization().get(j).getScreens().equalsIgnoreCase("Contract Renewal Revenue")){
					logger.log(Level.SEVERE,"Contract Renewal Loading...");
					List<Contract> contractRenewalList = null;/*
					List<String> contractRenewalStatusList = new ArrayList<>();
//					contractRenewalStatusList.add(Contract.APPROVED); 
					contractRenewalStatusList.add(Contract.CANCELLED);*/
					try {
						contractRenewalList = ofy().load().type(Contract.class)
								.filter("companyId", comp.getCompanyId()).list();
//								.filter("endDate >=",sdf.parse("01/01/2000"))
//								.filter("endDate <",sdf.parse(toDateStr)).list();
//								.filter("status NOT IN ", contractRenewalStatusList)
						/*
						 * Name: Apeksha Gunjal
						 * Date: 30/07/2018 @ 19:23
						 * remove date filter for getting all inprocess data...
						 */
						logger.log(Level.SEVERE,"contractRenewalList..."+contractRenewalList.size());
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					double contractRenewalinProcess=0d, contractRenewalinProcessCount=0d;
					if(contractRenewalList != null){
						for (Contract contract : contractRenewalList) {
							/*logger.log(Level.SEVERE,"contractRenewal status..."+contract.getStatus()
									+" isRenewFlag..."+contract.isRenewFlag()
									+" isCustomerInterestFlag: "+contract.isCustomerInterestFlag());*/
							if(contract.getStatus().trim().equalsIgnoreCase(Contract.CANCELLED)){
								//cancelled...
							}else if(contract.isRenewFlag()){
								//signed up....
							}else if(contract.isCustomerInterestFlag()){
								//cancelled...
							}else if(!contract.isRenewFlag()){
								contractRenewalinProcess=contractRenewalinProcess+contract.getNetpayable();
								contractRenewalinProcessCount=contractRenewalinProcessCount+1;
							}		
						}
					}
					logger.log(Level.SEVERE,"contractRenewalinProcess..."+contractRenewalinProcess);
					logger.log(Level.SEVERE,"contractRenewalinProcessCount..."+contractRenewalinProcessCount);
					inProcessDataOfWrapperScreen.put("contractRenewalInprocessBalance", formatter.format(contractRenewalinProcess));
					inProcessDataOfWrapperScreen.put("contractRenewalInprocessValue", formatter.format(contractRenewalinProcessCount));
					

				}
				if(roleAuth.getAuthorization().get(j).getScreens().equalsIgnoreCase(ANDROIDSCREENLIST.INVOICE)){
					logger.log(Level.SEVERE,"Invoice Loading...");
					List<Invoice> invoiceList = null;
					List<String> invoiceStatusList = new ArrayList<>();
					invoiceStatusList.add(Invoice.CREATED); 
					invoiceStatusList.add(Invoice.REJECTED);
					invoiceStatusList.add(Invoice.REQUESTED);
					invoiceStatusList.add(Invoice.PROFORMAINVOICE);
					//APPROVED CANCELLED
					try {
						invoiceList = ofy().load().type(Invoice.class)
								.filter("companyId", comp.getCompanyId())
//								.filter("invoiceDate >=", sdf.parse("01/01/2000"))
//								.filter("invoiceDate <", sdf.parse(toDateStr))
								.filter("status IN ", invoiceStatusList).list();
						/*
						 * Name: Apeksha Gunjal
						 * Date: 30/07/2018 @ 19:23
						 * remove date filter for getting all inprocess data...
						 */
						logger.log(Level.SEVERE,"invoiceList..."+invoiceList.size());
					} catch (Exception e) {
						logger.log(Level.SEVERE,"Error in Invoice Loading..."+e.getMessage());
					}
					double invoiceUnBilled=0d;
					if(invoiceList != null){
						for (Invoice invoice:invoiceList) {
//							logger.log(Level.SEVERE,"invoice status..."+invoice.getStatus());
							invoiceUnBilled=invoiceUnBilled+invoice.getNetPayable();
						}
					}
					logger.log(Level.SEVERE,"invoiceUnBilled..."+invoiceUnBilled);
					inProcessDataOfWrapperScreen.put("invoiceInprocessBalance", formatter.format(invoiceUnBilled));
					
				}
				if(roleAuth.getAuthorization().get(j).getScreens().equalsIgnoreCase(ANDROIDSCREENLIST.PAYMENT)){
					logger.log(Level.SEVERE,"CustomerPayment Loading...");
					List<CustomerPayment> paymentList = null;
					List<String> paymentStatusList = new ArrayList<>();
					paymentStatusList.add(CustomerPayment.CREATED); 
//					paymentStatusList.add(CustomerPayment.CANCELLED);//CustomerPayment.CLOSED
					try {
						paymentList = ofy().load().type(CustomerPayment.class)
								.filter("companyId", comp.getCompanyId())
//								.filter("paymentDate >=", sdf.parse("01/01/2000"))
//								.filter("paymentDate <", sdf.parse(toDateStr))
								.filter("status IN ", paymentStatusList).list();
						/*
						 * Name: Apeksha Gunjal
						 * Date: 30/07/2018 @ 19:23
						 * remove date filter for getting all inprocess data...
						 */
						logger.log(Level.SEVERE,"paymentList..."+paymentList.size());
					} catch (Exception e) {
						logger.log(Level.SEVERE,"Error in CustomerPayment Loading..."+e.getMessage());
					}
					double paymentOutStandingBalance=0d;
					/*
					 * Name: Apeksha Gunjal
					 * Date: 04/10/2018 @ 18:03
					 * Note: Added actual payment amount received for android dashboard.
					 */
					double paymentActualInprocessBalance=0d;
					if(paymentList != null){
						for (CustomerPayment payment:paymentList) {
//							logger.log(Level.SEVERE,"payment status..."+payment.getStatus());
							paymentOutStandingBalance=paymentOutStandingBalance+payment.getPaymentAmt();
							paymentActualInprocessBalance = paymentActualInprocessBalance + payment.getPaymentReceived();
						}
					}
					logger.log(Level.SEVERE,"paymentOutStandingBalance..."+paymentOutStandingBalance);
					inProcessDataOfWrapperScreen.put("paymentInprocessBalance", formatter.format(paymentOutStandingBalance));
					inProcessDataOfWrapperScreen.put("paymentActualInprocessBalance", formatter.format(paymentActualInprocessBalance));
					
				}
				if(roleAuth.getAuthorization().get(j).getScreens().equalsIgnoreCase(ANDROIDSCREENLIST.CUSTOMER_SERVICE)){
					logger.log(Level.SEVERE,"Service Loading...");
					List<Service> serviceList = null;
					List<String> serviceStatusList = new ArrayList<>();
					serviceStatusList.add(Service.SERVICESTATUSSCHEDULE); 
					serviceStatusList.add(Service.SERVICESTATUSRESCHEDULE); 
					try {
						serviceList = ofy().load().type(Service.class)
								.filter("companyId", comp.getCompanyId())
//								.filter("serviceDate >=", sdf.parse("01/01/2000"))
//								.filter("serviceDate <", sdf.parse(toDateStr))
								.filter("status IN ", serviceStatusList).list();
						/*
						 * Name: Apeksha Gunjal
						 * Date: 30/07/2018 @ 19:23
						 * remove date filter for getting all inprocess data...
						 */
						logger.log(Level.SEVERE,"serviceList..."+serviceList.size());
					} catch (Exception e) {
						logger.log(Level.SEVERE,"Error in Service Loading..."+e.getMessage());
					}
					int serviceDue=0;
					double serviceDueValue=0;
					
					if(serviceList != null){
						for (Service service:serviceList) {
							serviceDue=serviceDue+1;
							serviceDueValue=serviceDueValue+service.getServiceValue();
//							logger.log(Level.SEVERE,"service status..."+service.getStatus());
						}
					}
					logger.log(Level.SEVERE,"serviceDue..."+serviceDue);
					logger.log(Level.SEVERE,"serviceDueValue..."+serviceDueValue);
					inProcessDataOfWrapperScreen.put("serviceInprocessValue", formatter.format(serviceDue));
					inProcessDataOfWrapperScreen.put("serviceInprocessBalance", formatter.format(serviceDueValue));
					
					
				}
				if(roleAuth.getAuthorization().get(j).getScreens().equalsIgnoreCase(ANDROIDSCREENLIST.SALES_ORDER)){
					logger.log(Level.SEVERE,"SalesOrder Loading...");
					List<SalesOrder> salesOrderList = null;
					try {
						if(roleAuth.getRoleName().equalsIgnoreCase("ADMIN")){
							salesOrderList = ofy().load().type(SalesOrder.class)
									.filter("companyId", comp.getCompanyId()).list();
//									.filter("creationDate >=", sdf.parse("01/01/2000"))
//									.filter("creationDate <", sdf.parse(toDateStr)).list();
						}else{
							salesOrderList = ofy().load().type(SalesOrder.class)
									.filter("companyId", comp.getCompanyId())
									.filter("employee", employeeName).list();
						}
						
						/*
						 * Name: Apeksha Gunjal
						 * Date: 30/07/2018 @ 19:23
						 * remove date filter for getting all inprocess data...
						 */
						//.filter("status NOT IN ", )
						logger.log(Level.SEVERE,"salesOrderList..."+salesOrderList.size());
					} catch (Exception e) {
						logger.log(Level.SEVERE,"Error in SalesOrder Loading..."+e.getMessage());
					}

					double salesOrderOutStandingBalance=0d;//,salesOrderSuccessfullBalance=0d,salesOrderCancelled=0;
					if(salesOrderList != null){
						for (SalesOrder salesOrder:salesOrderList) {
							if(salesOrder.getStatus().trim().equalsIgnoreCase("Approved")||salesOrder.getStatus().trim().equalsIgnoreCase("Successfull")){
//								salesOrderSuccessfullBalance=salesOrderSuccessfullBalance+salesOrder.getNetpayable();
							}else if(salesOrder.getStatus().trim().equalsIgnoreCase(SalesOrder.CANCELLED)){
//								salesOrderCancelled=salesOrderCancelled+salesOrder.getNetpayable();
							}else{
								salesOrderOutStandingBalance=salesOrderOutStandingBalance+salesOrder.getNetpayable();
//								logger.log(Level.SEVERE,"service salesOrder..."+salesOrder.getStatus());
							}
						}
					}
					logger.log(Level.SEVERE,"salesOrderOutStandingBalance..."+salesOrderOutStandingBalance);
					inProcessDataOfWrapperScreen.put("salesOrderInprocessBalance", formatter.format(salesOrderOutStandingBalance));
					
				}
			}
		}
		
		return inProcessDataOfWrapperScreen;
	}

	/*
	 * Name: Apeksha Gunjal
	 * Date: 24/07/2018 @15:10 (Tuesday)
	 * Note: InProcess data of Contract which includes contracts which are in inProcess / pending state 
	 * 			starting from beginning to till date.
	 */
	private JSONArray getInProcessContract(String fromDateStr, String toDateStr, String employeeName , String role){
		JSONArray jArray = new JSONArray();
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
			List<Contract> contractList = null;
			if(role.equalsIgnoreCase("ADMIN")){
				contractList= ofy().load().type(Contract.class)
				.filter("companyId", comp.getCompanyId()).list();
			}else{
				contractList = ofy().load().type(Contract.class)
				.filter("employee", employeeName).filter("companyId", comp.getCompanyId()).list();
			}
			
//					.filter("startDate >=",sdf.parse(fromDateStr))
//					.filter("startDate <",sdf.parse(toDateStr)).list();
			
			/*
			 * Name: Apeksha Gunjal
			 * Date: 30/07/2018 @ 19:23
			 * remove date filter for getting all inprocess data...
			 */
			logger.log(Level.SEVERE,  "getInProcessContract contractList: "+contractList.size());
			
			Gson gson = new Gson();
			for (int i = 0; i < contractList.size(); i++) {
				if(contractList.get(i).getStatus().trim().equalsIgnoreCase(Contract.APPROVED)||
						contractList.get(i).getStatus().trim().equalsIgnoreCase(Contract.CANCELLED)){
					//successful and cancelled contracts...
				}else {
					JSONObject obj = new JSONObject();
					obj.put("group", contractList.get(i).getGroup().trim());
					obj.put("type", contractList.get(i).getType().trim());
					obj.put("category", contractList.get(i).getCategory()
							.trim());
					obj.put("status", contractList.get(i).getStatus().trim());
					obj.put("contractDate",
							sdf.format(contractList.get(i).getContractDate()));
					obj.put("branch", contractList.get(i).getBranch().trim());
					obj.put("priority", contractList.get(i).getPriority()
							.trim());
					obj.put("netPayable", contractList.get(i).getNetpayable()
							+ "");
					obj.put("contractId", contractList.get(i).getCount() + "");
					obj.put("employeeName", contractList.get(i).getEmployee()
							.trim());
					obj.put("customerName", contractList.get(i).getCinfo()
							.getFullName().trim());
					obj.put("customerMobileNo", contractList.get(i).getCinfo()
							.getCellNumber()
							+ "");
					obj.put("customerId", contractList.get(i).getCinfo()
							.getCount()
							+ "");
					obj.put("productList",
							gson.toJson(contractList.get(i).getItems())
									.toString());
					logger.log(
							Level.SEVERE,
							"productList::::"
									+ gson.toJson(
											contractList.get(i).getItems())
											.toString());
					obj.put("totalNoOfServices", contractList.get(i).getItems()
							.size()
							+ "");
					obj.put("approveRejectionRemark", contractList.get(i)
							.getRemark() + "");
					obj.put("contrctStartDate",
							sdf.format(contractList.get(i).getStartDate()) + "");
					obj.put("contractEndDate",
							sdf.format(contractList.get(i).getEndDate()) + "");
					obj.put("approverName", contractList.get(i)
							.getApproverName() + "");
					obj.put("uniqueId", contractList.get(i).getId());
					jArray.add(obj);
				}
			}

			logger.log(Level.SEVERE,  "getInProcessContract jArray: "+jArray.size());
		}catch(Exception e){
			logger.log(Level.SEVERE, "Error getInProcessContract: "+e.getMessage());
		}
		return jArray;
	}
	/*
	 * Name: Apeksha Gunjal
	 * Date: 24/07/2018 @15:10 (Tuesday)
	 * Note: InProcess data of ContractRenewal which includes contractRenewals which are in inProcess / pending state 
	 * 			starting from beginning to till date.
	 */
	private JSONArray getInProcessContractRenewal(String fromDateStr, String toDateStr){
		JSONArray jArray = new JSONArray();
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
			List<Contract> contractList = ofy().load().type(Contract.class)
					.filter("companyId", comp.getCompanyId()).list();
//					.filter("endDate >=",sdf.parse(fromDateStr))
//					.filter("endDate <",sdf.parse(toDateStr)).list();
/*
			 * Name: Apeksha Gunjal
			 * Date: 30/07/2018 @ 19:23
			 * remove date filter for getting all inprocess data...
			 */
			logger.log(Level.SEVERE, "getInProcessContractRenewal contractRenewalList: "+contractList.size());
			Gson gson = new Gson();
			for (int i = 0; i < contractList.size(); i++) {
				if(contractList.get(i).getStatus().trim().equalsIgnoreCase(Contract.CANCELLED)){
					//cancelled...
				}else if(contractList.get(i).isRenewFlag()){
					//signed up....
				}else if(contractList.get(i).isCustomerInterestFlag()){
					//cancelled...
				}else if(!contractList.get(i).isRenewFlag()){
					JSONObject obj = new JSONObject();
					obj.put("group", contractList.get(i).getGroup().trim());
					obj.put("type", contractList.get(i).getType().trim());
					obj.put("category", contractList.get(i).getCategory()
							.trim());
					obj.put("status", contractList.get(i).getStatus().trim());
					obj.put("contractDate",
							sdf.format(contractList.get(i).getContractDate()));
					obj.put("branch", contractList.get(i).getBranch().trim());
					obj.put("priority", contractList.get(i).getPriority()
							.trim());
					obj.put("netPayable", contractList.get(i).getNetpayable()
							+ "");
					obj.put("contractId", contractList.get(i).getCount() + "");
					obj.put("employeeName", contractList.get(i).getEmployee()
							.trim());
					obj.put("customerName", contractList.get(i).getCinfo()
							.getFullName().trim());
					obj.put("customerMobileNo", contractList.get(i).getCinfo()
							.getCellNumber()
							+ "");
					obj.put("customerId", contractList.get(i).getCinfo()
							.getCount()
							+ "");
					obj.put("productList",
							gson.toJson(contractList.get(i).getItems())
									.toString());
					logger.log(
							Level.SEVERE,
							"productList::::"
									+ gson.toJson(
											contractList.get(i).getItems())
											.toString());
					obj.put("totalNoOfServices", contractList.get(i).getItems()
							.size()
							+ "");
					obj.put("approveRejectionRemark", contractList.get(i)
							.getRemark() + "");
					obj.put("contrctStartDate",
							sdf.format(contractList.get(i).getStartDate()) + "");
					obj.put("contractEndDate",
							sdf.format(contractList.get(i).getEndDate()) + "");
					obj.put("approverName", contractList.get(i)
							.getApproverName() + "");
					obj.put("uniqueId", contractList.get(i).getId());
					obj.put("doNotRenew", contractList.get(i).isCustomerInterestFlag());
					obj.put("isRenew",contractList.get(i).isRenewFlag());
					
					jArray.add(obj);
				}
			}

			logger.log(Level.SEVERE, "getInProcessContractRenewal jArray: "+jArray.size());
		}catch(Exception e){
			logger.log(Level.SEVERE, "Error getInProcessContractRenewal: "+e.getMessage());
		}
		return jArray;
	}
	/*
	 * Name: Apeksha Gunjal
	 * Date: 24/07/2018 @15:10 (Tuesday)
	 * Note: InProcess data of SalesOrder which includes SalesOrders which are in inProcess / pending state 
	 * 			starting from beginning to till date.
	 */
	private JSONArray getInProcessSalesOrder(String fromDateStr, String toDateStr, String employeeName , String role){
		JSONArray jArray = new JSONArray();
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
			List<SalesOrder> salesOrderList = null;
			try {
				
				if(role.equalsIgnoreCase("ADMIN")){
				salesOrderList = ofy().load().type(SalesOrder.class)
						.filter("companyId", comp.getCompanyId()).list();
//						.filter("creationDate >=",sdf.parse(fromDateStr))
//						.filter("creationDate <",sdf.parse(toDateStr)).list();
				}else{
					salesOrderList = ofy().load().type(SalesOrder.class)
							.filter("companyId", comp.getCompanyId())
							.filter("employee", employeeName).list();
				}
				/*
				 * Name: Apeksha Gunjal
				 * Date: 30/07/2018 @ 19:23
				 * remove date filter for getting all inprocess data...
				 */
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			logger.log(Level.SEVERE, "getInProcessSalesOrder salesOrderList: "+salesOrderList.size());
			Gson gson = new Gson();
			for (SalesOrder salesOrder : salesOrderList) {
				if(salesOrder.getStatus().trim().equalsIgnoreCase("Approved")||salesOrder.getStatus().trim().equalsIgnoreCase("Successfull")){
//					salesOrderSuccessfullBalance=salesOrderSuccessfullBalance+salesOrder.getNetpayable();
				}else if(salesOrder.getStatus().trim().equalsIgnoreCase(SalesOrder.CANCELLED)){
//					salesOrderCancelled=salesOrderCancelled+salesOrder.getNetpayable();
				}else{
					JSONObject obj = new JSONObject();
					obj.put("group", salesOrder.getGroup().trim());
					obj.put("type", salesOrder.getType().trim());
					obj.put("category", salesOrder.getCategory().trim());
					obj.put("status", salesOrder.getStatus().trim());
					obj.put("salesOrderId", salesOrder.getCount());
					obj.put("customerId", salesOrder.getCinfo().getCount());
					obj.put("customerName", salesOrder.getCinfo().getFullName());
					obj.put("customerCell", salesOrder.getCinfo().getCellNumber());
					obj.put("salesOrderDate", sdf.format(salesOrder.getSalesOrderDate()));
					obj.put("branch", salesOrder.getBranch());
					obj.put("employeeName", salesOrder.getEmployee());
					obj.put("approverName", salesOrder.getApproverName());
					obj.put("paymentMethods", salesOrder.getPaymentMethod());
					obj.put("productArray", gson.toJson(salesOrder.getItems()));
					obj.put("deliveryDate", sdf.format(salesOrder.getDeliveryDate()));
					obj.put("isShippingAddressDifferent", salesOrder.getCheckAddress());
					obj.put("shippingAddress",salesOrder.getShippingAddress().getCompleteAddress());
					obj.put("netPayable", salesOrder.getNetpayable());
					obj.put("uniqueId", salesOrder.getId());
					
					jArray.add(obj);
				}
			}
			logger.log(Level.SEVERE, "getInProcessSalesOrder jArray: "+jArray.size());
		}catch(Exception e){
			logger.log(Level.SEVERE, "Error getInProcessSalesOrder: "+e.getMessage());
		}
		return jArray;
	}
	/*
	 * Name: Apeksha Gunjal
	 * Date: 24/07/2018 @15:10 (Tuesday)
	 * Note: InProcess data of Service which includes Services which are in inProcess / pending state 
	 * 			starting from beginning to till date.
	 */
	private JSONArray getInProcessService(String fromDateStr, String toDateStr){
		JSONArray jArray = new JSONArray();
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
			List<Service> serviceList = null;
			ArrayList<String> serviceStatusList = new ArrayList<>();
			serviceStatusList.add(Service.SERVICESTATUSSCHEDULE); 
			serviceStatusList.add(Service.SERVICESTATUSRESCHEDULE);
			try {
				serviceList = ofy().load().type(Service.class)
						.filter("companyId", comp.getCompanyId())
//						.filter("serviceDate >=",sdf.parse(fromDateStr))
//						.filter("serviceDate <",sdf.parse(toDateStr))
						.filter("status IN ", serviceStatusList).list();
				/*
				 * Name: Apeksha Gunjal
				 * Date: 30/07/2018 @ 19:23
				 * remove date filter for getting all inprocess data...
				 */
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "getInProcessService serviceList: "+serviceList.size());
			Gson gson = new Gson();
			for (Service service : serviceList) {
				JSONObject obj = new JSONObject();
				obj.put("group", service.getGroup().trim());
				obj.put("type", service.getType().trim());
				obj.put("category", service.getCategory().trim());
				obj.put("status", service.getStatus().trim());
				obj.put("serviceId", service.getCount());
				obj.put("contractId", service.getContractCount());
				if(service.getContractStartDate()!=null){
				obj.put("contractStartDate", sdf.format(service.getContractStartDate()));
				}
				obj.put("contractEndDate", sdf.format(service.getContractEndDate()));
				obj.put("customerId", service.getCustomerId());
				obj.put("customerName", service.getCustomerName());
				obj.put("customerMobNo", service.getPersonInfo().getCellNumber());
				obj.put("servicingTimeInHrs", service.getServicingTime());
				obj.put("productId", service.getProduct().getCount());
				obj.put("productCode", service.getProduct().getProductCode().trim());
				obj.put("productName", service.getProduct().getProductName().trim());
				obj.put("serviceSrNo", service.getServiceSerialNo());
				obj.put("serviceEngineer",service.getEmployee().trim());
				obj.put("branch", service.getBranch().trim());
				obj.put("serviceDate", sdf.format(service.getServiceDate()));
				obj.put("serviceDay", service.getServiceDay().trim());
				obj.put("serviceTime", service.getServiceTime().trim());
				obj.put("serviceBranch", service.getServiceBranch().trim());
				obj.put("serviceType", service.getServiceType().trim());
				if(service.getServiceCompletionDate()!=null){
					obj.put("completionDate", sdf.format(service.getServiceCompletionDate()));
				}else{
					obj.put("completionDate","");
				}
				obj.put("completionDuration", service.getServiceCompleteDuration());
				obj.put("completionRemark", service.getServiceCompleteRemark());
				obj.put("customerFeedback", service.getCustomerFeedback());
				obj.put("team", service.getTeam());
				obj.put("premisesDetails", service.getPremises());
				obj.put("quantity", service.getQuantity());
				obj.put("serviceValue", formatter.format(service.getServiceValue()));
				obj.put("serviceAddress", service.getAddress().getCompleteAddress());
				obj.put("uniqueId", service.getId());
				obj.put("locality", service.getAddress().getLocality());
				
				jArray.add(obj);
			}
			logger.log(Level.SEVERE, "getInProcessService jArray: "+jArray.size());
		}catch(Exception e){
			logger.log(Level.SEVERE, "Error getInProcessService: "+e.getMessage());
		}
		return jArray;
	}
	/*
	 * Name: Apeksha Gunjal
	 * Date: 24/07/2018 @15:10 (Tuesday)
	 * Note: InProcess data of Invoice which includes Invoices which are in inProcess / pending state 
	 * 			starting from beginning to till date.
	 */
	private JSONArray getInProcessInvoice(String fromDateStr, String toDateStr){
		JSONArray jArray = new JSONArray();
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

			List<String> invoiceStatusList = new ArrayList<>();
			invoiceStatusList.add(Invoice.CREATED); 
			invoiceStatusList.add(Invoice.REJECTED);
			invoiceStatusList.add(Invoice.REQUESTED);
			invoiceStatusList.add(Invoice.PROFORMAINVOICE);
			
			List<Invoice> invoiceList = ofy().load().type(Invoice.class)
					.filter("companyId", comp.getCompanyId())
//					.filter("invoiceDate >=",sdf.parse(fromDateStr))
//					.filter("invoiceDate <",sdf.parse(toDateStr))
					.filter("status IN ", invoiceStatusList).list();
			/*
			 * Name: Apeksha Gunjal
			 * Date: 30/07/2018 @ 19:23
			 * remove date filter for getting all inprocess data...
			 */
			logger.log(Level.SEVERE, "getInProcessInvoice invoiceList: "+invoiceList.size());
			
			Gson gson = new Gson();
			for (int i = 0; i < invoiceList.size(); i++) {
				JSONObject obj = new JSONObject();
				obj.put("group", invoiceList.get(i).getGroup().trim());
				obj.put("type", invoiceList.get(i).getType().trim());
				obj.put("category", invoiceList.get(i).getCategory().trim());
				obj.put("status", invoiceList.get(i).getStatus().trim());
				obj.put("invoiceDate",
						sdf.format(invoiceList.get(i).getInvoiceDate()));
				obj.put("branch", invoiceList.get(i).getBranch().trim());
				obj.put("netPayable", invoiceList.get(i).getNetPayable()
						+ "");
				obj.put("invoiceId", invoiceList.get(i).getCount() + "");
				obj.put("customerName", invoiceList.get(i).getPersonInfo()
						.getFullName().trim());
				obj.put("customerMobileNo", invoiceList.get(i)
						.getPersonInfo().getCellNumber()
						+ "");
				obj.put("uniqueId", invoiceList.get(i).getId());
				obj.put("customerId", invoiceList.get(i).getPersonInfo()
						.getCount()
						+ "");
				obj.put("invoiceProductList",
						gson.toJson(
								invoiceList.get(i).getSalesOrderProducts())
								.toString());
				logger.log(
						Level.SEVERE,
						"productList::::"
								+ gson.toJson(
										invoiceList.get(i)
												.getSalesOrderProducts())
										.toString());
				obj.put("orderAmount", invoiceList.get(i)
						.getSalesOrderProducts().size()
						+ "");
				obj.put("orderId", invoiceList.get(i).getRemark() + "");
				obj.put("employeeName", invoiceList.get(i).getEmployee() + "");
				obj.put("approverName", invoiceList.get(i).getApproverName() + "");
				obj.put("productList",gson.toJson(invoiceList.get(i).getSalesOrderProducts())
								.toString());

				//Apeksha on 21/07/2018 17:30
				String linkToCall="";
				try{
					logger.log(Level.SEVERE,"Invoice:isMultipleOrderBilling: "+invoiceList.get(i).isMultipleOrderBilling());
					
					if(!invoiceList.get(i).isMultipleOrderBilling()){
						linkToCall="/slick_erp/serviceinvoiceprint"+"?Id="+invoiceList.get(i).getId()+"&"+"preprint="+"plane"+"&"+"type=SingleBilling";
					}else{
						linkToCall="/slick_erp/serviceinvoiceprint"+"?Id="+invoiceList.get(i).getId()+"&"+"preprint="+"plane"+"&"+"type=MultipleBilling";
					}

					obj.put("linkToCall", linkToCall);
				}catch(Exception e){
					e.printStackTrace();
					try{
						linkToCall="/slick_erp/serviceinvoiceprint"+"?Id="+invoiceList.get(i).getId()+"&"+"preprint="+"plane"+"&"+"type=SingleBilling";
						obj.put("linkToCall", linkToCall);
					}catch(Exception e1){
						e1.printStackTrace();
						obj.put("linkToCall", linkToCall);
					}
				}
				jArray.add(obj);
			}
			logger.log(Level.SEVERE, "getInProcessInvoice jArray: "+jArray.size());

		}catch(Exception e){
			logger.log(Level.SEVERE, "Error getInProcessInvoice: "+e.getMessage());
		}
		return jArray;
	}
	/*
	 * Name: Apeksha Gunjal
	 * Date: 24/07/2018 @15:10 (Tuesday)
	 * Note: InProcess data of Payment which includes Payments which are in inProcess / pending state 
	 * 			starting from beginning to till date.
	 */
	private JSONArray getInProcessPayment(String fromDateStr, String toDateStr){
		JSONArray jArray = new JSONArray();
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
			List<CustomerPayment> paymentList = null;

			List<String> paymentStatusList = new ArrayList<>();
			paymentStatusList.add(CustomerPayment.CREATED); 
			
			try {
				paymentList = ofy().load().type(CustomerPayment.class)
						.filter("companyId", comp.getCompanyId())
//						.filter("paymentDate >=",sdf.parse(fromDateStr))
//						.filter("paymentDate <",sdf.parse(toDateStr))
						.filter("status IN ", paymentStatusList).list();
				/*
				 * Name: Apeksha Gunjal
				 * Date: 30/07/2018 @ 19:23
				 * remove date filter for getting all inprocess data...
				 */
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "getInProcessPayment paymentList: "+paymentList.size());
			Gson gson = new Gson();
			for (int i = 0; i < paymentList.size(); i++) {
				JSONObject obj = new JSONObject();
				obj.put("group", paymentList.get(i).getGroup().trim());
				obj.put("type", paymentList.get(i).getType().trim());
				obj.put("category", paymentList.get(i).getCategory()
						.trim());
				obj.put("status", paymentList.get(i).getStatus().trim());
				obj.put("paymentDate", sdf.format(paymentList.get(i).getPaymentDate()));
				obj.put("invoiceDate",
						sdf.format(paymentList.get(i).getInvoiceDate()));
				obj.put("uniqueId", paymentList.get(i).getId());
				obj.put("branch", paymentList.get(i).getBranch().trim());
				obj.put("paymentAmount", paymentList.get(i).getPaymentAmt());
				obj.put("amountRecieved", paymentList.get(i).getPaymentReceived());
				obj.put("paymentId", paymentList.get(i).getCount()+ "");
				obj.put("customerName", paymentList.get(i).getPersonInfo()
						.getFullName().trim());
				obj.put("customerMobileNo", paymentList.get(i).getPersonInfo()
						.getCellNumber()
						+ "");
				obj.put("customerId", paymentList.get(i).getPersonInfo()
						.getCount()
						+ "");
				obj.put("paymentProductList",
						gson.toJson(paymentList.get(i).getSalesOrderProducts())
								.toString());
				logger.log(
						Level.SEVERE,
						"productList::::"
								+ gson.toJson(
										paymentList.get(i).getSalesOrderProducts())
										.toString());
				obj.put("orderAmount", paymentList.get(i).getSalesOrderProducts()
						.size()
						+ "");
				obj.put("orderId", paymentList.get(i)
						.getRemark() + "");
				obj.put("employeeName", paymentList.get(i).getEmployee().trim()+"");
				obj.put("productList",gson.toJson(paymentList.get(i).getSalesOrderProducts())
						.toString());
				
				jArray.add(obj);
			}

			logger.log(Level.SEVERE, "getInProcessPayment jArray: "+jArray.size());
		}catch(Exception e){
			logger.log(Level.SEVERE, "Error getInProcessPayment: "+e.getMessage());
		}
		return jArray;
	}
	
	/*
	 * Name: Apeksha Gunjal
	 * Date: 27/07/2018 @16:20 (Friday)
	 * Note: Add Complains in JsonArray.
	 */
	private JSONArray getComplainJsonArray(List<Complain> complainList, String apiCallfrom){
		JSONArray array = new JSONArray();
		
		if(complainList != null){
			for(int i =0; i < complainList.size(); i++){
				JSONObject jsonObject = new JSONObject();
				Complain complain = complainList.get(i);
				jsonObject.put("amountReceived", ""+complain.getAmountReceived());
				jsonObject.put("assignTo", ""+complain.getAssignto());
				jsonObject.put("balance", ""+complain.getBalance());
				jsonObject.put("billable", ""+complain.getBbillable());
				jsonObject.put("branch", ""+complain.getBranch());
				jsonObject.put("brandName", ""+complain.getBrandName());
				jsonObject.put("callerCellNo", ""+complain.getCallerCellNo());
				jsonObject.put("callerName", ""+complain.getCallerName());
				jsonObject.put("companyId", ""+complain.getCompanyId());
				jsonObject.put("complainDate", ""+dateFormat.format(complain.getComplainDate()));
				jsonObject.put("complainTime", ""+complain.getComplaintime());
				jsonObject.put("complainTitle", ""+complain.getComplainTitle());
				jsonObject.put("compStatus", ""+complain.getCompStatus());
				jsonObject.put("contractId", ""+complain.getContrtactId());
				jsonObject.put("count", ""+complain.getCount());
				jsonObject.put("createdBy", ""+complain.getCreatedBy());
				jsonObject.put("customerFeedback", ""+complain.getCustomerFeedback());
				jsonObject.put("description", ""+complain.getDestription());
				jsonObject.put("modelNumber", ""+complain.getModelNumber());
				jsonObject.put("netPayble", ""+complain.getNetPayable());
				try{
				if(complain.getPic() != null){
					if(complain.getPic().getProductName() != null)
						jsonObject.put("productName", ""+complain.getPic().getProductName());
					else
						jsonObject.put("productName", "");
				}else
					jsonObject.put("productName", "");
				}catch(Exception e){
					logger.log(Level.SEVERE, "Error: "+e.getMessage());
					jsonObject.put("productName", "");
				}
				jsonObject.put("fullName", ""+complain.getPersoninfo().getFullName());
				jsonObject.put("remark", ""+complain.getRemark());
				jsonObject.put("salesPerson", ""+complain.getSalesPerson());
				jsonObject.put("serialNumber", ""+complain.getSerialNumber());
				jsonObject.put("userId", ""+complain.getUserId());
				/**
				 * @author Vijay Date :- 17-01-2023
				 * Des :- added for customer portal
				 */
				if(complain.getCompletionDate()!=null){
					jsonObject.put("closureDate", complain.getCompletionDate());
				}
				else{
					jsonObject.put("closureDate", "");
				}

				array.add(jsonObject);
			}
		}
		
		if(complainList.size()==0 && apiCallfrom!=null && apiCallfrom.equals(AppConstants.CUSTOMERPORTAL) ) {
			JSONObject obj = new JSONObject();
			obj.put("Message", "No data found");
			array.add(obj);

		}
		
		return array;
	}
	
	/*
	 * Name:Apeksha Gunjal
	 * Date: 11/09/2018 @15:50
	 * Note: Added code for getting customer info from customerId.
	 */ 
	private JSONObject getIndividualCustomerInfo(long companyId, long customerId){
		JSONObject object = new JSONObject();
		try{
	        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Customer customer = ofy().load().type(Customer.class)
					.filter("companyId", companyId)
					.filter("count", customerId).first().now();
			object.put("custId", customerId);
			object.put("companyName", customer.getCompanyName());
			object.put("fullName", customer.getFullname());
			object.put("employeeName", customer.getEmployee());
			object.put("creationDate", sdf.format(customer.getCreationDate()));
			if(customer.getArticleTypeDetails() != null && customer.getArticleTypeDetails().size() > 0){
				ArticleType articleType = customer.getArticleTypeDetails().get(0);
				object.put("gstNo", articleType.getArticleTypeValue());
			}else{
				object.put("gstNo", "");
			}
			object.put("cellNumber1", customer.getCellNumber1());
			object.put("email", customer.getEmail());
			object.put("branch", customer.getBranch());
			Address address = customer.getAdress();
			object.put("addressLine1", address.getAddrLine1());
			object.put("addressLine2", address.getAddrLine2());
			object.put("landmark", address.getLandmark());
			object.put("locality", address.getLocality());
			object.put("city", address.getCity());
			object.put("state", address.getState());
			object.put("country", address.getCountry());
			object.put("pin", address.getPin());
			if(customer.getType() != null){
				object.put("type",customer.getType());
			}else{
				object.put("type", "");
			}
			
			if(customer.getCategory() != null){
				object.put("category", customer.getCategory());
			}else{
				object.put("category", "");
			}
		}catch(Exception e){
			logger.log(Level.SEVERE, "getIndividualCustomerInfo: "+e);
		}
		return object;
	}
	
	/** date 2.2.2019 aded by komal to send data in ascending order of followup date in lead**/
	private Comparator<Lead> getFollowupDateComparator(){
		Comparator<Lead> comp=new Comparator<Lead>() {
			@Override
			public int compare(Lead e1, Lead e2) {
				if(e1!=null && e2!=null)
				{
					 if(e1.getFollowUpDate() == null && e2.getFollowUpDate()==null){
					        return 0;
					    }
					    if(e1.getFollowUpDate() == null) return 1;
					    if(e2.getFollowUpDate() == null) return -1;
					    
					    return e1.getFollowUpDate().compareTo(e2.getFollowUpDate());
				}
				return 0;
			}
		};
		return comp;
	}
	public JSONArray getServiceArray(List<Service> serviceList, String apiCallfrom, Company company){
		
		
		DecimalFormat df = new DecimalFormat("0.00");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		JSONArray jArray = new JSONArray();
		
		/**
		 * @author Vijay
		 * Des :- updated code to send feedback zoho URL for customer protal
		 */
		boolean serviceFeedbacklinkFlag = false;
		if(apiCallfrom!=null && apiCallfrom.equals(AppConstants.CUSTOMERPORTAL)){
			serviceFeedbacklinkFlag = true;
		}
		
		try {
			
			for (Service service : serviceList) {
				JSONObject obj = new JSONObject();
				obj.put("group", service.getGroup().trim());
				obj.put("type", service.getType().trim());
				obj.put("category", service.getCategory().trim());
				obj.put("status", service.getStatus().trim());
				obj.put("serviceId", service.getCount());
				obj.put("contractId", service.getContractCount());
				if(service.getContractStartDate()!=null){
				obj.put("contractStartDate", sdf.format(service.getContractStartDate()));
				}
				obj.put("contractEndDate", sdf.format(service.getContractEndDate()));
				obj.put("customerId", service.getCustomerId());
				obj.put("customerName", service.getCustomerName());
				obj.put("customerMobNo", service.getPersonInfo().getCellNumber());
				obj.put("servicingTimeInHrs", service.getServicingTime());
				obj.put("productId", service.getProduct().getCount());
				obj.put("productCode", service.getProduct().getProductCode().trim());
				obj.put("productName", service.getProduct().getProductName().trim());
				obj.put("serviceSrNo", service.getServiceSerialNo());
				obj.put("serviceEngineer",service.getEmployee().trim());
				obj.put("branch", service.getBranch().trim());
				obj.put("serviceDate", sdf.format(service.getServiceDate()));
				obj.put("serviceDay", service.getServiceDay().trim());
				
				/* Ashwini Patil 
				* Date:12-09-2023 
				* Ajinkya reported that when service is scheduled from ERP the schedule time shows in AM/PM format
				*and started, reported, completed time shows in 24 hour format. So making it standard
				*/
				String serviceTime="";
				try {
					if(service.getServiceTime()!=null) {
						if(service.getServiceTime().contains("AM")) {
							String hour=service.getServiceTime().substring(0, 2);
							String min=service.getServiceTime().substring(3, 5);
							if(hour.equals("12"))
								serviceTime="00:"+min;
							else
								serviceTime=hour+":"+min;
						}else if(service.getServiceTime().contains("PM")) {
							int hour=Integer.parseInt(service.getServiceTime().substring(0, 2));
							String min=service.getServiceTime().substring(3,5);
							if(hour==12)
								serviceTime=hour+":"+min;
							else {
								hour=hour+12;
								serviceTime=hour+":"+min;
							}
						}else
							serviceTime=service.getServiceTime();
					}
					obj.put("serviceTime", serviceTime);
					logger.log(Level.SEVERE, "serviceTime::::::::::"
							+ serviceTime);
				
				}catch(Exception e) {
					e.printStackTrace();
					obj.put("serviceTime", service.getServiceTime().trim());
				}
				
				
				
				obj.put("serviceBranch", service.getServiceBranch().trim());
				obj.put("serviceType", service.getServiceType().trim());
				if(service.getServiceCompletionDate()!=null){
					obj.put("completionDate", sdf.format(service.getServiceCompletionDate()));
				}else{
					obj.put("completionDate","");
				}
				obj.put("completionDuration", service.getServiceCompleteDuration());
				obj.put("completionRemark", service.getServiceCompleteRemark());
				obj.put("customerFeedback", service.getCustomerFeedback());
				obj.put("team", service.getTeam());
				obj.put("premisesDetails", service.getPremises());
				obj.put("quantity", service.getQuantity());
				obj.put("serviceValue", df.format(service.getServiceValue()));
				if(service.getAddress()!=null)
					obj.put("serviceAddress", service.getAddress().getCompleteAddress());
				else
					obj.put("serviceAddress", "");
				obj.put("uniqueId", service.getId());
				if(service.getAddress()!=null&&service.getAddress().getLocality()!=null)
					obj.put("locality", service.getAddress().getLocality());
				else
					obj.put("locality", "");
				//Added by Apeksha Gunjal on 20/09/2018 @ 19:44. Added service number.
				obj.put("serviceNumber", service.getServiceSrNo());
				//Added by Apeksha Gunjal on 21/09/2018 @ 13:10. Added service remark.
				obj.put("remark", service.getRemark());
				JSONArray statusArray = new JSONArray();
				JSONObject statusObject = null;
				Map<String , TrackTableDetails> trackMap = new HashMap<String , TrackTableDetails>();
				for(TrackTableDetails trackDetails : service.getTrackServiceTabledetails()){
					if(trackMap.containsKey(trackDetails.getStatus())){
						if(trackMap.get(trackDetails.getStatus()).getSrNo() 
								< trackDetails.getSrNo()){
							trackMap.put(trackDetails.getStatus(), trackDetails);
						}
					}else{
						trackMap.put(trackDetails.getStatus(), trackDetails);
					}
				}
				for(Map.Entry<String, TrackTableDetails> entry : trackMap.entrySet()){
					TrackTableDetails trackDetails = entry.getValue();
					statusObject= new JSONObject();
					statusObject.put("dateTime", trackDetails.getDate_time());
					statusObject.put("status", trackDetails.getStatus());
					statusObject.put("location" , trackDetails.getLocationName());
					statusObject.put("latitude" , trackDetails.getLatitude());
					statusObject.put("longitude" , trackDetails.getLongitude());
					statusObject.put("km" , trackDetails.getKiloMeters());
					statusObject.put("time" , trackDetails.getTime());
					statusArray.add(statusObject);
				}
				obj.put("trackingDetais", statusArray);
				/**Date 24-1-2020 by AMOL Added a serviceSchedule status raised by AMIT**/
				obj.put("scheduleStatus", service.isServiceScheduled());
				
				/**
				 * @author Vijay Date :- 10-12-2021
				 * Des :- when this API call from customerPortal then i am sending zoho link 
				 */
				String feedbackFormUrl="";
				if(serviceFeedbacklinkFlag && service.getStatus().equals(service.SERVICESTATUSCOMPLETED)){
					if(company!=null&&company.getFeedbackUrlList()!=null&&company.getFeedbackUrlList().size()!=0){
						Customer customer=ofy().load().type(Customer.class).filter("companyId",service.getCompanyId()).filter("count",service.getPersonInfo().getCount()).first().now();
						if(customer!=null){
							if(customer.getCategory()!=null&&!customer.getCategory().equals("")){
								for(FeedbackUrlAsPerCustomerCategory feedback:company.getFeedbackUrlList()){
									if(feedback.getCustomerCategory().equals(customer.getCategory())){
										feedbackFormUrl=feedback.getFeedbackUrl();
										break;
									}
								}
							}
						}
						
						if(feedbackFormUrl!=null&&!feedbackFormUrl.equals("")){
							String serDate = dateFormat.format(service.getServiceDate());
							logger.log(Level.SEVERE, "ZOHO FEEDBACK URL " + feedbackFormUrl);
							StringBuilder sbPostData= new StringBuilder(feedbackFormUrl);
					        sbPostData.append("?customerId="+service.getPersonInfo().getCount());
					        sbPostData.append("&customerName="+service.getPersonInfo().getFullName());
					        sbPostData.append("&serviceId="+service.getCount());
					        sbPostData.append("&serviceDate="+serDate);
					        sbPostData.append("&serviceName="+service.getProductName());
					        sbPostData.append("&technicianName="+service.getEmployee());
					        sbPostData.append("&branch="+service.getBranch());
					        feedbackFormUrl = sbPostData.toString();
					        feedbackFormUrl=feedbackFormUrl.replaceAll("\\s", "%20");
					        feedbackFormUrl=feedbackFormUrl.replaceAll("&", "%26");
					        feedbackFormUrl=ServerAppUtility.getTinyUrl(feedbackFormUrl,company.getCompanyId());
					        
					        logger.log(Level.SEVERE, "FINAL ZOHO FEEDBACK URL " + feedbackFormUrl);
						}
					}

				}
				obj.put("serviceFeedback", feedbackFormUrl);
				
				
				jArray.add(obj);
				
				String jsonString=jArray.toString().replace("\\\\", "");
				logger.log(Level.SEVERE,"jsonString" +jsonString);
			}
			
			

			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		if(apiCallfrom!=null && apiCallfrom.equals(AppConstants.CUSTOMERPORTAL) && serviceList.size()==0){
			JSONObject obj = new JSONObject();
			obj.put("Message", "No data found");
			jArray.add(obj);
		}
		
		return jArray;
	}
	
	
	public String getCustomerAddress(int custId,List<Customer> customerList) {
		if (customerList != null && customerList.size() != 0) {
			for (Customer cust : customerList) {
				if (custId == cust.getCount()) {
					return cust.getAdress().getCompleteAddress();
				}
			}
		}
		return "";
	}
	
	
	public JSONArray getLeadArray(List<Lead> leadList){
		
		HashSet<Integer> custSet=new HashSet<Integer>();
		if(leadList!=null&&leadList.size()!=0){
			for(Lead lead:leadList){
				custSet.add(lead.getPersonInfo().getCount());
			}
		}
		ArrayList<Integer> custList=new ArrayList<Integer>(custSet);
		List<Customer> customerList=null;
		if(custList!=null){
			customerList=ofy().load().type(Customer.class).filter("companyId",leadList.get(0).getCompanyId()).
					            filter("count IN", custList).list();
		
		}
		
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		JSONArray jArray = new JSONArray();
		Collections.sort(leadList ,getFollowupDateComparator());
		for (int i = 0; i < leadList.size(); i++) {
			JSONObject obj=new JSONObject();
			obj.put("group", leadList.get(i).getGroup().trim());
			obj.put("type", leadList.get(i).getType().trim());
			obj.put("category", leadList.get(i).getCategory().trim());
			obj.put("status", leadList.get(i).getStatus().trim());
			obj.put("leadTitle", leadList.get(i).getTitle().trim());
//			try {
				obj.put("leadDate",sdf.format(leadList.get(i).getCreationDate())+"");
//			} catch (ParseException e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			obj.put("branch", leadList.get(i).getBranch().trim());
			obj.put("priority", leadList.get(i).getPriority().trim());
			obj.put("netPayable", leadList.get(i).getNetpayable()+"");
			obj.put("employeeName", leadList.get(i).getEmployee().trim());
			obj.put("leadId", leadList.get(i).getCount()+"");
			obj.put("customerName", leadList.get(i).getPersonInfo().getFullName());
			obj.put("customerMobileNo",leadList.get(i).getPersonInfo().getCellNumber()+"");
			obj.put("custPocName", leadList.get(i).getPersonInfo().getPocName());
			obj.put("customerId", leadList.get(i).getPersonInfo().getCount()+"");
			obj.put("description", leadList.get(i).getDescription()+"");
			if(leadList.get(i).getFollowUpDate()!=null){
//			try {
				obj.put("followUpDate", sdf.format(leadList.get(i).getFollowUpDate())+"");
//			} catch (ParseException e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
				/**Added a Followoup time by Amol Date 26-12-2019**/
			if(leadList.get(i).getFollowUpTime()!=null){
			obj.put("followUpTime", leadList.get(i).getFollowUpTime());
			}else{
				obj.put("followUpTime","");
			}
			List<InteractionType> interactionTypeList=ofy().load().type(InteractionType.class).filter("interactionDocumentName","Lead").filter("interactionDocumentId",leadList.get(i).getCount()).list();
			if(interactionTypeList.size()!=0){
			ArrayList<InteractionType> communicationList=new ArrayList<InteractionType>();
			if(interactionTypeList.size()!=0){
				communicationList.addAll(interactionTypeList);
			}
		
			Comparator<InteractionType> comp=new Comparator<InteractionType>() {
				@Override
				public int compare(InteractionType e1, InteractionType e2) {
					if (e1.getCount() == e2.getCount()) {
						return 0;
					}
					if (e1.getCount() < e2.getCount()) {
						return 1;
					} else {
						return -1;
					}
				}
			};
			Collections.sort(communicationList,comp);
			
			JSONArray jsonArray=new JSONArray();
			boolean greaterThenFive=false;
			int sizeOfCommunication=0;
			if(communicationList.size()>5){
				sizeOfCommunication=5;
				greaterThenFive=true;
			}else{
				greaterThenFive=false;
				sizeOfCommunication=communicationList.size();
			}
			/**It is commented by Amol because it was change the date format**/
//			sdf = new SimpleDateFormat("dd MMM yyyy");
//			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
//			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			for (int j = 0; j < sizeOfCommunication; j++) {
				JSONObject objInteractype=new JSONObject();
				objInteractype.put("communicationId",communicationList.get(j).getCount());
				objInteractype.put("communicationCreationDate",sdf.format(communicationList.get(j).getinteractionCreationDate()));
				objInteractype.put("communicationTime",communicationList.get(j).getInteractionTime());
				objInteractype.put("communicationUsername",communicationList.get(j).getCreatedBy());
				objInteractype.put("communicationRemark",communicationList.get(j).getInteractionPurpose());
				objInteractype.put("communicationDueDate",sdf.format(communicationList.get(j).getInteractionDueDate()));
				objInteractype.put("communicationDueTime",communicationList.get(j).getInteractionDueTime());
				objInteractype.put("greaterThenFive",greaterThenFive);
				jsonArray.add(objInteractype);
			}
			obj.put("lastFiveCommunication", ""+jsonArray);
			obj.put("lastCommunication",""+communicationList.get(0).getInteractionPurpose());
			
			}else{
				obj.put("lastCommunication", "");
				obj.put("lastFiveCommunication", "");
			}
			}
		
			obj.put("unsuccessfulReason", leadList.get(i).getDocStatusReason());
			obj.put("unsuccessfulRemark", leadList.get(i).getRemark());
			
			boolean flag =true;
			if(leadList.get(i).getLeadProducts() != null && leadList.get(i).getLeadProducts().size() > 0){
				if(leadList.get(i).getLeadProducts().get(0).getPrduct() instanceof ServiceProduct){
					flag = true;
				}else{
					flag = false;
				}
			}
			obj.put("isServiceProduct", flag);
			Gson gson = new Gson();
			obj.put("productList",
					gson.toJson(leadList.get(i).getLeadProducts())
							.toString());
			obj.put("uniqueId", leadList.get(i).getId());
			/**Date 3-12-2019 by Amol send customer address**/
			obj.put("address", getCustomerAddress(leadList.get(i).getPersonInfo().getCount(), customerList));
			jArray.add(obj);
		}
			return jArray;
		}
	
		public String validateCustomerCelllNoEmailId(String customerCellNo, String customerEmailId) {
			if(customerCellNo!=null && !customerCellNo.equals("") && customerEmailId!=null && !customerEmailId.equals("")){
				return "";
			}
			else{
				return "Please send either customer cell no or customer email id";
			}

		}
		
		public Customer loadCustomer(String customerCellNo, String customerEmailId) {
			logger.log(Level.SEVERE, "customerCellNo "+customerCellNo);
			logger.log(Level.SEVERE, "customerEmailId "+customerEmailId);
			long customerCellNumber = 0;
			try {
				customerCellNumber = Long.parseLong(customerCellNo);
				logger.log(Level.SEVERE, "customerCellNumber "+customerCellNumber);

			} catch (Exception e) {
				// TODO: handle exception
			}
			
			Customer customer = null;
			try {
				if(customerCellNo!=null && !customerCellNo.equals("")){
					customer = ofy().load().type(Customer.class).filter("companyId", comp.getCompanyId()).filter("contacts.cellNo1", customerCellNumber).first().now();
					logger.log(Level.SEVERE, "customer "+customer.getCount());
				}
				else if(customerEmailId!=null && !customerEmailId.equals("")){
					customer = ofy().load().type(Customer.class).filter("companyId", comp.getCompanyId()).filter("contacts.email", customerEmailId).first().now();
					logger.log(Level.SEVERE, "customer = "+customer.getCount());
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			return customer;
		}
	
	
		/**
		 * @author Ashwini Patil
		 * Date: 27-05-2023
		 * Since there are many api changes in contract retrieval from customer portal, creating this new method so that old logic and working will not get affected
		 */
		@SuppressWarnings("unchecked")
		private String createContractJSONData(String loadType, String fromDateStr, String toDateStr, String employeeName, String role , String employeeGroup, String status, String customerCellNo, String customerEmailId, String apiCallfrom, String loadActiveContract, String customerId, String actionTask,String contractStatus) {

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			DecimalFormat df = new DecimalFormat("0.00");
			
			JSONArray jArray = null;		
			
			 
			if (loadType.trim().equalsIgnoreCase("Contract")) {
				int customerid = 0;
				try {
					customerid = Integer.parseInt(customerId);
				} catch (Exception e) {
					return "please send valid customer id";
				}
				if(customerid!=0) {
					Customer customerEntity = ofy().load().type(Customer.class).filter("count", customerid).filter("companyId",comp.getCompanyId()).first().now();
					if(customerEntity!=null) {
				
				
				try {
					jArray = new JSONArray();
					List<Contract> contractList = new ArrayList<Contract>();
					logger.log(Level.SEVERE, "apiCallfrom "+apiCallfrom);
					if(contractStatus!=null&&!contractStatus.equals("")&&contractStatus.equals("Active")) {
							logger.log(Level.SEVERE, "inside Active contract condition ");
							
									List<Contract> customerwisecontractList = ofy().load().type(Contract.class)
											.filter("companyId", comp.getCompanyId()).filter("cinfo.count", customerEntity.getCount())
											.filter("status", Contract.APPROVED).list();
									logger.log(Level.SEVERE, "customerwisecontractList size "+customerwisecontractList.size());
									
									Date todaysDate = new Date();
									Calendar cal4=Calendar.getInstance();
									cal4.setTime(todaysDate);
									cal4.add(Calendar.DATE, 0);
									
									Date todayDate=null;
									
									try {
										todayDate=dateFormat.parse(dateFormat.format(cal4.getTime()));
									} catch (ParseException e) {
										e.printStackTrace();
									}
									logger.log(Level.SEVERE, "todaysDate"+todayDate);

									if(customerwisecontractList.size()>0){

										for(Contract contractEnity : customerwisecontractList){
											if(contractEnity.getEndDate().after(todayDate) || contractEnity.getEndDate().equals(todayDate)){
												contractList.add(contractEnity);
											}
										}
									}
									logger.log(Level.SEVERE, "For Active contract contractList size = "+contractList.size());
					
					}
					else if(contractStatus!=null&&!contractStatus.equals("")&&contractStatus.equals("Expired")) {

						logger.log(Level.SEVERE, "inside Expired contract condition ");
						
								List<Contract> customerwisecontractList = ofy().load().type(Contract.class)
										.filter("companyId", comp.getCompanyId()).filter("cinfo.count", customerEntity.getCount())
										.filter("status", Contract.APPROVED).list();
								logger.log(Level.SEVERE, "customerwisecontractList size "+customerwisecontractList.size());
								

								
								try {
									Date fromDate = sdf.parse(fromDateStr);
									Date todate = sdf.parse(toDateStr);

									if(customerwisecontractList.size()>0){

										for(Contract contractEnity : customerwisecontractList){
											if((contractEnity.getEndDate().after(fromDate) && contractEnity.getEndDate().before(todate)) || 
													((contractEnity.getEndDate().equals(fromDate) || contractEnity.getEndDate().equals(todate)))) {
												contractList.add(contractEnity);
											}
										}
									}
								
								}
								catch(Exception e) {
									e.printStackTrace();
								}
								logger.log(Level.SEVERE, "Expired  contractList size = "+contractList.size());
		
					}else if(contractStatus!=null&&!contractStatus.equals("")&&contractStatus.equals("Cancelled")) {
								logger.log(Level.SEVERE, "inside Cancelled contract condition ");		
								ArrayList<String> statuslist=new ArrayList<String>();
								statuslist.add(Contract.CANCELLED);
								statuslist.add(Contract.CONTRACTDISCOUNTINED);
								List<Contract> customerwisecontractList = ofy().load().type(Contract.class)
										.filter("companyId", comp.getCompanyId()).filter("cinfo.count", customerEntity.getCount())
										.filter("status IN", statuslist).list();
								logger.log(Level.SEVERE, "customerwisecontractList size "+customerwisecontractList.size());
								
								try {
									Date fromDate = sdf.parse(fromDateStr);
									Date todate = sdf.parse(toDateStr);

									if(customerwisecontractList.size()>0){

										for(Contract contractEnity : customerwisecontractList){
											if(contractEnity.getCancellationDate()!=null) {
												if((contractEnity.getCancellationDate().after(fromDate) && contractEnity.getCancellationDate().before(todate)) || 
														((contractEnity.getCancellationDate().equals(fromDate) || contractEnity.getCancellationDate().equals(todate)))) {
													contractList.add(contractEnity);
												}											
											}
										}
									}
								
								}
								catch(Exception e) {
									e.printStackTrace();
								}

								logger.log(Level.SEVERE, "Cancelled  contractList size = "+contractList.size());
						
					}
					
					if(contractList.size()==0 && apiCallfrom!=null && apiCallfrom.equals(AppConstants.CUSTOMERPORTAL) ) {
						JSONObject obj = new JSONObject();
						obj.put("Message", "No data found");
						jArray.add(obj);

					}

					//Ashwini Patil Date:19-06-2023
					if(apiCallfrom!=null&&apiCallfrom.equals("CustomerPortal")) {
						if(contractList!=null&&contractList.size()>1) {
							Comparator<Contract> dateComp=new Comparator<Contract>() {
								@Override
								public int compare(Contract arg0, Contract arg1) {
									return arg0.getStartDate().compareTo(arg1.getStartDate());
								}
							};
							Collections.sort(contractList, dateComp);
							
						}
					}
					
					Gson gson = new Gson();
					JSONObject errorObj = new JSONObject();
					for (int i = 0; i < contractList.size(); i++) {
						JSONObject obj = new JSONObject();
						obj.put("group", contractList.get(i).getGroup().trim());
						obj.put("type", contractList.get(i).getType().trim());
						obj.put("category", contractList.get(i).getCategory()
								.trim());
						
						if(contractStatus!=null&&!contractStatus.equals("")&&!contractStatus.equals("Cancelled")) {
							obj.put("status", contractStatus);
						}else
							obj.put("status", contractList.get(i).getStatus().trim());
						
						obj.put("contractDate",
								sdf.format(contractList.get(i).getContractDate()));
						obj.put("branch", contractList.get(i).getBranch().trim());
						obj.put("priority", contractList.get(i).getPriority()
								.trim());
						obj.put("netPayable", contractList.get(i).getNetpayable()
								+ "");
						obj.put("contractId", contractList.get(i).getCount() + "");
						obj.put("employeeName", contractList.get(i).getEmployee()
								.trim());
						
						if (contractList.get(i).getCinfo().getFullName().contains("\"")) {
							errorObj = new JSONObject();
							errorObj.put("Message","Remove double quote from customer name. Customer id:"+ contractList.get(i).getCinfo().getCount()+". Contact system administrator.");
							String jsonString = errorObj.toJSONString();
							logger.log(Level.SEVERE,"error message sent for customer name "+ contractList.get(i).getCinfo().getFullName() + "Customer id:"
											+ contractList.get(i).getCinfo().getCount()+ " Contract id:"+contractList.get(i).getCount());
							return jsonString;
						}
						obj.put("customerName", contractList.get(i).getCinfo()
								.getFullName().trim());
						obj.put("customerMobileNo", contractList.get(i).getCinfo()
								.getCellNumber()
								+ "");
						obj.put("customerId", contractList.get(i).getCinfo()
								.getCount()
								+ "");
						
						if(apiCallfrom!=null && apiCallfrom.equals(AppConstants.CUSTOMERPORTAL)){
							obj.put("productList",
									gson.toJson(contractList.get(i).getItems())
											.toString());
							logger.log(
									Level.SEVERE,
									"productList::::"
											+ gson.toJson(
													contractList.get(i).getItems())
													.toString());	
							
							logger.log(
									Level.SEVERE,
									"productList from contract::::"	+ contractList.get(i).getItems().toString());
						}else {

							//Ashwini Patil Date:18-04-2023
							JSONArray serviceProductarrayJson = new JSONArray();
							List<SalesLineItem> serviceProductList =contractList.get(i).getItems();
							for (int j = 0; j < serviceProductList.size(); j++) {
								JSONObject jobj = new JSONObject();
								jobj.put("id", serviceProductList.get(j).getPrduct().getCount());
								String productName=serviceProductList.get(j).getPrduct().getProductName();

								if (productName.contains("\"")) {
									errorObj = new JSONObject();
									errorObj.put("Message","Remove double quote from product name. Contract id:"+ contractList.get(i).getCount()+". Contact system administrator.");
									String jsonString = errorObj.toJSONString();
									logger.log(Level.SEVERE,"error message sent for product name "+ productName+ " Contract id:"+contractList.get(i).getCount());
									return jsonString;
								}
								jobj.put("productName",productName);
								jobj.put("price", serviceProductList.get(j).getPrduct().getPrice());
								jobj.put("noOfServices", serviceProductList.get(j).getNumberOfServices());
								jobj.put("productType",serviceProductList.get(j).getPrduct().getProductType());

								if (serviceProductList.get(j).getSpecification()!=null&&serviceProductList.get(j).getSpecification().contains("\"")) {
									errorObj = new JSONObject();
									errorObj.put("Message","Remove double quote from product specification. Contract id:"+ contractList.get(i).getCount()+". Contact system administrator.");
									String jsonString = errorObj.toJSONString();
									logger.log(Level.SEVERE,"error message sent for product specification "+ serviceProductList.get(j).getSpecification()+ " Contract id:"+contractList.get(i).getCount());
									return jsonString;
								}						
								jobj.put("specification",serviceProductList.get(j).getSpecification());
								
								jobj.put("specifications",serviceProductList.get(j).getSpecification());						
								jobj.put("duration",serviceProductList.get(j).getDuration());
								if(serviceProductList.get(j).getProductCategory()!=null)
									jobj.put("productCategory",serviceProductList.get(j).getProductCategory());
								else
									jobj.put("productCategory","");

								serviceProductarrayJson.add(jobj);
							}
							obj.put("productList",serviceProductarrayJson);
							
						}
						obj.put("totalNoOfServices", contractList.get(i).getItems()
								.size()
								+ "");
						
						if (contractList.get(i).getRemark()!=null&&contractList.get(i).getRemark().contains("\"")) {
							errorObj = new JSONObject();
							errorObj.put("Message","Remove double quote from Remark. Contract id:"+ contractList.get(i).getCount()+". Contact system administrator.");
							String jsonString = errorObj.toJSONString();
							logger.log(Level.SEVERE,"error message sent for remark "+ contractList.get(i).getRemark()+ " Contract id:"+contractList.get(i).getCount());
							return jsonString;
						}	
						obj.put("approveRejectionRemark", contractList.get(i)
								.getRemark() + "");
						obj.put("contrctStartDate",
								sdf.format(contractList.get(i).getStartDate()) + "");
						obj.put("contractEndDate",
								sdf.format(contractList.get(i).getEndDate()) + "");
						obj.put("approverName", contractList.get(i)
								.getApproverName() + "");
						obj.put("uniqueId", contractList.get(i).getId());
						jArray.add(obj);
					}
					

				} catch (Exception e) {
					e.printStackTrace();
				}

			}else
				return "Customer does no exist";
			
			}
			else {
				return "Customer id can not be zero";
			}
				
			String jsonString=jArray.toString().replace("\\\\", "");
			logger.log(Level.SEVERE, jsonString);
			
			return jsonString;
		}
	return null;
	}
			
		
}
