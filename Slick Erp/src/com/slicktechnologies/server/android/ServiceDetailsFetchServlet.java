package com.slicktechnologies.server.android;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
/**Description :
*  It is used to fetch invidual service details. Not using currently
*/
public class ServiceDetailsFetchServlet extends HttpServlet {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4753665349417475290L;
	/**
	 * 
	 */
	Logger logger = Logger.getLogger("ServiceDetailsFetchServlet.class");
	List<Service> serviceList = new ArrayList<Service>();
	List<ServiceProject> serviceProjectList = new ArrayList<ServiceProject>();
	Company comp;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doGet(req, resp);
		doPost(req, resp);
		logger.log(Level.SEVERE, "In do GetCalled from Android");
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		ServerAppUtility appUtility=new ServerAppUtility();
		String urlCalled=req.getRequestURL().toString().trim();
//		String url=urlCalled.replace("http://", "");
//		String[] splitUrl=url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		Exception exep = null;
		try {

			
			comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();	String serviceId = req.getParameter("serviceId").trim();
			logger.log(Level.SEVERE, serviceId);
			String companyId = comp.getCompanyId()+"";
			logger.log(Level.SEVERE, companyId);
			logger.log(Level.SEVERE, "Called from Android");

			serviceList = ofy().load().type(Service.class)
					.filter("companyId", Long.parseLong(companyId))
					.filter("count", Integer.parseInt(serviceId)).list();
			logger.log(Level.SEVERE, serviceList.size() + "");

			serviceProjectList = ofy().load().type(ServiceProject.class)
					.filter("companyId", Long.parseLong(companyId))
					.filter("serviceId", Integer.parseInt(serviceId)).list();

			logger.log(Level.SEVERE, serviceProjectList.size() + "");

			JSONArray jarray = new JSONArray();
			for (int i = 0; i < serviceList.size(); i++) {
				JSONObject jobj = new JSONObject();

				SimpleDateFormat spf = new SimpleDateFormat("dd MMM yyyy");
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				spf.setTimeZone(TimeZone.getTimeZone("IST"));
				jobj.put("serviceDate",
						spf.format(serviceList.get(i).getServiceDate()).trim());
				logger.log(
						Level.SEVERE,
						"Service Date::::::::::"
								+ spf.format(
										serviceList.get(i).getServiceDate())
										.trim());

				jobj.put("serviceTime", serviceList.get(i).getServiceTime()
						.trim());
				logger.log(Level.SEVERE, "serviceTime::::::::::"
						+ serviceList.get(i).getServiceTime().trim());

				jobj.put("serviceName", serviceList.get(i).getProductName()
						.trim());
				logger.log(Level.SEVERE, "serviceName::::::::::"
						+ serviceList.get(i).getProductName().trim());

				jobj.put("clientfullname", serviceList.get(i).getPersonInfo()
						.getFullName().trim());
				logger.log(Level.SEVERE, "clientfullname::::::::::"
						+ serviceList.get(i).getPersonInfo().getFullName()
								.trim());

				jobj.put("cellNo", (serviceList.get(i).getPersonInfo()
						.getCellNumber() + "").trim());
				logger.log(Level.SEVERE,
						"cellNo::::::::::"
								+ (serviceList.get(i).getPersonInfo()
										.getCellNumber() + "").trim());

				jobj.put("fullAddress", getFullAddress(i));
				logger.log(Level.SEVERE, "fullAddress::::::::::"
						+ (getFullAddress(i)));
				jobj.put("serviceProjectList", serviceProjectList.size() + "");
				
//				jObj.put("description",serviceList.get(i).get)
				jobj.put("description","Sample Decription");
				
				//Added by Apeksha on 06/10/2017 for adding serviceSerialNumber to corresponding Api
				jobj.put("serviceSerialNumber",serviceList.get(i).getServiceSerialNo());
				jobj.put("contractId",serviceList.get(i).getContractCount());
				
				logger.log(Level.SEVERE, "serviceProjectList::::::::::"
						+ serviceProjectList.size() + "");
				if (serviceProjectList.size() != 0) {

					// for (int j = 0; j < serviceProjectList.size(); j++) {

					try {
						JSONArray materialRequiredArray = new JSONArray();
						logger.log(Level.SEVERE,
								"materialRequiredArray.size():::"
										+ materialRequiredArray.size());
						logger.log(Level.SEVERE,
								"serviceProjectList.get(i).getProdDetailsList().size():::::::"
										+ serviceProjectList.get(i)
												.getProdDetailsList().size());

						for (int j = 0; j < serviceProjectList.get(i)
								.getProdDetailsList().size(); j++) {
							JSONObject obj = new JSONObject();
							obj.put("materialName", serviceProjectList.get(i)
									.getProdDetailsList().get(j).getName()
									.trim());
							obj.put("materialCode", serviceProjectList.get(i)
									.getProdDetailsList().get(j).getCode()
									.trim());
							obj.put("materialQty",
									(serviceProjectList.get(i)
											.getProdDetailsList().get(j)
											.getQuantity() + "").trim());
							materialRequiredArray.add(obj);
						}
						/**
						 * @author Anil , Date : 27-01-2020
						 */
//						jobj.put("materialRequired", materialRequiredArray);
						logger.log(Level.SEVERE,"Befor Material List : "+materialRequiredArray.toString());
						String materialArray=appUtility.encodeText(materialRequiredArray.toString());
						logger.log(Level.SEVERE,"After Material List : "+materialArray);
						jobj.put("materialRequired", materialArray);
						
						/**
						 * End
						 */

						try {
							logger.log(Level.SEVERE,
									"serviceProjectList.get(i).getTooltable().size()::::::::"
											+ serviceProjectList.get(i)
													.getTooltable().size());
							
							JSONArray toolsRequiredArray = new JSONArray();
							for (int j = 0; j < serviceProjectList.get(i)
									.getTooltable().size(); j++) {

								JSONObject obj = new JSONObject();
								obj.put("toolName", serviceProjectList.get(i)
										.getTooltable().get(j).getName().trim());
								// obj.put("toolBrand",
								// serviceProjectList.get(i)
								// .getTooltable().get(j).getBrand().trim());
								// obj.put("toolMNo",
								// (serviceProjectList.get(i).getTooltable()
								// .get(j).getModelNo() + "").trim());
								toolsRequiredArray.add(obj);
							}
							jobj.put("toolsRequired", toolsRequiredArray);
						} catch (Exception e) {
							e.printStackTrace();
							logger.log(Level.SEVERE,
									"ERROR in Tools Required:::" + e);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
				/*
				 * Name: Apeksha Gunjal
				 * Date: 23/08/2018 @19:20
				 * Note: Added Service serial number and service model number in 
				 * json object to show it in EVAPediO application.
				 */
				try{
					jobj.put("serialNumber", serviceList.get(i).getProSerialNo());
				}catch(Exception e){
					jobj.put("serialNumber", "");
					logger.log(Level.SEVERE, "Error Serial No. "+e);
				}
				try{
					jobj.put("modelNumber", serviceList.get(i).getProModelNo());
				}catch(Exception e){
					jobj.put("modelNumber", "");
					logger.log(Level.SEVERE, "Error Serial No. "+e);
				}
				/*
				 * end model no serial no
				 */
				/*
				 * Name: Apeksha Gunjal
				 * Date: 27/09/2018 @ 15:23
				 * Note: Added serviceCompleteDuration to jsonObject, which is in minutes
				 */
				try{
					jobj.put("serviceDuration", serviceList.get(i).getServiceCompleteDuration());
				}catch(Exception e){
					jobj.put("serviceDuration", "");
					logger.log(Level.SEVERE, "Error serviceDuration: "+e);
				}
				/*
				 * end serviceCompleteDuration...
				 */

				jarray.add(jobj);
			}

			// Gson gson=new Gson();
			// String json=gson.toJson(allocate);
			String jsonString = jarray.toJSONString().replaceAll("\\\\", "");
			logger.log(Level.SEVERE, jsonString);
			resp.getWriter().println(jsonString);

		} catch (Exception e) {
			logger.log(Level.SEVERE, "ERROR::::::::::::::" + e);
			e.printStackTrace();
		}

	}

	private Object getFullAddress(int i) {
		// TODO Auto-generated method stub
		String address = null;
		String add = null;
		String fullAddress = null;
		if (!serviceList.get(i).getAddress().getAddrLine2()
				.equalsIgnoreCase("")) {
			if (!serviceList.get(i).getAddress().getLandmark()
					.equalsIgnoreCase("")) {
				address = serviceList.get(i).getAddress().getAddrLine1() + ", "
						+ serviceList.get(i).getAddress().getAddrLine2() + ", "
						+ serviceList.get(i).getAddress().getLandmark();
			} else {
				address = serviceList.get(i).getAddress().getAddrLine1() + ", "
						+ serviceList.get(i).getAddress().getAddrLine2();
			}
		} else {
			if (!serviceList.get(i).getAddress().getLandmark()
					.equalsIgnoreCase("")) {
				address = serviceList.get(i).getAddress().getAddrLine1() + ", "
						+ serviceList.get(i).getAddress().getLandmark();
			} else {
				address = serviceList.get(i).getAddress().getAddrLine1();
			}
		}

		if (!serviceList.get(i).getAddress().getLocality().equalsIgnoreCase("")) {
			add = address + ", "
					+ serviceList.get(i).getAddress().getLocality();
		} else {
			add = address;
		}

		fullAddress = add + ", " + serviceList.get(i).getAddress().getCity()
				+ ", " + serviceList.get(i).getAddress().getState() + ", "
				+ serviceList.get(i).getAddress().getCountry() + ", Pin :"
				+ serviceList.get(i).getAddress().getPin();
		return fullAddress;

	}

}
