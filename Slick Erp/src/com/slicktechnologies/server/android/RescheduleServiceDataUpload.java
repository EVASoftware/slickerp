package com.slicktechnologies.server.android;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.device.TrackTableDetails;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.email.SendGridEmailServlet;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.ModificationHistory;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
/**
 * Description :This API is called from app at the time of service reschedule.
 * @author pc1
 *
 */
public class RescheduleServiceDataUpload extends HttpServlet {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3097219612395662781L;
	/**
	 * 
	 */
	Logger logger = Logger.getLogger("RescheduleServiceDataUpload.class");
	Company comp;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doGet(req, resp);

		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doPost(req, resp);
		
//		resp.setHeader("Access-Control-Allow-Origin", AppConstants.TRENDZWEBSOLUTION);
		resp.setHeader("Access-Control-Allow-Origin", "*");
		
		resp.setContentType("text/plain");
		String urlCalled=req.getRequestURL().toString().trim();
//		String url=urlCalled.replace("http://", "");
//		String[] splitUrl=url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		Exception exep = null;
		
		
		String apiCallfrom="";
		try {
			apiCallfrom = req.getParameter("apiCallFrom"); //portal
		} catch (Exception e) {
			// TODO: handle exception
		}
		/**
		 * ends here
		 */
		
		try {
			comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();	

		} catch (Exception e) {
		}
		
		/**
		 * @author Vijay Date :- 15-12-2022
		 * Des :- when API call from customer portal then auth code will validate
		 * if auth code is validated then only process the API 
		 */
		ServerAppUtility apputility = new ServerAppUtility();

//		try {
//			if(apiCallfrom!=null && !apiCallfrom.equals("")) {
//				String strcompanyId = req.getParameter("authCode");
//				ServerAppUtility apputility = new ServerAppUtility();
//				boolean authonticationflag = apputility.validateAuthCode(strcompanyId,comp.getCompanyId());
//				logger.log(Level.SEVERE, "authonticationflag"+authonticationflag);
//				if(!authonticationflag) {
//					resp.getWriter().println("Authentication Failed!");
//					return;
//				}
//			}
//		} catch (Exception e) {
//		}
		
		/**
		 * ends here
		 */
		
		try {

			String serviceId = req.getParameter("serviceId").trim();
			String companyId = comp.getCompanyId()+"";
			String rescheduleDate = req.getParameter("rescheduleDate").trim();
			String rescheduleTime = req.getParameter("rescheduleTime").trim();
			String rescheduleReason = req.getParameter("rescheduleReason")
					.trim();
			
			String technicianName = "";
			boolean syncFlag= false;
			String reportedData="";
			try {
				
				 technicianName = req.getParameter("technicianName").trim();
				  syncFlag=Boolean.parseBoolean(req.getParameter("syncFlag"));
					if(syncFlag){
						reportedData=req.getParameter("reportedData").trim();
					}
			} catch (Exception e) {
//				e.printStackTrace();
			}
			

			logger.log(Level.SEVERE, "serviceId" + serviceId);
			logger.log(Level.SEVERE, "companyId" + companyId);
			logger.log(Level.SEVERE, "rescheduleDate" + rescheduleDate);
			logger.log(Level.SEVERE, "rescheduleTime" + rescheduleTime);
			logger.log(Level.SEVERE, "rescheduleReason" + rescheduleReason);

			Service service = ofy().load().type(Service.class)
					.filter("companyId", Long.parseLong(companyId))
					.filter("count", Long.parseLong(serviceId)).first().now();

			logger.log(Level.SEVERE, "service" + service);
			
			/**
			 * @author Vijay
			 * Des :- when service reschedule from customer portal then in rescheduled by customer should updated so added below code
			 */ 
			if(apiCallfrom!=null && apiCallfrom.equals(AppConstants.CUSTOMERPORTAL) && service!=null){
				technicianName = service.getPersonInfo().getFullName(); 
			}
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			
//			SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
			sdf2.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			
			Date oldServiceDate = service.getServiceDate(); // Old ServiceDate
			try {
				service.setServiceDate(sdf.parse(rescheduleDate));
			} catch (Exception e) {
//				e.printStackTrace();
			}
			
			try {
				if(apiCallfrom!=null && apiCallfrom.equals(AppConstants.CUSTOMERPORTAL)){
					service.setServiceDate(sdf2.parse(rescheduleDate));
				}
			} catch (Exception e) {
//				e.printStackTrace();
			}
			
					
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ServiceStatusAsPlanned",Long.parseLong(companyId))){
				service.setStatus(Service.SERVICESTATUSREOPEN);
			}else{
				service.setStatus(Service.SERVICESTATUSRESCHEDULE);
			}
			service.setServiceScheduled(false);
			service.setServiceNumber("");
			
			if(rescheduleTime!=null&&!rescheduleTime.equals("")) {
				if(rescheduleTime.contains(":")) {
					String[] time = rescheduleTime.split(":");
					int hours = Integer.parseInt(time[0]);
					int mins = Integer.parseInt(time[1].substring(0, 2));
					logger.log(Level.SEVERE, "hours" + hours);
					logger.log(Level.SEVERE, "mins" + mins);

					if (hours > 12) {
						int hoursDecrease = 0;
						if (hours != 12) {
							hoursDecrease = 12;
						}
						if(mins<10){
							service.setServiceTime((hours - hoursDecrease) + ":" +"0"+mins + " PM");
						}else{
							service.setServiceTime((hours - hoursDecrease) + ":" + mins + " PM");
						}
						
						logger.log(Level.SEVERE, "Service Time::::::" + (hours - 12)
								+ ":" + mins + " PM");
					} else {
						logger.log(Level.SEVERE, "Service Time::::::" + hours + ":"
								+ mins + " AM");
						service.setServiceTime(hours + ":" + mins + " AM");
					}		
				}else {
					service.setServiceTime(rescheduleTime);
				}
					

			}
			service.setReasonForChange(rescheduleReason);

			ModificationHistory modHistory = new ModificationHistory();
			modHistory.setOldServiceDate(oldServiceDate);
			try {
				modHistory.setResheduleDate(sdf.parse(rescheduleDate));
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				if(apiCallfrom!=null && apiCallfrom.equals(AppConstants.CUSTOMERPORTAL)){
					modHistory.setResheduleDate(sdf2.parse(rescheduleDate));

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			modHistory.setUserName(technicianName);
			// SimpleDateFormat sdf1=new
			// SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
			// sdf1.setTimeZone(TimeZone.getTimeZone("IST"));
			modHistory.setSystemDate(new Date());
			if(rescheduleTime!=null&&!rescheduleTime.equals("")) {
				if(rescheduleTime.contains(":")) {
					String[] time = rescheduleTime.split(":");
					int hours = Integer.parseInt(time[0]);
					int mins = Integer.parseInt(time[1].substring(0, 2));
					if (hours >= 12) {
						int hoursDecrease = 0;
						if (hours != 12) {
							hoursDecrease = 12;
						}
						if(mins<10){
							modHistory.setResheduleTime((hours - hoursDecrease) + ":"+"0"+ mins + " PM");
						}else{
							modHistory.setResheduleTime((hours - hoursDecrease) + ":"
									+ mins + " PM");
						}
						
						logger.log(Level.SEVERE, "Service Time::::::"
								+ (hours - hoursDecrease) + ":" + mins + " PM");

					} else {
						logger.log(Level.SEVERE, "Service Time::::::" + hours + ":"
								+ mins + " AM");
						modHistory.setResheduleTime(hours + ":" + mins + " AM");
					}
			
				}else {
					modHistory.setResheduleTime(rescheduleTime);
				}
			}
			modHistory.setReason(rescheduleReason);
			
//			List<ModificationHistory> modList=new ArrayList<ModificationHistory>();
//			modList.add(modHistory);
//			ModificationHistoryTable table = new ModificationHistoryTable();
//			table.getDataprovider().getList().add(modHistory);
			service.getListHistory().add(modHistory);
			
			if(syncFlag){
				TrackTableDetails tracktabledetails=new TrackTableDetails();
				logger.log(Level.SEVERE,"Stage::::::::2");
				
				SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				sdf1.setTimeZone(TimeZone.getTimeZone("IST"));
				
				JSONObject reportedObj=new JSONObject(reportedData);
				
				tracktabledetails.setDate_time(sdf.format(reportedObj.opt("reportedDate").toString().trim()+" "+reportedObj.opt("reportedTime").toString().trim())+"");
				
				logger.log(Level.SEVERE,"Stage::::::::3");
				tracktabledetails.setStatus(Service.SERVICESTATUSREPORTED);
				logger.log(Level.SEVERE,"Stage::::::::4");
				
				//   rahul added this for null pointer exp in table 
//				if(service.getTrackServiceTabledetails()!=null){
				tracktabledetails.setSrNo(service.getTrackServiceTabledetails().size()+1);
//				}
//				else
//				{
//					tracktabledetails.setSrNo(1);
//				}
				service.getTrackServiceTabledetails().add(tracktabledetails);
				logger.log(Level.SEVERE,"Stage::::::::5"+service.getTrackServiceTabledetails().size());
			}
			
			ofy().save().entity(service).now();
			/**
			 * nidhi
			 * 30-07-2018
			 * for reschedule email 
			 * @author Anil @since 27-04-2021
			 * As per directed by Nitin sir and Ashwini, we should not send mail to company if rescheduled from pedio app
			 * raised by Ashwini for Pecopp
			 */
			try {
				//Ashwini Patil Date:4-09-2023 Ankita pest does not want this email to be sent to their customer
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "StopServiceRescheduleEmail(Pedio,CustomerPortal)", service.getCompanyId())){
					sendEmail(service,oldServiceDate);
				}				
				
			} catch (Exception e) {
			}
			
			
			
			if(service.isCallwiseService()){
				CustomerLogDetails comp=ofy().load().type(CustomerLogDetails.class).filter("companyId", service.getCompanyId()).filter("count", service.getContractCount()).first().now();
				logger.log(Level.SEVERE,"get  count  --" + service.getContractCount());
				if(comp!=null){
					logger.log(Level.SEVERE,"get  count  --" + service.getServiceDate());
					comp.setAppointmentDate(service.getServiceDate());
					
					ofy().save().entity(comp).now();
				}
			}
			/**
			 * nidhi
			 * end
			 */
		} catch (Exception e) {
			e.printStackTrace();
			exep = e;
			logger.log(Level.SEVERE, "ERROR::::::::::::::::" + e);
			if(apiCallfrom!=null && apiCallfrom.equals(AppConstants.CUSTOMERPORTAL)){
				resp.getWriter().println(apputility.getMessageInJson("Failed"));
			}
			else {
				resp.getWriter().println("Failed");
			}
		}
		if (exep == null) {
			if(apiCallfrom!=null && apiCallfrom.equals(AppConstants.CUSTOMERPORTAL)){
				resp.getWriter().println(apputility.getMessageInJson("Success"));
			}
			else {
				resp.getWriter().println("Success");
			}

		}

	}
	
	/**
	 * nidhi
	 * 30-07-2018
	 * for send reschedule email 
	 * @param service
	 */
private void sendEmail(Service service,Date prvdate){
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	sdf.setTimeZone(TimeZone.getTimeZone("IST"));
    
		String fromEmail="";
		Company comp=ofy().load().type(Company.class).filter("companyId", service.getCompanyId()).first().now();
//		logger.log(Level.SEVERE,"company --"+comp.getEmail());
		ArrayList<String> toEmailList = new ArrayList<String>();
		
		
		Customer customer = ofy().load().type(Customer.class).filter("companyId", service.getCompanyId())
			 	.filter("count", service.getPersonInfo().getCount()).first().now();
		
		if(customer!=null&customer.getEmail()!=null&&!customer.getEmail().equals(""))
			toEmailList.add(customer.getEmail());
		if(comp!=null){
//			if(comp.getEmail()!=null&&!comp.getEmail().equals("")){
//				fromEmail=comp.getEmail();
//			}
			fromEmail=ServerAppUtility.getCompanyEmail(service.getBranch(), comp);
		String techName  ="";
		if(service.getEmployee()!=null){
			techName= service.getEmployee();
		}
		String mailSub=""; //thank you for reaching out to us. you have enquired about product name
		String mailMsg="";
		StringBuilder builder = new StringBuilder();
		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("<table>");
		
		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Service Id");
		builder.append("</td>");
		builder.append("<td>:");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(service.getCount()+"");
		builder.append("</td>");
		builder.append("</tr>");
		
		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Service Name");
		builder.append("</td>");
		builder.append("<td>:");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(service.getProductName()+"");
		builder.append("</td>");
		builder.append("</tr>");
		
		builder.append("<tr>");
		builder.append("<td>");
		builder.append("New Date");
		builder.append("</td>");
		builder.append("<td>:");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(sdf.format(service.getServiceDate())+"  " + service.getServiceTime());
		builder.append("</td>");
		builder.append("</tr>");
		
		if(service.getReasonForChange()!=null&&!service.getReasonForChange().equals("")) {
			builder.append("<tr>");
			builder.append("<td>");
			builder.append("Reason for Change ");
			builder.append("</td>");
			builder.append("<td>:");
			builder.append("</td>");
			builder.append("<td>");
			builder.append(service.getReasonForChange()+"");
			builder.append("</td>");
			builder.append("</tr>");		
		}
		
		if(service.getEmployee()!=null&&!service.getEmployee().equals("")) {
			builder.append("<tr>");
			builder.append("<td>");
			builder.append("Opreator ");
			builder.append("</td>");
			builder.append("<td>:");
			builder.append("</td>");
			builder.append("<td>");
			builder.append(service.getEmployee()+"");
			builder.append("</td>");
			builder.append("</tr>");		
		}
		
		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Customer Name ");
		builder.append("</td>");
		builder.append("<td>:");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(service.getCustomerName()+"");
		builder.append("</td>");
		builder.append("</tr>");
		
		if(service.getPersonInfo().getCellNumber()!=null&&service.getPersonInfo().getCellNumber()>0) {
			builder.append("<tr>");
			builder.append("<td>");
			builder.append("Customer Cell");
			builder.append("</td>");
			builder.append("<td>:");
			builder.append("</td>");
			builder.append("<td>");
			builder.append(service.getPersonInfo().getCellNumber()+"");
			builder.append("</td>");
			builder.append("</tr>");		
		}
		
		
		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Address");
		builder.append("</td>");
		builder.append("<td>:");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(service.getAddrLine1()+", "+ 
				(service.getAddrLine2() != null ? (service.getAddrLine2().trim().length()>0 ? service.getAddrLine2().trim() +", " : "" ) : "" ) +
				(service.getAddress().getLandmark() != null ? (service.getAddress().getLandmark().trim().length()>0 ? service.getAddress().getLandmark().trim()+", "   : "" ) : "" )+
				(service.getAddress().getLocality() != null ? (service.getAddress().getLocality().trim().length()>0 ? service.getAddress().getLocality().trim()  +", " : "" ) : "" ) +
				(service.getAddress().getCity() != null ? (service.getAddress().getCity().trim().length()>0 ? service.getAddress().getCity().trim() +", " : " " ) : "" ) +
				(service.getAddress().getState() != null ? (service.getAddress().getState().trim().length()>0 ? service.getAddress().getState().trim() +", "  : "" ) : "" ) +
				(service.getAddress().getCountry() != null ? (service.getAddress().getCountry().trim().length()>0 ? service.getAddress().getCountry().trim() +", "  : "" ) : "" )+
				(service.getAddress().getPin()+"" != null ? (service.getAddress().getPin()>0 ? service.getAddress().getPin()+"" +", "  : " " ) : "" )
				);
		builder.append("</td>");
		builder.append("</tr>");
		builder.append("</table>");
		
		mailSub="Reschedule Service Id - " + service.getCount() + " / " +sdf.format(prvdate) + " / " +techName;
			mailMsg="Dear Sir/Madam"+","+"</br> </br> </br> </br>"
					+"    Following Service has been reschedule from "
					+ sdf.format(prvdate) + " to "+sdf.format(service.getServiceDate())+"."+"</br> </br>"
					+" " +builder.toString() +"</br> "+"</br> "+"</br> "+"Thanks & Regards, "+"</br> "+ service.getEmployee() +"</body> </html>" ;
							/*+"Service : "+service.getCount() +" has been reschedule by "+techName +
							"\n Remark : - " + service.getReasonForChange() +" \n"+"\n"+"\n";*/
			/*builder.append("</body>");
			builder.append("</html>");*/
			toEmailList.add(fromEmail);
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, service.getCompanyId())){
				logger.log(Level.SEVERE,"In send grid condition");
				SendGridEmailServlet sdEmail=new SendGridEmailServlet();
				sdEmail.sendMailWithSendGrid(fromEmail, toEmailList, null, null, mailSub, mailMsg, "text/html",null,null,"application/pdf",null);

			}else {				
				Email obj=new Email();
				obj.sendEmailforhtml(mailSub, mailMsg, toEmailList, new ArrayList<String>(), fromEmail);
				logger.log(Level.SEVERE,"EMAIL Sending --"+comp.getEmail());
			}

		}
		
	
	}
}
