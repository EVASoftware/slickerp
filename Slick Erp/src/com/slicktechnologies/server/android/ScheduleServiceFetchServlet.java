package com.slicktechnologies.server.android;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.device.CatchTraps;
import com.slicktechnologies.client.views.device.TrackTableDetails;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
/** Description : It is used to load all services for app.
*   Not using currently
**/
public class ScheduleServiceFetchServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -938059768018970433L;
	/**
	 * 
	 */
	List<Service> serviceList = new ArrayList<Service>();
	Logger logger = Logger.getLogger("ScheduleServiceFetchServlet.class");
	Company comp;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doGet(req, resp);
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// fo
		RegisterServiceImpl impl = new RegisterServiceImpl();
		impl.getClass();
		// super.doPost(req, resp);
		resp.setContentType("text/plain");
		// Exception exep=null;
		String urlCalled = req.getRequestURL().toString().trim();
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl::" + splitUrl);
		
		
		/*
		 * For Local
		 */
//		splitUrl[0] = "ganesh";
		Exception exep = null;
		try {

			comp = ofy().load().type(Company.class)
					.filter("accessUrl", splitUrl[0]).first().now();
			logger.log(Level.SEVERE, "Called from Android");
			String companyId = comp.getCompanyId() + "";
			logger.log(Level.SEVERE, "companyId::::::::::" + companyId);
			String team = req.getParameter("team").trim();
			logger.log(Level.SEVERE, "team::::::::::" + team);
			String type = req.getParameter("type").trim();
			logger.log(Level.SEVERE, "type::::::::::" + type);
			String fromDate=req.getParameter("fromDate").trim();
			logger.log(Level.SEVERE, "fromDate::::::::::" + fromDate);
			String toDate=req.getParameter("toDate").trim();
			logger.log(Level.SEVERE, "toDate::::::::::" + toDate);
			// This is for testing purpose
			//
			// String team="Team 1";
			SimpleDateFormat format = new SimpleDateFormat("HH:mm");
			format.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			calendar.add(Calendar.DAY_OF_YEAR, 1);
			Date afterDate = calendar.getTime();

			String aftrDateinString = sdf.format(afterDate);

			logger.log(Level.SEVERE, "Calendar afterDate:::::::" + afterDate);

			String date = sdf.format(new Date());
			logger.log(Level.SEVERE, "String new Date::::::::" + date);
			List<String> statusList = new ArrayList<String>();
			if (type.trim().equalsIgnoreCase("history")) {
				statusList.add(Service.SERVICESTATUSCOMPLETED);
				statusList.add(Service.SERVICESTATUSTECHCOMPLETED);
			} else if (type.trim().equalsIgnoreCase("schedule")) {
				statusList.add(Service.SERVICESTATUSSCHEDULE);
				statusList.add(Service.SERVICESTATUSRESCHEDULE);
				statusList.add(Service.SERVICESTATUSREPORTED);
				statusList.add(Service.SERVICESTATUSSTARTED);
				statusList.add(Service.SERVICETCOMPLETED);
			}

			User user = ofy().load().type(User.class)
					.filter("companyId", Long.parseLong(companyId))
					.filter("userName", team.trim()).first().now();
			logger.log(Level.SEVERE, "user.getEmployeeName().trim()::::::"
					+ user.getEmployeeName().trim());

			serviceList = ofy().load().type(Service.class)
					.filter("companyId", Long.parseLong(companyId))
					.filter("serviceDate >=", sdf.parse(fromDate))
					.filter("serviceDate <", sdf.parse(toDate))
					.filter("employee", user.getEmployeeName().trim())
					.filter("status IN", statusList).list();
			logger.log(Level.SEVERE,
					"After parsing new Date::::::::" + sdf.parse(date));
			logger.log(Level.SEVERE, "After parsing after new Date::::::::"
					+ sdf.parse(aftrDateinString));

			// .filter("status IN",statusList)
			// .filter("serviceDate <=", sdf.parse(date))
			logger.log(Level.SEVERE, "serviceList::::::::" + serviceList);
			logger.log(Level.SEVERE, "serviceList::::::::" + serviceList.size());

			JSONArray jarray = new JSONArray();
		label :	for (int i = 0; i < serviceList.size(); i++) {
				JSONObject jobj = new JSONObject();
				jobj.put("serviceId",
						(serviceList.get(i).getCount() + "").trim());
				logger.log(Level.SEVERE, "position " + i
						+ "Service ID::::::::::"
						+ (serviceList.get(i).getCount() + "").trim());

				SimpleDateFormat spf = new SimpleDateFormat("dd MMM yyyy");
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				spf.setTimeZone(TimeZone.getTimeZone("IST"));
				jobj.put("serviceDate",
						spf.format(serviceList.get(i).getServiceDate()).trim());
				logger.log(
						Level.SEVERE,
						"Service Date::::::::::"
								+ spf.format(
										serviceList.get(i).getServiceDate())
										.trim());

				jobj.put("serviceTime", serviceList.get(i).getServiceTime()
						.trim());
				logger.log(Level.SEVERE, "serviceTime::::::::::"
						+ serviceList.get(i).getServiceTime().trim());

				jobj.put("serviceName", serviceList.get(i).getProductName()
						.trim());
				logger.log(Level.SEVERE, "serviceName::::::::::"
						+ serviceList.get(i).getProductName().trim());

				jobj.put("clientfullname", serviceList.get(i).getPersonInfo()
						.getFullName().trim());
				logger.log(Level.SEVERE, "clientfullname::::::::::"
						+ serviceList.get(i).getPersonInfo().getFullName()
								.trim());

				jobj.put("cellNo", (serviceList.get(i).getPersonInfo()
						.getCellNumber() + "").trim());
				logger.log(Level.SEVERE,
						"cellNo::::::::::"
								+ (serviceList.get(i).getPersonInfo()
										.getCellNumber() + "").trim());

				jobj.put("locality", serviceList.get(i).getAddress()
						.getLocality());

				jobj.put("fullAddress", getFullAddress(i));

				jobj.put("status", serviceList.get(i).getStatus().trim());

				/*
				 * Name: Apeksha Gunjal
				 * Date: 07/06/2018 17:01
				 * Task: Added Premised details
				 */
				try{
					if(serviceList.get(i).getPremises() != null){
						jobj.put("premisesDetails", serviceList.get(i).getPremises());
					}else{
						jobj.put("premisesDetails", "");
					}
				}catch(Exception e){
					jobj.put("premisesDetails", "");
				}
				// Added by Apeksha on 06/10/2017 for adding serviceSerialNumber
				// to corresponding Api
				jobj.put("serviceSerialNumber", serviceList.get(i)
						.getServiceSerialNo());
				
				if (type.trim().equalsIgnoreCase("history")) {
					if(serviceList.get(i).getCustomerSignature()!=null){
					jobj.put("customerSignatureUrl", serviceList.get(i).getCustomerSignature().getUrl());
					}
					try{
					if(serviceList.get(i).getServiceWiseLatLong()!=null){
						jobj.put("serviceWiseLatLong", new org.json.JSONArray(serviceList.get(i).getServiceWiseLatLong().trim()));
						
					}
				}catch(Exception e){
					jobj.put("serviceWiseLatLong", "");
					e.printStackTrace();
				}
					/*
					 * Name: Apeksha Gunjal
					 * Date: 30/07/2018 @ 14:37
					 * Note: Adding technician remark to history details.
					 */
					try{
						logger.log(Level.SEVERE, "serviceList.get(i).getTechnicianRemark() ::::::::::"+serviceList.get(i).getTechnicianRemark());
						jobj.put("technicianRemark", serviceList.get(i).getTechnicianRemark());
					}catch(Exception e){
						jobj.put("technicianRemark", "");
						logger.log(Level.SEVERE, "Error ::::::::::"+e.getMessage());
					}
					try{
						if(serviceList.get(i).getServiceCompletionDate() != null){
							String time = format.format(serviceList.get(i).getServiceCompletionDate());
							jobj.put("completionTime", time);
						}
					}catch(Exception e){
						jobj.put("completionTime", "");
					}
					if(serviceList.get(i).getServiceImage1()!=null){
						jobj.put("image1", serviceList.get(i).getServiceImage1().getUrl());
						}
					if(serviceList.get(i).getServiceImage2()!=null){
						jobj.put("image2", serviceList.get(i).getServiceImage2().getUrl());
						}
					if(serviceList.get(i).getServiceImage3()!=null){
						jobj.put("image3", serviceList.get(i).getServiceImage3().getUrl());
						}
					if(serviceList.get(i).getServiceImage4()!=null){
						jobj.put("image4", serviceList.get(i).getServiceImage4().getUrl());
						}
				}
				
				/*
				 * Added by Apeksha Date: 12/10/2017 for adding description,
				 * contractId, material and tools required
				 */
				try {
					if (serviceList.get(i).getDescription() != null) {
						jobj.put("description", serviceList.get(i)
								.getDescription());
					}
				}catch(Exception e){
					jobj.put("description", " ");
				}
				
				jobj.put("contractId", serviceList.get(i).getContractCount());

				if (type.trim().equalsIgnoreCase("schedule")) {

					List<ServiceProject> serviceProjectList = ofy().load()
							.type(ServiceProject.class)
							.filter("companyId", Long.parseLong(companyId))
							.filter("serviceId", serviceList.get(i).getCount())
							.list();

					logger.log(Level.SEVERE, "serviceProjectList::::::::::"
							+ serviceProjectList.size() + "");

					jobj.put("serviceProjectListSize",
							serviceProjectList.size() + "");

					if (serviceProjectList.size() > 0) {
						logger.log(Level.SEVERE,
								"inside if serviceProjectList.size()> 0");
						JSONArray materialRequiredArray = new JSONArray();
						JSONArray toolsRequiredArray = new JSONArray();
						/** date 12.3.2019 added by komal for technician list **/
						JSONArray technicianArray = new JSONArray();
						for (int k = 0; k < serviceProjectList.size(); k++) {						
							logger.log(Level.SEVERE,
									"serviceProjectList.get(k).getProdDetailsList():::"+serviceProjectList.get(k).getProdDetailsList().size());
							if (serviceProjectList.get(k).getProdDetailsList().size() != 0) {
								if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "TechnicianwiseMaterialPopup" , comp.getCompanyId())){
									if(!serviceList.get(i).isServiceScheduled()){
										continue label;
									}
								
								}
								try {

									for (int j = 0; j < serviceProjectList
											.get(k).getProdDetailsList().size(); j++) {
										JSONObject obj = new JSONObject();
										logger.log(Level.SEVERE,"serviceProjectList.get(i).getProdDetailsList().get(j).getName()"+serviceProjectList.get(k).getProdDetailsList().get(j).getName());
										logger.log(Level.SEVERE,"serviceProjectList.get(i).getProdDetailsList().get(j).getName()"+serviceProjectList.get(k).getProdDetailsList().get(j).getCode());
										logger.log(Level.SEVERE,"serviceProjectList.get(i).getProdDetailsList().get(j).getName()"+serviceProjectList.get(k).getProdDetailsList().get(j).getQuantity());
										
										obj.put("materialName",
												serviceProjectList.get(k)
														.getProdDetailsList()
														.get(j).getName()
														.trim());
										obj.put("materialCode",
												serviceProjectList.get(k)
														.getProdDetailsList()
														.get(j).getCode()
														.trim());
										obj.put("materialQty",
												serviceProjectList.get(k)
														.getProdDetailsList()
														.get(j).getQuantity() + "");
										obj.put("unit",
												serviceProjectList.get(k)
														.getProdDetailsList()
														.get(j).getUnit() + "");
										
										materialRequiredArray.add(obj);
									}
									logger.log(
											Level.SEVERE,
											"materialRequiredArray.size():::"
													+ materialRequiredArray
															.size());
									jobj.put("materialRequired",
											materialRequiredArray);

								} catch (Exception e) {
									e.printStackTrace();
									logger.log(Level.SEVERE,"Error::::"+e);
								}
							}

							logger.log(Level.SEVERE,
									"serviceProjectList.get(k).getTooltable():::"+serviceProjectList.get(k).getTooltable().size());
							if (serviceProjectList.get(k).getTooltable().size() != 0) {
								try {

									for (int j = 0; j < serviceProjectList
											.get(k).getTooltable().size(); j++) {
										logger.log(Level.SEVERE,"serviceProjectList.get(k).getTooltable().get(j).getName()"+serviceProjectList.get(k).getTooltable().get(j).getName());
										
										JSONObject obj = new JSONObject();
										obj.put("toolName", serviceProjectList
												.get(k).getTooltable().get(j)
												.getName().trim());
										// obj.put("toolBrand",
										// serviceProjectList.get(i)
										// .getTooltable().get(j).getBrand().trim());
										// obj.put("toolMNo",
										// (serviceProjectList.get(i).getTooltable()
										// .get(j).getModelNo() + "").trim());
										toolsRequiredArray.add(obj);
									}
									logger.log(Level.SEVERE,
											"toolsRequiredArray.size():::"
													+ toolsRequiredArray.size());
									jobj.put("toolsRequired",
											toolsRequiredArray);
									/*
									 * } catch (Exception e) {
									 * e.printStackTrace();
									 * logger.log(Level.SEVERE,
									 * "ERROR in Tools Required:::" + e); }
									 */
								} catch (Exception e) {
									e.printStackTrace();
									logger.log(Level.SEVERE,"Error 2::::"+e);
								}
							}
							
							/** date 12.3.2019 added by komal fr technician list **/
							logger.log(Level.SEVERE,"serviceProjectList.get(i).getProdDetailsList().size() "+serviceProjectList.get(k).getTechnicians().size());
								if (serviceProjectList.get(k).getTechnicians().size() != 0) {
									try {

										for (int j = 0; j < serviceProjectList
												.get(k).getTechnicians().size(); j++) {
											JSONObject obj = new JSONObject();
											logger.log(Level.SEVERE,"serviceProjectList.get(i).getTechnicians().get(j).getName()"+serviceProjectList.get(k).getTechnicians().get(j).getFullName());
											
											
											obj.put("technicianName",
													serviceProjectList.get(k)
															.getTechnicians()
															.get(j).getFullName()
															.trim());
											if(serviceProjectList.get(k)
													.getTechnicians()
													.get(j).getFullName()
													.trim().equalsIgnoreCase(user.getEmployeeName().trim())){
												obj.put("mainTechnician", "YES");
											}else{
												obj.put("mainTechnician", "NO");
											}
											technicianArray.add(obj);
										}
										logger.log(
												Level.SEVERE,
												"materialRequiredArray.size():::"
														+ materialRequiredArray
																.size());
										jobj.put("technician",
												technicianArray);

									} catch (Exception e) {
										e.printStackTrace();
										logger.log(Level.SEVERE,"Error::::"+e);
									}
								}
						}
					}
				}
				/*
				 * Name: Apeksha Gunjal
				 * Date: 23/08/2018 @19:10
				 * Note: Added Service serial number and service model number in 
				 * json object to show it in EVAPediO application.
				 */
				try{
					jobj.put("serialNumber", serviceList.get(i).getProSerialNo());
				}catch(Exception e){
					jobj.put("serialNumber", "");
					logger.log(Level.SEVERE, "Error Serial No. "+e);
				}
				try{
					jobj.put("modelNumber", serviceList.get(i).getProModelNo());
				}catch(Exception e){
					jobj.put("modelNumber", "");
					logger.log(Level.SEVERE, "Error Serial No. "+e);
				}
				/*
				 * end model no serial no
				 */
				/**
				 * Rahul Verma added this on 04 Oct 2018
				 * Description: Multiple Technician for service
				 * 
				 */
				try{
					String technicians="";
					int count=0;
					
					if(serviceList.get(i).getEmployee() != null && !serviceList.get(i).equals("")){
						technicians = serviceList.get(i).getEmployee();
					}
					for(EmployeeInfo empInfo:serviceList.get(i).getTechnicians()){
						if(count==0 && !technicians.contains(serviceList.get(i).getEmployee())){
							technicians=empInfo.getFullName();
						}else if(!technicians.contains(serviceList.get(i).getEmployee())){
							technicians=technicians+","+empInfo.getFullName();
						}
						count++;
					}
					jobj.put("technicians", technicians);
				}catch(Exception e){
					jobj.put("technicians", "");
					logger.log(Level.SEVERE, "Error Serial No. "+e);
				}
				/**
				 * Ends
				 */

				jobj.put("isRateContractService", serviceList.get(i).isRateContractService());
				logger.log(Level.SEVERE, "status::::::::::"
						+ serviceList.get(i).getStatus().trim());

				/*
				 * Name: Apeksha Gunjal
				 * Date: 27/09/2018 @ 15:23
				 * Note: Added serviceCompleteDuration to jsonObject, which is in minutes
				 */
				try{
					jobj.put("serviceDuration", serviceList.get(i).getServiceCompleteDuration());
				}catch(Exception e){
					jobj.put("serviceDuration", "");
					logger.log(Level.SEVERE, "Error serviceDuration: "+e);
				}
				/*
				 * end serviceCompleteDuration...
				 */
				/**date 23.03.2019 to get trap details from service**/
				JSONArray trapDetailsArray = new JSONArray();
				JSONObject obj = new JSONObject();
				String fumigationType = "";
				if(serviceList.get(i).getProduct() != null && serviceList.get(i).getProduct().getProductCode() != null){
					if(serviceList.get(i).getFumigationProductCodes().contains(serviceList.get(i).getProduct().getProductCode())){
						fumigationType = "Fumigation";
					}else{
						fumigationType = "NonFumigation";
					}
				}
				try{
					jobj.put("type", fumigationType);
				}catch(Exception e){
					jobj.put("type", "");
					logger.log(Level.SEVERE, "Error serviceDuration: "+e);
				}
				if(serviceList.get(i).getCatchtrapList() != null && serviceList.get(i).getCatchtrapList().size() > 0){
					for(CatchTraps trap : serviceList.get(i).getCatchtrapList()){
						obj = new JSONObject();
						obj.put("name", trap.getPestName());
						obj.put("count", trap.getCount());
						obj.put("location", trap.getLocation());
						obj.put("number", trap.getContainerNo());
						obj.put("size", trap.getContainerSize());
						trapDetailsArray.add(obj);
					
					}
					logger.log(Level.SEVERE,"trapDetailsArray.size():::"+ trapDetailsArray.size());
					
				}
				jobj.put("trapDetails",trapDetailsArray);
				
				JSONArray statusArray = new JSONArray();
				JSONObject statusObject = null;
				Map<String , TrackTableDetails> trackMap = new HashMap<String , TrackTableDetails>();
				for(TrackTableDetails trackDetails : serviceList.get(i).getTrackServiceTabledetails()){
					if(trackMap.containsKey(trackDetails.getStatus())){
						if(trackMap.get(trackDetails.getStatus()).getSrNo() 
								< trackDetails.getSrNo()){
							trackMap.put(trackDetails.getStatus(), trackDetails);
						}
					}else{
						trackMap.put(trackDetails.getStatus(), trackDetails);
					}
				}
				for(Map.Entry<String, TrackTableDetails> entry : trackMap.entrySet()){
					TrackTableDetails trackDetails = entry.getValue();
					statusObject= new JSONObject();
					statusObject.put("dateTime", trackDetails.getDate_time());
					statusObject.put("status", trackDetails.getStatus());
					statusObject.put("location" , trackDetails.getLocationName());
					statusObject.put("latitude" , trackDetails.getLatitude());
					statusObject.put("longitude" , trackDetails.getLongitude());
					statusObject.put("km" , trackDetails.getKiloMeters());
					statusObject.put("time" , trackDetails.getTime());
					statusArray.add(statusObject);
				}
				jobj.put("trackingDetails", statusArray);
				
				jarray.add(jobj);
			}
			

			// Gson gson=new Gson();
			// String json=gson.toJson(allocate);
			String jsonString = jarray.toJSONString().replaceAll("\\\\", "");
			logger.log(Level.SEVERE, jsonString);
			resp.getWriter().println(jsonString);

		} catch (Exception e) {
			logger.log(Level.SEVERE, "ERROR:::::::::::::::::::::" + e);
			// exep=e;
			e.printStackTrace();
			resp.getWriter().println("Failed");
		}

	}

	private Object getFullAddress(int i) {
		// TODO Auto-generated method stub
		String address = null;
		String add = null;
		String fullAddress = null;
		if (!serviceList.get(i).getAddress().getAddrLine2()
				.equalsIgnoreCase("")) {
			if (!serviceList.get(i).getAddress().getLandmark()
					.equalsIgnoreCase("")) {
				address = serviceList.get(i).getAddress().getAddrLine1() + ", "
						+ serviceList.get(i).getAddress().getAddrLine2() + ", "
						+ serviceList.get(i).getAddress().getLandmark();
			} else {
				address = serviceList.get(i).getAddress().getAddrLine1() + ", "
						+ serviceList.get(i).getAddress().getAddrLine2();
			}
		} else {
			if (!serviceList.get(i).getAddress().getLandmark()
					.equalsIgnoreCase("")) {
				address = serviceList.get(i).getAddress().getAddrLine1() + ", "
						+ serviceList.get(i).getAddress().getLandmark();
			} else {
				address = serviceList.get(i).getAddress().getAddrLine1();
			}
		}

		if (!serviceList.get(i).getAddress().getLocality().equalsIgnoreCase("")) {
			add = address + ", "
					+ serviceList.get(i).getAddress().getLocality();
		} else {
			add = address;
		}

		fullAddress = add + ", " + serviceList.get(i).getAddress().getCity()
				+ ", " + serviceList.get(i).getAddress().getState() + ", "
				+ serviceList.get(i).getAddress().getCountry() + ", Pin :"
				+ serviceList.get(i).getAddress().getPin();
		return fullAddress;
	}

}
