package com.slicktechnologies.server.android.hr;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.android.LoginStatusCheckServlet;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.attendance.EmployeeAttendance;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class LoadAttendanceFromEmployee extends HttpServlet{

	/**Description : 
	 *  It is used to fetch attendance details of employees for given date , project , shift.
	 */
	private static final long serialVersionUID = -7558811188756070135L;
	/**
	 * 
	 */
	Logger logger = Logger.getLogger("LoadAttendanceFromEmployee.class");
	
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		spf.setTimeZone(TimeZone.getTimeZone("IST"));
		RegisterServiceImpl impl=new RegisterServiceImpl();
		impl.getClass();
		String urlCalled = req.getRequestURL().toString().trim();
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
		
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		Company company = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
		if(company==null){
			company=ofy().load().type(Company.class).filter("accessUrl","127").first().now();
		}
		
		String employeeData=req.getParameter("employeeData");
		String userRole = req.getParameter("employeeRole");
		String employeeName = req.getParameter("employeeName");
		
		/**
		 * @author Anil
		 * @since 18-05-2020
		 * checking license details at ateh
		 */
		
		User user= ofy().load().type(User.class).filter("companyId", company.getCompanyId())
				.filter("employeeName", employeeName).first().now();
		LoginStatusCheckServlet logStatus = new LoginStatusCheckServlet();
		String returnValue = logStatus.validateLicenseDate(company.getCompanyId(),user,"Attendance","");
		
		if(!returnValue.equalsIgnoreCase("Success")){
			resp.getWriter().println(returnValue);
			return;
		}
		
		/**
		 * End
		 */
		
		
		JSONArray jsonArray=null;
		try {
			jsonArray=new JSONArray(employeeData.trim());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONArray arrayOfAttendance=new JSONArray();
		
		String attendanceDate="";
		String projectName="";
		String shift = "";
		String jsonString  = "";
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jObj = null;
			try {
				jObj = jsonArray.getJSONObject(i);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			projectName=jObj.optString("projectName");
			attendanceDate=jObj.optString("attendanceDate");
			shift=jObj.optString("shift");
			
			
			/**
			 * Date 11-1-2020 by AMOL FOR SUMMARY DETAILS
			 */
			
			if(projectName.equalsIgnoreCase("All")||shift.equalsIgnoreCase("All")){
				logger.log(Level.SEVERE,"INSIDE ALL PROJECTS AND ALL SHIFT ");
				List<ProjectAllocation> projectListFinal = new ArrayList<ProjectAllocation>();
				if(userRole==null||userRole.equals("")){
					resp.getWriter().println("User Role is null.");
					return ;
				}
				if(employeeName==null||employeeName.equals("")){
					logger.log(Level.SEVERE,"employee name is blank ");
					resp.getWriter().println("Employee Name is null.");
					return ;
				}
				
				
				List<ProjectAllocation> projectListOfManager = new ArrayList<ProjectAllocation>();
				List<ProjectAllocation> projectListOfSuperVisor = new ArrayList<ProjectAllocation>();
				
				List<ProjectAllocation> projectList = new ArrayList<ProjectAllocation>();
				
				if(projectName.equalsIgnoreCase("All")){
				
					if (!userRole.trim().equalsIgnoreCase("Admin")) {
						projectListOfManager = ofy().load().type(ProjectAllocation.class).filter("companyId", company.getId())
								.filter("managerName", employeeName.trim()).list();
						
						logger.log(Level.SEVERE,"projectlist of manager "+projectListOfManager.size());
						
						
						if (projectListOfManager != null) {
							projectList.addAll(projectListOfManager);
						}
						projectListOfSuperVisor = ofy().load().type(ProjectAllocation.class).filter("companyId", company.getId())
								.filter("supervisorName", employeeName.trim()).list();
						
						
						logger.log(Level.SEVERE,"projectlist of supervisor "+projectListOfSuperVisor.size());
						
						
						if (projectListOfSuperVisor != null) {
							projectList.addAll(projectListOfSuperVisor);
						}
						HashSet<Integer> uniqueProjectIdSet = new HashSet<Integer>();
						for (ProjectAllocation projectAllocation : projectList) {
							uniqueProjectIdSet.add(projectAllocation.getCount());
						}
	                  
						logger.log(Level.SEVERE,"unique project SET  "+uniqueProjectIdSet.size());
						
						for (ProjectAllocation projectAllocation : projectList) {
							for (int integerValue : uniqueProjectIdSet) {
								if (integerValue == projectAllocation.getCount()) {
									projectListFinal.add(projectAllocation);
								}
							}
						}
						logger.log(Level.SEVERE,"projectList Final  "+projectListFinal.size());
					} else {
						projectListFinal = ofy().load()
								.type(ProjectAllocation.class)
								.filter("companyId", company.getId()).list();
						
						
						logger.log(Level.SEVERE," ALLLL projectList Final"+projectListFinal.size());
					}
					JSONArray empSummaryArray=new JSONArray();
					for(ProjectAllocation project:projectListFinal){
						ArrayList<String> shiftList=new ArrayList<String>();
						HashSet<String> shiftHs=new HashSet<String>();
						if(shift.equalsIgnoreCase("All")){
							/**
							 * @author Vijay @Since 21-09-2022
							 * Loading employee project allocation from different entity
							 */
							   CommonServiceImpl commonservice = new CommonServiceImpl();
							   List<EmployeeProjectAllocation> empprojectallocationlist = commonservice.getEmployeeProjectAllocationlist(project);
						   /**
						     * ends here
						     */
//							for(EmployeeProjectAllocation empProj:project.getEmployeeProjectAllocationList()){
							for(EmployeeProjectAllocation empProj: empprojectallocationlist){
								shiftHs.add(empProj.getShift());
							}
							shiftList.addAll(shiftHs);
							logger.log(Level.SEVERE," SHIFT LIST SET "+shiftList.size());
							projectName=project.getProjectName();
							for(String shiftName:shiftList){
								/**
								 * @author Vijay Date 19-11-2020 by Vijay
								 * Des :- in history inactive project attendence should not show so updated code with check project status
								 */
								HrProject hrProject = ofy().load().type(HrProject.class).filter("companyId", company.getCompanyId()).
										filter("projectName", projectName).filter("status", true).first().now();
								if(hrProject!=null) {
									
								try {
									JSONObject attendanceJson = getAttendanceString(projectName ,spf.parse(attendanceDate) , shiftName,company,userRole,employeeName);
									empSummaryArray.put(attendanceJson);
									
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
								}
							}
						}
					}
					JSONObject json=new JSONObject();
					try {
						json.put("employeeProjectSummary", empSummaryArray);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					jsonString=json.toString().replaceAll("\\\\", "");
					logger.log(Level.SEVERE,jsonString);
					resp.getWriter().println(jsonString);
					return;
				
				}else if(!projectName.equalsIgnoreCase("All")&&shift.equalsIgnoreCase("All")){
					
					projectListFinal = ofy().load().type(ProjectAllocation.class).filter("companyId", company.getId()).filter("projectName",projectName).list();
					
					
					logger.log(Level.SEVERE," ALL SHIFT projectListFinal "+projectListFinal.size());
					
					
					JSONArray empSummaryArray=new JSONArray();
					for(ProjectAllocation project:projectListFinal){
						ArrayList<String> shiftList=new ArrayList<String>();
						HashSet<String> shiftHs=new HashSet<String>();
						if(shift.equalsIgnoreCase("All")){
							/**
							 * @author Vijay @Since 21-09-2022
							 * Loading employee project allocation from different entity
							 */
							   CommonServiceImpl commonservice = new CommonServiceImpl();
							   List<EmployeeProjectAllocation> empprojectallocationlist = commonservice.getEmployeeProjectAllocationlist(project);
						    /**
						     * ends here
						     */
//							for(EmployeeProjectAllocation empProj:project.getEmployeeProjectAllocationList()){
							for(EmployeeProjectAllocation empProj: empprojectallocationlist){
								shiftHs.add(empProj.getShift());
							}
							shiftList.addAll(shiftHs);
							
							
							logger.log(Level.SEVERE," ALL SHIFT LISTTTTT "+shiftList.size());
							projectName=project.getProjectName();
							for(String shiftName:shiftList){
//								shift=shiftName;
								try {
									JSONObject attendanceJson = getAttendanceString(projectName ,spf.parse(attendanceDate) , shiftName,company,userRole,employeeName);
									empSummaryArray.put(attendanceJson);
									
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
					}
					JSONObject json=new JSONObject();
					try {
						json.put("employeeProjectSummary", empSummaryArray);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					jsonString=json.toString().replaceAll("\\\\", "");
					logger.log(Level.SEVERE,jsonString);
					resp.getWriter().println(jsonString);
					return;
					
				}
				
				
				/// return statement
				
			}
			
			
			
			/**
			 * end of summary details by AMOL
			 */
			
			
			try {
				JSONObject json = getAttendanceString(projectName ,spf.parse(attendanceDate) , shift,company,userRole,employeeName);
				jsonString=json.toString().replaceAll("\\\\", "");
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		logger.log(Level.SEVERE,jsonString);
		resp.getWriter().println(jsonString);
	}
	public JSONObject getAttendanceString(String projectName ,Date attendanceDate , String shift,
			Company company , String userRole , String employeeName){
		String approver = "";
		Date approvedDate = null;
		
		SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		spf.setTimeZone(TimeZone.getTimeZone("IST"));
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		format.setTimeZone(TimeZone.getTimeZone("IST"));
		
		ArrayList<Integer> employeeIdList=new ArrayList<Integer>();
		/**
		 * @author Anil , Date : 03-01-2020
		 * Multiple attendance details are shown against one employee
		 */
		HashSet<Integer> employeeIdSet=new HashSet<Integer>();
		
		HrProject project = ofy().load().type(HrProject.class).filter("companyId", company.getCompanyId()).
				filter("otList.shift", shift).
				filter("projectName", projectName).filter("status", true).first().now();
		logger.log(Level.SEVERE , "hrProject object :" + project);
		
		if(project != null){
		
			/**
			 * @author Vijay Date :- 08-09-2022
			 * Des :- used common method to read OT from project or new entity HrProjectOvertime`
			 */
			ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
			CommonServiceImpl commonservice = new CommonServiceImpl();
			overtimelist = commonservice.getHRProjectOvertimelist(project);
			/**
			 * ends here
			 */
//			for(Overtime overtime : project.getOtList()){
			for(Overtime overtime : overtimelist){
				if(overtime.getShift().equalsIgnoreCase(shift)){
					if(userRole.trim().equalsIgnoreCase("ADMIN") ||
							userRole.trim().equalsIgnoreCase("Manager")  ||
								userRole.trim().equalsIgnoreCase("Supervisor") ){
						
					}else if(userRole.trim().equalsIgnoreCase("Sales")){
						if(!employeeName.trim().equalsIgnoreCase(overtime.getEmpName())){
							continue;
						}
					}else{
						if(!employeeName.trim().equalsIgnoreCase(overtime.getEmpName())){
							continue;
						}
					}
//					employeeIdList.add(overtime.getEmpId());
					employeeIdSet.add(overtime.getEmpId());
				}
			}
		}
		/**
		 * 
		 */
		if(employeeIdSet!=null&&employeeIdSet.size()!=0){
			employeeIdList.addAll(employeeIdSet);
		}
		
//		JSONArray employeeListArray=jObj.optJSONArray("employeeList");
//		for (int j = 0; j < employeeListArray.length(); j++) {
//			try{
//			JSONObject empObj=employeeListArray.optJSONObject(j);
//			String employeeId=empObj.optString("employeeId");
//			employeeIdList.add(Integer.parseInt(employeeId));	
//			}catch(Exception e){
//				logger.log(Level.SEVERE,"Error: "+e);
//			}
//		}
	//}
	List<Attendance> attendanceList=new ArrayList<Attendance>();
	logger.log(Level.SEVERE,"spf.parse(attendanceDate)"+attendanceDate);
	logger.log(Level.SEVERE,"company.getCompanyId()"+company.getCompanyId());
	logger.log(Level.SEVERE,"projectName"+projectName);
	try {
		attendanceList=ofy().load().type(Attendance.class)
				.filter("companyId",company.getCompanyId())
				.filter("attendanceDate",attendanceDate)
				.filter("projectName",projectName.trim())
				.filter("empId IN", employeeIdList).list();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	logger.log(Level.SEVERE,"attendanceList"+attendanceList.size());
	logger.log(Level.SEVERE,"employeeIdList"+employeeIdList.size());
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	sdf.setTimeZone(TimeZone.getTimeZone("IST"));
	JSONArray employeeAttendanceArray=new JSONArray();
	String status = Attendance.ATTENDANCECREATED;
	for (int employeeId : employeeIdList) {
		
		boolean employeeMatched=false;
		for (Attendance attendace : attendanceList) {
			if(employeeId==attendace.getEmpId()){
				logger.log(Level.SEVERE,"Attendance is present of : "+employeeId+ " of project: "+projectName);
				employeeMatched=true;
				JSONObject empObjData=new JSONObject();
				
				try {
					empObjData.put("count",attendace.getCount());
					empObjData.put("createdBy",attendace.getCreatedBy());
					empObjData.put("userId",attendace.getUserId());
					empObjData.put("status",attendace.getStatus());
					empObjData.put("employee",attendace.getEmployee());
					empObjData.put("branch",attendace.getBranch());
					empObjData.put("creationTime",attendace.getCreationTime());
					empObjData.put("approvalTime",attendace.getApprovalTime());
					empObjData.put("creationDate",attendace.getCreationDate());
					empObjData.put("approvalDate",attendace.getApprovalDate());
					empObjData.put("approverName",attendace.getApproverName());
					empObjData.put("group",attendace.getGroup());
					empObjData.put("category",attendace.getCategory());
					empObjData.put("type",attendace.getType());
					empObjData.put("remark",attendace.getRemark());
					empObjData.put("attendanceDate",spf.format(attendace.getAttendanceDate()));
					empObjData.put("projectNameForOT",attendace.getProjectNameForOT());
					empObjData.put("leaveType",attendace.getLeaveType());
					empObjData.put("overtimeType",attendace.getOvertimeType());
					
					/* Name: Apeksha Gunjal 
					 * Date: 5/08/2018  @8:30 Sunday
					 * Note: Add profile photo url of an employee
					 */
					try{
						empObjData.put("updatedEmployeeRole",attendace.getUpdatedEmployeeRole());
						empObjData.put("updatedEmployeeName",attendace.getUpdatedEmployeeName());
					}catch(Exception e){
						logger.log(Level.SEVERE , "employee role and name");
						empObjData.put("updatedEmployeeRole","");
						empObjData.put("updatedEmployeeName","");
					}
					
					try{
						Employee emp = ofy().load()
								.type(Employee.class)
								.filter("companyId",company.getCompanyId())
								.filter("count", employeeId).first().now();
						logger.log(Level.SEVERE,"employee: "+emp);
						logger.log(Level.SEVERE,"employeeProfileUrl:fullName: "+emp.getFullname());
						try{
							empObjData.put("outStationEmployee",emp.isOutstationEmployee()+"");
						}catch(Exception e){
							empObjData.put("outStationEmployee",false+"");
							e.printStackTrace();
						}
						try {
							empObjData.put("fingerDataPresent",
									emp.isFingerDataPresent()+"");
						} catch (Exception e) {
							empObjData.put("fingerDataPresent", false + "");
							// TODO Auto-generated catch block
							logger.log(Level.SEVERE, "employeefinger: Error: " + e);
						}
						empObjData.put("employeeProfileUrl",emp.getPhoto().getUrl());
						logger.log(Level.SEVERE,"employeeProfileUrl: "+emp.getPhoto().getUrl());
					}catch(Exception e){
						logger.log(Level.SEVERE,"employeeProfileUrl:Error: "+e);
						empObjData.put("employeeProfileUrl","");
					}
					try{
						EmployeeAttendance emp = ofy().load()
								.type(EmployeeAttendance.class)
								.filter("companyId",company.getCompanyId())
								.filter("count", employeeId).first().now();
						logger.log(Level.SEVERE,"Aemployee: "+emp);
						logger.log(Level.SEVERE,"AemployeeProfileUrl:fullName: "+emp.getEmpName());
						logger.log(Level.SEVERE,"AemployeeProfileUrl:Else: "+emp.getEmployeeInfo().getString());
					}catch(Exception e){
						logger.log(Level.SEVERE,"employeeProfileUrl:Else Error: "+e);
					}
					/* Name: Apeksha Gunjal 
					 * Date: 15/07/2018 
					 * Note: if in-time & out-time is blank or null..
					 */
					try{
						empObjData.put("inTime",sdf.format(attendace.getInTime()));
					}catch(Exception e){
						empObjData.put("inTime","");
					}
					try{
						if(attendace.getOutTime()!=null)
							empObjData.put("outTime",sdf.format(attendace.getOutTime()));
						else
							empObjData.put("outTime", "");
					}catch(Exception e){
						empObjData.put("outTime", "");
					}
					empObjData.put("taskName",attendace.getTaskName());
					empObjData.put("isOvertime",attendace.isOvertime());
					empObjData.put("isLeave",attendace.isLeave());
					empObjData.put("isPresent",attendace.isPresent());
					empObjData.put("isHalfDay",attendace.isHalfDay());
					empObjData.put("totalWorkedHours",attendace.getTotalWorkedHours());
					empObjData.put("overtimeHours",attendace.getOvertimeHours());
					empObjData.put("empId",attendace.getEmpId());
					empObjData.put("employeeName",attendace.getEmployeeName());
					empObjData.put("approverName",attendace.getApproverName());
					empObjData.put("approverRemark",attendace.getApproverRemark());
					empObjData.put("status",attendace.getStatus());
					empObjData.put("actionLabel",attendace.getActionLabel());
					empObjData.put("shift",attendace.getShift());
					empObjData.put("workingHours",attendace.getWorkingHours());
					empObjData.put("workedHours",attendace.getWorkedHours());
					empObjData.put("projectName",attendace.getProjectName());
					empObjData.put("otHours",attendace.getOvertimeHours());
					empObjData.put("month",attendace.getMonth());
					
					/* Name: Apeksha Gunjal 
					 * Date: 15/07/2018 
					 * Note: Send these parameter which are not in previous version
					 */
					empObjData.put("isEarlyMarkInTime",attendace.isEarlyMarkInTime());
					empObjData.put("isEarlyMarkOutTime",attendace.isEarlyMarkOutTime());
					empObjData.put("isLateMarkInTime",attendace.isLateMarkInTime());
					empObjData.put("isLateMarkOutTime",attendace.isLateMarkOutTime());
					
					SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));
					sdfTime.setTimeZone(TimeZone.getTimeZone("IST"));
					if(attendace.getTargetInTime()!=null)
						empObjData.put("targetInTime",sdfTime.format(attendace.getTargetInTime()));
					else
						empObjData.put("targetOutTime","");
					if(attendace.getTargetInTime()!=null)
						empObjData.put("targetOutTime",sdfTime.format(attendace.getTargetOutTime()));
					else
						empObjData.put("targetOutTime","");
					empObjData.put("empDesignation",
							attendace.getEmpDesignation());
					//Added by apeksha on 11/08/2018
					try{
						empObjData.put("requestedHours",attendace.getRequestedOTHours());
						empObjData.put("OTStatus",attendace.getOtStatus());
						empObjData.put("approvedHours",attendace.getApprovedOTHOURS());
					}catch(Exception e){
						logger.log(Level.SEVERE,"error: "+e);
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				try {
					empObjData.put("leaveHours",attendace.getLeaveHours());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/* Name: Apeksha Gunjal 
				 * Date: 16/07/2018 @12:21
				 * Note: Send these parameter which are not in previous version
				 */
				try{
					try {
						empObjData.put("serverInTime",attendace.getServerInTime());
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						empObjData.put("serverInTime","");
					}
					try {
						empObjData.put("serverOutTime",attendace.getServerOutTime());
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						empObjData.put("serverOutTime","");
					}
					/**
					 * Rahul Verma added this on 05 Oct 2018
					 * 
					 */
					try {
						empObjData.put("markInAddress",attendace.getMarkInAddress());
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						empObjData.put("markInAddress","");
					}
					try {
						empObjData.put("markOutAddress",attendace.getMarkOutAddress());
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						empObjData.put("markOutAddress","");
					}
					/**
					 * Ends
					 */
					status = attendace.getStatus();
						
					
				}catch(Exception e){
					logger.log(Level.SEVERE,"error: "+e);
				}
				if(status.equals(Contract.APPROVED)){
					approver = attendace.getApproverName();
					approvedDate = attendace.getApprovalDate();
				}else{
					if(approvedDate == null){
						approvedDate = attendace.getCreationDate();
						approver     = attendace.getCreatedBy();
					}else{
						if(attendace.getCreationDate().after(approvedDate)){
							approvedDate = attendace.getCreationDate();
							approver     = attendace.getCreatedBy();
						}
					}
				}
				logger.log(Level.SEVERE , "date and approver :"+ approvedDate +" "+approver);
				employeeAttendanceArray.put(empObjData);
			}
		}
		if(!employeeMatched){
			logger.log(Level.SEVERE,"Attendance is not present of : "+employeeId+ " of project: "+projectName);
			employeeMatched=false;
			JSONObject empObjData=new JSONObject();
			try {
				empObjData.put("count","0");
				empObjData.put("createdBy","");
				empObjData.put("userId","");
				empObjData.put("status","");
				empObjData.put("employee","");
				empObjData.put("branch","");
				empObjData.put("creationTime","");
				empObjData.put("approvalTime","");
				empObjData.put("creationDate","");
				empObjData.put("approvalDate","");
				empObjData.put("approverName","");
				empObjData.put("group","");
				empObjData.put("category","");
				empObjData.put("type","");
				empObjData.put("remark","");
				empObjData.put("attendanceDate","");
				empObjData.put("projectNameForOT","");
				empObjData.put("leaveType","");
				empObjData.put("overtimeType","");	
				logger.log(Level.SEVERE , "employee role and name");
				empObjData.put("updatedEmployeeRole","");
				empObjData.put("updatedEmployeeName","");
				empObjData.put("attendanceLocation","");
				
				
				
				
				/* Name: Apeksha Gunjal 
				 * Date: 5/08/2018 @8:30 Sunday
				 * Note: Add profile photo url of an employee
				 */
				Employee emp = null;
				try{
					emp = ofy().load()
							.type(Employee.class)
							.filter("companyId",company.getCompanyId())
							.filter("count", employeeId).first().now();
				}catch(Exception e){
					logger.log(Level.SEVERE,"emp loading Error: "+e);
				}
					logger.log(Level.SEVERE,"employee: "+emp);
					
					try{
						logger.log(Level.SEVERE,"employeeProfileUrl:fullName: "+emp.getFullname());
						empObjData.put("employeeName",emp.getFullname());
					}catch(Exception e){
						empObjData.put("employeeName","");
					}
					try{
						empObjData.put("empDesignation",emp.getDesignation());
					}catch(Exception e){
						empObjData.put("empDesignation","");
					}
					try{
						empObjData.put("employeeProfileUrl",emp.getPhoto().getUrl());
						logger.log(Level.SEVERE,"employeeProfileUrl:Else: "+emp.getPhoto().getUrl());
					}catch(Exception e){
						logger.log(Level.SEVERE,"employeeProfileUrl:Else Error: "+e);
						empObjData.put("employeeProfileUrl","");
					}
					
					try{
						empObjData.put("outStationEmployee",emp.isOutstationEmployee()+"");
					}catch(Exception e){
						empObjData.put("outStationEmployee",false+"");
						e.printStackTrace();
					}
					try {
						empObjData.put("fingerDataPresent",
								emp.isFingerDataPresent()+"");
					} catch (Exception e) {
						empObjData.put("fingerDataPresent", false + "");
						// TODO Auto-generated catch block
						logger.log(Level.SEVERE, "employeefinger: Error: " + e);
					}
				/*try{
					EmployeeAttendance emp = ofy().load()
							.type(EmployeeAttendance.class)
							.filter("companyId",company.getCompanyId())
							.filter("count", employeeId).first().now();
					logger.log(Level.SEVERE,"Aemployee: "+emp);
					logger.log(Level.SEVERE,"AemployeeProfileUrl:fullName: "+emp.getEmpName());
					logger.log(Level.SEVERE,"AemployeeProfileUrl:Else: "+emp.getEmployeeInfo().getString());
				}catch(Exception e){
					logger.log(Level.SEVERE,"employeeProfileUrl:Else Error: "+e);
				}*/
				
				/* Name: Apeksha Gunjal 
				 * Date: 15/07/2018 
				 * Note: if in-time & out-time is blank or null..
				 */
				try{
					empObjData.put("inTime","");
					empObjData.put("outTime","");
				}catch(Exception e){
					empObjData.put("inTime","");
					empObjData.put("outTime","");
				}
				empObjData.put("taskName","");
				empObjData.put("isOvertime",""+false);
				empObjData.put("isLeave",""+false);
				empObjData.put("isPresent",""+false);
				empObjData.put("isHalfDay",""+false);
				empObjData.put("totalWorkedHours","");
				empObjData.put("overtimeHours","");
				empObjData.put("empId",employeeId);
				empObjData.put("approverName","");
				empObjData.put("approverRemark","");
				empObjData.put("status","");
				empObjData.put("actionLabel","");
				empObjData.put("shift","");
				empObjData.put("workingHours","");
				empObjData.put("workedHours","");
				empObjData.put("projectName",projectName);
				empObjData.put("otHours","");
				empObjData.put("month","");
				empObjData.put("targetInTime","");
				empObjData.put("targetOutTime","");
				/* Name: Apeksha Gunjal 
				 * Date: 15/07/2018 
				 * Note: Send these parameter which are not in previous version
				 */ 
				empObjData.put("isEarlyMarkInTime","");
				empObjData.put("isEarlyMarkOutTime","");
				empObjData.put("isLateMarkInTime","");
				empObjData.put("isLateMarkOutTime","");
				//Added by apeksha on 11/08/2018
				try{
					empObjData.put("requestedHours","");
					empObjData.put("OTStatus","");
					empObjData.put("approvedHours","");
				}catch(Exception e){
					logger.log(Level.SEVERE,"error: "+e);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				empObjData.put("leaveHours","");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/* Name: Apeksha Gunjal 
			 * Date: 16/07/2018 @12:21
			 * Note: Send these parameter which are not in previous version
			 */
			try{
				empObjData.put("serverInTime","");
				empObjData.put("serverOutTime","");
			}catch(Exception e){
				logger.log(Level.SEVERE,"error: "+e);
			}
			employeeAttendanceArray.put(empObjData);
		}
	}
	JSONObject attendanceObject=new JSONObject();
	
	try {
		attendanceObject.put("projectName", projectName);
		attendanceObject.put("attendanceDate", spf.format(attendanceDate));
		attendanceObject.put("shift", shift);
		attendanceObject.put("employeeAttendance" , employeeAttendanceArray);
		attendanceObject.put("status" ,status);
		
		try{
//			attendanceObject.put("date", format.format(approvedDate));
			/**
			 * @author Vijay Date 11-11-2020
			 * Des :- as per nitin sir insturction sync date time should be at run time from server
			 * so updated the same.
			 */
			attendanceObject.put("date", format.format(new Date()));

		}catch(Exception e){
			e.printStackTrace();
			attendanceObject.put("date", "");
		}
		
		HrProject hrproject=ofy().load().type(HrProject.class).filter("companyId", company.getCompanyId()).filter("projectName", projectName).first().now();
		if(hrproject!=null && !hrproject.getSynchedBy().equals("")) {
			attendanceObject.put("employee", hrproject.getSynchedBy());
			logger.log(Level.SEVERE,"hrproject synched by"+hrproject.getSynchedBy());
		}
		else {
			attendanceObject.put("employee", approver);
			logger.log(Level.SEVERE,"employee = "+approver);

		}
		
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
//	String jsonString=attendanceObject.toString().replaceAll("\\\\", "");
		return attendanceObject;
	}
}
