package com.slicktechnologies.server.android.hr;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.itextpdf.text.pdf.codec.Base64;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.android.EmployeeFingerDB;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
/**
 * Description : It is used to save employee fingerprint data into server when user click on "Register" button 
  				 from register fingerprint.
 * @author pc1
 *
 */
public class UpdateEmployeeFinger extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3859620754083796277L;

	/**
	 * 
	 */

	/**
	 * 
	 */
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doGet(req, resp);
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doPost(req, resp);
		String urlCalled = req.getRequestURL().toString().trim();
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
		
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		spf.setTimeZone(TimeZone.getTimeZone("IST"));
		
		Company company = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
		if(company==null){
			company=ofy().load().type(Company.class).filter("accessUrl","127").first().now();
		}
		
		System.out.println("Hello World");
		String data=req.getParameter("data");
		JSONObject obj = null;
		try {
			obj = new JSONObject(data);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String actionTask=obj.optString("actionTask");
		String responseData="";
		if("SAVE".trim().equalsIgnoreCase(actionTask)){
			responseData=saveEmployeeFingerData(obj,company);
		}else if("FETCH".trim().equalsIgnoreCase(actionTask)){
			responseData=fetchEmployeeFingerData(obj,company);
		}else{
			responseData="No such action task present!";
		}
		resp.getWriter().print(responseData);
	}

	private String fetchEmployeeFingerData(JSONObject obj, Company company) {
		// TODO Auto-generated method stub
		String respData="";
		int empId=Integer.parseInt(obj.optString("empId").trim());
		EmployeeFingerDB fingerDB=ofy().load().type(EmployeeFingerDB.class).filter("companyId",company.getCompanyId()).filter("empId", empId).first().now();
		if(fingerDB!=null){
//			String encodeRawData=Base64.encodeBytes(fingerDB.getRawdata());
//			String encodedISOTemplate=Base64.encodeBytes(fingerDB.getIsoTemplate());
			JSONObject dataObj=new JSONObject();
			try {
				dataObj.put("quality", fingerDB.getQuality());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				dataObj.put("nfiq", fingerDB.getNfiq());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				dataObj.put("rawdataURL", fingerDB.getRawdataUrl());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				dataObj.put("isoTemplateURL",fingerDB.getIsoTemplateUrl());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				dataObj.put("inWidth", fingerDB.getInWidth());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				dataObj.put("inHeight",fingerDB.getInHeight());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				dataObj.put("inArea", fingerDB.getInArea());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				dataObj.put("resolution",fingerDB.getResolution());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				dataObj.put("grayScale", fingerDB.getGrayScale());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				dataObj.put("bpp", fingerDB.getBpp());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				dataObj.put("wsqCompressRatio",fingerDB.getWsqCompressRatio());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				dataObj.put("wsqInfo",fingerDB.getWsqInfo());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				dataObj.put("empId",fingerDB.getEmpId()+"");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			respData=dataObj.toString().replaceAll("\\\\", "");
		}else{
			respData="No data found for emp id "+empId;
		}
		return respData;
	}

	private String saveEmployeeFingerData(JSONObject obj, Company company) {
		// TODO Auto-generated method stub
		String respData="";
		int empId=Integer.parseInt(obj.optString("empId").trim());
		EmployeeFingerDB fingerDB=ofy().load().type(EmployeeFingerDB.class).filter("empId", empId).first().now();
		if(fingerDB!=null){
//			byte[] rawDataArray=Base64.decode(obj.optString("encodedRawData"));
//			byte[] isoTemplate=Base64.decode(obj.optString("encodedISOTemplate"));
//			fingerDB.setRawdataU(rawDataArray);
//			fingerDB.setIsoTemplate(isoTemplate);
			ofy().save().entity(fingerDB);
			respData="Employee Finger Data updated successfully!";
		}else{
			fingerDB=new EmployeeFingerDB();
//			byte[] rawDataArray=Base64.decode(obj.optString("encodedRawData"));
//			byte[] isoTemplate=Base64.decode(obj.optString("encodedISOTemplate"));
//			
			fingerDB.setEmpId(Integer.parseInt(obj.optString("empId")));
			fingerDB.setCompanyId(company.getCompanyId());
			fingerDB.setQuality(obj.optString("quality"));         
			fingerDB.setNfiq(obj.optString("nfiq"));    
//			fingerDB.setRawdata(rawDataArray);        
//			fingerDB.setIsoTemplate(isoTemplate);     
			fingerDB.setInWidth(obj.optString("inWidth"));          
			fingerDB.setInHeight(obj.optString("inHeight"));         
			fingerDB.setInArea(obj.optString("inArea"));           
			fingerDB.setResolution(obj.optString("resolution"));       
			fingerDB.setGrayScale(obj.optString("grayScale"));        
			fingerDB.setBpp(obj.optString("bpp"));              
			fingerDB.setWsqCompressRatio(obj.optString("wsqCompressRatio")); 
			fingerDB.setWsqInfo(obj.optString("wsqInfo"));
			ofy().save().entity(fingerDB);
			
			respData="Employee Finger Data created successfully!";
		}
		return respData;
	}

}
