package com.slicktechnologies.server.android.hr;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.attendance.EmployeeAttendance;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.personlayer.Employee;

/**
 * Description : It is used to load data to download excel sheet from android app. The API accepts from date ,
 *  to date , project name , employee name , shift and returns attendance data json.
 * @author pc1
 *
 */
public class FetchAttendanceDownloadData extends HttpServlet {

	private static final long serialVersionUID = 2754983897114137831L;

	Logger logger = Logger.getLogger("FetchAttendanceDownloadData.class");
	
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		spf.setTimeZone(TimeZone.getTimeZone("IST"));
		RegisterServiceImpl impl=new RegisterServiceImpl();
		impl.getClass();
		String urlCalled = req.getRequestURL().toString().trim();
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
		
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		Company company = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
		if(company==null){
			company=ofy().load().type(Company.class).filter("accessUrl","127").first().now();
		}
		
		
//		JSONArray jsonArray=null;
//		try {
//			jsonArray=new JSONArray(employeeData.trim());
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		JSONArray arrayOfAttendance=new JSONArray();
		
		String attendanceDate1="";
		String attendanceDate2="";
		String projectName="";
		String shift = "";
		String jsonString  = "";
		//for (int i = 0; i < jsonArray.length(); i++) {
//			JSONObject jObj = null;
//			try {
//				jObj = jsonArray.getJSONObject(i);
//			} catch (JSONException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			projectName=req.getParameter("projectName");
			attendanceDate1=req.getParameter("fromDate");
			attendanceDate2=req.getParameter("toDate");
			shift=req.getParameter("shift");
			String employeeName = req.getParameter("employeeName");
			try {
				JSONObject json = getAttendanceString(projectName ,spf.parse(attendanceDate1) , shift,company,spf.parse(attendanceDate2),employeeName);
				jsonString=json.toString().replaceAll("\\\\", "");
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		//}
		logger.log(Level.SEVERE,jsonString);
		resp.getWriter().println(jsonString);
	}
	public JSONObject getAttendanceString(String projectName ,Date attendanceDate1 , String shift,
			Company company , Date attendanceDate2 , String employeeName){
		String approver = "";
		Date approvedDate = null;
		
		SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		spf.setTimeZone(TimeZone.getTimeZone("IST"));
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		format.setTimeZone(TimeZone.getTimeZone("IST"));

	List<Attendance> attendanceList=new ArrayList<Attendance>();
	logger.log(Level.SEVERE,"spf.parse(attendanceDate1)"+attendanceDate1);
	logger.log(Level.SEVERE,"spf.parse(attendanceDate2)"+attendanceDate2);
	logger.log(Level.SEVERE,"company.getCompanyId()"+company.getCompanyId());
	logger.log(Level.SEVERE,"projectName"+projectName);
	logger.log(Level.SEVERE,"employeeName"+employeeName);
	logger.log(Level.SEVERE,"shift"+shift);
	
	String queryFilter = "";
	if(projectName != null && !projectName.equalsIgnoreCase("") 
			&& (employeeName == null || employeeName.equalsIgnoreCase("") 
			|| employeeName.equalsIgnoreCase("null"))){
		try {
			attendanceList=ofy().load().type(Attendance.class)
					.filter("companyId",company.getCompanyId())
					.filter("attendanceDate >=",attendanceDate1)
					.filter("attendanceDate <=",attendanceDate2)
					.filter("projectName",projectName.trim())
					.list();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"attendanceList 1"+attendanceList.size());
	}else{
	
	try {
		attendanceList=ofy().load().type(Attendance.class)
				.filter("companyId",company.getCompanyId())
				.filter("attendanceDate >=",attendanceDate1)
				.filter("attendanceDate <=",attendanceDate2)
				.filter("projectName",projectName.trim())
				.filter("employeeName", employeeName).list();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	logger.log(Level.SEVERE,"attendanceList 2"+attendanceList.size());
	}
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	sdf.setTimeZone(TimeZone.getTimeZone("IST"));
	JSONArray employeeAttendanceArray=new JSONArray();
	String status = Attendance.ATTENDANCECREATED;
		boolean employeeMatched=false;
		for (Attendance attendace : attendanceList) {
		//	if(employeeId==attendace.getEmpId()){
			//	logger.log(Level.SEVERE,"Attendance is present of : "+employeeId+ " of project: "+projectName);
				
				employeeMatched=true;
				JSONObject empObjData=new JSONObject();
				
				try {
					empObjData.put("count",attendace.getCount());
					empObjData.put("createdBy",attendace.getCreatedBy());
					empObjData.put("userId",attendace.getUserId());
					empObjData.put("status",attendace.getStatus());
					empObjData.put("employee",attendace.getEmployee());
					empObjData.put("branch",attendace.getBranch());
					empObjData.put("creationTime",attendace.getCreationTime());
					empObjData.put("approvalTime",attendace.getApprovalTime());
					empObjData.put("creationDate",attendace.getCreationDate());
					empObjData.put("approvalDate",attendace.getApprovalDate());
					empObjData.put("approverName",attendace.getApproverName());
					empObjData.put("group",attendace.getGroup());
					empObjData.put("category",attendace.getCategory());
					empObjData.put("type",attendace.getType());
					empObjData.put("remark",attendace.getRemark());
					empObjData.put("attendanceDate",spf.format(attendace.getAttendanceDate()));
					empObjData.put("projectNameForOT",attendace.getProjectNameForOT());
					empObjData.put("leaveType",attendace.getLeaveType());
					empObjData.put("overtimeType",attendace.getOvertimeType());
					try{
						empObjData.put("updatedEmployeeRole",attendace.getUpdatedEmployeeRole());
						empObjData.put("updatedEmployeeName",attendace.getUpdatedEmployeeName());
					}catch(Exception e){
						logger.log(Level.SEVERE , "employee role and name");
						empObjData.put("updatedEmployeeRole","");
						empObjData.put("updatedEmployeeName","");
					}
					
					
					try{
						empObjData.put("inTime",sdf.format(attendace.getInTime()));
					}catch(Exception e){
						empObjData.put("inTime","");
					}
					try{
						if(attendace.getOutTime()!=null)
							empObjData.put("outTime",sdf.format(attendace.getOutTime()));
						else
							empObjData.put("outTime", "");
					}catch(Exception e){
						empObjData.put("outTime", "");
					}
					empObjData.put("taskName",attendace.getTaskName());
					empObjData.put("isOvertime",attendace.isOvertime());
					empObjData.put("isLeave",attendace.isLeave());
					empObjData.put("isPresent",attendace.isPresent());
					empObjData.put("isHalfDay",attendace.isHalfDay());
					empObjData.put("totalWorkedHours",attendace.getTotalWorkedHours());
					empObjData.put("overtimeHours",attendace.getOvertimeHours());
					empObjData.put("empId",attendace.getEmpId());
					empObjData.put("employeeName",attendace.getEmployeeName());
					empObjData.put("approverName",attendace.getApproverName());
					empObjData.put("approverRemark",attendace.getApproverRemark());
					empObjData.put("status",attendace.getStatus());
					empObjData.put("actionLabel",attendace.getActionLabel());
					empObjData.put("shift",attendace.getShift());
					empObjData.put("workingHours",attendace.getWorkingHours());
					empObjData.put("workedHours",attendace.getWorkedHours());
					empObjData.put("projectName",attendace.getProjectName());
					empObjData.put("otHours",attendace.getOvertimeHours());
					empObjData.put("month",attendace.getMonth());
					empObjData.put("isEarlyMarkInTime",attendace.isEarlyMarkInTime());
					empObjData.put("isEarlyMarkOutTime",attendace.isEarlyMarkOutTime());
					empObjData.put("isLateMarkInTime",attendace.isLateMarkInTime());
					empObjData.put("isLateMarkOutTime",attendace.isLateMarkOutTime());
					
					SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));
					sdfTime.setTimeZone(TimeZone.getTimeZone("IST"));
					if(attendace.getTargetInTime()!=null)
						empObjData.put("targetInTime",sdfTime.format(attendace.getTargetInTime()));
					else
						empObjData.put("targetOutTime","");
					if(attendace.getTargetInTime()!=null)
						empObjData.put("targetOutTime",sdfTime.format(attendace.getTargetOutTime()));
					else
						empObjData.put("targetOutTime","");
					empObjData.put("empDesignation",
							attendace.getEmpDesignation());
					try{
						empObjData.put("requestedHours",attendace.getRequestedOTHours());
						empObjData.put("OTStatus",attendace.getOtStatus());
						empObjData.put("approvedHours",attendace.getApprovedOTHOURS());
					}catch(Exception e){
						logger.log(Level.SEVERE,"error: "+e);
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				try {
					empObjData.put("leaveHours",attendace.getLeaveHours());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try{
					try {
						empObjData.put("serverInTime",attendace.getServerInTime());
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						empObjData.put("serverInTime","");
					}
					try {
						empObjData.put("serverOutTime",attendace.getServerOutTime());
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						empObjData.put("serverOutTime","");
					}
					/**
					 * Rahul Verma added this on 05 Oct 2018
					 * 
					 */
					try {
						empObjData.put("markInAddress",attendace.getMarkInAddress());
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						empObjData.put("markInAddress","");
					}
					try {
						empObjData.put("markOutAddress",attendace.getMarkOutAddress());
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						empObjData.put("markOutAddress","");
					}
					/**
					 * Ends
					 */
					status = attendace.getStatus();
						
					
				}catch(Exception e){
					logger.log(Level.SEVERE,"error: "+e);
				}
				if(status.equals(Contract.APPROVED)){
					approver = attendace.getApproverName();
					approvedDate = attendace.getApprovalDate();
				}else{
					if(approvedDate == null){
						approvedDate = attendace.getCreationDate();
						approver     = attendace.getCreatedBy();
					}else{
						if(attendace.getCreationDate().after(approvedDate)){
							approvedDate = attendace.getCreationDate();
							approver     = attendace.getCreatedBy();
						}
					}
				}
				logger.log(Level.SEVERE , "date and approver :"+ approvedDate +" "+approver);
				if(shift != null && !shift.equalsIgnoreCase("") && shift.equalsIgnoreCase(attendace.getShift())){
					employeeAttendanceArray.put(empObjData);
				}else{
					employeeAttendanceArray.put(empObjData);
				}
			//}
		}
		
	//}
	JSONObject attendanceObject=new JSONObject();
	
	try {
//		attendanceObject.put("projectName", projectName);
//		attendanceObject.put("attendanceDate", spf.format(attendanceDate));
//		attendanceObject.put("shift", shift);
		attendanceObject.put("employeeAttendance" , employeeAttendanceArray);
//		attendanceObject.put("status" ,status);
//		try{
//			attendanceObject.put("date", format.format(approvedDate));
//		}catch(Exception e){
//			e.printStackTrace();
//			attendanceObject.put("date", "");
//		}
//		attendanceObject.put("employee", approver);
		
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
//	String jsonString=attendanceObject.toString().replaceAll("\\\\", "");
		return attendanceObject;
	}

	

}
