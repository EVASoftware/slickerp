package com.slicktechnologies.server.android.hr;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.server.utility.ServerAppUtility;

/**
 * Description : It is used to fetch individual employee details when user click 
 * on employee photo from attendance app.
 * @author pc1
 *
 */
public class FetchIndividualEmployeeDetails extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1832212705552833586L;
	Logger logger = Logger.getLogger("FetchIndividualEmployeeDetails.class");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doGet(req, resp);
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doPost(req, resp);
		RegisterServiceImpl impl=new RegisterServiceImpl();
		impl.getClass();
		
		String urlCalled = req.getRequestURL().toString().trim();
		//	String url = req.getParameter("url").trim();
//		String[] urlArray = url.split("\\.");
	String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		
		

		Company company = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		if (company == null) {
			company = ofy().load().type(Company.class)
					.filter("accessUrl", "127").first().now();
		}
		
		String employeeId_Name=req.getParameter("empID_Name");
		int empId=0;
		Exception empIdExp=null;
		try{
			empId=Integer.parseInt(employeeId_Name.trim());
		}catch(Exception e){
			e.printStackTrace();
			empIdExp=e;
		}
		Employee emp=null;
		EmployeeInfo info = null;
		if(empIdExp==null){
			emp=ofy().load().type(Employee.class).filter("companyId", company.getCompanyId()).filter("count", empId).first().now();
			info = ofy().load().type(EmployeeInfo.class).filter("empCount", empId).filter("companyId", company.getCompanyId()).first().now();
		}else{
			emp=ofy().load().type(Employee.class).filter("companyId", company.getCompanyId()).filter("fullname", employeeId_Name.trim()).first().now();
			info = ofy().load().type(EmployeeInfo.class).filter("fullname", employeeId_Name.trim()).filter("companyId", company.getCompanyId()).first().now();
		}
		if(emp!=null){
			
			if(info != null){
				if(!info.isLeaveAllocated()){
					resp.getWriter().print("No Leave found for "+employeeId_Name);
					return;
				}
			}
			SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			spf.setTimeZone(TimeZone.getTimeZone("IST"));
			
			JSONObject jsonObject=new JSONObject();
			try {
				jsonObject.put("employeeName", emp.getFullname());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				jsonObject.put("employeeFullName", emp.getFullName());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				jsonObject.put("employeeProfilePhoto", emp.getPhoto().getUrl());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    String hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
			try {
				jsonObject.put("employeeProfilePhoto_url", hostUrl+emp.getPhoto().getUrl());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				jsonObject.put("employeeId", emp.getCount());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				jsonObject.put("DOJ", spf.format(emp.getJoinedAt()));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				jsonObject.put("DOB", spf.format(emp.getDob()));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				jsonObject.put("address", emp.getAddress().getCompleteAddress());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String empData=jsonObject.toString();
			empData=empData.toString().replaceAll("\\\\", "");
			resp.getWriter().print(empData);
		}else{
			resp.getWriter().print("No Employee found for "+employeeId_Name);
		}
		
		
	}
}
