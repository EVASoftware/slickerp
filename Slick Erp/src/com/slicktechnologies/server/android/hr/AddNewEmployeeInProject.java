package com.slicktechnologies.server.android.hr;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.personlayer.Employee;

/**  Description :
 * It is used to add employee in cnc and hr project through app.
 * @author pc1
 */
public class AddNewEmployeeInProject extends HttpServlet {

	private static final long serialVersionUID = -4064715856305752869L;
	Logger logger = Logger.getLogger("add new employee.class");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//super.doGet(req, resp);
		doPost(req , resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//super.doPost(req, resp);
		SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		spf.setTimeZone(TimeZone.getTimeZone("IST"));
		RegisterServiceImpl impl=new RegisterServiceImpl();
		impl.getClass();
		String urlCalled = req.getRequestURL().toString().trim();
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		Company company = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
		if(company==null){
			company=ofy().load().type(Company.class).filter("accessUrl","127").first().now();
		}
	
		JSONArray arrayOfAttendance=new JSONArray();
		
		String attendanceDate="";
		String projectName="";
		String shift = "";
		String jsonString  = "";
			projectName=req.getParameter("projectName");
			attendanceDate=req.getParameter("attendanceDate");
			shift=req.getParameter("shift");
			String employeeName = req.getParameter("employeeName");
			String userRole = req.getParameter("employeeRole");
			String empId=req.getParameter("employeeId");
			logger.log(Level.SEVERE,"EmployeeId: "+empId);
			
			Employee employee=ofy().load().type(Employee.class)
					.filter("companyId",company.getCompanyId())
					.filter("count",Integer.parseInt(empId.trim())).first().now();
			
			logger.log(Level.SEVERE,"Employee id :" + empId +"  "+ employee);
			
			ProjectAllocation projectAllocation = ofy().load().type(ProjectAllocation.class)
					.filter("companyId", company.getCompanyId()).
					filter("employeeProjectAllocationList.employeeId", empId).first().now();
			
			if(projectAllocation != null){
				ArrayList<EmployeeProjectAllocation> list = new ArrayList<EmployeeProjectAllocation>();
					
					/**
					 * @author Vijay @Since 21-09-2022
					 * Loading employee project allocation from different entity
					 */
					   CommonServiceImpl commonservice = new CommonServiceImpl();
					   List<EmployeeProjectAllocation> empprojectallocationlist = commonservice.getEmployeeProjectAllocationlist(projectAllocation);
				    /**
				     * ends here
				     */
				   
//					for(EmployeeProjectAllocation all : projectAllocation.getEmployeeProjectAllocationList()){
					for(EmployeeProjectAllocation all : empprojectallocationlist){

						if(all.getEmployeeId().equalsIgnoreCase(empId.trim())){
							
						}else{
							all.setProjectAllocationId(projectAllocation.getCount());
							list.add(all);
						}
					}
					projectAllocation.setEmployeeProjectAllocationList(list);
					
			
				
				/**
				 * @author Vijay @Since 19-09-2022
				 * if Employee Project Allocation list greater than 600 then storing it into seperate intity 
				 */
				if(list.size()>AppConstants.employeeProjectAllocationListNumber || projectAllocation.isSeperateEntityFlag()){
					logger.log(Level.SEVERE,"for updating seperate entity");
					if(list.size()>0){
						projectAllocation.getEmployeeProjectAllocationList().clear();
						commonservice.updateEmployeeProjectAllocationList(list, "Save");
					}
					
				}
				/**
				 * ends here
				 */
				
				ofy().save().entity(projectAllocation);
			}
			
			HrProject hrProject = ofy().load().type(HrProject.class)
					.filter("companyId", company.getCompanyId()).
					filter("otList.empId", Integer.parseInt(empId.trim())).first().now();
			
			if(hrProject != null){
				ArrayList<Overtime> list = new ArrayList<Overtime>();
				
				/**
				 * @author Vijay Date :- 08-09-2022
				 * Des :- used common method to read OT from project or new entity HrProjectOvertime`
				 */
				ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
				CommonServiceImpl commonservice = new CommonServiceImpl();
				overtimelist = commonservice.getHRProjectOvertimelist(hrProject);
				/**
				 * ends here
				 */
//				for(Overtime all : hrProject.getOtList()){
				for(Overtime all : overtimelist){
					if(all.getEmpId() == Integer.parseInt(empId.trim())){
						
				}else{
					list.add(all);
				}
				
			}
				hrProject.setOtList(list);
				ofy().save().entity(hrProject);
			}
			ProjectAllocation project = ofy().load().type(ProjectAllocation.class)
					.filter("companyId", company.getCompanyId()).
					filter("projectName", projectName).first().now();
			

			/**
			 * @author Vijay @Since 21-09-2022
			 * Loading employee project allocation from different entity
			 */
			   CommonServiceImpl commonservice = new CommonServiceImpl();
			   ArrayList<EmployeeProjectAllocation> empprojectallocationlist = commonservice.getEmployeeProjectAllocationlist(project);
		    /**
		     * ends here
		     */
//			if(project != null && project.getEmployeeProjectAllocationList() != null){

			if(project != null && empprojectallocationlist != null && empprojectallocationlist.size()>0){
				ArrayList<EmployeeProjectAllocation> list1 = empprojectallocationlist;
				EmployeeProjectAllocation allocation = new EmployeeProjectAllocation();
				allocation.setEmployeeId(empId.trim());
				allocation.setEmployeeName(employeeName);
				allocation.setShift(shift);
				allocation.setProjectCount(project.getCount());
				if(employee != null){
					allocation.setEmployeeRole(employee.getRoleName());
				}
				list1.add(allocation);
				project.setEmployeeProjectAllocationList(list1);
				
				/**
				 * @author Vijay @Since 19-09-2022
				 * if Employee Project Allocation list greater than 600 then storing it into seperate intity 
				 */
				if(project.getEmployeeProjectAllocationList().size()>AppConstants.employeeProjectAllocationListNumber || project.isSeperateEntityFlag()){
					logger.log(Level.SEVERE,"for updating seperate entity");
					project.getEmployeeProjectAllocationList().clear();
						commonservice.updateEmployeeProjectAllocationList(project.getEmployeeProjectAllocationList(), "Save");
					
				}
				/**
				 * ends here
				 */
				
				ofy().save().entity(project);
			}
			
			LoadAttendanceFromEmployee loadAttendanceFromEmployee = new LoadAttendanceFromEmployee();
			try {
				JSONObject json = loadAttendanceFromEmployee.getAttendanceString(projectName ,spf.parse(attendanceDate) , shift,company,userRole,employeeName);
				jsonString=json.toString().replaceAll("\\\\", "");
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		//}
		logger.log(Level.SEVERE,jsonString);
		resp.getWriter().println(jsonString);
	
	}
	
}
