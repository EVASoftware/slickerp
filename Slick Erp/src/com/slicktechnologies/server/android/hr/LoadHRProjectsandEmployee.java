package com.slicktechnologies.server.android.hr;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.personlayer.Employee;

/**
 * Created by Rahul Verma
 * Descritpion : It loads Hr projects or cnc projects( based on configuration ) assigned to that logged in person if he is 
     Supervisor or manager. If person is Admin then it will load all projects and employees. If person is of role Sales then 
     only his projectand name will be loaded.
 * @author Admin
 */
public class LoadHRProjectsandEmployee extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3114199562548570407L;
	Logger logger = Logger.getLogger("LoadHRProjectsandEmployee.class");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String urlCalled = req.getRequestURL().toString().trim();
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		Company company = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		if (company == null) {
			company = ofy().load().type(Company.class)
					.filter("accessUrl", "127").first().now();
		}
		String userName = req.getParameter("userName");
		String employeeName = req.getParameter("employeeName");
		/**
		 * Date : 20 Sep 2018 Dev : Rahul Verma Description : Employee Role
		 * added to check whether it is admin or not
		 */
		String userRole = req.getParameter("employeeRole");
		String jsonString = "";
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance", "LoadHrProjectInApp", company.getCompanyId())){
			jsonString = getHrProject(userName, employeeName, userRole, company);
			logger.log(Level.SEVERE, jsonString);
			resp.getWriter().println(jsonString);
		}else{
		List<ProjectAllocation> projectList = new ArrayList<ProjectAllocation>();
		List<ProjectAllocation> projectListFinal = new ArrayList<ProjectAllocation>();
		List<ProjectAllocation> projectListOfManager;
		List<ProjectAllocation> projectListOfSuperVisor;
		List<ProjectAllocation> projectListOfSales;
		if (!userRole.trim().equalsIgnoreCase("Admin")) {
			if(userRole.trim().equalsIgnoreCase("Sales")){
				projectListOfSales = ofy().load()
						.type(ProjectAllocation.class)
						.filter("companyId", company.getId())
						.filter("employeeProjectAllocationList.employeeName", employeeName.trim()).filter("projectStatus", true).list();
				if (projectListOfSales != null) {
					projectList.addAll(projectListOfSales);
				}
			}else{
			projectListOfManager = ofy().load().type(ProjectAllocation.class)
					.filter("companyId", company.getId())
					.filter("managerName", employeeName.trim()).filter("projectStatus", true).list();
			if (projectListOfManager != null) {
				projectList.addAll(projectListOfManager);
			}
			projectListOfSuperVisor = ofy().load()
					.type(ProjectAllocation.class)
					.filter("companyId", company.getId())
					.filter("supervisorName", employeeName.trim()).filter("projectStatus", true).list();
			if (projectListOfSuperVisor != null) {
				projectList.addAll(projectListOfSuperVisor);
			}
		}
			
			HashSet<Integer> uniqueProjectIdSet = new HashSet<Integer>();
			for (ProjectAllocation projectAllocation : projectList) {
				uniqueProjectIdSet.add(projectAllocation.getCount());
			}

			for (ProjectAllocation projectAllocation : projectList) {
				for (int integerValue : uniqueProjectIdSet) {
					if (integerValue == projectAllocation.getCount()) {
						projectListFinal.add(projectAllocation);
					}
				}
			} 
//			projectListFinal=ofy().load().type(ProjectAllocation.class)
//					.filter("companyId", company.getId()).list();
		} else if(userRole.trim().equalsIgnoreCase("Admin")) {
			projectListFinal=ofy().load().type(ProjectAllocation.class)
					.filter("companyId", company.getId()).filter("projectStatus", true).list();
		}
		boolean flag = false;
		 if(projectListFinal == null || projectListFinal.size() == 0){
			projectListOfSales = ofy().load()
					.type(ProjectAllocation.class)
					.filter("companyId", company.getId())
					.filter("employeeProjectAllocationList.employeeName", employeeName.trim()).filter("projectStatus", true).list();
				if (projectListOfSales != null) {
					flag = true;
					projectListFinal.addAll(projectListOfSales);
				}
		}
		List<Shift> shiftList = ofy().load().type(Shift.class)
				.filter("companyId", company.getCompanyId()).list();

		SimpleDateFormat spf = new SimpleDateFormat("HH:mm");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		spf.setTimeZone(TimeZone.getTimeZone("IST"));

		JSONArray projectArray = new JSONArray();
		Map<String,Shift> shiftMap = new HashMap<String,Shift>();
		for (ProjectAllocation projectAllocation : projectListFinal) {
			shiftMap = new HashMap<String,Shift>();
			
			/**
			 * @author Vijay @Since 21-09-2022
			 * Loading employee project allocation from different entity
			 */
			   CommonServiceImpl commonservice = new CommonServiceImpl();
			   List<EmployeeProjectAllocation> empprojectallocationlist = commonservice.getEmployeeProjectAllocationlist(projectAllocation);
		    /**
		     * ends here
		     */
			   
//	label :		for (EmployeeProjectAllocation employees : projectAllocation
//					.getEmployeeProjectAllocationList()) {
	   label :	for (EmployeeProjectAllocation employees : empprojectallocationlist ) {
				if(userRole.trim().equalsIgnoreCase("Sales") || flag){
					if(!employeeName.trim().equalsIgnoreCase(employees.getEmployeeName())){
						continue;
					}
				}
				
				try{
				JSONObject projectObj = new JSONObject();
				projectObj.put("projectId", projectAllocation.getCount());
				projectObj.put("projectName",
						projectAllocation.getProjectName());
				projectObj.put("branch", projectAllocation.getBranch());
				/*
				 * Name: Apeksha Gunjal Date: 10/08/2018 @15:00 Friday Note: Add
				 * managerName and supervisorName
				 */
				try {
					logger.log(Level.SEVERE,
							"Manager: " + projectAllocation.getManagerName());
					projectObj.put("managerName",
							projectAllocation.getManagerName());
				} catch (Exception e) {
					logger.log(Level.SEVERE, "Error: " + e);
					projectObj.put("managerName", "");
				}
				try {
					logger.log(Level.SEVERE, "supervisorName: "
							+ projectAllocation.getSupervisorName());
					projectObj.put("supervisorName",
							projectAllocation.getSupervisorName());
				} catch (Exception e) {
					logger.log(Level.SEVERE, "Error: " + e);
					projectObj.put("supervisorName", "");
				}
							projectObj.put("shift", employees.getShift());
				String targetINTime = "", targetOUTTime = "";
				
				for (Shift shift : shiftList) {
					if (shift.getShiftName().trim()
							.equalsIgnoreCase(employees.getShift().trim())) {
						if(shiftMap.containsKey(employees.getShift().trim())){
							continue label;
						}else{
							shiftMap.put(employees.getShift().trim(), shift);
						}
						/** date 11.1.2019 added by komal to get values  before and after precision **/
						DecimalFormat df = new DecimalFormat("##");
						double precisionValue1 = 0 , precisionValue2 = 0;
						String array[];
						if((shift.getFromTime()+"").contains(".")){
							array = (shift.getFromTime()+"").split("\\.");
							if(array.length ==2){
								try{
								precisionValue1 = Double.parseDouble(array[1]);
								}catch(Exception e){
//									precisionValue1 = shift.getFromTime() - Math.floor(shift.getFromTime());
								}
							}
						}
						logger.log(Level.SEVERE , 
								"shift in time :"+ precisionValue1);
						if(precisionValue1 != 0){
							targetINTime = shift.getFromTime().intValue() + ":" + df.format(precisionValue1*10);
						}else{
							targetINTime = shift.getFromTime().intValue() + ":" + "00";
						}
						logger.log(Level.SEVERE , 
								"shift in time :"+ targetINTime);
						if((shift.getToTime()+"").contains(".")){
							array = (shift.getToTime()+"").split("\\.");
							if(array.length ==2){
								try{
								precisionValue2 = Double.parseDouble(array[1]);
								}catch(Exception e){
//									precisionValue2 = shift.getToTime() - Math.floor(shift.getToTime());
								}
							}
						}
						logger.log(Level.SEVERE , 
								"shift out time :"+ precisionValue2);
					//	targetOUTTime = shift.getToTime().intValue() + ":" +precisionValue2+"";
						if(precisionValue2 != 0){
							targetOUTTime = shift.getToTime().intValue() + ":" + df.format(precisionValue2*10);
						}else{
							targetOUTTime = shift.getToTime().intValue() + ":" + "00";
						}
						logger.log(Level.SEVERE , 
								"shift out time :"+ targetOUTTime);					
						
						/**date 11.1.2019 commented by komal **/		
//						targetINTime = shift.getFromTime().intValue() + ":00";
//						targetOUTTime = shift.getToTime().intValue() + ":00";
						break;
					}
				}
				projectObj.put("targetINTime", targetINTime);
				projectObj.put("targetOUTTime", targetOUTTime);
				/*
				 * Name: Apeksha Gunjal Date: 24/08/2018 @16:51 Note: Added
				 * boolean variable to check is finger print allowed or not for
				 * specific project
				 */
				try {
					projectObj.put("isFingerprintAllowed",
							projectAllocation.isFingerprintAllowed());
				} catch (Exception e) {
					logger.log(Level.SEVERE,
							"Error in adding isFingerprintAllowed: " + e);
					projectObj.put("isFingerprintAllowed", "" + true);
				}

				/**
				 * date 14.1.2019 added by komal
				 * description : to set sync , submit frequency ,lat - long of project , allowed radius within
				 *               which one is allowed to mark attendance
				 **/
				projectObj.put("syncFrequency", projectAllocation.getSyncFrequency());
				projectObj.put("submitFrequency", projectAllocation.getSubmitFrequency());
				projectObj.put("radiusAllowed", projectAllocation.getAllowedRadius());
				projectObj.put("locationLatitude", projectAllocation.getLocationLatitude());
				projectObj.put("locationLongitude", projectAllocation.getLocationLongitude());
				
				projectArray.add(projectObj);
				}catch(Exception e){
					e.printStackTrace();
					logger.log(Level.SEVERE,
							"Error in whole not added "+employeeName +"  "+ e);
				}
			}
			
		}
		LoadAttendanceFromEmployee load = new LoadAttendanceFromEmployee();
		String project = ""  ,shift = "";
		
		if(projectArray != null && projectArray.size() > 0){
			JSONObject obj  = (JSONObject) projectArray.get(0);
			shift = (String) obj.get("shift");
			project = (String) obj.get("projectName");
		}
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		format.setTimeZone(TimeZone.getTimeZone("IST"));
		String projectString = projectArray.toJSONString().replaceAll("\\\\", "");
		org.json.JSONObject attendanceString = null;
		try {
			attendanceString = load.getAttendanceString(project, format.parse(format.format(new Date())), shift, company, userRole, employeeName);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject object = new JSONObject();
		object.put("attendance", attendanceString);
		object.put("project", projectArray);
		object.put("projectName", project);
		object.put("shift", shift);
		
		jsonString = object.toJSONString().replaceAll("\\\\", "");
		//jsonString = projectArray.toJSONString().replaceAll("\\\\", "");
		}
		logger.log(Level.SEVERE, jsonString);
		resp.getWriter().println(jsonString);
	}
	
	private String getHrProject(String userName , String employeeName , String userRole , Company company){
		List<HrProject> projectList = new ArrayList<HrProject>();
		List<HrProject> projectListFinal = new ArrayList<HrProject>();
		List<HrProject> projectListOfManager;
		List<HrProject> projectListOfSuperVisor;
		List<HrProject> projectListOfSales;
		if (!userRole.trim().equalsIgnoreCase("Admin")) {
			if(userRole.trim().equalsIgnoreCase("Sales")){
				projectListOfSales = ofy().load()
						.type(HrProject.class)
						.filter("companyId", company.getId())
						.filter("otList.empName", employeeName.trim()).filter("status", true).list();
				if (projectListOfSales != null) {
					projectList.addAll(projectListOfSales);
				}
			}else{
			
			
			projectListOfManager = ofy().load().type(HrProject.class)
					.filter("companyId", company.getId())
					.filter("manager", employeeName.trim()).filter("status", true).list();
			if (projectListOfManager != null) {
				projectList.addAll(projectListOfManager);
			}
			projectListOfSuperVisor = ofy().load()
					.type(HrProject.class)
					.filter("companyId", company.getId())
					.filter("supervisor", employeeName.trim()).filter("status", true).list();
			if (projectListOfSuperVisor != null) {
				projectList.addAll(projectListOfSuperVisor);
			}
			}
			HashSet<Integer> uniqueProjectIdSet = new HashSet<Integer>();
			for (HrProject projectAllocation : projectList) {
				uniqueProjectIdSet.add(projectAllocation.getCount());
			}

			for (HrProject projectAllocation : projectList) {
				for (int integerValue : uniqueProjectIdSet) {
					if (integerValue == projectAllocation.getCount()) {
						projectListFinal.add(projectAllocation);
					}
				}
			}
		} else if(userRole.trim().equalsIgnoreCase("Admin")){
			projectListFinal=ofy().load().type(HrProject.class)
					.filter("companyId", company.getId()).filter("status", true).list();
		}
		boolean flag = false;
		 if(projectListFinal == null || projectListFinal.size() == 0){
			 projectListOfSales = ofy().load()
						.type(HrProject.class)
						.filter("companyId", company.getId())
						.filter("otList.empName", employeeName.trim()).filter("status", true).list();
				if (projectListOfSales != null) {
					projectListFinal.addAll(projectListOfSales);
				}
		}
		List<Shift> shiftList = ofy().load().type(Shift.class)
				.filter("companyId", company.getCompanyId()).list();

		SimpleDateFormat spf = new SimpleDateFormat("HH:mm");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		spf.setTimeZone(TimeZone.getTimeZone("IST"));

		JSONArray projectArray = new JSONArray();
		ArrayList<Overtime> employeeList;
		Map<String,Shift> shiftMap = new HashMap<String,Shift>();
//		for(Shift sh : shiftList){
//			shiftMap.put(sh.getShiftName().trim(), sh);
//		}
		for (HrProject projectAllocation : projectListFinal) {
			employeeList = getUniqueEmp(projectAllocation);
			shiftMap = new HashMap<String,Shift>();
			
	 label:		for (Overtime employees : employeeList) {
				if(userRole.trim().equalsIgnoreCase("Sales") || flag){
					if(!employeeName.trim().equalsIgnoreCase(employees.getEmpName())){
						continue;
					}
				}
				try{
					JSONObject projectObj = new JSONObject();
					projectObj.put("projectId", projectAllocation.getCount());
					projectObj.put("projectName",projectAllocation.getProjectName());
					projectObj.put("branch", projectAllocation.getBranch());
					try {
						logger.log(Level.SEVERE,
								"Manager: " + projectAllocation.getManager());
						projectObj.put("managerName",
								projectAllocation.getManager());
					} catch (Exception e) {
						logger.log(Level.SEVERE, "Error: " + e);
						projectObj.put("managerName", "");
					}
					try {
						logger.log(Level.SEVERE, "supervisorName: "
								+ projectAllocation.getSupervisor());
						projectObj.put("supervisorName",
								projectAllocation.getSupervisor());
					} catch (Exception e) {
						logger.log(Level.SEVERE, "Error: " + e);
						projectObj.put("supervisorName", "");
					}
	
				projectObj.put("shift", employees.getShift());
				
				String targetINTime = "", targetOUTTime = "";

				for (Shift shift : shiftList) {
					if (shift.getShiftName().trim()
							.equalsIgnoreCase(employees.getShift().trim())) {
						if(shiftMap.containsKey(employees.getShift().trim())){
							continue label;
						}else{
							shiftMap.put(employees.getShift().trim(), shift);
						}
						/** date 11.1.2019 added by komal to get values  before and after precision **/
						DecimalFormat df = new DecimalFormat("##");
						double precisionValue1 = 0 , precisionValue2 = 0;
						String array[];
						if((shift.getFromTime()+"").contains(".")){
							array = (shift.getFromTime()+"").split("\\.");
							if(array.length ==2){
								try{
								precisionValue1 = Double.parseDouble(array[1]);
								}catch(Exception e){
//									precisionValue1 = shift.getFromTime() - Math.floor(shift.getFromTime());
								}
							}
						}
						logger.log(Level.SEVERE , 
								"shift in time :"+ precisionValue1);
						if(precisionValue1 != 0){
							targetINTime = shift.getFromTime().intValue() + ":" + df.format(precisionValue1*10);
						}else{
							targetINTime = shift.getFromTime().intValue() + ":" + "00";
						}
						logger.log(Level.SEVERE , 
								"shift in time :"+ targetINTime);
						if((shift.getToTime()+"").contains(".")){
							array = (shift.getToTime()+"").split("\\.");
							if(array.length ==2){
								try{
								precisionValue2 = Double.parseDouble(array[1]);
								}catch(Exception e){
//									precisionValue2 = shift.getToTime() - Math.floor(shift.getToTime());
								}
							}
						}
						logger.log(Level.SEVERE , 
								"shift out time :"+ precisionValue2);
					//	targetOUTTime = shift.getToTime().intValue() + ":" +precisionValue2+"";
						if(precisionValue2 != 0){
							targetOUTTime = shift.getToTime().intValue() + ":" + df.format(precisionValue2*10);
						}else{
							targetOUTTime = shift.getToTime().intValue() + ":" + "00";
						}
						logger.log(Level.SEVERE , 
								"shift out time :"+ targetOUTTime);					
						
						/**date 11.1.2019 commented by komal **/		
//						targetINTime = shift.getFromTime().intValue() + ":00";
//						targetOUTTime = shift.getToTime().intValue() + ":00";
						break;
					}
				}
				projectObj.put("targetINTime", targetINTime);
				projectObj.put("targetOUTTime", targetOUTTime);
				/*
				 * Name: Apeksha Gunjal Date: 24/08/2018 @16:51 Note: Added
				 * boolean variable to check is finger print allowed or not for
				 * specific project
				 */
				try {
					projectObj.put("isFingerprintAllowed",
							projectAllocation.isFingerprintAllowed());
				} catch (Exception e) {
					logger.log(Level.SEVERE,
							"Error in adding isFingerprintAllowed: " + e);
					projectObj.put("isFingerprintAllowed", "" + true);
				}
				/**
				 * date 14.1.2019 added by komal
				 * description : to set sync , submit frequency ,lat - long of project , allowed radius within
				 *               which one is allowed to mark attendance
				 **/
				projectObj.put("syncFrequency", projectAllocation.getSyncFrequency());
				projectObj.put("submitFrequency", projectAllocation.getSubmitFrequency());
				projectObj.put("radiusAllowed", projectAllocation.getAllowedRadius());
				projectObj.put("locationLatitude", projectAllocation.getLocationLatitude());
				projectObj.put("locationLongitude", projectAllocation.getLocationLongitude());
				
				projectArray.add(projectObj);
				}catch(Exception e){
					e.printStackTrace();
					logger.log(Level.SEVERE,
							"Error in whole not added "+employeeName +"  "+ e);
				}
			}
		}
		
		LoadAttendanceFromEmployee load = new LoadAttendanceFromEmployee();
		String project = ""  ,shift = "";
		
		if(projectArray != null && projectArray.size() > 0){
			JSONObject obj  = (JSONObject) projectArray.get(0);
			shift = (String) obj.get("shift");
			project = (String) obj.get("projectName");
		}
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		format.setTimeZone(TimeZone.getTimeZone("IST"));
		String projectString = projectArray.toJSONString().replaceAll("\\\\", "");
		org.json.JSONObject attendanceString = null;
		try {
			attendanceString = load.getAttendanceString(project, format.parse(format.format(new Date())), shift, company, userRole, employeeName);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject object = new JSONObject();
		object.put("attendance", attendanceString);
		object.put("project", projectArray);
		object.put("projectName", project);
		object.put("shift", shift);
		
		String jsonString = object.toJSONString().replaceAll("\\\\", "");
		return jsonString;
	}
	private ArrayList<Overtime> getUniqueEmp(HrProject hrProject){
		ArrayList<Overtime> empList = new ArrayList<Overtime>();		
		Map<Integer , Overtime> empUniqueMap = new HashMap<Integer , Overtime>();
		
		/**
		 * @author Vijay Date :- 08-09-2022
		 * Des :- used common method to read OT from project or new entity HrProjectOvertime`
		 */
		ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
		CommonServiceImpl commonservice = new CommonServiceImpl();
		overtimelist = commonservice.getHRProjectOvertimelist(hrProject);
		/**
		 * ends here
		 */
//		for(Overtime ot : project.getOtList()){
		for(Overtime ot : overtimelist){
			if(empUniqueMap.containsKey(ot.getEmpId())){				
			}else{
				empUniqueMap.put(ot.getEmpId(), ot);
			}
		}
		for (Map.Entry<Integer, Overtime> entry : empUniqueMap.entrySet()) {
		    Overtime value = entry.getValue();
		    empList.add(value);
		}
		return empList;	
	}
}
