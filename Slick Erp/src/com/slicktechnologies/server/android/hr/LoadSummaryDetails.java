package com.slicktechnologies.server.android.hr;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;

/**
 * Description : It loads summary of attendance i.e. total present count , absence count ,
 	holiday count , weeklyOff count , late count project wise.
 * @author pc1
 *
 */
public class LoadSummaryDetails extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4699974378793766107L;
	Logger logger = Logger.getLogger("LoadSummaryDetails.class");
	JSONObject objWhole;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String urlCalled = req.getRequestURL().toString().trim();
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		Company company = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		if (company == null) {
			company = ofy().load().type(Company.class)
					.filter("accessUrl", "127").first().now();
		}
		// String userName=req.getParameter("userName");
		String typeOfAction = req.getParameter("action");
		String attendanceDate = "", projectName = "", employeeName = "", userRole = "";
		ArrayList<Integer> employeeIdList = new ArrayList<Integer>();
		if (typeOfAction.trim().equalsIgnoreCase("LOADATTENDANCE")) {
			attendanceDate = req.getParameter("attendanceDate");
			String employeeListStr = req.getParameter("employeeList");
			JSONArray employeeListArray = null;
			try {
				employeeListArray = new JSONArray(employeeListStr.trim());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (employeeListArray != null) {
				for (int j = 0; j < employeeListArray.length(); j++) {
					JSONObject empObj = employeeListArray.optJSONObject(j);
					String employeeId = empObj.optString("employeeId");
					employeeIdList.add(Integer.parseInt(employeeId));
				}
			}
			projectName = req.getParameter("projectName");
		} else if (typeOfAction.trim().equalsIgnoreCase("LOADPROJECT")) {
			employeeName = req.getParameter("employeeName");
			/**
			 * Rahul Verma added this for role, just to check employeeRole
			 */
			userRole = req.getParameter("employeeRole");
		}

		List<ProjectAllocation> projectList = new ArrayList<ProjectAllocation>();
		List<ProjectAllocation> projectListFinal = new ArrayList<ProjectAllocation>();
		List<ProjectAllocation> projectListOfManager;
		List<ProjectAllocation> projectListOfSuperVisor;

		JSONArray dataArray = new JSONArray();

		if (typeOfAction.trim().equalsIgnoreCase("LOADPROJECT")) {
			/**
			 * Rahul Verma added for admin login as well Date : 20 Sep 2018
			 */
			if (!userRole.trim().equalsIgnoreCase("Admin")) {
				projectListOfManager = ofy().load()
						.type(ProjectAllocation.class)
						.filter("companyId", company.getId())
						.filter("managerName", employeeName.trim()).list();
				if (projectListOfManager != null) {
					projectList.addAll(projectListOfManager);
				}
				projectListOfSuperVisor = ofy().load()
						.type(ProjectAllocation.class)
						.filter("companyId", company.getId())
						.filter("supervisorName", employeeName.trim()).list();
				if (projectListOfSuperVisor != null) {
					projectList.addAll(projectListOfSuperVisor);
				}
				HashSet<Integer> uniqueProjectIdSet = new HashSet<Integer>();
				for (ProjectAllocation projectAllocation : projectList) {
					uniqueProjectIdSet.add(projectAllocation.getCount());
				}

				for (ProjectAllocation projectAllocation : projectList) {
					for (int integerValue : uniqueProjectIdSet) {
						if (integerValue == projectAllocation.getCount()) {
							projectListFinal.add(projectAllocation);
						}
					}
				}
			} else {

				projectListFinal = ofy().load().type(ProjectAllocation.class)
						.filter("companyId", company.getId()).list();
			}
			for (ProjectAllocation projectAllocation : projectListFinal) {
				JSONObject obj = new JSONObject();
				try {
					obj.put("projectName", projectAllocation.getProjectName());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					
					/**
					 * @author Vijay @Since 21-09-2022
					 * Loading employee project allocation from different entity
					 */
					   CommonServiceImpl commonservice = new CommonServiceImpl();
					   List<EmployeeProjectAllocation> empprojectallocationlist = commonservice.getEmployeeProjectAllocationlist(projectAllocation);
				    /**
				     * ends here
				     */
					   obj.put("totalEmployee", empprojectallocationlist.size());
//					obj.put("totalEmployee", projectAllocation
//							.getEmployeeProjectAllocationList().size());
				} catch (JSONException e) {
					e.printStackTrace();
				}
				dataArray.put(obj);
			}

		} else if (typeOfAction.trim().equalsIgnoreCase("LOADATTENDANCE")) {
//			SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy");
//			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
//			spf.setTimeZone(TimeZone.getTimeZone("IST"));
//
//			List<Attendance> attendanceList = new ArrayList<Attendance>();
//			List<String> shiftList = new ArrayList<>();
//			try {
//				attendanceList = ofy()
//						.load()
//						.type(Attendance.class)
//						.filter("companyId", company.getCompanyId())
//						.filter("attendanceDate",
//								spf.parse(attendanceDate.trim()))
//						.filter("projectName", projectName.trim()).list();// .filter("empId IN",
//																			// employeeIdList).list();//.filter("isPresent",
//																			// true).list();
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				logger.log(Level.SEVERE, "Error:attendanc: " + e);
//			}
//			logger.log(Level.SEVERE, "attendanceList" + attendanceList.size());
//			// List<Attendace> attendanceLeaveList=new ArrayList<Attendace>();
//			// try {
//			// attendanceLeaveList=ofy().load().type(Attendace.class).filter("companyId",company.getCompanyId()).filter("attendanceDate",spf.parse(attendanceDate.trim())).filter("projectName",projectName.trim()).filter("empId IN",
//			// employeeIdList).list();//.filter("isLeave", true).list();
//			// } catch (Exception e) {
//			// // TODO Auto-generated catch block
//			// e.printStackTrace();
//			// }
//			/*
//			 * Name: Apeksha Gunjal Date: 08/08/2018 @ 14:12 Note: Add shift
//			 * wise present absent count
//			 */
//			try {
//				for (Attendance attendance : attendanceList) {
//					logger.log(Level.SEVERE, "shift: " + attendance.getShift()
//							+ " " + attendance.getEmployeeName());
//					if (!shiftList.contains(attendance.getShift().trim()))
//						shiftList.add(attendance.getShift().trim());
//				}
//				logger.log(Level.SEVERE, "shift: " + shiftList.size());
//			} catch (Exception e) {
//				logger.log(Level.SEVERE, "Error:shift: " + e);
//			}
//			// logger.log(Level.SEVERE,"employeeIdList"+attendanceLeaveList.size());
//
//			for (String shift : shiftList) {
//				int presentCount = 0, leaveCount = 0, total = 0;
//				for (Attendance attendace : attendanceList) {
//					if (shift.trim().equalsIgnoreCase(
//							attendace.getShift().trim())) {
//						total += 1;
//						if (attendace.isPresent()) {
//							presentCount += 1;
//						} else if (attendace.isLeave()) {
//							leaveCount += 1;
//						}
//					}
//				}
//				logger.log(Level.SEVERE, "shift: " + shift + " total:" + total
//						+ " P: " + presentCount + " A: " + leaveCount);
//				JSONObject jObj = new JSONObject();
//				try {
//					jObj.put("totalPresent", presentCount);
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				try {
//					jObj.put("totalAbsent", leaveCount);
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				try {
//					jObj.put("projectName", projectName);
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				try {
//					jObj.put("shift", shift);
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				try {
//					jObj.put("total", total);
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				logger.log(Level.SEVERE, "jObj:shift: " + jObj);
//				dataArray.put(jObj);
			
			SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			spf.setTimeZone(TimeZone.getTimeZone("IST"));
			
			List<Shift> shiftListData=new ArrayList<Shift>();
			shiftListData=ofy().load().type(Shift.class).filter("companyId", company.getCompanyId()).filter("status", true).list();
			
			List<Attendance> attendanceList = new ArrayList<Attendance>();
			List<String> shiftList = new ArrayList<>();
			try {
				attendanceList = ofy()
						.load()
						.type(Attendance.class)
						.filter("companyId", company.getCompanyId())
						.filter("attendanceDate",
								spf.parse(attendanceDate.trim()))
						.filter("projectName", projectName.trim()).list();// .filter("empId IN",
																			// employeeIdList).list();//.filter("isPresent",
																			// true).list();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE, "Error:attendanc: " + e);
			}
			logger.log(Level.SEVERE, "attendanceList" + attendanceList.size());
			// List<Attendace> attendanceLeaveList=new ArrayList<Attendace>();
			// try {
			// attendanceLeaveList=ofy().load().type(Attendace.class).filter("companyId",company.getCompanyId()).filter("attendanceDate",spf.parse(attendanceDate.trim())).filter("projectName",projectName.trim()).filter("empId IN",
			// employeeIdList).list();//.filter("isLeave", true).list();
			// } catch (Exception e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			/*
			 * Name: Apeksha Gunjal Date: 08/08/2018 @ 14:12 Note: Add shift
			 * wise present absent count
			 */
			try {
				for (Attendance attendance : attendanceList) {
					logger.log(Level.SEVERE, "shift: " + attendance.getShift()
							+ " " + attendance.getEmployeeName());
					if (!shiftList.contains(attendance.getShift().trim()))
						shiftList.add(attendance.getShift().trim());
				}
				logger.log(Level.SEVERE, "shift: " + shiftList.size());
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Error:shift: " + e);
			}
			// logger.log(Level.SEVERE,"employeeIdList"+attendanceLeaveList.size());

			
			for (String shift : shiftList) {
				int lateValue=0,halfDayValue=0,earlyValue=0,otValue=0,compOffValue=0,holidayValue=0,weeklyoff=0,absentInnerValue=0;
				int presentCount = 0, leaveCount = 0, total = 0;
				for (Attendance attendace : attendanceList) {
					if (shift.trim().equalsIgnoreCase(
							attendace.getShift().trim())) {
						total += 1;
						if (attendace.getInTime()!=null) {//attendace.isPresent()|| 
							if(attendace.getActionLabel().trim().equalsIgnoreCase(AppConstants.HF)){
								halfDayValue+=1;
							}else if(attendace.getInTime()!=null){
								if(attendace.isLateMarkInTime()){
								   lateValue+=1;
								}
								if(attendace.isEarlyMarkOutTime()){
									earlyValue+=1;
								}
							}
						//	if(attendace.getOvertimeHours()!=0){
							if(attendace.isOvertime()){
								otValue+=1;
							}
							presentCount += 1;
						}else if(attendace.getActionLabel().trim().equalsIgnoreCase(AppConstants.HF)){
							halfDayValue+=1;
							presentCount += 1;
						}
						
						
						if(attendace.getActionLabel().trim().equalsIgnoreCase(AppConstants.CO)){
							compOffValue+=1;
							leaveCount += 1;
						}else if(attendace.getActionLabel().trim().equalsIgnoreCase(AppConstants.H)){
							holidayValue+=1;
							leaveCount += 1;
						}else if(attendace.getActionLabel().trim().equalsIgnoreCase(AppConstants.WO)){
							weeklyoff+=1;
							leaveCount += 1;
						}else if(attendace.getActionLabel().trim().equalsIgnoreCase(AppConstants.A)){
							absentInnerValue+=1;
							leaveCount += 1;
						}if (attendace.isLeave() && !(attendace.getActionLabel().trim().equalsIgnoreCase(AppConstants.HF)|| attendace.getActionLabel().trim().equalsIgnoreCase(AppConstants.P))) {
							leaveCount += 1;
						}
					}
				}
				
				logger.log(Level.SEVERE, "shift: " + shift + " total:" + total
						+ " P: " + presentCount + " A: " + leaveCount);
				JSONObject jObj = new JSONObject();
				try {
					jObj.put("totalPresent", presentCount);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					jObj.put("totalAbsent", leaveCount);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					jObj.put("projectName", projectName);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					jObj.put("shift", shift);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					jObj.put("total", total);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.log(Level.SEVERE, "jObj:shift: " + jObj);
				
				try {
					jObj.put("lateValue", lateValue);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.log(Level.SEVERE, "jObj:shift: " + jObj);
				
				try {
					jObj.put("halfDayValue", halfDayValue);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.log(Level.SEVERE, "jObj:shift: " + jObj);
				
				try {
					jObj.put("earlyValue", earlyValue);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.log(Level.SEVERE, "jObj:shift: " + jObj);
				
				try {
					jObj.put("otValue", otValue);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.log(Level.SEVERE, "jObj:shift: " + jObj);
				
				try {
					jObj.put("compOffValue", compOffValue);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.log(Level.SEVERE, "jObj:shift: " + jObj);
				
				try {
					jObj.put("holidayValue", holidayValue);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.log(Level.SEVERE, "jObj:shift: " + jObj);
				
				try {
					jObj.put("weeklyOff", weeklyoff);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.log(Level.SEVERE, "jObj:shift: " + jObj);
				
				try {
					jObj.put("absentInnerValue", absentInnerValue);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.log(Level.SEVERE, "jObj:shift: " + jObj);
//				
//				try {
//					jObj.put("total", total);
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				logger.log(Level.SEVERE, "jObj:shift: " + jObj);
				
				dataArray.put(jObj);
			}
			JSONArray totalShiftArray=new JSONArray();
			for (int i = 0; i < shiftListData.size(); i++) {
				JSONObject shift=new JSONObject();
				try {
					shift.put("shiftName", shiftListData.get(i).getShiftName());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				totalShiftArray.put(shift);
			}

			objWhole=new JSONObject();
			try {
				objWhole.put("attendanceList", dataArray);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				objWhole.put("shiftList",totalShiftArray);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		String jsonString = "";
		if(typeOfAction.trim().equalsIgnoreCase("LOADPROJECT")){
			jsonString=dataArray.toString().replaceAll("\\\\", "");
		}else if(typeOfAction.trim().equalsIgnoreCase("LOADATTENDANCE")){
			jsonString=objWhole.toString().replaceAll("\\\\", "");
		}
		logger.log(Level.SEVERE, jsonString);
		resp.getWriter().println(jsonString);
	}
	
	
	private boolean isInTimeLate(Attendance attendace) {
        boolean result = false;
        SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy HH:mm");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
        try {
        	
            SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
            TimeZone.setDefault(TimeZone.getTimeZone("IST"));
            sdfTime.setTimeZone(TimeZone.getTimeZone("IST"));
            SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
            TimeZone.setDefault(TimeZone.getTimeZone("IST"));
            sdfDate.setTimeZone(TimeZone.getTimeZone("IST"));
            
            Date inTimeDate = sdf.parse(sdf.format(attendace.getInTime()));
            logger.log(Level.SEVERE,"In Time :: "+sdfDate.format(inTimeDate)+" "+sdfTime.format(attendace.getTargetInTime()));
            Date targetDate = sdf.parse(sdfDate.format(inTimeDate)+" "+sdfTime.format(attendace.getTargetInTime()));
            if (inTimeDate.after(targetDate)) {
                result = true;
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Exception in isInTimeLate method"+e);
        }
        return result;
    }

}
