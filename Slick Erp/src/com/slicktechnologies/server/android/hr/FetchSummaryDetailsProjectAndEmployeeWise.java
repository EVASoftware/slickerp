package com.slicktechnologies.server.android.hr;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

/**
 * Description :It is used to get attendance summary project and employee wise.
 * @author pc1
 *
 */
public class FetchSummaryDetailsProjectAndEmployeeWise extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6333960790959939046L;
	Logger logger = Logger
			.getLogger("FetchSummaryDetailsProjectAndEmployeeWise.class");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doGet(req, resp);
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doPost(req, resp);
		RegisterServiceImpl impl = new RegisterServiceImpl();
		impl.getClass();

		String urlCalled = req.getRequestURL().toString().trim();
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		
		Company company = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		if (company == null) {
			company = ofy().load().type(Company.class)
					.filter("accessUrl", "127").first().now();
		}

		String isProjectWiseStr = req.getParameter("isProjectWise");
		String fromDate = req.getParameter("fromDate");
		String toDate = req.getParameter("toDate");
		String empInfo = null, projectName = null;
		logger.log(Level.SEVERE,"isProjectWiseStr"+isProjectWiseStr);
		logger.log(Level.SEVERE,"fromDate"+fromDate);
		logger.log(Level.SEVERE,"toDate"+toDate);
		logger.log(Level.SEVERE,"empInfo"+empInfo);
		
		boolean isProjectWise = Boolean.parseBoolean(isProjectWiseStr.trim());
		if (isProjectWise)
			projectName = req.getParameter("projectName");
		else
			empInfo = req.getParameter("empInfo");

		Exception empExp = null;
		int empId = 0;
		try {
			empId = Integer.parseInt(empInfo.trim());
		} catch (Exception e) {
			empExp = e;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		Calendar calendar = Calendar.getInstance();
        try {
			calendar.setTime(sdf.parse(fromDate));
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        calendar.set(Calendar.SECOND, 00);
        calendar.set(Calendar.MINUTE, 00);
        calendar.set(Calendar.HOUR, 00);
        
        Calendar calendar1 = Calendar.getInstance();
        try {
			calendar1.setTime(sdf.parse(toDate));
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        calendar1.set(Calendar.SECOND, 59);
        calendar1.set(Calendar.MINUTE, 59);
        calendar1.set(Calendar.HOUR, 23);


		List<Attendance> attendanceList = null;
		if (isProjectWise) {

		//	try {
				attendanceList = ofy().load().type(Attendance.class)
						.filter("companyId", company.getCompanyId())
						.filter("attendanceDate >=", calendar.getTime())
						.filter("attendanceDate <", calendar1.getTime())
						.filter("projectName", projectName.trim()).list();
//			} catch (ParseException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		} else {
			if (empExp == null) {
				//try {
					attendanceList = ofy().load().type(Attendance.class)
							.filter("companyId", company.getCompanyId())
							.filter("attendanceDate >=", calendar.getTime())
							.filter("attendanceDate <", calendar1.getTime())
							.filter("empId", empId).list();
//				} catch (ParseException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}

			} else {
				//try {
					attendanceList = ofy().load().type(Attendance.class)
							.filter("companyId", company.getCompanyId())
							.filter("attendanceDate >=", calendar.getTime())
							.filter("attendanceDate <", calendar1.getTime())
							.filter("employeeName", empInfo.trim()).list();
//				} catch (ParseException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
			}
		}
		HashSet<Integer> empIdSet = new HashSet<Integer>();
		for (Attendance attendance : attendanceList) {
			empIdSet.add(attendance.getEmpId());
		}

		JSONArray employeeAttendanceArray = new JSONArray();
		for (Integer employeeId : empIdSet) {
			int presentCount = 0, leaveCount = 0, total = 0;
			int lateValue = 0, halfDayValue = 0, earlyValue = 0, otValue = 0, compOffValue = 0, holidayValue = 0, weeklyoff = 0, absentInnerValue = 0;
			String employeeName = "";
			for (Attendance attendance : attendanceList) {
				if (attendance.getEmpId() == employeeId) {

					total += 1;
					if (attendance.getInTime()!=null) {//attendace.isPresent()|| 
						if(attendance.getActionLabel().trim().equalsIgnoreCase(AppConstants.HF)){
							halfDayValue+=1;
						}else if(attendance.getInTime()!=null){
							if(attendance.isLateMarkInTime()){
							   lateValue+=1;
							}
							if(attendance.isEarlyMarkOutTime()){
								earlyValue+=1;
							}
						}
					//	if(attendace.getOvertimeHours()!=0){
						if(attendance.isOvertime()){
							otValue+=1;
						}
						presentCount += 1;
					}else if(attendance.getActionLabel().trim().equalsIgnoreCase(AppConstants.HF)){
						halfDayValue+=1;
						presentCount += 1;
					}
					
					
					if(attendance.getActionLabel().trim().equalsIgnoreCase(AppConstants.CO)){
						compOffValue+=1;
						leaveCount += 1;
					}else if(attendance.getActionLabel().trim().equalsIgnoreCase(AppConstants.H)){
						holidayValue+=1;
						leaveCount += 1;
					}else if(attendance.getActionLabel().trim().equalsIgnoreCase(AppConstants.WO)){
						weeklyoff+=1;
						leaveCount += 1;
					}else if(attendance.getActionLabel().trim().equalsIgnoreCase(AppConstants.A)){
						absentInnerValue+=1;
						leaveCount += 1;
					}else if (attendance.isLeave() && !(attendance.getActionLabel().trim().equalsIgnoreCase(AppConstants.HF) || attendance.getActionLabel().trim().equalsIgnoreCase(AppConstants.P))) {
						leaveCount += 1;
					}
				
//					total += 1;
//					if (attendance.isPresent() || attendance.getInTime() != null) {//attendace.isPresent()|| 
//						if(attendance.getActionLabel().trim().equalsIgnoreCase(AppConstants.HF)){
//							halfDayValue+=1;
//						}else if(attendance.getInTime()!=null){
//							if(attendance.isLateMarkInTime()){
//							   lateValue+=1;
//							}else if(attendance.isEarlyMarkOutTime()){
//								earlyValue+=1;
//							}
//						}
//					//	if(attendace.getOvertimeHours()!=0){
//						if(attendance.isOvertime()){
//							otValue+=1;
//						}
//						if (!attendance.getActionLabel().trim()
//								.equalsIgnoreCase(AppConstants.A)){
//							presentCount += 1;
//						}
////					}else if(attendance.getInTime() != null){
////						if (!attendance.getActionLabel().trim()
////								.equalsIgnoreCase(AppConstants.A)) {
////							presentCount += 1;
////						}
//					} else if (attendance.getActionLabel().trim()
//							.equalsIgnoreCase(AppConstants.CO)) {
//						leaveCount += 1;
//						compOffValue += 1;
//					} else if (attendance.getActionLabel().trim()
//							.equalsIgnoreCase(AppConstants.H)) {
//						leaveCount += 1;
//						holidayValue += 1;
//					} else if (attendance.getActionLabel().trim()
//							.equalsIgnoreCase(AppConstants.WO)) {
//						leaveCount += 1;
//						weeklyoff += 1;
//					} else if (attendance.getActionLabel().trim()
//							.equalsIgnoreCase(AppConstants.A)) {
//						leaveCount += 1;
//						absentInnerValue += 1;
//					} else if (attendance.isLeave()) {
//						leaveCount += 1;
//					}

					employeeName = attendance.getEmployeeName();
				}
			}
			JSONObject jObj = new JSONObject();
			try {
				jObj.put("totalPresent", presentCount+"");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				jObj.put("totalAbsent", leaveCount+"");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				jObj.put("employeeId", employeeId+"");
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				jObj.put("employeeName", employeeName+"");
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				jObj.put("total", total+"");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				jObj.put("lateValue", lateValue+"");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				jObj.put("halfDayValue", halfDayValue+"");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				jObj.put("earlyValue", earlyValue+"");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				jObj.put("otValue", otValue+"");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				jObj.put("compOffValue", compOffValue+"");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				jObj.put("holidayValue", holidayValue+"");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				jObj.put("weeklyOff", weeklyoff+"");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				jObj.put("absentInnerValue", absentInnerValue+"");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "jObj"+jObj);
			employeeAttendanceArray.put(jObj);
		}
		String data=employeeAttendanceArray.toString().trim().replaceAll("\\\\", "");
		logger.log(Level.SEVERE, data);
		resp.getWriter().print(data);
	}

	private boolean isInTimeLate(Attendance attendance) {
		boolean result = false;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		try {

			SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			sdfTime.setTimeZone(TimeZone.getTimeZone("IST"));
			SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			sdfDate.setTimeZone(TimeZone.getTimeZone("IST"));

			Date inTimeDate = sdf.parse(sdf.format(attendance.getInTime()));
			Date targetDate = sdfTime.parse(sdfDate.format(inTimeDate)+" "+sdfTime.format(attendance.getTargetInTime()));
			if (inTimeDate.after(targetDate)) {
				result = true;
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception in isInTimeLate method" + e);
		}
		return result;
	}

}
