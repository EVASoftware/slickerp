package com.slicktechnologies.server.android.hr;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.simple.JSONObject;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.googlecode.objectify.ObjectifyService;
import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.DataMigrationTaskQueueImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.personlayer.Employee;

/**
 * Description : This API is used to sync and submit the attendance.
 * 
 * @author pc1
 *
 */
public class MarkAttendanceOfEmployee extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8259531172388181261L;
	/**
	 * 
	 */
	Logger logger = Logger.getLogger("MarkAttendanceOfEmployee.class");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String urlCalled = req.getRequestURL().toString().trim();
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		spf.setTimeZone(TimeZone.getTimeZone("IST"));
		
		/***03-01-2020 Deepak Salve Added this code for clear data***/
		ObjectifyService.reset();
		ofy().consistency(Consistency.STRONG);
		ofy().clear();
		/***End***/
		
		
		Company company = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
		if(company==null){
			company=ofy().load().type(Company.class).filter("accessUrl","127").first().now();
		}
		String dataOfAttendance="";
		dataOfAttendance=req.getParameter("data");
		//By Apeksha Gunjal on 11/08/2018 13:00
		//Note: check is server call for requestOtHours
		if(!dataOfAttendance.trim().equalsIgnoreCase("Request OT".trim())){
			String status = "";
			/* Name: Apeksha Gunjal 
			 * Date: 15/07/2018 
			 * Note: Send status as Created for Supervisor/ other and Approved for Manager / Admin
			 */
			try{
				logger.log(Level.SEVERE,"status: "+req.getParameter("status"));
				status = req.getParameter("status");
			}catch(Exception e){
				e.printStackTrace();
			}
			
			JSONArray jsonArray = null;
			try {
				/* Name: Apeksha Gunjal 
				 * Date: 07/08/2018 
				 * Note: Encode and decode data because URL accept & as a new parameter
				 */
				logger.log(Level.SEVERE,"dataOfAttendance: "+dataOfAttendance);
				String params = dataOfAttendance.replaceAll("$AND", "&")
						.replaceAll("0null", "");
				jsonArray = new JSONArray(params.trim());
				logger.log(Level.SEVERE,"jsonArray: "+jsonArray);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log(Level.SEVERE,"jsonArray:Error "+e);
			}
			
				/***Date 27-1-2020 by amol Added ProjectName And ShiftName for Nayra***/
				String projectName="";
				String shiftName="";
				int empId=0;
				//take a loop of jso aray
				logger.log(Level.SEVERE,"shiftName Name "+shiftName);
			for (int i = 0; i < jsonArray.length(); i++) {
//				JSONObject object = null;
//				try {
//					object = jsonArray.getJSONObject(i);
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
				try {
					projectName = jsonArray.getJSONObject(i).optString(
							"projectName");
				} catch (JSONException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				try {
					empId = Integer.parseInt(jsonArray.getJSONObject(i)
							.optString("empId"));
				} catch (NumberFormatException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				} catch (JSONException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				try {
					shiftName = jsonArray.getJSONObject(i).optString("shift");
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				logger.log(Level.SEVERE, "project Name " + projectName);
				logger.log(Level.SEVERE, "shiftName Name " + shiftName);
				logger.log(Level.SEVERE, "Emp Id " + empId);
				
           /**Date 10-2-2020
            * @author Amol
            * Check If Project Name is "App Project" and Shift Name "App Shift" and According to this flow executes
            * This Changes has been Done for "Nayra" raised by Sonu.
            * 
            */
				if (projectName.equalsIgnoreCase("App Project")
						&& shiftName.equalsIgnoreCase("App Shift")) {
					Employee emp = ofy().load().type(Employee.class).filter("companyId", company.getCompanyId())
					.filter("count", empId).first().now();
                   
					if(emp==null){
						resp.getWriter().write("Employee is Null");
						return;
					}
					
					
					logger.log(Level.SEVERE, "Employee Name " + emp);

					HrProject project = ofy().load().type(HrProject.class)
							.filter("companyId", company.getCompanyId())
							.filter("projectName", emp.getProjectName()).first().now();

					
					if(project==null){
						resp.getWriter().write("Project is Null");
						return;
					}
					logger.log(Level.SEVERE, "Hrproject Name " + project);
					
					HashSet<String> shiftHs = new HashSet<String>();
					
					/**
					 * @author Vijay Date :- 08-09-2022
					 * Des :- used common method to read OT from project or new entity HrProjectOvertime`
					 */
					ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
					CommonServiceImpl commonservice = new CommonServiceImpl();
					overtimelist = commonservice.getHRProjectOvertimelist(project);
					logger.log(Level.SEVERE, "otlist sIZE " + overtimelist.size());

					/**
					 * ends here
					 */
//					for(Overtime ot : project.getOtList()){
					for(Overtime ot : overtimelist){
						if (emp.getCount() == ot.getEmpId()) {
							shiftHs.add(ot.getShift());
						}
					}
					if(shiftHs==null){
						resp.getWriter().write("Shift is Null");
						return;
					}
					

					logger.log(Level.SEVERE, "shiftHs Size " + shiftHs.size());
					if (shiftHs.size() > 1) {
						resp.getWriter().write("Multiple Shift are Assign To this Project");
					}
					try {
						jsonArray.getJSONObject(i).put("empId", emp.getCount());
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						jsonArray.getJSONObject(i).put("projectName",emp.getProjectName());
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
					for (String shifths : shiftHs) {
						try {
							jsonArray.getJSONObject(i).put("shift", shifths);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					/**Date 10-2-2020 by Amol added a target In Time and Target OutTime**/
//					ArrayList<String> shiftList=new ArrayList<String>(shiftHs);
					Shift shift=null;
					SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
					if (shiftHs.size() != 0) {
						logger.log(Level.SEVERE, "shift hashset size ");
						for (String shifths : shiftHs) {
							shift = ofy().load().type(Shift.class).filter("companyId", company.getCompanyId())
									.filter("shiftName", shifths).first().now();
						}
						logger.log(Level.SEVERE, "shift Name " + shift);
					}
					
//					logger.log(Level.SEVERE, "targetInTime"+shift.getFromTime());
					Date targetInTime = null;
					try {
						targetInTime = timeFormat.parse(String.valueOf(shift.getFromTime()));
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					Date targetOutTime = null;
					try {
						targetOutTime = timeFormat.parse(String.valueOf(shift.getToTime()));
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					try{
					jsonArray.getJSONObject(i).put("targetInTime",targetInTime);
					}catch(JSONException e){
						e.printStackTrace();
					}
					try{
					jsonArray.getJSONObject(i).put("targetOutTime",targetOutTime);
					}catch(JSONException e){
						e.printStackTrace();
					}
				}
				
				/**
				 * @author Vijay Date :- 09-07-2022
				 * Des :- when technician sign out updating travelled KM in last completed service
				 */
				try {
					String strlastCompletedServiceId = "";
					if(req.getParameterMap().containsKey("serviceId")) {
						strlastCompletedServiceId=jsonArray.getJSONObject(i).optString("serviceId");
					}
					double km=0;
					if(req.getParameterMap().containsKey("km")) {
						logger.log(Level.SEVERE,"Received KM");
						km = Double.parseDouble(req.getParameter("km"));
					}
					if(!strlastCompletedServiceId.equals("")) {
						int serviceId = Integer.parseInt(strlastCompletedServiceId);
						Service serviceEntity = ofy().load().type(Service.class).filter("companyId", company.getCompanyId())
												.filter("count", serviceId).first().now();
						if(serviceEntity!=null){
							serviceEntity.setKmTravelled(km);
							ofy().save().entity(serviceEntity);
							logger.log(Level.SEVERE, "Last Service Km updated successfully for service id - "+strlastCompletedServiceId);
						}					
					}
					
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
				/**
				 * ends here
				 */
			}
			
			
			
			
			DataMigrationTaskQueueImpl impl=new DataMigrationTaskQueueImpl();
			/* Name: Apeksha Gunjal 
			 * Date: 15/07/2018 
			 * Note: Send status as Created for Supervisor/ other and Approved for Manager / Admin
			 */
			String responseFromMethod = "";
//			try{
				responseFromMethod = impl.employeeAttendance(company.getCompanyId(), jsonArray, status);
				logger.log(Level.SEVERE,"responseFromMethod: "+responseFromMethod);
			/*}catch(Exception e){
				responseFromMethod = "Attendance upload failed.";
				logger.log(Level.SEVERE,"responseFromMethod: "+responseFromMethod);
			}*/
			resp.getWriter().write(responseFromMethod);
		}else {
			/*
			 * Name: Apeksha Gunjal
			 * Date: 11/08/2018 @ 2:15
			 * Note: Request for ot hours 
			 */
			String responseFromMethod = "";
//			try{
				DataMigrationTaskQueueImpl impl=new DataMigrationTaskQueueImpl();
				org.json.JSONObject object = new org.json.JSONObject();
				try{
				object.put("empId", req.getParameter("empId"));
				object.put("projectName", req.getParameter("projectName"));
				object.put("attendanceDate", req.getParameter("attendanceDate"));
				object.put("OTStatus", req.getParameter("OTStatus"));
				object.put("requestedHours", req.getParameter("requestedHours"));
				object.put("supervisorName", req.getParameter("supervisorName"));

				logger.log(Level.SEVERE,"RequestOTHours object: "+object);
				}catch(Exception e){
					logger.log(Level.SEVERE,"RequestOTHours error: "+e);
				}
				
				responseFromMethod = impl.saveRequestedOTDetails(company.getCompanyId(),
						object);
			
			/*}catch(Exception e){
				logger.log(Level.SEVERE,"RequestOTHours error: "+e);
				responseFromMethod = "Failed";
			}*/
			logger.log(Level.SEVERE,"RequestOTHours responseFromMethod: "+responseFromMethod);
			resp.getWriter().write(responseFromMethod);
		}
		
	}
	
}
