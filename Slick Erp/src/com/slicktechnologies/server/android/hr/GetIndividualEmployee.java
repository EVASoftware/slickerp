package com.slicktechnologies.server.android.hr;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.personlayer.Employee;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**Description : It is used to get employee finger print data.
 * 
 * @author pc1
 *
 */
public class GetIndividualEmployee extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -899566600552258417L;
	/**
	 * 
	 */
	//Added by Apeksha Gunjal on 13/08/2018 @17:51 , to add loggers.
	Logger logger = Logger.getLogger("GetIndivisualEmployee.class");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doGet(req, resp);
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doPost(req, resp);
		RegisterServiceImpl impl=new RegisterServiceImpl();
		impl.getClass();
		String urlCalled = req.getRequestURL().toString().trim();
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		spf.setTimeZone(TimeZone.getTimeZone("IST"));
		
		Company company = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
		if(company==null){
			company=ofy().load().type(Company.class).filter("accessUrl","127").first().now();
		}
		String empId=req.getParameter("employeeId");

		//Added by Apeksha Gunjal on 13/08/2018 @17:51 , just printing loggers.
		logger.log(Level.SEVERE,"EmployeeId: "+empId);
		
		Employee employee=ofy().load().type(Employee.class)
				.filter("companyId",company.getCompanyId())
				.filter("count",Integer.parseInt(empId.trim())).first().now();
		if(employee!=null){
			JSONObject empObj=new JSONObject();
			try {
				empObj.put("empId", employee.getCount());
			} catch (JSONException e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
				//Added by Apeksha Gunjal on 13/08/2018 @17:51 , just printing loggers.
				logger.log(Level.SEVERE,"Error: "+e3);
			}
			try {
				empObj.put("empName", employee.getFullname());
			} catch (JSONException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
				//Added by Apeksha Gunjal on 13/08/2018 @17:51 , just printing loggers.
				logger.log(Level.SEVERE,"Error: "+e2);
			}
			try {
				empObj.put("empFullName", employee.getFullName());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				//Added by Apeksha Gunjal on 13/08/2018 @17:51 , just printing loggers.
				logger.log(Level.SEVERE,"Error: "+e1);
			}
			try{
				if(employee.getPhoto()!=null){
					empObj.put("empProfileImage",employee.getPhoto().getUrl());
				}
			}catch(Exception e){
				e.printStackTrace();
				//Added by Apeksha Gunjal on 13/08/2018 @17:51 , just printing loggers.
				logger.log(Level.SEVERE,"Error: "+e);
			}
			try {
				empObj.put("branch", employee.getBranchName());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				//Added by Apeksha Gunjal on 13/08/2018 @17:51 , just printing loggers.
				logger.log(Level.SEVERE,"Error: "+e);
			}
			try {
				empObj.put("employeeStatus", employee.isStatus());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				//Added by Apeksha Gunjal on 13/08/2018 @17:51 , just printing loggers.
				logger.log(Level.SEVERE,"Error: "+e);
			}
			String employeeData=empObj.toString().replace("\\\\", "");
			//Added by Apeksha Gunjal on 13/08/2018 @17:51 , just printing loggers.
			logger.log(Level.SEVERE,"employeeData: "+employeeData);
			resp.getWriter().println(employeeData);
		}else{
			//Added by Apeksha Gunjal on 13/08/2018 @17:51 , just printing loggers.
			logger.log(Level.SEVERE,"No employee found...");
			resp.getWriter().println("No employee found!");
		}
	}
}
