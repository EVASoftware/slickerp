package com.slicktechnologies.server.android;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.slicktechnologies.client.Slick_Erp;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.device.TrackTableDetails;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.ServiceImplementor;
import com.slicktechnologies.server.ServiceListServiceImpl;
import com.slicktechnologies.server.android.technicianapp.TechnicianAppOperation;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.ModificationHistory;
import com.slicktechnologies.shared.common.android.RegisterDevice;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.helperlayer.CustomerUser;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
	/**Description :
	 * single service mark complete. Not using currently.
	 */
public class MarkCompletedSubmitServlet extends HttpServlet {


	private static final long serialVersionUID = 1811057931247745874L;

	/**
	 * 
	 */

	Logger logger = Logger.getLogger("MarkCompletedSubmitServlet.class");

	Company comp;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doGet(req, resp);
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		/**
		 * @author Anil @since 14-06-2021
		 * Thai character was not getting download in proper format
		 * raised by Rahul Tiwari
		 */
//		resp.setContentType("text/plain");
		resp.setContentType("text/plain; charset=UTF-8");
		resp.setCharacterEncoding("UTF-8");
		
		String serviceLatnLong="";
		String urlCalled = req.getRequestURL().toString().trim();
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		Exception exep = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("HH:mm");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			format.setTimeZone(TimeZone.getTimeZone("IST"));

			comp = ofy().load().type(Company.class)
					.filter("accessUrl", splitUrl[0]).first().now();
			logger.log(Level.SEVERE, "Stage One");
			String companyId = comp.getCompanyId() + "";
			logger.log(Level.SEVERE, "companyId::::::::::" + companyId);
			float feedbackRating = Float.parseFloat(req.getParameter(
					"feedbackRating").trim());
			logger.log(Level.SEVERE, "feedbackRating::::::::::"
					+ feedbackRating);
			String custRemark = req.getParameter("custRemark").trim();
			logger.log(Level.SEVERE, "custRemark::::::::::" + custRemark);
			String serviceId = req.getParameter("serviceId").trim();
			String lat = req.getParameter("lat").trim();
			String lon = req.getParameter("lon").trim();
			/**
			 * Updated By: komal
			 * Date: 01-04-2019
			 * Description: To get and save location
			 */
			String location = "";
			try{
				location = req.getParameter("location").trim();
			}catch(Exception e){
				location = "";
				e.printStackTrace();
			}
			
			String serviceWiseLatLong = req.getParameter("serviceWiseLatLong")
					.trim();
			logger.log(Level.SEVERE, "serviceId::::::::::" + serviceId);
			boolean syncFlag = Boolean.parseBoolean(req
					.getParameter("syncFlag"));
			boolean isRateContractService = Boolean.parseBoolean(req
					.getParameter("isRateContractService"));
			/*
			 * Name: Apeksha Gunjal
			 * Date: 02/07/2018 @ 15:00
			 * Note: Added technician remark which is enter after completing service
			 */
			String technicianRemark = "";
			try{
				technicianRemark = req.getParameter("technicianRemark").trim(); 
			}catch(Exception e){
				logger.log(Level.SEVERE, "technicianRemark error::::::::::" + e.getMessage());
			}
			/**
			 * Added By Rahul Verma Date: 25 Oct 2017
			 * 
			 */
			
			
			
			
			logger.log(Level.SEVERE , "Request :" + req.getParameterMap().size() );
//			for(Map.Entry<String, String[]> entry : req.getParameterMap().entrySet()){
//				logger.log(Level.SEVERE , "Request Keys :" + entry.getKey() );
//			}
			TechnicianAppOperation app = new TechnicianAppOperation();
			try {
				String trapDetailsdata=req.getParameter("saveTrapDetails").trim();
				JSONObject jsonData = new JSONObject(trapDetailsdata);
				String tranResponse = app.saveTrapDetails(jsonData, comp, req, resp,null);
				logger.log(Level.SEVERE , "Trap response :" + tranResponse);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try{
				String materialdata=req.getParameter("materialReturn").trim();
				JSONObject jsonMaterialData = new JSONObject(materialdata);
				String materialResponse = app.createMaterialReturnDoc(comp, jsonMaterialData, req, resp);
				logger.log(Level.SEVERE , "Material response :" + materialResponse);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
			String customerNameOfSignature, customerMobileOfSignature, markCompleteDate, markCompleteTime, reportedObject = null, rescheduleObject = null;

			customerNameOfSignature = req.getParameter("customerName").trim();
			customerMobileOfSignature = req.getParameter("customerMobile")
					.trim();
			if (syncFlag) {
				// customerNameOfSignature=req.getParameter("customerName").trim();
				// customerMobileOfSignature=req.getParameter("customerMobile").trim();
				markCompleteDate = req.getParameter("markCompleteDate").trim();
				markCompleteTime = req.getParameter("markCompleteTime").trim();
				reportedObject = req.getParameter("reportedData").trim();
				rescheduleObject = req.getParameter("rescheduledData").trim();
			}

			/**
			 * Rahul Verma added on 01 Oct 2018
			 * 
			 */
			try{
				serviceLatnLong = req.getParameter("serviceLatnLong").trim();
				logger.log(Level.SEVERE,"serviceLatnLong:::::"+serviceLatnLong);
			}catch(Exception e){
				e.printStackTrace();
			}

			Service service = ofy().load().type(Service.class)
					.filter("companyId", Long.parseLong(companyId))
					.filter("count", Long.parseLong(serviceId)).first().now();
			logger.log(Level.SEVERE, "Service::::::" + service);
			// Adding in table
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
			timeFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			String newDate = sdf.format(new Date());
			service.setCompleteLatnLong(lat + "$" + lon);
			service.setServiceWiseLatLong(serviceWiseLatLong);
			try{
			if(serviceLatnLong!=null)
				service.setLatnLong(serviceLatnLong);
			logger.log(Level.SEVERE, "Stage two");
			}catch(Exception e){
				e.printStackTrace();
			}
			try {
				TrackTableDetails trackTableDetails = new TrackTableDetails();
				trackTableDetails.setStatus(Service.SERVICESTATUSTECHCOMPLETED);
				logger.log(Level.SEVERE,
						"service.getTrackServiceTabledetails().size()"
								+ service.getTrackServiceTabledetails().size());
				// if(service.getTrackServiceTabledetails()!=null){
				trackTableDetails.setSrNo(service.getTrackServiceTabledetails()
						.size() + 1);
				// }else{
				// trackTableDetails.setSrNo(0+1);
				// }
				/**
				 * Updated By: Viraj
				 * Date: 01-03-2019
				 * Description: To show lat and long
				 */
				trackTableDetails.setLatitude(lat);
				trackTableDetails.setLongitude(lon);
				/** Ends **/
				/**
				 * Updated By: komal
				 * Date: 01-04-2019
				 * Description: To get and save location and time
				 */
				trackTableDetails.setLocationName(location);
				trackTableDetails.setTime(timeFormat.format(new Date()));
				/**
				 * end komal
				 */
				trackTableDetails.setDate_time(newDate);
				service.getTrackServiceTabledetails().add(trackTableDetails);
			} catch (Exception e) {
				e.printStackTrace();
				logger.log(Level.SEVERE, "Error::::::::in tracktable" + e);

			}
			logger.log(Level.SEVERE, "new Date::::::::::" + newDate);
			try {
				logger.log(Level.SEVERE, "After parse new Date:::::::::::"
						+ sdf.parse(newDate));
			} catch (ParseException e1) {
				e1.printStackTrace();
			}

			logger.log(Level.SEVERE, "Stage 3");
			try {
				service.setServiceCompletionDate(sdf.parse(newDate));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log(Level.SEVERE,
						"Error::::::::setServiceCompletionDate" + e);
			}

			logger.log(Level.SEVERE, "Stage 4");
			service.setServiceCompleteRemark(custRemark.trim());

			if (feedbackRating > 0f && feedbackRating <= 1f) {
				service.setCustomerFeedback("Poor");
			} else if (feedbackRating > 1f && feedbackRating <= 2f) {
				service.setCustomerFeedback("Average");
			} else if (feedbackRating > 2f && feedbackRating <= 3f) {
				service.setCustomerFeedback("Good");
			} else if (feedbackRating > 3f && feedbackRating <= 4f) {
				service.setCustomerFeedback("Very Good");
			} else if (feedbackRating > 4f && feedbackRating <= 5f) {
				service.setCustomerFeedback("Excellent");
			} else {
				service.setCustomerFeedback("");
			}
			/*
			 * Name: Apeksha Gunjal
			 * Date: 02/07/2018 @ 15:37
			 * Note: Added technician remark which is enter after completing service
			 */
			try{
				service.setTechnicianRemark(technicianRemark);
			}catch(Exception e){
				logger.log(Level.SEVERE, "error setting technician remark: "+e.getMessage());
			}

			logger.log(Level.SEVERE, "Stage 5");
			int reportedMin = 0;
			int completedMin = 0;
			/** date 23.03.2019 added by komal 
			 * purpose : to calculate service duration = completed - started **/
			int startedMin = 0;
			for (int i = 0; i < service.getTrackServiceTabledetails().size(); i++) {
				if (Service.SERVICESTATUSREPORTED.equalsIgnoreCase(service
						.getTrackServiceTabledetails().get(i).getStatus()
						.trim())) {
					String[] timeSplit = service.getTrackServiceTabledetails()
							.get(i).getDate_time().split(" ");
					String[] time = timeSplit[1].split(":");
					int reportedSec = (Integer.parseInt(time[0]) * 60 * 60)
							+ (Integer.parseInt(time[1]) * 60)
							+ (Integer.parseInt(time[2]));
					logger.log(Level.SEVERE, "reportedMin:::::::::::::"
							+ reportedMin);
					reportedMin = (reportedSec / 60);
					if(service.getAddress() != null){
						if(service.getAddress().getLatitude() == null || service.getAddress().getLatitude().equals("")){
							String lat1 = "";
							try{
								lat1 = service.getTrackServiceTabledetails().get(i).getLatitude();
							}catch(Exception e){
								
							}
							service.getAddress().setLatitude(lat1);
						}
						if(service.getAddress().getLongitude() == null || service.getAddress().getLongitude().equals("")){
							String long1 = "";
							try{
								long1 = service.getTrackServiceTabledetails().get(i).getLongitude();
							}catch(Exception e){
								
							}
							service.getAddress().setLongitude(long1);
						}
						}
				}

				if (Service.SERVICESTATUSTECHCOMPLETED.equalsIgnoreCase(service
						.getTrackServiceTabledetails().get(i).getStatus()
						.trim())) {
					String[] timeSplit = service.getTrackServiceTabledetails()
							.get(i).getDate_time().split(" ");
					String[] time = timeSplit[1].split(":");
					int completedSec = (Integer.parseInt(time[0]) * 60 * 60)
							+ (Integer.parseInt(time[1]) * 60)
							+ (Integer.parseInt(time[2]));
					completedMin = (completedSec / 60);
					logger.log(Level.SEVERE, "completedMin:::::::::::::"
							+ completedMin);
				}
				/** date 23.03.2019 added by komal 
				 * purpose : to calculate service duration = completed - started **/
				if (Service.SERVICESTATUSSTARTED.equalsIgnoreCase(service
						.getTrackServiceTabledetails().get(i).getStatus()
						.trim())) {
					String[] timeSplit = service.getTrackServiceTabledetails()
							.get(i).getDate_time().split(" ");
					String[] time = timeSplit[1].split(":");
					int startedSec = (Integer.parseInt(time[0]) * 60 * 60)
							+ (Integer.parseInt(time[1]) * 60)
							+ (Integer.parseInt(time[2]));
					logger.log(Level.SEVERE, "reportedMin:::::::::::::"
							+ reportedMin);
					startedMin = (startedSec / 60);
				}
			}
			service.setServiceCompleteDuration(completedMin - reportedMin);
			/** date 23.03.2019 added by komal 
			 * purpose : to calculate service duration = completed - started **/
			service.setTotalServiceDuration(completedMin - startedMin);
		//	service.setStatus(Service.SERVICESTATUSTECHCOMPLETED);
			/*** date 5.3.2019 added by komal for nbhc to set status as completed**/
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MarkServiceCompletedFromApp", Long.parseLong(companyId))){
				service.setStatus(Service.SERVICESTATUSCOMPLETED);
			}else{				
				service.setStatus(Service.SERVICESTATUSTECHCOMPLETED);
			}
			service.setToTime(format.format(new Date()));
			// List<ServiceProject> serviceProjectList = new
			// ArrayList<ServiceProject>();
			//
			// serviceProjectList = ofy().load().type(ServiceProject.class)
			// .filter("companyId", Long.parseLong(companyId))
			// .filter("serviceId", Long.parseLong(serviceId)).list();
			// logger.log(Level.SEVERE, serviceProjectList.size() + "");
			//
			// if(serviceProjectList.size()>0){
			// for (int i = 0; i < serviceProjectList.size(); i++) {
			// ServiceProject
			// serviceProject=ofy().load().type(ServiceProject.class).filter("companyId",
			// Long.parseLong(companyId)).filter("serviceId",
			// Long.parseLong(serviceId)).first().now();
			// serviceProject.setProjectStatus(ServiceProject.COMPLETED);
			// ofy().save().entity(serviceProject).now();
			// }
			// }
			logger.log(Level.SEVERE, "Stage 6");
			if (syncFlag) {
				// Updating Reported Time and Date
				TrackTableDetails tracktabledetails = new TrackTableDetails();
				logger.log(Level.SEVERE, "Stage::::::::2");

				SimpleDateFormat sdf1 = new SimpleDateFormat(
						"dd/MM/yyyy HH:mm:ss");
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				sdf1.setTimeZone(TimeZone.getTimeZone("IST"));

				JSONObject reportedObj = new JSONObject(reportedObject);

				tracktabledetails.setDate_time(sdf.format(reportedObj
						.opt("reportedDate").toString().trim()
						+ " "
						+ reportedObj.opt("reportedTime").toString().trim())
						+ "");

				logger.log(Level.SEVERE, "Stage::::::::3");
				tracktabledetails.setStatus(Service.SERVICESTATUSREPORTED);
				logger.log(Level.SEVERE, "Stage::::::::4");

				// rahul added this for null pointer exp in table
				// if(service.getTrackServiceTabledetails()!=null){
				tracktabledetails.setSrNo(service.getTrackServiceTabledetails()
						.size() + 1);
				// }
				// else
				// {
				// tracktabledetails.setSrNo(1);
				// }
				service.getTrackServiceTabledetails().add(tracktabledetails);
				logger.log(Level.SEVERE, "Stage::::::::5"
						+ service.getTrackServiceTabledetails().size());

				// Reschedule
				JSONObject rescheduleHistory = new JSONObject(rescheduleObject);
				ModificationHistory modHistory = new ModificationHistory();
				modHistory.setOldServiceDate(sdf.parse(rescheduleHistory
						.optString("previousServiceDate")));
				modHistory.setResheduleDate(sdf.parse(rescheduleHistory
						.optString("rescheduleDate")));

				// SimpleDateFormat sdf1=new
				// SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
				// sdf1.setTimeZone(TimeZone.getTimeZone("IST"));
				modHistory.setSystemDate(new Date());
				String[] time = rescheduleHistory.optString("rescheduleTime")
						.trim().split(":");
				int hours = Integer.parseInt(time[0]);
				int mins = Integer.parseInt(time[1].substring(0, 2));
				if (hours >= 12) {
					int hoursDecrease = 0;
					if (hours != 12) {
						hoursDecrease = 12;
					}
					if (mins < 10) {
						modHistory.setResheduleTime((hours - hoursDecrease)
								+ ":" + "0" + mins + " PM");
					} else {
						modHistory.setResheduleTime((hours - hoursDecrease)
								+ ":" + mins + " PM");
					}

					logger.log(Level.SEVERE, "Service Time::::::"
							+ (hours - hoursDecrease) + ":" + mins + " PM");

				} else {
					logger.log(Level.SEVERE, "Service Time::::::" + hours + ":"
							+ mins + " AM");
					modHistory.setResheduleTime(hours + ":" + mins + " AM");
				}
				modHistory.setReason(rescheduleHistory
						.optString("rescheduledReason"));

				// List<ModificationHistory> modList=new
				// ArrayList<ModificationHistory>();
				// modList.add(modHistory);
				// ModificationHistoryTable table = new
				// ModificationHistoryTable();
				// table.getDataprovider().getList().add(modHistory);
				service.getListHistory().add(modHistory);

			}
			/********************* Added Rate Contract Flag Logic *********************/
			ServiceImplementor serviceImpl = new ServiceImplementor();
			/** date 06.03.2019 commented by komal as it is working only for rate contract billing**/
//			if (isRateContractService) {
//				ServiceImplementor serviceImpl = new ServiceImplementor();
//				serviceImpl.updateRateContractBilling(service,
//						isRateContractService);
//			}
			
			service.setCustomerSignName(customerNameOfSignature);
			service.setCustomerSignNumber(customerMobileOfSignature);

			ofy().save().entity(service).now();
			
			/** date 09/05/2018 added by komal  for call log completion**/
			if(service.isCallwiseService()){
				CustomerLogDetails customerLog = ofy().load().type(CustomerLogDetails.class).filter("companyId", Long.parseLong(companyId)).filter("count", service.getContractCount()).first().now();		
				customerLog.setStatus(CustomerLogDetails.CLOSED);
				/** date 6.7.2018 added by komal for closing date and resolution**/
				customerLog.setClosingDate(new Date());
				if(service.getTechnicianRemark()!=null){
					customerLog.setResolution(service.getTechnicianRemark());
				}
				/**
				 * end komal
				 */
				ofy().save().entity(customerLog);
			}else{
				/** date 5.3.2018 added by komal to create or update billings(for servicewise billing and rate contract service billing**/
				serviceImpl.saveStatus(service);			
			}
			/**
			 * end komal
			 */
			/** date 03.06.2019 added by komal to save lat and long in customer and customer branches**/
			String taskName="updateServiceAddressLatLong"+"$"+companyId+"$"+service.getCount();
			Queue queue = QueueFactory.getQueue("documentCancellation-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));
			
			/** date 16/07/2018 added by komal to send email**/
			sendEmail(service);
			// Added by Apeksha for sending notification and sms to customerr
			// after service completed successfully.
			try {
				// sendMessageToCustomer(service);
				CustomerUser customerUser = ofy()
						.load()
						.type(CustomerUser.class)
						.filter("companyId", Long.parseLong(companyId))
						.filter("cinfo.count",
								service.getPersonInfo().getCount()).first()
						.now();
				String response = "Service Completed Successfully.";
				try{
				SmsServiceImpl smsimpl = new SmsServiceImpl();
				String sms =sendSmsToClientFromMob(smsimpl, service, sdf);
				logger.log(Level.SEVERE,"sms "+sms);

//				sendPushNotification(Long.parseLong(companyId),
//						AppConstants.IAndroid.EVA_KRETO, customerUser
//								.getUserName().trim(), response,service,comp);
				}catch(Exception e){
					logger.log(Level.SEVERE,"SMS Error or Push Notification");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
			exep = e;
			resp.getWriter().println("Failed");
			logger.log(Level.SEVERE, "Error::::::::" + e);
		}

		if (exep == null) {
			resp.getWriter().println("Success");
		}
	}

	private String sendSmsToClientFromMob(SmsServiceImpl smsimpl,
			Service service, SimpleDateFormat sdf) {

		logger.log(Level.SEVERE, "Inside sendSmsToClientFromMob SMS send");

		SmsConfiguration smsConfig = ofy().load().type(SmsConfiguration.class)
				.filter("companyId", comp.getCompanyId()).first().now();

//		if (smsConfig != null) {
//			if (smsConfig.getStatus() == false) {
//				return "SMS Config Not Active";
//			}
			String senderId = smsConfig.getAccountSID();
			String userName = smsConfig.getAuthToken();
			String password = smsConfig.getPassword();

			SmsTemplate smsTemplate = ofy().load().type(SmsTemplate.class)
					.filter("companyId", comp.getCompanyId())
					.filter("event", "Service Completion").first().now();
			if (smsTemplate == null) {
				return "SMS Template not found!";
			}
			if (smsTemplate != null) {
				if (smsTemplate.getStatus() == false) {
					return "SMS Template not Active!!";
				} else {

					String templateMsgwithbraces = smsTemplate.getMessage();
					System.out.println("SMS===" + templateMsgwithbraces); 
					String cutomerName = templateMsgwithbraces.replace(
							"{CustomerName}", service.getPersonInfo()
									.getFullName());
					String productName = cutomerName.replace("{ProductName}",
							service.getProductName());
					String serviceDate = productName.replace("{ServiceDate}",
							sdf.format(service.getServiceDate()));
					/** date 20.12.2018 added by komal to send service number through sms when 
					 * service is completed from app
					 */
					String serviceNo = serviceDate.replace("{ServiceNo}", service.getServiceSrNo()+"");
					String companyName = comp.getBusinessUnitName();
					String actualMsg = serviceNo.replace("{companyName}",
							companyName);
					System.out.println("actaulMsg" + actualMsg);
					logger.log(Level.SEVERE,
							"Calling SMS API method to send SMS"+actualMsg);
//					int value = smsimpl.sendSmsToClient(actualMsg, service
//							.getPersonInfo().getCellNumber(), senderId,
//							userName, password, comp.getCompanyId());
//					System.out.println(" Valuee===" + value);
//					if (value == 1) {
//						return "SMS sent successfully!!";
//					}

					/**
					 * @author Vijay Date :- 29-04-2022
					 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
					 * whats and sms based on communication channel configuration.
					 */
					smsimpl.sendMessage(AppConstants.SERVICEMODULE,AppConstants.SERVICE,smsTemplate.getEvent(),smsTemplate.getCompanyId(),actualMsg,service.getPersonInfo().getCellNumber(),false);
					logger.log(Level.SEVERE,"after sendMessage method");
					
				}

			}

//		}

		return "Failed to send sms!!";

	}

	/*
	 * Date: 31 Jul 2017 By: Apeksha Gunjal send Message to Client when Service
	 * is completed successfully.
	 */

	private void sendMessageToCustomer(Service service) {
		// TODO Auto-generated method stub
		SmsServiceImpl smsServiceImpl = new SmsServiceImpl();
		smsServiceImpl.sendSmsToCustomers(null, null, null, null, null, false);
	}

	/*
	 * Date: 31 Jul 2017 By: Apeksha Gunjal send Push Notification to Client
	 * when Service is completed successfully.
	 */

//	private void sendPushNotification(long companyId, String applicationName,
//			String userName, String response, Service service, Company comp2) {
//		response="event:Mark Completed"+"$"+service.getProductName()+" service has been completed by "+service.getEmployee()+". Would you like to rate?";//getJSONResponse(service);
//		CustomerUser customerUser=ofy().load().type(CustomerUser.class).filter("companyId",companyId).filter("cinfo.fullName",service.getPersonInfo().getFullName()).first().now(); 
//		RegisterDevice device = ofy().load().type(RegisterDevice.class)
//				.filter("companyId", companyId)
//				.filter("applicationName", applicationName)
//				.filter("userName", customerUser.getUserName()).first().now();
//		String deviceName = device.getRegId().trim();
//		logger.log(Level.SEVERE,"deviceName"+deviceName);
//		String url="http://my.evaesserp.appspot.com/slick_erp/notificationHandler?deviceName="+deviceName+"&"+"companyId="+companyId+"&applicationName="+applicationName;
//		
//		
//		
//		URL url = null;
//		try {
//			url = new URL("https://fcm.googleapis.com/fcm/send");
//		} catch (MalformedURLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		HttpURLConnection conn = null;
//		try {
//			conn = (HttpURLConnection) url.openConnection();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		conn.setDoOutput(true);
//		try {
//			conn.setRequestMethod("POST");
//		} catch (ProtocolException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		conn.setRequestProperty("Content-Type",
//				"application/json; charset=UTF-8");
//		conn.setRequestProperty("Authorization", "key="
//				+ comp2.getFcmServerKeyForKreto().trim());
//		conn.setDoOutput(true);
//		conn.setDoInput(true);
//
//		try {
//			conn.setRequestMethod("POST");
//		} catch (ProtocolException e) {
//			// TODO Auto-generated catch block
//			logger.log(Level.SEVERE, "ProtocolException ERROR::::::::::" + e);
//			e.printStackTrace();
//		}
//		OutputStream os = null;
//		try {
//			os = conn.getOutputStream();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
//			e.printStackTrace();
//		}
//		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
//		// Log.d("Test", "From Get Post Method" + getPostData(values));
//		try {
//			writer.write(createJSONData(response, deviceName));
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			logger.log(Level.SEVERE, "IOException2 ERROR::::::::::" + e);
//			e.printStackTrace();
//		}
//		try {
//			writer.flush();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			logger.log(Level.SEVERE, "IOException3 ERROR::::::::::" + e);
//			e.printStackTrace();
//		}
//
//		InputStream is = null;
//		try {
//			is = conn.getInputStream();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
//			e.printStackTrace();
//		}
//		BufferedReader br = new BufferedReader(new InputStreamReader(is));
//		String temp;
//		String data = null;
//		try {
//			while ((temp = br.readLine()) != null) {
//				data = data + temp;
//			}
//			logger.log(Level.SEVERE, "data data::::::::::" + data);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
//			e.printStackTrace();
//		}
//
//		logger.log(Level.SEVERE, "Data::::::::::" + data);
//	}

	private String getJSONResponse(Service service) {
		// TODO Auto-generated method stub
		JSONObject obj=new JSONObject();
		try {
			obj.put("status",1);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			obj.put("message",service.getProductName()+" service has been completed by "+service.getEmployee()+". Would you like to rate?");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return obj.toString();
	}

	private String createJSONData(String message, String deviceName) {
		// TODO Auto-generated method stub
		org.json.simple.JSONObject jObj1 = new org.json.simple.JSONObject();
		jObj1.put("message", message);

		org.json.simple.JSONObject jObj = new org.json.simple.JSONObject();
		jObj.put("data", jObj1);
		jObj.put("to", deviceName);

		String jsonString = jObj.toJSONString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE, "JSON Data Sent:::::::" + jsonString);

		return jsonString;
	}
	/** date 16.7.2018 added by komal**/
	public void sendEmail(Service service){
		
		String fromEmail="";
		Company comp=ofy().load().type(Company.class).filter("companyId", service.getCompanyId()).first().now();
		logger.log(Level.SEVERE,"customer --"+comp.getEmail());
		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add(comp.getEmail());
		if(comp!=null){
			if(comp.getEmail()!=null){
				fromEmail=comp.getEmail();
			}
		
		String techName  ="";
		if(service.getEmployee()!=null){
			techName= service.getEmployee();
		}
		String mailSub=""; //thank you for reaching out to us. you have enquired about product name
		String mailMsg="";
			mailSub="Service Completion";
			mailMsg="Dear Sir/Madam"+","+"\n"+"\n"+"\n"
							+"Service : "+service.getCount() +" has been completed by "+techName +"\n"+
							"with resolution "+ service.getTechnicianRemark()+" \n"+"\n"+"\n";
						
		
		Email obj=new Email();
		obj.sendEmail(mailSub, mailMsg, toEmailList, new ArrayList<String>(), fromEmail);
		logger.log(Level.SEVERE,"EMAIL Sending --"+comp.getEmail());
		
//		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "SendSRCopyOnServiceCompletion", service.getCompanyId())){
//			sendEmailtoCustomer(service , obj);
//		}
		}
	}	
	 
	public void sendEmailtoCustomer(Service serviceObj , Email obj){
		List<Service> servicelist = new ArrayList<Service>();
		servicelist.add(serviceObj);
		
		ArrayList<String> toEmailList = new ArrayList<String>();
//		try {
//			Thread.sleep(180000);
//		} catch (InterruptedException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		
		String branchEmail = "";
		Branch branch = null ;
		Company comp = ofy().load().type(Company.class).filter("companyId", serviceObj.getCompanyId()).first().now();
		if(serviceObj.getBranch() != null){
			branch = ofy().load().type(Branch.class).filter("companyId", serviceObj.getCompanyId()).filter("buisnessUnitName", serviceObj.getBranch()).first().now();
		}
		if(branch != null && branch.getEmail() != null && !branch.getEmail().equals("")){
			branchEmail = branch.getEmail();
			toEmailList.add(branchEmail);
		}
		
		ArrayList<String> ccEmailList = new ArrayList<String>();
		String fromEmailId = "";
		
		/**
		 * @author Anil , Date : 05-03-2020
		 * Right now Pestinct is send in all mail for all client
		 * it should be only for NBHC
		 */
		String compName="SR - ";
		String compEmail=comp.getEmail();
		if(comp!=null&&comp.getBusinessUnitName()!=null){
			if(comp.getBusinessUnitName().contains("NBHC")){
				compName="Pestinct SR - ";
				compEmail="pestinct@nbhcindia.com";
			}
		}
		String mailSub = compName+serviceObj.getPersonInfo().getFullName()+" "+serviceObj.getCount();
		String productName = "";
		if(serviceObj.getProductName()!=null && !serviceObj.getProductName().equalsIgnoreCase("")) {
			productName = serviceObj.getProductName();
		}
//		String mailSub = "Pestinct SR - "+serviceObj.getPersonInfo().getFullName()+" "+serviceObj.getCount();
		String mailMsg = "Dear Customer , <br></br> Please find the service record of our "+productName+" carried out at your premises.<br></br>For any queries contact us at: "+compEmail;
		logger.log(Level.SEVERE,"mail msg --"+mailMsg);
	//	Service service = ofy().load().type(Service.class).filter("companyId", serviceObj.getCompanyId()).filter("count", serviceObj.getCount()).first().now();
		/**
		 * @author Anil
		 * @since 27-05-2020
		 * Raised by Rahul Tiwari
		 * Premium tech - if email id is present on customer branch then instead of customer mail to be sent to branch else customer
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "ComplainServiceWithTurnAroundTime", serviceObj.getCompanyId())){
			if(serviceObj.getServiceBranch()!=null&&!serviceObj.getServiceBranch().equals("")&&!serviceObj.getServiceBranch().equals("Service Address")){
				CustomerBranchDetails custBranch = ofy().load().type(CustomerBranchDetails.class).filter("companyId", serviceObj.getCompanyId()).filter("buisnessUnitName", serviceObj.getServiceBranch()).filter("cinfo.count", serviceObj.getPersonInfo().getCount()).first().now();
				if(custBranch != null && custBranch.getEmail() != null&&!custBranch.getEmail().equals("")){
					toEmailList.add(custBranch.getEmail());
				}else{
					Customer cust = ofy().load().type(Customer.class).filter("companyId", serviceObj.getCompanyId()).filter("count", serviceObj.getPersonInfo().getCount()).first().now();
					if(cust != null && cust.getEmail() != null){
						toEmailList.add(cust.getEmail());
					}	
				}
			}else{
				Customer cust = ofy().load().type(Customer.class).filter("companyId", serviceObj.getCompanyId()).filter("count", serviceObj.getPersonInfo().getCount()).first().now();
				if(cust != null && cust.getEmail() != null){
					toEmailList.add(cust.getEmail());
				}
			}
			logger.log(Level.SEVERE,"customer -- == "+ toEmailList);
			
			/**
			 * @author Vijay Chougule Date 28-12-2020
			 * Des :- Email From Sales Person and CC in Approver Name and Branch Email id and Company Email Id
			 * Sending Emails from Sengrid
			 */
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", "EnableSendEmailsFromSalesPerson", serviceObj.getCompanyId())) {
				Email email = new Email();
				fromEmailId = comp.getEmail();
				ccEmailList.add(comp.getEmail());
				
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", serviceObj.getCompanyId())){
					String branchEmailId = email.getBranchEmailId(serviceObj.getBranch(),serviceObj.getCompanyId());
					if(branchEmailId!=null && !branchEmailId.equals("")) {
						fromEmailId = branchEmailId;
					}
				}

				String branchEmailId = email.getBranchEmailId(serviceObj.getBranch(), serviceObj.getCompanyId());
				if(branchEmailId!=null) {
					ccEmailList.add(branchEmailId);
				}
				Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", serviceObj.getCompanyId())
										.filter("count", serviceObj.getContractCount()).first().now();
				logger.log(Level.SEVERE,"contractEntity"+contractEntity);
				if(contractEntity!=null) {
					if(contractEntity.getAccountManager()!=null) {
						logger.log(Level.SEVERE,"contractEntity getAccountManager "+contractEntity.getAccountManager());
						User userEntity = ofy().load().type(User.class).filter("companyId",serviceObj.getCompanyId())
											.filter("employeeName", contractEntity.getAccountManager()).first().now();
						logger.log(Level.SEVERE,"userEntity"+userEntity);
						if(userEntity!=null && userEntity.getEmail()!=null && !userEntity.getEmail().equals("")) {
							ccEmailList.add(userEntity.getEmail());
							logger.log(Level.SEVERE,"userEntity.getEmail()"+userEntity.getEmail());

						}
					}
				}
			}
			/**
			 * ends here
			 */
			
			
		}else{
			Customer cust = ofy().load().type(Customer.class).filter("companyId", serviceObj.getCompanyId()).filter("count", serviceObj.getPersonInfo().getCount()).first().now();
			if(cust != null && cust.getEmail() != null){
				toEmailList.add(cust.getEmail());
			}
			logger.log(Level.SEVERE,"customer --"+ cust.getEmail());
		}
		String ssr1="";
		String ssr2="";

		
		if(serviceObj.getUptestReport()!=null){
			ssr1=serviceObj.getUptestReport().getName();
		}

		if(serviceObj.getServiceSummaryReport2()!=null){
			ssr2=serviceObj.getServiceSummaryReport2().getName();
		}
		

		if(serviceObj.getServiceSummaryReport2()!=null) {
		logger.log(Level.SEVERE,"serviceObj.getServiceSummaryReport2().getName()"+ serviceObj.getServiceSummaryReport2().getName());
		}
		logger.log(Level.SEVERE,"ssr1"+ ssr1);
		logger.log(Level.SEVERE,"ssr1"+ ssr2);
		if(fromEmailId.equals("")) {
			logger.log(Level.SEVERE,"comp.getEmail() "+ comp.getEmail());
			fromEmailId = comp.getEmail();
		}
		
		String[] fileName = { "SR Copy -" +serviceObj.getCount() + " ",ssr1,ssr2};
		obj.createSRCopyttachment(servicelist);
		try {	
			if(toEmailList.size() > 0){
				obj.sendNewEmail(toEmailList, mailSub, "", 0, "", new Date(), comp,	
					null, null, null, null, null, null, mailMsg, null, null,fileName, null,ccEmailList,fromEmailId);
		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"Exception ====="+ e.getCause());
			e.printStackTrace();
		}
	
		

	}
}
