package com.slicktechnologies.server.android;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;

public class paymentUpdationServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8692549218565739866L;
	
	Logger logger = Logger.getLogger("Logger");
	SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");

//	JSON DATA should receive below format
//	uploadPaymentDetails: Json{"cheque-Bank Account No":"","Tds Pecentage":"0","cheque Date":"",
//		"is Tds Applicable":"No","netReceivable":"2000","paymentId":"100000226","cheque Number":"","Tds Amount":"0",
//		"paymentMethod":"Cash","invoiceId":"900000082","cheque-Bank Name":"","NEFT-Transfer Date":"","paymentDate":"10/15/2020",
//		"reference No":""}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		format.setTimeZone(TimeZone.getTimeZone("IST"));
		
		String strJsonData = req.getParameter("paymentUpdationData");
		String actionTask = req.getParameter("actionTask");
		String strcompanyId = req.getParameter("companyId");
		long companyId = Long.parseLong(strcompanyId);
		
		/**
		 * when this API will use add validation part then call below task queue to payment Updation
		 */
		
		logger.log(Level.SEVERE, "Json data "+strJsonData);
		
		try {
			Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue")
					.param("EntityName",actionTask).param("companyId", companyId+"")
					.param("paymentUpdationData",strJsonData) );
			
			} catch (Exception e) {
				logger.log(Level.SEVERE,"Exception =="+e.getMessage());
			}

	}
	
}
