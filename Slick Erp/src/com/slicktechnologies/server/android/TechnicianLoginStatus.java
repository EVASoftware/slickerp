package com.slicktechnologies.server.android;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.mindrot.jbcrypt.BCrypt;

import com.googlecode.objectify.annotation.Index;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.RoleDefinition;
import com.slicktechnologies.shared.common.android.RegisterDevice;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.LicenseDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.ProductCategoryChecklist;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.role.UserRole;

import org.json.JSONException;
import org.json.JSONObject;
/**
*This API is called from all apps when user login into App which checks whether user is registered or not and also checks credentials.
*/
public class TechnicianLoginStatus extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1003562251938648579L;


	Logger logger = Logger.getLogger("TechnicianLoginStatus.class");

	Company comp;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doGet(req, resp);
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doPost(req, resp);
		
//		resp.setHeader("Access-Control-Allow-Origin", AppConstants.TRENDZWEBSOLUTION);
		resp.setHeader("Access-Control-Allow-Origin", "*");
		/**
		 * @author Anil @since 14-06-2021
		 * Thai character was not getting download in proper format
		 * raised by Rahul Tiwari
		 */
//		resp.setContentType("text/plain");
		resp.setContentType("text/plain; charset=UTF-8");
		resp.setCharacterEncoding("UTF-8");

		RegisterServiceImpl impl=new RegisterServiceImpl();
		
		String urlCalled=req.getRequestURL().toString().trim();
//		String url=urlCalled.replace("http://", "");
//		String[] splitUrl=url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		Exception exep = null;
		try {

			
			comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();resp.setContentType("text/plain");
			String username = req.getParameter("username").trim();
			String password = req.getParameter("password").trim();
//			String regId=req.getParameter("regId").trim();
			String imeiNumber=req.getParameter("imeiNumber").trim();
			String applicationName=req.getParameter("applicationName").trim();
			String companyIdString =comp.getCompanyId()+"";
			long companyId = Long.parseLong(companyIdString);
			
			
			/**Date 10-1-2020 by AMOL Send a Attendance Intime ,OutTime, InAddress ,OutAddress based on Attendance Date***/
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			String attendanceDateInString=null;
			
			try{
			attendanceDateInString=req.getParameter("attendanceDate").trim();
			}catch(Exception e){
				attendanceDateInString=null;
			}
			
			Date attendanceDate=null;
			if(attendanceDateInString!=null){
				try {
					attendanceDate=sdf.parse(attendanceDateInString);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			
			User user = null;
			// If encrypted
			user = ofy().load().type(User.class).filter("companyId", companyId)
					.filter("userName", username.trim()).first().now();
	//		RegisterDevice registerDevice=ofy().load().type(RegisterDevice.class).filter("companyId", comp.getCompanyId()).filter("imeiNumber",Long.parseLong(imeiNumber.trim())).first().now();
			LoginStatusCheckServlet logStatus = new LoginStatusCheckServlet();
			String returnValue = logStatus.validateLicenseDate(companyId,user,applicationName,imeiNumber);
			if(user!=null&&imeiNumber!=null)
				logger.log(Level.SEVERE , "User "+user.getEmployeeName()+" has logged into the pedio application at"+new Date() + "imei number="+imeiNumber);
			
			if(!returnValue.equalsIgnoreCase("Success")){
				resp.getWriter().println(returnValue);
				return;
			}
			
			RegisterDevice registerDevice=ofy().load().type(RegisterDevice.class).filter("companyId", companyId).filter("imeiNumber",Long.parseLong(imeiNumber.trim())).filter("status", true).filter("applicationName", applicationName).first().now();	
			if(registerDevice == null){
				logger.log(Level.SEVERE , "Device is not registered.");
				resp.getWriter().println("Device is not registered.");
				return;
			}
			
			if(registerDevice!=null){
				logger.log(Level.SEVERE,"registerDevice"+registerDevice.getImeiNumber());
				registerDevice.setEmployeeName(user.getEmployeeName());
				registerDevice.setUserName(username);
				registerDevice.setUserId(username);
				ofy().save().entity(registerDevice);
			}
			
			

			logger.log(Level.SEVERE, "username" + username);
			logger.log(Level.SEVERE, "password" + password);
			logger.log(Level.SEVERE, "companyId" + companyId);
			
			
			boolean status;
//			if(user.getRoleName().equalsIgnoreCase("Admin")){
			status = checkUserLogin(user,username, password, companyId);
			
			if (status) {
				/*
				 * Date: 31 Jul 2017
				 * By: Apeksha Gunjal
				 * Comment Below code because all Register Details are saved at the time of Registration
				 * so no need to to save it here 
				 */
				
				/*RegisterDevice registerDeviceFound=new RegisterDevice();
				registerDeviceFound=ofy().load().type(RegisterDevice.class).filter("companyId", companyId).filter("imeiNumber", imeiNumber.trim()).first().now();
				logger.log(Level.SEVERE,"registerDeviceFound:::::::::"+registerDeviceFound);
				if(registerDeviceFound==null){
				RegisterDevice registerDevice=new RegisterDevice();
				registerDevice.setImeiNumber(imeiNumber);
				registerDevice.setUserName(user.getUserName().trim());
				registerDevice.setRegId(regId);
				registerDevice.setCompanyId(companyId);
				registerDevice.setEmployeeName(user.getEmployeeName());
				ofy().save().entity(registerDevice);
				}else{
					registerDeviceFound.setCompanyId(companyId);
					registerDeviceFound.setImeiNumber(imeiNumber);
					registerDeviceFound.setUserName(user.getUserName().trim());
					registerDeviceFound.setRegId(regId);
					registerDeviceFound.setEmployeeName(user.getEmployeeName());
					ofy().save().entity(registerDeviceFound);
				}*/
				//				registerDevice.setEmployeeId();
				String jsonString=createUserDetails(user , applicationName,attendanceDate);
				
				resp.getWriter().println(jsonString);
			} else {
				resp.getWriter().println("Failed");
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE,"Error :::"+e);
			e.printStackTrace();
		}
	}

	private String createUserDetails(User user ,  String applicationName, Date attendanceDate) {
		// TODO Auto-generated method stub
		JSONObject obj = null;
		try {
			obj = new JSONObject();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"ERROR stage 1"+e1);
			e1.printStackTrace();
		}
		try {
			obj.put("userName", user.getUserName().trim());
		} catch (JSONException e) {
			logger.log(Level.SEVERE,"ERROR stage 2"+e);
			e.printStackTrace();
		}
		try {
			obj.put("password", user.getPassword().trim());
		} catch (JSONException e) {
			logger.log(Level.SEVERE,"ERROR stage 3"+e);
			e.printStackTrace();
		}
		try {
			obj.put("userRole", user.getRoleName().trim());
		} catch (JSONException e) {
			logger.log(Level.SEVERE,"ERROR stage 4"+e);
			e.printStackTrace();
		}
		
		try {
			obj.put("employeeRole", user.getRoleName().trim());
		} catch (JSONException e) {
			logger.log(Level.SEVERE,"ERROR stage 4"+e);
			e.printStackTrace();
		}
		
		Employee employee=ofy().load().type(Employee.class).filter("companyId", user.getCompanyId()).filter("fullname", user.getEmployeeName().trim()).first().now();
		try {
			obj.put("employeeId", employee.getCount()+"");
			obj.put("employeeName", employee.getFullname()+"");
			if(employee.getPhoto()!=null){
				obj.put("employeeProfileURL", employee.getPhoto().getUrl());
			}else{
				obj.put("employeeProfileURL","");
			}
			JSONArray jBranchArray=new JSONArray();
			if(employee.getEmpBranchList().size()>0){
				for (EmployeeBranch empBranch:employee.getEmpBranchList()) {
					JSONObject branchObj=new JSONObject();
					branchObj.put("branchName",employee.getBranchName());
					jBranchArray.add(branchObj);
				}	
			}else{
				JSONObject branchObj=new JSONObject();
				branchObj.put("branchName",employee.getBranchName());
				jBranchArray.add(branchObj);
			}
			obj.put("branch", employee.getBranchName());
			obj.put("branchList",jBranchArray);
			
			/**
			 * @author Vijay Date :- 08-09-2022
			 * Des :- as per Nitin sir we should check role with user role
			 * not with employe role so below code commented and sending user role in  employeeRole parameter
			 */
			
//			/**
//			 * @author Anil , Date " 26-08-2019
//			 * Setting Employee role and employee cell
//			 */
//			if(employee.getRoleName()!=null){
//				obj.put("employeeRole",employee.getRoleName());
//			}
			/**
			 * ends here
			 */
			
			if(employee.getCellNumber1()!=null){
				obj.put("employeeCellNumber",employee.getCellNumber1());
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"ERROR stage 5"+e);
			e.printStackTrace();
		}
		JSONArray jArray = new JSONArray();
		UserRole roleAuth=ofy().load().type(UserRole.class).filter("companyId", user.getCompanyId()).filter("roleName", user.getRoleName().trim())
						.filter("authorization.module", applicationName).first().now();
		if(roleAuth != null){
		//	logger.log(Level.SEVERE,"roleAuth"+roleAuth.getRoleName());
		for (int j = 0; j < roleAuth.getAuthorization().size(); j++) {
			if(roleAuth.getAuthorization().get(j).isAndroid() && roleAuth.getAuthorization().get(j).getModule().equalsIgnoreCase(applicationName)){
			JSONObject screenName = new JSONObject();
			try {
			//	logger.log(Level.SEVERE,"moduleName "+roleAuth.getAuthorization().get(j).getModule());
				screenName.put("moduleName", roleAuth.getAuthorization().get(j).getModule());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
			//	logger.log(Level.SEVERE,"screenName "+roleAuth.getAuthorization().get(j).getScreens());
				
				screenName.put("screenName", roleAuth.getAuthorization().get(j).getScreens());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			try {
				if(screenName.get("moduleName")!=null&&screenName.get("screenName")!=null&&  screenName.get("moduleName").equals("EVA Pedio")&&screenName.get("screenName").equals("Audit")) {
					if(ServerAppUtility.checkIfLicensePresent(comp, AppConstants.LICENSETYPELIST.AUDITLICENSE)) {
						logger.log(Level.SEVERE,"Audit license present");
						jArray.add(screenName);
					}else {
						logger.log(Level.SEVERE,"Audit license not found");
					}
				}else
					jArray.add(screenName);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			}
		}
		}
		try {
			obj.put("screenList", jArray);
		} catch (JSONException e) {
			logger.log(Level.SEVERE,"ERROR stage 5"+e);
			e.printStackTrace();
		}
		/** date 20.3.2019 added by komal to load pest name **/
		JSONArray pestNameArray = new JSONArray();
		List<Config> pestNameList=ofy().load().type(Config.class).filter("companyId", comp.getCompanyId()).filter("type",92).list();
		for(Config config : pestNameList){
			JSONObject pestNameObj = new JSONObject();
			try {
				pestNameObj.put("name", config.getName());
				pestNameArray.add(pestNameObj);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		try {
			obj.put("pestNameList", pestNameArray);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/**
		 * end komal
		 */
		/**@Sheetal : 08-02-2022
		 *  Des : if product wise checklist exist for that product (product category) then that checklist will get print **/
		boolean checklistFlag= false;
		List<Category> list=ofy().load().type(Category.class).filter("companyId", comp.getCompanyId()).filter("isServiceCategory",true).list();
		for(Category category : list){
			if(category.getcheckList().size()>0){
				checklistFlag=true;
				break;
			}  
		}
		if(checklistFlag){
			JSONArray checklistArray2 = new JSONArray();


			for(Category category : list){
				JSONArray checklistArray = new JSONArray();

				JSONObject checkObj2 = new JSONObject();
				try {
					checkObj2.put("productCategory", category.getCatName());
				} catch (JSONException e) {
					e.printStackTrace();
				}
			
				for(ProductCategoryChecklist productchecklist : category.getcheckList()){
					JSONObject checkObj = new JSONObject();
					try {
						checkObj.put("name", productchecklist.getChecklistName());
					} catch (JSONException e) {
						e.printStackTrace();
					}
					
					checklistArray.add(checkObj);

				}
			
				try {
					checkObj2.put("productchecklistName", checklistArray);
					
					checklistArray2.add(checkObj2);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			try {
				obj.put("productCategoryWisechecklist", checklistArray2);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}/**end**/	
		/**
		 * @author Anil
		 * @since 11-08-2020
		 */
		JSONArray checkListArray = new JSONArray();
		List<Config> checklist=ofy().load().type(Config.class).filter("companyId", comp.getCompanyId()).filter("type",122).list();
		for(Config config : checklist){
			JSONObject checkObj = new JSONObject();
			try {
				checkObj.put("name", config.getName());
				checkListArray.add(checkObj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
		}
		try {
			obj.put("checklist", checkListArray);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
		
		/** date 20.3.2019 added by komal to load pest name **/
		JSONArray specificReasonArray = new JSONArray();
		List<Config> specificReasonList=ofy().load().type(Config.class).filter("companyId", comp.getCompanyId()).filter("type",89).list();
		for(Config config : specificReasonList){
			JSONObject specificReasonObj = new JSONObject();
			try {
				specificReasonObj.put("name", config.getName());
				specificReasonArray.add(specificReasonObj);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		try {
			obj.put("specificReasonList", specificReasonArray);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/**
		 * end komal
		 */
		boolean modelAndSerialNoFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","HideOrShowModelNoAndSerialNoDetails", comp.getCompanyId());
		try {
			obj.put("modelAndSerialNoFlag", modelAndSerialNoFlag);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean premisesFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ShowProductPremises", comp.getCompanyId());
		try {
			obj.put("premisesFlag", premisesFlag);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean otpFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","SendOTPForServiceCompletion", comp.getCompanyId());
		try {
			obj.put("otpFlag", otpFlag);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		boolean materialmandatory = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","MakeMaterialMandatory", comp.getCompanyId());
		try {
			obj.put("materialmandatory", materialmandatory);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONArray configArray = new JSONArray();
		ProcessConfiguration config = ofy().load().type(ProcessConfiguration.class).filter("companyId", comp.getCompanyId())
				.filter("processList.processName", applicationName)
				.filter("processList.status", true).first().now();
		if(config != null){
			for(ProcessTypeDetails details :config.getProcessList()){
				if(details.isStatus()){
					if(details.getProcessName().equals("EVA Pedio")&&details.getProcessType().equals("PC_ONLYATTENDANCE")) {
						if(user!=null&&!user.getRole().getRoleName().equalsIgnoreCase("Technician")&&!user.getRole().getRoleName().equalsIgnoreCase("Operator")&&!user.getRole().getRoleName().equalsIgnoreCase("ADMIN")) {
							JSONObject object = new JSONObject();
							try {
								object.put("name",  details.getProcessName());
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							try {
								object.put("type",  details.getProcessType());
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
								configArray.add(object);
							}
						}else {
							JSONObject object = new JSONObject();
							try {
								object.put("name",  details.getProcessName());
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							try {
								object.put("type",  details.getProcessType());
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
								configArray.add(object);					
					
						}
				
				}
				
			}
		}
		try {
			obj.put("processConfigArray", configArray);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String employeeRole = "";
		RoleDefinition roleDef = ofy().load().type(RoleDefinition.class).filter("companyId", comp.getCompanyId())
				.filter("moduleName",AppConstants.SERVICEMODULE).filter("documentName",AppConstants.CUSTOMERPROJECT)
				.filter("roleList",employee.getRoleName()).filter("status",true).first().now();
		logger.log(Level.SEVERE , "ROLE: "+ roleDef);
		
		if(roleDef != null ){			
			employeeRole = roleDef.getRoleType();
		}
		try {
			obj.put("employeeGroup", employeeRole);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<SmsTemplate> templateList = ofy().load().type(SmsTemplate.class).filter("companyId", comp.getCompanyId()).filter("status",true).list();
		JSONArray smsTemplateArray = new JSONArray();
		if(templateList != null){
			for(SmsTemplate temp : templateList){
				JSONObject object = new JSONObject();
				try {
					object.put("event",  temp.getEvent());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					object.put("message",  temp.getMessage());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				smsTemplateArray.add(object);
			
			}
		}
		try {
			obj.put("smsTemplate", smsTemplateArray);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			obj.put("userId", user.getCount());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		
		/**Date 10-1-2020 by AMOL Send a Attendance Intime ,OutTime, InAddress ,OutAddress based on Attendance Date***/
		if(attendanceDate!=null){
		
		Attendance objects=ofy().load().type(Attendance.class).filter("companyId", comp.getCompanyId())
				.filter("empId", employee.getCount()).filter("attendanceDate", attendanceDate).first().now();
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		sdf1.setTimeZone(TimeZone.getTimeZone("IST"));
		
		if(objects==null){
			try{
			obj.put("inTime", "");
			obj.put("outTime", "");
			obj.put("markInAddress", "");
			obj.put("markOutAddress", "");
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}else{
			
			if(objects.getInTime()!=null){
			try {
				obj.put("inTime",sdf1.format(objects.getInTime()));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
			if(objects.getOutTime()!=null){
			try {
				obj.put("outTime", sdf1.format(objects.getOutTime()));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
			if(objects.getMarkInAddress()!=null){
			try {
				obj.put("markInAddress", objects.getMarkInAddress().trim());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
			
			if(objects.getMarkOutAddress()!=null){
			try {
				obj.put("markOutAddress", objects.getMarkOutAddress().trim());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			}
			}
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio", "CustomizedSrCopy", comp.getCompanyId())){
		try {
			obj.put("serviceRecordUrl","/slick_erp/serviceRecord");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}else{
		try {
			obj.put("serviceRecordUrl", "/slick_erp/pdfCustserjob");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		}
		
		}
	
		/**
		 * @author Vijay Date :- 20-11-2020
		 * Des :- as per nitin sir requirment added IMEI number in json 
		 */
		try {
		
		List<RegisterDevice> registerlist = ofy().load().type(RegisterDevice.class).filter("companyId", user.getCompanyId())
							.filter("employeeName", user.getEmployeeName()).filter("status", true).list();
		logger.log(Level.SEVERE, "registerlist"+registerlist.size());

		if(registerlist.size()!=0) {
		
			Comparator<RegisterDevice> registerDeviceComparator = new Comparator<RegisterDevice>() {
				public int compare(RegisterDevice r1, RegisterDevice r2) {
				
				Integer count = r1.getCount();
				Integer count2 = r2.getCount();
					
				//ascending order
				return count2.compareTo(count);
				}
				};
				Collections.sort(registerlist, registerDeviceComparator);
				
				try {
					obj.put("IMEI Number", registerlist.get(0).getImeiNumber());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		/**
		 * @author Anil @since 28-12-2021
		 * Adding type of treatment 
		 * Raised by Nithila and Nitin sir for innovative
		 */
		JSONArray typeOfTreatmentArray = new JSONArray();
		List<Config> typeOfTreatment=ofy().load().type(Config.class).filter("companyId", comp.getCompanyId()).filter("type",103).filter("status", true).list();
		for(Config object : typeOfTreatment){
			JSONObject checkObj = new JSONObject();
			try {
				checkObj.put("name", object.getName());
				typeOfTreatmentArray.add(checkObj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
		}
		try {
			obj.put("typeOfTreatment", typeOfTreatmentArray);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/**
		 * @author Anil @since 01-01-2022
		 * Adding risk level and assessment category for audit app
		 * Raised Nitin sir
		 */
		JSONArray riskLevelArray = new JSONArray();
		List<Config> riskLevel=ofy().load().type(Config.class).filter("companyId", comp.getCompanyId()).filter("type",123).filter("status", true).list();
		for(Config object : riskLevel){
			JSONObject checkObj = new JSONObject();
			try {
				checkObj.put("name", object.getName());
				riskLevelArray.add(checkObj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
		}
		try {
			obj.put("riskLevel", riskLevelArray);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		JSONArray assessmentCatArray = new JSONArray();
		List<ConfigCategory> assessmentCat=ofy().load().type(ConfigCategory.class).filter("companyId", comp.getCompanyId()).filter("internalType",26).filter("status", true).list();
		for(ConfigCategory object : assessmentCat){
			JSONObject checkObj = new JSONObject();
			try {
				checkObj.put("name", object.getCategoryName());
				assessmentCatArray.add(checkObj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
		}
		try {
			obj.put("assessmentCategory", assessmentCatArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		/**
		 * @author Anil @since 03-01-2022
		 * Telephony configuration fields for pedio and priora app
		 * Raised Nitin sir
		 */
		String accountSid="";
		String apiKey="";
		String apiToken="";
		String callerId="";
		/**
		 * @author Anil @since 17-01-2022
		 * Added api url value as requested by Nitin sir and Android team
		 */
		String apiUrl="";
		
		if(comp!=null){
			if(comp.getAccountSid()!=null){
				accountSid=comp.getAccountSid();
			}
			if(comp.getApiKey()!=null){
				apiKey=comp.getApiKey();
			}
			if(comp.getApiToken()!=null){
				apiToken=comp.getApiToken();
			}
			if(comp.getCallerId()!=null){
				callerId=comp.getCallerId();
			}
			
			if(comp.getApiUrl()!=null){
				apiUrl=comp.getApiUrl();
			}
		}
		JSONArray telephonyConfigArray = new JSONArray();
		JSONObject telephonyObj = new JSONObject();
		try {
			
			telephonyObj.put("accountSid", accountSid);
			telephonyObj.put("apiKey", apiKey);
			telephonyObj.put("apiToken", apiToken);
			telephonyObj.put("callerId", callerId);
			telephonyObj.put("apiUrl", apiUrl);
			telephonyConfigArray.add(telephonyObj);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		try {
			obj.put("telephonyConfig", telephonyConfigArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		/**
		 * @author Anil
		 * @since 15-02-2022
		 * Sending Authcode for authenticating audit app API's
		 */
		try {
			obj.put("authCode",comp.getCompanyId());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		if (applicationName.contains("Pedio")) {
			Date nearestEndDate=getNearestEndDate(comp, AppConstants.LICENSETYPELIST.EVA_PEDIO_LICENSE);
		
			SimpleDateFormat spf = new SimpleDateFormat("dd MMM yyyy"); // for playstore version
			//SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy"); for new release and psipl
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			spf.setTimeZone(TimeZone.getTimeZone("IST"));
			if(nearestEndDate!=null) {
				try {
					obj.put("licenseExpiryDate",spf.format(nearestEndDate));					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		//Ashwini Patil Date:21-10-2024 Innovative want thai labels in pedio. To manage that, sending country at the time of technician login. If the country is thailand then thai labels will be shown.
		if(comp!=null&&comp.getAddress()!=null&&comp.getAddress().getCountry()!=null) {
			try {
				if(comp.getAddress().getCountry().trim().equalsIgnoreCase("Thailand")||comp.getAddress().getCountry().trim().equalsIgnoreCase("ประเทศไทย"))				
					obj.put("country","Thailand");
				else
					obj.put("country",comp.getAddress().getCountry().trim());	
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
		
		
		String jsonString = obj.toString().replace("\\\\", "");
		logger.log(Level.SEVERE, jsonString);
		
		return jsonString;
	}

	public boolean checkUserLogin(User user, String username, String password,
			long companyId) {

		
		if (user.getStatus()) {

			if (user != null) {
				if(user.getPassword().trim().equals(password.trim())){
					logger.log(Level.SEVERE, "User Login Done");
					return true;
				}else{
					boolean matched = BCrypt.checkpw(password.trim(),
							user.getPassword());
					logger.log(Level.SEVERE, "Login matched value:::::" + matched);
	
					if (matched) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	
	public Date getNearestEndDate(Company c,String licenceCode) {
		Date endDate=null;
		SimpleDateFormat isoFormat = new SimpleDateFormat("dd-MM-yyyy");			
		Date todaysDate=DateUtility.getDateWithTimeZone("IST", new Date());
		logger.log(Level.SEVERE, "todaysDate="+todaysDate);
		if(c!=null) {
			ArrayList<LicenseDetails> licenseDetailsList = new ArrayList<LicenseDetails>();
			if (c.getLicenseDetailsList() != null&& c.getLicenseDetailsList().size() != 0) {
				logger.log(Level.SEVERE, "TOTAL EVA LICENSE LIST : "+ c.getLicenseDetailsList().size());
				// Console.log("TOTAL EVA LICENSE LIST : "+c.getLicenseDetailsList().size());
				
				for (LicenseDetails license : c.getLicenseDetailsList()) {
						if (licenceCode.equalsIgnoreCase(license.getLicenseType())
								&& license.getStatus() == true) {
							
							if ((license.getEndDate().after(todaysDate))&&(todaysDate.before(license.getEndDate())||isoFormat.format(todaysDate).equals(isoFormat.format(license.getEndDate())))) 
								licenseDetailsList.add(license);
						}				

				}
				logger.log(Level.SEVERE, "ACTIVE "+licenceCode+" LICENSE LIST SIZE : "+ licenseDetailsList.size());
			}
			else{
				logger.log(Level.SEVERE,"No licenses are allocated to you contact support. "+c.getCellNumber1());
			}

			
			if (licenseDetailsList.size() != 0) {
				endDate=licenseDetailsList.get(0).getEndDate();
				logger.log(Level.SEVERE, "initial endDate="+endDate);
				for (LicenseDetails license : licenseDetailsList) {
					logger.log(Level.SEVERE, license.getLicenseType()
							+ " No. Of License : " + license.getNoOfLicense()
							+ " LICENSE START DATE : " + license.getStartDate()
							+ " LICENSE END DATE : " + (license.getEndDate()));
					
					SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
					if ((endDate.after(license.getEndDate())&&endDate.after(todaysDate))&&(todaysDate.before(license.getEndDate())||isoFormat.format(todaysDate).equals(isoFormat.format(license.getEndDate())))) {
						endDate=license.getEndDate();
					} 
				}
			logger.log(Level.SEVERE, "nearest endDate="+endDate);
						
			}
		}
		return endDate;
	}
	
	
}
