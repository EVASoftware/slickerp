package com.slicktechnologies.server.android.contractwiseservice;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.omg.PortableServer.Servant;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.googlecode.objectify.ObjectifyService;
import com.slicktechnologies.client.Slick_Erp;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.device.TrackTableDetails;
import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.ServiceImplementor;
import com.slicktechnologies.server.ServiceListServiceImpl;
import com.slicktechnologies.server.android.technicianapp.TechnicianAppOperation;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.email.SendGridEmailServlet;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.FumigationServiceReport;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.ModificationHistory;
import com.slicktechnologies.shared.common.android.RegisterDevice;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.FeedbackUrlAsPerCustomerCategory;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.email.EmailTemplateConfiguration;
import com.slicktechnologies.shared.common.helperlayer.CustomerUser;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
/**
 * Description : It is used to final mark complete the sevices which are clubbed together.
	It mark service status as Completed and save customer remark , feedback , sign 	details. 
 * @author pc1
 *
 */
public class MarkCompletedMultipleServices extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1811057931247745874L;

	/**
	 * 
	 */

	Logger logger = Logger.getLogger("MarkCompletedSubmitServlet.class");
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

	Company comp;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doGet(req, resp);
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		/**
		 * @author Anil @since 14-06-2021
		 * Thai character was not getting download in proper format
		 * raised by Rahul Tiwari
		 */
//		resp.setContentType("text/plain");
		resp.setContentType("text/plain; charset=UTF-8");
		resp.setCharacterEncoding("UTF-8");
		
		String serviceLatnLong="";
		String urlCalled = req.getRequestURL().toString().trim();
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		Exception exep = null;
		try {
			
			/**
			 * @author Anil , Date : 09-07-2019
			 */
			ObjectifyService.reset();
			logger.log(Level.SEVERE, "Objectify Consistency Strong");
			ofy().consistency(Consistency.STRONG);
			ofy().clear();
			
			
			/***
			 * 
			 */
			
			
			SimpleDateFormat format = new SimpleDateFormat("HH:mm");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			format.setTimeZone(TimeZone.getTimeZone("IST"));
			SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			format1.setTimeZone(TimeZone.getTimeZone("IST"));
			String srNumber = format1.format(new Date());
			comp = ofy().load().type(Company.class)
					.filter("accessUrl", splitUrl[0]).first().now();
			logger.log(Level.SEVERE, "Stage One");
			String companyId = comp.getCompanyId() + "";
			logger.log(Level.SEVERE, "companyId::::::::::" + companyId);
			
			String servicesData=req.getParameter("servicesData").trim();
			logger.log(Level.SEVERE, "data::::::::::" + servicesData);
			JSONObject serviceJSONObject = new JSONObject(servicesData);
			JSONArray servicesArray = null;
			try {
				servicesArray = serviceJSONObject.getJSONArray("service");
				
			} catch (JSONException e1) {
				// TODO Auto-generated catch blocks
				e1.printStackTrace();
			}
			JSONObject obj = null;
			String serviceEngineer = "";
			Service serObject = null;
			String branchEmailId = "";
			ArrayList<String> serviceidlist = new ArrayList<String>();
			
			for(int m =0 ; m<servicesArray.length() ; m++){
				try {
					obj = servicesArray.getJSONObject(m);
					
			float feedbackRating = Float.parseFloat(obj.optString(
					"feedbackRating").trim());
			logger.log(Level.SEVERE, "feedbackRating::::::::::"
					+ feedbackRating);
			String custRemark = obj.optString("custRemark").trim();
			logger.log(Level.SEVERE, "custRemark::::::::::" + custRemark);
			String serviceId = obj.optString("serviceId").trim();
			serviceidlist.add(serviceId);
			
			String lat = obj.optString("lat").trim();
			String lon = obj.optString("lon").trim();
			/**
			 * Updated By: komal
			 * Date: 01-04-2019
			 * Description: To get and save location
			 */
			String location = "";
			try{
				location = obj.optString("location").trim();
			}catch(Exception e){
				location = "";
				e.printStackTrace();
			}
			
			String serviceWiseLatLong = obj.optString("serviceWiseLatLong")
					.trim();
			logger.log(Level.SEVERE, "serviceId::::::::::" + serviceId);
			boolean syncFlag = Boolean.parseBoolean(req
					.getParameter("syncFlag"));
			boolean isRateContractService = Boolean.parseBoolean(req
					.getParameter("isRateContractService"));
			/*
			 * Name: Apeksha Gunjal
			 * Date: 02/07/2018 @ 15:00
			 * Note: Added technician remark which is enter after completing service
			 */
			String technicianRemark = "";
			try{
				technicianRemark = obj.optString("technicianRemark").trim(); 
			}catch(Exception e){
				logger.log(Level.SEVERE, "technicianRemark error::::::::::" + e.getMessage());
			}
			/**
			 * Added By Rahul Verma Date: 25 Oct 2017
			 * 
			 */
			
			
			
			
			logger.log(Level.SEVERE , "Request :" + req.getParameterMap().size() );
			
			String customerNameOfSignature, customerMobileOfSignature;
			//	,markCompleteDate, markCompleteTime, 
			//	reportedObject = null, rescheduleObject = null;

			customerNameOfSignature = obj.optString("customerName").trim();
			customerMobileOfSignature = obj.optString("customerMobile")
					.trim();
//			if (syncFlag) {
//				markCompleteDate = obj.optString("markCompleteDate").trim();
//				markCompleteTime = obj.optString("markCompleteTime").trim();
//				reportedObject = obj.optString("reportedData").trim();
//				rescheduleObject = obj.optString("rescheduledData").trim();
//			}

			
			
			/**
			 * Rahul Verma added on 01 Oct 2018
			 * 
			 */
			try{
				serviceLatnLong = obj.optString("serviceLatnLong").trim();
				logger.log(Level.SEVERE,"serviceLatnLong:::::"+serviceLatnLong);
			}catch(Exception e){
				e.printStackTrace();
			}

			Service service = ofy().load().type(Service.class)
					.filter("companyId", Long.parseLong(companyId))
					.filter("count", Long.parseLong(serviceId)).first().now();
			logger.log(Level.SEVERE, "Service::::::" + service);
			// Adding in table
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
			timeFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			String newDate = sdf.format(new Date());
			service.setCompleteLatnLong(lat + "$" + lon);
			service.setServiceWiseLatLong(serviceWiseLatLong);
			
			
			/**
			 * @author Vijay Date :- 24-10-2020
			 * Des :- As per nitin sir instruction Completed must store in Date 
			 * format with date and time 
			 */
			String strActionDateTime = "";
			Date actionDateTime	= null;
			try {
				strActionDateTime = obj.optString("serviceActionDateTime");
				actionDateTime = sdf.parse(strActionDateTime);
			} catch (Exception e) {
				// TODO: handle exception
			}
			logger.log(Level.SEVERE,"strActionDateTime "+strActionDateTime);
			logger.log(Level.SEVERE,"actionDateTime "+actionDateTime);

			
			try{
			if(serviceLatnLong!=null)
				service.setLatnLong(serviceLatnLong);
			logger.log(Level.SEVERE, "Stage two");
			}catch(Exception e){
				e.printStackTrace();
			}
			try {
				TrackTableDetails trackTableDetails = new TrackTableDetails();
				trackTableDetails.setStatus(Service.SERVICESTATUSCOMPLETED);
				logger.log(Level.SEVERE,
						"service.getTrackServiceTabledetails().size()"
								+ service.getTrackServiceTabledetails().size());
				// if(service.getTrackServiceTabledetails()!=null){
				trackTableDetails.setSrNo(service.getTrackServiceTabledetails()
						.size() + 1);
				// }else{
				// trackTableDetails.setSrNo(0+1);
				// }
				/**
				 * Updated By: Viraj
				 * Date: 01-03-2019
				 * Description: To show lat and long
				 */
				trackTableDetails.setLatitude(lat);
				trackTableDetails.setLongitude(lon);
				/** Ends **/
				/**
				 * Updated By: komal
				 * Date: 01-04-2019
				 * Description: To get and save location and time
				 */
				trackTableDetails.setLocationName(location);
				/**
				 * @author Vijay Chougule Date 01-10-2020
				 * Des :- Sevice started reported tcompleted and completed Time issue in other country so now mapping timing from app not
				 * from the Date
				 */
				String completionTime="";
				try {
					completionTime = obj.optString("markCompleteTime").trim();
					if(!completionTime.equals("")){
						trackTableDetails.setTime(completionTime);
					}
					else{
						trackTableDetails.setTime(timeFormat.format(new Date()));

					}

				} catch (Exception e) {
					// TODO: handle exception
				}
				
				/**
				 * end komal
				 */
				
				if(strActionDateTime!=null && !strActionDateTime.equals("")){
					trackTableDetails.setDate_time(strActionDateTime);
				}
				else{
					trackTableDetails.setDate_time(newDate);
				}
//				trackTableDetails.setDate_time(newDate);
				
				
				service.getTrackServiceTabledetails().add(trackTableDetails);
				if(strActionDateTime!=null && !strActionDateTime.equals("")){
					service.setCompletedDate_time(strActionDateTime);
				}
				else{
					service.setCompletedDate_time(newDate);

				}
//				service.setCompletedDate_time(newDate);
				if(actionDateTime!=null){
					service.setCompletedDate(actionDateTime);
				}
				service.setCompletedLatitude(lat);
				service.setCompletedLongitude(lon);
				if(!completionTime.equals("")){
					service.setCompletedTime(completionTime);
				}
				else{
					service.setCompletedTime(timeFormat.format(new Date()));
				}

				service.setCompletedLocationName(location);
			} catch (Exception e) {
				e.printStackTrace();
				logger.log(Level.SEVERE, "Error::::::::in tracktable" + e);

			}
			logger.log(Level.SEVERE, "new Date::::::::::" + newDate);
			try {
				logger.log(Level.SEVERE, "After parse new Date:::::::::::"
						+ sdf.parse(newDate));
			} catch (ParseException e1) {
				e1.printStackTrace();
			}

			logger.log(Level.SEVERE, "Stage 3");
			try {
				service.setServiceCompletionDate(sdf.parse(newDate));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log(Level.SEVERE,
						"Error::::::::setServiceCompletionDate" + e);
			}

			logger.log(Level.SEVERE, "Stage 4");
			service.setServiceCompleteRemark(custRemark.trim());

			if (feedbackRating > 0f && feedbackRating <= 1f) {
				service.setCustomerFeedback("Poor");
			} else if (feedbackRating > 1f && feedbackRating <= 2f) {
				service.setCustomerFeedback("Average");
			} else if (feedbackRating > 2f && feedbackRating <= 3f) {
				service.setCustomerFeedback("Good");
			} else if (feedbackRating > 3f && feedbackRating <= 4f) {
				service.setCustomerFeedback("Very Good");
			} else if (feedbackRating > 4f && feedbackRating <= 5f) {
				service.setCustomerFeedback("Excellent");
			} else {
				service.setCustomerFeedback("");
			}
			/*
			 * Name: Apeksha Gunjal
			 * Date: 02/07/2018 @ 15:37
			 * Note: Added technician remark which is enter after completing service
			 */
			try{
				service.setTechnicianRemark(technicianRemark);
			}catch(Exception e){
				logger.log(Level.SEVERE, "error setting technician remark: "+e.getMessage());
			}

			logger.log(Level.SEVERE, "Stage 5");
			int reportedMin = 0;
			int completedMin = 0;
			/** date 23.03.2019 added by komal 
			 * purpose : to calculate service duration = completed - started **/
			int startedMin = 0;
			for (int i = 0; i < service.getTrackServiceTabledetails().size(); i++) {
				if (Service.SERVICESTATUSREPORTED.equalsIgnoreCase(service
						.getTrackServiceTabledetails().get(i).getStatus()
						.trim())) {
					String[] timeSplit = service.getTrackServiceTabledetails()
							.get(i).getDate_time().split(" ");
					String[] time = timeSplit[1].split(":");
					int reportedSec = (Integer.parseInt(time[0]) * 60 * 60)
							+ (Integer.parseInt(time[1]) * 60)
							+ (Integer.parseInt(time[2]));
					logger.log(Level.SEVERE, "reportedMin:::::::::::::"
							+ reportedMin);
					reportedMin = (reportedSec / 60);
					if(service.getAddress() != null){
						if(service.getAddress().getLatitude() == null || service.getAddress().getLatitude().equals("")){
							String lat1 = "";
							try{
								lat1 = service.getTrackServiceTabledetails().get(i).getLatitude();
							}catch(Exception e){
								
							}
							service.getAddress().setLatitude(lat1);
						}
						if(service.getAddress().getLongitude() == null || service.getAddress().getLongitude().equals("")){
							String long1 = "";
							try{
								long1 = service.getTrackServiceTabledetails().get(i).getLongitude();
							}catch(Exception e){
								
							}
							service.getAddress().setLongitude(long1);
						}
						}
				}

				if (Service.SERVICETCOMPLETED.equalsIgnoreCase(service
						.getTrackServiceTabledetails().get(i).getStatus()
						.trim())) {
					String[] timeSplit = service.getTrackServiceTabledetails()
							.get(i).getDate_time().split(" ");
					String[] time = timeSplit[1].split(":");
					int completedSec = (Integer.parseInt(time[0]) * 60 * 60)
							+ (Integer.parseInt(time[1]) * 60)
							+ (Integer.parseInt(time[2]));
					completedMin = (completedSec / 60);
					logger.log(Level.SEVERE, "completedMin:::::::::::::"
							+ completedMin);
				}
				/** date 23.03.2019 added by komal 
				 * purpose : to calculate service duration = completed - started **/
				if (Service.SERVICESTATUSSTARTED.equalsIgnoreCase(service
						.getTrackServiceTabledetails().get(i).getStatus()
						.trim())) {
					try {
						String[] timeSplit = service.getTrackServiceTabledetails()
								.get(i).getDate_time().split(" ");
						String[] time = timeSplit[1].split(":");
						int startedSec = (Integer.parseInt(time[0]) * 60 * 60)
								+ (Integer.parseInt(time[1]) * 60)
								+ (Integer.parseInt(time[2]));
						logger.log(Level.SEVERE, "reportedMin:::::::::::::"
								+ reportedMin);
						startedMin = (startedSec / 60);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
			}
			service.setServiceCompleteDuration(completedMin - reportedMin);
			/** date 23.03.2019 added by komal 
			 * purpose : to calculate service duration = completed - started **/
			service.setTotalServiceDuration(completedMin - startedMin);
		//	service.setStatus(Service.SERVICESTATUSTECHCOMPLETED);
			/*** date 5.3.2019 added by komal for nbhc to set status as completed**/
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MarkServiceCompletedFromApp", Long.parseLong(companyId))){
				service.setStatus(Service.SERVICESTATUSCOMPLETED);
			}else{				
				service.setStatus(Service.SERVICESTATUSTECHCOMPLETED);
			}
			service.setCompletedByApp(true);
			service.setToTime(format.format(new Date()));

			logger.log(Level.SEVERE, "Stage 6");
//			if (syncFlag) {
//				// Updating Reported Time and Date
//				TrackTableDetails tracktabledetails = new TrackTableDetails();
//				logger.log(Level.SEVERE, "Stage::::::::2");
//
//				SimpleDateFormat sdf1 = new SimpleDateFormat(
//						"dd/MM/yyyy HH:mm:ss");
//				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
//				sdf1.setTimeZone(TimeZone.getTimeZone("IST"));
//
//				JSONObject reportedObj = new JSONObject(reportedObject);
//
//				tracktabledetails.setDate_time(sdf.format(reportedObj
//						.opt("reportedDate").toString().trim()
//						+ " "
//						+ reportedObj.opt("reportedTime").toString().trim())
//						+ "");
//
//				logger.log(Level.SEVERE, "Stage::::::::3");
//				tracktabledetails.setStatus(Service.SERVICESTATUSREPORTED);
//				logger.log(Level.SEVERE, "Stage::::::::4");
//
//				// rahul added this for null pointer exp in table
//				// if(service.getTrackServiceTabledetails()!=null){
//				tracktabledetails.setSrNo(service.getTrackServiceTabledetails()
//						.size() + 1);
//				// }
//				// else
//				// {
//				// tracktabledetails.setSrNo(1);
//				// }
//				service.getTrackServiceTabledetails().add(tracktabledetails);
//				logger.log(Level.SEVERE, "Stage::::::::5"
//						+ service.getTrackServiceTabledetails().size());
//
//				// Reschedule
//				JSONObject rescheduleHistory = new JSONObject(rescheduleObject);
//				ModificationHistory modHistory = new ModificationHistory();
//				modHistory.setOldServiceDate(sdf.parse(rescheduleHistory
//						.optString("previousServiceDate")));
//				modHistory.setResheduleDate(sdf.parse(rescheduleHistory
//						.optString("rescheduleDate")));
//
//				// SimpleDateFormat sdf1=new
//				// SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
//				// sdf1.setTimeZone(TimeZone.getTimeZone("IST"));
//				modHistory.setSystemDate(new Date());
//				String[] time = rescheduleHistory.optString("rescheduleTime")
//						.trim().split(":");
//				int hours = Integer.parseInt(time[0]);
//				int mins = Integer.parseInt(time[1].substring(0, 2));
//				if (hours >= 12) {
//					int hoursDecrease = 0;
//					if (hours != 12) {
//						hoursDecrease = 12;
//					}
//					if (mins < 10) {
//						modHistory.setResheduleTime((hours - hoursDecrease)
//								+ ":" + "0" + mins + " PM");
//					} else {
//						modHistory.setResheduleTime((hours - hoursDecrease)
//								+ ":" + mins + " PM");
//					}
//
//					logger.log(Level.SEVERE, "Service Time::::::"
//							+ (hours - hoursDecrease) + ":" + mins + " PM");
//
//				} else {
//					logger.log(Level.SEVERE, "Service Time::::::" + hours + ":"
//							+ mins + " AM");
//					modHistory.setResheduleTime(hours + ":" + mins + " AM");
//				}
//				modHistory.setReason(rescheduleHistory
//						.optString("rescheduledReason"));
//
//				// List<ModificationHistory> modList=new
//				// ArrayList<ModificationHistory>();
//				// modList.add(modHistory);
//				// ModificationHistoryTable table = new
//				// ModificationHistoryTable();
//				// table.getDataprovider().getList().add(modHistory);
//				service.getListHistory().add(modHistory);
//
//			}
			/********************* Added Rate Contract Flag Logic *********************/
			ServiceImplementor serviceImpl = new ServiceImplementor();
			/** date 06.03.2019 commented by komal as it is working only for rate contract billing**/
//			if (isRateContractService) {
//			//	ServiceImplementor serviceImpl = new ServiceImplementor();
//				serviceImpl.updateRateContractBilling(service,
//						isRateContractService);
//			}
//			
			service.setCustomerSignName(customerNameOfSignature);
			service.setCustomerSignNumber(customerMobileOfSignature);
			service.setSrCopyNumber(srNumber+"-"+service.getEmployee());
			serviceEngineer = service.getEmployee();
			try{
				branchEmailId = req.getParameter("customerBranchEmail");
				logger.log(Level.SEVERE, "branch email : "+ branchEmailId);
			}catch(Exception e){
				logger.log(Level.SEVERE, "branch email : "+ branchEmailId);
				branchEmailId = "";
			}
			/**
			 * Date 06-01-2019 by Vijay for NBHC CCPM
			 * Des :-to capture Sign Person Employee Code and Sign Person Designation
			 */
			if(obj.optString("person_code")!=null){
				String personCode  = obj.optString("person_code");
				service.setCustomerPersonEmpId(personCode);
			}
			if(obj.optString("person_designation")!=null){
				String personDesignation  = obj.optString("person_designation");
				service.setCustomerPersonDesignation(personDesignation);
			}
			/**
			 * ends here
			 */
			
			try {
				if(obj.optString("OTP")!=null &&!obj.optString("OTP").equals("") ){
					String serviccecompletionOTP = obj.optString("OTP");
					service.setServiceCompletionOTP(serviccecompletionOTP);
				}
			} catch (Exception e) {
			}
			
			
			service.setBranchEmail(branchEmailId);
			serObject = service;
			ofy().save().entity(service).now();
			
			logger.log(Level.SEVERE, "SERVICE UPDATED : "+ new Date());
			/**
			 * @author Vijay Chougule
			 * project :- Fumigation Tracker
			 * Des :- when wms (fumigation) services mark completed then updating its status in fumigationReportEntity
			 */
			if(service.isWmsServiceFlag()){
				logger.log(Level.SEVERE, "Updating fumigation report entity when service is marked completed");
				UpdateServiceCompletetionInFumigationServiceReport(service);
			}
			
			/** date 09/05/2018 added by komal  for call log completion**/
			if(service.isCallwiseService()){
				CustomerLogDetails customerLog = ofy().load().type(CustomerLogDetails.class).filter("companyId", Long.parseLong(companyId)).filter("count", service.getContractCount()).first().now();		
				customerLog.setStatus(CustomerLogDetails.CLOSED);
				/** date 6.7.2018 added by komal for closing date and resolution**/
				customerLog.setClosingDate(new Date());
				if(service.getTechnicianRemark()!=null){
					customerLog.setResolution(service.getTechnicianRemark());
				}
				/**
				 * end komal
				 */
				ofy().save().entity(customerLog);
			}
			else{
				
				/**
				 * @author Anil , Date : 09-07-2019
				 * for temporary purpose me and amit(android) is commenting this code
				 */
				/** date 5.3.2018 added by komal to create or update billings(for servicewise billing and rate contract service billing**/
				serviceImpl.saveStatus(service);			
			}
			/**
			 * end komal
			 */
			
			try {
				sendSMStoCustomer(service);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			
			/**
			 * @author Anil , DAte : 09-07-2019
			 */
			
			
			
//			/** date 03.06.2019 added by komal to save lat and long in customer and customer branches**/
//			String taskName="updateServiceAddressLatLong"+"$"+companyId+"$"+service.getCount();
//			Queue queue = QueueFactory.getQueue("documentCancellation-queue");
//			queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));
			
			/** date 16/07/2018 added by komal to send email**/
			
			/**
			 * @author Anil @since 27-04-2021
			 * As per directed by Nitin sir and Ashwini, we should not send mail to company if rescheduled from pedio app
			 * raised by Ashwini for Pecopp
			 */
//			if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "SendSRCopyOnServiceCompletion", service.getCompanyId())){
//				sendEmail(service);
//			}
			
			// Added by Apeksha for sending notification and sms to customerr
			// after service completed successfully.
			try {
				// sendMessageToCustomer(service);
				CustomerUser customerUser = ofy()
						.load()
						.type(CustomerUser.class)
						.filter("companyId", Long.parseLong(companyId))
						.filter("cinfo.count",
								service.getPersonInfo().getCount()).first()
						.now();
				String response = "Service Completed Successfully.";
				try{
				SmsServiceImpl smsimpl = new SmsServiceImpl();
				sendSmsToClientFromMob(smsimpl, service, sdf);
				}catch(Exception e){
					logger.log(Level.SEVERE, "");
				}
//				sendPushNotification(Long.parseLong(companyId),
//						AppConstants.IAndroid.EVA_KRETO, customerUser
//								.getUserName().trim(), response,service,comp);
				}catch(Exception e){
					logger.log(Level.SEVERE,"SMS Error or Push Notification");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
				
				
				
			}
			
			/**
			 * @author Anil , Date : 13-03-2020
			 * As discussed with Nitin Sir , commenting lat long api
			 */
			
//			/** date 03.06.2019 added by komal to save lat and long in customer and customer branches**/
//			String taskName1="updateServiceAddressLatLong"+"$"+companyId+"$"+serObject.getCount();
////			Queue queue1 = QueueFactory.getQueue("documentCancellation-queue");
//			Queue queue1 = QueueFactory.getQueue("SeviceaddressLatLong-queue");
//			queue1.add(TaskOptions.Builder.withUrl("/slick_erp/UpdateServiceAddressLatLongQueue").param("taskKeyAndValue", taskName1));
			
			/**
			 * @author Anil , Date : 09-03-2020
			 * Commented this , mail will be sent through cron job
			 */
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "SendSRCopyOnServiceCompletion", comp.getCompanyId())){
//				Email obj1=new Email();
//				//sendEmailtoCustomer(serObject , srNumber+"-"+serviceEngineer , obj1);
//				String branchEmailId = null;
//				try{
//					branchEmailId = req.getParameter("customerBranchEmail");
//				}catch(Exception e){
//					
//				}
				logger.log(Level.SEVERE, "Delaying queue by 3 min" );
				String serviceid = getServiceId(serviceidlist);
				logger.log(Level.SEVERE, "serviceid "+serviceid );

				String taskName="SendSRCopyEmail"+"$"+serObject.getCompanyId()+"$"+serObject.getCount()+"$"+srNumber+"$"+branchEmailId;
//				Queue queue = QueueFactory.getQueue("documentCancellation-queue");
				Queue queue = QueueFactory.getQueue("SRCopyEmailQueueRevised-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/SRCopyEmailTaskQueue").param("taskKeyAndValue", taskName)
						.param("serviceid", serviceid).etaMillis(System.currentTimeMillis()+240000));
			}
			
			
		
			
			
		} catch (Exception e) {
			e.printStackTrace();
			exep = e;
			resp.getWriter().println("Failed");
			logger.log(Level.SEVERE, "Error::::::::" + e);
		}

		if (exep == null) {
			resp.getWriter().println("Success");
		}
	}

	

	

	private String getServiceId(ArrayList<String> serviceidlist) {
		StringBuffer sbserviceId = new StringBuffer();
		for(String strserviceId : serviceidlist) {
			sbserviceId.append(strserviceId);
			sbserviceId.append("$");
		}
		return sbserviceId.toString();
	}

	private String sendSmsToClientFromMob(SmsServiceImpl smsimpl,
			Service service, SimpleDateFormat sdf) {

		logger.log(Level.SEVERE, "Inside sendSmsToClientFromMob SMS send");

		SmsConfiguration smsConfig = ofy().load().type(SmsConfiguration.class)
				.filter("companyId", comp.getCompanyId()).first().now();

		if (smsConfig != null) {
			if (smsConfig.getStatus() == false) {
				return "SMS Config Not Active";
			}
			String senderId = smsConfig.getAccountSID();
			String userName = smsConfig.getAuthToken();
			String password = smsConfig.getPassword();

			SmsTemplate smsTemplate = ofy().load().type(SmsTemplate.class)
					.filter("companyId", comp.getCompanyId())
					.filter("event", "Service Completion").first().now();
			if (smsTemplate == null) {
				return "SMS Template not found!";
			}
			if (smsTemplate != null) {
				if (smsTemplate.getStatus() == false) {
					return "SMS Template not Active!!";
				} else {

					String templateMsgwithbraces = smsTemplate.getMessage();
					System.out.println("SMS===" + templateMsgwithbraces); 
					String cutomerName = templateMsgwithbraces.replace(
							"{CustomerName}", service.getPersonInfo()
									.getFullName());
					String productName = cutomerName.replace("{ProductName}",
							service.getProductName());
					String serviceDate = productName.replace("{ServiceDate}",
							sdf.format(service.getServiceDate()));
					/** date 20.12.2018 added by komal to send service number through sms when 
					 * service is completed from app
					 */
					String serviceNo = serviceDate.replace("{ServiceNo}", service.getServiceSrNo()+"");
					String companyName = comp.getBusinessUnitName();
					String actualMsg = serviceNo.replace("{companyName}",
							companyName);
					System.out.println("actaulMsg" + actualMsg);
					logger.log(Level.SEVERE,
							"Calling SMS API method to send SMS");
					int value = 1;
					//smsimpl.sendSmsToClient(actualMsg, service
//							.getPersonInfo().getCellNumber(), senderId,
//							userName, password, false);
					System.out.println(" Valuee===" + value);
					if (value == 1) {
						return "SMS sent successfully!!";
					}

				}

			}

		}

		return "Failed to send sms!!";

	}

	/*
	 * Date: 31 Jul 2017 By: Apeksha Gunjal send Message to Client when Service
	 * is completed successfully.
	 */

	private void sendMessageToCustomer(Service service) {
		// TODO Auto-generated method stub
		SmsServiceImpl smsServiceImpl = new SmsServiceImpl();
		smsServiceImpl.sendSmsToCustomers(null, null, null, null, null, false);
	}

	/*
	 * Date: 31 Jul 2017 By: Apeksha Gunjal send Push Notification to Client
	 * when Service is completed successfully.
	 */

//	private void sendPushNotification(long companyId, String applicationName,
//			String userName, String response, Service service, Company comp2) {
//		response="event:Mark Completed"+"$"+service.getProductName()+" service has been completed by "+service.getEmployee()+". Would you like to rate?";//getJSONResponse(service);
//		CustomerUser customerUser=ofy().load().type(CustomerUser.class).filter("companyId",companyId).filter("cinfo.fullName",service.getPersonInfo().getFullName()).first().now(); 
//		RegisterDevice device = ofy().load().type(RegisterDevice.class)
//				.filter("companyId", companyId)
//				.filter("applicationName", applicationName)
//				.filter("userName", customerUser.getUserName()).first().now();
//		String deviceName = device.getRegId().trim();
//		logger.log(Level.SEVERE,"deviceName"+deviceName);
//		String url="http://my.evaesserp.appspot.com/slick_erp/notificationHandler?deviceName="+deviceName+"&"+"companyId="+companyId+"&applicationName="+applicationName;
//		
//		
//		
//		URL url = null;
//		try {
//			url = new URL("https://fcm.googleapis.com/fcm/send");
//		} catch (MalformedURLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		HttpURLConnection conn = null;
//		try {
//			conn = (HttpURLConnection) url.openConnection();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		conn.setDoOutput(true);
//		try {
//			conn.setRequestMethod("POST");
//		} catch (ProtocolException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		conn.setRequestProperty("Content-Type",
//				"application/json; charset=UTF-8");
//		conn.setRequestProperty("Authorization", "key="
//				+ comp2.getFcmServerKeyForKreto().trim());
//		conn.setDoOutput(true);
//		conn.setDoInput(true);
//
//		try {
//			conn.setRequestMethod("POST");
//		} catch (ProtocolException e) {
//			// TODO Auto-generated catch block
//			logger.log(Level.SEVERE, "ProtocolException ERROR::::::::::" + e);
//			e.printStackTrace();
//		}
//		OutputStream os = null;
//		try {
//			os = conn.getOutputStream();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
//			e.printStackTrace();
//		}
//		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
//		// Log.d("Test", "From Get Post Method" + getPostData(values));
//		try {
//			writer.write(createJSONData(response, deviceName));
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			logger.log(Level.SEVERE, "IOException2 ERROR::::::::::" + e);
//			e.printStackTrace();
//		}
//		try {
//			writer.flush();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			logger.log(Level.SEVERE, "IOException3 ERROR::::::::::" + e);
//			e.printStackTrace();
//		}
//
//		InputStream is = null;
//		try {
//			is = conn.getInputStream();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
//			e.printStackTrace();
//		}
//		BufferedReader br = new BufferedReader(new InputStreamReader(is));
//		String temp;
//		String data = null;
//		try {
//			while ((temp = br.readLine()) != null) {
//				data = data + temp;
//			}
//			logger.log(Level.SEVERE, "data data::::::::::" + data);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
//			e.printStackTrace();
//		}
//
//		logger.log(Level.SEVERE, "Data::::::::::" + data);
//	}

	private String getJSONResponse(Service service) {
		// TODO Auto-generated method stub
		JSONObject obj=new JSONObject();
		try {
			obj.put("status",1);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			obj.put("message",service.getProductName()+" service has been completed by "+service.getEmployee()+". Would you like to rate?");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return obj.toString();
	}

	private String createJSONData(String message, String deviceName) {
		// TODO Auto-generated method stub
		org.json.simple.JSONObject jObj1 = new org.json.simple.JSONObject();
		jObj1.put("message", message);

		org.json.simple.JSONObject jObj = new org.json.simple.JSONObject();
		jObj.put("data", jObj1);
		jObj.put("to", deviceName);

		String jsonString = jObj.toJSONString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE, "JSON Data Sent:::::::" + jsonString);

		return jsonString;
	}
	/** date 16.7.2018 added by komal**/
	public void sendEmail(Service service){
		
		String fromEmail="";
		Company comp=ofy().load().type(Company.class).filter("companyId", service.getCompanyId()).first().now();
		logger.log(Level.SEVERE,"customer --"+comp.getEmail());
		ArrayList<String> toEmailList = new ArrayList<String>();
		/**
		 * @author Anil @since 26-03-2021
		 * Commenting below code is cuases exception while completing service from app
		 * raised by Ashwini for Pecop
		 */
//		toEmailList.add(comp.getEmail());
		if(comp!=null){
			if(comp.getEmail()!=null){
				fromEmail=comp.getEmail();
			}
			
			/**
			 * @author Anil @since 26-03-2021
			 * to mail address to be customer branch email or customer email
			 */
			if(!service.getServiceBranch().equalsIgnoreCase("Service Address")){
				CustomerBranchDetails details = ofy().load().type(CustomerBranchDetails.class).filter("companyId", service.getCompanyId()).filter("buisnessUnitName", service.getServiceBranch()).first().now();
				if(details != null && details.getEmail() != null && !details.getEmail().equals("")){
					toEmailList.add(details.getEmail());
				}
			}
			
			String customerName="";
			Customer cust = ofy().load().type(Customer.class).filter("companyId", service.getCompanyId()).filter("count", service.getPersonInfo().getCount()).first().now();
			if(cust != null && cust.getEmail() != null){
				toEmailList.add(cust.getEmail());
				if(cust.isCompany()){
					customerName=cust.getCompanyName();
				}else{
					customerName=cust.getFullname();
				}
			}
			
			
		
		String techName  ="";
		if(service.getEmployee()!=null){
			techName= service.getEmployee();
		}
		String mailSub=""; //thank you for reaching out to us. you have enquired about product name
		String mailMsg="";
			mailSub="Service Completion";
			mailMsg="Dear Sir/Madam"+","+"\n"+"\n"+"\n"
							+"Service : "+service.getCount() +" has been completed by "+techName +"\n"+
							"with resolution "+ service.getTechnicianRemark()+" \n"+"\n"+"\n";
						
		
		Email obj=new Email();
		obj.sendEmail(mailSub, mailMsg, toEmailList, new ArrayList<String>(), fromEmail);
		logger.log(Level.SEVERE,"EMAIL Sending --"+comp.getEmail());
		
//		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "SendSRCopyOnServiceCompletion", service.getCompanyId())){
//			sendEmailtoCustomer(service , obj);
//		}
		}
	}	
	 
	public void sendEmailtoCustomer(List<Service> servicelist ,String srNumber , Email obj, String customerBranchEmail){
		
		Service serviceObj = servicelist.get(0);
		logger.log(Level.SEVERE,"in sendEmailtoCustomer service id="+serviceObj.getCount());
		ArrayList<String> toEmailList = new ArrayList<String>();
//		try {
//			Thread.sleep(180000);
//		} catch (InterruptedException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		
		String branchEmail = "";
		Branch branch = null ;
		Company comp = ofy().load().type(Company.class).filter("companyId", serviceObj.getCompanyId()).first().now();
		if(serviceObj.getBranch() != null){
			branch = ofy().load().type(Branch.class).filter("companyId", serviceObj.getCompanyId()).filter("buisnessUnitName", serviceObj.getBranch()).first().now();
		}
		if(!serviceObj.getServiceBranch().equalsIgnoreCase("Service Address")){
			CustomerBranchDetails details = ofy().load().type(CustomerBranchDetails.class).filter("companyId", serviceObj.getCompanyId()).filter("buisnessUnitName", serviceObj.getServiceBranch()).first().now();
			
			if(details != null){
				details.setEmail(customerBranchEmail);
				ofy().save().entity(details);
			}
		}
		if(branch != null && branch.getEmail() != null && !branch.getEmail().equals("")){
			
			branchEmail = branch.getEmail();
			logger.log(Level.SEVERE,"branchEmail="+branchEmail);
			
//			toEmailList.add(branchEmail); //Commented by Ashwini Patil as send grid is giving error if same email id present in from, to and bcc
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
				if(comp.getEmail()!=null&&!comp.getEmail().equals(""))
					toEmailList.add(comp.getEmail());
			}else {
				toEmailList.add(branchEmail); //Ashwini Patil Date:9-09-2024 ankita pest want to receive sr copy on branch email as we had this feature before.
			}
			if(customerBranchEmail != null && !customerBranchEmail.equalsIgnoreCase("null")){
				toEmailList.add(customerBranchEmail);
			}
		}
		
		/**
		 * @author Anil , Date : 09-01-2019
		 * After updating customer name, sometime customer name not updated in service hence in mail subject old name got send
		 */
		String customerName="";
		Customer cust = ofy().load().type(Customer.class).filter("companyId", serviceObj.getCompanyId()).filter("count", serviceObj.getPersonInfo().getCount()).first().now();
		if(cust != null && cust.getEmail() != null){
			toEmailList.add(cust.getEmail());
			if(cust.isCompany()){
				customerName=cust.getCompanyName();
			}else{
				customerName=cust.getFullname();
			}
		}
		
		if(cust!=null&&ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Company", "SendAutoEmailToAllContactsOfCustomer", cust.getCompanyId())) {
				
			/*
			 * Ashwini Patil
			 * Date:9-07-2024
			 * Ultra pest requirement - send payment reminder to all contacts of the customer
			 */
			try {
			List<ContactPersonIdentification> contactList= ofy().load().type(ContactPersonIdentification.class).filter("companyId", cust.getCompanyId())
					.filter("personInfo.count", cust.getCount()).list();
			if(contactList!=null&&contactList.size()>0) {
				for(ContactPersonIdentification contact:contactList) {
					if(contact.getEmail()!=null&&!contact.getEmail().equals("")&&!toEmailList.contains(contact.getEmail())) {
						toEmailList.add(contact.getEmail());
					}
					if(contact.getEmail1()!=null&&!contact.getEmail1().equals("")&&!toEmailList.contains(contact.getEmail1())) {
						toEmailList.add(contact.getEmail1());
					}
				}
				
			}
			}catch(Exception e) {
				
			}
		}
		
		
		
		/**
		 * @author Anil , Date : 05-03-2020
		 * Right now Pestinct is send in all mail for all client
		 * it should be only for NBHC
		 */
		String compName="SR - ";
		String compEmail=comp.getEmail();
		if(comp!=null&&comp.getBusinessUnitName()!=null){
			if(comp.getBusinessUnitName().contains("NBHC")){
				compName="Pestinct SR - ";
				compEmail="pestinct@nbhcindia.com";
			}
		}
		
		//Ashwini Patil Date:30-01-2022
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			
			logger.log(Level.SEVERE,"BranchAsCompany Process active --");
			if(branch != null && branch.getEmail() != null && !branch.getEmail().equals(""))
				compEmail=branch.getEmail();
		}		
		/**
		 * @author Vijay Date :- 12-11-2021 
		 * Des :- if zhoho feedback link configured then feedback link will share along with email
		 */
		String feedbackFormUrl="";
		String onlyfeedbackLink="";

		try {
			
		Company companyEntity = ofy().load().type(Company.class).filter("companyId", serviceObj.getCompanyId()).first().now();
		if(companyEntity!=null&&companyEntity.getFeedbackUrlList()!=null&&companyEntity.getFeedbackUrlList().size()!=0){
//			Customer customer=ofy().load().type(Customer.class).filter("companyId",serviceObj.getCompanyId()).filter("count",serviceObj.getPersonInfo().getCount()).first().now();
//			if(customer!=null){
//				if(customer.getCategory()!=null&&!customer.getCategory().equals("")){
					for(FeedbackUrlAsPerCustomerCategory feedback:companyEntity.getFeedbackUrlList()){
//						if(feedback.getCustomerCategory().equals(customer.getCategory())){
							feedbackFormUrl=feedback.getFeedbackUrl();
							break;
//						}
					}
//				}
//			}
			
		String serDate = dateFormat.format(serviceObj.getServiceDate());
		logger.log(Level.SEVERE, "ZOHO FEEDBACK URL feedbackFormUrl " + feedbackFormUrl);

		if(feedbackFormUrl!=null&&!feedbackFormUrl.equals("")){
			logger.log(Level.SEVERE, "ZOHO FEEDBACK URL " + feedbackFormUrl);
			StringBuilder sbPostData= new StringBuilder(feedbackFormUrl);
	        sbPostData.append("?customerId="+serviceObj.getPersonInfo().getCount());
	        sbPostData.append("&customerName="+serviceObj.getPersonInfo().getFullName());
	        sbPostData.append("&serviceId="+serviceObj.getCount());
	        sbPostData.append("&serviceDate="+serDate);
	        sbPostData.append("&serviceName="+serviceObj.getProductName());
	        sbPostData.append("&technicianName="+serviceObj.getEmployee());
	        sbPostData.append("&branch="+serviceObj.getBranch());
	        feedbackFormUrl = sbPostData.toString();
	        feedbackFormUrl=feedbackFormUrl.replaceAll("\\s", "%20");
	        feedbackFormUrl=feedbackFormUrl.replaceAll("&", "%26");

	        feedbackFormUrl=ServerAppUtility.getTinyUrl(feedbackFormUrl,companyEntity.getCompanyId());
	        
	        logger.log(Level.SEVERE, "FINAL ZOHO FEEDBACK URL " + feedbackFormUrl);
	        onlyfeedbackLink = "<a href=\""+feedbackFormUrl+"\" >"+feedbackFormUrl+"</a>";
	        logger.log(Level.SEVERE, "onlyfeedbackLink="+onlyfeedbackLink);
	        feedbackFormUrl =" Please share your feedback by clicking on this link - "+"<a href=\""+feedbackFormUrl+"\" >"+feedbackFormUrl+"</a>";
	        logger.log(Level.SEVERE, "FINAL ZOHO FEEDBACK URL feedbackFormUrl wtih msg" + feedbackFormUrl);

		}
	  }
	
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		/**
		 * ends here
		 */
		
//		String mailSub = "Pestinct SR - "+serviceObj.getPersonInfo().getFullName()+" "+serviceObj.getCount();
//		String mailSub = "Pestinct SR - "+customerName+" "+serviceObj.getCount();
		

		
		String mailMsg = "Dear Customer"+",<br></br><br></br><br></br>"
				+"Please find the service record of our pest management carried out at your premises."+"<br></br>"+ " For any queries contact us at: "+compEmail+"<br></br>"+"<br></br>"+"<br></br>";

		String mailSub = "SR "+compName+customerName+" "+serviceObj.getCount();
		if(feedbackFormUrl!=null && !feedbackFormUrl.equals("")){
			mailMsg = "Dear Customer"+",<br></br><br></br><br></br>"
					+"Please find the service record of our pest management carried out at your premises."+feedbackFormUrl+"<br></br>"+ " For any queries contact us at: "+compEmail+"<br></br>"+"<br></br>"+"<br></br>";

		}
		
		String[] fileName = { "SR Copy -" +serviceObj.getCount() + ".pdf"};
		obj.createSRCopyttachment(servicelist);
				
		
		EmailTemplateConfiguration emailtemplateconfig = ofy().load().type(EmailTemplateConfiguration.class)
				.filter("companyId", serviceObj.getCompanyId()).filter("moduleName", "Service").filter("documentName", "Customer Service")
				.filter("communicationChannel", "Email").filter("templateName", AppConstants.SR_SRCOPYEMAILTEMPLATE)
				.filter("status", true).first().now();
		
		EmailTemplate emailTemplate = ofy().load().type(EmailTemplate.class).filter("templateName",AppConstants.SR_SRCOPYEMAILTEMPLATE)
				.filter("templateStatus", true).first().now();
		if(emailtemplateconfig!=null &&emailTemplate!=null){
			 logger.log(Level.SEVERE, "emailtemplateconfig!=null");
			
//			if(emailTemplate!=null){
				 logger.log(Level.SEVERE, "emailTemplate!=null");
				String emailMsg = emailTemplate.getEmailBody();
				if(emailTemplate.getEmailBody().contains("{Feedback_Link}")){
					emailMsg = emailMsg.replace("{Feedback_Link}", onlyfeedbackLink);
				}else{
					emailMsg += "<br><br> "+feedbackFormUrl;
				}
				if(emailTemplate.getEmailBody().contains("{CustomerName}")){
					emailMsg = emailMsg.replace("{CustomerName}", serviceObj.getPersonInfo().getFullName());
				}
				if(emailTemplate.getEmailBody().contains("{ProductName}")){
					emailMsg = emailMsg.replace("{ProductName}", serviceObj.getProductName());
				}
				if(emailTemplate.getEmailBody().contains("{TechnicianName}")){
					emailMsg = emailMsg.replace("{TechnicianName}", serviceObj.getEmployee());
				}
				if(emailTemplate.getEmailBody().contains("{CompletionDate}")){
					if(serviceObj.getCompletedDate()!=null)
						emailMsg = emailMsg.replace("{CompletionDate}", dateFormat.format(serviceObj.getCompletedDate()));
					else if(serviceObj.getServiceCompletionDate()!=null)
						emailMsg = emailMsg.replace("{CompletionDate}", dateFormat.format(serviceObj.getServiceCompletionDate()));
					else
						emailMsg = emailMsg.replace("{CompletionDate}", "--/--/--");
					
				}
				if(emailTemplate.getEmailBody().contains("{CompletionTime}")){
					if(serviceObj.getCompletedTime()!=null)
						emailMsg = emailMsg.replace("{CompletionTime}", serviceObj.getCompletedTime());
					else
						emailMsg = emailMsg.replace("{CompletionTime}", "--:--");
						
				}
				
				if(emailTemplate.getEmailBody().contains("{Company Signature}")){
					emailMsg = emailMsg.replace("{Company Signature}", ServerAppUtility.getCompanySignature(comp,null));
				}
				if(emailTemplate.getEmailBody().contains("{DocumentId}"))
					emailMsg = emailMsg.replace("{DocumentId}", serviceObj.getCount()+"");
				
				emailMsg = emailMsg.replaceAll("[\n]", "<br>");
				emailMsg = emailMsg.replaceAll("[\\s]", "&nbsp;");
				
				mailMsg = emailMsg;
				mailSub=emailTemplate.getSubject();
				if(mailSub.contains("{DocumentId}"))
					mailSub= mailSub.replace("{DocumentId}", serviceObj.getCount()+"");
				try {	
					if(toEmailList.size() > 0){
						/**
						 * @author Anil @since 13-08-2021
						 * Passed company email as parameter to method
						 */
						logger.log(Level.SEVERE,"company --"+ compEmail);
						obj.sendNewEmailFromEmailTemplate(toEmailList, mailSub, comp, mailMsg,fileName, null,null,compEmail);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
//			}
		}else {
			try {	
				if(toEmailList.size() > 0){
					/**
					 * @author Anil @since 13-08-2021
					 * Passed company email as parameter to method
					 */
					logger.log(Level.SEVERE,"company --"+ compEmail);
					obj.sendNewEmail(toEmailList, mailSub, "", 0, "", new Date(), comp,	
						null, null, null, null, null, null, mailMsg, null, null,fileName, null,null,compEmail);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	//	Service service = ofy().load().type(Service.class).filter("companyId", serviceObj.getCompanyId()).filter("count", serviceObj.getCount()).first().now();
		
		
		logger.log(Level.SEVERE,"SR Copy mailMsg "+ mailMsg);

		
		logger.log(Level.SEVERE,"customer --"+ cust.getEmail());
		
		
	
	}
	
	/**
	 * @author Vijay Chougule
	 * project :- Fumigation Tracker
	 * Des :- when wms (fumigation) services mark completed then updating its status in fumigationReportEntity
	 * if condition for When wms fumigation service is completed then its status and date updating in fumigationReportEntity.
	 * else condition for when wms prophylactic and degassing service is completed then its status and date updating in fumigationReportEntity
	 */
	public void UpdateServiceCompletetionInFumigationServiceReport(Service service) {
		logger.log(Level.SEVERE, "Service completion uodation in fumigation Report");
		if(service.getProduct().getProductCode().equals("STK-01")){
			FumigationServiceReport fumigationReport = ofy().load().type(FumigationServiceReport.class)
					.filter("companyId", service.getCompanyId()).filter("contractId", service.getContractCount())
					.filter("warehouseCode", service.getRefNo2()).filter("compartment", service.getCompartment())
					.filter("shade", service.getShade()).filter("fumigationServiceId", service.getCount()).first().now();
			if(fumigationReport!=null){
				logger.log(Level.SEVERE, "Fumigation Report id"+fumigationReport.getCount());
				fumigationReport.setFumigationServiceCompletionDate(service.getServiceCompletionDate());
				fumigationReport.setFumigationCompletionRemark(service.getServiceCompleteRemark());
				fumigationReport.setStackQuantity(service.getStackQty());
				fumigationReport.setFumigationTechnicianName(service.getEmployee());
				fumigationReport.setRescheduleCount(service.getListHistory().size());
				fumigationReport.setFumigationServiceStatus(service.getStatus());// service completion status updation in report
				ofy().save().entity(fumigationReport);
				logger.log(Level.SEVERE, "fumigationReport updated successfully");
			}
		}
		else{
			FumigationServiceReport fumigationReport = ofy().load().type(FumigationServiceReport.class)
					.filter("companyId", service.getCompanyId()).filter("contractId", service.getContractCount())
					.filter("warehouseCode", service.getRefNo2()).filter("compartment", service.getCompartment())
					.filter("shade", service.getShade()).filter("fumigationServiceId", service.getFumigationServiceId()).first().now();
			if(fumigationReport!=null){
				logger.log(Level.SEVERE, "Fumigation Report id"+fumigationReport.getCount());
				
				fumigationReport.setStackQuantity(service.getStackQty());
				if(service.getProduct().getProductCode().equals("PHM-01")){
					fumigationReport.setProphylacticStatus(Service.SERVICESTATUSCOMPLETED);
					fumigationReport.setProphylacticServiceCompletionDate(service.getServiceCompletionDate());
					fumigationReport.setProphylacticCompletionRemark(service.getServiceCompleteRemark());
					fumigationReport.setProphylacticTechnicianName(service.getEmployee());
					fumigationReport.setRescheduleCount(service.getListHistory().size());
				}
				else{
					fumigationReport.setDegassingStatus(Service.SERVICESTATUSCOMPLETED);
					fumigationReport.setDeggasingServiceCompletionDate(service.getServiceCompletionDate());
					fumigationReport.setDegassingCompletionRemark(service.getServiceCompleteRemark());
					fumigationReport.setDegassingTechnicianName(service.getEmployee());
					fumigationReport.setRescheduleCount(service.getListHistory().size());
				}
				ofy().save().entity(fumigationReport);
				logger.log(Level.SEVERE, "fumigationReport updated successfully");
			}
		}
	}
	
	private void sendSMStoCustomer(Service service) {

		try {
			
//		SmsConfiguration smsconfig = ofy().load().type(SmsConfiguration.class).filter("companyId", service.getCompanyId()).filter("status", true).first().now();
//		logger.log(Level.SEVERE, "smsconfig" + smsconfig);
//
//		if(smsconfig!=null){
//			
//			String accountSid = smsconfig.getAccountSID();
//			String authoToken = smsconfig.getAuthToken();
//			String password = smsconfig.getPassword();
			
			SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId",service.getCompanyId()).filter("event","Service Completion").filter("status",true).first().now();
			logger.log(Level.SEVERE, "smsEntity" + smsEntity);

			if(smsEntity!=null){
				
				/**
				 * @author Vijay Date :- 09-08-2021
				 * Des :- added below method to check customer DND status if customer DND status is Active 
				 * then SMS will not send to customer
				 */
				ServerAppUtility serverapputility = new ServerAppUtility();
				boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(service.getPersonInfo().getCount(), service.getCompanyId());
				if(!dndstatusFlag){
					
				String templateMsgWithBraces = smsEntity.getMessage();
				String custName = service.getPersonInfo().getFullName();
				String prodName = "";
				if(service.getProductName()!=null){
					prodName = service.getProductName();
				}
				String serNo = service.getServiceSerialNo()+"";
				final String serDate = ServerAppUtility.parseDate(service.getServiceDate());
				String companyName = "";
				
				Company companyEntity = ofy().load().type(Company.class).filter("companyId", service.getCompanyId()).first().now();
				if(companyEntity!=null){
					companyName = companyEntity.getBusinessUnitName();
				}

				String cutomerName = templateMsgWithBraces.replace("{CustomerName}", custName);
				String productName = cutomerName.replace("{ProductName}", prodName);
				String serviceNo = productName.replace("{ServiceNo}", serNo);
				String serviceDate =  serviceNo.replace("{ServiceDate}", serDate);
				String actualMsg = serviceDate.replace("{companyName}", companyName);
				
				
				/**
				 * @author Anil @since 09-03-2021
				 * loading feedback form as per the customer category
				 */
				if(companyEntity!=null&&companyEntity.getFeedbackUrlList()!=null&&companyEntity.getFeedbackUrlList().size()!=0){
					
					/**
					 * @author Anil @since 03-03-2021
					 * Adding feedback form url on service completion sms
					 * Raised by Nitin Sir for Pecopp
					 */
					String feedbackFormUrl="";
//					ProcessConfiguration processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", service.getCompanyId()).filter("processName", "ZohoFeedbackForm").filter("configStatus", true).first().now();
//					if(processConfig!=null){
//						for(ProcessTypeDetails obj:processConfig.getProcessList()){
//							if(obj.isStatus()==true){
//								feedbackFormUrl=obj.getProcessType();
//							}
//						}
//					}
					
					
					
					Customer customer=ofy().load().type(Customer.class).filter("companyId",service.getCompanyId()).filter("count",service.getPersonInfo().getCount()).first().now();
					if(customer!=null){
						if(customer.getCategory()!=null&&!customer.getCategory().equals("")){
							for(FeedbackUrlAsPerCustomerCategory feedback:companyEntity.getFeedbackUrlList()){
								if(feedback.getCustomerCategory().equals(customer.getCategory())){
									feedbackFormUrl=feedback.getFeedbackUrl();
									break;
								}
							}
						}
					}
					

					boolean customergroupHighFrequency = true;
					if(customer!=null){
							
						String customerGroup = customer.getGroup();
						if(customerGroup.trim().equals(AppConstants.HIGHFREQUENCYSERVICES)){
							Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", service.getCompanyId())
									.filter("count", service.getContractCount()).first().now();
								double frequency =0;
								for(SalesLineItem saleslineitem : contractEntity.getItems()){
									if(saleslineitem.getProductCode().trim().equals(service.getProduct().getProductCode().trim())){
										frequency = saleslineitem.getDuration()/saleslineitem.getNumberOfServices();
										break;
									}
								}
								logger.log(Level.SEVERE, "frequency" + frequency);
								logger.log(Level.SEVERE, "company.getMinimumDuration()" + companyEntity.getMinimumDuration());
					
								if(frequency<companyEntity.getMinimumDuration()){
									customergroupHighFrequency = false;
								}
						}
						
					}
					
					if(feedbackFormUrl!=null&&!feedbackFormUrl.equals("") && customergroupHighFrequency){
						logger.log(Level.SEVERE, "ZOHO FEEDBACK URL " + feedbackFormUrl);
						StringBuilder sbPostData= new StringBuilder(feedbackFormUrl);
				        sbPostData.append("?customerId="+service.getPersonInfo().getCount());
				        sbPostData.append("&customerName="+service.getPersonInfo().getFullName());
				        sbPostData.append("&serviceId="+service.getCount());
				        sbPostData.append("&serviceDate="+serDate);
				        sbPostData.append("&serviceName="+service.getProductName());
				        sbPostData.append("&technicianName="+service.getEmployee());
				        sbPostData.append("&branch="+service.getBranch());
				        feedbackFormUrl = sbPostData.toString();
				        feedbackFormUrl=feedbackFormUrl.replaceAll("\\s", "%20");
				        feedbackFormUrl=feedbackFormUrl.replaceAll("&", "%26");
				        
				        /**
				         * @author Anil @since 10-03-2021
				         * Modified feedback Url i.e. tiny utl
				         */
				        feedbackFormUrl=ServerAppUtility.getTinyUrl(feedbackFormUrl,companyEntity.getCompanyId());
				        
				        logger.log(Level.SEVERE, "FINAL ZOHO FEEDBACK URL " + feedbackFormUrl);
						actualMsg=actualMsg.replace("{Link}", feedbackFormUrl);
					}
					/**
					 * @author Anil @since 19-05-2021
					 * As per Nitin sir's instruction if no schduling URL found then 
					 * remove {Link} keyword from SMS template as well
					 */
					else{
//						actualMsg=actualMsg.replace("{Link}", "");
						actualMsg=actualMsg.replace(" Please share your feedback by clicking on this link {Link} -", "-");
						actualMsg=actualMsg.trim();
					}
				
				}
				
				/*
				 * Ashwini Patil
				 * Date:21-10-2024
				 * Ultra pest requested that sr copy url shall get sent along with service completion whatsapp msg
				 */
				if(actualMsg.contains("{PDFLink}")){
					CommonServiceImpl commonserviceimpl = new CommonServiceImpl();
					String pdflink = commonserviceimpl.getPDFURL("Service", service, service.getCount(), service.getCompanyId(), companyEntity, commonserviceimpl.getCompleteURL(companyEntity),null);
					logger.log(Level.SEVERE, "Service record pdflink " + pdflink);
					String tinyurl = ServerAppUtility.getTinyUrl(pdflink,companyEntity.getCompanyId());
					actualMsg =  actualMsg.replace("{PDFLink}", tinyurl);
				}
				
				long customercell = service.getPersonInfo().getCellNumber();

				SmsServiceImpl smsimpl = new SmsServiceImpl();
				logger.log(Level.SEVERE, "customercell" + customercell);
//				if(customercell!=0){
//					int value =  smsimpl.sendSmsToClient(actualMsg, customercell, accountSid,  authoToken,  password, service.getCompanyId());
//
//				}
				
				/**
				 * @author Vijay Date :- 29-04-2022
				 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
				 * whats and sms based on communication channel configuration.
				 */
				smsimpl.sendMessage(AppConstants.SERVICEMODULE,AppConstants.SERVICE,smsEntity.getEvent(),smsEntity.getCompanyId(),actualMsg,customercell,false);
				logger.log(Level.SEVERE,"after sendMessage method");
			}
			}
		
//		}
		
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	
	}
}
