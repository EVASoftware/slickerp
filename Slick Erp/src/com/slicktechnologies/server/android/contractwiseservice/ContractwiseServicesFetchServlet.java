package com.slicktechnologies.server.android.contractwiseservice;


import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.search.FromTerm;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.googlecode.objectify.ObjectifyService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.ExpenseManagement;
import com.slicktechnologies.client.views.device.CatchTraps;
import com.slicktechnologies.client.views.device.TrackTableDetails;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
//import com.slicktechnologies.shared.CheckList;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
/**
 * Description :
 *type = "schedule" : It is used to load all services on technician dashboard. The services of same contract
  , same technician , same date and same service branch are clubbed together and sent back to the application.
  When type = "history" then it returns completed services details of logged in technician.
   When type = "service" then it returns individual service details of given service id of logged in technician.
 * @author pc1
 *
 */
public class ContractwiseServicesFetchServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -938059768018970433L;
	/**
	 * 
	 */
	
	Logger logger = Logger.getLogger("ScheduleServiceFetchServlet.class");
	Company comp;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doGet(req, resp);
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// fo
		
//		resp.setHeader("Access-Control-Allow-Origin", AppConstants.TRENDZWEBSOLUTION);
		resp.setHeader("Access-Control-Allow-Origin", "*");
		List<Service> serviceList = new ArrayList<Service>();
		RegisterServiceImpl impl = new RegisterServiceImpl();
		impl.getClass();
		/**
		 * @author Anil @since 14-06-2021
		 * Thai character was not getting download in proper format
		 * raised by Rahul Tiwari
		 */
//		resp.setContentType("text/plain");
		resp.setContentType("text/plain; charset=UTF-8");
		resp.setCharacterEncoding("UTF-8");
		
		String urlCalled = req.getRequestURL().toString().trim();
	
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl::" + splitUrl);
		

		Exception exep = null;
		try {

			comp = ofy().load().type(Company.class)
					.filter("accessUrl", splitUrl[0]).first().now();
			logger.log(Level.SEVERE, "Called from Android");
			String companyId = comp.getCompanyId() + "";
			logger.log(Level.SEVERE, "companyId::::::::::" + companyId);
			String team = req.getParameter("team").trim();
			logger.log(Level.SEVERE, "team::::::::::" + team);
			String type = req.getParameter("type").trim();
			logger.log(Level.SEVERE, "type::::::::::" + type);
			String fromDate=req.getParameter("fromDate").trim();
			logger.log(Level.SEVERE, "fromDate::::::::::" + fromDate);
			String toDate=req.getParameter("toDate").trim();
			logger.log(Level.SEVERE, "toDate::::::::::" + toDate);
			String custId=null;
			try {
				custId = req.getParameter("customerId");
				
			}catch (Exception e) {
				
			}
			
			if(custId!=null)
				logger.log(Level.SEVERE, "customerId::::::::::" + custId);
			
			String custName=null;
			try {
				custName = req.getParameter("customerName");
				
			}catch (Exception e) {
				
			}
			if(custName!=null)
				logger.log(Level.SEVERE, "customerName::::::::::" + custName);
			
			String custBranch=null;
			try {
				custBranch = req.getParameter("customerBranch");
				
			}catch (Exception e) {
				
			}
			if(custBranch!=null)
				logger.log(Level.SEVERE, "customerBranch::::::::::" + custBranch);
			
			boolean isFromRefreshService=false;//Ashwini Patil Date:26-12-2023 to check whether api is called from service refresh
			try {
				isFromRefreshService=Boolean.parseBoolean(req.getParameter("isFromRefreshService"));
			}catch(Exception e) {
				logger.log(Level.SEVERE,"in isFromRefreshService exception");
			}
			logger.log(Level.SEVERE, team+" called "+type+" from Service Refresh="+isFromRefreshService);
					
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			calendar.add(Calendar.DAY_OF_YEAR, 1);
			Date afterDate = calendar.getTime();

			String aftrDateinString = sdf.format(afterDate);
			logger.log(Level.SEVERE, "Calendar afterDate:::::::" + afterDate);
			String date = sdf.format(new Date());
			logger.log(Level.SEVERE, "String new Date::::::::" + date);
			
			
			/**
			 * @author Anil @since 27-01-2021
			 * loading today's and day before today's date services for all clients
			 * Also loading in process services if the process configuration is active
			 * and loading the service as per the number of days configured on ERP
			 */
			Date newFromDate=null;
			Date applicableFromDate=null;
			Date applicableToDate=null;
			
			if(type.trim().equalsIgnoreCase("schedule")){
				Calendar cal1 = Calendar.getInstance();
				cal1.setTime(new Date(sdf.parse(fromDate).getTime()));
				cal1.add(Calendar.DAY_OF_YEAR, -1);
				newFromDate = cal1.getTime();
				
				int noOfDays=0;
				ProcessConfiguration processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", comp.getCompanyId()).filter("processName", "LoadInProcessServices").filter("configStatus", true).first().now();
				if(processConfig!=null){
					for(ProcessTypeDetails ptDetails:processConfig.getProcessList()){
						if(ptDetails.isStatus()==true){
							noOfDays=Integer.parseInt(ptDetails.getProcessType().trim());
						}
					}
				}
				logger.log(Level.SEVERE,"noOfDays: "+noOfDays);
				
				if(noOfDays!=0){
					Calendar cal2 = Calendar.getInstance();
					cal2.setTime(new Date(sdf.parse(fromDate).getTime()));
					cal2.add(Calendar.DAY_OF_YEAR, -noOfDays);
					applicableFromDate = cal2.getTime();
					
					Calendar cal3 = Calendar.getInstance();
					cal3.setTime(new Date(newFromDate.getTime()));
					cal3.add(Calendar.DAY_OF_YEAR, -1);
					cal3.set(Calendar.SECOND, 59);
					cal3.set(Calendar.MINUTE, 59);
					cal3.set(Calendar.HOUR, 23);
					applicableToDate = cal3.getTime();
				}
				logger.log(Level.SEVERE,"newFromDate: "+newFromDate);
				logger.log(Level.SEVERE,"applicableFromDate: "+applicableFromDate);
				logger.log(Level.SEVERE,"applicableToDate: "+applicableToDate);
			}
			
			List<String> statusList = new ArrayList<String>();
			int serviceId =0 ;
			if (type.trim().equalsIgnoreCase("history")) {
				statusList.add(Service.SERVICESTATUSCOMPLETED);
				statusList.add(Service.SERVICESTATUSTECHCOMPLETED);
			} else if (type.trim().equalsIgnoreCase("schedule")) {
				statusList.add(Service.SERVICESTATUSSCHEDULE);
				statusList.add(Service.SERVICESTATUSRESCHEDULE);
				statusList.add(Service.SERVICESTATUSREPORTED);
				statusList.add(Service.SERVICESTATUSSTARTED);
				statusList.add(Service.SERVICETCOMPLETED);
			} else if (type.trim().equalsIgnoreCase("service")) {
				try{
					serviceId=Integer.parseInt(req.getParameter("serviceId").trim());
				}catch(Exception e){
					
				}
			}

			User user = ofy().load().type(User.class)
					.filter("companyId", Long.parseLong(companyId))
					.filter("userName", team.trim()).first().now();

			logger.log(Level.SEVERE, "user.getEmployeeName().trim()::::::"
					+ user.getEmployeeName().trim() +" "+serviceId);
			
			if(user!=null)
				logger.log(Level.SEVERE , "User "+user.getEmployeeName()+" is fetching services from pedio application at"+new Date());
			
			
//			ArrayList<EmployeeBranch> employeeBranchlist = new ArrayList<EmployeeBranch>();
//			logger.log(Level.SEVERE,"user"+user);
//
//			/**
//			 * @author Vijay Date :- 14-06-2021
//			 * Des :- updated Code with Branch Fitler data should load as per user (Technician) accessable branches 
//			 * requirement raised by Nitin Sir
//			 */
//			if(user!=null) {
//				logger.log(Level.SEVERE,"inside user Employee name"+employeeBranchlist.size());
//
//				Employee employeeEntity = ofy().load().type(Employee.class).filter("fullname",user.getEmployeeName()).filter("companyId", Long.parseLong(companyId))
//											.first().now();
//				 logger.log(Level.SEVERE,"employeeEntity"+employeeEntity);
//				if(employeeEntity!=null) {
//					if(employeeEntity.getEmpBranchList()!=null)
//					employeeBranchlist.addAll(employeeEntity.getEmpBranchList());
//					
//					EmployeeBranch empBranch = new EmployeeBranch();
//					empBranch.setBranchName(employeeEntity.getBranchName());
//					employeeBranchlist.add(empBranch);
//				}
//				logger.log(Level.SEVERE,"employeeBranchlist size "+employeeBranchlist.size());
//
//			}
//			HashSet<EmployeeBranch> hsempBranch = new HashSet<EmployeeBranch>(employeeBranchlist);
//			ArrayList<EmployeeBranch> empBranchlist = new ArrayList<EmployeeBranch>(hsempBranch);
//			logger.log(Level.SEVERE,"empBranchlist size "+empBranchlist.size());
//			
//			ArrayList<String> strEmpBranchlist = new ArrayList<String>();
//			for(EmployeeBranch branch : empBranchlist) {
//				if(branch.getBranchName()!=null) {
//					strEmpBranchlist.add(branch.getBranchName());
//				}
//			}
//			logger.log(Level.SEVERE,"strEmpBranchlist size "+strEmpBranchlist.size());

			 if (type.trim().equalsIgnoreCase("service")){
				 Service ser = ofy().load().type(Service.class)
					.filter("companyId", Long.parseLong(companyId))
					.filter("count", serviceId).first().now();
				 serviceList = new ArrayList<Service>();
				 serviceList.add(ser);
			 }else{
				 /**
				  * @author Anil @since 27-01-2021
				  * Loading services here as per the new logic
				  */
				 if(type.trim().equalsIgnoreCase("schedule")){
					
					 //Ashwini Patil Date:8-02-2023 For PSIPL requirement sending service list as per customer, date and status(scheduled/Rescheduled)
					 if(custId!=null&&custName!=null) {
						 Employee employeeEntity = ofy().load().type(Employee.class).filter("companyId",Long.parseLong(companyId)).filter("fullname",user.getEmployeeName().trim()).first().now();
						 List<Service> tempserviceList = new ArrayList<Service>();
						 ArrayList<String> compBranchList=new ArrayList<String>();
						 
						 if(employeeEntity!=null) {
							 List<EmployeeBranch> empbranchlist= employeeEntity.getEmpBranchList();
							 if(empbranchlist!=null&&empbranchlist.size()>0) {
								 for(EmployeeBranch empbranch:empbranchlist) {
									 compBranchList.add(empbranch.getBranchName());
								 }
							 }
							 if(compBranchList!=null&&compBranchList.size()>0) {
								 Calendar cal2 = Calendar.getInstance();
									cal2.setTime(new Date(sdf.parse(toDate).getTime()));
									cal2.add(Calendar.DAY_OF_YEAR, 1);
									Date todate = cal2.getTime();
									String strtodate=todate.toString();
									 logger.log(Level.SEVERE,"CustomerWise service fetch : fromDate="+sdf.parse(fromDate)+"todate="+todate);
									 tempserviceList = ofy().load().type(Service.class)
												.filter("companyId", Long.parseLong(companyId))
												.filter("serviceDate >=", sdf.parse(fromDate))
												.filter("serviceDate <", todate)
												.filter("personInfo.count",Integer.parseInt(custId))
												.list();
									 
									 
									 if(tempserviceList!=null&&tempserviceList.size()!=0){
										 logger.log(Level.SEVERE,"tempserviceList size  "+tempserviceList.size());
											
										 for(Service s:tempserviceList) {
											 if(s.getStatus().equals(Service.SERVICESTATUSSCHEDULE)||s.getStatus().equals(Service.SERVICESTATUSRESCHEDULE)) {
												for(String br:compBranchList) {
													if(s.getBranch().equals(br)) {
														if(custBranch!=null&&!custBranch.equals("")) {
															if(s.getServiceBranch().equals(custBranch))
																serviceList.add(s);
														}else
															serviceList.add(s);
													}
												}
											 }
										 }
										 logger.log(Level.SEVERE,"serviceList size  "+serviceList.size());										 
									 }
								 }else 
									 logger.log(Level.SEVERE,"No branch found in branchlist of employee"+user.getEmployeeName());							 
						 
						 }else
							 logger.log(Level.SEVERE,"No employee found with name "+user.getEmployeeName());
						 
						 
					 }else {
						  if(newFromDate!=null){
							 logger.log(Level.SEVERE,"STEP 1 : ");
							 serviceList = ofy().load().type(Service.class)
										.filter("companyId", Long.parseLong(companyId))
										.filter("serviceDate >=", newFromDate)
										.filter("serviceDate <", sdf.parse(toDate))
										.filter("employee", user.getEmployeeName().trim())
										.filter("status IN", statusList).list();
							 logger.log(Level.SEVERE,"STEP 1 : serviceList size  "+serviceList.size());

							 if(applicableFromDate!=null&&applicableToDate!=null){
								 logger.log(Level.SEVERE,"STEP 2 : ");
								 List<String> status = new ArrayList<String>();
								 status.add(Service.SERVICESTATUSREPORTED);
								 status.add(Service.SERVICESTATUSSTARTED);
								 status.add(Service.SERVICETCOMPLETED);
								 
								 List<Service> list = ofy().load().type(Service.class)
											.filter("companyId", Long.parseLong(companyId))
											.filter("serviceDate >=", applicableFromDate)
											.filter("serviceDate <", applicableToDate)
											.filter("employee", user.getEmployeeName().trim())
											.filter("status IN", status)
											.list();
								 
								 if(list!=null&&list.size()!=0){
									 logger.log(Level.SEVERE,"IN PROCESS SERVICE LIST : "+list.size());
									 if(serviceList!=null&&serviceList.size()!=0){
										 logger.log(Level.SEVERE,"BF SERVICE LIST : "+serviceList.size());
										 serviceList.addAll(list);
										 logger.log(Level.SEVERE,"AF SERVICE LIST : "+serviceList.size());
									 }
								 }
							 }
						 }else{
							 serviceList = ofy().load().type(Service.class)
										.filter("companyId", Long.parseLong(companyId))
										.filter("serviceDate >=", sdf.parse(fromDate))
										.filter("serviceDate <", sdf.parse(toDate))
										.filter("employee", user.getEmployeeName().trim())
										.filter("status IN", statusList)
										.list();
						 }		
						 
					 }
					 
					 
				 }else{
					 serviceList = ofy().load().type(Service.class)
								.filter("companyId", Long.parseLong(companyId))
								.filter("serviceDate >=", sdf.parse(fromDate))
								.filter("serviceDate <", sdf.parse(toDate))
								.filter("employee", user.getEmployeeName().trim())
								.filter("status IN", statusList)
								.list();
				 }
			
				logger.log(Level.SEVERE,
						"After parsing new Date::::::::" + sdf.parse(date));
				logger.log(Level.SEVERE, "After parsing after new Date::::::::"
					+ sdf.parse(aftrDateinString));
			 }
			// .filter("status IN",statusList)
			// .filter("serviceDate <=", sdf.parse(date))
			Map<String , ArrayList<Service>> serviceMap = getServiceMap(serviceList);
			if(serviceMap!=null)
				logger.log(Level.SEVERE, "serviceMap size::::::::" + serviceMap.size());
			logger.log(Level.SEVERE, "serviceList::::::::" + serviceList);
			logger.log(Level.SEVERE, "serviceList::::::::" + serviceList.size());
			JSONArray jsonArray = new JSONArray();
			JSONObject jsonObj = null;
			
			ServerAppUtility serverappUtility = new ServerAppUtility();
			boolean addCountryCodeToCellNumber = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio", "PC_AddCountryCodeInCellNumber", comp.getCompanyId());
			for(Map.Entry<String, ArrayList<Service>> entry : serviceMap.entrySet()){
				serviceList = entry.getValue();
				jsonObj = new JSONObject();
				JSONArray jArray = new JSONArray();
				String customerName = "";
				String customerBranchEmail = "";
				String customerBranchName="";
				String customerBranchId ="";
				
				if(!serviceList.get(0).getServiceBranch().equalsIgnoreCase("Service Address")){
					CustomerBranchDetails details = ofy().load().type(CustomerBranchDetails.class).filter("companyId", serviceList.get(0).getCompanyId()).filter("buisnessUnitName", serviceList.get(0).getServiceBranch()).filter("cinfo.count", serviceList.get(0).getPersonInfo().getCount()) .first().now(); //Ashwini Patil Date:20-07-2023 added cinfo.count filter as different customers can have branches with same name and because of that email were going to different customer email
					if(details != null){
						customerBranchEmail = details.getEmail();		
						customerBranchName = details.getBusinessUnitName();
						customerBranchId = details.getCount()+"";
					}
				}
			label :	for (int i = 0; i < serviceList.size(); i++) {
				if(type.trim().equalsIgnoreCase("service") 
						|| type.trim().equalsIgnoreCase("history")){
					
				}else {
					
					if(!serviceList.get(i).isServiceScheduled()&&custId==null&&custName==null){
						continue label;
					}
				}
				if(serviceList.get(i).getCustomerName() != null){
					customerName = serviceList.get(i).getCustomerName();
				}else{
					customerName ="";
				}
				
				JSONObject jobj = new JSONObject();
				jobj.put("serviceId",
						(serviceList.get(i).getCount() + "").trim());
				logger.log(Level.SEVERE, "position " + i
						+ "Service ID::::::::::"
						+ (serviceList.get(i).getCount() + "").trim());
				jobj.put("branch",  serviceList.get(i).getBranch());
				SimpleDateFormat spf = new SimpleDateFormat("dd MMM yyyy");
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				spf.setTimeZone(TimeZone.getTimeZone("IST"));
				jobj.put("serviceDate",
						spf.format(serviceList.get(i).getServiceDate()).trim());
				logger.log(
						Level.SEVERE,
						"Service Date::::::::::"
								+ spf.format(
										serviceList.get(i).getServiceDate())
										.trim());

				/* Ashwini Patil 
				* Date:12-09-2023 
				* Ajinkya reported that when service is scheduled from ERP the schedule time shows in AM/PM format
				* and started, reported, completed time shows in 24 hour format. So making it standard
				*/
				String serviceTime="";
				try {
					if(serviceList.get(i).getServiceTime()!=null) {
						if(serviceList.get(i).getServiceTime().contains("AM")) {
							String hour=serviceList.get(i).getServiceTime().substring(0, 2);
							String min=serviceList.get(i).getServiceTime().substring(3,5);
							if(hour.equals("12"))
								serviceTime="00:"+min;
							else
								serviceTime=hour+":"+min;
						}else if(serviceList.get(i).getServiceTime().contains("PM")) {
							int hour=Integer.parseInt(serviceList.get(i).getServiceTime().substring(0, 2));
							String min=serviceList.get(i).getServiceTime().substring(3,5);
							if(hour==12)
								serviceTime=hour+":"+min;
							else {
								hour=hour+12;
								serviceTime=hour+":"+min;
							}
						}else
							serviceTime=serviceList.get(i).getServiceTime();
					}
					jobj.put("serviceTime",serviceTime);
					logger.log(Level.SEVERE, "serviceTime::::::::::"
							+ serviceTime);
				
				}catch(Exception e) {
					jobj.put("serviceTime",serviceList.get(i).getServiceTime());
				}

				jobj.put("serviceName", serviceList.get(i).getProductName()
						.trim());
				logger.log(Level.SEVERE, "serviceName::::::::::"
						+ serviceList.get(i).getProductName().trim());

				String clientName=serviceList.get(i).getPersonInfo().getFullName().trim();
//				if(customerBranchName!=null&&!customerBranchName.equals(""))
//					clientName+=" - "+customerBranchName; //not required as branch field is added in pedio below customer name
				jobj.put("clientfullname", clientName);
				logger.log(Level.SEVERE, "clientfullname::::::::::"
						+ clientName);
				
				jobj.put("poc", serviceList.get(i).getPersonInfo()
						.getPocName().trim());
				logger.log(Level.SEVERE, "poc::::::::::"
						+ serviceList.get(i).getPersonInfo().getPocName()
								.trim());

				/**
				 * @author Vijay Date :- 19-10-2021 
				 * Des :- added if below process config is active then adding country code(from country master) in cell number for innovative.
				 * else old code only cell number
				 */
				String customerCellNo = serviceList.get(i).getPersonInfo().getCellNumber()+"".trim();
				if(addCountryCodeToCellNumber){
					customerCellNo = serverappUtility.getMobileNoWithCountryCode(customerCellNo, serviceList.get(i).getAddress().getCountry(), comp.getCompanyId());
				}
				/**
				 * ends here
				 */
				if(customerCellNo!=null){
					jobj.put("cellNo", customerCellNo.trim());
				
//				jobj.put("cellNo", (serviceList.get(i).getPersonInfo()
//						.getCellNumber() + "").trim());
				logger.log(Level.SEVERE,
						"cellNo::::::::::"
								+ (customerCellNo.trim()));
				}else{
					jobj.put("cellNo","");
					logger.log(Level.SEVERE,"cellNo::::::::::");
				}

				if(serviceList.get(i).getAddress()!=null){
					if(serviceList.get(i).getAddress().getLocality()!=null)
						jobj.put("locality", serviceList.get(i).getAddress()
							.getLocality());
					else
						jobj.put("locality", "");

					String addr = getFullAddress(i,serviceList).toString();
					
					String address = getFullAddress(i,serviceList).toString();
					try{
						address	= getFullAddress(i,serviceList).toString().replace('"', ' ');
					}catch(Exception e){
						address = getFullAddress(i,serviceList).toString();
					}
					
					jobj.put("fullAddress", address);
				
				}else {
					jobj.put("locality", "");
					jobj.put("fullAddress", "");
				}
					

				jobj.put("status", serviceList.get(i).getStatus().trim());
                
				/***Date 24-01-2020 Deepak Salve added this code for Latitude & Longitude ***/
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio", "loadLatitudeLongitude" , comp.getCompanyId())){
				if(serviceList!=null&&serviceList.get(i).getAddress()!=null&&serviceList.get(i).getAddress().getLatitude()!=null){
				   jobj.put("latitude", serviceList.get(i).getAddress().getLatitude());
				}else{
					jobj.put("latitude","");
				}
				
				if(serviceList!=null&&serviceList.get(i).getAddress()!=null&&serviceList.get(i).getAddress().getLongitude()!=null){
				    jobj.put("longitude", serviceList.get(i).getAddress().getLongitude());
				}else{
					jobj.put("longitude","");
				}
			}
				/***End***/
				
				/*
				 * Name: Apeksha Gunjal
				 * Date: 07/06/2018 17:01
				 * Task: Added Premised details
				 */
				try{
					if(serviceList.get(i).getPremises() != null){
						jobj.put("premisesDetails", serviceList.get(i).getPremises());
					}else{
						jobj.put("premisesDetails", "");
					}
				}catch(Exception e){
					jobj.put("premisesDetails", "");
				}
				// Added by Apeksha on 06/10/2017 for adding serviceSerialNumber
				// to corresponding Api
				jobj.put("serviceSerialNumber", serviceList.get(i)
						.getServiceSerialNo());
				
				if (type.trim().equalsIgnoreCase("history") ||
						type.trim().equalsIgnoreCase("service")) {
					if(serviceList.get(i).getCustomerSignature()!=null){
					jobj.put("customerSignatureUrl", serviceList.get(i).getCustomerSignature().getUrl());
					}
					try{
						if(serviceList.get(i).getServiceWiseLatLong()!=null){
							jobj.put("serviceWiseLatLong", new org.json.JSONArray(serviceList.get(i).getServiceWiseLatLong().trim()));
							
						}
					}catch(Exception e){
						jobj.put("serviceWiseLatLong", "");
						e.printStackTrace();
					}
					/*
					 * Name: Apeksha Gunjal
					 * Date: 30/07/2018 @ 14:37
					 * Note: Adding technician remark to history details.
					 */
					try{
						logger.log(Level.SEVERE, "serviceList.get(i).getTechnicianRemark() ::::::::::"+serviceList.get(i).getTechnicianRemark());
						jobj.put("technicianRemark", serviceList.get(i).getTechnicianRemark());
					}catch(Exception e){
						jobj.put("technicianRemark", "");
						logger.log(Level.SEVERE, "Error ::::::::::"+e.getMessage());
					}
					SimpleDateFormat format = new SimpleDateFormat("HH:mm");
					format.setTimeZone(TimeZone.getTimeZone("IST"));
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));
					try{
						if(serviceList.get(i).getServiceCompletionDate() != null){
							String time = format.format(serviceList.get(i).getServiceCompletionDate());
							jobj.put("completionTime", time);
						}
					}catch(Exception e){
						jobj.put("completionTime", "");
					}
					if(serviceList.get(i).getServiceImage1()!=null){
						jobj.put("image1", serviceList.get(i).getServiceImage1().getUrl());
						}
					if(serviceList.get(i).getServiceImage2()!=null){
						jobj.put("image2", serviceList.get(i).getServiceImage2().getUrl());
						}
					if(serviceList.get(i).getServiceImage3()!=null){
						jobj.put("image3", serviceList.get(i).getServiceImage3().getUrl());
						}
					if(serviceList.get(i).getServiceImage4()!=null){
						jobj.put("image4", serviceList.get(i).getServiceImage4().getUrl());
						}
					if(serviceList.get(i).getServiceCompleteRemark()!=null){
						jobj.put("customerRemark", serviceList.get(i).getServiceCompleteRemark());
						}
					
					/** employee code and designation to show in histor ***/
					try {
					
					if(serviceList.get(i).getCustomerPersonEmpId()!=null){
						jobj.put("empCode", serviceList.get(i).getCustomerPersonEmpId());
						}
					if(serviceList.get(i).getCustomerPersonDesignation()!=null){
						jobj.put("empDesignation", serviceList.get(i).getCustomerPersonDesignation());
						}
					} catch (Exception e) {
						logger.log(Level.SEVERE, "exception in employee code and designation"+e.getMessage());
					}
					/**
					 * ends here
					 */
					
					if(serviceList.get(i).getCustomerSignName()!=null){
						jobj.put("customerSignName", serviceList.get(i).getCustomerSignName());
						}
					if(serviceList.get(i).getCustomerSignNumber()!=null){
						jobj.put("customerSignNumber", serviceList.get(i).getCustomerSignNumber());
						}
					if(serviceList.get(i).getCustomerFeedback() != null){
						jobj.put("feedbackRating",serviceList.get(i).getCustomerFeedback());
						
					}
					if(serviceList.get(i).getBranchEmail() != null){
						jobj.put("branchEmailId",serviceList.get(i).getBranchEmail());						
					}
					jobj.put("Id" , serviceList.get(i).getId()+"");
					jobj.put("SRNumber", serviceList.get(i).getSrCopyNumber());
					jobj.put("companyId", serviceList.get(i).getCompanyId()+"");
					JSONObject expenseObject  = getExpenseDetails(serviceList.get(i).getCompanyId() , serviceList.get(i).getCount());
					jobj.put("expense", expenseObject);
					JSONArray statusArray = new JSONArray();
					JSONObject statusObject = null;
					Map<String , TrackTableDetails> trackMap = new HashMap<String , TrackTableDetails>();
					for(TrackTableDetails trackDetails : serviceList.get(i).getTrackServiceTabledetails()){
						if(trackMap.containsKey(trackDetails.getStatus())){
							if(trackMap.get(trackDetails.getStatus()).getSrNo() 
									< trackDetails.getSrNo()){
								trackMap.put(trackDetails.getStatus(), trackDetails);
							}
						}else{
							trackMap.put(trackDetails.getStatus(), trackDetails);
						}
					}
					for(Map.Entry<String, TrackTableDetails> entry1 : trackMap.entrySet()){
						TrackTableDetails trackDetails = entry1.getValue();
						statusObject= new JSONObject();
						statusObject.put("dateTime", trackDetails.getDate_time());
						statusObject.put("status", trackDetails.getStatus());
						statusObject.put("location" , trackDetails.getLocationName());
						statusObject.put("latitude" , trackDetails.getLatitude());
						statusObject.put("longitude" , trackDetails.getLongitude());
						statusObject.put("km" , trackDetails.getKiloMeters());
						statusObject.put("time" , trackDetails.getTime());
						statusArray.add(statusObject);
					}
					jobj.put("trackingDetails", statusArray);
				}
				
				
				String servicetype=serviceList.get(i).getServiceType();
				//Ashwini Patil Date:26-12-2023 Ultra pest required complain word in service description to tell technician that this is complain service.
				if(!servicetype.equalsIgnoreCase("Complain")&&!servicetype.equalsIgnoreCase("Complaint"))
					servicetype="";
				else
					servicetype=servicetype+" service. ";	
				
				
				/*
				 * Added by Apeksha Date: 12/10/2017 for adding description,
				 * contractId, material and tools required
				 */
				
				try {
					if (serviceList.get(i).getDescription() != null) {
						jobj.put("description",servicetype+ serviceList.get(i)
								.getDescription());
					}
				}catch(Exception e){
					jobj.put("description", servicetype+" ");
				}
				
				jobj.put("contractId", serviceList.get(i).getContractCount());

//				if (type.trim().equalsIgnoreCase("schedule")) {

					List<ServiceProject> serviceProjectList = ofy().load()
							.type(ServiceProject.class)
							.filter("companyId", Long.parseLong(companyId))
							.filter("serviceId", serviceList.get(i).getCount())
							.list();

					logger.log(Level.SEVERE, "serviceProjectList::::::::::"
							+ serviceProjectList.size() + "");

					jobj.put("serviceProjectListSize",
							serviceProjectList.size() + "");

					if (serviceProjectList.size() > 0) {
						logger.log(Level.SEVERE,
								"inside if serviceProjectList.size()> 0");
						JSONArray materialRequiredArray = new JSONArray();
						JSONArray toolsRequiredArray = new JSONArray();
						/** date 12.3.2019 added by komal for technician list **/
						JSONArray technicianArray = new JSONArray();
						for (int k = 0; k < serviceProjectList.size(); k++) {						
							logger.log(Level.SEVERE,
									"serviceProjectList.get(k).getProdDetailsList():::"+serviceProjectList.get(k).getProdDetailsList().size());
							if (serviceProjectList.get(k).getProdDetailsList().size() != 0) {
								if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "TechnicianwiseMaterialPopup" , comp.getCompanyId())){
									if(type.trim().equalsIgnoreCase("service") 
											|| type.trim().equalsIgnoreCase("history")){
										
									}else {
										if(!serviceList.get(i).isServiceScheduled()){
											continue label;
										}
									}
								
								}
								try {
									ServerAppUtility appUtility=new ServerAppUtility();
									for (int j = 0; j < serviceProjectList
											.get(k).getProdDetailsList().size(); j++) {
										JSONObject obj = new JSONObject();
										logger.log(Level.SEVERE,"serviceProjectList.get(i).getProdDetailsList().get(j).getName()"+serviceProjectList.get(k).getProdDetailsList().get(j).getName());
										logger.log(Level.SEVERE,"serviceProjectList.get(i).getProdDetailsList().get(j).getName()"+serviceProjectList.get(k).getProdDetailsList().get(j).getCode());
										logger.log(Level.SEVERE,"serviceProjectList.get(i).getProdDetailsList().get(j).getName()"+serviceProjectList.get(k).getProdDetailsList().get(j).getQuantity());
										
										
										/**
										 * @author Anil , Date : 28-01-2020
										 */
										String productName = serviceProjectList.get(k).getProdDetailsList().get(j).getName().trim();
										logger.log(Level.SEVERE , "Befor Material Name :" + productName);
										productName=appUtility.encodeText(productName);
										logger.log(Level.SEVERE , "After Material Name :" + productName);
										obj.put("materialName",productName);
										
										logger.log(Level.SEVERE , "Decoding Material Name :" + appUtility.decodeText(productName));
										
//										obj.put("materialName",
//												serviceProjectList.get(k)
//														.getProdDetailsList()
//														.get(j).getName()
//														.trim());
										/**
										 * End
										 */
										
										
										
										obj.put("materialCode",
												serviceProjectList.get(k)
														.getProdDetailsList()
														.get(j).getCode()
														.trim());
										
										/**
									     * @author Vijay Chougule
									     * Des :- if Automatic Scheduling process Config is active then Quantity will map from Technician Stock Master
									     * its available Qty. for Service Scheduling for Technician App.
									     * Else part added as old code for normal senarios
									     */
										
										//Ashwini Patil Date:17-02-2023
										//Commenting this log as per Nitin sir's instruction. Even if automatic scheduling is active the quantity should get mapped to pedio from service only
//									    if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICEMODULE, AppConstants.AUTOMATICSCHEDULING, serviceList.get(i).getCompanyId())){
//									    	double availableQty = getAvailableStock(serviceList.get(i).getCompanyId(), serviceProjectList.get(k).getProdDetailsList().get(j).getProduct_id(),
//									    			serviceProjectList.get(k).getProdDetailsList().get(j).getWarehouse(), serviceProjectList.get(k).getProdDetailsList().get(j).getStorageLocation(),
//									    			serviceProjectList.get(k).getProdDetailsList().get(j).getStorageBin());
//									    	logger.log(Level.SEVERE, "availableQty from stock master "+availableQty);
//									    	obj.put("materialQty",availableQty);
//									    }
//									    else{
									    	obj.put("materialQty",
													serviceProjectList.get(k)
															.getProdDetailsList()
															.get(j).getQuantity() + "");	
//									    }

//										obj.put("materialQty",
//												serviceProjectList.get(k)
//														.getProdDetailsList()
//														.get(j).getQuantity() + "");
										obj.put("unit",
												serviceProjectList.get(k)
														.getProdDetailsList()
														.get(j).getUnit() + "");
										
										obj.put("materialReturnQty",
												serviceProjectList.get(k)
														.getProdDetailsList()
														.get(j).getReturnQuantity()+ "");
										materialRequiredArray.add(obj);
									}
									logger.log(
											Level.SEVERE,
											"materialRequiredArray.size():::"
													+ materialRequiredArray
															.size());
									jobj.put("materialRequired",
											materialRequiredArray);

								} catch (Exception e) {
									e.printStackTrace();
									logger.log(Level.SEVERE,"Error::::"+e);
								}
							}

							logger.log(Level.SEVERE,
									"serviceProjectList.get(k).getTooltable():::"+serviceProjectList.get(k).getTooltable().size());
							if (serviceProjectList.get(k).getTooltable().size() != 0) {
								try {

									for (int j = 0; j < serviceProjectList
											.get(k).getTooltable().size(); j++) {
										logger.log(Level.SEVERE,"serviceProjectList.get(k).getTooltable().get(j).getName()"+serviceProjectList.get(k).getTooltable().get(j).getName());
										
										JSONObject obj = new JSONObject();
										obj.put("toolName", serviceProjectList
												.get(k).getTooltable().get(j)
												.getName().trim());
										// obj.put("toolBrand",
										// serviceProjectList.get(i)
										// .getTooltable().get(j).getBrand().trim());
										// obj.put("toolMNo",
										// (serviceProjectList.get(i).getTooltable()
										// .get(j).getModelNo() + "").trim());
										toolsRequiredArray.add(obj);
									}
									logger.log(Level.SEVERE,
											"toolsRequiredArray.size():::"
													+ toolsRequiredArray.size());
									jobj.put("toolsRequired",
											toolsRequiredArray);
									/*
									 * } catch (Exception e) {
									 * e.printStackTrace();
									 * logger.log(Level.SEVERE,
									 * "ERROR in Tools Required:::" + e); }
									 */
								} catch (Exception e) {
									e.printStackTrace();
									logger.log(Level.SEVERE,"Error 2::::"+e);
								}
							}
							
							/** date 12.3.2019 added by komal fr technician list **/
							logger.log(Level.SEVERE,"serviceProjectList.get(i).getProdDetailsList().size() "+serviceProjectList.get(k).getTechnicians().size());
								if (serviceProjectList.get(k).getTechnicians().size() != 0) {
									try {

										for (int j = 0; j < serviceProjectList
												.get(k).getTechnicians().size(); j++) {
											JSONObject obj = new JSONObject();
											logger.log(Level.SEVERE,"serviceProjectList.get(i).getTechnicians().get(j).getName()"+serviceProjectList.get(k).getTechnicians().get(j).getFullName());
											
											
											obj.put("technicianName",
													serviceProjectList.get(k)
															.getTechnicians()
															.get(j).getFullName()
															.trim());
											if(serviceProjectList.get(k)
													.getTechnicians()
													.get(j).getFullName()
													.trim().equalsIgnoreCase(user.getEmployeeName().trim())){
												obj.put("mainTechnician", "YES");
											}else{
												obj.put("mainTechnician", "NO");
											}
											technicianArray.add(obj);
										}
										logger.log(
												Level.SEVERE,
												"materialRequiredArray.size():::"
														+ materialRequiredArray
																.size());
										jobj.put("technician",
												technicianArray);

									} catch (Exception e) {
										e.printStackTrace();
										logger.log(Level.SEVERE,"Error::::"+e);
									}
								}
						}
					}else if(serviceList.get(i).getTechnicians()!=null&&serviceList.get(i).getTechnicians().size()>0) {

							try {
								JSONArray technicianArray = new JSONArray();
								List<EmployeeInfo> teamMembers=serviceList.get(i).getTechnicians();
								for (EmployeeInfo info:teamMembers) {
									JSONObject obj = new JSONObject();
									
									obj.put("technicianName",info.getFullName());
									if(info.getFullName().trim().equalsIgnoreCase(user.getEmployeeName().trim())){
										obj.put("mainTechnician", "YES");
									}else{
										obj.put("mainTechnician", "NO");
									}
									technicianArray.add(obj);
								}
								jobj.put("technician",technicianArray);

							} catch (Exception e) {
								e.printStackTrace();
								logger.log(Level.SEVERE,"Error::::"+e);
							}
						
						}					
	//			}
				/*
				 * Name: Apeksha Gunjal
				 * Date: 23/08/2018 @19:10
				 * Note: Added Service serial number and service model number in 
				 * json object to show it in EVAPediO application.
				 */
				try{
					jobj.put("serialNumber", serviceList.get(i).getProSerialNo());
				}catch(Exception e){
					jobj.put("serialNumber", "");
					logger.log(Level.SEVERE, "Error Serial No. "+e);
				}
				try{
					jobj.put("modelNumber", serviceList.get(i).getProModelNo());
				}catch(Exception e){
					jobj.put("modelNumber", "");
					logger.log(Level.SEVERE, "Error Serial No. "+e);
				}
				/*
				 * end model no serial no
				 */
				/**
				 * Rahul Verma added this on 04 Oct 2018
				 * Description: Multiple Technician for service
				 * 
				 */
				try{
					String technicians="";
					int count=0;
					
					if(serviceList.get(i).getEmployee() != null && !serviceList.get(i).equals("")){
						technicians = serviceList.get(i).getEmployee();
					}
					for(EmployeeInfo empInfo:serviceList.get(i).getTechnicians()){
						if(count==0 && !technicians.contains(serviceList.get(i).getEmployee())){
							technicians=empInfo.getFullName();
						}else if(!technicians.contains(serviceList.get(i).getEmployee())){
							technicians=technicians+","+empInfo.getFullName();
						}
						count++;
					}
					jobj.put("technicians", technicians);
				}catch(Exception e){
					jobj.put("technicians", "");
					logger.log(Level.SEVERE, "Error Serial No. "+e);
				}
				/**
				 * Ends
				 */

				jobj.put("isRateContractService", serviceList.get(i).isRateContractService());
				logger.log(Level.SEVERE, "status::::::::::"
						+ serviceList.get(i).getStatus().trim());

				/*
				 * Name: Apeksha Gunjal
				 * Date: 27/09/2018 @ 15:23
				 * Note: Added serviceCompleteDuration to jsonObject, which is in minutes
				 */
				try{	
					//Ashwini Patil Date:20-10-2022 Ankita Pest reported that in pedio history service duration shown is tcompleted to completed but they want reported to completed so making this change
					jobj.put("serviceDuration", serviceList.get(i).getTotalServiceDuration());//serviceList.get(i).getServiceCompleteDuration()
				}catch(Exception e){
					jobj.put("serviceDuration", "");
					logger.log(Level.SEVERE, "Error serviceDuration: "+e);
				}
				/*
				 * end serviceCompleteDuration...
				 */
				/**date 23.03.2019 to get trap details from service**/
				JSONArray trapDetailsArray = new JSONArray();
				JSONObject obj = new JSONObject();
				String fumigationType = "";
				if(serviceList.get(i).getProduct() != null && serviceList.get(i).getProduct().getProductCode() != null){
					if(serviceList.get(i).getFumigationProductCodes().contains(serviceList.get(i).getProduct().getProductCode())){
						fumigationType = "Fumigation";
					}else{
						fumigationType = "NonFumigation";
					}
				}
				try{
					jobj.put("type", fumigationType);
				}catch(Exception e){
					jobj.put("type", "");
					logger.log(Level.SEVERE, "Error serviceDuration: "+e);
				}
				if(serviceList.get(i).getCatchtrapList() != null && serviceList.get(i).getCatchtrapList().size() > 0){
					for(CatchTraps trap : serviceList.get(i).getCatchtrapList()){
						obj = new JSONObject();
						obj.put("name", trap.getPestName());
						obj.put("count", trap.getCount());
						obj.put("location", trap.getLocation());
						if(trap.getCatchLevel()!=null)
							obj.put("catchLevel", trap.getCatchLevel());
						else
							obj.put("catchLevel", "");
						obj.put("number", trap.getContainerNo());
						obj.put("size", trap.getContainerSize());
						trapDetailsArray.add(obj);					
					}
					logger.log(Level.SEVERE,"trapDetailsArray.size():::"+ trapDetailsArray.size());
					
				}
				jobj.put("trapDetails",trapDetailsArray);
				
				/**
				 * @author Vijay Chougule
				 * Des :- NBHC CCPM Operator App when service is WMS Service then i am sending stack details 
				 * else i will send "NA" for stack details for other services
				 */
				if(serviceList.get(i).isWmsServiceFlag()){
					try{
						jobj.put("stackName", serviceList.get(i).getStackNo());
						jobj.put("stackQty", serviceList.get(i).getStackQty());
						jobj.put("commodityName", serviceList.get(i).getCommodityName());
					}catch(Exception e){
						logger.log(Level.SEVERE, "Error reading stack details "+e);
					}
				}
				else{
					jobj.put("stackName", "NA");
					jobj.put("stackQty", "NA");
					jobj.put("commodityName", "NA");
				}
				/**
				 * @author Anil
				 * @since 26-06-2020
				 * FOR UMAS- check list need to be separated, earlier both check list and finding are stored in same field 
				 */
				JSONArray checkListDetailsArray = new JSONArray();
				if(serviceList.get(i).getCheckList()!= null &&serviceList.get(i).getCheckList().size()>0){
					for(CatchTraps trap : serviceList.get(i).getCheckList()){
						obj = new JSONObject();
						obj.put("name", trap.getPestName());
						obj.put("remark", trap.getLocation());
						checkListDetailsArray.add(obj);
					
					}
					logger.log(Level.SEVERE,"checkListDetailsArray.size():::"+ checkListDetailsArray.size());
				}
				jobj.put("checkListDetails",checkListDetailsArray);
				
				jobj.put("customerId",serviceList.get(i).getPersonInfo().getCount());

				jobj.put("productCategory",serviceList.get(i).getProduct().getProductCategory());

				
				
//				/** @UAT
//				 * @author Vijay Chougule 13-01-2020
//				 * Des :- Seperated Check list Data in service entity
//				 */
//				JSONArray checklistDetailsArray = new JSONArray();
//				if(serviceList.get(i).getChecklist() != null && serviceList.get(i).getChecklist().size() > 0){
//					for(CheckList checkdata : serviceList.get(i).getChecklist()){
//						obj = new JSONObject();
//						obj.put("name", checkdata.getName());
//						checklistDetailsArray.add(obj);
//					
//					}
//					logger.log(Level.SEVERE,"checklistDetailsArray.size():::"+ checklistDetailsArray.size());
//					
//				}
//				jobj.put("checklistDetails",checklistDetailsArray);
//				
//				if(serviceList.get(i).getChecklistRemark()!=null){
//					jobj.put("checklistRemark", serviceList.get(i).getChecklistRemark());
//				}
//				else{
//					jobj.put("checklistRemark", "");
//				}
//				/**
//				 * ends here
//				 */

				/***
				 * @author Anil @since 20-12-2021
				 * Adding assessment flag and service type in service data to pedio app
				 * Raised by Nitin sir for prominent
				 */
				jobj.put("assessmentFlag",serviceList.get(i).isAssessmentServiceFlag());
				jobj.put("serviceType",serviceList.get(i).getServiceType());
				
				if(customerBranchName!=null&&!customerBranchName.equals(""))
					jobj.put("customerBranchName", customerBranchName);
				else if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("EVA Pedio", "PC_ENABLESEARCHFILTERBYCUSTOMERBRANCH", comp.getCompanyId()))//Ashwini Patil Date:18-07-2023 if customer branch is blank then filter in pedio is not working  for such services
					jobj.put("customerBranchName", "NA");
				else
					jobj.put("customerBranchName", "");
				jobj.put("customerBranchId", customerBranchId);
				
				jArray.add(jobj);
			}
				
				jsonObj.put("contractId", entry.getKey() + " - "+ customerName);
				if(customerBranchEmail != null && !customerBranchEmail.equalsIgnoreCase("null")){
					jsonObj.put("customerBranchEmail",customerBranchEmail);
				}else{
					jsonObj.put("customerBranchEmail","");
				}
				
				
				jsonObj.put("service", jArray);
				if(jArray.size()>0){
					jsonArray.add(jsonObj);
				}
			}
			
			logger.log(Level.SEVERE, "json array:::::::::::::::::::::" + jsonArray);
			// Gson gson=new Gson();
			// String json=gson.toJson(allocate);
			String jsonString = jsonArray.toJSONString().replaceAll("\\\\", "");
			logger.log(Level.SEVERE, jsonString);
			resp.getWriter().println(jsonString);

		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "ERROR:::::::::::::::::::::" + e);
			// exep=e;
			resp.getWriter().println("Failed");
		}

	}

	private Object getFullAddress(int i , List<Service> serviceList) {
		// TODO Auto-generated method stub
		String address = null;
		String add = null;
		String fullAddress = null;
		if (!serviceList.get(i).getAddress().getAddrLine2()
				.equalsIgnoreCase("")) {
			if (!serviceList.get(i).getAddress().getLandmark()
					.equalsIgnoreCase("")) {
				address = serviceList.get(i).getAddress().getAddrLine1() + ", "
						+ serviceList.get(i).getAddress().getAddrLine2() + ", "
						+ serviceList.get(i).getAddress().getLandmark();
			} else {
				address = serviceList.get(i).getAddress().getAddrLine1() + ", "
						+ serviceList.get(i).getAddress().getAddrLine2();
			}
		} else {
			if (!serviceList.get(i).getAddress().getLandmark()
					.equalsIgnoreCase("")) {
				address = serviceList.get(i).getAddress().getAddrLine1() + ", "
						+ serviceList.get(i).getAddress().getLandmark();
			} else {
				address = serviceList.get(i).getAddress().getAddrLine1();
			}
		}

		if (!serviceList.get(i).getAddress().getLocality().equalsIgnoreCase("")) {
			add = address + ", "
					+ serviceList.get(i).getAddress().getLocality();
		} else {
			add = address;
		}

		fullAddress = add + ", " + serviceList.get(i).getAddress().getCity()
				+ ", " + serviceList.get(i).getAddress().getState() + ", "
				+ serviceList.get(i).getAddress().getCountry() + ", Pin :"
				+ serviceList.get(i).getAddress().getPin();
		return fullAddress;
	}
  private HashMap<String,ArrayList<Service>> getServiceMap(List<Service> serviceList){
	  HashMap<String,ArrayList<Service>> map = new HashMap<String,ArrayList<Service>>();
	  ArrayList<Service> list = new ArrayList<Service>();
	  for(Service service : serviceList){
		  if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "CustomerwiseServices", comp.getCompanyId())){
			  if(map.containsKey(service.getPersonInfo().getCount()+"/"+service.getServiceBranch())){
			     list = map.get(service.getPersonInfo().getCount()+"/"+service.getServiceBranch());
			     list.add(service);
			     map.put(service.getPersonInfo().getCount()+"/"+service.getServiceBranch(), list);
			  }else{
				  list = new ArrayList<Service>();
				  list.add(service);
				  map.put(service.getPersonInfo().getCount()+"/"+service.getServiceBranch(), list);
			  }
		  }else{
			  if(map.containsKey(service.getContractCount()+"/"+service.getServiceBranch())){
				     list = map.get(service.getContractCount()+"/"+service.getServiceBranch());
				     list.add(service);
				     map.put(service.getContractCount()+"/"+service.getServiceBranch(), list);
				  }else{
					  list = new ArrayList<Service>();
					  list.add(service);
					  map.put(service.getContractCount()+"/"+service.getServiceBranch(), list);
				  }
		  }
	  }
	  
	  return map;
  }
  private JSONObject getExpenseDetails(long companyId , int serviceId){
	  	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	  	JSONObject mainJsonObj = new JSONObject();
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj  = null;
		MultipleExpenseMngt expense=ofy().load().type(MultipleExpenseMngt.class).filter("companyId", comp.getCompanyId()).filter("serviceId", serviceId).first().now();
		if(expense != null){
			
			if(expense.getExpenseList() != null && expense.getExpenseList().size() > 0){
				for(ExpenseManagement expenseManagement : expense.getExpenseList()){
				jsonObj = new JSONObject();
				if(expenseManagement.getExpenseDate() != null && !expenseManagement.getExpenseGroup().equals("")){
					jsonObj.put("expenseDate", sdf.format(expenseManagement.getExpenseDate()));
				}else{
					jsonObj.put("expenseDate","");
				}
				
				if(expenseManagement.getInvoiceNumber() != null){
					jsonObj.put("invoiceId", expenseManagement.getInvoiceNumber());
				}else{
					jsonObj.put("invoiceId","");
				}
				
				if(expenseManagement.getExpenseGroup() != null){
					jsonObj.put("expenseGroup", expenseManagement.getExpenseGroup());
				}else{
					jsonObj.put("expenseGroup","");
				}
				
				if(expenseManagement.getFromCity() != null){
					jsonObj.put("deptCity", expenseManagement.getFromCity());
				}else{
					jsonObj.put("deptCity","");
				}
				
				if(expenseManagement.getToCity() != null){
					jsonObj.put("arrCity", expenseManagement.getToCity());
				}else{
					jsonObj.put("arrCity","");
				}
				
				if(expenseManagement.getFromDate() != null && !expenseManagement.getFromDate().equals("")){
					jsonObj.put("deptDate", sdf.format(expenseManagement.getFromDate()));
				}else{
					jsonObj.put("deptDate","");
				}
				
				if(expenseManagement.getToDate() != null && !expenseManagement.getToDate().equals("")){
					jsonObj.put("arrDate", sdf.format(expenseManagement.getToDate()));
				}else{
					jsonObj.put("arrDate","");
				}
				
				jsonObj.put("kmTravelled", expenseManagement.getKmTravelled()+"");
				
				if(expenseManagement.getRemark() != null){
					jsonObj.put("remark", expenseManagement.getRemark());
				}else{
					jsonObj.put("remark","");
				}
				
				if(expenseManagement.getExpenseCategory() != null){
					jsonObj.put("expenseCategory", expenseManagement.getExpenseCategory());
				}else{
					jsonObj.put("expenseCategory","");
				}
				
				if(expenseManagement.getExpenseType() != null){
					jsonObj.put("expenseType", expenseManagement.getExpenseType());
				}else{
					jsonObj.put("expenseType","");
				}
				
				if(expenseManagement.getPaymentMethod() != null){
					jsonObj.put("paymentMethod", expenseManagement.getPaymentMethod());
				}else{
					jsonObj.put("paymentMethod","");
				}
				
				if(expenseManagement.getVendor() != null){
					jsonObj.put("vendor", expenseManagement.getVendor());
				}else{
					jsonObj.put("vendor","");
				}
				
				jsonObj.put("Amount", expenseManagement.getAmount()+"");
				
				if(expenseManagement.getVendor() != null){
					jsonObj.put("vendor", expenseManagement.getVendor());
				}else{
					jsonObj.put("vendor","");
				}
				
				if(expenseManagement.getVendor() != null){
					jsonObj.put("vendor", expenseManagement.getVendor());
				}else{
					jsonObj.put("vendor","");
				}
				jArray.add(jsonObj);
				}
			 }			
			mainJsonObj.put("expenseList", jArray);
			mainJsonObj.put("branch", expense.getBranch());
			mainJsonObj.put("serviceId", expense.getCount());
			mainJsonObj.put("totalAmount", expense.getTotalAmount()+"");
		}
		return mainJsonObj;
  }
  
  private double getAvailableStock(long companyId, Integer prodId, String technicianWarehouse, String technicianLocation, String technicianBin) {

		List<ProductInventoryView> prodInvViewList=ofy().load().type(ProductInventoryView.class).filter("companyId",companyId).filter("productinfo.prodID", prodId).list();
		ObjectifyService.reset();
	    ofy().consistency(Consistency.STRONG);
		ofy().clear();
		
		double availableQty=0;
		logger.log(Level.SEVERE,"Technician Stock Master"+prodInvViewList.size());
		for(ProductInventoryView piv:prodInvViewList){
				if(piv.getProdID() == prodId){
					for(ProductInventoryViewDetails prod:piv.getDetails()){
						if(prod.getWarehousename()!=null && prod.getWarehousename().equals(technicianWarehouse)
								&&prod.getStoragelocation().equals(technicianLocation)
								&&prod.getStoragebin().equals(technicianBin)){
							availableQty=prod.getAvailableqty();
							break;
						}		
					}
			}	
		}	
		logger.log(Level.SEVERE,"availableQty"+availableQty);

		return availableQty;
	}
}
