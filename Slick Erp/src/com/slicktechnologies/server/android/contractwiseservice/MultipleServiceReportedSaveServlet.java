package com.slicktechnologies.server.android.contractwiseservice;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;


import org.json.JSONObject;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.device.TrackTableDetails;
import com.slicktechnologies.server.android.technicianapp.TechnicianAppOperation;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.ModificationHistory;
import com.slicktechnologies.shared.common.android.RegisterDevice;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.CustomerUser;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.personlayer.Employee;

/**
 * Description :
 * It is used to start all clubbed service at a time.
 * @author pc1
 *
 */
public class MultipleServiceReportedSaveServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2558449433504212668L;
	/**
	 * 
	 */
	
	
	Logger logger = Logger.getLogger("ReportedServiceDetailsSaveServlet.class");
	Company comp;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doGet(req, resp);
		doPost(req, resp);
	}

	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String urlCalled=req.getRequestURL().toString().trim();
//		String url=urlCalled.replace("http://", "");
//		String[] splitUrl=url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		/**
		 * Updated By: Viraj
		 * Date: 02-03-2019
		 * Description: To store latitude and longitude
		 */
		String lat = null;
		String lon = null;
		/** Ends **/
		/**
		 * Updated By: komal
		 * Date: 01-04-2019
		 * Description: To get and save location 
		 */
		String location = "";
		Exception exep = null;
		try {

			
		comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();	
//		resp.setContentType("text/plain");
		/**
		 * @author Anil @since 14-06-2021
		 * Thai character was not getting download in proper format
		 * raised by Rahul Tiwari
		 */
//		resp.setContentType("text/plain");
		resp.setContentType("text/plain; charset=UTF-8");
		resp.setCharacterEncoding("UTF-8");
		
		String companyId=comp.getCompanyId()+"";
		String servicesData=req.getParameter("servicesData").trim();
		logger.log(Level.SEVERE,"servicesData::::::::"+servicesData);
		ArrayList<String> serIdList = new ArrayList<String>();	
		JSONObject serviceJSONObject = new JSONObject(servicesData);
		JSONArray servicesArray = null;
		try {
			servicesArray = serviceJSONObject.getJSONArray("service");
			
		} catch (JSONException e1) {
			// TODO Auto-generated catch blocks
			e1.printStackTrace();
		}
		JSONObject obj = null;
		//Ashwini Patil Date:15-04-2023 Whatsapp msgs were getting sent multiple times because sometimes erp was getting 2 or more copies of same service. Could not find the scenario so managing from code that one service id will be processed only once.
		HashSet<String> processedServiceIdList=new HashSet<String>();
		logger.log(Level.SEVERE,"processedServiceIdList created");
		for(int m =0 ; m<servicesArray.length() ; m++){
			try {
				obj = servicesArray.getJSONObject(m);
		
		
		String status=obj.optString("status").trim();
		String serviceId=obj.optString("serviceId").trim();
		
		serIdList.add(serviceId);
		boolean syncFlag=Boolean.parseBoolean(obj.optString("syncFlag"));
		String rescheduleData="";
		
		if(!processedServiceIdList.contains(serviceId)){

			
			if(syncFlag){
				rescheduleData=obj.optString("rescheduledData").trim();
			}
			logger.log(Level.SEVERE,"companyId::::::::"+companyId);
			logger.log(Level.SEVERE,"status:::::::::::"+status);
			logger.log(Level.SEVERE,"serviceId::::::::"+serviceId);
			logger.log(Level.SEVERE,"processedServiceIdList size "+processedServiceIdList.size());
			/**
			 * @author Vijay Chougule Date 01-10-2020
			 * Des :- Sevice started reported tcompleted and completed Time issue in other country so now mapping timing from app not
			 * from the Date
			 */
			String serviceActionTime ="";
			try {
				serviceActionTime = obj.optString("serviceTime");
			} catch (Exception e) {
				// TODO: handle exception
			}
			logger.log(Level.SEVERE,"time == "+serviceActionTime);
			
			SimpleDateFormat format = new SimpleDateFormat("HH:mm");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			format.setTimeZone(TimeZone.getTimeZone("IST"));
			
			SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
			timeFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			
			
			/**
			 * @author Anil @since 06-03-2021
			 * New formatter is added to send correct date in sms which is getting sent to customer at the time of starting/reporting of services
			 */
			SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			
			/**
			 * @author Vijay Date :- 24-10-2020
			 * Des :- As per nitin sir instruction started reported tcompleted and Completed must store in Date 
			 * format with date and time 
			 */
			String strActionDateTime = "";
			Date actionDateTime = null;
			try {
				strActionDateTime = obj.optString("serviceActionDateTime");
				actionDateTime = sdf.parse(strActionDateTime);
			} catch (Exception e) {
				// TODO: handle exception
			}
			logger.log(Level.SEVERE,"strActionDateTime "+strActionDateTime);
			logger.log(Level.SEVERE,"actionDateTime "+actionDateTime);
			/**
			 * ends here
			 */
			Service service=ofy().load().type(Service.class).filter("companyId", Long.parseLong(companyId)).filter("count",Long.parseLong(serviceId)).first().now();
			logger.log(Level.SEVERE,"service::::::::"+service);
			
			/**
			 * @author Vijay Date :- 23-10-2020
			 * Des :- if service is completed then it should not update for started reported again added checker here
			 */
			boolean valdateflag = true;
			if(service.getStatus().trim().equalsIgnoreCase(Service.SERVICESTATUSCOMPLETED) && 
					(status.trim().equalsIgnoreCase(Service.SERVICESTATUSREPORTED) || status.trim().equalsIgnoreCase(Service.SERVICESTATUSSTARTED) 
							|| status.trim().equalsIgnoreCase(Service.SERVICETCOMPLETED))){
				resp.getWriter().print("Service status is completed in ERP System so you can not mark as Started or Reported or Tcompleted");
			}
			logger.log(Level.SEVERE,"after response "+service);
			logger.log(Level.SEVERE,"valdateflag"+valdateflag);
			if(valdateflag){
				
			if(status.trim().equalsIgnoreCase(Service.SERVICESTATUSREPORTED)){
				lat=obj.optString("lat").trim();
				lon=obj.optString("lon").trim();
				/**
				 * Updated By: komal
				 * Date: 01-04-2019
				 * Description: To get and save location
				 */
				try{
					location = obj.optString("location").trim();
				}catch(Exception e){
					location = "";
					e.printStackTrace();
				}
				service.setReportLatnLong(lat+"$"+lon);
				service.setFromTime(format.format(new Date())+"");
				service.setStatus(Service.SERVICESTATUSREPORTED);
				
				if(strActionDateTime!=null && !strActionDateTime.equals("") ){
					service.setReportedDate_time(strActionDateTime);
				}
				else{
					service.setReportedDate_time(sdf.format(new Date())+"");
				}
				service.setReportedLatitude(lat);
				service.setReportedLongitude(lon);
				if(serviceActionTime.equals("")){
					service.setReportedTime(serviceActionTime);
				}
				else{
					service.setReportedTime(timeFormat.format(new Date()));
				}
				service.setReportedLocationName(location);
				if(actionDateTime!=null){
					service.setReportedDate(actionDateTime);
				}
			}else if(status.trim().equalsIgnoreCase(Service.SERVICESTATUSSTARTED)){
				lat=obj.optString("lat").trim();
				lon=obj.optString("lon").trim();
				/**
				 * Updated By: komal
				 * Date: 01-04-2019
				 * Description: To get and save location
				 */
				try{
					location = obj.optString("location").trim();
				}catch(Exception e){
					location = "";
					e.printStackTrace();
				}
				service.setStartLatnLong(lat+"$"+lon);
				
				service.setStatus(Service.SERVICESTATUSSTARTED);
				
				if(strActionDateTime!=null && !strActionDateTime.equals("")){
					service.setStartedDate_time(strActionDateTime);
				}
				else{
					service.setStartedDate_time(sdf.format(new Date())+"");
				}
				service.setStartedLatitude(lat);
				service.setStartedLongitude(lon);
				if(serviceActionTime.equals("")){
					service.setStartedTime(serviceActionTime);
				}
				else{
					service.setStartedTime(timeFormat.format(new Date()));
				}
				
				service.setStartedLocationName(location);
				if(actionDateTime!=null){
					service.setStartedDate(actionDateTime);
				}
			}else if(status.trim().equalsIgnoreCase(Service.SERVICETCOMPLETED)){
				lat=obj.optString("lat").trim();
				lon=obj.optString("lon").trim();
				/**
				 * Updated By: komal
				 * Date: 01-04-2019
				 * Description: To get and save location
				 */
				try{
					location = obj.optString("location").trim();
				}catch(Exception e){
					location = "";
					e.printStackTrace();
				}
				service.setStartLatnLong(lat+"$"+lon);
				
				service.setStatus(Service.SERVICETCOMPLETED);
				if(strActionDateTime!=null && !strActionDateTime.equals("")){
					service.settCompletedDate_time(strActionDateTime);
				}
				else{
					service.settCompletedDate_time(sdf.format(new Date())+"");
				}
//				service.settCompletedDate_time(sdf.format(new Date())+"");
				service.settCompletedLatitude(lat);
				service.settCompletedLongitude(lon);
				if(serviceActionTime.equals("")){
					service.settCompletedTime(serviceActionTime);
				}
				else{
					service.settCompletedTime(timeFormat.format(new Date()));
				}
				
				service.settCompletedLocationName(location);
				if(actionDateTime!=null){
					service.setTcompletedDate(actionDateTime);
				}
			}
			
			logger.log(Level.SEVERE,"Stage::::::::1");
			TrackTableDetails tracktabledetails=new TrackTableDetails();
			logger.log(Level.SEVERE,"Stage::::::::2");
			
			
			if(strActionDateTime!=null && !strActionDateTime.equals("")){
				tracktabledetails.setDate_time(strActionDateTime);
			}
			else{
				tracktabledetails.setDate_time(sdf.format(new Date())+"");
			}
//			tracktabledetails.setDate_time(sdf.format(new Date())+"");
			
			logger.log(Level.SEVERE,"Stage::::::::3");
			if(status.trim().equalsIgnoreCase(Service.SERVICESTATUSREPORTED)){
			tracktabledetails.setStatus(Service.SERVICESTATUSREPORTED);
			}else if(status.trim().equalsIgnoreCase(Service.SERVICESTATUSSTARTED)){
			tracktabledetails.setStatus(Service.SERVICESTATUSSTARTED);
			}else if(status.trim().equalsIgnoreCase(Service.SERVICETCOMPLETED)){
			tracktabledetails.setStatus(Service.SERVICETCOMPLETED);
			TechnicianAppOperation app = new TechnicianAppOperation();
				try {
					String trapDetailsdata=obj.optString("saveTrapDetails").trim();
					JSONObject jsonData = new JSONObject(trapDetailsdata);
					String tranResponse = app.saveTrapDetails(jsonData, comp, req, resp,service);
					logger.log(Level.SEVERE , "Trap response :" + tranResponse);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try{
					String materialdata=obj.optString("materialReturn").trim();
					JSONObject jsonMaterialData = new JSONObject(materialdata);
					String materialResponse = app.createMaterialReturnDoc(comp, jsonMaterialData, req, resp);
					logger.log(Level.SEVERE , "Material response :" + materialResponse);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			logger.log(Level.SEVERE,"Stage::::::::4");
			
			//   rahul added this for null pointer exp in table 
//			if(service.getTrackServiceTabledetails()!=null){
			tracktabledetails.setSrNo(service.getTrackServiceTabledetails().size()+1);
//			}
//			else
//			{
//				tracktabledetails.setSrNo(1);
//			}
			/**
			 * Updated By: Viraj
			 * Date: 02-03-2019
			 * Description: To store latitude and longitude
			 */
			tracktabledetails.setLatitude(lat);
			tracktabledetails.setLongitude(lon);
			/**
			 * Updated By: komal
			 * Date: 01-04-2019
			 * Description: To get and save location and time seperately
			 */
			tracktabledetails.setLocationName(location);
			if(!serviceActionTime.equals("")){
				tracktabledetails.setTime(serviceActionTime);

			}
			else{
				tracktabledetails.setTime(timeFormat.format(new Date()));
			}
			/** Ends **/
			service.getTrackServiceTabledetails().add(tracktabledetails);
			logger.log(Level.SEVERE,"Stage::::::::5"+service.getTrackServiceTabledetails().size());
			if(syncFlag){
				//Reschedule
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ServiceStatusAsPlanned",Long.parseLong(companyId))){
					service.setStatus(Service.SERVICESTATUSREOPEN);
					service.setServiceScheduled(false);
					service.setServiceNumber("");
				}
				org.json.JSONObject rescheduleHistory=new org.json.JSONObject(rescheduleData);
				ModificationHistory modHistory = new ModificationHistory();
				modHistory.setOldServiceDate(sdf.parse(rescheduleHistory.optString("previousServiceDate")));
				modHistory.setResheduleDate(sdf.parse(rescheduleHistory.optString("rescheduleDate")));

				// SimpleDateFormat sdf1=new
				// SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
				// sdf1.setTimeZone(TimeZone.getTimeZone("IST"));
				modHistory.setSystemDate(new Date());
				String[] time = rescheduleHistory.optString("rescheduleTime").trim().split(":");
				int hours = Integer.parseInt(time[0]);
				int mins = Integer.parseInt(time[1].substring(0, 2));
				if (hours >= 12) {
					int hoursDecrease = 0;
					if (hours != 12) {
						hoursDecrease = 12;
					}
					if(mins<10){
						modHistory.setResheduleTime((hours - hoursDecrease) + ":"+"0"+ mins + " PM");
					}else{
						modHistory.setResheduleTime((hours - hoursDecrease) + ":"
								+ mins + " PM");
					}
					
					logger.log(Level.SEVERE, "Service Time::::::"
							+ (hours - hoursDecrease) + ":" + mins + " PM");

				} else {
					logger.log(Level.SEVERE, "Service Time::::::" + hours + ":"
							+ mins + " AM");
					modHistory.setResheduleTime(hours + ":" + mins + " AM");
				}
				modHistory.setReason(rescheduleHistory.optString("rescheduledReason"));
				/** date 23.3.2019 added by komal to store employee name**/
				modHistory.setUserName(rescheduleHistory.optString("employeeName"));
//				List<ModificationHistory> modList=new ArrayList<ModificationHistory>();
//				modList.add(modHistory);
//				ModificationHistoryTable table = new ModificationHistoryTable();
//				table.getDataprovider().getList().add(modHistory);
				service.getListHistory().add(modHistory);
			}
			
			ofy().save().entity(service).now();
			
			

			//Added by Apeksha for sending notification and sms to customerr after service reported successfully.
			try{
//				sendMessageToCustomer(service);
				CustomerUser customerUser = ofy().load().type(CustomerUser.class)
						.filter("companyId", Long.parseLong(companyId))
						.filter("cinfo.count",service.getPersonInfo().getCount()).first().now();
				String response = "Service Reported";
				logger.log(Level.SEVERE,"AppConstants.IAndroid.EVA_KRETU:"+AppConstants.IAndroid.EVA_KRETO);
				
				SmsServiceImpl sms=new SmsServiceImpl();
				String event="";
				if(status.trim().equalsIgnoreCase(Service.SERVICESTATUSREPORTED)){
					event="Technician Reported";
				}else if(status.trim().equalsIgnoreCase(Service.SERVICESTATUSSTARTED)){
					event="Technician Started";
				}
				
				/**
				 * @author Anil @since 10-03-2021
				 * getting technician mobile no. from technician list in service 
				 */
				long technicianCellNo=0;
				if(service.getTechnicians()!=null&&service.getTechnicians().size()!=0){
					for(EmployeeInfo info:service.getTechnicians()){
						if(info.getFullName().equals(service.getEmployee())){
							if(info.getCellNumber()!=null){
								technicianCellNo=info.getCellNumber();
							}
							break;
						}
					}
				}
				
				if(technicianCellNo==0){
					Employee employee=ofy().load().type(Employee.class).filter("companyId",comp.getCompanyId()).filter("fullname", service.getEmployee()).first().now();
					if(employee!=null){
						if(employee.getCellNumber1()!=null){
							technicianCellNo=employee.getCellNumber1();
						}
					}
				}
				
				try{
					String smsResponse=sms.sendSMSFromTechnician(comp, comp.getCompanyId(), event, service.getPersonInfo().getFullName(), service.getEmployee(), fmt.format(service.getServiceDate()), service.getPersonInfo().getCellNumber(), service.getProductName(),technicianCellNo,service.getPersonInfo().getCount());
					logger.log(Level.SEVERE,"smsResponse"+smsResponse);
				}catch(Exception e){
					logger.log(Level.SEVERE,"error in sms"+e);
				}
				sendPushNotification(Long.parseLong(companyId),event, AppConstants.IAndroid.EVA_KRETO, 
						customerUser.getUserName().trim(), response,service);
			
			} catch(Exception e) {
				logger.log(Level.SEVERE,"Error::::::::"+e);
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"Stage::::::::6");
			processedServiceIdList.add(serviceId);
			logger.log(Level.SEVERE,serviceId+ "added to processedServiceIdList");
			}
			
		}//end of processed service id list if
	}catch(Exception e){
		e.printStackTrace();
		logger.log(Level.SEVERE,"Error::::::::"+e);
	}
		
 }
		}catch(Exception e){
			e.printStackTrace();
			logger.log(Level.SEVERE,"Error::::::::"+e);
			exep=e;
			resp.getWriter().print("Failed");
		}
		if(exep==null){
//			org.json.simple.JSONObject ServiceIdjObj = new org.json.simple.JSONObject();
//			ServiceIdjObj.put("ServiceIdList", ServiceIdjObj);
//			String jsonString = ServiceIdjObj.toJSONString().replaceAll("\\\\", "");
			resp.getWriter().print("Success");
		}
		
		
	}
	
	/*
	 * Date: 29 Jul 2017
	 * By: Apeksha Gunjal
	 * send Message to Custmer when Service is reported successfully. 
	 */
	
	private void sendMessageToCustomer(Service service) {
		// TODO Auto-generated method stub
		SmsServiceImpl smsServiceImpl = new SmsServiceImpl();
		smsServiceImpl.sendSmsToCustomers(null, null, null, null, null, false);
	}
	
	/*
	 * Date: 31 Jul 2017
	 * By: Apeksha Gunjal
	 * send Push Notification to Custmer when Service is reported successfully. 
	 */
	
	private void sendPushNotification(long companyId,String event, String applicationName,String customerUserName,String response, Service service) {
		if(event.trim().equalsIgnoreCase("Technician Reported")){
			response="event:Technician Reported"+"$"+service.getEmployee()+"has reported to your location for "+service.getProductName()+" service";
		}else if(event.trim().equalsIgnoreCase("Technician Started")){
			response="event:Technician Started"+"$"+service.getEmployee()+"has started to your location for "+service.getProductName()+" service";
		}
		;//getJSONResponse(service);
		CustomerUser customerUser=ofy().load().type(CustomerUser.class).filter("companyId",companyId).filter("cinfo.fullName",service.getPersonInfo().getFullName()).first().now(); 
		RegisterDevice device = ofy().load().type(RegisterDevice.class)
				.filter("companyId", companyId)
				.filter("applicationName", applicationName)
				.filter("userName", customerUser.getUserName()).first().now();
		String deviceName = device.getRegId().trim();
		logger.log(Level.SEVERE,"deviceName"+deviceName);
		URL url = null;
		try {
			url = new URL("https://fcm.googleapis.com/fcm/send");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HttpURLConnection conn = null;
		try {
			conn = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		conn.setDoOutput(true);
		try {
			conn.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		conn.setRequestProperty("Content-Type",
				"application/json; charset=UTF-8");
		conn.setRequestProperty("Authorization", "key="
				+ comp.getFcmServerKeyForKreto().trim());
		conn.setDoOutput(true);
		conn.setDoInput(true);

		try {
			conn.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "ProtocolException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		OutputStream os = null;
		try {
			os = conn.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
		// Log.d("Test", "From Get Post Method" + getPostData(values));
		try {
			writer.write(createJSONData(response, deviceName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException2 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		try {
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException3 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		InputStream is = null;
		try {
			is = conn.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String temp;
		String data = null;
		try {
			while ((temp = br.readLine()) != null) {
				data = data + temp;
			}
			logger.log(Level.SEVERE, "data data::::::::::" + data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		logger.log(Level.SEVERE, "Data::::::::::" + data);
	}
	
	
	private String createJSONData(String message, String deviceName) {
		// TODO Auto-generated method stub
		org.json.simple.JSONObject jObj1 = new org.json.simple.JSONObject();
		jObj1.put("message", message);

		org.json.simple.JSONObject jObj = new org.json.simple.JSONObject();
		jObj.put("data", jObj1);
		jObj.put("to", deviceName);

		String jsonString = jObj.toJSONString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE, "JSON Data Sent:::::::" + jsonString);

		return jsonString;
	}
}
