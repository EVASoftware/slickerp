package com.slicktechnologies.server.android;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.mindrot.jbcrypt.BCrypt;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.android.RegisterDevice;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.LicenseDetails;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;

public class LoginStatusCheckServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5880847530973762354L;

	/**
	 * At the time of registration of all apps this API is called.
	 */

	Logger logger = Logger.getLogger("LoginStatusCheckServlet.class");

	Company comp;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
//		resp.setHeader("Access-Control-Allow-Origin", AppConstants.TRENDZWEBSOLUTION);
		resp.setHeader("Access-Control-Allow-Origin", "*");
		RegisterServiceImpl impl = new RegisterServiceImpl();
		String urlCalled = req.getRequestURL().toString().trim();
		String url;
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		Exception exep = null;
		try {

			/*
			 * Date: 31 Jul 2017 By: Apeksha Gunjal Saving RegisterDevice items
			 */
			logger.log(Level.SEVERE, "accessUrl " + splitUrl[0]);
			comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
			
			/**
			 * @author Anil @since 14-06-2021
			 * Thai character was not getting download in proper format
			 * raised by Rahul Tiwari
			 */
//			resp.setContentType("text/plain");
			resp.setContentType("text/plain; charset=UTF-8");
			resp.setCharacterEncoding("UTF-8");
			
			String username = req.getParameter("username").trim();
			String password = req.getParameter("password").trim();
			String companyIdString = comp.getCompanyId() + "";
			String regId = req.getParameter("regId").trim();
			String actionTask = req.getParameter("actionTask").trim();
			String applicationName = req.getParameter("applicationName").trim();
			String imeiNumber = req.getParameter("imeiNumber").trim();

			long companyId = Long.parseLong(companyIdString);
			logger.log(Level.SEVERE, "actionTask " + actionTask);
			// If any other actionTask is called then add else condition here..
			if (actionTask.trim().equalsIgnoreCase("DeRegisterDevice")) {
				logger.log(Level.SEVERE, "imei number" + imeiNumber.trim());
				logger.log(Level.SEVERE, "app" + applicationName);
				logger.log(Level.SEVERE, "companyId" + companyId);
				RegisterDevice register = ofy().load().type(RegisterDevice.class).filter("companyId", comp.getCompanyId())
						.filter("imeiNumber", Long.parseLong(imeiNumber.trim())).filter("status", true).filter("applicationName", applicationName).first().now();
				logger.log(Level.SEVERE, "deregister:::::::::" + register);
				if (register != null) {
					register.setStatus(false);
					register.setDeactivatedBy(username);
					register.setDeactivationDate(new Date());
					ofy().save().entity(register);
					resp.getWriter().println("Application Deregistered Successfully.");
					return;
				}
				resp.getWriter().println("Application Deregistration Failed.");
				return;
			}
			logger.log(Level.SEVERE, "username" + username);
			logger.log(Level.SEVERE, "password" + password);
			logger.log(Level.SEVERE, "companyId" + companyId);
			logger.log(Level.SEVERE, "imei number" + imeiNumber.trim());
			logger.log(Level.SEVERE, "app" + applicationName);
			User user = null;
			// If encrypted
			user = ofy().load().type(User.class).filter("companyId", companyId)
					.filter("userName", username.trim()).first().now();

			if (user == null) {
				resp.getWriter().println("Invalid UserName.");
				return;
			}
			boolean status;
			/**
			 * @author Anil, Date : 11-02-2020
			 */
			status = checkUserLogin(user, username, password, companyId);
			if (status == false) {
				resp.getWriter().println("Invalid user credential.");
				return;
			}
			
			if (user.getRoleName().equalsIgnoreCase("Admin")
					|| user.getRoleName().equalsIgnoreCase("App Register")) {
				
				String returnValue = validateLicenseDate(companyId, user,applicationName, imeiNumber);

				if (!returnValue.equalsIgnoreCase("Success")) {
					resp.getWriter().println(returnValue);
					return;
				}
				if (status&& actionTask.trim().equalsIgnoreCase("RegisterDevice")) {
					// Remaining Task : Add Application Name

					RegisterDevice registerDeviceFound = new RegisterDevice();
					registerDeviceFound = ofy().load().type(RegisterDevice.class)
							.filter("companyId", comp.getCompanyId())
							.filter("imeiNumber",Long.parseLong(imeiNumber.trim()))
							.filter("status", true)
							.filter("applicationName", applicationName).first().now();
					
					logger.log(Level.SEVERE, "registerDeviceFound:::::::::"+ registerDeviceFound);

					// if(applicationName.trim().equalsIgnoreCase("EVA Pedio")){
					// comp.getPedioLicense()
					// }
					// if(registerDeviceFound==null){
					RegisterDevice registerDevice = new RegisterDevice();
					NumberGeneration ng = new NumberGeneration();
					ng = ofy().load().type(NumberGeneration.class)
							.filter("companyId", comp.getCompanyId())
							.filter("processName", "RegisterDevice")
							.filter("status", true).first().now();

					long number = ng.getNumber();
					int count = (int) number;
					ng.setNumber(count + 1);
					ofy().save().entity(ng);
					registerDevice.setCount(count + 1);
					registerDevice.setImeiNumber(Long.parseLong(imeiNumber));
					registerDevice.setUserName(user.getUserName().trim());
					registerDevice.setRegId(regId);
					registerDevice.setCompanyId(companyId);
					registerDevice.setApplicationName(applicationName);
					registerDevice.setEmployeeName(user.getEmployeeName());
					registerDevice.setStatus(true);
					ofy().save().entity(registerDevice);

					// registerDeviceForNotifications(registerDevice,splitUrl[0]);
					// }else{
					if (registerDeviceFound != null) {
						registerDeviceFound.setCompanyId(companyId);
						registerDeviceFound.setImeiNumber(Long.parseLong(imeiNumber));
						registerDeviceFound.setUserName(user.getUserName().trim());
						registerDeviceFound.setRegId(regId);
						registerDeviceFound.setApplicationName(applicationName);
						registerDeviceFound.setEmployeeName(user.getEmployeeName());
						registerDeviceFound.setStatus(false);
						ofy().save().entity(registerDeviceFound);
						// registerDeviceForNotifications(registerDeviceFound,splitUrl[0]);
					}
					// }
				}
			} else {
				status = false;
			}
			if (status) {
				resp.getWriter().println("Success");
			} else {
				resp.getWriter().println("Failed");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "ERROR---->" + e);
		}
	}

	private String registerDeviceForNotifications(RegisterDevice registerDevice, String companyUrl) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE, "This is where i will call invoice URL");
		final String URL = "http://my.evaesserp.appspot.com/slick_erp/registerInNotificationsOfEVA?data="
				+ createJSONObject(registerDevice, companyUrl);
		String data = "";

		URL url = null;
		try {
			url = new URL(URL);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "MalformedURLException ERROR::::::::::"
					+ e);
			e.printStackTrace();
		}
		HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		con.setDoInput(true);
		con.setDoOutput(true);
		con.setRequestProperty("Content-Type",
				"application/json; charset=UTF-8");
		try {
			con.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "ProtocolException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		OutputStream os = null;
		try {
			os = con.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
		// Log.d("Test", "From Get Post Method" + getPostData(values));
		// try {
		// writer.write();
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// logger.log(Level.SEVERE,"IOException2 ERROR::::::::::"+e);
		// e.printStackTrace();
		// }
		try {
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException3 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		InputStream is = null;
		try {
			is = con.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String temp;

		try {
			while ((temp = br.readLine()) != null) {
				data = data + temp;
			}
			logger.log(Level.SEVERE, "data data::::::::::" + data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		logger.log(Level.SEVERE, "Data::::::::::" + data);

		return data;

	}

	private String createJSONObject(RegisterDevice registerDevice,
			String companyUrl) {
		// TODO Auto-generated method

		JSONObject obj = new JSONObject();
		try {
			obj.put("count", registerDevice.getCount());
		} catch (JSONException e7) {
			// TODO Auto-generated catch block
			e7.printStackTrace();
		}
		try {
			obj.put("imeiNumber", registerDevice.getImeiNumber());
		} catch (JSONException e6) {
			// TODO Auto-generated catch block
			e6.printStackTrace();
		}
		try {
			obj.put("userName", registerDevice.getUserName());
		} catch (JSONException e5) {
			// TODO Auto-generated catch block
			e5.printStackTrace();
		}
		try {
			obj.put("regId", registerDevice.getRegId());
		} catch (JSONException e4) {
			// TODO Auto-generated catch block
			e4.printStackTrace();
		}
		try {
			obj.put("companyId", registerDevice.getCompanyId());
		} catch (JSONException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		try {
			obj.put("applicationName", registerDevice.getApplicationName());
		} catch (JSONException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		try {
			obj.put("employeeName", registerDevice.getEmployeeName());
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			obj.put("companyUrl", companyUrl);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String jsonString = obj.toString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE, "Invoice Data::" + jsonString);
		return jsonString;
	}

	public boolean checkUserLogin(User user, String username, String password,long companyId) {

		if (user.getStatus()) {
			if (user != null) {
				if (user.getPassword().trim().equals(password.trim())) {
					return true;
				} else {
					boolean matched = BCrypt.checkpw(password.trim(),user.getPassword());
					logger.log(Level.SEVERE, "Login matched value:::::"+ matched);
					if (matched) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public String validateLicenseDate(Long companyId, User userEntity,String applicationName, String imeiNumber) {

		String returnValue = "Success";
		Company c = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		Date todaydate = new Date();
		// System.out.println("LOGIN DATE : "+AppUtility.parseDate(todaydate));
		// Console.log("LOGIN DATE : "+AppUtility.parseDate(todaydate));
		logger.log(Level.SEVERE, "APPLICATION NAME "+ applicationName);
		ArrayList<LicenseDetails> licenseDetailsList = new ArrayList<LicenseDetails>();
		if (c.getLicenseDetailsList() != null&& c.getLicenseDetailsList().size() != 0) {
			logger.log(Level.SEVERE, "TOTAL EVA LICENSE LIST : "+ c.getLicenseDetailsList().size());
			// Console.log("TOTAL EVA LICENSE LIST : "+c.getLicenseDetailsList().size());
			for (LicenseDetails license : c.getLicenseDetailsList()) {
				if (applicationName.contains("Pedio")) {
					if (AppConstants.LICENSETYPELIST.EVA_PEDIO_LICENSE
							.equals(license.getLicenseType())
							&& license.getStatus() == true) {
						licenseDetailsList.add(license);
					}
				} else if (applicationName.contains("Priora")) {
					if (AppConstants.LICENSETYPELIST.EVA_PRIORA_LICENSE
							.equals(license.getLicenseType())
							&& license.getStatus() == true) {
						licenseDetailsList.add(license);
					}
				} else if (applicationName.contains("Kreto")) {
					if (AppConstants.LICENSETYPELIST.EVA_KRETO_LICENSE
							.equals(license.getLicenseType())
							&& license.getStatus() == true) {
						licenseDetailsList.add(license);
					}
				} else if (applicationName.contains("Attendance")) {
					if (AppConstants.LICENSETYPELIST.EVA_ATTENDANCE_LICENSE
							.equals(license.getLicenseType())
							&& license.getStatus() == true) {
						licenseDetailsList.add(license);
					}
				}
				/**
				 * @author Anil
				 * @since 07-05-2020
				 * For PTSPL Services login
				 */
				else if (applicationName.contains("PTSPL")) {
					if (AppConstants.LICENSETYPELIST.EVA_PTSPL_LICENSE
							.equals(license.getLicenseType())
							&& license.getStatus() == true) {
						licenseDetailsList.add(license);
					}
				}

			}
			logger.log(Level.SEVERE, "ACTIVE EVE ERP LICENSE LIST SIZE : "+ licenseDetailsList.size());
		}
		/**
		 * @author Anil , Date : 11-02-2020
		 */
		else{
			return "No licenses are allocated to you contact support.";
		}

		Comparator<LicenseDetails> licDtComp = new Comparator<LicenseDetails>() {
			@Override
			public int compare(LicenseDetails arg0, LicenseDetails arg1) {
				return arg0.getStartDate().compareTo(arg1.getStartDate());
			}
		};
		Collections.sort(licenseDetailsList, licDtComp);

		int noOfLicense = 0;
		int expiredLicenseCount = 0;
		int notActiveLicenseCount = 0;
		boolean validLicense = false;
		if (licenseDetailsList.size() != 0) {
			for (LicenseDetails license : licenseDetailsList) {
				logger.log(Level.SEVERE, license.getLicenseType()
						+ " No. Of License : " + license.getNoOfLicense()
						+ " LICENSE START DATE : " + license.getStartDate()
						+ " LICENSE END DATE : " + (license.getEndDate()));
				
				SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
				logger.log(Level.SEVERE,"Today="+sdf.format(todaydate)+"license start day="+sdf.format(license.getStartDate()));
				//Ashwini Patil Date:20-12-2022 added one more condition so that license should work from the currect day
				if (todaydate.after(license.getStartDate())||sdf.format(todaydate).equals(sdf.format(license.getStartDate()))) {
					if (todaydate.before(license.getEndDate())) {
						noOfLicense = noOfLicense + license.getNoOfLicense();
						validLicense = true;
					} else {
						expiredLicenseCount++;
					}
				} else {
					notActiveLicenseCount++;
				}
			}

			// Console.log("TOTAL NO. OF ACTIVE LICENSE : "+noOfLicense);
			logger.log(Level.SEVERE, "TOTAL NO. OF ACTIVE LICENSE : "
					+ noOfLicense);
			c.setNoOfUser(noOfLicense);
			if (validLicense) {
				returnValue = validateNumberOfUsers(c.getCompanyId(),
						userEntity, c.getNoOfUser(), applicationName,
						imeiNumber);
			} else if (validLicense == false && notActiveLicenseCount > 0) {

				returnValue = "License is not Active. Please contact support@evasoftwaresolutions.com";
			} else if (validLicense == false && expiredLicenseCount > 0) {

				returnValue = "License Expired. Please contact support@evasoftwaresolutions.com";
			}
		}
		/**
		 * @author Anil , Date : 11-02-2020
		 */
		else{
			return "No licenses are allocated to you contact support.";
		}
		return returnValue;
	}

	public String validateNumberOfUsers(Long companyId, User userEntity,
			int noOfUser, String appliCationName, String imeiNumber) {

		List<RegisterDevice> registerDeviceList = ofy().load()
				.type(RegisterDevice.class).filter("companyId", companyId)
				.filter("status", true)
				.filter("applicationName", appliCationName).list();
		List<RegisterDevice> rDeviceList = new ArrayList<RegisterDevice>();
		if (registerDeviceList != null && registerDeviceList.size() > 0) {
			for (RegisterDevice rDevice : registerDeviceList) {
				if ((rDevice.getImeiNumber() + "").equalsIgnoreCase(imeiNumber)) {
				} else {
					rDeviceList.add(rDevice);
				}
			}
		}
		if (rDeviceList != null && rDeviceList.size() >= noOfUser) {
			return "Number of licenses exceeded , Please contact system administrator.";

		}

		return "Success";
	}
}
