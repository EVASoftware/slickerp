package com.slicktechnologies.server.android.customerapp;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class FetchContractRenewalData extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5049155234790016721L;
	Logger logger = Logger.getLogger("FetchContractRenewalData.class");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Gson gson = new Gson();
		// TODO Auto-generated method stub
		resp.setContentType("text/plain");

		RegisterServiceImpl registerService = new RegisterServiceImpl();
		registerService.getClass();

		String customerId = req.getParameter("customerId").trim();

		String urlCalled = req.getRequestURL().toString().trim();
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl[0]" + splitUrl[0]);

		Company company = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		logger.log(Level.SEVERE, "company" + company);
		Calendar c = Calendar.getInstance();
		try {
			c.setTime(new Date());
		} catch (Exception e) {
			e.printStackTrace();
		}
		c.add(Calendar.DATE, 30); // number of days to add, can also use
									// Calendar.DAY_OF_MONTH in place of
									// Calendar.DATE
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MMM/yyyy");
		Date toDate = null;
		try {
			toDate = sdf1.parse(sdf1.format(c.getTime()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"to Date "+toDate);
		List<Contract> contractRenewalList = ofy().load().type(Contract.class)
				.filter("companyId", company.getCompanyId())
				.filter("cinfo.count", Integer.parseInt(customerId))
				.filter("endDate <=", toDate).list();
		logger.log(Level.SEVERE,"contractRenewalList"+contractRenewalList+"contractRenewalList size"+contractRenewalList.size());
		
		JSONArray jsonArray = new JSONArray();
		for (Contract contractRenewal : contractRenewalList) {
			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject.put("contractId", contractRenewal.getCount());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log(Level.SEVERE,"Error Contract Id::"+e);
			}
			try {
				jsonObject.put("docId", contractRenewal.getCount());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log(Level.SEVERE,"Error docId::"+e);
			}
			try {
				jsonObject.put("netPayable", contractRenewal.getNetpayable());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log(Level.SEVERE,"netPayable::"+e);
			}
			try {
				jsonObject.put("productList",
						new JSONArray(gson.toJson(contractRenewal.getItems())));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log(Level.SEVERE,"productList::"+e);
			}
			SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
			sdfDate.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			try {
				jsonObject.put("contractEndDate",
						sdfDate.format(contractRenewal.getEndDate()));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log(Level.SEVERE,"contractEndDate::"+e);
			}
			try {
				jsonObject.put("contractStartDate",
						sdfDate.format(contractRenewal.getStartDate()));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log(Level.SEVERE,"contractStartDate::"+e);
			}

			jsonArray.put(jsonObject);
		}

		String jsonString = jsonArray.toString().replace("\\\\", "");
		logger.log(Level.SEVERE, jsonString);
		resp.getWriter().print(jsonString);

	}
}
