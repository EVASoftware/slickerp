package com.slicktechnologies.server.android.customerapp;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.server.utility.ServerAppUtility;

public class FetchCustomerContractDetails extends HttpServlet {


	/**
	 * 
	 */
	private static final long serialVersionUID = -8959780518244310311L;

	/**
	 * 
	 */

	Logger logger = Logger.getLogger("FetchCustomerContractDetails.class");

	RegisterServiceImpl registerService;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Gson gson = new Gson();
		resp.setContentType("text/plain");

		String url = req.getParameter("url").trim();
		String[] urlArray = url.split("\\.");
//String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		String accessUrl = urlArray[0];
		logger.log(Level.SEVERE, "accessUrl" + accessUrl);
		String customerId = req.getParameter("customerId").trim();

		registerService = new RegisterServiceImpl();
		registerService.getClass();

		Company company = ofy().load().type(Company.class)
				.filter("accessUrl", accessUrl).first().now();
		logger.log(Level.SEVERE, "company" + company);
		List<Contract> contractList = new ArrayList<Contract>();
		contractList = ofy().load().type(Contract.class)
				.filter("companyId", company.getCompanyId())
				.filter("cinfo.count", Integer.parseInt(customerId))
				.filter("status", Contract.APPROVED).list();

		JSONArray jArray = new JSONArray();
		for (Contract contract : contractList) {

			JSONObject jObj = new JSONObject();
			SimpleDateFormat spf = new SimpleDateFormat("dd/MMM/yyyy");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			spf.setTimeZone(TimeZone.getTimeZone("IST"));
			jObj.put("contractId", contract.getCount());
			jObj.put("renewDate", spf.format(contract.getEndDate()));
			jObj.put("startDate", spf.format(contract.getStartDate()));
			jObj.put("endDate", spf.format(contract.getEndDate()));
			jObj.put("id", contract.getId());
			jObj.put("status",contract.getStatus());
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			if(contract.getCreationDate()!=null){
				jObj.put("creationDate",sdf.format(contract.getCreationDate()));
			}else{
				jObj.put("creationDate","");	
			}
			try {
				jObj.put("productList",
						new org.json.JSONArray(gson.toJson(contract.getItems())));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			jObj.put("renewFlag", contract.isRenewFlag());
//			jObj.put("serviceDetailsArray", jArray2);
			jObj.put("netPayable", contract.getNetpayable());
			jArray.add(jObj);
		}
		String jsonString = jArray.toJSONString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE, jsonString);
		resp.getWriter().println(jsonString);

	}

}
