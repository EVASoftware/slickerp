package com.slicktechnologies.server.android.customerapp;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.server.utility.ServerAppUtility;

public class FetchCustomerInvoiceDetails extends HttpServlet{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3865006138433681798L;
	/**
	 * 
	 */
	RegisterServiceImpl registerService;
	/**
	 * 
	 */
	Logger logger = Logger.getLogger("FetchCustomerInvoiceDetails.class");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("text/plain");
		registerService = new RegisterServiceImpl();
		registerService.getClass();
		
		//	String url = req.getParameter("url").trim();
//		String[] urlArray = url.split("\\.");
	String urlCalled=req.getRequestURL().toString().trim();
	String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		String accessUrl = splitUrl[0];
		logger.log(Level.SEVERE, "accessUrl" + accessUrl);
		String customerId = req.getParameter("customerId").trim();
		
		
		Company company = ofy().load().type(Company.class)
				.filter("accessUrl", accessUrl).first().now();
		logger.log(Level.SEVERE, "company" + company);
		List<CustomerPayment> invoiceList = new ArrayList<CustomerPayment>();
//		invoiceList = ofy().load().type(Invoice.class)
//				.filter("companyId", company.getCompanyId())
//				.filter("personInfo.count", Integer.parseInt(customerId))
//				.filter("status", Contract.APPROVED).list();
		ArrayList<String> statusList=new ArrayList<String>();
		statusList.add(CustomerPayment.CREATED);
		statusList.add(CustomerPayment.CLOSED);
		
		invoiceList = ofy().load().type(CustomerPayment.class)
				.filter("companyId", company.getCompanyId())
				.filter("personInfo.count", Integer.parseInt(customerId))
				.filter("status IN",statusList).list();
		
		JSONArray jArray = new JSONArray();
		for (CustomerPayment invoice : invoiceList) {
			JSONObject jObj = new JSONObject();
			SimpleDateFormat spf = new SimpleDateFormat("dd/MMM/yyyy");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			spf.setTimeZone(TimeZone.getTimeZone("IST"));
			Invoice invoiceDetails=ofy().load().type(Invoice.class).filter("companyId", invoice.getCompanyId()).filter("count", invoice.getInvoiceCount()).first().now();
			jObj.put("invoiceUniqueId", invoiceDetails.getId());
			jObj.put("invoiceID", invoice.getInvoiceCount());
			jObj.put("invoiceDate", spf.format(invoice.getInvoiceDate()).trim());
			jObj.put("contractId", invoice.getContractCount());
			jObj.put("invoiceAmt", invoice.getInvoiceAmount());
			jObj.put("uniqueId", invoice.getId());
			jObj.put("status", invoice.getStatus());
			jArray.add(jObj);
		}
		
		String jsonString = jArray.toJSONString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE, jsonString);
		resp.getWriter().println(jsonString);
		
	}
}
