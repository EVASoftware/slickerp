package com.slicktechnologies.server.android.customerapp.operations;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.simple.JSONObject;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.ContractServiceImplementor;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.ModificationHistory;
import com.slicktechnologies.shared.common.android.RegisterDevice;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.helperlayer.CustomerUser;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;

public class CustomerAppOperations extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 456866449475476581L;
	/**
	 * 
	 */
	Logger logger = Logger.getLogger("RescheduleServiceDataUpload.class");
	Company comp;

	SimpleDateFormat sdf;
	
	int contractPeriod=0;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doGet(req, resp);
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doPost(req, resp);
		RegisterServiceImpl registerServiceImpl = new RegisterServiceImpl();
		String actionTask = req.getParameter("actionTask");
		sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

		logger.log(Level.SEVERE, "actionTask::::::::" + actionTask);
		
		if (actionTask.trim().equalsIgnoreCase("RescheduleService")) {
			Object rescheduleFlag = rescheduleServices(req, resp);
			if (rescheduleFlag instanceof Boolean) {
				if ((boolean) rescheduleFlag) {
					resp.getWriter().println("Success");
				} else {
					resp.getWriter().println("Failed");
				}
			} else if (rescheduleFlag instanceof String) {
				resp.getWriter().println(rescheduleFlag.toString());
			}
		} else if (actionTask.trim().equalsIgnoreCase("ComplainService")) {
			Object complainFlag = complainServices(req, resp);
			if (complainFlag instanceof Boolean) {
				if ((boolean) complainFlag) {
					resp.getWriter().println("Success");
				} else {
					resp.getWriter().println("Failed");
				}
			} else if (complainFlag instanceof String) {
				resp.getWriter().println(complainFlag.toString());
			}
		} else if (actionTask.trim().equalsIgnoreCase("LoginService")) {
			Object loginFlag = loginService(req, resp);
			if (loginFlag instanceof CustomerUser
					|| loginFlag instanceof Boolean) {
				if (loginFlag instanceof CustomerUser) {
					updateDeviceRegistration(req, resp, (CustomerUser) loginFlag);
					String jsonString = getCustomerDetails((CustomerUser) loginFlag);
					resp.getWriter().println(jsonString);
				} else {
					resp.getWriter().println("Failed");
				}
			} else if (loginFlag instanceof String) {
				resp.getWriter().println(loginFlag.toString());
			}
		} else if (actionTask.trim().equalsIgnoreCase("Feedback")) {
			Object loginFlag = serviceFeedback(req, resp);
			if (loginFlag instanceof Boolean) {
				if ((boolean) loginFlag) {
					// String
					// jsonString=getCustomerDetails((CustomerUser)loginFlag);
					resp.getWriter().println("Success");
				} else {
					resp.getWriter().println("Failed");
				}
			} else if (loginFlag instanceof String) {
				resp.getWriter().println(loginFlag.toString());
			}
		} else if (actionTask.trim().equalsIgnoreCase("CreateLead")) {
			Object loginFlag = createLead(req, resp);
			if (loginFlag instanceof Boolean) {
				if ((boolean) loginFlag) {
					// String
					// jsonString=getCustomerDetails((CustomerUser)loginFlag);
					resp.getWriter().println("Success");
				} else {
					resp.getWriter().println("Failed");
				}
			} else if (loginFlag instanceof String) {
				resp.getWriter().println(loginFlag.toString());
			}
		} else if (actionTask.trim().equalsIgnoreCase("RenewContract")) {
			Object loginFlag = renewContract(req, resp);
			if (loginFlag instanceof Boolean) {
				if ((boolean) loginFlag) {
					// String
					// jsonString=getCustomerDetails((CustomerUser)loginFlag);
					resp.getWriter().println("Success");
				} else {
					resp.getWriter().println("Failed");
				}
			} else if (loginFlag instanceof String) {
				resp.getWriter().println(loginFlag.toString());
			}
		} else if (actionTask.trim().equalsIgnoreCase("PaymentSubmit")) {
			Object loginFlag = submitPaymentDocument(req, resp);
			if (loginFlag instanceof Boolean) {
				if ((boolean) loginFlag) {
					// String
					// jsonString=getCustomerDetails((CustomerUser)loginFlag);
					resp.getWriter().println("Success");
				}
			} else if (loginFlag instanceof String) {
				resp.getWriter().println(loginFlag.toString());
			} else {
				resp.getWriter().println("Failed");
			}
		} else if (actionTask.trim().equalsIgnoreCase("RegisterDevice")) {
			// Added by Apeksha to save RegisterDevice data on server
			Object registerDeviceFlag = submitRegisterDetails(req, resp);
			if (registerDeviceFlag instanceof Boolean) {
				if ((boolean) registerDeviceFlag) {
					// String
					resp.getWriter().println("Success");
				} else {
					resp.getWriter().println("Failed");
				}
			} else if (registerDeviceFlag instanceof String) {
				resp.getWriter().println(registerDeviceFlag.toString());
			}

		}else if(actionTask.trim().equalsIgnoreCase("requestNewService")){
			Object newContract_Service = createNewContract_Service(req, resp);
			if (newContract_Service instanceof Boolean) {
				if ((boolean) newContract_Service) {
					// String
					resp.getWriter().println("Success");
				} else {
					resp.getWriter().println("Failed");
				}
			} else if (newContract_Service instanceof String) {
				resp.getWriter().println(newContract_Service.toString());
			}
		}else if(actionTask.trim().equalsIgnoreCase("fetchComplainList")){
			Object fetchComplainList = fecthComplainList(req, resp);
			if (fetchComplainList instanceof Boolean) {
				if ((boolean) fetchComplainList) {
					// String
					resp.getWriter().println("Success");
				} else {
					resp.getWriter().println("Failed");
				}
			} else if (fetchComplainList instanceof String) {
				resp.getWriter().println(fetchComplainList.toString());
			}
		}else if(actionTask.trim().equalsIgnoreCase("getScheduleList")){
			Object fecthServicesList = fecthServices(req, resp);
			if (fecthServicesList instanceof Boolean) {
				if ((boolean) fecthServicesList) {
					// String
					resp.getWriter().println("Success");
				} else {
					resp.getWriter().println("Failed");
				}
			} else if (fecthServicesList instanceof String) {
				resp.getWriter().println(fecthServicesList.toString());
			}
		}
	}

	private Object fecthServices(HttpServletRequest req,
			HttpServletResponse resp) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		String response = "";
		String urlCalled = req.getRequestURL().toString().trim();
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		String contractCount=req.getParameter("contractId");
		Exception exep = null;
		comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		List<Service> serviceList=ofy().load().type(Service.class).filter("companyId", comp.getCompanyId()).filter("contractCount",Integer.parseInt(contractCount)).list();
		org.json.simple.JSONArray jArray = new org.json.simple.JSONArray();
		for (Service service : serviceList) {
			JSONObject jObj = new JSONObject();
			jObj.put("serviceId", service.getCount());
			jObj.put("contractId", service.getContractCount());
			jObj.put("serviceName", service.getProductName().trim());
			jObj.put("status", service.getStatus().trim());
			jObj.put("serviceAddress", service.getAddress().getCompleteAddress());
			SimpleDateFormat spf = new SimpleDateFormat("dd/MMM/yyyy");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			spf.setTimeZone(TimeZone.getTimeZone("IST"));
			logger.log(Level.SEVERE, "service Date::::" + service.getCount()
					+ "service Date:::" + spf.format(service.getServiceDate()));
			jObj.put("serviceDate", spf.format(service.getServiceDate()));
			jObj.put("serviceTime", service.getServiceTime());
			if(service.getEmployee()!=null){
				Employee employee=ofy().load().type(Employee.class).filter("companyId", service.getCompanyId()).filter("fullname",service.getEmployee().trim()).first().now();
				if(employee!=null){
					jObj.put("employeeName", employee.getFullname());
					
				if(employee.getPhoto()!=null){
					jObj.put("employeeProfile", employee.getPhoto().getUrl());
					
				}else{
					jObj.put("employeeProfile", "No Profile Photo");
				}
				if(employee.getCellNumber1()!=null){
					jObj.put("employeeMobile",employee.getCellNumber1());
				}else{
					jObj.put("employeeMobile",0);
				}
				}
				}
			jArray.add(jObj);
		}
		String jsonString=jArray.toJSONString().replaceAll("\\\\", "");
		return jsonString;
	}

	private Object fecthComplainList(HttpServletRequest req,
			HttpServletResponse resp) {
		// TODO Auto-generated method stub
		String response = "";
		String urlCalled = req.getRequestURL().toString().trim();
		String url = urlCalled.replace("http://", "");
		String[] splitUrl = url.split("\\.");
		Exception exep = null;
		comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		
		String customerId=req.getParameter("customerId");
		List<Complain> complainList=ofy().load().type(Complain.class).filter("companyId", comp.getCompanyId()).filter("personinfo.count", Long.parseLong(customerId)).list();
		org.json.JSONArray jArray=new org.json.JSONArray();
		for (Complain complain : complainList) {
			JSONObject obj=new JSONObject();
			//Added by Apeksha Gunjal on 26/07/2018 @19:00
			try{
				logger.log(Level.SEVERE, "product by pic::::::::" + complain.getPic().getProductName());
				if(complain.getPic().getProductName() == null)
					obj.put("serviceName",""+complain.getProductName());
				else
					obj.put("serviceName",""+complain.getPic().getProductName());
			}catch(Exception e){
				obj.put("serviceName","");
				logger.log(Level.SEVERE, "Error complain set product::::::::" + e.getMessage());	
			}
			obj.put("ticketId",complain.getCount());
			obj.put("description",complain.getDestription());
			obj.put("status",complain.getCompStatus());
			jArray.put(obj);
		}
		String jsonString=jArray.toString().replaceAll("\\\\", "");
		return jsonString;
	}

	/*
	 * Date: 31 Jul 2017
	 * By: Apeksha Gunjal
	 * To update imei Number and other register details for PushNotification purpose. 
	 */
	
	private Object createNewContract_Service(HttpServletRequest req,
			HttpServletResponse resp) {
		String response = "";
		String urlCalled = req.getRequestURL().toString().trim();
		String url = urlCalled.replace("http://", "");
		String[] splitUrl = url.split("\\.");
		Exception exep = null;
		comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		
		/**Comp for local*/
		if(comp==null){
			comp = ofy().load().type(Company.class)
					.filter("companyId", Long.parseLong("5348024557502464")).first().now();
		}
		String customerCount=req.getParameter("customerId").trim();
		logger.log(Level.SEVERE,"customerCount--->>>"+customerCount);
		String customerCellNo=req.getParameter("cellNo").trim();
		logger.log(Level.SEVERE,"customerCellNo--->>>"+customerCellNo);
		String customerName=req.getParameter("customerName").trim();
		logger.log(Level.SEVERE,"customerName--->>>"+customerName);
		String addressArray=req.getParameter("addressArray").trim();
		logger.log(Level.SEVERE,"addressArray--->>>"+addressArray);
		String productArray=req.getParameter("productArray").trim();
		logger.log(Level.SEVERE,"productArray--->>>"+productArray);
		String contractDate=req.getParameter("contractDate").trim();
		logger.log(Level.SEVERE,"contractDate--->>>"+contractDate);
		String paymentMethod="Cash";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		Customer customer=null;
		if(comp!=null){
			customer=ofy().load().type(Customer.class).filter("companyId",comp.getCompanyId()).filter("count", Integer.parseInt(customerCount)).first().now();
		}else{
			customer=ofy().load().type(Customer.class).filter("companyId",Long.parseLong("5348024557502464")).filter("count", Integer.parseInt(customerCount)).first().now();
		}
		Contract contract=new Contract();
		PersonInfo personInfo=new PersonInfo();
		personInfo.setCount(customer.getCount());
		personInfo.setFullName(customer.getFullname());
		personInfo.setCellNumber(customer.getCellNumber1());
		contract.setCreditPeriod(0);
		contract.setDescription("");
		contract.setPaymentMethod(paymentMethod);
		contract.setCompanyId(comp.getCompanyId());
		contract.setCinfo(personInfo);
		contract.setBranch(customer.getBranch());
		contract.setBranch(customer.getBranch());
		contract.setStatus(Contract.APPROVED);
		contract.setEmployee(customer.getEmployee());
		try {
			contract.setCreationDate(sdf.parse(sdf.format(new Date())));
		} catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		try {
			contract.setContractDate(sdf.parse(contractDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		try {
			contract.setStartDate(sdf.parse(contractDate));
		} catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		/**Android Percentage*/
		PaymentTerms payTerms=new PaymentTerms();
		payTerms.setPayTermDays(0);
		payTerms.setPayTermPercent(100.00);
		payTerms.setPayTermComment("By Default");
		
		List<PaymentTerms> listPaymentTerms=new ArrayList<PaymentTerms>();
		listPaymentTerms.add(payTerms);
		contract.setPaymentTermsList(listPaymentTerms);
		contract.setQuotationCount(-1);
		contract.setTicketNumber(-1);
		contract.setLeadCount(-1);
		/**Android Percentage Done**/
		logger.log(Level.SEVERE,"productArray--->"+req.getParameter("productArray"));
		
		/**Adding Product**/
		JSONArray jsonArray = null;
		try {
			String productArrayString="";
			if(req.getParameter("productArray").trim().contains("#and")){
				productArrayString=req.getParameter("productArray").trim().replace("#and", "&");
			}else{
				productArrayString=req.getParameter("productArray").trim();
			}
			jsonArray = new JSONArray(productArrayString);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		List<Integer> intIdList=new ArrayList<Integer>();
		List<Integer> intNoOfServicesList=new ArrayList<Integer>();
		List<Double> priceOfServicesList=new ArrayList<Double>();
		
		for (int i = 0; i < jsonArray.length(); i++) {
			org.json.JSONObject obj = null;
			try {
				obj = jsonArray.getJSONObject(i);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			intIdList.add(Integer.parseInt(obj.optString("id").trim()));
			intNoOfServicesList.add(Integer.parseInt(obj.optString("noOfServices").trim()));
			priceOfServicesList.add(Double.parseDouble(obj.optString("price").trim()));
		}
		/**Group/Category/Type**/
		contract.setCategory("");
		contract.setGroup("");
		contract.setType("");
		
		/**Done Adding Group/Category/Type**/
		List<ServiceProduct> serviceProductList=null;
		if(comp!=null){
			serviceProductList=ofy().load().type(ServiceProduct.class).filter("companyId", comp.getCompanyId()).filter("count IN",intIdList).list();
		}else{
			serviceProductList=ofy().load().type(ServiceProduct.class).filter("companyId", Long.parseLong("5348024557502464")).filter("count IN",intIdList).list();
		}
		List<SalesLineItem> listofSaleLineItem=new ArrayList<SalesLineItem>();
		for (int i = 0; i < jsonArray.length(); i++) {
			SalesLineItem  items=new SalesLineItem();
			items.setPrduct(serviceProductList.get(i));
			items.setProductSrNo(i+1);
			logger.log(Level.SEVERE,"serviceProductList.get(i).getProductCode()--->>>"+serviceProductList.get(i).getProductCode());
			items.setProductCode(serviceProductList.get(i).getProductCode());
			items.setProductName(serviceProductList.get(i).getProductName().trim());
			items.setProductName(serviceProductList.get(i).getProductName().trim());
			try {
				items.setStartDate(sdf.parse(contractDate));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Calendar c = Calendar.getInstance();
			try {
				c.setTime(sdf.parse(contractDate));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			c.add(Calendar.DATE, serviceProductList.get(i).getDuration());
			Date date1 = c.getTime();
			items.setEndDate(date1);
			items.setDuration(serviceProductList.get(i).getDuration());
			items.setNumberOfService(intNoOfServicesList.get(i));
			items.setQuantity(1d);
			items.setArea("NA");
			items.setPrice(priceOfServicesList.get(i));
			items.setPremisesDetails("");
			items.setServicingTime(serviceProductList.get(i).getServiceTime());
			items.setVatTax(serviceProductList.get(i).getVatTax());
			items.setServiceTax(serviceProductList.get(i).getServiceTax());
			if (contractPeriod < serviceProductList.get(i).getDuration()) {
				contractPeriod = serviceProductList.get(i).getDuration();
			 }
			listofSaleLineItem.add(items);
		}
		contract.setItems(listofSaleLineItem);
		/**
		 * Tax Calculations
		 */

		List<ProductOtherCharges> productOtherChargesList = new ArrayList<ProductOtherCharges>();

		// vat tax part
		// productOtherCharges.setChargeName(serviceProductItem
		// .getVatTax().getTaxConfigName().trim());
		
		double totalAmount = 0;
		double totalAmtFix = 0;
		double netPayFix = 0;
		double netPayable = 0;
		double assessableAmount = 0;

		for (SalesLineItem item : contract.getItems()) {
			double quantity=0;
			if(item.getArea()!=null&&!item.getArea().equals("")){
				if(!item.getArea().equals("NA")){
				quantity = Double.parseDouble(item.getArea());
				}else{
					quantity = 0;
				}
			}else{
				quantity=0;
			}
			
			if (((item.getVatTax() != null) && (item
					.getVatTax().isInclusive()))
					&& (item.getServiceTax() == null)) {

				logger.log(Level.SEVERE,
						"Vat Tax Inclusive and Service Tax Not Present::::::::::::");
				// this is the value on which the tax will get
				// calculated
				// Formula : (Price-Price*VAT%) .....Because it is
				// inclusive
				assessableAmount = ((item.getPrice() * quantity) - ((item
						.getPrice() * quantity * item
						.getVatTax().getPercentage()) / 100));
				logger.log(Level.SEVERE,
						"assessableAmount::::::::::::"
								+ assessableAmount);
				totalAmount = assessableAmount;
				ProductOtherCharges productOtherCharges = new ProductOtherCharges();
				productOtherCharges
						.setChargeName(item
								.getVatTax().getTaxConfigName()
								.trim());
				productOtherCharges
						.setChargePercent(item
								.getVatTax().getPercentage());
				productOtherCharges
						.setAssessableAmount(assessableAmount);
				double chargePayable = assessableAmount
						* item.getVatTax()
								.getPercentage() / 100;
				productOtherCharges.setChargePayable(chargePayable);
				logger.log(Level.SEVERE,
						"chargePayable::::::::::::" + chargePayable);
				totalAmtFix = totalAmtFix + totalAmount;
				netPayable = item.getPrice();
				netPayFix = netPayFix + netPayable;
				productOtherChargesList.add(productOtherCharges);
			} else if (((item.getVatTax() != null) && !(item
					.getVatTax().isInclusive()))
					&& (item.getServiceTax() == null)) {

				logger.log(Level.SEVERE,
						"Vat Tax Exclusive and Service Tax Not Present::::::::::::");
				assessableAmount = item.getPrice()
						* quantity;
				logger.log(Level.SEVERE,
						"assessableAmount::::::::::::"
								+ assessableAmount);
				totalAmount = item.getPrice()
						* quantity;
				ProductOtherCharges productOtherCharges = new ProductOtherCharges();
				productOtherCharges
						.setChargeName(item
								.getVatTax().getTaxConfigName());
				productOtherCharges
						.setChargePercent(item
								.getVatTax().getPercentage());
				productOtherCharges
						.setAssessableAmount(assessableAmount);
				double chargePayable = assessableAmount
						* item.getVatTax()
								.getPercentage() / 100;
				productOtherCharges.setChargePayable(chargePayable);
				logger.log(Level.SEVERE,
						"chargePayable::::::::::::" + chargePayable);
				netPayable = totalAmount + chargePayable;
				totalAmtFix = totalAmtFix + totalAmount;
				netPayFix = netPayFix + netPayable;
			} else if (((item.getServiceTax() != null) && (item
					.getServiceTax().isInclusive()))
					&& (item.getVatTax() == null)) {

				logger.log(Level.SEVERE,
						"Vat Tax Not Present and Service Tax inclusive::::::::::::");
				assessableAmount = ((item.getPrice() * quantity) - ((item
						.getPrice() * quantity * item
						.getServiceTax().getPercentage()) / 100));
				totalAmount = assessableAmount;
				logger.log(Level.SEVERE,
						"assessableAmount::::::::::::"
								+ assessableAmount);
				ProductOtherCharges productOtherCharges = new ProductOtherCharges();
				productOtherCharges
						.setChargeName(item
								.getServiceTax().getTaxConfigName());
				productOtherCharges
						.setChargePercent(item
								.getServiceTax().getPercentage());
				productOtherCharges
						.setAssessableAmount(assessableAmount);
				double chargePayable = assessableAmount
						* item.getServiceTax()
								.getPercentage() / 100;
				productOtherCharges.setChargePayable(chargePayable);
				logger.log(Level.SEVERE,
						"chargePayable::::::::::::" + chargePayable);
				netPayable = item.getPrice();
				totalAmtFix = totalAmtFix + totalAmount;
				netPayFix = netPayFix + netPayable;
			} else if (((item.getServiceTax() != null) && !(item
					.getServiceTax().isInclusive()))
					&& (item.getVatTax() == null)) {

				logger.log(Level.SEVERE,
						"Vat Tax Not Present and Service Tax Exclusive::::::::::::");
				assessableAmount = item.getPrice()
						* quantity;
				logger.log(Level.SEVERE,
						"assessableAmount::::::::::::"
								+ assessableAmount);
				totalAmount = item.getPrice()
						* quantity;
				ProductOtherCharges productOtherCharges = new ProductOtherCharges();
				productOtherCharges
						.setChargeName(item
								.getServiceTax().getTaxConfigName());
				productOtherCharges
						.setChargePercent(item
								.getServiceTax().getPercentage());
				productOtherCharges
						.setAssessableAmount(assessableAmount);
				double chargePayable = assessableAmount
						* item.getServiceTax()
								.getPercentage() / 100;
				productOtherCharges.setChargePayable(chargePayable);
				logger.log(Level.SEVERE,
						"chargePayable::::::::::::" + chargePayable);
				netPayable = totalAmount + chargePayable;
				totalAmtFix = totalAmtFix + totalAmount;
				netPayFix = netPayFix + netPayable;
			} else if (item.getServiceTax() != null
					&& item.getVatTax() != null) {

				logger.log(Level.SEVERE,
						"Vat Tax Present  and Service Tax Present::::::::::::");

				if (item.getServiceTax()
						.isInclusive()
						&& item.getVatTax()
								.isInclusive()) {
					logger.log(Level.SEVERE,
							"Vat Tax Inclusive and Service Tax Inclusive::::::::::::");

					netPayable = item.getPrice()
							* quantity;
					// For Tax
					double assessableAmountForST = (netPayable * 100)
							/ (100 + item
									.getServiceTax()
									.getPercentage());
					logger.log(Level.SEVERE,
							"assessableAmountForST::::::::::::"
									+ assessableAmountForST);

					double assessableAmountForVT = (assessableAmountForST * 100)
							/ (100 + item.getVatTax()
									.getPercentage());
					logger.log(Level.SEVERE,
							"assessableAmountForVT::::::::::::"
									+ assessableAmountForVT);
					totalAmount = assessableAmountForVT;
					totalAmtFix = totalAmtFix + totalAmount;
					netPayFix = netPayFix + netPayable;

					// For VAT
					ProductOtherCharges productOtherCharges = new ProductOtherCharges();
					productOtherCharges
							.setChargeName(item
									.getVatTax().getTaxConfigName());
					productOtherCharges
							.setChargePercent(item
									.getVatTax().getPercentage());
					productOtherCharges
							.setAssessableAmount(assessableAmountForVT);
					double chargePayableVT = assessableAmountForVT
							* item.getVatTax()
									.getPercentage() / 100;
					productOtherCharges
							.setChargePayable(chargePayableVT);
					logger.log(Level.SEVERE,
							"chargePayableVT::::::::::::"
									+ chargePayableVT);
					productOtherChargesList
							.add(productOtherCharges);

					// For ST
					ProductOtherCharges productOtherChargesST = new ProductOtherCharges();
					productOtherChargesST
							.setChargeName(item
									.getServiceTax()
									.getTaxConfigName());
					productOtherChargesST
							.setChargePercent(item
									.getServiceTax()
									.getPercentage());
					productOtherChargesST
							.setAssessableAmount(assessableAmountForST);
					double chargePayableST = assessableAmountForST
							* item.getServiceTax()
									.getPercentage() / 100;
					productOtherChargesST
							.setChargePayable(chargePayableST);
					logger.log(Level.SEVERE,
							"chargePayable::::::::::::"
									+ chargePayableST);
					productOtherChargesList
							.add(productOtherChargesST);

				} else if (!(item.getServiceTax()
						.isInclusive())
						&& !(item.getVatTax()
								.isInclusive())) {

					logger.log(Level.SEVERE,
							"Vat Tax Exclusive and Service Tax Exclusive::::::::::::");
					totalAmount = item.getPrice();
					double assessableAmountForVT = totalAmount;
					logger.log(Level.SEVERE,
							"assessableAmountForVT::::::::::::"
									+ assessableAmountForVT);
					ProductOtherCharges productOtherCharges = new ProductOtherCharges();
					productOtherCharges
							.setChargeName(item
									.getVatTax().getTaxConfigName());
					productOtherCharges
							.setChargePercent(item
									.getVatTax().getPercentage());
					productOtherCharges
							.setAssessableAmount(assessableAmountForVT);
					double chargePayableVT = (assessableAmountForVT * item
							.getVatTax().getPercentage()) / 100;
					productOtherCharges
							.setChargePayable(chargePayableVT);
					logger.log(Level.SEVERE,
							"chargePayableST::::::::::::"
									+ chargePayableVT);
					productOtherChargesList
							.add(productOtherCharges);

					// For ST
					double assessableAmountForST = totalAmount
							+ chargePayableVT;

					logger.log(Level.SEVERE,
							"assessableAmountForST::::::::::::"
									+ assessableAmountForST);
					ProductOtherCharges productOtherChargesST = new ProductOtherCharges();
					productOtherChargesST
							.setChargeName(item
									.getServiceTax()
									.getTaxConfigName());
					productOtherChargesST
							.setChargePercent(item
									.getServiceTax()
									.getPercentage());
					productOtherChargesST
							.setAssessableAmount(assessableAmountForST);
					double chargePayableST = assessableAmountForST
							* item.getServiceTax()
									.getPercentage() / 100;
					productOtherChargesST
							.setChargePayable(chargePayableST);
					netPayable = assessableAmountForST
							+ chargePayableST;
					totalAmtFix = totalAmtFix + totalAmount;
					netPayFix = netPayFix + netPayable;
					logger.log(Level.SEVERE,
							"chargePayable::::::::::::"
									+ chargePayableST);
					productOtherChargesList
							.add(productOtherChargesST);
				} else if (item.getServiceTax()
						.isInclusive()
						&& !(item.getVatTax()
								.isInclusive())) {

					logger.log(Level.SEVERE,
							"Vat Tax Exclusive and Service Tax Inclusive::::::::::::");
					// ServiceTax=Inclusive && VatTax=Exclusive....
					// Here we are adding VAT TAX first because it
					// is
					// exclusive, and calculating NetPayable value.
					// The
					// we will do reverse calculation
					double netPay = (item.getPrice() * item
							.getVatTax().getPercentage()) / 100;
					netPayable = netPay;

					double assessableAmountST = (netPayable * 100)
							/ (100 + item
									.getServiceTax()
									.getPercentage());
					double assessableAmountVT = (assessableAmountST * 100)
							/ (100 + item.getVatTax()
									.getPercentage());

					totalAmount = assessableAmountVT;

					// Now we will calculate VAT TAX
					ProductOtherCharges productOtherCharges = new ProductOtherCharges();
					productOtherCharges
							.setChargeName(item
									.getVatTax().getTaxConfigName());
					productOtherCharges
							.setChargePercent(item
									.getVatTax().getPercentage());
					productOtherCharges
							.setAssessableAmount(assessableAmountVT);
					productOtherCharges
							.setChargePayable((assessableAmountVT * item
									.getVatTax().getPercentage()) / 100);
					productOtherChargesList
							.add(productOtherCharges);

					// First we are calculating Service Tax of the
					// product to find assessable amount to show in
					// product
					ProductOtherCharges productOtherChargesST = new ProductOtherCharges();
					productOtherChargesST
							.setChargeName(item
									.getServiceTax()
									.getTaxConfigName());
					productOtherChargesST
							.setChargePercent(item
									.getServiceTax()
									.getPercentage());
					productOtherChargesST
							.setAssessableAmount(assessableAmountST);
					productOtherChargesST
							.setChargePayable((assessableAmountST * item
									.getServiceTax()
									.getPercentage()) / 100);
					productOtherChargesList
							.add(productOtherChargesST);
					totalAmtFix = totalAmtFix + totalAmount;
					netPayFix = netPayFix + netPayable;

				} else if (!(item.getServiceTax()
						.isInclusive())
						&& (item.getVatTax()
								.isInclusive())) {

					logger.log(Level.SEVERE,
							"Vat Tax Inclusive and Service Tax Exclusive ::::::::::::");
					// ServiceTax=Exclusive && VatTax=Inclusive....
					double assesableAmountST = item
							.getPrice();
					double assesableAmountVT = (assesableAmountST * 100)
							/ (100 + item
									.getServiceTax()
									.getPercentage());
					totalAmount = assesableAmountVT;
					netPayable = assesableAmountST;

					netPayFix = netPayFix + netPayable;

					totalAmtFix = totalAmtFix + totalAmount;

					// Calculation of VAT TAX
					ProductOtherCharges productOtherCharges = new ProductOtherCharges();
					productOtherCharges
							.setChargeName(item
									.getVatTax().getTaxConfigName());
					productOtherCharges
							.setChargePercent(item
									.getVatTax().getPercentage());
					productOtherCharges
							.setAssessableAmount(assesableAmountVT);
					double chargePayable = (assesableAmountVT * item
							.getVatTax().getPercentage()) / 100;
					productOtherCharges
							.setChargePayable(chargePayable);
					logger.log(Level.SEVERE,
							"chargePayable::::::::::::"
									+ chargePayable);
					productOtherChargesList
							.add(productOtherCharges);
					// Calculation for Service Tax
					ProductOtherCharges productOtherChargesST = new ProductOtherCharges();
					productOtherChargesST
							.setChargeName(item
									.getServiceTax()
									.getTaxConfigName());
					productOtherChargesST
							.setChargePercent(item
									.getServiceTax()
									.getPercentage());
					productOtherChargesST
							.setAssessableAmount(assesableAmountST);
					double chargePayableST = (assesableAmountST * item
							.getServiceTax().getPercentage()) / 100;
					productOtherChargesST
							.setChargePayable(chargePayableST);
					logger.log(Level.SEVERE,
							"chargePayableST::::::::::::"
									+ chargePayableST);
					productOtherChargesList
							.add(productOtherChargesST);
				}

			} else {
				logger.log(Level.SEVERE,
						"Vat Tax Not Present and Service Tax Not Present::::::::::::");
				totalAmount = item.getPrice();
				netPayable = totalAmount;
				totalAmtFix = totalAmtFix + totalAmount;
				netPayFix = netPayFix + netPayable;
			}
		}
		contract.getProductTaxes().addAll(productOtherChargesList);
		logger.log(Level.SEVERE, "Total Amount:::::::"
				+ totalAmtFix);
		logger.log(Level.SEVERE, "netPayFix:::::::" + netPayFix);
		contract.setTotalAmount(totalAmtFix);
		contract.setNetpayable(netPayFix);
		contract.setGrandTotal(netPayFix);

		/**Done Adding Product**/
		
		/**
		 * Service Schedule Pop Up
		 */
		List<ServiceSchedule> serviceScheduleList = new ArrayList<ServiceSchedule>();

		try {
			for (int i = 0; i < listofSaleLineItem.size(); i++) {
				String previousDate = "";
				int gapOfService = (int) (listofSaleLineItem
						.get(i).getDuration() / (listofSaleLineItem
						.get(i).getNumberOfServices() * listofSaleLineItem
						.get(i).getQty()));
				logger.log(Level.SEVERE, "gapOfService:::::::"
						+ gapOfService);
				for (int j = 0; j < listofSaleLineItem.get(i)
						.getNumberOfServices()
						* listofSaleLineItem.get(i).getQty(); j++) {
					logger.log(Level.SEVERE,
							"inside No of Services:::::::::::"
									+ j);
					logger.log(Level.SEVERE,
							"listofSaleLineItem.get(i).getProductSrNo()::::"
									+ listofSaleLineItem.get(i)
											.getProductSrNo());
					logger.log(Level.SEVERE,
							"listofSaleLineItem.get(i).getPrduct().getCount():::::"
									+ listofSaleLineItem.get(i)
											.getPrduct()
											.getCount());
					logger.log(Level.SEVERE,
							"listofSaleLineItem.get(i).getPrduct().getProductName()::"
									+ listofSaleLineItem.get(i)
											.getPrduct()
											.getProductName());
					logger.log(Level.SEVERE,
							"listofSaleLineItem.get(i).getPrduct().getProductName()::"
									+ listofSaleLineItem.get(i)
											.getPrduct()
											.getProductName());
					logger.log(Level.SEVERE,
							"listofSaleLineItem.get(i).getDuration()::"
									+ listofSaleLineItem.get(i)
											.getDuration());
					logger.log(
							Level.SEVERE,
							"listofSaleLineItem.get(i).getNumberOfServices()::::::::"
									+ listofSaleLineItem
											.get(i)
											.getNumberOfServices());
					logger.log(Level.SEVERE, "j+1:::::::::::"
							+ j + 1);

					ServiceSchedule serviceSchedule = new ServiceSchedule();
					serviceSchedule.setScheduleStartDate(listofSaleLineItem.get(i).getStartDate());
					serviceSchedule
							.setSerSrNo(listofSaleLineItem.get(
									i).getProductSrNo());
					serviceSchedule
							.setScheduleProdId(listofSaleLineItem
									.get(i).getPrduct()
									.getCount());
//					serviceSchedule.setScheduleServiceDate();
					serviceSchedule
							.setScheduleProQty((int) listofSaleLineItem
									.get(i).getQty());
					serviceSchedule
							.setScheduleProdName(listofSaleLineItem
									.get(i).getPrduct()
									.getProductName());
					serviceSchedule
							.setScheduleDuration(listofSaleLineItem
									
									.get(i).getDuration());
					serviceSchedule
							.setScheduleNoOfServices(listofSaleLineItem
									.get(i)
									.getNumberOfServices());
					serviceSchedule.setScheduleServiceNo(j + 1);
					logger.log(Level.SEVERE,
							"gapOfService:::::::"
									+ gapOfService);
					
					//***************Used for marking service date as red/black  Date : 2/1/2017******** 
					
//					  SimpleDateFormat sdf = new SimpleDateFormat(
//                                "dd-MM-yyyy");
                        sdf.setTimeZone(TimeZone
                                .getTimeZone("IST"));
                        TimeZone.setDefault(TimeZone
                                .getTimeZone("IST"));
                        Date dateProdEnd=sdf.parse(sdf
                                .format(listofSaleLineItem.get(i).getStartDate()));
                        Calendar c2 = Calendar.getInstance();
                        try {
                            c2.setTime(dateProdEnd);
                            c2.add(Calendar.DATE,listofSaleLineItem.get(i).getDuration());
                        } catch (Exception e1) {
                            // TODO Auto-generated catch block
                            logger.log(Level.SEVERE,
                                    "Error inn j>1:::::::" + e1);

                            e1.printStackTrace();
                        }
                        System.out.println("dateProdEnd:::::::::::"+dateProdEnd+"cal::::::::"+c2.getTime());
                        serviceSchedule.setScheduleProdEndDate(c2.getTime());

					
					//**************ends here ****************************** 

					if (j + 1 == 1) {
//						SimpleDateFormat sdfDateP = new SimpleDateFormat(
//								"dd-MM-yyyy");
//						sdfDateP.setTimeZone(TimeZone
//								.getTimeZone("IST"));
//						TimeZone.setDefault(TimeZone
//								.getTimeZone("IST"));

						try {
							serviceSchedule
									.setScheduleServiceDate(sdf
											.parse(contractDate));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							logger.log(Level.SEVERE,
									"Error in j==1" + e);
							e.printStackTrace();
						}
						previousDate = contractDate;
					} else {
						SimpleDateFormat sdfDateS = new SimpleDateFormat(
								"dd-MM-yyyy");
						sdfDateS.setTimeZone(TimeZone
								.getTimeZone("IST"));
						TimeZone.setDefault(TimeZone
								.getTimeZone("IST"));

						Calendar c1 = Calendar.getInstance();
						try {
							c1.setTime(sdfDateS
									.parse(previousDate));
						} catch (ParseException e1) {
							// TODO Auto-generated catch block
							logger.log(Level.SEVERE,
									"Error inn j>1:::::::" + e1);

							e1.printStackTrace();
						}
						logger.log(Level.SEVERE,
								"gapOfService:::::::"
										+ gapOfService);
						Calendar c = Calendar.getInstance();
						try {
							c.add(Calendar.DATE, gapOfService);
							Date date1 = c.getTime();
							// serviceSchedule2.setScheduleServiceDay();

							serviceSchedule
									.setScheduleServiceDate(sdfDateS.parse(sdfDateS
											.format(date1)));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							logger.log(Level.SEVERE,
									"Error in Adding days:::::::"
											+ e);
							e.printStackTrace();
						}
					}
					serviceSchedule
							.setScheduleServiceTime("Flexible");
					serviceSchedule.setTotalServicingTime(0.0);
					serviceSchedule
							.setScheduleProBranch("Service Address");
					serviceScheduleList.add(serviceSchedule);
					logger.log(
							Level.SEVERE,
							"serviceScheduleList:::::::::::"
									+ serviceScheduleList
											.size());
				}
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE,
					"ERROR in Method:::::::::::" + e);
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,
				"serviceScheduleList.size()::::::::::::"
						+ serviceScheduleList.size());
		contract.setServiceScheduleList(serviceScheduleList);

		SimpleDateFormat sdfContractDate = new SimpleDateFormat(
				"dd-MM-yyyy");
		sdfContractDate
				.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		Calendar c = Calendar.getInstance();
		c.setTime(contract.getStartDate());
		try {
			c.add(Calendar.DATE, contractPeriod);
			Date date1 = c.getTime();
			contract.setEndDate(sdf.parse(sdf.format(date1)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class)
				.filter("companyId", comp.getCompanyId())
				.filter("processName", "Contract").filter("status", true)
				.first().now();

		long number = ng.getNumber();
		int count = (int) number;
		ng.setNumber(count+1);
		ofy().save().entity(ng);
		contract.setCount(count+1);
		logger.log(Level.SEVERE,"Contract Count::::-->>>>"+(count+1));
		
		ofy().save().entity(contract);
		createServiceAndBilling(contract,comp);
		try{
			sendSms(contract);
			CustomerUser customerUser = ofy()
					.load()
					.type(CustomerUser.class)
					.filter("companyId", comp.getCompanyId())
					.filter("cinfo.count",
							contract.getCinfo().getCount()).first()
					.now();
			sendPushNotification(comp.getCompanyId(),
					AppConstants.IAndroid.EVA_KRETO, customerUser
							.getUserName().trim(), response,contract,comp);
			sendPushNotificationToPriora(comp.getCompanyId(),
					AppConstants.IAndroid.EVA_PRIORA, customerUser
							.getUserName().trim(), response,contract,comp);
			Email email=new Email();
			email.initiateServiceContractEmail(contract);
//			boolean successfull=email.sendEmailOfContract();
//			logger.log(Level.SEVERE,"successfull::"+successfull);
		}catch(Exception e){
			logger.log(Level.SEVERE,"Error in sending sms"+e);
		}
		return true;
	}

	private void sendPushNotificationToPriora(Long companyId, String applicationName,
			String trim, String response, Contract contract, Company comp2) {
		CustomerUser customerUser=ofy().load().type(CustomerUser.class).filter("companyId",companyId).filter("cinfo.fullName",contract.getCinfo().getFullName()).first().now(); 
		response="event:New Contract"+"$"+customerUser.getCustomerName()+" has created new contract,its contract id is "+contract.getCount();//getJSONResponse(service);
		
		List<RegisterDevice> device = ofy().load().type(RegisterDevice.class)
				.filter("companyId", companyId)
				.filter("applicationName", applicationName).list();
		for (RegisterDevice registerDevice : device) {
			String deviceName = registerDevice.getRegId().trim();
			logger.log(Level.SEVERE,"deviceName"+deviceName);
			URL url = null;
			try {
				url = new URL("https://fcm.googleapis.com/fcm/send");
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			HttpURLConnection conn = null;
			try {
				conn = (HttpURLConnection) url.openConnection();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			conn.setDoOutput(true);
			try {
				conn.setRequestMethod("POST");
			} catch (ProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			conn.setRequestProperty("Content-Type",
					"application/json; charset=UTF-8");
			conn.setRequestProperty("Authorization", "key="
					+ comp2.getFcmServerKeyForPriora().trim());
			conn.setDoOutput(true);
			conn.setDoInput(true);

			try {
				conn.setRequestMethod("POST");
			} catch (ProtocolException e) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE, "ProtocolException ERROR::::::::::" + e);
				e.printStackTrace();
			}
			OutputStream os = null;
			try {
				os = conn.getOutputStream();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
				e.printStackTrace();
			}
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
			// Log.d("Test", "From Get Post Method" + getPostData(values));
			try {
				writer.write(createJSONData(response, deviceName));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE, "IOException2 ERROR::::::::::" + e);
				e.printStackTrace();
			}
			try {
				writer.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE, "IOException3 ERROR::::::::::" + e);
				e.printStackTrace();
			}

			InputStream is = null;
			try {
				is = conn.getInputStream();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
				e.printStackTrace();
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String temp;
			String data = null;
			try {
				while ((temp = br.readLine()) != null) {
					data = data + temp;
				}
				logger.log(Level.SEVERE, "data data::::::::::" + data);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
				e.printStackTrace();
			}

			logger.log(Level.SEVERE, "Data::::::::::" + data);	
		}
		
	}

	private void sendPushNotification(Long companyId, String applicationName,
			String trim, String response, Contract contract, Company comp2) {
		response="event:New Contract"+"$"+"Thank You for choosing "+comp.getBusinessUnitName()+ "Your contract id is "+contract.getCount();//getJSONResponse(service);
		CustomerUser customerUser=ofy().load().type(CustomerUser.class).filter("companyId",companyId).filter("cinfo.fullName",contract.getCinfo().getFullName()).first().now(); 
		RegisterDevice device = ofy().load().type(RegisterDevice.class)
				.filter("companyId", companyId)
				.filter("applicationName", applicationName)
				.filter("userName", customerUser.getUserName()).first().now();
		String deviceName = device.getRegId().trim();
		logger.log(Level.SEVERE,"deviceName"+deviceName);
		URL url = null;
		try {
			url = new URL("https://fcm.googleapis.com/fcm/send");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HttpURLConnection conn = null;
		try {
			conn = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		conn.setDoOutput(true);
		try {
			conn.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		conn.setRequestProperty("Content-Type",
				"application/json; charset=UTF-8");
		conn.setRequestProperty("Authorization", "key="
				+ comp2.getFcmServerKeyForKreto().trim());
		conn.setDoOutput(true);
		conn.setDoInput(true);

		try {
			conn.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "ProtocolException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		OutputStream os = null;
		try {
			os = conn.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
		// Log.d("Test", "From Get Post Method" + getPostData(values));
		try {
			writer.write(createJSONData(response, deviceName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException2 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		try {
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException3 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		InputStream is = null;
		try {
			is = conn.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String temp;
		String data = null;
		try {
			while ((temp = br.readLine()) != null) {
				data = data + temp;
			}
			logger.log(Level.SEVERE, "data data::::::::::" + data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		logger.log(Level.SEVERE, "Data::::::::::" + data);
	}

	private void sendSms(Contract contract) {


		 Logger logger = Logger.getLogger("NameOfYourLogger");
		 logger.log(Level.SEVERE," inside sms method");
		 System.out.println("inside SMS method");
		SmsConfiguration smsconfig = ofy().load().type(SmsConfiguration.class).filter("companyId",contract.getCompanyId()).filter("status",true).first().now();
		SimpleDateFormat fmtcondate = new SimpleDateFormat("dd/MM/yyyy");

		/**
		 * Date 9-03-2017
		 * added by vijay
		 * for ankita pest control needed reference number of contract in place of contract id
		 * so while sending sms below if condition for contract id value is reference number and else for contract id value
		 */
		
		ProcessConfiguration processconfiguration = ofy().load().type(ProcessConfiguration.class).filter("processList.processName", "ContractApprovedSMS").filter("processList.processType", "SMSWithReferenceNumber").filter("processList.status", true).filter("companyId", contract.getCompanyId()).first().now();
		/**
		 * ends here
		 */
		
//		if(smsconfig!=null){
//			
//			String accountauthkey = smsconfig.getAuthToken();
//			String accSenderId =smsconfig.getAccountSID(); 
//			String accRoute = smsconfig.getPassword();
//			System.out.println("in sms config");
//			 logger.log(Level.SEVERE,"in sms config");

//			if(accountauthkey!=null && accSenderId!=null && accRoute!=null  )
			
			SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId", contract.getCompanyId()).filter("event", "Contract Approved" ).filter("status",true).first().now();
			if(smsEntity!=null){
				
				String templatemsgwithbraces = smsEntity.getMessage();
				Company comp = ofy().load().type(Company.class).filter("companyId", contract.getCompanyId()).first().now();
				logger.log(Level.SEVERE,"in sms template msg");
				
				String fullname = contract.getCinfo().getFullName();
				System.out.println(" customerName ="+contract.getCinfo().getCellNumber());
				String constartDate = fmtcondate.format(contract.getStartDate());
				String conendDate = fmtcondate.format(contract.getEndDate());
				
				
				/**
				 * Date 29 jun 2017 added by vijay for Eco friendly
				 */
				
				ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","SMS").filter("status",true).filter("companyId", contract.getCompanyId()).first().now();
				ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", contract.getCompanyId()).filter("processName","SMS").first().now();				
					
				boolean prossconfigBhashSMSAPIflag = false;
				if(processName!=null){
				if(processConfig!=null){
					if(processConfig.isConfigStatus()){
						for(int l=0;l<processConfig.getProcessList().size();l++){
							if(processConfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("SMS") && processConfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("BHASHSMSAPI") && processConfig.getProcessList().get(l).isStatus()==true){
								prossconfigBhashSMSAPIflag=true;
							}
						}
				   }
				}
				}
				logger.log(Level.SEVERE,"process config  BHASH SMS Flag =="+prossconfigBhashSMSAPIflag);
				String actualMsg="";
//				if(prossconfigBhashSMSAPIflag){
//					fullname = getCustomerName(fullname,contract.getCompanyId());
//					
//					String customerName = templatemsgwithbraces.replace("{customerName}",fullname);
//					String companyName = customerName.replace("{companyName}",comp.getBusinessUnitName());
//					String contractStartDate = companyName.replace("{contractstartDate}", constartDate);
//					 actualMsg = contractStartDate.replace("{contractendDate}", conendDate);
//				}
				/**
				 * ends here
				 * else for normal sms template
				 */
//				else{
					
					String customerName = templatemsgwithbraces.replace("{customerName}",fullname);
					String companyName = customerName.replace("{companyName}",comp.getBusinessUnitName());
					String contractId;
					if(processconfiguration!=null){
						contractId= companyName.replace("{referenceNum}", contract.getRefNo()+"");
					}else{
						contractId = companyName.replace("{contractId}", contract.getCount()+"");
					}
					String contractStartDate = contractId.replace("{contractstartDate}", constartDate);
					 actualMsg = contractStartDate.replace("{contractendDate}", conendDate);
//				}
				
				System.out.println(" msg =="+actualMsg);
				logger.log(Level.SEVERE," Message =="+actualMsg);
				long customermobileNo = contract.getCinfo().getCellNumber();
				logger.log(Level.SEVERE," Customer Mob No =="+customermobileNo);
				
				SmsServiceImpl smsimpl = new SmsServiceImpl();
//				smsimpl.sendSmsToClient(actualMsg, customermobileNo, accSenderId, accountauthkey, accRoute,contract.getCompanyId());
//				logger.log(Level.SEVERE," Impl method called");

				/**
				 * @author Vijay Date :- 29-04-2022
				 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
				 * whats and sms based on communication channel configuration.
				 */
				smsimpl.sendMessage(AppConstants.SERVICEMODULE,AppConstants.CONTRACT,smsEntity.getEvent(),contract.getCompanyId(),actualMsg,customermobileNo,false);
				logger.log(Level.SEVERE,"after sendMessage method");
			}
//		}
			
	}

	private void createServiceAndBilling(Contract contract, Company comp) {
		// TODO Auto-generated method stub
		createServices(contract,comp);
		createBilling(contract,comp);
	}

	private void createBilling(Contract contract, Company comp) {
		logger.log(Level.SEVERE, "Creating Billing Doc");
		logger.log(Level.SEVERE, "contract in create billing::" + contract);
		if (contract != null) {
			BillingDocument billingDoc = new BillingDocument();
			// Setting customer information
			PersonInfo personInfo = new PersonInfo();
			personInfo.setCount(contract.getCinfo().getCount());
			personInfo.setFullName(contract.getCinfo().getFullName());
			personInfo.setCellNumber(contract.getCinfo().getCellNumber());
			personInfo.setPocName(contract.getCinfo().getPocName());
			billingDoc.setPersonInfo(personInfo);
			// Setting contract information
			/*** Order Informaton *****/
			billingDoc.setContractCount(contract.getCount());
			System.out.println("The Contract Count is" + contract.getCount());
			billingDoc.setTotalSalesAmount(contract.getNetpayable());

			System.out.println("Net Payable1 is :" + contract.getNetpayable());

			billingDoc.setAccountType("AR");
			/*** Done with Order Informaton *****/

			/********* Payment Terms are Hard Coded ********************/
			ArrayList<PaymentTerms> listofpayterms = new ArrayList<PaymentTerms>();
			PaymentTerms paymentterms = new PaymentTerms();
			paymentterms.setPayTermDays(0);
			paymentterms.setPayTermPercent(100d);
			paymentterms.setPayTermComment("By Default");
			listofpayterms.add(paymentterms);
			billingDoc.setArrPayTerms(listofpayterms);
			/************ Done with Payment Terms *********************/

			/**** Billing Info **********/
			NumberGeneration ngforbilling = ofy().load()
					.type(NumberGeneration.class)
					.filter("companyId", comp.getCompanyId())
					.filter("processName", "BillingDocument")
					.filter("status", true).first().now();

			long number1 = ngforbilling.getNumber();
			int countforbilling = (int) number1;
			// billingDoc.setCount(countforbilling + 1);
			SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
			sdfDate.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			try {
				billingDoc.setBillingDate(sdfDate.parse(sdfDate.format(contract.getContractDate())));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			billingDoc.setStatus(BillingDocument.BILLINGINVOICED);
			billingDoc.setApproverName(contract.getEmployee());
			billingDoc.setEmployee(contract.getEmployee());
			billingDoc.setBranch(contract.getBranch());
			/**** Done with Billing Info **********/

			/********** Billing Document Information ****************/
			billingDoc.getBillingDocumentInfo().setBillId(countforbilling + 1);
			billingDoc.getBillingDocumentInfo().setBillingDate(new Date());
			billingDoc.getBillingDocumentInfo().setBillAmount(
					contract.getNetpayable());
			System.out.println("net payable amount 2:"
					+ contract.getNetpayable());
			billingDoc.getBillingDocumentInfo().setBillstatus(
					BillingDocument.BILLINGINVOICED);
			/********** Billing Document Information ****************/

			/************ Product Details ***************/

			int noOfProduct = contract.getItems().size();
			System.out.println("The No of Product" + noOfProduct);

			ArrayList<SalesOrderProductLineItem> productTable = new ArrayList<SalesOrderProductLineItem>();

			for (int i = 0; i < noOfProduct; i++) {

				SalesOrderProductLineItem product = new SalesOrderProductLineItem();

				System.out
						.println("product.setProdId(contract.getItems().get(i).getPrduct().getCount())"
								+ (contract.getItems().get(i).getPrduct()
										.getCount()));

				if (contract.getItems().get(i).getPrduct().getCount() != 0) {

					product.setProdId(contract.getItems().get(i).getPrduct()
							.getCount());

				}

				product.setProdCategory(contract.getItems().get(i)
						.getProductCategory());
				product.setProdCode(contract.getItems().get(i).getProductCode());
				product.setProdName(contract.getItems().get(i).getProductName());
				product.setQuantity(contract.getItems().get(i).getQty());
				product.setPrice(contract.getItems().get(i).getPrice());
				product.setVatTax(contract.getItems().get(i).getVatTax());
				product.setServiceTax(contract.getItems().get(i)
						.getServiceTax());
				product.setTotalAmount(contract.getItems().get(i).getPrice());
				product.setBaseBillingAmount((contract.getItems().get(i)
						.getQty()) * (contract.getItems().get(i).getPrice()));
				product.setPaymentPercent(100);
//				product.setBasePaymentAmount();
				productTable.add(product);
			}

			billingDoc.setSalesOrderProducts(productTable);
			System.out.println("contract.getNetpayable()"
					+ contract.getNetpayable());
			billingDoc.setTotalBillingAmount(contract.getNetpayable());
			try {
				billingDoc.setInvoiceDate(sdf.parse(sdf.format(new Date())));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				billingDoc.setPaymentDate(sdf.parse(sdf.format(new Date())));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/************ Finish with Product Details ***************/

			/***** Tax Table *************/

			List<ContractCharges> taxTableList = new ArrayList<ContractCharges>();

			for (int i = 0; i < contract.getProductTaxes().size(); i++) {
				ContractCharges taxTable = new ContractCharges();
				taxTable.setTaxChargeName(contract.getProductTaxes().get(i)
						.getChargeName());
				taxTable.setTaxChargePercent(contract.getProductTaxes().get(i)
						.getChargePercent());
				taxTable.setTaxChargeAssesVal(contract.getProductTaxes().get(i)
						.getAssessableAmount());
				taxTable.setPayableAmt(contract.getProductTaxes().get(i)
						.getChargePayable());
				taxTableList.add(taxTable);
			}
			billingDoc.setBillingTaxes(taxTableList);
			/**** Done with Tax Table **************/

			// Saving Billing Document
			billingDoc.setTypeOfOrder(AppConstants.ORDERTYPESERVICE);
			billingDoc.setCompanyId(comp.getCompanyId());
			billingDoc.setCount(countforbilling+1);
			ngforbilling.setNumber(countforbilling+1);
			ofy().save().entity(ngforbilling);
			ofy().save().entity(billingDoc);
			createInvoice(billingDoc,comp);
		}
	}

	private void createInvoice(BillingDocument billingDoc, Company comp) {
		final Invoice inventity = new Invoice();
		PersonInfo pinfo = new PersonInfo();
			pinfo.setCount(billingDoc.getPersonInfo().getCount());
			pinfo.setFullName(billingDoc.getPersonInfo().getFullName());
			pinfo.setCellNumber(billingDoc.getPersonInfo().getCellNumber());
			pinfo.setPocName(billingDoc.getPersonInfo().getPocName());

			inventity.setGrossValue(billingDoc.getGrossValue());


			inventity.setTaxPercent(billingDoc.getArrPayTerms().get(0).getPayTermPercent());

			inventity.setPersonInfo(pinfo);

			inventity.setContractCount(billingDoc.getContractCount());
			inventity.setContractStartDate(billingDoc.getContractStartDate());
			inventity.setContractEndDate(billingDoc.getContractEndDate());
			inventity.setTotalSalesAmount(billingDoc.getTotalSalesAmount());

		List<BillingDocumentDetails> billtablelis =billingDoc.getArrayBillingDocument();
		ArrayList<BillingDocumentDetails> billtablearr = new ArrayList<BillingDocumentDetails>();
		billtablearr.addAll(billtablelis);
		inventity.setArrayBillingDocument(billtablearr);

//		System.out.println("MULTIPLE CONTRAT STATUS ::: "
//				+ isMultipleContractBilling());
//		if (isMultipleContractBilling()) {
//			inventity.setMultipleOrderBilling(true);
//		}

			inventity
					.setTotalBillingAmount(billingDoc.getTotalBillingAmount());

			inventity.setNetPayable(billingDoc.getTotalBillingAmount());

		inventity.setDiscount(0);
		// ends here

			inventity.setInvoiceDate(billingDoc.getBillingDate());
			inventity.setPaymentDate(billingDoc.getPaymentDate());
			inventity.setApproverName(billingDoc.getApproverName());
			inventity.setEmployee(billingDoc.getEmployee());
			inventity.setBranch(billingDoc.getBranch());

		inventity.setOrderCreationDate(billingDoc.getOrderCreationDate());
		inventity.setCompanyId(comp.getCompanyId());
		inventity.setInvoiceAmount(billingDoc.getTotalBillingAmount());
		inventity.setInvoiceType(AppConstants.CREATETAXINVOICE);
		inventity.setPaymentMethod(billingDoc.getPaymentMethod());
		inventity.setAccountType(billingDoc.getAccountType());
		inventity.setTypeOfOrder(billingDoc.getTypeOfOrder());

			inventity.setStatus(Invoice.APPROVED);

		List<SalesOrderProductLineItem> productlist = billingDoc.getSalesOrderProducts();
		ArrayList<SalesOrderProductLineItem> prodList = new ArrayList<SalesOrderProductLineItem>();
		prodList.addAll(productlist);
		System.out.println();
		System.out.println("SALES PRODUCT TABLE SIZE ::: " + prodList.size());

		List<ContractCharges> taxestable = billingDoc.getBillingOtherCharges();
		ArrayList<ContractCharges> taxesList = new ArrayList<ContractCharges>();
		taxesList.addAll(taxestable);
		System.out.println("TAXES TABLE SIZE ::: " + taxesList.size());

		List<ContractCharges> otherChargesable = billingDoc.getBillingOtherCharges();
		ArrayList<ContractCharges> otherchargesList = new ArrayList<ContractCharges>();
		otherchargesList.addAll(otherChargesable);
		System.out.println("OTHER CHARGES TABLE SIZE ::: "
				+ otherchargesList.size());
		inventity.setSalesOrderProductFromBilling(prodList);
		// inventity.setSalesOrderProducts(prodList);
		inventity.setBillingTaxes(taxesList);
		inventity.setBillingOtherCharges(otherchargesList);

		inventity.setCustomerBranch(billingDoc.getCustomerBranch());

		inventity.setAccountType(billingDoc.getAccountType());
		inventity.setArrPayTerms(billingDoc.getArrPayTerms());

			inventity.setInvoiceGroup(billingDoc.getGroup());
			inventity.setOrderCformStatus(billingDoc.getOrderCformStatus());
			inventity.setOrderCformPercent(billingDoc.getOrderCformPercent());
//		} else {
//			inventity.setOrderCformPercent(-1);
//		}

		/***************** vijay number range *************************/
		if (billingDoc.getNumberRange() != null)
			inventity.setNumberRange(billingDoc.getNumberRange());
		/****************************************************/

		/**
		 * date 14 Feb 2017 added by vijay for Setting billing period from date
		 * and To date in invoice
		 */
		if (billingDoc.getBillingPeroidFromDate()!= null)
			inventity.setBillingPeroidFromDate(billingDoc.getBillingPeroidFromDate());
		if (billingDoc.getBillingPeroidToDate()!= null)
			inventity.setBillingPeroidToDate(billingDoc.getBillingPeroidToDate());
		/**
		 * End here
		 */

		/*
		 * nidhi 1-07-2017
		 */
			inventity.setSegment(billingDoc.getSegment());
		/*
		 * end
		 */

		/*
		 * nidhi 1-07-2017
		 */
			inventity.setRateContractServiceId(billingDoc.getRateContractServiceId());
		/*
		 * end
		 */

		/**
		 * nidhi 20-07-2017 quantity and mesurement transfer to the invoice
		 * entity
		 */
			inventity.setQuantity(billingDoc.getQuantity());

			inventity.setUom(billingDoc.getUom());
		/*
		 * end
		 */
		/*
		 * nidhi 24-07-2017 save customer ref no to invoice bean
		 */
			inventity.setRefNumber(billingDoc.getRefNumber());
		/**
		 * end
		 */
			NumberGeneration ngforInvoice = ofy().load()
					.type(NumberGeneration.class)
					.filter("companyId", comp.getCompanyId())
					.filter("processName", "Invoice")
					.filter("status", true).first().now();
			long number1 = ngforInvoice.getNumber();
			int countforinvoice = (int) number1;
			inventity.setCompanyId(comp.getCompanyId());
			inventity.setCount(countforinvoice+1);
			ngforInvoice.setNumber(countforinvoice+1);
			billingDoc.setInvoiceCount(countforinvoice+1);
			try{
			ofy().save().entity(billingDoc);
			}catch(Exception e){
				e.printStackTrace();
			}
			ofy().save().entity(ngforInvoice);
			ofy().save().entity(inventity);
			createPayment(inventity,comp);
	}

	private void createPayment(Invoice inventity, Company comp) {
		// TODO Auto-generated method stub
		
		GenricServiceImpl impl=new GenricServiceImpl();
		CustomerPayment paydetails=new CustomerPayment();
		paydetails.setPersonInfo(inventity.getPersonInfo());
		paydetails.setContractCount(inventity.getContractCount());
		if(inventity.getContractStartDate()!=null){
			paydetails.setContractStartDate(inventity.getContractStartDate());
		}
		if(inventity.getContractEndDate()!=null){
			paydetails.setContractEndDate(inventity.getContractEndDate());
		}
		paydetails.setTotalSalesAmount(inventity.getTotalSalesAmount());
		paydetails.setArrayBillingDocument(inventity.getArrayBillingDocument());
		paydetails.setTotalBillingAmount(inventity.getTotalBillingAmount());
		paydetails.setInvoiceAmount(inventity.getInvoiceAmount());
		paydetails.setInvoiceCount(inventity.getCount());
		paydetails.setInvoiceDate(inventity.getInvoiceDate());
		paydetails.setInvoiceType(inventity.getInvoiceType());
		paydetails.setPaymentDate(inventity.getPaymentDate());
		double invoiceamt=inventity.getInvoiceAmount();
		int payAmt=(int)(invoiceamt);
		paydetails.setPaymentAmt(payAmt);
		paydetails.setPaymentMethod(inventity.getPaymentMethod());
		paydetails.setTypeOfOrder(inventity.getTypeOfOrder());
		paydetails.setStatus(CustomerPayment.CLOSED);
		paydetails.setOrderCreationDate(inventity.getOrderCreationDate());
		paydetails.setBranch(inventity.getBranch());
		paydetails.setAccountType(inventity.getAccountType());
		paydetails.setEmployee(inventity.getEmployee());
		paydetails.setCompanyId(inventity.getCompanyId());
		paydetails.setOrderCformStatus(inventity.getOrderCformStatus());
		paydetails.setOrderCformPercent(inventity.getOrderCformPercent());
		
		//  rohan added inventity code for sessing c
		
		if(inventity.getPaymentMethod()!= null && !inventity.getPaymentMethod().equals(""))
		{
			if(inventity.getPaymentMethod().equalsIgnoreCase("Cheque"))
			{
				paydetails.setChequeIssuedBy(inventity.getPersonInfo().getFullName());
			}
			else
			{
				paydetails.setChequeIssuedBy("");
			}
		}
		else
		{
			paydetails.setChequeIssuedBy("");
		}
		/***************** vijay*********************/
		if(inventity.getNumberRange()!=null)
		paydetails.setNumberRange(inventity.getNumberRange());
		/*******************************************/
		
		/**
		 * date 14 Feb 2017
		 * added by vijay for Setting billing period from date and To date in Payment
		 */
		if(inventity.getBillingPeroidFromDate()!=null)
			paydetails.setBillingPeroidFromDate(inventity.getBillingPeroidFromDate());
		if(inventity.getBillingPeroidToDate()!=null)
			paydetails.setBillingPeroidToDate(inventity.getBillingPeroidToDate());
		/**
		 * end here
		 */
		
		/*
		 *  nidhi
		 *   1-07-2017
		 */
		if(inventity.getSegment()!=null){
			paydetails.setSegment(inventity.getSegment());
		}
		/*
		 *  end
		 */
		
		/*
		 *  nidhi
		 *   1-07-2017
		 */
		if(inventity.getRateContractServiceId()!=0){
			paydetails.setRateContractServiceId(inventity.getRateContractServiceId());
		}
		/*
		 *  end
		 */
		
		/*
		 *  nidhi
		 *   1-07-2017
		 */
		if(inventity.getRateContractServiceId()!=0){
			paydetails.setRateContractServiceId(inventity.getRateContractServiceId());
		}
		/*
		 *  end
		 */
		
		/**
		 *  nidhi
		 *  20-07-2017
		 *   quantity and mesurement transfer to the invoice entity 
		 */
		if( inventity.getQuantity() > 0){
			paydetails.setQuantity(inventity.getQuantity());
		}
		
		if( inventity.getUom() !=null){
			paydetails.setUom(inventity.getUom());
		}
		/*
		 * end   
		 */
		
		/**
		 *  nidhi
		 *  24-07-2017
		 *  
		 */
		if( inventity.getRefNumber() !=null){
			paydetails.setRefNumber(inventity.getRefNumber());
		}
		/**
		 *  end
		 */
		NumberGeneration ngpayment = new NumberGeneration();
		ngpayment = ofy().load().type(NumberGeneration.class)
				.filter("companyId", comp.getCompanyId())
				.filter("processName", "CompanyPayment").filter("status", true)
				.first().now();
		long number = ngpayment.getNumber();
		int countforPayment = (int) number;
		paydetails.setCompanyId(comp.getCompanyId());
		paydetails.setCount(countforPayment+1);
		
		ngpayment.setCount(countforPayment+1);
		ofy().save().entity(ngpayment);
		ofy().save().entity(paydetails);
		
		
	}

	private void createServices(Contract contract, Company comp2) {
		// TODO Auto-generated method stub
		ContractServiceImplementor impl = new ContractServiceImplementor();
		try {
			impl.makeServices(contract);
		} catch (Exception e) {
			logger.log(Level.SEVERE,"Services not created:::::" + e);
			e.printStackTrace();
		}	
	}

	private void updateDeviceRegistration(HttpServletRequest req,
			HttpServletResponse resp, CustomerUser user) {
		// TODO Auto-generated method stub
		String response = "";
		String urlCalled = req.getRequestURL().toString().trim();
		String url = urlCalled.replace("http://", "");
		String[] splitUrl = url.split("\\.");
		Exception exep = null;
		try {
			comp = ofy().load().type(Company.class)
					.filter("accessUrl", splitUrl[0]).first().now();
			long companyId = comp.getCompanyId();
			
			String imeiNumber = req.getParameter("imeiNumber").trim();
			String regId = req.getParameter("regId").trim();
			String applicationName = req.getParameter("applicationName");
			
			RegisterDevice registerDeviceFound=new RegisterDevice();
			registerDeviceFound=ofy().load().type(RegisterDevice.class).filter("companyId", companyId).filter("imeiNumber", imeiNumber.trim()).first().now();
			logger.log(Level.SEVERE,"registerDeviceFound:::::::::"+registerDeviceFound);
			if(registerDeviceFound!=null){
				registerDeviceFound.setCompanyId(companyId);
				registerDeviceFound.setImeiNumber(Long.parseLong(imeiNumber));
				registerDeviceFound.setUserName(user.getUserName().trim());
				registerDeviceFound.setRegId(regId);
				registerDeviceFound.setApplicationName(applicationName);
				registerDeviceFound.setEmployeeName("");
				ofy().save().entity(registerDeviceFound);
			}else{
				logger.log(Level.SEVERE, "Device not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "ERROR in saving deviceId::::" + e);
		}
	}
	
	/*
	 * Date: 31 Jul 2017
	 * By: Apeksha Gunjal
	 * To save imei Number and other register details to server. 
	 */
	private Object submitRegisterDetails(HttpServletRequest req,
			HttpServletResponse resp) {
		// TODO Auto-generated method stub
				String urlCalled = req.getRequestURL().toString().trim();
				String url = urlCalled.replace("http://", "");
				String[] splitUrl = url.split("\\.");
				Exception exep = null;
				try {
					comp = ofy().load().type(Company.class)
							.filter("accessUrl", splitUrl[0]).first().now();
					logger.log(Level.SEVERE, "Stage One");
					long companyId = comp.getCompanyId();
					NumberGeneration ng = new NumberGeneration();
					logger.log(Level.SEVERE, "companyId: "+comp.getCompanyId());
					ng = ofy().load().type(NumberGeneration.class)
							.filter("companyId", comp.getCompanyId())
							.filter("processName", "RegisterDevice").filter("status", true)
							.first().now();

					long number = ng.getNumber();
					int count = (int) number;
					ng.setNumber(count+1);
					ofy().save().entity(ng);
					
					String applicationName = req.getParameter("applicationName").trim();
					String imeiNumber = req.getParameter("imeiNumber").trim();
					String companyCode = req.getParameter("companyCode").trim();
					String regId = req.getParameter("regId").trim();
					String userName = req.getParameter("userName").trim();

					logger.log(Level.SEVERE, "applicationName: "+applicationName);
					logger.log(Level.SEVERE, "imeiNumber: "+imeiNumber);
					logger.log(Level.SEVERE, "userName: "+userName);
					logger.log(Level.SEVERE, "companyCode: "+companyCode);
					logger.log(Level.SEVERE, "regId: "+regId);
					
					RegisterDevice registerDeviceFound=new RegisterDevice();
					
					registerDeviceFound=ofy().load().type(RegisterDevice.class).filter("companyId", companyId).filter("imeiNumber", imeiNumber.trim()).first().now();
					logger.log(Level.SEVERE,"registerDeviceFound:::::::::"+registerDeviceFound);
					logger.log(Level.SEVERE,"userName::::companyCode::::"+userName+" "+companyCode);
					if(registerDeviceFound==null){
						//Remaining Task : add Application name 
					RegisterDevice registerDevice=new RegisterDevice();
					registerDevice.setCount(count+1);
					registerDevice.setImeiNumber(Long.parseLong(imeiNumber));
					registerDevice.setUserName(userName);
					registerDevice.setRegId(regId);
					registerDevice.setCompanyCode(companyCode.trim());
					registerDevice.setCompanyId(companyId);
					registerDevice.setApplicationName(applicationName);
					registerDevice.setEmployeeName(companyCode.trim());
					ofy().save().entity(registerDevice);
					}else{
						registerDeviceFound.setCompanyId(companyId);
						registerDeviceFound.setImeiNumber(Long.parseLong(imeiNumber));
						registerDeviceFound.setUserName(userName);
						registerDeviceFound.setRegId(regId);
						registerDeviceFound.setApplicationName(applicationName);
						registerDeviceFound.setCompanyCode(companyCode.trim());
						registerDeviceFound.setEmployeeName("");
						ofy().save().entity(registerDeviceFound);
					}

					logger.log(Level.SEVERE,"Registraion completed..");
					return true;
				} catch (Exception e) {
					logger.log(Level.SEVERE, "ERROR::::::" + e);
					e.printStackTrace();
					return false;
				}
	}

	private Object submitPaymentDocument(HttpServletRequest req,
			HttpServletResponse resp) {
		// TODO Auto-generated method stub
		String urlCalled = req.getRequestURL().toString().trim();
		String url = urlCalled.replace("http://", "");
		String[] splitUrl = url.split("\\.");
		Exception exep = null;
		try {
			comp = ofy().load().type(Company.class)
					.filter("accessUrl", splitUrl[0]).first().now();
			logger.log(Level.SEVERE, "Stage One");
			long companyId = comp.getCompanyId();
			String paymentCount = req.getParameter("paymentCount");
			double paymentRecieved = Double.parseDouble(req.getParameter(
					"amountPaid").trim());
			String paymentRefNo = req.getParameter("refNo").trim();
			CustomerPayment customerPayment = ofy().load()
					.type(CustomerPayment.class).filter("companyId", companyId)
					.filter("count", Integer.parseInt(paymentCount)).first()
					.now();
			customerPayment.setStatus(CustomerPayment.CLOSED);
			customerPayment.setPaymentMethod("ONLINE");
			customerPayment.setPaymentReceived(paymentRecieved);
			customerPayment.setReferenceNo(paymentRefNo);
			ofy().save().entity(customerPayment);

			return true;
		} catch (Exception e) {
			logger.log(Level.SEVERE, "ERROR::::::" + e);
			e.printStackTrace();
			return false;
		}
	}

	private Object renewContract(HttpServletRequest req,
			HttpServletResponse resp) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE, "Inside Create Lead");
		String urlCalled = req.getRequestURL().toString().trim();
		String url = urlCalled.replace("http://", "");
		String[] splitUrl = url.split("\\.");
		Exception exep = null;
		try {
			comp = ofy().load().type(Company.class)
					.filter("accessUrl", splitUrl[0]).first().now();
			logger.log(Level.SEVERE, "Stage One");
			long companyId = comp.getCompanyId();
			String contractId = req.getParameter("contractId");
			Contract contract = ofy().load().type(Contract.class)
					.filter("companyId", companyId)
					.filter("count", Integer.parseInt(contractId)).first()
					.now();
			Contract contract1 = new Contract();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(contract.getEndDate());
			calendar.add(Calendar.DAY_OF_YEAR, 1);
			Date newstartdate = calendar.getTime();
			contract1.setCinfo(contract.getCinfo());
			contract1.setBranch(contract.getBranch());
			contract1.setEmployee(contract.getEmployee().trim());
			
			contract1.setPaymentMethod(contract.getPaymentMethod());
			contract1.setApproverName(contract.getApproverName().trim());
			contract1.setGroup(contract.getGroup());
			contract1.setType(contract.getType());
			contract1.setCategory(contract.getCategory());
			contract1.setCreditPeriod(contract.getCreditPeriod());
			contract1.setCreationDate(sdf.parse(sdf.format(new Date())));
			contract1.setDescription(contract.getDescription());
			contract1.setPaymentTermsList(contract.getPaymentTermsList());
			contract1.setTotalAmount(contract.getTotalAmount());
			contract1.setProductTaxes(contract.getProductTaxes());
			contract1.setProductCharges(contract.getProductCharges());
			contract1.setRefContractCount(contract.getCount());
			contract1.setNetpayable(contract.getNetpayable());
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			contract1.setStartDate(sdf.parse(sdf.format(newstartdate)));
			long diff = sdf.parse(sdf.format(contract.getEndDate())).getTime()
					- sdf.parse(sdf.format(contract.getStartDate())).getTime();
			int numOfDays = (int) (diff / (1000 * 60 * 60 * 24));
			logger.log(Level.SEVERE, "Number of Days::::" + numOfDays);
			Calendar calendarEnd = Calendar.getInstance();
			calendarEnd.setTime(sdf.parse(sdf.format(newstartdate)));
			calendarEnd.add(Calendar.DAY_OF_YEAR, numOfDays);
			Date newenddate = calendarEnd.getTime();
			contract1.setEndDate(sdf.parse(sdf.format(newenddate)));

			contract1.setContractDate(sdf.parse(sdf.format(newstartdate)));
			List<SalesLineItem> itemList = new ArrayList<SalesLineItem>();
			contract1.setItems(contract.getItems());
			for (SalesLineItem item : contract1.getItems()) {
				item.setStartDate(sdf.parse(sdf.format(newstartdate)));
				itemList.add(item);
			}
			contract1.setItems(itemList);
			Calendar c = Calendar.getInstance();
			try {
				c.setTime(sdf.parse(sdf.format(contract1.getContractDate())));
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
			contract1.setServiceScheduleList(contract.getServiceScheduleList());
			List<ServiceSchedule> serviceScheduleList = getServiceScheduleList(
					contract, contract1, contract1.getItems(),
					contract1.getServiceScheduleList(), c);
			contract1.setServiceScheduleList(serviceScheduleList);
			contract1.setStatus(Contract.CREATED);
			contract1.setCompanyId(contract.getCompanyId());
			contract1.setCount(0);
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(contract1);
			contract.setRenewFlag(true);
			ofy().save().entities(contract);
			return true;
		} catch (Exception e) {
			logger.log(Level.SEVERE, "ERROR::::::" + e);
			e.printStackTrace();
			return false;
		}
	}

	private List<ServiceSchedule> getServiceScheduleList(Contract contract,
			Contract contract1, List<SalesLineItem> listofSaleLineItem,
			List<ServiceSchedule> list, Calendar c) {
		// TODO Auto-generated method stub

		List<ServiceSchedule> serviceScheduleList = new ArrayList<ServiceSchedule>();
		for (int i = 0; i < listofSaleLineItem.size(); i++) {
			String previousDate = "";
			int gapOfService = (int) (listofSaleLineItem.get(i).getDuration() / (listofSaleLineItem
					.get(i).getNumberOfServices() * listofSaleLineItem.get(i)
					.getQty()));
			for (int j = 0; j < contract.getServiceScheduleList().size(); j++) {
				ServiceSchedule serviceSchedule = new ServiceSchedule();
				serviceSchedule = contract.getServiceScheduleList().get(j);
				if (j + 1 == 1) {
					SimpleDateFormat sdfDateP = new SimpleDateFormat(
							"dd-MM-yyyy");
					sdfDateP.setTimeZone(TimeZone.getTimeZone("IST"));
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));

					try {
						serviceSchedule.setScheduleServiceDate(sdfDateP
								.parse(sdfDateP.format(contract
										.getContractDate())));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						logger.log(Level.SEVERE, "Error in j==1" + e);
						e.printStackTrace();
					}
					previousDate = sdfDateP.format(contract.getContractDate());
				} else {
					SimpleDateFormat sdfDateS = new SimpleDateFormat(
							"dd-MM-yyyy");
					sdfDateS.setTimeZone(TimeZone.getTimeZone("IST"));
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));

					Calendar c1 = Calendar.getInstance();
					try {
						c1.setTime(sdfDateS.parse(previousDate));
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						logger.log(Level.SEVERE, "Error inn j>1:::::::" + e1);

						e1.printStackTrace();
					}
					logger.log(Level.SEVERE, "gapOfService:::::::"
							+ gapOfService);
					try {
						c.add(Calendar.DATE, gapOfService);
						Date date1 = c.getTime();
						// serviceSchedule2.setScheduleServiceDay();

						serviceSchedule.setScheduleServiceDate(sdfDateS
								.parse(sdfDateS.format(date1)));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						logger.log(Level.SEVERE, "Error in Adding days:::::::"
								+ e);
						e.printStackTrace();
					}
				}
				serviceScheduleList.add(serviceSchedule);
			}
		}
		return serviceScheduleList;
	}

	private Object createLead(HttpServletRequest req, HttpServletResponse resp) {
		// TODO Auto-generated method stub

		logger.log(Level.SEVERE, "Inside Create Lead");
		String urlCalled = req.getRequestURL().toString().trim();
		String url = urlCalled.replace("http://", "");
		String[] splitUrl = url.split("\\.");
		Exception exep = null;
		try {
			comp = ofy().load().type(Company.class)
					.filter("accessUrl", splitUrl[0]).first().now();
			logger.log(Level.SEVERE, "Stage One");
			long companyId = comp.getCompanyId();
			String lead_title = req.getParameter("title").trim();
			String lead_description = req.getParameter("description").trim();
			String lead_customerId = req.getParameter("customerId").trim();

			Customer customer = ofy().load().type(Customer.class)
					.filter("companyId", companyId)
					.filter("count", Integer.parseInt(lead_customerId)).first()
					.now();

			Lead lead = new Lead();
			lead.setTitle(lead_title);
			lead.setDescription(lead_description);
			lead.setStatus("Created");
			lead.setPriority("High");
			lead.setCategory("Application");
			PersonInfo personInfo = new PersonInfo();
			personInfo.setCount(customer.getCount());
			if (customer.isCompany()) {
				personInfo.setFullName(customer.getCompanyName());
			} else {
				personInfo.setFullName(customer.getFullname());
			}
			personInfo.setCellNumber(customer.getCellNumber1());
			personInfo.setPocName(customer.getFullname());
			lead.setCompanyId(companyId);
			lead.setPersonInfo(personInfo);
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(lead);
			return true;
		} catch (Exception e) {
			logger.log(Level.SEVERE, "ERROR:::::::" + e);
			return false;
		}
	}

	private Object serviceFeedback(HttpServletRequest req,
			HttpServletResponse resp) {
		String urlCalled = req.getRequestURL().toString().trim();
		String url = urlCalled.replace("http://", "");
		String[] splitUrl = url.split("\\.");
		Exception exep = null;
		try {
			comp = ofy().load().type(Company.class)
					.filter("accessUrl", splitUrl[0]).first().now();
			logger.log(Level.SEVERE, "Stage One");
			long companyId = comp.getCompanyId();
			float feedbackRating = Float.parseFloat(req.getParameter(
					"feedbackRating").trim());
			logger.log(Level.SEVERE, "feedbackRating::::::::::"
					+ feedbackRating);
			String custRemark = req.getParameter("custRemark").trim();
			logger.log(Level.SEVERE, "custRemark::::::::::" + custRemark);
			String serviceId = req.getParameter("serviceId").trim();
			logger.log(Level.SEVERE, "serviceId::::::::::" + serviceId);

			Service service = ofy().load().type(Service.class)
					.filter("companyId", companyId)
					.filter("count", Integer.parseInt(serviceId)).first().now();
			logger.log(Level.SEVERE, "Service::::::" + service);
			// Adding in table
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			String newDate = sdf.format(new Date());

			service.setServiceCompleteRemark(custRemark.trim());

			if (feedbackRating > 0f && feedbackRating <= 1f) {
				service.setCustomerFeedback("Poor");
			} else if (feedbackRating > 1f && feedbackRating <= 2f) {
				service.setCustomerFeedback("Average");
			} else if (feedbackRating > 2f && feedbackRating <= 3f) {
				service.setCustomerFeedback("Good");
			} else if (feedbackRating > 3f && feedbackRating <= 4f) {
				service.setCustomerFeedback("Very Good");
			} else if (feedbackRating > 4f && feedbackRating <= 5f) {
				service.setCustomerFeedback("Excellent");
			} else {
				service.setCustomerFeedback("");
			}

			ofy().save().entity(service).now();

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private String getCustomerDetails(CustomerUser user) {
		// TODO Auto-generated method stub
		JSONObject obj = new JSONObject();
		obj.put("user_count", user.getCinfo().getCount());
		obj.put("user_fullName", user.getCinfo().getFullName());
		obj.put("user_PocName", user.getCinfo().getFullName());
		obj.put("user_CellNum", user.getCinfo().getCellNumber());
		obj.put("userName", user.getUserName());
		JSONArray jsonArray=new JSONArray();
		JSONObject objAddress=new JSONObject();
		Customer customer=ofy().load().type(Customer.class).filter("companyId", user.getCompanyId()).filter("count",user.getCinfo().getCount()).first().now();
		objAddress.put("addressLine1",customer.getSecondaryAdress().getAddrLine1().trim());
		objAddress.put("addressLine2",customer.getSecondaryAdress().getAddrLine2().trim());
		objAddress.put("landmark",customer.getSecondaryAdress().getLandmark().trim());
		objAddress.put("locality",customer.getSecondaryAdress().getLocality().trim());
		objAddress.put("state",customer.getSecondaryAdress().getState().trim());
		objAddress.put("city",customer.getSecondaryAdress().getCity().trim());
		objAddress.put("country",customer.getSecondaryAdress().getCountry().trim());
		objAddress.put("pin",customer.getSecondaryAdress().getPin()+"");
		
		jsonArray.put(objAddress);
		obj.put("addressArray",jsonArray);
		String jsonString = obj.toJSONString().replace("\\\\", "");
		logger.log(Level.SEVERE, "jsonString::::" + jsonString);
		return jsonString;
	}

	private Object loginService(HttpServletRequest req, HttpServletResponse resp) {
		// TODO Auto-generated method stub
		String response = "";
		String urlCalled = req.getRequestURL().toString().trim();
		String url = urlCalled.replace("http://", "");
		String[] splitUrl = url.split("\\.");
		Exception exep = null;
		try {
			comp = ofy().load().type(Company.class)
					.filter("accessUrl", splitUrl[0]).first().now();
			long companyId = comp.getCompanyId();
			String userName = req.getParameter("username");
			String passWord = req.getParameter("password");
			
			
			CustomerUser user = ofy().load().type(CustomerUser.class)
					.filter("companyId", companyId)
					.filter("userName", userName.trim())
					.filter("password", passWord.trim()).first().now();
			if (user != null) {
				try{
					String imeiNumber=req.getParameter("imeiNumber");

					RegisterDevice registerDevice=ofy().load().type(RegisterDevice.class).filter("companyId", comp.getCompanyId()).filter("imeiNumber",Long.parseLong(imeiNumber.trim())).first().now();
					
					if(registerDevice!=null){
						logger.log(Level.SEVERE,"registerDevice"+registerDevice.getImeiNumber());
						registerDevice.setEmployeeName(user.getCustomerName());
						registerDevice.setUserName(user.getUserName());
						registerDevice.setUserId(user.getUserName());
						ofy().save().entity(registerDevice);
					}
					}catch(Exception e){
						logger.log(Level.SEVERE,"error in imei number");
					}
				return user;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "ERROR in loginService::::" + e);
			return false;
		}
	}

	private Object complainServices(HttpServletRequest req,
			HttpServletResponse resp) {
		
		String response = "";
		String urlCalled = req.getRequestURL().toString().trim();
		String url = urlCalled.replace("http://", "");
		String[] splitUrl = url.split("\\.");
		Exception exep = null;

		try {
			comp = ofy().load().type(Company.class)
					.filter("accessUrl", splitUrl[0]).first().now();
			long companyId = comp.getCompanyId();
			String contractId = req.getParameter("contractId");
			String serviceId = req.getParameter("serviceId");
			String serviceEngg = req.getParameter("serviceEngg");
			String serviceDate = req.getParameter("serviceDate");
			String serviceName = req.getParameter("serviceName");
			String description = req.getParameter("description");
			String customerId = req.getParameter("customerId");

			Complain complain = new Complain();

			Service service = ofy().load().type(Service.class)
					.filter("companyId", companyId)
					.filter("count", Integer.parseInt(serviceId.trim()))
					.first().now();
			/*
			 * Customer Info
			 */
			PersonInfo personInfo = new PersonInfo();
			personInfo.setCount(service.getPersonInfo().getCount());
			personInfo.setFullName(service.getPersonInfo().getFullName());
			personInfo.setPocName(service.getPersonInfo().getPocName());
			personInfo.setCellNumber(service.getPersonInfo().getCellNumber());
			complain.setPersoninfo(personInfo);

			/*
			 * Reference Details
			 */
			complain.setExistingContractId(service.getContractCount());
			complain.setExistingServiceDate(service.getServiceDate());
			complain.setExistingServiceId(service.getCount());
			complain.setServiceEngForExistingService(service.getEmployee());
			complain.setContrtactId(service.getContractCount());
			complain.setServiceId(service.getCount());
			complain.setServiceDate(service.getServiceDate());
			complain.setServicetime(service.getServiceTime());
			complain.setServiceDetails(service.getCount() + " "
					+ service.getServiceDate() + " " + service.getProductName());
			/*
			 * Ticket Information
			 */

			complain.setDate(sdf.parse(sdf.format(new Date())));
			SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			sdf1.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			String[] date_time = sdf1.format(new Date()).split(" ");

			complain.setTime(date_time[1].trim());
			complain.setCallerName(service.getPersonInfo().getFullName());
			complain.setCallerCellNo(service.getPersonInfo().getCellNumber());
			complain.setCompStatus("Created");
			// complain.setSalesPerson(salesPerson);
			complain.setPriority("High");
			complain.setComplainDate(sdf.parse(sdf.format(new Date())));
			complain.setDestription(description);

			/*
			 * Product Info
			 */
			ProductInfo pic = new ProductInfo();
			pic.setProdID(service.getProduct().getCount());
			pic.setProductName(service.getProduct().getProductName());
			pic.setProductCode(service.getProduct().getProductCode());
			complain.setPic(pic);
			complain.setCompanyId(companyId);

			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(complain);

		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "ERROR in complain::::" + e);
			return false;
		}
		return true;
	}

	private Object rescheduleServices(HttpServletRequest req,
			HttpServletResponse resp) {
		// TODO Auto-generated method stub
		String response = "";
		String urlCalled = req.getRequestURL().toString().trim();
		String url = urlCalled.replace("http://", "");
		String[] splitUrl = url.split("\\.");
		Exception exep = null;

		try {

			comp = ofy().load().type(Company.class)
					.filter("accessUrl", splitUrl[0]).first().now();
			String serviceId = req.getParameter("serviceId").trim();
			String companyId = comp.getCompanyId() + "";
			String rescheduleDate = req.getParameter("rescheduleDate").trim();
			String rescheduleTime = req.getParameter("rescheduleTime").trim();
			String rescheduleReason = req.getParameter("rescheduleReason")
					.trim();

			logger.log(Level.SEVERE, "serviceId" + serviceId);
			logger.log(Level.SEVERE, "companyId" + companyId);
			logger.log(Level.SEVERE, "rescheduleDate" + rescheduleDate);
			logger.log(Level.SEVERE, "rescheduleTime" + rescheduleTime);
			logger.log(Level.SEVERE, "rescheduleReason" + rescheduleReason);

			Service service = ofy().load().type(Service.class)
					.filter("companyId", Long.parseLong(companyId))
					.filter("count", Long.parseLong(serviceId)).first().now();

			logger.log(Level.SEVERE, "service" + service);
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));

			Date oldServiceDate = service.getServiceDate(); // Old ServiceDate
			service.setServiceDate(sdf.parse(rescheduleDate));
			service.setStatus(Service.SERVICESTATUSRESCHEDULE);
			String[] time = rescheduleTime.split(":");
			int hours = Integer.parseInt(time[0]);
			int mins = Integer.parseInt(time[1].substring(0, 2));
			logger.log(Level.SEVERE, "hours" + hours);
			logger.log(Level.SEVERE, "mins" + mins);

			if (hours > 12) {
				int hoursDecrease = 0;
				if (hours != 12) {
					hoursDecrease = 12;
				}
				if (mins < 10) {
					service.setServiceTime((hours - hoursDecrease) + ":" + "0"
							+ mins + " PM");
				} else {
					service.setServiceTime((hours - hoursDecrease) + ":" + mins
							+ " PM");
				}

				logger.log(Level.SEVERE, "Service Time::::::" + (hours - 12)
						+ ":" + mins + " PM");
			} else {
				logger.log(Level.SEVERE, "Service Time::::::" + hours + ":"
						+ mins + " AM");
				service.setServiceTime(hours + ":" + mins + " AM");
			}

			service.setReasonForChange(rescheduleReason);

			ModificationHistory modHistory = new ModificationHistory();
			modHistory.setOldServiceDate(oldServiceDate);
			modHistory.setResheduleDate(sdf.parse(rescheduleDate));

			// SimpleDateFormat sdf1=new
			// SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
			// sdf1.setTimeZone(TimeZone.getTimeZone("IST"));
			modHistory.setSystemDate(new Date());
			if (hours >= 12) {
				int hoursDecrease = 0;
				if (hours != 12) {
					hoursDecrease = 12;
				}
				if (mins < 10) {
					modHistory.setResheduleTime((hours - hoursDecrease) + ":"
							+ "0" + mins + " PM");
				} else {
					modHistory.setResheduleTime((hours - hoursDecrease) + ":"
							+ mins + " PM");
				}

				logger.log(Level.SEVERE, "Service Time::::::"
						+ (hours - hoursDecrease) + ":" + mins + " PM");

			} else {
				logger.log(Level.SEVERE, "Service Time::::::" + hours + ":"
						+ mins + " AM");
				modHistory.setResheduleTime(hours + ":" + mins + " AM");
			}
			modHistory.setReason(rescheduleReason);

			// List<ModificationHistory> modList=new
			// ArrayList<ModificationHistory>();
			// modList.add(modHistory);
			// ModificationHistoryTable table = new ModificationHistoryTable();
			// table.getDataprovider().getList().add(modHistory);
			service.getListHistory().add(modHistory);

			ofy().save().entity(service).now();
			try{
				CustomerUser customerUser = ofy()
						.load()
						.type(CustomerUser.class)
						.filter("companyId", comp.getCompanyId())
						.filter("cinfo.count",
								service.getPersonInfo().getCount()).first()
						.now();
				sendPushNotificationToPrioraForReschedule(comp.getCompanyId(),
						AppConstants.IAndroid.EVA_PRIORA,customerUser, response,service,comp);
				
			}catch(Exception e){
				logger.log(Level.SEVERE,"Error in send notification to priora and technician");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private void sendPushNotificationToPrioraForReschedule(Long companyId,
			String applicationName, CustomerUser customerUser, String response, Service service,
			Company comp2) {
		response="event:Service Rescheduled"+"$"+customerUser.getCustomerName()+" has rescheduled "+service.getProductName()+" service on "+sdf.format(service.getServiceDate())+" "+service.getServiceTime();//getJSONResponse(service);
		
		List<RegisterDevice> device = ofy().load().type(RegisterDevice.class)
				.filter("companyId", companyId)
				.filter("applicationName", applicationName).list();
		for (RegisterDevice registerDevice : device) {
			String deviceName = registerDevice.getRegId().trim();
			logger.log(Level.SEVERE,"deviceName"+deviceName);
			URL url = null;
			try {
				url = new URL("https://fcm.googleapis.com/fcm/send");
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			HttpURLConnection conn = null;
			try {
				conn = (HttpURLConnection) url.openConnection();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			conn.setDoOutput(true);
			try {
				conn.setRequestMethod("POST");
			} catch (ProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			conn.setRequestProperty("Content-Type",
					"application/json; charset=UTF-8");
			conn.setRequestProperty("Authorization", "key="
					+ comp2.getFcmServerKeyForPriora().trim());
			conn.setDoOutput(true);
			conn.setDoInput(true);

			try {
				conn.setRequestMethod("POST");
			} catch (ProtocolException e) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE, "ProtocolException ERROR::::::::::" + e);
				e.printStackTrace();
			}
			OutputStream os = null;
			try {
				os = conn.getOutputStream();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
				e.printStackTrace();
			}
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
			// Log.d("Test", "From Get Post Method" + getPostData(values));
			try {
				writer.write(createJSONData(response, deviceName));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE, "IOException2 ERROR::::::::::" + e);
				e.printStackTrace();
			}
			try {
				writer.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE, "IOException3 ERROR::::::::::" + e);
				e.printStackTrace();
			}

			InputStream is = null;
			try {
				is = conn.getInputStream();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
				e.printStackTrace();
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String temp;
			String data = null;
			try {
				while ((temp = br.readLine()) != null) {
					data = data + temp;
				}
				logger.log(Level.SEVERE, "data data::::::::::" + data);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
				e.printStackTrace();
			}

			logger.log(Level.SEVERE, "Data::::::::::" + data);	
		}
		if(service.getEmployee()!=null&&!service.getEmployee().equalsIgnoreCase("")){
				User user=ofy().load().type(User.class).filter("companyId", comp.getCompanyId()).filter("employeeName", service.getEmployee().trim()).first().now();
			 	RegisterDevice registerDevice=ofy().load().type(RegisterDevice.class).filter("companyId",comp.getCompanyId()).filter("applicationName",AppConstants.IAndroid.EVA_PEDIO).filter("userName", user.getUserName()).first().now();

				String deviceName = registerDevice.getRegId().trim();
				logger.log(Level.SEVERE,"deviceName"+deviceName);
				URL url = null;
				try {
					url = new URL("https://fcm.googleapis.com/fcm/send");
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				HttpURLConnection conn = null;
				try {
					conn = (HttpURLConnection) url.openConnection();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				conn.setDoOutput(true);
				try {
					conn.setRequestMethod("POST");
				} catch (ProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				conn.setRequestProperty("Content-Type",
						"application/json; charset=UTF-8");
				conn.setRequestProperty("Authorization", "key="
						+ comp2.getFcmServerKeyForPriora().trim());
				conn.setDoOutput(true);
				conn.setDoInput(true);

				try {
					conn.setRequestMethod("POST");
				} catch (ProtocolException e) {
					// TODO Auto-generated catch block
					logger.log(Level.SEVERE, "ProtocolException ERROR::::::::::" + e);
					e.printStackTrace();
				}
				OutputStream os = null;
				try {
					os = conn.getOutputStream();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
					e.printStackTrace();
				}
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
				// Log.d("Test", "From Get Post Method" + getPostData(values));
				try {
					writer.write(createJSONData(response, deviceName));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.log(Level.SEVERE, "IOException2 ERROR::::::::::" + e);
					e.printStackTrace();
				}
				try {
					writer.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.log(Level.SEVERE, "IOException3 ERROR::::::::::" + e);
					e.printStackTrace();
				}

				InputStream is = null;
				try {
					is = conn.getInputStream();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
					e.printStackTrace();
				}
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				String temp;
				String data = null;
				try {
					while ((temp = br.readLine()) != null) {
						data = data + temp;
					}
					logger.log(Level.SEVERE, "data data::::::::::" + data);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
					e.printStackTrace();
				}

				logger.log(Level.SEVERE, "Data::::::::::" + data);	
			
		}
	}

	private String createJSONData(String message, String deviceName) {
		// TODO Auto-generated method stub
		org.json.simple.JSONObject jObj1 = new org.json.simple.JSONObject();
		jObj1.put("message", message);

		org.json.simple.JSONObject jObj = new org.json.simple.JSONObject();
		jObj.put("data", jObj1);
		jObj.put("to", deviceName);

		String jsonString = jObj.toJSONString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE, "JSON Data Sent:::::::" + jsonString);

		return jsonString;
	}
}
