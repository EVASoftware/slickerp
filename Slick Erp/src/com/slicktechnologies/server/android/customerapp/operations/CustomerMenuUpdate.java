package com.slicktechnologies.server.android.customerapp.operations;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.server.ContractServiceImplementor;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.addhocprinting.CreatePayRollPDFServlet;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;

public class CustomerMenuUpdate extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 825429186356670479L;
	Logger logger=Logger.getLogger("AnalyticsStatusUpdate.class");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		String menuName= req.getParameter("menuName");
		String docId=req.getParameter("docId");
		
		String urlCalled = req.getRequestURL().toString().trim();
//		String url = urlCalled.replace("http://", "");
//		String[] splitUrl = url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		Exception exep = null;
		// try {
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		logger.log(Level.SEVERE, "Stage One");
		long companyId=comp.getCompanyId();
		
		String response=performActionOnMenu(comp,menuName,docId,req);
		
		resp.getWriter().print(response);
	}

	private String performActionOnMenu(Company comp, String menuName, String docId, HttpServletRequest req) {
		// TODO Auto-generated method stub
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		String response="Failed";
		if(menuName.trim().equalsIgnoreCase("Renew Contract")){
			String data=req.getParameter("data");//createInvoice
			
			Contract contract = ofy().load().type(Contract.class)
					.filter("companyId", comp.getCompanyId())
					.filter("count", Integer.parseInt(docId)).first()
					.now();
			Contract newContract = new Contract();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(contract.getEndDate());
			calendar.add(Calendar.DAY_OF_YEAR, 1);
			Date newstartdate = calendar.getTime();
			newContract.setCinfo(contract.getCinfo());
			newContract.setBranch(contract.getBranch());
			newContract.setEmployee(contract.getEmployee().trim());
			
			newContract.setPaymentMethod(contract.getPaymentMethod());
			newContract.setApproverName(contract.getApproverName().trim());
			newContract.setGroup(contract.getGroup());
			newContract.setType(contract.getType());
			newContract.setCategory(contract.getCategory());
			newContract.setCreditPeriod(contract.getCreditPeriod());
			try {
				newContract.setCreationDate(sdf.parse(sdf.format(new Date())));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			newContract.setDescription(contract.getDescription());
			newContract.setPaymentTermsList(contract.getPaymentTermsList());
			newContract.setTotalAmount(contract.getTotalAmount());
			newContract.setProductTaxes(contract.getProductTaxes());
			newContract.setProductCharges(contract.getProductCharges());
			newContract.setRefContractCount(contract.getCount());
			newContract.setNetpayable(contract.getNetpayable());
			
			try {
				newContract.setStartDate(sdf.parse(sdf.format(newstartdate)));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			long diff = 0;
			try {
				diff = sdf.parse(sdf.format(contract.getEndDate())).getTime()
						- sdf.parse(sdf.format(contract.getStartDate())).getTime();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			int numOfDays = (int) (diff / (1000 * 60 * 60 * 24));
			logger.log(Level.SEVERE, "Number of Days::::" + numOfDays);
			Calendar calendarEnd = Calendar.getInstance();
			try {
				calendarEnd.setTime(sdf.parse(sdf.format(newstartdate)));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			calendarEnd.add(Calendar.DAY_OF_YEAR, numOfDays);
			Date newenddate = calendarEnd.getTime();
			try {
				newContract.setEndDate(sdf.parse(sdf.format(newenddate)));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				newContract.setContractDate(sdf.parse(sdf.format(newstartdate)));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			List<SalesLineItem> itemList = new ArrayList<SalesLineItem>();
//			contract1.setItems(contract.getItems());
			for (SalesLineItem item : contract.getItems()) {
				try {
					item.setStartDate(sdf.parse(sdf.format(newstartdate)));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				itemList.add(item);
			}
			newContract.setItems(itemList);
			Calendar c = Calendar.getInstance();
			try {
				c.setTime(sdf.parse(sdf.format(newContract.getContractDate())));
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
//			newContract.setServiceScheduleList(contract.getServiceScheduleList());
			List<ServiceSchedule> serviceScheduleList = getServiceScheduleList(
					contract, newContract, newContract.getItems(),
					newContract.getServiceScheduleList(), c);
			newContract.setServiceScheduleList(serviceScheduleList);
			newContract.setStatus(Contract.APPROVED);
			NumberGeneration ng = new NumberGeneration();
			ng = ofy().load().type(NumberGeneration.class)
					.filter("companyId", comp.getCompanyId())
					.filter("processName", "Contract")
					.filter("status", true).first().now();
			long number = ng.getNumber();
			int count = (int) number;
			int blabla = count + 1;
			logger.log(Level.SEVERE, "Count+1:::::::::" + blabla);
			newContract.setCount(count + 1);
			final int contractId = blabla;
			newContract.setCompanyId(comp.getCompanyId());
			Exception exep = null;
//			try {
//				ofy().save().entity(contract);
//			} catch (Exception e) {
//				exep = e;
//			}

			ng.setNumber(count + 1);
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(ng);
			
			ofy().save().entities(newContract);
			contract.setRenewFlag(true);
			ofy().save().entities(contract);
			
			createService(req, comp,newContract);
			createBilling(req, comp.getCompanyId(), newContract,data);
			return "Successfull";
		}
		return response;
	}
	
	private void createBilling(HttpServletRequest req, Long companyId,
			Contract newContract, String data) {
//		Contract contract = ofy().load().type(Contract.class)
//				.filter("companyId", companyId).filter("count", contractId)
//				.first().now();
		Contract contract=newContract;
		logger.log(Level.SEVERE, "contract in create billing::" + contract);
		// Customer customer = ofy().load().type(Customer.class)
		// .filter("companyId", companyId)
		// .filter("count", contract.getCustomerId()).first().now();
		if (contract != null) {
			BillingDocument billingDoc = new BillingDocument();
			// Setting customer information
			PersonInfo personInfo = new PersonInfo();
			personInfo.setCount(contract.getCinfo().getCount());
			personInfo.setFullName(contract.getCinfo().getFullName());
			personInfo.setCellNumber(contract.getCinfo().getCellNumber());
			personInfo.setPocName(contract.getCinfo().getPocName());
			billingDoc.setPersonInfo(personInfo);
			// Setting contract information
			/*** Order Informaton *****/
			billingDoc.setContractCount(contract.getCount());
			logger.log(Level.SEVERE,"The Contract Count is" + contract.getCount());
			billingDoc.setTotalSalesAmount(contract.getNetpayable());

			logger.log(Level.SEVERE,"Net Payable1 is :" + contract.getNetpayable());

			billingDoc.setAccountType("AR");
			/*** Done with Order Informaton *****/

			/********* Payment Terms are Hard Coded ********************/
			ArrayList<PaymentTerms> listofpayterms = new ArrayList<PaymentTerms>();
			PaymentTerms paymentterms = new PaymentTerms();
			paymentterms.setPayTermDays(0);
			paymentterms.setPayTermPercent(100d);
			paymentterms.setPayTermComment("Default by Batch");
			listofpayterms.add(paymentterms);
			billingDoc.setArrPayTerms(listofpayterms);
			/************ Done with Payment Terms *********************/

			/**** Billing Info **********/
			NumberGeneration ngforbilling = ofy().load()
					.type(NumberGeneration.class)
					.filter("companyId", companyId)
					.filter("processName", "BillingDocument")
					.filter("status", true).first().now();

			long number1 = ngforbilling.getNumber();
			int countforbilling = (int) number1;
			billingDoc.setCount(countforbilling + 1);
			SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
			sdfDate.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			billingDoc.setBillingDate(contract.getContractDate());
			billingDoc.setApproverName(contract.getEmployee());
			billingDoc.setEmployee(contract.getEmployee());
			billingDoc.setBranch(contract.getBranch());
			/**** Done with Billing Info **********/

			/********** Billing Document Information ****************/
			billingDoc.getBillingDocumentInfo().setBillId(countforbilling + 1);
			billingDoc.getBillingDocumentInfo().setBillingDate(new Date());
			billingDoc.getBillingDocumentInfo().setBillAmount(
					contract.getNetpayable());
			logger.log(Level.SEVERE,"net payable amount 2:"
					+ contract.getNetpayable());
			billingDoc.getBillingDocumentInfo().setBillstatus(
					BillingDocument.CREATED);
			/********** Billing Document Information ****************/

			/************ Product Details ***************/

			int noOfProduct = contract.getItems().size();
			logger.log(Level.SEVERE,"The No of Product" + noOfProduct);

			ArrayList<SalesOrderProductLineItem> productTable = new ArrayList<SalesOrderProductLineItem>();

			for (int i = 0; i < noOfProduct; i++) {

				SalesOrderProductLineItem product = new SalesOrderProductLineItem();

				logger.log(Level.SEVERE,"product.setProdId(contract.getItems().get(i).getPrduct().getCount())"
								+ (contract.getItems().get(i).getPrduct()
										.getCount()));

				if (contract.getItems().get(i).getPrduct().getCount() != 0) {

					product.setProdId(contract.getItems().get(i).getPrduct()
							.getCount());

				}

				product.setProdCategory(contract.getItems().get(i)
						.getProductCategory());
				product.setProdCode(contract.getItems().get(i).getProductCode());
				product.setProdName(contract.getItems().get(i).getProductName());
				product.setQuantity(contract.getItems().get(i).getQty());
				product.setPrice(contract.getItems().get(i).getPrice());
				product.setVatTax(contract.getItems().get(i).getVatTax());
				product.setServiceTax(contract.getItems().get(i)
						.getServiceTax());
				// Tax tax=new Tax();
				// tax.setPercentage(14.5);
				// product.setServiceTax(tax);
				product.setTotalAmount(contract.getItems().get(i).getPrice());
				product.setBaseBillingAmount((contract.getItems().get(i)
						.getQty()) * (contract.getItems().get(i).getPrice()));
				product.setPaymentPercent(100);
				// PAyable amount bacha hai

				productTable.add(product);
			}

			billingDoc.setSalesOrderProducts(productTable);
			logger.log(Level.SEVERE,"contract.getNetpayable()"
					+ contract.getNetpayable());
			billingDoc.setTotalBillingAmount(contract.getNetpayable());
			if(contract.getNumberRange()!=null){
				billingDoc.setNumberRange(contract.getNumberRange());
			}
			// billing.setTotalSalesAmount(billing.getTotalBillingAmount());
			// logger.log(Level.SEVERE,"The Total Value is :"+billing.getTotalBillingAmount());
			/************ Finish with Product Details ***************/

			/***** Tax Table *************/

			List<ContractCharges> taxTableList = new ArrayList<ContractCharges>();

			for (int i = 0; i < contract.getProductTaxes().size(); i++) {
				ContractCharges taxTable = new ContractCharges();
				taxTable.setTaxChargeName(contract.getProductTaxes().get(i)
						.getChargeName());
				taxTable.setTaxChargePercent(contract.getProductTaxes().get(i)
						.getChargePercent());
				taxTable.setTaxChargeAssesVal(contract.getProductTaxes().get(i)
						.getAssessableAmount());
				taxTable.setPayableAmt(contract.getProductTaxes().get(i)
						.getChargePayable());
				taxTableList.add(taxTable);
			}
			billingDoc.setBillingTaxes(taxTableList);
			int invoiceCount=0;
			if(data.trim().equalsIgnoreCase("createInvoice")){
				billingDoc.setStatus(BillingDocument.BILLINGINVOICED);
				try {
					billingDoc.setInvoiceDate(sdfDate.parse(sdfDate.format(new Date())));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					billingDoc.setPaymentDate(sdfDate.parse(sdfDate.format(new Date())));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				NumberGeneration ng = new NumberGeneration();
				ng = ofy().load().type(NumberGeneration.class)
						.filter("companyId", companyId)
						.filter("processName", "Invoice")
						.filter("status", true).first().now();
				long number = ng.getNumber();
				int count = (int) number;
				ng.setNumber(count+1);
				ofy().save().entity(ng);
				invoiceCount=count+1;
				
				billingDoc.setInvoiceCount(count+1);
			}else{
				billingDoc.setStatus(BillingDocument.CREATED);
			}
			
			/**** Done with Tax Table **************/

			// Saving Billing Document
			billingDoc.setTypeOfOrder(AppConstants.ORDERTYPESERVICE);
			billingDoc.setCompanyId(companyId);
			ofy().save().entity(billingDoc);
			if(data.trim().equalsIgnoreCase("createInvoice")){
			createInvoice(billingDoc,invoiceCount);
			}
		}
		
		
	}

	private void createInvoice(BillingDocument billingDoc, int invoiceCount) {
	
		final Invoice inventity = new Invoice();
		inventity.setPersonInfo(billingDoc.getPersonInfo());

		// ************************rohan made changes here for setting
		// fields()**************

		inventity.setGrossValue(billingDoc.getGrossValue());

		inventity.setTaxPercent(billingDoc.getArrPayTerms().get(0).getPayTermPercent());

		
		inventity.setContractCount(billingDoc.getContractCount());
		inventity.setContractStartDate(billingDoc.getContractStartDate());
		inventity.setContractEndDate(billingDoc.getContractEndDate());
		inventity.setTotalSalesAmount(billingDoc.getTotalSalesAmount());
		
		inventity.setArrayBillingDocument(billingDoc.getArrayBillingDocument());
		
//		inventity.setMultipleOrderBilling(true);
		
		inventity.setTotalBillingAmount(billingDoc.getTotalBillingAmount());
		
		//  rohan added inventity code for setting disc = 0 and net payable =setTotalBillingAmount
		
			inventity.setNetPayable(billingDoc.getTotalBillingAmount());
		
			inventity.setDiscount(0);
		//   ends here 
		
		if(billingDoc.getInvoiceDate()!=null){
			inventity.setInvoiceDate(billingDoc.getInvoiceDate());
		}
		if(billingDoc.getPaymentDate()!=null){
			inventity.setPaymentDate(billingDoc.getPaymentDate());
		}
		
		if (billingDoc.getApproverName()!=null)
			inventity.setApproverName(billingDoc.getApproverName());
		if (billingDoc.getEmployee()!=null)
			inventity.setEmployee(billingDoc.getEmployee());
		if (billingDoc.getBranch()!=null)
			inventity.setBranch(billingDoc.getBranch());
		
		inventity.setOrderCreationDate(billingDoc.getOrderCreationDate());
		inventity.setCompanyId(billingDoc.getCompanyId());
		inventity.setInvoiceAmount(billingDoc.getTotalBillingAmount());
		inventity.setInvoiceType(AppConstants.CREATETAXINVOICE);
		inventity.setPaymentMethod(billingDoc.getPaymentMethod());
		inventity.setAccountType(billingDoc.getAccountType());
		inventity.setTypeOfOrder(billingDoc.getTypeOfOrder());
		
		
		inventity.setStatus(Invoice.APPROVED);
		
		
		
		
		
		List<SalesOrderProductLineItem>productlist=billingDoc.getSalesOrderProducts();
		ArrayList<SalesOrderProductLineItem>prodList=new ArrayList<SalesOrderProductLineItem>();
		prodList.addAll(productlist);
		logger.log(Level.SEVERE,"");
		logger.log(Level.SEVERE,"SALES PRODUCT TABLE SIZE ::: "+prodList.size());
		
		/**
		 * Date :22-09-2017 BY ANIL
		 */
		
		List<OtherCharges>OthChargeTbl=billingDoc.getOtherCharges();
		ArrayList<OtherCharges>ocList=new ArrayList<OtherCharges>();
		ocList.addAll(OthChargeTbl);
		logger.log(Level.SEVERE,"OTHER TAXES TABLE SIZE ::: "+ocList.size());
		
		/**
		 * End
		 */
		
		List<ContractCharges>taxestable=billingDoc.getBillingOtherCharges();
		ArrayList<ContractCharges>taxesList=new ArrayList<ContractCharges>();
		taxesList.addAll(taxestable);
		logger.log(Level.SEVERE,"TAXES TABLE SIZE ::: "+taxesList.size());
		
		List<ContractCharges>otherChargesable=billingDoc.getBillingOtherCharges();
		ArrayList<ContractCharges>otherchargesList=new ArrayList<ContractCharges>();
		otherchargesList.addAll(otherChargesable);
		logger.log(Level.SEVERE,"OTHER CHARGES TABLE SIZE ::: "+otherchargesList.size());
		inventity.setSalesOrderProductFromBilling(prodList);
//		inventity.setSalesOrderProducts(prodList);
		
		/**
		 * Date : 22-09-2017 By ANIL
		 */
		inventity.setOtherCharges(ocList);
		inventity.setTotalOtherCharges(billingDoc.getTotalOtherCharges());
		/**
		 * ENd
		 */
		
		
		inventity.setBillingTaxes(taxesList);
		inventity.setBillingOtherCharges(otherchargesList);
		
		inventity.setCustomerBranch(billingDoc.getCustomerBranch());

		inventity.setAccountType(billingDoc.getAccountType());
		inventity.setArrPayTerms(billingDoc.getArrPayTerms());
		
		inventity.setInvoiceGroup(billingDoc.getBillingGroup());
		if (billingDoc.getOrderCformStatus() != null) {
			inventity.setOrderCformStatus(billingDoc.getOrderCformStatus());
		}
		if (billingDoc.getOrderCformPercent() != 0&& billingDoc.getOrderCformPercent() != -1) {
			inventity.setOrderCformPercent(billingDoc.getOrderCformPercent());
		} else {
			inventity.setOrderCformPercent(-1);
		}
		
		/***************** vijay number range *************************/
		if(billingDoc.getNumberRange()!=null)
			inventity.setNumberRange(billingDoc.getNumberRange());
		/****************************************************/
		
		/**
		 * date 14 Feb 2017
		 * added by vijay for Setting billing period from date and To date in invoice
		 */
		if(billingDoc.getBillingPeroidFromDate()!=null)
			inventity.setBillingPeroidFromDate(billingDoc.getBillingPeroidFromDate());
		if(billingDoc.getBillingPeroidToDate()!=null)
			inventity.setBillingPeroidToDate(billingDoc.getBillingPeroidToDate());
		/**
		 * End here
		 */
		
		/*
		 *  nidhi
		 *   1-07-2017
		 */
		if (billingDoc.getSegment() != null)
			inventity.setSegment(billingDoc.getSegment());
		/*
		 *  end
		 */
		
		
		/*
		 *  nidhi
		 *   1-07-2017
		 */
		if (billingDoc.getRateContractServiceId()!=0)
			inventity.setRateContractServiceId(billingDoc.getRateContractServiceId());
		/*
		 *  end
		 */
		
		/**
		 *  nidhi
		 *  20-07-2017
		 *   quantity and mesurement transfer to the invoice entity 
		 */
		if(billingDoc.getQuantity()!=0){
			inventity.setQuantity(billingDoc.getQuantity());
		}
		
		if(billingDoc.getUom()!=null){
			inventity.setUom(billingDoc.getUom());
		}
		/*
		 * end   
		 */
		/*
		 * 	nidhi
		 * 	24-07-2017
		 *  save customer ref no to invoice bean 	
		 *  
		 */
		if(billingDoc.getRefNumber()!=null){
			inventity.setRefNumber(billingDoc.getRefNumber());
		}
		/**
		 *  end
		 */
		
		/** Date 06-09-2017 added by vijay for total amt roundoff *******/
		
		inventity.setTotalAmtExcludingTax(billingDoc.getTotalAmount());
		inventity.setDiscountAmt(billingDoc.getDiscountAmt());
		inventity.setFinalTotalAmt(billingDoc.getFinalTotalAmt());
		inventity.setTotalAmtIncludingTax(billingDoc.getTotalAmtIncludingTax());
		inventity.setTotalBillingAmount(billingDoc.getGrandTotalAmount());
		if(billingDoc.getRoundOffAmt()!=0){
			inventity.setDiscount(billingDoc.getRoundOffAmt());
		}
		
		/**
		 * Date : 06-11-2017 BY ANIL
		 * setting billing comment to invoice
		 * for NBHC
		 */
		if(billingDoc.getComment()!=null){
			inventity.setComment(billingDoc.getComment());
		}
		inventity.setCompanyId(billingDoc.getCompanyId());
		inventity.setCount(invoiceCount);
		
		int paymentCount=0;
		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class)
				.filter("companyId", billingDoc.getCompanyId())
				.filter("processName", "CustomerPayment")
				.filter("status", true).first().now();
		long number = ng.getNumber();
		int count = (int) number;
		ng.setNumber(count+1);
		ofy().save().entity(ng);
		
		ofy().save().entity(inventity);
		paymentCount=count+1;
		createPayment(inventity,paymentCount);
}

	private void createPayment(Invoice inventity, int paymentCount) {
		GenricServiceImpl impl=new GenricServiceImpl();
		CustomerPayment paydetails=new CustomerPayment();
		paydetails.setPersonInfo(inventity.getPersonInfo());
		paydetails.setContractCount(inventity.getContractCount());
		if(inventity.getContractStartDate()!=null){
			paydetails.setContractStartDate(inventity.getContractStartDate());
		}
		if(inventity.getContractEndDate()!=null){
			paydetails.setContractEndDate(inventity.getContractEndDate());
		}
		paydetails.setTotalSalesAmount(inventity.getTotalSalesAmount());
		paydetails.setArrayBillingDocument(inventity.getArrayBillingDocument());
		
		/**
		 * old Code commented by aNil on 23-10-2017
		 */
//		paydetails.setTotalBillingAmount(inventity.getTotalBillingAmount());
		/**
		 * New code by ANIL
		 */
		paydetails.setTotalBillingAmount(inventity.getInvoiceAmount());
		/**
		 * End
		 */
		paydetails.setInvoiceAmount(inventity.getInvoiceAmount());
		
		
		paydetails.setInvoiceCount(inventity.getCount());
		paydetails.setInvoiceDate(inventity.getInvoiceDate());
		paydetails.setInvoiceType(inventity.getInvoiceType());
		paydetails.setPaymentDate(inventity.getPaymentDate());
		double invoiceamt=inventity.getInvoiceAmount();
		int payAmt=(int)(invoiceamt);
		paydetails.setPaymentAmt(payAmt);
		paydetails.setPaymentReceived(payAmt);
		paydetails.setPaymentMethod(inventity.getPaymentMethod());
		paydetails.setTypeOfOrder(inventity.getTypeOfOrder());
		paydetails.setStatus(CustomerPayment.CLOSED);
		paydetails.setOrderCreationDate(inventity.getOrderCreationDate());
		paydetails.setBranch(inventity.getBranch());
		paydetails.setAccountType(inventity.getAccountType());
		paydetails.setEmployee(inventity.getEmployee());
		paydetails.setCompanyId(inventity.getCompanyId());
		paydetails.setOrderCformStatus(inventity.getOrderCformStatus());
		paydetails.setOrderCformPercent(inventity.getOrderCformPercent());
		
		//  rohan added inventity code for sessing c
		
		if(inventity.getPaymentMethod()!= null && !inventity.getPaymentMethod().equals(""))
		{
			if(inventity.getPaymentMethod().equalsIgnoreCase("Cheque"))
			{
				paydetails.setChequeIssuedBy(inventity.getPersonInfo().getFullName());
			}
			else
			{
				paydetails.setChequeIssuedBy("");
			}
		}
		else
		{
			paydetails.setChequeIssuedBy("");
		}
		/***************** vijay*********************/
		if(inventity.getNumberRange()!=null)
		paydetails.setNumberRange(inventity.getNumberRange());
		/*******************************************/
		
		/**
		 * date 14 Feb 2017
		 * added by vijay for Setting billing period from date and To date in Payment
		 */
		if(inventity.getBillingPeroidFromDate()!=null)
			paydetails.setBillingPeroidFromDate(inventity.getBillingPeroidFromDate());
		if(inventity.getBillingPeroidToDate()!=null)
			paydetails.setBillingPeroidToDate(inventity.getBillingPeroidToDate());
		/**
		 * end here
		 */
		
		/*
		 *  nidhi
		 *   1-07-2017
		 */
		if(inventity.getSegment()!=null){
			paydetails.setSegment(inventity.getSegment());
		}
		/*
		 *  end
		 */
		
		/*
		 *  nidhi
		 *   1-07-2017
		 */
		if(inventity.getRateContractServiceId()!=0){
			paydetails.setRateContractServiceId(inventity.getRateContractServiceId());
		}
		/*
		 *  end
		 */
		
		/*
		 *  nidhi
		 *   1-07-2017
		 */
		if(inventity.getRateContractServiceId()!=0){
			paydetails.setRateContractServiceId(inventity.getRateContractServiceId());
		}
		/*
		 *  end
		 */
		
		/**
		 *  nidhi
		 *  20-07-2017
		 *   quantity and mesurement transfer to the invoice entity 
		 */
		if( inventity.getQuantity() > 0){
			paydetails.setQuantity(inventity.getQuantity());
		}
		
		if( inventity.getUom() !=null){
			paydetails.setUom(inventity.getUom());
		}
		/*
		 * end   
		 */
		
		/**
		 *  nidhi
		 *  24-07-2017
		 *  
		 */
		if( inventity.getRefNumber() !=null){
			paydetails.setRefNumber(inventity.getRefNumber());
		}
		/**
		 *  end
		 */
		paydetails.setInvoiceCount(inventity.getCount());
		paydetails.setCount(paymentCount);
		paydetails.setCompanyId(inventity.getCompanyId());
		ofy().save().entity(paydetails);
    }

	private void createService(HttpServletRequest req, Company comp, Contract newContract) {
		// TODO Auto-generated method stub
//		Contract contract = ofy().load().type(Contract.class)
//				.filter("companyId", comp.getCompanyId()).filter("count", contractId)
//				.first().now();

		logger.log(Level.SEVERE, "contract in create service::" + newContract);
		if (newContract != null) {
			// createService();
			createServicesByContract(newContract);
		} else {
			logger.log(Level.SEVERE, "Contract not found");
		}

	}

	private void createServicesByContract(Contract model) {
		ContractServiceImplementor impl = new ContractServiceImplementor();
		try {
			impl.makeServices(model);
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE,"ERROR in Creating Services"+e);
		}
	}
	private List<ServiceSchedule> getServiceScheduleList(Contract contract,
			Contract contract1, List<SalesLineItem> listofSaleLineItem,
			List<ServiceSchedule> list, Calendar c) {
		// TODO Auto-generated method stub

		List<ServiceSchedule> serviceScheduleList = new ArrayList<ServiceSchedule>();
		for (int i = 0; i < listofSaleLineItem.size(); i++) {
			String previousDate = "";
			int gapOfService = (int) (listofSaleLineItem.get(i).getDuration() / (listofSaleLineItem
					.get(i).getNumberOfServices() * listofSaleLineItem.get(i)
					.getQty()));
			for (int j = 0; j < contract.getServiceScheduleList().size(); j++) {
				ServiceSchedule serviceSchedule = new ServiceSchedule();
				serviceSchedule = contract.getServiceScheduleList().get(j);
				if (j + 1 == 1) {
					SimpleDateFormat sdfDateP = new SimpleDateFormat(
							"dd-MM-yyyy");
					sdfDateP.setTimeZone(TimeZone.getTimeZone("IST"));
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));

					try {
						serviceSchedule.setScheduleServiceDate(sdfDateP
								.parse(sdfDateP.format(contract
										.getContractDate())));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						logger.log(Level.SEVERE, "Error in j==1" + e);
						e.printStackTrace();
					}
					previousDate = sdfDateP.format(contract.getContractDate());
				} else {
					SimpleDateFormat sdfDateS = new SimpleDateFormat(
							"dd-MM-yyyy");
					sdfDateS.setTimeZone(TimeZone.getTimeZone("IST"));
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));

					Calendar c1 = Calendar.getInstance();
					try {
						c1.setTime(sdfDateS.parse(previousDate));
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						logger.log(Level.SEVERE, "Error inn j>1:::::::" + e1);

						e1.printStackTrace();
					}
					logger.log(Level.SEVERE, "gapOfService:::::::"
							+ gapOfService);
					try {
						c.add(Calendar.DATE, gapOfService);
						Date date1 = c.getTime();
						// serviceSchedule2.setScheduleServiceDay();

						serviceSchedule.setScheduleServiceDate(sdfDateS
								.parse(sdfDateS.format(date1)));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						logger.log(Level.SEVERE, "Error in Adding days:::::::"
								+ e);
						e.printStackTrace();
					}
				}
				serviceScheduleList.add(serviceSchedule);
			}
		}
		return serviceScheduleList;
	}

}
