package com.slicktechnologies.server.android.customerapp;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class FetchCustomerServiceDetails extends HttpServlet {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4312650270396877297L;

	/**
	 * 
	 */

	Logger logger = Logger.getLogger("FetchCustomerServiceDetails.class");

	RegisterServiceImpl registerService;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doGet(req, resp);
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doPost(req, resp);
		resp.setContentType("text/plain");
		
		String url = req.getParameter("url").trim();
		String[] urlArray = url.split("\\.");
		String accessUrl = urlArray[0];
		logger.log(Level.SEVERE,"accessUrl"+accessUrl);
		String customerId = req.getParameter("customerId").trim();
		
		registerService = new RegisterServiceImpl();
		registerService.getClass();
		
		Company company = ofy().load().type(Company.class)
				.filter("accessUrl", accessUrl).first().now();
		logger.log(Level.SEVERE,"company"+company);
		ArrayList<String> statusList=new ArrayList<String>();
		statusList.add(Service.SERVICESTATUSSCHEDULE);
		statusList.add(Service.SERVICESTATUSRESCHEDULE);
		statusList.add(Service.SERVICESTATUSSTARTED);
		statusList.add(Service.SERVICESTATUSSTARTED);
		statusList.add(Service.SERVICESTATUSREPORTED);
		
		
		List<Service> serviceList = new ArrayList<Service>();
		serviceList = ofy().load().type(Service.class)
				.filter("companyId", company.getCompanyId())
				.filter("personInfo.count", Integer.parseInt(customerId))
				.filter("status IN", statusList).list();

		JSONArray jArray = new JSONArray();
		for (Service service : serviceList) {
			JSONObject jObj = new JSONObject();
			jObj.put("serviceId", service.getCount());
			jObj.put("contractId", service.getContractCount());
			jObj.put("serviceName", service.getProductName().trim());
			jObj.put("status", service.getStatus().trim());
			jObj.put("serviceAddress", service.getAddress().getCompleteAddress());
			SimpleDateFormat spf = new SimpleDateFormat("dd/MMM/yyyy");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			spf.setTimeZone(TimeZone.getTimeZone("IST"));
			logger.log(Level.SEVERE, "service Date::::" + service.getCount()
					+ "service Date:::" + spf.format(service.getServiceDate()));
			jObj.put("serviceDate", spf.format(service.getServiceDate()));
			if(service.getEmployee()!=null){
				Employee employee=ofy().load().type(Employee.class).filter("companyId", service.getCompanyId()).filter("fullname",service.getEmployee().trim()).first().now();
				if(employee!=null){
				if(employee.getPhoto()!=null){
					jObj.put("employeeProfile", employee.getPhoto().getUrl());
					
				}else{
					jObj.put("employeeProfile", "No Profile Photo");
				}
				if(employee.getCellNumber1()!=null){
					jObj.put("employeeMobile",employee.getCellNumber1());
				}else{
					jObj.put("employeeMobile",0);
				}
				}
				}
			jArray.add(jObj);
		}
		String jsonString=jArray.toJSONString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE,jsonString);
		resp.getWriter().println(jsonString);
	}
}
