package com.slicktechnologies.server.android.customerapp;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;


public class FetchCustomerSummaryDetails extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5446100080335041209L;
	/**
	 * 
	 */
	Logger logger = Logger.getLogger("FetchCustomerSummaryDetails.class");
	SimpleDateFormat sdf;
	RegisterServiceImpl registerService;
	JSONObject globalJObj;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doGet(req, resp);
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("text/plain");

		sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		String url = req.getParameter("url").trim();
		logger.log(Level.SEVERE, "url: " + url);
		String[] urlArray = url.split("\\.");
		logger.log(Level.SEVERE, "urlArray[0]: " + urlArray[0]);
		String accessUrl = urlArray[0];
		logger.log(Level.SEVERE, "accessUrl: " + accessUrl);
		String customerId = req.getParameter("customerId").trim();
		logger.log(Level.SEVERE, "customerId: " + customerId);

		registerService = new RegisterServiceImpl();
		registerService.getClass();

		Company company = ofy().load().type(Company.class)
				.filter("accessUrl", accessUrl).first().now();

		ArrayList<String> statusList = new ArrayList<String>();
		statusList.add(Service.SERVICESTATUSSCHEDULE);
		statusList.add(Service.SERVICESTATUSRESCHEDULE);
		statusList.add(Service.SERVICESTATUSSTARTED);
		statusList.add(Service.SERVICESTATUSREPORTED);
		
		statusList.add(Service.CREATED);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.DAY_OF_YEAR, 30);
		Date date = calendar.getTime();
		
		Calendar serviceCalend = Calendar.getInstance();
		serviceCalend.setTime(new Date());
		serviceCalend.add(Calendar.DAY_OF_YEAR, 7);
		Date date1 = serviceCalend.getTime();
		
		List<Service> serviceList = new ArrayList<Service>();
		serviceList = ofy().load().type(Service.class)
				.filter("companyId", company.getCompanyId())
				.filter("personInfo.count", Integer.parseInt(customerId))
				.filter("status IN", statusList)
				.filter("serviceDate <=", date1).list();

		

		// String date = sdf.format(new Date());
		logger.log(Level.SEVERE, "String new date1::::::::" + date1 +"serviceList"+serviceList.size());

		List<ContractRenewal> contractRenewalList = ofy().load().type(ContractRenewal.class)
				.filter("companyId", company.getCompanyId())
				.filter("customerId", Integer.parseInt(customerId)).list();

		List<CustomerPayment> paymentList = new ArrayList<CustomerPayment>();
		paymentList = ofy().load().type(CustomerPayment.class)
				.filter("companyId", company.getCompanyId())
				.filter("personInfo.count", Integer.parseInt(customerId))
				.filter("status", CustomerPayment.CREATED)
				.list();
		
		List<ConfigCategory> categoryList=ofy().load().type(ConfigCategory.class).filter("companyId", company.getCompanyId()).filter("internalType", 28).list();

		globalJObj = new JSONObject();

		globalJObj.put("serviceList", getServiceList(serviceList));
		globalJObj.put("contractList", getContractList(contractRenewalList));
		globalJObj.put("paymentList", getPaymentList(paymentList));
		globalJObj.put("androidAdsList",getadsList(categoryList));

		String jsonString = globalJObj.toJSONString().replace("\\\\", "");
		logger.log(Level.SEVERE, jsonString);
		resp.getWriter().println(jsonString);
	}

	private Object getadsList(List<ConfigCategory> categoryList) {
		JSONArray jArray = new JSONArray();
		try {
			for (ConfigCategory category : categoryList) {
				JSONObject obj = new JSONObject();
				obj.put("title",category.getCategoryName());
				obj.put("data", category.getDescription());
				jArray.add(obj);
			}
			return jArray;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private Object getPaymentList(List<CustomerPayment> paymentList) {
		// TODO Auto-generated method stub
		JSONArray jArray = new JSONArray();
		try {
			for (CustomerPayment payment : paymentList) {

				Invoice invoiceDetails=ofy().load().type(Invoice.class).filter("companyId", payment.getCompanyId()).filter("count", payment.getInvoiceCount()).first().now();
				
				JSONObject obj = new JSONObject();
				obj.put("type","Payment");
				obj.put("contractId", payment.getContractCount());
				obj.put("invoiceId", payment.getInvoiceCount());
				obj.put("paymentId", payment.getCount());
				obj.put("paymentDate",
						getFormattedDate(payment.getPaymentDate()));
				obj.put("invoiceAmount", payment.getInvoiceAmount());
				obj.put("payableAmount", payment.getPaymentAmt());
				obj.put("uniqueId", payment.getId());
				obj.put("invoiceUniqueId", invoiceDetails.getId());
				jArray.add(obj);
			}
			return jArray;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private Object getContractList(List<ContractRenewal> contractRenewalList) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		try {
			JSONArray jArray = new JSONArray();
			for (ContractRenewal contractRenewal : contractRenewalList) {
				Contract contract=ofy().load().type(Contract.class).filter("companyId", contractRenewal.getCompanyId()).filter("count", contractRenewal.getContractId()).first().now();
				if (contract != null) {
					JSONObject obj = new JSONObject();
					obj.put("type", "Contract");
					obj.put("contractId", contract.getCount());
					obj.put("contractStartDate",
							getFormattedDate(contract.getStartDate()));
					obj.put("contractEndDate",
							getFormattedDate(contract.getEndDate()));
					obj.put("contractStartDate",
							getFormattedDate(contract.getStartDate()));
					// renewDate
//					JSONArray jproductArray = new JSONArray();
//					for (int j = 0; j < contract.getItems()
//							.size(); j++) {
//						JSONObject jprodObj = new JSONObject();
//						jprodObj.put("noOfServices", contract
//								.getServiceScheduleList().get(j)
//								.getScheduleServiceNo());
//						jprodObj.put("serviceName", contract
//								.getServiceScheduleList().get(j)
//								.getScheduleProdName());
//						jprodObj.put("serviceDate", getFormattedDate(contract.getServiceScheduleList().get(j)
//								.getScheduleServiceDate()));
//						jproductArray.add(jprodObj);
//					}
					try {
						obj.put("productList",
								new org.json.JSONArray(gson.toJson(contractRenewal.getItems())));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					obj.put("uniqueId", contract.getId());
					jArray.add(obj);
				}
			}
			return jArray;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private JSONArray getServiceList(List<Service> serviceList) {
		// TODO Auto-generated method stub

//		try {
			JSONArray jArray = new JSONArray();
			for (int i = 0; i < serviceList.size(); i++) {
				JSONObject obj = new JSONObject();
				obj.put("type", "Service");
				obj.put("contractId", serviceList.get(i).getContractCount());
				obj.put("startDate", getFormattedDate(serviceList.get(i)
						.getContractStartDate()));
				obj.put("endDate", getFormattedDate(serviceList.get(i)
						.getContractEndDate()));
				obj.put("serviceName", serviceList.get(i).getProductName());
				obj.put("serviceNo", serviceList.get(i).getServiceSerialNo());
				obj.put("serviceEngineer", serviceList.get(i).getEmployee());
				obj.put("serviceDate", getFormattedDate(serviceList.get(i)
						.getServiceDate()));
				obj.put("serviceAddress", serviceList.get(i).getAddress().getCompleteAddress());
				obj.put("serviceBranch", serviceList.get(i).getServiceBranch());
				obj.put("status", serviceList.get(i).getStatus().trim());
				obj.put("serviceId",serviceList.get(i).getCount());
				obj.put("uniqueId", serviceList.get(i).getId());
				if(serviceList.get(i).getEmployee()!=null){
				Employee employee=ofy().load().type(Employee.class).filter("companyId", serviceList.get(i).getCompanyId()).filter("fullname",serviceList.get(i).getEmployee().trim()).first().now();
				if(employee!=null){
				if(employee.getPhoto()!=null){
					obj.put("employeeProfile", employee.getPhoto().getUrl());
				}else{
					obj.put("employeeProfile", "No Profile Photo");
				}
				if(employee.getCellNumber1()!=null){
					obj.put("employeeMobile",employee.getCellNumber1());
				}else{
					obj.put("employeeMobile",0);
				}
				}
				
				}
				jArray.add(obj);
			}
			return jArray;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
	}

	public String getFormattedDate(Date date) {

		return sdf.format(date);

	}
}
