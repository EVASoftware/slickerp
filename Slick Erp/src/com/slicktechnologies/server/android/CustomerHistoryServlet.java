package com.slicktechnologies.server.android;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class CustomerHistoryServlet extends HttpServlet {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -663259554187682689L;
	/**
	 * 
	 */
	Logger logger = Logger.getLogger("CustomerHistoryServlet.class");
	Service service = new Service();
	List<Service> serviceList = new ArrayList<Service>();

	Company comp;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doGet(req, resp);
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doPost(req, resp);
		String urlCalled=req.getRequestURL().toString().trim();
//		String url=urlCalled.replace("http://", "");
//		String[] splitUrl=url.split("\\.");
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		Exception exep = null;
		try {

			
			comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
			
			String serviceId = req.getParameter("serviceId").trim();
			logger.log(Level.SEVERE, serviceId);
			String companyId = comp.getCompanyId()+"";
			logger.log(Level.SEVERE, companyId);
			String team = req.getParameter("team").trim();
			logger.log(Level.SEVERE, team);

			service = ofy().load().type(Service.class)
					.filter("companyId", comp.getCompanyId())
					.filter("count", Integer.parseInt(serviceId)).first().now();
			

			serviceList = ofy().load().type(Service.class)
					.filter("companyId", Long.parseLong(companyId))
					.filter("contractCount", service.getContractCount())
					.filter("status", Service.SERVICESTATUSCOMPLETED).list();
			JSONArray jarray = new JSONArray();

			for (int i = 0; i < serviceList.size(); i++) {
				JSONObject jobj = new JSONObject();

				SimpleDateFormat spf = new SimpleDateFormat("dd MMM yyyy");
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				spf.setTimeZone(TimeZone.getTimeZone("IST"));

				jobj.put("serviceDate",
						spf.format(serviceList.get(i).getServiceDate()).trim());
				logger.log(
						Level.SEVERE,
						"Service Date::::::::::"
								+ spf.format(
										serviceList.get(i).getServiceDate())
										.trim());

				jobj.put("serviceTime", serviceList.get(i).getServiceTime()
						.trim());
				logger.log(Level.SEVERE, "serviceTime::::::::::"
						+ serviceList.get(i).getServiceTime().trim());

				jobj.put("locality", serviceList.get(i).getAddress()
						.getLocality());

				jobj.put("serviceId",
						(serviceList.get(i).getCount() + "").trim());
				logger.log(Level.SEVERE, "Service ID::::::::::"
						+ (serviceList.get(i).getCount() + "").trim());

				jobj.put("status", serviceList.get(i).getStatus().trim());
				logger.log(Level.SEVERE, "status::::::::::"
						+ serviceList.get(i).getStatus().trim());

				jobj.put("serviceName", serviceList.get(i).getProductName()
						.trim());
				
				jobj.put("customerSignatureUrl", serviceList.get(i).getCustomerSignature());
				
				/*
				 * Name: Apeksha Gunjal
				 * Date: 07/06/2018 17:01
				 * Task: Added Premised details
				 */
				try{
					if(serviceList.get(i).getPremises() != null){
						jobj.put("premisesDetails", serviceList.get(i).getPremises());
					}else{
						jobj.put("premisesDetails", "");
					}
				}catch(Exception e){
					jobj.put("premisesDetails", "");
				}
				logger.log(Level.SEVERE, "serviceName::::::::::"
						+ serviceList.get(i).getProductName().trim());

				jobj.put("fullAddress", getFullAddress(i));
				
				//Added by Apeksha on 06/10/2017 for adding serviceSerialNumber to corresponding Api
				jobj.put("serviceSerialNumber",serviceList.get(i).getServiceSerialNo());
				
				jobj.put("premises",serviceList.get(i).getPremises());
				

				jarray.add(jobj);

			}

			String jsonString = jarray.toJSONString().replaceAll("\\\\", "");
			logger.log(Level.SEVERE, jsonString);
			resp.getWriter().println(jsonString);
		} catch (Exception e) {
			e.printStackTrace();
			exep = e;
			logger.log(Level.SEVERE, "ERROR:::::::::::::::::::::" + e);
			resp.getWriter().println("Failed");
		}

	}

	private Object getFullAddress(int i) {
		// TODO Auto-generated method stub
		String address = null;
		String add = null;
		String fullAddress = null;
		if (!serviceList.get(i).getAddress().getAddrLine2()
				.equalsIgnoreCase("")) {
			if (!serviceList.get(i).getAddress().getLandmark()
					.equalsIgnoreCase("")) {
				address = serviceList.get(i).getAddress().getAddrLine1() + ", "
						+ serviceList.get(i).getAddress().getAddrLine2() + ", "
						+ serviceList.get(i).getAddress().getLandmark();
			} else {
				address = serviceList.get(i).getAddress().getAddrLine1() + ", "
						+ serviceList.get(i).getAddress().getAddrLine2();
			}
		} else {
			if (!serviceList.get(i).getAddress().getLandmark()
					.equalsIgnoreCase("")) {
				address = serviceList.get(i).getAddress().getAddrLine1() + ", "
						+ serviceList.get(i).getAddress().getLandmark();
			} else {
				address = serviceList.get(i).getAddress().getAddrLine1();
			}
		}

		if (!serviceList.get(i).getAddress().getLocality().equalsIgnoreCase("")) {
			add = address + ", "
					+ serviceList.get(i).getAddress().getLocality();
		} else {
			add = address;
		}

		fullAddress = add + ", " + serviceList.get(i).getAddress().getCity()
				+ ", " + serviceList.get(i).getAddress().getState() + ", "
				+ serviceList.get(i).getAddress().getCountry() + ", Pin :"
				+ serviceList.get(i).getAddress().getPin();
		return fullAddress;
	}
}
