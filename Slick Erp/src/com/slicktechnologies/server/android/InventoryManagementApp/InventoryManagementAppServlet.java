package com.slicktechnologies.server.android.InventoryManagementApp;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.simple.JSONObject;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.inventory.productinventorytransaction.WarehouseInfo;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.SerialNumberDeviation;
import com.slicktechnologies.shared.common.inventory.SerialNumberMapping;
import com.slicktechnologies.shared.common.inventory.SerialNumberStockMaster;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class InventoryManagementAppServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3970527273286885654L;
	Logger logger = Logger.getLogger("Logger");
	
	List<WareHouse> globalWarehouselist = null;
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		doPost(req, resp);
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		String strCompanyId = "5675267779461120"; // server
//		String strCompanyId = "5348024557502464";
		long companyId = Long.parseLong(strCompanyId);
		String userId = req.getParameter("username").trim();
		String password = req.getParameter("password").trim();
		String actionTask=req.getParameter("actionTask").trim();
		String productCode = req.getParameter("productCode").trim();
		String fromWarehouse = req.getParameter("FromWarehouse").trim();
		String transferToWarehouse = req.getParameter("ToWarehouse").trim();
		String branchName = req.getParameter("Branch").trim();
		String quantity = req.getParameter("Quantity").trim();
		String startSerialNo = req.getParameter("StartSerialNo").trim();
		String endSerialNo = req.getParameter("EndSerialNo").trim();
		String employeeName = req.getParameter("EmployeeName").trim();
		Date documnetDate=null;
		if(!actionTask.trim().equals("Login") && !actionTask.trim().equals("MIN")){
			String strdocumentDate = req.getParameter("Date").trim();
			try {
				documnetDate = dateFormat.parse(strdocumentDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		/** Date 22-01-2020 by Vijay below logic not required because
		 * I have updated the same using Encoding and Decoding for warehouse 
		 */
		
//		/**
//		 * @author Abhinav Bihade
//		 * @since 27/12/2019
//		 * Lock Seal API Warehouse Name contains '&' then Json data unable to read.
//		 */
//		if(fromWarehouse!=null && req.getParameter("WHName_AND")!=null){
//		String sepeartor = req.getParameter("WHName_AND").trim();
//		logger.log(Level.SEVERE, "sepeartor " + sepeartor);
//		if (sepeartor.equals("1")) {
//			String actualWarehouseName = fromWarehouse.replace("AND", "&");
//			fromWarehouse = actualWarehouseName;
//		}
//		logger.log(Level.SEVERE, "fromWarehouse after seperator=" + fromWarehouse);
//		}
		
		if(employeeName==null){
			employeeName = getEmployeeName(userId,companyId);
		}
		if(actionTask.trim().equals("Login")){
			ServerAppUtility serverUtility = new ServerAppUtility();
			User user = ofy().load().type(User.class).filter("companyId", companyId).filter("userName", userId).first().now();
			if(user ==null){
				resp.getWriter().println("Invalid Credentials");
			}
			boolean loginStatus = serverUtility.checkUserLogin(user, userId, password, companyId);
			if(loginStatus){
				String jsonString  = reactonLogin(companyId,user);
				resp.getWriter().println(jsonString);
			}
			else{
				resp.getWriter().println("Failed");
			}
		}
		else if(actionTask.trim().equals("GRN")){
			double Quantity = Double.parseDouble(quantity);
			String strResponseMessage = reactonGRN(companyId,employeeName,productCode,fromWarehouse,branchName,Quantity,startSerialNo,endSerialNo,documnetDate);
			resp.getWriter().println(strResponseMessage);
		}
		else if(actionTask.trim().equals("MMN")){
			double Quantity = Double.parseDouble(quantity);
			String strResponseMessage = reactonMMN(companyId,employeeName,productCode,fromWarehouse,transferToWarehouse,branchName,Quantity,startSerialNo,endSerialNo,documnetDate);
			resp.getWriter().println(strResponseMessage);
		}
		else if(actionTask.trim().equals("MIN")){
			double Quantity = Double.parseDouble(quantity);
			String strResponseMessage = reactonMIN(companyId,employeeName,productCode,fromWarehouse,branchName.toUpperCase(),Quantity,startSerialNo,endSerialNo);
			resp.getWriter().println(strResponseMessage);
		}
		else{
			logger.log(Level.SEVERE,"Wrong action task"+actionTask);
			resp.getWriter().println("Failed");

		}
	}

	

	private String getEmployeeName(String userId, long companyId) {
		User user = ofy().load().type(User.class).filter("companyId", companyId).filter("userName", userId).first().now();
		return user.getEmployeeName();
	}

	private String reactonGRN(long companyId, String username, String productCode, String fromWarehouse, String branchName, double quantity, String startSerialNo, String endSerialNo, Date documnetDate) {
		long startSerialNumber = getSerialNumber(startSerialNo);
		long endSerialNumber = getSerialNumber(endSerialNo);
		List<SerialNumberStockMaster> serialNumberStocklist = ofy().load().type(SerialNumberStockMaster.class)
															.filter("productCode", productCode)
															.filter("status", true)
															.filter("startSerialNumber", startSerialNumber)
															.filter("endSerialNumber", endSerialNumber)
															.filter("companyId", companyId).list();
		
		logger.log(Level.SEVERE,"Same Serial Number Active GRN list size"+serialNumberStocklist.size());
		
		if(endSerialNumber<startSerialNumber){
			return "End Serial No should not be less than Start Serialn Number! Serial number not readed properly Please try again";
		}
		/**
		 * Date 14-09-2019 by Vijay
		 * Des :- if open PR Balance Qty exceeds the GRN Receiving Qty then its should not allow to GRN 
		 */
//		if(quantity > validatePRQty(productCode,branchName,companyId,quantity,fromWarehouse)){
//			return "Quantity exceeds PR Balance Qty ";
//		}
		if(serialNumberStocklist.size()==0 && ValidateGRN(companyId, productCode, fromWarehouse, startSerialNo, endSerialNo)){
			logger.log(Level.SEVERE, "Inside GRN");
			GRN grnEntity=new GRN();
			grnEntity.setTitle("");
			grnEntity.setCreationDate(documnetDate);
			grnEntity.setEmployee(username);
			grnEntity.setApproverName(username);
			grnEntity.setStatus(GRN.APPROVED);
			grnEntity.setInspectionRequired(AppConstants.NO);
			grnEntity.setBranch(branchName);
			
			SuperProduct product = getproductDetails(productCode, companyId);
			String storageLocation = getStorageLocation(fromWarehouse, companyId);
			if(storageLocation.equals("Failed")){
				return "Storage Location not found for this warehouse in EVA ERP system kindly check!";
			}
			ArrayList<GRNDetails> grnlist = new ArrayList<GRNDetails>();	
			GRNDetails grn = new GRNDetails();
			grn.setProductID(product.getCount());
			grn.setProductCode(product.getProductCode());
			grn.setProductCategory(product.getProductCategory());
			grn.setProductName(product.getProductName());
			grn.setUnitOfmeasurement(product.getUnitOfMeasurement());
			grn.setProductQuantity(quantity);
			grn.setProdPrice(product.getPrice());
			grn.setTotal(product.getPrice()*quantity);
			grn.setDiscount(0);
			grn.setWarehouseLocation(fromWarehouse);
			grn.setWarehouseName(fromWarehouse);
			grn.setStorageLoc(storageLocation);
			String storageBin = getStorageBin(storageLocation, companyId);
			if(storageBin.equals("Failed")){
				return "Storage Bin not found for this warehouse in EVA ERP system kindly check!";
			}
			grn.setStorageBin(storageBin);
			grn.setPrduct(product);
			grn.setPurchaseTax1(product.getPurchaseTax1());
			grn.setPurchaseTax2(product.getPurchaseTax2());
			/**
			 * Date 29-11-2019 by Vijay
			 * Des :- This old logic not required
			 */
//			HashMap<Integer , ArrayList<ProductSerialNoMapping>> prserdt = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
//			ArrayList<ProductSerialNoMapping> serialNoMappinglist = new ArrayList<ProductSerialNoMapping>();
//			serialNoMappinglist = getProductSerialNoMapping(startSerialNo,endSerialNo);
//			prserdt.put(0, serialNoMappinglist);
//			grn.setProSerialNoDetails(prserdt);
			/** Date 05-12-2019 new logic added ***/
			String locksealNumber = getProductSerialNumber(startSerialNo,endSerialNo);
			grn.setSerialNumber(locksealNumber);
			
			grnlist.add(grn);
			grnEntity.setInventoryProductItem(grnlist);
			grnEntity.setCompanyId(companyId);
			GenricServiceImpl impl=new GenricServiceImpl();
			ReturnFromServer server = new ReturnFromServer();
			server = impl.save(grnEntity);
			logger.log(Level.SEVERE,"GRN Saved Successfully");
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("GRN", "EnableApprovalProcessThroughTaskQueue", companyId)){
				Approvals model = sendApprovalRequestGRNMMN(server.count, username, companyId, grnEntity.getBranch(),
						grnEntity.getEmployee(), grnEntity.getCreatedBy(),"GRN");
				if(model!=null){
					String taskName="ApprovalTaskQueueProcess"+"$"+model.getCompanyId()+"$"+model.getId()+"$"+model.getStatus();
					Queue queue = QueueFactory.getQueue("documentCancellation-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));
				}
			}
			else{
				grnEntity.reactOnApproval();	
			}
			logger.log(Level.SEVERE, "After approval task queue");
			return "Success";
		}
		else{
			logger.log(Level.SEVERE, "Inside else block of GRN");
			return "This stock already assigned to other cluster";
		}
		
		
	}

	

	

	private long getSerialNumber(String strserialNo) {
		long number = 0;
		number = Long.parseLong(strserialNo.trim());
		return number;
	}

	private ArrayList<ProductSerialNoMapping> getProductSerialNoMapping(String startSerialNo, String endSerialNo) {
		ArrayList<ProductSerialNoMapping> serialNoMappinglist = new ArrayList<ProductSerialNoMapping>();
			ProductSerialNoMapping serialNoMapping = new ProductSerialNoMapping();
			serialNoMapping.setProSerialNo(startSerialNo+"-"+endSerialNo);		
			serialNoMapping.setStatus(true);
			serialNoMappinglist.add(serialNoMapping);
		return serialNoMappinglist;
		
	}

	private String reactonMMN(long companyId, String username, String productCode, String frmWarehouse, String trnsferToWarehouse, String branchName, double quantity, String startSerialNo, String endSerialNo, Date documnetDate) {
		
		String fromWarehouse = getDecodeOfWarehouseName(frmWarehouse);
		String transferToWarehouse = getDecodeOfWarehouseName(trnsferToWarehouse);
		
		/**
		 * Date 14-09-2019 by Vijay
		 * Des :- if open PR Balance Qty exceeds the GRN Receiving Qty then its should not allow to GRN 
		 */
//		ArrayList<String> strwarehouseslist = new ArrayList<String>();
//		strwarehouseslist.add(fromWarehouse);
//		strwarehouseslist.add(transferToWarehouse);
//		
//		List<WareHouse> warehouselist = ofy().load().type(WareHouse.class).filter("companyId", companyId)
//							.filter("buisnessUnitName IN", strwarehouseslist).list();
//		boolean flag = true;
//		if(warehouselist.size()==2){
//			if(warehouselist.get(0).getParentWarehouse().equals("NBHC HO") && 
//					warehouselist.get(1).getParentWarehouse().equals("NBHC HO")){
//				flag = false;
//			}
//		}
//		if(flag){
//			if(quantity > validateMRNQty(productCode,branchName,companyId,quantity,fromWarehouse,transferToWarehouse)){
//				return "Quantity exceeds MRN Balance Qty ";
//			}
//		}
		
//		List<ProductInventoryView> productInventoryView=ofy().load().type(ProductInventoryView.class).filter("companyId", companyId).filter("productinfo.productCode", productCode).list();
//		
//		String storagelocationValue=getStorageLocation(fromWarehouse, companyId);
//		String storagebinValue=getStorageBin(storagelocationValue, companyId);
//		boolean firstFlag=true;
//		
//		for(ProductInventoryView view:productInventoryView){
//			for(ProductInventoryViewDetails details:view.getDetails()){
//				if(details.getWarehousename().equals(fromWarehouse)
//					&&details.getStoragelocation().equals(storagelocationValue)
//					  &&details.getStoragebin().equals(storagebinValue)){
//					if(details.getAvailableqty()<quantity){
//					  firstFlag=false;
//					}
//				}
//			}
//		}
//		if(firstFlag==false){
//			return "There is no Available stock for Warehouse :- "+fromWarehouse;
//		}
		
		
		String transferfromstoragelocation = getStorageLocation(fromWarehouse, companyId);
		if(transferfromstoragelocation.equals("Failed")){
			return "Storage Location not found for Transfer From warehouse in EVA ERP system kindly check!";
		}
		String fromwarehousestorageBin = getStorageBin(transferfromstoragelocation, companyId);
		if(fromwarehousestorageBin.equals("Failed")){
			return "Storage Bin not found for Transfer From warehouse in EVA ERP system kindly check!";
		}
		
		ProductInventoryViewDetails prodInventoryviewdetails=ofy().load().type(ProductInventoryViewDetails.class).filter("companyId", companyId).filter("prodcode", productCode)
				.filter("warehousename", fromWarehouse).filter("storagelocation",transferfromstoragelocation )
				.filter("storagebin", fromwarehousestorageBin).first().now();
		logger.log(Level.SEVERE, "prodInventoryviewdetails"+prodInventoryviewdetails);
		if(prodInventoryviewdetails==null){
			return "stock master not found for "+fromWarehouse;
		}
		else{
			if(prodInventoryviewdetails.getAvailableqty()<quantity){
				return "There is no Available stock for Warehouse :- "+fromWarehouse;
			}
		}
		
		boolean validateFlag = Validate(companyId,productCode,fromWarehouse,startSerialNo,endSerialNo,"MMN");
		logger.log(Level.SEVERE, "validateFlag"+validateFlag);
		if(validateFlag){
			MaterialMovementNote mmn = new MaterialMovementNote();
			mmn.setCompanyId(companyId);
			mmn.setStatus(MaterialMovementNote.APPROVED);
			mmn.setCreationDate(new Date());
			mmn.setMmnDate(documnetDate);
			mmn.setMmnTransactionType("TransferOUT");
			mmn.setTransDirection("TRANSFEROUT");
			ArrayList<MaterialProduct> materiallist = new ArrayList<MaterialProduct>();
			mmn.setTransToWareHouse(transferToWarehouse);
			String storagelocation = getStorageLocation(transferToWarehouse, companyId);
			if(storagelocation.equals("Failed")){
				return "Storage Location not found for Transfer To warehouse in EVA ERP system kindly check!";
			}
			mmn.setTransToStorLoc(storagelocation);
			String storageBin = getStorageBin(storagelocation, companyId);
			if(storageBin.equals("Failed")){
				return "Storage Bin not found for Transfer To warehouse in EVA ERP system kindly check!";
			}
			mmn.setTransToStorBin(storageBin);
			
			SuperProduct product = getproductDetails(productCode, companyId);
			MaterialProduct materialProd = new MaterialProduct();
			materialProd.setMaterialProductId(product.getCount());
			materialProd.setMaterialProductCode(product.getProductCode());
			materialProd.setMaterialProductName(product.getProductName());
			materialProd.setMaterialProductRequiredQuantity(quantity);
			materialProd.setMaterialProductRemarks("");
			materialProd.setMaterialProductUOM(product.getUnitOfMeasurement());
			materialProd.setMaterialProductWarehouse(fromWarehouse);
			String storagelocationName = getStorageLocation(fromWarehouse, companyId);
			if(storagelocationName.equals("Failed")){
				return "Storage Location not found for Transfer From warehouse in EVA ERP system kindly check!";
			}
			materialProd.setMaterialProductStorageLocation(storagelocationName);
			String storageBin2 = getStorageBin(storagelocationName, companyId);
			if(storageBin2.equals("Failed")){
				return "Storage Bin not found for Transfer From warehouse in EVA ERP system kindly check!";
			}
			materialProd.setMaterialProductStorageBin(storageBin2);
			
			/***
			 * 
			 */
			logger.log(Level.SEVERE,"productCode "+productCode);
			logger.log(Level.SEVERE,"warehousename "+fromWarehouse);
			/**
			 * Date 29-11-2019 by Vijay
			 * Des :- This old logic not required
			 */
//			HashMap<Integer , ArrayList<ProductSerialNoMapping>> prserdt = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
//			ArrayList<ProductSerialNoMapping> serialNoMappinglist = new ArrayList<ProductSerialNoMapping>();
//			serialNoMappinglist = getProductSerialNoMapping(startSerialNo,endSerialNo);
//			prserdt.put(0, serialNoMappinglist);
//			materialProd.setProSerialNoDetails(prserdt);
			/** Date 05-12-2019 new logic added ***/
			String locksealNumber = getProductSerialNumber(startSerialNo,endSerialNo);
			materialProd.setLockSealSerialNo(locksealNumber);
			
			materiallist.add(materialProd);
			mmn.setSubProductTableMmn(materiallist);
			mmn.setEmployee(username);
			mmn.setBranch(branchName);	
			
			GenricServiceImpl genimpl = new GenricServiceImpl();
			ReturnFromServer server = new ReturnFromServer();
			server = genimpl.save(mmn);
//			mmn.reactOnApproval();
			logger.log(Level.SEVERE, "MMN Saved successfully");

			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialMovementNote", "EnableApprovalProcessThroughTaskQueue", companyId)){
				Approvals model = sendApprovalRequestGRNMMN(server.count, username, companyId, mmn.getBranch(),
						mmn.getEmployee(), mmn.getCreatedBy(),"MMN");
				if(model!=null){
					String taskName="ApprovalTaskQueueProcess"+"$"+model.getCompanyId()+"$"+model.getId()+"$"+model.getStatus();
					Queue queue = QueueFactory.getQueue("documentCancellation-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));
				}
			}
			else{
				mmn.reactOnApproval();	
			}
			logger.log(Level.SEVERE, "After MMN approval task queue");
			return "Success";
		}
		else{
			logger.log(Level.SEVERE, "Invalid Lock Seal");
			return "Invalid Lock Seal";
		}
		
	
		
	}

	

	

	private String reactonMIN(long companyId, String username, String productCode, String fromWarehouse, String branchName, double quantity, String startSerialNo, String endSerialNo) {
			
		/**
		 * @author Vijay Chougule
		 * Date 21-01-2020
		 * Des :- warehouse Name contains & so decoding the warehouse name  
		 */ 
		fromWarehouse = getDecodeOfWarehouseName(fromWarehouse);
		logger.log(Level.SEVERE, "fromwarehouse after decoding"+fromWarehouse);
		
		/**
		 * @author Vijay Chougule
		 * Des :- if warehouse name does not exist in the eva and Warehouse is in Inactive status then
		 * i m sending its response to EOD App to NBHC. and not allow to consume the same.
		 */
			WareHouse warehouseEntity = ofy().load().type(WareHouse.class).filter("buisnessUnitName",fromWarehouse)
										.filter("companyId", companyId).first().now();
			logger.log(Level.SEVERE,"warehouse Name "+fromWarehouse);
			/**
			 * @author Vijay Chougule
			 * Bug Des :- NBHC and EVA system warehouse name mismatching due to & and AND defined in the warehouse name
			 * so if warehouse name not found with the name then i will check the same with warehouse code
			 */
			if(warehouseEntity == null){
				if(fromWarehouse.contains("-")){
					String warehouseCodearr[] = fromWarehouse.split("-");
					String warehouseCode = warehouseCodearr[1];
					logger.log(Level.SEVERE,"Inside warehouse name not found to check with warehouse code "+warehouseCode);
					
					warehouseEntity = ofy().load().type(WareHouse.class).filter("warehouseCode",warehouseCode)
							.filter("companyId", companyId).first().now();
					if(warehouseEntity!=null){
						fromWarehouse = warehouseEntity.getBusinessUnitName();
						logger.log(Level.SEVERE,"warehouse Name"+fromWarehouse);

					}

				}
			}
			/**
			 * ends here
			 */
			
			if(warehouseEntity!=null){
				if(!warehouseEntity.getstatus()){
					logger.log(Level.SEVERE, "warehouse status is InActive");
					return "7";
				}
			}
			else{
				logger.log(Level.SEVERE, "Warehouse does not exist in EVA");
				return "6";
			}
		/**
		 * ends here	
		 */
			List<MaterialIssueNote> minlist = ofy().load().type(MaterialIssueNote.class).filter("companyId", companyId)
					.filter("referenceNo", startSerialNo).list();
			logger.log(Level.SEVERE,"minlist size"+minlist.size());
			logger.log(Level.SEVERE,"fromWarehouse"+fromWarehouse);

			if(minlist.size()==0){
				
				boolean validateFlag = Validate(companyId,productCode,fromWarehouse,startSerialNo,endSerialNo,"MIN");
				
				if(validateFlag){
					MaterialIssueNote minEntity = new MaterialIssueNote();
					minEntity.setCompanyId(companyId);
					minEntity.setMinDate(DateUtility.getDateWithTimeZone("IST",(new Date())));
					minEntity.setBranch(branchName);
					minEntity.setMinSalesPerson(username);		
					minEntity.setEmployee(username);
					minEntity.setMinIssuedBy(username);
					minEntity.setApproverName(username);
					minEntity.setStatus(MaterialIssueNote.APPROVED);
					minEntity.setMinIssuedBy(username);
					minEntity.setApprovalDate( DateUtility.getDateWithTimeZone("IST", (new Date())));
					SuperProduct itemproduct = getproductDetails(productCode,companyId);
					ArrayList<MaterialProduct> productlist = new ArrayList<MaterialProduct>();
					MaterialProduct materialProduct = new MaterialProduct();
					materialProduct.setMaterialProductId(itemproduct.getCount());
					materialProduct.setMaterialProductCode(itemproduct.getProductCode());
					materialProduct.setMaterialProductName(itemproduct.getProductName());
					materialProduct.setMaterialProductUOM(itemproduct.getUnitOfMeasurement());
					materialProduct.setMaterialProductRequiredQuantity(quantity);
					materialProduct.setMaterialProductWarehouse(fromWarehouse);
					materialProduct.setLockSealSerialNo(startSerialNo);
					String storageLocation = getStorageLocation(fromWarehouse,companyId); 
					if(storageLocation.equals("Failed")){
//						return "Storage Location not found for this warehouse in EVA ERP system kindly check!";
						return "4";
					}
					materialProduct.setMaterialProductStorageLocation(storageLocation);
					String storageBin = getStorageBin(storageLocation, companyId);
					if(storageBin.equals("Failed")){
//						return "Storage Bin not found for this warehouse in EVA ERP system kindly check!";
						return "5";
					}
					materialProduct.setMaterialProductStorageBin(storageBin);
					/**
					 * Date 29-11-2019 by Vijay
					 * Des :- This old logic not required
					 */
//					HashMap<Integer , ArrayList<ProductSerialNoMapping>> prserdt = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
//					ArrayList<ProductSerialNoMapping> serialNoMappinglist = new ArrayList<ProductSerialNoMapping>();
//					serialNoMappinglist = getProductSerialNoMapping(startSerialNo,endSerialNo);
//					prserdt.put(0, serialNoMappinglist);
//					materialProduct.setProSerialNoDetails(prserdt);
					
					String locksealNumber = getProductSerialNumber(startSerialNo,endSerialNo);
					materialProduct.setLockSealSerialNo(locksealNumber);
					
					productlist.add(materialProduct);
					minEntity.setSubProductTablemin(productlist);
					minEntity.setReferenceNo(startSerialNo);
					GenricServiceImpl impl = new GenricServiceImpl();
					ReturnFromServer server = new ReturnFromServer();
					server = impl.save(minEntity);
					logger.log(Level.SEVERE,"Min Saved sucessfully");
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialIssueNote", "EnableApprovalProcessThroughTaskQueue", companyId)){
						Approvals model = sendApprovalRequest(server.count,username,companyId,minEntity);
						if(model!=null){
							String taskName="ApprovalTaskQueueProcess"+"$"+model.getCompanyId()+"$"+model.getId()+"$"+model.getStatus();
							Queue queue = QueueFactory.getQueue("documentCancellation-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));
						}
					}
					else{
						minEntity.reactOnApproval();
					}
					
					
//						return "Success";
						return "1";
				}
				else{
					logger.log(Level.SEVERE,"Invalid Lock Seal");
					reactonMINDeviation(companyId,productCode,fromWarehouse,branchName,startSerialNo,endSerialNo);
//					return "Invalid Lock Seal";
					return "3";
				}
			
			}
			else{
				logger.log(Level.SEVERE,"Lock Seal Already consumed In EVA");
//				return "Already Consumed";
				return "2";
			}
	}

	private void reactonMINDeviation(long companyId, String productCode, String fromWarehouse, String branchName, String startSerialNo, String endSerialNo) {
		
		SerialNumberDeviation serialnumberDeviation = new SerialNumberDeviation();
		serialnumberDeviation.setCompanyId(companyId);
		serialnumberDeviation.setProductCode(productCode);
		serialnumberDeviation.setDocumentType("MIN");
		serialnumberDeviation.setStartSerialNumber(Long.parseLong(startSerialNo.trim()));
		serialnumberDeviation.setWarehouseName(fromWarehouse);
		serialnumberDeviation.setStatus(true);
		serialnumberDeviation.setEndSerialNumber(Long.parseLong(endSerialNo.trim()));
		serialnumberDeviation.setTransactionDate(new Date());
		
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(serialnumberDeviation);
		
	}

	private String getStorageBin(String storageLocation, long companyId) {
		Storagebin storageBin = ofy().load().type(Storagebin.class).filter("companyId", companyId).filter("storagelocation", storageLocation).first().now();
		if(storageBin==null){
			return "Failed";
		}
		logger.log(Level.SEVERE,"Storage Bin Name =="+storageBin.getBinName());
		return storageBin.getBinName();
	}

	private String getStorageLocation(String fromWarehouse, long companyId) {
		StorageLocation storagelocation = ofy().load().type(StorageLocation.class).filter("companyId", companyId).filter("warehouseName", fromWarehouse).first().now();
		if(storagelocation==null){
			return "Failed";
		}
		logger.log(Level.SEVERE,"storagelocation Name =="+storagelocation.getBusinessUnitName());
		return storagelocation.getBusinessUnitName();
	}

	private SuperProduct getproductDetails(String productCode, long companyId) {
		ItemProduct product = ofy().load().type(ItemProduct.class).filter("companyId", companyId).filter("productCode", productCode).first().now();
		logger.log(Level.SEVERE,"Product Code"+productCode);
		logger.log(Level.SEVERE,"companyId"+companyId);
		logger.log(Level.SEVERE,"Product Name =="+product.getProductName());
		
		return product;
	}

	/**
	 * Date 29-04-2019 by Vijay
	 * Des :- NBHC Inventory management App 
	 * @param user 
	 * @param company 
	 * @return
	 */
	private String reactonLogin(long companyId, User user) {
		
		Employee employee = ofy().load().type(Employee.class).filter("companyId", companyId)
							.filter("fullname", user.getEmployeeName()).first().now();
		logger.log(Level.SEVERE , "Employee Name "+employee.getFullname());
		 ArrayList<String> strBranch = new ArrayList<String>();
		 for(EmployeeBranch empBranch : employee.getEmpBranchList()){
			 strBranch.add(empBranch.getBranchName());
		 }
		logger.log(Level.SEVERE , "Employee Branches size"+strBranch.size());
		if(strBranch.size()!=0){
			List<WareHouse> warehouselist = ofy().load().type(WareHouse.class).filter("companyId", companyId)
					.filter("branchdetails.branchName IN", strBranch).list();
			logger.log(Level.SEVERE , "Employee Warehouse size"+warehouselist.size());
			
			globalWarehouselist = ofy().load().type(WareHouse.class).filter("companyId", companyId).list();
			ArrayList<String> fromWarehouses = new ArrayList<String>();
			ArrayList<String> toWarehouses = new ArrayList<String>();
			fromWarehouses = getfromWarehouse(warehouselist);
			logger.log(Level.SEVERE, "From warehouse details"+fromWarehouses.toString());
			toWarehouses = getTowarehouse(globalWarehouselist,strBranch);
			logger.log(Level.SEVERE, "TO warehouse details"+toWarehouses.toString());
			return  createJSONWarehouses(fromWarehouses,toWarehouses,employee.getBranchName(),employee.getFullname(),warehouselist,companyId);
		}
		else{
			return "Branches is not assigned to Employee";
		}
	}


	private String createJSONWarehouses(ArrayList<String> fromWarehouses, ArrayList<String> toWarehouses, String employeeBranch, String employeeName, List<WareHouse> warehouselist, long companyId) {
		JSONObject jsonobj = new JSONObject();
		JSONArray arrayjsonFromwarehouse = new JSONArray();
		for(String warehouse : fromWarehouses){
			arrayjsonFromwarehouse.put(warehouse);
		}
		jsonobj.put("FromWarehouse", arrayjsonFromwarehouse);
		
		JSONArray arrayjsonTowarehouse = new JSONArray();
		for(String warehouse : toWarehouses){
			arrayjsonTowarehouse.put(warehouse);
		}
		jsonobj.put("ToWarehouse", arrayjsonTowarehouse);
		jsonobj.put("productCode", "P100000065");
		jsonobj.put("productName", "Plastic seal with Barcode");
		jsonobj.put("Branch", employeeBranch);
		jsonobj.put("EmployeeName", employeeName);
		/** Date 15-06-20019 by Vijay for Cluster warehouses only showing to GRN screen in app ***/
		ArrayList<String> clusterWarehouse = getClusterWarehouseOnly(warehouselist);
		JSONArray arrayjsonClusterwarehouse = new JSONArray();
		for(String clsterWarehouse : clusterWarehouse){
			arrayjsonClusterwarehouse.put(clsterWarehouse);
		}
		jsonobj.put("clusterWarehouses", arrayjsonClusterwarehouse);
		
		List<WareHouse> allClusterWarehouses = ofy().load().type(WareHouse.class).filter("companyId", companyId)
				.filter("parentWarehouse", "NBHC HO").list();
		logger.log(Level.SEVERE, "All Cluster Warehouses"+allClusterWarehouses.size());
		
		List<WareHouse> updatedAllClusterWarehouses = new ArrayList<WareHouse>();
		updatedAllClusterWarehouses.addAll(allClusterWarehouses);
		
		JSONArray arrayWarehouses = new JSONArray();
		for(String clusterwarehouse : clusterWarehouse){
			JSONObject jobj = new JSONObject();
			JSONArray arrayClsutersWarehouses = new JSONArray();
			arrayClsutersWarehouses = getWarehousesOfCluster(clusterwarehouse,warehouselist);
			jobj.put(clusterwarehouse,arrayClsutersWarehouses);
			arrayWarehouses.put(jobj);
			
			for(WareHouse warehouse : updatedAllClusterWarehouses){
				if(warehouse.getBusinessUnitName().trim().equals(clusterwarehouse)){
					allClusterWarehouses.remove(warehouse);
				}
			}
		}
		logger.log(Level.SEVERE, "Updated Warehouse Cluster Size"+allClusterWarehouses.size());
		
		for(WareHouse warehouses : allClusterWarehouses){
			JSONObject jobj = new JSONObject();
			JSONArray arrayClsutersWarehouses = new JSONArray();
			jobj.put(warehouses,arrayClsutersWarehouses);
			arrayWarehouses.put(jobj);
		}
		
		jsonobj.put("Warehouses", arrayWarehouses);
		
		String jsonString = jsonobj.toString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE,"Login response json data"+jsonString);
		return jsonString;
	}

	private JSONArray getWarehousesOfCluster(String clusterwarehouse, List<WareHouse> warehouselist) {
		JSONArray arrClsutersWarehouses = new JSONArray();
		
		for(WareHouse warehouse : warehouselist){
			if(warehouse.getstatus() && warehouse.getWarehouseExpiryDate() !=null && warehouse.getWarehouseExpiryDate().after(new Date()) 
				&& warehouse.getParentWarehouse().trim().equalsIgnoreCase(clusterwarehouse.trim())){
				arrClsutersWarehouses.put(warehouse.getBusinessUnitName());
			}
		}
		
		return arrClsutersWarehouses;
	}

	private ArrayList<String> getClusterWarehouseOnly(List<WareHouse> warehouselist) {
    	ArrayList<String> clusterWarehouselist = new ArrayList<String>();
    	for(WareHouse warehouse : warehouselist){
    		if(warehouse.getstatus() && warehouse.getWarehouseExpiryDate() !=null && warehouse.getWarehouseExpiryDate().after(new Date())
    				&& warehouse.getWarehouseType().trim().equalsIgnoreCase("CLUSTER OFFICE") 
					&& warehouse.getParentWarehouse().trim().equalsIgnoreCase("NBHC HO")){
    			clusterWarehouselist.add(warehouse.getBusinessUnitName());
    		}
    	}
		return clusterWarehouselist;
	}

	private ArrayList<String> getfromWarehouse(List<WareHouse> warehouselist) {
		ArrayList<String> fromWarehouselist = new ArrayList<String>();
		for(WareHouse warehouse : warehouselist){
			if(warehouse.getstatus())
			fromWarehouselist.add(warehouse.getBusinessUnitName());
		}
		return fromWarehouselist;
	}
	
	private ArrayList<String> getTowarehouse(List<WareHouse> warehouselist, ArrayList<String> strBranch) {
		ArrayList<String> towarehouselist = new ArrayList<String>();
		for(String branch :strBranch){
			for(WareHouse warehouse : warehouselist){
				if(warehouse.getstatus()){
					if(warehouse.getParentWarehouse().equals(branch)){
						towarehouselist.add(warehouse.getBusinessUnitName());
					}
				}
				
			}
		}
		
		return towarehouselist;
	}
	
	
//	private boolean Validate(long companyId, String productCode, String fromWarehouse, String startSerialNo,
//			String endSerialNo) {
//		
//		String serialNumberRange = startSerialNo+"-"+endSerialNo;
//		long startSerialNumber = getSerialNumber(startSerialNo);
//		long endSerialNumber = getSerialNumber(endSerialNo);
//		boolean validateFlag = false;
//		SerialNumberStockMaster serialStockMaster = ofy().load().type(SerialNumberStockMaster.class)
//				.filter("warehouseName", fromWarehouse)
//				.filter("productCode", productCode)
//				.filter("companyId", companyId)
//				.filter("serialNumberRange", serialNumberRange)
//				.filter("status", true).first().now();
//
//		logger.log(Level.SEVERE, "serialStockMaster "+serialStockMaster);
//		logger.log(Level.SEVERE, "serialNumberRange "+serialNumberRange);
//
//		if(serialStockMaster!=null){
//			logger.log(Level.SEVERE, "Flag True ====");
//				validateFlag =true;
//		}
//		else{
//			List<SerialNumberStockMaster> serialStockMasterlist = ofy().load().type(SerialNumberStockMaster.class)
//					.filter("warehouseName", fromWarehouse)
//					.filter("productCode", productCode)
//					.filter("companyId", companyId)
//					.filter("status", true).list();
//			logger.log(Level.SEVERE, "serialStockMasterlist size"+serialStockMasterlist.size());
//			
//			if(serialStockMasterlist.size()!=0){
//				for(SerialNumberStockMaster serialNumberStockMaster : serialStockMasterlist){
//					if(serialNumberStockMaster.getInActiveserailNumberlist()!=null && serialNumberStockMaster.getInActiveserailNumberlist().size()!=0){
//						 boolean flag = false;
//						for(SerialNumberMapping srmappingNumber : serialNumberStockMaster.getInActiveserailNumberlist()){
//							 String strserialNumberRange[] =  srmappingNumber.getProSerialNo().split("-"); 
//							 long fromSerialNumber = Long.parseLong(strserialNumberRange[0].trim());
//							 long toSerialNumber = Long.parseLong(strserialNumberRange[1].trim());
//							if(fromSerialNumber>=startSerialNumber && toSerialNumber<=endSerialNumber){
//								flag  = true;
//								break;
//							}
//							else{
//								for(SerialNumberMapping srNumber : serialNumberStockMaster.getSerailNumberlist()){
//									if(srNumber.isStatus() && srNumber.getSerialNumber()>=startSerialNumber
//													&& srNumber.getSerialNumber() <= endSerialNumber){
//										validateFlag =true;
//										logger.log(Level.SEVERE, "Flag True 1");
//										break;
//									}
//								}
//							}
//						}
//						if(flag){
//							continue;
//						}
//						if(validateFlag){
//							break;
//						}
//					}
//					else{
//						for(SerialNumberMapping srNumber : serialNumberStockMaster.getSerailNumberlist()){
//							if(srNumber.isStatus() && srNumber.getSerialNumber()>=startSerialNumber
//											&& srNumber.getSerialNumber() <= endSerialNumber){
//								validateFlag =true;
//								logger.log(Level.SEVERE, "Flag True 2");
//								break;
//							}
//						}
//					
//					}
//					if(validateFlag){
//						break;
//					}
//				}
//			}
//			else{
//				logger.log(Level.SEVERE, "No Stock Master");
//				return true;
//			}
//		}
//			
//		
//		return validateFlag;
//	}
//	
//	private boolean ValidateGRN(long companyId, String productCode, String fromWarehouse, String startSerialNo,
//			String endSerialNo) {
//		logger.log(Level.SEVERE, "GRN Validation");
//		String serialNumberRange = startSerialNo+"-"+endSerialNo;
//		long startSerialNumber = getSerialNumber(startSerialNo);
//		long endSerialNumber = getSerialNumber(endSerialNo);
//		boolean validateFlag = true;
//		SerialNumberStockMaster serialStockMaster = ofy().load().type(SerialNumberStockMaster.class)
//				.filter("productCode", productCode)
//				.filter("companyId", companyId)
//				.filter("serialNumberRange", serialNumberRange)
//				.filter("status", true).first().now();
//
//		logger.log(Level.SEVERE, "serialStockMaster "+serialStockMaster);
//		logger.log(Level.SEVERE, "serialNumberRange "+serialNumberRange);
//
//		if(serialStockMaster!=null){
//			logger.log(Level.SEVERE, "Flag True ====");
//			return	validateFlag =false;
//		}
//		else{
//			List<SerialNumberStockMaster> serialStockMasterlist = ofy().load().type(SerialNumberStockMaster.class)
//					.filter("productCode", productCode)
//					.filter("companyId", companyId)
//					.filter("status", true).list();
//			logger.log(Level.SEVERE, "serialStockMasterlist size"+serialStockMasterlist.size());
//			
//			if(serialStockMasterlist.size()!=0){
//				for(SerialNumberStockMaster serialNumberStockMaster : serialStockMasterlist){
//					if(serialNumberStockMaster.getInActiveserailNumberlist()!=null && serialNumberStockMaster.getInActiveserailNumberlist().size()!=0){
//						 boolean flag = false;
//						for(SerialNumberMapping srmappingNumber : serialNumberStockMaster.getInActiveserailNumberlist()){
//							 String strserialNumberRange[] =  srmappingNumber.getProSerialNo().split("-"); 
//							 long fromSerialNumber = Long.parseLong(strserialNumberRange[0].trim());
//							 long toSerialNumber = Long.parseLong(strserialNumberRange[1].trim());
//							if(fromSerialNumber>=startSerialNumber && toSerialNumber<=endSerialNumber){
//								flag  = true;
//								break;
//							}
//							else{
//								for(SerialNumberMapping srNumber : serialNumberStockMaster.getSerailNumberlist()){
//									if(srNumber.isStatus() && srNumber.getSerialNumber()>=startSerialNumber
//													&& srNumber.getSerialNumber() <= endSerialNumber){
//										validateFlag =false;
//										logger.log(Level.SEVERE, "Flag True 1");
//										break;
//									}
//								}
//							}
//						}
//						if(flag){
//							continue;
//						}
//						if(validateFlag==false){
//							break;
//						}
//					}
//					else{
//						if(serialNumberStockMaster.getSerailNumberlist()!=null){
//							for(SerialNumberMapping srNumber : serialNumberStockMaster.getSerailNumberlist()){
//								if(srNumber.isStatus() && srNumber.getSerialNumber()>=startSerialNumber
//												&& srNumber.getSerialNumber() <= endSerialNumber){
//									validateFlag =false;
//									logger.log(Level.SEVERE, "Flag True 2");
//									break;
//								}
//							}
//						}
//						
//					
//					}
//					if(validateFlag==false){
//						break;
//					}
//				}
//			}
//			else{
//				logger.log(Level.SEVERE, "No Stock Master");
//				return true;
//			}
//		}
//		return validateFlag;
//	}
	
	private double validatePRQty(String productCode, String branchName, long companyId, double quantity, String fromWarehouse) {
		double prQty = 0;
		List<PurchaseRequisition> purchaseReqlist = ofy().load().type(PurchaseRequisition.class)
				.filter("companyId", companyId).filter("purchasedetails.branch", branchName)
				.filter("purchasedetails.status", PurchaseRequisition.APPROVED).list();
		logger.log(Level.SEVERE, "purchaseReqlist "+purchaseReqlist.size());
		if(purchaseReqlist.size()!=0){
			for (PurchaseRequisition purchaseReqn : purchaseReqlist) {
				for (ProductDetails prDetails : purchaseReqn.getPrproduct()) {
					if (productCode.equals(prDetails.getProductCode())
							&& fromWarehouse.trim().equals(prDetails.getWarehouseName().trim())) {
						prQty +=prDetails.getGrnBalancedQty();
					}
				}
			}	
		}
		logger.log(Level.SEVERE, "Balnce PR Qty "+prQty);
		return prQty;
	}
	
	private double validateMRNQty(String productCode, String branchName, long companyId, double quantity,
			String fromWarehouse, String transferToWarehouse) {
		double mrnQty = 0;
		List<MaterialRequestNote> materialRequestNotelist = ofy().load().type(MaterialRequestNote.class)
				.filter("companyId",companyId).filter("branch", branchName)
				.filter("status", MaterialRequestNote.APPROVED).list();
		logger.log(Level.SEVERE, "materialRequestNotelist "+materialRequestNotelist.size());
		if(materialRequestNotelist.size()!=0){
				for(MaterialRequestNote mrn1 : materialRequestNotelist){
					for(MaterialProduct mrnMaterialProd : mrn1.getSubProductTableMrn()){
						if(productCode.trim().equals(mrnMaterialProd.getMaterialProductCode().trim())
							&& transferToWarehouse.trim().equals(mrnMaterialProd.getMaterialProductWarehouse().trim())	){
							mrnQty +=mrnMaterialProd.getMaterialProductBalanceQty();
						}
					}	
				}
								
		}
		logger.log(Level.SEVERE, "Balnce MRN Qty "+mrnQty);
		return mrnQty;
	}
	
	
	public Approvals sendApprovalRequest(int count, String username, long companyId, MaterialIssueNote min) {

		logger.log(Level.SEVERE, "Approval Sending == MIN =="+count);
		Approvals approval = new Approvals();
		try {
		approval.setApproverName(username);
		approval.setBranchname(min.getBranch());
		approval.setBusinessprocessId(count);
		if(username!=null){
		approval.setRequestedBy(username);
		}
		if(min.getEmployee()!=null)
		approval.setPersonResponsible(min.getEmployee());
		if(min.getCreatedBy()!=null){
			approval.setDocumentCreatedBy(min.getCreatedBy());
		}
		approval.setApprovalLevel(1);
		approval.setStatus(Approvals.APPROVED);
		approval.setBusinessprocesstype("MIN");
		approval.setDocumentValidation(false);
		approval.setCompanyId(companyId);
		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(approval);
		logger.log(Level.SEVERE, "Approval Sent successfully");
		
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception"+e.getMessage());
		}
		return approval;
	}

	private String getProductSerialNumber(String startSerialNo, String endSerialNo) {
		return startSerialNo+"-"+endSerialNo;
	}
	
	private boolean Validate(long companyId, String productCode, String fromWarehouse, String startSerialNo,
			String endSerialNo, String documentType) {
		
		String serialNumberRange = startSerialNo+"-"+endSerialNo;
		long startSerialNumber = getSerialNumber(startSerialNo);
		long endSerialNumber = getSerialNumber(endSerialNo);
		boolean validateFlag = false;
		SerialNumberStockMaster serialStockMaster = ofy().load().type(SerialNumberStockMaster.class)
				.filter("warehouseName", fromWarehouse)
				.filter("productCode", productCode)
				.filter("companyId", companyId)
				.filter("serialNumberRange", serialNumberRange)
				.filter("status", true).first().now();

		logger.log(Level.SEVERE, "serialStockMaster "+serialStockMaster);
		logger.log(Level.SEVERE, "serialNumberRange "+serialNumberRange);

		if(serialStockMaster!=null){
			logger.log(Level.SEVERE, "Flag True ====");
				validateFlag =true;
				if(documentType.equals("MMN")){
					return true;
				}
		}
		else{
			List<SerialNumberStockMaster> serialStockMasterlist = ofy().load().type(SerialNumberStockMaster.class)
					.filter("warehouseName", fromWarehouse)
					.filter("productCode", productCode)
					.filter("companyId", companyId)
					.filter("status", true).list();
			logger.log(Level.SEVERE, "serialStockMasterlist size"+serialStockMasterlist.size());
			if(serialStockMasterlist.size()==0){
				logger.log(Level.SEVERE, "warehouseName"+fromWarehouse);
				logger.log(Level.SEVERE, "productCode"+productCode);
				logger.log(Level.SEVERE, "companyId"+companyId);
			}
			if(serialStockMasterlist.size()!=0){
				for(SerialNumberStockMaster serialNumberStockMaster : serialStockMasterlist){
					if(documentType.equals("MIN")){
						validateFlag = isWithinRage(serialNumberStockMaster.getStartSerialNumber(),
								serialNumberStockMaster.getEndSerialNumber(),startSerialNumber);
						logger.log(Level.SEVERE, "MIN validateFlag"+validateFlag);
						if(validateFlag){
							break;
						}
					}
//					else if(documentType.equals("MMN")){
//						boolean start = isWithinRage(serialNumberStockMaster.getStartSerialNumber(),
//								serialNumberStockMaster.getEndSerialNumber(),startSerialNumber);
//						logger.log(Level.SEVERE, "MMN start"+start);
//
//						boolean end = isWithinRage(serialNumberStockMaster.getStartSerialNumber(),
//								serialNumberStockMaster.getEndSerialNumber(),endSerialNumber);
//						logger.log(Level.SEVERE, "MMN end"+end);
//						if(start && end){
//							validateFlag = true;
//							break;
//						}	
//					}
				}
			}
			else{
				logger.log(Level.SEVERE, "No Stock Master");
				return false;
			}
		}
		return validateFlag;
	}
	
	private boolean isWithinRage(long startSerialNumber, long endSerialNumber, long SerialNumber) {
		return (startSerialNumber <= SerialNumber && SerialNumber <= endSerialNumber);
	}
	

	private boolean ValidateGRN(long companyId, String productCode, String fromWarehouse, String startSerialNo,
			String endSerialNo) {
		logger.log(Level.SEVERE, "GRN Validation");
		String serialNumberRange = startSerialNo+"-"+endSerialNo;
		
		SerialNumberStockMaster serialStockMaster = ofy().load().type(SerialNumberStockMaster.class)
				.filter("productCode", productCode)
				.filter("companyId", companyId)
				.filter("serialNumberRange", serialNumberRange)
				.filter("status", true).first().now();

		logger.log(Level.SEVERE, "serialStockMaster "+serialStockMaster);
		logger.log(Level.SEVERE, "serialNumberRange "+serialNumberRange);

		if(serialStockMaster!=null){
			logger.log(Level.SEVERE,"this serial number range already assigned to another warehouse");
			return false;
		}
		else{
			return true;
		}
//		long startSerialNumber = getSerialNumber(startSerialNo);
//		long endSerialNumber = getSerialNumber(endSerialNo);
//		boolean validateFlag = true;
//		if(serialStockMaster!=null){
//			logger.log(Level.SEVERE, "Flag True ====");
//			return	validateFlag =false;
//		}
//		else{
//			List<SerialNumberStockMaster> serialStockMasterlist = ofy().load().type(SerialNumberStockMaster.class)
//					.filter("productCode", productCode)
//					.filter("companyId", companyId)
//					.filter("status", true).list();
//			logger.log(Level.SEVERE, "serialStockMasterlist size"+serialStockMasterlist.size());
//			
//			if(serialStockMasterlist.size()!=0){
//				for(SerialNumberStockMaster serialNumberStockMaster : serialStockMasterlist){
//
//					boolean startNumberFlag = isWithinRage(serialNumberStockMaster.getStartSerialNumber(),
//							serialNumberStockMaster.getEndSerialNumber(),startSerialNumber);
//					logger.log(Level.SEVERE, "GRN startNumberFlag"+startNumberFlag);
//
//					boolean endNumberFlag = isWithinRage(serialNumberStockMaster.getStartSerialNumber(),
//							serialNumberStockMaster.getEndSerialNumber(),endSerialNumber);
//					logger.log(Level.SEVERE, "GRN endNumberFlag"+endNumberFlag);
//					if(startNumberFlag && endNumberFlag){
//						for(SerialNumberMapping srNumber : serialNumberStockMaster.getSerailNumberlist()){
//							if(srNumber.getSerialNumber()==startSerialNumber && srNumber.isStatus() 
//									|| srNumber.getSerialNumber()==endSerialNumber &&srNumber.isStatus()){
//								validateFlag = false;
//								break;
//							}
//						}
//						if(validateFlag==false){
//							break;
//						}
//					}	
//				
//				}
//			}
//			else{
//				logger.log(Level.SEVERE, "No Stock Master");
//				return true;
//			}
//		}
//		return validateFlag;
	}
	
	private Approvals sendApprovalRequestGRNMMN(int count, String username, long companyId, String branch, String employee,
								String createdby, String documentName) {

		logger.log(Level.SEVERE, "Approval Sending == MIN =="+count);
		Approvals approval = new Approvals();
		try {
		approval.setApproverName(username);
		approval.setBranchname(branch);
		approval.setBusinessprocessId(count);
		if(username!=null){
		approval.setRequestedBy(username);
		}
		if(employee!=null)
		approval.setPersonResponsible(employee);
		if(createdby!=null){
			approval.setDocumentCreatedBy(createdby);
		}
		approval.setApprovalLevel(1);
		approval.setStatus(Approvals.APPROVED);
		approval.setBusinessprocesstype(documentName);
		approval.setDocumentValidation(false);
		approval.setCompanyId(companyId);
		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(approval);
		logger.log(Level.SEVERE, "Approval Sent successfully"+documentName);
		
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception"+e.getMessage());
		}
		return approval;
	}
	
	private String getDecodeOfWarehouseName(String fromWarehouse) {
		
		try {
			return URLDecoder.decode(fromWarehouse, StandardCharsets.UTF_8.toString());
		} catch (UnsupportedEncodingException e) {
			
			throw new RuntimeException(e.getCause());
		}
	}
}
