package com.slicktechnologies.server.android.InventoryManagementApp;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.bcel.generic.FMUL;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.inventorydashboard.MrnTableProxy;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.TechnicianWareHouseDetailsList;
import com.slicktechnologies.shared.TechnicianWarehouseDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.inventory.InventoryTransactionLineItem;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.UpdateStock;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;

import static com.googlecode.objectify.ObjectifyService.ofy;


public class InventoryManagementApp extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1449906466376086201L;

	Logger logger = Logger.getLogger("Logger");

	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		doPost(req, resp);
		
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		String actionTask="";
		String strCompanyId = "";
		
		String urlCalled = req.getRequestURL().toString().trim();
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		logger.log(Level.SEVERE, "splitUrl::" + splitUrl);
		
		Company comp = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		long companyid = comp.getCompanyId();
		logger.log(Level.SEVERE, "companyid"+companyid);

		JSONObject msgjsonobj = new JSONObject();

		
		try {
			actionTask=req.getParameter("actionTask").trim();
		} catch (Exception e) {
		}
		try {
			strCompanyId = req.getParameter("authocde").trim();;
		} catch (Exception e) {
		}
		
		long companyId = 0;
		try {
			companyId = Long.parseLong(strCompanyId);
			logger.log(Level.SEVERE, "companyId"+companyId);
		} catch (Exception e) {
		}

		if(actionTask==null || actionTask.equals("")) {
			resp.getWriter().println(getMessageinJson(msgjsonobj,"Action task can not be blank"));
		}
		else if(strCompanyId == null ) {
//			resp.getWriter().println("Auth code can not be blank");
			resp.getWriter().println(getMessageinJson(msgjsonobj,"Auth code can not be blank"));
		}
//		else if(companyId!=companyid) {
////			resp.getWriter().println("Authintication failed");
//			resp.getWriter().println(getMessageinJson(msgjsonobj,"Authintication failed"));
//		}
		else {
			logger.log(Level.SEVERE, "actionTask"+actionTask);

			if(actionTask.trim().equals("getWarehouses")){
				String strEmployeeId = "";
				try {
					strEmployeeId = req.getParameter("employeeId").trim();
					String data = getEmployeeWarehouses(strEmployeeId,companyId);
					logger.log(Level.SEVERE, "warehouses data"+data);
					
					resp.getWriter().println(data);
				} catch (Exception e) {
//					resp.getWriter().println("please send technician employee id");
					resp.getWriter().println(getMessageinJson(msgjsonobj,"please send technician employee id"));
					
				}
			}
			else if(actionTask.trim().equals("getStockDetails")) {
				try {
				
				String validationMsg = "";
				
				String warehouseName = "";
				String storageLocaton = "";
				String storageBin = "";
				try {
					 warehouseName = req.getParameter("warehouseName");
					 storageLocaton = req.getParameter("storageLocation");
					 storageBin = req.getParameter("storageBin");
				} catch (Exception e) {
				}
				
				if(warehouseName==null || warehouseName.equals("")) {
					validationMsg =" warehouseName can no be blank." ;
				}
				if(storageLocaton==null || storageLocaton.equals("")) {
					validationMsg +=" storage location can no be blank." ;
				}
				if(storageBin==null || storageBin.equals("")) {
					validationMsg +=" storage bin can no be blank." ;
				}
				
				logger.log(Level.SEVERE, "validation msg "+validationMsg);
				if(validationMsg.equals("")) {
					
					List<ProductInventoryViewDetails> productInventorydetailslist = ofy().load().type(ProductInventoryViewDetails.class).filter("companyId", companyId)
							.filter("warehousename", warehouseName).filter("storagelocation", storageLocaton).filter("storagebin", storageBin).list();
					logger.log(Level.SEVERE, "productInventorydetailslist size "+productInventorydetailslist.size());
					if(productInventorydetailslist.size()>0) {
						JSONObject jsonobj = new JSONObject();
						JSONArray jsonarray = new JSONArray();
						
						for(ProductInventoryViewDetails productInventory : productInventorydetailslist) {
							JSONObject obj = new JSONObject();
							try {
								obj.put("productName", productInventory.getProdname());
								obj.put("availableQty", productInventory.getAvailableqty());
								obj.put("reorderLevel", productInventory.getReorderlevel());
								obj.put("productId", productInventory.getProdid());
								jsonarray.put(obj);
								
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
						try {
							jsonobj.put("stockDetails", jsonarray);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						String jsonstring = jsonobj.toString().replaceAll("\\\\", "");
						logger.log(Level.SEVERE, "stockDetails Json"+jsonstring);
						resp.getWriter().println(jsonstring);
				
					}
					else {
						resp.getWriter().println(getMessageinJson(msgjsonobj,"No data found"));

					}
					
				}
				else {
					resp.getWriter().println(getMessageinJson(msgjsonobj,validationMsg));

				}
				
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if(actionTask.trim().equals("MRN")) {
				
				
				String jsonData=req.getParameter("data").trim();
				JSONArray dataArray = null;
				JSONObject jsonobj = null;

				try {
				logger.log(Level.SEVERE, "jsonData " + jsonData);
				JSONObject serviceJSONObject = new JSONObject(jsonData);
				
					dataArray = serviceJSONObject.getJSONArray("mrnData");
					
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
				
				String validationMsg ="";

				String strEmployeeId = req.getParameter("employeeId").trim();

				List<MaterialProduct> productlist = new ArrayList<MaterialProduct>();

				for(int m =0 ; m<dataArray.length() ; m++){
					
					try {
						jsonobj = dataArray.getJSONObject(m);
					} catch (JSONException e1) {
						e1.printStackTrace();
					}

					String warehouseName = "";
					String storageLocaton = "";
					String storageBin = "";
						
					try {
						warehouseName = jsonobj.optString("warehouseName");
						storageLocaton = jsonobj.optString("storageLocation");
						storageBin = jsonobj.optString("storageBin");
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					String strproductId = "";
					try {
						strproductId = jsonobj.optString("productId");
					} catch (Exception e) {
					}
					String strAvailableQty = "";
					double productAvailableQty = 0;
					try {
						strAvailableQty = jsonobj.optString("availableQty");
						productAvailableQty = Double.parseDouble(strAvailableQty);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					String strRequesteQty ="";
					double productRequestedQty = 0;
					
					try {
						strRequesteQty = jsonobj.optString("requestedQty");
						productRequestedQty = Double.parseDouble(strRequesteQty);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					if(warehouseName==null || warehouseName.equals("")) {
						validationMsg =" warehouseName can no be blank." ;
					}
					if(storageLocaton==null || storageLocaton.equals("")) {
						validationMsg +=" storage location can no be blank." ;
					}
					if(storageBin==null || storageBin.equals("")) {
						validationMsg +=" storage bin can no be blank." ;
					}
					if(strproductId==null || strproductId.equals("") || strproductId.equals("")) {
						validationMsg +=" product id can no be blank or zero." ;
					}
					if(productRequestedQty==0) {
						validationMsg += " Quantity can not be zero.";
					}
					if(productRequestedQty<0) {
						validationMsg += " Quantity can not be negative value.";
					}
					
					int productId = 0;
					if(strproductId!=null && !strproductId.equals("") || strproductId.equals("0")) {
						productId = Integer.parseInt(strproductId);
					}
					
					ItemProduct itemproduct = ofy().load().type(ItemProduct.class).filter("companyId", companyId).filter("count", productId).first().now();

					if(itemproduct==null) {
						validationMsg += " Product id "+productId+" does not exist in ERP";

					}

					if(validationMsg.equals("")) {
						
						MaterialProduct prod = new MaterialProduct();
						prod.setMaterialProductId(itemproduct.getCount());
						prod.setMaterialProductCode(itemproduct.getProductCode());
						prod.setMaterialProductCategory(itemproduct.getProductCategory());
						prod.setMaterialProductName(itemproduct.getProductName());
						prod.setMaterialProductAvailableQuantity(productAvailableQty);
						prod.setMaterialProductUOM(itemproduct.getUnitOfMeasurement());
						prod.setMaterialProductRequiredQuantity(productRequestedQty);
						prod.setMaterialProductPlannedQuantity(productRequestedQty);
						prod.setMaterialProductRemarks("");
						prod.setMaterialProductWarehouse(warehouseName);
						prod.setMaterialProductStorageLocation(storageLocaton);
						prod.setMaterialProductStorageBin(storageBin);
						
						productlist.add(prod);
					}

					
					
					}
					
					logger.log(Level.SEVERE, "validation msg "+validationMsg);
					logger.log(Level.SEVERE, "productlist size "+productlist.size());
					logger.log(Level.SEVERE, "condition"+(validationMsg.equals("") && productlist.size()>0));

					if(validationMsg.equals("") && productlist.size()>0) {
						String warehouseName = "";
						String storageLocaton = "";
						String storageBin = "";
						
						try {
							 warehouseName = productlist.get(0).getMaterialProductWarehouse();
							 storageLocaton = productlist.get(0).getMaterialProductStorageLocation();
							 storageBin = productlist.get(0).getMaterialProductStorageBin();

						} catch (Exception e) {
							// TODO: handle exception
						}
						

					int employeeId = Integer.parseInt(strEmployeeId);
					Employee employeeEntity = getemployeDetails(companyId, employeeId);
					
					MaterialRequestNote mrnEntity = new MaterialRequestNote();
					mrnEntity.setCompanyId(companyId);
					
					String mrnTitle = "";
					try {
						if(employeeEntity!=null) {
							mrnTitle = employeeEntity.getFullname() +" requested on date"+dateFormat.format(new Date())+" "+warehouseName+" "+storageLocaton+" "+storageBin;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					mrnEntity.setMrnTitle(mrnTitle);
					mrnEntity.setBranch(employeeEntity.getBranchName());
					mrnEntity.setEmployee(employeeEntity.getFullname());
					mrnEntity.setStatus(MaterialRequestNote.REQUESTED);
					String approverName = getApproverName(employeeEntity.getBranchName(),companyId);
					logger.log(Level.SEVERE, "approverName"+approverName);
					mrnEntity.setApproverName(approverName);
					mrnEntity.setMrnDate(new Date());
					mrnEntity.setSubProductTableMrn(productlist);
					
					GenricServiceImpl genimpl = new GenricServiceImpl();
					ReturnFromServer server =  genimpl.save(mrnEntity);
					int mrnId = server.count;
					logger.log(Level.SEVERE, "MRN Id"+mrnId);
					ServerAppUtility serverUtility = new ServerAppUtility();
					serverUtility.sendApprovalRequest(mrnEntity.getBranch(), mrnId, companyId, mrnEntity.getApproverName(),mrnEntity.getEmployee(), mrnEntity.getEmployee(), "MRN");
					
					resp.getWriter().println(getMessageinJson(msgjsonobj,"MRN created successfully. MRN id - "+mrnId+""));

					}
					else {
						resp.getWriter().println(getMessageinJson(msgjsonobj,validationMsg));

					}
					
				
////				old code
//				try {
//				String strEmployeeId = req.getParameter("employeeId").trim();
//				String validationMsg = "";
//				String warehouseName = "";
//				String storageLocaton = "";
//				String storageBin = "";
//				
//				try {
//					warehouseName = req.getParameter("warehouseName");
//					storageLocaton = req.getParameter("storageLocation");
//					storageBin = req.getParameter("storageBin");
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				
//				String strproductId = "";
//				try {
//					strproductId = req.getParameter("productId");
//				} catch (Exception e) {
//				}
//				String strAvailableQty = "";
//				double productAvailableQty = 0;
//				try {
//					strAvailableQty = req.getParameter("availableQty");
//					productAvailableQty = Double.parseDouble(strAvailableQty);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				
//				String strRequesteQty ="";
//				double productRequestedQty = 0;
//				
//				try {
//					strRequesteQty = req.getParameter("requestedQty");
//					productRequestedQty = Double.parseDouble(strRequesteQty);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				
//				if(warehouseName==null || warehouseName.equals("")) {
//					validationMsg =" warehouseName can no be blank." ;
//				}
//				if(storageLocaton==null || storageLocaton.equals("")) {
//					validationMsg +=" storage location can no be blank." ;
//				}
//				if(storageBin==null || storageBin.equals("")) {
//					validationMsg +=" storage bin can no be blank." ;
//				}
//				if(strproductId==null || strproductId.equals("") || strproductId.equals("")) {
//					validationMsg +=" product id can no be blank or zero." ;
//				}
//				if(productRequestedQty==0) {
//					validationMsg += "Quantity can not be zero.";
//				}
//				if(productRequestedQty<0) {
//					validationMsg += "Quantity can not be negative value.";
//				}
//				
//				logger.log(Level.SEVERE, "validation msg "+validationMsg);
//				if(validationMsg.equals("")) {
//					
//				int productId = 0;
//				if(strproductId!=null && !strproductId.equals("") || strproductId.equals("0")) {
//					productId = Integer.parseInt(strproductId);
//				}
//				int employeeId = Integer.parseInt(strEmployeeId);
//				Employee employeeEntity = getemployeDetails(companyId, employeeId);
//				
//				MaterialRequestNote mrnEntity = new MaterialRequestNote();
//				mrnEntity.setCompanyId(companyId);
//				String mrnTitle = "";
//				if(employeeEntity!=null) {
//					mrnTitle = employeeEntity.getFullname() +" requested on date"+dateFormat.format(new Date())+" "+warehouseName+" "+storageLocaton+" "+storageBin;
//				}
//				mrnEntity.setMrnTitle(mrnTitle);
//				mrnEntity.setBranch(employeeEntity.getBranchName());
//				mrnEntity.setEmployee(employeeEntity.getFullname());
//				mrnEntity.setStatus(MaterialRequestNote.REQUESTED);
//				String approverName = getApproverName(employeeEntity.getBankBranch(),companyId);
//				mrnEntity.setApproverName(approverName);
//				
//				ItemProduct itemproduct = ofy().load().type(ItemProduct.class).filter("companyId", companyId).filter("count", productId).first().now();
//				
//				List<MaterialProduct> productlist = new ArrayList<MaterialProduct>();
//				MaterialProduct prod = new MaterialProduct();
//				prod.setMaterialProductId(itemproduct.getCount());
//				prod.setMaterialProductCode(itemproduct.getProductCode());
//				prod.setMaterialProductCategory(itemproduct.getProductCategory());
//				prod.setMaterialProductName(itemproduct.getProductName());
//				prod.setMaterialProductAvailableQuantity(productAvailableQty);
//				prod.setMaterialProductUOM(itemproduct.getUnitOfMeasurement());
//				prod.setMaterialProductRequiredQuantity(productRequestedQty);
//				prod.setMaterialProductRemarks("");
//				prod.setMaterialProductWarehouse(warehouseName);
//				prod.setMaterialProductStorageLocation(storageLocaton);
//				prod.setMaterialProductStorageBin(storageBin);
//				
//				productlist.add(prod);
//				
//				mrnEntity.setSubProductTableMrn(productlist);
//				
//				GenricServiceImpl genimpl = new GenricServiceImpl();
//				ReturnFromServer server =  genimpl.save(mrnEntity);
//				int mrnId = server.count;
//				logger.log(Level.SEVERE, "MRN Id"+mrnId);
//				ServerAppUtility serverUtility = new ServerAppUtility();
//				serverUtility.sendApprovalRequest(mrnEntity.getBranch(), mrnId, companyId, mrnEntity.getApproverName(),mrnEntity.getEmployee(), mrnEntity.getEmployee(), "MRN");
//				
//				resp.getWriter().println("MRN created successfully. MRN id - "+mrnId);
//				}
//				else {
//					resp.getWriter().println(validationMsg);
//				}
//				
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
			}
			else if(actionTask.trim().equals("MRN Cancellation")) {
				logger.log(Level.SEVERE, "Inside MRN Cancellation ");
	
				String strMrnId = "";
				String cancelledBy = "";
				String cancellationRemark ="";
				String validationmsg = "";
				try {
					strMrnId = req.getParameter("mrnId");
				} catch (Exception e) {
				}
				
				try {
					 cancelledBy = req.getParameter("cancelationBy");
				} catch (Exception e) {
				}
				
				try {
					cancellationRemark = req.getParameter("remark");
				} catch (Exception e) {
				}
				if(strMrnId ==null || strMrnId.equals("")) {
					validationmsg +=" MRN id can not be blank!";
				}
				if(cancelledBy ==null || cancelledBy.equals("")) {
					validationmsg +=" cancelled by can not be blank!";
				}
				if(cancellationRemark ==null || cancellationRemark.equals("")) {
					validationmsg +=" Remark can not be blank!";
				}
				
				if(validationmsg.equals("")) {
					int mrnId = Integer.parseInt(strMrnId);
					
					MaterialRequestNote mrnEntity = ofy().load().type(MaterialRequestNote.class).filter("companyId", companyId).filter("count", mrnId).first().now();
					logger.log(Level.SEVERE, "mrnEntity "+mrnEntity);
					if(mrnEntity!=null) {
						mrnEntity.setStatus(MaterialRequestNote.CANCELLED);
						cancellationRemark = cancellationRemark +" "+ cancelledBy + " on "+ dateFormat.format(new Date());
						mrnEntity.setRemark(cancellationRemark);
						ofy().save().entity(mrnEntity);
						resp.getWriter().println(getMessageinJson(msgjsonobj,"MRN cancelled successfully"));

					}
					else {
						resp.getWriter().println(getMessageinJson(msgjsonobj,"MRN does not exist!"));
					}
				}
				else {
					resp.getWriter().println(getMessageinJson(msgjsonobj,validationmsg));
				}
				
			}
			else if(actionTask.trim().equals("MRN Approval")) {
				String strMrnId = req.getParameter("mrnId");
				String approvalRemark = "";
				try {
						approvalRemark = req.getParameter("approvalRemark");
				} catch (Exception e) {
				}
			
				String approvalStatus = "";
				try {
					approvalStatus = req.getParameter("approvalStatus");
				} catch (Exception e) {
				}
				
				String materialData="";
				try {
					materialData = req.getParameter("materialData").trim();
				} catch (Exception e) {
					// TODO: handle exception
				}

				JSONObject serviceJSONObject = null;
				try {
					serviceJSONObject = new JSONObject(materialData);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				JSONArray materialArray = null;
				try {
					materialArray = serviceJSONObject.getJSONArray("material");
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
				JSONObject obj = null;
				ArrayList<MaterialProduct> approvedMaterialQtylist = new ArrayList<MaterialProduct>();
				for(int m =0 ; m<materialArray.length() ; m++){
						try {
							obj = materialArray.getJSONObject(m);
							if(obj.optDouble("approvedQty")>0) {
								MaterialProduct prod = new MaterialProduct();
								prod.setMaterialProductId(obj.optInt("productId"));
								prod.setMaterialProductRequiredQuantity(obj.optDouble("approvedQty"));
								approvedMaterialQtylist.add(prod);
							}
							
						} catch (JSONException e) {
							e.printStackTrace();
						}
				}		
				
				String validationMsg = "";
				if(approvalStatus==null || approvalStatus.equals("")) {
					validationMsg = "Approval status can not be blank!";
				}
				if(strMrnId==null || strMrnId.equals("")) {
					validationMsg += " Mrn id can not be blank!";
				}
				logger.log(Level.SEVERE, "validationMsg"+validationMsg+"done here");
	
				if(validationMsg.equals("")) {
					int mrnId = Integer.parseInt(strMrnId);
	
					Approvals approvalEntity = ofy().load().type(Approvals.class).filter("businessprocessId", mrnId).filter("companyId", companyId)
												.filter("businessprocesstype", "MRN").first().now();
					logger.log(Level.SEVERE, "approvalEntity "+approvalEntity);
					if(approvalEntity!=null) {
						approvalEntity.setRemark(approvalRemark);
						approvalEntity.setStatus(approvalStatus);
						ofy().save().entity(approvalEntity);
						logger.log(Level.SEVERE, "approvalEntity document approved successfully"+approvalEntity);
						
						MaterialRequestNote mrnEntity = ofy().load().type(MaterialRequestNote.class).filter("companyId", companyId).filter("count", mrnId).first().now();
						logger.log(Level.SEVERE, "mrnEntity "+mrnEntity);
						if(mrnEntity!=null) {
							String mrnstatus=mrnEntity.getStatus();

						if(approvalStatus!=null && approvalStatus.equals(Approvals.APPROVED)) {
							label: for(MaterialProduct materialproduct : mrnEntity.getSubProductTableMrn()) {
								for(MaterialProduct product : approvedMaterialQtylist) {
									if(product.getMaterialProductId()==materialproduct.getMaterialProductId()) {
										logger.log(Level.SEVERE, "product.getMaterialProductId()"+product.getMaterialProductId());
										logger.log(Level.SEVERE, "product.getMaterialProductRequiredQuantity()"+product.getMaterialProductRequiredQuantity());
										materialproduct.setMaterialProductRequiredQuantity(product.getMaterialProductRequiredQuantity());
									}
								}
							}
							
							String msg = createMMNOnBasisOfMRN(mrnEntity);
							logger.log(Level.SEVERE, "msg "+msg);
							resp.getWriter().println(getMessageinJson(msgjsonobj,msg));
						}
						else {
							logger.log(Level.SEVERE, "approvalStatus "+approvalStatus);

							mrnEntity.setStatus(approvalStatus);
							if(approvalRemark!=null && !approvalRemark.equals(""))
								mrnEntity.setMrnDescription(approvalRemark);
							else {
								String remark = "MRN ID ="+mrnEntity.getCount()+""+" MRN status ="+mrnstatus+
										"has been rejected on "+new Date();
								mrnEntity.setMrnDescription(remark);
							}

							ofy().save().entity(mrnEntity);

						}
						
						}
						else {
							resp.getWriter().println(getMessageinJson(msgjsonobj,"MRN id does not exist!"));

						}
						
					}
					else {
						resp.getWriter().println(getMessageinJson(msgjsonobj,"MRN id does not exist in the approval!"));

					}
				}
				else {
					resp.getWriter().println(getMessageinJson(msgjsonobj,validationMsg));

				}
			
			}
			else if(actionTask.trim().equals("MMN Approval")) {
				String strmmnId = req.getParameter("mmnId");
				
					logger.log(Level.SEVERE, "strmmnId"+strmmnId);
				
					String approvalRemark = "";
					try {
							approvalRemark = req.getParameter("approvalRemark");
					} catch (Exception e) {
					}
			
					String approvalStatus = "";
					try {
						approvalStatus = req.getParameter("approvalStatus");
					} catch (Exception e) {
					}
					
					String materialData="";
					try {
						materialData = req.getParameter("materialData").trim();
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					String userRole = "";
					boolean storekeeperflag = false;
					try {
						userRole = req.getParameter("userRole");
					} catch (Exception e) {
						userRole = "";
					}
					if(userRole!=null && !userRole.equals("") && userRole.equals(AppConstants.STOREKEEPER)) {
						storekeeperflag = true;
					}

					JSONObject serviceJSONObject = null;
					try {
						serviceJSONObject = new JSONObject(materialData);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					JSONArray materialArray = null;
					try {
						materialArray = serviceJSONObject.getJSONArray("material");
					} catch (JSONException e1) {
						e1.printStackTrace();
					}
					
					ArrayList<Integer> productidlist = new ArrayList<Integer>();
					
					
					JSONObject obj = null;
					ArrayList<MaterialProduct> approvedMaterialQtylist = new ArrayList<MaterialProduct>();
					for(int m =0 ; m<materialArray.length() ; m++){
							try {
								obj = materialArray.getJSONObject(m);
								if(obj.optDouble("approvedQty")>0) {
									MaterialProduct prod = new MaterialProduct();
									prod.setMaterialProductId(obj.optInt("productId"));
									prod.setMaterialProductRequiredQuantity(obj.optDouble("approvedQty"));
									approvedMaterialQtylist.add(prod);
									productidlist.add(obj.optInt("productId"));

								}
								
							} catch (JSONException e) {
								e.printStackTrace();
							}
					}	
					
					String validationMsg = "";
					if(approvalStatus==null || approvalStatus.equals("")) {
						validationMsg = "Approval status can not be blank!";
					}
					if(strmmnId==null || strmmnId.equals("")) {
						validationMsg += " MMN id can not be blank!";
					}
					logger.log(Level.SEVERE, "validationMsg"+validationMsg+"done here");
					int mmnId = 0;
					try {
						mmnId = Integer.parseInt(strmmnId);
					} catch (Exception e) {
					}
					logger.log(Level.SEVERE, "mmnId"+mmnId);

					MaterialMovementNote mmnEntity = null;
					try {
						mmnEntity = ofy().load().type(MaterialMovementNote.class).filter("companyId", companyId).filter("count", mmnId).first().now();
					} catch (Exception e) {
					}
					logger.log(Level.SEVERE, "mmnEntity "+mmnEntity);
					
					if(mmnEntity!=null && storekeeperflag) {
						String msg ="";
						List<ProductInventoryViewDetails> prodInventorylist = ofy().load().type(ProductInventoryViewDetails.class).filter("prodid IN", productidlist)
										.filter("companyId", companyId).list();
						logger.log(Level.SEVERE, "prodInventorylist size"+prodInventorylist.size());
						if(prodInventorylist.size()!=0) {
							for(MaterialProduct materialproduct : mmnEntity.getSubProductTableMmn()) {
								materialproduct.setMaterialProductAvailableQuantity(getmaterialAvailableQty(prodInventorylist, materialproduct));
						
								 for(MaterialProduct materialprod : approvedMaterialQtylist) {
									 if(materialprod.getMaterialProductId() == materialproduct.getMaterialProductId()) {
										 if(materialprod.getMaterialProductRequiredQuantity()>materialproduct.getMaterialProductAvailableQuantity()) {
											msg += "You are trying to issue "+materialprod.getMaterialProductRequiredQuantity()+" quantity for "+materialproduct.getMaterialProductName()+" more than available "+
													materialproduct.getMaterialProductAvailableQuantity();
											 break;
										 }
									 }
								 }	 
								
							}
						}
						logger.log(Level.SEVERE, "msg"+msg);
						if(!msg.trim().equals("")) {
							validationMsg +=msg;
						}

					}
					logger.log(Level.SEVERE, "validationMsg"+validationMsg);

					if(validationMsg.equals("")) {
						
						Approvals approvalEntity = ofy().load().type(Approvals.class).filter("businessprocessId", mmnId).filter("companyId", companyId)
													.filter("businessprocesstype", "MMN").first().now();
						logger.log(Level.SEVERE, "approvalEntity "+approvalEntity);
						if(approvalEntity!=null) {
							approvalEntity.setRemark(approvalRemark);
							approvalEntity.setStatus(approvalStatus);
							ofy().save().entity(approvalEntity);
							logger.log(Level.SEVERE, "approvalEntity document approved successfully"+approvalEntity);
							
//							MaterialMovementNote mmnEntity = ofy().load().type(MaterialMovementNote.class).filter("companyId", companyId).filter("count", mmnId).first().now();
//							logger.log(Level.SEVERE, "mmnEntity "+mmnEntity);
							
							if(mmnEntity!=null&&mmnEntity.getStatus().equals(MaterialMovementNote.APPROVED)) {
								resp.getWriter().println(getMessageinJson(msgjsonobj,"MMN already approved!"));
							}
							else if(mmnEntity!=null) {
								
								if(approvalStatus!=null && approvalStatus.equals(Approvals.APPROVED)) {
									
									for(MaterialProduct materialproduct : mmnEntity.getSubProductTableMmn()) {
										 for(MaterialProduct materialprod : approvedMaterialQtylist) {
											 if(materialprod.getMaterialProductId() == materialproduct.getMaterialProductId()) {
													logger.log(Level.SEVERE, "materialprod.getMaterialProductId()"+materialprod.getMaterialProductId());
													logger.log(Level.SEVERE, "materialprod.getMaterialProductRequiredQuantity()"+materialprod.getMaterialProductRequiredQuantity());
													materialproduct.setMaterialProductRequiredQuantity(materialprod.getMaterialProductRequiredQuantity());
												}
											 }
									 }
										
									 String response = MMNAppprovalProcess(mmnEntity);
									 logger.log(Level.SEVERE, "MMn Approval response "+response);
										resp.getWriter().println(getMessageinJson(msgjsonobj,response));

								}
								else {
									logger.log(Level.SEVERE, "approvalStatus "+approvalStatus);
									mmnEntity.setStatus(approvalStatus);
									if(approvalRemark!=null && !approvalRemark.equals(""))
										mmnEntity.setMmnDescription(approvalRemark);
									else {
										String remark = "MRN ID ="+mmnEntity.getCount()+""+" MRN status ="+approvalStatus+
												"has been rejected on "+new Date();
										mmnEntity.setMmnDescription(remark);
									}
									ofy().save().entity(mmnEntity);
									
								}
								 
							}
							else {
								resp.getWriter().println(getMessageinJson(msgjsonobj,"MMN id does not exist!"));

							}
						}	
						else {
							resp.getWriter().println(getMessageinJson(msgjsonobj,"MMN id does not found in approval!"));

						}
						
					}
					else {
						resp.getWriter().println(getMessageinJson(msgjsonobj,validationMsg));
					}
					
			}
			else if(actionTask.trim().equals("MIN")) {
				
				
				String jsonData=req.getParameter("data").trim();
				JSONArray dataArray = null;
				JSONObject jsonobj = null;

				try {
				logger.log(Level.SEVERE, "jsonData " + jsonData);
				JSONObject serviceJSONObject = new JSONObject(jsonData);
				
					dataArray = serviceJSONObject.getJSONArray("minData");
					
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
				
				String validationMsg ="";

				String strEmployeeId = req.getParameter("employeeId").trim();

				List<MaterialProduct> productlist = new ArrayList<MaterialProduct>();

				for(int m =0 ; m<dataArray.length() ; m++){
					
					try {
						jsonobj = dataArray.getJSONObject(m);
					} catch (JSONException e1) {
						e1.printStackTrace();
					}

					String warehouseName = "";
					String storageLocaton = "";
					String storageBin = "";
					
					try {
						warehouseName = jsonobj.optString("warehouseName");
						storageLocaton = jsonobj.optString("storageLocation");
						storageBin = jsonobj.optString("storageBin");
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					String strproductId = jsonobj.optString("productId");
					String strAvailableQty = "";
					double productAvailableQty = 0;
					try {
						strAvailableQty = jsonobj.optString("availableQty");
						productAvailableQty = Double.parseDouble(strAvailableQty);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					String strRequesteQty ="";
					double productConsumedQty = 0;
					
					try {
						strRequesteQty = jsonobj.optString("consumedQty");
						productConsumedQty = Double.parseDouble(strRequesteQty);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					if(warehouseName==null || warehouseName.equals("")) {
						validationMsg =" warehouseName can no be blank." ;
					}
					if(storageLocaton==null || storageLocaton.equals("")) {
						validationMsg +=" storage location can no be blank." ;
					}
					if(storageBin==null || storageBin.equals("")) {
						validationMsg +=" storage bin can no be blank." ;
					}
					if(strproductId==null || strproductId.equals("") || strproductId.equals("")) {
						validationMsg +=" product id can no be blank or zero." ;
					}
					if(productConsumedQty==0) {
						validationMsg += "Quantity can not be zero.";
					}
					if(productConsumedQty<0) {
						validationMsg += "Quantity can not be negative value.";
					}
					
					int productId = 0;
					if(strproductId!=null && !strproductId.equals("") && !strproductId.equals("0")) {
						productId = Integer.parseInt(strproductId);
					}
					
					ItemProduct itemproduct = ofy().load().type(ItemProduct.class).filter("companyId", companyId).filter("count", productId).first().now();

					if(itemproduct==null) {
						validationMsg += " Product id "+productId+" does not exist in ERP";

					}

						if(validationMsg.equals("")) {
							
							MaterialProduct materialProduct = new MaterialProduct();
							materialProduct.setMaterialProductId(itemproduct.getCount());
							materialProduct.setMaterialProductCode(itemproduct.getProductCode());
							materialProduct.setMaterialProductName(itemproduct.getProductName());
							materialProduct.setMaterialProductCategory(itemproduct.getProductCategory());
							materialProduct.setMaterialProductAvailableQuantity(productAvailableQty);
							materialProduct.setMaterialProductUOM(itemproduct.getUnitOfMeasurement());
							materialProduct.setMaterialProductRequiredQuantity(productConsumedQty);
							materialProduct.setMaterialProductPlannedQuantity(productConsumedQty);
							materialProduct.setMaterialProductWarehouse(warehouseName);
							materialProduct.setMaterialProductStorageLocation(storageLocaton);
							materialProduct.setMaterialProductStorageBin(storageBin);
							
							productlist.add(materialProduct);
							
						}

				}
					
					logger.log(Level.SEVERE, "validation msg "+validationMsg);
					logger.log(Level.SEVERE, "productlist size "+productlist.size());
					logger.log(Level.SEVERE, "condition"+(validationMsg.equals("") && productlist.size()>0));
					if(validationMsg.equals("") && productlist.size()>0) {
						
						int employeeId = Integer.parseInt(strEmployeeId);
						Employee employeeEntity = getemployeDetails(companyId, employeeId);
						
						String branchName = "";
						String salesPersonName = "";
						String employeeName = "";
						String issuedBy = "";
						String approverName = "";
						
						if(employeeEntity!=null) {
							branchName = employeeEntity.getBranchName();
							salesPersonName = employeeEntity.getFullname();
							employeeName = employeeEntity.getFullname();
							issuedBy = employeeEntity.getFullname();
							approverName = employeeEntity.getFullname();
							
							approverName = getApproverName(employeeEntity.getBranchName(),companyId);
							logger.log(Level.SEVERE, "approverName"+approverName);
							
						}

						MaterialIssueNote minEntity = new MaterialIssueNote();
						minEntity.setCompanyId(companyId);
						minEntity.setMinDate(DateUtility.getDateWithTimeZone("IST",(new Date())));
						minEntity.setBranch(branchName);
						minEntity.setMinSalesPerson(salesPersonName);		
						minEntity.setEmployee(employeeName);
						minEntity.setMinIssuedBy(issuedBy);
						minEntity.setApproverName(approverName);
						minEntity.setStatus(MaterialIssueNote.APPROVED);
						minEntity.setApprovalDate( DateUtility.getDateWithTimeZone("IST", (new Date())));
						
						minEntity.setSubProductTablemin(productlist);
						GenricServiceImpl impl = new GenricServiceImpl();
						ReturnFromServer server = new ReturnFromServer();
						server = impl.save(minEntity);
						logger.log(Level.SEVERE,"Min Saved sucessfully");
						InventoryManagementAppServlet inventoryapp = new InventoryManagementAppServlet();
						
						if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialIssueNote", "EnableApprovalProcessThroughTaskQueue", companyId)){
							Approvals model = inventoryapp.sendApprovalRequest(server.count,minEntity.getApproverName(),companyId,minEntity);
							if(model!=null){
								String taskName="ApprovalTaskQueueProcess"+"$"+model.getCompanyId()+"$"+model.getId()+"$"+model.getStatus();
								Queue queue = QueueFactory.getQueue("documentCancellation-queue");
								queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));
							}
						}
						else{
							Approvals model = inventoryapp.sendApprovalRequest(server.count,minEntity.getApproverName(),companyId,minEntity);

							minEntity.reactOnApproval();
						}
						
						resp.getWriter().println(getMessageinJson(msgjsonobj,"MIN Id :- "+server.count+" approved successfully!"));

						
					}
					else {
						resp.getWriter().println(getMessageinJson(msgjsonobj,validationMsg));

					}
					
//				String strEmployeeId = req.getParameter("employeeId").trim();
//				String validationMsg = "";
//				String warehouseName = "";
//				String storageLocaton = "";
//				String storageBin = "";
//				
//				try {
//					warehouseName = req.getParameter("warehouseName");
//					storageLocaton = req.getParameter("storageLocation");
//					storageBin = req.getParameter("storageBin");
//					
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				
//				String strproductId = req.getParameter("productId");
//				String strAvailableQty = "";
//				double productAvailableQty = 0;
//				try {
//					strAvailableQty = req.getParameter("availableQty");
//					productAvailableQty = Double.parseDouble(strAvailableQty);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				
//				String strRequesteQty ="";
//				double productConsumedQty = 0;
//				
//				try {
//					strRequesteQty = req.getParameter("consumedQty");
//					productConsumedQty = Double.parseDouble(strRequesteQty);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				
//				if(warehouseName==null || warehouseName.equals("")) {
//					validationMsg =" warehouseName can no be blank." ;
//				}
//				if(storageLocaton==null || storageLocaton.equals("")) {
//					validationMsg +=" storage location can no be blank." ;
//				}
//				if(storageBin==null || storageBin.equals("")) {
//					validationMsg +=" storage bin can no be blank." ;
//				}
//				if(strproductId==null || strproductId.equals("") || strproductId.equals("")) {
//					validationMsg +=" product id can no be blank or zero." ;
//				}
//				if(productConsumedQty==0) {
//					validationMsg += "Quantity can not be zero.";
//				}
//				if(productConsumedQty<0) {
//					validationMsg += "Quantity can not be negative value.";
//				}
//				
//				logger.log(Level.SEVERE, "validation msg "+validationMsg);
//				if(validationMsg.equals("")) {
//					int productid = 0;
//					if(strproductId!=null && !strproductId.equals("") || strproductId.equals("0")) {
//						productid = Integer.parseInt(strproductId);
//					}
//					int employeeId = Integer.parseInt(strEmployeeId);
//					Employee employeeEntity = getemployeDetails(companyId, employeeId);
//					
//					String msg = reactonMIN(companyId,warehouseName,storageLocaton,storageBin,employeeEntity.getBranchName(),employeeEntity.getFullname(),productid,productAvailableQty,productConsumedQty);
//					logger.log(Level.SEVERE, "msg "+msg);
//					resp.getWriter().println(msg);
//	
//				}
//				else {
//					resp.getWriter().println(validationMsg);
//				}
				
	
			}
			else if(actionTask.trim().equals("getAllTransaction")) {

				String validationMsg="";
				String fromDateStr="";
				try {
					fromDateStr = req.getParameter("fromtDate").trim();
				} catch (Exception e) {
					fromDateStr="";
				}
				String toDateStr="";
				try {
					toDateStr = req.getParameter("toDate").trim();
				} catch (Exception e) {
					toDateStr="";
				}
				
				if(fromDateStr.equals("")) {
					validationMsg +=" From date can not be blank.";
				}
				if(toDateStr.equals("")) {
					validationMsg +=" To date can not be blank.";
				}
				
				Date fromDate = null;
				try {
					fromDate = dateFormat.parse(fromDateStr);
				} catch (Exception e) {
					e.printStackTrace();
					validationMsg +=" From date should be dd/mm/yyyy format.";
				}
				
				Date toDate = null;
				try {
					toDate = dateFormat.parse(toDateStr);
				} catch (Exception e) {
					e.printStackTrace();
					validationMsg +=" To date should be dd/mm/yyyy format.";
				}
				
				String strEmployeeId="";
				try {
					strEmployeeId = req.getParameter("employeeId");
				} catch (Exception e) {
				}
				if(strEmployeeId==null || strEmployeeId.equals("")) {
					validationMsg += " Employee id can not be blank";
				}
				
				String userRole = "";
				try {
					userRole = req.getParameter("userRole");
				} catch (Exception e) {
				}
				
				if(userRole==null || userRole.equals("")) {
					validationMsg += " User role can not be blank";
				}
				
				String status="";
				try {
					status = req.getParameter("status");
				} catch (Exception e) {
				}
				
				if(status==null || status.equals("")) {
					validationMsg += " Status can not be blank";
				}
				
				ArrayList<String> statuslist = new ArrayList<String>();
				if(status.equals("Created/Requested")) {
					statuslist.add(MaterialIssueNote.CREATED);
					statuslist.add(MaterialIssueNote.REQUESTED);
				}
				else if(status.equals("Created")) {
					statuslist.add(MaterialIssueNote.CREATED);
				}
				else {
					statuslist.add(status);
				}
				logger.log(Level.SEVERE, "statuslist"+statuslist);
				logger.log(Level.SEVERE, "fromDate"+fromDate);
				logger.log(Level.SEVERE, "toDate"+toDate);

				Calendar cal=Calendar.getInstance();
				cal.setTime(toDate);
				cal.add(Calendar.DATE, +0);
				
				Date todatewithTime=null;
				
				try {
					todatewithTime=dateFormat.parse(dateFormat.format(cal.getTime()));
					cal.set(Calendar.HOUR_OF_DAY,23);
					cal.set(Calendar.MINUTE,59);
					cal.set(Calendar.SECOND,59);
					cal.set(Calendar.MILLISECOND,999);
					todatewithTime=cal.getTime();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				logger.log(Level.SEVERE,"Today after time set date"+todatewithTime);
				
				
				if(validationMsg.equals("")) {
//					Date lastThirtyDaysDate = getLastThirtyDaysDate();
//					logger.log(Level.SEVERE,"lastThirtyDaysDate"+lastThirtyDaysDate);

					int employeeId = Integer.parseInt(strEmployeeId);
					Employee employeeEntity = getemployeDetails(companyId, employeeId);
					
					String warehouseName = "";
					String storageLocaton = "";
					String storageBin = "";
					
					try {
						warehouseName = req.getParameter("warehouseName");
						storageLocaton = req.getParameter("storageLocation");
						storageBin = req.getParameter("storageBin");
						
					} catch (Exception e) {
					}
					
					List<MaterialRequestNote> mrnList = getMRNList(employeeEntity.getFullname(),companyId,fromDate,todatewithTime,warehouseName,storageLocaton,storageBin,userRole,statuslist);
					List<MaterialMovementNote> mmnList = getMMNList(employeeEntity.getFullname(),companyId,fromDate,todatewithTime,warehouseName,storageLocaton,storageBin,userRole,statuslist);
					List<MaterialIssueNote> minList = getMINList(employeeEntity.getFullname(),companyId,fromDate,todatewithTime,warehouseName,storageLocaton,storageBin,userRole,statuslist);

					if(mrnList.size()>0) {
						Comparator<MaterialRequestNote> listcomparator = new Comparator<MaterialRequestNote>() {
							public int compare(MaterialRequestNote s1, MaterialRequestNote s2) {
							
							Date date1 = s1.getMrnDate();
							Date date2 = s2.getMrnDate();
							
							return date2.compareTo(date1);
							}
						};
						Collections.sort(mrnList, listcomparator);
					}
					
					if(mmnList.size()>0) {
						Comparator<MaterialMovementNote> listcomparator = new Comparator<MaterialMovementNote>() {
							public int compare(MaterialMovementNote s1, MaterialMovementNote s2) {
							
							Date date1 = s1.getMmnDate();
							Date date2 = s2.getMmnDate();
							
							return date2.compareTo(date1);
							}
						};
						Collections.sort(mmnList, listcomparator);
					}
					
					if(minList.size()>0) {
						Comparator<MaterialIssueNote> listcomparator = new Comparator<MaterialIssueNote>() {
							public int compare(MaterialIssueNote s1, MaterialIssueNote s2) {
							
							Date date1 = s1.getMinDate();
							Date date2 = s2.getMinDate();
							
							return date2.compareTo(date1);
							}
						};
						Collections.sort(minList, listcomparator);
					}
					
					JSONObject jsonobj = new JSONObject();
					JSONArray jsonarray = new JSONArray();
					
					for(MaterialRequestNote mrnEntity : mrnList) {
//						JSONObject jobj = new JSONObject();
//						try {
//							jobj.put("documnetId", mrnEntity.getCount());
//							jobj.put("documentDate", dateFormat.format(mrnEntity.getMrnDate()));
//							jobj.put("status", mrnEntity.getStatus());
//							jobj.put("documentType", "MRN");
//							jobj.put("warehouse", getwarehouseName(mrnEntity.getSubProductTableMrn()));
							
							JSONArray materialdetails = new JSONArray();
							for(MaterialProduct materialproduct : mrnEntity.getSubProductTableMrn()) {
//								JSONObject obj = new JSONObject();
								
								JSONObject jobj = new JSONObject();

								try {
									
									jobj.put("documnetId", mrnEntity.getCount());
									jobj.put("documentDate", dateFormat.format(mrnEntity.getMrnDate()));
									jobj.put("status", mrnEntity.getStatus());
									jobj.put("documentType", "MRN");
									jobj.put("requestedBy", mrnEntity.getEmployee());

									jobj.put("warehouse", getwarehouseName(mrnEntity.getSubProductTableMrn()));
									
//									obj.put("productName", materialproduct.getMaterialProductName());
//									obj.put("productId", materialproduct.getMaterialProductId());
//									obj.put("availableQty", materialproduct.getMaterialProductAvailableQuantity());
//									obj.put("requestQty", materialproduct.getMaterialProductPlannedQuantity());
//									materialdetails.put(obj);
									
									jobj.put("productName", materialproduct.getMaterialProductName());
									jobj.put("productId", materialproduct.getMaterialProductId());
									jobj.put("availableQty", materialproduct.getMaterialProductAvailableQuantity());
									jobj.put("requestQty", materialproduct.getMaterialProductPlannedQuantity());
//									materialdetails.put(obj);
									materialdetails.put(jobj);

								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							
//							materialdetails.put(jobj);
							jsonarray.put(materialdetails);
							
//							jsonarray.put(jsonarray);
							
//						} catch (JSONException e) {
//							e.printStackTrace();
//						}
					}
					for(MaterialMovementNote mmnEntity : mmnList) {
//						JSONObject jobj = new JSONObject();
//						try {
//							jobj.put("documnetId", mmnEntity.getCount());
//							jobj.put("documentDate", dateFormat.format(mmnEntity.getMmnDate()));
//							jobj.put("status", mmnEntity.getStatus());
//							jobj.put("documentType", "MMN");
//							jobj.put("warehouse", getwarehouseName(mmnEntity.getSubProductTableMmn()));
							
							JSONArray materialdetails = new JSONArray();
							for(MaterialProduct materialproduct : mmnEntity.getSubProductTableMmn()) {
//								JSONObject obj = new JSONObject();
								JSONObject jobj = new JSONObject();

								try {
									String productwarehouseName =  materialproduct.getMaterialProductWarehouse()+" / "+materialproduct.getMaterialProductStorageLocation()+" / "+
											materialproduct.getMaterialProductStorageBin()+"";
									
									jobj.put("documnetId", mmnEntity.getCount());
									jobj.put("documentDate", dateFormat.format(mmnEntity.getMmnDate()));
									jobj.put("status", mmnEntity.getStatus());
									jobj.put("documentType", "MMN");
									jobj.put("requestedBy", mmnEntity.getEmployee());
									jobj.put("personResponsible", mmnEntity.getMmnPersonResposible()); // supervisor name

									jobj.put("warehouse", productwarehouseName);
									
//									obj.put("productName", materialproduct.getMaterialProductName());
//									obj.put("productId", materialproduct.getMaterialProductId());
//									obj.put("availableQty", materialproduct.getMaterialProductAvailableQuantity());
//									obj.put("requestQty", materialproduct.getMaterialProductRequiredQuantity());
//									materialdetails.put(obj);
									
									jobj.put("productName", materialproduct.getMaterialProductName());
									jobj.put("productId", materialproduct.getMaterialProductId());
									jobj.put("availableQty", materialproduct.getMaterialProductAvailableQuantity());
									jobj.put("requestQty", materialproduct.getMaterialProductRequiredQuantity());
									materialdetails.put(jobj);

								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							
//							materialdetails.put(jobj);
							jsonarray.put(materialdetails);
//							jsonarray.put(jobj);

//						} catch (JSONException e) {
//							e.printStackTrace();
//						}
					}
					for(MaterialIssueNote minEntity : minList) {
//						try {
							
							
							JSONArray materialdetails = new JSONArray();
							for(MaterialProduct materialproduct : minEntity.getSubProductTablemin()) {
								JSONObject jobj = new JSONObject();
								try {
									String productwarehouseName =  materialproduct.getMaterialProductWarehouse()+" / "+materialproduct.getMaterialProductStorageLocation()+" / "+
											materialproduct.getMaterialProductStorageBin()+"";
									
									jobj.put("documnetId", minEntity.getCount());
									jobj.put("documentDate", dateFormat.format(minEntity.getMinDate()));
									jobj.put("status", minEntity.getStatus());
									jobj.put("documentType", "MIN");
									jobj.put("requestedBy", minEntity.getEmployee());
									jobj.put("warehouse", productwarehouseName);

									jobj.put("productName", materialproduct.getMaterialProductName());
									jobj.put("productId", materialproduct.getMaterialProductId());
									jobj.put("availableQty", materialproduct.getMaterialProductAvailableQuantity());
									jobj.put("requestQty", materialproduct.getMaterialProductRequiredQuantity());
									materialdetails.put(jobj);
									
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							
//							materialdetails.put(jobj);
							jsonarray.put(materialdetails);
							
//							jsonarray.put(jobj);
//						} catch (JSONException e) {
//							e.printStackTrace();
//						}
					}
					
					try {
						jsonobj.put("data", jsonarray);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					String jsonstring = jsonobj.toString().replaceAll("\\\\", "");
					logger.log(Level.SEVERE, "Json"+jsonstring);
					resp.getWriter().println(jsonstring);
				}
				else {
					resp.getWriter().println(getMessageinJson(msgjsonobj,validationMsg));

				}
				
			}
			else {
				resp.getWriter().println(getMessageinJson(msgjsonobj,"Invalid action task"));

			}

			
		 }

	}

	

	private double getmaterialAvailableQty(List<ProductInventoryViewDetails> prodInventorylist,
			MaterialProduct materialproduct) {
		
			for(ProductInventoryViewDetails invnetorydetails : prodInventorylist) {
				if(materialproduct.getMaterialProductId() == invnetorydetails.getProdid() &&
						materialproduct.getMaterialProductWarehouse().trim().equals(invnetorydetails.getWarehousename().trim()) &&
						materialproduct.getMaterialProductStorageLocation().trim().equals(invnetorydetails.getStoragelocation().trim()) && 
						materialproduct.getMaterialProductStorageBin().trim().equals(invnetorydetails.getStoragebin().trim()) ) {
					
					return invnetorydetails.getAvailableqty();
				}
			}
		
		return 0;
	}

	private String getMessageinJson(JSONObject msgjsonobj, String msg) {
		
		try {
			msgjsonobj.put("message", msg);
		} catch (JSONException e) {
		}
		
		String jsonstring = msgjsonobj.toString().replaceAll("\\\\", "");
		
		return jsonstring;
		
	}

	private String MMNAppprovalProcess(MaterialMovementNote mmnEntity) {
		
		
		ArrayList<InventoryTransactionLineItem> itemList=new ArrayList<InventoryTransactionLineItem>();
		for(MaterialProduct product:mmnEntity.getSubProductTableMmn()){
			InventoryTransactionLineItem item=new InventoryTransactionLineItem();
			item.setCompanyId(mmnEntity.getCompanyId());
			item.setBranch(mmnEntity.getBranch());
			item.setDocumentType(AppConstants.MMN);
			item.setDocumentId(mmnEntity.getCount());
			item.setDocumentDate(mmnEntity.getCreationDate());
			item.setDocumnetTitle(mmnEntity.getMmnTitle());
			if(mmnEntity.getServiceId()>0)
				item.setServiceId(mmnEntity.getServiceId()+"");
			item.setProdId(product.getMaterialProductId());
			item.setUpdateQty(product.getMaterialProductRequiredQuantity());
			/*** Date 30-08-2019 by Vijay for Inventory Management Lock seal ***/
			item.setProductCode(product.getMaterialProductCode());
			item.setWarehouseName(product.getMaterialProductWarehouse());
			item.setStorageLocation(product.getMaterialProductStorageLocation());
			item.setStorageBin(product.getMaterialProductStorageBin());
			
			String transaction=mmnEntity.getTransDirection().trim();
			String operation = null;
			if(transaction.equals("IN")){
				 operation=AppConstants.ADD;
			}else if(transaction.equals("OUT")){
				operation=AppConstants.SUBTRACT;
			}else if(transaction.equals("TRANSFEROUT")){
				operation=AppConstants.SUBTRACT;
			}else if(transaction.equals("TRANSFERIN")){
				operation = AppConstants.ADD;
				item.setWarehouseName(mmnEntity.getTransToWareHouse());
				item.setStorageLocation(mmnEntity.getTransToStorLoc());
				item.setStorageBin(mmnEntity.getTransToStorBin());
				System.out.println("transfer in end");
			}
			item.setOperation(operation);
			
			/**
			 * Date : 01-03-2017 By ANIL
			 */
			item.setDocumentSubType(transaction);
			/*** Date 17-04-2019 by Vijay for Scrap Transaction Type must display as Scrap. it was showing as OUT ***/
			if(transaction.equals("OUT")){
				item.setDocumentSubType("Scrap");
			}
			
			/** Date 04-12-2019 by vijay for Inventory management lock seal Number ***/
			if(product.getLockSealSerialNo()!=null){
			item.setSerialNumber(product.getLockSealSerialNo());
			}
			
			
			itemList.add(item);
		
		}
		
		UpdateStock.setProductInventory(itemList); 
		 
		if(mmnEntity.getTransDirection().trim().equals("TRANSFEROUT")){
			mmnEntity.createNewMMNForSelectedWareHouse();
		}
		
		mmnEntity.accountingInterface();
		mmnEntity.setStatus(MaterialIssueNote.APPROVED);
		ofy().save().entity(mmnEntity);
		
		return "MMN approval approval process started";
		
	}

	private String getwarehouseName(List<MaterialProduct> productTable) {
		String warehouseStorageLocationBinName = "";
		for(MaterialProduct product : productTable) {
			warehouseStorageLocationBinName += product.getMaterialProductWarehouse()+" / "+product.getMaterialProductStorageLocation()+" / "+
												product.getMaterialProductStorageBin()+" ";
			break;
		}
		return warehouseStorageLocationBinName;
	}

	private List<MaterialIssueNote> getMINList(String employeeName, long companyId, Date fromDate, Date toDate, String warehouseName, String storageLocaton, String storageBin, String userRole, ArrayList<String> statuslist) {
		
		List<MaterialIssueNote> minEntitylist = null;
		logger.log(Level.SEVERE, "MIN userRole"+userRole);

		minEntitylist = ofy().load().type(MaterialIssueNote.class).filter("companyId", companyId).filter("minDate >=", fromDate).filter("minDate <=", toDate)
						.list(); //.filter("employee", employeeName)
		logger.log(Level.SEVERE, "minEntitylist size "+minEntitylist.size());

		if(userRole.equalsIgnoreCase(AppConstants.TECHNICIAN) || userRole.equalsIgnoreCase(AppConstants.OPERATOR) ) {
			ArrayList<MaterialIssueNote> updatedminlist = new ArrayList<MaterialIssueNote>();

			if(warehouseName!=null && !warehouseName.equals("") && storageLocaton!=null && !storageLocaton.equals("") && storageBin!=null && !storageBin.equals("")) {
				logger.log(Level.SEVERE, "warehouseName "+warehouseName +" Storagelocation "+storageLocaton+" storagebin"+storageBin);
			
				List<MaterialIssueNote> warehousewiseData = new ArrayList<MaterialIssueNote>();
				try {
//					minEntitylist = ofy().load().type(MaterialIssueNote.class).filter("companyId", companyId).filter("minDate >=", fromDate).filter("minDate <=", toDate)
//									.filter("employee", employeeName).list();
//					logger.log(Level.SEVERE, "minEntitylist size "+minEntitylist.size());
		
					if(minEntitylist!=null && minEntitylist.size()>0) {
						for(MaterialIssueNote minentity : minEntitylist) {
							
							boolean flag = getWarehouseWiseData(minentity.getSubProductTablemin(),warehouseName,storageLocaton,storageBin);
							if(flag) {
								warehousewiseData.add(minentity);
							}
						}
					}
					logger.log(Level.SEVERE, "warehouseWiseminEntitylist size "+warehousewiseData.size());
					
					if(warehousewiseData!=null) {
						for(MaterialIssueNote min : warehousewiseData) {
							if(min.getEmployee().equals(employeeName)) {
								updatedminlist.add(min);
							}
						}
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				return updatedminlist;

			}
			else {
//				minEntitylist = ofy().load().type(MaterialIssueNote.class).filter("companyId", companyId)
//						.filter("minDate >=", fromDate).filter("minDate <=", toDate).filter("employee", employeeName).list();
				
				if(minEntitylist!=null) {
					for(MaterialIssueNote min : minEntitylist) {
						if(min.getEmployee().equals(employeeName)) {
							updatedminlist.add(min);
						}
					}
				}
				return updatedminlist;

			}
			
		}
		else if(userRole.equalsIgnoreCase(AppConstants.SUPERVISOR)) {
			ArrayList<MaterialIssueNote> updatedminlist = new ArrayList<MaterialIssueNote>();

			if(warehouseName!=null && !warehouseName.equals("") && storageLocaton!=null && !storageLocaton.equals("") && storageBin!=null && !storageBin.equals("")) {
				logger.log(Level.SEVERE, "warehouseName "+warehouseName +" Storagelocation "+storageLocaton+" storagebin"+storageBin);

				List<MaterialIssueNote> warehousewiseData = new ArrayList<MaterialIssueNote>();
				try {
					
//					minEntitylist = ofy().load().type(MaterialIssueNote.class).filter("companyId", companyId).filter("minDate >=", fromDate).filter("minDate <=", toDate)
//							.filter("approverName", employeeName).list();
					logger.log(Level.SEVERE, "minEntitylist size "+minEntitylist.size());
			
					if(minEntitylist!=null && minEntitylist.size()>0) {
						for(MaterialIssueNote minentity : minEntitylist) {
							
							boolean flag = getWarehouseWiseData(minentity.getSubProductTablemin(),warehouseName,storageLocaton,storageBin);
							if(flag) {
								warehousewiseData.add(minentity);
							}
						}
					}
					logger.log(Level.SEVERE, "warehouseWiseminEntitylist size "+warehousewiseData.size());
					if(warehousewiseData!=null) {
						for(MaterialIssueNote min : warehousewiseData) {
							if(min.getApproverName().equals(employeeName)) {
								updatedminlist.add(min);
							}
						}
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				return updatedminlist;

			}
			else {
//				minEntitylist = ofy().load().type(MaterialIssueNote.class).filter("companyId", companyId)
//						.filter("minDate >=", fromDate).filter("minDate <=", toDate).filter("approverName", employeeName).list();
				
				if(minEntitylist!=null) {
					for(MaterialIssueNote min : minEntitylist) {
						if(min.getApproverName().equals(employeeName)) {
							updatedminlist.add(min);
						}
					}
				}
				return updatedminlist;
			}
			
		}

		
		
		return minEntitylist;
	}

	private boolean getWarehouseWiseData(List<MaterialProduct> materialProductlist,
			String warehouseName, String storageLocaton, String storageBin) {


		for(MaterialProduct materialprod : materialProductlist) {

			if(materialprod.getMaterialProductWarehouse().trim().equals(warehouseName.trim()) && materialprod.getMaterialProductStorageLocation().trim()
					.equals(storageLocaton.trim()) && materialprod.getMaterialProductStorageBin().trim().equals(storageBin.trim())) {
				
				return true;

			}
		}
		
		return false;
	}

	private List<MaterialMovementNote> getMMNList(String employeeName, long companyId, Date fromDate, Date toDate, String warehouseName, String storageLocaton, String storageBin, String userRole, ArrayList<String> statuslist) {
		List<MaterialMovementNote> mmnEntitylist = null;
		logger.log(Level.SEVERE, "MMN userRole"+userRole);
		logger.log(Level.SEVERE, "employeeName"+employeeName);

		mmnEntitylist = ofy().load().type(MaterialMovementNote.class).filter("companyId", companyId).filter("mmnDate >=", fromDate).filter("mmnDate <=", toDate)
		 		.filter("status IN", statuslist).list(); 
		logger.log(Level.SEVERE, "mmnEntitylist size "+mmnEntitylist.size());
		
		if(userRole.equalsIgnoreCase(AppConstants.TECHNICIAN) || userRole.equalsIgnoreCase(AppConstants.OPERATOR) ) {
			ArrayList<MaterialMovementNote> updatedmmnlist = new ArrayList<MaterialMovementNote>();

			if(warehouseName!=null && !warehouseName.equals("") && storageLocaton!=null && !storageLocaton.equals("") && storageBin!=null && !storageBin.equals("")) {
				logger.log(Level.SEVERE, "warehouseName "+warehouseName +" Storagelocation "+storageLocaton+" storagebin"+storageBin);
				List<MaterialMovementNote> warehousewiseData = new ArrayList<MaterialMovementNote>();
				try {
					
//					mmnEntitylist = ofy().load().type(MaterialMovementNote.class).filter("companyId", companyId).filter("mmnDate >=", fromDate).filter("mmnDate <=", toDate)
//					 		.filter("status IN", statuslist).list(); //.filter("employee", employeeName)
//					logger.log(Level.SEVERE, "mmnEntitylist size "+mmnEntitylist.size());
					
					if(mmnEntitylist!=null && mmnEntitylist.size()>0) {
						for(MaterialMovementNote mmnentity : mmnEntitylist) {
							boolean flag = getWarehouseWiseData(mmnentity.getSubProductTableMmn(),warehouseName,storageLocaton,storageBin);
							if(flag) {
								warehousewiseData.add(mmnentity);
							}
						}
						
						logger.log(Level.SEVERE, "warehousewiseData size "+warehousewiseData.size());
						if(warehousewiseData!=null) {
							for(MaterialMovementNote mmn : warehousewiseData) {
								if(mmn.getEmployee().equals(employeeName)) {
									updatedmmnlist.add(mmn);
								}
							}
						}
					}
			
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			else {
//				 mmnEntitylist = ofy().load().type(MaterialMovementNote.class).filter("companyId", companyId)
//						 		.filter("mmnDate >=", fromDate).filter("mmnDate <=", toDate).filter("status IN", statuslist).list(); //.filter("employee", employeeName)
//				logger.log(Level.SEVERE, "mmnEntitylist size "+mmnEntitylist.size());
				
				if(mmnEntitylist!=null) {
					for(MaterialMovementNote mmn : mmnEntitylist) {
						if(mmn.getEmployee().equals(employeeName)) {
							updatedmmnlist.add(mmn);
						}
					}
				}

			}
			
//			List<MaterialMovementNote> mmnEntitylist2 = null;

			if(warehouseName!=null && !warehouseName.equals("") && storageLocaton!=null && !storageLocaton.equals("") && storageBin!=null && !storageBin.equals("")) {
				logger.log(Level.SEVERE, "warehouseName "+warehouseName +" Storagelocation "+storageLocaton+" storagebin"+storageBin);
				List<MaterialMovementNote> warehousewiseData = new ArrayList<MaterialMovementNote>();

				try {
//					mmnEntitylist2 = ofy().load().type(MaterialMovementNote.class).filter("companyId", companyId).filter("mmnDate >=", fromDate).filter("mmnDate <=", toDate)
//							.filter("status IN", statuslist).list(); //.filter("approverName", employeeName)
//					logger.log(Level.SEVERE, "mmnEntitylist size "+mmnEntitylist.size());
					
					if(mmnEntitylist!=null && mmnEntitylist.size()>0) {
						for(MaterialMovementNote mmnentity : mmnEntitylist) {
//							boolean flag = getWarehouseWiseData(mmnentity.getSubProductTableMmn(),warehouseName,storageLocaton,storageBin);
//							if(flag) {
//								warehousewiseData.add(mmnentity);
//							}
							if(mmnentity.getApproverName().equals(employeeName)) {
								updatedmmnlist.add(mmnentity);
							}
						}
						logger.log(Level.SEVERE, "warehousewiseData size "+warehousewiseData.size());
						if(warehousewiseData!=null) {
							for(MaterialMovementNote mmn : warehousewiseData) {
								if(mmn.getApproverName().equals(employeeName)) {
									updatedmmnlist.add(mmn);
								}
							}
						}
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			else {
//				mmnEntitylist2 = ofy().load().type(MaterialMovementNote.class).filter("companyId", companyId)
//						 		.filter("mmnDate >=", fromDate).filter("mmnDate <=", toDate).filter("status IN", statuslist).list(); //.filter("approverName", employeeName)
//				logger.log(Level.SEVERE, "mmnEntitylist size "+mmnEntitylist.size());

				if(mmnEntitylist!=null) {
					for(MaterialMovementNote mmn : mmnEntitylist) {
						if(mmn.getApproverName().equals(employeeName)) {
							updatedmmnlist.add(mmn);
						}
					}
				}
			}
			
			return updatedmmnlist;
		}
		else if(userRole.equalsIgnoreCase(AppConstants.SUPERVISOR)) {
			ArrayList<MaterialMovementNote> updatedmmnlist = new ArrayList<MaterialMovementNote>();

			if(warehouseName!=null && !warehouseName.equals("") && storageLocaton!=null && !storageLocaton.equals("") && storageBin!=null && !storageBin.equals("")) {
				logger.log(Level.SEVERE, "warehouseName "+warehouseName +" Storagelocation "+storageLocaton+" storagebin"+storageBin);
				List<MaterialMovementNote> warehousewiseData = new ArrayList<MaterialMovementNote>();
					
				try {
//					mmnEntitylist = ofy().load().type(MaterialMovementNote.class).filter("companyId", companyId).filter("mmnDate >=", fromDate).filter("mmnDate <=", toDate)
//							.filter("status IN", statuslist).list();  //.filter("mmnPersonResposible", employeeName)
//					logger.log(Level.SEVERE, "mmnEntitylist size "+mmnEntitylist.size());
			
					if(mmnEntitylist!=null && mmnEntitylist.size()>0) {
						for(MaterialMovementNote mmnentity : mmnEntitylist) {
							boolean flag = getWarehouseWiseData(mmnentity.getSubProductTableMmn(),warehouseName,storageLocaton,storageBin);
							if(flag) {
								warehousewiseData.add(mmnentity);
							}
						}
						logger.log(Level.SEVERE, "warehousewiseData size "+warehousewiseData.size());
						if(warehousewiseData!=null) {
							for(MaterialMovementNote mmn : warehousewiseData) {
								if(mmn.getMmnPersonResposible().equals(employeeName)) {
									updatedmmnlist.add(mmn);
								}
							}
						}
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				return updatedmmnlist;
			}
			else {
//				 mmnEntitylist = ofy().load().type(MaterialMovementNote.class).filter("companyId", companyId)
//						 		.filter("mmnDate >=", fromDate).filter("mmnDate <=", toDate).filter("status IN", statuslist).list(); //.filter("mmnPersonResposible", employeeName)
//				logger.log(Level.SEVERE, "mmnEntitylist size "+mmnEntitylist.size());
				if(mmnEntitylist!=null) {
					for(MaterialMovementNote mmn : mmnEntitylist) {
						if(mmn.getMmnPersonResposible().equals(employeeName)) {
							updatedmmnlist.add(mmn);
						}
					}
				}
				
				return updatedmmnlist;
			}
		}
		else if(userRole.equalsIgnoreCase(AppConstants.STOREKEEPER)) {
			ArrayList<MaterialMovementNote> updatedmmnlist = new ArrayList<MaterialMovementNote>();

//			if(warehouseName!=null && !warehouseName.equals("") && storageLocaton!=null && !storageLocaton.equals("") && storageBin!=null && !storageBin.equals("")) {
//				logger.log(Level.SEVERE, "warehouseName "+warehouseName +" Storagelocation "+storageLocaton+" storagebin"+storageBin);
//				List<MaterialMovementNote> warehousewiseData = new ArrayList<MaterialMovementNote>();
//				try {
//			
//					if(mmnEntitylist!=null && mmnEntitylist.size()>0) {
//						for(MaterialMovementNote mmnentity : mmnEntitylist) {
////							boolean flag = getWarehouseWiseData(mmnentity.getSubProductTableMmn(),warehouseName,storageLocaton,storageBin);
//							if(mmnentity.getTransToWareHouse().trim().equals(warehouseName.trim()) && mmnentity.getTransToStorBin().trim().equals(storageLocaton.trim())
//									&& mmnentity.getTransToStorBin().trim().equals(anObject)) {
//								warehousewiseData.add(mmnentity);
//							}
//						}
//						logger.log(Level.SEVERE, "warehousewiseData size "+warehousewiseData.size());
//						if(warehousewiseData!=null) {
//							for(MaterialMovementNote mmn : warehousewiseData) {
//								if(mmn.getApproverName().equals(employeeName)) {
//									updatedmmnlist.add(mmn);
//								}
//							}
//						}
//					}
//					
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				logger.log(Level.SEVERE, "updatedmmnlist size "+updatedmmnlist.size());
//
//				return updatedmmnlist;
//			}
//			else {
////				 mmnEntitylist = ofy().load().type(MaterialMovementNote.class).filter("companyId", companyId)
////						 		.filter("mmnDate >=", fromDate).filter("mmnDate <=", toDate).filter("status IN", statuslist).list(); //.filter("employee", employeeName)
////				logger.log(Level.SEVERE, "mmnEntitylist size "+mmnEntitylist.size());
//				
//				if(mmnEntitylist!=null) {
//					for(MaterialMovementNote mmn : mmnEntitylist) {
//						if(mmn.getApproverName().equals(employeeName)) {
//							updatedmmnlist.add(mmn);
//						}
//					}
//				}
//				logger.log(Level.SEVERE, "updatedmmnlist size "+updatedmmnlist.size());
//
//				return updatedmmnlist;
//
//			}
			
			
			if(mmnEntitylist!=null) {
				for(MaterialMovementNote mmn : mmnEntitylist) {
					if(mmn.getApproverName().equals(employeeName)) {
						updatedmmnlist.add(mmn);
					}
				}
			}
			logger.log(Level.SEVERE, "updatedmmnlist size "+updatedmmnlist.size());

			
		}

		
		
		return mmnEntitylist;
	}

	private List<MaterialRequestNote> getMRNList(String employeeName, long companyId, Date fromDate, Date toDate, String warehouseName, String storageLocaton, String storageBin, String userRole, ArrayList<String> statuslist) {
		logger.log(Level.SEVERE, "MRN userRole"+userRole);

		List<MaterialRequestNote> mrnEntitylist = ofy().load().type(MaterialRequestNote.class).filter("companyId", companyId)
				.filter("mrnDate >=",fromDate).filter("mrnDate <=",toDate).filter("status IN", statuslist).list();
		logger.log(Level.SEVERE, "mrnEntitylist size "+mrnEntitylist.size()); 
	
			
		if(userRole.equalsIgnoreCase(AppConstants.TECHNICIAN) || userRole.equalsIgnoreCase(AppConstants.OPERATOR)) {
			ArrayList<MaterialRequestNote> updatedmrnlist = new ArrayList<MaterialRequestNote>();

			if(warehouseName!=null && !warehouseName.equals("") && storageLocaton!=null && !storageLocaton.equals("") && storageBin!=null && !storageBin.equals("")) {
				logger.log(Level.SEVERE, "warehouseName "+warehouseName +" Storagelocation "+storageLocaton+" storagebin"+storageBin);
				List<MaterialRequestNote> warehousewiseData = new ArrayList<MaterialRequestNote>();

				try {
//					mrnEntitylist = ofy().load().type(MaterialRequestNote.class).filter("companyId", companyId)
//							.filter("mrnDate >=", fromDate).filter("mrnDate <=", toDate).filter("status IN", statuslist).list();
//					logger.log(Level.SEVERE, "mrnEntitylist size "+mrnEntitylist.size()); // .filter("employee", employeeName)
					
					if(mrnEntitylist!=null && mrnEntitylist.size()>0) {
						for(MaterialRequestNote mrnentity : mrnEntitylist) {
							logger.log(Level.SEVERE, "mrnEntitylist count "+mrnentity.getCount()); 

							boolean flag = getWarehouseWiseData(mrnentity.getSubProductTableMrn(),warehouseName,storageLocaton,storageBin);
							if(flag) {
								warehousewiseData.add(mrnentity);
							}
						}
						logger.log(Level.SEVERE, "warehousewiseData size "+warehousewiseData.size());
						if(warehousewiseData!=null) {
							for(MaterialRequestNote mrn : warehousewiseData) {
								if(mrn.getEmployee().equals(employeeName)) {
									updatedmrnlist.add(mrn);
								}
							}
							return updatedmrnlist;
						}
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
//				return warehousewiseData;
			}
			else {
//				 mrnEntitylist = ofy().load().type(MaterialRequestNote.class).filter("companyId", companyId)
//						.filter("mrnDate >=", fromDate).filter("mrnDate <=", toDate).filter("status IN", statuslist).list(); //.filter("employee", employeeName)
//					logger.log(Level.SEVERE, "mrnEntitylist size "+mrnEntitylist.size());
					if(mrnEntitylist!=null) {
						for(MaterialRequestNote mrn : mrnEntitylist) {
							if(mrn.getEmployee().equals(employeeName)) {
								updatedmrnlist.add(mrn);
							}
						}
						return updatedmrnlist;
					}
					
			}
		}
		else if(userRole.equalsIgnoreCase(AppConstants.SUPERVISOR) || userRole.equalsIgnoreCase(AppConstants.STOREKEEPER)) {
			ArrayList<MaterialRequestNote> updatedmrnlist = new ArrayList<MaterialRequestNote>();

			if(warehouseName!=null && !warehouseName.equals("") && storageLocaton!=null && !storageLocaton.equals("") && storageBin!=null && !storageBin.equals("")) {
				logger.log(Level.SEVERE, "warehouseName "+warehouseName +" Storagelocation "+storageLocaton+" storagebin"+storageBin);
				List<MaterialRequestNote> warehousewiseData = new ArrayList<MaterialRequestNote>();
				try {
//					mrnEntitylist = ofy().load().type(MaterialRequestNote.class).filter("companyId", companyId)
//							.filter("mrnDate >=", fromDate).filter("mrnDate <=", toDate).filter("status IN", statuslist).list(); //.filter("approverName", employeeName)
//					logger.log(Level.SEVERE, "mrnEntitylist size "+mrnEntitylist.size());
					
					if(mrnEntitylist!=null && mrnEntitylist.size()>0) {
						for(MaterialRequestNote mrnentity : mrnEntitylist) {
							boolean flag = getWarehouseWiseData(mrnentity.getSubProductTableMrn(),warehouseName,storageLocaton,storageBin);
							if(flag) {
								warehousewiseData.add(mrnentity);
							}
						}
						logger.log(Level.SEVERE, "warehousewiseData size "+warehousewiseData.size());
						if(warehousewiseData!=null) {
							for(MaterialRequestNote mrn : warehousewiseData) {
								if(mrn.getApproverName().equals(employeeName)) {
									updatedmrnlist.add(mrn);
								}
							}
							return updatedmrnlist;
						}
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
//				return warehousewiseData;
			}
			else {
//				 mrnEntitylist = ofy().load().type(MaterialRequestNote.class).filter("companyId", companyId)
//						.filter("mrnDate >=", fromDate).filter("mrnDate <=", toDate).filter("status IN", statuslist).list(); //.filter("approverName", employeeName)
//					logger.log(Level.SEVERE, "mrnEntitylist size "+mrnEntitylist.size());
					if(mrnEntitylist!=null) {
						for(MaterialRequestNote mrn : mrnEntitylist) {
							if(mrn.getApproverName().equals(employeeName)) {
								updatedmrnlist.add(mrn);
							}
						}
						return updatedmrnlist;
					}
			}
			
		}
		


		
		return mrnEntitylist;
	}

	private Date getLastThirtyDaysDate() {
		Date today=DateUtility.getDateWithTimeZone("IST", new Date());

		Calendar cal=Calendar.getInstance();
		cal.setTime(today);
		cal.add(Calendar.DATE, -30);
		
		Date minusThirtyDaysDate=null;
		
		try {
			minusThirtyDaysDate=dateFormat.parse(dateFormat.format(cal.getTime()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"Today Date -30 Days Date"+minusThirtyDaysDate);
		
		return minusThirtyDaysDate;
	}

	private String reactonMIN(long companyId, String warehouseName, String storageLocaton, String storageBin, String branchName, String technicinaName, int productId, double productAvailableQty, double productRequiredQty) {


		MaterialIssueNote minEntity = new MaterialIssueNote();
		minEntity.setCompanyId(companyId);
		minEntity.setMinDate(DateUtility.getDateWithTimeZone("IST",(new Date())));
		minEntity.setBranch(branchName);
		minEntity.setMinSalesPerson(technicinaName);		
		minEntity.setEmployee(technicinaName);
		minEntity.setMinIssuedBy(technicinaName);
		minEntity.setApproverName(technicinaName);
		minEntity.setStatus(MaterialIssueNote.APPROVED);
		minEntity.setApprovalDate( DateUtility.getDateWithTimeZone("IST", (new Date())));
		SuperProduct itemproduct = getproductDetails(productId,companyId);
		ArrayList<MaterialProduct> productlist = new ArrayList<MaterialProduct>();
		MaterialProduct materialProduct = new MaterialProduct();
		materialProduct.setMaterialProductId(itemproduct.getCount());
		materialProduct.setMaterialProductCode(itemproduct.getProductCode());
		materialProduct.setMaterialProductName(itemproduct.getProductName());
		materialProduct.setMaterialProductUOM(itemproduct.getUnitOfMeasurement());
		materialProduct.setMaterialProductRequiredQuantity(productRequiredQty);
		materialProduct.setMaterialProductWarehouse(warehouseName);
		materialProduct.setMaterialProductStorageLocation(storageLocaton);
		materialProduct.setMaterialProductStorageBin(storageBin);
		
		productlist.add(materialProduct);
		minEntity.setSubProductTablemin(productlist);
		GenricServiceImpl impl = new GenricServiceImpl();
		ReturnFromServer server = new ReturnFromServer();
		server = impl.save(minEntity);
		logger.log(Level.SEVERE,"Min Saved sucessfully");
		InventoryManagementAppServlet inventoryapp = new InventoryManagementAppServlet();
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialIssueNote", "EnableApprovalProcessThroughTaskQueue", companyId)){
			Approvals model = inventoryapp.sendApprovalRequest(server.count,technicinaName,companyId,minEntity);
			if(model!=null){
				String taskName="ApprovalTaskQueueProcess"+"$"+model.getCompanyId()+"$"+model.getId()+"$"+model.getStatus();
				Queue queue = QueueFactory.getQueue("documentCancellation-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));
			}
		}
		else{
			Approvals model = inventoryapp.sendApprovalRequest(server.count,technicinaName,companyId,minEntity);

			minEntity.reactOnApproval();
		}
		return "MIN Id :-"+server.count+" approved successfully!";
		
		
	
	}

	private ItemProduct getproductDetails(int productId, long companyId) {

		ItemProduct product = ofy().load().type(ItemProduct.class).filter("companyId", companyId).filter("count", productId).first().now();
		
		return product;
	}

	private String getApproverName(String bankBranch, long companyId) {
		
		Branch branchEntity  =ofy().load().type(Branch.class).filter("companyId", companyId).filter("buisnessUnitName", bankBranch).first().now();
		if(branchEntity!=null && branchEntity.getPocName()!=null && !branchEntity.getPocName().equals("")) {
			return branchEntity.getPocName();
		}
		else {
			Company companyEntity = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
			if(companyEntity!=null && companyEntity.getPocName()!=null && !companyEntity.getPocName().equals("")) {
				return companyEntity.getPocName();
			}
		}
 		return "";
	}

	private String getEmployeeWarehouses(String strEmployeeId, long companyId) {
		try {
		int employeeId = Integer.parseInt(strEmployeeId);
		Employee employeeEntity = getemployeDetails(companyId, employeeId);
		List<TechnicianWareHouseDetailsList> technicianWarehouselist = ofy().load().type(TechnicianWareHouseDetailsList.class).filter("companyId", companyId)
							.filter("wareHouseList.technicianName", employeeEntity.getFullname()).list();
		logger.log(Level.SEVERE, "technicianWarehouselist size "+technicianWarehouselist.size());
		if(technicianWarehouselist.size()>0) {
			JSONObject jsonobj = new JSONObject();
			JSONArray jsonarray = new JSONArray();

			for(TechnicianWareHouseDetailsList technicianWarehouse : technicianWarehouselist) {
				for(TechnicianWarehouseDetails technician : technicianWarehouse.getWareHouseList()) {
					if(employeeEntity.getFullname().trim().equals(technician.getTechnicianName())) {
						JSONObject obj = new JSONObject();
						try {
							obj.put("parentWarehouse", technician.getParentWareHouse());
							obj.put("parentStorageLocation", technician.getParentStorageLocation());
							obj.put("parentStorageBin", technician.getPearentStorageBin());
							
							obj.put("warehouseName", technician.getTechnicianWareHouse());
							obj.put("storageLocation", technician.getTechnicianStorageLocation());
							obj.put("storageBin", technician.getTechnicianStorageBin());
							jsonarray.put(obj);
							
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}

				}

			}

			try {
				jsonobj.put("warehouseDetails", jsonarray);
				
				String jsonstring = jsonobj.toString().replaceAll("\\\\", "");
				logger.log(Level.SEVERE, "warehouseDetails Json"+jsonstring);
				return jsonstring;
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		JSONObject msgjsonobj = new JSONObject();
		return getMessageinJson(msgjsonobj,"Technician warehouse does not exist in technician warehouse details");
	}

	private Employee getemployeDetails(long companyId, int employeeId) {
		Employee employeeEntity = ofy().load().type(Employee.class).filter("companyId", companyId).filter("count", employeeId).first().now();
		return employeeEntity;
	}
	
	private String createMMNOnBasisOfMRN(MaterialRequestNote mrnEntity) {
		logger.log(Level.SEVERE, "inside create MMN");

		MaterialMovementNote mmn = new MaterialMovementNote();
		mmn.setCompanyId(mrnEntity.getCompanyId());
		mmn.setStatus(MaterialMovementNote.REQUESTED);
		mmn.setCreationDate(new Date());
		mmn.setMmnDate(new Date());
		mmn.setCompanyId(mrnEntity.getCompanyId());
		mmn.setMmnTransactionType("TransferOUT");
		mmn.setTransDirection("TRANSFEROUT");
		
		WareHouse warehouse = getParentWarehouse(mrnEntity);
		logger.log(Level.SEVERE, "parent warehouse name "+warehouse.getBusinessUnitName());

		ProductInventoryViewDetails productinventoryView = null;
		if (warehouse != null) {
			if(warehouse.getBranchdetails().size()!=0)
				mmn.setBranch(warehouse.getBranchdetails().get(0).getBranchName());
			
				productinventoryView = ofy().load().type(ProductInventoryViewDetails.class).filter("warehousename", warehouse.getBusinessUnitName())
						.filter("prodid", mrnEntity.getSubProductTableMrn().get(0).getMaterialProductId())
						.filter("companyId", mrnEntity.getCompanyId()).first().now();
		}
		
		ArrayList<MaterialProduct> materiallist = new ArrayList<MaterialProduct>();

		if(mrnEntity.getSubProductTableMrn().size()!=0){
		mmn.setTransToWareHouse(mrnEntity.getSubProductTableMrn().get(0).getMaterialProductWarehouse());
		mmn.setTransToStorLoc(mrnEntity.getSubProductTableMrn().get(0).getMaterialProductStorageLocation());
		mmn.setTransToStorBin(mrnEntity.getSubProductTableMrn().get(0).getMaterialProductStorageBin());
		mmn.setMmnMrnId(mrnEntity.getCount());
		for(MaterialProduct materialProdMRN : mrnEntity.getSubProductTableMrn()){
			MaterialProduct materialProd = new MaterialProduct();
			materialProd.setMaterialProductId(materialProdMRN.getMaterialProductId());
			materialProd.setMaterialProductCode(materialProdMRN.getMaterialProductCode());
			materialProd.setMaterialProductName(materialProdMRN.getMaterialProductName());
			logger.log(Level.SEVERE, "MRN material qty "+materialProdMRN.getMaterialProductRequiredQuantity());
			materialProd.setMaterialProductRequiredQuantity(materialProdMRN.getMaterialProductRequiredQuantity());
			materialProd.setMaterialProductPlannedQuantity(materialProdMRN.getMaterialProductRequiredQuantity());
			materialProd.setMaterialProductRemarks(materialProdMRN.getMaterialProductRemarks());
			materialProd.setMaterialProductUOM(materialProdMRN.getMaterialProductUOM());
			materialProd.setMaterialProductBalanceQty(materialProdMRN.getMaterialProductRequiredQuantity());
			if(productinventoryView!=null){
				materialProd.setMaterialProductWarehouse(productinventoryView.getWarehousename());
				materialProd.setMaterialProductStorageLocation(productinventoryView.getStoragelocation());
				materialProd.setMaterialProductStorageBin(productinventoryView.getStoragebin());
				materialProd.setMaterialProductAvailableQuantity(productinventoryView.getAvailableqty());
			}
			materiallist.add(materialProd);
		}
		}
		mmn.setSubProductTableMmn(materiallist);
		mmn.setEmployee(mrnEntity.getEmployee());
		String approvarName = getApprovarName(warehouse,mrnEntity.getCompanyId());
		logger.log(Level.SEVERE, "approvar Name"+approvarName);
		mmn.setApproverName(approvarName);
		mmn.setMmnPersonResposible(mrnEntity.getApproverName());

		GenricServiceImpl genimpl = new GenricServiceImpl();
		ReturnFromServer server = genimpl.save(mmn);
	
		String mmnoutId = server.count+"";
		
		ServerAppUtility serverUtility = new ServerAppUtility();
		String msg = serverUtility.sendApprovalRequest(mmn.getBranch(),server.count, mmn.getCompanyId(), mmn.getApproverName(),mmn.getEmployee(), mmn.getEmployee(), "MMN");
		
		logger.log(Level.SEVERE, "Approval request msg "+msg);

		return "MMN id :-"+mmnoutId+" created successfully and sent approval to "+mmn.getApproverName();
	}

	

	private WareHouse getParentWarehouse(MaterialRequestNote mrnEntity) {

		List<TechnicianWareHouseDetailsList> technicianWarehouselist = ofy().load().type(TechnicianWareHouseDetailsList.class).filter("companyId", mrnEntity.getCompanyId())
				.filter("wareHouseList.technicianName", mrnEntity.getEmployee()).list();
		logger.log(Level.SEVERE, "technicianWarehouselist size "+technicianWarehouselist.size());
		if(technicianWarehouselist.size()>0) {
		String technicianWarehouseName = mrnEntity.getSubProductTableMrn().get(0).getMaterialProductWarehouse().trim();
		String technicianStorageLocation = mrnEntity.getSubProductTableMrn().get(0).getMaterialProductStorageLocation().trim();
		String technicianStorageBin = mrnEntity.getSubProductTableMrn().get(0).getMaterialProductStorageBin().trim();
		logger.log(Level.SEVERE, "technicianWarehouseName"+technicianWarehouseName);
		logger.log(Level.SEVERE, "technicianWarehouseName"+technicianStorageLocation);
		logger.log(Level.SEVERE, "technicianWarehouseName"+technicianStorageBin);

		for(TechnicianWareHouseDetailsList technicianWarehouse : technicianWarehouselist) {
			for(TechnicianWarehouseDetails technician : technicianWarehouse.getWareHouseList()) {
				logger.log(Level.SEVERE, "technician.getTechnicianWareHouse().trim()"+technician.getTechnicianWareHouse().trim());
				logger.log(Level.SEVERE, "technician.getTechnicianStorageLocation().trim()"+technician.getTechnicianStorageLocation().trim());
				logger.log(Level.SEVERE, "technician.getPearentStorageBin().trim()"+technician.getTechnicianStorageBin().trim());

				if(technician.getTechnicianWareHouse().trim().equals(technicianWarehouseName) && technician.getTechnicianStorageLocation().trim().equals(technicianStorageLocation)
						&& technician.getTechnicianStorageBin().trim().equals(technicianStorageBin)) {
					
					WareHouse parentWarehouse = ofy().load().type(WareHouse.class).filter("companyId", mrnEntity.getCompanyId())
							.filter("buisnessUnitName", technician.getParentWareHouse()).first().now();
					logger.log(Level.SEVERE, "parentWarehouse "+parentWarehouse);
					return parentWarehouse;
				}
		
			}

		}
		}
		return null;
	}
	// for MMN
	private String getApprovarName(WareHouse warehouse, Long companyId) {
		ServerAppUtility utility = new ServerAppUtility();
		Company comp = null;
		if(warehouse!=null) {
			if(warehouse.getPocName()!=null && !warehouse.getPocName().equals("")) {
				logger.log(Level.SEVERE, "warehouse.getPocName() "+warehouse.getPocName());
				return warehouse.getPocName();
			}
			else if(warehouse.getBranchdetails()!=null && warehouse.getBranchdetails().size()>0) {
				Branch branchEntity = ofy().load().type(Branch.class).filter("buisnessUnitName", warehouse.getBranchdetails().get(0).getBranchName()).filter("companyId", companyId).first().now();
				if(branchEntity!=null && branchEntity.getPocName()!=null && !branchEntity.getPocName().equals("")) {
					logger.log(Level.SEVERE, "branchEntity.getPocName()"+branchEntity.getPocName());
					logger.log(Level.SEVERE, "branchEntity.getBusinessUnitName()"+branchEntity.getBusinessUnitName());
					return branchEntity.getPocName();
				}
				else {
					logger.log(Level.SEVERE, "for company poc name ");
					comp = utility.loadCompany(companyId);
					if(comp!=null && comp.getPocName()!=null && !comp.getPocName().equals("")) {
						logger.log(Level.SEVERE, "comp.getPocName() "+comp.getPocName());
						return comp.getPocName();
					}
				}
										
			}
			else {
				logger.log(Level.SEVERE, "for company poc name = ");
				comp = utility.loadCompany(companyId);
				if(comp!=null && comp.getPocName()!=null && !comp.getPocName().equals("")) {
					logger.log(Level.SEVERE, "comp.getPocName() "+comp.getPocName());
					return comp.getPocName();
				}
			}
			
		}
		else {
			comp = utility.loadCompany(companyId);
			if(comp!=null && comp.getPocName()!=null && !comp.getPocName().equals("")) {
				return comp.getPocName();
			}
		}
		return "";
	}
	
}
