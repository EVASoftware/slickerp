package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.slicktechnologies.client.services.ServerUnitConversionServices;
import com.slicktechnologies.server.addhocdownload.XlsxWriter;
import com.slicktechnologies.server.taskqueue.BOMProductListMappingTaskQueue;
import com.slicktechnologies.shared.BOMServiceListBean;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.UnitConverCal;
import com.slicktechnologies.shared.common.billofproductmaterial.BillOfProductMaterialDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.inventory.BillProductDetails;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productgroup.ServiceProductGroupList;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
import com.slicktechnologies.shared.common.unitconversion.UnitConversion;
import com.slicktechnologies.shared.common.unitconversion.UnitConversionDetailList;

public class ServerUnitConversionUtility  extends RemoteServiceServlet implements  ServerUnitConversionServices{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4123045828356670195L;
	
	public static List<UnitConversion> prodUnitConverList  = new ArrayList<UnitConversion>();
	public static List<UnitConversion> areaUnitConverList  = new ArrayList<UnitConversion>();
	public static List<Config> unitForProduct = new ArrayList<Config>();
	public static List<Config> unitForService = new ArrayList<Config>();
	public static boolean billofMaterialActive = false;
	public List<ItemProduct> itemProList = new ArrayList<ItemProduct>();
	public  List<ProductGroupDetails> proGroupList = new ArrayList<ProductGroupDetails>();
	ArrayList<ItemProduct> proGroupItemList = new ArrayList<ItemProduct>();
//	DecimalFormat df2 = new DecimalFormat( "#.00" );
	DecimalFormat dft2 = new DecimalFormat("0.000");
	Logger logger=Logger.getLogger("UnitConversionUtility Logger");
	
	/**
	 * nidhi ||*||
	 * 27-12-2018
	 * @param unitName
	 * @param area
	 * @param prodTitle
	 * @param companyId
	 * @return
	 */
	public static List<BillOfMaterial> prodBillOfMeterialList  = new ArrayList<BillOfMaterial>();
	public ArrayList<ProductGroupList> getServiceProDetails(String unitName,Double area,String prodTitle,Long companyId){
		
		ArrayList<ProductGroupList> prodtList = new ArrayList<ProductGroupList>();
		if(proGroupList == null || proGroupList.size() == 0 ){
			proGroupList = ofy().load().type(ProductGroupDetails.class).filter("companyId", companyId).filter("title", prodTitle).list();
		}
		ProductGroupDetails prGpLt = new ProductGroupDetails();
		if(proGroupList!=null){
			for(ProductGroupDetails prDt  : proGroupList){
				if(prDt.getTitle().equals(prodTitle)){
					prGpLt = prDt;
					break;
				}
			}
		}
		
		
		if(prGpLt!=null){
			logger.log(Level.SEVERE,"billPrDt1.getBillProdItems() " + prGpLt.getTitle());
			
			ArrayList<Integer> proId = new ArrayList<Integer>();
			
			for(ProductGroupList proDt : prGpLt.getPitems()){
				proId.add(proDt.getProduct_id());
			}
			/**
			 *  ||*||
			 *  nidhi 27-12-2018
			 */
			if(areaUnitConverList==null || areaUnitConverList.size()==0)
			areaUnitConverList =ofy().load().type(UnitConversion.class).filter("companyId", prGpLt.getCompanyId()).filter("unitCode", 100).list();
			
			if(prodUnitConverList==null || prodUnitConverList.size()==0)
			prodUnitConverList =ofy().load().type(UnitConversion.class).filter("companyId", prGpLt.getCompanyId()).filter("unitCode",47).list();
			
			if(itemProList==null || itemProList.size()==0)
			itemProList = ofy().load().type(ItemProduct.class).filter("companyId", prGpLt.getCompanyId()).filter("count IN", proId).list();
			
			/**
			 * end
			 */
			
			for(ProductGroupList proDt : prGpLt.getPitems()){
				ProductGroupList proGrpList = proDt.copyOfObject();
				String prUnit = proGrpList.getUnit();
				System.out.println("unit --  " + proDt.getQuantity());
				Double prQty = proGrpList.getQuantity();
				if(unitName.equals("")){
					unitName = prGpLt.getUnit();
				}
				if(area==0){
					area = 1.0;
				}
				if(unitName.equals(prGpLt.getUnit())){
					prQty = (area*proGrpList.getQuantity()) / prGpLt.getArea();
				}else{
					double unitConver  = getConversionOfUnit(prGpLt.getUnit(),unitName,prGpLt.getArea());
					prQty = (area * proGrpList.getQuantity())/ unitConver;
				}
				
				double price = getPriceConversion(proGrpList.getUnit(), prQty, prUnit, proGrpList.getCode());
				proGrpList.setPrice(Double.parseDouble(dft2.format(price)));
				proGrpList.setUnit(prUnit);
				proGrpList.setQuantity(Double.parseDouble(dft2.format(prQty)));
				/**
				 *  ||*||
				 *  nidhi 27-12-2018 for area and unit
				 */
				proGrpList.setProActualUnit(unitName);
				proGrpList.setProActualQty(Double.parseDouble(dft2.format(area)));
				proGrpList.setPlannedQty(prQty);
				proGrpList.setPlannedUnit(prUnit);
				/**
				 * end ||*||
				 */
				prodtList.add(proGrpList);
				
			}
		}
		
		return prodtList;
	}
	
	public boolean varifyUnitConversion(BillOfMaterial billPRod, String unit,long companyId){
		
		if(proGroupList != null || proGroupList.size() == 0 ){
			proGroupList = ofy().load().type(ProductGroupDetails.class).filter("companyId", companyId).list();
		}
		
		if(areaUnitConverList==null || areaUnitConverList.size()==0)
			areaUnitConverList =ofy().load().type(UnitConversion.class).filter("companyId",companyId).filter("unitCode", 100).list();
			
		
		boolean get = true;
		for(BillProductDetails prDt: billPRod.getBillProdItems()){
			get = false;
			for(ProductGroupDetails pr : proGroupList){
				if(prDt.getProdGroupTitle().equals(pr.getTitle())){
					if(pr.getUnit().equals(unit)){
						get = true;
						break;
					}else{
						for( UnitConversion unitCon : areaUnitConverList){
							if(pr.getUnit().equals(unitCon.getUnit())){
								for(UnitConversionDetailList unList :unitCon.getUnitConversionList()){
									if((unList.getMaxUnit().equals(unit) && unList.getMinUnit().equals(pr.getUnit())) ||
											(unList.getMinUnit().equals(unit) && unList.getMaxUnit().equals(pr.getUnit()))){
										get = true;
										break;
									}
								}
//								break;
							}
						}
					}
				}
			}
			if(!get){
				return false;
			}
		}
		
		return get;
	}
	
	/**
	 *  ||**||
	 *  nidhi for get bom for old service and not define define bom for service
	 * @param popuplist1
	 * @param suprPrd
	 * @param serEntity
	 * @param billPrDt1
	 * @return
	 */
	public ArrayList<ProductGroupList>  getServiceitemProList(List<ServiceSchedule> popuplist1,SuperProduct suprPrd,Service serEntity,BillOfMaterial billPrDt1){
//		 HashMap<Integer, ArrayList<BranchWiseScheduling>> custBranch = lis1.getCustomerBranchSchedulingInfo();
		 
		 List<ServiceSchedule> seriveScheList = new ArrayList<ServiceSchedule>();
		 ArrayList<ProductGroupList> proGrop =new ArrayList<ProductGroupList>();
		 
		 
		 if(billPrDt1==null  && serEntity!=null){
			 
			 if(prodBillOfMeterialList == null || prodBillOfMeterialList.size()==0){
				 List<BillOfMaterial> billList  = ofy().load().type(BillOfMaterial.class).filter("companyId", serEntity.getCompanyId()).filter("status", true).list();
				 prodBillOfMeterialList.clear();
				 if(billList!=null){
					 billList.addAll(prodBillOfMeterialList);
					 prodBillOfMeterialList.addAll(billList);
				 }
			 }
			 	
			if(prodBillOfMeterialList.size()>0){
				billPrDt1 = verifyBillofMaterilProd(serEntity.getProduct());
			}
		 
		 }
		 
		
		 if(serEntity!=null && billPrDt1 !=null){
			 {
				 logger.log(Level.SEVERE,"billPrDt1.getBillProdItems() " + billPrDt1.getBillProdItems().size());
			 		{
			 			
			 			if(billPrDt1.getBillProdItems().size()>0){

			 				Comparator<BillProductDetails> serviceScheduleComparator = new Comparator<BillProductDetails>() {
								public int compare(BillProductDetails s1, BillProductDetails s2) {
									if(s1!=null && s2!=null)
									{
										if(s1.getServiceNo() == s2.getServiceNo()){
											return 0;}
										if(s1.getServiceNo()> s2.getServiceNo()){
											return 1;}
										else{
											return -1;}
									}
									else{
										return 0;}
								}
								};
								Collections.sort(billPrDt1.getBillProdItems(), serviceScheduleComparator);
			 			
			 					boolean get = false;
			 					{
									get = false;
									for(BillProductDetails bill : billPrDt1.getBillProdItems()){
//										setServiceSerialNo
										if(bill.getServiceNo() == serEntity.getServiceSerialNo()){
											get = true;
											
											String area = "";
											if(serEntity.getUom()!=null &&serEntity.getUom().trim().length()>0){
												area  = serEntity.getUom() ;
											}
											
											proGrop = getServiceProDetails(area,serEntity.getQuantity(),bill.getProdGroupTitle(),serEntity.getCompanyId());
											break;
										}
									}
									if(!get){
										BillProductDetails bill1 =	billPrDt1.getBillProdItems().get(billPrDt1.getBillProdItems().size()-1);
										
										proGrop = getServiceProDetails(serEntity.getUom(),serEntity.getQuantity(),bill1.getProdGroupTitle(),serEntity.getCompanyId());
									}
									
								}
								
			 			}
			 			
			 			
			 		}
			 	} 
		 }
		 	
		 	
		 logger.log(Level.SEVERE,"material list size --  "+ seriveScheList.size());
		 return proGrop;
	}
	
	
	public double getConversionOfUnit(String prGroupUnit, String serviceUnit,double area){
		double unitValue = 1;
		
//		if(areaUnitConverList==null || areaUnitConverList.size()==0)
//			areaUnitConverList =ofy().load().type(UnitConversion.class).filter("companyId",companyId).filter("unitCode", 100).list();
			
			for(UnitConversion unit : areaUnitConverList){
				if(unit.getUnit().equals(prGroupUnit)){
					for(UnitConversionDetailList unitDt : unit.getUnitConversionList()){
						
						if((unitDt.getMaxUnit().equals(prGroupUnit) && unitDt.getMinUnit().equals(serviceUnit))
							||	(unitDt.getMinUnit().equals(prGroupUnit) && unitDt.getMaxUnit().equals(serviceUnit))){
							if(prGroupUnit.equals(unitDt.getMinUnit())){
								unitValue = (area * unitDt.getMaxValue()) / unitDt.getMinValue();
							}else{
								unitValue = (area * unitDt.getMinValue()) / unitDt.getMaxValue();
							}
						}
					}
				}
			}
		
		return unitValue;
	}
	
	
	public UnitConverCal getConversionOfQtyUnit(String prGroupUnit,double qty){
		double unitValue = qty;
		String proUnit = prGroupUnit;
			for(UnitConversion unit : prodUnitConverList){
				if(unit.getUnit().equals(prGroupUnit)){
					for(UnitConversionDetailList unitDt : unit.getUnitConversionList()){
						if(unitDt.getMinValue()<qty && unitDt.getMinUnit().equals(prGroupUnit)){
							unitValue = (unitDt.getMaxValue() * qty ) / unitDt.getMinValue();
							proUnit = unitDt.getMaxUnit();
						}else if(unitDt.getMaxValue()>qty && unitDt.getMaxUnit().equals(prGroupUnit)){
							unitValue = (unitDt.getMinValue() * qty ) / unitDt.getMaxValue();
							proUnit = unitDt.getMinUnit();
						}
						
					}
				}
			}
			UnitConverCal unitcal = new UnitConverCal();
			unitcal.setArea(unitValue);
			unitcal.setUom(proUnit);
		return unitcal;
	}
	
	public double getPriceConversion(String proGroupUnit,double qty,String convertedUnit,String proCode){
		double upPrice = 1;
		
		
		ArrayList<Integer> proID = new ArrayList<Integer>();
		
		
			boolean flag = false;
				for(ItemProduct itm : proGroupItemList){
					if(itm.getProductCode().equals(proCode)){
						flag = true;
						upPrice = convertItemProPrice(itm,proGroupUnit,qty,convertedUnit,proCode);
						break;
					}
				}
				
				if(!flag){
					ItemProduct item = getItemProductFromGlobalLit(proCode);
					upPrice = convertItemProPrice(item, proGroupUnit, qty, convertedUnit, proCode);
				}
			
		
		return upPrice;
	}
	
	public ItemProduct getItemProductFromGlobalLit(String proCode){
		if(proGroupItemList == null){
			proGroupItemList = new ArrayList<ItemProduct>();
		}
		for(ItemProduct item : itemProList){
			if(item.getProductCode().equals(proCode)){
				
				proGroupItemList.add(item);
				return item;
			}
		}
		return null;
	}
	
	
	public double convertItemProPrice(ItemProduct item,String prGroupUnit,double qty,String convertedUnit,String proCode){
		double price = 1;
			if(convertedUnit.equals(prGroupUnit)){
				price = item.getPrice() * qty;
			}else{
				for(UnitConversion unit : prodUnitConverList){
					if(unit.getUnit().equals(prGroupUnit) && item.getUnitOfMeasurement().equals(prGroupUnit)){
						for(UnitConversionDetailList unitDt : unit.getUnitConversionList()){
							if(unitDt.getMinUnit().equals(prGroupUnit) && unitDt.getMaxUnit().equals(convertedUnit) ){
								price = (qty * item.getPrice()) / unitDt.getMaxValue(); 
							}
						}
					}
				}
			}
			
		return price;
	}

	@Override
	public BOMServiceListBean getServiceBOMReport(Long companyId, Date startDate,Date endDate,
			String branch) {

		List<Service> serDetails = ofy().load().type(Service.class)
				.filter("companyId", companyId)
				.filter("branch", branch)
				.filter("serviceDate >=", startDate)
				.filter("serviceDate <=", endDate).list();
		BOMServiceListBean bomBean = new BOMServiceListBean();
		
		if(serDetails.size()>0){
			logger.log(Level.SEVERE,"get service size -- " + serDetails.size());
			for(int j = 0 ;j<serDetails.size();j++){
				if(serDetails.get(j).getQuantity()>0 
						&& serDetails.get(j).getUom()!=null && serDetails.get(j).getUom().trim().length()>0){
				
					if(serDetails.get(j).getServiceProductList()==null || serDetails.get(j).getServiceProductList().size()==0){
						if(serDetails.get(j).getStatus().equals(Service.SERVICESTATUSCOMPLETED)){
							bomBean.getServicewithCompleted().add(serDetails.get(j).getCount());
							bomBean.getServicewithCompeted().add(serDetails.get(j).Myclone());
							bomBean.getServicePedding().add(serDetails.get(j).getProduct().getCount());
						
						}else if(serDetails.get(j).getStatus().equals(Service.SERVICESTATUSSCHEDULE)
								|| serDetails.get(j).getStatus().equals(Service.SERVICESTATUSPENDING )
								||  serDetails.get(j).getStatus().equals(Service.SERVICESTATUSRESCHEDULE)){
							bomBean.getServicePedding().add(serDetails.get(j).getProduct().getCount());
							bomBean.getServicePeddingLst().add(serDetails.get(j).Myclone());
						
						}
					}else if(serDetails.get(j).getServiceProductList()!=null || serDetails.get(j).getServiceProductList().size()!=0){
						bomBean.getServiceProList().add(serDetails.get(j).Myclone());
					}
			}else if(serDetails.get(j).getStatus().equals(Service.SERVICESTATUSSCHEDULE)
					|| serDetails.get(j).getStatus().equals(Service.SERVICESTATUSPENDING )
					||  serDetails.get(j).getStatus().equals(Service.SERVICESTATUSRESCHEDULE)){
//				bomBean.getServiceCompleted().add(serDetails.get(j).getCount());
				bomBean.getServiceProList().add(serDetails.get(j).Myclone());
			}else if(serDetails.get(j).getStatus().equals(Service.SERVICESTATUSCOMPLETED)){
				bomBean.getServiceCompleted().add(serDetails.get(j).getCount());
				bomBean.getServiceCompeted().add(serDetails.get(j).Myclone());
			}
		}
			
			ArrayList<Integer> comboList = new ArrayList<Integer>();
			comboList.addAll(bomBean.getServiceCompleted());
			comboList.addAll(bomBean.getServicewithCompleted());
			
			if(comboList.size()>0){
				List<MaterialIssueNote> minList = ofy().load().type(MaterialIssueNote.class)
						.filter("companyId", companyId)
						.filter("branch", branch)
						.filter("serviceId IN",comboList).list();
				
				if(minList.size()>0){
					bomBean.getMinList().addAll(minList);
					
				}
			}
			
			
			
			 if(bomBean.getServicePedding().size()>0){
				List<BillOfMaterial> bomEntity=ofy().load().type(BillOfMaterial.class)
						.filter("product_id IN", bomBean.getServicePedding())
						.filter("companyId",companyId).filter("status",true).list();
			
				if(bomEntity!=null){
					bomBean.getBomList().addAll(bomEntity);
				}
			 }
		}

		logger.log(Level.SEVERE,"get bom ser pendding list -- "+bomBean.getServicePedding().size() );
		logger.log(Level.SEVERE,"get bom ser pendding list -- "+bomBean.getServiceCompleted().size() );
		logger.log(Level.SEVERE,"get bom ser pendding list -- "+bomBean.getServiceProList().size() );
		return bomBean;
	}

	@Override
	public BOMServiceListBean getServiceGetUpdate(BOMServiceListBean bomBean,Long company) {
		
		logger.log(Level.SEVERE,"get bom ser pendding list -- "+bomBean.getServicePedding().size() );
		logger.log(Level.SEVERE,"get bom ser pendding list -- "+bomBean.getServiceCompleted().size() );
		logger.log(Level.SEVERE,"get bom ser pendding list -- "+bomBean.getServiceProList().size() );
		logger.log(Level.SEVERE,"get complete service list -- " + bomBean.getMinList().size());
		for(int j =0 ;j< bomBean.getServicePeddingLst().size();j++){
			
			boolean getFlag = false;
			if(bomBean.getServicePeddingLst().get(j).getQuantity()>0
					&& bomBean.getServicePeddingLst().get(j).getUom()!=null 
					&& bomBean.getServicePeddingLst().get(j).getUom().trim().length()>0){
				getFlag = false;
				for(BillOfMaterial bill : bomBean.getBomList()){
					
					if(bill.getProduct_id() == bomBean.getServicePeddingLst().get(j).getProjectId()){
						 ArrayList<ProductGroupList> serProGroupList =	getServiceitemProList(null, null, bomBean.getServicePeddingLst().get(j), bill);
						 for(ProductGroupList pr : serProGroupList){
							 	bomBean.getServicePeddingLst().get(j).getServiceProductList().add(bomBean.getServicePeddingLst().get(j).getServiceProdDt(pr));	 
						 }
						
						 if(serProGroupList.size()>0){
							 getFlag= true;
							 bomBean.getServiceProList().add(bomBean.getServicePeddingLst().get(j).Myclone());
						 }
					}
				}
			}
			
		}
		
		
		
		for(int j =0 ;j< bomBean.getServicewithCompeted().size();j++){
			
			boolean getFlag = false;
			if(bomBean.getServicewithCompeted().get(j).getQuantity()>0
					&& bomBean.getServicewithCompeted().get(j).getUom()!=null 
					&& bomBean.getServicewithCompeted().get(j).getUom().trim().length()>0){
				getFlag = false;
				for(BillOfMaterial bill : bomBean.getBomList()){
					
					if(bill.getProduct_id() == bomBean.getServicewithCompeted().get(j).getProjectId()){
						 ArrayList<ProductGroupList> serProGroupList =	getServiceitemProList(null, null, bomBean.getServicewithCompeted().get(j), bill);
						 for(ProductGroupList pr : serProGroupList){
							 	bomBean.getServicewithCompeted().get(j).getServiceProductList().add(bomBean.getServicewithCompeted().get(j).getServiceProdDt(pr));	 
						 }
						
						 for(MaterialIssueNote min : bomBean.getMinList()){
							 if(min.getServiceId() == bomBean.getServicewithCompeted().get(j).getCount() 
								&& min.getMinSoId() == bomBean.getServicewithCompeted().get(j).getContractCount()){
								 
								 BOMProductListMappingTaskQueue bomObj = new BOMProductListMappingTaskQueue();
								 Service ser = bomObj.minProListMapping(min, bomBean.getServicewithCompeted().get(j));
								 getFlag= true;
								 bomBean.getMinSerList().add(min.getCount());
								 bomBean.getServiceProList().add(ser.Myclone());
							 }
						 }
						 
						 if(serProGroupList.size()>0 && !getFlag){
							 getFlag= true;
							 bomBean.getServiceProList().add(bomBean.getServicewithCompeted().get(j).Myclone());
						 }
					}
				}
			}
			
		}
		
		if(bomBean.getMinList().size()>0){
		logger.log(Level.SEVERE,"get complete service list -- " + bomBean.getMinList().size());
		
		for(int j =0 ;j< bomBean.getServiceCompeted().size();j++){
			
			boolean getFlag = false;
			if(bomBean.getServiceCompeted().get(j).getQuantity()!=0
					&& bomBean.getServiceCompeted().get(j).getUom()!=null 
					&& bomBean.getServiceCompeted().get(j).getUom().trim().length()!=0){
				getFlag = false;
				for(BillOfMaterial bill : bomBean.getBomList()){
					
					if(bill.getProduct_id() == bomBean.getServiceCompeted().get(j).getProjectId()){
//						bomBean.getServiceCompeted().get(j).setUom(bill);
						 ArrayList<ProductGroupList> serProGroupList =	getServiceitemProList(null, null, bomBean.getServiceCompeted().get(j), bill);
						 for(ProductGroupList pr : serProGroupList){
							 	bomBean.getServiceCompeted().get(j).getServiceProductList().add(bomBean.getServiceCompeted().get(j).getServiceProdDt(pr));	 
						 }
						
						 for(MaterialIssueNote min : bomBean.getMinList()){
							 if(min.getServiceId() == bomBean.getServiceCompeted().get(j).getCount() 
								&& min.getMinSoId() == bomBean.getServiceCompeted().get(j).getContractCount()){
								 
								 BOMProductListMappingTaskQueue bomObj = new BOMProductListMappingTaskQueue();
								 Service ser = bomObj.minProListMapping(min, bomBean.getServiceCompeted().get(j));
								 getFlag= true;
								 bomBean.getMinSerList().add(min.getCount());
								 bomBean.getMmnSerSet().put(min.getServiceId(), min.getServiceId()+"-"+ min.getMinSoId());
								 bomBean.getServiceProList().add(ser.Myclone());
							 }
						 }
						 
						 if(serProGroupList.size()>0 && !getFlag){
							 getFlag= true;
							 bomBean.getServiceProList().add(bomBean.getServiceCompeted().get(j).Myclone());
						 }
					}
				}
			}
			
		}
		
		if(bomBean.getMinSerList().size()>0){
			List<MaterialMovementNote> mmnList =ofy().load().type(MaterialMovementNote.class)
					.filter("mmnMinId IN", bomBean.getMinSerList())
					.filter("companyId",company).list();
			
			if(mmnList.size()>0){
				bomBean.getMmnList().addAll(mmnList);
			}
		}
		
		}
		
		
		
		return bomBean;
	}
	
	
	@Override
	public BOMServiceListBean getServiceGetWithMrnUpdate(BOMServiceListBean bomBean,Long company) {
		
		if(bomBean.getMmnList()!=null && bomBean.getMmnList().size()>0){
			
			for(MaterialMovementNote mmnObj : bomBean.getMmnList()){
				List<String> al = Arrays.asList(bomBean.getMmnSerSet().get(mmnObj.getMmnMinId()));
				
				
				Integer serID= Integer.parseInt(al.get(0));
				Integer contId = Integer.parseInt(al.get(1));
				
				for(int i = 0 ;i<bomBean.getServiceProList().size();i++){
					
					if(bomBean.getServiceProList().get(i).getCount()==serID &&
							bomBean.getServiceProList().get(i).getContractCount()==contId){
						
						
						 for(MaterialIssueNote min : bomBean.getMinList()){
							 if(min.getServiceId() == bomBean.getServiceProList().get(i).getCount() 
								&& min.getMinSoId() == bomBean.getServiceProList().get(i).getContractCount()){
								 
								 BOMProductListMappingTaskQueue bomObj = new BOMProductListMappingTaskQueue();
								 Service ser = bomObj.getMrnUpdateProductList(mmnObj, min, bomBean.getServiceProList().get(i));
								
								 
								 
								 bomBean.getMinSerList().add(min.getCount());
								 bomBean.getServiceProList().set(i, ser.Myclone());
							 }
						 }
					}
					
				}
				
			}
			
		}
		if(bomBean.getServiceCompeted().size()>0){
			ofy().save().entities(bomBean.getServiceCompeted());
		}
		if(bomBean.getServicewithCompeted().size()>0){
			ofy().save().entities(bomBean.getServicewithCompeted());
		}
		
		if(bomBean.getServiceProList().size()>0){
			ofy().save().entities(bomBean.getServiceProList());
		}
		
		
		if(bomBean.getServiceProList().size()>0){
			XlsxWriter.bomServiceList = new ArrayList<Service>();
			for(Service ser : bomBean.getServiceProList()){
				XlsxWriter.bomServiceList.add(ser);
			}
		}
		
		
		return bomBean;
	}
	
	public BillOfMaterial getBomDetailsForServcice(){
		return null;
		
	}
	/**
	 *  ||*|| nidhi
	 * @param superProd
	 * @return
	 */
	
	public BillOfMaterial verifyBillofMaterilProd(SuperProduct superProd){
		BillOfMaterial bill = null;
		 if(prodBillOfMeterialList == null || prodBillOfMeterialList.size()==0){
			 List<BillOfMaterial> billList  = ofy().load().type(BillOfMaterial.class).filter("companyId", superProd.getCompanyId()).filter("status", true).list();
			 prodBillOfMeterialList.clear();
			 if(billList!=null){
				 billList.addAll(prodBillOfMeterialList);
				 prodBillOfMeterialList.addAll(billList);
			 }
		 }
		for(BillOfMaterial billpro : prodBillOfMeterialList){
			if(billpro.getCode().equals(superProd.getProductCode()) && billpro.getProduct_id() == superProd.getCount()){
				return billpro;
			}
		}
		
		return bill;
	}
	
	public BillOfMaterial getServiceUnitConvertionPoint(BillOfMaterial billPrDt1,Service serEntity){
		BillOfMaterial billDt = new BillOfMaterial();
		ArrayList<BillProductDetails> billProd = new ArrayList<BillProductDetails>();
		boolean get = false;
		for(BillProductDetails bill : billPrDt1.getBillProdItems()){
//			setServiceSerialNo
			if(bill.getServiceNo() == serEntity.getServiceSerialNo()){
				get = true;
				
				String area = "";
				if(serEntity.getUom()!=null &&serEntity.getUom().trim().length()>0){
					area  = serEntity.getUom() ;
				}
				billProd.add(0, bill);
//				proGrop = getServiceProDetails(area,serEntity.getQuantity(),bill.getProdGroupTitle(),serEntity.getCompanyId());
				break;
			}
		}
		if(!get){
			
			BillProductDetails bill1 =	billPrDt1.getBillProdItems().get(billPrDt1.getBillProdItems().size()-1);
			billProd.add(0, bill1);
//			proGrop = getServiceProDetails(serEntity.getUom(),serEntity.getQuantity(),bill1.getProdGroupTitle(),serEntity.getCompanyId());
		}
		billDt.setTitle(billPrDt1.gettitle());
		billDt.setBillProdItems(billProd);
		return  billDt;
	}
	/**
	 * end
	 */
}
