package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tools.ant.Project;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreInputStream;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.google.gwt.logging.client.DefaultLevel.Severe;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.DataMigrationService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.device.CatchTraps;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.addhocdownload.XlsxWriter;
import com.slicktechnologies.server.email.SendGridEmailServlet;
import com.slicktechnologies.server.humanresourcelayer.CtcAllocationServiceImpl;
import com.slicktechnologies.server.humanresourcelayer.LeaveAllocationServiceImpl;
import com.slicktechnologies.server.taskqueue.ContractDetailsTaskQueue;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.server.webservices.CreateServiceServlet;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ContractUploadDetails;
import com.slicktechnologies.shared.CreditNote;
import com.slicktechnologies.shared.CustomerContractDetails;
import com.slicktechnologies.shared.EmployeeAsset;
import com.slicktechnologies.shared.EmployeeAssetBean;
import com.slicktechnologies.shared.FumigationServiceReport;
import com.slicktechnologies.shared.MINUploadDetails;
import com.slicktechnologies.shared.MINUploadExcelDetails;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.StackMaster;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.ModificationHistory;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.articletype.ArticleTypeTravel;
import com.slicktechnologies.shared.common.attendance.AttandanceInfo;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorPriceListDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.Consumables;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchServiceLocation;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.helperlayer.PassportInformation;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.SocialInformation;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.humanresourcelayer.Bonus;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.Esic;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.PaidLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.ProvidentFund;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveAllocationProcess;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveGroup;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.inventory.BranchDetails;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.personlayer.Person;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productgroup.ServiceProductGroupList;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.role.UserRole;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.servicerelated.AssetMovementInfo;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.shippingpackingprocess.InspectionMethod;

public class DataMigrationImpl extends RemoteServiceServlet implements DataMigrationService {

	/**
	 * 
	 */

	/**
	 * Date - 11-12-2019 Deepak Added this below code for Employee configtype and
	 * configcategory
	 **/

	/** End **/

	private static final long serialVersionUID = -2846878289776129318L;

	ArrayList<ProductInventoryView> ProductInventoryViewlist = new ArrayList<ProductInventoryView>();
	ArrayList<ProductInventoryView> proinventoryviewlist = new ArrayList<ProductInventoryView>();
	ArrayList<ProductInventoryViewDetails> proinvendetaillist = new ArrayList<ProductInventoryViewDetails>();

	ArrayList<BranchDetails> brandetailslist = new ArrayList<BranchDetails>();

	Branch branch = new Branch();

	Logger logger = Logger.getLogger("Name of logger");
	private SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
	static BlobKey blobkey;

	/**
	 * Save Flag is added Date:22-10-2016 By Anil Release : 30 Sept 2016 Project
	 * :Purchase Modification(NBHC)
	 * 
	 * if this flag is true that mean we will save excel data to datastore and if
	 * false that mean we will only validate the given excel we will not create
	 * records currently only used for company asset
	 */
	/**
	 * nidhi 28-10-2017 for MIN Data upload proccess
	 * minDetailsMap.put("ServiceDetails",containtData.get(0));
	 * minDetailsMap.put("MinDetails",minDetails);
	 * minDetailsMap.put("EmpDetails",empInfoList);
	 * minDetailsMap.put("SerProject",serProject);
	 * minDetailsMap.put("MinIssNote",minIssnote);
	 * minDetailsMap.put("ProdInDetails",productInDetails);
	 * minDetailsMap.put("ProInErrorList",productInErrorList);
	 * minDetailsMap.put("ProInvDetails",prodInvDetails);
	 * minDetailsMap.put("MinList",minList);
	 * 
	 * minDetailsMap.put("EmpCount",emplcount);
	 * minDetailsMap.put("WareHouse",wareHouse);
	 * minDetailsMap.put("ProdCount",prodCount);
	 * minDetailsMap.put("ContCount",contCount);
	 * minDetailsMap.put("ProdDeatils",prodDetails);
	 */
	public static final String ERRORLIST = "ErrorList";
	public static final String SERVICEDETAILS = "ServiceDetails";
	public static final String MINDETAILS = "minDetails";
	public static final String EMPINFOLIST = "empInfoList";
	public static final String SERPROJECTS = "SerProjects";
	public static final String MINISSNOTE = "minIssnote";
	public static final String PRODUCTINDETAILS = "productInDetails";
	public static final String PRODUCTINERRORLIST = "productInErrorList";
	public static final String PRODINVDETAILS = "prodInvDetails";
	public static final String MINLIST = "minList";
	public static final String EMPLCOUNT = "emplcount";
	public static final String WAREHOUSE = "wareHouse";
	public static final String PRODCOUNT = "prodCount";
	public static final String CONTCOUNT = "contCount";
	public static final String PRODDETAILS = "prodDetails";
	public static final String SERCOUNT = "SerCount";
	public static final String COMPANYID = "CompanyID";
	public static final String BRANCH = "Branch";
	public static final String BRANCHList = "BranchList";
	public static final String CUSTSERLIST = "CustSerList";
	public static final String LOGINNAME = "loginId";
	public static final String EXCELSHIT = "ExcelShit";

	/**
	 * @author Anil , Date : 19-12-2019 removed the static keyword
	 */
	public ArrayList<String> minList;

	public ArrayList<String> errorList;
	HashSet<Integer> serCount = new HashSet<Integer>();
	HashSet<Integer> emplcount;
	HashSet<String> branchDtList;
	HashSet<String> wareHouse;
	HashSet<Integer> prodCount;
	HashSet<Integer> contCount;
	HashSet<Integer> CustSerList;
//	ArrayList<Service> serviceDetailList = new ArrayList<Service>();
	ArrayList<Employee> empInfoList = new ArrayList<Employee>();
	ArrayList<Branch> branchDetailList = new ArrayList<Branch>();
	ArrayList<ProductInventoryView> prodInvDetails = new ArrayList<ProductInventoryView>();

	ArrayList<ItemProduct> prodDetails = new ArrayList<ItemProduct>();
	ArrayList<ServiceProject> serProjectDetails = new ArrayList<ServiceProject>();
	ArrayList<MaterialIssueNote> minIssnote = new ArrayList<MaterialIssueNote>();

	Map<String, Double> productInDetails = new HashMap<String, Double>();
	Map<String, Double> productInErrorList = new HashMap<String, Double>();
//	List<ServiceProduct> serProDetails;
	private SimpleDateFormat sefmt = new SimpleDateFormat("dd-MM-yyyy");

//	ArrayList<MINUploadDetails> minDetails = new ArrayList<MINUploadDetails>();
	/**
	 * end
	 */

	/**
	 * nidhi 2-11-2017 contract upload process
	 */
	HashSet<String> empNameList;
	HashSet<String> paymentTerms;
	HashMap<Integer, String> customerDetail;
	HashMap<Integer, String> productDetail;
	List<CustomerContractDetails> contractDetails;
	List<Customer> custDetails;
	List<ServiceProduct> serProDetails;
	List<ConfigCategory> payTerms;
	List<Employee> empDetails;
	private SimpleDateFormat contfmt = new SimpleDateFormat("dd/MM/yyyy");
	private SimpleDateFormat mmDDfmt = new SimpleDateFormat("MM/dd/yyyy");
	public static ArrayList<String> contractList;
	/**
	 * end
	 */

	/***
	 * Date :- 06-07-2018 By Vijay Des :- below global list initilize from
	 * dropdownserviceImpl
	 **/
	public static List<Branch> globalBranchlist;
	public static List<City> globalCitylist;
	/**
	 * ends here
	 */

	int index = 0;

	public DataMigrationImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public ArrayList<Integer> savedRecordsDeatils(long companyId, String entityname, boolean saveFlag,
			String loggedinuser) {

		ArrayList<Integer> intlist = new ArrayList<Integer>();
		ArrayList<String> exceldatalist = new ArrayList<String>();
		DataFormatter datafmt1 = new DataFormatter();
		/*********************************
		 * Reading Excel Data & adding in list
		 **************************************/
		try {
			logger.log(Level.SEVERE, "Hiiiiiiiiiiiiiiiiiiiii from savedRecordsDeatils entityname=" + entityname);

			long i;

			Workbook wb = null;

			try {
				wb = WorkbookFactory.create(new BlobstoreInputStream(blobkey));
			} catch (EncryptedDocumentException e) {
				e.printStackTrace();
			} catch (InvalidFormatException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// if you upload other than .xls then it will give an error
				e.printStackTrace();
				intlist.add(-18);
				return intlist;
			}

			Sheet sheet = wb.getSheetAt(0);

			// Get iterator to all the rows in current sheet

			Iterator<Row> rowIterator = sheet.iterator();

			// Traversing over each row of XLSX file

			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through each columns
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						exceldatalist.add(cell.getStringCellValue());
						break;
					case Cell.CELL_TYPE_NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) {
							exceldatalist.add(fmt.format(cell.getDateCellValue()));
						} else {
							cell.getNumericCellValue();
							String sampletest = datafmt1.formatCellValue(cell);
							// i = (long) cell.getNumericCellValue();
							exceldatalist.add(sampletest + "");
							System.out.println("Value:" + sampletest);
						}
						break;
					case Cell.CELL_TYPE_BOOLEAN:
						break;

					// case Cell.CELL_TYPE_BLANK:
					// exceldatalist.add("NA");
					// break;

					default:
					}
				}
				System.out.println("");

			}

			System.out.println("Total size:" + exceldatalist.size());
			logger.log(Level.SEVERE, "FileString List Size:" + exceldatalist);

			wb.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		/*******************************
		 * end of excel data reading and adding in list
		 *******************/

		/*******************************
		 * Methods for inserting records in Entity
		 ********************/
		if (entityname.equals("Customer")) {
			if (exceldatalist.get(0).trim().equalsIgnoreCase("Company")
					&& exceldatalist.get(1).trim().equalsIgnoreCase("Company Name")
					&& exceldatalist.get(2).trim().equalsIgnoreCase("D.O.B/Establishment Date(mm/dd/yyyy)")
					&& exceldatalist.get(3).equalsIgnoreCase("salutation")
					&& exceldatalist.get(4).trim().equalsIgnoreCase("Full Name")
					&& exceldatalist.get(5).trim().equalsIgnoreCase("Email")
					&& exceldatalist.get(6).equalsIgnoreCase("Landline No.")
					&& exceldatalist.get(7).equalsIgnoreCase("Cell No.1")
					&& exceldatalist.get(8).equalsIgnoreCase("Cell No.2")
					&& exceldatalist.get(9).equalsIgnoreCase("Fax No.")
					&& exceldatalist.get(10).equalsIgnoreCase("Branch")
					&& exceldatalist.get(11).equalsIgnoreCase("Sales Person")
					&& exceldatalist.get(12).equalsIgnoreCase("Customer Group")
					&& exceldatalist.get(13).equalsIgnoreCase("Customer Category")
					&& exceldatalist.get(14).equalsIgnoreCase("Customer Type")
					&& exceldatalist.get(15).equalsIgnoreCase("Customer Priority")
					&& exceldatalist.get(16).equalsIgnoreCase("customer level")
					&& exceldatalist.get(17).equalsIgnoreCase("Reference No.1")
					&& exceldatalist.get(18).equalsIgnoreCase("Reference No.2")
					&& exceldatalist.get(19).equalsIgnoreCase("website")
					&& exceldatalist.get(20).equalsIgnoreCase("Description")
					&& exceldatalist.get(21).equalsIgnoreCase("google plus id")
					&& exceldatalist.get(22).equalsIgnoreCase("facebook id")
					&& exceldatalist.get(23).equalsIgnoreCase("twitter id")
					&& exceldatalist.get(24).equalsIgnoreCase("address line1")
					&& exceldatalist.get(25).equalsIgnoreCase("address line2")
					&& exceldatalist.get(26).equalsIgnoreCase("landmark")
					&& exceldatalist.get(27).equalsIgnoreCase("country")
					&& exceldatalist.get(28).equalsIgnoreCase("state") && exceldatalist.get(29).equalsIgnoreCase("city")
					&& exceldatalist.get(30).equalsIgnoreCase("locality")
					&& exceldatalist.get(31).equalsIgnoreCase("pin")
					&& exceldatalist.get(32).equalsIgnoreCase("address line1")
					&& exceldatalist.get(33).equalsIgnoreCase("address line2")
					&& exceldatalist.get(34).equalsIgnoreCase("landmark")
					&& exceldatalist.get(35).equalsIgnoreCase("country")
					&& exceldatalist.get(36).equalsIgnoreCase("state") && exceldatalist.get(37).equalsIgnoreCase("city")
					&& exceldatalist.get(38).equalsIgnoreCase("locality")
					&& exceldatalist.get(39).equalsIgnoreCase("pin")
					&& exceldatalist.get(40).equalsIgnoreCase("Customer GST No")) {
				intlist = customersSave(companyId, exceldatalist);
			} else {
				intlist.add(-1);
			}

		}

		if (entityname.equals("Branch")) {
			if (exceldatalist.get(0).equalsIgnoreCase("Branch Name")
					&& exceldatalist.get(1).equalsIgnoreCase("Branch Email")
					&& exceldatalist.get(2).equalsIgnoreCase("Status")
					&& exceldatalist.get(3).equalsIgnoreCase("Landline Phone")
					&& exceldatalist.get(4).equalsIgnoreCase("Cell Phone No.1")
					&& exceldatalist.get(5).equalsIgnoreCase("Cell Phone No.2")
					&& exceldatalist.get(6).equalsIgnoreCase("Fax No.")
					&& exceldatalist.get(7).equalsIgnoreCase("Point of contact name")
					&& exceldatalist.get(8).equalsIgnoreCase("Point of contact landline")
					&& exceldatalist.get(9).equalsIgnoreCase("Point of contact contact cell")
					&& exceldatalist.get(10).equalsIgnoreCase("Point of contact Email")
					&& exceldatalist.get(11).equalsIgnoreCase("Address Line 1")
					&& exceldatalist.get(12).equalsIgnoreCase("Address Line 2")
					&& exceldatalist.get(13).equalsIgnoreCase("Landmark")
					&& exceldatalist.get(14).equalsIgnoreCase("Country")
					&& exceldatalist.get(15).equalsIgnoreCase("State") && exceldatalist.get(16).equalsIgnoreCase("City")
					&& exceldatalist.get(17).equalsIgnoreCase("Locality")
					&& exceldatalist.get(18).equalsIgnoreCase("Pin")) {

				intlist = branchSave(companyId, exceldatalist);
			} else {
				intlist.add(-1);
			}
		}

		if (entityname.equals("Service Product")) {

			/**
			 * Date : 12-10-2017 BY ANIL earlier tax label Service Tax(%) and VAT Tax is
			 * replaced with Tax 1(CGST) and Tax 2(SGST) respectively.
			 */
			if (exceldatalist.get(0).equalsIgnoreCase("Durations in Days")
					&& exceldatalist.get(1).equalsIgnoreCase("No. of Services")
					&& exceldatalist.get(2).equalsIgnoreCase("Status")
					&& exceldatalist.get(3).equalsIgnoreCase("Product Code")
					&& exceldatalist.get(4).equalsIgnoreCase("Product Category")
					&& exceldatalist.get(5).equalsIgnoreCase("Product Name")
					&& exceldatalist.get(6).equalsIgnoreCase("Specification")
					&& exceldatalist.get(7).equalsIgnoreCase("Product Group")
					&& exceldatalist.get(8).equalsIgnoreCase("Product Type")
					&& exceldatalist.get(9).equalsIgnoreCase("Product Classification")
					&& exceldatalist.get(10).equalsIgnoreCase("Reference No.1")
					&& exceldatalist.get(11).equalsIgnoreCase("Reference No.2")
					&& exceldatalist.get(12).equalsIgnoreCase("Product Price")
					&& exceldatalist.get(13).equalsIgnoreCase("Tax 1(CGST)")
					&& exceldatalist.get(14).equalsIgnoreCase("Inclusive")
					&& exceldatalist.get(15).equalsIgnoreCase("Tax 2(SGST)")
					&& exceldatalist.get(16).equalsIgnoreCase("Inclusive")
					&& exceldatalist.get(17).equalsIgnoreCase("Unit of Measurement")
					&& exceldatalist.get(18).equalsIgnoreCase("Description 1")
					&& exceldatalist.get(19).equalsIgnoreCase("Description 2")) {
//				intlist = serviceproductsave(companyId, exceldatalist);
				intlist = validateServiceproductFile(companyId, exceldatalist);

			} else {
				intlist.add(-2);
			}
		}
		/**
		 * Date : 12-10-2017 BY ANIL earlier tax label VAT Tax and Service Tax is
		 * replaced with Tax 1(CGST) and Tax 2(SGST) respectively. Date : 23-07-2018 BY
		 * ANIL added HSN CODE at the upload excel level
		 */
		if (entityname.equals("Item Product")) {
			if (exceldatalist.get(0).trim().equalsIgnoreCase("Product Code")
					&& exceldatalist.get(1).trim().equalsIgnoreCase("Product Category")
					&& exceldatalist.get(2).trim().equalsIgnoreCase("Product Name")
					&& exceldatalist.get(3).trim().equalsIgnoreCase("Specification")
					&& exceldatalist.get(4).trim().equalsIgnoreCase("Product Group")
					&& exceldatalist.get(5).trim().equalsIgnoreCase("Product Type")
					&& exceldatalist.get(6).trim().equalsIgnoreCase("Product Classification")
					&& exceldatalist.get(7).trim().equalsIgnoreCase("Reference No.1")
					&& exceldatalist.get(8).trim().equalsIgnoreCase("Reference No.2")
					&& exceldatalist.get(9).trim().equalsIgnoreCase("Product Price")
					&& exceldatalist.get(10).trim().equalsIgnoreCase("Tax 1(CGST)")
					&& exceldatalist.get(11).trim().equalsIgnoreCase("Inclusive")
					&& exceldatalist.get(12).trim().equalsIgnoreCase("Tax 2(SGST)")
					&& exceldatalist.get(13).trim().equalsIgnoreCase("Inclusive")
					&& exceldatalist.get(14).trim().equalsIgnoreCase("Status")
					&& exceldatalist.get(15).trim().equalsIgnoreCase("Brand Name")
					&& exceldatalist.get(16).trim().equalsIgnoreCase("Model")
					&& exceldatalist.get(17).trim().equalsIgnoreCase("Serial No.")
					&& exceldatalist.get(18).trim().equalsIgnoreCase("Unit of Measurement")
					&& exceldatalist.get(19).trim().equalsIgnoreCase("Description 1")
					&& exceldatalist.get(20).trim().equalsIgnoreCase("Description 2")
					&& exceldatalist.get(21).trim().equalsIgnoreCase("Update Product Inventory")
					&& exceldatalist.get(22).trim().equalsIgnoreCase("Inspection Required")
					&& exceldatalist.get(23).trim().equalsIgnoreCase("Inspection Method")
					&& exceldatalist.get(24).trim().equalsIgnoreCase("Warehouse")
					&& exceldatalist.get(25).trim().equalsIgnoreCase("Storage Location")
					&& exceldatalist.get(26).trim().equalsIgnoreCase("Storage Bin")
					&& exceldatalist.get(27).trim().equalsIgnoreCase("Available Stock")
					&& exceldatalist.get(28).trim().equalsIgnoreCase("Service Id")
					&& exceldatalist.get(29).trim().equalsIgnoreCase("Purchase Price")
					&& exceldatalist.get(30).trim().equalsIgnoreCase("Purchase Tax 1(CGST)")
					&& exceldatalist.get(31).trim().equalsIgnoreCase("Inclusive")
					&& exceldatalist.get(32).trim().equalsIgnoreCase("Purchase Tax 2(SGST)")
					&& exceldatalist.get(33).trim().equalsIgnoreCase("Inclusive")
					&& exceldatalist.get(34).trim().equalsIgnoreCase("MRP")
					&& exceldatalist.get(35).trim().equalsIgnoreCase("HSN Code")
					&& exceldatalist.get(36).trim().equalsIgnoreCase("RefNo")) {
//				intlist = itemproductsave(companyId, exceldatalist);
				intlist = validateItemproductAndCreate(companyId, exceldatalist);

			} else {
				intlist.add(-2);
			}
		}

		if (entityname.equals("Country")) {

			if (exceldatalist.get(0).equalsIgnoreCase("Country") && exceldatalist.get(1).equalsIgnoreCase("Language")) {
				intlist = countrysave(companyId, exceldatalist);
			} else {
				intlist.add(-1);
			}
		}

		if (entityname.equals("State")) {
			if (exceldatalist.get(0).equalsIgnoreCase("Country") && exceldatalist.get(1).equalsIgnoreCase("State Name")
					&& exceldatalist.get(2).equalsIgnoreCase("State Code")
					&& exceldatalist.get(3).equalsIgnoreCase("Status")) {
				intlist = statesave(companyId, exceldatalist);
			} else {
				intlist.add(-1);
			}
		}

		if (entityname.equals("City")) {

			if (exceldatalist.get(0).equalsIgnoreCase("State") && exceldatalist.get(1).equalsIgnoreCase("City Name")
					&& exceldatalist.get(2).equalsIgnoreCase("Status")
					&& exceldatalist.get(3).equalsIgnoreCase("Classification Of City")
					&& exceldatalist.get(4).equalsIgnoreCase("TAT")) {
				intlist = citysave(companyId, exceldatalist);
			} else {
				intlist.add(-1);
			}
		}

		if (entityname.equals("Locality")) {
			if (exceldatalist.get(0).equalsIgnoreCase("City") && exceldatalist.get(1).equalsIgnoreCase("Locality Name")
					&& exceldatalist.get(2).equalsIgnoreCase("Pin Code")
					&& exceldatalist.get(3).equalsIgnoreCase("Status")) {
				intlist = localitysave(companyId, exceldatalist);
			} else {
				intlist.add(-1);
			}
		}

		if (entityname.equals("Employee")) {
			if (exceldatalist == null || exceldatalist.size() <= 0) {
				intlist.add(-3);
			} else if (exceldatalist.get(0).trim().equalsIgnoreCase("country")
					&& exceldatalist.get(1).trim().equalsIgnoreCase("department")
					&& exceldatalist.get(2).trim().equalsIgnoreCase("employee type")

					&& exceldatalist.get(3).trim().equalsIgnoreCase("full name")
					&& exceldatalist.get(4).trim().equalsIgnoreCase("Dob(mm/dd/yyyy)")
					&& exceldatalist.get(5).trim().equalsIgnoreCase("branch")
					&& exceldatalist.get(6).trim().equalsIgnoreCase("designation")
					&& exceldatalist.get(7).trim().equalsIgnoreCase("role")
					&& exceldatalist.get(8).trim().equalsIgnoreCase("joining date(mm/dd/yyyy)")
					&& exceldatalist.get(9).trim().equalsIgnoreCase("Marital Status")

					&& exceldatalist.get(10).trim().equalsIgnoreCase("reports to")
					&& exceldatalist.get(11).trim().equalsIgnoreCase("access card no.")
					&& exceldatalist.get(12).trim().equalsIgnoreCase("id card number")
					&& exceldatalist.get(13).trim().equalsIgnoreCase("blood group")
					&& exceldatalist.get(14).trim().equalsIgnoreCase("status")
					&& exceldatalist.get(15).trim().equalsIgnoreCase("bank name")
					&& exceldatalist.get(16).trim().equalsIgnoreCase("bank branch")
					&& exceldatalist.get(17).trim().equalsIgnoreCase("ifsc")
					&& exceldatalist.get(18).trim().equalsIgnoreCase("bank account no.")
					&& exceldatalist.get(19).trim().equalsIgnoreCase("esic no.")
					&& exceldatalist.get(20).trim().equalsIgnoreCase("pan no.")
					&& exceldatalist.get(21).trim().equalsIgnoreCase("ppf no.")
					&& exceldatalist.get(22).trim().equalsIgnoreCase("Aadhar Number")
					&& exceldatalist.get(23).trim().equalsIgnoreCase("UAN Number")
					&& exceldatalist.get(24).trim().equalsIgnoreCase("landline phone")
					&& exceldatalist.get(25).trim().equalsIgnoreCase("cell phone no.1")
					&& exceldatalist.get(26).trim().equalsIgnoreCase("cell phone no.2")
					&& exceldatalist.get(27).trim().equalsIgnoreCase("email")
					&& exceldatalist.get(28).trim().equalsIgnoreCase("Primary address line1")
					&& exceldatalist.get(29).trim().equalsIgnoreCase("Primary address line2")
					&& exceldatalist.get(30).trim().equalsIgnoreCase("landmark")
					&& exceldatalist.get(31).trim().equalsIgnoreCase("country")
					&& exceldatalist.get(32).trim().equalsIgnoreCase("state")
					&& exceldatalist.get(33).trim().equalsIgnoreCase("city")
					&& exceldatalist.get(34).trim().equalsIgnoreCase("locality")
					&& exceldatalist.get(35).trim().equalsIgnoreCase("Pin")

					&& exceldatalist.get(36).trim().equalsIgnoreCase("Secondary address line1")
					&& exceldatalist.get(37).trim().equalsIgnoreCase("Secondary address line2")
					&& exceldatalist.get(38).trim().equalsIgnoreCase("landmark")
					&& exceldatalist.get(39).trim().equalsIgnoreCase("country")
					&& exceldatalist.get(40).trim().equalsIgnoreCase("state")
					&& exceldatalist.get(41).trim().equalsIgnoreCase("city")
					&& exceldatalist.get(42).trim().equalsIgnoreCase("locality")
					&& exceldatalist.get(43).trim().equalsIgnoreCase("pin")
					&& exceldatalist.get(44).trim().equalsIgnoreCase("passport no.")
					&& exceldatalist.get(45).trim().equalsIgnoreCase("issued at")
					&& exceldatalist.get(46).trim().equalsIgnoreCase("issue date(mm/dd/yyyy)")
					&& exceldatalist.get(47).trim().equalsIgnoreCase("expiry date(mm/dd/yyyy)")
					&& exceldatalist.get(48).trim().equalsIgnoreCase("google plus id")
					&& exceldatalist.get(49).trim().equalsIgnoreCase("facebook id")
					&& exceldatalist.get(50).trim().equalsIgnoreCase("twitter id")

					// Date : 15-05-2018 BY ANIL,Added gender and number range
					&& exceldatalist.get(51).trim().equalsIgnoreCase("Gender")
					&& exceldatalist.get(52).trim().equalsIgnoreCase("Number Range")
					/** Date 17-5-2019 by AMOL added Employee Group **/
					&& exceldatalist.get(53).trim().equalsIgnoreCase("Employee Group")
					&& exceldatalist.get(54).trim().equalsIgnoreCase("Gross Salary")
					&& exceldatalist.get(55).trim().equalsIgnoreCase("Father/Husband Name")
					/**
					 * Added By Priyanka Date : 06-04-2021
					 */

					/**
					 * Ashwini Patil Date:9-05-2023 Need to setup CTC allocation, Project
					 * allocatioon, calendar and leave allocation in single upload
					 */
					&& exceldatalist.get(56).trim().equalsIgnoreCase("Is Reliever")
					&& exceldatalist.get(57).trim().equalsIgnoreCase("CTC Template")
					&& exceldatalist.get(58).trim().equalsIgnoreCase("CTC Applicable From Date(mm/dd/yyyy)")
					&& exceldatalist.get(59).trim().equalsIgnoreCase("CTC Applicable To Date(mm/dd/yyyy)")
					&& exceldatalist.get(60).trim().equalsIgnoreCase("CTC Effective From Date(mm/dd/yyyy)")
					&& exceldatalist.get(61).trim().equalsIgnoreCase("CTC Effective To Date(mm/dd/yyyy)")
					&& exceldatalist.get(62).trim().equalsIgnoreCase("Calender Name")
					&& exceldatalist.get(63).trim().equalsIgnoreCase("Leave Group")
					&& exceldatalist.get(64).trim().equalsIgnoreCase("Project")
					&& exceldatalist.get(65).trim().equalsIgnoreCase("OT")
					&& exceldatalist.get(66).trim().equalsIgnoreCase("Shift")
					&& exceldatalist.get(67).trim().equalsIgnoreCase("Shift Start Date(mm/dd/yyyy)")
					&& exceldatalist.get(68).trim().equalsIgnoreCase("Shift End Date(mm/dd/yyyy)")

			) {
//				intlist = employeesave(companyId, exceldatalist);

				intlist = employeeDeatilsSave(companyId, exceldatalist, loggedinuser);

			} else {
				intlist.add(-2);
			}
		}

		if (entityname.equals("Vendor")) {

			if (exceldatalist.get(0).equalsIgnoreCase("Vendor Name") && exceldatalist.get(1).equalsIgnoreCase("Status")
					&& exceldatalist.get(2).equalsIgnoreCase("POC Full Name")

//					&& exceldatalist.get(3).equalsIgnoreCase(
//							"P.O.C Middle Name")
//					&& exceldatalist.get(4).equalsIgnoreCase("P.O.C Last Name")

					&& exceldatalist.get(3).equalsIgnoreCase("Email")
					&& exceldatalist.get(4).equalsIgnoreCase("Landline No.")
					&& exceldatalist.get(5).equalsIgnoreCase("Cell No.1")
					&& exceldatalist.get(6).equalsIgnoreCase("Cell No.2")
					&& exceldatalist.get(7).equalsIgnoreCase("Fax No.")
					&& exceldatalist.get(8).equalsIgnoreCase("Vendor Group")
					&& exceldatalist.get(9).equalsIgnoreCase("Vendor Category")
					&& exceldatalist.get(10).equalsIgnoreCase("Vendor Type")
					&& exceldatalist.get(11).equalsIgnoreCase("Google Plus Id")
					&& exceldatalist.get(12).equalsIgnoreCase("Facebook id")
					&& exceldatalist.get(13).equalsIgnoreCase("Twitter Id")
					&& exceldatalist.get(14).equalsIgnoreCase("Address Line1")
					&& exceldatalist.get(15).equalsIgnoreCase("Address Line2")
					&& exceldatalist.get(16).equalsIgnoreCase("Landmark")
					&& exceldatalist.get(17).equalsIgnoreCase("Country")
					&& exceldatalist.get(18).equalsIgnoreCase("State") && exceldatalist.get(19).equalsIgnoreCase("City")
					&& exceldatalist.get(20).equalsIgnoreCase("Locality")
					&& exceldatalist.get(21).equalsIgnoreCase("Pin")
					&& exceldatalist.get(22).equalsIgnoreCase("Address Line1")
					&& exceldatalist.get(23).equalsIgnoreCase("Address Line2")
					&& exceldatalist.get(24).equalsIgnoreCase("Landmark")
					&& exceldatalist.get(25).equalsIgnoreCase("Country")
					&& exceldatalist.get(26).equalsIgnoreCase("State") && exceldatalist.get(27).equalsIgnoreCase("City")
					&& exceldatalist.get(28).equalsIgnoreCase("Locality")
					&& exceldatalist.get(29).equalsIgnoreCase("Pin")
					&& exceldatalist.get(30).equalsIgnoreCase("GST Number")) {
				intlist = vendorsave(companyId, exceldatalist);
			} else {
				intlist.add(-1);
			}
		}

		if (entityname.equals("Department")) {
			if (exceldatalist.get(0).equalsIgnoreCase("Department Name")
					&& exceldatalist.get(1).equalsIgnoreCase("Parent Department")
					&& exceldatalist.get(2).equalsIgnoreCase("H.O.D Name")
					&& exceldatalist.get(3).equalsIgnoreCase("Status")
					&& exceldatalist.get(4).equalsIgnoreCase("Description")
					&& exceldatalist.get(5).equalsIgnoreCase("Landline No.")
					&& exceldatalist.get(6).equalsIgnoreCase("Cell Phone No.1")
					&& exceldatalist.get(7).equalsIgnoreCase("Cell Phone No.2")
					&& exceldatalist.get(8).equalsIgnoreCase("Address Line 1")
					&& exceldatalist.get(9).equalsIgnoreCase("Address Line 2")
					&& exceldatalist.get(10).equalsIgnoreCase("Landmark")
					&& exceldatalist.get(11).equalsIgnoreCase("Country")
					&& exceldatalist.get(12).equalsIgnoreCase("State") && exceldatalist.get(13).equalsIgnoreCase("City")
					&& exceldatalist.get(14).equalsIgnoreCase("Locality")
					&& exceldatalist.get(15).equalsIgnoreCase("Pin")) {
				intlist = departmentsave(companyId, exceldatalist);
			} else {
				intlist.add(-1);
			}
		}

		if (entityname.equals("Warehouse")) {
			System.out.println("In Warehouse Option");
			if (exceldatalist.get(0).equalsIgnoreCase("WareHouse Name")
					&& exceldatalist.get(1).equalsIgnoreCase("WareHouse Email")
					&& exceldatalist.get(2).equalsIgnoreCase("Status")
					&& exceldatalist.get(3).equalsIgnoreCase("Landline No.")
					&& exceldatalist.get(4).equalsIgnoreCase("Cell Phone 1")
					&& exceldatalist.get(5).equalsIgnoreCase("Cell Phone 2")
					&& exceldatalist.get(6).equalsIgnoreCase("Fax No.")
					&& exceldatalist.get(7).equalsIgnoreCase("Point of Contact Name")
					&& exceldatalist.get(8).equalsIgnoreCase("Point of Contact Landline")
					&& exceldatalist.get(9).equalsIgnoreCase("Point of Contact Cell")
					&& exceldatalist.get(10).equalsIgnoreCase("Point of Contact Email")
					&& exceldatalist.get(11).equalsIgnoreCase("Address Line 1")
					&& exceldatalist.get(12).equalsIgnoreCase("Address Line 2")
					&& exceldatalist.get(13).equalsIgnoreCase("Landmark")
					&& exceldatalist.get(14).equalsIgnoreCase("Country")
					&& exceldatalist.get(15).equalsIgnoreCase("State") && exceldatalist.get(16).equalsIgnoreCase("City")
					&& exceldatalist.get(17).equalsIgnoreCase("Locality")
					&& exceldatalist.get(18).equalsIgnoreCase("Pin")
					&& exceldatalist.get(19).equalsIgnoreCase("Branch 1")
					&& exceldatalist.get(20).equalsIgnoreCase("Branch 2")
					&& exceldatalist.get(21).equalsIgnoreCase("Branch 3")
					&& exceldatalist.get(22).equalsIgnoreCase("Branch 4")
					&& exceldatalist.get(23).equalsIgnoreCase("Branch 5")
					&& exceldatalist.get(24).equalsIgnoreCase("Warehouse Code")
					&& exceldatalist.get(25).equalsIgnoreCase("Warehouse Type")
					&& exceldatalist.get(26).equalsIgnoreCase("Parent Warehouse")
					&& exceldatalist.get(27).equalsIgnoreCase("Expiry Date(mm/dd/yyyy)")) {
				intlist = warehousesave(companyId, exceldatalist);
			} else {
				intlist.add(-1);
			}
		}

		if (entityname.equals("Storage Location")) {
			if (exceldatalist.get(0).trim().equalsIgnoreCase("Warehouse")
					&& exceldatalist.get(1).trim().equalsIgnoreCase("Storage Location Name")
					&& exceldatalist.get(2).trim().equalsIgnoreCase("Storage Location Email")
					&& exceldatalist.get(3).trim().equalsIgnoreCase("Status")
					&& exceldatalist.get(4).trim().equalsIgnoreCase("Landline Phone")
					&& exceldatalist.get(5).trim().equalsIgnoreCase("Cell Phone 1")
					&& exceldatalist.get(6).trim().equalsIgnoreCase("Cell Phone 2")
					&& exceldatalist.get(7).trim().equalsIgnoreCase("Fax No.")
					&& exceldatalist.get(8).trim().equalsIgnoreCase("P.O.C Name")
					&& exceldatalist.get(9).trim().equalsIgnoreCase("P.O.C Landline")
					&& exceldatalist.get(10).trim().equalsIgnoreCase("P.O.C Contact Cell")
					&& exceldatalist.get(11).trim().equalsIgnoreCase("P.O.C Email")
					&& exceldatalist.get(12).trim().equalsIgnoreCase("Address Line 1")
					&& exceldatalist.get(13).trim().equalsIgnoreCase("Address Line 2")
					&& exceldatalist.get(14).trim().equalsIgnoreCase("Landmark")
					&& exceldatalist.get(15).trim().equalsIgnoreCase("Country")
					&& exceldatalist.get(16).trim().equalsIgnoreCase("State")
					&& exceldatalist.get(17).trim().equalsIgnoreCase("City")
					&& exceldatalist.get(18).trim().equalsIgnoreCase("Locality")
					&& exceldatalist.get(19).trim().equalsIgnoreCase("Pin")) {
				intlist = storagelocationsave(companyId, exceldatalist);
			} else {
				System.out.println("In Else of Storage Location");
				intlist.add(-1);
			}
		}

		if (entityname.equals("Storage Bin")) {
			if (exceldatalist.get(0).equalsIgnoreCase("Name") && exceldatalist.get(1).equalsIgnoreCase("X-Coordinate")
					&& exceldatalist.get(2).equalsIgnoreCase("Y-Coordinate")
					&& exceldatalist.get(3).equalsIgnoreCase("Status")
					&& exceldatalist.get(4).equalsIgnoreCase("Storage Location")
					&& exceldatalist.get(5).equalsIgnoreCase("Description")) {
				intlist = storagebinsave(companyId, exceldatalist);
			} else {
				intlist.add(-1);
			}
		}
		if (entityname.equals("Contact Person")) {

			if (exceldatalist.get(0).equalsIgnoreCase("Entity Type") && exceldatalist.get(1).equalsIgnoreCase("Role")
					&& exceldatalist.get(2).equalsIgnoreCase("Name")
					&& exceldatalist.get(3).equalsIgnoreCase("Cell Phone 1")
					&& exceldatalist.get(4).equalsIgnoreCase("Cell Phone 2")
					&& exceldatalist.get(5).equalsIgnoreCase("Landline")
					&& exceldatalist.get(6).equalsIgnoreCase("Fax No.")
					&& exceldatalist.get(7).equalsIgnoreCase("Email 1")
					&& exceldatalist.get(8).equalsIgnoreCase("Email 2")
					&& exceldatalist.get(9).equalsIgnoreCase("Address Line 1")
					&& exceldatalist.get(10).equalsIgnoreCase("Address Line 2")
					&& exceldatalist.get(11).equalsIgnoreCase("Landmark")
					&& exceldatalist.get(12).equalsIgnoreCase("Country")
					&& exceldatalist.get(13).equalsIgnoreCase("State") && exceldatalist.get(14).equalsIgnoreCase("City")
					&& exceldatalist.get(15).equalsIgnoreCase("Locality")
					&& exceldatalist.get(16).equalsIgnoreCase("Pin")
					&& exceldatalist.get(17).equalsIgnoreCase("Google Plus ID")
					&& exceldatalist.get(18).equalsIgnoreCase("Facebook Id")
					&& exceldatalist.get(19).equalsIgnoreCase("Twitter Id")) {
				intlist = contactpersonsave(companyId, exceldatalist);
			} else {
				intlist.add(-1);
			}
		}

		if (entityname.equals("Company Asset")) {
			if (exceldatalist.get(0).trim().equalsIgnoreCase("Asset Name")
					&& exceldatalist.get(1).trim().equalsIgnoreCase("Asset Category")
					&& exceldatalist.get(2).trim().equalsIgnoreCase("Brand")
					&& exceldatalist.get(3).trim().equalsIgnoreCase("Model Number")
					&& exceldatalist.get(4).trim().equalsIgnoreCase("Sr. Number")
					&& exceldatalist.get(5).trim().equalsIgnoreCase("Purchased From")
					&& exceldatalist.get(6).trim().equalsIgnoreCase("PO No")
					&& exceldatalist.get(7).trim().equalsIgnoreCase("Purchase Date(mm/dd/yyyy)")
					&& exceldatalist.get(8).trim().equalsIgnoreCase("Date Of manufacture(mm/dd/yyyy)")
					&& exceldatalist.get(9).trim().equalsIgnoreCase("Date of Installation(mm/dd/yyyy)")
					&& exceldatalist.get(10).trim().equalsIgnoreCase("Warrenty Until(mm/dd/yyyy)")
					&& exceldatalist.get(11).trim().equalsIgnoreCase("Status")
					&& exceldatalist.get(12).trim().equalsIgnoreCase("Reference Number")
					&& exceldatalist.get(13).trim().equalsIgnoreCase("Warehouse Name")
					&& exceldatalist.get(14).trim().equalsIgnoreCase("Storage Location")
					&& exceldatalist.get(15).trim().equalsIgnoreCase("Storage Bin")
					&& exceldatalist.get(16).trim().equalsIgnoreCase("Product Id")

					&& exceldatalist.get(17).trim().equalsIgnoreCase("Article Type1")
					&& exceldatalist.get(18).trim().equalsIgnoreCase("Article Number1")
					&& exceldatalist.get(19).trim().equalsIgnoreCase("Validity1(In Days)")
					&& exceldatalist.get(20).trim().equalsIgnoreCase("Issue Date1(mm/dd/yyyy)")
					&& exceldatalist.get(21).trim().equalsIgnoreCase("Issued At1")
					&& exceldatalist.get(22).trim().equalsIgnoreCase("Issued By1")

					&& exceldatalist.get(23).trim().equalsIgnoreCase("Article Type2")
					&& exceldatalist.get(24).trim().equalsIgnoreCase("Article Number2")
					&& exceldatalist.get(25).trim().equalsIgnoreCase("Validity2(In Days)")
					&& exceldatalist.get(26).trim().equalsIgnoreCase("Issue Date2(mm/dd/yyyy)")
					&& exceldatalist.get(27).trim().equalsIgnoreCase("Issued At2")
					&& exceldatalist.get(28).trim().equalsIgnoreCase("Issued By2")

					&& exceldatalist.get(29).trim().equalsIgnoreCase("Article Type3")
					&& exceldatalist.get(30).trim().equalsIgnoreCase("Article Number3")
					&& exceldatalist.get(31).trim().equalsIgnoreCase("Validity3(In Days)")
					&& exceldatalist.get(32).trim().equalsIgnoreCase("Issue Date3(mm/dd/yyyy)")
					&& exceldatalist.get(33).trim().equalsIgnoreCase("Issued At3")
					&& exceldatalist.get(34).trim().equalsIgnoreCase("Issued By3")
					&& exceldatalist.get(35).trim().equalsIgnoreCase("GRN Number") && exceldatalist.get(36).trim()
							.equalsIgnoreCase("Branch")
					&& exceldatalist.get(37).trim() /** date 7.12.2018 added by komal for asset price **/
							.equalsIgnoreCase("Price")) {

				intlist = companyAssetsave(companyId, exceldatalist, saveFlag);

			} else {
				intlist.add(-1);
			}
		}

		/**
		 * Description: This is for updating in Product in inventory. Creation Date:
		 * 15-Sept-2016 Project : EVA Erp Required For: Everyone(This was created for
		 * NBHC Update). Created By : Rahul Verma
		 * 
		 */
		if (entityname.equals("Stock Update")) {

			if (exceldatalist.get(0).trim().equalsIgnoreCase("*Product Code")
					&& exceldatalist.get(1).trim().equalsIgnoreCase("*Inspection Required (Yes/No)")
					&& exceldatalist.get(2).trim().equalsIgnoreCase("Inspection Method(*/NA)")
					&& exceldatalist.get(3).trim().equalsIgnoreCase("Warehouse")
					&& exceldatalist.get(4).trim().equalsIgnoreCase("Storage Location")
					&& exceldatalist.get(5).trim().equalsIgnoreCase("Storage Bin")
					&& exceldatalist.get(6).trim().equalsIgnoreCase("Minimum Quantity")
					&& exceldatalist.get(7).trim().equalsIgnoreCase("Maximum Quantity")
					&& exceldatalist.get(8).trim().equalsIgnoreCase("Normal Level")
					&& exceldatalist.get(9).trim().equalsIgnoreCase("Reorder Level")
					&& exceldatalist.get(10).trim().equalsIgnoreCase("Available Quantity")) {
				intlist = stockUpdateSave(companyId, exceldatalist);
			} else {
				intlist.add(-1);
			}
		}

//		/** date 14/11/2017 added by komal to save customer branch **/
//		if (entityname.equals("Customer Branch")) {
//				if (exceldatalist.get(0).trim().equalsIgnoreCase("*Customer ID")
////						&& exceldatalist.get(1).trim()
////								.equalsIgnoreCase("*Customer Name")
////						&& exceldatalist.get(2).trim()
////								.equalsIgnoreCase("*Customer Cell")
//						&& exceldatalist.get(1).trim()
//								.equalsIgnoreCase("*Branch Name")
//						&& exceldatalist.get(2).trim()
//								.equalsIgnoreCase("Branch Email")
//						&& exceldatalist.get(3).trim()
//								.equalsIgnoreCase("*Servicing Branch")
//						&& exceldatalist.get(4).trim()
//								.equalsIgnoreCase("*Cell Phone No 1")
//						&& exceldatalist.get(5).trim()
//								.equalsIgnoreCase("Branch GSTIN")
//						&& exceldatalist.get(6).trim()
//								.equalsIgnoreCase("Point Of Contact Name")
//						&& exceldatalist.get(7).trim()
//								.equalsIgnoreCase("Point Of Contact Landline")
//				        && exceldatalist.get(8).trim()
//								.equalsIgnoreCase("Point Of Contact Cell")				
//						&& exceldatalist.get(9).trim()
//								.equalsIgnoreCase("Point Of Contact Email")
//						&& exceldatalist.get(10).trim()
//								.equalsIgnoreCase("*Address Line 1")
//						&& exceldatalist.get(11).trim()
//								.equalsIgnoreCase("Address Line 2")
//						&& exceldatalist.get(12).trim()
//								.equalsIgnoreCase("Landmark")
//						&& exceldatalist.get(13).trim()
//								.equalsIgnoreCase("Locality")
//						&& exceldatalist.get(14).trim()
//								.equalsIgnoreCase("*City")
//						&& exceldatalist.get(15).trim()
//								.equalsIgnoreCase("*State")
//					    && exceldatalist.get(16).trim()
//								.equalsIgnoreCase("*Country")
//						&& exceldatalist.get(17).trim()
//								.equalsIgnoreCase("Pin")
//						/** date 16.02.2018 added by komal for ares wise billing **/
//						&& exceldatalist.get(18).trim()
//								.equalsIgnoreCase("Area")
//						&& exceldatalist.get(19).trim()
//								.equalsIgnoreCase("UOM")
//						&& exceldatalist.get(20).trim()
//								.equalsIgnoreCase("Cost Center"))		
//								
//					{
//					intlist = customerBranchSave(companyId, exceldatalist);
//				} else {
//					intlist.add(-1);
//				}
//			
//		}
		/**
		 * END
		 */

		/**
		 * Description: This is for updating in Employee's Salary and Monthly Working
		 * Hours. Creation Date: 18-Oct-2017 Project : EVA Erp Required For: NBHC(This
		 * was created for NBHC Update). Created By : Rahul Verma
		 * 
		 */

		if (entityname.equals("Update Employee")) {
			logger.log(Level.SEVERE, "inside read header merthod ");
			if (exceldatalist.get(0).trim().equalsIgnoreCase("Employee Id")
					&& exceldatalist.get(1).trim().equalsIgnoreCase("Employee Name")
					&& exceldatalist.get(2).trim().equalsIgnoreCase("Branch")
					&& exceldatalist.get(3).trim().equalsIgnoreCase("Salary")
					&& exceldatalist.get(4).trim().equalsIgnoreCase("Working Hours")
					&& exceldatalist.get(5).trim().equalsIgnoreCase("Designation")
					&& exceldatalist.get(6).trim().equalsIgnoreCase("Role")
					&& exceldatalist.get(7).trim().equalsIgnoreCase("Joining Date(mm/dd/yyyy)")
					&& exceldatalist.get(8).trim().equalsIgnoreCase("Marital Status")
					&& exceldatalist.get(9).trim().equalsIgnoreCase("Reports To")
					&& exceldatalist.get(10).trim().equalsIgnoreCase("Access Card No.")
					&& exceldatalist.get(11).trim().equalsIgnoreCase("Id Card Number")
					&& exceldatalist.get(12).trim().equalsIgnoreCase("Blood Group")
					&& exceldatalist.get(13).trim().equalsIgnoreCase("Status")
					&& exceldatalist.get(14).trim().equalsIgnoreCase("Bank Name")
					&& exceldatalist.get(15).trim().equalsIgnoreCase("Bank Branch")
					&& exceldatalist.get(16).trim().equalsIgnoreCase("IFSC")
					&& exceldatalist.get(17).trim().equalsIgnoreCase("Bank Account No.")
					&& exceldatalist.get(18).trim().equalsIgnoreCase("ESIC No.")
					&& exceldatalist.get(19).trim().equalsIgnoreCase("PAN No.")
					&& exceldatalist.get(20).trim().equalsIgnoreCase("PPF No")
					&& exceldatalist.get(21).trim().equalsIgnoreCase("Aadhar Number")
					&& exceldatalist.get(22).trim().equalsIgnoreCase("UAN Number")
					&& exceldatalist.get(23).trim().equalsIgnoreCase("Landline Phone")
					&& exceldatalist.get(24).trim().equalsIgnoreCase("Cell Phone No.1")
					&& exceldatalist.get(25).trim().equalsIgnoreCase("Cell Phone No.2")
					&& exceldatalist.get(26).trim().equalsIgnoreCase("Email")
					&& exceldatalist.get(27).trim().equalsIgnoreCase("Primary address line1")
					&& exceldatalist.get(28).trim().equalsIgnoreCase("Primary address line2")
					&& exceldatalist.get(29).trim().equalsIgnoreCase("Landmark")
					&& exceldatalist.get(30).trim().equalsIgnoreCase("Country")
					&& exceldatalist.get(31).trim().equalsIgnoreCase("State")
					&& exceldatalist.get(32).trim().equalsIgnoreCase("City")
					&& exceldatalist.get(33).trim().equalsIgnoreCase("Locality")
					&& exceldatalist.get(34).trim().equalsIgnoreCase("Pin")
					&& exceldatalist.get(35).trim().equalsIgnoreCase("Secondary address line1")
					&& exceldatalist.get(36).trim().equalsIgnoreCase("Secondary address line2")
					&& exceldatalist.get(37).trim().equalsIgnoreCase("Secondary address landmark")
					&& exceldatalist.get(38).trim().equalsIgnoreCase("Secondary address country")
					&& exceldatalist.get(39).trim().equalsIgnoreCase("Secondary address state")
					&& exceldatalist.get(40).trim().equalsIgnoreCase("Secondary address city")
					&& exceldatalist.get(41).trim().equalsIgnoreCase("Secondary address locality")
					&& exceldatalist.get(42).trim().equalsIgnoreCase("Secondary address pin")
					&& exceldatalist.get(43).trim().equalsIgnoreCase("Gender")
					&& exceldatalist.get(44).trim().equalsIgnoreCase("Number Range")
					&& exceldatalist.get(45).trim().equalsIgnoreCase("Employee Group")

					&& exceldatalist.get(46).trim().equalsIgnoreCase("Father/Husband Name")
					&& exceldatalist.get(47).trim().equalsIgnoreCase("Project")
					&& exceldatalist.get(48).trim().equalsIgnoreCase("Site Location")
					&& exceldatalist.get(49).trim().equalsIgnoreCase("Is Reliever")
					&& exceldatalist.get(50).trim().equalsIgnoreCase("Birth Date(mm/dd/yyyy)")) {
				intlist = updateEmployeeRecords(companyId, exceldatalist);
			} else {
				logger.log(Level.SEVERE, "inside else of header");
				intlist.add(-2);
			}
		}
		/**
		 * END
		 */

		/**
		 * This is for updating contract type ...!!
		 * 
		 * created by Manisha on 13/11/2017
		 */

		/*
		 * commented by Ashwini
		 */
//		if (entityname.equals("Update Contract")) {
//			if (exceldatalist.get(0).trim().equalsIgnoreCase("Contract Id")
//					&& exceldatalist.get(1).trim().equalsIgnoreCase("Branch")
//					&& exceldatalist.get(2).trim()
//							.equalsIgnoreCase("Customer id")
//					&& exceldatalist.get(3).trim()
//							.equalsIgnoreCase("Contract type")
//					&& exceldatalist.get(4).trim().equalsIgnoreCase("Contract category")
//							) {
//				intlist = updateContractRecords(companyId, exceldatalist);
//			} else {
//				intlist.add(-1);
//			}
//		}
		/** end **/

		/*
		 * @author Ashwini Date :11-01-2018 Des:To update sales person
		 */
		if (entityname.equals("Update Contract")) {
			if (exceldatalist.get(0).trim().equalsIgnoreCase("Contract Id")
					&& exceldatalist.get(1).trim().equalsIgnoreCase("Branch")
					&& exceldatalist.get(2).trim().equalsIgnoreCase("Customer id")
					&& exceldatalist.get(3).trim().equalsIgnoreCase("Contract type")
					&& exceldatalist.get(4).trim().equalsIgnoreCase("Contract category")
					&& exceldatalist.get(5).trim().equalsIgnoreCase("Sales Person")) {
				intlist = updateContractRecords(companyId, exceldatalist);
			} else {
				intlist.add(-1);
			}
		}
		// if (entityname.equals("Product Inventory")) {
		// intlist = productinventorysave(companyId, exceldatalist);
		// }

		// ////////*********************************** End of Methods//
		// *****************************************///////////

		if (entityname.equals(AppConstants.ScheduleOrCompleteViaExcel)) {
			logger.log(Level.SEVERE, "in ScheduleOrCompleteViaExcel");

			if (exceldatalist.get(0).trim().equalsIgnoreCase("Service ID")
					&& exceldatalist.get(1).trim().equalsIgnoreCase("Customer Name")
					&& exceldatalist.get(2).trim().equalsIgnoreCase("Customer Branch")
					&& exceldatalist.get(3).trim().equalsIgnoreCase("Locality")
					&& exceldatalist.get(4).trim().equalsIgnoreCase("Service Date(MM/dd/yyyy)")
					&& exceldatalist.get(5).trim().equalsIgnoreCase("Service Time")
					&& exceldatalist.get(6).trim().equalsIgnoreCase("Technician")
					&& exceldatalist.get(7).trim().equalsIgnoreCase("Service Description")
					&& exceldatalist.get(8).trim().equalsIgnoreCase("Service Status")
					&& exceldatalist.get(9).trim().equalsIgnoreCase("Schedule Status")
					&& exceldatalist.get(10).trim().equalsIgnoreCase("Completion Date(MM/dd/yyyy)")
					&& exceldatalist.get(11).trim().equalsIgnoreCase("Completion Time")
					&& exceldatalist.get(12).trim().equalsIgnoreCase("Completion Remark")
					&& exceldatalist.get(13).trim().equalsIgnoreCase("Technician Remark")
					&& exceldatalist.get(14).trim().equalsIgnoreCase("Customer Rating")
					&& exceldatalist.get(15).trim().equalsIgnoreCase("Reason For Cancellation")
					&& exceldatalist.get(16).trim().equalsIgnoreCase("Service Address")
					&& exceldatalist.get(17).trim().equalsIgnoreCase("Service Name")
					&& exceldatalist.get(18).trim().equalsIgnoreCase("Contract ID")) {
				intlist = validateServiceData(companyId, exceldatalist, loggedinuser);

			} else {
				intlist.add(-2);
			}
		}

		return intlist;
	}

	/** date 14/11/2017 added by komal to save customer branch **/

	private ArrayList<CustomerBranchDetails> customerBranchSave(long companyId, ArrayList<String> exceldatalist) {

		/**
		 * @author Anil @since 10-05-2021 Due to large customer database, customer
		 *         branch is not uploading on pecopp link so loading only specific
		 *         customers
		 */
		ArrayList<Integer> custIdList = new ArrayList<Integer>();
		for (int i = 31; i < exceldatalist.size(); i += 31) {
			try {
				custIdList.add(Integer.parseInt(exceldatalist.get(i)));
			} catch (Exception e) {

			}
		}

		logger.log(Level.SEVERE, "Cust Id List size : " + custIdList.size());

		ArrayList<CustomerBranchDetails> branchlist = new ArrayList<CustomerBranchDetails>();

		int sizeOfList = 0;
		for (int i = 31; i < exceldatalist.size(); i += 31) {
			sizeOfList++;
		}
		if (sizeOfList > 150) {
			CustomerBranchDetails branch = new CustomerBranchDetails();
			branch.setCount(-2);
			branchlist.add(branch);
			return branchlist;
		}
//		int fileRecords;
//		int insertedRecords;
//		int lastCount;
//		int newCount;
//		ArrayList<CustomerBranchDetails> list = new ArrayList<CustomerBranchDetails>();
		ArrayList<String> errorList = new ArrayList<String>();
		ArrayList<Integer> integerlist = new ArrayList<Integer>();

		/**
		 * @author Anil @since 10-05-2021
		 */
		List<CustomerBranchDetails> listofbranch = new ArrayList<CustomerBranchDetails>();
		List<Customer> listofCustomer = new ArrayList<Customer>();
		if (custIdList.size() != 0) {
			listofbranch = ofy().load().type(CustomerBranchDetails.class).filter("companyId", companyId)
					.filter("cinfo.count IN", custIdList).list();
			listofCustomer = ofy().load().type(Customer.class).filter("companyId", companyId)
					.filter("count IN", custIdList).list();
		} else {
			listofbranch = ofy().load().type(CustomerBranchDetails.class).filter("companyId", companyId).list();
			listofCustomer = ofy().load().type(Customer.class).filter("companyId", companyId).list();
		}
//
//		NumberGeneration numgenration = new NumberGeneration();
//		numgenration = ofy().load().type(NumberGeneration.class)
//				.filter("companyId", companyId).filter("processName", "CustomerBranchDetails")
//				.filter("status", true).first().now();
//
//		long number = numgenration.getNumber();
//		int count = (int) number;
		boolean checkNull = true;

		/***
		 * Date 06/07/2018 By Vijay For global list loading if not loaded for validation
		 * purpose
		 */
		if (globalBranchlist == null) {
			List<Branch> branchData = ofy().load().type(Branch.class).filter("companyId", companyId).list();
			globalBranchlist = branchData;
		}
		if (globalCitylist == null) {
			List<City> cityData = ofy().load().type(City.class).filter("companyId", companyId).list();
			globalCitylist = cityData;
		}

		for (int i = 31; i < exceldatalist.size(); i += 31) {

			String error = "";
			CustomerBranchDetails branch = new CustomerBranchDetails();

			boolean validcheck = validationCustomerBranch(listofbranch, exceldatalist, i);
			boolean validateCustomer = validationCustomer(listofCustomer, exceldatalist, i);

			System.out.println("The value of validcheck is : " + validcheck);

			errorList.add(exceldatalist.get(i));
			errorList.add(exceldatalist.get(i + 1));
			try {
				branch.getCinfo().setCount(Integer.parseInt(exceldatalist.get(i)));
			} catch (Exception e) {
				error += "Please enter valid customer ID / ";
				branch.setCount(-1);
			}
			String fullname = "";
			Long cellNumber = 0l;
			String pocName = "";
			/** new **/
			for (Customer customer : listofCustomer) {
				if (customer.getCount() == Integer.parseInt(exceldatalist.get(i))) {

					if (customer.isCompany()) {
						fullname = customer.getCompanyName();
						pocName = customer.getFullname();
					} else {
						fullname = customer.getFullname();
					}

					cellNumber = customer.getCellNumber1();
					break;
				}
			}
			branch.getCinfo().setFullName(fullname);
			branch.getCinfo().setCellNumber(cellNumber);
			branch.getCinfo().setPocName(pocName);
//			if (exceldatalist.get(i + 1).equalsIgnoreCase("na")) {
//				branch.getCinfo().setFullName("");
//
//			} else {
//				branch.getCinfo().setFullName(exceldatalist.get(i + 1));
//			}
//			
//			if (exceldatalist.get(i + 2).equalsIgnoreCase("No")) {
//				branch.getCinfo().setCellNumber(0l);
//
//			} else {
//				branch.getCinfo().setCellNumber(Long.parseLong(exceldatalist.get(i + 2)));
//			}

			if (exceldatalist.get(i + 1).equalsIgnoreCase("na")) {
				// branch.setBusinessUnitName("");
				error += "Branch Name is Mamdatory / ";
				branch.setCount(-1);

			} else {
				branch.setBusinessUnitName(exceldatalist.get(i + 1));

			}

			if (validcheck == false) {
				error += "Customer branch is already exist / ";
				branch.setCount(-1);
			}

			if (validateCustomer == false) {
				error += "Customer does not exist / ";
				branch.setCount(-1);
			}

			if (exceldatalist.get(i + 2).equalsIgnoreCase("na")) {
				branch.setEmail("");
			} else {

				branch.setEmail(exceldatalist.get(i + 2));
			}

			if (exceldatalist.get(i + 3).equalsIgnoreCase("na")) {
				// branch.setBranch("");
				error += "Servicing Branch is Mandatory / ";
				branch.setCount(-1);
			} else {
				boolean servicingBranchValidation = ValidateServiceBranch(exceldatalist.get(i + 3).trim());
				if (servicingBranchValidation == false) {
					error += "Servicing Branch does not exist or does not match with database Please check / ";
					branch.setCount(-1);
				}
				branch.setBranch(exceldatalist.get(i + 3));
			}

			if (exceldatalist.get(i + 4).equalsIgnoreCase("na")) {
				// branch.setCellNumber1(0l);
				error += "Cell Phone No 1 is Mandatory / ";
				branch.setCount(-1);
			} else {
				try {
					branch.setCellNumber1(Long.parseLong(exceldatalist.get(i + 4)));
				} catch (Exception e) {
					error += "Please enter valid Cell Phone No 1 / ";
					branch.setCount(-1);
				}
			}

			if (exceldatalist.get(i + 5).equalsIgnoreCase("na")) {
				branch.setGSTINNumber("");
			} else {

				branch.setGSTINNumber(exceldatalist.get(i + 5));
			}
			if (exceldatalist.get(i + 6).equalsIgnoreCase("na")) {
				branch.setPocName("");
			} else {

				branch.setPocName(exceldatalist.get(i + 6));
			}
			if (exceldatalist.get(i + 7).equalsIgnoreCase("na")) {
				branch.setPocLandline(0l);
			} else {
				try {
					branch.setPocLandline(Long.parseLong(exceldatalist.get(i + 7)));
				} catch (Exception e) {
					error += "Please enter valid Point Of Contact Landline / ";
					branch.setCount(-1);
				}
			}
			if (exceldatalist.get(i + 8).equalsIgnoreCase("na")) {
				branch.setPocCell(0l);
			} else {
				try {
					branch.setPocCell(Long.parseLong(exceldatalist.get(i + 8)));
				} catch (Exception e) {
					error += "Please enter valid Point Of Contact Cell / ";
					branch.setCount(-1);
				}
			}
			if (exceldatalist.get(i + 9).equalsIgnoreCase("na")) {
				branch.setPocEmail("");
			} else {

				branch.setPocEmail(exceldatalist.get(i + 9));
			}

			Address address = new Address();
			if (exceldatalist.get(i + 10).equalsIgnoreCase("na")) {
				// address.setAddrLine1("");
				error += "Address Line 1 is Mandatory / ";
				branch.setCount(-1);
			} else {
				address.setAddrLine1(exceldatalist.get(i + 10));
			}
			if (exceldatalist.get(i + 11).equalsIgnoreCase("na")) {
				address.setAddrLine2("");
			} else {
				address.setAddrLine2(exceldatalist.get(i + 11));
			}
			if (exceldatalist.get(i + 12).equalsIgnoreCase("na")) {
				address.setLandmark("");
			} else {
				address.setLandmark(exceldatalist.get(i + 12));
			}
			if (exceldatalist.get(i + 13).equalsIgnoreCase("na")) {
				address.setLocality("");
			} else {
				address.setLocality(exceldatalist.get(i + 13));
			}

			if (exceldatalist.get(i + 14).equalsIgnoreCase("na")) {
				// address.setCity("");
				error += "City is Mandatory / ";
				branch.setCount(-1);
			} else {

				/**
				 * Date 06-07-2018 by Vijay Des :- for servicing City validation
				 */
				boolean cityValidation = ValidateCity(exceldatalist.get(i + 14).trim());
				if (cityValidation == false) {
					error += exceldatalist.get(i + 14).trim() + "-"
							+ "City does not exist or does not match Please check / ";
					branch.setCount(-1);
				}
				/**
				 * ends here
				 */

				address.setCity(exceldatalist.get(i + 14));
			}

			if (exceldatalist.get(i + 15).equalsIgnoreCase("na")) {
				// address.setState("");
				error += "State is Mandatory / ";
				branch.setCount(-1);
			} else {
				address.setState(exceldatalist.get(i + 15));
			}
			if (exceldatalist.get(i + 16).equalsIgnoreCase("na")) {
				// address.setCountry("");
				error += "Country is Mandatory / ";
				branch.setCount(-1);
			} else {
				address.setCountry(exceldatalist.get(i + 16));
			}

			if (exceldatalist.get(i + 17).equalsIgnoreCase("na")) {
				address.setPin(0l);
			} else {
				try {
					address.setPin(Long.parseLong(exceldatalist.get(i + 17)));
				} catch (Exception e) {
					error += "Please enter valid Pin Code / ";
					branch.setCount(-1);
				}

			}

			Address address1 = new Address();
			if (exceldatalist.get(i + 18).equalsIgnoreCase("na")) {
				error += "Billing Address Line 1 is Mandatory / ";
				branch.setCount(-1);
			} else {
				address1.setAddrLine1(exceldatalist.get(i + 18));
			}
			if (exceldatalist.get(i + 19).equalsIgnoreCase("na")) {
				address1.setAddrLine2("");
			} else {
				address1.setAddrLine2(exceldatalist.get(i + 19));
			}
			if (exceldatalist.get(i + 20).equalsIgnoreCase("na")) {
				address1.setLandmark("");
			} else {
				address1.setLandmark(exceldatalist.get(i + 20));
			}
			if (exceldatalist.get(i + 21).equalsIgnoreCase("na")) {
				address1.setLocality("");
			} else {
				address1.setLocality(exceldatalist.get(i + 21));
			}

			if (exceldatalist.get(i + 22).equalsIgnoreCase("na")) {
				error += "Billing Address City is Mandatory / ";
				branch.setCount(-1);
			} else {
				boolean cityValidation = ValidateCity(exceldatalist.get(i + 22).trim());
				if (cityValidation == false) {
					error += "Billing Address City does not exist or does not match Please check / ";
					branch.setCount(-1);
				}
				address1.setCity(exceldatalist.get(i + 22));
			}

			if (exceldatalist.get(i + 23).equalsIgnoreCase("na")) {
				// address.setState("");
				error += "Billing Address State is Mandatory / ";
				branch.setCount(-1);
			} else {
				address1.setState(exceldatalist.get(i + 23));
			}
			if (exceldatalist.get(i + 24).equalsIgnoreCase("na")) {
				// address.setCountry("");
				error += "Billing Address Country is Mandatory / ";
				branch.setCount(-1);
			} else {
				address1.setCountry(exceldatalist.get(i + 24));
			}

			if (exceldatalist.get(i + 25).equalsIgnoreCase("na")) {
				address1.setPin(0l);
			} else {
				try {
					address1.setPin(Long.parseLong(exceldatalist.get(i + 25)));
				} catch (Exception e) {
					error += "Please enter valid Pin Code / ";
					branch.setCount(-1);
				}

			}
			branch.setBillingAddress(address1);

			/** date 16.02.2018 added by komal foe area wise billing **/
			if (exceldatalist.get(i + 26).equalsIgnoreCase("na")) {
				branch.setArea(0);
			} else {
				try {
					branch.setArea(Double.parseDouble(exceldatalist.get(i + 26)));
				} catch (Exception e) {
					error += "Please enter valid Area / ";
					branch.setCount(-1);
				}

			}
			if (exceldatalist.get(i + 27).equalsIgnoreCase("na")) {
				branch.setUnitOfMeasurement(null);
			} else {
				branch.setUnitOfMeasurement(exceldatalist.get(i + 27));
			}
			if (exceldatalist.get(i + 28).equalsIgnoreCase("na")) {
				branch.setCostCenter(null);
			} else {
				branch.setCostCenter(exceldatalist.get(i + 28));
			}
			/**
			 * end komal
			 */
			if (exceldatalist.get(i + 29).equalsIgnoreCase("na")) {
				branch.setTierName("");
			} else {
				branch.setTierName(exceldatalist.get(i + 29));
			}
			if (exceldatalist.get(i + 30).equalsIgnoreCase("na")) {
				branch.setTat("");
			} else {
				branch.setTat(exceldatalist.get(i + 30));
			}

			branch.setAddress(address);
			branch.setStatus(true);
//			branch.setCount(++count);
			branch.setCompanyId(companyId);
			errorList.add(error);
			branchlist.add(branch);
			System.out.println("inside loop");
//			else if(result.contains(-22)){
//				form.showDialogMessage("Error: Customer branch is already exist!!");
//			} else if(result.contains(-23)){	
//				form.showDialogMessage("Error: Customer does not exist!!");
//		    } else if(result.contains(-24)){	
//				form.showDialogMessage("Error: Please enter values in all Mandatory Fields !!");
//		    }
//		    else if(result.contains(-25)){	
//				form.showDialogMessage("Error: Servicing Branch does not exist or does not match Please check !!");
//		    }
//			else if(result.contains(-26)){	
//				form.showDialogMessage("Error: City does not exist or does not match Please check !!");
//		    }
		}
//		if(checkNull){
//		System.out.println("Outside loop");
//		lastCount = ofy().load().type(CustomerBranchDetails.class)
//				.filter("companyId", companyId).count();
//		System.out.println("LastCount" + lastCount);
//
//		ofy().save().entities(branchlist).now();
//
//		numgenration.setNumber(count);
//		GenricServiceImpl impl = new GenricServiceImpl();
//		impl.save(numgenration);
//
//		System.out
//				.println("Data saved successfuly");
//		logger.log(Level.SEVERE,
//				"Data saved successfuly");
//
//		newCount = ofy().load().type(CustomerBranchDetails.class)
//				.filter("companyId", companyId).count();
//		System.out.println("New Count" + newCount);
//
//		System.out.println("number@@@" + numgenration.getNumber());
//
//		fileRecords = branchlist.size();
//		insertedRecords = newCount - lastCount;
//		System.out.println("Result:" + insertedRecords);
//		integerlist.add(fileRecords);
//		integerlist.add(insertedRecords);
//		System.out.println("SIZE:" + branchlist.size());
//		}
//		else{
//			integerlist.add(-24);
//		}
		CsvWriter.customerBranchErrorList = errorList;
		return branchlist;
	}

	private boolean validationCustomerBranch(List<CustomerBranchDetails> listofbranch, ArrayList<String> exceldatalist,
			int i) {
		// TODO Auto-generated method stub
		boolean value = true;

		for (int j = 0; j < listofbranch.size(); j++) {
			if (listofbranch.get(j).getBusinessUnitName().trim().equalsIgnoreCase(exceldatalist.get(i + 1).trim())
					&& listofbranch.get(j).getCinfo().getCount() == Integer.parseInt(exceldatalist.get(i).trim())) {
				System.out.println("Inside if of validationforbranch method");
				value = false;
				System.out.println("The Value contains at stage 1: " + value);
				break;
			}
		}
		System.out.println("The Value contains at stage 2 : " + value);
		return value;

	}

	private boolean validationCustomer(List<Customer> listofCustomer, ArrayList<String> exceldatalist, int i) {
		// TODO Auto-generated method stub
		boolean value = false;

		for (int j = 0; j < listofCustomer.size(); j++) {
			if (listofCustomer.get(j).getCount() == Integer.parseInt(exceldatalist.get(i).trim())) {
				System.out.println("Inside if of validationforbranch method");
				value = true;
				System.out.println("The Value contains at stage 1: " + value);
				break;
			}
		}
		System.out.println("The Value contains at stage 2 : " + value);
		return value;

	}

	/**
	 * Ends for Komal
	 */

	/**
	 * Date 06-07-2018 by Vijay Des :- for servicing branch validation
	 */
	private boolean ValidateCity(String city) {
		for (int i = 0; i < globalCitylist.size(); i++) {
			if (globalCitylist.get(i).getCityName().equals(city)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Date 06-07-2018 by Vijay Des :- for servicing branch validation
	 */
	private boolean ValidateServiceBranch(String servicingBranch) {

		if (globalBranchlist != null && globalBranchlist.size() > 0) {
			for (int i = 0; i < globalBranchlist.size(); i++) {
				if (globalBranchlist.get(i).getBusinessUnitName().equals(servicingBranch)) {
					return true;
				}
			}
		} else {

		}
		return false;
	}

	/**
	 * ends here
	 */

	/**
	 * This is for updating contract type ...!!
	 * 
	 * created by Manisha Date:13/11/2017
	 */

	private ArrayList<Integer> updateContractRecords(long companyId, ArrayList<String> exceldatalist) {

		int records;

		ArrayList<Contract> contractlist = new ArrayList<Contract>();
		ArrayList<Integer> integerlist1 = new ArrayList<Integer>();
		ArrayList<Integer> contractCountList = new ArrayList<Integer>();

		/*
		 * commented by Ashwini
		 */
//		for (int c = 5; c < exceldatalist.size(); c += 5) {
//			contractCountList
//					.add(Integer.parseInt(exceldatalist.get(c).trim()));
//		}

		/*
		 * @author Ashwini
		 */
		for (int c = 6; c < exceldatalist.size(); c += 6) {
			contractCountList.add(Integer.parseInt(exceldatalist.get(c).trim()));
		}

		/*
		 * 
		 * end here
		 */
		List<Contract> listofcontract = ofy().load().type(Contract.class).filter("companyId", companyId)
				.filter("count IN", contractCountList).list();

		if (listofcontract == null || listofcontract.size() == 0) {
			integerlist1.add(-21);
			return integerlist1;
		}

//		for (int c = 5; c < exceldatalist.size(); c += 5) {  //Commented by Ashwini
		for (int c = 6; c < exceldatalist.size(); c += 6) { // Added by Ashwini
			boolean matchFlag = false;
			int contractCount = Integer.parseInt(exceldatalist.get(c).trim());
			int customercount = Integer.parseInt(exceldatalist.get(c + 2).trim());

			for (int i = 0; i < listofcontract.size(); i++) {
				if (listofcontract.get(i).getCount() == contractCount
						&& listofcontract.get(i).getCinfo().getCount() == customercount) {
					Contract con = listofcontract.get(i);
					String contracttype = exceldatalist.get(c + 3);
					String contractcategory = exceldatalist.get(c + 4);
					String salesPerson = exceldatalist.get(c + 5); // Added by Ashwini
					if (contracttype.equalsIgnoreCase("NA")) {
					} else {
						con.setType(contracttype);
					}

					if (contractcategory.equalsIgnoreCase("NA")) {
					} else {
						con.setCategory(contractcategory);
					}

					if (salesPerson.equalsIgnoreCase("NA")) {

					} else {
						con.setEmployee(salesPerson);
					}

					/*
					 * end by Ashwini
					 */

					contractlist.add(con);
					matchFlag = true;
				}
			}
			if (matchFlag == false) {
				integerlist1.add(-21);
				return integerlist1;
			}
		}

		if (contractlist.size() != 0) {
			ofy().save().entities(contractlist).now();
		}

		records = contractlist.size();
		integerlist1.add(records);
		integerlist1.add(records);

		return integerlist1;

	}

	/** ends **/

	/**
	 * By: Rahul Verma Date 18 Oct 2017 Description: Added for NBHC to update all
	 * Employee Salaries and Monthly Working Hours
	 * 
	 * @param companyId
	 * @param exceldatalist
	 * @return
	 */
	private ArrayList<Integer> updateEmployeeRecords(long companyId, ArrayList<String> exceldatalist) {
		ArrayList<Employee> employeelist = new ArrayList<Employee>();
		ArrayList<String> errorlist = new ArrayList<String>();

		logger.log(Level.SEVERE, "inside read value merthod ");
		int fileRecords;
		int insertedRecords;
		int lastcount;
		int newcount;
		errorList = new ArrayList<>();
		ArrayList<Integer> integerlist = new ArrayList<Integer>();

		List<EmployeeInfo> employeeInfolist = ofy().load().type(EmployeeInfo.class).filter("companyId", companyId)
				.list();

		HashSet<Integer> hsemployeeid = new HashSet<Integer>();
		HashSet<String> hsProj = new HashSet<String>();
		HashSet<City> hsCity = new HashSet<City>();

		boolean loadallEmployeeFlag = false;
		// for (int p = 49; p < exceldatalist.size(); p += 49)
		for (int p = 51; p < exceldatalist.size(); p += 51) { // changed from 50 to 51 as birthdate column added at the
																// end
			if (!exceldatalist.get(p + 47).trim().equalsIgnoreCase("NA")) {
				hsProj.add(exceldatalist.get(p + 47).trim());
			}
			if (!exceldatalist.get(p).trim().equalsIgnoreCase("0")
					|| !exceldatalist.get(p).trim().equalsIgnoreCase("NA")) {
				hsemployeeid.add(Integer.parseInt(exceldatalist.get(p).trim()));
			}
			if (!exceldatalist.get(p + 9).trim().equalsIgnoreCase("NA")) {
				loadallEmployeeFlag = true;
			}

			if (!exceldatalist.get(p + 32).trim().equalsIgnoreCase("NA")) {
				City city = new City();
				city.setCityName(exceldatalist.get(p + 32).trim());
				if (!exceldatalist.get(p + 31).trim().equalsIgnoreCase("NA")) {
					city.setState(exceldatalist.get(p + 31).trim());
				}
				hsCity.add(city);
			}
			if (!exceldatalist.get(p + 40).trim().equalsIgnoreCase("NA")) {
				City city = new City();
				city.setCityName(exceldatalist.get(p + 40).trim());
				if (!exceldatalist.get(p + 39).trim().equalsIgnoreCase("NA")) {
					city.setState(exceldatalist.get(p + 39).trim());
				}
				hsCity.add(city);
			}

		}
		ArrayList<Integer> arrayEmployeeid = new ArrayList<Integer>(hsemployeeid);
		List<Employee> listofemployee = ofy().load().type(Employee.class).filter("companyId", companyId)
				.filter("count IN", arrayEmployeeid).list();
		logger.log(Level.SEVERE, "listofemployee" + listofemployee.size());

		List<Employee> allEmployeelist = new ArrayList<Employee>();
		if (loadallEmployeeFlag) {
			allEmployeelist = ofy().load().type(Employee.class).filter("companyId", companyId).list();
			logger.log(Level.SEVERE, "listofemployee " + listofemployee.size());
		}
		ArrayList<String> projList = new ArrayList<String>(hsProj);

		List<HrProject> projectList = null;

		if (projList.size() != 0) {
			projectList = ofy().load().type(HrProject.class).filter("companyId", companyId).filter("status", true)
					.filter("projectName IN", projList).list();
		}

		ArrayList<CustomerBranchDetails> siteLocationList = new ArrayList<CustomerBranchDetails>();
		HashSet<String> stateHs = new HashSet<String>();
		HashSet<Integer> custIdHs = new HashSet<Integer>();
		if (projectList != null && projectList.size() > 0) {
			for (HrProject project : projectList) {
				if (project.getPersoninfo() != null && project.getPersoninfo().getCount() != 0) {
					custIdHs.add(project.getPersoninfo().getCount());
				}
				if (project.getState() != null && !project.getState().equals("")) {
					stateHs.add(project.getState());
				}
			}
		}

		if (custIdHs.size() > 0) {
			List<CustomerBranchDetails> customerBranchList = ofy().load().type(CustomerBranchDetails.class)
					.filter("companyId", companyId).filter("cinfo.count IN", custIdHs).list();
			if (customerBranchList != null) {
				logger.log(Level.SEVERE, "customerBranchList SIZE :: " + customerBranchList.size());
				for (String state : stateHs) {
					for (CustomerBranchDetails obj : customerBranchList) {
						if (obj.getAddress() != null && obj.getAddress().getState().equals(state)) {
							siteLocationList.add(obj);
						}
					}
				}
			}
		}
		logger.log(Level.SEVERE, "siteLocationList SIZE :: " + siteLocationList.size());

		ServerAppUtility serverapputility = new ServerAppUtility();

		List<Config> empDesignationlist = serverapputility.loadConfigsData(13, companyId);
		List<Config> emprolelist = serverapputility.loadConfigsData(12, companyId);

		List<Country> globalCountrylist = ofy().load().type(Country.class).filter("companyId", companyId).list();
		List<State> globalStatelist = ofy().load().type(State.class).filter("companyId", companyId).list();
		List<Locality> globalLocalitylist = ofy().load().type(Locality.class).filter("companyId", companyId).list();

		List<City> globalCitylist = ofy().load().type(City.class).filter("companyId", companyId).list();
		ArrayList<City> newcitylist = null;
		if (hsCity.size() > 0) {
			newcitylist = new ArrayList<City>(hsCity);
			serverapputility.createnewCities(globalCitylist, newcitylist, companyId);
		}

		List<Config> numberRangelist = serverapputility.loadConfigsData(91, companyId);
		List<String> employeeGrouplist = new ArrayList<String>();
		employeeGrouplist.add("Direct");
		employeeGrouplist.add("Indirect");

		ArrayList<String> genderlist = new ArrayList<String>();
		genderlist.add("Male");
		genderlist.add("Female");
		genderlist.add("Other");

		ArrayList<String> martialstatus = new ArrayList<String>();
		martialstatus.add("Married");
		martialstatus.add("Unmarried");
		martialstatus.add("widow");
		martialstatus.add("widower");

		// for (int p = 49; p < exceldatalist.size(); p += 49)
		for (int p = 51; p < exceldatalist.size(); p += 51) {

			logger.log(Level.SEVERE, "inside print values ");
			String error = "";

			try {

				if (exceldatalist.get(p).equalsIgnoreCase("0") || exceldatalist.get(p).equalsIgnoreCase("NA")) {
					error += "Employee Id is Mandatory. ";
				} else {
					error += serverapputility.validateNumericCloumn(exceldatalist.get(0).trim(),
							exceldatalist.get(p).trim());

				}
				if (exceldatalist.get(p + 1).trim().equalsIgnoreCase("NA")) {
					error += "Employee Name is Mandatory. ";
				} else {
					error += serverapputility.validateSpecialCharComma(exceldatalist.get(1).trim(),
							exceldatalist.get(p + 1).trim());

					String strEmp = validateEmployee(listofemployee, exceldatalist.get(p).trim(),
							exceldatalist.get(p + 1).trim());
					System.out.println("strEmp ==" + strEmp);
					if (!strEmp.equals("")) {
						error += strEmp;
					}

					/**
					 * @author Vijay Date :- 25-12-2020 added validation and with project Name and
					 *         Site Location
					 * 
					 */
					if (exceldatalist.get(p + 2).trim().equalsIgnoreCase("NA")) {
						Employee emp = getEmployee(listofemployee, exceldatalist.get(p).trim());
						String strempbranch = validateEmployeeBranch(emp.getBranchName(), exceldatalist.get(p + 47),
								projectList);
						if (strempbranch != null && !strempbranch.equals("")) {
							error += strempbranch;
						}
					} else {
						error += serverapputility.validateStringCloumn(exceldatalist.get(2).trim(),
								exceldatalist.get(p + 2).trim());

						String strempbranch = validateEmployeeBranch(exceldatalist.get(p + 2),
								exceldatalist.get(p + 47), projectList);
						if (strempbranch != null && !strempbranch.equals("")) {
							error += strempbranch;
						}
					}

					if (!exceldatalist.get(p + 47).trim().equalsIgnoreCase("NA")
							|| !exceldatalist.get(p + 48).trim().equalsIgnoreCase("NA")) {
						String strEmpCal = validateEmployeeCalender(exceldatalist.get(p).trim(), employeeInfolist,
								true);
						if (!strEmpCal.equals("")) {
							error += strEmpCal;
						}
					}

					if (!exceldatalist.get(p + 47).trim().equalsIgnoreCase("NA")) {
						HrProject project = getProjectName(exceldatalist.get(p + 47).trim(), projectList);
						if (project == null) {
							String strEmp1 = "Project Name does not found in the system!"
									+ exceldatalist.get(p + 47).trim() + ". ";
							error += strEmp1;
						}

					}

					if (!exceldatalist.get(p + 48).trim().equalsIgnoreCase("NA")) {

						HrProject project = getProjectName(exceldatalist.get(p + 47).trim(), projectList);
						if (project != null && project.getState() == null || project.getState().equals("")) {
							String strEmp1 = "No state details found in project. " + exceldatalist.get(p + 47).trim()
									+ ". ";
							error += strEmp1;
						}
						if (siteLocationList.size() == 0) {

							String strEmp1 = "Site location does not exist. " + exceldatalist.get(p + 48).trim() + ". ";
							error += strEmp1;
						}
						int custId = 0;
						if (project.getPersoninfo() != null) {
							custId = project.getPersoninfo().getCount();
						}

						if (!validateSiteLocation(siteLocationList, exceldatalist.get(p + 48).trim(),
								project.getState(), custId)) {

							String strEmp1 = "Site location does not exist. " + exceldatalist.get(p + 48).trim() + " / "
									+ project.getState() + ". ";
							error += strEmp1;
						}
					}
				}

				error += serverapputility.validateNumericCloumn(exceldatalist.get(3).trim(),
						exceldatalist.get(p + 3).trim());

				error += serverapputility.validateNumericCloumn(exceldatalist.get(4).trim(),
						exceldatalist.get(p + 4).trim());

				error += serverapputility.validateSpecialCharComma(exceldatalist.get(5).trim(),
						exceldatalist.get(p + 5).trim());

				if (!exceldatalist.get(p + 5).trim().equalsIgnoreCase("NA")) {
					error += serverapputility.validateConfigsNames(empDesignationlist, exceldatalist.get(p + 5).trim(),
							exceldatalist.get(5).trim());
				}
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(6).trim(),
						exceldatalist.get(p + 6).trim());

				if (!exceldatalist.get(p + 6).trim().equalsIgnoreCase("NA")) {
					error += serverapputility.validateConfigsNames(emprolelist, exceldatalist.get(p + 6).trim(),
							exceldatalist.get(6).trim());
					error += serverapputility.validateSpecialCharComma(exceldatalist.get(6).trim(),
							exceldatalist.get(p + 6).trim());

				}

				if (!exceldatalist.get(p + 7).trim().equalsIgnoreCase("NA")) {

					Date joinDate = null;
					try {
						SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
						joinDate = fmt.parse((exceldatalist.get(p + 7).trim()));

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						error += exceldatalist.get(7).trim()
								+ " Column Invalid date format. Date format must be MM/DD/YYYY ";
						logger.log(Level.SEVERE, exceldatalist.get(7).trim()
								+ " Column Invalid date format. Date format must be MM/DD/YYYY");
					}
					logger.log(Level.SEVERE, "value $ == "
							+ serverapputility.isDateValid(exceldatalist.get(p + 7).trim(), "MM/dd/yyyy"));

					if (!serverapputility.isDateValid(exceldatalist.get(p + 7).trim(), "MM/dd/yyyy")) {
						error += exceldatalist.get(7).trim() + " Column Invalid date.";
					}
				}

				error += serverapputility.validateSpecialCharComma(exceldatalist.get(8).trim(),
						exceldatalist.get(p + 8).trim());

				if (!exceldatalist.get(p + 8).trim().equalsIgnoreCase("NA")) {
					if (!validateEmployeeMartialStatus(martialstatus, exceldatalist.get(p + 8).trim())) {
						error += exceldatalist.get(p + 8) + " - " + exceldatalist.get(8).trim()
								+ " does not exist in the system. ";
					}
				}
				if (!exceldatalist.get(p + 9).trim().equalsIgnoreCase("NA")) {
					error += serverapputility.validateEmployeeName(allEmployeelist, exceldatalist.get(p + 9).trim(),
							exceldatalist.get(9).trim());
				}

				error += serverapputility.validateSpecialCharComma(exceldatalist.get(9).trim(),
						exceldatalist.get(p + 9).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(10).trim(),
						exceldatalist.get(p + 10).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(11).trim(),
						exceldatalist.get(p + 11).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(12).trim(),
						exceldatalist.get(p + 12).trim());

				if (!exceldatalist.get(p + 13).trim().equals("NA")) {
					try {
						boolean status = Boolean.parseBoolean(exceldatalist.get(p + 13).trim());
					} catch (Exception e) {
						error += exceldatalist.get(13).trim() + "Column should be True or False value. ";
					}
					error += serverapputility.validateSpecialCharComma(exceldatalist.get(13).trim(),
							exceldatalist.get(p + 13).trim());

				}
				error += serverapputility.validateStringCloumn(exceldatalist.get(14).trim(),
						exceldatalist.get(p + 14).trim());
				error += serverapputility.validateStringCloumn(exceldatalist.get(15).trim(),
						exceldatalist.get(p + 15).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(16).trim(),
						exceldatalist.get(p + 16).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(17).trim(),
						exceldatalist.get(p + 17).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(18).trim(),
						exceldatalist.get(p + 18).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(19).trim(),
						exceldatalist.get(p + 20).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(20).trim(),
						exceldatalist.get(p + 20).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(21).trim(),
						exceldatalist.get(p + 21).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(22).trim(),
						exceldatalist.get(p + 22).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(23).trim(),
						exceldatalist.get(p + 23).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(24).trim(),
						exceldatalist.get(p + 24).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(25).trim(),
						exceldatalist.get(p + 25).trim());

				error += serverapputility.validateEmailId(exceldatalist.get(26).trim(),
						exceldatalist.get(p + 26).trim());

				error += serverapputility.validateSpecialCharComma(exceldatalist.get(27).trim(),
						exceldatalist.get(p + 27).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(28).trim(),
						exceldatalist.get(p + 28).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(29).trim(),
						exceldatalist.get(p + 29).trim());
				error += serverapputility.validateStringCloumn(exceldatalist.get(30).trim(),
						exceldatalist.get(p + 30).trim());

				if (!exceldatalist.get(p + 30).trim().equalsIgnoreCase("NA")) {
					error += serverapputility.validateCountryName(globalCountrylist, exceldatalist.get(p + 30).trim(),
							exceldatalist.get(30).trim());

				}
				if (!exceldatalist.get(p + 31).trim().equalsIgnoreCase("NA")) {
					error += serverapputility.validateStateName(globalStatelist, exceldatalist.get(p + 31).trim(),
							exceldatalist.get(p + 30), exceldatalist.get(31).trim());

				}
				if (!exceldatalist.get(p + 32).trim().equalsIgnoreCase("NA")) {
					error += serverapputility.validateSpecialCharComma(exceldatalist.get(32).trim(),
							exceldatalist.get(p + 32).trim());
				}

				if (!exceldatalist.get(p + 33).trim().equalsIgnoreCase("NA")) {
					error += serverapputility.validateLocalityName(globalLocalitylist, exceldatalist.get(p + 33).trim(),
							exceldatalist.get(p + 32).trim(), exceldatalist.get(33).trim());

				}

				error += serverapputility.validateStringCloumn(exceldatalist.get(31).trim(),
						exceldatalist.get(p + 31).trim());
				error += serverapputility.validateStringCloumn(exceldatalist.get(32).trim(),
						exceldatalist.get(p + 32).trim());
				error += serverapputility.validateStringCloumn(exceldatalist.get(33).trim(),
						exceldatalist.get(p + 33).trim());
				error += serverapputility.validateStringCloumn(exceldatalist.get(33).trim(),
						exceldatalist.get(p + 33).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(34).trim(),
						exceldatalist.get(p + 34).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(35).trim(),
						exceldatalist.get(p + 35).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(36).trim(),
						exceldatalist.get(p + 36).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(37).trim(),
						exceldatalist.get(p + 37).trim());
				error += serverapputility.validateStringCloumn(exceldatalist.get(38).trim(),
						exceldatalist.get(p + 38).trim());
				error += serverapputility.validateStringCloumn(exceldatalist.get(39).trim(),
						exceldatalist.get(p + 39).trim());
				error += serverapputility.validateStringCloumn(exceldatalist.get(40).trim(),
						exceldatalist.get(p + 40).trim());
				error += serverapputility.validateStringCloumn(exceldatalist.get(41).trim(),
						exceldatalist.get(p + 41).trim());

				if (!exceldatalist.get(p + 38).trim().equalsIgnoreCase("NA")) {
					error += serverapputility.validateCountryName(globalCountrylist, exceldatalist.get(p + 38).trim(),
							exceldatalist.get(38).trim());

				}
				if (!exceldatalist.get(p + 39).trim().equalsIgnoreCase("NA")) {
					error += serverapputility.validateStateName(globalStatelist, exceldatalist.get(p + 39).trim(),
							exceldatalist.get(p + 39), exceldatalist.get(39).trim());

				}
				if (!exceldatalist.get(p + 40).trim().equalsIgnoreCase("NA")) {
					error += serverapputility.validateSpecialCharComma(exceldatalist.get(40).trim(),
							exceldatalist.get(p + 40).trim());
				}

				if (!exceldatalist.get(p + 41).trim().equalsIgnoreCase("NA")) {
					error += serverapputility.validateLocalityName(globalLocalitylist, exceldatalist.get(p + 41).trim(),
							exceldatalist.get(p + 40).trim(), exceldatalist.get(41).trim());

				}

				error += serverapputility.validateNumericCloumn(exceldatalist.get(42).trim(),
						exceldatalist.get(p + 42).trim());
				error += serverapputility.validateStringCloumn(exceldatalist.get(43).trim(),
						exceldatalist.get(p + 43).trim());

				if (!exceldatalist.get(p + 43).trim().equalsIgnoreCase("NA")) {
					if (!validateEmployeeGender(genderlist, exceldatalist.get(p + 43).trim())) {
						error += exceldatalist.get(p + 43) + " - " + exceldatalist.get(43).trim()
								+ " does not exist in the system. ";
					}
				}

				error += serverapputility.validateStringCloumn(exceldatalist.get(44).trim(),
						exceldatalist.get(p + 44).trim());

				if (!exceldatalist.get(p + 44).trim().equalsIgnoreCase("NA")) {
					error += serverapputility.validateConfigsNames(numberRangelist, exceldatalist.get(p + 44).trim(),
							exceldatalist.get(44).trim());

				}

				error += serverapputility.validateStringCloumn(exceldatalist.get(45).trim(),
						exceldatalist.get(p + 45).trim());

				if (!exceldatalist.get(p + 45).trim().equalsIgnoreCase("NA")) {
					if (!validateEmployeeGroup(employeeGrouplist, exceldatalist.get(p + 45).trim())) {
						error += exceldatalist.get(p + 45) + " - " + exceldatalist.get(45).trim()
								+ " does not exist in the system. ";
					}
				}
				error += serverapputility.validateStringCloumn(exceldatalist.get(46).trim(),
						exceldatalist.get(p + 46).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(47).trim(),
						exceldatalist.get(p + 47).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(48).trim(),
						exceldatalist.get(p + 48).trim());
				// Added by Priyanka
				if (!exceldatalist.get(p + 49).trim().equalsIgnoreCase("NA")) {
					error += serverapputility.validateYesNo(exceldatalist.get(49).trim(),
							exceldatalist.get(p + 49).trim());
				}

				// Ashwini Patil Date:29-09-2023
				if (!exceldatalist.get(p + 50).trim().equalsIgnoreCase("NA")) {

					Date birthDate = null;
					try {
						SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
						birthDate = fmt.parse((exceldatalist.get(p + 50).trim()));

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						error += exceldatalist.get(50).trim()
								+ " Column Invalid date format. Date format must be MM/DD/YYYY ";
						logger.log(Level.SEVERE, exceldatalist.get(50).trim()
								+ " Column Invalid date format. Date format must be MM/DD/YYYY");
					}
					logger.log(Level.SEVERE, "value $ == "
							+ serverapputility.isDateValid(exceldatalist.get(p + 50).trim(), "MM/dd/yyyy"));

					if (!serverapputility.isDateValid(exceldatalist.get(p + 50).trim(), "MM/dd/yyyy")) {
						error += exceldatalist.get(50).trim() + " Column Invalid date.";
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				error += " There may be blank cell in the Column Please Check. Blank not allowed!. OR There may be some formula used in some column. Formula value column not allowed! OR May be hiden column is there please check ";
			}

			if (!error.equals("")) {
				errorList.add(exceldatalist.get(p));
				errorList.add(error);
			}

		}

		CsvWriter.updatedEmployeeList = errorList;
		if (errorList != null && errorList.size() != 0) {
			integerlist.add(-1);
			return integerlist;
		}

		/**
		 * @author Vijay Date :- 09-10-2020 Des :- Updated code into task queue to
		 *         update large employee data
		 */

		String msg = updateEmployeeData(companyId);

		logger.log(Level.SEVERE, "update employee " + msg);
		integerlist.add(0);

		return integerlist;

	}

	private ArrayList<Integer> customersSave(long companyId, ArrayList<String> exceldatalist) {

		ArrayList<Customer> customerlist = new ArrayList<Customer>();

		int fileRecords;
		int insertedRecords;
		int lastcount;
		int newcount;

		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		List<Customer> listofcustomer = ofy().load().type(Customer.class).filter("companyId", companyId).list();

		// if(entityname.equalsIgnoreCase("Customer")){

		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId).filter("processName", "Customer")
				.filter("status", true).first().now();

		long number = ng.getNumber();
		int count = (int) number;

		for (int p = 41; p < exceldatalist.size(); p += 41) {
			Customer customer = new Customer();

			// boolean customercheck = validationforcustomer(listofcustomer,
			// exceldatalist, p);
			// System.out.println("the value of customrecheck is: "
			// + customercheck);
			// if (customercheck == false) {
			// integerlist.add(-5);
			// return integerlist;
			// }

			/***************** changes by vijay ***************************/

			boolean customercheck = true;
			boolean clientcheck = true;

			if (exceldatalist.get(p).equalsIgnoreCase("yes")) {
				clientcheck = validationforclientcompnay(listofcustomer, exceldatalist, p);
			} else {
				customercheck = validationforcustomer(listofcustomer, exceldatalist, p);
			}

			if (customercheck == false) {
				integerlist.add(-5);
				return integerlist;
			}

			if (clientcheck == false) {
				integerlist.add(-5);
				return integerlist;
			}
			/***************** chnages end here **************************/

			if (exceldatalist.get(p).trim().equalsIgnoreCase("NO")
					|| exceldatalist.get(p).trim().equalsIgnoreCase("NA")) {
				customer.setCompany(false);
			} else {
				customer.setCompany(true);
			}

			if (exceldatalist.get(p + 1).trim().equalsIgnoreCase("na")) {
				customer.setCompanyName("");
			} else {
				String companyName = exceldatalist.get(p + 1).toUpperCase();
				customer.setCompanyName(companyName);
			}

			try {

				if (exceldatalist.get(p + 2).trim().equalsIgnoreCase("NA")) {
					// customer.setDob(fmt.parse(""));
				} else {
					customer.setDob(fmt.parse(exceldatalist.get(p + 2)));
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}

			// rohan added salutation for NBHC in customer Dat : 7/2/2017
			if (exceldatalist.get(p + 3).trim().equalsIgnoreCase("na")) {
				customer.setSalutation("");
			} else {
				customer.setSalutation(exceldatalist.get(p + 3).trim());
			}

			if (exceldatalist.get(p + 4).trim().equalsIgnoreCase("NA")) {
				customer.setFullname();

			} else {
				String fullname = exceldatalist.get(p + 4).trim();
				System.out.println("Fname is  :" + fullname);
				customer.setFullname(fullname.toUpperCase());
				System.out.println("fname.toUpperCase()" + fullname.toUpperCase());
			}
			// if (exceldatalist.get(p + 4).trim().equalsIgnoreCase("NA")) {
			// customer.setMiddleName("");
			//
			// } else {
			// String mname=exceldatalist.get(p + 4).trim();
			// customer.setMiddleName(mname.toUpperCase());
			// }
			// if (exceldatalist.get(p + 5).trim().equalsIgnoreCase("na")) {
			// customer.setLastName("");
			// } else {
			// String lname=exceldatalist.get(p + 5).trim();
			// customer.setLastName(lname.toUpperCase());
			// }
			if (exceldatalist.get(p + 5).trim().equalsIgnoreCase("na")) {
				customer.setEmail("");

			} else {
				customer.setEmail(exceldatalist.get(p + 5));
			}
			if (exceldatalist.get(p + 6).trim().equalsIgnoreCase("na")) {
				customer.setLandLine(0l);
			} else {
				customer.setLandLine(Long.parseLong(exceldatalist.get(p + 6)));
			}
			if (exceldatalist.get(p + 7).trim().equalsIgnoreCase("NA")) {
				customer.setCellNumber1(0l);
			} else {
				customer.setCellNumber1(Long.parseLong(exceldatalist.get(p + 7)));
			}

			if (exceldatalist.get(p + 8).trim().equalsIgnoreCase("na")) {
				customer.setCellNumber2(0l);
			} else {
				customer.setCellNumber2(Long.parseLong(exceldatalist.get(p + 8)));
			}
			if (exceldatalist.get(p + 9).trim().equalsIgnoreCase("na")) {
				customer.setFaxNumber(0l);
			} else {
				customer.setFaxNumber(Long.parseLong(exceldatalist.get(p + 9)));
			}
			if (exceldatalist.get(p + 10).trim().equalsIgnoreCase("na")) {
				customer.setBranch("");
			} else {
				customer.setBranch(exceldatalist.get(p + 10));
			}
			if (exceldatalist.get(p + 11).trim().equalsIgnoreCase("na")) {
				customer.setEmployee("");
			} else {
				customer.setEmployee(exceldatalist.get(p + 11));
			}
			if (exceldatalist.get(p + 12).trim().equalsIgnoreCase("na")) {
				customer.setGroup("");
			} else {
				customer.setGroup(exceldatalist.get(p + 12));
			}
			if (exceldatalist.get(p + 13).trim().equalsIgnoreCase("na")) {
				customer.setCategory("");
			} else {
				customer.setCategory(exceldatalist.get(p + 13));
			}
			if (exceldatalist.get(p + 14).trim().equalsIgnoreCase("na")) {
				customer.setType("");
			} else {
				customer.setType(exceldatalist.get(p + 14));
			}
			if (exceldatalist.get(p + 15).trim().equalsIgnoreCase("na")) {
				customer.setCustomerPriority("");
			} else {
				customer.setCustomerPriority(exceldatalist.get(p + 15));
			}
			if (exceldatalist.get(p + 16).trim().equalsIgnoreCase("na")) {
				customer.setCustomerLevel("");
			} else {
				customer.setCustomerLevel(exceldatalist.get(p + 16));
			}
			if (exceldatalist.get(p + 17).trim().equalsIgnoreCase("na")) {
				customer.setRefrNumber1("");
			} else {
				customer.setRefrNumber1(exceldatalist.get(p + 17));
			}
			if (exceldatalist.get(p + 18).trim().equalsIgnoreCase("na")) {
				customer.setRefrNumber2("");
			} else {
				customer.setRefrNumber2(exceldatalist.get(p + 18).trim());
			}
			if (exceldatalist.get(p + 19).trim().equalsIgnoreCase("na")) {
				customer.setWebsite("");
			} else {
				customer.setWebsite(exceldatalist.get(p + 19));
			}
			if (exceldatalist.get(p + 20).trim().equalsIgnoreCase("NA")) {
				customer.setDescription("");
			} else {
				customer.setDescription(exceldatalist.get(p + 20));
			}

			SocialInformation socinfo = new SocialInformation();

			if (exceldatalist.get(p + 21).trim().equalsIgnoreCase("NA")) {
				socinfo.setGooglePlusId("");
			} else {
				socinfo.setGooglePlusId(exceldatalist.get(p + 21));
			}
			if (exceldatalist.get(p + 22).trim().equalsIgnoreCase("na")) {
				socinfo.setFaceBookId("");
			} else {
				socinfo.setFaceBookId(exceldatalist.get(p + 22));
			}
			if (exceldatalist.get(p + 23).trim().equalsIgnoreCase("na")) {
				socinfo.setTwitterId("");
			} else {
				socinfo.setTwitterId(exceldatalist.get(p + 23));
			}
			customer.setSocialInfo(socinfo);

			Address address = new Address();

			if (exceldatalist.get(p + 24).trim().equalsIgnoreCase("na")) {
				address.setAddrLine1("");
			} else {
				address.setAddrLine1(exceldatalist.get(p + 24));
			}
			if (exceldatalist.get(p + 25).trim().equalsIgnoreCase("na")) {
				address.setAddrLine2("");
			} else {
				address.setAddrLine2(exceldatalist.get(p + 25));
			}
			if (exceldatalist.get(p + 26).trim().equalsIgnoreCase("na")) {
				address.setLandmark("");
			} else {
				address.setLandmark(exceldatalist.get(p + 26));
			}
			if (exceldatalist.get(p + 27).trim().equalsIgnoreCase("na")) {
				address.setCountry("");
			} else {
				address.setCountry(exceldatalist.get(p + 27));
			}
			if (exceldatalist.get(p + 28).trim().equalsIgnoreCase("na")) {
				address.setState("");
			} else {
				address.setState(exceldatalist.get(p + 28));
			}
			if (exceldatalist.get(p + 29).trim().equalsIgnoreCase("na")) {
				address.setCity("");
			} else {
				address.setCity(exceldatalist.get(p + 29));
			}
			if (exceldatalist.get(p + 30).trim().equalsIgnoreCase("na")) {
				address.setLocality("");
			} else {
				address.setLocality(exceldatalist.get(p + 30));
			}
			if (exceldatalist.get(p + 31).trim().equalsIgnoreCase("na")) {
				address.setPin(0l);
			} else {
				address.setPin(Long.parseLong(exceldatalist.get(p + 31)));
			}
			customer.setAdress(address);

			Address address2 = new Address();
			if (exceldatalist.get(p + 32).trim().equalsIgnoreCase("na")) {
				address2.setAddrLine1("");
			} else {
				address2.setAddrLine1(exceldatalist.get(p + 32));
			}
			if (exceldatalist.get(p + 33).trim().equalsIgnoreCase("na")) {
				address2.setAddrLine2("");
			} else {
				address2.setAddrLine2(exceldatalist.get(p + 33));
			}
			if (exceldatalist.get(p + 34).trim().equalsIgnoreCase("na")) {
				address2.setLandmark("");
			} else {
				address2.setLandmark(exceldatalist.get(p + 34));
			}
			if (exceldatalist.get(p + 35).trim().equalsIgnoreCase("na")) {
				address2.setCountry("");
			} else {
				address2.setCountry(exceldatalist.get(p + 35));
			}
			if (exceldatalist.get(p + 36).trim().equalsIgnoreCase("na")) {
				address2.setState("");
			} else {
				address2.setState(exceldatalist.get(p + 36));
			}
			if (exceldatalist.get(p + 37).trim().equalsIgnoreCase("na")) {
				address2.setCity("");
			} else {
				address2.setCity(exceldatalist.get(p + 37));
			}
			if (exceldatalist.get(p + 38).trim().equalsIgnoreCase("")) {
				address2.setLocality("");
			} else {
				address2.setLocality(exceldatalist.get(p + 38));
			}
			if (exceldatalist.get(p + 39).trim().equalsIgnoreCase("na")) {
				address2.setPin(0l);
			} else {
				address2.setPin(Long.parseLong(exceldatalist.get(p + 39)));
			}
			customer.setSecondaryAdress(address2);

			ArrayList<ArticleType> articletypelist = new ArrayList<ArticleType>();
			ArticleType articletype = new ArticleType();

			if (exceldatalist.get(p + 40).equalsIgnoreCase("na")) {
				articletype.setArticleTypeValue("");
			} else {
				articletype.setArticleTypeValue(exceldatalist.get(p + 40));
			}

			articletype.setDocumentName("Invoice Detail");
			articletype.setArticleTypeName("GSTIN");
//			 articletype.setArticleDescription("");
			articletype.setArticlePrint("Yes");

			articletypelist.add(articletype);
			customer.setArticleTypeDetails(articletypelist);

			customer.setCount(++count);

			customer.setCompanyId(companyId);

			customerlist.add(customer);

		}

		lastcount = ofy().load().type(Customer.class).filter("companyId", companyId).count();
		System.out.println("LastCount" + lastcount);

		ofy().save().entities(customerlist).now();

		ng.setNumber(count);
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(ng);

		System.out.println("Data saved successfuly=======================================");
		logger.log(Level.SEVERE, "Data saved successfuly=======================================");

		newcount = ofy().load().type(Customer.class).filter("companyId", companyId).count();
		System.out.println("New Count" + newcount);

		System.out.println("number@@@" + ng.getNumber());

		fileRecords = customerlist.size();
		insertedRecords = newcount - lastcount;
		System.out.println("Result:" + insertedRecords);
		integerlist.add(fileRecords);
		integerlist.add(insertedRecords);
		System.out.println("SIZE:" + customerlist.size());

		// }
		return integerlist;

	}

	private boolean validationforcustomer(List<Customer> listofcustomer, ArrayList<String> exceldatalist, int p) {

		// // TODO Auto-generated method stub
		// boolean value = true;
		//
		// for (int i = 0; i < listofcustomer.size(); i++) {
		// if ((listofcustomer.get(i).getFirstName().trim()
		// .equalsIgnoreCase(exceldatalist.get(p + 3).trim()))
		// && (listofcustomer.get(i).getMiddleName().trim()
		// .equalsIgnoreCase(exceldatalist.get(p + 4).trim()))
		// && (listofcustomer.get(i).getLastName().trim()
		// .equalsIgnoreCase(exceldatalist.get(p + 5).trim()))
		// || (listofcustomer.get(i).getFirstName().trim()
		// .equalsIgnoreCase(exceldatalist.get(p + 3).trim()) && (listofcustomer
		// .get(i).getLastName().trim()
		// .equalsIgnoreCase(exceldatalist.get(p + 5).trim())))) {
		// value = false;
		// }
		// }
		//
		// return value;

		boolean value = true;

		for (int i = 0; i < listofcustomer.size(); i++) {
			if (!listofcustomer.get(i).getFullname().trim().equals("")
					&& listofcustomer.get(i).getFullname().trim() != null) {
				if (listofcustomer.get(i).getFullname().trim().equalsIgnoreCase(exceldatalist.get(p + 4).trim())// Ashwini
																												// Patil
																												// Date:22-12-2022
						&& listofcustomer.get(i).isCompany() == false) {
					return false;
				}
			}
		}

		return value;
	}

	private ArrayList<Integer> branchSave(long companyId, ArrayList<String> exceldatalist) {

		int fileRecords;
		int insertedRecords;
		int lastCount;
		int newCount;

		ArrayList<Integer> integerlist = new ArrayList<Integer>();

		ArrayList<Branch> branchlist = new ArrayList<Branch>();

		List<Branch> listofbranch = ofy().load().type(Branch.class).filter("companyId", companyId).list();
		System.out.println(listofbranch);

		NumberGeneration numgenration = new NumberGeneration();
		numgenration = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
				.filter("processName", "Branch").filter("status", true).first().now();

		long number = numgenration.getNumber();
		int count = (int) number;

		for (int i = 19; i < exceldatalist.size(); i += 19) {

			Branch branch = new Branch();

			boolean validcheck = validationforbranch(listofbranch, exceldatalist, i);

			System.out.println("The value of validcheck is : " + validcheck);

			if (validcheck == false) {
				integerlist.add(-2);
				System.out.println("Return Ho Gaya");
				return integerlist;
			}

			if (exceldatalist.get(i).equalsIgnoreCase("na")) {
				branch.setBusinessUnitName("");

			} else {
				branch.setBusinessUnitName(exceldatalist.get(i));
			}
			if (exceldatalist.get(i + 1).equalsIgnoreCase("na")) {
				branch.setEmail("");

			} else {
				branch.setEmail(exceldatalist.get(i + 1));
			}
			if (exceldatalist.get(i + 2).equalsIgnoreCase("No")) {
				branch.setstatus(false);

			} else {
				branch.setstatus(true);
			}

			if (exceldatalist.get(i + 3).equalsIgnoreCase("na")) {
				branch.setLandline(0l);

			} else {
				branch.setLandline(Long.parseLong(exceldatalist.get(i + 3)));

			}
			if (exceldatalist.get(i + 4).equalsIgnoreCase("na")) {
				branch.setCellNumber1(0l);
			} else {

				branch.setCellNumber1(Long.parseLong(exceldatalist.get(i + 4)));
			}
			if (exceldatalist.get(i + 5).equalsIgnoreCase("na")) {
				branch.setCellNumber2(0l);
			} else {

				branch.setCellNumber2(Long.parseLong(exceldatalist.get(i + 5)));
			}

			if (exceldatalist.get(i + 6).equalsIgnoreCase("na")) {
				branch.setFaxNumber(0l);
			} else {

				branch.setFaxNumber(Long.parseLong(exceldatalist.get(i + 6)));
			}

			if (exceldatalist.get(i + 7).equalsIgnoreCase("na")) {
				branch.setPocName("");
			} else {

				branch.setPocName(exceldatalist.get(i + 7));
			}
			if (exceldatalist.get(i + 8).equalsIgnoreCase("na")) {
				branch.setPocLandline(0l);
			} else {

				branch.setPocLandline(Long.parseLong(exceldatalist.get(i + 8)));
			}
			if (exceldatalist.get(i + 9).equalsIgnoreCase("na")) {
				branch.setPocCell(0l);
			} else {

				branch.setPocCell(Long.parseLong(exceldatalist.get(i + 9)));
			}
			if (exceldatalist.get(i + 10).equalsIgnoreCase("na")) {
				branch.setPocEmail("");
			} else {

				branch.setPocEmail(exceldatalist.get(i + 10));
			}

			Address address = new Address();
			if (exceldatalist.get(i + 11).equalsIgnoreCase("na")) {
				address.setAddrLine1("");
			} else {
				address.setAddrLine1(exceldatalist.get(i + 11));
			}
			if (exceldatalist.get(i + 12).equalsIgnoreCase("na")) {
				address.setAddrLine2("");
			} else {
				address.setAddrLine2(exceldatalist.get(i + 12));
			}
			if (exceldatalist.get(i + 13).equalsIgnoreCase("na")) {
				address.setLandmark("");
			} else {
				address.setLandmark(exceldatalist.get(i + 13));
			}
			if (exceldatalist.get(i + 14).equalsIgnoreCase("na")) {
				address.setCountry("");
			} else {
				address.setCountry(exceldatalist.get(i + 14));
			}
			if (exceldatalist.get(i + 15).equalsIgnoreCase("na")) {
				address.setState("");
			} else {
				address.setState(exceldatalist.get(i + 15));
			}
			if (exceldatalist.get(i + 16).equalsIgnoreCase("na")) {
				address.setCity("");
			} else {
				address.setCity(exceldatalist.get(i + 16));
			}
			if (exceldatalist.get(i + 17).equalsIgnoreCase("na")) {
				address.setLocality("");
			} else {
				address.setLocality(exceldatalist.get(i + 17));
			}
			if (exceldatalist.get(i + 18).equalsIgnoreCase("na")) {
				address.setPin(0l);
			} else {
				address.setPin(Long.parseLong(exceldatalist.get(i + 18)));
			}
			branch.setAddress(address);

			branch.setCount(++count);
			branch.setCompanyId(companyId);
			branchlist.add(branch);
			System.out.println("Still inside loop");
		}

		System.out.println("Now Out of loop");
		lastCount = ofy().load().type(Branch.class).filter("companyId", companyId).count();
		System.out.println("LastCount" + lastCount);

		ofy().save().entities(branchlist).now();

		numgenration.setNumber(count);
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(numgenration);

		System.out.println("Data saved successfuly=======================================");
		logger.log(Level.SEVERE, "Data saved successfuly=======================================");

		newCount = ofy().load().type(Branch.class).filter("companyId", companyId).count();
		System.out.println("New Count" + newCount);

		System.out.println("number@@@" + numgenration.getNumber());

		fileRecords = branchlist.size();
		insertedRecords = newCount - lastCount;
		System.out.println("Result:" + insertedRecords);
		integerlist.add(fileRecords);
		integerlist.add(insertedRecords);
		System.out.println("SIZE:" + branchlist.size());

		return integerlist;
	}

	private boolean validationforbranch(List<Branch> listofbranch, ArrayList<String> exceldatalist, int i) {
		// TODO Auto-generated method stub
		boolean value = true;

		for (int j = 0; j < listofbranch.size(); j++) {
			System.out.println("The Branch Name is: " + listofbranch.get(j).getBusinessUnitName());
			System.out.println("The Input provided is: " + exceldatalist.get(i));
			if (listofbranch.get(j).getBusinessUnitName().trim().equalsIgnoreCase(exceldatalist.get(i).trim())) {
				System.out.println("Inside if of validationforbranch method");
				value = false;
				System.out.println("The Value contains at stage 1: " + value);
			}
		}
		System.out.println("The Value contains at stage 2 : " + value);
		return value;

	}

	public ArrayList<Integer> serviceproductsave(long companyId, ArrayList<String> exceldatalist) {
		// TODO Auto-generated method stub

		ArrayList<ServiceProduct> serviceproductlist = new ArrayList<ServiceProduct>();

		int filerecords;
		int insertedRecord;
		int lastcount;
		int newcount;

		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		List<ServiceProduct> listofserviceproduct = ofy().load().type(ServiceProduct.class)
				.filter("companyId", companyId).list();

		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
				.filter("processName", "ServiceProduct").filter("status", true).first().now();
		// Getting product number from entity data created in datastore
		long number = ng.getNumber();

		int count = (int) number;

		List<TaxDetails> taxlist = ofy().load().type(TaxDetails.class).filter("companyId", companyId).list();
		ServerAppUtility serverapputility = new ServerAppUtility();

		try {

			for (int i = 20; i < exceldatalist.size(); i += 20) {

				ServiceProduct serviceproductobj = new ServiceProduct();

//			boolean serviceproductcheck = validationforserviceproduct(
//					listofserviceproduct, exceldatalist, i);
//
//			if (serviceproductcheck == false) {
//				integerlist.add(-10);
//				return integerlist;
//			}

				if (exceldatalist.get(i).equalsIgnoreCase("Na")) {
					serviceproductobj.setDuration(0);
				} else {
					serviceproductobj.setDuration(Integer.parseInt(exceldatalist.get(i)));
				}

				if (exceldatalist.get(i + 1).equalsIgnoreCase("NA")) {
					serviceproductobj.setNumberOfServices(0);
				} else {
					serviceproductobj.setNumberOfService(Integer.parseInt(exceldatalist.get(i + 1)));
				}

				if (exceldatalist.get(i + 2).equalsIgnoreCase("No")) {
					serviceproductobj.setStatus(false);
				} else {
					serviceproductobj.setStatus(true);
				}

				if (exceldatalist.get(i + 3).equalsIgnoreCase("Na")) {
					serviceproductobj.setProductCode(" ");
				} else {
					serviceproductobj.setProductCode(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).equalsIgnoreCase("NA")) {
					serviceproductobj.setProductCategory(" ");
				} else {
					serviceproductobj.setProductCategory(exceldatalist.get(i + 4));
				}

				if (exceldatalist.get(i + 5).equalsIgnoreCase("Na")) {
					serviceproductobj.setProductName("");
				} else {
					serviceproductobj.setProductName(exceldatalist.get(i + 5));
				}

				if (exceldatalist.get(i + 6).equalsIgnoreCase("Na")) {
					serviceproductobj.setSpecifications("");
				} else {
					serviceproductobj.setSpecifications(exceldatalist.get(i + 6));
				}

				if (exceldatalist.get(i + 7).equalsIgnoreCase("Na")) {
					serviceproductobj.setProductGroup(" ");
				} else {
					serviceproductobj.setProductGroup(exceldatalist.get(i + 7));
				}

				if (exceldatalist.get(i + 8).equalsIgnoreCase("Na")) {
					serviceproductobj.setProductType("");
				} else {
					serviceproductobj.setProductType(exceldatalist.get(i + 8));
				}

				if (exceldatalist.get(i + 9).equalsIgnoreCase("Na")) {
					serviceproductobj.setProductClassification("");
				} else {
					serviceproductobj.setProductClassification(exceldatalist.get(i + 9));
				}

				if (exceldatalist.get(i + 10).equalsIgnoreCase("Na")) {
					serviceproductobj.setRefNumber1("");
				} else {
					serviceproductobj.setRefNumber1(exceldatalist.get(i + 10));
				}

				if (exceldatalist.get(i + 11).equalsIgnoreCase("Na")) {
					serviceproductobj.setRefNumber2("");
				} else {
					serviceproductobj.setRefNumber2(exceldatalist.get(i + 11));
				}

				if (exceldatalist.get(i + 12).equalsIgnoreCase("Na")) {
					serviceproductobj.setPrice(0);
					;
				} else {
					serviceproductobj.setPrice(Double.parseDouble(exceldatalist.get(i + 12)));
				}

				if (exceldatalist.get(i + 13).equalsIgnoreCase("Na")) {

					/**
					 * Date : 12-10-2017 BY ANIL
					 */
					Tax tax = new Tax();
					tax.setTaxName("NA");
					tax.setTaxConfigName("NA");
					tax.setPercentage(0d);
					serviceproductobj.setServiceTax(tax);

				} else {
//				/**
//				 * Date : 12-10-2017 BY ANIL
//				 */
//				/**Date 9-12-2019 by AMol added a @taxpercent raised  by Rahul**/
//				
//				String taxPercent=getTaxPercentForName(Double.parseDouble(exceldatalist.get(i + 13)));
//				Tax tax = new Tax();
//				tax.setTaxName("CGST@"+taxPercent);
//				tax.setTaxPrintName("CGST");
//				tax.setTaxConfigName("CGST@"+taxPercent);
//				serviceproductobj.setStatus(true);
//				tax.setPercentage(Double.parseDouble(exceldatalist.get(i + 13)));
//				serviceproductobj.setServiceTax(tax);

					/**
					 * @author Vijay Date 03-02-2021 Des :- updated code as per Tax Name tax details
					 *         will map
					 */
					TaxDetails taxdetails = serverapputility.gettaxDetails(taxlist, exceldatalist.get(i + 13));
					if (taxdetails != null) {
						Tax tax = new Tax();
						tax.setTaxName(taxdetails.getTaxChargeName());
						tax.setTaxPrintName(taxdetails.getTaxPrintName());
						tax.setTaxConfigName(taxdetails.getTaxChargeName());
						serviceproductobj.setStatus(true);
						tax.setPercentage(taxdetails.getTaxChargePercent());
						serviceproductobj.setServiceTax(tax);
					}

				}
				if (exceldatalist.get(i + 14).equalsIgnoreCase("No")
						|| exceldatalist.get(i + 14).equalsIgnoreCase("Na")) {
					/**
					 * Old Code
					 */
					// Tax tax = new Tax();
					// tax.setInclusive(false);
					// serviceproductobj.setStatus(false);
					/**
					 * Date : 12-10-2017 BY ANIL
					 **/
					serviceproductobj.getServiceTax().setInclusive(false);
				} else {
					/**
					 * Old Code
					 */
					// Tax tax = new Tax();
					// tax.setInclusive(true);
					// serviceproductobj.setStatus(true);
					serviceproductobj.getServiceTax().setInclusive(true);
				}

				if (exceldatalist.get(i + 15).equalsIgnoreCase("NA")) {
					/**
					 * Old code
					 */
					// Tax tax = new Tax();
					// tax.setTaxName("VAT");
					// tax.setPercentage(0d);
					// serviceproductobj.setVatTax(tax);
					/**
					 * Date : 12-10-2017 BY ANIL
					 */
					Tax tax = new Tax();
					tax.setTaxName("NA");
					tax.setTaxConfigName("NA");
					tax.setPercentage(0d);
					serviceproductobj.setVatTax(tax);

				} else {
					/**
					 * Old code
					 */
					// Tax tax = new Tax();
					// tax.setTaxName("VAT");
					// tax.setPercentage(Double.parseDouble(exceldatalist.get(i +
					// 15).trim()));
					// serviceproductobj.setStatus(true);
					// serviceproductobj.setVatTax(tax);

//				/**
//				 * Date : 12-10-2017 BY ANIL
//				 */
//				String taxPercent=getTaxPercentForName(Double.parseDouble(exceldatalist.get(i + 15)));
//				Tax tax = new Tax();
//				tax.setTaxName("SGST@"+taxPercent);
//				tax.setTaxPrintName("SGST");
//				tax.setTaxConfigName("SGST@"+taxPercent);
//				tax.setPercentage(Double.parseDouble(exceldatalist.get(i + 15)
//						.trim()));
//				serviceproductobj.setStatus(true);
//				serviceproductobj.setVatTax(tax);

					/**
					 * @author Vijay Date 03-02-2021 Des :- updated code as per Tax Name tax details
					 *         will map
					 */
					TaxDetails taxdetails = serverapputility.gettaxDetails(taxlist, exceldatalist.get(i + 15));
					if (taxdetails != null) {
						Tax tax = new Tax();
						tax.setTaxName(taxdetails.getTaxChargeName());
						tax.setTaxPrintName(taxdetails.getTaxPrintName());
						tax.setTaxConfigName(taxdetails.getTaxChargeName());
						serviceproductobj.setStatus(true);
						tax.setPercentage(taxdetails.getTaxChargePercent());
						serviceproductobj.setVatTax(tax);
					}
				}
				if (exceldatalist.get(i + 16).equalsIgnoreCase("No")
						|| exceldatalist.get(i + 16).equalsIgnoreCase("Na")) {
					/**
					 * Old Code
					 */
					// Tax tax = new Tax();
					// tax.setInclusive(false);
					// serviceproductobj.setStatus(false);
					serviceproductobj.getVatTax().setInclusive(false);

				} else {
					/**
					 * Old Code
					 */
					// Tax tax = new Tax();
					// tax.setInclusive(true);
					// // serviceproductobj.setStatus(true);

					serviceproductobj.getVatTax().setInclusive(true);
				}
				if (exceldatalist.get(i + 17).equalsIgnoreCase("Na")) {
					serviceproductobj.setUnitOfMeasurement("");
				} else {
					serviceproductobj.setUnitOfMeasurement(exceldatalist.get(i + 17).trim());
					;
				}

				if (exceldatalist.get(i + 18).equalsIgnoreCase("Na")) {
					serviceproductobj.setComment("");
				} else {
					serviceproductobj.setComment(exceldatalist.get(i + 18).trim());
				}

				if (exceldatalist.get(i + 19).equalsIgnoreCase("Na")) {
					serviceproductobj.setCommentdesc("");
				} else {
					serviceproductobj.setCommentdesc(exceldatalist.get(i + 19));
				}
				serviceproductobj.setCount(++count);
				serviceproductobj.setCompanyId(companyId);
				serviceproductlist.add(serviceproductobj);
			}

			// lastcount is data count of list before saving my data
			lastcount = ofy().load().type(ServiceProduct.class).filter("companyId", companyId).count();

			// Saving our list now by below commands
			ofy().save().entities(serviceproductlist).now();

			// this is only our setter
			ng.setNumber(count);

			// This class consist of save method..
			GenricServiceImpl impl = new GenricServiceImpl();

			impl.save(ng);

			System.out.println("Data saved successfuly=======================================");
			logger.log(Level.SEVERE, "Data saved successfuly=======================================");

			newcount = ofy().load().type(ServiceProduct.class).filter("companyId", companyId).count();

			System.out.println("New Count" + newcount);

			System.out.println("number@@@" + ng.getNumber());

			filerecords = serviceproductlist.size();
			insertedRecord = newcount - lastcount;
			System.out.println("Result:" + insertedRecord);
			integerlist.add(filerecords);
			integerlist.add(insertedRecord);
			System.out.println("SIZE:" + serviceproductlist.size());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return integerlist;
	}

	private boolean validationforserviceproduct(List<ServiceProduct> listofserviceproduct,
			ArrayList<String> exceldatalist, int i) {
		// TODO Auto-generated method stub
		boolean value = true;

		for (int j = 0; j < listofserviceproduct.size(); j++) {
			if (listofserviceproduct.get(j).getProductCode().trim().equalsIgnoreCase(exceldatalist.get(i + 3).trim())) {
				value = false;
			}
		}

		return value;
	}

//	public ArrayList<Integer> itemproductsave(long companyId,ArrayList<String> exceldatalist) {
//		// TODO Auto-generated method stub
//
//		int filerecords;
//		int insertedrecords;
//		int newcount;
//		int lastcount;
//		int reflastcount1 = 0;
//		int reflastcount2 = 0;
//		int refcount = 0;
//
//		ArrayList<ItemProduct> itemproductlist = new ArrayList<ItemProduct>();
//		/** date 7.12.2018 added by komal for product inventory view save **/
//		ArrayList<ProductInventoryView> productInventoryViewList = new ArrayList<ProductInventoryView>();
//		// itemProductref=ofy().load().type(ItemProduct.class).filter("companyId",
//		// companyId).first().now();
//		// List<ItemProduct>
//		// listproduct=ofy().load().type(ItemProduct.class).filter("companyId",
//		// companyId).list();
//		// System.out.println("The Value of Item Product is : "+listproduct.size());
//		//
//		// listproduct.get(0).getProductCode();
//
//		List<ItemProduct> listofItemproduct = ofy().load()
//				.type(ItemProduct.class).filter("companyId", companyId).list();
//		System.out.println("The List contains :" + listofItemproduct);
//
//		ArrayList<Integer> integerlist = new ArrayList<Integer>();
//
//		NumberGeneration ng = new NumberGeneration();
//
//		ng = ofy().load().type(NumberGeneration.class)
//				.filter("companyId", companyId)
//				.filter("processName", "ItemProduct").filter("status", true)
//				.first().now();
//
//		long number = ng.getNumber();
//		int count = (int) number;
//		
//		String strRefNo = "";
//		ItemProduct itemproductobj = null;
//		boolean multpleWarehouseFlag = false;
//		ProductInventoryView productInventoryView= null;
//		for (int i = 37; i < exceldatalist.size(); i += 37) {
//			/**
//			 * Date 07-12-2018 by Vijay
//			 * Des :- Added ref number for multiple warehouse upload for the same product
//			 */
//			 if(exceldatalist.get(i+36).trim().equalsIgnoreCase("NA")
//				|| !exceldatalist.get(i+36).trim().equalsIgnoreCase("NA") 
//				&& strRefNo.equals("") || !strRefNo.equals(exceldatalist.get(i+36).trim())){
//			itemproductobj = new ItemProduct();
////			ItemProduct itemproductobj = new ItemProduct();
//			multpleWarehouseFlag = true;
//			// ProductInventoryView productInventory=new ProductInventoryView();
//			boolean validcheck = checkvalidationforitemproduct(listofItemproduct, exceldatalist, i);
//
//			if (validcheck == false) {
//				integerlist.add(-8);
//				return integerlist;
//			}
//
//			if (exceldatalist.get(i).equalsIgnoreCase("na")) {
//				itemproductobj.setProductCode("");
//			} else {
//				itemproductobj.setProductCode(exceldatalist.get(i));
//			}
//
//			if (exceldatalist.get(i + 1).equalsIgnoreCase("na")) {
//				itemproductobj.setProductCategory("");
//			} else {
//				itemproductobj.setProductCategory(exceldatalist.get(i + 1));
//			}
//
//			if (exceldatalist.get(i + 2).equalsIgnoreCase("na")) {
//				itemproductobj.setProductName("");
//			} else {
//				itemproductobj.setProductName(exceldatalist.get(i + 2));
//			}
//
//			if (exceldatalist.get(i + 3).equalsIgnoreCase("na")) {
//				itemproductobj.setSpecifications("");
//			} else {
//				itemproductobj.setSpecifications(exceldatalist.get(i + 3));
//			}
//
//			if (exceldatalist.get(i + 4).equalsIgnoreCase("na")) {
//				itemproductobj.setProductGroup("");
//			} else {
//				itemproductobj.setProductGroup(exceldatalist.get(i + 4));
//			}
//
//			if (exceldatalist.get(i + 5).equalsIgnoreCase("na")) {
//				itemproductobj.setProductType("");
//			} else {
//				itemproductobj.setProductType(exceldatalist.get(i + 5));
//			}
//
//			if (exceldatalist.get(i + 6).equalsIgnoreCase("na")) {
//				itemproductobj.setProductClassification("");
//			} else {
//				itemproductobj.setProductClassification(exceldatalist
//						.get(i + 6));
//			}
//
//			if (exceldatalist.get(i + 7).equalsIgnoreCase("na")) {
//				itemproductobj.setRefNumber1("");
//			} else {
//				itemproductobj.setRefNumber1(exceldatalist.get(i + 7));
//			}
//
//			if (exceldatalist.get(i + 8).equalsIgnoreCase("na")) {
//				itemproductobj.setRefNumber2("");
//			} else {
//				itemproductobj.setRefNumber2(exceldatalist.get(i + 8));
//			}
//
//			if (exceldatalist.get(i + 9).equalsIgnoreCase("na")) {
//				itemproductobj.setPrice(0d);
//			} else {
//				itemproductobj.setPrice(Double.parseDouble(exceldatalist.get(i + 9)));
//			}
//
//			if (exceldatalist.get(i + 10).equalsIgnoreCase("na")) {
//
//				// Tax tax = new Tax();
//				// // tax.setTaxName("VAT TAX");
//				// tax.setPercentage(0);
//				// itemproductobj.setStatus(false);
//				// itemproductobj.setVatTax(tax);
//				/**
//				 * Date : 12-10-2017 By ANIL
//				 */
//				Tax tax = new Tax();
//				tax.setTaxName("NA");
//				tax.setTaxConfigName("NA");
//				tax.setPercentage(0);
//				itemproductobj.setStatus(false);
//				itemproductobj.setVatTax(tax);
//
//			} else {
//				/**
//				 * old COde
//				 */
//				// Tax tax = new Tax();
//				// // tax.setTaxName("VAT TAX");
//				// tax.setPercentage(Double.parseDouble(exceldatalist.get(i +
//				// 10)));
//				// itemproductobj.setStatus(true);
//				// itemproductobj.setVatTax(tax);
//				/**
//				 * Date : 12-10-2017 BY ANIL
//				 * Date : 23-07-2018 BY ANIL assigning tax percent in name;
//				 */
//				
//				String taxPercent=getTaxPercentForName(Double.parseDouble(exceldatalist.get(i + 10)));
//				Tax tax = new Tax();
//				tax.setTaxName("CGST@"+taxPercent);
//				tax.setTaxPrintName("CGST");
//				tax.setTaxConfigName("CGST@"+taxPercent);
//				tax.setPercentage(Double.parseDouble(exceldatalist.get(i + 10)));
//				itemproductobj.setStatus(true);
//				itemproductobj.setVatTax(tax);
//				
//				
//			}
//
//			if (exceldatalist.get(i + 11).equalsIgnoreCase("no")|| exceldatalist.get(i + 11).equalsIgnoreCase("na")) {
//				/**
//				 * Old Code
//				 */
//				// Tax tax = new Tax();
//				// tax.setInclusive(false);
//				/**
//				 * Date : 12-10-2017 BY ANIL
//				 */
//				itemproductobj.getVatTax().setInclusive(false);
//			} else {
//				/**
//				 * Old Code
//				 */
//				// Tax tax = new Tax();
//				// tax.setInclusive(true);
//				/**
//				 * Date :12-10-2017 BY ANIL
//				 */
//				itemproductobj.getVatTax().setInclusive(true);
//			}
//
//			if (exceldatalist.get(i + 12).equalsIgnoreCase("na")) {
//				/**
//				 * Old Code
//				 */
//				// Tax tax = new Tax();
//				// tax.setTaxName("Service Tax");
//				// tax.setPercentage(0d);
//				// itemproductobj.setStatus(false);
//				// itemproductobj.setServiceTax(tax);
//				/**
//				 * Date : 12-10-2017 BY ANIL
//				 */
//				Tax tax = new Tax();
//				tax.setTaxName("NA");
//				tax.setTaxConfigName("NA");
//				tax.setPercentage(0d);
//				itemproductobj.setStatus(false);
//				itemproductobj.setServiceTax(tax);
//			} else {
//				/**
//				 * ' Old Code
//				 */
//				// Tax tax = new Tax();
//				// tax.setTaxName("Service Tax");
//				// tax.setPercentage(Double.parseDouble(exceldatalist.get(i +
//				// 12)));
//				// itemproductobj.setStatus(true);
//				// itemproductobj.setServiceTax(tax);
//				/**
//				 * Date : 12-10-2017 BY ANIL
//				 */
//				String taxPercent=getTaxPercentForName(Double.parseDouble(exceldatalist.get(i + 12)));
//				Tax tax = new Tax();
//				tax.setTaxName("SGST@"+taxPercent);
//				tax.setTaxPrintName("SGST");
//				tax.setTaxConfigName("SGST@"+taxPercent);
//				tax.setPercentage(Double.parseDouble(exceldatalist.get(i + 12)));
//				itemproductobj.setStatus(true);
//				itemproductobj.setServiceTax(tax);
//			}
//
//			if (exceldatalist.get(i + 13).equalsIgnoreCase("no")|| exceldatalist.get(i + 13).equalsIgnoreCase("na")) {
//				/**
//				 * Old Code
//				 */
//				// Tax tax = new Tax();
//				// tax.setInclusive(false);
//				/**
//				 * Date : 12-10-2017 By ANIL
//				 */
//				itemproductobj.getServiceTax().setInclusive(false);
//			} else {
//				/**
//				 * Old Code
//				 */
//				// Tax tax = new Tax();
//				// tax.setInclusive(true);
//				/**
//				 * Date : 12-10-2017 By ANIL
//				 */
//				itemproductobj.getServiceTax().setInclusive(true);
//			}
//
//			if (exceldatalist.get(i + 14).equalsIgnoreCase("no")) {
//				itemproductobj.setStatus(false);
//			} else {
//				itemproductobj.setStatus(true);
//			}
//			if (exceldatalist.get(i + 15).equalsIgnoreCase("na")) {
//				itemproductobj.setBrandName("");
//			} else {
//				itemproductobj.setBrandName(exceldatalist.get(i + 15));
//			}
//			if (exceldatalist.get(i + 16).equalsIgnoreCase("na")) {
//				itemproductobj.setModel("");
//			} else {
//				itemproductobj.setModel(exceldatalist.get(i + 16));
//			}
//			if (exceldatalist.get(i + 17).equalsIgnoreCase("na")) {
//				itemproductobj.setSerialNumber("");
//			} else {
//				itemproductobj.setSerialNumber(exceldatalist.get(i + 17));
//			}
//			if (exceldatalist.get(i + 18).equalsIgnoreCase("na")) {
//				itemproductobj.setUnitOfMeasurement("");
//			} else {
//				itemproductobj.setUnitOfMeasurement(exceldatalist.get(i + 18));
//			}
//			if (exceldatalist.get(i + 19).equalsIgnoreCase("na")) {
//				itemproductobj.setComment("");
//			} else {
//				itemproductobj.setComment(exceldatalist.get(i + 19));
//			}
//
//			if (exceldatalist.get(i + 20).equalsIgnoreCase("na")) {
//				itemproductobj.setCommentdesc("");
//			} else {
//				itemproductobj.setCommentdesc(exceldatalist.get(i + 20));
//			}
//			
//			/***
//			 * 
//			 * Date : 23-04-2018 BY ANIL
//			 * Added new column which stores reference number of service product
//			 * for HVAC/Rohan
//			 */
//			
//			if (exceldatalist.get(i + 28).equalsIgnoreCase("na")) {
//				itemproductobj.setSerProdId(0);
//				itemproductobj.setSerProdCode("");
//				itemproductobj.setSerProdName("");
//				itemproductobj.setWarrantyDuration(0);
//				itemproductobj.setNoOfServices(0);
//			} else {
//				
//				int serviceId=Integer.parseInt(exceldatalist.get(i + 28).trim());
//				ServiceProduct serProd=ofy().load().type(ServiceProduct.class).filter("companyId", companyId).filter("count", serviceId).first().now();
//				
//				itemproductobj.setSerProdId(serProd.getCount());
//				itemproductobj.setSerProdCode(serProd.getProductCode());
//				itemproductobj.setSerProdName(serProd.getProductName());
//				itemproductobj.setWarrantyDuration(serProd.getDuration());
//				itemproductobj.setNoOfServices(serProd.getNumberOfServices());
//			}
//			
//			
//			if(exceldatalist.get(i + 29).equalsIgnoreCase("na")){
//				logger.log(Level.SEVERE,"Inside purchase price zero");
//				itemproductobj.setPurchasePrice(0);
//			}else{
//				logger.log(Level.SEVERE,"Inside purchase price non zero");
//				itemproductobj.setPurchasePrice(Double.parseDouble(exceldatalist.get(i + 29).trim()));
//			}
//			
//			if(exceldatalist.get(i + 30).equalsIgnoreCase("na")){
////				itemproductobj.setPurchasePrice(0);
//				Tax tax = new Tax();
//				tax.setTaxName("CGST");
//				tax.setTaxPrintName("CGST");
//				tax.setTaxConfigName("CGST");
//				tax.setPercentage(0d);
//				itemproductobj.setStatus(false);
//				itemproductobj.setPurchaseTax1(tax);
//			}else{
//				
//				String taxPercent=getTaxPercentForName(Double.parseDouble(exceldatalist.get(i + 30)));
//				Tax tax = new Tax();
//				tax.setTaxName("CGST@"+taxPercent);
//				tax.setTaxPrintName("CGST");
//				tax.setTaxConfigName("CGST@"+taxPercent);
//				tax.setPercentage(Double.parseDouble(exceldatalist.get(i + 30)));
//				itemproductobj.setStatus(true);
//				itemproductobj.setPurchaseTax1(tax);
//			}
//			
//			if (exceldatalist.get(i + 31).equalsIgnoreCase("no")|| exceldatalist.get(i + 31).equalsIgnoreCase("na")) {
//				/**
//				 * Old Code
//				 */
//				// Tax tax = new Tax();
//				// tax.setInclusive(false);
//				/**
//				 * Date : 12-10-2017 By RAHUL
//				 */
//				itemproductobj.getPurchaseTax1().setInclusive(false);
//			} else {
//				/**
//				 * Old Code
//				 */
//				// Tax tax = new Tax();
//				// tax.setInclusive(true);
//				/**
//				 * Date : 12-10-2017 By RAHUL
//				 */
//				itemproductobj.getPurchaseTax1().setInclusive(true);
//			}
//			
//			if(exceldatalist.get(i + 32).equalsIgnoreCase("na")){
////				itemproductobj.setPurchasePrice(0);
//				Tax tax = new Tax();
//				tax.setTaxName("SGST");
//				tax.setTaxPrintName("SGST");
//				tax.setTaxConfigName("SGST");
//				tax.setPercentage(0d);
//				itemproductobj.setStatus(false);
//				itemproductobj.setPurchaseTax2(tax);
//			}else{
////				itemproductobj.setPurchasePrice(0);
//				String taxPercent=getTaxPercentForName(Double.parseDouble(exceldatalist.get(i + 32)));
//				Tax tax = new Tax();
//				tax.setTaxName("SGST@"+taxPercent);
//				tax.setTaxPrintName("SGST");
//				tax.setTaxConfigName("SGST@"+taxPercent);
//				tax.setPercentage(Double.parseDouble(exceldatalist.get(i + 32)));
//				itemproductobj.setStatus(true);
//				itemproductobj.setPurchaseTax2(tax);
//			}
//			
//			if (exceldatalist.get(i + 33).equalsIgnoreCase("no")|| exceldatalist.get(i + 33).equalsIgnoreCase("na")) {
//				/**
//				 * Old Code
//				 */
//				// Tax tax = new Tax();
//				// tax.setInclusive(false);
//				/**
//				 * Date : 12-10-2017 By RAHUL
//				 */
//				itemproductobj.getPurchaseTax2().setInclusive(false);
//			} else {
//				/**
//				 * Old Code
//				 */
//				// Tax tax = new Tax();
//				// tax.setInclusive(true);
//				/**
//				 * Date : 12-10-2017 By RAHUL
//				 */
//				itemproductobj.getPurchaseTax2().setInclusive(true);
//			}
//			
//			if (exceldatalist.get(i + 34).equalsIgnoreCase("NA")){
//				itemproductobj.setMrp(0);
//			}else{
//				itemproductobj.setMrp(Double.parseDouble(exceldatalist.get(i + 34)));
//			}
//			
//			if (exceldatalist.get(i + 35).equalsIgnoreCase("NA")){
//				itemproductobj.setHsnNumber("");
//			}else{
//				itemproductobj.setHsnNumber(exceldatalist.get(i + 35));
//			}
//			
//			/**
//			 * End
//			 */
//			itemproductobj.setCount(++count);
//			itemproductobj.setCompanyId(companyId);
//			itemproductlist.add(itemproductobj);
//			System.out.println("Done Adding");
//			
//		    } 
//		    /**
//			 * Date 07-12-2018 by Vijay
//			 * Des :- Added for multiple warehouse upload for the same product using ref no
//			 */
//			else{
//				multpleWarehouseFlag = false;
//			}
//			 
//			refcount++;
//			System.out.println("The refcount count : " + refcount);
//
//			if (exceldatalist.get(i + 21).equalsIgnoreCase("yes")) {
//
//				reflastcount1 = ofy().load().type(ItemProduct.class).filter("companyId", companyId).count();
//				System.out.println("The refllastcount1: " + reflastcount1);
//
//				// ofy().save().entities(itemproductlist);
////				System.out.println("The Entered product is: "+ itemproductlist.get(refcount - 1));
//
//				int refnum = i + 21;
//				int pid = itemproductobj.getCount();
//				System.out.println("The Count is : " + count);
//				String pcode = itemproductobj.getProductCode();
//				String pname = itemproductobj.getProductName();
//				double pprice = itemproductobj.getPrice();
//				String pcategory = itemproductobj.getProductCategory();
//
//			//	boolean check = productinventorysave(refnum, pid, pcode, pname,pprice, pcategory, companyId, exceldatalist);
//				/** Date 07.12.2018 changed by komal **/
////				ProductInventoryView productInventoryView = productinventorysave(refnum, pid, pcode, pname,pprice, pcategory, companyId, exceldatalist);
////				productInventoryViewList.add(productInventoryView);
//				/**
//				 * Date 07-12-2018 by Vijay
//				 * Des :- Added for multiple warehouse upload for the same product 
//				 */
//				if(multpleWarehouseFlag){
//					if(productInventoryView!=null)
//					productInventoryViewList.add(productInventoryView);
//					
//					productInventoryView = productinventorysave(refnum, pid, pcode, pname,pprice, pcategory, companyId, exceldatalist);
//				}else{
//					ProductInventoryViewDetails pinvendetails =  productinventoryDetails(refnum, pid, pcode, pname,pprice, pcategory, companyId, exceldatalist);
//					productInventoryView.getDetails().add(pinvendetails);
//				}
//				/**
//				 * ends here
//				 */
//				
//				/**
//				 * Date : 23-04-2018 BY  ANIL
//				 */
////				System.out.println("The Value Of check is :" + check);
////				ofy().save().entities(proinventoryviewlist).now();
////				System.out.println("Saved Saved Saved Saved");
//				/**
//				 * End
//				 */
//
//			}
//			
//			
//			/**
//			 * Date 10-12-2018 by Vijay
//			 * Des :- Added for multiple warehouse upload for the same product 
//			 */
//			strRefNo = exceldatalist.get(i+36).trim(); 
//			
//		}
//		
//		/**
//		 * Date 10-12-2018 by Vijay
//		 * Des :- Added for multiple warehouse upload for the same product 
//		 * for the last product entry added here
//		 */
//		if(productInventoryView!=null)
//			productInventoryViewList.add(productInventoryView);
//
//		reflastcount2 = ofy().load().type(ItemProduct.class).filter("companyId", companyId).count();
//		System.out.println("Reflastcount2: " + reflastcount2);
//		int dummy = reflastcount2 - reflastcount1;
//		System.out.println("Dummy :" + dummy);
//		// Taking the last count of the Entity
//		if (dummy != reflastcount2) {
//			lastcount = (ofy().load().type(ItemProduct.class).filter("companyId", companyId).count())- dummy;
//		} else {
//			lastcount = reflastcount2;
//		}
//		System.out.println("The Lastcount : " + lastcount);
//		System.out.println("The Item Product list :" + itemproductlist);
//		ofy().save().entities(itemproductlist).now();
//		ng.setNumber(count);
//		
//		/** date 7.12.2018 added by komal for product inventory view list save**/
//		ofy().save().entities(productInventoryViewList).now();
//		
//		GenricServiceImpl impl = new GenricServiceImpl();
//		impl.save(ng);
//		impl.save(ng);
//
//		filerecords = itemproductlist.size();
//		newcount = ofy().load().type(ItemProduct.class).filter("companyId", companyId).count();
//
//		System.out.println("The Value of New Count is : " + newcount);
//		insertedrecords = newcount - lastcount;
//
//		integerlist.add(filerecords);
//		integerlist.add(insertedrecords);
//
//		return integerlist;
//	}

	private String getTaxPercentForName(double taxPercent) {
		String txP = "";
		int percent = (int) taxPercent;
		if (percent < taxPercent) {
			txP = taxPercent + "";
		} else {
			txP = percent + "";
		}
		return txP;
	}

	private boolean checkvalidationforitemproduct(List<ItemProduct> listofItemproduct, ArrayList<String> exceldatalist,
			int i) {
		// TODO Auto-generated method stub

		boolean value = true;

		for (int j = 0; j < listofItemproduct.size(); j++) {
			if (listofItemproduct.get(j).getProductCode().trim().equalsIgnoreCase(exceldatalist.get(i).trim())) {
				System.out.println("The list product code is: " + listofItemproduct.get(j).getProductCode());
				System.out.println("The excelsheet product code is : " + exceldatalist.get(i));
				value = false;
			}

		}

		return value;

	}

	private ProductInventoryView productinventorysave(int refnum, int pid, String pcode, String pname, double pprice,
			String pcategory, long companyId, ArrayList<String> exceldatalist) {
		// TODO Auto-generated method stub

		ProductInventoryView productinventoryview = new ProductInventoryView();

		productinventoryview.setProdID(pid);
		productinventoryview.setProductCode(pcode);
		productinventoryview.setProductName(pname);
		productinventoryview.setProductPrice(pprice);
		productinventoryview.setProductCategory(pcategory);

		if (exceldatalist.get(refnum + 1).equalsIgnoreCase("no")) {
			productinventoryview.setInspectionRequired("No");
		} else {
			productinventoryview.setInspectionRequired("Yes");
		}
		if (exceldatalist.get(refnum + 2).equalsIgnoreCase("na")) {
			productinventoryview.setInspectionMethod("");
		} else {
			productinventoryview.setInspectionMethod(exceldatalist.get(refnum + 2));
		}
		ProductInventoryViewDetails pinvendetails = new ProductInventoryViewDetails();

		/**
		 * Date : 23-04-2018 BY ANIL
		 */
		pinvendetails.setProdid(pid);
		pinvendetails.setProdcode(pcode);
		pinvendetails.setProdname(pname);
		/**
		 * End
		 */

		if (exceldatalist.get(refnum + 3).equalsIgnoreCase("na")) {
			pinvendetails.setWarehousename("");
		} else {
			pinvendetails.setWarehousename(exceldatalist.get(refnum + 3));
		}
		if (exceldatalist.get(refnum + 4).equalsIgnoreCase("na")) {
			pinvendetails.setStoragelocation("");
		} else {
			pinvendetails.setStoragelocation(exceldatalist.get(refnum + 4));
		}
		if (exceldatalist.get(refnum + 5).equalsIgnoreCase("na")) {
			pinvendetails.setStoragebin("");
		} else {
			pinvendetails.setStoragebin(exceldatalist.get(refnum + 5));
		}

		/**
		 * Date : 23-04-2018 BY ANIL setting available qty at the time of uploading
		 * product for HVAC/ROhan
		 */
		if (exceldatalist.get(refnum + 6).equalsIgnoreCase("na")) {
			pinvendetails.setAvailableqty(0);
			;
		} else {
			double availableStock = Double.parseDouble(exceldatalist.get(refnum + 6).trim());
			pinvendetails.setAvailableqty(availableStock);
		}

		productinventoryview.setCount(pid);
		/**
		 * End
		 */

		/**
		 * Date : 23-04-2018 BY ANIL
		 */
//		productinventoryview.setDetails(proinvendetaillist);
		productinventoryview.getDetails().add(pinvendetails);
		/**
		 * End
		 */
		productinventoryview.setCompanyId(companyId);

		// ofy().save().entity(productinventoryview).now();

		/**
		 * Date : 23-04-2018 BY ANIL Old code commented by anil
		 */
//		proinventoryviewlist.add(productinventoryview);
//		System.out.println("The checkList is :" + proinventoryviewlist);
//		// ofy().save().entities(proinvendetaillist).now();

		/**
		 * End
		 */

		return productinventoryview;
	}

	private ArrayList<Integer> countrysave(long companyId, ArrayList<String> exceldatalist) {

		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		ArrayList<Country> countrylist = new ArrayList<Country>();

		List<Country> listofcountry = ofy().load().type(Country.class).filter("companyId", companyId).list();

		int filerecords;
		int insertedrecords;
		int lastcount;
		int newcount;

		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId).filter("processName", "Country")
				.filter("status", true).first().now();

		long number = ng.getNumber();
		int count = (int) number;

		for (int i = 2; i < exceldatalist.size(); i += 2) {
			Country countryobj = new Country();

			boolean countrycheck = validationforcountry(listofcountry, exceldatalist, i);

			if (countrycheck == false) {
				integerlist.add(-4);
				return integerlist;
			}

			if (exceldatalist.get(i).equalsIgnoreCase("na")) {
				countryobj.setCountryName("");
			} else {
				countryobj.setCountryName(exceldatalist.get(i));
			}

			if (exceldatalist.get(i + 1).equalsIgnoreCase("na")) {
				countryobj.setLanguage("");
			} else {
				countryobj.setLanguage(exceldatalist.get(i + 1));
			}

			countryobj.setCount(++count);
			countryobj.setCompanyId(companyId);
			countrylist.add(countryobj);
		}

		lastcount = ofy().load().type(Country.class).filter("companyId", companyId).count();

		ofy().save().entities(countrylist).now();

		ng.setNumber(count);
		GenricServiceImpl impl = new GenricServiceImpl();

		impl.save(ng);

		filerecords = countrylist.size();
		newcount = ofy().load().type(Country.class).filter("companyId", companyId).count();
		insertedrecords = newcount - lastcount;
		integerlist.add(filerecords);
		integerlist.add(insertedrecords);

		return integerlist;
	}

	private boolean validationforcountry(List<Country> listofcountry, ArrayList<String> exceldatalist, int i) {
		// TODO Auto-generated method stub
		boolean value = true;
		for (int j = 0; j < listofcountry.size(); j++) {
			if (listofcountry.get(j).getCountryName().trim().equalsIgnoreCase(exceldatalist.get(i).trim())) {
				value = false;
			}
		}

		return value;

	}

	private ArrayList<Integer> statesave(long companyId, ArrayList<String> exceldatalist) {
		// TODO Auto-generated method stub
		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		ArrayList<State> statelist = new ArrayList<State>();
		List<State> listofstate = ofy().load().type(State.class).filter("companyId", companyId).list();

		int lastcount;
		int newcount;
		int filerecords;
		int insertedrecord;

		NumberGeneration ng = new NumberGeneration();

		ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId).filter("processName", "State")
				.first().now();

		long number = ng.getNumber();

		int count = (int) number;

		for (int i = 4; i < exceldatalist.size(); i += 4) {
			State state = new State();
			boolean statecheck = validationforstate(listofstate, exceldatalist, i);
			if (statecheck == false) {
				integerlist.add(-11);
				return integerlist;
			}
			System.out.println("Country :" + exceldatalist.get(i));
			System.out.println("State :" + exceldatalist.get(i + 1));
			System.out.println("Status :" + exceldatalist.get(i + 2));

			if (exceldatalist.get(i).equalsIgnoreCase("na")) {
				state.setCountry("");
			} else {
				state.setCountry(exceldatalist.get(i));
			}

			if (exceldatalist.get(i + 1).equalsIgnoreCase("na")) {
				state.setStateName("");
			} else {
				state.setStateName(exceldatalist.get(i + 1));
			}
			if (exceldatalist.get(i + 2).equalsIgnoreCase("na")) {
				state.setStateCode("");
			} else {
				state.setStateCode(exceldatalist.get(i + 2));
			}

			if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
				System.out.println("Inside status condition");
				System.out.println("Data Recieved in if condition :" + exceldatalist.get(i + 3).trim());
				state.setStatus(false);
			} else {
				System.out.println("Data Recieved in else condition :" + exceldatalist.get(i + 3).trim());
				System.out.println("Inside else status condition");
				state.setStatus(true);
			}
			state.setCount(++count);
			state.setCompanyId(companyId);
			statelist.add(state);
		}

		lastcount = ofy().load().type(State.class).filter("companyId", companyId).count();

		ofy().save().entities(statelist).now();

		ng.setNumber(count);

		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(ng);

		newcount = ofy().load().type(State.class).filter("companyId", companyId).count();

		filerecords = statelist.size();
		insertedrecord = newcount - lastcount;

		integerlist.add(filerecords);
		integerlist.add(insertedrecord);

		return integerlist;
	}

	private boolean validationforstate(List<State> listofstate, ArrayList<String> exceldatalist, int i) {
		// TODO Auto-generated method stub
		boolean value = true;
		for (int j = 0; j < listofstate.size(); j++) {
			if (listofstate.get(j).getStateName().trim().equalsIgnoreCase(exceldatalist.get(i + 1).trim())) {
				value = false;
			}
		}
		return value;
	}

	private ArrayList<Integer> citysave(long companyId, ArrayList<String> exceldatalist) {
		// TODO Auto-generated method stub
		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		ArrayList<City> citylist = new ArrayList<City>();
		System.out.println("The excel size is: " + exceldatalist.size());
		errorList = new ArrayList<String>();
		List<City> listofcity = ofy().load().type(City.class).filter("companyId", companyId).list();

		System.out.println("Inside Citysave:part1");

		int filerecord;
		int insertedrecord;
		int lastcount;
		int newcount;
		System.out.println("Inside Citysave:part2");

		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId).filter("processName", "City")
				.filter("status", true).first().now();
		long number = ng.getNumber();
		int count = (int) number;

		for (int i = 5; i < exceldatalist.size(); i += 5) {

			String error = "";
			if (exceldatalist.get(i + 1).equalsIgnoreCase("NA")) {
				error += "City is Mandatory";
				errorList.add(exceldatalist.get(i + 3));
				errorList.add(error);
				continue;
			} else {
				String strEmp = duplicateCityValidation(listofcity, exceldatalist.get(i + 1).trim());
				System.out.println("strEmp ==" + strEmp);
				if (!strEmp.equals("")) {
					error += strEmp;
					errorList.add(exceldatalist.get(i + 1));
					errorList.add(error);
					continue;
				}
			}

			System.out.println("Inside Citysave:part3 in for loops");
			City city = new City();

//			boolean citycheck = validationforcity(listofcity, exceldatalist,
//					i + 1);
//
//			if (citycheck == false) {
//				integerlist.add(-3);
//				return integerlist;
//			}

			if (exceldatalist.get(i).equalsIgnoreCase("na")) {
				city.setStateName("");

			} else {
				city.setStateName(exceldatalist.get(i));
			}
			if (exceldatalist.get(i + 1).equalsIgnoreCase("na")) {
				city.setCityName("");
			} else {
				city.setCityName(exceldatalist.get(i + 1));
			}

			if (exceldatalist.get(i + 2).equalsIgnoreCase("no")) {
				city.setStatus(false);
			} else {
				city.setStatus(true);
			}

			if (exceldatalist.get(i + 3).equalsIgnoreCase("na")) {
				city.setClassName("");
			} else {
				city.setClassName(exceldatalist.get(i + 3));
			}
			if (exceldatalist.get(i + 4).equalsIgnoreCase("na")) {
				city.setTat("");
				;
			} else {
				city.setTat(exceldatalist.get(i + 4));
			}

			city.setCount(++count);
			city.setCompanyId(companyId);
			if (!error.equals("")) {
				errorList.add(exceldatalist.get(i));
				errorList.add(error);
			}
			citylist.add(city);
		}
		CsvWriter.cityErrorList = errorList;
		if (errorList != null && errorList.size() != 0) {
			integerlist.add(-1);
			return integerlist;
		}
		lastcount = ofy().load().type(City.class).filter("companyId", companyId).count();

		ofy().save().entities(citylist).now();

		ng.setCount(count);

		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(ng);

		newcount = ofy().load().type(City.class).filter("companyId", companyId).count();
		filerecord = citylist.size();
		insertedrecord = newcount - lastcount;

		integerlist.add(filerecord);
		integerlist.add(insertedrecord);

		return integerlist;
	}

	private String duplicateCityValidation(List<City> listofcity, String cityName) {
		String city = "";
		for (City obj : listofcity) {
			if (obj.getCityName().equals(cityName)) {
				city = "Duplicate city name";
				break;
			} else {
				continue;
			}
		}
		return city;
	}

	private boolean validationforcity(List<City> listofcity, ArrayList<String> exceldatalist, int i) {
		// TODO Auto-generated method stub
		boolean value = true;
		System.out.println("the value of i is(4): " + i);

		for (int j = 0; j < listofcity.size(); j++) {
			System.out.println("The value of City list is :" + listofcity.get(j).getCityName());
			System.out.println("The List of input city is : " + exceldatalist.get(i));
			if (listofcity.get(j).getCityName().trim().equalsIgnoreCase(exceldatalist.get(i).trim())) {

				value = false;
			}
		}

		return value;
	}

	private ArrayList<Integer> localitysave(long companyId, ArrayList<String> exceldatalist) {
		// TODO Auto-generated method stub
		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		ArrayList<Locality> localitylist = new ArrayList<Locality>();
		List<Locality> listoflocality = ofy().load().type(Locality.class).filter("companyId", companyId).list();

		errorList = new ArrayList<String>();
		int filerecord;
		int insertedrecord;
		int lastcount;
		int newcount;

		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId).filter("processName", "Locality")
				.filter("status", true).first().now();
		long number = ng.getNumber();
		int count = (int) number;

		for (int i = 4; i < exceldatalist.size(); i += 4) {
			Locality locality = new Locality();

//			boolean localitycheck = validationforlocality(listoflocality,
//					exceldatalist, i);
//
//			if (localitycheck == false) {
//				integerlist.add(-9);
//				return integerlist;
//			}
//		    String duplicateLocality=duplicateLocalityValidaion(listoflocality,exceldatalist, i);
			String error = "";
			if (exceldatalist.get(i + 1).equalsIgnoreCase("NA")) {
				error += "Locality is Mandatory";
				errorList.add(exceldatalist.get(i + 3));
				errorList.add(error);
				continue;
			} else {
				String strEmp = duplicateLocalityValidaion(listoflocality, exceldatalist.get(i + 1).trim(),
						Long.parseLong(exceldatalist.get(i + 2).trim()));
				System.out.println("strEmp ==" + strEmp);
				if (!strEmp.equals("")) {
					error += strEmp;
					errorList.add(exceldatalist.get(i + 1));
					errorList.add(error);
					continue;
				}
			}

			if (exceldatalist.get(i).equalsIgnoreCase("na")) {
				locality.setCityName("");
			} else {
				locality.setCityName(exceldatalist.get(i));
			}
			if (exceldatalist.get(i + 1).equalsIgnoreCase("na")) {
				locality.setLocality("");
			} else {

				locality.setLocality(exceldatalist.get(i + 1));
			}
			/**** Date 28-09-2017 added by vijay for locality pincode *******/
			if (exceldatalist.get(i + 2).equalsIgnoreCase("na")) {
				locality.setPinCode(0l);
			} else {
				locality.setPinCode(Long.parseLong(exceldatalist.get(i + 2).trim()));
			}

			if (exceldatalist.get(i + 3).equalsIgnoreCase("no")) {
				locality.setStatus(false);
			} else {
				locality.setStatus(true);
			}
			locality.setCount(++count);
			locality.setCompanyId(companyId);
			if (!error.equals("")) {
				errorList.add(exceldatalist.get(i));
				errorList.add(error);
			}
			localitylist.add(locality);
		}
		CsvWriter.localityErrorList = errorList;
		if (errorList != null && errorList.size() != 0) {
			integerlist.add(-1);
			return integerlist;
		}
		lastcount = ofy().load().type(Locality.class).filter("companyId", companyId).count();

		ofy().save().entities(localitylist).now();

		ng.setNumber(count);

		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(ng);

		newcount = ofy().load().type(Locality.class).filter("companyId", companyId).count();
		filerecord = localitylist.size();
		insertedrecord = newcount - lastcount;

		integerlist.add(filerecord);
		integerlist.add(insertedrecord);

		return integerlist;
	}

	private boolean validationforlocality(List<Locality> listoflocality, ArrayList<String> exceldatalist, int i) {
		// TODO Auto-generated method stub
		boolean value = true;

		for (int j = 0; j < listoflocality.size(); j++) {
			if (listoflocality.get(j).getLocality().trim().equalsIgnoreCase(exceldatalist.get(i + 1).trim())) {
				value = false;
			}
		}
		return value;
	}

	// Ashwini Patil Date:15-04-2022 Description: Bulk upload was giving error for
	// duplicate locality name with different pincode so one more pincode check is
	// added
	private String duplicateLocalityValidaion(List<Locality> listoflocality, String localityName, long pincode) {
		String msg = "";
		for (Locality obj : listoflocality) {
			if (obj.getLocality().equals(localityName) && obj.getPinCode() == pincode) {
				msg = "Duplicate locality"; // old one "Duplicate locality Name"
				break;
			} else {
				continue;
			}
		}

		return msg;

	}

	private ArrayList<Integer> employeesave(long companyId, ArrayList<String> exceldatalist) {

		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		ArrayList<Employee> employeelist = new ArrayList<Employee>();
		List<Employee> listofemployee = ofy().load().type(Employee.class).filter("companyId", companyId).list();

		int filerecord;
		int insertedrecord;
		int lastcount;
		int newcount;

		HashMap<String, Integer> hashduplicateEmplpoyee = new HashMap<String, Integer>();

//			NumberGeneration ng = new NumberGeneration();
//
//			ng = ofy().load().type(NumberGeneration.class)
//					.filter("companyId", companyId)
//					.filter("processName", "Employee").filter("status", true)
//					.first().now();
//			long number = ng.getNumber();
//			int count = (int) number;
//			//Change i from 40 to 41
//			for (int i = 56; i < exceldatalist.size(); i += 56) {
//
//				Employee employee = new Employee();
//
//				boolean employeecheck = validationforemployee(listofemployee,
//						exceldatalist, i,exceldatalist.get(i + 22));
//
//				if (employeecheck == false) {
//					integerlist.add(-7);
//					return integerlist;
//				}
//
//				if (exceldatalist.get(i).equalsIgnoreCase("na")) {
//					employee.setCountry("");
//				} else {
//					employee.setCountry(exceldatalist.get(i));
//				}
//				if (exceldatalist.get(i + 1).equalsIgnoreCase("na")) {
//					employee.setDepartMent("");
//				} else {
//					employee.setDepartMent(exceldatalist.get(i + 1));
//				}
//				if (exceldatalist.get(i + 2).equalsIgnoreCase("na")) {
//					employee.setEmployeeType("");
//				} else {
//					employee.setEmployeeType(exceldatalist.get(i + 2));
//				}
//				// if (exceldatalist.get(i + 3).equalsIgnoreCase("na")) {
//				// employee.setFirstName("");
//				// } else {
//				// employee.setFirstName(exceldatalist.get(i + 3));
//				// }
//				// if (exceldatalist.get(i + 4).equalsIgnoreCase("na")) {
//				// employee.setMiddleName("");
//				// } else {
//				// employee.setMiddleName(exceldatalist.get(i + 4));
//				// }
//				// if (exceldatalist.get(i + 5).equalsIgnoreCase("na")) {
//				// employee.setLastName("");
//				// } else {
//				// employee.setLastName(exceldatalist.get(i + 5));
//				// }
//				if (exceldatalist.get(i + 3).equalsIgnoreCase("na")) {
//					employee.setFullname("");
//				} else {
//					employee.setFullname(exceldatalist.get(i + 3));
//				}
//				
//				try {
//					if (exceldatalist.get(i + 4).equalsIgnoreCase("na")) {
//
//					} else {
//						employee.setDob(fmt.parse(exceldatalist.get(i + 4).trim()));
//					}
//				} catch (ParseException e) {
//					e.printStackTrace();
//				}
//
//				if (exceldatalist.get(i + 5).equalsIgnoreCase("na")) {
//					employee.setBranchName("");
//				} else {
//					employee.setBranchName(exceldatalist.get(i + 5));
//				}
//				if (exceldatalist.get(i + 6).equalsIgnoreCase("na")) {
//					employee.setDesignation("");
//				} else {
//					employee.setDesignation(exceldatalist.get(i + 6));
//				}
//				if (exceldatalist.get(i + 7).equalsIgnoreCase("na")) {
//					employee.setRoleName("");
//				} else {
//					employee.setRoleName(exceldatalist.get(i + 7));
//				}
//
//				try {
//					if (exceldatalist.get(i + 8).equalsIgnoreCase("na")) {
//                         
//					} else {
//						employee.setJoinedAt(fmt.parse(exceldatalist.get(i + 8)
//								.trim()));
//					}
//				} catch (ParseException e) {
//					e.printStackTrace();
//				}
//                
//				if(exceldatalist.get(i+9).equalsIgnoreCase("na")){
//					employee.setMaritalStatus("");
//				}else{
//					employee.setMaritalStatus(exceldatalist.get(i+9));
//				}
//				
//				if (exceldatalist.get(i + 10).equalsIgnoreCase("na")) {
//					employee.setReportsTo("");
//				} else {
//					employee.setReportsTo(exceldatalist.get(i + 10).trim());
//				}
//				
//				if (exceldatalist.get(i + 11).equalsIgnoreCase("na")) {
//					employee.setAccessCardNo("");
//				} else {
//					employee.setAccessCardNo(exceldatalist.get(i + 11));
//				}
//				if (exceldatalist.get(i + 12).equalsIgnoreCase("na")) {
//					employee.setIdCardNo("");
//				} else {
//					employee.setIdCardNo(exceldatalist.get(i + 12));
//				}
//				if (exceldatalist.get(i + 13).equalsIgnoreCase("na")) {
//					employee.setBloodGroup("");
//				} else {
//					employee.setBloodGroup(exceldatalist.get(i + 13));
//				}
//				if (exceldatalist.get(i + 14).equalsIgnoreCase("na")) {
//					employee.setStatus(false);
//				} else {
//					employee.setStatus(true);
//				}
//				if (exceldatalist.get(i + 15).equalsIgnoreCase("na")) {
//					employee.setEmployeeBankName("");
//					;
//				} else {
//					employee.setEmployeeBankName(exceldatalist.get(i + 15));
//				}
//				if (exceldatalist.get(i + 16).equalsIgnoreCase("na")) {
//					employee.setBankBranch("");
//				} else {
//					employee.setBankBranch(exceldatalist.get(i + 16));
//				}
//				if (exceldatalist.get(i + 17).equalsIgnoreCase("na")) {
//					employee.setIfscCode("");
//				} else {
//					employee.setIfscCode(exceldatalist.get(i + 17));
//				}
//				if (exceldatalist.get(i + 18).equalsIgnoreCase("na")) {
//					employee.setEmployeeBankAccountNo("");
//				} else {
//					employee.setEmployeeBankAccountNo(exceldatalist.get(i + 18));
//				}
//				if (exceldatalist.get(i + 19).equalsIgnoreCase("na")) {
//					employee.setEmployeeESICcode("");
//				} else {
//					employee.setEmployeeESICcode(exceldatalist.get(i + 19));
//				}
//				if (exceldatalist.get(i + 20).equalsIgnoreCase("na")) {
//					employee.setEmployeePanNo("");
//				} else {
//					employee.setEmployeePanNo(exceldatalist.get(i + 20));
//				}
//				if (exceldatalist.get(i + 21).equalsIgnoreCase("na")) {
//					employee.setPPFNumber("");
//				} else {
//					employee.setPPFNumber(exceldatalist.get(i + 21));
//				}
//				if (exceldatalist.get(i + 22).equalsIgnoreCase("na")) {
//					employee.setAadharNumber(0l);
//				} else {
//					employee.setAadharNumber(Long.parseLong(exceldatalist.get(i + 22)));
//				}
//				if (exceldatalist.get(i + 23).equalsIgnoreCase("na")) {
//					employee.setUANno("");
//				} else {
//					employee.setUANno(exceldatalist.get(i + 23));
//				}
//				
//				if (exceldatalist.get(i + 24).equalsIgnoreCase("na")) {
//					// Long landlineno=exceldatalist.get(i+23);
//					employee.setLandline(0l);
//				} else {
//					employee.setLandline(Long.parseLong(exceldatalist.get(i + 24)));
//				}
//				if (exceldatalist.get(i + 25).equalsIgnoreCase("na")) {
//					employee.setCellNumber1(0l);
//				} else {
//					employee.setCellNumber1(Long.parseLong(exceldatalist
//							.get(i + 25)));
//				}
//				
//				if (exceldatalist.get(i + 26).equalsIgnoreCase("na")) {
//					employee.setCellNumber2(0l);
//				} else {
//					employee.setCellNumber2(Long.parseLong(exceldatalist
//							.get(i + 26)));
//				}
//				if (exceldatalist.get(i + 27).equalsIgnoreCase("na")) {
//					employee.setEmail("");
//				} else {
//					employee.setEmail(exceldatalist.get(i + 27));
//				}
//
//				Address address = new Address();
//
//				if (exceldatalist.get(i + 28).equalsIgnoreCase("na")) {
//
//					address.setAddrLine1("");
//				} else {
//					address.setAddrLine1(exceldatalist.get(i + 28));
//				}
//				if (exceldatalist.get(i + 29).equalsIgnoreCase("na")) {
//					address.setAddrLine2("");
//				} else {
//					address.setAddrLine2(exceldatalist.get(i + 29));
//				}
//				if (exceldatalist.get(i + 30).equalsIgnoreCase("na")) {
//					address.setLandmark("");
//				} else {
//					address.setLandmark(exceldatalist.get(i + 30));
//				}
//				if (exceldatalist.get(i + 31).equalsIgnoreCase("na")) {
//					address.setCountry("");
//				} else {
//					address.setCountry(exceldatalist.get(i + 31));
//					
//				}
//				if (exceldatalist.get(i + 32).equalsIgnoreCase("na")) {
//					address.setState("");
//				} else {
//					address.setState(exceldatalist.get(i + 32));
//				}
//				if (exceldatalist.get(i + 33).equalsIgnoreCase("na")) {
//					address.setCity("");
//				} else {
//					address.setCity(exceldatalist.get(i + 33));
//				}
//				if (exceldatalist.get(i + 34).equalsIgnoreCase("na")) {
//					address.setLocality("");
//				} else {
//					address.setLocality(exceldatalist.get(i + 34));
//				}
//				if (exceldatalist.get(i + 35).equalsIgnoreCase("na")) {
//					address.setPin(0l);
//				} else {
//					address.setPin(Long.parseLong(exceldatalist.get(i + 35)));
//				}
//				employee.setAddress(address);
//               
//				
//				Address address1=new Address();
//				
//				if (exceldatalist.get(i + 36).equalsIgnoreCase("na")) {
//
//					address1.setAddrLine1("");
//				} else {
//					address1.setAddrLine1(exceldatalist.get(i + 36));
//				}
//				if (exceldatalist.get(i + 37).equalsIgnoreCase("na")) {
//					address1.setAddrLine2("");
//				} else {
//					address1.setAddrLine2(exceldatalist.get(i + 37));
//				}
//				if (exceldatalist.get(i + 38).equalsIgnoreCase("na")) {
//					address1.setLandmark("");
//				} else {
//					address1.setLandmark(exceldatalist.get(i + 38));
//				}
//				if (exceldatalist.get(i + 39).equalsIgnoreCase("na")) {
//					address1.setCountry("");
//				} else {
//					address1.setCountry(exceldatalist.get(i + 39));
//					
//				}
//				if (exceldatalist.get(i + 40).equalsIgnoreCase("na")) {
//					address1.setState("");
//				} else {
//					address1.setState(exceldatalist.get(i + 40));
//				}
//				if (exceldatalist.get(i + 41).equalsIgnoreCase("na")) {
//					address1.setCity("");
//				} else {
//					address1.setCity(exceldatalist.get(i + 41));
//				}
//				if (exceldatalist.get(i + 42).equalsIgnoreCase("na")) {
//					address1.setLocality("");
//				} else {
//					address1.setLocality(exceldatalist.get(i + 42));
//				}
//				if (exceldatalist.get(i + 43).equalsIgnoreCase("na")) {
//					address1.setPin(0l);
//				} else {
//					address1.setPin(Long.parseLong(exceldatalist.get(i + 43)));
//				}
//				employee.setSecondaryAddress(address1);
//
//				
//				// /********Passport**************//
//				PassportInformation passport = new PassportInformation();
//				if (exceldatalist.get(i + 44).equalsIgnoreCase("na")) {
//					passport.setPassportNumber("");
//				} else {
//					passport.setPassportNumber(exceldatalist.get(i + 44));
//				}
//				if (exceldatalist.get(i + 45).equalsIgnoreCase("na")) {
//					passport.setIssuedAt("");
//				} else {
//					passport.setIssuedAt(exceldatalist.get(i + 45));
//				}
//
//				try {
//					if (exceldatalist.get(i + 46).equalsIgnoreCase("na")) {
//
//					} else {
//						passport.setIssueDate(fmt.parse(exceldatalist.get(i + 46)
//								.trim()));
//					}
//				} catch (ParseException e) {
//					e.printStackTrace();
//				}
//
//				try {
//					if (exceldatalist.get(i + 47).equalsIgnoreCase("na")) {
//
//					} else {
//						passport.setExpiryDate(fmt.parse(exceldatalist.get(i + 47)
//								.trim()));
//					}
//				} catch (ParseException e) {
//					e.printStackTrace();
//				}
//
//				employee.setPassPortInformation(passport);
//
//				// ///************Social
//				// Info********************////////////////////////
//				SocialInformation socialinfo = new SocialInformation();
//
//				if (exceldatalist.get(i + 48).equalsIgnoreCase("na")) {
//					socialinfo.setGooglePlusId("");
//				} else {
//					socialinfo.setGooglePlusId(exceldatalist.get(i + 48));
//				}
//				if (exceldatalist.get(i + 49).equalsIgnoreCase("na")) {
//					socialinfo.setFaceBookId("");
//				} else {
//					socialinfo.setFaceBookId(exceldatalist.get(i + 49));
//				}
//				if (exceldatalist.get(i + 50).equalsIgnoreCase("na")) {
//					socialinfo.setTwitterId("");
//				} else {
//					socialinfo.setTwitterId(exceldatalist.get(i + 50));
//				}
//				employee.setSocialInfo(socialinfo);
//				/**
//				 * Date : 04-05-2018 By ANIL
//				 */
//				if (exceldatalist.get(i + 51).equalsIgnoreCase("na")) {
//					employee.setGender("");
//				} else {
//					employee.setGender(exceldatalist.get(i + 51));
//				}
//				/**
//				 * Date : 15-05-2018 By ANIL
//				 */
//				if (exceldatalist.get(i + 52).equalsIgnoreCase("na")) {
//					employee.setNumberRange("");
//				} else {
//					employee.setNumberRange(exceldatalist.get(i + 52));
//				}
//				/**
//				 * Date 15-5-2019 By Amol
//				 */
//				if(exceldatalist.get(i+53).equalsIgnoreCase("na")){
//					employee.setEmpGrp("");
//				}else{
//					employee.setEmpGrp(exceldatalist.get(i+53));
//				}
//				
//				if(exceldatalist.get(i+54).equalsIgnoreCase("na")){
//					employee.setSalaryAmt(0d);
//				}else{
//					employee.setSalaryAmt(Double.parseDouble(exceldatalist.get(i+54)));
//				}
//				if(exceldatalist.get(i+55).equalsIgnoreCase("na")){
//					employee.setHusbandName("");
//				}else{
//					employee.setHusbandName(exceldatalist.get(i+55));
//				}
//
//				/**
//				 * @author Vijay Chougule Date :- 05-11-2020
//				 * Des :- if Duplicate employee found in the excel then giving validation message
//				 */
//				if(!exceldatalist.get(i + 3).equalsIgnoreCase("na")&&!exceldatalist.get(i + 22).equalsIgnoreCase("na")){
//					
//					if(hashduplicateEmplpoyee!=null && hashduplicateEmplpoyee.size()!=0){
//						if(hashduplicateEmplpoyee.containsKey(exceldatalist.get(i + 3).trim()+"-"+exceldatalist.get(i + 22).trim())){
//							int num = hashduplicateEmplpoyee.get(exceldatalist.get(i + 3).trim()+"-"+exceldatalist.get(i + 22).trim());
//						  num++;
//						  hashduplicateEmplpoyee.put(exceldatalist.get(i + 3).trim()+"-"+exceldatalist.get(i + 22).trim(),num);
//						  if(num>1){
//							  integerlist.add(-27);
//							  return integerlist;
//						  }
//						}
//						else{
//							hashduplicateEmplpoyee.put(exceldatalist.get(i + 3).trim()+"-"+exceldatalist.get(i + 22).trim(), 1);
//						}
//					}
//					else{
//						hashduplicateEmplpoyee.put(exceldatalist.get(i + 3).trim()+"-"+exceldatalist.get(i + 22).trim(), 1);
//					}
//				}
//				/**
//				 * ends here
//				 */
//				
//				employee.setCount(++count);
//				employee.setCompanyId(companyId);
//				employeelist.add(employee);
//			}
//
//			lastcount = ofy().load().type(Employee.class)
//					.filter("companyId", companyId).count();
//
//			ofy().save().entities(employeelist).now();
//
//			ng.setNumber(count);
//
//			GenricServiceImpl impl = new GenricServiceImpl();
//			impl.save(ng);
//
//			newcount = ofy().load().type(Employee.class)
//					.filter("companyId", companyId).count();
//			filerecord = employeelist.size();
//
//			insertedrecord = newcount - lastcount;
//
//			integerlist.add(filerecord);
//			integerlist.add(insertedrecord);

		/**
		 * @author Vijay Date :- 09-10-2020 Des :- updated code into task queue to save
		 *         large data
		 */
		// Change i from 40 to 41
		for (int i = 56; i < exceldatalist.size(); i += 56) {

			Employee employee = new Employee();

			boolean employeecheck = validationforemployee(listofemployee, exceldatalist, i, exceldatalist.get(i + 22));

			if (employeecheck == false) {
				integerlist.add(-7);
				return integerlist;
			}

			if (exceldatalist.get(i).equalsIgnoreCase("na")) {
				employee.setCountry("");
			} else {
				employee.setCountry(exceldatalist.get(i));
			}

			/**
			 * @author Vijay Chougule Date :- 05-11-2020 Des :- if Duplicate employee found
			 *         in the excel then giving validation message
			 */
			if (!exceldatalist.get(i + 3).equalsIgnoreCase("na") && !exceldatalist.get(i + 22).equalsIgnoreCase("na")) {

				if (hashduplicateEmplpoyee != null && hashduplicateEmplpoyee.size() != 0) {
					if (hashduplicateEmplpoyee
							.containsKey(exceldatalist.get(i + 3).trim() + "-" + exceldatalist.get(i + 22).trim())) {
						int num = hashduplicateEmplpoyee
								.get(exceldatalist.get(i + 3).trim() + "-" + exceldatalist.get(i + 22).trim());
						num++;
						hashduplicateEmplpoyee
								.put(exceldatalist.get(i + 3).trim() + "-" + exceldatalist.get(i + 22).trim(), num);
						if (num > 1) {
							integerlist.add(-27);
							return integerlist;
						}
					} else {
						hashduplicateEmplpoyee
								.put(exceldatalist.get(i + 3).trim() + "-" + exceldatalist.get(i + 22).trim(), 1);
					}
				} else {
					hashduplicateEmplpoyee.put(exceldatalist.get(i + 3).trim() + "-" + exceldatalist.get(i + 22).trim(),
							1);
				}
			}
			/**
			 * ends here
			 */

			employeelist.add(employee);
		}

		filerecord = employeelist.size();

		integerlist.add(filerecord);
		integerlist.add(filerecord);

		String msg = callTaskQueueForEmployeeUpload(companyId, null);
		logger.log(Level.SEVERE, "msg " + msg);

		return integerlist;

	}

	private boolean validationforemployee(List<Employee> listofemployee, ArrayList<String> exceldatalist, int i,
			String stradharNumber) {
		boolean value = true;
		long adharNumber = 0;
		System.out.println("stradharNumber == " + stradharNumber);
		if (!stradharNumber.equalsIgnoreCase("NA")) {
			adharNumber = Long.parseLong(stradharNumber);
		}
		System.out.println("excel adharNumber " + adharNumber);
		for (int j = 0; j < listofemployee.size(); j++) {

			if ((listofemployee.get(j).getFullName().trim().equalsIgnoreCase(exceldatalist.get(i + 3).trim())
					&& adharNumber == listofemployee.get(j).getAadharNumber())
			// && (listofemployee.get(j).getMiddleName().trim()
			// .equalsIgnoreCase(exceldatalist.get(i + 4).trim()))
			// && (listofemployee.get(j).getLastName().trim()
			// .equalsIgnoreCase(exceldatalist.get(i + 5).trim()))
			// || (listofemployee.get(j).getFirstName().trim()
			// .equalsIgnoreCase(exceldatalist.get(i + 3).trim()))
			// && (listofemployee.get(j).getLastName().trim()
			// .equalsIgnoreCase(exceldatalist.get(i + 5).trim()))
			) {

				return false;
			}
		}
		return value;
	}

	private ArrayList<Integer> vendorsave(long companyId, ArrayList<String> exceldatalist) {
		ServerAppUtility serverutitlity = new ServerAppUtility();
		// TODO Auto-generated method stub
		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		ArrayList<Vendor> vendorlist = new ArrayList<Vendor>();
		List<Vendor> listofvendor = ofy().load().type(Vendor.class).filter("companyId", companyId).list();

		int filerecords;
		int insertedrecords;
		int lastcount;
		int newcount;

		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId).filter("processName", "Vendor")
				.filter("status", true).first().now();

		long number = ng.getNumber();
		int count = (int) number;

		for (int i = 31; i < exceldatalist.size(); i += 31) {
			Vendor vendor = new Vendor();

			boolean vendorcheck = validationforvendor(listofvendor, exceldatalist, i);
			if (vendorcheck == false) {
				integerlist.add(-14);
				return integerlist;
			}

			if (exceldatalist.get(i).equalsIgnoreCase("na")) {
				vendor.setVendorName("");
			} else {
				vendor.setVendorName(exceldatalist.get(i));
			}

			if (exceldatalist.get(i + 1).equalsIgnoreCase("no")) {
				vendor.setVendorStatus(false);
			} else {
				vendor.setVendorStatus(true);
			}

//			Person person = new Person();
			if (exceldatalist.get(i + 2).equalsIgnoreCase("na")) {
				vendor.setfullName("");
			} else {
				vendor.setfullName(exceldatalist.get(i + 2));
			}
//			if (exceldatalist.get(i + 3).equalsIgnoreCase("na")) {
//				person.setMiddleName("");
//			} else {
//				person.setMiddleName(exceldatalist.get(i + 3));
//			}
//			if (exceldatalist.get(i + 4).equalsIgnoreCase("na")) {
//				person.setLastName("");
//			} else {
//				person.setLastName(exceldatalist.get(i + 4));
//			}

//			vendor.setPointOfContact(person);

			if (exceldatalist.get(i + 3).equalsIgnoreCase("na")) {
				vendor.setEmail("");
			} else {
				vendor.setEmail(exceldatalist.get(i + 3));
			}
			if (exceldatalist.get(i + 4).equalsIgnoreCase("na")) {
				vendor.setLandline(0l);
			} else {
				String error = serverutitlity.validateNumericCloumn(exceldatalist.get(4).trim(),
						exceldatalist.get(i + 4).trim());
				if (error.equals("")) {
					try {
						vendor.setLandline(Long.parseLong(exceldatalist.get(i + 4)));
					} catch (Exception e) {
						integerlist.add(-31);
						return integerlist;
					}

				} else {
					integerlist.add(-28);
					return integerlist;
				}

			}
			if (exceldatalist.get(i + 5).equalsIgnoreCase("na")) {
				vendor.setCellNumber1(0l);
			} else {
				String error = serverutitlity.validateNumericCloumn(exceldatalist.get(5).trim(),
						exceldatalist.get(i + 5).trim());
				if (error.equals("")) {
					try {
						vendor.setCellNumber1(Long.parseLong(exceldatalist.get(i + 5)));
					} catch (Exception e) {
						integerlist.add(-31);
						return integerlist;
					}
				} else {
					integerlist.add(-29);
					return integerlist;
				}
			}
			if (exceldatalist.get(i + 6).equalsIgnoreCase("na")) {
				vendor.setCellNumber2(0l);
			} else {
				String error = serverutitlity.validateNumericCloumn(exceldatalist.get(6).trim(),
						exceldatalist.get(i + 6).trim());
				if (error.equals("")) {

					try {
						vendor.setCellNumber2(Long.parseLong(exceldatalist.get(i + 6)));
					} catch (Exception e) {
						integerlist.add(-31);
						return integerlist;
					}
				} else {
					integerlist.add(-29);
					return integerlist;
				}

			}
			if (exceldatalist.get(i + 7).equalsIgnoreCase("na")) {
				vendor.setFaxNumber(0l);
			} else {
				String error = serverutitlity.validateNumericCloumn(exceldatalist.get(7).trim(),
						exceldatalist.get(i + 7).trim());
				if (error.equals("")) {

					try {
						vendor.setFaxNumber(Long.parseLong(exceldatalist.get(i + 7)));
					} catch (Exception e) {
						integerlist.add(-31);
						return integerlist;
					}
				} else {
					integerlist.add(-30);
					return integerlist;
				}
			}
			if (exceldatalist.get(i + 8).equalsIgnoreCase("na")) {
				vendor.setGroup("");
			} else {
				vendor.setGroup(exceldatalist.get(i + 8));
			}
			if (exceldatalist.get(i + 9).equalsIgnoreCase("na")) {
				vendor.setCategory("");
			} else {
				vendor.setCategory(exceldatalist.get(i + 9));
			}
			if (exceldatalist.get(i + 10).equalsIgnoreCase("na")) {
				vendor.setType("");
			} else {
				vendor.setType(exceldatalist.get(i + 10));
			}

			// ///****************social info***********????///////
			SocialInformation socialInfo = new SocialInformation();

			if (exceldatalist.get(i + 11).equalsIgnoreCase("na")) {
				socialInfo.setGooglePlusId("");
			} else {
				socialInfo.setGooglePlusId(exceldatalist.get(i + 11));
			}
			if (exceldatalist.get(i + 12).equalsIgnoreCase("na")) {
				socialInfo.setFaceBookId("");
			} else {
				socialInfo.setFaceBookId(exceldatalist.get(i + 12));
			}
			if (exceldatalist.get(i + 13).equalsIgnoreCase("na")) {
				socialInfo.setTwitterId("");
			} else {
				socialInfo.setTwitterId(exceldatalist.get(i + 13));
			}
			vendor.setSocialInfo(socialInfo);

			// *******************Address****************************//
			Address address = new Address();
			if (exceldatalist.get(i + 14).equalsIgnoreCase("na")) {

				address.setAddrLine1("");
			} else {
				address.setAddrLine1(exceldatalist.get(i + 14));
			}
			if (exceldatalist.get(i + 15).equalsIgnoreCase("na")) {
				address.setAddrLine2("");
			} else {
				address.setAddrLine2(exceldatalist.get(i + 15));
			}
			if (exceldatalist.get(i + 16).equalsIgnoreCase("na")) {
				address.setLandmark("");
			} else {
				address.setLandmark(exceldatalist.get(i + 16));
			}
			if (exceldatalist.get(i + 17).equalsIgnoreCase("na")) {
				address.setCountry("");
			} else {
				address.setCountry(exceldatalist.get(i + 17));
				;
			}
			if (exceldatalist.get(i + 18).equalsIgnoreCase("na")) {
				address.setState("");
			} else {
				address.setState(exceldatalist.get(i + 18));
			}
			if (exceldatalist.get(i + 19).equalsIgnoreCase("na")) {
				address.setCity("");
			} else {
				address.setCity(exceldatalist.get(i + 19));
			}
			if (exceldatalist.get(i + 20).equalsIgnoreCase("na")) {
				address.setLocality("");
			} else {
				address.setLocality(exceldatalist.get(i + 20));
			}
			if (exceldatalist.get(i + 21).equalsIgnoreCase("na")) {
				address.setPin(0l);
			} else {
				address.setPin(Long.parseLong(exceldatalist.get(i + 21)));
			}
			vendor.setPrimaryAddress(address);

			Address address1 = new Address();

			if (exceldatalist.get(i + 22).equalsIgnoreCase("na")) {

				address1.setAddrLine1("");
			} else {
				address1.setAddrLine1(exceldatalist.get(i + 22));
			}
			if (exceldatalist.get(i + 23).equalsIgnoreCase("na")) {
				address1.setAddrLine2("");
			} else {
				address1.setAddrLine2(exceldatalist.get(i + 23));
			}
			if (exceldatalist.get(i + 24).equalsIgnoreCase("na")) {
				address1.setLandmark("");
			} else {
				address1.setLandmark(exceldatalist.get(i + 24));
			}
			if (exceldatalist.get(i + 25).equalsIgnoreCase("na")) {
				address1.setCountry("");
			} else {
				address1.setCountry(exceldatalist.get(i + 25));
				;
			}
			if (exceldatalist.get(i + 26).equalsIgnoreCase("na")) {
				address1.setState("");
			} else {
				address1.setState(exceldatalist.get(i + 26));
			}
			if (exceldatalist.get(i + 27).equalsIgnoreCase("na")) {
				address1.setCity("");
			} else {
				address1.setCity(exceldatalist.get(i + 27));
			}
			if (exceldatalist.get(i + 28).equalsIgnoreCase("na")) {
				address1.setLocality("");
			} else {
				address1.setLocality(exceldatalist.get(i + 28));
			}
			if (exceldatalist.get(i + 29).equalsIgnoreCase("na")) {
				address1.setPin(0l);
			} else {
				address1.setPin(Long.parseLong(exceldatalist.get(i + 29)));
			}
			vendor.setSecondaryAddress(address);

			/**
			 * @author Vijay Date :- 24-06-2023 Des :- GST number added for vendor
			 */

			if (!exceldatalist.get(i + 30).equalsIgnoreCase("na")) {
				String gstinnumber = exceldatalist.get(i + 30);
				if (gstinnumber.length() < 15 || gstinnumber.length() > 15) {
					integerlist.add(-32);
					return integerlist;
				}
				ArrayList<ArticleType> articletypelist = new ArrayList<ArticleType>();

				for (int j = 0; j < 2; j++) {
					ArticleType articletype = new ArticleType();

					if (exceldatalist.get(i + 30).equalsIgnoreCase("na")) {
						articletype.setArticleTypeValue("");
					} else {
						articletype.setArticleTypeValue(exceldatalist.get(i + 30));
					}
					if (j == 0) {
						articletype.setDocumentName("Purchase Order");
					} else {
						articletype.setDocumentName("Invoice Details");
					}
					articletype.setArticleTypeName("GSTIN");
					articletype.setArticlePrint("Yes");

					articletypelist.add(articletype);
				}

				vendor.setArticleTypeDetails(articletypelist);
				/**
				 * ends here
				 */
			}

			vendor.setCount(++count);
			vendor.setCompanyId(companyId);
			vendorlist.add(vendor);
		}
		lastcount = ofy().load().type(Vendor.class).filter("companyId", companyId).count();

		ofy().save().entities(vendorlist).now();

		ng.setNumber(count);

		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(ng);

		newcount = ofy().load().type(Vendor.class).filter("companyId", companyId).count();
		filerecords = vendorlist.size();

		insertedrecords = newcount - lastcount;

		integerlist.add(filerecords);
		integerlist.add(insertedrecords);

		return integerlist;
	}

	private boolean validationforvendor(List<Vendor> listofvendor, ArrayList<String> exceldatalist, int i) {
		// TODO Auto-generated method stub
		boolean value = true;
		for (int j = 0; j < listofvendor.size(); j++) {
			if (listofvendor.get(j).getVendorName().trim().equalsIgnoreCase(exceldatalist.get(i).trim())) {
				value = false;
			}
		}
		return value;
	}

	private ArrayList<Integer> departmentsave(long companyId, ArrayList<String> exceldatalist) {
		// TODO Auto-generated method stub
		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		ArrayList<Department> departmentlist = new ArrayList<Department>();

		List<Department> listofdepartment = ofy().load().type(Department.class).filter("companyId", companyId).list();

		int fileRecords;
		int insertedRecords;
		int lastcount;
		int newcount;

		// if(entityname.equalsIgnoreCase("Customer")){

		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
				.filter("processName", "Department").filter("status", true).first().now();

		long number = ng.getNumber();
		int count = (int) number;

		for (int i = 16; i < exceldatalist.size(); i += 16) {
			Department department = new Department();

			boolean departmentcheck = validationfordepartment(listofdepartment, exceldatalist, i);
			if (departmentcheck == false) {
				integerlist.add(-6);
				return integerlist;
			}
			if (exceldatalist.get(i).equalsIgnoreCase("na")) {
				department.setDeptName("");
			} else {
				department.setDeptName(exceldatalist.get(i));
			}

			if (exceldatalist.get(i + 1).equalsIgnoreCase("na")) {
				department.setParentDeptName("");
			} else {
				department.setParentDeptName(exceldatalist.get(i + 1));
			}

			if (exceldatalist.get(i + 2).equalsIgnoreCase("na")) {
				department.setHodName("");
			} else {
				department.setHodName(exceldatalist.get(i + 2));
			}

			if (exceldatalist.get(i + 3).equalsIgnoreCase("no")) {
				department.setStatus(false);
			} else {
				department.setStatus(true);
			}

			if (exceldatalist.get(i + 4).equalsIgnoreCase("na")) {
				department.setDescription("");
			} else {
				department.setDescription(exceldatalist.get(i + 4));
			}

			if (exceldatalist.get(i + 5).equalsIgnoreCase("na")) {
				department.setLandline(0l);
			} else {
				department.setLandline(Long.parseLong(exceldatalist.get(i + 5)));
			}

			if (exceldatalist.get(i + 6).equalsIgnoreCase("na")) {
				department.setCellNo1(0l);
			} else {
				department.setCellNo1(Long.parseLong(exceldatalist.get(i + 6)));
			}

			if (exceldatalist.get(i + 7).equalsIgnoreCase("na")) {
				department.setCellNo2(0l);
			} else {
				department.setCellNo2(Long.parseLong(exceldatalist.get(i + 7)));
			}

			Address address = new Address();
			if (exceldatalist.get(i + 8).equalsIgnoreCase("na")) {

				address.setAddrLine1("");
			} else {
				address.setAddrLine1(exceldatalist.get(i + 8));
			}
			if (exceldatalist.get(i + 9).equalsIgnoreCase("na")) {
				address.setAddrLine2("");
			} else {
				address.setAddrLine2(exceldatalist.get(i + 9));
			}
			if (exceldatalist.get(i + 10).equalsIgnoreCase("na")) {
				address.setLandmark("");
			} else {
				address.setLandmark(exceldatalist.get(i + 10));
			}
			if (exceldatalist.get(i + 11).equalsIgnoreCase("na")) {
				address.setCountry("");
			} else {
				address.setCountry(exceldatalist.get(i + 11));
				;
			}
			if (exceldatalist.get(i + 12).equalsIgnoreCase("na")) {
				address.setState("");
			} else {
				address.setState(exceldatalist.get(i + 12));
			}
			if (exceldatalist.get(i + 13).equalsIgnoreCase("na")) {
				address.setCity("");
			} else {
				address.setCity(exceldatalist.get(i + 13));
			}
			if (exceldatalist.get(i + 14).equalsIgnoreCase("na")) {
				address.setLocality("");
			} else {
				address.setLocality(exceldatalist.get(i + 14));
			}
			if (exceldatalist.get(i + 15).equalsIgnoreCase("na")) {
				address.setPin(0l);
			} else {
				address.setPin(Long.parseLong(exceldatalist.get(i + 15)));
			}
			department.setAddress(address);

			department.setCount(++count);
			department.setCompanyId(companyId);
			departmentlist.add(department);
		}

		lastcount = ofy().load().type(Department.class).filter("companyId", companyId).count();

		ofy().save().entities(departmentlist).now();

		ng.setNumber(count);

		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(ng);

		newcount = ofy().load().type(Department.class).filter("companyId", companyId).count();
		fileRecords = departmentlist.size();

		insertedRecords = newcount - lastcount;

		integerlist.add(fileRecords);
		integerlist.add(insertedRecords);

		return integerlist;
	}

	private boolean validationfordepartment(List<Department> listofdepartment, ArrayList<String> exceldatalist, int i) {
		// TODO Auto-generated method stub
		boolean value = true;

		for (int j = 0; j < listofdepartment.size(); j++) {

			if ((listofdepartment.get(j).getDeptName().trim().equalsIgnoreCase(exceldatalist.get(i).trim()))
					&& (listofdepartment.get(j).getParentDeptName().trim()
							.equalsIgnoreCase(exceldatalist.get(i + 1).trim()))) {
				value = false;
			}
		}
		return value;
	}

	private ArrayList<Integer> warehousesave(long companyId, ArrayList<String> exceldatalist) {

		System.out.println("Reached in Warehouse Method");
		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		ArrayList<WareHouse> warehouselist = new ArrayList<WareHouse>();
		ArrayList<StorageLocation> storageLocList = new ArrayList<StorageLocation>();
		ArrayList<Storagebin> storageBinList = new ArrayList<Storagebin>();
		List<WareHouse> listofwareHouses = ofy().load().type(WareHouse.class).filter("companyId", companyId).list();

		// ArrayList<BranchDetails> branchDetails=new
		// ArrayList<BranchDetails>();

		int fileRecords;
		int insertedRecords;
		int lastcount;
		int newcount;

		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId).filter("processName", "WareHouse")
				.filter("status", true).first().now();
		System.out.println("Loaded the data in number gen(warehouse)");
		NumberGeneration ngStorageLoc = new NumberGeneration();
		ngStorageLoc = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
				.filter("processName", "StorageLocation").filter("status", true).first().now();

		NumberGeneration ngStorageBin = new NumberGeneration();
		ngStorageBin = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
				.filter("processName", "Storagebin").filter("status", true).first().now();

		long number = ng.getNumber();
		int count = (int) number;

		long numberStorageLoc = ngStorageLoc.getNumber();
		int countStorageLoc = (int) numberStorageLoc;

		long numberStorageBin = ngStorageBin.getNumber();
		int countStorageBin = (int) numberStorageBin;

		for (int i = 28; i < exceldatalist.size(); i += 28) {
			System.out.println("Inside for loop of warehouse Method");
			WareHouse warehouse = new WareHouse();

			boolean warehousecheck = validationcheckforwarehouse(listofwareHouses, exceldatalist, i);
			if (warehousecheck == false) {
				integerlist.add(-15);
				return integerlist;
			}
			if (exceldatalist.get(i).equalsIgnoreCase("na")) {
				warehouse.setBusinessUnitName("");
			} else {
				warehouse.setBusinessUnitName(exceldatalist.get(i));
			}

			if (exceldatalist.get(i + 1).equalsIgnoreCase("na")) {
				warehouse.setEmail("");
			} else {
				warehouse.setEmail(exceldatalist.get(i + 1));
			}
			if (exceldatalist.get(i + 2).equalsIgnoreCase("no")) {
				warehouse.setstatus(false);
			} else {
				warehouse.setstatus(true);
			}
			if (exceldatalist.get(i + 3).equalsIgnoreCase("na")) {
				warehouse.setLandline(0l);
			} else {
				warehouse.setLandline(Long.parseLong(exceldatalist.get(i + 3)));
			}
			if (exceldatalist.get(i + 4).equalsIgnoreCase("na")) {
				warehouse.setCellNumber1(0l);
			} else {
				warehouse.setCellNumber1(Long.parseLong(exceldatalist.get(i + 4)));
			}
			if (exceldatalist.get(i + 5).equalsIgnoreCase("na")) {
				warehouse.setCellNumber2(0l);
			} else {
				warehouse.setCellNumber2(Long.parseLong(exceldatalist.get(i + 5)));
			}
			if (exceldatalist.get(i + 6).equalsIgnoreCase("na")) {
				warehouse.setFaxNumber(0l);
			} else {
				warehouse.setFaxNumber(Long.parseLong(exceldatalist.get(i + 6)));
			}
			if (exceldatalist.get(i + 7).equalsIgnoreCase("na")) {
				warehouse.setPocName("");
			} else {
				warehouse.setPocName(exceldatalist.get(i + 7));
			}
			if (exceldatalist.get(i + 8).equalsIgnoreCase("na")) {
				warehouse.setPocLandline(0l);
			} else {
				warehouse.setPocLandline(Long.parseLong(exceldatalist.get(i + 8)));
			}
			if (exceldatalist.get(i + 9).equalsIgnoreCase("na")) {
				warehouse.setPocCell(0l);
			} else {
				warehouse.setPocCell(Long.parseLong(exceldatalist.get(i + 9)));
			}
			if (exceldatalist.get(i + 10).equalsIgnoreCase("na")) {
				warehouse.setEmail("");
			} else {
				warehouse.setEmail(exceldatalist.get(i + 10));
			}

			Address address = new Address();

			if (exceldatalist.get(i + 11).equalsIgnoreCase("na")) {

				address.setAddrLine1("");
			} else {
				address.setAddrLine1(exceldatalist.get(i + 11));
			}
			if (exceldatalist.get(i + 12).equalsIgnoreCase("na")) {
				address.setAddrLine2("");
			} else {
				address.setAddrLine2(exceldatalist.get(i + 12));
			}
			if (exceldatalist.get(i + 13).equalsIgnoreCase("na")) {
				address.setLandmark("");
			} else {
				address.setLandmark(exceldatalist.get(i + 13));
			}
			if (exceldatalist.get(i + 14).equalsIgnoreCase("na")) {
				address.setCountry("");
			} else {
				address.setCountry(exceldatalist.get(i + 14));

			}
			if (exceldatalist.get(i + 15).equalsIgnoreCase("na")) {
				address.setState("");
			} else {
				address.setState(exceldatalist.get(i + 15));
			}
			if (exceldatalist.get(i + 16).equalsIgnoreCase("na")) {
				address.setCity("");
			} else {
				address.setCity(exceldatalist.get(i + 16));
			}
			if (exceldatalist.get(i + 17).equalsIgnoreCase("na")) {
				address.setLocality("");
			} else {
				address.setLocality(exceldatalist.get(i + 17));
			}
			if (exceldatalist.get(i + 18).equalsIgnoreCase("na")) {
				address.setPin(0l);
			} else {
				address.setPin(Long.parseLong(exceldatalist.get(i + 18)));

			}

			warehouse.setAddress(address);

			ArrayList<BranchDetails> brandetailslist = new ArrayList<BranchDetails>();

			BranchDetails bdet1 = new BranchDetails();
			BranchDetails bdet2 = new BranchDetails();
			BranchDetails bdet3 = new BranchDetails();
			BranchDetails bdet4 = new BranchDetails();
			BranchDetails bdet5 = new BranchDetails();

			if (exceldatalist.get(i + 19).equalsIgnoreCase("na")) {
				bdet1.setBranchName("");
			} else {
				bdet1.setBranchName(exceldatalist.get(i + 19));
				branch = ofy().load().type(Branch.class).filter("companyId", companyId)
						.filter("buisnessUnitName", exceldatalist.get(i + 19)).first().now();
				System.out.println("Branch 1: " + branch);
				int branchid = branch.getCount();
				bdet1.setBranchID(branchid);
				String city = branch.getAddress().getCity();
				bdet1.setBranchAdds(city);
			}
			if (exceldatalist.get(i + 20).equalsIgnoreCase("na")) {
				bdet2.setBranchName("");

			} else {
				bdet2.setBranchName(exceldatalist.get(i + 20));
				branch = ofy().load().type(Branch.class).filter("companyId", companyId)
						.filter("buisnessUnitName", exceldatalist.get(i + 20)).first().now();
				System.out.println("Branch 2: " + branch);
				int branchid = branch.getCount();
				bdet2.setBranchID(branchid);
				String city = branch.getAddress().getCity();
				bdet2.setBranchAdds(city);
			}
			System.out.println("Reached Here");
			if (exceldatalist.get(i + 21).equalsIgnoreCase("na")) {
				bdet3.setBranchName("");
			} else {
				bdet3.setBranchName(exceldatalist.get(i + 21));
				branch = ofy().load().type(Branch.class).filter("companyId", companyId)
						.filter("buisnessUnitName", exceldatalist.get(i + 21)).first().now();
				System.out.println("Branch 3: " + branch);
				int branchid = branch.getCount();
				String city = branch.getAddress().getCity();
				bdet3.setBranchID(branchid);
				bdet3.setBranchAdds(city);
			}
			if (exceldatalist.get(i + 22).equalsIgnoreCase("na")) {
				bdet4.setBranchName("");

			} else {
				bdet4.setBranchName(exceldatalist.get(i + 22));
				System.out.println("Excel Dta" + exceldatalist.get(i + 22));
				branch = ofy().load().type(Branch.class).filter("companyId", companyId)
						.filter("buisnessUnitName", exceldatalist.get(i + 22)).first().now();
				System.out.println("Branch 4: " + branch);
				int branchid = branch.getCount();
				String city = branch.getAddress().getCity();
				bdet4.setBranchID(branchid);
				bdet4.setBranchAdds(city);
			}
			if (exceldatalist.get(i + 23).equalsIgnoreCase("na")) {
				bdet5.setBranchName("");

			} else {
				bdet5.setBranchName(exceldatalist.get(i + 23));
				branch = ofy().load().type(Branch.class).filter("companyId", companyId)
						.filter("buisnessUnitName", exceldatalist.get(i + 23)).first().now();
				System.out.println("Branch 3: " + branch);
				int branchid = branch.getCount();
				String city = branch.getAddress().getCity();
				bdet5.setBranchID(branchid);
				bdet5.setBranchAdds(city);
			}

			if (!bdet1.getBranchName().trim().equals("")) {
				brandetailslist.add(bdet1);
			}
			if (!bdet2.getBranchName().trim().equals("")) {
				brandetailslist.add(bdet2);
			}
			if (!bdet3.getBranchName().trim().equals("")) {
				brandetailslist.add(bdet3);
			}
			if (!bdet4.getBranchName().trim().equals("")) {
				brandetailslist.add(bdet4);
			}
			if (!bdet5.getBranchName().trim().equals("")) {
				brandetailslist.add(bdet5);
			}

			warehouse.setBranchdetails(brandetailslist);

			if (exceldatalist.get(i + 24).equalsIgnoreCase("na")) {
				warehouse.setWarehouseCode(exceldatalist.get(i + 24).trim());
			} else {
				warehouse.setWarehouseCode(exceldatalist.get(i + 24).trim());

			}
			if (exceldatalist.get(i + 25).equalsIgnoreCase("na")) {
				warehouse.setWarehouseType(exceldatalist.get(i + 25).trim());
			} else {
				warehouse.setWarehouseType(exceldatalist.get(i + 25).trim());

			}
			if (exceldatalist.get(i + 26).equalsIgnoreCase("na")) {
				warehouse.setParentWarehouse(exceldatalist.get(i + 26).trim());
			} else {
				warehouse.setParentWarehouse(exceldatalist.get(i + 26).trim());

			}
			try {
				if (exceldatalist.get(i + 27).equalsIgnoreCase("na")) {

				} else {
					warehouse.setWarehouseExpiryDate(fmt.parse(exceldatalist.get(i + 27).trim()));
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}

			warehouse.setCount(++count);
			warehouse.setCompanyId(companyId);
			warehouselist.add(warehouse);

			/*
			 * Storage Location Creation
			 */
			StorageLocation storageLocation = new StorageLocation();
			storageLocation.setBusinessUnitName(warehouse.getBusinessUnitName() + " Location");
			storageLocation.setWarehouseName(warehouse.getBusinessUnitName());

			storageLocation.setstatus(true);

			storageLocation.setPocName(warehouse.getPocName());

			storageLocation.setPocLandline(warehouse.getPocLandline());
			storageLocation.setPocCell(warehouse.getPocCell());

			storageLocation.setPocEmail(warehouse.getPocEmail());

			storageLocation.setAddress(warehouse.getAddress());

			storageLocation.setCompanyId(companyId);

			storageLocation.setCount(++countStorageLoc);

			storageLocList.add(storageLocation);
			/*
			 * Storage Location Creation Done
			 */
			/*
			 * Storage Bin Creation
			 */
			Storagebin storageBin = new Storagebin();
			storageBin.setBinName(warehouse.getBusinessUnitName() + " Bin");

			storageBin.setXcoordinate(1 + "");
			storageBin.setYcoordinate(1 + "");

			storageBin.setStatus(warehouse.getstatus());
			storageBin.setStoragelocation(warehouse.getBusinessUnitName() + " Location");
			storageBin.setWarehouseName(warehouse.getBusinessUnitName());
			storageBin.setCompanyId(companyId);
			storageBin.setCount(++countStorageBin);

			storageBinList.add(storageBin);
			/*
			 * Storage Bin Creation Done
			 */

		}
		lastcount = ofy().load().type(WareHouse.class).filter("companyId", companyId).count();

		logger.log(Level.SEVERE, "warehouse List::::" + warehouselist.size());
		logger.log(Level.SEVERE, "storageBinList List::::" + storageLocList.size());
		logger.log(Level.SEVERE, "storageBinList List::::" + storageBinList.size());

		ofy().save().entities(warehouselist).now();
		ofy().save().entities(storageLocList).now();
		ofy().save().entities(storageBinList).now();

		ng.setNumber(count);
		ngStorageLoc.setNumber(countStorageLoc);
		ngStorageBin.setNumber(countStorageBin);

		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(ng);
		impl.save(ngStorageLoc);
		impl.save(ngStorageBin);

		newcount = ofy().load().type(WareHouse.class).filter("companyId", companyId).count();
		fileRecords = warehouselist.size();

		insertedRecords = newcount - lastcount;

		integerlist.add(fileRecords);
		integerlist.add(insertedRecords);

		// TODO Auto-generated method stub
		return integerlist;

	}

	private boolean validationcheckforwarehouse(List<WareHouse> listofwareHouses, ArrayList<String> exceldatalist,
			int i) {
		// TODO Auto-generated method stub
		boolean value = true;
		for (int j = 0; j < listofwareHouses.size(); j++) {
			if (listofwareHouses.get(j).getBusinessUnitName().trim().equalsIgnoreCase(exceldatalist.get(i).trim())) {
				value = false;
			}
		}
		return value;
	}

	private ArrayList<Integer> storagelocationsave(long companyId, ArrayList<String> exceldatalist) {
		// TODO Auto-generated method stub
		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		ArrayList<StorageLocation> storageloclist = new ArrayList<StorageLocation>();
		List<StorageLocation> liststoragelocation = ofy().load().type(StorageLocation.class)
				.filter("companyId", companyId).list();

		int fileRecords;
		int insertedRecords;
		int lastcount;
		int newcount;

		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
				.filter("processName", "StorageLocation").filter("status", true).first().now();

		long number = ng.getNumber();
		int count = (int) number;

		for (int i = 20; i < exceldatalist.size(); i += 20) {

			StorageLocation storagelocation = new StorageLocation();

			boolean storageloccheck = validationforstoragelocation(liststoragelocation, exceldatalist, i);
			if (storageloccheck == false) {
				integerlist.get(-13);
				return integerlist;
			}
			if (exceldatalist.get(i).equalsIgnoreCase("na")) {
				storagelocation.setWarehouseName("");
			} else {
				storagelocation.setWarehouseName(exceldatalist.get(i));
			}
			if (exceldatalist.get(i + 1).equalsIgnoreCase("na")) {
				storagelocation.setBusinessUnitName("");
			} else {
				storagelocation.setBusinessUnitName(exceldatalist.get(i + 1));
			}
			if (exceldatalist.get(i + 2).equalsIgnoreCase("na")) {
				storagelocation.setEmail("");
			} else {
				storagelocation.setEmail(exceldatalist.get(i + 2));
			}
			if (exceldatalist.get(i + 3).equalsIgnoreCase("na")) {
				storagelocation.setstatus(false);
			} else {
				storagelocation.setstatus(true);
			}
			if (exceldatalist.get(i + 4).equalsIgnoreCase("na")) {
				storagelocation.setLandline(0l);
			} else {
				storagelocation.setLandline(Long.parseLong(exceldatalist.get(i + 4)));
			}
			if (exceldatalist.get(i + 5).equalsIgnoreCase("na")) {
				storagelocation.setCellNumber1(0l);
			} else {
				storagelocation.setCellNumber1(Long.parseLong(exceldatalist.get(i + 5)));
			}
			if (exceldatalist.get(i + 6).equalsIgnoreCase("na")) {
				storagelocation.setCellNumber2(0l);
			} else {
				storagelocation.setCellNumber2(Long.parseLong(exceldatalist.get(i + 6)));
			}
			if (exceldatalist.get(i + 7).equalsIgnoreCase("na")) {
				storagelocation.setFaxNumber(0l);
			} else {
				storagelocation.setFaxNumber(Long.parseLong(exceldatalist.get(i + 7)));
			}
			if (exceldatalist.get(i + 8).equalsIgnoreCase("na")) {
				storagelocation.setPocName("");
			} else {
				storagelocation.setPocName(exceldatalist.get(i + 8));
			}
			if (exceldatalist.get(i + 9).equalsIgnoreCase("na")) {
				storagelocation.setPocLandline(0l);
			} else {
				storagelocation.setPocLandline(Long.parseLong(exceldatalist.get(i + 9)));
			}
			if (exceldatalist.get(i + 10).equalsIgnoreCase("na")) {
				storagelocation.setPocCell(0l);
			} else {
				storagelocation.setPocCell(Long.parseLong(exceldatalist.get(i + 10)));
			}
			if (exceldatalist.get(i + 11).equalsIgnoreCase("na")) {
				storagelocation.setPocEmail("");
			} else {
				storagelocation.setPocEmail(exceldatalist.get(i + 11));
			}
			Address address = new Address();

			if (exceldatalist.get(i + 12).equalsIgnoreCase("na")) {

				address.setAddrLine1("");
			} else {
				address.setAddrLine1(exceldatalist.get(i + 12));
			}
			if (exceldatalist.get(i + 13).equalsIgnoreCase("na")) {
				address.setAddrLine2("");
			} else {
				address.setAddrLine2(exceldatalist.get(i + 13));
			}
			if (exceldatalist.get(i + 14).equalsIgnoreCase("na")) {
				address.setLandmark("");
			} else {
				address.setLandmark(exceldatalist.get(i + 14));
			}
			if (exceldatalist.get(i + 15).equalsIgnoreCase("na")) {
				address.setCountry("");
			} else {
				address.setCountry(exceldatalist.get(i + 15));

			}
			if (exceldatalist.get(i + 16).equalsIgnoreCase("na")) {
				address.setState("");
			} else {
				address.setState(exceldatalist.get(i + 16));
			}
			if (exceldatalist.get(i + 17).equalsIgnoreCase("na")) {
				address.setCity("");
			} else {
				address.setCity(exceldatalist.get(i + 17));
			}
			if (exceldatalist.get(i + 18).equalsIgnoreCase("na")) {
				address.setLocality("");
			} else {
				address.setLocality(exceldatalist.get(i + 18));
			}
			if (exceldatalist.get(i + 19).equalsIgnoreCase("na")) {
				address.setPin(0l);
			} else {
				address.setPin(Long.parseLong(exceldatalist.get(i + 19)));

			}
			storagelocation.setAddress(address);

			storagelocation.setCount(++count);
			storagelocation.setCompanyId(companyId);
			storageloclist.add(storagelocation);
		}
		lastcount = ofy().load().type(StorageLocation.class).filter("companyId", companyId).count();

		ofy().save().entities(storageloclist).now();

		ng.setNumber(count);

		GenricServiceImpl impl = new GenricServiceImpl();

		impl.save(ng);

		newcount = ofy().load().type(StorageLocation.class).filter("companyId", companyId).count();

		fileRecords = storageloclist.size();

		insertedRecords = newcount - lastcount;

		integerlist.add(fileRecords);
		integerlist.add(insertedRecords);

		return integerlist;
	}

	private boolean validationforstoragelocation(List<StorageLocation> liststoragelocation,
			ArrayList<String> exceldatalist, int i) {
		// TODO Auto-generated method stub
		boolean value = true;

		for (int j = 0; j < liststoragelocation.size(); j++) {
			if (liststoragelocation.get(j).getBusinessUnitName().trim().equalsIgnoreCase(exceldatalist.get(i + 1))) {
				value = false;
			}
		}
		return value;
	}

	private ArrayList<Integer> storagebinsave(long companyId, ArrayList<String> exceldatalist) {
		// TODO Auto-generated method stub
		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		ArrayList<Storagebin> storagebinlist = new ArrayList<Storagebin>();

		List<Storagebin> listofstoragebin = ofy().load().type(Storagebin.class).filter("companyId", companyId).list();

		int fileRecords;
		int insertedRecords;
		int lastcount;
		int newcount;

		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
				.filter("processName", "Storagebin").filter("status", true).first().now();
		long number = ng.getNumber();
		int count = (int) number;

		for (int i = 6; i < exceldatalist.size(); i += 6) {

			boolean storagebincheck = validationforstoragebin(listofstoragebin, exceldatalist, i);

			if (storagebincheck == false) {
				integerlist.add(-12);
				return integerlist;
			}
			Storagebin storagebin = new Storagebin();
			if (exceldatalist.get(i).equalsIgnoreCase("na")) {
				storagebin.setBinName("");
			} else {
				storagebin.setBinName(exceldatalist.get(i));
			}
			if (exceldatalist.get(i + 1).equalsIgnoreCase("na")) {
				storagebin.setXcoordinate("");
			} else {
				storagebin.setXcoordinate(exceldatalist.get(i + 1));
			}
			if (exceldatalist.get(i + 2).equalsIgnoreCase("na")) {
				storagebin.setYcoordinate("");
			} else {
				storagebin.setYcoordinate(exceldatalist.get(i + 2));
			}
			if (exceldatalist.get(i + 3).equalsIgnoreCase("no")) {
				storagebin.setStatus(false);
			} else {
				storagebin.setStatus(true);
			}
			if (exceldatalist.get(i + 4).equalsIgnoreCase("na")) {
				storagebin.setStoragelocation("");
			} else {
				storagebin.setStoragelocation(exceldatalist.get(i + 4));
			}
			if (exceldatalist.get(i + 5).equalsIgnoreCase("na")) {
				storagebin.setDescription("");
			} else {
				storagebin.setDescription(exceldatalist.get(i + 5));
			}

			storagebin.setCount(++count);
			storagebin.setCompanyId(companyId);
			storagebinlist.add(storagebin);

		}

		lastcount = ofy().load().type(Storagebin.class).filter("companyId", companyId).count();

		ofy().save().entities(storagebinlist).now();

		ng.setNumber(count);

		GenricServiceImpl impl = new GenricServiceImpl();

		impl.save(ng);

		newcount = ofy().load().type(Storagebin.class).filter("companyId", companyId).count();

		fileRecords = storagebinlist.size();

		insertedRecords = newcount - lastcount;

		integerlist.add(fileRecords);
		integerlist.add(insertedRecords);

		return integerlist;
	}

	private boolean validationforstoragebin(List<Storagebin> listofstoragebin, ArrayList<String> exceldatalist, int i) {
		// TODO Auto-generated method stub
		boolean value = true;
		for (int j = 0; j < listofstoragebin.size(); j++) {
			if (listofstoragebin.get(j).getBinName().trim().equalsIgnoreCase(exceldatalist.get(i).trim())) {
				value = false;
			}
		}
		return value;
	}

	private ArrayList<Integer> contactpersonsave(long companyId, ArrayList<String> exceldatalist) {
		// TODO Auto-generated method stub
		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		ArrayList<ContactPersonIdentification> ContactPersonIdentificationlist = new ArrayList<ContactPersonIdentification>();
		// List<ContactPersonIdentification>
		// listofcontactperson=ofy().load().type(ContactPersonIdentification.class).filter("companyId",
		// companyId).list();

		int fileRecords;
		int insertedRecords;
		int lastcount;
		int newcount;

		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
				.filter("processName", "ContactPersonIdentification").filter("status", true).first().now();
		long number = ng.getNumber();
		int count = (int) number;

		for (int i = 20; i < exceldatalist.size(); i += 20) {
			ContactPersonIdentification contactperson = new ContactPersonIdentification();

			if (exceldatalist.get(i).equalsIgnoreCase("na")) {
				contactperson.setEntype("");
			} else {
				contactperson.setEntype(exceldatalist.get(i));
			}

			if (exceldatalist.get(i + 1).equalsIgnoreCase("na")) {
				contactperson.setRole("");
			} else {
				contactperson.setRole(exceldatalist.get(i + 1));
			}
			if (exceldatalist.get(i + 2).equalsIgnoreCase("na")) {
				contactperson.setName("");
			} else {
				contactperson.setName(exceldatalist.get(i + 2));
			}
			if (exceldatalist.get(i + 3).equalsIgnoreCase("na")) {
				contactperson.setCell(0l);
			} else {
				contactperson.setCell(Long.parseLong(exceldatalist.get(i + 3)));
			}
			if (exceldatalist.get(i + 4).equalsIgnoreCase("na")) {
				contactperson.setCell1(0l);
			} else {
				contactperson.setCell1(Long.parseLong(exceldatalist.get(i + 4)));
			}
			if (exceldatalist.get(i + 5).equalsIgnoreCase("na")) {
				contactperson.setLandline(0l);
			} else {
				contactperson.setLandline(Long.parseLong(exceldatalist.get(i + 5)));
			}
			if (exceldatalist.get(i + 6).equalsIgnoreCase("na")) {
				contactperson.setFax(0l);
			} else {
				contactperson.setFax(Long.parseLong(exceldatalist.get(i + 6)));
			}
			if (exceldatalist.get(i + 7).equalsIgnoreCase("na")) {
				contactperson.setEmail("");
			} else {
				contactperson.setEmail(exceldatalist.get(i + 7));
			}
			if (exceldatalist.get(i + 8).equalsIgnoreCase("na")) {
				contactperson.setEmail1("");
			} else {
				contactperson.setEmail1(exceldatalist.get(i + 8));
			}

			Address address = new Address();
			if (exceldatalist.get(i + 9).equalsIgnoreCase("na")) {

				address.setAddrLine1("");
			} else {
				address.setAddrLine1(exceldatalist.get(i + 9));
			}
			if (exceldatalist.get(i + 10).equalsIgnoreCase("na")) {
				address.setAddrLine2("");
			} else {
				address.setAddrLine2(exceldatalist.get(i + 10));
			}
			if (exceldatalist.get(i + 11).equalsIgnoreCase("na")) {
				address.setLandmark("");
			} else {
				address.setLandmark(exceldatalist.get(i + 11));
			}
			if (exceldatalist.get(i + 12).equalsIgnoreCase("na")) {
				address.setCountry("");
			} else {
				address.setCountry(exceldatalist.get(i + 12));
				;
			}
			if (exceldatalist.get(i + 13).equalsIgnoreCase("na")) {
				address.setState("");
			} else {
				address.setState(exceldatalist.get(i + 13));
			}
			if (exceldatalist.get(i + 14).equalsIgnoreCase("na")) {
				address.setCity("");
			} else {
				address.setCity(exceldatalist.get(i + 14));
			}
			if (exceldatalist.get(i + 15).equalsIgnoreCase("na")) {
				address.setLocality("");
			} else {
				address.setLocality(exceldatalist.get(i + 15));
			}
			if (exceldatalist.get(i + 16).equalsIgnoreCase("na")) {
				address.setPin(0l);
			} else {
				address.setPin(Long.parseLong(exceldatalist.get(i + 16)));
			}
			contactperson.setAddressInfo(address);

			SocialInformation socialinfo = new SocialInformation();

			if (exceldatalist.get(i + 17).equalsIgnoreCase("na")) {
				socialinfo.setGooglePlusId("");
			} else {
				socialinfo.setGooglePlusId(exceldatalist.get(i + 17));
			}

			if (exceldatalist.get(i + 18).equalsIgnoreCase("na")) {
				socialinfo.setFaceBookId("");
			} else {
				socialinfo.setFaceBookId(exceldatalist.get(i + 18));
			}
			if (exceldatalist.get(i + 19).equalsIgnoreCase("na")) {
				socialinfo.setTwitterId("");
			} else {
				socialinfo.setTwitterId(exceldatalist.get(i + 19));
			}
			contactperson.setSocialInfo(socialinfo);

			contactperson.setCount(++count);
			contactperson.setCompanyId(companyId);
			ContactPersonIdentificationlist.add(contactperson);

		}
		lastcount = ofy().load().type(ContactPersonIdentification.class).filter("companyId", companyId).count();

		ofy().save().entities(ContactPersonIdentificationlist).now();

		ng.setNumber(count);

		GenricServiceImpl impl = new GenricServiceImpl();

		impl.save(ng);

		newcount = ofy().load().type(ContactPersonIdentification.class).filter("companyId", companyId).count();
		fileRecords = ContactPersonIdentificationlist.size();

		insertedRecords = newcount - lastcount;

		integerlist.add(fileRecords);
		integerlist.add(insertedRecords);

		return integerlist;
	}

	// *******************************rahul
	// *********************************************************

	/*************************************
	 * This is for Configurations Excel upload Part*************************&
	 ***/

	@Override
	public ArrayList<Integer> savedConfigDetails(long companyId, String entityname) {
		// TODO Auto-generated method stub
		ArrayList<Integer> intlist = new ArrayList<Integer>();
		ArrayList<String> exceldatalist = new ArrayList<String>();
		DataFormatter datafmt2 = new DataFormatter();
		/***************************************
		 * Reading Excel File
		 *****************************************/
		// ////
		try {
			long i;

			Workbook wb = null;

			try {
				wb = WorkbookFactory.create(new BlobstoreInputStream(blobkey));
			} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Sheet sheet = wb.getSheetAt(0);

			Iterator<Row> rowIterator = sheet.iterator();

			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();

				Iterator<Cell> celliterator = row.cellIterator();

				while (celliterator.hasNext()) {
					Cell cell = celliterator.next();

					switch (cell.getCellType()) {

					case Cell.CELL_TYPE_STRING:
						exceldatalist.add(cell.getStringCellValue());
						break;

					case Cell.CELL_TYPE_NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) {
							exceldatalist.add(fmt.format(cell.getDateCellValue()));

						} else {
							cell.getNumericCellValue();
							String sampletest = datafmt2.formatCellValue(cell);
							// i = (long) cell.getNumericCellValue();
							exceldatalist.add(sampletest + "");
							System.out.println("Value:" + sampletest);
						}
						break;

					case Cell.CELL_TYPE_BOOLEAN:
						break;
					default:

					}
					System.out.println("List Size " + exceldatalist.size());
				}
			}

			logger.log(Level.SEVERE, "FileString List Size11:" + exceldatalist.size());

			wb.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		/******************** Finished Reading Excel Sheet *************************/
		// ////

		if (entityname.equals("Category")) {

			if (exceldatalist.get(0).trim().equalsIgnoreCase("Entity Name")
					&& exceldatalist.get(1).trim().equalsIgnoreCase("Category Code")
					&& exceldatalist.get(2).trim().equalsIgnoreCase("Category Name")
					&& exceldatalist.get(3).trim().equalsIgnoreCase("Status")
					&& exceldatalist.get(4).trim().equalsIgnoreCase("Description")) {
				System.out.println("inside category save call");
				logger.log(Level.SEVERE, "inside category header");
				intlist = categorysave(companyId, exceldatalist);
			} else {
				logger.log(Level.SEVERE, "inside else category header");
				intlist.add(-1);

			}

		}
		/**
		 * Date : 12-10-2017 BY ANIL Added field category code in upload
		 */
		if (entityname.equalsIgnoreCase("Type")) {
			if (exceldatalist.get(0).trim().equalsIgnoreCase("Entity Type")
					&& exceldatalist.get(1).trim().equalsIgnoreCase("Category")
					&& exceldatalist.get(2).trim().equalsIgnoreCase("Type Code")
					&& exceldatalist.get(3).equalsIgnoreCase("Type Name")
					&& exceldatalist.get(4).equalsIgnoreCase("Status")
					&& exceldatalist.get(5).trim().equalsIgnoreCase("Description")
					&& exceldatalist.get(6).trim().equalsIgnoreCase("Category Code")) {
				intlist = typesave(companyId, exceldatalist);
			} else {
				intlist.add(-1);
			}

		}

		if (entityname.equalsIgnoreCase("Configurations")) {
			System.out.println("Inside Config save call");
			if (exceldatalist.get(0).trim().equalsIgnoreCase("Entity Name")
					&& exceldatalist.get(1).trim().equalsIgnoreCase("Name")
					&& exceldatalist.get(2).trim().equalsIgnoreCase("Status")) {
				intlist = configsave(companyId, exceldatalist);
			} else {
				intlist.add(-1);
			}

		}
		if (entityname.equalsIgnoreCase("Cron Job Configrations")) {
			System.out.println("Inside Config save call");
			if (exceldatalist.get(0).trim().equalsIgnoreCase("Cron Job Name")
					&& exceldatalist.get(1).trim().equalsIgnoreCase("Description")

					&& exceldatalist.get(2).trim().equalsIgnoreCase("Status")) {
				intlist = cronConfigsave(companyId, exceldatalist);
			} else {
				intlist.add(-1);
			}

		}
		return intlist;
	}

	private ArrayList<Integer> categorysave(long companyId, ArrayList<String> exceldatalist) {
		// TODO Auto-generated method stub
		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		ArrayList<ConfigCategory> categorylist = new ArrayList<ConfigCategory>();
		errorList = new ArrayList<>();
		logger.log(Level.SEVERE, "inside  categorysave value");
		int fileRecords;
		int insertedRecords;
		int lastcount;
		int newcount;
		List<ConfigCategory> listOfConfigCategory = ofy().load().type(ConfigCategory.class)
				.filter("companyId", companyId).filter("internalType", 13).list();

		NumberGeneration ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
				.filter("processName", "ConfigCategory").filter("status", true).first().now();

		long number = ng.getNumber();
		int count = (int) number;

		for (int i = 5; i < exceldatalist.size(); i += 5) {

			String error = "";
			if (exceldatalist.get(i).equalsIgnoreCase("NA")) {
				error += "Entity name is Mandatory";
				errorList.add(exceldatalist.get(i + 2));
				errorList.add(error);
				continue;
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Customer")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(1);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Lead")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(2);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Quotation")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(3);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Contract")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(4);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Vendor")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(5);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Complain")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(6);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Expense")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(7);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Billing")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(8);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Sales Quotation")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(9);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Sales Order")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(10);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("GRN")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(10);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Delivery Note")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(12);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Module Name")) {
				logger.log(Level.SEVERE, "inside  Module Name");

				if (exceldatalist.get(i + 1).equalsIgnoreCase("NA")) {
					error += "Category code is Mandatory";
					errorList.add(exceldatalist.get(i + 2));
					errorList.add(error);
					continue;
				} else {
					String strEmp = validateCategoryCode(listOfConfigCategory, exceldatalist.get(i + 1).trim());
					System.out.println("strEmp ==" + strEmp);
					if (!strEmp.equals("")) {
						error += strEmp;
						errorList.add(exceldatalist.get(i + 2));
						errorList.add(error);
						continue;
					}
				}

				if (exceldatalist.get(i + 2).equalsIgnoreCase("NA")) {
					error += "Category name is Mandatory";
					errorList.add(exceldatalist.get(i + 2));
					errorList.add(error);
					continue;
				} else {
					String strEmp = validateCategoryName(listOfConfigCategory, exceldatalist.get(i + 2).trim());
					System.out.println("strEmp ==" + strEmp);
					if (!strEmp.equals("")) {
						error += strEmp;
						errorList.add(exceldatalist.get(i + 2));
						errorList.add(error);
						continue;
					}
				}

				ConfigCategory category = new ConfigCategory();

				category.setInternalType(13);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Purchase Order")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(14);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Letter of Intent")
					|| exceldatalist.get(i).trim().equalsIgnoreCase("LOI")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(15);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Request for Quotation")
					|| exceldatalist.get(i).trim().equalsIgnoreCase("RFQ")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(16);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Purchase Requisition")
					|| exceldatalist.get(i).trim().equalsIgnoreCase("PR")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(17);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Material Request Note")
					|| exceldatalist.get(i).trim().equalsIgnoreCase("MRN")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(18);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Material Issue Note")
					|| exceldatalist.get(i).trim().equalsIgnoreCase("MIN")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(19);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Material Movement Note")
					|| exceldatalist.get(i).trim().equalsIgnoreCase("MMN")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(20);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Ticket")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(21);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Inspection")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(22);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Work Order")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(23);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Invoice")) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(24);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					category.setCategoryCode("");
				} else {
					category.setCategoryCode(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i + 2));
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 4));
				}

				category.setCount(++count);
				category.setCompanyId(companyId);
				if (!error.equals("")) {
					errorList.add(exceldatalist.get(i));
					errorList.add(error);
				}
				categorylist.add(category);
			}
		}

		CsvWriter.updatedCategoryList = errorList;
		if (errorList != null && errorList.size() != 0) {
			integerlist.add(-1);
			return integerlist;
		}

		lastcount = ofy().load().type(ConfigCategory.class).filter("companyId", companyId).count();

		ofy().save().entities(categorylist).now();

		ng.setNumber(count);

		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(ng);

		newcount = ofy().load().type(ConfigCategory.class).filter("companyId", companyId).count();

		fileRecords = categorylist.size();

		insertedRecords = newcount - lastcount;

		integerlist.add(fileRecords);
		integerlist.add(insertedRecords);

		return integerlist;
	}

	private String validateCategoryCode(List<ConfigCategory> listOfConfigCategory, String catCode) {
		String str = "";

		for (ConfigCategory obj : listOfConfigCategory) {

			str = "";
			if (obj.getCategoryCode().trim().equals(catCode)) {
				str = " Duplicate category code";
				logger.log(Level.SEVERE, "errrorrrr 11" + str);
				break;
			} else {
				continue;
			}
		}
		logger.log(Level.SEVERE, "return error value " + str);
		return str;

	}

	private String validateCategoryName(List<ConfigCategory> listOfConfigCategory, String catName) {
		String str = "";

		for (ConfigCategory obj : listOfConfigCategory) {

			str = "";
			if (obj.getCategoryName().trim().equals(catName)) {
				str = " Duplicate category Name";
				logger.log(Level.SEVERE, "errrorrrr 11" + str);
				break;
			} else {
				continue;
			}
		}
		logger.log(Level.SEVERE, "return error value " + str);
		return str;

	}

	private ArrayList<Integer> typesave(long companyId, ArrayList<String> exceldatalist) {
		// TODO Auto-generated method stub
		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		ArrayList<Type> typelist = new ArrayList<Type>();
		errorList = new ArrayList<String>();
		int fileRecords;
		int insertedRecords;
		int lastcount;
		int newcount;
		List<Type> listOfType = ofy().load().type(Type.class).filter("companyId", companyId).filter("internalType", 13)
				.list();

		List<ConfigCategory> listOfConfigCategory1 = ofy().load().type(ConfigCategory.class)
				.filter("companyId", companyId).filter("internalType", 13).list();

		NumberGeneration ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
				.filter("processName", "Type").filter("status", true).first().now();
		long number = ng.getNumber();
		int count = (int) number;

		for (int i = 7; i < exceldatalist.size(); i += 7) {

			String error = "";

			if (exceldatalist.get(i).equalsIgnoreCase("NA")) {
				error += "Entity type is Mandatory";
				errorList.add(exceldatalist.get(i + 3));
				errorList.add(error);
				continue;
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Customer")) {
				System.out.println("Inside Customer");
				Type type = new Type();
				type.setInternalType(1);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}
				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}
				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Lead")) {

				Type type = new Type();
				type.setInternalType(2);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}
				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}
				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}
			if (exceldatalist.get(i).trim().equalsIgnoreCase("Quotation")) {

				Type type = new Type();
				type.setInternalType(3);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}
				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}

				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Contract")) {

				Type type = new Type();
				type.setInternalType(4);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}
				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}

				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}
			if (exceldatalist.get(i).trim().equalsIgnoreCase("Vendor")) {

				Type type = new Type();
				type.setInternalType(5);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}
				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}

				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Complain")) {

				Type type = new Type();
				type.setInternalType(6);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}
				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}

				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Expense")) {

				Type type = new Type();
				type.setInternalType(7);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}
				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}

				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Billing")) {

				Type type = new Type();
				type.setInternalType(8);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}
				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}

				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Sales Quotation")) {

				Type type = new Type();
				type.setInternalType(9);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}
				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}

				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Sales Order")) {

				Type type = new Type();
				type.setInternalType(10);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}
				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}

				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("GRN")) {

				Type type = new Type();
				type.setInternalType(11);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}
				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}

				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Delivery Note")) {

				Type type = new Type();
				type.setInternalType(12);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}
				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}

				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Document Name")) {

				if (exceldatalist.get(i + 1).equalsIgnoreCase("NA")) {
					error += "Category is Mandatory";
					errorList.add(exceldatalist.get(i + 3));
					errorList.add(error);
					continue;
				} else {
					String strEmp = validateTypeCatName(listOfConfigCategory1, exceldatalist.get(i + 1).trim(),
							exceldatalist.get(i + 6).trim());
					System.out.println("strEmp ==" + strEmp);
					if (!strEmp.equals("")) {
						error += strEmp;
						errorList.add(exceldatalist.get(i + 3));
						errorList.add(error);
						continue;
					}
				}

				if (exceldatalist.get(i + 2).equalsIgnoreCase("NA")) {
					error += "Type code is Mandatory";
					errorList.add(exceldatalist.get(i + 3));
					errorList.add(error);
					continue;
				} else {
					String strEmp = validateTypeCode(listOfType, exceldatalist.get(i + 2).trim());
					System.out.println("strEmp ==" + strEmp);
					if (!strEmp.equals("")) {
						error += strEmp;
						errorList.add(exceldatalist.get(i + 3));
						errorList.add(error);
						continue;
					}
				}

				if (exceldatalist.get(i + 3).equalsIgnoreCase("NA")) {
					error += "Type name is Mandatory";
					errorList.add(exceldatalist.get(i + 3));
					errorList.add(error);
					continue;
				} else {
					String strEmp = validateTypeName(listOfType, exceldatalist.get(i + 3).trim());
					System.out.println("strEmp ==" + strEmp);
					if (!strEmp.equals("")) {
						error += strEmp;
						errorList.add(exceldatalist.get(i + 3));
						errorList.add(error);
						continue;
					}
				}

				if (exceldatalist.get(i + 6).equalsIgnoreCase("NA")) {
					error += "Category code is Mandatory";
					errorList.add(exceldatalist.get(i + 3));
					errorList.add(error);
					continue;
				}

				Type type = new Type();
				type.setInternalType(13);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}
				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}

				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Purchase Order")) {

				Type type = new Type();
				type.setInternalType(14);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}
				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}

				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Letter of Intent")
					|| exceldatalist.get(i).trim().equalsIgnoreCase("LOI")) {

				Type type = new Type();
				type.setInternalType(15);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}
				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}

				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Request for Quotation")
					|| exceldatalist.get(i).trim().equalsIgnoreCase("RFQ")) {

				Type type = new Type();
				type.setInternalType(16);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}

				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}

				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Purchase Requisition")
					|| exceldatalist.get(i).trim().equalsIgnoreCase("PR")) {

				Type type = new Type();
				type.setInternalType(17);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}

				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}

				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Material Request Note")
					|| exceldatalist.get(i).trim().equalsIgnoreCase("MRN")) {

				Type type = new Type();
				type.setInternalType(18);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}

				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}

				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Material Issue Note")
					|| exceldatalist.get(i).trim().equalsIgnoreCase("MIN")) {

				Type type = new Type();
				type.setInternalType(19);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}

				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}

				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Material Movement Note")
					|| exceldatalist.get(i).trim().equalsIgnoreCase("MMN")) {

				Type type = new Type();
				type.setInternalType(20);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}
				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}

				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Ticket")) {

				Type type = new Type();
				type.setInternalType(21);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}

				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}

				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Inspection")) {

				Type type = new Type();
				type.setInternalType(22);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}

				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}

				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Work Order")) {

				Type type = new Type();
				type.setInternalType(23);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}

				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}

				type.setCount(++count);
				type.setCompanyId(companyId);
				typelist.add(type);
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Invoice")) {

				Type type = new Type();
				type.setInternalType(24);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					type.setCategoryName("");
				} else {
					type.setCategoryName(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					type.setTypeCode("");
				} else {
					type.setTypeCode(exceldatalist.get(i + 2).trim());
				}

				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					type.setTypeName("");
				} else {
					type.setTypeName(exceldatalist.get(i + 3));
				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("no")) {
					type.setStatus(false);
				} else {
					type.setStatus(true);
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					type.setDescription("");
				} else {
					type.setDescription(exceldatalist.get(i + 5));
				}

				/**
				 * Date : 12-10-2017 BY ANIL adding category code
				 */
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					type.setCatCode("");
				} else {
					type.setCatCode(exceldatalist.get(i + 6));
				}

				type.setCount(++count);
				type.setCompanyId(companyId);
				if (!error.equals("")) {
					errorList.add(exceldatalist.get(i));
					errorList.add(error);
				}
				typelist.add(type);
			}

		}

		CsvWriter.updatedTypeList = errorList;
		if (errorList != null && errorList.size() != 0) {
			integerlist.add(-1);
			return integerlist;
		}

		lastcount = ofy().load().type(Type.class).filter("companyId", companyId).count();

		ofy().save().entities(typelist).now();

		ng.setNumber(count);

		GenricServiceImpl impl = new GenricServiceImpl();

		impl.save(ng);

		newcount = ofy().load().type(Type.class).filter("companyId", companyId).count();

		fileRecords = typelist.size();

		insertedRecords = newcount - lastcount;

		integerlist.add(fileRecords);
		integerlist.add(insertedRecords);

		return integerlist;
	}

	private String validateTypeName(List<Type> listOfType, String typeName) {
		String str = "";

		for (Type obj : listOfType) {

			str = "";
			if (obj.getTypeName().trim().equals(typeName)) {
				str = " Duplicate type name";
				logger.log(Level.SEVERE, "errrorrrr 11" + str);
				break;
			} else {
				continue;
			}
		}
		logger.log(Level.SEVERE, "return error value " + str);
		return str;

	}

	private String validateTypeCode(List<Type> listOfType, String typeCode) {
		String str = "";

		for (Type obj : listOfType) {

			str = "";
			if (obj.getTypeCode().trim().equals(typeCode)) {
				str = " Duplicate type code";
				logger.log(Level.SEVERE, "errrorrrr 11" + str);
				break;
			} else {
				continue;
			}
		}
		logger.log(Level.SEVERE, "return error value " + str);
		return str;

	}

	private String validateTypeCatName(List<ConfigCategory> listOfCat, String typeCatName, String catCode) {
		String str = "";
		for (ConfigCategory obj : listOfCat) {
			str = "";
			if (obj.getCategoryName().trim().equals(typeCatName)) {

				if (!obj.getCategoryCode().equals(catCode)) {
					str = " Category code and category  Name does not match ";
					logger.log(Level.SEVERE, "errrorrrr 11" + str);
				}
				break;
			} else {
				str = " Category name Does not exist in the system";
				logger.log(Level.SEVERE, "errrorrrr 22" + str);
			}
		}

		logger.log(Level.SEVERE, "return error value " + str);
		return str;

	}

	private ArrayList<Integer> configsave(long companyId, ArrayList<String> exceldatalist) {
		// TODO Auto-generated method stub
		System.out.println("Inside Config save");
		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		ArrayList<Config> configlist = new ArrayList<Config>();

		int filerecords;
		int insertedrecords;
		int lastcount;
		int newcount;

		NumberGeneration ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
				.filter("processName", "Config").filter("status", true).first().now();
		System.out.println("NG : " + ng);
		long number = ng.getNumber();
		System.out.println("number : " + number);
		int count = (int) number;

		for (int i = 3; i < exceldatalist.size(); i += 3) {
			System.out.println("Start of Method");
			if (exceldatalist.get(i).trim().equalsIgnoreCase("Service Product Category")) {
				Config config = new Config();
				config.setType(0);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Item Master Category")) {
				Config config = new Config();
				config.setType(1);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Company Type")) {
				Config config = new Config();
				config.setType(2);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Customer Level")) {
				Config config = new Config();
				config.setType(3);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Customer Type")) {
				Config config = new Config();
				config.setType(4);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Customer Status")) {
				Config config = new Config();
				config.setType(5);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Lead Status")) {
				Config config = new Config();
				config.setType(6);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Lead Priority")) {
				Config config = new Config();
				config.setType(7);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Contract Status")) {
				Config config = new Config();
				config.setType(8);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Customer Priority")) {
				Config config = new Config();
				config.setType(9);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Quotation Priority")) {
				Config config = new Config();
				config.setType(10);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Quotation Status")) {
				Config config = new Config();
				config.setType(11);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Employee Role")) {
				Config config = new Config();
				config.setType(12);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Employee Designation")) {
				Config config = new Config();
				config.setType(13);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Access Level")) {
				Config config = new Config();
				config.setType(14);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Expense Category")) {
				Config config = new Config();
				config.setType(15);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Payment Methods")) {
				Config config = new Config();
				config.setType(16);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Payment Terms")) {
				Config config = new Config();
				config.setType(17);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Currency")) {
				Config config = new Config();
				config.setType(18);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Action Taken")) {
				Config config = new Config();
				config.setType(19);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Service Status")) {
				Config config = new Config();
				config.setType(20);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Complain Priority")) {
				Config config = new Config();
				config.setType(21);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Complain Category")) {
				Config config = new Config();
				config.setType(22);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Complain Status")) {
				Config config = new Config();
				config.setType(23);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Action Priority")) {
				Config config = new Config();
				config.setType(24);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Follow Up Category")) {
				Config config = new Config();
				config.setType(25);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Follow Up Type")) {
				Config config = new Config();
				config.setType(26);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Action Status")) {
				Config config = new Config();
				config.setType(27);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Expense Type")) {
				Config config = new Config();
				config.setType(28);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Customer Group")) {
				Config config = new Config();
				config.setType(29);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Quotation Group")) {
				Config config = new Config();
				config.setType(30);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Contract Group")) {
				Config config = new Config();
				config.setType(31);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Lead Group")) {
				Config config = new Config();
				config.setType(32);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Vendor Group")) {
				Config config = new Config();
				config.setType(33);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Employee Type")) {
				Config config = new Config();
				config.setType(34);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Employee Group")) {
				Config config = new Config();
				config.setType(35);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Shift Category")) {
				Config config = new Config();
				config.setType(36);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Pattern Category")) {
				Config config = new Config();
				config.setType(37);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Complain Group")) {
				Config config = new Config();
				config.setType(38);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Complain Type")) {
				Config config = new Config();
				config.setType(39);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Expense Group")) {
				Config config = new Config();
				config.setType(40);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Asset Category")) {
				Config config = new Config();
				config.setType(41);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Clients Side Asset Category ")) {
				Config config = new Config();
				config.setType(42);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Attendence")) {
				Config config = new Config();
				config.setType(43);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("LOI Type")) {
				Config config = new Config();
				config.setType(44);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("LOI Group")) {
				Config config = new Config();
				config.setType(45);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Billing Group")) {
				Config config = new Config();
				config.setType(46);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Unit of Measurement")) {
				Config config = new Config();
				config.setType(47);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("GL Account Group")) {
				Config config = new Config();
				config.setType(48);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Sales Order Group")) {
				Config config = new Config();
				config.setType(49);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Sales Quotation Group")) {
				Config config = new Config();
				config.setType(50);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Sales Quotation Priority")) {
				Config config = new Config();
				config.setType(51);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Quotation Type")) {
				Config config = new Config();
				config.setType(52);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("GRN Type")) {
				Config config = new Config();
				config.setType(53);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}
			if (exceldatalist.get(i).trim().equalsIgnoreCase("GRN Group")) {
				Config config = new Config();
				config.setType(54);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("PR Group")) {
				Config config = new Config();
				config.setType(55);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("MMN Direction")) {
				Config config = new Config();
				config.setType(56);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Product Type")) {
				Config config = new Config();
				config.setType(57);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Product Group")) {
				Config config = new Config();
				config.setType(58);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Product Classification")) {
				Config config = new Config();
				config.setType(59);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Check List Category")) {
				Config config = new Config();
				config.setType(60);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Delivery Note Group")) {
				Config config = new Config();
				config.setType(61);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Process Name")) {
				Config config = new Config();
				config.setType(62);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Communication Type")) {
				Config config = new Config();
				config.setType(63);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Communication Category")) {
				Config config = new Config();
				config.setType(64);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Person Type")) {
				Config config = new Config();
				config.setType(65);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Group Type")) {
				Config config = new Config();
				config.setType(66);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Contact Role")) {
				Config config = new Config();
				config.setType(67);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Contact Person")) {
				Config config = new Config();
				config.setType(68);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Customer Contact")) {
				Config config = new Config();
				config.setType(69);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Interaction Priority")) {
				Config config = new Config();
				config.setType(70);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Ticket Priority")) {
				Config config = new Config();
				config.setType(71);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Ticket Level")) {
				Config config = new Config();
				config.setType(72);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Process Type")) {
				Config config = new Config();
				config.setType(73);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Article Type")) {
				Config config = new Config();
				config.setType(74);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Inspection Group")) {
				Config config = new Config();
				config.setType(75);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Work Order Group")) {
				Config config = new Config();
				config.setType(76);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Invoice Group")) {
				Config config = new Config();
				config.setType(77);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("Service Type")) {
				Config config = new Config();
				config.setType(78);

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					config.setName("");
				} else {
					config.setName(exceldatalist.get(i + 1));
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("no")) {
					config.setStatus(false);
				} else {
					config.setStatus(true);
				}

				config.setCount(++count);
				config.setCompanyId(companyId);
				configlist.add(config);
				System.out.println("End of Method");
			}
		}

		lastcount = ofy().load().type(Config.class).filter("companyId", companyId).count();
		System.out.println("Lastcount : " + lastcount);
		ofy().save().entities(configlist).now();

		ng.setNumber(count);

		GenricServiceImpl impl = new GenricServiceImpl();

		impl.save(ng);

		newcount = ofy().load().type(Config.class).filter("companyId", companyId).count();

		filerecords = configlist.size();
		insertedrecords = newcount - lastcount;
		System.out.println("Filerecords : " + filerecords);
		System.out.println("Inserted Records: " + insertedrecords);
		integerlist.add(filerecords);
		integerlist.add(insertedrecords);

		return integerlist;
	}

	/********************
	 * This is For Transactions section in Data Migration
	 *******************/
	@Override
	public ArrayList<Integer> savedTransactionDetails(long companyid, String entityname) {
		// TODO Auto-generated method stub
		ArrayList<Integer> intlist = new ArrayList<Integer>();
		ArrayList<String> exceldatalist = new ArrayList<String>();

		/***************************************
		 * Reading Excel File
		 *****************************************/
		// ////

		try {

			DataFormatter datafmt = new DataFormatter();
			long i;

			Workbook wb = null;

			try {
				wb = WorkbookFactory.create(new BlobstoreInputStream(blobkey));
			} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Sheet sheet = wb.getSheetAt(0);

			Iterator<Row> rowIterator = sheet.iterator();

			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();

				Iterator<Cell> celliterator = row.cellIterator();

				while (celliterator.hasNext()) {
					Cell cell = celliterator.next();

					switch (cell.getCellType()) {

					case Cell.CELL_TYPE_STRING:
						exceldatalist.add(cell.getStringCellValue());
						break;

					case Cell.CELL_TYPE_NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) {
							exceldatalist.add(fmt.format(cell.getDateCellValue()));

						} else {

							cell.getNumericCellValue();
							String sampletest = datafmt.formatCellValue(cell);
							System.out.println("sampletest :" + sampletest);
							// i = (long) cell.getNumericCellValue();
							exceldatalist.add(sampletest + "");

							System.out.println("Value:" + sampletest);
						}
						break;

					case Cell.CELL_TYPE_BOOLEAN:

						break;
					default:

					}
					// System.out.println("List Size " + exceldatalist.size());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		/******************** Finished Reading Excel Sheet *************************/
		// ////

		if (entityname.equalsIgnoreCase("Contract")) {

			if (exceldatalist.get(0).trim().equalsIgnoreCase("Customer Reference ID")
					&& exceldatalist.get(1).trim().equalsIgnoreCase("Reference Number")
					&& exceldatalist.get(2).trim().equalsIgnoreCase("Reference Date(MM/dd/yyyy)")
					&& exceldatalist.get(3).trim().equalsIgnoreCase("Referred By")
					&& exceldatalist.get(4).trim().equalsIgnoreCase("Contract Start Date(MM/dd/yyyy)")
					&& exceldatalist.get(5).trim().equalsIgnoreCase("Contract End Date(MM/dd/yyyy)")
					&& exceldatalist.get(6).trim().equalsIgnoreCase("Reference Order Id")
					&& exceldatalist.get(7).trim().equalsIgnoreCase("Branch")
					&& exceldatalist.get(8).trim().equalsIgnoreCase("Sales Person")
					&& exceldatalist.get(9).trim().equalsIgnoreCase("Payment Method")
					&& exceldatalist.get(10).trim().equalsIgnoreCase("Approver Name")
					&& exceldatalist.get(11).trim().equalsIgnoreCase("Contract Group")
					&& exceldatalist.get(12).trim().equalsIgnoreCase("Contract Category")
					&& exceldatalist.get(13).trim().equalsIgnoreCase("Contract Type")
					&& exceldatalist.get(14).trim().equalsIgnoreCase("Contract Date(MM/dd/yyyy)")
					&& exceldatalist.get(15).trim().equalsIgnoreCase("Credit Days")
					&& exceldatalist.get(16).trim().equalsIgnoreCase("Product Name")
					// && exceldatalist.get(17).trim()
					// .equalsIgnoreCase("Duration")
					&& exceldatalist.get(17).trim().equalsIgnoreCase("Product Code")
					&& exceldatalist.get(18).trim().equalsIgnoreCase("VAT Tax")
					&& exceldatalist.get(19).trim().equalsIgnoreCase("Service Tax")
					&& exceldatalist.get(20).trim().equalsIgnoreCase("Quantity")
					// && exceldatalist.get(20).trim()
					// .equalsIgnoreCase("Contract Status")
					&& exceldatalist.get(21).trim().equalsIgnoreCase("Number of Services")
					&& exceldatalist.get(22).trim().equalsIgnoreCase("Product Price")
					&& exceldatalist.get(23).trim().equalsIgnoreCase("Contract Status")) {
				intlist = contractsave(companyid, exceldatalist);
			} else {
				intlist.add(-1);
			}
		}

		if (entityname.equalsIgnoreCase("Service")) {
			if (exceldatalist.get(0).trim().equalsIgnoreCase("Contract Reference ID")) {
				intlist = servicesave(companyid, exceldatalist);
			} else {
				intlist.add(-1);
			}
		}

		if (entityname.equalsIgnoreCase("Payment")) {

			if (exceldatalist.get(0).trim().equalsIgnoreCase("Contract Reference ID")
					&& exceldatalist.get(1).trim().equalsIgnoreCase("Approver Name")
					&& exceldatalist.get(2).trim().equalsIgnoreCase("Person Responsible")
					&& exceldatalist.get(3).trim().equalsIgnoreCase("Billing Date(MM/dd/yyyy)")
					&& exceldatalist.get(4).trim().equalsIgnoreCase("Invoice Date(MM/dd/yyyy)")
					&& exceldatalist.get(5).trim().equalsIgnoreCase("Payment Date(MM/dd/yyyy)")
					&& exceldatalist.get(6).trim().equalsIgnoreCase("Payment Method")
					&& exceldatalist.get(7).trim().equalsIgnoreCase("Bank Account No.")
					// && exceldatalist.get(8).trim()
					// .equalsIgnoreCase("Payment Method")
					&& exceldatalist.get(8).trim().equalsIgnoreCase("Bank Name")
					&& exceldatalist.get(9).trim().equalsIgnoreCase("Bank Branch")
					&& exceldatalist.get(10).trim().equalsIgnoreCase("Cheque No.")
					&& exceldatalist.get(11).trim().equalsIgnoreCase("Cheque Date(mm/dd/yyyy)")) {

				intlist = paymentsave(companyid, exceldatalist);
			} else {
				intlist.add(-1);
			}
		}

		// vijay added code for lead excel
		if (entityname.equalsIgnoreCase("Lead")) {
			if (exceldatalist.get(0).trim().equalsIgnoreCase("Lead Title")
					&& exceldatalist.get(1).trim().equalsIgnoreCase("Lead Description")
					&& exceldatalist.get(2).trim().equalsIgnoreCase("Lead Status")
					&& exceldatalist.get(3).trim().equalsIgnoreCase("Lead Priority")
					&& exceldatalist.get(4).trim().equalsIgnoreCase("Branch")
					&& exceldatalist.get(5).trim().equalsIgnoreCase("Sales Person")
					&& exceldatalist.get(6).trim().equalsIgnoreCase("Lead Group")
					&& exceldatalist.get(7).trim().equalsIgnoreCase("Lead Category")
					&& exceldatalist.get(8).trim().equalsIgnoreCase("Lead Type")
					&& exceldatalist.get(9).trim().equalsIgnoreCase("Customer Name")
					&& exceldatalist.get(10).trim().equalsIgnoreCase("Product Name")
					&& exceldatalist.get(11).trim().equalsIgnoreCase("Product Price")) {
				intlist = leadsave(companyid, exceldatalist);
			} else {
				intlist.add(-1);
			}
		}

		return intlist;
	}

	private ArrayList<Integer> leadsave(long companyid, ArrayList<String> exceldatalist) {

		ArrayList<Lead> leadlist = new ArrayList<Lead>();

		int fileRecords;
		int insertedRecords;
		int lastcount;
		int newcount;

		ArrayList<Integer> integerlist = new ArrayList<Integer>();

		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyid).filter("processName", "Lead")
				.filter("status", true).first().now();

		long number = ng.getNumber();
		int count = (int) number;

		for (int p = 12; p < exceldatalist.size(); p += 12) {

			Lead lead = new Lead();

			if (exceldatalist.get(p).trim().equalsIgnoreCase("na")) {
				lead.setTitle("");
			} else {
				lead.setTitle(exceldatalist.get(p));
			}

			if (exceldatalist.get(p + 1).trim().equalsIgnoreCase("na")) {
				lead.setDescription("");
			} else {
				lead.setDescription(exceldatalist.get(p + 1));
			}

			if (exceldatalist.get(p + 2).trim().equalsIgnoreCase("NA")) {
				lead.setStatus("");

			} else {
				lead.setStatus(exceldatalist.get(p + 2));
			}

			if (exceldatalist.get(p + 3).trim().equalsIgnoreCase("NA")) {
				lead.setPriority("");
			} else {
				lead.setPriority(exceldatalist.get(p + 3));
			}
			if (exceldatalist.get(p + 4).trim().equalsIgnoreCase("na")) {
				lead.setBranch("");
			} else {
				lead.setBranch(exceldatalist.get(p + 4));
			}
			if (exceldatalist.get(p + 5).trim().equalsIgnoreCase("na")) {
				lead.setEmployee("");
			} else {
				lead.setEmployee(exceldatalist.get(p + 5));
			}
			if (exceldatalist.get(p + 6).trim().equalsIgnoreCase("NA")) {
				lead.setGroup("");
			} else {
				lead.setGroup(exceldatalist.get(p + 6));
			}
			if (exceldatalist.get(p + 7).trim().equalsIgnoreCase("na")) {
				lead.setCategory("");
			} else {
				lead.setCategory(exceldatalist.get(p + 7));
			}
			if (exceldatalist.get(p + 8).trim().equalsIgnoreCase("na")) {
				lead.setType("");
			} else {
				lead.setType(exceldatalist.get(p + 8));
			}

			if (exceldatalist.get(p + 9).trim().equalsIgnoreCase("na")) {

			} else {

				String customernameexcel = exceldatalist.get(p + 9).trim();

				Customer customerName = ofy().load().type(Customer.class).filter("companyId", companyid)
						.filter("fullname", customernameexcel).first().now();

				Customer companyName = ofy().load().type(Customer.class).filter("companyId", companyid)
						.filter("companyName", customernameexcel).first().now();

				if (customerName == null && companyName == null) {
					integerlist.add(-2);
					return integerlist;
				}

				if (customerName != null) {

					PersonInfo personinfo = new PersonInfo();
					personinfo.setCount(customerName.getCount());
					personinfo.setCellNumber(customerName.getCellNumber1());
					personinfo.setFullName(customerName.getFullname());
					personinfo.setPocName(customerName.getFullname());
					lead.setPersonInfo(personinfo);
				}

				if (companyName != null) {

					PersonInfo personinfo = new PersonInfo();
					personinfo.setCount(companyName.getCount());
					personinfo.setCellNumber(companyName.getCellNumber1());
					personinfo.setFullName(companyName.getCompanyName());
					personinfo.setPocName(companyName.getFullname());
					lead.setPersonInfo(personinfo);
				}

			}

			if (exceldatalist.get(p + 10).trim().equalsIgnoreCase("na")) {

			} else {

				String productName = exceldatalist.get(p + 10).trim();

				SuperProduct superproduct = ofy().load().type(SuperProduct.class).filter("companyId", companyid)
						.filter("productName", productName).first().now();

				if (superproduct == null) {
					integerlist.add(-3);
					return integerlist;
				}

				if (superproduct != null) {

					SalesLineItem lineItem = new SalesLineItem();
					ArrayList<SalesLineItem> saleslinearr = new ArrayList<SalesLineItem>();

					int srno = 0;

					if (exceldatalist.get(p + 11).trim().equalsIgnoreCase("na")) {

					} else {
						double price = Double.parseDouble(exceldatalist.get(p + 11));
						superproduct.setPrice(price);
					}

					lineItem.setPrduct(superproduct);
					lineItem.setProductSrNo(++srno);

					saleslinearr.add(lineItem);

					lead.setLeadProducts(saleslinearr);

				}

			}

			lead.setCount(++count);
			lead.setCompanyId(companyid);
			leadlist.add(lead);

		}

		lastcount = ofy().load().type(Lead.class).filter("companyId", companyid).count();
		System.out.println("LastCount" + lastcount);

		ofy().save().entities(leadlist).now();

		ng.setNumber(count);
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(ng);

		System.out.println("Data saved successfuly=======================================");
		logger.log(Level.SEVERE, "Data saved successfuly=======================================");

		newcount = ofy().load().type(Lead.class).filter("companyId", companyid).count();
		System.out.println("New Count" + newcount);

		System.out.println("number@@@" + ng.getNumber());

		fileRecords = leadlist.size();
		insertedRecords = newcount - lastcount;
		System.out.println("Result:" + insertedRecords);
		integerlist.add(fileRecords);
		integerlist.add(insertedRecords);
		System.out.println("SIZE:" + leadlist.size());

		return integerlist;

	}

	private ArrayList<Integer> contractsave(long companyId, ArrayList<String> exceldatalist) {
		ArrayList<Integer> integerlist = new ArrayList<Integer>();

		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId).filter("processName", "Contract")
				.filter("status", true).first().now();
		long number = ng.getNumber();
		int count = (int) number;
		/************
		 * Fields changing in each case thats y maikng this things in global
		 ****************/

		String contractdata = null;
		String productdata = null;
		String productquantity = null;
		String productnoofservice = null;
		String productprice = null;
		String forproductcontractEnddate = null;
		String contractenddate = null;
		String pcode = null;
		String vatTax = null;
		String serviceTax = null;

		int numberoffile = 0;

		System.out.println("The count is :" + count);
		int dummyreforderid = 0;
		for (int i = 0; i < exceldatalist.size(); i++) {
			System.out.println("The Excel Data at " + i + " is: " + exceldatalist.get(i));
		}

		for (int i = 24; i < exceldatalist.size(); i += 24) {
			// productdata = exceldatalist.get(i+16);
			System.out.println("Product Data :" + productdata);
			System.out.println("Loop Chal pada" + (i / 21));

			if (contractenddate == null) {
				if (i == 24) {
					contractenddate = exceldatalist.get(i + 5).trim();
				} else {
					contractenddate = exceldatalist.get((i + 5) - 24).trim();
				}
			}

			if (dummyreforderid == 0) {
				if (i == 24) {
					dummyreforderid = Integer.parseInt(exceldatalist.get(i + 6));
				} else {
					dummyreforderid = Integer.parseInt(exceldatalist.get((i + 6) - 24));
				}

			}

			if (dummyreforderid == Integer.parseInt(exceldatalist.get(i + 6))) {
				System.out.println("Order Id Matched");

				int checkedEndDate = 0;
				try {
					checkedEndDate = fmt.parse(contractenddate).compareTo(fmt.parse(exceldatalist.get(i + 5).trim()));
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (checkedEndDate == 1) {
					contractenddate = contractenddate;
					System.out.println("Contract End Date Unchanged");
				} else if (checkedEndDate == -1) {
					contractenddate = exceldatalist.get(i + 5);
					System.out.println("Contract End Date Changed");
				}

				if (productdata == null) {
					productdata = exceldatalist.get(i + 16).trim();
					pcode = exceldatalist.get(i + 17).trim();
					vatTax = exceldatalist.get(i + 18).trim();
					serviceTax = exceldatalist.get(i + 19);
					productquantity = exceldatalist.get(i + 20).trim();
					productnoofservice = exceldatalist.get(i + 21).trim();
					productprice = exceldatalist.get(i + 22).trim();
					forproductcontractEnddate = exceldatalist.get(i + 5).trim();
					System.out.println("Product Data :(in if)" + productdata);
					System.out.println("pcode :(in if)" + pcode);
					System.out.println("vatTax :(In if)" + vatTax);
					System.out.println("serviceTax :(in if)" + serviceTax);
					System.out.println("Product Quantity :(in if)" + productquantity);
					System.out.println("Product service :(in if)" + productnoofservice);
					System.out.println("Product price :(in if)" + productprice);

				} else {

					productdata = productdata + "/" + exceldatalist.get(i + 16).trim();
					pcode = pcode + "/" + exceldatalist.get(i + 17).trim();
					vatTax = vatTax + "/" + exceldatalist.get(i + 18).trim();
					serviceTax = serviceTax + "/" + exceldatalist.get(i + 19);
					productquantity = productquantity + "/" + exceldatalist.get(i + 20).trim();
					productnoofservice = productnoofservice + "/" + exceldatalist.get(i + 21).trim();
					productprice = productprice + "/" + exceldatalist.get(i + 22).trim();
					forproductcontractEnddate = forproductcontractEnddate + "*" + exceldatalist.get(i + 5).trim();

					System.out.println("Product Data :(in else)" + productdata);
					System.out.println("pcode :(in else)" + pcode);
					System.out.println("vatTax :(In Else)" + vatTax);
					System.out.println("serviceTax :(in else)" + serviceTax);
					System.out.println("Product Quantity :(in else)" + productquantity);
					System.out.println("Product service :(in else)" + productnoofservice);
					System.out.println("Product price :(in else)" + productprice);
					System.out.println("Contract End Date :(in else)" + forproductcontractEnddate);
				}

			} else {
				System.out.println("Order ID did not matched");
				int setcount = ++count;

				String contracttaskdata2 = contractdata + "$" + contractenddate + "$" + productdata + "$" + pcode + "$"
						+ vatTax + "$" + serviceTax + "$" + productquantity + "$" + productnoofservice + "$"
						+ productprice + "$" + forproductcontractEnddate + "$" + setcount + "$" + companyId;
				System.out.println("Data sent from 1st part");
				System.out.println("productdata :" + productdata);
				System.out.println("pcode :" + pcode);
				System.out.println("vatTax :" + vatTax);
				System.out.println("serviceTax :" + serviceTax);
				System.out.println("productquantity :" + productquantity);
				System.out.println("productnoofservice :" + productnoofservice);
				System.out.println("productprice :" + productprice);
				System.out.println("forproductcontractEnddate :" + forproductcontractEnddate);
				Queue queue = QueueFactory.getQueue("Contractcreation-queue");

				queue.add(TaskOptions.Builder.withUrl("/slick_erp/contractcreationtaskqueue").param("contractkey",
						contracttaskdata2));
				System.out.println("Setting all data as null");
				contractdata = null;
				System.out.println("The contract data is:" + contractdata);
				productdata = null;
				System.out.println("The productdata data is:" + productdata);
				pcode = null;
				System.out.println("The pcode data is:" + pcode);
				vatTax = null;
				System.out.println("The vatTax data is:" + vatTax);
				serviceTax = null;
				System.out.println("The serviceTax data is:" + serviceTax);
				productnoofservice = null;
				System.out.println("The productnoofservice data is:" + productnoofservice);
				productprice = null;
				System.out.println("The productprice data is:" + productprice);
				dummyreforderid = 0;
				System.out.println("The dummyreforderid data is:" + dummyreforderid);
				forproductcontractEnddate = null;
				System.out.println("The forproductcontractEnddate data is:" + forproductcontractEnddate);
				contractenddate = null;
				System.out.println("The contractenddate data is:" + contractenddate);
			}

			if (contractdata == null) {
				System.out.println("Setting Contract data");
				System.out.println("Product data is :" + productdata);
				System.out.println("The productnoofservice data is:" + productnoofservice);
				System.out.println("The productprice data is:" + productprice);
				System.out.println("The dummyreforderid data is:" + dummyreforderid);

				System.out.println("exceldatalist.get(i+15).trim()" + exceldatalist.get(i + 16).trim());
				if (productdata == null) {

					productdata = exceldatalist.get(i + 16).trim();
					pcode = exceldatalist.get(i + 17).trim();
					vatTax = exceldatalist.get(i + 18).trim();
					serviceTax = exceldatalist.get(i + 19).trim();
					productquantity = exceldatalist.get(i + 20).trim();
					productnoofservice = exceldatalist.get(i + 21).trim();
					productprice = exceldatalist.get(i + 22).trim();
					forproductcontractEnddate = exceldatalist.get(i + 5).trim();
					System.out.println("Product Data :(in if)" + productdata);
					System.out.println("pcode :(in if)" + pcode);
					System.out.println("vatTax :(in if)" + vatTax);
					System.out.println("serviceTax :(in if)" + serviceTax);
					System.out.println("Product Quantity :(in if)" + productquantity);
					System.out.println("Product service :(in if)" + productnoofservice);
					System.out.println("Product price :(in if)" + productprice);
					System.out.println("The forproductcontractEnddate data is:" + forproductcontractEnddate);
				}

				if (contractenddate == null) {
					contractenddate = exceldatalist.get(i + 5);
				}

				contractdata = exceldatalist.get(i).trim() + "$" + exceldatalist.get(i + 1).trim() + "$"
						+ exceldatalist.get(i + 2).trim() + "$" + exceldatalist.get(i + 3).trim() + "$"
						+ exceldatalist.get(i + 4).trim() + "$"
						// + exceldatalist.get(i + 5).trim() + "$" This is End
						// Date and will be selected by Sorting and we send it
						// separately
						+ exceldatalist.get(i + 6).trim() + "$" + exceldatalist.get(i + 7).trim() + "$"
						+ exceldatalist.get(i + 8).trim() + "$" + exceldatalist.get(i + 9).trim() + "$"
						+ exceldatalist.get(i + 10).trim() + "$" + exceldatalist.get(i + 11).trim() + "$"
						+ exceldatalist.get(i + 12).trim() + "$" + exceldatalist.get(i + 13).trim() + "$"
						+ exceldatalist.get(i + 14).trim() + "$" + exceldatalist.get(i + 15).trim() + "$"
						+ exceldatalist.get(i + 23);

			}
			System.out.println("(i + 23)" + (i + 23));
			System.out.println("exceldatalist.size()" + exceldatalist.size());
			if ((i + 24) == exceldatalist.size()) {
				int setcount = ++count;
				// productdata=exceldatalist.get(i+16).trim();

				String contracttaskdata2 = contractdata + "$" + contractenddate + "$" + productdata + "$" + pcode + "$"
						+ vatTax + "$" + serviceTax + "$" + productquantity + "$" + productnoofservice + "$"
						+ productprice + "$" + forproductcontractEnddate + "$" + setcount + "$" + companyId;
				System.out.println("Data sent from 2nd part");
				System.out.println("productdata :" + productdata);
				Queue queue = QueueFactory.getQueue("Contractcreation-queue");

				queue.add(TaskOptions.Builder.withUrl("/slick_erp/contractcreationtaskqueue").param("contractkey",
						contracttaskdata2));
				contractdata = null;
				productdata = null;
				dummyreforderid = 0;
			}
		}

		ng.setNumber(count);
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(ng);
		int count2 = (int) number;
		integerlist.add(count - count2);
		integerlist.add(count - count2);
		// integerlist.add(0);
		return integerlist;
	}

	//
	// private boolean checkreforderid(String string,
	// ArrayList<String> reforderidlist, ArrayList<Contract> contractlist) {
	// // TODO Auto-generated method stub
	// for (int i = 0; i < contractlist.size(); i++)
	// if (string.equalsIgnoreCase(contractlist.get(i)
	// .getRefContractCount() + "")) {
	// System.out.println("Matched inside method tan tana tan tan");
	// return true;
	// }
	// return false;
	// }

	private ArrayList<Integer> servicesave(long companyId, ArrayList<String> exceldatalist) {
		// TODO Auto-generated method stub
		System.out.println("Inside service Save");
		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		ArrayList<Service> servicelist = new ArrayList<Service>();
		int fileRecords;
		int insertedRecords;
		int lastcount;
		int newcount;
		String servicedata = null;
		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId).filter("processName", "Service")
				.filter("status", true).first().now();

		long number1 = ng.getNumber();
		int count = (int) number1;
		// int number2 = 0;
		int loop = 0;
		for (int i = 1; i < exceldatalist.size(); i += 1) {
			// Contract contract=new Contract();
			loop = loop + 1;
			System.out.println("Loop is :" + loop);
			List<Contract> contractlist = ofy().load().type(Contract.class).filter("companyId", companyId)
					.filter("refContractCount", Integer.parseInt(exceldatalist.get(i).trim())).list();
			System.out.println("The list size is :" + contractlist.size());
			System.out.println("The Contract Status is :" + contractlist.get(0).getContractstatus());

			for (int j = 0; j < contractlist.size(); j++) {

				if (contractlist.get(j).getContractstatus().trim().equalsIgnoreCase("opened")) {

					System.out.println("The data is :" + contractlist.get(j));
					System.out.println("The total  Products are :" + contractlist.get(j).getItems().size());

					for (int productno = 0; productno < contractlist.get(j).getItems().size(); productno++) {
						System.out.println("The Product Name is :" + productno
								+ contractlist.get(j).getItems().get(productno).getProductName());

						int durationofdays = contractlist.get(j).getItems().get(productno).getDuration();
						System.out.println("The duration of days is:" + durationofdays);
						int gapofdays = (int) (durationofdays
								/ (contractlist.get(j).getItems().get(productno).getNumberOfServices()
										* contractlist.get(j).getItems().get(productno).getQty()));
						System.out.println("The gap of days:" + gapofdays);
						int addtostartdate = 0;
						System.out.println("No. of services is: "
								+ contractlist.get(j).getItems().get(productno).getNumberOfServices());
						int noofservices = (int) (contractlist.get(j).getItems().get(productno).getNumberOfServices()
								* contractlist.get(j).getItems().get(productno).getQty());

						for (int k = 0; k < noofservices; k++) {
							int setcount = ++count;
							System.out.println("Task queue fired for :" + k);
							int serviceno = k + 1;
							System.out.println("THe Value of serviceno is " + serviceno);
							if ((k + 1) <= 1) {
								System.out.println("Inside Zero");
								addtostartdate = 0;
							} else {
								addtostartdate = addtostartdate + gapofdays;
							}

							servicedata = contractlist.get(j).getCount() + "$"
									+ fmt.format(contractlist.get(j).getStartDate()) + "$"
									+ fmt.format(contractlist.get(j).getEndDate()) + "$"
									+ contractlist.get(j).getCinfo().getCount() + "$"
									+ contractlist.get(j).getCinfo().getFullName() + "$"
									+ contractlist.get(j).getCinfo().getCellNumber() + "$"
									+ contractlist.get(j).getItems().get(productno).getPrduct() + "$"
									+ contractlist.get(j).getCinfo().getPocName() + "$"
									+ contractlist.get(j).getItems().get(productno).getProductCode() + "$"
									+ contractlist.get(j).getItems().get(productno).getProductName() + "$" + serviceno
									+ "$" + contractlist.get(j).getEmployee() + "$" + contractlist.get(j).getBranch()
									+ "$" + fmt.format(contractlist.get(j).getStartDate()) + "$" + addtostartdate + "$"
									+ "Flexible" + "$" + "Periodic" + "$" + setcount + "$" + companyId;

							Queue queue2 = QueueFactory.getQueue("ServiceCreation-queue");

							queue2.add(TaskOptions.Builder.withUrl("/slick_erp/servicecreationtaskqueue")
									.param("servicekey", servicedata));
							// ++number2;
						}
					}
				} else {
					System.out.println(
							"Nothing to do with :" + contractlist.get(0).getRefContractCount() + "Ref Order Id number");
				}
			}

		}

		ng.setNumber(count);
		long number2 = ng.getNumber();
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(ng);
		integerlist.add(loop);
		integerlist.add((int) (number2 - number1));
		return integerlist;
	}

	private ArrayList<Integer> paymentsave(long companyId, ArrayList<String> exceldatalist) {
		// TODO Auto-generated method stub
		System.out.println("Inside payment Save");
		ArrayList<Integer> integerlist = new ArrayList<Integer>();

		NumberGeneration ngforbilling = new NumberGeneration();

		ngforbilling = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
				.filter("processName", "BillingDocument").filter("status", true).first().now();

		long number1 = ngforbilling.getNumber();
		int countforbilling = (int) number1;

		NumberGeneration ngforinvoice = new NumberGeneration();

		ngforinvoice = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
				.filter("processName", "Invoice").filter("status", true).first().now();

		long number2 = ngforinvoice.getNumber();
		int countforinvoice = (int) number2;

		NumberGeneration ngforCustomerPayment = new NumberGeneration();

		ngforCustomerPayment = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
				.filter("processName", "CustomerPayment").filter("status", true).first().now();

		long number3 = ngforinvoice.getNumber();
		int countforCustomerPayment = (int) number3;
		int loop = 0;
		int loop2 = 0;
		for (int i = 12; i < exceldatalist.size(); i += 12) {
			++loop;
			List<Contract> contractlist = ofy().load().type(Contract.class).filter("companyId", companyId)
					.filter("refContractCount", Integer.parseInt(exceldatalist.get(i).trim())).list();
			System.out.println("The Contract Status in payent  is :" + contractlist.get(0).getContractstatus());
			if (contractlist.get(0).getContractstatus().trim().equalsIgnoreCase("opened")) {
				++loop2;
				int setcountforbilling = ++countforbilling;
				int setcounforinvoice = ++countforinvoice;
				int setcountforCustPay = ++countforCustomerPayment;
				String paymentdata = exceldatalist.get(i).trim() + "$" + exceldatalist.get(i + 1).trim() + "$"
						+ exceldatalist.get(i + 2).trim() + "$" + exceldatalist.get(i + 3).trim() + "$"
						+ exceldatalist.get(i + 4).trim() + "$" + exceldatalist.get(i + 5).trim() + "$"
						+ setcountforbilling + "$" + setcounforinvoice + "$" + setcountforCustPay + "$" + companyId
						+ "$" + exceldatalist.get(i + 6).trim() + "$" + exceldatalist.get(i + 7).trim() + "$"
						+ exceldatalist.get(i + 8).trim() + "$" + exceldatalist.get(i + 9).trim() + "$"
						+ exceldatalist.get(i + 10).trim() + "$" + exceldatalist.get(i + 11).trim();
				Queue queue = QueueFactory.getQueue("PaymentCreation-queue");

				queue.add(TaskOptions.Builder.withUrl("/slick_erp/paymenttaskqueue").param("paymentkey", paymentdata));
			}

		}
		ngforbilling.setNumber(countforbilling);
		GenricServiceImpl implforbill = new GenricServiceImpl();
		implforbill.save(ngforbilling);

		ngforinvoice.setNumber(countforinvoice);
		GenricServiceImpl implforinvoice = new GenricServiceImpl();
		implforinvoice.save(ngforinvoice);

		ngforCustomerPayment.setNumber(countforCustomerPayment);
		GenricServiceImpl implforpay = new GenricServiceImpl();
		implforpay.save(ngforCustomerPayment);

		integerlist.add(loop);
		integerlist.add(loop2);
		return integerlist;
	}

	// vijay added method for customer validation

	private boolean validationforclientcompnay(List<Customer> listofcustomer, ArrayList<String> exceldatalist, int p) {
		boolean value = true;
		logger.log(Level.SEVERE, "hiiiiiiiiiiiiiiiiiii in validation client is company");
		for (int i = 0; i < listofcustomer.size(); i++) {

			logger.log(Level.SEVERE, "in validation client is company name from database ====="
					+ listofcustomer.get(i).getCompanyName().trim());
			logger.log(Level.SEVERE,
					"in validation client is company name from Excel =====" + exceldatalist.get(p + 1));
			if (!listofcustomer.get(i).getCompanyName().trim().equals("")
					&& listofcustomer.get(i).getCompanyName().trim() != null) {
				if (listofcustomer.get(i).getCompanyName().trim().equalsIgnoreCase(exceldatalist.get(p + 1).trim())) {
					return false;
				}
			}
		}

		return value;
	}

	/**
	 * 
	 * Description: @param companyId
	 * 
	 * @param exceldatalist
	 * @return intergerList for updating statusof upload This is for saving in
	 *         Product in inventory. Creation Date: 15-Sept-2016 Project : EVA Erp
	 *         Required For: Everyone(This was created for NBHC Update). Created By
	 *         : Rahul Verma
	 */
	private ArrayList<Integer> stockUpdateSave(long companyId, ArrayList<String> exceldatalist) {
		// TODO Auto-generated method stub
		ArrayList<ProductInventoryView> productInventorylist = new ArrayList<ProductInventoryView>();

		int fileRecords;
		int insertedRecords;
		int lastcount;
		int newcount;

		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		List<ProductInventoryView> listofproductInventory = ofy().load().type(ProductInventoryView.class)
				.filter("companyId", companyId).list();

		// if(entityname.equalsIgnoreCase("Customer")){

		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
				.filter("processName", "ProductInventoryView").filter("status", true).first().now();

		long number = ng.getNumber();
		int count = (int) number;

		for (int i = 11; i < exceldatalist.size(); i += 11) {
			ProductInventoryView productInventoryView = new ProductInventoryView();
			ItemProduct itemProduct = null;
			String uom = "";

			boolean productInvenCheck = true;

			productInvenCheck = validationforStockUpdate(listofproductInventory, exceldatalist, i);

			if (productInvenCheck == false) {
				logger.log(Level.SEVERE, "Product excel position:::::::::" + i);
				integerlist.add(-19);
				return integerlist;
			}

			if (exceldatalist.get(i).trim().equalsIgnoreCase("NA")) {
				logger.log(Level.SEVERE, "Product code is mandatory and product is::::::::" + i);
			} else {
				itemProduct = ofy().load().type(ItemProduct.class).filter("companyId", companyId)
						.filter("productCode", exceldatalist.get(i).trim()).first().now();
				productInventoryView.setProdID(itemProduct.getCount());
				productInventoryView.setProductCode(itemProduct.getProductCode());
				productInventoryView.setProductName(itemProduct.getProductName());
				productInventoryView.setProductPrice(itemProduct.getPrice());
				if (itemProduct.getUnitOfMeasurement() != null) {
					uom = itemProduct.getUnitOfMeasurement();
				}
				productInventoryView.setProductUOM(uom);
			}

			if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("NA")) {
				productInventoryView.setInspectionRequired("NO");
			} else if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("NO")) {
				productInventoryView.setInspectionRequired("NO");
			} else if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("Yes")) {
				productInventoryView.setInspectionRequired("YES");
			}

			if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("NA")) {
				productInventoryView.setInspectionMethod(" ");
			} else {
				productInventoryView.setInspectionMethod(exceldatalist.get(i + 2).trim());
			}
			// Setting Table
			ProductInventoryViewDetails details = new ProductInventoryViewDetails();
			if (itemProduct != null) {
				details.setProdid(itemProduct.getCount());
				details.setProdname(itemProduct.getProductName());
			} else {
				details.setProdid(0);
				details.setProdname("");
			}

			if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("NA")) {
				details.setWarehousename("");
			} else {
				details.setWarehousename(exceldatalist.get(i + 3).trim());
			}

			if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("NA")) {
				details.setStoragelocation("");
			} else {
				details.setStoragelocation(exceldatalist.get(i + 4).trim());
			}

			if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("NA")) {
				details.setStoragebin("");
			} else {
				details.setStoragebin(exceldatalist.get(i + 5).trim());
			}

			if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("NA")) {
				details.setMinqty(0.0);
			} else {
				details.setMinqty(Double.parseDouble(exceldatalist.get(i + 6).trim()));
			}

			if (exceldatalist.get(i + 7).trim().equalsIgnoreCase("NA")) {
				details.setMaxqty(0.0);
			} else {
				details.setMaxqty(Double.parseDouble(exceldatalist.get(i + 7).trim()));
			}

			if (exceldatalist.get(i + 8).trim().equalsIgnoreCase("NA")) {
				details.setNormallevel(0.0);
			} else {
				details.setNormallevel(Double.parseDouble(exceldatalist.get(i + 8).trim()));
			}

			if (exceldatalist.get(i + 9).trim().equalsIgnoreCase("NA")) {
				details.setReorderlevel(0.0);
			} else {
				details.setReorderlevel(Double.parseDouble(exceldatalist.get(i + 9).trim()));
			}

			if (exceldatalist.get(i + 10).trim().equalsIgnoreCase("NA")) {
				details.setAvailableqty(0.0);
			} else {
				details.setAvailableqty(Double.parseDouble(exceldatalist.get(i + 10).trim()));
			}
			// details.set
			List<ProductInventoryViewDetails> detailsList = new ArrayList<ProductInventoryViewDetails>();
			detailsList.add(details);
			productInventoryView.setDetails(detailsList);
			System.out.println("detailsList:::::::::::::::" + detailsList.size());

			// for (int j = 0; j < detailsList.size(); j++) {
			// System.out.println("Inside update stock loop:::::"+j);
			// UpdateStock.setProductInventoryTransaction(detailsList.get(j).getProdid(),
			// detailsList.get(j).getProdname(),
			// detailsList.get(j).getAvailableqty(), uom,
			// itemProduct.getPrice(), detailsList.get(j).getWarehousename(),
			// detailsList.get(j).getStoragelocation(),
			// detailsList.get(j).getStoragebin(), "Product Inventory View",
			// count+1, "", DateUtility.getDateWithTimeZone("IST", new Date()),
			// AppConstants.ADD, 0, detailsList.get(j).getAvailableqty(),
			// companyId);
			// }

			productInventoryView.setCount(++count);

			productInventoryView.setCompanyId(companyId);

			productInventorylist.add(productInventoryView);
		}
		lastcount = ofy().load().type(ProductInventoryView.class).filter("companyId", companyId).count();
		System.out.println("LastCount" + lastcount);

		ofy().save().entities(productInventorylist).now();

		ng.setNumber(count);
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(ng);

		System.out.println("Data saved successfuly=======================================");
		logger.log(Level.SEVERE, "Data saved successfuly=======================================");

		newcount = ofy().load().type(ProductInventoryView.class).filter("companyId", companyId).count();
		System.out.println("New Count" + newcount);

		System.out.println("number@@@" + ng.getNumber());

		fileRecords = productInventorylist.size();
		insertedRecords = newcount - lastcount;
		System.out.println("Result:" + insertedRecords);
		integerlist.add(fileRecords);
		integerlist.add(insertedRecords);
		System.out.println("SIZE:" + productInventorylist.size());

		// }
		return integerlist;
	}

	/**
	 * END
	 * 
	 */

	/**
	 * 
	 * Description: @param listofproductInventory
	 * 
	 * @param exceldatalist
	 * @param i
	 * @return boolean value whether the product is already created or not. Creation
	 *         Date: 15-Sept-2016 Project : EVA Erp Required For: Everyone(This was
	 *         created for NBHC Update). Created By : Rahul Verma
	 */
	private boolean validationforStockUpdate(List<ProductInventoryView> listofproductInventory,
			ArrayList<String> exceldatalist, int i) {
		// TODO Auto-generated method stub
		boolean value = true;
		for (int j = 0; j < listofproductInventory.size(); j++) {
			if (listofproductInventory.get(j).getProductCode().trim().equalsIgnoreCase(exceldatalist.get(i).trim())) {
				logger.log(Level.SEVERE, "Same Product Code is :::::::::" + exceldatalist.get(i).trim());
				return false;
			}
		}

		return value;
	}

	/**
	 * END
	 */

	/**
	 * 
	 * This is developed by anil on30/01/2017(merge)
	 */

	private ArrayList<Integer> companyAssetsave(long companyId, ArrayList<String> exceldatalist, boolean saveFlag) {

		int fileRecords;
		int insertedRecords;
		int lastCount;
		int newCount;

		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		ArrayList<CompanyAsset> companyAssetlist = new ArrayList<CompanyAsset>();

		List<CompanyAsset> listofcompanyAsset = ofy().load().type(CompanyAsset.class).filter("companyId", companyId)
				.list();
		System.out.println(listofcompanyAsset);

		NumberGeneration numgenration = new NumberGeneration();
		numgenration = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
				.filter("processName", "CompanyAsset").filter("status", true).first().now();

		long number = numgenration.getNumber();
		int count = (int) number;

		for (int i = 38; i < exceldatalist.size(); i += 38) {

			CompanyAsset companyasset = new CompanyAsset();

			boolean validcheck = validationforcompanyAsset(listofcompanyAsset, exceldatalist, i);

			System.out.println("The value of validcheck is : " + validcheck);

			if (validcheck == false) {
				integerlist.add(-16);
				System.out.println("Return Ho Gaya");
				return integerlist;
			}

			if (exceldatalist.get(i).equalsIgnoreCase("na")) {
				companyasset.setName("");

			} else {
				companyasset.setName(exceldatalist.get(i));
			}
			if (exceldatalist.get(i + 1).equalsIgnoreCase("na")) {
				companyasset.setCategory("");

			} else {
				companyasset.setCategory(exceldatalist.get(i + 1));
			}

			if (exceldatalist.get(i + 2).equalsIgnoreCase("na")) {
				companyasset.setBrand("");

			} else {
				companyasset.setBrand(exceldatalist.get(i + 2));

			}
			if (exceldatalist.get(i + 3).equalsIgnoreCase("na")) {
				companyasset.setModelNo("");
			} else {

				companyasset.setModelNo(exceldatalist.get(i + 3));
			}
			if (exceldatalist.get(i + 4).equalsIgnoreCase("na")) {
				companyasset.setSrNo("");
			} else {

				companyasset.setSrNo(exceldatalist.get(i + 4));
			}

			if (exceldatalist.get(i + 5).equalsIgnoreCase("na")) {
				companyasset.setPurchasedFrom("");
			} else {

				companyasset.setPurchasedFrom(exceldatalist.get(i + 5));
			}

			if (exceldatalist.get(i + 6).equalsIgnoreCase("na")) {
				companyasset.setPoNo("");
			} else {

				companyasset.setPoNo(exceldatalist.get(i + 6));
			}
			try {
				if (exceldatalist.get(i + 7).equalsIgnoreCase("na")) {

				} else {
					System.out.println("PO DATE : " + exceldatalist.get(i + 7));
					companyasset.setPODate(fmt.parse(exceldatalist.get(i + 7).trim()));
				}
			} catch (ParseException e) {
				e.printStackTrace();
				integerlist.add(-17);
				return integerlist;
			}

			try {
				if (exceldatalist.get(i + 8).equalsIgnoreCase("na")) {

				} else {
					System.out.println("MFG DATE : " + exceldatalist.get(i + 8));
					companyasset.setDateOfManufacture(fmt.parse(exceldatalist.get(i + 8).trim()));
				}
			} catch (ParseException e) {
				e.printStackTrace();
				integerlist.add(-17);
				return integerlist;
			}
			try {
				if (exceldatalist.get(i + 9).equalsIgnoreCase("na")) {

				} else {
					System.out.println("INSTALLATION DATE : " + exceldatalist.get(i + 9));
					companyasset.setDateOfInstallation(fmt.parse(exceldatalist.get(i + 9).trim()));
				}
			} catch (ParseException e) {
				e.printStackTrace();
				integerlist.add(-17);
				return integerlist;
			}

			try {

				if (exceldatalist.get(i + 10).equalsIgnoreCase("na")) {

				} else {
					System.out.println("WARR DATE : " + exceldatalist.get(i + 10));
					companyasset.setWarrenty(fmt.parse(exceldatalist.get(i + 10).trim()));
				}
			} catch (ParseException e) {
				e.printStackTrace();
				integerlist.add(-17);
				return integerlist;
			}

			if (exceldatalist.get(i + 11).equalsIgnoreCase("no")) {
				companyasset.setStatus(false);
			} else {
				companyasset.setStatus(true);
			}

			if (exceldatalist.get(i + 12).equalsIgnoreCase("na")) {

			} else {
				companyasset.setReferenceNumber(exceldatalist.get(i + 12));
			}
			if (exceldatalist.get(i + 13).equalsIgnoreCase("na")) {

			} else {
				companyasset.setWarehouseName(exceldatalist.get(i + 13));
			}
			if (exceldatalist.get(i + 14).equalsIgnoreCase("na")) {

			} else {
				companyasset.setStorageLocation(exceldatalist.get(i + 14));
			}
			if (exceldatalist.get(i + 15).equalsIgnoreCase("na")) {

			} else {
				companyasset.setStorageBin(exceldatalist.get(i + 15));
			}
			if (exceldatalist.get(i + 16).equalsIgnoreCase("na")) {

			} else {
				companyasset.setProductId(Integer.parseInt(exceldatalist.get(i + 16).trim()));
			}

			/**************************** article info ****************************/

			System.out.println("Before areticle type");
			ArticleTypeTravel articletype = null;
			List<ArticleTypeTravel> articletypelist = new ArrayList<ArticleTypeTravel>();

			// ///////////////// 1
			articletype = new ArticleTypeTravel();

			if (exceldatalist.get(i + 17).equalsIgnoreCase("na")) {
			} else {
				articletype.setArticleTypeName(exceldatalist.get(i + 17));
				System.out.println("hiiii" + exceldatalist.get(i + 17));
			}
			if (exceldatalist.get(i + 18).equalsIgnoreCase("na")) {
			} else {
				articletype.setArticleTypeValue(exceldatalist.get(i + 18));
			}
			if (exceldatalist.get(i + 19).equalsIgnoreCase("na")) {
			} else {
				articletype.setValidity(Integer.parseInt(exceldatalist.get(i + 19)));
			}

			try {
				if (exceldatalist.get(i + 20).equalsIgnoreCase("na")) {
				} else {
					System.out.println("issue Dt 1 " + exceldatalist.get(i + 20));
					articletype.setIssueDate(fmt.parse(exceldatalist.get(i + 20).trim()));
					Date validUntil = null;
					Date d1 = fmt.parse(exceldatalist.get(i + 20).trim());
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(d1);
					calendar.add(calendar.DATE, Integer.parseInt(exceldatalist.get(i + 19)));
					validUntil = calendar.getTime();
					articletype.setValidityuntil(validUntil);

				}
			} catch (ParseException e) {
				e.printStackTrace();
				integerlist.add(-17);
				return integerlist;
			}

			if (exceldatalist.get(i + 21).equalsIgnoreCase("na")) {
			} else {
				articletype.setIssuedAt(exceldatalist.get(i + 21));
			}
			if (exceldatalist.get(i + 22).equalsIgnoreCase("na")) {
			} else {
				articletype.setIssuedBy(exceldatalist.get(i + 23));
			}
			articletype.setStatus("Active");
			articletype.setCompanyId(companyId);
			if (!articletype.getArticleTypeName().equals("")) {
				articletypelist.add(articletype);
			}

			// ///////////////// 2
			articletype = new ArticleTypeTravel();

			if (exceldatalist.get(i + 23).equalsIgnoreCase("na")) {
			} else {
				articletype.setArticleTypeName(exceldatalist.get(i + 23));
				System.out.println("hiiii" + exceldatalist.get(i + 23));
			}
			if (exceldatalist.get(i + 24).equalsIgnoreCase("na")) {
			} else {
				articletype.setArticleTypeValue(exceldatalist.get(i + 24));
			}
			if (exceldatalist.get(i + 25).equalsIgnoreCase("na")) {
			} else {
				articletype.setValidity(Integer.parseInt(exceldatalist.get(i + 25)));
			}

			try {
				if (exceldatalist.get(i + 26).equalsIgnoreCase("na")) {
				} else {
					System.out.println("issue Dt 2 " + exceldatalist.get(i + 26));
					articletype.setIssueDate(fmt.parse(exceldatalist.get(i + 26).trim()));

					Date validUntil = null;
					Date d1 = fmt.parse(exceldatalist.get(i + 26).trim());
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(d1);
					calendar.add(calendar.DATE, Integer.parseInt(exceldatalist.get(i + 25)));
					validUntil = calendar.getTime();
					articletype.setValidityuntil(validUntil);
				}
			} catch (ParseException e) {
				e.printStackTrace();
				integerlist.add(-17);
				return integerlist;
			}

			if (exceldatalist.get(i + 27).equalsIgnoreCase("na")) {
			} else {
				articletype.setIssuedAt(exceldatalist.get(i + 27));
			}
			if (exceldatalist.get(i + 28).equalsIgnoreCase("na")) {
			} else {
				articletype.setIssuedBy(exceldatalist.get(i + 28));
			}
			articletype.setStatus("Active");
			articletype.setCompanyId(companyId);
			if (!articletype.getArticleTypeName().equals("")) {
				articletypelist.add(articletype);
			}

			// ///////////////// 3
			articletype = new ArticleTypeTravel();

			if (exceldatalist.get(i + 29).equalsIgnoreCase("na")) {
			} else {
				articletype.setArticleTypeName(exceldatalist.get(i + 29));
				System.out.println("hiiii" + exceldatalist.get(i + 29));
			}
			if (exceldatalist.get(i + 30).equalsIgnoreCase("na")) {
			} else {
				articletype.setArticleTypeValue(exceldatalist.get(i + 30));
			}
			if (exceldatalist.get(i + 31).equalsIgnoreCase("na")) {
			} else {
				articletype.setValidity(Integer.parseInt(exceldatalist.get(i + 31)));
			}

			try {
				if (exceldatalist.get(i + 32).equalsIgnoreCase("na")) {
				} else {
					System.out.println("issue Dt 3 " + exceldatalist.get(i + 32));
					articletype.setIssueDate(fmt.parse(exceldatalist.get(i + 32).trim()));
					Date validUntil = null;
					Date d1 = fmt.parse(exceldatalist.get(i + 32).trim());
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(d1);
					calendar.add(calendar.DATE, Integer.parseInt(exceldatalist.get(i + 31)));
					validUntil = calendar.getTime();
					articletype.setValidityuntil(validUntil);
				}
			} catch (ParseException e) {
				e.printStackTrace();
				integerlist.add(-17);
				return integerlist;
			}

			if (exceldatalist.get(i + 33).equalsIgnoreCase("na")) {
			} else {
				articletype.setIssuedAt(exceldatalist.get(i + 33));
			}
			if (exceldatalist.get(i + 34).equalsIgnoreCase("na")) {
			} else {
				articletype.setIssuedBy(exceldatalist.get(i + 34));
			}
			articletype.setStatus("Active");
			articletype.setCompanyId(companyId);
			if (!articletype.getArticleTypeName().equals("")) {
				articletypelist.add(articletype);
			}

			System.out.println("HI Article type list:===" + articletypelist.size());
			companyasset.setArticleTypeDetails(articletypelist);

			if (exceldatalist.get(i + 35).equalsIgnoreCase("na")) {

			} else {
				companyasset.setGrnId(Integer.parseInt(exceldatalist.get(i + 35).trim()));
			}

			if (exceldatalist.get(i + 36).equalsIgnoreCase("na")) {

			} else {
				companyasset.setBranch(exceldatalist.get(i + 36).trim());
			}
			/** date 7.12.2018 added by komal for asset price **/
			if (exceldatalist.get(i + 37).equalsIgnoreCase("na")) {
				companyasset.setPrice(0);
			} else {
				double price = 0;
				try {
					price = Double.parseDouble(exceldatalist.get(i + 37).trim());
				} catch (Exception e) {
					price = 0;
				}
				companyasset.setPrice(price);

			}

			/**********************************
			 * article info end here
			 ***************************************/

			System.out.println("HIIIIIIIIIIIIIIIIII");

			System.out.println("SAVE FLAG " + saveFlag);

			if (saveFlag) {
				System.out.println("Setting Count");
				companyasset.setCount(++count);
			}

			companyasset.setCompanyId(companyId);
			companyAssetlist.add(companyasset);
			System.out.println("Still inside loop");

		}

		if (saveFlag) {
			System.out.println("Saving data");
			System.out.println("Now Out of loop");
			lastCount = ofy().load().type(CompanyAsset.class).filter("companyId", companyId).count();
			System.out.println("LastCount" + lastCount);

			ofy().save().entities(companyAssetlist).now();

			numgenration.setNumber(count);
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(numgenration);

			System.out.println("Data saved successfuly=======================================");
			logger.log(Level.SEVERE, "Data saved successfuly=======================================");

			newCount = ofy().load().type(CompanyAsset.class).filter("companyId", companyId).count();
			System.out.println("New Count" + newCount);
			System.out.println("number@@@" + numgenration.getNumber());

			fileRecords = companyAssetlist.size();
			insertedRecords = newCount - lastCount;
			System.out.println("Result:" + insertedRecords);
			integerlist.add(fileRecords);
			integerlist.add(insertedRecords);
			System.out.println("SIZE:" + companyAssetlist.size());
		}

		return integerlist;

	}

	private boolean validationforcompanyAsset(List<CompanyAsset> listofcompanyAsset, ArrayList<String> exceldatalist,
			int i) {
		boolean value = true;
		System.out.println(" registration numer == " + exceldatalist.get(i).trim());
		for (int j = 0; j < listofcompanyAsset.size(); j++) {
			if (listofcompanyAsset.get(j).getName().trim().equalsIgnoreCase(exceldatalist.get(i).trim())) {
				value = false;
			}
		}
		return value;

	}

	/**
	 * ends here
	 */

	/**
	 * nidhi 25-10-2018 update for min upload with multiple instance
	 */
	@Override
	public ArrayList<String> savedContractTransactionDetails(long companyid, String entityname, Boolean serviceStatus,
			Boolean billingStatus, String loginPerson) {
		// TODO Auto-generated method stub
		ArrayList<String> intlist = new ArrayList<String>();
		ArrayList<String> exceldatalist = new ArrayList<String>();

		/***************************************
		 * Reading Excel File
		 *****************************************/
		// ////
		logger.log(Level.SEVERE, " Get Transaction details method -- " + entityname);

		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));

		try {

			DataFormatter datafmt = new DataFormatter();
			long i;

			Workbook wb = null;

//			try {
//				wb = WorkbookFactory.create(new BlobstoreInputStream(blobkey));
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			try {
				logger.log(Level.SEVERE, " get workbook before ");
				wb = WorkbookFactory.create(new BlobstoreInputStream(blobkey));
				logger.log(Level.SEVERE, " get workbook after ");

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				intlist.add("UnAble to read Excel Sheet. Please Try after some time");
//				ErrorDetailsList.put("ErrorList", intlist);
//				minUploadDt.setErrorList(intlist);
				return intlist;
			}

			Sheet sheet = wb.getSheetAt(0);

			Iterator<Row> rowIterator = sheet.iterator();

			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();

				Iterator<Cell> celliterator = row.cellIterator();

				while (celliterator.hasNext()) {
					Cell cell = celliterator.next();

					switch (cell.getCellType()) {

					case Cell.CELL_TYPE_STRING:
						exceldatalist.add(cell.getStringCellValue());
						break;

					case Cell.CELL_TYPE_NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) {
							exceldatalist.add(fmt.format(cell.getDateCellValue()));

						} else {

							cell.getNumericCellValue();
							String sampletest = datafmt.formatCellValue(cell);
							System.out.println("sampletest :" + sampletest);
							// i = (long) cell.getNumericCellValue();
							exceldatalist.add(sampletest + "");

							System.out.println("Value:" + sampletest);
						}
						break;

					case Cell.CELL_TYPE_BOOLEAN:

						break;
					default:

					}
					// System.out.println("List Size " + exceldatalist.size());
				}
			}
		} catch (Exception e) {
			intlist.add("-4");
			e.printStackTrace();
		}
		/******************** Finished Reading Excel Sheet *************************/
		// ////

		try {

			logger.log(Level.SEVERE,
					" value  ;; -->> " + entityname + " get first value " + exceldatalist.get(0).trim());
			for (int i = 0; i < 36; i++) {
				System.out.println(" i -->" + i + " --" + exceldatalist.get(i));
			}
			/**
			 * nidhi 13-03-2018 added some new field
			 */
			if (entityname.equals("Customer With Service Details")) {
				if (exceldatalist.get(0).trim().equalsIgnoreCase("IsCompany")
						&& exceldatalist.get(1).trim().equalsIgnoreCase("Company Name")
						&& exceldatalist.get(2).trim().equalsIgnoreCase("Poc Name/Fullname")
						&& exceldatalist.get(3).trim().equalsIgnoreCase("Cell Number")
						&& exceldatalist.get(4).trim().equalsIgnoreCase("Landline Number")
						&& exceldatalist.get(5).trim().equalsIgnoreCase("Email")
						&& exceldatalist.get(6).trim().equalsIgnoreCase("Customer GST No.")
						&& exceldatalist.get(7).trim().equalsIgnoreCase("Address Line 1")
						&& exceldatalist.get(8).trim().equalsIgnoreCase("Address Line 2")
						&& exceldatalist.get(9).trim().equalsIgnoreCase("Landmark")
						&& exceldatalist.get(10).trim().equalsIgnoreCase("Locality")
						&& exceldatalist.get(11).trim().equalsIgnoreCase("Pin Number")
						&& exceldatalist.get(12).trim().equalsIgnoreCase("City")
						&& exceldatalist.get(13).trim().equalsIgnoreCase("State")
						&& exceldatalist.get(14).trim().equalsIgnoreCase("Country")
						&& exceldatalist.get(15).trim().equalsIgnoreCase("Address Line 1")
						&& exceldatalist.get(16).trim().equalsIgnoreCase("Address Line 2")
						&& exceldatalist.get(17).trim().equalsIgnoreCase("Landmark")
						&& exceldatalist.get(18).trim().equalsIgnoreCase("Locality")
						&& exceldatalist.get(19).trim().equalsIgnoreCase("Pin Number")
						&& exceldatalist.get(20).trim().equalsIgnoreCase("City")
						&& exceldatalist.get(21).trim().equalsIgnoreCase("State")
						&& exceldatalist.get(22).trim().equalsIgnoreCase("Country")
						&& exceldatalist.get(23).trim().equalsIgnoreCase("Reference Order Id")
						&& exceldatalist.get(24).trim().equalsIgnoreCase("Contract Date(MM/dd/yyyy)")
						&& exceldatalist.get(25).trim().equalsIgnoreCase("Contract Start Date(MM/dd/yyyy)")
						&& exceldatalist.get(26).trim().equalsIgnoreCase("Contract End Date(MM/dd/yyyy)")
						&& exceldatalist.get(27).trim().equalsIgnoreCase("Contract Group")
						&& exceldatalist.get(28).trim().equalsIgnoreCase("Contract Category")
						&& exceldatalist.get(29).trim().equalsIgnoreCase("Contract Type")
						&& exceldatalist.get(30).trim().equalsIgnoreCase("Sales Person")
						&& exceldatalist.get(31).trim().equalsIgnoreCase("Branch")
						&& exceldatalist.get(32).trim().equalsIgnoreCase("Product Name")
						&& exceldatalist.get(33).trim().equalsIgnoreCase("Number of Services")
						&& exceldatalist.get(34).trim().equalsIgnoreCase("Product Price")
						&& exceldatalist.get(35).trim().equalsIgnoreCase("Tax 1") // Added by sheetal 16-11-2021
						&& exceldatalist.get(36).trim().equalsIgnoreCase("Tax 2") // Added by sheetal 16-11-2021

						&& exceldatalist.get(37).trim().equalsIgnoreCase("Outsatanding Amount")
						&& exceldatalist.get(38).trim().equalsIgnoreCase("Cutoff Date For Service Creation(MM/dd/yyyy)")
						&& exceldatalist.get(39).trim().equalsIgnoreCase("Is Rate Contract (Yes/No)")
						&& exceldatalist.get(40).trim().equalsIgnoreCase("Service Wise Billing (Yes/No)")
						&& exceldatalist.get(41).trim().equalsIgnoreCase("Consilated (Yes/No)")
						&& exceldatalist.get(42).trim().equalsIgnoreCase("Sales source")
						&& exceldatalist.get(43).trim().equalsIgnoreCase("Types of frequancy")
						&& exceldatalist.get(44).trim().equalsIgnoreCase("Types of Treatment")
						&& exceldatalist.get(45).trim().equalsIgnoreCase("Salutation")
						&& exceldatalist.get(46).trim().equalsIgnoreCase("Model No.")
						&& exceldatalist.get(47).trim().equalsIgnoreCase("Serial No.")
						&& exceldatalist.get(48).trim().equalsIgnoreCase("Day")
						&& exceldatalist.get(49).trim().equalsIgnoreCase("Time")
						&& exceldatalist.get(50).trim().equalsIgnoreCase("Number Range")
						&& exceldatalist.get(51).trim().equalsIgnoreCase("Service Duration")
						&& exceldatalist.get(52).trim().equalsIgnoreCase("Correspondence Name") // added by Priyanka
																								// Bhagwat for star cool
						&& exceldatalist.get(53).trim().equalsIgnoreCase("Description")) // added by priyanka for star
																							// cool

				{
					System.out.println("get here data match ");
					logger.log(Level.SEVERE, "Excelsheet matched.. ");
					intlist = customerServiceDetailSave(companyid, exceldatalist, serviceStatus, billingStatus,
							loginPerson);
				} else {
					intlist.add("-1");
					intlist.add("Invalid Excel Sheet Headers Please check.");
				}
			} /**
				 * nidhi 27-10-2017 25-01-2018 ' commented for uploded method upload process
				 * with multi instance
				 */
//			else if (entityname.equals("MIN Upload")) {
//				if (exceldatalist.get(0).trim().equalsIgnoreCase("SERVICE ID")
//						&& exceldatalist.get(1).trim()
//								.equalsIgnoreCase("BRANCH NAME")
//						&& exceldatalist.get(2).trim()
//								.equalsIgnoreCase("CONTRACT ID")
//						&& exceldatalist.get(3).trim()
//								.equalsIgnoreCase("SERVICE DATE(dd-mm-yyyy)")
//						&& exceldatalist
//								.get(4)
//								.trim()
//								.equalsIgnoreCase(
//										"Service Completion Date(dd-mm-yyyy)")
//						&& exceldatalist
//								.get(5)
//								.trim()
//								.equalsIgnoreCase(
//										"Service Completion time(Minutes)")
//						&& exceldatalist.get(6).trim()
//								.equalsIgnoreCase("Remark")
//						&& exceldatalist.get(7).trim()
//								.equalsIgnoreCase("SERVICE TYPE")
//						&& exceldatalist.get(8).trim().equalsIgnoreCase("UNIT")
//						&& exceldatalist.get(9).trim().equalsIgnoreCase("UOM")
//						&& exceldatalist.get(10).trim()
//								.equalsIgnoreCase("EMPLOYEE ID(USER's ID )")
//						&& exceldatalist
//								.get(11)
//								.trim()
//								.equalsIgnoreCase(
//										"EMPLOYEE ID(Techinician mapped to project)")
//						&& exceldatalist
//								.get(12)
//								.trim()
//								.equalsIgnoreCase(
//										"EMPLOYEE ID(Techinician mapped to project)")
//						&& exceldatalist
//								.get(13)
//								.trim()
//								.equalsIgnoreCase(
//										"EMPLOYEE ID(Techinician mapped to project)")
//						&& exceldatalist
//								.get(14)
//								.trim()
//								.equalsIgnoreCase(
//										"EMPLOYEE ID(Techinician mapped to project)")
//						&& exceldatalist
//								.get(15)
//								.trim()
//								.equalsIgnoreCase(
//										"EMPLOYEE ID(Techinician mapped to project)")
//						&& exceldatalist.get(16).trim()
//								.equalsIgnoreCase("MATERIAL ID 1")
//						&& exceldatalist.get(17).trim()
//								.equalsIgnoreCase("WAREHOUSE 1")
//						&& exceldatalist.get(18).trim()
//								.equalsIgnoreCase("CONSUMPTION VALUE 1")
//						&& exceldatalist.get(19).trim()
//								.equalsIgnoreCase("MATERIAL ID 2")
//						&& exceldatalist.get(20).trim()
//								.equalsIgnoreCase("WAREHOUSE 2")
//						&& exceldatalist.get(21).trim()
//								.equalsIgnoreCase("CONSUMPTION VALUE 2")
//						&& exceldatalist.get(22).trim()
//								.equalsIgnoreCase("MATERIAL ID 3")
//						&& exceldatalist.get(23).trim()
//								.equalsIgnoreCase("WAREHOUSE 3")
//						&& exceldatalist.get(24).trim()
//								.equalsIgnoreCase("CONSUMPTION VALUE 3")
//						&& exceldatalist.get(25).trim()
//								.equalsIgnoreCase("MATERIAL ID 4")
//						&& exceldatalist.get(26).trim()
//								.equalsIgnoreCase("WAREHOUSE 4")
//						&& exceldatalist.get(27).trim()
//								.equalsIgnoreCase("CONSUMPTION VALUE 4")
//						&& exceldatalist.get(28).trim()
//								.equalsIgnoreCase("MATERIAL ID 5")
//						&& exceldatalist.get(29).trim()
//								.equalsIgnoreCase("WAREHOUSE 5")
//						&& exceldatalist.get(30).trim()
//								.equalsIgnoreCase("CONSUMPTION VALUE 5")
//						&& exceldatalist.get(31).trim()
//								.equalsIgnoreCase("MATERIAL ID 6")
//						&& exceldatalist.get(32).trim()
//								.equalsIgnoreCase("WAREHOUSE 6")
//						&& exceldatalist.get(33).trim()
//								.equalsIgnoreCase("CONSUMPTION VALUE 6")
//						&& exceldatalist.get(34).trim()
//								.equalsIgnoreCase("MATERIAL ID 7")
//						&& exceldatalist.get(35).trim()
//								.equalsIgnoreCase("WAREHOUSE 7")
//						&& exceldatalist.get(36).trim()
//								.equalsIgnoreCase("CONSUMPTION VALUE 7")
//						&& exceldatalist.get(37).trim()
//								.equalsIgnoreCase("MATERIAL ID 8")
//						&& exceldatalist.get(38).trim()
//								.equalsIgnoreCase("WAREHOUSE 8")
//						&& exceldatalist.get(39).trim()
//								.equalsIgnoreCase("CONSUMPTION VALUE 8")
//						&& exceldatalist.get(40).trim()
//								.equalsIgnoreCase("MATERIAL ID 9")
//						&& exceldatalist.get(41).trim()
//								.equalsIgnoreCase("WAREHOUSE 9")
//						&& exceldatalist.get(42).trim()
//								.equalsIgnoreCase("CONSUMPTION VALUE 9")
//						&& exceldatalist.get(43).trim()
//								.equalsIgnoreCase("MATERIAL ID 10")
//						&& exceldatalist.get(44).trim()
//								.equalsIgnoreCase("WAREHOUSE 10")
//						&& exceldatalist.get(45).trim()
//								.equalsIgnoreCase("CONSUMPTION VALUE 10")) { // (long
//																				// companyId,
//					intlist = saveMinUploadProcess(companyid, exceldatalist,
//							false, false, loginPerson);
//				} else {
//					intlist.add("In valid Excel Formate");
//				}
//			}
			/*
			 * nidhi 26-03-2018 commented for new upload process else if
			 * (entityname.equals("Contract Upload")) { if (exceldatalist.get(0).trim()
			 * .equalsIgnoreCase("QUOTATION ID") && exceldatalist.get(1).trim()
			 * .equalsIgnoreCase("LEAD ID") && exceldatalist.get(2).trim()
			 * .equalsIgnoreCase("REF NUMBER1") && exceldatalist.get(3).trim()
			 * .equalsIgnoreCase("CUSTOMER ID") && exceldatalist.get(4).trim()
			 * .equalsIgnoreCase("CUSTOMER NAME") && exceldatalist.get(5).trim()
			 * .equalsIgnoreCase("CONTRACT DATE") && exceldatalist.get(6).trim()
			 * .equalsIgnoreCase("CONTRACT GROUP") && exceldatalist.get(7).trim()
			 * .equalsIgnoreCase("CONTRACT CATEGORY") && exceldatalist.get(8).trim()
			 * .equalsIgnoreCase("CONTRACT TYPE") && exceldatalist.get(9).trim()
			 * .equalsIgnoreCase("BRANCH") && exceldatalist.get(10).trim()
			 * .equalsIgnoreCase("SALES PERSON") && exceldatalist.get(11).trim()
			 * .equalsIgnoreCase("APPROVER NAME") && exceldatalist.get(12).trim()
			 * .equalsIgnoreCase("PAYMENT TREMS") && exceldatalist.get(13).trim()
			 * .equalsIgnoreCase("Is rate Contract")
			 * 
			 * && exceldatalist.get(14).trim() .equalsIgnoreCase("PRODUCT ID") &&
			 * exceldatalist.get(15).trim() .equalsIgnoreCase("PRODUCT NAME") &&
			 * exceldatalist.get(16).trim() .equalsIgnoreCase("Price / Unit Price") &&
			 * exceldatalist.get(17).trim() .equalsIgnoreCase("Unit") &&
			 * exceldatalist.get(18).trim() .equalsIgnoreCase("No Of Service") &&
			 * exceldatalist.get(19).trim() .equalsIgnoreCase("Contract Start date") &&
			 * exceldatalist.get(20).trim() .equalsIgnoreCase("Contract duration In Days")
			 * 
			 * && exceldatalist.get(21).trim() .equalsIgnoreCase("PRODUCT ID") &&
			 * exceldatalist.get(22).trim() .equalsIgnoreCase("PRODUCT NAME") &&
			 * exceldatalist.get(23).trim() .equalsIgnoreCase("Price / Unit Price") &&
			 * exceldatalist.get(24).trim() .equalsIgnoreCase("Unit") &&
			 * exceldatalist.get(25).trim() .equalsIgnoreCase("No Of Service") &&
			 * exceldatalist.get(26).trim() .equalsIgnoreCase("Contract Start date") &&
			 * exceldatalist.get(27).trim() .equalsIgnoreCase("Contract duration In Days")
			 * 
			 * && exceldatalist.get(28).trim() .equalsIgnoreCase("PRODUCT ID") &&
			 * exceldatalist.get(29).trim() .equalsIgnoreCase("PRODUCT NAME") &&
			 * exceldatalist.get(30).trim() .equalsIgnoreCase("Price / Unit Price") &&
			 * exceldatalist.get(31).trim() .equalsIgnoreCase("Unit") &&
			 * exceldatalist.get(32).trim() .equalsIgnoreCase("No Of Service") &&
			 * exceldatalist.get(33).trim() .equalsIgnoreCase("Contract Start date") &&
			 * exceldatalist.get(34).trim() .equalsIgnoreCase("Contract duration In Days")
			 * 
			 * && exceldatalist.get(35).trim() .equalsIgnoreCase("PRODUCT ID") &&
			 * exceldatalist.get(36).trim() .equalsIgnoreCase("PRODUCT NAME") &&
			 * exceldatalist.get(37).trim() .equalsIgnoreCase("Price / Unit Price") &&
			 * exceldatalist.get(38).trim() .equalsIgnoreCase("Unit") &&
			 * exceldatalist.get(39).trim() .equalsIgnoreCase("No Of Service") &&
			 * exceldatalist.get(40).trim() .equalsIgnoreCase("Contract Start date") &&
			 * exceldatalist.get(41).trim() .equalsIgnoreCase("Contract duration In Days")
			 * 
			 * && exceldatalist.get(42).trim() .equalsIgnoreCase("PRODUCT ID") &&
			 * exceldatalist.get(43).trim() .equalsIgnoreCase("PRODUCT NAME") &&
			 * exceldatalist.get(44).trim() .equalsIgnoreCase("Price / Unit Price") &&
			 * exceldatalist.get(45).trim() .equalsIgnoreCase("Unit") &&
			 * exceldatalist.get(46).trim() .equalsIgnoreCase("No Of Service") &&
			 * exceldatalist.get(47).trim() .equalsIgnoreCase("Contract Start date") &&
			 * exceldatalist.get(48).trim() .equalsIgnoreCase("Contract duration In Days")
			 * 
			 * ) {
			 * 
			 * if(exceldatalist.size()>784){ intlist.add("-2"); }else{ // intlist =
			 * checkContractUploadDetails(companyid, exceldatalist, loginPerson); }
			 * 
			 * } else { intlist.add("Invalid Excel Sheet"); } }
			 */ else {
				intlist.add("Invalid Excel Sheet");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		return intlist;
	}

	@Override
	public MINUploadExcelDetails savedUploadTransactionDetails(long companyid, String entityname, Boolean serviceStatus,
			Boolean billingStatus, String loginPerson) {
		// TODO Auto-generated method stub
		HashMap<String, Object> ErrorDetailsList = new HashMap<String, Object>();
		ArrayList<String> intlist = new ArrayList<String>();
		ArrayList<String> exceldatalist = new ArrayList<String>();
		MINUploadExcelDetails minUploadDt = new MINUploadExcelDetails();
		/***************************************
		 * Reading Excel File
		 *****************************************/
		// ////
		logger.log(Level.SEVERE, " Get Transaction details method -- " + entityname + " size -- ");
		try {

			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			sefmt.setTimeZone(TimeZone.getTimeZone("IST"));

			DataFormatter datafmt = new DataFormatter();
			long i;

			Workbook wb = null;

			try {
				logger.log(Level.SEVERE, " get workbook before ");
				wb = WorkbookFactory.create(new BlobstoreInputStream(blobkey));
				logger.log(Level.SEVERE, " get workbook after ");

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				intlist.add("UnAble to read Excel Sheet. Please Try after some time.");
				ErrorDetailsList.put("ErrorList", intlist);
				minUploadDt.setErrorList(intlist);
				return minUploadDt;
			} finally {
			}

			Sheet sheet = wb.getSheetAt(0);

			Iterator<Row> rowIterator = sheet.iterator();

			logger.log(Level.SEVERE, " iterator size =-- " + rowIterator.toString());
			rEnd: while (rowIterator.hasNext()) {
				Row row = rowIterator.next();

				Iterator<Cell> celliterator = row.cellIterator();
				cEnd: while (celliterator.hasNext()) {
					Cell cell = celliterator.next();

					switch (cell.getCellType()) {

					case Cell.CELL_TYPE_STRING:
						if (cell.getStringCellValue().trim().equalsIgnoreCase("@end")) {

							break cEnd;
						} else if (cell.getStringCellValue().trim().equalsIgnoreCase("@Rend")) {
							break rEnd;
						} else {
							exceldatalist.add(cell.getStringCellValue());
						}
						break;

					case Cell.CELL_TYPE_NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) {
							exceldatalist.add(sefmt.format(cell.getDateCellValue()));
						} else {
							cell.getNumericCellValue();
							String sampletest = datafmt.formatCellValue(cell);
							logger.log(Level.SEVERE, "excel date --" + sampletest);
//							System.out.println("sampletest :" + sampletest);
							// i = (long) cell.getNumericCellValue();
							exceldatalist.add(sampletest + "");
//							System.out.println("Value:" + sampletest);
						}
						break;

					case Cell.CELL_TYPE_BOOLEAN:

						break;
					default:

					}
					// System.out.println("List Size " + exceldatalist.size());
				}
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, " size -- " + exceldatalist.size());
			logger.log(Level.SEVERE, " exception --" + e);
			e.printStackTrace();
			intlist.add("Excel Sheet Invalide data Formate ");
			ErrorDetailsList.put("ErrorList", intlist);
			minUploadDt.setErrorList(intlist);
			return minUploadDt;
		} finally {

		}
		/******************** Finished Reading Excel Sheet *************************/
		// ////

		try {

			/*** 01/11/2019 By Vijay getting null pointer exception so **/
			errorList = new ArrayList<String>();

			logger.log(Level.SEVERE,
					" value  ;; -->> " + entityname + " get first value " + exceldatalist.get(0).trim());
			// for(int i = 0; i< 36;i++){
			// System.out.println(" i -->" + i + " --" + exceldatalist.get(i));
			// }
			/*
			 * nidhi 27-10-2017
			 */
			if (entityname.equals("MIN Upload")) {
				if (exceldatalist.get(0).trim().equalsIgnoreCase("SERVICE ID")
						&& exceldatalist.get(1).trim().equalsIgnoreCase("BRANCH NAME")
						&& exceldatalist.get(2).trim().equalsIgnoreCase("CONTRACT ID")
						&& exceldatalist.get(3).trim().equalsIgnoreCase("SERVICE DATE(dd-mm-yyyy)")
						&& exceldatalist.get(4).trim().equalsIgnoreCase("Service Completion Date(dd-mm-yyyy)")
						&& exceldatalist.get(5).trim().equalsIgnoreCase("Service Completion time(Minutes)")
						&& exceldatalist.get(6).trim().equalsIgnoreCase("Remark")
						&& exceldatalist.get(7).trim().equalsIgnoreCase("SERVICE TYPE")
						&& exceldatalist.get(8).trim().equalsIgnoreCase("UNIT")
						&& exceldatalist.get(9).trim().equalsIgnoreCase("UOM")
						&& exceldatalist.get(10).trim().equalsIgnoreCase("EMPLOYEE ID(USER's ID )")
						&& exceldatalist.get(11).trim().equalsIgnoreCase("EMPLOYEE ID(Techinician mapped to project)")
						&& exceldatalist.get(12).trim().equalsIgnoreCase("EMPLOYEE ID(Techinician mapped to project)")
						&& exceldatalist.get(13).trim().equalsIgnoreCase("EMPLOYEE ID(Techinician mapped to project)")
						&& exceldatalist.get(14).trim().equalsIgnoreCase("EMPLOYEE ID(Techinician mapped to project)")
						&& exceldatalist.get(15).trim().equalsIgnoreCase("EMPLOYEE ID(Techinician mapped to project)")
						&& exceldatalist.get(16).trim().equalsIgnoreCase("MATERIAL ID 1")
						&& exceldatalist.get(17).trim().equalsIgnoreCase("WAREHOUSE 1")
						&& exceldatalist.get(18).trim().equalsIgnoreCase("CONSUMPTION VALUE 1")
						&& exceldatalist.get(19).trim().equalsIgnoreCase("MATERIAL ID 2")
						&& exceldatalist.get(20).trim().equalsIgnoreCase("WAREHOUSE 2")
						&& exceldatalist.get(21).trim().equalsIgnoreCase("CONSUMPTION VALUE 2")
						&& exceldatalist.get(22).trim().equalsIgnoreCase("MATERIAL ID 3")
						&& exceldatalist.get(23).trim().equalsIgnoreCase("WAREHOUSE 3")
						&& exceldatalist.get(24).trim().equalsIgnoreCase("CONSUMPTION VALUE 3")
						&& exceldatalist.get(25).trim().equalsIgnoreCase("MATERIAL ID 4")
						&& exceldatalist.get(26).trim().equalsIgnoreCase("WAREHOUSE 4")
						&& exceldatalist.get(27).trim().equalsIgnoreCase("CONSUMPTION VALUE 4")
						&& exceldatalist.get(28).trim().equalsIgnoreCase("MATERIAL ID 5")
						&& exceldatalist.get(29).trim().equalsIgnoreCase("WAREHOUSE 5")
						&& exceldatalist.get(30).trim().equalsIgnoreCase("CONSUMPTION VALUE 5")
						&& exceldatalist.get(31).trim().equalsIgnoreCase("MATERIAL ID 6")
						&& exceldatalist.get(32).trim().equalsIgnoreCase("WAREHOUSE 6")
						&& exceldatalist.get(33).trim().equalsIgnoreCase("CONSUMPTION VALUE 6")
						&& exceldatalist.get(34).trim().equalsIgnoreCase("MATERIAL ID 7")
						&& exceldatalist.get(35).trim().equalsIgnoreCase("WAREHOUSE 7")
						&& exceldatalist.get(36).trim().equalsIgnoreCase("CONSUMPTION VALUE 7")
						&& exceldatalist.get(37).trim().equalsIgnoreCase("MATERIAL ID 8")
						&& exceldatalist.get(38).trim().equalsIgnoreCase("WAREHOUSE 8")
						&& exceldatalist.get(39).trim().equalsIgnoreCase("CONSUMPTION VALUE 8")
						&& exceldatalist.get(40).trim().equalsIgnoreCase("MATERIAL ID 9")
						&& exceldatalist.get(41).trim().equalsIgnoreCase("WAREHOUSE 9")
						&& exceldatalist.get(42).trim().equalsIgnoreCase("CONSUMPTION VALUE 9")
						&& exceldatalist.get(43).trim().equalsIgnoreCase("MATERIAL ID 10")
						&& exceldatalist.get(44).trim().equalsIgnoreCase("WAREHOUSE 10")
						&& exceldatalist.get(45).trim().equalsIgnoreCase("CONSUMPTION VALUE 10")) { // (long
//					intlist.add("Success");									
					// companyId,
//					intlist = saveMinUploadProcess(companyid, exceldatalist,
//							false, false, loginPerson);
					logger.log(Level.SEVERE, "get... header match...");
					if (exceldatalist.size() > 4646) {
						errorList.add("List contains more than 100 records!");
					}
				} else {
					errorList.add("Please check Excel Headers.");
					intlist.add("Please check Excel Headers.");
					ErrorDetailsList.put(ERRORLIST, intlist);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			errorList.add("Error while reading Excel Sheet");
			minUploadDt.setErrorList(errorList);
//			intlist.add("Error while reading Excel Sheet");
//			minUploadDt.setErrorList(intlist);
			return minUploadDt;
		} finally {
			if (intlist.size() == 0 && entityname.equals("MIN Upload")) {
				ErrorDetailsList.put("ErrorList", intlist.toString());
				ErrorDetailsList.put(COMPANYID, "000");
				ErrorDetailsList.put(LOGINNAME, loginPerson);
				ErrorDetailsList.put(EXCELSHIT, exceldatalist.toString());
			}
		}
		minUploadDt.setExceldatalist(exceldatalist);
		minUploadDt.setLoginName(loginPerson);
		minUploadDt.setCompanyId(companyid);
//		minUploadDt.setErrorList(intlist);
		minUploadDt.setErrorList(errorList);

		/**
		 * @author Anil , Date : 20-12-2019 if error captured in validation programme
		 *         mannuly clearing bolbkey
		 */
		if (errorList.size() != 0) {
			blobkey = null;
		}
		return minUploadDt;
	}

	/**
	 * nidhi 13-03-2018 changes for new updated fields
	 * 
	 * @param companyId
	 * @param exceldatalist
	 * @param serviceStatus
	 * @param billingStatus
	 * @param loginPerson
	 * @return
	 */
	private ArrayList<String> customerServiceDetailSave(long companyId, ArrayList<String> exceldatalist,
			Boolean serviceStatus, Boolean billingStatus, String loginPerson) {

		ArrayList<String> integerlist = new ArrayList<String>();

		logger.log(Level.SEVERE, "The count is :" + exceldatalist.size());

		ArrayList<String> weekDays = new ArrayList<String>(
				Arrays.asList("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"));

		CustomerContractDetails custdetail = null;
//		HashSet<String> errorExList = new  HashSet<String>();
		ArrayList<String> errorExList = new ArrayList<String>();

//		HashSet<String> custCategory=new HashSet<String>();
//		HashSet<String> custType=new HashSet<String>();
//		HashSet<String> numberRangeHS=new HashSet<String>();
		HashSet<String> salesPerson = new HashSet<String>();
		HashSet<String> branchName = new HashSet<String>();
		ArrayList<String> errorList = new ArrayList<String>();

		List<City> globalCitylist = ofy().load().type(City.class).filter("companyId", companyId).list();

		List<CustomerContractDetails> custList = new ArrayList<CustomerContractDetails>();
		try {
			java.text.DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			Date dateobj = new Date();
			Date date = new Date();
			System.out.println(df.format(dateobj));
			TreeSet<String> refList = new TreeSet<String>();
			int rowCount = 1;

			/*** 11-12-2019 Deepak Salve added below code ***/
			try {
				for (int i = 54; i < exceldatalist.size() - 1; i += 54) { /** Date 23-11-2020 added by Priyanka **/
					// for (int i = 50; i < exceldatalist.size()-1; i += 50) {
//				custCategory.add(exceldatalist.get(i+28));
//				custType.add(exceldatalist.get(i+29));
					salesPerson.add(exceldatalist.get(i + 30));
//				numberRangeHS.add(exceldatalist.get(i+48));
//				
//				System.out.println("1"+exceldatalist.get(i+28));
//				System.out.println("2"+exceldatalist.get(i+29));
//				System.out.println("3"+exceldatalist.get(i+29));
					branchName.add(exceldatalist.get(i + 31));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

//			ArrayList<String> categoryList=new ArrayList<String>(custCategory);
//			ArrayList<String> typeList=new ArrayList<String>(custType);
			ArrayList<String> arraysalesperon = new ArrayList<String>(salesPerson);
//			ArrayList<String> numRange=new ArrayList<String>(numberRangeHS);
			ArrayList<String> strbrancharray = new ArrayList<String>(branchName);

			ServerAppUtility serverutitlity = new ServerAppUtility();
			List<ConfigCategory> contractCategorylist = serverutitlity.loadConfigsCategoryData(4, companyId);
			List<Config> contractgrouplist = serverutitlity.loadConfigsData(31, companyId);
			List<Type> contractCategoryTypelist = serverutitlity.loadCategoryTypeData(4, companyId);
			List<Branch> branchlist = ofy().load().type(Branch.class).filter("companyId", companyId)
					.filter("status", true).filter("buisnessUnitName IN", strbrancharray).list();

			List<Country> globalCountrylist = ofy().load().type(Country.class).filter("companyId", companyId).list();
			List<State> globalStatelist = ofy().load().type(State.class).filter("status", true)
					.filter("companyId", companyId).list();
			List<Locality> globalLocalitylist = ofy().load().type(Locality.class).filter("status", true)
					.filter("companyId", companyId).list();

//				List<Config> contractPeriodsalesSourcelist = serverutitlity.loadConfigsData(101, companyId);
			List<Config> salutationlist = serverutitlity.loadConfigsData(98, companyId);
//				List<Config> numberRangelist = serverutitlity.loadConfigsData(91, companyId);

//			    List<ConfigCategory> globalConfigprcat = ofy().load().type(ConfigCategory.class).filter("companyId", companyId).filter("internalType", 1).filter("categoryName IN", custCategory).list();
			List<Employee> salespersonlist = ofy().load().type(Employee.class).filter("companyId", companyId)
					.filter("fullname IN", arraysalesperon).list();
//				List<com.slicktechnologies.shared.common.helperlayer.Type> globalConfitype = ofy().load().type(com.slicktechnologies.shared.common.helperlayer.Type.class).filter("companyId", companyId).filter("internalType", 1).filter("typeName IN", typeList).list();
//				List<com.slicktechnologies.shared.common.helperlayer.Type> globalConfitype = ofy().load().type(com.slicktechnologies.shared.common.helperlayer.Type.class).filter("companyId", companyId).filter("internalType", 1).filter("typeName IN", typeList).list();
			List<com.slicktechnologies.shared.common.helperlayer.Type> globalConfitype = serverutitlity
					.loadCategoryTypeData(4, companyId);
			List<Config> numberRangeConfig = serverutitlity.loadConfigsData(91, companyId);

//				List<Config> numberRangeConfig=ofy().load().type(Config.class).filter("companyId", companyId).filter("name IN", numRange).list();
//				List<Config> numberRangeConfig1=ofy().load().type(Config.class).filter("companyId", companyId).list();
//				System.out.println("Customer Type value size "+globalConfigprcat.size()+" Type "+globalConfitype);
//				System.out.println("SalesPerson value size "+salesPerson1.size()+" Type "+salesPerson1);
			/***** Ends ****/
			List<TaxDetails> taxlist = ofy().load().type(TaxDetails.class).filter("taxChargeStatus", true)
					.filter("companyId", companyId).list();
			List<Config> contractPeriodlist = serverutitlity.loadConfigsData(101, companyId);
			boolean customerCellNoValidationFlag = ServerAppUtility
					.checkForProcessConfigurartionIsActiveOrNot("Customer", "DISABLECELLNUMBERVALIDATION", companyId);
			if (exceldatalist.size() % 54 != 0) { // Ashwini Patil Date:27-12-2023 added blank cell validation
				errorExList.add("File");
				errorExList.add(
						"Blank cells were found! Put NA instead of blank for the non-mandatory field, and put the correct existing value that is already in your ERP system instead of blank for the mandatory field.");
			}
			for (int i = 54; i < exceldatalist.size() - 1; i += 54) { /** Date 23-11-2020 added by Priyanka **/

				String error = "";

				System.out.println("The Excel Data at " + i + " is: " + exceldatalist.get(i));
				logger.log(Level.SEVERE, "ref no --" + exceldatalist.get(i + 23) + " coiunt -- " + rowCount);
				custdetail = new CustomerContractDetails();
				rowCount++;

				if (exceldatalist.get(i + 23).trim() != null
						&& !exceldatalist.get(i + 23).trim().equalsIgnoreCase("NA")) {
					refList.add(exceldatalist.get(i + 23));
				} else {

					error += "Please add Ref No. in" + exceldatalist.get(23).trim()
							+ " Column. It is mandatory field it should not be NA. ";
				}

				if (exceldatalist.get(i).trim().equalsIgnoreCase("yes")) {
					if (exceldatalist.get(i + 1).trim() != null
							&& !exceldatalist.get(i + 1).trim().equalsIgnoreCase("NA")) {
						custdetail.setCompanyName(exceldatalist.get(i + 1).trim().toUpperCase());
					} else {
						error += "Please add Company Name in " + exceldatalist.get(1).trim()
								+ " Column. It is mandatory field it should not be NA. ";
					}
					custdetail.isCompany(true);
				} else {
					custdetail.setCompanyName(exceldatalist.get(i + 2).trim().toUpperCase());
					custdetail.isCompany(false);
				}

				System.out.println(exceldatalist.get(i));

				if (exceldatalist.get(i + 2).trim() != null
						&& !exceldatalist.get(i + 2).trim().equalsIgnoreCase("NA")) {
					custdetail.setFullName(exceldatalist.get(i + 2).trim().toUpperCase());
				} else {
					error += "Please add Customer Name in " + exceldatalist.get(2).trim()
							+ " Column. It is mandatory field it should not be NA. ";
				}

				// Ashwini Patil Date:3-06-2022 If MAKECELLNUMBERNONMANDATORY config is true
				// then if cell number is blank it should not give error. It should add zero
				// there.
				if (!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer",
						"MAKECELLNUMBERNONMANDATORY", companyId)) {

					if (exceldatalist.get(i + 3).trim() != null
							&& !exceldatalist.get(i + 3).trim().equalsIgnoreCase("NA")) {
						if (exceldatalist.get(i + 3).trim().matches("[0-9]+")) {
							custdetail.setCellNumber(Long.parseLong(exceldatalist.get(i + 3).trim()));
						}

					} else {
						error += "Please add Cell Number in " + exceldatalist.get(3).trim()
								+ " Column. It is mandatory field it should not be NA. ";
//						break; //Ashwini Patil Date:27-12-2023 it was breaking the loop and process fails at backend without any acknowledgement
					}
				} else {

					if (exceldatalist.get(i + 3).trim() != null
							&& !exceldatalist.get(i + 3).trim().equalsIgnoreCase("NA")) {
						if (exceldatalist.get(i + 3).trim().matches("[0-9]+")) {
							custdetail.setCellNumber(Long.parseLong(exceldatalist.get(i + 3).trim()));
						}

					} else {
						custdetail.setCellNumber(0l);

					}
				}

				if (exceldatalist.get(i + 4).trim() != null
						&& !exceldatalist.get(i + 4).trim().equalsIgnoreCase("NA")) {
					if (exceldatalist.get(i + 4).trim().matches("[0-9]+")) {
						custdetail.setLandLine(Long.parseLong(exceldatalist.get(i + 4).trim()));
					}
				}

				if (exceldatalist.get(i + 5).trim() != null
						&& !exceldatalist.get(i + 5).trim().equalsIgnoreCase("NA")) {
					custdetail.setEmail(exceldatalist.get(i + 5).trim());
				}

				if (exceldatalist.get(i + 6).trim() != null
						&& !exceldatalist.get(i + 6).trim().equalsIgnoreCase("NA")) {
					if (exceldatalist.get(i + 6).trim().length() != 15) {
						error += "Invalid GST No! GST No should be 15 digit. ";
					}
					custdetail.setCustGST(exceldatalist.get(i + 6).trim());
				}

				if (exceldatalist.get(i + 7).trim() != null
						&& !exceldatalist.get(i + 7).trim().equalsIgnoreCase("NA")) {
					custdetail.getAdress().setAddrLine1(exceldatalist.get(i + 7).trim().trim());
				} else {
					error += "Please add Address Line 1 " + exceldatalist.get(7).trim()
							+ " Column. It is mandatory field it should not be NA. ";
				}

				if (exceldatalist.get(i + 8).trim() != null
						&& !exceldatalist.get(i + 8).trim().equalsIgnoreCase("NA")) {
					custdetail.getAdress().setAddrLine2(exceldatalist.get(i + 8).trim().trim());
				}

				if (exceldatalist.get(i + 9).trim() != null
						&& !exceldatalist.get(i + 9).trim().equalsIgnoreCase("NA")) {
					custdetail.getAdress().setLandmark(exceldatalist.get(i + 9).trim().trim());
				}

				if (exceldatalist.get(i + 10).trim() != null
						&& !exceldatalist.get(i + 10).trim().equalsIgnoreCase("NA")) {
					custdetail.getAdress().setLocality(exceldatalist.get(i + 10).trim().trim());

					error += serverutitlity.validateLocalityName(globalLocalitylist, exceldatalist.get(i + 10).trim(),
							exceldatalist.get(i + 12).trim().trim(), exceldatalist.get(10).trim());
				}

				if (exceldatalist.get(i + 11).trim() != null
						&& !exceldatalist.get(i + 11).trim().equalsIgnoreCase("NA")) {
					custdetail.getAdress().setPin(Integer.parseInt(exceldatalist.get(i + 11).trim()));
				}

				if (exceldatalist.get(i + 12).trim() != null
						&& !exceldatalist.get(i + 12).trim().equalsIgnoreCase("NA")) {
					custdetail.getAdress().setCity(exceldatalist.get(i + 12).trim().trim());
					createCity(globalCitylist, exceldatalist.get(i + 12).trim(), exceldatalist.get(i + 13).trim(),
							companyId);

				} else {
					error += "Please add City in " + exceldatalist.get(12).trim()
							+ " Column. It is mandatory field it should not be NA. ";
				}

				if (exceldatalist.get(i + 13).trim() != null
						&& !exceldatalist.get(i + 13).trim().equalsIgnoreCase("NA")) {
					custdetail.getAdress().setState(exceldatalist.get(i + 13).trim());
					error += serverutitlity.validateStateName(globalStatelist, exceldatalist.get(i + 13).trim(),
							exceldatalist.get(i + 14).trim(), exceldatalist.get(13).trim());

				} else {
					error += "Please add State in " + exceldatalist.get(13).trim()
							+ " Column. It is mandatory field it should not be NA. ";
				}

				if (exceldatalist.get(i + 14).trim() != null
						&& !exceldatalist.get(i + 14).trim().equalsIgnoreCase("NA")) {
					custdetail.getAdress().setCountry(exceldatalist.get(i + 14).trim().trim());
					error += serverutitlity.validateCountryName(globalCountrylist, exceldatalist.get(i + 14).trim(),
							exceldatalist.get(14).trim());

				} else {
					error += "Please add Country in " + exceldatalist.get(14).trim()
							+ " Column. It is mandatory field it should not be NA. ";
				}

				if (exceldatalist.get(i + 15).trim() != null
						&& !exceldatalist.get(i + 15).trim().equalsIgnoreCase("NA")) {
					custdetail.getSecondaryAdress().setAddrLine1(exceldatalist.get(i + 15).trim());
				}
				if (exceldatalist.get(i + 16).trim() != null
						&& !exceldatalist.get(i + 16).trim().equalsIgnoreCase("NA")) {
					custdetail.getSecondaryAdress().setAddrLine2(exceldatalist.get(i + 16).trim());
				}

				boolean secdflag = true;
				if (exceldatalist.get(i + 17).trim() != null
						&& !exceldatalist.get(i + 17).trim().equalsIgnoreCase("NA")) {
					custdetail.getSecondaryAdress().setLandmark(exceldatalist.get(i + 17).trim());
					secdflag = true;
				}

				if (exceldatalist.get(i + 18).trim() != null
						&& !exceldatalist.get(i + 18).trim().equalsIgnoreCase("NA")) {
					custdetail.getSecondaryAdress().setLocality(exceldatalist.get(i + 18).trim());
					secdflag = true;
					error += serverutitlity.validateLocalityName(globalLocalitylist, exceldatalist.get(i + 18).trim(),
							exceldatalist.get(i + 20).trim(), exceldatalist.get(18).trim());
				}

				if (exceldatalist.get(i + 19).trim() != null
						&& !exceldatalist.get(i + 19).trim().equalsIgnoreCase("NA")) {
					custdetail.getSecondaryAdress().setPin(Integer.parseInt(exceldatalist.get(i + 19).trim()));
					secdflag = true;
				}

				if (exceldatalist.get(i + 20).trim() != null
						&& !exceldatalist.get(i + 20).trim().equalsIgnoreCase("NA")) {
					custdetail.getSecondaryAdress().setCity(exceldatalist.get(i + 20).trim());
					secdflag = true;
					createCity(globalCitylist, exceldatalist.get(i + 20).trim(), exceldatalist.get(i + 20).trim(),
							companyId);

				}

				if (exceldatalist.get(i + 21).trim() != null
						&& !exceldatalist.get(i + 21).trim().equalsIgnoreCase("NA")) {
					custdetail.getSecondaryAdress().setState(exceldatalist.get(i + 21).trim());
					secdflag = true;

					error += serverutitlity.validateStateName(globalStatelist, exceldatalist.get(i + 21).trim(),
							exceldatalist.get(i + 22).trim(), exceldatalist.get(21).trim());

				}

				if (exceldatalist.get(i + 22).trim() != null
						&& !exceldatalist.get(i + 22).trim().equalsIgnoreCase("NA")) {
					custdetail.getSecondaryAdress().setCountry(exceldatalist.get(i + 22).trim());
					secdflag = true;
					error += serverutitlity.validateCountryName(globalCountrylist, exceldatalist.get(i + 22).trim(),
							exceldatalist.get(22).trim());

				}

				if ((exceldatalist.get(i + 17).trim() == null
						|| exceldatalist.get(i + 17).trim().equalsIgnoreCase("NA"))
						&& (exceldatalist.get(i + 18).trim() == null
								|| exceldatalist.get(i + 18).trim().equalsIgnoreCase("NA"))
						&& (exceldatalist.get(i + 19).trim() == null
								|| exceldatalist.get(i + 19).trim().equalsIgnoreCase("NA"))
						&& (exceldatalist.get(i + 20).trim() == null
								|| exceldatalist.get(i + 20).trim().equalsIgnoreCase("NA"))
						&& (exceldatalist.get(i + 21).trim() == null
								|| exceldatalist.get(i + 21).trim().equalsIgnoreCase("NA"))
						&& (exceldatalist.get(i + 22).trim() == null
								|| exceldatalist.get(i + 22).trim().equalsIgnoreCase("NA"))) {
					secdflag = false;
				}

				if (!secdflag) {
					custdetail.setSecondaryAdress(custdetail.getAdress());
				}

				custdetail.setRefId(exceldatalist.get(i + 23).trim());

				if (exceldatalist.get(i + 24).trim() != null
						&& !exceldatalist.get(i + 24).trim().equalsIgnoreCase("NA")) {
					try {
						date = fmt.parse(exceldatalist.get(i + 24).trim());
						logger.log(Level.SEVERE, " This contract date -- date " + date
								+ " contract Date formate problem ref id" + exceldatalist.get(i + 23).trim());
						if (date == null) {
							error += "Please check Contract Date in Contract Date Column. contract Date not able to read. Please Check Date formate"
									+ " \n";

						} else {
							custdetail.setContractDate(date);
						}
						if (!serverutitlity.isDateValid(exceldatalist.get(i + 24).trim(), "MM/dd/yyyy")) {
							error += "Invalid date in " + exceldatalist.get(24).trim() + " Column. ";
						}

					} catch (Exception e) {
						logger.log(Level.SEVERE, " This  Contract Date at ref id -- " + exceldatalist.get(i)
								+ " contact Date formate problem");
						error += "Contract Date Column Date is Invalid format. Please add Date in MM/dd/yyyy format. ";

					}

				} else {
					error += "Please add Contract Date in " + exceldatalist.get(24).trim()
							+ " Column. It is mandatory field it should not be NA. ";

				}

				if (exceldatalist.get(i + 25).trim() != null
						&& !exceldatalist.get(i + 25).trim().equalsIgnoreCase("NA")) {
					try {
						date = null;
						date = fmt.parse(exceldatalist.get(i + 25).trim());
						logger.log(Level.SEVERE, " This contract date -- date " + date
								+ " contract Date formate problem" + exceldatalist.get(i + 23).trim());
						if (date == null) {
							error += "Please check Contract Start Date in Contract Start Date Column. contract Start Date not able to read. Please Check Date formate"
									+ " \n";
						} else {
							custdetail.setContractStartDate(date);
						}

						if (!serverutitlity.isDateValid(exceldatalist.get(i + 25).trim(), "MM/dd/yyyy")) {
							error += "Invalid date in " + exceldatalist.get(25).trim() + " Column. ";
						}

					} catch (Exception e) {
						logger.log(Level.SEVERE, " This  Contract start Date at ref id -- " + exceldatalist.get(i + 23)
								+ " contact Date formate problem");
						error += "Contract Start Date Column Date is Invalid format. Please add Date in MM/dd/yyyy format. ";

					}

				} else {
					error += "Please add Contract Start Date in " + exceldatalist.get(25).trim()
							+ " Column. It is mandatory field it should not be NA. ";
				}

				if (exceldatalist.get(i + 26).trim() != null
						&& !exceldatalist.get(i + 26).trim().equalsIgnoreCase("NA")) {

					try {

						date = null;
						date = fmt.parse(exceldatalist.get(i + 26).trim());
						logger.log(Level.SEVERE, " This contract date -- date " + date
								+ " contract Date formate problem" + exceldatalist.get(i + 23).trim());
						if (date == null) {
							error += "Please check Contract End Date in Contract End Date Column. contract End Date not able to read. Please Check Date formate"
									+ " \n";

						} else {
							if (fmt.parse(exceldatalist.get(i + 26).trim()).before(custdetail.getContractStartDate())) {
								error += "Please add contract end date After contract start date.";
							} else {
								custdetail.setContractEndDate(fmt.parse(exceldatalist.get(i + 26).trim()));

							}
						}

						if (!serverutitlity.isDateValid(exceldatalist.get(i + 26).trim(), "MM/dd/yyyy")) {
							error += "Invalid date in " + exceldatalist.get(26).trim() + " Column. ";
						}

					} catch (Exception e) {
						// TODO: handle exception
						error += "Contract End Date Column Date is Invalid format. Please add Date in MM/dd/yyyy format. ";

					}

				} else {
					error += "Please add Contract End  Date in " + exceldatalist.get(26).trim()
							+ " Column. It is mandatory field it should not be NA. ";
				}

				if (exceldatalist.get(i + 27).trim() != null
						&& !exceldatalist.get(i + 27).trim().equalsIgnoreCase("NA")) {
					custdetail.setContractGroup(exceldatalist.get(i + 27).trim());
					error += serverutitlity.validateConfigsNames(contractgrouplist, exceldatalist.get(i + 27).trim(),
							exceldatalist.get(27).trim());
				}

				if (exceldatalist.get(i + 28).trim() != null
						&& !exceldatalist.get(i + 28).trim().equalsIgnoreCase("NA")) {
					custdetail.setContractCategory(exceldatalist.get(i + 28).trim());
					error += serverutitlity.validateCategoryName(contractCategorylist, exceldatalist.get(i + 28).trim(),
							exceldatalist.get(28).trim());
				}

				if (exceldatalist.get(i + 29).trim() != null
						&& !exceldatalist.get(i + 29).trim().equalsIgnoreCase("NA")) {
					custdetail.setContractType(exceldatalist.get(i + 29).trim());
					error += serverutitlity.validateCategoryTypeNames(contractCategoryTypelist,
							exceldatalist.get(i + 28), exceldatalist.get(i + 29).trim(), exceldatalist.get(29).trim());
				}

				if (exceldatalist.get(i + 30).trim() != null
						&& !exceldatalist.get(i + 30).trim().equalsIgnoreCase("NA")) {
					custdetail.setSalePerson(exceldatalist.get(i + 30).trim());
					error += serverutitlity.validateSalesPersonName(salespersonlist, exceldatalist.get(i + 30).trim(),
							exceldatalist.get(30).trim());
				} else {
					error += "Please add Sales Person Name in " + exceldatalist.get(30).trim()
							+ " Column. It is mandatory field it should not be NA. ";
				}

				if (exceldatalist.get(i + 31).trim() != null
						&& !exceldatalist.get(i + 31).trim().equalsIgnoreCase("NA")) {
					custdetail.setBranch(exceldatalist.get(i + 31).trim());

					error += serverutitlity.validateBranchName(branchlist, exceldatalist.get(i + 31).trim(),
							exceldatalist.get(31).trim());

				} else {
					error += "Please add Branch Name in " + exceldatalist.get(31).trim()
							+ " Column. It is mandatory field it should not be NA. ";
				}

				if (exceldatalist.get(i + 32).trim() != null
						&& !exceldatalist.get(i + 32).trim().equalsIgnoreCase("NA")) {
					custdetail.setProName(exceldatalist.get(i + 32).trim());
				} else {
					error += "Please add Product Name in " + exceldatalist.get(32).trim()
							+ " Column. It is mandatory field it should not be NA. ";

				}

				if (exceldatalist.get(i + 33).trim() != null
						&& !exceldatalist.get(i + 33).trim().equalsIgnoreCase("NA")) {
					if (exceldatalist.get(i + 33).trim().matches("[0-9]+")) {
						custdetail.setNoOfService(Integer.parseInt(exceldatalist.get(i + 33).trim()));
					}

				} else {
					error += "Please add No of Services in " + exceldatalist.get(33).trim()
							+ " Column. It is mandatory field it should not be NA. ";

				}

				if (exceldatalist.get(i + 34).trim() != null
						&& !exceldatalist.get(i + 34).trim().equalsIgnoreCase("NA")) {
					if (exceldatalist.get(i + 34).trim().matches("[0-9.]+")) {
						custdetail.setPrice(Double.parseDouble(exceldatalist.get(i + 34).trim()));
					}

				}
				/**
				 * added by sheetal 16-11-2021 Des :- added Tax1 , Tax2 column requirement
				 * raised by rahul sir
				 */
				if (exceldatalist.get(i + 35).trim() != null) {
					custdetail.setTax1((exceldatalist.get(i + 35).trim()));

					error += serverutitlity.validateTaxName(taxlist, exceldatalist.get(35).trim(),
							exceldatalist.get(i + 35).trim());
					logger.log(Level.SEVERE, "  Tax1 " + error);
				}

				if (exceldatalist.get(i + 36).trim() != null) {
					custdetail.setTax2((exceldatalist.get(i + 36).trim()));

					error += serverutitlity.validateTaxName(taxlist, exceldatalist.get(36).trim(),
							exceldatalist.get(i + 36).trim());
					logger.log(Level.SEVERE, "  Tax2 " + error);
				}
				/*
				 * end
				 */

				System.out.println(" Outstanding amount ;; " + exceldatalist.get(i + 37).trim());

				if (exceldatalist.get(i + 37).trim() != null
						&& !exceldatalist.get(i + 37).trim().equalsIgnoreCase("NA")) {
					if (exceldatalist.get(i + 37).trim().matches("[0-9.]+")) {
						custdetail.setOutStandingamount(Double.parseDouble(exceldatalist.get(i + 37).trim()));
					}
				}

				if (exceldatalist.get(i + 38).trim() != null
						&& !exceldatalist.get(i + 38).trim().equalsIgnoreCase("NA")) {
					if (!serverutitlity.isDateValid(exceldatalist.get(i + 38).trim(), "MM/dd/yyyy")) {
						error += "Invalid date in " + exceldatalist.get(38).trim() + " Column. ";
					}
					try {
						date = null;
						date = fmt.parse(exceldatalist.get(i + 38).trim());
						logger.log(Level.SEVERE, " This cutoff date -- date " + date + " cutoff date formate problem"
								+ exceldatalist.get(i + 23).trim());
						if (date == null) {
							error += "Please add Cutoff Date value properly at in " + exceldatalist.get(38)
									+ "cutoff date not able to read. Check cutoff date format";
						} else {
							custdetail.setCutOffDate(fmt.parse(exceldatalist.get(i + 38).trim()));
						}
					} catch (Exception e) {
						// TODO: handle exception
						error += "Cutoff Date Column Date is Invalid format. Please add Date in MM/dd/yyyy format. ";

					}

				}

				if (exceldatalist.get(i + 39).trim().equalsIgnoreCase("yes")) {
					custdetail.setRateContract(true);
				}

				if (exceldatalist.get(i + 40).trim().equalsIgnoreCase("yes")) {
					custdetail.setServiceBilling(true);
				}

				if (exceldatalist.get(i + 41).trim().equalsIgnoreCase("yes")) {
					custdetail.setConsilated(true);
				}

				if (exceldatalist.get(i + 42).trim() != null
						&& !exceldatalist.get(i + 42).trim().equalsIgnoreCase("NA")) {
					error += serverutitlity.validateConfigsNames(contractPeriodlist, exceldatalist.get(i + 42).trim(),
							exceldatalist.get(42).trim());
					custdetail.setSalesSource(exceldatalist.get(i + 42).trim());
				}

				if (exceldatalist.get(i + 43).trim() != null
						&& !exceldatalist.get(i + 43).trim().equalsIgnoreCase("NA")) {
					custdetail.setTypeOfFreq(exceldatalist.get(i + 43).trim());
				}
				if (exceldatalist.get(i + 44).trim() != null
						&& !exceldatalist.get(i + 44).trim().equalsIgnoreCase("NA")) {
					custdetail.setTypeOfTretment(exceldatalist.get(i + 44).trim());
				}

				if (exceldatalist.get(i + 45).trim() != null
						&& !exceldatalist.get(i + 45).trim().equalsIgnoreCase("NA")) {
					custdetail.setSalutation(exceldatalist.get(i + 45).trim());
					error += serverutitlity.validateConfigsNames(salutationlist, exceldatalist.get(i + 45).trim(),
							exceldatalist.get(45).trim());

				}
				if (exceldatalist.get(i + 46).trim() != null
						&& !exceldatalist.get(i + 46).trim().equalsIgnoreCase("NA")) {
					custdetail.setProModelNo(exceldatalist.get(i + 46).trim());
				}
				if (exceldatalist.get(i + 47).trim() != null
						&& !exceldatalist.get(i + 47).trim().equalsIgnoreCase("NA")) {
					custdetail.setProSerialNo(exceldatalist.get(i + 47).trim());
				}

				if (exceldatalist.get(i + 48).trim() != null
						&& !exceldatalist.get(i + 48).trim().equalsIgnoreCase("NA")) {
					if (weekDays.contains(exceldatalist.get(i + 48))) {
						custdetail.setServiceWeekDay(exceldatalist.get(i + 48).trim());
					} else {
//						errorExList.add("Input Day Not Match !!!!!!! Please Check Day Field");
						error += "Invalid Day in " + exceldatalist.get(48).trim() + " Coulmn. Please check ";
					}
				}
				if (exceldatalist.get(i + 49).trim() != null
						&& !exceldatalist.get(i + 49).trim().equalsIgnoreCase("NA")) {
					custdetail.setServiceTime(exceldatalist.get(i + 49).trim());
				}

				/***
				 * 17-03-2020 Deepak Salve added this field for Number Range - Requirement given
				 * by Rahul sir
				 ***/

				if (exceldatalist.get(i + 50).trim() != null
						&& !exceldatalist.get(i + 50).trim().equalsIgnoreCase("NA")) {
					error += serverutitlity.validateConfigsNames(numberRangeConfig, exceldatalist.get(i + 50).trim(),
							exceldatalist.get(50).trim());
					if (numberRangeConfig != null && numberRangeConfig.size() != 0) {
						custdetail.setNumberRang(
								getNumberRangeValue(numberRangeConfig, exceldatalist.get(i + 50).trim()));
					} else {
						custdetail.setNumberRang("");
					}
				}

				/*** End ***/

				/**
				 * @author Vijay Choguule Date 10-09-2020 Des :- added Service Duration column
				 *         for Life Line Services
				 */
				if (exceldatalist.get(i + 51).trim() != null
						&& !exceldatalist.get(i + 51).trim().equalsIgnoreCase("NA")) {
					if (exceldatalist.get(i + 51).trim().matches("[0-9.]+")) {
						custdetail.setServiceDuration(Double.parseDouble(exceldatalist.get(i + 51).trim()));
					}
				}
				/**
				 * ends here
				 */

				/**
				 * @author Priyanka Bhagwat Date 12-11-2020 Des :- added Correspondence Name and
				 *         Description Remark column for star cool
				 */
				if (exceldatalist.get(i + 52).trim() != null
						&& !exceldatalist.get(i + 52).trim().equalsIgnoreCase("NA")) {
					custdetail.setCorrespondenceName(exceldatalist.get(i + 52).trim().toUpperCase());
				}
				if (exceldatalist.get(i + 53).trim() != null
						&& !exceldatalist.get(i + 53).trim().equalsIgnoreCase("NA")) {
					custdetail.setDescription(exceldatalist.get(i + 53).trim().toUpperCase());
				}

				/**
				 * ends here
				 */

				error += serverutitlity.validateYesNo(exceldatalist.get(0).trim(), exceldatalist.get(i).trim());
				error += serverutitlity.validateSpecialCharComma(exceldatalist.get(1).trim(),
						exceldatalist.get(i + 1).trim());
				error += serverutitlity.validateSpecialCharComma(exceldatalist.get(2).trim(),
						exceldatalist.get(i + 2).trim());
				error += serverutitlity.validateNumericCloumn(exceldatalist.get(3).trim(),
						exceldatalist.get(i + 3).trim());
				error += serverutitlity.validateNumericCloumn(exceldatalist.get(4).trim(),
						exceldatalist.get(i + 4).trim());
				error += serverutitlity.validateEmailId(exceldatalist.get(5).trim(), exceldatalist.get(i + 5).trim());
				error += serverutitlity.validateSpecialCharComma(exceldatalist.get(6).trim(),
						exceldatalist.get(i + 6).trim());
				error += serverutitlity.validateSpecialCharComma(exceldatalist.get(7).trim(),
						exceldatalist.get(i + 7).trim());
				error += serverutitlity.validateSpecialCharComma(exceldatalist.get(8).trim(),
						exceldatalist.get(i + 8).trim());
				error += serverutitlity.validateSpecialCharComma(exceldatalist.get(9).trim(),
						exceldatalist.get(i + 9).trim());
				error += serverutitlity.validateStringCloumn(exceldatalist.get(10).trim(),
						exceldatalist.get(i + 10).trim());
				error += serverutitlity.validateNumericCloumn(exceldatalist.get(11).trim(),
						exceldatalist.get(i + 11).trim());
				error += serverutitlity.validateStringCloumn(exceldatalist.get(12).trim(),
						exceldatalist.get(i + 12).trim());
				error += serverutitlity.validateStringCloumn(exceldatalist.get(11).trim(),
						exceldatalist.get(i + 13).trim());
				error += serverutitlity.validateStringCloumn(exceldatalist.get(14).trim(),
						exceldatalist.get(i + 14).trim());
				error += serverutitlity.validateSpecialCharComma(exceldatalist.get(15).trim(),
						exceldatalist.get(i + 15).trim());
				error += serverutitlity.validateSpecialCharComma(exceldatalist.get(16).trim(),
						exceldatalist.get(i + 16).trim());
				error += serverutitlity.validateSpecialCharComma(exceldatalist.get(17).trim(),
						exceldatalist.get(i + 17).trim());
				error += serverutitlity.validateStringCloumn(exceldatalist.get(18).trim(),
						exceldatalist.get(i + 18).trim());
				error += serverutitlity.validateNumericCloumn(exceldatalist.get(19).trim(),
						exceldatalist.get(i + 19).trim());
				error += serverutitlity.validateStringCloumn(exceldatalist.get(20).trim(),
						exceldatalist.get(i + 20).trim());
				error += serverutitlity.validateStringCloumn(exceldatalist.get(21).trim(),
						exceldatalist.get(i + 21).trim());
				error += serverutitlity.validateStringCloumn(exceldatalist.get(22).trim(),
						exceldatalist.get(i + 22).trim());
				error += serverutitlity.validateNumericCloumn(exceldatalist.get(23).trim(),
						exceldatalist.get(i + 23).trim());
				error += serverutitlity.validateSpecialCharComma(exceldatalist.get(27).trim(),
						exceldatalist.get(i + 27).trim());
				error += serverutitlity.validateSpecialCharComma(exceldatalist.get(28).trim(),
						exceldatalist.get(i + 28).trim());
				error += serverutitlity.validateSpecialCharComma(exceldatalist.get(29).trim(),
						exceldatalist.get(i + 29).trim());
				error += serverutitlity.validateSpecialCharComma(exceldatalist.get(30).trim(),
						exceldatalist.get(i + 30).trim());
				error += serverutitlity.validateSpecialCharComma(exceldatalist.get(31).trim(),
						exceldatalist.get(i + 31).trim());
				/**
				 * @author Ashwini Patil Date:17-06-2022 If product name contains number then
				 *         upload process giver error but this checker is not there in service
				 *         product upload. So Nitin sir said accept productname if it contains
				 *         number. removing validateStringCloumn validation from productname.
				 *         Keeping only comma validation
				 */
				error += serverutitlity.validateSpecialCharComma(exceldatalist.get(32).trim(),
						exceldatalist.get(i + 32).trim());
				error += serverutitlity.validateNumericCloumn(exceldatalist.get(33).trim(),
						exceldatalist.get(i + 33).trim());
				error += serverutitlity.validateNumericCloumn(exceldatalist.get(34).trim(),
						exceldatalist.get(i + 34).trim());

				/* added by sheetal 16-11-2021 for adding tax1,tax2 column */
				error += serverutitlity.validateSpecialCharComma(exceldatalist.get(35).trim(),
						exceldatalist.get(i + 35).trim());
				error += serverutitlity.validateSpecialCharComma(exceldatalist.get(36).trim(),
						exceldatalist.get(i + 36).trim());

				error += serverutitlity.validateNumericCloumn(exceldatalist.get(37).trim(),
						exceldatalist.get(i + 37).trim());

				error += serverutitlity.validateYesNo(exceldatalist.get(39).trim(), exceldatalist.get(i + 39).trim());
				error += serverutitlity.validateYesNo(exceldatalist.get(40).trim(), exceldatalist.get(i + 40).trim());
				error += serverutitlity.validateYesNo(exceldatalist.get(41).trim(), exceldatalist.get(i + 41).trim());
				error += serverutitlity.validateStringCloumn(exceldatalist.get(42).trim(),
						exceldatalist.get(i + 42).trim());
				error += serverutitlity.validateStringCloumn(exceldatalist.get(43).trim(),
						exceldatalist.get(i + 43).trim());
				error += serverutitlity.validateStringCloumn(exceldatalist.get(44).trim(),
						exceldatalist.get(i + 44).trim());
				error += serverutitlity.validateStringCloumn(exceldatalist.get(45).trim(),
						exceldatalist.get(i + 45).trim());
				error += serverutitlity.validateStringCloumn(exceldatalist.get(46).trim(),
						exceldatalist.get(i + 46).trim());
				error += serverutitlity.validateStringCloumn(exceldatalist.get(48).trim(),
						exceldatalist.get(i + 48).trim());
				/**
				 * @author Anil @since 31-08-2021 Removed String value validation raised by
				 *         Rahul Tiwari for Life line Number range is defined in alpha numeric
				 */
//				error += serverutitlity.validateStringCloumn(exceldatalist.get(48).trim(), exceldatalist.get(i+48).trim());

				error += serverutitlity.validateNumericCloumn(exceldatalist.get(51).trim(),
						exceldatalist.get(i + 51).trim());
				error += serverutitlity.validateStringCloumn(exceldatalist.get(52).trim(),
						exceldatalist.get(i + 52).trim());
				error += serverutitlity.validateSpecialCharComma(exceldatalist.get(53).trim(),
						exceldatalist.get(i + 53).trim());

				if (customerCellNoValidationFlag == false) {
					error += serverutitlity.validateCellNo(exceldatalist.get(3).trim(),
							exceldatalist.get(i + 3).trim());
				}

				if (exceldatalist.get(i + 39).trim().equalsIgnoreCase("Yes")) {
					if (exceldatalist.get(i + 40).trim().equalsIgnoreCase("Yes")) {
						error += "Please add yes for Either Is Rate Contract Column Or Service Wise Billing Column. ";
					}
				}
				if (exceldatalist.get(i + 40).trim().equalsIgnoreCase("Yes")) {
					if (exceldatalist.get(i + 39).trim().equalsIgnoreCase("Yes")) {
						error += "Please add yes for Either Service Wise Billing Column  Or Is Rate Contract Column Column. ";
					}
				}

				if (!exceldatalist.get(i + 49).trim().equalsIgnoreCase("Flexible")) {
					error += serverutitlity.validateServiceTime(exceldatalist.get(49).trim(),
							exceldatalist.get(i + 49).trim());
				}

				System.out.println("error " + error);
				custList.add(custdetail);

				if (!error.equals("")) {
					errorExList.add(exceldatalist.get(i + 23).trim());
					errorExList.add(error);
				}

			}
			logger.log(Level.SEVERE, "Cust list size -- " + custList.size());

			CsvWriter.customerWithContractErrorList = errorExList;
			if (errorExList != null && errorExList.size() != 0) {
				integerlist.add("-2");
				return integerlist;
			}

			if (errorExList.size() == 0) {
				Comparator<CustomerContractDetails> compare = new Comparator<CustomerContractDetails>() {
					@Override
					public int compare(CustomerContractDetails o1, CustomerContractDetails o2) {
						if (o1.getRefId() != null && o2.getRefId() != null) {
							return o1.getRefId().compareTo(o2.getRefId());
						}
						return 0;
					}
				};
				Collections.sort(custList, compare);
				System.out.println("ref liats  -- " + refList);
				List<CustomerContractDetails> backcustList = new ArrayList<CustomerContractDetails>();
				// refList.
				backcustList.addAll(custList);

				boolean lastobj = false;

				// boolean creatflag = createCountDetails(companyId);
				CsvWriter.custStatusList.clear();

				Gson gson = new Gson();
				logger.log(Level.SEVERE, " String ;;--- " + ContractDetailsTaskQueue.jsonStr);
				Queue queue = QueueFactory.getQueue("ContractDetailscreation-queue");
				ContractDetailsTaskQueue.refStr = new ArrayList<String>();
				ContractDetailsTaskQueue.refStr.addAll(refList);

				/**
				 * @author Vijay Date :- 23-05-2023 Des :- getting null pointer for jsonStr so
				 *         created object and assigned the same to manage the issue
				 */
//				ContractDetailsTaskQueue.jsonStr = gson.toJson(custList);
				ContractDetailsTaskQueue.jsonStr = new String();
				ContractDetailsTaskQueue.jsonStr = gson.toJson(custList);
				/**
				 * ends here
				 */
				logger.log(Level.SEVERE, "Get reff list " + ContractDetailsTaskQueue.refStr);
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/ContractDetailsTaskQueue").param("user", loginPerson)
						.param("companyId", companyId + "").param("serviceStatus", serviceStatus + "")
						.param("billingStatus", billingStatus + "").param("lastObject", lastobj + ""));
				errorExList.add("1");
			} else {
				errorExList.add("-1");
				errorExList.add("Please check data of excel sheet" + " \n");
			}

		} catch (Exception e) {
			integerlist.clear();
			integerlist.addAll(errorExList);
			integerlist.add("-1");
			custList.clear();
			integerlist.add("There is some other erros in Excel Sheet" + " \n");
			e.printStackTrace();
			System.out.println(e.getMessage());
			logger.log(Level.SEVERE, e.getMessage());
		}
		integerlist.clear();
		integerlist.addAll(errorExList);
		return integerlist;

	}

	private String getNumberRangeValue(List<Config> numberRangeConfig, String numberRangeString) {
		// TODO Auto-generated method stub

		String numberRangeValue = "";

		for (Config numRange : numberRangeConfig) {
			if (numRange.getName().equals(numberRangeString)) {
				numberRangeValue = numRange.getName();
				return numberRangeValue;
			}
		}

		return null;
	}

	private String getSalesPersonMethod(String trimName, List<Employee> salesPerson1) {
		// TODO Auto-generated method stub

		String person = "";

		for (Employee salesPerson : salesPerson1) {
			if (salesPerson.getFullname().trim().equals(trimName)) {
				person = salesPerson.getFullname();
				return person;
			}
		}

		return person;

	}

	private String getCustomerType(String trimName, List<Type> globalConfitype2) {
		// TODO Auto-generated method stub

		String type = "";

		for (Type cat : globalConfitype2) {
			if (cat.getTypeName().trim().equals(trimName)) {
				type = cat.getTypeName();
				System.out.println(" Inside Method " + cat.getTypeName() + " Value " + type);
				return type;
			}
		}
		System.out.println(" Type " + type);
		return type;

	}

	private String getContractCat(String trimName, List<ConfigCategory> globalConfigprcat2) {
		// TODO Auto-generated method stub

		String category = "";

		for (ConfigCategory cat : globalConfigprcat2) {
			if (cat.getCategoryName().trim().equals(trimName)) {
				category = cat.getCategoryName();
				return category;
			}
		}

		return category;
	}

	/**
	 * ends here
	 */

	@Override
	public MINUploadExcelDetails saveMinUploadProcess(MINUploadExcelDetails MinListDetails) {
		/**
		 * ErrorDetailsList.put("ErrorList",intlist); ErrorDetailsList.put("companyId",
		 * companyid); ErrorDetailsList.put("loginId",loginPerson);
		 * ErrorDetailsList.put("ExcelList", exceldatalist);
		 */
		Long companyId = MinListDetails.getCompanyId();
		List<String> exceldataDetailslist = MinListDetails.getExceldatalist();
		MINUploadExcelDetails minDetails = new MINUploadExcelDetails();
		logger.log(Level.SEVERE, " saveMinUploadProcess method call--  " + companyId);
		try {
			sefmt.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			minList = new ArrayList<String>();
			errorList = new ArrayList<String>();
			emplcount = new HashSet<Integer>();
			wareHouse = new HashSet<String>();
			prodCount = new HashSet<Integer>();
			contCount = new HashSet<Integer>();

			CustSerList = new HashSet<Integer>();
			minList.addAll(exceldataDetailslist);

			logger.log(Level.SEVERE, " saveMinUploadProcess List " + minList.size());
//			serviceDetailList = new ArrayList<Service>();
			empInfoList = new ArrayList<Employee>();

			prodInvDetails = new ArrayList<ProductInventoryView>();

			prodDetails = new ArrayList<ItemProduct>();
			serProjectDetails = new ArrayList<ServiceProject>();
			minIssnote = new ArrayList<MaterialIssueNote>();

			productInDetails = new HashMap<String, Double>();
			productInErrorList = new HashMap<String, Double>();
			CustSerList = new HashSet<Integer>();

			minDetails = (getServiceListDetails(MinListDetails));

			/**
			 * @author Anil , Date : 20-12-2019 if error captured in validation programme
			 *         mannuly clearing bolbkey
			 */
			if (errorList.size() != 0) {
				blobkey = null;
			}

		} catch (Exception e) {
//			errorList.add("There is some error in MIN Upload Process");
			errorList.add("Upload process stoped due to " + e.getMessage());
			System.out.println(e.getMessage());
			logger.log(Level.SEVERE, "Main error in saveMinUploadProcess" + e.getMessage());
			minDetails.setErrorList(errorList);
			/**
			 * @author Anil , Date : 20-12-2019 if error captured in validation programme
			 *         mannuly clearing bolbkey
			 */
			if (errorList.size() != 0) {
				blobkey = null;
			}
			return minDetails;
		}

		for (String str : errorList) {
			logger.log(Level.SEVERE, " saveMinUploadProcess error list -- " + str);
		}
		minDetails.setErrorList(errorList);
		return minDetails;
	}

	@SuppressWarnings("unchecked")
	MINUploadExcelDetails getServiceListDetails(MINUploadExcelDetails minDetailsMapDt) {

		int rowcount = 1;
		MINUploadExcelDetails minUploadDetails = new MINUploadExcelDetails();
		try {
			Long companyId = minDetailsMapDt.getCompanyId();
			logger.log(Level.SEVERE, " getServiceListDetails method call--  " + companyId);
			serCount = new HashSet<Integer>();
			branchDtList = new HashSet<String>();
//			minDetails.clear();
			boolean prodFlag = false;
			Date date = new Date();
			ArrayList<MINUploadDetails> minDetails = new ArrayList<MINUploadDetails>();
			MINUploadDetails minUpload;
			EmployeeInfo empinfo;
			ProductGroupList proDetails = null;

			for (int i = 46; i < minList.size(); i += 46) {

				minUpload = new MINUploadDetails();

				if (!minList.get(i).trim().equalsIgnoreCase("NA")) {
					if (minList.get(i).trim().matches("[0-9]+")) {
						serCount.add(Integer.parseInt(minList.get(i).trim()));
						minUpload.setServiceCount(Integer.parseInt(minList.get(i).trim()));
					} else {
						errorList.add(" Row no -- " + rowcount + "   Service id should be numeric value.");
						continue;
					}

				} else {
					errorList.add(" Row no -- " + rowcount + "   No service id found.");
					continue;
				}

				if (!minList.get(i + 2).trim().equalsIgnoreCase("NA")) {

					if (minList.get(i + 2).trim().matches("[0-9]+")) {
						contCount.add(Integer.parseInt(minList.get(i + 2).trim()));
						minUpload.setContractCount(Integer.parseInt(minList.get(i + 2).trim()));
					} else {
						errorList.add(" Row no -- " + rowcount + "   Contract id should be numeric value.");
						continue;
					}
				} else {
					errorList.add(" Row no -- " + rowcount + " No contract id found.");
					continue;
				}

				if (!minList.get(i + 1).trim().equalsIgnoreCase("NA")) {
					minUpload.setBranch(minList.get(i + 1).trim());
					branchDtList.add(minList.get(i + 1).trim());
				} else {
					errorList.add(" Row no -- " + rowcount + " No branch details found.");
					continue;
				}
				/***
				 * Date 20-08-2019 By Vijay for 19 AUg onwards MIN Upload Restrcition to pune
				 * and Indore branch
				 ****/
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
				String dateInString = "19-08-2019 00:00:00";
				Date validationDate = sdf.parse(dateInString);
				logger.log(Level.SEVERE, "validationDate == " + validationDate);
				Date serviceComletionDate = null;
				try {
					serviceComletionDate = sefmt.parse(minList.get(i + 4).trim());
				} catch (Exception e) {
					// TODO: handle exception
				}
				if (serviceComletionDate == null) {
					errorList.add(" This service -- " + minList.get(i)
							+ " Invalid date format for service compeletion date.");
				}

				logger.log(Level.SEVERE, "serviceComletionDate == " + serviceComletionDate);

				/**
				 * date 08.08.2019 added by komal to restrict min upload for Pune and indore
				 * branch
				 **/
				if (!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialIssueNote",
						"AllowMINUploadForAllBranches", minDetailsMapDt.getCompanyId())) {
					if (minList.get(i + 1).trim().equalsIgnoreCase("Pune")
							|| minList.get(i + 1).trim().equalsIgnoreCase("Indore")
									&& (serviceComletionDate.after(validationDate)
											|| serviceComletionDate.equals(validationDate))) {
						Contract contract = ofy().load().type(Contract.class).filter("companyId", companyId)
								.filter("count", minUpload.getContractCount()).first().now();
						if (contract != null && contract.isContractRate()) {

						} else {
							errorList.add(" Row no -- " + rowcount + " This Row contains Pune/Indore Branch.");
							continue;
						}
					}
				}
				/**
				 * end komal
				 */
				if (!minList.get(i + 3).trim().equalsIgnoreCase("NA")) {
					try {
						logger.log(Level.SEVERE, " Service Date formate problem" + minList.get(i + 3).trim());
						date = sefmt.parse(minList.get(i + 3).trim());
						logger.log(Level.SEVERE, " This service -- date " + date + " Service Date formate problem"
								+ minList.get(i + 3).trim());
						if (date == null) {
							errorList.add(
									"For service id -- " + minList.get(i) + " Invalid date format for service date.");
						} else {
							Date hardCodeDate = null;
							try {
								hardCodeDate = sefmt.parse("01-01-2010");
							} catch (Exception e) {

							}
							if (hardCodeDate != null && date.after(hardCodeDate)) {
								minUpload.setServiceDate(date);
								/**
								 * Date 30-09-2020 by Vijay if service completion date before service date then
								 * it should give validation msg
								 ***/
								if (serviceComletionDate != null && serviceComletionDate.before(date)) {
									errorList.add("For service id -- " + minList.get(i)
											+ "Service Completion Date should be on or after Service Date");
								}

							} else {
								errorList.add("For service id -- " + minList.get(i)
										+ "Service date should be after year 2010");
							}

						}

					} catch (Exception e) {
						errorList.add("For service id -- " + minList.get(i) + " Invalid service date format. ");
						logger.log(Level.SEVERE,
								" This service -- " + minList.get(i) + " Service Date formate problem");
					}

				} else {
					errorList.add("For service id -- " + minList.get(i) + " No service date found.");
					logger.log(Level.SEVERE, " This service -- " + minList.get(i) + " Service Date formate problem");
				}

				if (!minList.get(i + 4).trim().equalsIgnoreCase("NA")) {
					try {
						date = sefmt.parse(minList.get(i + 4).trim());
//						minUpload.setServiceComplDate(date);
						if (date == null) {
							errorList.add("For service id -- " + minList.get(i)
									+ " Invalid date format for service completion date.");
						} else {
//							minUpload.setServiceDate(date);
							minUpload.setServiceComplDate(date);
						}
					} catch (Exception e) {
						errorList.add("For service id -- " + minList.get(i)
								+ " Invalid date format for service completion date.");
						logger.log(Level.SEVERE,
								" This service -- " + minList.get(i) + " Service complition Date formate problem");
					}
				} else {
					errorList.add("For service id -- " + minList.get(i) + "No service completion date found.");
					logger.log(Level.SEVERE,
							" This service -- " + minList.get(i) + " Service complition Date formate problem");
				}

				if (!minList.get(i + 5).trim().equalsIgnoreCase("NA")) {
					try {
						if (minList.get(i + 5).trim().matches("[0-9.]*")) {
							minUpload.setServiceComplTime(Double.parseDouble(minList.get(i + 5).trim()));
						} else {
							errorList.add(
									" Row no -- " + rowcount + " : Service completion time should be numeric value.");
							continue;
						}

					} catch (NumberFormatException e) {

						errorList.add("For service id -- " + minList.get(i)
								+ " : Service completion time should be numeric value.");
						logger.log(Level.SEVERE,
								" This service -- " + minList.get(i) + " Service complition Date formate problem");

					} catch (Exception e) {

						errorList.add("For service id -- " + minList.get(i)
								+ " : Service completion time should be numeric value.");
						logger.log(Level.SEVERE,
								" This service -- " + minList.get(i) + " Service complition Date formate problem");

					}
				} else {

					errorList.add("For service id -- " + minList.get(i) + " No service completion time found.");
					logger.log(Level.SEVERE,
							" This service -- " + minList.get(i) + " Service complition Date formate problem");
				}

				if (!minList.get(i + 6).trim().equalsIgnoreCase("NA")) {
					minUpload.setRemark(minList.get(i + 6).trim());
				}

				if (!minList.get(i + 7).trim().equalsIgnoreCase("NA")) {
					minUpload.setServiType(minList.get(i + 7).trim());
				} else {

					errorList.add("For service id -- " + minList.get(i) + " No service type is added. ");
					logger.log(Level.SEVERE, " This service -- " + minList.get(i) + " Service Type Not define");
				}

				if (!minList.get(i + 8).trim().equalsIgnoreCase("NA")) {
					try {

						/**
						 * Date 10-08-2018 By Vijay Des :- Qty must be grater than 0 validation
						 */
						if (minList.get(i + 8).trim().equals("0")) {
							errorList.add(" Row no -- " + rowcount + " Service unit should be greater than zero.");
							logger.log(Level.SEVERE,
									" Row no -- " + rowcount + " service unit should be greater than Zero.");

						}
						/**
						 * ends here
						 */
						else if (minList.get(i + 8).trim().matches("[0-9.]*")) {
							minUpload.setUnit(Double.parseDouble(minList.get(i + 8).trim()));
						} else {
							errorList.add(" Row no -- " + rowcount + " Service unit should be numeric value.");
							continue;
						}

					} catch (NumberFormatException e) {

						errorList.add("For service id -- " + minList.get(i) + " Service unit should be numeric value.");
						logger.log(Level.SEVERE,
								" This service -- " + minList.get(i) + " Service complition Date formate problem");

					} catch (Exception e) {

						errorList.add("For service id -- " + minList.get(i) + " Service unit should be numeric value.");
						logger.log(Level.SEVERE,
								" This service -- " + minList.get(i) + " Service complition Date formate problem");

					}
				} else {
					/**
					 * Date 10-08-2018 By Vijay Des :- Service unit should not be NA
					 */
					if (minList.get(i + 8).trim().equalsIgnoreCase("NA")) {
						errorList.add(" Row no -- " + rowcount + " service unit of measurement should not be NA");
						logger.log(Level.SEVERE, " Row no -- " + rowcount + " service unit should not be NA");
					}
					/**
					 * ends here
					 */
				}

				if (!minList.get(i + 9).trim().equalsIgnoreCase("NA")) {
					minUpload.setUom(minList.get(i + 9).trim());
				}
				/**
				 * Date 10-08-2018 By Vijay Des :- Qty must be grater than 0 validation
				 */
				else {
					if (minList.get(i + 9).trim().equalsIgnoreCase("NA"))
						errorList.add(" Row no -- " + rowcount + " service unit of measurement should not be NA");
					logger.log(Level.SEVERE,
							" Row no -- " + rowcount + " service unit of measurement should not be NA");
				}
				/**
				 * ends here
				 */

				if (!minList.get(i + 10).trim().equalsIgnoreCase("NA")) {
					if (minList.get(i + 10).trim().matches("[0-9]+")) {
						emplcount.add(Integer.parseInt(minList.get(i + 10).trim()));
						minUpload.setEmpCount(Integer.parseInt(minList.get(i + 10).trim()));
					} else {
						errorList.add(
								"For service id -- " + minList.get(i) + " : Employee id should be numeric value. ");
						logger.log(Level.SEVERE,
								" This service -- " + minList.get(i) + " Employee id Should contain numeric value");
					}
				} else {
					errorList.add("For service id -- " + minList.get(i) + " No employee id found.");
					logger.log(Level.SEVERE,
							" This service -- " + minList.get(i) + " Service employee user id not define.");
				}

				for (int j = 11; j <= 15; j++) {

					if (!minList.get(i + j).trim().equalsIgnoreCase("NA")) {
						if (minList.get(i + j).trim().matches("[0-9]+")) {
							empinfo = new EmployeeInfo();
							empinfo.setCount(Integer.parseInt(minList.get(i + j).trim()));
							minUpload.getEmpDetails().add(empinfo);
							emplcount.add(Integer.parseInt(minList.get(i + j).trim()));
						} else {
							errorList.add(
									"Service id -- " + minList.get(i) + " : Employee id should be numeric value. ");
							logger.log(Level.SEVERE,
									" This service -- " + minList.get(i) + " Employee id not contains");
						}

					}
				}

				prodFlag = false;
				for (int j = 16; j <= 45; j = j + 3) {
					System.out.println(" -- nu -- " + minList.get(i + j) + " raange --" + (int) (i + j)
							+ " num i+j+1 - " + (int) (i + j + 1) + " name -- " + minList.get(i + j + 1) + " qty "
							+ minList.get(i + j + 2) + " num - " + (int) (i + j + 2));
					if (!minList.get(i + j).trim().equalsIgnoreCase("NA")
							&& !minList.get(i + j + 1).trim().equalsIgnoreCase("NA")
							&& !minList.get(i + j + 2).trim().equalsIgnoreCase("NA")) {
						if (!minList.get(i + j).trim().equalsIgnoreCase("NA")) {
							if (minList.get(i + j).trim().matches("[0-9]+")) {
								prodCount.add(Integer.parseInt(minList.get(i + j).trim()));
								prodFlag = true;
							} else {
								prodFlag = false;
								errorList.add(
										" Service id :" + minList.get(i) + " : Product id should be numeric value. ");
								logger.log(Level.SEVERE,
										" This service -- " + minList.get(i) + "  Product id not contains.");
							}

						} else {
							prodFlag = false;
							errorList.add(" Service -- " + minList.get(i) + " : Product id should not be blank. ");
							logger.log(Level.SEVERE,
									" This service -- " + minList.get(i) + "  Product id not contains");
						}

						if (!minList.get(i + j + 1).trim().equalsIgnoreCase("NA")) {
							wareHouse.add(minList.get(i + j + 1).trim());
							prodFlag = true;
						} else {
							prodFlag = false;
							errorList.add(" Service -- " + minList.get(i) + " : Warehouse Name should not be blank.");
							logger.log(Level.SEVERE,
									" This service -- " + minList.get(i) + "  Warehouse not exist id not contains");
						}

						if (!minList.get(i + j + 2).trim().equalsIgnoreCase("NA")
								&& minList.get(i + j + 2).trim().matches("[0-9.]*")) {

//							wareHouse.add(minList.get(i+j+1).trim());
							try {
								double qty = Double.parseDouble(minList.get(i + j + 2).trim());
								prodFlag = true;

							} catch (NumberFormatException e) {

								prodFlag = false;
								errorList.add("For service id -- " + minList.get(i)
										+ " : Service completion time should be in minutes.");
								logger.log(Level.SEVERE, " This service -- " + minList.get(i)
										+ " Service complition Date format problem");

							} catch (Exception e) {
								prodFlag = false;
								errorList.add("For service id -- " + minList.get(i)
										+ " : Invalid service completion time format.");
								logger.log(Level.SEVERE, " This service -- " + minList.get(i)
										+ " : Service complition Date formate problem");

							}

							if (prodFlag) {
								proDetails = new ProductGroupList();
								proDetails.setProduct_id(Integer.parseInt(minList.get(j + i).trim()));
								proDetails.setQuantity(Double.parseDouble(minList.get(j + 2 + i).trim()));

								proDetails.setWarehouse(minList.get(j + i + 1).trim());
								minUpload.getMinDetails().add(proDetails);
							}
						} else {
							prodFlag = false;
							errorList.add(
									"Service id -- " + minList.get(i) + " : consumption value should not be blank.");
						}

					}
				}

				minUpload.setCompanyId(companyId);
				minDetails.add(minUpload);
				rowcount++;

			}

			logger.log(Level.SEVERE, " ser id list -- " + " Value -- " + serCount.toString());
			logger.log(Level.SEVERE,
					" wareHouse  id list -- " + wareHouse.size() + " Value -- " + wareHouse.toString());
			logger.log(Level.SEVERE,
					" emplcount  id list -- " + emplcount.size() + " Value -- " + emplcount.toString());
			logger.log(Level.SEVERE,
					" prodCount  id list -- " + prodCount.size() + " Value -- " + prodCount.toString());
			logger.log(Level.SEVERE, " minDetails  id list -- " + minDetails.size() + " Value -- " + minDetails);

			/*
			 * List<Object> containtData = new ArrayList<Object>();
			 * 
			 * containtData.add(serviceDetailList); containtData.add(minDetails);
			 * 
			 * List<MINUploadDetails> min = (List<MINUploadDetails>) containtData.get(1);
			 * 
			 * logger.log(Level.SEVERE,"get size of min -- " + min.size() + " contant -- " +
			 * min.get(0).getServiceCount());
			 */

			minUploadDetails.setMinDetails(minDetails);

//			minUploadDetails.setServiceDetailList(serviceDetailList);

			minUploadDetails.setEmpInfoList(empInfoList);

//			minDetailsMap.put(SERPROJECTS,serProject);
			minUploadDetails.setSerProject(serProjectDetails);
			minUploadDetails.setMinIssnote(minIssnote);

			minUploadDetails.setProdInvDetails(prodInvDetails);

			minUploadDetails.setMinList(minList);
			minUploadDetails.setSerCount(serCount);

			minUploadDetails.setEmplcount(emplcount);
			minUploadDetails.setWareHouse(wareHouse);
			minUploadDetails.setProdCount(prodCount);
			minUploadDetails.setContCount(contCount);
			minUploadDetails.setProdDetails(prodDetails);
			minUploadDetails.setBranchDtList(branchDetailList);
			minUploadDetails.setBranchList(branchDtList);
			minUploadDetails.setCustSerList(CustSerList);
			minUploadDetails.setCompanyId(companyId);
			minUploadDetails.setLoginName(minDetailsMapDt.getLoginName());
			logger.log(Level.SEVERE,
					"get min detail list fisrt time save -- " + minUploadDetails.getMinDetails().size());
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			logger.log(Level.SEVERE, "getServiceListDetails Method-- " + e.getMessage());
			errorList.add("Excel sheet contain some blank cell check excel sheet.");
			return minUploadDetails;
		} catch (Exception e) {

			System.out.println(e.getMessage());
			e.printStackTrace();
			logger.log(Level.SEVERE, "getServiceListDetails Method-- " + e.getMessage());
			errorList.add(
					"  No-- " + rowcount + " this row have some undefine error please check it!! " + e.getMessage());
			return minUploadDetails;
		} finally {
//			MINUploadExcelDetails.put("ErrorList",errorList);
		}
		return minUploadDetails;
	}

	@Override
	public MINUploadExcelDetails checkMinUploadServiceListDetails(MINUploadExcelDetails minDetails) {
		MINUploadExcelDetails minDetailsDt = new MINUploadExcelDetails();
		try {
			ArrayList<Service> serviceDetailList = new ArrayList<Service>();
			Long companyId = minDetails.getCompanyId();
			logger.log(Level.SEVERE,
					"checkMinUploadServiceListDetails  + min detail size --   " + minDetails.getMinDetails().size());

			logger.log(Level.SEVERE, " ser id list -- " + minDetails.getSerCount().size() + " Value -- "
					+ minDetails.getSerCount().toString());
			logger.log(Level.SEVERE, " wareHouse  id list -- " + minDetails.getWareHouse().size() + " Value -- "
					+ minDetails.getWareHouse().toString());
			logger.log(Level.SEVERE, " emplcount  id list -- " + minDetails.getEmplcount().size() + " Value -- "
					+ minDetails.getEmplcount().toString());
			logger.log(Level.SEVERE, " prodCount  id list -- " + minDetails.getProdCount().size() + " Value -- "
					+ minDetails.getProdCount().toString());
			logger.log(Level.SEVERE, " minDetails  id list -- " + minDetails.getWareHouse().size() + " Value -- "
					+ minDetails.getWareHouse().size());
			errorList = new ArrayList<String>();
			errorList.clear();
			serCount = new HashSet<Integer>();

			if (minDetails.getSerCount().size() > 0) {
				serCount.addAll(minDetails.getSerCount());
			}
			if (serCount.size() > 0) {
				serviceDetailList = new ArrayList<Service>();
				List<Integer> servlist = new ArrayList<Integer>();
				servlist.addAll(serCount);
				List<Service> serviceDetailListdemo = ofy().load().type(Service.class).filter("companyId", companyId)
						.filter("count IN", serCount).list();
				if (serviceDetailListdemo.size() > 0) {
					serviceDetailList.addAll(serviceDetailListdemo);
					minDetails.getServiceDetailList().addAll(serviceDetailListdemo);
				}

				minIssnote = new ArrayList<MaterialIssueNote>();
				List<MaterialIssueNote> minIssnotedemo = ofy().load().type(MaterialIssueNote.class)
						.filter("companyId", companyId).filter("serviceId IN", serCount).list();
				if (minIssnotedemo.size() > 0) {
					minIssnote.addAll(minIssnotedemo);
				}
				serProjectDetails = new ArrayList<ServiceProject>();
				List<ServiceProject> serProjectdemo = ofy().load().type(ServiceProject.class)
						.filter("companyId", companyId).filter("serviceId IN", serCount).list();
				if (serProjectdemo.size() > 0) {
					serProjectDetails.addAll(serProjectdemo);
				}

			}
			emplcount = new HashSet<Integer>();
			if (minDetails.getEmplcount().size() > 0) {
				emplcount.addAll(minDetails.getEmplcount());
			}
			if (emplcount.size() > 0) {
				List<Integer> emplist = new ArrayList<Integer>();
				emplist.addAll(emplcount);
				List<Employee> empInfoListdemo = ofy().load().type(Employee.class).filter("companyId", companyId)
						.filter("count IN", emplist).list();
				empInfoList.addAll(empInfoListdemo);
			}

			prodCount = new HashSet<Integer>();
			if (minDetails.getProdCount().size() > 0) {
				prodCount.addAll(minDetails.getProdCount());
			}
			prodDetails = new ArrayList<ItemProduct>();
			if (prodCount.size() > 0) {
				List<Integer> prodlist = new ArrayList<Integer>();
				prodlist.addAll(prodCount);
				List<ItemProduct> prodDetailsDemo = ofy().load().type(ItemProduct.class).filter("companyId", companyId)
						.filter("count IN", prodlist).list();
				if (prodDetailsDemo.size() > 0) {
					prodDetails.addAll(prodDetailsDemo);
				}

			}

			wareHouse = new HashSet<String>();

			if (minDetails.getWareHouse().size() > 0) {
				wareHouse.addAll(minDetails.getWareHouse());
			}
			prodInvDetails = new ArrayList<ProductInventoryView>();
			if (wareHouse.size() > 0) {
				List<String> prodlist = new ArrayList<String>();
				prodlist.addAll(wareHouse);
				List<Integer> prodlist1 = new ArrayList<Integer>();
				prodlist1.addAll(prodCount);
				List<ProductInventoryView> prodInvDetailsDemo = ofy().load().type(ProductInventoryView.class)
						.filter("companyId", companyId).filter("details.warehousename IN", prodlist)
						.filter("productinfo.prodID IN", prodlist1).list();
				if (prodInvDetailsDemo.size() > 0) {
					prodInvDetails.addAll(prodInvDetailsDemo);
				}

			}

			branchDtList = new HashSet<String>();
			if (minDetails.getBranchList().size() > 0) {
				branchDtList.addAll(minDetails.getBranchList());
			}
			if (branchDtList.size() > 0) {
				List<String> branchdt = new ArrayList<String>();
				branchdt.addAll(branchDtList);
				List<Branch> branchDtListDemo = ofy().load().type(Branch.class).filter("companyId", companyId)
						.filter("buisnessUnitName IN", branchdt).list();
				if (branchDtListDemo.size() > 0) {
					branchDetailList.addAll(branchDtListDemo);
				}

				/**
				 * @author Anil @since 11-03-2021 validating input branch name from branch
				 *         master Raised by Vaishnavi for NBHC
				 */
				try {
					boolean errorFlag = false;
					for (String branch : branchDtList) {
						if (branchDetailList != null && branchDetailList.size() != 0) {
							boolean branchErrFlag = true;
							for (Branch entity : branchDetailList) {
								if (entity.getBusinessUnitName().equals(branch)) {
									branchErrFlag = false;
									break;
								}
							}
							if (branchErrFlag) {
								errorFlag = true;
								errorList.add("Branch- " + branch + " does not exist in the system.");
							}
						} else {
							errorFlag = true;
							errorList.add("Branch- " + branch + " does not exist in the system.");
						}
					}

					if (errorFlag) {
						minDetailsDt.setErrorList(errorList);
					}
				} catch (Exception e) {

				}

			}
			logger.log(Level.SEVERE,
					" serviceDetailList id list -- " + serviceDetailList.size() + " Value -- " + serCount.size());
			logger.log(Level.SEVERE, " empInfoList  id list -- " + empInfoList.size() + " Value -- ");
			logger.log(Level.SEVERE, " prodDetails  id list -- " + prodDetails.size() + " Value -- ");
			logger.log(Level.SEVERE,
					" prodInvDetails  id list -- " + prodInvDetails.size() + " Value -- " + prodInvDetails.toString());
			logger.log(Level.SEVERE,
					" prodCount  id list -- " + prodCount.size() + " Value -- " + prodCount.toString());

			/**
			 * nidhi 26-12-2018 ||*|| for check bom available or not
			 */
			if (serviceDetailList.size() > 0) {
				ArrayList<Integer> proId = new ArrayList<Integer>();
				for (Service ser : serviceDetailList) {
					proId.add(ser.getProjectId());
				}

				List<BillOfMaterial> billList = ofy().load().type(BillOfMaterial.class).filter("companyId", companyId)
						.filter("product_id IN", proId).list();

				if (billList != null && billList.size() > 0) {
					ArrayList<BillOfMaterial> billDt = new ArrayList<BillOfMaterial>();
					billDt.addAll(billList);
					minDetails.setProdBillOfMetrialList(billDt);
				}
			}
			/**
			 * end
			 */

			int value = checkServiceDetailsExitsOrNot(minDetails);
			logger.log(Level.SEVERE, "service details --" + value);

			minDetailsDt.setMinDetails(minDetails.getMinDetails());

			minDetailsDt.setServiceDetailList(serviceDetailList);

			minDetailsDt.setEmpInfoList(empInfoList);

//		minDetailsMap.put(SERPROJECTS,serProject);
			minDetailsDt.setSerProject(serProjectDetails);
			minDetailsDt.setMinIssnote(minIssnote);

			minDetailsDt.setProdInvDetails(prodInvDetails);

			minDetailsDt.setProductInDetails(productInDetails);
			minDetailsDt.setMinList(minList);
			minDetailsDt.setSerCount(serCount);
			minDetailsDt.setProductInErrorList(productInErrorList);
			minDetailsDt.setEmpInfoList(empInfoList);
			minDetailsDt.setEmplcount(emplcount);
			minDetailsDt.setWareHouse(wareHouse);
			minDetailsDt.setProdCount(prodCount);
			minDetailsDt.setContCount(contCount);
			minDetailsDt.setProdDetails(prodDetails);
			minDetailsDt.setBranchDtList(branchDetailList);
			minDetailsDt.setBranchList(branchDtList);
			minDetailsDt.setCustSerList(CustSerList);
			minDetailsDt.setCompanyId(companyId);
			minDetailsDt.setErrorList(errorList);
			minDetailsDt.setLoginName(minDetails.getLoginName());
			/**
			 * ||*|| nidhi 26-12-2018
			 */
			minDetailsDt.setProdBillOfMetrialList(minDetails.getProdBillOfMetrialList());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			logger.log(Level.SEVERE, "Check Min upload Service Details Problem --" + e.getMessage());
//			logger.log(Level.SEVERE, e.getLocalizedMessage());
			errorList.add(" There is some problem in data fatch please check !!" + e.getMessage());
			minDetailsDt.setErrorList(errorList);
			return minDetailsDt;
		}

		for (String str : errorList) {
			logger.log(Level.SEVERE, " checkMinUploadServiceListDetails error list -- " + str);
		}

		/**
		 * @author Anil , Date : 20-12-2019 if error captured in validation programme
		 *         mannuly clearing bolbkey
		 */
		if (errorList.size() != 0) {
			blobkey = null;
		}

		return minDetailsDt;
	}

	@Override
	public MINUploadExcelDetails ServiceUploadToCompleteStatus(MINUploadExcelDetails minDetailsDt) {
		logger.log(Level.SEVERE, " GEt min list count -- " + minDetailsDt.getMinDetails().size() + "Ser id -- "
				+ minDetailsDt.getServiceDetailList().size());
		try {
			logger.log(Level.SEVERE, " GEt company  count -- " + minDetailsDt.getCompanyId());
			errorList = new ArrayList<String>();
			Long companyId = minDetailsDt.getCompanyId();
			prodInvDetails = new ArrayList<ProductInventoryView>();
			prodInvDetails = minDetailsDt.getProdInvDetails();
//			minDetails = new ArrayList<MINUploadDetails>();
//			minDetails = minDetailsDt.getMinDetails();
//			serviceDetailList = minDetailsDt.getServiceDetailList();
			empInfoList = minDetailsDt.getEmpInfoList();
			DecimalFormat df = new DecimalFormat("0.00");
			boolean getSer = false;
			int loopCount = 1;
			logger.log(Level.SEVERE, " GEt min list count -- " + minDetailsDt.getMinDetails().size());
			logger.log(Level.SEVERE, " GEt serdetails  list count -- " + minDetailsDt.getServiceDetailList().size());
			for (final MINUploadDetails min : minDetailsDt.getMinDetails()) {
				getSer = false;
				for (Service ser : minDetailsDt.getServiceDetailList()) {
					logger.log(Level.SEVERE,
							"Ser min -- " + min.getServiceCount() + "  ser count -- " + ser.getCount());
					logger.log(Level.SEVERE, "Ser getContractCount -- " + min.getContractCount()
							+ "  ser getContractCount -- " + ser.getContractCount());
					if (min.getServiceCount() == ser.getCount() && min.getContractCount() == ser.getContractCount()) {
						getSer = true;
						logger.log(Level.SEVERE, "get min pro list size  --" + min.getMinDetails().size());
						logger.log(Level.SEVERE, " get in count conditions -- min count -- " + min.getMinProCount()
								+ "ser count --" + min.getServiceCount());
						Gson gson = new Gson();
						Queue queue = QueueFactory.getQueue("MINUploadProcessTaskQueue-queue");

						List<MINUploadDetails> minList = new ArrayList<MINUploadDetails>();
						minList.add(min.myClone());

						final String minDetail = gson.toJson(min, MINUploadDetails.class);
						logger.log(Level.SEVERE, " Min  object to string -- " + minDetail);
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/MINUploadProcessTaskQueue")
								.param("delayCount", loopCount + 1 + "").param("companyId", companyId + "")
								.param("MinDetail", minDetail));
						Thread.sleep(20);
						break;
					}

				}

				if (!getSer) {
					errorList.add("Service id does not exist  or contract details not matched with service id - "
							+ min.getServiceCount());
				}
			}
			logger.log(Level.SEVERE, "inv count --" + minDetailsDt.getProductInvViewDt().size());
			logger.log(Level.SEVERE, "getNumGenList count --" + minDetailsDt.getNumGenList().size());
			logger.log(Level.SEVERE, "errorList count --" + errorList.size());
			logger.log(Level.SEVERE, "prodInvDetails count --" + prodInvDetails.size());
			if (errorList.size() == 0) {
				logger.log(Level.SEVERE, "inv count --" + minDetailsDt.getProductInvViewDt().size());
				logger.log(Level.SEVERE, "getNumGenList count --" + minDetailsDt.getNumGenList().size());
				if (minDetailsDt.getProductInvViewDt().size() > 0) {

					ofy().save().entities(minDetailsDt.getProductInvViewDt());
				}
				if (minDetailsDt.getNumGenList().size() > 0) {
					ofy().save().entities(minDetailsDt.getNumGenList());
				}

				ofy().save().entities(prodInvDetails);
			}
		} catch (Exception e) {
			errorList.add("-1");
			e.printStackTrace();
			System.out.println("Service Upload complition problem -- " + e.getMessage());
			logger.log(Level.SEVERE,
					"Service Upload complition problem -- " + e.getMessage() + " --- " + e.getLocalizedMessage());
//			minDetailsMap.put(ERRORLIST, errorList);
			minDetailsDt.setErrorList(errorList);
			return minDetailsDt;
		} finally {
			if (minDetailsDt.getNumGenList().size() > 0) {
				ofy().save().entities(minDetailsDt.getNumGenList());
			}
		}

		if (errorList.size() == 0) {
			errorList.add("1");
		}

		for (String str : errorList) {
			logger.log(Level.SEVERE, " ServiceUploadToCompleteStatus error list -- " + str);
		}
		minDetailsDt.setErrorList(errorList);
		/**
		 * @author Anil , Date : 20-12-2019 if error captured in validation programme
		 *         mannuly clearing bolbkey
		 */
		if (errorList.size() != 0) {
			blobkey = null;
		}
		return minDetailsDt;
	}

	int checkServiceDetailsExitsOrNot(MINUploadExcelDetails minDetailsMap) {
		/** date 10.09.2019 added by komal to load all branc bin **/
		Map<String, String> warehouseBinMap = new HashMap<String, String>();
		List<Storagebin> binList = ofy().load().type(Storagebin.class).filter("companyId", minDetailsMap.getCompanyId())
				.filter("isMainWareHouseBin", true).filter("status", true).list();
		if (binList != null) {
			logger.log(Level.SEVERE, "Bin list size" + binList.size());
			for (Storagebin stBin : binList) {
				warehouseBinMap.put(stBin.getWarehouseName(), stBin.getBinName());
			}
		}
		logger.log(Level.SEVERE, "Bin map size" + warehouseBinMap);
		int retvalue = 0;
		try {
			logger.log(Level.SEVERE, " checkServiceDetailsExitsOrNot -- " + minDetailsMap.getMinDetails().size());
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:MM");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			String modifiedDate = sdf.format(new Date());
			DecimalFormat df = new DecimalFormat("0.0000");
			productInDetails = new HashMap<String, Double>();
			productInErrorList = new HashMap<String, Double>();
			productInDetails.clear();
			boolean getSer = false, getSerPro = true;
			boolean errorget = false, getProInv = false, getemp = false;

//			minDetails = new ArrayList<MINUploadDetails>();
//			minDetails.addAll(minDetailsMap.getMinDetails());
			CustSerList = new HashSet<Integer>();
			if (minDetailsMap.getMinDetails().size() == 0) {
				errorList.add("Error while checking uploaded service details ");
				logger.log(Level.SEVERE, "min details - " + minDetailsMap.getMinDetails().size());
			}

			logger.log(Level.SEVERE, " serviceDetailList id list -- " + minDetailsMap.getServiceDetailList().size()
					+ " Value -- " + serCount.size());
			logger.log(Level.SEVERE, " empInfoList  id list -- " + empInfoList.size() + " Value -- ");
			logger.log(Level.SEVERE, " prodDetails  id list -- " + prodDetails.size() + " Value -- ");
			logger.log(Level.SEVERE,
					" prodInvDetails  id list -- " + prodInvDetails.size() + " Value -- " + prodInvDetails.toString());
			logger.log(Level.SEVERE,
					" prodCount  id list -- " + prodCount.size() + " Value -- " + prodCount.toString());
			logger.log(Level.SEVERE, " minDetails  id list -- " + minDetailsMap.getMinDetails().size() + " Value -- ");
			logger.log(Level.SEVERE, " serProjectDetails  id list -- " + serProjectDetails.size() + " Value -- ");
			logger.log(Level.SEVERE, " minIssnote  id list -- " + minIssnote.size() + " Value -- ");
			logger.log(Level.SEVERE, "empInfoList list --" + empInfoList.size());
			logger.log(Level.SEVERE, "branchDetailList list --" + branchDetailList.size());

			/**
			 * Date 14-11-2019
			 * 
			 * @author Vijay Chougule Des :- NBHC CCPM complaint service assessment should
			 *         be closed validation
			 */
			ArrayList<Integer> complaintServiceid = new ArrayList<Integer>();
			for (Service service : minDetailsMap.getServiceDetailList()) {
				if (service.getTicketNumber() != -1) {
					complaintServiceid.add(service.getCount());
				}
			}
			if (complaintServiceid.size() != 0) {
				logger.log(Level.SEVERE, "complaintServiceid" + complaintServiceid.size());
				List<AssesmentReport> assementreportlist = ofy().load().type(AssesmentReport.class)
						.filter("serviceId IN", complaintServiceid).filter("companyId", minDetailsMap.getCompanyId())
						.filter("status", AssesmentReport.CREATED).list();
				logger.log(Level.SEVERE, "Open assementreportlist " + assementreportlist.size());
				if (assementreportlist.size() != 0) {
					for (AssesmentReport assmentreport : assementreportlist) {
						errorList.add("Service Id -" + assmentreport.getServiceId()
								+ " This is complaint service Its assessment is in created status. Please complete the assessment");
					}
				}
			}
			/**
			 * ends here
			 */

			int serContractCount = 0;
			Service serDt;
			for (final MINUploadDetails min : minDetailsMap.getMinDetails()) {
				/**
				 * @author Anil , Date : 23-12-2019
				 */
				errorget = false;

				getSer = false;
				getProInv = false;
				getSerPro = false;
				logger.log(Level.SEVERE,
						"min ser -- " + min.getServiceCount() + "   contract -- " + min.getContractCount());
				serContractCount = 0;
				serLoop: for (Service ser : minDetailsMap.getServiceDetailList()) {
					/**
					 * @author Anil , Date : 06-04-2020 for NBHC fumigation services are not allowed
					 *         for MIN upload process
					 */
					if (min.getServiceCount() == ser.getCount() && ser.isWmsServiceFlag()
							&& ser.getPersonInfo().getFullName().equals("NBHC INTERNAL")) {
						errorList.add("Service Id -" + min.getServiceCount()
								+ " Fumigation services are excluded from MIN Upload Process.");
						errorget = true;
					}
					if (min.getServiceCount() == ser.getCount() && min.getContractCount() == ser.getContractCount()) {

						serContractCount++;
						logger.log(Level.SEVERE, " get ser count -- " + min.getServiceCount() + " dupnlicate count -- "
								+ serContractCount);
						if (serContractCount > 1) {
							errorList.add("Service Id -" + min.getServiceCount()
									+ " Duplication service id exist Please update it through manual process.");

							break serLoop;
						}

						getSer = true;
						if (ser.getStatus().equals(ser.SERVICESTATUSCOMPLETED)
								|| ser.getStatus().equals(ser.SERVICESTATUSCANCELLED)
								|| ser.getStatus().equals(ser.SERVICESTATUSCLOSED)
								|| ser.getStatus().equals(ser.SERVICESUSPENDED)) {
							errorList.add("Service No -- " + min.getServiceCount() + " : This service is in - "
									+ ser.getStatus());
							errorget = true;
//							break;
						} else {
							if (ser.isRateContractService()) {
								if (min.getUom().equalsIgnoreCase("NA")) {
									errorget = true;
									errorList.add("Service No -- " + min.getServiceCount()
											+ " : This service is for rate contract Pleasse define Unit and UOM ");
								}
							}

							if (!errorget) {
								if (min.getEmpDetails().size() > 0) {
									boolean getCompletePro = false;
									for (ServiceProject serProj : serProjectDetails) {
										if (serProj.getserviceId() == ser.getCount()
												&& serProj.getContractId() == ser.getContractCount()) {
											if (serProj.getProjectStatus().equalsIgnoreCase(serProj.COMPLETED)
													|| serProj.getProjectStatus().equalsIgnoreCase(serProj.CANCELLED)) {

												errorget = true;
												getCompletePro = true;
											}
											if (serProj.getProjectStatus().equalsIgnoreCase(serProj.CREATED)) {
												getSerPro = true;
												errorget = false;
												break;
											}
										}
									}

									if (!getSerPro && getCompletePro) {
										errorList.add("Service id -- " + min.getServiceCount()
												+ " : This service Project is already in - completed or cancled status.");
									}
								}

							}

							if (!errorget) {
								if (min.getMinDetails().size() > 0) {
									for (MaterialIssueNote minIss : minIssnote) {
										if (ser.getCount() == minIss.getServiceId()
												&& ser.getContractCount() == minIss.getMinSoId()) {
											errorList.add("Service Id - " + min.getServiceCount()
													+ " : MIN is already created against service.");
											errorget = true;
//											break;
										}
									}
								}

							}

							// empInfoList
							if (!errorget) {
								getemp = false;
								for (Employee emp : empInfoList) {

									if (emp.getCount() == min.getEmpCount()) {
										getemp = true;
										min.setAppEmpName(emp.getFullname());
										break;
									}
								}
								if (!getemp) {
									errorList.add("Service Id - " + min.getServiceCount() + " : Employee id(User's) "
											+ min.getEmpCount() + " not found.");
									errorget = true;
								}
							}

							if (!errorget) {
								getemp = false;
								if (min.getEmpDetails().size() > 0) {
									for (int i = 0; i < min.getEmpDetails().size(); i++) {
										getemp = false;
										for (Employee empinfo : empInfoList) {
											if (empinfo.getCount() == min.getEmpDetails().get(i).getCount()) {

												min.getEmpDetails().get(i).setEmpCount(empinfo.getCount());
												min.getEmpDetails().get(i).setFullName(empinfo.getFullName());
												min.getEmpDetails().get(i).setCellNumber(empinfo.getCellNumber1());
												min.getEmpDetails().get(i).setDesignation(empinfo.getDesignation());
												min.getEmpDetails().get(i).setDepartment(empinfo.getDepartMent());
												min.getEmpDetails().get(i).setEmployeeType(empinfo.getEmployeeType());
												min.getEmpDetails().get(i).setEmployeerole(empinfo.getRoleName());
												min.getEmpDetails().get(i).setBranch(empinfo.getBranchName());
												min.getEmpDetails().get(i).setCountry(empinfo.getCountry());

												getemp = true;
												break;

											}
										}

										if (!getemp && min.getEmpDetails().size() > 0) {
											/**
											 * @author Anil , Date : 02-01-2020 If techincian id is wrong or invalid
											 *         then after in alert message we were showing "Employee User Id"
											 *         which is wrong
											 */
//											errorList.add("Service No -- " + min.getServiceCount() +" This Employee user Id not correct  - " + min.getEmpCount());
											errorList.add("For service id -- " + min.getServiceCount()
													+ " The Employee id is incorrect  - "
													+ min.getEmpDetails().get(i).getCount());
											errorget = true;
										}
									}
								}

							}

							if (!errorget) {
								boolean getbranch = false;
								if (min.getBranch() != null && min.getBranch().trim().equalsIgnoreCase("")) {
									getbranch = false;
									for (Branch branch : branchDetailList) {
										if (branch.getBusinessUnitName().equalsIgnoreCase(min.getBranch())) {
											getbranch = true;
											break;
										}
									}
									if (!getbranch && min.getBranch() != null
											&& min.getBranch().trim().equalsIgnoreCase("")) {
										errorList.add("Service No -- " + min.getServiceCount()
												+ " This Branch not Exist in system  - " + min.getBranch());
										errorget = true;
									}
								}

							}
							/**
							 * nidhi ||*|| for check product list with bom product list
							 */
//							if(!errorget){
//								if(ser.getServiceProductList()!=null && ser.getServiceProductList().size()>0){
//									logger.log(Level.SEVERE,"get service pro list " + ser.getServiceProductList().size());
//									
//								}else{
//									
//									ser.setQuantity(min.getUnit());
//									ser.setUom(min.getUom());
//									
//									BillOfMaterial bom = new BillOfMaterial();
//									ServerUnitConversionUtility  serUtility = new ServerUnitConversionUtility();
//									bom = serUtility.verifyBillofMaterilProd(ser.getProduct());
//									
//									if(bom!=null && ser.getQuantity()==0){
//										errorList.add("Service id - " + min.getServiceCount() 
//												+ " BOM is Available  bt unit not define");
//									}
//									
//									if(bom!=null){
//										BillOfMaterial billProList = serUtility.getServiceUnitConvertionPoint(bom, ser);
//										
//										boolean unitFlg = serUtility.varifyUnitConversion(billProList, ser.getUom(), ser.getCompanyId());
//									
//										if(!unitFlg || ser.getUom().trim().length()==0){
//
//											errorList.add("Service id - " + min.getServiceCount() 
//													+ " BOM is Available  bt Unit conversion or Unit of mesurement not define in sheet");
//										
//										}
//									
//									}
//									
//									
//									if(ser.getQuantity()>0 && bom !=null){
//										
//										 ArrayList<ProductGroupList> serProGroupList =	serUtility.getServiceitemProList(null, null, ser, null);
//										 
//										 ArrayList<ServiceProductGroupList> listDt = new ArrayList<ServiceProductGroupList>();
//										 
//										 if(serProGroupList!=null){
//											 for(int k = 0 ; k<serProGroupList.size();k++ ){
//												 listDt.add(ser.getServiceProdDt(serProGroupList.get(k)));
//											 }
//										 }
//										
//										 ser.setServiceProductList(listDt);
//										 ofy().save().entity(ser);
//									}
//								
//								}
//								
//								boolean getprod = false;
//								ArrayList<Integer> proIdMin = new ArrayList<Integer>();
//								mainPro:
//								for(int i= 0 ; i< min.getMinDetails().size() ; i++){
//									getprod = false;
//									for(int j=0;j<(ser.getServiceProductList()==null ? 0:ser.getServiceProductList().size());j++){
//										if(ser.getServiceProductList().get(j).getProduct_id()
//												.equals(min.getMinDetails().get(i).getProduct_id())){
//											getprod = true;
//											min.getMinDetails().get(i).setPlannedQty(ser.getServiceProductList().get(j).getPlannedQty());
//											min.getMinDetails().get(i).setPlannedUnit(ser.getServiceProductList().get(j).getPlannedUnit());
//											continue mainPro;
//										}
//									}
//									if(!getprod && ser.getServiceProductList()!=null && ser.getServiceProductList().size()>0  ){
//										proIdMin.add(min.getMinDetails().get(i).getProduct_id());
//										errorget = true;
//										errorList.add("Service id - " + min.getServiceCount() 
//												+" : Product  id - "
//												+ min.getMinDetails().get(i).getProduct_id() 
//												+ " not maintain in BOM not allow to use in Min.");
//									}
//								}
//								
//							}
							/**
							 * end
							 */

							if (!errorget) {
								logger.log(Level.SEVERE, "min details -- " + min.getServiceCount() + " min inv list -- "
										+ prodInvDetails.size());
								for (int i = 0; i < min.getMinDetails().size(); i++) {
									getProInv = false;

//									for(ProductInventoryView proIn : prodInvDetails)
									for (int j = 0; j < prodInvDetails.size(); j++) {

										if (prodInvDetails.get(j).getProdID() == min.getMinDetails().get(i)
												.getProduct_id()) {
//											for(ProductInventoryViewDetails proInDetails : prodInvDetails.get(j).getDetails())
											for (int k = 0; k < prodInvDetails.get(j).getDetails().size(); k++) {
												String binName = "";
												if (warehouseBinMap != null) {
													binName = warehouseBinMap
															.get(min.getMinDetails().get(i).getWarehouse());
												}
												if (prodInvDetails.get(j).getDetails().get(k).getProdid() == min
														.getMinDetails().get(i).getProduct_id()
														&& prodInvDetails.get(j).getDetails().get(k).getWarehousename()
																.equals(min.getMinDetails().get(i).getWarehouse())
														&& prodInvDetails.get(j).getDetails().get(k).getStoragebin()
																.equals(binName)) {

													MaterialProduct material = new MaterialProduct();
													getProInv = true;
													double value = 0;
													if (productInDetails.containsKey(
															min.getMinDetails().get(i).getProduct_id() + "$"
																	+ prodInvDetails.get(j).getDetails().get(k)
																			.getWarehousename()
																	+ "$"
																	+ prodInvDetails.get(j).getDetails().get(k)
																			.getStoragebin()
																	+ "$" + prodInvDetails.get(j).getDetails().get(k)
																			.getStoragelocation())) {

														material.setMaterialProductAvailableQuantity(
																Double.parseDouble(df.format(productInDetails.get(
																		min.getMinDetails().get(i).getProduct_id() + "$"
																				+ prodInvDetails.get(j).getDetails()
																						.get(k).getWarehousename()
																				+ "$"
																				+ prodInvDetails.get(j).getDetails()
																						.get(k).getStoragebin()
																				+ "$"
																				+ prodInvDetails.get(j).getDetails()
																						.get(k)
																						.getStoragelocation()))));
														value = productInDetails.get(prodInvDetails.get(j).getDetails()
																.get(k).getProdid()
																+ "$"
																+ prodInvDetails
																		.get(j).getDetails().get(k).getWarehousename()
																+ "$"
																+ prodInvDetails
																		.get(j).getDetails().get(k).getStoragebin()
																+ "$"
																+ prodInvDetails.get(j).getDetails().get(k)
																		.getStoragelocation())
																- min.getMinDetails().get(i).getQuantity();

													} else {
														material.setMaterialProductAvailableQuantity(
																Double.parseDouble(df.format(prodInvDetails.get(j)
																		.getDetails().get(k).getAvailableqty())));
														value = prodInvDetails.get(j).getDetails().get(k)
																.getAvailableqty()
																- min.getMinDetails().get(i).getQuantity();

													}

													productInDetails.put(
															min.getMinDetails().get(i).getProduct_id() + "$"
																	+ prodInvDetails.get(j).getDetails().get(k)
																			.getWarehousename()
																	+ "$"
																	+ prodInvDetails
																			.get(j).getDetails().get(k).getStoragebin()
																	+ "$"
																	+ prodInvDetails.get(j).getDetails().get(k)
																			.getStoragelocation(),
															Double.parseDouble(df.format(value)));
													if (value < 0) {
														productInErrorList.put(
																min.getMinDetails().get(i).getProduct_id() + "$"
																		+ prodInvDetails.get(j).getDetails().get(k)
																				.getWarehousename()
																		+ "$"
																		+ prodInvDetails.get(j).getDetails().get(k)
																				.getStoragebin()
																		+ "$"
																		+ prodInvDetails.get(j).getDetails().get(k)
																				.getStoragelocation(),
																Double.parseDouble(df.format(value)));
														errorList.add("For Service id - " + min.getServiceCount()
																+ " Product id - "
																+ min.getMinDetails().get(i).getProduct_id()
																+ " Warehouse name - "
																+ min.getMinDetails().get(i).getWarehouse()
																+ " qty went to in "
																+ Double.parseDouble(df.format(value)));
														break;

													} else {
														prodInvDetails.get(j).getDetails().get(k)
																.setAvailableqty(Double.parseDouble(df.format(value)));
													}

													logger.log(Level.SEVERE,
															"get avi qty after update --" + prodInvDetails.get(j)
																	.getDetails().get(k).getAvailableqty());
													logger.log(Level.SEVERE, "get pro id after update --"
															+ prodInvDetails.get(j).getDetails().get(k).getProdid());
													material.setMaterialProductId(prodInvDetails.get(j).getProdID());
													material.setMaterialProductCode(
															prodInvDetails.get(j).getProductCode());
													material.setMaterialProductName(
															prodInvDetails.get(j).getProductName());
													material.setMaterialProductUOM(
															prodInvDetails.get(j).getProductUOM());
													material.setMaterialProductRequiredQuantity(
															min.getMinDetails().get(i).getQuantity());
//													material.setMaterialProductRemarks("This data uploded through upload program.  " + fmt.parse(modifiedDate) );
													material.setMaterialProductWarehouse(
															min.getMinDetails().get(i).getWarehouse());
													material.setMaterialProductStorageBin(
															prodInvDetails.get(j).getDetails().get(k).getStoragebin());
													material.setMaterialProductStorageLocation(prodInvDetails.get(j)
															.getDetails().get(k).getStoragelocation());
													logger.log(Level.SEVERE, "get pro id after Stock update --"
															+ material.getMaterialProductRequiredQuantity());
													/**
													 * nidhi ||*|| for material map planned qty
													 */
													material.setMaterialProductPlannedQuantity(
															min.getMinDetails().get(i).getPlannedQty());
													logger.log(Level.SEVERE, "get pro id after Stock update req --"
															+ material.getMaterialProductPlannedQuantity());

													min.getMinDetails().get(i)
															.setProduct_id(prodInvDetails.get(j).getProdID());
													min.getMinDetails().get(i)
															.setCode(prodInvDetails.get(j).getProductCode());
													min.getMinDetails().get(i)
															.setUnit(prodInvDetails.get(j).getProductUOM());
													min.getMinDetails().get(i)
															.setName(prodInvDetails.get(j).getProductName());
													min.getMinDetails().get(i).setStorageBin(
															prodInvDetails.get(j).getDetails().get(k).getStoragebin());
													min.getMinDetails().get(i).setStorageLocation(prodInvDetails.get(j)
															.getDetails().get(k).getStoragelocation());
													/**
													 * nidhi ||*|| for material map planned qty
													 */
													min.getMinDetails().get(i)
															.setPlannedQty(min.getMinDetails().get(i).getPlannedQty());
													min.getMinDetails().get(i).setPlannedUnit(
															min.getMinDetails().get(i).getPlannedUnit());

													boolean getprod = false, getprodError = false;
													mainPro: for (int ii = 0; ii < min.getMinDetails().size(); ii++) {
														getprod = false;
														for (int jj = 0; jj < (ser.getServiceProductList() == null ? 0
																: ser.getServiceProductList().size()); jj++) {
															if (ser.getServiceProductList().get(jj).getProduct_id()
																	.equals(min.getMinDetails().get(ii)
																			.getProduct_id())) {
																getprod = true;
//																	material.setmaterialproduct
																material.setMaterialProductPlannedQuantity(
																		ser.getServiceProductList().get(jj)
																				.getPlannedQty());
																min.getMinDetails().get(i)
																		.setPlannedQty(ser.getServiceProductList()
																				.get(jj).getPlannedQty());
																min.getMinDetails().get(i)
																		.setPlannedUnit(ser.getServiceProductList()
																				.get(jj).getPlannedUnit());
																ser.getServiceProductList().get(jj).setQuantity(
																		min.getMinDetails().get(i).getQuantity());
																continue mainPro;
															}
														}
//															if( !getprod && ser.getServiceProductList()!=null && ser.getServiceProductList().size()>0  ){
//																errorget = true;
//																getprodError = true;
//																errorList.add("Service id - " + min.getServiceCount() 
//																		+" : Product  id - "
//																		+ min.getMinDetails().get(ii).getProduct_id() 
//																		+ " not maintain in BOM not allow to use in Min.");
//															}
													}
													if (!getprodError) {
														ofy().save().entity(ser);
													}
													try {
														min.getMinDetails().get(i)
																.setTitle("This data uploded through upload program.  "
																		+ fmt.parse(modifiedDate));
													} catch (ParseException e) {
														// TODO Auto-generated catch block
														e.printStackTrace();
													}

													min.getMinProdDetails().add(material);
													break;
												}
											}
										}

									}
									if (!getProInv) {
										errorList.add("Service id - " + min.getServiceCount()
												+ " : Stock is not maintain for product id - "
												+ min.getMinDetails().get(i).getProduct_id() + " in "
												+ min.getMinDetails().get(i).getWarehouse() + ".");
									}
								}

							}

						}
//						break serLoop;
					}
				}

				if (!getSerPro) {
					CustSerList.add(min.getServiceCount());
				}

				if (!getSer) {
					errorList.add("Service Id not Exist in Data or Contract details not match with service ID - "
							+ min.getServiceCount());
				}
			}

		} catch (Exception e) {
			errorList.add("error in check service Details" + e);
			e.printStackTrace();
			System.out.println("check service Details method --" + e.getMessage());
			logger.log(Level.SEVERE, "check service Details method -- \n " + e.getMessage());
			logger.log(Level.SEVERE, "check service Details method -- \n " + e);
			return 0;
		}
		return retvalue;
	}

	/**
	 * nidhi commented for new contract upload proces 26-03-2018
	 * 
	 */
	/*
	 * public ArrayList<String> checkContractUploadDetails(long
	 * companyId,ArrayList<String> exceldatalist,String loginPerson){ try {
	 * 
	 * fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	 * TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	 * 
	 * logger.log(Level.SEVERE," checkContractUploadDetails method called -- ");
	 * 
	 * errorList = new ArrayList<String>(); minList = new ArrayList<String>();
	 * minList.addAll(exceldatalist); contractDetails = new
	 * ArrayList<CustomerContractDetails>();
	 * 
	 * boolean prodFlag = false; int rowCount = 1;
	 * 
	 * 
	 * empNameList = new HashSet<String>(); paymentTerms = new HashSet<String>();
	 * 
	 * customerDetail = new HashMap<Integer, String>(); productDetail = new
	 * HashMap<Integer,String>();
	 * 
	 * Date date = new Date(); CustomerContractDetails conUpload ; EmployeeInfo
	 * empinfo ; ProductGroupList proDetails; for (int i = 49; i < minList.size(); i
	 * += 49) { logger.log(Level.SEVERE,"i --> "+i +"   custcount --"+
	 * minList.get(i+3).trim()); conUpload = new CustomerContractDetails();
	 * conUpload.setCompanyId(companyId);
	 * if(!minList.get(i).trim().equalsIgnoreCase("na")){
	 * if(minList.get(i).trim().matches("[0-9]+")){
	 * conUpload.setQuotationCount(Integer.parseInt(minList.get(i).trim())); }else{
	 * errorList.add(rowCount +". Quotation id must contain numeric value."); } }
	 * 
	 * 
	 * if(!minList.get(i+1).trim().equalsIgnoreCase("na")){
	 * if(minList.get(i+1).trim().matches("[0-9]+")){
	 * conUpload.setLeadCount(Integer.parseInt(minList.get(i+1).trim())); }else{
	 * errorList.add(rowCount +".  Lead id must contain numeric value."); } }
	 * 
	 * if(!minList.get(i+2).trim().equalsIgnoreCase("na")){
	 * conUpload.setRefId(minList.get(i+2).trim()); }
	 * 
	 * if(!minList.get(i+3).trim().equalsIgnoreCase("na")){
	 * if(minList.get(i+3).trim().matches("[0-9]+")){
	 * conUpload.setCustomerCount(Integer.parseInt(minList.get(i+3).trim())); }else{
	 * errorList.add(rowCount +".  Customer id must contain numeric value."); }
	 * }else{ errorList.add(rowCount +".  Customer Id is mandatory."); }
	 * 
	 * if(!minList.get(i+4).trim().equalsIgnoreCase("na")){ //
	 * if(minList.get(i+4).trim().matches("[0-9]+")){
	 * conUpload.setFullName((minList.get(i+4).trim()));
	 * customerDetail.put(Integer.parseInt(minList.get(i+3).trim()),
	 * minList.get(i+4).trim()); // }else{ // ErrorList.add(rowCount
	 * +".  This row lead id must contain numeric value."); // } }else{
	 * errorList.add(rowCount +".  Customer name is mandatory."); }
	 * 
	 * if(!minList.get(i+5).trim().equalsIgnoreCase("NA")){ try { date =
	 * fmt.parse(minList.get(i+5).trim()); conUpload.setContractDate(date); } catch
	 * (Exception e) { errorList.add(
	 * rowCount+" Contract date should be in 'dd/mm/yyyy' format only.");
	 * logger.log(Level.SEVERE," This contract  Date formate problem"); } }else{
	 * errorList.add(rowCount+" Contract date is mandatory.");
	 * logger.log(Level.SEVERE, rowCount+" This contract Date not define "); }
	 * 
	 * if(!minList.get(i+6).trim().equalsIgnoreCase("na")){
	 * conUpload.setContractGroup(minList.get(i+6).trim()); }
	 * 
	 * if(!minList.get(i+7).trim().equalsIgnoreCase("na")){
	 * conUpload.setContractCategory(minList.get(i+7).trim()); }else{
	 * errorList.add(rowCount+"Contract category is mandatory.");
	 * logger.log(Level.SEVERE, rowCount+" This contract Category not define "); }
	 * 
	 * if(!minList.get(i+8).trim().equalsIgnoreCase("na")){
	 * conUpload.setContractType(minList.get(i+8).trim()); }
	 * 
	 * if(!minList.get(i+9).trim().equalsIgnoreCase("na")){
	 * conUpload.setBranch(minList.get(i+9).trim()); }else{
	 * errorList.add(rowCount+"Branch is mandatory."); logger.log(Level.SEVERE,
	 * rowCount+" This contract Branch not define "); }
	 * 
	 * if(!minList.get(i+10).trim().equalsIgnoreCase("na")){
	 * conUpload.setSalePerson(minList.get(i+10).trim());
	 * empNameList.add(minList.get(i+10).trim()); }else{
	 * errorList.add(rowCount+"Sales person is mandatory.");
	 * logger.log(Level.SEVERE, rowCount+" This contract Person not define "); }
	 * 
	 * if(!minList.get(i+11).trim().equalsIgnoreCase("na")){
	 * conUpload.setApproverName(minList.get(i+11).trim());
	 * empNameList.add(minList.get(i+11).trim()); }else{
	 * errorList.add(rowCount+"Approver person is mandatory.");
	 * logger.log(Level.SEVERE,
	 * rowCount+" This contract approver person not define "); }
	 * 
	 * 
	 * 
	 * if(!minList.get(i+13).trim().equalsIgnoreCase("na")){
	 * if(minList.get(i+13).trim().equalsIgnoreCase("yes")){
	 * conUpload.setRateContract(true); }
	 * 
	 * }else{ errorList.add(rowCount+"Please enter yes/no only.");
	 * logger.log(Level.SEVERE,
	 * rowCount+" This contract Is rate contract yes/no not define  "); }
	 * 
	 * if(!minList.get(i+12).trim().equalsIgnoreCase("na")){
	 * conUpload.setPaymentTerms(minList.get(i+12).trim());
	 * paymentTerms.add(minList.get(i+12).trim()); }else{
	 * errorList.add(rowCount+"Payment terms is mandatory");
	 * logger.log(Level.SEVERE,
	 * rowCount+" This contract Payment terms not define "); }
	 * 
	 * int nu = 1,proSrNo = 0; for(int j=14;j<49;j=j+7){
	 * 
	 * prodFlag = true; SalesLineItem serDetail = new SalesLineItem();
	 * 
	 * if(!minList.get(i+j).trim().equalsIgnoreCase("na") &&
	 * !minList.get(i+j+1).trim().equalsIgnoreCase("na") &&
	 * !minList.get(i+j+2).trim().equalsIgnoreCase("na") &&
	 * !minList.get(i+j+4).trim().equalsIgnoreCase("na") &&
	 * !minList.get(i+j+5).trim().equalsIgnoreCase("na") &&
	 * !minList.get(i+j+6).trim().equalsIgnoreCase("na")){
	 * 
	 * if(minList.get(i+j).trim().matches("[0-9]+") &&
	 * minList.get(i+j+2).trim().matches("[0-9.]+") &&
	 * minList.get(i+j+4).trim().matches("[0-9]+") &&
	 * minList.get(i+j+6).trim().matches("[0-9]+")){
	 * 
	 * if( (!minList.get(i+j+3).trim().equalsIgnoreCase("na") &&
	 * minList.get(i+j+3).trim().matches("[0-9.]+"))){ prodFlag = true; }else{
	 * prodFlag = false; errorList.add("Row No. : "+rowCount +",Area "+
	 * minList.get(i+j).trim()+" must contain numeric value."); }
	 * 
	 * }else{ prodFlag = false; errorList.add("Row No. : "+rowCount +",Product id "+
	 * minList.get(i+j).trim()+" must contain numeric value."); }
	 * 
	 * try { date = fmt.parse(minList.get(i+j+5).trim()); prodFlag = true;
	 * 
	 * } catch (Exception e) { prodFlag = false; errorList.add(
	 * rowCount+"Contract start date should be in 'dd/mm/yyyy' format only");
	 * logger.log(Level.SEVERE," This contract  Date formate problem"); }
	 * 
	 * if(prodFlag){
	 * 
	 * SalesLineItem item = new SalesLineItem(); ServiceProduct serPro = new
	 * ServiceProduct(); serPro.setCount(Integer.parseInt(minList.get(i+j).trim()));
	 * item.setPrduct(serPro); item.setProductName(minList.get(i+j+1).trim());
	 * item.setPrice(Double.parseDouble(minList.get(i+j+2).trim())); //
	 * item.setArea(minList.get(i+j+3).trim())); logger.log(Level.SEVERE,
	 * "Duration -- "+ date + " no of service "+ minList.get(i+j+4).trim());
	 * 
	 * System.out.println("Duration -- "+ minList.get(i+j+6).trim() +
	 * " no of service "+ minList.get(i+j+4).trim());
	 * 
	 * item.setProductSrNo(++proSrNo); System.out.println("SEr sr no "
	 * +item.getProductSrNo()+" proSrNo " + proSrNo);
	 * item.setDuration(Integer.parseInt(minList.get(i+j+6).trim()));
	 * 
	 * 
	 * item.setNumberOfService(Integer.parseInt(minList.get(i+j+4).trim()));
	 * item.setStartDate(date);
	 * 
	 * Calendar cal = Calendar.getInstance(); cal.setTime(date);
	 * cal.add(Calendar.DATE, Integer.parseInt(minList.get(i+j+6).trim())-1);
	 * 
	 * Date productEndDate = cal.getTime(); Date prodenddate=new
	 * Date(productEndDate.getTime()); productEndDate=prodenddate;
	 * 
	 * item.setArea(minList.get(i+j+3)+""); item.setEndDate(productEndDate);
	 * item.setQuantity(1.0); item.setVatTax(new Tax()); item.setServiceTax(new
	 * Tax());
	 * item.setBranchSchedulingInfo("Service Address#Yes#"+conUpload.getBranch()+
	 * "#$"); conUpload.getServiceProduct().add(item);
	 * productDetail.put(Integer.parseInt(minList.get(i+j).trim()),minList.get(i+j+1
	 * ).trim()); } }
	 * 
	 * }
	 * 
	 * contractDetails.add(conUpload); rowCount++; }
	 * logger.log(Level.SEVERE,"contractDetails list size -- "+
	 * contractDetails.size()); } catch (Exception e) { e.printStackTrace();
	 * System.out.println("checkContractUploadDetails -- "+e.getLocalizedMessage());
	 * logger.log(Level.SEVERE,"checkContractUploadDetails -- " + e.getMessage()); }
	 * for(String str : errorList){ logger.log(Level.SEVERE,"error list -- "+ str);
	 * } return errorList; }
	 * 
	 * @Override public ArrayList<String> CheckUploadContractDetail(long companyId){
	 * 
	 * try { for(String str : errorList){
	 * logger.log(Level.SEVERE," CheckUploadContractDetail error list -- "+ str); }
	 * errorList.clear(); serProDetails = new ArrayList<ServiceProduct>();
	 * if(customerDetail.size()>0){
	 * 
	 * Set<Integer> custIdKeySet = customerDetail.keySet();
	 * 
	 * ArrayList<Integer> custIdList = new ArrayList<Integer>();
	 * 
	 * custIdList.addAll(custIdKeySet);
	 * 
	 * custDetails = ofy().load().type(Customer.class).filter("companyId",
	 * companyId).filter("count IN", custIdList).list();
	 * 
	 * }
	 * 
	 * if(productDetail.size()>0){ Set<Integer> product = productDetail.keySet();
	 * 
	 * ArrayList<Integer> pro = new ArrayList<Integer>();
	 * 
	 * pro.addAll(product);
	 * 
	 * serProDetails = ofy().load().type(ServiceProduct.class).filter("companyId",
	 * companyId).filter("count IN", pro).list(); }
	 * 
	 * if(empNameList.size()>0){
	 * 
	 * ArrayList<String> emp = new ArrayList<String>();
	 * 
	 * emp.addAll(empNameList);
	 * 
	 * empDetails = ofy().load().type(Employee.class).filter("companyId",
	 * companyId).filter("fullname IN", emp).list();
	 * 
	 * }
	 * 
	 * if(paymentTerms.size()>0){ ArrayList<String> terms = new ArrayList<String>();
	 * 
	 * terms.addAll(empNameList);
	 * 
	 * payTerms = ofy().load().type(ConfigCategory.class).filter("companyId",
	 * companyId).filter("internalType", 25).list(); }
	 * 
	 * logger.log(Level.SEVERE," Customer count -- "+ custDetails.size()
	 * +"  pro count - " + serProDetails.size() + " emp count- " + empDetails.size()
	 * +"  payment terms -- " + payTerms.size());
	 * 
	 * 
	 * checkContractDetailsValidation();
	 * 
	 * } catch (Exception e) { e.printStackTrace();
	 * logger.log(Level.SEVERE,"CheckUploadContractDetail -- " + e.getMessage()); }
	 * for(String str : errorList){
	 * logger.log(Level.SEVERE," CheckUploadContractDetail error list out -- "+
	 * str); } return errorList; }
	 * 
	 * 
	 * void checkContractDetailsValidation(){ try { boolean getCust = false, getPay
	 * = false; boolean getSerPro = false; for(Integer cust :
	 * customerDetail.keySet()){ getCust = false; for(Customer custDt :
	 * custDetails){ if(custDt.getCount() == cust ){ getCust = true; break; } }
	 * 
	 * if(!getCust){ errorList.add("Customer Id -" + cust + " name - " +
	 * customerDetail.get(cust) + " does not exist in database."); } }
	 * 
	 * 
	 * for(String payment : paymentTerms){ getPay = false; for(ConfigCategory
	 * catconfi : payTerms){ if(catconfi.getCategoryName().equals(payment) &&
	 * !catconfi.getDescription().equals("")){ getPay = true; break; } }
	 * 
	 * if(!getPay){ errorList.add("Payment terms or duration is not define -- " +
	 * payment); } }
	 * 
	 * for(Integer proID : productDetail.keySet()){ getSerPro = false;
	 * for(ServiceProduct service : serProDetails){
	 * if(service.getProductName().equals(productDetail.get(proID)) && proID ==
	 * service.getCount()){ getSerPro = true; break; } } if(!getSerPro){
	 * errorList.add("Product id - " +proID + " Product Name -- " +
	 * productDetail.get(proID) + " does not exist in database."); } }
	 * 
	 * } catch (Exception e) { e.printStackTrace();
	 * System.out.println("checkContractDetailsValidation -- " + e.getMessage());
	 * logger.log(Level.SEVERE," check Contract Details Validation Error -- " +
	 * e.getMessage()); errorList.add(" check Contract Details Validation Error -- "
	 * + e.getMessage()); }finally{ logger.log(Level.
	 * SEVERE," check Contract Details Validation Error excution done -- "); } }
	 * 
	 * @Override public ArrayList<String> UploadContractDetails(long companyId) {
	 * try { logger.log(Level.SEVERE,"contractDetails size --   "
	 * +contractDetails.size()); Customer custDt ; List<ServiceProduct> serPro = new
	 * ArrayList<ServiceProduct>(); ConfigCategory catCon ; String
	 * jsonCust="",jsonSerPro="",jsonPay="",jsonCustDetails="";
	 * 
	 * int contRefCount =0, serRefCount=0, billRefCount=0;
	 * 
	 * ArrayList<String> processNameList = new ArrayList<String>();
	 * processNameList.add("Service"); processNameList.add("Contract");
	 * processNameList.add("BillingDocument");
	 * 
	 * List<NumberGeneration> numGenList =
	 * ofy().load().type(NumberGeneration.class).filter("companyId",
	 * companyId).filter("processName IN", processNameList).filter("status",
	 * true).list();
	 * 
	 * for(NumberGeneration num : numGenList){
	 * if(num.getProcessName().equals("Service")){ serRefCount =
	 * num.getNumber().intValue(); } if(num.getProcessName().equals("Contract")){
	 * contRefCount = num.getNumber().intValue(); }
	 * if(num.getProcessName().equals("BillingDocument")){ billRefCount =
	 * num.getNumber().intValue(); } }
	 * 
	 * logger.log(Level.SEVERE,"Contract count -- "+contRefCount + " server -- "+
	 * serRefCount + "  billing --- "+ billRefCount); if(contRefCount>0 &&
	 * serRefCount>0 && billRefCount>0){ // for(CustomerContractDetails custCont :
	 * contractDetails){ for(int i = 0 ; i<contractDetails.size();i++){ Gson gson =
	 * new Gson(); logger.log(Level.SEVERE,"Cost details cust id -- " +
	 * contractDetails.get(i).getCustomerCount()); int totalSer = 0; Date StartDate
	 * = contractDetails.get(i).getContractDate(); // for(SalesLineItem proID :
	 * custCont.getServiceProduct()){ for(int j=0 ;
	 * j<contractDetails.get(i).getServiceProduct().size(); j++){ for(ServiceProduct
	 * service : serProDetails){
	 * if(contractDetails.get(i).getServiceProduct().get(j).getPrduct().getCount()
	 * == service.getCount()){ // jsonSerPro = gson.toJson(service,
	 * ServiceProduct.class); serPro.add(service); ServiceProduct serPro1 = new
	 * ServiceProduct(); serPro1.copyOfObject(service);
	 * 
	 * serPro1.setPrice(contractDetails.get(i).getServiceProduct().get(j).getPrduct(
	 * ).getPrice());
	 * serPro1.setDuration(contractDetails.get(i).getServiceProduct().get(j).
	 * getDuration());
	 * serPro1.setNumberOfService(contractDetails.get(i).getServiceProduct().get(j).
	 * getNumberOfServices());
	 * 
	 * 
	 * serPro1.setServiceTax(new Tax()); serPro1.setVatTax(new Tax());
	 * 
	 * 
	 * contractDetails.get(i).getServiceProduct().get(j).setProductCode(serPro1.
	 * getProductCode());
	 * 
	 *//**
		 * nidhi need to write rate contract condition
		 */

	/*
	 * totalSer = totalSer +
	 * contractDetails.get(i).getServiceProduct().get(j).getNumberOfServices();
	 * contractDetails.get(i).getServiceProduct().get(j).setNumberOfService(
	 * contractDetails.get(i).getServiceProduct().get(j).getNumberOfServices());
	 * contractDetails.get(i).getServiceProduct().get(j).setPrduct(serPro1);
	 * 
	 * if(contractDetails.get(i).getServiceProduct().get(j).getArea().equals("0")){
	 * contractDetails.get(i).getServiceProduct().get(j).setArea("NA"); }
	 * 
	 * if(contractDetails.get(i).getServiceProduct().get(j).getStartDate().before(
	 * StartDate)){ StartDate =
	 * contractDetails.get(i).getServiceProduct().get(j).getStartDate(); }
	 * 
	 * } } }
	 * 
	 * ConfigCategory mainCatconfi = null; for(ConfigCategory catconfi : payTerms){
	 * if(catconfi.getCategoryName().equals(contractDetails.get(i).getPaymentTerms()
	 * )){ jsonPay = new String(); jsonPay = gson.toJson(catconfi,
	 * ConfigCategory.class); mainCatconfi = catconfi; break; } }
	 * 
	 * 
	 * int contractRefCount = contRefCount; int serviceRefCount = serRefCount;
	 * 
	 * 
	 * int totalBill =
	 * reactOnAddPayTerms(contractDetails.get(i).getServiceProduct(),contractDetails
	 * .get(i).getPaymentTerms(),mainCatconfi,StartDate); int billinfRefCount =
	 * billRefCount;
	 * 
	 * contractDetails.get(i).setContractCount(contractRefCount);
	 * contractDetails.get(i).setServiceCount(serviceRefCount);
	 * contractDetails.get(i).setBillingCount(billinfRefCount);
	 * 
	 *//**
		 *
		 * jsonCustDetails = new String(); logger.log(Level.SEVERE," cust con details --
		 * " + custCont.getCustomerCount() + " cum name -- " + custCont.getRefId());
		 * jsonCustDetails = gson.toJson(custCont, CustomerContractDetails.class); Queue
		 * queue = QueueFactory.getQueue("CustomerContractUploadTaskQueue-queue");
		 * 
		 * queue.add(TaskOptions.Builder.withUrl("/slick_erp/CustomerContractUploadTaskQueue")
		 * .param("companyId", companyId+"").param("customerDetail", jsonCust)
		 * .param("confiCat", jsonPay) .param("CustContract",
		 * jsonCustDetails).param("contractRefCount", contractRefCount+"")
		 * .param("serviceRefCount", serviceRefCount+"").param("billinfRefCount",
		 * billinfRefCount+""));
		 *//*
			 * serRefCount= serRefCount+totalSer; billRefCount = billRefCount+totalBill;
			 * ++contRefCount; // logger.log(Level.SEVERE,"Countract Count -- " +
			 * contRefCount); // logger.log(Level.SEVERE,"serRefCount Count -- " +
			 * serRefCount); // logger.log(Level.SEVERE,"billRefCount Count -- " +
			 * billRefCount);
			 * 
			 * }
			 * 
			 * }else{ errorList.add("There is some problem in number generation."); }
			 * 
			 * for(int i = 0 ; i <numGenList.size() ; i++){
			 * if(numGenList.get(i).getProcessName().equals("Service")){
			 * numGenList.get(i).setNumber(serRefCount); //
			 * logger.log(Level.SEVERE,"ser Countract Count -- " + serRefCount); }
			 * if(numGenList.get(i).getProcessName().equals("Contract")){
			 * numGenList.get(i).setNumber(contRefCount); //
			 * logger.log(Level.SEVERE,"cont Countract Count -- " + contRefCount); }
			 * if(numGenList.get(i).getProcessName().equals("BillingDocument")){
			 * numGenList.get(i).setNumber(billRefCount); //
			 * logger.log(Level.SEVERE,"cont billRefCount Count -- " + billRefCount); } }
			 * if(contRefCount>0 && serRefCount>0 && billRefCount>0){
			 * ofy().save().entities(numGenList); }else{
			 * errorList.add("There is some problem in number generation."); }
			 * 
			 * 
			 * 
			 * } catch (Exception e) { errorList.clear(); e.printStackTrace();
			 * System.out.println("Upload Contract Details" + e.getMessage());
			 * logger.log(Level.SEVERE,"Upload Contract Details" + e.getMessage() );
			 * errorList.add("Upload Contract Details" +e.getMessage()); }
			 * 
			 * for(String str : errorList){
			 * logger.log(Level.SEVERE," UploadContractDetails error list -- "+ str); }
			 * return errorList; }
			 * 
			 * 
			 * private int reactOnAddPayTerms(List<SalesLineItem> serProduct, String
			 * paymentTerms, ConfigCategory confi, Date startDate) { int noOfPayTerms = 1;
			 * ConfigCategory conCategory = confi; if (conCategory != null) { int days =
			 * Integer.parseInt(conCategory.getDescription().trim());
			 * System.out.println("Payment terms Days::::::::::::::" + days); int duration =
			 * getDurationFromProductTable(serProduct);
			 * System.out.println("Duration::::::::: " + duration); noOfPayTerms = duration
			 * / days; System.out.println("No. of payment terms::::::::: "+ noOfPayTerms);
			 * if (noOfPayTerms == 0) { noOfPayTerms = 1; } } return noOfPayTerms; }
			 * 
			 * private int getDurationFromProductTable(List<SalesLineItem> serProduct) { int
			 * maxDur = 0; int currDur = 0; for (int i = 0; i < serProduct.size(); i++) {
			 * currDur = serProduct.get(i).getDuration(); System.out.println(" curruent dur"
			 * + currDur); if (maxDur <= currDur) { maxDur = currDur; } }
			 * 
			 * return maxDur; }
			 * 
			 * @Override public ArrayList<String> finallyUploadContractDetails(long
			 * companyId) { try { Customer custDt ; List<ServiceProduct> serPro = new
			 * ArrayList<ServiceProduct>(); String
			 * jsonCust="",jsonPay="",jsonCustDetails=""; int loopCount = 1;
			 * errorList.clear(); if(errorList.size()==0){ for(CustomerContractDetails
			 * custCont : contractDetails){ Gson gson = new Gson();
			 * logger.log(Level.SEVERE,"Cost details cust id -- " +
			 * custCont.getCustomerCount()); for(Customer cust: custDetails){
			 * if(cust.getCount() == custCont.getCustomerCount()){ custDt = new Customer();
			 * custDt = cust; jsonCust = new String();
			 * logger.log(Level.SEVERE,"queue Customer name -- "+ cust.getFullname());
			 * jsonCust = gson.toJson(cust, Customer.class); break; }
			 * 
			 * }
			 * 
			 * for(ConfigCategory catconfi : payTerms){
			 * if(catconfi.getCategoryName().equals(custCont.getPaymentTerms())){ jsonPay =
			 * new String(); jsonPay = gson.toJson(catconfi, ConfigCategory.class); break; }
			 * }
			 * 
			 * 
			 * jsonCustDetails = new String();
			 * logger.log(Level.SEVERE," cust con details -- " + custCont.getCustomerCount()
			 * + " cum name -- " + custCont.getRefId()); jsonCustDetails =
			 * gson.toJson(custCont, CustomerContractDetails.class); Queue queue =
			 * QueueFactory.getQueue("CustomerContractUploadTaskQueue-queue");
			 * queue.add(TaskOptions.Builder.withUrl(
			 * "/slick_erp/CustomerContractUploadTaskQueue") .param("companyId",
			 * companyId+"").param("customerDetail", jsonCust) .param("confiCat",
			 * jsonPay).param("CustContract",
			 * jsonCustDetails).param("delayCount",loopCount+1+""));
			 * 
			 * logger.log(Level.SEVERE,"Countract Count -- " + custCont.getContractCount());
			 * logger.log(Level.SEVERE,"serRefCount Count -- " +
			 * custCont.getServiceCount()); logger.log(Level.SEVERE,"billRefCount Count -- "
			 * + custCont.getBillingCount()); loopCount++; Thread.sleep(3500); }
			 * errorList.add("1"); }else{
			 * errorList.add("There is some problem to start data upload process"); }
			 * 
			 * } catch (Exception e) {
			 * logger.log(Level.SEVERE,"Exception occured in final process. "+e);
			 * e.printStackTrace();
			 * System.out.println("Exception occured in final process. "+e); }
			 * 
			 * return errorList; }
			 */

	@Override
	public MINUploadExcelDetails updateProductInventoryViewList(MINUploadExcelDetails minDetailsMap) {
		try {
			errorList = new ArrayList<String>();
			errorList.clear();

			prodInvDetails = new ArrayList<ProductInventoryView>();
			productInDetails = new HashMap<String, Double>();
			productInErrorList = new HashMap<String, Double>();

			Long companyId = minDetailsMap.getCompanyId();
			prodInvDetails.addAll(minDetailsMap.getProdInvDetails());
			productInDetails.putAll(minDetailsMap.getProductInDetails());
			productInErrorList.putAll(minDetailsMap.getProductInErrorList());
//			minDetails = new ArrayList<MINUploadDetails>();
//			minDetails.addAll(minDetailsMap.getMinDetails());
			/*
			 * serviceDetailList = new ArrayList<Service>();
			 * serviceDetailList.addAll(minDetailsMap.getServiceDetailList());
			 */
			if (minDetailsMap.getMinDetails().size() < 1) {
				errorList.add("Error While Min Uploading Process");
				minDetailsMap.setErrorList(errorList);
				return minDetailsMap;
			}
			logger.log(Level.SEVERE, "pro inv size -- " + prodInvDetails.size());
			HashSet<String> warehouse = new HashSet<String>();
			HashSet<Integer> proId = new HashSet<Integer>();
			HashSet<String> storageBin = new HashSet<String>();
			HashSet<String> storageLocation = new HashSet<String>();

			List<String> Prodetail = new ArrayList<String>();

			for (String str : productInDetails.keySet()) {
				Prodetail = Arrays.asList(str.split("[$]"));
				logger.log(Level.SEVERE, "get here prod ware house list --" + str.split("[$]").toString() + Prodetail);
				proId.add(Integer.parseInt(Prodetail.get(0).trim()));
				warehouse.add(Prodetail.get(1).trim());
				storageBin.add(Prodetail.get(2).trim());
				storageLocation.add(Prodetail.get(3).trim());
			}

			List<ProductInventoryViewDetails> prodInvDtls = ofy().load().type(ProductInventoryViewDetails.class)
					.filter("companyId", companyId).filter("prodInvViewCount IN", proId).list();

			logger.log(Level.SEVERE, " Pro Inventory view details -- " + prodInvDtls.size());

			int count = 0;

			logger.log(Level.SEVERE, " prod inv -- " + productInDetails.keySet().toString());
			for (int i = 0; i < prodInvDtls.size(); i++) {
				logger.log(Level.SEVERE, "prod ware --" + prodInvDtls.get(i).getProdid() + "$"
						+ prodInvDtls.get(i).getWarehousename().trim() + "$" + prodInvDtls.get(i).getStoragebin().trim()
						+ "$" + prodInvDtls.get(i).getStoragelocation().trim());
				if (productInDetails.containsKey(prodInvDtls.get(i).getProdid() + "$"
						+ prodInvDtls.get(i).getWarehousename().trim() + "$" + prodInvDtls.get(i).getStoragebin().trim()
						+ "$" + prodInvDtls.get(i).getStoragelocation().trim())) {
					prodInvDtls.get(i)
							.setAvailableqty(productInDetails.get(
									prodInvDtls.get(i).getProdid() + "$" + prodInvDtls.get(i).getWarehousename().trim()
											+ "$" + prodInvDtls.get(i).getStoragebin().trim() + "$"
											+ prodInvDtls.get(i).getStoragelocation().trim()));
					logger.log(Level.SEVERE, " pro id -- " + prodInvDtls.get(i).getAvailableqty());
					if (prodInvDtls.get(i).getAvailableqty() <= -1) {
						errorList.add("Product warehouse avaliable qty moving upto "
								+ prodInvDtls.get(i).getAvailableqty() + " status. This verify available stock. ");
					}
					count++;
				}

			}

			logger.log(Level.SEVERE, "get count - " + count + " pri map -- " + productInDetails.size());
			if (errorList.size() == 0 && count == productInDetails.size()) {
//					ofy().save().entities(prodInvDtls);
				ArrayList<ProductInventoryViewDetails> prodInvDtlsDemo = new ArrayList<ProductInventoryViewDetails>();
				prodInvDtlsDemo.addAll(prodInvDtls);
				minDetailsMap.setProductInvViewDt(new ArrayList<ProductInventoryViewDetails>(prodInvDtlsDemo));

				ArrayList<String> processNameList = new ArrayList<String>();
				processNameList.add("MaterialIssueNote");
				processNameList.add("ProductInventoryTransaction");
				processNameList.add("ServiceProject");

				List<NumberGeneration> numGenList = ofy().load().type(NumberGeneration.class)
						.filter("companyId", companyId).filter("processName IN", processNameList).filter("status", true)
						.list();

				int serPro = 0, mincount = 0, serIncTranCount = 0;

				for (NumberGeneration num : numGenList) {
					if (num.getProcessName().equals("ProductInventoryTransaction")) {
						serIncTranCount = num.getNumber().intValue();
					}
					if (num.getProcessName().equals("MaterialIssueNote")) {
						mincount = num.getNumber().intValue();
					}
					if (num.getProcessName().equals("ServiceProject")) {
						serPro = num.getNumber().intValue();
					}
				}

				for (int i = 0; i < minDetailsMap.getMinDetails().size(); i++) {
//						for(Service ser :  serviceDetailList){

//							if(minDetails.get(i).getServiceCount() == ser.getCount() && minDetails.get(i).getContractCount() == ser.getContractCount()){
					logger.log(Level.SEVERE, "  ");
					if (minDetailsMap.getMinDetails().get(i).getEmpDetails().size() > 0) {
						minDetailsMap.getMinDetails().get(i).setCustomerProCount(serPro);
						serPro++;
					}

					if (minDetailsMap.getMinDetails().get(i).getMinDetails().size() > 0) {

						Integer minC = new Integer(mincount);
						Integer mintran = new Integer(serIncTranCount);

						minDetailsMap.getMinDetails().get(i).setMinTrCount(mintran.intValue());
						minDetailsMap.getMinDetails().get(i).setMinProCount(minC.intValue());
						mincount++;
						logger.log(Level.SEVERE,
								"get min count in set loop--" + minDetailsMap.getMinDetails().get(i).getMinProCount()
										+ "  ser id --" + minDetailsMap.getMinDetails().get(i).getServiceCount()
										+ " contract  id -- "
										+ minDetailsMap.getMinDetails().get(i).getContractCount());
//									mincount =mincount +minDetails.get(i).getMinDetails().size();
						serIncTranCount = serIncTranCount + minDetailsMap.getMinDetails().get(i).getMinDetails().size();
					}
					Thread.sleep(100);
					continue;
//							}
//						}
				}
				processNameList.add("MaterialIssueNote");
				processNameList.add("ProductInventoryTransaction");
				processNameList.add("ServiceProject");
				for (int i = 0; i < numGenList.size(); i++) {
					if (numGenList.get(i).getProcessName().equals("MaterialIssueNote")) {
						numGenList.get(i).setNumber(mincount);
						logger.log(Level.SEVERE, "ser mincount Count -- " + mincount);
					}
					if (numGenList.get(i).getProcessName().equals("ProductInventoryTransaction")) {
						numGenList.get(i).setNumber(serIncTranCount);
						logger.log(Level.SEVERE, "cont ProductInventoryTransaction -- " + serIncTranCount);
					}
					if (numGenList.get(i).getProcessName().equals("ServiceProject")) {
						numGenList.get(i).setNumber(serPro);
						logger.log(Level.SEVERE, "cont ProductInventoryTransaction -- " + serIncTranCount);
					}
				}
				if (serIncTranCount > 0 && mincount > 0) {
//						ofy().save().entities(numGenList);
					ArrayList<NumberGeneration> numGenListDemo = new ArrayList<NumberGeneration>();
					numGenListDemo.addAll(numGenList);
					minDetailsMap.setNumGenList(new ArrayList<NumberGeneration>(numGenListDemo));
					logger.log(Level.SEVERE, "cont ProductInventoryTransaction -- " + numGenList.size());
				} else {
					errorList.add("There is some problem in number generation.");
				}

			} else {
//					errorList.add("Get some error in Stock list update Process");
				errorList.add("Please check material details ");
			}

		} catch (Exception e) {
//			errorList.add("Get some error in Stock list update Process " + e.getMessage());
			errorList.add("Please check material details " + e.getMessage());
			logger.log(Level.SEVERE, "Get some error in Stock list update Process" + e.getMessage());
			e.printStackTrace();
			logger.log(Level.SEVERE, "Get some error in Stock list update Process" + e);
			minDetailsMap.setErrorList(errorList);
			return minDetailsMap;
		}

//		minDetailsMap.setMinDetails(minDetailsMap.getMinDetails());
		minDetailsMap.setErrorList(errorList);

		for (MINUploadDetails min : minDetailsMap.getMinDetails()) {
			logger.log(Level.SEVERE,
					" min ser --" + min.getServiceCount() + " min cus pro -- " + min.getCustomerProCount()
							+ " min cus min co --" + min.getMinProCount() + " min tra = " + min.getMinTrCount());
		}
		/**
		 * @author Anil , Date : 20-12-2019 if error captured in validation programme
		 *         mannuly clearing bolbkey
		 */
		if (errorList.size() != 0) {
			blobkey = null;
		}

		return minDetailsMap;
	}

	private ArrayList<Integer> cronConfigsave(long companyId, ArrayList<String> exceldatalist) {
		System.out.println("Inside Config save");
		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		ArrayList<ConfigCategory> categorylist = new ArrayList<ConfigCategory>();
		List<ConfigCategory> cronJobList = ofy().load().type(ConfigCategory.class).filter("companyId", companyId)
				.filter("internalType", 29).list();

		int lastcount, newcount, fileRecords, insertedRecords;

		NumberGeneration ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
				.filter("processName", "ConfigCategory").filter("status", true).first().now();

		long number = ng.getNumber();
		int count = (int) number;

		boolean flag = true;
		for (int i = 3; i < exceldatalist.size(); i += 3) {

			flag = true;
			for (ConfigCategory con : cronJobList) {
				if (con.getCategoryName().equalsIgnoreCase(exceldatalist.get(i))) {
					flag = false;
				}
			}

			if (flag) {

				ConfigCategory category = new ConfigCategory();
				category.setInternalType(29);

				if (exceldatalist.get(i).trim().equalsIgnoreCase("na")) {
					category.setCategoryName("");
				} else {
					category.setCategoryName(exceldatalist.get(i));
				}

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("no")) {
					category.setStatus(false);
				} else {
					category.setStatus(true);
				}

				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					category.setDescription("");
				} else {
					category.setDescription(exceldatalist.get(i + 2));
				}

				category.setInternalType(29);
//					category.setCount(++count);
				category.setCompanyId(companyId);
				categorylist.add(category);
			}

		}

		if (categorylist.size() > 0) {
			lastcount = ofy().load().type(ConfigCategory.class).filter("companyId", companyId).count();

//			ofy().save().entities(categorylist).now();
			ArrayList<SuperModel> superlist = new ArrayList<SuperModel>();
			superlist.addAll(categorylist);

			GenricServiceImpl implupload = new GenricServiceImpl();
//			List<re>
//			implupload.save(superlist);
//			ArrayList<ReturnFromServer> returnList = implupload.save(superlist);

			for (SuperModel supEntity : superlist) {
				ReturnFromServer retList = implupload.save(supEntity);
				logger.log(Level.SEVERE, "corn config count --" + retList.count);
			}

//			ng.setNumber(count);

//			GenricServiceImpl impl = new GenricServiceImpl();
//			impl.save(ng);

			newcount = ofy().load().type(ConfigCategory.class).filter("companyId", companyId).count();

			fileRecords = categorylist.size();

			insertedRecords = newcount - lastcount;

			integerlist.add(fileRecords);
			integerlist.add(insertedRecords);

		}

		return integerlist;
	}

	@Override
	public ContractUploadDetails savedContractUploadTransactionDetails(long companyid, String entityname,
			String loginPerson) {
		// TODO Auto-generated method stub
		HashMap<String, Object> ErrorDetailsList = new HashMap<String, Object>();
		ArrayList<String> intlist = new ArrayList<String>();
		ArrayList<String> exceldatalist = new ArrayList<String>();
		ContractUploadDetails conUploadDt = new ContractUploadDetails();
		/***************************************
		 * Reading Excel File
		 *****************************************/
		// ////
		logger.log(Level.SEVERE, " Get Transaction details method -- " + entityname + " size -- ");
		try {

			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			contfmt.setTimeZone(TimeZone.getTimeZone("IST"));

			DataFormatter datafmt = new DataFormatter();
			long i;

			Workbook wb = null;

			try {
				logger.log(Level.SEVERE, " get workbook before ");
				wb = WorkbookFactory.create(new BlobstoreInputStream(blobkey));
				logger.log(Level.SEVERE, " get workbook after ");

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				intlist.add("UnAble to read Excel Sheet. Please Try after some time.");
				ErrorDetailsList.put("ErrorList", intlist);
				conUploadDt.setErrorList(intlist);
				return conUploadDt;
			} finally {
			}

			Sheet sheet = wb.getSheetAt(0);

			Iterator<Row> rowIterator = sheet.iterator();

			logger.log(Level.SEVERE, " iterator size =-- " + rowIterator.toString());
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();

				Iterator<Cell> celliterator = row.cellIterator();

				while (celliterator.hasNext()) {
					Cell cell = celliterator.next();

					switch (cell.getCellType()) {

					case Cell.CELL_TYPE_STRING:
						exceldatalist.add(cell.getStringCellValue());
						break;

					case Cell.CELL_TYPE_NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) {
							logger.log(Level.SEVERE, " date formate get from excel  " + cell.getDateCellValue());
							exceldatalist.add(contfmt.format(cell.getDateCellValue()));
							logger.log(Level.SEVERE,
									" date formate get from bin -- " + exceldatalist.get(exceldatalist.size() - 1)
											+ "\n save array date.. " + contfmt.format(cell.getDateCellValue()));
						} else {
							cell.getNumericCellValue();
							String sampletest = datafmt.formatCellValue(cell);
							exceldatalist.add(sampletest + "");
						}
						break;

					case Cell.CELL_TYPE_BOOLEAN:

						break;
					default:

					}
				}
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, " size -- " + exceldatalist.size());
			logger.log(Level.SEVERE, " exception --" + e);
			e.printStackTrace();
			intlist.add("Excel Sheet Invalide data Formate ");
			ErrorDetailsList.put("ErrorList", intlist);
			conUploadDt.setErrorList(intlist);
			return conUploadDt;
		} finally {

		}
		/******************** Finished Reading Excel Sheet *************************/
		// ////

		try {

			logger.log(Level.SEVERE,
					" value  ;; -->> " + entityname + " get first value " + exceldatalist.get(0).trim());

			if (entityname.equals("Contract Upload")) {
				if (exceldatalist.get(0).trim().equalsIgnoreCase("QUOTATION ID")
						&& exceldatalist.get(1).trim().equalsIgnoreCase("LEAD ID")
						&& exceldatalist.get(2).trim().equalsIgnoreCase("REF NUMBER1")
						&& exceldatalist.get(3).trim().equalsIgnoreCase("CUSTOMER ID")
						&& exceldatalist.get(4).trim().equalsIgnoreCase("CUSTOMER NAME")
						&& exceldatalist.get(5).trim().equalsIgnoreCase("CONTRACT DATE")
						&& exceldatalist.get(6).trim().equalsIgnoreCase("CONTRACT GROUP")
						&& exceldatalist.get(7).trim().equalsIgnoreCase("CONTRACT CATEGORY")
						&& exceldatalist.get(8).trim().equalsIgnoreCase("CONTRACT TYPE")
						&& exceldatalist.get(9).trim().equalsIgnoreCase("BRANCH")
						&& exceldatalist.get(10).trim().equalsIgnoreCase("SALES PERSON")
						&& exceldatalist.get(11).trim().equalsIgnoreCase("APPROVER NAME")
						&& exceldatalist.get(12).trim().equalsIgnoreCase("PAYMENT TREMS")
						&& exceldatalist.get(13).trim().equalsIgnoreCase("Is rate Contract")

						&& exceldatalist.get(14).trim().equalsIgnoreCase("PRODUCT ID")
						&& exceldatalist.get(15).trim().equalsIgnoreCase("PRODUCT NAME")
						&& exceldatalist.get(16).trim().equalsIgnoreCase("Price / Unit Price")
						&& exceldatalist.get(17).trim().equalsIgnoreCase("Unit")
						&& exceldatalist.get(18).trim().equalsIgnoreCase("No Of Service")
						&& exceldatalist.get(19).trim().equalsIgnoreCase("Contract Start date")
						&& exceldatalist.get(20).trim().equalsIgnoreCase("Contract duration In Days")

						&& exceldatalist.get(21).trim().equalsIgnoreCase("PRODUCT ID")
						&& exceldatalist.get(22).trim().equalsIgnoreCase("PRODUCT NAME")
						&& exceldatalist.get(23).trim().equalsIgnoreCase("Price / Unit Price")
						&& exceldatalist.get(24).trim().equalsIgnoreCase("Unit")
						&& exceldatalist.get(25).trim().equalsIgnoreCase("No Of Service")
						&& exceldatalist.get(26).trim().equalsIgnoreCase("Contract Start date")
						&& exceldatalist.get(27).trim().equalsIgnoreCase("Contract duration In Days")

						&& exceldatalist.get(28).trim().equalsIgnoreCase("PRODUCT ID")
						&& exceldatalist.get(29).trim().equalsIgnoreCase("PRODUCT NAME")
						&& exceldatalist.get(30).trim().equalsIgnoreCase("Price / Unit Price")
						&& exceldatalist.get(31).trim().equalsIgnoreCase("Unit")
						&& exceldatalist.get(32).trim().equalsIgnoreCase("No Of Service")
						&& exceldatalist.get(33).trim().equalsIgnoreCase("Contract Start date")
						&& exceldatalist.get(34).trim().equalsIgnoreCase("Contract duration In Days")

						&& exceldatalist.get(35).trim().equalsIgnoreCase("PRODUCT ID")
						&& exceldatalist.get(36).trim().equalsIgnoreCase("PRODUCT NAME")
						&& exceldatalist.get(37).trim().equalsIgnoreCase("Price / Unit Price")
						&& exceldatalist.get(38).trim().equalsIgnoreCase("Unit")
						&& exceldatalist.get(39).trim().equalsIgnoreCase("No Of Service")
						&& exceldatalist.get(40).trim().equalsIgnoreCase("Contract Start date")
						&& exceldatalist.get(41).trim().equalsIgnoreCase("Contract duration In Days")

						&& exceldatalist.get(42).trim().equalsIgnoreCase("PRODUCT ID")
						&& exceldatalist.get(43).trim().equalsIgnoreCase("PRODUCT NAME")
						&& exceldatalist.get(44).trim().equalsIgnoreCase("Price / Unit Price")
						&& exceldatalist.get(45).trim().equalsIgnoreCase("Unit")
						&& exceldatalist.get(46).trim().equalsIgnoreCase("No Of Service")
						&& exceldatalist.get(47).trim().equalsIgnoreCase("Contract Start date")
						&& exceldatalist.get(48).trim().equalsIgnoreCase("Contract duration In Days")

				) {

					if (exceldatalist.size() > 784) {
						intlist.add("There is more then 15 records in excel sheet.");
					} else {
//							intlist = checkContractUploadDetails(companyid,exceldatalist, loginPerson);
					}

				} else {
					intlist.add("Please check Excel Headers.");
				}
			} else {
				intlist.add("Invalid Excel Sheet");
			}

		} catch (Exception e) {
			e.printStackTrace();
			intlist.add("Error while reading Excel Sheet");
			System.out.println(e.getMessage());
			conUploadDt.setErrorList(intlist);
			return conUploadDt;
		} finally {
//			if(intlist.size()==0 && entityname.equals("MIN Upload")){
//				ErrorDetailsList.put("ErrorList",intlist.toString());
//				ErrorDetailsList.put(COMPANYID, "000");
//				ErrorDetailsList.put(LOGINNAME,loginPerson);
//				ErrorDetailsList.put(EXCELSHIT, exceldatalist.toString());
//			}
		}
		conUploadDt.setExceldatalist(exceldatalist);
		conUploadDt.setLoginName(loginPerson);
		conUploadDt.setCompanyId(companyid);
		conUploadDt.setErrorList(intlist);
		return conUploadDt;
	}

	@Override
	public ContractUploadDetails checkContractUploadDetails(ContractUploadDetails contDetails) {
		ContractUploadDetails checkContractDt = new ContractUploadDetails();
		try {

			contfmt.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));

			logger.log(Level.SEVERE, " checkContractUploadDetails method called -- ");

			errorList = new ArrayList<String>();
			contractList = new ArrayList<String>();
			contractList.addAll(contDetails.getExceldatalist());
			contractDetails = new ArrayList<CustomerContractDetails>();

			boolean prodFlag = false;
			int rowCount = 1;

			minList = new ArrayList<String>();
			empNameList = new HashSet<String>();
			paymentTerms = new HashSet<String>();

			customerDetail = new HashMap<Integer, String>();
			productDetail = new HashMap<Integer, String>();

			Date date = new Date();
			CustomerContractDetails conUpload;
			EmployeeInfo empinfo;
			ProductGroupList proDetails;
			for (int i = 49; i < contractList.size(); i += 49) {
				logger.log(Level.SEVERE, "i --> " + i + "   custcount --" + contractList.get(i + 3).trim());
				conUpload = new CustomerContractDetails();
				conUpload.setCompanyId(contDetails.getCompanyId());
				if (!contractList.get(i).trim().equalsIgnoreCase("na")) {
					if (contractList.get(i).trim().matches("[0-9]+")) {
						conUpload.setQuotationCount(Integer.parseInt(contractList.get(i).trim()));
					} else {
						errorList.add(rowCount + ". Quotation id must contain numeric value.");
					}
				}

				if (!contractList.get(i + 1).trim().equalsIgnoreCase("na")) {
					if (contractList.get(i + 1).trim().matches("[0-9]+")) {
						conUpload.setLeadCount(Integer.parseInt(contractList.get(i + 1).trim()));
					} else {
						errorList.add(rowCount + ".  Lead id must contain numeric value.");
					}
				}

				if (!contractList.get(i + 2).trim().equalsIgnoreCase("na")) {
					conUpload.setRefId(contractList.get(i + 2).trim());
				}

				if (!contractList.get(i + 3).trim().equalsIgnoreCase("na")) {
					if (contractList.get(i + 3).trim().matches("[0-9]+")) {
						conUpload.setCustomerCount(Integer.parseInt(contractList.get(i + 3).trim()));
					} else {
						errorList.add(rowCount + ".  Customer id must contain numeric value.");
					}
				} else {
					errorList.add(rowCount + ".  Customer Id is mandatory.");
				}

				if (!contractList.get(i + 4).trim().equalsIgnoreCase("na")) {
					conUpload.setFullName((contractList.get(i + 4).trim()));
					customerDetail.put(Integer.parseInt(contractList.get(i + 3).trim()),
							contractList.get(i + 4).trim());
				} else {
					errorList.add(rowCount + ".  Customer name is mandatory.");
				}

				if (!contractList.get(i + 5).trim().equalsIgnoreCase("NA")) {
					try {

						date = contfmt.parse(contractList.get(i + 5).trim());
						conUpload.setContractDate(date);
					} catch (Exception e) {
						errorList.add(rowCount + " Contract date should be in 'dd/mm/yyyy' format only.");
						logger.log(Level.SEVERE, " This contract  Date formate problem" + date + " excel value "
								+ contractList.get(i + 5).trim());
					}
				} else {
					errorList.add(rowCount + " Contract date is mandatory.");
					logger.log(Level.SEVERE, rowCount + " This contract Date not define ");
				}

				if (!contractList.get(i + 6).trim().equalsIgnoreCase("na")) {
					conUpload.setContractGroup(contractList.get(i + 6).trim());
				}

				if (!contractList.get(i + 7).trim().equalsIgnoreCase("na")) {
					conUpload.setContractCategory(contractList.get(i + 7).trim());
				} else {
					errorList.add(rowCount + "Contract category is mandatory.");
					logger.log(Level.SEVERE, rowCount + " This contract Category not define ");
				}

				if (!contractList.get(i + 8).trim().equalsIgnoreCase("na")) {
					conUpload.setContractType(contractList.get(i + 8).trim());
				}

				if (!contractList.get(i + 9).trim().equalsIgnoreCase("na")) {
					conUpload.setBranch(contractList.get(i + 9).trim());
				} else {
					errorList.add(rowCount + "Branch is mandatory.");
					logger.log(Level.SEVERE, rowCount + " This contract Branch not define ");
				}

				if (!contractList.get(i + 10).trim().equalsIgnoreCase("na")) {
					conUpload.setSalePerson(contractList.get(i + 10).trim());
					empNameList.add(contractList.get(i + 10).trim());
				} else {
					errorList.add(rowCount + "Sales person is mandatory.");
					logger.log(Level.SEVERE, rowCount + " This contract Person not define ");
				}

				if (!contractList.get(i + 11).trim().equalsIgnoreCase("na")) {
					conUpload.setApproverName(contractList.get(i + 11).trim());
					empNameList.add(contractList.get(i + 11).trim());
				} else {
					errorList.add(rowCount + "Approver person is mandatory.");
					logger.log(Level.SEVERE, rowCount + " This contract approver person not define ");
				}

				if (!contractList.get(i + 13).trim().equalsIgnoreCase("na")) {
					if (contractList.get(i + 13).trim().equalsIgnoreCase("yes")) {
						conUpload.setRateContract(true);
					}

				} else {
					errorList.add(rowCount + "Please enter yes/no only.");
					logger.log(Level.SEVERE, rowCount + " This contract Is rate contract yes/no not define  ");
				}

				if (!contractList.get(i + 12).trim().equalsIgnoreCase("na")) {
					conUpload.setPaymentTerms(contractList.get(i + 12).trim());
					paymentTerms.add(contractList.get(i + 12).trim());
				} else if (!contractList.get(i + 13).trim().equalsIgnoreCase("yes")) {

					errorList.add(rowCount + "Payment terms is mandatory");
					logger.log(Level.SEVERE, rowCount + " This contract Payment terms not define ");
				}

				int nu = 1, proSrNo = 0;
				for (int j = 14; j < 49; j = j + 7) {

					prodFlag = true;
					SalesLineItem serDetail = new SalesLineItem();

					if (!contractList.get(i + j).trim().equalsIgnoreCase("na")
							&& !contractList.get(i + j + 1).trim().equalsIgnoreCase("na")
							&& !contractList.get(i + j + 2).trim().equalsIgnoreCase("na")
							&& !contractList.get(i + j + 4).trim().equalsIgnoreCase("na")
							&& !contractList.get(i + j + 5).trim().equalsIgnoreCase("na")
							&& !contractList.get(i + j + 6).trim().equalsIgnoreCase("na")) {

						if (contractList.get(i + j).trim().matches("[0-9]+")
								&& contractList.get(i + j + 2).trim().matches("[0-9.]+")
								&& contractList.get(i + j + 4).trim().matches("[0-9]+")
								&& contractList.get(i + j + 6).trim().matches("[0-9]+")) {

							if ((!contractList.get(i + j + 3).trim().equalsIgnoreCase("na")
									&& contractList.get(i + j + 3).trim().matches("[0-9.]+"))) {
								prodFlag = true;
							} else if (!contractList.get(i + j + 3).trim().equalsIgnoreCase("na")) {
								prodFlag = false;
								errorList.add("Row No. : " + rowCount + ",Area " + contractList.get(i + j).trim()
										+ " must contain numeric value.");
							}

						} else {
							prodFlag = false;
							errorList.add("Row No. : " + rowCount + ",Product id " + contractList.get(i + j).trim()
									+ " must contain numeric value.");
						}

						try {
							date = contfmt.parse(contractList.get(i + j + 5).trim());
							prodFlag = true;

						} catch (Exception e) {
							prodFlag = false;
							errorList.add(rowCount + "Contract start date should be in 'dd/mm/yyyy' format only");
							logger.log(Level.SEVERE, " This contract start Date formate problem" + date
									+ " excel value " + contractList.get(i + j + 5).trim());
						}

						if (prodFlag) {

							SalesLineItem item = new SalesLineItem();
							ServiceProduct serPro = new ServiceProduct();
							serPro.setCount(Integer.parseInt(contractList.get(i + j).trim()));
							item.setPrduct(serPro);
							item.setProductName(contractList.get(i + j + 1).trim());
							item.setPrice(Double.parseDouble(contractList.get(i + j + 2).trim()));
//							item.setArea(minList.get(i+j+3).trim()));
							logger.log(Level.SEVERE,
									"Duration -- " + date + " no of service " + contractList.get(i + j + 4).trim());

							System.out.println("Duration -- " + contractList.get(i + j + 6).trim() + " no of service "
									+ contractList.get(i + j + 4).trim());

							item.setProductSrNo(++proSrNo);
							System.out.println("SEr sr no " + item.getProductSrNo() + " proSrNo " + proSrNo);
							item.setDuration(Integer.parseInt(contractList.get(i + j + 6).trim()));

							item.setNumberOfService(Integer.parseInt(contractList.get(i + j + 4).trim()));
							item.setStartDate(date);

							Calendar cal = Calendar.getInstance();
							cal.setTime(date);
							cal.add(Calendar.DATE, Integer.parseInt(contractList.get(i + j + 6).trim()) - 1);

							Date productEndDate = cal.getTime();
							Date prodenddate = new Date(productEndDate.getTime());
							productEndDate = prodenddate;

							item.setArea(contractList.get(i + j + 3) + "");
							item.setEndDate(productEndDate);
							item.setQuantity(1.0);
							item.setVatTax(new Tax());
							item.setServiceTax(new Tax());
							item.setBranchSchedulingInfo("Service Address#Yes#" + conUpload.getBranch() + "#$");
							conUpload.getServiceProduct().add(item);
							productDetail.put(Integer.parseInt(contractList.get(i + j).trim()),
									contractList.get(i + j + 1).trim());
						}
					}

				}

				contractDetails.add(conUpload);
				rowCount++;
			}
			logger.log(Level.SEVERE, "contractDetails list size -- " + contractDetails.size());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("checkContractUploadDetails -- " + e.getLocalizedMessage());
			logger.log(Level.SEVERE, "checkContractUploadDetails -- " + e.getMessage());
		}
		for (String str : errorList) {
			logger.log(Level.SEVERE, "error list -- " + str);
		}

//		if(errorList.size()>0){
		checkContractDt.setContractDetails(contractDetails);
		checkContractDt.setErrorList(errorList);
		checkContractDt.setPaymentTerms(paymentTerms);
		checkContractDt.setEmpNameList(empNameList);
		checkContractDt.setCompanyId(contDetails.getCompanyId());
		checkContractDt.setProductDetail(productDetail);
		checkContractDt.setCustomerDetail(customerDetail);
		checkContractDt.setLoginName(contDetails.getLoginName());
//		}

		return checkContractDt;

	}

	@Override
	public ContractUploadDetails CheckUploadContractDetail(ContractUploadDetails contractDt) {
		ContractUploadDetails contDetails = new ContractUploadDetails();
		try {

			for (String str : errorList) {
				logger.log(Level.SEVERE, " CheckUploadContractDetail error list -- " + str);
			}
			errorList.clear();
			customerDetail = new HashMap<Integer, String>();
			productDetail = new HashMap<Integer, String>();
			empNameList = new HashSet<String>();
			paymentTerms = new HashSet<String>();
			serProDetails = new ArrayList<ServiceProduct>();
			payTerms = new ArrayList<ConfigCategory>();
			custDetails = new ArrayList<Customer>();

			customerDetail.putAll(contractDt.getCustomerDetail());
			productDetail.putAll(contractDt.getProductDetail());
			empNameList.addAll(contractDt.getEmpNameList());
			paymentTerms.addAll(contractDt.getPaymentTerms());
			logger.log(Level.SEVERE,
					"get cust detail --" + customerDetail.size() + " prode de --" + productDetail.size());

			if (customerDetail.size() > 0) {

				Set<Integer> custIdKeySet = contractDt.getCustomerDetail().keySet(); // customerDetail.keySet();

				ArrayList<Integer> custIdList = new ArrayList<Integer>();

				custIdList.addAll(custIdKeySet);

				custDetails = ofy().load().type(Customer.class).filter("companyId", contractDt.getCompanyId())
						.filter("count IN", custIdList).list();
				contractDt.getCustDetails().addAll(custDetails);
//				contDetails.getCustDetails().addAll(custDetails);
			}

			if (productDetail.size() > 0) {
				Set<Integer> product = productDetail.keySet();

				ArrayList<Integer> pro = new ArrayList<Integer>();

				pro.addAll(product);

				serProDetails = ofy().load().type(ServiceProduct.class).filter("companyId", contractDt.getCompanyId())
						.filter("count IN", pro).list();
//				contractDt.setSerProDetails(serProDetails);
			}

			if (empNameList.size() > 0) {

				ArrayList<String> emp = new ArrayList<String>();

				emp.addAll(empNameList);

				empDetails = ofy().load().type(Employee.class).filter("companyId", contractDt.getCompanyId())
						.filter("fullname IN", emp).list();
				contractDt.getEmpDetails().addAll(empDetails);
			}

			if (paymentTerms.size() > 0) {
				ArrayList<String> terms = new ArrayList<String>();

				terms.addAll(paymentTerms);

				payTerms = ofy().load().type(ConfigCategory.class).filter("companyId", contractDt.getCompanyId())
						.filter("internalType", 25).list();
				contractDt.getPayTerms().addAll(payTerms);
			}

			logger.log(Level.SEVERE,
					" Customer count -- " + custDetails.size() + "  pro count - " + serProDetails.size()
							+ " emp count- " + empDetails.size() + "  payment terms -- " + payTerms.size());

			contDetails = checkContractDetailsValidation(contractDt);

		} catch (Exception e) {
			e.printStackTrace();
			contDetails.getErrorList().add("There is some error in data fatch");
			logger.log(Level.SEVERE, "CheckUploadContractDetail -- " + e.getMessage());
			return contDetails;
		} finally {

		}
		for (String str : errorList) {
			logger.log(Level.SEVERE, " CheckUploadContractDetail error list out -- " + str);
		}
		contDetails.setLoginName(contractDt.getLoginName());
		return contDetails;
	}

	ContractUploadDetails checkContractDetailsValidation(ContractUploadDetails contractDt) {
		try {
			boolean getCust = false, getPay = false;
			boolean getSerPro = false;
			for (Integer cust : contractDt.getCustomerDetail().keySet()) {
				getCust = false;
				for (Customer custDt : contractDt.getCustDetails()) {
					if (custDt.getCount() == cust) {
						getCust = true;
						break;
					}
				}

				if (!getCust) {
					contractDt.getErrorList().add("Customer Id -" + cust + " name - " + customerDetail.get(cust)
							+ " does not exist in database.");
				}
			}

			for (String payment : contractDt.getPaymentTerms()) {
				getPay = false;
				for (ConfigCategory catconfi : contractDt.getPayTerms()) {
					if (catconfi.getCategoryName().equals(payment) && !catconfi.getDescription().equals("")) {
						getPay = true;
						break;
					}
				}

				if (!getPay) {
					contractDt.getErrorList().add("Payment terms or duration is not define -- " + payment);
				}
			}

			for (String empName : contractDt.getEmpNameList()) {
				getPay = false;
				for (Employee catconfi : contractDt.getEmpDetails()) {
					if (catconfi.getFullname().equalsIgnoreCase(empName)) {
						getPay = true;
						break;
					}
				}

				if (!getPay) {
					contractDt.getErrorList().add("Employee name not define -- " + empName);
				}
			}

			for (Integer proID : contractDt.getProductDetail().keySet()) {
				getSerPro = false;
				for (ServiceProduct service : serProDetails) {
					if (service.getProductName().equals(productDetail.get(proID)) && proID == service.getCount()) {
						getSerPro = true;
						break;
					}
				}
				if (!getSerPro) {
					contractDt.getErrorList().add("Product id - " + proID + " Product Name -- "
							+ productDetail.get(proID) + " does not exist in database.");
				}
			}

			if (contractDt.getErrorList().size() == 0) {
				for (int i = 0; i < contractDt.getContractDetails().size(); i++) {
					Gson gson = new Gson();
					logger.log(Level.SEVERE,
							"Cost details cust id -- " + contractDt.getContractDetails().get(i).getCustomerCount());
					int totalSer = 0;
					Date StartDate = contractDt.getContractDetails().get(i).getContractDate();
					for (int j = 0; j < contractDt.getContractDetails().get(i).getServiceProduct().size(); j++) {
						for (ServiceProduct service : serProDetails) {
							if (contractDt.getContractDetails().get(i).getServiceProduct().get(j).getPrduct()
									.getCount() == service.getCount()) {
//								jsonSerPro =  gson.toJson(service, ServiceProduct.class);
//								serPro.add(service);
								ServiceProduct serPro1 = new ServiceProduct();
								serPro1.copyOfObject(service);

								serPro1.setPrice(contractDt.getContractDetails().get(i).getServiceProduct().get(j)
										.getPrduct().getPrice());
								serPro1.setDuration(contractDt.getContractDetails().get(i).getServiceProduct().get(j)
										.getDuration());
								serPro1.setNumberOfService(contractDt.getContractDetails().get(i).getServiceProduct()
										.get(j).getNumberOfServices());

								serPro1.setServiceTax(new Tax());
								serPro1.setVatTax(new Tax());

								contractDt.getContractDetails().get(i).getServiceProduct().get(j)
										.setProductCode(serPro1.getProductCode());

								/**
								 * nidhi need to write rate contract condition
								 */
								totalSer = totalSer + contractDt.getContractDetails().get(i).getServiceProduct().get(j)
										.getNumberOfServices();
								contractDt.getContractDetails().get(i).getServiceProduct().get(j)
										.setNumberOfService(contractDt.getContractDetails().get(i).getServiceProduct()
												.get(j).getNumberOfServices());
								contractDt.getContractDetails().get(i).getServiceProduct().get(j).setPrduct(serPro1);

								if (contractDt.getContractDetails().get(i).getServiceProduct().get(j).getArea()
										.equals("0")) {
									contractDt.getContractDetails().get(i).getServiceProduct().get(j).setArea("NA");
								}

								if (contractDt.getContractDetails().get(i).getServiceProduct().get(j).getStartDate()
										.before(StartDate)) {
									StartDate = contractDt.getContractDetails().get(i).getServiceProduct().get(j)
											.getStartDate();
								}

							}
						}
					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("checkContractDetailsValidation -- " + e.getMessage());
			logger.log(Level.SEVERE, " check Contract Details Validation Error -- " + e.getMessage());
			contractDt.getErrorList().add(" check Contract Details Validation Error -- " + e.getMessage());
			return contractDt;
		} finally {
			logger.log(Level.SEVERE, " check Contract Details Validation Error excution done -- ");
			logger.log(Level.SEVERE, " check customer list --" + contractDt.getCustDetails().size());
//			contractDt.getSerProDetails().clear();
		}
		return contractDt;
	}

	@Override
	public ContractUploadDetails UploadContractDetails(ContractUploadDetails contractDt) {
		try {
			logger.log(Level.SEVERE, "contractDetails size --   " + contractDt.getContractDetails().size() + "  con -- "
					+ contractDetails.size());
			Customer custDt;
			List<ServiceProduct> serPro = new ArrayList<ServiceProduct>();
			ConfigCategory catCon;
			String jsonCust = "", jsonSerPro = "", jsonPay = "", jsonCustDetails = "";
			contractDt.getErrorList().clear();
			int contRefCount = 0, serRefCount = 0, billRefCount = 0;

			ArrayList<String> processNameList = new ArrayList<String>();
			processNameList.add("Service");
			processNameList.add("Contract");
			processNameList.add("BillingDocument");

			List<NumberGeneration> numGenList = ofy().load().type(NumberGeneration.class)
					.filter("companyId", contractDt.getCompanyId()).filter("processName IN", processNameList)
					.filter("status", true).list();

			for (NumberGeneration num : numGenList) {
				if (num.getProcessName().equals("Service")) {
					serRefCount = num.getNumber().intValue();
				}
				if (num.getProcessName().equals("Contract")) {
					contRefCount = num.getNumber().intValue();
				}
				if (num.getProcessName().equals("BillingDocument")) {
					billRefCount = num.getNumber().intValue();
				}
			}

			logger.log(Level.SEVERE, "Contract count -- " + contRefCount + " server -- " + serRefCount
					+ "  billing --- " + billRefCount);
			if (contRefCount > 0 && serRefCount > 0 && billRefCount > 0) {
//				for(CustomerContractDetails custCont : contractDetails){ 

				for (int i = 0; i < contractDt.getContractDetails().size(); i++) {
					Gson gson = new Gson();
					logger.log(Level.SEVERE,
							"Cost details cust id -- " + contractDt.getContractDetails().get(i).getCustomerCount());
					int totalSer = 0;
					Date StartDate = contractDt.getContractDetails().get(i).getContractDate();
					for (int j = 0; j < contractDt.getContractDetails().get(i).getServiceProduct().size(); j++) {
//						for(ServiceProduct service : contractDt.getSerProDetails()){
//							if(contractDt.getContractDetails().get(i).getServiceProduct().get(j).getPrduct().getCount() == service.getCount()){
//								jsonSerPro =  gson.toJson(service, ServiceProduct.class);
//								serPro.add(service);
//								ServiceProduct serPro1 = new ServiceProduct();
//								serPro1.copyOfObject(service);

//								serPro1.setPrice(contractDt.getContractDetails().get(i).getServiceProduct().get(j).getPrduct().getPrice());
//								serPro1.setDuration(contractDt.getContractDetails().get(i).getServiceProduct().get(j).getDuration());
//								serPro1.setNumberOfService(contractDt.getContractDetails().get(i).getServiceProduct().get(j).getNumberOfServices());
//								
//								
//								serPro1.setServiceTax(new Tax());
//								serPro1.setVatTax(new Tax());
//								
//								
//								contractDt.getContractDetails().get(i).getServiceProduct().get(j).setProductCode(serPro1.getProductCode());

						/**
						 * nidhi need to write rate contract condition
						 */
						totalSer = totalSer + contractDt.getContractDetails().get(i).getServiceProduct().get(j)
								.getNumberOfServices();
						contractDt.getContractDetails().get(i).getServiceProduct().get(j).setNumberOfService(contractDt
								.getContractDetails().get(i).getServiceProduct().get(j).getNumberOfServices());
//								contractDt.getContractDetails().get(i).getServiceProduct().get(j).setPrduct(serPro1);

						if (contractDt.getContractDetails().get(i).getServiceProduct().get(j).getArea().equals("0")) {
							contractDt.getContractDetails().get(i).getServiceProduct().get(j).setArea("NA");
						}

						if (contractDt.getContractDetails().get(i).getServiceProduct().get(j).getStartDate()
								.before(StartDate)) {
							StartDate = contractDt.getContractDetails().get(i).getServiceProduct().get(j)
									.getStartDate();
						}

//							}
//						}
					}

					ConfigCategory mainCatconfi = null;
					for (ConfigCategory catconfi : contractDt.getPayTerms()) {
						if (catconfi.getCategoryName()
								.equals(contractDt.getContractDetails().get(i).getPaymentTerms())) {
							jsonPay = new String();
							jsonPay = gson.toJson(catconfi, ConfigCategory.class);
							mainCatconfi = catconfi;
							break;
						}
					}

					int contractRefCount = contRefCount;
					int serviceRefCount = serRefCount;

					int totalBill = reactOnAddPayTerms(contractDt.getContractDetails().get(i).getServiceProduct(),
							contractDt.getContractDetails().get(i).getPaymentTerms(), mainCatconfi, StartDate);
					int billinfRefCount = billRefCount;

					contractDt.getContractDetails().get(i).setContractCount(contractRefCount);
					contractDt.getContractDetails().get(i).setServiceCount(serviceRefCount);
					contractDt.getContractDetails().get(i).setBillingCount(billinfRefCount);

					/**
					 *
					 * jsonCustDetails = new String(); logger.log(Level.SEVERE," cust con details --
					 * " + custCont.getCustomerCount() + " cum name -- " + custCont.getRefId());
					 * jsonCustDetails = gson.toJson(custCont, CustomerContractDetails.class); Queue
					 * queue = QueueFactory.getQueue("CustomerContractUploadTaskQueue-queue");
					 * 
					 * queue.add(TaskOptions.Builder.withUrl("/slick_erp/CustomerContractUploadTaskQueue")
					 * .param("companyId", companyId+"").param("customerDetail", jsonCust)
					 * .param("confiCat", jsonPay) .param("CustContract",
					 * jsonCustDetails).param("contractRefCount", contractRefCount+"")
					 * .param("serviceRefCount", serviceRefCount+"").param("billinfRefCount",
					 * billinfRefCount+""));
					 */
					serRefCount = serRefCount + totalSer;
					billRefCount = billRefCount + totalBill;
					++contRefCount;
//					logger.log(Level.SEVERE,"Countract Count -- " + contRefCount);
//					logger.log(Level.SEVERE,"serRefCount Count -- " + serRefCount);
//					logger.log(Level.SEVERE,"billRefCount Count -- " + billRefCount);

				}

			} else {
				contractDt.getErrorList().add("There is some problem in number generation.");
			}

			for (int i = 0; i < numGenList.size(); i++) {
				if (numGenList.get(i).getProcessName().equals("Service")) {
					numGenList.get(i).setNumber(serRefCount);
//					logger.log(Level.SEVERE,"ser Countract Count -- " + serRefCount);
				}
				if (numGenList.get(i).getProcessName().equals("Contract")) {
					numGenList.get(i).setNumber(contRefCount);
//					logger.log(Level.SEVERE,"cont Countract Count -- " + contRefCount);
				}
				if (numGenList.get(i).getProcessName().equals("BillingDocument")) {
					numGenList.get(i).setNumber(billRefCount);
//					logger.log(Level.SEVERE,"cont billRefCount Count -- " + billRefCount);
				}
			}
			if (contRefCount > 0 && serRefCount > 0 && billRefCount > 0) {
				ofy().save().entities(numGenList);
			} else {
				errorList.add("There is some problem in number generation.");
			}

		} catch (Exception e) {
			contractDt.getErrorList().clear();
			e.printStackTrace();
			System.out.println("Upload Contract Details" + e.getMessage());
			logger.log(Level.SEVERE, "Upload Contract Details" + e.getMessage());
			contractDt.getErrorList().add("Upload Contract Details" + e.getMessage());
		}

		for (String str : errorList) {
			logger.log(Level.SEVERE, " UploadContractDetails error list -- " + str);
		}
		return contractDt;
	}

	private int reactOnAddPayTerms(List<SalesLineItem> serProduct, String paymentTerms, ConfigCategory confi,
			Date startDate) {
		int noOfPayTerms = 1;
		ConfigCategory conCategory = confi;
		if (conCategory != null) {
			int days = Integer.parseInt(conCategory.getDescription().trim());
			System.out.println("Payment terms Days::::::::::::::" + days);
			int duration = getDurationFromProductTable(serProduct);
			System.out.println("Duration::::::::: " + duration);
			noOfPayTerms = duration / days;
			System.out.println("No. of payment terms::::::::: " + noOfPayTerms);
			if (noOfPayTerms == 0) {
				noOfPayTerms = 1;
			}
		}
		return noOfPayTerms;
	}

	private int getDurationFromProductTable(List<SalesLineItem> serProduct) {
		int maxDur = 0;
		int currDur = 0;
		for (int i = 0; i < serProduct.size(); i++) {
			currDur = serProduct.get(i).getDuration();
			System.out.println(" curruent dur" + currDur);
			if (maxDur <= currDur) {
				maxDur = currDur;
			}
		}

		return maxDur;
	}

	@Override
	public ArrayList<String> finallyUploadContractDetails(ContractUploadDetails contractDt) {
		try {
			Customer custDt;
			List<ServiceProduct> serPro = new ArrayList<ServiceProduct>();
			String jsonCust = "", jsonPay = "", jsonCustDetails = "";
			contractDt.getErrorList().clear();
			int loopCount = 1;
			errorList.clear();
			if (contractDt.getErrorList().size() == 0) {
				for (CustomerContractDetails custCont : contractDt.getContractDetails()) {
					Gson gson = new Gson();
					logger.log(Level.SEVERE, "Cost details cust id -- " + custCont.getCustomerCount());
					for (Customer cust : contractDt.getCustDetails()) {
						if (cust.getCount() == custCont.getCustomerCount()) {
							custDt = new Customer();
							custDt = cust;
							jsonCust = new String();
							logger.log(Level.SEVERE, "queue Customer name -- " + cust.getFullname());
							jsonCust = gson.toJson(cust, Customer.class);
							break;
						}

					}

					for (ConfigCategory catconfi : contractDt.getPayTerms()) {
						if (catconfi.getCategoryName().equals(custCont.getPaymentTerms())) {
							jsonPay = new String();
							jsonPay = gson.toJson(catconfi, ConfigCategory.class);
							break;
						}
					}

					jsonCustDetails = new String();
					logger.log(Level.SEVERE, " cust con details -- " + custCont.getCustomerCount() + " cum name -- "
							+ custCont.getRefId());
					jsonCustDetails = gson.toJson(custCont, CustomerContractDetails.class);
					Queue queue = QueueFactory.getQueue("CustomerContractUploadTaskQueue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/CustomerContractUploadTaskQueue")
							.param("loginPerson", contractDt.getLoginName())
							.param("companyId", contractDt.getCompanyId() + "").param("customerDetail", jsonCust)
							.param("confiCat", jsonPay).param("CustContract", jsonCustDetails)
							.param("delayCount", loopCount + 1 + ""));

					logger.log(Level.SEVERE, "Countract Count -- " + custCont.getContractCount());
					logger.log(Level.SEVERE, "serRefCount Count -- " + custCont.getServiceCount());
					logger.log(Level.SEVERE, "billRefCount Count -- " + custCont.getBillingCount());
					loopCount++;
					Thread.sleep(3500);
				}
				contractDt.getErrorList().add("1");
			} else {
				contractDt.getErrorList().add("There is some problem to start data upload process");
			}

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception occured in final process. " + e);
			e.printStackTrace();
			System.out.println("Exception occured in final process. " + e);
			contractDt.getErrorList().add("Exception occured in final process. " + e);
			return contractDt.getErrorList();
		}

		return contractDt.getErrorList();
	}

	/**
	 * Date 07-12-2018 by Vijay Des :- Added for multiple warehouse upload for the
	 * same product
	 */
	private ProductInventoryViewDetails productinventoryDetails(int refnum, int pid, String pcode, String pname,
			double pprice, String pcategory, long companyId2, ArrayList<String> exceldatalist) {

		ProductInventoryViewDetails pinvendetails = new ProductInventoryViewDetails();

		/**
		 * Date : 23-04-2018 BY ANIL
		 */
		pinvendetails.setProdid(pid);
		pinvendetails.setProdcode(pcode);
		pinvendetails.setProdname(pname);
		/**
		 * End
		 */

		if (exceldatalist.get(refnum + 3).equalsIgnoreCase("na")) {
			pinvendetails.setWarehousename("");
		} else {
			pinvendetails.setWarehousename(exceldatalist.get(refnum + 3));
		}
		if (exceldatalist.get(refnum + 4).equalsIgnoreCase("na")) {
			pinvendetails.setStoragelocation("");
		} else {
			pinvendetails.setStoragelocation(exceldatalist.get(refnum + 4));
		}
		if (exceldatalist.get(refnum + 5).equalsIgnoreCase("na")) {
			pinvendetails.setStoragebin("");
		} else {
			pinvendetails.setStoragebin(exceldatalist.get(refnum + 5));
		}

		/**
		 * Date : 23-04-2018 BY ANIL setting available qty at the time of uploading
		 * product for HVAC/ROhan
		 */
		if (exceldatalist.get(refnum + 6).equalsIgnoreCase("na")) {
			pinvendetails.setAvailableqty(0);
			;
		} else {
			double availableStock = Double.parseDouble(exceldatalist.get(refnum + 6).trim());
			pinvendetails.setAvailableqty(availableStock);
		}

		return pinvendetails;

	}

	/**
	 * ends here
	 */

	@Override
	public ArrayList<EmployeeAsset> saveEmployeeAsset(long companyId) {
		// TODO Auto-generated method stub
		ArrayList<Integer> intlist = new ArrayList<Integer>();
		ArrayList<String> exceldatalist = new ArrayList<String>();
		DataFormatter datafmt1 = new DataFormatter();
		/*********************************
		 * Reading Excel Data & adding in list
		 **************************************/
		try {
			logger.log(Level.SEVERE, "Hiiiiiiiiiiiiiiiiiiiii");

			long i;

			Workbook wb = null;

			try {
				wb = WorkbookFactory.create(new BlobstoreInputStream(blobkey));
			} catch (EncryptedDocumentException e) {
				e.printStackTrace();
			} catch (InvalidFormatException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// if you upload other than .xls then it will give an error
				e.printStackTrace();
				intlist.add(-18);
//				return intlist;
			}
			Sheet sheet = wb.getSheetAt(0);
			// Get iterator to all the rows in current sheet
			Iterator<Row> rowIterator = sheet.iterator();
			// Traversing over each row of XLSX file
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through each columns
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						exceldatalist.add(cell.getStringCellValue());
						break;
					case Cell.CELL_TYPE_NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) {
							exceldatalist.add(fmt.format(cell.getDateCellValue()));
						} else {
							cell.getNumericCellValue();
							String sampletest = datafmt1.formatCellValue(cell);
							// i = (long) cell.getNumericCellValue();
							exceldatalist.add(sampletest + "");
							System.out.println("Value:" + sampletest);
						}
						break;
					case Cell.CELL_TYPE_BOOLEAN:
						break;

					// case Cell.CELL_TYPE_BLANK:
					// exceldatalist.add("NA");
					// break;

					default:
					}
				}
				System.out.println("");

			}
			System.out.println("Total size:" + exceldatalist.size());
			logger.log(Level.SEVERE, "FileString List Size:" + exceldatalist);
			wb.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		/**
		 * @author Anil , Date : 19-07-2019
		 */
//		if (entityname.equalsIgnoreCase("Employee Asset Allocation")) {
		if (exceldatalist.get(0).trim().equalsIgnoreCase("Employee Id")
				&& exceldatalist.get(1).trim().equalsIgnoreCase("Employee Name")
				&& exceldatalist.get(2).trim().equalsIgnoreCase("Branch")
				&& exceldatalist.get(3).trim().equalsIgnoreCase("Product Id")
				&& exceldatalist.get(4).trim().equalsIgnoreCase("Product Code")
				&& exceldatalist.get(5).trim().equalsIgnoreCase("Product Name")
				&& exceldatalist.get(6).trim().equalsIgnoreCase("Quantity")
				&& exceldatalist.get(7).trim().equalsIgnoreCase("Uom")
				&& exceldatalist.get(8).trim().equalsIgnoreCase("Price")
				&& exceldatalist.get(9).trim().equalsIgnoreCase("Total Amount")
				&& exceldatalist.get(10).trim().equalsIgnoreCase("Issue From Date")
				&& exceldatalist.get(11).trim().equalsIgnoreCase("Issue To Date")
				&& exceldatalist.get(12).trim().equalsIgnoreCase("Update Stock(Yes/No)")
				&& exceldatalist.get(13).trim().equalsIgnoreCase("Warehouse Name")
				&& exceldatalist.get(14).trim().equalsIgnoreCase("Storage Location")
				&& exceldatalist.get(15).trim().equalsIgnoreCase("Storage Bin")) {
//				intlist = leadsave(companyid, exceldatalist);
			ArrayList<EmployeeAsset> list = saveEmployeeAsset(companyId, exceldatalist);
			return list;
		} else {
//				ArrayList<EmployeeAsset> list
//				intlist.add(-1);
			return null;
		}
//		}

//		return null;
	}

	private ArrayList<EmployeeAsset> saveEmployeeAsset(long companyid, ArrayList<String> exceldatalist) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE, "Inside Employee Asset Save");

		ArrayList<EmployeeAsset> empAssetList = new ArrayList<EmployeeAsset>();
		ArrayList<Integer> integerlist = new ArrayList<Integer>();

//		int fileRecords;
//		int insertedRecords;
//		int lastcount;
//		int newcount;
//
//
//		NumberGeneration ng = new NumberGeneration();
//		ng = ofy().load().type(NumberGeneration.class)
//				.filter("companyId", companyid).filter("processName", "EmployeeAsset")
//				.filter("status", true).first().now();
//
//		long number = ng.getNumber();
//		int count = (int) number;

		for (int p = 16; p < exceldatalist.size(); p += 16) {

			EmployeeAsset empAsset = new EmployeeAsset();
			EmployeeAssetBean assetBean = new EmployeeAssetBean();

			if (exceldatalist.get(p).trim().equalsIgnoreCase("na")) {
				empAsset.setEmpId(Integer.parseInt(exceldatalist.get(p).trim()));
			} else {
				empAsset.setEmpId(Integer.parseInt(exceldatalist.get(p).trim()));
			}
			if (exceldatalist.get(p + 1).trim().equalsIgnoreCase("na")) {
				empAsset.setEmpName(exceldatalist.get(p + 1).trim());
			} else {
				empAsset.setEmpName(exceldatalist.get(p + 1).trim());
			}
			if (exceldatalist.get(p + 2).trim().equalsIgnoreCase("na")) {
				empAsset.setBranch(exceldatalist.get(p + 2).trim());
			} else {
				empAsset.setBranch(exceldatalist.get(p + 2).trim());
			}
			if (exceldatalist.get(p + 3).trim().equalsIgnoreCase("NA")) {
				assetBean.setAssetId(Integer.parseInt(exceldatalist.get(p + 3).trim()));
			} else {
				assetBean.setAssetId(Integer.parseInt(exceldatalist.get(p + 3).trim()));
			}
			if (exceldatalist.get(p + 4).trim().equalsIgnoreCase("NA")) {
				assetBean.setAssetCode(exceldatalist.get(p + 4).trim());
			} else {
				assetBean.setAssetCode(exceldatalist.get(p + 4).trim());
			}
			if (exceldatalist.get(p + 5).trim().equalsIgnoreCase("na")) {
				assetBean.setAssetName(exceldatalist.get(p + 5).trim());
			} else {
				assetBean.setAssetName(exceldatalist.get(p + 5).trim());
			}
			if (exceldatalist.get(p + 6).trim().equalsIgnoreCase("na")) {
				assetBean.setQty(Double.parseDouble(exceldatalist.get(p + 6).trim()));
			} else {
				assetBean.setQty(Double.parseDouble(exceldatalist.get(p + 6).trim()));
			}
			if (exceldatalist.get(p + 7).trim().equalsIgnoreCase("na")) {
				assetBean.setUom(exceldatalist.get(p + 7).trim());
			} else {
				assetBean.setUom(exceldatalist.get(p + 7).trim());
			}
			if (exceldatalist.get(p + 8).trim().equalsIgnoreCase("NA")) {
				assetBean.setPerUnitPrice(Double.parseDouble(exceldatalist.get(p + 8).trim()));
			} else {
				assetBean.setPerUnitPrice(Double.parseDouble(exceldatalist.get(p + 8).trim()));
			}
			if (exceldatalist.get(p + 9).trim().equalsIgnoreCase("na")) {
				assetBean.setPrice(Double.parseDouble(exceldatalist.get(p + 9).trim()));
			} else {
				assetBean.setPrice(Double.parseDouble(exceldatalist.get(p + 9).trim()));
			}
			if (exceldatalist.get(p + 10).trim().equalsIgnoreCase("na")) {
				try {
					assetBean.setFromDate(sefmt.parse(exceldatalist.get(p + 10).trim()));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				try {
					assetBean.setFromDate(sefmt.parse(exceldatalist.get(p + 10).trim()));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (exceldatalist.get(p + 11).trim().equalsIgnoreCase("na")) {
				try {
					assetBean.setToDate(sefmt.parse(exceldatalist.get(p + 11).trim()));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				try {
					assetBean.setToDate(sefmt.parse(exceldatalist.get(p + 11).trim()));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

			if (exceldatalist.get(p + 12).trim().equalsIgnoreCase("na")) {
				assetBean.setStatus(exceldatalist.get(p + 12).trim());
			} else {
				assetBean.setStatus(exceldatalist.get(p + 12).trim());
			}

			if (exceldatalist.get(p + 13).trim().equalsIgnoreCase("na")) {
				assetBean.setWarehouseName(exceldatalist.get(p + 13).trim());
			} else {
				assetBean.setWarehouseName(exceldatalist.get(p + 13).trim());
			}

			if (exceldatalist.get(p + 14).trim().equalsIgnoreCase("na")) {
				assetBean.setStorageLocName(exceldatalist.get(p + 14).trim());
			} else {
				assetBean.setStorageLocName(exceldatalist.get(p + 14).trim());
			}

			if (exceldatalist.get(p + 15).trim().equalsIgnoreCase("na")) {
				assetBean.setStorageLocBin(exceldatalist.get(p + 15).trim());
			} else {
				assetBean.setStorageLocBin(exceldatalist.get(p + 15).trim());
			}
			empAsset.getEmpAssetList().add(assetBean);
//			empAsset.setCount(++count);
			empAsset.setCompanyId(companyid);
			empAssetList.add(empAsset);

		}
		logger.log(Level.SEVERE, "EMPLOYEE ASSET SIZE : " + empAssetList.size());

		return empAssetList;

//		ArrayList<EmployeeAsset> empAssList=validateEmployeeAsset(empAssetList);
//		ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
//		
//		for(EmployeeAsset object:empAssList){
//			SuperModel model=object;
//			modelList.add(model);
//		}
//		if(modelList!=null&&modelList.size()!=0){
//			GenricServiceImpl impl=new GenricServiceImpl();
//			impl.save(modelList);
//		}

//		lastcount = ofy().load().type(EmployeeAsset.class).filter("companyId", companyid).count();
//		System.out.println("LastCount" + lastcount);
//		ofy().save().entities(empAssetList).now();
//		ng.setNumber(count);
//		GenricServiceImpl impl = new GenricServiceImpl();
//		impl.save(ng);
//		logger.log(Level.SEVERE,"Data saved successfuly=======================================");
//		newcount = ofy().load().type(EmployeeAsset.class).filter("companyId", companyid).count();
//		System.out.println("New Count" + newcount);
//		fileRecords = empAssetList.size();
//		insertedRecords = newcount - lastcount;
//		System.out.println("Result:" + insertedRecords);
//		integerlist.add(fileRecords);
//		integerlist.add(insertedRecords);
//		System.out.println("SIZE:" + empAssetList.size());

//		return integerlist;

//		return null;
	}

	@Override
	public ArrayList<EmployeeAsset> validateEmployeeAsset(ArrayList<EmployeeAsset> empAssetList) {

		/**
		 * Emp Id Emp Name Branch Product Id Product Code Product Name Remark
		 */
		ArrayList<String> errorList = new ArrayList<String>();
		errorList.add("");
		errorList.add("");
		errorList.add("");
		errorList.add("");
		errorList.add("");
		errorList.add("");

		String errorMsg = "";

		long companyId = empAssetList.get(0).getCompanyId();
		boolean stockUpdateFlag = false;
		ArrayList<EmployeeAsset> assetList = new ArrayList<EmployeeAsset>();
		HashSet<Integer> empIdHs = new HashSet<Integer>();
		HashSet<Integer> prodIdHs = new HashSet<Integer>();
		HashSet<Integer> prodInvHs = new HashSet<Integer>();

		logger.log(Level.SEVERE, "INSIDE ASSET VALIDATION  " + companyId);

		HashMap<Integer, ArrayList<EmployeeAsset>> assetMap = new HashMap<Integer, ArrayList<EmployeeAsset>>();

		for (EmployeeAsset empAsset : empAssetList) {
			empIdHs.add(empAsset.getEmpId());
			for (EmployeeAssetBean assetBean : empAsset.getEmpAssetList()) {
				prodIdHs.add(assetBean.getAssetId());
				if (assetBean.getStatus().equalsIgnoreCase("Yes")) {
					prodInvHs.add(assetBean.getAssetId());
					stockUpdateFlag = true;
				}

				if (assetMap != null && assetMap.size() != 0) {
					if (assetMap.containsKey(empAsset.getEmpId())) {
						assetMap.get(empAsset.getEmpId()).add(empAsset);
					} else {
						ArrayList<EmployeeAsset> list = new ArrayList<EmployeeAsset>();
						list.add(empAsset);
						assetMap.put(empAsset.getEmpId(), list);
					}
				} else {
					ArrayList<EmployeeAsset> list = new ArrayList<EmployeeAsset>();
					list.add(empAsset);
					assetMap.put(empAsset.getEmpId(), list);
				}
			}
		}

		logger.log(Level.SEVERE, "ASSET MAP " + assetMap.size());

		ArrayList<Integer> empIdList = new ArrayList<Integer>(empIdHs);
		ArrayList<Integer> prodIdList = new ArrayList<Integer>(prodIdHs);
		ArrayList<Integer> prodInvList = new ArrayList<Integer>(prodInvHs);

		List<Employee> employeeList = new ArrayList<Employee>();
		List<EmployeeAsset> employeeAssetList = new ArrayList<EmployeeAsset>();
		if (empIdList.size() != 0) {
			employeeList = ofy().load().type(Employee.class).filter("companyId", companyId)
					.filter("count IN", empIdList).list();
			employeeAssetList = ofy().load().type(EmployeeAsset.class).filter("companyId", companyId)
					.filter("empId IN", empIdList).list();
			if (employeeList != null)
				logger.log(Level.SEVERE, "UNIQUE EMP " + empIdList.size() + " EMP MAS " + employeeList.size());
			if (employeeAssetList != null)
				logger.log(Level.SEVERE, "EXISTING EMP ASSET LIST  " + employeeAssetList.size());

		} else {
			// Error msg come here
			errorMsg = errorMsg + "No Employee Found." + "/";

		}

		List<ItemProduct> itemProductList = new ArrayList<ItemProduct>();
		if (prodIdList.size() != 0) {
			itemProductList = ofy().load().type(ItemProduct.class).filter("companyId", companyId)
					.filter("count IN", prodIdList).list();
		} else {
			// Error msg come here
			errorMsg = errorMsg + "No ItemProduct Found." + "/";
		}
		logger.log(Level.SEVERE, "UNIQUE PROD " + prodIdList.size() + " PROD MAS " + itemProductList.size());

		List<ProductInventoryView> pivList = new ArrayList<ProductInventoryView>();
		if (prodInvList != null && prodInvList.size() != 0) {
			pivList = ofy().load().type(ProductInventoryView.class).filter("companyId", companyId)
					.filter("productinfo.prodID IN", prodInvList).list();
			logger.log(Level.SEVERE, "UNIQUE PIV " + pivList.size() + " PIV MAS " + pivList.size());
		} else {
			// Error msg come here
			errorMsg = errorMsg + "No Stock Master Found." + "/";
		}

		errorList.add(errorMsg);

		for (Map.Entry<Integer, ArrayList<EmployeeAsset>> entry : assetMap.entrySet()) {
			EmployeeAsset asset = getUpdatedEmployeeAsset(entry.getValue(), employeeList, itemProductList, pivList,
					employeeAssetList, errorList);
			if (asset != null) {
				assetList.add(asset);
			}
		}
		CsvWriter.employeeAssetErrorList = errorList;
		logger.log(Level.SEVERE, "ASSET LIST  " + assetList.size());

		return assetList;
	}

	@Override
	public ArrayList<EmployeeAsset> uploadEmployeeAsset(ArrayList<EmployeeAsset> list) {
		// TODO Auto-generated method stub
//		ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
//		
//		for(EmployeeAsset object:list){
//			SuperModel model=object;
//			modelList.add(model);
//		}
//		if(modelList!=null&&modelList.size()!=0){
//			GenricServiceImpl impl=new GenricServiceImpl();
//			impl.save(modelList);
//		}
//		return list;

		logger.log(Level.SEVERE, "Hi Employee asset allocation save Task queue calling ");
		Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue")
				.param("EntityName", "Employee Asset Allocation").param("companyId", list.get(0).getCompanyId() + ""));
		logger.log(Level.SEVERE, "Hi Task queue cazlling save");

		return null;
	}

	private EmployeeAsset getUpdatedEmployeeAsset(ArrayList<EmployeeAsset> value, List<Employee> employeeList,
			List<ItemProduct> itemProductList, List<ProductInventoryView> pivList,
			List<EmployeeAsset> employeeAssetList, ArrayList<String> errorList) {
		// TODO Auto-generated method stub

		String errorMsg = "";
		HashMap<String, Double> stockMap = new HashMap<String, Double>();
		EmployeeAsset object = new EmployeeAsset();
		Employee employee = getEmployeeDetails(value.get(0).getEmpId(), employeeList);
		if (getEmployeeAssetDetail(value.get(0).getEmpId(), employeeAssetList) != null) {
			// ERROR
			errorList.add(value.get(0).getEmpId() + "");
			errorList.add("");
			errorList.add("");
			errorList.add("");
			errorList.add("");
			errorList.add("");
			errorMsg = errorMsg + "Employee asset master already exist" + " / ";
			errorList.add(errorMsg);
			CsvWriter.employeeAssetErrorList = errorList;
			object.setCount(-1);
			return object;

		}
		if (employee == null) {
			// ERROR
			errorList.add(value.get(0).getEmpId() + "");
			errorList.add("");
			errorList.add("");
			errorList.add("");
			errorList.add("");
			errorList.add("");
			errorMsg = errorMsg + "No Employee Master Found." + " / ";
			errorList.add(errorMsg);
			CsvWriter.employeeAssetErrorList = errorList;
			object.setCount(-1);
			return object;
		}
		for (EmployeeAsset obj : value) {
			errorMsg = "";
			object.setCompanyId(obj.getCompanyId());
			object.setEmpId(obj.getEmpId());
			if (employee.getFullname().equals(obj.getEmpName())) {
				object.setEmpName(obj.getEmpName());
			} else {
				// Error
				errorMsg = errorMsg + "Employee name mismatched." + " / ";
				object.setCount(-1);
			}
			if (employee.getCellNumber1() != null) {
				object.setEmpCellNo(employee.getCellNumber1());
			}

			if (employee.getBranchName().equals(obj.getBranch())) {
				object.setBranch(obj.getBranch());
			} else {
				// ERROR
				errorMsg = errorMsg + "Employee branch mismatched." + " / ";
				object.setCount(-1);
			}
			int counter = 0;
			for (EmployeeAssetBean bean : obj.getEmpAssetList()) {
				if (counter != 0) {
					errorMsg = "";
				}
				counter++;
				EmployeeAssetBean beanObj = new EmployeeAssetBean();
				EmployeeAssetBean beanHisObj = new EmployeeAssetBean();

				ItemProduct product = getProductDetail(bean.getAssetId(), itemProductList);
				if (product != null) {
					beanHisObj.setAssetId(bean.getAssetId());
					beanObj.setAssetId(bean.getAssetId());
					if (bean.getAssetCode().equals(product.getProductCode())) {
						beanHisObj.setAssetCode(bean.getAssetCode());
						beanObj.setAssetCode(bean.getAssetCode());
					} else {
						// ERROR
						errorMsg = errorMsg + "Asset code is mismatched." + " / ";
						object.setCount(-1);
					}
//					if(bean.getAssetCategory().equals(product.getProductCategory())){
					beanHisObj.setAssetCategory(product.getProductCategory());
					beanObj.setAssetCategory(product.getProductCategory());
//					}else{
//						//ERROR
//						errorMsg=errorMsg+"Asset category is mismatched."+" / ";
//						object.setCount(-1);
//					}
					if (bean.getAssetName().equals(product.getProductName())) {
						beanHisObj.setAssetName(bean.getAssetName());
						beanObj.setAssetName(bean.getAssetName());
					} else {
						// ERROR
						errorMsg = errorMsg + "Asset name is mismatched." + " / ";
						object.setCount(-1);
					}
					if (bean.getUom().equals(product.getUnitOfMeasurement())) {
						beanHisObj.setUom(bean.getUom());
						beanObj.setUom(bean.getUom());
					} else {
						// ERROR
						errorMsg = errorMsg + "Uom is mismatched." + " / ";
						object.setCount(-1);
					}

					if (bean.getFromDate() != null) {
						beanHisObj.setFromDate(bean.getFromDate());
						beanObj.setFromDate(bean.getFromDate());
					}
					if (bean.getToDate() != null) {
						beanHisObj.setToDate(bean.getToDate());
						beanObj.setToDate(bean.getToDate());
					}

					beanHisObj.setQty(bean.getQty());
					beanHisObj.setPerUnitPrice(bean.getPerUnitPrice());
					beanHisObj.setPrice(bean.getPrice());

					beanObj.setQty(bean.getQty());
					beanObj.setPerUnitPrice(bean.getPerUnitPrice());
					beanObj.setPrice(bean.getPrice());

					if (bean.getStatus().equalsIgnoreCase("No")) {
						beanHisObj.setStatus("Issue");
						beanHisObj.setStockUpdated(true);

						beanObj.setStatus("Issue");
						beanObj.setStockUpdated(true);
					} else if (bean.getStatus().equalsIgnoreCase("Yes")) {
						beanHisObj.setStatus("Issue");
						beanHisObj.setStockUpdated(false);

						beanObj.setStatus("Issue");
						beanObj.setStockUpdated(true);

						object.setTransactionType("Issue");

						ProductInventoryView piv = getStockDetails(bean.getAssetId(), bean.getWarehouseName(),
								bean.getStorageLocName(), bean.getStorageLocBin(), pivList);
						if (piv != null) {
							beanHisObj.setWarehouseName(bean.getWarehouseName());
							beanHisObj.setStorageLocName(bean.getStorageLocName());
							beanHisObj.setStorageLocBin(bean.getStorageLocBin());
							beanHisObj.setAvailableQty(getAvailableQty(bean.getWarehouseName(),
									bean.getStorageLocName(), bean.getStorageLocBin(), piv));

							beanObj.setWarehouseName(bean.getWarehouseName());
							beanObj.setStorageLocName(bean.getStorageLocName());
							beanObj.setStorageLocBin(bean.getStorageLocBin());
							beanObj.setAvailableQty(getAvailableQty(bean.getWarehouseName(), bean.getStorageLocName(),
									bean.getStorageLocBin(), piv));

							String key = bean.getAssetId() + bean.getWarehouseName() + bean.getStorageLocName()
									+ bean.getStorageLocBin();

							logger.log(Level.SEVERE, "Prod Id : " + bean.getAssetId() + " Available QTY : "
									+ beanHisObj.getAvailableQty() + " QTY : " + beanHisObj.getQty());

							if (stockMap != null && stockMap.size() != 0) {
								if (stockMap.containsKey(key)) {
									double qty = stockMap.get(key);
									qty = qty - beanHisObj.getQty();
									if (qty < beanHisObj.getQty()) {
										// ERROR
										errorMsg = errorMsg + "Available stock is less than required quantity." + " / ";
										object.setCount(-1);
									} else {
										stockMap.put(key, qty);
									}
								} else {
									stockMap.put(key, beanHisObj.getAvailableQty());
									if (beanHisObj.getAvailableQty() < beanHisObj.getQty()) {
										// ERROR
										errorMsg = errorMsg + "Available stock is less than required quantity." + " / ";
										object.setCount(-1);
									}
								}
							} else {
								stockMap.put(key, beanHisObj.getAvailableQty());
								if (beanHisObj.getAvailableQty() < beanHisObj.getQty()) {
									// ERROR
									errorMsg = errorMsg + "Available stock is less than required quantity." + " / ";
									object.setCount(-1);
								}
							}
						} else {
							// ERROR
							errorMsg = errorMsg + "Product master not found." + " / ";
							object.setCount(-1);
						}

					} else {
						// ERROR
						errorMsg = errorMsg + "Invalid input at update stock column." + " / ";
						object.setCount(-1);
					}
				} else {
					// ERROR
					errorMsg = errorMsg + "Product master not found." + " / ";
					object.setCount(-1);
				}

				object.getEmpAssetList().add(beanObj);
				if (bean.getStatus().equalsIgnoreCase("Yes")) {
					if (object.getEmpAssetHisList() != null) {
						object.getEmpAssetHisList().add(beanHisObj);
					} else {
						ArrayList<EmployeeAssetBean> beanList = new ArrayList<EmployeeAssetBean>();
						beanList.add(beanHisObj);
						object.setEmpAssetHisList(beanList);
					}

				}

				errorList.add(obj.getEmpId() + "");
				errorList.add(obj.getEmpName());
				errorList.add(obj.getBranch());
				errorList.add(bean.getAssetId() + "");
				errorList.add(bean.getAssetCode() + "");
				errorList.add(bean.getAssetName());
				errorList.add(errorMsg);

			}

		}

		return object;
	}

	private EmployeeAsset getEmployeeAssetDetail(int empId, List<EmployeeAsset> employeeAssetList) {
		// TODO Auto-generated method stub
		for (EmployeeAsset obj : employeeAssetList) {
			if (obj.getEmpId() == empId) {
				return obj;
			}
		}
		return null;
	}

	private double getAvailableQty(String warehouseName, String storageLocation, String storageBin,
			ProductInventoryView piv) {
		// TODO Auto-generated method stub
		double qty = 0;
		for (ProductInventoryViewDetails pivd : piv.getDetails()) {
			if (pivd.getWarehousename().equals(warehouseName) && pivd.getStoragelocation().equals(storageLocation)
					&& pivd.getStoragebin().equals(storageBin)) {
				return pivd.getAvailableqty();
			}
		}
		return 0;
	}

	private ProductInventoryView getStockDetails(int assetId, String warehouseName, String storageLocation,
			String storageBin, List<ProductInventoryView> pivList) {
		// TODO Auto-generated method stub
		for (ProductInventoryView piv : pivList) {
			if (assetId == piv.getProdID()) {
				for (ProductInventoryViewDetails pivd : piv.getDetails()) {
					if (pivd.getWarehousename().equals(warehouseName)
							&& pivd.getStoragelocation().equals(storageLocation)
							&& pivd.getStoragebin().equals(storageBin)) {
						return piv;
					}
				}
			}
		}
		return null;
	}

	private ItemProduct getProductDetail(int assetId, List<ItemProduct> itemProductList) {
		// TODO Auto-generated method stub
		for (ItemProduct product : itemProductList) {
			if (assetId == product.getCount()) {
				return product;
			}
		}
		return null;
	}

	private Employee getEmployeeDetails(int empId, List<Employee> employeeList) {
		// TODO Auto-generated method stub
		for (Employee employee : employeeList) {
			if (empId == employee.getCount()) {
				return employee;
			}
		}
		return null;
	}

	@Override
	public String saveCustomerBranch(ArrayList<CustomerBranchDetails> list) {
		ArrayList<SuperModel> modelList = new ArrayList<SuperModel>();

		for (CustomerBranchDetails object : list) {
			SuperModel model = object;
			modelList.add(model);
		}
		if (modelList != null && modelList.size() != 0) {
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(modelList);
		}
		return "success";
	}

	@Override
	public ArrayList<CustomerBranchDetails> uploadCustomerBranch(long companyId) {
		// TODO Auto-generated method stub
		ArrayList<String> exceldatalist = new ArrayList<String>();
		DataFormatter datafmt1 = new DataFormatter();
		/*********************************
		 * Reading Excel Data & adding in list
		 **************************************/
		try {
			logger.log(Level.SEVERE, "Hiiiiiiiiiiiiiiiiiiiii");

			long i;

			Workbook wb = null;

			try {
				wb = WorkbookFactory.create(new BlobstoreInputStream(blobkey));
			} catch (EncryptedDocumentException e) {
				e.printStackTrace();
			} catch (InvalidFormatException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// if you upload other than .xls then it will give an error
				e.printStackTrace();
				return null;
			}

			Sheet sheet = wb.getSheetAt(0);

			// Get iterator to all the rows in current sheet

			Iterator<Row> rowIterator = sheet.iterator();

			// Traversing over each row of XLSX file

			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through each columns
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						exceldatalist.add(cell.getStringCellValue());
						break;
					case Cell.CELL_TYPE_NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) {
							exceldatalist.add(fmt.format(cell.getDateCellValue()));
						} else {
							cell.getNumericCellValue();
							String sampletest = datafmt1.formatCellValue(cell);
							// i = (long) cell.getNumericCellValue();
							exceldatalist.add(sampletest + "");
							System.out.println("Value:" + sampletest);
						}
						break;
					case Cell.CELL_TYPE_BOOLEAN:
						break;

					// case Cell.CELL_TYPE_BLANK:
					// exceldatalist.add("NA");
					// break;

					default:
					}
				}
				System.out.println("");

			}

			System.out.println("Total size:" + exceldatalist.size());
			logger.log(Level.SEVERE, "FileString List Size:" + exceldatalist);

			wb.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (exceldatalist.get(0).trim().equalsIgnoreCase("*Customer ID")
//				&& exceldatalist.get(1).trim()
//						.equalsIgnoreCase("*Customer Name")
//				&& exceldatalist.get(2).trim()
//						.equalsIgnoreCase("*Customer Cell")
				&& exceldatalist.get(1).trim().equalsIgnoreCase("*Branch Name")
				&& exceldatalist.get(2).trim().equalsIgnoreCase("Branch Email")
				&& exceldatalist.get(3).trim().equalsIgnoreCase("*Servicing Branch")
				&& exceldatalist.get(4).trim().equalsIgnoreCase("*Cell Phone No 1")
				&& exceldatalist.get(5).trim().equalsIgnoreCase("Branch GSTIN")
				&& exceldatalist.get(6).trim().equalsIgnoreCase("Point Of Contact Name")
				&& exceldatalist.get(7).trim().equalsIgnoreCase("Point Of Contact Landline")
				&& exceldatalist.get(8).trim().equalsIgnoreCase("Point Of Contact Cell")
				&& exceldatalist.get(9).trim().equalsIgnoreCase("Point Of Contact Email")
				&& exceldatalist.get(10).trim().equalsIgnoreCase("*Address Line 1")
				&& exceldatalist.get(11).trim().equalsIgnoreCase("Address Line 2")
				&& exceldatalist.get(12).trim().equalsIgnoreCase("Landmark")
				&& exceldatalist.get(13).trim().equalsIgnoreCase("Locality")
				&& exceldatalist.get(14).trim().equalsIgnoreCase("*City")
				&& exceldatalist.get(15).trim().equalsIgnoreCase("*State")
				&& exceldatalist.get(16).trim().equalsIgnoreCase("*Country")
				&& exceldatalist.get(17).trim().equalsIgnoreCase("Pin")

				&& exceldatalist.get(18).trim().equalsIgnoreCase("*Billing Address Line 1")
				&& exceldatalist.get(19).trim().equalsIgnoreCase("Billing Address Line 2")
				&& exceldatalist.get(20).trim().equalsIgnoreCase("Billing Address Landmark")
				&& exceldatalist.get(21).trim().equalsIgnoreCase("Billing Address Locality")
				&& exceldatalist.get(22).trim().equalsIgnoreCase("*Billing Address City")
				&& exceldatalist.get(23).trim().equalsIgnoreCase("*Billing Address State")
				&& exceldatalist.get(24).trim().equalsIgnoreCase("*Billing Address Country")
				&& exceldatalist.get(25).trim().equalsIgnoreCase("Billing Address Pin")

				/** date 16.02.2018 added by komal for ares wise billing **/
				&& exceldatalist.get(26).trim().equalsIgnoreCase("Area")
				&& exceldatalist.get(27).trim().equalsIgnoreCase("UOM")
				&& exceldatalist.get(28).trim().equalsIgnoreCase("Cost Center")
				&& exceldatalist.get(29).trim().equalsIgnoreCase("Tier")
				&& exceldatalist.get(30).trim().equalsIgnoreCase("TAT"))

		{
			ArrayList<CustomerBranchDetails> list = customerBranchSave(companyId, exceldatalist);
			return list;
		} else {

			return null;
		}

	}

	@Override
	public ArrayList<VendorPriceListDetails> uploadVendorProductPrice(long companyId) {

		ArrayList<String> exceldatalist = new ArrayList<String>();

		exceldatalist = readExcelFile();
		logger.log(Level.SEVERE, "excel list after reading data exceldatalist size", exceldatalist.size());

		if (exceldatalist.get(0).trim().equalsIgnoreCase("Title")
				&& exceldatalist.get(1).trim().equalsIgnoreCase("Product Id")
				&& exceldatalist.get(2).trim().equalsIgnoreCase("Product Name")
				&& exceldatalist.get(3).trim().equalsIgnoreCase("Vendor Id")
				&& exceldatalist.get(4).trim().equalsIgnoreCase("Vendor Name")
				&& exceldatalist.get(5).trim().equalsIgnoreCase("Vendor Price")
				&& exceldatalist.get(6).trim().equalsIgnoreCase("From Date")
				&& exceldatalist.get(7).trim().equalsIgnoreCase("To Date"))

		{
			ArrayList<VendorPriceListDetails> list = VendorProductPriceSave(companyId, exceldatalist);
			return list;
		} else {
			return null;
		}

	}

	private ArrayList<VendorPriceListDetails> VendorProductPriceSave(long companyID, ArrayList<String> exceldatalist) {

		ArrayList<VendorPriceListDetails> vendorPricelist = new ArrayList<VendorPriceListDetails>();
		ArrayList<String> errorList = new ArrayList<String>();

		List<ItemProduct> productlist = ofy().load().type(ItemProduct.class).filter("companyId", companyID).list();
		logger.log(Level.SEVERE, "productlist size" + productlist.size());

		List<Vendor> vendorlist = ofy().load().type(Vendor.class).filter("companyId", companyID).list();
		logger.log(Level.SEVERE, "vendorlist size" + vendorlist.size());

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		format.setTimeZone(TimeZone.getTimeZone("IST"));

		HashMap<Integer, ArrayList<String>> pricelistHashmap = new HashMap<Integer, ArrayList<String>>();

		for (int i = 8; i < exceldatalist.size(); i += 8) {

			if (pricelistHashmap != null && pricelistHashmap.size() != 0) {

				if (pricelistHashmap.containsKey(Integer.parseInt(exceldatalist.get(i + 1)))) {
					pricelistHashmap.get(Integer.parseInt(exceldatalist.get(i + 1))).add(exceldatalist.get(i + 3));
				} else {
					ArrayList<String> list = new ArrayList<String>();
					list.add(exceldatalist.get(i + 3));
					pricelistHashmap.put(Integer.parseInt(exceldatalist.get(i + 1)), list);
				}
			} else {
				ArrayList<String> list = new ArrayList<String>();
				list.add(exceldatalist.get(i + 3));
				pricelistHashmap.put(Integer.parseInt(exceldatalist.get(i + 1)), list);
			}
		}

		for (Map.Entry<Integer, ArrayList<String>> entryset : pricelistHashmap.entrySet()) {

			ArrayList<String> listpricelist = entryset.getValue();
			logger.log(Level.SEVERE, "listpricelist size" + listpricelist.size());

			HashSet<String> hsvendorpricelist = new HashSet<String>();
			for (String str : listpricelist) {
				hsvendorpricelist.add(str);
			}

			System.out.println("hsvendorpricelist " + hsvendorpricelist.size());

			HashMap<Integer, Integer> vendorID = new HashMap<Integer, Integer>();

			if (listpricelist.size() != hsvendorpricelist.size()) {
				for (String str : listpricelist) {
					Integer count = 0;
					if (vendorID != null && vendorID.size() != 0) {

						if (vendorID.containsKey(Integer.parseInt(str))) {
							int vendorcount = vendorID.get(Integer.parseInt(str));
							vendorcount++;
							vendorID.put(Integer.parseInt(str), vendorcount);

						} else {
							vendorID.put(Integer.parseInt(str), ++count);
						}
					} else {
						vendorID.put(Integer.parseInt(str), ++count);
					}
				}
			}

			if (vendorID.size() != 0) {
				for (Map.Entry<Integer, Integer> vendorIdentryset : vendorID.entrySet()) {

					if (vendorIdentryset.getValue() > 1) {
						String error = " Vendor ID - " + vendorIdentryset.getKey()
								+ " exist more than once for product id - " + entryset.getKey();
						errorList.add(entryset.getKey() + "");
						errorList.add(error);
					}

				}
			}

		}

		for (int i = 8; i < exceldatalist.size(); i += 8) {

			VendorPriceListDetails vendorprice = new VendorPriceListDetails();

			String error = "";

			if (i == 8 && errorList.size() != 0) {

				vendorprice.setCount(-1);
			}

			errorList.add(exceldatalist.get(i + 1));

			if (exceldatalist.get(i).contains(",")) {
				error += " Comma not allowed in Title Column";
				vendorprice.setCount(-1);
			}
			if (exceldatalist.get(i + 1).contains(",")) {
				error += " Comma not allowed in Product Id Column";
				vendorprice.setCount(-1);
			}
			if (exceldatalist.get(i + 2).contains(",")) {
				error += " Comma not allowed in Product Name Column";
				vendorprice.setCount(-1);
			}
			if (exceldatalist.get(i + 3).contains(",")) {
				error += " Comma not allowed in Vendor Id Column";
				vendorprice.setCount(-1);
			}

			if (exceldatalist.get(i + 5).contains(",")) {
				error += " Comma not allowed in Vendor Name Column";
				vendorprice.setCount(-1);
			}

			if (exceldatalist.get(i + 6).contains(",")) {
				error += " Comma not allowed in Vendor Price Column";
				vendorprice.setCount(-1);
			}

			if (exceldatalist.get(i + 7).contains(",")) {
				error += " Comma not allowed in From Date Column";
				vendorprice.setCount(-1);
			}

			if (exceldatalist.get(i + 1).equalsIgnoreCase("NA")) {
				error += " Product Id is Mandatory in To Date Column";
				vendorprice.setCount(-1);
			} else {

				String strproduct = validateItemProduct(productlist, exceldatalist.get(i + 1).trim(),
						exceldatalist.get(i + 2).trim());
				System.out.println("strproduct ==" + strproduct);
				if (!strproduct.equals("")) {
					error += strproduct;
					vendorprice.setCount(-1);
				}
//				if(strproduct.equals("")){
//					Integer productId = Integer.parseInt(exceldatalist.get(i + 1).trim());
//					PriceList pricelist = ofy().load().type(PriceList.class).filter("companyId", companyID)
//										  .filter("prodID", productId).first().now();
//					System.out.println("pricelist"+pricelist);
//					logger.log(Level.SEVERE,"pricelist =="+pricelist);
//					if(pricelist!=null){
//						error += " Vendor Price list for Product ID "+exceldatalist.get(i + 1).trim()+" Product Name "+exceldatalist.get(i + 2).trim() +" already exist";
//						vendorprice.setCount(-1);
//					}
//				}
			}

			if (exceldatalist.get(i + 3).equalsIgnoreCase("NA")) {
				error += " Vendor Id is Mandatory";
				vendorprice.setCount(-1);
			} else {
				String strvenderId = ValidateVendorProduct(vendorlist, exceldatalist.get(i + 3).trim(),
						exceldatalist.get(i + 4).trim());

				if (!strvenderId.equals("")) {
					error += strvenderId;
					vendorprice.setCount(-1);
				}
			}

			if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("NA")) {
				error += " Vendor Price is Mandatory";
				vendorprice.setCount(-1);
			}

			double vendorPrice = Double.parseDouble(exceldatalist.get(i + 5).trim());

			if (vendorPrice <= 0) {
				error += " Vendor Price should be greater than 0";
				vendorprice.setCount(-1);
			}

			if (!exceldatalist.get(i + 6).trim().equalsIgnoreCase("NA")) {

				try {
					format.parse(exceldatalist.get(i + 6).trim());
				} catch (ParseException e) {
					error += " Error in From Date :-" + e.getMessage();
					vendorprice.setCount(-1);
					e.printStackTrace();
				}
			}

			if (!exceldatalist.get(i + 7).trim().equalsIgnoreCase("NA")) {
				try {
					format.parse(exceldatalist.get(i + 7).trim());
				} catch (ParseException e) {
					error += " Error in To Date :-" + e.getMessage();
					vendorprice.setCount(-1);
					e.printStackTrace();
				}
			}

			errorList.add(error);
			vendorPricelist.add(vendorprice);

		}
		CsvWriter.VendorProductPriceErrorList = errorList;

		return vendorPricelist;
	}

	private String ValidateVendorProduct(List<Vendor> vendorlist, String strvendorId, String vendorname) {
		String str = "";
		Integer vendorId = Integer.parseInt(strvendorId);

		for (Vendor vendorEntity : vendorlist) {
			if (vendorEntity.getCount() == vendorId) {
				str = "";
				if (!vendorEntity.getVendorName().equals(vendorname)) {
					str = " Vendor ID and Vendor Name does not match in Vendor Master";
				}
				break;
			} else {
				str = " Vendor ID Does not exist in Vendor Master";
			}
		}
		return str;
	}

	private String validateItemProduct(List<ItemProduct> productlist, String productId, String productName) {
		String str = "";
		Integer productID = Integer.parseInt(productId);

		for (ItemProduct itemproduct : productlist) {
			if (itemproduct.getCount() == productID) {
				str = "";
				if (!itemproduct.getProductName().trim().equals(productName)) {
					str = " Product ID and Product Name does not match in Item Product Master";
				}
				break;
			} else {
				str = " Product ID Does not exist in Item Product Master";
			}
		}
		return str;

	}

	private String validateEmployee(List<Employee> employeelist, String empId, String employeeName) {
		String str = "";
		Integer employeeID = Integer.parseInt(empId);

		for (Employee employee : employeelist) {
			if (employee.getCount() == employeeID) {
				str = "";
				if (!employee.getFullname().trim().equals(employeeName)) {
					str = " Employee ID and Employee Name does not match in Employee Master. ";
//					logger.log(Level.SEVERE, "errrorrrr 11"+str);
				}
				break;
			} else {
				str = " Employee ID Does not exist in Employee Master. ";
//				logger.log(Level.SEVERE, "errrorrrr 22"+str);
			}
		}
		logger.log(Level.SEVERE, "return error value " + str);
		return str;

	}

	/**
	 * @author Vijay Chougule Des :- used to read excel file and all data added in
	 *         list with the trim
	 */
	private ArrayList<String> readExcelFile() {

		ArrayList<String> exceldatalist = new ArrayList<String>();
		DataFormatter datafmt1 = new DataFormatter();

		/*********************************
		 * Reading Excel Data & adding in list
		 **************************************/
		try {
			logger.log(Level.SEVERE, "Hiiiiiiiiiiiiiiiiiiiii");

			long i;

			Workbook wb = null;

			try {
				wb = WorkbookFactory.create(new BlobstoreInputStream(blobkey));
			} catch (EncryptedDocumentException e) {
				e.printStackTrace();
			} catch (InvalidFormatException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// if you upload other than .xls then it will give an error
				e.printStackTrace();
				return null;
			}

			Sheet sheet = wb.getSheetAt(0);

			// Get iterator to all the rows in current sheet

			Iterator<Row> rowIterator = sheet.iterator();

			// Traversing over each row of XLSX file

			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through each columns
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						exceldatalist.add(cell.getStringCellValue().trim());
						break;
					case Cell.CELL_TYPE_NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) {
							exceldatalist.add(fmt.format(cell.getDateCellValue()).trim());
						} else {
							cell.getNumericCellValue();
							String sampletest = datafmt1.formatCellValue(cell);
							// i = (long) cell.getNumericCellValue();
							exceldatalist.add(sampletest.trim());
							System.out.println("Value:" + sampletest);
						}
						break;
					case Cell.CELL_TYPE_BOOLEAN:
						break;

					// case Cell.CELL_TYPE_BLANK:
					// exceldatalist.add("NA");
					// break;

					default:
					}
				}
				System.out.println("");

			}

			System.out.println("Total size:" + exceldatalist.size());
			logger.log(Level.SEVERE, "FileString List Size:" + exceldatalist);

			wb.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return exceldatalist;
	}

	@Override
	public String vednorProductPrice(long companyId, String taskName) {

		logger.log(Level.SEVERE, "Inhouse services updation task queue calling");
		try {
			Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue").param("EntityName", taskName)
					.param("companyId", companyId + ""));

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception ==" + e.getMessage());
			return "Failed please try again";
		}
		return "Vendor Product Price Upload Process Started";

	}

	@Override
	public ArrayList<CTCTemplate> validateCTCTemplate(long companyId) {

		ArrayList<String> exceldatalist = new ArrayList<String>();
		this.index = 0;
		exceldatalist.addAll(readExcelFile());
		logger.log(Level.SEVERE, "excel list after reading data exceldatalist size  ==" + exceldatalist.size());

		if (exceldatalist.get(index).trim().equalsIgnoreCase("Sr No.")
				&& exceldatalist.get(++index).trim().equalsIgnoreCase("CTC Template Name")
				&& exceldatalist.get(++index).trim().equalsIgnoreCase("Gross Amount (Monthly)")
				&& exceldatalist.get(++index).trim().equalsIgnoreCase("Gender")
				&& exceldatalist.get(++index).trim().equalsIgnoreCase("Category")
				&& exceldatalist.get(++index).trim().equalsIgnoreCase("Earning")
				&& exceldatalist.get(++index).trim().equalsIgnoreCase("Basic")
				&& exceldatalist.get(++index).trim().equalsIgnoreCase("DA")
				&& exceldatalist.get(++index).trim().equalsIgnoreCase("HRA")

				&& checkEmployeeDeductionColumn(exceldatalist, index)

				&& exceldatalist.get(++index).trim().equalsIgnoreCase("PF")
				&& exceldatalist.get(++index).trim().equalsIgnoreCase("ESIC")

				&& checkCompanyDeductionColumn(exceldatalist, index)
				&& exceldatalist.get(++index).trim().equalsIgnoreCase("PF")
				&& exceldatalist.get(++index).trim().equalsIgnoreCase("ESIC")

				&& checkOtherColumn(exceldatalist, index)

				&& exceldatalist.get(++index).trim().equalsIgnoreCase("Paid Leave Amount")
				&& exceldatalist.get(++index).trim().equalsIgnoreCase("Paid Leave Name")
				&& exceldatalist.get(++index).trim().equalsIgnoreCase("Paid Leave (Correspondence Name)")
				&& exceldatalist.get(++index).trim().equalsIgnoreCase("Bonus Amount")
				&& exceldatalist.get(++index).trim().equalsIgnoreCase("Bonus Name")
				&& exceldatalist.get(++index).trim().equalsIgnoreCase("Bonus (Correspondence Name)")) {
			ArrayList<CTCTemplate> list = validateCTCTemplatelist(companyId, exceldatalist, ++index);
			return list;
		} else {

			return null;
		}

	}

	private ArrayList<CTCTemplate> validateCTCTemplatelist(long companyId, ArrayList<String> exceldatalist,
			int columncount) {
		// TODO Auto-generated method stub
		System.out.println("columncount" + columncount);

		ArrayList<String> errorList = new ArrayList<String>();

		HashMap<String, Integer> ctcTemplateNameHashMap = new HashMap<String, Integer>();
		List<CtcComponent> ctcComponentlist = ofy().load().type(CtcComponent.class).filter("companyId", companyId)
				.list();

		for (int i = columncount; i < exceldatalist.size(); i += columncount) {
			System.out.println("exceldatalist.get(columncount+1)" + exceldatalist.get(i + 1));
			if (ctcTemplateNameHashMap != null && ctcTemplateNameHashMap.size() != 0) {
				if (ctcTemplateNameHashMap.containsKey(exceldatalist.get(i + 1))) {
					int count = ctcTemplateNameHashMap.get(exceldatalist.get(i + 1));
					count++;
					ctcTemplateNameHashMap.put(exceldatalist.get(i + 1), count);
					System.out.println("count ==" + count);
				} else {
					ctcTemplateNameHashMap.put(exceldatalist.get(i + 1), 1);
				}
			} else {
				ctcTemplateNameHashMap.put(exceldatalist.get(i + 1), 1);
			}
		}

		for (Map.Entry<String, Integer> ctctemplateName : ctcTemplateNameHashMap.entrySet()) {
			System.out.println("ctctemplateName.getValue() " + ctctemplateName.getValue());
			if (ctctemplateName.getValue() > 1) {
				String error = " CTC Template Name Can't be Duplicate in excel sheet.";
				errorList.add("0");
				errorList.add(error);
			}
		}

		List<CTCTemplate> ctctemplatelist = ofy().load().type(CTCTemplate.class).filter("companyId", companyId).list();
		logger.log(Level.SEVERE, "ctctemplatelist" + ctctemplatelist.size());
		int startcolumncount = columncount;
		System.out.println("startcolumncount ==" + startcolumncount);
		logger.log(Level.SEVERE, "startcolumncount" + startcolumncount);

		List<ConfigCategory> categorylist = ofy().load().type(ConfigCategory.class).filter("companyId", companyId)
				.filter("internalType", -1).list();
		logger.log(Level.SEVERE, "categorylist" + categorylist.size());

		List<PaidLeave> paidleavelist = ofy().load().type(PaidLeave.class).filter("companyId", companyId).list();
		logger.log(Level.SEVERE, "paidleavelist" + paidleavelist.size());

		List<Bonus> bonuslist = ofy().load().type(Bonus.class).filter("companyId", companyId).list();
		logger.log(Level.SEVERE, "bonuslist" + bonuslist.size());

		int earningStartColumn = 5;
		int employeeDeductionStartColumn = getEmployeeDeductionColumnNumber(exceldatalist, "Employee Deduction");
		System.out.println("employeeDeductionStartColumn " + employeeDeductionStartColumn);
		logger.log(Level.SEVERE, "employeeDeductionStartColumn" + employeeDeductionStartColumn);

		int companyDeductionStartColumn = getEmployeeDeductionColumnNumber(exceldatalist, "Company Deduction");
		System.out.println("companyDeductionStartColumn " + companyDeductionStartColumn);
		logger.log(Level.SEVERE, "companyDeductionStartColumn" + companyDeductionStartColumn);

		int otherStartColumn = getEmployeeDeductionColumnNumber(exceldatalist, "Other");
		System.out.println("otherStartColumn " + otherStartColumn);
		logger.log(Level.SEVERE, "otherStartColumn" + otherStartColumn);

		ArrayList<CTCTemplate> arrayCTCTemplate = new ArrayList<CTCTemplate>();
		System.out.println("columncount " + columncount);
		for (int i = 0; i < columncount; i += columncount) {
			String error = "";

			if (i == 0) {
				ProvidentFund fund = ofy().load().type(ProvidentFund.class).filter("companyId", companyId)
						.filter("pfName", "PF").first().now();
				logger.log(Level.SEVERE, "ProvidentFund" + fund);
				if (fund == null) {
					error += "PF Does not exist in the ProvidentFund master.";
				} else {
					if (fund.isStatus() == false) {
						error += "PF component Status is InActive in the in the ProvidentFund master.";
					}
				}

				Esic esic = ofy().load().type(Esic.class).filter("companyId", companyId).filter("esicName", "ESIC")
						.first().now();
				logger.log(Level.SEVERE, "Esic" + esic);
				if (esic == null) {
					error += "ESIC Does not exist in the Esic master.";
				} else {
					if (esic.isStatus() == false) {
						error += "ESIC component Status is InActive in the in the ProvidentFund master.";
					}
				}
			}
			if (earningStartColumn < employeeDeductionStartColumn) {
				for (int j = earningStartColumn; j < employeeDeductionStartColumn; j++) {
					if (j != earningStartColumn) {
						String newcolumnName = getColunName(exceldatalist, j);
						logger.log(Level.SEVERE, "New column in excel file " + newcolumnName);
//							CtcComponent ctccomponent = ofy().load().type(CtcComponent.class).filter("isDeduction", false).filter("name", newcolumnName).first().now();
						CtcComponent ctccomponent = null;
						for (CtcComponent component : ctcComponentlist) {
							if (component.isDeduction() == false && (component.getName().equals(newcolumnName)
									|| component.getShortName().equals(newcolumnName))) {
								ctccomponent = component;
								break;
							}
						}
						logger.log(Level.SEVERE, "ctccomponent" + ctccomponent);
						if (ctccomponent == null) {
							error += " " + newcolumnName + " Does not exist in the Earning component master.";
						} else {
							if (ctccomponent.getStatus() == false) {
								error += " " + newcolumnName + " Status is InActive in the Earning component master.";
							}
						}
					}

				}
			}

			if (employeeDeductionStartColumn < companyDeductionStartColumn) {
				for (int j = employeeDeductionStartColumn; j < companyDeductionStartColumn; j++) {
					if (j != employeeDeductionStartColumn) {
						if (!getColunName(exceldatalist, j).equalsIgnoreCase("PF")
								&& !getColunName(exceldatalist, j).equalsIgnoreCase("ESIC")) {
							String newcolumnName = getColunName(exceldatalist, j);
							logger.log(Level.SEVERE, "New column in excel file " + newcolumnName);
//							CtcComponent ctccomponent = ofy().load().type(CtcComponent.class).filter("isDeduction", true).filter("isRecord", false).filter("name", newcolumnName).first().now();
							CtcComponent ctccomponent = null;
							for (CtcComponent component : ctcComponentlist) {
								if (component.isDeduction() == true && component.getIsRecord() == false
										&& (component.getName().equals(newcolumnName)
												|| component.getShortName().equals(newcolumnName))) {
									ctccomponent = component;
									break;
								}
							}

							logger.log(Level.SEVERE, "ctccomponent" + ctccomponent);
							if (ctccomponent == null) {
								error += " " + newcolumnName
										+ " Does not exist in the Employee Deduction component master.";
							} else {
								if (ctccomponent.getStatus() == false) {
									error += " " + newcolumnName
											+ " Status is InActive in the Employee Deduction component master.";
								}
							}
						}
					}
				}
			}

			if (companyDeductionStartColumn < otherStartColumn) {
				for (int j = companyDeductionStartColumn; j < otherStartColumn; j++) {
					if (j != companyDeductionStartColumn) {
						if (!getColunName(exceldatalist, j).equalsIgnoreCase("PF")
								&& !getColunName(exceldatalist, j).equalsIgnoreCase("ESIC")) {
							String newcolumnName = getColunName(exceldatalist, j);
							logger.log(Level.SEVERE, "New column in excel file " + newcolumnName);
//							CtcComponent ctccomponent = ofy().load().type(CtcComponent.class).filter("isDeduction", true).filter("isRecord", true).filter("name", newcolumnName).first().now();
							CtcComponent ctccomponent = null;
							for (CtcComponent component : ctcComponentlist) {
								if (component.isDeduction() == true && component.getIsRecord() == true
										&& (component.getName().equals(newcolumnName)
												|| component.getShortName().equals(newcolumnName))) {
									ctccomponent = component;
									break;
								}
							}

							logger.log(Level.SEVERE, "ctccomponent" + ctccomponent);
							if (ctccomponent == null) {
								error += " " + newcolumnName
										+ " Does not exist in the Company Deduction component master.";
							} else {
								if (ctccomponent.getStatus() == false) {
									error += " " + newcolumnName
											+ " Status is InActive in the Company Deduction component master.";
								}
							}
						}
					}

				}
			}

			if (!error.equals("")) {
				errorList.add("0");
				errorList.add(error);
			}
		}

		for (int i = columncount; i < exceldatalist.size(); i += columncount) {

			double grossAmtMonthly = 0;
			double sumOfEarning = 0;
			try {
				grossAmtMonthly = Double.parseDouble(exceldatalist.get(i + 2));
			} catch (Exception e) {
			}

			String error = "";
			CTCTemplate ctctemplate = new CTCTemplate();

			if (i == startcolumncount && errorList.size() != 0) {
				ctctemplate.setCount(-1);
			}

			errorList.add(exceldatalist.get(i));

			if (exceldatalist.get(i + 1).equalsIgnoreCase("NA")) {
				error += " CTC Template Name can't be NA.";
				ctctemplate.setCount(-1);
			} else {
				boolean flag = validateCTCTemplateName(exceldatalist.get(i + 1), ctctemplatelist);
				if (flag) {
					error += " CTC Template Name can't be duplicate " + exceldatalist.get(i + 1)
							+ "- already defined in the system.";
					ctctemplate.setCount(-1);
				}
			}

			if (exceldatalist.get(i + 2).equalsIgnoreCase("NA")) {
				error += " Gross Amount (Monthly) can't be NA.";
				ctctemplate.setCount(-1);
			} else {
				try {
					Double.parseDouble(exceldatalist.get(i + 2));
				} catch (Exception e) {
					error += " Please enter numeric value in Gross Amount (Monthly) column.";
					ctctemplate.setCount(-1);
				}
			}

			if (exceldatalist.get(i + 3).equalsIgnoreCase("NA")) {
				error += " Gender can't be NA.";
				ctctemplate.setCount(-1);
			} else {
				if (exceldatalist.get(i + 3).equals("All") || exceldatalist.get(i + 3).equals("Male")
						|| exceldatalist.get(i + 3).equals("Female")) {
				} else {
					error += " Value in gender column can be All/Male/Female.";
					ctctemplate.setCount(-1);
				}
			}

			if (exceldatalist.get(i + 4).equalsIgnoreCase("NA")) {
				error += " Category can't be NA.";
				ctctemplate.setCount(-1);
			} else {
				if (validateCategoryName(exceldatalist.get(i + 4), categorylist) == false) {
					error += exceldatalist.get(i + 4) + "- category doesn't exist in category master.";
					ctctemplate.setCount(-1);
				}
			}

			if (earningStartColumn < employeeDeductionStartColumn) {
				for (int j = earningStartColumn; j < employeeDeductionStartColumn; j++) {

					try {
						if (j == earningStartColumn) {
							System.out.println("For Earning Column.");
						} else if (!exceldatalist.get(i + j).equalsIgnoreCase("NA")) {
							sumOfEarning += Double.parseDouble(exceldatalist.get(i + j));
						}
					} catch (Exception e) {
						error += " Please enter numeric value in " + getColunName(exceldatalist, j) + " column.";
						ctctemplate.setCount(-1);
					}

				}
			} else {
				System.out.println("Earning components column ends here");
			}

			double difference = grossAmtMonthly - sumOfEarning;
			if (difference != 0) {
				error += " Gross Amount is not equal to sum of earning component amount.";
				ctctemplate.setCount(-1);
			}

			if (employeeDeductionStartColumn < companyDeductionStartColumn) {
				for (int j = employeeDeductionStartColumn; j < companyDeductionStartColumn; j++) {
					try {
						if (j == employeeDeductionStartColumn) {
							System.out.println("For Employee Deduction Column");
						} else if (!exceldatalist.get(i + j).equalsIgnoreCase("NA")) {
							Double.parseDouble(exceldatalist.get(i + j));
						}
					} catch (Exception e) {
						error += " Please enter numeric value in " + getColunName(exceldatalist, j) + " column.";
						ctctemplate.setCount(-1);
					}
				}
			} else {
				System.out.println("Employee deduction components column ends here");
			}

			if (companyDeductionStartColumn < otherStartColumn) {
				for (int j = companyDeductionStartColumn; j < otherStartColumn; j++) {
					try {
						if (j == companyDeductionStartColumn) {
							System.out.println("For Company Deduction Column");
						} else if (!exceldatalist.get(i + j).equalsIgnoreCase("NA")) {
							Double.parseDouble(exceldatalist.get(i + j));
						}
					} catch (Exception e) {
						error += " Please enter numeric value in " + getColunName(exceldatalist, j) + " column.";
						ctctemplate.setCount(-1);
					}
				}
			} else {
				System.out.println("Employee deduction components column ends here");
			}

			System.out.println("i ==" + i);
			System.out.println("otherStartColumn " + otherStartColumn);
			System.out.println("i+otherStartColumn+1 " + (i + otherStartColumn + 1));

			if (!exceldatalist.get(i + otherStartColumn + 1).equalsIgnoreCase("NA")) {
				try {
					Double.parseDouble(exceldatalist.get(i + otherStartColumn + 1));
				} catch (Exception e) {
					error += " Please enter numeric value in Paid Leave Amount column.";
					ctctemplate.setCount(-1);
				}

			}
			if (!exceldatalist.get(i + otherStartColumn + 2).equalsIgnoreCase("NA")) {
				if (validatePaidLeaveName(exceldatalist.get(i + otherStartColumn + 2), paidleavelist) == false) {
					error += " Entered Paid Leave Name doesn't exist in Paid Leave master.";
					ctctemplate.setCount(-1);
				}

			}
			if (!exceldatalist.get(i + otherStartColumn + 4).equalsIgnoreCase("NA")) {
				try {
					Double.parseDouble(exceldatalist.get(i + otherStartColumn + 4));
				} catch (Exception e) {
					error += " Please enter numeric value in Bonus Amount column.";
					ctctemplate.setCount(-1);
				}

			}

			if (!exceldatalist.get(i + otherStartColumn + 5).equalsIgnoreCase("NA")) {
				if (validatePaidBonusName(exceldatalist.get(i + otherStartColumn + 5), bonuslist) == false) {
					error += " Entered Bonus Name doesn't exist in Bonus Master.";
					ctctemplate.setCount(-1);
				}
			}

			errorList.add(error);
			arrayCTCTemplate.add(ctctemplate);
		}
		CsvWriter.CTCTemplateUploadErrorlist = errorList;

		return arrayCTCTemplate;
	}

	private boolean validatePaidBonusName(String bonusName, List<Bonus> bonuslist) {
		for (Bonus bonusEntity : bonuslist) {
			if (bonusEntity.getBonusName().equals(bonusName)) {
				return true;
			}
		}
		return false;
	}

	private boolean validatePaidLeaveName(String paidleaveName, List<PaidLeave> paidleavelist) {

		for (PaidLeave paidleave : paidleavelist) {
			if (paidleave.getPaidLeaveName().equals(paidleaveName)) {
				return true;
			}
		}

		return false;
	}

	private String getColunName(ArrayList<String> exceldatalist, int j) {
		return exceldatalist.get(j);
	}

	private int getEmployeeDeductionColumnNumber(ArrayList<String> exceldatalist, String ColumnaName) {

		for (int i = 0; i < exceldatalist.size(); i++) {
			if (exceldatalist.get(i).equalsIgnoreCase(ColumnaName)) {
				System.out.println("column number" + i);
				return i;
			}
		}

		return 0;
	}

	private boolean validateCategoryName(String categoryName, List<ConfigCategory> categorylist) {

		for (ConfigCategory category : categorylist) {
			if (category.getCategoryName().equals(categoryName)) {
				return true;
			}
		}
		return false;
	}

	private boolean validateCTCTemplateName(String ctcTemplateName, List<CTCTemplate> ctctemplatelist) {

		for (CTCTemplate ctctemplate : ctctemplatelist) {
			if (ctctemplate.getCtcTemplateName().equals(ctcTemplateName)) {
				return true;
			}
		}
		return false;
	}

	private boolean checkOtherColumn(ArrayList<String> exceldatalist, int index) {
		boolean flag = false;
		if (exceldatalist.get(++index).trim().equalsIgnoreCase("Other")) {
			this.index = index;
			flag = true;
		} else {
			this.index = index;
			flag = checkOtherColumn(exceldatalist, index);
		}
		return flag;
	}

	private boolean checkCompanyDeductionColumn(ArrayList<String> exceldatalist, int index) {
		boolean flag = false;
		if (exceldatalist.get(++index).trim().equalsIgnoreCase("Company Deduction")) {
			this.index = index;
			flag = true;
		} else {
			this.index = index;
			flag = checkCompanyDeductionColumn(exceldatalist, index);
		}
		return flag;

	}

	private boolean checkEmployeeDeductionColumn(ArrayList<String> exceldatalist, int index) {
		boolean flag = false;
		if (exceldatalist.get(++index).trim().equalsIgnoreCase("Employee Deduction")) {
			this.index = index;
			flag = true;
		} else {
			this.index = index;
			flag = checkEmployeeDeductionColumn(exceldatalist, index);
		}
		return flag;
	}

	@Override
	public String uploadCTCTemplate(long companyId, String taskName) {

		logger.log(Level.SEVERE, "Inhouse services updation task queue calling");
		try {
			Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue").param("EntityName", taskName)
					.param("companyId", companyId + ""));

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception ==" + e.getMessage());
			return "Failed please try again";
		}
		return "CTC Template Upload Process Started";
	}

	@Override
	public ArrayList<AssetMovementInfo> validateUpdateAssetDetails(long companyId) {

		ArrayList<String> exceldatalist = new ArrayList<String>();

		exceldatalist = readExcelFile();
		logger.log(Level.SEVERE, "excel list after reading data exceldatalist size", exceldatalist.size());

		if (exceldatalist.get(0).trim().equalsIgnoreCase("Customer Id")
				&& exceldatalist.get(1).trim().equalsIgnoreCase("Customer Name")
				&& exceldatalist.get(2).trim().equalsIgnoreCase("Contract Id")
				&& exceldatalist.get(3).trim().equalsIgnoreCase("Service Name")
				&& exceldatalist.get(4).trim().equalsIgnoreCase("Customer Branch")
				&& exceldatalist.get(5).trim().equalsIgnoreCase("Asset Id")
				&& exceldatalist.get(6).trim().equalsIgnoreCase("Component Name")
				&& exceldatalist.get(7).trim().equalsIgnoreCase("Mfg No")
				&& exceldatalist.get(8).trim().equalsIgnoreCase("Mfg Date")
				&& exceldatalist.get(9).trim().equalsIgnoreCase("Replacement Date")
				&& exceldatalist.get(10).trim().equalsIgnoreCase("Unit"))

		{
			ArrayList<AssetMovementInfo> list = UpdateAssetDetails(companyId, exceldatalist);
			return list;
		} else {
			return null;
		}
	}

	private ArrayList<AssetMovementInfo> UpdateAssetDetails(long companyID, ArrayList<String> exceldatalist) {

		ArrayList<AssetMovementInfo> assetDetailslist = new ArrayList<AssetMovementInfo>();
		ArrayList<String> errorList = new ArrayList<String>();

		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		format.setTimeZone(TimeZone.getTimeZone("IST"));

		HashSet<Integer> hsAssestId = new HashSet<Integer>();

		for (int i = 11; i < exceldatalist.size(); i += 11) {
			hsAssestId.add(Integer.parseInt(exceldatalist.get(i + 5)));
		}

		for (Integer assetId : hsAssestId) {
			int assetIdcount = 0;
			for (int i = 11; i < exceldatalist.size(); i += 11) {
				if (assetId == Integer.parseInt(exceldatalist.get(i + 5))) {
					assetIdcount++;
				}
			}
			if (assetIdcount > 1) {
				errorList.add(assetId + "");
				errorList.add("Duplicate Asset id found in the excel");
			}
		}

		ArrayList<Integer> arrayassetId = new ArrayList<Integer>(hsAssestId);

		List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyID)
				.filter("assetId IN", arrayassetId).list();
		logger.log(Level.SEVERE, "servicelist size ==" + servicelist.size());

		for (int i = 11; i < exceldatalist.size(); i += 11) {

			AssetMovementInfo assetDetails = new AssetMovementInfo();

			String error = "";

			errorList.add(exceldatalist.get(i + 5));

			if (exceldatalist.get(i).contains(",")) {
				error += " Comma not allowed in Customer Id Column";
				assetDetails.setCount(-1);
			}
			if (exceldatalist.get(i + 1).contains(",")) {
				error += " Comma not allowed in Customer Name Column";
				assetDetails.setCount(-1);
			}
			if (exceldatalist.get(i + 2).contains(",")) {
				error += " Comma not allowed in Contract Id Column";
				assetDetails.setCount(-1);
			}
			if (exceldatalist.get(i + 5).contains(",")) {
				error += " Comma not allowed in Asset Id Column";
				assetDetails.setCount(-1);
			}

			if (exceldatalist.get(i + 8).contains(",")) {
				error += " Comma not allowed in Mfg Date Column";
				assetDetails.setCount(-1);
			}

			if (exceldatalist.get(i + 9).contains(",")) {
				error += " Comma not allowed in Replacement Date Column";
				assetDetails.setCount(-1);
			}

			if (exceldatalist.get(i).equalsIgnoreCase("NA")) {
				error += " Customer Id is Mandatory";
				assetDetails.setCount(-1);
			}

			if (exceldatalist.get(i + 1).equalsIgnoreCase("NA")) {
				error += " Customer Name is Mandatory";
				assetDetails.setCount(-1);
			}
			if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("NA")) {
				error += " Contract Id is Mandatory";
				assetDetails.setCount(-1);
			}

			if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("NA")) {
				error += " Service Name is Mandatory";
				assetDetails.setCount(-1);
			}

			if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("NA")) {
				error += " Customer Branch is Mandatory";
				assetDetails.setCount(-1);
			}

			if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("NA")) {
				error += " Asset Id is Mandatory";
				assetDetails.setCount(-1);
			}

			if (!exceldatalist.get(i + 8).trim().equalsIgnoreCase("NA")) {
				try {
					format.parse(exceldatalist.get(i + 8).trim());
				} catch (ParseException e) {
					error += " Error in Mfg Date :-" + e.getMessage();
					assetDetails.setCount(-1);
					e.printStackTrace();
				}
			}

			if (!exceldatalist.get(i + 9).trim().equalsIgnoreCase("NA")) {
				try {
					format.parse(exceldatalist.get(i + 9).trim());
				} catch (ParseException e) {
					error += " Error in Replacement Date :-" + e.getMessage();
					assetDetails.setCount(-1);
					e.printStackTrace();
				}
			}

			if (!exceldatalist.get(i + 5).trim().equalsIgnoreCase("NA")) {
				int assetId = Integer.parseInt(exceldatalist.get(i + 5).trim());
				Service serviceEntity = getAssetIdService(assetId, servicelist);
				if (serviceEntity == null) {
					error += " Asset Id does not match to any services ";
					assetDetails.setCount(-1);
				} else {
					int customerId = Integer.parseInt(exceldatalist.get(i).trim());
					int contractId = Integer.parseInt(exceldatalist.get(i + 2));

					if (serviceEntity.getPersonInfo().getCount() != customerId) {
						error += " Customer Id does not match in the services.";
						assetDetails.setCount(-1);
					}
					if (!serviceEntity.getPersonInfo().getFullName().trim().equals(exceldatalist.get(i + 1).trim())) {
						error += " Customer Name does not match in the services";
						;
						assetDetails.setCount(-1);
					}
					if (serviceEntity.getContractCount() != contractId) {
						error += " Contract Id does not match in the services.";
						assetDetails.setCount(-1);
					}
					if (!serviceEntity.getProductName().trim().equals(exceldatalist.get(i + 3).trim())) {
						error += " Service Name does not match in the services.";
						assetDetails.setCount(-1);
					}
					if (!serviceEntity.getServiceBranch().trim().equals(exceldatalist.get(i + 4).trim())) {
						error += " Customer Branch does not match in the services.";
						assetDetails.setCount(-1);
					}
				}
			}

			errorList.add(error);
			assetDetailslist.add(assetDetails);

			System.out.println("assetDetails size " + assetDetailslist.size());
			System.out.println("assetDetails " + assetDetailslist);

		}
		CsvWriter.assetDetailsUploadErrorList = errorList;

		return assetDetailslist;
	}

	private Service getAssetIdService(int assetId, List<Service> servicelist) {
		for (Service service : servicelist) {
			if (service.getAssetId() == assetId) {
				return service;
			}
		}
		return null;
	}

	@Override
	public String uploadAssetDetails(long companyId, String taskName) {

		logger.log(Level.SEVERE, "Asset Details upload updation task queue calling");
		try {
			Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue").param("EntityName", taskName)
					.param("companyId", companyId + ""));

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception ==" + e.getMessage());
			return "Failed please try again";
		}
		return "Update Asset Details Upload Process Started";
	}

	public void saveEmployeeData(ArrayList<String> exceldatalist, long companyId, String loggedinuser) {
		logger.log(Level.SEVERE, "Employee Upload Data");

		try {

			ArrayList<Employee> employeelist = new ArrayList<Employee>();

			NumberGeneration ng = new NumberGeneration();

			ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
					.filter("processName", "Employee").filter("status", true).first().now();
			long number = ng.getNumber();
			int count = (int) number;
			// Change i from 40 to 41
			// for (int i = 56; i < exceldatalist.size(); i += 56)
			String createdBy = "";
			if (loggedinuser != null)
				createdBy = loggedinuser;
			HashMap<Integer, String> ctcTemplatemap = new HashMap<Integer, String>();

			HashMap<Integer, String> calendarmap = new HashMap<Integer, String>();
			HashMap<Integer, String> leaveGroupmap = new HashMap<Integer, String>();
			HashMap<Integer, Date> applicableFromDateMap = new HashMap<Integer, Date>();
			HashMap<Integer, Date> applicableToDateMap = new HashMap<Integer, Date>();
			HashMap<Integer, Date> effectiveFromDateMap = new HashMap<Integer, Date>();
			HashMap<Integer, Date> effectiveToDateMap = new HashMap<Integer, Date>();

			HashMap<Integer, String> projectNamemap = new HashMap<Integer, String>();
			HashMap<Integer, String> OTNamemap = new HashMap<Integer, String>();
			HashMap<Integer, String> shiftNamemap = new HashMap<Integer, String>();
			HashMap<Integer, Date> shiftStartDateMap = new HashMap<Integer, Date>();
			HashMap<Integer, Date> shiftEndDateMap = new HashMap<Integer, Date>();

			HashSet<String> hsCtcTemplateName = new HashSet<String>();
			HashSet<String> hsCalendarName = new HashSet<String>();
			HashSet<String> hsProjectName = new HashSet<String>();
			HashSet<String> hsShift = new HashSet<String>();
			HashSet<Integer> empidset = new HashSet<Integer>();
			for (int i = 69; i < exceldatalist.size(); i += 69) {// old 57

				Employee employee = new Employee();

				if (exceldatalist.get(i).trim().equalsIgnoreCase("na")) {
					employee.setCountry("");
				} else {
					employee.setCountry(exceldatalist.get(i));
				}
				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					employee.setDepartMent("");
				} else {
					employee.setDepartMent(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					employee.setEmployeeType("");
				} else {
					employee.setEmployeeType(exceldatalist.get(i + 2));
				}
				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					employee.setFullname("");
				} else {
					employee.setFullname(exceldatalist.get(i + 3));
				}

				try {
					if (!exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
						employee.setDob(fmt.parse(exceldatalist.get(i + 4).trim()));
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}

				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					employee.setBranchName("");
				} else {
					employee.setBranchName(exceldatalist.get(i + 5));
				}
				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					employee.setDesignation("");
				} else {
					employee.setDesignation(exceldatalist.get(i + 6));
				}
				if (exceldatalist.get(i + 7).trim().equalsIgnoreCase("na")) {
					employee.setRoleName("");
				} else {
					employee.setRoleName(exceldatalist.get(i + 7));
				}

				try {
					if (exceldatalist.get(i + 8).trim().equalsIgnoreCase("na")) {

					} else {
						employee.setJoinedAt(fmt.parse(exceldatalist.get(i + 8).trim()));
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}

				if (exceldatalist.get(i + 9).trim().equalsIgnoreCase("na")) {
					employee.setMaritalStatus("");
				} else {
					employee.setMaritalStatus(exceldatalist.get(i + 9));
				}

				if (exceldatalist.get(i + 10).trim().equalsIgnoreCase("na")) {
					employee.setReportsTo("");
				} else {
					employee.setReportsTo(exceldatalist.get(i + 10).trim());
				}

				if (exceldatalist.get(i + 11).trim().equalsIgnoreCase("na")) {
					employee.setAccessCardNo("");
				} else {
					employee.setAccessCardNo(exceldatalist.get(i + 11));
				}
				if (exceldatalist.get(i + 12).trim().equalsIgnoreCase("na")) {
					employee.setIdCardNo("");
				} else {
					employee.setIdCardNo(exceldatalist.get(i + 12));
				}
				if (exceldatalist.get(i + 13).trim().equalsIgnoreCase("na")) {
					employee.setBloodGroup("");
				} else {
					employee.setBloodGroup(exceldatalist.get(i + 13));
				}
				if (exceldatalist.get(i + 14).trim().equalsIgnoreCase("Yes")) {
					employee.setStatus(true);
				} else {
					employee.setStatus(false);
				}
				if (exceldatalist.get(i + 15).trim().equalsIgnoreCase("na")) {
					employee.setEmployeeBankName("");
					;
				} else {
					employee.setEmployeeBankName(exceldatalist.get(i + 15));
				}
				if (exceldatalist.get(i + 16).trim().equalsIgnoreCase("na")) {
					employee.setBankBranch("");
				} else {
					employee.setBankBranch(exceldatalist.get(i + 16));
				}
				if (exceldatalist.get(i + 17).trim().equalsIgnoreCase("na")) {
					employee.setIfscCode("");
				} else {
					employee.setIfscCode(exceldatalist.get(i + 17));
				}
				if (exceldatalist.get(i + 18).trim().equalsIgnoreCase("na")) {
					employee.setEmployeeBankAccountNo("");
				} else {
					employee.setEmployeeBankAccountNo(exceldatalist.get(i + 18));
				}
				if (exceldatalist.get(i + 19).trim().equalsIgnoreCase("na")) {
					employee.setEmployeeESICcode("");
				} else {
					employee.setEmployeeESICcode(exceldatalist.get(i + 19));
				}
				if (exceldatalist.get(i + 20).trim().equalsIgnoreCase("na")) {
					employee.setEmployeePanNo("");
				} else {
					employee.setEmployeePanNo(exceldatalist.get(i + 20));
				}
				if (exceldatalist.get(i + 21).trim().equalsIgnoreCase("na")) {
					employee.setPPFNumber("");
				} else {
					employee.setPPFNumber(exceldatalist.get(i + 21));
				}
				if (exceldatalist.get(i + 22).trim().equalsIgnoreCase("na")) {
					employee.setAadharNumber(0l);
				} else {
					employee.setAadharNumber(Long.parseLong(exceldatalist.get(i + 22).trim()));
				}
				if (exceldatalist.get(i + 23).trim().equalsIgnoreCase("na")) {
					employee.setUANno("");
				} else {
					employee.setUANno(exceldatalist.get(i + 23));
				}

				if (exceldatalist.get(i + 24).trim().equalsIgnoreCase("na")) {
					// Long landlineno=exceldatalist.get(i+23);
					employee.setLandline(0l);
				} else {
					employee.setLandline(Long.parseLong(exceldatalist.get(i + 24)));
				}
				if (exceldatalist.get(i + 25).trim().equalsIgnoreCase("na")) {
					employee.setCellNumber1(0l);
				} else {
					employee.setCellNumber1(Long.parseLong(exceldatalist.get(i + 25).trim()));
				}

				if (exceldatalist.get(i + 26).trim().equalsIgnoreCase("na")) {
					employee.setCellNumber2(0l);
				} else {
					employee.setCellNumber2(Long.parseLong(exceldatalist.get(i + 26).trim()));
				}
				if (exceldatalist.get(i + 27).trim().equalsIgnoreCase("na")) {
					employee.setEmail("");
				} else {
					employee.setEmail(exceldatalist.get(i + 27));
				}

				Address address = new Address();

				if (exceldatalist.get(i + 28).trim().equalsIgnoreCase("na")) {

					address.setAddrLine1("");
				} else {
					address.setAddrLine1(exceldatalist.get(i + 28));
				}
				if (exceldatalist.get(i + 29).trim().equalsIgnoreCase("na")) {
					address.setAddrLine2("");
				} else {
					address.setAddrLine2(exceldatalist.get(i + 29));
				}
				if (exceldatalist.get(i + 30).trim().equalsIgnoreCase("na")) {
					address.setLandmark("");
				} else {
					address.setLandmark(exceldatalist.get(i + 30));
				}
				if (exceldatalist.get(i + 31).trim().equalsIgnoreCase("na")) {
					address.setCountry("");
				} else {
					address.setCountry(exceldatalist.get(i + 31));

				}
				if (exceldatalist.get(i + 32).trim().equalsIgnoreCase("na")) {
					address.setState("");
				} else {
					address.setState(exceldatalist.get(i + 32));
				}
				if (exceldatalist.get(i + 33).trim().equalsIgnoreCase("na")) {
					address.setCity("");
				} else {
					address.setCity(exceldatalist.get(i + 33));
				}
				if (exceldatalist.get(i + 34).trim().equalsIgnoreCase("na")) {
					address.setLocality("");
				} else {
					address.setLocality(exceldatalist.get(i + 34));
				}
				if (exceldatalist.get(i + 35).trim().equalsIgnoreCase("na")) {
					address.setPin(0l);
				} else {
					address.setPin(Long.parseLong(exceldatalist.get(i + 35).trim()));
				}
				employee.setAddress(address);

				Address address1 = new Address();

				if (exceldatalist.get(i + 36).trim().equalsIgnoreCase("na")) {

					address1.setAddrLine1("");
				} else {
					address1.setAddrLine1(exceldatalist.get(i + 36));
				}
				if (exceldatalist.get(i + 37).trim().equalsIgnoreCase("na")) {
					address1.setAddrLine2("");
				} else {
					address1.setAddrLine2(exceldatalist.get(i + 37));
				}
				if (exceldatalist.get(i + 38).trim().equalsIgnoreCase("na")) {
					address1.setLandmark("");
				} else {
					address1.setLandmark(exceldatalist.get(i + 38));
				}
				if (exceldatalist.get(i + 39).trim().equalsIgnoreCase("na")) {
					address1.setCountry("");
				} else {
					address1.setCountry(exceldatalist.get(i + 39));

				}
				if (exceldatalist.get(i + 40).trim().equalsIgnoreCase("na")) {
					address1.setState("");
				} else {
					address1.setState(exceldatalist.get(i + 40));
				}
				if (exceldatalist.get(i + 41).trim().equalsIgnoreCase("na")) {
					address1.setCity("");
				} else {
					address1.setCity(exceldatalist.get(i + 41));
				}
				if (exceldatalist.get(i + 42).trim().equalsIgnoreCase("na")) {
					address1.setLocality("");
				} else {
					address1.setLocality(exceldatalist.get(i + 42));
				}
				if (exceldatalist.get(i + 43).trim().equalsIgnoreCase("na")) {
					address1.setPin(0l);
				} else {
					address1.setPin(Long.parseLong(exceldatalist.get(i + 43).trim()));
				}
				employee.setSecondaryAddress(address1);

				// /********Passport**************//
				PassportInformation passport = new PassportInformation();
				if (exceldatalist.get(i + 44).trim().equalsIgnoreCase("na")) {
					passport.setPassportNumber("");
				} else {
					passport.setPassportNumber(exceldatalist.get(i + 44).trim());
				}
				if (exceldatalist.get(i + 45).trim().equalsIgnoreCase("na")) {
					passport.setIssuedAt("");
				} else {
					passport.setIssuedAt(exceldatalist.get(i + 45).trim());
				}

				try {
					if (exceldatalist.get(i + 46).trim().equalsIgnoreCase("na")) {

					} else {
						passport.setIssueDate(fmt.parse(exceldatalist.get(i + 46).trim()));
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}

				try {
					if (exceldatalist.get(i + 47).trim().equalsIgnoreCase("na")) {

					} else {
						passport.setExpiryDate(fmt.parse(exceldatalist.get(i + 47).trim()));
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}

				employee.setPassPortInformation(passport);

				// ///************Social
				// Info********************////////////////////////
				SocialInformation socialinfo = new SocialInformation();

				if (exceldatalist.get(i + 48).trim().equalsIgnoreCase("na")) {
					socialinfo.setGooglePlusId("");
				} else {
					socialinfo.setGooglePlusId(exceldatalist.get(i + 48));
				}
				if (exceldatalist.get(i + 49).trim().equalsIgnoreCase("na")) {
					socialinfo.setFaceBookId("");
				} else {
					socialinfo.setFaceBookId(exceldatalist.get(i + 49));
				}
				if (exceldatalist.get(i + 50).trim().equalsIgnoreCase("na")) {
					socialinfo.setTwitterId("");
				} else {
					socialinfo.setTwitterId(exceldatalist.get(i + 50));
				}
				employee.setSocialInfo(socialinfo);
				/**
				 * Date : 04-05-2018 By ANIL
				 */
				if (exceldatalist.get(i + 51).trim().equalsIgnoreCase("na")) {
					employee.setGender("");
				} else {
					employee.setGender(exceldatalist.get(i + 51));
				}
				/**
				 * Date : 15-05-2018 By ANIL
				 */
				if (exceldatalist.get(i + 52).trim().equalsIgnoreCase("na")) {
					employee.setNumberRange("");
				} else {
					employee.setNumberRange(exceldatalist.get(i + 52));
				}
				/**
				 * Date 15-5-2019 By Amol
				 */
				if (exceldatalist.get(i + 53).trim().equalsIgnoreCase("na")) {
					employee.setEmpGrp("");
				} else {
					employee.setEmpGrp(exceldatalist.get(i + 53));
				}

				if (exceldatalist.get(i + 54).trim().equalsIgnoreCase("na")) {
					employee.setSalaryAmt(0d);
				} else {
					employee.setSalaryAmt(Double.parseDouble(exceldatalist.get(i + 54).trim()));
				}
				if (exceldatalist.get(i + 55).equalsIgnoreCase("na")) {
					employee.setHusbandName("");
				} else {
					employee.setHusbandName(exceldatalist.get(i + 55));
				}

				/**
				 * Added By Priyanka Date : 06-04-2021 Des : Need Reliever colm in upload and
				 * update employee req by Sunrise issue raise by Rahul.
				 */

				if (exceldatalist.get(i + 56).trim().equalsIgnoreCase("NA")) {
					// continue;
				} else {
					if (exceldatalist.get(i + 56).trim().equalsIgnoreCase("NO")) {
						employee.setReliever(false);
					} else {
						employee.setReliever(true);
					}
				}
				if (!exceldatalist.get(i + 64).trim().equalsIgnoreCase("NA")) {
					employee.setProjectName(exceldatalist.get(i + 64).trim());
				}
				if (!exceldatalist.get(i + 66).trim().equalsIgnoreCase("NA")) {
					employee.setShiftName(exceldatalist.get(i + 66));
				}

				employee.setCount(++count);
				employee.setCompanyId(companyId);
				employeelist.add(employee);

				if (!exceldatalist.get(i + 57).trim().equalsIgnoreCase("NA")) {
					hsCtcTemplateName.add(exceldatalist.get(i + 57).trim());
					ctcTemplatemap.put(employee.getCount(), exceldatalist.get(i + 57).trim());
				}
				if (!exceldatalist.get(i + 58).trim().equalsIgnoreCase("NA")) {
					applicableFromDateMap.put(employee.getCount(), fmt.parse(exceldatalist.get(i + 58).trim()));
				}
				if (!exceldatalist.get(i + 59).trim().equalsIgnoreCase("NA")) {
					applicableToDateMap.put(employee.getCount(), fmt.parse(exceldatalist.get(i + 59).trim()));
				}
				if (!exceldatalist.get(i + 60).trim().equalsIgnoreCase("NA")) {
					effectiveFromDateMap.put(employee.getCount(), fmt.parse(exceldatalist.get(i + 60).trim()));
				}
				if (!exceldatalist.get(i + 61).trim().equalsIgnoreCase("NA")) {
					effectiveToDateMap.put(employee.getCount(), fmt.parse(exceldatalist.get(i + 61).trim()));
				}

				if (!exceldatalist.get(i + 62).trim().equalsIgnoreCase("NA")) {
					hsCalendarName.add(exceldatalist.get(i + 62).trim());
//				logger.log(Level.SEVERE, "cal name in excel="+exceldatalist.get(i+62).trim());
					calendarmap.put(employee.getCount(), exceldatalist.get(i + 62).trim());
				}

				if (!exceldatalist.get(i + 63).trim().equalsIgnoreCase("NA")) {
					leaveGroupmap.put(employee.getCount(), exceldatalist.get(i + 63).trim());
				}
				if (!exceldatalist.get(i + 64).trim().equalsIgnoreCase("NA")) {
					hsProjectName.add(exceldatalist.get(i + 64).trim());
					projectNamemap.put(employee.getCount(), exceldatalist.get(i + 64).trim());
				}

				if (!exceldatalist.get(i + 65).trim().equalsIgnoreCase("NA")) {
					OTNamemap.put(employee.getCount(), exceldatalist.get(i + 65).trim());
				}

				if (!exceldatalist.get(i + 66).trim().equalsIgnoreCase("NA")) {
					shiftNamemap.put(employee.getCount(), exceldatalist.get(i + 66).trim());
				}

				if (!exceldatalist.get(i + 67).trim().equalsIgnoreCase("NA")) {
					shiftStartDateMap.put(employee.getCount(), fmt.parse(exceldatalist.get(i + 67).trim()));
				}

				if (!exceldatalist.get(i + 68).trim().equalsIgnoreCase("NA")) {
					shiftEndDateMap.put(employee.getCount(), fmt.parse(exceldatalist.get(i + 68).trim()));
				}
				empidset.add(employee.getCount());
			}

			ofy().save().entities(employeelist).now();

			ng.setNumber(count);

			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(ng);

			// Ashwini Patil Date:13-05-2023

			List<CTCTemplate> ctcTemplateList = null;
			if (hsCtcTemplateName.size() > 0) {
				ArrayList<String> ctclist = new ArrayList<String>(hsCtcTemplateName);
				ctcTemplateList = ofy().load().type(CTCTemplate.class).filter("ctcTemplateName IN", ctclist)
						.filter("companyId", companyId).list();
			}

			List<com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar> calendarList = null;
			if (hsCalendarName.size() > 0) {
				ArrayList<String> clist = new ArrayList<String>(hsCalendarName);
				calendarList = ofy().load()
						.type(com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar.class)
						.filter("calName IN", clist).filter("companyId", companyId).list();
			}

			List<LeaveGroup> leaveGroupList = ofy().load().type(LeaveGroup.class).filter("companyId", companyId).list();

			List<HrProject> projectList = null;
			if (hsProjectName.size() > 0) {
				ArrayList<String> list = new ArrayList<String>(hsProjectName);
				projectList = ofy().load().type(HrProject.class).filter("projectName IN", list)
						.filter("companyId", companyId).list();
			}

			List<Overtime> otList = ofy().load().type(Overtime.class).filter("companyId", companyId).list();

			List<Shift> shiftList = ofy().load().type(Shift.class).filter("companyId", companyId).list();

			List<EmployeeInfo> empinfolist = ofy().load().type(EmployeeInfo.class).filter("companyId", companyId)
					.filter("empCount IN", empidset).list();

			logger.log(Level.SEVERE, "empidset size=" + empidset.size());
			if (empinfolist != null)
				logger.log(Level.SEVERE, "empinfolist size=" + empinfolist.size());
			else
				logger.log(Level.SEVERE, "empinfolist null");

			CtcAllocationServiceImpl ctcService = new CtcAllocationServiceImpl();
			for (Employee e : employeelist) {
				// ctc allocation process
				if (calendarmap.containsKey(e.getCount()) && ctcTemplatemap.containsKey(e.getCount())) {
					ArrayList<AllocationResult> allocationArray = new ArrayList<AllocationResult>();
					AllocationResult result = new AllocationResult();
					result.setEmpCount(e.getCount());
					result.setFullName(e.getFullname());
					result.setCellNumber(e.getCellNumber1());
					result.setEmployeeType(e.getEmployeeType());
					result.setEmployeerole(e.getRoleName());
					result.setDesignation(e.getDesignation());
					result.setBranch(e.getBranchName());
					result.setDepartment(e.getDepartMent());
					result.setCalendarName(calendarmap.get(e.getCount()));
					allocationArray.add(result);
					CTCTemplate ctctemplate = null;
					for (CTCTemplate template : ctcTemplateList) {
						if (template.getCtcTemplateName().equalsIgnoreCase(ctcTemplatemap.get(e.getCount())))
							ctctemplate = template;
					}
					ctcService.allocateCtc(allocationArray, ctctemplate, applicableFromDateMap.get(e.getCount()),
							applicableToDateMap.get(e.getCount()), effectiveFromDateMap.get(e.getCount()), createdBy,
							effectiveToDateMap.get(e.getCount()), projectNamemap.get(e.getCount()));

				}

				// calendar and leave allocation process
				if (leaveGroupmap.containsKey(e.getCount()) && calendarmap.containsKey(e.getCount())) {
					ArrayList<LeaveBalance> allocateLeavesArray = new ArrayList<LeaveBalance>();
					EmployeeInfo empinfo = null;
					for (EmployeeInfo emp : empinfolist) {
						if (emp.getEmpCount() == e.getCount()) {
							empinfo = emp;
							logger.log(Level.SEVERE, "info matched");
							break;
						}
					}
					LeaveBalance leavebal = new LeaveBalance();
					com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar cal = null;
//				logger.log(Level.SEVERE, "calendar value="+calendarmap.get(e.getCount()));
					for (com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar c : calendarList) {
//					logger.log(Level.SEVERE, "calendar name="+c.getCalName());
						if (c.getCalName().equalsIgnoreCase(calendarmap.get(e.getCount()))) {
							cal = c;
//						logger.log(Level.SEVERE, "calendar matched");
							break;
						}
					}

					if (empinfo == null)
						logger.log(Level.SEVERE, "empinfo null");
					if (cal == null)
						logger.log(Level.SEVERE, "cal null");
					empinfo.setLeaveCalendar(cal);
					empinfo.setLeaveGroupName(leaveGroupmap.get(e.getCount()));
					leavebal.setEmpInfo(empinfo);
					LeaveGroup leaveGroup = null;
					for (LeaveGroup lg : leaveGroupList) {
						if (lg.getGroupName().equalsIgnoreCase(leaveGroupmap.get(e.getCount())))
							leaveGroup = lg;
					}
					leavebal.setLeaveGroup(leaveGroup);
					leavebal.setValidfrom(cal.getCalStartDate());
					leavebal.setValidtill(cal.getCalEndDate());
					leavebal.setCompanyId(companyId);
					allocateLeavesArray.add(leavebal);
					logger.log(Level.SEVERE, "allocateLeavesArray size=" + allocateLeavesArray.size());
					LeaveAllocationServiceImpl leaveAllocationservice = new LeaveAllocationServiceImpl();
					leaveAllocationservice.allocateLeave(allocateLeavesArray);
				}

				// Project allocation process
				if (projectNamemap.containsKey(e.getCount())) {

					HrProject project = null;
					for (HrProject p : projectList) {
						if (p.getProjectName().equalsIgnoreCase(projectNamemap.get(e.getCount()))) {
							project = p;
							logger.log(Level.SEVERE, "project matched");
							break;
						}
					}
					List<Overtime> otlist = new ArrayList<Overtime>();
					Overtime overtime = new Overtime();
					if (project.getOtList() != null) {
						logger.log(Level.SEVERE, "project otlist size=" + project.getOtList().size());
						for (Overtime o : project.getOtList()) {
							if (o.getEmpId() != e.getCount())
								otlist.add(o);
						}
						logger.log(Level.SEVERE, "new otlist size=" + otlist.size());

					}

					overtime.setEmpId(e.getCount());
					overtime.setEmpName(e.getFullname());
					overtime.setEmpDesignation(e.getDesignation());
					if (OTNamemap.containsKey(e.getCount()))
						overtime.setOvertime(OTNamemap.get(e.getCount()));

					if (shiftNamemap.containsKey(e.getCount()))
						overtime.setShift(shiftNamemap.get(e.getCount()));

					if (shiftStartDateMap.containsKey(e.getCount()))
						overtime.setStartDate(shiftStartDateMap.get(e.getCount()));

					if (shiftEndDateMap.containsKey(e.getCount()))
						overtime.setEndDate(shiftEndDateMap.get(e.getCount()));

					otlist.add(overtime);
					logger.log(Level.SEVERE,
							"project otlist size after adding" + e.getFullname() + " =" + otlist.size());
					project.setOtList(otlist);
					ofy().save().entity(project).now();

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private String callTaskQueueForEmployeeUpload(long companyId, String loggedinuser) {

		logger.log(Level.SEVERE, "AEmployee Upload upload updation task queue calling");
		try {
			Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue")
					.param("EntityName", "Employee Upload").param("companyId", companyId + "")
					.param("loggedinuser", loggedinuser));

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception ==" + e.getMessage());
			return "Failed please try again";
		}
		return "Employee Upload Process Started";
	}

	private String updateEmployeeData(long companyId) {

		logger.log(Level.SEVERE, "Employee Update upload updation task queue calling");
		try {

			Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue")
					.param("EntityName", "Update Employee").param("companyId", companyId + ""));

		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Exception ==" + e.getMessage());
			return "Failed please try again";
		}
		return "Update Employee Upload Process Started";

	}

	public void updateEmployeeDataWithTaskQueue(long companyId, ArrayList<String> exceldatalist) {

		ArrayList<Employee> employeelist = new ArrayList<Employee>();
		logger.log(Level.SEVERE, "inside read value merthod ");
//		List<Employee> listofemployee = ofy().load().type(Employee.class)
//				.filter("companyId", companyId).list();

		HashSet<Integer> hsemployeeid = new HashSet<Integer>();
		// for (int p = 49; p < exceldatalist.size(); p += 49)
		for (int p = 51; p < exceldatalist.size(); p += 51) {
			if (!exceldatalist.get(p).trim().equalsIgnoreCase("0")
					|| !exceldatalist.get(p).trim().equalsIgnoreCase("NA")) {
				hsemployeeid.add(Integer.parseInt(exceldatalist.get(p).trim()));
			}
		}

		ArrayList<Integer> arrayEmployeeid = new ArrayList<Integer>(hsemployeeid);
		List<Employee> listofemployee = ofy().load().type(Employee.class).filter("companyId", companyId)
				.filter("count IN", arrayEmployeeid).list();
		logger.log(Level.SEVERE, "listofemployee" + listofemployee.size());

		ArrayList<Employee> sitelocationUpdationlist = new ArrayList<Employee>();
		// for (int p = 49; p < exceldatalist.size(); p += 49)
		for (int p = 51; p < exceldatalist.size(); p += 51) {

			int employeeCount = Integer.parseInt(exceldatalist.get(p).trim());

			for (int i = 0; i < listofemployee.size(); i++) {

				if (listofemployee.get(i).getCount() == employeeCount) {
					Employee employee = listofemployee.get(i);

					if (!exceldatalist.get(p + 1).trim().equalsIgnoreCase("NA")) {
						String empName = (exceldatalist.get(p + 1).trim());
						employee.setFullname(empName);
					}

					if (!exceldatalist.get(p + 2).trim().equalsIgnoreCase("NA")) {
						String branch = (exceldatalist.get(p + 2).trim());
						employee.setBranchName(branch);
					}

					if (!exceldatalist.get(p + 3).trim().equalsIgnoreCase("NA")) {

						double salaryOfEmployee = Double.parseDouble(exceldatalist.get(p + 3).trim());
						employee.setSalaryAmt(salaryOfEmployee);

					}

					if (!exceldatalist.get(p + 4).trim().equalsIgnoreCase("NA")) {
						double workingHrs = Double.parseDouble(exceldatalist.get(p + 4).trim());
						employee.setMonthlyWorkingHrs(workingHrs);
					}

					if (!exceldatalist.get(p + 5).trim().equalsIgnoreCase("NA")) {
						String designation = (exceldatalist.get(p + 5).trim());
						employee.setDesignation(designation);

					}
					if (!exceldatalist.get(p + 6).trim().equalsIgnoreCase("NA")) {
						String role = (exceldatalist.get(p + 6).trim());
						employee.setRoleName(role);
					}

					if (!exceldatalist.get(p + 7).trim().equalsIgnoreCase("NA")) {

						Date joinDate = null;
						try {
							joinDate = fmt.parse((exceldatalist.get(p + 7).trim()));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						employee.setJoinedAt(joinDate);

					}

					if (!exceldatalist.get(p + 8).trim().equalsIgnoreCase("NA")) {
						String maritalStatus = (exceldatalist.get(p + 8).trim());
						employee.setMaritalStatus(maritalStatus);
					}

					if (!exceldatalist.get(p + 9).trim().equalsIgnoreCase("NA")) {
						String reportTo = (exceldatalist.get(p + 9).trim());
						employee.setReportsTo(reportTo);
					}

					if (!exceldatalist.get(p + 10).trim().equalsIgnoreCase("NA")) {
						String accessCardNo = (exceldatalist.get(p + 10).trim());
						employee.setAccessCardNo(accessCardNo);
					}

					if (!exceldatalist.get(p + 11).trim().equalsIgnoreCase("NA")) {
						String idCardNo = (exceldatalist.get(p + 11).trim());
						employee.setIdCardNo(idCardNo);
					}

					if (!exceldatalist.get(p + 12).trim().equalsIgnoreCase("NA")) {
						String bloodGroup = (exceldatalist.get(p + 12).trim());
						employee.setBloodGroup(bloodGroup);
					}

					if (!exceldatalist.get(p + 13).trim().equalsIgnoreCase("NA")) {
						boolean status = Boolean.parseBoolean((exceldatalist.get(p + 13).trim()));
						employee.setStatus(status);
					}

					if (!exceldatalist.get(p + 14).trim().equalsIgnoreCase("NA")) {
						String bankName = (exceldatalist.get(p + 14).trim());
						employee.setEmployeeBankName(bankName);
					}

					if (!exceldatalist.get(p + 15).trim().equalsIgnoreCase("NA")) {
						String bankBranch = (exceldatalist.get(p + 15).trim());
						employee.setBankBranch(bankBranch);
					}

					if (!exceldatalist.get(p + 16).trim().equalsIgnoreCase("NA")) {
						String ifsc = (exceldatalist.get(p + 16).trim());
						employee.setIfscCode(ifsc);
					}

					if (!exceldatalist.get(p + 17).trim().equalsIgnoreCase("NA")) {
						String bankAccNo = (exceldatalist.get(p + 17).trim());
						employee.setEmployeeBankAccountNo(bankAccNo);
					}

					if (!exceldatalist.get(p + 18).trim().equalsIgnoreCase("NA")) {
						String esic = (exceldatalist.get(p + 18).trim());
						employee.setEmployeeESICcode(esic);

					}

					if (!exceldatalist.get(p + 19).trim().equalsIgnoreCase("NA")) {
						String employeePanNo = (exceldatalist.get(p + 19).trim());
						employee.setEmployeePanNo(employeePanNo);
					}

					if (!exceldatalist.get(p + 20).trim().equalsIgnoreCase("NA")) {
						String ppfNO = (exceldatalist.get(p + 20).trim());
						employee.setPPFNumber(ppfNO);
					}

					if (!exceldatalist.get(p + 21).trim().equalsIgnoreCase("NA")) {
						long aadharNo = Long.parseLong((exceldatalist.get(p + 21).trim()));
						employee.setAadharNumber(aadharNo);
					}

					if (!exceldatalist.get(p + 22).trim().equalsIgnoreCase("NA")) {
						String uanNo = (exceldatalist.get(p + 22).trim());
						employee.setUANno(uanNo);
					}

					if (!exceldatalist.get(p + 23).trim().equalsIgnoreCase("NA")) {
						long landLine = Long.parseLong((exceldatalist.get(p + 23).trim()));
						employee.setLandline(landLine);
					}

					if (!exceldatalist.get(p + 24).trim().equalsIgnoreCase("NA")) {
						long cell1 = Long.parseLong(exceldatalist.get(p + 24).trim());
						employee.setCellNumber1(cell1);
					}

					if (!exceldatalist.get(p + 25).trim().equalsIgnoreCase("NA")) {
						long cell2 = Long.parseLong(exceldatalist.get(p + 25).trim());
						employee.setCellNumber2(cell2);
					}

					if (!exceldatalist.get(p + 26).trim().equalsIgnoreCase("NA")) {
						String email = (exceldatalist.get(p + 26).trim());
						employee.setEmail(email);
					}

					if (!exceldatalist.get(p + 27).trim().equalsIgnoreCase("NA")) {
						String primaryAddress1 = (exceldatalist.get(p + 27).trim());
						employee.getAddress().setAddrLine1(primaryAddress1);
					}

					if (!exceldatalist.get(p + 28).trim().equalsIgnoreCase("NA")) {
						String primaryAddress2 = (exceldatalist.get(p + 28).trim());
						employee.getAddress().setAddrLine2(primaryAddress2);
					}

					if (!exceldatalist.get(p + 29).trim().equalsIgnoreCase("NA")) {
						String primaryLandmark = (exceldatalist.get(p + 29).trim());
						employee.getAddress().setLandmark(primaryLandmark);
					}

					if (!exceldatalist.get(p + 30).trim().equalsIgnoreCase("NA")) {
						String primaryCountry = (exceldatalist.get(p + 30).trim());
						employee.getAddress().setCountry(primaryCountry);
					}

					if (!exceldatalist.get(p + 31).trim().equalsIgnoreCase("NA")) {
						String primaryState = (exceldatalist.get(p + 31).trim());
						employee.getAddress().setState(primaryState);
					}

					if (!exceldatalist.get(p + 32).trim().equalsIgnoreCase("NA")) {
						String primaryCity = (exceldatalist.get(p + 32).trim());
						employee.getAddress().setCity(primaryCity);
					}

					if (!exceldatalist.get(p + 33).trim().equalsIgnoreCase("NA")) {
						String primaryLocality = (exceldatalist.get(p + 33).trim());
						employee.getAddress().setLocality(primaryLocality);
					}

					if (!exceldatalist.get(p + 34).trim().equalsIgnoreCase("NA")) {
						long primaryPin = Long.parseLong((exceldatalist.get(p + 34).trim()));
						employee.getAddress().setPin(primaryPin);
					}

					if (!exceldatalist.get(p + 35).trim().equalsIgnoreCase("NA")) {
						String secondaryAddress1 = (exceldatalist.get(p + 35).trim());
						employee.getSecondaryAddress().setAddrLine1(secondaryAddress1);
					}

					if (!exceldatalist.get(p + 36).trim().equalsIgnoreCase("NA")) {
						String secondaryAddress2 = (exceldatalist.get(p + 36).trim());
						employee.getSecondaryAddress().setAddrLine2(secondaryAddress2);
					}

					if (!exceldatalist.get(p + 37).trim().equalsIgnoreCase("NA")) {
						String secondaryLandmark = (exceldatalist.get(p + 37).trim());
						employee.getSecondaryAddress().setLandmark(secondaryLandmark);
					}

					if (!exceldatalist.get(p + 38).trim().equalsIgnoreCase("NA")) {
						String secondaryCountry = (exceldatalist.get(p + 38).trim());
						employee.getSecondaryAddress().setCountry(secondaryCountry);
					}

					if (!exceldatalist.get(p + 39).trim().equalsIgnoreCase("NA")) {
						String secondaryState = (exceldatalist.get(p + 39).trim());
						employee.getSecondaryAddress().setState(secondaryState);
					}

					if (!exceldatalist.get(p + 40).trim().equalsIgnoreCase("NA")) {
						String secondaryCity = (exceldatalist.get(p + 40).trim());
						employee.getSecondaryAddress().setCity(secondaryCity);
					}

					if (!exceldatalist.get(p + 41).trim().equalsIgnoreCase("NA")) {
						String secondaryLocality = (exceldatalist.get(p + 41).trim());
						employee.getSecondaryAddress().setLocality(secondaryLocality);
					}

					if (!exceldatalist.get(p + 42).trim().equalsIgnoreCase("NA")) {
						long secondaryPin = Long.parseLong((exceldatalist.get(p + 42).trim()));
						employee.getSecondaryAddress().setPin(secondaryPin);
					}

					if (!exceldatalist.get(p + 43).trim().equalsIgnoreCase("NA")) {

						String gender = (exceldatalist.get(p + 43).trim());
						employee.setGender(gender);

					}

					if (!exceldatalist.get(p + 44).trim().equalsIgnoreCase("NA")) {
						String numberRange = (exceldatalist.get(p + 44).trim());
						employee.setNumberRange(numberRange);
					}

					if (!exceldatalist.get(p + 45).trim().equalsIgnoreCase("NA")) {
						String employeeGroup = (exceldatalist.get(p + 45).trim());
						employee.setEmpGrp(employeeGroup);

					}

					if (!exceldatalist.get(p + 46).trim().equalsIgnoreCase("NA")) {
						String fatherName = (exceldatalist.get(p + 46).trim());
						employee.setHusbandName(fatherName);
					}

					boolean projectNameChangeFlag = false;
					if (!exceldatalist.get(p + 47).trim().equalsIgnoreCase("NA")) {
						if (employee.getProjectName() != null && !employee.getProjectName().equals("")) {
							if (!employee.getProjectName().trim().equals(exceldatalist.get(p + 47).trim())) {
								projectNameChangeFlag = true;
							}
						}
						String projectName = (exceldatalist.get(p + 47).trim());
						employee.setProjectName(projectName);
					}

					boolean hrptojectupdation = false;

					if (!exceldatalist.get(p + 48).trim().equalsIgnoreCase("NA")) {
						String siteLocation = (exceldatalist.get(p + 48).trim());
						if (employee.getProjectName() != null && !employee.getProjectName().equals("")) {
							hrptojectupdation = true;
						}
						employee.setSiteLocation(siteLocation);
					} else {
						if (projectNameChangeFlag) {
							employee.setSiteLocation("");
						}
					}

					/**
					 * Added By Priyanka Date : 06-04-2021 Des : Need Reliever colm in upload and
					 * update employee req by Sunrise issue raise by Rahul.
					 */

					if (exceldatalist.get(p + 49).trim().equalsIgnoreCase("NA")) {
						// continue;
					} else {
						if (exceldatalist.get(p + 49).trim().equalsIgnoreCase("NO")) {
							employee.setReliever(false);
						} else {
							employee.setReliever(true);
						}
					}

					// Ashiwini Patil Date:29-09-2023
					if (!exceldatalist.get(p + 50).trim().equalsIgnoreCase("NA")) {

						Date brithDate = null;
						try {
							brithDate = fmt.parse((exceldatalist.get(p + 50).trim()));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						employee.setDob(brithDate);

					}

					employeelist.add(employee);
					if (hrptojectupdation) {
						sitelocationUpdationlist.add(employee);
						logger.log(Level.SEVERE, "employee with Id :" + employee.getCount());

					}

				}
//				else {
//					logger.log(Level.SEVERE, "employee with Id :"
//							+ exceldatalist.get(p).trim() + "not found");
//				}
			}

		}

		logger.log(Level.SEVERE, "Employee Update save started here");

		ofy().save().entities(employeelist);

		logger.log(Level.SEVERE, "update employee succussfully ");

		if (sitelocationUpdationlist.size() > 0) {
			logger.log(Level.SEVERE, "sitelocationUpdationlist size" + sitelocationUpdationlist.size());

			for (Employee emp : sitelocationUpdationlist) {
				if (emp.getProjectName() != null && !emp.getProjectName().equals("")) {

					Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
							.param("OperationName", "Update Employee Site Location").param("companyId", companyId + "")
							.param("employeeId", emp.getCount() + "").param("projectName", emp.getProjectName())
							.param("empSiteLocation", emp.getSiteLocation()));

				}
			}

		}

	}

	private ArrayList<Integer> employeeDeatilsSave(long companyId, ArrayList<String> exceldatalist,
			String loggedinuser) {

		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		ArrayList<String> errorlist = new ArrayList<String>();
		ServerAppUtility serverapputility = new ServerAppUtility();

		/*
		 * Ashwini Patil Date:27-03-2023 We were loading all employess to check if same
		 * addharnumber exist in system. But since sunrise client has mote than 6000
		 * employess, this process gives deadline excpetion. As per discussion with
		 * nitin sir we will fire single single ofy queries to check whether the number
		 * already exist in system.
		 * 
		 */
//		List<Employee> listofemployee = ofy().load().type(Employee.class)
//				.filter("companyId", companyId).list();

		HashMap<String, Integer> hashduplicateEmplpoyee = new HashMap<String, Integer>();
		ArrayList<Long> aadharnumberlist = new ArrayList<Long>();
		ArrayList<String> employeeNameList = new ArrayList<String>();

		HashSet<String> hsdepartmentName = new HashSet<String>();
		HashSet<String> hsBranchName = new HashSet<String>();
		HashSet<City> hsCity = new HashSet<City>();

		HashSet<String> hsCtcTemplateName = new HashSet<String>();
		HashSet<String> hsCalendarName = new HashSet<String>();
//		HashSet<String> hsLeaveGroup = new HashSet<String>();
		HashSet<String> hsProjectName = new HashSet<String>();
//		HashSet<String> hsOT = new HashSet<String>();
		HashSet<String> hsShift = new HashSet<String>();

		if (exceldatalist.size() == 69) {
			errorlist.add("File");
			errorlist.add("No records found!");
		}

		if (exceldatalist.size() % 69 != 0) {
			errorlist.add("File");
			errorlist.add(
					"Blank cells were found! Put NA instead of blank for the non-mandatory field, and put the correct existing value that is already in your ERP system instead of blank for the mandatory field.");
		}

		// for (int i = 56; i < exceldatalist.size(); i += 56)
		for (int i = 69; i < exceldatalist.size(); i += 69) {// old 57

			if (!exceldatalist.get(i + 1).trim().equalsIgnoreCase("NA")) {
				hsdepartmentName.add(exceldatalist.get(i + 1).trim());
			}
			if (!exceldatalist.get(i + 5).trim().equalsIgnoreCase("NA")) {
				hsBranchName.add(exceldatalist.get(i + 5).trim());
			}

			if (!exceldatalist.get(i + 33).trim().equalsIgnoreCase("NA")) {
				City city = new City();
				city.setCityName(exceldatalist.get(i + 33).trim());
				if (!exceldatalist.get(i + 32).trim().equalsIgnoreCase("NA")) {
					city.setState(exceldatalist.get(i + 32).trim());
				}
				hsCity.add(city);
			}
			if (!exceldatalist.get(i + 41).trim().equalsIgnoreCase("NA")) {
				City city = new City();
				city.setCityName(exceldatalist.get(i + 41).trim());
				if (!exceldatalist.get(i + 40).trim().equalsIgnoreCase("NA")) {
					city.setState(exceldatalist.get(i + 40).trim());
				}
				hsCity.add(city);
			}

			if (!exceldatalist.get(i + 57).trim().equalsIgnoreCase("NA")) {
				hsCtcTemplateName.add(exceldatalist.get(i + 57).trim());
			}
			if (!exceldatalist.get(i + 62).trim().equalsIgnoreCase("NA")) {
				hsCalendarName.add(exceldatalist.get(i + 62).trim());
			}
			if (!exceldatalist.get(i + 64).trim().equalsIgnoreCase("NA")) {
				hsProjectName.add(exceldatalist.get(i + 64).trim());
			}
			if (!exceldatalist.get(i + 22).trim().equalsIgnoreCase("NA")) {
				if (aadharnumberlist != null && aadharnumberlist.contains(exceldatalist.get(i + 22).trim())) {
					errorlist.add(exceldatalist.get(i + 3));
					errorlist.add(" Duplicate entry found for Aadhar Number " + exceldatalist.get(i + 22));
				} else
					aadharnumberlist.add(Long.parseLong(exceldatalist.get(i + 22)));

			}
			employeeNameList.add(exceldatalist.get(i + 3).trim());
		}

		List<CTCTemplate> ctcTemplateList = null;
		if (hsCtcTemplateName.size() > 0) {
			ArrayList<String> ctclist = new ArrayList<String>(hsCtcTemplateName);
			ctcTemplateList = ofy().load().type(CTCTemplate.class).filter("ctcTemplateName IN", ctclist)
					.filter("companyId", companyId).list();
		}

		List<com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar> calendarList = null;
		if (hsCalendarName.size() > 0) {
			ArrayList<String> clist = new ArrayList<String>(hsCalendarName);
			calendarList = ofy().load()
					.type(com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar.class)
					.filter("calName IN", clist).filter("companyId", companyId).list();
		}

		List<LeaveGroup> leaveGroupList = ofy().load().type(LeaveGroup.class).filter("companyId", companyId).list();

		List<HrProject> projectList = null;
		if (hsProjectName.size() > 0) {
			ArrayList<String> list = new ArrayList<String>(hsProjectName);
			projectList = ofy().load().type(HrProject.class).filter("projectName IN", list)
					.filter("companyId", companyId).list();
		}

		List<Overtime> otList = ofy().load().type(Overtime.class).filter("companyId", companyId).list();

		List<Shift> shiftList = ofy().load().type(Shift.class).filter("companyId", companyId).list();

		List<Department> departmentlist = null;
		if (hsdepartmentName.size() > 0) {
			ArrayList<String> departmentNamelist = new ArrayList<String>(hsdepartmentName);
			departmentlist = ofy().load().type(Department.class).filter("deptName IN", departmentNamelist)
					.filter("companyId", companyId).list();
		}

		List<Branch> branchlist = null;
		if (hsBranchName.size() > 0) {
			ArrayList<String> branchNamelist = new ArrayList<String>(hsBranchName);
			branchlist = ofy().load().type(Branch.class).filter("buisnessUnitName IN", branchNamelist)
					.filter("companyId", companyId).list();
		}

		List<Config> empDesignationlist = serverapputility.loadConfigsData(13, companyId);
		List<Config> emprolelist = serverapputility.loadConfigsData(12, companyId);
		List<Config> empTypelist = serverapputility.loadConfigsData(34, companyId);

		List<Country> globalCountrylist = ofy().load().type(Country.class).filter("companyId", companyId).list();
		List<State> globalStatelist = ofy().load().type(State.class).filter("status", true)
				.filter("companyId", companyId).list();
		List<Locality> globalLocalitylist = ofy().load().type(Locality.class).filter("status", true)
				.filter("companyId", companyId).list();
		List<City> globalCitylist = ofy().load().type(City.class).filter("status", true).filter("companyId", companyId)
				.list();
		ArrayList<City> newcitylist = null;
		if (hsCity.size() > 0) {
			newcitylist = new ArrayList<City>(hsCity);
			serverapputility.createnewCities(globalCitylist, newcitylist, companyId);
		}

		List<Config> numberRangelist = serverapputility.loadConfigsData(91, companyId);
		List<String> employeeGrouplist = new ArrayList<String>();
		employeeGrouplist.add("Direct");
		employeeGrouplist.add("Indirect");

		ArrayList<String> genderlist = new ArrayList<String>();
		genderlist.add("Male");
		genderlist.add("Female");
		genderlist.add("Other");

		ArrayList<String> martialstatus = new ArrayList<String>();
		martialstatus.add("Married");
		martialstatus.add("Unmarried");
		martialstatus.add("widow");
		martialstatus.add("widower");
		List<Employee> aadharwiseEmployeeList = null;
		if (aadharnumberlist != null && aadharnumberlist.size() > 0)
			aadharwiseEmployeeList = ofy().load().type(Employee.class).filter("aadharNumber IN", aadharnumberlist)
					.filter("companyId", companyId).list();
		List<Employee> namewiseEmployeeList = ofy().load().type(Employee.class).filter("fullname IN", employeeNameList)
				.filter("companyId", companyId).list();

		// for (int i = 56; i < exceldatalist.size(); i += 56)
		for (int i = 69; i < exceldatalist.size(); i += 69) {// old 57

			String error = "";
			try {

				/**
				 * @author Vijay Chougule Date :- 05-11-2020 Des :- if Duplicate employee found
				 *         in the excel then giving validation message
				 */
				if (!exceldatalist.get(i + 3).equalsIgnoreCase("na")
						&& !exceldatalist.get(i + 22).equalsIgnoreCase("na")) {

					if (hashduplicateEmplpoyee != null && hashduplicateEmplpoyee.size() != 0) {
						if (hashduplicateEmplpoyee.containsKey(
								exceldatalist.get(i + 3).trim() + "-" + exceldatalist.get(i + 22).trim())) {
							int num = hashduplicateEmplpoyee
									.get(exceldatalist.get(i + 3).trim() + "-" + exceldatalist.get(i + 22).trim());
							num++;
							hashduplicateEmplpoyee
									.put(exceldatalist.get(i + 3).trim() + "-" + exceldatalist.get(i + 22).trim(), num);
							if (num > 1) {
								error += "Duplicate employee name found in the excel please check the excel. ";
							}
						} else {
							hashduplicateEmplpoyee
									.put(exceldatalist.get(i + 3).trim() + "-" + exceldatalist.get(i + 22).trim(), 1);
						}
					} else {
						hashduplicateEmplpoyee
								.put(exceldatalist.get(i + 3).trim() + "-" + exceldatalist.get(i + 22).trim(), 1);
					}
				}
				/**
				 * ends here
				 */

				error += serverapputility.validateMandatory(exceldatalist.get(i).trim(), exceldatalist.get(0).trim());
				error += serverapputility.validateMandatory(exceldatalist.get(i + 3).trim(),
						exceldatalist.get(3).trim());
				error += serverapputility.validateMandatory(exceldatalist.get(i + 5).trim(),
						exceldatalist.get(5).trim());
				error += serverapputility.validateMandatory(exceldatalist.get(i + 8).trim(),
						exceldatalist.get(8).trim());
				error += serverapputility.validateMandatory(exceldatalist.get(i + 51).trim(),
						exceldatalist.get(51).trim());
				error += serverapputility.validateMandatory(exceldatalist.get(i + 25).trim(),
						exceldatalist.get(25).trim());

				error += serverapputility.validateMandatory(exceldatalist.get(i + 28).trim(),
						exceldatalist.get(28).trim());
				error += serverapputility.validateMandatory(exceldatalist.get(i + 31).trim(),
						exceldatalist.get(31).trim());
				error += serverapputility.validateMandatory(exceldatalist.get(i + 32).trim(),
						exceldatalist.get(32).trim());
				error += serverapputility.validateMandatory(exceldatalist.get(i + 33).trim(),
						exceldatalist.get(33).trim());

				error += serverapputility.validateMandatory(exceldatalist.get(i + 36).trim(),
						exceldatalist.get(36).trim());
				error += serverapputility.validateMandatory(exceldatalist.get(i + 39).trim(),
						exceldatalist.get(39).trim());
				error += serverapputility.validateMandatory(exceldatalist.get(i + 40).trim(),
						exceldatalist.get(40).trim());
				error += serverapputility.validateMandatory(exceldatalist.get(i + 41).trim(),
						exceldatalist.get(41).trim());

				error += serverapputility.validateCountryName(globalCountrylist, exceldatalist.get(i).trim(),
						exceldatalist.get(0).trim());

				boolean flag = false;
				if (!exceldatalist.get(i + 1).trim().equalsIgnoreCase("NA") && !serverapputility
						.validateEmpDepartmentStatus(departmentlist, exceldatalist.get(i + 1).trim())) {
					error += " " + exceldatalist.get(1) + " - " + exceldatalist.get(i + 1)
							+ " Name is inactive in the system. ";
					flag = true;
				}
				if (!flag && !exceldatalist.get(i + 1).trim().equalsIgnoreCase("NA")
						&& !serverapputility.validateEmpDepartment(departmentlist, exceldatalist.get(i + 1).trim())) {
					error += " " + exceldatalist.get(1) + " - " + exceldatalist.get(i + 1)
							+ " doest not exist or does not match in the system.";
				}

				error += serverapputility.validateConfigsNames(empTypelist, exceldatalist.get(i + 2).trim(),
						exceldatalist.get(2));
				// Commenting as we are not loading all employees and duplicate name checking
				// already done before for loop
				// error += serverapputility.validateDuplicateEmployeeName(listofemployee,
				// exceldatalist.get(i + 3).trim(), exceldatalist.get(3).trim(),
				// exceldatalist.get(i + 22).trim());

				if (namewiseEmployeeList != null) {
//				error +="Full name - "+exceldatalist.get(i + 3).trim()+" Already exist in the system. Employee name with same name and same employee adhar number not allowed. ";
					error += serverapputility.validateDuplicateEmployeeName(namewiseEmployeeList,
							exceldatalist.get(i + 3).trim(), exceldatalist.get(3).trim(),
							exceldatalist.get(i + 22).trim());
				}

				error += serverapputility.validateDateFormat(exceldatalist.get(i + 4).trim(), "MM/dd/yyyy",
						exceldatalist.get(4).trim());
				error += serverapputility.validateDate(exceldatalist.get(i + 4).trim(), "MM/dd/yyyy",
						exceldatalist.get(4).trim());

				error += serverapputility.validateBranchName(branchlist, exceldatalist.get(i + 5).trim(),
						exceldatalist.get(5).trim());
				error += serverapputility.validateConfigsNames(empDesignationlist, exceldatalist.get(i + 6).trim(),
						exceldatalist.get(6));
				error += serverapputility.validateConfigsNames(emprolelist, exceldatalist.get(i + 7).trim(),
						exceldatalist.get(7));
				error += serverapputility.validateDateFormat(exceldatalist.get(i + 8).trim(), "MM/dd/yyyy",
						exceldatalist.get(8).trim());
				error += serverapputility.validateDate(exceldatalist.get(i + 8).trim(), "MM/dd/yyyy",
						exceldatalist.get(8).trim());

				error += serverapputility.validateEmployeeMartialStatus(martialstatus, exceldatalist.get(i + 9).trim(),
						exceldatalist.get(9));

				Employee salesperson = ofy().load().type(Employee.class).filter("fullname", exceldatalist.get(i + 10))
						.filter("companyId", companyId).first().now();

				if (!exceldatalist.get(i + 10).trim().equalsIgnoreCase("NA")) {
					if (salesperson == null) {
						error += " " + "Reports to - " + exceldatalist.get(i + 10)
								+ " does not exist or not matched in the system!. ";
					}
				}
//			error += serverapputility.validateEmployeeName(listofemployee, exceldatalist.get(i+10).trim(), exceldatalist.get(10));

				error += serverapputility.validateSpecialCharComma(exceldatalist.get(11).trim(),
						exceldatalist.get(i + 11).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(12).trim(),
						exceldatalist.get(i + 12).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(13).trim(),
						exceldatalist.get(i + 12).trim());

				error += serverapputility.ValidateStatus(exceldatalist.get(14).trim(),
						exceldatalist.get(i + 14).trim());

				error += serverapputility.validateStringCloumn(exceldatalist.get(15).trim(),
						exceldatalist.get(i + 15).trim());
				error += serverapputility.validateStringCloumn(exceldatalist.get(16).trim(),
						exceldatalist.get(i + 16).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(17).trim(),
						exceldatalist.get(i + 17).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(18).trim(),
						exceldatalist.get(i + 18).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(19).trim(),
						exceldatalist.get(i + 19).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(20).trim(),
						exceldatalist.get(i + 20).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(21).trim(),
						exceldatalist.get(i + 21).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(22).trim(),
						exceldatalist.get(i + 22).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(23).trim(),
						exceldatalist.get(i + 23).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(24).trim(),
						exceldatalist.get(i + 24).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(25).trim(),
						exceldatalist.get(i + 25).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(26).trim(),
						exceldatalist.get(i + 26).trim());
				error += serverapputility.validateEmailId(exceldatalist.get(27).trim(),
						exceldatalist.get(i + 27).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(28).trim(),
						exceldatalist.get(i + 28).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(29).trim(),
						exceldatalist.get(i + 29).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(30).trim(),
						exceldatalist.get(i + 30).trim());
				error += serverapputility.validateCountryName(globalCountrylist, exceldatalist.get(i + 31).trim(),
						exceldatalist.get(31).trim());

				error += serverapputility.validateStateName(globalStatelist, exceldatalist.get(i + 32).trim(),
						exceldatalist.get(i + 31).trim(), exceldatalist.get(32).trim());
				error += serverapputility.validateLocalityName(globalLocalitylist, exceldatalist.get(i + 34).trim(),
						exceldatalist.get(i + 33).trim(), exceldatalist.get(34).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(35).trim(),
						exceldatalist.get(i + 35).trim());

				error += serverapputility.validateCountryName(globalCountrylist, exceldatalist.get(i + 39).trim(),
						exceldatalist.get(39).trim());

				error += serverapputility.validateStateName(globalStatelist, exceldatalist.get(i + 40).trim(),
						exceldatalist.get(i + 39).trim(), exceldatalist.get(40).trim());
				error += serverapputility.validateLocalityName(globalLocalitylist, exceldatalist.get(i + 42).trim(),
						exceldatalist.get(i + 41).trim(), exceldatalist.get(42).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(43).trim(),
						exceldatalist.get(i + 43).trim());

				error += serverapputility.validateSpecialCharComma(exceldatalist.get(44).trim(),
						exceldatalist.get(i + 44).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(45).trim(),
						exceldatalist.get(i + 45).trim());
				error += serverapputility.validateDateFormat(exceldatalist.get(i + 46).trim(), "MM/dd/yyyy",
						exceldatalist.get(46).trim());
				error += serverapputility.validateDate(exceldatalist.get(i + 46).trim(), "MM/dd/yyyy",
						exceldatalist.get(46).trim());
				error += serverapputility.validateDateFormat(exceldatalist.get(i + 47).trim(), "MM/dd/yyyy",
						exceldatalist.get(47).trim());
				error += serverapputility.validateDate(exceldatalist.get(i + 47).trim(), "MM/dd/yyyy",
						exceldatalist.get(47).trim());

				error += serverapputility.validateSpecialCharComma(exceldatalist.get(48).trim(),
						exceldatalist.get(i + 48).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(49).trim(),
						exceldatalist.get(i + 49).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(50).trim(),
						exceldatalist.get(i + 50).trim());

				if (!validateEmployeeGender(genderlist, exceldatalist.get(i + 51).trim())) {
					error += " " + exceldatalist.get(i + 51) + " - " + exceldatalist.get(51).trim()
							+ " does not exist in the system. ";
				}
				error += serverapputility.validateConfigsNames(numberRangelist, exceldatalist.get(i + 52).trim(),
						exceldatalist.get(52));
				if (!validateEmployeeGroup(employeeGrouplist, exceldatalist.get(i + 53).trim())) {
					error += " " + exceldatalist.get(i + 53) + " - " + exceldatalist.get(53).trim()
							+ " does not exist in the system. ";
				}
				error += serverapputility.validateNumericCloumn(exceldatalist.get(54).trim(),
						exceldatalist.get(i + 54).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(55).trim(),
						exceldatalist.get(i + 55).trim());
				if (!exceldatalist.get(i + 56).trim().equalsIgnoreCase("NA")) {
					error += serverapputility.validateYesNo(exceldatalist.get(56).trim(),
							exceldatalist.get(i + 56).trim());
				}
				// Commenting this as now we are not loading all employees
//			error += serverapputility.validateEmpAdharNumber(listofemployee,exceldatalist.get(22).trim(), exceldatalist.get(i +22).trim());
				if (aadharwiseEmployeeList != null && aadharwiseEmployeeList.contains(exceldatalist.get(i + 22).trim()))
					error += " " + "Aadhar Number-" + exceldatalist.get(i + 22).trim()
							+ " already exist with employee name - " + exceldatalist.get(i + 3).trim();
				// Ashwini Patil Date:9-05-2023
				if (!exceldatalist.get(i + 57).trim().equalsIgnoreCase("NA")) {
					error += serverapputility.validateCTCTemplateName(ctcTemplateList, exceldatalist.get(i + 57).trim(),
							exceldatalist.get(57).trim());

					error += serverapputility.validateMandatory(exceldatalist.get(i + 58).trim(),
							exceldatalist.get(58).trim());
					error += serverapputility.validateMandatory(exceldatalist.get(i + 59).trim(),
							exceldatalist.get(59).trim());
					error += serverapputility.validateMandatory(exceldatalist.get(i + 60).trim(),
							exceldatalist.get(60).trim());
					error += serverapputility.validateMandatory(exceldatalist.get(i + 61).trim(),
							exceldatalist.get(61).trim());

					if (!exceldatalist.get(i + 58).trim().equalsIgnoreCase("NA"))
						error += serverapputility.validateDate(exceldatalist.get(i + 58).trim(), "MM/dd/yyyy",
								exceldatalist.get(58).trim());
					if (!exceldatalist.get(i + 59).trim().equalsIgnoreCase("NA"))
						error += serverapputility.validateDate(exceldatalist.get(i + 59).trim(), "MM/dd/yyyy",
								exceldatalist.get(59).trim());
					if (!exceldatalist.get(i + 60).trim().equalsIgnoreCase("NA"))
						error += serverapputility.validateDate(exceldatalist.get(i + 60).trim(), "MM/dd/yyyy",
								exceldatalist.get(60).trim());
					if (!exceldatalist.get(i + 61).trim().equalsIgnoreCase("NA"))
						error += serverapputility.validateDate(exceldatalist.get(i + 61).trim(), "MM/dd/yyyy",
								exceldatalist.get(61).trim());
				} else {
					if (!exceldatalist.get(i + 58).trim().equalsIgnoreCase("NA"))
						error += " " + "Enter CTC Template or Put NA in " + exceldatalist.get(58).trim();
					if (!exceldatalist.get(i + 59).trim().equalsIgnoreCase("NA"))
						error += " " + "Enter CTC Template or Put NA in " + exceldatalist.get(59).trim();
					if (!exceldatalist.get(i + 60).trim().equalsIgnoreCase("NA"))
						error += " " + "Enter CTC Template or Put NA in " + exceldatalist.get(60).trim();
					if (!exceldatalist.get(i + 61).trim().equalsIgnoreCase("NA"))
						error += " " + "Enter CTC Template or Put NA in " + exceldatalist.get(61).trim();
				}

				if (!exceldatalist.get(i + 62).trim().equalsIgnoreCase("NA")) {
					error += serverapputility.validateCalendarName(calendarList, exceldatalist.get(i + 62).trim(),
							exceldatalist.get(62).trim());
					error += serverapputility.validateMandatory(exceldatalist.get(i + 63).trim(),
							exceldatalist.get(63).trim());
				}

				if (!exceldatalist.get(i + 63).trim().equalsIgnoreCase("NA")) {
					error += serverapputility.validateMandatory(exceldatalist.get(i + 62).trim(),
							exceldatalist.get(62).trim());
					error += serverapputility.validateLeaveGroup(leaveGroupList, exceldatalist.get(i + 63).trim(),
							exceldatalist.get(63).trim());
				}

				if (!exceldatalist.get(i + 64).trim().equalsIgnoreCase("NA")) {
					error += serverapputility.validateProjectName(projectList, exceldatalist.get(i + 64).trim(),
							exceldatalist.get(64).trim(), exceldatalist.get(i + 5).trim());

					if (!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("HrProject",
							"PC_HRProjectOvertimeNonMandatory", companyId))
						error += serverapputility.validateMandatory(exceldatalist.get(i + 65).trim(),
								exceldatalist.get(65).trim());
					error += serverapputility.validateMandatory(exceldatalist.get(i + 66).trim(),
							exceldatalist.get(66).trim());
					error += serverapputility.validateMandatory(exceldatalist.get(i + 67).trim(),
							exceldatalist.get(67).trim());
					error += serverapputility.validateMandatory(exceldatalist.get(i + 68).trim(),
							exceldatalist.get(68).trim());

					if (!exceldatalist.get(i + 65).trim().equalsIgnoreCase("NA"))
						error += serverapputility.validateOTName(otList, exceldatalist.get(i + 65).trim(),
								exceldatalist.get(65).trim());
					if (!exceldatalist.get(i + 66).trim().equalsIgnoreCase("NA"))
						error += serverapputility.validateShiftName(shiftList, exceldatalist.get(i + 66).trim(),
								exceldatalist.get(66).trim());

					error += serverapputility.validateDate(exceldatalist.get(i + 67).trim(), "MM/dd/yyyy",
							exceldatalist.get(67).trim());
					error += serverapputility.validateDate(exceldatalist.get(i + 68).trim(), "MM/dd/yyyy",
							exceldatalist.get(68).trim());
				} else {
					if (!exceldatalist.get(i + 65).trim().equalsIgnoreCase("NA"))
						error += " " + "Enter Project name or Put NA in " + exceldatalist.get(65).trim();
					if (!exceldatalist.get(i + 66).trim().equalsIgnoreCase("NA"))
						error += " " + "Enter Project name or Put NA in " + exceldatalist.get(66).trim();
					if (!exceldatalist.get(i + 67).trim().equalsIgnoreCase("NA"))
						error += " " + "Enter Project name or Put NA in " + exceldatalist.get(67).trim();
					if (!exceldatalist.get(i + 68).trim().equalsIgnoreCase("NA"))
						error += " " + "Enter Project name or Put NA in " + exceldatalist.get(68).trim();
				}

			} catch (Exception e) {
				e.printStackTrace();
				error += " Excel Data is not proper format please check!";
			}

			if (!error.equals("")) {
				logger.log(Level.SEVERE, "Error" + error);
				logger.log(Level.SEVERE, "Error" + error.length());
				errorlist.add(exceldatalist.get(i + 3));
				errorlist.add(error);
			}

		}

		if (errorlist != null && errorlist.size() > 0) {
			CsvWriter.employeeUploadErrorlist = errorlist;
			integerlist.add(-1);
			return integerlist;
		}

		String msg = callTaskQueueForEmployeeUpload(companyId, loggedinuser);
		logger.log(Level.SEVERE, "msg " + msg);
		integerlist.add(0);
		return integerlist;

	}

	/**
	 * @author Administrator Date 03-02-2021 Des :- updated code added Error
	 *         handling and validated all columns
	 */
	private ArrayList<Integer> validateServiceproductFile(long companyId, ArrayList<String> exceldatalist) {

		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		List<ServiceProduct> listofserviceproduct = ofy().load().type(ServiceProduct.class)
				.filter("companyId", companyId).list();
		logger.log(Level.SEVERE, "listofserviceproduct size" + listofserviceproduct.size());
		ServerAppUtility serverapputility = new ServerAppUtility();
		List<Category> productCategorylist = serverapputility.loadproductCategory(companyId);
		List<Config> productGrouplist = serverapputility.loadConfigsData(58, companyId);
		List<Config> productTypelist = serverapputility.loadConfigsData(57, companyId);
		List<Config> productClassificationlist = serverapputility.loadConfigsData(59, companyId);
		List<Config> productUnitofMeasurementlist = serverapputility.loadConfigsData(47, companyId);
		List<TaxDetails> taxlist = ofy().load().type(TaxDetails.class).filter("taxChargeStatus", true)
				.filter("companyId", companyId).list();
		ArrayList<String> errorlist = new ArrayList<String>();

		HashMap<String, Integer> hashduplicateproductName = new HashMap<String, Integer>();

		for (int i = 20; i < exceldatalist.size(); i += 20) {

			String error = "";
			try {

				error += serverapputility.validateDuplicateIntheExcel(hashduplicateproductName,
						exceldatalist.get(3).trim(), exceldatalist.get(i + 3).trim());

				error += serverapputility.validateMandatory(exceldatalist.get(i).trim(), exceldatalist.get(0).trim());
				error += serverapputility.validateMandatory(exceldatalist.get(i + 1).trim(),
						exceldatalist.get(1).trim());
				error += serverapputility.validateMandatory(exceldatalist.get(i + 3).trim(),
						exceldatalist.get(3).trim());
				error += serverapputility.validateMandatory(exceldatalist.get(i + 4).trim(),
						exceldatalist.get(4).trim());
				error += serverapputility.validateMandatory(exceldatalist.get(i + 5).trim(),
						exceldatalist.get(5).trim());
				error += serverapputility.validateMandatory(exceldatalist.get(i + 12).trim(),
						exceldatalist.get(12).trim());

				error += serverapputility.validateServiceProduct(listofserviceproduct, exceldatalist.get(i + 3).trim(),
						exceldatalist.get(3).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(0).trim(),
						exceldatalist.get(i).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(1).trim(),
						exceldatalist.get(i + 1).trim());
				error += serverapputility.ValidateStatus(exceldatalist.get(2).trim(), exceldatalist.get(i + 2).trim());
//			error += serverapputility.validateSpecialCharComma(exceldatalist.get(3).trim(), exceldatalist.get(i+3).trim());
				error += serverapputility.validateServiceProductCategory(productCategorylist,
						exceldatalist.get(4).trim(), exceldatalist.get(i + 4).trim());
//			error += serverapputility.validateSpecialCharComma(exceldatalist.get(5).trim(), exceldatalist.get(i+5).trim());
//			error += serverapputility.validateSpecialCharComma(exceldatalist.get(6).trim(), exceldatalist.get(i+6).trim());
				error += serverapputility.validateConfigsNames(productGrouplist, exceldatalist.get(i + 7).trim(),
						exceldatalist.get(7).trim());
				error += serverapputility.validateConfigsNames(productTypelist, exceldatalist.get(i + 8).trim(),
						exceldatalist.get(8).trim());
				error += serverapputility.validateConfigsNames(productClassificationlist,
						exceldatalist.get(i + 9).trim(), exceldatalist.get(9).trim());
//			error += serverapputility.validateSpecialCharComma(exceldatalist.get(10).trim(), exceldatalist.get(i+10).trim());
//			error += serverapputility.validateSpecialCharComma(exceldatalist.get(11).trim(), exceldatalist.get(i+11).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(12).trim(),
						exceldatalist.get(i + 12).trim());

				error += serverapputility.validateSpecialCharComma(exceldatalist.get(13).trim(),
						exceldatalist.get(i + 13).trim());
				error += serverapputility.validateYesNo(exceldatalist.get(14).trim(), exceldatalist.get(i + 14).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(15).trim(),
						exceldatalist.get(i + 15).trim());
				error += serverapputility.validateYesNo(exceldatalist.get(16).trim(), exceldatalist.get(i + 16).trim());

				error += serverapputility.validateConfigsNames(productUnitofMeasurementlist,
						exceldatalist.get(i + 17).trim(), exceldatalist.get(17).trim());
//			error += serverapputility.validateSpecialCharComma(exceldatalist.get(18).trim(), exceldatalist.get(i+18).trim());
//			error += serverapputility.validateSpecialCharComma(exceldatalist.get(19).trim(), exceldatalist.get(i+19).trim());

				error += serverapputility.validateTaxName(taxlist, exceldatalist.get(13).trim(),
						exceldatalist.get(i + 13).trim());
				error += serverapputility.validateTaxName(taxlist, exceldatalist.get(15).trim(),
						exceldatalist.get(i + 15).trim());

				// Ashwini Patil Date:28-09-2023
				error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(3).trim(),
						exceldatalist.get(i + 3).trim());
				error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(4).trim(),
						exceldatalist.get(i + 4).trim());
				error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(5).trim(),
						exceldatalist.get(i + 5).trim());
				error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(6).trim(),
						exceldatalist.get(i + 6).trim());
				error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(10).trim(),
						exceldatalist.get(i + 10).trim());
				error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(11).trim(),
						exceldatalist.get(i + 11).trim());
				error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(18).trim(),
						exceldatalist.get(i + 18).trim());
				error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(19).trim(),
						exceldatalist.get(i + 19).trim());

			} catch (Exception e) {
				e.printStackTrace();
				error += " Excel Data is not proper format please check!";
			}

			if (!error.equals("")) {
				logger.log(Level.SEVERE, "Error" + error);
				errorlist.add(exceldatalist.get(i + 3));
				errorlist.add(error);
			}

		}

		if (errorlist != null && errorlist.size() > 0) {
			CsvWriter.serviceProductUploadErrorlist = errorlist;
			integerlist.add(-1);
			return integerlist;
		}

		String msg = callTaskQueueForServiceProductUpload(companyId);
		logger.log(Level.SEVERE, "msg " + msg);
		integerlist.add(0);

		return integerlist;
	}

	private String callTaskQueueForServiceProductUpload(long companyId) {

		logger.log(Level.SEVERE, "Service Product Upload  task queue calling");
		try {
			Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue")
					.param("EntityName", "Service Product Upload").param("companyId", companyId + ""));

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception ==" + e.getMessage());
			return "Failed please try again";
		}
		return "Service Product Upload Process Started";
	}

	private ArrayList<Integer> validateItemproductAndCreate(long companyId, ArrayList<String> exceldatalist) {

		ArrayList<Integer> integerlist = new ArrayList<Integer>();

		List<ItemProduct> listofItemproduct = ofy().load().type(ItemProduct.class).filter("companyId", companyId)
				.list();
		System.out.println("The List contains :" + listofItemproduct);

		ServerAppUtility serverapputility = new ServerAppUtility();

		List<Category> productCategorylist = serverapputility.loadproductCategory(companyId);
		List<Config> productGrouplist = serverapputility.loadConfigsData(58, companyId);
		List<Config> productTypelist = serverapputility.loadConfigsData(57, companyId);
		List<Config> productClassificationlist = serverapputility.loadConfigsData(59, companyId);
		List<Config> productUnitofMeasurementlist = serverapputility.loadConfigsData(47, companyId);
		List<TaxDetails> taxlist = ofy().load().type(TaxDetails.class).filter("taxChargeStatus", true)
				.filter("companyId", companyId).list();
		List<InspectionMethod> inspectionmethodlist = ofy().load().type(InspectionMethod.class)
				.filter("companyId", companyId).list();

		ArrayList<String> errorlist = new ArrayList<String>();

		for (int i = 37; i < exceldatalist.size(); i += 37) {
			String error = "";
			try {

				error += serverapputility.validateItemProduct(listofItemproduct, exceldatalist.get(i),
						exceldatalist.get(0));
				error += serverapputility.validateMandatory(exceldatalist.get(i), exceldatalist.get(0));
				error += serverapputility.validateMandatory(exceldatalist.get(i + 1), exceldatalist.get(1));
				error += serverapputility.validateMandatory(exceldatalist.get(i + 2), exceldatalist.get(2));
				error += serverapputility.validateMandatory(exceldatalist.get(i + 18), exceldatalist.get(18));

				error += serverapputility.validateItemProductCategory(productCategorylist, exceldatalist.get(1),
						exceldatalist.get(i + 1));
//			error += serverapputility.validateSpecialCharComma(exceldatalist.get(2), exceldatalist.get(i+2));
//			error += serverapputility.validateSpecialCharComma(exceldatalist.get(3), exceldatalist.get(i+3));
				error += serverapputility.validateConfigsNames(productGrouplist, exceldatalist.get(i + 4).trim(),
						exceldatalist.get(4).trim());
				error += serverapputility.validateConfigsNames(productTypelist, exceldatalist.get(i + 5).trim(),
						exceldatalist.get(5).trim());
				error += serverapputility.validateConfigsNames(productClassificationlist,
						exceldatalist.get(i + 6).trim(), exceldatalist.get(6).trim());
//			error += serverapputility.validateSpecialCharComma(exceldatalist.get(7), exceldatalist.get(i+7));
//			error += serverapputility.validateSpecialCharComma(exceldatalist.get(8), exceldatalist.get(i+8));
				error += serverapputility.validateNumericCloumn(exceldatalist.get(9), exceldatalist.get(i + 9));

				error += serverapputility.validateSpecialCharComma(exceldatalist.get(10).trim(),
						exceldatalist.get(i + 10).trim());
				error += serverapputility.validateYesNo(exceldatalist.get(11).trim(), exceldatalist.get(i + 11).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(12).trim(),
						exceldatalist.get(i + 12).trim());
				error += serverapputility.validateYesNo(exceldatalist.get(13).trim(), exceldatalist.get(i + 13).trim());

				error += serverapputility.validateTaxName(taxlist, exceldatalist.get(10).trim(),
						exceldatalist.get(i + 10).trim());
				error += serverapputility.validateTaxName(taxlist, exceldatalist.get(12).trim(),
						exceldatalist.get(i + 12).trim());
				error += serverapputility.validateYesNo(exceldatalist.get(14).trim(), exceldatalist.get(i + 14).trim());

//			error += serverapputility.validateSpecialCharComma(exceldatalist.get(15).trim(), exceldatalist.get(i+15).trim());
//			error += serverapputility.validateSpecialCharComma(exceldatalist.get(16).trim(), exceldatalist.get(i+16).trim());
//			error += serverapputility.validateSpecialCharComma(exceldatalist.get(17).trim(), exceldatalist.get(i+17).trim());
				error += serverapputility.validateConfigsNames(productUnitofMeasurementlist,
						exceldatalist.get(i + 18).trim(), exceldatalist.get(18).trim());

//			error += serverapputility.validateSpecialCharComma(exceldatalist.get(19).trim(), exceldatalist.get(i+19).trim());
//			error += serverapputility.validateSpecialCharComma(exceldatalist.get(20).trim(), exceldatalist.get(i+20).trim());
				error += serverapputility.validateYesNo(exceldatalist.get(21).trim(), exceldatalist.get(i + 21).trim());
				if (!exceldatalist.get(i + 22).trim().equalsIgnoreCase("NA")) {
					error += serverapputility.validateYesNo(exceldatalist.get(22).trim(),
							exceldatalist.get(i + 22).trim());
				}

				if (!exceldatalist.get(i + 23).trim().equalsIgnoreCase("NA")) {
					error += validateInspectionMethod(inspectionmethodlist, exceldatalist.get(i + 23).trim(),
							exceldatalist.get(23).trim());
				}
				if (exceldatalist.get(i + 21).trim().equalsIgnoreCase("Yes")) {
					if (exceldatalist.get(i + 24).trim().equalsIgnoreCase("NA")) {
						error += "Please add warehouse name to create inventory stock!";
					}
					if (exceldatalist.get(i + 25).trim().equalsIgnoreCase("NA")) {
						error += "Please add storage location to create inventory stock!";
					}
					if (exceldatalist.get(i + 26).trim().equalsIgnoreCase("NA")) {
						error += "Please add storage bin to create inventory stock!";
					}
				}
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(24).trim(),
						exceldatalist.get(i + 24).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(25).trim(),
						exceldatalist.get(i + 25).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(26).trim(),
						exceldatalist.get(i + 26).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(27).trim(),
						exceldatalist.get(i + 27).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(28).trim(),
						exceldatalist.get(i + 28).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(29).trim(),
						exceldatalist.get(i + 29).trim());

				error += serverapputility.validateSpecialCharComma(exceldatalist.get(30).trim(),
						exceldatalist.get(i + 30).trim());
				error += serverapputility.validateYesNo(exceldatalist.get(31).trim(), exceldatalist.get(i + 31).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(32).trim(),
						exceldatalist.get(i + 32).trim());
				error += serverapputility.validateYesNo(exceldatalist.get(33).trim(), exceldatalist.get(i + 33).trim());

				error += serverapputility.validateTaxName(taxlist, exceldatalist.get(30).trim(),
						exceldatalist.get(i + 30).trim());
				error += serverapputility.validateTaxName(taxlist, exceldatalist.get(32).trim(),
						exceldatalist.get(i + 32).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(34).trim(),
						exceldatalist.get(i + 34).trim());
//			error += serverapputility.validateSpecialCharComma(exceldatalist.get(35).trim(), exceldatalist.get(i+35).trim());
//			error += serverapputility.validateSpecialCharComma(exceldatalist.get(36).trim(), exceldatalist.get(i+36).trim());

				// Ashwini Patil Date:28-09-2023
				error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(0).trim(),
						exceldatalist.get(i + 0).trim());
				error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(1).trim(),
						exceldatalist.get(i + 1).trim());
				error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(2).trim(),
						exceldatalist.get(i + 2).trim());
				error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(3).trim(),
						exceldatalist.get(i + 3).trim());
				error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(7).trim(),
						exceldatalist.get(i + 7).trim());
				error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(8).trim(),
						exceldatalist.get(i + 8).trim());
				error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(15).trim(),
						exceldatalist.get(i + 15).trim());
				error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(16).trim(),
						exceldatalist.get(i + 16).trim());
				error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(17).trim(),
						exceldatalist.get(i + 17).trim());
				error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(19).trim(),
						exceldatalist.get(i + 19).trim());
				error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(20).trim(),
						exceldatalist.get(i + 20).trim());
				error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(35).trim(),
						exceldatalist.get(i + 35).trim());
				error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(36).trim(),
						exceldatalist.get(i + 36).trim());

			} catch (Exception e) {
				e.printStackTrace();
				error += " Excel Data is not proper format please check!";
			}

			if (!error.trim().equals("")) {
				if (!error.equals("")) {
					logger.log(Level.SEVERE, "Error" + error);
					errorlist.add(exceldatalist.get(i));
					errorlist.add(error);
				}
			}
		}

		if (errorlist != null && errorlist.size() > 0) {
			CsvWriter.itemProductUploadErrorlist = errorlist;
			integerlist.add(-1);
			return integerlist;
		}

		String msg = callTaskQueueForItemProductUpload(companyId);
		logger.log(Level.SEVERE, "msg " + msg);
		integerlist.add(0);

		return integerlist;

	}

	private String callTaskQueueForItemProductUpload(long companyId) {
		logger.log(Level.SEVERE, "Item Product Upload  task queue calling");
		try {
			Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue")
					.param("EntityName", "Item Product Upload").param("companyId", companyId + ""));

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception ==" + e.getMessage());
			return "Failed please try again";
		}
		return "Item Product Upload Process Started";
	}

	private String validateInspectionMethod(List<InspectionMethod> inspectionmethodlist, String inspectionMethod,
			String columnName) {
		if (!inspectionMethod.trim().equalsIgnoreCase("NA")) {
			for (InspectionMethod inspection : inspectionmethodlist) {
				if (inspection.getInspectionMethodName().trim().equals(inspectionMethod.trim())) {
					return "";
				}
			}
			return columnName + "- " + inspectionMethod + " does not exist or not matched! ";
		}
		return "";
	}

	public ArrayList<Integer> itemproductsave(long companyId, ArrayList<String> exceldatalist) {
		// TODO Auto-generated method stub

		ArrayList<Integer> integerlist = new ArrayList<Integer>();

		int refcount = 0;
		try {

			ArrayList<ItemProduct> itemproductlist = new ArrayList<ItemProduct>();
			/** date 7.12.2018 added by komal for product inventory view save **/
			ArrayList<ProductInventoryView> productInventoryViewList = new ArrayList<ProductInventoryView>();

			List<TaxDetails> taxlist = ofy().load().type(TaxDetails.class).filter("companyId", companyId).list();
			ServerAppUtility serverapputility = new ServerAppUtility();

			List<WareHouse> warehouselist = ofy().load().type(WareHouse.class).filter("companyId", companyId).list();
			List<StorageLocation> storagelocationlist = ofy().load().type(StorageLocation.class)
					.filter("companyId", companyId).list();
			List<Storagebin> storageBinlist = ofy().load().type(Storagebin.class).filter("companyId", companyId).list();

			HashSet<String> hswarehouselocationbin = new HashSet<String>();

			NumberGeneration ng = new NumberGeneration();

			ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
					.filter("processName", "ItemProduct").filter("status", true).first().now();

			long number = ng.getNumber();
			int count = (int) number;

			String strRefNo = "";
			ItemProduct itemproductobj = null;
			boolean multpleWarehouseFlag = false;
			ProductInventoryView productInventoryView = null;
			for (int i = 37; i < exceldatalist.size(); i += 37) {
				/**
				 * Date 07-12-2018 by Vijay Des :- Added ref number for multiple warehouse
				 * upload for the same product
				 */
				if (exceldatalist.get(i + 36).trim().equalsIgnoreCase("NA")
						|| !exceldatalist.get(i + 36).trim().equalsIgnoreCase("NA") && strRefNo.equals("")
						|| !strRefNo.equals(exceldatalist.get(i + 36).trim())) {
					itemproductobj = new ItemProduct();
					multpleWarehouseFlag = true;

					if (exceldatalist.get(i).equalsIgnoreCase("na")) {
						itemproductobj.setProductCode("");
					} else {
						itemproductobj.setProductCode(exceldatalist.get(i));
					}

					if (exceldatalist.get(i + 1).equalsIgnoreCase("na")) {
						itemproductobj.setProductCategory("");
					} else {
						itemproductobj.setProductCategory(exceldatalist.get(i + 1));
					}

					if (exceldatalist.get(i + 2).equalsIgnoreCase("na")) {
						itemproductobj.setProductName("");
					} else {
						itemproductobj.setProductName(exceldatalist.get(i + 2));
					}

					if (exceldatalist.get(i + 3).equalsIgnoreCase("na")) {
						itemproductobj.setSpecifications("");
					} else {
						itemproductobj.setSpecifications(exceldatalist.get(i + 3));
					}

					if (exceldatalist.get(i + 4).equalsIgnoreCase("na")) {
						itemproductobj.setProductGroup("");
					} else {
						itemproductobj.setProductGroup(exceldatalist.get(i + 4));
					}

					if (exceldatalist.get(i + 5).equalsIgnoreCase("na")) {
						itemproductobj.setProductType("");
					} else {
						itemproductobj.setProductType(exceldatalist.get(i + 5));
					}

					if (exceldatalist.get(i + 6).equalsIgnoreCase("na")) {
						itemproductobj.setProductClassification("");
					} else {
						itemproductobj.setProductClassification(exceldatalist.get(i + 6));
					}

					if (exceldatalist.get(i + 7).equalsIgnoreCase("na")) {
						itemproductobj.setRefNumber1("");
					} else {
						itemproductobj.setRefNumber1(exceldatalist.get(i + 7));
					}

					if (exceldatalist.get(i + 8).equalsIgnoreCase("na")) {
						itemproductobj.setRefNumber2("");
					} else {
						itemproductobj.setRefNumber2(exceldatalist.get(i + 8));
					}

					if (exceldatalist.get(i + 9).equalsIgnoreCase("na")) {
						itemproductobj.setPrice(0d);
					} else {
						itemproductobj.setPrice(Double.parseDouble(exceldatalist.get(i + 9)));
					}

					if (exceldatalist.get(i + 10).equalsIgnoreCase("na")) {

						Tax tax = new Tax();
						tax.setTaxName("NA");
						tax.setTaxConfigName("NA");
						tax.setPercentage(0);
						tax.setInclusive(false);
						itemproductobj.setVatTax(tax);

					} else {

//				String taxPercent=getTaxPercentForName(Double.parseDouble(exceldatalist.get(i + 10)));
//				Tax tax = new Tax();
//				tax.setTaxName("CGST@"+taxPercent);
//				tax.setTaxPrintName("CGST");
//				tax.setTaxConfigName("CGST@"+taxPercent);
//				tax.setPercentage(Double.parseDouble(exceldatalist.get(i + 10)));
//				itemproductobj.setStatus(true);
//				itemproductobj.setVatTax(tax);

						/**
						 * @author Vijay Date 03-02-2021 Des :- updated code as per Tax Name tax details
						 *         will map
						 */
						TaxDetails taxdetails = serverapputility.gettaxDetails(taxlist, exceldatalist.get(i + 10));
						if (taxdetails != null) {
							Tax tax = new Tax();
							tax.setTaxName(taxdetails.getTaxChargeName());
							tax.setTaxPrintName(taxdetails.getTaxPrintName());
							tax.setTaxConfigName(taxdetails.getTaxChargeName());
							tax.setInclusive(false);
							tax.setPercentage(taxdetails.getTaxChargePercent());
							itemproductobj.setVatTax(tax);
						}
					}

					if (exceldatalist.get(i + 11).equalsIgnoreCase("no")
							|| exceldatalist.get(i + 11).equalsIgnoreCase("na")) {
						/**
						 * Old Code
						 */
						// Tax tax = new Tax();
						// tax.setInclusive(false);
						/**
						 * Date : 12-10-2017 BY ANIL
						 */
						itemproductobj.getVatTax().setInclusive(false);
					} else {
						/**
						 * Old Code
						 */
						// Tax tax = new Tax();
						// tax.setInclusive(true);
						/**
						 * Date :12-10-2017 BY ANIL
						 */
						itemproductobj.getVatTax().setInclusive(true);
					}

					if (exceldatalist.get(i + 12).equalsIgnoreCase("na")) {
						/**
						 * Date : 12-10-2017 BY ANIL
						 */
						Tax tax = new Tax();
						tax.setTaxName("NA");
						tax.setTaxConfigName("NA");
						tax.setPercentage(0d);
						tax.setInclusive(false);
						itemproductobj.setServiceTax(tax);

					} else {

//				/**
//				 * Date : 12-10-2017 BY ANIL
//				 */
//				String taxPercent=getTaxPercentForName(Double.parseDouble(exceldatalist.get(i + 12)));
//				Tax tax = new Tax();
//				tax.setTaxName("SGST@"+taxPercent);
//				tax.setTaxPrintName("SGST");
//				tax.setTaxConfigName("SGST@"+taxPercent);
//				tax.setPercentage(Double.parseDouble(exceldatalist.get(i + 12)));
//				itemproductobj.setStatus(true);
//				itemproductobj.setServiceTax(tax);

						/**
						 * @author Vijay Date 03-02-2021 Des :- updated code as per Tax Name tax details
						 *         will map
						 */
						TaxDetails taxdetails = serverapputility.gettaxDetails(taxlist, exceldatalist.get(i + 12));
						if (taxdetails != null) {
							Tax tax = new Tax();
							tax.setTaxName(taxdetails.getTaxChargeName());
							tax.setTaxPrintName(taxdetails.getTaxPrintName());
							tax.setTaxConfigName(taxdetails.getTaxChargeName());
							tax.setInclusive(false);
							tax.setPercentage(taxdetails.getTaxChargePercent());
							itemproductobj.setServiceTax(tax);
						}
					}

					if (exceldatalist.get(i + 13).equalsIgnoreCase("no")
							|| exceldatalist.get(i + 13).equalsIgnoreCase("na")) {
						/**
						 * Old Code
						 */
						// Tax tax = new Tax();
						// tax.setInclusive(false);
						/**
						 * Date : 12-10-2017 By ANIL
						 */
						itemproductobj.getServiceTax().setInclusive(false);
					} else {
						/**
						 * Old Code
						 */
						// Tax tax = new Tax();
						// tax.setInclusive(true);
						/**
						 * Date : 12-10-2017 By ANIL
						 */
						itemproductobj.getServiceTax().setInclusive(true);
					}

					if (exceldatalist.get(i + 14).equalsIgnoreCase("no")) {
						itemproductobj.setStatus(false);
					} else {
						itemproductobj.setStatus(true);
					}
					if (exceldatalist.get(i + 15).equalsIgnoreCase("na")) {
						itemproductobj.setBrandName("");
					} else {
						itemproductobj.setBrandName(exceldatalist.get(i + 15));
					}
					if (exceldatalist.get(i + 16).equalsIgnoreCase("na")) {
						itemproductobj.setModel("");
					} else {
						itemproductobj.setModel(exceldatalist.get(i + 16));
					}
					if (exceldatalist.get(i + 17).equalsIgnoreCase("na")) {
						itemproductobj.setSerialNumber("");
					} else {
						itemproductobj.setSerialNumber(exceldatalist.get(i + 17));
					}
					if (exceldatalist.get(i + 18).equalsIgnoreCase("na")) {
						itemproductobj.setUnitOfMeasurement("");
					} else {
						itemproductobj.setUnitOfMeasurement(exceldatalist.get(i + 18));
					}
					if (exceldatalist.get(i + 19).equalsIgnoreCase("na")) {
						itemproductobj.setComment("");
					} else {
						itemproductobj.setComment(exceldatalist.get(i + 19));
					}

					if (exceldatalist.get(i + 20).equalsIgnoreCase("na")) {
						itemproductobj.setCommentdesc("");
					} else {
						itemproductobj.setCommentdesc(exceldatalist.get(i + 20));
					}

					/***
					 * 
					 * Date : 23-04-2018 BY ANIL Added new column which stores reference number of
					 * service product for HVAC/Rohan
					 */

					if (exceldatalist.get(i + 28).equalsIgnoreCase("na")) {
						itemproductobj.setSerProdId(0);
						itemproductobj.setSerProdCode("");
						itemproductobj.setSerProdName("");
						itemproductobj.setWarrantyDuration(0);
						itemproductobj.setNoOfServices(0);
					} else {

						int serviceId = Integer.parseInt(exceldatalist.get(i + 28).trim());
						ServiceProduct serProd = ofy().load().type(ServiceProduct.class).filter("companyId", companyId)
								.filter("count", serviceId).first().now();
						if (serProd != null) {
							itemproductobj.setSerProdId(serProd.getCount());
							itemproductobj.setSerProdCode(serProd.getProductCode());
							itemproductobj.setSerProdName(serProd.getProductName());
							itemproductobj.setWarrantyDuration(serProd.getDuration());
							itemproductobj.setNoOfServices(serProd.getNumberOfServices());
						}

					}

					if (exceldatalist.get(i + 29).equalsIgnoreCase("na")) {
						logger.log(Level.SEVERE, "Inside purchase price zero");
						itemproductobj.setPurchasePrice(0);
					} else {
						logger.log(Level.SEVERE, "Inside purchase price non zero");
						itemproductobj.setPurchasePrice(Double.parseDouble(exceldatalist.get(i + 29).trim()));
					}

					if (exceldatalist.get(i + 30).equalsIgnoreCase("na")) {
//				itemproductobj.setPurchasePrice(0);
						Tax tax = new Tax();
						tax.setTaxName("CGST");
						tax.setTaxPrintName("CGST");
						tax.setTaxConfigName("CGST");
						tax.setPercentage(0d);
						tax.setInclusive(false);

						itemproductobj.setPurchaseTax1(tax);
					} else {

//				String taxPercent=getTaxPercentForName(Double.parseDouble(exceldatalist.get(i + 30)));
//				Tax tax = new Tax();
//				tax.setTaxName("CGST@"+taxPercent);
//				tax.setTaxPrintName("CGST");
//				tax.setTaxConfigName("CGST@"+taxPercent);
//				tax.setPercentage(Double.parseDouble(exceldatalist.get(i + 30)));
//				itemproductobj.setStatus(true);
//				itemproductobj.setPurchaseTax1(tax);

						/**
						 * @author Vijay Date 03-02-2021 Des :- updated code as per Tax Name tax details
						 *         will map
						 */
						TaxDetails taxdetails = serverapputility.gettaxDetails(taxlist, exceldatalist.get(i + 30));
						if (taxdetails != null) {
							Tax tax = new Tax();
							tax.setTaxName(taxdetails.getTaxChargeName());
							tax.setTaxPrintName(taxdetails.getTaxPrintName());
							tax.setTaxConfigName(taxdetails.getTaxChargeName());
							tax.setInclusive(false);
							tax.setPercentage(taxdetails.getTaxChargePercent());
							itemproductobj.setPurchaseTax1(tax);
						}
					}

					if (exceldatalist.get(i + 31).equalsIgnoreCase("no")
							|| exceldatalist.get(i + 31).equalsIgnoreCase("na")) {
						/**
						 * Old Code
						 */
						// Tax tax = new Tax();
						// tax.setInclusive(false);
						/**
						 * Date : 12-10-2017 By RAHUL
						 */
						itemproductobj.getPurchaseTax1().setInclusive(false);
					} else {
						/**
						 * Old Code
						 */
						// Tax tax = new Tax();
						// tax.setInclusive(true);
						/**
						 * Date : 12-10-2017 By RAHUL
						 */
						itemproductobj.getPurchaseTax1().setInclusive(true);
					}

					if (exceldatalist.get(i + 32).equalsIgnoreCase("na")) {
//				itemproductobj.setPurchasePrice(0);
						Tax tax = new Tax();
						tax.setTaxName("SGST");
						tax.setTaxPrintName("SGST");
						tax.setTaxConfigName("SGST");
						tax.setPercentage(0d);
						tax.setInclusive(false);
						itemproductobj.setPurchaseTax2(tax);
					} else {
////				itemproductobj.setPurchasePrice(0);
//				String taxPercent=getTaxPercentForName(Double.parseDouble(exceldatalist.get(i + 32)));
//				Tax tax = new Tax();
//				tax.setTaxName("SGST@"+taxPercent);
//				tax.setTaxPrintName("SGST");
//				tax.setTaxConfigName("SGST@"+taxPercent);
//				tax.setPercentage(Double.parseDouble(exceldatalist.get(i + 32)));
//				itemproductobj.setStatus(true);
//				itemproductobj.setPurchaseTax2(tax);

						/**
						 * @author Vijay Date 03-02-2021 Des :- updated code as per Tax Name tax details
						 *         will map
						 */
						TaxDetails taxdetails = serverapputility.gettaxDetails(taxlist, exceldatalist.get(i + 32));
						if (taxdetails != null) {
							Tax tax = new Tax();
							tax.setTaxName(taxdetails.getTaxChargeName());
							tax.setTaxPrintName(taxdetails.getTaxPrintName());
							tax.setTaxConfigName(taxdetails.getTaxChargeName());
							tax.setInclusive(false);
							tax.setPercentage(taxdetails.getTaxChargePercent());
							itemproductobj.setPurchaseTax2(tax);
						}
					}

					if (exceldatalist.get(i + 33).equalsIgnoreCase("no")
							|| exceldatalist.get(i + 33).equalsIgnoreCase("na")) {
						/**
						 * Old Code
						 */
						// Tax tax = new Tax();
						// tax.setInclusive(false);
						/**
						 * Date : 12-10-2017 By RAHUL
						 */
						itemproductobj.getPurchaseTax2().setInclusive(false);
					} else {
						/**
						 * Old Code
						 */
						// Tax tax = new Tax();
						// tax.setInclusive(true);
						/**
						 * Date : 12-10-2017 By RAHUL
						 */
						itemproductobj.getPurchaseTax2().setInclusive(true);
					}

					if (exceldatalist.get(i + 34).equalsIgnoreCase("NA")) {
						itemproductobj.setMrp(0);
					} else {
						itemproductobj.setMrp(Double.parseDouble(exceldatalist.get(i + 34)));
					}

					if (exceldatalist.get(i + 35).equalsIgnoreCase("NA")) {
						itemproductobj.setHsnNumber("");
					} else {
						itemproductobj.setHsnNumber(exceldatalist.get(i + 35));
					}

					/**
					 * End
					 */
					itemproductobj.setCount(++count);
					itemproductobj.setCompanyId(companyId);
					itemproductlist.add(itemproductobj);
					System.out.println("Done Adding");

					if (!exceldatalist.get(i + 24).equalsIgnoreCase("NA")
							&& !exceldatalist.get(i + 25).equalsIgnoreCase("NA")
							&& !exceldatalist.get(i + 26).equalsIgnoreCase("NA")) {

						String warehouselocationbin = exceldatalist.get(i + 24).trim() + "$"
								+ exceldatalist.get(i + 25).trim() + "$" + exceldatalist.get(i + 26).trim();
						hswarehouselocationbin.add(warehouselocationbin);
					}

				}
				/**
				 * Date 07-12-2018 by Vijay Des :- Added for multiple warehouse upload for the
				 * same product using ref no
				 */
				else {
					multpleWarehouseFlag = false;
				}

				refcount++;
				System.out.println("The refcount count : " + refcount);

				if (exceldatalist.get(i + 21).equalsIgnoreCase("yes")) {

					int refnum = i + 21;
					int pid = itemproductobj.getCount();
					System.out.println("The Count is : " + count);
					String pcode = itemproductobj.getProductCode();
					String pname = itemproductobj.getProductName();
					double pprice = itemproductobj.getPrice();
					String pcategory = itemproductobj.getProductCategory();

					/**
					 * Date 07-12-2018 by Vijay Des :- Added for multiple warehouse upload for the
					 * same product
					 */
					if (multpleWarehouseFlag) {
						if (productInventoryView != null)
							productInventoryViewList.add(productInventoryView);

						productInventoryView = productinventorysave(refnum, pid, pcode, pname, pprice, pcategory,
								companyId, exceldatalist);
					} else {
						ProductInventoryViewDetails pinvendetails = productinventoryDetails(refnum, pid, pcode, pname,
								pprice, pcategory, companyId, exceldatalist);
						productInventoryView.getDetails().add(pinvendetails);
					}
					/**
					 * ends here
					 */

				}

				/**
				 * Date 10-12-2018 by Vijay Des :- Added for multiple warehouse upload for the
				 * same product
				 */
				strRefNo = exceldatalist.get(i + 36).trim();

			}

			/**
			 * @author Vijay Date 05-02-2021 Des :- if warehouse storage location bin not
			 *         created then creating the same
			 */
			ArrayList<String> warehouselocbin = new ArrayList<String>(hswarehouselocationbin);

			ArrayList<WareHouse> warehouse = new ArrayList<WareHouse>();
			ArrayList<StorageLocation> warehousestoragelocation = new ArrayList<StorageLocation>();
			ArrayList<Storagebin> warehousestoragebin = new ArrayList<Storagebin>();

			for (String str : warehouselocbin) {
//			String stringwarehouselocbin = getstring(str);
				String stringwarehouselocbin = str;

				System.out.println("stringwarehouselocbin" + stringwarehouselocbin);
				String strwarehouselocbin[] = stringwarehouselocbin.split("\\$");
				System.out.println("strwarehouselocbin lenth" + strwarehouselocbin.length);
				String warehouseName = strwarehouselocbin[0];
				System.out.println("warehouseName" + warehouseName);
				boolean warehousecreationFlag = true;
				for (WareHouse warehouseEnity : warehouselist) {
					if (warehouseEnity.getBusinessUnitName().trim().equals(warehouseName.trim())) {
						warehousecreationFlag = false;
					}
				}

				if (warehousecreationFlag) {
					WareHouse warehouseEntity = new WareHouse();
					warehouseEntity.setCompanyId(companyId);
					warehouseEntity.setBusinessUnitName(warehouseName);
					warehouseEntity.setstatus(true);
					warehouse.add(warehouseEntity);
				}

				String warehouselocation = "";
				if (strwarehouselocbin.length > 1) {
					System.out.println("strwarehouselocbin.length " + strwarehouselocbin.length);
					warehouselocation = strwarehouselocbin[1];
				}
				boolean warehousestoragelocflag = true;
				if (!warehouselocation.equals("")) {
					for (StorageLocation storagelocationEntity : storagelocationlist) {
						if (storagelocationEntity.getWarehouseName().trim().equals(warehouseName.trim())
								&& storagelocationEntity.getBusinessUnitName().trim()
										.equals(warehouselocation.trim())) {
							warehousestoragelocflag = false;
							break;
						}
					}

					if (warehousestoragelocflag) {
						StorageLocation storagelocEntity = new StorageLocation();
						storagelocEntity.setWarehouseName(warehouseName);
						storagelocEntity.setBusinessUnitName(warehouselocation);
						storagelocEntity.setCompanyId(companyId);
						storagelocEntity.setstatus(true);
						warehousestoragelocation.add(storagelocEntity);
					}
				}

				String warehousebin = "";
				if (strwarehouselocbin.length > 2) {
					System.out.println("strwarehouselocbin.length ==" + strwarehouselocbin.length);
					warehousebin = strwarehouselocbin[2];
				}
				boolean warehousestoragebinflag = true;
				if (!warehouseName.equals("") && !strwarehouselocbin.equals("") && !warehousebin.equals("")) {
					for (Storagebin storagebin : storageBinlist) {
						if (storagebin.getWarehouseName().trim().equals(warehouseName.trim())
								&& storagebin.getStoragelocation().trim().equals(warehouselocation.trim())
								&& storagebin.getBinName().trim().equals(warehousebin.trim())) {
							warehousestoragebinflag = false;
							break;
						}
					}
					if (warehousestoragebinflag) {
						Storagebin storagebinEntity = new Storagebin();
						storagebinEntity.setWarehouseName(warehouseName);
						storagebinEntity.setStoragelocation(warehouselocation);
						storagebinEntity.setBinName(warehousebin);
						storagebinEntity.setCompanyId(companyId);
						storagebinEntity.setStatus(true);
						warehousestoragebin.add(storagebinEntity);
					}
				}

			}
			logger.log(Level.SEVERE, "warehouse size" + warehouse.size());
			logger.log(Level.SEVERE, "warehousestoragelocation size" + warehousestoragelocation.size());
			logger.log(Level.SEVERE, "warehousestoragebin size" + warehousestoragebin.size());

			if (warehouse.size() > 0) {
				ArrayList<SuperModel> list = new ArrayList<SuperModel>();
				list.addAll(warehouse);
				GenricServiceImpl impl = new GenricServiceImpl();
				impl.save(list);
			}
			if (warehousestoragelocation.size() > 0) {
				ArrayList<SuperModel> list = new ArrayList<SuperModel>();
				list.addAll(warehousestoragelocation);
				GenricServiceImpl impl = new GenricServiceImpl();
				impl.save(list);
			}
			if (warehousestoragebin.size() > 0) {
				ArrayList<SuperModel> list = new ArrayList<SuperModel>();
				list.addAll(warehousestoragebin);
				GenricServiceImpl impl = new GenricServiceImpl();
				impl.save(list);
			}

			/**
			 * Date 10-12-2018 by Vijay Des :- Added for multiple warehouse upload for the
			 * same product for the last product entry added here
			 */
			if (productInventoryView != null)
				productInventoryViewList.add(productInventoryView);

			// Taking the last count of the Entity
			ofy().save().entities(itemproductlist).now();
			ng.setNumber(count);

			/** date 7.12.2018 added by komal for product inventory view list save **/
			ofy().save().entities(productInventoryViewList).now();

			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(ng);
//		impl.save(ng);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return integerlist;
	}

	private String getstring(String str) {
		String updatedstr = str;
		return updatedstr;
	}

	private boolean validateSiteLocation(ArrayList<CustomerBranchDetails> siteLocationList, String siteLocation,
			String state, int custId) {
		for (CustomerBranchDetails obj : siteLocationList) {
			if (obj.getBusinessUnitName().equals(siteLocation) && obj.getAddress() != null
					&& obj.getAddress().getState().equals(state) && obj.getCinfo().getCount() == custId) {
				return true;
			}
		}
		return false;
	}

	private HrProject getProjectName(String projName, List<HrProject> projectList) {
		if (projectList != null) {
			for (HrProject proj : projectList) {
				if (proj.getProjectName().equals(projName)) {
					return proj;
				}
			}
		}
		return null;
	}

	private String validateEmployeeCalender(String emptId, List<EmployeeInfo> employeeInfolist, boolean flag) {
		String str = "";
		Integer employeeID = Integer.parseInt(emptId);

		EmployeeInfo empInfo = validateEmpId(employeeID, employeeInfolist);
		if (empInfo != null) {
			if (empInfo.getLeaveCalendar() != null && !empInfo.isAllocatable(new Date())) {
			} else {
				str = "Calendar is not assigned to employee or it is expired.";

			}
		} else {
			if (flag) {
				str = "Employee doesn't exist in the DB or it is inactive";
			}
		}

		return str;
	}

	private EmployeeInfo validateEmpId(int empId, List<EmployeeInfo> empInfoList) {
		for (EmployeeInfo info : empInfoList) {
			if (empId == info.getEmpCount() && info.isStatus() == true) {
				return info;
			}
		}
		return null;
	}

	private boolean validateEmployeeGroup(List<String> employeeGrouplist, String columnvalue) {
		if (!columnvalue.trim().equalsIgnoreCase("NA")) {
			for (String strName : employeeGrouplist) {
				if (strName.trim().equals(columnvalue.trim())) {
					return true;
				}
			}
			return false;
		} else {
			return true;
		}

	}

	private boolean validateEmployeeGender(ArrayList<String> genderlist, String columnvalue) {
		if (!columnvalue.trim().equalsIgnoreCase("NA")) {
			for (String strName : genderlist) {
				if (strName.trim().equals(columnvalue.trim())) {
					return true;
				}
			}
		}
		return false;

	}

	private void createCity(List<City> globalCitylist, String cityName, String stateName, long companyId) {
		boolean cityflag = true;
		if (globalCitylist != null) {
			for (City cityEntity : globalCitylist) {
				if (cityEntity.getCityName().trim().equals(cityName.trim())
						&& cityEntity.getStateName().trim().equals(stateName.trim())) {
					cityflag = false;
				}
			}
		}
		if (cityflag) {
			City cityEntity = new City();
			cityEntity.setCompanyId(companyId);
			cityEntity.setCityName(cityName);
			cityEntity.setState(stateName);
			cityEntity.setStatus(true);
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(cityEntity);
			globalCitylist.add(cityEntity);
		}

	}

	private boolean validateEmployeeMartialStatus(ArrayList<String> martialstatus, String columnvalue) {
		for (String strName : martialstatus) {
			if (strName.trim().equals(columnvalue.trim())) {
				return true;
			}
		}
		return false;
	}

	private Employee getEmployee(List<Employee> listofemployee, String empId) {

		Integer employeeID = Integer.parseInt(empId);

		for (Employee employee : listofemployee) {
			if (employee.getCount() == employeeID) {
				return employee;
			}

		}
		return null;

	}

	private String validateEmployeeBranch(String branch, String projectName, List<HrProject> projectList) {
		HrProject project = getProjectName(projectName.trim(), projectList);
		if (project != null) {
			if (!branch.trim().equals(project.getBranch())) {
				return "Employee branch does not match with project branch";
			}
		}
		return null;
	}

	@Override
	public ArrayList<CTCTemplate> validateCTCAllocation(long companyId, String loggedInUser) {
		ServerAppUtility serverAppUtility = new ServerAppUtility();

		ArrayList<String> exceldatalist = serverAppUtility.readExcelAndGetArraylist(DataMigrationImpl.blobkey,
				"dd/MM/yyyy");
		if (exceldatalist != null) {
			logger.log(Level.SEVERE, "excel list after reading data exceldatalist size", exceldatalist.size());

			ArrayList<String> uploadfileHeader = new ArrayList<String>();
			uploadfileHeader.add("Sr.No");
			uploadfileHeader.add("Employee Id");
			uploadfileHeader.add("Employee Name");
			uploadfileHeader.add("Project");
			uploadfileHeader.add("CTC Template Name");
			uploadfileHeader.add("Applicable Fom (DD/MM/YYYY)");
			uploadfileHeader.add("Applicable To (DD/MM/YYYY)");
			uploadfileHeader.add("Effective From (DD/MM/YYYY)");
			uploadfileHeader.add("Effective To (DD/MM/YYYY)");
			boolean validExcelfile = serverAppUtility.validateExcelSheet(exceldatalist, uploadfileHeader);
			if (validExcelfile) {
				ArrayList<CTCTemplate> list = ValidateCTCAllocationUploadFile(companyId, exceldatalist,
						serverAppUtility, loggedInUser);
				return list;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	private ArrayList<CTCTemplate> ValidateCTCAllocationUploadFile(long companyId, ArrayList<String> exceldatalist,
			ServerAppUtility serverapputility, String loggedInUser) {

		HashSet<Integer> hsempId = new HashSet<Integer>();
		HashSet<String> hsprojectName = new HashSet<String>();
		HashSet<String> hstemplateName = new HashSet<String>();

		for (int i = 9; i < exceldatalist.size(); i += 9) {
			if (!exceldatalist.get(i + 1).trim().equalsIgnoreCase("NA")) {
				try {
					hsempId.add(Integer.parseInt(exceldatalist.get(i + 1).trim()));
				} catch (Exception e) {
				}
			}
			if (!exceldatalist.get(i + 3).trim().equalsIgnoreCase("NA")) {
				hsprojectName.add(exceldatalist.get(i + 3).trim());
			}

			if (!exceldatalist.get(i + 4).trim().equalsIgnoreCase("NA")) {
				hstemplateName.add(exceldatalist.get(i + 4).trim());
			}
		}
		ArrayList<Integer> employeeidlist = new ArrayList<Integer>(hsempId);
		List<EmployeeInfo> employeeInfolist = ofy().load().type(EmployeeInfo.class).filter("companyId", companyId)
				.filter("empCount IN", employeeidlist).filter("status", true).list();

		ArrayList<String> projList = new ArrayList<String>(hsprojectName);
		List<HrProject> projectList = null;
		if (projList.size() != 0) {
			projectList = ofy().load().type(HrProject.class).filter("companyId", companyId).filter("status", true)
					.filter("projectName IN", projList).list();
		}

		List<CTCTemplate> ctctemplatelist = null;
		if (hstemplateName.size() != 0) {
			ArrayList<String> templatename = new ArrayList<String>(hstemplateName);
			ctctemplatelist = ofy().load().type(CTCTemplate.class).filter("companyId", companyId)
					.filter("ctcTemplateName IN", templatename).list();

		}

		SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
		ArrayList<String> errorlist = new ArrayList<String>();
		ArrayList<CTCTemplate> ctctemplatearraylist = new ArrayList<CTCTemplate>();

		for (int i = 9; i < exceldatalist.size(); i += 9) {
			String error = "";

			error += serverapputility.validateMandatory(exceldatalist.get(i).trim(), exceldatalist.get(0).trim());
			error += serverapputility.validateMandatory(exceldatalist.get(i + 1).trim(), exceldatalist.get(1).trim());
			error += serverapputility.validateMandatory(exceldatalist.get(i + 2).trim(), exceldatalist.get(2).trim());
			error += serverapputility.validateMandatory(exceldatalist.get(i + 4).trim(), exceldatalist.get(4).trim());
			error += serverapputility.validateMandatory(exceldatalist.get(i + 5).trim(), exceldatalist.get(5).trim());
			error += serverapputility.validateMandatory(exceldatalist.get(i + 6).trim(), exceldatalist.get(6).trim());
			error += serverapputility.validateMandatory(exceldatalist.get(i + 7).trim(), exceldatalist.get(7).trim());
			error += serverapputility.validateMandatory(exceldatalist.get(i + 8).trim(), exceldatalist.get(8).trim());
			error += serverapputility.validateNumericCloumn(exceldatalist.get(0).trim(), exceldatalist.get(i).trim());
			error += serverapputility.validateNumericCloumn(exceldatalist.get(1).trim(),
					exceldatalist.get(i + 1).trim());

			error += serverapputility.validateSpecialCharComma(exceldatalist.get(0).trim(),
					exceldatalist.get(i).trim());
			error += serverapputility.validateSpecialCharComma(exceldatalist.get(1).trim(),
					exceldatalist.get(i + 1).trim());
			error += serverapputility.validateSpecialCharComma(exceldatalist.get(2).trim(),
					exceldatalist.get(i + 2).trim());
			error += serverapputility.validateSpecialCharComma(exceldatalist.get(3).trim(),
					exceldatalist.get(i + 3).trim());
			error += serverapputility.validateSpecialCharComma(exceldatalist.get(4).trim(),
					exceldatalist.get(i + 4).trim());
			error += serverapputility.validateSpecialCharComma(exceldatalist.get(5).trim(),
					exceldatalist.get(i + 5).trim());
			error += serverapputility.validateSpecialCharComma(exceldatalist.get(6).trim(),
					exceldatalist.get(i + 6).trim());
			error += serverapputility.validateSpecialCharComma(exceldatalist.get(7).trim(),
					exceldatalist.get(i + 7).trim());
			error += serverapputility.validateSpecialCharComma(exceldatalist.get(8).trim(),
					exceldatalist.get(i + 8).trim());

			error += serverapputility.validateFormulla(exceldatalist.get(0).trim(), exceldatalist.get(i).trim());
			error += serverapputility.validateFormulla(exceldatalist.get(1).trim(), exceldatalist.get(i + 1).trim());
			error += serverapputility.validateFormulla(exceldatalist.get(2).trim(), exceldatalist.get(i + 2).trim());
			error += serverapputility.validateFormulla(exceldatalist.get(3).trim(), exceldatalist.get(i + 3).trim());
			error += serverapputility.validateFormulla(exceldatalist.get(4).trim(), exceldatalist.get(i + 4).trim());
			error += serverapputility.validateFormulla(exceldatalist.get(5).trim(), exceldatalist.get(i + 5).trim());
			error += serverapputility.validateFormulla(exceldatalist.get(6).trim(), exceldatalist.get(i + 6).trim());
			error += serverapputility.validateFormulla(exceldatalist.get(7).trim(), exceldatalist.get(i + 7).trim());
			error += serverapputility.validateFormulla(exceldatalist.get(8).trim(), exceldatalist.get(i + 8).trim());

			logger.log(Level.SEVERE, "Applicable from Date" + exceldatalist.get(i + 5).trim());
			logger.log(Level.SEVERE, "Applicable to Date" + exceldatalist.get(i + 6).trim());
			logger.log(Level.SEVERE, "Effective from Date" + exceldatalist.get(i + 7).trim());
			logger.log(Level.SEVERE, "Effective to Date" + exceldatalist.get(i + 8).trim());

			error += serverapputility.validateDateFormat(exceldatalist.get(i + 5).trim(), "dd/MM/yyyy",
					exceldatalist.get(5).trim());
			error += serverapputility.validateDate(exceldatalist.get(i + 6).trim(), "dd/MM/yyyy",
					exceldatalist.get(6).trim());
			error += serverapputility.validateDate(exceldatalist.get(i + 7).trim(), "dd/MM/yyyy",
					exceldatalist.get(7).trim());
			error += serverapputility.validateDate(exceldatalist.get(i + 8).trim(), "dd/MM/yyyy",
					exceldatalist.get(8).trim());

			if (!exceldatalist.get(i + 1).trim().equalsIgnoreCase("NA")
					&& !exceldatalist.get(i + 2).trim().equalsIgnoreCase("NA")) {
				String strEmp = validateEmployeeInEmpInfo(employeeInfolist, exceldatalist.get(i + 1).trim(),
						exceldatalist.get(i + 2).trim());
				if (!strEmp.equals("")) {
					error += strEmp;
				}
			}

			if (!exceldatalist.get(i + 3).trim().equalsIgnoreCase("NA") && projectList != null) {
				HrProject project = getProjectName(exceldatalist.get(i + 3).trim(), projectList);
				if (project == null) {
					error += " Project Name does not exist - " + exceldatalist.get(i + 3).trim() + ". ";
				}
			}
			if (ctctemplatelist != null && !exceldatalist.get(i + 4).trim().equalsIgnoreCase("NA")) {
				boolean flag = validateCTCTemplateName(exceldatalist.get(i + 4), ctctemplatelist);
				if (!flag) {
					error += " CTC Template Name does not exist! ";
				}
			}

			String strEmpCal = validateEmployeeCalender(exceldatalist.get(i + 1).trim(), employeeInfolist, false);
			if (!strEmpCal.equals("")) {
				error += strEmpCal;
			}

			if (!exceldatalist.get(i + 5).trim().equalsIgnoreCase("NA")
					&& !exceldatalist.get(i + 6).trim().equalsIgnoreCase("NA")) {
				try {
					Date applicablefromDate = dateformat.parse(exceldatalist.get(i + 5).trim());
					Date applicableToDate = dateformat.parse(exceldatalist.get(i + 6).trim());
					if (applicableToDate.before(applicablefromDate)) {
						error += "Applicable To date should be greater than applicable from date. ";
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (!exceldatalist.get(i + 7).trim().equalsIgnoreCase("NA")
					&& !exceldatalist.get(i + 8).trim().equalsIgnoreCase("NA")) {
				try {
					Date effectivefromDate = dateformat.parse(exceldatalist.get(i + 7).trim());
					Date effectiveToDate = dateformat.parse(exceldatalist.get(i + 8).trim());
					if (effectiveToDate.before(effectivefromDate)) {
						error += " Effective To date should be greater than effective from date. ";
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (!error.trim().equals("")) {
				if (!error.equals("")) {
					errorlist.add(exceldatalist.get(i));
					errorlist.add(error);

					CTCTemplate ctctemplate = new CTCTemplate();
					ctctemplate.setCount(-1);
					ctctemplatearraylist.add(ctctemplate);
				}
			}
		}

		if (errorlist != null && errorlist.size() > 0) {
			CsvWriter.ctcallocationUploadErrorlist = errorlist;
			return ctctemplatearraylist;
		}

		String msg = callTaskQueueForCTCAllocationUpload(companyId, loggedInUser);
		logger.log(Level.SEVERE, "msg " + msg);

		return ctctemplatearraylist;
	}

	private String callTaskQueueForCTCAllocationUpload(long companyId, String loggedInUser) {

		logger.log(Level.SEVERE, "CTC Allocation upload updation task queue calling");
		try {

			Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue")
					.param("EntityName", "CTC Allocation").param("companyId", companyId + "")
					.param("loggedInUser", loggedInUser));

		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Exception ==" + e.getMessage());
			return "Failed please try again";
		}
		return "CTC Allocation Upload Process Started";

	}

	public void ctcAllocationUpload(long companyId, ArrayList<String> exceldatalist, String loggedInUser) {
		logger.log(Level.SEVERE, "CTC allocaton upload ");
		HashSet<Integer> hsempId = new HashSet<Integer>();
		HashSet<String> hstemplateName = new HashSet<String>();

		for (int i = 9; i < exceldatalist.size(); i += 9) {
			if (!exceldatalist.get(i + 1).trim().equalsIgnoreCase("NA")) {
				try {
					hsempId.add(Integer.parseInt(exceldatalist.get(i + 1).trim()));
				} catch (Exception e) {
				}
			}

			if (!exceldatalist.get(i + 4).trim().equalsIgnoreCase("NA")) {
				hstemplateName.add(exceldatalist.get(i + 4).trim());
			}
		}
		ArrayList<Integer> employeeidlist = new ArrayList<Integer>(hsempId);
		List<EmployeeInfo> employeelist = ofy().load().type(EmployeeInfo.class).filter("companyId", companyId)
				.filter("count IN", employeeidlist).list();

		List<CTCTemplate> ctctemplatelist = null;
		if (hstemplateName.size() != 0) {
			ArrayList<String> templatename = new ArrayList<String>(hstemplateName);
			ctctemplatelist = ofy().load().type(CTCTemplate.class).filter("companyId", companyId)
					.filter("ctcTemplateName IN", templatename).list();

		}

		SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");

		List<CTC> ctcList = ofy().load().type(CTC.class).filter("companyId", companyId).list();// .filter("empid",
																								// empIdList).list();

		for (int i = 9; i < exceldatalist.size(); i += 9) {

			int employeeId = Integer.parseInt(exceldatalist.get(i + 1));
			boolean ctcreviseFlag = false;
			for (CTC ctc : ctcList) {
				if (ctc.getEmpid() == employeeId) {
					ctcreviseFlag = true;
					break;
				}
			}
			CtcAllocationServiceImpl ctcallocationimpl = new CtcAllocationServiceImpl();

			if (ctcreviseFlag) {
				EmployeeInfo employeeInfo = getEmployeeInfo(employeelist, employeeId);
				if (employeeInfo != null) {
					CTCTemplate ctcTemplate = getCtcTemplate(ctctemplatelist, exceldatalist.get(i + 4).trim());
					try {
						Date applicablefromDate = dateformat.parse(exceldatalist.get(i + 5).trim());
						Date applicableToDate = dateformat.parse(exceldatalist.get(i + 6).trim());

						Date effectivefromDate = dateformat.parse(exceldatalist.get(i + 7).trim());
						Date effectiveToDate = dateformat.parse(exceldatalist.get(i + 8).trim());

						String projectName = "";
						if (!exceldatalist.get(i + 3).trim().equalsIgnoreCase("NA")) {
							projectName = exceldatalist.get(i + 3).trim();
						}
						ArrayList<EmployeeInfo> allocationArray = new ArrayList<EmployeeInfo>();
						allocationArray.add(employeeInfo);

						logger.log(Level.SEVERE, "CTC Revise RPC calling ");

						ctcallocationimpl.reviseSalary(allocationArray, ctcTemplate, applicablefromDate,
								applicableToDate, effectivefromDate, loggedInUser, effectiveToDate, projectName, false);

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			} else {

				EmployeeInfo employeeInfo = getEmployeeInfo(employeelist, employeeId);
				if (employeeInfo != null) {
					CTCTemplate ctcTemplate = getCtcTemplate(ctctemplatelist, exceldatalist.get(i + 4).trim());
					try {
						Date applicablefromDate = dateformat.parse(exceldatalist.get(i + 5).trim());
						Date applicableToDate = dateformat.parse(exceldatalist.get(i + 6).trim());

						Date effectivefromDate = dateformat.parse(exceldatalist.get(i + 7).trim());
						Date effectiveToDate = dateformat.parse(exceldatalist.get(i + 8).trim());

						String projectName = "";
						if (!exceldatalist.get(i + 3).trim().equalsIgnoreCase("NA")) {
							projectName = exceldatalist.get(i + 3).trim();
						}

						AllocationResult result = new AllocationResult();
						result.setInfo(employeeInfo);
						if (employeeInfo.getLeaveCalendar() == null) {
						} else {
							result.setReason(employeeInfo.getCalendarName());
						}

						ArrayList<AllocationResult> allocationArray = new ArrayList<AllocationResult>();
						allocationArray.add(result);

						logger.log(Level.SEVERE, "CTC allocaton RPC calling ");

						ctcallocationimpl.allocateCtc(allocationArray, ctcTemplate, applicablefromDate,
								applicableToDate, effectivefromDate, loggedInUser, effectiveToDate, projectName);

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			}

		}
	}

	private CTCTemplate getCtcTemplate(List<CTCTemplate> ctctemplatelist, String ctcTemplateName) {
		for (CTCTemplate ctctemplate : ctctemplatelist) {
			if (ctctemplate.getCtcTemplateName().trim().equals(ctcTemplateName.trim())) {
				return ctctemplate;
			}
		}
		return null;
	}

	private EmployeeInfo getEmployeeInfo(List<EmployeeInfo> employeelist, int employeeId) {
		for (EmployeeInfo empinfo : employeelist) {
			if (empinfo.getCount() == employeeId) {
				return empinfo;
			}
		}
		return null;
	}

	private String validateEmployeeInEmpInfo(List<EmployeeInfo> employeelist, String empId, String employeeName) {
		String str = "";
		Integer employeeID = Integer.parseInt(empId);

		for (EmployeeInfo employee : employeelist) {
			if (employee.getEmpCount() == employeeID) {
				str = "";
				if (!employee.getFullName().trim().equals(employeeName)) {
					str = " Employee ID and Employee Name does not match in Employee Master. ";
//					logger.log(Level.SEVERE, "errrorrrr 11"+str);
				}
				break;
			} else {
				str = " Employee ID Does not exist in Employee Master. ";
//				logger.log(Level.SEVERE, "errrorrrr 22"+str);
			}
		}
		logger.log(Level.SEVERE, "return error value " + str);
		return str;

	}

	@Override
	public ArrayList<CreditNote> validateAndUploadCreditNote(long companyId) {

		ServerAppUtility serverAppUtility = new ServerAppUtility();

		ArrayList<String> exceldatalist = serverAppUtility.readExcelAndGetArraylist(DataMigrationImpl.blobkey, null);
		if (exceldatalist != null) {
			logger.log(Level.SEVERE, "excel list after reading data exceldatalist size", exceldatalist.size());

			ArrayList<String> uploadfileHeader = new ArrayList<String>();
			uploadfileHeader.add("Voucher No");
			uploadfileHeader.add("Voucher Date");
			uploadfileHeader.add("Reference No");
			uploadfileHeader.add("Gross Total");
			uploadfileHeader.add("Base Amount");
			uploadfileHeader.add("Cgst");
			uploadfileHeader.add("Sgst");
			uploadfileHeader.add("Igst");
			uploadfileHeader.add("Round Off");
			uploadfileHeader.add("Narration");
			boolean validExcelfile = serverAppUtility.validateExcelSheet(exceldatalist, uploadfileHeader);
			if (validExcelfile) {
				ArrayList<CreditNote> list = ValidateCreditNoteUploadFile(companyId, exceldatalist, serverAppUtility);
				return list;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	private ArrayList<CreditNote> ValidateCreditNoteUploadFile(long companyId, ArrayList<String> exceldatalist,
			ServerAppUtility serverAppUtility) {

		ArrayList<CreditNote> creditnotelist = new ArrayList<CreditNote>();
		ArrayList<String> errorList = new ArrayList<String>();

		SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		format.setTimeZone(TimeZone.getTimeZone("IST"));

		HashSet<Integer> hsinvoiceId = new HashSet<Integer>();

		HashMap<String, Integer> invoiceIdMap = new HashMap<String, Integer>();

		for (int i = 10; i < exceldatalist.size(); i += 10) {

			try {
				hsinvoiceId.add(Integer.parseInt(exceldatalist.get(i + 2)));

				if (invoiceIdMap != null && invoiceIdMap.size() > 0) {
					if (invoiceIdMap.containsKey(exceldatalist.get(i + 2))) {
						int count = invoiceIdMap.get(exceldatalist.get(i + 2));
						count++;
						invoiceIdMap.put(exceldatalist.get(i + 2), count);
					} else {
						invoiceIdMap.put(exceldatalist.get(i + 2), 1);
					}

				} else {
					invoiceIdMap.put(exceldatalist.get(i + 2), 1);
				}

			} catch (Exception e) {
				errorList.add(exceldatalist.get(i));
				errorList.add(
						"Invalid Invoice Number! Column Name - Voucher Ref. No. (Invoice Id) must be only numeric values.");
			}
		}

		if (errorList.size() > 0) {
			CreditNote creditnote = new CreditNote();
			creditnote.setCount(-1);
			creditnotelist.add(creditnote);
		}

		for (Map.Entry<String, Integer> invoiceId : invoiceIdMap.entrySet()) {
			if (invoiceId.getValue() > 1) {
				errorList.add("");
				errorList.add("Invoice id can't be duplicate in excel. - " + invoiceId.getKey());

			}
		}

		if (errorList.size() > 0) {
			CreditNote creditnote = new CreditNote();
			creditnote.setCount(-1);
			creditnotelist.add(creditnote);
		}

		ArrayList<Integer> arryinvoiceId = new ArrayList<Integer>(hsinvoiceId);
		List<Invoice> invoicelist = serverAppUtility.loadInvoices(companyId, arryinvoiceId);
		List<CustomerPayment> paymentdetailslist = serverAppUtility.loadCustomerPayment(companyId, arryinvoiceId,
				CustomerPayment.CREATED);
		List<CustomerPayment> cancelledPaymentlist = serverAppUtility.loadCustomerPayment(companyId, arryinvoiceId,
				CustomerPayment.CANCELLED);
		if (cancelledPaymentlist != null)
			paymentdetailslist.addAll(cancelledPaymentlist); // 23-11-2023
		List<CreditNote> creditNoteEntitylist = ofy().load().type(CreditNote.class).filter("companyId", companyId)
				.filter("invoiceId IN", arryinvoiceId).list();

		for (int i = 10; i < exceldatalist.size(); i += 10) {

			String error = "";

			error += serverAppUtility.validateMandatory(exceldatalist.get(i).trim(), exceldatalist.get(0).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 1).trim(), exceldatalist.get(1).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 2).trim(), exceldatalist.get(2).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 3).trim(), exceldatalist.get(3).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 4).trim(), exceldatalist.get(4).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 9).trim(), exceldatalist.get(9).trim());

			error += serverAppUtility.validateNumericCloumn(exceldatalist.get(0).trim(), exceldatalist.get(i).trim());
			error += serverAppUtility.validateNumericCloumn(exceldatalist.get(2).trim(),
					exceldatalist.get(i + 2).trim());
			error += serverAppUtility.validateNumericCloumn(exceldatalist.get(3).trim(),
					exceldatalist.get(i + 3).trim());
			error += serverAppUtility.validateNumericCloumn(exceldatalist.get(4).trim(),
					exceldatalist.get(i + 4).trim());
			error += serverAppUtility.validateNumericCloumn(exceldatalist.get(5).trim(),
					exceldatalist.get(i + 5).trim());
			error += serverAppUtility.validateNumericCloumn(exceldatalist.get(6).trim(),
					exceldatalist.get(i + 6).trim());
			error += serverAppUtility.validateNumericCloumn(exceldatalist.get(7).trim(),
					exceldatalist.get(i + 7).trim());
			error += serverAppUtility.validateNumericCloumn(exceldatalist.get(8).trim(),
					exceldatalist.get(i + 8).trim());

			error += serverAppUtility.validateFormulla(exceldatalist.get(0).trim(), exceldatalist.get(i).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(1).trim(), exceldatalist.get(i + 1).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(2).trim(), exceldatalist.get(i + 2).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(3).trim(), exceldatalist.get(i + 3).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(4).trim(), exceldatalist.get(i + 4).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(5).trim(), exceldatalist.get(i + 5).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(6).trim(), exceldatalist.get(i + 6).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(7).trim(), exceldatalist.get(i + 7).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(8).trim(), exceldatalist.get(i + 8).trim());

			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(0).trim(),
					exceldatalist.get(i).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(1).trim(),
					exceldatalist.get(i + 1).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(2).trim(),
					exceldatalist.get(i + 2).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(3).trim(),
					exceldatalist.get(i + 3).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(4).trim(),
					exceldatalist.get(i + 4).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(5).trim(),
					exceldatalist.get(i + 5).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(6).trim(),
					exceldatalist.get(i + 6).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(7).trim(),
					exceldatalist.get(i + 7).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(8).trim(),
					exceldatalist.get(i + 8).trim());

			String dateformat = serverAppUtility.validateDateFormat(exceldatalist.get(i + 1).trim(), "dd-MMM-yyyy",
					exceldatalist.get(1).trim());
			if (!dateformat.equals("") && dateformat.length() > 0) {
				error += dateformat;
			} else {
				error += serverAppUtility.validateDate(exceldatalist.get(i + 1).trim(), "dd-MMM-yyyy",
						exceldatalist.get(1).trim());
			}

			try {
				boolean invoiceIdFlag = validateInvoiceId(invoicelist,
						Integer.parseInt(exceldatalist.get(i + 2).trim()));
				if (invoiceIdFlag == false) {
					error += " Invoice Id does not exist in the system.";
				}

				boolean paymentstatusFlag = validatePaymentDocumentStatus(paymentdetailslist,
						Integer.parseInt(exceldatalist.get(i + 2).trim()));
				if (paymentstatusFlag == false) {
					error += " No Open or Cancelled Payment Document found for Invoice Id -"
							+ exceldatalist.get(i + 2).trim(); // Ashwini Patil Date:23-11-2023 for orion cancelled
																// status added
				}
				error += validateInvoiceIdStatus(invoicelist, Integer.parseInt(exceldatalist.get(i + 2).trim()));

				double creditnoteamt = 0;
				if (!exceldatalist.get(i + 3).trim().equalsIgnoreCase("NA")) {
					try {
						creditnoteamt = Double.parseDouble(exceldatalist.get(i + 3).trim());
					} catch (Exception e) {
						// TODO: handle exception
					}
				}

				if (creditnoteamt > 0) {
					Invoice invoiceEntity = getinvoiceDetails(invoicelist,
							Integer.parseInt(exceldatalist.get(i + 2).trim()));
					if (invoiceEntity != null) {
						error += validatePaymentDocumentAmount(paymentdetailslist,
								Integer.parseInt(exceldatalist.get(i + 2).trim()), creditnoteamt,
								invoiceEntity.getAccountType());
					}
				}

			} catch (Exception e) {
			}

			if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("NA")
					&& exceldatalist.get(i + 6).trim().equalsIgnoreCase("NA")
					&& exceldatalist.get(i + 7).trim().equalsIgnoreCase("NA")) {
				error += "CGST SGST and IGST Column should not be NA!";
			}
			if (!exceldatalist.get(i + 5).trim().equalsIgnoreCase("NA")
					&& !exceldatalist.get(i + 6).trim().equalsIgnoreCase("NA")
					&& !exceldatalist.get(i + 7).trim().equalsIgnoreCase("NA")
					|| ((!exceldatalist.get(i + 5).trim().equalsIgnoreCase("NA")
							|| !exceldatalist.get(i + 6).trim().equalsIgnoreCase("NA"))
							&& !exceldatalist.get(i + 7).trim().equalsIgnoreCase("NA"))) {
				error += "Please add either CGST SGST or IGST!";
			}

			if (!exceldatalist.get(i + 2).trim().equalsIgnoreCase("NA")) {
				try {
					int invoiceId = Integer.parseInt(exceldatalist.get(i + 2).trim());
					error += validateDuplicateCreditNote(creditNoteEntitylist, invoiceId);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}

			if (!error.trim().equals("")) {
				if (!error.equals("")) {
					errorList.add(exceldatalist.get(i));
					errorList.add(error);

					CreditNote creditnote = new CreditNote();
					creditnote.setCount(-1);
					creditnotelist.add(creditnote);
				}
			}
		}

		if (errorList != null && errorList.size() > 0) {
			CsvWriter.creditNoteUploadErrorlist = errorList;
			return creditnotelist;
		}

		String msg = callTaskQueueForCreditNoteUpload(companyId);
		logger.log(Level.SEVERE, "msg " + msg);

		return creditnotelist;

	}

	private String validateDuplicateCreditNote(List<CreditNote> creditNoteEntitylist, int invoiceId) {
		for (CreditNote creditnote : creditNoteEntitylist) {
			if (creditnote.getInvoiceId() == invoiceId) {
				return "Credit Note already exist for invoice id - " + invoiceId;
			}
		}
		return "";
	}

	private Invoice getinvoiceDetails(List<Invoice> invoicelist, int invoiceId) {
		for (Invoice invoiceEntity : invoicelist) {
			if (invoiceEntity.getCount() == invoiceId && !invoiceEntity.getStatus().trim().equals(Invoice.APPROVED)) {
				return invoiceEntity;
			}
		}
		return null;
	}

	private String validatePaymentDocumentAmount(List<CustomerPayment> paymentdetailslist, int invoieId,
			double creditnoteamt, String accountType) {
		for (CustomerPayment paymentEntity : paymentdetailslist) {
			if (paymentEntity.getAccountType().trim().equals(accountType.trim())
					&& paymentEntity.getInvoiceCount() == invoieId
					&& paymentEntity.getStatus().equals(CustomerPayment.CREATED)) {
				if (creditnoteamt > paymentEntity.getPaymentAmt()) {
					return "Gross Amount should not be greater than payment balance amount!";
				}
			}
		}
		return "";
	}

	private String validateInvoiceIdStatus(List<Invoice> invoicelist, int invoiceId) {
		for (Invoice invoiceEntity : invoicelist) {
			if (invoiceEntity.getCount() == invoiceId && !invoiceEntity.getStatus().trim().equals(Invoice.APPROVED)
					&& !invoiceEntity.getStatus().trim().equals(Invoice.CANCELLED)) { // Ashwini Patil Date:22-11-2023
																						// added cancelled status as
																						// clients upload credit note
																						// after invoice cancellation
																						// only. specially in case of
																						// einvoicing. Modified for
																						// orion.
				return " Invoice Id status must be approved / cancelled! ";
			}
		}
		return "";
	}

	private String callTaskQueueForCreditNoteUpload(long companyId) {
		logger.log(Level.SEVERE, "Credit Note Upload task queue calling");
		try {

			Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue")
					.param("EntityName", "Credit Note Upload").param("companyId", companyId + ""));

		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Exception ==" + e.getMessage());
			return "Failed please try again";
		}
		return "Credit Note Upload Process Started";

	}

	private boolean validateInvoiceId(List<Invoice> invoicelist, int invoiceId) {

		for (Invoice invoiceEntity : invoicelist) {
			if (invoiceEntity.getCount() == invoiceId) {
				return true;
			}
		}
		return false;
	}

	private boolean validatePaymentDocumentStatus(List<CustomerPayment> paymentdetailslist, int invoieId) {
		if (paymentdetailslist != null)
			logger.log(Level.SEVERE,
					"paymentdetailslist size for invoice " + invoieId + " = " + paymentdetailslist.size());

		for (CustomerPayment paymentEntity : paymentdetailslist) {
			logger.log(Level.SEVERE,
					"paymentEntity id and status=" + paymentEntity.getCount() + " / " + paymentEntity.getStatus());

			if (paymentEntity.getInvoiceCount() == invoieId) {
				if (paymentEntity.getStatus().equals(CustomerPayment.CREATED)
						|| paymentEntity.getStatus().equals(CustomerPayment.CANCELLED))
					return true;
				else
					return false;
			}

		}
		return false;
	}

	public void creditNoteUpload(long companyId, ArrayList<String> exceldatalist) {

		SimpleDateFormat sdformat = new SimpleDateFormat("dd-MMM-yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		sdformat.setTimeZone(TimeZone.getTimeZone("IST"));

		SimpleDateFormat dateformat = new SimpleDateFormat("MM-dd-yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		dateformat.setTimeZone(TimeZone.getTimeZone("IST"));

		HashSet<Integer> hsinvoiceId = new HashSet<Integer>();

		for (int i = 10; i < exceldatalist.size(); i += 10) {

			try {
				hsinvoiceId.add(Integer.parseInt(exceldatalist.get(i + 2)));
			} catch (Exception e) {
			}
		}

		ServerAppUtility serverapputility = new ServerAppUtility();
		ArrayList<Integer> arryinvoiceId = new ArrayList<Integer>(hsinvoiceId);
		List<CustomerPayment> customerPaymentlist = serverapputility.loadCustomerPayment(companyId, arryinvoiceId,
				CustomerPayment.CREATED);

		GenricServiceImpl impl = new GenricServiceImpl();

		for (int i = 10; i < exceldatalist.size(); i += 10) {

			try {

				CreditNote creditnote = new CreditNote();
				creditnote.setCompanyId(companyId);
				if (!exceldatalist.get(i).trim().equalsIgnoreCase("NA"))
					creditnote.setTallyCreditNoteId(Integer.parseInt(exceldatalist.get(i).trim()));
				Date creditNoteDate = null;
				try {
					creditNoteDate = sdformat.parse(exceldatalist.get(i + 1).trim());
					logger.log(Level.SEVERE, "creditNoteDate ==" + creditNoteDate);
					String strdate = dateformat.format(creditNoteDate);
					logger.log(Level.SEVERE, "strdate ==" + strdate);
					creditNoteDate = dateformat.parse(strdate);
					logger.log(Level.SEVERE, "creditnote == ==" + creditNoteDate);

				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				if (creditNoteDate != null) {
					creditnote.setCreditNoteDate(creditNoteDate);
				}
				if (!exceldatalist.get(i + 2).trim().equalsIgnoreCase("NA"))
					creditnote.setInvoiceId(Integer.parseInt(exceldatalist.get(i + 2).trim()));
				if (!exceldatalist.get(i + 3).trim().equalsIgnoreCase("NA"))
					creditnote.setCreditNoteAmt(Double.parseDouble(exceldatalist.get(i + 3).trim()));
				if (!exceldatalist.get(i + 4).trim().equalsIgnoreCase("NA"))
					creditnote.setBaseAmount(Double.parseDouble(exceldatalist.get(i + 4).trim()));
				if (!exceldatalist.get(i + 5).trim().equalsIgnoreCase("NA"))
					creditnote.setCgstAmt(Double.parseDouble(exceldatalist.get(i + 5).trim()));
				if (!exceldatalist.get(i + 6).trim().equalsIgnoreCase("NA"))
					creditnote.setSgstAmt(Double.parseDouble(exceldatalist.get(i + 6).trim()));
				if (!exceldatalist.get(i + 7).trim().equalsIgnoreCase("NA"))
					creditnote.setIgstAmt(Double.parseDouble(exceldatalist.get(i + 7).trim()));
				if (!exceldatalist.get(i + 8).trim().equalsIgnoreCase("NA"))
					creditnote.setRoundOffAmt(Double.parseDouble(exceldatalist.get(i + 8).trim()));
				if (!exceldatalist.get(i + 9).trim().equalsIgnoreCase("NA"))
					creditnote.setRemark(exceldatalist.get(i + 9).trim());

				ReturnFromServer server = new ReturnFromServer();
				server = impl.save(creditnote);
				int creditNoteId = server.count;
				logger.log(Level.SEVERE, "creditNoteId" + creditNoteId);

				CustomerPayment customerPaymentEntity = getPaymentDocument(customerPaymentlist,
						creditnote.getInvoiceId());
				if (customerPaymentEntity != null) {
					logger.log(Level.SEVERE, "customerPaymentEntity" + customerPaymentEntity);

					if (creditnote.getCreditNoteAmt() == customerPaymentEntity.getPaymentAmt()) {
						customerPaymentEntity.setStatus(CustomerPayment.CANCELLED);
						customerPaymentEntity.setCreditNoteId(creditNoteId);
						customerPaymentEntity.setComment(creditnote.getRemark());
						ofy().save().entities(customerPaymentEntity);

						creditnote.setPaymentId(customerPaymentEntity.getCount());
						creditnote.setBranch(customerPaymentEntity.getBranch());
						ofy().save().entities(creditnote);

					} else {
						if (creditnote.getCreditNoteAmt() < customerPaymentEntity.getPaymentAmt()) {
							int balanceAmt = (int) (customerPaymentEntity.getPaymentAmt()
									- creditnote.getCreditNoteAmt());
							ContractInvoicePayment payment = new ContractInvoicePayment();
							String msg = createBalancePaymentDocument(customerPaymentEntity, balanceAmt);
							logger.log(Level.SEVERE, "msg" + msg);

							customerPaymentEntity.setPaymentAmt((int) creditnote.getCreditNoteAmt());
							customerPaymentEntity.setBalanceRevenue(customerPaymentEntity.getPaymentAmt());
							customerPaymentEntity.setStatus(CustomerPayment.CANCELLED);
							customerPaymentEntity.setCreditNoteId(creditNoteId);
							customerPaymentEntity.setComment(creditnote.getRemark());
							ofy().save().entities(customerPaymentEntity);
							logger.log(Level.SEVERE, "payment document updated successfully");

							creditnote.setPaymentId(customerPaymentEntity.getCount());
							creditnote.setBranch(customerPaymentEntity.getBranch());
							ofy().save().entities(creditnote);

						} else {
							logger.log(Level.SEVERE, "credit note amount is greate than balance open payment document");

						}
					}

				} else {
					CustomerPayment customerPayment = getPaymentDocumentDetails(customerPaymentlist,
							creditnote.getInvoiceId());
					if (customerPayment != null) {
						creditnote.setBranch(customerPayment.getBranch());
						ofy().save().entities(creditnote);
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
			}

		}

	}

	private CustomerPayment getPaymentDocumentDetails(List<CustomerPayment> customerPaymentlist, int invoiceId) {

		for (CustomerPayment custPayment : customerPaymentlist) {
			if (custPayment.getInvoiceCount() == invoiceId) {
				return custPayment;
			}
		}
		return null;

	}

	private CustomerPayment getPaymentDocument(List<CustomerPayment> customerPaymentlist, int invoiceId) {
		for (CustomerPayment custPayment : customerPaymentlist) {
			if (custPayment.getInvoiceCount() == invoiceId
					&& custPayment.getStatus().trim().equals(CustomerPayment.CREATED)) {
				return custPayment;
			}
		}
		return null;
	}

	public String createBalancePaymentDocument(CustomerPayment customerPayment, int balanceAmt) {

		CustomerPayment paydtls = new CustomerPayment();
		paydtls.setPersonInfo(customerPayment.getPersonInfo());
		paydtls.setContractCount(customerPayment.getContractCount());
		paydtls.setTotalSalesAmount(customerPayment.getTotalSalesAmount());
		paydtls.setArrayBillingDocument(customerPayment.getArrayBillingDocument());
		paydtls.setTotalBillingAmount(customerPayment.getTotalBillingAmount());
		paydtls.setInvoiceCount(customerPayment.getInvoiceCount());
		paydtls.setInvoiceDate(customerPayment.getInvoiceDate());
		if (customerPayment.getInvoiceType() != null)
			paydtls.setInvoiceType(customerPayment.getInvoiceType());
		if (customerPayment.getPaymentMethod() != null)
			paydtls.setPaymentMethod(customerPayment.getPaymentMethod());
		if (customerPayment.getContractStartDate() != null) {
			paydtls.setContractStartDate(customerPayment.getContractStartDate());
		}
		if (customerPayment.getContractEndDate() != null) {
			paydtls.setContractEndDate(customerPayment.getContractEndDate());
		}
		paydtls.setInvoiceAmount(customerPayment.getInvoiceAmount());

		double payreceived = 0;
		if (balanceAmt > 0) {
			paydtls.setPaymentAmt(balanceAmt);
			paydtls.setBalanceRevenue(balanceAmt);
		} else {
			double payableAmt = customerPayment.getPaymentAmt();
			payreceived = customerPayment.getPaymentReceived();
			int payAmt = (int) (payableAmt - payreceived);
			paydtls.setPaymentAmt(payAmt);

		}

		if (customerPayment.getPaymentDate() != null)
			paydtls.setPaymentDate(customerPayment.getPaymentDate());

		paydtls.setTypeOfOrder(customerPayment.getTypeOfOrder());

		paydtls.setOrderCreationDate(customerPayment.getOrderCreationDate());
		if (customerPayment.getOrderCformStatus() != null) {
			paydtls.setOrderCformStatus(customerPayment.getOrderCformStatus());
		}
		if (customerPayment.getOrderCformPercent() != -1) {
			paydtls.setOrderCformPercent(customerPayment.getOrderCformPercent());
		}
		paydtls.setOrderCreationDate(customerPayment.getOrderCreationDate());

		paydtls.setAccountType(customerPayment.getAccountType());

		paydtls.setStatus(CustomerPayment.CREATED);

		paydtls.setBranch(customerPayment.getBranch());

		/**
		 * added by vijay on Date 3 march 2017
		 */
		paydtls.setAddintoPettyCash(customerPayment.isAddintoPettyCash());
		if (customerPayment.getPettyCashName() != null)
			paydtls.setPettyCashName(customerPayment.getPettyCashName());

		/**
		 * ends here
		 */

		/**
		 * Date:25-11-2017 BY ANIL setting number range to newly generated payment
		 * document
		 */
		if (customerPayment.getNumberRange() != null) {
			paydtls.setNumberRange(customerPayment.getNumberRange());
		}
		/**
		 * End
		 */

		/** date 21/02/2018 added by komal for actual revenue and tax **/
		if (payreceived < customerPayment.getPaymentReceived()) {
//		if(payreceived < paydtls.getPaymentAmt()){
			double tax = customerPayment.getBalanceTax();
			logger.log(Level.SEVERE, "payreceived == " + payreceived);

			if (payreceived < tax) {
				paydtls.setBalanceRevenue(
						Math.round(customerPayment.getBalanceRevenue() - customerPayment.getReceivedRevenue()));
				paydtls.setBalanceTax(Math.round(tax - customerPayment.getReceivedTax()));
			}
			if (payreceived == tax) {
				paydtls.setBalanceRevenue(Math.round(customerPayment.getBalanceRevenue()));
				paydtls.setBalanceTax(0);
			}
			if (payreceived > tax) {
				paydtls.setBalanceRevenue(
						Math.round(customerPayment.getBalanceRevenue() - customerPayment.getReceivedRevenue()));
				paydtls.setBalanceTax(0);
			}
		}

		/***
		 * Date 07-08-2020 Bug :- partial payment tax amount not showing and TDS set
		 * default as 0
		 **/
		paydtls.setTdsPercentage(0 + "");
		logger.log(Level.SEVERE, "paydtls.getBalanceRevenue() " + paydtls.getBalanceRevenue());

		double balanceTax = (paydtls.getPaymentAmt() - paydtls.getBalanceRevenue());
		paydtls.setBalanceTax(balanceTax);

		paydtls.setCompanyId(customerPayment.getCompanyId());

		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(paydtls);

		logger.log(Level.SEVERE, "Balance payment document created");
		return "Balance payment document created";

	}

	/**
	 * @author Vijay Chougule Date : 06-04-2021 Des :-
	 */

	@Override
	public ArrayList<StackMaster> validateAndUploadInhouseSerivces(long companyId, ArrayList<String> strexcellist) {

		ServerAppUtility serverAppUtility = new ServerAppUtility();

		ArrayList<String> exceldatalist = serverAppUtility.readExcelAndGetArraylist(DataMigrationImpl.blobkey, null);
		try {
			logger.log(Level.SEVERE, "Inside 2nd RPC for Validation exceldatalist" + exceldatalist);
		} catch (Exception e) {
			// TODO: handle exception
		}
		if (exceldatalist == null) {
			exceldatalist = strexcellist;

		}
		if (exceldatalist != null) {
//			logger.log(Level.SEVERE, "excel list after reading data exceldatalist size", exceldatalist.size());
//			
//			ArrayList<String> uploadfileHeader = new ArrayList<String>();
//			uploadfileHeader.add("Warehouse Code");
//			uploadfileHeader.add("Warehouse Name");
//			uploadfileHeader.add("Shed Name");
//			uploadfileHeader.add("Stack Name");
//			uploadfileHeader.add("Stack Weight");
//			boolean validExcelfile = serverAppUtility.validateExcelSheet(exceldatalist,uploadfileHeader);
//			if(validExcelfile) {
			ArrayList<StackMaster> list = ValidateInhouseServiceUploadFile(companyId, exceldatalist, serverAppUtility);
			return list;
//			}
//			else {
//				return null;
//			}
		} else {
			return null;
		}
	}

	private ArrayList<StackMaster> ValidateInhouseServiceUploadFile(long companyId, ArrayList<String> exceldatalist,
			ServerAppUtility serverAppUtility) {

		ArrayList<StackMaster> stackmasterlist = new ArrayList<StackMaster>();
		ArrayList<String> errorList = new ArrayList<String>();

		HashSet<String> hshade = new HashSet<String>();
		HashSet<String> hsStackNo = new HashSet<String>();
		HashSet<String> hswarehouseCode = new HashSet<String>();

		for (int i = 5; i < exceldatalist.size(); i += 5) {
			hswarehouseCode.add(exceldatalist.get(i).trim());
			hshade.add(exceldatalist.get(i + 2));
			hsStackNo.add(exceldatalist.get(i + 3));
		}

		ArrayList<String> warehouselist = new ArrayList<String>(hswarehouseCode);
		ArrayList<String> shadelist = new ArrayList<String>(hshade);
		ArrayList<String> stacklist = new ArrayList<String>(hsStackNo);

		List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId)
				.filter("refNo2 IN", warehouselist).filter("shade IN", shadelist).filter("stackNo IN", stacklist)
				.list();

		logger.log(Level.SEVERE, "servicelist size" + servicelist.size());

		ArrayList<String> warehousecode = new ArrayList<String>(hswarehouseCode);

		List<Contract> contractlist = ofy().load().type(Contract.class).filter("refNo2 IN", warehousecode).list();

		for (int i = 5; i < exceldatalist.size(); i += 5) {
			String error = "";

			error += serverAppUtility.validateMandatory(exceldatalist.get(i).trim(), exceldatalist.get(0).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 1).trim(), exceldatalist.get(1).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 2).trim(), exceldatalist.get(2).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 3).trim(), exceldatalist.get(3).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 4).trim(), exceldatalist.get(4).trim());

			error += serverAppUtility.validateFormulla(exceldatalist.get(0).trim(), exceldatalist.get(i).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(1).trim(), exceldatalist.get(i + 1).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(2).trim(), exceldatalist.get(i + 2).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(3).trim(), exceldatalist.get(i + 3).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(4).trim(), exceldatalist.get(i + 4).trim());

			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(0).trim(),
					exceldatalist.get(i).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(1).trim(),
					exceldatalist.get(i + 1).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(2).trim(),
					exceldatalist.get(i + 2).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(3).trim(),
					exceldatalist.get(i + 3).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(4).trim(),
					exceldatalist.get(i + 4).trim());

			error += validateContract(contractlist, exceldatalist.get(i).trim());
//			error += validateStackEntryExist(servicelist,exceldatalist.get(i).trim(), exceldatalist.get(i+2).trim(),
//					exceldatalist.get(i+3).trim());

			if (!error.trim().equals("")) {
				if (!error.equals("")) {
					errorList.add(exceldatalist.get(i));
					errorList.add(error);

					StackMaster stackMaster = new StackMaster();
					stackMaster.setCount(-1);
					stackmasterlist.add(stackMaster);
				}
			}

		}

		if (errorList != null && errorList.size() > 0) {
			CsvWriter.inhouseServiceUploadErrorlist = errorList;
			return stackmasterlist;
		}

		String msg = callTaskQueueForInhouseServiceUpload(companyId);
		logger.log(Level.SEVERE, "msg " + msg);

		return stackmasterlist;

	}

	private String validateStackEntryExist(List<Service> servicelist, String warehouseCode, String shadeName,
			String stackNo) {

		for (Service serviceEntity : servicelist) {

			if (serviceEntity.getRefNo2().trim().equals(warehouseCode.trim())
					&& serviceEntity.getShade().trim().equals(shadeName.trim())
					&& serviceEntity.getStackNo().trim().equals(stackNo.trim())) {
				return "";

			}
		}

		return "Stack No - " + stackNo + " This stack no old services exist";
	}

	private String validateContract(List<Contract> contractlist, String warehouseCode) {
		boolean flag = false;
		for (Contract contract : contractlist) {
			if (contract.getRefNo2().trim().equals(warehouseCode.trim())) {
				if (!contract.getStatus().trim().equals(Contract.APPROVED)) {
					flag = true;
					return " Contract is not approved status. ";
				} else {
					flag = true;
				}
			}
		}
		if (!flag) {
			return "Contract does not exist. ";
		}
		return "";
	}

	private String callTaskQueueForInhouseServiceUpload(long companyId) {

		logger.log(Level.SEVERE, "Inhouse Services Upload task queue calling");
		try {

			Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue")
					.param("EntityName", "Upload Inhouse Services").param("companyId", companyId + ""));

		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Exception ==" + e.getMessage());
			return "Failed please try again";
		}
		return "Inhouse Services Upload Process Started";
	}

	/**
	 * 
	 */
	public void InhouseServiceCreationUpload(long companyId, ArrayList<String> exceldatalist) {

		logger.log(Level.SEVERE, "inside InhouseServiceCreationUpload");
		try {

			HashSet<String> hswarehouseCode = new HashSet<String>();
			HashSet<String> hshade = new HashSet<String>();
			HashSet<String> hsStackNo = new HashSet<String>();

			for (int i = 5; i < exceldatalist.size(); i += 5) {
				hswarehouseCode.add(exceldatalist.get(i));
				hshade.add(exceldatalist.get(i + 2));
				hsStackNo.add(exceldatalist.get(i + 3));

			}
			ArrayList<String> warehouselist = new ArrayList<String>(hswarehouseCode);
			ArrayList<String> shadelist = new ArrayList<String>(hshade);
			ArrayList<String> stacklist = new ArrayList<String>(hsStackNo);

			List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("refNo2 IN", warehouselist).filter("shade IN", shadelist).filter("stackNo IN", stacklist)
					.list();

			logger.log(Level.SEVERE, "servicelist size" + servicelist);

			for (int i = 5; i < exceldatalist.size(); i += 5) {
				boolean fumigationScheduleServiceFlag = false;
				for (Service service : servicelist) {
					if (service.getRefNo2().trim().equals(exceldatalist.get(i).trim())
							&& service.getShade().trim().equals(exceldatalist.get(i + 2).trim())
							&& service.getStackNo().trim().equals(exceldatalist.get(i + 3).trim())
							&& service.getProduct().getProductCode().trim().equals("DSSG")) {
						logger.log(Level.SEVERE, "Deggassing service found");

						Service serviceEntity = getFumigationService(servicelist, service);
						if (serviceEntity != null) {
							logger.log(Level.SEVERE, "serviceEntity.getFumigationStatus "
									+ serviceEntity.getFumigationStatus() + "-" + serviceEntity.getCount());
							if (serviceEntity.isWmsServiceFlag()
									&& serviceEntity.getStatus().equals(Service.SERVICESTATUSCOMPLETED)
									&& !serviceEntity.getFumigationStatus().equals(Service.SERVICESTATUSSUCCESS)) {
								logger.log(Level.SEVERE,
										"Fumigation Service Status  " + serviceEntity.getFumigationStatus());

								if (!serviceEntity.getFumigationStatus().equals(Service.SERVICESTATUSFAILURE)) {
									fumigationScheduleServiceFlag = true;
									String response = createFumigationAndProphalacticService(serviceEntity, null, null,
											exceldatalist.get(i + 4));
									logger.log(Level.SEVERE, "response for automatic fumigation successful", response);
								}

							}
						} else {
							logger.log(Level.SEVERE, "Fumigation service not found");

						}

					}

				}
				if (!fumigationScheduleServiceFlag && checkOpenServiceForstack(servicelist, exceldatalist.get(i).trim(),
						exceldatalist.get(i + 2).trim(), exceldatalist.get(i + 3).trim())) {

					logger.log(Level.SEVERE, "for create new service == $$ ");

					List<Service> list = getOnlyFumigationServicelist(servicelist, exceldatalist.get(i).trim(),
							exceldatalist.get(i + 2).trim(), exceldatalist.get(i + 3).trim());
					logger.log(Level.SEVERE, "for create new service list size " + list.size());

					if (list != null && list.size() > 0) {
						Comparator<Service> serviceidComparator = new Comparator<Service>() {
							public int compare(Service s1, Service s2) {

								Integer count1 = s1.getCount();
								Integer count2 = s2.getCount();

								// ascending order
								return count2.compareTo(count1);
							}
						};
						Collections.sort(list, serviceidComparator);
						logger.log(Level.SEVERE, "last fumigation service id - " + list.get(0).getCount()
								+ " fumigation service status - " + list.get(0).getStatus());

						String response = createFumigationAndProphalacticService(list.get(0), null, null,
								exceldatalist.get(i + 4));
						logger.log(Level.SEVERE, "response for automatic fumigation successful", response);

					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private List<Service> getOnlyFumigationServicelist(List<Service> servicelist, String warehouseCode,
			String shadeName, String stackNo) {
		ArrayList<Service> fumigationServicelist = new ArrayList<Service>();
		for (Service serviceEntity : servicelist) {
			if (serviceEntity.getRefNo2().trim().equals(warehouseCode.trim())
					&& serviceEntity.getShade().trim().equals(shadeName.trim())
					&& serviceEntity.getStackNo().trim().equals(stackNo.trim())) {
				if (serviceEntity.getProduct().getProductCode().trim().equals("STK-01")
						&& (serviceEntity.getStatus().trim().equals(Service.SERVICESTATUSCOMPLETED))) {
					fumigationServicelist.add(serviceEntity);
				}
			}
		}
		return fumigationServicelist;
	}

	private boolean checkOpenServiceForstack(List<Service> servicelist, String warehouseCode, String shadeName,
			String stackNo) {
		boolean flag = true;
		for (Service serviceEntity : servicelist) {
			if (serviceEntity.getRefNo2().trim().equals(warehouseCode.trim())
					&& serviceEntity.getShade().trim().equals(shadeName.trim())
					&& serviceEntity.getStackNo().trim().equals(stackNo.trim())) {
				if (serviceEntity.getProduct().getProductCode().trim().equals("STK-01")
						&& (serviceEntity.getStatus().trim().equals(Service.SERVICESTATUSSCHEDULE)
								|| serviceEntity.getStatus().trim().equals(Service.SERVICESTATUSRESCHEDULE))) {
					flag = false;
					break;
				}
			}
		}
		logger.log(Level.SEVERE, "flag == ==" + flag);
		return flag;
	}

	private Service getFumigationService(List<Service> servicelist, Service service) {

		for (Service serviceEntity : servicelist) {
			if (serviceEntity.getCount() == service.getFumigationServiceId()
					&& serviceEntity.getShade().trim().equals(service.getShade().trim())
					&& serviceEntity.getStackNo().trim().equals(service.getStackNo().trim())) {
				return serviceEntity;
			}
		}
		return null;
	}

	private String createFumigationAndProphalacticService(Service serviceEntity, String userName, String remark,
			String stackQty) {

		String response = "";

		try {

			if (serviceEntity.getStackQty() > 0) {
				logger.log(Level.SEVERE, "Inside Fumigation Service Creation method");
				Service prophylacticService = ofy().load().type(Service.class)
						.filter("companyId", serviceEntity.getCompanyId()).filter("wmsServiceFlag", true)
						.filter("count", serviceEntity.getProphylacticServiceId()).first().now();
				logger.log(Level.SEVERE, "serviceProduct ==" + prophylacticService);
				if (prophylacticService != null) {
					SingletoneNumberGeneration numGen = SingletoneNumberGeneration.getSingletonInstance();
//				int serviceId=(int) numGen.getLastUpdatedNumber("Service", serviceEntity.getCompanyId(), (long) 2);
					int serviceId = (int) numGen.getLastUpdatedNumber(AppConstants.INHOUSESERVICE,
							serviceEntity.getCompanyId(), (long) 2);
					int fumigationServiceId = serviceId;
					int prophylacticServiceId = ++serviceId;

					for (int i = 0; i < 2; i++) {
						if (i == 0) {
							Service service = new Service();
							service = getServiceDetails(prophylacticService, serviceEntity, stackQty);
							service.setCount(prophylacticServiceId);
							service.setFumigationServiceId(fumigationServiceId);
							ofy().save().entity(service).now();
						} else {
							Service service = new Service();
							service = getServiceDetails(serviceEntity, serviceEntity, stackQty);
							service.setCount(fumigationServiceId);
							service.setProphylacticServiceId(prophylacticServiceId);
							ofy().save().entity(service).now();
							CreateFumigationReport(service, -1);

							/*** Date 11-11-2019 By Vijay Fumigation Service marking as Successfull ***/
							serviceEntity.setFumigationStatus(Service.SERVICESTATUSSUCCESS);
							Date today = DateUtility.getDateWithTimeZone("IST", new Date());
							serviceEntity.setFumigationStatusDate(today);
							if (userName != null) {
								serviceEntity.setActionBy(userName);
							} else {
								serviceEntity.setActionBy("Cron Job");
							}
							if (remark != null) {
								serviceEntity.setActionRemark(remark);
							} else {
								serviceEntity.setActionRemark("This service marked as successful by System");
							}
							ofy().save().entity(serviceEntity);
							response = "New service scheduled successfully with 45 Days";
							logger.log(Level.SEVERE, "New service scheduled successfully with 45 Days");
							updateFumigationSucessfullStatuInFumigationReport(serviceEntity);
						}
					}
				} else {
					response = "Propholactic Service not found for this fumigation service so future services not created";
				}

			} else {
				logger.log(Level.SEVERE, "There no stack Qty so futre services not created");
				response = "There no stack qty so future services not created";
				/*** Date 11-11-2019 By Vijay Fumigation Service marking as Successfull ***/
				serviceEntity.setFumigationStatus(Service.SERVICESTATUSSUCCESS);
				Date today = DateUtility.getDateWithTimeZone("IST", new Date());
				serviceEntity.setFumigationStatusDate(today);
				if (userName != null) {
					serviceEntity.setActionBy(userName);
				} else {
					serviceEntity.setActionBy("Cron Job");
				}
				if (remark != null) {
					serviceEntity.setActionRemark(remark);
				} else {
					serviceEntity.setActionRemark("This service marked as successful by System");
				}
				ofy().save().entity(serviceEntity);
				logger.log(Level.SEVERE, "Stack Qty Zero so new service not created");
				updateFumigationSucessfullStatuInFumigationReport(serviceEntity);

			}

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception " + e.getMessage());
		}

		return response;

	}

	private Service getServiceDetails(Service serviceEntity, Service fumigationService, String stackQty) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

		Service temp = new Service();
		temp.setRefNo(serviceEntity.getRefNo()); // inward // transaction // id
		temp.setPersonInfo(serviceEntity.getPersonInfo());
		temp.setContractCount(serviceEntity.getContractCount());
		temp.setProduct((ServiceProduct) serviceEntity.getProduct());
		temp.setBranch(serviceEntity.getBranch());
		temp.setStatus(Service.SERVICESTATUSSCHEDULE);
		temp.setContractStartDate(serviceEntity.getContractStartDate());
		temp.setContractEndDate(serviceEntity.getContractEndDate());
		temp.setAdHocService(false);
		temp.setServiceIndexNo(serviceEntity.getServiceIndexNo() + 1);
		temp.setServiceType(serviceEntity.getServiceType());

		temp.setServiceSerialNo(serviceEntity.getServiceSerialNo() + 1);
		/**
		 * Date 11-12-2019 by Vijay Des :- New service will create based on Degassing
		 * Original Service Date + 45 Days
		 */
//		Date tPlus45DaysDate = serviceEntity.getServiceCompletionDate();
		Date tPlus45DaysDate = null;
		Service deggassingService = ofy().load().type(Service.class)
				.filter("companyId", fumigationService.getCompanyId())
				.filter("count", fumigationService.getDeggassingServiceId()).first().now();
		if (deggassingService != null) {
			logger.log(Level.SEVERE, "deggassingService" + deggassingService.getOldServiceDate());
			tPlus45DaysDate = deggassingService.getOldServiceDate();
		}
		try {
			Calendar c4 = Calendar.getInstance();
			c4.setTime(tPlus45DaysDate);
			c4.add(Calendar.DATE, +45);
			logger.log(Level.SEVERE, "Date after 45 Days===" + tPlus45DaysDate);

			tPlus45DaysDate = sdf.parse(sdf.format(c4.getTime()));
		} catch (ParseException e) {
			logger.log(Level.SEVERE, " Deggasing service not found");
			e.printStackTrace();
		}
		logger.log(Level.SEVERE, "Date after 45 Days" + tPlus45DaysDate);
		ServerAppUtility apputility = new ServerAppUtility();
		SimpleDateFormat smpDate = new SimpleDateFormat("dd/MM/yyyy");
		String serviceDate = apputility.getForProcessConfigurartionIsActiveOrNot("NewFumigationServiceDate",
				serviceEntity.getCompanyId());
		logger.log(Level.SEVERE, "serviceDate $== " + serviceDate);
		if (serviceDate != null) {
			try {
				tPlus45DaysDate = smpDate.parse(serviceDate);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			logger.log(Level.SEVERE, "tPlus45DaysDate " + tPlus45DaysDate);
		}

		temp.setServiceDate(tPlus45DaysDate);
		temp.setOldServiceDate(tPlus45DaysDate);
		temp.setAddress(serviceEntity.getAddress());

		temp.setServiceTime(serviceEntity.getServiceTime());
		temp.setServiceBranch(serviceEntity.getServiceBranch());
		temp.setServiceDateDay(serviceEntity.getServiceDateDay());

		temp.setBranch(serviceEntity.getBranch());
		temp.setServicingTime(serviceEntity.getServicingTime());

		temp.setServiceSrNo(serviceEntity.getServiceSerialNo());
		temp.setCompanyId(serviceEntity.getCompanyId());
		/**
		 * Date 01-08-2019 by Vijay for NBHC CCPM Fumigation Services for every Stack
		 * This code only executed from Web Services of NBHC Fumigation service
		 */
		temp.setWareHouse(serviceEntity.getWareHouse());
		temp.setRefNo2(serviceEntity.getRefNo2());
		temp.setShade(serviceEntity.getShade());
		temp.setCompartment(serviceEntity.getCompartment());
		temp.setStackNo(serviceEntity.getStackNo());
		Double stackQuantity = Double.parseDouble(stackQty);
		if (stackQuantity != 0) {
			temp.setStackQty(stackQuantity);
		} else {
			temp.setStackQty(serviceEntity.getStackQty());
		}
		if (serviceEntity.getDepositeDate() != null)
			temp.setDepositeDate(serviceEntity.getDepositeDate());
		temp.setCommodityName(serviceEntity.getCommodityName());

		temp.setWmsServiceFlag(true);

		return temp;
	}

	private void updateFumigationSucessfullStatuInFumigationReport(Service service) {
		if (service.getProduct().getProductCode().equals("STK-01")) {
			FumigationServiceReport fumigationReport = ofy().load().type(FumigationServiceReport.class)
					.filter("companyId", service.getCompanyId()).filter("contractId", service.getContractCount())
					.filter("warehouseCode", service.getRefNo2()).filter("compartment", service.getCompartment())
					.filter("shade", service.getShade()).filter("fumigationServiceId", service.getCount()).first()
					.now();
			if (fumigationReport != null) {
				logger.log(Level.SEVERE, "Fumigation Report id" + fumigationReport.getCount());
				fumigationReport.setFumigationStatusDate(service.getFumigationStatusDate());
				fumigationReport.setFumigationRemark(service.getActionRemark());
				fumigationReport.setFumigationStatus(service.getFumigationStatus());
				fumigationReport.setStackQuantity(service.getStackQty());
				fumigationReport.setSuccessFailureBy(service.getActionBy());
				ofy().save().entity(fumigationReport);
				logger.log(Level.SEVERE, "fumigationReport updated successfully");
			}
		}
	}

	private String CreateFumigationReport(Service service, int originalfumigationServiceId) {
		String remark = "";
		logger.log(Level.SEVERE,
				"In the fumigation report entity prophylactic Service ID" + service.getProphylacticServiceId());
		logger.log(Level.SEVERE, "In the fumigation report entity Fumigation Service ID" + service.getCount());

		FumigationServiceReport fumigationReport = new FumigationServiceReport();
		fumigationReport.setCompanyId(service.getCompanyId());
		fumigationReport.setWarehouseName(service.getWareHouse());
		fumigationReport.setWarehouseCode(service.getRefNo2());
		fumigationReport.setCompartment(service.getCompartment());
		fumigationReport.setShade(service.getShade());
		fumigationReport.setStackNumber(service.getStackNo());
		fumigationReport.setStackQuantity(service.getStackQty());
		fumigationReport.setFumigationServiceId(service.getCount());
		fumigationReport.setFumigationServiceDate(service.getOldServiceDate());
		Service serviceEntity = ofy().load().type(Service.class).filter("count", service.getProphylacticServiceId())
				.filter("companyId", service.getCompanyId()).filter("contractCount", service.getContractCount()).first()
				.now();
		logger.log(Level.SEVERE, "prophylactic service serviceEntity" + serviceEntity);
		if (serviceEntity != null) {
			fumigationReport.setProphylacticServiceDate(serviceEntity.getOldServiceDate());
			fumigationReport.setProphylacticServiceId(service.getProphylacticServiceId());
		}
		fumigationReport.setContractId(service.getContractCount());
		fumigationReport.setFumigationServiceStatus(Service.SERVICESTATUSSCHEDULE);
		if (originalfumigationServiceId != -1) {
			fumigationReport.setOriginalfumigationId(originalfumigationServiceId);
		}
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(fumigationReport);
		logger.log(Level.SEVERE, "Fumigation Report created for new fumigation service");
		remark = "Fumigation Report created for new fumigation service";
		return remark;
	}

	@Override
	public ArrayList<String> validateInhouseSerivces(long companyId) {
		logger.log(Level.SEVERE, "Inside 1st validation RPC");
		ArrayList<String> stringlist = new ArrayList<String>();

		ServerAppUtility serverAppUtility = new ServerAppUtility();
		ArrayList<String> exceldatalist = serverAppUtility.readExcelAndGetArraylist(DataMigrationImpl.blobkey, null);

		if (exceldatalist != null) {
			logger.log(Level.SEVERE, "excel list after reading data exceldatalist size", exceldatalist.size());

			ArrayList<String> uploadfileHeader = new ArrayList<String>();
			uploadfileHeader.add("Warehouse Code");
			uploadfileHeader.add("Warehouse Name");
			uploadfileHeader.add("Shed Name");
			uploadfileHeader.add("Stack Name");
			uploadfileHeader.add("Stack Weight");
			boolean validExcelfile = serverAppUtility.validateExcelSheet(exceldatalist, uploadfileHeader);
			if (validExcelfile) {
				stringlist
						.addAll(ValidateInhouseStackWiseServiceUploadFile(companyId, exceldatalist, serverAppUtility));
				return stringlist;
			} else {
				return null;
			}
		} else {
			return null;
		}

	}

	private ArrayList<String> ValidateInhouseStackWiseServiceUploadFile(long companyId, ArrayList<String> exceldatalist,
			ServerAppUtility serverAppUtility) {

		ArrayList<String> stringlist = new ArrayList<String>();

		ArrayList<StackMaster> list = new ArrayList<StackMaster>();

		HashSet<String> hshade = new HashSet<String>();
		HashSet<String> hsStackNo = new HashSet<String>();
		HashSet<String> hswarehouseCode = new HashSet<String>();

		for (int i = 5; i < exceldatalist.size(); i += 5) {
			hswarehouseCode.add(exceldatalist.get(i).trim());
			hshade.add(exceldatalist.get(i + 2));
			hsStackNo.add(exceldatalist.get(i + 3));
		}

		ArrayList<String> warehouselist = new ArrayList<String>(hswarehouseCode);
		ArrayList<String> shadelist = new ArrayList<String>(hshade);
		ArrayList<String> stacklist = new ArrayList<String>(hsStackNo);

		List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId)
				.filter("refNo2 IN", warehouselist).filter("shade IN", shadelist).filter("stackNo IN", stacklist)
				.list();

		logger.log(Level.SEVERE, "servicelist size" + servicelist.size());

		for (int i = 5; i < exceldatalist.size(); i += 5) {
			String error = "";

			error += validateStackEntryExist(servicelist, exceldatalist.get(i).trim(), exceldatalist.get(i + 2).trim(),
					exceldatalist.get(i + 3).trim());

			if (!error.trim().equals("")) {
				if (!error.equals("")) {
					StackMaster stackmaster = getDetails(exceldatalist.get(i), exceldatalist.get(i + 1),
							exceldatalist.get(i + 2), exceldatalist.get(i + 3), exceldatalist.get(i + 4));
					list.add(stackmaster);
				}
			}

		}

		if (list != null && list.size() > 0) {
			CsvWriter.inhouseStackServiceErrorlist = list;
			logger.log(Level.SEVERE, "no stack service list size" + list.size());
			stringlist.add("-1");
			return stringlist;
		}

		stringlist.add("1");
		return stringlist;

	}

	private StackMaster getDetails(String warehouseCode, String warehouseName, String shedName, String stackName,
			String netWeight) {
		StackMaster stackMaster = new StackMaster();
		stackMaster.setWarehouseCode(warehouseCode);
		stackMaster.setWarehouseName(warehouseName);
		stackMaster.setShade(shedName);
		stackMaster.setStackNo(stackName);
		if (!netWeight.equalsIgnoreCase("NA")) {
			stackMaster.setQuantity(Double.parseDouble(netWeight));
		}
		return stackMaster;
	}

	@Override
	public ArrayList<ProductInventoryView> validateStockUpload(long companyId) {

		ServerAppUtility serverAppUtility = new ServerAppUtility();

		ArrayList<String> exceldatalist = serverAppUtility.readExcelAndGetArraylist(DataMigrationImpl.blobkey, null);
		if (exceldatalist != null) {
			logger.log(Level.SEVERE, "excel list after reading data exceldatalist size", exceldatalist.size());

			ArrayList<String> uploadfileHeader = new ArrayList<String>();
			uploadfileHeader.add("Product Id");
			uploadfileHeader.add("Product Name");
			uploadfileHeader.add("Warehouse");
			uploadfileHeader.add("Storage Location");
			uploadfileHeader.add("Storage Bin");
			uploadfileHeader.add("Min Qty");
			uploadfileHeader.add("Max Qty");
			uploadfileHeader.add("Normal level");
			uploadfileHeader.add("Reorder level");
			uploadfileHeader.add("Reorder Qty");
			uploadfileHeader.add("Available Qty");

			boolean validExcelfile = serverAppUtility.validateExcelSheet(exceldatalist, uploadfileHeader);
			if (validExcelfile) {
				ArrayList<ProductInventoryView> list = ValidateStockUploadUploadFile(companyId, exceldatalist,
						serverAppUtility);
				return list;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	private ArrayList<ProductInventoryView> ValidateStockUploadUploadFile(long companyId,
			ArrayList<String> exceldatalist, ServerAppUtility serverAppUtility) {

		ArrayList<ProductInventoryView> productinventoryviewlist = new ArrayList<ProductInventoryView>();
		ArrayList<String> errorList = new ArrayList<String>();

		HashSet<String> hsuniquewarehouseprodid = new HashSet<String>();
		HashSet<String> hswarehouseName = new HashSet<String>();
		HashSet<String> hsstoragelocation = new HashSet<String>();
		HashSet<String> hsstorageBin = new HashSet<String>();

		for (int i = 11; i < exceldatalist.size(); i += 11) {
			hsuniquewarehouseprodid.add(exceldatalist.get(i) + "$" + exceldatalist.get(i + 1) + exceldatalist.get(i + 2)
					+ exceldatalist.get(i + 3) + exceldatalist.get(i + 4));

			hswarehouseName.add(exceldatalist.get(i + 2).trim());
			hsstoragelocation.add(exceldatalist.get(i + 3).trim());
			hsstorageBin.add(exceldatalist.get(i + 4).trim());
		}
		HashSet<Integer> productId = new HashSet<Integer>();

		for (String productwarehouse : hsuniquewarehouseprodid) {
			int duplicateproductwarehouse = 0;
			for (int i = 11; i < exceldatalist.size(); i += 11) {
				String uniquewarehouseprodid = exceldatalist.get(i) + "$" + exceldatalist.get(i + 1)
						+ exceldatalist.get(i + 2) + exceldatalist.get(i + 3) + exceldatalist.get(i + 4);

				if (uniquewarehouseprodid.equals(productwarehouse)) {
					duplicateproductwarehouse++;
				}
				try {
					productId.add(Integer.parseInt(exceldatalist.get(i).trim()));
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			if (duplicateproductwarehouse > 1) {
				String[] productid = productwarehouse.split("$");
				errorList.add(productid[0]);
				errorList.add("Duplicate product id with warehouse location bin found in the excel");
			}
		}

		if (errorList.size() > 0) {
			ProductInventoryView productinventory = new ProductInventoryView();
			productinventory.setCount(-1);
			productinventoryviewlist.add(productinventory);
		}

		ArrayList<Integer> productidlist = new ArrayList<Integer>(productId);

		List<ItemProduct> itemproductlist = ofy().load().type(ItemProduct.class).filter("companyId", companyId)
				.filter("count IN", productidlist).list();
		logger.log(Level.SEVERE, "itemproductlist size" + itemproductlist.size());

		ArrayList<String> warehouseName = new ArrayList<String>(hswarehouseName);
		List<WareHouse> warehouselist = ofy().load().type(WareHouse.class).filter("companyId", companyId)
				.filter("buisnessUnitName IN", warehouseName).list();

		ArrayList<String> storagelocation = new ArrayList<String>(hsstoragelocation);
		List<StorageLocation> storagelocationlist = ofy().load().type(StorageLocation.class)
				.filter("companyId", companyId).filter("buisnessUnitName IN", storagelocation).list();

		ArrayList<String> storageBin = new ArrayList<String>(hsstorageBin);
		List<Storagebin> storageBinlist = ofy().load().type(Storagebin.class).filter("companyId", companyId)
				.filter("binName IN", storageBin).list();

		for (int i = 11; i < exceldatalist.size(); i += 11) {

			String error = "";

			error += serverAppUtility.validateMandatory(exceldatalist.get(i).trim(), exceldatalist.get(0).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 1).trim(), exceldatalist.get(1).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 2).trim(), exceldatalist.get(2).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 3).trim(), exceldatalist.get(3).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 4).trim(), exceldatalist.get(4).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 10).trim(), exceldatalist.get(10).trim());

			error += serverAppUtility.validateNumericCloumn(exceldatalist.get(i).trim(), exceldatalist.get(i).trim());
			error += serverAppUtility.validateNumericCloumn(exceldatalist.get(5).trim(),
					exceldatalist.get(i + 5).trim());
			error += serverAppUtility.validateNumericCloumn(exceldatalist.get(6).trim(),
					exceldatalist.get(i + 6).trim());
			error += serverAppUtility.validateNumericCloumn(exceldatalist.get(7).trim(),
					exceldatalist.get(i + 7).trim());
			error += serverAppUtility.validateNumericCloumn(exceldatalist.get(8).trim(),
					exceldatalist.get(i + 8).trim());
			error += serverAppUtility.validateNumericCloumn(exceldatalist.get(9).trim(),
					exceldatalist.get(i + 9).trim());
			error += serverAppUtility.validateNumericCloumn(exceldatalist.get(10).trim(),
					exceldatalist.get(i + 10).trim());

			error += serverAppUtility.validateFormulla(exceldatalist.get(0).trim(), exceldatalist.get(i).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(1).trim(), exceldatalist.get(i + 1).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(2).trim(), exceldatalist.get(i + 2).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(3).trim(), exceldatalist.get(i + 3).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(4).trim(), exceldatalist.get(i + 4).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(5).trim(), exceldatalist.get(i + 5).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(6).trim(), exceldatalist.get(i + 6).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(7).trim(), exceldatalist.get(i + 7).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(8).trim(), exceldatalist.get(i + 8).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(9).trim(), exceldatalist.get(i + 9).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(10).trim(), exceldatalist.get(i + 10).trim());

			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(0).trim(),
					exceldatalist.get(i).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(1).trim(),
					exceldatalist.get(i + 1).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(2).trim(),
					exceldatalist.get(i + 2).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(3).trim(),
					exceldatalist.get(i + 3).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(4).trim(),
					exceldatalist.get(i + 4).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(5).trim(),
					exceldatalist.get(i + 5).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(6).trim(),
					exceldatalist.get(i + 6).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(7).trim(),
					exceldatalist.get(i + 7).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(8).trim(),
					exceldatalist.get(i + 8).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(9).trim(),
					exceldatalist.get(i + 9).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(10).trim(),
					exceldatalist.get(i + 10).trim());

			error += validateproduct(itemproductlist, exceldatalist.get(i).trim(), exceldatalist.get(i + 1).trim());

			error += validateWarehouse(warehouselist, exceldatalist.get(i + 2).trim());
			error += validateLocation(storagelocationlist, exceldatalist.get(i + 3).trim(),
					exceldatalist.get(i + 2).trim());
			error += validateStorageBin(storageBinlist, exceldatalist.get(i + 4).trim(),
					exceldatalist.get(i + 2).trim(), exceldatalist.get(i + 3).trim());

			if (!error.trim().equals("")) {
				if (!error.equals("")) {
					errorList.add(exceldatalist.get(i));
					errorList.add(error);

					ProductInventoryView productinventoryview = new ProductInventoryView();
					productinventoryview.setCount(-1);
					productinventoryviewlist.add(productinventoryview);
				}
			}
		}

		if (errorList != null && errorList.size() > 0) {
			CsvWriter.stockUploadErrorlist = errorList;
			return productinventoryviewlist;
		}

		String msg = callTaskQueueForStockUploadAndUpdate(companyId);

		logger.log(Level.SEVERE, "msg " + msg);

		return productinventoryviewlist;
	}

	private String validateproduct(List<ItemProduct> itemproductlist, String productId, String productName) {
		try {

			if (!productId.equalsIgnoreCase("NA")) {
				for (ItemProduct itempproduct : itemproductlist) {
					if (itempproduct.getCount() == Integer.parseInt(productId)) {
						if (itempproduct.getProductName().trim().equals(productName.trim())) {
							return "";
						} else {
							return " Product name does not match with product id ";
						}
					}
				}
				return " Product id does not exist! ";
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		return "";

	}

	private String validateStorageBin(List<Storagebin> storageBinlist, String storageBinName, String warehouseName,
			String storagelocation) {
		for (Storagebin storageBin : storageBinlist) {
			if (storageBin.getBinName().trim().equals(storageBinName.trim())
					&& storageBin.getWarehouseName().trim().equals(warehouseName.trim())
					&& storageBin.getStoragelocation().trim().equals(storagelocation.trim())) {
				return "";
			}
		}
		return " Storage bin does not exist - " + storageBinName + " ";
	}

	private String validateLocation(List<StorageLocation> storagelocationlist, String storageLocationName,
			String warehouseName) {
		for (StorageLocation storageLocation : storagelocationlist) {
			if (storageLocation.getBusinessUnitName().trim().equals(storageLocationName.trim())
					&& storageLocation.getWarehouseName().trim().equals(warehouseName.trim())) {
				return "";
			}
		}
		return " Storage location does not exist - " + storageLocationName + " ";

	}

	private String validateWarehouse(List<WareHouse> warehouselist, String warehouseName) {

		for (WareHouse warehouseEntity : warehouselist) {
			if (warehouseEntity.getBusinessUnitName().trim().equals(warehouseName.trim())) {
				return "";
			}
		}
		return " Warehouse does not exist - " + warehouseName + " ";
	}

	private String callTaskQueueForStockUploadAndUpdate(long companyId) {
		logger.log(Level.SEVERE, "stock Upload task queue calling");
		try {

			Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue")
					.param("EntityName", "Stock Upload").param("companyId", companyId + ""));

		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Exception ==" + e.getMessage());
			return "Failed please try again";
		}
		return "Stock Upload Process Started";
	}

	public void stockUpload(long companyId, ArrayList<String> exceldatalist) {

		HashSet<Integer> productId = new HashSet<Integer>();

		for (int i = 11; i < exceldatalist.size(); i += 11) {
			try {
				productId.add(Integer.parseInt(exceldatalist.get(i).trim()));
			} catch (Exception e) {
			}
		}
		ArrayList<Integer> productidlist = new ArrayList<Integer>(productId);

		List<ProductInventoryView> productinventorylist = ofy().load().type(ProductInventoryView.class)
				.filter("companyId", companyId).filter("productinfo.prodID IN", productidlist).list();
		logger.log(Level.SEVERE, "productinventorylist size" + productinventorylist.size());

		List<ItemProduct> itemproductlist = ofy().load().type(ItemProduct.class).filter("companyId", companyId)
				.filter("count IN", productidlist).list();
		logger.log(Level.SEVERE, "itemproductlist size" + itemproductlist.size());

		HashMap<Integer, ArrayList<ProductInventoryViewDetails>> productinventorymap = new HashMap<Integer, ArrayList<ProductInventoryViewDetails>>();

		for (int i = 11; i < exceldatalist.size(); i += 11) {

			ProductInventoryViewDetails prodinventoryview = new ProductInventoryViewDetails();
			prodinventoryview.setProdid(Integer.parseInt(exceldatalist.get(i).trim()));
			ItemProduct product = getProduct(itemproductlist, Integer.parseInt(exceldatalist.get(i).trim()));
			prodinventoryview.setProdcode(product.getProductCode());
			prodinventoryview.setProdname(product.getProductName());
			prodinventoryview.setWarehousename(exceldatalist.get(i + 2).trim());
			prodinventoryview.setStoragelocation(exceldatalist.get(i + 3).trim());
			prodinventoryview.setStoragebin(exceldatalist.get(i + 4).trim());
			if (!exceldatalist.get(i + 5).trim().equalsIgnoreCase("NA"))
				prodinventoryview.setMinqty(Double.parseDouble(exceldatalist.get(i + 5).trim()));
			if (!exceldatalist.get(i + 6).trim().equalsIgnoreCase("NA"))
				prodinventoryview.setMaxqty(Double.parseDouble(exceldatalist.get(i + 6).trim()));
			if (!exceldatalist.get(i + 7).trim().equalsIgnoreCase("NA"))
				prodinventoryview.setNormallevel(Double.parseDouble(exceldatalist.get(i + 7).trim()));
			if (!exceldatalist.get(i + 8).trim().equalsIgnoreCase("NA"))
				prodinventoryview.setReorderlevel(Double.parseDouble(exceldatalist.get(i + 8).trim()));
			if (!exceldatalist.get(i + 9).trim().equalsIgnoreCase("NA"))
				prodinventoryview.setReorderqty(Double.parseDouble(exceldatalist.get(i + 9).trim()));
			prodinventoryview.setAvailableqty(Double.parseDouble(exceldatalist.get(i + 10).trim()));
			if (product.getProductCategory() != null)
				prodinventoryview.setProductCategory(product.getProductCategory());

			if (productinventorymap != null && productinventorymap.size() != 0) {
				if (productinventorymap.containsKey(prodinventoryview.getProdid())) {
					productinventorymap.get(prodinventoryview.getProdid()).add(prodinventoryview);
				} else {
					ArrayList<ProductInventoryViewDetails> list = new ArrayList<ProductInventoryViewDetails>();
					list.add(prodinventoryview);
					productinventorymap.put(prodinventoryview.getProdid(), list);
				}
			} else {
				ArrayList<ProductInventoryViewDetails> list = new ArrayList<ProductInventoryViewDetails>();
				list.add(prodinventoryview);
				productinventorymap.put(prodinventoryview.getProdid(), list);
			}

		}

		logger.log(Level.SEVERE, "productinventorymap size" + productinventorymap.size());
//		ArrayList<ProductInventoryViewDetails> productinventoryviewdetailslist = new ArrayList<ProductInventoryViewDetails>();

		for (Map.Entry<Integer, ArrayList<ProductInventoryViewDetails>> entryset : productinventorymap.entrySet()) {

			ArrayList<ProductInventoryViewDetails> listproductinventorydetails = entryset.getValue();
			boolean newrecordFlag = true;

//			ArrayList<ProductInventoryViewDetails> newentrylist = new ArrayList<ProductInventoryViewDetails>();

			for (ProductInventoryViewDetails prodviewdetails : listproductinventorydetails) {
				for (ProductInventoryView productview : productinventorylist) {
					if (productview.getProdID() == listproductinventorydetails.get(0).getProdid()) {
						newrecordFlag = false;

						boolean newrecordinexistinglistFlag = true;
						for (ProductInventoryViewDetails prodinventoryviewdetails : productview.getDetails()) {
							if (prodinventoryviewdetails.getProdid() == prodviewdetails.getProdid()
									&& prodinventoryviewdetails.getProdcode().trim()
											.equals(prodviewdetails.getProdcode().trim())
									&& prodinventoryviewdetails.getWarehousename().trim()
											.equals(prodviewdetails.getWarehousename().trim())
									&& prodinventoryviewdetails.getStoragelocation().trim()
											.equals(prodviewdetails.getStoragelocation().trim())
									&& prodinventoryviewdetails.getStoragebin().trim()
											.equals(prodviewdetails.getStoragebin().trim())) {

								newrecordinexistinglistFlag = false;

								prodinventoryviewdetails.setMinqty(prodviewdetails.getMinqty());
								prodinventoryviewdetails.setMaxqty(prodviewdetails.getMaxqty());
								prodinventoryviewdetails.setNormallevel(prodviewdetails.getNormallevel());
								prodinventoryviewdetails.setReorderlevel(prodviewdetails.getReorderlevel());
								prodinventoryviewdetails.setReorderqty(prodviewdetails.getReorderqty());
								prodinventoryviewdetails.setAvailableqty(prodviewdetails.getAvailableqty());
								productview.setEditFlag(true);
							}
						}

						if (newrecordinexistinglistFlag) {
							productview.getDetails().add(prodviewdetails);
							productview.setEditFlag(true);

						}
					}
				}
			}

//			GenricServiceImpl serviceimpl = new GenricServiceImpl();
//			serviceimpl.save(productinventorylist);

			ofy().save().entities(productinventorylist);

			if (newrecordFlag) {
				logger.log(Level.SEVERE, "for new entry in product inventory view master");
				ProductInventoryView prodinventoryview = new ProductInventoryView();
				prodinventoryview.setDetails(listproductinventorydetails);
				prodinventoryview.setProdID(listproductinventorydetails.get(0).getProdid());
				prodinventoryview.setProductCode(listproductinventorydetails.get(0).getProdcode());
				prodinventoryview.setProductName(listproductinventorydetails.get(0).getProdname());
				prodinventoryview.setCompanyId(companyId);
				prodinventoryview.setCount(listproductinventorydetails.get(0).getProdid());

				if (getproductUOM(prodinventoryview.getProdID(), itemproductlist) != null)
					prodinventoryview.setProductUOM(getproductUOM(prodinventoryview.getProdID(), itemproductlist));

				ofy().save().entities(prodinventoryview);

			}
		}

	}

	private String getproductUOM(int prodID, List<ItemProduct> itemproductlist) {
		for (ItemProduct product : itemproductlist) {
			if (product.getCount() == prodID) {
				if (product.getUnitOfMeasurement() != null) {
					return product.getUnitOfMeasurement();
				}
			}
		}
		return null;

	}

	private ItemProduct getProduct(List<ItemProduct> itemproductlist, int productid) {

		for (ItemProduct product : itemproductlist) {
			if (product.getCount() == productid) {
				return product;
			}
		}

		return null;
	}

	@Override
	public ArrayList<Consumables> validateConsumables(long companyId, int customerId) {
		logger.log(Level.SEVERE, "validateConsumables : " + companyId + " / " + customerId);
		ServerAppUtility serverAppUtility = new ServerAppUtility();

		ArrayList<String> exceldatalist = serverAppUtility.readExcelAndGetArraylist(DataMigrationImpl.blobkey, null);
		if (exceldatalist != null) {
			logger.log(Level.SEVERE, "excel list after reading data exceldatalist size", exceldatalist.size());

			ArrayList<String> uploadfileHeader = new ArrayList<String>();
			uploadfileHeader.add("Site Location");
			uploadfileHeader.add("Cost Code");
			uploadfileHeader.add("Carpet Area");
			uploadfileHeader.add("Billing Type");
			uploadfileHeader.add("Budget");

			boolean validExcelfile = serverAppUtility.validateExcelSheet(exceldatalist, uploadfileHeader);
			if (validExcelfile) {
				ArrayList<Consumables> list = validateConsumableUploadFile(companyId, customerId, exceldatalist,
						serverAppUtility);
				return list;
			} else {
				logger.log(Level.SEVERE, "validExcelfile : " + validExcelfile);
				return null;
			}
		} else {
			logger.log(Level.SEVERE, "exceldatalist : " + exceldatalist);
			return null;
		}
	}

	private ArrayList<Consumables> validateConsumableUploadFile(long companyId, int customerId,
			ArrayList<String> exceldatalist, ServerAppUtility serverAppUtility) {
		ArrayList<Consumables> consumableList = new ArrayList<Consumables>();
		ArrayList<String> errorList = new ArrayList<String>();

		HashMap<String, Integer> siteLocationHmap = new HashMap<String, Integer>();

		for (int i = 5; i < exceldatalist.size(); i += 5) {
			logger.log(Level.SEVERE, "exceldatalist.get(columncount+1)" + exceldatalist.get(i));
			if (siteLocationHmap != null && siteLocationHmap.size() != 0) {
				if (siteLocationHmap.containsKey(exceldatalist.get(i))) {
					int count = siteLocationHmap.get(exceldatalist.get(i));
					count++;
					siteLocationHmap.put(exceldatalist.get(i), count);
					logger.log(Level.SEVERE, "count ==" + count);
				} else {
					siteLocationHmap.put(exceldatalist.get(i), 1);
				}
			} else {
				siteLocationHmap.put(exceldatalist.get(i), 1);
			}
		}

		for (Map.Entry<String, Integer> sitelocationName : siteLocationHmap.entrySet()) {
			logger.log(Level.SEVERE, "sitelocationName.getValue() " + sitelocationName.getValue());
			if (sitelocationName.getValue() > 1) {
				String error = "Site location " + sitelocationName.getKey() + " is added multiple times on excelsheet.";
				errorList.add("0");
				errorList.add(error);
			}
		}

		if (errorList.size() > 0) {
			Consumables consumable = new Consumables();
			consumable.setConsumbales("-1");
			consumableList.add(consumable);
		}

		List<CustomerBranchDetails> customerBranchList = ofy().load().type(CustomerBranchDetails.class)
				.filter("companyId", companyId).filter("cinfo.count", customerId).filter("status", true).list();
		if (customerBranchList != null && customerBranchList.size() != 0) {
			logger.log(Level.SEVERE, "customerBranchList size" + customerBranchList.size());
		}

		for (int i = 5; i < exceldatalist.size(); i += 5) {

			String error = "";

			error += serverAppUtility.validateMandatory(exceldatalist.get(i).trim(), exceldatalist.get(0).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 3).trim(), exceldatalist.get(3).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 4).trim(), exceldatalist.get(4).trim());

			error += serverAppUtility.validateNumericCloumn(exceldatalist.get(4).trim(),
					exceldatalist.get(i + 4).trim());

			error += serverAppUtility.validateFormulla(exceldatalist.get(0).trim(), exceldatalist.get(i).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(1).trim(), exceldatalist.get(i + 1).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(2).trim(), exceldatalist.get(i + 2).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(3).trim(), exceldatalist.get(i + 3).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(4).trim(), exceldatalist.get(i + 4).trim());

			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(0).trim(),
					exceldatalist.get(i).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(1).trim(),
					exceldatalist.get(i + 1).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(2).trim(),
					exceldatalist.get(i + 2).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(3).trim(),
					exceldatalist.get(i + 3).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(4).trim(),
					exceldatalist.get(i + 4).trim());

			error += validateSiteLocation(customerBranchList, exceldatalist.get(i).trim());
			error += validateBillType(exceldatalist.get(i + 3).trim());

			if (!error.trim().equals("")) {
				if (!error.equals("")) {
					errorList.add(exceldatalist.get(i));
					errorList.add(error);

					Consumables consumables = new Consumables();
					consumables.setConsumbales("-1");
					consumableList.add(consumables);
				}
			}
		}

		if (errorList != null && errorList.size() > 0) {
			CsvWriter.stockUploadErrorlist = errorList;
			return consumableList;
		}

		for (int i = 5; i < exceldatalist.size(); i += 5) {
			Consumables obj = new Consumables();
			obj.setConsumbales("Consumable");
			obj.setSiteLocation(exceldatalist.get(i).trim());
			obj.setCostCode(exceldatalist.get(i + 1).trim());
			obj.setBranchCarpetArea(exceldatalist.get(i + 2).trim());
			obj.setBillingType(exceldatalist.get(i + 3).trim().toUpperCase());
			if (obj.getBillingType().equals("ACTUALS")) {
				obj.setOpsBudget(exceldatalist.get(i + 4).trim());
			} else {
				obj.setChargable(exceldatalist.get(i + 4).trim());
			}
			consumableList.add(obj);
		}

		logger.log(Level.SEVERE, "All is well !!!");

		return consumableList;
	}

	private String validateBillType(String billType) {
		if (billType.equalsIgnoreCase("ACTUALS") || billType.equalsIgnoreCase("FIXED")) {
			return "";
		}
		return "Bill type should be either ACTUALS or FIXED.";
	}

	private String validateSiteLocation(List<CustomerBranchDetails> customerBranchList, String sitelocation) {
		try {
			for (CustomerBranchDetails customerBranch : customerBranchList) {
				if (customerBranch.getBusinessUnitName().trim().equals(sitelocation.trim())) {
					return "";
				}
			}
			return "Site location " + sitelocation + " does not exist on customer branch master.";

		} catch (Exception e) {
		}
		return "";
	}

	/**
	 * Added by sheetal:31:01:2022 Des: for user creation upload program
	 **/
	@Override
	public ArrayList<Employee> validateAndUploadUser(long companyId) {

		ServerAppUtility serverAppUtility = new ServerAppUtility();

		ArrayList<String> exceldatalist = serverAppUtility.readExcelAndGetArraylist(DataMigrationImpl.blobkey, null);
		if (exceldatalist != null) {
			logger.log(Level.SEVERE, "excel list after reading data exceldatalist size", exceldatalist.size());

			ArrayList<String> uploadfileHeader = new ArrayList<String>();
			uploadfileHeader.add("Employee Name");
			uploadfileHeader.add("Branch");
			uploadfileHeader.add("Mobile Number");
			uploadfileHeader.add("Email Id");
			uploadfileHeader.add("User Role");
			uploadfileHeader.add("User ID");
			uploadfileHeader.add("Password");

			boolean validExcelfile = serverAppUtility.validateExcelSheet(exceldatalist, uploadfileHeader);
			if (validExcelfile) {
				ArrayList<Employee> list = ValidateUserUploadFile(companyId, exceldatalist, serverAppUtility);
				return list;
			} else {
				return null;
			}
		} else {
			return null;
		}

	}

	private ArrayList<Employee> ValidateUserUploadFile(long companyId, ArrayList<String> exceldatalist,
			ServerAppUtility serverAppUtility) {

		ArrayList<Employee> userlist = new ArrayList<Employee>();
		ArrayList<String> errorList = new ArrayList<String>();
		HashSet<String> hsemployeename = new HashSet<String>();
		HashSet<String> hsuserRole = new HashSet<String>();

		for (int i = 7; i < exceldatalist.size(); i += 7) {
			hsemployeename.add(exceldatalist.get(i).trim());
			hsuserRole.add(exceldatalist.get(i + 4).trim());
		}

		ArrayList<String> employeelist = new ArrayList<String>(hsemployeename);
		List<Employee> listofemployee = ofy().load().type(Employee.class).filter("companyId", companyId)
				.filter("fullname IN", employeelist).list();
		System.out.println("Employee list sheetal" + listofemployee.size());
		logger.log(Level.SEVERE, "User creation Employee list :" + listofemployee.size());

		ArrayList<String> userRolelist = new ArrayList<String>(hsuserRole);
		List<UserRole> listofUserRole = ofy().load().type(UserRole.class).filter("companyId", companyId)
				.filter("roleName IN", userRolelist).list();

		for (int i = 7; i < exceldatalist.size(); i += 7) {
			String error = "";

			error += serverAppUtility.validateMandatory(exceldatalist.get(i).trim(), exceldatalist.get(0).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 1).trim(), exceldatalist.get(1).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 4).trim(), exceldatalist.get(4).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 5).trim(), exceldatalist.get(5).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 6).trim(), exceldatalist.get(6).trim());

			error += serverAppUtility.validateEmailId(exceldatalist.get(3).trim(), exceldatalist.get(i + 3).trim());
			error += serverAppUtility.validateNumericCloumn(exceldatalist.get(2).trim(),
					exceldatalist.get(i + 2).trim());

			error += serverAppUtility.validateFormulla(exceldatalist.get(0).trim(), exceldatalist.get(i).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(1).trim(), exceldatalist.get(i + 1).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(2).trim(), exceldatalist.get(i + 2).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(3).trim(), exceldatalist.get(i + 3).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(4).trim(), exceldatalist.get(i + 4).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(5).trim(), exceldatalist.get(i + 5).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(6).trim(), exceldatalist.get(i + 6).trim());

			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(0).trim(),
					exceldatalist.get(i).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(1).trim(),
					exceldatalist.get(i + 1).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(2).trim(),
					exceldatalist.get(i + 2).trim());

			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(4).trim(),
					exceldatalist.get(i + 4).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(5).trim(),
					exceldatalist.get(i + 5).trim());

			String strDuplicateEmp = validateDuplicateEmployee(listofemployee, exceldatalist.get(i).trim());
			if (!strDuplicateEmp.equals("")) {
				error += strDuplicateEmp;
			}
			String strUserRole = validateUserRole(listofUserRole, exceldatalist.get(i + 4).trim());
			if (!strUserRole.equals("")) {
				error += strUserRole;
			}
			if (!error.trim().equals("")) {
				if (!error.equals("")) {
					errorList.add(exceldatalist.get(i));
					errorList.add(error);
					logger.log(Level.SEVERE, "User Creation errors" + error);
					Employee employee = new Employee();
					employee.setCount(-1);
					userlist.add(employee);
				}
			}
		}

		if (errorList != null && errorList.size() > 0) {
			CsvWriter.UserUploadErrorlist = errorList;
			logger.log(Level.SEVERE, "csv call" + errorList.size());
			return userlist;
		}
		String msg = getuseruploadsave(companyId, exceldatalist);
		logger.log(Level.SEVERE, "msg " + msg);

		return userlist;

	}

	private String getuseruploadsave(long companyId2, ArrayList<String> exceldatalist) {
		ArrayList<Employee> employeelist = new ArrayList<Employee>();
		ArrayList<User> userlist = new ArrayList<User>();
		ArrayList<UserRole> userrolelist = new ArrayList<UserRole>();
		List<UserRole> listofUserRole = ofy().load().type(UserRole.class).filter("companyId", companyId2).list();
		logger.log(Level.SEVERE, "User role list :" + listofUserRole.size());

		for (int i = 7; i < exceldatalist.size(); i += 7) {
			Employee employee = new Employee();
			User user = new User();

			employee.setFullname(exceldatalist.get(i));
			user.setEmployeeName(exceldatalist.get(i));

			employee.setBranchName(exceldatalist.get(i + 1));
			user.setBranch(exceldatalist.get(i + 1));

			if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("NA")) {
				employee.setCellNumber1(null);
			} else {
				long cell1 = Long.parseLong(exceldatalist.get(i + 2).trim());
				employee.setCellNumber1(cell1);
			}

			employee.setEmail(exceldatalist.get(i + 3));
			user.setEmail(exceldatalist.get(i + 3));
			user.setRole(getuserRole(listofUserRole, exceldatalist.get(i + 4)));

			user.setUserName(exceldatalist.get(i + 5));
			user.setPassword(exceldatalist.get(i + 6));

			employee.setCompanyId(companyId2);
			user.setCompanyId(companyId2);
			user.setStatus(true);
//			userRole.setCompanyId(companyId2);

			employeelist.add(employee);
			userlist.add(user);

		}
		ArrayList<SuperModel> list = new ArrayList<SuperModel>();
		list.addAll(employeelist);
		ArrayList<SuperModel> list2 = new ArrayList<SuperModel>();
		list2.addAll(userlist);

		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(list);
		GenricServiceImpl impluser = new GenricServiceImpl();
		impluser.save(list2);

		return null;
	}

	private UserRole getuserRole(List<UserRole> listofUserRole, String rolename) {

		for (UserRole userrole : listofUserRole) {
			if (userrole.getRoleName().trim().equals(rolename.trim())) {
				logger.log(Level.SEVERE, "User Role");
				return userrole;
			}
		}

		return null;
	}

	private String validateDuplicateEmployee(List<Employee> employeelist, String employeeName) {
		String str = "";
		for (Employee employee : employeelist) {
			if (employee.getFullname().trim().equals(employeeName)) {
				str = "Employee Name already exist !";
				logger.log(Level.SEVERE, " Duplicate Employee" + str);
			}

		}
		return str;
	}

	private String validateUserRole(List<UserRole> userlist, String userRole) {
		for (UserRole user : userlist) {
			if (user.getRoleName().trim().equals(userRole)) {
				return "";
			}

		}

		logger.log(Level.SEVERE, " userRole does not exist " + userRole);
		return "User Role does not exist in the system !";
	}

	@Override
	public ArrayList<Service> validateAndUploadServiceCreation(long companyId) {

		ServerAppUtility serverAppUtility = new ServerAppUtility();

		ArrayList<String> exceldatalist = serverAppUtility.readExcelAndGetArraylist(DataMigrationImpl.blobkey,
				"dd/MM/yyyy");
		if (exceldatalist != null) {
			logger.log(Level.SEVERE, "excel list after reading data exceldatalist size", exceldatalist.size());

			ArrayList<String> uploadfileHeader = new ArrayList<String>();
			uploadfileHeader.add("Contract ID");
			uploadfileHeader.add("Customer Branch");
			uploadfileHeader.add("Service Location");
			uploadfileHeader.add("Area");
			uploadfileHeader.add("Service Date");
			uploadfileHeader.add("Technician User Id");
			uploadfileHeader.add("Product Code");
			uploadfileHeader.add("Type");
			uploadfileHeader.add("Quantity");
			boolean validExcelfile = serverAppUtility.validateExcelSheet(exceldatalist, uploadfileHeader);
			if (validExcelfile) {
				ArrayList<Service> list = ValidateServiceCreationUploadFile(companyId, exceldatalist, serverAppUtility);
				return list;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	private ArrayList<Service> ValidateServiceCreationUploadFile(long companyId, ArrayList<String> exceldatalist,
			ServerAppUtility serverAppUtility) {

		ArrayList<Service> servicelist = new ArrayList<Service>();
		ArrayList<String> errorList = new ArrayList<String>();

		HashSet<Integer> hscontractId = new HashSet<Integer>();
		HashSet<String> hsproductCode = new HashSet<String>();
		HashSet<String> hsuserId = new HashSet<String>();
		HashSet<String> hsType = new HashSet<String>();

		for (int i = 9; i < exceldatalist.size(); i += 9) {
			if (!exceldatalist.get(i).trim().equalsIgnoreCase("NA"))
				hscontractId.add(Integer.parseInt(exceldatalist.get(i).trim()));

			if (!exceldatalist.get(i + 6).trim().equalsIgnoreCase("NA")) {
				hsproductCode.add(exceldatalist.get(i + 6).trim());
			}
			if (!exceldatalist.get(i + 5).trim().equalsIgnoreCase("NA")) {
				hsuserId.add(exceldatalist.get(i + 5).trim());
			}

		}

		ArrayList<Integer> contractidarray = new ArrayList<Integer>(hscontractId);
		List<Contract> contractlist = ofy().load().type(Contract.class).filter("companyId", companyId)
				.filter("count IN", contractidarray).list();
		logger.log(Level.SEVERE, "contractlist size" + contractlist.size());

		ArrayList<String> serviceproductCode = new ArrayList<String>(hsproductCode);
		List<ServiceProduct> serviceproductlist = ofy().load().type(ServiceProduct.class).filter("companyId", companyId)
				.filter("productCode IN", serviceproductCode).list();
		ArrayList<String> userid = new ArrayList<String>(hsuserId);
		List<User> userlist = ofy().load().type(User.class).filter("companyId", companyId).filter("userName IN", userid)
				.list();

		List<Config> typelist = ofy().load().type(Config.class).filter("companyId", companyId).filter("type", 92)
				.list();

		for (int i = 9; i < exceldatalist.size(); i += 9) {

			String error = "";

			String validateContractAndCustomerBranchMsg = validateContractAndCustomerBranch(contractlist,
					exceldatalist.get(i).trim(), exceldatalist.get(i + 1).trim());
			if (!validateContractAndCustomerBranchMsg.equals("")) {
				error += validateContractAndCustomerBranchMsg;
			}
			String productValidationmsg = validateServieProduct(serviceproductlist, exceldatalist.get(i + 6).trim());
			if (!productValidationmsg.equals("")) {
				error += productValidationmsg;
			}

			String userIdValidationmsg = validateUserId(userlist, exceldatalist.get(i + 5).trim());
			if (!userIdValidationmsg.equals("")) {
				error += userIdValidationmsg;
			}

			error += serverAppUtility.validateConfigsNames(typelist, exceldatalist.get(i + 7).trim(),
					exceldatalist.get(7).trim());

			error += serverAppUtility.validateMandatory(exceldatalist.get(i).trim(), exceldatalist.get(0).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 4).trim(), exceldatalist.get(4).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 6).trim(), exceldatalist.get(6).trim());

			error += serverAppUtility.validateNumericCloumn(exceldatalist.get(0).trim(), exceldatalist.get(i).trim());
			error += serverAppUtility.validateNumericCloumn(exceldatalist.get(8).trim(),
					exceldatalist.get(i + 8).trim());

			error += serverAppUtility.validateFormulla(exceldatalist.get(0).trim(), exceldatalist.get(i).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(1).trim(), exceldatalist.get(i + 1).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(2).trim(), exceldatalist.get(i + 2).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(3).trim(), exceldatalist.get(i + 3).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(4).trim(), exceldatalist.get(i + 4).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(5).trim(), exceldatalist.get(i + 5).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(6).trim(), exceldatalist.get(i + 6).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(7).trim(), exceldatalist.get(i + 7).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(8).trim(), exceldatalist.get(i + 8).trim());

			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(0).trim(),
					exceldatalist.get(i).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(1).trim(),
					exceldatalist.get(i + 1).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(2).trim(),
					exceldatalist.get(i + 2).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(3).trim(),
					exceldatalist.get(i + 3).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(4).trim(),
					exceldatalist.get(i + 4).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(5).trim(),
					exceldatalist.get(i + 5).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(6).trim(),
					exceldatalist.get(i + 6).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(7).trim(),
					exceldatalist.get(i + 7).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(8).trim(),
					exceldatalist.get(i + 8).trim());

			logger.log(Level.SEVERE, "Service Date ==" + exceldatalist.get(i + 4).trim());
			error += serverAppUtility.validateDateFormat(exceldatalist.get(i + 4).trim(), "dd/MM/yyyy",
					exceldatalist.get(4).trim());
			error += serverAppUtility.validateDate(exceldatalist.get(i + 4).trim(), "dd/MM/yyyy",
					exceldatalist.get(4).trim());

//		String dateformat = serverAppUtility.validateDateFormat(exceldatalist.get(i+4).trim(), "dd-MM-yyyy", exceldatalist.get(4).trim());
//		if(!dateformat.equals("") && dateformat.length()>0) {
//			error += dateformat;
//		}

			if (!error.trim().equals("")) {
				if (!error.equals("")) {
					errorList.add(exceldatalist.get(i));
					errorList.add(error);

					Service service = new Service();
					service.setCount(-1);
					servicelist.add(service);
				}
			}

		}

		if (errorList != null && errorList.size() > 0) {
			CsvWriter.ServiceCreationUploadErrorlist = errorList;
			return servicelist;
		}

		String taskqueueMethodmsg = callTaskQueueForServiceCreationNoteUpload(companyId);

		logger.log(Level.SEVERE, "taskqueueMethodmsg " + taskqueueMethodmsg);

		return servicelist;

	}

	private String validateUserId(List<User> userlist, String userId) {

		for (User userEntity : userlist) {

			if (userEntity.getUserName().trim().equals(userId.trim())) {
				return "";
			}
		}

		return "User id does not exist in the erp.";
	}

	private String validateServieProduct(List<ServiceProduct> serviceproductlist, String productCode) {

		for (ServiceProduct serviceproduct : serviceproductlist) {
			if (serviceproduct.getProductCode().trim().equals(productCode.trim())) {
				return "";
			}
		}
		return "Service product code does not exist in erp.";
	}

	private String validateContractAndCustomerBranch(List<Contract> contractlist2, String strContractId,
			String CustomerBranchName) {

		Contract contract = getContract(contractlist2, strContractId);
		if (contract != null) {
			if (contract.getStatus().equals(Contract.APPROVED)) {
				if (!CustomerBranchName.equalsIgnoreCase("NA")) {

					CustomerBranchDetails customerBranch = ofy().load().type(CustomerBranchDetails.class)
							.filter("cinfo.count", contract.getCinfo().getCount())
							.filter("buisnessUnitName", CustomerBranchName).first().now();
					if (customerBranch == null) {
						return " Customer branch does not exist";
					} else {
						if (customerBranch != null && customerBranch.getStatus() == false) {
							return " Customer branch status is inactive";
						}
					}

				}
			} else {
				return "Contract status is not approved";

			}

		} else {
			return "Contract id does not exist!";
		}
		return "";
	}

	private Contract getContract(List<Contract> contractlist2, String strContractId) {
		logger.log(Level.SEVERE, "strContractId " + strContractId);
		int contractId = Integer.parseInt(strContractId);
		for (Contract contract : contractlist2) {
			if (contract.getCount() == contractId) {
				return contract;
			}
		}
		return null;
	}

//private String validateContractId(List<Contract> contractlist2, String strcontractId) {
//	
//	for(Contract contract : contractlist2) {
//		if(contract.getCount() == Integer.parseInt(strcontractId)) {
//			return "";
//		}
//	}
//	return "Contract id does not exist!";
//}

	private String callTaskQueueForServiceCreationNoteUpload(long companyId) {

		logger.log(Level.SEVERE, "Service Creation Upload task queue calling");
		try {

			Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue")
					.param("EntityName", AppConstants.SERVICECREATIONUPLOAD).param("companyId", companyId + ""));

		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Exception ==" + e.getMessage());
			return "Failed please try again";
		}
		return "Service Creation Upload Process Started";

	}

	public void ServiceCreationUpload(long companyId, ArrayList<String> exceldatalist) {

		SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		dateformat.setTimeZone(TimeZone.getTimeZone("IST"));

		HashSet<Integer> hscontractId = new HashSet<Integer>();
		HashSet<String> hsproductCode = new HashSet<String>();
		HashSet<String> hsuserId = new HashSet<String>();

		for (int i = 9; i < exceldatalist.size(); i += 9) {
			if (!exceldatalist.get(i).trim().equalsIgnoreCase("NA"))
				hscontractId.add(Integer.parseInt(exceldatalist.get(i).trim()));
			if (!exceldatalist.get(i + 6).trim().equalsIgnoreCase("NA")) {
				hsproductCode.add(exceldatalist.get(i + 6).trim());
			}
			if (!exceldatalist.get(i + 5).trim().equalsIgnoreCase("NA")) {
				hsuserId.add(exceldatalist.get(i + 5).trim());
			}
		}
		ArrayList<Integer> contractidarray = new ArrayList<Integer>(hscontractId);
		System.out.println("contractidarray size" + contractidarray.size());
		List<Contract> contractlist = ofy().load().type(Contract.class).filter("companyId", companyId)
				.filter("count IN", contractidarray).list();
		ArrayList<String> serviceproductCode = new ArrayList<String>(hsproductCode);
		List<ServiceProduct> serviceproductlist = ofy().load().type(ServiceProduct.class).filter("companyId", companyId)
				.filter("productCode IN", serviceproductCode).list();
		ArrayList<String> userid = new ArrayList<String>(hsuserId);
		List<User> userlist = ofy().load().type(User.class).filter("companyId", companyId).filter("userName IN", userid)
				.list();

		List<Service> servicelist = new ArrayList<Service>();
		Calendar cal = Calendar.getInstance();

		for (int i = 9; i < exceldatalist.size(); i += 9) {
			Service service = new Service();
			service.setCompanyId(companyId);
			Contract contract = getContract(contractlist, exceldatalist.get(i).trim());
			if (contract != null) {
				service.setPersonInfo(contract.getCinfo());
				service.setBranch(contract.getBranch());
				service.setContractStartDate(contract.getStartDate());
				service.setContractEndDate(contract.getEndDate());
				service.setContractCount(contract.getCount());

				if (!exceldatalist.get(i + 1).trim().equalsIgnoreCase("NA")) {
					service.setServiceBranch(exceldatalist.get(i + 1).trim());
				} else {
					service.setServiceBranch("Service Address");
				}

				if (!exceldatalist.get(i + 2).trim().equalsIgnoreCase("NA"))
					service.setServiceLocation(exceldatalist.get(i + 2).trim());
				if (!exceldatalist.get(i + 3).trim().equalsIgnoreCase("NA"))
					service.setServiceLocationArea(exceldatalist.get(i + 3).trim());
				try {
					if (!exceldatalist.get(i + 4).trim().equalsIgnoreCase("NA")) {
						logger.log(Level.SEVERE, "String Service Date " + exceldatalist.get(i + 4).trim());
						Date serviceDate = dateformat.parse(exceldatalist.get(i + 4).trim());
						logger.log(Level.SEVERE, "Final Service Date " + serviceDate);
						service.setServiceDate(serviceDate);
						service.setServiceCompletionDate(serviceDate);
					}

				} catch (ParseException e) {
					e.printStackTrace();
				}

				User userentity = getuser(userlist, exceldatalist.get(i + 5).trim());
				if (userentity != null && userentity.getEmployeeName() != null) {
					service.setEmployee(userentity.getEmployeeName());
				}

				if (!exceldatalist.get(i + 6).trim().equalsIgnoreCase("NA")) {
					ServiceProduct serviceproduct = getServiceproduct(serviceproductlist,
							exceldatalist.get(i + 6).trim());
					service.setProduct(serviceproduct);
				}

				service.setAdHocService(false);
				service.setServiceType(AppConstants.PERIODIC);

				ArrayList<CatchTraps> findinlist = new ArrayList<CatchTraps>();
				CatchTraps findings = new CatchTraps();
				if (!exceldatalist.get(i + 7).trim().equalsIgnoreCase("NA")) {
					findings.setPestName(exceldatalist.get(i + 7).trim());
				}
				try {
					if (!exceldatalist.get(i + 8).trim().equalsIgnoreCase("NA")) {
						findings.setCount(Integer.parseInt(exceldatalist.get(i + 8).trim()));
					}
				} catch (Exception e) {
				}

				String location = "";
				if (!exceldatalist.get(i + 1).trim().equalsIgnoreCase("NA")) {
					location = exceldatalist.get(i + 1).trim();
				}
				if (!exceldatalist.get(i + 2).trim().equalsIgnoreCase("NA")) {
					location = exceldatalist.get(i + 2).trim();
				}
				if (!exceldatalist.get(i + 3).trim().equalsIgnoreCase("NA")) {
					location = exceldatalist.get(i + 3).trim();
				}
				findings.setLocation(location);
				findinlist.add(findings);
				service.setCatchtrapList(findinlist);

				if (!exceldatalist.get(i + 1).trim().equalsIgnoreCase("NA")) {
					Address serviceAddress = getCustomerBranchServiceAddress(contract.getCinfo().getCount(),
							exceldatalist.get(i + 1).trim(), companyId);
					if (serviceAddress != null) {
						service.setAddress(serviceAddress);
					} else {
						Address serviceaddress = getCustomerServiceAddress(contract.getCinfo().getCount(), companyId);
						service.setAddress(serviceaddress);

					}
				} else {
					Address serviceAddress = getCustomerServiceAddress(contract.getCinfo().getCount(), companyId);
					service.setAddress(serviceAddress);
				}

				cal.setTime(DateUtility.getDateWithTimeZone("IST", service.getServiceDate()));
				int week = cal.get(Calendar.WEEK_OF_MONTH);
				service.setServiceWeekNo(week);
				service.setRefNo("");
				service.setStatus(Service.SERVICESTATUSCOMPLETED);
				SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
				String serviceDay = outFormat
						.format(DateUtility.getDateWithTimeZone("IST", (service.getServiceDate())));
				if (serviceDay != null) {
					service.setServiceDay(serviceDay);
				}
				servicelist.add(service);

			} else {
				logger.log(Level.SEVERE, "Contract not found for contract id " + exceldatalist.get(i).trim());
			}

		}

		ArrayList<SuperModel> list = new ArrayList<SuperModel>();
		list.addAll(servicelist);

		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(list);

		logger.log(Level.SEVERE, "Service created successfully");
	}

	private Address getCustomerServiceAddress(int customerId, long companyId) {
		Customer customerEntity = ofy().load().type(Customer.class).filter("count", customerId)
				.filter("companyId", companyId).first().now();
		if (customerEntity != null) {
			return customerEntity.getSecondaryAdress();
		}
		return null;
	}

	private Address getCustomerBranchServiceAddress(int customerId, String customerBranchName, long companyId) {
		CustomerBranchDetails customerBranch = ofy().load().type(CustomerBranchDetails.class)
				.filter("cinfo.count", customerId).filter("buisnessUnitName", customerBranchName)
				.filter("companyId", companyId).first().now();
		if (customerBranch != null) {
			return customerBranch.getAddress();
		}
		return null;
	}

	private User getuser(List<User> userlist, String userId) {
		for (User userentity : userlist) {
			if (userentity.getUserName().trim().equals(userId.trim())) {
				return userentity;
			}
		}
		return null;
	}

	private ServiceProduct getServiceproduct(List<ServiceProduct> serviceproductlist, String productCode) {

		for (ServiceProduct serviceproduct : serviceproductlist) {
			if (serviceproduct.getProductCode().trim().equals(productCode.trim())) {
				return serviceproduct;
			}
		}
		return null;
	}

	/**
	 * @author Sheetal : 02-06-2022 Des : Upload program for customer branch with
	 *         service location and area
	 **/
	@Override
	public ArrayList<CustomerBranchServiceLocation> uploadCustomerBranchWithLocationandArea(long companyId) {

		ServerAppUtility serverAppUtility = new ServerAppUtility();

		ArrayList<String> exceldatalist = serverAppUtility.readExcelAndGetArraylist(DataMigrationImpl.blobkey, null);
		if (exceldatalist != null) {
			logger.log(Level.SEVERE, "excel list after reading data exceldatalist size", exceldatalist.size());

			ArrayList<String> uploadfileHeader = new ArrayList<String>();
			uploadfileHeader.add("*Customer ID");
			uploadfileHeader.add("*Branch ID");
			uploadfileHeader.add("Service Location");
			uploadfileHeader.add("Area");

			boolean validExcelfile = serverAppUtility.validateExcelSheet(exceldatalist, uploadfileHeader);
			if (validExcelfile) {
				ArrayList<CustomerBranchServiceLocation> list = ValidateCustomerBranchWithLocationFile(companyId,
						exceldatalist, serverAppUtility);
				return list;
			} else {
				return null;
			}
		} else {
			return null;
		}

	}

	private ArrayList<CustomerBranchServiceLocation> ValidateCustomerBranchWithLocationFile(long companyId,
			ArrayList<String> exceldatalist, ServerAppUtility serverAppUtility) {

		ArrayList<String> errorList = new ArrayList<String>();

		ArrayList<Integer> custIdList = new ArrayList<Integer>();
		ArrayList<Integer> custbranchId = new ArrayList<Integer>();

		for (int i = 4; i < exceldatalist.size(); i += 4) {
			try {
				custIdList.add(Integer.parseInt(exceldatalist.get(i)));
			} catch (Exception e) {

			}

			try {
				custbranchId.add(Integer.parseInt(exceldatalist.get(i + 1)));
			} catch (Exception e) {

			}
		}
		ArrayList<CustomerBranchServiceLocation> branchlist = new ArrayList<CustomerBranchServiceLocation>();

		List<CustomerBranchServiceLocation> customerBranchServicelocationlist = new ArrayList<CustomerBranchServiceLocation>();

		List<Customer> listofCustomer = new ArrayList<Customer>();
		if (custIdList.size() != 0) {
			customerBranchServicelocationlist = ofy().load().type(CustomerBranchServiceLocation.class)
					.filter("companyId", companyId).filter("cinfo.count IN", custIdList)
					.filter("custBranchId IN", custbranchId).list();
			listofCustomer = ofy().load().type(Customer.class).filter("companyId", companyId)
					.filter("count IN", custIdList).list();
		} else {
			customerBranchServicelocationlist = ofy().load().type(CustomerBranchServiceLocation.class)
					.filter("companyId", companyId).list();
			listofCustomer = ofy().load().type(Customer.class).filter("companyId", companyId).list();
		}
		List<CustomerBranchDetails> listofbranch = new ArrayList<CustomerBranchDetails>();
		if (custIdList.size() != 0) {
			listofbranch = ofy().load().type(CustomerBranchDetails.class).filter("companyId", companyId)
					.filter("cinfo.count IN", custIdList).filter("count IN", custbranchId).list();
		} else {
			listofbranch = ofy().load().type(CustomerBranchDetails.class).filter("companyId", companyId).list();

		}
		CustomerBranchServiceLocation custbranchlocation = new CustomerBranchServiceLocation();

		for (int i = 4; i < exceldatalist.size(); i += 4) {

			String error = "";

			error += serverAppUtility.validateMandatory(exceldatalist.get(i).trim(), exceldatalist.get(0).trim());
			error += serverAppUtility.validateMandatory(exceldatalist.get(i + 1).trim(), exceldatalist.get(1).trim());

			error += serverAppUtility.validateNumericCloumn(exceldatalist.get(0).trim(), exceldatalist.get(i).trim());
			error += serverAppUtility.validateNumericCloumn(exceldatalist.get(1).trim(),
					exceldatalist.get(i + 1).trim());

			error += serverAppUtility.validateFormulla(exceldatalist.get(0).trim(), exceldatalist.get(i).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(1).trim(), exceldatalist.get(i + 1).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(2).trim(), exceldatalist.get(i + 2).trim());
			error += serverAppUtility.validateFormulla(exceldatalist.get(3).trim(), exceldatalist.get(i + 3).trim());

			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(0).trim(),
					exceldatalist.get(i).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(1).trim(),
					exceldatalist.get(i + 1).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(2).trim(),
					exceldatalist.get(i + 2).trim());
			error += serverAppUtility.validateSpecialCharComma(exceldatalist.get(3).trim(),
					exceldatalist.get(i + 3).trim());

			boolean validatecustID = getcustomerIDvalidation(listofbranch, exceldatalist, i);

			boolean validatecustBranchID = getcustomerBranchIDvalidation(listofbranch, exceldatalist, i);

			CustomerBranchServiceLocation customerbranchservicelocation = getcustomerBranchServiceLocation(
					customerBranchServicelocationlist, exceldatalist.get(i).trim(), exceldatalist.get(i + 1).trim(),
					exceldatalist.get(i + 2).trim());
			if (customerbranchservicelocation != null) {
				error += "Service Location already exist !! ";
				custbranchlocation.setCount(-1);
			}

			if (validatecustBranchID == false) {
				error += "Customer branch ID does not exist !!";
				custbranchlocation.setCount(-1);
			}

			if (validatecustID == false) {
				error += "Customer does not exist !! ";
				custbranchlocation.setCount(-1);
			}

			if (!error.trim().equals("")) {
				if (!error.equals("")) {
					errorList.add(exceldatalist.get(i));
					errorList.add(exceldatalist.get(i + 1));
					errorList.add(error);
					branchlist.add(custbranchlocation);
				}
			}
		}

		if (errorList != null && errorList.size() > 0) {
			CsvWriter.CustomerBranchWithServiceLocationErrorlist = errorList;
			return branchlist;
		}

		String msg = TaskQueueForCustomerBranchWithServiceLocationUpload(companyId);
		logger.log(Level.SEVERE, "msg for customer branch upload::" + msg);

		return branchlist;
	}

	private String TaskQueueForCustomerBranchWithServiceLocationUpload(long companyId) {

		logger.log(Level.SEVERE, "Customer Branch with Service Location Upload task queue calling");
		try {

			Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue")
					.param("EntityName", "Customer Branch with Service Location and area Upload")
					.param("companyId", companyId + ""));

		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Exception ==" + e.getMessage());
			return "Failed please try again";
		}
		return "Customer Branch with Service Location and area Upload Process Started";
	}

	public void CusBranchServiceLocationUpload(long companyId, ArrayList<String> exceldatalist) {

		List<CustomerBranchDetails> listofbranch = new ArrayList<CustomerBranchDetails>();

		ArrayList<Integer> custIdList = new ArrayList<Integer>();
		ArrayList<Integer> custbranchId = new ArrayList<Integer>();

		for (int i = 4; i < exceldatalist.size(); i += 4) {
			try {
				custIdList.add(Integer.parseInt(exceldatalist.get(i)));
			} catch (Exception e) {

			}

			try {
				custbranchId.add(Integer.parseInt(exceldatalist.get(i + 1)));
			} catch (Exception e) {

			}
		}

		if (custIdList.size() != 0) {
			listofbranch = ofy().load().type(CustomerBranchDetails.class).filter("companyId", companyId)
					.filter("cinfo.count IN", custIdList).filter("count IN", custbranchId).list();
		} else {
			listofbranch = ofy().load().type(CustomerBranchDetails.class).filter("companyId", companyId).list();

		}

		ArrayList<CustomerBranchServiceLocation> servicelocationlist = new ArrayList<CustomerBranchServiceLocation>();

		for (int i = 4; i < exceldatalist.size(); i += 4) {
			CustomerBranchServiceLocation custBranchServiceLocEntity = new CustomerBranchServiceLocation();

			CustomerBranchDetails customerBranchDetails = getcustomerBranch(listofbranch, exceldatalist.get(i).trim(),
					exceldatalist.get(i + 1).trim());

			custBranchServiceLocEntity.setCinfo(customerBranchDetails.getCinfo());
			custBranchServiceLocEntity.setCustomerBranchName(customerBranchDetails.getBusinessUnitName());
			custBranchServiceLocEntity.setCustBranchId(customerBranchDetails.getCount());
			custBranchServiceLocEntity.setCompanyId(customerBranchDetails.getCompanyId());
			custBranchServiceLocEntity.setStatus(true);
			if (!exceldatalist.get(i + 2).trim().equalsIgnoreCase("NA"))
				custBranchServiceLocEntity.setServiceLocation(exceldatalist.get(i + 2).trim());
			ArrayList<String> arealist = new ArrayList<String>();

			if (!exceldatalist.get(i + 3).trim().equalsIgnoreCase("NA")) {
				arealist.add(exceldatalist.get(i + 3).trim());
			}
			custBranchServiceLocEntity.setAreaList(arealist);
			servicelocationlist.add(custBranchServiceLocEntity);

		}

		HashMap<String, ArrayList<CustomerBranchServiceLocation>> servicelocationHashMap = new HashMap<String, ArrayList<CustomerBranchServiceLocation>>();

		for (CustomerBranchServiceLocation customerbranchservicelocation : servicelocationlist) {

			String custidBranchIdServicelocation = customerbranchservicelocation.getCinfo().getCount()
					+ customerbranchservicelocation.getCustBranchId()
					+ customerbranchservicelocation.getServiceLocation();

			if (servicelocationHashMap != null && servicelocationHashMap.size() != 0) {
				if (servicelocationHashMap.containsKey(custidBranchIdServicelocation)) {

					servicelocationHashMap.get(custidBranchIdServicelocation).add(customerbranchservicelocation);
				} else {
					ArrayList<CustomerBranchServiceLocation> list = new ArrayList<CustomerBranchServiceLocation>();
					list.add(customerbranchservicelocation);

					servicelocationHashMap.put(custidBranchIdServicelocation, list);
				}
			} else {
				ArrayList<CustomerBranchServiceLocation> list = new ArrayList<CustomerBranchServiceLocation>();
				list.add(customerbranchservicelocation);

				servicelocationHashMap.put(custidBranchIdServicelocation, list);
			}

		}

		List<CustomerBranchServiceLocation> updatedservicelocationlist = new ArrayList<CustomerBranchServiceLocation>();

		for (Map.Entry<String, ArrayList<CustomerBranchServiceLocation>> entryset : servicelocationHashMap.entrySet()) {
			System.out.println("entryset.getkey" + entryset.getKey());

			logger.log(Level.SEVERE, "entryset.getkey" + entryset.getKey());
			ArrayList<CustomerBranchServiceLocation> listservicelocation = entryset.getValue();
			logger.log(Level.SEVERE, "listservicelocation size==" + listservicelocation.size());

			if (listservicelocation.size() > 0) {
				CustomerBranchServiceLocation servicelocation = new CustomerBranchServiceLocation();

				ArrayList<String> arealist = new ArrayList<String>();
				for (CustomerBranchServiceLocation custbranchservicelocation : listservicelocation) {
					if (custbranchservicelocation.getAreaList() != null
							&& custbranchservicelocation.getAreaList().size() > 0)
						arealist.addAll(custbranchservicelocation.getAreaList());
				}
				logger.log(Level.SEVERE, "arealist size==" + arealist.size());
				servicelocation.setCinfo(listservicelocation.get(0).getCinfo());
				servicelocation.setCustomerBranchName(listservicelocation.get(0).getCustomerBranchName());
				servicelocation.setCustBranchId(listservicelocation.get(0).getCustBranchId());
				servicelocation.setCompanyId(listservicelocation.get(0).getCompanyId());
				servicelocation.setStatus(true);
				servicelocation.setServiceLocation(listservicelocation.get(0).getServiceLocation());

				servicelocation.setAreaList(arealist);

				updatedservicelocationlist.add(servicelocation);
			}

		}
		logger.log(Level.SEVERE, "updatedservicelocationlist size" + updatedservicelocationlist.size());

		if (updatedservicelocationlist.size() > 0) {
			ArrayList<SuperModel> list = new ArrayList<SuperModel>();
			list.addAll(updatedservicelocationlist);

			GenricServiceImpl implsave = new GenricServiceImpl();
			implsave.save(list);
			logger.log(Level.SEVERE, "save successfully " + updatedservicelocationlist.size());

		}

	}

	private CustomerBranchDetails getcustomerBranch(List<CustomerBranchDetails> listofbranch, String custid,
			String custbranchid) {

		int customerId = Integer.parseInt(custid);
		int customerbranchid = Integer.parseInt(custbranchid);

		for (CustomerBranchDetails custbranchDetails : listofbranch) {
			if (custbranchDetails.getCinfo().getCount() == customerId
					&& custbranchDetails.getCount() == customerbranchid) {
				return custbranchDetails;
			}
		}
		return null;

	}

	private CustomerBranchServiceLocation getcustomerBranchServiceLocation(
			List<CustomerBranchServiceLocation> listofcustomerbranchServicelocation, String custid, String custbranchid,
			String servicelocation) {

		int customerId = Integer.parseInt(custid);
		int customerbranchid = Integer.parseInt(custbranchid);

		for (CustomerBranchServiceLocation custbranchDetails : listofcustomerbranchServicelocation) {
			if (custbranchDetails.getCinfo().getCount() == customerId
					&& custbranchDetails.getCustBranchId() == customerbranchid
					&& custbranchDetails.getServiceLocation().trim().equals(servicelocation)) {
				return custbranchDetails;
			}
		}
		return null;

	}

	private boolean getcustomerIDvalidation(List<CustomerBranchDetails> listofbranch, ArrayList<String> exceldatalist,
			int i) {

		boolean value = false;
		for (int j = 0; j < listofbranch.size(); j++) {
			if (listofbranch.get(j).getCinfo().getCount() == Integer.parseInt(exceldatalist.get(i).trim())) {
				System.out.println("inside customer ID validation method");
				value = true;
				System.out.println("The Value stage 1: " + value);
				break;
			}
		}
		System.out.println("The Value stage 2 : " + value);
		return value;

	}

	private boolean getcustomerBranchIDvalidation(List<CustomerBranchDetails> listofbranch,
			ArrayList<String> exceldatalist, int i) {

		boolean value = false;
		for (int j = 0; j < listofbranch.size(); j++) {
			if (listofbranch.get(j).getCount() == Integer.parseInt(exceldatalist.get(i + 1).trim())) {
				System.out.println("inside cust branch id validation method");
				value = true;
				System.out.println("The Value stage 1: " + value);
				break;
			}
		}
		System.out.println("The Value stage 2 : " + value);
		return value;

	}

	public void customerBranchUpload(long companyId, ArrayList<String> exceldatalist) {
		Company companyEntity = ofy().load().type(Company.class).filter("companyId", companyId).first().now();

		try {
		/**
		 * @author Anil @since 10-05-2021 Due to large customer database, customer
		 *         branch is not uploading on pecopp link so loading only specific
		 *         customers
		 */
		ArrayList<Integer> custIdList = new ArrayList<Integer>();
		for (int i = 31; i < exceldatalist.size(); i += 31) {
			try {
				custIdList.add(Integer.parseInt(exceldatalist.get(i).trim()));
			} catch (Exception e) {

			}
		}
		logger.log(Level.SEVERE, "Cust Id List size : " + custIdList.size());

		ArrayList<CustomerBranchDetails> branchlist = new ArrayList<CustomerBranchDetails>();

		ArrayList<String> errorList = new ArrayList<String>();

		/**
		 * @author Anil @since 10-05-2021
		 */
		List<CustomerBranchDetails> listofbranch = new ArrayList<CustomerBranchDetails>();
		List<Customer> listofCustomer = new ArrayList<Customer>();
		if (custIdList.size() != 0) {
			listofbranch = ofy().load().type(CustomerBranchDetails.class).filter("companyId", companyId)
					.filter("cinfo.count IN", custIdList).list();
			listofCustomer = ofy().load().type(Customer.class).filter("companyId", companyId)
					.filter("count IN", custIdList).list();
		} else {
			listofbranch = ofy().load().type(CustomerBranchDetails.class).filter("companyId", companyId).list();
			listofCustomer = ofy().load().type(Customer.class).filter("companyId", companyId).list();
		}
		/***
		 * Date 06/07/2018 By Vijay For global list loading if not loaded for validation
		 * purpose
		 */
		if (globalBranchlist == null) {
			List<Branch> branchData = ofy().load().type(Branch.class).filter("companyId", companyId).list();
			globalBranchlist = branchData;
		}
		if (globalCitylist == null) {
			List<City> cityData = ofy().load().type(City.class).filter("companyId", companyId).list();
			globalCitylist = cityData;
		}

		
		errorList.add("Customer Id");
		errorList.add("Branch");
		errorList.add("Remark");

		boolean errorreportflag = false;

		for (int i = 31; i < exceldatalist.size(); i += 31) {

			String error = "";
			CustomerBranchDetails branch = new CustomerBranchDetails();

			boolean validcheck = validationCustomerBranch(listofbranch, exceldatalist.get(i), exceldatalist.get(i + 1));
			boolean validateCustomer = validationCustomer(listofCustomer, exceldatalist, i);

			System.out.println("The value of validcheck is : " + validcheck);

			try {
				branch.getCinfo().setCount(Integer.parseInt(exceldatalist.get(i)));
			} catch (Exception e) {
				error += "Please enter valid customer ID / ";
				branch.setCount(-1);
			}
			String fullname = "";
			Long cellNumber = 0l;
			String pocName = "";
			/** new **/
			for (Customer customer : listofCustomer) {
				if (customer.getCount() == Integer.parseInt(exceldatalist.get(i))) {

					if (customer.isCompany()) {
						fullname = customer.getCompanyName();
						pocName = customer.getFullname();
					} else {
						fullname = customer.getFullname();
					}

					cellNumber = customer.getCellNumber1();
					break;
				}
			}
			branch.getCinfo().setFullName(fullname);
			branch.getCinfo().setCellNumber(cellNumber);
			branch.getCinfo().setPocName(pocName);

			if (exceldatalist.get(i + 1).equalsIgnoreCase("na")) {
				// branch.setBusinessUnitName("");
				error += "Branch Name is Mamdatory / ";
				branch.setCount(-1);

			} else {
				branch.setBusinessUnitName(exceldatalist.get(i + 1));

			}

			if (validcheck == false) {
				error += "Customer branch is already exist / ";
				branch.setCount(-1);
			}

			if (validateCustomer == false) {
				error += "Customer does not exist / ";
				branch.setCount(-1);
			}

			if (exceldatalist.get(i + 2).equalsIgnoreCase("na")) {
				branch.setEmail("");
			} else {

				branch.setEmail(exceldatalist.get(i + 2));
			}

			if (exceldatalist.get(i + 3).equalsIgnoreCase("na")) {
				// branch.setBranch("");
				error += "Servicing Branch is Mandatory / ";
				branch.setCount(-1);
			} else {
				boolean servicingBranchValidation = ValidateServiceBranch(exceldatalist.get(i + 3).trim());
				if (servicingBranchValidation == false) {
					error += "Servicing Branch - " + exceldatalist.get(i + 3).trim()
							+ " does not exist or does not match with database Please check /  ";
					branch.setCount(-1);
				}
				branch.setBranch(exceldatalist.get(i + 3));
			}

			if (exceldatalist.get(i + 4).equalsIgnoreCase("na")) {
				// branch.setCellNumber1(0l);
				error += "Cell Phone No 1 is Mandatory / ";
				branch.setCount(-1);
			} else {
				try {
					branch.setCellNumber1(Long.parseLong(exceldatalist.get(i + 4)));
				} catch (Exception e) {
					error += "Please enter valid Cell Phone No 1 / ";
					branch.setCount(-1);
				}
			}

			if (exceldatalist.get(i + 5).equalsIgnoreCase("na")) {
				branch.setGSTINNumber("");
			} else {

				branch.setGSTINNumber(exceldatalist.get(i + 5));
			}
			if (exceldatalist.get(i + 6).equalsIgnoreCase("na")) {
				branch.setPocName("");
			} else {

				branch.setPocName(exceldatalist.get(i + 6));
			}
			if (exceldatalist.get(i + 7).equalsIgnoreCase("na")) {
				branch.setPocLandline(0l);
			} else {
				try {
					branch.setPocLandline(Long.parseLong(exceldatalist.get(i + 7)));
				} catch (Exception e) {
					error += "Please enter valid Point Of Contact Landline / ";
					branch.setCount(-1);
				}
			}
			if (exceldatalist.get(i + 8).equalsIgnoreCase("na")) {
				branch.setPocCell(0l);
			} else {
				try {
					branch.setPocCell(Long.parseLong(exceldatalist.get(i + 8)));
				} catch (Exception e) {
					error += "Please enter valid Point Of Contact Cell / ";
					branch.setCount(-1);
				}
			}
			if (exceldatalist.get(i + 9).equalsIgnoreCase("na")) {
				branch.setPocEmail("");
			} else {

				branch.setPocEmail(exceldatalist.get(i + 9));
			}

			Address address = new Address();
			if (exceldatalist.get(i + 10).equalsIgnoreCase("na")) {
				// address.setAddrLine1("");
				error += "Address Line 1 is Mandatory / ";
				branch.setCount(-1);
			} else {
				address.setAddrLine1(exceldatalist.get(i + 10));
			}
			if (exceldatalist.get(i + 11).equalsIgnoreCase("na")) {
				address.setAddrLine2("");
			} else {
				address.setAddrLine2(exceldatalist.get(i + 11));
			}
			if (exceldatalist.get(i + 12).equalsIgnoreCase("na")) {
				address.setLandmark("");
			} else {
				address.setLandmark(exceldatalist.get(i + 12));
			}
			if (exceldatalist.get(i + 13).equalsIgnoreCase("na")) {
				address.setLocality("");
			} else {
				address.setLocality(exceldatalist.get(i + 13));
			}

			if (exceldatalist.get(i + 14).equalsIgnoreCase("na")) {
				// address.setCity("");
				error += "City is Mandatory / ";
				branch.setCount(-1);
			} else {

				/**
				 * Date 06-07-2018 by Vijay Des :- for servicing City validation
				 */
				boolean cityValidation = ValidateCity(exceldatalist.get(i + 14).trim());
				if (cityValidation == false) {
					error += exceldatalist.get(i + 14).trim() + "-"
							+ "City does not exist or does not match Please check / ";
					branch.setCount(-1);
				}
				/**
				 * ends here
				 */

				address.setCity(exceldatalist.get(i + 14));
			}

			if (exceldatalist.get(i + 15).equalsIgnoreCase("na")) {
				// address.setState("");
				error += "State is Mandatory / ";
				branch.setCount(-1);
			} else {
				address.setState(exceldatalist.get(i + 15));
			}
			if (exceldatalist.get(i + 16).equalsIgnoreCase("na")) {
				// address.setCountry("");
				error += "Country is Mandatory / ";
				branch.setCount(-1);
			} else {
				address.setCountry(exceldatalist.get(i + 16));
			}

			if (exceldatalist.get(i + 17).equalsIgnoreCase("na")) {
				address.setPin(0l);
			} else {
				try {
					address.setPin(Long.parseLong(exceldatalist.get(i + 17)));
				} catch (Exception e) {
					error += "Please enter valid Pin Code / ";
					branch.setCount(-1);
				}

			}

			Address address1 = new Address();
			if (exceldatalist.get(i + 18).equalsIgnoreCase("na")) {
				error += "Billing Address Line 1 is Mandatory / ";
				branch.setCount(-1);
			} else {
				address1.setAddrLine1(exceldatalist.get(i + 18));
			}
			if (exceldatalist.get(i + 19).equalsIgnoreCase("na")) {
				address1.setAddrLine2("");
			} else {
				address1.setAddrLine2(exceldatalist.get(i + 19));
			}
			if (exceldatalist.get(i + 20).equalsIgnoreCase("na")) {
				address1.setLandmark("");
			} else {
				address1.setLandmark(exceldatalist.get(i + 20));
			}
			if (exceldatalist.get(i + 21).equalsIgnoreCase("na")) {
				address1.setLocality("");
			} else {
				address1.setLocality(exceldatalist.get(i + 21));
			}

			if (exceldatalist.get(i + 22).equalsIgnoreCase("na")) {
				error += "Billing Address City is Mandatory / ";
				branch.setCount(-1);
			} else {
				boolean cityValidation = ValidateCity(exceldatalist.get(i + 22).trim());
				if (cityValidation == false) {
					error += "Billing Address City - " + exceldatalist.get(i + 22).trim()
							+ " does not exist or does not match Please check / ";
					branch.setCount(-1);
				}
				address1.setCity(exceldatalist.get(i + 22));
			}

			if (exceldatalist.get(i + 23).equalsIgnoreCase("na")) {
				// address.setState("");
				error += "Billing Address State is Mandatory / ";
				branch.setCount(-1);
			} else {
				address1.setState(exceldatalist.get(i + 23));
			}
			if (exceldatalist.get(i + 24).equalsIgnoreCase("na")) {
				// address.setCountry("");
				error += "Billing Address Country is Mandatory / ";
				branch.setCount(-1);
			} else {
				address1.setCountry(exceldatalist.get(i + 24));
			}

			if (exceldatalist.get(i + 25).equalsIgnoreCase("na")) {
				address1.setPin(0l);
			} else {
				try {
					address1.setPin(Long.parseLong(exceldatalist.get(i + 25)));
				} catch (Exception e) {
					error += "Please enter valid Pin Code / ";
					branch.setCount(-1);
				}

			}
			branch.setBillingAddress(address1);

			/** date 16.02.2018 added by komal foe area wise billing **/
			if (exceldatalist.get(i + 26).equalsIgnoreCase("na")) {
				branch.setArea(0);
			} else {
				try {
					branch.setArea(Double.parseDouble(exceldatalist.get(i + 26)));
				} catch (Exception e) {
					error += "Please enter valid Area / ";
					branch.setCount(-1);
				}

			}
			if (exceldatalist.get(i + 27).equalsIgnoreCase("na")) {
				branch.setUnitOfMeasurement(null);
			} else {
				branch.setUnitOfMeasurement(exceldatalist.get(i + 27));
			}
			if (exceldatalist.get(i + 28).equalsIgnoreCase("na")) {
				branch.setCostCenter(null);
			} else {
				branch.setCostCenter(exceldatalist.get(i + 28));
			}
			/**
			 * end komal
			 */
			if (exceldatalist.get(i + 29).equalsIgnoreCase("na")) {
				branch.setTierName("");
			} else {
				branch.setTierName(exceldatalist.get(i + 29));
			}
			if (exceldatalist.get(i + 30).equalsIgnoreCase("na")) {
				branch.setTat("");
			} else {
				branch.setTat(exceldatalist.get(i + 30));
			}

			branch.setAddress(address);
			branch.setStatus(true);
			branch.setCompanyId(companyId);

//		logger.log(Level.SEVERE, "error"+error);
			if (error != null && !error.equals("")) {
				errorList.add(exceldatalist.get(i));
				errorList.add(exceldatalist.get(i + 1));
				errorList.add(error);
				errorreportflag = true;
			} else {
				branchlist.add(branch);
			}

		}

		if (errorreportflag) {

			ByteArrayOutputStream bytestream = new ByteArrayOutputStream();

			XSSFWorkbook workbook = new XSSFWorkbook();
			try {
				XlsxWriter xlswriter = new XlsxWriter();
				xlswriter.createExcelData(errorList, 3, workbook);
				workbook.write(bytestream);
				logger.log(Level.SEVERE, "bytestream created");

			} catch (IOException e) {
				e.printStackTrace();
			}

			String mailSub = "Customer branch upload report";

			String mailMsg = "Dear Sir/Ma'am ," + "\n" + "\n" + "\n" + " <BR>"
					+ "Please find the customer branch upload details report.";
			Date d = new Date();
			String filename = "Customer branch upload report" + d + ".xlsx";
			ArrayList<String> toemaillist = new ArrayList<String>();
			toemaillist.add(companyEntity.getPocEmail());

			if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", "EnableSendGridEmailApi",
					companyId)) {
				SendGridEmailServlet sdEmail = new SendGridEmailServlet();
				sdEmail.sendMailWithSendGrid(companyEntity.getEmail(), toemaillist, null, null, mailSub, mailMsg,
						"text/html", bytestream, filename,
						"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
						companyEntity.getDisplayNameForCompanyEmailId());
				logger.log(Level.SEVERE, "attached xlsx");
			} else {
				Email email = new Email();
				email.sendMailWithGmail(companyEntity.getEmail(), toemaillist, null, null, mailSub, mailMsg,
						"text/html", bytestream, filename,
						"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				logger.log(Level.SEVERE, "attached xlsx");
			}

		} else {

			ArrayList<SuperModel> modelList = new ArrayList<SuperModel>();

			for (CustomerBranchDetails object : branchlist) {
				SuperModel model = object;
				modelList.add(model);
			}
			if (modelList != null && modelList.size() != 0) {
				GenricServiceImpl impl = new GenricServiceImpl();
				impl.save(modelList);
			}

			String mailSub = "Customer branch upload report";

			String mailMsg = "Dear Sir/Ma'am ," + "\n" + "\n" + "\n" + " <BR>"
					+ "Customer branches uploaded sucessfully. please check in the erp system";
			Date d = new Date();
			ArrayList<String> toemaillist = new ArrayList<String>();
			toemaillist.add(companyEntity.getPocEmail());

			if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", "EnableSendGridEmailApi",
					companyId)) {
				SendGridEmailServlet sdEmail = new SendGridEmailServlet();
				sdEmail.sendMailWithSendGrid(companyEntity.getEmail(), toemaillist, null, null, mailSub, mailMsg,
						"text/html", null, null, null, companyEntity.getDisplayNameForCompanyEmailId());
				logger.log(Level.SEVERE, "attached xlsx");
			} else {
				Email email = new Email();
				email.sendMailWithGmail(companyEntity.getEmail(), toemaillist, null, null, mailSub, mailMsg,
						"text/html", null, null, null);
				logger.log(Level.SEVERE, "attached xlsx");
			}

		}
	}catch(Exception e) {
		String mailSub = "Customer branch upload report";

		String mailMsg = "Dear Sir/Ma'am ," + "\n" + "\n" + "\n" + " <BR>"
				+ "There is some error in uploading Customer branches. please contact eva support and ask to check backend logs. ";
		Date d = new Date();
		ArrayList<String> toemaillist = new ArrayList<String>();
		toemaillist.add(companyEntity.getPocEmail());

		if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", "EnableSendGridEmailApi",
				companyId)) {
			SendGridEmailServlet sdEmail = new SendGridEmailServlet();
			sdEmail.sendMailWithSendGrid(companyEntity.getEmail(), toemaillist, null, null, mailSub, mailMsg,
					"text/html", null, null, null, companyEntity.getDisplayNameForCompanyEmailId());
			logger.log(Level.SEVERE, "email sent from catch");
		} else {
			Email email = new Email();
			email.sendMailWithGmail(companyEntity.getEmail(), toemaillist, null, null, mailSub, mailMsg,
					"text/html", null, null, null);
			logger.log(Level.SEVERE, "email sent from catch");
		}
	}

	}

	private boolean validationCustomerBranch(List<CustomerBranchDetails> listofbranch, String customerId,
			String customerBranchName) {

		for (CustomerBranchDetails customerbranch : listofbranch) {

			if (customerbranch.getCinfo().getCount() == (Integer.parseInt(customerId.trim()))
					&& customerbranch.getBusinessUnitName().trim().equals(customerBranchName.trim())) {

				return false;
			}

		}

		return true;
	}

	private ArrayList<Integer> validateServiceData(long companyId, ArrayList<String> exceldatalist,
			String loggedinuser) {

		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		ArrayList<String> techNameList = new ArrayList<String>();
		HashMap<String, Employee> techMap = new HashMap<String, Employee>();

		ServerAppUtility serverapputility = new ServerAppUtility();
		ArrayList<String> statusList = new ArrayList<String>();
		statusList.add(Service.SERVICESTATUSCANCELLED);
		statusList.add(Service.SERVICESTATUSCOMPLETED);
		statusList.add(Service.SERVICESTATUSSCHEDULE);
		statusList.add(Service.SERVICESTATUSRESCHEDULE);
		statusList.add(Service.SERVICESTATUSPENDING);
		statusList.add(Service.SERVICESTATUSCLOSED);
		statusList.add(Service.SERVICESTATUSREPORTED);
		statusList.add(Service.SERVICESUSPENDED);
		statusList.add(Service.SERVICESTATUSOPEN);
		statusList.add(Service.SERVICESTATUSSTARTED);
		statusList.add(Service.SERVICESTATUSPLANNED);

		ArrayList<String> ratingList = new ArrayList<String>();
		ratingList.add("Excellent");
		ratingList.add("Very Good");
		ratingList.add("Good");
		ratingList.add("Satisfactory");
		ratingList.add("Poor");

		ArrayList<String> errorlist = new ArrayList<String>();
		System.out.println("service exceldatalist.size()=" + exceldatalist.size());
		if (exceldatalist.size() % 19 != 0) {
			errorlist.add("File");
			errorlist.add(
					"Blank cells were found! Put NA instead of blank for the non-mandatory field and put the correct existing value that is already in your ERP system instead of blank for the mandatory field.");
		}
		if (exceldatalist.size() % 19 > 1000) {
			errorlist.add("File");
			errorlist.add("Upload limit is 1000");
		}
		if (errorlist != null && errorlist.size() > 0) {
			CsvWriter.serviceDownloadForScheduleOrCompleteViaExcelErrorlist = errorlist;
			integerlist.add(-1);
			return integerlist;
		}

		for (int i = 19; i < exceldatalist.size(); i += 19) {
			if (!exceldatalist.get(i + 6).equalsIgnoreCase("NA"))
				techNameList.add(exceldatalist.get(i + 6));
		}

		List<Employee> techList = null;
		if (techNameList != null && techNameList.size() > 0) {
			techList = ofy().load().type(Employee.class).filter("companyId", companyId)
					.filter("fullname IN", techNameList).list();
		}
		if (techList != null) {
			for (Employee tech : techList) {
				techMap.put(tech.getFullname(), tech);
			}
		}
		logger.log(Level.SEVERE, "techMap size=" + techMap);
		for (int i = 19; i < exceldatalist.size(); i += 19) {
			String error = "";
			try {

				error += serverapputility.validateMandatory(exceldatalist.get(i), exceldatalist.get(0));
				error += serverapputility.validateMandatory(exceldatalist.get(i + 8), exceldatalist.get(8));
				error += serverapputility.validateServiceStatus(exceldatalist.get(i + 8), statusList,
						exceldatalist.get(8));
				
				error += serverapputility.validateDate(exceldatalist.get(i + 4).trim(), "MM/dd/yyyy", exceldatalist.get(4)); // old
																														// dd/MM/yyyy
																								// Ashwini
																														// Patil
																														// Date:21-11-2024
				error += serverapputility.validateYesNo(exceldatalist.get(9), exceldatalist.get(i + 9));
				if (!exceldatalist.get(i + 6).equalsIgnoreCase("NA")) {
					if (techMap != null && techMap.size() > 0) {
						Employee emp = techMap.get(exceldatalist.get(i + 6));
						if (emp == null)
							error += "Employee " + exceldatalist.get(i + 6) + " does not exist in system.";
						else if (!emp.isStatus())
							error += "Employee " + exceldatalist.get(i + 6) + " is inactive.";
					}
				}
				if (!exceldatalist.get(i + 7).equalsIgnoreCase("NA"))
					error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(7).trim(),
							exceldatalist.get(i + 7).trim());

				if (!exceldatalist.get(i + 14).trim().equalsIgnoreCase("NA"))
					error += serverapputility.validateServiceStatus(exceldatalist.get(i + 14), ratingList,
							exceldatalist.get(14));

				if (exceldatalist.get(i + 8).trim().equalsIgnoreCase(Service.SERVICESTATUSCANCELLED)) {
					error += serverapputility.validateMandatory(exceldatalist.get(i + 15), exceldatalist.get(15));
				}

				if (!exceldatalist.get(i + 17).equalsIgnoreCase("NA"))
					error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(17).trim(),
							exceldatalist.get(i + 17).trim());

				if (exceldatalist.get(i + 8).trim().equalsIgnoreCase(Service.SERVICESTATUSCOMPLETED)) {
					error += serverapputility.validateMandatory(exceldatalist.get(i + 6), exceldatalist.get(6));
					error += serverapputility.validateMandatory(exceldatalist.get(i + 10), exceldatalist.get(10));
					if (!exceldatalist.get(i + 10).equalsIgnoreCase("NA")) {
						System.out.println("exceldatalist.get(i+10)=" + exceldatalist.get(i + 10));
						error += serverapputility.validateDate(exceldatalist.get(i + 10), "MM/dd/yyyy",
								exceldatalist.get(10));
					}
					error += serverapputility.validateMandatory(exceldatalist.get(i + 11), exceldatalist.get(11));
					error += serverapputility.validateMandatory(exceldatalist.get(i + 12), exceldatalist.get(12));
					error += serverapputility.validateMandatory(exceldatalist.get(i + 13), exceldatalist.get(13));
					error += serverapputility.validateMandatory(exceldatalist.get(i + 14), exceldatalist.get(14));
				} else {
					if (!exceldatalist.get(i + 10).equalsIgnoreCase("NA"))
						error += "Set service status as Completed to add completion date";
					if (!exceldatalist.get(i + 11).equalsIgnoreCase("NA"))
						error += "Set service status as Completed to add completion time";
					if (!exceldatalist.get(i + 12).equalsIgnoreCase("NA"))
						error += "Set service status as Completed to add completion remark";
					if (!exceldatalist.get(i + 13).equalsIgnoreCase("NA"))
						error += "Set service status as Completed to add technician remark";
					if (!exceldatalist.get(i + 14).equalsIgnoreCase("NA"))
						error += "Set service status as Completed to add customer rating";
				}

				if (!exceldatalist.get(i + 12).equalsIgnoreCase("NA"))
					error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(12).trim(),
							exceldatalist.get(i + 12).trim());
				if (!exceldatalist.get(i + 13).equalsIgnoreCase("NA"))
					error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(13).trim(),
							exceldatalist.get(i + 13).trim());
				if (!exceldatalist.get(i + 15).equalsIgnoreCase("NA"))
					error += serverapputility.checkIfCommaQuoteAmpersandPresent(exceldatalist.get(15).trim(),
							exceldatalist.get(i + 15).trim());

			} catch (Exception e) {
				e.printStackTrace();
				error += " Excel Data is not proper format please check!";
			}

			if (!error.trim().equals("")) {
				if (!error.equals("")) {
					logger.log(Level.SEVERE, "Error" + error);
					errorlist.add(exceldatalist.get(i));
					errorlist.add(error);
				}
			}
		}

		if (errorlist != null && errorlist.size() > 0) {
			CsvWriter.serviceDownloadForScheduleOrCompleteViaExcelErrorlist = errorlist;
			integerlist.add(-1);
			return integerlist;
		}

		String msg = callTaskQueueForServiceUpdate(companyId, loggedinuser);
		logger.log(Level.SEVERE, "msg " + msg);
		integerlist.add(0);

		return integerlist;

	}

	private String callTaskQueueForServiceUpdate(long companyId, String loggedinuser) {
		logger.log(Level.SEVERE, "Service update  task queue calling");
		try {
			Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue")
					.param("EntityName", AppConstants.ScheduleOrCompleteViaExcel).param("companyId", companyId + "")
					.param("loggedinuser", loggedinuser));

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception ==" + e.getMessage());
			return "Failed please try again";
		}
		return "Service Update Process Started";
	}

	public void ServiceUpdateViaExcelUpload(long companyId, ArrayList<String> exceldatalist, String loggedInUser) {

		ArrayList<Integer> serviceidlist = new ArrayList<Integer>();
		HashMap<Integer, Service> serviceMap = new HashMap<Integer, Service>();
		for (int i = 19; i < exceldatalist.size(); i += 19) {
			serviceidlist.add(Integer.parseInt(exceldatalist.get(i).trim()));
		}

		List<Service> list = ofy().load().type(Service.class).filter("companyId", companyId)
				.filter("count IN", serviceidlist).list();
		if (list != null)
			logger.log(Level.SEVERE, "list size " + list.size());

		for (Service s : list) {
			serviceMap.put(s.getCount(), s);
		}
		logger.log(Level.SEVERE, "serviceMap size " + serviceMap.size());
		ArrayList<Service> updatedList = new ArrayList<Service>();
		for (int i = 19; i < exceldatalist.size(); i += 19) {
			Service service = serviceMap.get(Integer.parseInt(exceldatalist.get(i).trim()));
			if (service != null) {
				ModificationHistory history = new ModificationHistory();
				history.oldServiceDate = service.getServiceDate();
				try {
					history.resheduleDate = contfmt.parse(exceldatalist.get(i + 4).trim());
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (!exceldatalist.get(i + 5).equalsIgnoreCase("NA"))
					history.resheduleTime = exceldatalist.get(i + 5);
				else if (service.getServiceTime() != null)
					history.resheduleTime = service.getServiceTime();
				else
					history.resheduleTime = "Flexible";

				history.reason = "User " + loggedInUser
						+ " has made changes in service through upload via excel program on " + new Date();
				history.userName = loggedInUser;
				history.systemDate = new Date();
				service.getListHistory().add(history);

				if (!exceldatalist.get(i + 4).equalsIgnoreCase("NA"))
					try {
						service.setServiceDate(fmt.parse(exceldatalist.get(i + 4).trim()));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				if (!exceldatalist.get(i + 5).equalsIgnoreCase("NA"))
					service.setServiceTime(exceldatalist.get(i + 5).trim());

				if (!exceldatalist.get(i + 6).equalsIgnoreCase("NA"))
					service.setEmployee(exceldatalist.get(i + 6).trim());

				if (!exceldatalist.get(i + 7).equalsIgnoreCase("NA"))
					service.setDescription(exceldatalist.get(i + 7).trim());

				if (!exceldatalist.get(i + 8).equalsIgnoreCase("NA"))
					service.setStatus(exceldatalist.get(i + 8).trim());

				if (exceldatalist.get(i + 9).equalsIgnoreCase("yes"))
					service.setServiceScheduled(true);
				else
					service.setServiceScheduled(false);

				if (!exceldatalist.get(i + 10).equalsIgnoreCase("NA"))
					try {
						service.setServiceCompletionDate(mmDDfmt.parse(exceldatalist.get(i + 10).trim()));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				if (!exceldatalist.get(i + 12).equalsIgnoreCase("NA")) // Customer service -> completion tab -> customer
																		// feedback(mapped through pedio)
					service.setServiceCompleteRemark(exceldatalist.get(i + 12).trim());

				if (!exceldatalist.get(i + 13).equalsIgnoreCase("NA"))
					service.setTechnicianRemark(exceldatalist.get(i + 13).trim());

				if (!exceldatalist.get(i + 14).equalsIgnoreCase("NA"))// Customer service -> completion tab -> customer
																		// rating(mapped through pedio)
					service.setCustomerFeedback(exceldatalist.get(i + 14).trim());

				if (!exceldatalist.get(i + 15).equalsIgnoreCase("NA")) {

					service.setRemark(exceldatalist.get(i + 15).trim());
					if (exceldatalist.get(i + 8).equalsIgnoreCase(Service.SERVICESTATUSCANCELLED)) {
						service.setCancellationDate(new Date());
						service.setReasonForChange("Service ID =" + service.getCount() + " " + "Service status ="
								+ service.getStatus().trim() + "\n" + "has been cancelled by " + loggedInUser
								+ " with remark " + "\n" + "Remark =" + exceldatalist.get(i + 15).trim() + " "
								+ "Cancellation Date =" + fmt.format(new Date()));

					} else
						service.setReasonForChange(exceldatalist.get(i + 15).trim());
				}

				updatedList.add(service);
			}

		}

		logger.log(Level.SEVERE, "After ServiceUpdateViaExcelUpload updated data now saving the data into db");
		ofy().save().entities(updatedList);
		logger.log(Level.SEVERE, "Done here");

	}

}
