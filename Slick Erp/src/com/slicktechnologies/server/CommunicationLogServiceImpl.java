package com.slicktechnologies.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.ObjectifyService;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.services.CommunicationLogService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class CommunicationLogServiceImpl extends RemoteServiceServlet implements CommunicationLogService{

	/**
	 * 
	 */
	private static final long serialVersionUID = -790673533108608193L;
	 Logger logger = Logger.getLogger("NameOfYourLogger");

	/**
	 * 14 April 2017
	 * added by vijay
	 * when we click on save button then this method will call 
	 * for communication log save we can use below method reusable for every Screen
	 */
	
	@Override
	public void saveCommunicationLog(ArrayList<InteractionType> communicationloglist) {

		ArrayList<SuperModel> interactionlist = new ArrayList<SuperModel>();
		for(int i=0;i<communicationloglist.size();i++){
			
			if(communicationloglist.get(i).isCommunicationlogFlag()){
				communicationloglist.get(i).setCommunicationlogFlag(false);
				interactionlist.add(communicationloglist.get(i));
			}
		}
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(interactionlist);
		/**
		 * added  by komal to maintain followup date of leads and quotations from communication log
		 */
		saveFollowUpDate(communicationloglist);
	}
	
	/**
	 * added  by komal to maintain followup date of leads and quotations from communication log
	 */
		
	protected void saveFollowUpDate(ArrayList<InteractionType> communicationloglist)
	{
		/**
		 * Date : 17-01-2020 BY ANIL
		 * for clearing all loaded data from appengine cache memory
		 */
		ObjectifyService.reset();
		ofy().consistency(Consistency.STRONG);
		ofy().clear();
		/**
		 * End
		 */
		/**
		 * Date : 08-11-2017 BY Komal
		 */
		Comparator<InteractionType> comp=new Comparator<InteractionType>() {
			@Override
			public int compare(InteractionType e1, InteractionType e2) {
//				Integer coutn1=o1.getCount();
//				Integer count2=o2.getCount();
//				return ;
				if (e1.getCount() == e2.getCount()) {
					return 0;
				}
				if (e1.getCount() > e2.getCount()) {
					return 1;
				} else {
					return -1;
				}
			}
		};
		Collections.sort(communicationloglist,comp);

		int length = communicationloglist.size();
		if(communicationloglist.get(0).getInteractionDocumentName().equals("Lead")){
			Lead lead = ofy().load().type(Lead.class).filter("companyId", communicationloglist.get(0).getCompanyId()).filter("count", communicationloglist.get(0).getInteractionDocumentId()).first().now();
			
			/**
			 * @author Vijay Chougule Date :- 07-09-2020
			 * Des :- As per Nitin Sir at any condition Follow up Date will update in lead so i am commented below
			 * If condition 
			 */
//			/**
//			 * @author Anil , Date : 16-01-2020
//			 * As per Nitin sir , if lead status is  'Created' ,'Cancelled' ,'Successful' or 'Unsuccessful' then only update follow up date and time in lead.
//			 */
//			if(lead.getStatus().equalsIgnoreCase(Lead.CREATED)||lead.getStatus().equalsIgnoreCase(Lead.CANCELLED)||lead.getStatus().equalsIgnoreCase("Successful")||lead.getStatus().equalsIgnoreCase("Unsuccessful")){
				
				if(lead!=null){
					lead.setFollowUpDate(communicationloglist.get(length-1).getInteractionDueDate());
					/**
					 * 26-12-2019
					 */
					lead.setFollowUpTime(communicationloglist.get(length-1).getInteractionDueTime());
					
					ofy().save().entity(lead);	
				}
				
//			}
		}
			else if(communicationloglist.get(0).getInteractionModuleName().equals("Service") && communicationloglist.get(0).getInteractionDocumentName().equals("Quotation")){
				Quotation quotation  = ofy().load().type(Quotation.class).filter("companyId", communicationloglist.get(0).getCompanyId()).filter("count", communicationloglist.get(0).getInteractionDocumentId()).first().now();
				quotation.setFollowUpDate(communicationloglist.get(length-1).getInteractionDueDate());
				ofy().save().entity(quotation);
			}
		
			else if(communicationloglist.get(0).getInteractionModuleName().equals("Service") && communicationloglist.get(0).getInteractionDocumentName().equals("Contract")){
				Contract contract  = ofy().load().type(Contract.class).filter("companyId", communicationloglist.get(0).getCompanyId()).filter("count", communicationloglist.get(0).getInteractionDocumentId()).first().now();
				contract.setFollowUpDate(communicationloglist.get(length-1).getInteractionDueDate());
				ofy().save().entity(contract);
			}
			/* date 10/03/2018 added by komal for followupdate in sales module**/
			else if(communicationloglist.get(0).getInteractionModuleName().equals(AppConstants.SALESMODULE) && communicationloglist.get(0).getInteractionDocumentName().equals("Quotation")){
				SalesQuotation quotation  = ofy().load().type(SalesQuotation.class).filter("companyId", communicationloglist.get(0).getCompanyId()).filter("count", communicationloglist.get(0).getInteractionDocumentId()).first().now();
				quotation.setFollowUpDate(communicationloglist.get(length-1).getInteractionDueDate());
				ofy().save().entity(quotation);
			}
		
		    /**2-1-2019 added by amol for followupdate in payment dashboard**/
			else if(communicationloglist.get(0).getInteractionModuleName().equals(AppConstants.ACCOUNTMODULE) && communicationloglist.get(0).getInteractionDocumentName().equals("CustomerPayment")){
				CustomerPayment cust  = ofy().load().type(CustomerPayment.class).filter("companyId", communicationloglist.get(0).getCompanyId()).filter("count", communicationloglist.get(0).getInteractionDocumentId()).first().now();
				cust.setFollowUpDate(communicationloglist.get(length-1).getInteractionDueDate());
				ofy().save().entity(cust);
			}
	}

	/**
	 * Date 13-11-2019 
	 * @author Vijay Chougule 
	 * Des :- NBHC CCPM Rate Contract Service value updation when Billing document Approved
	 */
	
	@Override
	public String updateRateContractServiceValue(BillingDocument billingDocument) {
		Contract contractEntity = ofy().load().type(Contract.class).filter("count", billingDocument.getContractCount())
									.filter("companyId", billingDocument.getCompanyId()).first().now();
		if(contractEntity!=null){
			logger.log(Level.SEVERE, "contractEntity "+contractEntity);
			if(contractEntity.isContractRate()){
				Service serviceEntity = ofy().load().type(Service.class).filter("companyId", billingDocument.getCompanyId())
										.filter("count", billingDocument.getRateContractServiceId()).first().now();
				if(serviceEntity!=null){
					logger.log(Level.SEVERE, "serviceEntity");
					serviceEntity.setServiceValue(billingDocument.getTotalBillingAmount());
					ofy().save().entity(serviceEntity);
					logger.log(Level.SEVERE, "Service Value updated");
					logger.log(Level.SEVERE, "Billing Value"+billingDocument.getTotalBillingAmount());

				}
			}
		}
		return "Success";
	}

	/**
	 * Date 14-11-2019 
	 * @author Vijay Chougule
	 * Des :- NBHC CCPM complaint service validation for if Assessment is in created status
	 */
	@Override
	public String ValidateAssessmentForComplaintService(ArrayList<Integer> complaintServiceid,long companyId) {
		String validateStatus= "success";
		if(complaintServiceid.size()!=0){
			logger.log(Level.SEVERE, "complaintServiceid"+complaintServiceid.size());
			List<AssesmentReport> assementreportlist = ofy().load().type(AssesmentReport.class)
										.filter("serviceId IN", complaintServiceid).filter("companyId",companyId)
										.filter("status", AssesmentReport.CREATED).list();
			logger.log(Level.SEVERE, "Open assementreportlist "+assementreportlist.size());
			
			if(assementreportlist.size()!=0){
				validateStatus ="Please complete Assessment of selected service id as follows";
				for(AssesmentReport assmentreport : assementreportlist){
					for(int i=0;i<complaintServiceid.size();i++){
						if(assmentreport.getServiceId()==complaintServiceid.get(i)){
							validateStatus = validateStatus + assmentreport.getServiceId()+""+",";
						}
					}
				}
				return validateStatus;
			}
			else{
				return validateStatus;
			}
		}
		return validateStatus;
	}

}
