package com.slicktechnologies.server;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.mindrot.jbcrypt.BCrypt;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.simplesoftwares.client.library.libservice.LoginService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.email.SendGridEmailServlet;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.CreatedByInfo;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.LicenseDetails;
import com.slicktechnologies.shared.common.helperlayer.CustomerUser;
import com.slicktechnologies.shared.common.helperlayer.LoggedIn;
import com.slicktechnologies.shared.common.helperlayer.LoginProxy;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class LoginServiceImpl extends RemoteServiceServlet implements
		LoginService {
	/**
	 */
	private static final long serialVersionUID = -7849431696181450915L;
	Logger logger = Logger.getLogger("Komal");
	
	public static String url = new String();

	@Override
	public User giveLogin(LoginProxy loginproxy) {
		User user;

		System.out.println("In side give login comp name"+ loginproxy.getCompanyName());

		if (loginproxy.getCompanyName() == null) {
			return null;
		}
		// Company
		// company=ofy().load().type(Company.class).filter("buisnessUnitName",
		// loginproxy.getCompanyName()).first().now();

		logger.log(Level.SEVERE, loginproxy.getCompanyName());
		Company company = ofy().load().type(Company.class).filter("accessUrl", loginproxy.getCompanyName()).first().now();
		if (company == null) {
			return null;
		}
		String companyStatus = company.getStatus();
		if (companyStatus.equals(Company.ACTIVE)|| companyStatus.equals(Company.DEMO)) {
			Long companyId = company.getCompanyId();

			System.out.println("company id------- " + companyId);
			logger.log(Level.SEVERE, "ID   " + companyId);
			logger.log(Level.SEVERE, "NO Of User ==   " + company.getNoOfUser());
			logger.log(Level.SEVERE, "Company Type: " + company.getCompanyType());
			// Also comment code present at bottom written by anil for forgot
			// password
			// search word = ForgotPasswordByAnil
			/***************************** comment first time deploying new code ************************/
//			logger.log(Level.SEVERE, "userName " + loginproxy.getUserId());
//			user = ofy().load().type(User.class).filter("companyId", companyId).filter("userName", loginproxy.getUserId()).first().now();
//
//			logger.log(Level.SEVERE, "user = " + user);
//
//			if (user != null) {
//				logger.log(Level.SEVERE, "inside if user != null ==");
//
//				System.out.println("Database password" + user.getPassword());
//				boolean matched = BCrypt.checkpw(loginproxy.getPassword(),user.getPassword());
//				System.out.println("Matched:" + matched);
//				logger.log(Level.SEVERE, "Login matched value:::::" + matched);
//				/**************************** new code end **********************/
//
//				if (matched) {
//					logger.log(Level.SEVERE, "Login matched value:::::"+ matched + " User name " + loginproxy.getUserId()+ "Pass word" + loginproxy.getPassword());
//					user.setAccessLevel(company.getNoOfUser() + "");
//					 /**
//					  * Date : 04-12-2017 BY ANIL
//					  */
//					 user.setLicenseEndDate(company.getLicenseEndingDate());

			
					 /**
					  * @author Vijay Date :- 06-09-2022
					  * Des :- Password Encryption with process config
					  */
					 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("User", AppConstants.PC_ENABLEPASSWORDENCRYPTION, companyId)) {
							user = ofy().load().type(User.class).filter("companyId", companyId).filter("userName", loginproxy.getUserId()).first().now();
							if(user!=null) {
								try {
									logger.log(Level.SEVERE,"user "+user);
									boolean matched = BCrypt.checkpw(loginproxy.getPassword(),user.getPassword());
									logger.log(Level.SEVERE,"encryption password "+matched);
									if(!matched) {
										user = null;
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							

							}
					 }
					 else {
						 System.out.println("ROHAN username "+loginproxy.getUserId()+" Password "+loginproxy.getPassword());
						 user=ofy().load().type(User.class).filter("userName",loginproxy.getUserId()).filter("password",
						 loginproxy.getPassword())
						 .filter("companyId",companyId).first().now();
					 }
					
						logger.log(Level.SEVERE,"user entity "+user);

					 if(user!=null)
					 {
						 
						 /**
						  * Developer Name : Rahul Verma
						  * Date : 19 Dec 2017
						  * Description : This will not allow any inactive user to log in
						  */
					if(user.getStatus()){
					
						
					 /**Developed by : Rohan bhagde
					 * Reason : rohan has commented this code for because of
					 no need we have save user in login entity with
					 status as "Active" while creating session for new user
					 */
					logger.log(Level.SEVERE,"Logged In Successfull!");
					
					/**
					 * @author Anil
					 * @since 01-07-2020
					 */
//					user.setAccessLevel(company.getNoOfUser()+"");
					user.setAccessLevel(getNoOfActiveLicense(company)+"");
					 /**
					  * Date : 04-12-2017 BY ANIL
					  */
					 user.setLicenseEndDate(company.getLicenseEndingDate());
					 /**
					  * Updated By: Viraj
					  * Date: 22-06-2019
					  * Description: To store companyType in user and status of process configuration in user
					  */
					 user.setCompanyType(company.getCompanyType());
					 boolean status;
					 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("AppConnector", "HIDEHR" , companyId)) {
						 logger.log(Level.SEVERE,"Inside hideHR process config");
						 status = true;
					 }else {
						 status = false;
					 }
					 user.setHrStatus(status);
					 /** Ends **/
					// // changes ends here ***********************
					// // LoggedIn loggedin=new LoggedIn();
					// // CreatedByInfo.empName=user.getEmployeeName();
					// // CreatedByInfo.userId=user.getUserName();
					// // loggedin.setCompanyId(companyId);
					// //// loggedin.setLoginTime(new Date());
					// // loggedin.setUserName(user.getUserName());
					// // ofy().save().entity(loggedin).now();
					// // logger.log(Level.SEVERE,"User ID   "+user.getId());

					return user;
				}else{
					return user;
				}
					
				} else
					return null;
			} else
				return null;
//		} else
//			return null;
//		

	}

	@Override
	public CustomerUser giveCustLogin(LoginProxy loginproxy) {
		CustomerUser user;
		if (loginproxy.getCompanyName() == null) {
			return null;
		}
		Logger logger = Logger.getLogger("Komal");
		logger.log(Level.SEVERE, loginproxy.getCompanyName());
		Company company = ofy().load().type(Company.class)
				.filter("accessUrl", loginproxy.getCompanyName()).first().now();
		if (company == null) {
			return null;
		}
		String companyStatus = company.getStatus();
		if (companyStatus.equals(Company.ACTIVE)
				|| companyStatus.equals(Company.DEMO)) {
			Long companyId = company.getCompanyId();
			System.out.println("company id------- " + companyId);
			logger.log(Level.SEVERE, "ID   " + companyId);
			user = ofy().load().type(CustomerUser.class)
					.filter("userName", loginproxy.getUserId())
					.filter("password", loginproxy.getPassword())
					.filter("companyId", companyId).first().now();
			
			
			if (user != null) {
				/**Date 19-2-2020 by Amol set a company Type**/
				user.setCompanyType(company.getCompanyType());
				
				// LoggedIn loggedin=new LoggedIn();
				// CreatedByInfo.empName=user.getEmployeeName();
				// CreatedByInfo.userId=user.getUserName();
				// loggedin.setCompanyId(companyId);
				// // loggedin.setLoginTime(new Date());
				// loggedin.setUserName(user.getUserName());
				// ofy().save().entity(loggedin).now();
				// logger.log(Level.SEVERE,"User ID   "+user.getId());

				return user;
			} else
				return null;
		} else
			return null;
	}

	@Override
	public Employee getBranchLevelRestriction(User user) {

		// rohan added this code for branch level restriction
		try {
			
		
		
		Employee employee = null;
		boolean flag = false;
		System.out
				.println("Rohan on server side before loading process config");

		ProcessConfiguration processConfig = ofy().load()
				.type(ProcessConfiguration.class)
				.filter("processName", "Branch").filter("configStatus", true)
				.filter("companyId", user.getCompanyId()).first().now();

		if (processConfig != null) {

			for (int k = 0; k < processConfig.getProcessList().size(); k++) {
				if (processConfig.getProcessList().get(k).getProcessType()
						.trim().equalsIgnoreCase("BranchLevelRestriction")
						&& processConfig.getProcessList().get(k).isStatus() == true) {
					flag = true;
					System.out
							.println("Rohan on service side after loading process config"
									+ flag);
				}
			}
		}

		if (flag == true) {
			System.out.println("rohan in side employee load ");
			System.out.println("Rohan employee " + user.getEmployeeName());

			employee = ofy().load().type(Employee.class)
					.filter("companyId", user.getCompanyId())
					.filter("fullname", user.getEmployeeName().trim()).first()
					.now();

			return employee;
		} else {
			return employee;
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * This method checks username and/or email in user and customer user and if
	 * its valid then generate random password and sent email on users email.
	 * Date :28-09-2016 By Anil Release : 30 Sept 2016
	 * 
	 */

	@Override
	public String processForgotPasswordRequest(String companyName,
			String uname, String email) {
		System.out.println("COMPANY NAME : " + companyName + " USERNAME : "
				+ uname + " Email : " + email);
		GenricServiceImpl genImpl = new GenricServiceImpl();
		Email emailObj = new Email();
		User user;
		CustomerUser customerUser;
		String emailSubject;
		String emailMessage;
		String fromEmailId;
		String toEmailId;
		Branch branch=null;
		String emailidlist="";
		ArrayList<String> toemaillist=new ArrayList<String>();

		if (companyName == null) {
			return null;
		}

		logger.log(Level.SEVERE, "COMPANY NAME " + companyName);
		Company company = ofy().load().type(Company.class)
				.filter("accessUrl", companyName).first().now();
		if (company == null) {
			System.out.println("Company Null");
			return null;
		}
		String companyStatus = company.getStatus();
		if (companyStatus.equals(Company.ACTIVE)
				|| companyStatus.equals(Company.DEMO)) {
			Long companyId = company.getCompanyId();
			logger.log(Level.SEVERE, "COMPANY ID   " + companyId);
			String ranGenePass = "";

			if (uname != null && email != null) {
				user = ofy().load().type(User.class)
						.filter("companyId", companyId)
						.filter("userName", uname).first().now();
				
				
				if (user != null) {
					if(user.getBranch()!=null) {
						branch=ofy().load().type(Branch.class)
							.filter("companyId", companyId)
							.filter("buisnessUnitName", user.getBranch()).first().now();
					}
					
					if (user.getEmail() != null) {
						if (user.getEmail().equals("")) {
							return "Email id is not registered with system,Please contact admin!"; //do not change the message as condition written on this text in ForgotPasswordPresenter.java
						} else {
							if (user.getEmail().trim().equals(email.trim())) {
								// write a code which generate random password
								// and send it to user
								// also save generated password in User entity

								ranGenePass = StringUtils
										.generateRandomPassword();
								System.out
										.println("RANDOM GENERATED PASSWORD : "
												+ ranGenePass);

								user.setPassword(ranGenePass);
								user.setStatus(true);
								genImpl.save(user);

								emailSubject = "Password Reset Successfully";
								fromEmailId = company.getEmail();
								emailidlist+="Company email- "+company.getEmail();
								if(company.getCellNumber1()!=null&&company.getCellNumber1()>0)
									emailidlist+=" - Contact on - "+company.getCellNumber1();
								if(branch!=null&&branch.getEmail()!=null&&!branch.getEmail().equals("")) {
									toemaillist.add(branch.getEmail());
									emailidlist+=",\n Branch email- "+branch.getEmail();
									if(branch.getCellNumber1()!=null&&branch.getCellNumber1()>0)
										emailidlist+=" - Contact on - "+branch.getCellNumber1();
								}
								toEmailId = user.getEmail();
								emailidlist+=",\n "+user.getEmail();
								emailMessage = "Dear "
										+ user.getEmployeeName()
										+ ","
										+ "\n"
										+ "\n"
										+ "\n"
										+ "Your password on "
										+ company.getCompanyURL()
										+ " has been reset successfully. Please find below the new credentials."
										+ "\n" + "\n" + "Username : "
										+ user.getUserName() + "\n"
										+ "Password : " + ranGenePass + "\n";
								
								if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, company.getCompanyId())){
									logger.log(Level.SEVERE, "Inside Sendgird API wise Email ");
									SendGridEmailServlet sdEmail=new SendGridEmailServlet();									
									toemaillist.add(toEmailId);									
									sdEmail.sendMailWithSendGrid(fromEmailId, toemaillist, null, null, emailSubject, emailMessage, "text/html",null,null,"application/pdf", null, null, "application/pdf", null, "", "application/pdf", null, null, null, null);								

								}else
								emailObj.sendPasswordResetEmail(emailSubject,
										emailMessage, fromEmailId, toEmailId);//Commented by Ashwini Patil Date:27-04-2023 since this method was giving unauthorised sender error and email was not getting sent
								
								

								return "Password is sent to "+emailidlist;
							} else {
								return "Email id is wrong!";//do not change the message as condition written on this text in ForgotPasswordPresenter.java
							}
						}
					} else {
						return "Email id is not registered with system,Please contact admin!";//do not change the message as condition written on this text in ForgotPasswordPresenter.java
					}
				} else {
					customerUser = ofy().load().type(CustomerUser.class)
							.filter("companyId", companyId)
							.filter("userName", uname).first().now();
					if (customerUser != null) {
						if(customerUser.getBranch()!=null) {
							branch=ofy().load().type(Branch.class)
								.filter("companyId", companyId)
								.filter("buisnessUnitName", customerUser.getBranch()).first().now();
						}
						
						if (customerUser.getEmail() != null) {
							if (customerUser.getEmail().equals("")) {
								return "Email id is not registered with system,Please contact admin!";//do not change the message as condition written on this text in ForgotPasswordPresenter.java
							} else {
								if (customerUser.getEmail().trim()
										.equals(email.trim())) {
									// write a code which generate random
									// password and send it to user
									// also save generated password in
									// CustomerUser entity

									ranGenePass = StringUtils
											.generateRandomPassword();
									System.out
											.println("RANDOM GENERATED PASSWORD : "
													+ ranGenePass);

									customerUser.setPassword(ranGenePass);
									genImpl.save(customerUser);
									emailSubject = "Password Reset Successfully";
									fromEmailId = company.getEmail();
									emailidlist+="Company email- "+company.getEmail();
									if(company.getCellNumber1()!=null&&company.getCellNumber1()>0)
										emailidlist+=" - Contact on - "+company.getCellNumber1();
									if(branch!=null&&branch.getEmail()!=null&&!branch.getEmail().equals("")) {
										toemaillist.add(branch.getEmail());
										emailidlist+=",\n Branch email- "+branch.getEmail();
										if(branch.getCellNumber1()!=null&&branch.getCellNumber1()>0)
											emailidlist+=" - Contact on - "+branch.getCellNumber1();
									}
									toEmailId = customerUser.getEmail();
									emailidlist+=",\n "+customerUser.getEmail();
									
									
									
									emailMessage = "Dear "
											+ customerUser.getCustomerName()
											+ ","
											+ "\n"
											+ "\n"
											+ "\n"
											+ "Your password on "
											+ company.getCompanyURL()
											+ " has been reset successfully. Please find below the new credentials."
											+ "\n" + "\n" + "Username : "
											+ customerUser.getUserName() + "\n"
											+ "Password : " + ranGenePass
											+ "\n";
									if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, company.getCompanyId())){
										logger.log(Level.SEVERE, "Inside Sendgird API wise Email ");
										SendGridEmailServlet sdEmail=new SendGridEmailServlet();
										toemaillist.add(toEmailId);
										sdEmail.sendMailWithSendGrid(fromEmailId, toemaillist, null, null, emailSubject, emailMessage, "text/html",null,null,"application/pdf", null, null, "application/pdf", null, "", "application/pdf", null, null, null, null);								

									}else
									emailObj.sendPasswordResetEmail(
											emailSubject, emailMessage,
											fromEmailId, toEmailId);

									return "Password is sent to "+emailidlist;
								} else {
									return "Email id is wrong!";//do not change the message as condition written on this text in ForgotPasswordPresenter.java
								}
							}
						} else {
							return "Email id is not registered with system,Please contact admin!";//do not change the message as condition written on this text in ForgotPasswordPresenter.java
						}
					} else {
						return "Username/Email is wrong!";
					}
				}
			} else if (uname != null && email == null) {
				user = ofy().load().type(User.class)
						.filter("companyId", companyId)
						.filter("userName", uname).first().now();
				if (user != null) {
					
					if(user.getBranch()!=null) {
						branch=ofy().load().type(Branch.class)
							.filter("companyId", companyId)
							.filter("buisnessUnitName", user.getBranch()).first().now();
					}
					if (user.getEmail() != null) {
						if (user.getEmail().equals("")) {
							return "Email id is not registered with system,Please contact admin!";//do not change the message as condition written on this text in ForgotPasswordPresenter.java
						} else {
							// write a code which generate random password and
							// send it to user
							// also save generated password in User entity

							ranGenePass = StringUtils.generateRandomPassword();
							System.out.println("RANDOM GENERATED PASSWORD : "
									+ ranGenePass);

							user.setPassword(ranGenePass);
							genImpl.save(user);
							user.setStatus(true);
							System.out.println("AFTER SAVE");
							emailSubject = "Password Reset Successfully";
							fromEmailId = company.getEmail();
							emailidlist+="Company email- "+company.getEmail();
							if(company.getCellNumber1()!=null&&company.getCellNumber1()>0)
								emailidlist+=" - Contact on - "+company.getCellNumber1();
							if(branch!=null&&branch.getEmail()!=null&&!branch.getEmail().equals("")) {
								toemaillist.add(branch.getEmail());
								emailidlist+=",\n Branch email- "+branch.getEmail();
								if(branch.getCellNumber1()!=null&&branch.getCellNumber1()>0)
									emailidlist+=" - Contact on - "+branch.getCellNumber1();
							}
							toEmailId = user.getEmail();
							emailidlist+=", "+user.getEmail();
							emailMessage = "Dear "
									+ user.getEmployeeName()
									+ ","
									+ "\n"
									+ "\n"
									+ "\n"
									+ "Your password on "
									+ company.getCompanyURL()
									+ " has been reset successfully. Please find below the new credentials."
									+ "\n" + "\n" + "Username : "
									+ user.getUserName() + "\n" + "Password : "
									+ ranGenePass + "\n";
							if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, company.getCompanyId())){
								logger.log(Level.SEVERE, "Inside Sendgird API wise Email ");
								SendGridEmailServlet sdEmail=new SendGridEmailServlet();
								toemaillist.add(toEmailId);
								sdEmail.sendMailWithSendGrid(fromEmailId, toemaillist, null, null, emailSubject, emailMessage, "text/html",null,null,"application/pdf", null, null, "application/pdf", null, "", "application/pdf", null, null, null, null);								

							}else
								emailObj.sendPasswordResetEmail(emailSubject,
									emailMessage, fromEmailId, toEmailId);

							return "Password is sent to "+emailidlist;
						}
					} else {
						return "Email id is not registered with system,Please contact admin!";//do not change the message as condition written on this text in ForgotPasswordPresenter.java
					}
				} else {
					customerUser = ofy().load().type(CustomerUser.class)
							.filter("companyId", companyId)
							.filter("userName", uname).first().now();
					if (customerUser != null) {
						
						if(customerUser.getBranch()!=null) {
							branch=ofy().load().type(Branch.class)
								.filter("companyId", companyId)
								.filter("buisnessUnitName", customerUser.getBranch()).first().now();
						}
						if (customerUser.getEmail() != null) {
							if (customerUser.getEmail().equals("")) {
								return "Email id is not registered with system,Please contact admin!";//do not change the message as condition written on this text in ForgotPasswordPresenter.java
							} else {
								// write a code which generate random password
								// and send it to user
								// also save generated password in CustomerUser
								// entity

								ranGenePass = StringUtils
										.generateRandomPassword();
								System.out
										.println("RANDOM GENERATED PASSWORD : "
												+ ranGenePass);

								customerUser.setPassword(ranGenePass);
								genImpl.save(customerUser);
								emailSubject = "Password Reset Successfully";
								fromEmailId = company.getEmail();
								emailidlist+="Company email- "+company.getEmail();
								if(company.getCellNumber1()!=null&&company.getCellNumber1()>0)
									emailidlist+=" - Contact on - "+company.getCellNumber1();
								if(branch!=null&&branch.getEmail()!=null&&!branch.getEmail().equals("")) {
									toemaillist.add(branch.getEmail());
									emailidlist+=",\n Branch email- "+branch.getEmail();
									if(branch.getCellNumber1()!=null&&branch.getCellNumber1()>0)
										emailidlist+=" - Contact on - "+branch.getCellNumber1();
								}
								toEmailId = customerUser.getEmail();
								emailidlist+=",\n "+customerUser.getEmail();
								
								emailMessage = "Dear "
										+ customerUser.getCustomerName()
										+ ","
										+ "\n"
										+ "\n"
										+ "\n"
										+ "Your password on "
										+ company.getCompanyURL()
										+ " has been reset successfully. Please find below the new credentials."
										+ "\n" + "\n" + "Username : "
										+ customerUser.getUserName() + "\n"
										+ "Password : " + ranGenePass + "\n";
								
								if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, company.getCompanyId())){
									logger.log(Level.SEVERE, "Inside Sendgird API wise Email ");
									SendGridEmailServlet sdEmail=new SendGridEmailServlet();
									toemaillist.add(toEmailId);
									sdEmail.sendMailWithSendGrid(fromEmailId, toemaillist, null, null, emailSubject, emailMessage, "text/html",null,null,"application/pdf", null, null, "application/pdf", null, "", "application/pdf", null, null, null, null);								

								}else
								emailObj.sendPasswordResetEmail(emailSubject,
										emailMessage, fromEmailId, toEmailId);

								return "Password is sent to "+emailidlist;
							}
						} else {
							return "Email id is not registered with system,Please contact admin!";//do not change the message as condition written on this text in ForgotPasswordPresenter.java
						}
					} else {
						return "Username is wrong!";
					}
				}
			} else if (uname == null && email != null) {
				user = ofy().load().type(User.class)
						.filter("companyId", companyId).filter("email", email)
						.first().now();
				if (user != null) {
					if(user.getBranch()!=null) {
						branch=ofy().load().type(Branch.class)
							.filter("companyId", companyId)
							.filter("buisnessUnitName", user.getBranch()).first().now();
					}
					
					
					// write a code which generate random password and send it
					// to user
					// also save generated password in User entity

					ranGenePass = StringUtils.generateRandomPassword();
					System.out.println("RANDOM GENERATED PASSWORD : "
							+ ranGenePass);

					user.setPassword(ranGenePass);
					user.setStatus(true);
					genImpl.save(user);
					emailSubject = "Password Reset Successfully";
					fromEmailId = company.getEmail();
					emailidlist+="Company email- "+company.getEmail();
					if(company.getCellNumber1()!=null&&company.getCellNumber1()>0)
						emailidlist+=" - Contact on - "+company.getCellNumber1();
					if(branch!=null&&branch.getEmail()!=null&&!branch.getEmail().equals("")) {
						toemaillist.add(branch.getEmail());
						emailidlist+=",\n Branch email- "+branch.getEmail();
						if(branch.getCellNumber1()!=null&&branch.getCellNumber1()>0)
							emailidlist+=" - Contact on - "+branch.getCellNumber1();
					}
					toEmailId = user.getEmail();
					emailidlist+=",\n "+user.getEmail();
					emailMessage = "Dear "
							+ user.getEmployeeName()
							+ ","
							+ "\n"
							+ "\n"
							+ "\n"
							+ "Your password on "
							+ company.getCompanyURL()
							+ " has been reset successfully. Please find below the new credentials."
							+ "\n" + "\n" + "Username : " + user.getUserName()
							+ "\n" + "Password : " + ranGenePass + "\n";
					emailObj.sendPasswordResetEmail(emailSubject, emailMessage,
							fromEmailId, toEmailId);

					return "Password is sent to "+emailidlist;
				} else {
					customerUser = ofy().load().type(CustomerUser.class)
							.filter("companyId", companyId)
							.filter("email", email).first().now();
					if (customerUser != null) {
						if(customerUser.getBranch()!=null) {
							branch=ofy().load().type(Branch.class)
								.filter("companyId", companyId)
								.filter("buisnessUnitName", customerUser.getBranch()).first().now();
						}

						// write a code which generate random password and send
						// it to user
						// also save generated password in CustomerUser entity

						ranGenePass = StringUtils.generateRandomPassword();
						System.out.println("RANDOM GENERATED PASSWORD : "
								+ ranGenePass);

						customerUser.setPassword(ranGenePass);
						genImpl.save(customerUser);

						logger.log(Level.SEVERE, "RANDOM GENERATED PASSWORD : "
								+ ranGenePass);
						emailSubject = "Password Reset Successfully";
						fromEmailId = company.getEmail();
						emailidlist+="Company email- "+company.getEmail();
						if(company.getCellNumber1()!=null&&company.getCellNumber1()>0)
							emailidlist+=" - Contact on - "+company.getCellNumber1();
						if(branch!=null&&branch.getEmail()!=null&&!branch.getEmail().equals("")) {
							toemaillist.add(branch.getEmail());
							emailidlist+=",\n Branch email- "+branch.getEmail();
							if(branch.getCellNumber1()!=null&&branch.getCellNumber1()>0)
								emailidlist+=" - Contact on - "+branch.getCellNumber1();
						}
						toEmailId = customerUser.getEmail();
						emailidlist+=", \n"+customerUser.getEmail();
						emailMessage = "Dear "
								+ customerUser.getCustomerName()
								+ ","
								+ "\n"
								+ "\n"
								+ "\n"
								+ "Your password on "
								+ company.getCompanyURL()
								+ " has been reset successfully. Please find below the new credentials."
								+ "\n" + "\n" + "Username : "
								+ customerUser.getUserName() + "\n"
								+ "Password : " + ranGenePass + "\n";
						
						if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, company.getCompanyId())){
							logger.log(Level.SEVERE, "Inside Sendgird API wise Email ");
							SendGridEmailServlet sdEmail=new SendGridEmailServlet();
							toemaillist.add(toEmailId);
							sdEmail.sendMailWithSendGrid(fromEmailId, toemaillist, null, null, emailSubject, emailMessage, "text/html",null,null,"application/pdf", null, null, "application/pdf", null, "", "application/pdf", null, null, null, null);								

						}else
							emailObj.sendPasswordResetEmail(emailSubject,
								emailMessage, fromEmailId, toEmailId);

						return "Password is sent to "+emailidlist;
					} else {
						return "Email id is wrong!";//do not change the message as condition written on this text in ForgotPasswordPresenter.java
					}
				}
			}

		} else {
			return null;
		}

		return null;
	}

	public static class StringUtils {
		private static final Random RANDOM = new SecureRandom();
		/** Length of password. @see #generateRandomPassword() */
		public static final int PASSWORD_LENGTH = 8;

		/**
		 * Generate a random String suitable for use as a temporary password.
		 */
		public static String generateRandomPassword() {
			// Pick from some letters that won't be easily mistaken for each
			// other. So, for example, omit o O and 0, 1 l and L.
			String letters = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789";

			String pw = "";
			for (int i = 0; i < PASSWORD_LENGTH; i++) {
				int index = (int) (RANDOM.nextDouble() * letters.length());
				pw += letters.substring(index, index + 1);
			}
			return pw;
		}
	}

	/**
	 * while changing password,old password password should be verified. Date :
	 * 20-09-2016 By Anil Release 30 Sept 2016
	 */

	@Override
	public Boolean isOldPasswordValid(User user, String oldPassword) {

		if (user != null) {

			// ForgotPasswordByAnil

			/**
			 * If Encryption of Password is On
			 */
			boolean matched = BCrypt.checkpw(oldPassword, user.getPassword());
			logger.log(Level.SEVERE, "Checking  Status " + matched);
			if (matched) {
				return true;
			}

			/**
			 * If Encryption of Password is Off
			 */
			// if (user.getPassword().equals(oldPassword)) {
			// return true;
			// }

		}
		return false;
	}
	
	
	/**
	 * @author Anil
	 * @since 01-07-2020
	 * This method returns total number of active license
	 * Issue - session time out due to simultaneous login or number of license exceeded.
	 * This was happening because system was reading number of license from two different field
	 * at the time of login it was reading/calculating from license list at the time of CURD operation it is reading from old number of license field
	 */
	
	public int getNoOfActiveLicense(Company company){
		SimpleDateFormat isoFormat = new SimpleDateFormat("dd-MM-yyyy");
		int noOfLicense=0;
		Date todaysDate=DateUtility.getDateWithTimeZone("IST", new Date());
		
		ArrayList<LicenseDetails> licenseDetailsList=new ArrayList<LicenseDetails>();
		if(company.getLicenseDetailsList()!=null&&company.getLicenseDetailsList().size()!=0){
			logger.log(Level.SEVERE,"TOTAL EVA LICENSE LIST : "+company.getLicenseDetailsList().size());
			for(LicenseDetails license:company.getLicenseDetailsList()){
				if(AppConstants.LICENSETYPELIST.EVA_ERP_LICENSE.equals(license.getLicenseType())&&license.getStatus()==true){
					licenseDetailsList.add(license);
				}
			}
			logger.log(Level.SEVERE,"ACTIVE EVE ERP LICENSE LIST SIZE : "+licenseDetailsList.size());
		}
		
		Comparator<LicenseDetails> licDtComp=new Comparator<LicenseDetails>() {
			@Override
			public int compare(LicenseDetails arg0, LicenseDetails arg1) {
				return arg0.getStartDate().compareTo(arg1.getStartDate());
			}
		};
		Collections.sort(licenseDetailsList, licDtComp);
		
		ArrayList<LicenseDetails> contractWiseActiveLicLis=new ArrayList<LicenseDetails>();
		for(LicenseDetails obj:licenseDetailsList){
			if(obj.getContractCount()!=0L){ //0 replaced by 0L as type changed from int to long. Ashwini Patil Date:22-12-2023 change made to store zoho salesorder id which is long
				contractWiseActiveLicLis.add(obj);
			}
		}
		
		ArrayList<LicenseDetails> licenseList=new ArrayList<LicenseDetails>();
		if(contractWiseActiveLicLis.size()!=0){
			licenseList=contractWiseActiveLicLis;
		}else{
			licenseList=licenseDetailsList;
		}
		if(licenseList.size()!=0){
			for(LicenseDetails license:licenseList){
				/**
				 * @author Anil
				 * @since 21-08-2020
				 * Om Pest Issue-license is not activated for same day login as start date
				 */
				if(todaysDate.after(license.getStartDate())||isoFormat.format(todaysDate).equals(isoFormat.format(license.getStartDate()))){
					if(todaysDate.before(license.getEndDate())||isoFormat.format(todaysDate).equals(isoFormat.format(license.getEndDate()))){
						noOfLicense=noOfLicense+license.getNoOfLicense();
					}
				}
			}
		}
		logger.log(Level.SEVERE,"TOTAL ACTIVE LICENSE LIST : "+noOfLicense);
		if(company.getNoOfUser()!=noOfLicense){
			company.setNoOfUser(noOfLicense);
			ofy().save().entity(company).now();
		}
		return noOfLicense;
	}

	@Override
	public String sendPasswordResetEmailToCompanyAndBranch(String companyName, String uname) {
		// TODO Auto-generated method stub
		
		GenricServiceImpl genImpl = new GenricServiceImpl();
		Email emailObj = new Email();
		User user;
		CustomerUser customerUser;
		String emailSubject;
		String emailMessage;
		String fromEmailId;
		Branch branch=null;
		String emailidlist="";
		ArrayList<String> toemaillist=new ArrayList<String>();
		
		Company company = ofy().load().type(Company.class)
				.filter("accessUrl", companyName).first().now();
		if (company == null) {
			System.out.println("Company Null");
			return null;
		}
		String companyStatus = company.getStatus();
		if (companyStatus.equals(Company.ACTIVE)
				|| companyStatus.equals(Company.DEMO)) {
			Long companyId = company.getCompanyId();
			String ranGenePass = "";
			if (uname != null && !uname.equals("")) {
			user = ofy().load().type(User.class)
									.filter("companyId", companyId)
									.filter("userName", uname).first().now();
							
			if (user != null) {
				
				if(user.getBranch()!=null) {
					branch=ofy().load().type(Branch.class)
						.filter("companyId", companyId)
						.filter("buisnessUnitName", user.getBranch()).first().now();
				}
				

						ranGenePass = StringUtils.generateRandomPassword();
						System.out.println("RANDOM GENERATED PASSWORD : "
								+ ranGenePass);

						user.setPassword(ranGenePass);
						genImpl.save(user);
						user.setStatus(true);
						System.out.println("AFTER SAVE");
						emailSubject = "Password Reset Successfully";
						fromEmailId = company.getEmail();
						emailidlist+="Company email- "+company.getEmail();
						if(company.getCellNumber1()!=null&&company.getCellNumber1()>0)
							emailidlist+=" - Contact on - "+company.getCellNumber1();
						if(branch!=null&&branch.getEmail()!=null&&!branch.getEmail().equals("")) {
							toemaillist.add(branch.getEmail());
							emailidlist+=",\n Branch email- "+branch.getEmail();
							if(branch.getCellNumber1()!=null&&branch.getCellNumber1()>0)
								emailidlist+=" - Contact on - "+branch.getCellNumber1();
						}
						emailMessage = "Dear "
								+ user.getEmployeeName()
								+ ","
								+ "\n"
								+ "\n"
								+ "\n"
								+ "Your password on "
								+ company.getCompanyURL()
								+ " has been reset successfully. Please find below the new credentials."
								+ "\n" + "\n" + "Username : "
								+ user.getUserName() + "\n" + "Password : "
								+ ranGenePass + "\n";
						if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, company.getCompanyId())){
							logger.log(Level.SEVERE, "Inside Sendgird API wise Email ");
							SendGridEmailServlet sdEmail=new SendGridEmailServlet();
							sdEmail.sendMailWithSendGrid(fromEmailId, toemaillist, null, null, emailSubject, emailMessage, "text/html",null,null,"application/pdf", null, null, "application/pdf", null, "", "application/pdf", null, null, null, null);								

						}else if(toemaillist.size()>0)
							emailObj.sendPasswordResetEmail(emailSubject,
								emailMessage, fromEmailId, toemaillist.get(0));

						return "Password is sent to "+emailidlist;
			}else {			

				customerUser = ofy().load().type(CustomerUser.class)
						.filter("companyId", companyId)
						.filter("userName", uname).first().now();
						
				if (customerUser != null) {
					
					if(customerUser.getBranch()!=null) {
						branch=ofy().load().type(Branch.class)
							.filter("companyId", companyId)
							.filter("buisnessUnitName", customerUser.getBranch()).first().now();
					}
							ranGenePass = StringUtils
									.generateRandomPassword();
							System.out
									.println("RANDOM GENERATED PASSWORD : "
											+ ranGenePass);

							customerUser.setPassword(ranGenePass);
							genImpl.save(customerUser);
							emailSubject = "Password Reset Successfully";
							fromEmailId = company.getEmail();
							emailidlist+="Company email- "+company.getEmail();
							if(company.getCellNumber1()!=null&&company.getCellNumber1()>0)
								emailidlist+=" - Contact on - "+company.getCellNumber1();
							if(branch!=null&&branch.getEmail()!=null&&!branch.getEmail().equals("")) {
								toemaillist.add(branch.getEmail());
								emailidlist+=",\n Branch email- "+branch.getEmail();
								if(branch.getCellNumber1()!=null&&branch.getCellNumber1()>0)
									emailidlist+=" - Contact on - "+branch.getCellNumber1();
							}
							
							emailMessage = "Dear "
									+ customerUser.getCustomerName()
									+ ","
									+ "\n"
									+ "\n"
									+ "\n"
									+ "Your password on "
									+ company.getCompanyURL()
									+ " has been reset successfully. Please find below the new credentials."
									+ "\n" + "\n" + "Username : "
									+ customerUser.getUserName() + "\n"
									+ "Password : " + ranGenePass + "\n";
							
							if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, company.getCompanyId())){
								logger.log(Level.SEVERE, "Inside Sendgird API wise Email ");
								SendGridEmailServlet sdEmail=new SendGridEmailServlet();
								sdEmail.sendMailWithSendGrid(fromEmailId, toemaillist, null, null, emailSubject, emailMessage, "text/html",null,null,"application/pdf", null, null, "application/pdf", null, "", "application/pdf", null, null, null, null);								

							}else if(toemaillist.size()>0)
							emailObj.sendPasswordResetEmail(emailSubject,
									emailMessage, fromEmailId, toemaillist.get(0));

							return "Password is sent to "+emailidlist;						
					
				} else {
					return "Username is wrong!";
				}
			
			}
				
			}else {
				return "Username cannnot be blank";
			}
			}//end of company check
		
		return "Failed";
	}

}
