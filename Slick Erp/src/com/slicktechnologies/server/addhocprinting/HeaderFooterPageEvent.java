package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.net.URL;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

/**
 * 
 * @author Vijay Date :- 19-02-2022
 * Des :- This class used for automatic header footer for every page
 *
 */
public class HeaderFooterPageEvent extends PdfPageEventHelper{

	int documentId;
	String preprintStatus;
	
	Quotation quotation;
	Company company;
	Customer cust;
	Branch branchEntity;
	
	boolean branchAsCompanyFlag = false;
	String preprinttype = "No";
	
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);

	
	public HeaderFooterPageEvent(Company comp, String branchName, String printtype){
		
		company = comp;
		branchEntity = ofy().load().type(Branch.class).filter("buisnessUnitName", branchName).filter("companyId", comp.getCompanyId()).first().now();
		
		branchAsCompanyFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId());
		preprinttype = printtype;
		
	}
	
	public void onStartPage(PdfWriter writer, Document document) {
  	  
		if(preprinttype.equalsIgnoreCase("No")) {
			try {
				
				DocumentUpload document1 = null;
				if(company.getUploadHeader()!=null)
					document1=company.getUploadHeader();
				if(branchAsCompanyFlag && branchEntity!=null && branchEntity.getUploadHeader()!=null){
					document1 = branchEntity.getUploadHeader();
				}
				
				// patch
				String hostUrl;
				String environment = System
						.getProperty("com.google.appengine.runtime.environment");
				if (environment.equals("Production")) {
					String applicationId = System
							.getProperty("com.google.appengine.application.id");
					String version = System
							.getProperty("com.google.appengine.application.version");
					hostUrl = "http://" + version + "." + applicationId
							+ ".appspot.com/";
				} else {
					hostUrl = "http://localhost:8888";
				}

				    try {
				    	Image image =  Image.getInstance(new URL(hostUrl
								+ document1.getUrl()));
				        image.setAlignment(Element.ALIGN_CENTER);
				        
				        image.scalePercent(15f);
				        image.scaleAbsoluteWidth(520f);
				        image.setAbsolutePosition(40f, 725f);
				        document.add(image);
				    } catch (IOException | DocumentException e) {
				    	e.printStackTrace();
				    }
				    
		  	    } catch (Exception e) {
		  	    	e.printStackTrace();
				}   
		}
		else if(preprinttype.equals("yes")) {
			
				Phrase blankphrase = new Phrase("", font8);
				PdfPCell blankCell = new PdfPCell();
				blankCell.addElement(blankphrase);
				blankCell.setBorder(0);
				try {
					document.add(blankCell);
					document.add(blankCell);
					document.add(blankCell);
					document.add(blankCell);
					document.add(blankCell);
					document.add(blankCell);
					
				} catch (DocumentException e) {
					e.printStackTrace();
				}
		}
		
		/** for local ***/
		
//		 try
//		 {
//		 Image
//		 image1=Image.getInstance("images/headerimg.jpg");
//		 image1.scalePercent(15f);
//		 image1.scaleAbsoluteWidth(520f);
//		 image1.setAbsolutePosition(40f, 725f);
//		 document.add(image1);
////		 writer.getDirectContent().addImage(image1, true);
//		 
//		 }
//		 catch(Exception e)
//		 {
//		 e.printStackTrace();
//		 }
		 
		    
    }

    public void onEndPage(PdfWriter writer, Document document) {

		if(preprinttype.equalsIgnoreCase("No")) {
			
			try {
			   	
		    	DocumentUpload document1 = null;
				if(company.getUploadFooter()!=null)
					document1=company.getUploadFooter();
				
		    	if(branchAsCompanyFlag && branchEntity!=null && branchEntity.getUploadHeader()!=null){
					document1 = branchEntity.getUploadFooter();
				}
				
				// patch
				String hostUrl;
				String environment = System
						.getProperty("com.google.appengine.runtime.environment");
				if (environment.equals("Production")) {
					String applicationId = System
							.getProperty("com.google.appengine.application.id");
					String version = System
							.getProperty("com.google.appengine.application.version");
					hostUrl = "http://" + version + "." + applicationId
							+ ".appspot.com/";
				} else {
					hostUrl = "http://localhost:8888";
				}

				
				    try {
				    	Image image =  Image.getInstance(new URL(hostUrl
								+ document1.getUrl()));
				        image.setAlignment(Element.ALIGN_CENTER);
				        
				        image.scalePercent(15f);
				        image.scaleAbsoluteWidth(520f);
				        image.setAbsolutePosition(40f,40f);
				        
				        document.add(image);
				    } catch (IOException | DocumentException e) {
				    	e.printStackTrace();
				    }
		  	    
		    	
		    	} catch (Exception e) {
		    		e.printStackTrace();
				}
		}
		else if(preprinttype.equals("yes")) {
			
			Phrase blankphrase = new Phrase("", font8);
			PdfPCell blankCell = new PdfPCell();
			blankCell.addElement(blankphrase);
			blankCell.setBorder(0);
			try {
				document.add(blankCell);
				document.add(blankCell);
				document.add(blankCell);
				document.add(blankCell);
				document.add(blankCell);
				document.add(blankCell);
				
			} catch (DocumentException e) {
				e.printStackTrace();
			}
	}

    	
//    	/** for local ***/
//    	 try
//		 {
//		 Image
//		 image=Image.getInstance("images/footerimg.jpg");
////		 image1.scalePercent(15f);
////		 image1.scaleAbsoluteWidth(520f);
////		 image1.setAbsolutePosition(40f,40f);
////		 document.add(image1);
//		 
//		 image.scalePercent(15f);
//		 image.scaleAbsoluteWidth(520f);
//		 image.setAbsolutePosition(40f,40f);
////		 writer.getDirectContent().addImage(image, true);
//		 document.add(image);
//
//		 
//		 }
//		 catch(Exception e)
//		 {
//		 e.printStackTrace();
//		 }
    }
    
}
