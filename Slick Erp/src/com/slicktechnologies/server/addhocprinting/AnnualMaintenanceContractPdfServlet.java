package com.slicktechnologies.server.addhocprinting;

import javax.servlet.http.HttpServlet;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.shared.Contract;

public class AnnualMaintenanceContractPdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2650048405424725508L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {

			String stringid = req.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			System.out.println("Vaishnavi +++++++++++++++"+stringid);

			AnnualMaintenanceContractPdf amcpdf = new AnnualMaintenanceContractPdf();

			Contract con = ofy().load().type(Contract.class).id(count).now();
			
			amcpdf.document = new Document();
			Document document = amcpdf.document;
			PdfWriter writer = PdfWriter.getInstance(document,
					resp.getOutputStream()); // write

			document.open();

			amcpdf.setpdfamc(con);
			amcpdf.createPdf();

			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
}
