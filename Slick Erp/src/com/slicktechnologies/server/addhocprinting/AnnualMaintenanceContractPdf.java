package com.slicktechnologies.server.addhocprinting;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import static com.googlecode.objectify.ObjectifyService.ofy;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
public class AnnualMaintenanceContractPdf {

	Document document;

	Logger logger = Logger.getLogger("Name of logger");

	ProcessConfiguration processConfig;
	boolean forAmcFlag = false;
	boolean equipmentFlag = false;

	Company comp;
	Customer cust;
	Contract con;
	Phrase chunk;
	String str;

	Phrase blank;
	Phrase col;

	PdfPCell pdfsr, pdfname, pdfbrand, pdfmod, pdfdt1, pdfdt2, pdfprodName;

	float[] columnWidth = { (float) 33.33, (float) 33.33, (float) 33.33 };
	float[] columnWidth1 = { 5f, 0.5f, 8f };
	float[] colWidth = { 0.5f, 4f, 3f, 2f, 2.9f, 2.4f };
	float[] colWidth1 = { 20f, 10f, 70f };
	float[] colWidth2 = { 20f, 10f, 20f, 50f };
	float[] colWidth3 = { 0.3f, 1.1f };
	// float[] colWidth4 = { 0.2f, 0.8f };
	float[] colWidth5 = { 0.1f, 0.2f };
	float[] colWidth6 = { 0.1f, 0.3f, 0.1f, 0.2f, 0.2f, 0.2f };

	DecimalFormat df = new DecimalFormat("0.00");

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt1 = new SimpleDateFormat("MMM-yyyy");

	private Font font16boldul, font12bold, font8bold, font8, font16bold,
			font12boldul, font12, font14bold, font10, font10bold, font10boldul,
			font9, font9bold, font14boldul, font9boldul;

	public AnnualMaintenanceContractPdf() {

		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD
				| Font.UNDERLINE);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD
				| Font.UNDERLINE);
		font9boldul = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD
				| Font.UNDERLINE);

	}

	public void setpdfamc(Contract count) {
		System.out.println("Vaishnavi " + count);

		con = count;

		// con = ofy().load().type(Contract.class).id(count).now();

		//
		if (con.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", con.getCompanyId()).first().now();
		System.out.println("Vaishnavi :::::::::::::::::" + comp);

		if (con.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", con.getCustomerId()).first().now();
		else
			cust = ofy().load().type(Customer.class)
					.filter("companyId", con.getCompanyId())
					.filter("count", con.getCustomerId()).first().now();
		System.out.println("Vaishnavi ++++++++++++++++" + cust);

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));

		if (con.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", con.getCompanyId())
					.filter("processName", "Contract")
					.filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("AMCForGenesis")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						forAmcFlag = true;
					}
				}
			}
		}

	}

	public void createPdf() {

		// mymethodfortesting();
		createLogo(document, comp);
		createBlankHeading();
		createTitle();
		createUserTitle();
		createContractInfo();
		createCustomerDetail1();
		createCustomerDetail2();
		createCustomerDetail3();
		createSystemDetail();
		createAssetTable();
		createInWordsDetail();
		createRemarkHeading();
		createLine();
		createBottom();
		createDates();

		String amountInWord = ServiceInvoicePdf.convert(con.getNetpayable());

	}

	private void mymethodfortesting() {

		Phrase my = new Phrase("Rohan=============" + con.getCount(), font8);
		Paragraph para = new Paragraph(my);

		try {
			document.add(para);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createLogo(Document doc, Company comp) {

		DocumentUpload document = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f, 765f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createBlankHeading() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		// blank.add(Chunk.NEWLINE);
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		PdfPTable bltable = new PdfPTable(1);
		bltable.addCell(blcell);
		bltable.setWidthPercentage(100);

		try {
			document.add(bltable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createTitle() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		Phrase amc = new Phrase("ANNUAL MAINTENANCE CONTRACT", font14boldul);
		PdfPCell amccell = new PdfPCell(amc);
		amccell.setBorder(0);
		amccell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable amctable = new PdfPTable(1);
		amctable.setWidthPercentage(100);
		amctable.addCell(amccell);
		amctable.addCell(blcell);

		PdfPCell cell = new PdfPCell(amctable);
		cell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		// table.addCell(blcell);
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createUserTitle() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		Phrase user = new Phrase("USER DETAILS", font12boldul);
		PdfPCell usercell = new PdfPCell(user);
		usercell.setBorder(0);
		usercell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable usertable = new PdfPTable(1);
		usertable.setWidthPercentage(100);
		usertable.addCell(usercell);
		usertable.addCell(blcell);

		PdfPCell cell = new PdfPCell(usertable);
		cell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		table.addCell(blcell);
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createContractInfo() {

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		Phrase dt1 = null;
		if (con.getStartDate() != null) {
			dt1 = new Phrase("CONTRACT START DATE : "
					+ fmt.format(con.getStartDate()), font9bold);
		}
		PdfPCell dt1cell = new PdfPCell(dt1);
		dt1cell.setBorder(0);
		dt1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase id = null;
		if (con.getCount() != 0) {
			id = new Phrase("CONTRACT ID : " + con.getCount(), font9bold);
		}
		PdfPCell idcell = new PdfPCell(id);
		idcell.setBorder(0);
		idcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase dt2 = null;
		if (con.getEndDate() != null) {
			dt2 = new Phrase("CONTRACT END DATE : "
					+ fmt.format(con.getEndDate()), font9bold);
		}
		PdfPCell dt2cell = new PdfPCell(dt2);
		dt2cell.setBorder(0);
		dt2cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		PdfPTable infoTable = new PdfPTable(3);
		infoTable.setWidthPercentage(100f);
		infoTable.addCell(dt1cell);
		infoTable.addCell(idcell);
		infoTable.addCell(dt2cell);

		PdfPCell cell = new PdfPCell(infoTable);
		cell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);
		parentTable.addCell(blcell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCustomerDetail1() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		Phrase col = new Phrase(":");
		PdfPCell colcell = new PdfPCell(col);
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// *************left table**************
		PdfPTable lefttable = new PdfPTable(3);

		try {
			lefttable.setWidths(columnWidth1);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		lefttable.setWidthPercentage(100f);

		Phrase custname = new Phrase("CUSTOMER NAME ", font9bold);
		PdfPCell custnamecell = new PdfPCell(custname);
		custnamecell.setBorder(0);
		custnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase company = null;
		System.out
				.println("company===================" + cust.getCompanyName());

		if (cust.isCompany() == true) {
			company = new Phrase(" " + cust.getCompanyName(), font9);
		} else {
			company = new Phrase(" " + cust.getFullname(), font9);
		}
		PdfPCell companycell = new PdfPCell(company);
		companycell.setBorder(0);
		companycell.setHorizontalAlignment(Element.ALIGN_LEFT);

		lefttable.addCell(custnamecell);
		lefttable.addCell(colcell);
		lefttable.addCell(companycell);

		PdfPCell leftcell = new PdfPCell(lefttable);
		leftcell.setBorder(0);

		// ************right table**************
		PdfPTable righttable = new PdfPTable(3);

		try {
			righttable.setWidths(columnWidth1);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		righttable.setWidthPercentage(100f);

		Phrase land1 = new Phrase("TEL. NO.", font9bold);
		PdfPCell land1cell = new PdfPCell(land1);
		land1cell.setBorder(0);
		land1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase landline = null;
		if (cust.getLandline() != null) {
			landline = new Phrase(" " + cust.getLandline(), font9);
		}
		PdfPCell landlinecell = new PdfPCell(landline);
		landlinecell.setBorder(0);
		landlinecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		righttable.addCell(land1cell);
		righttable.addCell(colcell);
		righttable.addCell(landlinecell);

		PdfPCell rightcell = new PdfPCell(righttable);
		rightcell.setBorder(0);

		PdfPTable table = new PdfPTable(2);
		table.addCell(leftcell);
		table.addCell(rightcell);
		table.addCell(blcell);
		table.addCell(blcell);

		try {
			table.setWidths(new float[] { 60, 40 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createCustomerDetail2() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		Phrase col = new Phrase(":");
		PdfPCell colcell = new PdfPCell(col);
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// ***********left table**************
		PdfPTable lefttable = new PdfPTable(3);

		try {
			lefttable.setWidths(columnWidth1);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		lefttable.setWidthPercentage(100f);

		Phrase add = new Phrase("INST. ADDRESS ", font9bold);
		PdfPCell addcell = new PdfPCell(add);
		addcell.setBorder(0);
		addcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String custAdd1 = "";
		String custFullAdd1 = "";

		if (cust.getAdress() != null) {
			logger.log(Level.SEVERE, "111111111111111111");
			if (!cust.getAdress().getAddrLine2().equals("")) {
				logger.log(Level.SEVERE, "22222222222222222");
				logger.log(Level.SEVERE, "hiii"
						+ cust.getAdress().getAddrLine2());
				if (!cust.getAdress().getLandmark().equals("")) {
					logger.log(Level.SEVERE, "333333333333333");
					custAdd1 = cust.getAdress().getAddrLine1() + "," + "\n"
							+ cust.getAdress().getAddrLine2() + "," + "\n"
							+ cust.getAdress().getLandmark();
				} else {
					logger.log(Level.SEVERE, "4444444444444");
					custAdd1 = cust.getAdress().getAddrLine1() + "," + "\n"
							+ cust.getAdress().getAddrLine2();
				}
			} else {
				logger.log(Level.SEVERE, "5555555555555");
				if (!cust.getAdress().getLandmark().equals("")) {
					logger.log(Level.SEVERE, "66666666666666666");
					custAdd1 = cust.getAdress().getAddrLine1() + "," + "\n"
							+ cust.getAdress().getLandmark();
				} else {
					logger.log(Level.SEVERE, "77777777777777777");
					custAdd1 = cust.getAdress().getAddrLine1();
				}
			}

			if (cust.getAdress().getLocality() != null) {
				custFullAdd1 = custAdd1 + "\n" + cust.getAdress().getCountry()
						+ "," + cust.getAdress().getState() + ","
						+ cust.getAdress().getCity() + "\n"
						// + cust.getAdress().getLocality() + " "+
						// "\n" +
						+ "Pin : " + cust.getAdress().getPin();

			} else {
				custFullAdd1 = custAdd1 + "\n" + cust.getAdress().getCountry()
						+ "," + cust.getAdress().getState() + ","
						+ cust.getAdress().getCity() + " "
						+ cust.getAdress().getPin();
			}
		}

		Phrase custAddInfo = new Phrase(custFullAdd1, font9);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfoCell.setBorder(0);

		lefttable.addCell(addcell);
		lefttable.addCell(colcell);
		lefttable.addCell(custAddInfoCell);

		lefttable.addCell(blcell);
		lefttable.addCell(blcell);
		lefttable.addCell(blcell);

		lefttable.addCell(blcell);
		lefttable.addCell(blcell);
		lefttable.addCell(blcell);

		PdfPCell leftcell = new PdfPCell(lefttable);
		leftcell.setBorder(0);

		// ***********right table**************
		PdfPTable righttable = new PdfPTable(3);

		try {
			righttable.setWidths(columnWidth1);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		righttable.setWidthPercentage(100f);

		Phrase cel1 = new Phrase("TEL. NO.", font9bold);
		PdfPCell cel1cell = new PdfPCell(cel1);
		cel1cell.setBorder(0);
		cel1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase cell1 = null;
		if (cust.getCellNumber1() != null) {
			cell1 = new Phrase(" " + cust.getCellNumber1(), font9);
		}
		PdfPCell cell1cell = new PdfPCell(cell1);
		cell1cell.setBorder(0);
		cell1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase cel2 = new Phrase("TEL. NO.", font9bold);
		PdfPCell cel2cell = new PdfPCell(cel2);
		cel2cell.setBorder(0);
		cel2cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase cell2 = null;
		if (cust.getCellNumber2() != null) {
			cell2 = new Phrase(" " + cust.getCellNumber2(), font9);
		}
		PdfPCell cell2cell = new PdfPCell(cell2);
		cell2cell.setBorder(0);
		cell2cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		righttable.addCell(cel1cell);
		righttable.addCell(colcell);
		righttable.addCell(cell1cell);

		righttable.addCell(blcell);
		righttable.addCell(blcell);
		righttable.addCell(blcell);

		righttable.addCell(cel2cell);
		righttable.addCell(colcell);
		righttable.addCell(cell2cell);

		PdfPCell rightcell = new PdfPCell(righttable);
		rightcell.setBorder(0);

		PdfPTable table = new PdfPTable(2);
		table.addCell(leftcell);
		table.addCell(rightcell);

		try {
			table.setWidths(new float[] { 60, 40 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createCustomerDetail3() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		Phrase col = new Phrase(":");
		PdfPCell colcell = new PdfPCell(col);
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// **********left table*******************
		PdfPTable lefttable = new PdfPTable(3);

		try {
			lefttable.setWidths(columnWidth1);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		lefttable.setWidthPercentage(100f);

		Phrase poc = new Phrase("CONTACT PERSON", font9bold);
		PdfPCell poccell = new PdfPCell(poc);
		poccell.setBorder(0);
		poccell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase pocname = null;
		if (cust.getFullname() != null) {
			pocname = new Phrase(" " + cust.getFullname(), font9);
		}
		PdfPCell pocnamecell = new PdfPCell(pocname);
		pocnamecell.setBorder(0);
		pocnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		lefttable.addCell(poccell);
		lefttable.addCell(colcell);
		lefttable.addCell(pocnamecell);

		lefttable.addCell(blcell);
		lefttable.addCell(blcell);
		lefttable.addCell(blcell);

		PdfPCell hrscell = null;
		PdfPCell hrs1cell = null;
		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {

			if (comp.getArticleTypeDetails().get(i).getArticleTypeName()
					.equals("BUSINESS HOURS")) {
				Phrase hrs = null;
				if (comp.getArticleTypeDetails().get(i).getArticleTypeName() != null) {
					hrs = new Phrase(comp.getArticleTypeDetails().get(i)
							.getArticleTypeName(), font9bold);
				}
				hrscell = new PdfPCell(hrs);
				hrscell.setBorder(0);
				hrscell.setHorizontalAlignment(Element.ALIGN_LEFT);

				Phrase hrs1 = null;
				if (comp.getArticleTypeDetails().get(i).getArticleTypeValue() != null) {
					hrs1 = new Phrase(comp.getArticleTypeDetails().get(i)
							.getArticleTypeValue(), font9);
				}
				hrs1cell = new PdfPCell(hrs1);
				hrs1cell.setBorder(0);
				hrs1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

				lefttable.addCell(hrscell);
				lefttable.addCell(colcell);
				lefttable.addCell(hrs1cell);
			}
		}

		PdfPCell leftcell = new PdfPCell(lefttable);
		leftcell.setBorder(0);

		// ***********right table**************
		PdfPTable righttable = new PdfPTable(3);

		try {
			righttable.setWidths(columnWidth1);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		righttable.setWidthPercentage(100f);

		Phrase designation = new Phrase("DESIGNATION", font9bold);
		PdfPCell designationcell = new PdfPCell(designation);
		designationcell.setBorder(0);
		designationcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase dname = null;
		if (cust.getCustCorresponence() != null) {
			dname = new Phrase(" " + cust.getCustCorresponence(), font9);
		}
		PdfPCell dnamecell = new PdfPCell(dname);
		dnamecell.setBorder(0);
		dnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// ///////////

		righttable.addCell(designationcell);
		righttable.addCell(colcell);
		righttable.addCell(dnamecell);

		righttable.addCell(blcell);
		righttable.addCell(blcell);
		righttable.addCell(blcell);

		PdfPCell weekcell = null;
		PdfPCell week1cell = null;
		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
			if (comp.getArticleTypeDetails().get(i).getArticleTypeName()
					.equals("WEEKLY OFF")) {
				Phrase week = null;
				if (comp.getArticleTypeDetails().get(i).getArticleTypeName() != null) {
					week = new Phrase(comp.getArticleTypeDetails().get(i)
							.getArticleTypeName(), font9bold);
				}
				weekcell = new PdfPCell(week);
				weekcell.setBorder(0);
				weekcell.setHorizontalAlignment(Element.ALIGN_LEFT);

				Phrase week1 = null;
				if (comp.getArticleTypeDetails().get(i).getArticleTypeValue() != null) {
					week1 = new Phrase(comp.getArticleTypeDetails().get(i)
							.getArticleTypeValue(), font9);
				}
				week1cell = new PdfPCell(week1);
				week1cell.setBorder(0);
				week1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

				righttable.addCell(weekcell);
				righttable.addCell(colcell);
				righttable.addCell(week1cell);

			}
		}

		PdfPCell rightcell = new PdfPCell(righttable);
		rightcell.setBorder(0);

		PdfPTable table = new PdfPTable(2);
		table.addCell(leftcell);
		table.addCell(rightcell);

		try {
			table.setWidths(new float[] { 60, 40 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);

		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
		parenttable.addCell(blcell);
		parenttable.addCell(blcell);
		parenttable.setWidthPercentage(100);

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	// public void createEquipmentDetail() {
	//
	// Paragraph blank = new Paragraph();
	// blank.add(Chunk.NEWLINE);
	// blank.add(Chunk.NEWLINE);
	//
	// PdfPCell blankcell = new PdfPCell(blank);
	// blankcell.setBorder(0);
	//
	// PdfPTable equiptable = new PdfPTable(3);
	//
	// try {
	// equiptable.setWidths(new float[] {40f, 30f, 30f });
	// } catch (DocumentException e) {
	// e.printStackTrace();
	// }
	// equiptable.setWidthPercentage(100);
	//
	//
	// Phrase equipment = new Phrase("SYSTEM DETAILS  :", font10bold);
	// PdfPCell equipcell = new PdfPCell(equipment);
	// equipcell.setBorder(0);
	// equipcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	// /**************************************/
	// Phrase KTS = null;
	// PdfPCell tvcell = null;
	// if(equipmentFlag==true){
	// KTS =new Phrase("KTS",font10bold);
	// }
	// else{
	// KTS =new Phrase("KTS",font10);
	// }
	//
	// tvcell = new PdfPCell(KTS);
	// tvcell.setBorder(0);
	// tvcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	//
	// Phrase EPABX = null;
	// PdfPCell vdpcell = null;
	//
	// if(equipmentFlag==false){
	// EPABX = new Phrase("EPABX",font10bold);
	// }
	// else{
	// EPABX = new Phrase("EPABX",font10);
	// }
	//
	// vdpcell = new PdfPCell(EPABX);
	// vdpcell.setBorder(0);
	// vdpcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	// /********************************************************/
	//
	// equiptable.addCell(equipcell);
	// equiptable.addCell(equipcell);
	// equiptable.addCell(equipcell);
	//
	// // for (int i = 0; i < wo.getBomTable().size(); i++) {
	// //
	// // chunk = new Phrase(wo.getBomTable().get(i).getProductName() + "",
	// // font10bold);
	// // pdfprodName = new PdfPCell(chunk);
	// // pdfprodName.setBorderWidthLeft(0);
	// // pdfprodName.setBorderWidthTop(0);
	// // pdfprodName.setBorderWidthRight(0);
	// // pdfprodName.setHorizontalAlignment(Element.ALIGN_LEFT);
	// //
	// // equiptable.addCell(pdfprodName);
	// //
	// // }
	//
	//
	//
	// PdfPCell cell = new PdfPCell(equiptable);
	// cell.setBorder(0);
	//
	// PdfPTable parenttable = new PdfPTable(1);
	// parenttable.addCell(cell);
	// parenttable.addCell(blankcell);
	// parenttable.setWidthPercentage(100);
	//
	// try {
	// document.add(parenttable);
	// } catch (DocumentException e) {
	// e.printStackTrace();
	// }
	// }
	/********************************************************************************/

	public void createSystemDetail() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);

		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);

		PdfPTable systemtable = new PdfPTable(2);

		Phrase system = new Phrase("SYSTEM DETAILS  : ", font9bold);
		PdfPCell systemcell = new PdfPCell(system);
		systemcell.setBorder(0);
		systemcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		systemtable.addCell(systemcell);

		for (int i = 0; i < con.getItems().size(); i++) {

			chunk = new Phrase(con.getItems().get(i).getProductName() + "",
					font9bold);
			pdfprodName = new PdfPCell(chunk);
			pdfprodName.setBorderWidthLeft(0);
			pdfprodName.setBorderWidthTop(0);
			pdfprodName.setBorderWidthRight(0);
			pdfprodName.setHorizontalAlignment(Element.ALIGN_LEFT);

			systemtable.addCell(pdfprodName);

		}

		try {
			systemtable.setWidths(new float[] { 20, 80 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		systemtable.setWidthPercentage(100);

		PdfPCell cell = new PdfPCell(systemtable);
		cell.setBorder(0);

		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
		parenttable.addCell(blankcell);
		parenttable.setWidthPercentage(100);

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createAssetTable() {

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		PdfPTable table = new PdfPTable(6);

		try {
			table.setWidths(colWidth6);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100);
		
		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		Phrase srno = new Phrase("SR.No.", font1);
		Phrase name = new Phrase("ASSET", font1);
		Phrase brand = new Phrase("BRAND", font1);
		Phrase model = new Phrase("MODEL NO.", font1);
		Phrase dt1 = new Phrase("INSTALLATION DATE", font1);
		Phrase dt2 = new Phrase("WARRANTY UNTIL", font1);

		PdfPCell srnocell = new PdfPCell(srno);
		srnocell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell namecell = new PdfPCell(name);
		namecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell brandcell = new PdfPCell(brand);
		brandcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell modelcell = new PdfPCell(model);
		modelcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell dt1cell = new PdfPCell(dt1);
		dt1cell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell dt2cell = new PdfPCell(dt2);
		dt2cell.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(srnocell);
		table.addCell(namecell);
		table.addCell(brandcell);
		table.addCell(modelcell);
		table.addCell(dt1cell);
		table.addCell(dt2cell);

		for (int i = 0; i < con.getClientSideAsset().size(); i++) {

			chunk = new Phrase(i + 1 + "", font8);
			pdfsr = new PdfPCell(chunk);
			pdfsr.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(con.getClientSideAsset().get(i).getName() + "",
					font8);
			pdfname = new PdfPCell(chunk);
			pdfname.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(con.getClientSideAsset().get(i).getBrand() + "",
					font8);
			pdfbrand = new PdfPCell(chunk);
			pdfbrand.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(con.getClientSideAsset().get(i).getModelNo()
					+ "", font8);
			pdfmod = new PdfPCell(chunk);
			pdfmod.setHorizontalAlignment(Element.ALIGN_CENTER);

			if (con.getClientSideAsset().get(i).getDateOfInstallation() != null) {
				chunk = new Phrase(fmt.format(con.getClientSideAsset().get(i)
						.getDateOfInstallation())
						+ "", font8);
			} else {
				chunk = new Phrase("", font8);
			}
			pdfdt1 = new PdfPCell(chunk);
			pdfdt1.setHorizontalAlignment(Element.ALIGN_CENTER);

			if (con.getClientSideAsset().get(i).isWarrenty() != null) {
				chunk = new Phrase(fmt.format(con.getClientSideAsset().get(i)
						.isWarrenty())
						+ "", font8);
			} else {
				chunk = new Phrase("", font8);
			}
			pdfdt2 = new PdfPCell(chunk);
			pdfdt2.setHorizontalAlignment(Element.ALIGN_CENTER);

			table.addCell(pdfsr);
			table.addCell(pdfname);
			table.addCell(pdfbrand);
			table.addCell(pdfmod);
			table.addCell(pdfdt1);
			table.addCell(pdfdt2);

		}
		Logger logger = Logger.getLogger("Name of logger");
		System.out.println("SIZE OF CLIENTSIDE ASSETS LIST....."
				+ con.getClientSideAsset().size());

		PdfPCell contcell = new PdfPCell(table);
		contcell.setBorder(0);

		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(contcell);
		parenttable.addCell(blcell);
		parenttable.setWidthPercentage(100);

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	public void createInWordsDetail() {

		PdfPTable rstable = new PdfPTable(2);

		try {
			rstable.setWidths(colWidth3);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		rstable.setWidthPercentage(100f);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		Phrase rs = new Phrase("CONTRACT CHARGES : ", font9bold);
		PdfPCell rscell = new PdfPCell(rs);
		rscell.setBorder(0);
		rscell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String amountInWord = ServiceInvoicePdf.convert(con.getNetpayable());
		Phrase bl1 = new Phrase("" + Math.round(con.getNetpayable()) + "/-    "
				+ "( " + amountInWord + " Only )", font9bold);
		PdfPCell bl1cell = new PdfPCell(bl1);
		bl1cell.setBorderWidthLeft(0);
		bl1cell.setBorderWidthRight(0);
		bl1cell.setBorderWidthTop(0);
		bl1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		rstable.addCell(rscell);
		rstable.addCell(bl1cell);

		PdfPCell cell = new PdfPCell(rstable);
		cell.setBorder(0);

		PdfPTable amttable = new PdfPTable(1);
		amttable.addCell(cell);
		amttable.addCell(blcell);
		amttable.addCell(blcell);
		amttable.setWidthPercentage(100);

		try {
			document.add(amttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createRemarkHeading() {

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		Phrase remark = new Phrase("REMARK ( IF ANY ) :", font9bold);
		PdfPCell remcell = new PdfPCell(remark);
		remcell.setBorder(0);
		remcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase bl1 = new Phrase(" ");
		PdfPCell bl1cell = new PdfPCell(bl1);
		bl1cell.setBorderWidthLeft(0);
		bl1cell.setBorderWidthTop(0);
		bl1cell.setBorderWidthRight(0);

		PdfPTable remtable = new PdfPTable(2);
		remtable.addCell(remcell);
		remtable.addCell(bl1cell);

		try {
			remtable.setWidths(new float[] { 20, 80 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		remtable.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(remtable);
		cell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.addCell(cell);
		table.addCell(blcell);
		table.addCell(blcell);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createLine() {
		System.out.println("INSIDE CREATE LINE METHOD::::::::::");

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		String str = "";
		str = "DECLARATION  :  ";

		Phrase strp = new Phrase(str, font9boldul);
		PdfPCell strpcell = new PdfPCell(strp);
		strpcell.setBorder(0);
		strpcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String str1 = "";
		str1 = "GENESIS TELESERVICES WILL RENDER MAINTENANCE SERVICSE AS PER THE TERMS"
				+ " MENTIONED OVERLEAF.";

		Phrase str1p = new Phrase(str1, font9bold);
		PdfPCell str1pcell = new PdfPCell(str1p);
		str1pcell.setBorder(0);
		str1pcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable declarationtable = new PdfPTable(2);

		// try {
		// declarationtable.setWidths(colWidth4);
		// } catch (DocumentException e1) {
		// e1.printStackTrace();
		// }

		try {
			declarationtable.setWidths(new float[] { 15, 85 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		declarationtable.setWidthPercentage(100f);
		declarationtable.addCell(strpcell);
		declarationtable.addCell(str1pcell);

		declarationtable.setSpacingAfter(50f);

		// PdfPCell cell = new PdfPCell(declarationtable);
		// cell.setBorder(0);
		//
		// PdfPTable parenttable = new PdfPTable(1);
		// parenttable.setWidthPercentage(100);
		//
		// parenttable.addCell(cell);
		// parenttable.addCell(blcell);
		// parenttable.addCell(blcell);
		// parenttable.addCell(blcell);

		try {
			document.add(declarationtable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createBottom() {

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		Phrase bl1 = new Phrase(" ");
		PdfPCell bl1cell = new PdfPCell(bl1);
		bl1cell.setBorderWidthLeft(0);
		bl1cell.setBorderWidthRight(0);
		bl1cell.setBorderWidthTop(0);
		bl1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase company = null;
		if (comp.getBusinessUnitName() != null) {
			company = new Phrase("" + "FOR "
					+ comp.getBusinessUnitName().toUpperCase(), font9bold);
		}
		PdfPCell compcell = new PdfPCell(company);
		compcell.setBorder(0);
		compcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String str = "";
		str = "  CUSTOMER SIGN & STAMP";

		Phrase strp = new Phrase(str, font9bold);
		PdfPCell strpcell = new PdfPCell(strp);
		strpcell.setBorder(0);
		strpcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable bottomtable = new PdfPTable(3);
		bottomtable.setWidthPercentage(100f);

		bottomtable.addCell(bl1cell);
		bottomtable.addCell(blcell);
		bottomtable.addCell(bl1cell);

		bottomtable.addCell(compcell);
		bottomtable.addCell(blcell);
		bottomtable.addCell(strpcell);

		// bottomtable.addCell(image);
		// bottomtable.addCell(image);
		// bottomtable.addCell(image);

		PdfPCell cell = new PdfPCell(bottomtable);
		cell.setBorder(0);

		PdfPTable parenttable = new PdfPTable(1);
		parenttable.setWidthPercentage(100);
		parenttable.addCell(cell);
		parenttable.addCell(blcell);

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createDates() {

		Phrase bl1 = new Phrase(" ");
		PdfPCell bl1cell = new PdfPCell(bl1);
		bl1cell.setBorderWidthLeft(0);
		bl1cell.setBorderWidthRight(0);
		bl1cell.setBorderWidthTop(0);
		bl1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		Phrase date = new Phrase("DATE  :", font9bold);
		PdfPCell datecell = new PdfPCell(date);
		datecell.setBorder(0);
		datecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// ***********left table**************
		PdfPTable lefttable = new PdfPTable(2);

		try {
			lefttable.setWidths(colWidth5);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		lefttable.setWidthPercentage(100f);

		lefttable.addCell(datecell);
		lefttable.addCell(bl1cell);

		PdfPCell leftcell = new PdfPCell(lefttable);
		leftcell.setBorder(0);

		// ***********center table**************
		PdfPTable centertable = new PdfPTable(2);

		try {
			centertable.setWidths(colWidth5);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		centertable.setWidthPercentage(100f);

		centertable.addCell(blcell);
		centertable.addCell(blcell);

		PdfPCell centercell = new PdfPCell(centertable);
		centercell.setBorder(0);

		// ***********right table**************
		PdfPTable righttable = new PdfPTable(2);

		try {
			righttable.setWidths(colWidth5);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		righttable.setWidthPercentage(100f);

		righttable.addCell(datecell);
		righttable.addCell(bl1cell);

		PdfPCell rightcell = new PdfPCell(righttable);
		rightcell.setBorder(0);

		PdfPTable table = new PdfPTable(3);

		try {
			table.setWidths(columnWidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100f);

		table.addCell(leftcell);
		table.addCell(centercell);
		table.addCell(rightcell);

		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);

		PdfPTable parenttable = new PdfPTable(1);
		parenttable.setWidthPercentage(100);
		parenttable.addCell(cell);
		parenttable.addCell(blcell);

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}
