package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import org.apache.poi.ss.formula.functions.Vlookup;

import com.google.appengine.api.images.Image.Format;
import com.google.gwt.thirdparty.javascript.rhino.head.Ref;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

public class BuildingQuotationPdf {


	public Document document;
	
	Company comp;
	Customer cust;
	Quotation bui;
	Phrase blank;
	String title;
	Phrase chunk;
	PdfPCell pdfno, pdftreatment, pdfpest, pdffrequency, pdfpremise, pdfprice, pdfdiscount;
	PdfPCell pdfsubttl, pdftaxpercent, pdfttlamt;

	float[] colwidth = { 1.0f, 0.1f, 7.0f, 1.5f, 0.1f, 1.5f };
	float[] col1width = { 1.0f, 0.1f, 1.0f, 4.0f, 1.0f, 0.1f, 1.0f };
	// float[] tblcol1width = { 1.5f, 1.5f, 7.0f };
	float[] tblcowidth = { 1.5f, 0.2f, 7.f };
	float[] tblcolwidth = { 0.7f, 1.0f, 1.0f, 2.0f, 3.0f };

	private Font font16boldul, font12bold, font8bold, font8, font9bold,
			font12boldul, font12, font10bold, font10, font14bold, font9,
			font14boldul;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");

	DecimalFormat df = new DecimalFormat("0.00");

	private PdfPCell custlandcell;
	
	public BuildingQuotationPdf()

	{
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD
				| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	}


	public void setbuildingquotation(Long count) {
		
		System.out.println("in side setbuildingquotation method");
		
		bui = ofy().load().type(Quotation.class).id(count).now();

		if (bui.getCompanyId() != null)
			cust = ofy().load().type(Customer.class)
					.filter("count", bui.getCustomerId())
					.filter("companyId", bui.getCompanyId()).first().now();

		if (bui.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", bui.getCustomerId()).first().now();

		if (bui.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", bui.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	}
	
	
	public void createPdf(String status) {
		
		System.out.println("status value "+status);
		if(status.equals("yes"))
		{
			createBlankHeading();
			createLogo(document,comp);
		}
		else if(status.equals("no"))
		{
			createBlankHeading();
		    	if(comp.getUploadHeader()!=null){
				createCompanyNameAsHeader(document,comp);
				}
				
				if(comp.getUploadFooter()!=null){
				createCompanyNameAsFooter(document,comp);
				}
		}
		
		
		createBlankHeading();	
		createTitleHeading();
		createReferenceHeading();
		createCustomerDetailsHeading();
		createQuotationTable();
		createOPCSHeading();
		createNoteHeading();
		
	}


    private void createLogo(Document doc, Company comp) {
			//********************logo for server ********************
				DocumentUpload document =comp.getLogo();
				//patch
				String hostUrl; 
				String environment = System.getProperty("com.google.appengine.runtime.environment");
				if (environment.equals("Production")) {
				    String applicationId = System.getProperty("com.google.appengine.application.id");
				    String version = System.getProperty("com.google.appengine.application.version");
				    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
				} else {
				    hostUrl = "http://localhost:8888";
			}
				
				try {
					Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
					image2.scalePercent(20f);
					image2.setAbsolutePosition(40f,765f);	
					doc.add(image2);
				} catch (Exception e) {
					e.printStackTrace();
			}			
	}	
	
private void createCompanyNameAsHeader(Document doc, Company comp) {
		
		DocumentUpload document =comp.getUploadHeader();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,725f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		try
//		{
//			Image image1=Image.getInstance("images/OM Pest Letter Head.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,725f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}

	private void createCompanyNameAsFooter(Document doc, Company comp) {
		
		
		DocumentUpload document =comp.getUploadFooter();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,40f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		try
//		{
//		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,40f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}
	
	
	

	public void createBlankHeading() {
		
		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);

		try {
			document.add(blank);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);

		PdfPTable blanktable = new PdfPTable(1);
		blanktable.addCell(blankcell);
		blanktable.addCell(blankcell);
		blanktable.setWidthPercentage(100f);

//		PdfPCell cell = new PdfPCell();
//		cell.addElement(blanktable);
//		// cell1.setBorder(0);
//		cell.setBorderWidthTop(0);
//		cell.setBorderWidthLeft(0);
//		cell.setBorderWidthRight(0);
//
//		PdfPTable table = new PdfPTable(1);
//		table.addCell(cell);
//		table.setWidthPercentage(100);

		try {
			document.add(blanktable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void createTitleHeading(){
		
		String title = "Quotation";
		Phrase quotation = new Phrase(title, font16boldul);
		Paragraph qpara = new Paragraph(quotation);
		qpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell qcell = new PdfPCell();
		qcell.addElement(qpara);
		qcell.setBorder(0);
		qcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase blank = new Phrase(" ");
		PdfPCell blkcell = new PdfPCell(blank);
		blkcell.setBorder(0);
		blkcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		PdfPTable qtable = new PdfPTable(1);
		qtable.addCell(qcell);
		qtable.addCell(blkcell);
		qtable.setWidthPercentage(100f);

		PdfPCell cell1 = new PdfPCell();
		cell1.addElement(qtable);
		cell1.setBorder(0);

		PdfPTable table1 = new PdfPTable(1);
		table1.addCell(cell1);
		table1.setWidthPercentage(100);

		try {
			document.add(table1);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public void createReferenceHeading(){
		
		Phrase ref = new Phrase("Ref. no.", font8);
		PdfPCell refcell = new PdfPCell(ref);
		refcell.setBorder(0);
		refcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase r = null;

		if (bui.getReferenceNumber() != null) {
			r = new Phrase("" + bui.getReferenceNumber(), font8);
		}

		PdfPCell rcell = new PdfPCell(r);
		rcell.setBorder(0);
		rcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase colon = new Phrase(":", font8);
		PdfPCell colcell = new PdfPCell(colon);
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase date = new Phrase("Date", font8);
		PdfPCell datecell = new PdfPCell(date);
		datecell.setBorder(0);
		datecell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase dt = null;
		if (bui.getReferenceDate() != null) {
			dt = new Phrase(fmt.format(bui.getQuotationDate()), font8);
		}

		PdfPCell dtcell = new PdfPCell(dt);
		dtcell.setBorder(0);
		dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		
		PdfPTable rdttable = new PdfPTable(6);
		rdttable.addCell(refcell);
		rdttable.addCell(colcell);
		rdttable.addCell(rcell);
		rdttable.addCell(datecell);
		rdttable.addCell(colcell);
		rdttable.addCell(dtcell);
		rdttable.setWidthPercentage(100f);
		
		try {
			rdttable.setWidths(colwidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			document.add(rdttable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void createCustomerDetailsHeading(){
		
		Phrase to = new Phrase("To", font8);
		PdfPCell toCell = new PdfPCell(to);
		toCell.setBorder(0);
		toCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String custAdd1 = "";
		String custFullAdd1 = "";

		Phrase designation = null;
		PdfPCell designationCell = null;
		if(cust.getCustCorresponence()!=null)
		{
			designation = new Phrase(cust.getCustCorresponence(),font8);
			
			designationCell = new PdfPCell(designation);
			designationCell.setBorder(0);
		}
//		else
//		{
//			designation = new Phrase(" " ,font8);
//			
//			designationCell = new PdfPCell(designation);
//			designationCell.setBorder(0);
//		}
		
		Phrase custName = null;
		
		if(cust.isCompany()==true)
		{
			custName = new Phrase(cust.getCompanyName(),font8);
		}
		else
		{
			custName = new Phrase(cust.getFullname(),font8);
		}
		PdfPCell custNameCell =  new PdfPCell(custName);
		custNameCell.setBorder(0);
		
		
		
		if (cust.getAdress() != null) {

			if (!cust.getAdress().getAddrLine2().equals("") ) {
				if (!cust.getAdress().getLandmark().equals("")) {
					custAdd1 = cust.getAdress().getAddrLine1() + "\n"
							+ cust.getAdress().getAddrLine2() + "\n"
							+ cust.getAdress().getLandmark();
				} else {
					custAdd1 = cust.getAdress().getAddrLine1() + "\n"
							+ cust.getAdress().getAddrLine2();
				}
			} else {
				if (!cust.getAdress().getLandmark().equals("")) {
					custAdd1 = cust.getAdress().getAddrLine1() + "\n"
							+ cust.getAdress().getLandmark();
				} else {
					custAdd1 = cust.getAdress().getAddrLine1();
				}
			}

			if(!cust.getSecondaryAdress().getLocality().equals("")){
				custFullAdd1=custAdd1+","+cust.getSecondaryAdress().getLocality()+"\n"+cust.getSecondaryAdress().getCity()+"-"+cust.getSecondaryAdress().getPin()+","+cust.getSecondaryAdress().getState()+","+cust.getSecondaryAdress().getCountry();
						
			}else{
				custFullAdd1=custAdd1+"\n"+cust.getSecondaryAdress().getCity()+"-"+cust.getSecondaryAdress().getPin()+","+cust.getSecondaryAdress().getState()+","+cust.getSecondaryAdress().getCountry();
			}
		}
/**********************************************************************************/
		
//		Phrase mobno = null;
//		if (cust.getCellNumber1() != null) {
//			mobno = new Phrase("Tel No. : " + cust.getCellNumber1(), font10);
//		}
//
//		PdfPCell mobcell = new PdfPCell(mobno);
//		mobcell.setBorder(0);
//		mobcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase mobno = null;
		if (cust.getLandline() != null && cust.getLandline()!=0) {
			mobno = new Phrase("Tel No. : " + cust.getCellNumber1()+" ,022-"+cust.getLandline(), font8);
		}
		else
		{
			mobno = new Phrase("Tel No. : " + cust.getCellNumber1(),font8);
		}
		
		PdfPCell mobcell = new PdfPCell(mobno);
		mobcell.setBorder(0);
		mobcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase pname = null;
		PdfPCell pnamecell=null;
		if(cust.isCompany()==true){
			if(!bui.getDesignation().equals(""))
			{
				pname = new Phrase("CTC: " + cust.getFullname()+" - "+bui.getDesignation(), font8);
			}
			else
			{
				pname = new Phrase("CTC: " + cust.getFullname(), font8);
			}
			pnamecell = new PdfPCell(pname);
			pnamecell.setBorder(0);
			pnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		}
		
		
		
		PdfPCell cellNo2=null;
		if (cust.getCellNumber2() != null && cust.getCellNumber2() != 0)
		{
			Phrase phNo2 = new Phrase("Cell No. : "+cust.getCellNumber2(),font8); 
			cellNo2 = new PdfPCell(phNo2);
			cellNo2.setBorder(0);
		}
		
		
		/*************************************************************************************/
		Phrase sub=null;
		if(bui.getPremisesDesc()!=null){
			 sub = new Phrase("Sub: Quotation for Pest Management Services "+bui.getPremisesDesc()+".",font8);
		}
		else
		{
			 sub = new Phrase("Sub: Quotation for Pest Management Services.",font8);
		}
		PdfPCell subcell = new PdfPCell(sub);
		subcell.setBorder(0);
		subcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase custAddInfo = new Phrase(custFullAdd1, font8);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfoCell.setBorder(0);

		Phrase blank = new Phrase(" ", font8);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		blankCell.setBorder(0);

		Phrase dear = new Phrase("Dear Sir / Madam" + "," + "\n", font8);
		PdfPCell dearcell = new PdfPCell(dear);
		dearcell.setBorder(0);
		dearcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		Phrase msg=null;
		if(bui.getPremisesDesc()!=null){
			msg = new Phrase("With reference to your enquiry, we would like to quote our best rates for the pest management services "+bui.getPremisesDesc()+"."+"\n"+ "Following are our recommended services along with their respective charges. ",font8);
		}
		else
		{
			 msg = new Phrase("With reference to your enquiry, we would like to quote our best rates for the pest management services."+"\n"+ "Following are our recommended services along with their respective charges. ",font8);
		}
		PdfPCell msgcell = new PdfPCell(msg);
		msgcell.setBorder(0);
		msgcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase gd = new Phrase("General Management(G.D.): ",font8bold);
        PdfPCell gdcell = new PdfPCell(gd);
		gdcell.setBorder(0);
		gdcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		PdfPTable cdtable = new PdfPTable(1);
		cdtable.addCell(toCell);
		if(cust.getCustCorresponence()!=null)
		{
		cdtable.addCell(designationCell);
		}
		cdtable.addCell(custNameCell);
		cdtable.addCell(custAddInfoCell);
		if(cust.isCompany()==true){
		cdtable.addCell(pnamecell);
		}
		cdtable.addCell(mobcell);
		if (cust.getCellNumber2() != null && cust.getCellNumber2() != 0)
		{
			cdtable.addCell(cellNo2);
		}
		cdtable.addCell(blankCell);
		cdtable.addCell(subcell);
		cdtable.addCell(blankCell);
		cdtable.addCell(dearcell);
		cdtable.addCell(msgcell);
		cdtable.addCell(gdcell);
		
		cdtable.setWidthPercentage(100f);

		try {
			cdtable.setWidths(colwidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
//		PdfPCell cell3 = new PdfPCell(cdtable);
//		cell3.setBorder(0);
//
//		PdfPTable table3 = new PdfPTable(1);
//		table3.addCell(cell3);
//		table3.setWidthPercentage(100);

		try {
			document.add(cdtable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void createOPCSHeading() {
		
		Phrase blank = new Phrase(" ", font8);
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		blankcell.setBorder(0);
		
		Phrase opcs = new Phrase("OPCS Assurance: " , font8bold);
		PdfPCell opcscell = new PdfPCell(opcs);
		opcscell.setBorder(0);
		opcscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase colon = new Phrase(":",font8);
		PdfPCell colcell = new PdfPCell(colon); 
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase line = new Phrase(
				"In case of pest resurgence during the contarct period, interim calls, if any, would be attended to, without any extra cost. ",
				font8);
		PdfPCell linecell = new PdfPCell(line);
		linecell.setBorder(0);
		linecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable otable = new PdfPTable(3);
		otable.addCell(opcscell);
		otable.addCell(colcell);
		otable.addCell(linecell);
		otable.setWidthPercentage(100f);
//		PdfPCell ocell = new PdfPCell(otable);
//		ocell.setBorder(0);
//		
//		PdfPTable table4 = new PdfPTable(1);
//		table4.addCell(ocell);
//		table4.setWidthPercentage(100);
		
		try {
			otable.setWidths(tblcowidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		try {
			document.add(otable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void createNoteHeading(){
		
		PdfPCell descCell=null;
		PdfPCell bnoteCell=null;
		if(bui.getDescription()!=null){
			
			Phrase bnote = new  Phrase("Notes : ", font8bold);
			bnoteCell = new PdfPCell(bnote);
			bnoteCell.setBorder(0);
			
			Phrase description =new Phrase(bui.getDescription(),font8);
			descCell = new PdfPCell(description);
			descCell.setBorder(0);
		}
		
//		Phrase notes = new Phrase("\n"+"Notes" + "-", font10bold);
//		PdfPCell notecell = new PdfPCell(notes);
//		notecell.setBorder(0);
//		notecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		
//		Phrase abc = new Phrase(
//				"- Further reduction in cost will not be economic for us."
//						+ "\n"
//						+ "- Advance Gel technology or Odorless Spray treatment for Cockroaches, Ants(Red & Blank), Silverfish, Spiders."
//						+ "\n" 
//						+ "- Full Payment in Advance."
//						+ "\n"
//						+ "- Our treatment are Eco Friendly, Nontoxic & Odourless."
//						+ "\n"
//						+ "- No need to keep your premise close or vacant before or after service."
//						+ "\n"
//						+ "- Service details, SOP's, MSDS and Service schedule will be submitted after signing the contract."
//						+ "\n"
//						+ "- We are members of Pest Managemnet Association."
//						+ "\n"+ "\n"
//						, font10);
//		
//		PdfPCell abcCell = new PdfPCell(abc);
//		abcCell.setBorder(0);
//		abcCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase abc1 = new Phrase(
				"This treatment makes human life easy & regular from pest problem. We assure you best pest management services with advanced technologies. Your positive response will be appreciated."
						+ "\n" + "\n" + "\n"+ "Thanks & Regards, ", font8);
		PdfPCell abc1cell = new PdfPCell(abc1);
		abc1cell.setBorder(0);
		abc1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase abc2 = new Phrase("For Om Pest Control Services " + "\n" + "\n"+ "\n"
				+ "Authorised Signatory. ", font8bold);
		PdfPCell abc2cell = new PdfPCell(abc2);
		abc2cell.setBorder(0);
		abc2cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable notetable = new PdfPTable(1);
//		notetable.addCell(notecell);
//		notetable.addCell(abcCell);
		
		if(bui.getDescription()!=null){
			notetable.addCell(bnoteCell);
			notetable.addCell(descCell);
		}
		notetable.addCell(abc1cell);
		notetable.addCell(abc2cell);
		notetable.setWidthPercentage(100f);
		
		PdfPCell cell5 = new PdfPCell(notetable);
		cell5.setBorder(0);
		
		PdfPTable table5 = new PdfPTable(1);
		table5.addCell(cell5);
		table5.setWidthPercentage(100);
		
		try {
			document.add(table5);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
/**************************************************************************************/

	public void createQuotationTable(){
		
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		PdfPTable table = new PdfPTable(6);
		table.setWidthPercentage(100);
		
		Phrase no = new Phrase("No.", font1);
		Phrase pest = new Phrase("Pest Covered", font1);
		Phrase frequency = new Phrase("Frequency(Services)", font1);
		Phrase premise = new Phrase("Area covered", font1);
		Phrase price = new Phrase("Our AMC Charges(Rs).", font1);
		Phrase discount = new Phrase("Discounted AMC Charges.", font1);
		
		PdfPCell nocell = new PdfPCell(no);
		nocell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell pestcell = new PdfPCell(pest);
		pestcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell freqcell = new PdfPCell(frequency);
		freqcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		 PdfPCell precell = new PdfPCell(premise);
		 precell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell pricecell = new PdfPCell(price);
		pricecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell discountcell = new PdfPCell(discount);
		discountcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
		table.addCell(nocell);
		table.addCell(pestcell);
		table.addCell(freqcell);
		table.addCell(precell);
		table.addCell(pricecell);
		table.addCell(discountcell);
		
		
		for (int i = 0; i < this.bui.getItems().size(); i++){
			
			chunk = new Phrase(i + 1 + "", font8);
			pdfno = new PdfPCell(chunk);
			pdfno.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			chunk = new Phrase(bui.getItems().get(i).getComment() + "", font8);
			System.out.println("chunk 2 :" + bui.getItems().get(i).getComment());
			pdfpest = new PdfPCell(chunk);
			pdfpest.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(
					bui.getItems().get(i).getNumberOfServices() + "", font8);
			System.out.println("chunk 3 :"
					+ bui.getItems().get(i).getNumberOfServices());
			pdffrequency = new PdfPCell(chunk);
			pdffrequency.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			if(bui.getItems().get(i).getPremisesDetails()!=null){
				chunk = new Phrase(bui.getItems().get(i).getPremisesDetails(), font8);
				}
			else
			{
				chunk = new Phrase(" ", font8);
			}
				pdfpremise = new PdfPCell(chunk);
				pdfpremise.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			chunk = new Phrase(bui.getItems().get(i).getPrice() + "", font8);
			System.out.println("chunk 4 :" + bui.getNetpayable());
			pdfprice = new PdfPCell(chunk);
			pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			double discAmt=0;
			discAmt=discAmt+bui.getItems().get(i).getPrice()*(bui.getItems().get(i).getPercentageDiscount()/100);
			double disc=discAmt ;
			double total = disc *bui.getItems().get(i).getQty();
			chunk = new Phrase(disc+" x "+bui.getItems().get(i).getQty()+" = "+total,font8);
			pdfdiscount = new PdfPCell(chunk);	
			pdfdiscount.setHorizontalAlignment(Element.ALIGN_CENTER);
			
					
			table.addCell(pdfno);
			table.addCell(pdfpest);
			table.addCell(pdffrequency);
			table.addCell(pdfpremise);
			table.addCell(pdfprice);
			table.addCell(pdfdiscount);
			
		}
		
		Phrase blank=new Phrase();
		blank.add(" ");
		
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blank);
		blankCell.setBorderWidthLeft(0);
		blankCell.setBorderWidthRight(0);
		blankCell.setBorderWidthTop(0);

		
		PdfPCell blank12Cell = new PdfPCell();
		blank12Cell.addElement(blank);
		blank12Cell.setBorderWidthRight(0);

		
		Phrase tamt = new Phrase("Total Amount", font8bold);
		PdfPCell tamtcell = new PdfPCell(tamt);
		tamtcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		chunk = new Phrase(df.format(bui.getTotalAmount()), font8bold);
		pdfttlamt = new PdfPCell(chunk);
		pdfttlamt.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		table.addCell(blank12Cell);
		table.addCell(blankCell);
		table.addCell(blankCell);
		table.addCell(blankCell);
		table.addCell(tamtcell);
		table.addCell(pdfttlamt);
	
		PdfPCell itemcell = new PdfPCell();
		itemcell.addElement(table);
		itemcell.setBorder(0);

		PdfPTable parent = new PdfPTable(1);
		parent.addCell(itemcell);
//		parent.addCell(inwordscell);
		parent.setWidthPercentage(100);
	

		try {
			document.add(parent);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
}
