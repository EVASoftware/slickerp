package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.search.query.QueryParser.phrase_return;
import com.itextpdf.text.Document;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTable;
//import com.slicktechnologies.server.SummaryReport;
import com.slicktechnologies.shared.Contract;
//import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlipPrintHelper;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.workorder.WorkOrder;

import java.text.ParseException;

import org.apache.poi.ss.formula.functions.Now;
public class MaterialReportPdf {

	public Document document;
	
	Company comp;
	Customer cust;
	Service service;
	Phrase chunk;
	PdfPCell pdfsno,pdfteamName,pdfdate,pdfname,pdfqty,pdfunit,pdfrec,pdfretQ,pdfcomment;
//	pdfid
	
	List<Service> serviceList;
	
	List<MaterialIssueNote>minList;
	
	ArrayList<MaterialProduct> materialList;
	
	ArrayList<SummaryReport> productList;
	
	Logger logger=Logger.getLogger("My logger");
	
	 HashMap<String, ArrayList<SummaryReport>> hmap=new HashMap<String, ArrayList<SummaryReport>>();
	 
	 HashMap<String,ArrayList<Service>> datemap=new HashMap<String,ArrayList<Service>>();
	
	float[] columnWidth = {0.7f,1.3f,5.0f,1.0f,0.9f,1.1f,1.1f,2.0f};
	float[] colWidth = {(float) 33.33, (float) 33.33, (float) 33.33};
	float[] col1Width = {0.1f,0.1f,0.2f};
	
	float[] col1WidthforBox = {2.0f,0.8f,2.0f};
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	
	private Font font16boldul, font12bold, font8bold, font8, font9bold,
	font12boldul, font12, font10bold, font10, font14bold, font9, font9boldul,
	font14boldul, font16bold;
	
	
public MaterialReportPdf(){
		
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD
				| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font9boldul = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD
				| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		
	}


public void setpdfmr2(Long companyId, HashMap<String, ArrayList<SummaryReport>> hmap) {
	comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
	
	this.hmap=hmap;
	
	fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	
}

/**************************************************************************/
//	public void setpdfmr1(Long companyId, ArrayList<ProductGroupList> list) {
//		comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
//		
//		productList=list;
//		
//	}

//	public void setpdfmr(Long companyId, String fromdate, String todate, String team) {
//		Date frmDate = null, toDate = null;
//		if (fromdate != null) {
//			SimpleDateFormat fmt123 = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
//			
//			try {
//				frmDate = fmt123.parse(fromdate);
//				toDate = fmt123.parse(todate);
//			} catch (ParseException e1) {
//				e1.printStackTrace();
//			}
//		}
//		
//		System.out.println("  "+companyId);
//		System.out.println("  "+frmDate);
//		System.out.println("  "+toDate);
//		System.out.println("  "+team);
//		
//		
//		if((fromdate!=null&&todate!=null)&&team!=null){
//			
//			System.out.println(" 1 ");
//			
//			serviceList = ofy().load().type(Service.class).filter("companyId",companyId).filter("serviceDate >=", frmDate).filter("serviceDate <=", toDate).filter("team", team).list();	
//		}
//		else if((fromdate!=null&&todate ==null) && team !=null){
//			System.out.println(" 2 ");
//			
//			serviceList = ofy().load().type(Service.class).filter("companyId",companyId).filter("serviceDate", frmDate).filter("team", team).list();	
//		}
//		else if((todate !=null && fromdate !=null)&& team ==null){
//			System.out.println(" 2 1");
//			
//			serviceList = ofy().load().type(Service.class).filter("companyId",companyId).filter("serviceDate >=", frmDate).filter("serviceDate <=", toDate).list();	
//		}
//		System.out.println("serviceList "+ serviceList.size());
//		
//		
//		minList=new ArrayList<MaterialIssueNote>();
//		
//		if(serviceList.size() !=0){
//			
//			System.out.println(" 3 ");
//			
//			for(int i=0; i<serviceList.size(); i++){
//			
//			MaterialIssueNote min = ofy().load().type(MaterialIssueNote.class).filter("companyId", companyId).filter("serviceId", serviceList.get(i).getCount()).first().now();
//			
//			if(min !=null){
//				
//			minList.add(min);
//			
//			}
//			
//		}
//	
//		System.out.println("minList "+ minList.size());
//		
//		}
//		
//		materialList=new ArrayList<MaterialProduct>();
//		
//		if(minList.size()!=0){
//			
//			for(int i=0;i<minList.size();i++){
//				
//				for(int j=0;j<minList.get(i).getSubProductTablemin().size();j++){
//					
//					if(materialList.size()==0){
//						
//						MaterialProduct matProd = minList.get(i).getSubProductTablemin().get(j);
//						materialList.add(matProd);
//						
//					}
//					else{
//						boolean flag=false;
//						for(int k=0;k<materialList.size();k++){
//							if(materialList.get(k).getMaterialProductId()==minList.get(i).getSubProductTablemin().get(j).getMaterialProductId()){
//								
//								MaterialProduct matProd=minList.get(i).getSubProductTablemin().get(j);
//								double qty=materialList.get(k).getMaterialProductRequiredQuantity();
//								qty=qty+matProd.getMaterialProductRequiredQuantity();
//								materialList.get(k).setMaterialProductRequiredQuantity(qty);
//								flag=true;
//							}
//						}
//						
//						if(flag==false){
//							MaterialProduct matProd=minList.get(i).getSubProductTablemin().get(j);
//							materialList.add(matProd);
//						}
//					}
//				}
//			}
//		}
//		
//		System.out.println("Material List Size :: "+materialList.size());
//		
//		
//		
//		comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
//		
//		cust = ofy().load().type(Customer.class).filter("companyId", companyId).first().now();	
//		
//	}
	/**************************************************************************/
	
	
	public void createPdf() {
		
//		createLogo(document,comp);
		createBlankHeading();
		createCompanyAddress();
//		createCompanyHeading();
		createDateandTimeTable();
		createMinTable();
		
	}
	
//	private void createLogo(Document doc, Company comp) {
//		DocumentUpload document =comp.getLogo();
//
//		//patch
//		String hostUrl;
//		String environment = System.getProperty("com.google.appengine.runtime.environment");
//		if (environment.equals("Production")) {
//		    String applicationId = System.getProperty("com.google.appengine.application.id");
//		    String version = System.getProperty("com.google.appengine.application.version");
//		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
//		} else {
//		    hostUrl = "http://localhost:8888";
//		}
//		
//		try {
//			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
//			image2.scalePercent(20f);
//			image2.setAbsolutePosition(40f,765f);	
//			doc.add(image2);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
	
	
	public void createBlankHeading() {
		
		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);	
		
		PdfPTable bltable = new PdfPTable(1);
		bltable.addCell(blcell);
		bltable.setWidthPercentage(100);
		
		try {
			document.add(bltable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	
	public void createCompanyAddress(){
		
		 Phrase companyName= new Phrase(comp.getBusinessUnitName(),font16bold);
	     Paragraph p =new Paragraph();
	     p.add(Chunk.NEWLINE);
	     p.add(companyName);
	     p.setAlignment(Element.ALIGN_CENTER);
	     PdfPCell companyNameCell=new PdfPCell();
	     companyNameCell.addElement(p);
	     companyNameCell.setBorder(0);
	     
		String addressline1="";
		
		if(comp.getAddress().getAddrLine2()!=null){
			addressline1=comp.getAddress().getAddrLine1()+" , "+comp.getAddress().getAddrLine2();
		}
		else{
			addressline1=comp.getAddress().getAddrLine1();
		}
		Phrase addressline=new Phrase(addressline1,font12);
		Paragraph addresspara=new Paragraph();
		addresspara.add(addressline);
		addresspara.setAlignment(Element.ALIGN_CENTER);
		
		Phrase locality=null;
		if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition1");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 2");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition 3");
			locality= new Phrase(comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 4");
			locality= new Phrase(comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		Paragraph localityPragraph=new Paragraph();
		localityPragraph.add(locality);
		localityPragraph.setAlignment(Element.ALIGN_CENTER);
		
		Phrase contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+"    Tel: "+comp.getLandline()+"    Email: "+comp.getEmail().trim(),font10);
		Paragraph realmobpara=new Paragraph();
		realmobpara.add(contactinfo);
		realmobpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell addresscell=new PdfPCell();
		addresscell.addElement(addresspara);
		addresscell.setBorder(0);
		
		PdfPCell localitycell=new PdfPCell();
		localitycell.addElement(localityPragraph);
		localitycell.setBorder(0);
		
		PdfPCell contactcell=new PdfPCell();
		contactcell.addElement(realmobpara);
		contactcell.setBorder(0);
		
		PdfPTable companyDetails=new PdfPTable(1);
		companyDetails.setWidthPercentage(100);
		
		companyDetails.addCell(companyNameCell);
		companyDetails.addCell(addresscell);
		companyDetails.addCell(localitycell);
		companyDetails.addCell(contactcell);
		companyDetails.setSpacingAfter(10f);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell comapnyCell=new PdfPCell(companyDetails);
		parent.addCell(comapnyCell);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	

public void createMinTable(){
	
	Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	
		PdfPTable table;
		Set set = hmap.entrySet();
		Iterator it = set.iterator();
		int counter=0;
		
		
	    while(it.hasNext()) {
    	
    	table = new PdfPTable(8);
    	try {
			table.setWidths(columnWidth);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
    	table.setWidthPercentage(100);
    	
    	
    	Map.Entry me = (Map.Entry) it.next();
    	System.out.print(me.getKey() + ": ");
    	@SuppressWarnings("unused")
		String teamName=me.getKey().toString();
    	
    	productList = (ArrayList<SummaryReport>) me.getValue();
    	
    	Phrase teamName1 = new Phrase(teamName,font10bold);
    	
		Phrase serialno = new Phrase("Sr. No.", font1);
//		Phrase teamName1 = new Phrase("Team : "+teamName, font1);
		Phrase date=new Phrase("Date",font1);
		Phrase name = new Phrase("Product Name", font1);
		Phrase quantity = new Phrase("Quantity", font1);
		//Phrase id = new Phrase("Product Id", font1);
		Phrase unit = new Phrase("UOM",font1);
		Phrase rec = new Phrase("Received",font1);
		Phrase ret = new Phrase("Return Quantity",font1);
		Phrase com = new Phrase("Comment",font1);
		
		
		PdfPCell nocell = new PdfPCell(serialno);
		nocell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
//		PdfPCell teamName1cell = new PdfPCell(teamName1);
//		teamName1cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell datecell = new PdfPCell(date);
		datecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell namecell = new PdfPCell(name);
		namecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell qtycell = new PdfPCell(quantity);
		qtycell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
//		PdfPCell idcell = new PdfPCell(id);
//		idcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
		PdfPCell unitcell = new PdfPCell(unit);
		unitcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell reccell = new PdfPCell(rec);
		reccell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell retcell = new PdfPCell(ret);
		retcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell comcell = new PdfPCell(com);
		comcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		table.addCell(nocell);
		//table.addCell(idcell);
//		table.addCell(teamName1cell);
		table.addCell(datecell);
		table.addCell(namecell);
		table.addCell(qtycell);
		table.addCell(unitcell);
		table.addCell(reccell);
		table.addCell(retcell);
		table.addCell(comcell);
		
//		chunk = new Phrase(teamName+"",font8);	
//		pdfteamName = new PdfPCell(chunk);
//		pdfteamName.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		/**************code for a box******************/
		PdfPTable abc =new PdfPTable(1);
		abc.setWidthPercentage(100);
		
		Phrase p = new Phrase(" " ,font10);
		PdfPCell abcCell = new PdfPCell(p); 
		abcCell.setBorder(0);
		abc.addCell(abcCell);
		
		
		PdfPCell qp = new PdfPCell(abc);
		
		PdfPTable ab = new PdfPTable(3);
		
		ab.setWidthPercentage(100);
		try {
			ab.setWidths(col1WidthforBox);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		ab.addCell(abcCell);
		ab.addCell(abcCell);
		ab.addCell(abcCell);
		ab.addCell(abcCell);
		ab.addCell(qp);
		ab.addCell(abcCell);
		ab.addCell(abcCell);
		ab.addCell(abcCell);
		ab.addCell(abcCell);
		
		System.out.println("RRRRRRRRRRR   in side pdf"+this.productList.size());
		for(int i=0; i<this.productList.size(); i++){
			
			chunk = new Phrase(i+1+"",font8);	
			pdfsno = new PdfPCell(chunk);
			pdfsno.setHorizontalAlignment(Element.ALIGN_CENTER);	
			
//			chunk = new Phrase(productList.get(i).getProduct_id()+"",font8);
//			pdfid = new PdfPCell(chunk);
//			pdfid.setHorizontalAlignment(Element.ALIGN_CENTER);
			
//			chunk = new Phrase(fmt.format(productList.get(i).getDate())+"",font8);	
//			pdfteamName = new PdfPCell(chunk);
//			pdfteamName.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			chunk = new Phrase(fmt.format(productList.get(i).getDate())+"",font8);	
			pdfdate = new PdfPCell(chunk);
			pdfdate.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			chunk = new Phrase(productList.get(i).getProdName()+"",font8);	
			pdfname = new PdfPCell(chunk);
			pdfname.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			chunk = new Phrase(productList.get(i).getQuantity()+"",font8);	
			pdfqty = new PdfPCell(chunk);
			pdfqty.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			chunk = new Phrase(productList.get(i).getUnit()+"",font8);	
			pdfunit = new PdfPCell(chunk);
			pdfunit.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			pdfrec = new PdfPCell(ab);
			pdfrec.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			chunk = new Phrase(" ",font8);	
			pdfretQ = new PdfPCell(chunk);
			pdfretQ.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			chunk = new Phrase(" ",font8);	
			pdfcomment = new PdfPCell(chunk);
			pdfcomment.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			table.addCell(pdfsno);
//			table.addCell(pdfteamName);
//			table.addCell(pdfid);
			table.addCell(pdfdate);
			table.addCell(pdfname);
			table.addCell(pdfqty);
			table.addCell(pdfunit);
			table.addCell(pdfrec);
			table.addCell(pdfretQ);
			table.addCell(pdfcomment);
			
		
		}
		
		PdfPCell teamcell = new PdfPCell(teamName1);
		teamcell.setBorder(0);
		
		PdfPCell invcell = new PdfPCell();
		invcell.addElement(table);
		invcell.setBorder(0);

		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(teamcell);
		parenttable.addCell(invcell);
		
		parenttable.setWidthPercentage(100);
		
		if(counter==0){
			try {
				document.add(parenttable);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		
		}else{
			try {
				document.add(Chunk.NEXTPAGE);
				document.add(parenttable);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}
		counter++;
    }
}


public void createDateandTimeTable(){
	
	Paragraph blank = new Paragraph();
	blank.add(Chunk.NEWLINE);
	blank.add(Chunk.NEWLINE);
	PdfPCell blcell = new PdfPCell(blank);
	blcell.setBorder(0);	
	
	Phrase col = new Phrase(":");
	PdfPCell colcell = new PdfPCell(col);	
	colcell.setBorder(0);
	colcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
/************************************************************/
	
	Phrase bl1 = new Phrase(" ");
	PdfPCell bl1cell = new PdfPCell(bl1);
	bl1cell.setBorder(0);
	
	Date todaysdate = new Date();
	SimpleDateFormat sdf=new SimpleDateFormat("dd-MMM-yyyy HH:mm");
	sdf.setTimeZone(TimeZone.getTimeZone("IST"));
	
	String date_time=sdf.format(new Date());
	String[] dateNtime=date_time.split(" ");
	String todayD=dateNtime[0].trim();
	String todayTime=dateNtime[1].trim();
	String todaysTime = todaysdate.getHours()+":"+todaysdate.getMinutes();
	
//	Phrase date = new Phrase("Date",font10bold);
//	PdfPCell datecell = new PdfPCell(date);
//	datecell.setBorder(0);
//	datecell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase dt = null;
	
	if(todaysdate !=null){
		dt = new Phrase("Date : "+fmt.format(todaysdate),font10bold);
	}
	System.out.println("Date::::::::::::::"+fmt.format(todaysdate));
	logger.log(Level.SEVERE,"Date1:::::::::::::::"+fmt.format(todaysdate));
	
	
	PdfPCell dtcell = new PdfPCell(dt);
	dtcell.setBorder(0);
	dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	PdfPTable lefttable = new PdfPTable(1);
	
	lefttable.addCell(dtcell);
	lefttable.setWidthPercentage(100f);

	PdfPCell leftcell = new PdfPCell(lefttable);
	leftcell.setBorder(0);
	leftcell.setHorizontalAlignment(Element.ALIGN_LEFT);
/***********************************************************/	
	
	Phrase head = new Phrase("Material Summary", font12bold);
	PdfPCell headcell = new PdfPCell(head);	
	headcell.setBorder(0);
	headcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	PdfPTable centertable = new PdfPTable(1);
	
	centertable.addCell(headcell);
	centertable.setWidthPercentage(100f);
	
	PdfPCell centercell = new PdfPCell(centertable);
	centercell.setBorder(0);
	centercell.setHorizontalAlignment(Element.ALIGN_CENTER);
	/********************************************/	
	
//	Date todaysdate = new Date();
//	String todaysTime = todaysdate.getHours()+":"+todaysdate.getMinutes();
//	
//	Phrase date = new Phrase("Date",font10bold);
//	PdfPCell datecell = new PdfPCell(date);
//	datecell.setBorder(0);
//	datecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	
//	Phrase dt = null;
//	if(todaysdate !=null){
//		dt = new Phrase(" "+fmt.format(todaysdate),font10bold);
//	}
//	PdfPCell dtcell = new PdfPCell(dt);
//	dtcell.setBorder(0);
//	dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
//	Phrase time = new Phrase("Time",font10bold);
//	PdfPCell timecell = new PdfPCell(time);
//	timecell.setBorder(0);
//	timecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	
	Phrase tm = null;
	if(todayTime !=null){
		tm = new Phrase("Time : "+todayTime,font10bold);
	}
	System.out.println("Time::::::::::::::"+todayTime);
	logger.log(Level.SEVERE,"Time1:::::::::::::::"+todayTime);
	
	PdfPCell tmcell = new PdfPCell(tm);
	tmcell.setBorder(0);
	tmcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	
	PdfPTable righttable = new PdfPTable(1);
	righttable.addCell(tmcell);
	righttable.setWidthPercentage(100f);
	
	PdfPCell rightcell = new PdfPCell(righttable);
	rightcell.setBorder(0);
	rightcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	/*********************************************************/	
	
	PdfPTable parentable = new PdfPTable(3);
	
	parentable.addCell(leftcell);
	parentable.addCell(centercell);
	parentable.addCell(rightcell);
	parentable.setWidthPercentage(100);
	
	PdfPCell cell = new PdfPCell(parentable);
	cell.setBorderWidthTop(0);
	
	PdfPTable table = new PdfPTable(1);
	table.addCell(cell);
	
//	try {
//		table.setWidths(colWidth);
//	} catch (DocumentException e1) {
//		e1.printStackTrace();
//	}
	table.setWidthPercentage(100);
	
	try {
	document.add(table);
	} catch (DocumentException e) {
	e.printStackTrace();
		}	
	}
}
