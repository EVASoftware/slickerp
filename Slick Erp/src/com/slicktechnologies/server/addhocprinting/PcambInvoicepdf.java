package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceCharges;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.workorder.WorkOrder;

public class PcambInvoicepdf 
{
	
	Company comp;
	Contract con;
	Customer cust;
	Service ser;
	Invoice invoiceentity;  
	WorkOrder wo ;
	List<ArticleType> articletype;      
	List<BillingDocumentDetails> billingDoc;  
	List<PaymentTerms> payTerms= new ArrayList<PaymentTerms>();
	List<SalesOrderProductLineItem> salesProd;
	List<ContractCharges> billingTaxesLis;
	List<ContractCharges> billingChargesLis;
	List<PaymentTerms> payTermsLis;
	List<InvoiceCharges> conbillingTaxesLis = new ArrayList<InvoiceCharges>();
	String invComment="";
	BillingDocument billEntity;
	String invoiceOrderType="";     
/**2-11-2017 sagar sore [to access document in email]**/
	 public Document document;  
  
		private Font font16boldul, font12bold, font8bold, font8,  font9bold,  font9, font12,
		font12boldul, font10boldul, font16bold, font10, font10bold,font10boldred,font11bold,
		font14bold,font7,font7bold,font9red,font9boldred,font12boldred,font16,font23,font20;
		
		private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
		
		DecimalFormat df=new DecimalFormat("0.00");
  
public PcambInvoicepdf() 

{
	font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
	font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	font16 = new Font(Font.FontFamily.HELVETICA, 16);
	font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	font8 = new Font(Font.FontFamily.HELVETICA, 8);
	font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
	font12 = new Font(Font.FontFamily.HELVETICA, 12);
	font12boldred = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,BaseColor.RED);
	font10 = new Font(Font.FontFamily.HELVETICA, 10);
	font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
	font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
	font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	font9boldred = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD,BaseColor.RED);
	font9 = new Font(Font.FontFamily.HELVETICA, 9);
	font9red = new Font(Font.FontFamily.HELVETICA, 9,Font.NORMAL, BaseColor.RED);
	font7 = new Font(Font.FontFamily.HELVETICA, 7);
	font7bold = new Font(Font.FontFamily.HELVETICA, 7,Font.BOLD);  
	font23= new Font(Font.FontFamily.HELVETICA, 23);
	font20= new Font(Font.FontFamily.HELVETICA, 20);
	font11bold = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);
	font10boldred = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD,BaseColor.RED);
	
}  

	public void setInvoice(Long count)
	{
		
		//Load Invoice
		invoiceentity=ofy().load().type(Invoice.class).id(count).now(); 
		
		// load Customer
		if(invoiceentity.getCompanyId()==null)
		cust = ofy().load().type(Customer.class).first().now();
		else
	    cust=ofy().load().type(Customer.class).filter("count",invoiceentity.getPersonInfo().getCount()).filter("companyId", invoiceentity.getCompanyId()).first().now();

		//Load Company
		if(invoiceentity.getCompanyId()==null)
		   comp=ofy().load().type(Company.class).first().now();
		else
		   comp=ofy().load().type(Company.class).filter("companyId",invoiceentity.getCompanyId()).first().now();
		//Load Contract
		if(invoiceentity.getCompanyId()!=null)
			con=ofy().load().type(Contract.class).filter("count", invoiceentity.getContractCount()).first().now();
			
		else
			con=ofy().load().type(Contract.class).filter("count",invoiceentity.getContractCount()).filter("companyId",invoiceentity.getCompanyId()).first().now();	
		
		//Load service
		
		System.out.println("companyId:-"+ invoiceentity.getCompanyId());  
		System.out.println("contractCount"+ invoiceentity.getContractCount());
		
				if(invoiceentity.getCompanyId()==null) 
				{
				   ser =ofy().load().type(Service.class).first().now();
				System.out.println("Service Load If "+ ser.getContractCount());
				}
				else{
					ser=ofy().load().type(Service.class).filter("companyId",invoiceentity.getCompanyId()).filter("contractCount", invoiceentity.getContractCount()).first().now();
//					System.out.println("Service Load else "+ ser.getContractCount());
				}
		//Load Article type Details   
				
		     articletype = new ArrayList<ArticleType>();
			if(cust.getArticleTypeDetails().size()!=0){  
				articletype.addAll(cust.getArticleTypeDetails());     
			}
			if(comp.getArticleTypeDetails().size()!=0){  
				articletype.addAll(comp.getArticleTypeDetails());
			}
			// load billing document    
			
			 salesProd=invoiceentity.getSalesOrderProducts();   
//			 System.out.println("AAAAAAAAAA sales prod size"+salesProd.size());
			 billingTaxesLis=invoiceentity.getBillingTaxes();
			 billingChargesLis=invoiceentity.getBillingOtherCharges();
			 payTermsLis=con.getPaymentTermsList();
		
			 if(invoiceentity.getArrayBillingDocument().size()>1)
				{
					for(int i=0;i<invoiceentity.getArrayBillingDocument().size();i++){
						BillingDocument bill =new BillingDocument();
						if(invoiceentity.getCompanyId()!=null){
							bill=ofy().load().type(BillingDocument.class).filter("companyId",invoiceentity.getCompanyId()).filter("contractCount", invoiceentity.getContractCount()).filter("count", invoiceentity.getArrayBillingDocument().get(i).getBillId()).filter("typeOfOrder", invoiceentity.getTypeOfOrder().trim()).first().now();
						}else{
							bill=ofy().load().type(BillingDocument.class).filter("contractCount", invoiceentity.getContractCount()).filter("count", invoiceentity.getArrayBillingDocument().get(i).getBillId()).filter("typeOfOrder", invoiceentity.getTypeOfOrder().trim()).first().now();
						}
						payTerms.addAll(bill.getArrPayTerms());
					}
			 
				}else
				{
					payTerms.addAll(invoiceentity.getArrPayTerms());
				}
			 
			 // Load WorkOrder 
//			 wo
			 if(invoiceentity.getCompanyId()==null)
				 wo = ofy().load().type(WorkOrder.class).first().now();
					else
						wo=ofy().load().type(WorkOrder.class).filter("count",invoiceentity.getPersonInfo().getCount()).filter("companyId", invoiceentity.getCompanyId()).first().now();

	} 
	  
	public void createPdf() {
		
		createHeader();
		createbillInfo();
		createodrinfotbl();
		createparticulars();
		createrswords();
		createfooter();
		
	}
	private void createHeader()
	{
		 
			//Ajinkya code 8\12\2016    font size updated to 10 As per Mail Confirmation Date :23/05/2017
			//1st method start
		
		String addressline1="";
		
		if(comp.getAddress().getAddrLine2()!=null){
			addressline1=comp.getAddress().getAddrLine1()+", "+comp.getAddress().getAddrLine2() +", ";
		}
		else{
			addressline1=comp.getAddress().getAddrLine1()+", ";
		}
	   
		String locality=null;
		
		if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition1");
			locality= (comp.getAddress().getLandmark()+", "+comp.getAddress().getLocality()+", "+comp.getAddress().getCity()+" - "
				      +comp.getAddress().getPin()+". "+comp.getAddress().getState()+", "+comp.getAddress().getCountry()+". ");
		}
		else if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 2");
			locality= (comp.getAddress().getLandmark()+", "+comp.getAddress().getCity()+" - "
				      +comp.getAddress().getPin()+". "+comp.getAddress().getState()+", "+comp.getAddress().getCountry()+". ");
		}
		
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition 3");
			locality= (comp.getAddress().getLocality()+", "+comp.getAddress().getCity()+" - "
				      +comp.getAddress().getPin()+". "+comp.getAddress().getState()+", "+comp.getAddress().getCountry()+". ");
		}
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 4");
			locality=(comp.getAddress().getCity()+" - "
				      +comp.getAddress().getPin()+". "+comp.getAddress().getState()+", "+comp.getAddress().getCountry()+". ");
		}
		
		
			Phrase mycomHeader=new Phrase(comp.getBusinessUnitName().toUpperCase(),font16);
			Phrase header1 = new Phrase ("Regd Office : "+addressline1 + locality ,font10);
			Phrase header2 = new Phrase ("Tel.: (91-22) 2266 1091, 2262 5376 Fax : (91-22) 2266 0810 ",font10);
			Phrase header3 = new Phrase (" E-Mail : "+comp.getEmail().trim()+ " Website : "+comp.getWebsite(),font10);
			Phrase header4 = new Phrase ("  ",font10);
			
			PdfPCell mycomHeaderCell=new PdfPCell(mycomHeader);
			mycomHeaderCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			mycomHeaderCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			mycomHeaderCell.setBorder(0);
			PdfPCell header1Cell=new PdfPCell(header1);
			header1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			header1Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			header1Cell.setBorder(0);
			PdfPCell header2Cell=new PdfPCell(header2);
			header2Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			header2Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			header2Cell.setBorder(0);
			PdfPCell header3Cell=new PdfPCell(header3);
			header3Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			header3Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			header3Cell.setBorder(0);
			PdfPCell header4Cell=new PdfPCell(header4);
		    header4Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		    header4Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		    header4Cell.setBorder(0);
 			
		    PdfPTable parentTbl=new PdfPTable(1);
			parentTbl.setWidthPercentage(100);      
			
			parentTbl.addCell(mycomHeaderCell);
			parentTbl.addCell(header1Cell);
			parentTbl.addCell(header2Cell);
			parentTbl.addCell(header3Cell);
			parentTbl.addCell(header4Cell);
   
			try {
				document.add(parentTbl);  
			} catch (Exception e) {
				e.printStackTrace();
			}
	}  
	 private void createbillInfo() 
	 {  
			PdfPTable ParentTbl = new PdfPTable (2);  
			ParentTbl.setWidthPercentage(100);
			
			PdfPTable billInfoTbl=new PdfPTable(2);
			billInfoTbl.setWidthPercentage(100);
			  
				try {
			billInfoTbl.setWidths(new float[] { 25 , 75 });
			ParentTbl.setWidths(new float[] { 70 , 30 });
		     
			document.add(ParentTbl);
		} catch (Exception e1) {
			e1.printStackTrace();
		}

			Phrase blnkphrse =new Phrase ("",font10);   
			PdfPCell blnkCell = new PdfPCell ();
			blnkCell.addElement(blnkphrse);
			blnkCell.setBorder(0);     
			
			Phrase billName=new Phrase(" Billing Name:",font10bold);
			PdfPCell billNameCell=new PdfPCell(billName);
			billNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			billNameCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			billNameCell.setBorder(0);
			billInfoTbl.addCell(billNameCell);
		
			////////////////////////////////////////////////////////
			
			String customerFullName="";
			
			if(cust.isCompany()){
				customerFullName=cust.getCompanyName();
			}
			else
			{
				if(cust.getSalutation()!= null && ! cust.getSalutation().equals(""))
				{
					customerFullName=cust.getSalutation()+" "+cust.getFullname();
				}
				else
				{
					customerFullName=cust.getFullname();
				}
				
			}   
			//////////////////////////////////////////////////////////
			////////    
			/*
			 * nameInFirstLetterUpperCase defined by Ajinkya  suggested by Rahul V 
			 * Used to take customer name from Uppercase to Only first letter Uppercase(camel)
			 */
			      String nameInFirstLetterUpperCase=getFirstLetterUpperCase(customerFullName.trim());
			      
			///////
			
			Phrase billNameVal=new Phrase( nameInFirstLetterUpperCase ,font10);
			PdfPCell billNameValCell=new PdfPCell(billNameVal);
			billNameValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			billNameValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			billNameValCell.setBorder(0);
			billInfoTbl.addCell(billNameValCell);
                                                              
			Phrase billAddress=new Phrase(" Billing Address:",font10bold);
			PdfPCell billAddressCell=new PdfPCell(billAddress);
			billAddressCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			billAddressCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			billAddressCell.setBorder(0);
			billInfoTbl.addCell(billAddressCell);
			
			Phrase billAddressVal1=new Phrase(cust.getAdress().getAddrLine1()+", ",font10);
			PdfPCell billAddressVal1Cell=new PdfPCell(billAddressVal1);
			billAddressVal1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			billAddressVal1Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			billAddressVal1Cell.setBorderWidth(0);
			billInfoTbl.addCell(billAddressVal1Cell);  
			
		    System.out.println( "addrs 2 is = "+cust.getAdress().getAddrLine2());  
		    System.out.println( "Landmark  is = "+cust.getAdress().getLandmark());
		     System.out.println( "Locality is = "+cust.getAdress().getLocality()); 
		     
                      //////////////////////////////////Billing Address  /////////////////////////////////// 
		     
		     if ((cust.getAdress().getAddrLine2()!= null) && (!cust.getAdress().getAddrLine2().equals("")))
		     { 
		    	 System.out.println("Inside Addrs Not Null");  
		    	 
		    	 Phrase	billAddressVal2=new Phrase(cust.getAdress().getAddrLine2()+", ",font10);
				 PdfPCell billAddressVal2Cell=new PdfPCell(billAddressVal2);
				 billAddressVal2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 billAddressVal2Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				 billAddressVal2Cell.setBorder(0);
				 
				 billInfoTbl.addCell(blnkCell);
				 billInfoTbl.addCell(billAddressVal2Cell);	 
		    	 
		     }
		     
		     
		     
		     if((!cust.getAdress().getLandmark().equals(""))&& cust.getAdress().getLocality().equals("")==false )  //(cust.getAdress().getLandmark()!= null) &&
		     {     
		    	 System.out.println("Inside Loc N Lan Not Null");
		    	 
		    	   Phrase billAddressVal3=new Phrase(cust.getAdress().getLandmark()+", "+ cust.getAdress().getLocality()+", " ,font10);
					PdfPCell billAddressVal3Cell=new PdfPCell(billAddressVal3);
					billAddressVal3Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					billAddressVal3Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					billAddressVal3Cell.setBorder(0);
					
					billInfoTbl.addCell(blnkCell);
					billInfoTbl.addCell(billAddressVal3Cell);
		    	 
		     }
		     
		     else if((cust.getAdress().getLandmark().equals(""))&& cust.getAdress().getLocality().equals("")==false )
		     {
		    	 System.out.println("Inside Landmark Null");
		    	 
		    	   Phrase billAddressVal3=new Phrase(cust.getAdress().getLocality()+", " ,font10);
					PdfPCell billAddressVal3Cell=new PdfPCell(billAddressVal3);
					billAddressVal3Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					billAddressVal3Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					billAddressVal3Cell.setBorder(0);
					
					billInfoTbl.addCell(blnkCell);
					billInfoTbl.addCell(billAddressVal3Cell);
		    	 
		     }
		     
		     else if((!cust.getAdress().getLandmark().equals(""))&& cust.getAdress().getLocality().equals("")==true )
		     { 
		    	 System.out.println("Inside Loc Null");
		    	   Phrase billAddressVal3=new Phrase(cust.getAdress().getLandmark()+", " ,font10);
					PdfPCell billAddressVal3Cell=new PdfPCell(billAddressVal3);
					billAddressVal3Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					billAddressVal3Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					billAddressVal3Cell.setBorder(0);
					
					billInfoTbl.addCell(blnkCell);
					billInfoTbl.addCell(billAddressVal3Cell);
		    	 
		     }
      		     /////////////////////////// billing Address Code End Here ///////////////////////

				Phrase billAddressVal4=new Phrase(cust.getAdress().getCity()+" - "+ cust.getAdress().getPin()+". ",font10);
				PdfPCell billAddressVal4Cell=new PdfPCell(billAddressVal4);
				billAddressVal4Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				billAddressVal4Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				billAddressVal4Cell.setBorder(0);
				
				 billInfoTbl.addCell(blnkCell);
				 billInfoTbl.addCell(billAddressVal4Cell);
				 
					Phrase billAddressVal5=new Phrase(cust.getAdress().getState()+", "+ cust.getAdress().getCountry()+". ",font10);
					
					PdfPCell billAddressVal5Cell=new PdfPCell(billAddressVal5);
					billAddressVal5Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					billAddressVal5Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					billAddressVal5Cell.setBorder(0);
					
					 billInfoTbl.addCell(blnkCell);
					 billInfoTbl.addCell(billAddressVal5Cell);
					 
  			
			
			PdfPCell billInfotblCell = new PdfPCell ();
			billInfotblCell.addElement(billInfoTbl);
			billInfotblCell.setRowspan(8);
			
			ParentTbl.addCell(billInfotblCell);
			//////////// start adding cell ////// 
			  
			Phrase invoicenoPhrs=new Phrase("Invoice No.: " + invoiceentity.getCount(),font10);
			PdfPCell invoicenoCell=new PdfPCell(invoicenoPhrs); 
			invoicenoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			invoicenoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			invoicenoCell.setFixedHeight(15f);
			ParentTbl.addCell(invoicenoCell);  
			
			
			Phrase datePhrs=new Phrase("Date: "+fmt.format(con.getContractDate()),font10);
			PdfPCell dateCell=new PdfPCell(datePhrs);
			dateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			dateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			dateCell.setFixedHeight(15f);
			ParentTbl.addCell(dateCell);  
			  
			 //////////////// CIN,ESIC,ST/GST,PAN NO Label is not Changed as per telephonic & mail conversation    
			Phrase cinPhrs=new Phrase("CIN: ",font10);
			 for(int i=0;i<this.articletype.size();i++){
				System.out.println( articletype.get(i).getArticleTypeName());
				System.out.println(articletype.get(i).getArticlePrint());
				System.out.println( articletype.get(i).getDocumentName());
			
				  if( articletype.get(i).getArticleTypeName().equalsIgnoreCase("CIN")&& articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().equalsIgnoreCase("Invoice Details")){  
					                         
			       cinPhrs=new Phrase("CIN: "+articletype.get(i).getArticleTypeValue(),font10);
			
				  }
				      
			 }
			   PdfPCell  cinCell=new PdfPCell(cinPhrs);
			   cinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			   cinCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			   cinCell.setFixedHeight(15f);
			   ParentTbl.addCell(cinCell);
			 
				
				
				Phrase panPhrs=new Phrase("PAN NO: ",font10);	
			 for(int i=0;i<this.articletype.size();i++){
				  
				  if(articletype.get(i).getArticleTypeName().equalsIgnoreCase("PAN NO")&&articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().equalsIgnoreCase("Invoice Details")){  
				 	System.out.println("Inside pan no");
			                   panPhrs=new Phrase("PAN NO: "+articletype.get(i).getArticleTypeValue(),font10);
			 } 
	 }
			   PdfPCell panCell=new PdfPCell(panPhrs);
				panCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				panCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				panCell.setFixedHeight(15f);
				ParentTbl.addCell(panCell);
				
			 Phrase tanPhrs=new Phrase("TAN : ",font10);
			 for(int i=0;i<this.articletype.size();i++){
           				  
				  if(articletype.get(i).getArticleTypeName().equalsIgnoreCase("TAN NO")&&articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().equalsIgnoreCase("Invoice Details")){  
					  System.out.println("Inside tan no");
		                    tanPhrs=new Phrase("TAN : "+articletype.get(i).getArticleTypeValue(),font10);
			 } 
	 }
			    PdfPCell tanCell=new PdfPCell(tanPhrs);
				tanCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				tanCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				tanCell.setFixedHeight(15f);
				ParentTbl.addCell(tanCell);
			 
			 
				 Phrase ecNoPhrs=new Phrase("ESIC : ",font10);
			  for(int i=0;i<this.articletype.size();i++){

				  if( articletype.get(i).getArticleTypeName().equalsIgnoreCase("ESIC")&& articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().equalsIgnoreCase("Invoice Details")){  
					  System.out.println("Inside esic  no");
					    ecNoPhrs=new Phrase("ESIC : "+articletype.get(i).getArticleTypeValue(),font10);	  
				  }
			  }
			    PdfPCell ecNoCell=new PdfPCell(ecNoPhrs);
			    ecNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			    ecNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			    ecNoCell.setFixedHeight(15f);
				ParentTbl.addCell(ecNoCell);  
			
		 	 Phrase pfphrs=new Phrase("P.F : ",font10);
	for(int i=0;i<this.articletype.size();i++){		  
						  if(articletype.get(i).getArticleTypeName().equalsIgnoreCase("P.F")&&articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().equalsIgnoreCase("Invoice Details")){  
			 	
				     pfphrs=new Phrase("P.F : "+articletype.get(i).getArticleTypeValue(),font10);
				     System.out.println("Inside p.f. no");
				     
					 }  
		}
	
	    PdfPCell pfCell =new PdfPCell(pfphrs);
		pfCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		pfCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		pfCell.setFixedHeight(15f);
		ParentTbl.addCell(pfCell);
	               
	
		 Phrase stphrs=new Phrase("ST/GST : ",font10);
	for(int i=0;i<this.articletype.size();i++)
	   {		  
						  if(articletype.get(i).getArticleTypeName().equalsIgnoreCase("Service Tax No")&&articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().equalsIgnoreCase("Invoice Details")){  
			 	
				     stphrs=new Phrase("ST/GST : "+articletype.get(i).getArticleTypeValue(),font10);
				     System.out.println("Inside service tax no");
						  }
		  }
	
	    PdfPCell stCell =new PdfPCell(stphrs);
		stCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		stCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		stCell.setFixedHeight(15f);
		ParentTbl.addCell(stCell);
		
		try {
			
	              document.add(ParentTbl);
	              
       } catch (Exception e1) 
		   {   
	           e1.printStackTrace();
           }
		}
private String getFirstLetterUpperCase(String customerFullName) {
	
	String customerName="";
	String[] customerNameSpaceSpilt=customerFullName.split(" ");
	int count=0;
	for (String name : customerNameSpaceSpilt) 
	{
		String nameLowerCase=name.toLowerCase();
		if(count==0)
		{
			customerName=customerName+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);
		}
		else
		{
			customerName=customerName+" "+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);	
		}
		
		count=count+1;
		
	}
	return customerName;  
	
	}

		//method 3 start
	     public void createodrinfotbl()
	     {
	    
	 		PdfPTable odrinfoTbl=new PdfPTable(2);
	 		odrinfoTbl.setWidthPercentage(100);
	 		try {
	 			odrinfoTbl.setWidths(new float[] { 70,30 });
	 		} catch (Exception e1) {
	 			e1.printStackTrace();
	 		}
	 		
	 		Phrase odrnophrs=new Phrase("Order No. / Voucher No : "+invoiceentity.getContractCount(),font10bold);
	 		PdfPCell odrinfoCell =new PdfPCell(odrnophrs);
	 		odrinfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	 		odrinfoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	 		odrinfoCell.setBorderWidthRight(0);
	 		odrinfoCell.setFixedHeight(25f);
	 		odrinfoTbl.addCell(odrinfoCell);
	 		
	 		Phrase odrdtphrs=new Phrase("Dated"+"  "+fmt.format(invoiceentity.getOrderCreationDate()),font10bold);
	 		PdfPCell odrdtCell =new PdfPCell(odrdtphrs);
	 		odrdtCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	 		odrdtCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	 		odrdtCell.setFixedHeight(25f);
	 		odrdtCell.setBorderWidthLeft(0);
	 		odrinfoTbl.addCell(odrdtCell);
	 		
	 		try {
	 			document.add(odrinfoTbl);
	 		} catch (Exception e) 
	 		{
	 			e.printStackTrace();
	 		}
	 	}  
	       //method 3 end	     
	     private void createparticulars() 
	       {
		 		PdfPTable particularesTbl=new PdfPTable(2);
		 		particularesTbl.setWidthPercentage(100);
		 		try {
		 			particularesTbl.setWidths(new float[] { 88, 12 });
		 		} catch (Exception e1) {
		 			e1.printStackTrace();
		 		}
		 		Phrase phrs1=new Phrase("PARTICULARS",font10bold);
		 		PdfPCell particularsCell =new PdfPCell(phrs1);  
		 		particularsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		 		particularsCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		 		particularsCell.setFixedHeight(25f);
		 		
		 		Phrase rsphrs=new Phrase("RS",font10bold);
		 		PdfPCell rsCell =new PdfPCell(rsphrs);
		 		rsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		 		rsCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		 		rsCell.setFixedHeight(25f);
		 		
		 		particularesTbl.addCell(particularsCell);
		 		particularesTbl.addCell(rsCell);
		 		
//		 		for(int i=0;i<invoiceentity.getSalesOrderProducts().size();i++){
		 			
		 			System.out.println("outside particular product condition ");
		 			
		 if(invoiceentity.getSalesOrderProducts().size()<= 2 )
		 {
			 System.out.println("inside particular product condition ");
			 String prodName = "";
	 /******************************* commented this code as per mail confirmation  ********************************/
//			 Phrase po = new Phrase("");
//			 if(con.getRefNo()!=null && !con.getRefNo().equals("") )
//			 {
//			  po = new Phrase("PO No : "+ con.getRefNo(),font9);
//			 }
//			 else
//			 {
//		      po = new Phrase("PO No : " ,font9);	 
//			 }
//			 PdfPCell poCell = new PdfPCell(po);
//			 poCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			 poCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//			 poCell.setBorderWidthBottom(0);
//			 poCell.setBorderWidthTop(0);
//			 particularesTbl.addCell(poCell);
//			 
//			 Phrase poblnk = new Phrase(" ",font9);
//			 PdfPCell poblnkCell = new PdfPCell(poblnk);
//			 poblnkCell.setBorderWidthBottom(0);
//			 poblnkCell.setBorderWidthTop(0);
//			 particularesTbl.addCell(poblnkCell);   

			 /******************************* End Here  ********************************/
			 
			 ////////////////////////////
			 
			 Phrase blnkphrse = new Phrase ("  ",font10);
				PdfPCell blnkCell =new PdfPCell();
				blnkCell.addElement(blnkphrse);
				blnkCell.setBorderWidthTop(0);    
				blnkCell.setBorderWidthBottom(0);  
			 
			 Phrase workOrder = new Phrase(""); 
				if(con.getRefNo2()!=null && !con.getRefNo2().equals("")){
				  workOrder =new Phrase("Work Order No : "+ con.getRefNo2() ,font10);  // suggested by Nitin sir
				}
				else
				{
				  workOrder =new Phrase("Work Order No : " ,font10);   
				}
				
				PdfPCell workOrderCell = new PdfPCell(workOrder);
				workOrderCell.setBorderWidthTop(0);
				workOrderCell.setBorderWidthBottom(0);
				workOrderCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				workOrderCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				particularesTbl.addCell(workOrderCell);
				particularesTbl.addCell(blnkCell);
				
				Phrase workOrderDate =new Phrase("");
				if(con.getRefDate()!=null && !con.getRefDate().equals("")){  
				 workOrderDate =new Phrase("Work Order Date : "+ con.getRefDate(),font10);   // suggested by Nitin sir
				}
				else 
				{
				 workOrderDate =new Phrase("Work Order Date : ",font10);	
				}
				PdfPCell workOrderDateCell = new PdfPCell(workOrderDate);
				workOrderDateCell.setBorderWidthTop(0);    
				workOrderDateCell.setBorderWidthBottom(0);  
				workOrderDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				workOrderDateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				particularesTbl.addCell(workOrderDateCell);  
				particularesTbl.addCell(blnkCell);
			
			 ////////////////////////////
			 
			 for(int i=0;i<invoiceentity.getSalesOrderProducts().size();i++)
			 {
		 		 prodName = invoiceentity.getSalesOrderProducts().get(i).getProdName();
			 
				Phrase prodNamephrase=new Phrase(prodName,font10);
				PdfPCell prodNamecell=new PdfPCell(prodNamephrase);  
				
				prodNamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				prodNamecell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				prodNamecell.setBorderWidthTop(0);
				prodNamecell.setBorderWidthBottom(0);
				particularesTbl.addCell(prodNamecell);   
			 
				Phrase prodPrice = new Phrase (""+invoiceentity.getSalesOrderProducts().get(i).getPrice(),font10);
				 
			    PdfPCell prodPricecell=new PdfPCell(prodPrice);  
				 prodPricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				 prodPricecell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				 prodPricecell.setBorderWidthTop(0);
				 prodPricecell.setBorderWidthBottom(0);
				 particularesTbl.addCell(prodPricecell);
				 
			 }		  
				
/****************************** As per mail this code commented ***************************************/
//				Phrase custPanNo =new Phrase("Pan No : ",font9);
//				 for(int i=0;i<invoiceentity.getSalesOrderProducts().size();i++)
//				 {
//				 if(articletype.get(i).getArticleTypeName().equalsIgnoreCase("Pan No")&&articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().equalsIgnoreCase("Invoice Details"))
//				   {  
//					 	
//					 custPanNo = new Phrase("Pan No : " + articletype.get(i).getArticleTypeValue(),font9);
//	                 System.out.println("Pan No ");
//                   }
//				 }
//				 
//				PdfPCell custpanNoCell = new PdfPCell(custPanNo);
//				custpanNoCell.setBorderWidthBottom(0);    
//				custpanNoCell.setBorderWidthTop(0);
//				custpanNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				custpanNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//				particularesTbl.addCell(custpanNoCell);
//				particularesTbl.addCell(blnkCell);
			 
			 /////////////// Pan no Code  End Here ///////////// 
				//////////////////////////////////////////////////////////////////////////////////////////////////////
//				 String ferqVal= null ;
//				 for(int i=0;i<invoiceentity.getSalesOrderProducts().size();i++)
//				 {
				    	
//				    if(con.getItems().get(i).getProductCode().equalsIgnoreCase("WB")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT")
//				    	||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT")
//				    	||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO")
//				    	||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA")
//				    	||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT")
//				    	||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP")
//				    	||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC")
//				    	)
//				    {
//				    	 ferqVal= " Treatment " ;
//				    }
//				    
//				   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT00")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT00")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO00")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA00")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT00")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP00")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC00")
//					)
//				{   
//					   ferqVal= " As & When " ;
//					
//				}
//				   
//				   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT365")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO365")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA365")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT365")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP365")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC365")
//							)
//						{
//					      ferqVal= " Daily" ;
//						}
//				   
//				   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT52")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT52")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO52")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA52")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT52")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP52")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC52")
//							)
//						{
//					         ferqVal= " Weeekly " ;	
//						}
//				   
//				   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT36")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT36")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO36")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA36")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT36")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP36")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC36")
//							)
//						{
//					        ferqVal= " Twice the Month " ;		
//						}
//				   
//				   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT24")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT24")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO24")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA24")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT24")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP24")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC24")
//							)
//						{
//					        ferqVal= " Four Nightly " ; 
//						}
//				   
//				   
//				   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT12")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT12")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO12")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA12")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT12")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP12")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC12")
//							)
//						{
//					      ferqVal= " Monthly" ;
//						}
//				   
//				   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT06")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT06")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO06")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA06")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT06")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP06")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC06")
//							)
//						{
//					      ferqVal= " Alternate Month" ;
//						}
//				   
//				   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT04")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT04")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO04")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA04")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT04")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP04")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC04")
//							)
//						{
//					         ferqVal= " Quarterly " ;
//						}
//				   
//				   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT03")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT03")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO03")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA03")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT03")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP03")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC03")
//							)
//						{
//					          ferqVal= " Four Monthtly " ;	
//						}
//				   
//				   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB02")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT02")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC02")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT02")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY02")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO02")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST02")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA02")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT02")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT02")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT02")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP02")
//							||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC02")
//							)
//						{
//					        ferqVal= " Six Monthly " ;	
//						}
//				  }  
			  //////////////////////////////////////////////frequency value end here   //////////////////////////////////// 
				    
//				    Phrase ferqValphrase = new Phrase ("Frequency : "+ ferqVal,font9);
//					PdfPCell freqValueCell = new PdfPCell(ferqValphrase);
//					freqValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//					freqValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//					freqValueCell.setBorderWidthBottom(0);
//					freqValueCell.setBorderWidthTop(0);
			 /************************************** End Here ***************************/
				
				////////////////////////// As per Mail below changes done by Ajinkya on Date: 23/05/2017 ////// 
				Phrase serAdrsLbl = new Phrase ("Services Delivered At : ",font10);
				PdfPCell serAdrsLblCell = new PdfPCell(serAdrsLbl);
				serAdrsLblCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				serAdrsLblCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				serAdrsLblCell.setBorderWidthBottom(0);
				serAdrsLblCell.setBorderWidthTop(0);
				particularesTbl.addCell(serAdrsLblCell);
				           /////////////////////////// End Here ///////////////////////
				
//					particularesTbl.addCell(blnkCell);
					particularesTbl.addCell(blnkCell);
				
				//////////////////////////////////////////////////////////////////////////////////////////////////////
				
					
					///////////// address code  / //////
				    String localityline1= ""; 
				    String localityline2= ""; 
				    String localityline3= ""; 
					String addressline=" ";
					
					System.out.println("Loc2 "+cust.getSecondaryAdress().getCity()+" - "+cust.getSecondaryAdress().getPin());
					System.out.println("Loc3 "+cust.getSecondaryAdress().getState()+", "+cust.getSecondaryAdress().getCountry());
					
					if(ser!=null)
					{	
						System.out.println("ser not Null ");
					if(ser.getAddress().getAddrLine2()!=null && !ser.getAddress().getAddrLine2().equals(""))
					{    
						 System.out.println("Adrs 2 not null ");
						addressline=ser.getAddress().getAddrLine1()+", "+ser.getAddress().getAddrLine2()+", ";
						 Phrase refInfophrs1 = new Phrase(addressline,font10);
						 PdfPCell refinfo1cell=new PdfPCell(refInfophrs1);
							refinfo1cell.setBorderWidthTop(0);
							refinfo1cell.setBorderWidthBottom(0);
							particularesTbl.addCell(refinfo1cell);
							particularesTbl.addCell(blnkCell);
						
					}
					else    
					{
						addressline=ser.getAddress().getAddrLine1()+", ";
						 Phrase refInfophrs1 = new Phrase(addressline,font10);
						 PdfPCell refinfo1cell=new PdfPCell(refInfophrs1);
							refinfo1cell.setBorderWidthTop(0);
							refinfo1cell.setBorderWidthBottom(0);
							particularesTbl.addCell(refinfo1cell);
							particularesTbl.addCell(blnkCell);
						 System.out.println("Adrs 2 null ");
						 
					}
					
					System.out.println("ser Loc"+ser.getAddress().getLocality());
					System.out.println("ser landmrk"+ser.getAddress().getLandmark());
					
					if((!ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==false))
					{
						System.out.println("inside both null condition1");
						System.out.println("Landmark "+ser.getAddress().getLandmark()+" Locality "+ser.getAddress().getLocality());
						localityline1 = (ser.getAddress().getLandmark()+", "+ser.getAddress().getLocality()+", ");
						Phrase refInfophrs2 = new Phrase(localityline1,font10);
						 PdfPCell refinfo2cell=new PdfPCell(refInfophrs2);
							refinfo2cell.setBorderWidthTop(0);
							refinfo2cell.setBorderWidthBottom(0);
							particularesTbl.addCell(refinfo2cell);
							particularesTbl.addCell(blnkCell);
					
					}
					else if((!ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==true))
					{
						System.out.println("inside Loc null condition 2");
						localityline1= (ser.getAddress().getLandmark()+", ");
						Phrase refInfophrs2 = new Phrase(localityline1,font10);
						 PdfPCell refinfo2cell=new PdfPCell(refInfophrs2);
							refinfo2cell.setBorderWidthTop(0);
							refinfo2cell.setBorderWidthBottom(0);
							particularesTbl.addCell(refinfo2cell);
							particularesTbl.addCell(blnkCell);
					}
					
					else if((ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==false))
					{
						System.out.println("inside both null condition 3");
						localityline1= (ser.getAddress().getLocality()+", ");
						Phrase refInfophrs2 = new Phrase(localityline1,font10);
						 PdfPCell refinfo2cell=new PdfPCell(refInfophrs2);
							refinfo2cell.setBorderWidthTop(0);
							refinfo2cell.setBorderWidthBottom(0);
							particularesTbl.addCell(refinfo2cell);
							particularesTbl.addCell(blnkCell);
						
					}   
					System.out.println("if Locline condn 2 & 3");
					localityline2 =(ser.getAddress().getCity()+" - "+ser.getAddress().getPin()+". " );
					Phrase refInfophrs3 = new Phrase(localityline2,font10);
					 PdfPCell refinfo3cell=new PdfPCell(refInfophrs3);
						refinfo3cell.setBorderWidthTop(0);
						refinfo3cell.setBorderWidthBottom(0);
						particularesTbl.addCell(refinfo3cell);
						particularesTbl.addCell(blnkCell);
					
					localityline3 =(ser.getAddress().getState()+", "+ser.getAddress().getCountry()+". ");
					Phrase refInfophrs4 = new Phrase(localityline3,font10);
					 PdfPCell refinfo4cell=new PdfPCell(refInfophrs4);
						refinfo4cell.setBorderWidthTop(0);
						refinfo4cell.setBorderWidthBottom(0);
						particularesTbl.addCell(refinfo4cell);
						particularesTbl.addCell(blnkCell);
					
					}
					else
					{
						System.out.println("ser Null ");

						////////////////////////////////// rohan suggested that take this address from customer ////////////////////// 
					
//						take service addrsss from customer or contract
						if(cust.getSecondaryAdress().getAddrLine2() != null && !cust.getSecondaryAdress().getAddrLine2().equals(""))   
						{
						    addressline=cust.getSecondaryAdress().getAddrLine1()+", "+cust.getSecondaryAdress().getAddrLine2()+", ";
						    Phrase refInfophrs1 = new Phrase(addressline,font10);
						    PdfPCell refinfo1cell=new PdfPCell(refInfophrs1);
							refinfo1cell.setBorderWidthTop(0);
							refinfo1cell.setBorderWidthBottom(0);
							particularesTbl.addCell(refinfo1cell);
							particularesTbl.addCell(blnkCell);
						    System.out.println("Adrs 2 not null ");
						}
						else
						{   
							
							 addressline= cust.getSecondaryAdress().getAddrLine1()+", ";
							 Phrase refInfophrs1 = new Phrase(addressline,font10);
							 PdfPCell refinfo1cell=new PdfPCell(refInfophrs1);
								refinfo1cell.setBorderWidthTop(0);
								refinfo1cell.setBorderWidthBottom(0);
								particularesTbl.addCell(refinfo1cell);
								particularesTbl.addCell(blnkCell);
							 System.out.println("Adrs 2  null ");
						}
						
						System.out.println("Loc "+cust.getAdress().getLocality());
						System.out.println("landmrk "+cust.getAdress().getLandmark());
						
						if((!cust.getSecondaryAdress().getLandmark().equals(""))&&(cust.getSecondaryAdress().getLocality().equals("")==false)){
							System.out.println("inside both null condition1");
							System.out.println("Else Loc condn 1");
							localityline1 = (cust.getSecondaryAdress().getLandmark()+", "+cust.getSecondaryAdress().getLocality()+", ");
							Phrase refInfophrs2 = new Phrase(localityline1,font10);
							   PdfPCell refinfo2cell=new PdfPCell(refInfophrs2);
								refinfo2cell.setBorderWidthTop(0);
								refinfo2cell.setBorderWidthBottom(0);
								particularesTbl.addCell(refinfo2cell);
								particularesTbl.addCell(blnkCell);
						}
						else if((!cust.getSecondaryAdress().getLandmark().equals(""))&&(cust.getSecondaryAdress().getLocality().equals("")==true)){
							System.out.println("inside both null condition 2");
							System.out.println("Loc Null ");
							localityline1= (cust.getSecondaryAdress().getLandmark()+", ");
							Phrase refInfophrs2 = new Phrase(localityline1,font10);
							 PdfPCell refinfo2cell=new PdfPCell(refInfophrs2);
								refinfo2cell.setBorderWidthTop(0);
								refinfo2cell.setBorderWidthBottom(0);
								particularesTbl.addCell(refinfo2cell);
								particularesTbl.addCell(blnkCell);
						} 
						else if((cust.getSecondaryAdress().getLandmark().equals(""))&&(cust.getSecondaryAdress().getLocality().equals("")==false)){
							System.out.println("inside both null condition 3");
							System.out.println("LAndmark Null ");
							localityline1= (cust.getSecondaryAdress().getLocality()+", ");
							Phrase refInfophrs2 = new Phrase(localityline1,font10);
							 PdfPCell refinfo2cell=new PdfPCell(refInfophrs2);
								refinfo2cell.setBorderWidthTop(0);
								refinfo2cell.setBorderWidthBottom(0);
								particularesTbl.addCell(refinfo2cell);
								particularesTbl.addCell(blnkCell);
						}
						
						System.out.println("else Locline condn 2 & 3");
						localityline2 = (cust.getSecondaryAdress().getCity()+" - "+cust.getSecondaryAdress().getPin()+". " );
						Phrase refInfophrs3 = new Phrase(localityline2,font10);
						 PdfPCell refinfo3cell=new PdfPCell(refInfophrs3);
							refinfo3cell.setBorderWidthTop(0);
							refinfo3cell.setBorderWidthBottom(0);
							particularesTbl.addCell(refinfo3cell);
							particularesTbl.addCell(blnkCell);
						localityline3 = (cust.getSecondaryAdress().getState()+", "+cust.getSecondaryAdress().getCountry()+". ");
						Phrase refInfophrs4 = new Phrase(localityline3,font10);
						PdfPCell refinfo4cell=new PdfPCell(refInfophrs4);
							refinfo4cell.setBorderWidthTop(0);
							refinfo4cell.setBorderWidthBottom(0);
							particularesTbl.addCell(refinfo4cell);
							particularesTbl.addCell(blnkCell);
					}
					
					/////////////////////// end here  /////////////////
					
				/////////////////////////////////////////////////////////////////////////////////////////////////////////
//				String periodOfodr = "Service period billing FOR THE PERIOD FROM " + fmt.format( con.getStartDate()) + " TO "+ fmt.format(con.getEndDate());
				String periodOfodr = "FOR THE PERIOD FROM " + fmt.format( con.getStartDate()) + " TO "+ fmt.format(con.getEndDate());
				
				Phrase orderPeriod = new Phrase(periodOfodr,font10);
				PdfPCell periodOfodrcell=new PdfPCell(orderPeriod);
				periodOfodrcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				periodOfodrcell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				periodOfodrcell.setBorderWidthTop(0);
				periodOfodrcell.setBorderWidthBottom(0);
				particularesTbl.addCell(periodOfodrcell);
				particularesTbl.addCell(blnkCell);
				
			/**
			 * 
			 *  As per Mail branch location removed from pdf	    
			 *   
			*/
				/////////////////////////// Start tax calc for only two records ////////////////////////////
				
					 for(int i=0;i<invoiceentity.getSalesOrderProducts().size();i++)
					 {
						 System.out.println("inside Billing for loop Condn");
						 
			    	if((invoiceentity.getSalesOrderProducts().get(i).getVatTax().getPercentage()==0)&&
			    		(invoiceentity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0)&&
			    		(invoiceentity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()==15))    
			    	 { 
			    		  System.out.println("3 inside st 15");
			    		
			    		/////////////////////////
			    		 
			    		  double tax1 = 14  *billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
				          double tax2 = 0.5 *billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
				          double tax3 = 0.5 *billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
				          double tax = tax1 + tax2 + tax3 ;
				          
				            System.out.println(tax1);
				        	System.out.println(tax2);
				        	System.out.println(tax3);   
				        	System.out.println(tax);
				        	
				          if(tax1!=0){
				        	  
				        System.out.println("inside ServiceTax Value");
				          
			    		String str = "Service Tax "+ " @ "+" 14%";
		 				Phrase strPh = new Phrase(str,font10);
						PdfPCell strPhCell =new PdfPCell(strPh);
						strPhCell.setBorderWidthTop(0);
						strPhCell.setBorderWidthBottom(0);
						strPhCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						strPhCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						particularesTbl.addCell(strPhCell);
						
						String strTax = ""+ df.format(tax1);
	                    Phrase words1 = new Phrase( strTax ,font10);
	    			    PdfPCell taxCell = new PdfPCell(words1);
	    			    taxCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	    			    taxCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	    			    taxCell.setBorderWidthBottom(0);
	    			    taxCell.setBorderWidthTop(0);
	    			    particularesTbl.addCell(taxCell); 
				          
				          }
			    	
			    	 if(tax2!=0){
			    		 
					    System.out.println("inside sbc Value");
						String str123 = "Swachha Bharat Cess"+ " @ "+" 0.5%";
		 				Phrase str123Ph = new Phrase(str123,font10);
						PdfPCell str123PhCell =new PdfPCell(str123Ph);
						str123PhCell.setBorderWidthTop(0);
						str123PhCell.setBorderWidthBottom(0);
						str123PhCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						str123PhCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						particularesTbl.addCell(str123PhCell);
						
						String strTax1 = ""+ df.format(tax2);
        	    	    Phrase words2 = new Phrase(strTax1 ,font10);
					    PdfPCell tax1Cell = new PdfPCell(words2);
					    tax1Cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					    tax1Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					    tax1Cell.setBorderWidthBottom(0);
					    tax1Cell.setBorderWidthTop(0);
					    particularesTbl.addCell(tax1Cell); 
						
			          }
					    
					    if(tax3!=0)
					    {
			        	  System.out.println("inside kkc Value");
					    
						String kkrstr = "Krishi Kalyan Cess "+ " @ "+" 0.5%";
		 				Phrase kkrstrPh = new Phrase(kkrstr,font10);
						PdfPCell kkrstrPhCell =new PdfPCell(kkrstrPh);
						kkrstrPhCell.setBorderWidthTop(0);
						kkrstrPhCell.setBorderWidthBottom(0);
						kkrstrPhCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						kkrstrPhCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						particularesTbl.addCell(kkrstrPhCell);
						
						 String strTax2 = ""+ df.format(tax3);
						 Phrase words3 = new Phrase(""+ strTax2 +"",font10);
						 PdfPCell tax2Cell = new PdfPCell(words3);
						 tax2Cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						 tax2Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						 tax2Cell.setBorderWidthBottom(0);
						 tax2Cell.setBorderWidthTop(0);
					     particularesTbl.addCell(tax2Cell); 
						
			          } 
					    	
//			    	   }
//			    } 
		 }
			      
			    	else if((invoiceentity.getSalesOrderProducts().get(i).getVatTax().getPercentage()==0)&&(invoiceentity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()>0)&&(invoiceentity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()==14.5))
			      	{
			      		System.out.println("inside st 14.5");
			      		
						double tax = 14 * billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
			        	double tax2 = 0.5 * billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
			        	double totaltax = tax + tax2 ;
			        	
			    	if(tax!=0)
			    	{
			      		String str = "Service Tax "+ " @ "+" 14%";
						Phrase strp = new Phrase(str,font10);
						PdfPCell strcell =new PdfPCell(strp);
						strcell.setBorderWidthTop(0);
						strcell.setBorderWidthBottom(0);
						strcell.setHorizontalAlignment(Element.ALIGN_LEFT);
						strcell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						particularesTbl.addCell(strcell);
						
			            String strTax = ""+ df.format(tax);
			    	    Phrase words1 = new Phrase(""+ strTax ,font10);
			    	    System.out.println(tax);
			    	    System.out.println(tax2);
			    	    PdfPCell tax3Cell = new PdfPCell(words1);
			    	    tax3Cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			    	    tax3Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			    	    tax3Cell.setBorderWidthBottom(0);
				    	tax3Cell.setBorderWidthTop(0);
				    	particularesTbl.addCell(tax3Cell);
						
			      	}
	
			    	if(tax2!=0)
			    	{
						    String str123 = "Swachha Bharat Cess"+ " @ "+" 0.5%";   
						 	Phrase str123p = new Phrase(str123,font10);
							PdfPCell str123cell =new PdfPCell(str123p);
							str123cell.setBorderWidthTop(0);
							str123cell.setBorderWidthBottom(0);
							str123cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							str123cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							particularesTbl.addCell(str123cell);
							
							String strTax1 = ""+ df.format(tax2);
							Phrase words2 = new Phrase(""+ strTax1 +"",font10);
						    PdfPCell tax4Cell = new PdfPCell(words2);
						    tax4Cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						    tax4Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						    tax4Cell.setBorderWidthBottom(0);
						    tax4Cell.setBorderWidthTop(0);
						    particularesTbl.addCell(tax4Cell); 
			    	}		
			      		////////////////////////////////////////
//				    	 
			    	}
			    	
			    	else if((invoiceentity.getSalesOrderProducts().get(i).getVatTax().getPercentage()!=0)&&(invoiceentity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()==0))
			    	{
			    		System.out.println("inside vat");
			    		double vat1 =invoiceentity.getSalesOrderProducts().get(i).getVatTax().getPercentage()*billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
			    		
			    		if(vat1!=0)
			    		{
			    	    String str = billingTaxesLis.get(i).getTaxChargeName().trim()+ " @ "+ billingTaxesLis.get(i).getTaxChargePercent();
						Phrase strp = new Phrase(str,font10);
						PdfPCell strcell =new PdfPCell(strp);
						strcell.setBorderWidthTop(0);
						strcell.setBorderWidthBottom(0);
						strcell.setHorizontalAlignment(Element.ALIGN_LEFT);
						strcell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						particularesTbl.addCell(strcell);
//							
			    		String strTax = "" + df.format(vat1) ;
			        	Phrase words1 = new Phrase(""+ strTax,font10);
			        	PdfPCell tax5Cell = new PdfPCell(words1);
			        	tax5Cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			        	tax5Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			        	tax5Cell.setBorderWidthBottom(0);
				    	tax5Cell.setBorderWidthTop(0);
				    	particularesTbl.addCell(tax5Cell);
				    	
				    	} 
			    	}  
			    	else 
			    	{
			    	
			    		particularesTbl.addCell(blnkCell);
			    		particularesTbl.addCell(blnkCell);
			    	}
			    	
				 }
//			 }
			
			    /////////////////////////////////////////////////////////End	
				
		 			}
				
		 	else if(invoiceentity.getSalesOrderProducts().size()> 2 && invoiceentity.getSalesOrderProducts().size()<= 7 )
		 		{
		 		
//		 		 Phrase po = new Phrase("");
//				 if(con.getRefNo()!=null && !con.getRefNo().equals("") ){
//				  po = new Phrase("PO No : "+ con.getRefNo(),font9);
//				 }
//				 else
//				 {
//			      po = new Phrase("PO No : " ,font9);	 
//				 }
//				 PdfPCell poCell = new PdfPCell(po);
//				 poCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				 poCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//				 poCell.setBorderWidthBottom(0);
//				 poCell.setBorderWidthTop(0);
//				 particularesTbl.addCell(poCell);
//				 
//				 Phrase poblnk = new Phrase(" ",font9);
//				 PdfPCell poblnkCell = new PdfPCell(poblnk);
//				 poblnkCell.setBorderWidthBottom(0);
//				 poblnkCell.setBorderWidthTop(0);
//				 particularesTbl.addCell(poblnkCell);
		 		
		 		Phrase blnkphrse = new Phrase ("  ",font10);
				PdfPCell blnkCell =new PdfPCell();
				blnkCell.addElement(blnkphrse);
				blnkCell.setBorderWidthTop(0);    
				blnkCell.setBorderWidthBottom(0);  
				 
				 Phrase workOrder =new Phrase(""); 
				if(con.getRefNo2()!=null && !con.getRefNo2().equals("")){
				  workOrder =new Phrase("Work Order No : "+ con.getRefNo2() ,font10);    // + wo.getCount()
				}
				else
				{
				  workOrder =new Phrase("Work Order No : " ,font10);    // + wo.getCount()
				}
				
				PdfPCell workOrderCell = new PdfPCell(workOrder);
				workOrderCell.setBorderWidthTop(0);
				workOrderCell.setBorderWidthBottom(0);
				workOrderCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				workOrderCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				particularesTbl.addCell(workOrderCell);
				particularesTbl.addCell(blnkCell);
				
				Phrase workOrderDate =new Phrase("");
				if(con.getRefDate()!=null &&! con.getRefDate().equals("")){
				 workOrderDate =new Phrase("Work Order Date : "+ con.getRefDate(),font10);
				}
				else 
				{
				 workOrderDate =new Phrase("Work Order Date : ",font10);	
				}
				PdfPCell workOrderDateCell = new PdfPCell(workOrderDate);
				workOrderDateCell.setBorderWidthTop(0);    
				workOrderDateCell.setBorderWidthBottom(0);  
				workOrderDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				workOrderDateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				particularesTbl.addCell(workOrderDateCell);
				particularesTbl.addCell(blnkCell);
		 		
				
		 		for(int i=0;i<invoiceentity.getSalesOrderProducts().size();i++)
				 {
		 				
		 				 String prodName = invoiceentity.getSalesOrderProducts().get(i).getProdName();
						 Phrase prodNamephrase=new Phrase(prodName,font10);
						 PdfPCell prodNamecell=new PdfPCell(prodNamephrase);  
						 prodNamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
						 prodNamecell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						 prodNamecell.setBorderWidthTop(0);
						 prodNamecell.setBorderWidthBottom(0);
						 particularesTbl.addCell(prodNamecell);
						 
						 Phrase price = new Phrase (""+invoiceentity.getSalesOrderProducts().get(i).getPrice(),font10);	
					     PdfPCell prodPricecell=new PdfPCell(price);  
						 prodPricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						 prodPricecell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						 prodPricecell.setBorderWidthTop(0);
						 prodPricecell.setBorderWidthBottom(0);
						 particularesTbl.addCell(prodPricecell); 
				 }
				
//				Phrase custPanNo =new Phrase("Pan No : ",font9);
//				 for(int i=0;i<invoiceentity.getSalesOrderProducts().size();i++)
//				 {
//				 if(articletype.get(i).getArticleTypeName().equalsIgnoreCase("Pan No")&&articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().equalsIgnoreCase("Invoice Details"))
//				   {  
//					 	
//					 custPanNo = new Phrase("Pan No : " + articletype.get(i).getArticleTypeValue(),font9);
//	                 System.out.println("Pan No ");
//                   }
//				 }
//				 
//				PdfPCell custpanNoCell = new PdfPCell(custPanNo);
//				custpanNoCell.setBorderWidthBottom(0);    
//				custpanNoCell.setBorderWidthTop(0);
//				custpanNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				custpanNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//				particularesTbl.addCell(custpanNoCell);
//				particularesTbl.addCell(blnkCell);
				
		 		
	/////////////////////////////// Add tax  for invoice value cal ///////////////////////////////////////
						
						 Phrase blnk = new Phrase ("",font10);
						 PdfPCell blnkphrsCell = new PdfPCell(blnk);
						 blnkphrsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						 blnkphrsCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						 
						 for(int i=0;i<billingTaxesLis.size();i++)
						 {	 
							 if ( billingTaxesLis.get(i).getTaxChargeName().equals("Service Tax") && billingTaxesLis.get(i).getTaxChargePercent()!=0 )
							 {
					    	if((invoiceentity.getSalesOrderProducts().get(i).getVatTax().getPercentage()==0)&&(invoiceentity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0)&&(invoiceentity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()==15))    
					    	{
					    		System.out.println("3 inside st 15");
					    		
					    		double tax1 = 14 *billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
					        	double tax2 = 0.5 *billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
					        	double tax3 = 0.5 *billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
					        	double tax = tax1 + tax2 + tax3 ;
					    		
					    		/////////////////////////
					        	
					        	System.out.println(tax1);
					        	System.out.println(tax2);
					        	System.out.println(tax3);   
					        	System.out.println(tax);
					        	
					        if(tax1!=0)
					        {
					    		String str = "Service Tax "+ " @ "+" 14%";
				 				Phrase strPh = new Phrase(str,font10);
								PdfPCell strPhCell =new PdfPCell(strPh);
								strPhCell.setBorderWidthTop(0);
								strPhCell.setBorderWidthBottom(0);
								strPhCell.setHorizontalAlignment(Element.ALIGN_LEFT);
								strPhCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
								particularesTbl.addCell(strPhCell);
					          	
					            String strTax = ""+ df.format(tax1);
					    	    Phrase words1 = new Phrase(strTax,font10);
					    	    PdfPCell taxCell = new PdfPCell(words1);
					    	    taxCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					    	    taxCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					    	    taxCell.setBorderWidthBottom(0);
					    	    taxCell.setBorderWidthTop(0);
					    	    particularesTbl.addCell(taxCell);  
					    	    
					        }
				 			
					        if(tax2!=0)
					        {
								String str123 = "Swachha Bharat Cess"+ " @ "+" 0.5%";
				 				Phrase str123Ph = new Phrase(str123,font10);
								PdfPCell str123PhCell =new PdfPCell(str123Ph);
								str123PhCell.setBorderWidthTop(0);
								str123PhCell.setBorderWidthBottom(0);
								str123PhCell.setHorizontalAlignment(Element.ALIGN_LEFT);
								str123PhCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
								particularesTbl.addCell(str123PhCell);
								
								  String strTax1 = ""+ df.format(tax2);
							    	Phrase words2 = new Phrase(""+ strTax1 +"",font10);
							    	PdfPCell tax1Cell = new PdfPCell(words2);
							    	tax1Cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							    	tax1Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							    	tax1Cell.setBorderWidthBottom(0);
							    	tax1Cell.setBorderWidthTop(0);
							    	 particularesTbl.addCell(tax1Cell); 
							    	 
					        }
					        
					        if(tax3!=0)
					        {
								String kkrstr = "Krishi Kalyan Cess "+ " @ "+" 0.5%";
				 				Phrase kkrstrPh = new Phrase(kkrstr,font10);
								PdfPCell kkrstrPhCell =new PdfPCell(kkrstrPh);
								kkrstrPhCell.setBorderWidthTop(0);
								kkrstrPhCell.setBorderWidthBottom(0);
								kkrstrPhCell.setHorizontalAlignment(Element.ALIGN_LEFT);
								kkrstrPhCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
								particularesTbl.addCell(kkrstrPhCell);
								
								 String strTax2 = ""+ df.format(tax3);
							    	Phrase words3 = new Phrase(""+ strTax2 +"",font10);
							    	PdfPCell tax2Cell = new PdfPCell(words3);
							    	tax2Cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							    	tax2Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							    	tax2Cell.setBorderWidthBottom(0);
							    	tax2Cell.setBorderWidthTop(0);
							    	 particularesTbl.addCell(tax2Cell); 
					        }
					    	}  
					      
					    	else if((invoiceentity.getSalesOrderProducts().get(i).getVatTax().getPercentage()==0)&&(invoiceentity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()>0)&&(invoiceentity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()==14.5))
					      	{
					      		System.out.println("inside st 14.5");
					      		
					      		double tax = 14 * billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
					        	double tax2 = 0.5 * billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
					        	double totaltax = tax + tax2 ;
					        	
					        	 System.out.println(tax);
						    	 System.out.println(tax2);
					    		
					      	if(tax!=0)
					      	{
					        	String str = "Service Tax "+ " @ "+" 14%";
								Phrase strp = new Phrase(str,font10);
								PdfPCell strcell =new PdfPCell(strp);
								strcell.setBorderWidthTop(0);
								strcell.setBorderWidthBottom(0);
								strcell.setHorizontalAlignment(Element.ALIGN_LEFT);
								strcell.setVerticalAlignment(Element.ALIGN_MIDDLE);
								particularesTbl.addCell(strcell);
								
					        	
					            String strTax = ""+ df.format(tax);
					    	    Phrase words1 = new Phrase(""+ strTax ,font10);
					    	   
					    	    PdfPCell tax3Cell = new PdfPCell(words1);
					    	    tax3Cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					    	    tax3Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					    	    tax3Cell.setBorderWidthBottom(0);
						    	tax3Cell.setBorderWidthTop(0);
						    	particularesTbl.addCell(tax3Cell);
								
					      	}
			
					      	if(tax2!=0){
					      	        
					      		    String str123 = "Swachha Bharat Cess"+ " @ "+" 0.5%";
								 	Phrase str123p = new Phrase(str123,font10);
									PdfPCell str123cell =new PdfPCell(str123p);
									str123cell.setBorderWidthTop(0);
									str123cell.setBorderWidthBottom(0);
									str123cell.setHorizontalAlignment(Element.ALIGN_LEFT);
									str123cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
									particularesTbl.addCell(str123cell);
									
									 String strTax1 = ""+ df.format(tax2);
									 Phrase words2 = new Phrase(""+ strTax1,font10);
									 PdfPCell tax4Cell = new PdfPCell(words2); 
									 tax4Cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
									 tax4Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
									 tax4Cell.setBorderWidthBottom(0);
									 tax4Cell.setBorderWidthTop(0);
									 particularesTbl.addCell(tax4Cell); 
									 
					      	}	
					      		////////////////////////////////////////
					    	}
					    	
					    	else if((invoiceentity.getSalesOrderProducts().get(i).getVatTax().getPercentage()!=0)&&(invoiceentity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()==0))
					    	{
					    		System.out.println("inside vat");
					    		double vat1 =invoiceentity.getSalesOrderProducts().get(i).getVatTax().getPercentage()*billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
					    	  	
					    		if(vat1!=0)
					    		{
					    		
					    	    String str = billingTaxesLis.get(i).getTaxChargeName().trim()+ " @ "+ billingTaxesLis.get(i).getTaxChargePercent();
								Phrase strp = new Phrase(str,font10);
								PdfPCell strcell =new PdfPCell(strp);
								strcell.setBorderWidthTop(0);
								strcell.setBorderWidthBottom(0);
								strcell.setHorizontalAlignment(Element.ALIGN_LEFT);
								strcell.setVerticalAlignment(Element.ALIGN_MIDDLE);
								particularesTbl.addCell(strcell);
					    		
					    		String strTax = "" + df.format(vat1) ;
					        	Phrase words1 = new Phrase(""+ strTax,font10);
					        	PdfPCell tax5Cell = new PdfPCell(words1);				
					        	tax5Cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					        	tax5Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					        	tax5Cell.setBorderWidthBottom(0);
						    	tax5Cell.setBorderWidthTop(0);
						    	particularesTbl.addCell(tax5Cell);
						    	
					    		}
//						    	 
					    	}  
					    	
					    	else  
					    	{
					    	    double amount1 =invoiceentity.getSalesOrderProducts().get(i).getPrice();
					    		Phrase proVal1 = new Phrase(""+ amount1, font10);
					    		PdfPCell valCell = new PdfPCell (proVal1);
					    		valCell.setHorizontalAlignment(Element.ALIGN_RIGHT);   
					    	    particularesTbl.addCell(valCell);
					    		 
					    	}
					    
					    ////////////////////////////////   calc end Here    ///////////////////////////////////////////// 
						 /////////////////////////////////////////////////////////////////////////
						 } 
		 			}
		 	}
		 
		 	else 
		 	{
//		 		 Phrase po = new Phrase("");
//				 if(con.getRefNo()!=null && !con.getRefNo().equals("") ){
//				  po = new Phrase("PO No : "+ con.getRefNo(),font9);
//				 }
//				 else
//				 {
//			      po = new Phrase("PO No : " ,font9);	 
//				 }
//				 PdfPCell poCell = new PdfPCell(po);
//				 poCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				 poCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//				 poCell.setBorderWidthBottom(0);
//				 poCell.setBorderWidthTop(0);
//				 particularesTbl.addCell(poCell);
//				 
//				 Phrase poblnk = new Phrase(" ",font9);
//				 PdfPCell poblnkCell = new PdfPCell(poblnk);
//				 poblnkCell.setBorderWidthBottom(0);
//				 poblnkCell.setBorderWidthTop(0);
//				 particularesTbl.addCell(poblnkCell);
		 		
		 		Phrase blnkphrse = new Phrase ("  ",font10);
				PdfPCell blnkCell =new PdfPCell();
				blnkCell.addElement(blnkphrse);
				blnkCell.setBorderWidthTop(0);    
				blnkCell.setBorderWidthBottom(0);  
				 
				 Phrase workOrder =new Phrase(""); 
				if(con.getRefNo2()!=null && !con.getRefNo2().equals("")){
				  workOrder =new Phrase("Work Order No : "+ con.getRefNo2() ,font10);    
				}
				else
				{
				  workOrder =new Phrase("Work Order No : " ,font10);    
				}
				
				PdfPCell workOrderCell = new PdfPCell(workOrder);
				workOrderCell.setBorderWidthTop(0);
				workOrderCell.setBorderWidthBottom(0);
				workOrderCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				workOrderCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				particularesTbl.addCell(workOrderCell);
				particularesTbl.addCell(blnkCell);
				
				Phrase workOrderDate =new Phrase("");
				if(con.getRefDate()!=null &&! con.getRefDate().equals("")){
				 workOrderDate =new Phrase("Work Order Date : "+ con.getRefDate(),font10);
				}
				else 
				{
				 workOrderDate =new Phrase("Work Order Date : ",font10);	
				}
				PdfPCell workOrderDateCell = new PdfPCell(workOrderDate);
				workOrderDateCell.setBorderWidthTop(0);    
				workOrderDateCell.setBorderWidthBottom(0);  
				workOrderDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				workOrderDateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				particularesTbl.addCell(workOrderDateCell);
				particularesTbl.addCell(blnkCell);
		 		
		 		for(int i=0;i<invoiceentity.getSalesOrderProducts().size();i++)
				 {
		 				
		 				 String prodName = invoiceentity.getSalesOrderProducts().get(i).getProdName();
						 Phrase prodNamephrase=new Phrase(prodName,font10);
						 PdfPCell prodNamecell=new PdfPCell(prodNamephrase);  
						 prodNamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
						 prodNamecell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						 prodNamecell.setBorderWidthTop(0);
						 prodNamecell.setBorderWidthBottom(0);
						 particularesTbl.addCell(prodNamecell);
						 
						 Phrase price = new Phrase (""+invoiceentity.getSalesOrderProducts().get(i).getPrice(),font10);	
					     PdfPCell prodPricecell=new PdfPCell(price);  
						 prodPricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						 prodPricecell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						 prodPricecell.setBorderWidthTop(0);
						 prodPricecell.setBorderWidthBottom(0);
						 particularesTbl.addCell(prodPricecell); 
				 
				 }
		 		
		 		 /************************** Commented As per Mail Confirmation Date:23/05/2017  ***********************/ 
//				Phrase custPanNo =new Phrase("Pan No : ",font10);
//				 for(int i=0;i<invoiceentity.getSalesOrderProducts().size();i++)
//				 {
//				 if(articletype.get(i).getArticleTypeName().equalsIgnoreCase("Pan No")&&articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().equalsIgnoreCase("Invoice Details"))
//				   {  
//					 	
//					 custPanNo = new Phrase("Pan No : " + articletype.get(i).getArticleTypeValue(),font10);
//	                 System.out.println("Pan No ");
//                  }
//				 }
//				 
//				PdfPCell custpanNoCell = new PdfPCell(custPanNo);
//				custpanNoCell.setBorderWidthBottom(0);    
//				custpanNoCell.setBorderWidthTop(0);
//				custpanNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				custpanNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//				particularesTbl.addCell(custpanNoCell);
//				particularesTbl.addCell(blnkCell);
		 		
		 		/*************************************** End Here **********************************/
		 		
	/////////////////////////////// Add tax  for invoice value cal ///////////////////////////////////////
						
						 Phrase blnk = new Phrase ("",font10);
						 PdfPCell blnkphrsCell = new PdfPCell(blnk);
						 blnkphrsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						 blnkphrsCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						 
						 for(int i=0;i<billingTaxesLis.size();i++)
						 {	 
							 if ( billingTaxesLis.get(i).getTaxChargeName().equals("Service Tax") && billingTaxesLis.get(i).getTaxChargePercent()!=0 )
							 {
					    	    if((invoiceentity.getSalesOrderProducts().get(i).getVatTax().getPercentage()==0)&&(invoiceentity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0)&&(invoiceentity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()==15))    
					    	         {
					    		System.out.println("3 inside st 15");
					    		/////////////////////////
					    		String str = "Service Tax "+ " @ "+" 14%";
				 				Phrase strPh = new Phrase(str,font10);
								PdfPCell strPhCell =new PdfPCell(strPh);
								strPhCell.setBorderWidthTop(0);
								strPhCell.setBorderWidthBottom(0);
								strPhCell.setHorizontalAlignment(Element.ALIGN_LEFT);
								strPhCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
								particularesTbl.addCell(strPhCell);
								
								double tax1 = 14 *billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
					        	double tax2 = 0.5 *billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
					        	double tax3 = 0.5 *billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
					        	double tax = tax1 + tax2 + tax3 ;
					        	
					        	System.out.println(tax1);
					        	System.out.println(tax2);
					        	System.out.println(tax3);   
					        	System.out.println(tax);
					          	
					            String strTax = ""+ df.format(tax1);
					    	    Phrase words1 = new Phrase(strTax,font10);
					    	    PdfPCell taxCell = new PdfPCell(words1);
					    	    taxCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					    	    taxCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					    	    taxCell.setBorderWidthBottom(0);
					    	    taxCell.setBorderWidthTop(0);
					    	    particularesTbl.addCell(taxCell);   
				 			
								String str123 = "Swachha Bharat Cess"+ " @ "+" 0.5%";
				 				Phrase str123Ph = new Phrase(str123,font10);
								PdfPCell str123PhCell =new PdfPCell(str123Ph);
								str123PhCell.setBorderWidthTop(0);
								str123PhCell.setBorderWidthBottom(0);
								str123PhCell.setHorizontalAlignment(Element.ALIGN_LEFT);
								str123PhCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
								particularesTbl.addCell(str123PhCell);
								
								  String strTax1 = ""+ df.format(tax2);
							      Phrase words2 = new Phrase(""+ strTax1 +"",font10);
							      PdfPCell tax1Cell = new PdfPCell(words2);
							      tax1Cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							      tax1Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							      tax1Cell.setBorderWidthBottom(0);
							      tax1Cell.setBorderWidthTop(0);
							      particularesTbl.addCell(tax1Cell); 
								
							      String kkrstr = "Krishi Kalyan Cess "+ " @ "+" 0.5%";
							      Phrase kkrstrPh = new Phrase(kkrstr,font10);
							      PdfPCell kkrstrPhCell =new PdfPCell(kkrstrPh);
							      kkrstrPhCell.setBorderWidthTop(0);
							      kkrstrPhCell.setBorderWidthBottom(0);
							      kkrstrPhCell.setHorizontalAlignment(Element.ALIGN_LEFT);
							      kkrstrPhCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							      particularesTbl.addCell(kkrstrPhCell);
								
								  String strTax2 = ""+ df.format(tax3);
								  Phrase words3 = new Phrase(""+ strTax2 +"",font10);
								  PdfPCell tax2Cell = new PdfPCell(words3);
								  tax2Cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
								  tax2Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
								  tax2Cell.setBorderWidthBottom(0);
								  tax2Cell.setBorderWidthTop(0);
							      particularesTbl.addCell(tax2Cell); 
								
					    	}  
					      
					    	else if((invoiceentity.getSalesOrderProducts().get(i).getVatTax().getPercentage()==0)&&(invoiceentity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()>0)&&(invoiceentity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()==14.5))
					      	{
					      		System.out.println("inside st 14.5");
					      		String str = "Service Tax "+ " @ "+" 14%";
								
								Phrase strp = new Phrase(str,font10);
								PdfPCell strcell =new PdfPCell(strp);
								strcell.setBorderWidthTop(0);
								strcell.setBorderWidthBottom(0);
								strcell.setHorizontalAlignment(Element.ALIGN_LEFT);
								strcell.setVerticalAlignment(Element.ALIGN_MIDDLE);
								particularesTbl.addCell(strcell);
								
								double tax = 14 * billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
					        	double tax2 = 0.5 * billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
					        	double totaltax = tax + tax2 ;
					        	
					            String strTax = ""+ df.format(tax);
					    	    Phrase words1 = new Phrase(""+ strTax ,font10);
					    	    System.out.println(tax);
					    	    System.out.println(tax2);
					    	    
					    	    PdfPCell tax3Cell = new PdfPCell(words1);
					    	    tax3Cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					    	    tax3Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					    	    tax3Cell.setBorderWidthBottom(0);
						    	tax3Cell.setBorderWidthTop(0);
						    	particularesTbl.addCell(tax3Cell);
								
			
								    String str123 = "Swachha Bharat Cess"+ " @ "+" 0.5%";   
								 	Phrase str123p = new Phrase(str123,font10);
									PdfPCell str123cell =new PdfPCell(str123p);
									str123cell.setBorderWidthTop(0);
									str123cell.setBorderWidthBottom(0);
									str123cell.setHorizontalAlignment(Element.ALIGN_LEFT);
									str123cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
									particularesTbl.addCell(str123cell);
									
									 String strTax1 = ""+ df.format(tax2);
									 Phrase words2 = new Phrase(""+ strTax1,font10);
									 PdfPCell tax4Cell = new PdfPCell(words2); 
									 tax4Cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
									 tax4Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
									 tax4Cell.setBorderWidthBottom(0);
									 tax4Cell.setBorderWidthTop(0);
								     particularesTbl.addCell(tax4Cell); 
									
					      		////////////////////////////////////////
					    	}
					    	
					    	else if((invoiceentity.getSalesOrderProducts().get(i).getVatTax().getPercentage()!=0)&&(invoiceentity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()==0))
					    	{
					    		System.out.println("inside vat");
					    	  	
					    	    String str = billingTaxesLis.get(i).getTaxChargeName().trim()+ " @ "+ billingTaxesLis.get(i).getTaxChargePercent();
								Phrase strp = new Phrase(str,font10);
								PdfPCell strcell =new PdfPCell(strp);
								strcell.setBorderWidthTop(0);
								strcell.setBorderWidthBottom(0);
								strcell.setHorizontalAlignment(Element.ALIGN_LEFT);
								strcell.setVerticalAlignment(Element.ALIGN_MIDDLE);
								particularesTbl.addCell(strcell);
									
					    		double vat1 =invoiceentity.getSalesOrderProducts().get(i).getVatTax().getPercentage()*billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
					    		String strTax = "" + df.format(vat1) ;
					        	Phrase words1 = new Phrase(""+ strTax,font10);
					        	
					        	PdfPCell tax5Cell = new PdfPCell(words1);				
					        	tax5Cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					        	tax5Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					        	tax5Cell.setBorderWidthBottom(0);
						    	tax5Cell.setBorderWidthTop(0);
						    	particularesTbl.addCell(tax5Cell);
//						    	 
					    	}  
					    	
					    	else  
					    	{
					    	    double amount1 =invoiceentity.getSalesOrderProducts().get(i).getPrice();
					    		Phrase proVal1 = new Phrase(""+ amount1, font10);
					    		PdfPCell valCell = new PdfPCell (proVal1);
					    		valCell.setHorizontalAlignment(Element.ALIGN_RIGHT);   
					    	    particularesTbl.addCell(valCell);
					    		 
					    	}
					    
					    ////////////////////////////////   calc end Here    ///////////////////////////////////////////// 

						 /////////////////////////////////////////////////////////////////////////
						 } 
		 			}
		 	}
		 		
		 		//********************     added tax calculation here  *******************
		 		
//		 		for (int i = 0; i < billingTaxesLis.size(); i++) {
//		 			
//	 				System.out.println("inside particular product condition ");
//
////		 			if(invoiceentity.getSalesOrderProducts().size()< 2)
//	               {
//					   
//		 				System.out.println("outside particular product condition ");
//		 				
//		 			  if((billingTaxesLis.get(i).getTaxChargeName().equals("VAT")||billingTaxesLis.get(i).getTaxChargeName().equals("CST"))&& (billingTaxesLis.get(i).getTaxChargePercent()!=0.0))
//		 		      {
//							
//							System.out.println("1st loop"+billingTaxesLis.get(i).getTaxChargeName().trim()+ " @ "+ billingTaxesLis.get(i).getTaxChargePercent());
//							
//							String str = billingTaxesLis.get(i).getTaxChargeName().trim()+ " @ "+ billingTaxesLis.get(i).getTaxChargePercent();
//							double taxAmt1 = billingTaxesLis.get(i).getTaxChargePercent()
//									* billingTaxesLis.get(i).getTaxChargeAssesVal() / 100;
//							Phrase strp = new Phrase(str,font9);
//							PdfPCell strcell =new PdfPCell(strp);
//							strcell.setBorderWidthTop(0);
//							strcell.setBorderWidthBottom(0);
//							strcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//							particularesTbl.addCell(strcell);
//							
//						}
//		 			
//		 			  else if((billingTaxesLis.get(i).getTaxChargePercent()!=0.0)&&(billingTaxesLis.get(i).getTaxChargeName().equalsIgnoreCase("Service Tax")&& billingTaxesLis.get(i).getTaxChargePercent()==15))
//		 		       {	
//		 				
//		 				String str = "Service Tax "+ " @ "+" 14%";
//		 				Phrase strPh = new Phrase(str,font9);
//						PdfPCell strPhCell =new PdfPCell(strPh);
//						strPhCell.setBorderWidthTop(0);
//						strPhCell.setBorderWidthBottom(0);
//						strPhCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//						particularesTbl.addCell(strPhCell);
//						particularsTbl.addCell(blnk);?
//		 			
//						String str123 = "Swachha Bharat Cess"+ " @ "+" 0.5%";
//		 				Phrase str123Ph = new Phrase(str123,font9);
//						PdfPCell str123PhCell =new PdfPCell(str123Ph);
//						str123PhCell.setBorderWidthTop(0);
//						str123PhCell.setBorderWidthBottom(0);
//						str123PhCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//						particularesTbl.addCell(str123PhCell);
//						particularsTbl.addCell(blnk);?
//						
//						String kkrstr = "Krishi Kalyan Cess "+ " @ "+" 0.5%";
//		 				Phrase kkrstrPh = new Phrase(kkrstr,font9);
//						PdfPCell kkrstrPhCell =new PdfPCell(kkrstrPh);
//						kkrstrPhCell.setBorderWidthTop(0);
//						kkrstrPhCell.setBorderWidthBottom(0);
//						kkrstrPhCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//						particularesTbl.addCell(kkrstrPhCell);
//						particularsTbl.addCell(blnk);?
//						
//							  
//					}
//		   		  
//					else if(((billingTaxesLis.get(i).getTaxChargePercent()!=0.0)&&(billingTaxesLis.get(i).getTaxChargeName().equalsIgnoreCase("Service Tax")&&  billingTaxesLis.get(i).getTaxChargePercent()==14.5)) )
//		 		{
//					String str = "Service Tax "+ " @ "+" 14%";
//					double taxAmt1 = 14* billingTaxesLis.get(i).getTaxChargeAssesVal() / 100;
//					
//					Phrase strp = new Phrase(str,font9);
//					PdfPCell strcell =new PdfPCell(strp);
//					strcell.setBorderWidthTop(0);
//					strcell.setBorderWidthBottom(0);
//					strcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//					particularesTbl.addCell(strcell);
//					
//
//					    String str123 = "Swachha Bharat Cess"+ " @ "+" 0.5%";   
//					 	Phrase str123p = new Phrase(str123,font9);
//						PdfPCell str123cell =new PdfPCell(str123p);
//						str123cell.setBorderWidthTop(0);
//						str123cell.setBorderWidthBottom(0);
//						str123cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//						particularesTbl.addCell(str123cell);
//						
//				  
//					}	
//		 		}
//			}
				//***********************************************
   
				PdfPTable paymentTbl=new PdfPTable(2);
		 		paymentTbl.setWidthPercentage(100);
		 		
		 		try 
		 		{
		 			paymentTbl.setWidths(new float[] {85,15});
		 			
		 		} catch (Exception e1) 
		 		 {
		 			e1.printStackTrace();
		 		 }  
		 		
		 		Phrase paymentphrase=new Phrase("Please issue the cheque in the name of  Pest Control Associates (AMB) Pvt. Ltd.",font10bold);
		 		PdfPCell paycell1= new PdfPCell(paymentphrase);
		 		paycell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		 		paycell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		 		paycell1.setBorder(0);
		 		paycell1.setBorderWidthTop(0);
		 		paymentTbl.addCell(paycell1);
				
//				Phrase blnkphrase=new Phrase("",font9bold);
//		 		PdfPCell paycell2= new PdfPCell();
//		 		paycell2.addElement(blnkphrase);
//		 		paycell2.addElement(blnkphrase);
//		 		paycell2.setBorder(0);
//		 		paycell2.setBorderWidthTop(0);
//		 		paymentTbl.addCell(paycell2);
		 		
		 		Phrase totalphrase=new Phrase("NET TOTAL",font10bold);
		 		
		 		PdfPCell paycell3= new PdfPCell(totalphrase);
		 		paycell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
		 		paycell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
		 		paycell3.setBorder(0);
		 		paymentTbl.addCell(paycell3);
		 		
		 		PdfPCell paymCell= new PdfPCell();
		 		paymCell.addElement(paymentTbl);
		 		paymCell.setBorderWidthTop(0);  
		 		
		 	    double sum = 0;
				sum = invoiceentity.getTotalBillingAmount();
		 		Phrase sumphrase=new Phrase(""+ df.format(sum),font10);
		 		
		 		PdfPCell sumcell= new PdfPCell(sumphrase);
		 		sumcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		 		sumcell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		 		
		 		particularesTbl.addCell(paymCell);
		        particularesTbl.addCell(sumcell);
		
		 		try {
		 			document.add(particularesTbl);   
		 		} catch (Exception e) {
		 			e.printStackTrace();
		 		} 
//		 }
		 		
	 	} 	
	     private static final String[] tensNames = { "", " Ten", " Twenty", " Thirty", " Fourty", " Fifty", " Sixty", " Seventy", " Eighty", " Ninety" };

	     private static final String[] numNames = { "", " One", " Two", " Three", " Four", " Five", " Six", " Seven", " Eight", " Nine", " Ten", " Eleven", " Twelve",
	     	" Thirteen", " Fourteen", " Fifteen", " Sixteen", " Seventeen", " Eighteen", " Nineteen" };
	     private static String convertLessThanOneThousand(int number) {
	     	String soFar;
	     	if (number % 100 < 20){ soFar = numNames[number % 100]; number /= 100; } else { soFar = numNames[number % 10]; number /= 10;
	     	soFar = tensNames[number % 10] + soFar; number /= 10; } if (number == 0) return soFar; return numNames[number] + " Hundred" + soFar; 
	     	}
	     public static  String convert(double number) {
	     	// 0 to 999 999 999 999
	     	if (number == 0) { return "Zero"; }
	     	String snumber = Double.toString(number);
	     	// pad with "0"
	     	String mask = "000000000000";
	     	DecimalFormat df = new DecimalFormat(mask); 
	     	snumber = df.format(number);
	     	int hyndredCrore = Integer.parseInt(snumber.substring(3,5));
	     	int hundredLakh = Integer.parseInt(snumber.substring(5,7));
	     	int hundredThousands = Integer.parseInt(snumber.substring(7,9));
	     	int thousands = Integer.parseInt(snumber.substring(9,12));
	     	String tradBillions;
	     	switch (hyndredCrore) { case 0: tradBillions = ""; break; case 1 : tradBillions = convertLessThanOneThousand(hyndredCrore) + " Crore "; break; default : tradBillions = convertLessThanOneThousand(hyndredCrore) + " Crore "; }

	     	String result = tradBillions;
	     	String tradMillions;
	     	switch (hundredLakh) { case 0: tradMillions = ""; break; case 1 : tradMillions = convertLessThanOneThousand(hundredLakh) + " Lakh "; break; default : tradMillions = convertLessThanOneThousand(hundredLakh) + " Lakh "; }
	     	result = result + tradMillions;
	     	String tradHundredThousands;

	     	switch (hundredThousands) { case 0: tradHundredThousands = ""; break; case 1 : tradHundredThousands = "One Thousand "; break; default : tradHundredThousands = convertLessThanOneThousand(hundredThousands) + " Thousand "; }
	     	result = result + tradHundredThousands;

	     	String tradThousand;
	     	tradThousand = convertLessThanOneThousand(thousands);
	     	result = result + tradThousand;return result.replaceAll("^\\s+", "").replaceAll("file://b//s%7B2,%7D//b", " "); 
	     	}
	  //*******************************
	     private void createrswords()
	     {
	 			//Ajinkya code 8\12\2016
	    		double sum = 0;
	    		double prtclrtlPrice =  invoiceentity.getTotalBillingAmount();
			    sum= sum+prtclrtlPrice;
	    		 
	    		String amtInWords=PcambInvoicepdf.convert(sum);
	    		 
	 			Phrase rupees=new Phrase("RUPEES:  ",font10bold); 
	 			Paragraph rupeesPara=new Paragraph();
	 			rupeesPara.add(rupees);  
	 			rupeesPara.add(new Chunk(amtInWords.toUpperCase()+" ONLY.",font10) );
	 			rupeesPara.setAlignment(Element.ALIGN_LEFT);  
	 			rupeesPara.add(Chunk.NEWLINE);
	 			rupeesPara.add(Chunk.NEWLINE);
	 			
	 			PdfPCell rupeesCell=new PdfPCell();
	 			rupeesCell.addElement(rupeesPara);
	 				 			
	 			PdfPTable parentTbl=new PdfPTable(1);
	 			parentTbl.setWidthPercentage(100); 
	 			
	 			parentTbl.addCell(rupeesCell);     
	    		 
	 			try {
	 				document.add(parentTbl);  
	 			} catch (Exception e) {
	 				e.printStackTrace();
	 			}  
	 	}
	     
	     //*****************************************
	     private void createfooter() 
	     {
	    	 try {
	    		 PdfPTable compNameTbl = new PdfPTable (2);
	    		 compNameTbl.setWidthPercentage(100);
	    		 
	    		 try 
	    		 {  
	    			   compNameTbl.setWidths(new float[] { 45, 55 });
	    			 
			 	  } catch (Exception e1) 
	    		  {
			 			e1.printStackTrace();
			 	  }  
	    		 
	    		 Phrase instruct1 = new Phrase ("Kindly issue all payments for this order in this name only.",font10);
	    		 PdfPCell  instruct1Cell = new PdfPCell (instruct1);
	    		 instruct1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	    		 instruct1Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	    		 instruct1Cell.setBorder(0);
	    		 instruct1Cell.setFixedHeight(15f);
	    		 compNameTbl.addCell(instruct1Cell);
	    		   
	    		 Phrase compName = new Phrase ();
	    		 compName.add(new Chunk("       "+comp.getBusinessUnitName().toUpperCase(),font10bold));
	    		
	    		 PdfPCell  compNameCell = new PdfPCell (compName);
	    		 compNameCell.setFixedHeight(15f);  
	    		 compNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	    		 compNameCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
	    		 compNameCell.setBorder(0);
	    		 compNameTbl.addCell(compNameCell);  
	    	   	 
		 			try {  
		 				document.add(compNameTbl);  
		 			} catch (Exception e) {
		 				e.printStackTrace();
		 			} 
	    		 
	    	 			// footer of Company footerlogo n details 
		 			
		 			 Font zapfdingbats = new Font();
//		 		    Font font = new Font();
		 			
	    		        Phrase blnkPhrse=new Phrase("",font10); 
	    		        PdfPCell blnkCell=new PdfPCell();
	    		        blnkCell.addElement(blnkPhrse);
	    		        blnkCell.setBorderWidthBottom(0);  
	    		        blnkCell.setBorderWidthRight(0);
	    		        blnkCell.setBorderWidthLeft(0);
	    		        
	    		        Paragraph instructionPara2=new Paragraph(); 
	    		        
	    		        instructionPara2.add(new Chunk(" Subject to Mumbai Jurisdiction only.",font10));
	    		        instructionPara2.add(Chunk.NEWLINE);
	    		        instructionPara2.add(new Chunk(" Any Taxes or Government levies will be charged Extra.",font10));
	    		        instructionPara2.add(Chunk.NEWLINE);
	    		        instructionPara2.add(new Chunk(" Interest @ 21 % p.a. will be charged, if payment is not made ",font10));
	    		        instructionPara2.add(Chunk.NEWLINE);
	    		        instructionPara2.add(new Chunk(" on demand / within 30 days of invoice being received.",font10));
	    		        instructionPara2.add(Chunk.NEWLINE);
	    		        instructionPara2.add(new Chunk(" You can directly credit the invoice amount to our bank account - ",font10));
	    		        instructionPara2.add(Chunk.NEWLINE);                                                   
	    		        instructionPara2.add(new Chunk(" HDFC Bank Limited, Fort Branch Mumbai - 400001 ",font10));
	    		        instructionPara2.add(Chunk.NEWLINE);
	    		        instructionPara2.add(new Chunk(" Current A/c No - 00602560009297 ",font10));     // here company name was removed as per mail confirmation on Date 24/05/2017
	    		        instructionPara2.add(Chunk.NEWLINE);
	    		        instructionPara2.setAlignment(Element.ALIGN_LEFT);       
	    		        
	    		        
	    		        Paragraph instructionPara1=new Paragraph();  
	    		        Chunk bullet = new Chunk("\u2022", zapfdingbats);
	    		        instructionPara1.add(bullet);
//	    		        instructionPara1.add(Chunk.NEWLINE);
	    		        instructionPara1.add(Chunk.NEWLINE);
	    		        instructionPara1.add(bullet);
//	    		        instructionPara1.add(Chunk.NEWLINE);
	    		        instructionPara1.add(Chunk.NEWLINE);
	    		        instructionPara1.add(bullet);
//	    		        instructionPara1.add(Chunk.NEWLINE);
//	    		        instructionPara1.add(Chunk.NEWLINE);
	    		        instructionPara1.add(Chunk.NEWLINE);
	    		        instructionPara1.add(Chunk.NEWLINE);
	    		        instructionPara1.add(bullet);
	    		        
	    		        
	    		        PdfPCell instruction1Cell = new PdfPCell();
	    		        instruction1Cell.addElement(instructionPara1);
	    		        instruction1Cell.setBorder(0);
	    		        
	    		        PdfPCell instruction2Cell = new PdfPCell();
	    		        instruction2Cell.addElement(instructionPara2);
	    		        instruction2Cell.setBorder(0);
                               
	    		        PdfPTable parentTbl=new PdfPTable(2);   
	    	 			parentTbl.setWidthPercentage(100); 
	    	 			
	    	 			 try {
		   	    			 
	    	 				parentTbl.setWidths(new float[] { 02, 98 });
	 	   	    			 
	 	   			 		} catch (Exception e1) {
	 	   			 			e1.printStackTrace();
	 	   			 		}  
	    	 			parentTbl.addCell(instruction1Cell);
	    	 			parentTbl.addCell(instruction2Cell);
	    	 			
	    	 			
	    	 			
	    		        PdfPTable bankDetailsTbl = new PdfPTable (2);
	    		        bankDetailsTbl.setWidthPercentage(100);
	   	    		 
	   	    		 try {
	   	    			 
	   	    			bankDetailsTbl.setWidths(new float[] { 45, 55 });
	   	    			 
	   			 		} catch (Exception e1) {
	   			 			e1.printStackTrace();
	   			 		}  
	   	    		 
//	   	    		Phrase blnk = new Phrase (" ",font9);
//	   	    		PdfPCell blnkPhraseCell = new PdfPCell(blnk);
//	   	    		blnkPhraseCell.setBorder(0);
//	   	    		blnkPhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	   	    		blnkPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	   	    		bankDetailsTbl.addCell(blnkPhraseCell);
//	   	    		
//	   	    	    Phrase companyName = new Phrase (""+comp.getBusinessUnitName(),font9);
//   	    		PdfPCell companyNameCell = new PdfPCell(companyName);
//   	    		companyNameCell.setBorder(0);
//   	    		companyNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//   	    		companyNameCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//   	    		bankDetailsTbl.addCell(companyNameCell);
	   	    		
	   	    		 Phrase instruct2 = new Phrase ("     IFSC Code-HDFC0000060 ",font10);
	   	    		 PdfPCell  instruct2Cell = new PdfPCell (instruct2);
	   	    		 instruct2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	   	    		 instruct2Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	   	    		 instruct2Cell.setBorder(0);
	   	    		 instruct2Cell.setFixedHeight(15f);
	   	    		 
	   	    		 bankDetailsTbl.addCell(instruct2Cell);
	   	    		   
	   	    		 Phrase signature = new Phrase ("                 AUTHORISED SIGNATORY",font10);
	   	    		 PdfPCell  signatureCell = new PdfPCell (signature);
	   	    		 signatureCell.setFixedHeight(15f);
	   	    		 signatureCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	   	    		 signatureCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	   	    		 signatureCell.setBorder(0);
	   	    		 bankDetailsTbl.addCell(signatureCell);      
	   	    		 
               	     ///////////////////////////////////////////////////////////////////// /                     		
	    	 			
	    	 		   PdfPCell instructionBlnkCell = new PdfPCell();
                       instructionBlnkCell.addElement(blnkPhrse);
                       instructionBlnkCell.setBorder(0);
                      
                       PdfPTable instructionPANCellTbl=new PdfPTable(2);
                       instructionPANCellTbl.setWidthPercentage(100);    
                       
		 		try {
		 			instructionPANCellTbl.setWidths(new float[] {55,45});
		 			
		 		} catch (Exception e1) {
		 			e1.printStackTrace();
		 		}
		 		
		 		  String panno = "";
	    	for(int i=0;i<this.articletype.size();i++)
	    	 {		  
	    					  if(articletype.get(i).getArticleTypeName().equalsIgnoreCase("PAN NO")&&articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().equalsIgnoreCase("Invoice Details")){  
	    					 		 
	    		                     panno = articletype.get(i).getArticleTypeValue();  
	    		  	    		         
	    					  }
	    		    	}
	                       Phrase panNoPhrase = new Phrase("Deposit T.D.S. According to the PAN NO. "+panno,font10boldred);
	                       PdfPCell pannoCell = new PdfPCell();  
                           pannoCell.addElement(panNoPhrase);
                           pannoCell.setFixedHeight(25f);
                           
                     /*********************** code updated as per mail  (Commented) *************************/      
//                           Phrase note = new Phrase ("Category cleaning services",font9);
//                           PdfPCell noteCell = new PdfPCell(note);
//                           noteCell.setBorder(0);
                           /*********************** End Here *************************/       
                           instructionPANCellTbl.addCell(pannoCell);
	    		    	   instructionPANCellTbl.addCell(instructionBlnkCell); 
//	    		    	   instructionPANCellTbl.addCell(noteCell);
                           instructionPANCellTbl.addCell(instructionBlnkCell);
	    		    	   
	    		    	
	    	 			try {    
	    	 				document.add(parentTbl);
	    	 				document.add(bankDetailsTbl); 
	    	 				document.add(instructionPANCellTbl); 
	    	 			} catch (Exception e) {
	    	 				e.printStackTrace();   
	    	 			}  
	    	 			  
	    	 		} catch (Exception e) 
	    	          {
	    	 			e.printStackTrace();
	    	 		}
	    	 	}
}
