package com.slicktechnologies.server.addhocprinting;

import javax.servlet.http.HttpServlet;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
public class TaxInvoicePdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6000320387071285941L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {
			
			String stringid = req.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			  String preprintStatus=req.getParameter("preprint");
		Invoice taxinv = ofy().load().type(Invoice.class).id(count).now();
			
		TaxInvoicePdf taxinvpdf = new TaxInvoicePdf();
			
		taxinvpdf.document = new Document();
			Document document = taxinvpdf.document;
			PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
																		
			 document.open();
			 taxinvpdf.setInvoice(count);
			 if(preprintStatus.contains("yes")){
				  
					System.out.println("yes ");
					taxinvpdf.createPdf(preprintStatus);
			  }
			  else
			  {
					System.out.println("no ");
					taxinvpdf.createPdf(preprintStatus);
			  }
			document.close();
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
}
