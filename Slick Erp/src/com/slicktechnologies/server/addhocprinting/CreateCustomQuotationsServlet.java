package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;





public class CreateCustomQuotationsServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9063027252301948954L;

	@Override
	 protected void doGet(HttpServletRequest request,
	   HttpServletResponse response) throws ServletException, IOException {
		
		 response.setContentType("application/pdf");  // Type of response , helps browser to identify the response type.
		  

		  String stringid=request.getParameter("Id");
		  stringid=stringid.trim();
		  Long count =Long.parseLong(stringid);
		  
	   	  String group=request.getParameter("group");
		   System.out.println("****************"+group);
		   
		   String status=request.getParameter("preprint");
		   System.out.println("preprint status "+status);
		   if(group.contains("Five Years"))
		   {
			   System.out.println("server in side five years ");
				  FiveYearsQuotationPdf quopdf = new FiveYearsQuotationPdf();
				  quopdf.document=new Document();
				  Document document = quopdf.document;
				  try {
					PdfWriter.getInstance(document, response.getOutputStream());
				} catch (DocumentException e) {
					e.printStackTrace();
				} 
				  document.open();
				  quopdf.setfiveyearsquotation(count);
				  quopdf.createPdf(status);
				  document.close();
		   }
		   
		   if(group.contains("Direct Quotation"))
		   {
				System.out.println("server in side Direct Quotation ");
				DirectQuotationPdf directquotation = new DirectQuotationPdf();
				directquotation.document = new Document();
				Document document = directquotation.document;
				try {
					PdfWriter.getInstance(document, response.getOutputStream());
				} catch (DocumentException e) {
					e.printStackTrace();
				} 
				document.open();
				directquotation.setdirectquotation(count,status);
				directquotation.createPdf(status);
				System.out.println("document close....");
				document.close();
		   }
		   
		   if(group.contains("Building"))
		   {
				System.out.println("server in side Building ");
				BuildingQuotationPdf buildingquot = new BuildingQuotationPdf();
				buildingquot.document = new Document();
				Document document = buildingquot.document;
				try {
					PdfWriter.getInstance(document, response.getOutputStream());
				} catch (DocumentException e) {
					e.printStackTrace();
				} 
				document.open();
				buildingquot.setbuildingquotation(count);
				buildingquot.createPdf(status);
				document.close();
		   }
		   if(group.contains("After Inspection"))
		   {
				System.out.println("server in side After Inspection ");
				InspectionQuotationPdf inspectionpdf = new InspectionQuotationPdf();
				inspectionpdf.document = new Document();
				Document document = inspectionpdf.document;
				try {
					PdfWriter.getInstance(document, response.getOutputStream());
				} catch (DocumentException e) {
					e.printStackTrace();
				} 
				document.open();
				inspectionpdf.setinspectionquotation(count);
				inspectionpdf.createPdf(status);
				document.close();
		   }
		
		   
	}
}
