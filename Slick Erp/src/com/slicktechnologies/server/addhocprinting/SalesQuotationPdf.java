package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;

public class SalesQuotationPdf {
	
	Sales qp;
	List<SalesLineItem> products;
	List<PaymentTerms> payTermsLis;
	List<ProductOtherCharges> prodCharges;
	List<ProductOtherCharges> prodTaxes;
	Customer cust;
	Company comp;
	SalesOrder soEntity;
	public  Document document;
	public GenricServiceImpl impl;
	private  Font font16boldul,font12bold,font8bold,font8,font12boldul,font12;
	private SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");
	DecimalFormat df=new DecimalFormat("#.00");
	
	final static String disclaimerText="I/We hereby certify that my/our registration certificate"
			+ " Under the Maharashtra Value Added Tax Act 2002, is in force on the date on which the sale "
			+ "of the goods specified in this Invoice is made by me/us and that the transaction of sales "
			+ "covered by this Invoice has been effected by me/us and it shall be accounted for in the turnover "
			+ "of sales while filling of return and the due tax, if any, payable on the sale has been paid or shall be paid.";
	
	
	public 	SalesQuotationPdf()
	{
		font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD| Font.UNDERLINE);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
		font8= new Font(Font.FontFamily.HELVETICA,8);
		font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12=new Font(Font.FontFamily.HELVETICA,12);
		impl= new GenricServiceImpl();
	}
	//PLASE TO APPLY COMPANY SPECIFEC SETTINGS
	public void getSalesOrder(long count)
	{
		//Load Contract 
	   
		qp=ofy().load().type(SalesOrder.class).id(count).now();
		int contractCount=qp.getCount();
	   //Load Customer
		if(qp.getCompanyId()!=null)
	      cust=ofy().load().type(Customer.class).filter("count",qp.getCustomerId()).
	        filter("companyId",qp.getCompanyId()).first().now();
		if(qp.getCompanyId()==null)
		      cust=ofy().load().type(Customer.class).filter("count",qp.getCustomerId()).first().now();
		
		
		if(qp.getCompanyId()!=null)
			 comp=ofy().load().type(Company.class).filter("companyId",qp.getCompanyId()).first().now();
		else
			comp=ofy().load().type(Company.class).first().now();
		
		if(qp.getCompanyId()!=null)
			soEntity=ofy().load().type(SalesOrder.class).filter("companyId",qp.getCompanyId()).first().now();
		else
			soEntity=ofy().load().type(SalesOrder.class).first().now();
	   
		products=qp.getItems();
		prodCharges=qp.getProductCharges();
		prodTaxes=qp.getProductTaxes();
		payTermsLis=qp.getPaymentTermsList();
		
	}
	
	public void getSalesQuotation(long count)
	{
		//Load Contract 
		
			qp=ofy().load().type(SalesQuotation.class).id(count).now();
			int contractCount=qp.getCount();
			List<Service>services=null;
			
		   //Load Customer
			
			if(qp.getCompanyId()!=null)
		      cust=ofy().load().type(Customer.class).filter("count",qp.getCustomerId()).
		        filter("companyId",qp.getCompanyId()).first().now();
			if(qp.getCompanyId()==null)
			      cust=ofy().load().type(Customer.class).filter("count",qp.getCustomerId()).first().now();
			
			if(qp.getCompanyId()!=null)
				 comp=ofy().load().type(Company.class).filter("companyId",qp.getCompanyId()).first().now();
			else
				comp=ofy().load().type(Company.class).first().now();
		   
			products=qp.getItems();
			prodCharges=qp.getProductCharges();
			prodTaxes=qp.getProductTaxes();
			payTermsLis=qp.getPaymentTermsList();
	}
	
	public  void createPdf() {
		 
		 JobCardPdf.showLogo(document, comp);    
		createCompanyHedding();
	      createCompanyAdress();
	    //  addType();
	      
	      try {
			createHeaderTable();
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	      ceateComments();
	      createPaymentTerms();
	      createProductDetail();
	      createProductTaxes();
	      createProductCharges();
	      addFooter();	  
	      //createDefaultText();
	    }
	
	//patch patch patch
	public  void createPdfForEmail(Company comp,Customer cust ,Sales qp) {
		 
	      this.comp=comp;
	      this.qp=qp;
	      this.cust=cust;
	      this.products=qp.getItems();
		  createCompanyHedding();
	      createCompanyAdress();
	      // addType();
	      
	      try {
			createHeaderTable();
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
	      ceateComments();
	      createProductDetail();
	    }
	public  void createCompanyHedding()
	{
		Phrase companyName= new Phrase(comp.getBusinessUnitName(),font16boldul);
	     Paragraph p =new Paragraph();
	    
	     p.add(Chunk.NEWLINE);
	     p.add(companyName);
	     p.setAlignment(Element.ALIGN_CENTER);
	     try {
			document.add(p);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	public  void createCompanyAdress()
	{
		Phrase adressline1= new Phrase(comp.getAddress().getAddrLine1(),font12);
		Phrase adressline2=null;
		if(comp.getAddress().getAddrLine2()!=null)
		{
			adressline2= new Phrase(comp.getAddress().getAddrLine2(),font12);
		}
		
		Phrase landmark=null;
		Phrase locality=null;
		if(comp.getAddress().getLandmark()!=null&&comp.getAddress().getLocality().equals("")==false)
		{
			
			String landmarks=comp.getAddress().getLandmark()+" , ";
			locality= new Phrase(landmarks+comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin(),font12);
		}
		else
		{
			locality= new Phrase(comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin(),font12);
		}
		Paragraph adressPragraph=new Paragraph();
		adressPragraph.add(adressline1);
		adressPragraph.add(Chunk.NEWLINE);
		if(adressline2!=null)
		{
				adressPragraph.add(adressline2);
				adressPragraph.add(Chunk.NEWLINE);
		}
		
		adressPragraph.add(locality);
		adressPragraph.setAlignment(Element.ALIGN_CENTER);
		adressPragraph.setSpacingBefore(10);
		adressPragraph.setSpacingAfter(10);
		
		
		// Phrase for phone,landline ,fax and email
		
		Phrase titlecell=new Phrase("Mob :",font8bold);
		Phrase titleTele = new Phrase("Tele :",font8bold);
		Phrase titleemail= new Phrase("Email :",font8bold);
		Phrase titlefax= new Phrase("Fax :",font8bold);
		
		Phrase titleservicetax=new Phrase("Service Tax No     :",font8bold);
		Phrase titlevatatx=new Phrase("Vat Tax No :    ",font8bold);
		String servicetax=comp.getServiceTaxNo();
		String vatttax=comp.getVatTaxNo();
		Phrase servicetaxphrase=null;
		Phrase vattaxphrase=null;
		
		if(servicetax!=null&&servicetax.trim().equals("")==false)
		{
			servicetaxphrase=new Phrase(servicetax,font8);
		}

		if(vatttax!=null&&vatttax.trim().equals("")==false)
		{
			vattaxphrase=new Phrase(vatttax,font8);
		}
		
		// cell number logic
		String stringcell1=comp.getContact().get(0).getCellNo1()+"";
		String stringcell2=null;
		Phrase mob=null;
		if(comp.getContact().get(0).getCellNo2()!=-1)
			stringcell2=comp.getContact().get(0).getCellNo2()+"";
		if(stringcell2!=null)
		    mob=new Phrase(stringcell1+" / "+stringcell2,font8);
		else
			 mob=new Phrase(stringcell1,font8);
		
		
		//LANDLINE LOGIC
		Phrase landline=null;
		if(comp.getContact().get(0).getLandline()!=-1)
			landline=new Phrase(comp.getContact().get(0).getLandline()+"",font8);
		
		// fax logic
		Phrase fax=null;
		if(comp.getContact().get(0).getFaxNo()!=null)
			fax=new Phrase(comp.getContact().get(0).getFaxNo()+"",font8);
		// email logic
		Phrase email= new Phrase(comp.getContact().get(0).getEmail(),font8);
		
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(titlecell);
		adressPragraph.add(mob);
		adressPragraph.add(new Chunk("            "));
		
		if(landline!=null)
		{
			adressPragraph.add(titleTele);
			adressPragraph.add(landline);
			adressPragraph.add(new Chunk("            "));
		}
		
		if(fax!=null)
		{
			adressPragraph.add(titlefax);
			adressPragraph.add(fax);
			adressPragraph.add(Chunk.NEWLINE);
		}
		
		adressPragraph.add(titleemail);
		adressPragraph.add(new Chunk("            "));
		adressPragraph.add(email);
		adressPragraph.add(Chunk.NEWLINE);
		
		if(servicetaxphrase!=null)
		{
			adressPragraph.add(titleservicetax);
			adressPragraph.add(servicetaxphrase);
		}
		
		if(vattaxphrase!=null)
		{
			adressPragraph.add(titlevatatx);
			adressPragraph.add(vattaxphrase);
			adressPragraph.add(new Chunk("            "));
		}
		
		adressPragraph.add(Chunk.NEWLINE);
		String title="";
		if(qp instanceof SalesOrder)
			title="Sales Order";
		else
			title="Quotation";
		
		Phrase Ptitle= new Phrase(title,font12bold);
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(Ptitle);
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(Chunk.NEWLINE);
		
		try {
			document.add(adressPragraph);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	public  void createHeaderTable() throws DocumentException
	{
		 Phrase nameTitle = new Phrase("Name :",font8bold);
		 Phrase phoneTitle= new Phrase("Phone :",font8bold);
		 Phrase adressTitle= new Phrase("Address :",font8bold);
		 Phrase emailTitle= new Phrase("Email :",font8bold);
		 
		 PdfPCell titlecustnameCell=new PdfPCell();
		 titlecustnameCell.addElement(nameTitle);
		 titlecustnameCell.setBorder(0);
		 PdfPCell titlecustPhonecell = new PdfPCell();
		 titlecustPhonecell.addElement(phoneTitle);
		 titlecustPhonecell.setBorder(0);
		 PdfPCell titleadressCell = new PdfPCell();
		 titleadressCell.addElement(adressTitle);
		 titleadressCell.setBorder(0);
		 PdfPCell titleemailCell = new PdfPCell(emailTitle);
		 titleemailCell.addElement(emailTitle);
		 titleemailCell.setBorder(0);
		
		 String fullname=cust.getFullname();
		
		Phrase customername = new Phrase(fullname,font8);
		Phrase phonechunk = new Phrase(cust.getCellNumber1()+"",font8);
		Phrase emailchunk = new Phrase(cust.getEmail()+"",font8);
		
		Phrase blankphrase = new Phrase("  ");
		PdfPCell blankcell = new PdfPCell(blankphrase);
		
		Phrase customeradress= new Phrase(cust.getAdress().getAddrLine1(),font8);
		Phrase customeradress2=null;
		//Patch
		if(cust.getAdress().getAddrLine2()!=null)
		   customeradress2= new Phrase(cust.getAdress().getAddrLine2(),font8);
		Phrase landmark=null;
		Phrase locality= null;
		
		if(cust.getAdress().getLandmark()!=null)
		{
		    landmark = new Phrase(cust.getAdress().getLandmark());
		    locality=new Phrase(landmark+" "+cust.getAdress().getLocality(),font8);
		}
		else
			locality=new Phrase(cust.getAdress().getLocality(),font8);
		
		Phrase cityState=new Phrase(cust.getAdress().getCity()
				+" "+cust.getAdress().getState(),font8);
		Phrase country=new Phrase("  "+cust.getAdress().getCountry(),font8);
		Phrase pin=new Phrase("  "+cust.getAdress().getPin()+"",font8);
		 
		
		Paragraph adresspara = new Paragraph();
		 adresspara.add(customeradress);
		 adresspara.add(Chunk.NEWLINE);
		 if(customeradress2!=null)
			 adresspara.add(customeradress2);
		 adresspara.add(locality);
		 adresspara.add("  ");
		 adresspara.add(cityState);
		 adresspara.add("   ");
		 adresspara.add(pin);
		 adresspara.add(Chunk.NEWLINE);
		 adresspara.add(country);
		
		 
		 PdfPCell custnameCell=new PdfPCell();
		 custnameCell.addElement(customername);
		 custnameCell.setBorder(0);
		 PdfPCell custPhonecell = new PdfPCell();
		 custPhonecell.addElement(phonechunk);
		 custPhonecell.setBorder(0);
		 PdfPCell adressCell = new PdfPCell();
		 adressCell.addElement(adresspara);
		 adressCell.setColspan(3);
		 adressCell.setBorder(0);
		 PdfPCell emailCell = new PdfPCell(emailTitle);
		 emailCell.addElement(emailchunk);
		 emailCell.setColspan(0);
		 emailCell.setBorder(0);
		
		String header;
		header = "ID :";
		
		Phrase billno = new Phrase(header,font8bold);
		Phrase validuntildate= new Phrase("Valid Until :",font8bold);
		Phrase branch = new Phrase("Branch :",font8bold);
		Phrase deliverydate = new Phrase("Delivery Date :",font8bold);
		Phrase salesperson = new Phrase("Sales Person :",font8bold);
		Phrase cellnosalesperson = new Phrase("Contact Number :",font8bold);
		Phrase status = new Phrase("Status :",font8bold);
		
		 PdfPCell titlebillCell=new PdfPCell();
		 titlebillCell.addElement(billno);
		 titlebillCell.setBorder(0);
		 PdfPCell titlevaliduntilcell = new PdfPCell();
		 titlevaliduntilcell.addElement(validuntildate);
		 titlevaliduntilcell.setBorder(0);
		 PdfPCell branchCell = new PdfPCell();
		 branchCell.addElement(branch);
		 branchCell.setBorder(0);
		 PdfPCell deliverydatecell = new PdfPCell();
		 deliverydatecell.addElement(deliverydate);
		 deliverydatecell.setBorder(0);
		 PdfPCell salespersoncell = new PdfPCell();
		 salespersoncell.addElement(salesperson);
		 salespersoncell.setBorder(0);
		 PdfPCell phoneno = new PdfPCell();
		 phoneno.addElement(cellnosalesperson);
		 phoneno.setBorder(0);
		 PdfPCell statuscell = new PdfPCell();
		 statuscell.addElement(status);
		 statuscell.setBorder(0);
		 
		Phrase realqno=new Phrase(qp.getCount()+"",font8);
		
		String validdate="";
		SalesQuotation salesquot=(SalesQuotation) qp;
		validdate=fmt.format(salesquot.getValidUntill());
		Phrase realvaliduntildate= new Phrase(validdate+"",font8);
		realvaliduntildate.trimToSize();
		
		String branchstr="";
		if(qp instanceof SalesQuotation){
			SalesQuotation quot=(SalesQuotation) qp;
			branchstr=quot.getBranch();
		}
		if(qp instanceof SalesOrder){
			SalesOrder so=(SalesOrder)qp;
			branchstr=so.getBranch();
		}
		
		Phrase realbranchphrase=new Phrase(branchstr,font8);
		
		String delivrydate="";
		if(qp instanceof SalesQuotation){
			SalesQuotation quot=(SalesQuotation) qp;
			delivrydate=fmt.format(quot.getDeliveryDate());
		}
		else{
			SalesOrder so=(SalesOrder)qp;
			delivrydate=fmt.format(so.getDeliveryDate());
		}
		
		Phrase realdelivrydatephrase=new Phrase(delivrydate,font8);
		
		String salesemployee="";
		String contactno="";
		if(qp instanceof SalesQuotation){
			SalesQuotation quot=(SalesQuotation) qp;
			salesemployee=quot.getEmployee();
			contactno="9898657676";
		}
		
		if(qp instanceof SalesOrder){
			SalesOrder so=(SalesOrder) qp;
			salesemployee=so.getEmployee();
			contactno="9898657676";
		}
		Phrase realsalesempphrase=new Phrase(salesemployee,font8);
		Phrase realcellnophrase=new Phrase(contactno,font8);
		
		String statusval="";
		if(qp instanceof SalesQuotation){
			SalesQuotation quot=(SalesQuotation) qp;
			statusval=quot.getStatus();
		}
		
		if(qp instanceof SalesOrder){
			SalesOrder so=(SalesOrder) qp;
			statusval=so.getStatus();
		}
		Phrase realstatus=new Phrase(statusval,font8);
		
		PdfPCell billCell=new PdfPCell();
		billCell.addElement(realqno);
		billCell.setBorder(0);
		PdfPCell validuntilcell = new PdfPCell();
		validuntilcell.addElement(realvaliduntildate);
		validuntilcell.setBorder(0);
		PdfPCell realbranchcell = new PdfPCell();
		realbranchcell.addElement(realbranchphrase);
		realbranchcell.setBorder(0);
		PdfPCell realdeliverydatecell = new PdfPCell();
		realdeliverydatecell.addElement(realdelivrydatephrase);
		realdeliverydatecell.setBorder(0);
		PdfPCell realsalespersoncell = new PdfPCell();
		realsalespersoncell.addElement(realsalesempphrase);
		realsalespersoncell.setBorder(0);
		PdfPCell realcontactcell = new PdfPCell();
		realcontactcell.addElement(realcellnophrase);
		realcontactcell.setBorder(0);
		PdfPCell realstatuscell = new PdfPCell();
		realstatuscell.addElement(realstatus);
		realstatuscell.setBorder(0);
		
		 PdfPTable table = new PdfPTable(4);
		 
		 // 1 st row name phone
		 table.addCell(titlecustnameCell);
		 table.addCell(custnameCell);
		 
		 table.addCell(titlecustPhonecell);
		 table.addCell(custPhonecell);
		 
		 // 2nd row adress and email
		 table.addCell(titleadressCell);
		 table.addCell(adressCell);
		 table.addCell(titleemailCell);
		 table.addCell(emailCell);
		 
		table.setWidths(new float[]{18,32,18,32});
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.setWidthPercentage(100);
		 
		PdfPTable billtable = new PdfPTable(4);
		billtable.setWidths(new float[]{20,30,20,30});
		billtable.setWidthPercentage(100);
		billtable.setHorizontalAlignment(Element.ALIGN_LEFT);

		//1  row bill number and date
		 billtable.addCell(titlebillCell);
		 billtable.addCell(billCell);
		 billtable.addCell(titlevaliduntilcell);
		 billtable.addCell(validuntilcell);
		 
		 billtable.addCell(branchCell);
		 billtable.addCell(realbranchcell);
		 
		 billtable.addCell(deliverydatecell);
		 billtable.addCell(realdeliverydatecell);
		 
		 billtable.addCell(salespersoncell);
		 billtable.addCell(realsalespersoncell);
		 billtable.addCell(phoneno);
		 billtable.addCell(realcontactcell);
		 billtable.addCell(statuscell);
		 billtable.addCell(realstatuscell);
		/* for(int i=0;i<2;i++)
		 {
			 PdfPRow temp=table.getRow(i);
			PdfPCell[] cells= temp.getCells();
			for(PdfPCell cell:cells)
				cell.setBorder(Rectangle.NO_BORDER);
		}*/
		 
//		 for(int i=0;i<4;i++)
//		 {
//			 PdfPRow temp=billtable.getRow(i);
//			PdfPCell[] cells= temp.getCells();
//			for(PdfPCell cell:cells){
//				cell.setBorder(Rectangle.NO_BORDER);
//			}
//		}
//		 
		PdfPTable parenttable= new PdfPTable(2);
		parenttable.setWidthPercentage(100);
		parenttable.setWidths(new float[]{50,50});
		
		PdfPCell custinfocell = new PdfPCell();
		PdfPCell  billinfocell = new PdfPCell();
		
		custinfocell.addElement(table);
		billinfocell.addElement(billtable);
		parenttable.addCell(custinfocell);
		parenttable.addCell(billinfocell);
	
		document.add(parenttable);
	}
	
	
	public  void ceateComments()
	{
		Font font1 = new Font(Font.FontFamily.HELVETICA  , 8,Font.BOLD);
		Font font2 = new Font(Font.FontFamily.HELVETICA  , 9,Font.BOLD| Font.UNDERLINE);
		Chunk termsAndCondition= new Chunk("Terms And Conditions :",font2);
		Chunk commentChunk= new Chunk("Description :",font1);
		
		Chunk deschunk = null;
		if(qp.getDescription()!=null)
			deschunk= new Chunk(qp.getDescription(),font8);
	
		Paragraph para = new Paragraph();
		para.add(Chunk.NEWLINE);
		para.add(termsAndCondition);
		para.add(Chunk.NEWLINE);
		para.add(commentChunk);
		para.add(Chunk.NEWLINE);
		if(deschunk!=null)
				para.add(deschunk);
		para.add(Chunk.NEWLINE);
		try {
			document.add(para);
			document.add(Chunk.NEWLINE);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	
	public  void createProductDetail()
	{
		Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
		Phrase productdetails= new Phrase("Product Details",font1);
		Paragraph para = new Paragraph();
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_CENTER);
			
		PdfPTable table = new PdfPTable(8);
		table.setWidthPercentage(100);
		Phrase category = new Phrase("Category ",font1);
        Phrase product = new Phrase("Product ",font1);
        Phrase qty = new Phrase("Quantity",font1);
        Phrase rate= new Phrase ("Rate",font1);
        Phrase servicetax = new Phrase("Service Tax",font1);
        Phrase vat = new Phrase("VAT",font1);
        Phrase percdisc = new Phrase("% Discount",font1);
        Phrase total = new Phrase("Total",font1);
		
		 PdfPCell cellcategory = new PdfPCell(category);
         PdfPCell cellproduct = new PdfPCell(product);
         PdfPCell cellqty = new PdfPCell(qty);
         PdfPCell cellrate = new PdfPCell(rate);
         PdfPCell cellservicetax = new PdfPCell(servicetax);
         PdfPCell cellvat = new PdfPCell(vat);
         PdfPCell cellperdisc = new PdfPCell(percdisc);
         PdfPCell celltotal= new PdfPCell(total);
         
         table.addCell(cellcategory);
         table.addCell(cellproduct);
         table.addCell(cellqty);
         table.addCell(cellrate);
         table.addCell(cellservicetax);
         table.addCell(cellvat);
         table.addCell(cellperdisc);
         table.addCell(celltotal);
         
         for(int i=0;i<this.products.size();i++)
         {
        	 Phrase chunk=new Phrase(products.get(i).getProductCategory(),font8);
        	 PdfPCell pdfcategcell = new PdfPCell(chunk);
        	 
        	 chunk = new Phrase(products.get(i).getProductName(),font8);
        	 PdfPCell pdfnamecell = new PdfPCell(chunk);
        	 
        	 chunk = new Phrase(products.get(i).getQty()+"",font8);
        	 PdfPCell pdfqtycell = new PdfPCell(chunk);
        	 
        	 double taxVal=removeAllTaxes(products.get(i).getPrduct());
        	 double calculatedRate=products.get(i).getPrice()-taxVal;
        	 chunk = new Phrase(df.format(calculatedRate),font8);
        	 PdfPCell pdfspricecell = new PdfPCell(chunk);
        	 
        	 if(products.get(i).getServiceTax()!=null)
        	    chunk = new Phrase(products.get(i).getServiceTax().getPercentage()+"",font8);
        	 else
        		 chunk = new Phrase("N.A"+"",font8);
        	 
        	 PdfPCell pdfservice = new PdfPCell(chunk);
        	 
        	 if(products.get(i).getVatTax()!=null)
        	    chunk = new Phrase(products.get(i).getVatTax().getPercentage()+"",font8);
        	 else
        		 chunk = new Phrase("N.A"+"",font8);
        	 PdfPCell pdfvattax = new PdfPCell(chunk);
        	 
        	 if(products.get(i).getPercentageDiscount()!=0)
         	    chunk = new Phrase(products.get(i).getPercentageDiscount()+"",font8);
         	 else
         		 chunk = new Phrase("0"+"",font8);
         	 PdfPCell pdfperdiscount = new PdfPCell(chunk);
        	
         	 double totalVal=(products.get(i).getPrice()-taxVal)*products.get(i).getQty();
         	 if(products.get(i).getPercentageDiscount()!=0){
         		 totalVal=totalVal-(totalVal*products.get(i).getPercentageDiscount()/100);
         	 }
        	 chunk = new Phrase(df.format(totalVal),font8);
        	 PdfPCell pdftotalproduct = new PdfPCell(chunk);
        	 
        	 table.addCell(pdfcategcell);
        	 table.addCell(pdfnamecell);
        	 table.addCell(pdfqtycell);
        	 table.addCell(pdfspricecell);
        	 table.addCell(pdfservice);
        	 table.addCell(pdfvattax);
        	 table.addCell(pdfperdiscount);
        	 table.addCell(pdftotalproduct);
        	}
        
         
         try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
         
      }

   
	public  void createPaymentFooter()
	{
	
		SalesQuotation quot=null;
		SalesOrder cont=null;
		if(qp instanceof SalesOrder)
			cont=(SalesOrder) qp;
		
		else
			quot=(SalesQuotation) qp;
		
		SalesQuotation quotation;
		 if(quot!=null)
		     quotation=quot;
		 else
			 quotation=cont;
		 int row=4;
		 PdfPTable table = new PdfPTable(2);
		 table.setWidthPercentage(35);
		
		 
		 Phrase phrasetotal =        new Phrase("Total   :",font8bold);
		 Phrase phraseperdiscount =  new Phrase("% Discount    :",font8bold);
		 Phrase phraseflatdiscount = new Phrase("Flat Discount :",font8bold);
		 Phrase phrasefinaltotal =   new Phrase("Net Payable   :",font8bold);
		 Phrase paidamt =   new Phrase("Amount Paid   :",font8bold);
		 Phrase balance =   new Phrase("Balance   :",font8bold);
		 
		
		 Phrase realdiscount, realflatdiscount,finaltotal;
		 //Calculate Discount in AMT
		 
		
		 Phrase amtpaid = null,amtbal = null;
		//Patch Patch
		
		if(cont!=null)
		
		{
			//amtpaid=new Phrase(cont.getTotalAmount()-cont.getbalance()+"",font8);
			//amtbal=new Phrase(cont.getbalance()+"",font8);
		}
		
		 Paragraph par = new Paragraph();
		 par.setSpacingBefore(40);
		 table.setHorizontalAlignment(Element.ALIGN_RIGHT);
		 
		 table.setSpacingBefore(20);
		 
		 System.out.println("Row value is "+row);
		 row=table.getRows().size();
		 for(int i=0;i<row;i++)
		 {
			 PdfPRow temp=table.getRow(i);
			PdfPCell[] cells= temp.getCells();
			for(PdfPCell cell:cells)
			{
				cell.setBorder(Rectangle.NO_BORDER);
			System.out.println("ajjj");
			}
		}
		 
		 try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	public  void createPaymentTerms()
	{
		Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
			
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(70);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		Phrase paytermdays = new Phrase("Days",font1);
		Phrase paytermpercent = new Phrase("Percent",font1);
        Phrase paytermcomment = new Phrase("Comment",font1);
      
	    PdfPCell celldays = new PdfPCell(paytermdays);
		PdfPCell cellpercent = new PdfPCell(paytermpercent);
	    PdfPCell cellcomment = new PdfPCell(paytermcomment);
		
        table.addCell(celldays);
        table.addCell(cellpercent);
        table.addCell(cellcomment);
       
     
       for(int i=0;i<this.payTermsLis.size();i++)
       {
      	 Phrase chunk=new Phrase(payTermsLis.get(i).getPayTermDays()+"",font8);
      	 PdfPCell pdfdayscell = new PdfPCell(chunk);
      	 
      	 chunk=new Phrase(df.format(payTermsLis.get(i).getPayTermPercent())+"",font8);
      	 PdfPCell pdfpercentcell = new PdfPCell(chunk);
      	 
      	 chunk = new Phrase(payTermsLis.get(i).getPayTermComment().trim(),font8);
      	 PdfPCell pdfcommentcell = new PdfPCell(chunk);

      	 table.addCell(pdfdayscell);
      	 table.addCell(pdfpercentcell);
      	 table.addCell(pdfcommentcell);
      	}
       
       try {
			document.add(table);
			document.add(Chunk.NEWLINE);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
    }

	
	public  void createProductTaxes()
	{
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8|Font.BOLD);
			
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(35);
		
		Phrase totalAmtPhrase=new Phrase("Total Amount   "+"",font1);
		PdfPCell totalAmtCell=new PdfPCell(totalAmtPhrase);
		totalAmtCell.setBorder(0);
		double totalAmt=0;
		if(qp instanceof SalesQuotation){
			totalAmt=((SalesQuotation) qp).getTotalAmount();
		}
		if(qp instanceof SalesOrder){
			totalAmt=((SalesOrder) qp).getTotalAmount();
		}
		
		Phrase realtotalAmt=new Phrase(df.format(totalAmt),font1);
		PdfPCell realtotalAmtCell=new PdfPCell(realtotalAmt);
		realtotalAmtCell.setBorder(0);
		table.addCell(totalAmtCell);
  		table.addCell(realtotalAmtCell);
		
       for(int i=0;i<this.prodTaxes.size();i++)
       {
	    	Phrase chunk = new Phrase(prodTaxes.get(i).getChargeName()+" @ "+prodTaxes.get(i).getChargePercent(),font1);
	        PdfPCell pdfservicecell = new PdfPCell(chunk);
	        pdfservicecell.setBorder(0);

	        double taxAmt=prodTaxes.get(i).getChargePercent()*prodTaxes.get(i).getAssessableAmount()/100;
	        chunk=new Phrase(df.format(taxAmt),font1);
	      	PdfPCell pdftaxamtcell = new PdfPCell(chunk);
	      	pdftaxamtcell.setBorder(0);
	      	
	      	table.addCell(pdfservicecell);
	      	table.addCell(pdftaxamtcell);
	      	table.setHorizontalAlignment(Element.ALIGN_RIGHT);
      	}
       
       try {
    	   document.add(Chunk.NEWLINE);
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
    }

	public  void createProductCharges()
	{
		Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
			
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(35);
		
       for(int i=0;i<this.prodCharges.size();i++)
       {
    	   Phrase chunk = null;
    	   double chargeAmt=0;
    	   PdfPCell pdfchargeamtcell = null;
    	   PdfPCell pdfchargecell = null;
    	   if(prodCharges.get(i).getChargePercent()!=0){
    		   chunk = new Phrase(prodCharges.get(i).getChargeName()+" @ "+prodCharges.get(i).getChargePercent(),font1);
    		   pdfchargecell=new PdfPCell(chunk);
    		   chargeAmt=prodCharges.get(i).getChargePercent()*prodCharges.get(i).getAssessableAmount()/100;
    		   chunk=new Phrase(df.format(chargeAmt),font8);
   	      	   pdfchargeamtcell = new PdfPCell(chunk);
   	      	   pdfchargeamtcell.setBorder(0);
    	   }
    	   if(prodCharges.get(i).getChargeAbsValue()!=0){
    		   chunk = new Phrase(prodCharges.get(i).getChargeName()+"",font1);
    		   pdfchargecell=new PdfPCell(chunk);
    		   chargeAmt=prodCharges.get(i).getChargeAbsValue();
    		   chunk=new Phrase(chargeAmt+"",font8);
   	      	   pdfchargeamtcell = new PdfPCell(chunk);
   	      	   pdfchargeamtcell.setBorder(0);
    	   }
  	       pdfchargecell.setBorder(0);
  	       
	    	table.addCell(pdfchargecell);
	      	table.addCell(pdfchargeamtcell);
	      	table.setHorizontalAlignment(Element.ALIGN_RIGHT);
	      	
	      	System.out.println("======I  "+i+"   size"+prodCharges.size());
	      	if(i==prodCharges.size()-1){
	      		chunk = new Phrase("Net Payable",font1);
		        PdfPCell pdfnetpaycell = new PdfPCell(chunk);
		        pdfnetpaycell.setBorder(0);
		      	double netPayAmt=0;
		    	if(qp instanceof SalesQuotation){
		    		netPayAmt=((SalesQuotation) qp).getNetpayable();
		    	}
		    	if(qp instanceof SalesOrder){
		    		netPayAmt=((SalesOrder) qp).getNetpayable();
		    	}
		      	chunk=new Phrase(netPayAmt+"",font8);
		        PdfPCell pdfnetpayamt = new PdfPCell(chunk);
		        pdfnetpayamt.setBorder(0);
		        
		        table.addCell(pdfnetpaycell);
		        table.addCell(pdfnetpayamt);
	      	}
      	}
       
       try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
    }

	
	
	public void addtaxparagraph()
	{
		Paragraph taxpara= new Paragraph();
		Phrase titleservicetax=new Phrase("Service Tax No     :",font8bold);
		Phrase titlevatatx=new Phrase("Vat Tax No :    ",font8bold);
		Phrase titlelbttax=new Phrase("LBT Tax No :    ",font8bold);
		Phrase space = new Phrase("									");
		String serv = null,lbt,vat = null;
		if(comp.getServiceTaxNo()==null)
			serv="";
		else
			serv=comp.getServiceTaxNo();
		if(comp.getVatTaxNo()==null)
			vat="";
		else
			vat=comp.getVatTaxNo();
		
		if(serv.equals("")==false)
		{
			taxpara.add(titleservicetax);
			Phrase servp= new Phrase(serv,font8);
			taxpara.add(servp);
		}
		taxpara.add(space);
		
		if(vat.equals("")==false)
		{
			taxpara.add("                ");
			taxpara.add(titlevatatx);
			Phrase vatpp= new Phrase(vat,font8);
			taxpara.add(vatpp);
		}  	 
		
		try {
			taxpara.setSpacingAfter(10);
			document.add(taxpara);
			
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void addType()
	{
		Phrase header=null;
		if(qp instanceof SalesOrder)
			header= new Phrase("Invoice",font12boldul);
			
		else
			header= new Phrase("Quotation",font12boldul);
		
		Paragraph para= new Paragraph(header);
		para.setAlignment(Element.ALIGN_CENTER);
		para.setSpacingBefore(10);
		para.setSpacingAfter(20);
		try {
			document.add(para);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public void addFooter()
	{
		 Phrase recieverSinature = new Phrase("",font12boldul);
		 Phrase blankphrase = new Phrase(" ");
		 Phrase blankphrase1 = new Phrase(" ");
		 Phrase blankphrase2 = new Phrase(" ");
		 Phrase companyname= new Phrase("For "+comp.getBusinessUnitName(),font12bold);
		 Phrase authorizedsignatory=null;
	     authorizedsignatory = new Phrase("AUTHORISED SIGNATORY"+"",font12boldul);
		 PdfPTable table = new PdfPTable(2);
		 try {
			table.setWidths(new float[]{50,50});
			table.setSpacingBefore(60);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 table.addCell(recieverSinature);
		 table.addCell(companyname);
		 table.addCell(blankphrase);
		 table.addCell(blankphrase1);
		 table.addCell(blankphrase2);
		 table.addCell(authorizedsignatory);
		 
		 table.setWidthPercentage(100);
		 
		 for(int i=0;i<3;i++)
		 {
			 PdfPRow temp=table.getRow(i);
			 
			PdfPCell[] cells= temp.getCells();
			for(PdfPCell cell:cells)
				cell.setBorder(Rectangle.NO_BORDER);
		}
		 
		
	
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public double removeAllTaxes(SuperProduct entity)
	{
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;

		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
			retrVat=entity.getPrice()/(1+(vat/100));
			retrVat=entity.getPrice()-retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=entity.getPrice()/(1+service/100);
			retrServ=entity.getPrice()-retrServ;
		}
		if(service!=0&&vat!=0){
			double taxPerc=service+vat;
			retrServ=entity.getPrice()/(1+taxPerc/100);
			retrServ=entity.getPrice()-retrServ;
		}
		tax=retrVat+retrServ;
		return tax;
	}
	
	public void createDefaultText()
	{
		PdfPTable disclaimrtable = new PdfPTable(1);
		disclaimrtable.setWidthPercentage(100);
		
		PdfPTable companyinfotable=new PdfPTable(2);
		companyinfotable.setWidthPercentage(60);
		companyinfotable.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase discaimertextphrase=new Phrase(disclaimerText,font8);
		PdfPCell realtext=new PdfPCell(discaimertextphrase);
		realtext.setBorder(0);
		disclaimrtable.addCell(realtext);
		
		
		Phrase serviceTaxNr=new Phrase("Service Tax Nr  ",font8);
		Phrase vatTaxNr=new Phrase("VAT Registration Nr  ",font8);
		Phrase licenseNr=new Phrase("License Nr  ",font8);
		Phrase pancardTaxNr=new Phrase("PAN No  ",font8);
		
		PdfPCell serviceTaxCell=new PdfPCell(serviceTaxNr);
		serviceTaxCell.setBorder(0);
		PdfPCell vatRegNrCell=new PdfPCell(vatTaxNr);
		vatRegNrCell.setBorder(0);
		PdfPCell PANNoCell=new PdfPCell(pancardTaxNr);
		PANNoCell.setBorder(0);
		PdfPCell licenseNrCell=new PdfPCell(licenseNr);
		licenseNrCell.setBorder(0);
		
		Phrase realservicetax=new Phrase(comp.getServiceTaxNo()+"",font8);
		Phrase realvattax=new Phrase(comp.getVatTaxNo()+"",font8);
		Phrase realpanno=new Phrase(comp.getPanCard()+"",font8);
		Phrase reallicenseno=new Phrase(comp.getLicense()+"",font8);
		
		PdfPCell realserviceTaxCell=new PdfPCell(realservicetax);
		realserviceTaxCell.setBorder(0);
		PdfPCell realvatRegNrCell=new PdfPCell(realvattax);
		realvatRegNrCell.setBorder(0);
		PdfPCell realPANNoCell=new PdfPCell(realpanno);
		realPANNoCell.setBorder(0);
		PdfPCell reallicenseNrCell=new PdfPCell(reallicenseno);
		reallicenseNrCell.setBorder(0);
		
		
		
		if(comp.getServiceTaxNo()!=null){
			companyinfotable.addCell(serviceTaxCell);
			companyinfotable.addCell(realserviceTaxCell);
		}
		if(comp.getVatTaxNo()!=null){
			companyinfotable.addCell(vatRegNrCell);
			companyinfotable.addCell(realvatRegNrCell);
		}
		if(comp.getPanCard()!=null){
			companyinfotable.addCell(PANNoCell);
			companyinfotable.addCell(realPANNoCell);
		}
		if(comp.getLicense()!=null){
			companyinfotable.addCell(licenseNrCell);
			companyinfotable.addCell(reallicenseNrCell);
		}
		
		try {
			document.add(Chunk.NEWLINE);
			document.add(companyinfotable);
			document.add(disclaimrtable);
			
			
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		
		
	}

	
}
