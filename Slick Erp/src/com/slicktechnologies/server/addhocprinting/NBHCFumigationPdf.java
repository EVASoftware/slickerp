package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

public class NBHCFumigationPdf {

	
	public Document document;
	
	Service service;
	Company comp;
	private Font font16boldul, font12bold, font8bold, font8, font9bold,
	font11bold, font12boldul, font12, font10bold, font10, font14bold,
	font9;
	List<ServiceProject> projectlist = new ArrayList<ServiceProject>();
	SimpleDateFormat fmt =new SimpleDateFormat("dd-MM-yyyy");
	
	float[] columnWidths = {1.2f, 0.3f, 8.0f};
	float[] columnWidths1 = {1.2f, 0.3f, 1.5f, 1.5f, 0.3f, 1.5f,1.5f,0.3f,1.5f};
	/**30-10-2017 sagar sore []**/
//	float[] columnWidths2 = {1.4f,4.1f,0.9f,1.0f,1.3f,1.8f,1.0f};
	float[] columnWidths2 = {1.1f,4.7f,0.8f,0.9f,1.5f,1.7f,0.8f};
	float[] columnWidths3 = {2.5f,1.5f,2.0f,2.0f,1.5f};
	
	/**
	 * Ajinkya added this below table to display 2 copies of Pdf on Same A4 size Page On Date: 10/08/2017
	 */
	 PdfPTable a5ParentTable = new PdfPTable(2);
	
	 PdfPTable a5tbl1 = new PdfPTable(1);
	 PdfPTable a5tbl2 = new PdfPTable(1);
	 
	 /**
	  * End Here
	  */
	 
	 /*
		 * Date :21-01-2018
		 * @author Ashwini
		 */
		Boolean rowflag = false;
		Boolean headerFooter = false ;
		ProcessConfiguration processconfig;
		
		/*
		 * end by Ashwini
		 */
	
	public NBHCFumigationPdf() {

//		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
//		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
//		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
//		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
//		font8 = new Font(Font.FontFamily.HELVETICA, 8);
//		font9 = new Font(Font.FontFamily.HELVETICA, 9);
//		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
//		font12 = new Font(Font.FontFamily.HELVETICA, 12);
//		font11bold = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);
//		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
//		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
//		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
//		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
//		
//		
		
		/**
		 * Date 27/11/2017
		 * By jayshree 
		 * Font size increses by one
		 */
		new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 17, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);//checking font 6 by Ajinkya
		font8 = new Font(Font.FontFamily.HELVETICA, 6);//checking font 6 by Ajinkya
		font9 = new Font(Font.FontFamily.HELVETICA, 10);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD);
		font12 = new Font(Font.FontFamily.HELVETICA, 13);
		font11bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font10bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);//checking font 8 by Ajinkya
		font10 = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL);   //checking font 8 by Ajinkya
		font14bold = new Font(Font.FontFamily.HELVETICA, 15, Font.BOLD);
		font9bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		
		//End by Jayshree
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}
	
	public void getfumigation(Long count)
	
	{
		
		service = ofy().load().type(Service.class).id(count).now();
		
		if (service.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", service.getCompanyId()).first().now();
		
		if (service.getCompanyId() == null)
			projectlist = ofy().load().type(ServiceProject.class).filter("serviceId",service.getCount()).list();
			else
			projectlist = ofy().load().type(ServiceProject.class).filter("companyId", service.getCompanyId()).filter("serviceId",service.getCount()).list();
			
			System.out.println("service list size "+projectlist.size());
			
			if(service.getCompanyId()!=null){
				processconfig = ofy().load().type(ProcessConfiguration.class)
						.filter("companyId", service.getCompanyId())
						.filter("processName", "Service")
						.filter("configStatus", true).first().now();
				if(processconfig!=null){
					for(int k= 0 ;k<processconfig.getProcessList().size() ; k++){
						
						if (processconfig.getProcessList().get(k).getProcessType()
								.trim().equalsIgnoreCase("DecreaseTableRow")
								&& processconfig.getProcessList().get(k).isStatus() == true) {
							rowflag = true;
							System.out.println("rowflag:::::");
						}
						
						if (processconfig.getProcessList().get(k).getProcessType()
								.trim().equalsIgnoreCase("HeaderFooter")
								&& processconfig.getProcessList().get(k).isStatus() == true) {
							headerFooter = true;
							
						}
					}
				}
				
				
			}
			
			/*
			 * end by Ashwini
			 */
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	}

	public void createPdf(String preePrint)
	{
		/**
		 * Date 27/11/2017
		 * Dev.By jayshree
		 * Des.To remove the header comment this line
		 */
		
		if(headerFooter){
			
			if(comp.getUploadHeader()!=null && preePrint.equalsIgnoreCase("no")){
				
				/**
				 * Date 06-03-2019 by vijay
				 */
				Paragraph blank = new Paragraph();
				blank.add(Chunk.NEWLINE);
				try {
					document.add(blank);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/**
				 * ends here
				 */
				createCompanyNameAsHeader(document , comp);
				}
		}
		
		
		if(preePrint.equalsIgnoreCase("yes")){
			createBlankforUPC();
		}
		
		
//		createBlankForLetterHead();
		//End By Jayshree
		createserviceFumigationDetails();
		createFumigationnDetails();
		createTableFiveColumns();
		createTable();
		feedbackFromCustomer();
		employeeAndCustomerSign();
		
		/**
		 * Date 27/11/2017
		 * Dev.By jayshree
		 * Des.To remove the header comment this line
		 */
		
		if(headerFooter){
			
			if(comp.getUploadFooter()!=null  && preePrint.equalsIgnoreCase("no")){
				createCompanyNameAsFooter(document,comp);
			}
		}
		
		
		//End By Jayshree
		addToParentTbl();
//		createCompanyDetails();
	}
	
	
	/*private void createCompanyNameAsHeader(Document doc, Company comp) {
		
		DocumentUpload document =comp.getUploadHeader();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
		//	Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			Image image2 = Image.getInstance("images/NBHC_Header.jpg");
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(270f);
			image2.setAbsolutePosition(40f,725f);	
			*//*** 30-10-2017 sagar sore [To show header two time on vertically divided page]**//*
			//		doc.add(image2);
			PdfPCell imageCell = new PdfPCell();
			imageCell.addElement(image2);
			imageCell.setBorder(0);
					a5tbl1.addCell(imageCell);
					a5tbl2.addCell(imageCell);
					
				  *//** end **//*
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		
		
//		try
//		{
//		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE .jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,725f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}
	
	private void createCompanyNameAsFooter(Document doc, Company comp) {
		
		
		DocumentUpload document =comp.getUploadFooter();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
		//	Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			Image image2 = Image.getInstance("images/NBHC_Footer.jpg");
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(270f);
			image2.setAbsolutePosition(5f,40f);	
		//	Image image3 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			Image image3 = Image.getInstance("images/NBHC_Footer.jpg");
			image3.scalePercent(15f);
			image3.scaleAbsoluteWidth(270f);
			image3.setAbsolutePosition(320f,40f);
			*//*** 30-10-2017 sagar sore [To show header two time on vertically divided page]**//*
			//		doc.add(image2);
//					a5tbl1.addCell(image2);
//					a5tbl2.addCell(image2);
			doc.add(image2);
			doc.add(image3);
				  *//** end **//*		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		try
//		{
//		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,40f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}*/
	
	private void createBlankForLetterHead(){
		
		a5tbl1.setWidthPercentage(100);
		a5tbl2.setWidthPercentage(100);
		
		System.out.println("Inside create blank heading");
		Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
	  
		PdfPCell paraCell = new PdfPCell(blank);
		paraCell.setBorder(0);
		
		a5tbl1.addCell(paraCell);
		a5tbl2.addCell(paraCell); 

	    PdfPCell blcell = new PdfPCell(blank);
	    blcell.setBorder(0);
	    blcell.setBorder(0);

		PdfPTable bltable = new PdfPTable(1);
		bltable.addCell(blcell);
		bltable.addCell(blcell);
		bltable.setWidthPercentage(100f);
		
		PdfPCell blTableCell = new PdfPCell();
		blTableCell.setBorder(0);
		blTableCell.addElement(bltable);

		System.out.println("BLANK CELL ADDED IN DOCUMENT...");

	}

	private void createFumigationnDetails() {
		a5tbl1.setWidthPercentage(100);
		
		a5tbl2.setWidthPercentage(100);
		

		Paragraph para = new Paragraph("Fumigant Details",font10bold);
		para.setAlignment(Element.ALIGN_CENTER);
		
		Phrase blnkPhrse = new Phrase(" ",font8);
		PdfPCell blnkCell = new PdfPCell(blnkPhrse);
		blnkCell.setBorder(0);
		
		PdfPCell paracell = new PdfPCell();
		paracell.addElement(para);
		paracell.setBorder(0);
		paracell.setHorizontalAlignment(Element.ALIGN_CENTER);
		paracell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
//		a5tbl1.addCell(blnkCell);//Date 27/11/2017 By jayshree comment this line to remove space
		a5tbl1.addCell(paracell);
		
//		a5tbl2.addCell(blnkCell);//Date 27/11/2017 By jayshree comment this line to remove space
		a5tbl2.addCell(paracell);
		
//		try {
//			document.add(Chunk.NEWLINE);
//			document.add(para);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
		
		
		PdfPTable table = new PdfPTable(7);
		table.setWidthPercentage(100f);
//		table.setSpacingBefore(5f);//Date 27/11/2017 By Jayshree Change the spacing 10 to 5
		
		
		try {
			table.setWidths(columnWidths2);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		Phrase fumigantUsed =new Phrase("Fumigant Used",font8bold);
		PdfPCell fumigantUsedCell = new PdfPCell(fumigantUsed);
		fumigantUsedCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(fumigantUsedCell);
		
		Phrase packType =new Phrase("Pack Type",font8bold);
		PdfPCell packTypeCell = new PdfPCell(packType);
		packTypeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(packTypeCell);
		
		Phrase noUsed =new Phrase("No. Used",font8bold);
		PdfPCell noUsedCell = new PdfPCell(noUsed);
		noUsedCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(noUsedCell);
		
		Phrase qtyUsed =new Phrase("Qty. Used",font8bold);
		PdfPCell qtyUsedCell  = new PdfPCell(qtyUsed);
		qtyUsedCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(qtyUsedCell);
		
		Phrase manufacturer  =new Phrase("Manufacturer",font8bold);
		PdfPCell manufacturerCell  = new PdfPCell(manufacturer);
		manufacturerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(manufacturerCell);
		
		
		Phrase manufacturerDate  =new Phrase("Date of Manufacturer",font8bold);
		PdfPCell manufacturerDateCell  = new PdfPCell(manufacturerDate);
		manufacturerDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(manufacturerDateCell);
		
		Phrase batchNo  =new Phrase("Batch No",font8bold);
		PdfPCell batchNoCell  = new PdfPCell(batchNo);
		batchNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(batchNoCell);
		
		
		Phrase blank1 = new Phrase(" ",font8);
		PdfPCell blank1Cell = new PdfPCell(blank1);
		blank1Cell.setBorder(0);
		
		PdfPCell blank2Cell = new PdfPCell(blank1);
		
		Phrase serviceALP = new Phrase("[ ] ALP",font8);
		PdfPCell serviceTypeCell = new PdfPCell(serviceALP);
		serviceTypeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase servicePackType = new Phrase("[ ] 3gm Tablets  [ ] 10 gm Pouch  [ ] 34 gm Pouch",font8);
		PdfPCell servicePackTypeCell = new PdfPCell(servicePackType);
		
		servicePackTypeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable gmTbale = new PdfPTable(2);
		gmTbale.setWidthPercentage(100);
		gmTbale.setSpacingAfter(4f);
		
		Phrase myblank = new Phrase();
		PdfPCell myblankCell = new PdfPCell(myblank);
		myblankCell.setBorderWidthLeft(0);
		myblankCell.setBorderWidthRight(0);
		myblankCell.setBorderWidthTop(0);
		gmTbale.addCell(myblankCell);
		
		Phrase gm = new Phrase("gm",font8);
		PdfPCell gmCell = new PdfPCell(gm);
		gmCell.setBorder(0);
		
		gmTbale.addCell(gmCell);
		
		
		PdfPCell gmColumnCell = new PdfPCell(gmTbale);
	
	
	table.addCell(serviceTypeCell);
	table.addCell(servicePackTypeCell);
	table.addCell(blank2Cell);
	table.addCell(gmColumnCell);
	table.addCell(blank2Cell);
	table.addCell(blank2Cell);
	table.addCell(blank2Cell);
	
	
	
	Phrase serviceMBR = new Phrase("[ ] MBR",font8);
	PdfPCell serviceMBRCell = new PdfPCell(serviceMBR);
	serviceMBRCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase serviceMBRPackType = new Phrase("[ ] 1 lb Can  [ ] 1.5 lb Can   [ ] Cylinder",font8);
	PdfPCell serviceMBRPackTypeCell = new PdfPCell(serviceMBRPackType);
	serviceMBRPackTypeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	
	table.addCell(serviceMBRCell);
	table.addCell(serviceMBRPackTypeCell);
	table.addCell(blank2Cell);
	table.addCell(gmColumnCell);
	table.addCell(blank2Cell);
	table.addCell(blank2Cell);
	table.addCell(blank2Cell);
	
	
	//   rohan commented this code and write new code for static data for NBHC requirment
//		if(projectlist.size()==0){
//			table.addCell(serviceTypeCell);
//			table.addCell(blank2Cell);
//			table.addCell(blank2Cell);
//			table.addCell(gmColumnCell);
//			table.addCell(blank2Cell);
//			table.addCell(blank2Cell);
//			table.addCell(blank2Cell);
//		}
//		else
//		{
//			for (int i = 0; i < projectlist.size(); i++) {
//				
//				System.out.println("project list size"+ projectlist.get(i).getProdDetailsList().size());
//				for (int j = 0; j < projectlist.get(i).getProdDetailsList().size(); j++) {
//					
//					Phrase type = new Phrase(projectlist.get(i).getProdDetailsList().get(j).getName(),font8);
//					PdfPCell typeCell = new PdfPCell(type);
//					typeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//					
//					table.addCell(serviceTypeCell);
//					table.addCell(typeCell);
//					table.addCell(blank2Cell);
//					table.addCell(gmColumnCell);
//					table.addCell(blank2Cell);
//					table.addCell(blank2Cell);
//					table.addCell(blank2Cell);
//				}
//			}
//		}
		PdfPCell tableCell = new PdfPCell();
		tableCell.setBorder(0);
		tableCell.addElement(table);
		
		a5tbl1.addCell(tableCell);
		a5tbl2.addCell(tableCell);
		
		
//		try {
//			document.add(table);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
		
	}
	
	
	private void createBlankforUPC() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		
		try {
			document.add(blank);
			document.add(blank);

		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}


	private void createserviceFumigationDetails() {
	
		a5tbl1.setWidthPercentage(100);
		a5tbl2.setWidthPercentage(100);
		
		Paragraph para = new Paragraph("WPM/Container Fumigation Report",font10bold);
		para.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell paraCell = new PdfPCell(para);
		paraCell.setBorder(0);
		paraCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		paraCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		
		a5tbl1.addCell(paraCell);
		a5tbl2.addCell(paraCell);
		
		
//		try {
//			document.add(para);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
	
		
		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100f);
		parentTable.setSpacingBefore(15f);//Date 27/11/2017 By jayshree comment this line to remove space
		
		
		
		PdfPTable table1 =new PdfPTable(9);
		table1.setWidthPercentage(100f);
		try {
			table1.setWidths(columnWidths1);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		Phrase dotted = new Phrase(" : ",font8bold);
		PdfPCell dottedCell = new PdfPCell(dotted);
		dottedCell.setBorder(0);
		
		Phrase srno = new Phrase("Serial No",font8bold);
		PdfPCell srnoCell = new PdfPCell(srno);
		srnoCell.setBorder(0);
		table1.addCell(srnoCell);
		table1.addCell(dottedCell);
		
		Phrase srnoValue = new Phrase(service.getCount()+"",font8);
		PdfPCell srnoValueCell = new PdfPCell(srnoValue);
		srnoValueCell.setBorder(0);
		table1.addCell(srnoValueCell);
		
		
		
		Phrase branch = new Phrase("Branch",font8);
		PdfPCell branchCell = new PdfPCell(branch);
		branchCell.setBorder(0);
		branchCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table1.addCell(branchCell);
		table1.addCell(dottedCell);
		
		Phrase branchValue = new Phrase(service.getBranch(),font8);
		PdfPCell branchValueCell = new PdfPCell(branchValue);
		branchValueCell.setBorder(0);
		table1.addCell(branchValueCell);
		
		
		Phrase date = new Phrase("Date",font8);
		PdfPCell dateCell = new PdfPCell(date);
		dateCell.setBorder(0);
		dateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table1.addCell(dateCell);
		table1.addCell(dottedCell);
		
		Phrase dateValue = new Phrase(fmt.format(service.getServiceDate()),font8);
		PdfPCell dateValueCell = new PdfPCell(dateValue);
		dateValueCell.setBorder(0);
		table1.addCell(dateValueCell);
		
		PdfPCell table1Cell = new PdfPCell(table1);
		table1Cell.setBorder(0);
		
		
		//    for customer name 
		PdfPTable table2 = new PdfPTable(3);
		table2.setWidthPercentage(100f);
		try {
			table2.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		
		Phrase custName = new Phrase("Customer", font8bold);
		PdfPCell custNameCell = new PdfPCell(custName);
		custNameCell.setBorder(0);
		table2.addCell(custNameCell);
		table2.addCell(dottedCell);
		
		Phrase custNameValue = new Phrase(service.getCustomerName(),font8);
		PdfPCell custNameValueCell = new PdfPCell(custNameValue);
		custNameValueCell.setBorder(0);
		table2.addCell(custNameValueCell);
		
		Phrase siteAddress = new Phrase("Site Address", font8bold);
		PdfPCell siteAddressCell = new PdfPCell(siteAddress);
		siteAddressCell.setBorder(0);
		table2.addCell(siteAddressCell);
		table2.addCell(dottedCell);
		
		String custAdd1="";
		String custFullAdd1="";
		
	if(service.getAddress()!=null){
			
			if(!service.getAddress().getAddrLine2().equals("")){
				if(!service.getAddress().getLandmark().equals("")){
					custAdd1=service.getAddress().getAddrLine1()+","+service.getAddress().getAddrLine2()+","+service.getAddress().getLandmark();
				}else{
					custAdd1=service.getAddress().getAddrLine1()+","+service.getAddress().getAddrLine2();
				}
			}else{
				if(!service.getAddress().getLandmark().equals("")){
					custAdd1=service.getAddress().getAddrLine1()+","+service.getAddress().getLandmark();
				}else{
					custAdd1=service.getAddress().getAddrLine1();
				}
			}
			
			if(!service.getAddress().getLocality().equals("")){
				custFullAdd1=custAdd1+","+service.getAddress().getLocality()+","+service.getAddress().getCity()+"-"+service.getAddress().getPin()+","+service.getAddress().getState()+","+service.getAddress().getCountry();
						
			}else{
				custFullAdd1=custAdd1+","+service.getAddress().getCity()+"-"+service.getAddress().getPin()+","+service.getAddress().getState()+","+service.getAddress().getCountry();
			}
		}
		
		Phrase siteAddressValue = new Phrase(custFullAdd1,font8);
		PdfPCell siteAddressValueCell = new PdfPCell(siteAddressValue);
		siteAddressValueCell.setBorder(0);
		table2.addCell(siteAddressValueCell);
		
		PdfPCell table2Cell = new PdfPCell(table2);
		table2Cell.setBorder(0);
		
		
		
		
		PdfPTable table3= new PdfPTable(2);
		table3.setWidthPercentage(100f);
		
		
		Phrase fumiagtionTyp= new Phrase("Fumigation Type            [ ] WPM              [ ] Container",font8);
		PdfPCell fumiagtionTypCell = new PdfPCell(fumiagtionTyp);
		fumiagtionTypCell.setBorder(0);
		table3.addCell(fumiagtionTypCell);
		
		
		Phrase fromDtTodt= new Phrase(" Fumigation Start Date :"+fmt.format(service.getContractStartDate())+"   End Date :"+fmt.format(service.getContractEndDate()),font8);
		PdfPCell fromDtTodtCell = new PdfPCell(fromDtTodt);
		fromDtTodtCell.setBorder(0);
		table3.addCell(fromDtTodtCell);
		
		PdfPCell table3Cell = new PdfPCell(table3);
		table3Cell.setBorder(0);
		
		
		parentTable.addCell(table1Cell);
		parentTable.addCell(table2Cell);
		parentTable.addCell(table3Cell);
		
		PdfPCell parentTblCell = new PdfPCell();
		parentTblCell.setBorder(0);
		parentTblCell.addElement(parentTable);
		
		a5tbl1.addCell(parentTblCell);
		a5tbl2.addCell(parentTblCell);
		
		
//		try {
//			document.add(parentTable);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
		
		
	}

	private void createTableFiveColumns() {
		
		a5tbl1.setWidthPercentage(100);
		a5tbl2.setWidthPercentage(100);
		
		Paragraph  para= new Paragraph("WPM Fumigation Details" , font10bold);
		para.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell paraCell = new PdfPCell(para);
		paraCell.setBorder(0);
		paraCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		paraCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		a5tbl1.addCell(paraCell);
		a5tbl2.addCell(paraCell);
		
//		try {
//			document.add(para);
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
		
		PdfPTable myTable = new PdfPTable(2);
		myTable.setWidthPercentage(100f);
//		myTable.setSpacingBefore(10f);//Date 27/11/2017 By jayshree comment this line to remove space
		
		Phrase myPh = new Phrase("Type of WPM               [ ] Pallets            [ ] Boxes           [ ] Other",font8);
		PdfPCell myphCell = new PdfPCell(myPh);
		myphCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		myphCell.setBorder(0);
		myTable.addCell(myphCell);
		
		Phrase myPhBlank = new Phrase(" ",font8);
		PdfPCell myPhBlankCell = new PdfPCell(myPhBlank);
		myPhBlankCell.setBorderWidthLeft(0);
		myPhBlankCell.setBorderWidthRight(0);
		myPhBlankCell.setBorderWidthTop(0);
		myTable.addCell(myPhBlankCell);
		
		PdfPTable table = new PdfPTable(5);
		table.setWidthPercentage(100f);
//		table.setSpacingBefore(10f);//Date 27/11/2017 By jayshree comment this line to remove space
		
		try {
			table.setWidths(columnWidths3);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		Phrase Volume = new Phrase("Volume Fumigated [Cu.M.]",font8bold);
		PdfPCell VolumeCell = new PdfPCell(Volume);
		VolumeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase dosage = new Phrase("Dosage gm.",font8bold);
		PdfPCell dosageCell = new PdfPCell(dosage);
		dosageCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase fumigantQty = new Phrase("Fumigant Quantity [gm.]",font8bold);
		PdfPCell fumigantQtyCell = new PdfPCell(fumigantQty);
		fumigantQtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase destination = new Phrase("Destination",font8bold);
		PdfPCell destinationCell = new PdfPCell(destination);
		destinationCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase remarks = new Phrase("Remarks",font8bold);
		PdfPCell remarksCell = new PdfPCell(remarks);
		remarksCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		table.addCell(VolumeCell);
		table.addCell(dosageCell);
		table.addCell(fumigantQtyCell);
		table.addCell(destinationCell);
		table.addCell(remarksCell);
		
		
		for(int i=0; i<15;i++)
		{
			Phrase blank = new Phrase(" ",font8);
			PdfPCell blankCell = new PdfPCell(blank);
			table.addCell(blankCell);
		}
		
		PdfPCell myTblCell = new PdfPCell();
		myTblCell.setBorder(0);
		myTblCell.addElement(myTable);
		
		PdfPCell tblCell = new PdfPCell();
		tblCell.setBorder(0);
		tblCell.addElement(table);
		
		a5tbl1.addCell(myTblCell);
		a5tbl1.addCell(tblCell);
		
		a5tbl2.addCell(myTblCell);
		a5tbl2.addCell(tblCell);
		
		
//		try {
//			document.add(myTable);
//			document.add(table);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
	}

	private void createTable() {
		
		a5tbl1.setWidthPercentage(100);
		a5tbl2.setWidthPercentage(100);
		
		Paragraph  para= new Paragraph("Container Fumigation Details" , font10bold);
		para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell paraCell = new PdfPCell(para);      //////////////////// Check this working OR Not
		paraCell.setBorder(0);
		paraCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		paraCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		a5tbl1.addCell(paraCell);
		a5tbl2.addCell(paraCell);
		
//		try {
//			document.add(para);
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
		
		PdfPTable parentTable = new PdfPTable(4);
		parentTable.setWidthPercentage(100f);
//		parentTable.setSpacingBefore(10f);//Date 27/11/2017 By jayshree comment this line to remove space
		
		Phrase fumigation =new Phrase("Fumigation details :" ,font8);
		PdfPCell fumigationCell = new PdfPCell(fumigation);
		fumigationCell.setBorder(0);
		parentTable.addCell(fumigationCell);
		
//		Phrase alp = new Phrase("[ ] ALP (No. of Tablets/Sachets) ",font8);
//		PdfPCell alpCell = new PdfPCell(alp);
//		alpCell.setBorder(0);
//		parentTable.addCell(alpCell);
		
//		Phrase mbr = new Phrase("[ ] Methyl Bromide (gm./Kg.)",font8);
//		PdfPCell mbrCell = new PdfPCell(mbr);
//		mbrCell.setBorder(0);
//		mbrCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		parentTable.addCell(mbrCell);
		
		Phrase threeSideBlack = new Phrase(" ", font8);//By Jayshree
		PdfPCell threeSideBlackCell = new PdfPCell(threeSideBlack);
		threeSideBlackCell.setBorder(0);
//		threeSideBlackCell.setBorderWidthLeft(0);
//		threeSideBlackCell.setBorderWidthRight(0);
//		threeSideBlackCell.setBorderWidthTop(0);
//		threeSideBlackCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		parentTable.addCell(threeSideBlackCell);
		parentTable.addCell(threeSideBlackCell);
		parentTable.addCell(threeSideBlackCell);
		
		
		
		PdfPTable table = new PdfPTable(6);
		table.setWidthPercentage(100f);
//		table.setSpacingBefore(10f);//Date 27/11/2017 By jayshree comment this line to remove space
		
		Phrase containerNo = new Phrase("Container No.",font8bold);
		PdfPCell containerNoCell = new PdfPCell(containerNo);
		containerNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase container = new Phrase("Container Size (Ft.)",font8bold);
		PdfPCell containerCell = new PdfPCell(container);
		containerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase commodity = new Phrase("Commodity",font8bold);
		PdfPCell commodityCell = new PdfPCell(commodity);
		commodityCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase dosage = new Phrase("Dosage gm",font8bold);
		PdfPCell dosageCell = new PdfPCell(dosage);
		dosageCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase destination = new Phrase("Destination",font8bold);
		PdfPCell destinationCell = new PdfPCell(destination);
		destinationCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase remarks = new Phrase("Remarks",font8bold);
		PdfPCell remarksCell = new PdfPCell(remarks);
		remarksCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		table.addCell(containerNoCell);
		table.addCell(containerCell);
		table.addCell(commodityCell);
		table.addCell(dosageCell);
		table.addCell(destinationCell);
		table.addCell(remarksCell);
		
		/*
		 * Date:24-01-2018
		 * @author Ashwini
		 */
		if(rowflag){
			for(int i=0; i<24;i++)  {
				System.out.println("inside if in rowflag ");
				{
					Phrase blank = new Phrase(" ",font8);//By Jayshree add Font size
					PdfPCell blankCell = new PdfPCell(blank);
					table.addCell(blankCell);
				}
				
			}
			
		}else{
			for(int i=0; i<36;i++)  
				
			{
				Phrase blank = new Phrase(" ",font8);//By Jayshree add Font size
				PdfPCell blankCell = new PdfPCell(blank);
				table.addCell(blankCell);
			}
		}
		
	
		
		
		PdfPCell parentTblCell = new PdfPCell();
		parentTblCell.setBorder(0);
		parentTblCell.addElement(parentTable);
		
		PdfPCell tblCell = new PdfPCell();
		tblCell.setBorder(0);
		tblCell.addElement(table);
		
		a5tbl1.addCell(parentTblCell);
		a5tbl1.addCell(tblCell);
		
		a5tbl2.addCell(parentTblCell);
		a5tbl2.addCell(tblCell);
		
		
//		try {
//			document.add(parentTable);
//			document.add(table);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
		
	}
	
	

	private void createCompanyDetails() {


		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);
		
		Phrase reg = new Phrase("Registered Office: ", font10bold);
		PdfPCell regCell = new PdfPCell(reg);
		regCell.setBorder(0);
		regCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(regCell);
		
		
		Phrase name = new Phrase(comp.getBusinessUnitName(), font10bold);
		PdfPCell nameCell = new PdfPCell(name);
		nameCell.setBorder(0);
		nameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(nameCell);

		Phrase hardcoded = new Phrase(
				"Division: Commodity Care and Pest Management", font8bold);
		PdfPCell hardcodedCell = new PdfPCell(hardcoded);
		hardcodedCell.setBorder(0);
		hardcodedCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(hardcodedCell);

		// / address *************

		String custAdd1 = "";
		String custFullAdd1 = "";

		if (comp.getAddress() != null) {

			if (!comp.getAddress().getAddrLine2().equals("")) {
				if (!comp.getAddress().getLandmark().equals("")) {
					custAdd1 = comp.getAddress().getAddrLine1() + ","
							+ comp.getAddress().getAddrLine2() + ","
							+ comp.getAddress().getLandmark();
				} else {
					custAdd1 = comp.getAddress().getAddrLine1() + ","
							+ comp.getAddress().getAddrLine2();
				}
			} else {
				if (!comp.getAddress().getLandmark().equals("")) {
					custAdd1 = comp.getAddress().getAddrLine1() + ","
							+ comp.getAddress().getLandmark();
				} else {
					custAdd1 = comp.getAddress().getAddrLine1();
				}
			}

			if (!comp.getAddress().getLocality().equals("")) {
				custFullAdd1 = custAdd1 + "," + comp.getAddress().getLocality()
						+ "," + comp.getAddress().getCity() + "-"
						+ comp.getAddress().getPin() + ","
						+ comp.getAddress().getState()
						+ comp.getAddress().getCountry();

			} else {
				custFullAdd1 = custAdd1 + "," + comp.getAddress().getCity()
						+ "-" + comp.getAddress().getPin() + ","
						+ comp.getAddress().getState() + ","
						+ comp.getAddress().getCountry();
			}
		}

		Phrase address = new Phrase(custFullAdd1, font8);
		PdfPCell addressCell = new PdfPCell(address);
		addressCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		addressCell.setBorder(0);
		table.addCell(addressCell);

		// ends here *******************

		// contact details ************

		String contactInfo = "Tel :" + comp.getCellNumber1();

		if (comp.getFaxNumber() != null) {
			contactInfo = contactInfo + " | Fax :" + comp.getFaxNumber();
		}

		if (comp.getEmail() != null) {
			contactInfo = contactInfo + " | Email :" + comp.getEmail();
		}
		if (comp.getWebsite() != null) {
			contactInfo = contactInfo + " | Website :" + comp.getWebsite();
		}

		Phrase contactDetailsInfo = new Phrase(contactInfo, font8);
		PdfPCell contactDetailsInfoCell = new PdfPCell(contactDetailsInfo);
		contactDetailsInfoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		contactDetailsInfoCell.setBorder(0);
		table.addCell(contactDetailsInfoCell);
		// ends here **********************
		
		PdfPCell tblCell = new PdfPCell();
		tblCell.addElement(table);
		tblCell.setBorder(0);

		a5tbl1.addCell(tblCell);
		a5tbl2.addCell(tblCell);
		
//		
//		try {
//			document.add(table);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}

			
	}
	
	private void employeeAndCustomerSign() {
		
		a5tbl1.setWidthPercentage(100);
		a5tbl2.setWidthPercentage(100);
		
		
		PdfPTable table  = new PdfPTable(2);
		table.setWidthPercentage(100f);
		table.setSpacingBefore(10f);
		
		Phrase empSign = new Phrase("Employee Signature ",font8);
		PdfPCell empSignCell = new PdfPCell(empSign);
		empSignCell.setBorder(0);
		table.addCell(empSignCell);
		
		Phrase custSign = new Phrase("Customer Signature ",font8);
		PdfPCell custSignCell = new PdfPCell(custSign);
		custSignCell.setBorder(0);
		table.addCell(custSignCell);
		
		Phrase blackspace = new Phrase(" ",font8);
		PdfPCell blackspaceCell = new PdfPCell(blackspace);
		blackspaceCell.setBorder(0);
		
		Phrase black = new Phrase(" ",font8);
		PdfPCell blackCell = new PdfPCell(black);
		blackCell.setBorderWidthLeft(0);
		blackCell.setBorderWidthTop(0);
		blackCell.setBorderWidthRight(0);
		
		table.addCell(blackspaceCell);
		table.addCell(blackspaceCell);
		table.addCell(blackCell);
		table.addCell(blackCell);
		
		PdfPCell tblCell = new PdfPCell();
		tblCell.setBorder(0);
		tblCell.addElement(table);
		
		a5tbl1.addCell(tblCell);
		a5tbl2.addCell(tblCell);
		
		
		
//		try {
//			document.add(table);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//		
	}

	
	
//  rohan added this code for NBHC customer feed back 	
	
	private void feedbackFromCustomer() 
	{
		a5tbl1.setWidthPercentage(100);
		a5tbl2.setWidthPercentage(100);
		
		float[] columnWidths = {2f, 0.1f, 2f};
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100f);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		PdfPTable obsTable = new PdfPTable(1);
		obsTable.setWidthPercentage(100f);
		
		Phrase obsByNBHC = new Phrase("Observations",font8bold);//"Observations by NBHC"
		PdfPCell obsByNBHCCell = new PdfPCell(obsByNBHC);
		obsByNBHCCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		obsByNBHCCell.setBorder(0);
		obsTable.addCell(obsByNBHCCell);
		
		Phrase sideBlank = null;
		if(service.getComment()!=null)
		{
			sideBlank = new Phrase(service.getComment(),font8);
			PdfPCell sideBlankCell = new PdfPCell(sideBlank);
			sideBlankCell.setBorder(0);
			obsTable.addCell(sideBlankCell);
		}
		else
		{
			sideBlank = new Phrase(" ",font8);
			PdfPCell sideBlankCell = new PdfPCell(sideBlank);
			sideBlankCell.setBorderWidthRight(0);
			sideBlankCell.setBorderWidthLeft(0);
			sideBlankCell.setBorderWidthTop(0);
			
			for (int i = 0; i <= 5; i++) {
				obsTable.addCell(sideBlankCell);
			}
		}
		
		PdfPCell sideBlankCell = new PdfPCell(sideBlank);
		sideBlankCell.setBorder(0);
		obsTable.addCell(sideBlankCell);
		
//		sideBlankCell.setBorderWidthLeft(0);
//		sideBlankCell.setBorderWidthRight(0);
//		sideBlankCell.setBorderWidthTop(0);
//		obsTable.addCell(sideBlankCell);
//		obsTable.addCell(sideBlankCell);
//		obsTable.addCell(sideBlankCell);
//		obsTable.addCell(sideBlankCell);
		
		
		PdfPCell obsTableCell = new PdfPCell();
		obsTableCell.addElement(obsTable);
		obsTableCell.setBorder(0);
		
		
		
		
		PdfPTable blankTable = new PdfPTable(1);
		blankTable.setWidthPercentage(100f);
		
		Phrase blank =  new Phrase();
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);
//		blankTable.addCell(blankcell);
//		blankTable.addCell(blankcell);
//		blankTable.addCell(blankcell);
//		blankTable.addCell(blankcell);
//		blankTable.addCell(blankcell);
		
		PdfPCell blankTableCell = new PdfPCell();
		blankTableCell.addElement(blankTable);
		blankTableCell.setBorder(0);
		
	
		
		PdfPTable feedbackTable = new PdfPTable(1);
		feedbackTable.setWidthPercentage(100f);
	
//		float columnWidths2[] = {2.5f,7.5f}; 
//		try {
//			feedbackTable.setWidths(columnWidths2);
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
		
		
		Phrase customerFeedBack = new Phrase("Customer Feedback",font8bold);
		PdfPCell customerFeedBackCell = new PdfPCell(customerFeedBack);
		customerFeedBackCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		customerFeedBackCell.setBorder(0);
		feedbackTable.addCell(customerFeedBackCell);
		
//		Phrase FeedBack = new Phrase(" ",font8bold);
//		PdfPCell FeedBackCell = new PdfPCell(FeedBack);
//		FeedBackCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		FeedBackCell.setBorder(0);
//		feedbackTable.addCell(FeedBackCell);
		
		Phrase blanks = new Phrase(" ",font8bold);
		PdfPCell blanksCell = new PdfPCell(blanks);
		blanksCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		blanksCell.setBorder(0);
//		feedbackTable.addCell(blanksCell);
//		feedbackTable.addCell(blanksCell);
		
		
		// Service Quality Rating (Please circle your choice):
		/**30-10-2017 sagar sore[]**/
//		float columnWidths3[] = {3.8f,6.2f}; 
		float columnWidths3[] = {4.2f,5.8f}; 
		PdfPTable qualityRatingTable = new PdfPTable(2); 
		qualityRatingTable.setWidthPercentage(100f);
		
		try {
			qualityRatingTable.setWidths(columnWidths3);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		Phrase serviceQualityRating =new Phrase("Service Quality Rating", font8bold);
		PdfPCell serviceQualityRatingCell = new PdfPCell(serviceQualityRating);
		serviceQualityRatingCell.setBorder(0);
		qualityRatingTable.addCell(serviceQualityRatingCell);
		
		
		
		Phrase PleaseCircleYourchoice =new Phrase("(Please circle your choice):", font8);
		PdfPCell PleaseCircleYourchoiceCell = new PdfPCell(PleaseCircleYourchoice);
		PleaseCircleYourchoiceCell.setBorder(0);
		qualityRatingTable.addCell(PleaseCircleYourchoiceCell);
		
		
		PdfPCell qualityRatingTableCell = new PdfPCell(qualityRatingTable);
		qualityRatingTableCell.setBorder(0);
		
		//   rating 
		PdfPTable ratingTabele = new  PdfPTable(1);
		ratingTabele.setWidthPercentage(100f);
	/** 30-10-2017 sagar sore []**/	
	//	Phrase ratingPhrase =new Phrase("Excellent          Very Good          Good          Average          Poor" ,font8);
		Phrase ratingPhrase =new Phrase("Excellent       Very Good       Good      Average       Poor" ,font8);
		PdfPCell ratingCell = new PdfPCell(ratingPhrase);
		ratingCell.setBorder(0);
		ratingTabele.addCell(ratingCell);
		
		PdfPCell ratingTabeleCell = new PdfPCell(ratingTabele);
		ratingTabeleCell.setBorder(0);
		
		
		//   commenst 
		PdfPTable commentsTable = new  PdfPTable(2);
		commentsTable.setWidthPercentage(100f);
	
		/**30-10-2017 sagar sore[]**/
	//	float mycolumnWidths[] = {2.0f,8.0f}; 
		float mycolumnWidths[] = {2.3f,7.7f}; 
		
		try {
			commentsTable.setWidths(mycolumnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		Phrase comentsValue = new Phrase("Comments :",font8);
		PdfPCell comentsValueCell = new PdfPCell(comentsValue);
/** 30-10-2017 sagar sore [padding added ]**/
		comentsValueCell.setPadding(1);
		comentsValueCell.setBorder(0);
		commentsTable.addCell(comentsValueCell);
		
		Phrase comentsValueBlank = new Phrase(" ",font8);
		PdfPCell comentsValueBlankCell = new PdfPCell(comentsValueBlank);
		comentsValueBlankCell.setBorderWidthRight(0);
		comentsValueBlankCell.setBorderWidthTop(0);
		comentsValueBlankCell.setBorderWidthLeft(0);
		
		commentsTable.addCell(comentsValueBlankCell);
		
		PdfPCell commentsTableCell = new PdfPCell(commentsTable);
		commentsTableCell.setBorder(0);
		
		
				
		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100f);
		parentTable.addCell(qualityRatingTableCell);
		parentTable.addCell(ratingTabeleCell);
		parentTable.addCell(blanksCell);
		parentTable.addCell(commentsTableCell);
		parentTable.addCell(comentsValueBlankCell);
		parentTable.addCell(comentsValueBlankCell);
//		parentTable.addCell(comentsValueBlankCell);
		
		PdfPCell parentTableCell = new PdfPCell(parentTable);
//		parentTableCell.setBorderWidthRight(0);
//		parentTableCell.setBorderWidthTop(0);
//		parentTableCell.setBorderWidthBottom(0);
		parentTableCell.setBorder(0);
		
		Phrase customerFeedBackfromService=null;
		PdfPCell customerFeedBackfromServiceCell = null;
		if(service.getCustomerFeedback()!=null && !service.getCustomerFeedback().equals(""))
		{
			customerFeedBackfromService = new Phrase(service.getCustomerFeedback()+" "+service.getRemark(),font8);
			customerFeedBackfromServiceCell = new PdfPCell(customerFeedBackfromService);
			customerFeedBackfromServiceCell.setBorder(0);
			feedbackTable.addCell(customerFeedBackfromServiceCell);
			feedbackTable.addCell(parentTableCell);
			feedbackTable.addCell(parentTableCell);
		}
		else
		{
//				customerFeedBackfromService = new Phrase(" ",font8);
//				customerFeedBackfromServiceCell = new PdfPCell(customerFeedBackfromService);
//				customerFeedBackfromServiceCell.setBorderWidthRight(0);
//				customerFeedBackfromServiceCell.setBorderWidthLeft(0);
//				customerFeedBackfromServiceCell.setBorderWidthTop(0);
//				
//				
//				PdfPTable blankTableTable = new PdfPTable(1);
//				blankTableTable.setWidthPercentage(100f);
//				for (int i = 0; i < 5; i++) 
//				{
//					blankTableTable.addCell(customerFeedBackfromServiceCell);
//				}
//				PdfPCell blankTableTableCell = new PdfPCell(blankTableTable);
//				blankTableTableCell.setBorderWidthBottom(0);
//				blankTableTableCell.setBorderWidthLeft(0);
//				blankTableTableCell.setBorderWidthRight(0);
//				
//				feedbackTable.addCell(blankTableTableCell);
				feedbackTable.addCell(parentTableCell);
		}
		
		PdfPCell feedbackTableCell = new PdfPCell();
		feedbackTableCell.addElement(feedbackTable);
		feedbackTableCell.setBorder(0);
		
		table.addCell(obsTableCell);
		table.addCell(blankTableCell);
		table.addCell(feedbackTableCell);
		
		Phrase blnk  = new Phrase(" ",font8);//By Jayshree add Font size
		PdfPCell blnkCell = new PdfPCell(blnk);
		blnkCell.setBorder(0);
		blnkCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		blnkCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		PdfPCell tblcell = new PdfPCell();
		tblcell.setBorder(0);
		tblcell.addElement(table);
		
		a5tbl1.addCell(blnkCell);
		a5tbl1.addCell(tblcell);
		
		a5tbl2.addCell(blnkCell);
		a5tbl2.addCell(tblcell);
		
		
//		try {
//			document.add(Chunk.NEWLINE);
//			document.add(table);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
	}
	
	public void addToParentTbl() {
		
		a5ParentTable.setWidthPercentage(100);
		try {
			a5ParentTable.setWidths(new float[] {50,50});
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		/**30-10-2017 sagar sore[]**/
		PdfPCell a5tbl1Cell = new PdfPCell(a5tbl1);
		a5tbl1Cell.setBorder(0);
//		a5tbl1Cell.addElement(a5tbl1);//Date 11/12/2017 By Jayshree Comment this to remove space
		a5tbl1Cell.setPaddingRight(20);//Date 11/12/2017 By Jayshre to set the cell padding
		
		PdfPCell a5tbl2Cell = new PdfPCell(a5tbl2);
		a5tbl2Cell.setBorder(0);
//		a5tbl2Cell.addElement(a5tbl2);//Date 11/12/2017 By jayshree comment this to remove space
		a5tbl2Cell.setPaddingLeft(20);//Date 11/12/2017 By Jayshree to set the Cell Padding
		/*end*/
		
		a5ParentTable.addCell(a5tbl1Cell);
		a5ParentTable.addCell(a5tbl2Cell);
		
		try {
			document.add(a5ParentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
	}

	/**
	 * Updated by: Viraj
	 * Date: 21-02-2019
	 * Description: To show header and footer dynamically
	 * @param doc
	 * @param comp
	 */
	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
//			doc.add(image2);
			a5tbl1.addCell(image2);
			a5tbl2.addCell(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
//			doc.add(image2);
			a5tbl1.addCell(image2);
			a5tbl2.addCell(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	/** Ends **/
//	private void createCompanyNameAsFooter(Document doc, Company comp) {
//		
//		
//		DocumentUpload document =comp.getUploadFooter();
//
//		//patch
//		String hostUrl;
//		String environment = System.getProperty("com.google.appengine.runtime.environment");
//		if (environment.equals("Production")) {
//		    String applicationId = System.getProperty("com.google.appengine.application.id");
//		    String version = System.getProperty("com.google.appengine.application.version");
//		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
//		} else {
//		    hostUrl = "http://localhost:8888";
//		}
//		
//		try {
////			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
//			Image image2 = Image.getInstance("images/NBHC_Footer.jpg");
////			image2.scalePercent(10f);
////			image2.scaleAbsoluteWidth(270f);
//			image2.setAbsolutePosition(50f,730f);	
//			image2.scalePercent(35f);
//			image2.scaleAbsoluteWidth(375f);
//			image2.setSpacingBefore(5f);
////			image2.setAbsolutePosition(0f,725f);
//			
//			Image image3 = Image.getInstance(new URL(hostUrl+document.getUrl()));
////			Image image3 = Image.getInstance("images/NBHC_Footer.png");
////			image3.scalePercent(15f);
////			image3.scaleAbsoluteWidth(270f);
//		/*	image3.setAbsolutePosition(480f,20f);
//			image3.scalePercent(20f);
//			image3.scaleAbsoluteWidth(350f);*/
//			
////			image3.setAbsolutePosition(0f,725f);
//			/*** 30-10-2017 sagar sore [To show header two time on vertically divided page]**/
//		/*	PdfPCell blankCell = new PdfPCell();
//			PdfPCell imageCell = new PdfPCell();
//			imageCell.addElement(image2);
//			imageCell.setBorder(0);
//		
//			//		doc.add(image2);
//				doc.add(image2);
//				doc.add(image3);*/
//				  /** end **/
//				
//				PdfPCell imageCell = new PdfPCell();
//				imageCell.addElement(image2);
//				imageCell.setBorder(0);
//		        
//				/*
//				 * commented by Ashwini
//				 */
////				a5tbl1.addCell(imageCell);
////				a5tbl2.addCell(imageCell);
//				
//				a5tbl1.addCell(image2);
//				a5tbl2.addCell(image2);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
////		try
////		{
////		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
////		image1.scalePercent(15f);
////		image1.scaleAbsoluteWidth(520f);
////		image1.setAbsolutePosition(40f,40f);	
////		doc.add(image1);
////		}
////		catch(Exception e)
////		{
////			e.printStackTrace();
////		}
//		}
}
