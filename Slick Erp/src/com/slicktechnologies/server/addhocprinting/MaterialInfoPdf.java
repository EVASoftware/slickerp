package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.approval.MaterialInfo;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;

public class MaterialInfoPdf {

	

	public  Document document;
	Company comp;
	Customer cust;
	Contract conEntity;
	Quotation quotEntity;
	BillOfMaterial bom;
	ProductGroupDetails pgd;
	String docType;
	int docId;
	
	ArrayList<MaterialInfo> materialList;
	
	
	
	
	
	private  Font font16boldul,font12bold,font8bold,font8,font12boldul,font12,font16bold,font10,font10bold,font14bold,font14,font9;
	private SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");
	DecimalFormat df=new DecimalFormat("0.00");
	
	
	public void setMaterialInfo(String doctype,int docid,long companyid){
		docType=doctype;
		docId=docid;
		if(doctype.equals("Contract")){
			conEntity=ofy().load().type(Contract.class).filter("companyId", companyid).filter("count", docid).first().now();
			if(conEntity.getCompanyId()!=null){
				comp=ofy().load().type(Company.class).filter("companyId", conEntity.getCompanyId()).first().now();
				cust=ofy().load().type(Customer.class).filter("companyId", conEntity.getCompanyId()).filter("count", conEntity.getCinfo().getCount()).first().now();
			}
		}
		if(doctype.equals(AppConstants.APPROVALQUOTATION)){
			quotEntity=ofy().load().type(Quotation.class).filter("companyId", companyid).filter("count", docid).first().now();
			if(quotEntity.getCompanyId()!=null){
				comp=ofy().load().type(Company.class).filter("companyId", quotEntity.getCompanyId()).first().now();
				cust=ofy().load().type(Customer.class).filter("companyId", quotEntity.getCompanyId()).filter("count", quotEntity.getCinfo().getCount()).first().now();
			}
		}
		createProductDetails();
		
	}
	
	public MaterialInfoPdf() {
		font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
		font8= new Font(Font.FontFamily.HELVETICA,8);
		font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font12=new Font(Font.FontFamily.HELVETICA,12);
		font10=new Font(Font.FontFamily.HELVETICA,10);
		font10bold = new Font(Font.FontFamily.HELVETICA  , 10, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font14 = new Font(Font.FontFamily.HELVETICA  , 14, Font.NORMAL); 
		font9 = new Font(Font.FontFamily.HELVETICA  , 9, Font.NORMAL); 
	}
	
	
	public void createPdf() {
		createLogo(document,comp);
		createCompanyHedding();
		createProductTableDetails();
		
	}
		
	private void createProductTableDetails() {
		
		Font font1 = new Font(Font.FontFamily.HELVETICA  , 8);
		
		PdfPTable table = new PdfPTable(8);
		table.setWidthPercentage(100);
//		table.setSpacingBefore(10f);
		
		Phrase serProdId = new  Phrase("Service Product Id",font8bold);
		Phrase serProdName = new  Phrase("Service Product Name",font8bold);
		Phrase serNo = new  Phrase("Service No.",font8bold);
		Phrase materId = new  Phrase("Material Id",font8bold);
		Phrase materCode = new Phrase("Material Code",font8bold);
	    Phrase materName = new Phrase("Material Name",font8bold);
	    Phrase materQty = new Phrase("Material QTY",font8bold);
	    Phrase materUom = new Phrase("Material UOM",font8bold);
	    
	    
	    PdfPCell serProdIdCell = new PdfPCell(serProdId);
	    serProdIdCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	    
	    PdfPCell serProdNameCell = new PdfPCell(serProdName);
	    serProdNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	    
	    PdfPCell serNoCell = new PdfPCell(serNo);
	    serNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	     
	    PdfPCell materIdCell = new PdfPCell(materId);
	    materIdCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	    
		PdfPCell materCodeCell = new PdfPCell(materCode);
		materCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		 
	    PdfPCell materNameCell = new PdfPCell(materName);
	    materNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	     
	    PdfPCell materQtyCell = new PdfPCell(materQty);
	    materQtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	     
	    PdfPCell materUomCell = new PdfPCell(materUom);
	    materUomCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	    
	    
	    table.addCell(serProdIdCell);
	    table.addCell(serProdNameCell);
	    table.addCell(serNoCell);
	    table.addCell(materIdCell);
	    table.addCell(materCodeCell);
	    table.addCell(materNameCell);
	    table.addCell(materQtyCell);
	    table.addCell(materUomCell);
	    
	    for(int i=0;i<materialList.size();i++){
	    	
	    	Phrase serProdIdValue = new  Phrase(materialList.get(i).getSerProdId()+"",font1);
			Phrase serProdNameValue = new  Phrase(materialList.get(i).getSerProdName()+"",font1);
			Phrase serNoValue = new  Phrase(materialList.get(i).getSerNo()+"",font1);
			Phrase materIdValue = new  Phrase(materialList.get(i).getMaterialProdId()+"",font1);
			Phrase materCodeValue = new Phrase(materialList.get(i).getMaterialProdCode()+"",font1);
		    Phrase materNameValue = new Phrase(materialList.get(i).getMaterialProdName()+"",font1);
		    Phrase materQtyValue = new Phrase(materialList.get(i).getMaterialProdQty()+"",font1);
		    Phrase materUomValue = new Phrase(materialList.get(i).getMatrialProdUom()+"",font1);
		    
		    
		    PdfPCell serProdIdValueCell = new PdfPCell(serProdIdValue);
		    serProdIdValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		    
		    PdfPCell serProdNameValueCell = new PdfPCell(serProdNameValue);
		    serProdNameValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		    
		    PdfPCell serNoValueCell = new PdfPCell(serNoValue);
		    serNoValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		     
		    PdfPCell materIdValueCell = new PdfPCell(materIdValue);
		    materIdValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		    
			PdfPCell materCodeValueCell = new PdfPCell(materCodeValue);
			materCodeValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			 
		    PdfPCell materNameValueCell = new PdfPCell(materNameValue);
		    materNameValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		     
		    PdfPCell materQtyValueCell = new PdfPCell(materQtyValue);
		    materQtyValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		     
		    PdfPCell materUomValueCell = new PdfPCell(materUomValue);
		    materUomValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		    
		    
		    table.addCell(serProdIdValueCell);
		    table.addCell(serProdNameValueCell);
		    table.addCell(serNoValueCell);
		    table.addCell(materIdValueCell);
		    table.addCell(materCodeValueCell);
		    table.addCell(materNameValueCell);
		    table.addCell(materQtyValueCell);
		    table.addCell(materUomValueCell);
	    	
	    }
	    
	    
	    
	    try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
	}

	private void createProductDetails() {
		if(docType.equals("Contract")){
			materialList=new ArrayList<MaterialInfo>();
			for(int i=0;i<conEntity.getItems().size();i++){
				bom=ofy().load().type(BillOfMaterial.class).filter("product_id", conEntity.getItems().get(i).getPrduct().getCount()).filter("companyId", conEntity.getCompanyId()).first().now();
					if(bom!=null){
						for(int k=0;k<bom.getBillProdItems().size();k++){
						
						pgd=ofy().load().type(ProductGroupDetails.class).filter("companyId", bom.getCompanyId()).filter("count", bom.getBillProdItems().get(k).getProdGroupId()).first().now();
							
							if(pgd!=null){
								
								for(int j=0;j<pgd.getPitems().size();j++){
									
									MaterialInfo info =new MaterialInfo();
									info.setSerProdId(conEntity.getItems().get(i).getPrduct().getCount());
									info.setSerProdName(conEntity.getItems().get(i).getProductName());
									info.setSerNo( bom.getBillProdItems().get(k).getServiceNo());
									info.setMaterialProdId(pgd.getPitems().get(j).getProduct_id());
									info.setMaterialProdCode(pgd.getPitems().get(j).getCode());
									info.setMaterialProdName(pgd.getPitems().get(j).getName());
									info.setMaterialProdQty(pgd.getPitems().get(j).getQuantity());
									info.setMatrialProdUom(pgd.getPitems().get(j).getUnit());
									
									materialList.add(info);
								}
							}
						}
						
					}
			}
		}if(docType.equals("Quotation")){
			materialList=new ArrayList<MaterialInfo>();
			for(int i=0;i<quotEntity.getItems().size();i++){
				bom=ofy().load().type(BillOfMaterial.class).filter("product_id", quotEntity.getItems().get(i).getPrduct().getCount()).filter("companyId", quotEntity.getCompanyId()).first().now();
					if(bom!=null){
						for(int k=0;k<bom.getBillProdItems().size();k++){
						
						pgd=ofy().load().type(ProductGroupDetails.class).filter("companyId", bom.getCompanyId()).filter("count", bom.getBillProdItems().get(k).getProdGroupId()).first().now();
							
							if(pgd!=null){
								
								for(int j=0;j<pgd.getPitems().size();j++){
									
									MaterialInfo info =new MaterialInfo();
									info.setSerProdId(quotEntity.getItems().get(i).getPrduct().getCount());
									info.setSerProdName(quotEntity.getItems().get(i).getProductName());
									info.setSerNo( bom.getBillProdItems().get(k).getServiceNo());
									info.setMaterialProdId(pgd.getPitems().get(j).getProduct_id());
									info.setMaterialProdCode(pgd.getPitems().get(j).getCode());
									info.setMaterialProdName(pgd.getPitems().get(j).getName());
									info.setMaterialProdQty(pgd.getPitems().get(j).getQuantity());
									info.setMatrialProdUom(pgd.getPitems().get(j).getUnit());
									
									materialList.add(info);
								}
							}
						}
						
					}
			}
			
		}
	}

	private void createLogo(Document doc, Company comp) {
		DocumentUpload document =comp.getLogo();

		//patch
		String hostUrl; 
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f,765f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		
	public  void createCompanyHedding()
	{
			Phrase companyName= new Phrase("                  "+comp.getBusinessUnitName(),font12boldul);
		    Paragraph p =new Paragraph();
		    p.add(Chunk.NEWLINE);
		    p.add(companyName);
		    
		    PdfPCell companyHeadingCell=new PdfPCell();
		    companyHeadingCell.setBorder(0);
		    companyHeadingCell.addElement(p);
		    
		    Phrase adressline1= new Phrase(comp.getAddress().getAddrLine1(),font10);
			Phrase adressline2=null;
			if(comp.getAddress().getAddrLine2()!=null)
			{
				adressline2= new Phrase(comp.getAddress().getAddrLine2(),font10);
			}
			
			String complandmark1="";
			Phrase landmar1=null;
			
			String  localit1="";
			Phrase complocality1= null;
			
			if(comp.getAddress().getLandmark()!=null)
			{
				complandmark1 = comp.getAddress().getLandmark();
				landmar1=new Phrase(complandmark1,font10);
			}
			
			
			if(comp.getAddress().getLocality()!=null){
				localit1=comp.getAddress().getLocality();
				complocality1=new Phrase(localit1,font10);
				}
			
			Phrase cityState=new Phrase(comp.getAddress().getCity()
					+" - "+comp.getAddress().getPin()
					,font10);
			
			Phrase branch=null;
			if(cust.getBranch() !=null){
				
			 branch =new Phrase("Branch : "+cust.getBranch(),font10);
			}
			else
				{ 
				branch =new Phrase(cust.getBranch()); 
				}
			
			PdfPCell branchcell=new PdfPCell();
			branchcell.addElement(branch);
			branchcell.setBorder(0);
			
			
			PdfPCell custlandcell=new PdfPCell();
			custlandcell.addElement(landmar1);
			custlandcell.setBorder(0);
			
			PdfPCell custlocalitycell=new PdfPCell();
			custlocalitycell.addElement(complocality1);
			custlocalitycell.setBorder(0);
			
			PdfPCell custcitycell=new PdfPCell();
			custcitycell.addElement(cityState);
			custcitycell.setBorder(0); 

			PdfPCell addressline1cell=new PdfPCell();
			PdfPCell addressline2cell=new PdfPCell();
			addressline1cell.addElement(adressline1);
			addressline1cell.setBorder(0);
			if(adressline2!=null)
			{
				addressline2cell.addElement(adressline2);
				addressline2cell.setBorder(0);
			}
			
			String contactinfo="Mobile: "+comp.getContact().get(0).getCellNo1();
			if(comp.getContact().get(0).getCellNo2()!=-1)
				contactinfo=contactinfo+","+comp.getContact().get(0).getCellNo2();
			if(comp.getContact().get(0).getLandline()!=-1){
				contactinfo=contactinfo+"     "+" Phone: 0"+comp.getContact().get(0).getLandline();
			}
			
			Phrase contactnos=new Phrase(contactinfo,font9);
			Phrase email= new Phrase("email: "+comp.getContact().get(0).getEmail(),font9);
			
			PdfPCell contactcell=new PdfPCell();
			contactcell.addElement(contactnos);
			contactcell.setBorder(0);
			PdfPCell emailcell=new PdfPCell();
			emailcell.setBorder(0);
			emailcell.addElement(email);
			
			
			
			
			PdfPTable companytable=new PdfPTable(1);
			companytable.addCell(companyHeadingCell);
			companytable.addCell(addressline1cell);
			if(!comp.getAddress().getAddrLine2().equals(""))
			{
				companytable.addCell(addressline2cell);
			}
			if(!comp.getAddress().getLandmark().equals("")){
			companytable.addCell(custlandcell);
			}
			if(!comp.getAddress().getLocality().equals("")){
			companytable.addCell(custlocalitycell);
			}
			companytable.addCell(custcitycell);
			companytable.addCell(branchcell);
			companytable.addCell(contactcell);
			companytable.addCell(emailcell);
			
			
			Phrase blankPhase= new Phrase("",font9);
			PdfPCell blankcell = new PdfPCell(blankPhase);
			blankcell.setBorder(0);
			
			
			if(comp.getAddress().getAddrLine2().equals("")){
				companytable.addCell(blankcell);
			}
			if(comp.getAddress().getLandmark().equals("")){
				companytable.addCell(blankcell);
			}
			if(comp.getAddress().getLocality().equals("")){
				companytable.addCell(blankcell);
			}
			
			companytable.setHorizontalAlignment(Element.ALIGN_LEFT);
			companytable.setWidthPercentage(100);

			

			if(comp.getContact()!=null&&comp.getCompanyId()!=null){
					Phrase realcontact=new Phrase("Contact Person: "+comp.getPocName(),font9);
					realcontact.add(Chunk.NEWLINE);
					PdfPCell realcontactcell=new PdfPCell();
					realcontactcell.addElement(realcontact);
					realcontactcell.setBorder(0);
					companytable.addCell(realcontactcell);
				}
			
			
			/**
			 * Customer Info
			 */
			String tosir="To, M/S";
			String custName="";
			if(cust.isCompany()==true&&cust.getCompanyName()!=null){
				custName=cust.getCompanyName().trim();
			}
			else{
				custName=cust.getFullname().trim();
			}
			
			Phrase tosirphrase= new Phrase(tosir,font12bold);
			
			Phrase customername= new Phrase(custName,font12boldul);
		    Paragraph fullname =new Paragraph();
		    fullname.add(Chunk.NEWLINE);
		    fullname.add(tosir+"   "+custName);
		    fullname.setFont(font12bold);
		    
				PdfPCell custnamecell=new PdfPCell();
				custnamecell.addElement(fullname);
				custnamecell.setBorder(0);
				 
				Phrase customeradress= new Phrase("                "+cust.getAdress().getAddrLine1(),font10);
			Phrase customeradress2=null;
			if(cust.getAdress().getAddrLine2()!=null){
			   customeradress2= new Phrase("                "+cust.getAdress().getAddrLine2(),font10);
			}
			
			PdfPCell custaddress1=new PdfPCell();
			custaddress1.addElement(customeradress);
			custaddress1.setBorder(0);
			
			PdfPCell custaddress2=new PdfPCell();
			if(cust.getAdress().getAddrLine2()!=null){
				custaddress2.addElement(customeradress2);
				custaddress2.setBorder(0);
				}
			
			
			String custlandmark2="";
			Phrase landmar2=null;
			
			String  localit2="";
			Phrase custlocality2= null;
			
			if(cust.getAdress().getLandmark()!=null)
			{
				custlandmark2 = cust.getAdress().getLandmark();
				landmar2=new Phrase("                "+custlandmark2,font10);
			}
			
			
			if(cust.getAdress().getLocality()!=null){
				localit2=cust.getAdress().getLocality();
				custlocality2=new Phrase("                "+localit2,font10);
				}
			
			Phrase cityState2=new Phrase("                "+cust.getAdress().getCity()
					+" - "+cust.getAdress().getPin()
//					+" "+cust.getAdress().getState()+" "+cust.getAdress().getCountry()
					,font10);
			
			PdfPCell custlandcell2=new PdfPCell();
			custlandcell2.addElement(landmar2);
			custlandcell2.setBorder(0);
			
			PdfPCell custlocalitycell2=new PdfPCell();
			custlocalitycell2.addElement(custlocality2);
			custlocalitycell2.setBorder(0);
			
			PdfPCell custcitycell2=new PdfPCell();
			custcitycell2.addElement(cityState2);
			custcitycell2.setBorder(0); 
//			custcitycell2.addElement(Chunk.NEWLINE);
				 
			Phrase custcontact=new Phrase("Mobile: "+cust.getCellNumber1()+"",font9);
				PdfPCell custcontactcell=new PdfPCell();
				custcontactcell.addElement(custcontact);
				custcontactcell.setBorder(0);
				
				Phrase custemail=new Phrase("email: "+cust.getEmail(),font9);
				PdfPCell custemailcell=new PdfPCell();
				custemailcell.addElement(custemail);
				custemailcell.setBorder(0);
				
					
				PdfPTable custtable=new PdfPTable(1);
				custtable.setWidthPercentage(100);
				custtable.setHorizontalAlignment(Element.ALIGN_LEFT);
				custtable.addCell(custnamecell);
				custtable.addCell(custaddress1);
				if(!cust.getAdress().getAddrLine2().equals("")){
					custtable.addCell(custaddress2);
				}
				if(!cust.getAdress().getLandmark().equals("")){
					custtable.addCell(custlandcell2);
				}
				if(!cust.getAdress().getLocality().equals("")){
					
				custtable.addCell(custlocalitycell2);
				}
				custtable.addCell(custcitycell2);
				custtable.addCell(custcontactcell);
				custtable.addCell(custemailcell);
				
				
				Phrase docDetails=new Phrase(docType+" : "+docId,font9);
				PdfPCell docDeatilsCell=new PdfPCell();
				docDeatilsCell.addElement(docDetails);
				docDeatilsCell.setBorder(0);
				
				Date docDate = null;
				if(docType.equals(AppConstants.APPROVALCONTRACT)){
					docDate=conEntity.getContractDate();
					System.out.println("CONTRACT DATE : "+conEntity.getContractDate());
				}
				if(docType.equals(AppConstants.APPROVALQUOTATION)){
					docDate=quotEntity.getQuotationDate();
					System.out.println("QUOTATION DATE : "+quotEntity.getQuotationDate());
				}
				
				Phrase docDatep=new Phrase(docType+" Date"+" : "+fmt.format(docDate),font9);
				PdfPCell docDateCell=new PdfPCell();
				docDateCell.addElement(docDatep);
				docDateCell.setBorder(0);
				
				PdfPTable docTable= new PdfPTable(2);
				docTable.setWidthPercentage(100f);
				docTable.addCell(docDeatilsCell);
				docTable.addCell(docDateCell);
				
				
				PdfPCell docDateTableCell=new PdfPCell();
				docDateTableCell.addElement(docTable);
				docDateTableCell.setBorder(0);
				
				
				if(cust.isCompany()==true&&cust.getCompanyName()!=null){
					Phrase realcontact=new Phrase("Attn: "+cust.getFullname(),font10);
					realcontact.add(Chunk.NEWLINE);
					PdfPCell realcontactcell=new PdfPCell();
					realcontactcell.addElement(realcontact);
					realcontactcell.setBorder(0);
					custtable.addCell(realcontactcell);
				}
				custtable.addCell(docDateTableCell);
				
			PdfPTable headparenttable= new PdfPTable(2);
			headparenttable.setWidthPercentage(100);
			try {
				headparenttable.setWidths(new float[]{50,50});
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			
			PdfPCell  companyinfocell = new PdfPCell();
			PdfPCell custinfocell = new PdfPCell();
			
			
			companyinfocell.addElement(companytable);
			custinfocell.addElement(custtable);
			headparenttable.addCell(companyinfocell);
			headparenttable.addCell(custinfocell);
			
			
			 String title="Material Requirement";
				
				Phrase titlephrase = new Phrase(title,font14bold);
				PdfPCell cell = new PdfPCell(titlephrase);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				
				PdfPTable table = new PdfPTable(1);
				table.setWidthPercentage(100f);
				table.addCell(cell);

			try {
				document.add(headparenttable);
				document.add(table);
				
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
			}
	
}
