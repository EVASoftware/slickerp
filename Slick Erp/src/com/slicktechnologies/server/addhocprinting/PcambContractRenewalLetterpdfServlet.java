package com.slicktechnologies.server.addhocprinting;


import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;

public class PcambContractRenewalLetterpdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6828603811167470318L;

	
	protected  void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException ,IOException {
			 //super.doGet(request, response);
		    response.setContentType("application/pdf");
		    
	
	  try{
			
			String stringid1 = request.getParameter("ContractId");
			stringid1 = stringid1.trim();
			int count = Integer.parseInt(stringid1);
			
			String stringid = request.getParameter("CompanyId");
			stringid = stringid.trim();
			Long companyId = Long.parseLong(stringid);
			
			ContractRenewal wo = ofy().load().type(ContractRenewal.class).filter("companyId", companyId).filter("contractId", count).first().now();
			
			PcambContractRenewalLetterpdf wopdf = new PcambContractRenewalLetterpdf();
			
			wopdf.document = new Document();
			Document document = wopdf.document;
			PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); // write
																		
//			if(!wo.getStatus().equals("Renewed")){
//				writer.setPageEvent(new PdfWatermark());
//				
//			}
//			else {
////				if(!wo.getStatus().equals("Inprocessed")){	
//				writer.setPageEvent(new PdfCancelWatermark());
//			} 
																		
			 document.open();
			
			wopdf.setContractRewnewal(wo);
			wopdf.createPdf();
			document.close();
		}catch (DocumentException e) {
		  e.printStackTrace();
	  }
	}
}
