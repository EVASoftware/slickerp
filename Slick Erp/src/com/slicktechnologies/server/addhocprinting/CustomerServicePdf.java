package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TimeZone;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

		public class CustomerServicePdf {

			Service service;
			ServiceProject serproject;
			CustomerBranchDetails customerbranch;
			Branch branch;
			Customer cust;
			ProductGroupList proglist;
			Company comp;
			Contract conEntity;
			SuperProduct sup;
			public  Document document;
			ProductGroupDetails prodGrpEntity=null;
			ArrayList<ProductGroupList> list=new ArrayList<ProductGroupList>();
			List<EmployeeInfo> technicians=new ArrayList<EmployeeInfo>();
			List<CompanyAsset> tooltable;
//			List<BillProductDetails> billMaterialLis;
			
			
			//***********************rohan changes here on 8/6/15*********************888
			
			List<ProductGroupList> prodList=new ArrayList<ProductGroupList>();
			
			//*************************changes ends here *******************************
			List<ProductGroupList> billMaterialLis;
			ArrayList<SuperProduct>stringlis= new ArrayList<SuperProduct>();
			
			
			//********************* Anil changes 26/12/15 **************************
			
			List<Service> serviceList=new ArrayList<Service>();
			ProcessConfiguration processConfig;
			boolean showPreviousRecordFlag=false;
			//********************* Anil changes 26/12/15 **************************
			
			private  Font font16boldul,font12bold,font8bold,font8,font12boldul,font12,font16bold,font10,font10bold,font14bold,font14,font9;
			private SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");
			DecimalFormat df=new DecimalFormat("0.00");
			float[] columnWidths = {0.6f, 0.8f, 2.7f, 0.8f,1f,1f};
			float[] columnWidths1 = {1.16f, 2.2f, 0.8f,1.5f};
			float[] columnWidths3 = {1.5f, 3f, 1f,1f,1f};
			float[] columnWidths4 = {6f,2f};
			float[] columnWidths5 = {10f,10f,10f,30f,19f,9f,12f};
			
			public CustomerServicePdf() {
				font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD| Font.UNDERLINE);
				font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
				new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
				font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
				font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
				font8= new Font(Font.FontFamily.HELVETICA,8);
				font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
				font12=new Font(Font.FontFamily.HELVETICA,12);
				font10=new Font(Font.FontFamily.HELVETICA,10);
				font10bold = new Font(Font.FontFamily.HELVETICA  , 10, Font.BOLD);
				font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
				font14 = new Font(Font.FontFamily.HELVETICA  , 14, Font.NORMAL); 
				font9 = new Font(Font.FontFamily.HELVETICA  , 9, Font.NORMAL); 
			}
			
			public void setCustomerservice(long count){
				//Load Service
				service=ofy().load().type(Service.class).id(count).now();
				
				//All services
				List<Service> serviceLst=ofy().load().type(Service.class).filter("companyId", service.getCompanyId()).filter("contractCount", service.getContractCount()).list();
				
				System.out.println("Service List :: "+serviceLst.size());
				for(Service ser:serviceLst){
					if(ser.getServiceSerialNo()<service.getServiceSerialNo()){
						serviceList.add(ser);
					}
				}
				
				System.out.println("Service List SIZE :: "+serviceList.size());
				
				//Load Service project
				if(service.getCompanyId()==null)
					serproject=ofy().load().type(ServiceProject.class).filter("companyId",service.getCompanyId()).first().now();
				else
					serproject=ofy().load().type(ServiceProject.class).filter("companyId",service.getCompanyId()).filter("serviceId",service.getCount()).filter("serviceSrNo",service.getServiceSerialNo()).first().now();
						
				//Load Branch
				if(service.getCompanyId()==null)
					branch=ofy().load().type(Branch.class).filter("companyId",service.getCompanyId()).first().now();
				else
					branch=ofy().load().type(Branch.class).filter("companyId",service.getCompanyId()).filter("buisnessUnitName",service.getBranch()).first().now();

				
				
				
				//Load Customer
				if(service.getCompanyId()==null)
					cust=ofy().load().type(Customer.class).filter("count",service.getPersonInfo().getCount()).first().now();
				else
					cust=ofy().load().type(Customer.class).filter("count",service.getPersonInfo().getCount()).filter("companyId", service.getCompanyId()).first().now();
				
				//Load Company
				if(service.getCompanyId()==null)
				   comp=ofy().load().type(Company.class).first().now();
				else
				   comp=ofy().load().type(Company.class).filter("companyId",service.getCompanyId()).first().now();
							
				
				//Load contract 
				if(service.getCompanyId()==null)
					conEntity=ofy().load().type(Contract.class).filter("companyId",service.getCompanyId()).first().now();
				else
					conEntity=ofy().load().type(Contract.class).filter("companyId",service.getCompanyId()).filter("count",service.getContractCount()).first().now();
				
				if(service.getCompanyId()==null)
					customerbranch=ofy().load().type(CustomerBranchDetails.class).filter("companyId",service.getCompanyId()).first().now();
				else
					customerbranch=ofy().load().type(CustomerBranchDetails.class).filter("companyId",service.getCompanyId()).filter("buisnessUnitName",service.getServiceBranch()).first().now();
				
				
				int prodGrpId=0;
				if(serproject!=null){
					if(serproject.getBillMaterialLis().size()!=0){
						prodGrpId=serproject.getBillMaterialLis().get(0).getProdGroupId();
					}
				}
				
				//***********************rohan changes here on 8/6/15*********************888
				if(serproject!=null){
					if(serproject.getProdDetailsList().size()!=0){
					prodList=serproject.getProdDetailsList();
					}
				}
				
				//*************************changes ends here *******************************
				
				
				if(prodGrpId!=0){
					prodGrpEntity=ofy().load().type(ProductGroupDetails.class).filter("companyId", service.getCompanyId()).filter("count", prodGrpId).first().now();
				}
				
				if(serproject!=null){
					technicians=serproject.getTechnicians();
				}
				
				if(serproject!=null){
				tooltable=serproject.getTooltable();
				}
				if(prodGrpEntity!=null){
					billMaterialLis=prodGrpEntity.getPitems();
				}
				
				
				
				if(service.getCompanyId()!=null){
					sup=ofy().load().type(SuperProduct.class).filter("companyId",service.getCompanyId()).
					filter("count", service.getProduct().getCount()).first().now();
				
					
					SuperProduct superprod= new SuperProduct();
					
					superprod.setCount(sup.getCount());
					superprod.setProductName(sup.getProductName());
					superprod.setComment(sup.getComment()+" "+sup.getCommentdesc());
					
					stringlis.add(superprod);
				
				}
				
				else{
					sup=ofy().load().type(SuperProduct.class).filter("count", service.getProduct().getCount()).first().now();
				}
				
				
				if (service.getCompanyId() != null){
					processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", service.getCompanyId()).filter("processName", "Service").filter("configStatus", true).first().now();
					System.out.println("PROCESS CONFIG");
					if(processConfig!=null){
						System.out.println("PROCESS CONFIG NOT NULL");
						for(int k=0;k<processConfig.getProcessList().size();k++)
						{
							if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("ShowServiceRecords")&&processConfig.getProcessList().get(k).isStatus()==true)
							{
								System.out.println("PROCESS CONFIG FLAG FALSE");
								showPreviousRecordFlag=true;
							}
						}
					}
				}
				
				
			}

			public void createPdf() {
				createLogo(document,comp);
				
				if(customerbranch!=null){
					createHeadingInfo();
				}else{
					createHeadingInfo1();
				}
				
				if (showPreviousRecordFlag) {
					if (serviceList.size() != 0) {
						createPreviousServiceDetails();
					}
				}
				createServiceHeadingTable();
				
				createCustomerInfo();
				createMaterialReq();
				createTechnicianinfo();
				createTools();
				customerFeedback();
				customerAndTechSign();
			}


	

	private void createPreviousServiceDetails() {

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);

		PdfPTable table = new PdfPTable(7);
		table.setWidthPercentage(100);
		try {
			table.setWidths(columnWidths5);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		Paragraph para = null;

		Phrase productdetails = new Phrase("Service Record", font10bold);
		para = new Paragraph();
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_CENTER);

		Phrase servNo = new Phrase("SERVICE NO.", font1);
		Phrase servId = new Phrase("SERVICE ID", font1);
		Phrase servDate = new Phrase("SERVICE DATE", font1);
		Phrase prodName = new Phrase("ITEM NAME", font1);
		Phrase serEngg = new Phrase("TECHNICIAN", font1);
		Phrase status = new Phrase("STATUS", font1);
		Phrase feedback = new Phrase("FEEDBACK", font1);
		

		PdfPCell servIdCell = new PdfPCell(servId);
		servIdCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell servDateCell = new PdfPCell(servDate);
		servDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell servNoCell = new PdfPCell(servNo);
		servNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell prodNameCell = new PdfPCell(prodName);
		prodNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell servEnggCell = new PdfPCell(serEngg);
		servEnggCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell statusCell = new PdfPCell(status);
		statusCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell feedbackCell = new PdfPCell(feedback);
		feedbackCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(servNoCell);
		table.addCell(servIdCell);
		table.addCell(servDateCell);
		table.addCell(prodNameCell);
		table.addCell(servEnggCell);
		table.addCell(statusCell);
		table.addCell(feedbackCell);
		
		
		Comparator<Service> sortingServices = new Comparator<Service>() {  
			@Override  
			public int compare(Service o1, Service o2) { 
				Integer c1=o1.getCount();
				Integer c2=o2.getCount();
				return c1.compareTo(c2);  
			}  
		};  
		Collections.sort(serviceList,sortingServices);
		
		

		for (int i = 0; i < this.serviceList.size(); i++) {

			Phrase chunk = null;
			chunk = new Phrase(serviceList.get(i).getCount()+ "", font8);
			PdfPCell pdfServIdCell = new PdfPCell(chunk);
			pdfServIdCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			chunk = new Phrase(serviceList.get(i).getServiceSerialNo()+ "", font8);
			PdfPCell pdfServNoCell = new PdfPCell(chunk);
			pdfServNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			chunk = new Phrase(fmt.format(serviceList.get(i).getServiceDate())+ "", font8);
			PdfPCell pdfServDateCell = new PdfPCell(chunk);
			pdfServDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			chunk = new Phrase(serviceList.get(i).getProductName()+ "", font8);
			PdfPCell pdfProdNameCell = new PdfPCell(chunk);
			pdfProdNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			chunk = new Phrase(serviceList.get(i).getEmployee()+ "", font8);
			PdfPCell pdfServEnggCell = new PdfPCell(chunk);
			pdfServEnggCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			chunk = new Phrase(serviceList.get(i).getStatus()+ "", font8);
			PdfPCell pdfStatusCell = new PdfPCell(chunk);
			pdfStatusCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			if(serviceList.get(i).getCustomerFeedback()!=null){
			chunk = new Phrase(serviceList.get(i).getCustomerFeedback()+ "", font8);
			}else{
				chunk = new Phrase(" ", font8);
			}
			PdfPCell pdfCustFeedbackCell = new PdfPCell(chunk);
			pdfCustFeedbackCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			table.addCell(pdfServNoCell);
			table.addCell(pdfServIdCell);
			table.addCell(pdfServDateCell);
			table.addCell(pdfProdNameCell);
			table.addCell(pdfServEnggCell);
			table.addCell(pdfStatusCell);
			table.addCell(pdfCustFeedbackCell);

		}

		PdfPTable parentTableTect = new PdfPTable(1);
		parentTableTect.setWidthPercentage(100);

		PdfPCell prodtablecell = new PdfPCell();
		prodtablecell.addElement(para);
		prodtablecell.addElement(table);

		parentTableTect.addCell(prodtablecell);

		try {
			document.add(parentTableTect);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

private void createLogo(Document doc, Company comp) {
	DocumentUpload document =comp.getLogo();

	//patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(20f);
		image2.setAbsolutePosition(40f,765f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
//	try
//	{
//	Image image1=Image.getInstance("images/ultrapest.jpg");
//	image1.scalePercent(20f);
//	image1.setAbsolutePosition(40f,765f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}

}
	
			

			private void createHeadingInfo() {

				Phrase companyName= new Phrase("                  "+comp.getBusinessUnitName().toUpperCase(),font12bold);
				//18 spaces added for setting logo 
			    Paragraph p =new Paragraph();
		        p.add(Chunk.NEWLINE);
			    p.add(companyName);
			    
			    PdfPCell companyHeadingCell=new PdfPCell();
			    companyHeadingCell.setBorder(0);
			    companyHeadingCell.addElement(p);
			    
			    String branch1="Branch Name";
			    Phrase branchphrase=new Phrase(branch1+" - "+branch.getBusinessUnitName(),font10);
			    PdfPCell companybranchCell=new PdfPCell();
			    companybranchCell.setBorder(0);
			    companybranchCell.addElement(branchphrase);
			    
			    
			    
			    Phrase adressline1= new Phrase(branch.getAddress().getAddrLine1(),font10);
				Phrase adressline2=null;
				if(branch.getAddress().getAddrLine2()!=null)
				{
					adressline2= new Phrase(branch.getAddress().getAddrLine2(),font10);
				}else{
					adressline2= new Phrase("",font10);
				}
				
				Phrase landmark=null;
				if(branch.getAddress().getLandmark()!=null)
				{
					landmark= new Phrase(branch.getAddress().getLandmark(),font10);
				}
				else
				{
					landmark= new Phrase("",font12);
				}
				
				
				Phrase locality=null;
				if(branch.getAddress().getLocality()!=null)
				{
					locality= new Phrase(branch.getAddress().getLocality(),font10);
				}
				else
				{
					locality= new Phrase("",font12);
				}
				Phrase	city=new Phrase(branch.getAddress().getCity()+" - "+branch.getAddress().getPin(),font10);
				
				
				PdfPCell citycell= new PdfPCell();
				citycell.addElement(city);
				citycell.setBorder(0);
				PdfPCell addressline1cell=new PdfPCell();
				PdfPCell addressline2cell=new PdfPCell();
				addressline1cell.addElement(adressline1);
				addressline1cell.setBorder(0);
					
				addressline2cell.addElement(adressline2);
				addressline2cell.setBorder(0);
				
				PdfPCell landmarkcell=new PdfPCell();
				landmarkcell.addElement(landmark);
				landmarkcell.setBorder(0);
				
				PdfPCell localitycell=new PdfPCell();
				localitycell.addElement(locality);
				localitycell.setBorder(0);
				
				String contactinfo="Cell: "+branch.getContact().get(0).getCellNo1();
				if(branch.getContact().get(0).getCellNo2()!=-1)
					contactinfo=contactinfo+","+branch.getContact().get(0).getCellNo2();
				if(branch.getContact().get(0).getLandline()!=-1){
					contactinfo=contactinfo+"     "+" Tel: "+branch.getContact().get(0).getLandline();
				}
				
				Phrase contactnos=new Phrase(contactinfo,font9);
				Phrase email= new Phrase("Email: "+branch.getContact().get(0).getEmail(),font9);
				
				PdfPCell contactcell=new PdfPCell();
				contactcell.addElement(contactnos);
				contactcell.setBorder(0);
				PdfPCell emailcell=new PdfPCell();
				emailcell.setBorder(0);
				emailcell.addElement(email);
				
				
				PdfPTable companytable=new PdfPTable(1);
				companytable.addCell(companyHeadingCell);
				companytable.addCell(companybranchCell);
				companytable.addCell(addressline1cell);
				if(!branch.getAddress().getAddrLine2().equals(""))
				{
					companytable.addCell(addressline2cell);
				}
				if(!branch.getAddress().getLandmark().equals("")){
					companytable.addCell(landmarkcell);
				}
				if(!branch.getAddress().getLocality().equals("")){
					companytable.addCell(localitycell);
				}
				companytable.addCell(citycell);
				companytable.addCell(contactcell);
				companytable.addCell(emailcell);
				companytable.setHorizontalAlignment(Element.ALIGN_LEFT);
				companytable.setWidthPercentage(100);

				
				/**
				 * Customer Info
				 */
//				String tosir="To, M/S";
				
				//************************changes made by rohan	for M/s **************** 
				String tosir= null;
				if(cust.isCompany()==true){
					
					
				tosir="To, M/S";
				}
				else{
				tosir="To,";
				}
				//*******************changes ends here ********************sss*************
				
				
//				Phrase tosirphrase= new Phrase(tosir,font12bold);
//				Paragraph tosirpara= new Paragraph();
//				tosirpara.add(tosirphrase);
				
				String custName="";
				
				if(cust.isCompany()==true&&cust.getCompanyName()!=null){
					custName=cust.getCompanyName().trim();
				}
				else{
					custName=cust.getFullname().trim();
				}
				
				/**********tosir**********/
				
				Phrase tosirphrase= new Phrase(tosir,font12bold);
//				Paragraph tosirpara= new Paragraph();
//				tosirpara.add(tosirphrase);
//				PdfPCell custnamecell1=new PdfPCell();
//				custnamecell1.addElement(tosirpara);
//		 		custnamecell1.setBorder(0);
				
				
				
				Phrase customername= new Phrase(custName,font12bold);
			    Paragraph fullname =new Paragraph();
			    fullname.add(Chunk.NEWLINE);
			    fullname.setFont(font12bold);
			    fullname.add(tosir+"   "+custName);
			    
			    
			    
			    
			    
			    String branch="Branch Name";
			    Phrase branchphrase1=new Phrase("                   "+branch+" - "+customerbranch.getBusinessUnitName(),font10);
			    PdfPCell companybranchCell1=new PdfPCell();
			    companybranchCell1.setBorder(0);
			    companybranchCell1.addElement(branchphrase1);
			    
			    
			    
			    
			    
		 		PdfPCell custnamecell=new PdfPCell();
		 		custnamecell.addElement(fullname);
		 		custnamecell.setBorder(0);
		 		 
		 		Phrase customeradress=null;
		 		if(customerbranch.getAddress().getAddrLine1()!=null){
		 			customeradress= new Phrase("                   "+customerbranch.getAddress().getAddrLine1(),font10);
		 		}
		 		
				Phrase customeradress2=null;
				if(customerbranch.getAddress().getAddrLine2()!=null){
				   customeradress2= new Phrase("                   "+customerbranch.getAddress().getAddrLine2(),font10);
				}
				
				PdfPCell custaddress1=new PdfPCell();
				custaddress1.addElement(customeradress);
				custaddress1.setBorder(0);
				PdfPCell custaddress2=new PdfPCell();
				if(customerbranch.getAddress().getAddrLine2()!=null){
					custaddress2.addElement(customeradress2);
					custaddress2.setBorder(0);
					}
				
				
//				Phrase custlandmark=null;
				String custlandmark="";
				Phrase custlocality= null;
				String custlocality1="";
				Phrase custlocalityphrase= null;
				if(!customerbranch.getAddress().getLandmark().equals(""))
				{
					custlandmark = customerbranch.getAddress().getLandmark();
					custlocality=new Phrase("                   "+custlandmark,font10);
				}
				else{
					custlocality=new Phrase("",font10);
				}
				if(!customerbranch.getAddress().getLocality().equals("")){
					custlocality1 = customerbranch.getAddress().getLocality();
					custlocalityphrase=new Phrase("                   "+custlocality1,font10);
				}
				
					
				
				Phrase cityState=new Phrase("                   "+customerbranch.getAddress().getCity()+" - "+customerbranch.getAddress().getPin(),font10);
				
				
				
				
				
				PdfPCell custlocalitycell1=new PdfPCell();
				custlocalitycell1.addElement(custlocalityphrase);
				custlocalitycell1.setBorder(0);
				
				PdfPCell custlocalitycell=new PdfPCell();
				custlocalitycell.addElement(custlocality);
				custlocalitycell.setBorder(0);
				PdfPCell custcitycell=new PdfPCell();
				custcitycell.addElement(cityState);
				custcitycell.setBorder(0); 
//				custcitycell.addElement(Chunk.NEWLINE);
		 		 
				Phrase custcontact=new Phrase("Cell: "+customerbranch.getCellNumber1()+"",font9);
		 		PdfPCell custcontactcell=new PdfPCell();
		 		custcontactcell.addElement(custcontact);
		 		custcontactcell.setBorder(0);
		 		
		 		Phrase custemail=new Phrase("Email: "+customerbranch.getEmail(),font9);
		 		PdfPCell custemailcell=new PdfPCell();
		 		custemailcell.addElement(custemail);
		 		custemailcell.setBorder(0);
		 		
		 		Phrase contract=new Phrase("Contract Id : "+serproject.getContractId()+"",font9);
		 		PdfPCell contractcell=new PdfPCell();
		 		contractcell.addElement(contract);
		 		contractcell.setBorder(0);
		 		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		 		String contactinfo1="Start Date : "+fmt.format(serproject.getContractStartDate());
				contactinfo1=contactinfo1+"       "+"End Date : "+fmt.format(serproject.getContractEndDate())+"";
				Phrase cstduphase=new Phrase(contactinfo1,this.font10);
				PdfPCell datecell=new PdfPCell();
				datecell.addElement(cstduphase);
				datecell.setBorder(0);
			//****************rohan bhagde changes on 25/5/15*******************	
				String refInfo=null;
				if(conEntity.getRefDate()!=null){
				refInfo="Ref Date : "+fmt.format(conEntity.getRefDate());
				}else{
				refInfo="";
				}
				
				String refnoInfo=null;
				if(conEntity.getRefDate()!=null){
					refnoInfo="Ref No : "+conEntity.getRefNo();
				}else{
					refnoInfo="";
				}
				Phrase refnoInfophase=new Phrase(refInfo+"          "+refnoInfo,this.font10);
				PdfPCell nocell=new PdfPCell();
				nocell.addElement(refnoInfophase);
				nocell.setBorder(0);
				
			//*****************changes ends**********************************	
				
	/***************************************Service Day Added***********************************/
	/*******************************Scheduling Change 27 June***********************************/
				
				PdfPCell servicecell=null;
				if(!conEntity.getScheduleServiceDay().equals("Not Select")){
					
					String serviceday="Week Day : "+conEntity.getScheduleServiceDay();
					Phrase servicephrase=new Phrase(serviceday,this.font10);
					 servicecell=new PdfPCell();
					servicecell.addElement(servicephrase);
					servicecell.setBorder(0);
					
				}
	/******************************************************************************************/
				
				
		 		PdfPTable custtable=new PdfPTable(1);
		 		custtable.setWidthPercentage(100);
		 		custtable.setHorizontalAlignment(Element.ALIGN_LEFT);
		 		custtable.addCell(custnamecell);
		 		custtable.addCell(companybranchCell1);
		 		custtable.addCell(custaddress1);
		 		if(!serproject.getAddr().getAddrLine2().equals("")){
		 			custtable.addCell(custaddress2);
		 		}
		 		if(!serproject.getAddr().getLandmark().equals("")){
		 			custtable.addCell(custlocalitycell);
		 		}
		 		if(!serproject.getAddr().getLocality().equals("")){
		 			custtable.addCell(custlocalitycell1);
		 		}
		 		custtable.addCell(custcitycell);
		 		custtable.addCell(custcontactcell);
		 		custtable.addCell(custemailcell);
		 		custtable.addCell(contractcell);
		 		custtable.addCell(datecell);
		 		
		 		//***************rohan changes here********************
		 		custtable.addCell(nocell);
		 		
		 		if(!conEntity.getScheduleServiceDay().equals("Not Select")){
			 		custtable.addCell(servicecell);
			 		}
		 		
		 		// if company is true then the name of contact person
		 		
		 		if(cust.isCompany()==true&&cust.getCompanyName()!=null){
		 			Phrase realcontact=new Phrase("Contact: "+cust.getFullname(),font10);
		 			realcontact.add(Chunk.NEWLINE);
		 			PdfPCell realcontactcell=new PdfPCell();
		 			realcontactcell.addElement(realcontact);
		 			realcontactcell.setBorder(0);
		 			custtable.addCell(realcontactcell);
		 		}
		 		 
		 		
		 		PdfPTable headparenttable= new PdfPTable(2);
				headparenttable.setWidthPercentage(100);
				try {
					headparenttable.setWidths(new float[]{50,50});
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
				
				PdfPCell  companyinfocell = new PdfPCell();
				PdfPCell custinfocell = new PdfPCell();
				
				
				companyinfocell.addElement(companytable);
				custinfocell.addElement(custtable);
				headparenttable.addCell(companyinfocell);
				headparenttable.addCell(custinfocell);
				
				
				////////////////////////////////////////////////////////
				
				
				
			
				String title="Service";
				
				String countinfo="ID : "+service.getCount()+"";
				fmt.setTimeZone(TimeZone.getTimeZone("IST"));
				String creationdateinfo="";
				if(!service.getServiceTime().equals("Flexible")){
				creationdateinfo="Date : "+fmt.format(service.getServiceDate())+" "+service.getServiceTime();
				}else{
					creationdateinfo="Date: "+fmt.format(service.getServiceDate());
				}
				
				Phrase titlephrase =new Phrase(title,font14bold);
				Paragraph titlepdfpara=new Paragraph();
				titlepdfpara.add(titlephrase);
				titlepdfpara.setAlignment(Element.ALIGN_CENTER);
				titlepdfpara.add(Chunk.NEWLINE);
				PdfPCell titlepdfcell=new PdfPCell();
				titlepdfcell.addElement(titlepdfpara);
				titlepdfcell.setBorderWidthRight(0);
				titlepdfcell.setBorderWidthLeft(0);
				titlepdfcell.addElement(Chunk.NEWLINE);
				
				Phrase idphrase=new Phrase(countinfo,font14);
				Paragraph idofpdfpara=new Paragraph();
				idofpdfpara.add(idphrase);
				idofpdfpara.setAlignment(Element.ALIGN_LEFT);
				idofpdfpara.add(Chunk.NEWLINE);
				PdfPCell countcell=new PdfPCell();
				countcell.addElement(idofpdfpara);
				countcell.setBorderWidthRight(0);
				countcell.addElement(Chunk.NEWLINE);
				
				Phrase dateofpdf=new Phrase(creationdateinfo,font14);
				Paragraph creatndatepara=new Paragraph();
				creatndatepara.add(dateofpdf);
				creatndatepara.setAlignment(Element.ALIGN_RIGHT);
				creatndatepara.add(Chunk.NEWLINE);
				PdfPCell creationcell=new PdfPCell();
				creationcell.addElement(creatndatepara);
				creationcell.setBorderWidthLeft(0);
				creationcell.addElement(Chunk.NEWLINE);
				
				PdfPTable titlepdftable=new PdfPTable(3);
				titlepdftable.setWidthPercentage(100);
				titlepdftable.addCell(countcell);
				titlepdftable.addCell(titlepdfcell);
				titlepdftable.addCell(creationcell);
				
				try {
					document.add(headparenttable);
//					document.add(titlepdftable);
				} catch (DocumentException e) {
					e.printStackTrace();
					}
				}
			
			
			private void createHeadingInfo1() {

				Phrase companyName= new Phrase("                  "+comp.getBusinessUnitName().toUpperCase(),font12bold);
				//18 spaces added for setting logo 
			    Paragraph p =new Paragraph();
		        p.add(Chunk.NEWLINE);
			    p.add(companyName);
			    
			    PdfPCell companyHeadingCell=new PdfPCell();
			    companyHeadingCell.setBorder(0);
			    companyHeadingCell.addElement(p);
			    
			    String branch1="Branch Name";
			    Phrase branchphrase=new Phrase(branch1+" - "+branch.getBusinessUnitName(),font10);
			    PdfPCell companybranchCell=new PdfPCell();
			    companybranchCell.setBorder(0);
			    companybranchCell.addElement(branchphrase);
			    
			    
			    
			    Phrase adressline1= new Phrase(branch.getAddress().getAddrLine1(),font10);
				Phrase adressline2=null;
				if(branch.getAddress().getAddrLine2()!=null)
				{
					adressline2= new Phrase(branch.getAddress().getAddrLine2(),font10);
				}else{
					adressline2= new Phrase("",font10);
				}
				
				Phrase landmark=null;
				if(branch.getAddress().getLandmark()!=null)
				{
					landmark= new Phrase(branch.getAddress().getLandmark(),font10);
				}
				else
				{
					landmark= new Phrase("",font12);
				}
				
				
				Phrase locality=null;
				if(branch.getAddress().getLocality()!=null)
				{
					locality= new Phrase(branch.getAddress().getLocality(),font10);
				}
				else
				{
					locality= new Phrase("",font12);
				}
				Phrase	city=new Phrase(branch.getAddress().getCity()+" - "+branch.getAddress().getPin(),font10);
				
				
				PdfPCell citycell= new PdfPCell();
				citycell.addElement(city);
				citycell.setBorder(0);
				PdfPCell addressline1cell=new PdfPCell();
				PdfPCell addressline2cell=new PdfPCell();
				addressline1cell.addElement(adressline1);
				addressline1cell.setBorder(0);
					
				addressline2cell.addElement(adressline2);
				addressline2cell.setBorder(0);
				
				PdfPCell landmarkcell=new PdfPCell();
				landmarkcell.addElement(landmark);
				landmarkcell.setBorder(0);
				
				PdfPCell localitycell=new PdfPCell();
				localitycell.addElement(locality);
				localitycell.setBorder(0);
				
				String contactinfo="Cell: "+branch.getContact().get(0).getCellNo1();
				if(branch.getContact().get(0).getCellNo2()!=-1)
					contactinfo=contactinfo+","+branch.getContact().get(0).getCellNo2();
				if(branch.getContact().get(0).getLandline()!=-1){
					contactinfo=contactinfo+"     "+" Tel: "+branch.getContact().get(0).getLandline();
				}
				
				Phrase contactnos=new Phrase(contactinfo,font9);
				Phrase email= new Phrase("Email: "+branch.getContact().get(0).getEmail(),font9);
				
				PdfPCell contactcell=new PdfPCell();
				contactcell.addElement(contactnos);
				contactcell.setBorder(0);
				PdfPCell emailcell=new PdfPCell();
				emailcell.setBorder(0);
				emailcell.addElement(email);
				
				
				PdfPTable companytable=new PdfPTable(1);
				companytable.addCell(companyHeadingCell);
				companytable.addCell(companybranchCell);
				companytable.addCell(addressline1cell);
				if(!branch.getAddress().getAddrLine2().equals(""))
				{
					companytable.addCell(addressline2cell);
				}
				if(!branch.getAddress().getLandmark().equals("")){
					companytable.addCell(landmarkcell);
				}
				if(!branch.getAddress().getLocality().equals("")){
					companytable.addCell(localitycell);
				}
				companytable.addCell(citycell);
				companytable.addCell(contactcell);
				companytable.addCell(emailcell);
				companytable.setHorizontalAlignment(Element.ALIGN_LEFT);
				companytable.setWidthPercentage(100);

				
				/**
				 * Customer Info
				 */
				String tosir="To, M/S";
//				Phrase tosirphrase= new Phrase(tosir,font12bold);
//				Paragraph tosirpara= new Paragraph();
//				tosirpara.add(tosirphrase);
				
				String custName="";
				
				if(cust.isCompany()==true&&cust.getCompanyName()!=null){
					custName=cust.getCompanyName().trim();
				}
				else{
					custName=cust.getFullname().trim();
				}
				
				/**********tosir**********/
				
				Phrase tosirphrase= new Phrase(tosir,font12bold);
//				Paragraph tosirpara= new Paragraph();
//				tosirpara.add(tosirphrase);
//				PdfPCell custnamecell1=new PdfPCell();
//				custnamecell1.addElement(tosirpara);
//		 		custnamecell1.setBorder(0);
				
				
				
				Phrase customername= new Phrase(custName,font12bold);
			    Paragraph fullname =new Paragraph();
			    fullname.add(Chunk.NEWLINE);
			    fullname.setFont(font12bold);
			    fullname.add(tosir+"   "+custName);
			    
		 		PdfPCell custnamecell=new PdfPCell();
		 		custnamecell.addElement(fullname);
		 		custnamecell.setBorder(0);
		 		 
		 		Phrase customeradress=null;
		 		
		 			customeradress= new Phrase("                   "+serproject.getAddr().getAddrLine1().trim(),font10);
		 		
				Phrase customeradress2=null;
				if(serproject.getAddr().getAddrLine2()!=null){
				   customeradress2= new Phrase("                   "+serproject.getAddr().getAddrLine2().trim(),font10);
				}
				
				PdfPCell custaddress1=new PdfPCell();
				custaddress1.addElement(customeradress);
				custaddress1.setBorder(0);
				PdfPCell custaddress2=new PdfPCell();
				if(serproject.getAddr().getAddrLine2()!=null){
					custaddress2.addElement(customeradress2);
					custaddress2.setBorder(0);
					}
				
				
//				Phrase custlandmark=null;
				String custlandmark="";
				Phrase custlocality= null;
				String custlocality1="";
				Phrase custlocalityphrase= null;
				if(!serproject.getAddr().getLandmark().equals(""))
				{
					custlandmark = serproject.getAddr().getLandmark().trim();
					custlocality=new Phrase("                   "+custlandmark,font10);
				}
				else{
					custlocality=new Phrase("",font10);
				}
				if(!serproject.getAddr().getLocality().equals("")){
					custlocality1 = serproject.getAddr().getLocality().trim();
					custlocalityphrase=new Phrase("                   "+custlocality1,font10);
				}
				
					
				
				Phrase cityState=new Phrase("                   "+serproject.getAddr().getCity()+" - "+serproject.getAddr().getPin(),font10);
				
				
				
				
				
				PdfPCell custlocalitycell1=new PdfPCell();
				custlocalitycell1.addElement(custlocalityphrase);
				custlocalitycell1.setBorder(0);
				
				PdfPCell custlocalitycell=new PdfPCell();
				custlocalitycell.addElement(custlocality);
				custlocalitycell.setBorder(0);
				PdfPCell custcitycell=new PdfPCell();
				custcitycell.addElement(cityState);
				custcitycell.setBorder(0); 
//				custcitycell.addElement(Chunk.NEWLINE);
		 		 
				Phrase custcontact=new Phrase("Cell: "+cust.getCellNumber1()+"",font9);
		 		PdfPCell custcontactcell=new PdfPCell();
		 		custcontactcell.addElement(custcontact);
		 		custcontactcell.setBorder(0);
		 		
		 		Phrase custemail=new Phrase("Email: "+cust.getEmail(),font9);
		 		PdfPCell custemailcell=new PdfPCell();
		 		custemailcell.addElement(custemail);
		 		custemailcell.setBorder(0);
		 		
		 		Phrase contract=new Phrase("Contract Id : "+serproject.getContractId()+"",font9);
		 		PdfPCell contractcell=new PdfPCell();
		 		contractcell.addElement(contract);
		 		contractcell.setBorder(0);
		 		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		 		String contactinfo1="Start Date : "+fmt.format(serproject.getContractStartDate());
				contactinfo1=contactinfo1+"       "+"End Date : "+fmt.format(serproject.getContractEndDate())+"";
				Phrase cstduphase=new Phrase(contactinfo1,this.font10);
				PdfPCell datecell=new PdfPCell();
				datecell.addElement(cstduphase);
				datecell.setBorder(0);
			//****************rohan bhagde changes on 25/5/15*******************	
				String refInfo=null;
				if(conEntity.getRefDate()!=null){
				refInfo="Ref Date : "+fmt.format(conEntity.getRefDate());
				}else{
				refInfo="";
				}
				
				String refnoInfo=null;
				if(conEntity.getRefDate()!=null){
					refnoInfo="Ref No : "+conEntity.getRefNo();
				}else{
					refnoInfo="";
				}
				Phrase refnoInfophase=new Phrase(refInfo+"          "+refnoInfo,this.font10);
				PdfPCell nocell=new PdfPCell();
				nocell.addElement(refnoInfophase);
				nocell.setBorder(0);
				
				
			//*****************changes ends**********************************	
				/**
				 * Change for add service day on 13/6/15 by mukesh
				 */
				PdfPCell servicecell=null;
				if(!conEntity.getScheduleServiceDay().equals("Not Select")){
					
					String serviceday="Week Day : "+conEntity.getScheduleServiceDay();
					Phrase servicephrase=new Phrase(serviceday,this.font10);
					 servicecell=new PdfPCell();
					servicecell.addElement(servicephrase);
					servicecell.setBorder(0);
					
				}
				
				
				
				
		 		PdfPTable custtable=new PdfPTable(1);
		 		custtable.setWidthPercentage(100);
		 		custtable.setHorizontalAlignment(Element.ALIGN_LEFT);
		 		custtable.addCell(custnamecell);
		 		custtable.addCell(custaddress1);
		 		if(!serproject.getAddr().getAddrLine2().equals("")){
		 			custtable.addCell(custaddress2);
		 		}
		 		if(!serproject.getAddr().getLandmark().equals("")){
		 			custtable.addCell(custlocalitycell);
		 		}
		 		if(!serproject.getAddr().getLocality().equals("")){
		 			custtable.addCell(custlocalitycell1);
		 		}
		 		custtable.addCell(custcitycell);
		 		custtable.addCell(custcontactcell);
		 		custtable.addCell(custemailcell);
		 		custtable.addCell(contractcell);
		 		custtable.addCell(datecell);
		 		
		 		//***************rohan changes here********************
		 		if(conEntity.getRefDate()!=null){
		 			custtable.addCell(nocell);
		 		}
		 		
		 		//***************mukesh changes here********************
		 		if(!conEntity.getScheduleServiceDay().equals("Not Select")){
		 		custtable.addCell(servicecell);
		 		}
		 		// if company is true then the name of contact person
		 		
		 		if(cust.isCompany()==true&&cust.getCompanyName()!=null){
		 			Phrase realcontact=new Phrase("Contact: "+cust.getFullname(),font10);
		 			realcontact.add(Chunk.NEWLINE);
		 			PdfPCell realcontactcell=new PdfPCell();
		 			realcontactcell.addElement(realcontact);
		 			realcontactcell.setBorder(0);
		 			custtable.addCell(realcontactcell);
		 		}
		 		 
		 		
		 		PdfPTable headparenttable= new PdfPTable(2);
				headparenttable.setWidthPercentage(100);
				try {
					headparenttable.setWidths(new float[]{50,50});
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
				
				PdfPCell  companyinfocell = new PdfPCell();
				PdfPCell custinfocell = new PdfPCell();
				
				
				companyinfocell.addElement(companytable);
				custinfocell.addElement(custtable);
				headparenttable.addCell(companyinfocell);
				headparenttable.addCell(custinfocell);
				
				
				
				
			/////////////////////////////////////////////////////////////////////////////////////////////	
				
				
				
				
				
				
				
				
			
				String title="Service";
				
				String countinfo="ID : "+service.getCount()+"";
				fmt.setTimeZone(TimeZone.getTimeZone("IST"));
				
				/**
				 * for service time on pdf
				 */
				String creationdateinfo="";
				if(!service.getServiceTime().equals("Flexible")){
				creationdateinfo="Date : "+fmt.format(service.getServiceDate())+" "+service.getServiceTime();
				}else{
					creationdateinfo="Date: "+fmt.format(service.getServiceDate());
				}
				
				
				
				Phrase titlephrase =new Phrase(title,font14bold);
				Paragraph titlepdfpara=new Paragraph();
				titlepdfpara.add(titlephrase);
				titlepdfpara.setAlignment(Element.ALIGN_CENTER);
				titlepdfpara.add(Chunk.NEWLINE);
				PdfPCell titlepdfcell=new PdfPCell();
				titlepdfcell.addElement(titlepdfpara);
				titlepdfcell.setBorderWidthRight(0);
				titlepdfcell.setBorderWidthLeft(0);
				titlepdfcell.addElement(Chunk.NEWLINE);
				
				Phrase idphrase=new Phrase(countinfo,font14);
				Paragraph idofpdfpara=new Paragraph();
				idofpdfpara.add(idphrase);
				idofpdfpara.setAlignment(Element.ALIGN_LEFT);
				idofpdfpara.add(Chunk.NEWLINE);
				PdfPCell countcell=new PdfPCell();
				countcell.addElement(idofpdfpara);
				countcell.setBorderWidthRight(0);
				countcell.addElement(Chunk.NEWLINE);
				
				Phrase dateofpdf=new Phrase(creationdateinfo,font14);
				Paragraph creatndatepara=new Paragraph();
				creatndatepara.add(dateofpdf);
				creatndatepara.setAlignment(Element.ALIGN_RIGHT);
				creatndatepara.add(Chunk.NEWLINE);
				PdfPCell creationcell=new PdfPCell();
				creationcell.addElement(creatndatepara);
				creationcell.setBorderWidthLeft(0);
				creationcell.addElement(Chunk.NEWLINE);
				
				
				
				
				PdfPTable titlepdftable=new PdfPTable(3);
				titlepdftable.setWidthPercentage(100);
				titlepdftable.addCell(countcell);
				titlepdftable.addCell(titlepdfcell);
				titlepdftable.addCell(creationcell);
				
				
				
				try {
					document.add(headparenttable);
//					document.add(titlepdftable);
				} catch (DocumentException e) {
					e.printStackTrace();
					}
				}
			
			
			private void createServiceHeadingTable() {
				String title="Service";
				
				String countinfo="ID : "+service.getCount()+"";
				fmt.setTimeZone(TimeZone.getTimeZone("IST"));
				
				/**
				 * for service time on pdf
				 */
				String creationdateinfo="";
				if(!service.getServiceTime().equals("Flexible")){
				creationdateinfo="Date : "+fmt.format(service.getServiceDate())+" "+service.getServiceTime();
				}else{
					creationdateinfo="Date: "+fmt.format(service.getServiceDate());
				}
				
				
				
				Phrase titlephrase =new Phrase(title,font14bold);
				Paragraph titlepdfpara=new Paragraph();
				titlepdfpara.add(titlephrase);
				titlepdfpara.setAlignment(Element.ALIGN_CENTER);
				titlepdfpara.add(Chunk.NEWLINE);
				PdfPCell titlepdfcell=new PdfPCell();
				titlepdfcell.addElement(titlepdfpara);
				titlepdfcell.setBorderWidthRight(0);
				titlepdfcell.setBorderWidthLeft(0);
				titlepdfcell.addElement(Chunk.NEWLINE);
				
				Phrase idphrase=new Phrase(countinfo,font14);
				Paragraph idofpdfpara=new Paragraph();
				idofpdfpara.add(idphrase);
				idofpdfpara.setAlignment(Element.ALIGN_LEFT);
				idofpdfpara.add(Chunk.NEWLINE);
				PdfPCell countcell=new PdfPCell();
				countcell.addElement(idofpdfpara);
				countcell.setBorderWidthRight(0);
				countcell.addElement(Chunk.NEWLINE);
				
				Phrase dateofpdf=new Phrase(creationdateinfo,font14);
				Paragraph creatndatepara=new Paragraph();
				creatndatepara.add(dateofpdf);
				creatndatepara.setAlignment(Element.ALIGN_RIGHT);
				creatndatepara.add(Chunk.NEWLINE);
				PdfPCell creationcell=new PdfPCell();
				creationcell.addElement(creatndatepara);
				creationcell.setBorderWidthLeft(0);
				creationcell.addElement(Chunk.NEWLINE);
				
				
				
				
				PdfPTable titlepdftable=new PdfPTable(3);
				titlepdftable.setWidthPercentage(100);
				titlepdftable.addCell(countcell);
				titlepdftable.addCell(titlepdfcell);
				titlepdftable.addCell(creationcell);
				
				
				
				try {
					document.add(titlepdftable);
				} catch (DocumentException e) {
					e.printStackTrace();
					}
				
			}	
			
			
			
			//*******************rohan changes on 23/5/15 for customer details************************
			private void customerAndTechSign(){
				
				PdfPTable signtable=new PdfPTable(2);
				signtable.setWidthPercentage(100);
				try {
					signtable.setWidths(columnWidths4);
				} catch (DocumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				Phrase dotedline = new Phrase("------------------------------",font10bold);
				Phrase custsign= new Phrase("Customer Signature",font10bold);
				Phrase techsign= new Phrase("Technician Signature",font10bold);
				
				PdfPCell dotedcell=new PdfPCell();
				dotedcell.addElement(dotedline);
				dotedcell.setBorder(0);
				dotedcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				PdfPCell custcell=new PdfPCell();
				custcell.addElement(custsign);
				custcell.setBorder(0);
				custcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				
				PdfPCell dotedcell1=new PdfPCell();
				dotedcell1.addElement(dotedline);
				dotedcell1.setBorder(0);
				dotedcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				
				
				PdfPCell techcell=new PdfPCell();
				techcell.addElement(techsign);
				techcell.setBorder(0);
				techcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				
				
				signtable.addCell(dotedcell);
				
				signtable.addCell(dotedcell1);
				
				signtable.addCell(custcell);
				
				signtable.addCell(techcell);
				
				signtable.setSpacingBefore(50f);
				
				 try {
						document.add(signtable);
					} catch (DocumentException e) {
						e.printStackTrace();
					}
				
				
			}
			
			private void createCustomerInfo()
			{
				
				
				Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);

			
				
				Phrase custInfo= new Phrase("Customer Information",font10bold);
				Paragraph para = new Paragraph();
				para.add(custInfo);
				para.setAlignment(Element.ALIGN_CENTER);
				
				
				PdfPTable table = new PdfPTable(5);
				table.setWidthPercentage(100);
				
				Phrase category = new Phrase("CATEGORY",font1);
		        Phrase type = new Phrase("TYPE",font1);
		        Phrase group = new Phrase("GROUP",font1);
		        Phrase level = new Phrase("LEVEL",font1);
		        Phrase priority = new Phrase("PRIORITY",font1);
		        
		        PdfPCell cellcategory = new PdfPCell(category);
		        cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
		        PdfPCell celltype = new PdfPCell(type);
		        celltype.setHorizontalAlignment(Element.ALIGN_CENTER);
		        PdfPCell cellgroup = new PdfPCell(group);
		        cellgroup.setHorizontalAlignment(Element.ALIGN_CENTER);
		        PdfPCell celllevel = new PdfPCell(level);
		        celllevel.setHorizontalAlignment(Element.ALIGN_CENTER);
		        PdfPCell cellpriority = new PdfPCell(priority);
		        cellpriority.setHorizontalAlignment(Element.ALIGN_CENTER);
		        
		        
		        
		        Phrase categoryvalue = new Phrase(cust.getCategory(),font1);
		        Phrase typevalue = new Phrase(cust.getType(),font1);
		        Phrase groupvalue = new Phrase(cust.getGroup(),font1);
		        Phrase levelvalue = new Phrase(cust.getCustomerLevel(),font1);
		        Phrase priorityvalue = new Phrase(cust.getCustomerPriority(),font1);
		        
		        
		        PdfPCell cellcategoryvalue = new PdfPCell(categoryvalue);
		        cellcategoryvalue.setHorizontalAlignment(Element.ALIGN_LEFT);
		        PdfPCell celltypevalue = new PdfPCell(typevalue);
		        celltypevalue.setHorizontalAlignment(Element.ALIGN_LEFT);
		        PdfPCell cellgroupvalue = new PdfPCell(groupvalue);
		        cellgroupvalue.setHorizontalAlignment(Element.ALIGN_LEFT);
		        PdfPCell celllevelvalue = new PdfPCell(levelvalue);
		        celllevelvalue.setHorizontalAlignment(Element.ALIGN_LEFT);
		        PdfPCell cellpriorityvalue = new PdfPCell(priorityvalue);
		        cellpriorityvalue.setHorizontalAlignment(Element.ALIGN_LEFT);
		        
		        
		        
		        
		        
		        table.addCell(cellcategory);
		        table.addCell(celltype);
		        table.addCell(cellgroup);
		        table.addCell(celllevel);
		        table.addCell(cellpriority);
		        
		        table.addCell(cellcategoryvalue);
		        table.addCell(celltypevalue);
		        table.addCell(cellgroupvalue);
		        table.addCell(celllevelvalue);
		        table.addCell(cellpriorityvalue);
		        
		        
		        try {
		   				table.setTotalWidth(columnWidths3);
		   			} catch (DocumentException e1) {
		   				// TODO Auto-generated catch block
		   				e1.printStackTrace();
		   			}
		        
		        
		        PdfPTable parentTableTect=new PdfPTable(1);
				 parentTableTect.setWidthPercentage(100);
				 
				 PdfPCell prodtablecell=new PdfPCell();
				 
				
				 
					 prodtablecell.addElement(para);
					 prodtablecell.addElement(table);
					 
				
				 
				 parentTableTect.addCell(prodtablecell);
				 
				
				 try {
						document.add(parentTableTect);
					} catch (DocumentException e) {
						e.printStackTrace();
					}
			}
			
			
			
			private void customerFeedback()
			{
				
				if(service.getStatus().equals(Service.SERVICESTATUSCOMPLETED)){
					
					System.out.println("Completed status............");
					Font font1 = new Font(Font.FontFamily.HELVETICA  , 8);
					Font font8 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
					
					Phrase custFback= new Phrase("Customer Feedback : ",font10bold);
					Paragraph para = new Paragraph();
					para.add(custFback);
					para.setAlignment(Element.ALIGN_LEFT);
					
					PdfPTable feedtable = new PdfPTable(6);
					feedtable.setWidthPercentage(100);
					
//					Phrase rate = new Phrase("RATE :",font8);
					
					Phrase poor = null;
					if (service.getCustomerFeedback() != null) {
						poor = new Phrase("RATE :  "+service.getCustomerFeedback(), font8);
					} else {
						poor = new Phrase("RATE :", font8);
					}
					
					Phrase blank = new Phrase("", font1);
					PdfPCell blankcell = new PdfPCell(blank);
					blankcell.setBorder(0);
					
//					PdfPCell cellrate = new PdfPCell(rate);
//					cellrate.setBorder(0);
//					cellrate.setHorizontalAlignment(Element.ALIGN_LEFT);
					PdfPCell cellpoor = new PdfPCell(poor);
					cellpoor.setBorder(0);
			        cellpoor.setHorizontalAlignment(Element.ALIGN_LEFT);
			        
			        
			        feedtable.addCell(cellpoor);
			        feedtable.addCell(blankcell);
			        feedtable.addCell(blankcell);
			        feedtable.addCell(blankcell);
			        feedtable.addCell(blankcell);
			        feedtable.addCell(blankcell);
			        
			        
			        PdfPCell feedtablecell = new PdfPCell(feedtable);
			        feedtablecell.setBorder(0);
			        
			        PdfPTable parentTableTect=new PdfPTable(1);
			    	parentTableTect.setWidthPercentage(100);
			    	
			    	PdfPCell prodtablecell=new PdfPCell();
					prodtablecell.addElement(Chunk.NEWLINE);
					prodtablecell.addElement(Chunk.NEWLINE);
					prodtablecell.addElement(Chunk.NEWLINE);
					prodtablecell.setBorder(0);
					 
				 
//				 parentTableTect.addCell(prodtablecell);
				 parentTableTect.addCell(feedtablecell);
				
					try {
						document.add(Chunk.NEWLINE);
						document.add(Chunk.NEWLINE);
						document.add(para);
						document.add(parentTableTect);
					} catch (DocumentException e) {
						e.printStackTrace();
					}
					
				}else{
				
				
				
				Font font1 = new Font(Font.FontFamily.HELVETICA  , 8);
				Font font8 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
				
				Phrase custFback= new Phrase("Customer Feedback : ",font10bold);
				Paragraph para = new Paragraph();
				para.add(custFback);
				para.setAlignment(Element.ALIGN_LEFT);
				
				PdfPTable feedtable = new PdfPTable(6);
				feedtable.setWidthPercentage(100);
				
				Phrase rate = new Phrase("RATE :",font8);
				Phrase poor = new Phrase("[   ] POOR ",font1);
				Phrase avg = new Phrase("[   ] AVERAGE ",font1);
				Phrase good = new Phrase("[   ] GOOD ",font1);
				Phrase verygood = new Phrase("[   ] VERY GOOD ",font1);
				Phrase execellent = new Phrase("[   ]  EXCELLENT ",font1);
				
		        
				PdfPCell cellrate = new PdfPCell(rate);
				cellrate.setBorder(0);
				cellrate.setHorizontalAlignment(Element.ALIGN_LEFT);
				PdfPCell cellpoor = new PdfPCell(poor);
				cellpoor.setBorder(0);
		        cellpoor.setHorizontalAlignment(Element.ALIGN_CENTER);
		        PdfPCell cellavg = new PdfPCell(avg);
		        cellavg.setBorder(0);
		        cellavg.setHorizontalAlignment(Element.ALIGN_CENTER);
		        PdfPCell cellgood = new PdfPCell(good);
		        cellgood.setBorder(0);
		        cellgood.setHorizontalAlignment(Element.ALIGN_CENTER);
		        PdfPCell cellverygood = new PdfPCell(verygood);
		        cellverygood.setBorder(0);
		        cellverygood.setHorizontalAlignment(Element.ALIGN_CENTER);
		        PdfPCell cellexecellent = new PdfPCell(execellent);
		        cellexecellent.setBorder(0);
		        cellexecellent.setHorizontalAlignment(Element.ALIGN_CENTER);
		        
		        feedtable.addCell(cellrate);
		        feedtable.addCell(cellpoor);
		        feedtable.addCell(cellavg);
		        feedtable.addCell(cellgood);
		        feedtable.addCell(cellverygood);
		        feedtable.addCell(cellexecellent);
		        
		        
		        PdfPCell feedtablecell = new PdfPCell(feedtable);
		        feedtablecell.setBorder(0);
		        
		        PdfPTable parentTableTect=new PdfPTable(1);
		    	parentTableTect.setWidthPercentage(100);
		    	
		    	PdfPCell prodtablecell=new PdfPCell();
				prodtablecell.addElement(Chunk.NEWLINE);
				prodtablecell.addElement(Chunk.NEWLINE);
				prodtablecell.addElement(Chunk.NEWLINE);
				prodtablecell.setBorder(0);
				 
			 
			 parentTableTect.addCell(prodtablecell);
			 parentTableTect.addCell(feedtablecell);
			
			 try {
				 	document.add(Chunk.NEWLINE);
				 	document.add(Chunk.NEWLINE);
					document.add(para);
					document.add(parentTableTect);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
				}
		    	
			}
			
			//****************************changes ends here***********************************************
			
			private void createMaterialReq() {
				
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);

		Phrase productphrase = new Phrase("Product : "+ service.getProductName(), font10bold);

		Paragraph productpara = new Paragraph(productphrase);

		Phrase productdesc = null;
		System.out.println("Product....size  === " + this.stringlis.size());

		for (int i = 0; i < this.stringlis.size(); i++) {

			if (!stringlis.get(i).getComment().equals("")) {
				productdesc = new Phrase(stringlis.get(i).getComment(), font8);
				System.out.println("pro coment+++   "
						+ stringlis.get(i).getComment());
			} else {

				productdesc = new Phrase("", font8);
			}

		}
				
				 
				 
				
//				if(service.getComment()!=null){
//					
//					productdesc = new Phrase(service.getComment(),font8);
//					
//				}else{
//					productdesc = new Phrase("",font8);
//				}
				
				Paragraph prodesc=new Paragraph(productdesc); 
				
				PdfPTable table = new PdfPTable(6);
				table.setWidthPercentage(100);
				Paragraph para = null;
			
					
					Phrase productdetails= new Phrase("Material Required",font10bold);
					 para = new Paragraph();
					para.add(productdetails);
					para.setAlignment(Element.ALIGN_CENTER);
					
					
					
					Phrase servicephase = new Phrase("SR. NO.",font1);
			        Phrase productidphrase = new Phrase("CODE",font1);
			        Phrase namephrase = new Phrase("ITEM NAME",font1);
			        Phrase codephrase = new Phrase("QTY",font1);
			        Phrase titlephrase= new Phrase ("UNIT",font1);
			        Phrase pgidphrase = new Phrase("RECEIVED",font1);
			        
			        PdfPCell cellservice = new PdfPCell(servicephase);
			        cellservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			        PdfPCell cellproductid = new PdfPCell(productidphrase);
			        cellproductid.setHorizontalAlignment(Element.ALIGN_CENTER);
			        PdfPCell cellname = new PdfPCell(namephrase);
			        cellname.setHorizontalAlignment(Element.ALIGN_CENTER);
			        PdfPCell cellcode = new PdfPCell(codephrase);
			        cellcode.setHorizontalAlignment(Element.ALIGN_CENTER);
			        PdfPCell celltitle = new PdfPCell(titlephrase);
			        celltitle.setHorizontalAlignment(Element.ALIGN_CENTER);
			        PdfPCell cellpgid = new PdfPCell(pgidphrase);
			        cellpgid.setHorizontalAlignment(Element.ALIGN_CENTER);
			        
			        
			        table.addCell(cellservice);
			        table.addCell(cellproductid);
			        table.addCell(cellname);
			        table.addCell(cellcode);
			        table.addCell(celltitle);
			        table.addCell(cellpgid);
			        
			        try {
						table.setWidths(columnWidths);
					} catch (DocumentException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			        
//			    	if(prodGrpEntity!=null){
			    		
			        for(int i = 0;i<this.prodList.size();i++){
			        	
			        	Phrase chunk = null;
			      	  chunk = new Phrase((i+1)+"",font8);
			      	 	
			      	  PdfPCell pdfservicenocell = new PdfPCell(chunk);
			      	pdfservicenocell.setHorizontalAlignment(Element.ALIGN_CENTER);
			      	 System.out.println("Title iiss === "+prodList.get(i).getTitle());
			      	 if(prodList.get(i).getCode()!=null){ 
			      	 chunk=new Phrase(prodList.get(i).getCode(),font8);
			      	 System.out.println("Title iiss === "+prodList.get(i).getCode());
			      	 }else{
			      		chunk = new Phrase(" ",font8);
			      	 }
			      	 
			      	 PdfPCell pdfprodidcell = new PdfPCell(chunk);
			      	if(prodList.get(i).getName()!=null){
			      	 chunk = new Phrase(prodList.get(i).getName(),font8);
			      	}else{
			      		chunk = new Phrase(" ",font8);
			      	}
			      	 PdfPCell pdfprodnamecell = new PdfPCell(chunk);
			      
			      	if(prodList.get(i).getQuantity()!=0){
			         	 chunk = new Phrase(prodList.get(i).getQuantity()+"",font8);
			         	}else{
			         		chunk = new Phrase(" ",font8);
			         	}
			         	 PdfPCell pdfcodecell = new PdfPCell(chunk);
			         	pdfcodecell.setHorizontalAlignment(Element.ALIGN_CENTER);
			         	
			         	if(prodList.get(i).getUnit()!=null){
			             	 chunk = new Phrase(prodList.get(i).getUnit(),font8);
			             	}else{
			             		chunk = new Phrase(" ",font8);
			             	}
			             	 PdfPCell pdftitlecell = new PdfPCell(chunk);
			             	pdftitlecell.setHorizontalAlignment(Element.ALIGN_CENTER);
			             	 Phrase recphrase=new Phrase("Yes / No",font8);
			             	 
			             	 PdfPCell recevivedcellCell=new PdfPCell(recphrase);
			             	recevivedcellCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			             	 
			             	
			                	table.addCell(pdfservicenocell);
			                  	table.addCell(pdfprodidcell);
			                  	table.addCell(pdfprodnamecell);
			                  	table.addCell(pdfcodecell);
			                  	table.addCell(pdftitlecell);
			                	table.addCell(recevivedcellCell);
			                  	
			                  	 try {
			             			table.setWidths(columnWidths);
			             		} catch (DocumentException e1) {
			             			// TODO Auto-generated catch block
			             			e1.printStackTrace();
			             		}
			      
			        	}
					
//				}
				
				
						 PdfPTable parentTableTect=new PdfPTable(1);
						 parentTableTect.setWidthPercentage(100);
						 
						 PdfPCell prodtablecell=new PdfPCell();
						 
						 prodtablecell.addElement(productpara);
						 prodtablecell.addElement(prodesc);
						 
							 prodtablecell.addElement(para);
							 prodtablecell.addElement(table);
							 
						
						 
						 parentTableTect.addCell(prodtablecell);
						 
						
						 try {
								document.add(parentTableTect);
							} catch (DocumentException e) {
								e.printStackTrace();
							}
				
					}
			
			
			
			
			
			private void createTechnicianinfo() {

				Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
				Phrase productdetails= new Phrase("Technician Information",font10bold);
				Paragraph para = new Paragraph();
				para.add(productdetails);
				para.setAlignment(Element.ALIGN_CENTER);
				PdfPTable table = new PdfPTable(4);
				table.setWidthPercentage(100);
				
				Phrase tectid = new Phrase("EMPLOYEE ID",font1);
		        Phrase tectname = new Phrase("NAME",font1);
		        Phrase tectcell = new Phrase("CELL",font1);
		        Phrase branchcell = new Phrase("BRANCH",font1);
		        
		        PdfPCell celltectid = new PdfPCell(tectid);
		        celltectid.setHorizontalAlignment(Element.ALIGN_CENTER);
		        PdfPCell celltectname = new PdfPCell(tectname);
		        celltectname.setHorizontalAlignment(Element.ALIGN_CENTER);
		        PdfPCell celltectcell = new PdfPCell(tectcell);
		        celltectcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		        PdfPCell celltectbranch = new PdfPCell(branchcell);
		        celltectbranch.setHorizontalAlignment(Element.ALIGN_CENTER);
		        
		        table.addCell(celltectid);
		        table.addCell(celltectname);
		        table.addCell(celltectcell);
		        table.addCell(celltectbranch);
				
		        try {
					table.setTotalWidth(columnWidths1);
				} catch (DocumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		        
		        
		        for(int i = 0;i<this.technicians.size();i++){
		        	
		        	Phrase chunk = null;
		     	if(technicians.get(i).getCount()+""!=null){
		      	  chunk = new Phrase(technicians.get(i).getEmpCount()+"",font8);
		     	}
		      	 	PdfPCell pdfdayscell = new PdfPCell(chunk);
//		      	 pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
		      	 	
		      	 if(technicians.get(i).getFullName()!=null){ 
		      	 chunk=new Phrase(technicians.get(i).getFullName()+"",font8);
		      	 }else{
		      		chunk = new Phrase(" ",font8);
		      	 }
		      	 
		      	 PdfPCell pdfpercentcell = new PdfPCell(chunk);
//		      	pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		      	if(technicians.get(i).getCellNumber()!=null){
		      	 chunk = new Phrase(technicians.get(i).getCellNumber()+"",font8);
		      	}else{
		      		chunk = new Phrase(" ",font8);
		      	}
		      	 PdfPCell pdfcommentcell = new PdfPCell(chunk);
//		      	pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		      
		      	 
		   	if(technicians.get(i).getBranch()!=null){
		   	 chunk = new Phrase(technicians.get(i).getBranch(),font8);
		   	}else{
		   		chunk = new Phrase(" ",font8);
		   	}
		   	 PdfPCell pdfbranchcell = new PdfPCell(chunk);
//		   	pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		      	 
		      	 
		      	table.addCell(pdfdayscell);
		      	table.addCell(pdfpercentcell);
		      	table.addCell(pdfcommentcell);
		    	table.addCell(pdfbranchcell);
		    	
		    	try {
					table.setTotalWidth(columnWidths1);
				} catch (DocumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		    	
		        }
		        
		        PdfPTable parentTableTect=new PdfPTable(1);
		        parentTableTect.setWidthPercentage(100);
		        
		        PdfPCell prodtablecell=new PdfPCell();
		        prodtablecell.addElement(para);
		        prodtablecell.addElement(table);
		        parentTableTect.addCell(prodtablecell);
		        
		       
		        try {
					document.add(parentTableTect);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
			}
			
			
					private void createTools() {

						Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
						
						Phrase productdetails= new Phrase("Tools",font10bold);
						Paragraph para = new Paragraph();
						para.add(productdetails);
						para.setAlignment(Element.ALIGN_CENTER);
						
						PdfPTable table = new PdfPTable(5);
						table.setWidthPercentage(100);
						
						Phrase namephase = new Phrase("NAME",font1);
				        Phrase brandphrase = new Phrase("BRAND",font1);
				        Phrase modelnophrase = new Phrase("MODEL NO.",font1);
				        Phrase srnophrase = new Phrase("SR. NO.",font1);
				        Phrase pgidphrase = new Phrase("RECEIVED",font1);
				        
//				        Phrase installdatephrase = new Phrase("INSTALLATION DATE",font1);
				        
				        PdfPCell cellservice = new PdfPCell(namephase);
				        cellservice.setHorizontalAlignment(Element.ALIGN_CENTER);
				        PdfPCell cellproductid = new PdfPCell(brandphrase);
				        cellproductid.setHorizontalAlignment(Element.ALIGN_CENTER);
				        PdfPCell cellname = new PdfPCell(modelnophrase);
				        cellname.setHorizontalAlignment(Element.ALIGN_CENTER);
				        PdfPCell cellcode = new PdfPCell(srnophrase);
				        cellcode.setHorizontalAlignment(Element.ALIGN_CENTER);
				        PdfPCell celltitle = new PdfPCell(pgidphrase);
				        celltitle.setHorizontalAlignment(Element.ALIGN_CENTER);
//				        PdfPCell cellpgid = new PdfPCell(installdatephrase);
//				        cellpgid.setHorizontalAlignment(Element.ALIGN_CENTER);
				        
				        
				        table.addCell(cellservice);
				        table.addCell(cellproductid);
				        table.addCell(cellname);
				        table.addCell(cellcode);
				        table.addCell(celltitle);
//				        table.addCell(cellpgid);
				        
				        try {
							table.setWidths(columnWidths3);
						} catch (DocumentException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
				        
				        
				 for(int i = 0;i<this.tooltable.size();i++){
				        	
				        	Phrase chunk = null;
				     	if(tooltable.get(i).getName()!=null){
				      	  chunk = new Phrase(tooltable.get(i).getName(),font8);
				     	}
				      	 	PdfPCell pdfservicenocell = new PdfPCell(chunk);
				      	 	
				      	 if(tooltable.get(i).getBrand()!=null){ 
				      	 chunk=new Phrase(tooltable.get(i).getBrand(),font8);
				      	 }else{
				      		chunk = new Phrase(" ",font8);
				      	 }
				      	 
				      	 PdfPCell pdfprodidcell = new PdfPCell(chunk);
				      	if(tooltable.get(i).getModelNo()!=null){
				      	 chunk = new Phrase(tooltable.get(i).getModelNo(),font8);
				      	}else{
				      		chunk = new Phrase(" ",font8);
				      	}
				      	 PdfPCell pdfprodnamecell = new PdfPCell(chunk);
				      
				      	if(tooltable.get(i).getSrNo()!=null){
				         	 chunk = new Phrase(tooltable.get(i).getSrNo(),font8);
				         	}else{
				         		chunk = new Phrase(" ",font8);
				         	}
				         	 PdfPCell pdfcodecell = new PdfPCell(chunk);
				         	 
				         	 
				         	Phrase recphrase=new Phrase("Yes / No",font8);
			             	 
			             	 PdfPCell recevivedcellCell=new PdfPCell(recphrase);
			             	recevivedcellCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				         	 
				         	 
//				         	pdfcodecell.setHorizontalAlignment(Element.ALIGN_CENTER);
				         	
				         	
//				         	if(tooltable.get(i).getDateOfManufacture()!=null){
//				             	 chunk = new Phrase(fmt1.format(tooltable.get(i).getDateOfManufacture()),font8);
//				             	}else{
//				             		chunk = new Phrase(" ",font8);
//				             	}
//				             	 PdfPCell pdftitlecell = new PdfPCell(chunk);
//				             	pdftitlecell.setHorizontalAlignment(Element.ALIGN_CENTER);
				             	
//				             	if(tooltable.get(i).getDateOfInstallation()!=null){
//				                 	 chunk = new Phrase(fmt1.format(tooltable.get(i).getDateOfInstallation()),font8);
//				                 	}else{
//				                 		chunk = new Phrase(" ",font8);
//				                 	}
//				                 	 PdfPCell pdfpgidcell = new PdfPCell(chunk);
//				                 	pdfpgidcell.setHorizontalAlignment(Element.ALIGN_CENTER);
				      	
				                	table.addCell(pdfservicenocell);
				                  	table.addCell(pdfprodidcell);
				                  	table.addCell(pdfprodnamecell);
				                  	table.addCell(pdfcodecell);
				                  	table.addCell(recevivedcellCell);
//				                  	table.addCell(pdfpgidcell);
				      	
				                  	
				                  	try {
				    					table.setWidths(columnWidths3);
				    				} catch (DocumentException e1) {
				    					// TODO Auto-generated catch block
				    					e1.printStackTrace();
				    				}
				      
				        	}
								 PdfPTable parentTableTect=new PdfPTable(1);
								 parentTableTect.setWidthPercentage(100);
								 
								 PdfPCell prodtablecell=new PdfPCell();
								 prodtablecell.addElement(para);
								 prodtablecell.addElement(table);
								 parentTableTect.addCell(prodtablecell);
								 
								
								 try {
										document.add(parentTableTect);
									} catch (DocumentException e) {
										e.printStackTrace();
									}
						
						
					}
				}

