package com.slicktechnologies.server.addhocprinting;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class PcambServiceVoucherPdf 
{
public	Document document;
	    Company comp;
	    Contract con;
	    Customer cust;
	    Service ser;   
	    Invoice invoiceentity;   
	
	private Font font16boldul, font12bold, font8bold, font9bold,  font8, //font9,
	font12boldul, font10boldul, font12, font16bold, font10, font10bold,font9boldul,font10ul,
	font14bold,font7,font7bold,font9red,font9boldred,font12boldred,font16,font23,font20;
	
	private SimpleDateFormat fmt1 = new SimpleDateFormat("dd MMM yyyy");
	DecimalFormat df=new DecimalFormat("0.00");
	Logger logger = Logger.getLogger("PcambServiceVoucherPdf.class");
    public PcambServiceVoucherPdf() 
    {
    	
    	font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
    	font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
    	font16 = new Font(Font.FontFamily.HELVETICA, 16);
    	font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
    	font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
    	font8 = new Font(Font.FontFamily.HELVETICA, 8);
    	font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
    	font12 = new Font(Font.FontFamily.HELVETICA, 12);
    	font12boldred = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,BaseColor.RED);
    	font10 = new Font(Font.FontFamily.HELVETICA, 10);
    	font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
    	font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
    	font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
    	font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
    	font9boldred = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD,BaseColor.RED);
//    	font9 = new Font(Font.FontFamily.HELVETICA, 9);
    	font10ul= new Font(Font.FontFamily.HELVETICA, 10, Font.UNDERLINE); 
    	font9red = new Font(Font.FontFamily.HELVETICA, 9,Font.NORMAL, BaseColor.RED);
    	font7 = new Font(Font.FontFamily.HELVETICA, 7);
    	font7bold = new Font(Font.FontFamily.HELVETICA, 7,Font.BOLD);
    	font23= new Font(Font.FontFamily.HELVETICA, 23);
    	font20= new Font(Font.FontFamily.HELVETICA, 20);
    	font9boldul = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD| Font.UNDERLINE);
	}
   
	
	public void setContractserviceVoucher(Long count) 
	{
		
		//Load Contract
		    con = ofy().load().type(Contract.class).id(count).now();
	   
		// load Company
		if(con.getCompanyId()==null)
			   comp=ofy().load().type(Company.class).first().now();    
			else
			   comp=ofy().load().type(Company.class).filter("companyId",con.getCompanyId()).first().now();
//		     System.out.println("company loaded "+comp.getCellNumber2());
		//Load Invoice
		
		if(con.getCompanyId()==null)
				invoiceentity=ofy().load().type(Invoice.class).first().now();
		else		
				invoiceentity=ofy().load().type(Invoice.class).filter("companyId",con.getCompanyId()).first().now();
				
		// load Customer
		if(con.getCompanyId()==null)
		cust = ofy().load().type(Customer.class).first().now();
		else
	    cust=ofy().load().type(Customer.class).filter("companyId", con.getCompanyId()).filter("count", con.getCustomerId()).first().now();
		
	// load Service
		
		if (con.getCompanyId() == null)
			ser = ofy().load().type(Service.class).first().now();
		  
		else
			
		    ser = ofy().load().type(Service.class).filter("companyId", con.getCompanyId()).filter("contractCount", con.getCount()).first().now();
		
			fmt1.setTimeZone(TimeZone.getTimeZone("IST"));  
	}

	public void createPdf() {
		
		for(int i = 0; i<con.getItems().size();i++)
		{
			CreateHeader(); 	
		   voucherDetails(con.getItems().get(i).getProductName());
		}
	}

	private void CreateHeader() 
	
	{
//		System.out.println("landline: " +comp.getLandline());
//		System.out.println("Cell no 2: " +comp.getCellNumber2());
//		System.out.println("fax No: " +comp.getFaxNumber());
		
		
		Phrase myHeader1=new Phrase(""+comp.getBusinessUnitName().toUpperCase(),font10bold);
//		Phrase	 myHeader3=new Phrase(" ");
////			 myHeader3=new Phrase("            "+"TEL: "+comp.getLandline()+" , "+comp.getCellNumber2()+" FAX :"+comp.getFaxNumber(),font9boldul);
//		
//		if(comp.getLandline()!=null && comp.getCellNumber2()!= null && comp.getFaxNumber()!= null)
//		{     
//			System.out.println("Values are not null condn ");
//			 myHeader3=new Phrase("            "+"TEL: "+comp.getLandline()+" , "+comp.getCellNumber2()+" FAX :"+comp.getFaxNumber(),font9boldul);
//
//		}
//		else if(comp.getLandline()==null && comp.getCellNumber2()!= null && comp.getFaxNumber()!= null)
//		{
//			System.out.println("landline null condn ");
//			myHeader3=new Phrase("            "+"TEL: "+comp.getCellNumber2()+" FAX :"+comp.getFaxNumber(),font9boldul);
//
//		}
//		else if(comp.getLandline()!=null && comp.getCellNumber2()== null && comp.getFaxNumber()!= null)
//		{ 
//			System.out.println("cell2 null condn ");
//			 myHeader3=new Phrase("            "+"TEL: "+comp.getLandline()+" FAX :"+comp.getFaxNumber(),font9boldul);
//
//		}
//		
//		else if(comp.getLandline()==null && comp.getCellNumber2()== null && comp.getFaxNumber()!= null)
//		{
//			System.out.println("landline cell2  null condn ");
//			 myHeader3=new Phrase("            "+"TEL: "+"          "+" FAX :"+comp.getFaxNumber(),font9boldul);
//
//		}
//		/////////////////////////////////////////////////////////////////////////////////////////
//		else if(comp.getLandline()==null && comp.getCellNumber2()!= null && comp.getFaxNumber()== null)
//		{  
//			System.out.println("landline fax no null condn ");
//			 myHeader3=new Phrase("            "+"TEL: "+comp.getCellNumber2()+" FAX :"+"    ",font9boldul);
//  
//		}
//		else if(comp.getLandline()!=null && comp.getCellNumber2()== null && comp.getFaxNumber()== null)
//		{
//			
//			System.out.println("cell2 faxno   null condn ");
//			 myHeader3=new Phrase("            "+"TEL: "+comp.getLandline()+" FAX :"+"    ",font9boldul);
//
//		}
//		
//		else if(comp.getLandline()==null && comp.getCellNumber2()== null && comp.getFaxNumber()== null)
//		{
//			
//			System.out.println("all values  null condn ");
//			 myHeader3=new Phrase("            "+"TEL: "+"          "+" FAX :"+"     ",font9boldul);
//
//		}
//		
//		else if(comp.getLandline()!=null && comp.getCellNumber2()!= null && comp.getFaxNumber()== null)
//		{
//			
//			System.out.println("all values  null condn ");
//			 myHeader3=new Phrase("            "+"TEL: "+comp.getLandline()+" , "+comp.getCellNumber2()+" FAX :"+"                   .",font9boldul);
//
//		}
//		else      
//		{    
//			System.out.println("all values  null condn ");
//			 myHeader3=new Phrase("            "+"TEL: "+"          "+" FAX :"+"     ",font9boldul);
//
//		}    
		
		
/////////////////////////// As per the mail reqiured format hardcoded in contacts details ////////////////////////////////     
		
//String contactinfo = "Tel.: (91-22) 2266 1091, 2262 5376 Fax : (91-22) 2266 0810 ";
Phrase contactInfo = new Phrase ("Tel.: (91-22) 2266 1091, 2262 5376 Fax : (91-22) 2266 0810 ",font10ul);
///////////////////////////////   END HERE  ////////////////////////////////  
		
		Paragraph headerPara = new Paragraph();
		headerPara.add(myHeader1);                       
		headerPara.add(Chunk.NEWLINE);
		headerPara.add(contactInfo);
		headerPara.add(Chunk.NEWLINE);   
		headerPara.setAlignment(Element.ALIGN_LEFT);   
		PdfPCell header =new PdfPCell();
		header.addElement(headerPara);
		header.setBorder(0);   
		  
		Phrase blnkphrse = new Phrase ("",font10);
		PdfPCell blnkCell = new PdfPCell();
		blnkCell.addElement(blnkphrse);
		blnkCell.setBorder(0);

		Phrase blnk1phrse = new Phrase ("",font10);
		PdfPCell blnk1Cell = new PdfPCell();
		blnk1Cell.addElement(blnk1phrse);
		blnk1Cell.setBorder(0);
		
		  
		PdfPTable headertbl = new PdfPTable(3);
		headertbl.setWidthPercentage(100);
		headertbl.addCell(blnkCell);
		headertbl.addCell(header);
		headertbl.addCell(blnk1Cell);
		
	
		try {
			headertbl.setWidths(new float [] {10,80,10});
			document.add(headertbl);
		} catch (DocumentException e) 
		{
			e.printStackTrace();
		}
	}
	
	
	
	private void voucherDetails(String productName)
	      { 
	    	  
	    PdfPTable voucherDetails1 = new PdfPTable(5);
	    voucherDetails1.setWidthPercentage(100);
	    
		Phrase blnkphrse = new Phrase ("",font10);
		PdfPCell blnkCell = new PdfPCell();  
		blnkCell.addElement(blnkphrse);
		blnkCell.setBorder(0);
		
	    String salutation = "" ;
		if (cust.getSalutation()!= null && !cust.getSalutation().equals("")){
			salutation = cust.getSalutation();
		}
		else{  
			 salutation = "" ;
		}
		String custNamestr = "";
		
		if (!salutation.equals("")){
		 
		if(ser!=null)
		{
	      custNamestr =   salutation  + " "+ ser.getPersonInfo().getFullName();
		}
		else {
			custNamestr =   salutation  + " "+ con.getCustomerFullName();  
		}
		}
		else
		{
			if(ser!=null)
			{
		      custNamestr = ser.getPersonInfo().getFullName();
			}
			else {
				custNamestr = con.getCustomerFullName();  
				
			}
		}
		/*
		 * nameInFirstLetterUpperCase defined by Ajinkya  suggested by Rahul V 
		 * Used to take customer name from Uppercase to Only first letter Uppercase
		 */
		logger.log(Level.SEVERE, "Customer Name"+custNamestr.trim());
		String nameInFirstLetterUpperCase=getFirstLetterUpperCase(custNamestr.trim()); 
		 
                           ////////////////////// end here ///////////////// 
	
		
	    Phrase custNamePhrse = new Phrase (nameInFirstLetterUpperCase ,font10);
	    PdfPCell custNameCell = new PdfPCell();
	    custNameCell.addElement(custNamePhrse);	
	    custNameCell.setBorder(0);
	        
	    Calendar calobj = Calendar.getInstance();
	    System.out.println(fmt1.format(calobj.getTime()));
		Phrase datePhrs=new Phrase("          DATE : "+fmt1.format(calobj.getTime()),font10);
	    
	   
		PdfPCell dateCell=new PdfPCell(datePhrs);
		dateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		dateCell.setBorder(0);
		
		Phrase voucherPhrs=new Phrase("          VOUCHER NO : ",font10);
	   if(ser!=null) {
		       voucherPhrs=new Phrase("          VOUCHER NO : "+ser.getContractCount(),font10);
	   }
	   else{
		       voucherPhrs=new Phrase("          VOUCHER NO : "+con.getContractCount(),font10);
	   }
		PdfPCell voucherCell=new PdfPCell(voucherPhrs);
		voucherCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		voucherCell.setBorder(0);
		
		voucherDetails1.addCell(blnkCell);
		voucherDetails1.addCell(blnkCell);
		voucherDetails1.addCell(custNameCell);
		voucherDetails1.addCell(dateCell);
		voucherDetails1.addCell(blnkCell);
		
		voucherDetails1.addCell(blnkCell);
		voucherDetails1.addCell(blnkCell);
		voucherDetails1.addCell(blnkCell);
		voucherDetails1.addCell(voucherCell);
		voucherDetails1.addCell(blnkCell);
		   
		try {
			
			voucherDetails1.setWidths(new float[]{10,5,30,30,25});
			document.add(voucherDetails1);
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		PdfPTable premisesDetails = new PdfPTable(6);
		premisesDetails.setWidthPercentage(100);
		
		PdfPCell PremiseslblCell = new PdfPCell ();
		Phrase premisesLbl = new Phrase ("Premises to be Treated : ",font10 );
		PremiseslblCell.addElement(premisesLbl);
		PremiseslblCell.setBorder(0);
		
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		PdfPCell premisesValCell = new PdfPCell ();
          String addressline="";
          String localityline=null;
          Phrase serAdrs = new Phrase ("");
          if(ser!=null)
          {		
		if(ser.getAddress().getAddrLine2().equals(null)&& ser.getAddress().getAddrLine2().equals("")){
			addressline = ser.getAddress().getAddrLine1()+", "+ser.getAddress().getAddrLine2()+", "+"\n";
		}
		else
		{
			addressline=ser.getAddress().getAddrLine1()+", "+"\n";
		}
		
		if((!ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition1");
			localityline= (ser.getAddress().getLandmark()+", "+ser.getAddress().getLocality()+", "+"\n"+ser.getAddress().getCity()+"- "
				      +ser.getAddress().getPin()+", "+"\n" +ser.getAddress().getState()+", "+ser.getAddress().getCountry()+". ");
		}
		else if((!ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 2");
			localityline= (ser.getAddress().getLandmark()+", "+"\n"+ ser.getAddress().getCity()+" - "
				      +ser.getAddress().getPin()+", "+"\n"+ser.getAddress().getState()+", "+ser.getAddress().getCountry()+". ");
		}
		
		else if((ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition 3");
			localityline= (ser.getAddress().getLocality()+", "+"\n"+ser.getAddress().getCity()+" - "
				      +ser.getAddress().getPin()+", "+"\n"+ser.getAddress().getState()+", "+ser.getAddress().getCountry()+". ");
		}
		else if((ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 4");
			localityline=(ser.getAddress().getCity()+", "+ser.getAddress().getPin()+", "+"\n"+ser.getAddress().getState()+", "+ser.getAddress().getCountry()+". ");
		}
	      
		 serAdrs = new Phrase (addressline + localityline,font10);
		/////////////////////////////////////////////////////////////////////////////////////////////////
		
	    String salutation1 = "" ;
		if (cust.getSalutation()!= null && !cust.getSalutation().equals("")){
			salutation1 = cust.getSalutation();
		}   
		else{  
			 salutation1 = "" ;
		}
		
		String  serCustName = "";	
       	if(!salutation.equals("")&& salutation!=null )	
       	{
       		
       		if(ser!=null)
       		{
       		  serCustName = salutation1 +" "+ ser.getCustomerName();
       		}
       		else 
       		{
       		  serCustName = salutation1 +" "+ con.getCustomerFullName();	
       		}
       	}
       	else
       	{
       		if(ser!=null)
       		{
       		  serCustName =   ser.getCustomerName();
       		}
       		else 
       		{
       		  serCustName =   con.getCustomerFullName();	    
       		}
       	}
		/*
		 * nameInFirstLetterUpperCase defined by Ajinkya  suggested by Rahul V 
		 * Used to take customer name from Uppercase to Only first letter Uppercase
		 */
		String nameInFirstLetterUpperCase1=getFirstLetterUpperCase(serCustName.trim());   // 2 
                           ////////////////////// end here ///////////////// 
		
		Phrase custName = new Phrase ( nameInFirstLetterUpperCase1 ,font10); 
		
		 premisesValCell.addElement(custName);
		 premisesValCell.addElement(serAdrs); 
		 premisesValCell.setBorder(0);
		 
		 premisesDetails.addCell(blnkCell);
		 premisesDetails.addCell(PremiseslblCell);
		 premisesDetails.addCell(premisesValCell);
		 premisesDetails.addCell(blnkCell);
		 premisesDetails.addCell(blnkCell);
		 premisesDetails.addCell(blnkCell);
		 
          }
          else
          {
        	  ///////////////////////////////////////////////////////////

      		////////////////////// rohan suggested that take this address from customer ////////////////////// 
      	
//      		take service addrsss from customer or contract
      		if(cust.getSecondaryAdress().getAddrLine2() != null && !cust.getSecondaryAdress().getAddrLine2().equals(""))   
      		{
      			addressline=cust.getSecondaryAdress().getAddrLine1()+", "+cust.getSecondaryAdress().getAddrLine2()+", "+"\n";
      		}
      		else
      		{
      			addressline= cust.getSecondaryAdress().getAddrLine1()+", "+"\n";
      		}
      		
      		if((!cust.getSecondaryAdress().getLandmark().equals(""))&&(cust.getSecondaryAdress().getLocality().equals("")==false)){
      			System.out.println("inside both null condition1");
      			localityline= (cust.getSecondaryAdress().getLandmark()+", "+cust.getSecondaryAdress().getLocality()+", "+"\n"+cust.getSecondaryAdress().getCity()+" - "
      				          +cust.getSecondaryAdress().getPin()+", "+"\n" +cust.getSecondaryAdress().getState()+", "+cust.getSecondaryAdress().getCountry()+".");
      		}
      		else if((!cust.getSecondaryAdress().getLandmark().equals(""))&&(cust.getSecondaryAdress().getLocality().equals("")==true)){
      			System.out.println("inside both null condition 2");
      			localityline= (cust.getSecondaryAdress().getLandmark()+", "+"\n"+ cust.getSecondaryAdress().getCity()+" - "
      				      +cust.getSecondaryAdress().getPin()+", "+"\n"+cust.getSecondaryAdress().getState()+", "+cust.getSecondaryAdress().getCountry()+". ");
      		}
      		
      		else if((cust.getSecondaryAdress().getLandmark().equals(""))&&(cust.getSecondaryAdress().getLocality().equals("")==false)){
      			System.out.println("inside both null condition 3");
      			localityline= (cust.getSecondaryAdress().getLocality()+", "+"\n"+cust.getSecondaryAdress().getCity()+" - "
      				      +cust.getSecondaryAdress().getPin()+", "+"\n"+cust.getSecondaryAdress().getState()+", "+cust.getSecondaryAdress().getCountry()+". ");
      		}
      		else if((cust.getSecondaryAdress().getLandmark().equals(""))&&(cust.getSecondaryAdress().getLocality().equals("")==true)){
      			System.out.println("inside both null condition 4");
      			localityline=(cust.getSecondaryAdress().getCity()+", "+cust.getSecondaryAdress().getPin()+", "+"\n"+cust.getSecondaryAdress().getState()+", "+cust.getSecondaryAdress().getCountry()+". ");
      		}
      		
      		 serAdrs = new Phrase (addressline + localityline,font10);  
        	  ///////////////////////////////////////////////////////////
        	  
        	   String salutation1 = "" ;
        	   
       		if (cust.getSalutation()!= null && !cust.getSalutation().equals("")){
       			salutation1 = cust.getSalutation();
       		}   
       		else{  
       			 salutation1 = "" ;
       		}
       		
       	String  serCustName = "";	
       	if(!salutation.equals("")&& salutation!=null)	
       	{
       		
       		if(ser!=null)
       		{
       		  serCustName = salutation1 +" "+ ser.getCustomerName();
       		}
       		else 
       		{
       		  serCustName = salutation1 +" "+ con.getCustomerFullName();	
       		}
       	}
       	else{
       		if(ser!=null)
       		{
       		  serCustName =   ser.getCustomerName();
       		}
       		else 
       		{
       		  serCustName =   con.getCustomerFullName();	    
       		}
       	}
        		/*
      		 * nameInFirstLetterUpperCase defined by Ajinkya  suggested by Rahul V 
      		 * Used to take customer name from Uppercase to Only first letter Uppercase
      		 */
      		String nameInFirstLetterUpperCase1=getFirstLetterUpperCase(serCustName.trim()); // 1 
                                 ////////////////////// end here ///////////////// 
      		
      		Phrase custName = new Phrase ( nameInFirstLetterUpperCase1 ,font10); 
      		
      		 premisesValCell.addElement(custName);
      		 premisesValCell.addElement(serAdrs); 
      		 premisesValCell.setBorder(0);
      		 
      		 premisesDetails.addCell(blnkCell);
      		 premisesDetails.addCell(PremiseslblCell);
      		 premisesDetails.addCell(premisesValCell);
      		 premisesDetails.addCell(blnkCell);
      		 premisesDetails.addCell(blnkCell);
      		 premisesDetails.addCell(blnkCell);
      		 
          }
		 try {
			 premisesDetails.setWidths(new float[]{10,18,20,20,22,10});
				document.add(premisesDetails);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		
		 
		 PdfPTable serviceDetalsTbl = new PdfPTable(6);
		 serviceDetalsTbl.setWidthPercentage(100);
		 
		 try {
			 serviceDetalsTbl.setWidths(new float[]{10,18,20,20,22,10});
		} catch (DocumentException e) 
		 {
			e.printStackTrace();
		} 
		 
		 Phrase dateTime = new Phrase("Date & Time of Services : ",font10);
		 Phrase dateTimeVal = new Phrase(""+fmt1.format(ser.getServiceDate()) + "       "+ser.getServiceTime(),font10);
		 Phrase typeOfTreatment = new Phrase("Type Of Treatment : ",font10);
		 Phrase typeOfTreatmentVal = new Phrase(""+ser.getProductName(),font10);
		 Phrase clientTelNo =new Phrase ("Client Tel. No :",font10); 
		 Phrase clientTelNoVal =new Phrase (""+ser.getPersonInfo().getCellNumber(),font10);
		 Phrase remark = new Phrase("Remark :",font10);
		 
		 String remarkVal = "";
		 
		 for(int i=0;i<con.getItems().size();i++)
		{
			 if(con.getItems().get(i).getProductName()== productName){
			 remarkVal = con.getItems().get(i).getRemark();
			 }

		}
		 Phrase remarkValPhrase = new Phrase(remarkVal,font10);  
		 
		 PdfPCell dateTimeCell = new PdfPCell ();   
		 dateTimeCell.addElement(dateTime);  
		 dateTimeCell.setBorder(0);
		 
		 PdfPCell dateTimeValCell = new PdfPCell ();     
		 dateTimeValCell.addElement(dateTimeVal);
		 dateTimeValCell.setBorder(0);  
		 
		 PdfPCell typeOfTreatmentCell = new PdfPCell (); 
		 typeOfTreatmentCell.addElement(typeOfTreatment);
		 typeOfTreatmentCell.setBorder(0);
		 
		 PdfPCell typeOfTreatmentValCell = new PdfPCell (); 
		 typeOfTreatmentValCell.addElement(typeOfTreatmentVal);
		 typeOfTreatmentValCell.setBorder(0);
		 
		 PdfPCell clientTelNoCell = new PdfPCell (); 
		 clientTelNoCell.addElement(clientTelNo);
		 clientTelNoCell.setBorder(0);
		 
		 PdfPCell clientTelNoValCell = new PdfPCell (); 
		 clientTelNoValCell.addElement(clientTelNoVal);
		 clientTelNoValCell.setBorder(0);                
		 
		 PdfPCell remarkCell = new PdfPCell (); 
		 remarkCell.addElement(remark);
		 remarkCell.setBorder(0);
		 
		 PdfPCell remarkValCell = new PdfPCell (); 
		 remarkValCell.addElement(remarkValPhrase);
		 remarkValCell.setBorder(0);
		 
		 serviceDetalsTbl.addCell(blnkCell); 
		 serviceDetalsTbl.addCell(dateTimeCell);
		 serviceDetalsTbl.addCell(dateTimeValCell);
		 serviceDetalsTbl.addCell(blnkCell);  
		 serviceDetalsTbl.addCell(blnkCell);
		 serviceDetalsTbl.addCell(blnkCell);
		 
		 serviceDetalsTbl.addCell(blnkCell); 
		 serviceDetalsTbl.addCell(typeOfTreatmentCell);
		 serviceDetalsTbl.addCell(typeOfTreatmentValCell);  
		 serviceDetalsTbl.addCell(blnkCell);
		 serviceDetalsTbl.addCell(blnkCell);
		 serviceDetalsTbl.addCell(blnkCell);
		 
		 serviceDetalsTbl.addCell(blnkCell); 
		 serviceDetalsTbl.addCell(clientTelNoCell);
		 serviceDetalsTbl.addCell(clientTelNoValCell);
		 serviceDetalsTbl.addCell(blnkCell);
		 serviceDetalsTbl.addCell(blnkCell);
		 serviceDetalsTbl.addCell(blnkCell);
		 
		 serviceDetalsTbl.addCell(blnkCell); 
		 serviceDetalsTbl.addCell(remarkCell);
		 serviceDetalsTbl.addCell(remarkValCell);  
		 serviceDetalsTbl.addCell(blnkCell);
		 serviceDetalsTbl.addCell(blnkCell);
		 serviceDetalsTbl.addCell(blnkCell);  
		 
		 PdfPTable sigTbl = new PdfPTable(2);
		 sigTbl.setWidthPercentage(100);
		 
		 try {
			 sigTbl.setWidths(new float[]{80,20});  
		} catch (DocumentException e) 
		 {
			e.printStackTrace();
		} 
		                                                                                                                     
		 Phrase signature =new Phrase("                                                                                            Signature of the Occupant ",font9bold);
		
		 PdfPCell signatureCell = new PdfPCell(signature);
		 signatureCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		 signatureCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		 signatureCell.setBorder(0);
		 sigTbl.addCell(signatureCell);
		 sigTbl.addCell(blnkCell);
     
		 try {
			document.add(serviceDetalsTbl);
			document.add(sigTbl);
		} catch (DocumentException e) {
		
			e.printStackTrace();
		}
		
	   }

	      //Ajinkya added this code to convert customer name in CamelCase    
	  	//  Date : 12/4/2017

	private String getFirstLetterUpperCase(String customerFullName)
	{
	  	System.out.println("customerFullName"+customerFullName);
	  	String customerName="";
	  	String[] customerNameSpaceSpilt=customerFullName.split(" ");
	  	System.out.println("customerFullName size "+customerFullName.length());
	  	System.out.println("customerNameSpaceSpilt size "+ customerNameSpaceSpilt.length);
	  	logger.log(Level.SEVERE,"customerFullName size "+customerFullName.length());
	  	logger.log(Level.SEVERE,"customerNameSpaceSpilt size "+ customerNameSpaceSpilt.length);
	  	int count=0;
	  	for (String name : customerNameSpaceSpilt) {
	  		String nameLowerCase=name.toLowerCase();
	  		System.out.println("nameLowerCase"+nameLowerCase+"-"+count);
	  		logger.log(Level.SEVERE,"nameLowerCase"+nameLowerCase+"-"+count);
	  		if(count==0)
	  		{
	  			customerName=customerName+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);
	  			System.out.println("customerName"+customerName);
	  			
	  			logger.log(Level.SEVERE,"customerName"+customerName);
	  		}
	  		else
	  		{
	  			customerName=customerName+" "+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);
	  			System.out.println("customerName else"+customerName);
	  			
	  			logger.log(Level.SEVERE,"customerName"+customerName);
	  		}
	  		count=count+1;  
	  	}
	  	return customerName;    

	  	
	  }

}

