package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;
import java.awt.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;

public class CreateComplaintPdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1167319131782376125L;

	
	
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("application/pdf"); // Type of response , helps
													// browser to identify the
													// response type.

		try {
			String stringid = request.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);

			ServiceComplaintPdf complain = new ServiceComplaintPdf();
			complain.document = new Document();
			Document document = complain.document;
			PdfWriter writer = PdfWriter.getInstance(document,response.getOutputStream()); // write the pdf in response

			document.open();

			complain.setComplain(count);
			complain.createPdf();
			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

}
