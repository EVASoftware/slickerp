package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.layout.client.Layout.Alignment;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.device.CatchTraps;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

public class MultipleServicesRecordVersionOnePdf {
	
	Logger logger = Logger.getLogger("Name of logger");

	public Document document;
	Service service;
	Customer cust;
	Company comp;
	//Contract con;
	DecimalFormat df = new DecimalFormat("0.00");
	Branch branchDt = null;
	CompanyPayment companyPayment;
	//boolean checkBranchdata = false;
	
	/**
	 * Date : 29-05-2017 by ANIL
	 */
	Contract contract;
	
	/**
	 * End
	 */
	
	/*
	 * Date :21-01-2018
	 * @author Ashwini
	 */
	Boolean rowflag =false;
	Boolean upcflag = false;
	Boolean hidecolum = false;
	ProcessConfiguration processconfig;
	
	/*
	 * end by Ashwini
	 */
	/**Date 5-2-2020 by Amol **/
	boolean clusterNameflag=false;
	
	
	private Font font16boldul, font12bold, font8bold, font8, font9bold,
			font11bold, font12boldul, font12, font10bold, font10, font14bold,
			font9,font8underline;
	
	SimpleDateFormat fmt =new SimpleDateFormat("dd-MM-yyyy");
	PdfPTable a4Table = new PdfPTable(1);

	PdfPTable a5Table1 = new PdfPTable(1);
//	PdfPTable a5Table2 = new PdfPTable(1);
	ServiceProject serProject ;
	boolean fumigationFlag = false;
	List<Service> serviceList = new ArrayList<Service>();
	List<ServiceProject> serviceProjectList = new ArrayList<ServiceProject>();
	List<Integer> serIdList = new ArrayList<Integer>();
	HashMap<Integer , ServiceProject> projectMap = new HashMap<Integer , ServiceProject>();
	
	/**
	 * @author Anil , Date : 24-02-2020
	 * If scoketTimeOut occurs then system should try atleast twice to reload header and footer
	 */
	int headerCounter=0;
	int footerCounter=0;
	
	/**
	 * @author Ashwini Patil
	 * @since 17-05-2022 
	 * When completed services are in large amount size of pdf document becomes more than 25MB and SR email is not getting send.
	 * Header and footer imagaes are right now getting inserted on every page which is creating size issue.
	 * As per Nitin sir's instruction, adding header in the first copy and footer in the last copy.
	 */
	boolean printHeaderFlag=true;
	boolean printFooterFlag=false;
	
	public MultipleServicesRecordVersionOnePdf() {
      
		/**30-10-2017 sagar sore [ partitioning page vertically in middle and displaying same data on left and right side of page ]**/
		a5Table1.setSpacingAfter(0);
		a5Table1.setSpacingBefore(0);
//		a5Table2.setSpacingAfter(0);
//		a5Table2.setSpacingBefore(0);
		a5Table1.setWidthPercentage(100);
//		a5Table2.setWidthPercentage(100);
		a4Table.setWidthPercentage(100);
		/** 30-10-2017 sagar sore []**/
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font11bold = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		
		
		
		
//		new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD);
//		font16boldul = new Font(Font.FontFamily.HELVETICA, 17, Font.BOLD);
//		font12bold = new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD);
//		font8bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);//checking font 6 by Ajinkya
//		font8underline = new Font(Font.FontFamily.HELVETICA, 6, Font.UNDEFINED );
//		font8 = new Font(Font.FontFamily.HELVETICA, 6);//checking font 6 by Ajinkya
//		font9 = new Font(Font.FontFamily.HELVETICA, 10);
//		font12boldul = new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD);
//		font12 = new Font(Font.FontFamily.HELVETICA, 13);
//		font11bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
//		font10bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);//checking font 8 by Ajinkya
//		font10 = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL);   //checking font 8 by Ajinkya
//		font14bold = new Font(Font.FontFamily.HELVETICA, 15, Font.BOLD);
//		font9bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		
		//End By Jayshree
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}
	
	
	public void loadAll(Long count) {
		
		a4Table = new PdfPTable(1);
		a5Table1 = new PdfPTable(1);

		service=ofy().load().type(Service.class).id(count).now();
		
		if(service.getCompanyId()==null)
			cust=ofy().load().type(Customer.class).filter("count",service.getPersonInfo().getCount()).first().now();
		else
			cust=ofy().load().type(Customer.class).filter("count",service.getPersonInfo().getCount()).filter("companyId", service.getCompanyId()).first().now();

		
		
		if (service.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class).filter("companyId", service.getCompanyId()).first().now();
		
		/**
		 * Date : 29-05-2017 By ANIL
		 * Loading Contract
		 */
		if(service.getCompanyId()!= null){
			contract=ofy().load().type(Contract.class).filter("companyId", service.getCompanyId()).filter("count", service.getContractCount()).first().now();
		}
		/**
		 * End
		 */
			
		
//		compPayment = ofy().load().type(CompanyPayment.class)
//				.filter("companyId", service.getCompanyId()).filter(" paymentStatus", true).filter("paymentDefault", true).first().now();
		
//		if (service.getCompanyId() == null)
//			branchDt = ofy().load().type(Branch.class)
//					.filter("companyId", service.getCompanyId()).first().now();
//		else
//			branchDt = ofy().load().type(Branch.class)
//					.filter("companyId", service.getCompanyId())
//					.filter("buisnessUnitName", service.getBranch().trim()).first()
//					.now();
		
		if(service.getCompanyId()!=null){
			processconfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", service.getCompanyId())
					.filter("processName", "Service")
					.filter("configStatus", true).first().now();
			if(processconfig!=null){
				for(int k= 0 ;k<processconfig.getProcessList().size() ; k++){
					
					if (processconfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("DecreaseTableRow")
							&& processconfig.getProcessList().get(k).isStatus() == true) {
						rowflag = true;
						System.out.println("rowflag:::::");
					}
					
					//hidecolum
					if (processconfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("CompanyLetterHead")
							&& processconfig.getProcessList().get(k).isStatus() == true) {
						upcflag = true;
						System.out.println("upcflag:::::");
						
					}
					
					if (processconfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("HideMaterialandUOM")
							&& processconfig.getProcessList().get(k).isStatus() == true) {
						hidecolum = true;
						System.out.println("hidecolum:::::");//checkBranchdata
						
					}
					
					
				}
			}
			
			
		}
		
		/*
		 * end by Ashwini
		 */
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		if (service.getCompanyId() != null){
			serProject = ofy().load().type(ServiceProject.class).filter("companyId", service.getCompanyId()).filter("serviceId", this.service.getCount()).filter("count", this.service.getProjectId()).first().now();
		}else{
			serProject = ofy().load().type(ServiceProject.class).filter("serviceId", this.service.getCount()).filter("count", this.service.getProjectId()).first().now();
		}
		if(this.service.getProduct() != null && this.service.getProduct().getProductCode() != null){
			if(Service.getFumigationProductCodes().contains(this.service.getProduct().getProductCode())){
				fumigationFlag = true;
			}
			
		}
		
		
		
//		if (service.getCompanyId() != null){
//			serProject = ofy().load().type(ServiceProject.class).filter("companyId", service.getCompanyId()).filter("serviceId", this.service.getCount()).filter("count", this.service.getProjectId()).first().now();
//		}else{
//			serProject = ofy().load().type(ServiceProject.class).filter("serviceId", this.service.getCount()).filter("count", this.service.getProjectId()).first().now();
//		}
		if(serIdList != null && serIdList.size() > 0){
			serviceProjectList = ofy().load().type(ServiceProject.class).filter("companyId", service.getCompanyId()).filter("serviceId IN", serIdList).list();
		}
		if(serviceProjectList != null){
			for(ServiceProject proj : serviceProjectList){
				projectMap.put(proj.getserviceId(), proj);
			}
		}
		if(serProject != null){
			projectMap.put(serProject.getserviceId(), serProject);
		}

		
		
		
		
	//	branchDt = ofy().load().type(Branch.class).filter("companyId", contract.getCompanyId()).filter("buisnessUnitName", service.getBranch()).first().now();
		
//		//checkBranchdata
//		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
//			checkBranchdata=true;
//		}
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			System.out.println("Inside branch As company");
			logger.log(Level.SEVERE,"Process active --");
			if(contract !=null && contract.getBranch() != null && contract.getBranch().trim().length() >0){
				
				branchDt = ofy().load().type(Branch.class).filter("companyId",contract.getCompanyId()).filter("buisnessUnitName", contract.getBranch()).first().now();
				
			
				if(branchDt!=null){
					logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());		
					
					if(branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
						
					
					List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
					
					if(paymentDt.get(0).trim().matches("[0-9]+")){
						
						
						
						int payId = Integer.parseInt(paymentDt.get(0).trim());
						
						companyPayment = ofy().load().type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", contract.getCompanyId()).first()
								.now();
						
						
						if(companyPayment != null){
							
							}
						
						}
					}
					
					comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
				}
			}
		}
	}


	public void createPdf(String preprintStatus) {
	if (upcflag == false && preprintStatus.equals("plane")) {
			//createBlankHeading();
			Createblank();
			createHeader();
			//Createtitle();
		}
//	else{

			if (preprintStatus.equals("yes")){

				createBlankforUPC();
			}

			if(upcflag){
			if (preprintStatus.equals("no")){
				if(comp.getUploadHeader()!=null&&printHeaderFlag){
					createCompanyNameAsHeader(document,comp);
					printHeaderFlag=false;
					}
					
//					if(comp.getUploadFooter()!=null){
//					createCompanyNameAsFooter(document,comp);
//					}
				createBlankforUPCOne();
 				    
			}
	}

//		}
//		if(headerFooter){
//			
//			if(preprintStatus.equals("no")){
//				if(comp.getUploadHeader()!=null){
//					createCompanyNameAsHeader(document , comp);
//					}
//			}
//		}
		
		
						
//		createBlankHeading();
//        createBlankForLetterHead();By jayshree comment method
		//End By Jayshree
		
		/**
		 * Date 15-09-2018 By Vijay
		 * Des :- added header and footer They will print on preeprint stationary so if preprint is no them it will print as old pdf settingd
		 * if yes then it will print on proper with preeprint stationary
		 */
		
//		if(preprintStatus.equals("yes")){
//			createBlankforUPC();
//		}
		/**
		 * ends here
		 */
	     
		//createCompanyDetails();
		createHeading();
		serviceinformation();
		
		servicesCarriedOut();
		
		trapCatchesList();
		
		
		feedbackFromCustomer();
		sinatureofCustAndNBHCRepresentative();
	
		createChecklistDetails();
		
//		/**
//		 * Date 28-01-2018
//		 * By Ashwini
//		 * To add footer in pdf 
//		 */
//		if(headerFooter){
//			
//			
//			if(preprintStatus.equals("no")){
//				if(comp.getUploadFooter()!=null){
//					createCompanyNameAsFooter(document,comp);//By jayshree comment method
//				}
//			}
//		}
		
		
		if(upcflag){
			if (preprintStatus.equals("no")){
				
					createBlankforUPC();
					if(comp.getUploadFooter()!=null&&printFooterFlag){
						
						createCompanyNameAsFooter(document,comp);
					}
			}
	}
		
		
		
		
		/**28-10-2017 sagar sore [to make copy of pdf on half side of page]**/
		addTablesToParentTable();
		/**31-10-2017 sagar sore [to make copy of pdf on half side of page]**/
		
//		createCompanyDetails();
	}
//End By Jayshree

	private void createChecklistDetails() {

		System.out.println("inside checklist");
		
		if(service.getCheckList().size()>0){
			Paragraph checklistpara = new Paragraph("Checklist :",font8bold);
			checklistpara.setAlignment(Element.ALIGN_LEFT);
			

		    PdfPCell checklistParaCell = new PdfPCell();
		    checklistParaCell.setBorder(0);
		    checklistParaCell.addElement(checklistpara);
		    a5Table1.addCell(checklistParaCell);
		}
		
	    
		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);
		
//        table.setSpacingBefore(5f);
//		table.setSpacingAfter(5f);
		
		float checklisttablewidth[] = {1f,5f,1f,5f};
		PdfPTable checklisttable = new PdfPTable(4);
		checklisttable.setWidthPercentage(100f);
		try {
			checklisttable.setWidths(checklisttablewidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        checklisttable.setSpacingBefore(5f);
		checklisttable.setSpacingAfter(5f);
		
		Image blankImg = null;
		try {
			blankImg = Image.getInstance("images/unchecked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		blankImg.scalePercent(10);
		blankImg.scaleAbsoluteHeight(10);
		blankImg.scaleAbsoluteWidth(10);
		
		Image checkedImg = null;
		try {
			checkedImg = Image.getInstance("images/checked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		checkedImg.scalePercent(10);
		checkedImg.scaleAbsoluteHeight(10);
		checkedImg.scaleAbsoluteWidth(10);
		
		PdfPCell uncheckCell=new PdfPCell(blankImg);
		uncheckCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		uncheckCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		uncheckCell.setBorder(1);
		uncheckCell.setBorderWidthRight(0);

		
		PdfPCell checkCell=new PdfPCell(checkedImg);
		checkCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		checkCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		checkCell.setBorder(1);
		checkCell.setBorderWidthRight(0);

		
		Phrase blank = new Phrase(" ",font8);
		PdfPCell blankCell = new PdfPCell(blank);
//		blankCell.setBorder(1);
		blankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		blankCell.setBorderWidthRight(0);
		
		
		Phrase blank2 = new Phrase(" ",font8);
		PdfPCell blankCell2 = new PdfPCell(blank2);
//		blankCell.setBorder(1);
		blankCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		blankCell2.setBorderWidthLeft(0);
		
		for(int i=0;i<service.getCheckList().size();i+=2){
			
				
				if(service.getCheckList().get(i).getStatus()==null){
					checklisttable.addCell(blankCell);
				}
				else if(service.getCheckList().get(i).getStatus()){
					checklisttable.addCell(checkCell);
				}
				else{
					checklisttable.addCell(uncheckCell);
				}
				
				Phrase name = new Phrase(service.getCheckList().get(i).getPestName(),font8);
				PdfPCell nameCell = new PdfPCell(name);
				nameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				nameCell.setBorderWidthLeft(0);
				checklisttable.addCell(nameCell);
				
				
				try {
					
				
				if(service.getCheckList().get(i+1).getStatus()==null){
					checklisttable.addCell(blankCell);
				}
				else if(service.getCheckList().get(i+1).getStatus()){
					checklisttable.addCell(checkCell);
				}
				else{
					checklisttable.addCell(uncheckCell);
				}
				
				Phrase name1 = new Phrase(service.getCheckList().get(i+1).getPestName(),font8);
				PdfPCell nameCell2 = new PdfPCell(name1);
				nameCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
				nameCell2.setBorderWidthLeft(0);
				checklisttable.addCell(nameCell2);
				
				} catch (Exception e) {
					checklisttable.addCell(blankCell);
					checklisttable.addCell(blankCell2);
				}
				
			
			
		}
		
		

//		for(CatchTraps catchtraps : service.getCheckList()){
//			System.out.println("inside checklist table");
//			if(catchtraps.getStatus()==null){
//				checklisttable.addCell(blankCell);
//			}
//			else if(catchtraps.getStatus()){
//				checklisttable.addCell(checkCell);
//			}
//			else{
//				checklisttable.addCell(uncheckCell);
//			}
//			
//			Phrase name = new Phrase(catchtraps.getPestName(),font8);
//			PdfPCell nameCell = new PdfPCell(name);
//			nameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			checklisttable.addCell(nameCell);
//			
//			PdfPCell checklistcell = new PdfPCell(checklisttable);
//			checklistcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			table.addCell(checklistcell);
//			
//			
////			PdfPCell checklistcell2 = new PdfPCell(table);
////			checklistcell2.setHorizontalAlignment(Element.ALIGN_LEFT);
////			table2.addCell(checklistcell2);
//			
//		}
//		
//		
//		 	PdfPCell tableCell1 = new PdfPCell();
//		 	tableCell1.setBorder(0);
//		 	tableCell1.addElement(table);	   
//			a5Table1.addCell(tableCell1);
			
			
		    PdfPCell tableCell = new PdfPCell();
		    tableCell.setBorder(0);
		    tableCell.addElement(checklisttable);
			a5Table1.addCell(tableCell);
			
			
//		PdfPCell checklistcel = new PdfPCell(table);
//
//		a5Table1.addCell(tableCell);
//		a5Table1.addCell(checklistcel);
//
//		
//		try {
//			document.add(table);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
		
		
	}


	private void createBlankforUPC() {

		Phrase spacing = new Phrase(Chunk.NEWLINE);
		Paragraph spacingPara = new Paragraph(spacing);
		
		try {
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		} catch (DocumentException e) {
		e.printStackTrace();
		}
	}
	
	private void createBlankforUPCOne() {

		Phrase spacing = new Phrase(Chunk.NEWLINE);
		Paragraph spacingPara = new Paragraph(spacing);
		
		try {
		document.add(spacingPara);
		document.add(spacingPara);
		//document.add(spacingPara);
		//document.add(spacingPara);
		//document.add(spacingPara);
		//document.add(spacingPara);
		} catch (DocumentException e) {
		e.printStackTrace();
		}
	}

	/**
	 * Updated by: Viraj
	 * Date: 21-02-2019
	 * Description: To show header and footer dynamically
	 * @param doc
	 * @param comp
	 */
	
	private void Createblank() {
		
	    Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
	    try {
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
			
	}
	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			image2.setSpacingBefore(5f);
			doc.add(image2);
		//	a5Table1.addCell(image2);
		//	a5Table2.addCell(image2);
			
		}
		catch (Exception e) {
			/**
			 * @author Anil , Date : 24-02-2020
			 * System throws this exception if call this method in loop
			 */
			if(e instanceof SocketTimeoutException){
				headerCounter++;
				if(headerCounter<=2 && e instanceof SocketTimeoutException ){
					createCompanyNameAsHeader(doc, comp);
				}else{
					e.printStackTrace();
				}
			}else{
				e.printStackTrace();
			}
		}

	}

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
			image2.setBorder(0);
			doc.add(image2);
			
			
	//		a5Table1.addCell(image2);
			
//			a5Table2.addCell(image2);
		}
		catch (Exception e) {
			/**
			 * @author Anil , Date : 24-02-2020
			 * System throws this exception if call this method in loop
			 */
			if(e instanceof SocketTimeoutException){
				footerCounter++;
				if(footerCounter<=2){
					createCompanyNameAsFooter(doc, comp);
				}else{
					e.printStackTrace();
				}
			}else{
				e.printStackTrace();
			}
		}

	}

private void createBlankForLetterHead(){
		
		a5Table1.setWidthPercentage(100);
//		a5Table2.setWidthPercentage(100);
		
		System.out.println("Inside create blank heading");
		Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
	  
		PdfPCell paraCell = new PdfPCell(blank);
		paraCell.setBorder(0);
		
		a5Table1.addCell(paraCell);
//		a5Table2.addCell(paraCell); 

	    PdfPCell blcell = new PdfPCell(blank);
	    blcell.setBorder(0);
	    blcell.setBorder(0);

		PdfPTable bltable = new PdfPTable(1);
		bltable.addCell(blcell);
		bltable.addCell(blcell);
		bltable.setWidthPercentage(100f);
		
		PdfPCell blTableCell = new PdfPCell();
		blTableCell.setBorder(0);
		blTableCell.addElement(bltable);

		System.out.println("BLANK CELL ADDED IN DOCUMENT...");

	}
	private void createBlankHeading() {
		
		System.out.println("Inside create blank heading");
		Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
	
	    
		
	    /** 30-10-2017 sagar sore [commented to show same content on left and right side after vertically partitioned ]**/
//	    try {
//			document.add(blank);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//       
	    /**28-10-2017 sagar sore [to make copy of pdf on half side of page]**/
	    PdfPCell blankCell = new PdfPCell();
	    blankCell.setBorder(0);
	    blankCell.addElement(blank);		
//		a5Table1.addCell(blankCell);
//		a5Table2.addCell(blankCell);
		
	    PdfPCell blcell = new PdfPCell(blank);
	    blcell.setBorder(0);
	    blcell.setBorder(0);

		PdfPTable bltable = new PdfPTable(1);
		bltable.addCell(blcell);
//		bltable.addCell(blcell);
		bltable.setWidthPercentage(100f);

	    /** 30-10-2017 sagar sore [commented to show same content on left and right side after vertically partitioned ]**/	    
//		try {
//			document.add(bltable);
//			
//			
//			System.out.println("BLANK CELL ADDED IN DOCUMENT...");
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
	
		/**28-10-2017 sagar sore [to make copy of pdf on half side of page]**/

		a5Table1.addCell(blankCell);
//		a5Table2.addCell(blankCell);
	}
	
//private void createCompanyNameAsHeader(Document doc, Company comp) {
//		
//		DocumentUpload document =comp.getUploadHeader();
//
//		//patch
//		String hostUrl;
//		String environment = System.getProperty("com.google.appengine.runtime.environment");
//		if (environment.equals("Production")) {
//		    String applicationId = System.getProperty("com.google.appengine.application.id");
//		    String version = System.getProperty("com.google.appengine.application.version");
//		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
//		} else {
//		    hostUrl = "http://localhost:8888";
//		}
//		
//		try {
//			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
//			image2.scalePercent(15f);
//			image2.scaleAbsoluteWidth(520f);
//			image2.setAbsolutePosition(40f,725f);	
//			doc.add(image2);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		
//		
////		try
////		{
////			Image image1=Image.getInstance("images/OM Pest Letter Head.jpg");
////		image1.scalePercent(15f);
////		image1.scaleAbsoluteWidth(520f);
////		image1.setAbsolutePosition(40f,725f);	
////		doc.add(image1);
////		}
////		catch(Exception e)
////		{
////			e.printStackTrace();
////		}
//		}

	

	
	private void createHeading() {
		a5Table1.setWidthPercentage(100);
	//	a5Table2.setWidthPercentage(100);
		Paragraph para = null;
		
			para = new Paragraph("Pest Management Service Record",font10bold);
		
		 
		para.setAlignment(Element.ALIGN_CENTER);

	    /** 30-10-2017 sagar sore [commented to show same content on left and right side after vertically partitioned ]**/
//		try {
//			document.add(para);
//			
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		
		/**28-10-2017 sagar sore [to make copy of pdf on half side of page]**/
        PdfPCell paraCell = new PdfPCell();
        paraCell.setBorder(0);
        paraCell.addElement(para);
        
		a5Table1.addCell(paraCell);
//		a5Table2.addCell(paraCell);
	}
	
	

	private void serviceinformation() {
        /** 30-10-2017 sagar sore [widths updated]**/
//		
		PdfPTable table=null;
		logger.log(Level.SEVERE,"CLUSTERNAME FLAG "+clusterNameflag);
		if(clusterNameflag){
			
			logger.log(Level.SEVERE,"CLUSTERNAME FLAG Table Creation"+clusterNameflag);
			table=new PdfPTable(12);
			table.setWidthPercentage(100);
			
			
			
			try {
				table.setWidths(new float[]{11,1,10,8,1,17,8,1,11,8,1,20});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
		
		float[] columnWidths = {1.5f, 0.1f, 2.0f,1.0f, 0.1f, 2.1f,0.8f, 0.1f, 2.3f};
        table = new PdfPTable(9);
		
		table.setSpacingBefore(0f);
		table.setWidthPercentage(100f);

		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		}
		
		/****Date 5-2-2020 by Amol Added a Cluster Name Column****/

		Phrase clusterName =  new Phrase("Cluster",font8bold);
		PdfPCell clusterNameCell = new PdfPCell(clusterName);
		clusterNameCell.setBorder(0);
		clusterNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		
		String serviceCustomerName="";
		if(service.getCustomerName()!=null&&service.getCustomerName().equalsIgnoreCase("NBHC Internal")){
			serviceCustomerName=service.getClusterName();
			logger.log(Level.SEVERE,"customer Name "+serviceCustomerName);
		}else{
			serviceCustomerName="";
		}
		logger.log(Level.SEVERE,"CUSTOMER NAME"+serviceCustomerName);
		
		Phrase clusterNameValue =  new Phrase(serviceCustomerName,font8);
		PdfPCell clusterNameValueCell = new PdfPCell(clusterNameValue);
		clusterNameValueCell.setBorder(0);
		clusterNameValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase serialNo =  new Phrase("Contract ID",font8bold);
		PdfPCell serialNoCell = new PdfPCell(serialNo);
		serialNoCell.setBorder(0);
		serialNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase branch =new Phrase("Branch",font8bold);
		PdfPCell branchCell = new PdfPCell(branch);
		branchCell.setBorder(0);
		branchCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase serdate =new Phrase("Date",font8bold);
		PdfPCell serdateCell = new PdfPCell(serdate);
		serdateCell.setBorder(0);
		serdateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase dotted =new Phrase(":",font8);
		PdfPCell dottedCell = new PdfPCell(dotted);
		dottedCell.setBorder(0);
		dottedCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase serialNoValue =new Phrase(service.getContractCount()+"",font8);
		PdfPCell serdateValueCell = new PdfPCell(serialNoValue);
		serdateValueCell.setBorder(0);
		serdateValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase branchValue =new Phrase(service.getBranch(),font8);
		PdfPCell branchValueCell = new PdfPCell(branchValue);
		branchValueCell.setBorder(0);
		branchValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase servicedateValue =new Phrase(fmt.format(service.getServiceDate()),font8);
		PdfPCell servicedateValueCell = new PdfPCell(servicedateValue);
		servicedateValueCell.setBorder(0);
		servicedateValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		/**
		 * Date : 29-05-2017 By ANIL
		 * Adding Blank Cell
		 */
		
		Phrase blank =new Phrase("",font8);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setBorder(0);
		blankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		/**
		 * End
		 */
		
		table.addCell(serialNoCell);
		table.addCell(dottedCell);
		/**
		 *  Date : 29-05-2017 By ANIL
		 */
//		table.addCell(serdateVaslueCell);
		table.addCell(serdateValueCell);
		/**
		 * End
		 */
		
		table.addCell(branchCell);
		table.addCell(dottedCell);
		table.addCell(branchValueCell);
		
		table.addCell(serdateCell);
		table.addCell(dottedCell);
		/**
		 *  Date : 29-05-2017 By ANIL
		 */
//		table.addCell(servicedateValueCell);
		table.addCell(servicedateValueCell);
		
		logger.log(Level.SEVERE,"customer Name flag "+clusterNameflag);
		if(clusterNameflag){
			
			logger.log(Level.SEVERE,"addcluster cell to table"+clusterNameflag);
			table.addCell(clusterNameCell);
			table.addCell(dottedCell);
			table.addCell(clusterNameValueCell);
		}
		
		
		
		/**
		 * End
		 */
		
		
		
		PdfPTable parenttable = new PdfPTable(1);
		parenttable.setWidthPercentage(100);
		
		parenttable.setSpacingBefore(10f);//By Jayshree Add the table spacing
//		parenttable.setSpacingAfter(20f);
		
		PdfPCell tableCell = new PdfPCell(table);
		tableCell.setBorder(0);
		parenttable.addCell(tableCell);
		Phrase customerName = new Phrase("Customer : "+cust.getCustomerName(),font8);
		PdfPCell customerNameCell = new PdfPCell(customerName);
		customerNameCell.addElement(customerName);
		customerNameCell.setBorder(0);
		parenttable.addCell(customerNameCell);
		
		Phrase custBranchName = new Phrase("Customer Branch : "+service.getServiceBranch(),font8);
		PdfPCell custBranchCell = new PdfPCell(custBranchName);
//		custBranchCell.addElement(custBranchName);
		custBranchCell.setBorder(0);
		parenttable.addCell(custBranchCell);
				
		String custAdd1="";
		String custFullAdd1="";
		
	if(service.getAddress()!=null){
			
			if(!service.getAddress().getAddrLine2().equals("")){
				if(!service.getAddress().getLandmark().equals("")){
					custAdd1=service.getAddress().getAddrLine1()+","+service.getAddress().getAddrLine2()+","+service.getAddress().getLandmark();
				}else{
					custAdd1=service.getAddress().getAddrLine1()+","+service.getAddress().getAddrLine2();
				}
			}else{
				if(!cust.getAdress().getLandmark().equals("")){
					custAdd1=service.getAddress().getAddrLine1()+","+service.getAddress().getLandmark();
				}else{
					custAdd1=service.getAddress().getAddrLine1();
				}
			}
			
			if(!service.getAddress().getLocality().equals("")){
				custFullAdd1=custAdd1+","+service.getAddress().getLocality()+","+service.getAddress().getCity()+"-"+service.getAddress().getPin()+","+service.getAddress().getState()+","+service.getAddress().getCountry();
						
			}else{
				custFullAdd1=custAdd1+","+service.getAddress().getCity()+"-"+service.getAddress().getPin()+","+service.getAddress().getState()+","+service.getAddress().getCountry();
			}
		}
		
	
	Phrase custFullAdd1p = new Phrase("Address : "+custFullAdd1,font8);
	PdfPCell custFullAdd1pCell = new PdfPCell(custFullAdd1p);
	custFullAdd1pCell.setBorder(0);
	parenttable.addCell(custFullAdd1pCell);
		
	
	
	
	//   this code is for service timing   
	Phrase serviceTime = null;
	if(!service.getFromTime().equals("") && !service.getToTime().equals("")){
		 serviceTime = new Phrase("Time Of Service : From "+service.getFromTime()+" To "+service.getToTime(),font8);
	}
	else
	{
		// serviceTime = new Phrase("Time Of Service : From "+"_______"+" To "+"_______",font8);
		serviceTime = new Phrase(" ",font8);
	}
	PdfPCell serviceTimeCell = new PdfPCell(serviceTime);
	serviceTimeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	serviceTimeCell.setBorder(0);
	
	parenttable.addCell(serviceTimeCell);

    /** 30-10-2017 sagar sore [commented to show same content on left and right side after vertically partitioned ]**/
//		try {
//			document.add(parenttable);
//			
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		
		/**28-10-2017 sagar sore [to make copy of pdf on half side of page]**/
		PdfPCell parentTableCell = new PdfPCell();
		parentTableCell.setBorder(0);
		parentTableCell.addElement(parenttable);
		a5Table1.addCell(parentTableCell);
	//	a5Table2.addCell(parentTableCell);
		
	}

	
	private void servicesCarriedOut(){
		
		Paragraph servicepara = new Paragraph("Summary of Pest Treatments :",font8bold);
		servicepara.setAlignment(Element.ALIGN_LEFT);
		
		Paragraph product = new Paragraph(service.getProductName(),font8);
		product.setAlignment(Element.ALIGN_LEFT);
		float[] columnWidths = {1.5f, 3f, 2f , 2f ,1.5f};
		float[] columnWidths1 = {2f, 5f, 3f };
		
		
	
		
		PdfPTable table;
		if(!hidecolum) {
			table = new PdfPTable(5);
			table.setWidthPercentage(100f);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	//table.setSpacingBefore(5f);
		}else{
			table = new PdfPTable(3);
			 table.setWidthPercentage(100f);
			try {
				table.setWidths(columnWidths1);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		
		table.setSpacingBefore(5f);
		
		Phrase serviceId = new Phrase("Service Id",font8bold);
		Phrase service = new Phrase("Service",font8bold);
		Phrase treatments = new Phrase("Treatments",font8bold);
		Phrase materials = new Phrase("Material Name",font8bold);//  //Materials
		Phrase targetPest = new Phrase("Quantity",font8bold);//Quantity & UOM//Target Pest
		
		
		
		Phrase blanks = new Phrase(" ",font8);
		
		PdfPCell serviceIDCell = new PdfPCell(serviceId);
		serviceIDCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		serviceIDCell.setPadding(4f);
		
		PdfPCell serviceCell = new PdfPCell(service);
		serviceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		serviceCell.setPadding(4f);
		
		PdfPCell treatmentsCell = new PdfPCell(treatments);
		treatmentsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		treatmentsCell.setPadding(4f);
		
		PdfPCell targetPestCell = new PdfPCell(targetPest);
		targetPestCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		targetPestCell.setPadding(4f);
		
		PdfPCell materialsCell = new PdfPCell(materials);
		materialsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		materialsCell.setPadding(4f);
		
		PdfPCell blanksCell = new PdfPCell(blanks);
		
		
		
		if(!hidecolum){
			table.addCell(serviceIDCell);
			table.addCell(serviceCell);
			table.addCell(treatmentsCell);
			table.addCell(materialsCell);
			table.addCell(targetPestCell);
			
		
		}else{
		table.addCell(serviceIDCell);
		table.addCell(serviceCell);
		table.addCell(treatmentsCell);
		}
		
	for(Service srObject : serviceList){
		int  size = 0;
		//	int i = serviceList.size() ; i < 4; i++
		//for (int k =serviceList.size() ; k <serviceList.size() ;  k++) {
		//int j = 0;
		if(srObject.getProductName() != null){
			Phrase serIdPh=new Phrase(srObject.getCount()+"",font8);
			PdfPCell serIdCell = new PdfPCell(serIdPh);
			serIdCell.setPadding(3f);
			serIdCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			Phrase itemPh=new Phrase(srObject.getProductName(),font8);
			PdfPCell itemCell = new PdfPCell(itemPh);
			itemCell.setPadding(3f);
			itemCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			/**
			 *  Date :03/06/2021  Added By Priyanka.
			 *  Des : Need to map value from app in treatment and target table 
			 */
			
//			
//			Phrase targetPestph=new Phrase(srObject.getPestTreatmentList().get(j).getPestTarget()+"",font8);
//			PdfPCell targetPestcell = new PdfPCell(targetPestph);
//			serIdCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			String Treatment =" ";
			Phrase targetPestph1=null;
			if(srObject.getPestTreatmentList() != null && srObject.getPestTreatmentList().size() >0){
			size=srObject.getPestTreatmentList().size();
			for(int j =0 ; j < size ; j++){
				if(srObject.getPestTreatmentList().get(j).getMethodOfApplication()!=null){
				Treatment += srObject.getPestTreatmentList().get(j).getMethodOfApplication()+",";
			  }else{
				  Treatment = " ";
			  }
			}
			if(Treatment.endsWith(",")){
				targetPestph1=new Phrase(Treatment.substring(0,Treatment.length()-1),font8);
			}else{
				targetPestph1=new Phrase(Treatment,font8);
			}
			
			}else
			{
				targetPestph1=new Phrase(" ",font8);
			}
			
			
			
//
//			if(srObject.getPestTreatmentList().get(j).getMethodOfApplication()!=null)
//			{
//				targetPestph1=new Phrase(Treatment,font8);
//			}else{
//				targetPestph1=new Phrase(" ",font8);
//			}
			
		//	Phrase targetPestph1=new Phrase(Treatment,font8);
			PdfPCell targetPestcell1 = new PdfPCell(targetPestph1);
			targetPestcell1.setPadding(3f);
			targetPestcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			table.addCell(serIdCell);
			table.addCell(itemCell);
			table.addCell(targetPestcell1);
			//table.addCell(targetPestcell);
		
			/** end **/
			
			serProject = projectMap.get(srObject.getCount());
			if(serProject != null && serProject.getProdDetailsList().size() >0){
				size =  serProject.getProdDetailsList().size();
				String uom="";
				Phrase UomPhrase = null;
				String material = "";
				Phrase materialPh = null;
				for(int i =0 ; i < size ; i++){
				
				material += serProject.getProdDetailsList().get(i).getName()+",";
				
				}
				if(material.endsWith(",")){
					materialPh=new Phrase(material.substring(0,material.length()-1),font8);
				}else{
					materialPh=new Phrase(material,font8);
				}
				PdfPCell materialCell = new PdfPCell(materialPh);
				materialCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				materialCell.setPadding(3f);
				if(!hidecolum) {
				table.addCell(materialCell);
			}
			}
			
			
			
			if(serProject != null && serProject.getProdDetailsList().size() >0){
				size =  serProject.getProdDetailsList().size();
				String uom="";
				Phrase UomPhrase = null;
				//String material = "";
				//Phrase materialPh = null;
				for(int i =0 ; i < size ; i++){
					String unit = "";
					if(serProject.getProdDetailsList().get(i).getUnit() != null){
						unit = serProject.getProdDetailsList().get(i).getUnit();
					}
					String quantity=df.format(serProject.getProdDetailsList().get(i).getReturnQuantity());
					if(serProject.getProdDetailsList().get(i).getReturnQuantity()!=0){
						
					uom += quantity+" "+unit+",";
					}
				}
				System.out.println("uom---"+uom);
				//UomPhrase=new Phrase(uom,font8);
				if(uom.endsWith(",")){
					UomPhrase=new Phrase(uom.substring(0,uom.length()-1),font8);
				}else{
					UomPhrase=new Phrase(uom,font8);
				}
				
				PdfPCell materialCell1 = new PdfPCell(UomPhrase);
				materialCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
				materialCell1.setPadding(3f);
				if(!hidecolum) {
				table.addCell(materialCell1);
				}
			}
			
			
			
			
			
		}
	//}
	}
		for (int i = serviceList.size() ; i < 2; i++) {//8
			table.addCell(blanksCell);
			table.addCell(blanksCell);
			table.addCell(blanksCell);
			table.addCell(blanksCell);
			table.addCell(blanksCell);
		}	
//		int blankRows ;
//		if(rowflag){
//			 blankRows=4-size;
//		}else{
//			 blankRows=9-size; 
//		}
//		
//		if(blankRows > 0){
//			
//			for(int i =1 ; i < size ; i++){
//				table.addCell(blanksCell);
//				table.addCell(blanksCell);
//				table.addCell(blanksCell);
//				if(serProject != null && serProject.getProdDetailsList().size() >0){
//					Phrase materialPh = null;
//					if(fumigationFlag){
//						String unit = "";
//						if(serProject.getProdDetailsList().get(i).getUnit() != null){
//							unit = serProject.getProdDetailsList().get(i).getUnit();
//						}
//						materialPh=new Phrase(serProject.getProdDetailsList().get(i).getName() + " - " +
//								serProject.getProdDetailsList().get(i).getReturnQuantity() + " "+ unit ,font8);
//					}else{
//						materialPh=new Phrase(serProject.getProdDetailsList().get(i).getName(),font8);
//					}
//					PdfPCell materialCell = new PdfPCell(materialPh);
//					materialCell.setHorizontalAlignment(Element.ALIGN_LEFT);					
//					table.addCell(materialCell);
//				}else{
//					table.addCell(blanksCell);
//				}
//			}
//			for (int i = 0; i < blankRows; i++) {
//				table.addCell(blanksCell);
//				table.addCell(blanksCell);
//				table.addCell(blanksCell);
//				table.addCell(blanksCell);
//			}	
//	}else{
//		int count = 0;
//		if(rowflag){
//			count = 4;
//		}else{
//			count= 8;
//		}
//		for (int i = 0; i <= count; i++) {
//			table.addCell(blanksCell);
//			table.addCell(blanksCell);
//			table.addCell(blanksCell);
//			table.addCell(blanksCell);
//		}
//	}
		
	
		/**28-10-2017 sagar sore [to make copy of pdf on half side of page]**/
	    PdfPCell serviceParaCell = new PdfPCell();
	    PdfPCell tableCell = new PdfPCell();
	    serviceParaCell.setBorder(0);
	    tableCell.setBorder(0);
	    serviceParaCell.addElement(servicepara);
	    tableCell.addElement(table);
	    a5Table1.addCell(serviceParaCell);
//		a5Table2.addCell(serviceParaCell);
		a5Table1.addCell(tableCell);
//		a5Table2.addCell(tableCell);
		
//		try {
//			document.add(a5Table1);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	
	private void trapCatchesList() {
		
		System.out.println("in side trap catch list");
		Phrase trapCatchesPhrase = null;
		
		
		trapCatchesPhrase = new Phrase("Trap Catches :",font8bold);
	
		
		Paragraph para = new Paragraph(trapCatchesPhrase);
		para.setAlignment(Element.ALIGN_LEFT);
		
		Phrase pestNameHeading = null;
		PdfPCell pestNameHeadingCell= null;
		
		Phrase trapCatches =null;
		PdfPCell trapCatchesCell = null;
		
		Phrase details = null;
		PdfPCell detailsCell = null;
		
		 pestNameHeading = new  Phrase("Pest Name",font8bold);
		 pestNameHeadingCell = new PdfPCell(pestNameHeading);
		 pestNameHeadingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		 pestNameHeadingCell.setPadding(4f);
		 
		 trapCatches = new  Phrase("Trap Catches",font8bold);
		 trapCatchesCell = new PdfPCell(trapCatches);
		 trapCatchesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		 trapCatchesCell.setPadding(4f);
		
		 details = new  Phrase("Location/No.",font8bold);
		 detailsCell = new PdfPCell(details);
		 detailsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		 detailsCell.setPadding(4f);
		 
		 
			PdfPTable table = new PdfPTable(6);
			table.setWidthPercentage(100f);
			table.setWidthPercentage(100f);
			
			table.setSpacingBefore(5f);
			table.setSpacingAfter(10f);
			
			
			table.addCell(pestNameHeadingCell);
			table.addCell(trapCatchesCell);
			table.addCell(detailsCell);
		
			table.addCell(pestNameHeadingCell);
			table.addCell(trapCatchesCell);
			table.addCell(detailsCell);
			boolean flag = false;
			for(Service ser : serviceList){
				if(ser.getCatchtrapList().size() > 0){
					flag = true;
				}
			}
			
		if(flag)
		{
			
				Phrase detailsValue = new Phrase(" ",font8);
				PdfPCell detailsValueCell = new PdfPCell(detailsValue);
				detailsValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				int count = 0;
				Phrase pestName =null;
				Phrase catches=null;
				Phrase locationPh =null;
				for(Service sr : serviceList){
				for(int i=0; i<sr.getCatchtrapList().size();i++)
				{
					pestName = new Phrase(sr.getCatchtrapList().get(i).getPestName(),font8);
					PdfPCell pestNameCell = new PdfPCell(pestName);
					pestNameCell.setPadding(4f);
					pestNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					table.addCell(pestNameCell);
					
					catches = new Phrase(sr.getCatchtrapList().get(i).getCount()+"",font8);
					PdfPCell catchesCell = new PdfPCell(catches);
					catchesCell.setPadding(4f);
					catchesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					table.addCell(catchesCell);
					
					locationPh = new Phrase(sr.getCatchtrapList().get(i).getLocation()+"",font8);	
					PdfPCell locationCell = new PdfPCell(locationPh);
					locationCell.setPadding(4f);
					locationCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					table.addCell(locationCell);
					
					count = (i+1)*3;
				}
		}
				 if(rowflag){
		            	for (int i = count; i <=18 ; i++) {
		    				
		    				table.addCell(detailsValueCell);
		    			}
		            }else{
		            	for (int i = count; i <=24 ; i++) {//48
		 				
		    				table.addCell(detailsValueCell);
		    			}
		   			
		            }

			    /** 30-10-2017 sagar sore [commented to show same content on left and right side after vertically partitioned ]**/
//				try {
//					document.add(para);
//					document.add(table);
//				} catch (DocumentException e) {
//					e.printStackTrace();
//				}
				/**28-10-2017 sagar sore [to make copy of pdf on half side of page]**/
				
				 PdfPCell paraCell = new PdfPCell();
				    PdfPCell tableCell = new PdfPCell();
				    paraCell.setBorder(0);
				    tableCell.setBorder(0);
				    paraCell.addElement(para);
				    tableCell.addElement(table);
				    a5Table1.addCell(paraCell);
//					a5Table2.addCell(paraCell);
					a5Table1.addCell(tableCell);
//					a5Table2.addCell(tableCell);
		
			
			}
		else
		{
//			PdfPTable table = new PdfPTable(6);
//			table.setWidthPercentage(100f);
//			
//			table.setSpacingBefore(5f);
//			table.setSpacingAfter(10f);
//			table.addCell(pestNameHeadingCell);
//			table.addCell(trapCatchesCell);
//			table.addCell(detailsCell);
//		
//			table.addCell(pestNameHeadingCell);
//			table.addCell(trapCatchesCell);
//			table.addCell(detailsCell);
			
			Phrase detailsValue = new Phrase(" ",font8);
			PdfPCell detailsValueCell = new PdfPCell(detailsValue);
			detailsValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			/*
			 * commented by Ashwini
			 */
			
//			for (int i = 0; i <=48 ; i++) {
//				
//				table.addCell(detailsValueCell);
//			}
//			
			/*
			 * @author Ashwini
			 * Date:21-01-2018
			 */
            if(rowflag){
            	for (int i = 0; i <=18 ; i++) {
    				
    				table.addCell(detailsValueCell);
    			}
            }else{
            	for (int i = 0; i <=48 ; i++) {
 				
    				table.addCell(detailsValueCell);
    			}
   			
            }
			
			
			/*
			 * end by Ashwini
			 */

		    /** 30-10-2017 sagar sore [commented to show same content on left and right side after vertically partitioned ]**/
//			try {
//				document.add(para);
//				document.add(table);
//			} catch (DocumentException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			
			/**28-10-2017 sagar sore [to make copy of pdf on half side of page]**/
			 PdfPCell paraCell = new PdfPCell();
			    PdfPCell tableCell = new PdfPCell();
			    paraCell.setBorder(0);
			    tableCell.setBorder(0);
			    paraCell.addElement(para);
			    tableCell.addElement(table);
			    a5Table1.addCell(paraCell);
	//			a5Table2.addCell(paraCell);
				a5Table1.addCell(tableCell);
	//			a5Table2.addCell(tableCell);
	
		}
	}
	
	
	private void feedbackFromCustomer() 
	{
		float[] columnWidths = { 2.8f,2.2f,2.5f,2.5f};
		PdfPTable table = new PdfPTable(4);
		table.setWidthPercentage(100f);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		Phrase obsByNBHC = new Phrase("Observations by Pecopp : ",font8bold);
		PdfPCell obsByNBHCCell = new PdfPCell(obsByNBHC);
		obsByNBHCCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		obsByNBHCCell.setBorder(0);
		
		
		Phrase sideBlank = null;
	//	PdfPCell sideBlankCell = null;
		
			sideBlank = new Phrase(service.getTechnicianRemark(),font8);
			PdfPCell sideBlankCell = new PdfPCell(sideBlank);
//			sideBlankCell.setBorderWidthRight(0);
//			sideBlankCell.setBorderWidthLeft(0);
//			sideBlankCell.setBorderWidthTop(0);
			sideBlankCell.setBorder(0);
			//sideBlankCell.setPadding(3f);
			
			
		
//		else
//		{
//			sideBlank = new Phrase(" ",font8);
//			PdfPCell sideBlankCell = new PdfPCell(sideBlank);
////			sideBlankCell.setBorderWidthRight(0);
////			sideBlankCell.setBorderWidthLeft(0);
////			sideBlankCell.setBorderWidthTop(0);
//			sideBlankCell.setBorder(0);
//			for (int i = 0; i <= 1; i++) {//5
//				commentsTable.addCell(sideBlankCell);
//			}
//		}
		
		Phrase comentsValue = new Phrase("Customer Comments : ",font8bold);
		PdfPCell comentsValueCell = new PdfPCell(comentsValue);
		comentsValueCell.setBorder(0);
		
		
		String remark = "";
		Phrase comentsValueBlank = null;
		if(service.getServiceCompleteRemark()!=null && !service.getServiceCompleteRemark().equals(""))
		{
			remark = service.getServiceCompleteRemark();
			comentsValueBlank = new Phrase(remark,font8);
		}else{
			comentsValueBlank = new Phrase(" ",font8);
		}
		
		PdfPCell comentsValueBlankCell = new PdfPCell(comentsValueBlank);
		comentsValueBlankCell.setBorder(0);
	//	comentsValueBlankCell.setPadding(3f);
		
	
		
//		if(service.getServiceCompleteRemark()!=null && !service.getServiceCompleteRemark().equals(""))
//		{
//			PdfPCell remarkValueCell = new PdfPCell(comentsValueBlank);
//			remarkValueCell.setBorder(0);
//			commentsTable.addCell(remarkValueCell);
//		}else{
//			commentsTable.addCell(comentsValueBlankCell);
//			//commentsTable.addCell(comentsValueBlankCell);
//			//commentsTable.addCell(comentsValueBlankCell);
//		}
		
//		PdfPCell commentsTableCell = new PdfPCell(commentsTable);
//		commentsTableCell.setBorder(0);
		
	
		
		
		
		
//		PdfPTable obsTable = new PdfPTable(1);
//		obsTable.setWidthPercentage(100f);
		
//		Phrase obsByNBHC = new Phrase("Observations by Pecopp",font8bold);
//		PdfPCell obsByNBHCCell = new PdfPCell(obsByNBHC);
//		obsByNBHCCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		obsByNBHCCell.setBorder(0);
//		obsTable.addCell(obsByNBHCCell);
//		
//		Phrase sideBlank = null;
//		if(service.getTechnicianRemark()!=null && !service.getTechnicianRemark().equals(""))
//		{
//			sideBlank = new Phrase(service.getTechnicianRemark(),font8);
//			PdfPCell sideBlankCell = new PdfPCell(sideBlank);
//			sideBlankCell.setBorderWidthRight(0);
//			sideBlankCell.setBorderWidthLeft(0);
//			sideBlankCell.setBorderWidthTop(0);
//			sideBlankCell.setPadding(3f);
//			obsTable.addCell(sideBlankCell);
//		}
//		else
//		{
//			sideBlank = new Phrase(" ",font8);
//			PdfPCell sideBlankCell = new PdfPCell(sideBlank);
////			sideBlankCell.setBorderWidthRight(0);
////			sideBlankCell.setBorderWidthLeft(0);
////			sideBlankCell.setBorderWidthTop(0);
//			sideBlankCell.setBorder(0);
//			for (int i = 0; i <= 1; i++) {//5
//				obsTable.addCell(sideBlankCell);
//			}
//		}
		
//		PdfPCell sideBlankCell = new PdfPCell(sideBlank);
//		sideBlankCell.setBorder(0);
//		obsTable.addCell(sideBlankCell);
		
//		sideBlankCell.setBorderWidthLeft(0);
//		sideBlankCell.setBorderWidthRight(0);
//		sideBlankCell.setBorderWidthTop(0);
//		obsTable.addCell(sideBlankCell);
//		obsTable.addCell(sideBlankCell);
//		obsTable.addCell(sideBlankCell);
//		obsTable.addCell(sideBlankCell);
		
		
//		PdfPCell obsTableCell = new PdfPCell();
//		obsTableCell.addElement(obsTable);
//		obsTableCell.setBorder(0);
//		
//		
//		
//		
//		PdfPTable blankTable = new PdfPTable(1);
//		blankTable.setWidthPercentage(100f);
//		
//		Phrase blank =  new Phrase();
//		PdfPCell blankcell = new PdfPCell(blank);
//		blankcell.setBorder(0);
////		blankTable.addCell(blankcell);
////		blankTable.addCell(blankcell);
////		blankTable.addCell(blankcell);
////		blankTable.addCell(blankcell);
////		blankTable.addCell(blankcell);
//		
//		PdfPCell blankTableCell = new PdfPCell();
//		blankTableCell.addElement(blankTable);
//		blankTableCell.setBorder(0);
//		
//	
//		
//		PdfPTable feedbackTable = new PdfPTable(1);
//		feedbackTable.setWidthPercentage(100f);
//	
//		float columnWidths2[] = {2.5f,7.5f}; 
//		try {
//			feedbackTable.setWidths(columnWidths2);
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
		
		
//		Phrase customerFeedBack = new Phrase("Customer Feedback",font8bold);
//		PdfPCell customerFeedBackCell = new PdfPCell(customerFeedBack);
//		customerFeedBackCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		customerFeedBackCell.setBorder(0);
//	//	feedbackTable.addCell(customerFeedBackCell);
//		
////		Phrase FeedBack = new Phrase(" ",font8bold);
////		PdfPCell FeedBackCell = new PdfPCell(FeedBack);
////		FeedBackCell.setHorizontalAlignment(Element.ALIGN_LEFT);
////		FeedBackCell.setBorder(0);
////		feedbackTable.addCell(FeedBackCell);
//		
//		Phrase blanks = new Phrase(" ",font8bold);
//		PdfPCell blanksCell = new PdfPCell(blanks);
//		blanksCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		blanksCell.setBorder(0);
////		feedbackTable.addCell(blanksCell);
////		feedbackTable.addCell(blanksCell);
//		
//		
//		// Service Quality Rating (Please circle your choice):
//		/** 30-10-2017 sagar sore[]**/
//	//	float columnWidths3[] = {3.8f,6.2f}; 
//		float columnWidths3[] = {4.0f,6.0f}; 
//		PdfPTable qualityRatingTable = new PdfPTable(2); 
//		qualityRatingTable.setWidthPercentage(100f);
//		
//		try {
//			qualityRatingTable.setWidths(columnWidths3);
//		} catch (DocumentException e2) {
//			// TODO Auto-generated catch block
//			e2.printStackTrace();
//		}
//		
//		Phrase serviceQualityRating =new Phrase("Service Quality Rating : ", font8bold);
//		PdfPCell serviceQualityRatingCell = new PdfPCell(serviceQualityRating);
//		serviceQualityRatingCell.setBorder(0);
//		//qualityRatingTable.addCell(serviceQualityRatingCell);
//		
//		
//		
//		Phrase customerFeedBackfromService=null;
//		PdfPCell customerFeedBackfromServiceCell = null;
//		String feedback = "";
//		if(service.getCustomerFeedback()!=null && !service.getCustomerFeedback().equals(""))
//		{
//			feedback = service.getCustomerFeedback();
//		}
//		
//		if(!feedback.equalsIgnoreCase("")){
//		//	feedbackTable.addCell(qualityRatingTableCell);
//		//	feedbackTable.addCell(ratingTabeleCell);
//		//	feedbackTable.addCell(blanksCell);
//			customerFeedBackfromService = new Phrase(feedback, font8);
//			customerFeedBackfromServiceCell = new PdfPCell(customerFeedBackfromService);
//			customerFeedBackfromServiceCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			customerFeedBackfromServiceCell.setBorder(0);
//			qualityRatingTable.addCell(customerFeedBackfromServiceCell);
//		}else{
//			qualityRatingTable.addCell(blanksCell);
//		}
//		
//		PdfPCell qualityRatingTableCell = new PdfPCell(qualityRatingTable);
//		qualityRatingTableCell.setBorder(0);
//		
//		PdfPTable ratingTabele = new  PdfPTable(1);
//		ratingTabele.setWidthPercentage(100f);
//
////		Phrase ratingPhrase =new Phrase("Excellent      Very Good      Good      Average     Poor" ,font8);
////		PdfPCell ratingCell = new PdfPCell(ratingPhrase);
////		ratingCell.setBorder(0);
////		ratingTabele.addCell(ratingCell);
//		
////		PdfPCell ratingTabeleCell = new PdfPCell(ratingTabele);
////		ratingTabeleCell.setBorder(0);
//		
		
		
		
//		
//		 
//		PdfPTable commentsTable = new  PdfPTable(4);
//		commentsTable.setWidthPercentage(100f);
//		
//		float mycolumnWidths[] = {2.7f,2.3f,2.7f,2.3f}; 
//		
//		try {
//			commentsTable.setWidths(mycolumnWidths);
//		} catch (DocumentException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		
//		Phrase obsByNBHC = new Phrase("Observations by Pecopp",font8bold);
//		PdfPCell obsByNBHCCell = new PdfPCell(obsByNBHC);
//		obsByNBHCCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		obsByNBHCCell.setBorder(0);
//		commentsTable.addCell(obsByNBHCCell);
//		
//		Phrase sideBlank = null;
//		if(service.getTechnicianRemark()!=null && !service.getTechnicianRemark().equals(""))
//		{
//			sideBlank = new Phrase(service.getTechnicianRemark(),font8);
//			PdfPCell sideBlankCell = new PdfPCell(sideBlank);
////			sideBlankCell.setBorderWidthRight(0);
////			sideBlankCell.setBorderWidthLeft(0);
////			sideBlankCell.setBorderWidthTop(0);
//			sideBlankCell.setBorder(0);
//			sideBlankCell.setPadding(3f);
//			commentsTable.addCell(sideBlankCell);
//		}
//		else
//		{
//			sideBlank = new Phrase(" ",font8);
//			PdfPCell sideBlankCell = new PdfPCell(sideBlank);
////			sideBlankCell.setBorderWidthRight(0);
////			sideBlankCell.setBorderWidthLeft(0);
////			sideBlankCell.setBorderWidthTop(0);
//			sideBlankCell.setBorder(0);
//			for (int i = 0; i <= 1; i++) {//5
//				commentsTable.addCell(sideBlankCell);
//			}
//		}
//		
//		Phrase comentsValue = new Phrase("Customer Comments :",font8bold);
//		PdfPCell comentsValueCell = new PdfPCell(comentsValue);
//		comentsValueCell.setBorder(0);
//		commentsTable.addCell(comentsValueCell);
//		
//		String remark = "";
//		Phrase comentsValueBlank = null;
//		if(service.getServiceCompleteRemark()!=null && !service.getServiceCompleteRemark().equals(""))
//		{
//			remark = service.getServiceCompleteRemark();
//			comentsValueBlank = new Phrase(remark,font8);
//		}else{
//			comentsValueBlank = new Phrase(" ",font8);
//		}
//		
//		PdfPCell comentsValueBlankCell = new PdfPCell(comentsValueBlank);
//		comentsValueBlankCell.setBorder(0);
//		comentsValueBlankCell.setPadding(3f);
//		
//		commentsTable.addCell(comentsValueBlankCell);
//		
//		if(service.getServiceCompleteRemark()!=null && !service.getServiceCompleteRemark().equals(""))
//		{
//			PdfPCell remarkValueCell = new PdfPCell(comentsValueBlank);
//			remarkValueCell.setBorder(0);
//			commentsTable.addCell(remarkValueCell);
//		}else{
//			commentsTable.addCell(comentsValueBlankCell);
//			//commentsTable.addCell(comentsValueBlankCell);
//			//commentsTable.addCell(comentsValueBlankCell);
//		}
//		
//		PdfPCell commentsTableCell = new PdfPCell(commentsTable);
//		commentsTableCell.setBorder(0);
//		
//		
//				
//		PdfPTable parentTable = new PdfPTable(1);
//		parentTable.setWidthPercentage(100f);
////		parentTable.addCell(qualityRatingTableCell);
////		parentTable.addCell(ratingTabeleCell);
//	//	parentTable.addCell(blanksCell);
//		parentTable.addCell(commentsTableCell);
//	//	parentTable.addCell(comentsValueBlankCell);
//	//	parentTable.addCell(comentsValueBlankCell);
////		parentTable.addCell(comentsValueBlankCell);
//		
//		PdfPCell parentTableCell = new PdfPCell(parentTable);
////		parentTableCell.setBorderWidthRight(0);
////		parentTableCell.setBorderWidthTop(0);
////		parentTableCell.setBorderWidthBottom(0);
//		parentTableCell.setBorder(0);
//		
////		Phrase customerFeedBackfromService=null;
////		PdfPCell customerFeedBackfromServiceCell = null;
////		String feedback = "";
////		if(service.getCustomerFeedback()!=null && !service.getCustomerFeedback().equals(""))
////		{
////			feedback = service.getCustomerFeedback();
////		}
////		
////		if(!feedback.equalsIgnoreCase("")){
////		//	feedbackTable.addCell(qualityRatingTableCell);
////		//	feedbackTable.addCell(ratingTabeleCell);
////		//	feedbackTable.addCell(blanksCell);
////			customerFeedBackfromService = new Phrase(feedback, font8bold);
////			customerFeedBackfromServiceCell = new PdfPCell(customerFeedBackfromService);
////			customerFeedBackfromServiceCell.setHorizontalAlignment(Element.ALIGN_LEFT);
////			customerFeedBackfromServiceCell.setBorder(0);
////			feedbackTable.addCell(customerFeedBackfromServiceCell);
////			feedbackTable.addCell(commentsTableCell);
////			//feedbackTable.addCell(parentTableCell);
////		}
////		else
////		{
		
		
		
		
		
		table.addCell(obsByNBHCCell);
		table.addCell(sideBlankCell);
		table.addCell(comentsValueCell);
		table.addCell(comentsValueBlankCell);
		
//		}
//		
//		PdfPCell feedbackTableCell = new PdfPCell();
//		feedbackTableCell.addElement(table);
//		feedbackTableCell.setBorder(0);
		
		//table.addCell(obsTableCell);
		//table.addCell(blankTableCell);
		//table.addCell(feedbackTableCell);

	 
	    PdfPCell tableCell = new PdfPCell();
	    tableCell.setBorder(0);
	    tableCell.addElement(table);	   
		a5Table1.addCell(tableCell);
//		a5Table2.addCell(tableCell);
		

		
	}
	
	private void sinatureofCustAndNBHCRepresentative() {
		/**30-10-2017 sagar sore** [added column widths]**/
			float sinatureofCustAndNBHCRepresentative[] = {4.0f,3.0f,3.0f};
		PdfPTable table = new PdfPTable(sinatureofCustAndNBHCRepresentative);
		table.setWidthPercentage(100f);
		
          table.setSpacingBefore(5f);
		table.setSpacingAfter(5f);
		
		Phrase name = new Phrase("Name",font8bold);
		PdfPCell nameCell = new PdfPCell(name);
		nameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(nameCell);
		
		Phrase signature = new Phrase("Customer Signature",font8bold);
		PdfPCell signatureCell = new PdfPCell(signature);
		signatureCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(signatureCell);
		
//		Phrase date = new Phrase("Date",font8bold);
//		PdfPCell dateCell = new PdfPCell(date);
//		dateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		table.addCell(dateCell);
		
		Phrase blankThreeSide = new Phrase(" ",font8);
		PdfPCell blankThreeSideCell = new PdfPCell(blankThreeSide);
		blankThreeSideCell.setBorder(0);
		//table.addCell(blankThreeSideCell);// this line comment
		
//		Phrase NBHCrepresentative = new Phrase("Technician  : "+service.getEmployee(),font8);
//		PdfPCell NBHCrepresentativeCell = new PdfPCell(NBHCrepresentative);
//		table.addCell(NBHCrepresentativeCell);
//		
		PdfPTable leftTable = new PdfPTable(1);
		leftTable.setWidthPercentage(100);
		
		
		Phrase NBHCrepresentative = new Phrase("Technician  : "+service.getEmployee(),font8);
		PdfPCell NBHCrepresentativeCell = new PdfPCell(NBHCrepresentative);
		NBHCrepresentativeCell.setBorder(0);
		leftTable.addCell(NBHCrepresentativeCell);
		
		Phrase blankThreeSide1 = new Phrase(" ",font8);
		PdfPCell blankThreeSideCell1 = new PdfPCell(blankThreeSide1);
		blankThreeSideCell1.setBorder(0);
		leftTable.addCell(blankThreeSideCell1);// this line comment
		
		
		String desg_empcode_name="";
		if(service.getCustomerSignName()!=null&&!service.getCustomerSignName().equals("")){
			desg_empcode_name=service.getCustomerSignName();
		}
		if(service.getCustomerPersonDesignation()!=null&&!service.getCustomerPersonDesignation().equals("")){
			desg_empcode_name=desg_empcode_name+ "/"+service.getCustomerPersonDesignation();
		}
		
		if(service.getCustomerPersonEmpId()!=null&&!service.getCustomerPersonEmpId().equals("")){
			desg_empcode_name=desg_empcode_name+"/"+service.getCustomerPersonEmpId();
		}
		
//		Phrase customerRepresentative = new Phrase("Customer Representative : " +service.getCustomerSignName(),font8);
		Phrase customerRepresentative = new Phrase("Customer Representative : " +desg_empcode_name,font8);
		PdfPCell customerRepresentativeCell = new PdfPCell(customerRepresentative);
		customerRepresentativeCell.setBorder(0);
		leftTable.addCell(customerRepresentativeCell);
		
		
		PdfPTable middleTable = new PdfPTable(1);
		middleTable.setWidthPercentage(100);
		
		if(service.getCustomerSignature() != null ){
			String hostUrl;
			String environment = System
					.getProperty("com.google.appengine.runtime.environment");
			if (environment.equals("Production")) {
				String applicationId = System
						.getProperty("com.google.appengine.application.id");
				String version = System
						.getProperty("com.google.appengine.application.version");
				hostUrl = "http://" + version + "." + applicationId
						+ ".appspot.com/";
			} else {
				hostUrl = "http://localhost:8888";
			}
			PdfPCell imageSignCell = null;
			Image image2 = null;
			try {
				try{
				image2 = Image
						.getInstance(new URL(hostUrl + service.getCustomerSignature().getUrl()));
				logger.log(Level.SEVERE,"screen" + image2+"");
				}catch(Exception e){
				//	if(image2 == null){
						
						image2 = Image
								.getInstance(new URL(service.getCustomerSignature().getUrl())); 
						logger.log(Level.SEVERE, "Android " +image2+"");
				//		}
				}
					
					
				
				image2.scalePercent(13f);
				image2.scaleAbsoluteWidth(100);
				image2.scaleAbsoluteHeight(40);;
				
				imageSignCell = new PdfPCell(image2);
				//imageSignCell.setBorder(0);
				imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				//imageSignCell.setRowspan(2);
				imageSignCell.setPaddingTop(3);
				middleTable.addCell(imageSignCell);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			//blankThreeSideCell.setRowspan(2);
			blankThreeSideCell.setPaddingTop(3);
			//blankThreeSideCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			middleTable.addCell(blankThreeSideCell);
		}
		
		
//		PdfPTable rightTable = new PdfPTable(1);
//		rightTable.setWidthPercentage(100);
//		
//		if(service.getServiceCompletionDate() != null){
//			Phrase serviceDate = new Phrase(" ",font8);//fmt.format(service.getServiceCompletionDate())+
//			PdfPCell serviceDateCell = new PdfPCell(serviceDate);
//			serviceDateCell.setBorder(0);
//			rightTable.addCell(serviceDateCell);
//		}else{
//			rightTable.addCell(blankThreeSideCell);
//		}
//		
//		rightTable.addCell(blankThreeSideCell);
//		
//		if(service.getServiceCompletionDate() != null){
//			Phrase serviceDate = new Phrase(fmt.format(service.getServiceCompletionDate())+"",font8);
//			PdfPCell serviceDateCell = new PdfPCell(serviceDate);
//			serviceDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			serviceDateCell.setBorder(0);
//			rightTable.addCell(serviceDateCell);
//		}else{
//			rightTable.addCell(blankThreeSideCell);
//		}
		
		
		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);
		
		rightTable.setSpacingBefore(5f);
		rightTable.setSpacingAfter(5f);
		
		
//		if(service.getServiceCompletionOTP()!=null && !service.getServiceCompletionOTP().equals("")){

		Phrase blank = new Phrase(" ",font8);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setBorder(0);
		
		if(service.getCustomerSignature() != null && service.getCustomerSignature().getUrl().length()>0){
			logger.log(Level.SEVERE,"service.getCustomerSignature()"+service.getCustomerSignature());
			rightTable.addCell(blankCell);
		}
		else{
			logger.log(Level.SEVERE, "No signature");
			Phrase otp = new Phrase("OTP : "+service.getServiceCompletionOTP(),font8);
			PdfPCell otpCell = new PdfPCell(otp);
			otpCell.setBorder(0);
			otpCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			otpCell.setRowspan(2);
			rightTable.addCell(otpCell);
		}
			
			
			rightTable.addCell(blankThreeSideCell);
//			rightTable.addCell(blankThreeSideCell);
			
//			blankCell.setRowspan(2);
			rightTable.addCell(blankCell);

//		}
//		else{
//			
//			Phrase blank = new Phrase(" ",font8);
//			PdfPCell blankCell = new PdfPCell(blank);
//			blankCell.setBorder(0);
////			blankCell.setRowspan(2);
//			rightTable.addCell(blankCell);
//		}
		
		Phrase customerName = new Phrase(service.getPersonInfo().getFullName(),font8);
		PdfPCell customerNameCell = new PdfPCell(customerName);
		customerNameCell.setBorder(0);
		customerNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		customerNameCell.setRowspan(2);

		rightTable.addCell(customerNameCell);
		
		if(service.getPersonInfo().getCellNumber()!=null && !service.getPersonInfo().getCellNumber().equals("0")){
			
			Phrase customerNumber = new Phrase(service.getPersonInfo().getCellNumber()+"",font8);
			PdfPCell customerNumberCell = new PdfPCell(customerNumber);
			customerNumberCell.setBorder(0);
			customerNumberCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			rightTable.addCell(customerNumberCell);
		}
		
		
		
		
		
		
//	if(service.getCustomerSignature() != null ){
//		String hostUrl;
//		String environment = System
//				.getProperty("com.google.appengine.runtime.environment");
//		if (environment.equals("Production")) {
//			String applicationId = System
//					.getProperty("com.google.appengine.application.id");
//			String version = System
//					.getProperty("com.google.appengine.application.version");
//			hostUrl = "http://" + version + "." + applicationId
//					+ ".appspot.com/";
//		} else {
//			hostUrl = "http://localhost:8888";
//		}
//		PdfPCell imageSignCell = null;
//		Image image2 = null;
//		try {
//			try{
//			image2 = Image
//					.getInstance(new URL(hostUrl + service.getCustomerSignature().getUrl()));
//			logger.log(Level.SEVERE,"screen" + image2+"");
//			}catch(Exception e){
//			//	if(image2 == null){
//					
//					image2 = Image
//							.getInstance(new URL(service.getCustomerSignature().getUrl())); 
//					logger.log(Level.SEVERE, "Android " +image2+"");
//			//		}
//			}
//				
//				
//			
//			image2.scalePercent(13f);
//			image2.scaleAbsoluteWidth(100);
//			image2.scaleAbsoluteHeight(40);;
//			
//			imageSignCell = new PdfPCell(image2);
//			//imageSignCell.setBorder(0);
//			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			imageSignCell.setRowspan(2);
//			imageSignCell.setPaddingTop(3);
//			table.addCell(imageSignCell);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}else{
//		blankThreeSideCell.setRowspan(2);
//		blankThreeSideCell.setPaddingTop(3);
//		//blankThreeSideCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		table.addCell(blankThreeSideCell);
//	}
		
//	if(service.getServiceCompletionDate() != null){
//		Phrase serviceDate = new Phrase(" ",font8);//fmt.format(service.getServiceCompletionDate())+
//		PdfPCell serviceDateCell = new PdfPCell(serviceDate);
//		table.addCell(serviceDateCell);
//	}else{
//		table.addCell(blankThreeSideCell);
//	}
	
//	String desg_empcode_name="";
//	if(service.getCustomerSignName()!=null&&!service.getCustomerSignName().equals("")){
//		desg_empcode_name=service.getCustomerSignName();
//	}
//	if(service.getCustomerPersonDesignation()!=null&&!service.getCustomerPersonDesignation().equals("")){
//		desg_empcode_name=desg_empcode_name+ "/"+service.getCustomerPersonDesignation();
//	}
//	
//	if(service.getCustomerPersonEmpId()!=null&&!service.getCustomerPersonEmpId().equals("")){
//		desg_empcode_name=desg_empcode_name+"/"+service.getCustomerPersonEmpId();
//	}
//	
////	Phrase customerRepresentative = new Phrase("Customer Representative : " +service.getCustomerSignName(),font8);
//	Phrase customerRepresentative = new Phrase("Customer Representative : " +desg_empcode_name,font8);
//	PdfPCell customerRepresentativeCell = new PdfPCell(customerRepresentative);
//	table.addCell(customerRepresentativeCell);
	
//		if(service.getServiceCompletionDate() != null){
//			Phrase serviceDate = new Phrase(fmt.format(service.getServiceCompletionDate())+"",font8);
//			PdfPCell serviceDateCell = new PdfPCell(serviceDate);
//			table.addCell(serviceDateCell);
//		}else{
//			table.addCell(blankThreeSideCell);
//		}
		

		//image2 = Image.getInstance(new URL(digitalsign.getUrl())); 
	    /** 30-10-2017 sagar sore [commented to show same content on left and right side after vertically partitioned ]**/
//		try {
//			document.add(table);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		/**28-10-2017 sagar sore [to make copy of pdf on half side of page]**/
//		table.addCell(blankThreeSideCell);
//		table.addCell(blankThreeSideCell);
//		table.addCell(blankThreeSideCell);
		
		PdfPCell leftCell = new PdfPCell(leftTable);
		leftCell.setRowspan(2);

		//leftCell.setBorder(0);
		// leftCell.addElement();
		
		PdfPCell middleCell = new PdfPCell(middleTable);
		//middleCell.setBorder(0);
		middleCell.setRowspan(2);

		
		PdfPCell rightCell = new PdfPCell(rightTable);
		rightCell.setRowspan(2);
//		rightCell.setRowspan(4);
		// rightCell.setBorder(0);
		// rightCell.addElement();
		
		table.addCell(rightCell);

		
		table.addCell(leftCell);
		table.addCell(middleCell);
//		table.addCell(rightCell);
		
		
		
	    PdfPCell tableCell = new PdfPCell(table);
	    tableCell.setBorder(0);	 //uncomment this 
//	    tableCell.setBorderWidthTop(0);
//	    tableCell.setBorderWidthLeft(0);
//	    tableCell.setBorderWidthRight(0);
//	    tableCell.addElement(table);
		a5Table1.addCell(tableCell);
//		a5Table2.addCell(tableCell);
//		a5Table1.setSpacingAfter(10f);
	}
	
	
	
	private void createHeader() {
		
		/**
		 * @author Vijay Chougule Date:- 24-07-2020
		 * Des :- As per the requirement here must be print the Branch Address as per Nitin Sir
		 * if branch address is not defined then it will print Company address.
		 */
		
//		if(branchDt.getAddress()!=null){
//			if(!branchDt.getAddress().getCompleteAddress().equals("")){
//				checkBranchdata = true;
//			}
//			else{
//				checkBranchdata = false;
//
//			}
//		}
		
		
		PdfPTable headerTAB=new PdfPTable(3);
		headerTAB.setWidthPercentage(100);
		try {
			headerTAB.setWidths(new float[]{20,60,20});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPTable headertable=new PdfPTable(1);
		headertable.setWidthPercentage(100);
		headertable.setSpacingAfter(10);
		
		
		 String companyName ="";
			if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
				companyName=branchDt.getCorrespondenceName();
			}else{
				companyName = comp.getBusinessUnitName().trim().toUpperCase();
			}
		//Set Company Name
//		String businessunit=null;
////		if(checkBranchdata) {
////			businessunit=branchDt.getBusinessUnitName();
////		}
////		else {
//			businessunit=comp.getBusinessUnitName();
////		}
		
		Phrase companyNameph = new Phrase(companyName,font12bold);
		PdfPCell companyNameCell = new PdfPCell(companyNameph);
		companyNameCell.setBorder(0);
		companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headertable.addCell(companyNameCell);
		
		//Set Address of Company
		String address = "";
		if(branchDt!=null&&branchDt.getAddress().getCompleteAddress()!=null) {
			address=branchDt.getAddress().getCompleteAddress();
		} else {
			address=comp.getAddress().getCompleteAddress();
		}
		
		Phrase companyAddph = new Phrase(address, font9);
		PdfPCell companyAddCell = new PdfPCell(companyAddph);
		companyAddCell.setBorder(0);
		companyAddCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headertable.addCell(companyAddCell);

		//Email ID
		String emailid="";
	   if(comp.getEmail()==null||comp.getEmail().equals("")){
		   emailid="";
	   }else{
		   emailid=comp.getEmail(); 
	   }
		
		
		Phrase companyemailph = new Phrase("E-Mail : "+emailid, font9);
		PdfPCell companyemailCell = new PdfPCell(companyemailph);
		companyemailCell.setBorder(0);
		companyemailCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headertable.addCell(companyemailCell);
		
		
		//WebSite
		String website="";
		if(comp.getWebsite()==null || comp.getWebsite().equals(""))
		{
			website="";
		}
		else
		{
			
			website="Website : "+comp.getWebsite();
		}
		Phrase companyWebph = new Phrase(website, font9);
		PdfPCell companyWebCell = new PdfPCell(companyWebph);
		companyWebCell.setBorder(0);
		companyWebCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headertable.addCell(companyWebCell);
		
		
		//Contact
       String contactinfo="";
		
			if (comp.getCellNumber1() != null && comp.getCellNumber1() != 0) {
				contactinfo = comp.getCellNumber1() + "";
			}
//			}else
//			{
//				contactinfo = comp.getCellNumber1() + "";
//			}
//			if (branchDt.getCellNumber2() != null && branchDt.getCellNumber2() != 0) {
//				if (!contactinfo.trim().isEmpty()) {
//					contactinfo = contactinfo + " / " + branchDt.getCellNumber2() + "";
//				} else {
//					contactinfo = branchDt.getCellNumber2()
//							+ "";
//				}
//				System.out.println("pn33" + contactinfo);
//			}else{
//				if (!contactinfo.trim().isEmpty()) {
//					contactinfo = contactinfo + " / " + comp.getCountryCode()
//							+ comp.getCellNumber2() + "";
//				} else {
//					contactinfo = comp.getCountryCode() + comp.getCellNumber2()
//							+ "";
//				}
//			}
			
		
		Phrase companymobph = new Phrase("Phone :  "+contactinfo,font9);
		PdfPCell companymobCell = new PdfPCell(companymobph);
		companymobCell.setBorder(0);
		companymobCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headertable.addCell(companymobCell);
		
		PdfPTable logoTable = new PdfPTable(2);
		logoTable.setSpacingAfter(5);
		logoTable.setWidthPercentage(100f);
		try {
			logoTable.setWidths(new float[]{8,92});
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		DocumentUpload logodocument ;
	
			logodocument =comp.getLogo();
		
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2=null;
		if(logodocument!=null){
		try {
			image2 = Image.getInstance(new URL(hostUrl+logodocument.getUrl()));
			image2.scalePercent(20f);
//			image2.setAbsolutePosition(40f,765f);	
//			doc.add(image2);
			
			imageSignCell = new PdfPCell();
			imageSignCell.addElement(image2);
			imageSignCell.setImage(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			imageSignCell.setFixedHeight(20);
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
		
		Phrase blank =new Phrase(" ");
		PdfPCell blankCell=new PdfPCell(blank);
		blankCell.setBorder(0);
		if(imageSignCell != null)
		{
			logoTable.addCell(blankCell);
			logoTable.addCell(imageSignCell);
		}
		else
		{
			logoTable.addCell(blankCell);
			logoTable.addCell(blankCell);
		}

		PdfPTable titletab = new PdfPTable(1);
		titletab.setWidthPercentage(100f);
		titletab.setSpacingAfter(5);
		
		Phrase title =new Phrase("",font9bold);
		PdfPCell titlecell=new PdfPCell(title);
		titlecell.setBorder(0);
		titlecell.setPaddingRight(3);
		titlecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		titletab.addCell(titlecell);
		
		
		PdfPCell logocell=new PdfPCell(logoTable);
		//logocell.setBorder(0);
		logocell.setBorderWidthLeft(0);
		logocell.setBorderWidthRight(0);
		logocell.setBorderWidthTop(0);
		
		PdfPCell headerCell=new PdfPCell(headertable);
		//headerCell.setBorder(0);
		headerCell.setBorderWidthLeft(0);
		headerCell.setBorderWidthRight(0);
		headerCell.setBorderWidthTop(0);
		//headerCell.setBorderWidthRight(0);
		
		PdfPCell titlcell=new PdfPCell(titletab);
		titlcell.setBorderWidthLeft(0);
		titlcell.setBorderWidthRight(0);
		titlcell.setBorderWidthTop(0);
		//titlcell.setBorder(0);
		
		headerTAB.addCell(logocell);
		headerTAB.addCell(headerCell);
		headerTAB.addCell(titlcell);
		
		
		
		
		
		
		try {
			document.add(headerTAB);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable headertab2=new PdfPTable(3);
		headertab2.setWidthPercentage(100);
		try {
			headertab2.setWidths(new float[]{33,33,33});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		
	}

	private void createCompanyDetails(){



		DocumentUpload logodocument = comp.getLogo();
		
		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image
					.getInstance(new URL(hostUrl + logodocument.getUrl()));
			image2.scalePercent(20f);
			// image2.setAbsolutePosition(40f,765f);
			// doc.add(image2);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(20);
		} catch (Exception e) {
			e.printStackTrace();
		}

		
		 PdfPTable logotab=new PdfPTable(1);
		 logotab.setWidthPercentage(100);
		 
		 if (imageSignCell != null) {
			 logotab.addCell(imageSignCell);
			} 
		 else {
				Phrase blank = new Phrase(" ");
				PdfPCell blankCell = new PdfPCell(blank);
				blankCell.setBorder(0);
				logotab.addCell(blankCell);
			}
		
		 //End by jayshree
		 
		 String companyName ="";
			if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
				companyName=branchDt.getCorrespondenceName();
			}else{
				companyName = comp.getBusinessUnitName().trim().toUpperCase();
			}
		 
		 
		// Phrase companyName= new Phrase(comp.getBusinessUnitName(),font16bold);
		 
//	     Paragraph p =new Paragraph();
//	     p.add(Chunk.NEWLINE);
//	     p.add(companyName);
//	     p.setAlignment(Element.ALIGN_CENTER);
		 Paragraph companynamepara = new Paragraph();
		 companynamepara.add(companyName);
		 companynamepara.setFont(font14bold);
		 
			
	     PdfPCell companyNameCell=new PdfPCell(companynamepara);
	     companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	     companyNameCell.setFixedHeight(0);
	    
//	     companyNameCell.addElement(p);
	     companyNameCell.setBorder(0);
	     
	     
	     String custAdd1="";
			String custFullAdd1="";
			
			if(comp.getAddress()!=null){
				
				if(comp.getAddress().getAddrLine2()!=null && !comp.getAddress().getAddrLine2().equals("")){
				
					if(comp.getAddress().getLandmark()!=null && !comp.getAddress().getLandmark().equals("")){
						custAdd1=comp.getAddress().getAddrLine1()+","+comp.getAddress().getAddrLine2()+","+comp.getAddress().getLandmark();
					}else{
						custAdd1=comp.getAddress().getAddrLine1()+","+comp.getAddress().getAddrLine2();
					}
				}else{
					if(comp.getAddress().getLandmark()!=null && !comp.getAddress().getLandmark().equals("") ){
						custAdd1=comp.getAddress().getAddrLine1()+"\n"+comp.getAddress().getLandmark();
					}else{
						custAdd1=comp.getAddress().getAddrLine1();
					}
				}
				
				if(comp.getAddress().getLocality()!=null&& !comp.getAddress().getLocality().equals("")){
					if(comp.getAddress().getPin()!=0){
						custFullAdd1=custAdd1+"\n"+comp.getAddress().getLocality()+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry()+","+comp.getAddress().getPin();
					}else{
						custFullAdd1=custAdd1+"\n"+comp.getAddress().getLocality()+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
					}
				}else{
					if(comp.getAddress().getPin()!=0){
						custFullAdd1=custAdd1+"\n"+comp.getAddress().getCity()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry()+","+comp.getAddress().getPin();
					}else{
						custFullAdd1=custAdd1+"\n"+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
					}
				}
				
			}	
			
				Phrase addressline=new Phrase(custFullAdd1,font10);
				Paragraph addresspara=new Paragraph();
				addresspara.add(addressline);
				addresspara.setAlignment(Element.ALIGN_CENTER);
				
				PdfPCell addresscell=new PdfPCell();
				addresscell.addElement(addresspara);
				addresscell.setBorder(0);
			
	     
//		String addressline1="";
//		
//		if(comp.getAddress().getAddrLine2()!=null){
//			addressline1=comp.getAddress().getAddrLine1()+" , "+comp.getAddress().getAddrLine2();
//		}
//		else{
//			addressline1=comp.getAddress().getAddrLine1();
//		}
//		Phrase addressline=new Phrase(addressline1,font12);
//		Paragraph addresspara=new Paragraph();
//		addresspara.add(addressline);
//		addresspara.setAlignment(Element.ALIGN_CENTER);
//		
//		
//		
//		String pinno;
//		if(comp.getAddress().getPin()!=0){
//			pinno=""+comp.getAddress().getPin();
//		}
//		else
//		{
//			pinno="  ";
//		}
//		Phrase locality=null;
//		if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
//			System.out.println("inside both null condition1");
//			locality= new Phrase(comp.getAddress().getLandmark()+" ,"+comp.getAddress().getLocality()+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry()+","+pinno,font12);
//		}
//		else if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
//			System.out.println("inside both null condition 2");
//			locality= new Phrase(comp.getAddress().getLandmark()+" ,"+comp.getAddress().getCity()+","+comp.getAddress().getState()+" , "+comp.getAddress().getCountry()+","+pinno,font12);
//		}
//		
//		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
//			System.out.println("inside both null condition 3");
//			locality= new Phrase(comp.getAddress().getLocality()+" ,"+comp.getAddress().getCity()+","+comp.getAddress().getState()+" , "+comp.getAddress().getCountry()+","+pinno,font12);
//		}
//		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
//			System.out.println("inside both null condition 4");
//			locality= new Phrase(comp.getAddress().getCity()+" ,"+comp.getAddress().getState()+" , "+comp.getAddress().getCountry()+","+pinno,font12);
//		}
//		
//		Paragraph localityPragraph=new Paragraph();
//		localityPragraph.add(locality);
//		localityPragraph.setAlignment(Element.ALIGN_CENTER);
//		
		
		String mobile="";
		String landline="";
		
		
		if(comp.getCellNumber1()!=0){
			mobile=comp.getCellNumber1()+"";
		}
		else{
			mobile="";
		}
		
		if(comp.getLandline()!=0){
			landline=comp.getLandline()+"";
		}
		else{
			landline="";
		}
		
		String email="";
		if(comp.getEmail()!=null&&comp.getEmail()!="")
		{
			email=comp.getEmail()+"";
		}else{
			email="";
		}
		
		
//		String email = "";
//		ServerAppUtility serverApp = new ServerAppUtility();
//
//		if (checkEmailId == true) {
//			branchmail = serverApp.getBranchEmail(comp, con.getBranch());
//			System.out.println("server method " + branchmail);
//
//		} else {
//			branchmail = serverApp.getCompanyEmail(comp.getCompanyId());
//			System.out.println("server method 22" + branchmail);
//		}
//		
//		if(branchmail!=null){
//			email=branchmail;
//		}
//		else{
//			email="";
//		}
//		Phrase contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+"    Tel: "+comp.getLandline()+"    Email: "+comp.getEmail().trim(),font10);
		
//		String custCellno="";
//		String custContactDetails="";
//		
//		
//		if(comp.getCellNumber1()!=0){
//			custCellno=comp.getCellNumber1()+"";
//		}
//		else{
//			custCellno="";
//		}
//		
		
		Phrase contactinfo=new Phrase("Phone : "+mobile+"  "+"Email : "+email,font10);

		Paragraph realmobpara=new Paragraph();
		realmobpara.add(contactinfo);
		realmobpara.setAlignment(Element.ALIGN_CENTER);
		
//		Phrase contactinfo1=new Phrase("Email : "+email,font10);
//		Paragraph realmobpara1=new Paragraph();
//		realmobpara1.add(contactinfo1);
//		realmobpara1.setAlignment(Element.ALIGN_CENTER);
		
//		PdfPCell addresscell=new PdfPCell();
//		addresscell.addElement(addresspara);
////		addresscell.setBorder(0);
//		PdfPCell localitycell=new PdfPCell();
//		localitycell.addElement(localityPragraph);
//		localitycell.setBorder(0);
		PdfPCell contactcell=new PdfPCell();
		contactcell.addElement(realmobpara);
		contactcell.setBorder(0);
		
//		PdfPCell contactcell1=new PdfPCell();
//		contactcell1.addElement(realmobpara1);
//		contactcell1.setBorder(0);
		
		PdfPTable companyDetails=new PdfPTable(1);
		companyDetails.setWidthPercentage(100);
		
		companyDetails.addCell(companyNameCell);
		companyDetails.addCell(addresscell);
	//	companyDetails.addCell(localitycell);
//		if(comp.getCellNumber1()!=0&&email!=null&&!email.equals(""))
//		{
		companyDetails.addCell(contactcell);   //contactcell1
//		}
//		if(email!=null&&!email.equals("")){
//		//companyDetails.addCell(contactcell1);
//		}
		companyDetails.setSpacingAfter(12f);
		
	
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		try {
			parent.setWidths(new float[]{100});
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		PdfPCell logocell=new PdfPCell(logotab);
		logocell.setBorderWidthTop(0);
		logocell.setBorderWidthLeft(0);
		logocell.setBorderWidthRight(0);
//		parent.addCell(logocell);
		
		//End By Jayshree
		
		PdfPCell comapnyCell=new PdfPCell(companyDetails);
//		PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		comapnyCell.setBorderWidthTop(0);
		comapnyCell.setBorderWidthLeft(0);
		comapnyCell.setBorderWidthRight(0);
		comapnyCell.setFixedHeight(0);
		parent.addCell(comapnyCell);
//		parent.addCell(titlePdfCell);
		
		try {
			document.add(parent);
//			document.add(Chunk.NEWLINE);
//			document.add(Chunk.NEWLINE);
//			document.add(titlepdftable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

			
	}
	
	private void addTablesToParentTable() {
		PdfPCell a5TableCell1 = new PdfPCell(a5Table1);
		a5TableCell1.setBorder(0);
//		a5TableCell1.addElement(a5Table1);//Date 11/12/2017 comment By Jayshree To remaove the space
		a5TableCell1.setPaddingRight(20);//Date 11/12/2017 by Jayshree set the padding to increse the size
//		PdfPCell a5TableCell2 = new PdfPCell(a5Table2);
//		a5TableCell2.setBorder(0);
//		a5TableCell2.addElement(a5Table2);//Date 11/12/2017 comment By Jayshree To remaove the space
//		a5TableCell2.setPaddingLeft(20);//Date 11/12/2017 by Jayshree set the padding to increse the size
		a4Table.addCell(a5TableCell1);
//		a4Table.addCell(a5TableCell2);
		try {
			document.add(a4Table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}




public void loadAll(long id , long companyId) { //int customerId,
	headerCounter=0;
	footerCounter=0;
	
	a4Table = new PdfPTable(1);
	a5Table1 = new PdfPTable(1);
//	if(srCopyNumber != null && !srCopyNumber.equals("")){
//		serviceList = ofy().load().type(Service.class).filter("companyId", companyId).filter("srCopyNumber", srCopyNumber).list();
//	}
	if(serviceList != null && serviceList.size() > 0){
		for(Service sr : serviceList){
		if(sr.getCustomerSignature() != null && !sr.getCustomerSignature().getUrl().equals("")){
			service = serviceList.get(0);
			break;
		}
		if(service == null){
			service = serviceList.get(0);
		}
	}
		for(Service sr :serviceList){
			serIdList.add(sr.getCount());
		}
		}
	else{
		service =ofy().load().type(Service.class).id(id).now();
		serviceList.add(service);
	}
	if(service.getCompanyId()==null)
		cust=ofy().load().type(Customer.class).filter("count",service.getPersonInfo().getCount()).first().now();
	else
		cust=ofy().load().type(Customer.class).filter("count",service.getPersonInfo().getCount()).filter("companyId", service.getCompanyId()).first().now();

	//cust = ofy().load().type(Customer.class).filter("count", service.getPersonInfo().getCount()).filter("companyId", companyId).first().now();
	
	if (service.getCompanyId() == null)
		comp = ofy().load().type(Company.class).first().now();
	else
		comp = ofy().load().type(Company.class).filter("companyId", service.getCompanyId()).first().now();
	
	
	if(service.getCompanyId()!= null){
		contract=ofy().load().type(Contract.class).filter("companyId", service.getCompanyId()).filter("count", service.getContractCount()).first().now();
	}
	
	
	
	
	if(service.getCompanyId()!=null){
		processconfig = ofy().load().type(ProcessConfiguration.class)
				.filter("companyId", service.getCompanyId())
				.filter("processName", "Service")
				.filter("configStatus", true).first().now();
		if(processconfig!=null){
			for(int k= 0 ;k<processconfig.getProcessList().size() ; k++){
				
				if (processconfig.getProcessList().get(k).getProcessType()
						.trim().equalsIgnoreCase("DecreaseTableRow")
						&& processconfig.getProcessList().get(k).isStatus() == true) {
					rowflag = true;
					System.out.println("rowflag:::::");
				}
				
				
				if (processconfig.getProcessList().get(k).getProcessType()
						.trim().equalsIgnoreCase("CompanyLetterHead")
						&& processconfig.getProcessList().get(k).isStatus() == true) {
					upcflag = true;
					
				}
				
				if (processconfig.getProcessList().get(k).getProcessType()
						.trim().equalsIgnoreCase("HideMaterialandUOM")
						&& processconfig.getProcessList().get(k).isStatus() == true) {
					hidecolum = true;
					System.out.println("hidecolum:::::");
					
				}
				
			}
		}
		
	  }	
	
	
	/*
	 * end by Ashwini
	 */
	
	fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	if (service.getCompanyId() != null){
		serProject = ofy().load().type(ServiceProject.class).filter("companyId", service.getCompanyId()).filter("serviceId", this.service.getCount()).filter("count", this.service.getProjectId()).first().now();
	}else{
		serProject = ofy().load().type(ServiceProject.class).filter("serviceId", this.service.getCount()).filter("count", this.service.getProjectId()).first().now();
	}
	if(serIdList != null && serIdList.size() > 0){
		serviceProjectList = ofy().load().type(ServiceProject.class).filter("companyId", service.getCompanyId()).filter("serviceId IN", serIdList).list();
	}
	if(serviceProjectList != null){
		for(ServiceProject proj : serviceProjectList){
			projectMap.put(proj.getserviceId(), proj);
		}
	}
	if(serProject != null){
		projectMap.put(serProject.getserviceId(), serProject);
	}
	
	if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
		System.out.println("Inside branch As company");
		logger.log(Level.SEVERE,"Process active --");
		if(contract !=null && contract.getBranch() != null && contract.getBranch().trim().length() >0){
			
			branchDt = ofy().load().type(Branch.class).filter("companyId",contract.getCompanyId()).filter("buisnessUnitName", contract.getBranch()).first().now();
			
		
			if(branchDt!=null){
				logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());		
				
				if(branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
					
				
				List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
				
				if(paymentDt.get(0).trim().matches("[0-9]+")){
					
					
					
					int payId = Integer.parseInt(paymentDt.get(0).trim());
					
					companyPayment = ofy().load().type(CompanyPayment.class)
							.filter("count", payId)
							.filter("companyId", contract.getCompanyId()).first()
							.now();
					
					
					if(companyPayment != null){
						
						}
					
					}
				}
				
				comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
			}
		}
	}
	
}

public void loadBasicAll(long companyId,int customerId,String branch, Service service) { //int customerId, long companyId,int customerId,String branch
	this.service=service;
//	headerCounter=0;
//	footerCounter=0;
//	
//	a4Table = new PdfPTable(1);
//	a5Table1 = new PdfPTable(1);
	
	
//	if (serviceList != null && serviceList.size() > 0) {
//		for (Service sr : serviceList) {
//			if (sr.getCustomerSignature() != null && !sr.getCustomerSignature().getUrl().equals("")) {
//				service = serviceList.get(0);
//				break;
//			}
//			if (service == null) {
//				service = serviceList.get(0);
//			}
//		}
//		for (Service sr : serviceList) {
//			serIdList.add(sr.getCount());
//		}
//	}
	
	
	
	if(service.getCompanyId()!= null){
		contract=ofy().load().type(Contract.class).filter("companyId", service.getCompanyId()).filter("count", service.getContractCount()).first().now();
	}
	
	
	
	cust = ofy().load().type(Customer.class).filter("count", customerId).filter("companyId", companyId).first().now();
	comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
	branchDt = ofy().load().type(Branch.class).filter("companyId", companyId).filter("buisnessUnitName", branch).first().now();

	List<ProcessConfiguration> processConfigList=ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId).filter("processName", "Service").filter("configStatus", true).list();
		if(processConfigList!=null){
			for(ProcessConfiguration processConf:processConfigList){
				for(ProcessTypeDetails ptDetails:processConf.getProcessList()){
					if(ptDetails.getProcessType().equalsIgnoreCase("DecreaseTableRow")&&ptDetails.isStatus()==true){
						rowflag = true;
						System.out.println("rowflag:::::");
					}
					if(ptDetails.getProcessType().equalsIgnoreCase("CompanyLetterHead")&&ptDetails.isStatus()==true){
						upcflag=true;
					}
					if(ptDetails.getProcessType().equalsIgnoreCase("HideMaterialandUOM")&&ptDetails.isStatus()==true){
						hidecolum=true;
					}
				}
			}
		}
	

	
	if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
		System.out.println("Inside branch As company");
		logger.log(Level.SEVERE,"Process active --");
		if(contract !=null && contract.getBranch() != null && contract.getBranch().trim().length() >0){
			
			branchDt = ofy().load().type(Branch.class).filter("companyId",contract.getCompanyId()).filter("buisnessUnitName", contract.getBranch()).first().now();
			
		
			if(branchDt!=null){
				logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());		
				logger.log(Level.SEVERE,"branch name="+branchDt.getBusinessUnitName());
				if(branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
					
				
				List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
				
				if(paymentDt.get(0).trim().matches("[0-9]+")){
					
					
					
					int payId = Integer.parseInt(paymentDt.get(0).trim());
					
					companyPayment = ofy().load().type(CompanyPayment.class)
							.filter("count", payId)
							.filter("companyId", contract.getCompanyId()).first()
							.now();
					
					
					if(companyPayment != null){
						
						}
					
					}
				}
				
				comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
			}
		}
	}
	
	
	
	
	

	
	
}

public void loadMultipleSRCopies(List<Service> serviceList){
	logger.log(Level.SEVERE,"ServiceList Size--"+serviceList.size());
	logger.log(Level.SEVERE,"Company Id --"+serviceList.get(0).getCompanyId());
	logger.log(Level.SEVERE,"Count  --"+serviceList.get(0).getPersonInfo().getCount());
	logger.log(Level.SEVERE,"Branch --"+serviceList.get(0).getBranch());
	
	loadBasicAll(serviceList.get(0).getCompanyId(),serviceList.get(0).getPersonInfo().getCount(),serviceList.get(0).getBranch(),serviceList.get(0));
	//loadAll(serviceList.get(0).getId(),serviceList.get(0).getCompanyId());//serviceList.get(0).getPersonInfo().getCount()
    int serviceNo=1;
	for(Service service:serviceList){
		if(serviceNo==serviceList.size())
		{
			printFooterFlag=true;			
		}
		headerCounter=0;
		footerCounter=0;
		
		a4Table = new PdfPTable(1);
		a5Table1 = new PdfPTable(1);
		
		this.serviceList=new ArrayList<Service>();
		projectMap=new HashMap<Integer, ServiceProject>();
		this.service=service;
		this.serviceList.add(service);
		if (this.service != null) {
			serProject = ofy().load().type(ServiceProject.class)
					.filter("companyId", service.getCompanyId())
					.filter("contractId", service.getContractCount())
					.filter("serviceId", service.getCount()).first().now();
		}
		
		if(serProject != null){
			projectMap.put(serProject.getserviceId(), serProject);
		}
		
		if(upcflag) {
			createPdf("no");
		}else
		{
			createPdf("plane");
		}
		serviceNo++;
		Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
		try {
			document.add(nextpage);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
	}
}

}
