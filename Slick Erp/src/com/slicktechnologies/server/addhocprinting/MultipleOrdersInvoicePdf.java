package com.slicktechnologies.server.addhocprinting;


import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

public class MultipleOrdersInvoicePdf {

	ArrayList<Service> service;
	// List<SalesLineItem> products;
	List<SalesOrderProductLineItem> salesProd;
	List<ContractCharges> billingTaxesLis;
	List<ContractCharges> billingChargesLis;
	List<BillingDocumentDetails> billingDoc;
//	List<PaymentTerms> payTermsLis;
	List<ArticleType> articletype;
	Customer cust;
	ProcessConfiguration processConfig;
//	List<ProductOtherCharges> contractTaxesLis;
//	List<ProductOtherCharges> contractChargesLis;
//	List<PaymentTerms> payTerms = new ArrayList<PaymentTerms>();
//	Boolean paytermsflag = false;
	SuperProduct sup;
	ArrayList<SuperProduct> stringlis = new ArrayList<SuperProduct>();
	List<CustomerBranchDetails> custbranchlist;
	
//	List<InvoiceCharges> conbillingTaxesLis = new ArrayList<InvoiceCharges>();
//	List<InvoiceCharges> conbillingChargesLis = new ArrayList<InvoiceCharges>();
	
	PdfPTable chargetaxtableToNewPage = new PdfPTable(2);
	PdfPTable netPayableTable = new PdfPTable(2);

	Company comp;
//	Contract con;
	public Document document;
	Invoice invoiceentity;
//	List<PaymentTerms> arrPayTerms;
	String invComment = "";
//	BillingDocument billEntity;

	String invoiceOrderType = "";
	
	private Font font16boldul, font12bold, font8bold, font8, font9bold,
			font12boldul, font10boldul, font12, font16bold, font10, font10bold,
			font14bold;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	double total = 0;

	Phrase chunk;
	int eva = 100;
	int firstBreakPoint = 5;
	int BreakPoint = 5;
	int count = 0;
	float blankLines;
	int flag = 0;
	int flag1 = 0;
//	float[] columnWidths = { 0.5f, 0.9f, 0.9f, 3.5f, 0.7f, 0.5f, 0.7f, 0.5f,0.8f, 1f };
	float[] columnWidths = { 0.5f, 0.9f, 0.9f, 3.5f,3.0f,0.8f, 1f };
	float[] columnWidths12 = { 1.7f, 0.3f, 2.5f, 1.7f, 0.3f, 2.5f };
	Logger logger = Logger.getLogger("NameOfYourLogger");
	boolean upcflag = false;
	boolean disclaimerflag = false;
	PdfPTable remainigInfotable;
	DecimalFormat df = new DecimalFormat("0.00");
	
	ArrayList<Date> startDateList;
	ArrayList<Date> endDateList;
	ArrayList<BillingDocument>billingList=new ArrayList<BillingDocument>();
	ArrayList<Date> serviceDateList;
	
	
	final static String disclaimerText="I/We hereby certify that my/our registration certificate"
			+ " Under the Maharashtra Value Added Tax Act 2002, is in force on the date on which the sale "
			+ "of the goods specified in this Invoice is made by me/us and that the transaction of sales "
			+ "covered by this Invoice has been effected by me/us and it shall be accounted for in the turnover "
			+ "of sales while filling of return and the due tax, if any, payable on the sale has been paid or shall be paid.";

	public MultipleOrdersInvoicePdf() {
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
//		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	}

	public void setInvoice(long count) {
		// Load Invoice
		invoiceentity = ofy().load().type(Invoice.class).id(count).now();

		billingDoc = invoiceentity.getArrayBillingDocument();
		invoiceOrderType = invoiceentity.getTypeOfOrder().trim();
//		arrPayTerms = invoiceentity.getArrPayTerms();
		// Load Customer
		if (invoiceentity.getCompanyId() == null)
			cust = ofy().load().type(Customer.class).filter("count", invoiceentity.getPersonInfo().getCount()).first().now();
		else
			cust = ofy().load().type(Customer.class).filter("count", invoiceentity.getPersonInfo().getCount())
					.filter("companyId", invoiceentity.getCompanyId()).first().now();

		// Load Company
		if (invoiceentity.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", invoiceentity.getCompanyId()).first().now();

//		if (invoiceentity.getCompanyId() != null)
//			con = ofy().load().type(Contract.class).filter("count", invoiceentity.getContractCount())
//					.filter("companyId", invoiceentity.getCompanyId()).first().now();
//		else
//			con = ofy().load().type(Contract.class)
//					.filter("count", invoiceentity.getContractCount()).first().now();

		if (invoiceentity.getCompanyId() == null)
			custbranchlist = ofy().load().type(CustomerBranchDetails.class)
					.filter("cinfo.count",invoiceentity.getPersonInfo().getCount()).list();
		else
			custbranchlist = ofy().load().type(CustomerBranchDetails.class).filter("cinfo.count",invoiceentity.getPersonInfo().getCount())
					.filter("companyId", invoiceentity.getCompanyId()).list();

		/********************* Display Payment Terms Flag *****************************/

//		if (invoiceentity.getCompanyId() != null) {
//			processConfig = ofy().load().type(ProcessConfiguration.class)
//					.filter("companyId", invoiceentity.getCompanyId())
//					.filter("processName", "Invoice")
//					.filter("configStatus", true).first().now();
//			if (processConfig != null) {
//				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
//					if (processConfig.getProcessList().get(k).getProcessType()
//							.trim().equalsIgnoreCase("ServiceInvoicePayTerm")
//							&& processConfig.getProcessList().get(k).isStatus() == true) {
//						paytermsflag = true;
//					}
//				}
//			}
//		}

		/************************************ Letter Head Flag *******************************/

		if (invoiceentity.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", invoiceentity.getCompanyId())
					.filter("processName", "Invoice").filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						upcflag = true;
					}
				}
			}
		}

		/********************************** Disclaimer Flag ******************************************/

		if (invoiceentity.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", invoiceentity.getCompanyId())
					.filter("processName", "Invoice").filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("DisclaimerText")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						disclaimerflag = true;
					}
				}
			}
		}

//		if (invoiceentity.getCompanyId() != null)
//			billEntity = ofy().load().type(BillingDocument.class).filter("companyId", invoiceentity.getCompanyId()).filter("contractCount", invoiceentity.getContractCount())
//					.filter("invoiceCount", invoiceentity.getCount())
//					.filter("typeOfOrder",invoiceentity.getTypeOfOrder().trim()).first().now();
//		else
//			billEntity = ofy().load().type(BillingDocument.class).filter("contractCount", invoiceentity.getContractCount())
//					.filter("invoiceCount", invoiceentity.getCount())
//					.filter("typeOfOrder",invoiceentity.getTypeOfOrder().trim()).first().now();
		
		
		

		salesProd = invoiceentity.getSalesOrderProducts();
		billingTaxesLis = invoiceentity.getBillingTaxes();
		billingChargesLis = invoiceentity.getBillingOtherCharges();
//		payTermsLis = con.getPaymentTermsList();

		if (invoiceentity.getArrayBillingDocument().size() > 1) {
			for (int i = 0; i < invoiceentity.getArrayBillingDocument().size(); i++) {
				BillingDocument bill = new BillingDocument();
				if (invoiceentity.getCompanyId() != null) {
					bill = ofy().load().type(BillingDocument.class)
							.filter("companyId", invoiceentity.getCompanyId())
							.filter("contractCount",invoiceentity.getContractCount())
							.filter("count",invoiceentity.getArrayBillingDocument().get(i).getBillId())
							.filter("typeOfOrder",invoiceentity.getTypeOfOrder().trim()).first().now();
				} else {
					bill = ofy().load().type(BillingDocument.class)
							.filter("contractCount",invoiceentity.getContractCount())
							.filter("count",invoiceentity.getArrayBillingDocument().get(i).getBillId())
							.filter("typeOfOrder",invoiceentity.getTypeOfOrder().trim()).first().now();
				}
			}
		}

//		contractTaxesLis = con.getProductTaxes();
//		contractChargesLis = con.getProductCharges();

		// ************************rohan changes here *******************

//		if (invoiceentity.getArrayBillingDocument().size() != 0) {
//			BillingDocument billDocumentEntity = null;
//			for (int i = 0; i < invoiceentity.getArrayBillingDocument().size(); i++) {
//
//				if (invoiceentity.getCompanyId() != null) {
//					billDocumentEntity = ofy().load().type(BillingDocument.class)
//							.filter("count",invoiceentity.getArrayBillingDocument().get(i).getBillId())
//							.filter("companyId", billEntity.getCompanyId())
//							.filter("contractCount",billEntity.getContractCount())
//							.filter("typeOfOrder",billEntity.getTypeOfOrder().trim()).first().now();
//				} else {
//					billDocumentEntity = ofy().load().type(BillingDocument.class)
//							.filter("count",invoiceentity.getArrayBillingDocument().get(i).getBillId())
//							.filter("contractCount",billEntity.getContractCount())
//							.filter("invoiceCount",billEntity.getInvoiceCount())
//							.filter("typeOfOrder",billEntity.getTypeOfOrder().trim()).first().now();
//				}

				// ////////////////////////////////////////////////////////////////////

//				if (billDocumentEntity != null) {
//
//					InvoiceCharges taxes = new InvoiceCharges();
//					taxes.setChargesList(billDocumentEntity.getBillingTaxes());
//					taxes.setPayPercent(billDocumentEntity.getArrPayTerms().get(0).getPayTermPercent());
//					conbillingTaxesLis.add(taxes);
//
//					InvoiceCharges charges = new InvoiceCharges();
//					charges.setPayPercent(billDocumentEntity.getArrPayTerms().get(0).getPayTermPercent());
//					charges.setOtherChargesList(billDocumentEntity.getBillingOtherCharges());
//					conbillingChargesLis.add(charges);
//				}
//			}

//		}

		// **********************changes ends here ***********************

		articletype = new ArrayList<ArticleType>();
		if (cust.getArticleTypeDetails().size() != 0) {
			articletype.addAll(cust.getArticleTypeDetails());
		}
		if (comp.getArticleTypeDetails().size() != 0) {
			articletype.addAll(comp.getArticleTypeDetails());
		}
//		arrPayTerms = invoiceentity.getArrPayTerms();

//		if (cust != null) {
//			logger.log(Level.SEVERE, "Customer Loaded");
//		} else {
//			logger.log(Level.SEVERE, "Customer Not Loaded");
//		}
//
//		if (con != null) {
//			logger.log(Level.SEVERE, "con Loaded");
//		} else {
//			logger.log(Level.SEVERE, "con Not Loaded");
//		}
//
//		logger.log(Level.SEVERE, "All initilizations over");
//
//		logger.log(Level.SEVERE, "set invoice 1.....");
		try {
			invComment = getInvComment();
			logger.log(Level.SEVERE, "set invoice 2.....");
		} catch (Exception e) {
			logger.log(Level.SEVERE, "set invoice 3.....");
			e.printStackTrace();
		}
		logger.log(Level.SEVERE, "set invoice 4.....");

		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			if (invoiceentity.getCompanyId() != null) {
				sup = ofy().load().type(SuperProduct.class).filter("companyId", invoiceentity.getCompanyId())
						.filter("count",invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()).first().now();

				SuperProduct superprod = new SuperProduct();
				superprod.setCount(sup.getCount());
				superprod.setProductName(sup.getProductName());
				superprod.setComment(sup.getComment() + " "+ sup.getCommentdesc());
				stringlis.add(superprod);
			}

			else {
				sup = ofy().load().type(SuperProduct.class).filter("count",invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()).first().now();
			}
		}
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		
		startDateList=new ArrayList<Date>();
		endDateList=new ArrayList<Date>();
		for(int i=0;i<invoiceentity.getArrayBillingDocument().size();i++){
			System.out.println("CONTTRACT ID :: "+invoiceentity.getArrayBillingDocument().get(i).getOrderId());
			Contract contract=ofy().load().type(Contract.class).filter("companyId", invoiceentity.getCompanyId()).filter("count", invoiceentity.getArrayBillingDocument().get(i).getOrderId()).first().now();
			if(contract!=null){
				System.out.println("CONTRACT NOT NULL");
				System.out.println("START DATE---"+contract.getStartDate());
				System.out.println("END DATE---"+contract.getEndDate());
				startDateList.add(contract.getStartDate());
				endDateList.add(contract.getEndDate());
			}
		}
		
		
		for(int i=0;i<salesProd.size();i++){
			BillingDocument billDocumentEntity = ofy().load().type(BillingDocument.class).filter("count",salesProd.get(i).getBiilingId()).filter("companyId", invoiceentity.getCompanyId()).first().now();
			if(billDocumentEntity!=null){
				billingList.add(billDocumentEntity);
			}
		}
		serviceDateList=new ArrayList<Date>();
		for(int i=0;i<salesProd.size();i++){
			List<Service> serviceList=ofy().load().type(Service.class).filter("contractCount",salesProd.get(i).getOrderId()).filter("companyId", invoiceentity.getCompanyId()).list();
			if(serviceList.size()!=0){
				for(int j=0;j<serviceList.size();j++){
					serviceDateList.add(serviceList.get(j).getServiceDate());
				}
			}
		}
	}

	
	
	
	public Date getStartDate(){
		Date startDate = null;
		Collections.sort(startDateList);
		startDate=startDateList.get(0);
		System.out.println("::::::: CONTRACT START DATE :::::: "+startDate);
		return startDate;
	}
	
	public Date getEndDate(){
		Date endDate = null;
		Collections.sort(endDateList, Collections.reverseOrder());
		endDate=endDateList.get(0);
		System.out.println("::::::: CONTRACT END DATE :::::: "+endDate);
		return endDate;
	}
	
	public double getTotalAmount(){
		double amount=0;
		for(int i=0;i<salesProd.size();i++){
			amount=amount+salesProd.get(i).getBaseBillingAmount();
		}
		return amount;
	}
	
	
	public String getInvComment() throws Exception {
		logger.log(Level.SEVERE, "get invoice 1.....");
		String commnt = "";
		if (invoiceentity != null) {
			logger.log(Level.SEVERE, "get invoice 2.....");
			commnt = invoiceentity.getComment();
			logger.log(Level.SEVERE, "get invoice 3.....");
		}
		logger.log(Level.SEVERE, "get invoice 4.....");
		return commnt;

	}

	public void createPdf(String preprint) {
		try {
			if (upcflag == false && preprint.equals("plane")) {
				createLogo(document, comp);
				createCompanyAddress();
			} else {
				if (preprint.equals("yes")) {
					createBlankforUPC();
				}
				if (preprint.equals("no")) {
					if (comp.getUploadHeader() != null) {
						createCompanyNameAsHeader(document, comp);
					}
					if (comp.getUploadFooter() != null) {
						createCompanyNameAsFooter(document, comp);
					}
					createBlankforUPC();
				}
			}
			createCustInfo1();
			createProductTable();
			termsConditionsInfo123();
			addFooter();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createLogo(Document doc, Company comp) {
		DocumentUpload document = comp.getLogo();
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f, 765f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void createCompanyAddress() throws Exception {
		logger.log(Level.SEVERE, "Creating Company Info");
		Phrase companyName = new Phrase(comp.getBusinessUnitName(), font16bold);
		Paragraph p = new Paragraph();
		p.add(Chunk.NEWLINE);
		p.add(companyName);
		p.setAlignment(Element.ALIGN_CENTER);
		PdfPCell companyNameCell = new PdfPCell();
		companyNameCell.addElement(p);
		companyNameCell.setBorder(0);

		String addressline1 = "";

		if (comp.getAddress().getAddrLine2() != null) {
			addressline1 = comp.getAddress().getAddrLine1() + "  "
					+ comp.getAddress().getAddrLine2();
		} else {
			addressline1 = comp.getAddress().getAddrLine1();
		}
		Phrase addressline = new Phrase(addressline1, font12);
		Paragraph addresspara = new Paragraph();
		addresspara.add(addressline);
		addresspara.setAlignment(Element.ALIGN_CENTER);

		Phrase locality = null;
		if ((!comp.getAddress().getLandmark().equals(""))
				&& (comp.getAddress().getLocality().equals("") == false)) {
			System.out.println("inside both null condition1");
			locality = new Phrase(comp.getAddress().getLandmark() + " , "
					+ comp.getAddress().getLocality() + " , "
					+ comp.getAddress().getCity() + " , "
					+ comp.getAddress().getPin() + " , "
					+ comp.getAddress().getState() + " , "
					+ comp.getAddress().getCountry(), font12);
		} else if ((!comp.getAddress().getLandmark().equals(""))
				&& (comp.getAddress().getLocality().equals("") == true)) {
			System.out.println("inside both null condition 2");
			locality = new Phrase(comp.getAddress().getLandmark() + " , "
					+ comp.getAddress().getCity() + " , "
					+ comp.getAddress().getPin() + " , "
					+ comp.getAddress().getState() + " , "
					+ comp.getAddress().getCountry(), font12);
		}

		else if ((comp.getAddress().getLandmark().equals(""))
				&& (comp.getAddress().getLocality().equals("") == false)) {
			System.out.println("inside both null condition 3");
			locality = new Phrase(comp.getAddress().getLocality() + " , "
					+ comp.getAddress().getCity() + " , "
					+ comp.getAddress().getPin() + " , "
					+ comp.getAddress().getState() + " , "
					+ comp.getAddress().getCountry(), font12);
		} else if ((comp.getAddress().getLandmark().equals(""))
				&& (comp.getAddress().getLocality().equals("") == true)) {
			System.out.println("inside both null condition 4");
			locality = new Phrase(comp.getAddress().getCity() + " , "
					+ comp.getAddress().getPin() + " , "
					+ comp.getAddress().getState() + " , "
					+ comp.getAddress().getCountry(), font12);
		}

		Paragraph localityPragraph = new Paragraph();
		localityPragraph.add(locality);
		localityPragraph.setAlignment(Element.ALIGN_CENTER);

		Phrase branchPhrase = new Phrase("Branch : " + cust.getBranch());
		PdfPCell branchCell = new PdfPCell(branchPhrase);
		branchCell.setBorder(0);
		branchCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		Phrase contactinfo = null;
		if (comp.getLandline() != 0) {
			contactinfo = new Phrase("Mobile: " + comp.getCellNumber1()
					+ "    Phone: " + comp.getLandline() + "    email: "
					+ comp.getEmail().trim(), font10);
		} else {
			contactinfo = new Phrase("Mobile: " + comp.getCellNumber1()
					+ "    email: " + comp.getEmail().trim(), font10);
		}
		Paragraph realmobpara = new Paragraph();
		realmobpara.add(contactinfo);
		realmobpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell addresscell = new PdfPCell();
		addresscell.addElement(addresspara);
		addresscell.setBorder(0);
		PdfPCell localitycell = new PdfPCell();
		localitycell.addElement(localityPragraph);
		localitycell.setBorder(0);
		PdfPCell contactcell = new PdfPCell();
		contactcell.addElement(realmobpara);
		contactcell.setBorder(0);

		PdfPTable companyDetails = new PdfPTable(1);
		companyDetails.setWidthPercentage(100);

		companyDetails.addCell(companyNameCell);
		companyDetails.addCell(addresscell);
		companyDetails.addCell(localitycell);
		companyDetails.addCell(branchCell);
		companyDetails.addCell(contactcell);

		String titlepdf = "";
		if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity
				.getInvoiceType().trim())
				|| invoiceentity.getInvoiceType().trim()
						.equals(AppConstants.CREATEPROFORMAINVOICE))
			titlepdf = "Proforma Invoice";
		else
			titlepdf = "Tax Invoice";

		Phrase titlephrase = new Phrase(titlepdf, font14bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell titlecell = new PdfPCell();
		titlecell.addElement(titlepdfpara);
		titlecell.setBorder(0);

		Phrase blankphrase = new Phrase("", font8);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(titlecell);
		titlepdftable.addCell(blankCell);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);

		PdfPCell comapnyCell = new PdfPCell(companyDetails);
		PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
		parent.addCell(comapnyCell);
		parent.addCell(titlePdfCell);

		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		logger.log(Level.SEVERE, "End of Creating Company Info");
	}

	private void createBlankforUPC() {

		String titlepdf = "";
		if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity
				.getInvoiceType().trim())
				|| invoiceentity.getInvoiceType().trim()
						.equals(AppConstants.CREATEPROFORMAINVOICE))
			titlepdf = "Proforma Invoice";
		else
			titlepdf = "Tax Invoice";

		Phrase titlephrase = new Phrase(titlepdf, font14bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell titlecell = new PdfPCell();
		titlecell.addElement(titlepdfpara);
		titlecell.setBorder(0);

		Phrase blankphrase = new Phrase("", font8);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(titlecell);
		titlepdftable.addCell(blankCell);

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);

		PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
		parent.addCell(titlePdfCell);

		/*
		 * Commented by Ashwini
		 */
//		try {
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(parent);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
		
		/*
		 * Date:30/07/2018
		 * Developer:Ashwini
		 */
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Header","INCREASEUPPERHEADER" , invoiceentity.getCompanyId())){
			 Paragraph blank1 =new Paragraph();
			    blank1.add(Chunk.NEWLINE);
			    
			
			    
			try {
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(parent);
				
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}else{
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
		/*
		 * End by Ashwini
		 */
	}

	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void createCustInfo1() throws Exception {
		logger.log(Level.SEVERE, "Creating customer  Info");

		String tosir = null;
		if (cust.isCompany() == true) {

			tosir = "To, M/S";
		} else {
			tosir = "To, ";
		}

		Phrase clientname = null;

		if (cust.isCompany() == true) {
			clientname = new Phrase(tosir + "  " + cust.getCompanyName().trim(), font10bold);
		} else {
			clientname = new Phrase(tosir + "  " + cust.getFullname().trim(),font10bold);
		}
		PdfPCell custnamecell = new PdfPCell();
		custnamecell.addElement(clientname);
		custnamecell.setBorder(0);

		Phrase custaddress1 = new Phrase("                   "+ cust.getAdress().getAddrLine1().trim(), font8);

		Phrase custaddress2 = null;
		if (!cust.getAdress().getAddrLine2().equals("")|| cust.getAdress().getAddrLine2() != null) {
			custaddress2 = new Phrase("                   "+ cust.getAdress().getAddrLine2().trim(), font8);
		} else {
			custaddress2 = new Phrase("", font10);
		}
		Phrase landmark = null;

		if (!cust.getAdress().getLandmark().equals("")) {
			landmark = new Phrase("                   "+ cust.getAdress().getLandmark(), font8);
		} else {
			landmark = new Phrase("", font10);
		}
		Phrase locality = null;
		if (!cust.getAdress().getLocality().equals("")) {
			locality = new Phrase("                   "+ cust.getAdress().getLocality(), font8);
		} else {
			locality = new Phrase("", font8);
		}
		Phrase city = new Phrase("                   "+ cust.getAdress().getCity().trim() + " - "
				+ cust.getAdress().getPin(), font8);

		Phrase contactcust = new Phrase("mobile: " + cust.getCellNumber1()+ "    Email:  " + cust.getEmail().trim() + " ", font8);
		
		PdfPCell custaddr1cell = new PdfPCell();
		custaddr1cell.addElement(custaddress1);
		custaddr1cell.setBorder(0);

		PdfPCell custaddr2cell = null;
		if (custaddress2 != null) {
			custaddr2cell = new PdfPCell();
			custaddr2cell.addElement(custaddress2);
			custaddr2cell.setBorder(0);
		}

		PdfPCell custlocalitycell = new PdfPCell();
		custlocalitycell.addElement(locality);
		custlocalitycell.setBorder(0);

		PdfPCell lanmarkcell = new PdfPCell();
		lanmarkcell.addElement(landmark);
		lanmarkcell.setBorder(0);

		PdfPCell citycell = new PdfPCell();
		citycell.addElement(city);
		citycell.setBorder(0);

		PdfPCell custcontactcell = new PdfPCell();
		custcontactcell.addElement(contactcust);
		custcontactcell.setBorder(0);
		
		PdfPTable custtable = new PdfPTable(1);
		custtable.setWidthPercentage(100);
		custtable.addCell(custnamecell);
		custtable.addCell(custaddr1cell);
		if (!cust.getAdress().getAddrLine2().equals("")) {
			custtable.addCell(custaddr2cell);
		}
		if (!cust.getAdress().getLandmark().equals("")) {
			custtable.addCell(lanmarkcell);
		}
		if (!cust.getAdress().getLocality().equals("")) {
			custtable.addCell(custlocalitycell);
		}
		custtable.addCell(citycell);
		custtable.addCell(custcontactcell);

		Phrase billnotitle = new Phrase("Bill No", font8);
		Phrase realbillno = new Phrase(invoiceentity.getCount() + "", font8);
		
		Phrase datetitle = new Phrase("Date", font8);
		Phrase realdate = new Phrase(fmt.format(invoiceentity.getInvoiceDate())+ "", font8);

//		Phrase billnotitle1 = new Phrase("Contract Id", font8);
//		Phrase realbillno1 = new Phrase(invoiceentity.getContractCount() + "",font8);
//		
//		Phrase datetitle1 = new Phrase("Date", font8);
//		Phrase realdate1 = new Phrase(fmt.format(invoiceentity.getCreationDate()) + "", font8);

		Phrase stdate = new Phrase("Start Date", font8);
//		Phrase sdate = new Phrase(fmt.format(con.getStartDate()) + "", font8);
		Phrase sdate=null;
		if(invoiceentity.isMultipleOrderBilling()==true)
		{
			if(invoiceentity.getConStartDur()!=null)
			{
				sdate= new Phrase(fmt.format(invoiceentity.getConStartDur()) + "", font8);
			}
			else
			{
				sdate= new Phrase(fmt.format(getStartDate()) + "", font8);
			}
		}
//		else
//		{
//			sdate= new Phrase(fmt.format(getStartDate()) + "", font8);
//		}
//		
		
		
		
		Phrase enddate = new Phrase("End Date", font8);
//		Phrase edate = new Phrase(fmt.format(con.getEndDate()) + "", font8);
		Phrase edate=null;
		if(invoiceentity.isMultipleOrderBilling()==true)
		{
			if(invoiceentity.getConEndDur()!=null)
			{
				edate= new Phrase(fmt.format(invoiceentity.getConEndDur()) + "", font8);
			}
			else
			{
				edate = new Phrase(fmt.format(getEndDate()) + "", font8);
			}
			 
		}
//		else
//		{
//			 edate = new Phrase(fmt.format(getEndDate()) + "", font8);
//		}
//		Phrase refno = new Phrase("Ref No", font8);
//
//		Phrase ref = null;
//		String refNO = "";
//
//		if (!con.getRefNo().equals("")) {
//			refNO = con.getRefNo();
//			ref = new Phrase(refNO + "", font8);
//		} else {
//			ref = new Phrase("", font8);
//		}

		/**
		 * InvoiceGroup Add 29/6/15
		 */

		Phrase invoicegroupphrase1 = null;
		Phrase invoicegroupphrase3 = null;
		if (!invoiceentity.getInvoiceGroup().equals("")) {
			invoicegroupphrase1 = new Phrase("Invoice Group", font8);
			invoicegroupphrase3 = new Phrase(invoiceentity.getInvoiceGroup(),font8);
		} else {
			invoicegroupphrase1 = new Phrase("", font8);
			invoicegroupphrase3 = new Phrase("", font8);
		}

		PdfPCell invoicegroupcell1 = new PdfPCell();
		invoicegroupcell1.setBorder(0);
		invoicegroupcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		invoicegroupcell1.addElement(invoicegroupphrase1);
		PdfPCell invoicegroupcell3 = new PdfPCell();
		invoicegroupcell3.setBorder(0);
		invoicegroupcell3.setHorizontalAlignment(Element.ALIGN_CENTER);
		invoicegroupcell3.addElement(invoicegroupphrase3);

		Phrase columnPhrase = new Phrase(":", font8);
		PdfPCell columnCell = new PdfPCell();
		columnCell.setBorder(0);
		columnCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		columnCell.addElement(columnPhrase);

		PdfPCell stdatecell = new PdfPCell();
		stdatecell.addElement(stdate);
		stdatecell.setBorder(0);

		PdfPCell sdatecell = new PdfPCell();
		sdatecell.addElement(sdate);
		sdatecell.setBorder(0);

		PdfPCell enddatecell = new PdfPCell();
		enddatecell.addElement(enddate);
		enddatecell.setBorder(0);

		PdfPCell edatecell = new PdfPCell();
		edatecell.addElement(edate);
		edatecell.setBorder(0);

//		PdfPCell refnocell = new PdfPCell();
//		refnocell.addElement(refno);
//		refnocell.setBorder(0);
//
//		PdfPCell refcell = new PdfPCell();
//		refcell.addElement(ref);
//		refcell.setBorder(0);

		PdfPCell billnotitlecell = new PdfPCell();
		billnotitlecell.addElement(billnotitle);
		billnotitlecell.setBorder(0);

		PdfPCell billnocell = new PdfPCell();
		billnocell.addElement(realbillno);
		billnocell.setBorder(0);

		PdfPCell datetitlecell = new PdfPCell();
		datetitlecell.addElement(datetitle);
		datetitlecell.setBorder(0);

		PdfPCell datecell = new PdfPCell();
		datecell.addElement(realdate);
		datecell.setBorder(0);

//		PdfPCell ponotitlecell = new PdfPCell();
//		ponotitlecell.addElement(billnotitle1);
//		ponotitlecell.setBorder(0);
//
//		PdfPCell ponocell = new PdfPCell();
//		ponocell.addElement(realbillno1);
//		ponocell.setBorder(0);
//
//		PdfPCell podatetitlecell = new PdfPCell();
//		podatetitlecell.addElement(datetitle1);
//		podatetitlecell.setBorder(0);
//
//		PdfPCell podatecell = new PdfPCell();
//		podatecell.addElement(realdate1);
//		podatecell.setBorder(0);

		PdfPTable invoiceinfotable = new PdfPTable(6);
		invoiceinfotable.setWidths(columnWidths12);
		invoiceinfotable.setWidthPercentage(100);
		invoiceinfotable.addCell(billnotitlecell);
		invoiceinfotable.addCell(columnCell);
		invoiceinfotable.addCell(billnocell);

		invoiceinfotable.addCell(datetitlecell);
		invoiceinfotable.addCell(columnCell);
		invoiceinfotable.addCell(datecell);

//		invoiceinfotable.addCell(ponotitlecell);
//		invoiceinfotable.addCell(columnCell);
//		invoiceinfotable.addCell(ponocell);
//		invoiceinfotable.addCell(podatetitlecell);
//		invoiceinfotable.addCell(columnCell);
//		invoiceinfotable.addCell(podatecell);

		invoiceinfotable.addCell(stdatecell);
		invoiceinfotable.addCell(columnCell);
		invoiceinfotable.addCell(sdatecell);
		invoiceinfotable.addCell(enddatecell);
		invoiceinfotable.addCell(columnCell);
		invoiceinfotable.addCell(edatecell);
//		if (!con.getRefNo().equals("")) {
//			invoiceinfotable.addCell(refnocell);
//			invoiceinfotable.addCell(columnCell);
//			invoiceinfotable.addCell(refcell);
//		}

		/**
		 * add on table invoicegroup 29/6/15
		 */
		if (!invoiceentity.getInvoiceGroup().equals("")) {
			invoiceinfotable.addCell(invoicegroupcell1);
			invoiceinfotable.addCell(columnCell);
			invoiceinfotable.addCell(invoicegroupcell3);
			PdfPCell blankcell = new PdfPCell();
			blankcell.setBorder(0);
		}

		Phrase titleBranch = new Phrase("Branch", font8);
		PdfPCell titlebranchcell = new PdfPCell();
		titlebranchcell.addElement(titleBranch);
		titlebranchcell.setBorder(0);

		Phrase branchCustPhrase = new Phrase(cust.getBranch(), font8);
		PdfPCell branchCustCell = new PdfPCell();
		branchCustCell.addElement(branchCustPhrase);
		branchCustCell.setBorder(0);
		invoiceinfotable.addCell(titlebranchcell);
		invoiceinfotable.addCell(columnCell);
		invoiceinfotable.addCell(branchCustCell);

		PdfPTable pfifparenttable = new PdfPTable(1);
		pfifparenttable.setWidthPercentage(100f);

		PdfPCell invoiccell = new PdfPCell();
		invoiccell.addElement(invoiceinfotable);
		invoiccell.setBorder(0);

		pfifparenttable.addCell(invoiccell);

		if (!cust.getSecondaryAdress().getAddrLine1().equals("")
				&& custbranchlist.size() == 0) {

			Phrase addtitle = new Phrase("Service Address ", font8bold);

			Phrase serviceaddress1 = new Phrase("     "
					+ cust.getSecondaryAdress().getAddrLine1().trim(), font8);

			Phrase serviceaddress2 = null;
			if (!cust.getSecondaryAdress().getAddrLine2().equals("")
					|| cust.getSecondaryAdress().getAddrLine2() != null) {
				serviceaddress2 = new Phrase("     "
						+ cust.getSecondaryAdress().getAddrLine2().trim(),
						font8);
			} else {
				serviceaddress2 = new Phrase("", font10);
			}
			Phrase sevicelandmark = null;

			if (!cust.getSecondaryAdress().getLandmark().equals("")) {
				sevicelandmark = new Phrase("     "
						+ cust.getSecondaryAdress().getLandmark(), font8);
			} else {
				sevicelandmark = new Phrase("", font10);
			}
			Phrase servicelocality = null;
			if (!cust.getSecondaryAdress().getLocality().equals("")) {
				servicelocality = new Phrase("     "
						+ cust.getSecondaryAdress().getLocality(), font8);
			} else {
				servicelocality = new Phrase("", font8);
			}
			Phrase servicecity = new Phrase("     "
					+ cust.getSecondaryAdress().getCity().trim() + " - "
					+ cust.getSecondaryAdress().getPin(), font8);

			PdfPCell addtiltlecell = new PdfPCell();
			addtiltlecell.addElement(addtitle);
			addtiltlecell.setBorder(0);

			PdfPCell serviceaddr1cell = new PdfPCell();
			serviceaddr1cell.addElement(serviceaddress1);
			serviceaddr1cell.setBorder(0);

			PdfPCell serviceadde2cell = new PdfPCell();
			serviceadde2cell.addElement(serviceaddress2);
			serviceadde2cell.setBorder(0);

			PdfPCell servicelocalitycell = new PdfPCell();
			servicelocalitycell.addElement(servicelocality);
			servicelocalitycell.setBorder(0);

			PdfPCell servicelanmarkcell = new PdfPCell();
			servicelanmarkcell.addElement(sevicelandmark);
			servicelanmarkcell.setBorder(0);

			PdfPCell servicecitycell = new PdfPCell();
			servicecitycell.addElement(servicecity);
			servicecitycell.setBorder(0);

			PdfPTable servicetable = new PdfPTable(1);
			servicetable.setWidthPercentage(100f);
			servicetable.addCell(addtiltlecell);
			servicetable.addCell(serviceaddr1cell);
			servicetable.addCell(serviceadde2cell);
			servicetable.addCell(servicelanmarkcell);
			servicetable.addCell(servicelocalitycell);
			servicetable.addCell(servicecitycell);

			PdfPCell sevicecell = new PdfPCell();
			sevicecell.addElement(servicetable);
			sevicecell.setBorder(0);

			pfifparenttable.addCell(servicetable);
		}

		PdfPTable parenttable = new PdfPTable(2);
		parenttable.setWidthPercentage(100);
		try {
			parenttable.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		PdfPCell custinfocell = new PdfPCell();
		PdfPCell invoiceinfocell = new PdfPCell();

		custinfocell.addElement(custtable);
		invoiceinfocell.addElement(pfifparenttable);
		parenttable.addCell(custinfocell);
		parenttable.addCell(invoiceinfocell);
		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	

	public void createProductTable() {
		logger.log(Level.SEVERE, "create product Info");
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);

		Phrase productdetails = new Phrase("Product Details", font1);
		Paragraph para = new Paragraph();
		para.add(Chunk.NEWLINE);
		para.add(Chunk.NEWLINE);
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_CENTER);
		para.add(Chunk.NEWLINE);

		PdfPTable table = new PdfPTable(7);
		table.setWidthPercentage(100);

		Phrase category = new Phrase("SR. NO. ", font1);
		Phrase orderid = new Phrase("ORDER ID", font1);
		Phrase billingid = new Phrase("BILLING ID", font1);
		Phrase productname = new Phrase("ITEM DETAILS", font1);
		Phrase paymentterms = new Phrase("PAYMENT TERMS", font1);
		
//		Phrase qty = new Phrase("QTY", font1);
//		Phrase unit = new Phrase("UNIT", font1);
//		Phrase rate = new Phrase("RATE", font1);
//		Phrase percDisc = new Phrase("DISC %", font1);


		Phrase servicetax = null;
		Phrase servicetax1 = null;
		int flag = 0;
		int vat = 0;
		int st = 0;
		for (int i = 0; i < salesProd.size(); i++) {
			if ((salesProd.get(i).getVatTax().getPercentage() > 0)&& (salesProd.get(i).getServiceTax().getPercentage() == 0)) {
				servicetax = new Phrase("VAT %", font1);
				vat = vat + 1;
			} else if ((salesProd.get(i).getServiceTax().getPercentage() > 0)&& (salesProd.get(i).getVatTax().getPercentage() == 0)) {
				servicetax = new Phrase("ST %", font1);
				st = st + 1;
			} else if ((salesProd.get(i).getVatTax().getPercentage() > 0)&& (salesProd.get(i).getServiceTax().getPercentage() > 0)) {
				servicetax1 = new Phrase("VAT / ST %", font1);
				flag = flag + 1;
			} else {
				servicetax = new Phrase("TAX %", font1);
			}
		}

		Phrase total = new Phrase("AMOUNT", font1);

		PdfPCell cellorderid = new PdfPCell(orderid);
		cellorderid.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellbillingid = new PdfPCell(billingid);
		cellbillingid.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellcategory = new PdfPCell(category);
		cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellproductname = new PdfPCell(productname);
		cellproductname.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell cellpaymentterms = new PdfPCell(paymentterms);
		cellpaymentterms.setHorizontalAlignment(Element.ALIGN_CENTER);
		
//		PdfPCell cellqty = new PdfPCell(qty);
//		cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);
//		PdfPCell cellunit = new PdfPCell(unit);
//		cellunit.setHorizontalAlignment(Element.ALIGN_CENTER);
//		PdfPCell cellrate = new PdfPCell(rate);
//		cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
//		PdfPCell percDisccell = new PdfPCell(percDisc);
//		percDisccell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellservicetax1 = null;
		PdfPCell cellservicetax = null;
		PdfPCell cellservicetax2 = null;
		Phrase servicetax2 = null;

		if (flag > 0) {
			cellservicetax1 = new PdfPCell(servicetax1);
			cellservicetax1.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else if (vat > 0 && st > 0) {
			System.out.println("in side condition");
			servicetax2 = new Phrase("VAT / ST", font1);
			cellservicetax2 = new PdfPCell(servicetax2);
			cellservicetax2.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else {
			cellservicetax = new PdfPCell(servicetax);
			cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);

		}

		PdfPCell celltotal = new PdfPCell(total);
		celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(cellcategory);
		table.addCell(cellorderid);
		table.addCell(cellbillingid);
		table.addCell(cellproductname);
		table.addCell(cellpaymentterms);
		
//		table.addCell(cellqty);
//		table.addCell(cellunit);
//		table.addCell(cellrate);
//		table.addCell(percDisccell);

		if (flag > 0) {
			table.addCell(cellservicetax1);
		} else if (vat > 0 && st > 0) {
			table.addCell(cellservicetax2);
		} else {
			table.addCell(cellservicetax);
		}
		table.addCell(celltotal);

		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		if (this.salesProd.size() <= firstBreakPoint) {
			int size = firstBreakPoint - this.salesProd.size();
			blankLines = size * (70 / 5);
			System.out.println("blankLines size =" + blankLines);
			System.out.println("blankLines size =" + blankLines);
		} else {
			blankLines = 10f;
		}
		table.setSpacingAfter(blankLines);
		Phrase rephrse = null;

		for (int i = 0; i < this.salesProd.size(); i++) {
			Phrase chunk = new Phrase((i + 1) + "", font8);
			PdfPCell pdfcategcell = new PdfPCell(chunk);
			pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(salesProd.get(i).getOrderId() + "", font8);
			PdfPCell pdforderidcell = new PdfPCell(chunk);

			chunk = new Phrase(salesProd.get(i).getBiilingId() + "", font8);
			PdfPCell pdfbillingidcell = new PdfPCell(chunk);

			chunk = new Phrase(salesProd.get(i).getProdName(), font8);
			PdfPCell pdfnamecell = new PdfPCell(chunk);
			
			PdfPCell pdfpaymenttermscell=null;
			for(int j=0;j<billingList.size();j++){
				if(billingList.get(j).getCount()==salesProd.get(i).getBiilingId()){
					chunk=new Phrase(billingList.get(j).getArrPayTerms().get(0).getPayTermDays() +" Days / "+billingList.get(j).getArrPayTerms().get(0).getPayTermPercent()+" % / "+billingList.get(j).getArrPayTerms().get(0).getPayTermComment(),font8);
				}
			}
			pdfpaymenttermscell =new PdfPCell(chunk);
//			chunk = new Phrase(salesProd.get(i).getQuantity() + "", font8);
//			PdfPCell pdfqtycell = new PdfPCell(chunk);
//			pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//			chunk = new Phrase(salesProd.get(i).getUnitOfMeasurement(), font8);
//			PdfPCell pdfunitcell = new PdfPCell(chunk);
//			pdfunitcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//			chunk = new Phrase(df.format(salesProd.get(i).getPrice()), font8);
//			PdfPCell pdfspricecell = new PdfPCell(chunk);
//			pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//
//			if (salesProd.get(i).getProdPercDiscount() != 0)
//				chunk = new Phrase(salesProd.get(i).getProdPercDiscount() + "",font8);
//			else
//				chunk = new Phrase("0" + "", font8);
//			PdfPCell pdfperdiscount = new PdfPCell(chunk);
//			pdfperdiscount.setHorizontalAlignment(Element.ALIGN_RIGHT);

			PdfPCell pdfservice = null;
			Phrase vatno = null;
			Phrase stno = null;
			if ((salesProd.get(i).getVatTax().getPercentage() == 0)&& (salesProd.get(i).getServiceTax().getPercentage() > 0)) {

				if (salesProd.get(i).getServiceTax().getPercentage() != 0) {
					if (st == salesProd.size()) {
						chunk = new Phrase(salesProd.get(i).getServiceTax().getPercentage()+ "", font8);
					} else {
						stno = new Phrase(salesProd.get(i).getVatTax().getPercentage()+ ""+ " / "+ salesProd.get(i).getServiceTax().getPercentage() + "", font8);
					}
				} 
				else
					chunk = new Phrase("N.A" + "", font8);
				
				if (st == salesProd.size()) {
					pdfservice = new PdfPCell(chunk);
				} else {
					pdfservice = new PdfPCell(stno);
				}
				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
				
			} else if ((salesProd.get(i).getServiceTax().getPercentage() == 0)&& (salesProd.get(i).getVatTax().getPercentage() > 0)) {

				if (salesProd.get(i).getVatTax() != null) {
					if (vat == salesProd.size()) {
						chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+ "", font8);
					} else {
						vatno = new Phrase(salesProd.get(i).getVatTax().getPercentage()+ ""+ " / "+ salesProd.get(i).getServiceTax().getPercentage(), font8);
					}
				} else {
					chunk = new Phrase("N.A" + "", font8);
				}
				if (vat == salesProd.size()) {
					pdfservice = new PdfPCell(chunk);
				} else {
					pdfservice = new PdfPCell(vatno);
				}
				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
				
			} else if ((salesProd.get(i).getServiceTax().getPercentage() > 0)&& (salesProd.get(i).getVatTax().getPercentage() > 0)) {
				if ((salesProd.get(i).getVatTax() != null)&& (salesProd.get(i).getServiceTax() != null))
					chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+ ""+ " / "+ ""+ salesProd.get(i).getServiceTax().getPercentage(),font8);
				else
					chunk = new Phrase("N.A" + "", font8);
				
				pdfservice = new PdfPCell(chunk);
				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);

			} else {
				chunk = new Phrase("N.A" + "", font8);
				pdfservice = new PdfPCell(chunk);
				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			}

			chunk = new Phrase(df.format(salesProd.get(i).getBaseBillingAmount()),font8);
			PdfPCell pdftotalproduct = new PdfPCell(chunk);
			pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);

			table.addCell(pdfcategcell);
			table.addCell(pdforderidcell);
			table.addCell(pdfbillingidcell);
			table.addCell(pdfnamecell);
			table.addCell(pdfpaymenttermscell);
//			table.addCell(pdfqtycell);
//			table.addCell(pdfunitcell);
//			table.addCell(pdfspricecell);
//			table.addCell(pdfperdiscount);
			table.addCell(pdfservice);
			table.addCell(pdftotalproduct);
			try {
				table.setWidths(columnWidths);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			count = i;

			if (count == this.salesProd.size() || count == firstBreakPoint) {
				rephrse = new Phrase("Refer Annexure 1 for additional products ", font8);
				flag = firstBreakPoint;
				break;
			}

			System.out.println("flag value" + flag);
			if (firstBreakPoint == flag) {

//				 termsConditionsInfo();
				 addFooter();

			}

		}

		PdfPTable parentTableProd = new PdfPTable(1);
		parentTableProd.setWidthPercentage(100);

		PdfPCell prodtablecell = new PdfPCell();
		prodtablecell.addElement(table);

		prodtablecell.addElement(rephrse);
		parentTableProd.addCell(prodtablecell);

		try {
			document.add(parentTableProd);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		logger.log(Level.SEVERE, "ensds product info Info");
	}

	private void termsConditionsInfo123() {

		// SERVICE DATE TABLE
		Collections.sort(serviceDateList);
				
		PdfPTable serDtTbl = new PdfPTable(5);
		serDtTbl.setWidthPercentage(100f);
		
		
		Phrase serDate;
		PdfPCell serDateCell1=null;
		PdfPCell serDateCell2=null;
		PdfPCell serDateCell3=null;
		PdfPCell serDateCell4=null;
		PdfPCell serDateCell5=null;
		
		for(int i=0;i<serviceDateList.size();i++){
			
			serDateCell1=new PdfPCell();
			if(i<serviceDateList.size()){
				System.out.println("@@@@@ Counter ::: "+i);
			serDate=new Phrase(fmt.format(serviceDateList.get(i)),font8);
//			serDateCell1=new PdfPCell(serDate);
			serDateCell1.addElement(serDate);
			
			i++;
			}
			serDateCell1.setBorder(0);
			
			serDateCell2=new PdfPCell();
			if(i<serviceDateList.size()){
				System.out.println("@@@@@ Counter ::: "+i);
			serDate=new Phrase(fmt.format(serviceDateList.get(i)),font8);
//			serDateCell2=new PdfPCell(serDate);
			serDateCell2.addElement(serDate);
			
			i++;
			}
			serDateCell2.setBorder(0);
			
			serDateCell3=new PdfPCell();
			if(i<serviceDateList.size()){
				System.out.println("@@@@@ Counter ::: "+i);
			serDate=new Phrase(fmt.format(serviceDateList.get(i)),font8);
//			serDateCell3=new PdfPCell(serDate);
			serDateCell3.addElement(serDate);
			
			i++;
			}
			serDateCell3.setBorder(0);
			
			serDateCell4=new PdfPCell();
			if(i<serviceDateList.size()){
				System.out.println("@@@@@ Counter ::: "+i);
			serDate=new Phrase(fmt.format(serviceDateList.get(i)),font8);
//			serDateCell4=new PdfPCell(serDate);
			serDateCell4.addElement(serDate);
			
			i++;
			}
			serDateCell4.setBorder(0);
			
			serDateCell5=new PdfPCell();
			if(i<serviceDateList.size()){
				System.out.println("@@@@@ Counter ::: "+i);
			serDate=new Phrase(fmt.format(serviceDateList.get(i)),font8);
//			serDateCell5=new PdfPCell(serDate);
			serDateCell5.addElement(serDate);
			
//			i++;
			}
			serDateCell5.setBorder(0);
			
			serDtTbl.addCell(serDateCell1);
			serDtTbl.addCell(serDateCell2);
			serDtTbl.addCell(serDateCell3);
			serDtTbl.addCell(serDateCell4);
			serDtTbl.addCell(serDateCell5);
			
		}
		
		
		
		String date="Service Date : ";
//		for(int i=0;i<serviceDateList.size();i++){
//			if(i==serviceDateList.size()-1){
//				date=date+fmt.format(serviceDateList.get(i))+".";
//			}else{
//				date=date+fmt.format(serviceDateList.get(i))+", ";
//			}
//		}
		
		System.out.println(" @@@@@@@@@@@@@@@@@@ SERVICE DATE LIST SIZE ::: "+serviceDateList.size());
		Phrase serviceDateTitle = new Phrase("Special Notes: ", font10);
		PdfPCell serviceDateTitleCell=new PdfPCell();
		serviceDateTitleCell.addElement(serviceDateTitle);
		serviceDateTitleCell.setBorder(0);
		
		Phrase serviceDate = new Phrase(date, font8);
		PdfPCell serviceDateCell=new PdfPCell();
		serviceDateCell.addElement(serviceDate);
		serviceDateCell.setBorder(0);
		
		PdfPCell serviceCell=new PdfPCell();
		serviceCell.addElement(serDtTbl);
		serviceCell.setBorder(0);
		
		PdfPTable serviceDateTable = new PdfPTable(1);
		serviceDateTable.setWidthPercentage(100f);
		serviceDateTable.addCell(serviceDateTitleCell);
		serviceDateTable.addCell(serviceDateCell);
		serviceDateTable.addCell(serviceCell);
		
		
		
		// TAX CALCULATION TABLE
		
		double grandTotalAmount=0;
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
		
		Phrase blankval = new Phrase(" ", font8);
		PdfPCell blankCell=new PdfPCell();
		blankCell.addElement(blankval);
		
		PdfPTable chargetaxtable = new PdfPTable(2);
		try {
			chargetaxtable.setWidths(new float[] { 70, 30 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		Phrase totalAmtPhrase = new Phrase("Total Amount   " + "", font1);
		PdfPCell totalAmtCell = new PdfPCell(totalAmtPhrase);
		totalAmtCell.setBorder(0);
		double totalAmt = 0;
		totalAmt = getTotalAmount();
		grandTotalAmount=grandTotalAmount+totalAmt;
		Phrase realtotalAmt = new Phrase(df.format(totalAmt), font1);
		PdfPCell realtotalAmtCell = new PdfPCell(realtotalAmt);
		realtotalAmtCell.setBorder(0);
		realtotalAmtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		chargetaxtable.addCell(totalAmtCell);
		chargetaxtable.addCell(realtotalAmtCell);
		
		List<String> myList1 = new ArrayList<>();
		List<String> myList2 = new ArrayList<>();
		List<String> myList3 = new ArrayList<>();
		List<String> myList4 = new ArrayList<>();
			 
		for (int i = 0; i < this.invoiceentity.getBillingTaxes().size(); i++) {

			if (invoiceentity.getBillingTaxes().get(i).getTaxChargePercent()!= 0) {

				if (invoiceentity.getBillingTaxes().get(i).getTaxChargeName().equals("VAT")|| invoiceentity.getBillingTaxes().get(i).getTaxChargeName().equals("CST")) {

					String str = invoiceentity.getBillingTaxes().get(i).getTaxChargeName().trim()
							+ " @ "+ invoiceentity.getBillingTaxes().get(i).getTaxChargePercent();
					double taxAmt1 = invoiceentity.getBillingTaxes().get(i).getTaxChargePercent()
							* invoiceentity.getBillingTaxes().get(i).getTaxChargeAssesVal()/ 100;

					myList1.add(str);
					myList2.add(df.format(taxAmt1));
					grandTotalAmount=grandTotalAmount+taxAmt1;

				}

				if (invoiceentity.getBillingTaxes().get(i).getTaxChargeName().equals("Service Tax")) {

					String str = invoiceentity.getBillingTaxes().get(i).getTaxChargeName().trim()
							+ " @ "+ invoiceentity.getBillingTaxes().get(i).getTaxChargePercent();
					double taxAmt1 = invoiceentity.getBillingTaxes().get(i).getTaxChargePercent()
							* invoiceentity.getBillingTaxes().get(i).getTaxChargeAssesVal()/ 100;
					String ss = " ";
					myList3.add(str);
					myList4.add(df.format(taxAmt1));
					myList4.add(ss);
					grandTotalAmount=grandTotalAmount+taxAmt1;
				}
			}
		}
			 
		PdfPCell pdfservicecell = null;

		PdfPTable other1table = new PdfPTable(1);
		other1table.setWidthPercentage(100);
		for (int j = 0; j < myList1.size(); j++) {
			chunk = new Phrase(myList1.get(j), font8);
			pdfservicecell = new PdfPCell(chunk);
			pdfservicecell.setBorder(0);
			pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			other1table.addCell(pdfservicecell);
		}

		PdfPCell pdfservicecell1 = null;
		PdfPTable other2table = new PdfPTable(1);
		other2table.setWidthPercentage(100);
		for (int j = 0; j < myList2.size(); j++) {
			chunk = new Phrase(myList2.get(j), font8);
			pdfservicecell1 = new PdfPCell(chunk);
			pdfservicecell1.setBorder(0);
			pdfservicecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			other2table.addCell(pdfservicecell1);

		}

		PdfPCell chargescell = null;
		for (int j = 0; j < myList3.size(); j++) {
			chunk = new Phrase(myList3.get(j), font8);
			chargescell = new PdfPCell(chunk);
			chargescell.setBorder(0);
			chargescell.setHorizontalAlignment(Element.ALIGN_LEFT);
			other1table.addCell(chargescell);
		}

		PdfPCell chargescell1 = null;
		for (int j = 0; j < myList4.size(); j++) {
			chunk = new Phrase(myList4.get(j), font8);
			chargescell1 = new PdfPCell(chunk);
			chargescell1.setBorder(0);
			chargescell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			other2table.addCell(chargescell1);

		}

		PdfPCell othercell = new PdfPCell();
		othercell.addElement(other1table);
		othercell.setBorder(0);
		chargetaxtable.addCell(othercell);

		PdfPCell othercell1 = new PdfPCell();
		othercell1.addElement(other2table);
		othercell1.setBorder(0);
		chargetaxtable.addCell(othercell1);
				
				
		      
		for(int i=0;i<this.invoiceentity.getBillingOtherCharges().size();i++)
		{
			
			Phrase chunk = null;
			Phrase chunk1 = new Phrase("", font8);
			double chargeAmt = 0;
			PdfPCell pdfchargeamtcell = null;
			PdfPCell pdfchargecell = null;
			if (invoiceentity.getBillingOtherCharges().get(i).getTaxChargePercent() != 0) {
				chunk = new Phrase(invoiceentity.getBillingOtherCharges().get(i).getOrderId()+"   "+ invoiceentity.getBillingOtherCharges().get(i).getTaxChargeName()+ " @ " + invoiceentity.getBillingOtherCharges().get(i).getTaxChargePercent(),font1);
				pdfchargecell = new PdfPCell(chunk);
				pdfchargecell.setBorder(0);
				pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				chargeAmt = invoiceentity.getBillingOtherCharges().get(i).getTaxChargePercent()* invoiceentity.getBillingOtherCharges().get(i).getTaxChargeAssesVal() / 100;
				chargeAmt = invoiceentity.getBillingOtherCharges().get(i).getPayableAmt();
				chunk = new Phrase(df.format(chargeAmt), font8);
				pdfchargeamtcell = new PdfPCell(chunk);
				pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				pdfchargeamtcell.setBorder(0);
				grandTotalAmount=grandTotalAmount+invoiceentity.getBillingOtherCharges().get(i).getPayableAmt();
			} else {
				pdfchargecell = new PdfPCell(chunk1);
				pdfchargeamtcell = new PdfPCell(chunk1);
			}


			PdfPCell pdfchargeamtcell1 = null;
			PdfPCell pdfchargecell1 = null;

			if (invoiceentity.getBillingOtherCharges().get(i).getTaxChargeAbsVal() != 0) {
				chunk = new Phrase(invoiceentity.getBillingOtherCharges().get(i).getOrderId()+"   "+invoiceentity.getBillingOtherCharges().get(i).getTaxChargeName()+ "", font1);
				pdfchargecell1 = new PdfPCell(chunk);
//				chargeAmt = invoiceentity.getBillingOtherCharges().get(i).getTaxChargeAbsVal();
				chargeAmt = invoiceentity.getBillingOtherCharges().get(i).getPayableAmt();
				chunk = new Phrase(chargeAmt + "", font8);
				pdfchargeamtcell1 = new PdfPCell(chunk);
				pdfchargeamtcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				pdfchargeamtcell1.setBorder(0);
				grandTotalAmount=grandTotalAmount+invoiceentity.getBillingOtherCharges().get(i).getPayableAmt();
			} else {
				pdfchargecell1 = new PdfPCell(chunk1);
				pdfchargeamtcell1 = new PdfPCell(chunk1);
			}
			pdfchargecell.setBorder(0);
			pdfchargeamtcell.setBorder(0);
			pdfchargecell1.setBorder(0);
			pdfchargeamtcell1.setBorder(0);

			if(invoiceentity.getBillingOtherCharges().get(i).getPayableAmt()!=0){
				chargetaxtable.addCell(pdfchargecell1);
				chargetaxtable.addCell(pdfchargeamtcell1);
				chargetaxtable.addCell(pdfchargecell);
				chargetaxtable.addCell(pdfchargeamtcell);
			}

		}  
			 
			
			
			
		Phrase netname = new Phrase("Grand Total", font8);
		PdfPCell netnamecell = new PdfPCell(netname);
		netnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		netnamecell.setBorder(0);
		chargetaxtable.addCell(netnamecell);

		double netpayable = 0;
//		netpayable = con.getNetpayable();
//		netpayable=invoiceentity.getInvoiceAmount();
		netpayable=grandTotalAmount;
		String netpayableamt = df.format(netpayable);
		Phrase netpay = new Phrase(netpayableamt, font8);
		PdfPCell netpaycell = new PdfPCell(netpay);
		netpaycell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		netpaycell.setBorder(0);
		chargetaxtable.addCell(netpaycell);

		PdfPTable billamtable = new PdfPTable(2);
		billamtable.setWidthPercentage(100);
		billamtable.setHorizontalAlignment(Element.ALIGN_BOTTOM);

		chunk = new Phrase("Net Payable", font1);
		PdfPCell pdfinvamtcell = new PdfPCell(chunk);
		pdfinvamtcell.setBorder(0);
		Double invAmt = invoiceentity.getInvoiceAmount();

		int netpayble = (int) invAmt.doubleValue();
		chunk = new Phrase(netpayble + "", font8);
		PdfPCell pdfnetpayamt = new PdfPCell(chunk);
		pdfnetpayamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
		pdfnetpayamt.setBorder(0);

		billamtable.addCell(pdfinvamtcell);
		billamtable.addCell(pdfnetpayamt);

		String amtInWords = "Rupees:  "+ SalesInvoicePdf.convert(invoiceentity.getInvoiceAmount());
		System.out.println("amt" + amtInWords);
		Phrase amtwords = new Phrase(amtInWords + " Only.", font1);
		PdfPCell amtWordsCell = new PdfPCell();
		amtWordsCell.addElement(amtwords);
		amtWordsCell.setBorder(0);

		PdfPTable amountInWordsTable = new PdfPTable(1);
		amountInWordsTable.setWidthPercentage(100);
		amountInWordsTable.addCell(amtWordsCell);

		
		
		PdfPCell serDateTblCell = new PdfPCell();
		serDateTblCell.addElement(serviceDateTable);
//		serDateTblCell.setBorder(0);
		
		// Amount in word table and net payable.

		PdfPTable parenttaxtable1 = new PdfPTable(2);
		parenttaxtable1.setWidthPercentage(100);
		try {
			parenttaxtable1.setWidths(new float[] { 65, 35 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		
		PdfPCell amtWordsTblcell = new PdfPCell();
		PdfPCell taxdatacell1 = new PdfPCell();

		amtWordsTblcell.addElement(amountInWordsTable);
		taxdatacell1.addElement(billamtable);

		parenttaxtable1.addCell(serDateTblCell);
		parenttaxtable1.addCell(chargetaxtable);
		parenttaxtable1.addCell(amtWordsTblcell);
		parenttaxtable1.addCell(taxdatacell1);

		try {
			document.add(parenttaxtable1);

		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
	
	
	
	
	
	
	
	
	
	
	
 public void addFooter()
 {
		logger.log(Level.SEVERE, "footer Info");
		String terms = "";
		Phrase desc1 = null;

		Phrase dotline = null;

		if (!invComment.equals("")) {
			if (upcflag == false) {
				terms = "Terms And Conditions";
			}
			if (upcflag == true) {
				terms = "";
			}
			desc1 = new Phrase(invComment, font8);
		}

		PdfPCell desc1cell = new PdfPCell();
		desc1cell.addElement(desc1);
		desc1cell.setBorder(0);

		Phrase term = new Phrase(terms, font10bold);

		PdfPCell termcell = new PdfPCell();
		termcell.addElement(term);
		termcell.setBorder(0);

		Phrase vatTaxNO = null;
		Phrase stTaxNO = null;
		PdfPCell dotlinecell = new PdfPCell();
		dotlinecell.addElement(dotline);
		dotlinecell.setBorder(0);

		PdfPTable desctable = new PdfPTable(1);
		desctable.setWidthPercentage(100);
		desctable.addCell(termcell);
		desctable.addCell(desc1cell);
		desctable.addCell(dotlinecell);

		PdfPCell blankkcell = new PdfPCell();
		blankkcell.addElement(Chunk.NEWLINE);
		blankkcell.setBorder(0);
		int rrrr = 0;
		for (int i = 0; i < this.articletype.size(); i++) {
			if (articletype.get(i).getArticlePrint().equalsIgnoreCase("YES")&& articletype.get(i).getDocumentName().equalsIgnoreCase("ServiceInvoice")) {
				rrrr = 1;
			}
		}

		if (rrrr == 1) {
			dotline = new Phrase("------------------------------------------------------------------------------------",font8);
			PdfPCell dotlinecell1 = new PdfPCell();
			dotlinecell1.addElement(dotline);
			dotlinecell1.setBorder(0);

			if (invComment.equals("")) {
				desctable.addCell(blankkcell);
				desctable.addCell(blankkcell);
			}

			desctable.addCell(dotlinecell1);
		}

		PdfPTable articletable = new PdfPTable(4);
		articletable.setWidthPercentage(100);
		articletable.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase blanck = new Phrase("");

		for (int i = 0; i < this.articletype.size(); i++) {

			if (articletype.get(i).getArticlePrint().equalsIgnoreCase("YES")
					&& articletype.get(i).getDocumentName()
							.equals("ServiceInvoice")) {

				vatTaxNO = new Phrase(articletype.get(i).getArticleTypeName(),
						font8);
				stTaxNO = new Phrase(articletype.get(i).getArticleTypeValue(),
						font8);

				PdfPCell vatcell = new PdfPCell();
				vatcell.addElement(vatTaxNO);
				vatcell.setBorder(0);
				vatcell.setHorizontalAlignment(Element.ALIGN_LEFT);

				PdfPCell vatcell1 = new PdfPCell();
				vatcell1.addElement(stTaxNO);
				vatcell1.setBorder(0);
				vatcell1.setHorizontalAlignment(Element.ALIGN_LEFT);

				PdfPCell vatcell2 = new PdfPCell();
				vatcell2.addElement(blanck);
				vatcell2.setBorder(0);

				articletable.addCell(vatcell);
				articletable.addCell(vatcell1);

				articletable.addCell(vatcell2);
				articletable.addCell(vatcell2);

			}

		}

		PdfPCell erticlecell = new PdfPCell(articletable);
		erticlecell.setBorder(0);
		erticlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		desctable.addCell(erticlecell);

		PdfPCell stcell = new PdfPCell();
		stcell.addElement(stTaxNO);
		stcell.setBorder(0);

		PdfPTable taxNOtable = new PdfPTable(1);
		taxNOtable.addCell(termcell);
		taxNOtable.addCell(desc1cell);
		taxNOtable.setSpacingAfter(60f);
		PdfPCell taxcell = new PdfPCell();
		taxcell.addElement(taxNOtable);
		taxcell.setBorder(0);

		String companyname = comp.getBusinessUnitName().trim().toUpperCase();
		Paragraph companynamepara = new Paragraph();
		companynamepara.add("FOR " + companyname);
		companynamepara.setFont(font9bold);
		companynamepara.setAlignment(Element.ALIGN_CENTER);
		String authsign = "AUTHORISED SIGNATORY";

		Paragraph authpara = new Paragraph();
		authpara.add(authsign);
		authpara.setFont(font9bold);
		authpara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell companynamecell = new PdfPCell();
		companynamecell.addElement(companynamepara);
		companynamecell.setBorder(0);

		PdfPCell authsigncell = new PdfPCell();
		authsigncell.addElement(authpara);
		authsigncell.setBorder(0);
		Phrase blankphrase = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell();
		blankcell.addElement(blankphrase);
		blankcell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(companynamecell);
		table.addCell(blankcell);
		table.addCell(blankcell);
		table.addCell(blankcell);
		table.addCell(authsigncell);

		table.setWidthPercentage(100);

		PdfPTable parentdesctable = new PdfPTable(2);
		parentdesctable.setWidthPercentage(100);
		PdfPCell signparent = new PdfPCell();
		PdfPCell descparent = new PdfPCell();
		PdfPCell authparent = new PdfPCell();

		descparent.addElement(desctable);
		authparent.addElement(table);
		parentdesctable.addCell(descparent);
		parentdesctable.addCell(authparent);

		try {
			parentdesctable.setWidths(new float[] { 65, 35 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		String endDucument = "*************************X--X--X*************************";
		Paragraph endpara = new Paragraph();
		endpara.add(endDucument);
		endpara.setAlignment(Element.ALIGN_CENTER);

		PdfPTable parentbanktable = new PdfPTable(1);
		parentbanktable.setWidthPercentage(100);

		PdfPCell authorisedcell = new PdfPCell();

		authorisedcell.addElement(parentdesctable);
		parentbanktable.addCell(authorisedcell);

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
		PdfPTable disclaimertable = new PdfPTable(1);

		if (disclaimerflag == false) {

			disclaimertable.setWidthPercentage(100);
			Phrase disclaimerphrase = new Phrase(disclaimerText, font1);

			PdfPCell disclaimercell = new PdfPCell(disclaimerphrase);
			disclaimertable.addCell(disclaimercell);
		}

		Phrase refphrase = new Phrase("Annexure 1 ", font10bold);
		Paragraph repara = new Paragraph(refphrase);

		try {
			document.add(parentdesctable);
			document.add(Chunk.NEWLINE);
			if (disclaimerflag == true) {
				document.add(disclaimertable);
			}

//			if ((contractChargesLis.size() + invoiceentity.getBillingTaxes().size()) > 4) {
//
//				chunk = new Phrase("Annexure for more details", font1);
//				Paragraph para = new Paragraph(chunk);
//				para.setSpacingAfter(10f);
//
//				PdfPTable parentcelltable = new PdfPTable(1);
//				parentcelltable.setWidthPercentage(100);
//				parentcelltable.addCell(remainigInfotable);
//
//				document.add(Chunk.NEXTPAGE);
//				document.add(para);
//				document.add(parentcelltable);
//			}

			Paragraph blank1 = new Paragraph();
			blank1.add(Chunk.NEWLINE);

			if (this.salesProd.size() > 5) {
				// if(eva>20){
				document.newPage();

				if (upcflag == true) {

					document.add(blank1);
					document.add(blank1);
					document.add(blank1);
					document.add(blank1);
					document.add(blank1);

				}

				document.add(repara);
				settingRemainingRowsToPDF(6);
			}
			if (this.salesProd.size() > 55) {
				// }if(eva>80){
				document.newPage();
				if (upcflag == true) {

					document.add(blank1);
					document.add(blank1);
					document.add(blank1);
					document.add(blank1);
					document.add(blank1);

				}
				document.add(repara);
				settingRemainingRowsToPDF(56);
			}
			if (this.salesProd.size() > 115) {
				// }if(eva>140){
				document.newPage();
				if (upcflag == true) {

					document.add(blank1);
					document.add(blank1);
					document.add(blank1);
					document.add(blank1);
					document.add(blank1);

				}
				document.add(repara);
				settingRemainingRowsToPDF(116);
			}
			if (this.salesProd.size() > 175) {
				// }if(eva>200){
				document.newPage();
				if (upcflag == true) {

					document.add(blank1);
					document.add(blank1);
					document.add(blank1);
					document.add(blank1);
					document.add(blank1);

				}
				document.add(repara);
				settingRemainingRowsToPDF(176);
			}

			/***************************************** Start product Description ************************************************************/

			String prodetails = "";
			int ctr = 0;
			for (int i = 0; i < this.salesProd.size(); i++) {
				if (!stringlis.get(i).getComment().equals("")
						|| custbranchlist.size() != 0) {
					ctr = ctr + 1;
				}
				if ((i == this.salesProd.size() - 1) && (ctr > 0)) {
					document.add(Chunk.NEXTPAGE);
					if (upcflag == true) {
						Paragraph blank = new Paragraph();
						blank.add(Chunk.NEWLINE);

						document.add(blank);
						document.add(blank);
						document.add(blank);
						document.add(blank);
						document.add(blank);
					}
				}
			}

			if (custbranchlist.size() != 0
					&& !cust.getSecondaryAdress().getAddrLine1().equals("")) {

				System.out.println("in if ===============");

				Phrase title = new Phrase("Site Address");

				Paragraph titlpara = new Paragraph();
				titlpara.add(title);
				titlpara.setAlignment(Element.ALIGN_LEFT);

				document.add(titlpara);
				document.add(Chunk.NEWLINE);
				for (int i = 0; i < custbranchlist.size(); i++) {

					System.out.println("in for ============================");

					Phrase clientname = null;

					clientname = new Phrase("Branch Name : "
							+ custbranchlist.get(i).getBusinessUnitName()
									.trim(), font10bold);

					PdfPCell custnamecell = new PdfPCell();
					custnamecell.addElement(clientname);
					custnamecell.setBorder(0);

					Phrase custaddress1 = new Phrase(
							"                                 "
									+ custbranchlist.get(i).getAddress()
											.getAddrLine1().trim(), font8);

					Phrase custaddress2 = null;
					if (!custbranchlist.get(i).getAddress().getAddrLine2()
							.equals("")) {
						custaddress2 = new Phrase(
								"                                 "
										+ custbranchlist.get(i).getAddress()
												.getAddrLine2().trim(), font8);
					} else {
						custaddress2 = new Phrase("", font10);
					}
					Phrase landmark = null;

					if (!custbranchlist.get(i).getAddress().getLandmark()
							.equals("")) {
						landmark = new Phrase(
								"                                  "
										+ custbranchlist.get(i).getAddress()
												.getLandmark(), font8);
					} else {
						landmark = new Phrase("", font10);
					}
					Phrase locality = null;
					if (!custbranchlist.get(i).getAddress().getLocality()
							.equals("")) {
						locality = new Phrase(
								"                                 "
										+ custbranchlist.get(i).getAddress()
												.getLocality(), font8);
					} else {
						locality = new Phrase("", font8);
					}
					Phrase city = new Phrase(
							"                                 "
									+ custbranchlist.get(i).getAddress()
											.getCity().trim()
									+ " - "
									+ custbranchlist.get(i).getAddress()
											.getPin(), font8);

					Phrase contactcust = new Phrase("Mobile: "
							+ custbranchlist.get(i).getCellNumber1()
							+ "    Email:  "
							+ custbranchlist.get(i).getEmail().trim() + " ",
							font8);
					logger.log(Level.SEVERE,
							"222222222222222222222222222222222222222222222222222");
					PdfPCell custaddr1cell = new PdfPCell();
					custaddr1cell.addElement(custaddress1);
					custaddr1cell.setBorder(0);

					PdfPCell custaddr2cell = null;
					if (custaddress2 != null) {
						custaddr2cell = new PdfPCell();
						custaddr2cell.addElement(custaddress2);
						custaddr2cell.setBorder(0);
					}

					PdfPCell custlocalitycell = new PdfPCell();
					custlocalitycell.addElement(locality);
					custlocalitycell.setBorder(0);

					PdfPCell lanmarkcell = new PdfPCell();
					lanmarkcell.addElement(landmark);
					lanmarkcell.setBorder(0);

					PdfPCell citycell = new PdfPCell();
					citycell.addElement(city);
					citycell.setBorder(0);

					PdfPCell custcontactcell = new PdfPCell();
					custcontactcell.addElement(contactcust);
					// custcontactcell.addElement(Chunk.NEWLINE);
					custcontactcell.setBorder(0);
					logger.log(Level.SEVERE,
							"3333333333333333333333333333333333333333333333");
					PdfPTable custtable = new PdfPTable(1);
					custtable.setWidthPercentage(100);
					// custtable.addCell(titlecell);
					custtable.addCell(custnamecell);
					custtable.addCell(custaddr1cell);
					if (!cust.getAdress().getAddrLine2().equals("")) {
						custtable.addCell(custaddr2cell);
					}
					if (!cust.getAdress().getLandmark().equals("")) {
						custtable.addCell(lanmarkcell);
					}
					if (!cust.getAdress().getLocality().equals("")) {
						custtable.addCell(custlocalitycell);
					}
					custtable.addCell(citycell);
					custtable.addCell(custcontactcell);

					Paragraph blank = new Paragraph(" ");

					PdfPTable parenttable = new PdfPTable(1);
					parenttable.setWidthPercentage(100);

					PdfPCell custinfocell = new PdfPCell();

					custinfocell.addElement(custtable);
					parenttable.addCell(custinfocell);

					try {
						document.add(parenttable);
						// document.add(blank);
					} catch (DocumentException e) {
						e.printStackTrace();
					}

				}

			}

			for (int i = 0; i < this.salesProd.size(); i++) {
				if (!stringlis.get(i).getComment().equals("")) {
					prodetails = "Products Details";
				}

			}

			Phrase prophrase = new Phrase(prodetails, font12boldul);
			Paragraph propara = new Paragraph();
			propara.add(prophrase);

			if (upcflag == true) {
				if (custbranchlist.size() >= 4) {

					document.add(Chunk.NEXTPAGE);
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					document.add(propara);
					document.add(Chunk.NEWLINE);
				} else {
					document.add(Chunk.NEWLINE);
					document.add(propara);
					document.add(Chunk.NEWLINE);
				}

			} else {
				document.add(Chunk.NEWLINE);
				document.add(propara);
				document.add(Chunk.NEWLINE);
			}

			String descri = "";
			Phrase descri1 = new Phrase("", font10);
			Paragraph para1 = new Paragraph("");
			System.out.println("pro size() ==== " + this.salesProd.size());

			int ctr1 = 0;

			for (int i = 0; i < this.salesProd.size(); i++) {
				if (!stringlis.get(i).getComment().equals("")) {
					term = new Phrase("Product Id : "
							+ stringlis.get(i).getCount() + ""
							+ "        Product Name : "
							+ stringlis.get(i).getProductName(), font10bold);
					descri1 = new Phrase(
							descri + stringlis.get(i).getComment(), font10);
					ctr1 = ctr1 + 1;
				} else {

					term = new Phrase("", font10);
					descri1 = new Phrase("", font10);
				}

				para1 = new Paragraph();
				para1.add(term);
				para1.setAlignment(Element.ALIGN_LEFT);
				Paragraph para2 = new Paragraph();
				para2.add(descri1);

				if (upcflag == true) {

					if (ctr1 > 5) {

						document.add(Chunk.NEXTPAGE);
						document.add(Chunk.NEWLINE);
						document.add(Chunk.NEWLINE);
						document.add(Chunk.NEWLINE);
						document.add(Chunk.NEWLINE);
						document.add(Chunk.NEWLINE);

						document.add(para1);
						document.add(para2);
					} else {

						document.add(para1);
						document.add(para2);
					}

				} else {

					document.add(para1);
					document.add(para2);

				}

			}

			/***************************************** End product Description ************************************************************/

		} catch (DocumentException e) {
			e.printStackTrace();
		}

		int firstBreakPoint = 5;
		int cnt = this.billingChargesLis.size() + this.billingTaxesLis.size();
		System.out.println("cnt value===" + cnt);
		if (cnt > firstBreakPoint) {

			otherchargestoAnnexur();
		}
		logger.log(Level.SEVERE, "End of  footer Info");
	}





	private void otherchargestoAnnexur() {

		logger.log(Level.SEVERE, "otherchargestoAnnexur Info");

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
		String charge = "Other charge details";
		Chunk prodchunk = new Chunk(charge, font10bold);
		Paragraph parag = new Paragraph();
		parag.add(prodchunk);

		PdfPTable chargetable = new PdfPTable(2);
		chargetable.setWidthPercentage(100);
		// ********************
		double total = 0;

		for (int i = 0; i < this.billingChargesLis.size(); i++) {
			Phrase chunk = null;
			Phrase chunk1 = new Phrase("", font8);
			double chargeAmt = 0;
			PdfPCell pdfchargeamtcell = null;
			PdfPCell pdfchargecell = null;
			if (billingChargesLis.get(i).getTaxChargePercent() != 0) {
				chunk = new Phrase(billingChargesLis.get(i).getTaxChargeName()
						+ " @ "
						+ billingChargesLis.get(i).getTaxChargePercent(), font1);
				pdfchargecell = new PdfPCell(chunk);
				chargeAmt = billingChargesLis.get(i).getTaxChargePercent()
						* billingChargesLis.get(i).getTaxChargeAssesVal() / 100;
				chunk = new Phrase(df.format(chargeAmt), font8);
				pdfchargeamtcell = new PdfPCell(chunk);
				pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				pdfchargeamtcell.setBorder(0);
			}
			if (billingChargesLis.get(i).getTaxChargeAbsVal() != 0) {
				chunk = new Phrase(billingChargesLis.get(i).getTaxChargeName()
						+ "", font1);
				pdfchargecell = new PdfPCell(chunk);
				chargeAmt = billingChargesLis.get(i).getTaxChargeAbsVal();
				chunk = new Phrase(chargeAmt + "", font8);
				pdfchargeamtcell = new PdfPCell(chunk);
				pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				pdfchargeamtcell.setBorder(0);
				total = total + chargeAmt;
			}
			pdfchargecell.setBorder(0);
			pdfchargeamtcell.setBorder(0);
			chargetable.addCell(pdfchargecell);
			chargetable.addCell(pdfchargeamtcell);
		}

		Phrase totalchunk = new Phrase(
				"Total                                                                      "
						+ "                                       "
						+ df.format(total), font10bold);
		PdfPCell totalcell = new PdfPCell();
		totalcell.addElement(totalchunk);

		PdfPCell chargecell = new PdfPCell();
		chargecell.addElement(chargetable);
		chargecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(70);
		parent.setSpacingBefore(10f);
		parent.addCell(chargecell);
		parent.addCell(totalcell);
		parent.setHorizontalAlignment(Element.ALIGN_LEFT);

		try {
			document.add(Chunk.NEXTPAGE);
			document.add(parag);
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		logger.log(Level.SEVERE, " end of otherchargestoAnnexur Info");
	}





	private static final String[] tensNames = { "", " Ten", " Twenty", " Thirty", " Fourty", " Fifty", " Sixty", " Seventy", " Eighty", " Ninety" };

	    private static final String[] numNames = { "", " One", " Two", " Three", " Four", " Five", " Six", " Seven", " Eight", " Nine", " Ten", " Eleven", " Twelve",
	    	" Thirteen", " Fourteen", " Fifteen", " Sixteen", " Seventeen", " Eighteen", " Nineteen" };


	    private static String convertLessThanOneThousand(int number) {
	    	String soFar;
	    	if (number % 100 < 20){ soFar = numNames[number % 100]; number /= 100; } else { soFar = numNames[number % 10]; number /= 10;
	    	soFar = tensNames[number % 10] + soFar; number /= 10; } if (number == 0) return soFar; return numNames[number] + " Hundred" + soFar; 
	    	}
	    	public static  String convert(double number) {
	    	// 0 to 999 999 999 999
	    	if (number == 0) { return "Zero"; }
	    	String snumber = Double.toString(number);
	    	// pad with "0"
	    	String mask = "000000000000"; DecimalFormat df = new DecimalFormat(mask); snumber = df.format(number);
	    	int hyndredCrore = Integer.parseInt(snumber.substring(3,5));
	    	int hundredLakh = Integer.parseInt(snumber.substring(5,7));
	    	int hundredThousands = Integer.parseInt(snumber.substring(7,9));
	    	int thousands = Integer.parseInt(snumber.substring(9,12));
	    	String tradBillions;
	    	switch (hyndredCrore) { case 0: tradBillions = ""; break; case 1 : tradBillions = convertLessThanOneThousand(hyndredCrore) + " Crore "; break; default : tradBillions = convertLessThanOneThousand(hyndredCrore) + " Crore "; }

	    	String result = tradBillions;
	    	String tradMillions;
	    	switch (hundredLakh) { case 0: tradMillions = ""; break; case 1 : tradMillions = convertLessThanOneThousand(hundredLakh) + " Lakh "; break; default : tradMillions = convertLessThanOneThousand(hundredLakh) + " Lakh "; }
	    	result = result + tradMillions;
	    	String tradHundredThousands;

	    	switch (hundredThousands) { case 0: tradHundredThousands = ""; break; case 1 : tradHundredThousands = "One Thousand "; break; default : tradHundredThousands = convertLessThanOneThousand(hundredThousands) + " Thousand "; }
	    	result = result + tradHundredThousands;

	    	String tradThousand;
	    	tradThousand = convertLessThanOneThousand(thousands);
	    	result = result + tradThousand;return result.replaceAll("^\\s+", "").replaceAll("file://b//s%7B2,%7D//b", " "); 
	    	}
	    	
	    	
	    	List<ProductDetailsPO> productcount =new ArrayList<ProductDetailsPO>();
	    	
	  	  	    public List<ProductDetailsPO> getdata(){
	  	  	    	
	  	  	    	ProductDetailsPO po=new ProductDetailsPO();
	  	  	    	po.setProductCategory("category");
	  	  	    	po.setProductQuantity(20);
	  	  	    	po.setTax(65);
	  	  	    	po.setVat(55);
	  	  	    	po.setDiscount(22);
	  	  	    	productcount.add(po);
	  	  	    	return productcount;
	  	  	    }
	  	  	    
	  	  	    
	    	
	public void settingRemainingRowsToPDF(int flag) {

		logger.log(Level.SEVERE, "setting Remaining Rows To PDFInfo");
		logger.log(Level.SEVERE, "Flag Value in service invoice pdf 2" + flag);
		PdfPTable table = new PdfPTable(8);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100);

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
		Phrase productdetails = new Phrase("Product Details", font1);
		Paragraph para = new Paragraph();
		para.add(Chunk.NEWLINE);
		para.add(Chunk.NEWLINE);
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_CENTER);
		para.add(Chunk.NEWLINE);

		Phrase category = new Phrase("SR. NO. ", font1);
		Phrase productname = new Phrase("ITEM DETAILS", font1);
		Phrase qty = new Phrase("QTY", font1);
		Phrase unit = new Phrase("UNIT", font1);
		Phrase rate = new Phrase("RATE", font1);

		Phrase percDisc = new Phrase("DISC %", font1);
		Phrase adddisc = new Phrase("ADDL.DISC %", font1);

		Phrase servicetax = null;
		Phrase servicetax1 = null;
		int flag12 = 0;
		int vat = 0;
		int st = 0;
		for (int i = 0; i < salesProd.size(); i++) {

			if ((salesProd.get(i).getVatTax().getPercentage() > 0)
					&& (salesProd.get(i).getServiceTax().getPercentage() == 0)) {
				servicetax = new Phrase("VAT %", font1);
				vat = vat + 1;
				System.out.println("phrase value====" + servicetax.toString());
			} else if ((salesProd.get(i).getServiceTax().getPercentage() > 0)
					&& (salesProd.get(i).getVatTax().getPercentage() == 0)) {
				servicetax = new Phrase("ST %", font1);
				st = st + 1;
			} else if ((salesProd.get(i).getVatTax().getPercentage() > 0)
					&& (salesProd.get(i).getServiceTax().getPercentage() > 0)) {
				servicetax1 = new Phrase("VAT / ST %", font1);
				flag12 = flag12 + 1;
				System.out.println("flag value;;;;;" + flag12);
			} else {

				servicetax = new Phrase("TAX %", font1);
			}
		}
		Phrase total = new Phrase("AMOUNT", font1);

		logger.log(Level.SEVERE, "Flag Value in service invoice pdf 1" + flag);
		PdfPCell cellcategory = new PdfPCell(category);
		cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
		// PdfPCell cellproductcode = new PdfPCell(productcode);
		PdfPCell cellproductname = new PdfPCell(productname);
		cellproductname.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellqty = new PdfPCell(qty);
		cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellunit = new PdfPCell(unit);
		cellunit.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellrate = new PdfPCell(rate);
		cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell percDisccell = new PdfPCell(percDisc);
		percDisccell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// PdfPCell cellservicetax = new PdfPCell(tax);
		// cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
		// PdfPCell cellvat = new PdfPCell(vat);

		PdfPCell cellservicetax1 = null;
		PdfPCell cellservicetax = null;
		PdfPCell cellservicetax2 = null;
		Phrase servicetax2 = null;

		if (flag > 0) {
			cellservicetax1 = new PdfPCell(servicetax1);
			cellservicetax1.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else if (vat > 0 && st > 0) {
			System.out.println("in side condition");
			servicetax2 = new Phrase("VAT / ST %", font1);
			cellservicetax2 = new PdfPCell(servicetax2);
			cellservicetax2.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else {
			cellservicetax = new PdfPCell(servicetax);
			cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);

		}

		PdfPCell celltotal = new PdfPCell(total);
		celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(cellcategory);
		// table.addCell(cellproductcode);
		table.addCell(cellproductname);
		table.addCell(cellqty);
		table.addCell(cellunit);
		table.addCell(cellrate);
		table.addCell(percDisccell);

		if (flag > 0) {
			table.addCell(cellservicetax1);
		} else if (vat > 0 && st > 0) {
			table.addCell(cellservicetax2);
		} else {
			table.addCell(cellservicetax);
		}
		table.addCell(celltotal);
		logger.log(Level.SEVERE, "Flag Value in service invoice pdf" + flag
				+ "====" + this.salesProd.size());
		for (int i = flag; i < this.salesProd.size(); i++) {

			Phrase chunk = new Phrase((i + 1) + "", font8);
			PdfPCell pdfcategcell = new PdfPCell(chunk);
			pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			// chunk=new Phrase(salesProd.get(i).getProdCode(),font8);
			// PdfPCell pdfcpcodecell = new PdfPCell(chunk);
			logger.log(Level.SEVERE, "Flag Value in service invoice pdf" + flag
					+ "====" + salesProd.get(i).getProdName());
			chunk = new Phrase(salesProd.get(i).getProdName(), font8);
			PdfPCell pdfnamecell = new PdfPCell(chunk);

			chunk = new Phrase(salesProd.get(i).getQuantity() + "", font8);
			PdfPCell pdfqtycell = new PdfPCell(chunk);
			pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(salesProd.get(i).getUnitOfMeasurement(), font8);
			PdfPCell pdfunitcell = new PdfPCell(chunk);
			pdfunitcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(df.format(salesProd.get(i).getPrice()), font8);
			PdfPCell pdfspricecell = new PdfPCell(chunk);
			pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			if (salesProd.get(i).getProdPercDiscount() != 0)
				chunk = new Phrase(salesProd.get(i).getProdPercDiscount() + "",
						font8);
			else
				chunk = new Phrase("0" + "", font8);
			PdfPCell pdfperdiscount = new PdfPCell(chunk);
			pdfperdiscount.setHorizontalAlignment(Element.ALIGN_RIGHT);

			PdfPCell pdfservice = null;
			Phrase vatno = null;
			Phrase stno = null;
			if ((salesProd.get(i).getVatTax().getPercentage() == 0)
					&& (salesProd.get(i).getServiceTax().getPercentage() > 0)) {

				if (salesProd.get(i).getServiceTax().getPercentage() != 0) {
					if (st == salesProd.size()) {
						chunk = new Phrase(salesProd.get(i).getServiceTax()
								.getPercentage()
								+ "", font8);
					} else {
						stno = new Phrase(salesProd.get(i).getVatTax()
								.getPercentage()
								+ ""
								+ " / "
								+ salesProd.get(i).getServiceTax()
										.getPercentage() + "", font8);
					}
				} else
					chunk = new Phrase("N.A" + "", font8);
				if (st == salesProd.size()) {
					pdfservice = new PdfPCell(chunk);
				} else {
					pdfservice = new PdfPCell(stno);
				}

				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			} else if ((salesProd.get(i).getServiceTax().getPercentage() == 0)
					&& (salesProd.get(i).getVatTax().getPercentage() > 0)) {

				if (salesProd.get(i).getVatTax() != null) {
					if (vat == salesProd.size()) {
						System.out.println("rohan==" + vat);
						chunk = new Phrase(salesProd.get(i).getVatTax()
								.getPercentage()
								+ "", font8);
					} else {
						System.out.println("mukesh");
						vatno = new Phrase(salesProd.get(i).getVatTax()
								.getPercentage()
								+ ""
								+ " / "
								+ salesProd.get(i).getServiceTax()
										.getPercentage(), font8);
					}
				} else {
					chunk = new Phrase("N.A" + "", font8);
				}
				if (vat == salesProd.size()) {
					pdfservice = new PdfPCell(chunk);
				} else {
					pdfservice = new PdfPCell(vatno);
				}
				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			} else if ((salesProd.get(i).getServiceTax().getPercentage() > 0)
					&& (salesProd.get(i).getVatTax().getPercentage() > 0)) {
				if ((salesProd.get(i).getVatTax() != null)
						&& (salesProd.get(i).getServiceTax() != null))

					chunk = new Phrase(salesProd.get(i).getVatTax()
							.getPercentage()
							+ ""
							+ " / "
							+ ""
							+ salesProd.get(i).getServiceTax().getPercentage(),
							font8);
				else
					chunk = new Phrase("N.A" + "", font8);
				pdfservice = new PdfPCell(chunk);
				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);

			} else {

				chunk = new Phrase("N.A" + "", font8);
				pdfservice = new PdfPCell(chunk);
				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);

			}

			chunk = new Phrase(df.format(salesProd.get(i).getTotalAmount()),
					font8);
			PdfPCell pdftotalproduct = new PdfPCell(chunk);
			pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);

			table.addCell(pdfcategcell);
			// table.addCell(pdfcpcodecell);
			table.addCell(pdfnamecell);
			table.addCell(pdfqtycell);
			table.addCell(pdfunitcell);
			table.addCell(pdfspricecell);
			table.addCell(pdfperdiscount);
			table.addCell(pdfservice);
			table.addCell(pdftotalproduct);
			try {
				table.setWidths(columnWidths);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			flag = flag + 1;
			if (flag == 56 || flag == 116 || flag == 176) {
				break;
			}
		}

		PdfPTable parentTableProd = new PdfPTable(1);
		parentTableProd.setWidthPercentage(100);
		parentTableProd.setSpacingBefore(10f);

		PdfPCell prodtablecell = new PdfPCell();
		prodtablecell.addElement(table);
		prodtablecell.setBorder(0);
		parentTableProd.addCell(prodtablecell);

		try {
			document.add(parentTableProd);

		} catch (DocumentException e) {
			e.printStackTrace();
		}

		logger.log(Level.SEVERE, "end of setting Remaining Rows To PDFInfo");
	}
	
	
	
	
	    	
	public void productDetailsForEmail() {
		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			if (invoiceentity.getCompanyId() != null) {
				SuperProduct sup = ofy()
						.load()
						.type(SuperProduct.class)
						.filter("companyId", invoiceentity.getCompanyId())
						.filter("count",
								invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount())
						.first().now();

				SuperProduct superprod = new SuperProduct();

				superprod.setCount(sup.getCount());
				superprod.setProductName(sup.getProductName());
				superprod.setComment(sup.getComment() + " "
						+ sup.getCommentdesc());

				stringlis.add(superprod);
			}
		}
	}
	    	
 public  void createPdfForEmail(Invoice invoiceDetails,Company companyEntity,Customer custEntity) 
 {
		this.invoiceentity = invoiceDetails;
		this.comp = companyEntity;
		this.cust = custEntity;
//		this.con = contractEntity;
//		this.billEntity = billingEntity;
		this.salesProd = invoiceentity.getSalesOrderProducts();
//		this.contractTaxesLis = con.getProductTaxes();
//		this.contractChargesLis = con.getProductCharges();
		this.billingTaxesLis = invoiceentity.getBillingTaxes();
		this.billingChargesLis = invoiceentity.getBillingOtherCharges();

		/************************************ Letter Head Flag *******************************/

		if (invoiceentity.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", invoiceentity.getCompanyId())
					.filter("processName", "Invoice")
					.filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("CompanyAsLetterHead")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						this.upcflag = true;
					}
				}
			}
		}

		/********************************** Disclaimer Flag ******************************************/

		if (invoiceentity.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", invoiceentity.getCompanyId())
					.filter("processName", "Invoice")
					.filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("DisclaimerText")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						this.disclaimerflag = true;
					}
				}
			}
		}

		productDetailsForEmail();
		articletype = new ArrayList<ArticleType>();
		if (cust.getArticleTypeDetails().size() != 0) {
			articletype.addAll(cust.getArticleTypeDetails());
		}
		if (comp.getArticleTypeDetails().size() != 0) {
			articletype.addAll(comp.getArticleTypeDetails());
		}
//		arrPayTerms = invoiceentity.getArrPayTerms();

		try {
			createLogo(document, comp);
			createCompanyAddress();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			createCustInfo1();
		} catch (Exception e) {
			e.printStackTrace();
		}
		createProductTable();
		addFooter();

	}
	
	
}
