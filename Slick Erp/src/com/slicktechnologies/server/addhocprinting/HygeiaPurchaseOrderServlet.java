package com.slicktechnologies.server.addhocprinting;

import javax.servlet.http.HttpServlet;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;

public class HygeiaPurchaseOrderServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4833917478054408263L;


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {

			String stringid = req.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			System.out.println("Vaishnavi +++++++++++++++" + stringid);

			HygeiaPurchaseOrderPdf purchasepdf = new HygeiaPurchaseOrderPdf();

			purchasepdf.document = new Document();
			Document document = purchasepdf.document;
			PdfWriter writer = PdfWriter.getInstance(document,
					resp.getOutputStream()); // write

			document.open();

			purchasepdf.setpdfpurchaseorder(count);
			purchasepdf.createPdf();

			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
}
