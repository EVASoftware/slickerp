

package com.slicktechnologies.server.addhocprinting.friends;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.api.server.spi.response.ForbiddenException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class FrendsServiceCardPdf {
	/**
	 * 
	 * @author Jayshree 
	 * Description :For Friends pest control : It will print service card 
	 * Called From : CreateServiceCardServlet.java 
	 * Date:15/10/2017
	 * Created By : Jayshree Chavhan
	 */
	Logger logger = Logger.getLogger("ServiceGSTInvoice.class");
	public Document document;
	Contract con;
	Customer cust;
	Company comp;
	int servicecount = 0;
	Invoice invoiceentity;
	List <Branch> branchList;
	ContactPersonIdentification contactPerson;
	List<ContactPersonIdentification> contactList;
	private Font font16boldul, font12bold, font8bold, font8, font9bold,
			font12Wbold, font12boldul, font10boldul, font12, font16bold,
			font10, font10bold, font10ul, font9boldul, font14bold, font9,
			font7, font7bold, font9red, font9boldred, font12boldred, font23;

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt2 = new SimpleDateFormat("dd/MM/yyyy");
	DecimalFormat df = new DecimalFormat("0.00");
	SimpleDateFormat sdf;

	public FrendsServiceCardPdf() {
		super();

		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
				| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 5);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font12Wbold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,
				BaseColor.WHITE);
		font10 = new Font(Font.FontFamily.HELVETICA, 9);
		font10bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font10ul = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL
				| Font.UNDERLINE);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD
				| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font9boldul = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD
				| Font.UNDERLINE);
		font9boldred = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD,
				BaseColor.RED);
		font9 = new Font(Font.FontFamily.HELVETICA, 8);
		font9red = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL,
				BaseColor.RED);
		font7 = new Font(Font.FontFamily.HELVETICA, 7);
		font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
		font23 = new Font(Font.FontFamily.HELVETICA, 23);
		font12Wbold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,
				BaseColor.WHITE);

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

		sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}

	public void loadServiceCard(Contract contract, long id) {
		// TODO Auto-generated method stub
		System.out.println("contractid111=="+id);
		
		if (contract != null) {
			con = contract;
		} else {
			con = ofy().load().type(Contract.class).id(id).now();
		}
		invoiceentity = ofy().load().type(Invoice.class)
				.filter("contractCount", con.getCount()).first().now();

		cust = ofy().load().type(Customer.class)
				.filter("companyId", con.getCompanyId())
				.filter("count", con.getCustomerId()).first().now();

		if (con.getCompanyId() == null){
			comp = ofy().load().type(Company.class).first().now();
		}
		else{
			comp = ofy().load().type(Company.class)
					.filter("companyId", con.getCompanyId()).first().now();

		contactList = ofy().load().type(ContactPersonIdentification.class)
				.filter("companyId", comp.getCompanyId())
				.filter("personInfo.count", cust.getCount()).list();
		}
		
		 branchList=ofy().load().type(Branch.class).filter("companyId",con.getCompanyId())
				.filter("status",true).list();
		 System.out.println("branchlist value"+ branchList);
		 
		 
		 /**
			 * nidhi
			 * 06-04-2018
			 * for branch as a company process configration
			 * 
			 */
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
				
				logger.log(Level.SEVERE,"Process active --");
				if(con !=null && con.getBranch() != null && con.getBranch().trim().length()>0){
					
					Branch branchDt = ofy().load().type(Branch.class).filter("companyId",con.getCompanyId()).filter("buisnessUnitName", con.getBranch()).first().now();

					if(branchDt !=null && branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){

						logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());
							
						
						List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
						
						if(paymentDt.get(0).trim().matches("[0-9]+")){
							
							
							
							int payId = Integer.parseInt(paymentDt.get(0).trim());
							
//							comppayment = ofy().load().type(CompanyPayment.class)
//									.filter("count", payId)
//									.filter("companyId", invoiceentity.getCompanyId()).first()
//									.now();
//							
//							
//							if(comppayment != null){
								comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
//							}
							
						}
						
						
					}
				}
			}
			
			/**
			 * end
			 */

	}

	public void createPdf() {
		// TODO Auto-generated method stub
		for (int i = 0; i < con.getItems().size(); i++) {
			System.out.println("con.getItems().size()" + con.getItems().size());
			if (i != 0) {
				try {
					document.add(Chunk.NEXTPAGE);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
			}
			createHeaderTable(con.getItems().get(i));
			createCustomerTable(con.getItems().get(i));
			createTreatMentHeader();
			int startingProductIndex = 0;
			createTreatmentDetailTable(startingProductIndex, con.getItems().get(i));
		}
	}

	/**
	 * Date:16/10/2017 By Jayshre Deascription :this method is created for to
	 * print the header section part of service card
	 * 
	 * @param salesLineItem
	 */
	private void createHeaderTable(SalesLineItem salesLineItem) {

		//Date 16/11/2017 By Jayshree to add the logo
		DocumentUpload logodocument =comp.getLogo();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2=null;
		try {
			image2 = Image.getInstance(new URL(hostUrl+logodocument.getUrl()));
			image2.scalePercent(20f);
//			image2.setAbsolutePosition(40f,765f);	
//			doc.add(image2);
			
			imageSignCell = new PdfPCell();
			imageSignCell.addElement(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			imageSignCell.setFixedHeight(20);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		Image image1=null;
//		try
//		{
//		 image1=Image.getInstance("images/ipclogo4.jpg");
//		image1.scalePercent(20f);
////		image1.setAbsolutePosition(40f,765f);	
////		doc.add(image1);
//		
//		
//		
//		
//		
//		imageSignCell=new PdfPCell();
//		imageSignCell.addElement(image1);
//		imageSignCell.setFixedHeight(20);
//		imageSignCell.setBorder(0);
//		imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		
		PdfPTable logoTable = new PdfPTable(2);
		
		logoTable.setWidthPercentage(100f);
		try {
			logoTable.setWidths(new float[]{30,70});
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		Phrase blank =new Phrase(" ");
		PdfPCell blankCell=new PdfPCell(blank);
		blankCell.setBorder(0);
		if(imageSignCell != null)
		{
			logoTable.addCell(blankCell);
			logoTable.addCell(imageSignCell);
		}
		else
		{
			logoTable.addCell(blankCell);
			logoTable.addCell(blankCell);
		}
//Date 16/11/2017
//By Jayshree 
//to add the web site in header part
		PdfPTable headTab = new PdfPTable(1);
		headTab.setWidthPercentage(100f);

//		try {
//			headTab.setWidths(new float[] { 47, 6, 47 });
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
		String businessunit=null;
		businessunit=comp.getBusinessUnitName();
		Phrase companyNameph = new Phrase(businessunit.toUpperCase(),
				font12bold);
		PdfPCell companyNameCell = new PdfPCell(companyNameph);
//		companyNameCell.setColspan(3);
		companyNameCell.setBorder(0);
		companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headTab.addCell(companyNameCell);

		Phrase companyAddph = new Phrase(
				comp.getAddress().getCompleteAddress(), font9);
		PdfPCell companyAddCell = new PdfPCell(companyAddph);
//		companyAddCell.setColspan(3);
		companyAddCell.setBorder(0);
		companyAddCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headTab.addCell(companyAddCell);

		String website="";
		String emailid="";
		String conNo="";
		
		if(comp.getWebsite()==null || comp.getWebsite().equals(""))
		{
			website="";
		}
		else
		{
			
			website="Website:-"+comp.getWebsite();
		}
		
		if(comp.getEmail()==null|| comp.getEmail().equals(""))
		{
			
			emailid="";
		}
		else
		{
			emailid="E-Mail:- " + comp.getEmail();
		}
		Phrase companyWebph = new Phrase(website+"  "+emailid, font9bold);
		PdfPCell companyWebCell = new PdfPCell(companyWebph);
		companyWebCell.setBorder(0);
		companyWebCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headTab.addCell(companyWebCell);

//		Phrase companyWebBlankph = new Phrase("    ", font9bold);
//		PdfPCell companyWebBlankCell = new PdfPCell(companyWebBlankph);
//		companyWebBlankCell.setBorder(0);
//
//		headTab.addCell(companyWebBlankCell);

		if(comp.getCellNumber1()==null||comp.getCellNumber2()==null)
		{
			conNo=" ";
			
		}
		else 
		{
			conNo="Mobile:-"+comp.getCellNumber1()+","+comp.getCellNumber2();
		}
		Phrase companymobph = new Phrase(conNo,
				font9bold);
		PdfPCell companymobCell = new PdfPCell(companymobph);
		companymobCell.setBorder(0);
		companymobCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headTab.addCell(companymobCell);

		PdfPTable headerTable = new PdfPTable(2);
		headerTable.setWidthPercentage(100f);

		try {
			headerTable.setWidths(new float[] { 20, 90 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		
		
		PdfPCell logoout = new PdfPCell(logoTable);
//		logoout.addElement(logoTable);
		logoout.setBorderWidthBottom(0);
		logoout.setBorderWidthRight(0);
		logoout.setHorizontalAlignment(Element.ALIGN_RIGHT);
		headerTable.addCell(logoout);

		PdfPCell headOut = new PdfPCell(headTab);
//		headOut.addElement(headTab);
		headOut.setBorderWidthBottom(0);
		headOut.setBorderWidthLeft(0);
		headerTable.addCell(headOut);
		//end Here
		Phrase 	logoout2ph=new Phrase("  ",font8);
		PdfPCell logoout2 = new PdfPCell(logoout2ph);
		logoout2.setBorderWidthBottom(0);
		logoout2.setBorderWidthRight(0);
		logoout2.setBorderWidthTop(0);
		headerTable.addCell(logoout2);

		Phrase 	headOut2ph=new Phrase("  ",font8);
		PdfPCell headOut2 = new PdfPCell(headOut2ph);
		headOut2.setBorderWidthBottom(0);
		headOut2.setBorderWidthLeft(0);
		headOut2.setBorderWidthTop(0);
		headerTable.addCell(headOut2);

		try {
			document.add(headerTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
// End by jayshree
		PdfPTable serviceTab1 = new PdfPTable(3);
		serviceTab1.setWidthPercentage(100f);
//		serviceTab1.setSpacingBefore(5);

		try {
			serviceTab1.setWidths(new float[] { 35, 30, 35 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		Phrase serviBlankph = new Phrase(" ", font10bold);
		PdfPCell serviBlankCell = new PdfPCell(serviBlankph);
		serviBlankCell.setBorderWidthRight(0);
		serviBlankCell.setBorderWidthTop(0);
		serviBlankCell.setBorderWidthBottom(0);
		serviBlankCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		serviceTab1.addCell(serviBlankCell);

		Phrase serviceph = new Phrase("SERVICE CARD", font10bold);
		PdfPCell serviceCell = new PdfPCell(serviceph);
		serviceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		serviceTab1.addCell(serviceCell);

		Phrase serviBlankph2 = new Phrase(" ", font10bold);
		PdfPCell serviBlankCell2 = new PdfPCell(serviBlankph2);
		serviBlankCell2.setBorderWidthLeft(0);
		serviBlankCell2.setBorderWidthTop(0);
		serviBlankCell2.setBorderWidthBottom(0);
		serviBlankCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
		serviceTab1.addCell(serviBlankCell2);

		try {
			document.add(serviceTab1);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Date:16/10/2017 By Jayshre Deascription :this method is created for to
	 * print the customer info part of service card
	 * 
	 * @param salesLineItem
	 */
	private void createCustomerTable(SalesLineItem salesLineItem) {

		// Date 16/11/2017 By jayshree to set the column width
		//salutation part changes
		PdfPTable customernameTab = new PdfPTable(6);
		customernameTab.setWidthPercentage(100f);

		try {
			customernameTab.setWidths(new float[] { 10, 2, 48, 10, 2, 28 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		Phrase msph = new Phrase("Name", font9bold);
		PdfPCell msCell = new PdfPCell(msph);
		msCell.setBorderWidthRight(0);
		msCell.setBorderWidthTop(0);
		msCell.setBorderWidthBottom(0);
		msCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		msCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customernameTab.addCell(msCell);

		Phrase colph = new Phrase(":", font9);
		PdfPCell colCell = new PdfPCell(colph);
		colCell.setBorder(0);
		colCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		colCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customernameTab.addCell(colCell);

//		String tosir = null;
		String custName = "";

		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			custName = cust.getCustPrintableName().trim();
		} else {

			if (cust.isCompany() == true && cust.getCompanyName() != null) {
				custName = "M/S " + cust.getCompanyName().trim();
			} else {
				custName = "M/S " +cust.getFullname().trim();
			}
		}

		String fullname = "";
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			fullname = custName;
		} else {
			fullname = custName;
		}
		
		Phrase custNamePh = new Phrase(fullname, font9);
		PdfPCell custNameCell = new PdfPCell(custNamePh);
		custNameCell.setBorder(0);
		custNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custNameCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customernameTab.addCell(custNameCell);
//end here
		Phrase dateph = new Phrase("Date", font9bold);
		PdfPCell dateCell = new PdfPCell(dateph);
		dateCell.setBorder(0);
		dateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		dateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customernameTab.addCell(dateCell);

		Phrase colph3 = new Phrase(":", font9);
		PdfPCell colCell3 = new PdfPCell(colph3);
		colCell3.setBorder(0);
		colCell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customernameTab.addCell(colCell3);

		Phrase dateValPh = new Phrase(fmt.format(con.getStartDate()) + " "
				+ "to" + " " + fmt.format(con.getEndDate()), font9);
		PdfPCell dateValCell = new PdfPCell(dateValPh);
		dateValCell.setBorderWidthTop(0);
		dateValCell.setBorderWidthBottom(0);
		dateValCell.setBorderWidthLeft(0);
		dateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		dateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customernameTab.addCell(dateValCell);

		try {
			document.add(customernameTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable customerTab1 = new PdfPTable(3);
		customerTab1.setWidthPercentage(100f);

		try {
			customerTab1.setWidths(new float[] { 16, 2, 78 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		Phrase addressph = new Phrase("Address", font9bold);
		PdfPCell addressCell = new PdfPCell(addressph);
		addressCell.setBorderWidthLeft(0);
		addressCell.setBorderWidthRight(0);
		addressCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		addressCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerTab1.addCell(addressCell);

		Phrase colph2 = new Phrase(":", font9);
		PdfPCell colCell2 = new PdfPCell(colph2);
		colCell2.setBorderWidthLeft(0);
		colCell2.setBorderWidthRight(0);
		colCell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerTab1.addCell(colCell2);

		Phrase addressValph = null;

		if (con.getCustomerServiceAddress() != null) {
			addressValph = new Phrase(con.getCustomerServiceAddress()
					.getCompleteAddress(), font9);
		} else {
			addressValph = new Phrase(" ");
		}
		// Phrase addressValph =new
		// Phrase(con.getCustomerServiceAddress()+"",font9);
		PdfPCell addressValCell = new PdfPCell(addressValph);
		addressValCell.setBorderWidthLeft(0);
		addressValCell.setBorderWidthRight(0);
		addressValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		addressValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerTab1.addCell(addressValCell);

		PdfPTable customerTab2 = new PdfPTable(3);
		customerTab2.setWidthPercentage(100f);

		try {
			customerTab2.setWidths(new float[] { 18, 2, 53 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		/** date 03/02/2018 added by komal to print contract number **/

		Phrase contractNoph = new Phrase("Contract No", font9bold);
		PdfPCell contractNoCell = new PdfPCell(contractNoph);
		contractNoCell.setBorderWidthLeft(0);
		contractNoCell.setBorderWidthRight(0);
		contractNoCell.setBorderWidthBottom(0);
		contractNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		customerTab2.addCell(contractNoCell);
		
		//customerTab2.addCell(colCell2);
		Phrase colphase = new Phrase(":", font9);
		PdfPCell columnCell = new PdfPCell(colphase);
		columnCell.setBorderWidthLeft(0);
		columnCell.setBorderWidthRight(0);
		columnCell.setBorderWidthBottom(0);
		columnCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerTab2.addCell(columnCell);
		
		Phrase contractValph = null;
		// System.out.println("invoice22"+invoiceentity.getId());
		if (con != null) {
			if (con.getCount() != 0) {
				System.out.println("Contract" + con.getId());
				contractValph = new Phrase(con.getCount() +" "+ "/"+" "
						+ con.getBranch(), font9);
			}
		} else {
			contractValph = new Phrase(" "+ con.getBranch(), font9);

		}

		PdfPCell contractNoValCell = new PdfPCell(contractValph);
		contractNoValCell.setBorderWidthLeft(0);
		contractNoValCell.setBorderWidthRight(0);
		contractNoValCell.setBorderWidthBottom(0);
		contractNoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		customerTab2.addCell(contractNoValCell);
		/**  
		 * end
		 */
		Phrase billNoph = new Phrase("Bill No", font9bold);
		PdfPCell billNoCell = new PdfPCell(billNoph);
		billNoCell.setBorderWidthLeft(0);
		billNoCell.setBorderWidthRight(0);
		billNoCell.setBorderWidthTop(0);
		billNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		billNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerTab2.addCell(billNoCell);

		
		//customerTab2.addCell(colCell2);
		Phrase columnphase = new Phrase(":", font9);
		PdfPCell columnCell1 = new PdfPCell(columnphase);
		columnCell1.setBorderWidthLeft(0);
		columnCell1.setBorderWidthRight(0);
		columnCell1.setBorderWidthTop(0);
		columnCell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerTab2.addCell(columnCell1);
		// Phrase billNoValph = new Phrase(invoiceentity.getId() + "/" +
		// con.getBranch(), font9);
		Phrase billNoValph = null;
		// System.out.println("invoice22"+invoiceentity.getId());
		if (invoiceentity != null) {
			if (invoiceentity.getCount() != 0) {
				System.out.println("invoice" + invoiceentity.getId());
				/** date 03/02/2018 commented by komal **/
//				billNoValph = new Phrase(invoiceentity.getCount() +" "+ "/"+" "
//						+ con.getBranch(), font9);
				/** date 03/02/2018 added by komal **/
				billNoValph = new Phrase(invoiceentity.getCount() +" ", font9);
			}
		} else {
			/** date 03/02/2018 commented by komal **/
			//billNoValph = new Phrase(" "+ "/"+" "+ con.getBranch(), font9);
			/** date 03/02/2018 added by komal **/
			billNoValph = new Phrase(" ");

		}
		PdfPCell billNoValCell = new PdfPCell(billNoValph);
		billNoValCell.setBorderWidthLeft(0);
		billNoValCell.setBorderWidthRight(0);
		billNoValCell.setBorderWidthTop(0);
		billNoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		billNoValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerTab2.addCell(billNoValCell);

		// Phrase contactPersonph = new Phrase("Contact Person", font9bold);
		// PdfPCell contactPersonCell = new PdfPCell(contactPersonph);
		// contactPersonCell.setBorderWidthLeft(0);
		// contactPersonCell.setBorderWidthRight(0);
		// contactPersonCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// customerTab2.addCell(contactPersonCell);
		//
		// customerTab2.addCell(colCell2);
		//
		// Phrase contactPerValph = new Phrase(contactPerson.getRole(), font9);
		// PdfPCell contactPerValCell = new PdfPCell(contactPerValph);
		// contactPerValCell.setBorderWidthLeft(0);
		// contactPerValCell.setBorderWidthRight(0);
		// contactPerValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// customerTab2.addCell(contactPerValCell);

		PdfPTable customerTable = new PdfPTable(2);
		customerTable.setWidthPercentage(100f);

		try {
			customerTable.setWidths(new float[] { 60, 40 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell custCell1 = new PdfPCell(customerTab1);
		customerTable.addCell(custCell1);

		PdfPCell custCell2 = new PdfPCell(customerTab2);
		customerTable.addCell(custCell2);

		try {
			document.add(customerTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable customerContTab = new PdfPTable(3);
		customerContTab.setWidthPercentage(100f);

		try {
			customerContTab.setWidths(new float[] { 10, 2, 88 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		Phrase telNoph = new Phrase("Tel No", font9bold);
		PdfPCell telNoCell = new PdfPCell(telNoph);
		telNoCell.setBorderWidthRight(0);
		telNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		telNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerContTab.addCell(telNoCell);

		customerContTab.addCell(colCell2);
		String cellNo = "";
		if (contactList.size() == 0) {

			cellNo = cust.getCellNumber1() + "";
		} else {
			for (int i = 0; i < contactList.size(); i++) {
				if (i != 0) {
					if (i + 1 != 0) {

						cellNo = cellNo + "," + contactList.get(i).getName()
								+ "-" + contactList.get(i).getCell() + "("
								+ contactList.get(i).getRole()+")";

					} else {
						cellNo = cellNo + "&" + contactList.get(i).getName()
								+ "-" + contactList.get(i).getCell() + "("
								+ contactList.get(i).getRole()+")";
					}
				} else {
					cellNo = contactList.get(i).getName() + "-"
							+ contactList.get(i).getCell() + "("
							+ contactList.get(i).getRole()+")";
				}
			}
		}
		Phrase telValph = new Phrase(cellNo, font9);
		PdfPCell telValCell = new PdfPCell(telValph);
		telValCell.setBorderWidthLeft(0);
		telValCell.setColspan(4);
		telValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		telValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerContTab.addCell(telValCell);

		try {
			document.add(customerContTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable customerContTab2 = new PdfPTable(6);
		customerContTab2.setWidthPercentage(100f);

		try {
			customerContTab2.setWidths(new float[] { 15, 2, 43, 10, 2, 29 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		Phrase areaph = new Phrase("Contract Group", font9bold);
		PdfPCell areaCell = new PdfPCell(areaph);
		areaCell.setBorderWidthRight(0);
		areaCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		areaCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerContTab2.addCell(areaCell);

		customerContTab2.addCell(colCell2);

		Phrase areaValph = new Phrase(con.getGroup(), font9);
		PdfPCell areaValCell = new PdfPCell(areaValph);
		areaValCell.setBorderWidthLeft(0);
		areaValCell.setBorderWidthRight(0);
		areaValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		areaValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerContTab2.addCell(areaValCell);

		Phrase emailph = new Phrase("E-Mail Id", font9bold);
		PdfPCell emailCell = new PdfPCell(emailph);
		emailCell.setBorderWidthLeft(0);
		emailCell.setBorderWidthRight(0);
		emailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerContTab2.addCell(emailCell);

		customerContTab2.addCell(colCell2);

		Phrase emailValph = new Phrase(cust.getEmail(), font9);
		PdfPCell emailValCell = new PdfPCell(emailValph);
		emailValCell.setBorderWidthLeft(0);
		emailValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerContTab2.addCell(emailValCell);

		try {
			document.add(customerContTab2);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//Date 16/11/2017 by jayshree changes are made to change the position of rows and percentage wdth
		PdfPTable premiseInfoTab = new PdfPTable(3);
		premiseInfoTab.setWidthPercentage(100f);

		try {
			premiseInfoTab.setWidths(new float[] { 15, 2, 83 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		Phrase premisph = new Phrase("Type Of Premises ", font9bold);
		PdfPCell premisCell = new PdfPCell(premisph);
		premisCell.setBorderWidthRight(0);
		premisCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		premisCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		premiseInfoTab.addCell(premisCell);

		premiseInfoTab.addCell(colCell2);

		Phrase premisValph = new Phrase(salesLineItem.getPremisesDetails(),
				font9);
		PdfPCell premisValCell = new PdfPCell(premisValph);
		premisValCell.setBorderWidthLeft(0);
		premisValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		premisValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		premiseInfoTab.addCell(premisValCell);

		Phrase insectph = new Phrase("Insects", font9bold);
		PdfPCell insectCell = new PdfPCell(insectph);
		insectCell.setBorderWidthRight(0);
		insectCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		insectCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		premiseInfoTab.addCell(insectCell);

		premiseInfoTab.addCell(colCell2);

		Phrase insectValph=null;
		if(salesLineItem.getPrduct().getProductType()!=null){
		 insectValph = new Phrase(salesLineItem.getPrduct().getProductType(), font9);
		}
		else
		{
			insectValph = new Phrase(" ", font9);
		}
		PdfPCell insectValCell = new PdfPCell(insectValph);
		insectValCell.setBorderWidthLeft(0);
		insectValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		insectValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		premiseInfoTab.addCell(insectValCell);

		try {
			document.add(premiseInfoTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable typeOfTreatmentTab = new PdfPTable(3);
		typeOfTreatmentTab.setWidthPercentage(100f);

		try {
			typeOfTreatmentTab.setWidths(new float[] {  15, 2, 83 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		Phrase typeoftreatph = new Phrase("Type Of treatment ", font9bold);
		PdfPCell typeoftreatCell = new PdfPCell(typeoftreatph);
		typeoftreatCell.setBorderWidthRight(0);
		typeoftreatCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		typeoftreatCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		typeOfTreatmentTab.addCell(typeoftreatCell);

		typeOfTreatmentTab.addCell(colCell2);

		Phrase typeoftreatValph = new Phrase(salesLineItem.getProductName(),
				font9);
		PdfPCell typeoftreatValCell = new PdfPCell(typeoftreatValph);
		typeoftreatValCell.setBorderWidthLeft(0);
		typeoftreatValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		typeOfTreatmentTab.addCell(typeoftreatValCell);

		try {
			document.add(typeOfTreatmentTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//End here
	}

	private void createTreatMentHeader() {

		PdfPTable treatmentHeadTab = new PdfPTable(6);
		treatmentHeadTab.setWidthPercentage(100f);

		try {
			treatmentHeadTab.setWidths(new float[] { 7, 10, 10, 33, 20, 20 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		Phrase srNoph = new Phrase("Sr.No", font9bold);
		PdfPCell srNoCell = new PdfPCell(srNoph);
		srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		srNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		srNoCell.setFixedHeight(15);
		treatmentHeadTab.addCell(srNoCell);

		Phrase dateph = new Phrase("Date", font9bold);
		PdfPCell dateCell = new PdfPCell(dateph);
		dateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		dateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		dateCell.setFixedHeight(15);
		treatmentHeadTab.addCell(dateCell);

		Phrase timeph = new Phrase("Time", font9bold);
		PdfPCell timeCell = new PdfPCell(timeph);
		timeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		timeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		timeCell.setFixedHeight(15);
		treatmentHeadTab.addCell(timeCell);

		Phrase treatmentph = new Phrase("Treatment", font9bold);
		PdfPCell treatmentCell = new PdfPCell(treatmentph);
		treatmentCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		treatmentCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		treatmentCell.setFixedHeight(15);
		treatmentHeadTab.addCell(treatmentCell);

		Phrase custSignph = new Phrase("Cust.Sign", font9bold);
		PdfPCell custSignCell = new PdfPCell(custSignph);
		custSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		custSignCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		custSignCell.setFixedHeight(15);
		treatmentHeadTab.addCell(custSignCell);

		Phrase remarkph = new Phrase("Remark", font9bold);
		PdfPCell remarkCell = new PdfPCell(remarkph);
		remarkCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		remarkCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		remarkCell.setFixedHeight(15);
		treatmentHeadTab.addCell(remarkCell);

		try {
			document.add(treatmentHeadTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createTreatmentDetailTable(int startingProductIndex, SalesLineItem salesLineItem) {
		// TODO Auto-generated method stub

		 int srcount=0;
		 int itemSize=0;
		 
		 boolean serviceGreaterThanTwelve=false;
		 System.out.println("starting product"+startingProductIndex);
		 int matchedProduct=0;
		 for (int i = startingProductIndex; i < con.getServiceScheduleList().size(); i++) {
			 System.out.println("startingProductIndex b4 matched"+i);
			 srcount=srcount+1;
			if ((salesLineItem.getPrduct().getCount() == con
					.getServiceScheduleList().get(i).getScheduleProdId())&&(salesLineItem.getProductSrNo() == con
							.getServiceScheduleList().get(i).getSerSrNo())) {
//				itemSize=con.getServiceScheduleList().get(i).getScheduleNoOfServices(); 
				System.out.println("productname"+con.getServiceScheduleList().get(i).getScheduleProdName());
				System.out.println("servicecount"+servicecount);
				System.out.println("outif"+i);
				
				System.out.println("matched"+matchedProduct);
				if(matchedProduct+1==13)
				{
					System.out.println("insideif"+i);
					itemSize=i;
					serviceGreaterThanTwelve=true;
					break;
				}
				
				PdfPTable treatmentdetailTab = new PdfPTable(6);
				treatmentdetailTab.setWidthPercentage(100f);

				try {
					treatmentdetailTab.setWidths(new float[] { 7, 10, 10, 33, 20, 20  });
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}

				
//				for (int i = 0; i < con.getServiceScheduleList().size(); i++) {
//					System.out.println("inside for" + i);
//					if ((i + 1) == 13) {
//						System.out.println("inside if " + count);
//						count = i;
//						break;
//					}

				
					Phrase srNoValph = new Phrase(con.getServiceScheduleList().get(i).getScheduleServiceNo()+"",font9bold);
					PdfPCell srNoValCell = new PdfPCell(srNoValph);
					srNoValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					srNoValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					srNoValCell.setFixedHeight(15);
					treatmentdetailTab.addCell(srNoValCell);

					Phrase dateValph = new Phrase(" ", font9bold);
					PdfPCell dateValCell = new PdfPCell(dateValph);
					dateValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					dateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					dateValCell.setFixedHeight(15);
					treatmentdetailTab.addCell(dateValCell);

					Phrase timeValph = new Phrase(" ", font9bold);
					PdfPCell timeValCell = new PdfPCell(timeValph);
					timeValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					timeValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					timeValCell.setFixedHeight(15);
					treatmentdetailTab.addCell(timeValCell);

					Phrase treatmenValph = new Phrase(con
							.getServiceScheduleList().get(i)
							.getScheduleServiceNo()
							+ " " + "Service", font9bold);
					PdfPCell treatmenValCell = new PdfPCell(treatmenValph);
					treatmenValCell
							.setHorizontalAlignment(Element.ALIGN_CENTER);
					treatmenValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					treatmenValCell.setFixedHeight(15);
					treatmentdetailTab.addCell(treatmenValCell);

					Phrase custSignValph = new Phrase(" ", font9bold);
					PdfPCell custSignValCell = new PdfPCell(custSignValph);
					custSignValCell
							.setHorizontalAlignment(Element.ALIGN_CENTER);
					custSignValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					custSignValCell.setFixedHeight(15);
					treatmentdetailTab.addCell(custSignValCell);

					Phrase remarkValph = new Phrase(
							con.getServiceScheduleList().get(i)
									.getServiceRemark(), font9bold);
					PdfPCell remarkValCell = new PdfPCell(remarkValph);
					remarkValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					remarkValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					remarkValCell.setFixedHeight(15);
					treatmentdetailTab.addCell(remarkValCell);

//					
//					System.out.println("con.getServiceScheduleList()"+con.getServiceScheduleList().get(i)
//								.getScheduleNoOfServices());
					
					
					 
					
//						 System.out.println("con.getServiceScheduleList()222"+con.getServiceScheduleList().get(i)
//									.getScheduleNoOfServices());
////							 for (int j = 0; j <itemSize-12; j++) {
//								 	Phrase blankph = new Phrase(" ");
//									PdfPCell blankCell = new PdfPCell(blankph);
//									treatmentdetailTab.addCell(blankCell);
//									treatmentdetailTab.addCell(blankCell);
//									treatmentdetailTab.addCell(blankCell);
//									treatmentdetailTab.addCell(blankCell);
//									treatmentdetailTab.addCell(blankCell);
//									treatmentdetailTab.addCell(blankCell);
//									
//								 
//							}
						 
				try {
					document.add(treatmentdetailTab);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				matchedProduct=matchedProduct+1;
		 }
			
			 
	}
	if(serviceGreaterThanTwelve){
		crateFooterTable();
		try {
			document.add(Chunk.NEXTPAGE);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		System.out.println("remaining product"+itemSize);
		servicecount=itemSize;
		createHeaderTable(salesLineItem);
		createCustomerTable(salesLineItem);
		createTreatMentHeader();
		createTreatmentDetailTable(itemSize, salesLineItem);
		
//		
//		PdfPTable ProductDetail1 = new PdfPTable(1);
//		ProductDetail1.setWidthPercentage(100f);
//		Phrase indicate = new Phrase(
//				"Please Refere Annexure For More Details ", font9);
//		PdfPCell indicateCell = new PdfPCell(indicate);
//		indicateCell.setVerticalAlignment(Element.ALIGN_LEFT);
//		ProductDetail1.addCell(indicateCell);
//		
//		try {
//			document.add(ProductDetail1);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}else{
	 if(matchedProduct<12)
	 {
//		System.out.println("itemSize"+itemSize);
		PdfPTable treatmentdetailTab2 = new PdfPTable(6);
		treatmentdetailTab2.setWidthPercentage(100f);
	
			try {
				treatmentdetailTab2.setWidths(new float[] { 7, 10, 10, 33, 20, 20  });
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
		 
			 for (int j = 0; j < (12-matchedProduct); j++) {
//				System.out.println("insidefor" +itemSize);
			
			 	Phrase blankph = new Phrase(" ");
				PdfPCell blankCell = new PdfPCell(blankph);
				blankCell.setFixedHeight(15);
				treatmentdetailTab2.addCell(blankCell);
				treatmentdetailTab2.addCell(blankCell);
				treatmentdetailTab2.addCell(blankCell);
				treatmentdetailTab2.addCell(blankCell);
				treatmentdetailTab2.addCell(blankCell);
				treatmentdetailTab2.addCell(blankCell);
		 }
		 
		 try {
				document.add(treatmentdetailTab2);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }
	 	crateFooterTable();
	}
}

	private void crateFooterTable() {

		PdfPTable footerTab1 = new PdfPTable(1);
		footerTab1.setWidthPercentage(100f);
		footerTab1.setSpacingAfter(5);

		Phrase compNameph = new Phrase("For " + comp.getBusinessUnitName(),
				font9bold);
		PdfPCell compNameCell = new PdfPCell(compNameph);
		compNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		compNameCell.setBorder(0);
		footerTab1.addCell(compNameCell);

		Phrase blankph = new Phrase(" ",font7);
		PdfPCell blankCell = new PdfPCell(blankph);
		blankCell.setBorder(0);
		blankCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		footerTab1.addCell(blankCell);

//		footerTab1.addCell(blankCell);//comment by jayshree

		Phrase authorisedph = new Phrase("Manager/Auth.Signatory"+" ", font7bold);
		PdfPCell authorisedCell = new PdfPCell(authorisedph);
		authorisedCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		authorisedCell.setBorder(0);
		footerTab1.addCell(authorisedCell);

		PdfPTable footerTab2 = new PdfPTable(1);
		footerTab2.setWidthPercentage(100f);

		Phrase netpayble = new Phrase("Rs." + df.format(con.getNetpayable()),
				font8bold);
		PdfPCell netpaybleCell = new PdfPCell(netpayble);
		netpaybleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		netpaybleCell.setBorder(0);
		footerTab2.addCell(netpaybleCell);
		// Date 16/11/2017
		// By jayshree comment the code to remove the branch list
		
//		String branch=" ";
//		
//		System.out.println("branclist"+branchList.size());
//		for (int i = 0; i <branchList.size(); i++) 
//		{
//			if(i!=0){
//				if(i+1!=0)
//				{
//					if(branchList.get(i).getLandline()!=0){
//					branch= branch +" "+","+" "+branchList.get(i).getBusinessUnitName()+":"
//							+branchList.get(i).getLandline();
//					}
//					else
//					{
//						branch= branch +" "+","+" "+branchList.get(i).getBusinessUnitName()+":"
//								+branchList.get(i).getCellNumber1();
//					}
//				}
//			}
//			else
//			{
//				if(branchList.get(i).getLandline()!=0)
//				{
//					branch= branchList.get(i).getBusinessUnitName()+":"
//						+branchList.get(i).getLandline();
//				}
//				else
//				{
//					branch= branchList.get(i).getBusinessUnitName()+":"
//							+branchList.get(i).getCellNumber1();
//				}
//			}
//			
//			
//	
//		}
//		
//		Phrase servicediv = new Phrase("SERVICE DIVISION "+branch,
//				font8bold);
//		PdfPCell  servicedivCell = new PdfPCell(servicediv);
//		servicedivCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		servicedivCell.setBorder(0);
//		footerTab2.addCell(servicedivCell);
		
		
		
	
//		PdfPTable footerTab3 = new PdfPTable(1);
//		footerTab3.setWidthPercentage(100f);
		
		
		
//		footerTab3.addCell(blankCell);
//		
//		Phrase branchph = new Phrase(branch,font8bold);
//		PdfPCell branchCell = new PdfPCell(branchph);
//		branchCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		branchCell.setBorder(0);
//		footerTab3.addCell(branchCell);
		
		
//End by jayshree
		

		PdfPTable footerouterTab = new PdfPTable(2);
		footerouterTab.setWidthPercentage(100f);

		try {
			footerouterTab.setWidths(new float[] { 65, 35 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell footerouter1 = new PdfPCell(footerTab2);
		footerouter1.setBorderWidthRight(0);
		footerouterTab.addCell(footerouter1);
//By Jayshree to remove the branch details
		
//		PdfPCell footerouter2 = new PdfPCell(footerTab3);
//		footerouter2.setBorderWidthRight(0);
//		footerouter2.setBorderWidthLeft(0);
//		footerouterTab.addCell(footerouter2);
		
		//ends
		
		PdfPCell footerouter3 = new PdfPCell(footerTab1);
		footerouter3.setBorderWidthLeft(0);
		footerouterTab.addCell(footerouter3);

		try {
			document.add(footerouterTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

//	private void createAnextureTreatment(int productcount) {
//		// TODO Auto-generated method stub
//
//		if (servicecount != 0) {
//			try {
//				document.add(Chunk.NEXTPAGE);
//			} catch (DocumentException e2) {
//				// TODO Auto-generated catch block
//				e2.printStackTrace();
//			}
//
//			try {
//				document.add(new Phrase(" ", font9bold));
//			} catch (DocumentException e2) {
//				// TODO Auto-generated catch block
//				e2.printStackTrace();
//			}
//
//			for (int i = 0; i < con.getItems().size(); i++) {
//				createHeaderTable(con.getItems().get(i));
//				createCustomerTable(con.getItems().get(i));
//				createTreatMentHeader();
//
//				PdfPTable treatmentdetailTab = new PdfPTable(6);
//				treatmentdetailTab.setWidthPercentage(100f);
//
//				try {
//					treatmentdetailTab.setWidths(new float[] { 7, 10, 10, 33,
//							20, 20 });
//				} catch (DocumentException e1) {
//					e1.printStackTrace();
//				}
//
//				for (int j = productcount; j < con.getServiceScheduleList()
//						.size(); j++) {
//					Phrase srNoValph = new Phrase(con.getServiceScheduleList()
//							.get(j).getScheduleServiceNo()
//							+ "", font9bold);
//					PdfPCell srNoValCell = new PdfPCell(srNoValph);
//					srNoValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//					treatmentdetailTab.addCell(srNoValCell);
//
//					Phrase dateValph = new Phrase(sdf.format(con
//							.getServiceScheduleList().get(j)
//							.getScheduleServiceDate()), font9bold);
//					PdfPCell dateValCell = new PdfPCell(dateValph);
//					dateValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//					treatmentdetailTab.addCell(dateValCell);
//
//					Phrase timeValph = new Phrase(con.getServiceScheduleList()
//							.get(j).getScheduleDuration()
//							+ "", font9bold);
//					PdfPCell timeValCell = new PdfPCell(timeValph);
//					timeValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//					treatmentdetailTab.addCell(timeValCell);
//
//					Phrase treatmenValph = new Phrase(con
//							.getServiceScheduleList().get(j)
//							.getScheduleServiceNo()
//							+ " " + "Service", font9bold);
//					PdfPCell treatmenValCell = new PdfPCell(treatmenValph);
//					treatmenValCell
//							.setHorizontalAlignment(Element.ALIGN_CENTER);
//					treatmentdetailTab.addCell(treatmenValCell);
//
//					Phrase custSignValph = new Phrase(" ", font9bold);
//					PdfPCell custSignValCell = new PdfPCell(custSignValph);
//					custSignValCell
//							.setHorizontalAlignment(Element.ALIGN_CENTER);
//					treatmentdetailTab.addCell(custSignValCell);
//
//					Phrase remarkValph = new Phrase(
//							con.getServiceScheduleList().get(j)
//									.getServiceRemark(), font9bold);
//					PdfPCell remarkValCell = new PdfPCell(remarkValph);
//					remarkValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//					treatmentdetailTab.addCell(remarkValCell);
//
//				}
//				try {
//					document.add(treatmentdetailTab);
//				} catch (DocumentException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				crateFooterTable();
//			}
//		}
//	}

	// createCustomerTable();
	// createTreatMentHeader();
	//
	//
	// PdfPTable treatmentdetailTab = new PdfPTable(6);
	// treatmentdetailTab.setWidthPercentage(100f);
	//
	// try {
	// treatmentdetailTab.setWidths(new float[] {5,10,10,35,20,20});
	// } catch (DocumentException e1) {
	// e1.printStackTrace();
	// }
	//
	//
	// for (int i = count; i < con.getServiceScheduleList().size(); i++) {
	// System.out.println("second for"+count);
	// Phrase srNoValph =new Phrase(i+1+"",font9bold);
	// PdfPCell srNoValCell=new PdfPCell(srNoValph);
	// srNoValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	// treatmentdetailTab.addCell(srNoValCell);
	//
	// Phrase dateValph =new
	// Phrase(sdf.format(con.getServiceScheduleList().get(i).getScheduleServiceDate()),font9bold);
	// PdfPCell dateValCell=new PdfPCell(dateValph);
	// dateValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	// treatmentdetailTab.addCell(dateValCell);
	//
	//
	// Phrase timeValph =new
	// Phrase(con.getServiceScheduleList().get(i).getScheduleDuration()+"",font9bold);
	// PdfPCell timeValCell=new PdfPCell(timeValph);
	// timeValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	// treatmentdetailTab.addCell(timeValCell);
	//
	//
	// Phrase treatmenValph =new
	// Phrase(con.getServiceScheduleList().get(i).getSerSrNo()+"Services",font9bold);
	// PdfPCell treatmenValCell=new PdfPCell(treatmenValph);
	// treatmenValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	// treatmentdetailTab.addCell(treatmenValCell);
	//
	//
	// Phrase custSignValph =new Phrase(" ",font9bold);
	// PdfPCell custSignValCell=new PdfPCell(custSignValph);
	// custSignValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	// treatmentdetailTab.addCell(custSignValCell);
	//
	//
	// Phrase remarkValph =new
	// Phrase(con.getServiceScheduleList().get(i).getServiceRemark(),font9bold);
	// PdfPCell remarkValCell=new PdfPCell(remarkValph);
	// remarkValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	// treatmentdetailTab.addCell(remarkValCell);
	//
	// }
	//
	// try {
	// document.add(treatmentdetailTab);
	// } catch (DocumentException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// crateFooterTable();
	// }
	// }
	//
	// private void createCompanyNameAsHeader(Document doc, Company comp) {
	//
	// DocumentUpload document = comp.getUploadHeader();
	//
	// // patch
	// String hostUrl;
	// String environment = System
	// .getProperty("com.google.appengine.runtime.environment");
	// if (environment.equals("Production")) {
	// String applicationId = System
	// .getProperty("com.google.appengine.application.id");
	// String version = System
	// .getProperty("com.google.appengine.application.version");
	// hostUrl = "http://" + version + "." + applicationId
	// + ".appspot.com/";
	// } else {
	// hostUrl = "http://localhost:8888";
	// }
	//
	// try {
	// Image image2 = Image.getInstance(new URL(hostUrl
	// + document.getUrl()));
	// image2.scalePercent(15f);
	// image2.scaleAbsoluteWidth(520f);
	// image2.setAbsolutePosition(40f, 725f);
	// doc.add(image2);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	//
	// }

	// private void createCompanyNameAsFooter(Document doc, Company comp) {
	//
	// DocumentUpload document = comp.getUploadFooter();
	//
	// // patch
	// String hostUrl;
	// String environment = System
	// .getProperty("com.google.appengine.runtime.environment");
	// if (environment.equals("Production")) {
	// String applicationId = System
	// .getProperty("com.google.appengine.application.id");
	// String version = System
	// .getProperty("com.google.appengine.application.version");
	// hostUrl = "http://" + version + "." + applicationId
	// + ".appspot.com/";
	// } else {
	// hostUrl = "http://localhost:8888";
	// }
	//
	// try {
	// Image image2 = Image.getInstance(new URL(hostUrl
	// + document.getUrl()));
	// image2.scalePercent(15f);
	// image2.scaleAbsoluteWidth(520f);
	// image2.setAbsolutePosition(40f, 40f);
	// doc.add(image2);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	//
	// }

//	private void CreateRemainingTreatment(SalesLineItem salesLineItem) {
//		// TODO Auto-generated method stub
//
//		if (servicecount != 0) {
//
//			createAnextureTreatment(servicecount);
//
//		}
//	}
	// private void createBlankforUPC() {
	// Phrase blankphrase = new Phrase("", font9);
	// PdfPCell blankCell = new PdfPCell();
	// blankCell.addElement(blankphrase);
	// blankCell.setBorder(0);
	//
	// PdfPTable titlepdftable = new PdfPTable(3);
	// titlepdftable.setWidthPercentage(100);
	// titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
	// titlepdftable.addCell(blankCell);
	// titlepdftable.addCell(blankCell);
	//
	// Paragraph blank = new Paragraph();
	// blank.add(Chunk.NEWLINE);
	//
	// PdfPTable parent = new PdfPTable(1);
	// parent.setWidthPercentage(100);
	//
	// PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
	// titlePdfCell.setBorder(0);
	// parent.addCell(titlePdfCell);
	//
	// try {
	// document.add(blank);
	// document.add(blank);
	// document.add(blank);
	// document.add(blank);
	// document.add(blank);
	// document.add(parent);
	// } catch (DocumentException e) {
	// e.printStackTrace();
	// }
	// }
}
