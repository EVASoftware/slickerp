package com.slicktechnologies.server.addhocprinting.friends;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.addhocprinting.PdfCancelWatermark;
import com.slicktechnologies.server.addhocprinting.PdfWatermark;
import com.slicktechnologies.shared.Contract;

public class CreateServiceCardPdfServlet extends HttpServlet {

	/**
	 * 
	 * @author Jayshree 
	 * Description :For Friends pest control : It will print service card 
	 * 
	 * Date:15/10/2017
	 * Created By : Jayshree Chavhan
	 */
	private static final long serialVersionUID = 1261159833623767032L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		resp.setContentType("application/pdf"); // Type of response , helps
												// browser to identify the
												// response type.
		try {
			String stringid = req.getParameter("Id").trim();
			long count = Long.parseLong(stringid);
			System.out.println("contractid222=="+count);

			Contract con = ofy().load().type(Contract.class).id(count).now();

			FrendsServiceCardPdf frendpdf = new FrendsServiceCardPdf();
			frendpdf.document = new Document();
			Document document = frendpdf.document;
			PdfWriter writer = PdfWriter.getInstance(document,
					resp.getOutputStream());

			if (con.getStatus().equals("Cancelled")) {
				writer.setPageEvent(new PdfCancelWatermark());
			} else if (!con.getStatus().equals("Approved")) {
				writer.setPageEvent(new PdfWatermark());
			}

			document.open();
			frendpdf.loadServiceCard(con, count);
			frendpdf.createPdf();
			document.close();

		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}