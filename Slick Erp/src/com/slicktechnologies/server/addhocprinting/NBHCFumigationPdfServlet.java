package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;

@SuppressWarnings("serial")
public class NBHCFumigationPdfServlet extends HttpServlet{


	@Override
	 protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException 
	{
		
		 response.setContentType("application/pdf");  // Type of response , helps browser to identify the response type.
	  
	  try {
		  
		  String stringid = request.getParameter("Id");
		  stringid=stringid.trim();
		  Long count =Long.parseLong(stringid);
		  
		  NBHCFumigationPdf pdf = new NBHCFumigationPdf();
		  /** 30-10-2017 sagar sore []**/
		  //pdf.document = new Document(PageSize.A4.rotate());
		/**
		 * Date 27/11/2017
		 * By jayshree
		 * Des. To set the page in landscape mode changes are made
		 */
		  String preePrint = request.getParameter("preprint"); 
		  
		  if(preePrint.equals("no")){
			  pdf.document = new Document(PageSize.A4.rotate(),20,20,0,0);//Date 11/12/2017 By jayshree add the parameter to set the margins
		  }else{
			  pdf.document = new Document(PageSize.A4.rotate(),20,20,35,30);//Date 11/12/2017 By jayshree add the parameter to set the margins  
		  }
		  
		  //End By jayshree
		  Document document = pdf.document;
		  
		  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); // write the pdf in response
	 
	   document.open();
	   pdf.getfumigation(count);
	   pdf.createPdf(preePrint);
	   document.close();
		  
	  } 
	  catch (DocumentException e) {
		  e.printStackTrace();
	  }
	 
	}

}
