package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import java_cup.internal_error;

import com.google.apphosting.api.UserServicePb.CreateLogoutURLRequest;
import com.google.gwt.layout.client.Layout.Alignment;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class PcambContractpdf 
{

	public Document document;
	Contract con;
	Company comp;
	Customer cust;
	Invoice invoiceentity;   
	Service ser;
	List<ArticleType> articletype;
	List<BillingDocumentDetails> billingDoc;  
	Date fromDate;
	
	public int j = 0 ;

	private Font font16boldul, font12bold, font8,   font8bold , font11bolred ,
			font12boldul, font10boldul,font16bold, font10, font12,  font10bold,// font9bold,
			font10ul, font9boldul, font14bold,  font7, font7bold, font9,
			font9red, font9boldred, font12boldred, font23 , font20,
			font11,
			font9bold
			;   

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");

	DecimalFormat df = new DecimalFormat("0.00");

	// rohan added this code for printing 2nd page

	int bedbug = 0, termite = 0, woodborer = 0, flycontrol = 0, rodent = 0,
			gipc = 0, other = 0;

	public PcambContractpdf() 
	{  
		super();
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD | Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD |Font.UNDERLINE);
		font11 = new Font(Font.FontFamily.HELVETICA, 11);
		font11bolred = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD,BaseColor.RED);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font12boldred = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,BaseColor.RED);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font10bold = new Font(Font.FontFamily.HELVETICA , 10, Font.BOLD);
		font10ul = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL | Font.UNDERLINE);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD | Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font9boldul = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD | Font.UNDERLINE);
		font9boldred = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD,BaseColor.RED);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9red = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL,BaseColor.RED);
		font7 = new Font(Font.FontFamily.HELVETICA, 7);
		font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
		font23 = new Font(Font.FontFamily.HELVETICA, 23);
		font20 = new Font(Font.FontFamily.HELVETICA, 20);

	}

	public void setContract(Long count) 
	{
		// Load Contract

		con = ofy().load().type(Contract.class).id(count).now();   

		// Load Invoice
		if (con.getCompanyId() == null)
			invoiceentity = ofy().load().type(Invoice.class).first().now();       
			// load Customer;
		else
			invoiceentity = ofy().load().type(Invoice.class).filter("companyId", con.getCompanyId()).first().now();
		
		// load Customer
		if (con.getCompanyId() == null)
			cust = ofy().load().type(Customer.class).filter("count", con.getCustomerId()).first().now();
		else
			cust = ofy().load().type(Customer.class).filter("companyId", con.getCompanyId()).filter("count", con.getCustomerId()).first().now();

		// Load Company
		if (con.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class).filter("companyId", con.getCompanyId()).first().now();  
   
		// load Service
		
		if (con.getCompanyId() == null)
			ser = ofy().load().type(Service.class).first().now();
		else
			
		System.out.println("contract Count "+con.getCount()+"-"+con.getCompanyId());
		ser = ofy().load().type(Service.class).filter("companyId", con.getCompanyId()).filter("contractCount", con.getCount()).first().now();
	
//		System.out.println("service entity "+ser.getAddrLine1());
		////////////////////////////////////////
		
		///////////////////////////////////////
		
		// Load Article type Details  
		articletype = new ArrayList<ArticleType>();
		if (cust.getArticleTypeDetails().size() != 0) {
			articletype.addAll(cust.getArticleTypeDetails());
		}
		if (comp.getArticleTypeDetails().size() != 0) {
			articletype.addAll(comp.getArticleTypeDetails());  
		}
		
		System.out.println("prod Code outside for loop ");
		
		// rohan added this
		for (int i = 0; i < con.getItems().size(); i++) {
			System.out.println("prod Code "+ con.getItems().get(i).getProductCode());

	       if (con.getItems().get(i).getProductCode().equalsIgnoreCase("BT")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT02")) {
			      System.out.println("inside bedbugs");
			     bedbug = bedbug + 1;
			} else if (con.getItems().get(i).getProductCode().equalsIgnoreCase("WB")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WB00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WB365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WB52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WB36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WB24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WB12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WB06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WB04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WB03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WB02")) {
		    	   System.out.println("inside wood borer ");
				woodborer = woodborer + 1;
			} else if (con.getItems().get(i).getProductCode().equalsIgnoreCase("RC")||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC02")) {
		    	   System.out.println("inside rodent ");
				rodent = rodent + 1;
			} else if (con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC02")) {
		    	   System.out.println("inside GIPC ");
				gipc = gipc + 1;
			}     
			
			/// ajinkya Added this to update termite and fly mosquito
			       
			 else if (con.getItems().get(i).getProductCode().equalsIgnoreCase("WA")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA02")) {
		    	   System.out.println(" inside Termite ");
				 termite = termite + 1;
				}
			
			 else if (con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY02")) {
		    	   System.out.println("inside FLY");
				 flycontrol = flycontrol + 1;
				}
			 
			 else if (con.getItems().get(i).getProductCode().equalsIgnoreCase("MO")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO02")) {
		    	   System.out.println("inside MOS");
				 flycontrol = flycontrol + 1;
				}
			 else{
		    	   System.out.println("Else Condition ");
    			   other = other +1 ;
			 }
		}      
		// ends here    
	}  

	public void createPdf() 
	{

		//	 createLogo(document,comp); 
		System.out.println("for loop not printed");
		
		for (int i=0 ; i<2 ; i++)
			
		{
			j= i;

			createHeader();
			createAckcopy();
			createBlankforWrite(); 
			createTblFrmInfo();
			getamountNpaterms();  
			createfooter();
			System.out.println("i = "+ i);
			
					}
           System.out.println("J is now "+ j );
		
			createContractAddressPage();

		
		// 2nd page bedbug,termite,woodborer,flycontrol,rodent,gipc
		
		if (bedbug == 1) 
		{
			
			System.out.println("Now printing bedbugs T&C ");
			// /////////////////////////////// 3rd Page Bed Bugs mgnt methods   ////////////////////////////////////////
			createbbHeaderTable();
			createbbTermsAndConditions();
			createFooterTable();
		}
		 if (woodborer == 1) 
		 {
			System.out.println("Now printing WoodBorer T&C ");
			
			// /////////////////////////////// 2nd page wood Borer mgnt 30/12/2016 ////////////////////////////////////
//			createLogo(document,comp);
			createwbHeaderTable();
			createwbTermsAndConditions();
			createFooterTable();
		}

		 if (gipc == 1) 
		 {
			System.out.println("Now printing GIPC T&C ");
			// /////////////////////////////// 6th Page General insect mgnt methods ////////////////////////////////////////
			createGipHeaderTable();
			createGipTermsAndConditions();
			createFooterTable();
		}

		 if (rodent == 1) 
		 {
			System.out.println("Now printing Rodent T&C  ");
			createrodentHeaderTable();
			createrodentTermsAndConditions();
			createFooterTable();
		}

		// ///////////////////////////////// 4th Page termite mgnt methods ////////////////////////////////////////
		 if (termite == 1) {
			System.out.println("Now printing termite T&C ");
			
		createtmHeaderTable();
		 createtmTermsAndConditions();
		 createFooterTable();
		}
		// ///////////////////////////////// 5th Page fly insect mgnt methods ////////////////////////////////////////
		 
		 if (flycontrol == 1) {
			System.out.println("Now printing Fly & Mos T&C");
			
		createfipHeaderTable();
		 createfipTermsAndConditions();
		 createFooterTable();
		}
		if  (other == 1) {                      //  (other > 1)
			
			System.out.println("Now printing general T&C");
			
			createHeaderTable();
			createTermsAndConditions();
			createFooterTable();
			
		}  
	}

	
//	private void createLogo(Document doc, Company comp) {
//		DocumentUpload document = comp.getLogo();
//
//		// patch
//		String hostUrl;
//		String environment = System.getProperty("com.google.appengine.runtime.environment");
//		if (environment.equals("Production")) {
//			String applicationId = System.getProperty("com.google.appengine.application.id");
//			String version = System.getProperty("com.google.appengine.application.version");
//			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
//		} else {
//			hostUrl = "http://localhost:8888";
//		}
//               
//		
//		// try
//				// {
//				// Image image1=Image.getInstance("images/iso9001e.png");
//				// image1.scalePercent(35f);
//				// image1.scaleAbsoluteWidth(40f);
//				// image1.setAbsolutePosition(515f,740f);
//				// document.add(image1);
//				// }
//				// catch(Exception e)
//				// {
//				// e.printStackTrace();
//				// }
//				//
//		
//		try {
//			Image image1 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
//			image1.scalePercent(35f);
//			image1.setAbsolutePosition(515f,740f);
//			doc.add(image1);
//		} catch (Exception e) 
//		{
//			e.printStackTrace();
//		}
//		
//		//      try
//				// {
//				// Image image1=Image.getInstance("images/ISO14001e.png");
//				// image1.scalePercent(35f);
//				// image1.scaleAbsoluteWidth(40f);
//				// image1.setAbsolutePosition(35f,740f);
//				// document.add(image1);
//				// }
//				// catch(Exception e)
//				// {
//				// e.printStackTrace();
//				// }
//		
//		try {
//			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
//			image2.scalePercent(35f);
//			image2.scaleAbsoluteWidth(40f);
//			image2.setAbsolutePosition(35f,740f);
//			doc.add(image2);
//		} catch (Exception e) 
//		{
//			e.printStackTrace();
//		}
//		
//		
//		                 //////////// image logo code added here ////////////////
//		
//		// try
//		// {
//		// Image image1=Image.getInstance("images/ipca.png");
//		// image3.scalePercent(30f);
//		// image3.scaleAbsoluteWidth(70f);
//		// image3.setAbsolutePosition(50f,65f);
//		// document.add(image3);
//		// }
//		// catch(Exception e)
//		// {
//		// e.printStackTrace();
//		// }
//		              
//		                     //////////////////// image logo code end here ///////////////////////
//		
//		try {
//			Image image3 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
//			image3.scalePercent(30f);
//			image3.scaleAbsoluteWidth(70f);
//			image3.setAbsolutePosition(50f,65f);
//			doc.add(image3);
//		} catch (Exception e) 
//		{
//			e.printStackTrace();
//		}
//		
//		                             //// image logo code added here //
//				// try
//				// {
//				// Image image1=Image.getInstance("images/NPMA.png");
//				// image4.scalePercent(30f);
//				// image4.scaleAbsoluteWidth(70f);
//				// image4.setAbsolutePosition(475f,65f);
//				// document.add(image4);
//				// }
//				// catch(Exception e)
//				// {
//				// e.printStackTrace();
//				// }
//				//
//				// // image logo code end here //
//		
//		try {
//			Image image4 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
//			image4.scalePercent(35f);
//			image4.scaleAbsoluteWidth(40f);
//			image4.setAbsolutePosition(475f,65f);
//			doc.add(image4);
//		} catch (Exception e) 
//		{
//			e.printStackTrace();
//		}
//		
//	}

	private void createHeader() 
	 {
		// Ajinkya code 8\12\2016

		String addressline1 = "";

		if (comp.getAddress().getAddrLine2() != null) {
			addressline1 = comp.getAddress().getAddrLine1() + ", "
					     + comp.getAddress().getAddrLine2()+", ";
		} else
		{
			addressline1 = comp.getAddress().getAddrLine1()+", ";
		}

		String locality = null;
		
		if ((!comp.getAddress().getLandmark().equals(""))
				&& (comp.getAddress().getLocality().equals("") == false))
		{
			System.out.println("inside both null condition1");
			locality = (comp.getAddress().getLandmark() + ", "
					+ comp.getAddress().getLocality() + ", "
					+ comp.getAddress().getCity() + " - "
					+ comp.getAddress().getPin() + ", "
					+ comp.getAddress().getState() + ", " + comp.getAddress()
					.getCountry()+". ");
		} 
		else if ((!comp.getAddress().getLandmark().equals(""))
				&& (comp.getAddress().getLocality().equals("") == true)) 
		{
			System.out.println("inside both null condition 2");
			locality = (comp.getAddress().getLandmark() + ", "
					+ comp.getAddress().getCity() + " - "
					+ comp.getAddress().getPin() + ", "
					+ comp.getAddress().getState() + ", " + comp.getAddress()
					.getCountry()+". ");
		}

		else if ((comp.getAddress().getLandmark().equals(""))
				&& (comp.getAddress().getLocality().equals("") == false))
		{
			System.out.println("inside both null condition 3");
			locality = (comp.getAddress().getLocality() + ", "
					+ comp.getAddress().getCity() + " - "
					+ comp.getAddress().getPin() + ", "
					+ comp.getAddress().getState() + ", " + comp.getAddress()
					.getCountry()+". ");
		} else if ((comp.getAddress().getLandmark().equals(""))
				&& (comp.getAddress().getLocality().equals("") == true)) {
			System.out.println("inside both null condition 4");
			locality = (comp.getAddress().getCity() + " - "
					+ comp.getAddress().getPin() + ", "
					+ comp.getAddress().getState() + ", " + comp.getAddress()
					.getCountry()+". ");
		}
		
		String contactinfo = "";
		 System.out.println("landline no "+comp.getLandline());
		 System.out.println("Cell no1 "+comp.getCellNumber1());
		 System.out.println("Cell no2 "+comp.getCellNumber2());
		 System.out.println("fax no "+comp.getFaxNumber());
		 
//		if (comp.getLandline()!=0 && comp.getCellNumber2()!= 0  && comp.getFaxNumber()!=null )
//		{
//			// tel no Format 
//			contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getCellNumber2() +" / "+ comp.getLandline() + " SERVICE DEPT.: 2351 4360"
//					 + "  Fax : " + comp.getFaxNumber());
//		}
//		else if (comp.getLandline() == 0 && comp.getCellNumber2() != 0 && comp.getFaxNumber()!=null ) 
//		{
//			contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getCellNumber2()  + " SERVICE DEPT.: 2351 4360"
//					+ "  Fax : " + comp.getFaxNumber());
//		}
//		else if(comp.getLandline() != 0  && comp.getCellNumber2() == 0 && comp.getFaxNumber()!=null ){
//			contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getLandline() + " SERVICE DEPT.: 2351 4360"
//					 + "  Fax : " + comp.getFaxNumber());
//		}
//		else if(comp.getLandline() != 0 && comp.getCellNumber2() != 0 && comp.getFaxNumber()==null)
//		{
//			contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getCellNumber2()+" / "+ comp.getLandline() + " SERVICE DEPT.: 2351 4360"
//					+ "  Fax : "  );	
//		}
//		else if(comp.getLandline() != 0 && comp.getCellNumber2() == 0 && comp.getFaxNumber()==null )
//		{
//			contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getLandline() + " SERVICE DEPT.: 2351 4360"
//					+ "  Fax : "  );	
//		}
//		else if(comp.getLandline() == 0 && comp.getCellNumber2() != 0 && comp.getFaxNumber()==null )
//		{
//			contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getCellNumber2() + " SERVICE DEPT.: 2351 4360"
//					 + "  Fax : "  );	
//		}
//		else if(comp.getLandline() == 0 && comp.getCellNumber2() == 0 && comp.getFaxNumber()==null)
//		{
//			contactinfo = ("Tel. : "  + comp.getCellNumber1() + " SERVICE DEPT.: 2351 4360"
//					 + "  Fax : "  );	
//		}
		
		Phrase mycomHeader = new Phrase(comp.getBusinessUnitName()
				.toUpperCase(), font20);
		Phrase header1 = new Phrase("Regd. Office : "+addressline1 + locality, font9);
		Phrase header2 = new Phrase("Tel.: (91-22) 2266 1091, 2262 5376 Fax : (91-22) 2266 0810 ", font9);
		Phrase header3 = new Phrase(" E-Mail : " + comp.getEmail().trim()+ " Website : " + comp.getWebsite(), font10);
		
		Phrase header4 = new Phrase(" CUSTOMER COPY ", font11bolred);
		Phrase header5 = new Phrase(" ACKNOWLEDGEMENT COPY ", font11bolred);
		
		
		Paragraph mycomPara = new Paragraph();
		mycomPara.add(mycomHeader);
		mycomPara.add(Chunk.NEWLINE); 
		mycomPara.add(header1);
		mycomPara.add(Chunk.NEWLINE);
		mycomPara.add(header2);
		mycomPara.add(Chunk.NEWLINE);
		mycomPara.add(header3);  
		mycomPara.add(Chunk.NEWLINE);
		mycomPara.add(Chunk.NEWLINE);
		
if (j==0)
{
	    System.out.println("j in cust");
	    mycomPara.add(header4);
	    mycomPara.add(Chunk.NEWLINE);
	    mycomPara.add(Chunk.NEWLINE);
			
}

else if (j==1)
{
	System.out.println("j in ACK");
	mycomPara.add(header5);
	mycomPara.add(Chunk.NEWLINE);
	mycomPara.add(Chunk.NEWLINE);
}
else if (j==2){
	System.out.println("j in ACK");
	mycomPara.add(header5);
	mycomPara.add(Chunk.NEWLINE);
		
	}
else
 {
	mycomPara.add(Chunk.NEWLINE);
	mycomPara.add(Chunk.NEWLINE);
	
}
		mycomPara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell mycomCell = new PdfPCell();
		mycomCell.addElement(mycomPara);
		mycomCell.setBorder(0);
		PdfPTable parentTbl = new PdfPTable(1);
		parentTbl.setWidthPercentage(100);
		parentTbl.addCell(mycomCell);

		try 
		{
			document.add(parentTbl);
			
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}  

	private void createAckcopy() 
	
	{
		// rohan added this code as per salutation in customer here we will add
		// dear sir / madam
		// Date : 13/2/2017
		String salutation = "";
		if (cust.getSalutation() != null && !cust.getSalutation().equals("")) {
			System.out.println("salutation " + cust.getSalutation());

			if (cust.getSalutation().equalsIgnoreCase("Mr.")) {
				System.out.println("In side MR salutation condition ");
				salutation = "Dear Sir, ";
			} 
			else if (cust.getSalutation().equalsIgnoreCase("Ms."))
			{
				System.out.println("In side MS salutation condition ");
				salutation = "Dear Madam, ";
			}
			else 
			{
				System.out.println("In side  else1 salutation condition ");
				salutation = "Dear Sir / Madam, ";
			}
		}
		else
		{   
			System.out.println("In side  else2 salutation condition ");
			salutation = "Dear Sir / Madam, ";
		}
		
		Phrase phrs1 = new Phrase(salutation, font9);
		Phrase phrs2 = new Phrase("Please find below, for your signature, the order For " + " PEST CONTROL TREATMENTS .", font9);
		Phrase phrs3 = new Phrase("Please send us the copy duly signed, retaining the Original for your record.",font9);
		Phrase phrs4 = new Phrase("Since it is most important that we receive the copy of the signed order, we request ",font9);
		Phrase phrs5 = new Phrase("your co-operation in the Matter.", font9);
		Phrase phrs6 = new Phrase("Thanking you,", font9);
		Phrase phrs7 = new Phrase("Yours faithfully,", font9);
		Phrase phrs8 = new Phrase("FOR " + comp.getBusinessUnitName(), font9bold);
		Phrase phrs9 = new Phrase("Director / Authorised Signatory", font9);
        Phrase blankphrs = new Phrase (" ",font9);
		PdfPTable parentTbl = new PdfPTable(2);
		parentTbl.setWidthPercentage(100);
		
		PdfPTable cellinfoTbl = new PdfPTable(1);
		cellinfoTbl.setWidthPercentage(100);
        
		PdfPCell cellinfo1 = new PdfPCell(phrs1);
		cellinfo1.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellinfo1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cellinfo1.setBorder(0);
		
		PdfPCell cellinfo2 = new PdfPCell(phrs2);
		cellinfo2.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellinfo2.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cellinfo2.setBorder(0);
		
		PdfPCell cellinfo3 = new PdfPCell(phrs3);
		cellinfo3.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellinfo3.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cellinfo3.setBorder(0);
		
		PdfPCell cellinfo4 = new PdfPCell(phrs4);
		cellinfo4.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellinfo4.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cellinfo4.setBorder(0);
		
		PdfPCell cellinfo5 = new PdfPCell(phrs5);
		cellinfo5.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellinfo5.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cellinfo5.setBorder(0);
		
		PdfPCell cellinfo6 = new PdfPCell(phrs6);
		cellinfo6.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellinfo6.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cellinfo6.setBorder(0);
		
		PdfPCell cellinfo7 = new PdfPCell(phrs7);
		cellinfo7.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellinfo7.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cellinfo7.setBorder(0);
		
		PdfPCell cellinfo8 = new PdfPCell(phrs8);
		cellinfo8.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellinfo8.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cellinfo8.setBorder(0);
		
		PdfPCell cellinfo9 = new PdfPCell(phrs9);
		cellinfo9.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellinfo9.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cellinfo9.setBorder(0);
		
		PdfPCell blankCell = new PdfPCell(blankphrs);
		blankCell.setBorder(0);
		
		PdfPCell cellBlank = new PdfPCell();
		cellBlank.setFixedHeight(3);
		cellBlank.setBorder(0);
		
		cellinfoTbl.addCell(blankCell);
		cellinfoTbl.addCell(cellinfo1);
		cellinfoTbl.addCell(cellBlank);
		cellinfoTbl.addCell(cellinfo2);
		cellinfoTbl.addCell(cellBlank);
		cellinfoTbl.addCell(cellinfo3);
		cellinfoTbl.addCell(cellBlank);
		cellinfoTbl.addCell(cellinfo4);
		cellinfoTbl.addCell(cellBlank);
		cellinfoTbl.addCell(cellinfo5);
		cellinfoTbl.addCell(cellBlank);
		cellinfoTbl.addCell(cellinfo6);
		cellinfoTbl.addCell(cellBlank);
		cellinfoTbl.addCell(cellinfo7);
		cellinfoTbl.addCell(cellBlank);
		cellinfoTbl.addCell(cellinfo8);
		cellinfoTbl.addCell(blankCell);
		cellinfoTbl.addCell(blankCell);
		cellinfoTbl.addCell(blankCell);
		cellinfoTbl.addCell(blankCell);
		cellinfoTbl.addCell(blankCell);
//		cellinfoTbl.addCell(blankCell);
//		cellinfoTbl.addCell(blankCell);
		cellinfoTbl.addCell(cellinfo9);
		cellinfoTbl.addCell(blankCell);
		
		
//		PdfPCell cellinfo = new PdfPCell();
//		cellinfo.addElement(cellinfopara);
//		cellinfo.setBorder(0);

		PdfPTable cellinfotbl = new PdfPTable(1);
		cellinfotbl.setWidthPercentage(100);

		Phrase phrse = new Phrase("CIN : ",font9);
		for (int i = 0; i < this.articletype.size(); i++)
		{
			if (articletype.get(i).getArticleTypeName().equalsIgnoreCase("CIN")
					&& articletype.get(i).getArticlePrint()
							.equalsIgnoreCase("YES")
					&& articletype.get(i).getDocumentName()
							.equalsIgnoreCase("Contract")) {

				 phrse = new Phrase("CIN : "+ articletype.get(i).getArticleTypeValue(), font8bold);
			}
		}
		
		PdfPCell cincell = new PdfPCell(phrse);
		cincell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cincell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cincell.setFixedHeight(25f); 
		cincell.setBorder(0);
		cellinfotbl.addCell(cincell);

		Phrase odrNo = new Phrase("ORDER NO. : " + con.getCount(), font8bold);
		
		PdfPCell odrnoCell = new PdfPCell(odrNo);
		odrnoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		odrnoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		odrnoCell.setBorderWidthBottom(0);
		odrnoCell.setFixedHeight(25f);
		
		Phrase odrNodtls1 = new Phrase(" PLEASE MENTION THIS NUMBER IN ALL ", font8bold);
		Phrase odrNodtls2 = new Phrase(" FUTURE CORRESPONDENCE", font8bold);
		  
		
		PdfPCell odrnodtls1Cell = new PdfPCell(odrNodtls1);
		odrnodtls1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);  		
		odrnodtls1Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		odrnodtls1Cell.setFixedHeight(20f);
		odrnodtls1Cell.setBorderWidthBottom(0);  
		odrnodtls1Cell.setBorderWidthTop(0);
		
		
		PdfPCell odrnodtls2Cell = new PdfPCell(odrNodtls2);
		odrnodtls2Cell.setHorizontalAlignment(Element.ALIGN_CENTER); 		
		odrnodtls2Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		odrnodtls2Cell.setFixedHeight(25f);
		odrnodtls2Cell.setBorderWidthTop(0);
		  
		Phrase blnk = new Phrase("            ", font9bold);
		PdfPCell blnkcell = new PdfPCell();
		blnkcell.addElement(blnk);
		blnkcell.setFixedHeight(10);
		blnkcell.setBorder(0);
		
		Phrase blnk1 = new Phrase("            ", font9bold);
		PdfPCell blnk1cell = new PdfPCell();
		blnk1cell.addElement(blnk1);
		blnk1cell.setBorderWidthTop(0);
		

		Phrase clientno = new Phrase("CLIENT NO. : " + con.getCustomerId(),font8bold); // is this value of is correct
		
		PdfPCell clientnoCell = new PdfPCell(clientno);
		clientnoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		clientnoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		clientnoCell.setFixedHeight(25f);
		clientnoCell.setBorderWidthBottom(0);
       
		
		Phrase servicetaxno =new Phrase(" SERVICE TAX NO. : ",font8bold)  ;
		for (int i = 0; i < this.articletype.size(); i++) {
			if (articletype.get(i).getArticleTypeName().equalsIgnoreCase("Service Tax No")
					&& articletype.get(i).getArticlePrint()
							.equalsIgnoreCase("YES")
					&& articletype.get(i).getDocumentName()
							.equalsIgnoreCase("Contract")) {

				 servicetaxno = new Phrase("SERVICE TAX NO. : " + articletype.get(i).getArticleTypeValue(), font8bold);
				
			}
		}
		  
		Phrase panno = new Phrase(" PAN NO. : " ,font8bold);
		for (int i = 0; i < this.articletype.size(); i++) {
			if (articletype.get(i).getArticleTypeName().equalsIgnoreCase("PAN NO")&& articletype.get(i).getArticlePrint().equalsIgnoreCase("YES")
					&& articletype.get(i).getDocumentName().equalsIgnoreCase("Contract")) {

				 panno = new Phrase("                 PAN NO. : "+ articletype.get(i).getArticleTypeValue(), font8bold);  // 
			}
		}  
			PdfPCell servicetaxnoCell = new PdfPCell(servicetaxno);
			servicetaxnoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			servicetaxnoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);  
			servicetaxnoCell.setFixedHeight(25f);
			servicetaxnoCell.setBorderWidthBottom(0);
			
			PdfPCell panNoCell = new PdfPCell (panno);
			panNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			panNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			panNoCell.setFixedHeight(25f);
			panNoCell.setBorderWidthTop(0);
			  
			cellinfotbl.addCell(odrnoCell);
			cellinfotbl.addCell(blnk1cell);
			cellinfotbl.addCell(odrnodtls1Cell);
			cellinfotbl.addCell(odrnodtls2Cell);
			cellinfotbl.addCell(blnkcell);
			cellinfotbl.addCell(clientnoCell);
			cellinfotbl.addCell(blnk1cell);
			cellinfotbl.addCell(blnkcell);
			cellinfotbl.addCell(servicetaxnoCell);
			cellinfotbl.addCell(panNoCell);
			cellinfotbl.addCell(blnkcell);
			

		
		PdfPCell cellinfoTblCell = new PdfPCell();
		cellinfoTblCell.addElement(cellinfoTbl);
		cellinfoTblCell.setBorder(0);

		PdfPCell cellinfotblCell = new PdfPCell();
		cellinfotblCell.addElement(cellinfotbl);
		cellinfotblCell.setBorder(0);

		parentTbl.addCell(cellinfoTblCell); 
		parentTbl.addCell(cellinfotblCell);
		   

		try {
			parentTbl.setWidths(new float[] { 68, 32 });
			document.add(parentTbl);
		    }
		catch (Exception e1) 
		   {
			 e1.printStackTrace();
		   }
	}

	private void createBlankforWrite() 
	{

		Paragraph bp1 = new Paragraph();
		bp1.add(Chunk.NEWLINE);
//		bp1.add(Chunk.NEWLINE);
		bp1.add(Chunk.NEWLINE);
		PdfPCell blankCell1 = new PdfPCell();

		blankCell1.addElement(bp1);
		blankCell1.setBorderWidthLeft(0);

		PdfPCell blankCell2 = new PdfPCell();
		Paragraph bp2 = new Paragraph();
		bp2.add(Chunk.NEWLINE);
//		bp2.add(Chunk.NEWLINE);
		bp2.add(Chunk.NEWLINE);
		blankCell2.addElement(bp2);
		blankCell2.setBorderWidthRight(0);

		PdfPTable parenttbl = new PdfPTable(2);
		parenttbl.setWidthPercentage(100);
		parenttbl.addCell(blankCell1);
		parenttbl.addCell(blankCell2);

		try {
			parenttbl.setWidths(new float[] { 50, 50 });
			document.add(parenttbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createTblFrmInfo() 
	{
		PdfPTable treatmentinfoTbl = new PdfPTable(6);
		treatmentinfoTbl.setWidthPercentage(100);
		try {
			treatmentinfoTbl
					.setWidths(new float[] { 10, 27, 21, 15, 13, 14,});
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		Phrase cardno = new Phrase("CARD NO.", font9bold);
		PdfPCell cardnoCell = new PdfPCell(cardno);
		cardnoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cardnoCell.setBorderWidthLeft(0);
//		cardnoCell.setBorderWidthBottom(0);
		treatmentinfoTbl.addCell(cardnoCell);

		Phrase occupant = new Phrase("OCCUPANT", font9bold);
		PdfPCell occupantCell = new PdfPCell(occupant);
		occupantCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		occupantCell.setBorderWidthBottom(0);
		treatmentinfoTbl.addCell(occupantCell);

		Phrase typnfrq = new Phrase(" TYPE OF TREATMENT & FREQUENCY", font9bold);
		PdfPCell typnfrqCell = new PdfPCell(typnfrq);
		typnfrqCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		typnfrqCell.setBorderWidthBottom(0);
		treatmentinfoTbl.addCell(typnfrqCell);

		Phrase treated = new Phrase(" AREAS TO BE TREATED ", font9bold);
		PdfPCell treatedCell = new PdfPCell(treated);
		treatedCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		treatedCell.setBorderWidthBottom(0);
		treatmentinfoTbl.addCell(treatedCell);

		Phrase schedule = new Phrase(" SCHEDULE OF TREATMENT ", font9bold);
		PdfPCell scheduleCell = new PdfPCell(schedule);
		scheduleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		scheduleCell.setBorderWidthBottom(0);
		treatmentinfoTbl.addCell(scheduleCell);

		Phrase amnt = new Phrase(" AMOUNT ", font9bold);
		PdfPCell amntCell = new PdfPCell(amnt);
		amntCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		amntCell.setBorderWidthRight(0);
		treatmentinfoTbl.addCell(amntCell);

//		Phrase tc = new Phrase("TERMS & CONDITIONS ", font9bold);
//		PdfPCell tcCell = new PdfPCell(tc);
//		tcCell.setHorizontalAlignment(Element.ALIGN_CENTER);
////		tcCell.setBorderWidthBottom(0);
//		tcCell.setBorderWidthRight(0);
//		treatmentinfoTbl.addCell(tcCell);
		
		 ////////////////////////////// 4 digit card No logic added by Ajinkya on Date: 03/06/2017  /////////////////////////
		 
		 String strfrdigitCardNo = ""+ con.getCount()%10000 ; 
		 
		 if(strfrdigitCardNo.length()!=0 &&strfrdigitCardNo.length()==2){
			 strfrdigitCardNo = "00"+strfrdigitCardNo;
		 }
		 else if(strfrdigitCardNo.length()!=0 &&strfrdigitCardNo.length()==3){
			 strfrdigitCardNo = "0"+strfrdigitCardNo; 
		 }
		 System.out.println("4 digit " +strfrdigitCardNo);
		 
	//////////////////////////////4 digit card No logic end Here //////////////////////////

		String contractCardNo = "";

		for (int i = 0; i < con.getItems().size() ; i ++) {
			
		contractCardNo = con.getItems().get(i).getProductCode()+"/"+ strfrdigitCardNo ;  // +" /" + i to add 
//  		System.out.println("count :" + con.getCount());
//		System.out.println("count :" + con.getContractCount());
		Phrase cardnoVal = new Phrase(contractCardNo, font9);     
		PdfPCell cell1 = new PdfPCell(cardnoVal);
		cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell1.setBorderWidthLeft(0);
		cell1.setBorderWidthBottom(0);
		cell1.setBorderWidthTop(0);
		treatmentinfoTbl.addCell(cell1);
		

		String salutation = "";
		if (cust.getSalutation() != null && !cust.getSalutation().equals("")) 
		{
			System.out.println("salutation " + cust.getSalutation());
			salutation = cust.getSalutation();
		}
		String companyName = "";
		if (cust.isCompany() == true && cust.getCompanyName() != null)
		{

			if (cust.getCustPrintableName() != null && !cust.getCustPrintableName().equals("")) 
			{
				companyName = " " + salutation + cust.getCustPrintableName().trim();
			} else
			{
				companyName =  cust.getCompanyName().trim();    
			}

		} else 
		{     
			if (cust.getCustPrintableName() != null && !cust.getCustPrintableName().equals(""))
			{
				companyName = cust.getCustPrintableName().trim();
				
			} else
			{
				companyName = salutation + " " + cust.getFullname().trim();
			}
		}
		 Logger logger = Logger.getLogger("cust nameB ");
		 logger.log(Level.SEVERE,"Ind"+ companyName);
		/*
		 * nameInFirstLetterUpperCase defined by Ajinkya  suggested by Rahul V 
		 * Used to take customer name from Uppercase to Only first letter Uppercase
		 */
		String nameInFirstLetterUpperCase=getFirstLetterUpperCase(companyName.trim());
		///////
      
		 Logger logger2 = Logger.getLogger("cust nameA ");
		 logger2.log(Level.SEVERE,"After Converson name :"+nameInFirstLetterUpperCase);
		System.out.println(nameInFirstLetterUpperCase);
		/////////////////////////////////// one time print of customer name  ///////////////////////////////////////	
				if(i==0)
				{
				Phrase occupantVal = new Phrase(nameInFirstLetterUpperCase, font9); //
				PdfPCell cell2 = new PdfPCell(occupantVal);
				cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
				cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell2.setBorderWidthBottom(0);
				cell2.setBorderWidthTop(0);
				treatmentinfoTbl.addCell(cell2);
				}
				else
				{
					Phrase occupantVal = new Phrase(" ", font9); //
					PdfPCell cell2 = new PdfPCell(occupantVal);
					cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
					cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell2.setBorderWidthBottom(0);
					cell2.setBorderWidthTop(0);
					treatmentinfoTbl.addCell(cell2);
					
				}
/////////////////////////////////// one time print of customer name logic End Here   ///////////////////////////////////////			
// product name   & frequency    
		
		String prtclr = "" ;
//		String ferqVal= "" ;
//		for (int i = 0; i < con.getItems().size(); i++) 
//		{
			 prtclr = con.getItems().get(i).getProductName();
			 System.out.println(prtclr);
		
		////////////////////////////////////////////// frequency value    //////////////////////////////////// 
//		 
//			As per pratibha's discussion i disabled the freq value 
//			
//		    if(con.getItems().get(i).getProductCode().equalsIgnoreCase("WB")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT")
//		    	||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT")
//		    	||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO")
//		    	||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA")
//		    	||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT")
//		    	||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP")
//		    	||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC")
//		    	)
//		    {
//		    	 ferqVal= "   " ;
//		    }
//		    
//		   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT00")
//			||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT00")
//			||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO00")
//			||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA00")
//			||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT00")
//			||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP00")
//			||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC00")
//			)
//		{
//			   ferqVal= "As An When " ;
//		}
//		   
//		   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT365")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO365")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA365")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT365")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP365")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC365")
//					)
//				{
//			   ferqVal= " Daily" ;
//				}
//		   
//		   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT52")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT52")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO52")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA52")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT52")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP52")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC52")
//					)
//				{
//			   ferqVal= " Weeekly " ;	
//				}
//		   
//		   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT36")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT36")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO36")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA36")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT36")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP36")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC36")
//					)
//				{
//			       ferqVal= " Twice the Month " ;		
//				}
//		   
//		   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT24")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT24")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO24")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA24")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT24")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP24")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC24")
//					)
//				{
//			        ferqVal= " Four Nightly " ; 
//				}
//		   
//		   
//		   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT12")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT12")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO12")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA12")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT12")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP12")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC12")
//					)
//				{
//			   ferqVal= " Monthly" ;
//				}
//		   
//		   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT06")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT06")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO06")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA06")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT06")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP06")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC06")
//					)
//				{
//			   ferqVal= " Alternate Month" ;
//				}
//		   
//		   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT04")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT04")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO04")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA04")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT04")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP04")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC04")
//					)
//				{
//			   ferqVal= " Quarterly " ;
//				}
//		   
//		   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT03")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT03")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO03")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA03")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT03")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP03")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC03")
//					)
//				{
//			   ferqVal= " Four Monthtly " ;	
//				}
//		   
//		   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB02")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT02")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC02")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT02")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY02")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO02")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST02")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA02")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT02")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT02")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT02")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP02")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC02")
//					)
//				{
//			   ferqVal= " Six Monthly " ;	    
//				}
		    
        //////////////////////////////////////////////frequency value end here   //////////////////////////////////// 
		
		Phrase treatNfreaq = new Phrase(prtclr , font9);
		PdfPCell cell3 = new PdfPCell(treatNfreaq);
		cell3.setHorizontalAlignment(Element.ALIGN_LEFT);  
		cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell3.setBorderWidthBottom(0);
		cell3.setBorderWidthTop(0);
		treatmentinfoTbl.addCell(cell3);
		
		System.out.println("premises outside ");
		
		String  premises = " ";
		
		if (con.getItems().get(i).getPremisesDetails() != null && !con.getItems().get(i).getPremisesDetails().equals("") )
		{
			System.out.println("premises inside ");
			
		    premises = "" +con.getItems().get(i).getPremisesDetails();
		}
		else
		{
			System.out.println("premises else ");
			  premises = " ";
		}
		
		Phrase premisesPhrse = new Phrase( premises, font9);
		PdfPCell cell4 = new PdfPCell(premisesPhrse);
		cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell4.setBorderWidthBottom(0);
		cell4.setBorderWidthTop(0);
		treatmentinfoTbl.addCell(cell4);
		
    
		Phrase scheduleVal = new Phrase("");
		PdfPCell cell5 = new PdfPCell(scheduleVal);
		cell5.setHorizontalAlignment(Element.ALIGN_LEFT);  
		cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell5.setBorderWidthBottom(0);
		cell5.setBorderWidthTop(0);
		treatmentinfoTbl.addCell(cell5);
		
		
////////////////////////////////////////////////////////  adding taxs    /////////////////////////////////////
		 
			PdfPCell cell6 = new PdfPCell();
//			Paragraph p1 = new Paragraph();
			
		   /////////////////////////////// Add tax  for renew value cal ////////////////////////////////////////////////////
		  	  
		    	if((con.getItems().get(i).getVatTax().getPercentage()==0)&&(con.getItems().get(i).getServiceTax().getPercentage()!=0)&&(con.getItems().get(i).getServiceTax().getPercentage()==15))    
		    	{
		    		System.out.println("3 inside st 15");
		    		
		    		double productVal1 = con.getItems().get(i).getPrice();
		        	double tax1 = 14 * con.getItems().get(i).getPrice()/100;
		        	double tax2 = 0.5 * con.getItems().get(i).getPrice()/100;
		        	double tax3 = 0.5 * con.getItems().get(i).getPrice()/100;
		        	double tax = tax1 + tax2 + tax3 ;
		        	double total = tax + productVal1 ;  
		        	
		        	System.out.println("st @15 "+productVal1);   
		        	System.out.println(tax1);
		        	System.out.println(tax2);
		        	System.out.println(tax3);   
		        	System.out.println(tax);
		        	System.out.println(total);
		          	
		        String str = "service Tax @ 15 %"+ df.format(tax);
		    	
		    	Phrase proVal1 = new Phrase(""+productVal1, font10);
		    	Phrase words1 = new Phrase(" plus "+ str +" Rs ",font10);
		    	Phrase proVal2 = new Phrase(""+df.format(total),font10);
		    	Phrase words2 = new Phrase(" i.e ",font10);
		    	Phrase words3 = new Phrase(" per annum ",font10);
		        
		    	Date startDate = con.getStartDate();
		     	Date endDate = con.getEndDate();
		    	
		    	Calendar startCalendar = new GregorianCalendar();
		    	startCalendar.setTime(startDate);
		    	Calendar endCalendar = new GregorianCalendar();
		    	endCalendar.setTime(endDate);

		    	int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
		    	int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
		    	
		    	cell6.addElement(proVal1);
		    	cell6.addElement(words1);
		    	cell6.addElement(words2);
		    	cell6.addElement(proVal2);   
		    	
		    	 if( diffMonth == 12)
		    	    {
		    		 cell6.addElement(words3);
		    	    }
		    	                   
		    	}  
		      
		    	else if((con.getItems().get(i).getVatTax().getPercentage()==0)&&(con.getItems().get(i).getServiceTax().getPercentage()>0)&&(con.getItems().get(i).getServiceTax().getPercentage()==14.5))
		      	{
		      		System.out.println("inside st 14.5");
		    		
		    		double productVal1 = con.getItems().get(i).getPrice();
		        	double tax = 14 * con.getItems().get(i).getPrice()/100;
		        	double tax2 = 0.5 * con.getItems().get(i).getPrice()/100;
		        	double total = tax + productVal1 + tax2;
		        	double totaltax = tax + tax2 ;
		            String str = "service Tax @ 14.5 % "+ df.format(totaltax);
		    	
		    	    Phrase proVal1 = new Phrase(""+productVal1, font10);
		    	    Phrase words1 = new Phrase(" plus "+ str + " Rs ",font10);
		    	    Phrase proVal2 = new Phrase(""+df.format(total),font10);
		    	    Phrase words2 = new Phrase(" i.e ",font10);
		    	    Phrase words3 = new Phrase(" per annum ",font10);
		    	  
		    	    System.out.println("st@14.5"+ productVal1);    
		    	    System.out.println(tax);
		    	    System.out.println(tax2);
		    	    System.out.println(total);
		    	
		    	    Date startDate = con.getStartDate();
		         	Date endDate = con.getEndDate();
		        	
		        	Calendar startCalendar = new GregorianCalendar();
		        	startCalendar.setTime(startDate);
		        	Calendar endCalendar = new GregorianCalendar();
		        	endCalendar.setTime(endDate);

		        	int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
		        	int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
		        	

			    	cell6.addElement(proVal1);
			    	cell6.addElement(words1);
			    	cell6.addElement(words2);
			    	cell6.addElement(proVal2);   
			    	
			    	 if( diffMonth == 12){
			    		                   cell6.addElement(words3);
			    	                  }
			    	 
		    	}
		    	

		    	else if((con.getItems().get(i).getVatTax().getPercentage()!=0)&&(con.getItems().get(i).getServiceTax().getPercentage()==0))
		    	{
		    		System.out.println("inside vat");
		    	  	
		    		double amount1 = con.getItems().get(i).getPrice();
		    		double vat1 = con.getItems().get(i).getVatTax().getPercentage()*con.getItems().get(i).getPrice()/100;
		    		double toalAmount = amount1 + vat1 ; 
		    		String str = " VAT "+con.getItems().get(i).getVatTax().getPercentage()+" @ % " + df.format(vat1) ;
		    		
		    		  
		    		Phrase proVal1 = new Phrase(""+ amount1, font10);
		        	Phrase words1 = new Phrase(" plus "+ str + " Rs ",font10);
		        	Phrase proVal2 = new Phrase(""+df.format(toalAmount),font10);
		        	Phrase words2 = new Phrase(" i.e ",font10);
		        	Phrase words3 = new Phrase(" per annum ",font10);
		        	
		        	Date startDate = con.getStartDate();
		         	Date endDate = con.getEndDate();
		        	
		        	Calendar startCalendar = new GregorianCalendar();
		        	startCalendar.setTime(startDate);   
		        	Calendar endCalendar = new GregorianCalendar();
		        	endCalendar.setTime(endDate);

		        	int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
		        	int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
		        	
		        	  

			    	cell6.addElement(proVal1);
			    	cell6.addElement(words1);
			    	cell6.addElement(words2);
			    	cell6.addElement(proVal2);   
			    	
			    	 if( diffMonth == 12){
			    		                   cell6.addElement(words3);
			    	                  }
			    	 
		        	 System.out.println(proVal1);  
		    	}  
		    	
		    	else  {
		    		double amount1 = con.getItems().get(i).getPrice();
		    		Phrase proVal1 = new Phrase(""+ amount1, font10);
		    		cell6.addElement(proVal1);   
		    	}
		    
		    ////////////////////////////////   calc end Here    ///////////////////////////////////////////// 
		    
////////////////////////////////////////////////////////adding txes end Here  /////////////////////////////////////	
		cell6.setBorderWidthBottom(0);
		cell6.setBorderWidthRight(0);
		cell6.setBorderWidthTop(0);
		cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell6.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		treatmentinfoTbl.addCell(cell6);   

//		PdfPCell cell7 = new PdfPCell();
//		cell7.setHorizontalAlignment(Element.ALIGN_LEFT);
//		cell7.setBorderWidthRight(0);
//		cell7.setBorderWidthBottom(0);
//		cell7.setBorderWidthTop(0);
//		treatmentinfoTbl.addCell(cell7);
		}
		try 
		{
			document.add(treatmentinfoTbl);
		}
		catch (DocumentException e) {
			e.printStackTrace();
		}  
	}  
	
	  //Ajinkya added this code to convert customer name in CamelCase    
			//  Date : 12/4/2017

		private String getFirstLetterUpperCase(String customerFullName) {
			
			String customerName="";
			String[] customerNameSpaceSpilt=customerFullName.split(" ");
			int count=0;
			for (String name : customerNameSpaceSpilt) 
			{
				String nameLowerCase=name.trim().toLowerCase();
				if(count==0)
				{
					customerName=customerName+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);
				}
				else
				{
					customerName=customerName+" "+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);	
				}
				count=count+1;  
			}
			return customerName;  

			
		}

	private static final String[] tensNames = { "", " Ten", " Twenty", " Thirty", " Fourty", " Fifty", " Sixty", " Seventy", " Eighty", " Ninety" };

     private static final String[] numNames = { "", " One", " Two", " Three", " Four", " Five", " Six", " Seven", " Eight", " Nine", " Ten", " Eleven", " Twelve",
     	" Thirteen", " Fourteen", " Fifteen", " Sixteen", " Seventeen", " Eighteen", " Nineteen" };
     private static String convertLessThanOneThousand(int number) {
     	String soFar;
     	if (number % 100 < 20){ soFar = numNames[number % 100]; number /= 100; } else { soFar = numNames[number % 10]; number /= 10;
     	soFar = tensNames[number % 10] + soFar; number /= 10; } if (number == 0) return soFar; return numNames[number] + " Hundred" + soFar; 
     	}
     public static  String convert(double number) {
     	// 0 to 999 999 999 999
     	if (number == 0) { return "Zero"; }
     	String snumber = Double.toString(number);
     	// pad with "0"
     	String mask = "000000000000";   
     	DecimalFormat df = new DecimalFormat(mask); 
     	snumber = df.format(number);
     	int hyndredCrore = Integer.parseInt(snumber.substring(3,5));
     	int hundredLakh = Integer.parseInt(snumber.substring(5,7));
     	int hundredThousands = Integer.parseInt(snumber.substring(7,9));
     	int thousands = Integer.parseInt(snumber.substring(9,12));
     	String tradBillions;
     	switch (hyndredCrore) { case 0: tradBillions = ""; break; case 1 : tradBillions = convertLessThanOneThousand(hyndredCrore) + " Crore "; break; default : tradBillions = convertLessThanOneThousand(hyndredCrore) + " Crore "; }

     	String result = tradBillions;
     	String tradMillions;
     	switch (hundredLakh) { case 0: tradMillions = ""; break; case 1 : tradMillions = convertLessThanOneThousand(hundredLakh) + " Lakh "; break; default : tradMillions = convertLessThanOneThousand(hundredLakh) + " Lakh "; }
     	result = result + tradMillions;
     	String tradHundredThousands;

     	switch (hundredThousands) { case 0: tradHundredThousands = ""; break; case 1 : tradHundredThousands = "One Thousand "; break; default : tradHundredThousands = convertLessThanOneThousand(hundredThousands) + " Thousand "; }
     	result = result + tradHundredThousands;

     	String tradThousand;  
     	tradThousand = convertLessThanOneThousand(thousands);
     	result = result + tradThousand;return result.replaceAll("^\\s+", "").replaceAll("file://b//s%7B2,%7D//b", " "); 
     	
     }

	private void getamountNpaterms() {  
		  
		     /////////////////////////////// Amount  calc ///////////////////////////
		   
		
		    String amtInWords = PcambContractpdf.convert(con.getNetpayable());
			Phrase prodamnt = new Phrase("Amount In Words : " +amtInWords +" Only", font9);
			Phrase paymntterms = new Phrase("",font9);
			
			String payterms = "";
			if(con.getPayTerms()!= null){
				 payterms = con.getPayTerms();  
				System.out.println(payterms);
				 paymntterms = new Phrase("Payment Terms : "+ payterms, font9);
			}
			else if(con.getPayments().size()!=0)
			{
				String paytermsList ="";
				for(int i=0;i<con.getPayments().size();i++)
				{
				String paymenttermsDays ="" + con.getPaymentTermsList().get(i).getPayTermDays();
				String paymenttermsPercent ="" + con.getPaymentTermsList().get(i).getPayTermPercent();
				String paymenttermsComment = con.getPaymentTermsList().get(i).getPayTermComment();
				
				  paytermsList = paymenttermsDays+" "+paymenttermsPercent+" "+paymenttermsComment ;
				}
				 paymntterms = new Phrase("Payment Terms : "+ paytermsList, font9);
	
			}
			
			else 
			{
				 paymntterms = new Phrase("Payment Terms : ", font9);
			}
			  
		PdfPCell amontInWordsCell = new PdfPCell(prodamnt);
		amontInWordsCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		amontInWordsCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		amontInWordsCell.setBorderWidthLeft(0);
		amontInWordsCell.setBorderWidthRight(0);  
		amontInWordsCell.setBorderWidthBottom(0);
		amontInWordsCell.setFixedHeight(25f);  
		
		PdfPCell paytermsCell = new PdfPCell (paymntterms) ;
		paytermsCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		paytermsCell.setVerticalAlignment(Element.ALIGN_MIDDLE);   
		paytermsCell.setBorderWidthLeft(0);
		paytermsCell.setBorderWidthRight(0);
		paytermsCell.setBorderWidthTop(0);
		paytermsCell.setFixedHeight(25f);
		
		PdfPTable paymentinfo = new PdfPTable(1);
		paymentinfo.addCell(amontInWordsCell);
		paymentinfo.addCell(paytermsCell);
		try {
			paymentinfo.setWidthPercentage(100);
			document.add(paymentinfo);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		    
	
	}

	private void createfooter() {

		Phrase footer1 = new Phrase(
				"We have completed a computerissed system for our organisation. The entire service programme is generated on the computer. Hence, ",
				font9);
		Phrase footer2 = new Phrase(
				"we have certain norms to be maintained for the computers to work efficiently.",
				font9);
		Phrase footer3 = new Phrase(
				"A) The treatment schedule given herewith should be noted and the treatments must be taken on due date as far as possible. ",
				font9);
		Phrase footer4 = new Phrase(
				"B) In the event of cancellation of treatment due to unavoidable and unforeseen reasons, it shoud be taken at the earliest in the same month.",
				font10);
		Phrase footer5 = new Phrase(
				"C) In case of treatment not being taken in the same month and taken in next month, the remaining schedule will not be changed. ",
				font9);
		Phrase footer6 = new Phrase(
				"D) All treatments of the order must be taken during the period of order and no extension of order/refund will be granted. ",
				font9);
		Phrase footer7 = new Phrase(
				"E) All Government Levies/ taxes will be charged extra. ",
				font9bold);
		Phrase footer8 = new Phrase("F) Terms & conditions attached alongwith this document ",font9);
		
		Phrase footer9 = new Phrase("I/We Confirm acceptance of this order. ",
				font9);
		Phrase footer10 = new Phrase("Signature", font9);

		Paragraph footerPara = new Paragraph();
		footerPara.add(footer1);
		footerPara.add(footer2);
		footerPara.add(Chunk.NEWLINE);
		footerPara.add(footer3);
		footerPara.add(Chunk.NEWLINE);
		footerPara.add(footer4);
		footerPara.add(Chunk.NEWLINE);
		footerPara.add(footer5);
		footerPara.add(Chunk.NEWLINE);
		footerPara.add(footer6);
		footerPara.add(Chunk.NEWLINE);
		footerPara.add(footer7);
		footerPara.add(Chunk.NEWLINE);
		footerPara.add(footer8);
		footerPara.add(Chunk.NEWLINE);
		footerPara.add(footer9);
		footerPara.add(Chunk.NEWLINE);
		footerPara.add(Chunk.NEWLINE);
		footerPara.add(Chunk.NEWLINE);
		footerPara.add(footer10);
		footerPara.add(Chunk.NEWLINE);
		footerPara.setAlignment(Element.ALIGN_JUSTIFIED);

		PdfPCell footerPara2Cell = new PdfPCell();
		footerPara2Cell.addElement(footerPara);
		footerPara2Cell.setBorder(0);

		PdfPCell footerPara1Cell = new PdfPCell();
		Paragraph para = new Paragraph();
		para.add(new Chunk(" ", font9));
		footerPara1Cell.addElement(para);
		footerPara1Cell.setBorder(0);

		PdfPCell footerPara3Cell = new PdfPCell();
		para.add(new Chunk(" ", font9));
		footerPara3Cell.addElement(para);
		footerPara3Cell.setBorder(0);

		PdfPTable footerTable = new PdfPTable(3);
		footerTable.setWidthPercentage(100);
		try {
			footerTable.setWidths(new float[] { 7, 86, 7 });
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		footerTable.addCell(footerPara1Cell);
		footerTable.addCell(footerPara2Cell);
		footerTable.addCell(footerPara3Cell);

		try {
			document.add(footerTable);
		    document.add(Chunk.NEXTPAGE);

		} catch (DocumentException e) {

			e.printStackTrace();
		}

	}
//	*************************************** end of the code for contract copy ************************************ //

	
	private void createContractAddressPage()
	{
		
		PdfPTable addresTbl = new PdfPTable(4);
		addresTbl.setWidthPercentage(100);
		
		try {
			addresTbl.setWidths(new float[] { 20,35,20,25 });
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		Phrase blnk = new Phrase(" ",font9);
		PdfPCell blnkCell = new  PdfPCell (blnk); 
		blnkCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		blnkCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		blnkCell.setBorder(0);
		addresTbl.addCell(blnkCell);
		addresTbl.addCell(blnkCell);
		addresTbl.addCell(blnkCell);
		addresTbl.addCell(blnkCell);
		
		addresTbl.addCell(blnkCell);
		addresTbl.addCell(blnkCell);
		addresTbl.addCell(blnkCell);
		addresTbl.addCell(blnkCell);
		
		addresTbl.addCell(blnkCell);
		addresTbl.addCell(blnkCell);
		addresTbl.addCell(blnkCell);
		addresTbl.addCell(blnkCell);
		
		addresTbl.addCell(blnkCell);
		addresTbl.addCell(blnkCell);
		addresTbl.addCell(blnkCell);
		addresTbl.addCell(blnkCell);
//		addresTbl.addCell(blnkCell);
		

		
		Phrase cardno = new Phrase("Card No.", font9bold);
		PdfPCell cardnoCell = new PdfPCell(cardno);
		cardnoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cardnoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cardnoCell.setBorderWidthLeft(0);
		cardnoCell.setBorder(0);
		addresTbl.addCell(cardnoCell);

		Phrase occupant = new Phrase("Name / Address ", font9bold);
		PdfPCell occupantCell = new PdfPCell(occupant);
		occupantCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		occupantCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		occupantCell.setBorder(0);
		addresTbl.addCell(occupantCell);

		Phrase typnfrq = new Phrase("Premises Type ", font9bold);
		PdfPCell typnfrqCell = new PdfPCell(typnfrq);
		typnfrqCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		typnfrqCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		typnfrqCell.setBorder(0);
		addresTbl.addCell(typnfrqCell);

		Phrase treated = new Phrase("Treatment & Frequency ", font9bold);
		PdfPCell treatedCell = new PdfPCell(treated);
		treatedCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		treatedCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		treatedCell.setBorder(0);
		addresTbl.addCell(treatedCell);

//		Phrase schedule = new Phrase("Frequency ", font10bold);
//		PdfPCell scheduleCell = new PdfPCell(schedule);
//		scheduleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		scheduleCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		scheduleCell.setBorder(0);
//		addresTbl.addCell(scheduleCell);

		String contractCardNo = "";
		String salutation = null ;
		String custName = "";
		String addressline="";
		String localityline=null;
		
		Phrase blnkphrse =new Phrase ("    ",font10);
		PdfPCell blnkPhrseCell = new PdfPCell(blnkphrse);
		blnkPhrseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		blnkPhrseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		blnkPhrseCell.setBorder(0);
		
		addresTbl.addCell(blnkPhrseCell);
		addresTbl.addCell(blnkPhrseCell);
		addresTbl.addCell(blnkPhrseCell);
		addresTbl.addCell(blnkPhrseCell);
		
		 ////////////////////////////// 4 digit card No logic added by Ajinkya on Date: 03/06/2017  /////////////////////////
		 
		 String strfrdigitCardNo = ""+ con.getCount()%10000 ; 
		 
		 if(strfrdigitCardNo.length()!=0 &&strfrdigitCardNo.length()==2){
			 strfrdigitCardNo = "00"+strfrdigitCardNo;
		 }
		 else if(strfrdigitCardNo.length()!=0 &&strfrdigitCardNo.length()==3){
			 strfrdigitCardNo = "0"+strfrdigitCardNo; 
		 }
		 System.out.println("4 digit " +strfrdigitCardNo);
		 
	//////////////////////////////4 digit card No logic end Here //////////////////////////
		
	for (int i = 0; i < con.getItems().size() ; i ++) {
			
		contractCardNo = con.getItems().get(i).getProductCode()+"/"+ strfrdigitCardNo;
			
//		System.out.println("count :" + con.getCount());
//		System.out.println("count :" + con.getContractCount());
		Phrase cardnoVal = new Phrase(contractCardNo, font9); //
		PdfPCell cell1 = new PdfPCell(cardnoVal);
		cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell1.setVerticalAlignment(Element.ALIGN_TOP);
		cell1.setBorder(0);
		addresTbl.addCell(cell1);

		
		if (cust.getSalutation() != null && !cust.getSalutation().equals("")) {
			System.out.println("salutation " + cust.getSalutation());
			salutation = cust.getSalutation();
		}
		else
		{
			salutation = ""  ;	
		}
		
		if (cust.isCompany() == true && cust.getCompanyName() != null)
		{

			if (cust.getCustPrintableName() != null && !cust.getCustPrintableName().equals("")) 
			{
				custName = salutation + " " +cust.getCustPrintableName().trim();
			} else
			{
				custName = cust.getCompanyName().trim();    
			}

		} 
		else 
		  {     
			if (cust.getCustPrintableName() != null && !cust.getCustPrintableName().equals(""))
			{
				custName =  cust.getCustPrintableName().trim();
				
			} else
			{
				custName = salutation + " " + cust.getFullname().trim();
			}
		  }
		////////////////////////////////////////////////////////////////////////////////
//		String locdetails = ""; 
//		if (con.getBranch()!=null)
//		{
//
//		  locdetails =con.getBranch()+""; 
//			 
// 		}
		
		///////////////////////////////////////////////////////////////////////////////
		
		 if(ser!=null)
		{		
		if(ser.getAddress().getAddrLine2() != null && !ser.getAddress().getAddrLine2().equals(""))   
			{
				addressline=ser.getAddress().getAddrLine1()+", "+ser.getAddress().getAddrLine2()+", "+"\n";
			}
			else
			{
				addressline=ser.getAddress().getAddrLine1()+", "+"\n";
			}
			
			if((!ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==false)){
				System.out.println("inside both null condition1");
				localityline= (ser.getAddress().getLandmark()+", "+ser.getAddress().getLocality()+", "+"\n"+ser.getAddress().getCity()+" - "
					      +ser.getAddress().getPin()+". "+"\n" +ser.getAddress().getState()+", "+ser.getAddress().getCountry()+".");
			}
			else if((!ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==true)){
				System.out.println("inside both null condition 2");
				localityline= (ser.getAddress().getLandmark()+", "+"\n"+ ser.getAddress().getCity()+" - "
					      +ser.getAddress().getPin()+". "+"\n"+ser.getAddress().getState()+", "+ser.getAddress().getCountry()+". ");
			}
			
			else if((ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==false)){
				System.out.println("inside both null condition 3");
				localityline= (ser.getAddress().getLocality()+", "+"\n"+ser.getAddress().getCity()+" - "
					      +ser.getAddress().getPin()+". "+"\n"+ser.getAddress().getState()+", "+ser.getAddress().getCountry()+".");
			}
			else if((ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==true)){
				System.out.println("inside both null condition 4");
				localityline=(ser.getAddress().getCity()+" - "+ser.getAddress().getPin()+". "+"\n"+ser.getAddress().getState()+", "+ser.getAddress().getCountry()+". ");
			}
	}
	else
	{
		////////////////////// rohan suggested that take this address from customer ////////////////////// 
		
		if(cust.getSecondaryAdress().getAddrLine2() != null && !cust.getSecondaryAdress().getAddrLine2().equals(""))   
		{
			addressline=cust.getSecondaryAdress().getAddrLine1()+", "+cust.getSecondaryAdress().getAddrLine2()+", "+"\n";
		}
		else
		{
			addressline= cust.getSecondaryAdress().getAddrLine1()+", "+"\n";
		}
		
		if((!cust.getSecondaryAdress().getLandmark().equals(""))&&(cust.getSecondaryAdress().getLocality().equals("")==false)){
			                      System.out.println("inside both null condition1");
			localityline= (cust.getSecondaryAdress().getLandmark()+", "+cust.getSecondaryAdress().getLocality()+", "+"\n"+cust.getSecondaryAdress().getCity()+" - "
				      +cust.getSecondaryAdress().getPin()+". "+"\n" +cust.getSecondaryAdress().getState()+", "+cust.getSecondaryAdress().getCountry()+".");
		}
		else if((!cust.getSecondaryAdress().getLandmark().equals(""))&&(cust.getSecondaryAdress().getLocality().equals("")==true)){
			                         System.out.println("inside both null condition 2");
			localityline= (cust.getSecondaryAdress().getLandmark()+", "+"\n"+ cust.getSecondaryAdress().getCity()+" - "
				      +cust.getSecondaryAdress().getPin()+". "+"\n"+cust.getSecondaryAdress().getState()+", "+cust.getSecondaryAdress().getCountry()+".");
		}
		
		else if((cust.getSecondaryAdress().getLandmark().equals(""))&&(cust.getSecondaryAdress().getLocality().equals("")==false)){
			System.out.println("inside both null condition 3");
			localityline= (cust.getSecondaryAdress().getLocality()+", "+"\n"+cust.getSecondaryAdress().getCity()+" - "
				      +cust.getSecondaryAdress().getPin()+". "+"\n"+cust.getSecondaryAdress().getState()+", "+cust.getSecondaryAdress().getCountry()+". ");
		}
		else if((cust.getSecondaryAdress().getLandmark().equals(""))&&(cust.getSecondaryAdress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 4");
			localityline=(cust.getSecondaryAdress().getCity()+" - "+cust.getSecondaryAdress().getPin()+". "+"\n"+cust.getSecondaryAdress().getState()+", "+cust.getSecondaryAdress().getCountry()+". ");
		}

	}
		String customer = getFirstLetterUpperCase(custName.trim());
		System.out.println(customer);            
		Phrase custnameVal = new Phrase(" "+customer +"\n"+ addressline + localityline , font9); //
		
		Paragraph premisePara = new Paragraph();
		premisePara.add(custnameVal);
		premisePara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell cell2 = new PdfPCell();
		
		cell2.addElement(premisePara);
//		cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
//		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell2.setBorder(0);
		addresTbl.addCell(cell2);
		
		
	String  premises = " ";
		
		if (con.getItems().get(i).getPremisesDetails()!= null && !con.getItems().get(i).getPremisesDetails().equals("") )
		{
			System.out.println("premises inside ");
			
		  premises = "" +con.getItems().get(i).getPremisesDetails();
		}
		else{
			System.out.println("premises else ");
			  premises = " ";
		}
		
		Phrase premisetype = new Phrase(" "+premises ,font9);
		PdfPCell cell3 = new PdfPCell(premisetype);
		cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell3.setVerticalAlignment(Element.ALIGN_TOP);
		cell3.setBorder(0);
		addresTbl.addCell(cell3);
		
		Phrase treatVal = new Phrase(" "+con.getItems().get(i).getProductName(),font9);
		PdfPCell cell4 = new PdfPCell(treatVal);
		cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell4.setVerticalAlignment(Element.ALIGN_TOP);
		cell4.setBorder(0);
		addresTbl.addCell(cell4);
		
		
//		 String ferqVal= "" ;
//		    	
//		    if(con.getItems().get(i).getProductCode().equalsIgnoreCase("WB")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT")
//		    	||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT")
//		    	||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO")
//		    	||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA")
//		    	||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT")
//		    	||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP")
//		    	||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC")
//		    	)
//		    {
//		    	 ferqVal= "   " ;
//		    }
//		    
//		   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT00")
//			||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT00")
//			||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO00")
//			||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA00")
//			||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT00")
//			||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT00")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP00")
//			||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC00")
//			)
//		{
//			   ferqVal= " As & When " ;
//			
//		}
//		   
//		   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT365")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO365")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA365")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT365")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT365")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP365")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC365")
//					)
//				{
//			      ferqVal= " Daily" ;
//				}
//		   
//		   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT52")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT52")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO52")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA52")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT52")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT52")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP52")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC52")
//					)
//				{
//			         ferqVal= " Weeekly " ;	
//				}
//		   
//		   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT36")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT36")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO36")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA36")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT36")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT36")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP36")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC36")
//					)
//				{
//			        ferqVal= " Twice the Month " ;		
//				}
//		   
//		   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT24")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT24")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO24")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA24")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT24")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT24")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP24")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC24")
//					)
//				{
//			        ferqVal= " Four Nightly " ; 
//				}
//		   
//		   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT12")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT12")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO12")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA12")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT12")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT12")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP12")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC12")
//					)
//				{
//			      ferqVal= " Monthly" ;
//				}
//		   
//		   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT06")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT06")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO06")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA06")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT06")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT06")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP06")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC06")
//					)
//				{
//			      ferqVal= " Alternate Month" ;
//				}
//		   
//		   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT04")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT04")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO04")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA04")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT04")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT04")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP04")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC04")
//					)
//				{
//			         ferqVal= " Quarterly " ;
//				}
//		   
//		   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT03")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT03")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO03")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA03")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT03")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT03")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP03")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC03")
//					)
//				{
//			          ferqVal= " Four Monthtly " ;	
//				}
//		   
//		   if( con.getItems().get(i).getProductCode().equalsIgnoreCase("WB02")||con.getItems().get(i).getProductCode().equalsIgnoreCase("BT02")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC02")||con.getItems().get(i).getProductCode().equalsIgnoreCase("GT02")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("FLY02")||con.getItems().get(i).getProductCode().equalsIgnoreCase("MO02")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("GST02")||con.getItems().get(i).getProductCode().equalsIgnoreCase("WA02")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("DT02")||con.getItems().get(i).getProductCode().equalsIgnoreCase("FT02")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("BMT02")||con.getItems().get(i).getProductCode().equalsIgnoreCase("SP02")
//					||con.getItems().get(i).getProductCode().equalsIgnoreCase("RC02")
//					)
//				{
//			        ferqVal= " Six Monthly " ;	
//				}
		    
	  //////////////////////////////////////////////frequency value end here   //////////////////////////////////// 
		    
//		    Phrase ferqValphrase = new Phrase (ferqVal,font11);
//			PdfPCell freqValueCell = new PdfPCell(ferqValphrase);
//			freqValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			freqValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//			freqValueCell.setBorder(0);
//			addresTbl.addCell(freqValueCell);
//			
		
			addresTbl.addCell(blnkPhrseCell);
			addresTbl.addCell(blnkPhrseCell);
			addresTbl.addCell(blnkPhrseCell);
			addresTbl.addCell(blnkPhrseCell);
//			addresTbl.addCell(blnkPhrseCell);
			
			
			
	    }
			try {
				document.add(addresTbl);
				document.add(Chunk.NEXTPAGE);
			}
			catch (DocumentException e) 
			{
				
				e.printStackTrace();
			
			}  
	     
	}
	// ///////////////////////////////////////////////////////////////////////////////

	private void createHeaderTable() 
	{
		Phrase header = new Phrase(""
				+ comp.getBusinessUnitName().toUpperCase(), font16bold);
		Phrase title = new Phrase("TERMS AND CONDITIONS", font10boldul);
		Phrase gtc = new Phrase("GENERAL TERMS & CONDITIONS :", font9bold);

		Paragraph header1Para = new Paragraph();
		header1Para.add(header);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(title);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.setAlignment(Element.ALIGN_CENTER);

		Paragraph gtc2Para = new Paragraph();
		gtc2Para.add(gtc);
		gtc2Para.add(Chunk.NEWLINE);
		gtc2Para.setAlignment(Element.ALIGN_LEFT);

		PdfPCell Header1ParaCell = new PdfPCell();
		Header1ParaCell.addElement(header1Para);
		Header1ParaCell.setBorder(0);

		PdfPCell gtc2ParaCell = new PdfPCell();
		gtc2ParaCell.addElement(gtc2Para);
		gtc2ParaCell.setBorder(0);

		PdfPTable headerTable = new PdfPTable(1);
		headerTable.setWidthPercentage(100);
		headerTable.addCell(Header1ParaCell);

		PdfPTable gtcTable = new PdfPTable(1);
		gtcTable.setWidthPercentage(80);
		gtcTable.addCell(gtc2ParaCell);

		try {
			document.add(headerTable);
			document.add(gtcTable);
		} catch (DocumentException e) {

			e.printStackTrace();
		}

	}

	private void createTermsAndConditions() 
	{

		Phrase tc1 = new Phrase(
				"Treatment orders are valid for 1 year only from its date, unless otherwise Specified. ",
				font9);
		PdfPCell tc1Cell = new PdfPCell(tc1);
		tc1Cell.setBorder(0);
		tc1Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc1Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		Phrase tc2 = new Phrase(
				          "Number of services mentioned in the order must be availed as per the recommended "
						+ "schedule during the order period only. Balance services( for services not taken as per "
						+ "schedule) will not be carried forward to the next period nor will refund be provided "
						+ "for the same.", font9);
		PdfPCell tc2Cell = new PdfPCell(tc2);
		tc2Cell.setBorder(0);
		tc2Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc2Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc3 = new Phrase(
				"This treatment order is non-transferable. It is for the specified premises and for the "
						+ "specified client only. ", font9);
		PdfPCell tc3Cell = new PdfPCell(tc3);
		tc3Cell.setBorder(0);
		tc3Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc3Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc4 = new Phrase(
				"No guarantee of complete extermination of any pests will be given. The premises will be "
						+ "kept reasonably free from pests.  ", font9);
		PdfPCell tc4Cell = new PdfPCell(tc4);
		tc4Cell.setBorder(0);
		tc4Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc4Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc5 = new Phrase(
				"For effective control of pests, regular service as per the frequency recommended must "
						+ "be taken. We will not be responsible for pest re-infestation due to non-compliance of "
						+ "the same. ", font9);
		PdfPCell tc5Cell = new PdfPCell(tc5);
		tc5Cell.setBorder(0);
		tc5Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc5Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc6 = new Phrase(
				"As per the conditions of the treatment order, the entire premises must be made available "
						+ "for treatment during each scheduled service", font9);
		PdfPCell tc6Cell = new PdfPCell(tc6);
		tc6Cell.setBorder(0);
		tc6Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc6Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc7 = new Phrase(
				          "All pre & post treatment insrtuctions recommended by us, must be followed. We will not be responsible for pest problems arising due to "
						+ "non-conformance of the same.", font9);
		PdfPCell tc7Cell = new PdfPCell(tc7);
		tc7Cell.setBorder(0);
		tc7Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc7Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc8 = new Phrase(
				"The entire treatment order amount will be payable in advance, unless explicity stated otherwise in writing.",
				font9);
		PdfPCell tc8Cell = new PdfPCell(tc8);
		tc8Cell.setBorder(0);
		tc8Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc8Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc9 = new Phrase(
				"We do not accept any financial liability for any damages caused during the course of our treatments or service, "
						+ "unless resulting due to wrongful force applied by our technicians.",
				font9);
		PdfPCell tc9Cell = new PdfPCell(tc9);
		tc9Cell.setFixedHeight(10);
		tc9Cell.setBorder(0);
		tc9Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc9Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		Phrase tc10 = new Phrase(
				"We take no responsiblity for any damage of furniture / fixtures or property, which occurs due to circumstances beyond our control.",
				font9);
		PdfPCell tc10Cell = new PdfPCell(tc10);
		
		tc10Cell.setBorder(0);
		tc10Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc10Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc11 = new Phrase(
				"Our staff's (technicians, supervisor etc.) actions are governed and subject to company policy. They must not be subjected to any force undue pressure,under "
						+ "any circumstances ", font9);
		PdfPCell tc11Cell = new PdfPCell(tc11);
		tc11Cell.setBorder(0);
		tc11Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc11Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc12 = new Phrase(
				"For all maintanance contracts, the copy of maintenance contracts must be signed and received by us along with the entire payment to enable us to commence service.",
				font9);
		PdfPCell tc12Cell = new PdfPCell(tc12);
		tc12Cell.setBorder(0);
		tc12Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc12Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc13 = new Phrase(
				"Interest @18% per annum will be chargeable for all payments received beyond due date untill the payment is received by us.",
				font9);
		PdfPCell tc13Cell = new PdfPCell(tc13);
		tc13Cell.setBorder(0);
		tc13Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc13Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc14 = new Phrase(
				"The 2nd / next scheduled service will not be carried out if our payment are not made as per agreement.",
				font9);
		PdfPCell tc14Cell = new PdfPCell(tc14);
		tc14Cell.setBorder(0);
		tc14Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc14Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		

		Phrase tc15 = new Phrase(
				"Non-payment beyond the 60days of due date will lead to cancellation of order and subject to further leagal action.",
				font9);
		PdfPCell tc15Cell = new PdfPCell(tc15);
		tc15Cell.setBorder(0);
		tc15Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc15Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc16 = new Phrase(
				"Servie tax will be charged for all services rendered as per goverment rules and regulation.",
				font9);
		PdfPCell tc16Cell = new PdfPCell(tc16);
		tc16Cell.setBorder(0);
		tc16Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc16Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		

		Phrase blnk = new Phrase("     ", font9);
		PdfPCell blnkCell = new PdfPCell();
		blnkCell.addElement(blnk);
		blnkCell.setBorder(0);
		blnkCell.setFixedHeight(6);
		
		
		Phrase blnk1 = new Phrase("     ", font9);
		PdfPCell blnk1Cell = new PdfPCell();
		blnk1Cell.addElement(blnk1);
		blnk1Cell.setBorder(0);

		
		Phrase sr1 = new Phrase("1.", font9);
		Paragraph sr1Para = new Paragraph();
		sr1Para.add(sr1);
		sr1Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr1Cell = new PdfPCell(sr1Para);
		sr1Cell.setBorder(0);
//		sr1Cell.setFixedHeight(10);

		Phrase sr2 = new Phrase("2.", font9);
		Paragraph sr2Para = new Paragraph();
		sr2Para.add(sr2);
//		sr1Cell.setFixedHeight(10);

		sr2Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr2Cell = new PdfPCell(sr2Para);
		sr2Cell.setBorder(0);
		sr1Cell.setFixedHeight(10);

		Phrase sr3 = new Phrase("3.", font9);
		Paragraph sr3Para = new Paragraph();
		sr3Para.add(sr3);
		sr3Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr3Cell = new PdfPCell(sr3Para);
		sr3Cell.setBorder(0);

		Phrase sr4 = new Phrase("4.", font9);
		Paragraph sr4Para = new Paragraph();
		sr4Para.add(sr4);
		sr4Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr4Cell = new PdfPCell(sr4Para);
		sr4Cell.setBorder(0);

		Phrase sr5 = new Phrase("5.", font9);
		Paragraph sr5Para = new Paragraph();
		sr5Para.add(sr5);
		sr5Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr5Cell = new PdfPCell(sr5Para);
		sr5Cell.setBorder(0);

		Phrase sr6 = new Phrase("6.", font9);
		Paragraph sr6Para = new Paragraph();
		sr6Para.add(sr6);
		sr6Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr6Cell = new PdfPCell(sr6Para);
		sr6Cell.setBorder(0);

		Phrase sr7 = new Phrase("7.", font9);
		Paragraph sr7Para = new Paragraph();
		sr7Para.add(sr7);
		sr7Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr7Cell = new PdfPCell(sr7Para);
		sr7Cell.setBorder(0);

		Phrase sr8 = new Phrase("8.", font9);
		Paragraph sr8Para = new Paragraph();
		sr8Para.add(sr8);
		sr8Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr8Cell = new PdfPCell(sr8Para);
		sr8Cell.setBorder(0);

		Phrase sr9 = new Phrase("9.", font9);
		Paragraph sr9Para = new Paragraph();
		sr9Para.add(sr9);
		sr9Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr9Cell = new PdfPCell(sr9Para);
		sr9Cell.setBorder(0);

		Phrase sr10 = new Phrase("10.", font9);
		Paragraph sr10Para = new Paragraph();
		sr10Para.add(sr10);
		sr10Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr10Cell = new PdfPCell(sr10Para);
		sr10Cell.setBorder(0);

		Phrase sr11 = new Phrase("11.", font9);
		Paragraph sr11Para = new Paragraph();
		sr11Para.add(sr11);
		sr11Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr11Cell = new PdfPCell(sr11Para);
		sr11Cell.setBorder(0);

		Phrase sr12 = new Phrase("12.", font9);
		Paragraph sr12Para = new Paragraph();
		sr12Para.add(sr12);
		sr12Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr12Cell = new PdfPCell(sr12Para);
		sr12Cell.setBorder(0);

		Phrase sr13 = new Phrase("13.", font9);
		Paragraph sr13Para = new Paragraph();
		sr13Para.add(sr13);
		sr13Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr13Cell = new PdfPCell(sr13Para);
		sr13Cell.setBorder(0);

		Phrase sr14 = new Phrase("14.", font9);
		Paragraph sr14Para = new Paragraph();
		sr14Para.add(sr14);
		sr14Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr14Cell = new PdfPCell(sr14Para);
		sr14Cell.setBorder(0);

		Phrase sr15 = new Phrase("15.", font9);
		Paragraph sr15Para = new Paragraph();
		sr15Para.add(sr15);
		sr15Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr15Cell = new PdfPCell(sr15Para);
		sr15Cell.setBorder(0);

		Phrase sr16 = new Phrase("16.", font9);
		Paragraph sr16Para = new Paragraph();
		sr16Para.add(sr16);
		sr16Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr16Cell = new PdfPCell(sr16Para);
		sr16Cell.setBorder(0);

		Phrase nxtparatitle = new Phrase("FOR ONE TIME SERVICES", font9boldul);
//		Phrase space = new Phrase ("  ");
		PdfPCell nxtparatitleCell = new PdfPCell(nxtparatitle); 
		nxtparatitleCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		nxtparatitleCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		nxtparatitleCell.setBorderWidthBottom(0);
//		line1Cell.setBorderWidthTop(0);
		
		
		/// align Format 
		Phrase line1 = new Phrase(
				"This is a one-time service only and we do not provide any guarantee of complete pest extermination."
				+ "Complaints of pest re-infestation within 7 days will be attended to at no extra cost."
				+ "However pest re-infestation after that will be chargeable extra as per applicable rates."
				+ "One time treatment must be paid at the time of service.",
				font9);
		
		PdfPCell line1Cell = new PdfPCell(line1);
		Paragraph linePara = new Paragraph();
		linePara.add(line1);
		linePara.setAlignment(Element.ALIGN_JUSTIFIED);
		line1Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		line1Cell.setBorderWidthBottom(0);
		line1Cell.setBorderWidthTop(0);
		
		
		Phrase line2 = new Phrase (" ",font9);
		
		PdfPCell line2Cell = new PdfPCell(line2); 
		line2Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		line2Cell.setBorderWidthBottom(0);
		line2Cell.setBorderWidthTop(0);
		
        Phrase line3 = new Phrase (" ",font9);
		
		PdfPCell line3Cell = new PdfPCell(line3); 
		line3Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
//		line3Cell.setBorderWidthBottom(0);
		line3Cell.setBorderWidthTop(0);

		PdfPTable tcTable = new PdfPTable(2);
		tcTable.setWidthPercentage(80);

		tcTable.addCell(sr1Cell);
		tcTable.addCell(tc1Cell);
		tcTable.addCell(blnkCell);  
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr2Cell);
		tcTable.addCell(tc2Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr3Cell);
		tcTable.addCell(tc3Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr4Cell);
		tcTable.addCell(tc4Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr5Cell);
		tcTable.addCell(tc5Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr6Cell);
		tcTable.addCell(tc6Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr7Cell);
		tcTable.addCell(tc7Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr8Cell);
		tcTable.addCell(tc8Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr9Cell);
		tcTable.addCell(tc9Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr10Cell);
		tcTable.addCell(tc10Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr11Cell);
		tcTable.addCell(tc11Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr12Cell);
		tcTable.addCell(tc12Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr13Cell);
		tcTable.addCell(tc13Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr14Cell);
		tcTable.addCell(tc14Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr15Cell);
		tcTable.addCell(tc15Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr16Cell);
		tcTable.addCell(tc16Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
		
		//////////////////////////////
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);

		try {
			
			tcTable.setWidths(new float[] { 5, 95 });
			document.add(tcTable);
		
		} catch (DocumentException e) {

			e.printStackTrace();
		}

//		Paragraph notePara = new Paragraph();
//		notePara.add(nxtparatitle);
//		notePara.add(Chunk.NEWLINE);
//		notePara.add(line1);
//		notePara.add(Chunk.NEWLINE);
//		notePara.add(line2);
//		notePara.add(Chunk.NEWLINE);
//		notePara.add(line3);
//		notePara.add(Chunk.NEWLINE);
//		notePara.add(line4);
//		notePara.add(Chunk.NEWLINE);
//		notePara.add(Chunk.NEWLINE);

//		notePara.setAlignment(Element.ALIGN_JUSTIFIED);
//		PdfPCell noteParaCell = new PdfPCell();
//		noteParaCell.addElement(notePara);
		PdfPTable noteTable = new PdfPTable(1);
		noteTable.setWidthPercentage(100);
		noteTable.addCell(nxtparatitleCell);
		noteTable.addCell(line2Cell);
		noteTable.addCell(line1Cell);
		noteTable.addCell(line3Cell);

		try {
			document.add(noteTable);
		} catch (DocumentException e) {

			e.printStackTrace();
		}

	}

	private void createFooterTable()
	{
		String addressline1 = "";

		if (comp.getAddress().getAddrLine2() != null) {
			addressline1 = comp.getAddress().getAddrLine1() + ", "
					+ comp.getAddress().getAddrLine2() +", ";
		} else {
			addressline1 = comp.getAddress().getAddrLine1()+", ";
		}

		String locality = null;
		if ((!comp.getAddress().getLandmark().equals(""))
				&& (comp.getAddress().getLocality().equals("") == false)) {
			System.out.println("inside both null condition1");
			locality = (comp.getAddress().getLandmark() + ", "
					+ comp.getAddress().getLocality() + ", "
					+ comp.getAddress().getCity() + " "
					+ comp.getAddress().getPin() + ". "
//					+ comp.getAddress().getState() + ", "
					+ comp.getAddress().getCountry()+". ");
		}
		else if ((!comp.getAddress().getLandmark().equals(""))
				&& (comp.getAddress().getLocality().equals("") == true)) 
		{
			System.out.println("inside both null condition 2");
			locality = (comp.getAddress().getLandmark() + ", "
					+ comp.getAddress().getCity() + " "
					+ comp.getAddress().getPin() + ". "
//					+ comp.getAddress().getState() + ", " 
					+ comp.getAddress().getCountry()+".");
		}

		else if ((comp.getAddress().getLandmark().equals(""))
				&& (comp.getAddress().getLocality().equals("") == false))
		{
			System.out.println("inside both null condition 3");
			locality = (comp.getAddress().getLocality() + " , "
					+ comp.getAddress().getCity() + " "
					+ comp.getAddress().getPin() + ". "
//					+ comp.getAddress().getState() + ", "
					+ comp.getAddress().getCountry()+".");
		}
		else if ((comp.getAddress().getLandmark().equals(""))
				&& (comp.getAddress().getLocality().equals("") == true)) 
		  {
			System.out.println("inside both null condition 4");
			locality = (comp.getAddress().getCity() + " "
					+ comp.getAddress().getPin() + ". "
					+ comp.getAddress().getState() + " , " 
					+ comp.getAddress().getCountry()+". " );
		}
/////////////////////////// As per the mail reqiured format hardcoded in contacts details ////////////////////////////////     
		
		String contactinfo = "Tel.: (91-22) 2266 1091, 2262 5376 Fax : (91-22) 2266 0810 ";
		
                ///////////////////////////////   END HERE  ////////////////////////////////  
		
//		String contactinfo;
//		if (comp.getLandline() != 0) {
//			contactinfo = ("Tel: " + comp.getLandline() + " / "
//					+ comp.getCellNumber1() + "  FAX : " + comp.getFaxNumber());
//		} else {
//			contactinfo = (" Tel: " + comp.getCellNumber1() + "  FAX : " + comp
//					.getFaxNumber());
//		}
//		 System.out.println("landline no "+comp.getLandline());
//		 System.out.println("Cell no1 "+comp.getCellNumber1());
//		 System.out.println("Cell no2 "+comp.getCellNumber2());
//		 System.out.println("fax no "+comp.getFaxNumber());
		 
//		if (comp.getLandline()!=0 && comp.getCellNumber2()!= 0  && comp.getFaxNumber()!=null )
//		{
//			// tel no Format 
//			contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getCellNumber2() +" / "+ comp.getLandline()
//					 + "  Fax : " + comp.getFaxNumber());
//		}
//		else if (comp.getLandline() == 0 && comp.getCellNumber2() != 0 && comp.getFaxNumber()!=null ) 
//		{
//			contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getCellNumber2()
//					+ "  Fax : " + comp.getFaxNumber());
//		}
//		else if(comp.getLandline() != 0  && comp.getCellNumber2() == 0 && comp.getFaxNumber()!=null ){
//			contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getLandline()
//					 + "  Fax : " + comp.getFaxNumber());
//		}
//		else if(comp.getLandline() != 0 && comp.getCellNumber2() != 0 && comp.getFaxNumber()==null)
//		{
//			contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getCellNumber2()+" / "+ comp.getLandline() 
//					+ "  Fax : "  );	
//		}
//		else if(comp.getLandline() != 0 && comp.getCellNumber2() == 0 && comp.getFaxNumber()==null )
//		{
//			contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getLandline()
//					+ "  Fax : "  );	
//		}
//		else if(comp.getLandline() == 0 && comp.getCellNumber2() != 0 && comp.getFaxNumber()==null )
//		{
//			contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getCellNumber2()
//					 + "  Fax : "  );	
//		}
//		else if(comp.getLandline() == 0 && comp.getCellNumber2() == 0 && comp.getFaxNumber()==null)
//		{
//			contactinfo = ("Tel. : "  + comp.getCellNumber1() 
//					 + "  Fax : "  );	
//		}
		////////////////////////////////////
		
		Phrase footer1 = new Phrase("Regd. Office :  "+ addressline1 + locality, font9);
		Phrase footer2 = new Phrase(contactinfo, font9);
		Phrase footer3 = new Phrase(" E-Mail : " + comp.getEmail().trim()
				+ " Website : " + comp.getWebsite(), font10);

		Paragraph mycomPara = new Paragraph();

		
		mycomPara.add(Chunk.NEWLINE);
		mycomPara.add(Chunk.NEWLINE);
		mycomPara.add(Chunk.NEWLINE);
		mycomPara.add(footer1);
		mycomPara.add(Chunk.NEWLINE);
		mycomPara.add(footer2);
		mycomPara.add(Chunk.NEWLINE);
		mycomPara.add(footer3);
		mycomPara.add(Chunk.NEWLINE);
		mycomPara.add(Chunk.NEWLINE);
		mycomPara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell mycomCell = new PdfPCell();
		mycomCell.addElement(mycomPara);
		mycomCell.setBorder(0);

		PdfPTable parentTbl = new PdfPTable(1);
		parentTbl.setWidthPercentage(100);

		parentTbl.addCell(mycomCell);

		try {
			document.add(parentTbl);
			document.add(Chunk.NEXTPAGE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// /////////////////////////////// 2nd Page Wood Borer mgnt methods   ////////////////////////////////////////

	private void createwbHeaderTable() 
	{
		
		// Image image1=Image.getInstance("images/iso9001e.png");
		// image1.scalePercent(40f);
		// image1.scaleAbsoluteWidth(55f);
		// image1.setAbsolutePosition(450f,750f);
	
		// Image image1=Image.getInstance("images/ISO14001e.png");
		// image1.scalePercent(40f);
		// image1.scaleAbsoluteWidth(55f);
		// image1.setAbsolutePosition(88f,750f);
		

		Phrase header = new Phrase(""
				+ comp.getBusinessUnitName().toUpperCase(), font16bold);
		Phrase title = new Phrase("WOOD BORER MANAGEMENT ", font10boldul);

		Paragraph header1Para = new Paragraph();
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(header);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(title);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.setAlignment(Element.ALIGN_CENTER);

		PdfPCell Header1ParaCell = new PdfPCell();
		Header1ParaCell.addElement(header1Para);
		Header1ParaCell.setBorder(0);

		PdfPTable headerTable = new PdfPTable(1);
		headerTable.setWidthPercentage(100);
		headerTable.addCell(Header1ParaCell);

		try {
			document.add(headerTable);
		} catch (DocumentException e) {

			e.printStackTrace();
		}
	}
	
	private void createwbTermsAndConditions()
	{

		Phrase tc1 = new Phrase(
				"This order is to prevent & arrest the rapid spreador escalation of the existing wood borer problem.",
				font9);
		PdfPCell tc1Cell = new PdfPCell(tc1);
		tc1Cell.setBorder(0);
		tc1Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc1Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc2 = new Phrase(
				"No guarantee of complete extermination will be provided under this order.",
				font9);
		PdfPCell tc2Cell = new PdfPCell(tc2);
		tc2Cell.setBorder(0);
		tc2Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc2Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);  

		Phrase tc3 = new Phrase(
				"Borers are a hidden infestaion and can only detected when visible signs of infestation, "
						+ "manifest themselves like power formation, holes in wooden structures etc.",
				font9);
		PdfPCell tc3Cell = new PdfPCell(tc3);
		tc3Cell.setBorder(0);
		tc3Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc3Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc4 = new Phrase(
				"Cross infestation occuring due to any affected / damaged item / article (external source) "
						+ "brought into the property will not be our responsibility.",
				font9);
		PdfPCell tc4Cell = new PdfPCell(tc4);
		tc4Cell.setBorder(0);
		tc4Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc4Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc5 = new Phrase(
				"We will not be responsible (financially or otherwise) for any damaged to you & your "
						+ "property caused due to wood borers.", font9);
		PdfPCell tc5Cell = new PdfPCell(tc5);
		tc5Cell.setBorder(0);
		tc5Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc5Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc6 = new Phrase(
				"Any part of the property which is damaged beyond curable limits and requires replacement "
						+ "/disposal must be done. We hold no liability for any effected caused due to  non-"
						+ "compliance of the same.", font9);
		PdfPCell tc6Cell = new PdfPCell(tc6);
		tc6Cell.setBorder(0);
		tc6Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc6Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc7 = new Phrase(
				"Any part of the property which is damaged beyond curable limits and requires opening "
						+ "up or breaking of the wooden structure therein must be done. if the same requires "
						+ "major treatment, then the charges for the same will extra. We hold no liability for any "
						+ "effects caused due to non-compliance of the same.",
				font9);
		PdfPCell tc7Cell = new PdfPCell(tc7);
		tc7Cell.setBorder(0);
		tc7Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc7Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc8 = new Phrase(
				"Wood borers are an inherent infestation of wood / timber. They can remain dormant"
						+ " (inactive) for up to 8 years. We are not responsible for activity observed after our treatment "
						+ "order is not in effect (not operational).", font9);
		PdfPCell tc8Cell = new PdfPCell(tc8);
		tc8Cell.setBorder(0);
		tc8Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc8Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase blnk = new Phrase("     ", font9);
		PdfPCell blnkCell = new PdfPCell();
		blnkCell.addElement(blnk);
		blnkCell.setBorder(0);

		Phrase sr1 = new Phrase("1.", font9);
		Paragraph sr1Para = new Paragraph();
		sr1Para.add(sr1);
		sr1Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr1Cell = new PdfPCell(sr1Para);
		sr1Cell.setBorder(0);

		Phrase sr2 = new Phrase("2.", font9);
		Paragraph sr2Para = new Paragraph();
		sr2Para.add(sr2);

		sr2Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr2Cell = new PdfPCell(sr2Para);
		sr2Cell.setBorder(0);

		Phrase sr3 = new Phrase("3.", font9);
		Paragraph sr3Para = new Paragraph();
		sr3Para.add(sr3);
		sr3Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr3Cell = new PdfPCell(sr3Para);
		sr3Cell.setBorder(0);

		Phrase sr4 = new Phrase("4.", font9);
		Paragraph sr4Para = new Paragraph();
		sr4Para.add(sr4);
		sr4Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr4Cell = new PdfPCell(sr4Para);
		sr4Cell.setBorder(0);

		Phrase sr5 = new Phrase("5.", font9);
		Paragraph sr5Para = new Paragraph();
		sr5Para.add(sr5);
		sr5Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr5Cell = new PdfPCell(sr5Para);
		sr5Cell.setBorder(0);

		Phrase sr6 = new Phrase("6.", font9);
		Paragraph sr6Para = new Paragraph();
		sr6Para.add(sr6);
		sr6Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr6Cell = new PdfPCell(sr6Para);
		sr6Cell.setBorder(0);

		Phrase sr7 = new Phrase("7.", font9);
		Paragraph sr7Para = new Paragraph();
		sr7Para.add(sr7);
		sr7Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr7Cell = new PdfPCell(sr7Para);
		sr7Cell.setBorder(0);

		Phrase sr8 = new Phrase("8.", font9);
		Paragraph sr8Para = new Paragraph();
		sr8Para.add(sr8);
		sr8Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr8Cell = new PdfPCell(sr8Para);
		sr8Cell.setBorder(0);

		PdfPTable tcTable = new PdfPTable(2);
		tcTable.setWidthPercentage(80);

		tcTable.addCell(sr1Cell);
		tcTable.addCell(tc1Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr2Cell);
		tcTable.addCell(tc2Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr3Cell);
		tcTable.addCell(tc3Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr4Cell);
		tcTable.addCell(tc4Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr5Cell);
		tcTable.addCell(tc5Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr6Cell);
		tcTable.addCell(tc6Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr7Cell);
		tcTable.addCell(tc7Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr8Cell);
		tcTable.addCell(tc8Cell);

		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		
		///////////////////////////////////////
		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
		
		

		PdfPCell tcTblCell = new PdfPCell();
		tcTblCell.addElement(tcTable);
		tcTblCell.setBorder(0);

		try {
			tcTable.setWidths(new float[] { 5, 95 });
			document.add(tcTable);
		} catch (DocumentException e) {

			e.printStackTrace();
		}

	}

	
	// /////////////////////////////// 3rd Page BedBugs mgnt methods   ////////////////////////////////////////

	private void createbbHeaderTable() {

		Phrase header = new Phrase(""
				+ comp.getBusinessUnitName().toUpperCase(), font16bold);
		Phrase title = new Phrase("BED BUGS ", font10boldul);

		Paragraph header1Para = new Paragraph();
		//
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(header);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(title);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.setAlignment(Element.ALIGN_CENTER);

		PdfPCell Header1ParaCell = new PdfPCell();
		Header1ParaCell.addElement(header1Para);
		Header1ParaCell.setBorder(0);

		PdfPTable headerTable = new PdfPTable(1);
		headerTable.setWidthPercentage(100);
		headerTable.addCell(Header1ParaCell);

		try {
			document.add(headerTable);
		} catch (DocumentException e) {

			e.printStackTrace();
		}

	}

	private void createbbTermsAndConditions() 
	{

		Phrase tc1 = new Phrase( "Treatment for BED-BUGS can be either taken in set of 2 treatment or an annual "
						+ "treatment order with frequency may be recommended depending on the infestation levels. ",font9);
		
		PdfPCell tc1Cell = new PdfPCell(tc1);
		tc1Cell.setBorder(0);
		tc1Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc1Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc2 = new Phrase("Only spraying treatment can be carried out for bed bug problems .",font9);
		PdfPCell tc2Cell = new PdfPCell(tc2);
		tc2Cell.setBorder(0);
		tc2Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc2Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc3 = new Phrase(
				"The entire premise has to be treated at one time and not in parts, for controlling "
						+ "bed bugs.", font9);
		PdfPCell tc3Cell = new PdfPCell(tc3);
		tc3Cell.setBorder(0);
		tc3Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc3Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc4 = new Phrase(
				"Upholstery, furniture mattresses, etc must be made available for treatment.",
				font9);
		PdfPCell tc4Cell = new PdfPCell(tc4);
		tc4Cell.setBorder(0);
		tc4Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc4Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc5 = new Phrase(
				"Reinfestation arising due to non- availability of any area during our treatment will "
						+ "not be our responsibility and complaints will not be entertained.",
				font9);
		PdfPCell tc5Cell = new PdfPCell(tc5);
		tc5Cell.setBorder(0);
		tc5Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc5Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc6 = new Phrase(
				"We will try  and provide services to reduce the problem of bed bugs to the best possible "
						+ "extent.", font9);
		PdfPCell tc6Cell = new PdfPCell(tc6);
		tc6Cell.setBorder(0);
		tc6Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc6Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase blnk = new Phrase("     ", font9);
		PdfPCell blnkCell = new PdfPCell();
		blnkCell.addElement(blnk);
		blnkCell.setBorder(0);
		blnkCell.setFixedHeight(3);
		
		Phrase blnk1 = new Phrase("     ", font9);
		PdfPCell blnk1Cell = new PdfPCell();
		blnk1Cell.addElement(blnk1);
		blnk1Cell.setBorder(0);

		Phrase sr1 = new Phrase("1.", font9);
		Paragraph sr1Para = new Paragraph();
		sr1Para.add(sr1);
		sr1Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr1Cell = new PdfPCell(sr1Para);
		sr1Cell.setBorder(0);

		Phrase sr2 = new Phrase("2.", font9);
		Paragraph sr2Para = new Paragraph();
		sr2Para.add(sr2);

		sr2Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr2Cell = new PdfPCell(sr2Para);
		sr2Cell.setBorder(0);

		Phrase sr3 = new Phrase("3.", font9);
		Paragraph sr3Para = new Paragraph();
		sr3Para.add(sr3);
		sr3Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr3Cell = new PdfPCell(sr3Para);
		sr3Cell.setBorder(0);

		Phrase sr4 = new Phrase("4.", font9);
		Paragraph sr4Para = new Paragraph();
		sr4Para.add(sr4);
		sr4Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr4Cell = new PdfPCell(sr4Para);
		sr4Cell.setBorder(0);

		Phrase sr5 = new Phrase("5.", font9);
		Paragraph sr5Para = new Paragraph();
		sr5Para.add(sr5);
		sr5Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr5Cell = new PdfPCell(sr5Para);
		sr5Cell.setBorder(0);

		Phrase sr6 = new Phrase("6.", font9);
		Paragraph sr6Para = new Paragraph();
		sr6Para.add(sr6);
		sr6Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr6Cell = new PdfPCell(sr6Para);
		sr6Cell.setBorder(0);

		PdfPTable tcTable = new PdfPTable(2);
		tcTable.setWidthPercentage(80);

		tcTable.addCell(sr1Cell);
		tcTable.addCell(tc1Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr2Cell);
		tcTable.addCell(tc2Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr3Cell);
		tcTable.addCell(tc3Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr4Cell);
		tcTable.addCell(tc4Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr5Cell);
		tcTable.addCell(tc5Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr6Cell);
		tcTable.addCell(tc6Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);

		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		
		
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
		
		///////////////////////////////

		PdfPCell tcTblCell = new PdfPCell();
		tcTblCell.addElement(tcTable);
		tcTblCell.setBorder(0);

		try {
			tcTable.setWidths(new float[] { 5, 95 });
			document.add(tcTable);
		} catch (DocumentException e) {

			e.printStackTrace();
		}

	}

	// /////////////////////////////// 4th Page Termite mgnt methods
	// ////////////////////////////////////////

	private void createtmHeaderTable() {

		Phrase header = new Phrase(""
				+ comp.getBusinessUnitName().toUpperCase(), font16bold);
		Phrase title = new Phrase("TERMITE MANAGEMENT ", font10boldul);

		Paragraph header1Para = new Paragraph();

		header1Para.add(Chunk.NEWLINE);
		header1Para.add(header);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(title);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.setAlignment(Element.ALIGN_CENTER);

		PdfPCell Header1ParaCell = new PdfPCell();
		Header1ParaCell.addElement(header1Para);
		Header1ParaCell.setBorder(0);

		PdfPTable headerTable = new PdfPTable(1);
		headerTable.setWidthPercentage(100);
		headerTable.addCell(Header1ParaCell);

		try {
			document.add(headerTable);
		} catch (DocumentException e) {

			e.printStackTrace();
		}
	}

	private void createtmTermsAndConditions() 
	 
	{

		Phrase tc1 = new Phrase(
				"This order is to prevent & arrest the rapid spread or escalation of the existing termite "
						+ "problem.", font9);
		PdfPCell tc1Cell = new PdfPCell(tc1);
		tc1Cell.setBorder(0);
		tc1Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc1Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc2 = new Phrase(
				"No guarantee of complete extermination will be provided under this order.",
				font9);
		PdfPCell tc2Cell = new PdfPCell(tc2);
		tc2Cell.setBorder(0);
		tc2Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc2Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc3 = new Phrase(
				"Termites are a hidden infestaion and can only detected when visible signs of infestation,"
						+ "manifest themselves like warping of wood, discoloration of tiles , mud like lines appearing "
						+ "etc.", font9);
		PdfPCell tc3Cell = new PdfPCell(tc3);
		tc3Cell.setBorder(0);
		tc3Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc3Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc4 = new Phrase(
				"Reinfestation occuring due to persistent water leakage in the area will not be our "
						+ "responsibility.", font9);
		PdfPCell tc4Cell = new PdfPCell(tc4);
		tc4Cell.setBorder(0);
		tc4Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc4Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc5 = new Phrase(
				"Cross infestation occuring due to any affected / damaged item / article (external source)"
						+ "brought into the proerty will not be our responsibility.",
				font9);
		PdfPCell tc5Cell = new PdfPCell(tc5);
		tc5Cell.setBorder(0);
		tc5Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc5Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc6 = new Phrase(
				"We will not be responsible (financially or otherwise) for any damages caused to you or your "
						+ "property due to termites.", font9);
		PdfPCell tc6Cell = new PdfPCell(tc6);
		tc6Cell.setBorder(0);
		tc6Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc6Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc7 = new Phrase(
				"Any part of the property which is damaged beyond curable limits and requires replacement "
						+ "/ disposal must be done. We hold no liability for any effects caused due to  non-"
						+ "compliance of the same.", font9);
		PdfPCell tc7Cell = new PdfPCell(tc7);
		tc7Cell.setBorder(0);
		tc7Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc7Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc8 = new Phrase(
				"Any part of the property which is damaged beyond curable limits and requires opening "
						+ "up or breaking of the structure therein must be done. if the same requires "
						+ "major treatment, then the charges for the same will extra. We hold no liability for any "
						+ "effects caused due to non-compliance of the same.",
				font9);
		PdfPCell tc8Cell = new PdfPCell(tc8);
		tc8Cell.setBorder(0);
		tc8Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc8Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase blnk = new Phrase("       ", font9);
		PdfPCell blnkCell = new PdfPCell();
		blnkCell.addElement(blnk);
		blnkCell.setBorder(0);
		blnkCell.setFixedHeight(3);
		
		Phrase blnk1 = new Phrase("       ", font9);
		PdfPCell blnk1Cell = new PdfPCell();
		blnk1Cell.addElement(blnk1);
		blnk1Cell.setBorder(0);
	

		Phrase sr1 = new Phrase("1.", font9);
		Paragraph sr1Para = new Paragraph();
		sr1Para.add(sr1);
		sr1Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr1Cell = new PdfPCell(sr1Para);
		sr1Cell.setBorder(0);

		Phrase sr2 = new Phrase("2.", font9);
		Paragraph sr2Para = new Paragraph();
		sr2Para.add(sr2);

		sr2Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr2Cell = new PdfPCell(sr2Para);
		sr2Cell.setBorder(0);

		Phrase sr3 = new Phrase("3.", font9);
		Paragraph sr3Para = new Paragraph();
		sr3Para.add(sr3);
		sr3Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr3Cell = new PdfPCell(sr3Para);
		sr3Cell.setBorder(0);

		Phrase sr4 = new Phrase("4.", font9);
		Paragraph sr4Para = new Paragraph();
		sr4Para.add(sr4);
		sr4Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr4Cell = new PdfPCell(sr4Para);
		sr4Cell.setBorder(0);

		Phrase sr5 = new Phrase("5.", font9);
		Paragraph sr5Para = new Paragraph();
		sr5Para.add(sr5);
		sr5Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr5Cell = new PdfPCell(sr5Para);
		sr5Cell.setBorder(0);

		Phrase sr6 = new Phrase("6.", font9);
		Paragraph sr6Para = new Paragraph();
		sr6Para.add(sr6);
		sr6Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr6Cell = new PdfPCell(sr6Para);
		sr6Cell.setBorder(0);

		Phrase sr7 = new Phrase("7.", font9);
		Paragraph sr7Para = new Paragraph();
		sr7Para.add(sr7);
		sr7Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr7Cell = new PdfPCell(sr7Para);
		sr7Cell.setBorder(0);

		Phrase sr8 = new Phrase("8.", font9);
		Paragraph sr8Para = new Paragraph();
		sr8Para.add(sr8);
		sr8Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr8Cell = new PdfPCell(sr8Para);
		sr8Cell.setBorder(0);

		PdfPTable tcTable = new PdfPTable(2);
		tcTable.setWidthPercentage(80);

		tcTable.addCell(sr1Cell);
		tcTable.addCell(tc1Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr2Cell);
		tcTable.addCell(tc2Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr3Cell);
		tcTable.addCell(tc3Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr4Cell);
		tcTable.addCell(tc4Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr5Cell);
		tcTable.addCell(tc5Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr6Cell);
		tcTable.addCell(tc6Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr7Cell);
		tcTable.addCell(tc7Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr8Cell);
		tcTable.addCell(tc8Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);

		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		
		/////////////////////////////////////////////////
		
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		
		
		
		

		PdfPCell tcTblCell = new PdfPCell();
		tcTblCell.addElement(tcTable);
		tcTblCell.setBorder(0);

		try {
			tcTable.setWidths(new float[] { 5, 95 });
			document.add(tcTable);
		} catch (DocumentException e) {

			e.printStackTrace();
		}

	}

	// /////////////////////////////// 5th Page fly insect pest mgnt methods
	// ////////////////////////////////////////

	private void createfipHeaderTable() 
	{

		Phrase header = new Phrase(""
				+ comp.getBusinessUnitName().toUpperCase(), font16bold);
		Phrase title = new Phrase("FLYING INSECT PEST MANAGEMENT ",
				font10boldul);

		Paragraph header1Para = new Paragraph();

		header1Para.add(Chunk.NEWLINE);
		header1Para.add(header);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(title);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.setAlignment(Element.ALIGN_CENTER);

		PdfPCell Header1ParaCell = new PdfPCell();
		Header1ParaCell.addElement(header1Para);
		Header1ParaCell.setBorder(0);

		PdfPTable headerTable = new PdfPTable(1);
		headerTable.setWidthPercentage(100);
		headerTable.addCell(Header1ParaCell);

		try {
			document.add(headerTable);
		} catch (DocumentException e) {

			e.printStackTrace();
		}
	}

	private void createfipTermsAndConditions() 
	{

		Phrase tc1 = new Phrase(
				"It consists of control of house flies, mosquitoes,fruit flies and drain flies only.",
				font9);
		PdfPCell tc1Cell = new PdfPCell(tc1);
		tc1Cell.setBorder(0);
		tc1Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc1Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc2 = new Phrase(
				"Flying insects are related to the condition of external surroundings and hence,"
						+ "only reasonable control can be achieved by treatments done internally.",
				font9);
		PdfPCell tc2Cell = new PdfPCell(tc2);
		tc2Cell.setBorder(0);
		tc2Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc2Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc3 = new Phrase(
				"All preventive measures suggested by us must be implemented.",
				font9);
		PdfPCell tc3Cell = new PdfPCell(tc3);
		tc3Cell.setBorder(0);
		tc3Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc3Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc4 = new Phrase(
				"Reinfestation occuring internally between two scheduled services due to external "
						+ "conditions will be attened but at an additional cost.",
				font9);
		PdfPCell tc4Cell = new PdfPCell(tc4);
		tc4Cell.setBorder(0);
		tc4Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc4Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc5 = new Phrase(
				"Combination of residual spraying and ULV spraying will be carried out internally. ",
				font9);
		PdfPCell tc5Cell = new PdfPCell(tc5);
		tc5Cell.setBorder(0);
		tc5Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc5Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc6 = new Phrase(
				"Thermal fogging will be carried out externally during pre-dawn or post dusk period only.",
				font9);
		PdfPCell tc6Cell = new PdfPCell(tc6);
		tc6Cell.setBorder(0);
		tc6Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc6Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc7 = new Phrase(
				"Thermal fogging has temporary effect which usually lasts for 45 minutes to an hour.",
				font9);
		PdfPCell tc7Cell = new PdfPCell(tc7);
		tc7Cell.setBorder(0);
		tc7Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc7Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc8 = new Phrase(
				"Regular thorough cleaning of all drains / sewage areas must be done to prevent "
						+ "Reinfestation of drain flies. Complaints arising due to the lack of the above will be "
						+ "attented as additional service with extra costs.",
				font9);
		PdfPCell tc8Cell = new PdfPCell(tc8);
		tc8Cell.setBorder(0);
		tc8Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc8Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase blnk = new Phrase("     ", font9);
		PdfPCell blnkCell = new PdfPCell();
		blnkCell.addElement(blnk);
		blnkCell.setBorder(0);

		Phrase sr1 = new Phrase("1.", font9);
		Paragraph sr1Para = new Paragraph();
		sr1Para.add(sr1);
		sr1Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr1Cell = new PdfPCell(sr1Para);
		sr1Cell.setBorder(0);

		Phrase sr2 = new Phrase("2.", font9);
		Paragraph sr2Para = new Paragraph();
		sr2Para.add(sr2);

		sr2Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr2Cell = new PdfPCell(sr2Para);
		sr2Cell.setBorder(0);

		Phrase sr3 = new Phrase("3.", font9);
		Paragraph sr3Para = new Paragraph();
		sr3Para.add(sr3);
		sr3Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr3Cell = new PdfPCell(sr3Para);
		sr3Cell.setBorder(0);

		Phrase sr4 = new Phrase("4.", font9);
		Paragraph sr4Para = new Paragraph();
		sr4Para.add(sr4);
		sr4Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr4Cell = new PdfPCell(sr4Para);
		sr4Cell.setBorder(0);

		Phrase sr5 = new Phrase("5.", font9);
		Paragraph sr5Para = new Paragraph();
		sr5Para.add(sr5);
		sr5Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr5Cell = new PdfPCell(sr5Para);
		sr5Cell.setBorder(0);

		Phrase sr6 = new Phrase("6.", font9);
		Paragraph sr6Para = new Paragraph();
		sr6Para.add(sr6);
		sr6Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr6Cell = new PdfPCell(sr6Para);
		sr6Cell.setBorder(0);

		Phrase sr7 = new Phrase("7.", font9);
		Paragraph sr7Para = new Paragraph();
		sr7Para.add(sr7);
		sr7Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr7Cell = new PdfPCell(sr7Para);
		sr7Cell.setBorder(0);

		Phrase sr8 = new Phrase("8.", font9);
		Paragraph sr8Para = new Paragraph();
		sr8Para.add(sr8);
		sr8Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr8Cell = new PdfPCell(sr8Para);
		sr8Cell.setBorder(0);

		PdfPTable tcTable = new PdfPTable(2);
		tcTable.setWidthPercentage(80);

		tcTable.addCell(sr1Cell);
		tcTable.addCell(tc1Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr2Cell);
		tcTable.addCell(tc2Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr3Cell);
		tcTable.addCell(tc3Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr4Cell);
		tcTable.addCell(tc4Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr5Cell);
		tcTable.addCell(tc5Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr6Cell);
		tcTable.addCell(tc6Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr7Cell);
		tcTable.addCell(tc7Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr8Cell);
		tcTable.addCell(tc8Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);

		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);  
		
		////////////////////////////////////////////
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
		

		PdfPCell tcTblCell = new PdfPCell();
		tcTblCell.addElement(tcTable);
		tcTblCell.setBorder(0);

		try {
			tcTable.setWidths(new float[] { 5, 95 });
			document.add(tcTable);
		} catch (DocumentException e) {

			e.printStackTrace();
		}

	}

	// /////////////////////////////// 5th Page General insect pest mgnt methods
	// ////////////////////////////////////////

	private void createGipHeaderTable() 
	{

		Phrase header = new Phrase(""
				+ comp.getBusinessUnitName().toUpperCase(), font16bold);
		Phrase title = new Phrase("GENERAL INSECT PEST MANAGEMENT ",
				font10boldul);

		Paragraph header1Para = new Paragraph();
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(header);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(title);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.setAlignment(Element.ALIGN_CENTER);

		PdfPCell Header1ParaCell = new PdfPCell();
		Header1ParaCell.addElement(header1Para);
		Header1ParaCell.setBorder(0);

		PdfPTable headerTable = new PdfPTable(1);
		headerTable.setWidthPercentage(100);
		headerTable.addCell(Header1ParaCell);

		try {
			document.add(headerTable);
		} catch (DocumentException e) {

			e.printStackTrace();
		}
	}

	private void createGipTermsAndConditions()
	{

		Phrase tc1 = new Phrase(
				"General insect pest control covers cockroaches, red / black ants, and silver fish.",
				font9);
		PdfPCell tc1Cell = new PdfPCell(tc1);
		tc1Cell.setBorder(0);
		tc1Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc1Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc2 = new Phrase(
				"For treatment orders involving spraying applications, the kitchens / pantries / foods operating"
						+ " areas must be emptied out / made available for treatment.", font9);
		PdfPCell tc2Cell = new PdfPCell(tc2);
		tc2Cell.setBorder(0);
		tc2Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc2Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc3 = new Phrase(
				"We will not be responsible for any re-infestation of pests if the above is not done"
						+ " and additional services required due to the non conformity of the above will be charged"
						+ " and have to be paid extra.", font9);
		PdfPCell tc3Cell = new PdfPCell(tc3);
		tc3Cell.setBorder(0);
		tc3Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc3Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc4 = new Phrase(
				"As per the conditions of the treatment order, the entire premises must be made available"
						+ " for treatment during each scheduled service.",
				font9);
		PdfPCell tc4Cell = new PdfPCell(tc4);
		tc4Cell.setBorder(0);
		tc4Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc4Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc5 = new Phrase(
				"Partial treatment of premises will not be carried out unless if, a special condition is"
						+ " mentioned in the treatment order or on mutual agreement.",font9);
		PdfPCell tc5Cell = new PdfPCell(tc5);
		tc5Cell.setBorder(0);
		tc5Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc5Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc6 = new Phrase(
				"After spraying operations, the facility must be kept closed for about 3 hours for optimum effect.",
				font9);
		PdfPCell tc6Cell = new PdfPCell(tc6);
		tc6Cell.setBorder(0);
		tc6Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc6Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc7 = new Phrase(
				"No cleaning or washing of treated areas is to be done prior to the above mentioned"
						+ " time period after treatment", font9);
		PdfPCell tc7Cell = new PdfPCell(tc7);
		tc7Cell.setBorder(0);
		tc7Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc7Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc8 = new Phrase(
				"Gel treatment orders will be for the control of COCKROACHES ONLY. Treatment required "
						+ "for other general pest occurrences will have to be carried out seperately and charged extra.",
				font9);
		PdfPCell tc8Cell = new PdfPCell(tc8);
		tc8Cell.setBorder(0);
		tc8Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc8Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc9 = new Phrase(
				"Gel will be applied in only food operating areas and other places such as "
						+ "cabinets/drawers etc where our technician/ staff deems it appropriate for usage.",
				font9);
		PdfPCell tc9Cell = new PdfPCell(tc9);
		tc9Cell.setBorder(0);
		tc9Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc9Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc10 = new Phrase(
				"Along with Gel application, orderless spraying will be carried out in drains and other "
						+ "parts of the premises/ facility.", font9);
		PdfPCell tc10Cell = new PdfPCell(tc10);
		tc10Cell.setBorder(0);
		tc10Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc10Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc11 = new Phrase(
				"Please note that Gel and spraying in the same area/ location point cannot be carried out.",
				font9);
		PdfPCell tc11Cell = new PdfPCell(tc11);
		tc11Cell.setBorder(0);
		tc11Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc11Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		

		Phrase tc12 = new Phrase(
				"No other treatment type can be combined with a Gel treatment order.",
				font9);
		PdfPCell tc12Cell = new PdfPCell(tc12);
		tc12Cell.setBorder(0);
		tc12Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc12Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase blnk = new Phrase("     ", font9);
		PdfPCell blnkCell = new PdfPCell();
		blnkCell.addElement(blnk);
		blnkCell.setBorder(0);
		blnkCell.setFixedHeight(3);
		
		Phrase blnk1 = new Phrase("     ", font9);
		PdfPCell blnk1Cell = new PdfPCell();
		blnk1Cell.addElement(blnk1);
		blnk1Cell.setBorder(0);
		

		Phrase sr1 = new Phrase("1.", font9);
		Paragraph sr1Para = new Paragraph();
		sr1Para.add(sr1);
		sr1Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr1Cell = new PdfPCell(sr1Para);
		sr1Cell.setBorder(0);

		Phrase sr2 = new Phrase("2.", font9);
		Paragraph sr2Para = new Paragraph();
		sr2Para.add(sr2);

		sr2Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr2Cell = new PdfPCell(sr2Para);
		sr2Cell.setBorder(0);

		Phrase sr3 = new Phrase("3.", font9);
		Paragraph sr3Para = new Paragraph();
		sr3Para.add(sr3);
		sr3Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr3Cell = new PdfPCell(sr3Para);
		sr3Cell.setBorder(0);

		Phrase sr4 = new Phrase("4.", font9);
		Paragraph sr4Para = new Paragraph();
		sr4Para.add(sr4);
		sr4Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr4Cell = new PdfPCell(sr4Para);
		sr4Cell.setBorder(0);

		Phrase sr5 = new Phrase("5.", font9);
		Paragraph sr5Para = new Paragraph();
		sr5Para.add(sr5);
		sr5Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr5Cell = new PdfPCell(sr5Para);
		sr5Cell.setBorder(0);

		Phrase sr6 = new Phrase("6.", font9);
		Paragraph sr6Para = new Paragraph();
		sr6Para.add(sr6);
		sr6Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr6Cell = new PdfPCell(sr6Para);
		sr6Cell.setBorder(0);

		Phrase sr7 = new Phrase("7.", font9);
		Paragraph sr7Para = new Paragraph();
		sr7Para.add(sr7);
		sr7Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr7Cell = new PdfPCell(sr7Para);
		sr7Cell.setBorder(0);

		Phrase sr8 = new Phrase("8.", font9);
		Paragraph sr8Para = new Paragraph();
		sr8Para.add(sr8);
		sr8Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr8Cell = new PdfPCell(sr8Para);
		sr8Cell.setBorder(0);

		Phrase sr9 = new Phrase("9.", font9);
		Paragraph sr9Para = new Paragraph();
		sr9Para.add(sr9);
		sr9Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr9Cell = new PdfPCell(sr9Para);
		sr9Cell.setBorder(0);

		Phrase sr10 = new Phrase("10.", font9);
		Paragraph sr10Para = new Paragraph();
		sr10Para.add(sr10);
		sr10Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr10Cell = new PdfPCell(sr10Para);
		sr10Cell.setBorder(0);

		Phrase sr11 = new Phrase("11.", font9);
		Paragraph sr11Para = new Paragraph();
		sr11Para.add(sr11);
		sr11Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr11Cell = new PdfPCell(sr11Para);
		sr11Cell.setBorder(0);

		Phrase sr12 = new Phrase("12.", font9);
		Paragraph sr12Para = new Paragraph();
		sr12Para.add(sr12);
		sr12Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr12Cell = new PdfPCell(sr12Para);
		sr12Cell.setBorder(0);

		PdfPTable tcTable = new PdfPTable(2);
		tcTable.setWidthPercentage(80);

		tcTable.addCell(sr1Cell);
		tcTable.addCell(tc1Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr2Cell);
		tcTable.addCell(tc2Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr3Cell);
		tcTable.addCell(tc3Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr4Cell);
		tcTable.addCell(tc4Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr5Cell);
		tcTable.addCell(tc5Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr6Cell);
		tcTable.addCell(tc6Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr7Cell);
		tcTable.addCell(tc7Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr8Cell);
		tcTable.addCell(tc8Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr9Cell);
		tcTable.addCell(tc9Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr10Cell);
		tcTable.addCell(tc10Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr11Cell);
		tcTable.addCell(tc11Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr12Cell);
		tcTable.addCell(tc12Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);

		
		/////////////////////////////
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		tcTable.addCell(blnk1Cell);
		
		/////////////////////////////
		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnk1Cell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		
//        /////////////////////////////
//        tcTable.addCell(blnkCell);
//        tcTable.addCell(blnkCell);
//        tcTable.addCell(blnkCell);
//        tcTable.addCell(blnkCell);
//        tcTable.addCell(blnkCell);
//        tcTable.addCell(blnkCell);
//        tcTable.addCell(blnkCell);
//        tcTable.addCell(blnkCell);
//        tcTable.addCell(blnkCell);
//    	tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		
//         /////////////////////////////
//         tcTable.addCell(blnkCell);
//         tcTable.addCell(blnkCell);
//         tcTable.addCell(blnkCell);
//         tcTable.addCell(blnkCell);
//         tcTable.addCell(blnkCell);
//         tcTable.addCell(blnkCell);
//         tcTable.addCell(blnkCell);
//         tcTable.addCell(blnkCell);
//         tcTable.addCell(blnkCell);
//         
//         /////////////////////////////////////////////////////////
//        tcTable.addCell(blnkCell);
// 		tcTable.addCell(blnkCell);
// 		tcTable.addCell(blnkCell);
// 		tcTable.addCell(blnkCell);
// 		tcTable.addCell(blnkCell);
// 	 	tcTable.addCell(blnkCell);
// 	 	tcTable.addCell(blnkCell);
// 	 	tcTable.addCell(blnkCell);
// 	 		
// 		tcTable.addCell(blnkCell);
// 		tcTable.addCell(blnkCell);
// 		tcTable.addCell(blnkCell);
// 		tcTable.addCell(blnkCell);
// 		tcTable.addCell(blnkCell);
// 		tcTable.addCell(blnkCell);
// 		tcTable.addCell(blnkCell);
// 		tcTable.addCell(blnkCell);
// 		tcTable.addCell(blnkCell);
// 		tcTable.addCell(blnkCell);
// 		tcTable.addCell(blnkCell);
// 		tcTable.addCell(blnkCell);
// 		tcTable.addCell(blnkCell);
// 		tcTable.addCell(blnkCell);
// 		tcTable.addCell(blnkCell);
// 		tcTable.addCell(blnkCell);


		PdfPCell tcTblCell = new PdfPCell();
		tcTblCell.addElement(tcTable);
		tcTblCell.setBorder(0);

		try {
			tcTable.setWidths(new float[] { 5, 95 });
			document.add(tcTable);
		} catch (DocumentException e) {

			e.printStackTrace();
		}

	}

	// **********************code for Rodent management (terms and condition)****************

	private void createrodentHeaderTable()
	{

		Phrase header = new Phrase(""
				+ comp.getBusinessUnitName().toUpperCase(), font16bold);
		Phrase title = new Phrase("RODENT CONTROL ", font10boldul);

		Paragraph header1Para = new Paragraph();
		header1Para.add(header);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(title);
		header1Para.add(Chunk.NEWLINE);
		header1Para.add(Chunk.NEWLINE);
		header1Para.setAlignment(Element.ALIGN_CENTER);

		PdfPCell Header1ParaCell = new PdfPCell();
		Header1ParaCell.addElement(header1Para);
		Header1ParaCell.setBorder(0);

		PdfPCell gtc2ParaCell = new PdfPCell();
		gtc2ParaCell.setBorder(0);

		PdfPTable headerTable = new PdfPTable(1);
		headerTable.setWidthPercentage(100);
		headerTable.addCell(Header1ParaCell);

		try {
			document.add(headerTable);

		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createrodentTermsAndConditions()
	
	{
		Phrase blnk = new Phrase (" ",font9);
		PdfPCell blnkCell = new PdfPCell (blnk);
		blnkCell.setBorder(0);

		Phrase tc1 = new Phrase(
				"Rodent management plan consists of 80% preventive measures and balance 20% control measures. ",
				font9);
		PdfPCell tc1Cell = new PdfPCell(tc1);
		tc1Cell.setBorder(0);
		tc1Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc1Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc2 = new Phrase(
				"Rodent control treatment will be performed during evening hours (after 4 pm )only"
						+ " unless specified otherwise. ", font9);
		PdfPCell tc2Cell = new PdfPCell(tc2);
		tc2Cell.setBorder(0);
		tc2Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc2Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc3 = new Phrase(
				"The success of any rodent management plan critically depends on  the implementation "
						+ "of various preventive measures that are required to be taken. ",
				font9);
		PdfPCell tc3Cell = new PdfPCell(tc3);
		tc3Cell.setBorder(0);
		tc3Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc3Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc4 = new Phrase(
				"We will not responsible to the lack of results if, adequate support in the form of "
						+ "preventive measures to be implemented is not provided by you. ",
				font9);
		PdfPCell tc4Cell = new PdfPCell(tc4);
		tc4Cell.setBorder(0);
		tc4Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc4Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc5 = new Phrase(
				"No poison baiting will be carried out in internal areas of premises, unless it is deemed "
						+ "appropriate by our staff. ", font9);
		PdfPCell tc5Cell = new PdfPCell(tc5);
		tc5Cell.setBorder(0);
		tc5Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc5Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc6 = new Phrase(
				"Preventive measures implemented must be updated from time to time under advice "
						+ "from our team ", font9);
		PdfPCell tc6Cell = new PdfPCell(tc6);
		tc6Cell.setBorder(0);
		tc6Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc6Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc7 = new Phrase(
				"It must be ensured that all preventive measures implemented by us must be in place and"
						+ " not damaged or tampered with. ", font9);
		PdfPCell tc7Cell = new PdfPCell(tc7);
		tc7Cell.setBorder(0);
		tc7Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc7Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc8 = new Phrase(
				"Rodent metal traps or glue boards and bait station placed by our team must not be"
						+ " displaced, damaged or tampered with. It is responsibility of the client to ensure the same. ",
				font9);
		PdfPCell tc8Cell = new PdfPCell(tc8);
		tc8Cell.setBorder(0);
		tc8Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc8Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase tc9 = new Phrase(
				"We will not responsible (financially or otherwise) for any damages caused to you or"
						+ " your property due to rodents. ", font9);
		PdfPCell tc9Cell = new PdfPCell(tc9);
		tc9Cell.setBorder(0);
		tc9Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		tc9Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		

		Phrase sr1 = new Phrase("1.", font9);
		Paragraph sr1Para = new Paragraph();
		sr1Para.add(sr1);
		sr1Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr1Cell = new PdfPCell(sr1Para);
		sr1Cell.setBorder(0);
		

		Phrase sr2 = new Phrase("2.", font9);
		Paragraph sr2Para = new Paragraph();
		sr2Para.add(sr2);

		sr2Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr2Cell = new PdfPCell(sr2Para);
		sr2Cell.setBorder(0);

		Phrase sr3 = new Phrase("3.", font9);
		Paragraph sr3Para = new Paragraph();
		sr3Para.add(sr3);
		sr3Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr3Cell = new PdfPCell(sr3Para);
		sr3Cell.setBorder(0);

		Phrase sr4 = new Phrase("4.", font9);
		Paragraph sr4Para = new Paragraph();
		sr4Para.add(sr4);
		sr4Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr4Cell = new PdfPCell(sr4Para);
		sr4Cell.setBorder(0);

		Phrase sr5 = new Phrase("5.", font9);
		Paragraph sr5Para = new Paragraph();
		sr5Para.add(sr5);
		sr5Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr5Cell = new PdfPCell(sr5Para);
		sr5Cell.setBorder(0);

		Phrase sr6 = new Phrase("6.", font9);
		Paragraph sr6Para = new Paragraph();
		sr6Para.add(sr6);
		sr6Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr6Cell = new PdfPCell(sr6Para);
		sr6Cell.setBorder(0);

		Phrase sr7 = new Phrase("7.", font9);
		Paragraph sr7Para = new Paragraph();
		sr7Para.add(sr7);
		sr7Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr7Cell = new PdfPCell(sr7Para);
		sr7Cell.setBorder(0);

		Phrase sr8 = new Phrase("8.", font9);
		Paragraph sr8Para = new Paragraph();
		sr8Para.add(sr8);
		sr8Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr8Cell = new PdfPCell(sr8Para);
		sr8Cell.setBorder(0);

		Phrase sr9 = new Phrase("9.", font9);
		Paragraph sr9Para = new Paragraph();
		sr9Para.add(sr9);
		sr9Para.setAlignment(Element.ALIGN_CENTER);
		PdfPCell sr9Cell = new PdfPCell(sr9Para);
		sr9Cell.setBorder(0);

		PdfPTable tcTable = new PdfPTable(2);
		tcTable.setWidthPercentage(80);

		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr1Cell);
		tcTable.addCell(tc1Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr2Cell);
		tcTable.addCell(tc2Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr3Cell);
		tcTable.addCell(tc3Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr4Cell);
		tcTable.addCell(tc4Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr5Cell);
		tcTable.addCell(tc5Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr6Cell);
		tcTable.addCell(tc6Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr7Cell);
		tcTable.addCell(tc7Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr8Cell);
		tcTable.addCell(tc8Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(sr9Cell);
		tcTable.addCell(tc9Cell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		
		////////////////////////////////////////////////////
		
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);

		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
//		tcTable.addCell(blnkCell);
		
		

		PdfPCell tcTblCell = new PdfPCell();
		tcTblCell.addElement(tcTable);
		tcTblCell.setBorder(0);

		try {
			tcTable.setWidths(new float[] { 5, 95 });
			document.add(tcTable);
		} catch (DocumentException e) {

			e.printStackTrace();
		}
	}

	// **************ends here ***************************************

}
