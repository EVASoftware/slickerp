package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
public class PestoIndiaContractPdf {

	public Document document;
	Contract contract;
	Customer cust;
	Company comp;
	private Font font10underline, font16boldul, font12bold, font8bold, font8,
	font9bold, font12boldul, font12, font10bold, font10, font14bold,
	font9;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yy");
	float[] colwidthforheading = { 6.5f, 3f };
	DecimalFormat df=new DecimalFormat("0.00");
	
	public PestoIndiaContractPdf() {
		// TODO Auto-generated constructor stub
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10underline = new Font(Font.FontFamily.HELVETICA, 10,
				Font.UNDERLINE);

		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);

	}
	
	public void getContract(Long count) {
		// TODO Auto-generated method stub
		System.out.println("Inside getQuot");
		contract = ofy().load().type(Contract.class).id(count).now();
		System.out.println("contract" + contract);

		if (contract.getCompanyId() != null) {
			cust = ofy().load().type(Customer.class)
					.filter("count", contract.getCustomerId())
					.filter("companyId", contract.getCompanyId()).first().now();
		}

		if (contract.getCompanyId() == null) {
			cust = ofy().load().type(Customer.class)
					.filter("count", contract.getCustomerId()).first().now();
		}
		
		if (contract.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", contract.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();
		
	}

	public void createPdf(String preprintStatus) {
		
		try{
		
		System.out.println("Create Pdf");
		
		if(preprintStatus.contains("yes"))
		{
			System.out.println("in side yes condition");
			createLogo(document,comp);
			createSpcingForHeading();
		}
		else
		{
			System.out.println("in side no condition");
			createSpcingForHeading();
		    if(comp.getUploadHeader()!=null){
				createCompanyNameAsHeader(document,comp);
			}
						
			if(comp.getUploadFooter()!=null){
				createCompanyNameAsFooter(document,comp);
			}
		}
		
		createHeading();
//		createProductdetails();
		
		createProductDetailsAsPerProductTable();
		
		createTaxAndAmount();
	
		}
		catch(Exception e )
		{
		e.printStackTrace();
		}
	}
	
	
	

	private void createLogo(Document doc, Company comp) {

		DocumentUpload document =comp.getLogo();

		//patch
		String hostUrl; 
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f,765f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
			}
		}
		
		private void createCompanyNameAsHeader(Document doc, Company comp) {
			
			DocumentUpload document =comp.getUploadHeader();

			//patch
			String hostUrl;
			String environment = System.getProperty("com.google.appengine.runtime.environment");
			if (environment.equals("Production")) {
			    String applicationId = System.getProperty("com.google.appengine.application.id");
			    String version = System.getProperty("com.google.appengine.application.version");
			    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
			} else {
			    hostUrl = "http://localhost:8888";
			}
			
			try {
				Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
				image2.scalePercent(15f);
				image2.scaleAbsoluteWidth(520f);
				image2.setAbsolutePosition(40f,725f);	
				doc.add(image2);
			} catch (Exception e) {
				e.printStackTrace();
			}
			}

		private void createCompanyNameAsFooter(Document doc, Company comp) {
			
			
			DocumentUpload document =comp.getUploadFooter();

			//patch
			String hostUrl;
			String environment = System.getProperty("com.google.appengine.runtime.environment");
			if (environment.equals("Production")) {
			    String applicationId = System.getProperty("com.google.appengine.application.id");
			    String version = System.getProperty("com.google.appengine.application.version");
			    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
			} else {
			    hostUrl = "http://localhost:8888";
			}
			
			try {
				Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
				image2.scalePercent(15f);
				image2.scaleAbsoluteWidth(520f);
				image2.setAbsolutePosition(40f,40f);	
				doc.add(image2);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			}
	
		private void createSpcingForHeading() {
			
			Phrase spacing = new Phrase(Chunk.NEWLINE);
			Paragraph spacingPara = new Paragraph(spacing);
			
			try {
				document.add(spacingPara);
				document.add(spacingPara);
				document.add(spacingPara);
				document.add(spacingPara);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	private void createHeading() {
		// TODO Auto-generated method stub
		System.out.println("Inside Create HEading");
		String custname = "";

		if (cust.isCompany()) {
			if (cust.getCompanyName() != null) {
				custname = cust.getCompanyName();
			}

		} else {
			custname = cust.getFullname();
		}

		Phrase custnamephrase = new Phrase(custname, font10);

		String custAdd1 = "";

		String custFullAdd1 = "";

		if (cust.getAdress() != null) {		
			
			if(!cust.getAdress().getAddrLine2().equals("")){
				if(!cust.getAdress().getLandmark().equals("")){
					custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getAddrLine2()+"\n"+cust.getAdress().getLandmark();
				}else{
					custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getAddrLine2();
				}
			}else{
				if(!cust.getAdress().getLandmark().equals("")){
					custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getLandmark();
				}else{
					custAdd1=cust.getAdress().getAddrLine1();
				}
			}
			
			if(!cust.getAdress().getLocality().equals("")){
				custFullAdd1=custAdd1+"\n"+cust.getAdress().getLocality()+"\n"+cust.getAdress().getCity()+"-"+cust.getAdress().getPin();
						
			}else{
				custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+"-"+cust.getAdress().getPin();
			}
		}

		Phrase blank = new Phrase(" ", font10);
		PdfPCell blankcell = new PdfPCell();
		blankcell.addElement(blank);
		blankcell.setBorder(0);
		blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase addrcell = new Phrase(custFullAdd1, font10);
		PdfPCell custname_addrcell = new PdfPCell();
		custname_addrcell.addElement(custnamephrase);
		custname_addrcell.addElement(addrcell);
		custname_addrcell.setBorder(0);
		custname_addrcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase conId = new Phrase("Contract Id", font10bold);
		PdfPCell conIdCell = new PdfPCell();
		conIdCell.addElement(conId);
		// quotationIdCell.setBorder(0);
		conIdCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase contractId = null;
		if (contract.getCount() != 0) {
			contractId = new Phrase(contract.getCount() + "", font10);
		} else {
			contractId = new Phrase("-------", font10);
		}

		PdfPCell contractIdCell = new PdfPCell();
		contractIdCell.addElement(contractId);
		// quotIdCell.setBorder(0);
		contractIdCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable idtable = new PdfPTable(1);
		idtable.addCell(conIdCell);
		idtable.addCell(contractIdCell);
		idtable.setSpacingAfter(10f);
		idtable.setWidthPercentage(100);
//		idtable.addCell(blankcell);
//		idtable.addCell(blankcell);

		Phrase conperiod=new Phrase("Contract Period",font8bold);
		PdfPCell conperiodCell=new PdfPCell();
		conperiodCell.addElement(conperiod);
		conperiodCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		conperiodCell.setBorder(0);
		
		Phrase contractPeriod=new Phrase(fmt.format(contract.getStartDate())+" To "+fmt.format(contract.getEndDate()),font8);
		PdfPCell contractperiodCell=new PdfPCell();
		contractperiodCell.addElement(contractPeriod);
		contractperiodCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		contractperiodCell.setBorder(0);
		
		float[] columnWidths = {2.4f, 3.6f};
		PdfPTable contperiodTable=new PdfPTable(2);
		contperiodTable.setWidthPercentage(100);
		try {
			contperiodTable.setWidths(columnWidths);
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		contperiodTable.addCell(conperiodCell);
		contperiodTable.addCell(contractperiodCell);
		
		
		PdfPCell idCell = new PdfPCell();
		idCell.addElement(idtable);
		idCell.addElement(contperiodTable);
		idCell.setBorder(0);
		idCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable headTable = new PdfPTable(2);
		headTable.addCell(custname_addrcell);
		headTable.addCell(idCell);
		headTable.setWidthPercentage(100);
		try {
			headTable.setWidths(colwidthforheading);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
			headTable.setSpacingAfter(10f);
		try {
			document.add(headTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
private void createProductDetailsAsPerProductTable() {
	
	Phrase particulars = new Phrase("PARTICULARS", font10bold);
	PdfPCell particularCell = new PdfPCell(particulars);
	particularCell.setHorizontalAlignment(Element.ALIGN_CENTER);

	PdfPTable particulartable = new PdfPTable(1);
	particulartable.setWidthPercentage(100);
	particulartable.addCell(particularCell);

	try {
		document.add(particulartable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	PdfPTable table = new PdfPTable(4);
	table.setWidthPercentage(100);

	Phrase serviceType = new Phrase("Service Type", font10bold);
	PdfPCell serviceTypeCell = new PdfPCell(serviceType);
	serviceTypeCell.setBorder(0);
	serviceTypeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	Phrase placeTreatment = new Phrase("Place of Treatment", font10bold);
	PdfPCell placeTreatmentCell = new PdfPCell(placeTreatment);
	placeTreatmentCell.setBorder(0);
	placeTreatmentCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	Phrase termsofTreatment = new Phrase("Terms of Treatment", font10bold);
	PdfPCell termsofTreatmentCell = new PdfPCell(termsofTreatment);
	termsofTreatmentCell.setBorder(0);
	termsofTreatmentCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	Phrase permises = new Phrase("Premises Details", font10bold);
	PdfPCell permisesCell = new PdfPCell(permises);
	permisesCell.setBorder(0);
	permisesCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	table.addCell(serviceTypeCell);
	table.addCell(placeTreatmentCell);
	table.addCell(termsofTreatmentCell);
	table.addCell(permisesCell);

	
	
	Phrase sertype = null;
	Phrase placeoftreatment = null;
	Phrase termsoftreatment = null;
	Phrase premisesdetails = null;
	
	
	for (int i = 0; i < contract.getItems().size(); i++)
	{
		for (int j = 0; j < contract.getItems().get(i).getQty(); j++) {
			
		System.out.println("Rohan Product Name"+contract.getItems().get(i).getProductName());
			sertype = new Phrase(contract.getItems().get(i).getProductName(),font10);
			PdfPCell sertypeCell = new PdfPCell(sertype);
			sertypeCell.setBorder(0);
			sertypeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
	//  *****************for printing branch ************************
			String branch = null ;
			CustomerBranchDetails custBranch =null;
		for (int k = 0; k < contract.getServiceScheduleList().size(); k++) 
		{
			System.out.println("Rohan product sr no"+contract.getItems().get(i).getProductSrNo());
			System.out.println("Rohan Service sr no"+contract.getServiceScheduleList().get(k).getSerSrNo());
			
			if(contract.getItems().get(i).getProductSrNo()==contract.getServiceScheduleList().get(k).getSerSrNo())
			{
				System.out.println("in side method ");
				branch = contract.getServiceScheduleList().get(k).getScheduleProBranch();
				System.out.println("in side method branch Name "+branch);
			}
		}
		
		placeoftreatment = new Phrase(branch, font10);
		PdfPCell placeoftreatmentCell = new PdfPCell(placeoftreatment);
		placeoftreatmentCell.setBorder(0);
		placeoftreatmentCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
			
	// ********************for printing terms of treatment *********************	
		if( contract.getItems().get(i).getTermsoftreatment() != null && !contract.getItems().get(i).getTermsoftreatment().equals("")){
			System.out.println("Rohan contract.getItems().get(i).getTermsoftreatment()  "+contract.getItems().get(i).getTermsoftreatment());
			termsoftreatment = new Phrase(contract.getItems().get(i).getTermsoftreatment(),font10);
		
		}
		else
		{
			termsoftreatment = new Phrase(contract.getItems().get(i).getNumberOfServices()+" Services",font10);
			System.out.println("Rohan contract.getItems().get(i).getTermsoftreatment()  "+contract.getItems().get(i).getNumberOfServices());
		}
		
		PdfPCell termsoftreatmentCell = new PdfPCell(termsoftreatment);
		termsoftreatmentCell.setBorder(0);
		termsoftreatmentCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
	//  *****************for printing branch address details   ***************************	
		System.out.println("Rohan Branch name "+branch);
		
		
		if(!branch.equalsIgnoreCase("Service Address")){
			System.out.println("Inside if condition means without service address");
			custBranch = ofy().load().type(CustomerBranchDetails.class).filter("buisnessUnitName", branch).filter("cinfo.count", contract.getCinfo().getCount()).first().now();
			premisesdetails = new Phrase(custBranch.getAddress().getAddrLine1(), font10);
		}
		else
		{
			System.out.println("Inside else condition means with service address");
			premisesdetails = new Phrase(cust.getSecondaryAdress().getAddrLine1(), font10);	
		}
		
		
		PdfPCell premisesdetailsCell = new PdfPCell(premisesdetails);
		premisesdetailsCell.setBorder(0);
		premisesdetailsCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		table.addCell(sertypeCell);
		table.addCell(placeoftreatmentCell);
		table.addCell(termsoftreatmentCell);
		table.addCell(premisesdetailsCell);
	}
	}
		
	
	try {
		document.add(table);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
	
	}
	
	private void createProductdetails() {
		// TODO Auto-generated method stub
		Phrase particulars = new Phrase("PARTICULARS", font10bold);
		PdfPCell particularCell = new PdfPCell(particulars);
		particularCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable particulartable = new PdfPTable(1);
		particulartable.setWidthPercentage(100);
		particulartable.addCell(particularCell);

		try {
			document.add(particulartable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable table = new PdfPTable(4);
		table.setWidthPercentage(100);

		Phrase serviceType = new Phrase("Service Type", font10bold);
		PdfPCell serviceTypeCell = new PdfPCell(serviceType);
		serviceTypeCell.setBorder(0);
		serviceTypeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase placeTreatment = new Phrase("Place of Treatment", font10bold);
		PdfPCell placeTreatmentCell = new PdfPCell(placeTreatment);
		placeTreatmentCell.setBorder(0);
		placeTreatmentCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase termsofTreatment = new Phrase("Terms of Treatment", font10bold);
		PdfPCell termsofTreatmentCell = new PdfPCell(termsofTreatment);
		termsofTreatmentCell.setBorder(0);
		termsofTreatmentCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase permises = new Phrase("Premises Details", font10bold);
		PdfPCell permisesCell = new PdfPCell(permises);
		permisesCell.setBorder(0);
		permisesCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		table.addCell(serviceTypeCell);
		table.addCell(placeTreatmentCell);
		table.addCell(termsofTreatmentCell);
		table.addCell(permisesCell);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase sertype = null;
		Phrase placeoftreatment = null;
		Phrase termsoftreatment = null;
		Phrase premisesdetails = null;

		System.out.println("quot.getServiceScheduleList().size()"
				+ contract.getServiceScheduleList().size());

		
		
		for (int i = 0; i < contract.getServiceScheduleList().size(); i++) {
			
			System.out.println("Loop run i"+i);
			sertype = new Phrase(contract.getServiceScheduleList().get(i)
					.getScheduleProdName(), font10);
			PdfPCell sertypeCell = new PdfPCell(sertype);
			sertypeCell.setBorder(0);
			sertypeCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			System.out
					.println("quot.getServiceScheduleList().get(i).getScheduleProBranch()"
							+ contract.getServiceScheduleList().get(i)
									.getScheduleProBranch());
			placeoftreatment = new Phrase(contract.getServiceScheduleList().get(i)
					.getScheduleProBranch(), font10);
			PdfPCell placeoftreatmentCell = new PdfPCell(placeoftreatment);
			placeoftreatmentCell.setBorder(0);
			placeoftreatmentCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			for (int j = 0; j <contract.getItems().size(); j++) {
				if(contract.getServiceScheduleList().get(i).getScheduleProdId()==contract.getItems().get(j).getPrduct().getCount()){
					if(contract.getItems().get(j).getTermsoftreatment()!=null){
						termsoftreatment = new Phrase(contract.getItems().get(j).getTermsoftreatment(),font10);
					}else{
						termsoftreatment = new Phrase(contract.getServiceScheduleList().get(i)
								.getScheduleNoOfServices()
								+" Services", font10);
					}
				}
			}
			
			PdfPCell termsoftreatmentCell = new PdfPCell(termsoftreatment);
			termsoftreatmentCell.setBorder(0);
			termsoftreatmentCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			if(contract.getServiceScheduleList().get(i).getPremisesDetails()!=null){
				premisesdetails = new Phrase(contract.getServiceScheduleList().get(i).getPremisesDetails(), font10);
			}else{
				premisesdetails = new Phrase(" ", font10);
			}
			PdfPCell premisesdetailsCell = new PdfPCell(premisesdetails);
			premisesdetailsCell.setBorder(0);
			premisesdetailsCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			PdfPTable tableProd = new PdfPTable(4);
			tableProd.setWidthPercentage(100);
			tableProd.addCell(sertypeCell);
			tableProd.addCell(placeoftreatmentCell);
			tableProd.addCell(termsoftreatmentCell);
			tableProd.addCell(premisesdetailsCell);

			try {
				document.add(tableProd);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				// }
			}
		}

	}

	private void createTaxAndAmount() {
		// TODO Auto-generated method stub
		Phrase blank = new Phrase(" ", font10);
		PdfPCell blankcell = new PdfPCell();
		blankcell.addElement(blank);
		blankcell.setBorder(0);
		blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		double amount=0;
		double serviceamount=0;
//		
//		for (int i = 0; i < quot.getItems().size(); i++) {
//			System.out.println("product loop"+i);
//			amount=amount+quot.getItems().get(i).getPrice();
//			System.out.println("amount"+amount);
//			serviceamount=serviceamount+((amount)/Double.parseDouble(quot.getItems().get(i).getServiceTax()+""));
//			System.out.println("amount"+serviceamount);
//		}
//		

		Phrase amountP=new Phrase("Amount",font10bold);
		PdfPCell amountPcell=new PdfPCell(amountP);
		amountPcell.setBorder(0);
		amountPcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		Phrase amountphrase=new Phrase(df.format(contract.getTotalAmount())+"",font10);
		PdfPCell amountcell=new PdfPCell(amountphrase);
		amountcell.setBorder(0);
		amountcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		PdfPTable amountTable=new PdfPTable(2);
		amountTable.addCell(amountPcell);
		amountTable.addCell(amountcell);
		
		PdfPCell amountTCell=new PdfPCell(amountTable);
		amountTCell.setBorder(0);
		
		PdfPTable amtfinaltable=new PdfPTable(2);
		amtfinaltable.setWidthPercentage(100);
		try {
			amtfinaltable.setWidths(colwidthforheading);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		amtfinaltable.addCell(blankcell);
		amtfinaltable.addCell(amountTCell);
		amtfinaltable.setSpacingBefore(10f);;
		
		try {
			document.add(amtfinaltable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase serviceAmount=null;
		Phrase taxAmount=null;
		
		for (int i = 0; i < contract.getProductTaxes().size(); i++) {
			
			serviceAmount=new Phrase(contract.getProductTaxes().get(i).getChargeName(),font10bold);
			PdfPCell ServiceAmountcell=new PdfPCell(serviceAmount);
			ServiceAmountcell.setBorder(0);
			ServiceAmountcell.setHorizontalAlignment(Element.ALIGN_RIGHT);	
			
			taxAmount=new Phrase(df.format(contract.getProductTaxes().get(i).getChargePayable())+"",font10);
			PdfPCell serviceTaxAmountcell=new PdfPCell(taxAmount);
			serviceTaxAmountcell.setBorder(0);
			serviceTaxAmountcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			

			PdfPTable calucaltionTable=new PdfPTable(2);
			calucaltionTable.addCell(ServiceAmountcell);
			calucaltionTable.addCell(serviceTaxAmountcell);
			

			PdfPCell calCell=new PdfPCell(calucaltionTable);
			calCell.setBorder(0);
			
			PdfPTable table=new PdfPTable(2);
			table.setWidthPercentage(100);
			try {
				table.setWidths(colwidthforheading);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			table.addCell(blankcell);
			table.addCell(calCell);
			if(contract.getProductTaxes().get(i).getChargePayable()>0){
				System.out.println("quot.getProductTaxes().get(i).getChargePayable()"+contract.getProductTaxes().get(i).getChargePayable());
				try {
					document.add(table);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		Phrase totalphrase=new Phrase("Total",font10bold);
		PdfPCell totalcell=new PdfPCell(totalphrase);
		totalcell.setBorder(0);
		totalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		Phrase totalAmount=new Phrase(df.format(contract.getNetpayable())+"",font10);
		PdfPCell totalAmountcell=new PdfPCell(totalAmount);
		totalAmountcell.setBorder(0);
		totalAmountcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
	
		PdfPTable totalTable=new PdfPTable(2);
		totalTable.addCell(totalcell);
		totalTable.addCell(totalAmountcell);
		
		PdfPCell totalCell=new PdfPCell(totalTable);
		totalCell.setBorder(0);
		
		PdfPTable table=new PdfPTable(2);
		table.setWidthPercentage(100);
		try {
			table.setWidths(colwidthforheading);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		table.addCell(blankcell);
		table.addCell(totalCell);
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String amountInWord=ServiceInvoicePdf.convert(contract.getNetpayable());
		Phrase amountinwords=new Phrase("Rs. "+amountInWord,font10bold);
		PdfPCell amountinwordscell=new PdfPCell(amountinwords);
		amountinwordscell.setBorderWidthLeft(0);
		amountinwordscell.setBorderWidthTop(0);
		amountinwordscell.setBorderWidthRight(0);
		amountinwordscell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPTable tableinwords=new PdfPTable(1);
		tableinwords.addCell(amountinwordscell);
		tableinwords.setWidthPercentage(100);
		try {
			document.add(tableinwords);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		Phrase remark=new Phrase("Remarks : ",font10);
		PdfPCell remarkcell=new PdfPCell(remark);
		remarkcell.setBorder(0);
		remarkcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		String descrip=null;
		if(contract.getDescription()!=null){
			descrip=contract.getDescription();
		}else{
			descrip="";
		}
		
		Phrase desp=new Phrase(descrip+"",font10);
		PdfPCell despcell=new PdfPCell(desp);
		despcell.setBorder(0);
		despcell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPTable remarkTable=new PdfPTable(1);
		remarkTable.setWidthPercentage(100);
		remarkTable.addCell(remarkcell);
		remarkTable.addCell(despcell);
		
		try {
			document.add(remarkTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
