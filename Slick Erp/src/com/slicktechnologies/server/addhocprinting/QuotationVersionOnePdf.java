package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;

public class QuotationVersionOnePdf {
	
	public Document document;
	Company comp;
	List<SalesLineItem> products;
	Customer cust;
	Contract con; //Added by Ashwini
	Quotation quot;
	Lead serviceLead;
	Branch branchDt = null;
//	int noOfLines = 8;
	int noOfLines = 6;
	int prouductCount=0;
	CompanyPayment comppayment;
	boolean consolidatePrice = false;
	boolean upcflag=false;
	boolean quotationForPecop=false;
	//Logger logger = Logger.getLogger("NameOfYourLogger");
	Phrase chunk;
	PdfPCell pdfpremise, pdfservices, pdfduration, pdffrequency, pdfprice,pdfblank,pdfttl,pdfttlval;
	PdfPCell pdfdays, pdfpercent, pdfcomment;
	PdfPCell  pdftaxname,pdftaxpercent, pdfassval, pdfamt;
	ProcessConfiguration processConfig;
	float[] colwidth = {1.4f,0.2f,1.8f,0.7f,0.2f,1.9f,1.2f,0.2f,2f }; //2.0f, 0.2f, 5.0f, 1.0f, 0.2f, 1.5f 
	//1.5f,0.2f,1.5f,0.9f,0.2f,5.1f 
	
	float[] col1width = { 1.5f, 0.2f, 3.0f,3.0f, 1.0f, 0.2f, 3.0f};   
	float[] tblcolwidth = { 0.7f, 1.0f, 1.0f, 2.0f, 3.0f }; 
	
	float[] columnWidths7 = { 2.0f, 3.1f, 1.7f, 1.6f, 1.6f};
	
	Logger logger = Logger.getLogger("NameOfYourLogger");

	private Font font16boldul, font12bold, font8bold, font8, font9bold,font11,
			font12boldul, font12, font10bold, font10,font11bold, font14bold, font9,font14boldul,font14;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");

	DecimalFormat df = new DecimalFormat("0");
	
	DecimalFormat df1 = new DecimalFormat("0.00");
	private PdfPCell custlandcell;
	
	public QuotationVersionOnePdf(){
		
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font11 = new Font(Font.FontFamily.HELVETICA, 11);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font11bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14  = new Font(Font.FontFamily.HELVETICA, 14);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);

	}
	
	public void setPcservicequotation(Long count,String preprint) {
		
		quot = ofy().load().type(Quotation.class).id(count).now();
		
		
				
				if (quot.getCompanyId()!=null){
					con = ofy().load().type(Contract.class)
							.filter("count", quot.getCompanyId())
							.filter("companyId", quot.getCompanyId()).first().now();
				}
				/*
				 * End by Ashwini
				 */
				
		
		if (quot.getCompanyId() != null)
			cust = ofy().load().type(Customer.class)
					.filter("count", quot.getCustomerId())
					.filter("companyId", quot.getCompanyId()).first().now();
		
		if (quot.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", quot.getCustomerId()).first().now();

		if (quot.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", quot.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();

		if (quot.getCompanyId() != null)
			quot = ofy().load().type(Quotation.class)
					.filter("companyId", quot.getCompanyId())
					.filter("count", quot.getCount()).first().now();
		else
			quot = ofy().load().type(Quotation.class)
					.filter("count", quot.getCount()).first().now();
		
		 logger.log(Level.SEVERE,"before lead load comp id is"+quot.getCompanyId());
		if (quot.getCompanyId() != null){
			 logger.log(Level.SEVERE,"inside if lead load comp id is"+quot.getCompanyId());			
			serviceLead = ofy().load().type(Lead.class)
					.filter("companyId", quot.getCompanyId())
					.filter("count", quot.getLeadCount()).first().now();
		}
		else{
			 logger.log(Level.SEVERE,"inside else lead load comp id is"+quot.getCompanyId());
			serviceLead = ofy().load().type(Lead.class)
					.filter("count", quot.getLeadCount()).first().now();
		}
		
		
		if(quot.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", quot.getCompanyId()).filter("processName", "Quotation").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						upcflag=true;
						
					}
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("QuotationForPecop")&& processConfig.getProcessList().get(k).isStatus() == true) {
						quotationForPecop = true;
					}
				}
			}
		}
		
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			
			logger.log(Level.SEVERE,"Process active --");
			if(quot !=null && quot.getBranch() != null && quot.getBranch().trim().length() >0){
				
				branchDt = ofy().load().type(Branch.class).filter("companyId",quot.getCompanyId()).filter("buisnessUnitName", quot.getBranch()).first().now();
				
				if(branchDt!=null){
					logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());		
					
					if(branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
						
					
					List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
					
					if(paymentDt.get(0).trim().matches("[0-9]+")){
						
						
						
						int payId = Integer.parseInt(paymentDt.get(0).trim());
						
						comppayment = ofy().load().type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", quot.getCompanyId()).first()
								.now();
						
						
						if(comppayment != null){
							
						}
						
					}
					}
					
					
					comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
				}
			}
		}
		
		
		
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		
	}
	
	
	public void createPdf(String preprint) {

		if (upcflag == false && preprint.equals("plane")) {
//			//createBlankHeading();
			createLogo(document, comp);
			createCompanyHeadding();
		} else {

			if (preprint.equals("yes")) {

				System.out.println("inside prit yes");
				createBlankforUPC();
			}

			if (preprint.equals("no")) {
				System.out.println("inside prit no");

				if (comp.getUploadHeader() != null) {
					createCompanyNameAsHeader(document, comp);
				}

				if (comp.getUploadFooter() != null) {
					createCompanyNameAsFooter(document, comp);
				}
				createBlankforUPC();
			}

		}
		createReferenceandDateHeadding();
		createCustomerDetails();
		createSubjectandMessageHeadding();
		createProductInfoHeadding();
		createProductInfoHeaddingInfo();
		System.out.println("prouductCount == "+prouductCount);
		System.out.println("noOfLines == "+noOfLines);
		
		createTaxdetails();
		createValidityofquotationHeadding();
		
//		if((noOfLines==0 || noOfLines==-1)&&prouductCount!=0) {//prouductCount
		logger.log(Level.SEVERE, "this.quot.getItems().size() "+this.quot.getItems().size());
		logger.log(Level.SEVERE, "prouductCount "+prouductCount);
		
		//Commented by Ashwini Patil as Client doesn't want annexure table
//		if(this.quot.getItems().size()!=prouductCount) {//prouductCount
//
//			System.out.println("for next page condition true");
//			try {
//				document.add(Chunk.NEXTPAGE);
//			} catch (DocumentException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			if (preprint.equals("yes")) {
//
//				System.out.println("inside prit yes");
//				createBlankforUPC();
//			}
//
//			if (preprint.equals("no")) {
//				System.out.println("inside prit no");
//
//				if (comp.getUploadHeader() != null) {
//					createCompanyNameAsHeader(document, comp);
//				}
//
//				if (comp.getUploadFooter() != null) {
//					createCompanyNameAsFooter(document, comp);
//				}
//				createBlankforUPC();
//			}
//			Phrase ref = new Phrase(" ", font10bold);
//			Paragraph pararef = new Paragraph();
//			pararef.add(ref);
//			pararef.setSpacingAfter(7f);
//		   
//		    try {
//				document.add(pararef);
//			} catch (DocumentException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		    createProductDetailsForAnnextureTab(prouductCount);
//		    	
//		  }
//		createTaxdetails();
//		createValidityofquotationHeadding();

  }
	
			private void createLogo(Document doc, Company comp) {
				
				//********************logo for server ********************
				
					DocumentUpload document =comp.getLogo();
					//patch
					String hostUrl; 
					String environment = System.getProperty("com.google.appengine.runtime.environment");
					if (environment.equals("Production")) {
					    String applicationId = System.getProperty("com.google.appengine.application.id");
					    String version = System.getProperty("com.google.appengine.application.version");
					    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
					} else {
					    hostUrl = "http://localhost:8888";
					}
					
					try {
						Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
						image2.scalePercent(20f);
						image2.setAbsolutePosition(40f,765f);	
						doc.add(image2);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
	
	
			public void createCompanyHeadding() {
				
				
				
				
				String companyname ="";
				if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
					companyname=" "
							+  branchDt.getCorrespondenceName();
				}else{
				 companyname = " "
						+ comp.getBusinessUnitName().trim().toUpperCase();
				}

				Paragraph companynamepara = new Paragraph();
				companynamepara.add(companyname);
				companynamepara.setFont(font14);
				companynamepara.setAlignment(Element.ALIGN_CENTER);

				PdfPCell companynamecell = new PdfPCell();
				companynamecell.addElement(companynamepara);
				companynamecell.setBorder(0);
				

				
				String custAdd1="";
				String custFullAdd1="";
				
				if(comp.getAddress()!=null){
					
					if(comp.getAddress().getAddrLine2()!=null && !comp.getAddress().getAddrLine2().equals("")){
					
						if(comp.getAddress().getLandmark()!=null && !comp.getAddress().getLandmark().equals("")){
							custAdd1=comp.getAddress().getAddrLine1()+","+comp.getAddress().getAddrLine2()+","+comp.getAddress().getLandmark();
						}else{
							custAdd1=comp.getAddress().getAddrLine1()+","+comp.getAddress().getAddrLine2();
						}
					}else{
						if(comp.getAddress().getLandmark()!=null && !comp.getAddress().getLandmark().equals("") ){
							custAdd1=comp.getAddress().getAddrLine1()+"\n"+comp.getAddress().getLandmark();
						}else{
							custAdd1=comp.getAddress().getAddrLine1();
						}
					}
					
					if(comp.getAddress().getLocality()!=null&& !comp.getAddress().getLocality().equals("")){
						if(comp.getAddress().getPin()!=0){
							custFullAdd1=custAdd1+"\n"+comp.getAddress().getLocality()+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry()+","+comp.getAddress().getPin();
						}else{
							custFullAdd1=custAdd1+"\n"+comp.getAddress().getLocality()+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
						}
					}else{
						if(comp.getAddress().getPin()!=0){
							custFullAdd1=custAdd1+"\n"+comp.getAddress().getCity()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry()+","+comp.getAddress().getPin();
						}else{
							custFullAdd1=custAdd1+"\n"+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
						}
					}
					
				}	
				
					Phrase addressline=new Phrase(custFullAdd1,font11);
			
					Paragraph addresspara=new Paragraph();
					addresspara.add(addressline);
					addresspara.setAlignment(Element.ALIGN_CENTER);
					
					PdfPCell addresscell=new PdfPCell();
					addresscell.addElement(addresspara);
					addresscell.setBorder(0);
				   
				
				
				String contactinfo="";
				if (comp.getCellNumber1() != null && comp.getCellNumber1() != 0) {
					contactinfo =  "Mobile: "+comp.getCellNumber1()+" Email: "+comp.getEmail().trim();
				}
				//Phrase contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+" Email: "+comp.getEmail().trim(),font10);
				
				Paragraph realmobpara=new Paragraph();
				realmobpara.add(contactinfo);
				realmobpara.setFont(font11);
				realmobpara.setAlignment(Element.ALIGN_CENTER);
				
				PdfPCell contactcell=new PdfPCell();
				contactcell.addElement(realmobpara);
				contactcell.setBorder(0);
				
				PdfPTable table = new PdfPTable(1);
				table.addCell(companynamecell);
				table.addCell(addresscell);
//				table.addCell(localitycell);
				table.addCell(contactcell);
				table.setWidthPercentage(100f);
				table.setSpacingAfter(10f);
				
				PdfPCell cell = new PdfPCell();
				cell.addElement(table);
				cell.setBorderWidthLeft(0);
				cell.setBorderWidthRight(0);
				cell.setBorderWidthTop(0);
				
				PdfPTable parenttable = new PdfPTable(1);
				parenttable.addCell(cell);
				parenttable.setWidthPercentage(100);
				
			
			
			try {
				document.add(parenttable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
			
			
			public void createReferenceandDateHeadding() {
				
				
			
				Phrase ref = new Phrase("Quotation ID",font11);
				Paragraph refpara = new Paragraph();
				refpara.add(ref);
				refpara.setAlignment(Element.ALIGN_LEFT);
				
				PdfPCell refcell = new PdfPCell(refpara);
//				refcell.addElement(refpara);
				refcell.setBorder(0);
				refcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				Phrase refno=null;
				if(quot.getReferenceNumber() !=null && !quot.getReferenceNumber().equals("")){
					refno = new Phrase(quot.getReferenceNumber());
				}
				else{
					refno = new Phrase(quot.getCount()+"",font11);
				}
				
				PdfPCell refnocell  = new PdfPCell(refno);
				//refnocell.addElement(refno);
				refnocell.setBorder(0);
				refnocell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				Phrase date = new Phrase("Date",font11);
				Paragraph datepara = new Paragraph();
				datepara.add(date);
				datepara.setAlignment(Element.ALIGN_LEFT);
				
				PdfPCell datecell = new PdfPCell(datepara);
//				datecell.addElement(datepara);
				datecell.setBorder(0);
				datecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				Phrase dt = null;
				
				if(quot.getReferenceDate() !=null){
					dt = new Phrase(fmt.format(quot.getReferenceDate()));
				}
				else{

					dt = new Phrase(fmt.format(quot.getQuotationDate()),font11);
				}

				
				PdfPCell dtcell = new PdfPCell(dt);
//				dtcell.addElement(dt);
				dtcell.setBorder(0);
				dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				
				
				Phrase validdate = new Phrase("Valid Until ",font11);
				Paragraph validdatepara = new Paragraph();
				validdatepara.add(validdate);
				validdatepara.setAlignment(Element.ALIGN_LEFT);
				
				PdfPCell datecell1 = new PdfPCell(validdatepara);
//				datecell.addElement(datepara);
				datecell1.setBorder(0);
				datecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				Phrase dt1 = null;
				
				if(quot.getValidUntill() !=null){
					dt1 = new Phrase(fmt.format(quot.getValidUntill()));
				}
				else{

					dt1 = new Phrase(fmt.format(quot.getValidUntill()),font11);
				}

				
				PdfPCell dtcell1 = new PdfPCell(dt1);
//				dtcell.addElement(dt);
				dtcell1.setBorder(0);
				dtcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
				
//				*****************************************************************************
				Phrase col = new Phrase(":",font11);
				Paragraph colpara = new Paragraph();
				colpara.add(col);
				colpara.setAlignment(Element.ALIGN_LEFT);
				
				PdfPCell colcell = new PdfPCell(colpara);
				//colcell.addElement();
				colcell.setBorder(0);
				colcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			    ******************************************************************************	
				
				PdfPTable table1 = new PdfPTable(9);
				
				try {
					table1.setWidths(colwidth);
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
				
				
				table1.addCell(refcell);
				table1.addCell(colcell);
				table1.addCell(refnocell);
				
				table1.addCell(datecell);
				table1.addCell(colcell);
				table1.addCell(dtcell);
				
				
				table1.addCell(datecell1);
				table1.addCell(colcell);
				table1.addCell(dtcell1);
				
				table1.setWidthPercentage(100f);
				
				PdfPCell cell1 = new PdfPCell();
				cell1.addElement(table1);
				cell1.setBorder(0);
			
				PdfPTable parent1 = new PdfPTable(1);
				
				parent1.addCell(cell1);
				parent1.setWidthPercentage(100);
				
				
				try {
					document.add(parent1);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		
//			public void createCustomerDetailsHeading(){
//								
//				Phrase to = new Phrase(" ", font11);
//				PdfPCell toCell = new PdfPCell(to);
//				toCell.setBorder(0);
//				toCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			
//				
//				
//				String customerInfo="";
//				if (cust.getCustPrintableName() != null
//						&& !cust.getCustPrintableName().equals("")) {
//					customerInfo = cust.getCustPrintableName().trim();
//				} else {
//
//					if (cust.isCompany() == true && cust.getCompanyName() != null) {
//						customerInfo = "M/S " + cust.getCompanyName().trim();
//					} else {
//						customerInfo = cust.getFullname().trim();
//					}
//				}
//
//				String custAdd1="";
//				String custFullAdd1="";
//				
//				if(cust.getAdress()!=null){
//					
//					if(cust.getAdress().getAddrLine2()!=null && !cust.getAdress().getAddrLine2().equals("")){
//						if(cust.getAdress().getLandmark()!=null && !cust.getAdress().getLandmark().equals("")){
//							custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getAddrLine2()+"\n"+cust.getAdress().getLandmark();
//						}else{
//							custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getAddrLine2();
//						}
//					}else{
//						if(cust.getAdress().getLandmark()!=null && !cust.getAdress().getLandmark().equals("") ){
//							custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getLandmark();
//						}else{
//							custAdd1=cust.getAdress().getAddrLine1();
//						}
//					}
//					
//					if(cust.getAdress().getLocality()!=null&& !cust.getAdress().getLocality().equals("")){
//						if(cust.getAdress().getPin()!=0){
//							custFullAdd1=custAdd1+"\n"+cust.getAdress().getLocality()+"\n"+cust.getAdress().getPin()+"\n"+cust.getAdress().getCity()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry();
//						}else{
//							custFullAdd1=custAdd1+"\n"+cust.getAdress().getLocality()+"\n"+cust.getAdress().getCity()+"\n"+cust.getAdress().getState()+","+cust.getAdress().getCountry();
//						}
//					}else{
//						if(cust.getAdress().getPin()!=0){
//							custFullAdd1=custAdd1+"\n"+cust.getAdress().getPin()+","+cust.getAdress().getCity()+", "+cust.getAdress().getState()+", "+cust.getAdress().getCountry();
//						}else{
//							custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+"\n"+cust.getAdress().getState()+","+cust.getAdress().getCountry();
//						}
//					}
//					
//
//				}
//
//				
//				Phrase custAddInfo = new Phrase(customerInfo+"\n"+custFullAdd1, font10);
//				PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
//				custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				custAddInfoCell.setBorder(0);
//				
//				
//				Phrase blank = new Phrase(" ", font10);
//				PdfPCell blankCell = new PdfPCell(blank);
//				blankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//				blankCell.setBorder(0);
//				
//				PdfPCell pocCell=null;
//				if(cust.getCompanyName()!=null && cust.isCompany()==true){
//					
//					 Phrase poc = new Phrase("POC : "+cust.getFullname(),font10);
//					 pocCell = new PdfPCell(poc);
//					 pocCell.setBorder(0);
//				}
//				
//				PdfPTable table2 = new PdfPTable(1);
//				table2.addCell(toCell);
//
//				table2.addCell(custAddInfoCell);
//				
//				if(cust.getCompanyName()!=null && cust.isCompany()==true){
//					table2.addCell(pocCell);
//				}
//				
//				table2.setWidthPercentage(100f);		
//				
//				PdfPCell customerCell = new PdfPCell();
//				customerCell.addElement(table2);
//				customerCell.setBorder(0);
//				customerCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//				
//				PdfPTable parent2 = new PdfPTable(1);
//				parent2.addCell(customerCell);
//				parent2.setWidthPercentage(100);
//				
//				try {
//					document.add(parent2);
//				} catch (DocumentException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
			
			
			private void createCustomerDetails() {
				
				Phrase to = new Phrase(" ", font10);
				PdfPCell toCell = new PdfPCell(to);
				toCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				toCell.setBorder(0);
				
								
				
				Phrase custInfo ;
				if(cust.isCompany()){
					custInfo = new Phrase(cust.getCompanyName(), font11);
				}else{
					custInfo = new Phrase(cust.getFullname(), font11);
				}
				PdfPCell custInfoCell = new PdfPCell(custInfo);
				custInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				custInfoCell.setBorder(0);
				
				
				
				
//				String custAdd1="";
//				String custFullAdd1="";
//				
//				if(cust.getAdress()!=null){
//					
//					if(cust.getAdress().getAddrLine2()!=null){
//						if(cust.getAdress().getLandmark()!=null){
//							custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getAddrLine2()+","+cust.getAdress().getLandmark();
//						}else{
//							custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getAddrLine2();
//						}
//					}else{
//						if(cust.getAdress().getLandmark()!=null){
//							custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getLandmark();
//						}else{
//							custAdd1=cust.getAdress().getAddrLine1();
//						}
//					}
//					
//					
//					String pin="";
//					if(cust.getAdress().getPin()!=0){
//						pin=cust.getAdress().getPin()+"";
//					}
//					else{
//						pin="";
//					}
//					if(cust.getAdress().getLocality()!=null){
////						custFullAdd1=custAdd1+"\n"+cust.getAdress().getCountry()+","+cust.getAdress().getState()+","+cust.getAdress().getCity()+"\n"+cust.getAdress().getLocality()+" "+cust.getAdress().getPin();
//						
//						custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry()+"\n"+cust.getAdress().getLocality()+"\n"+pin;
//
//					}else{
//						custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry()+"\n"+pin;
//					}
//				}
//				String custAddress="";
//				if(cust.getAdress()!=null){
//					custAddress=cust.getAdress().getCompleteAddress();
//				}else{
//					custAddress="";
//				}
//				Phrase custAddInfo = new Phrase(custAddress, font10);
//				PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
//				custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				custAddInfoCell.setBorder(0);
				
				
				String custAdd1="";
				String custFullAdd1="";
				
				if(cust.getAdress()!=null){
					
					if(cust.getAdress().getAddrLine2()!=null && !cust.getAdress().getAddrLine2().equals("")){
					
						if(cust.getAdress().getLandmark()!=null && !cust.getAdress().getLandmark().equals("")){
							custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getAddrLine2()+","+cust.getAdress().getLandmark();
						}else{
							custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getAddrLine2();
						}
					}else{
						if(cust.getAdress().getLandmark()!=null && !cust.getAdress().getLandmark().equals("") ){
							custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getLandmark();
						}else{
							custAdd1=cust.getAdress().getAddrLine1();
						}
					}
					
					if(cust.getAdress().getLocality()!=null&& !cust.getAdress().getLocality().equals("")){
						custFullAdd1=custAdd1+"\n"+cust.getAdress().getLocality()+","+cust.getAdress().getCity()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry();
					}else{
						custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry();
					}

					
				}	
				
					Phrase addressline=new Phrase(custFullAdd1,font11);
			
					Paragraph addresspara=new Paragraph(addressline);
					//addresspara.add(addressline);
					addresspara.setAlignment(Element.ALIGN_LEFT);
					
					PdfPCell addresscell=new PdfPCell(addresspara);
					//addresscell.addElement(addresspara);
					addresscell.setBorder(0);
					
					
					String PinCode="";
						if(cust.getAdress().getPin()!=0){
							PinCode="Pin Code : "+cust.getAdress().getPin()+"";
						}else{
							PinCode=" ";
						}
						
				
						Phrase pin = new Phrase(PinCode, font11);
						PdfPCell pinCell = new PdfPCell(pin);
						pinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						pinCell.setBorder(0);
					
				
				
				Phrase blank = new Phrase(" ", font11);
				PdfPCell blankCell = new PdfPCell(blank);
				blankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				blankCell.setBorder(0);
				
				
				String cell="";
				if(cust.getCellNumber1()!=null){
					cell="Contact Number : "+cust.getCellNumber1()+"";
				}else{
					cell=" ";
				}
				
				Phrase cellNo = new Phrase(cell, font11);
				PdfPCell cellNoCell = new PdfPCell(cellNo);
				cellNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				cellNoCell.setBorder(0);
				
				String email="";
				if(cust.getEmail()!=null){
					email=cust.getEmail();
				}else{
					email="";
				}
				Phrase emailPr = new Phrase("E-mail : "+email, font11);
				PdfPCell emailCell = new PdfPCell(emailPr);
				emailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				emailCell.setBorder(0);
				
				PdfPTable customerTable=new PdfPTable(1);
				customerTable.setWidthPercentage(100);
				
				customerTable.addCell(toCell);
				
				customerTable.addCell(custInfoCell);
				customerTable.addCell(addresscell);
				if(cust.getAdress().getPin()!=0){
				 customerTable.addCell(pinCell);
				}
				if(cust.getCellNumber1()!=0)
				{
				customerTable.addCell(cellNoCell);
				}
				if(email!=null&&!email.equals(""))
				{	
				customerTable.addCell(emailCell);
				}
				PdfPCell customerCell = new PdfPCell();
				customerCell.addElement(customerTable);
				customerCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				customerCell.setBorder(0);
				
				
				
				PdfPTable headTbl=new PdfPTable(2);
				headTbl.setWidthPercentage(100f);
				
				headTbl.addCell(customerCell);
				headTbl.addCell(blankCell);
				
				PdfPCell parentCell = new PdfPCell();
				parentCell.addElement(headTbl);
				parentCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				parentCell.setBorder(0);
				
				PdfPTable parent=new PdfPTable(1);
				parent.setWidthPercentage(100);
				
				parent.addCell(parentCell);
				parent.setSpacingAfter(15f);
				
				try {
					document.add(parent);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
					
			}
			
			public void createSubjectandMessageHeadding(){
				
//				For static an dynamic entity2 ******************************************************************
				
				Phrase sub = new Phrase("                    Subject  : Quotation For Pest Management Services",font11);
				PdfPCell subcell = new PdfPCell();
				subcell.addElement(sub);
				subcell.setBorder(0);
				subcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				Phrase blank=new Phrase();
				blank.add(" ");
				PdfPCell blankCell = new PdfPCell();
				blankCell.addElement(blank);
				blankCell.setBorder(0);
				
				
				String firstName=cust.getFullname();
				String[] nameArray = firstName.split("\\s+");
				if(nameArray.length>0){
					firstName=nameArray[0];
				}
				Phrase dear = new Phrase("Dear"+" "+firstName+",", font11);
				PdfPCell dearCell = new PdfPCell(dear);
				dearCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				dearCell.setBorder(0);
				
			
				Phrase blanksub=new Phrase();
				blanksub.add(" ");
				PdfPCell blanksubCell = new PdfPCell();
				blanksubCell.addElement(blank);
				blanksubCell.setBorder(0);
				
				String title1 = "";
			
				 logger.log(Level.SEVERE,"service lead"+serviceLead);
				if(serviceLead!=null){
					 logger.log(Level.SEVERE,"inside servicelead!=null");
					 System.out.println("Lead"+serviceLead);
					if(serviceLead.getCreationDate()!=null){
						 logger.log(Level.SEVERE,"inside date is present");
					title1 = "Based on your recent enquiry, we have evaluated your requirement and quote as follows for our pest management services :";
					}
					else
					{
						title1 = "Based on your recent enquiry, we have evaluated your requirement and quote as follows for our pest management services :";	
					}
				}
				else
				{
					 logger.log(Level.SEVERE,"inside service lead is null");
						title1 = "Based on your recent enquiry, we have evaluated your requirement and quote as follows for our pest management services :";
				}
				
				Phrase msg = new Phrase(title1, font11);
				PdfPCell msgCell = new PdfPCell(msg);
				msgCell.setBorder(0);
				msgCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				Phrase blankmsg=new Phrase();
				blankmsg.add(" ");
				PdfPCell blankmsgCell = new PdfPCell();
				blankmsgCell.addElement(blank);
				blankmsgCell.setBorder(0);
			
				
				
				PdfPTable table3 = new PdfPTable(1);
				table3.addCell(subcell);
				table3.addCell(blankCell);
				table3.addCell(dearCell);
				table3.addCell(blanksubCell);
				table3.addCell(msgCell);
				table3.addCell(blankmsgCell);
				
				table3.setWidthPercentage(100f);
				
				PdfPCell cell2 = new PdfPCell();
				cell2.addElement(table3);
				cell2.setBorder(0);
				
				PdfPTable parent3 = new PdfPTable(1);
				parent3.addCell(cell2);
				parent3.setWidthPercentage(100);
				
				
				try {
					document.add(parent3);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
			public void createProductInfoHeadding(){
				Font font1 = new Font(Font.FontFamily.HELVETICA, 11);
				PdfPTable table = new PdfPTable(5);
				table.setWidthPercentage(100);
				try {
					table.setWidths(columnWidths7);
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
				
				Phrase premise = new Phrase("Premises Under Contract", font1);
				Phrase services = new Phrase("Type of Treatment", font1);
				Phrase duration = new Phrase("Start Date/End Date",font1);
				Phrase frequency = new Phrase("Number of Services", font1);
				Phrase price = new Phrase("Basic Amount", font1);
				
//				Phrase tax = new Phrase(quot.getProductTaxes().get(0).getChargeName() + "" + quot.getProductTaxes().get(0).getChargePercent() + "%",font8);
//				PdfPCell taxcell = new PdfPCell(tax);
//				taxcell.setBorder(0);
		//
//				Phrase taxval = new Phrase(quot.getProductTaxes().get(0).getChargePayable() + "" ,font8);
//				PdfPCell taxvalcell = new PdfPCell(taxval);
//				taxvalcell.setBorder(0);
//				
				
				
				PdfPCell codecell = new PdfPCell(premise);
				codecell.setHorizontalAlignment(Element.ALIGN_CENTER);
				codecell.setPaddingBottom(3f);
				
				PdfPCell servicescell = new PdfPCell(services);
				servicescell.setHorizontalAlignment(Element.ALIGN_CENTER);
				servicescell.setPaddingBottom(3f);
				
				PdfPCell durationcell = new PdfPCell(duration);
				durationcell.setHorizontalAlignment(Element.ALIGN_CENTER);
				durationcell.setPaddingBottom(3f);
				
				PdfPCell frequencycell = new PdfPCell(frequency);
				frequencycell.setHorizontalAlignment(Element.ALIGN_CENTER);
				frequencycell.setPaddingBottom(3f);
				
				PdfPCell pricecell = new PdfPCell(price);
				pricecell.setHorizontalAlignment(Element.ALIGN_CENTER);
				pricecell.setPaddingBottom(3f);
				
						
				table.addCell(codecell);
				table.addCell(servicescell);
				table.addCell(durationcell);
				table.addCell(frequencycell);
				table.addCell(pricecell);
				
				try {
					document.add(table);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		
			public void createProductDetailsForAnnextureTab(int prouductCount2){
				
				
				createProductInfoHeadding();
				
				noOfLines=55;
				logger.log(Level.SEVERE,"Annexture TABLE : "+noOfLines);
				
				//Font font1 = new Font(Font.FontFamily.HELVETICA, 11);
				PdfPTable table = new PdfPTable(5);
				table.setWidthPercentage(100);
				try {
					table.setWidths(columnWidths7);
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
				

				
				
				double totalAmount = 0;

				for (int j = 0; j < quot.getItems().size(); j++) {
//					double taxVal = removeAllTaxes(quot.getItems().get(j).getPrduct());  //quot.getItems().get(i).
//					double calculatedPrice = products.get(j).getPrice() - taxVal;
					totalAmount = totalAmount + quot.getItems().get(j).getPrice();
				
				}

				
				
				
			//for(int i=0; i<this.quot.getItems().size(); i++) {
				for(int i = prouductCount2; i < this.quot.getItems().size(); i++){
					prouductCount = i;
					if (noOfLines <= 0) {
						// if((i+1)!=invoiceentity.getSalesOrderProducts().size())
//						logger.log(Level.SEVERE,"NO OF LINES : "+noOfLines + "product no . "+i);
						prouductCount = i;
						//recursiveFlag=true;
						break;
					}
					int lenght=quot.getItems().get(i).getProductName().length();
					logger.log(Level.SEVERE,"Length : "+lenght);
					int lines= (int) Math.ceil((lenght/36.0));
					logger.log(Level.SEVERE,"LINES : "+lines);
//					noOfLines = noOfLines - 1;
					
					noOfLines = noOfLines - lines;
					
					logger.log(Level.SEVERE,"NO OF LINES : "+noOfLines+" Lines "+lines);
				    
					String premises ="NA";	
				    if(quot.getItems().get(i).getPremisesDetails()!=null && !quot.getItems().get(i).getPremisesDetails().equals("")){
				    	premises=quot.getItems().get(i).getPremisesDetails();
				    }
					chunk = new Phrase(premises,font11);
					pdfpremise = new PdfPCell(chunk);
					pdfpremise.setHorizontalAlignment(Element.ALIGN_LEFT);			
					pdfpremise.setPaddingTop(3f);
					pdfpremise.setPaddingBottom(3f);
					pdfpremise.setPaddingLeft(3f);
					pdfpremise.setPaddingRight(3f);
					
//					Phrase phrase = new Phrase();
//					phrase.add("The founders of iText are nominated for a ");
//					Chunk chunk1 = new Chunk("European Business Award!");
//					chunk1.setAnchor("http://itextpdf.com/blog/european-business-award-kick-ceremony");
//					phrase.add(chunk1);
//					//table.addCell(phrase);
//					
//					chunk = null;
//					if(quot.getItems().get(i).getProductName()!=null &&
//							!quot.getItems().get(i).getProductName().trim().equals("")){
//						
//						chunk = new Phrase(quot.getItems().get(i).getProductName(),font10);
//						 
//					}else{
//						
//						ServiceProduct serviceProduct = ofy()
//								.load()
//								.type(ServiceProduct.class)
//								.filter("companyId", comp.getCompanyId())
//								.filter("productCode",quot.getItems().get(i).getProductName().trim()).first().now();
//						if (serviceProduct.getProductCategory()!= null) {
//							chunk = new Phrase(serviceProduct.getProductCategory().trim(), font10);
//							 
//							
//						} else {
//							chunk = new Phrase(serviceProduct.getProductCategory().trim(), font10);
//						}
//					}
//					
//					
//					
					
					
					ServiceProduct serviceProduct = ofy()
							.load()
							.type(ServiceProduct.class)
							.filter("companyId", comp.getCompanyId())
							.filter("productCode",quot.getItems().get(i).getProductCode().trim()).first().now();
					
//					chunk = null;
//					if(quot.getItems().get(i).getProductName()!=null &&
//							!quot.getItems().get(i).getProductName().trim().equals("")){
//						
//						chunk = new Phrase(quot.getItems().get(i).getProductName(),font10);
//						 
//					}else{
//						
//				
//						if (serviceProduct.getProductCategory()!= null) {
//							chunk = new Phrase(serviceProduct.getProductCategory().trim(), font10);
////							quot.getItems().get(i).getProductCategory();
//							
//						} else {
//							chunk = new Phrase(serviceProduct.getProductCategory().trim(), font10);
//						}
//					}
					
					
//					Phrase phrase = new Phrase();
//					Chunk chunk1 = new Chunk(quot.getItems().get(i).getProductName());
//					chunk1.setAnchor(serviceProduct.getProductCategory().trim());
//					phrase.add(chunk1);
//					table.addCell(phrase);
//					
					
					
					if(serviceProduct!=null){
						logger.log(Level.SEVERE,"serviceProduct : "+serviceProduct);
//						if(serviceProduct.getProductCategory()!=null&&!serviceProduct.getProductCategory().equals("")){
//							Category prodCategory = ofy().load().type(Category.class)
//									.filter("companyId", comp.getCompanyId())
//									.filter("catName",serviceProduct.getProductCategory()).first().now();
//							
//							if(prodCategory!=null&&prodCategory.getDescription()!=null&&!prodCategory.getDescription().equals("")){
//								chunk = new Phrase();
//								Chunk chunk1 = new Chunk(serviceProduct.getProductCategory(), font11);
//								chunk1.setAnchor(prodCategory.getDescription().trim());
//								chunk1.setUnderline(0.1f, -2f);
//								chunk.add(chunk1);
//								
//							}else{
//								chunk = new Phrase(serviceProduct.getProductCategory().trim(), font11);
//							}
//						}else {
//							chunk = new Phrase(quot.getItems().get(i).getProductName() + "",font11);
//						}
						
						if(serviceProduct.getTermsAndConditionLink()!=null && !serviceProduct.getTermsAndConditionLink().equals("")){
							chunk = new Phrase();
							Chunk chunk1 = new Chunk(quot.getItems().get(i).getProductName(), font11);
							chunk1.setAnchor(serviceProduct.getTermsAndConditionLink());
							chunk1.setUnderline(0.1f, -2f);
							chunk.add(chunk1);

						}
						else{
							System.out.println("no link else block");
							chunk = new Phrase(quot.getItems().get(i).getProductName() + "",font11);
						}
						
					}else{
						chunk = new Phrase(quot.getItems().get(i).getProductName() + "",font11);
					}
					
					
					//chunk = new Phrase(quot.getItems().get(i).getProductName() + "",font10);
					
					pdfservices = new PdfPCell(chunk);
					pdfservices.setHorizontalAlignment(Element.ALIGN_LEFT);
					pdfservices.setPaddingTop(3f);
					pdfservices.setPaddingBottom(3f);
					pdfservices.setPaddingLeft(3f);
					pdfservices.setPaddingRight(3f);
					
					
					Calendar c = Calendar.getInstance();
					c.setTime(quot.getItems().get(i).getStartDate());
					c.add(Calendar.DATE, quot.getItems().get(i).getDuration() - 1);
					Date endDt = c.getTime();
					
					String date = "";
					if (quot.getItems().get(i).getEndDate() != null) {
						date = fmt.format(quot.getItems().get(i).getStartDate()) + "\n"+ fmt.format(quot.getItems().get(i).getEndDate());
					} else {
						date = fmt.format(quot.getItems().get(i).getStartDate()) + "\n"+ fmt.format(endDt);
					}
					
					chunk = new Phrase(date + "",font11);
					
					pdfduration = new PdfPCell(chunk);
					pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
					pdfduration.setPaddingTop(3f);
					pdfduration.setPaddingBottom(3f);
					pdfduration.setPaddingLeft(3f);
					pdfduration.setPaddingRight(3f);
					
					chunk = new Phrase(quot.getItems().get(i).getNumberOfServices() + "",font11);
					
					pdffrequency = new PdfPCell(chunk);
					pdffrequency.setHorizontalAlignment(Element.ALIGN_CENTER);
					pdffrequency.setPaddingTop(3f);
					pdffrequency.setPaddingBottom(3f);
					pdffrequency.setPaddingLeft(3f);
					pdffrequency.setPaddingRight(3f);
					
					int pricechunk= (int) quot.getItems().get(i).getPrice();
					
					chunk = new Phrase(pricechunk+"",font10);
					
//					chunk = new Phrase(quot.getItems().get(i).getPrice() + "",font8);
					
					pdfprice = new PdfPCell(chunk);
					pdfprice.setHorizontalAlignment(Element.ALIGN_RIGHT);
					pdfprice.setPaddingTop(3f);
					pdfprice.setPaddingBottom(3f);
					pdfprice.setPaddingLeft(3f);
					pdfprice.setPaddingRight(3f);
					
					if(quot.isConsolidatePrice()){
						if (i == 0) {
							chunk = new Phrase(df.format(totalAmount), font11);
							pdfprice = new PdfPCell(chunk);
							pdfprice.setHorizontalAlignment(Element.ALIGN_RIGHT);
							if(quot.getItems().size() > 1)
								pdfprice.setBorderWidthBottom(0);
							
						}else {
							chunk = new Phrase(" ", font11);
							pdfprice = new PdfPCell(chunk);
							pdfprice.setHorizontalAlignment(Element.ALIGN_RIGHT);
							if(i == quot.getItems().size()-1 ){							
								pdfprice.setBorderWidthTop(0);
							}else{
								pdfprice.setBorderWidthBottom(0);
								pdfprice.setBorderWidthTop(0);
							}
						}	
					}
					
					
					table.addCell(pdfpremise);
					table.addCell(pdfservices);
					table.addCell(pdfduration);
					table.addCell(pdffrequency);
					table.addCell(pdfprice);
				}
			
			
			
			try {
				document.add(table);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
			
	        public void createTaxdetails(){
	        	
	        	Font font1 = new Font(Font.FontFamily.HELVETICA, 11);
				PdfPTable table = new PdfPTable(5);
				table.setWidthPercentage(100);
				try {
					table.setWidths(columnWidths7);
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
				
				
	        	Phrase blank=new Phrase();
				blank.add(" ");
				
				PdfPCell blankCell = new PdfPCell();
				blankCell.addElement(blank);
				blankCell.setBorder(0);
				
				Phrase total = new Phrase("Total",font11);
				PdfPCell totalcell = new PdfPCell(total);
				totalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				
				Phrase totalvalue = new Phrase(df.format(quot.getTotalAmount())+"",font11);
				PdfPCell totalvalcell = new PdfPCell(totalvalue);
				totalvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

				table.addCell(blankCell);
				table.addCell(blankCell);
				table.addCell(blankCell);
				table.addCell(totalcell);
				table.addCell(totalvalcell);
				
				
				
				Phrase Dis = new Phrase("Discount",font11);
				PdfPCell discell = new PdfPCell(Dis);
				discell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				
				
				if(quot.getDiscountAmt()!=0){
	        	    chunk = new Phrase(df.format(quot.getDiscountAmt())+"",font11);
				}else{
	        		 chunk = new Phrase(" ",font10);
				}
				
				PdfPCell disvalcell = new PdfPCell(chunk);
				disvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

				if(quot.getDiscountAmt()!=0){
				table.addCell(blankCell);
				table.addCell(blankCell);
				table.addCell(blankCell);
				table.addCell(discell);
				table.addCell(disvalcell);
				}
				
				
				Phrase Distotal = new Phrase("Total After Discount",font11);
				PdfPCell distotalcell = new PdfPCell(Distotal);
				distotalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				
				
				Phrase Distotal1=new Phrase(df.format(quot.getFinalTotalAmt())+"",font11);	
				PdfPCell distotalvalcell = new PdfPCell(Distotal1);
				distotalvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

				if(quot.getDiscountAmt()!=0){
				table.addCell(blankCell);
				table.addCell(blankCell);
				table.addCell(blankCell);
				table.addCell(distotalcell);
				table.addCell(distotalvalcell);
				}
				
				
				
				for(int i=0; i<this.quot.getProductTaxes().size(); i++){
					
					if(quot.getProductTaxes().get(i).getChargePercent() !=0){
						
						
					chunk = new Phrase("" + quot.getProductTaxes().get(i).getChargeName() + " : " + quot.getProductTaxes().get(i).getChargePercent() + "%",font11);
					System.out.println("expected name: " + quot.getProductTaxes().get(i).getChargeName());
					pdftaxname = new PdfPCell(chunk);
					pdftaxname.setHorizontalAlignment(Element.ALIGN_RIGHT);
					
				/*****************************************************************************************************/	
//					chunk = new Phrase(quot.getProductTaxes().get(i).getChargePercent()+"",font8);
//					System.out.println("expected ChargePercent:" + quot.getProductTaxes().get(i).getChargePercent());
//					pdftaxpercent = new PdfPCell(chunk);
//					pdftaxpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
					
//					chunk = new Phrase("" + quot.getProductTaxes().get(i).getAssessableAmount(),font8);
//					System.out.println("expected AssessableAmount: " + quot.getProductTaxes().get(i).getAssessableAmount());
//					pdfassval = new PdfPCell(chunk);
//					pdfassval.setHorizontalAlignment(Element.ALIGN_CENTER);
					
				/***********************************************************************************************/	
					
					chunk = new Phrase(df.format(quot.getProductTaxes().get(i).getChargePayable())+ "",font11);
					System.out.println("expected ChargeAbsValue: " + quot.getProductTaxes().get(i).getChargePayable());
					pdfamt = new PdfPCell(chunk);
					pdfamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
					
					
					table.addCell(blankCell);
					table.addCell(blankCell);
					table.addCell(blankCell);
					table.addCell(pdftaxname);
					table.addCell(pdfamt);
					
					}
					
				}
				
				Phrase subtotal = new Phrase("Net Payable",font11);
				PdfPCell subtotalcell = new PdfPCell(subtotal);
				subtotalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				
				Phrase subtotalvalue = new Phrase(df.format(quot.getNetpayable())+"",font11);
				PdfPCell subtotalvalcell = new PdfPCell(subtotalvalue);
				subtotalvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

				table.addCell(blankCell);
				table.addCell(blankCell);
				table.addCell(blankCell);
				table.addCell(subtotalcell);
				table.addCell(subtotalvalcell);
				
				
				
//				PdfPCell productcell = new PdfPCell();
//				productcell.addElement(table);
//				productcell.setBorder(0);
//				
//				PdfPTable parent7 = new PdfPTable(1);
//				parent7.addCell(productcell);
//				parent7.setWidthPercentage(100);
				
				
				try {
					document.add(table);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
			public void createValidityofquotationHeadding(){
				
				
				//String paymentTerm="";
//				if(quot.getPaymentTermsList()!=null&&quot.getPaymentTermsList().size()!=0){
//					paymentTerm=quot.getPaymentTermsList().get(0).getPayTermComment();
					
				//}
				//"Terms of Payment : 
				Phrase terms = new Phrase("Payment Terms : " +quot.getPayTerms()  ,font11);
				PdfPCell termscell = new PdfPCell(terms);
				termscell.setBorder(0);
				termscell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				Phrase blank=new Phrase();
				blank.add(" ");
				PdfPCell blankCell = new PdfPCell();
				blankCell.addElement(blank);
				blankCell.setBorder(0);
				
				
//				String title3= "";
////				title3 = "We hope you will find our quotation reasonable and await your acceptance of this quotation; should you require further information on the above, please call us on "  + (comp.getCellNumber1() + "\n" + "Thanks & regards,");
//				title3 = "In acceptance of above, please click on the link below to accept our Terms & Conditions and make your payment."+"\n";
//						 
//				Phrase str2 = new Phrase(title3,font10);
//				PdfPCell str2cell = new PdfPCell(str2);
//				str2cell.setBorder(0);
//				str2cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				Phrase blankshall = new Phrase();
				blankshall.add(Chunk.NEWLINE);
			    PdfPCell blankshallcell = new PdfPCell(blankshall);
			    blankshallcell.setBorder(0);
				
				
				String titleshall= "";
//				title3 = "We hope you will find our quotation reasonable and await your acceptance of this quotation; should you require further information on the above, please call us on "  + (comp.getCellNumber1() + "\n" + "Thanks & regards,");
				titleshall = "We shall initiate services at your convenience after we receive contract confirmation."+"\n";
						 
				Phrase strshall = new Phrase(titleshall,font11);
				PdfPCell strshallcell = new PdfPCell(strshall);
				strshallcell.setBorder(0);
				strshallcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				
				
				Phrase blankfeel = new Phrase();
				blankfeel.add(Chunk.NEWLINE);
			    PdfPCell blank2cell = new PdfPCell(blankfeel);
			    blank2cell.setBorder(0);
				
				String title4 =" ";
				title4 = "We thank you for opportunity given to serve you and look forward to adding you to our family of customers. ";
				
				Phrase str3 = new Phrase(title4,font11);
				PdfPCell str3cell = new PdfPCell(str3);
				str3cell.setBorder(0);
				str3cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
				

				Phrase blank1 = new Phrase();
			    blank1.add(Chunk.NEWLINE);
			    PdfPCell blank1cell = new PdfPCell(blank1);
			    blank1cell.setBorder(0);
			    
			    String title5 =" ";
			    title5 = "Sincerely yours,";
			    
			    Phrase str4 = new Phrase(title5,font11);
				PdfPCell str4cell = new PdfPCell(str4);
				str4cell.setBorder(0);
				str4cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				
//				Phrase pest = null;
//				if(comp.getBusinessUnitName() !=null){
//					
//					pest = new Phrase("" + comp.getBusinessUnitName(),font12bold);	
//					
//				}
//				//Team, Marketing & Customer Relations
				
				/**
				 * @author Vijay Chougule
				 * Des :- added cell number and email id after company name if branch as company is active
				 * then Branch Cell number and Email will print else company Cell number and email will print
				 */
				String cellNoAndEmail = "";
				String companyname ="";
				if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
					companyname=branchDt.getCorrespondenceName();
					if(branchDt.getCellNumber1()!=null && branchDt.getCellNumber1()!=0){
						cellNoAndEmail = branchDt.getCellNumber1()+"";
					}
					if(branchDt.getEmail()!=null && !branchDt.getEmail().equals("")){
						if(cellNoAndEmail.length()>0){
							cellNoAndEmail += ", ";
						}
						cellNoAndEmail += branchDt.getEmail();
					}
				}else{
				 companyname =comp.getBusinessUnitName().trim().toUpperCase();
				 
				 if(comp.getCellNumber1()!=null && comp.getCellNumber1()!=0){
						cellNoAndEmail = comp.getCellNumber1()+"";
					}
					if(comp.getEmail()!=null && !comp.getEmail().equals("")){
						if(cellNoAndEmail.length()>0){
							cellNoAndEmail += ", ";
						}
						cellNoAndEmail += comp.getEmail();
					}
				}
				/**
				 * ends here
				 */

				
				Phrase companynamepara = new Phrase(companyname,font11);
				
//				Paragraph companynamepara = new Paragraph();
//				companynamepara.add(companyname);
//				companynamepara.setFont(font12bold);
//				companynamepara.setAlignment(Element.ALIGN_LEFT);

				PdfPCell companynamecell = new PdfPCell();
				companynamecell.addElement(companynamepara);
				companynamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				companynamecell.setBorder(0);
				
				Phrase companynaCellNoEmail = new Phrase(cellNoAndEmail,font11);
				
				PdfPCell companyCellNoEmailcell = new PdfPCell();
				companyCellNoEmailcell.addElement(companynaCellNoEmail);
				companyCellNoEmailcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				companyCellNoEmailcell.setBorder(0);
				
				
				Phrase newLine =  new Phrase(Chunk.NEWLINE);
				PdfPCell newLineCell= new PdfPCell(newLine);
				newLineCell.setBorder(0);
				
				Phrase newLine1 =  new Phrase(Chunk.NEWLINE);
				PdfPCell newLineCell1= new PdfPCell(newLine1);
				newLineCell1.setBorder(0);
				
				
				String Titlelast="This is a computer-generated document and does not carry a signature.";
				 
				 Phrase ref=new Phrase(Titlelast,font11);
				 PdfPCell refcell=new PdfPCell(ref);
				 //refcell.addElement(ref);
				 refcell.setBorder(0);
				 refcell.setHorizontalAlignment(Element.ALIGN_CENTER);
				
				
//				PdfPCell pestcell = new PdfPCell(pest); 
//				pestcell.setBorder(0);
//				pestcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				PdfPTable validitytable = new PdfPTable(1);
				validitytable.addCell(termscell);
				validitytable.addCell(blankCell);
				  
//				validitytable.addCell(blankvalcell);
				//validitytable.addCell(str2cell);  //strshallcell	    blankshallcell
				//validitytable.addCell(blank2cell);
				validitytable.addCell(strshallcell);
				validitytable.addCell(blankshallcell);
				validitytable.addCell(str3cell);
				validitytable.addCell(blank1cell);
				validitytable.addCell(str4cell);
//				validitytable.addCell(blank1cell);  
				validitytable.addCell(companynamecell);
				
				validitytable.addCell(companyCellNoEmailcell);

				validitytable.addCell(newLineCell);  //refcell
//				validitytable.addCell(newLineCell1);
				validitytable.addCell(refcell);
				
				validitytable.setWidthPercentage(100f);
				
				PdfPCell cell6 = new PdfPCell(validitytable);
				cell6.setBorder(0);
				
				PdfPTable parent6 = new PdfPTable(1);
				parent6.addCell(cell6);
				parent6.setWidthPercentage(100);
				
				try {
					document.add(parent6);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
						
			}

			private void createBlankforUPC() {
				
				
				
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Header","INCREASEUPPERHEADER" ,quot.getCompanyId())){
					 Paragraph blank =new Paragraph();
					 blank.add(Chunk.NEWLINE);
					
					
					try {
						document.add(blank);
						document.add(blank);
						document.add(blank);
						document.add(blank);
						document.add(blank);
						document.add(blank);
						document.add(blank);
						document.add(blank);
						
					} catch (DocumentException e) {
						e.printStackTrace();
					}
				}else{
					 Paragraph blank =new Paragraph();
					    blank.add(Chunk.NEWLINE);
					
					
					try {
						document.add(blank);
						document.add(blank);
						document.add(blank);
						document.add(blank);
						document.add(blank);
						document.add(blank);
						
						
					} catch (DocumentException e) {
						e.printStackTrace();
					}
				}
				
				
				}	
			
			private void createCompanyNameAsHeader(Document doc, Company comp) {
				
				DocumentUpload document =comp.getUploadHeader();

				//patch
				String hostUrl;
				String environment = System.getProperty("com.google.appengine.runtime.environment");
				if (environment.equals("Production")) {
				    String applicationId = System.getProperty("com.google.appengine.application.id");
				    String version = System.getProperty("com.google.appengine.application.version");
				    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
				} else {
				    hostUrl = "http://localhost:8888";
				}
				
				try {
					Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
					image2.scalePercent(15f);
					image2.scaleAbsoluteWidth(520f);
					image2.setAbsolutePosition(40f,725f);	
					doc.add(image2);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
				}

			private void createCompanyNameAsFooter(Document doc, Company comp) {
				
				
				DocumentUpload document =comp.getUploadFooter();

				//patch
				String hostUrl;
				String environment = System.getProperty("com.google.appengine.runtime.environment");
				if (environment.equals("Production")) {
				    String applicationId = System.getProperty("com.google.appengine.application.id");
				    String version = System.getProperty("com.google.appengine.application.version");
				    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
				} else {
				    hostUrl = "http://localhost:8888";
				}
				
				try {
					Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
					image2.scalePercent(15f);
					image2.scaleAbsoluteWidth(520f);
					image2.setAbsolutePosition(40f,10f);	
					doc.add(image2);
				} catch (Exception e) {
					e.printStackTrace();
				}
				

				}
			
			
			public void createProductInfoHeaddingInfo(){
				
				Font font1 = new Font(Font.FontFamily.HELVETICA, 11);
				PdfPTable table = new PdfPTable(5);
				table.setWidthPercentage(100);
				try {
					table.setWidths(columnWidths7);
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
				

				
				
				double totalAmount = 0;

				for (int j = 0; j < quot.getItems().size(); j++) {
//					double taxVal = removeAllTaxes(quot.getItems().get(j).getPrduct());  //quot.getItems().get(i).
//					double calculatedPrice = products.get(j).getPrice() - taxVal;
					totalAmount = totalAmount + quot.getItems().get(j).getPrice();
				
				}

				
				
				
			for(int i=0; i<this.quot.getItems().size(); i++) {
					System.out.println();
					prouductCount = i;
//					if (noOfLines <= 0) {
//						prouductCount = i;
//						System.out.println("table before annexture "+table.size());
//
//							Phrase my = new Phrase("Please Refer Annexure For More Details",font11);
//							PdfPCell productTableCell = new PdfPCell(my);
//							productTableCell.setColspan(5);
//							table.addCell(productTableCell);
//							System.out.println("table "+table.size());
//						break;
//					}
					System.out.println("prouductCount "+prouductCount);
//					if (prouductCount==2) {
//						prouductCount = i;
//						break;
//					}
					int lenght=quot.getItems().get(i).getProductName().length();
					logger.log(Level.SEVERE,"Length : "+lenght);
//					int lines= (int) Math.ceil((lenght/36.0));
					int lines= (int) Math.ceil((lenght/31.0));
					logger.log(Level.SEVERE,"LINES : "+lines);
//					noOfLines = noOfLines - 1;
//					noOfLines = noOfLines - lines;
					
					if(lines==1){
						lines = 2;
					}
					noOfLines = noOfLines - lines;

					logger.log(Level.SEVERE,"NO OF LINES : "+noOfLines+" Lines "+lines);
				    
					//commented by ashwini
//					if (noOfLines <= 0) {
//						prouductCount = i;
//						System.out.println("table before annexture "+table.size());
//
//							Phrase my = new Phrase("Please Refer Annexure For More Details",font11);
//							PdfPCell productTableCell = new PdfPCell(my);
//							productTableCell.setColspan(5);
//							table.addCell(productTableCell);
//							System.out.println("table "+table.size());
//						break;
//					}
					System.out.println("adding next product");
					String premises ="NA";	
				    if(quot.getItems().get(i).getPremisesDetails()!=null && !quot.getItems().get(i).getPremisesDetails().equals("")){
				    	premises=quot.getItems().get(i).getPremisesDetails();
				    	
				    	int primiceslenght=quot.getItems().get(i).getPremisesDetails().length();
						logger.log(Level.SEVERE,"primiceslenght Length : "+primiceslenght);
						int primiceslines= (int) Math.ceil((lenght/16.0));
						logger.log(Level.SEVERE,"primiceslenght LINES : "+primiceslines);
//						noOfLines = noOfLines - 1;
						noOfLines = noOfLines - primiceslines;
						if(lines==1){
							lines = 2;
						}
						logger.log(Level.SEVERE,"primiceslenght NO OF LINES : "+noOfLines+" Lines "+lines);
				    }
					chunk = new Phrase(premises,font11);
					pdfpremise = new PdfPCell(chunk);
					pdfpremise.setHorizontalAlignment(Element.ALIGN_LEFT);			
					pdfpremise.setPaddingTop(3f);
					pdfpremise.setPaddingBottom(3f);
					pdfpremise.setPaddingLeft(3f);
					pdfpremise.setPaddingRight(3f);
					
					
					
					
					
					ServiceProduct serviceProduct = ofy()
							.load()
							.type(ServiceProduct.class)
							.filter("companyId", comp.getCompanyId())
							.filter("productCode",quot.getItems().get(i).getProductCode().trim()).first().now();
					
//					chunk = null;
//					if(quot.getItems().get(i).getProductName()!=null &&
//							!quot.getItems().get(i).getProductName().trim().equals("")){
//						
//						chunk = new Phrase(quot.getItems().get(i).getProductName(),font10);
//						 
//					}else{
//						
//				
//						if (serviceProduct.getProductCategory()!= null) {
//							chunk = new Phrase(serviceProduct.getProductCategory().trim(), font10);
////							quot.getItems().get(i).getProductCategory();
//							
//						} else {
//							chunk = new Phrase(serviceProduct.getProductCategory().trim(), font10);
//						}
//					}
					
					
//					Phrase phrase = new Phrase();
//					Chunk chunk1 = new Chunk(quot.getItems().get(i).getProductName());
//					chunk1.setAnchor(serviceProduct.getProductCategory().trim());
//					phrase.add(chunk1);
//					table.addCell(phrase);
//					
					
					
					if(serviceProduct!=null){
						logger.log(Level.SEVERE,"serviceProduct : "+serviceProduct);
//						if(serviceProduct.getProductCategory()!=null&&!serviceProduct.getProductCategory().equals("")){
//							Category prodCategory = ofy().load().type(Category.class)
//									.filter("companyId", comp.getCompanyId())
//									.filter("catName",serviceProduct.getProductCategory()).first().now();
//							
//							if(prodCategory!=null&&prodCategory.getDescription()!=null&&!prodCategory.getDescription().equals("")){
//								chunk = new Phrase();
//								Chunk chunk1 = new Chunk(serviceProduct.getProductCategory(), font11);
//								chunk1.setAnchor(prodCategory.getDescription().trim());
//								chunk1.setUnderline(0.1f, -2f);
//								chunk.add(chunk1);
//								
//							}else{
//								chunk = new Phrase(serviceProduct.getProductCategory().trim(), font11);
//							}
//						}else {
						
//							chunk = new Phrase(quot.getItems().get(i).getProductName() + "",font11);
						if(serviceProduct.getTermsAndConditionLink()!=null && !serviceProduct.getTermsAndConditionLink().equals("")){
							chunk = new Phrase();
							Chunk chunk1 = new Chunk(quot.getItems().get(i).getProductName(), font11);
							chunk1.setAnchor(serviceProduct.getTermsAndConditionLink());
							chunk1.setUnderline(0.1f, -2f);
							chunk.add(chunk1);

						}
						else{
							System.out.println("no link else block");
							chunk = new Phrase(quot.getItems().get(i).getProductName() + "",font11);
						}


						
//						}
					}else{
						chunk = new Phrase(quot.getItems().get(i).getProductName() + "",font11);
					}
					
					
					

					
					
//					chunk = new Phrase(quot.getItems().get(i).getProductName() + "",font10);
					
					pdfservices = new PdfPCell(chunk);
					pdfservices.setHorizontalAlignment(Element.ALIGN_LEFT);
					pdfservices.setPaddingTop(3f);
					pdfservices.setPaddingBottom(3f);
					pdfservices.setPaddingLeft(3f);
					pdfservices.setPaddingRight(3f);
					
					
					Calendar c = Calendar.getInstance();
					c.setTime(quot.getItems().get(i).getStartDate());
					c.add(Calendar.DATE, quot.getItems().get(i).getDuration() - 1);
					Date endDt = c.getTime();
					
					String date = "";
					if (quot.getItems().get(i).getEndDate() != null) {
						date = fmt.format(quot.getItems().get(i).getStartDate()) + "\n"+ fmt.format(quot.getItems().get(i).getEndDate());
					} else {
						date = fmt.format(quot.getItems().get(i).getStartDate()) + "\n"+ fmt.format(endDt);
					}
					
					chunk = new Phrase(date + "",font11);
					
					pdfduration = new PdfPCell(chunk);
					pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
					pdfduration.setPaddingTop(3f);
					pdfduration.setPaddingBottom(3f);
					pdfduration.setPaddingLeft(3f);
					pdfduration.setPaddingRight(3f);
					
					chunk = new Phrase(quot.getItems().get(i).getNumberOfServices() + "",font11);
					
					pdffrequency = new PdfPCell(chunk);
					pdffrequency.setHorizontalAlignment(Element.ALIGN_CENTER);
					pdffrequency.setPaddingTop(3f);
					pdffrequency.setPaddingBottom(3f);
					pdffrequency.setPaddingLeft(3f);
					pdffrequency.setPaddingRight(3f);
					
//					int pricechunk= (int) quot.getItems().get(i).getPrice();
					
					 //Ashwini Patil Date: 7-03-2022 Description: When qty is more that one for product it was printing rate for one qty. Reuired is the total amount.
					int area=1;
					if(!quot.getItems().get(i).getArea().equals("")&&!quot.getItems().get(i).getArea().equals("NA")){
						area = Integer.parseInt(quot.getItems().get(i).getArea());
					}
						
					int pricechunk= (int) quot.getItems().get(i).getPrice()*area;
					
					chunk = new Phrase(pricechunk+"",font11);
					
//					chunk = new Phrase(quot.getItems().get(i).getPrice() + "",font8);
					
					pdfprice = new PdfPCell(chunk);
					pdfprice.setHorizontalAlignment(Element.ALIGN_RIGHT);
					pdfprice.setPaddingTop(3f);
					pdfprice.setPaddingBottom(3f);
					pdfprice.setPaddingLeft(3f);
					pdfprice.setPaddingRight(3f);
					
					if(quot.isConsolidatePrice()){
						if (i == 0) {
							chunk = new Phrase(df.format(totalAmount), font11);
							pdfprice = new PdfPCell(chunk);
							pdfprice.setHorizontalAlignment(Element.ALIGN_RIGHT);
							if(quot.getItems().size() > 1)
								pdfprice.setBorderWidthBottom(0);
							
						}else {
							chunk = new Phrase(" ", font11);
							pdfprice = new PdfPCell(chunk);
							pdfprice.setHorizontalAlignment(Element.ALIGN_RIGHT);
							if(i == quot.getItems().size()-1 ){							
								pdfprice.setBorderWidthTop(0);
							}else{
								pdfprice.setBorderWidthBottom(0);
								pdfprice.setBorderWidthTop(0);
							}
						}	
					}
					
					
					table.addCell(pdfpremise);
					table.addCell(pdfservices);
					table.addCell(pdfduration);
					table.addCell(pdffrequency);
					table.addCell(pdfprice);
				}
			
			
			
			try {
				document.add(table);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		

}
