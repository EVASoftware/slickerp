package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class CreateServiceGSTInvoiceServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 781588311838095578L;
	Logger logger = Logger.getLogger("Servlet");
	
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("application/pdf");
		SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
		try {
			
		
		String stringid = request.getParameter("Id");
		String type = request.getParameter("type");
		stringid = stringid.trim();
		Long count = Long.parseLong(stringid);
		
		String printtype = "no";
		try {
			 printtype = request.getParameter("preprint");
		} catch (Exception e) {
		}

		Invoice invoiceentity = ofy().load().type(Invoice.class).id(count)
				.now();
		
		boolean isMultipleBillingDocflag = loadMultipleBillingDoc(invoiceentity);
		logger.log(Level.SEVERE, "isMultipleBillingDocflag="+isMultipleBillingDocflag);
		
		ServerAppUtility apputility = new ServerAppUtility();
		
		Company company = apputility.loadCompany(invoiceentity.getCompanyId());
		
		Branch branch=null;
		boolean branchAsCompany=false;
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", company.getCompanyId())){
			branch = ofy().load().type(Branch.class).filter("companyId",invoiceentity.getCompanyId()).filter("buisnessUnitName", invoiceentity.getBranch()).first().now();
			branchAsCompany=true;
		}
		Boolean tabularGstServiceInvoiceFlag=false;
		
		tabularGstServiceInvoiceFlag=apputility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "TabularGstServiceInvoice", invoiceentity.getCompanyId())	;
			
		//Ashwini Patil Date:02-05-2023
		String filename="Invoice - "+invoiceentity.getCount()+" - "+invoiceentity.getPersonInfo().getFullName()+" - "+fmt.format(invoiceentity.getInvoiceDate())+".pdf";
		if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Company", "EnablePrintPreview", invoiceentity.getCompanyId()))
				response.setHeader("Content-Disposition", "attachment; filename=" + filename);
		System.out.println("filename="+filename);
		 
		if(type.equalsIgnoreCase("SingleBilling")){
			if(tabularGstServiceInvoiceFlag){
				System.out.println("in tabularGstServiceInvoiceFlag");
				ServiceGSTInvoiceWithTaxColumns invpdf = new ServiceGSTInvoiceWithTaxColumns();
				invpdf.document = new Document();
				Document document = invpdf.document;
				
				
				
				PdfWriter writer = PdfWriter.getInstance(document,
						response.getOutputStream());

				if (invoiceentity.getStatus().equals(Invoice.CANCELLED)) {
					writer.setPageEvent(new PdfCancelWatermark());
				}
				else if (!invoiceentity.getStatus().equals("Approved")
						&& (!invoiceentity.getStatus().equals(
								BillingDocument.PROFORMAINVOICE))
						&& (!invoiceentity.getStatus().equals(
								BillingDocument.BILLINGINVOICED))) {
					writer.setPageEvent(new PdfWatermark());
				}
				
		 	   String preprint = "no";
		 	   if(company.getUploadHeader()!=null && company.getUploadFooter()!=null){
		 		   document.setPageSize(PageSize.A4);
			 	   document.setMargins(30, 30, 120, 100);
			 	   document.setMarginMirroring(false);
			 	   
		 		  HeaderFooterPageEvent event = new HeaderFooterPageEvent(company,invoiceentity.getBranch(),"No");
			 	   writer.setPageEvent(event);
		 	   }
		 	   else{
		 		  preprint = "plane";
		 	   }
		 	   
				document.open();
				invpdf.setInvoice(count);
				invpdf.createPdf(preprint);
				document.close();
				
			}else{
				ServiceGSTInvoiceVersion1 invpdf = new ServiceGSTInvoiceVersion1();
				invpdf.document = new Document();
				Document document = invpdf.document;
				PdfWriter writer = PdfWriter.getInstance(document,
						response.getOutputStream());

				if (invoiceentity.getStatus().equals(Invoice.CANCELLED)) {
					writer.setPageEvent(new PdfCancelWatermark());
				}
				else if (!invoiceentity.getStatus().equals("Approved")
						&& (!invoiceentity.getStatus().equals(
								BillingDocument.PROFORMAINVOICE))
						&& (!invoiceentity.getStatus().equals(
								BillingDocument.BILLINGINVOICED))) {
					writer.setPageEvent(new PdfWatermark());
				}
				
		 	   String preprint = printtype;
		 	   if(branchAsCompany&&branch!=null) {
		 		  if(branch.getUploadHeader()!=null && branch.getUploadFooter()!=null ){
		 			 if(branch.getUploadHeader().getUrl()!=null&&!branch.getUploadHeader().getUrl().equals("")&&branch.getUploadFooter().getUrl()!=null&&!branch.getUploadFooter().getUrl().equals("")) {
		 			 		
			 		   document.setPageSize(PageSize.A4);
				 	   document.setMargins(30, 30, 120, 100);
				 	   document.setMarginMirroring(false);
//				 	  logger.log(Level.SEVERE, "calling HeaderFooterPageEvent for branch "+branch.getBusinessUnitName());
						
			 		  HeaderFooterPageEvent event = new HeaderFooterPageEvent(company,invoiceentity.getBranch(),printtype);
				 	   writer.setPageEvent(event);
		 			 }
			 	   }
		 	   }
		 	   if(company.getUploadHeader()!=null && company.getUploadFooter()!=null ){
		 		   if(company.getUploadHeader().getUrl()!=null&&!company.getUploadHeader().getUrl().equals("")&&company.getUploadFooter().getUrl()!=null&&!company.getUploadFooter().getUrl().equals("")) {
 			 		   document.setPageSize(PageSize.A4);
				 	   document.setMargins(30, 30, 120, 100);
				 	   document.setMarginMirroring(false);
//				 	  logger.log(Level.SEVERE, "calling HeaderFooterPageEvent for company");
						
			 		  HeaderFooterPageEvent event = new HeaderFooterPageEvent(company,invoiceentity.getBranch(),printtype);
				 	   writer.setPageEvent(event);
			 		   
		 		   }
		 	   }
//		 	   else{
//		 		  preprint = "plane";
//		 	   }
		 	   
				document.open();
				invpdf.setInvoice(count);
				invpdf.createPdf(preprint,isMultipleBillingDocflag);
				document.close();
			}	
			
			
		}else if(type.equalsIgnoreCase("MultipleBilling")){
			if(tabularGstServiceInvoiceFlag){
				System.out.println("in tabularGstServiceInvoiceFlag");
				ServiceGSTInvoiceWithTaxColumns invpdf = new ServiceGSTInvoiceWithTaxColumns();
				invpdf.document = new Document();
				Document document = invpdf.document;
				PdfWriter writer = PdfWriter.getInstance(document,
						response.getOutputStream());

				if (invoiceentity.getStatus().equals(Invoice.CANCELLED)) {
					writer.setPageEvent(new PdfCancelWatermark());
				}
				else if (!invoiceentity.getStatus().equals("Approved")
						&& (!invoiceentity.getStatus().equals(
								BillingDocument.PROFORMAINVOICE))
						&& (!invoiceentity.getStatus().equals(
								BillingDocument.BILLINGINVOICED))) {
					writer.setPageEvent(new PdfWatermark());
				}
				
		 	   String preprint = "no";
		 	   if(company.getUploadHeader()!=null && company.getUploadFooter()!=null){
		 		   document.setPageSize(PageSize.A4);
			 	   document.setMargins(30, 30, 120, 100);
			 	   document.setMarginMirroring(false);
			 	   
		 		  HeaderFooterPageEvent event = new HeaderFooterPageEvent(company,invoiceentity.getBranch(),"no");
			 	   writer.setPageEvent(event);
		 	   }
		 	   else{
		 		  preprint = "plane";
		 	   }
		 	   
				document.open();
				invpdf.setInvoice(count);
				invpdf.createPdf(preprint);
				document.close();
				
			}
		}
			
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private boolean loadMultipleBillingDoc(Invoice invoiceentity) {
		// TODO Auto-generated method stub

		HashSet<Integer> billCount = new HashSet<Integer>();
		for (BillingDocumentDetails billingDoc : invoiceentity.getArrayBillingDocument()) {
			billCount.add(billingDoc.getBillId());
		}
		if (billCount.size() > 1) {
			return true;
		}
		return false;
	}
}
