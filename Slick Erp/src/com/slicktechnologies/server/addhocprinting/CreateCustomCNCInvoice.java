package com.slicktechnologies.server.addhocprinting;


import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.addhocprinting.fm.CustomizedCncInvoiceVerOne;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class CreateCustomCNCInvoice extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7274437427671927675L;
	
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("application/pdf"); // Type of response , helps
													// browser to identify the
													// response type.

		try {
			String stringid = request.getParameter("Id").trim();
			String preprintStatus = request.getParameter("preprint");
			
			String version="";
			long count=0;
			try{
				version = request.getParameter("version");
				count=Long.parseLong(stringid);
			}catch(Exception e){
				
			}
			Invoice invoiceentity = ofy().load().type(Invoice.class).id(count).now();
			
			if(version!=null&&!version.equals("")){
				if(version.equals("one")){
					CustomizedCncInvoiceVerOne cncInvoice = new CustomizedCncInvoiceVerOne();
					cncInvoice.document = new Document();
					Document document = cncInvoice.document;
					PdfWriter writer =PdfWriter.getInstance(document, response.getOutputStream()); 
					
					if (invoiceentity.getStatus().equals("Cancelled")) {
						writer.setPageEvent(new PdfCancelWatermark());
					} else if (!invoiceentity.getStatus().equals("Approved")) {
						writer.setPageEvent(new PdfWatermark());
					}
					document.open();
		
					if (stringid != null) {
						stringid = stringid.trim();
//						Long count = Long.parseLong(stringid);
						cncInvoice.setInvoice(count);
						cncInvoice.createPdf(preprintStatus);
						document.close();
					} 
				}
				
			}else{
				CustomCNCInvoicePdf cncInvoice = new CustomCNCInvoicePdf();
				cncInvoice.document = new Document();
				Document document = cncInvoice.document;
				PdfWriter.getInstance(document, response.getOutputStream()); 
				document.open();
	
				if (stringid != null) {
					stringid = stringid.trim();
//					Long count = Long.parseLong(stringid);
					cncInvoice.setInvoice(count);
					cncInvoice.createPdf(preprintStatus);
					document.close();
				} 
			}
			
		}catch (DocumentException e) {
			e.printStackTrace();
		}	
	}

}
