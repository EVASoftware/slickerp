package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.simplesoftwares.client.library.libservice.PdfService;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.inventory.MaterialConsumptionReport;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.paymentmodelayer.vendorpayment.VendorPayment;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.workorder.WorkOrder;

/**
 * Date 15-05-2018 
 * Developer: Vijay
 * Des :- this pdf for IMP And Fumigation with process config
 */

@SuppressWarnings("serial")
public class ServicePdfForIMP extends RemoteServiceServlet implements PdfService{
	Logger logger = Logger.getLogger("Size");

	
	public static ArrayList<Service>servicearaylist;
	
	private  Font font16boldul,font8bold,font8,font12;
	private SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");
	public  Document document;
	public Company comp;
	public int serviceno=1;
	
	public ServicePdfForIMP()
	{
		font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD| Font.UNDERLINE);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
		font8= new Font(Font.FontFamily.HELVETICA,8);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12=new Font(Font.FontFamily.HELVETICA,12);
		
		 if(servicearaylist!=null)
		 {
		     if(servicearaylist.size()!=0)
		     {
		    	 Long companyid=servicearaylist.get(0).getCompanyId();
		    	 if(companyid!=null)
		    		 comp=ofy().load().type(Company.class).filter("companyId",companyid).first().now();
		    	 else
		    		 comp=ofy().load().type(Company.class).first().now(); 
		     }
			
			
		 
		 }
		 
		 System.out.println("Company ID IS ");
		
		
	}
	
	public  void createPdf() {
		if(comp.getLogo()!=null){
		createLogo(document,comp);
		}
		createCompanyHedding();
	      createCompanyAdress();
	     
				try {
					createServices();
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
	  
	    }
	
	
	
	
	public  void createCompanyHedding()
	{
		
		 Phrase companyName= new Phrase(comp.getBusinessUnitName(),font16boldul);
	     Paragraph p =new Paragraph(companyName);
	     p.setAlignment(Element.ALIGN_CENTER);
	     try {
			document.add(p);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public  void createCompanyAdress()
	{
		
		Phrase adressline1= new Phrase(comp.getAddress().getAddrLine1(),font12);
		Phrase adressline2=null;
		if(comp.getAddress().getAddrLine2()!=null)
		{
			adressline2= new Phrase(comp.getAddress().getAddrLine2(),font12);
		}
		
		
		Phrase landmark=null;
		Phrase locality=null;
		
//		if(comp.getAddress().getLocality()!=null&&comp.getAddress().getLandmark().equals("")==false)
//		{
//			
//			String landmarks=comp.getAddress().getLandmark()+"  ";
//			locality= new Phrase(landmarks+comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
//				      +comp.getAddress().getPin(),font12);
//		}
//		else
//		{
//			locality= new Phrase(comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
//				      +comp.getAddress().getPin(),font12);
//		}
		/**
		 * Date 27/12/2017
		 * By Jayshree
		 * To handle the null pointer exception
		 */
		
		if(!comp.getAddress().getLandmark().equals("")&&!comp.getAddress().getLocality().equals("")){
			
			locality= new Phrase(comp.getAddress().getLandmark()+","+comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin(),font12);
		}
		else if(comp.getAddress().getLandmark().equals("")&&!comp.getAddress().getLocality().equals("")){
			locality= new Phrase(comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin(),font12);
		}
		else if(comp.getAddress().getLocality().equals("")&&!comp.getAddress().getLandmark().equals("")){
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin(),font12);
		}
		else if(comp.getAddress().getLocality().equals("")&&comp.getAddress().getLandmark().equals("")){
			locality= new Phrase(comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin(),font12);
		}
		Paragraph adressPragraph=new Paragraph();
		adressPragraph.add(adressline1);
		adressPragraph.add(Chunk.NEWLINE);
		if(adressline2!=null)
		{
				adressPragraph.add(adressline2);
				adressPragraph.add(Chunk.NEWLINE);
		}
		
		
		
		adressPragraph.add(locality);
		
		adressPragraph.setAlignment(Element.ALIGN_CENTER);
		adressPragraph.setSpacingBefore(10);
		adressPragraph.setSpacingAfter(10);
		
		
		// Phrase for phone,landline ,fax and email
		
		Phrase titlecell=new Phrase("Mob :",font8bold);
		Phrase titleTele = new Phrase("Tele :",font8bold);
		Phrase titleemail= new Phrase("Email :",font8bold);
		Phrase titlefax= new Phrase("Fax :",font8bold);
		
		// serviceDate and Status
		Phrase serviceDate=new Phrase("Service Date :",font8bold);
		Phrase serStatus=new Phrase("Service Status :",font8bold);

		// cell number logic
		String stringcell1=comp.getCellNumber1()+"";
		String stringcell2=null;
		Phrase mob=null;
		/**
		 * Date 28/12/2017
		 * By Jayshree
		 * To Handle the null condition
		 */
		if(comp.getCellNumber2()!=0)
			stringcell2=comp.getCellNumber2()+"";
		if(stringcell2!=null)
		    mob=new Phrase(stringcell1+" / "+stringcell2,font8);
		else
			 mob=new Phrase(stringcell1,font8);
		
		
		//LANDLINE LOGIC
		/**
		 * Date 28/12/2017
		 * By Jayshree
		 * To Handle the null condition
		 */
		Phrase landline=null;
		if(comp.getLandline()!=0){
			landline=new Phrase(comp.getLandline()+"",font8);
		}else{
			landline=new Phrase(" ",font8);
		}
		// fax logic
		Phrase fax=null;
		if(comp.getFaxNumber()!=null)
			fax=new Phrase(comp.getFaxNumber()+"",font8);
		// email logic
		Phrase email= new Phrase(comp.getEmail(),font8);
		
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(titlecell);
		adressPragraph.add(mob);
		adressPragraph.add(new Chunk("            "));
		
		
		if(landline!=null)
		{
			
			adressPragraph.add(titleTele);
			adressPragraph.add(landline);
			adressPragraph.add(new Chunk("            "));
		}
		
		
		
		if(fax!=null)
		{
			
			adressPragraph.add(titlefax);
			adressPragraph.add(fax);
			adressPragraph.add(Chunk.NEWLINE);
		}
		
		Phrase phserviceDate=null;
		if(servicearaylist.size()!=0){
			String sDate = fmt.format(servicearaylist.get(0).getServiceDate());
			phserviceDate = new Phrase(sDate, font8);
		}else{
			phserviceDate = new Phrase("", font8);
		}
		
		Phrase phserviceStatus=null;
		if(servicearaylist.size()!=0){
			String serviceStatus = servicearaylist.get(0).getStatus();
			phserviceStatus = new Phrase(serviceStatus, font8);
		}else{
			phserviceStatus = new Phrase("", font8);
		}
				
		adressPragraph.add(titleemail);
//		adressPragraph.add(new Chunk("            "));
		adressPragraph.add(email);
		adressPragraph.add(new Chunk("            "));
		
		adressPragraph.add(serviceDate);
		adressPragraph.add(phserviceDate);
		adressPragraph.add(new Chunk("            "));
		
		adressPragraph.add(serStatus);
		adressPragraph.add(phserviceStatus);
		adressPragraph.add(new Chunk("            "));
		
		try {
			document.add(adressPragraph);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//Patch Patch Pradnya use Reflection
	public void createServices() throws DocumentException
    {
    	
		/**
		 * Developer :  Jayshree 
		 * Date 16/11/2017
		 * Description : Changes are made to set the column widths
		 */
		PdfPTable table = new PdfPTable(13);
		table.setWidthPercentage(100);
		table.setWidths(new float[]{4,7,7,7,8,7,7,8,8,7,7,7,14});
		createServiceHeader(table);
		/**
		 * @author Abhinav
		 * @since 29/11/2019
		 * For IPM and Fumigation, Requirement Raised by Viashnavi Pawar, they want multiple service engg print on pdf when click on plan popup button add employee 
		 */

		Map<Integer, ServiceProject> serProjMap = new HashMap<Integer, ServiceProject>();

		List<Integer> serIdList = new ArrayList<Integer>();
		for (Service s : servicearaylist) {
			serIdList.add(s.getCount());
		}
		List<ServiceProject> serviceProjectList = null;
		if (serIdList != null && serIdList.size() > 0) {
			serviceProjectList = ofy().load().type(ServiceProject.class)
					.filter("companyId", servicearaylist.get(0).getCompanyId())
					.filter("serviceId IN", serIdList).list();
			logger.log(Level.SEVERE,"serviceProjectList1="+serviceProjectList.size());
		}
		if (serviceProjectList != null && serviceProjectList.size() > 0) {
			for (ServiceProject serProject : serviceProjectList) {
				serProjMap.put(serProject.getserviceId(), serProject);
				logger.log(Level.SEVERE,"Map="+serProjMap.size());
			}
		}

		for (Service s : servicearaylist) {

			String sno = this.serviceno + "";

			Chunk serno = new Chunk(sno, font8);
			PdfPCell sernocell = new PdfPCell();
			sernocell.addElement(serno);

			String serviceId = s.getCount() + "";
			Chunk serviceid = new Chunk(serviceId, font8);
			PdfPCell seridcell = new PdfPCell();
			seridcell.addElement(serviceid);

			String contractid = s.getContractCount() + "";
			Chunk scontractid = new Chunk(contractid, font8);
			PdfPCell scontractcell = new PdfPCell();
			scontractcell.addElement(scontractid);

			// String customerid = s.getPersonInfo().getCount()+"";
			// Chunk sid= new Chunk(customerid,font8);
			// PdfPCell scustomerid= new PdfPCell();
			// scustomerid.addElement(sid);

			String customername = s.getPersonInfo().getFullName();
			Chunk sfullname = new Chunk(customername, font8);
			PdfPCell scustomernamecell = new PdfPCell();
			scustomernamecell.addElement(sfullname);

			// String customerphone = s.getPersonInfo().getCellNumber()+"";
			String blank = " ";
			Chunk chunkblank = new Chunk(blank, font8);
			PdfPCell blankCell = new PdfPCell();
			blankCell.addElement(chunkblank);
			// vijay

			PdfPCell scelladress = new PdfPCell();
			String locality = "";
			// String adressline1=s.getAddress().getAddrLine1();
			// adress=adress+adressline1;
			//
			//
			// if(s.getAddress().getLandmark()!=null)
			// adress=adress+s.getAddress().getLandmark();
			// if(s.getAddress().getLocality()!=null)
			// adress=adress+"\n"+s.getAddress().getLocality();
			// adress=adress+" "+s.getAddress().getPin();

			if (s.getAddress().getLocality() != null)
				locality = s.getAddress().getLocality();
			Chunk chunk = new Chunk(locality + "", font8);
			scelladress.addElement(chunk);

			PdfPCell scellproduct = new PdfPCell();
			chunk = new Chunk(s.getProduct().getProductName() + "", font8);
			scellproduct.addElement(chunk);

			PdfPCell cellTime = new PdfPCell();
			String stime = "";
			if (!s.getServiceTime().equalsIgnoreCase("Flexible")) {
				stime = s.getServiceTime();
			}
			chunk = new Chunk(stime + "", font8);
			cellTime.addElement(chunk);

			PdfPCell celltime = new PdfPCell();
			int hours = 0;
			int min = 0;
			String time = 0 + "";
			chunk = new Chunk(time + "", font8);
			celltime.addElement(chunk);

			// String serviceEng=s.getEmployee();
			// chunk=null;
			String technicians="";
			ServiceProject proj = serProjMap.get(s.getCount());
			if (proj != null && proj.getTechnicians() != null
					&& proj.getTechnicians().size() > 0) {
				
				for (EmployeeInfo object : proj.getTechnicians()) {
					technicians = technicians + object.getFullName() + ", ";
					logger.log(Level.SEVERE,"Tecnician="+technicians);
				}
			} else {
				if (s.getEmployee() != null) {
					technicians = s.getEmployee()+", ";
				}
			}
			try {
				if (!technicians.equals("")) {
					technicians = technicians.substring(0,
							technicians.length() - 2);
				}
			} catch (Exception e) {

			}

			if (technicians != null)
				chunk = new Chunk(technicians, font8);
			else
				chunk = new Chunk("Not Assigned", font8);

			PdfPCell cellserv = new PdfPCell();
			cellserv.addElement(chunk);

			PdfPCell scellbranch = new PdfPCell();
			chunk = new Chunk(s.getBranch(), font8);
			scellbranch.addElement(chunk);

			// PdfPCell scellreasonforchange = new PdfPCell();
			// if(s.getReasonForChange()!=null)
			// chunk =new Chunk(s.getReasonForChange(),font8);
			// else
			// chunk = new Chunk("  ",font8);
			// scellreasonforchange.addElement(chunk);

			PdfPCell scellstatus = new PdfPCell();
			chunk = new Chunk(s.getStatus(), font8);
			scellstatus.addElement(chunk);

			/*
			 * Date:16/07/2018 Developer:Ashwini Purpose:To display customer
			 * branch while printing Customer Service List
			 */

			String customerbranch = s.getServiceBranch();
			Chunk scustomerbranch = new Chunk(customerbranch, font8);
			PdfPCell scustomerbranchCell = new PdfPCell();
			scustomerbranchCell.addElement(scustomerbranch);

			/*
			 * End by Ashwini
			 */

			List<ServiceProject> serviceProjectlist = ofy().load()
					.type(ServiceProject.class)
					.filter("companyId", s.getCompanyId())
					.filter("serviceId", s.getCount())
					.filter("contractId", s.getContractCount()).list();
			String chemical = "";
			String quantity = "";
			for (ServiceProject serviceProject : serviceProjectlist) {
				for (ProductGroupList chemicallist : serviceProject
						.getProdDetailsList()) {
					chemical = chemical + chemicallist.getName() + ",";
					quantity = quantity + chemicallist.getQuantity() + ",";
				}
			}

			Phrase phChemical = new Phrase(chemical, font8);
			Phrase phQuantity = new Phrase(quantity, font8);

			table.addCell(sernocell);
			table.addCell(seridcell);
			table.addCell(scontractcell);
			// table.addCell(scustomerid);
			table.addCell(scustomernamecell);
			// table.addCell(scustomerphonecell);
			table.addCell(scelladress);

			table.addCell(scellproduct);
			table.addCell(phChemical);// vijay chemical
			table.addCell(phQuantity);// chemical quontity
			table.addCell(cellTime);
			table.addCell(scellbranch);
			// table.addCell(scellreasonforchange);
			// table.addCell(scellstatus);
			table.addCell(cellserv);
			table.addCell(scustomerbranchCell);// Added by Ashwini
			table.addCell(blankCell);// vijay
			// table.addCell(blankCell);//vijay //commented by Ashwini
			serviceno++;
		}
		document.add(table);

	}

	private void createServiceHeader(PdfPTable table) {
		/**
		 * Developer : Jayshree 
		 * Date: 14/11/2017
		 * Description : changes are made to set the horizontal alignment center
		 */
        Phrase seviceno = new Phrase("Sr No.",this.font8bold);
        PdfPCell servicenumbercell= new PdfPCell(seviceno);
//        servicenumbercell.addElement(seviceno);
        servicenumbercell.setHorizontalAlignment(Element.ALIGN_CENTER);
        
        
        Phrase seviceid = new Phrase("Service ID",this.font8bold);
        PdfPCell serviceidcell= new PdfPCell(seviceid);
//        serviceidcell.addElement(seviceid);
        serviceidcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        
        
        Phrase contractid = new Phrase("Contract ID",this.font8bold);
        PdfPCell contrctcell= new PdfPCell(contractid);
//        contrctcell.addElement(contractid);
        contrctcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        
//        Phrase cidchunk=new Phrase("Customer ID",font8bold);
//        PdfPCell customerIdCell= new PdfPCell(cidchunk);
////        customerIdCell.addElement(cidchunk);
//        customerIdCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        
        Phrase customernamechunk=new Phrase("Customer Name",font8bold);
        PdfPCell customernamecell= new PdfPCell(customernamechunk);
//        customernamecell.addElement(customernamechunk);
        customernamecell.setHorizontalAlignment(Element.ALIGN_CENTER);
        
        /**
         * Date 15-05-2018 
         * Developer: Vijay
         * Des :- this pdf for IMP And Fumigation 
         */
        Phrase report=new Phrase("Report",font8bold);
        PdfPCell reportcell= new PdfPCell(report);
//        customerphonecell.addElement(customerphonechunk);
        reportcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        /**
         * ends here
         */
        
        /**
         * Ends for Jayshree
         */
        
		
		// vijay 
        Chunk chunk1 = null;
        Phrase locality= new Phrase("Locality",font8bold);
		PdfPCell localitycell= new PdfPCell(locality);
//		adresscell.addElement(chunk1);
		localitycell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase product = new Phrase("Product",this.font8bold);
		PdfPCell productcell= new PdfPCell(product);
//		productcell.addElement(chunk1);
		productcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase serviceTime=new Phrase("Service Time",font8bold);
		PdfPCell serviceTimecell= new PdfPCell(serviceTime);
//		servicedatecell.addElement(chunk1);
		serviceTimecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		Phrase serviceeng=new Phrase("Service Engineer",font8bold);
		PdfPCell serviceengineerell= new PdfPCell(serviceeng);
//		serviceengineerell.addElement(chunk1);
		serviceengineerell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase branch=new Phrase("Branch",font8bold);
		PdfPCell branchcell= new PdfPCell(branch);
//		branchcell.addElement(chunk1);
		branchcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
//		Phrase reason=new Phrase("Reason For Change",font8bold);
//		PdfPCell reasonforchange= new PdfPCell(reason);
////		reasonforchange.addElement(chunk1);
//		reasonforchange.setHorizontalAlignment(Element.ALIGN_CENTER);
		
//		Phrase status=new Phrase("Status",font8bold);
//		PdfPCell statuscell= new PdfPCell(status);
////		statuscell.addElement(chunk1);
//		statuscell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase chemical=new Phrase("Chemical",font8bold);
		PdfPCell chemicalCell= new PdfPCell(chemical);
//		statuscell.addElement(chunk1);
		chemicalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase checmicalQty=new Phrase("Quantity",font8bold);
		PdfPCell checmicalQtyCell= new PdfPCell(checmicalQty);
//		statuscell.addElement(chunk1);
		checmicalQtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		 /*
		 * Commented by Ashwini
		 */
		
//		Phrase documents=new Phrase("Documents",font8bold);
//		PdfPCell documentsCell= new PdfPCell(documents);
////		statuscell.addElement(chunk1);
//		documentsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		/*
		 * Developer:Ashwini
		 * Date:16/07/2018
		 * Purpose:to add customer branch while printing customer service list.
		 */
		
		Phrase documents=new Phrase("Customer Branch",font8bold);
		PdfPCell documentsCell= new PdfPCell(documents);
//		statuscell.addElement(chunk1);
		documentsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		 /*
		 * End by Ashwini
		 */
		
		
		table.addCell(servicenumbercell);
		table.addCell(serviceidcell);
		table.addCell(contrctcell);
//		table.addCell(customerIdCell);
		table.addCell(customernamecell);
//		table.addCell(customerphonecell);
	    table.addCell(localitycell);
		table.addCell(productcell);
		table.addCell(chemicalCell);
		table.addCell(checmicalQtyCell);
		table.addCell(serviceTimecell);
		table.addCell(branchcell);
//		table.addCell(reasonforchange);
//		table.addCell(statuscell);
		table.addCell(serviceengineerell);
		table.addCell(documentsCell);

		table.addCell(reportcell);
		}
	
	public void addtaxparagraph()
	{
		Paragraph taxpara= new Paragraph();
		Phrase titleservicetax=new Phrase("Service Tax No     :",font8bold);
		Phrase titlevatatx=new Phrase("Vat Tax No :    ",font8bold);
		Phrase titlelbttax=new Phrase("LBT Tax No :    ",font8bold);
		Phrase space = new Phrase("									");
		String serv = null,lbt,vat = null;
		if(comp.getServiceTaxNo()==null)
			serv="";
		else
			serv=comp.getServiceTaxNo();
		if(comp.getVatTaxNo()==null)
			vat="";
		else
			vat=comp.getVatTaxNo();
		
		if(serv.equals("")==false)
		{
			taxpara.add(titleservicetax);
			Phrase servp= new Phrase(serv,font8);
			taxpara.add(servp);
		}
		taxpara.add(space);
		
		if(vat.equals("")==false)
		{
			taxpara.add("                ");
			taxpara.add(titlevatatx);
			Phrase vatpp= new Phrase(vat,font8);
			taxpara.add(vatpp);
		}  	 
		
		taxpara.setSpacingBefore(80);
		taxpara.setAlignment(Element.ALIGN_BASELINE);
		
		try {
			taxpara.setSpacingAfter(10);
			document.add(taxpara);
			
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

private void createLogo(Document doc, Company comp) {
	DocumentUpload document =comp.getLogo();

	//patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(20f);
		image2.setAbsolutePosition(40f,765f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
//	try
//	{
//	Image image1=Image.getInstance("images/ultrapest.jpg");
//	image1.scalePercent(20f);
//	image1.setAbsolutePosition(40f,765f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}

}
	

	

	@Override
	public void setServiceList(ArrayList<Service> aray)
			throws IllegalArgumentException {
		servicearaylist=aray;
	
		
	}

	@Override
	public void createPdf(Quotation quot) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createPdf(Invoice inv) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createPdf(SalesQuotation squot) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createPdf(PurchaseOrder purchaseorder) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createPdf(CompanyPayment companypayment) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createPdf(DeliveryNote dnote) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createPdf(VendorPayment vendorpayment) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createPdf(Service customerservice) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createPdf(Inspection inspection) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createPdf(WorkOrder workOrder) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createPdf(CustomerPayment custPayment) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createPdf(MaterialConsumptionReport report) {
		// TODO Auto-generated method stub
		
	}


}
