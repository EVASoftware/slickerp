package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
public class JobCompletionCertificatePdf {

	public Document document;
	Logger logger = Logger.getLogger("Name of logger");
	// boolean showPreviousRecordFlag = false;
	ProcessConfiguration processConfig;
	boolean antipestFlag = false;

	Service service;
	ServiceProject serproject;
	CustomerBranchDetails customerbranch;
	Branch branch;
	Customer cust;
	Company comp;
	Contract con;
	SuperProduct sup;
	String str, str1;
	Phrase chunk;
	Date servDate, fromDate, toDate;
	PdfPCell pdfprodname, pdfpremises, pdfchemical, pdfantidote, pdfsign,
			pdftech;

	ProductGroupList proglist;
	List<CompanyAsset> tooltable;
	ProductGroupDetails prodGrpEntity = null;
	List<ProductGroupList> billMaterialLis;

	ArrayList<ProductGroupList> list = new ArrayList<ProductGroupList>();
	List<EmployeeInfo> technicians = new ArrayList<EmployeeInfo>();
	List<ProductGroupList> prodList = new ArrayList<ProductGroupList>();
	ArrayList<SuperProduct> stringlis = new ArrayList<SuperProduct>();
	List<Service> serviceList = new ArrayList<Service>();

	List<Service> serList = new ArrayList<Service>();

	// List<EmployeeInfo> emp;
	// List<Employee> emp;
	// List<BillProductDetails> billMaterialLis;

	float[] colWidths = { 0.5f, 0.2f, 0.5f };
	float[] colWidths1 = { 0.7f, 0.3f, 0.3f };
	float[] colWidths2 = { 2f, 3f, 3f, 3.1f, 3.2f };
	float[] colWidths3 = { 3.5f, 6f, 2f, 2f, 3.9f };
	float[] colWidths4 = { 5.9f, 8f, 3f };
	float[] colWidths5 = { 2f, 3.9f };

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt1 = new SimpleDateFormat("MMM-yyyy");
	DecimalFormat df = new DecimalFormat("0.00");

	private Font font16boldul, font12bold, font8bold, font8, font12boldul,
			font10boldul, font12, font16bold, font10, font10bold, font14bold,
			font14, font9, font1, font9bold, font9boldul, font9ul;

	public JobCompletionCertificatePdf() {
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
				| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14 = new Font(Font.FontFamily.HELVETICA, 14, Font.NORMAL);
		font9 = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD
				| Font.UNDERLINE);
		font1 = new Font(Font.FontFamily.HELVETICA, 7);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font9boldul = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD
				| Font.UNDERLINE);
		font9ul = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL
				| Font.UNDERLINE);
	}

	public void setpdfjobcerti(Long count) {

		// Load Service
		service = ofy().load().type(Service.class).id(count).now();

		// All services
		List<Service> serviceLst = ofy().load().type(Service.class)
				.filter("companyId", service.getCompanyId())
				.filter("contractCount", service.getContractCount()).list();

		System.out.println("Service List :: " + serviceLst.size());
		for (Service ser : serviceLst) {
			if (ser.getServiceSerialNo() < service.getServiceSerialNo()) {
				serviceList.add(ser);
			}
		}

		System.out.println("Service List SIZE :: " + serviceList.size());

		// Load Service project
		if (service.getCompanyId() == null)
			serproject = ofy().load().type(ServiceProject.class)
					.filter("companyId", service.getCompanyId()).first().now();
		else
			serproject = ofy().load().type(ServiceProject.class)
					.filter("companyId", service.getCompanyId())
					.filter("serviceId", service.getCount())
					.filter("serviceSrNo", service.getServiceSerialNo())
					.first().now();

		// Load Branch
		if (service.getCompanyId() == null)
			branch = ofy().load().type(Branch.class)
					.filter("companyId", service.getCompanyId()).first().now();
		else
			branch = ofy().load().type(Branch.class)
					.filter("companyId", service.getCompanyId())
					.filter("buisnessUnitName", service.getBranch()).first()
					.now();

		// Load Customer
		if (service.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", service.getPersonInfo().getCount())
					.first().now();
		else
			cust = ofy().load().type(Customer.class)
					.filter("count", service.getPersonInfo().getCount())
					.filter("companyId", service.getCompanyId()).first().now();

		// Load company
		if (service.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", service.getCompanyId()).first().now();

		// Load contract
		if (service.getCompanyId() == null)
			con = ofy().load().type(Contract.class)
					.filter("companyId", service.getCompanyId()).first().now();
		else
			con = ofy().load().type(Contract.class)
					.filter("companyId", service.getCompanyId())
					.filter("count", service.getContractCount()).first().now();

		// Load customer branch
		if (service.getCompanyId() == null)
			customerbranch = ofy().load().type(CustomerBranchDetails.class)
					.filter("companyId", service.getCompanyId()).first().now();
		else
			customerbranch = ofy().load().type(CustomerBranchDetails.class)
					.filter("companyId", service.getCompanyId())
					.filter("buisnessUnitName", service.getServiceBranch())
					.first().now();

		int prodGrpId = 0;
		if (serproject != null) {
			if (serproject.getBillMaterialLis().size() != 0) {
				prodGrpId = serproject.getBillMaterialLis().get(0)
						.getProdGroupId();
			}
		}

		// ***********************rohan changes here on
		// 8/6/15*********************888
		if (serproject != null) {
			if (serproject.getProdDetailsList().size() != 0) {
				prodList = serproject.getProdDetailsList();
			}
		}

		// *************************changes ends here
		// *******************************

		if (prodGrpId != 0) {
			prodGrpEntity = ofy().load().type(ProductGroupDetails.class)
					.filter("companyId", service.getCompanyId())
					.filter("count", prodGrpId).first().now();
		}

		if (serproject != null) {
			technicians = serproject.getTechnicians();
		}

		if (serproject != null) {
			tooltable = serproject.getTooltable();
		}
		if (prodGrpEntity != null) {
			billMaterialLis = prodGrpEntity.getPitems();
		}

		if (service.getCompanyId() != null) {
			sup = ofy().load().type(SuperProduct.class)
					.filter("companyId", service.getCompanyId())
					.filter("count", service.getProduct().getCount()).first()
					.now();

			SuperProduct superprod = new SuperProduct();

			superprod.setCount(sup.getCount());
			superprod.setProductName(sup.getProductName());
			superprod.setComment(sup.getComment() + " " + sup.getCommentdesc());

			stringlis.add(superprod);

		} else {
			sup = ofy().load().type(SuperProduct.class)
					.filter("count", service.getProduct().getCount()).first()
					.now();
		}

		logger.log(Level.SEVERE, "Comapany Id " + service.getCompanyId());
		logger.log(Level.SEVERE, "CONTRACT ID " + service.getContractCount());

		// List<Service> ser = ofy().load().type(Service.class)
		// .filter("companyId", service.getCompanyId())
		// .filter("contractCount", service.getContractCount())
		// .filter("serviceDate >=", fromDate).filter("serviceDate <=",
		// toDate).list();

		List<Service> ser = ofy().load().type(Service.class)
				.filter("companyId", service.getCompanyId())
				.filter("contractCount", service.getContractCount()).list();

		logger.log(Level.SEVERE, "SERVICE LIST SIZE" + ser.size());

		if (ser.size() != 0) {

			ArrayList<Service> list = new ArrayList<Service>();
			ArrayList<Service> newlist = new ArrayList<Service>();

			for (Service serv : ser) {
				serv.getServiceDate().setHours(0);
				serv.getServiceDate().setMinutes(0);
				serv.getServiceDate().setSeconds(0);

				Service serObj = new Service();
				serObj = serv;
				newlist.add(serObj);

			}

			Long dateInTime = getServDate(newlist, service.getCount());
			Date serDate = new Date(dateInTime);
			logger.log(Level.SEVERE, "SERVICE ID  AND SERVICE DATE \\\\ :: "
					+ service.getCount() + " " + serDate);

			for (Service serv : newlist) {
				logger.log(Level.SEVERE, "SERVICE ID  AND SERVICE DATE  :: "
						+ serv.getCount() + " " + serv.getServiceDate());

				if (serDate != null) {

					if (fmt.format(serDate).equals(
							fmt.format(serv.getServiceDate()))) {

						logger.log(Level.SEVERE, "MATCHED SERVICE DATE :: "
								+ serDate);

						list.add(serv);
					}
				}

			}

			serList.addAll(list);
		}

		System.out
				.println("Size of ser List ++++++++++++++++" + serList.size());
		logger.log(Level.SEVERE, "SERVICE LIST SIZE AFTER" + serList.size());
		/*****************************************************************************/

		if (service.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", service.getCompanyId())
					.filter("processName", "Service")
					.filter("configStatus", true).first().now();
			System.out.println("PROCESS CONFIG");
			if (processConfig != null) {
				System.out.println("PROCESS CONFIG NOT NULL");
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("JobCompletionCertificate")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						System.out.println("PROCESS CONFIG FLAG FALSE");
						antipestFlag = true;
					}
				}
			}
		}

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));

	}

	private Long getServDate(List<Service> ser, int count) {
		Long date = null;
		for (Service serv : ser) {
			if (count == serv.getCount()) {
				date = serv.getServiceDate().getTime();
				break;
			}
		}
		return date;
	}

	public void createPdf() {

		// myMethod();
		createHeadingInfo();
		createCompanyInfo();
		createCustomerInfo();
		// createTable();
		createTable1();
		createBlankTable();
		// createEmployeeInfo();
		createEmployeeInfo1();
		createRemarkInfo();
		createHardCodeInfo();
		createHardCodeInfo1();
		createHardCodeInfo2();
		createHardCodeInfo3();
		createHardCodeInfo4();
		createHardCodeInfo5();
		createHardCodeInfo6();
		createHardCodeInfo7();
		createHardCodeInfo8();
		createHardCodeInfo9();
		createHardCodeInfo10();
		createHardCodeInfo11();
	}

	// private void myMethod() {
	//
	// Paragraph mypara = new Paragraph("My Pdf......",font10);
	// try {
	// document.add(mypara);
	// } catch (DocumentException e) {
	// e.printStackTrace();
	// }
	// }

	private void createHeadingInfo() {

		Phrase title = new Phrase("JOB COMPLETION CERTIFICATE ", font10bold);
		PdfPCell titlecell = new PdfPCell(title);
		titlecell.setBorder(0);
		titlecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase title1 = null;
		if (service.getServiceDate() != null) {
			title1 = new Phrase("SERVICE DATE :           "
					+ fmt.format(service.getServiceDate()), font10bold);
		}
		PdfPCell title1cell = new PdfPCell(title1);
		title1cell.setBorder(0);
		title1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable titleTable = new PdfPTable(2);

		try {
			titleTable.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		titleTable.setWidthPercentage(100);

		titleTable.addCell(titlecell);
		titleTable.addCell(title1cell);

		PdfPCell cell = new PdfPCell(titleTable);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCompanyInfo() {

		// **********company info************
		PdfPTable comptable = new PdfPTable(2);

		try {
			comptable.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		comptable.setWidthPercentage(100f);

		Phrase company = null;
		if (comp.getBusinessUnitName() != null) {
			company = new Phrase("" + comp.getBusinessUnitName().toUpperCase(),
					font10bold);
		}
		PdfPCell companycell = new PdfPCell(company);
		companycell.setBorderWidthRight(0);
		companycell.setBorderWidthBottom(0);
		companycell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase number = null;
		if (service.getCount() != 0) {
			number = new Phrase("     " + service.getCount(), font10bold);
		}
		PdfPCell numcell = new PdfPCell(number);
		numcell.setBorderWidthLeft(0);
		numcell.setBorderWidthBottom(0);
		numcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		comptable.addCell(companycell);
		comptable.addCell(numcell);

		PdfPCell cell = new PdfPCell(comptable);
		cell.setBorder(0);

		// ************contact info***********
		PdfPTable contacttable = new PdfPTable(2);

		try {
			contacttable.setWidths(new float[] { 40, 60 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		contacttable.setWidthPercentage(100f);

		Phrase bl = new Phrase(" ", font9);
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorderWidthTop(0);
		blcell.setBorderWidthBottom(0);
		blcell.setBorderWidthRight(0);
		blcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// Phrase contact = null;
		// if (branch.getContact().get(0).getLandline() != -1) {
		// contact = new Phrase("Tel: "
		// + branch.getContact().get(0).getLandline() + "/" + ""
		// + branch.getContact().get(0).getLandline() + ", "
		// + "Telfax: " + branch.getContact().get(0).getCellNo2(),
		// font9);
		// }

		Phrase contact = null;
		if (comp.getLandline() != null) {
			contact = new Phrase("Tel: " + comp.getLandline() + "/" + ""
					+ comp.getLandline() + ", " + "Telfax: "
					+ comp.getCellNumber2(), font9);
		}
		PdfPCell contactcell = new PdfPCell(contact);
		contactcell.setBorderWidthTop(0);
		contactcell.setBorderWidthBottom(0);
		contactcell.setBorderWidthLeft(0);
		contactcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		contacttable.addCell(blcell);
		contacttable.addCell(contactcell);

		PdfPCell cell1 = new PdfPCell(contacttable);
		cell1.setBorder(0);

		// **********company address info************
		PdfPTable addresstable = new PdfPTable(2);
		addresstable.setWidthPercentage(100f);

		String compAdd1 = "";
		String compFullAdd1 = "";

		if (comp.getAddress() != null) {

			if (!comp.getAddress().getAddrLine2().equals("")) {
				if (!comp.getAddress().getLandmark().equals("")) {
					compAdd1 = comp.getAddress().getAddrLine1() + "\n"
							+ comp.getAddress().getAddrLine2() + "\n"
							+ comp.getAddress().getLandmark();
				} else {
					compAdd1 = comp.getAddress().getAddrLine1() + "\n"
							+ comp.getAddress().getAddrLine2();
				}
			} else {
				if (!comp.getAddress().getLandmark().equals("")) {
					compAdd1 = comp.getAddress().getAddrLine1() + "\n"
							+ comp.getAddress().getLandmark();
				} else {
					compAdd1 = comp.getAddress().getAddrLine1();
				}
			}

			if (!comp.getAddress().getLocality().equals("")) {
				compFullAdd1 = compAdd1 + comp.getAddress().getCountry() + ","
						+ comp.getAddress().getState() + ","
						+ comp.getAddress().getCity() + "\n"
						+ comp.getAddress().getLocality() + " "
						+ comp.getAddress().getPin();

			} else {
				compFullAdd1 = compAdd1 + comp.getAddress().getCountry() + ","
						+ comp.getAddress().getState() + ","
						+ comp.getAddress().getCity() + " "
						+ comp.getAddress().getPin();
			}
		}
		/***************************************************************************/
		// if (comp.getAddress() != null) {
		// System.out.println("INSIDE COMPANY ADDRESS..............");
		// System.out.println("Company id...." + service.getCompanyId());
		//
		// if (comp.getAddress().getAddrLine2() != null
		// || !comp.getAddress().getAddrLine2().equals("")) {
		// System.out
		// .println("INSIDE COMPANY ADDRESS LINE2++++++++++++++");
		// if (comp.getAddress().getLandmark() != null
		// || !comp.getAddress().getLandmark().equals("")) {
		// custAdd1 = comp.getAddress().getAddrLine1() + ","
		// + comp.getAddress().getAddrLine2() + ","
		// + comp.getAddress().getLandmark();
		// } else {
		// custAdd1 = comp.getAddress().getAddrLine1() + ","
		// + comp.getAddress().getAddrLine2();
		// }
		// } else {
		// if (comp.getAddress().getLandmark() != null
		// || !comp.getAddress().getLandmark().equals("")) {
		// custAdd1 = comp.getAddress().getAddrLine1() + ","
		// + comp.getAddress().getLandmark();
		// } else {
		// custAdd1 = comp.getAddress().getAddrLine1();
		// }
		// }
		//
		// if (comp.getAddress().getLocality() != null
		// || !comp.getAddress().getLocality().equals("")) {
		//
		// custFullAdd1 = custAdd1 + comp.getAddress().getCountry() + ","
		// + comp.getAddress().getState() + ","
		// + comp.getAddress().getCity() + " "
		// + comp.getAddress().getPin();
		// }
		//
		// }
		Phrase compAddInfo = new Phrase(compFullAdd1, font9);
		PdfPCell compAddInfoCell = new PdfPCell(compAddInfo);
		compAddInfoCell.setBorderWidthTop(0);
		// custAddInfoCell.setBorderWidthBottom(0);
		compAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		addresstable.addCell(compAddInfoCell);

		PdfPCell cell2 = new PdfPCell(addresstable);
		cell2.setBorder(0);

		/************************************************/
		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);
		parentTable.addCell(cell1);
		parentTable.addCell(cell2);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCustomerInfo() {

		// ***********customer info**********
		PdfPTable customerTable = new PdfPTable(2);

		try {
			customerTable.setWidths(new float[] { 30, 70 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		customerTable.setWidthPercentage(100f);

		Phrase code = new Phrase("Clients Code No. : ", font9bold);
		PdfPCell codeCell = new PdfPCell(code);
		codeCell.setBorder(0);
		codeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase no = null;
		if (cust.getRefrNumber1() != null) {
			no = new Phrase("" + cust.getRefrNumber1(), font9);
		} else {
			no = new Phrase(" ", font9);
		}
		PdfPCell noCell = new PdfPCell(no);
		noCell.setBorder(0);
		noCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase client = new Phrase("Client : ", font9bold);
		PdfPCell clientCell = new PdfPCell(client);
		clientCell.setBorder(0);
		clientCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase clientname = null;
		if (cust.isCompany() == true) {
			clientname = new Phrase("" + cust.getCompanyName(), font9);
		} else {
			clientname = new Phrase("" + cust.getFullname(), font9);
		}
		PdfPCell clientnameCell = new PdfPCell(clientname);
		clientnameCell.setBorder(0);
		clientnameCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// ***********customer address info**********
		Phrase premises = new Phrase("Premises to be treated : ", font9bold);
		PdfPCell premisesCell = new PdfPCell(premises);
		premisesCell.setBorder(0);
		premisesCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String custAdd1 = "";
		String custFullAdd1 = "";

		// if (customerbranch.getAddress() != null ||
		// !customerbranch.getAddress().equals("")) {
		// System.out.println("INSIDE CUSTOMER ADDRESS..............");
		//
		// if (customerbranch.getAddress().getAddrLine2() != null
		// || !customerbranch.getAddress().getAddrLine2().equals("")) {
		// if (customerbranch.getAddress().getLandmark() != null
		// || !customerbranch.getAddress().getLandmark()
		// .equals("")) {
		// custAdd1 = customerbranch.getAddress().getAddrLine1() + ","
		// + customerbranch.getAddress().getAddrLine2() + ","
		// + customerbranch.getAddress().getLandmark();
		// } else {
		// custAdd1 = customerbranch.getAddress().getAddrLine1() + ","
		// + customerbranch.getAddress().getAddrLine2();
		// }
		// } else {
		// if (customerbranch.getAddress().getLandmark() != null
		// || !customerbranch.getAddress().getLandmark()
		// .equals("")) {
		// custAdd1 = customerbranch.getAddress().getAddrLine1() + ","
		// + customerbranch.getAddress().getLandmark();
		// } else {
		// custAdd1 = customerbranch.getAddress().getAddrLine1();
		// }
		// }
		//
		// if (customerbranch.getAddress().getLocality() != null
		// || !customerbranch.getAddress().getLocality().equals("")) {
		//
		// custFullAdd1 = custAdd1
		// + customerbranch.getAddress().getCountry() + ","
		// + customerbranch.getAddress().getState() + ","
		// + customerbranch.getAddress().getCity() + " "
		// + customerbranch.getAddress().getPin();
		// }
		//
		// }

		if (cust.getAdress() != null || !cust.getAdress().equals("")) {
			System.out.println("INSIDE CUSTOMER ADDRESS..............");

			if (cust.getAdress().getAddrLine2() != null
					|| !cust.getAdress().getAddrLine2().equals("")) {
				if (cust.getAdress().getLandmark() != null
						|| !cust.getAdress().getLandmark().equals("")) {
					custAdd1 = cust.getAdress().getAddrLine1() + ","
							+ cust.getAdress().getAddrLine2() + ","
							+ cust.getAdress().getLandmark();
				} else {
					custAdd1 = cust.getAdress().getAddrLine1() + ","
							+ cust.getAdress().getAddrLine2();
				}
			} else {
				if (cust.getAdress().getLandmark() != null
						|| !cust.getAdress().getLandmark().equals("")) {
					custAdd1 = cust.getAdress().getAddrLine1() + ","
							+ cust.getAdress().getLandmark() + ",";
				} else {
					custAdd1 = cust.getAdress().getAddrLine1();
				}
			}

			if (cust.getAdress().getLocality() != null
					|| !cust.getAdress().getLocality().equals("")) {

				custFullAdd1 = custAdd1 + cust.getAdress().getCountry() + ","
						+ cust.getAdress().getState() + ","
						+ cust.getAdress().getCity() + " "
						+ cust.getAdress().getPin();
			}

		}
		Phrase custAddInfo = new Phrase(custFullAdd1, font9);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setBorder(0);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		customerTable.addCell(codeCell);
		customerTable.addCell(noCell);

		customerTable.addCell(clientCell);
		customerTable.addCell(clientnameCell);

		customerTable.addCell(premisesCell);
		customerTable.addCell(custAddInfoCell);

		PdfPCell cell = new PdfPCell(customerTable);
		cell.setBorder(0);

		// ***********poc info*************
		PdfPTable pocTable = new PdfPTable(3);

		try {
			pocTable.setWidths(colWidths1);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		pocTable.setWidthPercentage(100f);

		// // ***********poc table*****************
		// PdfPTable pTable = new PdfPTable(2);
		// pTable.setWidthPercentage(100f);
		//
		// Phrase poc = new Phrase("Contact Person : ", font9bold);
		// PdfPCell pocCell = new PdfPCell(poc);
		// pocCell.setBorder(0);
		// pocCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//
		// Phrase pocname = null;
		// if (cust.getFullname() != null) {
		// pocname = new Phrase(" " + cust.getFullname(), font9);
		// } else {
		// pocname = new Phrase(" ", font9);
		// }
		// PdfPCell pocnameCell = new PdfPCell(pocname);
		// pocnameCell.setBorder(0);
		// pocnameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//
		// pTable.addCell(pocCell);
		// pTable.addCell(pocnameCell);
		//
		// PdfPCell pCell = new PdfPCell(pTable);
		// pCell.setBorder(0);
		//
		//
		// // ***********time table*****************
		// PdfPTable tTable = new PdfPTable(2);
		// tTable.setWidthPercentage(100f);
		//
		// Phrase time = new Phrase("Time : ", font9);
		// PdfPCell tmCell = new PdfPCell(time);
		// tmCell.setBorder(0);
		// tmCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//
		// Phrase timeinfo = null;
		// if (!service.getServiceTime().equals("Flexible")) {
		// System.out.println("SERVICE TIME::::::::::::::"
		// + service.getServiceTime());
		//
		// timeinfo = new Phrase("" + service.getServiceTime(), font9);
		// } else {
		// timeinfo = new Phrase(" ", font9);
		// }
		// PdfPCell timecell = new PdfPCell(timeinfo);
		// timecell.setBorder(0);
		// timecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		//
		// tTable.addCell(tmCell);
		// tTable.addCell(timecell);
		//
		// PdfPCell tCell = new PdfPCell(tTable);
		// tCell.setBorder(0);
		//
		//
		// Phrase cellinfo = null;
		// if (service.getCellNumber() != null) {
		// cellinfo = new Phrase("Tel No. : " + service.getCellNumber(), font9);
		// } else {
		// cellinfo = new Phrase(" ", font9);
		// }
		// PdfPCell cellinfocell = new PdfPCell(cellinfo);
		// cellinfocell.setBorder(0);
		// cellinfocell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		//
		// pocTable.addCell(pCell);
		// pocTable.addCell(tCell);
		// pocTable.addCell(cellinfocell);
		//
		// PdfPCell cell1 = new PdfPCell(pocTable);
		// cell1.setBorder(0);

		PdfPTable custinfoTable = new PdfPTable(1);
		custinfoTable.setWidthPercentage(100f);
		custinfoTable.addCell(cell);
		// custinfoTable.addCell(cell1);

		PdfPCell parentCell = new PdfPCell(custinfoTable);
		parentCell.setBorderWidthBottom(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(parentCell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	/***********************************************************************/
	// private void createTable() {
	//
	// PdfPTable table = new PdfPTable(5);
	//
	// try {
	// table.setWidths(colWidths3);
	// } catch (DocumentException e) {
	// e.printStackTrace();
	// }
	// table.setWidthPercentage(100f);
	//
	// Phrase treatment = new Phrase("Treatments", font9bold);
	// Phrase premises = new Phrase("Premises", font9bold);
	// Phrase chemical = new Phrase("Chemical", font9bold);
	// Phrase antidote = new Phrase("Antidote", font9bold);
	// Phrase sign = new Phrase("Sign", font9bold);
	//
	// PdfPCell treatCell = new PdfPCell(treatment);
	// treatCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	// PdfPCell preCell = new PdfPCell(premises);
	// preCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	// PdfPCell chemCell = new PdfPCell(chemical);
	// chemCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	// PdfPCell antiCell = new PdfPCell(antidote);
	// antiCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	// PdfPCell signCell = new PdfPCell(sign);
	// signCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	// table.addCell(treatCell);
	// table.addCell(preCell);
	// table.addCell(chemCell);
	// table.addCell(antiCell);
	// table.addCell(signCell);
	//
	// PdfPCell cell = new PdfPCell(table);
	// cell.setBorder(0);
	//
	// PdfPTable parentTable = new PdfPTable(1);
	// parentTable.setWidthPercentage(100);
	// parentTable.addCell(cell);
	//
	// try {
	// document.add(parentTable);
	// } catch (DocumentException e) {
	// e.printStackTrace();
	// }
	//
	// }
	/**************************************************************/

	private void createTable1() {

		PdfPTable table = new PdfPTable(5);

		try {
			table.setWidths(colWidths3);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		table.setWidthPercentage(100f);

		// ***********contact person info************
		PdfPTable bl1Table = new PdfPTable(1);
		bl1Table.setWidthPercentage(100f);

		PdfPTable bl2Table = new PdfPTable(1);
		bl2Table.setWidthPercentage(100f);

		PdfPTable bl3Table = new PdfPTable(1);
		bl3Table.setWidthPercentage(100f);

		PdfPTable bl4Table = new PdfPTable(1);
		bl4Table.setWidthPercentage(100f);

		PdfPTable bl5Table = new PdfPTable(1);
		bl5Table.setWidthPercentage(100f);

		Phrase person = new Phrase("Contact Person ", font9bold);

		Phrase pocname = null;
		if (cust.getFullname() != null) {
			pocname = new Phrase(" " + cust.getFullname(), font9);
		} else {
			pocname = new Phrase(" ", font9);
		}

		// Phrase tm = new Phrase("Time : ", font9bold);

		Phrase timeinfo = null;
		if (!service.getServiceTime().equals("Flexible")) {
			System.out.println("SERVICE TIME::::::::::::::"
					+ service.getServiceTime());

			timeinfo = new Phrase("Time: " + service.getServiceTime(), font9);
		} else {
			timeinfo = new Phrase(" ", font9);
		}

		Phrase telno = new Phrase("Tel No. : ", font9bold);

		Phrase cellinfo = null;
		if (service.getCellNumber() != null) {
			cellinfo = new Phrase("" + service.getCellNumber(), font9);
		} else {
			cellinfo = new Phrase(" ", font9);
		}

		PdfPCell bl1Cell = new PdfPCell(person);
		bl1Cell.setBorder(0);
		bl1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPCell bl2Cell = new PdfPCell(pocname);
		bl2Cell.setBorder(0);
		bl2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPCell bl3Cell = new PdfPCell(timeinfo);
		bl3Cell.setBorder(0);
		bl3Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPCell bl4Cell = new PdfPCell(telno);
		bl4Cell.setBorder(0);
		bl4Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPCell bl5Cell = new PdfPCell(cellinfo);
		bl5Cell.setBorder(0);
		bl5Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		bl1Table.addCell(bl1Cell);
		bl2Table.addCell(bl2Cell);
		bl3Table.addCell(bl3Cell);
		bl4Table.addCell(bl4Cell);
		bl5Table.addCell(bl5Cell);

		PdfPCell cell1 = new PdfPCell(bl1Table);
		cell1.setBorder(0);

		PdfPCell cell2 = new PdfPCell(bl2Table);
		cell2.setBorder(0);

		PdfPCell cell3 = new PdfPCell(bl3Table);
		cell3.setBorder(0);

		PdfPCell cell4 = new PdfPCell(bl4Table);
		cell4.setBorder(0);

		PdfPCell cell5 = new PdfPCell(bl5Table);
		cell5.setBorder(0);

		table.addCell(cell1);
		table.addCell(cell2);
		table.addCell(cell3);
		table.addCell(cell4);
		table.addCell(cell5);

		PdfPCell cell = new PdfPCell(table);
		// cell.setBorder(0);
		cell.setBorderWidthTop(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		/**************************************************************************/
		// ***********poc table*****************
		// PdfPTable pTable = new PdfPTable(2);
		// pTable.setWidthPercentage(100f);
		//
		// // Phrase poc = new Phrase("Contact Person : ", font9bold);
		// // PdfPCell pocCell = new PdfPCell(poc);
		// // pocCell.setBorder(0);
		// // pocCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//
		// // Phrase pocname = null;
		// // if (cust.getFullname() != null) {
		// // pocname = new Phrase(" " + cust.getFullname(), font9);
		// // } else {
		// // pocname = new Phrase(" ", font9);
		// // }
		// // PdfPCell pocnameCell = new PdfPCell(pocname);
		// // pocnameCell.setBorder(0);
		// // pocnameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//
		// pTable.addCell(pocCell);
		// pTable.addCell(pocnameCell);
		//
		// PdfPCell pCell = new PdfPCell(pTable);
		// pCell.setBorder(0);
		//
		//
		// // ***********time table*****************
		// PdfPTable tTable = new PdfPTable(2);
		// tTable.setWidthPercentage(100f);
		//
		// Phrase time = new Phrase("Time : ", font9);
		// PdfPCell tmCell = new PdfPCell(time);
		// tmCell.setBorder(0);
		// tmCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//
		// // Phrase timeinfo = null;
		// // if (!service.getServiceTime().equals("Flexible")) {
		// // System.out.println("SERVICE TIME::::::::::::::"
		// // + service.getServiceTime());
		// //
		// // timeinfo = new Phrase("" + service.getServiceTime(), font9);
		// // } else {
		// // timeinfo = new Phrase(" ", font9);
		// // }
		// PdfPCell timecell = new PdfPCell(timeinfo);
		// timecell.setBorder(0);
		// timecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		//
		// tTable.addCell(tmCell);
		// tTable.addCell(timecell);
		//
		// PdfPCell tCell = new PdfPCell(tTable);
		// tCell.setBorder(0);
		//
		//
		// // Phrase cellinfo = null;
		// // if (service.getCellNumber() != null) {
		// // cellinfo = new Phrase("Tel No. : " + service.getCellNumber(),
		// font9);
		// // } else {
		// // cellinfo = new Phrase(" ", font9);
		// // }
		// PdfPCell cellinfocell = new PdfPCell(cellinfo);
		// cellinfocell.setBorder(0);
		// cellinfocell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		//
		// pocTable.addCell(pCell);
		// pocTable.addCell(tCell);
		// pocTable.addCell(cellinfocell);
		//
		// PdfPCell cell1 = new PdfPCell(pocTable);
		// cell1.setBorder(0);
		/********************************************************************************/

	}

	private void createBlankTable() {

		PdfPTable table = new PdfPTable(5);

		try {
			table.setWidths(colWidths3);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		table.setWidthPercentage(100f);

		// ***********for 1st blank table************
		PdfPTable bl1Table = new PdfPTable(1);
		bl1Table.setWidthPercentage(100f);

		PdfPTable bl2Table = new PdfPTable(1);
		bl2Table.setWidthPercentage(100f);

		PdfPTable bl3Table = new PdfPTable(1);
		bl3Table.setWidthPercentage(100f);

		PdfPTable bl4Table = new PdfPTable(1);
		bl4Table.setWidthPercentage(100f);

		PdfPTable bl5Table = new PdfPTable(1);
		bl5Table.setWidthPercentage(100f);

		Phrase bl1 = new Phrase("Treatments", font9bold);
		Phrase bl2 = new Phrase("Premises", font9bold);
		Phrase bl3 = new Phrase("Chemical", font9bold);
		Phrase bl4 = new Phrase("Antidote", font9bold);
		Phrase bl5 = new Phrase("Sign", font9bold);

		PdfPCell bl1Cell = new PdfPCell(bl1);
		bl1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell bl2Cell = new PdfPCell(bl2);
		bl2Cell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell bl3Cell = new PdfPCell(bl3);
		bl3Cell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell bl4Cell = new PdfPCell(bl4);
		bl4Cell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell bl5Cell = new PdfPCell(bl5);
		bl5Cell.setHorizontalAlignment(Element.ALIGN_CENTER);

		/**********************************************/
		// bl1Table.addCell(bl1Cell);
		// for (int i = 0; i < con.getItems().size(); i++) {
		//
		// chunk = new Phrase(con.getItems().get(i).getProductName() + "",
		// font8);
		// pdfprodname = new PdfPCell(chunk);
		// pdfprodname.setHorizontalAlignment(Element.ALIGN_CENTER);
		//
		// bl1Table.addCell(pdfprodname);
		// }

		bl1Table.addCell(bl1Cell);
		for (int i = 0; i < serList.size(); i++) {

			System.out.println("Contract count....."
					+ service.getContractCount());
			System.out.println("Service date....." + service.getServiceDate());
			System.out.println("Service id....." + service.getCount());

			chunk = new Phrase(serList.get(i).getProductName() + "", font8);
			pdfprodname = new PdfPCell(chunk);
			pdfprodname.setHorizontalAlignment(Element.ALIGN_CENTER);

			bl1Table.addCell(pdfprodname);
		}

		/****************************************/
		bl2Table.addCell(bl2Cell);
		for (int i = 0; i < con.getItems().size(); i++) {

			if (con.getItems().get(i).getPremisesDetails() != null) {
				chunk = new Phrase(con.getItems().get(i).getPremisesDetails()
						+ "", font8);
			} else {
				chunk = new Phrase("", font8);
			}
			pdfpremises = new PdfPCell(chunk);
			pdfpremises.setHorizontalAlignment(Element.ALIGN_CENTER);

			bl2Table.addCell(pdfpremises);
		}

		/**************************************/
		bl3Table.addCell(bl3Cell);
		for (int i = 0; i < this.prodList.size(); i++) {

			if (this.prodList.get(i).getName() != null) {
				chunk = new Phrase(this.prodList.get(i).getName() + "", font8);
			} else {
				chunk = new Phrase("", font8);
			}
			pdfchemical = new PdfPCell(chunk);
			pdfchemical.setHorizontalAlignment(Element.ALIGN_CENTER);

			bl3Table.addCell(pdfchemical);
		}

		/************************************/
		bl4Table.addCell(bl4Cell);
		chunk = new Phrase("As Under", font8);
		pdfantidote = new PdfPCell(chunk);
		pdfantidote.setHorizontalAlignment(Element.ALIGN_CENTER);
		bl4Table.addCell(pdfantidote);

		/************************************/
		bl5Table.addCell(bl5Cell);
		chunk = new Phrase(" ", font8);
		pdfsign = new PdfPCell(chunk);
		pdfsign.setHorizontalAlignment(Element.ALIGN_CENTER);
		bl5Table.addCell(pdfsign);

		PdfPCell cell1 = new PdfPCell(bl1Table);
		cell1.setBorder(0);

		PdfPCell cell2 = new PdfPCell(bl2Table);
		cell2.setBorder(0);

		PdfPCell cell3 = new PdfPCell(bl3Table);
		cell3.setBorder(0);

		PdfPCell cell4 = new PdfPCell(bl4Table);
		cell4.setBorder(0);

		PdfPCell cell5 = new PdfPCell(bl5Table);
		cell5.setBorder(0);

		table.addCell(cell1);
		table.addCell(cell2);
		table.addCell(cell3);
		table.addCell(cell4);
		table.addCell(cell5);

		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createEmployeeInfo1() {

		PdfPTable employeeTable = new PdfPTable(5);

		try {
			employeeTable.setWidths(colWidths2);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		employeeTable.setWidthPercentage(100f);

		// **************crew table********
		PdfPTable crewTable = new PdfPTable(1);
		crewTable.setWidthPercentage(100f);

		Phrase crew = new Phrase("Crew: ", font9boldul);
		PdfPCell crewCell = new PdfPCell(crew);
		crewCell.setBorderWidthBottom(0);
		crewCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase bl = new Phrase(" ", font9);
		PdfPCell blCell = new PdfPCell(bl);
		blCell.setBorderWidthTop(0);
		blCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		crewTable.addCell(crewCell);
		crewTable.addCell(blCell);

		PdfPCell cell = new PdfPCell(crewTable);
		cell.setBorder(0);

		// **********employee table info**************
		PdfPTable emp1Table = new PdfPTable(1);
		emp1Table.setWidthPercentage(100f);

		PdfPTable emp2Table = new PdfPTable(1);
		emp2Table.setWidthPercentage(100f);

		PdfPTable emp3Table = new PdfPTable(1);
		emp3Table.setWidthPercentage(100f);

		PdfPTable emp4Table = new PdfPTable(1);
		emp4Table.setWidthPercentage(100f);

		Phrase emp = null;

		for (int i = 0; i < this.technicians.size(); i++) {

			if (technicians.get(i).getFullName() != null) {
				emp = new Phrase((i + 1) + ") "
						+ technicians.get(i).getFullName() + "", font8);
			} else {
				emp = new Phrase(" ", font8);
			}

			pdftech = new PdfPCell(emp);
			pdftech.setHorizontalAlignment(Element.ALIGN_LEFT);

			if (i == 0) {
				emp1Table.addCell(pdftech);
			} else if (i == 1) {
				emp2Table.addCell(pdftech);
			} else if (i == 2) {
				emp3Table.addCell(pdftech);
			} else if (i == 3) {
				emp4Table.addCell(pdftech);
			}

		}
		PdfPCell celle1 = new PdfPCell(emp1Table);
		// celle1.setBorder(0);

		PdfPCell celle2 = new PdfPCell(emp2Table);
		// celle2.setBorder(0);

		PdfPCell celle3 = new PdfPCell(emp3Table);
		// celle3.setBorder(0);

		PdfPCell celle4 = new PdfPCell(emp4Table);
		// celle4.setBorder(0);

		// *****Emp table***********
		PdfPTable e1Table = new PdfPTable(1);
		e1Table.setWidthPercentage(100f);

		e1Table.addCell(celle1);
		e1Table.addCell(celle2);

		PdfPCell cell1 = new PdfPCell(e1Table);
		cell1.setBorder(0);

		PdfPTable e2Table = new PdfPTable(1);
		e2Table.setWidthPercentage(100f);

		e2Table.addCell(celle3);
		e2Table.addCell(celle4);

		PdfPCell cell2 = new PdfPCell(e2Table);
		cell2.setBorder(0);

		/********************************************************************************/

		// *********arrival time info***********
		PdfPTable arrtimeTable = new PdfPTable(1);
		arrtimeTable.setWidthPercentage(100f);

		Phrase arrtime = new Phrase("Time of Arrival: ", font9bold);
		PdfPCell arrtimeCell = new PdfPCell(arrtime);
		arrtimeCell.setBorderWidthBottom(0);
		arrtimeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase bl1 = new Phrase(" ", font9);
		PdfPCell bl1Cell = new PdfPCell(bl1);
		bl1Cell.setBorderWidthTop(0);
		bl1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		arrtimeTable.addCell(arrtimeCell);
		arrtimeTable.addCell(bl1Cell);

		PdfPCell cell3 = new PdfPCell(arrtimeTable);
		cell3.setBorder(0);

		// *********departure time info***********
		PdfPTable deptimeTable = new PdfPTable(1);
		deptimeTable.setWidthPercentage(100f);

		Phrase deptime = new Phrase("Time of Departure: ", font9bold);
		PdfPCell deptimeCell = new PdfPCell(deptime);
		deptimeCell.setBorderWidthBottom(0);
		deptimeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase bl3 = new Phrase(" ", font9);
		PdfPCell bl3Cell = new PdfPCell(bl3);
		bl3Cell.setBorderWidthTop(0);
		bl3Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		deptimeTable.addCell(deptimeCell);
		deptimeTable.addCell(bl3Cell);

		PdfPCell cell4 = new PdfPCell(deptimeTable);
		cell4.setBorder(0);

		employeeTable.addCell(cell);
		employeeTable.addCell(cell1);
		employeeTable.addCell(cell2);
		employeeTable.addCell(cell3);
		employeeTable.addCell(cell4);

		PdfPCell employeecell = new PdfPCell(employeeTable);
		employeecell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(employeecell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	// private void createEmployeeInfo() {
	//
	// PdfPTable employeeTable = new PdfPTable(5);
	//
	// try {
	// employeeTable.setWidths(colWidths2);
	// } catch (DocumentException e) {
	// e.printStackTrace();
	// }
	// employeeTable.setWidthPercentage(100f);
	//
	// // **************crew table********
	// PdfPTable crewTable = new PdfPTable(1);
	// crewTable.setWidthPercentage(100f);
	//
	// Phrase crew = new Phrase("Crew: ", font9boldul);
	// PdfPCell crewCell = new PdfPCell(crew);
	// crewCell.setBorderWidthBottom(0);
	// crewCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	// Phrase bl = new Phrase(" ", font9);
	// PdfPCell blCell = new PdfPCell(bl);
	// blCell.setBorderWidthTop(0);
	// blCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	// crewTable.addCell(crewCell);
	// crewTable.addCell(blCell);
	//
	// PdfPCell cell = new PdfPCell(crewTable);
	// cell.setBorder(0);
	//
	// // *************emp1 info*************
	// PdfPTable emp1Table = new PdfPTable(1);
	// emp1Table.setWidthPercentage(100f);
	//
	// // Phrase emp = null;
	// // if (technicians.get(0).getFullName() != null) {
	// System.out.println("INSIDE SERVICE PROJECT....."+technicians.get(0).getFullName());
	// // emp = new Phrase("1) " + technicians.get(0).getFullName(), font9);
	// // } else {
	// // emp = new Phrase("1) ", font9);
	// // }
	// // PdfPCell empCell = new PdfPCell(emp);
	// // empCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	// Phrase emp1 = null;
	// // if (serproject.getEmp() != null) {
	// // emp1 = new Phrase("2) " + serproject.getEmp(), font9);
	// // } else {
	// emp1 = new Phrase("2) ", font9);
	// // }
	// PdfPCell emp1Cell = new PdfPCell(emp1);
	// emp1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	// // emp1Table.addCell(empCell);
	// emp1Table.addCell(emp1Cell);
	//
	// PdfPCell cell1 = new PdfPCell(emp1Table);
	// cell1.setBorder(0);
	//
	// // *************emp2 info*************
	// PdfPTable emp2Table = new PdfPTable(1);
	// emp2Table.setWidthPercentage(100f);
	//
	// Phrase emp2 = null;
	// // if (serproject.getEmp() != null) {
	// // emp2 = new Phrase("3) " + serproject.getEmp(), font9);
	// // } else {
	// emp2 = new Phrase("3) ", font9);
	// // }
	// PdfPCell emp2Cell = new PdfPCell(emp2);
	// emp2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	// Phrase emp3 = null;
	// // if (serproject.getEmp() != null) {
	// // emp3 = new Phrase("4) " + serproject.getEmp(), font9);
	// // } else {
	// emp3 = new Phrase("4) ", font9);
	// // }
	// PdfPCell emp3Cell = new PdfPCell(emp3);
	// emp3Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	// emp2Table.addCell(emp2Cell);
	// emp2Table.addCell(emp3Cell);
	//
	// PdfPCell cell2 = new PdfPCell(emp2Table);
	// cell2.setBorder(0);
	//
	// // *********arrival time info***********
	// PdfPTable arrtimeTable = new PdfPTable(1);
	// arrtimeTable.setWidthPercentage(100f);
	//
	// Phrase arrtime = new Phrase("Time of Arrival: ", font9bold);
	// PdfPCell arrtimeCell = new PdfPCell(arrtime);
	// arrtimeCell.setBorderWidthBottom(0);
	// arrtimeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	// Phrase bl1 = new Phrase(" ", font9);
	// PdfPCell bl1Cell = new PdfPCell(bl1);
	// bl1Cell.setBorderWidthTop(0);
	// bl1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	// arrtimeTable.addCell(arrtimeCell);
	// arrtimeTable.addCell(bl1Cell);
	//
	// PdfPCell cell3 = new PdfPCell(arrtimeTable);
	// cell3.setBorder(0);
	//
	// // *********departure time info***********
	// PdfPTable deptimeTable = new PdfPTable(1);
	// deptimeTable.setWidthPercentage(100f);
	//
	// Phrase deptime = new Phrase("Time of Departure: ", font9bold);
	// PdfPCell deptimeCell = new PdfPCell(deptime);
	// deptimeCell.setBorderWidthBottom(0);
	// deptimeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	// Phrase bl3 = new Phrase(" ", font9);
	// PdfPCell bl3Cell = new PdfPCell(bl3);
	// bl3Cell.setBorderWidthTop(0);
	// bl3Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	// deptimeTable.addCell(deptimeCell);
	// deptimeTable.addCell(bl3Cell);
	//
	// PdfPCell cell4 = new PdfPCell(deptimeTable);
	// cell4.setBorder(0);
	//
	// employeeTable.addCell(cell);
	// employeeTable.addCell(cell1);
	// employeeTable.addCell(cell2);
	// employeeTable.addCell(cell3);
	// employeeTable.addCell(cell4);
	//
	// PdfPCell employeecell = new PdfPCell(employeeTable);
	// employeecell.setBorder(0);
	//
	// PdfPTable parentTable = new PdfPTable(1);
	// parentTable.setWidthPercentage(100);
	// parentTable.addCell(employeecell);
	//
	// try {
	// document.add(parentTable);
	// } catch (DocumentException e) {
	// e.printStackTrace();
	// }
	//
	// }

	private void createRemarkInfo() {

		// *******for table 1*****************
		String str = "";
		str = "This is to certify that the treatment has been rendered to our satisfaction. Remark if any is mentioned below.";

		Phrase strp = new Phrase(str, font9);
		PdfPCell strpcell = new PdfPCell(strp);
		strpcell.setBorderWidthBottom(0);
		strpcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable strpTable = new PdfPTable(1);
		strpTable.setWidthPercentage(100f);
		strpTable.addCell(strpcell);

		PdfPCell cell = new PdfPCell(strpTable);
		cell.setBorder(0);

		// *******for table 2*****************
		PdfPTable tbl1 = new PdfPTable(1);
		tbl1.setWidthPercentage(100f);

		Phrase bl = new Phrase(" ", font9);
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorderWidthTop(0);
		blcell.setBorderWidthBottom(0);
		blcell.setBorderWidthRight(0);
		blcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		tbl1.addCell(blcell);
		tbl1.addCell(blcell);

		PdfPCell tbl1cell = new PdfPCell(tbl1);
		tbl1cell.setBorder(0);

		/*************************************/
		PdfPTable tbl2 = new PdfPTable(1);
		tbl2.setWidthPercentage(100f);

		Phrase client = new Phrase("    Client Signature ", font9);
		PdfPCell clientcell = new PdfPCell(client);
		clientcell.setBorder(0);
		clientcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase mr = new Phrase("   Mr./Mrs./Miss.", font9);
		PdfPCell mrcell = new PdfPCell(mr);
		mrcell.setBorder(0);
		mrcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		tbl2.addCell(clientcell);
		tbl2.addCell(mrcell);

		PdfPCell tbl2cell = new PdfPCell(tbl2);
		tbl2cell.setBorder(0);

		/**************************************/
		PdfPTable tbl3 = new PdfPTable(1);
		tbl3.setWidthPercentage(100f);

		Phrase name = null;
		if (cust.getFullname() != null) {
			name = new Phrase("" + cust.getFullname(), font9ul);
		}
		PdfPCell namecell = new PdfPCell(name);
		namecell.setBorderWidthTop(0);
		namecell.setBorderWidthBottom(0);
		namecell.setBorderWidthLeft(0);
		namecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase blank = new Phrase(" ", font9);
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorderWidthTop(0);
		blankcell.setBorderWidthBottom(0);
		blankcell.setBorderWidthLeft(0);
		blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		tbl3.addCell(blankcell);
		tbl3.addCell(namecell);

		PdfPCell tbl3cell = new PdfPCell(tbl3);
		tbl3cell.setBorder(0);

		PdfPTable blTable = new PdfPTable(3);

		try {
			blTable.setWidths(colWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		blTable.setWidthPercentage(100f);

		blTable.addCell(tbl1cell);
		blTable.addCell(tbl2cell);
		blTable.addCell(tbl3cell);

		PdfPCell cell1 = new PdfPCell(blTable);
		cell1.setBorder(0);

		// *******for table 3*****************
		Phrase sign = new Phrase("Signature by Technician ", font9);
		PdfPCell signcell = new PdfPCell(sign);
		signcell.setBorderWidthRight(0);
		signcell.setBorderWidthTop(0);
		signcell.setBorderWidthBottom(0);
		signcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String str3 = "";
		str3 = "Please Mention Time of Arrival & Departure of Crew .";

		Phrase strp3 = new Phrase(str3, font9);
		PdfPCell strp3cell = new PdfPCell(strp3);
		strp3cell.setBorderWidthLeft(0);
		strp3cell.setBorderWidthTop(0);
		strp3cell.setBorderWidthBottom(0);
		strp3cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable strp3Table = new PdfPTable(2);

		try {
			strp3Table.setWidths(new float[] { 55, 45 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		strp3Table.setWidthPercentage(100f);

		strp3Table.addCell(signcell);
		strp3Table.addCell(strp3cell);

		PdfPCell cell2 = new PdfPCell(strp3Table);
		cell2.setBorder(0);

		// *******for table 4*****************
		Phrase bl1 = new Phrase(" ", font9);
		PdfPCell bl1cell = new PdfPCell(bl1);
		bl1cell.setBorderWidthTop(0);
		bl1cell.setBorderWidthBottom(0);

		PdfPTable bl1Table = new PdfPTable(1);
		bl1Table.setWidthPercentage(100f);
		bl1Table.addCell(bl1cell);

		PdfPCell cell3 = new PdfPCell(bl1Table);
		cell3.setBorder(0);

		// *******for table 5*****************
		Phrase remark = new Phrase("Remarks :  ", font9bold);
		PdfPCell remcell = new PdfPCell(remark);
		remcell.setBorderWidthTop(0);
		remcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable remTable = new PdfPTable(1);
		remTable.setWidthPercentage(100f);
		remTable.addCell(remcell);

		PdfPCell cell4 = new PdfPCell(remTable);
		cell4.setBorder(0);

		// *******for table 6*****************
		Phrase bl2 = new Phrase(" ", font9);
		PdfPCell bl2cell = new PdfPCell(bl2);

		PdfPTable bl2Table = new PdfPTable(1);
		bl2Table.setWidthPercentage(100f);
		bl2Table.addCell(bl2cell);

		PdfPCell cell5 = new PdfPCell(bl2Table);
		cell5.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);
		parentTable.addCell(cell1);
		parentTable.addCell(cell2);
		parentTable.addCell(cell3);
		parentTable.addCell(cell4);
		parentTable.addCell(cell5);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createHardCodeInfo() {

		Phrase aid = new Phrase("FIRST AID", font9boldul);
		PdfPCell aidcell = new PdfPCell(aid);
		aidcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable aidTable = new PdfPTable(1);
		aidTable.setWidthPercentage(100);
		aidTable.addCell(aidcell);

		try {
			document.add(aidTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createHardCodeInfo1() {

		// String str,str1;

		String str = "";
		str = "A conscious victim should drink a small amount of water to dilute the pesticide. Induce vomiting "
				+ "only if doctor advices";

		// String str1 = "";
		// str1 = "only if doctor advices";

		Phrase line = new Phrase(str, font8);
		PdfPCell linecell = new PdfPCell(line);
		linecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable lineTable = new PdfPTable(1);
		lineTable.setWidthPercentage(100);
		lineTable.addCell(linecell);

		try {
			document.add(lineTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createHardCodeInfo2() {

		// String str = "";
		// str = "Poison on skin. ";

		String str = "";
		str = ("Poison on skin. Drench skin with water for at least 15 minutes. Remove the contaminated clothing. Wash skin and hair thoroughly "
				+ "with soap and water. Later, discard contaminated clothing thoroughly wash it separately from other laundry");

		Phrase line = new Phrase(str, font8);
		PdfPCell linecell = new PdfPCell(line);
		linecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable lineTable = new PdfPTable(1);
		lineTable.setWidthPercentage(100);
		lineTable.addCell(linecell);

		try {
			document.add(lineTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createHardCodeInfo3() {

		// String str = "";
		// str = ("Poison in eye ") + font8bold;

		String str = "";
		str = "Poison in eye. Hold eyelid open and wash eyes quickly and gently with clean and cool running water from the tap "
				+ "or a hos for a 15 minutes or more. Use only water; do not use eye drops, chemicals or drugs in the eye. "
				+ "Eye membrane absorbs pesticides fatser than any other external part of the body, and  "
				+ "eye damage can occure in few minutes with some types of pesticides. If a poisoning "
				+ "has occured, call for help, and be ready to read information from the pesticide label."
				+ "\n" + " ";

		Phrase line = new Phrase(str, font8);
		PdfPCell linecell = new PdfPCell(line);
		linecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable lineTable = new PdfPTable(1);
		lineTable.setWidthPercentage(100);
		lineTable.addCell(linecell);

		try {
			document.add(lineTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createHardCodeInfo4() {

		// String str = "";
		// str = ("Inhaled poison. ") + font8bold;

		String str = "";
		str = "Inhaled poison. If the victim is outside, move or carry the victim away form the area where pesticide were recently applied."
				+ "Move the victim to fresh air immediately. If victim needs your immediate help use eqipments "
				+ "like respirator before helping the victim and before entering the area. Loosen the victim's tight..."
				+ "If the victims skin is blue or the victim has stopped breathing, give artificial respiration (if you know how). Open doors "
				+ "and windows so no one else will be poisoned by fumes "
				+ "\n" + " ";

		Phrase line = new Phrase(str, font8);
		PdfPCell linecell = new PdfPCell(line);
		linecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable lineTable = new PdfPTable(1);
		lineTable.setWidthPercentage(100);
		lineTable.addCell(linecell);

		try {
			document.add(lineTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createHardCodeInfo5() {

		String str = "";
		str = "CHEMICAL AND ANTIDODE";

		Phrase line = new Phrase(str, font9boldul);
		PdfPCell linecell = new PdfPCell(line);
		linecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable lineTable = new PdfPTable(1);
		lineTable.setWidthPercentage(100);
		lineTable.addCell(linecell);

		try {
			document.add(lineTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createHardCodeInfo6() {

		String str = "";
		str = "Chlorpyriphos Diazinon " + "\n" + "Dichlorvos Fenitrothian "
				+ "\n" + "Fenthion Malathion " + "\n" + "Pirimiphos Methyle "
				+ "\n" + "Temephos ";

		String str1 = "";
		str1 = "*Inject atropine sulphate intravenously in a dose of 2-4 mg. For an adult (0.04 to 0.08 mg/kg body weight for children) "
				+ "every 5-10 minutes until signs of atropinisation occure(e.g) Dry mouth and usually dialated pupils. "
				+ "Maintain atropinisation for at least 24-48 hours and carefully observe the patient as further atropinisation stopped."
				+ "It may be necessary to recommence treatment if signs of poisoning return. *Convulsions and anxiety "
				+ "can be treatd with 5-10 mg. of diazepam injected intramuscularly. * While keeping the patient fully atropinised,"
				+ "administer also an oxime, if available, chlorinesterase reactivator e.g 2-PAM 1000-2000 mg IM or IV for adults "
				+ "(25 mg/kg body weight for children), or Toxogonin (Merck) 250 mg for adults (4-8 mg/kg body weight "
				+ "for children). Repeat if necessary after 1-2 hours. *Morphine, phenothiazines, succinylchloride, xanthenederivative,"
				+ "epinephrine and barbiturates are contraindicated  "
				+ "\n"
				+ " ";

		Phrase line = new Phrase(str, font1);
		PdfPCell linecell = new PdfPCell(line);
		linecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase line1 = new Phrase(str1, font1);
		PdfPCell line1cell = new PdfPCell(line1);
		line1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable lineTable = new PdfPTable(2);

		try {
			lineTable.setWidths(new float[] { 15, 85 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		lineTable.setWidthPercentage(100);

		lineTable.addCell(linecell);
		lineTable.addCell(line1cell);

		try {
			document.add(lineTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createHardCodeInfo7() {

		String str = "";
		str = "Carbaryl Carbofuran, Propoxure ";

		String str1 = "";
		str1 = "*Atropine therapy as indicated for Organophosphorous compounds. *Oximes such as 2-PAM, P25, Toxogonin should not been administered. "
				+ "*Morphin phenothiazines, succinylchloride, xanthenederivative, epinephrine, and barbiturates are also contraindicated "
				+ "*Convulsions can be treated with diazepam (vallium, Roche)";

		Phrase line = new Phrase(str, font1);
		PdfPCell linecell = new PdfPCell(line);
		linecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase line1 = new Phrase(str1, font1);
		PdfPCell line1cell = new PdfPCell(line1);
		line1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable lineTable = new PdfPTable(2);

		try {
			lineTable.setWidths(new float[] { 15, 85 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		lineTable.setWidthPercentage(100);

		lineTable.addCell(linecell);
		lineTable.addCell(line1cell);

		try {
			document.add(lineTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createHardCodeInfo8() {

		String str = "";
		str = "Cypermethrin," + "\n" + "Deltamethrin," + "\n" + "Fenvalerate"
				+ "\n" + "Permethrin" + "\n" + "Lambdacyhalothrin";

		String str1 = "";
		str1 = "*In case of severe skin exposure in handeling or application, typical sensations of exposed skin especially of "
				+ "face may appear which can be described as tingling, burning or numbness. These sensations will wear off in course "
				+ "of few hours. *The treatment is symptomatic. *In case larger amounts have been ingested, perform "
				+ "gastric lavage. *Administration of activated charcoal followed by saline Carthartic with sodium"
				+ "sulphate solution. *Control seizzers with injectable diazepam or barbiturates. "
				+ "\n" + " ";

		Phrase line = new Phrase(str, font1);
		PdfPCell linecell = new PdfPCell(line);
		linecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase line1 = new Phrase(str1, font1);
		PdfPCell line1cell = new PdfPCell(line1);
		line1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable lineTable = new PdfPTable(2);

		try {
			lineTable.setWidths(new float[] { 15, 85 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		lineTable.setWidthPercentage(100);

		lineTable.addCell(linecell);
		lineTable.addCell(line1cell);

		try {
			document.add(lineTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createHardCodeInfo9() {

		String str = "";
		str = "Zinc Phosphoids ";

		String str1 = "";
		str1 = "Gastric lavage, morphin for the relief of abdominal pain, administration of 100% oxygen for "
				+ "pulmonary endema, anticonvulsant therapy, high doses of corticosteroids "
				+ "and blood transusions for the treatment of shock and hemorrhage.";

		Phrase line = new Phrase(str, font1);
		PdfPCell linecell = new PdfPCell(line);
		linecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase line1 = new Phrase(str1, font1);
		PdfPCell line1cell = new PdfPCell(line1);
		line1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable lineTable = new PdfPTable(2);

		try {
			lineTable.setWidths(new float[] { 15, 85 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		lineTable.setWidthPercentage(100);

		lineTable.addCell(linecell);
		lineTable.addCell(line1cell);

		try {
			document.add(lineTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createHardCodeInfo10() {

		String str = "";
		str = "Bromadiolone, Warfarin ";

		String str1 = "";
		str1 = "*Remove ingested product by giving orally activated charcoal and saline cathartic. *Repeated prothrombin"
				+ "level determinations are important. *To restore blood clotting, give vitamin K, Phytonadion 15-25 mg to adults "
				+ "and 5-10 mg to children orally in mild cases. *In several cases, give Agnamephiton 5-10 mg to adults,"
				+ "1-5 mg to children, I.M or I.V " + "\n" + " ";

		Phrase line = new Phrase(str, font1);
		PdfPCell linecell = new PdfPCell(line);
		linecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase line1 = new Phrase(str1, font1);
		PdfPCell line1cell = new PdfPCell(line1);
		line1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable lineTable = new PdfPTable(2);

		try {
			lineTable.setWidths(new float[] { 15, 85 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		lineTable.setWidthPercentage(100);

		lineTable.addCell(linecell);
		lineTable.addCell(line1cell);

		try {
			document.add(lineTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createHardCodeInfo11() {

		String str = "";
		str = "Fipronil ";

		String str1 = "";
		str1 = "*Where ingestion is suspected rines mouth and give up to 200 ml water (5 ml/kg recommended) for dilution "
				+ "where patient is able to swallow, has a strong gag reflex and does not drool "
				+ "\n" + " ";

		Phrase line = new Phrase(str, font1);
		PdfPCell linecell = new PdfPCell(line);
		linecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase line1 = new Phrase(str1, font1);
		PdfPCell line1cell = new PdfPCell(line1);
		line1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable lineTable = new PdfPTable(2);

		try {
			lineTable.setWidths(new float[] { 15, 85 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		lineTable.setWidthPercentage(100);

		lineTable.addCell(linecell);
		lineTable.addCell(line1cell);

		try {
			document.add(lineTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
}
