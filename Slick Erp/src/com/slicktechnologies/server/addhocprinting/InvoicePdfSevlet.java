package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;


public class InvoicePdfSevlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1038955286526579198L;


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {
			
			String stringid = req.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			
			  String status=req.getParameter("preprint");
			   System.out.println("preprint status "+status);
			
//			Invoice invoiceentity = ofy().load().type(Invoice.class).id(count).now();
			
			
		InvoicePdf invpdf = new InvoicePdf();
			
		invpdf.document = new Document();
			Document document = invpdf.document;
			PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
																		
//			if(ins.getStatus().equals(Inspection.CANCELLED)){
//				writer.setPageEvent(new PdfCancelWatermark());
//			}else															
//			if(!insp.getStatus().equals(Inspection.APPROVED)){	
//				 writer.setPageEvent(new PdfWatermark());
//			} 
																		
			
			 
			 document.open();
			 invpdf.setinvoicepdf(count);
			 invpdf.createPdf(status);
			document.close();
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
	
	
}
