package com.slicktechnologies.server.addhocprinting;


import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceCharges;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

import static com.googlecode.objectify.ObjectifyService.ofy;

	
	public class SalesInvoicePdf {
		
		Invoice invoiceentity;
		ArrayList<Service> service;
		List<SalesOrderProductLineItem> salesProd;
		List<InvoiceCharges> billingTaxesLis = new ArrayList<InvoiceCharges>();
		List<InvoiceCharges> billingChargesLis= new ArrayList<InvoiceCharges>();
		
		
//		List<InvoiceCharges> billingTaxesLis11=new ArrayList<InvoiceCharges>();
//		List<InvoiceCharges> billingChargesLis11=new ArrayList<InvoiceCharges>();
		
		List<BillingDocumentDetails> billingDoc;
		List<PaymentTerms> payTermsLis;
		List<ArticleType> articletype;
		PurchaseOrder purchaseOrder=null;
		Vendor vendor;
		Customer cust;
		SalesOrder salesOrderEntity=null;
		Company comp;
		public  Document document;

//		BillingDocument billEntity;
		ProcessConfiguration processConfig;
		boolean CompanyNameLogoflag=false;
		

		String invoiceOrderType="";
		private  Font font16boldul,font12bold,font8bold,font8,font12boldul,font10boldul,font12,font16bold,font10,font10bold,font14bold,font9bold;
		private SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");
		 
		//**************rohan ****************************************
		PdfPTable chargetaxtableToNewPage = new PdfPTable(2);
		PdfPTable netPayableTable = new PdfPTable(2);
		
		
		List<ProductOtherCharges> purchaseOrderTaxesLis;
		List<ProductOtherCharges> purchaseOrderChargesLis;
		
		List<DeliveryNote> deliveryNote =null;
		
		//************changes here **********************************
		
		List<ProductOtherCharges> salesOrderTaxesLis;
		List<ProductOtherCharges> salesOrderChargesLis;
		List<PaymentTerms> payTerms= new ArrayList<PaymentTerms>();
		
		double total=0;
		Phrase chunk;
		int eva=100;
		int firstBreakPoint = 14;
		int BreakPoint = 5;
		int count=0;
		float blankLines;
		int flag=0;
		int flag1=0;
		float[] columnWidths = {0.5f, 3.5f, 0.5f, 0.5f,0.7f,0.5f,0.9f,1.2f};
		float[] columnWidths2 = {1.8f,0.5f,2.5f};
		String invComment="";
		Logger logger=Logger.getLogger("Salesinvooice");
		
		
		/***
		 * Date : 05-03-2017 By Anil
		 */
		
		ServicePo servicePo;


		DecimalFormat df=new DecimalFormat("0.00");

		final static String disclaimerText="I/We hereby certify that my/our registration certificate"
				+ " Under the Maharashtra Value Added Tax Act 2002, is in force on the date on which the sale "
				+ "of the goods specified in this Invoice is made by me/us and that the transaction of sales "
				+ "covered by this Invoice has been effected by me/us and it shall be accounted for in the turnover "
				+ "of sales while filling of return and the due tax, if any, payable on the sale has been paid or shall be paid.";
		
		public 	SalesInvoicePdf()
		{
			font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD| Font.UNDERLINE);
			font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
			new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
			font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
			font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
			font9bold = new Font(Font.FontFamily.HELVETICA,9,Font.BOLD);
			font8= new Font(Font.FontFamily.HELVETICA,8);
			font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
			font12=new Font(Font.FontFamily.HELVETICA,12);
			font10=new Font(Font.FontFamily.HELVETICA,10);
			font10bold = new Font(Font.FontFamily.HELVETICA  , 10, Font.BOLD);
			font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
			font10boldul= new Font(Font.FontFamily.HELVETICA,10,Font.BOLD|Font.UNDERLINE);
		}

		public void setInvoice(long count)
		{
			//Load Invoice
				invoiceentity=ofy().load().type(Invoice.class).id(count).now();
				
				billingDoc=invoiceentity.getArrayBillingDocument();
				invoiceOrderType=invoiceentity.getTypeOfOrder().trim();
				
			//Load Customer
			if(invoiceentity.getCompanyId()!=null&&invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
				cust=ofy().load().type(Customer.class).filter("count",invoiceentity.getPersonInfo().getCount()).filter("companyId", invoiceentity.getCompanyId()).first().now();
			}
			
			if(invoiceentity.getCompanyId()!=null
					&&(invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)
					||invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICEPO))){
				vendor=ofy().load().type(Vendor.class).filter("count",invoiceentity.getPersonInfo().getCount()).filter("companyId", invoiceentity.getCompanyId()).first().now();
			}
			
			
				if(invoiceentity.getCompanyId()!=null){
					processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", invoiceentity.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
					if(processConfig!=null){
						for(int k=0;k<processConfig.getProcessList().size();k++)
						{
							if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PrintCompanyNameAsLogo")&&processConfig.getProcessList().get(k).isStatus()==true)
							{
								CompanyNameLogoflag=true;
							}
						}
					}
				}
				
			//Load Company
			if(invoiceentity.getCompanyId()==null)
			   comp=ofy().load().type(Company.class).first().now();
			else
			   comp=ofy().load().type(Company.class).filter("companyId",invoiceentity.getCompanyId()).first().now();
			
			//Load Sales Order
			if(invoiceentity.getCompanyId()==null&&invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES))
				salesOrderEntity=ofy().load().type(SalesOrder.class).filter("count", invoiceentity.getContractCount()).first().now();
			
			
			if(invoiceentity.getCompanyId()!=null&&invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
				salesOrderEntity=ofy().load().type(SalesOrder.class).filter("count", invoiceentity.getContractCount()).filter("companyId",invoiceentity.getCompanyId()).first().now();
			
				//************rohan added this  for getting delivery note id 
				System.out.println("RRRRRRRRRRRRRRRRRRRRRRRRRRR");
				System.out.println("Sakesod=order id ======++++++++++++++="+salesOrderEntity.getCount());
				deliveryNote= ofy().load().type(DeliveryNote.class).filter("salesOrderCount", salesOrderEntity.getCount()).list();
			
			}
			
			
			//Load PO
			
			if(invoiceentity.getCompanyId()==null&&invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE))
				purchaseOrder=ofy().load().type(PurchaseOrder.class).filter("count", invoiceentity.getContractCount()).first().now();
			if(invoiceentity.getCompanyId()!=null&&invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
				purchaseOrder=ofy().load().type(PurchaseOrder.class).filter("count", invoiceentity.getContractCount()).filter("companyId",invoiceentity.getCompanyId()).first().now();
			}
			
			/**
			 * Date 05-03-2017 By Anil
			 */
			//Load Service PO
			
			if(invoiceentity.getCompanyId()==null&&invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICEPO))
				servicePo=ofy().load().type(ServicePo.class).filter("count", invoiceentity.getContractCount()).first().now();
			if(invoiceentity.getCompanyId()!=null&&invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICEPO)){
				servicePo=ofy().load().type(ServicePo.class).filter("count", invoiceentity.getContractCount()).filter("companyId",invoiceentity.getCompanyId()).first().now();
			}
			
//			if(invoiceentity.getCompanyId()!=null)
//			{
//				billEntity=ofy().load().type(BillingDocument.class).filter("companyId",invoiceentity.getCompanyId()).filter("contractCount", invoiceentity.getContractCount()).filter("invoiceCount", invoiceentity.getCount()).filter("typeOfOrder", invoiceentity.getTypeOfOrder().trim()).first().now();
//			}
//			else
//			{	
//				billEntity=ofy().load().type(BillingDocument.class).filter("contractCount", invoiceentity.getContractCount()).filter("invoiceCount", invoiceentity.getCount()).filter("typeOfOrder", invoiceentity.getTypeOfOrder().trim()).first().now();	
//			}
			
			
			
			logger.log(Level.SEVERE,"Size Of Products"+invoiceentity.getSalesOrderProducts());
			 salesProd=invoiceentity.getSalesOrderProducts();
			 
		
			 
			 //**************rohan changes here *********************	 
			
			 System.out.println("invoiceentity.getArrayBillingDocument().size()"+invoiceentity.getArrayBillingDocument().size());

			 if(invoiceentity.getArrayBillingDocument().size()!=0){
//				 BillingDocument billDocumentEntity =null;
				 for(int i=0;i<invoiceentity.getArrayBillingDocument().size();i++)
				 {
					 
					System.out.println("invoiceentity.getArrayBillingDocument().get(i).getBillId()"+invoiceentity.getArrayBillingDocument().get(i).getBillId());
					 
//					 if(invoiceentity.getCompanyId()!=null)
//						{
//							billDocumentEntity=ofy().load().type(BillingDocument.class).filter("count", invoiceentity.getArrayBillingDocument().get(i).getBillId()).filter("companyId",billEntity.getCompanyId()).filter("contractCount", billEntity.getContractCount()).filter("typeOfOrder", billEntity.getTypeOfOrder().trim()).first().now();
//						}
//						else
//						{	
//							billDocumentEntity=ofy().load().type(BillingDocument.class).filter("count", invoiceentity.getArrayBillingDocument().get(i).getBillId()).filter("contractCount", billEntity.getContractCount()).filter("invoiceCount", billEntity.getInvoiceCount()).filter("typeOfOrder", billEntity.getTypeOfOrder().trim()).first().now();	
//						}
					 System.out.println("billDocumentEntity.getBillingTaxes()"+invoiceentity.getBillingTaxes().size());
					
					 
					 InvoiceCharges taxes = new InvoiceCharges();
					 taxes.setChargesList(invoiceentity.getBillingTaxes());
					 taxes.setPayPercent(invoiceentity.getArrPayTerms().get(0).getPayTermPercent());
					 billingTaxesLis.add(taxes);
					 
					 InvoiceCharges charges = new InvoiceCharges();
					 charges.setPayPercent(invoiceentity.getArrPayTerms().get(0).getPayTermPercent());
					 charges.setOtherChargesList(invoiceentity.getBillingOtherCharges());
					 billingChargesLis.add(charges);
					 
//					 billingTaxesLis.addAll(billingTaxesLis11);
//					 billingChargesLis.addAll(billingChargesLis11);
					 System.out.println("billingTaxesLis============="+billingTaxesLis.size());
					 
					 System.out.println("billingChargesLis============"+billingChargesLis.size());
				 }
				 
			 }
			 
			
	//**************************changes ends here **************************************
			 
			 if(invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)&&salesOrderEntity!=null)
			 {
				 payTermsLis=salesOrderEntity.getPaymentTermsList();
				 
				 salesOrderTaxesLis=salesOrderEntity.getProductTaxes();
					salesOrderChargesLis= salesOrderEntity.getProductCharges();
				 
			 }
			 if(invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)&&purchaseOrder!=null){
				 payTermsLis=purchaseOrder.getPaymentTermsList();
				 
				 
				  purchaseOrderTaxesLis=purchaseOrder.getProductTaxes();
				  purchaseOrderChargesLis=purchaseOrder.getProductCharges();
				 
			 }
			 /**
			  * Date : 05-03-2017 By Anil
			  */
			 if(invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICEPO)&&servicePo!=null){
				 payTermsLis=servicePo.getPaymentTermsList();
				 
				 
				  purchaseOrderTaxesLis=servicePo.getProductTaxes();
				  purchaseOrderChargesLis=servicePo.getProductCharges();
				 
			 }
			 
			 if(invoiceentity.getArrayBillingDocument().size()>1)
				{
					for(int i=0;i<invoiceentity.getArrayBillingDocument().size();i++){
						BillingDocument bill =new BillingDocument();
						if(invoiceentity.getCompanyId()!=null){
							bill=ofy().load().type(BillingDocument.class).filter("companyId",invoiceentity.getCompanyId()).filter("contractCount", invoiceentity.getContractCount()).filter("count", invoiceentity.getArrayBillingDocument().get(i).getBillId()).filter("typeOfOrder", invoiceentity.getTypeOfOrder().trim()).first().now();
						}else{
							bill=ofy().load().type(BillingDocument.class).filter("contractCount", invoiceentity.getContractCount()).filter("count", invoiceentity.getArrayBillingDocument().get(i).getBillId()).filter("typeOfOrder", invoiceentity.getTypeOfOrder().trim()).first().now();
						}
						payTerms.addAll(bill.getArrPayTerms());
					}
			 
				}
			 else
				{
					payTerms.addAll(invoiceentity.getArrPayTerms());
				}
			 
			 
			 articletype = new ArrayList<ArticleType>();
				
				
				
				
			 
			 if(invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
				 if(cust.getArticleTypeDetails().size()!=0){
						articletype.addAll(cust.getArticleTypeDetails());
					}
					if(comp.getArticleTypeDetails().size()!=0){
						articletype.addAll(comp.getArticleTypeDetails());
					}
			 }
			 /**
			  * Date :05-03-2017 By Anil
			  */
			 
			 if(invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)
					 ||invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICEPO)){
				 if(vendor.getArticleTypeDetails().size()!=0){
						articletype.addAll(vendor.getArticleTypeDetails());
					}
					if(comp.getArticleTypeDetails().size()!=0){
						articletype.addAll(comp.getArticleTypeDetails());
					}
			 }
			 
			  logger.log(Level.SEVERE,"set invoice 1.....");
			 try {
				invComment=getInvComment();
				logger.log(Level.SEVERE,"set invoice 2.....");
			} catch (Exception e) {
				logger.log(Level.SEVERE,"set invoice 3.....");
				e.printStackTrace();
			}
			 logger.log(Level.SEVERE,"set invoice 4.....");
			 
		}

		public  void createPdf() 
		{
		    //showLogo(document, comp);
			try {
				if(CompanyNameLogoflag==true){
					createLogo123(document,comp);
					createCompanyAddressForLeatherTechno();
				}
				else{
					createLogo(document,comp);
					createCompanyAddress();
				}
				
			
			if(invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
				 createCustInfo1();
			}
			
			/**
			 * Date : 05-03-2017 By Anil
			 */
		   
		    if(invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)
		    		||invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICEPO)){
		    	createVendorInfo();
		    }
		    
			createProductTable();
			termsConditionsInfo();
			addFooter();
		} 
		    catch (Exception e1) {
				e1.printStackTrace();
			}
		}

		
		
		public  void createPdfForEmail(Invoice invoiceDetails,Company companyEntity,Customer custEntity,SalesOrder salesOrder,PurchaseOrder poEntity,Vendor vendorEntity) 
		{
			try {
				this.invoiceentity=invoiceDetails;
				this.comp=companyEntity;
				this.cust=custEntity;
				this.purchaseOrder=poEntity;
				this.salesOrderEntity=salesOrder;
				this.salesProd=invoiceDetails.getSalesOrderProducts();
//				this.billingTaxesLis=invoiceDetails.getBillingTaxes();
//				this.billingChargesLis=invoiceDetails.getBillingOtherCharges();
				this.vendor=vendorEntity;
				 if(invoiceDetails.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)&&salesOrderEntity!=null)
				 {
					 payTermsLis=salesOrderEntity.getPaymentTermsList();
				 }
				 if(invoiceDetails.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)&&poEntity!=null){
					 payTermsLis=poEntity.getPaymentTermsList();
				 }
				 this.salesOrderTaxesLis=salesOrderEntity.getProductTaxes();
				 this.salesOrderChargesLis= salesOrderEntity.getProductCharges();
				 
				 if(invoiceentity.getCompanyId()!=null){
						processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", invoiceentity.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
						if(processConfig!=null){
							for(int k=0;k<processConfig.getProcessList().size();k++)
							{
								if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PrintCompanyNameAsLogo")&&processConfig.getProcessList().get(k).isStatus()==true)
								{
									this.CompanyNameLogoflag=true;
								}
							}
						}
					}
				 
				 articletype = new ArrayList<ArticleType>();
					
				 if(invoiceDetails.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
					 if(cust.getArticleTypeDetails().size()!=0){
							articletype.addAll(cust.getArticleTypeDetails());
						}
						if(comp.getArticleTypeDetails().size()!=0){
							articletype.addAll(comp.getArticleTypeDetails());
						}
				 }
				 
				 if(invoiceDetails.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
					 if(vendor.getArticleTypeDetails().size()!=0){
							articletype.addAll(vendor.getArticleTypeDetails());
						}
						if(comp.getArticleTypeDetails().size()!=0){
							articletype.addAll(comp.getArticleTypeDetails());
						}
				 }
				 
//				 if(invoiceentity.getCompanyId()!=null)
//					{
//						billEntity=ofy().load().type(BillingDocument.class).filter("companyId",invoiceentity.getCompanyId()).filter("contractCount", invoiceentity.getContractCount()).filter("invoiceCount", invoiceentity.getCount()).filter("typeOfOrder", invoiceentity.getTypeOfOrder().trim()).first().now();
//					}
//					else
//					{	
//						billEntity=ofy().load().type(BillingDocument.class).filter("contractCount", invoiceentity.getContractCount()).filter("invoiceCount", invoiceentity.getCount()).filter("typeOfOrder", invoiceentity.getTypeOfOrder().trim()).first().now();	
//					}
				 
				 if(invoiceentity.getArrayBillingDocument().size()>1)
					{
						for(int i=0;i<invoiceentity.getArrayBillingDocument().size();i++){
							BillingDocument bill =new BillingDocument();
							if(invoiceentity.getCompanyId()!=null){
								bill=ofy().load().type(BillingDocument.class).filter("companyId",invoiceentity.getCompanyId()).filter("contractCount", invoiceentity.getContractCount()).filter("count", invoiceentity.getArrayBillingDocument().get(i).getBillId()).filter("typeOfOrder", invoiceentity.getTypeOfOrder().trim()).first().now();
							}else{
								bill=ofy().load().type(BillingDocument.class).filter("contractCount", invoiceentity.getContractCount()).filter("count", invoiceentity.getArrayBillingDocument().get(i).getBillId()).filter("typeOfOrder", invoiceentity.getTypeOfOrder().trim()).first().now();
							}
							payTerms.addAll(bill.getArrPayTerms());
						}
					}
				 else
					{
						payTerms.addAll(invoiceentity.getArrPayTerms());
					}
				
				 if(CompanyNameLogoflag==true){
						createLogo(document,comp);
						createCompanyAddressForLeatherTechno();
					}
					else{
						createLogo123(document,comp);
						createCompanyAddress();
					}
			
			if(invoiceDetails.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
				 createCustInfo1();
			}
		   
		    if(invoiceDetails.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
		    	createVendorInfo();
		    }
		    
			createProductTable();
			termsConditionsInfo();
			addFooter();
		} 
		    catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		
		
private void createLogo123(Document doc, Company comp) {
			
			
			//********************logo for server ********************
			
		DocumentUpload document =comp.getLogo();
		
		//patch
		String hostUrl; 
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(20f);
			image2.scaleAbsoluteWidth(250f);
			image2.setAbsolutePosition(40f,725);	
			doc.add(image2);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
		
		
		
		//***********************for local **************************
		
//		try
//		{
//		Image image1=Image.getInstance("images/logo1.png");
//		
//		image1.scalePercent(20f);
//		image1.scaleAbsoluteWidth(250f);
//		image1.setAbsolutePosition(35f,705);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		
		}

		private void createBlank() {

			 Paragraph blank =new Paragraph();
			    blank.add(Chunk.NEWLINE);
			    try {
					document.add(blank);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
			
			
	}

		private void createLogo(Document doc, Company comp) {
				
		
				//********************logo for server ********************
			DocumentUpload document =comp.getLogo();
		
			//patch
			String hostUrl; 
			String environment = System.getProperty("com.google.appengine.runtime.environment");
			if (environment.equals("Production")) {
			    String applicationId = System.getProperty("com.google.appengine.application.id");
			    String version = System.getProperty("com.google.appengine.application.version");
			    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
			} else {
			    hostUrl = "http://localhost:8888";
			}
			try {
				Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
				image2.scalePercent(20f);
				image2.setAbsolutePosition(40f,765f);	
				doc.add(image2);
			} catch (Exception e) {
				e.printStackTrace();
				}
			}
				




	public  void createCompanyHedding()
	{
		Phrase companyName= new Phrase(comp.getBusinessUnitName(),font16bold);
	     Paragraph p =new Paragraph();
	    
	     p.add(Chunk.NEWLINE);
	     p.add(companyName);
	     p.setAlignment(Element.ALIGN_CENTER);
	     
	     PdfPTable headingTable=new PdfPTable(1);
	     headingTable.setWidthPercentage(100);
	     headingTable.setHorizontalAlignment(Element.ALIGN_CENTER);
	     headingTable.addCell(p);
	     try {
			document.add(headingTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public  void createCompanyAdress()
	{
		Phrase adressline1= new Phrase(comp.getAddress().getAddrLine1(),font12);
		Phrase adressline2=null;
		if(comp.getAddress().getAddrLine2()!=null)
		{
			adressline2= new Phrase(comp.getAddress().getAddrLine2(),font12);
		}
		
		
		Phrase landmark=null;
		Phrase locality=null;
		if(comp.getAddress().getLandmark()!=null&&comp.getAddress().getLocality().equals("")==false)
		{
			
			String landmarks=comp.getAddress().getLandmark()+" , ";
			locality= new Phrase(landmarks+comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin(),font12);
		}
		else
		{
			locality= new Phrase(comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin(),font12);
		}
		Paragraph adressPragraph=new Paragraph();
		adressPragraph.add(adressline1);
		adressPragraph.add(Chunk.NEWLINE);
		if(adressline2!=null)
		{
				adressPragraph.add(adressline2);
				adressPragraph.add(Chunk.NEWLINE);
		}
		
		adressPragraph.add(locality);
		adressPragraph.setAlignment(Element.ALIGN_CENTER);
		adressPragraph.setSpacingBefore(10);
		adressPragraph.setSpacingAfter(10);
		
		
		// Phrase for phone,landline ,fax and email
		
		Phrase titlecell=new Phrase("Mob :",font8bold);
		Phrase titleTele = new Phrase("Tele :",font8bold);
		Phrase titleemail= new Phrase("Email :",font8bold);
		Phrase titlefax= new Phrase("Fax :",font8bold);
		
		Phrase titleservicetax=new Phrase("Service Tax No: ",font8bold);
		Phrase titlevatatx=new Phrase("   Vat Tax No: ",font8bold);
		String servicetax=comp.getServiceTaxNo();
		String vatttax=comp.getVatTaxNo();
		Phrase servicetaxphrase=null;
		Phrase vattaxphrase=null;
		
		if(servicetax!=null&&servicetax.trim().equals("")==false)
		{
			servicetaxphrase=new Phrase(servicetax,font8);
		}
			

		if(vatttax!=null&&vatttax.trim().equals("")==false)
		{
			vattaxphrase=new Phrase(vatttax,font8);
		}
		

		
		
		// cell number logic
		String stringcell1=comp.getContact().get(0).getCellNo1()+"";
		String stringcell2=null;
		Phrase mob=null;
		if(comp.getContact().get(0).getCellNo2()!=-1)
			stringcell2=comp.getContact().get(0).getCellNo2()+"";
		if(stringcell2!=null)
		    mob=new Phrase(stringcell1+" / "+stringcell2,font8);
		else
			 mob=new Phrase(stringcell1,font8);
		
		
		//LANDLINE LOGIC
		Phrase landline=null;
		if(comp.getContact().get(0).getLandline()!=-1)
			landline=new Phrase(comp.getContact().get(0).getLandline()+"",font8);
		
		
		
		
		// fax logic
		Phrase fax=null;
		if(comp.getContact().get(0).getFaxNo()!=null)
			fax=new Phrase(comp.getContact().get(0).getFaxNo()+"",font8);
		// email logic
		Phrase email= new Phrase(comp.getContact().get(0).getEmail(),font8);
		
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(titlecell);
		adressPragraph.add(mob);
		adressPragraph.add(new Chunk("         "));
		
		
		if(landline!=null)
		{
			
			adressPragraph.add(titleTele);
			adressPragraph.add(landline);
			adressPragraph.add(new Chunk("         "));
		}
		
		
		
		if(fax!=null)
		{
			
			adressPragraph.add(titlefax);
			adressPragraph.add(fax);
			adressPragraph.add(Chunk.NEWLINE);
		}
		
		adressPragraph.add(titleemail);
		adressPragraph.add(new Chunk(" "));
		adressPragraph.add(email);
		adressPragraph.add(Chunk.NEWLINE);
		
		if(servicetaxphrase!=null)
		{
			adressPragraph.add(titleservicetax);
			adressPragraph.add(servicetaxphrase);
			
		}
		
		if(vattaxphrase!=null)
		{
			adressPragraph.add(titlevatatx);
			adressPragraph.add(vattaxphrase);
			adressPragraph.add(new Chunk("         "));
			
		}
		
		String title="";
		
		if(AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())||invoiceentity.getInvoiceType().trim().equals(AppConstants.CREATEPROFORMAINVOICE)){
			title="Proforma Invoice";
		}
		else{
			title="Tax Invoice";
		}
		
		Phrase titlephrase =new Phrase(title,font14bold);
		Paragraph titlepdfpara=new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell titlepdfcell=new PdfPCell();
		titlepdfcell.addElement(titlepdfpara);
		titlepdfcell.setBorderWidthRight(0);
		titlepdfcell.setBorderWidthLeft(0);
		
		
		PdfPTable titlepdftable=new PdfPTable(1);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.addCell(titlepdfcell);
		
		PdfPTable addressTable=new PdfPTable(1);
		addressTable.setWidthPercentage(100);
		addressTable.addCell(adressPragraph);
		addressTable.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		try {
			document.add(addressTable);
			document.add(titlepdftable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public  void createCompanyAddress()
	{
		 Phrase companyName= new Phrase(comp.getBusinessUnitName(),font16bold);
	     Paragraph p =new Paragraph();
	     p.add(Chunk.NEWLINE);
	     p.add(companyName);
	     p.setAlignment(Element.ALIGN_CENTER);
	     PdfPCell companyNameCell=new PdfPCell();
	     companyNameCell.addElement(p);
	     companyNameCell.setBorder(0);
	     
		String addressline1="";
		
		if(comp.getAddress().getAddrLine2()!=null){
			addressline1=comp.getAddress().getAddrLine1()+" , "+comp.getAddress().getAddrLine2();
		}
		else{
			addressline1=comp.getAddress().getAddrLine1();
		}
		Phrase addressline=new Phrase(addressline1,font12);
		Paragraph addresspara=new Paragraph();
		addresspara.add(addressline);
		addresspara.setAlignment(Element.ALIGN_CENTER);
		
		Phrase locality=null;
		if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition1");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 2");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition 3");
			locality= new Phrase(comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 4");
			locality= new Phrase(comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		Paragraph localityPragraph=new Paragraph();
		localityPragraph.add(locality);
		localityPragraph.setAlignment(Element.ALIGN_CENTER);
		Phrase contactinfo=null;
		if(comp.getLandline()!=0){
			contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+"    Phone: "+comp.getLandline()+"    Email 1: "+comp.getEmail().trim(),font10);
		}
		else{
			contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+"    Email 1: "+comp.getEmail().trim(),font10);
		}
		
		Phrase contactemail=new Phrase("Email 2: "+comp.getPocEmail(),font10);
		Paragraph realmobpara1=new Paragraph();
		realmobpara1.add(contactemail);
		realmobpara1.setAlignment(Element.ALIGN_CENTER);
		
		Paragraph realmobpara=new Paragraph();
		realmobpara.add(contactinfo);
		realmobpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell addresscell=new PdfPCell();
		addresscell.addElement(addresspara);
		addresscell.setBorder(0);
		PdfPCell localitycell=new PdfPCell();
		localitycell.addElement(localityPragraph);
		localitycell.setBorder(0);
		PdfPCell contactcell=new PdfPCell();
		contactcell.addElement(realmobpara);
		contactcell.setBorder(0);
		
		PdfPCell contactcell1=new PdfPCell();
		contactcell1.addElement(realmobpara1);
		contactcell1.setBorder(0);
		
		PdfPTable companyDetails=new PdfPTable(1);
		companyDetails.setWidthPercentage(100);
		
		companyDetails.addCell(companyNameCell);
		companyDetails.addCell(addresscell);
		companyDetails.addCell(localitycell);
		companyDetails.addCell(contactcell);
		companyDetails.addCell(contactcell1);
		
		
		String titlepdf="";
		if(AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())||invoiceentity.getInvoiceType().trim().equals(AppConstants.CREATEPROFORMAINVOICE))
			titlepdf="Proforma Invoice";
		else
			titlepdf="Tax Invoice";
		
		Phrase titlephrase=new Phrase(titlepdf,font14bold);
		Paragraph titlepdfpara=new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell titlecell=new PdfPCell();
		titlecell.addElement(titlepdfpara);
		titlecell.setBorder(0);
		
		Phrase blankphrase=new Phrase("",font8);
		PdfPCell blankCell=new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);
		
		PdfPTable titlepdftable=new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(titlecell);
		titlepdftable.addCell(blankCell);
		
		
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell comapnyCell=new PdfPCell(companyDetails);
		PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		parent.addCell(comapnyCell);
		parent.addCell(titlePdfCell);
		
		try {
			document.add(parent);
//			document.add(Chunk.NEWLINE);
//			document.add(Chunk.NEWLINE);
//			document.add(titlepdftable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	
	

	public  void createCompanyAddressForLeatherTechno()
	{
//		 Phrase companyName= new Phrase(comp.getBusinessUnitName(),font16bold);
//	     Paragraph p =new Paragraph();
//	     p.add(Chunk.NEWLINE);
//	     p.add(companyName);
//	     p.setAlignment(Element.ALIGN_LEFT);
//	     PdfPCell companyNameCell=new PdfPCell();
//	     companyNameCell.addElement(p);
//	     companyNameCell.setBorder(0);
	     
		String addressline1="";
		
		if(comp.getAddress().getAddrLine2()!=null){
			addressline1=comp.getAddress().getAddrLine1()+" , "+comp.getAddress().getAddrLine2();
		}
		else{
			addressline1=comp.getAddress().getAddrLine1();
		}
		Phrase addressline=new Phrase(addressline1,font12);
		Paragraph addresspara=new Paragraph();
		addresspara.add(addressline);
		addresspara.setAlignment(Element.ALIGN_LEFT);
		
		Phrase locality=null;
		if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition1");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 2");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition 3");
			locality= new Phrase(comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 4");
			locality= new Phrase(comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		Paragraph localityPragraph=new Paragraph();
		localityPragraph.add(locality);
		localityPragraph.setAlignment(Element.ALIGN_LEFT);
		Phrase contactinfo=null;
		if(comp.getLandline()!=0){
			contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+"    Phone: "+comp.getLandline(),font10);
		}
		else{
			contactinfo=new Phrase("Mobile: "+comp.getCellNumber1(),font10);
		}
		
		//*********************changes here for print logo *****************************
		Phrase contactemail1=new Phrase("Email 1: "+comp.getEmail(),font10);
		Paragraph emailpara1=new Paragraph();
		emailpara1.add(contactemail1);
		emailpara1.setAlignment(Element.ALIGN_LEFT);
		
		Phrase contactemail=new Phrase("Email 2: "+comp.getPocEmail(),font10);
		Paragraph realmobpara1=new Paragraph();
		realmobpara1.add(contactemail);
		realmobpara1.setAlignment(Element.ALIGN_LEFT);
		
		Paragraph realmobpara=new Paragraph();
		realmobpara.add(contactinfo);
		realmobpara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell addresscell=new PdfPCell();
		addresscell.addElement(addresspara);
		addresscell.setBorder(0);
		PdfPCell localitycell=new PdfPCell();
		localitycell.addElement(localityPragraph);
		localitycell.setBorder(0);
		PdfPCell contactcell=new PdfPCell();
		contactcell.addElement(realmobpara);
		contactcell.setBorder(0);
		
		//******************************rohan changes for print logo 
		
		PdfPCell contactEmailcell=new PdfPCell();
		contactEmailcell.addElement(emailpara1);
		contactEmailcell.setBorder(0);
		
		
		
		PdfPCell contactcell1=new PdfPCell();
		contactcell1.addElement(realmobpara1);
		contactcell1.setBorder(0);
		
		PdfPTable companyDetails=new PdfPTable(1);
		companyDetails.setWidthPercentage(100);
		
//		companyDetails.addCell(companyNameCell);
		companyDetails.addCell(addresscell);
		companyDetails.addCell(localitycell);
		companyDetails.addCell(contactcell);
		
		//*******************rohan changes here *****************
		companyDetails.addCell(contactEmailcell);
		companyDetails.addCell(contactcell1);
		
		
		
		//*********************rohan changes here for print logo ****************
		Phrase blankFont10= new Phrase(" ",font10);
		PdfPCell blankcell= new PdfPCell(blankFont10);
		blankcell.setBorder(0);
		
		PdfPCell companyDetailstableCell =new PdfPCell();
		companyDetailstableCell.addElement(companyDetails);
		companyDetailstableCell.setBorder(0);
		
		PdfPTable parentCompanyDetailsTable=new PdfPTable(2);
		companyDetails.setWidthPercentage(100);
		
		parentCompanyDetailsTable.addCell(blankcell);
		parentCompanyDetailsTable.addCell(companyDetailstableCell);
		
		
		
		
		
		String titlepdf="";
		if(AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())||invoiceentity.getInvoiceType().trim().equals(AppConstants.CREATEPROFORMAINVOICE))
			titlepdf="Proforma Invoice";
		else
			titlepdf="Tax Invoice";
		
		Phrase titlephrase=new Phrase(titlepdf,font14bold);
		Paragraph titlepdfpara=new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell titlecell=new PdfPCell();
		titlecell.addElement(titlepdfpara);
		titlecell.setBorder(0);
		
		Phrase blankphrase=new Phrase("",font8);
		PdfPCell blankCell=new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);
		
		PdfPTable titlepdftable=new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(titlecell);
		titlepdftable.addCell(blankCell);
		
		
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell comapnyCell=new PdfPCell(parentCompanyDetailsTable);
		PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		parent.addCell(comapnyCell);
		parent.addCell(titlePdfCell);
		
		try {
			document.add(parent);
//			document.add(Chunk.NEWLINE);
//			document.add(Chunk.NEWLINE);
//			document.add(titlepdftable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	////do

	public void createCustInfo1()
	{
//		String tosir="To, M/S";
		
		//************************changes made by rohan	for M/s **************** 
		String tosir= null;
		if(cust.isCompany()==true){
			
			
		 tosir="To, M/S";
		}
		else{
			tosir="To,";
		}
		//*******************changes ends here ********************sss*************
		
		
		Phrase clientname=null;
		
		if(cust.isCompany()==true){
			clientname=new Phrase(tosir+"  "+cust.getCompanyName().trim(),font10bold);
		}
		else{
			clientname=new Phrase(tosir+"  "+cust.getFullname().trim(),font10bold);
		}
		PdfPCell custnamecell=new PdfPCell();
		custnamecell.addElement(clientname);
		custnamecell.setBorder(0);
		
	Phrase custaddress1=new Phrase("                   "+cust.getAdress().getAddrLine1().trim(),font8);
	Phrase custaddress2=null;
	if(!cust.getAdress().getAddrLine2().equals("")||cust.getAdress().getAddrLine2()!=null){
		custaddress2=new Phrase("                   "+cust.getAdress().getAddrLine2().trim(),font8);
	}
	Phrase landmark=null;

	if(!cust.getAdress().getLandmark().equals("")){
		landmark=new Phrase("                   "+cust.getAdress().getLandmark(),font8);
	}else{
		landmark=new Phrase("",font10);
	}
	Phrase locality=null;
	if(!cust.getAdress().getLocality().equals("")){
		locality=new Phrase("                   "+cust.getAdress().getLocality(),font8);
	}else{
		locality=new Phrase("",font8);
	}
		Phrase city=new Phrase("                   "+cust.getAdress().getCity().trim()+" - "+cust.getAdress().getPin(),font8);


	Phrase contactcust=new Phrase("Cell: "+cust.getCellNumber1()+"    Email:  "+cust.getEmail().trim()+" ",font8);

	PdfPCell custaddr1cell=new PdfPCell();
	custaddr1cell.addElement(custaddress1);
	custaddr1cell.setBorder(0);

	PdfPCell custaddr2cell=null;
	if(custaddress2!=null){
		custaddr2cell=new PdfPCell();
		custaddr2cell.addElement(custaddress2);
		custaddr2cell.setBorder(0);
	}

	PdfPCell custlocalitycell=new PdfPCell();
	custlocalitycell.addElement(locality);
	custlocalitycell.setBorder(0);

	PdfPCell lanmarkcell=new PdfPCell();
	lanmarkcell.addElement(landmark);
	lanmarkcell.setBorder(0);

	PdfPCell citycell=new PdfPCell();
	citycell.addElement(city);
	citycell.setBorder(0);

	PdfPCell custcontactcell=new PdfPCell();
	custcontactcell.addElement(contactcust);
	//custcontactcell.addElement(Chunk.NEWLINE);
	custcontactcell.setBorder(0);

	PdfPTable custtable=new PdfPTable(1);
	custtable.setWidthPercentage(100);
	custtable.addCell(custnamecell);
	custtable.addCell(custaddr1cell);
	if(!cust.getAdress().getAddrLine2().equals("")){
		custtable.addCell(custaddr2cell);
	}
	if(!cust.getAdress().getLandmark().equals("")){
		custtable.addCell(lanmarkcell);
	}
	if(!cust.getAdress().getLocality().equals("")){
		custtable.addCell(custlocalitycell);
	}
	custtable.addCell(citycell);
	custtable.addCell(custcontactcell);

	fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	Phrase billnotitle=new Phrase("Bill No.              : "+invoiceentity.getCount(),font8);
	
	Phrase datetitle=new Phrase("Date       :    "+fmt.format(invoiceentity.getInvoiceDate()),font8);

	
	
	//TimeZone.getTimeZone("India");

	// Use Madrid's time zone to format the date in


	//Phrase realdate=new Phrase(fmt.format(invoiceentity.getInvoiceDate())+"",font8);
	Phrase ponotitle=new Phrase("PO No.         :",font8);
	Phrase realpono=new Phrase("  ",font8);
	Phrase podatetitle=new Phrase("PO Date       :",font8);
	Phrase realpodate=new Phrase(" ",font8);

	Phrase chalno=new Phrase("Challan No.  :",font8);
	Phrase chalno1=new Phrase();
	Phrase chalndate=new Phrase("Date             :",font8);
	Phrase chalndate1=new Phrase();

	Phrase lrno=new Phrase("L. R. No.       :",font8);
	Phrase lrno1=new Phrase();

	Phrase lrnodate=new Phrase("Date             :",font8);
	Phrase lrnodate1=new Phrase(" ");

	Phrase despached=new Phrase("Despatched  :",font8);
	Phrase despached1=new Phrase();





	PdfPCell billnotitlecell=new PdfPCell();
	billnotitlecell.addElement(billnotitle);
	billnotitlecell.setBorder(0);

	//PdfPCell billnocell=new PdfPCell();
	//billnocell.addElement(realbillno);
	//billnocell.setBorder(0);

	PdfPCell datetitlecell=new PdfPCell();
	datetitlecell.addElement(datetitle);
	datetitlecell.setBorder(0);

	//PdfPCell datecell=new PdfPCell();
	//datecell.addElement(realdate);
	//datecell.setBorder(0);

	PdfPCell ponotitlecell=new PdfPCell();
	ponotitlecell.addElement(ponotitle);
	ponotitlecell.setBorder(0);

	PdfPCell ponocell=new PdfPCell();
	ponocell.addElement(realpono);
	ponocell.setBorder(0);

	PdfPCell podatetitlecell=new PdfPCell();
	podatetitlecell.addElement(podatetitle);
	podatetitlecell.setBorder(0);

	PdfPCell podatecell=new PdfPCell();
	podatecell.addElement(realpodate);
	podatecell.setBorder(0);

	PdfPCell chalncell=new PdfPCell();
	chalncell.addElement(chalno);
	chalncell.setBorder(0);

	PdfPCell chalncell1=new PdfPCell();
	chalncell1.addElement(chalno1);
	chalncell1.setBorder(0);


	PdfPCell chalnDatecell=new PdfPCell();
	chalnDatecell.addElement(chalndate);
	chalnDatecell.setBorder(0);

	PdfPCell chalnDatecell1=new PdfPCell();
	chalnDatecell1.addElement(chalndate1);
	chalnDatecell1.setBorder(0);

	PdfPCell despathedcell=new PdfPCell();
	despathedcell.addElement(despached);
	despathedcell.setBorder(0);

	PdfPCell despathedcell1=new PdfPCell();
	despathedcell1.addElement(despached1);
	despathedcell1.setBorder(0);

	PdfPCell lrnocell=new PdfPCell();
	lrnocell.addElement(lrno);
	lrnocell.setBorder(0);

	PdfPCell lrnocell1=new PdfPCell();
	lrnocell1.addElement(lrno1);
	lrnocell1.setBorder(0);

	PdfPCell lrnoDatecell=new PdfPCell();
	lrnoDatecell.addElement(lrnodate);
	lrnoDatecell.setBorder(0);

	PdfPCell lrnoDatecell1=new PdfPCell();
	lrnoDatecell1.addElement(lrnodate1);
	lrnoDatecell1.setBorder(0);





	PdfPTable invoiceinfotable=new PdfPTable(2);
	invoiceinfotable.setWidthPercentage(100);
	invoiceinfotable.addCell(billnotitlecell);
	//invoiceinfotable.addCell(billnocell);
	invoiceinfotable.addCell(datetitlecell);
	
	//************rohan added sales order id and delivery note id ********
	
	//**********************rohan added this code for inserting sales order and delivery note id in sales invoice pdf
	
		Phrase orderID=new Phrase("Order Id             : "+salesOrderEntity.getCount(),font8);
		PdfPCell orderIDCell = new PdfPCell(orderID);
		orderIDCell.setBorder(0);
		invoiceinfotable.addCell(orderIDCell);
		
		
		Phrase orderDt=new Phrase("Date          : "+fmt.format(salesOrderEntity.getSalesOrderDate()),font8);
		PdfPCell orderDtCell = new PdfPCell(orderDt);
		orderDtCell.setBorder(0);
		invoiceinfotable.addCell(orderDtCell);
		
		PdfPCell delNoteIdCell=null;
		PdfPCell delNoteDateCell=null;
		for(int i=0;i<deliveryNote.size();i++)
		{
			
			Phrase delNoteId=new Phrase("DeliveryNote Id  : "+deliveryNote.get(i).getCount(),font8);
			delNoteIdCell= new PdfPCell(delNoteId);
			delNoteIdCell.setBorder(0);
			invoiceinfotable.addCell(delNoteIdCell);
			
			Phrase delNoteDate=new Phrase("Date       :    "+fmt.format(deliveryNote.get(i).getCreationDate()),font8);
			delNoteDateCell = new PdfPCell(delNoteDate);
			delNoteDateCell.setBorder(0);
			invoiceinfotable.addCell(delNoteDateCell);
		}
		
		//   changes ends here ****************
	
	
	
	//invoiceinfotable.addCell(datecell);
	//invoiceinfotable.addCell(ponotitlecell);
	//invoiceinfotable.addCell(ponocell);
	//invoiceinfotable.addCell(podatetitlecell);
	//invoiceinfotable.addCell(podatecell);
	//
	//invoiceinfotable.addCell(chalncell);
	//invoiceinfotable.addCell(chalncell1);
	//invoiceinfotable.addCell(chalnDatecell);
	//invoiceinfotable.addCell(chalnDatecell1);
	//
	//invoiceinfotable.addCell(lrnocell);
	//invoiceinfotable.addCell(lrnocell1);
	//invoiceinfotable.addCell(lrnoDatecell);
	//invoiceinfotable.addCell(lrnoDatecell1);
	//invoiceinfotable.addCell(despathedcell);
	//invoiceinfotable.addCell(despathedcell1);


	////start

	PdfPCell blankcel=new PdfPCell();
	blankcel.setBorder(0);
	PdfPCell blankcel1=new PdfPCell();
	blankcel1.setBorder(0);
	invoiceinfotable.addCell(blankcel);
	invoiceinfotable.addCell(blankcel1);
//	Phrase typename=null;
//	Phrase typevalue=null;




//	PdfPTable artictable=new PdfPTable(3);
//	artictable.setWidthPercentage(100);
//
//	try {
//		artictable.setWidths(columnWidths2);
//	} catch (DocumentException e2) {
//		// TODO Auto-generated catch block
//		e2.printStackTrace();
//	}
	
	
	
//	for(int i=0;i<this.articletype.size();i++){
//		 
//			
//			if(articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().equals("SalesInvoice")){
//				
//				 typename = new Phrase(articletype.get(i).getArticleTypeName(),font8);
//				 typevalue = new Phrase(articletype.get(i).getArticleTypeValue(),font8);
//				 
//				 	PdfPCell tymanecell=new PdfPCell();
//					tymanecell.addElement(typename);
//					tymanecell.setBorder(0);
//					
//					PdfPCell typevalcell=new PdfPCell();
//					typevalcell.addElement(typevalue);
//					typevalcell.setBorder(0);
//					
//					Phrase colun=new Phrase(":",font8);
//					PdfPCell typevalcell1=new PdfPCell();
//					typevalcell1.addElement(colun);
//					typevalcell1.setBorder(0);
//					
//					
//					artictable.addCell(tymanecell);
//					artictable.addCell(typevalcell1);
//					artictable.addCell(typevalcell);
//					
//			 }
//			
//		 }
		

//	PdfPTable blanktable=new PdfPTable(1);
//
//
//	PdfPCell articcell=new PdfPCell(artictable);
//	articcell.setBorder(0);
//
//	PdfPCell articcell1=new PdfPCell(blanktable);
//	articcell1.setBorder(0);
//
//	invoiceinfotable.addCell(articcell);
//	invoiceinfotable.addCell(articcell1);



	/* Phrase artname=null;
	Phrase artvalue=null;


	for(int i=0;i<this.articletype.size();i++){
		 
		 if(!cust.getArticleTypeDetails().get(i).getArticlePrint().equals("")){
	 artname=new Phrase("Article Name : ");
	 artvalue=new Phrase("Article Value : ");

		 }
		 
	}
	PdfPTable artcltable=new PdfPTable(2);
	artcltable.setWidthPercentage(100);

	 Phrase dotline= new Phrase("------------------------------------------------------------------------------------"
		  		,font8);
		  PdfPCell dotlinecell= new PdfPCell();
		  dotlinecell.addElement(dotline);
		  dotlinecell.setBorder(0);
		  artcltable.addCell(dotlinecell);

	 for(int i=0;i<this.articletype.size();i++){
		 
		 if(!cust.getArticleTypeDetails().get(i).getArticlePrint().equals("")){
			
			 Phrase chunk = new Phrase(cust.getArticleTypeDetails().get(i).getArticleTypeName(),font8);
	    	 PdfPCell pdfnamecell = new PdfPCell(chunk);
			 
	    	  chunk = new Phrase(cust.getArticleTypeDetails().get(i).getArticleTypeValue(),font8);
	    	 PdfPCell pdfnamecell1 = new PdfPCell(chunk);
	    	 
			 
			 artcltable.addCell(pdfnamecell);
			 artcltable.addCell(pdfnamecell1);
		 }
		 
		
	 }
	 
	 PdfPCell artcell = new PdfPCell();
	 artcell.addElement(artcltable);
	// artcell.setBorder(0);
	 
	 invoiceinfotable.addCell(artcell);*/
	 


	//Phrase vatTaxNO=null;
	//  Phrase stTaxNO=null;
	// 
	//  if(comp.getVatTaxNo()!=null){
	//   vatTaxNO=new Phrase("Vat tax no      : "+comp.getVatTaxNo(),font8);
	//  }else{
//		  
//		  vatTaxNO=new Phrase("",font8);
	//  }
	//  
	//  if(comp.getServiceTaxNo()!=null){
	//   stTaxNO=new Phrase("Service tax no : "+comp.getServiceTaxNo(),font8);
	//  }else{
//		  stTaxNO=new Phrase("",font8);
	//  }
	  
	//  Phrase dotline= new Phrase("------------------------------------------------------------------------------------"
//	  		,font8);
	//  PdfPCell dotlinecell= new PdfPCell();
	//  dotlinecell.addElement(dotline);
	//  dotlinecell.setBorder(0);
	  
	  
	//  PdfPCell vatcell = new PdfPCell();
	//  vatcell.addElement(vatTaxNO);
	//  vatcell.setBorder(0);
	//  
	//  PdfPCell stcell = new PdfPCell();
	//  stcell.addElement(stTaxNO);
	//  stcell.setBorder(0);


	//  invoiceinfotable.addCell(dotlinecell);
//		invoiceinfotable.addCell(vatcell);
//		invoiceinfotable.addCell(stcell);
	//  

	////end



	PdfPTable parenttable= new PdfPTable(2);
	parenttable.setWidthPercentage(100);
	try {
		parenttable.setWidths(new float[]{50,50});
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}

	PdfPCell custinfocell = new PdfPCell();
	PdfPCell  invoiceinfocell = new PdfPCell();

	custinfocell.addElement(custtable);
	invoiceinfocell.addElement(invoiceinfotable);
	parenttable.addCell(custinfocell);
	parenttable.addCell(invoiceinfocell);

	try {
		document.add(parenttable);
	} catch (DocumentException e) {
		e.printStackTrace();
	}

	}

	///do


	private void createVendorInfo() {


		String tosir="To, M/S";
		
		Phrase clientname=null;
		// old cold commented by vijay becs we changed first middle and last name to full name of poc
//			clientname=new Phrase(tosir+"  "+vendor.getFirstName().trim()+" "+vendor.getLastName(),font10bold);
		
			// new code added by vijay to after TO m/s show must show vendor name not poc name
			clientname=new Phrase(tosir+"  "+vendor.getVendorName().trim(),font10bold);
			
			PdfPCell custnamecell=new PdfPCell();
		custnamecell.addElement(clientname);
		custnamecell.setBorder(0);
		
	Phrase custaddress1=new Phrase("                   "+vendor.getPrimaryAddress().getAddrLine1().trim(),font8);
	Phrase custaddress2=null;
	if(!vendor.getPrimaryAddress().getAddrLine2().equals("")||vendor.getPrimaryAddress().getAddrLine2()!=null){
		custaddress2=new Phrase("                   "+vendor.getPrimaryAddress().getAddrLine2().trim(),font8);
	}
	Phrase landmark=null;

	if(!vendor.getPrimaryAddress().getLandmark().equals("")){
		landmark=new Phrase("                   "+vendor.getPrimaryAddress().getLandmark(),font8);
	}else{
		landmark=new Phrase("",font10);
	}
	Phrase locality=null;
	if(!vendor.getPrimaryAddress().getLocality().equals("")){
		locality=new Phrase("                   "+vendor.getPrimaryAddress().getLocality(),font8);
	}else{
		locality=new Phrase("",font8);
	}
		Phrase city=new Phrase("                   "+vendor.getPrimaryAddress().getCity().trim()+" - "+vendor.getPrimaryAddress().getPin(),font8);


	Phrase contactcust=new Phrase("Cell: "+vendor.getCellNumber1()+"    Email:  "+vendor.getEmail().trim()+" ",font8);

	PdfPCell custaddr1cell=new PdfPCell();
	custaddr1cell.addElement(custaddress1);
	custaddr1cell.setBorder(0);

	PdfPCell custaddr2cell=null;
	if(custaddress2!=null){
		custaddr2cell=new PdfPCell();
		custaddr2cell.addElement(custaddress2);
		custaddr2cell.setBorder(0);
	}

	PdfPCell custlocalitycell=new PdfPCell();
	custlocalitycell.addElement(locality);
	custlocalitycell.setBorder(0);

	PdfPCell lanmarkcell=new PdfPCell();
	lanmarkcell.addElement(landmark);
	lanmarkcell.setBorder(0);

	PdfPCell citycell=new PdfPCell();
	citycell.addElement(city);
	citycell.setBorder(0);

	PdfPCell custcontactcell=new PdfPCell();
	custcontactcell.addElement(contactcust);
	//custcontactcell.addElement(Chunk.NEWLINE);
	custcontactcell.setBorder(0);

	PdfPTable custtable=new PdfPTable(1);
	custtable.setWidthPercentage(100);
	custtable.addCell(custnamecell);
	custtable.addCell(custaddr1cell);
	if(!vendor.getPrimaryAddress().getAddrLine2().equals("")){
		custtable.addCell(custaddr2cell);
	}
	if(!vendor.getPrimaryAddress().getLandmark().equals("")){
		custtable.addCell(lanmarkcell);
	}
	if(!vendor.getPrimaryAddress().getLocality().equals("")){
		custtable.addCell(custlocalitycell);
	}
	custtable.addCell(citycell);
	custtable.addCell(custcontactcell);

	fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	Phrase billnotitle=new Phrase("Bill No.          :    "+invoiceentity.getCount(),font8);
	//Phrase realbillno=new Phrase(invoiceentity.getCount()+"",font8); 
	Phrase datetitle=new Phrase("Date         :   "+fmt.format(invoiceentity.getInvoiceDate()),font8);

	//TimeZone.getTimeZone("India");

	// Use Madrid's time zone to format the date in


	//Phrase realdate=new Phrase(fmt.format(invoiceentity.getInvoiceDate())+"",font8);
	Phrase ponotitle=new Phrase("PO No.         :",font8);
	Phrase realpono=new Phrase("  ",font8);
	Phrase podatetitle=new Phrase("PO Date       :",font8);
	Phrase realpodate=new Phrase(" ",font8);

	Phrase chalno=new Phrase("Challan No.  :",font8);
	Phrase chalno1=new Phrase();
	Phrase chalndate=new Phrase("Date             :",font8);
	Phrase chalndate1=new Phrase();

	Phrase lrno=new Phrase("L. R. No.       :",font8);
	Phrase lrno1=new Phrase();

	Phrase lrnodate=new Phrase("Date             :",font8);
	Phrase lrnodate1=new Phrase(" ");

	Phrase despached=new Phrase("Despatched  :",font8);
	Phrase despached1=new Phrase();





	PdfPCell billnotitlecell=new PdfPCell();
	billnotitlecell.addElement(billnotitle);
	billnotitlecell.setBorder(0);

	//PdfPCell billnocell=new PdfPCell();
	//billnocell.addElement(realbillno);
	//billnocell.setBorder(0);

	PdfPCell datetitlecell=new PdfPCell();
	datetitlecell.addElement(datetitle);
	datetitlecell.setBorder(0);

	//PdfPCell datecell=new PdfPCell();
	//datecell.addElement(realdate);
	//datecell.setBorder(0);

	PdfPCell ponotitlecell=new PdfPCell();
	ponotitlecell.addElement(ponotitle);
	ponotitlecell.setBorder(0);

	PdfPCell ponocell=new PdfPCell();
	ponocell.addElement(realpono);
	ponocell.setBorder(0);

	PdfPCell podatetitlecell=new PdfPCell();
	podatetitlecell.addElement(podatetitle);
	podatetitlecell.setBorder(0);

	PdfPCell podatecell=new PdfPCell();
	podatecell.addElement(realpodate);
	podatecell.setBorder(0);

	PdfPCell chalncell=new PdfPCell();
	chalncell.addElement(chalno);
	chalncell.setBorder(0);

	PdfPCell chalncell1=new PdfPCell();
	chalncell1.addElement(chalno1);
	chalncell1.setBorder(0);


	PdfPCell chalnDatecell=new PdfPCell();
	chalnDatecell.addElement(chalndate);
	chalnDatecell.setBorder(0);

	PdfPCell chalnDatecell1=new PdfPCell();
	chalnDatecell1.addElement(chalndate1);
	chalnDatecell1.setBorder(0);

	PdfPCell despathedcell=new PdfPCell();
	despathedcell.addElement(despached);
	despathedcell.setBorder(0);

	PdfPCell despathedcell1=new PdfPCell();
	despathedcell1.addElement(despached1);
	despathedcell1.setBorder(0);

	PdfPCell lrnocell=new PdfPCell();
	lrnocell.addElement(lrno);
	lrnocell.setBorder(0);

	PdfPCell lrnocell1=new PdfPCell();
	lrnocell1.addElement(lrno1);
	lrnocell1.setBorder(0);

	PdfPCell lrnoDatecell=new PdfPCell();
	lrnoDatecell.addElement(lrnodate);
	lrnoDatecell.setBorder(0);

	PdfPCell lrnoDatecell1=new PdfPCell();
	lrnoDatecell1.addElement(lrnodate1);
	lrnoDatecell1.setBorder(0);





	PdfPTable invoiceinfotable=new PdfPTable(2);
	invoiceinfotable.setWidthPercentage(100);
	invoiceinfotable.addCell(billnotitlecell);
	invoiceinfotable.addCell(datetitlecell);


	////start

	PdfPCell blankcel=new PdfPCell();
	blankcel.setBorder(0);
	PdfPCell blankcel1=new PdfPCell();
	blankcel1.setBorder(0);
	invoiceinfotable.addCell(blankcel);
	invoiceinfotable.addCell(blankcel1);
	Phrase typename=null;
	Phrase typevalue=null;


//    rohan comment this code and insert at bottom in footer() method


//	PdfPTable artictable=new PdfPTable(3);
//	artictable.setWidthPercentage(100);
//
//	try {
//		artictable.setWidths(columnWidths2);
//	} catch (DocumentException e2) {
//		// TODO Auto-generated catch block
//		e2.printStackTrace();
//	}

//	for(int i=0;i<this.articletype.size();i++){
//		 
//
//			
//			if(articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().equals("SalesInvoice")){
//				
//				
//				 typename = new Phrase(articletype.get(i).getArticleTypeName(),font8);
//				 typevalue = new Phrase(articletype.get(i).getArticleTypeValue(),font8);
//				 
//				 	PdfPCell tymanecell=new PdfPCell();
//					tymanecell.addElement(typename);
//					tymanecell.setBorder(0);
//					
//					PdfPCell typevalcell=new PdfPCell();
//					typevalcell.addElement(typevalue);
//					typevalcell.setBorder(0);
//					
//					Phrase colun=new Phrase(":",font8);
//					PdfPCell typevalcell1=new PdfPCell();
//					typevalcell1.addElement(colun);
//					typevalcell1.setBorder(0);
//					
//					
//					artictable.addCell(tymanecell);
//					artictable.addCell(typevalcell1);
//					artictable.addCell(typevalcell);
//					
//			 }
//			
//			
//			
//		 }
		

//	PdfPTable blanktable=new PdfPTable(1);
//
//
//	PdfPCell articcell=new PdfPCell(artictable);
//	articcell.setBorder(0);
//
//	PdfPCell articcell1=new PdfPCell(blanktable);
//	articcell1.setBorder(0);
//
//	invoiceinfotable.addCell(articcell);
//	invoiceinfotable.addCell(articcell1);





	////end



	PdfPTable parenttable= new PdfPTable(2);
	parenttable.setWidthPercentage(100);
	try {
		parenttable.setWidths(new float[]{50,50});
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}

	PdfPCell custinfocell = new PdfPCell();
	PdfPCell  invoiceinfocell = new PdfPCell();

	custinfocell.addElement(custtable);
	invoiceinfocell.addElement(invoiceinfotable);
	parenttable.addCell(custinfocell);
	parenttable.addCell(invoiceinfocell);

	try {
		document.add(parenttable);
	} catch (DocumentException e) {
		e.printStackTrace();
	}


		
		
		
	}




	public void createCustInfo()
	{
		Phrase clientname=null;
		
		if(cust.isCompany()==true){
			clientname=new Phrase(cust.getCompanyName().trim(),font10bold);
		}
		else{
			clientname=new Phrase(cust.getFullname().trim(),font10bold);
		}
		PdfPCell custnamecell=new PdfPCell();
		custnamecell.addElement(clientname);
		custnamecell.setBorder(0);
		
	Phrase custaddress1=new Phrase(cust.getAdress().getAddrLine1().trim(),font8);
	Phrase custaddress2=null;
	if(!cust.getAdress().getAddrLine2().equals("")||cust.getAdress().getAddrLine2()!=null){
		custaddress2=new Phrase(cust.getAdress().getAddrLine2().trim(),font8);
	}
	Phrase locality=null;
	if(!cust.getAdress().getLandmark().equals("")||cust.getAdress().getLandmark()!=null){
		locality=new Phrase(cust.getAdress().getLandmark()+" "+cust.getAdress().getLocality().trim()+" "+
	cust.getAdress().getCity().trim()+" "+cust.getAdress().getPin(),font8);
	}

	Phrase contactcust=new Phrase("Mob: "+cust.getCellNumber1()+"    Email:  "+cust.getEmail().trim()+" ",font8);

	PdfPCell custaddr1cell=new PdfPCell();
	custaddr1cell.addElement(custaddress1);
	custaddr1cell.setBorder(0);

	PdfPCell custaddr2cell=null;
	if(custaddress2!=null){
		custaddr2cell=new PdfPCell();
		custaddr2cell.addElement(custaddress2);
		custaddr2cell.setBorder(0);
	}

	PdfPCell custlocalitycell=new PdfPCell();
	custlocalitycell.addElement(locality);
	custlocalitycell.setBorder(0);

	PdfPCell custcontactcell=new PdfPCell();
	custcontactcell.addElement(contactcust);
	custcontactcell.addElement(Chunk.NEWLINE);
	custcontactcell.setBorder(0);

	PdfPTable custtable=new PdfPTable(1);
	custtable.setWidthPercentage(100);
	custtable.addCell(custnamecell);
	custtable.addCell(custaddr1cell);
	if(custaddr2cell!=null){
		custtable.addCell(custaddr2cell);
	}
	custtable.addCell(custlocalitycell);
	custtable.addCell(custcontactcell);


	Phrase billnotitle=new Phrase("Bill No:",font8);
	Phrase realbillno=new Phrase(invoiceentity.getCount()+"",font8); 
	Phrase datetitle=new Phrase("Date:",font8);
	Phrase realdate=new Phrase(fmt.format(invoiceentity.getInvoiceDate())+"",font8);
	Phrase ponotitle=new Phrase("PO No.:",font8);
	Phrase realpono=new Phrase("  ",font8);
	Phrase podatetitle=new Phrase("PO Date:",font8);
	Phrase realpodate=new Phrase(" ",font8);

	PdfPCell billnotitlecell=new PdfPCell();
	billnotitlecell.addElement(billnotitle);
	billnotitlecell.setBorder(0);

	PdfPCell billnocell=new PdfPCell();
	billnocell.addElement(realbillno);
	billnocell.setBorder(0);

	PdfPCell datetitlecell=new PdfPCell();
	datetitlecell.addElement(datetitle);
	datetitlecell.setBorder(0);

	PdfPCell datecell=new PdfPCell();
	datecell.addElement(realdate);
	datecell.setBorder(0);

	PdfPCell ponotitlecell=new PdfPCell();
	ponotitlecell.addElement(ponotitle);
	ponotitlecell.setBorder(0);

	PdfPCell ponocell=new PdfPCell();
	ponocell.addElement(realpono);
	ponocell.setBorder(0);

	PdfPCell podatetitlecell=new PdfPCell();
	podatetitlecell.addElement(podatetitle);
	podatetitlecell.setBorder(0);

	PdfPCell podatecell=new PdfPCell();
	podatecell.addElement(realpodate);
	podatecell.setBorder(0);

	PdfPTable invoiceinfotable=new PdfPTable(4);
	invoiceinfotable.setWidthPercentage(100);
	invoiceinfotable.addCell(billnotitlecell);
	invoiceinfotable.addCell(billnocell);
	invoiceinfotable.addCell(datetitlecell);
	invoiceinfotable.addCell(datecell);
	invoiceinfotable.addCell(ponotitlecell);
	invoiceinfotable.addCell(ponocell);
	invoiceinfotable.addCell(podatetitlecell);
	invoiceinfotable.addCell(podatecell);

	PdfPTable parenttable= new PdfPTable(2);
	parenttable.setWidthPercentage(100);
	try {
		parenttable.setWidths(new float[]{50,50});
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}

	PdfPCell custinfocell = new PdfPCell();
	PdfPCell  invoiceinfocell = new PdfPCell();

	custinfocell.addElement(custtable);
	invoiceinfocell.addElement(invoiceinfotable);
	parenttable.addCell(custinfocell);
	parenttable.addCell(invoiceinfocell);

	try {
		document.add(parenttable);
	} catch (DocumentException e) {
		e.printStackTrace();
	}

	}


	public void createCustomerInformation()
	{
		 Phrase nameTitle = new Phrase("Name :",font8bold);
		 Phrase phoneTitle= new Phrase("Phone :",font8bold);
		 Phrase adressTitle= new Phrase("Address :",font8bold);
		 Phrase emailTitle= new Phrase("Email :",font8bold);
		 
		 PdfPCell titlecustnameCell=new PdfPCell();
		 titlecustnameCell.addElement(nameTitle);
		 titlecustnameCell.setBorder(0);
		 PdfPCell titlecustPhonecell = new PdfPCell();
		 titlecustPhonecell.addElement(phoneTitle);
		 titlecustPhonecell.setBorder(0);
		 PdfPCell titleadressCell = new PdfPCell();
		 titleadressCell.addElement(adressTitle);
		 titleadressCell.setBorder(0);
		 PdfPCell titleemailCell = new PdfPCell();
		 titleemailCell.addElement(emailTitle);
		 titleemailCell.setBorder(0);
		
		 String fullname=cust.getFullname().trim();
		
		Phrase customername = new Phrase(fullname,font8);
		Phrase phonechunk = new Phrase(cust.getCellNumber1()+"",font8);
		Phrase emailchunk = new Phrase(cust.getEmail().trim()+"",font8);
		
		Phrase blankphrase = new Phrase("  ");
		PdfPCell blankcell = new PdfPCell(blankphrase);
		
		Phrase customeradress1= new Phrase(cust.getAdress().getAddrLine1(),font8);
		Phrase customeradress2=null;
		//Patch
		if(cust.getAdress().getAddrLine2()!=null)
		   customeradress2= new Phrase(cust.getAdress().getAddrLine2(),font8);
		
		Phrase locality= null;
		
		if(cust.getAdress().getLandmark()!=null)
		{
		    locality=new Phrase(cust.getAdress().getLandmark()+" "+cust.getAdress().getLocality()+" "+cust.getAdress().getCity()+" "+cust.getAdress().getPin(),font8);
		}
		else{
			locality=new Phrase(cust.getAdress().getLocality()+" "+cust.getAdress().getCity()+" "+cust.getAdress().getPin(),font8);
		}
		
		Phrase blnkphrase=new Phrase(" ");
		 PdfPCell blnkcell=new PdfPCell();
		 blnkcell.addElement(blnkphrase);
		 blnkcell.setBorder(0);
		 PdfPCell custnameCell=new PdfPCell();
		 custnameCell.addElement(customername);
		 custnameCell.setBorder(0);
		 PdfPCell custPhonecell = new PdfPCell();
		 custPhonecell.addElement(phonechunk);
		 custPhonecell.setBorder(0);
		 PdfPCell adress1Cell = new PdfPCell();
		 adress1Cell.addElement(customeradress1);
		 adress1Cell.setBorder(0);
		 PdfPCell adress2Cell= new PdfPCell();
		 adress2Cell.addElement(customeradress2);
		 adress2Cell.setBorder(0);
		 PdfPCell localitycell = new PdfPCell();
		 localitycell.addElement(locality);
		 localitycell.setBorder(0);
		 PdfPCell emailcell = new PdfPCell();
		 emailcell.addElement(emailchunk);
		 emailcell.setBorder(0);
		
		String header;
		header = "Bill No :";
		
		Phrase billno = new Phrase(header,font8bold);
		Phrase invoicedate= new Phrase("Date :",font8bold);
		Phrase ponumber= new Phrase("PO No. :",font8bold);
		Phrase podate= new Phrase("PO Date:",font8bold);
		
		 PdfPCell titlebillCell=new PdfPCell();
		 titlebillCell.addElement(billno);
		 titlebillCell.setBorder(0);
		 PdfPCell titledatecell = new PdfPCell();
		 titledatecell.addElement(invoicedate);
		 titledatecell.setBorder(0);
		 PdfPCell titleponumberCell = new PdfPCell();
		 titleponumberCell.addElement(ponumber);
		 titleponumberCell.setBorder(0);
		 PdfPCell titlepodateCell = new PdfPCell();
		 titlepodateCell.addElement(podate);
		 titlepodateCell.setBorder(0);
		 
		Phrase realqno=null;
	    realqno = new Phrase(invoiceentity.getCount()+"",font8);
		
		String date=fmt.format(invoiceentity.getInvoiceDate());
		Phrase realorederdate= new Phrase(date+"",font8);
		realorederdate.trimToSize();
		Phrase realponumber= new Phrase("N.A",font8);
		Phrase realpodate= new Phrase("N.A",font8);
		
		PdfPCell billCell=new PdfPCell();
		billCell.addElement(realqno);
		PdfPCell datecell = new PdfPCell();
		datecell.addElement(realorederdate);
		PdfPCell durationCell = new PdfPCell();
		durationCell.addElement(realponumber);
		PdfPCell noServicesCell = new PdfPCell();
		noServicesCell.addElement(realpodate);
	     
		
		 PdfPTable table = new PdfPTable(2);
		 
		 // 1 st row name phone
		 table.addCell(titlecustnameCell);
		 table.addCell(custnameCell);
		 
		 table.addCell(titlecustPhonecell);
		 table.addCell(custPhonecell);
		 
		 // 2nd row adress and email
		 table.addCell(titleadressCell);
		 table.addCell(adress1Cell);
		 if(customeradress2!=null){
			 table.addCell(blnkcell);
			 table.addCell(adress2Cell);
		 }
		 table.addCell(blnkcell);
		 table.addCell(localitycell);
		 table.addCell(titleemailCell);
		 table.addCell(emailcell);
		 
		try {
			table.setWidths(new float[]{20,80});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.setWidthPercentage(100);
		 
		PdfPTable billtable = new PdfPTable(4);
		try {
			billtable.setWidths(new float[]{20,30,20,30});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		billtable.setWidthPercentage(100);
		billtable.setHorizontalAlignment(Element.ALIGN_LEFT);

		//1  row bill number and date
		 billtable.addCell(titlebillCell);
		 billtable.addCell(billCell);
		 billtable.addCell(titledatecell);
		 billtable.addCell(datecell);
		 
		//2  row duration   and start date
		 billtable.addCell(titleponumberCell);
		 billtable.addCell(durationCell);
		 billtable.addCell(titlepodateCell);
		 billtable.addCell(noServicesCell);

		 
		PdfPTable parenttable= new PdfPTable(2);
		parenttable.setWidthPercentage(100);
		try {
			parenttable.setWidths(new float[]{50,50});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPCell custinfocell = new PdfPCell();
		PdfPCell  billinfocell = new PdfPCell();
		
		custinfocell.addElement(table);
		billinfocell.addElement(billtable);
		parenttable.addCell(custinfocell);
		parenttable.addCell(billinfocell);

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}



	  public  void createHeaderTable() throws DocumentException
		{
			 Phrase nameTitle = new Phrase("Name :",font8bold);
			 Phrase phoneTitle= new Phrase("Phone :",font8bold);
			 Phrase adressTitle= new Phrase("Adress :",font8bold);
			 Phrase emailTitle= new Phrase("Email :",font8bold);
			 
			 PdfPCell titlecustnameCell=new PdfPCell();
			 titlecustnameCell.addElement(nameTitle);
			 PdfPCell titlecustPhonecell = new PdfPCell();
			 titlecustPhonecell.addElement(phoneTitle);
			 PdfPCell titleadressCell = new PdfPCell();
			 titleadressCell.addElement(adressTitle);
			 PdfPCell titleemailCell = new PdfPCell(emailTitle);
			 titleemailCell.addElement(emailTitle);
			
			 String fullname=cust.getFullname() ;
			
			Phrase customername = new Phrase(fullname,font8);
			Phrase phonechunk = new Phrase(cust.getCellNumber1()+"",font8);
			Phrase emailchunk = new Phrase(cust.getEmail()+"",font8);
			
			Phrase blankphrase = new Phrase("  ");
			PdfPCell blankcell = new PdfPCell(blankphrase);
			
			Phrase customeradress= new Phrase(cust.getAdress().getAddrLine1(),font8);
			Phrase customeradress2=null;
			//Patch
			if(cust.getAdress().getAddrLine2()!=null)
			   customeradress2= new Phrase(cust.getAdress().getAddrLine2(),font8);
			Phrase landmark=null;
			Phrase locality= null;
			
			
			if(cust.getAdress().getLandmark()!=null)
			{
			    landmark = new Phrase(cust.getAdress().getLandmark());
			    locality=new Phrase(landmark+" "+cust.getAdress().getLocality(),font8);
			}
			else
				locality=new Phrase(cust.getAdress().getLocality(),font8);
			
			Phrase cityState=new Phrase(cust.getAdress().getCity()
					+" "+cust.getAdress().getState(),font8);
			Phrase country=new Phrase("  "+cust.getAdress().getCountry(),font8);
			Phrase pin=new Phrase("  "+cust.getAdress().getPin()+"",font8);
			 
			
			Paragraph adresspara = new Paragraph();
			 adresspara.add(customeradress);
			 adresspara.add(Chunk.NEWLINE);
			 if(customeradress2!=null)
				 adresspara.add(customeradress2);
			 adresspara.add(locality);
			
			adresspara.add(cityState);
			
			adresspara.add(country);
			adresspara.add(pin);
			 
			 PdfPCell custnameCell=new PdfPCell();
			 custnameCell.addElement(customername);
			 PdfPCell custPhonecell = new PdfPCell();
			 custPhonecell.addElement(phonechunk);
			 PdfPCell adressCell = new PdfPCell();
			 adressCell.addElement(adresspara);
			 PdfPCell emailCell = new PdfPCell(emailTitle);
			 emailCell.addElement(emailchunk);
			
			String header;
			header = "ID     :";
			
			Phrase idphrase = new Phrase(header,font8bold);
			Phrase invdate= new Phrase("Date :",font8bold);
			Phrase ponumber= new Phrase("PO No :",font8bold);
			Phrase podate= new Phrase("PO Date:",font8bold);
			
			 PdfPCell titlebillCell=new PdfPCell();
			 titlebillCell.setBorder(0);
			 titlebillCell.addElement(idphrase);
			 PdfPCell titledatecell = new PdfPCell();
			 titledatecell.setBorder(0);
			 titledatecell.addElement(invdate);
			 PdfPCell titleponumberCell = new PdfPCell();
			 titleponumberCell.setBorder(0);
			 titleponumberCell.addElement(ponumber);
			 PdfPCell titlepodateCell = new PdfPCell();
			 titlepodateCell.setBorder(0);
			 titlepodateCell.addElement(podate);
			 
			Phrase realinvno=null;
		    realinvno = new Phrase(invoiceentity.getCount()+"",font8);
			
			String date=fmt.format(invoiceentity.getInvoiceDate());
			Phrase realorederdate= new Phrase(date+"",font8);
			realorederdate.trimToSize();
			Phrase relponumber= new Phrase("N.A.",font8);
			Phrase relpodate= new Phrase("N.A.",font8);
			
		    PdfPCell billCell=new PdfPCell();
		    billCell.setBorder(0);
			billCell.addElement(realinvno);
			PdfPCell dateCell = new PdfPCell();
			dateCell.setBorder(0);
			dateCell.addElement(realorederdate);
			PdfPCell ponumCell = new PdfPCell();
			ponumCell.setBorder(0);
			ponumCell.addElement(relponumber);
			PdfPCell podateCell = new PdfPCell();
			podateCell.setBorder(0);
			podateCell.addElement(relpodate);
			
			 PdfPTable table = new PdfPTable(4);
			 
			 // 1 st row name phone
			 table.addCell(titlecustnameCell);
			 table.addCell(custnameCell);
			 
			 table.addCell(titlecustPhonecell);
			 table.addCell(custPhonecell);
			 
			 
			 
			 // 2nd row adress and email
			 table.addCell(titleadressCell);
			 table.addCell(adressCell);
			 
			 table.addCell(titleemailCell);
			 table.addCell(emailCell);
			 
			table.setWidths(new float[]{18,32,18,32});
			table.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.setWidthPercentage(100);
			 
			PdfPTable billtable = new PdfPTable(4);
			billtable.setWidths(new float[]{20,30,20,30});
			billtable.setWidthPercentage(100);
			billtable.setHorizontalAlignment(Element.ALIGN_LEFT);

			//1  row bill number and date
			 billtable.addCell(titlebillCell);
			 billtable.addCell(billCell);
			 billtable.addCell(titledatecell);
			 billtable.addCell(dateCell);
			 
			//2  row duration   and start date
			 billtable.addCell(titleponumberCell);
			 billtable.addCell(ponumCell);
			 billtable.addCell(titlepodateCell);
			 billtable.addCell(podateCell);
			 
			 for(int i=0;i<2;i++)
			 {
				PdfPRow temp=table.getRow(i);
				PdfPCell[] cells= temp.getCells();
				for(PdfPCell cell:cells)
				cell.setBorder(Rectangle.NO_BORDER);
			}
			 
			/* for(int i=0;i<row;i++)
			 {
				 PdfPRow temp=billtable.getRow(i);
				PdfPCell[] cells= temp.getCells();
				for(PdfPCell cell:cells)
					cell.setBorder(Rectangle.NO_BORDER);
			}
			 */
			PdfPTable parenttable= new PdfPTable(2);
			parenttable.setWidthPercentage(100);
			parenttable.setWidths(new float[]{50,50});
			
			PdfPCell custinfocell = new PdfPCell();
			PdfPCell  billinfocell = new PdfPCell();
			
			custinfocell.addElement(table);
			billinfocell.addElement(billtable);
			parenttable.addCell(custinfocell);
			parenttable.addCell(billinfocell);
		
			document.add(parenttable);
		}
	  
	  
	  public  void createProductTable()
	  {
			Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
			Phrase productdetails= new Phrase("Product Details",font1);
			Paragraph para = new Paragraph();
			para.add(Chunk.NEWLINE);
			para.add(Chunk.NEWLINE);
			para.add(productdetails);
			para.setAlignment(Element.ALIGN_CENTER);
			para.add(Chunk.NEWLINE);
				
			PdfPTable table = new PdfPTable(8);
			table.setWidthPercentage(100);
			
			Phrase category = new Phrase("SR. NO. ",font1);
//			Phrase productcode = new Phrase("ITEM DETAILS",font1);
	        Phrase productname = new Phrase("ITEM DETAILS",font1);
	        Phrase qty = new Phrase("QTY",font1);
	        Phrase unit = new Phrase("UNIT",font1);
	        Phrase rate= new Phrase ("RATE",font1);
	        
	        Phrase percDisc = new Phrase("DISC %",font1);
	        Phrase adddisc = new Phrase("ADDL.DISC %",font1);
	        
	        Phrase servicetax = new Phrase("TAX",font1);
//	        Phrase vat = new Phrase("VAT",font1);
	        
	        
	        
	        
	        Paragraph tax= new Paragraph();
		       //*************chnges mukesh on 22/4/2015******************** 
//		        SalesQuotation salesQu=null;
//		        if(qp instanceof SalesQuotation){
//		        	salesQu=(SalesQuotation)qp;
//		        }
		        Phrase svat=null;
		        Phrase vvat=null;
		        Phrase vat=null;
		        int cout=0;
		        int flag21=0;
		        int st=0;
		        int st1=0;
//		        for(int i=0;i<this.prodTaxes.size();i++){
//		        	
//		        }
		        
		        
		      
		        for(int i=0;i<this.salesProd.size();i++) {
		        	 
		        	int cstFlag=0;
		        	if(invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
		        		if(salesOrderEntity.getCformstatus()!=null){
		        		if(salesOrderEntity.getCformstatus().trim().equals(AppConstants.YES)||salesOrderEntity.getCformstatus().trim().equals(AppConstants.NO))
		        		{
		        			cstFlag=1;
		        		}
		        		}
		        	}
		        	
		        	if(invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
		        		if(purchaseOrder.getcForm()!=null){
		        		if(purchaseOrder.getcForm().trim().equals(AppConstants.YES)||purchaseOrder.getcForm().trim().equals(AppConstants.NO))
		        		{
		        			cstFlag=1;
		        		}
		        		}
		        	}
		        	
		        	
//		        	Phrase vatphrase=new Phrase("VAT",font1);
//		             Phrase Cstphrase=new Phrase("CST",font1);
//		             Phrase Stphrase=new Phrase("ST",font1);
		        	
		        	
		             
//		             if(salesProd.get(i).getServiceTax().getPercentage()>0&&salesProd.get(i).getVatTax().getPercentage()>0&&cstFlag==0){
//		            	 vvat = new Phrase("VAT / ST %",font1);
//		            	 cout=1;
//		             }
//		            else if(salesProd.get(i).getServiceTax().getPercentage()>0&&salesProd.get(i).getVatTax().getPercentage()==0&&cstFlag==0){
//		         	    vat = new Phrase("ST %",font1);
//		         	   st=1;
//		            }
//		            else if(salesProd.get(i).getServiceTax().getPercentage()==0&&salesProd.get(i).getVatTax().getPercentage()>0&&cstFlag==0){
//		            	 vat = new Phrase("VAT %",font1);
//		            	 st1=1;
//		            }else if(cstFlag==1 &&salesProd.get(i).getServiceTax().getPercentage()>0){
//		            	svat = new Phrase("CST / ST %",font1);
//		            	flag21=1;
//		            } 
//		            else if(cstFlag==1 &&salesProd.get(i).getServiceTax().getPercentage()==0){
//		         	    vat = new Phrase("CST %",font1);
//		        
//		            }
//		            else{
//		                	 vat = new Phrase("TAX %",font1);
//		                }
		        	
		        	/**
		        	 * Date 31-08-2017 above old code commented below new code added by vijay for PO and sales PDF
		        	 */
		        	
		             if(salesProd.get(i).getServiceTax().getPercentage()>0&&salesProd.get(i).getVatTax().getPercentage()>0&&cstFlag==0){
	            	 vvat = new Phrase("TAX 1 / TAX 2 %",font1);
	            	 cout=1;
		             }
		            else if(salesProd.get(i).getServiceTax().getPercentage()>0&&salesProd.get(i).getVatTax().getPercentage()==0&&cstFlag==0){
		         	    vat = new Phrase("TAX 1 %",font1);
		         	   st=1;
		            }
		            else if(salesProd.get(i).getServiceTax().getPercentage()==0&&salesProd.get(i).getVatTax().getPercentage()>0&&cstFlag==0){
		            	 vat = new Phrase("TAX 1 %",font1);
		            	 st1=1;
		            }else if(cstFlag==1 &&salesProd.get(i).getServiceTax().getPercentage()>0){
		            	svat = new Phrase("TAX 1 / TAX 2 %",font1);
		            	flag21=1;
		            } 
		            else if(cstFlag==1 &&salesProd.get(i).getServiceTax().getPercentage()==0){
		         	    vat = new Phrase("TAX 1 %",font1);
		        
		            }
		            else{
		                	 vat = new Phrase("TAX %",font1);
		                }		             
		             
		        }
		        Phrase stvat=null;
		        if(cout>0){
		        	tax.add(vvat);
		        	tax.setAlignment(Element.ALIGN_CENTER);
		        }else if(st==1&&st1==1){
		         	
		         	stvat=new Phrase("TAX 1 / TAX 2 %",font1);
		         	tax.add(stvat);
		         	tax.setAlignment(Element.ALIGN_CENTER);
		         }else if(flag21>0){
		        	tax.add(svat);
		        	tax.setAlignment(Element.ALIGN_CENTER);
		        }else{
		        tax.add(vat);
		        tax.setAlignment(Element.ALIGN_CENTER);
		        }
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        Phrase total = new Phrase("AMOUNT",font1);
			
	        
			 PdfPCell cellcategory = new PdfPCell(category);
			 cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
	         PdfPCell cellproductname = new PdfPCell(productname);
	         cellproductname.setHorizontalAlignment(Element.ALIGN_CENTER);
	         PdfPCell cellqty = new PdfPCell(qty);
	         cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);
	         PdfPCell cellunit = new PdfPCell(unit);
	         cellunit.setHorizontalAlignment(Element.ALIGN_CENTER);
	         PdfPCell cellrate = new PdfPCell(rate);
	         cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
	         PdfPCell percDisccell = new PdfPCell(percDisc);
	         percDisccell.setHorizontalAlignment(Element.ALIGN_CENTER);
	         PdfPCell cellservicetax = new PdfPCell(tax);
	         cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
	         
	         PdfPCell celltotal= new PdfPCell(total);
	         celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);
	         
	         table.addCell(cellcategory);
	         table.addCell(cellproductname);
	         table.addCell(cellqty);
	         table.addCell(cellunit);
	         table.addCell(cellrate);
	         table.addCell(percDisccell);
	         table.addCell(cellservicetax);
	         
	         table.addCell(celltotal);
	         
	         try {
				table.setWidths(columnWidths);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
//	         ///change dummy this.products.size() to eva
//	         
//	        if( eva<= firstBreakPoint){
//	  			int size =  firstBreakPoint - eva;
//	  		    blankLines = size*(185/19);
//	  		}
//	  		else{
//				blankLines = 0;
//			}
//	  		table.setSpacingAfter(blankLines);	 
	         
	         

	      	 if( this.salesProd.size()<= firstBreakPoint){
	 			int size =  firstBreakPoint - this.salesProd.size();
	 			      blankLines = size*(140/14);
	 			  	System.out.println("blankLines size ="+blankLines);
	 			System.out.println("blankLines size ="+blankLines);
	 				}
	 				else{
	 					blankLines = 10f;
	 				}
	 				table.setSpacingAfter(blankLines);	
	         
	 				Phrase rephrse=null;
	 				
	 				
	 				
	 				 int flagr=0;
	 				 
	         for(int i=0;i<this.salesProd.size();i++)
	         {
	        	 Phrase chunk=new Phrase((i+1)+"",font8);
	        	 PdfPCell pdfcategcell = new PdfPCell(chunk);
	        	 pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        	 
	        	 
	        	 chunk = new Phrase(salesProd.get(i).getProdName(),font8);
	        	 PdfPCell pdfnamecell = new PdfPCell(chunk);
	        	 
	        	 chunk = new Phrase(salesProd.get(i).getQuantity()+"",font8);
	        	 PdfPCell pdfqtycell = new PdfPCell(chunk);
	        	 pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        	 
	        	 chunk = new Phrase(salesProd.get(i).getUnitOfMeasurement(),font8);
	        	 PdfPCell pdfunitcell = new PdfPCell(chunk);
	        	 pdfunitcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        	 
	        	 chunk = new Phrase(df.format(salesProd.get(i).getPrice()),font8);
	        	 PdfPCell pdfspricecell = new PdfPCell(chunk);
	        	 pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        	 
//	        	 if(salesProd.get(i).getServiceTax()!=null)
//	         	    chunk = new Phrase(salesProd.get(i).getServiceTax().getPercentage()+"",font8);
//	         	 else
//	         		 chunk = new Phrase("N.A"+"",font8);
//	         	 
//	         	 PdfPCell pdfservice = new PdfPCell(chunk);
//	         	pdfservice.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	         	 if(salesProd.get(i).getVatTax()!=null)
//	         	    chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+"",font8);
//	         	 else
//	         		 chunk = new Phrase("N.A"+"",font8);
//	         	 PdfPCell pdfvattax = new PdfPCell(chunk);
	         	 
	         	 if(salesProd.get(i).getProdPercDiscount()!=0)
	          	    chunk = new Phrase(salesProd.get(i).getProdPercDiscount()+"",font8);
	          	 else
	          		 chunk = new Phrase("0"+"",font8);
	          	 PdfPCell pdfperdiscount = new PdfPCell(chunk);
	          	pdfperdiscount.setHorizontalAlignment(Element.ALIGN_RIGHT);
	         	
	          	/////
	          	
	          	PdfPCell pdfservice1=null;
	        	 //******************************
	        	 double cstval=0;
	        	 int cstFlag=0;
	         	if(invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
	         		if(salesOrderEntity.getCformstatus()!=null){
	         		if(salesOrderEntity.getCformstatus().trim().equals(AppConstants.YES)||salesOrderEntity.getCformstatus().trim().equals(AppConstants.NO))
	         		{
	         			cstFlag=1;
	         			cstval=salesOrderEntity.getCstpercent();
	         		}
	         		}
	         	}
	         	
	         	if(invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
	         		if(purchaseOrder.getcForm()!=null){
	         		if(purchaseOrder.getcForm().trim().equals(AppConstants.YES)||purchaseOrder.getcForm().trim().equals(AppConstants.NO))
	         		{
	         			cstFlag=1;
	         			cstval=purchaseOrder.getCstpercent();
	         		}
	         		}
	         	}
	         	
	        	 
	        	 
	         	 System.out.println("one");
	        	 
	        	  if((salesProd.get(i).getVatTax().getPercentage()>0)&& (salesProd.get(i).getServiceTax().getPercentage()>0)&&(cstFlag==0)){
	              	   
	        		  System.out.println("two");
	        		  flagr=1;
	        		  if((salesProd.get(i).getVatTax()!=null)&&(salesProd.get(i).getServiceTax()!=null)){
	        			  if(cout>0){
	            			  chunk = new Phrase(df.format(salesProd.get(i).getVatTax().getPercentage())+" / "+df.format(salesProd.get(i).getServiceTax().getPercentage()),font8);
	            			  }
	        		  }else{
	                		
	        			  chunk = new Phrase("0"+"",font8);
	        		  }
	        			  pdfservice1 = new PdfPCell(chunk);
	           		 
	           		  
	           		  pdfservice1 = new PdfPCell(chunk);
	                	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        		  
	        		  
	        		  
	        	  }
	             else if((salesProd.get(i).getServiceTax().getPercentage()>0) && (salesProd.get(i).getVatTax().getPercentage()==0&&cstFlag==0)){
	            	
	            	 System.out.println("three");
	            	 if(salesProd.get(i).getServiceTax()!=null){
	            		if(flag21==1||cout==1){
	            				chunk = new Phrase(df.format(salesProd.get(i).getServiceTax().getPercentage())+" / "+"0.00",font8);
	            		}else if(st==1&&st1==1){
	            			chunk = new Phrase("0.00"+" / "+df.format(salesProd.get(i).getServiceTax().getPercentage()),font8);
	            		}else{
	            				chunk = new Phrase(df.format(salesProd.get(i).getServiceTax().getPercentage()),font8);
	            		}
	            		
	            	 }else{
	             		 chunk = new Phrase("0.00",font8);
	             	 }
	             	  pdfservice1 = new PdfPCell(chunk);
	             	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	             	 
	             }
	             else if((salesProd.get(i).getServiceTax().getPercentage()>0) &&(salesProd.get(i).getVatTax().getPercentage()>0)&&cstFlag==1){
	            	 System.out.println("four");
	            	
	            	 		
	            	 	chunk = new Phrase(df.format(cstval)+" / "+df.format(salesProd.get(i).getServiceTax().getPercentage()),font8);
	            		 
	            		 pdfservice1 = new PdfPCell(chunk);
	                 	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	            	 
	                 	 flagr=1;
	            	
	             }
	             else if(cstFlag==1&&(salesProd.get(i).getServiceTax().getPercentage()==0)
	            		 &&(salesProd.get(i).getVatTax().getPercentage()==0)){
	            	 
	            	 System.out.println("five");
	            	 if(flag21==1||cout==1){
	            		 chunk = new Phrase(df.format(cstval)+" / "+"0.00",font8);
	            	 }else{
	            		 chunk = new Phrase(df.format(cstval),font8);
	            	 }
	            		 
	            		 
	            		 pdfservice1 = new PdfPCell(chunk);
	                 	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	            	 
	             } 
	             else if(cstFlag==1&&salesProd.get(i).getServiceTax().getPercentage()>0){
	          	  
	            	 System.out.println("six");
	            	 flagr=1;
	            	 chunk = new Phrase(df.format(cstval)+" / "+
	            			 df.format(salesProd.get(i).getServiceTax().getPercentage()),font8);
	            		 
	            		 pdfservice1 = new PdfPCell(chunk);
	                 	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	            	 
	             } 
	             else if((cstFlag==1&&(salesProd.get(i).getServiceTax().getPercentage()==0)&&(salesProd.get(i).getVatTax().getPercentage()>0))){
	          	   
	            	 System.out.println("seven");
	            	 if(flag21==1||cout==1){
	            		 chunk = new Phrase(df.format(cstval)+" / "+"0.00",font8);
	            	 }else{
	            		 chunk = new Phrase(df.format(cstval),font8);
	            	 }
	            	
	      			  
//	      		 chunk = new Phrase("0"+"",font8);
	      	  pdfservice1 = new PdfPCell(chunk);
	      	  
	       	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	            	 
	             }else if((salesProd.get(i).getServiceTax().getPercentage()==0)&&(salesProd.get(i).getVatTax().getPercentage()>0)&&(cstFlag==0)){
	            	 System.out.println("eight");
	            	 if(flag21==1||cout==1){
	            		 chunk = new Phrase(df.format(salesProd.get(i).getVatTax().getPercentage())+" / "+"0.00",font8);
	            	 }else if(st==1&&st1==1){
	            		 chunk = new Phrase(df.format(salesProd.get(i).getVatTax().getPercentage())+" / "+"0.00",font8);
	            	 }else{
	            		 chunk = new Phrase(df.format(salesProd.get(i).getVatTax().getPercentage()),font8);
	            	 }
	            		 pdfservice1 = new PdfPCell(chunk);
	                 	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	                 	 
	                 	 System.out.println("service tax flagr:::::::::::::   "+flagr);
		  
	             }else{
	            	 if(flag21==1|| cout==1){
	            		 chunk= new Phrase("0.00 / 0.00",font8);
	            	 }else{
	            	 chunk = new Phrase("0.00",font8);
	            	 }
	            	 pdfservice1 = new PdfPCell(chunk);
	             }
	          	/////
	        	 chunk = new Phrase(df.format(salesProd.get(i).getTotalAmount()),font8);
	        	 PdfPCell pdftotalproduct = new PdfPCell(chunk);
	        	 pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        	 
	        	 
	        	 table.addCell(pdfcategcell);
	        	 table.addCell(pdfnamecell);
	        	 table.addCell(pdfqtycell);
	        	 table.addCell(pdfunitcell);
	        	 table.addCell(pdfspricecell);
	        	 table.addCell(pdfperdiscount);
	        	 pdfservice1.setHorizontalAlignment(Element.ALIGN_CENTER);
	        	 table.addCell(pdfservice1);
	        	 
	        	 table.addCell(pdftotalproduct);
	        	 try {
					table.setWidths(columnWidths);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        	 
	        	 
	        	 count=i;
					
					
		        	if(count==this.salesProd.size()|| count==firstBreakPoint){
								
		        		
		        		
		        		 rephrse=new Phrase("Refer Annexure 1 for additional products ",font8);
		        		
		        		
								flag=firstBreakPoint;
								break;
					}
						
					
					
					System.out.println("flag value" + flag);
					if(firstBreakPoint==flag){
						
						termsConditionsInfo();
						addFooter();
						
						
						}
	        	 
	        	 
//	        	 count= i;
//	        	 
//	        	 ///change this.salesProd.size() to eva.
//				 
//	 			if(count==eva|| count==firstBreakPoint){
//	 						
//	 						flag=firstBreakPoint;
//	 						break;
//	 			}
//	 			if(firstBreakPoint==flag){
//	 				
//	 				termsConditionsInfo();
//	 				addFooter();
//	 				}
//	         	}
	        	////
	         
//	         Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
//	      	Phrase category = new Phrase("Category ", font1);
//	      	Phrase qty = new Phrase("Quantity", font1);
//	      	Phrase servicetax = new Phrase("Service Tax", font1);
//	      	Phrase vat = new Phrase("VAT", font1);
//	      	Phrase percdisc = new Phrase("% Discount", font1);
	// 
//	      	PdfPCell cellcategory = new PdfPCell(category);
//	      	PdfPCell cellqty = new PdfPCell(qty);
//	      	PdfPCell cellperdisc = new PdfPCell(percdisc);
//	      	PdfPCell cellvat = new PdfPCell(vat);
//	      	PdfPCell cellservicetax = new PdfPCell(servicetax);
	// 
//	      	table.addCell(cellcategory);
//	      	table.addCell(cellqty);
//	      	table.addCell(cellperdisc);
//	      	table.addCell(cellvat);
//	      	table.addCell(cellservicetax);
	  		
	  		
//	      	 if( eva<= firstBreakPoint){
//	 			int size =  firstBreakPoint - eva;
//	 			      blankLines = size*(185/18);
//	 			  	System.out.println("blankLines size ="+blankLines);
//	 			System.out.println("blankLines size ="+blankLines);
//	 				}
//	 				else{
//	 					blankLines = 0;
//	 				}
//	 				table.setSpacingAfter(blankLines);	
//	      	
//	      	
//	  		
//	   				
//	   				for(int i =0;i<eva;i++){
//	   				if(getdata().get(i).getProductCategory()!=null){
//	  					Phrase chunk = new Phrase(getdata().get(i).getProductCategory(),font8);
//	  					
//	  					}else{
//	  					}
//	  					PdfPCell pdfcategcell = new PdfPCell(chunk);
//	  		
//	  					
//	  					
//	  					
//	  					if(getdata().get(i).getProductQuantity()!=0){
//	  					chunk = new Phrase(getdata().get(i).getProductQuantity() + "", font8);
//	  					}
//	  					else{
//	  						chunk = new Phrase("", font8);
//	  					}
//	  					PdfPCell pdfqtycell = new PdfPCell(chunk);
//	  					
//	  					
//	  					
//	  					if(getdata().get(i).getTax()!=0){
//	  						chunk = new Phrase(getdata().get(i).getTax() + "", font8);
//	  					}else{
//	  						chunk = new Phrase("", font8);
//	  					}
//	  					PdfPCell pdfservice = new PdfPCell(chunk);
//	  		
//	  					
//	  					
//	  					
//	  					if(getdata().get(i).getVat()!=0){
//	  					
//	  						chunk = new Phrase(getdata().get(i).getVat() + "", font8);
//	  					}else{
//	  						chunk = new Phrase( "", font8);
//	  					}
//	  					PdfPCell pdfvattax = new PdfPCell(chunk);
//	  					
//	  					
//	  					
//	  					
//	  					if(getdata().get(i).getDiscount()!=0)
//	  					{
//	  						chunk = new Phrase(getdata().get(i).getDiscount() + "", font8);
//	  				
//	  						}else{
//	  							chunk = new Phrase(getdata().get(i).getDiscount() + "", font8);
//	  						}	
//	  					
//	  					PdfPCell pdfperdiscount = new PdfPCell(chunk);
//	  		
//	  						table.addCell(pdfcategcell);
//	  						table.addCell(pdfqtycell);
//	  						table.addCell(pdfperdiscount);
//	  						table.addCell(pdfvattax);
//	  						table.addCell(pdfservice);
	  						
	  						
//	  						count=i;
//	 						
//	 						
//	  			        	if(count==eva|| count==firstBreakPoint){
//	  									
//	  									flag=firstBreakPoint;
//	  									break;
//	  						}
//	  							
//	  						
//	  						
//	  						System.out.println("flag value" + flag);
//	  						if(firstBreakPoint==flag){
//	  							
//	  							termsConditionsInfo();
//	  							addFooter();
//	  							
//	  							
//	  							}
	         
	         
	         ////
	         
	    }
	   				
	   				
	   				PdfPTable parentTableProd=new PdfPTable(1);
	   	         parentTableProd.setWidthPercentage(100);
	   	         
	   	         PdfPCell prodtablecell=new PdfPCell();
	   	         prodtablecell.addElement(table);
	   	      prodtablecell.addElement(rephrse);
	   	         parentTableProd.addCell(prodtablecell);
	   	         
	   	         
	   	         try {
	   				document.add(parentTableProd);
	   			} catch (DocumentException e) {
	   				e.printStackTrace();
	   			}
	  }
	  
	  public void termsConditionsInfo()
		{
		  Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
		  Phrase blankval=new Phrase(" ",font8);
		  Phrase payphrase=new Phrase("Payment Terms :",font10bold);
			payphrase.add(Chunk.NEWLINE);
			PdfPTable table = new PdfPTable(3);
			table.setWidthPercentage(100);
//			table.setSpacingAfter(50f);
			
			PdfPCell tablecell1 = new PdfPCell();
			 if(this.payTermsLis.size()>4){
				 tablecell1= new PdfPCell(table);
//				 tablecell1.setBorder(0);
			        }else{
			        	tablecell1= new PdfPCell(table);
			        	tablecell1.setBorder(0);
			        }
			
			
			
			
			Phrase paytermdays = new Phrase("Days",font1);
			Phrase paytermpercent = new Phrase("Percent",font1);
	      Phrase paytermcomment = new Phrase("Comment",font1);
	    
	      PdfPCell headingpayterms = new PdfPCell(payphrase);
	      headingpayterms.setBorder(0);
	      headingpayterms.setColspan(3);
		    PdfPCell celldays = new PdfPCell(paytermdays);
		    celldays.setBorder(0);
		    celldays.setHorizontalAlignment(Element.ALIGN_CENTER);
			PdfPCell cellpercent = new PdfPCell(paytermpercent);
			cellpercent.setBorder(0);
			cellpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
		    PdfPCell cellcomment = new PdfPCell(paytermcomment);
		    cellcomment.setBorder(0);
		    cellcomment.setHorizontalAlignment(Element.ALIGN_CENTER);
//			table.addCell(headingpayterms);
	      table.addCell(celldays);
	      table.addCell(cellpercent);
	      table.addCell(cellcomment);
		  
	      
	      if( this.payTerms.size()<= BreakPoint){
				int size =  BreakPoint - this.payTerms.size();
				      blankLines = size*(80/5);
				  	System.out.println("blankLines size ="+blankLines);
				System.out.println("blankLines size ="+blankLines);
					}
					else{
						blankLines = 0;
					}
					table.setSpacingAfter(blankLines);	
	      
	      System.out.println("this.payTerms.size()"+this.payTerms.size());
	      for(int i=0;i<this.payTerms.size();i++)
	      {
	      	System.out.println(this.payTermsLis.size()+"  in for1");
//	     	 Phrase chunk=new Phrase("Day: "+payTermsLis.get(i).getPayTermDays()+"                "+"Percent: "+payTermsLis.get(i).getPayTermPercent()+"               "+"Comment: "+payTermsLis.get(i).getPayTermComment().trim(),font8);
//	     	 PdfPCell pdfdayscell = new PdfPCell(chunk);
//	     	 pdfdayscell.setBorder(0);
	    	
	     	 	Phrase chunk = new Phrase(payTerms.get(i).getPayTermDays()+"",font8);
	     	 	PdfPCell pdfdayscell = new PdfPCell(chunk);
	     	 	pdfdayscell.setBorder(0);
	     	  
	     	 pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
	     	 	
	     	 chunk=new Phrase(payTerms.get(i).getPayTermPercent()+"",font8);
	     	 PdfPCell pdfpercentcell = new PdfPCell(chunk);
	     	 pdfpercentcell.setBorder(0);
	     	pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	     	 chunk = new Phrase(payTerms.get(i).getPayTermComment().trim(),font8);
	     	 PdfPCell pdfcommentcell = new PdfPCell(chunk);
	     	 pdfcommentcell.setBorder(0);
	     	pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	     
	     	table.addCell(pdfdayscell);
	     	table.addCell(pdfpercentcell);
	     	table.addCell(pdfcommentcell);
	     	
	     	
	     	
	     	count=i;
	    	 ///
	    	 if(count==4|| count==BreakPoint){
	 				
	 				flag1=BreakPoint;
	 				break;
	 	}
	     	
	     	
	     	}
		  
	    ///2nd table for pay terms start
	      System.out.println(this.payTerms.size()+"  out");
	      
//	      if(this.payTermsLis.size()>4){
	      	
	     
	      
	      
			PdfPTable table1 = new PdfPTable(3);
			table1.setWidthPercentage(100);
			
			PdfPCell table1cell = new PdfPCell();
			
			 if(this.payTerms.size()>5){
				 table1cell= new PdfPCell(table1);
//				 table1cell.setBorder(0);
			        }else{
			        	table1cell= new PdfPCell(blankval);
			        	table1cell.setBorder(0);
			        }
			
			Phrase paytermdays1 = new Phrase("Days",font1);
			Phrase paytermpercent1 = new Phrase("Percent",font1);
	      Phrase paytermcomment1 = new Phrase("Comment",font1);
	    
	      PdfPCell celldays1;
	      PdfPCell cellpercent1;
	      PdfPCell cellcomment1;
	      
	      System.out.println(this.payTerms.size()+" ...........b4 if");
	      if(this.payTermsLis.size()>4){
	      	System.out.println(this.payTermsLis.size()+" ...........af if");
	       celldays1= new PdfPCell(paytermdays1);
		    celldays1.setBorder(0);
		    celldays1.setHorizontalAlignment(Element.ALIGN_CENTER);
	      }else{
	      	celldays1= new PdfPCell(blankval);
	  	    celldays1.setBorder(0);
	      	
	      }
	      
	      if(this.payTerms.size()>4){
	      
			 cellpercent1 = new PdfPCell(paytermpercent1);
			cellpercent1.setBorder(0);
			cellpercent1.setHorizontalAlignment(Element.ALIGN_CENTER);
	      }else{
	      	cellpercent1 = new PdfPCell(blankval);
	  		cellpercent1.setBorder(0);
	      	
	      }
	      
	      if(this.payTerms.size()>4){
		    cellcomment1= new PdfPCell(paytermcomment1);
		    cellcomment1.setBorder(0);
		    cellcomment1.setHorizontalAlignment(Element.ALIGN_CENTER);
	      }else{
	      	cellcomment1= new PdfPCell(blankval);
	  	    cellcomment1.setBorder(0);
	      }
		
		
	      table1.addCell(celldays1);
	      table1.addCell(cellpercent1);
	      table1.addCell(cellcomment1);
	     
	   
	      
	      
	      
	      for(int i=5;i<this.payTerms.size();i++)
	      {
	      	System.out.println(this.payTerms.size()+"  in for");
	      	
	      	Phrase chunk = null;
//	     	 Phrase chunk=new Phrase("Day: "+payTermsLis.get(i).getPayTermDays()+"                "+"Percent: "+payTermsLis.get(i).getPayTermPercent()+"               "+"Comment: "+payTermsLis.get(i).getPayTermComment().trim(),font8);
//	     	 PdfPCell pdfdayscell = new PdfPCell(chunk);
//	     	 pdfdayscell.setBorder(0);
	    	if(payTerms.get(i).getPayTermDays()!=null){
	     	  chunk = new Phrase(payTerms.get(i).getPayTermDays()+"",font8);
	    	}else{
	    		chunk = new Phrase(" ",font8);
	    	}
	     	 	
	     	 	PdfPCell pdfdayscell = new PdfPCell(chunk);
	     	 	pdfdayscell.setBorder(0);
	     	 pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
	     	 if(payTerms.get(i).getPayTermPercent()!=0){ 
	     	 chunk=new Phrase(payTerms.get(i).getPayTermPercent()+"",font8);
	     	 }else{
	     		chunk = new Phrase(" ",font8);
	     	 }
	     	 
	     	 PdfPCell pdfpercentcell = new PdfPCell(chunk);
	     	 pdfpercentcell.setBorder(0);
	     	pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	     	if(payTerms.get(i).getPayTermComment()!=null){
	     	 chunk = new Phrase(payTerms.get(i).getPayTermComment().trim(),font8);
	     	}else{
	     		chunk = new Phrase(" ",font8);
	     	}
	     	 PdfPCell pdfcommentcell = new PdfPCell(chunk);
	     	 pdfcommentcell.setBorder(0);
	     	pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	     	
	     	table1.addCell(pdfdayscell);
	     	table1.addCell(pdfpercentcell);
	     	table1.addCell(pdfcommentcell);
	     	
	    	 if(i==9|| count==BreakPoint){
	 				
	 				break;
	 	}
	     	
	     	}
	      
	      
	      
	      ///2nd table for pay terms end
	      
//	      PdfPCell table1cell = new PdfPCell();
	      table1cell.addElement(table1);
	      
	      
	      PdfPTable termstable1 = new PdfPTable(2);
	      termstable1.setWidthPercentage(100);
	      termstable1.addCell(headingpayterms);
	      termstable1.addCell(tablecell1);
	      termstable1.addCell(table1cell);
	      
	      
		   PdfPTable chargetaxtable = new PdfPTable(2);
		   chargetaxtable.setWidthPercentage(100);
		   
		   
		   //***************rohan chnages here on 17/6/15**********************
		   
		  
		   chargetaxtableToNewPage.setWidthPercentage(100);
		   
		   
		   //*************************changes ends here **********************
		   
		   
		   
//		   chargetaxtable.setSpacingAfter(60f);
			
		   Phrase totalAmtPhrase = new Phrase("Total Amount   " + "", font1);
			PdfPCell totalAmtCell = new PdfPCell(totalAmtPhrase);
			totalAmtCell.setBorder(0);
			double totalAmt = 0;
			if (invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)) {
				totalAmt = salesOrderEntity.getTotalAmount();
			}
			if (invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)) {
				totalAmt = purchaseOrder.getTotalAmount();
			}

			Phrase realtotalAmt = new Phrase(df.format(totalAmt), font1);
			PdfPCell realtotalAmtCell = new PdfPCell(realtotalAmt);
			realtotalAmtCell.setBorder(0);
			realtotalAmtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			chargetaxtable.addCell(totalAmtCell);
			chargetaxtable.addCell(realtotalAmtCell);
		   
//			int breakpoint=5;
//			int cnt=this.billingTaxesLis.size()+this.billingChargesLis.size();
		   
			List<String> myList1 = new ArrayList<>();
			List<String> myList2 = new ArrayList<>();
			List<String> myList3 = new ArrayList<>();
			List<String> myList4 = new ArrayList<>();
			
			
	//*****************rohan changes here on 5/6/15 for part payment *****************
			 if(invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
				 
			 
			 for(int i=0;i<this.salesOrderTaxesLis.size();i++)
		      {
		    	  
		   	  if(salesOrderTaxesLis.get(i).getChargePercent()!=0){
		   	  
		   		  if(salesOrderTaxesLis.get(i).getChargeName().equals("VAT")||salesOrderTaxesLis.get(i).getChargeName().equals("CST")){
					
					System.out.println("1st loop"+salesOrderTaxesLis.get(i).getChargeName().trim()+ " @ "+ salesOrderTaxesLis.get(i).getChargePercent());
					
					String str = salesOrderTaxesLis.get(i).getChargeName().trim()+ " @ "+ salesOrderTaxesLis.get(i).getChargePercent();
					double taxAmt1 = salesOrderTaxesLis.get(i).getChargePercent()
							* salesOrderTaxesLis.get(i).getAssessableAmount() / 100;
					
					 myList1.add(str);
					 myList2.add(df.format(taxAmt1));
					 
					 System.out.println("Size of mylist1 is () === "+myList1.size());
				}
				
				if(salesOrderTaxesLis.get(i).getChargeName().equals("Service Tax")){
					
					System.out.println("2nd loop == "+salesOrderTaxesLis.get(i).getChargeName().trim()+ " @ "+ salesOrderTaxesLis.get(i).getChargePercent());
					
					String str = salesOrderTaxesLis.get(i).getChargeName().trim()+ " @ "+ salesOrderTaxesLis.get(i).getChargePercent();
					double taxAmt1 = salesOrderTaxesLis.get(i).getChargePercent()
							* salesOrderTaxesLis.get(i).getAssessableAmount() / 100;
					String ss=" ";
					 myList3.add(str);
					 myList4.add(df.format(taxAmt1));
					 myList4.add(ss);
					 System.out.println("Size of mylist2 is () === "+myList2.size());
				}
		   	  }
		    }
			 
			 PdfPCell pdfservicecell=null;
				
				PdfPTable other1table=new PdfPTable(1);
				other1table.setWidthPercentage(100);
				for(int j=0;j<myList1.size();j++){
					chunk = new Phrase(myList1.get(j),font8);
					 pdfservicecell = new PdfPCell(chunk);
					pdfservicecell.setBorder(0);
					pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
					other1table.addCell(pdfservicecell);
					
				}
				
				
				
				PdfPCell pdfservicecell1=null;
				
				PdfPTable other2table=new PdfPTable(1);
				other2table.setWidthPercentage(100);
				for(int j=0;j<myList2.size();j++){
					chunk = new Phrase(myList2.get(j),font8);
					 pdfservicecell1 = new PdfPCell(chunk);
					pdfservicecell1.setBorder(0);
					pdfservicecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
					other2table.addCell(pdfservicecell1);
					
				}
				
				PdfPCell chargescell=null;
				for(int j=0;j<myList3.size();j++){
					chunk = new Phrase(myList3.get(j),font8);
					chargescell = new PdfPCell(chunk);
					chargescell.setBorder(0);
					chargescell.setHorizontalAlignment(Element.ALIGN_LEFT);
					other1table.addCell(chargescell);
					
				}
				
				PdfPCell chargescell1=null;
				for(int j=0;j<myList4.size();j++){
					chunk = new Phrase(myList4.get(j),font8);
					chargescell1 = new PdfPCell(chunk);
					chargescell1.setBorder(0);
					chargescell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
					other2table.addCell(chargescell1);
					
				}
				
				
				
				PdfPCell othercell = new PdfPCell();
				othercell.addElement(other1table);
				othercell.setBorder(0);
				chargetaxtable.addCell(othercell);
				
				PdfPCell othercell1 = new PdfPCell();
				othercell1.addElement(other2table);
				othercell1.setBorder(0);
				chargetaxtable.addCell(othercell1);
			 
			 
			 
			 
			 
		      PdfPCell chargecell = null;
		      PdfPCell chargeamtcell=null;
		      PdfPCell otherchargecell=null;
		      
			 for(int i=0;i<this.salesOrderChargesLis.size();i++)
		      {
		   	   Phrase chunk = null;
		   	Phrase chunk1 = new Phrase("",font8);
		   	   double chargeAmt=0;
		   	   PdfPCell pdfchargeamtcell = null;
		   	   PdfPCell pdfchargecell = null;
		   	   if(salesOrderChargesLis.get(i).getChargePercent()!=0){
		   		   System.out.println(salesOrderChargesLis.get(i).getChargeName()+".......charge name"+"@"+salesOrderChargesLis.get(i).getChargePercent());
		   		   chunk = new Phrase(salesOrderChargesLis.get(i).getChargeName()+" @ "+salesOrderChargesLis.get(i).getChargePercent(),font1);
		   		   pdfchargecell=new PdfPCell(chunk);
		   		pdfchargecell.setBorder(0);
		   		pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		   		   chargeAmt=salesOrderChargesLis.get(i).getChargePercent()*salesOrderChargesLis.get(i).getAssessableAmount()/100;
		   		   chunk=new Phrase(df.format(chargeAmt),font8);
		  	      	   pdfchargeamtcell = new PdfPCell(chunk);
		  	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		  	      	   pdfchargeamtcell.setBorder(0);
		   	   }else{
		   		pdfchargecell=new PdfPCell(chunk1);
		   		pdfchargeamtcell = new PdfPCell(chunk1);
		   	   }
			      
		   	   ///start
		   	   
		   	PdfPCell pdfchargeamtcell1 = null;
			   PdfPCell pdfchargecell1 = null;
		   	   
		   	   if(salesOrderChargesLis.get(i).getChargeAbsValue()!=0){
		   		   chunk = new Phrase(salesOrderChargesLis.get(i).getChargeName()+"",font1);
		   		pdfchargecell1=new PdfPCell(chunk);
		   		chargeAmt=salesOrderChargesLis.get(i).getChargeAbsValue();
		   		   chunk=new Phrase(chargeAmt+"",font8);
		   		pdfchargeamtcell1 = new PdfPCell(chunk);
		   		pdfchargeamtcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		   		pdfchargeamtcell1.setBorder(0);
		   	    }else{
		   		pdfchargecell1=new PdfPCell(chunk1);
		   		pdfchargeamtcell1 = new PdfPCell(chunk1);
		   	    }
		 	      pdfchargecell.setBorder(0);
		 	      pdfchargeamtcell.setBorder(0);
			      pdfchargecell1.setBorder(0);
			      pdfchargeamtcell1.setBorder(0);
			      
			       chargetaxtable.addCell(pdfchargecell1);
			       chargetaxtable.addCell(pdfchargeamtcell1);
		 	       chargetaxtable.addCell(pdfchargecell);
		 	       chargetaxtable.addCell(pdfchargeamtcell);
		 	       
		      }  
			 
			
			
			//*******************************changes  here*********************
			
			Phrase netname = new Phrase("Grand Total",font8);
			PdfPCell netnamecell = new PdfPCell(netname);
			netnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			namephasevaluecell.addElement(namePhasevalue);
			netnamecell.setBorder(0);
			chargetaxtable.addCell(netnamecell);
		 	
		 
		 	double netpayable=0;
		 	netpayable = salesOrderEntity.getNetpayable();
			String netpayableamt=netpayable+"";
			System.out.println("total============"+netpayableamt);
			Phrase netpay = new Phrase(netpayableamt,font8);
			PdfPCell netpaycell = new PdfPCell(netpay);
			netpaycell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			namephasevaluecell.addElement(namePhasevalue);
			netpaycell.setBorder(0);
			chargetaxtable.addCell(netpaycell);
		 
		 
			Phrase blankSpace = new Phrase(" ",font8);
			PdfPCell blankSpacecell = new PdfPCell(blankSpace);
			blankSpacecell.setBorder(0);
			chargetaxtable.addCell(blankSpacecell);
			chargetaxtable.addCell(blankSpacecell);
			
			
			if(payTerms.get(0).getPayTermPercent()!=100){
			Phrase annexure = new Phrase("Reffer annexure for more details",font8);
			PdfPCell annexurecell = new PdfPCell(annexure);
			annexurecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			namephasevaluecell.addElement(namePhasevalue);
			annexurecell.setBorder(0);
			chargetaxtable.addCell(annexurecell);
			chargetaxtable.addCell(blankSpacecell);
			}
			 }
			 
			 
			
			 
			 if(invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
				 
				 for(int i=0;i<this.purchaseOrderTaxesLis.size();i++)
			      {
			    	  
			   	  if(purchaseOrderTaxesLis.get(i).getChargePercent()!=0){
			   	  
			   		  if(purchaseOrderTaxesLis.get(i).getChargeName().equals("VAT")||purchaseOrderTaxesLis.get(i).getChargeName().equals("CST")){
						
						System.out.println("1st loop"+purchaseOrderTaxesLis.get(i).getChargeName().trim()+ " @ "+ purchaseOrderTaxesLis.get(i).getChargePercent());
						
						String str = purchaseOrderTaxesLis.get(i).getChargeName().trim()+ " @ "+ purchaseOrderTaxesLis.get(i).getChargePercent();
						double taxAmt1 = purchaseOrderTaxesLis.get(i).getChargePercent()
								* purchaseOrderTaxesLis.get(i).getAssessableAmount() / 100;
						
						 myList1.add(str);
						 myList2.add(df.format(taxAmt1));
						 
						 System.out.println("Size of mylist1 is () === "+myList1.size());
					}
					
					if(purchaseOrderTaxesLis.get(i).getChargeName().equals("Service Tax")){
						
						System.out.println("2nd loop == "+purchaseOrderTaxesLis.get(i).getChargeName().trim()+ " @ "+ purchaseOrderTaxesLis.get(i).getChargePercent());
						
						String str = purchaseOrderTaxesLis.get(i).getChargeName().trim()+ " @ "+ purchaseOrderTaxesLis.get(i).getChargePercent();
						double taxAmt1 = purchaseOrderTaxesLis.get(i).getChargePercent()
								* purchaseOrderTaxesLis.get(i).getAssessableAmount() / 100;
						String ss=" ";
						 myList3.add(str);
						 myList4.add(df.format(taxAmt1));
						 myList4.add(ss);
						 System.out.println("Size of mylist2 is () === "+myList2.size());
					}
			   	  }
			    }
				 
				 PdfPCell pdfservicecell=null;
					
					PdfPTable other1table=new PdfPTable(1);
					other1table.setWidthPercentage(100);
					for(int j=0;j<myList1.size();j++){
						chunk = new Phrase(myList1.get(j),font8);
						 pdfservicecell = new PdfPCell(chunk);
						pdfservicecell.setBorder(0);
						pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
						other1table.addCell(pdfservicecell);
						
					}
					
					
					
					PdfPCell pdfservicecell1=null;
					
					PdfPTable other2table=new PdfPTable(1);
					other2table.setWidthPercentage(100);
					for(int j=0;j<myList2.size();j++){
						chunk = new Phrase(myList2.get(j),font8);
						 pdfservicecell1 = new PdfPCell(chunk);
						pdfservicecell1.setBorder(0);
						pdfservicecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
						other2table.addCell(pdfservicecell1);
						
					}
					
					PdfPCell chargescell=null;
					for(int j=0;j<myList3.size();j++){
						chunk = new Phrase(myList3.get(j),font8);
						chargescell = new PdfPCell(chunk);
						chargescell.setBorder(0);
						chargescell.setHorizontalAlignment(Element.ALIGN_LEFT);
						other1table.addCell(chargescell);
						
					}
					
					PdfPCell chargescell1=null;
					for(int j=0;j<myList4.size();j++){
						chunk = new Phrase(myList4.get(j),font8);
						chargescell1 = new PdfPCell(chunk);
						chargescell1.setBorder(0);
						chargescell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
						other2table.addCell(chargescell1);
						
					}
					
					
					
					PdfPCell othercell = new PdfPCell();
					othercell.addElement(other1table);
					othercell.setBorder(0);
					chargetaxtable.addCell(othercell);
					
					PdfPCell othercell1 = new PdfPCell();
					othercell1.addElement(other2table);
					othercell1.setBorder(0);
					chargetaxtable.addCell(othercell1);
				 
				 
				 
				 
				 
			      PdfPCell chargecell = null;
			      PdfPCell chargeamtcell=null;
			      PdfPCell otherchargecell=null;
			      
				 for(int i=0;i<this.purchaseOrderChargesLis.size();i++)
			      {
			   	   Phrase chunk = null;
			   	Phrase chunk1 = new Phrase("",font8);
			   	   double chargeAmt=0;
			   	   PdfPCell pdfchargeamtcell = null;
			   	   PdfPCell pdfchargecell = null;
			   	   if(purchaseOrderChargesLis.get(i).getChargePercent()!=0){
			   		   System.out.println(purchaseOrderChargesLis.get(i).getChargeName()+".......charge name"+"@"+purchaseOrderChargesLis.get(i).getChargePercent());
			   		   chunk = new Phrase(purchaseOrderChargesLis.get(i).getChargeName()+" @ "+purchaseOrderChargesLis.get(i).getChargePercent(),font1);
			   		   pdfchargecell=new PdfPCell(chunk);
			   		pdfchargecell.setBorder(0);
			   		pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			   		   chargeAmt=purchaseOrderChargesLis.get(i).getChargePercent()*purchaseOrderChargesLis.get(i).getAssessableAmount()/100;
			   		   chunk=new Phrase(df.format(chargeAmt),font8);
			  	      	   pdfchargeamtcell = new PdfPCell(chunk);
			  	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			  	      	   pdfchargeamtcell.setBorder(0);
			   	   }else{
			   		pdfchargecell=new PdfPCell(chunk1);
			   		pdfchargeamtcell = new PdfPCell(chunk1);
			   	   }
				      
			   	   ///start
			   	   
			   	PdfPCell pdfchargeamtcell1 = null;
				   PdfPCell pdfchargecell1 = null;
			   	   
			   	   if(purchaseOrderChargesLis.get(i).getChargeAbsValue()!=0){
			   		   chunk = new Phrase(purchaseOrderChargesLis.get(i).getChargeName()+"",font1);
			   		pdfchargecell1=new PdfPCell(chunk);
			   		chargeAmt=purchaseOrderChargesLis.get(i).getChargeAbsValue();
			   		   chunk=new Phrase(chargeAmt+"",font8);
			   		pdfchargeamtcell1 = new PdfPCell(chunk);
			   		pdfchargeamtcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			   		pdfchargeamtcell1.setBorder(0);
			   	    }else{
			   		pdfchargecell1=new PdfPCell(chunk1);
			   		pdfchargeamtcell1 = new PdfPCell(chunk1);
			   	    }
			 	      pdfchargecell.setBorder(0);
			 	      pdfchargeamtcell.setBorder(0);
				      pdfchargecell1.setBorder(0);
				      pdfchargeamtcell1.setBorder(0);
				      
				       chargetaxtable.addCell(pdfchargecell1);
				       chargetaxtable.addCell(pdfchargeamtcell1);
			 	       chargetaxtable.addCell(pdfchargecell);
			 	       chargetaxtable.addCell(pdfchargeamtcell);
			 	       
			      }  
				 
				
				
				//*******************************changes  here*********************
				
				Phrase netname = new Phrase("Grand Total",font8);
				PdfPCell netnamecell = new PdfPCell(netname);
				netnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				namephasevaluecell.addElement(namePhasevalue);
				netnamecell.setBorder(0);
				chargetaxtable.addCell(netnamecell);
			 	
			 
			 	double netpayable=0;
			 	netpayable = purchaseOrder.getNetpayble();
				String netpayableamt=netpayable+"";
				System.out.println("total============"+netpayableamt);
				Phrase netpay = new Phrase(netpayableamt,font8);
				PdfPCell netpaycell = new PdfPCell(netpay);
				netpaycell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//				namephasevaluecell.addElement(namePhasevalue);
				netpaycell.setBorder(0);
				chargetaxtable.addCell(netpaycell);
			 
			 
				Phrase blankSpace = new Phrase(" ",font8);
				PdfPCell blankSpacecell = new PdfPCell(blankSpace);
				blankSpacecell.setBorder(0);
				chargetaxtable.addCell(blankSpacecell);
				chargetaxtable.addCell(blankSpacecell);
				
				
				if(payTerms.get(0).getPayTermPercent()!=100){
				Phrase annexure = new Phrase("Reffer annexure for more details",font8);
				PdfPCell annexurecell = new PdfPCell(annexure);
				annexurecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				namephasevaluecell.addElement(namePhasevalue);
				annexurecell.setBorder(0);
				chargetaxtable.addCell(annexurecell);
				chargetaxtable.addCell(blankSpacecell);
				}
				
				 }
			 
			 
			 
			
	//***************************changes ends here ******************************************			
		
		//**********************rohan changes here on 26/5/15 for part payments********************************		
		System.out.println("billEntity.getArrPayTerms().size==========="+invoiceentity.getArrayBillingDocument().size());
		
		System.out.println("contract count========="+invoiceentity.getContractCount());
		System.out.println("order type======"+invoiceentity.getTypeOfOrder().trim());
		
		
		
		if(invoiceentity.getArrayBillingDocument().size()>1)
		{
		
			 for(int k=0;k<this.payTerms.size();k++)
		      {
				 
				 	List<String> myList11 = new ArrayList<>();
					List<String> myList22 = new ArrayList<>();
					List<String> myList33 = new ArrayList<>();
					List<String> myList44 = new ArrayList<>();
				
				 
				 System.out.println("11111111111111111111111111"+payTerms.get(k).getPayTermPercent());
				 System.out.println("22222222222222222222222222"+billingTaxesLis.get(k).getPayPercent());
				 
				if(payTerms.get(k).getPayTermPercent().equals(billingTaxesLis.get(k).getPayPercent())){
					 
				for(int z=0;z<invoiceentity.getSalesOrderProducts().size();z++){	 
				
				if(invoiceentity.getSalesOrderProducts().get(z).getPaymentPercent()==100){	
					
				
				String payterm=this.billingTaxesLis.get(k).getPayPercent()+"";
				 chunk= new Phrase("Pay Terms "+payterm+"%",font8);
				 PdfPCell paytermCell = new PdfPCell(chunk);
				 paytermCell.setBorder(0);
				 chargetaxtableToNewPage.addCell(paytermCell);
				 
				 double total =0;
					
				 if(invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
				total = ((salesOrderEntity.getTotalAmount()*payTerms.get(k).getPayTermPercent())/100);
				 }
				 
				 if(invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
						total = ((purchaseOrder.getTotalAmount()*payTerms.get(k).getPayTermPercent())/100);
						 } 
				 
				
				Phrase totalphase = new Phrase(df.format(total),font8bold);
				PdfPCell totalcell = new PdfPCell(totalphase);
				totalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				totalcell.setBorder(0);
				chargetaxtableToNewPage.addCell(totalcell);
				 
				 
				//*********************rohan make changes here for payment %
				
					System.out.println("*****************************************************123*");
				}
					else{
					
					double BillPay=100*(payTerms.get(k).getPayTermPercent()/100)*(invoiceentity.getSalesOrderProducts().get(z).getPaymentPercent()/100);
					
					 Phrase billPayPhrase= new Phrase("Pay Term "+BillPay+"%",font8);
					 PdfPCell BillPayCell = new PdfPCell(billPayPhrase);
					 BillPayCell.setBorder(0);
					 chargetaxtableToNewPage.addCell(BillPayCell);
					
					
					
					System.out.println("inside condition ***********************");
					double paymentAsperPercent= (invoiceentity.getSalesOrderProducts().get(z).getBaseBillingAmount()*invoiceentity.getSalesOrderProducts().get(z).getPaymentPercent())/100;
				
					Phrase paymentAsperPercentPhrase = new Phrase(df.format(paymentAsperPercent),font8bold);
					PdfPCell paymentAsperPercentCell = new PdfPCell(paymentAsperPercentPhrase);
					paymentAsperPercentCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					paymentAsperPercentCell.setBorder(0);
					chargetaxtableToNewPage.addCell(paymentAsperPercentCell);
				}
				}
				 
				//***************changes ends here *********************** 
				
				 
				 System.out.println("billingTaxesLis size"+billingTaxesLis.size());
				  System.out.println("iiiiiiii   value    "+k); 
		    	  System.out.println("billingTaxesLis.get(k).getChargesList().get(i) list "+billingTaxesLis.get(k).getChargesList().size());
		    	 for(int i=0;i<this.billingTaxesLis.get(k).getChargesList().size();i++){
		    	  
		    	  if(billingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()!=0){
		    	  
		    		  System.out.println("billingTaxesLis.get(i).getTaxChargeName()"+billingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName());
		    		  
		    		  if(billingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("VAT")||billingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("CST")){
							
							System.out.println("1st loop"+billingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ billingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent());
							
							String str = billingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ billingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent();
							double taxAmt1 = billingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()
									* billingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
							
							 myList11.add(str);
							 myList22.add(df.format(taxAmt1));
							 
							 System.out.println("Size of mylist1 is () === "+myList1.size());
						}
						
						if(billingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("Service Tax")){
							
							 System.out.println("billingTaxesLis.get(i).getTaxChargeName() service tax "+billingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName());
							
							System.out.println("2nd loop == "+billingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ billingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent());
							
							String str = billingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ billingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent();
							double taxAmt1 = billingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()
									* billingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
							String blidnk=" ";
							 myList33.add(str);
							 myList44.add(df.format(taxAmt1));
							 myList44.add(blidnk);
							 
							 System.out.println("Size of mylist2 is () === "+myList2.size());
						}
		    	  }
		    	 }
			 
			 
			 PdfPCell pdfservicecell123=null;
				
				PdfPTable other1table123=new PdfPTable(1);
				other1table123.setWidthPercentage(100);
				for(int j=0;j<myList11.size();j++){
					chunk = new Phrase(myList11.get(j),font8);
					pdfservicecell123 = new PdfPCell(chunk);
					pdfservicecell123.setBorder(0);
					pdfservicecell123.setHorizontalAlignment(Element.ALIGN_LEFT);
					other1table123.addCell(pdfservicecell123);
					
				}
				
				PdfPCell pdfservicecell1123=null;
				
				PdfPTable other2table123=new PdfPTable(1);
				other2table123.setWidthPercentage(100);
				for(int j=0;j<myList22.size();j++){
					chunk = new Phrase(myList22.get(j),font8);
					pdfservicecell1123 = new PdfPCell(chunk);
					pdfservicecell1123.setBorder(0);
					pdfservicecell1123.setHorizontalAlignment(Element.ALIGN_RIGHT);
					other2table123.addCell(pdfservicecell1123);
					
				}
				
				PdfPCell chargescell123=null;
				for(int j=0;j<myList33.size();j++){
					chunk = new Phrase(myList33.get(j),font8);
					chargescell123 = new PdfPCell(chunk);
					chargescell123.setBorder(0);
					chargescell123.setHorizontalAlignment(Element.ALIGN_LEFT);
					other1table123.addCell(chargescell123);
					
				}
				
				PdfPCell chargescell1123=null;
				for(int j=0;j<myList44.size();j++){
					chunk = new Phrase(myList44.get(j),font8);
					chargescell1123 = new PdfPCell(chunk);
					chargescell1123.setBorder(0);
					chargescell1123.setHorizontalAlignment(Element.ALIGN_RIGHT);
					other2table123.addCell(chargescell1123);
					
				}
				
				
				
				PdfPCell othercell123 = new PdfPCell();
				othercell123.addElement(other1table123);
				othercell123.setBorder(0);
				chargetaxtableToNewPage.addCell(othercell123);
				
				PdfPCell othercell1123 = new PdfPCell();
				othercell1123.addElement(other2table123);
				othercell1123.setBorder(0);
				chargetaxtableToNewPage.addCell(othercell1123);
//		    	 }
			 //*******ROHAN CHANGES HERE FOR PART PAYMENT ON 5/6/15******************
//		      }
				
			 //**********************CHANGES ENDS HERE********************************
			 
			 
//			 PdfPCell chargecell22 = null;
//		      PdfPCell chargeamtcell22=null;
//		      PdfPCell otherchargecell22=null;
//			 if(payTerms.get(k).getPayTermPercent().equals(billingChargesLis.get(0).getPayPercent())){
			 for(int j=0;j<this.billingChargesLis.size();j++)
		      {
				 
		   	   Phrase chunk = null;
		   	   Phrase chunk1 = new Phrase("",font8);
		   	   double chargeAmt=0;
		   	   PdfPCell pdfchargeamtcell = null;
		   	   PdfPCell pdfchargecell = null;
		   	
		   	 System.out.println("88888888888888888888888888"+payTerms.get(k).getPayTermPercent());
			 System.out.println("99999999999999999999999999"+billingChargesLis.get(j).getPayPercent());
			 
			 if(payTerms.get(k).getPayTermPercent().equals(billingChargesLis.get(j).getPayPercent())){
		   	   
		   	   
		   	 for(int i=0;i<this.billingChargesLis.get(j).getOtherChargesList().size();i++){
		    	  
		   	   if(billingChargesLis.get(j).getOtherChargesList().get(i).getTaxChargePercent()!=0){
		   		   chunk = new Phrase(billingChargesLis.get(j).getOtherChargesList().get(i).getTaxChargeName()+" @ "+billingChargesLis.get(i).getOtherChargesList().get(i).getTaxChargePercent(),font1);
		   		   pdfchargecell=new PdfPCell(chunk);
		   		pdfchargecell.setBorder(0);
		   		chargetaxtableToNewPage.addCell(pdfchargecell);
		 	     
//		   		   chargeAmt=billingChargesLis.get(i).getTaxChargePercent()*billingChargesLis.get(i).getTaxChargeAssesVal()/100;
		   		 chargeAmt=billingChargesLis.get(j).getOtherChargesList().get(i).getPayableAmt();
		   		   chunk=new Phrase(df.format(chargeAmt),font8);
		  	      	   pdfchargeamtcell = new PdfPCell(chunk);
		  	      	 pdfchargeamtcell.setBorder(0);
		  	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		  	      chargetaxtableToNewPage.addCell(pdfchargeamtcell);
		  	      	
		  	      	  
		   	   }else{
		   		pdfchargecell=new PdfPCell(chunk1);
		   		pdfchargecell.setBorder(0);
		   		pdfchargeamtcell = new PdfPCell(chunk1);
		   		pdfchargeamtcell.setBorder(0);
		   		chargetaxtableToNewPage.addCell(pdfchargecell);
		   		chargetaxtableToNewPage.addCell(pdfchargeamtcell);
		   	   }
		   	   if(billingChargesLis.get(j).getOtherChargesList().get(i).getTaxChargeAbsVal()!=0){
		   		   chunk = new Phrase(billingChargesLis.get(j).getOtherChargesList().get(i).getTaxChargeName()+"",font1);
		   		   pdfchargecell=new PdfPCell(chunk);
		   		pdfchargecell.setBorder(0);
		   		chargetaxtableToNewPage.addCell(pdfchargecell);
		   		   chargeAmt=billingChargesLis.get(j).getOtherChargesList().get(i).getPayableAmt();
		   		   chunk=new Phrase(chargeAmt+"",font8);
		  	      	   pdfchargeamtcell = new PdfPCell(chunk);
		  	      	  pdfchargeamtcell.setBorder(0);
		  	      	   pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		  	      	chargetaxtableToNewPage.addCell(pdfchargeamtcell);
		   	   }else{
		   		pdfchargecell=new PdfPCell(chunk1);
		   		pdfchargecell.setBorder(0);
		   		pdfchargeamtcell = new PdfPCell(chunk1);
		   		pdfchargeamtcell.setBorder(0);
		   		chargetaxtableToNewPage.addCell(pdfchargecell);
		   		chargetaxtableToNewPage.addCell(pdfchargeamtcell);
		   	   }
		   	   
			      }
			 	}
		      	}
				 }
				 
				 Phrase chunk1 = new Phrase(" ",font8);
				 PdfPCell pdfchargecell=new PdfPCell(chunk1);
			   		pdfchargecell.setBorder(0);
			   		PdfPCell pdfchargeamtcell = new PdfPCell(chunk1);
			   		pdfchargeamtcell.setBorder(0);
			   		chargetaxtableToNewPage.addCell(pdfchargecell);
			   		chargetaxtableToNewPage.addCell(pdfchargeamtcell);
				 
				 
		      }
		}
		else
		{
			System.out.println("indise else condition");
			
			
		for(int z=0;z<invoiceentity.getSalesOrderProducts().size();z++){	 
				
		if(invoiceentity.getSalesOrderProducts().get(z).getPaymentPercent()==100){	
			
		System.out.println("Pay Percent "+invoiceentity.getArrPayTerms().get(0).getPayTermPercent());
		Phrase namePhase = new Phrase("Pay Terms "+invoiceentity.getArrPayTerms().get(0).getPayTermPercent()+"%",font8);
		PdfPCell namephasecell = new PdfPCell(namePhase);
		namephasecell.addElement(namePhase);
		namephasecell.setBorder(0);
		chargetaxtableToNewPage.addCell(namephasecell);
		double total =0;
		
		 if(invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
		total = ((salesOrderEntity.getTotalAmount()*invoiceentity.getArrPayTerms().get(0).getPayTermPercent())/100);
		 }
		 
		 if(invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
				total = ((purchaseOrder.getTotalAmount()*invoiceentity.getArrPayTerms().get(0).getPayTermPercent())/100);
				 } 
		 
		
		Phrase totalphase = new Phrase(df.format(total),font8);
		PdfPCell totalcell = new PdfPCell(totalphase);
		totalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		totalcell.setBorder(0);
		chargetaxtableToNewPage.addCell(totalcell);
		
		
		//*********************rohan make changes here for payment %
//		for(int z=0;z<billEntity.getSalesOrderProducts().size();z++){
			System.out.println("*****************************************************123*");
		}
			else{
		
			double BillPay=100*(invoiceentity.getArrPayTerms().get(0).getPayTermPercent()/100)*(invoiceentity.getSalesOrderProducts().get(z).getPaymentPercent()/100);
			
			 Phrase billPayPhrase= new Phrase("Pay Term "+BillPay+"%",font8);
			 PdfPCell BillPayCell = new PdfPCell(billPayPhrase);
			 BillPayCell.setBorder(0);
			 chargetaxtableToNewPage.addCell(BillPayCell);
			
			
			
			System.out.println("inside condition ***********************");
			double paymentAsperPercent= (invoiceentity.getSalesOrderProducts().get(z).getBaseBillingAmount()*invoiceentity.getSalesOrderProducts().get(z).getPaymentPercent())/100;
		
			Phrase paymentAsperPercentPhrase = new Phrase(df.format(paymentAsperPercent),font8);
			PdfPCell paymentAsperPercentCell = new PdfPCell(paymentAsperPercentPhrase);
			paymentAsperPercentCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			paymentAsperPercentCell.setBorder(0);
			chargetaxtableToNewPage.addCell(paymentAsperPercentCell);
		}
		}
		 
		//***************changes ends here *********************** 
		
		
		
		
		//*********************rohan changes here ****************************
		List<String> myList11 = new ArrayList<>();
		List<String> myList22 = new ArrayList<>();
		List<String> myList33 = new ArrayList<>();
		List<String> myList44 = new ArrayList<>();
		//****************************rohan changes on 16/6/15*****************
		 for(int k=0;k<this.billingTaxesLis.size();k++)
	     {
	   	  
			 
			 System.out.println("billingTaxesLis size"+billingTaxesLis.size());
			  System.out.println("iiiiiiii   value    "+k); 
	   	  
	   	  for(int i=0;i<this.billingTaxesLis.get(k).getChargesList().size();i++){
	   	  
	   	  if(billingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()!=0){
	   	  
	   		  System.out.println("billingTaxesLis.get(i).getChargesList() size"+billingTaxesLis.get(k).getChargesList().size());
	   		  
	   		  System.out.println("billingTaxesLis.get(i).getTaxChargeName()"+billingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName());
	   		  
	   		  if(billingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("VAT")||billingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("CST")){
						
						System.out.println("1st loop"+billingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ billingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent());
						
						String str = billingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ billingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent();
						double taxAmt1 = billingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()
								* billingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
						
						 myList11.add(str);
						 myList22.add(df.format(taxAmt1));
						 
						 System.out.println("Size of mylist1 is () === "+myList1.size());
					}
					
					if(billingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("Service Tax")){
						
						 System.out.println("billingTaxesLis.get(i).getTaxChargeName() service tax "+billingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName());
						
						System.out.println("2nd loop == "+billingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ billingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent());
						
						String str = billingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ billingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent();
						double taxAmt1 = billingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()
								* billingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
						String blidnk=" ";
						 myList33.add(str);
						 myList44.add(df.format(taxAmt1));
						 myList44.add(blidnk);
						 
						 System.out.println("Size of mylist2 is () === "+myList2.size());
					}
	   	  }
	    	}
	     }
		 
		 PdfPCell pdfservicecell123=null;
			
			PdfPTable other1table123=new PdfPTable(1);
			other1table123.setWidthPercentage(100);
			for(int j=0;j<myList11.size();j++){
				chunk = new Phrase(myList11.get(j),font8);
				pdfservicecell123 = new PdfPCell(chunk);
				pdfservicecell123.setBorder(0);
				pdfservicecell123.setHorizontalAlignment(Element.ALIGN_LEFT);
				other1table123.addCell(pdfservicecell123);
				
			}
			
			PdfPCell pdfservicecell1123=null;
			
			PdfPTable other2table123=new PdfPTable(1);
			other2table123.setWidthPercentage(100);
			for(int j=0;j<myList22.size();j++){
				chunk = new Phrase(myList22.get(j),font8);
				pdfservicecell1123 = new PdfPCell(chunk);
				pdfservicecell1123.setBorder(0);
				pdfservicecell1123.setHorizontalAlignment(Element.ALIGN_RIGHT);
				other2table123.addCell(pdfservicecell1123);
				
			}
			
			PdfPCell chargescell123=null;
			for(int j=0;j<myList33.size();j++){
				chunk = new Phrase(myList33.get(j),font8);
				chargescell123 = new PdfPCell(chunk);
				chargescell123.setBorder(0);
				chargescell123.setHorizontalAlignment(Element.ALIGN_LEFT);
				other1table123.addCell(chargescell123);
				
			}
			
			PdfPCell chargescell1123=null;
			for(int j=0;j<myList44.size();j++){
				chunk = new Phrase(myList44.get(j),font8);
				chargescell1123 = new PdfPCell(chunk);
				chargescell1123.setBorder(0);
				chargescell1123.setHorizontalAlignment(Element.ALIGN_RIGHT);
				other2table123.addCell(chargescell1123);
				
			}
			
			PdfPCell othercell123 = new PdfPCell();
			othercell123.addElement(other1table123);
			othercell123.setBorder(0);
			chargetaxtableToNewPage.addCell(othercell123);
			
			PdfPCell othercell1123 = new PdfPCell();
			othercell1123.addElement(other2table123);
			othercell1123.setBorder(0);
			chargetaxtableToNewPage.addCell(othercell1123);
		 
		 //*******ROHAN CHANGES HERE FOR PART PAYMENT ON 5/6/15******************
			
			
			
		 //**********************CHANGES ENDS HERE********************************
		 
		 
//		 PdfPCell chargecell22 = null;
//	     PdfPCell chargeamtcell22=null;
//	     PdfPCell otherchargecell22=null;
		 System.out.println(billingChargesLis.size());
	     System.out.println("rohan bhagde====================== "+billingChargesLis.size());
		 for(int k=0;k<this.billingChargesLis.size();k++)
	     {
	  	   Phrase chunk = null;
	  	   Phrase chunk1 = new Phrase("",font8);
	  	   double chargeAmt=0;
	  	   PdfPCell pdfchargeamtcell = null;
	  	   PdfPCell pdfchargecell = null;
	  	   
	  	   
	  	   for(int i=0;i<this.billingChargesLis.get(k).getOtherChargesList().size();i++){
	  	   
	  	   if(billingChargesLis.get(k).getOtherChargesList().get(i).getTaxChargePercent()!=0){
	  		   chunk = new Phrase(billingChargesLis.get(k).getOtherChargesList().get(i).getTaxChargeName()+" @ "+billingChargesLis.get(k).getOtherChargesList().get(i).getTaxChargePercent(),font1);
	  		   pdfchargecell=new PdfPCell(chunk);
	  		pdfchargecell.setBorder(0);
	  		chargetaxtableToNewPage.addCell(pdfchargecell);
		     
//	  		   chargeAmt=billingChargesLis.get(i).getTaxChargePercent()*billingChargesLis.get(i).getTaxChargeAssesVal()/100;
	  		 chargeAmt=billingChargesLis.get(k).getOtherChargesList().get(i).getPayableAmt();
	  		   chunk=new Phrase(df.format(chargeAmt),font8);
	 	      	   pdfchargeamtcell = new PdfPCell(chunk);
	 	      	 pdfchargeamtcell.setBorder(0);
	 	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	 	      	chargetaxtableToNewPage.addCell(pdfchargeamtcell);
	 	      	
	 	      	  
	  	   }else{
	  		pdfchargecell=new PdfPCell(chunk1);
	  		pdfchargecell.setBorder(0);
	  		pdfchargeamtcell = new PdfPCell(chunk1);
	  		pdfchargeamtcell.setBorder(0);
	  		chargetaxtableToNewPage.addCell(pdfchargecell);
	  		chargetaxtableToNewPage.addCell(pdfchargeamtcell);
	  	   }
	  	   if(billingChargesLis.get(k).getOtherChargesList().get(i).getTaxChargeAbsVal()!=0){
	  		   chunk = new Phrase(billingChargesLis.get(k).getOtherChargesList().get(i).getTaxChargeName()+"",font1);
	  		   pdfchargecell=new PdfPCell(chunk);
	  		pdfchargecell.setBorder(0);
	  		chargetaxtableToNewPage.addCell(pdfchargecell);
	  		   chargeAmt=billingChargesLis.get(k).getOtherChargesList().get(i).getPayableAmt();
	  		   chunk=new Phrase(chargeAmt+"",font8);
	 	      	   pdfchargeamtcell = new PdfPCell(chunk);
	 	      	  pdfchargeamtcell.setBorder(0);
	 	      	   pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	 	      	chargetaxtableToNewPage.addCell(pdfchargeamtcell);
	  	   }else{
	  		pdfchargecell=new PdfPCell(chunk1);
	  		pdfchargecell.setBorder(0);
	  		pdfchargeamtcell = new PdfPCell(chunk1);
	  		pdfchargeamtcell.setBorder(0);
	  		chargetaxtableToNewPage.addCell(pdfchargecell);
	  		chargetaxtableToNewPage.addCell(pdfchargeamtcell);
	  	   }
		      
		      }
	     }
		//************************changes ends here ***********************8
		
		}			
//			  	      	 pdfchargeamtcell.setBorder(0);
//			  	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			  	      	  chargetaxtable.addCell(pdfchargeamtcell);
//			  	      	
//			  	      	  
//			   	   }else{
//			   		pdfchargecell=new PdfPCell(chunk1);
//			   		pdfchargecell.setBorder(0);
//			   		pdfchargeamtcell = new PdfPCell(chunk1);
//			   		pdfchargeamtcell.setBorder(0);
//			   		chargetaxtable.addCell(pdfchargecell);
//			   	  chargetaxtable.addCell(pdfchargeamtcell);
//			   	   }
//			   	   if(billingChargesLis.get(i).getTaxChargeAbsVal()!=0){
//			   		   chunk = new Phrase(billingChargesLis.get(i).getTaxChargeName()+"",font1);
//			   		   pdfchargecell=new PdfPCell(chunk);
//			   		pdfchargecell.setBorder(0);
//			   		chargetaxtable.addCell(pdfchargecell);
//			   		   chargeAmt=billingChargesLis.get(i).getPayableAmt();
//			   		   chunk=new Phrase(chargeAmt+"",font8);
//			  	      	   pdfchargeamtcell = new PdfPCell(chunk);
//			  	      	  pdfchargeamtcell.setBorder(0);
//			  	      	   pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			  	      	  chargetaxtable.addCell(pdfchargeamtcell);
//			   	   }else{
//			   		pdfchargecell=new PdfPCell(chunk1);
//			   		pdfchargecell.setBorder(0);
//			   		pdfchargeamtcell = new PdfPCell(chunk1);
//			   		pdfchargeamtcell.setBorder(0);
//			   		chargetaxtable.addCell(pdfchargecell);
//				   	  chargetaxtable.addCell(pdfchargeamtcell);
//			   	   }
//			 	      
//				      }
//		}
				
//				 for(int i=0;i<this.billingTaxesLis.size();i++)
//			      {
//			    	  
//			    	  if(billingTaxesLis.get(i).getTaxChargePercent()!=0){
//			    	  
//			    		  if(billingTaxesLis.get(i).getTaxChargeName().equals("VAT")||billingTaxesLis.get(i).getTaxChargeName().equals("CST")){
//								
//								System.out.println("1st loop"+billingTaxesLis.get(i).getTaxChargeName().trim()+ " @ "+ billingTaxesLis.get(i).getTaxChargePercent());
//								
//								String str = billingTaxesLis.get(i).getTaxChargeName().trim()+ " @ "+ billingTaxesLis.get(i).getTaxChargePercent();
//								double taxAmt1 = billingTaxesLis.get(i).getTaxChargePercent()
//										* billingTaxesLis.get(i).getTaxChargeAssesVal() / 100;
//								
//								 myList1.add(str);
//								 myList2.add(df.format(taxAmt1));
//								 
//								 System.out.println("Size of mylist1 is () === "+myList1.size());
//							}
//							
//							if(billingTaxesLis.get(i).getTaxChargeName().equals("Service Tax")){
//								
//								System.out.println("2nd loop == "+billingTaxesLis.get(i).getTaxChargeName().trim()+ " @ "+ billingTaxesLis.get(i).getTaxChargePercent());
//								
//								String str = billingTaxesLis.get(i).getTaxChargeName().trim()+ " @ "+ billingTaxesLis.get(i).getTaxChargePercent();
//								double taxAmt1 = billingTaxesLis.get(i).getTaxChargePercent()
//										* billingTaxesLis.get(i).getTaxChargeAssesVal() / 100;
//								
//								String blidnk=" ";
//								myList3.add(str);
//								 myList4.add(df.format(taxAmt1));
//								 myList4.add(blidnk);
//								 System.out.println("Size of mylist2 is () === "+myList2.size());
//							}
//			    	  }

//				 PdfPCell pdfservicecell22=null;
//					
//					PdfPTable other1table22=new PdfPTable(1);
//					other1table22.setWidthPercentage(100);
//					for(int j=0;j<myList1.size();j++){
//						chunk = new Phrase(myList1.get(j),font8);
//						 pdfservicecell22 = new PdfPCell(chunk);
//						pdfservicecell22.setBorder(0);
//						pdfservicecell22.setHorizontalAlignment(Element.ALIGN_LEFT);
//						other1table.addCell(pdfservicecell22);
//						
//					}
					
//					PdfPCell pdfservicecell122=null;
//					
//					PdfPTable other2table22=new PdfPTable(1);
//					other2table22.setWidthPercentage(100);
//					for(int j=0;j<myList2.size();j++){
//						chunk = new Phrase(myList2.get(j),font8);
//						 pdfservicecell122 = new PdfPCell(chunk);
//						pdfservicecell122.setBorder(0);
//						pdfservicecell122.setHorizontalAlignment(Element.ALIGN_RIGHT);
//						other2table.addCell(pdfservicecell122);
//						
//					}
					
					
//					PdfPCell chargescell22=null;
//					for(int j=0;j<myList3.size();j++){
//						chunk = new Phrase(myList3.get(j),font8);
//						chargescell22 = new PdfPCell(chunk);
//						chargescell22.setBorder(0);
//						chargescell22.setHorizontalAlignment(Element.ALIGN_LEFT);
//						other1table22.addCell(chargescell22);
//						
//					}
					
//					PdfPCell chargescell122=null;
//					for(int j=0;j<myList4.size();j++){
//						chunk = new Phrase(myList4.get(j),font8);
//						chargescell122 = new PdfPCell(chunk);
//						chargescell122.setBorder(0);
//						chargescell122.setHorizontalAlignment(Element.ALIGN_RIGHT);
//						other2table22.addCell(chargescell122);
//						
//					}
					
					
					
//					PdfPCell othercell22 = new PdfPCell();
//					othercell22.addElement(other1table22);
//					othercell22.setBorder(0);
//					chargetaxtable.addCell(othercell22);
//					
//					PdfPCell othercell122 = new PdfPCell();
//					othercell122.addElement(other2table22);
//					othercell122.setBorder(0);
//					chargetaxtable.addCell(othercell122);
				 
				 
			      /****************************************************************************************/
			      
//			      for(int i=0;i<this.billingChargesLis.size();i++)
//			      {
//			   	   Phrase chunk = null;
//			       Phrase chunk1 = new Phrase("",font8);
//			   	   double chargeAmt=0;
//			   	   PdfPCell pdfchargeamtcell = null;
//			   	   PdfPCell pdfchargecell = null;
//			   	   if(billingChargesLis.get(i).getTaxChargePercent()!=0){
//			   		   
//			   		   System.out.println("getTaxChargePercent");
//			   		   
//			   		   chunk = new Phrase(billingChargesLis.get(i).getTaxChargeName()+" @ "+billingChargesLis.get(i).getTaxChargePercent(),font1);
//			   		   pdfchargecell=new PdfPCell(chunk);
////			   		   chargeAmt=billingChargesLis.get(i).getTaxChargePercent()*billingChargesLis.get(i).getTaxChargeAssesVal()/100;
//			   		chargeAmt=billingChargesLis.get(i).getPayableAmt();
//			   		   chunk=new Phrase(df.format(chargeAmt),font8);
//			  	      	   pdfchargeamtcell = new PdfPCell(chunk);
//			  	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			  	      	   pdfchargeamtcell.setBorder(0);
//			  	      	   
//			  	      	  pdfchargecell.setBorder(0);
//				 	      pdfchargeamtcell.setBorder(0);
//				 	       chargetaxtable.addCell(pdfchargecell);
//				 	       chargetaxtable.addCell(pdfchargeamtcell);
//			   	   }else{
//			   		pdfchargecell=new PdfPCell(chunk1);
//			   		pdfchargeamtcell = new PdfPCell(chunk1);
//			   		
//			   	  pdfchargecell.setBorder(0);
//		 	      pdfchargeamtcell.setBorder(0);
//		 	       chargetaxtable.addCell(pdfchargecell);
//		 	       chargetaxtable.addCell(pdfchargeamtcell);
//			   	   }
//			   	   if(billingChargesLis.get(i).getTaxChargeAbsVal()!=0){
//			   		   chunk = new Phrase(billingChargesLis.get(i).getTaxChargeName()+"",font1);
//			   		   pdfchargecell=new PdfPCell(chunk);
//			   		   chargeAmt=billingChargesLis.get(i).getPayableAmt();
//			   		   
//			   		 System.out.println("getTaxChargeAbsVal");
//			   		   
//			   		   chunk=new Phrase(chargeAmt+"",font8);
//			  	      	   pdfchargeamtcell = new PdfPCell(chunk);
//			  	      	   pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			  	      	   pdfchargeamtcell.setBorder(0);
//			  	      	   
//			  	      	  pdfchargecell.setBorder(0);
//				 	      pdfchargeamtcell.setBorder(0);
//				 	       chargetaxtable.addCell(pdfchargecell);
//				 	       chargetaxtable.addCell(pdfchargeamtcell);
//			   	   }else{
//			   		pdfchargecell=new PdfPCell(chunk1);
//			   		pdfchargeamtcell = new PdfPCell(chunk1);
//			   		
//			   	  pdfchargecell.setBorder(0);
//		 	      pdfchargeamtcell.setBorder(0);
//		 	       chargetaxtable.addCell(pdfchargecell);
//		 	       chargetaxtable.addCell(pdfchargeamtcell);
//			   	   }
//			 	     
//				      	
//			      }
				
				
//			}
		   
		  
	    
	    
	    PdfPTable billamtable = new PdfPTable(2);
	    billamtable.setWidthPercentage(100);
	    billamtable.setHorizontalAlignment(Element.ALIGN_BOTTOM);
		       
		      for(int i=0;i<this.billingChargesLis.size();i++){
		       
		      	if(i==billingChargesLis.size()-1){
		      		chunk = new Phrase("Net Payable",font1);
			        PdfPCell pdfinvamtcell = new PdfPCell(chunk);
			        pdfinvamtcell.setBorder(0);
			      	Double invAmt=invoiceentity.getInvoiceAmount();
			      	
			      	int netpayble=(int) invAmt.doubleValue();
			      	
			      	chunk=new Phrase(netpayble+"",font8);
			        PdfPCell pdfnetpayamt = new PdfPCell(chunk);
			        pdfnetpayamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
			        pdfnetpayamt.setBorder(0);
			        
//			        chargetaxtable.addCell(pdfinvamtcell);
//			        chargetaxtable.addCell(pdfnetpayamt);
			        
			        billamtable.addCell(pdfinvamtcell);
			        billamtable.addCell(pdfnetpayamt);
			        
			        netPayableTable.addCell(pdfinvamtcell);
			        netPayableTable.addCell(pdfnetpayamt);
		      	}
	   	}
	    
	    if(this.billingChargesLis.size()==0){
	  	  
	  	  Phrase chunkinvAmttitle = new Phrase("Net Payable",font1);
	  	  PdfPCell invAmtTitleCell=new PdfPCell(chunkinvAmttitle);
	  	  invAmtTitleCell.setBorder(0);
	  	  Double invoiceAmount=invoiceentity.getInvoiceAmount();
	  	  
	  	  int netpayble=(int) invoiceAmount.doubleValue();
	  	  
		      Phrase chunkinvamt=new Phrase(netpayble+"",font8);
		      PdfPCell pdfinvamt = new PdfPCell(chunkinvamt);
		      pdfinvamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
		      pdfinvamt.setBorder(0);
		        
//		      chargetaxtable.addCell(invAmtTitleCell);
//		      chargetaxtable.addCell(pdfinvamt);
		      
		      billamtable.addCell(invAmtTitleCell);
		        billamtable.addCell(pdfinvamt);
		        
		        
		        netPayableTable.addCell(invAmtTitleCell);
		        netPayableTable.addCell(pdfinvamt);
	    }
	    
	    PdfPCell cagrecell=new PdfPCell();
	    cagrecell.addElement(chargetaxtable);
	    cagrecell.setBorder(0);
	    
	    PdfPTable mainbilltable = new PdfPTable(1);
	    mainbilltable.setWidthPercentage(100);
	    mainbilltable.setHorizontalAlignment(Element.ALIGN_RIGHT);
	    mainbilltable.addCell(cagrecell);
//	    mainbilltable.addCell(billamtable);
	    
	    
	    
	    PdfPTable taxinfotable=new PdfPTable(1);
//	    taxinfotable.setWidthPercentage(100);
//	    taxinfotable.addCell(mainbilltable);
	    
	    PdfPTable parenttaxtable=new PdfPTable(2);
	    parenttaxtable.setWidthPercentage(100);
	    try {
			parenttaxtable.setWidths(new float[]{65,35});
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
	    
		  PdfPCell amtWordsTblcell1 = new PdfPCell();
		  PdfPCell taxdatacell = new PdfPCell();
			
		  amtWordsTblcell1.addElement(termstable1);
		  taxdatacell.addElement(mainbilltable);
		  
		  parenttaxtable.addCell(amtWordsTblcell1);
		  parenttaxtable.addCell(taxdatacell);
		
			try {
				document.add(parenttaxtable);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
			
			  String amtInWords="Rupees:  "+SalesInvoicePdf.convert(invoiceentity.getInvoiceAmount());
			   System.out.println("amt"+amtInWords);
			   Phrase amtwords=new Phrase(amtInWords+" Only.",font1);
			   PdfPCell amtWordsCell=new PdfPCell();
			   amtWordsCell.addElement(amtwords);
			   amtWordsCell.setBorder(0);
			   
			   
			   PdfPTable amountInWordsTable=new PdfPTable(1);
			   amountInWordsTable.setWidthPercentage(100);
//			   amountInWordsTable.setHorizontalAlignment(Element.ALIGN_TOP);
			   amountInWordsTable.addCell(amtWordsCell);
				
			  
			   PdfPTable termpayTable=new PdfPTable(1);
			   termpayTable.setWidthPercentage(100);
//			   termpayTable.addCell(headingpayterms);
			   termpayTable.addCell(termstable1);
//			   termpayTable.addCell(amountInWordsTable);
			
			   PdfPTable parenttaxtable1=new PdfPTable(2);
			      parenttaxtable1.setWidthPercentage(100);
			      try {
					parenttaxtable1.setWidths(new float[]{65,35});
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
			      
				  PdfPCell amtWordsTblcell = new PdfPCell();
				  PdfPCell taxdatacell1 = new PdfPCell();
					
				  amtWordsTblcell.addElement(amountInWordsTable);
				  taxdatacell1.addElement(billamtable);
				  parenttaxtable1.addCell(amtWordsTblcell);
				  parenttaxtable1.addCell(taxdatacell1);
				
					try {
						document.add(parenttaxtable1);
						
					} catch (DocumentException e) {
						e.printStackTrace();
					}}
	  
	  
	  private void billingCharges() {
		// TODO Auto-generated method stub
		
	}

	public void addFooter()
	  {
		  logger.log(Level.SEVERE,"in ....footer.....");
		  	Phrase typename=null;
			Phrase typevalue=null;
			
			PdfPTable artictable=new PdfPTable(3);
			artictable.setWidthPercentage(100);

			try {
				artictable.setWidths(columnWidths2);
			} catch (DocumentException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			
			for(int i=0;i<this.articletype.size();i++){
				 
				
				if(articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().equals("SalesInvoice")){
					
					 typename = new Phrase(articletype.get(i).getArticleTypeName(),font8);
					 typevalue = new Phrase(articletype.get(i).getArticleTypeValue(),font8);
					 
					 	PdfPCell tymanecell=new PdfPCell();
						tymanecell.addElement(typename);
						tymanecell.setBorder(0);
						
						PdfPCell typevalcell=new PdfPCell();
						typevalcell.addElement(typevalue);
						typevalcell.setBorder(0);
						
						Phrase colun=new Phrase(":",font8);
						PdfPCell typevalcell1=new PdfPCell();
						typevalcell1.addElement(colun);
						typevalcell1.setBorder(0);
						
						
						artictable.addCell(tymanecell);
						artictable.addCell(typevalcell1);
						artictable.addCell(typevalcell);
						
				 }
				
			 }
		  
		  
			PdfPTable blanktable=new PdfPTable(1);


			PdfPCell articcell=new PdfPCell(artictable);
			articcell.setBorder(0);

			PdfPCell articcell1=new PdfPCell(blanktable);
			articcell1.setBorder(0);

		  
		  
		  PdfPTable desctable=new PdfPTable(1);
		  desctable.setWidthPercentage(100);
		  
		  desctable.addCell(articcell);
		  desctable.addCell(articcell1);
		  
		  String terms="";
		  Phrase desc1=new Phrase("",font10);
		  logger.log(Level.SEVERE,"in .out if...footer.....");
		 
//		  System.out.println("b4 cooment === "+invoiceentity.getComment());
		  logger.log(Level.SEVERE," in if....footer.....");
		  System.out.println("kkk"+invoiceentity.getComment());
//		  if(!invoiceentity.getComment().trim().equals("")){
			  logger.log(Level.SEVERE," in if....footer.....");
			  System.out.println("after coomet == "+invoiceentity.getComment());
			  
			 
			  
//			  terms="Terms and Condition";
//			  desc1=new Phrase(invoiceentity.getComment(),font8);
			  desc1=new Phrase(invComment,font8);
		
//		  }
		  
		  logger.log(Level.SEVERE," ....footer..2...");
		  logger.log(Level.SEVERE,"1111111111111"+invoiceentity.getComment());
		  Phrase term=new Phrase(terms,font10bold);
		  PdfPCell termcell= new PdfPCell();
		  termcell.addElement(term);
		  termcell.setBorder(0);
		  
		  PdfPCell desc1cell=new PdfPCell();
		  desc1cell.addElement(desc1);
		  desc1cell.setBorder(0);
		  
		  desctable.addCell(termcell);
		  desctable.addCell(desc1cell);
		  
		  
		  
		  logger.log(Level.SEVERE," ....footer..3...");
		  String companyname=comp.getBusinessUnitName().trim().toUpperCase();
		  Paragraph companynamepara=new Paragraph();
		  companynamepara.add("FOR "+companyname);
		  companynamepara.setFont(font9bold);
		  companynamepara.setAlignment(Element.ALIGN_CENTER);
		  String authsign="AUTHORISED SIGNATORY";
		  
		  Paragraph authpara=new Paragraph();
		  authpara.add(authsign);
		  authpara.setFont(font9bold);
		  authpara.setAlignment(Element.ALIGN_CENTER);
		  PdfPCell companynamecell=new PdfPCell();
		  companynamecell.addElement(companynamepara);
		  companynamecell.setBorder(0);
		     
		  PdfPCell authsigncell=new PdfPCell();
		  authsigncell.addElement(authpara);
		  authsigncell.setBorder(0);
		  Phrase blankphrase = new Phrase(" ");
		  PdfPCell blankcell=new PdfPCell();
		  blankcell.addElement(blankphrase);
		  blankcell.setBorder(0);
		  
		  PdfPTable table = new PdfPTable(1);
		  table.addCell(companynamecell);
		  table.addCell(blankcell);
		  table.addCell(blankcell);
	  	  table.addCell(blankcell);
		  table.addCell(authsigncell);
			 
		  table.setWidthPercentage(100);
		  
		  PdfPCell descparent=new PdfPCell();
		  PdfPCell authparent=new PdfPCell();
		  
		  descparent.addElement(desctable);
		  authparent.addElement(table);
		  
		  logger.log(Level.SEVERE," ....footer..4...");
			 
			  PdfPTable parentbanktable=new PdfPTable(2);
			  parentbanktable.setWidthPercentage(100);
			  try {
				  parentbanktable.setWidths(new float[]{65,35});
				  } 
				  catch (DocumentException e1) {
					e1.printStackTrace();
				  }
			  
			  parentbanktable.addCell(descparent);
			  parentbanktable.addCell(authparent);
			  
			  logger.log(Level.SEVERE," ....footer..5...");
			  Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
			  PdfPTable disclaimertable = new PdfPTable(1);
			  disclaimertable.setWidthPercentage(100);
			  Phrase disclaimerphrase = new Phrase(disclaimerText,font1);
					
			  PdfPCell disclaimercell = new PdfPCell(disclaimerphrase);
			  disclaimertable.addCell(disclaimercell);
			  
			  Phrase refphrase=new Phrase("Annexure 1 ",font10bold);
			  Paragraph repara=new Paragraph(refphrase);
			  
			  
			  logger.log(Level.SEVERE," ....footer..5...");
			  try {
					document.add(parentbanktable);
					 document.add(Chunk.NEWLINE);
					 document.add(disclaimertable);
					
					 logger.log(Level.SEVERE,"docu after ....in footer....");

					 System.out.println(" sales products isssssssssss"+this.salesProd.size());
					 
					 
					 logger.log(Level.SEVERE,"Product Size() == "+this.salesProd.size());
					 
				if(this.salesProd.size() > 14){
					 logger.log(Level.SEVERE,"Product Size()1 == "+this.salesProd.size());
//				if(eva>20){	
				document.newPage();
				document.add(repara);
				settingRemainingRowsToPDF(15);
				}if(this.salesProd.size() > 74){
//				}if(eva>80){
					document.newPage();
					document.add(repara);
				settingRemainingRowsToPDF(75);
				}if(this.salesProd.size() > 134){
//				}if(eva>140){	
					document.newPage();
					document.add(repara);
				settingRemainingRowsToPDF(135);
				}if(this.salesProd.size() > 194){
//				}if(eva>200){	
					document.newPage();
					document.add(repara);
				settingRemainingRowsToPDF(195);
				}
				
				if(payTerms.get(0).getPayTermPercent()!=100){
				
				document.add(Chunk.NEXTPAGE);
				
				PdfPCell parentcell=new PdfPCell(chargetaxtableToNewPage);
				
				PdfPTable chargetaxtableparent=new PdfPTable(1);
				chargetaxtableparent.addCell(parentcell);
				chargetaxtableparent.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				PdfPCell netPayableTablecell=new PdfPCell(netPayableTable);
				
				PdfPTable parentnetPayableTable=new PdfPTable(1);
				parentnetPayableTable.addCell(netPayableTablecell);
				parentnetPayableTable.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				document.add(chargetaxtableparent);
				document.add(parentnetPayableTable);
				
				}
//				if(this.salesProd.size()>16){
////					document.add(endpara);
//					}
					
				} 
				catch (DocumentException e) {
					e.printStackTrace();
				}
//			  int firstBreakPoint=5;
//				int cnt= this.billingChargesLis.size()+this.billingTaxesLis.size();
//				System.out.println("cnt value==="+cnt);
//				if(cnt > firstBreakPoint){
//		      
//		      otherchargestoAnnexur();
//				}
			  
	  }
	  
	  
	  private void otherchargestoAnnexur() {
			
//			Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
//			String charge ="Other charge details";
//			Chunk prodchunk = new Chunk(charge,font10bold);
//			Paragraph parag= new Paragraph();
//			parag.add(prodchunk);
//			
//			
//			PdfPTable chargetable = new PdfPTable(1);
//			chargetable.setWidthPercentage(100);
//			//********************
//			double total=0;
//			 
//			 for(int i=0;i<this.billingChargesLis.size();i++)
//			      {
//			   	   Phrase chunk = null;
//			   	   Phrase chunk1 = new Phrase("",font8);
//			   	   double chargeAmt=0;
//			   	   PdfPCell pdfchargeamtcell = null;
//			   	   PdfPCell pdfchargecell = null;
//			   	   if(billingChargesLis.get(i).getTaxChargePercent()!=0){
//			   		   chunk = new Phrase(billingChargesLis.get(i).getTaxChargeName()+" @ "+billingChargesLis.get(i).getTaxChargePercent(),font1);
//			   		   pdfchargecell=new PdfPCell(chunk);
//			   		   chargeAmt=billingChargesLis.get(i).getTaxChargePercent()*billingChargesLis.get(i).getTaxChargeAssesVal()/100;
//			   		   chunk=new Phrase(df.format(chargeAmt),font8);
//			  	      	   pdfchargeamtcell = new PdfPCell(chunk);
//			  	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			  	      	   pdfchargeamtcell.setBorder(0);
//			   	   }else{
//			   		pdfchargecell=new PdfPCell(chunk1);
//			   		pdfchargeamtcell = new PdfPCell(chunk1);
//			   	   }
//			   	   if(billingChargesLis.get(i).getTaxChargeAbsVal()!=0){
//			   		   chunk = new Phrase(billingChargesLis.get(i).getTaxChargeName()+"",font1);
//			   		   pdfchargecell=new PdfPCell(chunk);
//			   		   chargeAmt=billingChargesLis.get(i).getTaxChargeAbsVal();
//			   		   chunk=new Phrase(chargeAmt+"",font8);
//			  	      	   pdfchargeamtcell = new PdfPCell(chunk);
//			  	      	   pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			  	      	   pdfchargeamtcell.setBorder(0);
//			   	   }else{
//			   		pdfchargecell=new PdfPCell(chunk1);
//			   		pdfchargeamtcell = new PdfPCell(chunk1);
//			   	   }
//			 	       pdfchargecell.setBorder(0);
//			 	      pdfchargeamtcell.setBorder(0);
//			 	     chargetable.addCell(pdfchargecell);
//			 	      chargetable.addCell(pdfchargeamtcell);
//			      }
//			 
//			 Phrase totalchunk = new Phrase("Total                                                                      "+
//			 "                                       "+df.format(total),font10bold);
//				PdfPCell totalcell= new PdfPCell();
//				totalcell.addElement(totalchunk);
//			 
//			 PdfPCell chargecell= new PdfPCell();
//			 chargecell.addElement(chargetable);
//			 chargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			 
//			 
//			 PdfPTable parent= new PdfPTable(1);
//			 parent.setWidthPercentage(70);
//			 parent.setSpacingBefore(10f);
//			 parent.addCell(chargecell);
//			 parent.addCell(totalcell);
//			 parent.setHorizontalAlignment(Element.ALIGN_LEFT);
//			 
//			 try {
//				    document.add(Chunk.NEWLINE);
//				 	document.add(Chunk.NEWLINE);
//					document.add(parag);
//					document.add(parent);
//				} catch (DocumentException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}	 
			
		  
		  Phrase nextpage=new Phrase(Chunk.NEXTPAGE);
			try {
				document.add(nextpage);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		  
		  

			
			Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
			String charge ="Other charge details";
			Chunk prodchunk = new Chunk(charge,font10bold);
			Paragraph parag= new Paragraph();
			parag.add(prodchunk);
			
			
			PdfPTable chargetable = new PdfPTable(2);
			chargetable.setWidthPercentage(100);
			//********************
			double total=0;
			 for(int i=0;i<this.billingChargesLis.size();i++)
		     {
		  	   Phrase chunk = null;
		  	   double chargeAmt=0;
		  	   Phrase blank11=new Phrase("",font1);
		  	   PdfPCell blankcell=new PdfPCell(blank11);
		  	   PdfPCell pdfchargeamtcell = null;
		  	   PdfPCell pdfchargecell = null;
		  	   if(billingChargesLis.get(i).getOtherChargesList().get(i).getTaxChargePercent()!=0){
		  		   chunk = new Phrase(billingChargesLis.get(i).getOtherChargesList().get(i).getTaxChargeName()+" @ "+billingChargesLis.get(i).getOtherChargesList().get(i).getTaxChargePercent(),font1);
		  		   pdfchargecell=new PdfPCell(chunk);
		  		pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		  		   chargeAmt=billingChargesLis.get(i).getOtherChargesList().get(i).getTaxChargePercent()*billingChargesLis.get(i).getOtherChargesList().get(i).getTaxChargeAssesVal()/100;
		  		   chunk=new Phrase(df.format(chargeAmt),font8);
		 	      	   pdfchargeamtcell = new PdfPCell(chunk);
		 	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		 	      	   pdfchargeamtcell.setBorder(0);
		 	      	   
		  	   }
		  	   if(billingChargesLis.get(i).getOtherChargesList().get(i).getTaxChargeAbsVal()!=0){
		  		   chunk = new Phrase(billingChargesLis.get(i).getOtherChargesList().get(i).getTaxChargeName()+"",font1);
		  		   pdfchargecell=new PdfPCell(chunk);
		  		
		  		pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		  		   chargeAmt=billingChargesLis.get(i).getOtherChargesList().get(i).getTaxChargeAbsVal();
		  		   chunk=new Phrase(chargeAmt+"",font8);
		 	      	   pdfchargeamtcell = new PdfPCell(chunk);
		 	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		 	      	   pdfchargeamtcell.setBorder(0);
		  	   }
		  	 pdfchargecell.setBorder(0);
		  	 
		  	total=total+chargeAmt;
		  	if(chargeAmt!=0){
		  	chargetable.addCell(pdfchargecell);
		  	chargetable.addCell(pdfchargeamtcell);
		  	}
//		  	else{
//		  		chargetable.addCell(blankcell);
//	  	  	chargetable.addCell(blankcell);
//		  	}
			      	
		     }
			 
			 PdfPTable taotatable= new PdfPTable(2);
			 taotatable.setWidthPercentage(100);
			 
			 Phrase totalchunk = new Phrase("Total",font10bold);
				PdfPCell totalcell= new PdfPCell(totalchunk);
//				totalcell.addElement(totalchunk);
				totalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				totalcell.setBorder(0);
				
			 Phrase toatlamt = new Phrase(df.format(total),font10bold);
			 PdfPCell totalcell1= new PdfPCell(toatlamt);
//			totalcell1.addElement(toatlamt);
			totalcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			totalcell1.setBorder(0);
			
			taotatable.addCell(totalcell); 
			taotatable.addCell(totalcell1); 
			
			 PdfPCell chargecell= new PdfPCell();
			 chargecell.addElement(chargetable);
//			 chargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 
			 PdfPCell chargecell1= new PdfPCell();
			 chargecell1.addElement(taotatable);
			 
			 
			 PdfPTable parent= new PdfPTable(1);
			 parent.setWidthPercentage(70);
			 parent.setSpacingBefore(10f);
			 parent.addCell(chargecell);
			 parent.addCell(chargecell1);
			 parent.setHorizontalAlignment(Element.ALIGN_LEFT);
			 
			 try {
				    document.add(Chunk.NEWLINE);
				 	document.add(Chunk.NEWLINE);
					document.add(parag);
					document.add(parent);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	 
			
		
		  
		  
		
	}
	  
	  /**Manisha update the spelling of forty**/
		 private static final String[] tensNames = { "", " Ten", " Twenty", " Thirty", " Forty", " Fifty", " Sixty", " Seventy", " Eighty", " Ninety" };

		    private static final String[] numNames = { "", " One", " Two", " Three", " Four", " Five", " Six", " Seven", " Eight", " Nine", " Ten", " Eleven", " Twelve",
		    	" Thirteen", " Fourteen", " Fifteen", " Sixteen", " Seventeen", " Eighteen", " Nineteen" };


		    private static String convertLessThanOneThousand(int number) {
		    	String soFar;
		    	if (number % 100 < 20){ soFar = numNames[number % 100]; number /= 100; } else { soFar = numNames[number % 10]; number /= 10;
		    	soFar = tensNames[number % 10] + soFar; number /= 10; } if (number == 0) return soFar; return numNames[number] + " Hundred" + soFar; 
		    	}
		    	public static  String convert(double number) {
		    	// 0 to 999 999 999 999
		    	if (number == 0) { return "Zero"; }
		    	String snumber = Double.toString(number);
		    	// pad with "0"
		    	String mask = "000000000000"; DecimalFormat df = new DecimalFormat(mask); snumber = df.format(number);
		    	int hyndredCrore = Integer.parseInt(snumber.substring(3,5));
		    	int hundredLakh = Integer.parseInt(snumber.substring(5,7));
		    	int hundredThousands = Integer.parseInt(snumber.substring(7,9));
		    	int thousands = Integer.parseInt(snumber.substring(9,12));
		    	String tradBillions;
		    	switch (hyndredCrore) { case 0: tradBillions = ""; break; case 1 : tradBillions = convertLessThanOneThousand(hyndredCrore) + " Crore "; break; default : tradBillions = convertLessThanOneThousand(hyndredCrore) + " Crore "; }

		    	String result = tradBillions;
		    	String tradMillions;
		    	switch (hundredLakh) { case 0: tradMillions = ""; break; case 1 : tradMillions = convertLessThanOneThousand(hundredLakh) + " Lakh "; break; default : tradMillions = convertLessThanOneThousand(hundredLakh) + " Lakh "; }
		    	result = result + tradMillions;
		    	String tradHundredThousands;

		    	switch (hundredThousands) { case 0: tradHundredThousands = ""; break; case 1 : tradHundredThousands = "One Thousand "; break; default : tradHundredThousands = convertLessThanOneThousand(hundredThousands) + " Thousand "; }
		    	result = result + tradHundredThousands;

		    	String tradThousand;
		    	tradThousand = convertLessThanOneThousand(thousands);
		    	result = result + tradThousand;return result.replaceAll("^\\s+", "").replaceAll("file://b//s%7B2,%7D//b", " "); 
		    	}
		    	
		    	
		    	//
		    	
//		    	List<ProductDetailsPO> productcount =new ArrayList<ProductDetailsPO>();
//		  	  //
//		  	  	    public List<ProductDetailsPO> getdata(){
//		  	  	    	
//		  	  	    	ProductDetailsPO po=new ProductDetailsPO();
//		  	  	    	po.setProductCategory("category");
////		  	  	    	po.setPrduct("Product");
//		  	  	    	po.setProductQuantity(20);
//		  	  	    	po.setTax(65);
//		  	  	    	po.setVat(55);
//		  	  	    	po.setDiscount(22);
////		  	  	    	po.setTotal(50);
//		  	  	    	productcount.add(po);
//		  	  	    	return productcount;
//		  	  	    }
		    	
		    	//
		    	public void settingRemainingRowsToPDF(int flag){
		    		
		    		
		    		PdfPTable table = new PdfPTable(8);
		    		try {
		    			table.setWidths(columnWidths);
		    		} catch (DocumentException e1) {
		    			// TODO Auto-generated catch block
		    			e1.printStackTrace();
		    		}
		    		table.setWidthPercentage(100);
		    		
		    		
		    		Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
					Phrase productdetails= new Phrase("Product Details",font1);
					Paragraph para = new Paragraph();
					para.add(Chunk.NEWLINE);
					para.add(Chunk.NEWLINE);
					para.add(productdetails);
					para.setAlignment(Element.ALIGN_CENTER);
					para.add(Chunk.NEWLINE);
						
//					PdfPTable table = new PdfPTable(8);
//					table.setWidthPercentage(100);
					
					Phrase category = new Phrase("SR. NO. ",font1);
//					Phrase productcode = new Phrase("ITEM DETAILS",font1);
			        Phrase productname = new Phrase("ITEM DETAILS",font1);
			        Phrase qty = new Phrase("QTY",font1);
			        Phrase unit = new Phrase("UNIT",font1);
			        Phrase rate= new Phrase ("RATE",font1);
			        
			        Phrase percDisc = new Phrase("DISC %",font1);
			        Phrase adddisc = new Phrase("ADDL.DISC %",font1);
			        
			        Phrase servicetax = new Phrase("TAX",font1);
//			        Phrase vat = new Phrase("VAT",font1);
			        
			        Paragraph tax= new Paragraph();
				       //*************chnges mukesh on 22/4/2015******************** 
//				        SalesQuotation salesQu=null;
//				        if(qp instanceof SalesQuotation){
//				        	salesQu=(SalesQuotation)qp;
//				        }
				        Phrase svat=null;
				        Phrase vvat=null;
				        Phrase vat=null;
				        int cout=0;
				        int flag1=0;
				        int st=0;
				        int st1=0;
//				        for(int i=0;i<this.prodTaxes.size();i++){
//				        	
//				        }
				        
				        
				      
				        for(int i=0;i<this.salesProd.size();i++) {
				        	 
				        	int cstFlag=0;
				        	if(invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
				        		if(salesOrderEntity.getCformstatus()!=null){
				        		if(salesOrderEntity.getCformstatus().trim().equals(AppConstants.YES)||salesOrderEntity.getCformstatus().trim().equals(AppConstants.NO))
				        		{
				        			cstFlag=1;
				        		}
				        		}
				        	}
				        	
				        	if(invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
				        		if(purchaseOrder.getcForm()!=null){
				        		if(purchaseOrder.getcForm().trim().equals(AppConstants.YES)||purchaseOrder.getcForm().trim().equals(AppConstants.NO))
				        		{
				        			cstFlag=1;
				        		}
				        		}
				        	}
				        	
				        	
//				        	Phrase vatphrase=new Phrase("VAT",font1);
//				             Phrase Cstphrase=new Phrase("CST",font1);
//				             Phrase Stphrase=new Phrase("ST",font1);
				        	
				        	
				             
//				             if(salesProd.get(i).getServiceTax().getPercentage()>0&&salesProd.get(i).getVatTax().getPercentage()>0&&cstFlag==0){
//				            	 vvat = new Phrase("VAT / ST %",font1);
//				            	 cout=1;
//				             }
//				            else if(salesProd.get(i).getServiceTax().getPercentage()>0&&salesProd.get(i).getVatTax().getPercentage()==0&&cstFlag==0){
//				         	    vat = new Phrase("ST %",font1);
//				         	   st=1;
//				            }
//				            else if(salesProd.get(i).getServiceTax().getPercentage()==0&&salesProd.get(i).getVatTax().getPercentage()>0&&cstFlag==0){
//				            	 vat = new Phrase("VAT %",font1);
//				            	 st1=1;
//				            }else if(cstFlag==1 &&salesProd.get(i).getServiceTax().getPercentage()>0){
//				            	svat = new Phrase("CST / ST %",font1);
//				             	   flag1=1;
//				            } 
//				            else if(cstFlag==1 &&salesProd.get(i).getServiceTax().getPercentage()==0){
//				         	    vat = new Phrase("CST %",font1);
//				        
//				            }
//				            else{
//				                	 vat = new Phrase("TAX %",font1);
//				                }
//				             
				             	/**
					        	 * Date 31-08-2017 above old code commented below new code added by vijay for PO and sales PDF
					        	 */
				             
				            if(salesProd.get(i).getServiceTax().getPercentage()>0&&salesProd.get(i).getVatTax().getPercentage()>0&&cstFlag==0){
				            	 vvat = new Phrase("TAX 1 / TAX 2 %",font1);
				            	 cout=1;
				             }
				            else if(salesProd.get(i).getServiceTax().getPercentage()>0&&salesProd.get(i).getVatTax().getPercentage()==0&&cstFlag==0){
				         	    vat = new Phrase("TAX 1 %",font1);
				         	   st=1;
				            }
				            else if(salesProd.get(i).getServiceTax().getPercentage()==0&&salesProd.get(i).getVatTax().getPercentage()>0&&cstFlag==0){
				            	 vat = new Phrase("TAX 1 %",font1);
				            	 st1=1;
				            }else if(cstFlag==1 &&salesProd.get(i).getServiceTax().getPercentage()>0){
				            	svat = new Phrase("TAX 1 / TAX 2 %",font1);
				             	   flag1=1;
				            } 
				            else if(cstFlag==1 &&salesProd.get(i).getServiceTax().getPercentage()==0){
				         	    vat = new Phrase("TAX 1 %",font1);
				        
				            }
				            else{
				                	 vat = new Phrase("TAX %",font1);
				                }
				             
				        }
				        Phrase stvat=null;
				        if(cout>0){
				        	tax.add(vvat);
				        	tax.setAlignment(Element.ALIGN_CENTER);
				        }else if(st==1&&st1==1){
				         	
//				         	stvat=new Phrase("VAT / ST %",font1);
				         	stvat=new Phrase("TAX 1 / TAX 2 %",font1);

				         	tax.add(stvat);
				         	tax.setAlignment(Element.ALIGN_CENTER);
				         }else if(flag1>0){
				        	tax.add(svat);
				        	tax.setAlignment(Element.ALIGN_CENTER);
				        }else{
				        tax.add(vat);
				        tax.setAlignment(Element.ALIGN_CENTER);
				        }
			        
			        
			        
			        
			        
			        
			        Phrase total = new Phrase("AMOUNT",font1);
					
			        
					 PdfPCell cellcategory = new PdfPCell(category);
					 cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
//					 PdfPCell cellproductcode = new PdfPCell(productcode);
			         PdfPCell cellproductname = new PdfPCell(productname);
			         cellproductname.setHorizontalAlignment(Element.ALIGN_CENTER);
			         PdfPCell cellqty = new PdfPCell(qty);
			         cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);
			         PdfPCell cellunit = new PdfPCell(unit);
			         cellunit.setHorizontalAlignment(Element.ALIGN_CENTER);
			         PdfPCell cellrate = new PdfPCell(rate);
			         cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
			         PdfPCell percDisccell = new PdfPCell(percDisc);
			         percDisccell.setHorizontalAlignment(Element.ALIGN_CENTER);
			         PdfPCell cellservicetax = new PdfPCell(tax);
			         cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
//			         PdfPCell cellvat = new PdfPCell(vat);
			         
			         PdfPCell celltotal= new PdfPCell(total);
			         celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);
			         
			         table.addCell(cellcategory);
//			         table.addCell(cellproductcode);
			         table.addCell(cellproductname);
			         table.addCell(cellqty);
			         table.addCell(cellunit);
			         table.addCell(cellrate);
			         table.addCell(percDisccell);
			         table.addCell(cellservicetax);
			         table.addCell(celltotal);

			         
			         
			         ////
//			         Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
//				     	Phrase category = new Phrase("Category ", font1);
////				     	Phrase product = new Phrase("Product ", font1);
//				     	Phrase qty = new Phrase("Quantity", font1);
////				     	Phrase rate = new Phrase("Rate", font1);
//				     	Phrase servicetax = new Phrase("Service Tax", font1);
//				     	Phrase vat = new Phrase("VAT", font1);
//				     	Phrase percdisc = new Phrase("% Discount", font1);
////				     	Phrase total = new Phrase("Total", font1);
//			
//				     	PdfPCell cellcategory = new PdfPCell(category);
////				     	PdfPCell cellproduct = new PdfPCell(product);
//				     	PdfPCell cellqty = new PdfPCell(qty);
////				     	PdfPCell cellrate = new PdfPCell(rate);
//				     	PdfPCell cellperdisc = new PdfPCell(percdisc);
//				     	PdfPCell cellvat = new PdfPCell(vat);
//				     	PdfPCell cellservicetax = new PdfPCell(servicetax);
////				     	PdfPCell celltotal = new PdfPCell(total);
//			
//				     	table.addCell(cellcategory);
////				     	table.addCell(cellproduct);
//				     	table.addCell(cellqty);
////				     	table.addCell(cellrate);
//				     	table.addCell(cellperdisc);
//				     	table.addCell(cellvat);
//				     	table.addCell(cellservicetax);
				 		
			         
			         ///
			         
		    		for (int i = flag; i < this.salesProd.size(); i++) {
		    			
		    			Phrase chunk=new Phrase((i+1)+"",font8);
			        	 PdfPCell pdfcategcell = new PdfPCell(chunk);
			        	 pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			        	 
//			        	 chunk=new Phrase(salesProd.get(i).getProdCode(),font8);
//			        	 PdfPCell pdfcpcodecell = new PdfPCell(chunk);
			        	 
			        	 chunk = new Phrase(salesProd.get(i).getProdName(),font8);
			        	 PdfPCell pdfnamecell = new PdfPCell(chunk);
			        	 
			        	 chunk = new Phrase(salesProd.get(i).getQuantity()+"",font8);
			        	 PdfPCell pdfqtycell = new PdfPCell(chunk);
			        	 pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);
			        	 
			        	 chunk = new Phrase(salesProd.get(i).getUnitOfMeasurement(),font8);
			        	 PdfPCell pdfunitcell = new PdfPCell(chunk);
			        	 pdfunitcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			        	 
			        	 chunk = new Phrase(df.format(salesProd.get(i).getPrice()),font8);
			        	 PdfPCell pdfspricecell = new PdfPCell(chunk);
			        	 pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			        	 
//			        	 if(salesProd.get(i).getServiceTax()!=null)
//			         	    chunk = new Phrase(salesProd.get(i).getServiceTax().getPercentage()+"",font8);
//			         	 else
//			         		 chunk = new Phrase("N.A"+"",font8);
//			         	 
//			         	 PdfPCell pdfservice = new PdfPCell(chunk);
//			         	pdfservice.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			         	 if(salesProd.get(i).getVatTax()!=null)
//			         	    chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+"",font8);
//			         	 else
//			         		 chunk = new Phrase("N.A"+"",font8);
//			         	 PdfPCell pdfvattax = new PdfPCell(chunk);
			         	////
			         	
			         	
			        	 PdfPCell pdfservice1=null;
			        	 //******************************
			        	 double cstval=0;
			        	 int cstFlag=0;
			         	if(invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
			         		if(salesOrderEntity.getCformstatus()!=null){
			         		if(salesOrderEntity.getCformstatus().trim().equals(AppConstants.YES)||salesOrderEntity.getCformstatus().trim().equals(AppConstants.NO))
			         		{
			         			cstFlag=1;
			         			cstval=salesOrderEntity.getCstpercent();
			         		}
			         		}
			         	}
			         	
			         	if(invoiceentity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
			         		if(purchaseOrder.getcForm()!=null){
			         		if(purchaseOrder.getcForm().trim().equals(AppConstants.YES)||purchaseOrder.getcForm().trim().equals(AppConstants.NO))
			         		{
			         			cstFlag=1;
			         			cstval=purchaseOrder.getCstpercent();
			         		}
			         		}
			         	}
			         	
			        	 
			        	 
			        	 
			        	  if((salesProd.get(i).getVatTax().getPercentage()>0)&& (salesProd.get(i).getServiceTax().getPercentage()>0)&&(cstFlag==0)){
			              	   
			        		  System.out.println("two");
			        		  if((salesProd.get(i).getVatTax()!=null)&&(salesProd.get(i).getServiceTax()!=null)){
			        			  if(cout>0){
			            			  chunk = new Phrase(df.format(salesProd.get(i).getVatTax().getPercentage())+" / "+df.format(salesProd.get(i).getServiceTax().getPercentage()),font8);
			            			  }
			        		  }else{
			                		
			        			  chunk = new Phrase("0"+"",font8);
			        		  }
			        			  pdfservice1 = new PdfPCell(chunk);
			           		 
			           		  
			           		  pdfservice1 = new PdfPCell(chunk);
			                	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			        		  
			        		  
			        		  
			        	  }
			             else if((salesProd.get(i).getServiceTax().getPercentage()>0) && (salesProd.get(i).getVatTax().getPercentage()==0&&cstFlag==0)){
			            	
			            	 System.out.println("three");
			            	 if(salesProd.get(i).getServiceTax()!=null){
			            		if(flag1==1||cout==1){
			            				chunk = new Phrase(df.format(salesProd.get(i).getServiceTax().getPercentage())+" / "+"0.00",font8);
			            		}else if(st==1&&st1==1){
			            			chunk = new Phrase("0.00"+" / "+df.format(salesProd.get(i).getServiceTax().getPercentage()),font8);
			            		}else{
			            				chunk = new Phrase(df.format(salesProd.get(i).getServiceTax().getPercentage()),font8);
			            		}
			            		
			            	 }else{
			             		 chunk = new Phrase("0.00",font8);
			             	 }
			             	  pdfservice1 = new PdfPCell(chunk);
			             	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			             	 
			             }
			             else if((salesProd.get(i).getServiceTax().getPercentage()>0) &&(salesProd.get(i).getVatTax().getPercentage()>0)&&cstFlag==1){
			            	 System.out.println("four");
			            	
			            	 		
			            	 	chunk = new Phrase(df.format(cstval)+" / "+df.format(salesProd.get(i).getServiceTax().getPercentage()),font8);
			            		 
			            		 pdfservice1 = new PdfPCell(chunk);
			                 	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			            	 
			            	
			             }
			             else if(cstFlag==1&&(salesProd.get(i).getServiceTax().getPercentage()==0)
			            		 &&(salesProd.get(i).getVatTax().getPercentage()==0)){
			            	 
			            	 System.out.println("five");
			            	 if(flag1==1||cout==1){
			            		 chunk = new Phrase(df.format(cstval)+" / "+"0.00",font8);
			            	 }else{
			            		 chunk = new Phrase(df.format(cstval),font8);
			            	 }
			            		 
			            		 
			            		 pdfservice1 = new PdfPCell(chunk);
			                 	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			            	 
			             } 
			             else if(cstFlag==1&&salesProd.get(i).getServiceTax().getPercentage()>0){
			          	  
			            	 System.out.println("six");
			            	 chunk = new Phrase(df.format(cstval)+" / "+
			            			 df.format(salesProd.get(i).getServiceTax().getPercentage()),font8);
			            		 
			            		 pdfservice1 = new PdfPCell(chunk);
			                 	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			            	 
			             } 
			             else if((cstFlag==1&&(salesProd.get(i).getServiceTax().getPercentage()==0)&&(salesProd.get(i).getVatTax().getPercentage()>0))){
			          	   
			            	 System.out.println("seven");
			            	 if(flag1==1||cout==1){
			            		 chunk = new Phrase(df.format(cstval)+" / "+"0.00",font8);
			            	 }else{
			            		 chunk = new Phrase(df.format(cstval),font8);
			            	 }
			            	
			      			  
//			      		 chunk = new Phrase("0"+"",font8);
			      	  pdfservice1 = new PdfPCell(chunk);
			      	  
			       	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			            	 
			             }else if((salesProd.get(i).getServiceTax().getPercentage()==0)&&(salesProd.get(i).getVatTax().getPercentage()>0)&&(cstFlag==0)){
			            	 System.out.println("eight");
			            	 if(flag1==1||cout==1){
			            		 chunk = new Phrase(df.format(salesProd.get(i).getVatTax().getPercentage())+" / "+"0.00",font8);
			            	 }else if(st==1&&st1==1){
			            		 chunk = new Phrase(df.format(salesProd.get(i).getVatTax().getPercentage())+" / "+"0.00",font8);
			            	 }else{
			            		 chunk = new Phrase(df.format(salesProd.get(i).getVatTax().getPercentage()),font8);
			            	 }
			            		 pdfservice1 = new PdfPCell(chunk);
			                 	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			                 	 
				  
			             }else{
			            		 chunk= new Phrase("0.00 / 0.00",font8);
			            	 pdfservice1 = new PdfPCell(chunk);
			             }
			         	
			         	
			         	
			         	 /////
			         	 if(salesProd.get(i).getProdPercDiscount()!=0)
			          	    chunk = new Phrase(salesProd.get(i).getProdPercDiscount()+"",font8);
			          	 else
			          		 chunk = new Phrase("0"+"",font8);
			          	 PdfPCell pdfperdiscount = new PdfPCell(chunk);
			          	pdfperdiscount.setHorizontalAlignment(Element.ALIGN_RIGHT);
			         	
			        	 chunk = new Phrase(df.format(salesProd.get(i).getTotalAmount()),font8);
			        	 PdfPCell pdftotalproduct = new PdfPCell(chunk);
			        	 pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);
			        	 
			        	 
			        	 table.addCell(pdfcategcell);
//			        	 table.addCell(pdfcpcodecell);
			        	 table.addCell(pdfnamecell);
			        	 table.addCell(pdfqtycell);
			        	 table.addCell(pdfunitcell);
			        	 table.addCell(pdfspricecell);
			        	 table.addCell(pdfperdiscount);
			        	 pdfservice1.setHorizontalAlignment(Element.ALIGN_CENTER);
			        	 table.addCell(pdfservice1);
//			        	 table.addCell(pdfvattax);
			        	 
			        	 table.addCell(pdftotalproduct);
			        	 try {
							table.setWidths(columnWidths);
						} catch (DocumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			        	 
			        	 ///
//			        	 
//			        	 for(int i =0;i<eva;i++){
//			  				if(getdata().get(i).getProductCategory()!=null){
//			 					Phrase chunk = new Phrase(getdata().get(i).getProductCategory(),font8);
//			 					
//			 					}else{
//			 					}
//			 					PdfPCell pdfcategcell = new PdfPCell(chunk);
//			 		
//			 					
//			 					
//			 					
//			 					if(getdata().get(i).getProductQuantity()!=0){
//			 					chunk = new Phrase(getdata().get(i).getProductQuantity() + "", font8);
//			 					}
//			 					else{
//			 						chunk = new Phrase("", font8);
//			 					}
//			 					PdfPCell pdfqtycell = new PdfPCell(chunk);
//			 					
//			 					
//			 					
//			 					if(getdata().get(i).getTax()!=0){
//			 						chunk = new Phrase(getdata().get(i).getTax() + "", font8);
//			 					}else{
//			 						chunk = new Phrase("", font8);
//			 					}
//			 					PdfPCell pdfservice = new PdfPCell(chunk);
//			 		
//			 					
//			 					
//			 					
//			 					if(getdata().get(i).getVat()!=0){
//			 					
//			 						chunk = new Phrase(getdata().get(i).getVat() + "", font8);
//			 					}else{
//			 						chunk = new Phrase( "", font8);
//			 					}
//			 					PdfPCell pdfvattax = new PdfPCell(chunk);
//			 					
//			 					
//			 					
//			 					
//			 					if(getdata().get(i).getDiscount()!=0)
//			 					{
//			 						chunk = new Phrase(getdata().get(i).getDiscount() + "", font8);
//			 				
//			 						}else{
//			 							chunk = new Phrase(getdata().get(i).getDiscount() + "", font8);
//			 						}	
//			 					
//			 					PdfPCell pdfperdiscount = new PdfPCell(chunk);
//			 		
//			 						table.addCell(pdfcategcell);
//			 						table.addCell(pdfqtycell);
//			 						table.addCell(pdfperdiscount);
//			 						table.addCell(pdfvattax);
//			 						table.addCell(pdfservice);
			        	 
			        	 ///
		    			
		    			flag=flag+1;
		    			if(flag==78||flag==138||flag==198){
		    			break;
		    			}
		    		}
		    		
		    		PdfPTable parentTableProd=new PdfPTable(1);
		    	    parentTableProd.setWidthPercentage(100);
		    	    parentTableProd.setSpacingBefore(10f);
		    	    PdfPCell prodtablecell=new PdfPCell();
		    	    prodtablecell.addElement(table);
		    	    parentTableProd.addCell(prodtablecell);
		    	   
		    		
		    		try {
		    			document.add(parentTableProd);
		    			
		    		} catch (DocumentException e) {
		    			e.printStackTrace();
		    		}
		    	}
		    	
		    	
		    	public String getInvComment() throws Exception
		    	{
		    		logger.log(Level.SEVERE,"get invoice 1.....");
		    		String commnt="";
		    		if(invoiceentity!=null){
		    			logger.log(Level.SEVERE,"get invoice 2.....");
		    			commnt=invoiceentity.getComment();
		    			logger.log(Level.SEVERE,"get invoice 3.....");
		    		}
		    		logger.log(Level.SEVERE,"get invoice 4.....");
		    		return commnt;
		    		
		    	}
		    	}
