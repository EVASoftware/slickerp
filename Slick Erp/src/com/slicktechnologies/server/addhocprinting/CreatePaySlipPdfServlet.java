package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;

import static com.googlecode.objectify.ObjectifyService.ofy;


public class CreatePaySlipPdfServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3306680814991385226L;

	@Override
	 protected void doGet(HttpServletRequest request,
	   HttpServletResponse response) throws ServletException, IOException {
	  
		SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
		response.setContentType("application/pdf");  // Type of response , helps browser to identify the response type.
	  
		String stringid=request.getParameter("Id");
		stringid = stringid.trim();
		//Long companyId = Long.parseLong(stringid);
		Long count =Long.parseLong(stringid);
		CustomerPayment custPayEntity=ofy().load().type(CustomerPayment.class).id(count).now();
		
		//Ashwini Patil Date:02-05-2023
		
		if(custPayEntity!=null){			
			String filename="Payment - "+custPayEntity.getCount()+" - "+custPayEntity.getPersonInfo().getFullName();
			if(custPayEntity.getPaymentDate()!=null)
				filename+=" - "+fmt.format(custPayEntity.getPaymentDate())+".pdf";
			else
				filename+=	".pdf";
			
			if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Company", "EnablePrintPreview", custPayEntity.getCompanyId()))
					response.setHeader("Content-Disposition", "attachment; filename=" + filename);
			System.out.println("filename="+filename);			 
		}
		
	if(custPayEntity!=null&&ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment","OldVersionPdf", custPayEntity.getCompanyId())){ //CustomerPayment
	    System.out.println("OldVersionPdf");
		try {
//		  DeliveryNote delnoteEntity;
		 // String stringid=request.getParameter("Id");
		  // stringid=stringid.trim();
		   
		   String preprintStatus=request.getParameter("preprint");
		   
		//   Long count =Long.parseLong(stringid);
		//   CustomerPayment custPayEntity=ofy().load().type(CustomerPayment.class).id(count).now();
	  // CustomerPaySlipVersionOnePdf  custpaypdf=new CustomerPaySlipVersionOnePdf();
	   CustomerPaySlipPdf custpaypdf1=new CustomerPaySlipPdf();
	   custpaypdf1.document=new Document();
	   Document document = custpaypdf1.document;
	   PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); // write the pdf in response
	   
	   /**
		 * Added by Rahul on 16 June 2017
		 * this will add waterMark in Pdf if pdf is in created state
		 */
	   if(custPayEntity.getStatus().equals(CustomerPayment.CANCELLED)){//By jayshree Date 16/11/2017 Changes are made to add the cancelled watermark
		   
		   writer.setPageEvent(new PdfCancelWatermark());	
	   }
	   
	   if(!custPayEntity.getStatus().equals("Closed")){
		   writer.setPageEvent(new PdfWatermark());	
	   }
	   document.open();
	   
	   custpaypdf1.setCustomerPaySlip(count);
	   custpaypdf1.createPdf(preprintStatus);
	   document.close();
	  } 
	 
	  
	  catch (DocumentException e) {
		  e.printStackTrace();
	  }
	  
	}else{
		
	
		  try {
//			  DeliveryNote delnoteEntity;
			 // String stringid=request.getParameter("Id");
			 //  stringid=stringid.trim();
			   
			   String preprintStatus=request.getParameter("preprint");
			   
			  // Long count =Long.parseLong(stringid);
			   //CustomerPayment custPayEntity=ofy().load().type(CustomerPayment.class).id(count).now();
		  CustomerPaySlipVersionOnePdf  custpaypdf=new CustomerPaySlipVersionOnePdf();
		  // CustomerPaySlipPdf custpaypdf=new CustomerPaySlipPdf();
		   custpaypdf.document=new Document();
		   Document document = custpaypdf.document;
		   PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); // write the pdf in response
		   
		   /**
			 * Added by Rahul on 16 June 2017
			 * this will add waterMark in Pdf if pdf is in created state
			 */
		   if(custPayEntity.getStatus().equals(CustomerPayment.CANCELLED)){//By jayshree Date 16/11/2017 Changes are made to add the cancelled watermark
			   
			   writer.setPageEvent(new PdfCancelWatermark());	
		   }
		   
		   if(!custPayEntity.getStatus().equals("Closed")){
			   writer.setPageEvent(new PdfWatermark());	
		   }
		   document.open();
		   
		   custpaypdf.setCustomerPaySlip(count);
		   custpaypdf.createPdf(preprintStatus);
		   document.close();
		  } 
		 
		  
		  catch (DocumentException e) {
			  e.printStackTrace();
		  }
		  
		
	}
	 
	 }
}
	

//}
