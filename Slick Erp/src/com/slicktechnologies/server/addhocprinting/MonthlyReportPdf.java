package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.views.device.CatchTraps;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

public class MonthlyReportPdf {

	class ProductStatus {
		String productName;
		String productStatus;

		public ProductStatus() {

		}

		public String getProductName() {
			return productName;
		}

		public void setProductName(String productName) {
			this.productName = productName;
		}

		public String getProductStatus() {
			return productStatus;
		}

		public void setProductStatus(String productStatus) {
			this.productStatus = productStatus;
		}
	}

	public Document document;
	private Font font16boldul, font12bold, font8bold, font8, font9bold,
			font12boldul, font10boldul, font12, font16bold, font10, font10bold,
			font14bold;
	Contract con;
	Company comp;
	Customer cust;

	Logger logger = Logger.getLogger("NameOfYourLogger");
	Date fromDate;
	Date toDate;


	List<Service> serviceLis = new ArrayList<Service>();
	/**
	 * Date 18/12/2017
	 * By Jayshree
	 * Des.to add the branch code branch list is load
	 */
	List<Branch> branchList=new ArrayList<Branch>();//By Jayshree
//end
	
	/**
	 * This map has date as key and inner hash map as value inner hash map has
	 * trap name as key and its count as value
	 */

//	HashMap<String, HashMap<String, Integer>> hashmap = new HashMap<String, HashMap<String, Integer>>();

	/**
	 * This has map contains date as key and ProductStatus as value i.e per day
	 * holds services of no. of product
	 */
//	HashMap<String, ArrayList<ProductStatus>> productMap = new HashMap<String, ArrayList<ProductStatus>>();

	/**
	 * This hashset stores the unique list of trapcathes for given duration and
	 * for given contarct
	 */
//	HashSet<String> hash = new HashSet<String>();

	/**
	 * List contains the unique name of trap catches form hashset.
	 */
	List<String> list = new ArrayList<String>();

	float[] columnWidths = { 1.2f, 1.8f, 1f, 1.5f, 1.2f, 1.2f, 1f, 1.5f, 1f,1.5f };
	float[] relativeWidths1 = { 3.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f,0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f };
	float[] columnWidthsFor2Column = { 1.5f, 8.5f };
	float[] columnWidthsFor2Column1 = { 1.0f, 9.0f };

	BaseColor mycolor = WebColors.getRGBColor("#C0C0C0");
	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

	public MonthlyReportPdf() {
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	}

	public void createMontlyReport(Long count, Date fromDate, Date toDate) {

		this.fromDate = fromDate;
		this.toDate = toDate;

		// Load Invoice
		con = ofy().load().type(Contract.class).id(count).now();

		if (con.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class).filter("companyId", con.getCompanyId()).first().now();

		// load customer form here
		if (con.getCompanyId() == null)
			cust = ofy().load().type(Customer.class).filter("count", con.getCustomerId()).first().now();
		else
			cust = ofy().load().type(Customer.class)
					.filter("count", con.getCustomerId())
					.filter("companyId", con.getCompanyId()).first().now();

		// System.out.println("From date " + fromDate);
		// System.out.println("Todate " + toDate);

		logger.log(Level.SEVERE, "form date size :::" + fromDate);
		logger.log(Level.SEVERE, "to date size :::" + toDate);

		if (con.getCompanyId() == null){
			serviceLis = ofy().load().type(Service.class)
					.filter("contractCount", con.getCount())
					.filter("serviceDate >=", fromDate)
					.filter("serviceDate <=", toDate).list();
		}else{
			serviceLis = ofy().load().type(Service.class)
					.filter("contractCount", con.getCount())
					.filter("serviceDate >=", fromDate)
					.filter("serviceDate <=", toDate)
					.filter("companyId", con.getCompanyId()).list();
		}

		logger.log(Level.SEVERE, "serviceLis size :::" + serviceLis.size());
//		getHashMapLoad();

		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		/**
		 * Date 18/12/2017
		 * By Jayshree
		 * Des.to add the branch code branch list is load
		 */
		
		branchList=ofy().load().type(Branch.class).filter("companyId",con.getCompanyId()).filter("buisnessUnitName",con.getBranch()).list();
//end
	}

//	private void getHashMapLoad() {
//
//		for (int i = 0; i < serviceLis.size(); i++) {
//
//			ArrayList<ProductStatus> productName = new ArrayList<ProductStatus>();
//			if (productMap.size() == 0) {
//				ProductStatus ps = new ProductStatus();
//				ps.setProductName(serviceLis.get(i).getProductName());
//				ps.setProductStatus(serviceLis.get(i).getStatus());
//				productName.add(ps);
//				productMap.put(sdf.format(serviceLis.get(i).getServiceDate()),productName);
//			} else {
//
//				ProductStatus ps = new ProductStatus();
//				ps.setProductName(serviceLis.get(i).getProductName());
//				ps.setProductStatus(serviceLis.get(i).getStatus());
//
//				if (productMap.containsKey(sdf.format(serviceLis.get(i).getServiceDate()))) {
//					productMap.get(sdf.format(serviceLis.get(i).getServiceDate())).add(ps);
//				} else {
//					productName.add(ps);
//					productMap.put(sdf.format(serviceLis.get(i).getServiceDate()),productName);
//				}
//			}
//
//			for (int k = 0; k < serviceLis.get(i).getCatchtrapList().size(); k++) {
//				hash.add(serviceLis.get(i).getCatchtrapList().get(k).getPestName());
//			}
//		}
//
//		list.addAll(hash);
//
//		// System.out.println("list size " + list.size());
//
//		// *************************** getting single service data
//		// **********************************************
//
//		Comparator<Service> serviceDate = new Comparator<Service>() {
//			@Override
//			public int compare(Service o1, Service o2) {
//				return o1.getServiceDate().compareTo(o2.getServiceDate());
//			}
//		};
//		Collections.sort(serviceLis, serviceDate);
//
//		
//
//		List<Service> oneDayServiceList = new ArrayList<Service>();
//		List<CatchTraps> traplist = new ArrayList<CatchTraps>();
//
//		Date strtDate = serviceLis.get(0).getServiceDate();
//		Date edDate = serviceLis.get(serviceLis.size() - 1).getServiceDate();
//
//		Calendar startDate = Calendar.getInstance();
//		Calendar endDate = Calendar.getInstance();
//
//		startDate.setTime(strtDate);
//		endDate.setTime(edDate);
//
////		logger.log(Level.SEVERE, "start date :::" + startDate.getTime());
////		logger.log(Level.SEVERE, "end date :::" + endDate.getTime());
//
//		// System.out.println("START DATE :: " + startDate.getTime());
//		// System.out.println("END DATE :: " + endDate.getTime());
//		// System.out.println();
//
//		while (startDate.getTime().before(endDate.getTime())
//				|| startDate.getTime().equals(endDate.getTime())) {
//
////			logger.log(Level.SEVERE, "date :::" + startDate.getTime());
//
//			HashMap<String, Integer> innerHashMap = new HashMap<String, Integer>();
//			oneDayServiceList = new ArrayList<Service>();
//			traplist = new ArrayList<CatchTraps>();
//			// System.out.println("Loop  DATE :: " + startDate.getTime());
//			// System.out.println();
//			// oneDayServiceList = getOneDayServices(startDate.getTime());
//
//			List<Service> serviceList = getOneDayServices(startDate.getTime());
//
////			logger.log(Level.SEVERE,"service list size :::" + serviceList.size());
//			if (serviceList.size() != 0) {
//				oneDayServiceList.addAll(serviceList);
//
//				// System.out.println();
//
//				// System.out.println("oneDayServiceList size"+
//				// oneDayServiceList.size());
//
//				for (int i = 0; i < oneDayServiceList.size(); i++) {
//					// System.out.println("Rohan trap list size"+
//					// oneDayServiceList.get(i).getCatchtrapList().size());
//					// System.out.println();
//					traplist.addAll(oneDayServiceList.get(i).getCatchtrapList());
//				}
//				// System.out.println();
//				// System.out.println("traplis size" + traplist.size());
//
//				int qty = 0;
//
//				for (int i = 0; i < list.size(); i++) {
//					qty = getTrappedQty(list.get(i), traplist);
//					// System.out.println("trap name and qty "+list.get(i)+"-" +
//					// qty);
//					innerHashMap.put(list.get(i), qty);
//				}
//				// System.out.println("innerHashMap size"+innerHashMap.size());
//
//				hashmap.put(sdf.format(startDate.getTime()), innerHashMap);
//
//				// System.out.println("outer HashMap size"+hashmap.size());
//
//				startDate.add(Calendar.DATE, 1);
//
//				// System.out.println();
//			}
//		}
//	}

	public void createPdf() {

//		if (comp.getUploadHeader() != null) {
			createCompanyNameAsHeader(document, comp);
//		}

//		if (comp.getUploadFooter() != null) {
			createCompanyNameAsFooter(document, comp);
//		}
		createBlankHeading();
		createCompanyDetails();
		createHeadingWithCustomeDetails();
		createSetviceDetailsTable();
		createObservationByNBHC();
		createSuggestionByCustomer();
	}

	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(10f);
			image2.scaleAbsoluteWidth(750f);
			image2.setAbsolutePosition(50f, 510f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/* try
		 {
		 Image
		 image1=Image.getInstance("images/NBHC_Header.jpg");
		 
		 image1.scalePercent(10f);
		 image1.scaleAbsoluteWidth(750f);
		 image1.setAbsolutePosition(50f,510f);
		 image1.setBorder(2);
		 
		 image1.setBorderWidth(1);
		 doc.add(image1);
		 }
		 catch(Exception e)
		 {
		 e.printStackTrace();
		 }*/
	}

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(750f);
			image2.setAbsolutePosition(40f, 40f);

			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/* try
		 {
		 Image
		 image1=Image.getInstance("images/NBHC_Footer.png");
//		 image1.scalePercent(15f);
//		 image1.scaleAbsoluteWidth(750f);
//		 image1.setAbsolutePosition(40f,30f);
		 
		 image1.scalePercent(15f);
		 image1.scaleAbsoluteWidth(750f);
		 image1.setAbsolutePosition(40f, 15f);
		 
		 doc.add(image1);
		 }
		 catch(Exception e)
		 {
		 e.printStackTrace();
		 }*/
	}

	private void createBlankHeading() {

		// System.out.println("Inside create blank heading");
		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);

		try {
			document.add(blank);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);
		blcell.setBorder(0);

		PdfPTable bltable = new PdfPTable(1);
//		bltable.addCell(blcell);
		bltable.addCell(blcell);
		bltable.setWidthPercentage(100f);

		try {
			document.add(bltable);
			// System.out.println("BLANK CELL ADDED IN DOCUMENT...");
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createCompanyDetails() {

		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);

		Phrase name = new Phrase(comp.getBusinessUnitName(), font10bold);
		PdfPCell nameCell = new PdfPCell(name);
		nameCell.setBorder(0);
		nameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(nameCell);

		// Phrase hardcoded = new Phrase(
		// "Division: Commodity Care and Pest Management", font8bold);
		// PdfPCell hardcodedCell = new PdfPCell(hardcoded);
		// hardcodedCell.setBorder(0);
		// hardcodedCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// table.addCell(hardcodedCell);

		// / address *************

		String custAdd1 = "";
		String custFullAdd1 = "";

		if (comp.getAddress() != null) {

			if (!comp.getAddress().getAddrLine2().equals("")) {
				if (!comp.getAddress().getLandmark().equals("")) {
					custAdd1 = comp.getAddress().getAddrLine1() + ","
							+ comp.getAddress().getAddrLine2() + ","
							+ comp.getAddress().getLandmark();
				} else {
					custAdd1 = comp.getAddress().getAddrLine1() + ","
							+ comp.getAddress().getAddrLine2();
				}
			} else {
				if (!comp.getAddress().getLandmark().equals("")) {
					custAdd1 = comp.getAddress().getAddrLine1() + ","
							+ comp.getAddress().getLandmark();
				} else {
					custAdd1 = comp.getAddress().getAddrLine1();
				}
			}

			if (!comp.getAddress().getLocality().equals("")) {
				custFullAdd1 = custAdd1 + "," + comp.getAddress().getLocality()
						+ "," + comp.getAddress().getCity() + "-"
						+ comp.getAddress().getPin() + ","
						+ comp.getAddress().getState()
						+ comp.getAddress().getCountry();

			} else {
				custFullAdd1 = custAdd1 + "," + comp.getAddress().getCity()
						+ "-" + comp.getAddress().getPin() + ","
						+ comp.getAddress().getState() + ","
						+ comp.getAddress().getCountry();
			}
		}

		Phrase address = new Phrase("Registered Office: " + custFullAdd1, font8);
		PdfPCell addressCell = new PdfPCell(address);
		addressCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		addressCell.setBorder(0);
		table.addCell(addressCell);

		// ends here *******************

		// contact details ************

		String contactInfo = "Tel :" + comp.getCellNumber1();

		if (comp.getFaxNumber() != null) {
			contactInfo = contactInfo + " | Fax :" + comp.getFaxNumber();
		}

		if (comp.getEmail() != null) {
			contactInfo = contactInfo + " | Email :" + comp.getEmail();
		}
		if (comp.getWebsite() != null) {
			contactInfo = contactInfo + " | Website :" + comp.getWebsite();
		}

		Phrase contactDetailsInfo = new Phrase(contactInfo, font8);
		PdfPCell contactDetailsInfoCell = new PdfPCell(contactDetailsInfo);
		contactDetailsInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		contactDetailsInfoCell.setBorder(0);
		table.addCell(contactDetailsInfoCell);
		// ends here **********************

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createHeadingWithCustomeDetails() {

		Paragraph para = new Paragraph(
				"Monthly Pest Management Service Record", font10bold);
		para.setAlignment(Element.ALIGN_CENTER);
		try {
			document.add(para);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// code for customer Details

		Phrase srNO = new Phrase("Serial Number :", font8bold);
		PdfPCell srNOCell = new PdfPCell(srNO);
		srNOCell.setBorder(0);
		srNOCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		/**
		 * Date 16/12/2017
		 * By Jayshree
		 * To add the serial number as the combination of branch code and date 
		 */
		
		String branchCodeVal=null;
		Phrase srNOValue=null;
		Date currentdate=new Date();
		System.out.println("branchlistSize"+ branchList.size());
		for (int i = 0; i < branchList.size(); i++) {
			branchCodeVal=branchList.get(i).getBranchCode();
			System.out.println("Branchlist"+branchCodeVal);
		}
		
		if(branchCodeVal!=null&&!branchCodeVal.equals("")){
			System.out.println("branch present");
		
			srNOValue = new Phrase(branchCodeVal+"/"+con.getCount()+"/"+sdf.format(currentdate), font8);
		}
		else{
			System.out.println("code not present");
			srNOValue=new Phrase(con.getCount()+"",font8);
		}
		PdfPCell srNOValueCell = new PdfPCell(srNOValue);
		srNOValueCell.setBorder(0);
		srNOValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		//End by jayshree
		
		
		Phrase Location = new Phrase("Location : ", font8bold);
		PdfPCell LocationCell = new PdfPCell(Location);
		LocationCell.setBorder(0);
		LocationCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase LocationValue = new Phrase(cust.getAdress().getLocality(), font8);
		PdfPCell LocationValueCell = new PdfPCell(LocationValue);
		LocationValueCell.setBorder(0);
		LocationValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase custBranch = new Phrase("NBHC Branch : ", font8bold);
		PdfPCell custBranchCell = new PdfPCell(custBranch);
		custBranchCell.setBorder(0);
		custBranchCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase custBranchValue = new Phrase(con.getBranch(), font8);
		PdfPCell custBranchValueCell = new PdfPCell(custBranchValue);
		custBranchValueCell.setBorder(0);
		custBranchValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// get month and year form date

		Calendar now = Calendar.getInstance();
		now.setTime(fromDate);
		int yearNameValue = now.get(Calendar.YEAR);

		String[] monthName = { "January", "February", "March", "April", "May",
				"June", "July", "August", "September", "October", "November",
				"December" };

		String monthNameValue = monthName[now.get(Calendar.MONTH)];

		// System.out.println("Month name: " + monthNameValue);

		Phrase month = new Phrase("Month : ", font8bold);
		PdfPCell monthCell = new PdfPCell(month);
		monthCell.setBorder(0);
		monthCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase monthValue = new Phrase(monthNameValue, font8);
		PdfPCell monthValueCll = new PdfPCell(monthValue);
		monthValueCll.setBorder(0);
		monthValueCll.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase year = new Phrase("Year : ", font8bold);
		PdfPCell yearCell = new PdfPCell(year);
		yearCell.setBorder(0);
		yearCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase yearValue = new Phrase(yearNameValue + "", font8);
		PdfPCell yearValueCell = new PdfPCell(yearValue);
		yearValueCell.setBorder(0);
		yearValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable table = new PdfPTable(10);
		table.setWidthPercentage(100f);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		table.setSpacingBefore(10f);
		table.addCell(srNOCell);
		table.addCell(srNOValueCell);
		table.addCell(LocationCell);
		table.addCell(LocationValueCell);
		table.addCell(custBranchCell);
		table.addCell(custBranchValueCell);

		table.addCell(monthCell);
		table.addCell(monthValueCll);
		table.addCell(yearCell);
		table.addCell(yearValueCell);

		// customer Details
		Phrase custName = new Phrase("Customer        :", font8bold);
		PdfPCell custNameCell = new PdfPCell(custName);
		custNameCell.setBorder(0);
		custNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase custNameValue = new Phrase(con.getCustomerFullName(), font8);
		PdfPCell custNameValueCell = new PdfPCell(custNameValue);
		custNameValueCell.setBorder(0);
		custNameValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable custNameTable = new PdfPTable(2);
		custNameTable.setSpacingAfter(5f);
		custNameTable.setSpacingBefore(5f);
		custNameTable.setWidthPercentage(100f);
		try {
			custNameTable.setWidths(columnWidthsFor2Column1);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		custNameTable.addCell(custNameCell);
		custNameTable.addCell(custNameValueCell);

		try {
			document.add(table);
			document.add(custNameTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createSetviceDetailsTable() {

		// System.out.println("Servicelist Size" + serviceLis.size());
		PdfPTable table = new PdfPTable(17);
		table.setWidthPercentage(100f);
		table.setSpacingBefore(10f);

		try {
			table.setWidths(relativeWidths1);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase productName = new Phrase("Service Date", font8bold);
		PdfPCell productNameCell = new PdfPCell(productName);
		productNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		productNameCell.setBackgroundColor(mycolor);

		Phrase day1 = new Phrase("1", font8bold);
		PdfPCell day1Cell = new PdfPCell(day1);
		day1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day1Cell.setBackgroundColor(mycolor);

		Phrase day2 = new Phrase("2", font8bold);
		PdfPCell day2Cell = new PdfPCell(day2);
		day2Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day2Cell.setBackgroundColor(mycolor);

		Phrase day3 = new Phrase("3", font8bold);
		PdfPCell day3Cell = new PdfPCell(day3);
		day3Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day3Cell.setBackgroundColor(mycolor);

		Phrase day4 = new Phrase("4", font8bold);
		PdfPCell day4Cell = new PdfPCell(day4);
		day4Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day4Cell.setBackgroundColor(mycolor);

		Phrase day5 = new Phrase("5", font8bold);
		PdfPCell day5Cell = new PdfPCell(day5);
		day5Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day5Cell.setBackgroundColor(mycolor);

		Phrase day6 = new Phrase("6", font8bold);
		PdfPCell day6Cell = new PdfPCell(day6);
		day6Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day6Cell.setBackgroundColor(mycolor);

		Phrase day7 = new Phrase("7", font8bold);
		PdfPCell day7Cell = new PdfPCell(day7);
		day7Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day7Cell.setBackgroundColor(mycolor);

		Phrase day8 = new Phrase("8", font8bold);
		PdfPCell day8Cell = new PdfPCell(day8);
		day8Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day8Cell.setBackgroundColor(mycolor);

		Phrase day9 = new Phrase("9", font8bold);
		PdfPCell day9Cell = new PdfPCell(day9);
		day9Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day9Cell.setBackgroundColor(mycolor);

		Phrase day10 = new Phrase("10", font8bold);
		PdfPCell day10Cell = new PdfPCell(day10);
		day10Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day10Cell.setBackgroundColor(mycolor);

		Phrase day11 = new Phrase("11", font8bold);
		PdfPCell day11Cell = new PdfPCell(day11);
		day11Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day11Cell.setBackgroundColor(mycolor);

		Phrase day12 = new Phrase("12", font8bold);
		PdfPCell day12Cell = new PdfPCell(day12);
		day12Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day12Cell.setBackgroundColor(mycolor);

		Phrase day13 = new Phrase("13", font8bold);
		PdfPCell day13Cell = new PdfPCell(day13);
		day13Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day13Cell.setBackgroundColor(mycolor);

		Phrase day14 = new Phrase("14", font8bold);
		PdfPCell day14Cell = new PdfPCell(day14);
		day14Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day14Cell.setBackgroundColor(mycolor);

		Phrase day15 = new Phrase("15", font8bold);
		PdfPCell day15Cell = new PdfPCell(day15);
		day15Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day15Cell.setBackgroundColor(mycolor);

		Phrase day16 = new Phrase("16", font8bold);
		PdfPCell day16Cell = new PdfPCell(day16);
		day16Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day16Cell.setBackgroundColor(mycolor);

		table.addCell(productNameCell);
		table.addCell(day1Cell);
		table.addCell(day2Cell);
		table.addCell(day3Cell);
		table.addCell(day4Cell);
		table.addCell(day5Cell);
		table.addCell(day6Cell);
		table.addCell(day7Cell);
		table.addCell(day8Cell);
		table.addCell(day9Cell);
		table.addCell(day10Cell);
		table.addCell(day11Cell);
		table.addCell(day12Cell);
		table.addCell(day13Cell);
		table.addCell(day14Cell);
		table.addCell(day15Cell);
		table.addCell(day16Cell);

		// loading data to the table //

		// chages

		Phrase blank = null;
		PdfPCell blankcell = null;
		// System.out.println("contract list product size" +
		// con.getItems().size());

		Calendar startDate1 = Calendar.getInstance();
		startDate1.setTime(fromDate);
		// System.out.println("Fro date rohan"+fromDate);
		int year = startDate1.get(Calendar.YEAR);
		int month = startDate1.get(Calendar.MONTH);

		// System.out.println("year "+year);
		// System.out.println("month "+month);

		PdfPCell yesNoCell = null;

		for (int i = 0; i < con.getItems().size(); i++) {

			Phrase pName = new Phrase(con.getItems().get(i).getProductName(),font8bold);
			PdfPCell pNameCell = new PdfPCell(pName);
			table.addCell(pNameCell);

			for (int j = 1; j < 17; j++) {

				startDate1.set(year, month, j);

				Date date = startDate1.getTime();

				// System.out.println("start x date "+date);
//				String status=getProductStatus(sdf.format(date), con.getItems().get(i).getProductName());
//				if (status.equals("Complete")) {
//					Phrase yes = new Phrase("Yes", font8);
//					yesNoCell = new PdfPCell(yes);
//					yesNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//					table.addCell(yesNoCell);
//				} else if(status.equals("Schedule")) {
//					Phrase no = new Phrase("No", font8);
//					yesNoCell = new PdfPCell(no);
//					yesNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//					table.addCell(yesNoCell);
//				}
//				else{
					Phrase blankCell = new Phrase(" ", font8);
					yesNoCell = new PdfPCell(blankCell);
					yesNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					table.addCell(yesNoCell);
//				}
			}
		}

		int blankLine = 6 - con.getItems().size();
		// System.out.println("rohan 123" + blankLine);
		if (blankLine > 0) {
			for (int j = 0; j < blankLine; j++) {
				for (int i = 0; i < 17; i++) {
					blank = new Phrase(" ", font8);
					blankcell = new PdfPCell(blank);
					table.addCell(blankcell);
				}
			}
		}

		// code for trap cathches heading
		PdfPCell trapcatchesCell = null;
//		if (list.size() != 0) {
			Phrase trapcatches = new Phrase("Trap Catches", font8);
			trapcatchesCell = new PdfPCell(trapcatches);
			table.addCell(trapcatchesCell);

			for (int i = 0; i < 16; i++) {
				blank = new Phrase(" ", font8);
				blankcell = new PdfPCell(blank);
				table.addCell(blankcell);
			}

			for (int j = 0; j < list.size(); j++) {

				Phrase name = new Phrase(list.get(j), font8);
				PdfPCell nameCell = new PdfPCell(name);
				table.addCell(nameCell);

				for (int i = 1; i < 17; i++) {

//					startDate1.set(year, month, i);
//
//					Date date = startDate1.getTime();
//
//					// System.out.println("start date "+date);
//
//					int qty = getTrapQuantityByDate(sdf.format(date),list.get(j));
					// System.out.println("qty value "+qty);

					PdfPCell qtyCell = null;
//					if (qty != 0) {
//						Phrase qtyPhrase = new Phrase(qty + " ", font8);
//						qtyCell = new PdfPCell(qtyPhrase);
//						qtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//						table.addCell(qtyCell);
//					} else {
						Phrase qtyPhrase = new Phrase(" ", font8);
						qtyCell = new PdfPCell(qtyPhrase);
						qtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						table.addCell(qtyCell);
//					}
				}
			}
//		}

		Phrase nbhcRepesntative = new Phrase("NBHC Representative ", font8);
		PdfPCell nbhcRepesntativeCell = new PdfPCell(nbhcRepesntative);
		table.addCell(nbhcRepesntativeCell);

		for (int i = 0; i < 16; i++) {
			blank = new Phrase(" ", font8);
			blankcell = new PdfPCell(blank);
			table.addCell(blankcell);
		}

		Phrase custRepre = new Phrase("Customer Representative", font8);
		PdfPCell custRepreCell = new PdfPCell(custRepre);
		table.addCell(custRepreCell);

		for (int i = 0; i < 16; i++) {
			blank = new Phrase(" ", font8);
			blankcell = new PdfPCell(blank);
			table.addCell(blankcell);
		}

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// for remaining 15 days

		// System.out.println("Servicelist Size" + serviceLis.size());
		PdfPTable table1 = new PdfPTable(17);
		table1.setWidthPercentage(100f);
		table1.setSpacingBefore(10f);

		try {
			table1.setWidths(relativeWidths1);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase productName1 = new Phrase("Service Date", font8bold);
		PdfPCell productName1Cell = new PdfPCell(productName1);
		productName1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		productName1Cell.setBackgroundColor(mycolor);

		Phrase day17 = new Phrase("17", font8bold);
		PdfPCell day17Cell = new PdfPCell(day17);
		day17Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day17Cell.setBackgroundColor(mycolor);

		Phrase day18 = new Phrase("18", font8bold);
		PdfPCell day18Cell = new PdfPCell(day18);
		day18Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day18Cell.setBackgroundColor(mycolor);

		Phrase day19 = new Phrase("19", font8bold);
		PdfPCell day19Cell = new PdfPCell(day19);
		day19Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day19Cell.setBackgroundColor(mycolor);

		Phrase day20 = new Phrase("20", font8bold);
		PdfPCell day20Cell = new PdfPCell(day20);
		day20Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day20Cell.setBackgroundColor(mycolor);

		Phrase day21 = new Phrase("21", font8bold);
		PdfPCell day21Cell = new PdfPCell(day21);
		day21Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day21Cell.setBackgroundColor(mycolor);

		Phrase day22 = new Phrase("22", font8bold);
		PdfPCell day22Cell = new PdfPCell(day22);
		day22Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day22Cell.setBackgroundColor(mycolor);

		Phrase day23 = new Phrase("23", font8bold);
		PdfPCell day23Cell = new PdfPCell(day23);
		day23Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day23Cell.setBackgroundColor(mycolor);

		Phrase day24 = new Phrase("24", font8bold);
		PdfPCell day24Cell = new PdfPCell(day24);
		day24Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day24Cell.setBackgroundColor(mycolor);

		Phrase day25 = new Phrase("25", font8bold);
		PdfPCell day25Cell = new PdfPCell(day25);
		day25Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day25Cell.setBackgroundColor(mycolor);

		Phrase day26 = new Phrase("26", font8bold);
		PdfPCell day26Cell = new PdfPCell(day26);
		day26Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day26Cell.setBackgroundColor(mycolor);

		Phrase day27 = new Phrase("27", font8bold);
		PdfPCell day27Cell = new PdfPCell(day27);
		day27Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day27Cell.setBackgroundColor(mycolor);

		Phrase day28 = new Phrase("28", font8bold);
		PdfPCell day28Cell = new PdfPCell(day28);
		day28Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day28Cell.setBackgroundColor(mycolor);

		Phrase day29 = new Phrase("29", font8bold);
		PdfPCell day29Cell = new PdfPCell(day29);
		day29Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day29Cell.setBackgroundColor(mycolor);

		Phrase day30 = new Phrase("30", font8bold);
		PdfPCell day30Cell = new PdfPCell(day30);
		day30Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day30Cell.setBackgroundColor(mycolor);

		Phrase day31 = new Phrase("31", font8bold);
		PdfPCell day31Cell = new PdfPCell(day31);
		day31Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day31Cell.setBackgroundColor(mycolor);

		Phrase day32 = new Phrase(" ", font8bold);
		PdfPCell day32Cell = new PdfPCell(day32);
		day32Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		day32Cell.setBackgroundColor(mycolor);

		table1.addCell(productName1Cell);
		table1.addCell(day17Cell);
		table1.addCell(day18Cell);
		table1.addCell(day19Cell);
		table1.addCell(day20Cell);
		table1.addCell(day21Cell);
		table1.addCell(day22Cell);
		table1.addCell(day23Cell);
		table1.addCell(day24Cell);
		table1.addCell(day25Cell);
		table1.addCell(day26Cell);
		table1.addCell(day27Cell);
		table1.addCell(day28Cell);
		table1.addCell(day29Cell);
		table1.addCell(day30Cell);
		table1.addCell(day31Cell);
		table1.addCell(day32Cell);

		// Phrase blank1 =null;
		// PdfPCell blank1cell =null;

		Calendar startDate2 = Calendar.getInstance();
		startDate2.setTime(fromDate);

		int year1 = startDate1.get(Calendar.YEAR);
		int month1 = startDate2.get(Calendar.MONTH);

		for (int i = 0; i < con.getItems().size(); i++) {

			Phrase pName = new Phrase(con.getItems().get(i).getProductName(),font8bold);
			PdfPCell pNameCell = new PdfPCell(pName);
			table1.addCell(pNameCell);

			for (int j = 17; j < 33; j++) {

//				startDate1.set(year, month, j);
//
//				Date date = startDate1.getTime();
///**************************rohan made changes  here ******************************************/
//				// System.out.println("start date "+date);
//				if (j < 32) {
//				String servicStatus =getProductStatus(sdf.format(date), con.getItems().get(i).getProductName());
//					if (servicStatus.equals("Complete")) {
//						Phrase yes = new Phrase("Yes", font8);
//						yesNoCell = new PdfPCell(yes);
//						yesNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//						table1.addCell(yesNoCell);
//					} else if(servicStatus.equals("Schedule")) {
//						Phrase no = new Phrase("No", font8);
//						yesNoCell = new PdfPCell(no);
//						yesNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//						table1.addCell(yesNoCell);
//					}
//					else{
//						Phrase blankPhrase = new Phrase(" ", font8);
//						yesNoCell = new PdfPCell(blankPhrase);
//						yesNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//						table1.addCell(yesNoCell);
//					}
//				} else {
					Phrase no = new Phrase(" ", font8);
					yesNoCell = new PdfPCell(no);
					table1.addCell(yesNoCell);
//				}
			}
		}

		int blankLine1 = 6 - con.getItems().size();
		// System.out.println("rohan 123" + blankLine1);
		if (blankLine1 > 0) {
			for (int j = 0; j < blankLine1; j++) {
				for (int i = 0; i < 17; i++) {
					blank = new Phrase(" ", font8);
					blankcell = new PdfPCell(blank);
					table1.addCell(blankcell);
				}
			}
		}

		if (list.size() != 0) {
			table1.addCell(trapcatchesCell);

			for (int i = 0; i < 16; i++) {
				blank = new Phrase(" ", font8);
				blankcell = new PdfPCell(blank);
				table1.addCell(blankcell);
			}

			for (int j = 0; j < list.size(); j++) {

				Phrase name = new Phrase(list.get(j), font8);
				PdfPCell nameCell = new PdfPCell(name);
				table1.addCell(nameCell);

				for (int i = 17; i < 33; i++) {

//					startDate2.set(year1, month1, i);
//
//					Date date = startDate2.getTime();
//
//					// System.out.println("start date "+date);
//
//					int qty = getTrapQuantityByDate(sdf.format(date),list.get(j));
					// System.out.println("qty value "+qty);

					PdfPCell qtyCell = null;
//					if (qty != 0) {
//						Phrase qtyPhrase = new Phrase(qty + " ", font8);
//						qtyCell = new PdfPCell(qtyPhrase);
//						qtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//						table1.addCell(qtyCell);
//					} else {
						Phrase qtyPhrase = new Phrase(" ", font8);
						qtyCell = new PdfPCell(qtyPhrase);
						qtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						table1.addCell(qtyCell);
//					}
				}
			}
		}

		Phrase nbhcRepesntative1 = new Phrase("NBHC Representative ", font8);
		PdfPCell nbhcRepesntativeCell1 = new PdfPCell(nbhcRepesntative1);
		table1.addCell(nbhcRepesntativeCell1);

		for (int i = 0; i < 16; i++) {
			blank = new Phrase(" ", font8);
			blankcell = new PdfPCell(blank);
			table1.addCell(blankcell);
		}

		Phrase custRepre1 = new Phrase("Customer Representative", font8);
		PdfPCell custRepreCell1 = new PdfPCell(custRepre1);
		table1.addCell(custRepreCell1);

		for (int i = 0; i < 16; i++) {
			blank = new Phrase(" ", font8);
			blankcell = new PdfPCell(blank);
			table1.addCell(blankcell);
		}

		try {
			document.add(table1);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * This method returns true when service status is completed i/p : date and
	 * product name o/p : boolean status true if service is completed else false
	 * 
	 */
//	private String getProductStatus(String date, String productName) {
//
//		String serviceStatus="";
//		
//		ArrayList<ProductStatus> psList = new ArrayList<ProductStatus>();
//		if (productMap.size() != 0) {
//			if (productMap.containsKey(date)) {
//				psList = productMap.get(date);
//
//				for (ProductStatus ps : psList) {
//					if (ps.getProductName().equals(productName)) {
//						if (ps.getProductStatus().equals(Service.SERVICESTATUSCOMPLETED)) {
//							serviceStatus ="Complete";
//						}
//						else{
//							serviceStatus="Schedule";
//						}
//
//					}
//				}
//			}
//
//		}
//		return serviceStatus;
//	}

//	private int getTrapQuantityByDate(String date, String TrapName) {
//
//		HashMap<String, Integer> innerhash = new HashMap<String, Integer>();
//		int qty = 0;
//		if (hashmap.size() != 0) {
//			innerhash = hashmap.get(date);
//
//			if (innerhash != null)
//			// if(innerhash.size()!=0)
//			{
//				qty = innerhash.get(TrapName);
//			}
//
//		}
//
//		return qty;
//	}

	/**
	 * This method returns the list of service on particular day which passed as
	 * parameter
	 * 
	 * @param serviceDate
	 * @return
	 */
	private List<Service> getOneDayServices(Date serviceDate) {
		List<Service> onedayService = new ArrayList<Service>();
		for (int i = 0; i < serviceLis.size(); i++) {
			if (sdf.format(serviceDate).equals(sdf.format(serviceLis.get(i).getServiceDate()))) {
				// System.out.println("MY rohan In side condition");
				onedayService.add(serviceLis.get(i));
			}
		}
		return onedayService;
	}

	/**
	 * This method returns the count of trap catches for particular day
	 * 
	 * @param trapName
	 * @param traplist
	 *            : contains tarplist for single day
	 * @return
	 */
	private int getTrappedQty(String trapName, List<CatchTraps> traplist) {
		int qty = 0;
		for (int i = 0; i < traplist.size(); i++) {
			if (trapName.equals(traplist.get(i).getPestName())) {
				// System.out.println(traplist.get(i).getPestName() + "xx-"+
				// traplist.get(i).getCount());
				qty = qty + traplist.get(i).getCount();
			}
		}
		return qty;
	}

	private void createObservationByNBHC() {

		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100f);
		try {
			table.setWidths(columnWidthsFor2Column);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		table.setSpacingBefore(10f);

		Phrase observationByNBHC = new Phrase("Observations", font8);//Observations By NBHC 
		PdfPCell observationByNBHCCell = new PdfPCell(observationByNBHC);
		observationByNBHCCell.setBorder(0);
		table.addCell(observationByNBHCCell);

		Phrase blank = new Phrase(" ", font8);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setBorderWidthTop(0);
		blankCell.setBorderWidthLeft(0);
		blankCell.setBorderWidthRight(0);
		table.addCell(blankCell);

		Phrase suggestionByCustomer = new Phrase("Suggestion By Customer ",
				font8);
		PdfPCell suggestionByCustomerCell = new PdfPCell(suggestionByCustomer);
		suggestionByCustomerCell.setBorder(0);
		table.addCell(suggestionByCustomerCell);
		table.addCell(blankCell);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createSuggestionByCustomer() {

		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100f);

		Phrase observationByNBHC = new Phrase("Suggestions By Customer");
		PdfPCell observationByNBHCCell = new PdfPCell(observationByNBHC);

		Phrase blank = new Phrase("", font8);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setBorderWidthTop(0);
		blankCell.setBorderWidthLeft(0);
		blankCell.setBorderWidthRight(0);
		table.addCell(blankCell);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}