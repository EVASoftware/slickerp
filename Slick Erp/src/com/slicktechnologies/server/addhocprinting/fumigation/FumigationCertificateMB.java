package com.slicktechnologies.server.addhocprinting.fumigation;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.fumigation.Fumigation;

public class FumigationCertificateMB {

	public Document document;
	Fumigation fumigation;
	Company comp;
	List<Branch> branchlist=new ArrayList<Branch>();
	private  Font font16boldul,font12bold,font8bold,
				font8,font12boldul,font12,font16bold,
				font9bold,font10,font10bold,font10boldul,
				font14bold,font14,font9;
	private SimpleDateFormat fmt= new SimpleDateFormat("dd/MM/yyyy");   		//Updated By: Viraj Date: 08-03-2019 Description: To Change date format
	DecimalFormat df=new DecimalFormat("0.00");
	/**
	 * Updated By: Viraj
	 * Date: 14-01-2019
	 * Description: To restrict data in 3 lines 
	 */
	int  noOfChar = 103;
	int noOfLines = 3;
	/** Ends **/
	
public FumigationCertificateMB() {
		
		font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
		font9bold = new Font(Font.FontFamily.HELVETICA,9,Font.BOLD);
		font8= new Font(Font.FontFamily.HELVETICA,8);
		font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12=new Font(Font.FontFamily.HELVETICA,12);
		font10=new Font(Font.FontFamily.HELVETICA,10);
		font10bold = new Font(Font.FontFamily.HELVETICA  , 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA  , 10, Font.BOLD|Font.UNDERLINE);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font14 = new Font(Font.FontFamily.HELVETICA  , 14, Font.NORMAL); 
		font9 = new Font(Font.FontFamily.HELVETICA  , 8, Font.NORMAL); 					//updatedBy: Viraj Date:05-02-2019 Description: To reduce the font size
	}
	
	
	public void loadfugation(Long count) {
		
		fumigation=ofy().load().type(Fumigation.class).id(count).now();
		
		if(fumigation.getCompanyId()!=null)
			 comp=ofy().load().type(Company.class).filter("companyId",fumigation.getCompanyId()).first().now();
		else
			comp=ofy().load().type(Company.class).first().now();
		
		 branchlist = ofy().load().type(Branch.class).filter("companyId",fumigation.getCompanyId()).filter("buisnessUnitName", fumigation.getBranch()).list();
	}

	public void createPdf() {
		
		createheader();
		createcertificateinfo();
		createDetailofGoods();
		createTreatmentDetails();
		createtimmber();
		Createadddeclaration();
	}


	


	


	


	


	


	private void createheader() {
		
		PdfPTable companyDetails=new PdfPTable(1);
		companyDetails.setWidthPercentage(100);
		
		
		Phrase compName =new Phrase(comp.getBusinessUnitName(),font16bold);
		PdfPCell compNameCell=new PdfPCell(compName);
		compNameCell.setBorder(0);
		compNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		companyDetails.addCell(compNameCell);
		/**
		 * Updated By: Viraj
		 * Date: 27-02-2019
		 * Description: Hard coded statement
		 */
		Phrase approvedBy =new Phrase("(Approved by National Plant Protection Organization, Government of India)",font10bold);
		PdfPCell approvedByCell=new PdfPCell(approvedBy);
		approvedByCell.setBorder(0);
		approvedByCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		companyDetails.addCell(approvedByCell);
		
		Phrase approvedBy1 =new Phrase("AN ISO 9001:2009 CERTIFIED COMPANY",font10bold);
		PdfPCell approvedBy1Cell=new PdfPCell(approvedBy1);
		approvedBy1Cell.setBorder(0);
		approvedBy1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		companyDetails.addCell(approvedBy1Cell);
		/** Ends **/
		
		/**
		 * Updated By: Viraj
		 * Date: 14-01-2019
		 * Description: To add company description
		 */
		if(comp.getDescription() != null) {
			Phrase compDesc =new Phrase(comp.getDescription(),font10bold);
			PdfPCell compDescCell=new PdfPCell(compDesc);
			compDescCell.setBorder(0);
			compDescCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			companyDetails.addCell(compDescCell);
		} else {
			Phrase compDesc =new Phrase("",font10bold);
			PdfPCell compDescCell=new PdfPCell(compDesc);
			compDescCell.setBorder(0);
			compDescCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			companyDetails.addCell(compDescCell);
		}
		/** Ends **/
		String branchaddress=null;
		System.out.println("branchlist"+branchlist.size());
		for (int i = 0; i <branchlist.size(); i++) {
			branchaddress=branchlist.get(i).getAddress().getCompleteAddress();
		}
		System.out.println("branchaddress"+branchaddress);
		Phrase branchadd=new Phrase ("Branch Office :"+branchaddress,font10);
		PdfPCell branchaddCell=new PdfPCell(branchadd);
		branchaddCell.setBorder(0);
		branchaddCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		companyDetails.addCell(branchaddCell);
		
//		Phrase isoph=new Phrase ("AN ISO 9001 :",font10);
//		PdfPCell isoCell=new PdfPCell(isoph);
//		isoCell.setBorder(0);
//		isoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		companyDetails.addCell(isoCell);
		
		try {
			document.add(companyDetails);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		PdfPTable titletab=new PdfPTable(1);
		titletab.setWidthPercentage(100);
		
		Phrase titleph=new Phrase("FUMIGATION CERTIFICATE",font12);
		PdfPCell titleCell=new PdfPCell(titleph);
		titleCell.setBorder(0);
		titleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		titletab.addCell(titleCell);
		
		try {
			document.add(titletab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	private void createcertificateinfo() {
		
		PdfPTable certificatetab=new PdfPTable(5);
		certificatetab.setWidthPercentage(100);
		certificatetab.setSpacingBefore(10);
		try {
			certificatetab.setWidths(new float[]{20,25,10,20,25});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase srph=new Phrase("S.No.:",font8bold);
		PdfPCell srCell=new PdfPCell(srph);
		srCell.setBorder(0);
		srCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(srCell);
		
		Phrase srvalph=new Phrase(fumigation.getSerialNumber()+"",font8bold);
		PdfPCell srvalCell=new PdfPCell(srvalph);
		srvalCell.setBorder(0);
		srvalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(srvalCell);
		
		System.out.println("fumigation.getServiceID()"+fumigation.getSerialNumber());
		
		Phrase blankph=new Phrase(" ",font8);
		PdfPCell blankCell=new PdfPCell(blankph);
		blankCell.setBorder(0);
		blankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(blankCell);
		
		Phrase certnoph=new Phrase("Certificate Number : ",font8bold);
		PdfPCell certnoCell=new PdfPCell(certnoph);
		certnoCell.setBorder(0);
		certnoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(certnoCell);
		
		
		Phrase certnoValph=new Phrase(fumigation.getCertificateNo(),font8bold);
		PdfPCell certnoValCell=new PdfPCell(certnoValph);
		certnoValCell.setBorder(0);
		certnoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(certnoValCell);
		
		Phrase regdph=new Phrase("Dte.PPQS Regd.No :",font8bold);
		PdfPCell regdCell=new PdfPCell(regdph);
		regdCell.setBorder(0);
		regdCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(regdCell);
		
		Phrase regdValph=new Phrase(fumigation.getDtePPQSRegistrationNumber(),font8bold);
		PdfPCell regdValCell=new PdfPCell(regdValph);
		regdValCell.setBorder(0);
		regdValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(regdValCell);
		
		certificatetab.addCell(blankCell);
		
		Phrase dateofissueph=new Phrase("Date Of issue : ",font8bold);
		PdfPCell dateofissueValCell=new PdfPCell(dateofissueph);
		dateofissueValCell.setBorder(0);
		dateofissueValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(dateofissueValCell);
		
		Phrase dateofissuevalph=new Phrase(fmt.format(fumigation.getDateofissue()),font8bold);
		PdfPCell dateofissuevalCell=new PdfPCell(dateofissuevalph);
		dateofissuevalCell.setBorder(0);
		dateofissuevalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(dateofissuevalCell);
		
		
		Phrase cercontentph=new Phrase("This is to certify that the following regulated articles have been fumigated according to the appropriate procedures to conform to the current phytosanitory regulations of the importing country :",font10);
		PdfPCell cercontentCell=new PdfPCell(cercontentph);
		cercontentCell.setPaddingTop(5);
		cercontentCell.setBorder(0);
		cercontentCell.setColspan(5);
		cercontentCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(cercontentCell);
		
		try {
			document.add(certificatetab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void createDetailofGoods() {
		
		
		PdfPTable detailsofgoodtab=new PdfPTable(3);
		detailsofgoodtab.setWidthPercentage(100);
		try {
			detailsofgoodtab.setWidths(new float[]{23,1,76});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			Phrase titleph=new Phrase("Details Of goods ",font10bold);
			PdfPCell titleCell=new PdfPCell(titleph);
			titleCell.setBorder(0);
			titleCell.setColspan(3);
			titleCell.setPadding(0);
			titleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			detailsofgoodtab.addCell(titleCell);
		
			 Phrase descgoods=new Phrase("Description Of Goods ",font9);
			 PdfPCell descgoodscell=new PdfPCell();
			 descgoodscell.addElement(descgoods);
			 descgoodscell.setBorder(0);
			 descgoodscell.setPadding(0);
			 descgoodscell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 detailsofgoodtab.addCell(descgoodscell);
			 
			 Phrase colonph=new Phrase(":",font9);
			 PdfPCell coloncell=new PdfPCell();
			 coloncell.addElement(colonph);
			 coloncell.setBorder(0);
			 coloncell.setPadding(0);
			 coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 detailsofgoodtab.addCell(coloncell);
			 
			 Phrase descValue=null;
			 if(fumigation.getDescriptionofgoods()!=null){
				 descValue=new Phrase(fumigation.getDescriptionofgoods(),font9);
			 }
			 else
			 {
			 descValue=new Phrase("",font9);
			 }
		 
			 PdfPCell descValuecell=new PdfPCell();
			 descValuecell.addElement(descValue);
			 descValuecell.setBorder(0);
			 descValuecell.setPadding(0);
			 descValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 detailsofgoodtab.addCell(descValuecell);
			 
			 Phrase qtydeclar=new Phrase("Quantity Declared ",font9);
			 PdfPCell qtydeclarecell=new PdfPCell();
			 qtydeclarecell.addElement(qtydeclar);
			 qtydeclarecell.setBorder(0);
			 qtydeclarecell.setPadding(0);
			 qtydeclarecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 detailsofgoodtab.addCell(qtydeclarecell);
			 
			 detailsofgoodtab.addCell(coloncell);
			 
			 Phrase qtydeclartValue=null;
			 if((fumigation.getQuantitydeclare()!=null)){
				
				 qtydeclartValue=new Phrase(fumigation.getQuantitydeclare(),font9);
			 }
			 else
			 {
			 qtydeclartValue=new Phrase("",font9);
			 }
			 PdfPCell qtyValuecell=new PdfPCell();
			 qtyValuecell.addElement(qtydeclartValue);
			 qtyValuecell.setBorder(0);
			 qtyValuecell.setPadding(0);
			 qtyValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 detailsofgoodtab.addCell(qtyValuecell);
			 
			 Phrase distMark=new Phrase("Distinguishing Marks ",font9);
			 PdfPCell distmarkcell=new PdfPCell();
			 distmarkcell.addElement(distMark);
			 distmarkcell.setBorder(0);
			 distmarkcell.setPadding(0);
			 distmarkcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 detailsofgoodtab.addCell(distmarkcell);
			 
			 detailsofgoodtab.addCell(coloncell);
			 
			 Phrase distmarkValue=null;
			 if(fumigation.getDistiinguishingmarks()!=null){
				 distmarkValue=new Phrase(fumigation.getDistiinguishingmarks(),font9);
			 }
			 else
			 {
			 distmarkValue=new Phrase("",font9);
			 }
		
			 PdfPCell distmarkValuecell=new PdfPCell();
			 distmarkValuecell.addElement(distmarkValue);
			 distmarkValuecell.setBorder(0);
			 distmarkValuecell.setPadding(0);
			 distmarkValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 detailsofgoodtab.addCell(distmarkValuecell);
			 
			 Phrase conslisk=new Phrase("Consignment Link/Container",font9);
			 PdfPCell conslinkcell=new PdfPCell();
			 conslinkcell.addElement(conslisk);
			 conslinkcell.setBorder(0);
			 conslinkcell.setPadding(0);
			 conslinkcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 detailsofgoodtab.addCell(conslinkcell);
			 
			 detailsofgoodtab.addCell(coloncell);
			 
			 Phrase conslinkValue=null;
			 
				 if(fumigation.getConsignmentlink()!=null){
					 conslinkValue=new Phrase(fumigation.getConsignmentlink(),font9);
				 }
			 else
			 {
				 conslinkValue=new Phrase("",font9);
			 }
			
			 PdfPCell conslinkValuecell=new PdfPCell();
			 conslinkValuecell.addElement(conslinkValue);
			 conslinkValuecell.setBorder(0);
			 conslinkValuecell.setPadding(0);
			 conslinkValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 detailsofgoodtab.addCell(conslinkValuecell);
			 
			 
			 try {
				document.add(detailsofgoodtab);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			 
			PdfPTable consigntab=new PdfPTable(4);
			consigntab.setWidthPercentage(100);
			try {
				consigntab.setWidths(new float[]{19,31,19,31});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
			
			 Phrase pcofloading=new Phrase("Port & Country Of loading :",font9);
			 PdfPCell pocloadcell=new PdfPCell();
			 pocloadcell.addElement(pcofloading);
			 pocloadcell.setBorder(0);
			 pocloadcell.setPadding(0);
			 pocloadcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 consigntab.addCell(pocloadcell);
			 
			 Phrase pocloadValue=null;
			 
			 if(fumigation.getPortncountryloading()!=null){
				 pocloadValue=new Phrase(fumigation.getPortncountryloading(),font9);
			 }
			 else
			 {
			 pocloadValue=new Phrase("",font9);
			 }
		
			 PdfPCell pocloadValuecell=new PdfPCell();
			 pocloadValuecell.addElement(pocloadValue);
			 pocloadValuecell.setBorder(0);
			 pocloadValuecell.setPadding(0);
			 pocloadValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 consigntab.addCell(pocloadValuecell);
			 
//			 Phrase blank3ph=new Phrase(" ",font9);
//			 PdfPCell blank3cell=new PdfPCell();
//			 blank3cell.addElement(blank3ph);
//			 blank3cell.setBorder(0);
//			 blank3cell.setPadding(0);
//			 blank3cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			 consigntab.addCell(blank3cell);
			 
			 Phrase contrydest=new Phrase("Country Of Destination :",font9);
			 PdfPCell codestcell=new PdfPCell();
			 codestcell.addElement(contrydest);
			 codestcell.setBorder(0);
			 codestcell.setPadding(0);
			 codestcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 consigntab.addCell(codestcell);
			 
			 Phrase conrydestValue=null;
			 
			 if(fumigation.getCountryofdestination()!=null){
				 conrydestValue=new Phrase(fumigation.getCountryofdestination(),font9);
			 }
			 else
			 {
			 conrydestValue=new Phrase("",font9);
			 }
		
			 PdfPCell contrydestValuecell=new PdfPCell();
			 contrydestValuecell.addElement(conrydestValue);
			 contrydestValuecell.setBorder(0);
			 contrydestValuecell.setPadding(0);
			 contrydestValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 consigntab.addCell(contrydestValuecell);
			 
			 Phrase nameofvessel=new Phrase("Name of Vessel/Ship :",font9);
			 PdfPCell namevesscell=new PdfPCell();
			 namevesscell.addElement(nameofvessel);
			 namevesscell.setBorder(0);
			 namevesscell.setPadding(0);
			 namevesscell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 consigntab.addCell(namevesscell);
			
			
			 
			 Phrase namevessValue=null;
			 
			 if(fumigation.getNameofvessel()!=null){
				 namevessValue=new Phrase(fumigation.getNameofvessel(),font9);
			 }
			 else
			 {
			 namevessValue=new Phrase("",font9);
			 }
		
			 PdfPCell namevessValuecell=new PdfPCell();
			 namevessValuecell.addElement(namevessValue);
			 namevessValuecell.setBorder(0);
			 namevessValuecell.setPadding(0);
			 namevessValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 consigntab.addCell(namevessValuecell);
			 
//			 consigntab.addCell(blank3cell);
			 
			 Phrase dpentry=new Phrase("Port Of Entry :",font9);
			 PdfPCell dpentrycell=new PdfPCell();
			 dpentrycell.addElement(dpentry);
			 dpentrycell.setBorder(0);
			 dpentrycell.setPadding(0);
			 dpentrycell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 consigntab.addCell(dpentrycell);
			 
		
			 Phrase dpentryval=new Phrase(fumigation.getDeclarepointentry(),font9);
			 PdfPCell dpentryvalcell=new PdfPCell();
			 dpentryvalcell.addElement(dpentryval);
			 dpentryvalcell.setBorder(0);
			 dpentryvalcell.setPadding(0);
			 dpentryvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 consigntab.addCell(dpentryvalcell);
			 
			 try {
				document.add(consigntab);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			 
			 
			 PdfPTable consininfotab=new PdfPTable(3);
			 consininfotab.setWidthPercentage(100);
				try {
					consininfotab.setWidths(new float[]{30,1,69});
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				Phrase fromaddress=new Phrase("Name and address of consignor / exporter ",font9);
				PdfPCell fromaddcell=new PdfPCell();
				fromaddcell.addElement(fromaddress);
				fromaddcell.setBorder(0);
				fromaddcell.setPadding(0);
				consininfotab.addCell(fromaddcell);
				  
				Phrase colon=new Phrase(" : ",font9);
				PdfPCell colon3cell=new PdfPCell();
				colon3cell.addElement(colon);
				colon3cell.setBorder(0) ;
				colon3cell.setPadding(0);
				consininfotab.addCell(colon3cell);
				
				
				Phrase fromaddressvalph=new Phrase(fumigation.getFromCompanyname()+"\n"+fumigation.getFromAddess(),font9);
				PdfPCell fromaddvalcell=new PdfPCell();
				fromaddvalcell.addElement(fromaddressvalph);
				fromaddvalcell.setBorder(0);
				fromaddvalcell.setPadding(0);
				consininfotab.addCell(fromaddvalcell);
				
				Phrase toaddress=new Phrase("Declared Name and address Of consignee ",font9);
				PdfPCell toaddcell=new PdfPCell();
				toaddcell.addElement(toaddress);
				toaddcell.setBorder(0);
				toaddcell.setPadding(0);
				consininfotab.addCell(toaddcell);
			  
				consininfotab.addCell(colon3cell);
				
				Phrase toaddressvalph=new Phrase(fumigation.getToCompanyname()+"\n"+fumigation.getToAddress(),font9);
				PdfPCell toaddvalcell=new PdfPCell();
				toaddvalcell.addElement(toaddressvalph);
				toaddvalcell.setBorder(0);
				toaddvalcell.setPadding(0);
				consininfotab.addCell(toaddvalcell);
				
				try {
					document.add(consininfotab);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	}
	
	private void createTreatmentDetails() {
		PdfPTable treattable=new PdfPTable(6);
		 
		treattable.setWidthPercentage(100f);
		 
		 try {
			// updated by viraj to remove extra spacing on 04-03-2019
			 treattable.setWidths(new float[]{1.4f,0.1f,2.4f,1.4f,0.1f,2.4f});
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		 	String	titlepdf="Details Of treatment";
			Phrase titlephrase=new Phrase(titlepdf,font10bold);
			Paragraph titlepdfpara=new Paragraph();
			titlepdfpara.add(titlephrase); 
			PdfPCell titlecell=new PdfPCell(titlepdfpara);
			titlecell.setBorder(0);
			titlecell.setColspan(6);
			titlecell.setPadding(0);
			titlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			treattable.addCell(titlecell);
			
			
			 Phrase namefumihation=new Phrase("Name of Fumigant",font9);
			 PdfPCell namefumicell=new PdfPCell();
			 namefumicell.addElement(namefumihation);
			 namefumicell.setBorder(0);
			 namefumicell.setPadding(0);
			 namefumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(namefumicell);
			 
			 Phrase seperator=new Phrase(":",font9);
			 PdfPCell seperatorcell=new PdfPCell();
			 seperatorcell.addElement(seperator);
			 seperatorcell.setBorder(0);
			 seperatorcell.setPadding(0);
			 seperatorcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			 treattable.addCell(seperatorcell);
			 
			 Phrase namefumiValue=null;
			 if(fumigation.getNameoffumigation()!=null){
				 namefumiValue=new Phrase(fumigation.getNameoffumigation(),font9);
			 }
			 else
			 {
			 namefumiValue=new Phrase("",font9);
			 }
		 
			 PdfPCell namefumiValuecell=new PdfPCell();
			 namefumiValuecell.addElement(namefumiValue);
			 namefumiValuecell.setBorder(0);
			 namefumiValuecell.setPadding(0);
			 namefumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(namefumiValuecell);
			 
			 Phrase dateoffumi=new Phrase("Date of Fumigation",font9);
			 PdfPCell datefumicell=new PdfPCell();
			 datefumicell.addElement(dateoffumi);
			 datefumicell.setBorder(0);
			 datefumicell.setPadding(0);
			 datefumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(datefumicell);
			 
			 treattable.addCell(seperatorcell);
			 
			 Phrase dateoffumiValue=null;
			 if(fumigation.getDateoffumigation()!=null){
				 dateoffumiValue=new Phrase(fmt.format(fumigation.getDateoffumigation()),font9);
			 }
			 else
			 {
			 dateoffumiValue=new Phrase("",font9);
			 }
		 
			 PdfPCell datefumiValuecell=new PdfPCell();
			 datefumiValuecell.addElement(dateoffumiValue);
			 datefumiValuecell.setBorder(0);
			 datefumiValuecell.setPadding(0);
			 datefumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(datefumiValuecell);
			 
			 Phrase placeoffumi=new Phrase("Place of Fumigation",font9);
			 PdfPCell placefumicell=new PdfPCell();
			 placefumicell.addElement(placeoffumi);
			 placefumicell.setBorder(0);
			 placefumicell.setPadding(0);
			 placefumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(placefumicell);
			 
			 treattable.addCell(seperatorcell);
			 
			 Phrase placefumiValue=null;
			 if(fumigation.getPlaceoffumigation()!=null){
				 placefumiValue=new Phrase(fumigation.getPlaceoffumigation(),font9);
			 }
			 else
			 {
			 placefumiValue=new Phrase("",font9);
			 }
		 
			 PdfPCell placefumiValuecell=new PdfPCell();
			 placefumiValuecell.addElement(placefumiValue);
			 placefumiValuecell.setBorder(0);
			 placefumiValuecell.setPadding(0);
			 placefumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(placefumiValuecell);
			 
			 Phrase durationfumi=new Phrase("Duration of Fumigation",font9);
			 PdfPCell durarationfumicell=new PdfPCell();
			 durarationfumicell.addElement(durationfumi);
			 durarationfumicell.setBorder(0);
			 durarationfumicell.setPadding(0);
			 durarationfumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(durarationfumicell);
			 
			 treattable.addCell(seperatorcell);
			 
			 Phrase durafumiValue=null;
			 if(fumigation.getDuration()!=null){
				 durafumiValue=new Phrase(fumigation.getDuration()+" hrs",font9);
			 }
			 else
			 {
			 durafumiValue=new Phrase("",font9);
			 }
		 
			 PdfPCell durafumiValuecell=new PdfPCell();
			 durafumiValuecell.addElement(durafumiValue);
			 durafumiValuecell.setBorder(0);
			 durafumiValuecell.setPadding(0);
			 durafumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(durafumiValuecell);
			 
			 Phrase dosefumigation=new Phrase("Dosage Of fumigation",font9);
			 PdfPCell dosafumicell=new PdfPCell();
			 dosafumicell.addElement(dosefumigation);
			 dosafumicell.setBorder(0);
			 dosafumicell.setPadding(0);
			 dosafumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(dosafumicell);
			 
			 treattable.addCell(seperatorcell);
			 
			 Phrase dosefumiValue=null;
				 if(fumigation.getDoseratefumigation()!=null){
					 dosefumiValue=new Phrase(fumigation.getDoseratefumigation(),font9);   // updated by viraj to remove predefined unit on 04-03-2019
				 }
			 else
			 {
				 dosefumiValue=new Phrase("",font9);
			 }
			 
			 PdfPCell dosafumiValuecell=new PdfPCell();
			 dosafumiValuecell.addElement(dosefumiValue);
			 dosafumiValuecell.setBorder(0);
			 dosafumiValuecell.setPadding(0);
			 dosafumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(dosafumiValuecell);
			 
			 
			 Phrase minaietemp=new Phrase("Min. Air Temp.",font9);
			 PdfPCell tempfumicell=new PdfPCell();
			 tempfumicell.addElement(minaietemp);
			 tempfumicell.setBorder(0);
			 tempfumicell.setPadding(0);
			 tempfumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(tempfumicell);
			 
			 treattable.addCell(seperatorcell);
			 
			 Phrase tempfumiValue=null;
				 if(fumigation.getMinairtemp()!=null){
					 tempfumiValue=new Phrase(fumigation.getMinairtemp(),font9);		// updated by viraj to remove predefined unit on 04-03-2019
				 }
			 else
			 {
				 tempfumiValue=new Phrase("",font9);
			 }
			 
			 PdfPCell tempfumiValuecell=new PdfPCell();
			 tempfumiValuecell.addElement(tempfumiValue);
			 tempfumiValuecell.setBorder(0);
			 tempfumiValuecell.setPadding(0);
			 tempfumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(tempfumiValuecell);
			 
			 
			 try {
				document.add(treattable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
			 
			PdfPTable detailquotab=new PdfPTable(2); 
			detailquotab.setWidthPercentage(100);
			
			try {
				detailquotab.setWidths(new float[]{90,10});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			 String fumigationnote="Fumigation performed in";		// updated by viraj to remove (under) on 04-03-2019
			 String fumigationnote1="Container pressure test conducted";
			 String fumigationnote2="Container has 200 mm free air space at top of container";
			 String fumigationnote3="In transit fumigation -Needs ventillation at port of discharge";
			 String fumigationnote4=" has been ventilated to below 5ppm v/v Methyl Bromide";
			 
			 String fumigationPerformed = "";
			 if(fumigation.getFumigationPerformed()!=null){
				 fumigationPerformed =  fumigation.getFumigationPerformed();
			 }
			 
			 Phrase notephrase=new Phrase(fumigationnote+" "+fumigationPerformed,font9);
			 PdfPCell notecell=new PdfPCell();
			 notecell.addElement(notephrase);
			 notecell.setBorder(0);
			 notecell.setPadding(0);
			 detailquotab.addCell(notecell);
			 
			 Phrase notephraseans=new Phrase(" ",font9);
			 PdfPCell noteanscell=new PdfPCell();
			 noteanscell.addElement(notephraseans);
			 noteanscell.setBorder(0);
			 noteanscell.setPadding(0);
			 detailquotab.addCell(noteanscell);
			 
			 
			 Phrase notephrase1=new Phrase(fumigationnote1,font9);
			 PdfPCell notecell1=new PdfPCell();
			 notecell1.addElement(notephrase1);
			 notecell1.setBorder(0);
			 notecell1.setPadding(0);
			 detailquotab.addCell(notecell1);
			 
			 Phrase notephraseans1=new Phrase(fumigation.getNote1(),font9);
			 PdfPCell noteanscell1=new PdfPCell();
			 noteanscell1.addElement(notephraseans1);
			 noteanscell1.setBorder(0);
			 noteanscell1.setPadding(0);
			 detailquotab.addCell(noteanscell1);
			 
			 Phrase notephrase2=new Phrase(fumigationnote2,font9);
			 PdfPCell notecell2=new PdfPCell();
			 notecell2.addElement(notephrase2);
			 notecell2.setBorder(0);
			 detailquotab.addCell(notecell2);
			 
			 
			 Phrase notephraseans2=new Phrase(fumigation.getNote2(),font9);
			 PdfPCell noteanscell2=new PdfPCell();
			 noteanscell2.addElement(notephraseans2);
			 noteanscell2.setBorder(0);
			 noteanscell2.setPadding(0);
			 detailquotab.addCell(noteanscell2);
			 
			 Phrase notephrase3=new Phrase(fumigationnote3,font9);
			 PdfPCell notecell3=new PdfPCell();
			 notecell3.addElement(notephrase3);
			 notecell3.setBorder(0);
			 notecell3.setPadding(0);
			 detailquotab.addCell(notecell3);
			 
			 Phrase notephraseans3=new Phrase(fumigation.getNote3(),font9);
			 PdfPCell noteanscell3=new PdfPCell();
			 noteanscell3.addElement(notephraseans3);
			 noteanscell3.setBorder(0);
			 noteanscell3.setPadding(0);
			 detailquotab.addCell(noteanscell3);
			 
			 Phrase notephrase4=new Phrase(fumigation.getVentilated()+fumigationnote4,font9);
			 PdfPCell notecell4=new PdfPCell();
			 notecell4.addElement(notephrase4);
			 notecell4.setBorder(0);
			 notecell4.setPadding(0);
			 detailquotab.addCell(notecell4);
			 
			 Phrase notephraseans4=new Phrase(fumigation.getNote4(),font9);
			 PdfPCell noteanscell4=new PdfPCell();
			 noteanscell4.addElement(notephraseans4);
			 noteanscell4.setBorder(0);
			 noteanscell4.setPadding(0);
			 detailquotab.addCell(noteanscell4);
			 
			 try {
				document.add(detailquotab);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	private void createtimmber() {
		
		PdfPTable timbertab=new PdfPTable(2); 
		timbertab.setWidthPercentage(100);
		
		try {
			timbertab.setWidths(new float[]{90,10});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String	titlepdf="Wrapping And Timber";
		Phrase titlephrase=new Phrase(titlepdf,font10bold);
		Paragraph titlepdfpara=new Paragraph();
		titlepdfpara.add(titlephrase); 
		PdfPCell titlecell=new PdfPCell(titlepdfpara);
		titlecell.setBorder(0);
		titlecell.setColspan(2);
		titlecell.setPadding(0);
		titlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		timbertab.addCell(titlecell);
		
		String note1="Has the commodity been fumigated prior to lacquering,varnishing,painting or wrapping ?";
		String note2="Has plastic Wrapping been used in the consignment ?";
		String note3="*If yes, has the consignment been fumigated prior to wrapping ?";
		String note4="*Or has the plastic wrapping been slashed,opened or perforated in accordance with the wrapping and perforation standards ?";
		String note5="Is the timber in this consignment less than 200 mm thick in one dimention and correctly spaced every 200 mm in height ?";
		
		 Phrase note1ph=new Phrase(note1,font9);
		 PdfPCell note1cell=new PdfPCell();
		 note1cell.addElement(note1ph);
		 note1cell.setBorder(0);
		 note1cell.setPadding(0);
		 timbertab.addCell(note1cell);
		 
		 
		 Phrase note1Valph=new Phrase(fumigation.getNote11(),font9);
		 PdfPCell note1valcell=new PdfPCell();
		 note1valcell.addElement(note1Valph);
		 note1valcell.setBorder(0);
		 note1valcell.setPadding(0);
		 timbertab.addCell(note1valcell);
		 
		 Phrase note2ph=new Phrase(note2,font9);
		 PdfPCell note2cell=new PdfPCell();
		 note2cell.addElement(note2ph);
		 note2cell.setBorder(0);
		 note2cell.setPadding(0);
		 timbertab.addCell(note2cell);
		 
		 Phrase note2Valph=new Phrase(fumigation.getNote12(),font9);
		 PdfPCell note2valcell=new PdfPCell();
		 note2valcell.addElement(note2Valph);
		 note2valcell.setBorder(0);
		 note2valcell.setPadding(0);
		 timbertab.addCell(note2valcell);
		 
		 Phrase note3ph=new Phrase(note3,font9);
		 PdfPCell note3cell=new PdfPCell();
		 note3cell.addElement(note3ph);
		 note3cell.setBorder(0);
		 note3cell.setPadding(0);
		 timbertab.addCell(note3cell);
		 
		 Phrase note3Valph=new Phrase(fumigation.getNote13(),font9);
		 PdfPCell note3valcell=new PdfPCell();
		 note3valcell.addElement(note3Valph);
		 note3valcell.setBorder(0);
		 note3valcell.setPadding(0);
		 timbertab.addCell(note3valcell);
		 
		 Phrase note4ph=new Phrase(note4,font9);
		 PdfPCell note4cell=new PdfPCell();
		 note4cell.addElement(note4ph);
		 note4cell.setBorder(0);
		 note4cell.setPadding(0);
		 timbertab.addCell(note4cell);
		 
		 Phrase note4Valph=new Phrase(fumigation.getNote13(),font9);
		 PdfPCell note4valcell=new PdfPCell();
		 note4valcell.addElement(note4Valph);
		 note4valcell.setBorder(0);
		 note4valcell.setPadding(0);
		 timbertab.addCell(note4valcell);
		 
		 Phrase note5ph=new Phrase(note5,font9);
		 PdfPCell note5cell=new PdfPCell();
		 note5cell.addElement(note5ph);
		 note5cell.setBorder(0);
		 note5cell.setPadding(0);
		 timbertab.addCell(note5cell);
		 
		 Phrase note5Valph=new Phrase(fumigation.getNote13(),font9);
		 PdfPCell note5valcell=new PdfPCell();
		 note5valcell.addElement(note5Valph);
		 note5valcell.setBorder(0);
		 note5valcell.setPadding(0);
		 timbertab.addCell(note5valcell);
		 
		 try {
			document.add(timbertab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void Createadddeclaration() {
		
		PdfPTable addTab=new PdfPTable(1);
		addTab.setWidthPercentage(100);
		
		/**
		 * Updated By: Viraj
		 * Date: 14-01-2019
		 * Description: To restrict data in 3 lines 
		 */
		int tChar = noOfChar * noOfLines;
		int adChar = fumigation.getAdditionaldeclaration().length();
		/** Ends **/
		
		String	titlepdf="Additional Declaration :";
		
		Phrase titlephrase=new Phrase(titlepdf,font10bold);
		Paragraph titlepdfpara=new Paragraph();
		titlepdfpara.add(titlephrase);
		PdfPCell titlecell=new PdfPCell(titlepdfpara);
		titlecell.setBorder(0);
		addTab.addCell(titlecell);
		
		System.out.println("fumigation.getAdditionaldeclaration()"+fumigation.getAdditionaldeclaration());
		/**
		 * Updated By: Viraj
		 * Date: 14-01-2019
		 * Description: to add blank cell if  data not present
		 */
		Phrase blank3ph=new Phrase(" ",font9);
		PdfPCell blank3cell=new PdfPCell();
		blank3cell.addElement(blank3ph);
		blank3cell.setBorder(0);
		
		if(fumigation.getAdditionaldeclaration() != null && fumigation.getAdditionaldeclaration() != "") {
			getCellRemainingLines(noOfLines, noOfChar,0,fumigation.getAdditionaldeclaration(), addTab);
			System.out.println("adddeclaration size"+fumigation.getAdditionaldeclaration().length());
		} else {
			addTab.addCell(blank3cell);
			addTab.addCell(blank3cell);
			addTab.addCell(blank3cell);
		}
		/** Ends **/
		
		
//		Phrase invoiceno=new Phrase("Inv No."+"",font9);
//		PdfPCell invCell=new PdfPCell(invoiceno);
//		invCell.setBorder(0);
//		invCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		addTab.addCell(invCell);
//		
//		Phrase invoicedate=new Phrase("Inv Date."+"",font9);
//		PdfPCell invCelldate=new PdfPCell(invoicedate);
//		invCelldate.setBorder(0);
//		invCelldate.setHorizontalAlignment(Element.ALIGN_LEFT);
//		addTab.addCell(invCelldate);
//		
//		String note="I DECLARE THAT THESE DETAILS ARE TRUE & CORRECT AND THE FUMIGATION HAS BEEN CARRIED OUT IN ACCORDANCE WITH THE ISPM-15.";
//		
//		
//		Phrase notephrase=new Phrase(note,font9);
//		PdfPCell notecell=new PdfPCell(notephrase);
//		notecell.setBorder(0);
//		notecell.setPaddingTop(5);
//		notecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		addTab.addCell(notecell);
		
		try {
			document.add(addTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		PdfPTable signtab=new PdfPTable(1);
		signtab.setWidthPercentage(100);
		signtab.setSpacingBefore(10);
		
		/**
		 * Updated By: Viraj
		 * Date: 27-02-2019
		 * Description: To add declaration statement
		 */
		Phrase declarationPh=new Phrase("I declare that these details are true & correct and the fumigation has been carried out in accordance with the NSPM-12",font9);
		PdfPCell declarationPhCell=new PdfPCell(declarationPh);
		declarationPhCell.setBorder(0);
		declarationPhCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		signtab.addCell(declarationPhCell);
		/** Ends **/
		
		Phrase namesignph=new Phrase("Signature & Name Of Accreditd Fumigation Operator ",font10bold);
		PdfPCell namesignCell=new PdfPCell(namesignph);
		namesignCell.setBorder(0);
		namesignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
		signtab.addCell(namesignCell);
		
		
		Phrase dppqAccregationph=new Phrase("DPPQ&S Accreditation No.",font9);
		PdfPCell dppqAccregationCell=new PdfPCell(dppqAccregationph);
		dppqAccregationCell.setBorder(0);
		dppqAccregationCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		signtab.addCell(dppqAccregationCell);
		
		Phrase AFAsnoph=new Phrase("AFAS Accreditation No.",font9);
		PdfPCell AFAsnoCell=new PdfPCell(AFAsnoph);
		AFAsnoCell.setBorder(0);
		AFAsnoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		signtab.addCell(AFAsnoCell);
		
		
		Phrase endoredph=new Phrase("Endorsed by Specified PPQ official (in case of non-accredited agency)",font9);
		PdfPCell endoredCell=new PdfPCell(endoredph);
		endoredCell.setBorder(0);
		endoredCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		signtab.addCell(endoredCell);
		
		
		
		Phrase signph=new Phrase("Signature:                        Name:                      Accreditation No.",font9);
		PdfPCell signCell=new PdfPCell(signph);
		signCell.setBorder(0);
		signCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		signtab.addCell(signCell);
		
		
		try {
			document.add(signtab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Update By: Viraj
	 * Date: 15-01-2019
	 * Description: To add cell and remaining blank cell according to no of lines and decided lines for that cell
	 * @param noOfLines
	 * @param noOfChar
	 * @param value
	 * @param table
	 */
	public void getCellRemainingLines(int noOfLines,int noOfChar,int colspan,String value,PdfPTable table) {
		int adChar = value.length();
		int vChar = noOfLines * noOfChar;
		int tChar = 3 * noOfChar ;
		int t1Char = 2 * noOfChar;
		int t2Char = 1 * noOfChar;
		
		Phrase blank3ph=new Phrase(" ",font9);
		PdfPCell blank3cell=new PdfPCell();
		blank3cell.addElement(blank3ph);
		blank3cell.setColspan(colspan);
		blank3cell.setBorder(0);
		// 1 line
		if((0 <= adChar) && (adChar <= t2Char)) {
			System.out.println("2 blank line" +adChar);
			Phrase adddeclareval=new Phrase(value.substring(0, adChar),font9);
			PdfPCell adddeclarevalCell=new PdfPCell(adddeclareval);
			adddeclarevalCell.setBorder(0);
			adddeclarevalCell.setColspan(colspan);
			adddeclarevalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(adddeclarevalCell);
			table.addCell(blank3cell);
			table.addCell(blank3cell);
		} else if((t2Char <= adChar) && (adChar <= t1Char)) {								//line 2
					System.out.println("1 blank line" +adChar);
					Phrase adddeclareval=new Phrase(value.substring(0, adChar),font9);
					PdfPCell adddeclarevalCell=new PdfPCell(adddeclareval);
					adddeclarevalCell.setBorder(0);
					adddeclarevalCell.setColspan(colspan);
					adddeclarevalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					table.addCell(adddeclarevalCell);
					table.addCell(blank3cell);
				} else if((t1Char <= adChar) && (adChar <= tChar)) {						//line 3
					System.out.println("0 blank line 3 lines exact" +adChar);
					Phrase adddeclareval=new Phrase(value.substring(0, adChar),font9);
					PdfPCell adddeclarevalCell=new PdfPCell(adddeclareval);
					adddeclarevalCell.setBorder(0);
					adddeclarevalCell.setColspan(colspan);
					adddeclarevalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					table.addCell(adddeclarevalCell);
				} else if(adChar > tChar) {													// more than 3 lines
					System.out.println("0 blank line more than 3 lines" +adChar);
					Phrase adddeclareval=new Phrase(value.substring(0, vChar),font9);
					PdfPCell adddeclarevalCell=new PdfPCell(adddeclareval);
					adddeclarevalCell.setBorder(0);
					adddeclarevalCell.setColspan(colspan);
					adddeclarevalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					table.addCell(adddeclarevalCell);
				}
				else {
					table.addCell(blank3cell);
					table.addCell(blank3cell);
					table.addCell(blank3cell);
				}
	}
/** Ends **/
}
