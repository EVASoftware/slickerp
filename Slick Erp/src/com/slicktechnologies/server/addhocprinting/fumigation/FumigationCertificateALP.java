package com.slicktechnologies.server.addhocprinting.fumigation;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.fumigation.Fumigation;

public class FumigationCertificateALP {

	public Document document;
	Fumigation fumigation;
	Company comp;
	List<Branch> branchlist=new ArrayList<Branch>();
	private  Font font16boldul,font12bold,font8bold,
				font8,font12boldul,font12,font16bold,
				font9bold,font10,font10bold,font10boldul,
				font14bold,font14,font9;
	private SimpleDateFormat fmt= new SimpleDateFormat("dd/MM/yyyy");   		//Updated By: Viraj Date: 08-03-2019 Description: To Change date format
	DecimalFormat df=new DecimalFormat("0.00");
	/**
	 * Updated By: Viraj
	 * Date: 14-01-2019
	 * Description: To restrict data in 3 lines 
	 */
	int  noOfChar = 103;
	int noOfLines = 3;
	/** Ends **/

	
	public  FumigationCertificateALP(){
		
		font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
		font9bold = new Font(Font.FontFamily.HELVETICA,9,Font.BOLD);
		font8= new Font(Font.FontFamily.HELVETICA,8);
		font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12=new Font(Font.FontFamily.HELVETICA,12);
		font10=new Font(Font.FontFamily.HELVETICA,10);
		font10bold = new Font(Font.FontFamily.HELVETICA  , 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA  , 10, Font.BOLD|Font.UNDERLINE);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font14 = new Font(Font.FontFamily.HELVETICA  , 14, Font.NORMAL); 
		font9 = new Font(Font.FontFamily.HELVETICA  , 8, Font.NORMAL); 						//updatedBy: Viraj Date:05-02-2019 Description: To reduce the font size
	}
	public void loadfugation(Long count) {
		
		fumigation=ofy().load().type(Fumigation.class).id(count).now();
		
		if(fumigation.getCompanyId()!=null)
			 comp=ofy().load().type(Company.class).filter("companyId",fumigation.getCompanyId()).first().now();
		else
			comp=ofy().load().type(Company.class).first().now();
		
		branchlist = ofy().load().type(Branch.class).filter("companyId",fumigation.getCompanyId()).filter("buisnessUnitName", fumigation.getBranch()).list();
	}

	public void createPdf() {
		createheader();
		createcertificateinfo();
		createTreatmentDetails();
		createDetailofGoods();
		Createadddeclaration();
	}
	private void Createadddeclaration() {
		
		PdfPTable addTab=new PdfPTable(1);
		addTab.setWidthPercentage(100);
		
		String	titlepdf="Additional Declaration :";
		
		Phrase titlephrase=new Phrase(titlepdf,font10bold);
		Paragraph titlepdfpara=new Paragraph();
		titlepdfpara.add(titlephrase);
		PdfPCell titlecell=new PdfPCell(titlepdfpara);
		titlecell.setBorder(0);
		addTab.addCell(titlecell);
		/**
		 * Updated By: Viraj
		 * Date: 14-01-2019
		 * Description: to add blank cell if  data not present
		 */
		Phrase blank3ph=new Phrase(" ",font9);
		PdfPCell blank3cell=new PdfPCell();
		blank3cell.addElement(blank3ph);
		blank3cell.setBorder(0);
		
		if(fumigation.getAdditionaldeclaration() != null && fumigation.getAdditionaldeclaration() != "") {
			getCellRemainingLines(noOfLines, noOfChar,0,fumigation.getAdditionaldeclaration(), addTab);
			System.out.println("adddeclaration size"+fumigation.getAdditionaldeclaration().length());
		} else {
			addTab.addCell(blank3cell);
			addTab.addCell(blank3cell);
			addTab.addCell(blank3cell);
		}
		/** Ends **/
//		Phrase invoiceno=new Phrase("Inv No."+"",font9);
//		PdfPCell invCell=new PdfPCell(invoiceno);
//		invCell.setBorder(0);
//		invCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		addTab.addCell(invCell);
//		
//		Phrase invoicedate=new Phrase("Inv Date."+"",font9);
//		PdfPCell invCelldate=new PdfPCell(invoicedate);
//		invCelldate.setBorder(0);
//		invCelldate.setHorizontalAlignment(Element.ALIGN_LEFT);
//		addTab.addCell(invCelldate);
		
		try {
			document.add(addTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		PdfPTable signtab=new PdfPTable(3);
		signtab.setWidthPercentage(100);
		
		try {
			signtab.setWidths(new float[]{35,25,40});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase signph=new Phrase("Endosed by Specified Officer of Dte.of PPQS Name : Signature and Office Seal",font9);
		PdfPCell signCell=new PdfPCell(signph);
		signCell.setBorder(0);
		signCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		signtab.addCell(signCell);
		
		Phrase blankph=new Phrase(" ",font9);
		PdfPCell blankCell=new PdfPCell(blankph);
		blankCell.setBorder(0);
		blankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		signtab.addCell(blankCell);
		
		Phrase accredetedph=new Phrase("Name & Signature Of Accredeted Fumigation Operater with seal & Date/Accredation Number",font9);
		PdfPCell accredetedCell=new PdfPCell(accredetedph);
		accredetedCell.setBorder(0);
		accredetedCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		signtab.addCell(accredetedCell);
		
		try {
			document.add(signtab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	private void createDetailofGoods() {
		
		PdfPTable detailsofgoodtab=new PdfPTable(3);
		detailsofgoodtab.setWidthPercentage(100);
		/**
		 * Updated By:Viraj
		 * Date: 15-01-2019
		 * Description: to add blank cell 
		 */
		noOfChar = 73;
		
		Phrase blank3ph=new Phrase(" ",font9);
		PdfPCell blank3cell=new PdfPCell();
		blank3cell.addElement(blank3ph);
		blank3cell.setBorder(0);
		/** Ends **/
		try {
			detailsofgoodtab.setWidths(new float[]{30,1,69});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			Phrase titleph=new Phrase("Details Of goods ",font10bold);
			PdfPCell titleCell=new PdfPCell(titleph);
			titleCell.setBorder(0);
			titleCell.setColspan(3);
			titleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			detailsofgoodtab.addCell(titleCell);
			
			
			 Phrase containernoph=new Phrase("Container number/seal number ",font9);
			 PdfPCell containernocell=new PdfPCell();
			 containernocell.addElement(containernoph);
			 containernocell.setBorder(0);
			 containernocell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 detailsofgoodtab.addCell(containernocell);
			 
			 Phrase colonph=new Phrase(":",font9);
			 PdfPCell coloncell=new PdfPCell();
			 coloncell.addElement(colonph);
			 coloncell.setBorder(0);
			 coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 detailsofgoodtab.addCell(coloncell);
			 /**
			  * Updated By: Viraj
			  * Date: 15-01-2019
			  * Description: Changed method according to no of lines required (to keep fixed format)
			  */
			 Phrase containernovalph=null;
			 if(fumigation.getConsignmentlink()!=null){
//				 containernovalph=new Phrase(fumigation.getConsignmentlink(),font9);
				 System.out.println("inside Container no/seal no value");
				 System.out.println("char: "+noOfChar);
				 System.out.println("consignment size: "+fumigation.getConsignmentlink().length());
				 getCellRemainingLines(noOfLines, noOfChar,3,fumigation.getConsignmentlink(), detailsofgoodtab);
			 }
			 else
			 {
//				 containernovalph=new Phrase("",font9);
				 detailsofgoodtab.addCell(blank3cell);
				 detailsofgoodtab.addCell(blank3cell);
				 detailsofgoodtab.addCell(blank3cell);
			 }
			 /** Ends **/
//			 PdfPCell containernovalphcell=new PdfPCell();
//			 containernovalphcell.addElement(containernovalph);
//			 containernovalphcell.setBorder(0);
//			 containernovalphcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			 detailsofgoodtab.addCell(containernovalphcell);
			 
			 	Phrase fromaddress=new Phrase("Name and address of exporter ",font9);
				PdfPCell fromaddcell=new PdfPCell();
				fromaddcell.addElement(fromaddress);
				fromaddcell.setBorder(0);
				detailsofgoodtab.addCell(fromaddcell);
				  
				detailsofgoodtab.addCell(coloncell);
				
				//************rohan added this on date : 30/07/2018 
				
				String fromAddress= "";
				if(fumigation.getFromAddess()!=null){
					fromAddress=fumigation.getFromAddess();
				}
				/**
				  * Updated By: Viraj
				  * Date: 15-01-2019
				  * Description: Changed method according to no of lines required (to keep fixed format)
				  */
				String companyName = "";
				if(fumigation.getFromCompanyname() != null) {
					companyName = fumigation.getFromCompanyname()+"\n"+fromAddress;
				}
				else {
					companyName = fromAddress;
				}
				if(companyName != null) {
					System.out.println("companyName size: "+companyName.length());
				getCellRemainingLines(noOfLines, noOfChar,3,companyName, detailsofgoodtab);
				}else {
					 detailsofgoodtab.addCell(blank3cell);
					 detailsofgoodtab.addCell(blank3cell);
					 detailsofgoodtab.addCell(blank3cell);
				}
				/** Ends **/
//				Phrase fromaddressvalph=new Phrase(fumigation.getFromCompanyname()+"\n"+fromAddress,font9);
//				PdfPCell fromaddvalcell=new PdfPCell();
//				fromaddvalcell.addElement(fromaddressvalph);
//				fromaddvalcell.setBorder(0);
//				detailsofgoodtab.addCell(fromaddvalcell);
				
				Phrase toaddress=new Phrase(" Name and address of consignee ",font9);
				PdfPCell toaddcell=new PdfPCell();
				toaddcell.addElement(toaddress);
				toaddcell.setBorder(0);
				detailsofgoodtab.addCell(toaddcell);
			  
				detailsofgoodtab.addCell(coloncell);
				
				//************rohan added this on date : 30/07/2018 
				/**
				  * Updated By: Viraj
				  * Date: 15-01-2019
				  * Description: Changed method according to no of lines required (to keep fixed format)
				  */
				String toAdd= "";
				if(fumigation.getToAddress()!=null){
					toAdd=fumigation.getToAddress();
				}
				String comp = "";
				if(fumigation.getToCompanyname() != null) {
					comp = fumigation.getToCompanyname()+"\n"+toAdd;
				} else {
					comp = toAdd;
				}
				if(comp != null) {
					System.out.println("comp size: "+comp.length());
					getCellRemainingLines(noOfLines, noOfChar,3,comp, detailsofgoodtab);
				} else {
					 detailsofgoodtab.addCell(blank3cell);
					 detailsofgoodtab.addCell(blank3cell);
					 detailsofgoodtab.addCell(blank3cell);
				}
				/** Ends **/
//				Phrase toaddressvalph=new Phrase(fumigation.getToCompanyname()+"\n"+toAdd,font9);
//				PdfPCell toaddvalcell=new PdfPCell();
//				toaddvalcell.addElement(toaddressvalph);
//				toaddvalcell.setBorder(0);
//				detailsofgoodtab.addCell(toaddvalcell);
				
				Phrase notifiyaddress=new Phrase(" Name and address of notified party  ",font9);
				PdfPCell notifiyaddresscell=new PdfPCell();
				notifiyaddresscell.addElement(notifiyaddress);
				notifiyaddresscell.setBorder(0);
				detailsofgoodtab.addCell(notifiyaddresscell);
			  
				detailsofgoodtab.addCell(coloncell);
				
				//************rohan added this on date : 30/07/2018 
				/**
				  * Updated By: Viraj
				  * Date: 15-01-2019
				  * Description: Changed method according to no of lines required (to keep fixed format)
				  */
				String notifyAddreess= "";
				if(fumigation.getNotifiedAdd()!=null){
					notifyAddreess=fumigation.getNotifiedAdd();
				}
				String notifiedComp = "";
				if(fumigation.getNotifiedPartyCompanyname() != null) {
					notifiedComp = fumigation.getNotifiedPartyCompanyname()+"\n"+notifyAddreess;
				} else {
					notifiedComp = notifyAddreess;
				}
				if(notifiedComp != null) {
					System.out.println("notifiedComp size: "+notifiedComp.length());
					getCellRemainingLines(noOfLines, noOfChar,3,notifiedComp, detailsofgoodtab);
				} else {
					 detailsofgoodtab.addCell(blank3cell);
					 detailsofgoodtab.addCell(blank3cell);
					 detailsofgoodtab.addCell(blank3cell);
				}
				/** Ends **/
//				Phrase notifiyaddressvalph=new Phrase(fumigation.getNotifiedPartyCompanyname()+"\n"+notifyAddreess,font9);
//				PdfPCell notifiyaddressvalcell=new PdfPCell();
//				notifiyaddressvalcell.addElement(notifiyaddressvalph);
//				notifiyaddressvalcell.setBorder(0);
//				detailsofgoodtab.addCell(notifiyaddressvalcell);
				
				
				Phrase typedescph=new Phrase(" Type and description of cargo  ",font9);
				PdfPCell typedesccell=new PdfPCell();
				typedesccell.addElement(typedescph);
				typedesccell.setBorder(0);
				detailsofgoodtab.addCell(typedesccell);
			  
				detailsofgoodtab.addCell(coloncell);
				
				/**
				  * Updated By: Viraj
				  * Date: 15-01-2019
				  * Description: Changed method according to no of lines required (to keep fixed format)
				  */
				String typeCargo = "";
				if(fumigation.getTypeOfCargo() != null) {
					typeCargo = fumigation.getTypeOfCargo();
				}else {
					typeCargo = "";
				}
				String descCargo = "";
				if(fumigation.getDescriptionOfCargo() != null) {
					descCargo = fumigation.getDescriptionOfCargo()+" "+typeCargo;
				} else {
					descCargo = typeCargo;
				}
				if(descCargo != null) {
					System.out.println("descCargo size: "+descCargo.length());
					getCellRemainingLines(noOfLines, noOfChar, 3, descCargo, detailsofgoodtab);
				} else {
					 detailsofgoodtab.addCell(blank3cell);
					 detailsofgoodtab.addCell(blank3cell);
					 detailsofgoodtab.addCell(blank3cell);
				}
//				Phrase typedescphval=new Phrase(fumigation.getTypeOfCargo()+"  "+fumigation.getDescriptionOfCargo(),font9);
//				PdfPCell typedescphvalcell=new PdfPCell();
//				typedescphvalcell.addElement(typedescphval);
//				typedescphvalcell.setBorder(0);
//				detailsofgoodtab.addCell(typedescphvalcell);
				/** Ends **/
				
				Phrase quantyph=new Phrase(" Quantity(MTs)/No Of packages ",font9);
				PdfPCell quantycell=new PdfPCell();
				quantycell.addElement(quantyph);
				quantycell.setBorder(0);
				detailsofgoodtab.addCell(quantycell);
			  
				detailsofgoodtab.addCell(coloncell);
				
				Phrase quantyval=new Phrase(fumigation.getQuantitydeclare(),font9);
				PdfPCell quantyvalcell=new PdfPCell();
				quantyvalcell.addElement(quantyval);
				quantyvalcell.setBorder(0);
				detailsofgoodtab.addCell(quantyvalcell);
				
				 Phrase discriptionofmaterialph=new Phrase("Description of packaging material ",font9);
				 PdfPCell discriptionofmaterialcell=new PdfPCell();
				 discriptionofmaterialcell.addElement(discriptionofmaterialph);
				 discriptionofmaterialcell.setBorder(0);
				 containernocell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 detailsofgoodtab.addCell(discriptionofmaterialcell);
				 
				
				 detailsofgoodtab.addCell(coloncell);
				 
				 Phrase desmaterialvalph=null;
				 if(fumigation.getPackingValue()!=null){
					 desmaterialvalph=new Phrase(fumigation.getPackingValue(),font9);
				 }
				 else
				 {
					 desmaterialvalph=new Phrase("",font9);
				 }
			 
				 PdfPCell desmaterialvalphcell=new PdfPCell();
				 desmaterialvalphcell.addElement(desmaterialvalph);
				 desmaterialvalphcell.setBorder(0);
				 desmaterialvalphcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 detailsofgoodtab.addCell(desmaterialvalphcell);
				
				
				 Phrase namevessleph=new Phrase("Vessel name ",font9);
				 PdfPCell namevesslecell=new PdfPCell();
				 namevesslecell.addElement(namevessleph);
				 namevesslecell.setBorder(0);
				 namevesslecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 detailsofgoodtab.addCell(namevesslecell);
				 
				
				 detailsofgoodtab.addCell(coloncell);
				 
				 Phrase namevesslephval=null;
				 if(fumigation.getNameofvessel()!=null){
					 namevesslephval=new Phrase(fumigation.getNameofvessel(),font9);
				 }
				 else
				 {
					 namevesslephval=new Phrase("",font9);
				 }
			 
				 PdfPCell namevesslephvalcell=new PdfPCell();
				 namevesslephvalcell.addElement(namevesslephval);
				 namevesslephvalcell.setBorder(0);
				 namevesslephvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 detailsofgoodtab.addCell(namevesslephvalcell);
				 
				 Phrase portofloadph=new Phrase("Port of loading ",font9);
				 PdfPCell portofloadcell=new PdfPCell();
				 portofloadcell.addElement(portofloadph);
				 portofloadcell.setBorder(0);
				 portofloadcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 detailsofgoodtab.addCell(portofloadcell);
				 
				
				 detailsofgoodtab.addCell(coloncell);
				 
				 Phrase portofloadcellval=null;
				 if(fumigation.getPortncountryloading()!=null){
					 portofloadcellval=new Phrase(fumigation.getPortncountryloading(),font9);
				 }
				 else
				 {
					 portofloadcellval=new Phrase("",font9);
				 }
			 
				 PdfPCell portofloadcellvalcell=new PdfPCell();
				 portofloadcellvalcell.addElement(portofloadcellval);
				 portofloadcellvalcell.setBorder(0);
				 portofloadcellvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 detailsofgoodtab.addCell(portofloadcellvalcell);
				 
				 
				 Phrase shippingmarkph=new Phrase("Shipping mark or brand  ",font9);
				 PdfPCell shippingmarkcell=new PdfPCell();
				 shippingmarkcell.addElement(shippingmarkph);
				 shippingmarkcell.setBorder(0);
				 shippingmarkcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 detailsofgoodtab.addCell(shippingmarkcell);
				 
				
				 detailsofgoodtab.addCell(coloncell);
				 
				 Phrase shippingmarkval=null;
				 if(fumigation.getPortncountryloading()!=null){
					 shippingmarkval=new Phrase(fumigation.getPortncountryloading(),font9);
				 }
				 else
				 {
					 shippingmarkval=new Phrase("",font9);
				 }
			 
				 PdfPCell shippingmarkvalcell=new PdfPCell();
				 shippingmarkvalcell.addElement(shippingmarkval);
				 shippingmarkvalcell.setBorder(0);
				 shippingmarkvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 detailsofgoodtab.addCell(shippingmarkvalcell);
				 
				 Phrase portofentryph=new Phrase("Port of entry ",font9);
				 PdfPCell portofentrycell=new PdfPCell();
				 portofentrycell.addElement(portofentryph);
				 portofentrycell.setBorder(0);
				 portofentrycell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 detailsofgoodtab.addCell(portofentrycell);
				 
				
				 detailsofgoodtab.addCell(coloncell);
				 
				 Phrase portofentryval=null;
				 if(fumigation.getDeclarepointentry()!=null){
					 portofentryval=new Phrase(fumigation.getDeclarepointentry(),font9);
				 }
				 else
				 {
					 portofentryval=new Phrase("",font9);
				 }
			 
				 PdfPCell portofentryvalcell=new PdfPCell();
				 portofentryvalcell.addElement(portofentryval);
				 portofentryvalcell.setBorder(0);
				 portofentryvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 detailsofgoodtab.addCell(portofentryvalcell);
				 
				 
				 
				 try {
					document.add(detailsofgoodtab);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 
	}
	private void createTreatmentDetails() {
		PdfPTable treattable=new PdfPTable(6);
		 
		treattable.setWidthPercentage(100f);
		 
		 try {
			 treattable.setWidths(new float[]{1.7f,0.2f,2.0f,1.7f,0.2f,2.0f});
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 
		    String	titlepdf="Details Of treatment";
			Phrase titlephrase=new Phrase(titlepdf,font10bold);
			Paragraph titlepdfpara=new Paragraph();
			titlepdfpara.add(titlephrase); 
			PdfPCell titlecell=new PdfPCell(titlepdfpara);
			titlecell.setBorder(0);
			titlecell.setPadding(0);
			titlecell.setColspan(6);
			titlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			treattable.addCell(titlecell);
			
			Phrase namefumihation=new Phrase("Name of fumigant",font9);
			 PdfPCell namefumicell=new PdfPCell();
			 namefumicell.addElement(namefumihation);
			 namefumicell.setBorder(0);
			 namefumicell.setPadding(0);
			 namefumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(namefumicell);
			 
			 Phrase seperator=new Phrase(":",font9);
			 PdfPCell seperatorcell=new PdfPCell();
			 seperatorcell.addElement(seperator);
			 seperatorcell.setBorder(0);
			 seperatorcell.setPadding(0);
			 seperatorcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			 treattable.addCell(seperatorcell);
			 
			 Phrase namefumiValue=null;
			 if(fumigation.getNameoffumigation()!=null){
				 namefumiValue=new Phrase(fumigation.getNameoffumigation(),font9);
			 }
			 else
			 {
			 namefumiValue=new Phrase("",font9);
			 }
		 
			 PdfPCell namefumiValuecell=new PdfPCell();
			 namefumiValuecell.addElement(namefumiValue);
			 namefumiValuecell.setBorder(0);
			 namefumiValuecell.setPadding(0);
			 namefumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(namefumiValuecell);
			//
			 Phrase dateoffumi=new Phrase("Date of fumigation",font9);
			 PdfPCell datefumicell=new PdfPCell();
			 datefumicell.addElement(dateoffumi);
			 datefumicell.setBorder(0);
			 datefumicell.setPadding(0);
			 datefumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(datefumicell);
			 
			 treattable.addCell(seperatorcell);
			 
			 Phrase dateoffumiValue=null;
			 if(fumigation.getDateoffumigation()!=null){
				 dateoffumiValue=new Phrase(fmt.format(fumigation.getDateoffumigation()),font9);
			 }
			 else
			 {
			 dateoffumiValue=new Phrase("",font9);
			 }
		 
			 PdfPCell datefumiValuecell=new PdfPCell();
			 datefumiValuecell.addElement(dateoffumiValue);
			 datefumiValuecell.setBorder(0);
			 datefumiValuecell.setPadding(0);
			 datefumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(datefumiValuecell);
			 
			 
			 Phrase placeoffumi=new Phrase("Place of fumigation",font9);
			 PdfPCell placefumicell=new PdfPCell();
			 placefumicell.addElement(placeoffumi);
			 placefumicell.setBorder(0);
			 placefumicell.setPadding(0);
			 placefumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(placefumicell);
			 
			 treattable.addCell(seperatorcell);
			 
			 Phrase placefumiValue=null;
			 if(fumigation.getPlaceoffumigation()!=null){
				 placefumiValue=new Phrase(fumigation.getPlaceoffumigation(),font9);
			 }
			 else
			 {
			 placefumiValue=new Phrase("",font9);
			 }
		 
			 PdfPCell placefumiValuecell=new PdfPCell();
			 placefumiValuecell.addElement(placefumiValue);
			 placefumiValuecell.setBorder(0);
			 placefumiValuecell.setPadding(0);
			 placefumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(placefumiValuecell);
			 
			 
			 
			 
			 
			 Phrase dosefumigation=new Phrase("Dosage of fumigation",font9);
			 PdfPCell dosafumicell=new PdfPCell();
			 dosafumicell.addElement(dosefumigation);
			 dosafumicell.setBorder(0);
			 dosafumicell.setPadding(0);
			 dosafumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(dosafumicell);
			 
			 treattable.addCell(seperatorcell);
			 
			 Phrase dosefumiValue=null;
				 if(fumigation.getDoseratefumigation()!=null){
					 dosefumiValue=new Phrase(fumigation.getDoseratefumigation()+" gms/m3",font9);
				 }
			 else
			 {
				 dosefumiValue=new Phrase("",font9);
			 }
			 
			 PdfPCell dosafumiValuecell=new PdfPCell();
			 dosafumiValuecell.addElement(dosefumiValue);
			 dosafumiValuecell.setBorder(0);
			 dosafumiValuecell.setPadding(0);
			 dosafumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(dosafumiValuecell);
			 
			 
			 Phrase durationfumi=new Phrase("Duration of fumigation(in days/hrs)",font9);
			 PdfPCell durarationfumicell=new PdfPCell();
			 durarationfumicell.addElement(durationfumi);
			 durarationfumicell.setBorder(0);
			 durarationfumicell.setPadding(0);
			 durarationfumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(durarationfumicell);
			 
			 treattable.addCell(seperatorcell);
			 
			 Phrase durafumiValue=null;
			 if(fumigation.getDuration()!=null){
				 durafumiValue=new Phrase(fumigation.getDuration()+" hrs",font9);
			 }
			 else
			 {
			 durafumiValue=new Phrase("",font9);
			 }
		 
			 PdfPCell durafumiValuecell=new PdfPCell();
			 durafumiValuecell.addElement(durafumiValue);
			 durafumiValuecell.setBorder(0);
			 durafumiValuecell.setPadding(0);
			 durafumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(durafumiValuecell);
			 
			 Phrase averagehumidity=new Phrase("Average ambient humidity",font9);
			 PdfPCell averagehumiditycell=new PdfPCell();
			 averagehumiditycell.addElement(averagehumidity);
			 averagehumiditycell.setBorder(0);
			 averagehumiditycell.setPadding(0);
			 averagehumiditycell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(averagehumiditycell);
			 
			 treattable.addCell(seperatorcell);
			 
			 Phrase averagehumidityval=null;
				 if(fumigation.getHumidity()!=null){
					 averagehumidityval=new Phrase(fumigation.getHumidity()+" % ",font9);
				 }
			 else
			 {
				 averagehumidityval=new Phrase("",font9);
			 }
			 
			 PdfPCell averagehumidityvalcell=new PdfPCell();
			 averagehumidityvalcell.addElement(averagehumidityval);
			 averagehumidityvalcell.setBorder(0);
			 averagehumidityvalcell.setPadding(0);
			 averagehumidityvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(averagehumidityvalcell);
			 
			 Phrase fumperformed=new Phrase("Fumigation performed ",font9);
			 PdfPCell fumperformedcell=new PdfPCell();
			 fumperformedcell.addElement(fumperformed);
			 fumperformedcell.setBorder(0);
			 fumperformedcell.setPadding(0);
			 fumperformedcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(fumperformedcell);
			 
			 treattable.addCell(seperatorcell);
			 
			 Phrase fumperformedValue=null;
			 if(fumigation.getFumigationPerformedIn()!=null){
				 fumperformedValue=new Phrase(fumigation.getFumigationPerformedIn(),font9);
			 }
			 else
			 {
				 fumperformedValue=new Phrase("",font9);
			 }
		 
			 PdfPCell fumperformedValuecell=new PdfPCell();
			 fumperformedValuecell.addElement(fumperformedValue);
			 fumperformedValuecell.setBorder(0);
			 fumperformedValuecell.setPadding(0);
			 fumperformedValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(fumperformedValuecell);
			 
		 
			 Phrase minaietemp=new Phrase("Min. air temp.(in "+"\u00b0"+"C)",font9);
			 PdfPCell tempfumicell=new PdfPCell();
			 tempfumicell.addElement(minaietemp);
			 tempfumicell.setBorder(0);
			 tempfumicell.setPadding(0);
			 tempfumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(tempfumicell);
			 
			 treattable.addCell(seperatorcell);
			 
			 Phrase tempfumiValue=null;
				 if(fumigation.getMinairtemp()!=null){
					 tempfumiValue=new Phrase(fumigation.getMinairtemp()+" ",font9);
				 }
			 else
			 {
				 tempfumiValue=new Phrase("",font9);
			 }
			 
			 PdfPCell tempfumiValuecell=new PdfPCell();
			 tempfumiValuecell.addElement(tempfumiValue);
			 tempfumiValuecell.setBorder(0);
			 tempfumiValuecell.setPadding(0);
			 tempfumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(tempfumiValuecell);
			 
			 try {
				document.add(treattable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
	}
	private void createcertificateinfo() {
		
		PdfPTable certificatetab=new PdfPTable(5);
		certificatetab.setWidthPercentage(100);
		certificatetab.setSpacingBefore(10);
		try {
			certificatetab.setWidths(new float[]{20,25,10,20,25});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase srph=new Phrase("S.No.:",font8bold);
		PdfPCell srCell=new PdfPCell(srph);
		srCell.setBorder(0);
		srCell.setPadding(0);
		srCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(srCell);
		
		Phrase srvalph=new Phrase(fumigation.getSerialNumber()+"",font8bold);
		PdfPCell srvalCell=new PdfPCell(srvalph);
		srvalCell.setBorder(0);
		srvalCell.setPadding(0);
		srvalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(srvalCell);
		
		Phrase blankph=new Phrase(" ",font8);
		PdfPCell blankCell=new PdfPCell(blankph);
		blankCell.setBorder(0);
		blankCell.setPadding(0);
		blankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(blankCell);
		
		Phrase certnoph=new Phrase("Certificate Number : ",font8bold);
		PdfPCell certnoCell=new PdfPCell(certnoph);
		certnoCell.setBorder(0);
		certnoCell.setPadding(0);
		certnoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(certnoCell);
		
		
		Phrase certnoValph=new Phrase(fumigation.getCertificateNo(),font8bold);
		PdfPCell certnoValCell=new PdfPCell(certnoValph);
		certnoValCell.setBorder(0);
		certnoValCell.setPadding(0);
		certnoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(certnoValCell);
		
		Phrase regdph=new Phrase("Dte.PPQS Regd.No :",font8bold);
		PdfPCell regdCell=new PdfPCell(regdph);
		regdCell.setBorder(0);
		regdCell.setPadding(0);
		regdCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(regdCell);
		
		Phrase regdValph=new Phrase(fumigation.getDtePPQSRegistrationNumber(),font8bold);
		PdfPCell regdValCell=new PdfPCell(regdValph);
		regdValCell.setBorder(0);
		regdValCell.setPadding(0);
		regdValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(regdValCell);
		
		certificatetab.addCell(blankCell);
		
		Phrase dateofissueph=new Phrase("Date Of issue : ",font8bold);
		PdfPCell dateofissueValCell=new PdfPCell(dateofissueph);
		dateofissueValCell.setBorder(0);
		dateofissueValCell.setPadding(0);
		dateofissueValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(dateofissueValCell);
		
		Phrase dateofissuevalph=new Phrase(fmt.format(fumigation.getDateofissue()),font8bold);
		PdfPCell dateofissuevalCell=new PdfPCell(dateofissuevalph);
		dateofissuevalCell.setBorder(0);
		dateofissuevalCell.setPadding(0);
		dateofissuevalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(dateofissuevalCell);
		
		String country = "";
		if(fumigation.getImportcountry()!=null){
			country = fumigation.getImportcountry().toUpperCase();
		}
		Phrase cercontentph=new Phrase("This is to certify that the good described below where trated in accordance with fumigation treatment requirment of importing country "+country+" and declare that consigment has been verified free of imprevioter surface/layer such as plastic wrapping or laminated plastic films,laquered or painted surface,aluminium foil,tarred or waxed paper etc. that may adversely effect the penetration of fumigant,prior to fumigation.",font10);
		PdfPCell cercontentCell=new PdfPCell(cercontentph);
		cercontentCell.setPaddingTop(5);
		cercontentCell.setBorder(0);
		cercontentCell.setPaddingTop(5);
		cercontentCell.setColspan(5);
		cercontentCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(cercontentCell);
		
		try {
			document.add(certificatetab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void createheader() {
		
		PdfPTable companyDetails=new PdfPTable(1);
		companyDetails.setWidthPercentage(100);
		
		
		Phrase compName =new Phrase(comp.getBusinessUnitName(),font16bold);
		PdfPCell compNameCell=new PdfPCell(compName);
		compNameCell.setBorder(0);
		compNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		companyDetails.addCell(compNameCell);
		
		/**
		 * Updated By: Viraj
		 * Date: 27-02-2019
		 * Description: Hard coded statement
		 */
		Phrase approvedBy =new Phrase("(Approved by National Plant Protection Organization, Government of India)",font10bold);
		PdfPCell approvedByCell=new PdfPCell(approvedBy);
		approvedByCell.setBorder(0);
		approvedByCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		companyDetails.addCell(approvedByCell);
		
		Phrase approvedBy1 =new Phrase("AN ISO 9001:2009 CERTIFIED COMPANY",font10bold);
		PdfPCell approvedBy1Cell=new PdfPCell(approvedBy1);
		approvedBy1Cell.setBorder(0);
		approvedBy1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		companyDetails.addCell(approvedBy1Cell);
		/** Ends **/
		
		/**
		 * Updated By: Viraj
		 * Date: 14-01-2019
		 * Description: To add company description
		 */
		if(comp.getDescription() != null) {
			Phrase compDesc =new Phrase(comp.getDescription(),font10bold);
			PdfPCell compDescCell=new PdfPCell(compDesc);
			compDescCell.setBorder(0);
			compDescCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			companyDetails.addCell(compDescCell);
		} else {
			Phrase compDesc =new Phrase("",font10bold);
			PdfPCell compDescCell=new PdfPCell(compDesc);
			compDescCell.setBorder(0);
			compDescCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			companyDetails.addCell(compDescCell);
		}
		/** Ends **/
		
		String branchaddress=null;
		System.out.println("branchlist"+branchlist.size());
		for (int i = 0; i <branchlist.size(); i++) {
			branchaddress=branchlist.get(i).getAddress().getCompleteAddress();
		}
		System.out.println("branchaddress"+branchaddress);
		
		Phrase branchadd=new Phrase ("Branch Office :"+branchaddress,font10);
		PdfPCell branchaddCell=new PdfPCell(branchadd);
		branchaddCell.setBorder(0);
		branchaddCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		companyDetails.addCell(branchaddCell);
		
//		Phrase isoph=new Phrase ("AN ISO 9001 :",font10);
//		PdfPCell isoCell=new PdfPCell(isoph);
//		isoCell.setBorder(0);
//		isoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		companyDetails.addCell(isoCell);
		
		try {
			document.add(companyDetails);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		PdfPTable titletab=new PdfPTable(1);
		titletab.setWidthPercentage(100);
		
		Phrase titleph=new Phrase("FUMIGATION CERTIFICATE",font12);
		PdfPCell titleCell=new PdfPCell(titleph);
		titleCell.setBorder(0);
		titleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		titletab.addCell(titleCell);
		
		try {
			document.add(titletab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	/**
	 * Update By: Viraj
	 * Date: 15-01-2019
	 * Description: To add cell and remaining blank cell according to no of lines and decided lines for that cell
	 * @param noOfLines
	 * @param noOfChar
	 * @param value
	 * @param table
	 */
	public void getCellRemainingLines(int noOfLines,int noOfChar,int colspan,String value,PdfPTable table) {
		int adChar = value.length();
		int vChar = noOfLines * noOfChar;
		int tChar = 3 * noOfChar ;
		int t1Char = 2 * noOfChar;
		int t2Char = 1 * noOfChar;
		
		Phrase blank3ph=new Phrase(" ",font9);
		PdfPCell blank3cell=new PdfPCell();
		blank3cell.addElement(blank3ph);
		blank3cell.setColspan(colspan);
		blank3cell.setBorder(0);
		// 1 line
		if((0 <= adChar) && (adChar <= t2Char)) {
			System.out.println("2 blank line" +adChar);
			Phrase adddeclareval=new Phrase(value.substring(0, adChar),font9);
			PdfPCell adddeclarevalCell=new PdfPCell(adddeclareval);
			adddeclarevalCell.setBorder(0);
			adddeclarevalCell.setColspan(colspan);
			adddeclarevalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(adddeclarevalCell);
			table.addCell(blank3cell);
			table.addCell(blank3cell);
		} else if((t2Char <= adChar) && (adChar <= t1Char)) {								//line 2
					System.out.println("1 blank line" +adChar);
					Phrase adddeclareval=new Phrase(value.substring(0, adChar),font9);
					PdfPCell adddeclarevalCell=new PdfPCell(adddeclareval);
					adddeclarevalCell.setBorder(0);
					adddeclarevalCell.setColspan(colspan);
					adddeclarevalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					table.addCell(adddeclarevalCell);
					table.addCell(blank3cell);
				} else if((t1Char <= adChar) && (adChar <= tChar)) {						//line 3
					System.out.println("0 blank line 3 lines exact" +adChar);
					Phrase adddeclareval=new Phrase(value.substring(0, adChar),font9);
					PdfPCell adddeclarevalCell=new PdfPCell(adddeclareval);
					adddeclarevalCell.setBorder(0);
					adddeclarevalCell.setColspan(colspan);
					adddeclarevalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					table.addCell(adddeclarevalCell);
				} else if(adChar > tChar) {													// more than 3 lines
					System.out.println("0 blank line more than 3 lines" +adChar);
					Phrase adddeclareval=new Phrase(value.substring(0, vChar),font9);
					PdfPCell adddeclarevalCell=new PdfPCell(adddeclareval);
					adddeclarevalCell.setBorder(0);
					adddeclarevalCell.setColspan(colspan);
					adddeclarevalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					table.addCell(adddeclarevalCell);
				}
				else {
					table.addCell(blank3cell);
					table.addCell(blank3cell);
					table.addCell(blank3cell);
				}
	}
/** Ends **/
}