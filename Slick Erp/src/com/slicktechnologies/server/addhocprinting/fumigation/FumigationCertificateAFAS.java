package com.slicktechnologies.server.addhocprinting.fumigation;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.fumigation.Fumigation;

public class FumigationCertificateAFAS {

	public Document document;
	Fumigation fumigation;
	Company comp;
	List<Branch> branchlist=new ArrayList<Branch>();
	private  Font font16boldul,font12bold,font8bold,
				font8,font12boldul,font12,font16bold,
				font9bold,font10,font10bold,font10boldul,
				font14bold,font14,font9;
	private SimpleDateFormat fmt= new SimpleDateFormat("dd/MMM/yyyy");
	DecimalFormat df=new DecimalFormat("0.00");
	/**
	 * Updated By: Viraj
	 * Date: 14-01-2019
	 * Description: To restrict data in 3 lines 
	 */
	int  noOfChar = 103;
	int noOfLines = 3;
	int rowSpan = 3;
	/** Ends **/

	
	public  FumigationCertificateAFAS(){

		
		font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
		font9bold = new Font(Font.FontFamily.HELVETICA,9,Font.BOLD);
		font8= new Font(Font.FontFamily.HELVETICA,8);
		font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12=new Font(Font.FontFamily.HELVETICA,12);
		font10=new Font(Font.FontFamily.HELVETICA,10);
		font10bold = new Font(Font.FontFamily.HELVETICA  , 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA  , 10, Font.BOLD|Font.UNDERLINE);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font14 = new Font(Font.FontFamily.HELVETICA  , 14, Font.NORMAL); 
		font9 = new Font(Font.FontFamily.HELVETICA  , 8, Font.NORMAL);						//updatedBy: Viraj Date:05-02-2019 Description: To reduce the font size 
	
	}
	
	public void loadfugation(Long count) {
		
		fumigation=ofy().load().type(Fumigation.class).id(count).now();
		
		if(fumigation.getCompanyId()!=null)
			 comp=ofy().load().type(Company.class).filter("companyId",fumigation.getCompanyId()).first().now();
		else
			comp=ofy().load().type(Company.class).first().now();
		
		branchlist = ofy().load().type(Branch.class).filter("companyId",fumigation.getCompanyId()).filter("buisnessUnitName", fumigation.getBranch()).list();
		
	}

	public void createPdf() {
		createheader();
		createcertificateinfo();
		createTargetOfFumigation();
		createDetailOfTretment();
		createfumigationDetails();
		createadditionalDeclaration();
		
	}

	private void createadditionalDeclaration() {
		
		PdfPTable diclaretab1=new PdfPTable(1);
		diclaretab1.setWidthPercentage(100);
//		/**
//		 * Updated By: Viraj
//		 * Date: 14-01-2019
//		 * Description: To restrict data in 3 lines 
//		 */
//		int tChar = noOfChar * noOfLines;
//		int adChar = fumigation.getAdditionaldeclaration().length();
//		/** Ends **/
		
		try {
			diclaretab1.setWidths(new float[]{100});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 Phrase diclareph=new Phrase("DECLARATION ",font10bold);
		 PdfPCell diclarecell=new PdfPCell(diclareph);
//		 diclarecell.addElement(diclareph);
		 diclarecell.setBorder(0);
		 diclarecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		 diclaretab1.addCell(diclarecell);
		 
		 Phrase diclarevalph=new Phrase(fumigation.getDeclaration(),font9);		//Updated By: Viraj Date: 16-03-2019 changed font size
		 PdfPCell diclarevalcell=new PdfPCell();
		 diclarevalcell.addElement(diclarevalph);
		 diclarevalcell.setBorder(0);
		 diclarevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 diclaretab1.addCell(diclarevalcell);
		 
		 Phrase blank7ph=new Phrase(" ",font10bold);
		 PdfPCell blank7cell=new PdfPCell();
		 blank7cell.addElement(blank7ph);
		 blank7cell.setBorderWidthLeft(0);
		 blank7cell.setBorderWidthRight(0);
		 blank7cell.setBorderWidthTop(0);
		 blank7cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		 diclaretab1.addCell(blank7cell);
		 
		 Phrase adddiclareph=new Phrase("ADDITIONAL DECLARATION  ",font10bold);
		 PdfPCell adddiclarecell=new PdfPCell(adddiclareph);
//		 adddiclarecell.addElement(adddiclareph);
		 adddiclarecell.setBorder(0);
		 adddiclarecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		 diclaretab1.addCell(adddiclarecell);
		 
		 /**
			 * Updated By: Viraj
			 * Date: 14-01-2019
			 * Description: to add blank cell if  data not present
			 */
			Phrase blank3ph=new Phrase(" ",font9);
			PdfPCell blank3cell=new PdfPCell();
			blank3cell.addElement(blank3ph);
			blank3cell.setBorder(0);
			noOfChar = 103;
			if(fumigation.getAddDeclaration() != null && fumigation.getAddDeclaration() != "") {
				getCellRemainingLines(noOfLines, noOfChar,0,fumigation.getAddDeclaration(), diclaretab1);
				System.out.println("adddeclaration size"+fumigation.getAddDeclaration().length());
			} else {
				diclaretab1.addCell(blank3cell);
				diclaretab1.addCell(blank3cell);
				diclaretab1.addCell(blank3cell);
			}
			/** Ends **/
		 diclaretab1.addCell(blank7cell);
		 
		 try {
			document.add(diclaretab1);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 PdfPTable signtab=new PdfPTable(1);
		 signtab.setWidthPercentage(100);
			
			try {
				signtab.setWidths(new float[]{1});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			 Phrase signphline=new Phrase("----------------",font10bold);
			 PdfPCell signcellline=new PdfPCell(signphline);
//			 signcellline.addElement(signphline);
			 signcellline.setBorder(0);
			 signcellline.setHorizontalAlignment(Element.ALIGN_CENTER);
			 signtab.addCell(signcellline);
			 
			 Phrase signph=new Phrase("signature",font10bold);
			 PdfPCell signcell=new PdfPCell(signph);
//			 signcell.addElement(signph);
			 signcell.setBorder(0);
			 signcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			 signtab.addCell(signcell);
			 
			 Phrase signvalph=new Phrase(fumigation.getAccreditedFumigator(),font10bold);
			 PdfPCell signvalcell=new PdfPCell(signvalph);
//			 signvalcell.addElement(signvalph);
			 signvalcell.setBorder(0);
			 signvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			 signtab.addCell(signvalcell);
			 
			 Phrase signnameph=new Phrase("Name Of Accredited Fumigator ",font10bold);
			 PdfPCell signnamecell=new PdfPCell(signnameph);
//			 signnamecell.addElement(signnameph);
			 signnamecell.setBorder(0);
			 signnamecell.setHorizontalAlignment(Element.ALIGN_CENTER);
			 signtab.addCell(signnamecell);
			 
			 
			 PdfPTable datesigntab=new PdfPTable(1);
			 datesigntab.setWidthPercentage(100);
				
				try {
					datesigntab.setWidths(new float[]{100});
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			 
			 Phrase datenoph=new Phrase(fmt.format(fumigation.getDateoffumigation()),font10bold);
			 PdfPCell datenocell=new PdfPCell(datenoph);
//			 datenocell.addElement(datenoph);
			 datenocell.setBorder(0);
			 datenocell.setHorizontalAlignment(Element.ALIGN_CENTER);
			 datesigntab.addCell(datenocell);
			 
			 Phrase dateph=new Phrase("Date",font10bold);
			 PdfPCell datecell=new PdfPCell(dateph);
//			 datecell.addElement(dateph);
			 datecell.setBorder(0);
			 datecell.setHorizontalAlignment(Element.ALIGN_CENTER);
			 datesigntab.addCell(datecell);
			 
			 Phrase accreditenoph=new Phrase(fumigation.getAfasRegNo(),font10bold);
			 PdfPCell accreditenocell=new PdfPCell(accreditenoph);
//			 accreditenocell.addElement(accreditenoph);
			 accreditenocell.setBorder(0);
			 accreditenocell.setHorizontalAlignment(Element.ALIGN_CENTER);
			 datesigntab.addCell(accreditenocell);
			 
//			 Phrase accreditenamevalph=new Phrase("AFAS Accreditation Number",font10bold);
//			 PdfPCell accreditenamevalcell=new PdfPCell(accreditenamevalph);
////			 accreditenamevalcell.addElement(accreditenamevalph);
//			 accreditenamevalcell.setBorder(0);
//			 accreditenamevalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			 datesigntab.addCell(accreditenamevalcell);
			 
			 Phrase accreditenameph=new Phrase("AFAS Accreditation Number",font10bold);
			 PdfPCell accreditenamecell=new PdfPCell(accreditenameph);
//			 accreditenamecell.addElement(accreditenameph);
			 accreditenamecell.setBorder(0);
			 accreditenamecell.setHorizontalAlignment(Element.ALIGN_CENTER);
			 datesigntab.addCell(accreditenamecell);
			 
			 
			 PdfPTable compstamptab=new PdfPTable(1);
			 compstamptab.setWidthPercentage(100);
				
				try {
					compstamptab.setWidths(new float[]{100});
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			 
			 
			 Phrase blankph=new Phrase(" ",font10bold);
			 PdfPCell blankcell=new PdfPCell(blankph);
//			 blankcell.addElement(blankph);
			 blankcell.setBorder(0);
			 blankcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			 compstamptab.addCell(blankcell);
			 
			 
			 compstamptab.addCell(blankcell);
			 compstamptab.addCell(blankcell);
			 
			 
			 Phrase compstampph=new Phrase("Company Stamp",font10bold);
			 PdfPCell compstampcell=new PdfPCell(compstampph);
//			 compstampcell.addElement(compstampph);
			 compstampcell.setBorder(0);
			 compstampcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			 compstamptab.addCell(compstampcell);
		 
			 
			 PdfPTable mainsign=new PdfPTable(3);
			 mainsign.setWidthPercentage(100);
			 mainsign.setSpacingBefore(20);
			 try {
				mainsign.setWidths(new float[]{33,33,33});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
			 PdfPCell signmaincell=new PdfPCell();
			 signmaincell.addElement(signtab);
			 signmaincell.setBorder(0);
			 signmaincell.setHorizontalAlignment(Element.ALIGN_CENTER);
			 mainsign.addCell(signmaincell);
			 
			 PdfPCell datemaincell=new PdfPCell();
			 datemaincell.addElement(datesigntab);
			 datemaincell.setBorder(0);
			 datemaincell.setHorizontalAlignment(Element.ALIGN_CENTER);
			 mainsign.addCell(datemaincell);
			 
			 PdfPCell compmaincell=new PdfPCell();
			 compmaincell.addElement(compstamptab);
			 compmaincell.setBorder(0);
			 compmaincell.setHorizontalAlignment(Element.ALIGN_CENTER);
			 mainsign.addCell(compmaincell);
			 
			 
			 try {
				document.add(mainsign);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
	}

	private void createfumigationDetails() {
		
		Image checkedImg=null;
		try {
			checkedImg = Image.getInstance("images/checked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		checkedImg.scalePercent(9);
		checkedImg.scaleAbsoluteHeight(8);
		checkedImg.scaleAbsoluteWidth(8);
		
		
		Image crosscheckedImg=null;
		try {
			crosscheckedImg = Image.getInstance("images/unchecked_checkbox.png");//crosschecked.png.jpg
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		crosscheckedImg.scalePercent(9);
		crosscheckedImg.scaleAbsoluteHeight(8);
		crosscheckedImg.scaleAbsoluteWidth(8);
		
		PdfPTable fumogationdetailtab=new PdfPTable(7);
		fumogationdetailtab.setWidthPercentage(100);
		
		try {
			fumogationdetailtab.setWidths(new float[]{30,2,23,2,23,2,18});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 Phrase fumiconductph=new Phrase("How was the fumigation conducted ? : ",font9);
		 PdfPCell fumiconductcell=new PdfPCell();
		 fumiconductcell.addElement(fumiconductph);
		 fumiconductcell.setBorder(0);
		 fumiconductcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 fumogationdetailtab.addCell(fumiconductcell);
		//add komal code
		 System.out.println("fumigation.getUnSheetedContainer()"+fumigation.getUnSheetedContainer());
		 if(fumigation.getUnSheetedContainer()==true){
				PdfPCell cumodityckeck=new PdfPCell(checkedImg);
				cumodityckeck.setBorder(0);
				cumodityckeck.setPaddingTop(8);
				fumogationdetailtab.addCell(cumodityckeck);
			}
			else{
				PdfPCell cumodityckeck=new PdfPCell(crosscheckedImg);
				cumodityckeck.setBorder(0);
				cumodityckeck.setPaddingTop(8);
				fumogationdetailtab.addCell(cumodityckeck);
			}
		 
		 
		 
		 Phrase unsheetph=new Phrase("Un-Sheeted Container ",font9);
		 PdfPCell unsheetcell=new PdfPCell();
		 unsheetcell.addElement(unsheetph);
		 unsheetcell.setBorder(0);
		 unsheetcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 fumogationdetailtab.addCell(unsheetcell);
		 
		 if(fumigation.getContainerUnderSheet()==true){
				PdfPCell cumodityckeck=new PdfPCell(checkedImg);
				cumodityckeck.setBorder(0);
				cumodityckeck.setPaddingTop(8);
				fumogationdetailtab.addCell(cumodityckeck);
			}
			else{
				PdfPCell cumodityckeck=new PdfPCell(crosscheckedImg);
				cumodityckeck.setBorder(0);
				cumodityckeck.setPaddingTop(8);
				fumogationdetailtab.addCell(cumodityckeck);
			}
		 
		 
		 Phrase sheetph=new Phrase("Sheeted Container ",font9);
		 PdfPCell sheetcell=new PdfPCell();
		 sheetcell.addElement(sheetph);
		 sheetcell.setBorder(0);
		 sheetcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 fumogationdetailtab.addCell(sheetcell);
		 
		 if(fumigation.getPermanentChamber()==true){
				PdfPCell cumodityckeck=new PdfPCell(checkedImg);
				cumodityckeck.setBorder(0);
				cumodityckeck.setPaddingTop(8);
				fumogationdetailtab.addCell(cumodityckeck);
			}
			else{
				PdfPCell cumodityckeck=new PdfPCell(crosscheckedImg);
				cumodityckeck.setBorder(0);
				cumodityckeck.setPaddingTop(8);
				fumogationdetailtab.addCell(cumodityckeck);
			}
		 
		 Phrase chamberph=new Phrase("Chamber",font9);
		 PdfPCell chambercell=new PdfPCell();
		 chambercell.addElement(chamberph);
		 chambercell.setBorder(0);
		 chambercell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 fumogationdetailtab.addCell(chambercell);
		 
		 Phrase blankph=new Phrase(" ",font9);
		 PdfPCell blankcell=new PdfPCell();
		 blankcell.addElement(blankph);
		 blankcell.setBorder(0);
		 blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 fumogationdetailtab.addCell(blankcell);
		 
		 if(fumigation.getPressureTestedContainer()==true){
				PdfPCell cumodityckeck=new PdfPCell(checkedImg);
				cumodityckeck.setBorder(0);
				cumodityckeck.setPaddingTop(8);
				fumogationdetailtab.addCell(cumodityckeck);
			}
			else{
				PdfPCell cumodityckeck=new PdfPCell(crosscheckedImg);
				cumodityckeck.setBorder(0);
				cumodityckeck.setPaddingTop(8);
				fumogationdetailtab.addCell(cumodityckeck);
			}
		 Phrase presuretestph=new Phrase("Pressure Tested Container",font9);
		 PdfPCell presuretestcell=new PdfPCell();
		 presuretestcell.addElement(presuretestph);
		 presuretestcell.setBorder(0);
		 presuretestcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 fumogationdetailtab.addCell(presuretestcell);
		 
		 if(fumigation.getStackUnderSheet()==true){
				PdfPCell cumodityckeck=new PdfPCell(checkedImg);
				cumodityckeck.setBorder(0);
				cumodityckeck.setPaddingTop(8);
				fumogationdetailtab.addCell(cumodityckeck);
			}
			else{
				PdfPCell cumodityckeck=new PdfPCell(crosscheckedImg);
				cumodityckeck.setBorder(0);
				cumodityckeck.setPaddingTop(8);
				fumogationdetailtab.addCell(cumodityckeck);
			}
		 
		 Phrase sheetedstackph=new Phrase("Sheeted Stack",font9);
		 PdfPCell sheetedstackcell=new PdfPCell();
		 sheetedstackcell.addElement(sheetedstackph);
		 sheetedstackcell.setBorder(0);
		 sheetedstackcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 fumogationdetailtab.addCell(sheetedstackcell);
		 
		 fumogationdetailtab.addCell(blankcell);
		 fumogationdetailtab.addCell(blankcell);
		 
		 Phrase containernoph=new Phrase("Container Number/s(Where Applicable):",font9);  	//Updated By:Viraj Date: 16-03-2019
		 PdfPCell containernocell=new PdfPCell();
		 containernocell.addElement(containernoph);
		 containernocell.setBorder(0);
		 containernocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 fumogationdetailtab.addCell(containernocell);
		 
		 
		 Phrase containernophval=new Phrase(fumigation.getContainerNo(),font9);
		 PdfPCell containernocellval=new PdfPCell();
		 containernocellval.addElement(containernophval);
		 containernocellval.setBorder(0);
		 containernocellval.setColspan(6);
		 containernocellval.setHorizontalAlignment(Element.ALIGN_LEFT);
		 fumogationdetailtab.addCell(containernocellval);
		 
		 Phrase blankph2=new Phrase(" ",font9);
		 PdfPCell blankcell2=new PdfPCell();
		 blankcell2.addElement(blankph2);
		 blankcell2.setBorderWidthLeft(0);
		 blankcell2.setBorderWidthRight(0);
		 blankcell2.setBorderWidthTop(0);
		 blankcell2.setColspan(7);
		 blankcell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		 fumogationdetailtab.addCell(blankcell2);
		 
		 try {
			document.add(fumogationdetailtab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable  detaitab2=new PdfPTable(2);
		detaitab2.setWidthPercentage(100);
		
		try {
			detaitab2.setWidths(new float[]{80,20});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 Phrase dosedisph=new Phrase("Does the target of the fumigation conform to the DAFF plastic wrapping ,Impervious serface and timber thickness requirment at the time of fumigation ?",font9);
		 PdfPCell dosediscell=new PdfPCell();
		 dosediscell.addElement(dosedisph);
		 dosediscell.setBorder(0);
		 dosediscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 detaitab2.addCell(dosediscell);
		 
		 Phrase yesph=new Phrase(fumigation.getNote1(),font9);
		 PdfPCell yescell=new PdfPCell();
		 yescell.addElement(yesph);
		 yescell.setBorder(0);
		 yescell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 detaitab2.addCell(yescell);
		 
//		 if(fumigation.getNote1().equals("YES")){
//				PdfPCell cumodityckeck=new PdfPCell(checkedImg);
//				cumodityckeck.setBorder(0);
//				cumodityckeck.setPaddingTop(8);
//				fumogationdetailtab.addCell(cumodityckeck);
//			}
//			else{
//				PdfPCell cumodityckeck=new PdfPCell(crosscheckedImg);
//				cumodityckeck.setBorder(0);
//				cumodityckeck.setPaddingTop(8);
//				fumogationdetailtab.addCell(cumodityckeck);
//			}
//		 
//		 
//		 Phrase noph=new Phrase("NO",font9);
//		 PdfPCell nocell=new PdfPCell();
//		 nocell.addElement(noph);
//		 nocell.setBorder(0);
//		 nocell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		 detaitab2.addCell(nocell);
//		 
//		 if(fumigation.getNote1().equals("NO")){
//				PdfPCell cumodityckeck=new PdfPCell(checkedImg);
//				cumodityckeck.setBorder(0);
//				cumodityckeck.setPaddingTop(8);
//				fumogationdetailtab.addCell(cumodityckeck);
//			}
//			else{
//				PdfPCell cumodityckeck=new PdfPCell(crosscheckedImg);
//				cumodityckeck.setBorder(0);
//				cumodityckeck.setPaddingTop(8);
//				fumogationdetailtab.addCell(cumodityckeck);
//			}
		 
		 Phrase blank4ph=new Phrase(" ",font9);
		 PdfPCell blank4cell=new PdfPCell();
		 blank4cell.addElement(blank4ph);
		 blank4cell.setBorderWidthLeft(0);
		 blank4cell.setBorderWidthRight(0);
		 blank4cell.setBorderWidthTop(0);
		 blank4cell.setColspan(3);
		 blank4cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 detaitab2.addCell(blank4cell);
		 
		 try {
			document.add(detaitab2);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 PdfPTable  detaitab3=new PdfPTable(3);
		 detaitab3.setWidthPercentage(100);
			
			try {
				detaitab3.setWidths(new float[]{30,30,40});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		 
			 Phrase ventilationph=new Phrase("Ventilation Final TLV reading (ppm)",font9);
			 PdfPCell ventilationcell=new PdfPCell();
			 ventilationcell.addElement(ventilationph);
			 ventilationcell.setBorder(0);
			 ventilationcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 detaitab3.addCell(ventilationcell);
			 
			 
			 Phrase ventilationValph=new Phrase(fumigation.getVentilation(),font9);
			 PdfPCell ventilationValcell=new PdfPCell();
			 ventilationValcell.addElement(ventilationValph);
			 ventilationValcell.setBorder(0);
			 ventilationValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 detaitab3.addCell(ventilationValcell);
			
			 Phrase ventilationdesph=new Phrase("(Not required for stack or permanent fumigation)",font9);
			 PdfPCell ventilationDescell=new PdfPCell();
			 ventilationDescell.addElement(ventilationdesph);
			 ventilationDescell.setBorder(0);
			 ventilationDescell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 detaitab3.addCell(ventilationDescell);
			 
			 Phrase blank6ph=new Phrase(" ",font9);
			 PdfPCell blank6cell=new PdfPCell();
			 blank6cell.addElement(blank6ph);
			 blank6cell.setBorderWidthLeft(0);
			 blank6cell.setBorderWidthRight(0);
			 blank6cell.setBorderWidthTop(0);
			 blank6cell.setColspan(3);
			 blank6cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 detaitab3.addCell(blank6cell);
			 
			 
			 try {
				document.add(detaitab3);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
			 
			 
	}

	private void createDetailOfTretment() {
		PdfPTable treattable=new PdfPTable(6);
		 
		treattable.setWidthPercentage(100f);
		 
		 try {
			 treattable.setWidths(new float[]{1.6f,0.1f,1.8f,1.2f,0.1f,1.0f});		//Updated By: Viraj Date:16-03-2019
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 
		    String	titlepdf="TREATMENT DETAILS";		//Updated By: Viraj Date:16-03-2019
			Phrase titlephrase=new Phrase(titlepdf,font10bold);
			Paragraph titlepdfpara=new Paragraph();
			titlepdfpara.add(titlephrase); 
			PdfPCell titlecell=new PdfPCell(titlepdfpara);
			titlecell.setBorder(0);
			titlecell.setColspan(6);
			titlecell.setHorizontalAlignment(Element.ALIGN_CENTER); //Updated By: Viraj Date:16-03-2019
			treattable.addCell(titlecell);
			
			
			 Phrase dateoffumph=new Phrase("Date Of Fumigation Completed",font9);
			 PdfPCell dateoffumcell=new PdfPCell();
			 dateoffumcell.addElement(dateoffumph);
			 dateoffumcell.setBorder(0);
			 dateoffumcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(dateoffumcell);
			 
			 Phrase seperator=new Phrase(":",font9);
			 PdfPCell seperatorcell=new PdfPCell();
			 seperatorcell.addElement(seperator);
			 seperatorcell.setBorder(0);
			 seperatorcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			 treattable.addCell(seperatorcell);
			 
			 Phrase dateoffumphValue=null;
			 if(fumigation.getDateoffumigation()!=null){
				 dateoffumphValue=new Phrase(fmt.format(fumigation.getCompletionDate()),font9);
			 }
			 else
			 {
				 dateoffumphValue=new Phrase("",font9);
			 }
		 
			 PdfPCell dateoffumphValuecell=new PdfPCell();
			 dateoffumphValuecell.addElement(dateoffumphValue);
			 dateoffumphValuecell.setBorder(0);
			 dateoffumphValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(dateoffumphValuecell);
			 
			 Phrase placeoffumi=new Phrase("Place of Fumigation",font9);
			 PdfPCell placefumicell=new PdfPCell();
			 placefumicell.addElement(placeoffumi);
			 placefumicell.setBorder(0);
			 placefumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(placefumicell);
			 
			 treattable.addCell(seperatorcell);
			 
			 Phrase placefumiValue=null;
			 if(fumigation.getPlaceoffumigation()!=null){
				 placefumiValue=new Phrase(fumigation.getPlaceoffumigation(),font9);
			 }
			 else
			 {
			 placefumiValue=new Phrase("",font9);
			 }
		 
			 PdfPCell placefumiValuecell=new PdfPCell();
			 placefumiValuecell.addElement(placefumiValue);
			 placefumiValuecell.setBorder(0);
			 placefumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(placefumiValuecell);
			 
			 
			 Phrase prescribdoseph=new Phrase("DAFF Prescribe Dose rate",font9);  	//Updated By: Viraj Date: 05-04-2019
			 PdfPCell prescribdosecell=new PdfPCell();
			 prescribdosecell.addElement(prescribdoseph);
			 prescribdosecell.setBorder(0);
			 prescribdosecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(prescribdosecell);
			 
			 treattable.addCell(seperatorcell);
			 
			 Phrase prescribdosephValue=null;
			 if(fumigation.getDoseratefumigation()!=null){
				 prescribdosephValue=new Phrase(fumigation.getDoseratefumigation(),font9);
			 }
			 else
			 {
				 prescribdosephValue=new Phrase("",font9);
			 }
		 
			 PdfPCell prescribdosephValuecell=new PdfPCell();
			 prescribdosephValuecell.addElement(prescribdosephValue);
			 prescribdosephValuecell.setBorder(0);
			 prescribdosephValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(prescribdosephValuecell);
			 
			 Phrase exposerperiodph=new Phrase("Exposure Period",font9);		//Updated By: Viraj Date: 05-04-2019
			 PdfPCell exposerperiodcell=new PdfPCell();
			 exposerperiodcell.addElement(exposerperiodph);
			 exposerperiodcell.setBorder(0);
			 exposerperiodcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(exposerperiodcell);
			 
			 treattable.addCell(seperatorcell);
			 
			 Phrase exposerperiodphValue=null;
			 if(fumigation.getDurartionfumigation()!=null){
				 exposerperiodphValue=new Phrase(fumigation.getDurartionfumigation(),font9);
			 }
			 else
			 {
				 exposerperiodphValue=new Phrase("",font9);
			 }
		 
			 PdfPCell exposerperiodphValuecell=new PdfPCell();
			 exposerperiodphValuecell.addElement(exposerperiodphValue);
			 exposerperiodphValuecell.setBorder(0);
			 exposerperiodphValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(exposerperiodphValuecell);
			 
			 
			 Phrase forecastmintempph=new Phrase("Forecast Minimum temp",font9);		//Updated By: Viraj Date:16-03-2019
			 PdfPCell forecastmintempcell=new PdfPCell();
			 forecastmintempcell.addElement(forecastmintempph);
			 forecastmintempcell.setBorder(0);
			 forecastmintempcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(forecastmintempcell);
			 
			 treattable.addCell(seperatorcell);
			 
			 Phrase forecasttempVal=null;
			 if(fumigation.getMinairtemp()!=null){
				 forecasttempVal=new Phrase(fumigation.getMinairtemp(),font9);
			 }
			 else
			 {
				 forecasttempVal=new Phrase("",font9);
			 }
		 
			 PdfPCell forecasttempValcell=new PdfPCell();
			 forecasttempValcell.addElement(forecasttempVal);
			 forecasttempValcell.setBorder(0);
			 forecasttempValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(forecasttempValcell);
			 
			 Phrase applieddoserateph=new Phrase("Applied Dose Rate",font9);		//Updated By: Viraj Date:16-03-2019
			 PdfPCell applieddoseratecell=new PdfPCell();
			 applieddoseratecell.addElement(applieddoserateph);
			 applieddoseratecell.setBorder(0);
			 applieddoseratecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(applieddoseratecell);
			 
			 treattable.addCell(seperatorcell);
			 
			 Phrase applieddoseratephVal=null;
			 if(fumigation.getAppliedDoseRate()!=null){
				 applieddoseratephVal=new Phrase(fumigation.getAppliedDoseRate(),font9);
			 }
			 else
			 {
				 applieddoseratephVal=new Phrase("",font9);
			 }
		 
			 PdfPCell applieddoseratephValcell=new PdfPCell();
			 applieddoseratephValcell.addElement(applieddoseratephVal);
			 applieddoseratephValcell.setBorder(0);
			 applieddoseratephValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(applieddoseratephValcell);
			
			 Phrase blankph=new Phrase(" ",font9);
			 PdfPCell blankphcell=new PdfPCell();
			 blankphcell.addElement(blankph);
			 blankphcell.setBorderWidthLeft(0);
			 blankphcell.setBorderWidthRight(0);
			 blankphcell.setBorderWidthTop(0);
			 blankphcell.setColspan(6);
			 blankphcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			 treattable.addCell(blankphcell);
			 
			 
			 try {
				document.add(treattable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
			 
	}

	private void createTargetOfFumigation() {
		
		Image checkedImg=null;
		try {
			checkedImg = Image.getInstance("images/checked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		checkedImg.scalePercent(9);
		checkedImg.scaleAbsoluteHeight(8);
		checkedImg.scaleAbsoluteWidth(8);
		
		
		Image crosscheckedImg=null;
		try {
			crosscheckedImg = Image.getInstance("images/unchecked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		crosscheckedImg.scalePercent(9);
		crosscheckedImg.scaleAbsoluteHeight(8);
		crosscheckedImg.scaleAbsoluteWidth(8);
		
		
		
		PdfPTable targettab=new PdfPTable(7);
		targettab.setWidthPercentage(100);
		targettab.setSpacingBefore(10);
		try {
			targettab.setWidths(new float[]{25,2,23,2,23,2,23});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		Phrase TargetFumigation =new Phrase("Target Of Fumigation :",font8bold);
		PdfPCell TargetFumigationCell=new PdfPCell(TargetFumigation);
		TargetFumigationCell.setBorder(0);
		TargetFumigationCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		targettab.addCell(TargetFumigationCell);
		
		

		if(fumigation.getCommoditybool()==true){
			PdfPCell cumodityckeck=new PdfPCell(checkedImg);
			cumodityckeck.setBorder(0);
			cumodityckeck.setPaddingTop(2);
			targettab.addCell(cumodityckeck);
		}
		else{
			PdfPCell cumodityckeck=new PdfPCell(crosscheckedImg);
			cumodityckeck.setBorder(0);
			cumodityckeck.setPaddingTop(2);
			targettab.addCell(cumodityckeck);
		}
		
		Phrase cumodityph =new Phrase("Commodity",font8bold);
		PdfPCell cumodityCell=new PdfPCell(cumodityph);
		cumodityCell.setBorder(0);
		cumodityCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		targettab.addCell(cumodityCell);
		
		if(fumigation.getPacking()==true){
			PdfPCell cumodityckeck=new PdfPCell(checkedImg);
			cumodityckeck.setBorder(0);
			cumodityckeck.setPaddingTop(2);
			targettab.addCell(cumodityckeck);
		}
		else{
			PdfPCell cumodityckeck=new PdfPCell(crosscheckedImg);
			cumodityckeck.setBorder(0);
			cumodityckeck.setPaddingTop(2);
			targettab.addCell(cumodityckeck);
		}
		
		
		
		Phrase packingph =new Phrase("Packing",font8bold);
		PdfPCell packingCell=new PdfPCell(packingph);
		packingCell.setBorder(0);
		packingCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		targettab.addCell(packingCell);
		
		if(fumigation.getBothCommodityandPacking()==true){
			PdfPCell cumodityckeck=new PdfPCell(checkedImg);
			cumodityckeck.setBorder(0);
			cumodityckeck.setPaddingTop(2);
			targettab.addCell(cumodityckeck);
		}
		else{
			PdfPCell cumodityckeck=new PdfPCell(crosscheckedImg);
			cumodityckeck.setBorder(0);
			cumodityckeck.setPaddingTop(2);
			targettab.addCell(cumodityckeck);
		}
		
		
		
		Phrase bothph =new Phrase("Both Commodity and Packing ",font8bold);
		PdfPCell bothCell=new PdfPCell(bothph);
		bothCell.setBorder(0);
		bothCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		targettab.addCell(bothCell);
		
//		Phrase comoditypara =new Phrase("Commodity :"+fumigation.getCommodity(),font8bold);
//		PdfPCell comodityparaCell=new PdfPCell(comoditypara);
//		comodityparaCell.setBorder(0);
//		comodityparaCell.setColspan(2);
//		comodityparaCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		targettab.addCell(comodityparaCell);
//		
//		Phrase quantityph =new Phrase("Quantity :"+fumigation.getQuantitydeclare(),font8bold);
//		PdfPCell quantityCell=new PdfPCell(quantityph);
//		quantityCell.setBorder(0);
//		quantityCell.setColspan(2);
//		quantityCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		targettab.addCell(quantityCell);
		
		
		
		
		PdfPTable consigntable=new PdfPTable(6);
		consigntable.setWidthPercentage(100f);
		 
		 try {
			 consigntable.setWidths(new float[]{20,2,28,20,2,28});		//updated by: viraj on date 16-03-2019
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 /**
		  * Updated By: Viraj
		  * Date: 16-03-2019
		  * Description: To show commodity and quantity 
		  */
		 Phrase comoditypara =new Phrase("Commodity",font9);
		 PdfPCell comodityparaCell=new PdfPCell(comoditypara);
		 comodityparaCell.setBorder(0);
		 comodityparaCell.setPaddingTop(5);
		 comodityparaCell.setRowspan(rowSpan);
		 comodityparaCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 consigntable.addCell(comodityparaCell);
		
		 Phrase seperator=new Phrase(":",font9);
		 PdfPCell seperatorcell=new PdfPCell();
		 seperatorcell.addElement(seperator);
		 seperatorcell.setBorder(0);
		 seperatorcell.setRowspan(rowSpan);
		 seperatorcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		 consigntable.addCell(seperatorcell);
		 
		 noOfChar = 35;
		 if(fumigation.getNameOfCommodity() != null && fumigation.getNameOfCommodity() != "") {
			 getCellRemainingLines(noOfLines, noOfChar,1,fumigation.getNameOfCommodity(), consigntable);
		 }
//		 Phrase comoditypara1 =new Phrase(fumigation.getCommodity(),font8bold);
//		 PdfPCell comoditypara1Cell=new PdfPCell(comoditypara1);
//		 comoditypara1Cell.setBorder(0);
//		 comoditypara1Cell.setPaddingTop(5);
//		 comoditypara1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		 consigntable.addCell(comoditypara1Cell);
//		 
		 Phrase quantityph =new Phrase("Quantity",font9);
		 PdfPCell quantityCell=new PdfPCell(quantityph);
		 quantityCell.setBorder(0);
		 quantityCell.setPaddingTop(5);
		 quantityCell.setRowspan(rowSpan);
		 quantityCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 consigntable.addCell(quantityCell);
		 
		 consigntable.addCell(seperatorcell);
		 String quantity = "";
		 if(fumigation.getQuantity() != null) {
			 quantity = fumigation.getQuantity();
		 }
		 if(quantity != null) {
			 getCellRemainingLines(noOfLines, noOfChar,0,quantity, consigntable);
		 }
//		 Phrase quantityValph =new Phrase(quantity+"",font9);
//		 PdfPCell quantityValphCell=new PdfPCell(quantityValph);
//		 quantityValphCell.setBorder(0);
//		 quantityValphCell.setPaddingTop(5);
//		 quantityValphCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		 consigntable.addCell(quantityValphCell);
		 /** Ends **/
		 
		 Phrase consignmentlink=new Phrase("Consignment Link",font9);
		 PdfPCell consignmentlinkcell=new PdfPCell();
		 consignmentlinkcell.addElement(consignmentlink);
		 consignmentlinkcell.setBorder(0);
		 consignmentlinkcell.setRowspan(rowSpan);
		 consignmentlinkcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 consigntable.addCell(consignmentlinkcell); 
		 
		 consigntable.addCell(seperatorcell);
		 
		 Phrase consignmentlinkval=null;
		 if(fumigation.getConsignmentlink()!=null){
			 consignmentlinkval=new Phrase(fumigation.getConsignmentlink(),font9);
		 }
		 else
		 {
			 consignmentlinkval=new Phrase("",font9);
		 }
	 
		 PdfPCell consignmentlinkvalcell=new PdfPCell();
		 consignmentlinkvalcell.addElement(consignmentlinkval);
		 consignmentlinkvalcell.setBorder(0);
		 consignmentlinkvalcell.setRowspan(rowSpan);
		 consignmentlinkvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 consigntable.addCell(consignmentlinkvalcell);
		
		 Phrase contryph=new Phrase("Country Of Origin",font9);
		 PdfPCell contrycell=new PdfPCell();
		 contrycell.addElement(contryph);
		 contrycell.setBorder(0);
		 contrycell.setRowspan(rowSpan);
		 contrycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 consigntable.addCell(contrycell); 
		 
		 consigntable.addCell(seperatorcell);
		 
		 Phrase contryphval=null;
		 if(fumigation.getImportcountry()!=null){
			 contryphval=new Phrase(fumigation.getImportcountry(),font9);
		 }
		 else
		 {
			 contryphval=new Phrase("",font9);
		 }
	 
		 PdfPCell contryphvalcell=new PdfPCell();
		 contryphvalcell.addElement(contryphval);
		 contryphvalcell.setBorder(0);
		 contryphvalcell.setRowspan(rowSpan);
		 contryphvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 consigntable.addCell(contryphvalcell);
		 
		 
		 Phrase portofloadph=new Phrase("Port Of Loading ",font9);
		 PdfPCell portofloadcell=new PdfPCell();
		 portofloadcell.addElement(portofloadph);
		 portofloadcell.setBorder(0);
		 portofloadcell.setRowspan(rowSpan);
		 portofloadcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 consigntable.addCell(portofloadcell); 
		 
		 consigntable.addCell(seperatorcell);
		 
		 Phrase portofloadphval=null;
		 if(fumigation.getPortncountryloading()!=null){
			 portofloadphval=new Phrase(fumigation.getPortncountryloading(),font9);
		 }
		 else
		 {
			 portofloadphval=new Phrase("",font9);
		 }
	 
		 PdfPCell portofloadvalcell=new PdfPCell();
		 portofloadvalcell.addElement(portofloadphval);
		 portofloadvalcell.setBorder(0);
		 portofloadvalcell.setRowspan(rowSpan);
		 portofloadvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 consigntable.addCell(portofloadvalcell);
		 
		 Phrase countyofdesph=new Phrase("Country Of Destination ",font9);
		 PdfPCell countyofdescell=new PdfPCell();
		 countyofdescell.addElement(countyofdesph);
		 countyofdescell.setBorder(0);
		 countyofdescell.setRowspan(rowSpan);
		 countyofdescell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 consigntable.addCell(countyofdescell); 
		 
		 consigntable.addCell(seperatorcell);
		 
		 Phrase countyofdesphval=null;
		 if(fumigation.getCountryofdestination()!=null){
			 countyofdesphval=new Phrase(fumigation.getCountryofdestination(),font9);
		 }
		 else
		 {
			 countyofdesphval=new Phrase("",font9);
		 }
	 
		 PdfPCell countyofdesphvalcell=new PdfPCell();
		 countyofdesphvalcell.addElement(countyofdesphval);
		 countyofdesphvalcell.setBorder(0);
		 countyofdesphvalcell.setRowspan(rowSpan);
		 countyofdesphvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 consigntable.addCell(countyofdesphvalcell);
		
		 
		 PdfPCell targetcell=new PdfPCell(targettab);
		 targetcell.setBorderWidthBottom(0);
		 targetcell.setBorderWidthLeft(0);
		 targetcell.setBorderWidthRight(0);
		 
		 PdfPCell consigncell=new PdfPCell(consigntable);
		 consigncell.setBorderWidthLeft(0);
		 consigncell.setBorderWidthRight(0);
		 consigncell.setBorderWidthTop(0);
		 
		 PdfPTable mainTab=new PdfPTable(1);
		 mainTab.setWidthPercentage(100);
		 
		 mainTab.addCell(targetcell);
		 mainTab.addCell(consigncell);
		 
		 try {
			document.add(mainTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 
		 PdfPTable custtab=new PdfPTable(2);
		 custtab.setWidthPercentage(100f);
			 
			 try {
				 custtab.setWidths(new float[]{50,50});
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		 
			 	Phrase fromaddress=new Phrase("Name and address of consignor / exporter ",font9);
				PdfPCell fromaddcell=new PdfPCell();
				fromaddcell.addElement(fromaddress);
				fromaddcell.setBorderWidthLeft(0);
				fromaddcell.setBorderWidthBottom(0);
				custtab.addCell(fromaddcell); 
				
				Phrase toaddress=new Phrase(" Name and address Of importer ",font9);
				PdfPCell toaddcell=new PdfPCell();
				toaddcell.addElement(toaddress);
				toaddcell.setBorderWidthRight(0);
				toaddcell.setBorderWidthBottom(0);
				custtab.addCell(toaddcell);
				
				Phrase fromaddressvalph=new Phrase(fumigation.getFromCompanyname()+"\n"+fumigation.getFromAddess(),font9);
				PdfPCell fromaddvalcell=new PdfPCell();
				fromaddvalcell.addElement(fromaddressvalph);
				fromaddvalcell.setBorderWidthLeft(0);
				fromaddvalcell.setBorderWidthTop(0);
				custtab.addCell(fromaddvalcell);
				
				Phrase toaddressvalph=new Phrase(fumigation.getToCompanyname()+"\n"+fumigation.getToAddress(),font9);
				PdfPCell toaddvalcell=new PdfPCell();
				toaddvalcell.addElement(toaddressvalph);
				toaddvalcell.setBorderWidthRight(0);
				toaddvalcell.setBorderWidthTop(0);;
				custtab.addCell(toaddvalcell);
				
				try {
					document.add(custtab);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
				
	}

	private void createcertificateinfo() {
		
		PdfPTable certificatetab=new PdfPTable(5);
		certificatetab.setWidthPercentage(100);
		certificatetab.setSpacingBefore(10);
		try {
			certificatetab.setWidths(new float[]{20,45,10,10,15});		//updated by viraj on date 16-03-2019 to adjust the width of the columns
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase certnoph=new Phrase("CERTIFICATE NUMBER : ",font8bold);
		PdfPCell certnoCell=new PdfPCell(certnoph);
		certnoCell.setBorder(0);
		certnoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(certnoCell);
		
		System.out.println("certificate number: "+fumigation.getCertificateNo());
		Phrase certnoValph=new Phrase(fumigation.getCertificateNo(),font8bold);
		PdfPCell certnoValCell=new PdfPCell(certnoValph);
		certnoValCell.setBorder(0);
		certnoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(certnoValCell);
		
		Phrase blankph=new Phrase(" ",font8);
		PdfPCell blankCell=new PdfPCell(blankph);
		blankCell.setBorder(0);
		blankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(blankCell);
		
		Phrase srph=new Phrase("AEI No. :",font8bold);		//updated by viraj on date 16-03-2019
		PdfPCell srCell=new PdfPCell(srph);
		srCell.setBorder(0);
		srCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(srCell);
		
		System.out.println("AEI No: "+fumigation.getAeiRegNO());
		Phrase srvalph=new Phrase(fumigation.getAeiRegNO(),font8bold);
		PdfPCell srvalCell=new PdfPCell(srvalph);
		srvalCell.setBorder(0);
		srvalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificatetab.addCell(srvalCell);
		
		
		Phrase titleph=new Phrase("TARGET OF FUMIGATION DETAILS ",font8bold);
		PdfPCell titleCell=new PdfPCell(titleph);
		titleCell.setBorder(0);
		titleCell.setColspan(5);
		titleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		certificatetab.addCell(titleCell);
		
		try {
			document.add(certificatetab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

	private void createheader() {
		
		PdfPTable companyDetails=new PdfPTable(1);
		companyDetails.setWidthPercentage(100);
		
		
		Phrase compName =new Phrase(comp.getBusinessUnitName(),font16bold);
		PdfPCell compNameCell=new PdfPCell(compName);
		compNameCell.setBorder(0);
		compNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		companyDetails.addCell(compNameCell);
		
		/**
		 * Updated By: Viraj
		 * Date: 14-01-2019
		 * Description: To add company description
		 */
		if(comp.getDescription() != null) {
			Phrase compDesc =new Phrase(comp.getDescription(),font10bold);
			PdfPCell compDescCell=new PdfPCell(compDesc);
			compDescCell.setBorder(0);
			compDescCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			companyDetails.addCell(compDescCell);
		} else {
			Phrase compDesc =new Phrase("",font10bold);
			PdfPCell compDescCell=new PdfPCell(compDesc);
			compDescCell.setBorder(0);
			compDescCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			companyDetails.addCell(compDescCell);
		}
		/** Ends **/
		
		String branchaddress=null;
		System.out.println("branchlist"+branchlist.size());
		for (int i = 0; i <branchlist.size(); i++) {
			branchaddress=branchlist.get(i).getAddress().getCompleteAddress();
		}
		System.out.println("branchaddress"+branchaddress);
		Phrase branchadd=new Phrase ("Branch Office :"+branchaddress,font10);
		PdfPCell branchaddCell=new PdfPCell(branchadd);
		branchaddCell.setBorder(0);
		branchaddCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		companyDetails.addCell(branchaddCell);
		
//		Phrase isoph=new Phrase ("AN ISO 9001 :",font10);
//		PdfPCell isoCell=new PdfPCell(isoph);
//		isoCell.setBorder(0);
//		isoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		companyDetails.addCell(isoCell);
		
		try {
			document.add(companyDetails);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		PdfPTable titletab=new PdfPTable(1);
		titletab.setWidthPercentage(100);
		// updated by viraj on date 10-04-2019
		Phrase titleph=new Phrase("AFAS-"+fumigation.getNameoffumigation()+" FUMIGATION CERTIFICATE ",font12);
		PdfPCell titleCell=new PdfPCell(titleph);
		titleCell.setBorder(0);
		titleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		titletab.addCell(titleCell);
		
		try {
			document.add(titletab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	/**
	 * Update By: Viraj
	 * Date: 15-01-2019
	 * Description: To add cell and remaining blank cell according to no of lines and decided lines for that cell
	 * @param noOfLines
	 * @param noOfChar
	 * @param value
	 * @param table
	 */
	public void getCellRemainingLines(int noOfLines,int noOfChar,int colspan,String value,PdfPTable table) {
		int adChar = value.length();
		int vChar = noOfLines * noOfChar;
		int tChar = 3 * noOfChar ;
		int t1Char = 2 * noOfChar;
		int t2Char = 1 * noOfChar;
		
		Phrase blank3ph=new Phrase(" ",font9);
		PdfPCell blank3cell=new PdfPCell();
		blank3cell.addElement(blank3ph);
		blank3cell.setColspan(colspan);
		blank3cell.setBorder(0);
		// 1 line
		if((0 <= adChar) && (adChar <= t2Char)) {
			System.out.println("2 blank line" +adChar);
			Phrase adddeclareval=new Phrase(value.substring(0, adChar),font9);
			PdfPCell adddeclarevalCell=new PdfPCell(adddeclareval);
			adddeclarevalCell.setBorder(0);
			adddeclarevalCell.setPaddingTop(5);
			adddeclarevalCell.setColspan(colspan);
			adddeclarevalCell.setRowspan(noOfLines);
			adddeclarevalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(adddeclarevalCell);
//			table.addCell(blank3cell);
//			table.addCell(blank3cell);
		} else if((t2Char <= adChar) && (adChar <= t1Char)) {								//line 2
					System.out.println("1 blank line" +adChar);
					Phrase adddeclareval=new Phrase(value.substring(0, adChar),font9);
					PdfPCell adddeclarevalCell=new PdfPCell(adddeclareval);
					adddeclarevalCell.setBorder(0);
					adddeclarevalCell.setPaddingTop(5);
					adddeclarevalCell.setColspan(colspan);
					adddeclarevalCell.setRowspan(noOfLines);
					adddeclarevalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					table.addCell(adddeclarevalCell);
					
				//	table.addCell(blank3cell);
				} else if((t1Char <= adChar) && (tChar >= adChar)) {						//line 3
					System.out.println("0 blank line 3 lines exact" +adChar);
					Phrase adddeclareval=new Phrase(value.substring(0, adChar),font9);
					PdfPCell adddeclarevalCell=new PdfPCell(adddeclareval);
					adddeclarevalCell.setBorder(0);
					adddeclarevalCell.setPaddingTop(5);
					adddeclarevalCell.setColspan(colspan);
					adddeclarevalCell.setRowspan(noOfLines);
					adddeclarevalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					table.addCell(adddeclarevalCell);
				} else if(adChar > tChar) {													// more than 3 lines
					System.out.println("0 blank line more than 3 lines" +adChar);
					Phrase adddeclareval=new Phrase(value.substring(0, vChar),font9);
					PdfPCell adddeclarevalCell=new PdfPCell(adddeclareval);
					adddeclarevalCell.setBorder(0);
					adddeclarevalCell.setPaddingTop(5);
					adddeclarevalCell.setColspan(colspan);
					adddeclarevalCell.setRowspan(noOfLines);
					adddeclarevalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					table.addCell(adddeclarevalCell);
				}
				else {
					table.addCell(blank3cell);
					table.addCell(blank3cell);
					table.addCell(blank3cell);
				}
	}
/** Ends **/

}

