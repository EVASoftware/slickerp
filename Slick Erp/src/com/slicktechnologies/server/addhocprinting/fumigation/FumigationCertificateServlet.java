package com.slicktechnologies.server.addhocprinting.fumigation;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;

public class FumigationCertificateServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1350125552590712565L;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {
			
			String stringId = req.getParameter("Id");
			String type=req.getParameter("type");
			stringId = stringId.trim();
			Long count = Long.parseLong(stringId);
			
			if(type.equalsIgnoreCase("MB")){
			FumigationCertificateMB printpdf = new FumigationCertificateMB();
			printpdf.document = new Document();
			Document document = printpdf.document;
			PdfWriter writer = PdfWriter.getInstance(document,resp.getOutputStream());
			
			document.open();
			printpdf.loadfugation(count);
			printpdf.createPdf();
			document.close();
			}
			else if(type.equalsIgnoreCase("ALP")){
			
			FumigationCertificateALP printpdf = new FumigationCertificateALP();
			printpdf.document = new Document();
			Document document = printpdf.document;
			PdfWriter writer = PdfWriter.getInstance(document,resp.getOutputStream());
				
			document.open();
			printpdf.loadfugation(count);
			printpdf.createPdf();
			document.close();
				
			}
			else if(type.equalsIgnoreCase("AFAS")){
			FumigationCertificateAFAS printpdf = new FumigationCertificateAFAS();
			printpdf.document = new Document();
			Document document = printpdf.document;
			PdfWriter writer = PdfWriter.getInstance(document,resp.getOutputStream());
					
			document.open();
			printpdf.loadfugation(count);
			printpdf.createPdf();
			document.close();
			}
		}
		catch (DocumentException e) {
			e.printStackTrace();
		}
}
}
