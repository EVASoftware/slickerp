package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.addhocprinting.JobCardPdf;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.paymentmodelayer.vendorpayment.VendorPayment;

public class VendorPaymentPDF {
	VendorPayment qp;
	ArrayList<Service> service;
	Company comp;
	Customer cust;

	public Document document;
	public GenricServiceImpl impl;
	public int serviceno = 1;
	private Font font16boldul, font12bold, font8bold, font8, font12boldul,font12;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");

	public VendorPaymentPDF() {
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		impl = new GenricServiceImpl();
	}

	public void getVendorPaymentDetail(long count) {
		qp = ofy().load().type(VendorPayment.class).id(count).now();
		int paymentCount = qp.getCount();

		if (qp.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", qp.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();
	}

	public void createPdf() {

		JobCardPdf.showLogo(document, comp);
		createCompanyHedding();
		createCompanyAdress();
		try {
			createHeaderTable();
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
	}

	// patch patch patch
	public void createPdfForEmail(Company comp, VendorPayment qp) {

		this.comp = comp;
		this.qp = qp;
		createCompanyHedding();
		createCompanyAdress();
	}

	public void createCompanyHedding() {

		Phrase companyName = new Phrase(comp.getBusinessUnitName(),font16boldul);
		Paragraph p = new Paragraph();

		p.add(Chunk.NEWLINE);
		p.add(companyName);
		p.setAlignment(Element.ALIGN_CENTER);
		try {
			document.add(p);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createCompanyAdress() {

		Phrase adressline1 = new Phrase(comp.getAddress().getAddrLine1(),font12);
		Phrase adressline2 = null;
		if (comp.getAddress().getAddrLine2() != null) {
			adressline2 = new Phrase(comp.getAddress().getAddrLine2(), font12);
		}

		Phrase landmark = null;
		Phrase locality = null;
		if (comp.getAddress().getLandmark() != null && comp.getAddress().getLocality().equals("") == false) {
			String landmarks = comp.getAddress().getLandmark() + " , ";
			locality = new Phrase(landmarks + comp.getAddress().getLocality()
					+ " , " + comp.getAddress().getCity() + " , "
					+ comp.getAddress().getPin(), font12);
		} else {
			locality = new Phrase(comp.getAddress().getLocality() + " , "
					+ comp.getAddress().getCity() + " , "
					+ comp.getAddress().getPin(), font12);
		}
		
		Paragraph adressPragraph = new Paragraph();
		adressPragraph.add(adressline1);
		adressPragraph.add(Chunk.NEWLINE);
		if (adressline2 != null) {
			adressPragraph.add(adressline2);
			adressPragraph.add(Chunk.NEWLINE);
		}

		adressPragraph.add(locality);

		adressPragraph.setAlignment(Element.ALIGN_CENTER);
		adressPragraph.setSpacingBefore(10);
		adressPragraph.setSpacingAfter(10);

		// Phrase for phone,landline ,fax and email

		Phrase titlecell = new Phrase("Mob :", font8bold);
		Phrase titleTele = new Phrase("Tele :", font8bold);
		Phrase titleemail = new Phrase("Email :", font8bold);
		Phrase titlefax = new Phrase("Fax :", font8bold);

		Phrase titleservicetax = new Phrase("Service Tax No     :", font8bold);
		Phrase titlevatatx = new Phrase("Vat Tax No :    ", font8bold);
		String servicetax = comp.getServiceTaxNo();
		String vatttax = comp.getVatTaxNo();
		Phrase servicetaxphrase = null;
		Phrase vattaxphrase = null;

		if (servicetax != null && servicetax.trim().equals("") == false) {
			servicetaxphrase = new Phrase(servicetax, font8);
		}

		if (vatttax != null && vatttax.trim().equals("") == false) {
			vattaxphrase = new Phrase(vatttax, font8);
		}

		// cell number logic
		String stringcell1 = comp.getContact().get(0).getCellNo1() + "";
		String stringcell2 = null;
		Phrase mob = null;
		if (comp.getContact().get(0).getCellNo2() != -1)
			stringcell2 = comp.getContact().get(0).getCellNo2() + "";
		if (stringcell2 != null)
			mob = new Phrase(stringcell1 + " / " + stringcell2, font8);
		else
			mob = new Phrase(stringcell1, font8);

		// LANDLINE LOGIC
		Phrase landline = null;
		if (comp.getContact().get(0).getLandline() != -1)
			landline = new Phrase(comp.getContact().get(0).getLandline() + "",
					font8);

		// fax logic
		Phrase fax = null;
		if (comp.getContact().get(0).getFaxNo() != null)
			fax = new Phrase(comp.getContact().get(0).getFaxNo() + "", font8);
		// email logic
		Phrase email = new Phrase(comp.getContact().get(0).getEmail(), font8);

		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(titlecell);
		adressPragraph.add(mob);
		adressPragraph.add(new Chunk("            "));

		if (landline != null) {

			adressPragraph.add(titleTele);
			adressPragraph.add(landline);
			adressPragraph.add(new Chunk("            "));
		}

		if (fax != null) {

			adressPragraph.add(titlefax);
			adressPragraph.add(fax);
			adressPragraph.add(Chunk.NEWLINE);
		}

		adressPragraph.add(titleemail);
		adressPragraph.add(new Chunk("            "));
		adressPragraph.add(email);
		adressPragraph.add(Chunk.NEWLINE);

		if (servicetaxphrase != null) {
			adressPragraph.add(titleservicetax);
			adressPragraph.add(servicetaxphrase);

		}

		if (vattaxphrase != null) {
			adressPragraph.add(titlevatatx);
			adressPragraph.add(vattaxphrase);
			adressPragraph.add(new Chunk("            "));

		}

		adressPragraph.add(Chunk.NEWLINE);
		String title = "Payment Mode";

		Phrase Ptitle = new Phrase(title, font12bold);
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(Ptitle);
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(Chunk.NEWLINE);

		try {
			document.add(adressPragraph);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	public void createHeaderTable() throws DocumentException {

		// //////LEFT HAND SIDE TABLE 0 top
		Phrase semicolon =new Phrase(" :",font8bold);
		PdfPCell semicolonCell=new PdfPCell();
		semicolonCell.addElement(semicolon);
		
		Phrase instruction = new Phrase("Instruction", font8bold);
		PdfPCell instructioncell = new PdfPCell();
		instructioncell.addElement(instruction);

		PdfPTable table0 = new PdfPTable(1);

		// 1  row name phone
		table0.addCell(instructioncell);

		table0.setWidths(new float[] { 30 });
		table0.setHorizontalAlignment(Element.ALIGN_LEFT);
		table0.setWidthPercentage(100);

		
		// //////RIGHT HAND SIDE TABLE 0 top
		
		PdfPCell instructioncelldb = new PdfPCell();
		if (qp.getVendorPaymentInstruction() != null) {
			Phrase instructiondb = new Phrase(qp.getVendorPaymentInstruction(),font8);
			instructioncelldb.addElement(instructiondb);
		} else {
			Phrase instructiondb = new Phrase(" ", font8);
			instructioncelldb.addElement(instructiondb);
		}

		PdfPTable table01 = new PdfPTable(1);

		// 1  row name phone
		table01.addCell(instructioncelldb);

		table01.setWidths(new float[] { 15 });
		table01.setHorizontalAlignment(Element.ALIGN_LEFT);
		table01.setWidthPercentage(100);

		// //////LEFT HAND SIDE TABLE 1

		Phrase nameTitle = new Phrase("1. Cheque", font8bold);

		PdfPCell titlecustnameCell = new PdfPCell();
		titlecustnameCell.addElement(nameTitle);

		PdfPTable table = new PdfPTable(1);

		// 1 st row name phone
		table.addCell(titlecustnameCell);

		table.setWidths(new float[] { 30 });
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.setWidthPercentage(100);

		// //////////////// RIGHT HAND SIDE TABLE 2

		Phrase orederdate = new Phrase("In Favouring Of", font8bold);
		PdfPCell titledatecell = new PdfPCell();
		titledatecell.addElement(orederdate);
		
		PdfPCell favouringcell = new PdfPCell();
		if(qp.getVendorPaymentFavouring()!=null){
			Phrase favouring = new Phrase(qp.getVendorPaymentFavouring(), font8);
			favouringcell.addElement(favouring);
		}else{
			Phrase favouring = new Phrase(" ", font8);
			favouringcell.addElement(favouring);
		}
		

		Phrase duration = new Phrase("Payable At", font8bold);
		PdfPCell titledurationCell = new PdfPCell();
		titledurationCell.addElement(duration);
		
		PdfPCell payaableatcell = new PdfPCell();
		if(qp.getVendorPaymentPayableAt()!=null){
			Phrase payableat = new Phrase(qp.getVendorPaymentPayableAt(), font8);
			payaableatcell.addElement(payableat);
		}else{
			Phrase payableat = new Phrase(" ", font8);
			payaableatcell.addElement(payableat);
		}

		PdfPTable billtable = new PdfPTable(3);

		billtable.setWidths(new float[] { 10,5, 30, });
		billtable.setWidthPercentage(100);
		billtable.setHorizontalAlignment(Element.ALIGN_LEFT);

		// 1 row bill number and date
		billtable.addCell(titledatecell);
		billtable.addCell(semicolonCell);
		billtable.addCell(favouringcell);
		
		// 2 row
		billtable.addCell(titledurationCell);
		billtable.addCell(semicolonCell);
		billtable.addCell(payaableatcell);
		
		// //////LEFT HAND SIDE TABLE 3

		Phrase nefttitle = new Phrase("2. NEFT/RTGS/SWIFT", font8bold);

		PdfPCell neftcell = new PdfPCell();
		neftcell.addElement(nefttitle);

		PdfPTable table3 = new PdfPTable(1);

		// 1 st row name phone
		table3.addCell(neftcell);

		table3.setWidths(new float[] { 30 });
		table3.setHorizontalAlignment(Element.ALIGN_LEFT);
		table3.setWidthPercentage(100);

		// ////////////////RIGHT HAND SIDE TABLE 4

		Phrase accountno = new Phrase("Account No.", font8bold);
		PdfPCell accountnocell = new PdfPCell();
		accountnocell.addElement(accountno);
		
		PdfPCell accoutnnodbcell = new PdfPCell();
		if(qp.getVendorPaymentAccountNo()!=null){
			Phrase accountnodb = new Phrase(qp.getVendorPaymentAccountNo(), font8);
			accoutnnodbcell.addElement(accountnodb);
		}else{
			Phrase accountnodb = new Phrase(" ", font8);
			accoutnnodbcell.addElement(accountnodb);
		}

		Phrase bankname = new Phrase("Bank Name", font8bold);
		PdfPCell banknamecell = new PdfPCell();
		banknamecell.addElement(bankname);

		Phrase banknamedb = new Phrase(qp.getVendorPaymentBankName(), font8);
		PdfPCell banknamecelldb = new PdfPCell();
		banknamecelldb.addElement(banknamedb);

		Phrase branch = new Phrase("Branch", font8bold);
		PdfPCell branchCell = new PdfPCell();
		branchCell.addElement(branch);
		
		PdfPCell branchdbcell = new PdfPCell();
		if(qp.getVendorPaymentBranch()!=null){
			Phrase branchdb = new Phrase(qp.getVendorPaymentBranch(), font8);
			branchdbcell.addElement(branchdb);
		}
		else{
			Phrase branchdb = new Phrase(" ", font8);
			branchdbcell.addElement(branchdb);
		}

		Phrase address = new Phrase("Address", font8bold);
		PdfPCell addresscell = new PdfPCell();
		addresscell.addElement(address);

		Phrase ifsc = new Phrase("IFSC Code", font8bold);
		PdfPCell ifsccell = new PdfPCell();
		ifsccell.addElement(ifsc);

		Phrase micr = new Phrase("MICR Code", font8bold);
		PdfPCell micrcell = new PdfPCell();
		micrcell.addElement(micr);

		Phrase swift = new Phrase("SWIFT Code", font8bold);
		PdfPCell swiftcell = new PdfPCell();
		swiftcell.addElement(swift);
		
		PdfPCell ifsccelldb = new PdfPCell();
		if(qp.getVendorPaymentIFSCcode()!=null){
			Phrase ifscdb = new Phrase(qp.getVendorPaymentIFSCcode(), font8);
			ifsccelldb.addElement(ifscdb);
		}else{
			Phrase ifscdb = new Phrase(" ", font8);
			ifsccelldb.addElement(ifscdb);
		}
		
		PdfPCell micrcelldb = new PdfPCell();
		if(qp.getVendorPaymentMICRcode()!=null){
			Phrase micredb = new Phrase(qp.getVendorPaymentMICRcode(), font8);
			micrcelldb.addElement(micredb);
		}else{
			Phrase micredb = new Phrase(" ", font8);
			micrcelldb.addElement(micredb);
		}
		
		PdfPCell swiftcelldb = new PdfPCell();
		if(qp.getVendorPaymentSWIFTcode()!=null){
			Phrase swiftdb = new Phrase(qp.getVendorPaymentSWIFTcode(), font8);
			swiftcelldb.addElement(swiftdb);
		}else{
			Phrase swiftdb = new Phrase(" ", font8);
			swiftcelldb.addElement(swiftdb);
		}
		

		// /// For Printing Address from Address Composite

		Phrase customeradress = new Phrase(qp.getAddressInfo().getAddrLine1(),font8);
		Phrase customeradress2 = null;

		if (qp.getAddressInfo().getAddrLine2() != null)
			customeradress2 = new Phrase(qp.getAddressInfo().getAddrLine2(),font8);

		Phrase landmark = null;
		Phrase locality = null;

		if (qp.getAddressInfo().getLandmark() != null) {
			landmark = new Phrase(qp.getAddressInfo().getLandmark());
			locality = new Phrase(landmark + " "
					+ qp.getAddressInfo().getLocality(), font8);
		} else
			locality = new Phrase(qp.getAddressInfo().getLocality(), font8);

		Phrase cityState = new Phrase(qp.getAddressInfo().getCity() + " "
				+ qp.getAddressInfo().getState(), font8);
		Phrase pin = new Phrase("  " + qp.getAddressInfo().getPin() + "",
				font8);
		Phrase country = new Phrase(qp.getAddressInfo().getCountry(), font8);
		Paragraph adresspara = new Paragraph();

		adresspara.add(customeradress);
		adresspara.add(Chunk.NEWLINE);

		if (customeradress2 != null)
			adresspara.add(customeradress2);

		adresspara.add(locality);
		adresspara.add("  ");
		adresspara.add(cityState);
		adresspara.add("   ");
		adresspara.add(pin);
		adresspara.add(Chunk.NEWLINE);
		adresspara.add(country);

		// ////////////////////////////

		PdfPCell adressCell = new PdfPCell();
		adressCell.addElement(adresspara);

		PdfPTable table4 = new PdfPTable(3);

		table4.setWidths(new float[] { 10,5, 30, });
		table4.setWidthPercentage(100);
		table4.setHorizontalAlignment(Element.ALIGN_LEFT);

		// 1 row bill number and date
		table4.addCell(accountnocell);
		table4.addCell(semicolonCell);
		table4.addCell(accoutnnodbcell);
		// 2 row
		table4.addCell(banknamecell);
		table4.addCell(semicolonCell);
		table4.addCell(banknamecelldb);
		// 3 row
		table4.addCell(branchCell);
		table4.addCell(semicolonCell);
		table4.addCell(branchdbcell);
		// 4 row
		table4.addCell(ifsccell);
		table4.addCell(semicolonCell);
		table4.addCell(ifsccelldb);

		// 5 row
		table4.addCell(micrcell);
		table4.addCell(semicolonCell);
		table4.addCell(micrcelldb);

		// 6 row
		table4.addCell(swiftcell);
		table4.addCell(semicolonCell);
		table4.addCell(swiftcelldb);

		// 7 row
		table4.addCell(addresscell);
		table4.addCell(semicolonCell);
		table4.addCell(adressCell);

		// For Table 0
		for (int i = 0; i < 1; i++) {
			PdfPRow temp = table0.getRow(i);
			PdfPCell[] cells = temp.getCells();
			for (PdfPCell cell : cells)
				cell.setBorder(Rectangle.NO_BORDER);
		}

		// For Table 01
		for (int i = 0; i < 1; i++) {
			PdfPRow temp = table01.getRow(i);
			PdfPCell[] cells = temp.getCells();
			for (PdfPCell cell : cells)
				cell.setBorder(Rectangle.NO_BORDER);
		}

		// For Table 1
		for (int i = 0; i < 1; i++) {
			PdfPRow temp = table.getRow(i);
			PdfPCell[] cells = temp.getCells();
			for (PdfPCell cell : cells)
				cell.setBorder(Rectangle.NO_BORDER);
		}
		// For Table 2
		for (int i = 0; i < 2; i++) {
			PdfPRow temp = billtable.getRow(i);
			PdfPCell[] cells = temp.getCells();
			for (PdfPCell cell : cells)
				cell.setBorder(Rectangle.NO_BORDER);
		}

		// For Table 3
		for (int i = 0; i < 1; i++) {
			PdfPRow temp = table3.getRow(i);
			PdfPCell[] cells = temp.getCells();
			for (PdfPCell cell : cells)
				cell.setBorder(Rectangle.NO_BORDER);
		}

		// For Table 4
		for (int i = 0; i < 7; i++) {
			PdfPRow temp = table4.getRow(i);
			PdfPCell[] cells = temp.getCells();
			for (PdfPCell cell : cells)
				cell.setBorder(Rectangle.NO_BORDER);
		}

		PdfPTable parenttable = new PdfPTable(2);
		parenttable.setWidthPercentage(100);
		parenttable.setWidths(new float[] { 30, 70 });

		PdfPCell cell0 = new PdfPCell();
		PdfPCell cell1 = new PdfPCell();
		PdfPCell custinfocell = new PdfPCell();
		PdfPCell billinfocell = new PdfPCell();
		PdfPCell cell3 = new PdfPCell();
		PdfPCell cell4 = new PdfPCell();

		cell0.addElement(table0);
		cell1.addElement(table01);
		custinfocell.addElement(table);
		billinfocell.addElement(billtable);
		cell3.addElement(table3);
		cell4.addElement(table4);

		parenttable.addCell(cell0);
		parenttable.addCell(cell1);
		parenttable.addCell(custinfocell);
		parenttable.addCell(billinfocell);
		parenttable.addCell(cell3);
		parenttable.addCell(cell4);

		document.add(parenttable);

	}

}
